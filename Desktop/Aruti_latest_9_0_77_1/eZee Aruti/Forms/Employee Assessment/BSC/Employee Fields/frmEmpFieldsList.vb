﻿#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data

#End Region

Public Class frmEmpFieldsList

#Region " Private Variable "

    Private ReadOnly mstrModuleName As String = "frmEmpFieldsList"
    Private mdtFinal As DataTable = Nothing
    Private objEmpField1 As clsassess_empfield1_master
    Private objEmpField2 As clsassess_empfield2_master
    Private objEmpField3 As clsassess_empfield3_master
    Private objEmpField4 As clsassess_empfield4_master
    Private objEmpField5 As clsassess_empfield5_master
    Private cmnuAdd, cmnuEdit, cmnuDelete As ContextMenuStrip
    Private objFMaster As New clsAssess_Field_Master(True)
    Private mblnDropDownClosed As Boolean = False
    'S.SANDEEP |18-JAN-2019| -- START
    Private strPerspectiveColName As String = String.Empty
    Private mintPerspectiveColIndex As Integer = -1
    Private strGoalValueColName As String = String.Empty
    Private mintGoalValueColIndex As Integer = -1
    'S.SANDEEP |18-JAN-2019| -- END

#End Region

#Region " Private Methods "

    Private Sub SetFormViewSetting()
        Try
            If ConfigParameter._Object._CascadingTypeId = enPACascading.NEITHER_CASCADING_NOR_ALIGNMENT Then
                cboPerspective.Enabled = True
            Else
                cboPerspective.Enabled = False
            End If

            objlblField1.Text = objFMaster._Field1_Caption
            cboFieldValue1.Tag = objFMaster._Field1Unkid
            cboFieldValue1.DrawMode = DrawMode.OwnerDrawFixed
            If objlblField1.Text = "" Then
                cboFieldValue1.Enabled = False : objbtnSearchField1.Enabled = False
            End If
            objlblField2.Text = objFMaster._Field2_Caption
            cboFieldValue2.Tag = objFMaster._Field2Unkid
            cboFieldValue2.DrawMode = DrawMode.OwnerDrawFixed
            If objlblField2.Text = "" Then
                cboFieldValue2.Enabled = False : objbtnSearchField2.Enabled = False
            End If
            objlblField3.Text = objFMaster._Field3_Caption
            cboFieldValue3.Tag = objFMaster._Field3Unkid
            cboFieldValue3.DrawMode = DrawMode.OwnerDrawFixed
            If objlblField3.Text = "" Then
                cboFieldValue3.Enabled = False : objbtnSearchField3.Enabled = False
            End If
            objlblField4.Text = objFMaster._Field4_Caption
            cboFieldValue4.Tag = objFMaster._Field4Unkid
            cboFieldValue4.DrawMode = DrawMode.OwnerDrawFixed
            If objlblField4.Text = "" Then
                cboFieldValue4.Enabled = False : objbtnSearchField4.Enabled = False
            End If
            objlblField5.Text = objFMaster._Field5_Caption
            cboFieldValue5.Tag = objFMaster._Field5Unkid
            cboFieldValue5.DrawMode = DrawMode.OwnerDrawFixed
            If objlblField5.Text = "" Then
                cboFieldValue5.Enabled = False : objbtnSearchField5.Enabled = False
            End If

            If ConfigParameter._Object._CascadingTypeId = enPACascading.STRICT_CASCADING Then
                objdgcolhAdd.Visible = False : objdgcolhDelete.Visible = False
            End If
            mnuGlobalAssign.Visible = ConfigParameter._Object._FollowEmployeeHierarchy
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetFormViewSetting", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub FillCombo()
        Dim objMData As New clsMasterData
        Dim dsList As New DataSet
        Dim objPeriod As New clscommom_period_Tran
        Dim objEmp As New clsEmployee_Master
        Try
            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'dsList = objEmp.GetEmployeeList("List", True, True, , , , , , , , , , , , eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate))
            dsList = objEmp.GetEmployeeList(FinancialYear._Object._DatabaseName, _
                                            User._Object._Userunkid, _
                                            FinancialYear._Object._YearUnkid, _
                                            Company._Object._Companyunkid, _
                                            eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                            eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                            ConfigParameter._Object._UserAccessModeSetting, _
                                            True, False, "List", True)
            'S.SANDEEP [04 JUN 2015] -- END

            With cboEmployee
                .ValueMember = "employeeunkid"
                .DisplayMember = "employeename"
                .DataSource = dsList.Tables(0)
                .SelectedValue = 0
            End With

            'S.SANDEEP [ 05 NOV 2014 ] -- START
            'dsList = objMData.Get_BSC_Perspective("List", True)
            Dim objPerspective As New clsassess_perspective_master
            dsList = objPerspective.getComboList("List", True)
            objPerspective = Nothing
            'S.SANDEEP [ 05 NOV 2014 ] -- END
            With cboPerspective
                .ValueMember = "Id"
                .DisplayMember = "Name"
                .DataSource = dsList.Tables(0)
                .SelectedValue = 0
            End With

            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'dsList = objPeriod.getListForCombo(enModuleReference.Assessment, 0, "List", True, enStatusType.Open)   
            dsList = objPeriod.getListForCombo(enModuleReference.Assessment, 0, FinancialYear._Object._DatabaseName, FinancialYear._Object._Database_Start_Date, "List", True, enStatusType.Open)
            'Sohail (21 Aug 2015) -- End
            With cboPeriod
                .ValueMember = "periodunkid"
                .DisplayMember = "name"
                .DataSource = dsList.Tables(0)
            End With

            dsList = objMData.Get_CompanyGoal_Status("List", True)
            With cboStatus
                .ValueMember = "Id"
                .DisplayMember = "Name"
                .DataSource = dsList.Tables("List")
                .SelectedValue = 0
            End With

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub Fill_Grid()
        Dim dtFinal As DataTable
        Dim iSearch As String = String.Empty
        Try

            If objFMaster.IsPresent(clsAssess_Field_Master.enFieldCheckType.FIELD_DATA) = False Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 8, "Sorry, No data defined for asseessment caption settings screen."), enMsgBoxStyle.Information)
                Exit Sub
            End If

            Dim objMap As New clsAssess_Field_Mapping
            If objMap.isExist(CInt(cboPeriod.SelectedValue)) = False Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 9, "Sorry no field is mapped with the selected period."), enMsgBoxStyle.Information)
                Exit Sub
            End If
            objMap = Nothing


            'S.SANDEEP [28 MAY 2015] -- START
            'ENHANCEMENT : NEW PRIVILEGE (Performance Assessment)
            If User._Object.Privilege._AllowtoViewEmployeeGoalsList = False Then Exit Sub
            'S.SANDEEP [28 MAY 2015] -- END

            dtFinal = objEmpField1.GetDisplayList(CInt(cboEmployee.SelectedValue), CInt(cboPeriod.SelectedValue), "List")

            If cboPerspective.Enabled = True AndAlso CInt(cboPerspective.SelectedValue) > 0 Then
                iSearch &= "AND perspectiveunkid = '" & CInt(cboPerspective.SelectedValue) & "' "
            End If

            If cboFieldValue1.Enabled = True AndAlso CInt(cboFieldValue1.SelectedValue) > 0 Then
                iSearch &= "AND empfield1unkid = '" & CInt(cboFieldValue1.SelectedValue) & "' "
            End If

            If cboFieldValue2.Enabled = True AndAlso CInt(cboFieldValue2.SelectedValue) > 0 Then
                iSearch &= "AND empfield2unkid = '" & CInt(cboFieldValue2.SelectedValue) & "' "
            End If

            If cboFieldValue3.Enabled = True AndAlso CInt(cboFieldValue3.SelectedValue) > 0 Then
                iSearch &= "AND empfield3unkid = '" & CInt(cboFieldValue3.SelectedValue) & "' "
            End If

            If cboFieldValue4.Enabled = True AndAlso CInt(cboFieldValue4.SelectedValue) > 0 Then
                iSearch &= "AND empfield4unkid = '" & CInt(cboFieldValue4.SelectedValue) & "' "
            End If

            If cboFieldValue5.Enabled = True AndAlso CInt(cboFieldValue5.SelectedValue) > 0 Then
                iSearch &= "AND empfield5unkid = '" & CInt(cboFieldValue5.SelectedValue) & "' "
            End If

            If CInt(cboStatus.SelectedValue) > 0 Then
                iSearch &= "AND CStatusId = '" & CInt(cboStatus.SelectedValue) & "' "
            End If

            If iSearch.Trim.Length > 0 Then
                iSearch = iSearch.Substring(3)
                mdtFinal = New DataView(dtFinal, iSearch, "empfield1unkid ASC, Field1 ASC", DataViewRowState.CurrentRows).ToTable
            Else
                mdtFinal = New DataView(dtFinal, "", "empfield1unkid ASC, Field1 ASC", DataViewRowState.CurrentRows).ToTable
            End If

            If mdtFinal.Rows.Count <= 0 Then
                Dim iRow As DataRow = mdtFinal.NewRow
                mdtFinal.Rows.Add(iRow)
            End If

            dgvData.AutoGenerateColumns = False
            Dim iColName As String = String.Empty

            Dim iPlan() As String = Nothing
            If ConfigParameter._Object._ViewTitles_InPlanning.Trim.Length > 0 Then
                iPlan = ConfigParameter._Object._ViewTitles_InPlanning.Split("|")
            End If

            'S.SANDEEP [16 JUN 2015] -- START
            Dim dgvCol As DataGridViewColumn = Nothing
            'S.SANDEEP [16 JUN 2015] -- END


            'S.SANDEEP |18-JAN-2019| -- START
            strPerspectiveColName = mdtFinal.Columns.Cast(Of DataColumn)().Where(Function(x) CInt(x.ExtendedProperties(x.ColumnName)) = CInt(clsAssess_Field_Master.enOtherInfoField.PERSPECTIVE)).Select(Function(x) x.ColumnName).First()
            strGoalValueColName = mdtFinal.Columns.Cast(Of DataColumn)().Where(Function(x) CInt(x.ExtendedProperties(x.ColumnName)) = CInt(clsAssess_Field_Master.enOtherInfoField.GOAL_VALUE)).Select(Function(x) x.ColumnName).First()
            'S.SANDEEP |18-JAN-2019| -- END

            For Each dCol As DataColumn In mdtFinal.Columns
                iColName = "" : iColName = "obj" & dCol.ColumnName
                If dgvData.Columns.Contains(iColName) = True Then Continue For

                'S.SANDEEP [16 JUN 2015] -- START
                'Dim dgvCol As New DataGridViewTextBoxColumn()
                If dCol.ColumnName = "vuProgress" Then
                    dgvCol = New DataGridViewLinkColumn()
                    CType(dgvCol, DataGridViewLinkColumn).LinkBehavior = System.Windows.Forms.LinkBehavior.NeverUnderline
                Else
                    dgvCol = New DataGridViewTextBoxColumn()
                End If
                'S.SANDEEP [16 JUN 2015] -- END

                dgvCol.Name = iColName
                dgvCol.Width = 120

                'S.SANDEEP [24 SEP 2015] -- START
                'dgvCol.SortMode = DataGridViewColumnSortMode.NotSortable
                dgvCol.SortMode = DataGridViewColumnSortMode.Automatic
                'S.SANDEEP [24 SEP 2015] -- END
                dgvCol.ReadOnly = True
                dgvCol.DataPropertyName = dCol.ColumnName
                dgvCol.HeaderText = dCol.Caption
                If dCol.Caption.Length <= 0 Then
                    dgvCol.Visible = False
                Else
                    If mdtFinal.Columns(dCol.ColumnName).ExtendedProperties(dCol.ColumnName) IsNot Nothing Then
                        dgvCol.Width = objFMaster.GetColumnWidth(clsAssess_Field_Master.enColWidthType.COL_PLAN, mdtFinal.Columns(dCol.ColumnName).ExtendedProperties(dCol.ColumnName))
                        If iPlan IsNot Nothing Then
                            If Array.IndexOf(iPlan, mdtFinal.Columns(dCol.ColumnName).ExtendedProperties(dCol.ColumnName).ToString) < 0 Then
                                dgvCol.Visible = False
                            End If
                        End If
                    End If
                End If
                If dCol.ColumnName = "Emp" Then
                    dgvCol.Visible = False
                End If


                'Shani (26-Sep-2016) -- Start
                'Enhancement -Changes PA Flow And Add New screen Given By (Andrew)
                If dCol.ColumnName = "vuGoalAccomplishmentStatus" Then
                    dgvCol.Visible = False
                End If
                'Shani (26-Sep-2016) -- End



                'S.SANDEEP [02 JUL 2015] -- START
                'If ConfigParameter._Object._CascadingTypeId = enPACascading.NEITHER_CASCADING_NOR_ALIGNMENT Then
                '    If dCol.ColumnName.StartsWith("Owr") Then
                '        dgvCol.Visible = False
                '    End If
                'End If
                If dCol.ColumnName.StartsWith("Owr") Then
                    dgvCol.Visible = False
                End If
                'S.SANDEEP [02 JUL 2015] -- END

                dgvData.Columns.Add(dgvCol)

                'S.SANDEEP |18-JAN-2019| -- START
                If dCol.ColumnName = "OPeriod" Then
                    dgvCol.Visible = False
                End If
                If dgvCol.Visible = True Then
                    If strPerspectiveColName = dCol.ColumnName Then
                        mintPerspectiveColIndex = dgvCol.Index
                    End If
                End If

                If dgvCol.Visible = True Then
                    If strGoalValueColName = dCol.ColumnName Then
                        mintGoalValueColIndex = dgvCol.Index
                        dgvCol.DefaultCellStyle.Format = GUI.fmtCurrency
                    End If
                End If
                'S.SANDEEP |18-JAN-2019| -- END
            Next
            dgvData.DataSource = mdtFinal

            'S.SANDEEP [28 MAY 2015] -- START
            'ENHANCEMENT : NEW PRIVILEGE (Performance Assessment)
            Call SetVisibility()
            'S.SANDEEP [28 MAY 2015] -- END


            'S.SANDEEP [01 JUL 2015] -- START
            If CBool(dgvData.Rows(0).Cells("objisfinal").Value) = False Then
                dgvData.Columns("objvuRemark").Visible = False
                dgvData.Columns("objvuProgress").Visible = False
            End If
            RemoveHandler dgvData.CellContentClick, AddressOf dgvData_CellContentClick
            'S.SANDEEP [01 JUL 2015] -- END


            objdgcolhAdd.DefaultCellStyle.SelectionBackColor = Color.White
            objdgcolhAdd.DefaultCellStyle.SelectionForeColor = Color.White

            objdgcolhEdit.DefaultCellStyle.SelectionBackColor = Color.White
            objdgcolhEdit.DefaultCellStyle.SelectionForeColor = Color.White

            objdgcolhDelete.DefaultCellStyle.SelectionBackColor = Color.White
            objdgcolhDelete.DefaultCellStyle.SelectionForeColor = Color.White

            For Each dgvRow As DataGridViewRow In dgvData.Rows
                If CBool(dgvRow.Cells("objisfinal").Value) = True Then
                    objdgcolhAdd.Visible = False : objdgcolhDelete.Visible = False : objdgcolhEdit.Visible = False

                    'S.SANDEEP |10-AUG-2021| -- START
                    'dgvRow.DefaultCellStyle.ForeColor = Color.Blue
                    If ConfigParameter._Object._BSC_StatusColors.Keys.Count > 0 Then
                        If ConfigParameter._Object._BSC_StatusColors.ContainsKey(CInt(enObjective_Status.FINAL_SAVE)) Then
                            dgvRow.DefaultCellStyle.ForeColor = Color.FromArgb(CInt(ConfigParameter._Object._BSC_StatusColors(CInt(enObjective_Status.FINAL_SAVE))))
                        Else
                    dgvRow.DefaultCellStyle.ForeColor = Color.Blue
                        End If
                    Else
                    dgvRow.DefaultCellStyle.ForeColor = Color.Blue
                    End If
                    'S.SANDEEP |10-AUG-2021| -- END


                    'S.SANDEEP [01 JUL 2015] -- START
                    If dgvData.Columns("objvuRemark").Visible = False Then dgvData.Columns("objvuRemark").Visible = True
                    If dgvData.Columns("objvuProgress").Visible = False Then dgvData.Columns("objvuProgress").Visible = True
                    'S.SANDEEP [01 JUL 2015] -- END

                ElseIf CInt(dgvRow.Cells("objopstatusid").Value) = enObjective_Status.SUBMIT_APPROVAL Then
                    objdgcolhAdd.Visible = False : objdgcolhDelete.Visible = False : objdgcolhEdit.Visible = False
                    If ConfigParameter._Object._BSC_StatusColors.Keys.Count > 0 Then
                        If ConfigParameter._Object._BSC_StatusColors.ContainsKey(CInt(enObjective_Status.SUBMIT_APPROVAL)) Then
                            dgvRow.DefaultCellStyle.ForeColor = Color.FromArgb(CInt(ConfigParameter._Object._BSC_StatusColors(CInt(enObjective_Status.SUBMIT_APPROVAL))))
                        Else
                            dgvRow.DefaultCellStyle.ForeColor = Color.Green
                        End If
                    Else
                    dgvRow.DefaultCellStyle.ForeColor = Color.Green
                    End If
                Else
                    Dim cLr As Color
                    Select Case CInt(dgvRow.Cells("objopstatusid").Value)
                        Case enObjective_Status.OPEN_CHANGES
                            cLr = IIf(ConfigParameter._Object._BSC_StatusColors.ContainsKey(enObjective_Status.OPEN_CHANGES), Color.FromArgb(ConfigParameter._Object._BSC_StatusColors(enObjective_Status.OPEN_CHANGES)), Color.Black)
                        Case enObjective_Status.NOT_SUBMIT
                            cLr = IIf(ConfigParameter._Object._BSC_StatusColors.ContainsKey(enObjective_Status.NOT_SUBMIT), Color.FromArgb(ConfigParameter._Object._BSC_StatusColors(enObjective_Status.NOT_SUBMIT)), Color.Red)
                        Case enObjective_Status.NOT_COMMITTED
                            cLr = IIf(ConfigParameter._Object._BSC_StatusColors.ContainsKey(enObjective_Status.NOT_COMMITTED), Color.FromArgb(ConfigParameter._Object._BSC_StatusColors(enObjective_Status.NOT_COMMITTED)), Color.Purple)
                        Case enObjective_Status.FINAL_COMMITTED
                            cLr = IIf(ConfigParameter._Object._BSC_StatusColors.ContainsKey(enObjective_Status.FINAL_COMMITTED), Color.FromArgb(ConfigParameter._Object._BSC_StatusColors(enObjective_Status.FINAL_COMMITTED)), Color.Blue)
                        Case enObjective_Status.PERIODIC_REVIEW
                            cLr = IIf(ConfigParameter._Object._BSC_StatusColors.ContainsKey(enObjective_Status.PERIODIC_REVIEW), Color.FromArgb(ConfigParameter._Object._BSC_StatusColors(enObjective_Status.PERIODIC_REVIEW)), Color.Brown)
                    End Select
                    dgvRow.DefaultCellStyle.ForeColor = cLr
                    'S.SANDEEP [16 JUN 2015] -- START
                    'objdgcolhAdd.Visible = True : objdgcolhDelete.Visible = True : objdgcolhEdit.Visible = True

                    objdgcolhAdd.Visible = User._Object.Privilege._AllowtoAddEmployeeGoals
                    objdgcolhEdit.Visible = User._Object.Privilege._AllowtoEditEmployeeGoals
                    objdgcolhDelete.Visible = User._Object.Privilege._AllowtoDeleteEmployeeGoals
                    'S.SANDEEP [16 JUN 2015] -- END
                End If
                'S.SANDEEP [16 JUN 2015] -- START
                dgvRow.Cells("objvuProgress").Style.SelectionForeColor = Color.White
                dgvRow.Cells("objvuProgress").Style.SelectionBackColor = Color.White
                'S.SANDEEP [16 JUN 2015] -- END

                'S.SANDEEP |12-FEB-2019| -- START
                'ISSUE/ENHANCEMENT : {Performance Assessment Changes}
                If mintGoalValueColIndex <> -1 Then dgvRow.Cells(mintGoalValueColIndex).Value = CStr(Format(CDbl(dgvRow.Cells(mintGoalValueColIndex).Value), GUI.fmtCurrency)) & " " & dgvRow.Cells("objUoMType").Value.ToString()
                'S.SANDEEP |12-FEB-2019| -- END
            Next

            'S.SANDEEP [01 JUL 2015] -- START
            If dgvData.Columns("objvuProgress").Visible = True Then
                AddHandler dgvData.CellContentClick, AddressOf dgvData_CellContentClick
            End If
            'S.SANDEEP [01 JUL 2015] -- END

            If ConfigParameter._Object._CascadingTypeId = enPACascading.STRICT_CASCADING Then
                objdgcolhAdd.Visible = False : objdgcolhDelete.Visible = False
            End If

            Dim objFMapping As New clsAssess_Field_Mapping
            Dim xTotalWeight As Double = 0
            xTotalWeight = objFMapping.GetTotalWeight(clsAssess_Field_Mapping.enWeightCheckType.CKT_EMPLOYEE_LEVEL, CInt(cboPeriod.SelectedValue), 0, CInt(cboEmployee.SelectedValue))
            If xTotalWeight > 0 Then
                objlblTotalWeight.Text = Language.getMessage(mstrModuleName, 29, "Total Weight Assigned :") & " " & xTotalWeight.ToString
            Else
                objlblTotalWeight.Text = ""
            End If
            objFMapping = Nothing

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Fill_Grid", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub Generate_MenuItems()
        Dim dsList As New DataSet
        Try
            Dim objFMaster As New clsAssess_Field_Master
            dsList = objFMaster.Get_Field_Mapping("List", , True)
            If dsList.Tables(0).Rows.Count > 0 Then
                cmnuAdd.Items.Clear() : cmnuEdit.Items.Clear() : cmnuDelete.Items.Clear()
                For i As Integer = 0 To dsList.Tables(0).Rows.Count - 1
                    Dim objbtnAdd As New ToolStripMenuItem : Dim objbtnEdt As New ToolStripMenuItem : Dim objbtnDel As New ToolStripMenuItem

                    objbtnAdd.Name = "objbtnAdd" & dsList.Tables(0).Rows(i).Item("fieldunkid").ToString
                    objbtnAdd.Text = Language.getMessage(mstrModuleName, 26, "Add") & " " & dsList.Tables(0).Rows(i)("fieldcaption").ToString
                    objbtnAdd.Tag = dsList.Tables(0).Rows(i)("fieldunkid").ToString & "|" & dsList.Tables(0).Rows(i)("ExOrder").ToString
                    cmnuAdd.Items.Add(objbtnAdd)
                    AddHandler objbtnAdd.Click, AddressOf objbtnAdd_Click

                    objbtnEdt.Name = "objbtnEdt" & dsList.Tables(0).Rows(i).Item("fieldunkid").ToString
                    objbtnEdt.Text = Language.getMessage(mstrModuleName, 27, "Edit") & " " & dsList.Tables(0).Rows(i)("fieldcaption").ToString
                    objbtnEdt.Tag = dsList.Tables(0).Rows(i)("fieldunkid").ToString & "|" & dsList.Tables(0).Rows(i)("ExOrder").ToString
                    cmnuEdit.Items.Add(objbtnEdt)
                    AddHandler objbtnEdt.Click, AddressOf objbtnEdt_Click

                    objbtnDel.Name = "objbtnDel" & dsList.Tables(0).Rows(i).Item("fieldunkid").ToString
                    objbtnDel.Text = Language.getMessage(mstrModuleName, 28, "Delete") & " " & dsList.Tables(0).Rows(i)("fieldcaption").ToString
                    objbtnDel.Tag = dsList.Tables(0).Rows(i)("fieldunkid").ToString & "|" & dsList.Tables(0).Rows(i)("ExOrder").ToString
                    cmnuDelete.Items.Add(objbtnDel)
                    AddHandler objbtnDel.Click, AddressOf objbtnDel_Click
                Next
            End If
            objFMaster = Nothing
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Generate_MenuItems", mstrModuleName)
        Finally
        End Try
    End Sub

    'S.SANDEEP [28 MAY 2015] -- START
    'ENHANCEMENT : NEW PRIVILEGE (Performance Assessment)
    Private Sub SetVisibility()
        Try
            objdgcolhAdd.Visible = User._Object.Privilege._AllowtoAddEmployeeGoals
            objdgcolhEdit.Visible = User._Object.Privilege._AllowtoEditEmployeeGoals
            objdgcolhDelete.Visible = User._Object.Privilege._AllowtoDeleteEmployeeGoals
            mnuGlobalAssign.Enabled = User._Object.Privilege._AllowtoPerformGlobalAssignEmployeeGoals

            'Shani(06-Feb-2016) -- Start
            'PA Changes Given By Glory for CCBRT
            mnuVoidGoals.Enabled = User._Object.Privilege._AllowToVoidEmployeeGoals
            'Shani(06-Feb-2016) -- End

            'S.SANDEEP [16 JUN 2015] -- START
            'mnuUpdatePercentage.Enabled = User._Object.Privilege._AllowtoUpdatePercentCompletedEmployeeGoals
            If dgvData.ColumnCount > 0 Then
                If dgvData.Columns.Contains("objvuProgress") Then
                    dgvData.Columns("objvuProgress").Visible = User._Object.Privilege._AllowtoUpdatePercentCompletedEmployeeGoals
                End If
            End If
            'S.SANDEEP [16 JUN 2015] -- END

            'Shani (26-Sep-2016) -- Start
            'Enhancement -Changes PA Flow And Add New screen Given By (Andrew)
            If ConfigParameter._Object._GoalsAccomplishedRequiresApproval = True Then
                mnuAppRejGoalAccomplished.Visible = User._Object.Privilege._AllowToApproveGoalsAccomplishment
            Else
                mnuAppRejGoalAccomplished.Visible = False
            End If
            'Shani (26-Sep-2016) -- End

            'S.SANDEEP [07-OCT-2017] -- START
            'ISSUE/ENHANCEMENT : IMPORT GOALS
            If ConfigParameter._Object._CascadingTypeId = enPACascading.NEITHER_CASCADING_NOR_ALIGNMENT Then
                mnuImportGoals.Visible = True
            Else
                mnuImportGoals.Visible = False
            End If
            'S.SANDEEP [07-OCT-2017] -- END


            'Varsha Rana (17-Oct-2017) -- Start
            'Enhancement - Give user privileges.
            mnuGlobalOperation.Enabled = User._Object.Privilege._AllowToPerformGlobalGoalOperations
            mnuGlobalVoidGoals.Enabled = User._Object.Privilege._AllowToPerformGlobalVoidGoals
            btnSubmitForGoalAccomplished.Enabled = User._Object.Privilege._AllowToSubmitForGoalAccomplished
            mnuImportGoals.Enabled = User._Object.Privilege._AllowToImportEmployeeGoals
            'Varsha Rana (17-Oct-2017) -- End


        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetVisibility", mstrModuleName)
        Finally
        End Try
    End Sub
    'S.SANDEEP [28 MAY 2015] -- END

    'S.SANDEEP |08-JAN-2019| -- START
    Private Function LockEmployee() As Boolean
        Dim objLockEmp As New clsassess_plan_eval_lockunlock
        Try
            Dim mintLockId As Integer = 0
            Dim mblnisLock As Boolean = False
            Dim mdtNextLockDate As Date = Nothing
            If objLockEmp.isExist(CInt(cboEmployee.SelectedValue), CInt(cboPeriod.SelectedValue), clsassess_plan_eval_lockunlock.enAssementLockType.PLANNING, "", mblnisLock, mintLockId, mdtNextLockDate) Then
                If mintLockId > 0 And mblnisLock = True Then Return True
            End If

            If objLockEmp.isValidDate(CInt(cboEmployee.SelectedValue), CInt(cboPeriod.SelectedValue), clsassess_plan_eval_lockunlock.enAssementLockType.PLANNING, ConfigParameter._Object._CurrentDateAndTime) = True Then
                Return False
            End If
            objLockEmp._AssessorMasterunkid = 0
            objLockEmp._Assessmodeid = 0
            objLockEmp._Auditdatetime = ConfigParameter._Object._CurrentDateAndTime
            objLockEmp._Audittype = enAuditType.ADD
            objLockEmp._Audituserunkid = User._Object._Userunkid
            objLockEmp._Employeeunkid = CInt(cboEmployee.SelectedValue)
            objLockEmp._Form_Name = mstrModuleName
            objLockEmp._Hostname = getHostName()
            objLockEmp._Ip = getIP()
            objLockEmp._Islock = True
            objLockEmp._Isunlocktenure = False
            objLockEmp._Isweb = False
            objLockEmp._Locktypeid = clsassess_plan_eval_lockunlock.enAssementLockType.PLANNING
            objLockEmp._Lockunlockdatetime = ConfigParameter._Object._CurrentDateAndTime
            objLockEmp._Lockuserunkid = User._Object._Userunkid
            objLockEmp._Loginemployeeunkid = 0
            objLockEmp._Nextlockdatetime = Nothing
            objLockEmp._Periodunkid = CInt(cboPeriod.SelectedValue)
            objLockEmp._Unlockdays = 0
            objLockEmp._Unlockreason = ""
            objLockEmp._Unlockuserunkid = 0
            If objLockEmp.Insert() = False Then
                If objLockEmp._Message.Trim.Length > 0 Then
                    eZeeMsgBox.Show(objLockEmp._Message, enMsgBoxStyle.Information)
                End If
            End If
            Return True
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "LockEmployee", mstrModuleName)
        Finally
            objLockEmp = Nothing
        End Try
    End Function
    'S.SANDEEP |08-JAN-2019| -- END

    'S.SANDEEP |18-JAN-2019| -- START
    Private Function IsTheSameCellValue(ByVal column As Integer, ByVal row As Integer) As Boolean
        Try
            Dim cell1 As DataGridViewCell = dgvData(column, row)
            Dim cell2 As DataGridViewCell = dgvData(column, row - 1)
            If cell1.Value Is Nothing OrElse cell2.Value Is Nothing Then
                Return False
            End If
            Return cell1.Value.ToString() = cell2.Value.ToString()
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: IsTheSameCellValue; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Function
    'S.SANDEEP |18-JAN-2019| -- END

#End Region

#Region " Form's Event "

    Private Sub frmempFieldsList_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        objEmpField1 = New clsassess_empfield1_master : objEmpField2 = New clsassess_empfield2_master
        objEmpField3 = New clsassess_empfield3_master : objEmpField4 = New clsassess_empfield4_master
        objEmpField5 = New clsassess_empfield5_master
        cmnuAdd = New ContextMenuStrip() : cmnuEdit = New ContextMenuStrip() : cmnuDelete = New ContextMenuStrip()
        Try
            Call Set_Logo(Me, gApplicationType)
            Language.setLanguage(Me.Name)
            Call OtherSettings()
            Call Generate_MenuItems()
            Call SetFormViewSetting()
            Call FillCombo()
            'S.SANDEEP [28 MAY 2015] -- START
            'ENHANCEMENT : NEW PRIVILEGE (Performance Assessment)
            Call SetVisibility()
            'S.SANDEEP [28 MAY 2015] -- END

            If ConfigParameter._Object._BSC_StatusColors.Keys.Count > 0 Then
                If ConfigParameter._Object._BSC_StatusColors.ContainsKey(CInt(enObjective_Status.FINAL_SAVE)) Then
                    objpnl2.BackColor = Color.FromArgb(CInt(ConfigParameter._Object._BSC_StatusColors(CInt(enObjective_Status.FINAL_SAVE))))
                Else
                    objpnl2.BackColor = Color.Blue
                End If
            Else
                objpnl2.BackColor = Color.Blue
            End If

            If ConfigParameter._Object._BSC_StatusColors.Keys.Count > 0 Then
                If ConfigParameter._Object._BSC_StatusColors.ContainsKey(CInt(enObjective_Status.SUBMIT_APPROVAL)) Then
                    objpnl1.BackColor = Color.FromArgb(CInt(ConfigParameter._Object._BSC_StatusColors(CInt(enObjective_Status.SUBMIT_APPROVAL))))
                Else
                    objpnl1.BackColor = Color.Green
                End If
            Else
                objpnl1.BackColor = Color.Green
            End If


        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmempFieldsList_Load", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsassess_empfield1_master.SetMessages()
            clsassess_empfield2_master.SetMessages()
            clsassess_empfield3_master.SetMessages()
            clsassess_empfield4_master.SetMessages()
            clsassess_empfield5_master.SetMessages()
            objfrm._Other_ModuleNames = "clsassess_empfield1_master,clsassess_empfield2_master,clsassess_empfield3_master,clsassess_empfield4_master,clsassess_empfield5_master"
            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

#Region " Button's Events "

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Me.Close()
    End Sub

    Private Sub objbtnSearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearch.Click
        Try
            If CInt(cboEmployee.SelectedValue) <= 0 Or _
                   CInt(cboPeriod.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 10, "Employee and Period are mandatory information. Please select Employee and Period to continue."), enMsgBoxStyle.Information)
                Exit Sub
            End If
            Call Fill_Grid()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearch_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub objbtnReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnReset.Click
        Try
            cboPeriod.SelectedValue = 0
            cboPerspective.SelectedValue = 0
            cboStatus.SelectedValue = 0
            cboFieldValue1.SelectedValue = 0
            cboFieldValue2.SelectedValue = 0
            cboFieldValue3.SelectedValue = 0
            cboFieldValue4.SelectedValue = 0
            cboFieldValue5.SelectedValue = 0
            dgvData.DataSource = Nothing
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnReset_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub objbtnSearchField1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchField1.Click, _
                                                                                                             objbtnSearchField2.Click, _
                                                                                                             objbtnSearchField3.Click, _
                                                                                                             objbtnSearchField4.Click, _
                                                                                                             objbtnSearchField5.Click, _
                                                                                                             objbtnSearchEmp.Click
        Dim frm As New frmCommonSearch
        Try
            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If

            Dim iCbo As ComboBox = Nothing
            Select Case CType(sender, eZee.Common.eZeeGradientButton).Name.ToUpper
                Case objbtnSearchField1.Name.ToUpper
                    iCbo = cboFieldValue1
                Case objbtnSearchField2.Name.ToUpper
                    iCbo = cboFieldValue2
                Case objbtnSearchField3.Name.ToUpper
                    iCbo = cboFieldValue3
                Case objbtnSearchField4.Name.ToUpper
                    iCbo = cboFieldValue4
                Case objbtnSearchField5.Name.ToUpper
                    iCbo = cboFieldValue5
                Case objbtnSearchEmp.Name.ToUpper
                    iCbo = cboEmployee
            End Select
            With frm
                .ValueMember = iCbo.ValueMember
                .DisplayMember = iCbo.DisplayMember
                If iCbo.Name = cboEmployee.Name Then
                    .CodeMember = "employeecode"
                End If
                .DataSource = iCbo.DataSource
                If .DisplayDialog = True Then
                    iCbo.SelectedValue = .SelectedValue
                End If
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchField1_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    'Shani(24-JAN-2017) -- Start
    'Enhancement : 
    Private Sub btnSubmitForGoalAccomplished_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSubmitForGoalAccomplished.Click
        Try
            If CInt(cboEmployee.SelectedValue) <= 0 Or CInt(cboPeriod.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 15, "Sorry, Employee and Period are mandatory information to performe Add Operation."), enMsgBoxStyle.Information)
                Exit Sub
            End If

            Dim dsFinal As DataSet = (New clsassess_empfield1_master).GetAccomplishemtn_DisplayList(CInt(cboEmployee.SelectedValue), _
                                                                                                      CInt(cboPeriod.SelectedValue), _
                                                                                                      0, , _
                                                                                                      "List")

            If dsFinal Is Nothing OrElse dsFinal.Tables(0).Select("Accomplished_statusId = " & clsassess_empupdate_tran.enGoalAccomplished_Status.GA_Pending & " AND IsGrp = FALSE").Length <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 32, "Sorry, Pendding data not found in system."), enMsgBoxStyle.Information)
                Exit Sub
            End If

            Dim objEUpdateProgress As New clsassess_empupdate_tran
            With objEUpdateProgress
                ._FormName = mstrModuleName
                ._LoginEmployeeunkid = 0
                ._ClientIP = getIP()
                ._HostName = getHostName()
                ._FromWeb = False
                ._AuditUserId = User._Object._Userunkid
._CompanyUnkid = Company._Object._Companyunkid
                ._AuditDate = ConfigParameter._Object._CurrentDateAndTime
            End With

            objEUpdateProgress.Send_Notification_Assessor(CInt(cboEmployee.SelectedValue), _
                                                          CInt(cboPeriod.SelectedValue), _
                                                          FinancialYear._Object._YearUnkid, _
                                                          FinancialYear._Object._FinancialYear_Name, _
                                                          FinancialYear._Object._DatabaseName, objEUpdateProgress._Empupdatetranunkid, _
                                                          Company._Object._Companyunkid, _
                                                          ConfigParameter._Object._ArutiSelfServiceURL, _
                                                          enLogin_Mode.DESKTOP, 0, User._Object._Userunkid)
            'S.SANDEEP |25-MAR-2019| -- START
            'eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 33, "Mail send Sucussfully."))
            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 36, "Progress Update have been submitted for approval."), enMsgBoxStyle.Information)
            'S.SANDEEP |25-MAR-2019| -- END

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnSubmitForGoalAccomplished_Click", mstrModuleName)
        End Try
    End Sub
    'Shani(24-JAN-2017) -- End

    'S.SANDEEP [27-MAR-2017] -- START
    'ISSUE/ENHANCEMENT : PROVIDED SEARCHING, AS PACRA HAVING MANY PERSPECTIVES
    Private Sub objbtnSearchPerspective_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchPerspective.Click
        Dim frm As New frmCommonSearch
        Try
            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If
            With frm
                .ValueMember = cboPerspective.ValueMember
                .DisplayMember = cboPerspective.DisplayMember
                .DataSource = CType(cboPerspective.DataSource, DataTable)
                If .DisplayDialog Then
                    cboPerspective.SelectedValue = .SelectedValue
                    cboPerspective.Focus()
                End If
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchPerspective_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub
    'S.SANDEEP [27-MAR-2017] -- END

#End Region

#Region " Controls Events "

    Private Sub cboEmployee_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboEmployee.SelectedIndexChanged, cboPeriod.SelectedIndexChanged
        Try
            Dim dsList As New DataSet

            dsList = objEmpField1.getComboList(CInt(cboEmployee.SelectedValue), CInt(cboPeriod.SelectedValue), "List", True)
            With cboFieldValue1
                .ValueMember = "Id"
                .DisplayMember = "Name"
                .DataSource = dsList.Tables("List")
                .SelectedValue = 0
            End With
            'S.SANDEEP |08-JAN-2019| -- START
            If CInt(cboEmployee.SelectedValue) > 0 AndAlso CInt(cboPeriod.SelectedValue) > 0 Then
                Call Fill_Grid()
            End If
            'S.SANDEEP |08-JAN-2019| -- END
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboEmployee_SelectedIndexChanged", mstrModuleName)
        Finally
        End Try
    End Sub

    'S.SANDEEP [16 JUN 2015] -- START
    Private Sub dgvData_CellContentClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvData.CellContentClick
        Try
            'S.SANDEEP |16-AUG-2019| -- START
            'ISSUE/ENHANCEMENT : ZRA {Language & Custom Item} & NMB Email Notification {Calibration}
            If e.RowIndex <= -1 Then Exit Sub
            'S.SANDEEP |16-AUG-2019| -- END

            If dgvData.SelectedRows.Count > 0 Then
                If dgvData.Rows(e.RowIndex).Cells(e.ColumnIndex).ColumnIndex = dgvData.Rows(e.RowIndex).Cells("objvuProgress").ColumnIndex Then

                    'Shani (26-Sep-2016) -- Start
                    'Enhancement -Changes PA Flow And Add New screen Given By (Andrew)
                    Dim objAnalysis_Master As New clsevaluation_analysis_master
                    If objAnalysis_Master.isExist(enAssessmentMode.SELF_ASSESSMENT, CInt(cboEmployee.SelectedValue), CInt(cboPeriod.SelectedValue)) = True Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 31, "Sorry, you cannot unlock Performance Planning for the selected period. Reason : Assessment is already done for the selected period"), enMsgBoxStyle.Information)
                        Exit Sub
                    End If

                    If objAnalysis_Master.isExist(enAssessmentMode.APPRAISER_ASSESSMENT, CInt(cboEmployee.SelectedValue), CInt(cboPeriod.SelectedValue)) = True Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 31, "Sorry, you cannot unlock Performance Planning for the selected period. Reason : Assessment is already done for the selected period"), enMsgBoxStyle.Information)
                        Exit Sub
                    End If

                    If objAnalysis_Master.isExist(enAssessmentMode.REVIEWER_ASSESSMENT, CInt(cboEmployee.SelectedValue), CInt(cboPeriod.SelectedValue)) = True Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 31, "Sorry, you cannot unlock Performance Planning for the selected period. Reason : Assessment is already done for the selected period"), enMsgBoxStyle.Information)
                        Exit Sub
                    End If
                    objAnalysis_Master = Nothing
                    'Shani (26-Sep-2016) -- End

                    Dim iLinkedFieldId As Integer = 0
                    Dim iColName As String = ""
                    Dim iExOrder As Integer = 0
                    Dim objFMapping As New clsAssess_Field_Mapping
                    iLinkedFieldId = objFMapping.Get_Map_FieldId(CInt(cboPeriod.SelectedValue))
                    objFMapping = Nothing
                    If iLinkedFieldId <= 0 Then Exit Sub
                    Dim objFMaster As New clsAssess_Field_Master
                    iExOrder = objFMaster.Get_Field_ExOrder(iLinkedFieldId)
                    Select Case iExOrder
                        Case 1
                            iColName = "objempfield1unkid"
                        Case 2
                            iColName = "objempfield2unkid"
                        Case 3
                            iColName = "objempfield3unkid"
                        Case 4
                            iColName = "objempfield4unkid"
                        Case 5
                            iColName = "objempfield5unkid"
                    End Select


                    If dgvData.Rows(e.RowIndex).Cells("objField" & iLinkedFieldId.ToString).Value = "" Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Sorry, there is no") & _
                        " " & dgvData.Columns(dgvData.CurrentRow.Cells("objField" & iLinkedFieldId.ToString).ColumnIndex).HeaderText & " " & _
                        Language.getMessage(mstrModuleName, 2, "defined. Please define") & _
                        " " & dgvData.Columns(dgvData.CurrentRow.Cells("objField" & iLinkedFieldId.ToString).ColumnIndex).HeaderText & " " & _
                        Language.getMessage(mstrModuleName, 30, "in order to perform update progress operation."), enMsgBoxStyle.Information)
                        Exit Sub
                    Else
                        Dim frm As New frmUpdateFieldValue

                        If User._Object._Isrighttoleft = True Then
                            frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                            frm.RightToLeftLayout = True
                            Call Language.ctlRightToLeftlayOut(frm)
                        End If

                        If frm.displayDialog(iLinkedFieldId, _
                                             CInt(cboEmployee.SelectedValue), _
                                             CInt(cboPeriod.SelectedValue), _
                                             cboEmployee.Text, cboPeriod.Text, _
                                             CInt(dgvData.Rows(e.RowIndex).Cells(iColName).Value), _
                                             clsAssess_Field_Mapping.enWeightCheckType.CKT_EMPLOYEE_LEVEL, _
                                             dgvData.Rows(e.RowIndex).Cells("objField" & iLinkedFieldId.ToString).Value, _
                                             CInt(dgvData.Rows(e.RowIndex).Cells("objgoaltypeid").Value), _
                                             CDec(dgvData.Rows(e.RowIndex).Cells("objgoalvalue").Value), _
                                             dgvData.Rows(e.RowIndex).Cells("objUoMType").Value.ToString()) Then 'S.SANDEEP |18-JAN-2019| -- START {goaltypeid,goalvalue} -- END
                            'S.SANDEEP |12-FEB-2019| -- START {uomtypeid} -- END
                            Call Fill_Grid()
                        End If

                    End If
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgvData_CellContentClick", mstrModuleName)
        Finally
        End Try
    End Sub
    'S.SANDEEP [16 JUN 2015] -- END

    Private Sub dgvData_CellFormatting(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellFormattingEventArgs) Handles dgvData.CellFormatting
        Try
            If e.RowIndex <= -1 Then Exit Sub
            Select Case e.ColumnIndex
                Case objdgcolhAdd.Index
                    dgvData.Rows(e.RowIndex).Cells(e.ColumnIndex).ToolTipText = Language.getMessage(mstrModuleName, 5, "Add")
                Case objdgcolhEdit.Index
                    dgvData.Rows(e.RowIndex).Cells(e.ColumnIndex).ToolTipText = Language.getMessage(mstrModuleName, 6, "Edit")
                Case objdgcolhDelete.Index
                    dgvData.Rows(e.RowIndex).Cells(e.ColumnIndex).ToolTipText = Language.getMessage(mstrModuleName, 7, "Delete")
            End Select

            'S.SANDEEP |18-JAN-2019| -- START
            If mintPerspectiveColIndex > 0 Then
                If e.ColumnIndex = mintPerspectiveColIndex Then
                    If e.RowIndex <= 0 Then Return
                    If IsTheSameCellValue(e.ColumnIndex, e.RowIndex) Then
                        e.Value = ""
                        e.FormattingApplied = True
                    End If
                End If
            End If
            'S.SANDEEP |18-JAN-2019| -- END
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgvData_CellFormatting", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub dgvData_CellMouseClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellMouseEventArgs) Handles dgvData.CellMouseClick
        Try
            If e.RowIndex <= -1 Then Exit Sub
            If e.Button = Windows.Forms.MouseButtons.Left Then
                If e.ColumnIndex < 3 Then
                    'S.SANDEEP |08-DEC-2020| -- START
                    'ISSUE/ENHANCEMENT : NMB CASCADING CHANGES
                    Select Case ConfigParameter._Object._CascadingTypeId
                        Case enPACascading.LOOSE_CASCADING, enPACascading.STRICT_CASCADING
                            If ConfigParameter._Object._FollowEmployeeHierarchy Then
                                Dim StrMsg As String = ""
                                StrMsg = (New clsassess_empfield1_master).IsValidGoalOperation(CInt(cboPeriod.SelectedValue), eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), CInt(cboEmployee.SelectedValue), FinancialYear._Object._DatabaseName)
                                If StrMsg.Trim.Length > 0 Then
                                    eZeeMsgBox.Show(StrMsg, enMsgBoxStyle.Information)
                                    Exit Sub
                                End If
                            End If
                    End Select
                    'S.SANDEEP |08-DEC-2020| -- END

                    Select Case e.ColumnIndex
                        Case dgvData.Rows(e.RowIndex).Cells(objdgcolhAdd.Index).ColumnIndex
                            dgvData.CurrentCell = dgvData.Rows(e.RowIndex).Cells(e.ColumnIndex)
                            cmnuAdd.Show(dgvData, dgvData.PointToClient(Windows.Forms.Cursor.Position))
                        Case dgvData.Rows(e.RowIndex).Cells(objdgcolhEdit.Index).ColumnIndex
                            dgvData.CurrentCell = dgvData.Rows(e.RowIndex).Cells(e.ColumnIndex)
                            cmnuEdit.Show(dgvData, dgvData.PointToClient(Windows.Forms.Cursor.Position))
                        Case dgvData.Rows(e.RowIndex).Cells(objdgcolhDelete.Index).ColumnIndex
                            dgvData.CurrentCell = dgvData.Rows(e.RowIndex).Cells(e.ColumnIndex)
                            cmnuDelete.Show(dgvData, dgvData.PointToClient(Windows.Forms.Cursor.Position))
                    End Select
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgvData_CellMouseClick", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub objbtnDel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Dim frm As New frmReasonSelection
        Try
            Dim iVoidReason As String = String.Empty

            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If

            'S.SANDEEP |27-NOV-2020| -- START
            'ISSUE/ENHANCEMENT : VALID GOAL OPERATION
            'Dim xMsg As String = ""
            'xMsg = (New clsassess_empfield1_master).IsGoalsAssignedSubEmployee(CInt(cboPeriod.SelectedValue), CInt(cboEmployee.SelectedValue))
            'If xMsg.Trim.Length > 0 Then
            '    eZeeMsgBox.Show(xMsg, enMsgBoxStyle.Information)
            '    Exit Sub
            'End If
            'S.SANDEEP |27-NOV-2020| -- END

            Dim iMsg As String = Language.getMessage(mstrModuleName, 19, "You are about to remove the information from employee level, this will delete all child linked to this parent") & vbCrLf & Language.getMessage(mstrModuleName, 20, "Are you sure you want to delete?")

            If eZeeMsgBox.Show(iMsg, enMsgBoxStyle.Information + enMsgBoxStyle.YesNo) = Windows.Forms.DialogResult.No Then Exit Sub

            Select Case CInt(sender.tag.ToString.Split("|")(1))
                Case 1
                    If CInt(dgvData.CurrentRow.Cells("objempfield1unkid").Value) > 0 Then
                        frm.displayDialog(enVoidCategoryType.ASSESSMENT, iVoidReason)
                        If iVoidReason.Length <= 0 Then
                            Exit Sub
                        End If
                        objEmpField1._Isvoid = True
                        objEmpField1._Voiddatetime = ConfigParameter._Object._CurrentDateAndTime
                        objEmpField1._Voidreason = iVoidReason
                        objEmpField1._Voiduserunkid = User._Object._Userunkid

                        'S.SANDEEP [28-May-2018] -- START
                        'ISSUE/ENHANCEMENT : {Audit Trails} 
                        objEmpField1._FormName = mstrModuleName
                        objEmpField1._Loginemployeeunkid = 0
                        objEmpField1._ClientIP = getIP()
                        objEmpField1._HostName = getHostName()
                        objEmpField1._FromWeb = False
                        objEmpField1._AuditUserId = User._Object._Userunkid
objEmpField1._CompanyUnkid = Company._Object._Companyunkid
                        objEmpField1._AuditDate = ConfigParameter._Object._CurrentDateAndTime
                        'S.SANDEEP [28-May-2018] -- END

                        If objEmpField1.Delete(CInt(dgvData.CurrentRow.Cells("objempfield1unkid").Value)) = True Then
                            Call Fill_Grid()
                        Else
                            If objEmpField1._Message <> "" Then
                                eZeeMsgBox.Show(objEmpField1._Message, enMsgBoxStyle.Information)
                                Exit Sub
                            End If
                        End If
                    Else
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Sorry, there is no") & _
                                         " " & dgvData.Columns(dgvData.CurrentRow.Cells("objField1").ColumnIndex).HeaderText & " " & _
                                        Language.getMessage(mstrModuleName, 2, "defined. Please define") & _
                                         " " & dgvData.Columns(dgvData.CurrentRow.Cells("objField1").ColumnIndex).HeaderText & " " & _
                                        Language.getMessage(mstrModuleName, 4, "in order to perform delete operation."), enMsgBoxStyle.Information)
                        Exit Sub
                    End If
                Case 2
                    If CInt(dgvData.CurrentRow.Cells("objempfield2unkid").Value) > 0 Then
                        frm.displayDialog(enVoidCategoryType.ASSESSMENT, iVoidReason)
                        If iVoidReason.Length <= 0 Then
                            Exit Sub
                        End If
                        objEmpField2._Isvoid = True
                        objEmpField2._Voiddatetime = ConfigParameter._Object._CurrentDateAndTime
                        objEmpField2._Voidreason = iVoidReason
                        objEmpField2._Voiduserunkid = User._Object._Userunkid
                        With objEmpField2
                            ._FormName = mstrModuleName
                            ._Loginemployeeunkid = 0
                            ._ClientIP = getIP()
                            ._HostName = getHostName()
                            ._FromWeb = False
                            ._AuditUserId = User._Object._Userunkid
._CompanyUnkid = Company._Object._Companyunkid
                            ._AuditDate = ConfigParameter._Object._CurrentDateAndTime
                        End With
                        If objEmpField2.Delete(CInt(dgvData.CurrentRow.Cells("objempfield2unkid").Value)) = True Then
                            Call Fill_Grid()
                        End If
                    Else
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Sorry, there is no") & _
                                         " " & dgvData.Columns(dgvData.CurrentRow.Cells("objField2").ColumnIndex).HeaderText & " " & _
                                        Language.getMessage(mstrModuleName, 2, "defined. Please define") & _
                                         " " & dgvData.Columns(dgvData.CurrentRow.Cells("objField2").ColumnIndex).HeaderText & " " & _
                                        Language.getMessage(mstrModuleName, 4, "in order to perform delete operation."), enMsgBoxStyle.Information)
                        Exit Sub
                    End If
                Case 3
                    If CInt(dgvData.CurrentRow.Cells("objempfield3unkid").Value) > 0 Then
                        frm.displayDialog(enVoidCategoryType.ASSESSMENT, iVoidReason)
                        If iVoidReason.Length <= 0 Then
                            Exit Sub
                        End If
                        objEmpField3._Isvoid = True
                        objEmpField3._Voiddatetime = ConfigParameter._Object._CurrentDateAndTime
                        objEmpField3._Voidreason = iVoidReason
                        objEmpField3._Voiduserunkid = User._Object._Userunkid
                        With objEmpField3
                            ._FormName = mstrModuleName
                            ._Loginemployeeunkid = 0
                            ._ClientIP = getIP()
                            ._HostName = getHostName()
                            ._FromWeb = False
                            ._AuditUserId = User._Object._Userunkid
._CompanyUnkid = Company._Object._Companyunkid
                            ._AuditDate = ConfigParameter._Object._CurrentDateAndTime
                        End With
                        If objEmpField3.Delete(CInt(dgvData.CurrentRow.Cells("objempfield3unkid").Value)) = True Then
                            Call Fill_Grid()
                        End If
                    Else
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Sorry, there is no") & _
                                         " " & dgvData.Columns(dgvData.CurrentRow.Cells("objField3").ColumnIndex).HeaderText & " " & _
                                        Language.getMessage(mstrModuleName, 2, "defined. Please define") & _
                                         " " & dgvData.Columns(dgvData.CurrentRow.Cells("objField3").ColumnIndex).HeaderText & " " & _
                                        Language.getMessage(mstrModuleName, 4, "in order to perform delete operation."), enMsgBoxStyle.Information)
                        Exit Sub
                    End If
                Case 4
                    If CInt(dgvData.CurrentRow.Cells("objempfield4unkid").Value) > 0 Then
                        frm.displayDialog(enVoidCategoryType.ASSESSMENT, iVoidReason)
                        If iVoidReason.Length <= 0 Then
                            Exit Sub
                        End If
                        objEmpField4._Isvoid = True
                        objEmpField4._Voiddatetime = ConfigParameter._Object._CurrentDateAndTime
                        objEmpField4._Voidreason = iVoidReason
                        objEmpField4._Voiduserunkid = User._Object._Userunkid
                        With objEmpField4
                            ._FormName = mstrModuleName
                            ._Loginemployeeunkid = 0
                            ._ClientIP = getIP()
                            ._HostName = getHostName()
                            ._FromWeb = False
                            ._AuditUserId = User._Object._Userunkid
._CompanyUnkid = Company._Object._Companyunkid
                            ._AuditDate = ConfigParameter._Object._CurrentDateAndTime
                        End With
                        If objEmpField4.Delete(CInt(dgvData.CurrentRow.Cells("objempfield4unkid").Value)) = True Then
                            Call Fill_Grid()
                        End If
                    Else
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Sorry, there is no") & _
                                         " " & dgvData.Columns(dgvData.CurrentRow.Cells("objField4").ColumnIndex).HeaderText & " " & _
                                        Language.getMessage(mstrModuleName, 2, "defined. Please define") & _
                                         " " & dgvData.Columns(dgvData.CurrentRow.Cells("objField4").ColumnIndex).HeaderText & " " & _
                                        Language.getMessage(mstrModuleName, 4, "in order to perform delete operation."), enMsgBoxStyle.Information)
                        Exit Sub
                    End If
                Case 5
                    If CInt(dgvData.CurrentRow.Cells("objempfield5unkid").Value) > 0 Then
                        frm.displayDialog(enVoidCategoryType.ASSESSMENT, iVoidReason)
                        If iVoidReason.Length <= 0 Then
                            Exit Sub
                        End If
                        objEmpField5._Isvoid = True
                        objEmpField5._Voiddatetime = ConfigParameter._Object._CurrentDateAndTime
                        objEmpField5._Voidreason = iVoidReason
                        objEmpField5._Voiduserunkid = User._Object._Userunkid
                        With objEmpField5
                            ._FormName = mstrModuleName
                            ._Loginemployeeunkid = 0
                            ._ClientIP = getIP()
                            ._HostName = getHostName()
                            ._FromWeb = False
                            ._AuditUserId = User._Object._Userunkid
._CompanyUnkid = Company._Object._Companyunkid
                            ._AuditDate = ConfigParameter._Object._CurrentDateAndTime
                        End With
                        If objEmpField5.Delete(CInt(dgvData.CurrentRow.Cells("objempfield5unkid").Value)) = True Then
                            Call Fill_Grid()
                        End If
                    Else
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Sorry, there is no") & _
                                         " " & dgvData.Columns(dgvData.CurrentRow.Cells("objField5").ColumnIndex).HeaderText & " " & _
                                        Language.getMessage(mstrModuleName, 2, "defined. Please define") & _
                                         " " & dgvData.Columns(dgvData.CurrentRow.Cells("objField5").ColumnIndex).HeaderText & " " & _
                                        Language.getMessage(mstrModuleName, 4, "in order to perform delete operation."), enMsgBoxStyle.Information)
                        Exit Sub
                    End If
            End Select
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnDel_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub objbtnEdt_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Try
            Select Case CInt(sender.tag.ToString.Split("|")(1))
                Case 1
                    If CInt(dgvData.CurrentRow.Cells("objempfield1unkid").Value) > 0 Then
                        Dim frm As New objfrmAddEditEmpField1
                        If User._Object._Isrighttoleft = True Then
                            frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                            frm.RightToLeftLayout = True
                            Call Language.ctlRightToLeftlayOut(frm)
                        End If

                        If frm.displayDialog(CInt(dgvData.CurrentRow.Cells("objempfield1unkid").Value), enAction.EDIT_ONE, CInt(sender.tag.ToString.Split("|")(0)), CInt(cboEmployee.SelectedValue), CInt(cboPeriod.SelectedValue)) Then
                            Call Fill_Grid()
                        End If
                    Else
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Sorry, there is no") & _
                                        " " & dgvData.Columns(dgvData.CurrentRow.Cells("objField1").ColumnIndex).HeaderText & " " & _
                                        Language.getMessage(mstrModuleName, 2, "defined. Please define") & _
                                         " " & dgvData.Columns(dgvData.CurrentRow.Cells("objField1").ColumnIndex).HeaderText & " " & _
                                        Language.getMessage(mstrModuleName, 3, "in order to perform edit operation."), enMsgBoxStyle.Information)
                        Exit Sub
                    End If
                Case 2
                    If CInt(dgvData.CurrentRow.Cells("objempfield2unkid").Value) > 0 Then
                        Dim frm As New objfrmAddEditEmpField2

                        If User._Object._Isrighttoleft = True Then
                            frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                            frm.RightToLeftLayout = True
                            Call Language.ctlRightToLeftlayOut(frm)
                        End If
                        If frm.displayDialog(CInt(dgvData.CurrentRow.Cells("objempfield2unkid").Value), enAction.EDIT_ONE, CInt(sender.tag.ToString.Split("|")(0)), CInt(dgvData.CurrentRow.Cells("objemployeeunkid").Value), CInt(dgvData.CurrentRow.Cells("objperiodunkid").Value), CInt(dgvData.CurrentRow.Cells("objempfield1unkid").Value)) Then
                            Call Fill_Grid()
                        End If
                    Else
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Sorry, there is no") & _
                                         " " & dgvData.Columns(dgvData.CurrentRow.Cells("objField2").ColumnIndex).HeaderText & " " & _
                                        Language.getMessage(mstrModuleName, 2, "defined. Please define") & _
                                         " " & dgvData.Columns(dgvData.CurrentRow.Cells("objField2").ColumnIndex).HeaderText & " " & _
                                        Language.getMessage(mstrModuleName, 3, "in order to perform edit operation."), enMsgBoxStyle.Information)
                        Exit Sub
                    End If
                Case 3
                    If CInt(dgvData.CurrentRow.Cells("objempfield3unkid").Value) > 0 Then
                        Dim frm As New objfrmAddEditEmpField3

                        If User._Object._Isrighttoleft = True Then
                            frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                            frm.RightToLeftLayout = True
                            Call Language.ctlRightToLeftlayOut(frm)
                        End If

                        If frm.displayDialog(CInt(dgvData.CurrentRow.Cells("objempfield3unkid").Value), enAction.EDIT_ONE, CInt(sender.tag.ToString.Split("|")(0)), CInt(dgvData.CurrentRow.Cells("objemployeeunkid").Value), CInt(dgvData.CurrentRow.Cells("objperiodunkid").Value), CInt(dgvData.CurrentRow.Cells("objempfield2unkid").Value), CInt(dgvData.CurrentRow.Cells("objempfield1unkid").Value)) Then
                            Call Fill_Grid()
                        End If
                    Else
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Sorry, there is no") & _
                                         " " & dgvData.Columns(dgvData.CurrentRow.Cells("objField3").ColumnIndex).HeaderText & " " & _
                                        Language.getMessage(mstrModuleName, 2, "defined. Please define") & _
                                         " " & dgvData.Columns(dgvData.CurrentRow.Cells("objField3").ColumnIndex).HeaderText & " " & _
                                        Language.getMessage(mstrModuleName, 3, "in order to perform edit operation."), enMsgBoxStyle.Information)
                        Exit Sub
                    End If
                Case 4
                    If CInt(dgvData.CurrentRow.Cells("objempfield4unkid").Value) > 0 Then
                        Dim frm As New objfrmAddEditEmpField4

                        If User._Object._Isrighttoleft = True Then
                            frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                            frm.RightToLeftLayout = True
                            Call Language.ctlRightToLeftlayOut(frm)
                        End If

                        If frm.displayDialog(CInt(dgvData.CurrentRow.Cells("objempfield4unkid").Value), enAction.EDIT_ONE, CInt(sender.tag.ToString.Split("|")(0)), CInt(dgvData.CurrentRow.Cells("objemployeeunkid").Value), CInt(dgvData.CurrentRow.Cells("objperiodunkid").Value), CInt(dgvData.CurrentRow.Cells("objempfield3unkid").Value), CInt(dgvData.CurrentRow.Cells("objempfield2unkid").Value)) Then
                            Call Fill_Grid()
                        End If
                    Else
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Sorry, there is no") & _
                                         " " & dgvData.Columns(dgvData.CurrentRow.Cells("objField4").ColumnIndex).HeaderText & " " & _
                                        Language.getMessage(mstrModuleName, 2, "defined. Please define") & _
                                         " " & dgvData.Columns(dgvData.CurrentRow.Cells("objField4").ColumnIndex).HeaderText & " " & _
                                        Language.getMessage(mstrModuleName, 3, "in order to perform edit operation."), enMsgBoxStyle.Information)
                        Exit Sub
                    End If
                Case 5
                    If CInt(dgvData.CurrentRow.Cells("objempfield5unkid").Value) > 0 Then
                        Dim frm As New objfrmAddEditEmpField5

                        If User._Object._Isrighttoleft = True Then
                            frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                            frm.RightToLeftLayout = True
                            Call Language.ctlRightToLeftlayOut(frm)
                        End If

                        If frm.displayDialog(CInt(dgvData.CurrentRow.Cells("objempfield5unkid").Value), enAction.EDIT_ONE, CInt(sender.tag.ToString.Split("|")(0)), CInt(dgvData.CurrentRow.Cells("objemployeeunkid").Value), CInt(dgvData.CurrentRow.Cells("objperiodunkid").Value), CInt(dgvData.CurrentRow.Cells("objempfield4unkid").Value), CInt(dgvData.CurrentRow.Cells("objempfield3unkid").Value)) Then
                            Call Fill_Grid()
                        End If
                    Else
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Sorry, there is no") & _
                                         " " & dgvData.Columns(dgvData.CurrentRow.Cells("objField5").ColumnIndex).HeaderText & " " & _
                                        Language.getMessage(mstrModuleName, 2, "defined. Please define") & _
                                         " " & dgvData.Columns(dgvData.CurrentRow.Cells("objField5").ColumnIndex).HeaderText & " " & _
                                        Language.getMessage(mstrModuleName, 3, "in order to perform edit operation."), enMsgBoxStyle.Information)
                        Exit Sub
                    End If
            End Select
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnEdt_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub objbtnAdd_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Try
            If CInt(cboEmployee.SelectedValue) <= 0 Or CInt(cboPeriod.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 15, "Sorry, Employee and Period are mandatory information to performe Add Operation."))
                Exit Sub
            End If

            'S.SANDEEP |27-NOV-2020| -- START
            'ISSUE/ENHANCEMENT : VALID GOAL OPERATION
            'Select Case ConfigParameter._Object._CascadingTypeId
            '    Case enPACascading.LOOSE_CASCADING, enPACascading.STRICT_CASCADING
            'If (New clsassess_empfield1_master).IsGoalOwner(CInt(cboPeriod.SelectedValue), CInt(cboEmployee.SelectedValue)) = False Then
            '    Dim StrMsg As String = ""
            '    StrMsg = (New clsassess_empfield1_master).AllowedToAddGoals(CInt(cboPeriod.SelectedValue), CInt(cboEmployee.SelectedValue))
            '    If StrMsg.Trim.Length > 0 Then
            '        eZeeMsgBox.Show(StrMsg, enMsgBoxStyle.Information)
            '        Exit Sub
            '    End If
            'End If
            'End Select            
            'S.SANDEEP |27-NOV-2020| -- END

            'S.SANDEEP |08-JAN-2019| -- START
            If CInt(dgvData.CurrentRow.Cells("objempfield1unkid").Value) <= 0 Then
                'Dim objLckEmp As New clsassess_plan_eval_lockunlock
                'Dim mdtNextLockDate As Date = Nothing
                'If objLckEmp.isValidDate(CInt(cboEmployee.SelectedValue), CInt(cboPeriod.SelectedValue), clsassess_plan_eval_lockunlock.enAssementLockType.PLANNING, ConfigParameter._Object._CurrentDateAndTime) = False Then
                '    Dim mintLockId As Integer = 0
                '    Dim mblnisLock As Boolean = False
                '    objLckEmp.isExist(CInt(cboEmployee.SelectedValue), CInt(cboPeriod.SelectedValue), clsassess_plan_eval_lockunlock.enAssementLockType.PLANNING, "isunlocktenure = 1", mblnisLock, mintLockId, mdtNextLockDate)
                'End If
                Dim objPeriod As New clscommom_period_Tran
                Dim intDaysAfter As Integer = 0 : Dim iStartDate As DateTime = Nothing
                objPeriod._Periodunkid(FinancialYear._Object._DatabaseName) = CInt(cboPeriod.SelectedValue)
                intDaysAfter = objPeriod._LockDaysAfter
                If intDaysAfter > 0 Then
                    iStartDate = objPeriod._Start_Date.AddDays(intDaysAfter)
                End If
                objPeriod = Nothing
                If iStartDate <> Nothing Then
                    If ConfigParameter._Object._CurrentDateAndTime.Date > iStartDate Then
                        If LockEmployee() Then
                            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 34, "Sorry, you cannot do planning as last date for planning has been passed.") & Language.getMessage(mstrModuleName, 35, "Please contact your administrator/manager to do futher operation on it."), enMsgBoxStyle.Information)
                            Exit Sub
                        End If
                    End If
                End If
            End If
            'S.SANDEEP |08-JAN-2019| -- END

            Select Case CInt(sender.tag.ToString.Split("|")(1))
                Case 1
                    Dim frm As New objfrmAddEditEmpField1

                    If User._Object._Isrighttoleft = True Then
                        frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                        frm.RightToLeftLayout = True
                        Call Language.ctlRightToLeftlayOut(frm)
                    End If

                    If frm.displayDialog(-1, enAction.ADD_CONTINUE, CInt(sender.tag.ToString.Split("|")(0)), CInt(cboEmployee.SelectedValue), CInt(cboPeriod.SelectedValue)) Then
                        Call Fill_Grid()
                    End If
                Case 2
                    Dim frm As New objfrmAddEditEmpField2

                    If User._Object._Isrighttoleft = True Then
                        frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                        frm.RightToLeftLayout = True
                        Call Language.ctlRightToLeftlayOut(frm)
                    End If
                    If ConfigParameter._Object._CascadingTypeId = enPACascading.STRICT_CASCADING Or _
                       ConfigParameter._Object._CascadingTypeId = enPACascading.LOOSE_CASCADING Then
                        If frm.displayDialog(-1, enAction.ADD_CONTINUE, CInt(sender.tag.ToString.Split("|")(0)), CInt(cboEmployee.SelectedValue), CInt(cboPeriod.SelectedValue), CInt(dgvData.CurrentRow.Cells("objempfield1unkid").Value)) Then
                            Call Fill_Grid()
                        End If
                    Else
                        If frm.displayDialog(-1, enAction.ADD_CONTINUE, CInt(sender.tag.ToString.Split("|")(0)), CInt(cboEmployee.SelectedValue), CInt(cboPeriod.SelectedValue), CInt(dgvData.CurrentRow.Cells("objempfield1unkid").Value)) Then
                            Call Fill_Grid()
                        End If
                    End If
                Case 3
                    Dim frm As New objfrmAddEditEmpField3

                    If User._Object._Isrighttoleft = True Then
                        frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                        frm.RightToLeftLayout = True
                        Call Language.ctlRightToLeftlayOut(frm)
                    End If
                    If ConfigParameter._Object._CascadingTypeId = enPACascading.STRICT_CASCADING Or _
                       ConfigParameter._Object._CascadingTypeId = enPACascading.LOOSE_CASCADING Then
                        If frm.displayDialog(-1, enAction.ADD_CONTINUE, CInt(sender.tag.ToString.Split("|")(0)), CInt(cboEmployee.SelectedValue), CInt(cboPeriod.SelectedValue), CInt(dgvData.CurrentRow.Cells("objempfield2unkid").Value), CInt(dgvData.CurrentRow.Cells("objempfield1unkid").Value)) Then
                            Call Fill_Grid()
                        End If
                    Else
                        If frm.displayDialog(-1, enAction.ADD_CONTINUE, CInt(sender.tag.ToString.Split("|")(0)), CInt(cboEmployee.SelectedValue), CInt(cboPeriod.SelectedValue), CInt(dgvData.CurrentRow.Cells("objempfield2unkid").Value), CInt(dgvData.CurrentRow.Cells("objempfield1unkid").Value)) Then
                            Call Fill_Grid()
                        End If
                    End If
                Case 4
                    Dim frm As New objfrmAddEditEmpField4

                    If User._Object._Isrighttoleft = True Then
                        frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                        frm.RightToLeftLayout = True
                        Call Language.ctlRightToLeftlayOut(frm)
                    End If

                    If ConfigParameter._Object._CascadingTypeId = enPACascading.STRICT_CASCADING Or _
                       ConfigParameter._Object._CascadingTypeId = enPACascading.LOOSE_CASCADING Then
                        If frm.displayDialog(-1, enAction.ADD_CONTINUE, CInt(sender.tag.ToString.Split("|")(0)), CInt(cboEmployee.SelectedValue), CInt(cboPeriod.SelectedValue), CInt(dgvData.CurrentRow.Cells("objempfield3unkid").Value), CInt(dgvData.CurrentRow.Cells("objempfield2unkid").Value)) Then
                            Call Fill_Grid()
                        End If
                    Else
                        If frm.displayDialog(-1, enAction.ADD_CONTINUE, CInt(sender.tag.ToString.Split("|")(0)), CInt(cboEmployee.SelectedValue), CInt(cboPeriod.SelectedValue), CInt(dgvData.CurrentRow.Cells("objempfield3unkid").Value), CInt(dgvData.CurrentRow.Cells("objempfield2unkid").Value)) Then
                            Call Fill_Grid()
                        End If
                    End If

                Case 5
                    Dim frm As New objfrmAddEditEmpField5

                    If User._Object._Isrighttoleft = True Then
                        frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                        frm.RightToLeftLayout = True
                        Call Language.ctlRightToLeftlayOut(frm)
                    End If

                    If ConfigParameter._Object._CascadingTypeId = enPACascading.STRICT_CASCADING Or _
                       ConfigParameter._Object._CascadingTypeId = enPACascading.LOOSE_CASCADING Then
                        If frm.displayDialog(-1, enAction.ADD_CONTINUE, CInt(sender.tag.ToString.Split("|")(0)), CInt(cboEmployee.SelectedValue), CInt(cboPeriod.SelectedValue), CInt(dgvData.CurrentRow.Cells("objempfield4unkid").Value), CInt(dgvData.CurrentRow.Cells("objempfield3unkid").Value)) Then
                            Call Fill_Grid()
                        End If
                    Else
                        If frm.displayDialog(-1, enAction.ADD_CONTINUE, CInt(sender.tag.ToString.Split("|")(0)), CInt(cboEmployee.SelectedValue), CInt(cboPeriod.SelectedValue), CInt(dgvData.CurrentRow.Cells("objempfield4unkid").Value), CInt(dgvData.CurrentRow.Cells("objempfield3unkid").Value)) Then
                            Call Fill_Grid()
                        End If
                    End If
            End Select
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnAdd_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub mnuSubmitApproval_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuSubmitApproval.Click
        Try
            If dgvData.RowCount > 0 Then

                Dim dtemp() As DataRow = mdtFinal.Select("Emp <> ''")
                If dtemp.Length <= 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 11, "Sorry, Balance Score Card Planning is not planned for selected employee and period."), enMsgBoxStyle.Information)
                    Exit Sub
                End If
                'S.SANDEEP |10-AUG-2021| -- START
                'If dgvData.SelectedRows(0).DefaultCellStyle.ForeColor = Color.Green Then
                '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 12, "Sorry, Balance Score Card Planning is already sent for approval for selected employee and period. ."), enMsgBoxStyle.Information)
                '    Exit Sub
                'End If

                'If dgvData.SelectedRows(0).DefaultCellStyle.ForeColor = Color.Blue Then
                '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 18, "Sorry, Balance Score Card Planning is already approved for selected employee and period. ."), enMsgBoxStyle.Information)
                '    Exit Sub
                'End If
                Dim cLr1, cLr2 As Color

                If ConfigParameter._Object._BSC_StatusColors.Keys.Count > 0 Then
                    If ConfigParameter._Object._BSC_StatusColors.ContainsKey(CInt(enObjective_Status.FINAL_SAVE)) Then
                        cLr1 = Color.FromArgb(CInt(ConfigParameter._Object._BSC_StatusColors(CInt(enObjective_Status.FINAL_SAVE))))
                    Else
                        cLr1 = Color.Blue
                    End If
                Else
                    cLr1 = Color.Blue
                End If

                If ConfigParameter._Object._BSC_StatusColors.Keys.Count > 0 Then
                    If ConfigParameter._Object._BSC_StatusColors.ContainsKey(CInt(enObjective_Status.SUBMIT_APPROVAL)) Then
                        cLr2 = Color.FromArgb(CInt(ConfigParameter._Object._BSC_StatusColors(CInt(enObjective_Status.SUBMIT_APPROVAL))))
                    Else
                        cLr2 = Color.Green
                    End If
                Else
                    cLr2 = Color.Green
                End If

                If dgvData.SelectedRows(0).DefaultCellStyle.ForeColor = cLr2 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 12, "Sorry, Balance Score Card Planning is already sent for approval for selected employee and period. ."), enMsgBoxStyle.Information)
                    Exit Sub
                End If

                If dgvData.SelectedRows(0).DefaultCellStyle.ForeColor = cLr1 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 18, "Sorry, Balance Score Card Planning is already approved for selected employee and period. ."), enMsgBoxStyle.Information)
                    Exit Sub
                End If
                'S.SANDEEP |10-AUG-2021| -- END

                Dim objAssessor As New clsAssessor_tran
                Dim sMsg As String = String.Empty
                sMsg = objAssessor.IsAssessorPresent(CInt(cboEmployee.SelectedValue))
                If sMsg <> "" Then
                    eZeeMsgBox.Show(sMsg, enMsgBoxStyle.Information)
                    Exit Sub
                End If
                objAssessor = Nothing

                Dim objFMapping As New clsAssess_Field_Mapping
                sMsg = objFMapping.Is_Weight_Set_For_Commiting(clsAssess_Field_Mapping.enWeightCheckType.CKT_EMPLOYEE_LEVEL, CInt(cboPeriod.SelectedValue), 0, CInt(cboEmployee.SelectedValue))
                If sMsg.Trim.Length > 0 Then
                    eZeeMsgBox.Show(sMsg, enMsgBoxStyle.Information)
                    objFMapping = Nothing
                    Exit Sub
                End If
                If objFMapping IsNot Nothing Then objFMapping = Nothing

                If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 13, "You are about to Submit this Balance Score Card Planning for Approval, if you follow this process then this employee won't be able to modify or delete this transaction." & vbCrLf & _
                                                                      "Do you wish to continue?"), CType(enMsgBoxStyle.Information + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.No Then Exit Sub

                Dim objEStatusTran As New clsassess_empstatus_tran
                objEStatusTran._Assessoremployeeunkid = 0
                objEStatusTran._Assessormasterunkid = 0
                objEStatusTran._Commtents = ""
                objEStatusTran._Employeeunkid = CInt(cboEmployee.SelectedValue)
                objEStatusTran._Isunlock = False
                objEStatusTran._Loginemployeeunkid = 0
                objEStatusTran._Periodunkid = CInt(cboPeriod.SelectedValue)
                objEStatusTran._Status_Date = ConfigParameter._Object._CurrentDateAndTime
                objEStatusTran._Statustypeid = enObjective_Status.SUBMIT_APPROVAL
                objEStatusTran._Userunkid = User._Object._Userunkid
                With objEStatusTran
                    ._FormName = mstrModuleName
                    ._Loginemployeeunkid = 0
                    ._ClientIP = getIP()
                    ._HostName = getHostName()
                    ._FromWeb = False
                    ._AuditUserId = User._Object._Userunkid
._CompanyUnkid = Company._Object._Companyunkid
                    ._AuditDate = ConfigParameter._Object._CurrentDateAndTime
                End With
                If objEStatusTran.Insert() = False Then
                    eZeeMsgBox.Show(objEStatusTran._Message, enMsgBoxStyle.Information)
                    Exit Sub
                Else
                    If ConfigParameter._Object._ArutiSelfServiceURL.Trim.Length > 0 Then
                        Dim dtmp() As DataRow = CType(cboPeriod.DataSource, DataTable).Select("periodunkid = '" & CInt(cboPeriod.SelectedValue) & "'")
                        If dtmp.Length > 0 Then

                            'S.SANDEEP [28-May-2018] -- START
                            'ISSUE/ENHANCEMENT : {Audit Trails} 
                            objEmpField1._FormName = mstrModuleName
                            objEmpField1._Loginemployeeunkid = 0
                            objEmpField1._ClientIP = getIP()
                            objEmpField1._HostName = getHostName()
                            objEmpField1._FromWeb = False
                            objEmpField1._AuditUserId = User._Object._Userunkid
objEmpField1._CompanyUnkid = Company._Object._Companyunkid
                            objEmpField1._AuditDate = ConfigParameter._Object._CurrentDateAndTime
                            'S.SANDEEP [28-May-2018] -- END

                            'Sohail (21 Aug 2015) -- Start
                            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                            'objEmpField1.Send_Notification_Assessor(CInt(cboEmployee.SelectedValue), CInt(cboPeriod.SelectedValue), _
                            '                                        CInt(dtmp(0).Item("yearunkid")), FinancialYear._Object._FinancialYear_Name, , , _
                            '                                        enLogin_Mode.DESKTOP, 0, 0)
                            objEmpField1.Send_Notification_Assessor(CInt(cboEmployee.SelectedValue), CInt(cboPeriod.SelectedValue), _
                                                                   CInt(dtmp(0).Item("yearunkid")), FinancialYear._Object._FinancialYear_Name, FinancialYear._Object._DatabaseName, , , _
                                                                    enLogin_Mode.DESKTOP, 0, 0)
                            'Sohail (21 Aug 2015) -- End
                        End If
                    End If
                    Call Fill_Grid()
                End If
                objEStatusTran = Nothing
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "mnuSubmitApproval_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub mnuApproveSubmitted_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuApproveSubmitted.Click
        Try
            If dgvData.RowCount > 0 Then
                Dim dtemp() As DataRow = mdtFinal.Select("Emp <> ''")
                If dtemp.Length <= 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 11, "Sorry, Balance Score Card Planning is not planned for selected employee and period."), enMsgBoxStyle.Information)
                    Exit Sub
                End If

                'S.SANDEEP |10-AUG-2021| -- START
                'If dgvData.SelectedRows(0).DefaultCellStyle.ForeColor <> Color.Green AndAlso dgvData.SelectedRows(0).DefaultCellStyle.ForeColor <> Color.Blue Then
                '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 16, "Sorry, Balance Score Card Planning is yet not submitted for selected employee and period."), enMsgBoxStyle.Information)
                '    Exit Sub
                'End If

                'Dim iFinal As Boolean = False
                'If dgvData.SelectedRows(0).DefaultCellStyle.ForeColor = Color.Green Then
                '    iFinal = False
                'ElseIf dgvData.SelectedRows(0).DefaultCellStyle.ForeColor = Color.Blue Then
                '    iFinal = True
                'End If
                Dim cLr1, cLr2 As Color

                If ConfigParameter._Object._BSC_StatusColors.Keys.Count > 0 Then
                    If ConfigParameter._Object._BSC_StatusColors.ContainsKey(CInt(enObjective_Status.FINAL_SAVE)) Then
                        cLr1 = Color.FromArgb(CInt(ConfigParameter._Object._BSC_StatusColors(CInt(enObjective_Status.FINAL_SAVE))))
                    Else
                        cLr1 = Color.Blue
                    End If
                Else
                    cLr1 = Color.Blue
                End If

                If ConfigParameter._Object._BSC_StatusColors.Keys.Count > 0 Then
                    If ConfigParameter._Object._BSC_StatusColors.ContainsKey(CInt(enObjective_Status.SUBMIT_APPROVAL)) Then
                        cLr2 = Color.FromArgb(CInt(ConfigParameter._Object._BSC_StatusColors(CInt(enObjective_Status.SUBMIT_APPROVAL))))
                    Else
                        cLr2 = Color.Green
                    End If
                Else
                    cLr2 = Color.Green
                End If

                If dgvData.SelectedRows(0).DefaultCellStyle.ForeColor <> cLr2 AndAlso dgvData.SelectedRows(0).DefaultCellStyle.ForeColor <> cLr1 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 16, "Sorry, Balance Score Card Planning is yet not submitted for selected employee and period."), enMsgBoxStyle.Information)
                    Exit Sub
                End If

                Dim iFinal As Boolean = False
                If dgvData.SelectedRows(0).DefaultCellStyle.ForeColor = cLr1 Then
                    iFinal = True
                End If
                'S.SANDEEP |10-AUG-2021| -- END

                If iFinal = True Then
                    Dim objAnalysis_Master As New clsevaluation_analysis_master
                    If objAnalysis_Master.isExist(enAssessmentMode.SELF_ASSESSMENT, CInt(cboEmployee.SelectedValue), CInt(cboPeriod.SelectedValue)) = True Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 21, "Sorry, you cannot unlock BSC Planning for the selected period. Reason : Assessment is already done for the selected period"), enMsgBoxStyle.Information)
                        Exit Sub
                    End If

                    If objAnalysis_Master.isExist(enAssessmentMode.APPRAISER_ASSESSMENT, CInt(cboEmployee.SelectedValue), CInt(cboPeriod.SelectedValue)) = True Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 21, "Sorry, you cannot unlock BSC Planning for the selected period. Reason : Assessment is already done for the selected period"), enMsgBoxStyle.Information)
                        Exit Sub
                    End If

                    If objAnalysis_Master.isExist(enAssessmentMode.REVIEWER_ASSESSMENT, CInt(cboEmployee.SelectedValue), CInt(cboPeriod.SelectedValue)) = True Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 21, "Sorry, you cannot unlock BSC Planning for the selected period. Reason : Assessment is already done for the selected period"), enMsgBoxStyle.Information)
                        Exit Sub
                    End If
                    objAnalysis_Master = Nothing
                End If

                If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 14, "You are about to Approve/Disapprove this Balance Score Card Planning, with this process this employee can't be modified or deleted. It can only be modified or deleted if it is disapproved." & vbCrLf & _
                                                       "Do you wish to continue?"), CType(enMsgBoxStyle.Information + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.No Then Exit Sub

                Dim frm As New frmApproveRejectPlanning

                If User._Object._Isrighttoleft = True Then
                    frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                    frm.RightToLeftLayout = True
                    Call Language.ctlRightToLeftlayOut(frm)
                End If
                Dim iDisplayText As String = String.Empty
                iDisplayText = dtemp(0).Item("Emp").ToString.Trim & ", " & Language.getMessage(mstrModuleName, 17, "For Period : ") & " " & cboPeriod.Text
                If frm.displayDialog(iDisplayText, CInt(cboEmployee.SelectedValue), CInt(cboPeriod.SelectedValue), iFinal, mdtFinal) Then
                    Call Fill_Grid()
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "mnuApproveSubmitted_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub mnuUnlockFinalSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuUnlockFinalSave.Click
        Try
            If dgvData.RowCount > 0 Then
                Dim dtemp() As DataRow = mdtFinal.Select("Emp <> ''")
                If dtemp.Length <= 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 11, "Sorry, Balance Score Card Planning is not planned for selected employee and period."), enMsgBoxStyle.Information)
                    Exit Sub
                End If

                If dgvData.SelectedRows(0).DefaultCellStyle.ForeColor <> Color.Blue Then
                    'S.SANDEEP |12-MAR-2019| -- START
                    'ISSUE/ENHANCEMENT : {Action List Phase 2 - 61}
                    'eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 25, "Sorry, Balance Score Card Planning is not yet final saved for selected employee and period."), enMsgBoxStyle.Information)
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 101, "Sorry, Balance Score Card Planning is not yet final approved for selected employee and period."), enMsgBoxStyle.Information)
                    'S.SANDEEP |12-MAR-2019| -- END
                    Exit Sub
                End If

                Dim objAnalysis_Master As New clsevaluation_analysis_master
                If objAnalysis_Master.isExist(enAssessmentMode.SELF_ASSESSMENT, CInt(cboEmployee.SelectedValue), CInt(cboPeriod.SelectedValue)) = True Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 21, "Sorry, you cannot unlock BSC Planning for the selected period. Reason : Assessment is already done for the selected period"), enMsgBoxStyle.Information)
                    Exit Sub
                End If

                If objAnalysis_Master.isExist(enAssessmentMode.APPRAISER_ASSESSMENT, CInt(cboEmployee.SelectedValue), CInt(cboPeriod.SelectedValue)) = True Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 21, "Sorry, you cannot unlock BSC Planning for the selected period. Reason : Assessment is already done for the selected period"), enMsgBoxStyle.Information)
                    Exit Sub
                End If

                If objAnalysis_Master.isExist(enAssessmentMode.REVIEWER_ASSESSMENT, CInt(cboEmployee.SelectedValue), CInt(cboPeriod.SelectedValue)) = True Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 21, "Sorry, you cannot unlock BSC Planning for the selected period. Reason : Assessment is already done for the selected period"), enMsgBoxStyle.Information)
                    Exit Sub
                End If
                objAnalysis_Master = Nothing
                Dim iMsgText As String = ""
                iMsgText = Language.getMessage(mstrModuleName, 22, "You are about to unlock the employee goals. This will result in opening of all operation on employee goals.") & vbCrLf & _
                           Language.getMessage(mstrModuleName, 23, "Do you wish to continue?")

                If eZeeMsgBox.Show(iMsgText, enMsgBoxStyle.Information + enMsgBoxStyle.YesNo) = Windows.Forms.DialogResult.No Then Exit Sub

                Dim iUnlockComments As String = ""

                iUnlockComments = InputBox(Language.getMessage(mstrModuleName, 24, "Enter reason to unlock"), Me.Text)

                If iUnlockComments.Trim.Length > 0 Then
                    Dim objEStatusTran As New clsassess_empstatus_tran
                    objEStatusTran._Assessoremployeeunkid = 0
                    objEStatusTran._Assessormasterunkid = 0
                    objEStatusTran._Commtents = iUnlockComments
                    objEStatusTran._Employeeunkid = CInt(cboEmployee.SelectedValue)
                    objEStatusTran._Isunlock = True
                    objEStatusTran._Loginemployeeunkid = 0
                    objEStatusTran._Periodunkid = CInt(cboPeriod.SelectedValue)
                    objEStatusTran._Status_Date = ConfigParameter._Object._CurrentDateAndTime
                    objEStatusTran._Statustypeid = enObjective_Status.OPEN_CHANGES
                    objEStatusTran._Userunkid = User._Object._Userunkid
                    With objEStatusTran
                        ._FormName = mstrModuleName
                        ._Loginemployeeunkid = 0
                        ._ClientIP = getIP()
                        ._HostName = getHostName()
                        ._FromWeb = False
                        ._AuditUserId = User._Object._Userunkid
._CompanyUnkid = Company._Object._Companyunkid
                        ._AuditDate = ConfigParameter._Object._CurrentDateAndTime
                    End With
                    If objEStatusTran.Insert() = False Then
                        eZeeMsgBox.Show(objEStatusTran._Message, enMsgBoxStyle.Information)
                        Exit Sub
                    Else
                        Call Fill_Grid()
                    End If
                    objEStatusTran = Nothing
                End If


            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "mnuUnlockFinalSave_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub cboFieldValue1_DropDown(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboFieldValue1.DropDown, cboFieldValue2.DropDown, cboFieldValue3.DropDown, cboFieldValue4.DropDown, cboFieldValue5.DropDown
        Try
            mblnDropDownClosed = False
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboFieldValue1_DropDown", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub cboFieldValue1_DropDownClosed(ByVal sender As Object, ByVal e As EventArgs) Handles cboFieldValue1.DropDownClosed, cboFieldValue2.DropDownClosed, cboFieldValue3.DropDownClosed, cboFieldValue4.DropDownClosed, cboFieldValue5.DropDownClosed
        Try
            Dim iCbo As ComboBox = Nothing
            Select Case CType(sender, ComboBox).Name.ToUpper
                Case cboFieldValue1.Name.ToUpper
                    iCbo = cboFieldValue1
                Case cboFieldValue2.Name.ToUpper
                    iCbo = cboFieldValue2
                Case cboFieldValue3.Name.ToUpper
                    iCbo = cboFieldValue3
                Case cboFieldValue4.Name.ToUpper
                    iCbo = cboFieldValue4
                Case cboFieldValue5.Name.ToUpper
                    iCbo = cboFieldValue5
            End Select
            ToolTip1.Hide(iCbo) : mblnDropDownClosed = True
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboFieldValue1_DropDownClosed", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub cboFieldValue1_DrawItem(ByVal sender As Object, ByVal e As DrawItemEventArgs) Handles cboFieldValue1.DrawItem, cboFieldValue2.DrawItem, cboFieldValue3.DrawItem, cboFieldValue4.DrawItem, cboFieldValue5.DrawItem
        Try
            Dim iCbo As ComboBox = Nothing
            Select Case CType(sender, ComboBox).Name.ToUpper
                Case cboFieldValue1.Name.ToUpper
                    iCbo = cboFieldValue1
                Case cboFieldValue2.Name.ToUpper
                    iCbo = cboFieldValue2
                Case cboFieldValue3.Name.ToUpper
                    iCbo = cboFieldValue3
                Case cboFieldValue4.Name.ToUpper
                    iCbo = cboFieldValue4
                Case cboFieldValue5.Name.ToUpper
                    iCbo = cboFieldValue5
            End Select
            If e.Index < 0 Then
                Return
            End If
            Dim text As String = iCbo.GetItemText(iCbo.Items(e.Index))
            e.DrawBackground()
            Using br As New SolidBrush(e.ForeColor)
                e.Graphics.DrawString(text, e.Font, br, e.Bounds)
            End Using
            If (e.State And DrawItemState.Selected) = DrawItemState.Selected Then
                If mblnDropDownClosed = False Then
                    ToolTip1.Show(text, iCbo, e.Bounds.Right, e.Bounds.Bottom)
                End If
            End If
            e.DrawFocusRectangle()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboFieldValue1_DrawItem", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub cboFieldValue1_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboFieldValue1.SelectedIndexChanged
        Try
            Dim dsList As New DataSet
            dsList = objEmpField2.getComboList(CInt(cboEmployee.SelectedValue), CInt(cboPeriod.SelectedValue), CInt(cboFieldValue1.SelectedValue), "List", True)
            With cboFieldValue2
                .ValueMember = "Id"
                .DisplayMember = "Name"
                .DataSource = dsList.Tables("List")
                .SelectedValue = 0
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboFieldValue1_SelectedIndexChanged", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub cboFieldValue2_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboFieldValue2.SelectedIndexChanged
        Try
            Dim dsList As New DataSet
            dsList = objEmpField3.getComboList(CInt(cboEmployee.SelectedValue), CInt(cboPeriod.SelectedValue), CInt(cboFieldValue2.SelectedValue), "List", True)
            With cboFieldValue3
                .ValueMember = "Id"
                .DisplayMember = "Name"
                .DataSource = dsList.Tables("List")
                .SelectedValue = 0
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboFieldValue2_SelectedIndexChanged", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub cboFieldValue3_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboFieldValue3.SelectedIndexChanged
        Try
            Dim dsList As New DataSet
            dsList = objEmpField4.getComboList(CInt(cboEmployee.SelectedValue), CInt(cboPeriod.SelectedValue), CInt(cboFieldValue3.SelectedValue), "List", True)
            With cboFieldValue4
                .ValueMember = "Id"
                .DisplayMember = "Name"
                .DataSource = dsList.Tables("List")
                .SelectedValue = 0
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboFieldValue3_SelectedIndexChanged", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub cboFieldValue4_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboFieldValue4.SelectedIndexChanged
        Try
            Dim dsList As New DataSet
            dsList = objEmpField5.getComboList(CInt(cboEmployee.SelectedValue), CInt(cboPeriod.SelectedValue), CInt(cboFieldValue4.SelectedValue), "List", True)
            With cboFieldValue5
                .ValueMember = "Id"
                .DisplayMember = "Name"
                .DataSource = dsList.Tables("List")
                .SelectedValue = 0
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboFieldValue4_SelectedIndexChanged", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub mnuGlobalAssign_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuGlobalAssign.Click
        Dim frm As New frmGlobalAssignEmpGoals
        Try
            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If
            If frm.displayDialog(cboEmployee, cboPeriod) Then
                If CInt(cboEmployee.SelectedValue) > 0 Then
                    Call Fill_Grid()
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "mnuGlobalAssign_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub mnuUpdatePercentage_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuUpdatePercentage.Click
        Dim frm As New frmEmpUpdatePercentage
        Try
            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If
            If frm.displayDialog(cboEmployee, cboPeriod) Then
                If CInt(cboEmployee.SelectedValue) > 0 Then
                    Call Fill_Grid()
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "mnuGlobalAssign_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    'S.SANDEEP [01 JUN 2015] -- START
    'ENHANCEMENT : Void Goals Global
    Private Sub mnuVoidGoals_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuVoidGoals.Click
        Dim frm As New frmGlobalVoidEmpGoals
        Try
            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If
            frm.ShowDialog()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "mnuVoidGoals_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub
    'S.SANDEEP [01 JUN 2015] -- END

    'Shani (26-Sep-2016) -- Start
    'Enhancement -Changes PA Flow And Add New screen Given By (Andrew)
    Private Sub mnuAppRejGoalAccomplished_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuAppRejGoalAccomplished.Click
        Dim frm As New frmAppRejGoalAccomplishmentList
        Try
            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If
            frm.ShowDialog()

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "mnuAppRejGoalAccomplished_Click", mstrModuleName)
        End Try
    End Sub
    'Shani (26-Sep-2016) -- End

    'Pinkal (19-Sep-2016) -- Start
    'Enhancement - Global Operation for all Organization.
    Private Sub mnuGlobalOperation_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuGlobalOperation.Click
        Dim frm As New frmEmpGlobalOperation
        Try
            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If

            frm.ShowDialog()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "mnuGlobalOperation_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub
    'Pinkal (19-Sep-2016) -- End

    'S.SANDEEP [07-OCT-2017] -- START
    'ISSUE/ENHANCEMENT : IMPORT GOALS
    Private Sub mnuImportGoals_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuImportGoals.Click
        Dim frm As New frmImportEmployeeGoalsWizard
        Try
            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If
            frm.ShowDialog()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "mnuImportGoals_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub mnuGlobalVoidGoals_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuGlobalVoidGoals.Click
        Dim frm As New frmGlobalVoidAllEmpGoals
        Try
            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If
            frm.ShowDialog()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "mnuGlobalVoidGoals_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub
    'S.SANDEEP [07-OCT-2017] -- END

    'S.SANDEEP |08-JAN-2019| -- START
    Private Sub mnuUnlockEmployee_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuUnlockEmployee.Click
        Dim frm As New frmUnlockEmployee
        Try
            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If
            frm.ShowDialog()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "mnuUnlockEmployee_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub
    'S.SANDEEP |08-JAN-2019| -- END
    
    'S.SANDEEP |18-JAN-2019| -- START
    Private Sub dgvData_CellPainting(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellPaintingEventArgs) Handles dgvData.CellPainting
        Try
            If mintPerspectiveColIndex > 0 Then
                If e.ColumnIndex = mintPerspectiveColIndex Then
                    If e.RowIndex < 1 Or e.ColumnIndex < 0 Then Return
                    If IsTheSameCellValue(e.ColumnIndex, e.RowIndex) Then
                        e.AdvancedBorderStyle.Top = DataGridViewAdvancedCellBorderStyle.None
                    Else
                        e.AdvancedBorderStyle.Top = dgvData.AdvancedCellBorderStyle.Top
                    End If
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgvData_CellPainting", mstrModuleName)
        Finally
        End Try
    End Sub
    'S.SANDEEP |18-JAN-2019| -- END

    'S.SANDEEP |12-FEB-2019| -- START
    'ISSUE/ENHANCEMENT : {Performance Assessment Changes}
    Private Sub dgvData_DataError(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewDataErrorEventArgs) Handles dgvData.DataError

    End Sub
    'S.SANDEEP |12-FEB-2019| -- END

#End Region


    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Private Sub OtherSettings()
        Try
            Me.SuspendLayout()
            
            Call SetLanguage()

            Me.gbFilterCriteria.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbFilterCriteria.ForeColor = GUI._eZeeContainerHeaderForeColor


            Me.btnClose.GradientBackColor = GUI._ButttonBackColor
            Me.btnClose.GradientForeColor = GUI._ButttonFontColor

            Me.btnOperations.GradientBackColor = GUI._ButttonBackColor
            Me.btnOperations.GradientForeColor = GUI._ButttonFontColor

            Me.btnSubmitForGoalAccomplished.GradientBackColor = GUI._ButttonBackColor
            Me.btnSubmitForGoalAccomplished.GradientForeColor = GUI._ButttonFontColor


            Me.ResumeLayout()
        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "OtherSettings", mstrModuleName)
        End Try
    End Sub


    Private Sub SetLanguage()
        Try
            Me.Text = Language._Object.getCaption(Me.Name, Me.Text)

            Me.gbFilterCriteria.Text = Language._Object.getCaption(Me.gbFilterCriteria.Name, Me.gbFilterCriteria.Text)
            Me.lblPeriod.Text = Language._Object.getCaption(Me.lblPeriod.Name, Me.lblPeriod.Text)
            Me.lblStatus.Text = Language._Object.getCaption(Me.lblStatus.Name, Me.lblStatus.Text)
            Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
            Me.lblEmployee.Text = Language._Object.getCaption(Me.lblEmployee.Name, Me.lblEmployee.Text)
            Me.lblCaption1.Text = Language._Object.getCaption(Me.lblCaption1.Name, Me.lblCaption1.Text)
            Me.lblCaption2.Text = Language._Object.getCaption(Me.lblCaption2.Name, Me.lblCaption2.Text)
            Me.btnOperations.Text = Language._Object.getCaption(Me.btnOperations.Name, Me.btnOperations.Text)
            Me.mnuSubmitApproval.Text = Language._Object.getCaption(Me.mnuSubmitApproval.Name, Me.mnuSubmitApproval.Text)
            Me.mnuApproveSubmitted.Text = Language._Object.getCaption(Me.mnuApproveSubmitted.Name, Me.mnuApproveSubmitted.Text)
            Me.mnuFinalSave.Text = Language._Object.getCaption(Me.mnuFinalSave.Name, Me.mnuFinalSave.Text)
            Me.mnuUnlockFinalSave.Text = Language._Object.getCaption(Me.mnuUnlockFinalSave.Name, Me.mnuUnlockFinalSave.Text)
            Me.mnuGetFileFormat.Text = Language._Object.getCaption(Me.mnuGetFileFormat.Name, Me.mnuGetFileFormat.Text)
            Me.mnuImportBSC_Planning.Text = Language._Object.getCaption(Me.mnuImportBSC_Planning.Name, Me.mnuImportBSC_Planning.Text)
            Me.mnuPrintBSCForm.Text = Language._Object.getCaption(Me.mnuPrintBSCForm.Name, Me.mnuPrintBSCForm.Text)
            Me.lblPerspective.Text = Language._Object.getCaption(Me.lblPerspective.Name, Me.lblPerspective.Text)
            Me.mnuGlobalAssign.Text = Language._Object.getCaption(Me.mnuGlobalAssign.Name, Me.mnuGlobalAssign.Text)
            Me.mnuUpdatePercentage.Text = Language._Object.getCaption(Me.mnuUpdatePercentage.Name, Me.mnuUpdatePercentage.Text)
            Me.mnuVoidGoals.Text = Language._Object.getCaption(Me.mnuVoidGoals.Name, Me.mnuVoidGoals.Text)
            Me.mnuAppRejGoalAccomplished.Text = Language._Object.getCaption(Me.mnuAppRejGoalAccomplished.Name, Me.mnuAppRejGoalAccomplished.Text)
            Me.mnuGlobalOperation.Text = Language._Object.getCaption(Me.mnuGlobalOperation.Name, Me.mnuGlobalOperation.Text)
            Me.btnSubmitForGoalAccomplished.Text = Language._Object.getCaption(Me.btnSubmitForGoalAccomplished.Name, Me.btnSubmitForGoalAccomplished.Text)
            Me.mnuImportGoals.Text = Language._Object.getCaption(Me.mnuImportGoals.Name, Me.mnuImportGoals.Text)
            Me.mnuGlobalVoidGoals.Text = Language._Object.getCaption(Me.mnuGlobalVoidGoals.Name, Me.mnuGlobalVoidGoals.Text)

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
        End Try
    End Sub


    Private Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Sorry, there is no")
            Language.setMessage(mstrModuleName, 2, "defined. Please define")
            Language.setMessage(mstrModuleName, 3, "in order to perform edit operation.")
            Language.setMessage(mstrModuleName, 4, "in order to perform delete operation.")
            Language.setMessage(mstrModuleName, 5, "Add")
            Language.setMessage(mstrModuleName, 6, "Edit")
            Language.setMessage(mstrModuleName, 7, "Delete")
            Language.setMessage(mstrModuleName, 8, "Sorry, No data defined for asseessment caption settings screen.")
            Language.setMessage(mstrModuleName, 9, "Sorry no field is mapped with the selected period.")
            Language.setMessage(mstrModuleName, 10, "Employee and Period are mandatory information. Please select Employee and Period to continue.")
            Language.setMessage(mstrModuleName, 11, "Sorry, Balance Score Card Planning is not planned for selected employee and period.")
            Language.setMessage(mstrModuleName, 12, "Sorry, Balance Score Card Planning is already sent for approval for selected employee and period. .")
            Language.setMessage(mstrModuleName, 13, "You are about to Submit this Balance Score Card Planning for Approval, if you follow this process then this employee won't be able to modify or delete this transaction." & vbCrLf & _
                                                                               "Do you wish to continue?")
            Language.setMessage(mstrModuleName, 14, "You are about to Approve/Disapprove this Balance Score Card Planning, with this process this employee can't be modified or deleted. It can only be modified or deleted if it is disapproved." & vbCrLf & _
                                                                "Do you wish to continue?")
            Language.setMessage(mstrModuleName, 15, "Sorry, Employee and Period are mandatory information to performe Add Operation.")
            Language.setMessage(mstrModuleName, 16, "Sorry, Balance Score Card Planning is yet not submitted for selected employee and period.")
            Language.setMessage(mstrModuleName, 17, "For Period :")
            Language.setMessage(mstrModuleName, 18, "Sorry, Balance Score Card Planning is already approved for selected employee and period. .")
            Language.setMessage(mstrModuleName, 19, "You are about to remove the information from employee level, this will delete all child linked to this parent")
            Language.setMessage(mstrModuleName, 20, "Are you sure you want to delete?")
            Language.setMessage(mstrModuleName, 21, "Sorry, you cannot unlock BSC Planning for the selected period. Reason : Assessment is already done for the selected period")
            Language.setMessage(mstrModuleName, 22, "You are about to unlock the employee goals. This will result in opening of all operation on employee goals.")
            Language.setMessage(mstrModuleName, 23, "Do you wish to continue?")
            Language.setMessage(mstrModuleName, 24, "Enter reason to unlock")
            Language.setMessage(mstrModuleName, 101, "Sorry, Balance Score Card Planning is not yet final approved for selected employee and period.")
            Language.setMessage(mstrModuleName, 26, "Add")
            Language.setMessage(mstrModuleName, 27, "Edit")
            Language.setMessage(mstrModuleName, 28, "Delete")
            Language.setMessage(mstrModuleName, 29, "Total Weight Assigned :")
            Language.setMessage(mstrModuleName, 30, "in order to perform update progress operation.")
            Language.setMessage(mstrModuleName, 31, "Sorry, you cannot unlock Performance Planning for the selected period. Reason : Assessment is already done for the selected period")
            Language.setMessage(mstrModuleName, 32, "Sorry, Pendding data not found in system.")
            Language.setMessage(mstrModuleName, 33, "Mail send Sucussfully.")
            Language.setMessage(mstrModuleName, 34, "Sorry, you cannot do planning as last date for planning has been passed.")
            Language.setMessage(mstrModuleName, 35, "Please contact your administrator/manager to do futher operation on it.")
            Language.setMessage(mstrModuleName, 36, "Progress Update have been submitted for approval.")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>

End Class