﻿#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data

#End Region

Public Class frmGlobalVoidAllEmpGoals

#Region " Private Variable "

    Private ReadOnly mstrModuleName As String = "frmGlobalVoidAllEmpGoals"
    Private objassess_Empfield1Mst As clsassess_empfield1_master
    Private mdtFinalTable As DataTable = Nothing
    Private dvTable As DataView = Nothing
    Private objFMaster As New clsAssess_Field_Master(True)
    Private cmnuDelete As ContextMenuStrip
    Private mstrAdvanceFilter As String = ""
#End Region

#Region " Private Method(s) "

    Private Sub FillCombo()
        Dim objMData As New clsMasterData
        Dim dsList As New DataSet
        Dim objPeriod As New clscommom_period_Tran
        Try
            dsList = objPeriod.getListForCombo(enModuleReference.Assessment, 0, FinancialYear._Object._DatabaseName, FinancialYear._Object._Database_Start_Date, "List", True, enStatusType.Open)
            With cboPeriod
                .ValueMember = "periodunkid"
                .DisplayMember = "name"
                .DataSource = dsList.Tables(0)
            End With

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub FillGrid()
        Dim dsList As DataSet = Nothing
        Try
            dgvData.AutoGenerateColumns = False
            Dim mdtTab As DataTable = Nothing
            dsList = objassess_Empfield1Mst.GetGlobalEmployeeeList(CInt(cboPeriod.SelectedValue), enObjective_Status.OPEN_CHANGES, -1, FinancialYear._Object._DatabaseName, eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate).Date, mstrAdvanceFilter) 'S.SANDEEP |05-MAY-2019| -- START
            mdtTab = dsList.Tables(0).Copy
            dsList = objassess_Empfield1Mst.GetGlobalEmployeeeList(CInt(cboPeriod.SelectedValue), enObjective_Status.NOT_SUBMIT, -1, FinancialYear._Object._DatabaseName, eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate).Date, mstrAdvanceFilter) 'S.SANDEEP |05-MAY-2019| -- START
            mdtTab.Merge(dsList.Tables(0).Copy)
            dsList.Tables.RemoveAt(0) : dsList.Tables.Add(mdtTab.DefaultView.ToTable(True, "ischeck", "employee", "Weight", "employeeunkid"))

            mdtFinalTable = dsList.Tables(0).Copy

            If mdtFinalTable IsNot Nothing Then
                objcolhCheck.DataPropertyName = "ischeck"
                colhEmployee.DataPropertyName = "employee"
                colhWeight.DataPropertyName = "Weight"
                objdgcolhEmpId.DataPropertyName = "employeeunkid"
                dvTable = mdtFinalTable.DefaultView
                dgvData.DataSource = dvTable
            End If
            Call objbtnSearch.ShowResult(CStr(dgvData.RowCount))
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillGrid", mstrModuleName)
        End Try
    End Sub

    Private Sub Generate_MenuItems()
        Dim dsList As New DataSet
        Try
            dsList = objFMaster.Get_Field_Mapping("List", , True)
            If dsList.Tables(0).Rows.Count > 0 Then
                cmnuDelete.Items.Clear()
                For i As Integer = 0 To dsList.Tables(0).Rows.Count - 1
                    Dim objbtnDel As New ToolStripMenuItem
                    objbtnDel.Name = "objbtnDel" & dsList.Tables(0).Rows(i).Item("fieldunkid").ToString
                    objbtnDel.Text = Language.getMessage(mstrModuleName, 6, "Delete") & " " & dsList.Tables(0).Rows(i)("fieldcaption").ToString
                    objbtnDel.Tag = dsList.Tables(0).Rows(i)("fieldunkid").ToString & "|" & dsList.Tables(0).Rows(i)("ExOrder").ToString
                    cmnuDelete.Items.Add(objbtnDel)
                    AddHandler objbtnDel.Click, AddressOf objbtnDel_Click
                Next
            End If
            btnVoid.SplitButtonMenu = cmnuDelete
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Generate_MenuItems", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub VoidCheckedData(ByVal intFieldTypeId As Integer, ByVal strVoidReasion As String, ByVal drow() As DataRow)
        Try
            For iRdx As Integer = 0 To drow.Length - 1
                Select Case intFieldTypeId
                    Case 1  'EMP FIELD 1 UNKID

                        Dim objEmpField1 As New clsassess_empfield1_master
                        If CInt(drow(iRdx).Item("empfield1unkid")) > 0 Then
                            objEmpField1._Isvoid = True
                            objEmpField1._Voiddatetime = ConfigParameter._Object._CurrentDateAndTime
                            objEmpField1._Voidreason = strVoidReasion
                            objEmpField1._Voiduserunkid = User._Object._Userunkid
                            'S.SANDEEP [28-May-2018] -- START
                            'ISSUE/ENHANCEMENT : {Audit Trails} 
                            objEmpField1._FormName = mstrModuleName
                            objEmpField1._Loginemployeeunkid = 0
                            objEmpField1._ClientIP = getIP()
                            objEmpField1._HostName = getHostName()
                            objEmpField1._FromWeb = False
                            objEmpField1._AuditUserId = User._Object._Userunkid
objEmpField1._CompanyUnkid = Company._Object._Companyunkid
                            objEmpField1._AuditDate = ConfigParameter._Object._CurrentDateAndTime
                            'S.SANDEEP [28-May-2018] -- END
                            objEmpField1.Delete(CInt(drow(iRdx).Item("empfield1unkid")))
                        End If
                        objEmpField1 = Nothing

                    Case 2  'EMP FIELD 2 UNKID

                        Dim objEmpField2 As New clsassess_empfield2_master
                        If CInt(drow(iRdx).Item("empfield2unkid")) > 0 Then
                            objEmpField2._Isvoid = True
                            objEmpField2._Voiddatetime = ConfigParameter._Object._CurrentDateAndTime
                            objEmpField2._Voidreason = strVoidReasion
                            objEmpField2._Voiduserunkid = User._Object._Userunkid
                            'S.SANDEEP [28-May-2018] -- START
                            'ISSUE/ENHANCEMENT : {Audit Trails} 
                            objEmpField2._FormName = mstrModuleName
                            objEmpField2._Loginemployeeunkid = 0
                            objEmpField2._ClientIP = getIP()
                            objEmpField2._HostName = getHostName()
                            objEmpField2._FromWeb = False
                            objEmpField2._AuditUserId = User._Object._Userunkid
objEmpField2._CompanyUnkid = Company._Object._Companyunkid
                            objEmpField2._AuditDate = ConfigParameter._Object._CurrentDateAndTime
                            'S.SANDEEP [28-May-2018] -- END
                            objEmpField2.Delete(CInt(drow(iRdx).Item("empfield2unkid")))
                        End If
                        objEmpField2 = Nothing

                    Case 3  'EMP FIELD 3 UNKID

                        Dim objEmpField3 As New clsassess_empfield3_master
                        If CInt(drow(iRdx).Item("empfield3unkid")) > 0 Then
                            objEmpField3._Isvoid = True
                            objEmpField3._Voiddatetime = ConfigParameter._Object._CurrentDateAndTime
                            objEmpField3._Voidreason = strVoidReasion
                            objEmpField3._Voiduserunkid = User._Object._Userunkid
                            'S.SANDEEP [28-May-2018] -- START
                            'ISSUE/ENHANCEMENT : {Audit Trails} 
                            objEmpField3._FormName = mstrModuleName
                            objEmpField3._Loginemployeeunkid = 0
                            objEmpField3._ClientIP = getIP()
                            objEmpField3._HostName = getHostName()
                            objEmpField3._FromWeb = False
                            objEmpField3._AuditUserId = User._Object._Userunkid
objEmpField3._CompanyUnkid = Company._Object._Companyunkid
                            objEmpField3._AuditDate = ConfigParameter._Object._CurrentDateAndTime
                            'S.SANDEEP [28-May-2018] -- END
                            objEmpField3.Delete(CInt(drow(iRdx).Item("empfield3unkid")))
                        End If
                        objEmpField3 = Nothing

                    Case 4  'EMP FIELD 4 UNKID

                        Dim objEmpField4 As New clsassess_empfield4_master
                        If CInt(drow(iRdx).Item("empfield4unkid")) > 0 Then
                            objEmpField4._Isvoid = True
                            objEmpField4._Voiddatetime = ConfigParameter._Object._CurrentDateAndTime
                            objEmpField4._Voidreason = strVoidReasion
                            objEmpField4._Voiduserunkid = User._Object._Userunkid
                            'S.SANDEEP [28-May-2018] -- START
                            'ISSUE/ENHANCEMENT : {Audit Trails} 
                            objEmpField4._FormName = mstrModuleName
                            objEmpField4._Loginemployeeunkid = 0
                            objEmpField4._ClientIP = getIP()
                            objEmpField4._HostName = getHostName()
                            objEmpField4._FromWeb = False
                            objEmpField4._AuditUserId = User._Object._Userunkid
objEmpField4._CompanyUnkid = Company._Object._Companyunkid
                            objEmpField4._AuditDate = ConfigParameter._Object._CurrentDateAndTime
                            'S.SANDEEP [28-May-2018] -- END
                            objEmpField4.Delete(CInt(drow(iRdx).Item("empfield4unkid")))
                        End If
                        objEmpField4 = Nothing

                    Case 5  'EMP FIELD 5 UNKID

                        Dim objEmpField5 As New clsassess_empfield5_master
                        If CInt(drow(iRdx).Item("empfield5unkid")) > 0 Then
                            objEmpField5._Isvoid = True
                            objEmpField5._Voiddatetime = ConfigParameter._Object._CurrentDateAndTime
                            objEmpField5._Voidreason = strVoidReasion
                            objEmpField5._Voiduserunkid = User._Object._Userunkid
                            'S.SANDEEP [28-May-2018] -- START
                            'ISSUE/ENHANCEMENT : {Audit Trails} 
                            objEmpField5._FormName = mstrModuleName
                            objEmpField5._Loginemployeeunkid = 0
                            objEmpField5._ClientIP = getIP()
                            objEmpField5._HostName = getHostName()
                            objEmpField5._FromWeb = False
                            objEmpField5._AuditUserId = User._Object._Userunkid
objEmpField5._CompanyUnkid = Company._Object._Companyunkid
                            objEmpField5._AuditDate = ConfigParameter._Object._CurrentDateAndTime
                            'S.SANDEEP [28-May-2018] -- END
                            objEmpField5.Delete(CInt(drow(iRdx).Item("empfield5unkid")))
                        End If
                        objEmpField5 = Nothing

                End Select
            Next
            Call FillGrid()
            objchkEmployee.Checked = False
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "VoidCheckedData", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

#Region " Form's Event "

    Private Sub frmGlobalVoidAllEmpGoals_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            objassess_Empfield1Mst = New clsassess_empfield1_master
            cmnuDelete = New ContextMenuStrip()

            Call Set_Logo(Me, gApplicationType)
            Language.setLanguage(Me.Name)
            Call OtherSettings()
            Call FillCombo()
            Call Generate_MenuItems()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmGlobalVoidAllEmpGoals_Load", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsassess_empfield1_master.SetMessages()
            objfrm._Other_ModuleNames = "clsassess_empfield1_master"
            objfrm.displayDialog(Me)

            SetMessages()

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

#Region " Button's Event(s) "

    Private Sub objbtnSearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearch.Click
        Try
            If CInt(cboPeriod.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Sorry, Period is mandatory information. Please select Period to continue."), enMsgBoxStyle.Information)
                cboPeriod.Focus()
                Exit Sub
            End If
            Call FillGrid()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearch_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub objbtnReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnReset.Click
        Try
            cboPeriod.SelectedValue = 0
            mstrAdvanceFilter = ""
            Call objbtnSearch.ShowResult(CStr(dgvData.RowCount))
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Me.Close()
    End Sub

#End Region

#Region " Combobox Event(s) "

    Private Sub cboPeriod_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboPeriod.SelectedIndexChanged
        Try
            If CInt(cboPeriod.SelectedValue) > 0 Then
                tblpAssessorEmployee.Enabled = True
            Else
                txtSearchEmp.Text = ""
                objchkEmployee.Checked = False
                dgvData.DataSource = Nothing
                tblpAssessorEmployee.Enabled = False
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboPeriod_SelectedIndexChanged", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

#Region " Textbox Event(s) "

    Private Sub txtSearchEmp_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtSearchEmp.TextChanged
        Try
            Dim strSearch As String = ""
            If txtSearchEmp.Text.Trim.Length > 0 Then
                strSearch = colhEmployee.DataPropertyName & " LIKE '%" & txtSearchEmp.Text & "%'"
            End If
            dvTable.RowFilter = strSearch
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub txtSearchEmp_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtSearchEmp.KeyDown
        Try
            Select Case e.KeyCode
                Case Windows.Forms.Keys.Down
                    If dgvData.Rows.Count > 0 Then
                        If dgvData.SelectedRows(0).Index = dgvData.Rows(dgvData.RowCount - 1).Index Then Exit Sub
                        dgvData.Rows(dgvData.SelectedRows(0).Index + 1).Selected = True
                    End If
                Case Windows.Forms.Keys.Up
                    If dgvData.Rows.Count > 0 Then
                        If dgvData.SelectedRows(0).Index = 0 Then Exit Sub
                        dgvData.Rows(dgvData.SelectedRows(0).Index - 1).Selected = True
                    End If
            End Select
        Catch ex As Exception
            DisplayError.Show(CStr(-1), ex.Message, "txtSearchEmp_KeyDown", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Checkbox Event(s) "

    Private Sub objchkEmployee_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objchkEmployee.CheckedChanged
        Try
            RemoveHandler dgvData.CellContentClick, AddressOf dgvData_CellContentClick
            For Each dr As DataRowView In dvTable
                dr.Item("ischeck") = CBool(objchkEmployee.CheckState)
            Next
            dgvData.Refresh()
            AddHandler dgvData.CellContentClick, AddressOf dgvData_CellContentClick
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objchkEmployee_CheckedChanged", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

#Region " Context Menu Events "

    Private Sub objbtnDel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Dim frm As New frmReasonSelection
        Try
            mdtFinalTable.AcceptChanges()
            Dim dtmp() As DataRow = mdtFinalTable.Select("ischeck=true")
            If dtmp.Length <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "Sorry, Please tick atleast one employee to delete his goal(s)."), enMsgBoxStyle.Information)
                Exit Sub
            End If

            Dim iVoidReason As String = String.Empty

            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If

            Dim iMsg As String = Language.getMessage(mstrModuleName, 7, "You are about to remove the information from employee level, this will delete all child linked to this parent") & vbCrLf & _
                                 Language.getMessage(mstrModuleName, 8, "Are you sure you want to delete?")

            If eZeeMsgBox.Show(iMsg, CType(enMsgBoxStyle.Information + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.No Then Exit Sub

            frm.displayDialog(enVoidCategoryType.ASSESSMENT, iVoidReason)
            If iVoidReason.Length <= 0 Then
                Exit Sub
            End If

            Dim dGoals As New DataTable
            If dtmp.Length > 0 Then
                Me.Enabled = False
                For i As Integer = 0 To dtmp.Length - 1
                    objlnkProcess.Text = "Processing : " & CStr(i + 1) & " / " & dtmp.Length.ToString()
                    Application.DoEvents()
                    dGoals = objassess_Empfield1Mst.GetDisplayList(CInt(dtmp(i)("employeeunkid")), CInt(cboPeriod.SelectedValue), "List", ConfigParameter._Object._CascadingTypeId, False)
                    Dim xrow() = dGoals.Select("")

                    'S.SANDEEP |27-NOV-2020| -- START
                    'ISSUE/ENHANCEMENT : VALID GOAL OPERATION
                    'Dim xMsg As String = ""
                    'xMsg = (New clsassess_empfield1_master).IsGoalsAssignedSubEmployee(CInt(cboPeriod.SelectedValue), CInt(dtmp(i)("employeeunkid")))
                    'If xMsg.Trim.Length > 0 Then
                    '    eZeeMsgBox.Show(xMsg, enMsgBoxStyle.Information)
                    '    Exit Sub
                    'End If
                    'S.SANDEEP |27-NOV-2020| -- END

                    Call VoidCheckedData(CInt(sender.tag.ToString.Split("|")(1)), iVoidReason, xrow)
                Next
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnDel_Click", mstrModuleName)
        Finally
            Me.Enabled = True
            objlnkProcess.Text = ""
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

#End Region

#Region " DataGrid Event(s) "

    Private Sub dgvData_CellContentClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvData.CellContentClick
        Try
            RemoveHandler objchkEmployee.CheckedChanged, AddressOf objchkEmployee_CheckedChanged

            If e.ColumnIndex = objcolhCheck.Index Then

                If Me.dgvData.IsCurrentCellDirty Then
                    Me.dgvData.CommitEdit(DataGridViewDataErrorContexts.Commit)
                End If

                Dim drRow As DataRow() = dvTable.ToTable.Select("ischeck = true", "")
                If drRow.Length > 0 Then
                    If dvTable.ToTable.Rows.Count = drRow.Length Then
                        objchkEmployee.CheckState = CheckState.Checked
                    Else
                        objchkEmployee.CheckState = CheckState.Indeterminate
                    End If
                Else
                    objchkEmployee.CheckState = CheckState.Unchecked
                End If
            ElseIf e.ColumnIndex = objdgcolhPreview.Index Then
                Dim frm As New frmGlobalVoidEmpGoals
                If User._Object._Isrighttoleft = True Then
                    frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                    frm.RightToLeftLayout = True
                    Call Language.ctlRightToLeftlayOut(frm)
                End If
                frm._PeriodId = CInt(cboPeriod.SelectedValue)
                frm._EmployeeId = CInt(dgvData.Rows(e.RowIndex).Cells(objdgcolhEmpId.Index).Value)
                frm._IsPreview = True
                frm.ShowDialog()
            End If

            AddHandler objchkEmployee.CheckedChanged, AddressOf objchkEmployee_CheckedChanged

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgvAEmployee_CellContentClick", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub dgvData_CellMouseEnter(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvData.CellMouseEnter
        Try
            If e.RowIndex <= -1 Then Exit Sub
            Select Case e.ColumnIndex
                Case objdgcolhPreview.Index
                    dgvData.Rows(e.RowIndex).Cells(e.ColumnIndex).ToolTipText = "Preview Goal(s)"
            End Select
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgvData_CellMouseEnter", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

#Region " Link Event(s) "

    Private Sub lnkAdvFilter_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lnkAdvFilter.LinkClicked
        Try
            Dim frm As New frmAdvanceSearch
            frm._Hr_EmployeeTable_Alias = "hremployee_master"
            frm.ShowDialog()
            mstrAdvanceFilter = frm._GetFilterString
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lnkAdvFilter_LinkClicked", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region


    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Private Sub OtherSettings()
        Try
            Me.SuspendLayout()
            
            Call SetLanguage()

            Me.gbFilterCriteria.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbFilterCriteria.ForeColor = GUI._eZeeContainerHeaderForeColor


            Me.btnVoid.GradientBackColor = GUI._ButttonBackColor
            Me.btnVoid.GradientForeColor = GUI._ButttonFontColor

            Me.btnClose.GradientBackColor = GUI._ButttonBackColor
            Me.btnClose.GradientForeColor = GUI._ButttonFontColor


            Me.ResumeLayout()
        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "OtherSettings", mstrModuleName)
        End Try
    End Sub


    Private Sub SetLanguage()
        Try
            Me.Text = Language._Object.getCaption(Me.Name, Me.Text)

            Me.btnVoid.Text = Language._Object.getCaption(Me.btnVoid.Name, Me.btnVoid.Text)
            Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
            Me.lblPeriod.Text = Language._Object.getCaption(Me.lblPeriod.Name, Me.lblPeriod.Text)
            Me.colhEmployee.HeaderText = Language._Object.getCaption(Me.colhEmployee.Name, Me.colhEmployee.HeaderText)
            Me.colhWeight.HeaderText = Language._Object.getCaption(Me.colhWeight.Name, Me.colhWeight.HeaderText)
            Me.DataGridViewTextBoxColumn1.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn1.Name, Me.DataGridViewTextBoxColumn1.HeaderText)
            Me.DataGridViewTextBoxColumn2.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn2.Name, Me.DataGridViewTextBoxColumn2.HeaderText)
            Me.DataGridViewTextBoxColumn3.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn3.Name, Me.DataGridViewTextBoxColumn3.HeaderText)
            Me.lnkAdvFilter.Text = Language._Object.getCaption(Me.lnkAdvFilter.Name, Me.lnkAdvFilter.Text)
            Me.gbFilterCriteria.Text = Language._Object.getCaption(Me.gbFilterCriteria.Name, Me.gbFilterCriteria.Text)

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
        End Try
    End Sub


    Private Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Sorry, Period is mandatory information. Please select Period to continue.")
            Language.setMessage(mstrModuleName, 5, "Sorry, Please tick atleast one employee to delete his goal(s).")
            Language.setMessage(mstrModuleName, 6, "Delete")
            Language.setMessage(mstrModuleName, 7, "You are about to remove the information from employee level, this will delete all child linked to this parent")
            Language.setMessage(mstrModuleName, 8, "Are you sure you want to delete?")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>

End Class