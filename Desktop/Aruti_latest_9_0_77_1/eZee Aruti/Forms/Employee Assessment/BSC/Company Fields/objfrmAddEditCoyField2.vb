﻿Option Strict On

#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data

#End Region

Public Class objfrmAddEditCoyField2

#Region " Private Variables "

    Private ReadOnly mstrModuleName As String = "objfrmAddEditCoyField2"
    Private mblnCancel As Boolean = True
    Private mintCoyField2Unkid As Integer = 0
    Private objCoyField2 As clsassess_coyfield2_master
    Private objCoyOwner As clsassess_coyowner_tran
    Private menAction As enAction = enAction.ADD_ONE
    Private mdtOwner As DataTable
    Private mintFieldUnkid As Integer
    Private objFieldMaster As New clsAssess_Field_Master(True)
    Private mdicFieldData As New Dictionary(Of Integer, String)
    Private objWSetting As New clsWeight_Setting(True)
    Private mintParentId As Integer = 0
    Private mblnDropDownClosed As Boolean = False
    Private mintLinkedFieldId As Integer = -1
    Private mintSelectedPeriodId As Integer = -1
    Private iOwnerRefId As Integer = 0

#End Region

#Region " Display Dialog "

    Public Function displayDialog(ByRef intUnkId As Integer, _
                                  ByVal eAction As enAction, _
                                  ByVal intFieldId As Integer, _
                                  ByVal iPeriodId As Integer, _
                                  Optional ByVal iParentId As Integer = 0) As Boolean
        Try
            mintCoyField2Unkid = intUnkId
            mintFieldUnkid = intFieldId
            mintParentId = iParentId
            mintSelectedPeriodId = iPeriodId

            menAction = eAction

            objtabcRemarks.Enabled = False
            objpnlData.Enabled = False

            Me.ShowDialog()

            intUnkId = mintCoyField2Unkid

            Return Not mblnCancel
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "displayDialog", mstrModuleName)
        End Try
    End Function

#End Region

#Region " Private Methods "

    Private Sub Set_Form_Information()
        Try
            Me.Text = Language.getMessage(mstrModuleName, 10, "Add/Edit Company") & " " & objFieldMaster._Field2_Caption & " " & _
                      Language.getMessage(mstrModuleName, 11, "Information")

            objlblField1.Text = objFieldMaster._Field1_Caption
            txtFieldValue2.Tag = objFieldMaster._Field1Unkid

            objlblField2.Text = objFieldMaster._Field2_Caption
            txtFieldValue2.Tag = objFieldMaster._Field2Unkid

            If mintFieldUnkid = mintLinkedFieldId Then
                If objFieldMaster._Field6_Caption = "" Then
                    objtabcRemarks.TabPages.Remove(objtabpRemark1)
                Else
                    objtabpRemark1.Text = objFieldMaster._Field6_Caption
                    txtRemark1.Tag = objFieldMaster._Field6Unkid
                    If mdicFieldData.ContainsKey(objFieldMaster._Field6Unkid) = False Then
                        mdicFieldData.Add(objFieldMaster._Field6Unkid, "")
                    End If
                End If

                If objFieldMaster._Field7_Caption = "" Then
                    objtabcRemarks.TabPages.Remove(objtabpRemark2)
                Else
                    objtabpRemark2.Text = objFieldMaster._Field7_Caption
                    txtRemark2.Tag = objFieldMaster._Field7Unkid
                    If mdicFieldData.ContainsKey(objFieldMaster._Field7Unkid) = False Then
                        mdicFieldData.Add(objFieldMaster._Field7Unkid, "")
                    End If
                End If

                If objFieldMaster._Field8_Caption = "" Then
                    objtabcRemarks.TabPages.Remove(objtabpRemark3)
                Else
                    objtabpRemark3.Text = objFieldMaster._Field8_Caption
                    txtRemark3.Tag = objFieldMaster._Field8Unkid
                    If mdicFieldData.ContainsKey(objFieldMaster._Field8Unkid) = False Then
                        mdicFieldData.Add(objFieldMaster._Field8Unkid, "")
                    End If
                End If
                If mdicFieldData.Keys.Count > 0 Then objtabcRemarks.Enabled = True
            Else
                objtabcRemarks.Enabled = False : objpnlData.Enabled = False
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Set_Form_Information", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub FillCombo()
        Dim objMData As New clsMasterData
        Dim objCoyField1 As New clsassess_coyfield1_master
        Dim dsList As New DataSet
        Try
            RemoveHandler cboFieldValue1.SelectedIndexChanged, AddressOf cboFieldValue1_SelectedIndexChanged
            dsList = objCoyField1.getComboList(mintSelectedPeriodId, "List", True)
            With cboFieldValue1
                .ValueMember = "Id"
                .DisplayMember = "Name"
                .DataSource = dsList.Tables(0)
                .SelectedValue = 0
            End With
            cboFieldValue1.DrawMode = DrawMode.OwnerDrawFixed
            AddHandler cboFieldValue1.DrawItem, AddressOf cboFieldValue1_DrawItem
            AddHandler cboFieldValue1.DropDownClosed, AddressOf cboFieldValue1_DropDownClosed
            AddHandler cboFieldValue1.SelectedIndexChanged, AddressOf cboFieldValue1_SelectedIndexChanged

            dsList = objMData.GetEAllocation_Notification("List")
            Dim dtTable As DataTable = New DataView(dsList.Tables(0), "Id NOT IN(" & enAllocation.JOB_GROUP & "," & enAllocation.JOBS & "," & enAllocation.COST_CENTER & ")", "", DataViewRowState.CurrentRows).ToTable
            With cboAllocations
                .ValueMember = "Id"
                .DisplayMember = "Name"
                .DataSource = dtTable
                .SelectedValue = 1
            End With

            dsList = objMData.Get_CompanyGoal_Status("List", True)
            With cboStatus
                .ValueMember = "Id"
                .DisplayMember = "Name"
                .DataSource = dsList.Tables("List")
                .SelectedValue = enCompGoalStatus.ST_PENDING
            End With

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub SetColor()
        Try
            txtFieldValue2.BackColor = GUI.ColorComp
            cboAllocations.BackColor = GUI.ColorComp
            cboStatus.BackColor = GUI.ColorComp
            txtWeight.BackColor = GUI.ColorComp
            cboFieldValue1.BackColor = GUI.ColorComp
            txtPeriod.BackColor = GUI.ColorComp
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetColor", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub SetVisibility()
        Try

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetVisibility", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub Fill_Data()
        Dim dList As New DataSet
        Try
            Select Case CInt(cboAllocations.SelectedValue)
                Case enAllocation.BRANCH
                    Dim objBranch As New clsStation
                    dList = objBranch.GetList("List")
                    Call Fill_List(dList.Tables(0), "stationunkid", "name")
                Case enAllocation.DEPARTMENT_GROUP
                    Dim objDeptGrp As New clsDepartmentGroup
                    dList = objDeptGrp.GetList("List")
                    Call Fill_List(dList.Tables(0), "deptgroupunkid", "name")
                Case enAllocation.DEPARTMENT
                    Dim objDept As New clsDepartment
                    dList = objDept.GetList("List")
                    Call Fill_List(dList.Tables(0), "departmentunkid", "name")
                Case enAllocation.SECTION_GROUP
                    Dim objSecGrp As New clsSectionGroup
                    dList = objSecGrp.GetList("List")
                    Call Fill_List(dList.Tables(0), "sectiongroupunkid", "name")
                Case enAllocation.SECTION
                    Dim objSec As New clsSections
                    dList = objSec.GetList("List")
                    Call Fill_List(dList.Tables(0), "sectionunkid", "name")
                Case enAllocation.UNIT_GROUP
                    Dim objUnitGrp As New clsUnitGroup
                    dList = objUnitGrp.GetList("List")
                    Call Fill_List(dList.Tables(0), "unitgroupunkid", "name")
                Case enAllocation.UNIT
                    Dim objUnit As New clsUnits
                    dList = objUnit.GetList("List")
                    Call Fill_List(dList.Tables(0), "unitunkid", "name")
                Case enAllocation.TEAM
                    Dim objTeam As New clsTeams
                    dList = objTeam.GetList("List")
                    Call Fill_List(dList.Tables(0), "teamunkid", "name")
                Case enAllocation.JOB_GROUP
                    Dim objJobGrp As New clsJobGroup
                    dList = objJobGrp.GetList("List")
                    Call Fill_List(dList.Tables(0), "jobgroupunkid", "name")
                Case enAllocation.JOBS
                    Dim objJob As New clsJobs
                    dList = objJob.GetList("List")
                    Call Fill_List(dList.Tables(0), "jobunkid", "JobName")
                Case enAllocation.CLASS_GROUP
                    Dim objClsGrp As New clsClassGroup
                    dList = objClsGrp.GetList("List")
                    Call Fill_List(dList.Tables(0), "classgroupunkid", "name")
                Case enAllocation.CLASSES
                    Dim objCls As New clsClass
                    dList = objCls.GetList("List")
                    Call Fill_List(dList.Tables(0), "classesunkid", "name")
            End Select
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "Fill_List", mstrModuleName)
        End Try
    End Sub

    Private Sub Fill_List(ByVal dTable As DataTable, ByVal StrIdColName As String, ByVal StrDisColName As String)
        Try
            RemoveHandler objchkAll.CheckedChanged, AddressOf objchkAll_CheckedChanged
            RemoveHandler lvAllocation.ItemChecked, AddressOf lvAllocation_ItemChecked
            lvAllocation.Items.Clear()
            For Each dtRow As DataRow In dTable.Rows
                Dim lvItem As New ListViewItem

                lvItem.Text = dtRow.Item(StrDisColName).ToString
                lvItem.Tag = dtRow.Item(StrIdColName)
                If mdtOwner.Rows.Count > 0 Then
                    Dim dRow As DataRow() = mdtOwner.Select("allocationid = '" & CInt(dtRow.Item(StrIdColName)) & "' AND AUD <> 'D'")
                    If dRow.Length > 0 Then
                        lvItem.Checked = True
                    End If
                End If
                lvAllocation.Items.Add(lvItem)
            Next
            If lvAllocation.Items.Count > 7 Then
                objcolhAllocations.Width = 235 - 35
            Else
                objcolhAllocations.Width = 235
            End If
            AddHandler objchkAll.CheckedChanged, AddressOf objchkAll_CheckedChanged
            AddHandler lvAllocation.ItemChecked, AddressOf lvAllocation_ItemChecked
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Fill_List", mstrModuleName)
        End Try
    End Sub

    Private Sub GoalOwnerOperation(ByVal iTagUnkid As Integer, ByVal iFlag As Boolean)
        Try
            If mdtOwner IsNot Nothing Then
                Dim dtmp() As DataRow = mdtOwner.Select("allocationid = '" & iTagUnkid & "'")
                If dtmp.Length > 0 Then
                    If iFlag = False Then
                        dtmp(0).Item("AUD") = "D"
                    End If
                Else
                    If iFlag = True Then
                        Dim dRow As DataRow = mdtOwner.NewRow
                        dRow.Item("ownertranunkid") = -1
                        dRow.Item("coyfieldunkid") = mintCoyField2Unkid
                        dRow.Item("allocationid") = iTagUnkid
                        dRow.Item("coyfieldtypeid") = enWeight_Types.WEIGHT_FIELD2
                        dRow.Item("AUD") = "A"
                        dRow.Item("GUID") = Guid.NewGuid.ToString
                        mdtOwner.Rows.Add(dRow)
                    End If
                    mdtOwner.AcceptChanges()
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GoalOwnerOperation", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Function IsValidData() As Boolean
        Try
            Dim iMsg As String = String.Empty

            'If ConfigParameter._Object._CascadingTypeId = enPACascading.STRICT_CASCADING Then
            If CInt(cboFieldValue1.SelectedValue) <= 0 Then
                iMsg = Language.getMessage(mstrModuleName, 1, "Sorry, ") & objFieldMaster._Field1_Caption & _
                       Language.getMessage(mstrModuleName, 2, " is mandatory information. Please select ") & objFieldMaster._Field1_Caption & _
                       Language.getMessage(mstrModuleName, 3, " to continue.")

                eZeeMsgBox.Show(iMsg, enMsgBoxStyle.Information)
                cboFieldValue1.Focus()
                Return False
            End If
            'End If

            If txtFieldValue2.Text.Trim.Length <= 0 Then
                iMsg = Language.getMessage(mstrModuleName, 1, "Sorry, ") & objFieldMaster._Field2_Caption & _
                       Language.getMessage(mstrModuleName, 4, " is mandatory information. Please provide ") & objFieldMaster._Field2_Caption & _
                       Language.getMessage(mstrModuleName, 3, " to continue.")

                eZeeMsgBox.Show(iMsg, enMsgBoxStyle.Information)
                txtFieldValue2.Focus()
                Return False
            End If

            If mintFieldUnkid = mintLinkedFieldId Then

                If txtWeight.Decimal <= 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 9, "Sorry, Weight is mandatory information. Please provide Weight to continue."), enMsgBoxStyle.Information)
                    txtWeight.Focus()
                    Return False
                End If

                If txtWeight.Decimal > 0 Then
                    Dim objMapping As New clsAssess_Field_Mapping
                    iMsg = objMapping.Is_Valid_Weight(clsAssess_Field_Mapping.enWeightCheckType.CKT_COMPANY_LEVEL, enWeight_Types.WEIGHT_FIELD2, txtWeight.Decimal, mintSelectedPeriodId, mintLinkedFieldId, 0, 0, menAction, mintCoyField2Unkid)
                    objMapping = Nothing
                    If iMsg.Trim.Length > 0 Then
                        eZeeMsgBox.Show(iMsg, enMsgBoxStyle.Information)
                        txtWeight.Focus()
                        Return False
                    End If
                End If

                'If txtWeight.Decimal > 100 Then
                '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 12, "Sorry, Weight cannot exceed 100. Please provide Weight between 1 to 100."), enMsgBoxStyle.Information)
                '    txtWeight.Focus()
                '    Return False
                'End If

                If CInt(cboStatus.SelectedValue) <= 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "Sorry, Status is mandatory information. Please select Status to continue."), enMsgBoxStyle.Information)
                    cboStatus.Focus()
                    Return False
                End If

                If lvAllocation.CheckedItems.Count <= 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 6, "Sorry, Goal Owner mandatory information. Please check atleast one Goal Owner to continue."), enMsgBoxStyle.Information)
                    lvAllocation.Focus()
                    Return False
                End If
            End If

            Return True

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "IsValidData", mstrModuleName)
        Finally
        End Try
    End Function

    Private Sub SetValue()
        Try
            objCoyField2._Coyfield1unkid = CInt(cboFieldValue1.SelectedValue)
            If dtpEndDate.Checked = True Then
                objCoyField2._Enddate = dtpEndDate.Value
            Else
                objCoyField2._Enddate = Nothing
            End If
            objCoyField2._Field_Data = txtFieldValue2.Text
            objCoyField2._Fieldunkid = mintFieldUnkid
            objCoyField2._Isvoid = False

            objCoyField2._Periodunkid = mintSelectedPeriodId
            If dtpStartDate.Checked = True Then
                objCoyField2._Startdate = dtpStartDate.Value
            Else
                objCoyField2._Startdate = Nothing
            End If
            objCoyField2._Userunkid = User._Object._Userunkid
            objCoyField2._Voiddatetime = Nothing
            objCoyField2._Voidreason = ""
            objCoyField2._Voiduserunkid = 0
            objCoyField2._Weight = txtWeight.Decimal
            objCoyField2._CoyFieldTypeId = enWeight_Types.WEIGHT_FIELD2
            If mintFieldUnkid = mintLinkedFieldId Then
                objCoyField2._Ownerrefid = CInt(cboAllocations.SelectedValue)
                objCoyField2._Statusunkid = CInt(cboStatus.SelectedValue)
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetValue", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub GetValue()
        Try
            If objCoyField2._Enddate <> Nothing Then
                dtpEndDate.Value = objCoyField2._Enddate
                dtpEndDate.Checked = True
            End If
            txtFieldValue2.Text = objCoyField2._Field_Data
            cboAllocations.SelectedValue = IIf(objCoyField2._Ownerrefid <= 0, 1, objCoyField2._Ownerrefid)
            cboFieldValue1.SelectedValue = objCoyField2._Coyfield1unkid
            If objCoyField2._Startdate <> Nothing Then
                dtpStartDate.Value = objCoyField2._Startdate
                dtpStartDate.Checked = True
            End If
            cboStatus.SelectedValue = IIf(objCoyField2._Statusunkid <= 0, enCompGoalStatus.ST_PENDING, objCoyField2._Statusunkid)
            txtWeight.Decimal = CDec(objCoyField2._Weight)
            If menAction = enAction.EDIT_ONE Then
                Dim objCoyInfoField As New clsassess_coyinfofield_tran
                mdicFieldData = objCoyInfoField.Get_Data(mintCoyField2Unkid, enWeight_Types.WEIGHT_FIELD2)
                If mdicFieldData.Keys.Count > 0 Then
                    If mdicFieldData.ContainsKey(CInt(txtRemark1.Tag)) Then
                        txtRemark1.Text = mdicFieldData(CInt(txtRemark1.Tag))
                    End If
                    If mdicFieldData.ContainsKey(CInt(txtRemark2.Tag)) Then
                        txtRemark2.Text = mdicFieldData(CInt(txtRemark2.Tag))
                    End If
                    If mdicFieldData.ContainsKey(CInt(txtRemark3.Tag)) Then
                        txtRemark3.Text = mdicFieldData(CInt(txtRemark3.Tag))
                    End If
                End If
                objCoyInfoField = Nothing
            End If


            iOwnerRefId = objCoyField2.GetOwnerRefId
            If iOwnerRefId > 0 Then
                cboAllocations.SelectedValue = iOwnerRefId : cboAllocations.Enabled = False
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetValue", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

#Region " Form's Events "

    Private Sub frmAddEditCoyField2_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        objCoyField2 = New clsassess_coyfield2_master
        objCoyOwner = New clsassess_coyowner_tran
        Try
            Call Set_Logo(Me, gApplicationType)
            Language.setLanguage(Me.Name)
            Call OtherSettings()
            Call SetVisibility()
            Call SetColor()
            Call Set_Form_Information()
            If menAction = enAction.EDIT_ONE Then
                objCoyField2._Coyfield2unkid = mintCoyField2Unkid
                cboAllocations.Enabled = False
                'cboFieldValue1.Enabled = False : objbtnSearchField1.Enabled = False
            End If
            mdtOwner = objCoyOwner.Get_Data(mintCoyField2Unkid, enWeight_Types.WEIGHT_FIELD2)
            Call FillCombo()
            Call Fill_Data()
            Call GetValue()
            If mintParentId > 0 Then
                cboFieldValue1.SelectedValue = mintParentId
                'cboFieldValue1.Enabled = False : objbtnSearchField1.Enabled = False
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmAddEditCoyField2_Load", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub frmAddEditCoyField2_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        Try
            objCoyField2 = Nothing : objCoyOwner = Nothing
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmAddEditCoyField2_FormClosed", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsassess_coyfield2_master.SetMessages()
            objfrm._Other_ModuleNames = "clsassess_coyfield2_master"
            objfrm.displayDialog(Me)

            Call SetLanguage()

            Me.Text = Language.getMessage(mstrModuleName, 10, "Add/Edit Company") & " " & objFieldMaster._Field2_Caption & " " & _
                     Language.getMessage(mstrModuleName, 11, "Information")

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub

#End Region

#Region " Button's Events "

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            mblnCancel = False
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnClose_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Dim iblnFlag As Boolean = False
        Try
            If IsValidData() = False Then Exit Sub
            Call SetValue()
            With objCoyField2
                ._FormName = mstrModuleName
                ._LoginEmployeeunkid = 0
                ._ClientIP = getIP()
                ._HostName = getHostName()
                ._FromWeb = False
                ._AuditUserId = User._Object._Userunkid
                ._AuditDate = ConfigParameter._Object._CurrentDateAndTime
            End With
            If menAction = enAction.EDIT_ONE Then
                iblnFlag = objCoyField2.Update(mdtOwner, mdicFieldData)
            Else
                iblnFlag = objCoyField2.Insert(mdtOwner, mdicFieldData)
            End If
            If iblnFlag = False Then
                If objCoyField2._Message <> "" Then
                    eZeeMsgBox.Show(objCoyField2._Message, enMsgBoxStyle.Information)
                Else
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 7, "Sorry, problem in saving Company Goals."), enMsgBoxStyle.Information)
                End If
            Else
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 8, "Company Goals are saved successfully."), enMsgBoxStyle.Information)
                If menAction = enAction.ADD_CONTINUE Then
                    objCoyField2 = New clsassess_coyfield2_master
                    objCoyOwner = New clsassess_coyowner_tran
                    mdtOwner.Rows.Clear()
                    objchkAll.Checked = False
                    RemoveHandler cboFieldValue1.SelectedIndexChanged, AddressOf cboFieldValue1_SelectedIndexChanged
                    Call GetValue()
                    txtRemark1.Text = "" : txtRemark2.Text = "" : txtRemark3.Text = ""
                    If mintParentId > 0 Then
                        cboFieldValue1.SelectedValue = mintParentId
                    End If
                    AddHandler cboFieldValue1.SelectedIndexChanged, AddressOf cboFieldValue1_SelectedIndexChanged
                Else
                    Call btnClose_Click(sender, e)
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnSave_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub objbtnSearchField1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchField1.Click
        Dim frm As New frmCommonSearch
        Try
            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If

            With frm
                .ValueMember = cboFieldValue1.ValueMember
                .DisplayMember = cboFieldValue1.DisplayMember
                .DataSource = CType(cboFieldValue1.DataSource, DataTable)
                If .DisplayDialog Then
                    cboFieldValue1.SelectedValue = .SelectedValue
                End If
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchField1_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

#End Region

#Region " Controls Events "

    Private Sub cboAllocations_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboAllocations.SelectedIndexChanged
        Try
            objcolhAllocations.Text = cboAllocations.Text
            Call Fill_Data()
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "cboAllocations_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub txtSearch_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtSearch.TextChanged
        Try
            If lvAllocation.Items.Count <= 0 Then Exit Sub
            lvAllocation.SelectedIndices.Clear()
            Dim lvFoundItem As ListViewItem = lvAllocation.FindItemWithText(txtSearch.Text, True, 0, True)
            If lvFoundItem IsNot Nothing Then
                lvAllocation.TopItem = lvFoundItem
                lvFoundItem.Selected = True
                lvFoundItem.EnsureVisible()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "txtSearch_TextChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub objchkAll_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objchkAll.CheckedChanged
        Try
            RemoveHandler lvAllocation.ItemChecked, AddressOf lvAllocation_ItemChecked
            For Each LItem As ListViewItem In lvAllocation.Items
                LItem.Checked = objchkAll.Checked
                Call GoalOwnerOperation(CInt(LItem.Tag), LItem.Checked)
            Next
            If iOwnerRefId <= 0 Then
                If lvAllocation.CheckedItems.Count <= 0 Then
                    cboAllocations.Enabled = True
                Else
                    cboAllocations.Enabled = False
                End If
            End If
            AddHandler lvAllocation.ItemChecked, AddressOf lvAllocation_ItemChecked
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objchkAll_CheckedChanged", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub lvAllocation_ItemChecked(ByVal sender As Object, ByVal e As System.Windows.Forms.ItemCheckedEventArgs) Handles lvAllocation.ItemChecked
        Try
            RemoveHandler objchkAll.CheckedChanged, AddressOf objchkAll_CheckedChanged
            If lvAllocation.CheckedItems.Count <= 0 Then
                cboAllocations.Enabled = True
                objchkAll.CheckState = CheckState.Unchecked
            ElseIf lvAllocation.CheckedItems.Count < lvAllocation.Items.Count Then
                cboAllocations.Enabled = False
                objchkAll.CheckState = CheckState.Indeterminate
            ElseIf lvAllocation.CheckedItems.Count = lvAllocation.Items.Count Then
                cboAllocations.Enabled = False
                objchkAll.CheckState = CheckState.Checked
            End If
            Call GoalOwnerOperation(CInt(e.Item.Tag), e.Item.Checked)
            AddHandler objchkAll.CheckedChanged, AddressOf objchkAll_CheckedChanged
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lvAllocation_ItemChecked", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub txtRemark1_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtRemark1.TextChanged
        Try
            mdicFieldData(CInt(txtRemark1.Tag)) = txtRemark1.Text
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "txtRemark1_TextChanged", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub txtRemark2_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtRemark2.TextChanged
        Try
            mdicFieldData(CInt(txtRemark2.Tag)) = txtRemark2.Text
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "txtRemark2_TextChanged", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub txtRemark3_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtRemark3.TextChanged
        Try
            mdicFieldData(CInt(txtRemark3.Tag)) = txtRemark3.Text
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "txtRemark3_TextChanged", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub cboFieldValue1_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboFieldValue1.SelectedIndexChanged
        Try
            mintParentId = CInt(cboFieldValue1.SelectedValue)
            Dim objCoyField1 As New clsassess_coyfield1_master : Dim objMapping As New clsAssess_Field_Mapping : Dim objPrd As New clscommom_period_Tran
            objCoyField1._Coyfield1unkid = CInt(cboFieldValue1.SelectedValue)
            mintLinkedFieldId = objMapping.Get_Map_FieldId(CInt(objCoyField1._Periodunkid))
            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'objPrd._Periodunkid = CInt(objCoyField1._Periodunkid)
            objPrd._Periodunkid(FinancialYear._Object._DatabaseName) = CInt(objCoyField1._Periodunkid)
            'Sohail (21 Aug 2015) -- End
            txtPeriod.Text = objPrd._Period_Name
            If mintLinkedFieldId = mintFieldUnkid Then
                objpnlData.Enabled = True
                Call Set_Form_Information()
            Else
                objtabcRemarks.Enabled = False
                objpnlData.Enabled = False
            End If
            objMapping = Nothing : objCoyField1 = Nothing
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub cboFieldValue1_DropDown(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboFieldValue1.DropDown
        Try
            mblnDropDownClosed = False
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboFieldValue1_DropDown", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub cboFieldValue1_DropDownClosed(ByVal sender As Object, ByVal e As EventArgs)
        Try
            ToolTip1.Hide(cboFieldValue1) : mblnDropDownClosed = True
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboFieldValue1_DropDownClosed", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub cboFieldValue1_DrawItem(ByVal sender As Object, ByVal e As DrawItemEventArgs)
        Try
            If e.Index < 0 Then
                Return
            End If
            Dim text As String = cboFieldValue1.GetItemText(cboFieldValue1.Items(e.Index))
            e.DrawBackground()
            Using br As New SolidBrush(e.ForeColor)
                e.Graphics.DrawString(text, e.Font, br, e.Bounds)
            End Using
            If (e.State And DrawItemState.Selected) = DrawItemState.Selected Then
                If mblnDropDownClosed = False Then
                    ToolTip1.Show(text, cboFieldValue1, e.Bounds.Right, e.Bounds.Bottom)
                End If
            End If
            e.DrawFocusRectangle()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboFieldValue1_DrawItem", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Private Sub OtherSettings()
        Try
            Me.SuspendLayout()

            Call SetLanguage()

            Me.btnSave.GradientBackColor = GUI._ButttonBackColor
            Me.btnSave.GradientForeColor = GUI._ButttonFontColor

            Me.btnClose.GradientBackColor = GUI._ButttonBackColor
            Me.btnClose.GradientForeColor = GUI._ButttonFontColor


            Me.ResumeLayout()
        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "OtherSettings", mstrModuleName)
        End Try
    End Sub


    Private Sub SetLanguage()
        Try
            Me.Text = Language._Object.getCaption(Me.Name, Me.Text)

            Me.lblEndDate.Text = Language._Object.getCaption(Me.lblEndDate.Name, Me.lblEndDate.Text)
            Me.lblStartDate.Text = Language._Object.getCaption(Me.lblStartDate.Name, Me.lblStartDate.Text)
            Me.lblStatus.Text = Language._Object.getCaption(Me.lblStatus.Name, Me.lblStatus.Text)
            Me.lblGoalOwner.Text = Language._Object.getCaption(Me.lblGoalOwner.Name, Me.lblGoalOwner.Text)
            Me.lblWeight.Text = Language._Object.getCaption(Me.lblWeight.Name, Me.lblWeight.Text)
            Me.btnSave.Text = Language._Object.getCaption(Me.btnSave.Name, Me.btnSave.Text)
            Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
        End Try
    End Sub


    Private Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Sorry,")
            Language.setMessage(mstrModuleName, 2, " is mandatory information. Please select")
            Language.setMessage(mstrModuleName, 3, " to continue.")
            Language.setMessage(mstrModuleName, 4, " is mandatory information. Please provide")
            Language.setMessage(mstrModuleName, 5, "Sorry, Status is mandatory information. Please select Status to continue.")
            Language.setMessage(mstrModuleName, 6, "Sorry, Goal Owner mandatory information. Please check atleast one Goal Owner to continue.")
            Language.setMessage(mstrModuleName, 7, "Sorry, problem in saving Company Goals.")
            Language.setMessage(mstrModuleName, 8, "Company Goals are saved successfully.")
            Language.setMessage(mstrModuleName, 9, "Sorry, Weight is mandatory information. Please provide Weight to continue.")
            Language.setMessage(mstrModuleName, 10, "Add/Edit Company")
            Language.setMessage(mstrModuleName, 11, "Information")
            Language.setMessage(mstrModuleName, 12, "Sorry, Weight cannot exceed 100. Please provide Weight between 1 to 100.")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class