﻿#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data

#End Region

Public Class frmCoyFieldsList

#Region " Private Variable "

    Private ReadOnly mstrModuleName As String = "frmCoyFieldsList"
    Private mdtFinal As DataTable = Nothing
    Private objCoyField1 As clsassess_coyfield1_master
    Private objCoyField2 As clsassess_coyfield2_master
    Private objCoyField3 As clsassess_coyfield3_master
    Private objCoyField4 As clsassess_coyfield4_master
    Private objCoyField5 As clsassess_coyfield5_master
    Private cmnuAdd, cmnuEdit, cmnuDelete As ContextMenuStrip
    Private objFMaster As New clsAssess_Field_Master(True)
    Private mblnDropDownClosed As Boolean = False

#End Region

#Region " Private Methods "

    Private Sub SetFormViewSetting()
        Try
            objlblField1.Text = objFMaster._Field1_Caption
            cboFieldValue1.Tag = objFMaster._Field1Unkid
            cboFieldValue1.DrawMode = DrawMode.OwnerDrawFixed
            If objlblField1.Text = "" Then
                cboFieldValue1.Enabled = False : objbtnSearchField1.Enabled = False
            End If
            objlblField2.Text = objFMaster._Field2_Caption
            cboFieldValue2.Tag = objFMaster._Field2Unkid
            cboFieldValue2.DrawMode = DrawMode.OwnerDrawFixed
            If objlblField2.Text = "" Then
                cboFieldValue2.Enabled = False : objbtnSearchField2.Enabled = False
            End If
            objlblField3.Text = objFMaster._Field3_Caption
            cboFieldValue3.Tag = objFMaster._Field3Unkid
            cboFieldValue3.DrawMode = DrawMode.OwnerDrawFixed
            If objlblField3.Text = "" Then
                cboFieldValue3.Enabled = False : objbtnSearchField3.Enabled = False
            End If
            objlblField4.Text = objFMaster._Field4_Caption
            cboFieldValue4.Tag = objFMaster._Field4Unkid
            cboFieldValue4.DrawMode = DrawMode.OwnerDrawFixed
            If objlblField4.Text = "" Then
                cboFieldValue4.Enabled = False : objbtnSearchField4.Enabled = False
            End If
            objlblField5.Text = objFMaster._Field5_Caption
            cboFieldValue5.Tag = objFMaster._Field5Unkid
            cboFieldValue5.DrawMode = DrawMode.OwnerDrawFixed
            If objlblField5.Text = "" Then
                cboFieldValue5.Enabled = False : objbtnSearchField5.Enabled = False
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetFormViewSetting", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub FillCombo()
        Dim objMData As New clsMasterData
        Dim dsList As New DataSet
        Dim objPeriod As New clscommom_period_Tran
        Try
            'S.SANDEEP [ 05 NOV 2014 ] -- START
            'dsList = objMData.Get_BSC_Perspective("List", True)
            Dim objPerspective As New clsassess_perspective_master
            dsList = objPerspective.getComboList("List", True)
            objPerspective = Nothing
            'S.SANDEEP [ 05 NOV 2014 ] -- END
            With cboPerspective
                .ValueMember = "Id"
                .DisplayMember = "Name"
                .DataSource = dsList.Tables("List")
                .SelectedValue = 0
            End With

            dsList = objMData.Get_CompanyGoal_Status("List", True)
            With cboStatus
                .ValueMember = "Id"
                .DisplayMember = "Name"
                .DataSource = dsList.Tables("List")
                .SelectedValue = 0
            End With

            dsList = objMData.GetEAllocation_Notification("List")
            Dim dtTable As DataTable = Nothing
            dtTable = New DataView(dsList.Tables(0), "Id NOT IN(" & enAllocation.JOB_GROUP & "," & enAllocation.JOBS & "," & enAllocation.COST_CENTER & ")", "", DataViewRowState.CurrentRows).ToTable
            Dim iRow As DataRow = dtTable.NewRow
            iRow.Item("Id") = 0
            iRow.Item("Name") = Language.getMessage(mstrModuleName, 26, "Select")
            dtTable.Rows.InsertAt(iRow, 0)
            With cboAllocations
                .ValueMember = "Id"
                .DisplayMember = "Name"
                .DataSource = dtTable
                .SelectedValue = 0
            End With

            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'dsList = objPeriod.getListForCombo(enModuleReference.Assessment, 0, "List", True, enStatusType.Open)
            dsList = objPeriod.getListForCombo(enModuleReference.Assessment, 0, FinancialYear._Object._DatabaseName, FinancialYear._Object._Database_Start_Date, "List", True, enStatusType.Open)
            'Sohail (21 Aug 2015) -- End
            With cboPeriod
                .ValueMember = "periodunkid"
                .DisplayMember = "name"
                .DataSource = dsList.Tables(0)
            End With

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub Fill_Grid()
        Dim dtFinal As DataTable
        Dim iSearch As String = String.Empty
        Try
            If CInt(cboPeriod.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 25, "Period is mandatory information. Please select Period to continue."), enMsgBoxStyle.Information)
                cboPeriod.Focus()
                Exit Sub
            End If

            If objFMaster.IsPresent(clsAssess_Field_Master.enFieldCheckType.FIELD_DATA) = False Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 8, "Sorry, No data defined for asseessment caption settings screen."), enMsgBoxStyle.Information)
                Exit Sub
            End If

            Dim objMap As New clsAssess_Field_Mapping
            If objMap.isExist(CInt(cboPeriod.SelectedValue)) = False Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 9, "Sorry no field is mapped with the selected period."), enMsgBoxStyle.Information)
                Exit Sub
            End If
            objMap = Nothing


            'S.SANDEEP [28 MAY 2015] -- START
            'ENHANCEMENT : NEW PRIVILEGE (Performance Assessment)
            If User._Object.Privilege._AllowtoViewCompanyGoalsList = False Then Exit Sub
            'S.SANDEEP [28 MAY 2015] -- END

            dtFinal = objCoyField1.GetDisplayList(CInt(cboPeriod.SelectedValue), "List")
            Dim blnFlag As Boolean = False
            If dtFinal.Rows.Count <= 0 Then
                Dim iRow As DataRow = dtFinal.NewRow
                dtFinal.Rows.Add(iRow)
                blnFlag = True
            End If

            If blnFlag = False Then
                If CInt(cboPerspective.SelectedValue) > 0 Then
                    iSearch &= "AND perspectiveunkid = '" & CInt(cboPerspective.SelectedValue) & "' "
                End If

                If cboFieldValue1.Enabled = True AndAlso CInt(cboFieldValue1.SelectedValue) > 0 Then
                    iSearch &= "AND coyfield1unkid = '" & CInt(cboFieldValue1.SelectedValue) & "' "
                End If

                If cboFieldValue2.Enabled = True AndAlso CInt(cboFieldValue2.SelectedValue) > 0 Then
                    iSearch &= "AND coyfield2unkid = '" & CInt(cboFieldValue2.SelectedValue) & "' "
                End If

                If cboFieldValue3.Enabled = True AndAlso CInt(cboFieldValue3.SelectedValue) > 0 Then
                    iSearch &= "AND coyfield3unkid = '" & CInt(cboFieldValue3.SelectedValue) & "' "
                End If

                If cboFieldValue4.Enabled = True AndAlso CInt(cboFieldValue4.SelectedValue) > 0 Then
                    iSearch &= "AND coyfield4unkid = '" & CInt(cboFieldValue4.SelectedValue) & "' "
                End If

                If cboFieldValue5.Enabled = True AndAlso CInt(cboFieldValue5.SelectedValue) > 0 Then
                    iSearch &= "AND coyfield5unkid = '" & CInt(cboFieldValue5.SelectedValue) & "' "
                End If

                If dtpStartDate.Checked = True AndAlso dtpEndDate.Checked = True Then
                    iSearch &= "AND sdate >= '" & eZeeDate.convertDate(dtpStartDate.Value) & "' AND edate <= '" & eZeeDate.convertDate(dtpEndDate.Value) & "' "
                End If

                If CInt(cboStatus.SelectedValue) > 0 Then
                    iSearch &= "AND CStatusId = '" & CInt(cboStatus.SelectedValue) & "' "
                End If

                If CInt(cboAllocations.SelectedValue) > 0 Then
                    iSearch &= "AND COwnerId = '" & CInt(cboAllocations.SelectedValue) & "' "
                End If

                If CInt(cboOwner.SelectedValue) > 0 Then
                    Dim objOwr As New clsassess_coyowner_tran
                    Dim iString As String = ""
                    iString = objOwr.GetCSV_CoyFieldTableIds(CInt(cboOwner.SelectedValue), IIf(IsDBNull(dtFinal.Rows(0).Item("CFieldTypeId")) = True, 0, dtFinal.Rows(0).Item("CFieldTypeId")), True)
                    objOwr = Nothing
                    iSearch &= iString
                End If
            End If


            If iSearch.Trim.Length > 0 Then
                iSearch = iSearch.Substring(3)
                mdtFinal = New DataView(dtFinal, iSearch, "Field1Id DESC, perspectiveunkid ASC", DataViewRowState.CurrentRows).ToTable
            Else
                mdtFinal = New DataView(dtFinal, "", "Field1Id DESC, perspectiveunkid ASC", DataViewRowState.CurrentRows).ToTable
            End If

            Dim iPlan() As String = Nothing
            If ConfigParameter._Object._ViewTitles_InPlanning.Trim.Length > 0 Then
                iPlan = ConfigParameter._Object._ViewTitles_InPlanning.Split("|")
            End If
            dgvData.AutoGenerateColumns = False
            Dim iColName As String = String.Empty
            For Each dCol As DataColumn In mdtFinal.Columns
                iColName = "" : iColName = "obj" & dCol.ColumnName
                If dgvData.Columns.Contains(iColName) = True Then Continue For
                Dim dgvCol As New DataGridViewTextBoxColumn()
                dgvCol.Name = iColName
                dgvCol.Width = 110
                'S.SANDEEP [24 SEP 2015] -- START
                'dgvCol.SortMode = DataGridViewColumnSortMode.NotSortable
                dgvCol.SortMode = DataGridViewColumnSortMode.Automatic
                'S.SANDEEP [24 SEP 2015] -- END
                dgvCol.ReadOnly = True
                dgvCol.DataPropertyName = dCol.ColumnName
                dgvCol.HeaderText = dCol.Caption
                If dCol.Caption.Length <= 0 Then
                    dgvCol.Visible = False
                Else
                    If mdtFinal.Columns(dCol.ColumnName).ExtendedProperties(dCol.ColumnName) IsNot Nothing Then
                        dgvCol.Width = objFMaster.GetColumnWidth(clsAssess_Field_Master.enColWidthType.COL_PLAN, mdtFinal.Columns(dCol.ColumnName).ExtendedProperties(dCol.ColumnName))
                        If iPlan IsNot Nothing Then
                            If Array.IndexOf(iPlan, mdtFinal.Columns(dCol.ColumnName).ExtendedProperties(dCol.ColumnName).ToString) < 0 Then
                                dgvCol.Visible = False
                            End If
                        End If
                    End If
                End If
                dgvData.Columns.Add(dgvCol)
            Next
            dgvData.DataSource = mdtFinal

            'S.SANDEEP [28 MAY 2015] -- START
            'ENHANCEMENT : NEW PRIVILEGE (Performance Assessment)
            Call SetVisibility()
            'S.SANDEEP [28 MAY 2015] -- END

            objdgcolhAdd.DefaultCellStyle.SelectionBackColor = Color.White
            objdgcolhAdd.DefaultCellStyle.SelectionForeColor = Color.White

            objdgcolhEdit.DefaultCellStyle.SelectionBackColor = Color.White
            objdgcolhEdit.DefaultCellStyle.SelectionForeColor = Color.White

            objdgcolhDelete.DefaultCellStyle.SelectionBackColor = Color.White
            objdgcolhDelete.DefaultCellStyle.SelectionForeColor = Color.White

            For Each dgvRow As DataGridViewRow In dgvData.Rows
                If CInt(dgvRow.Cells("objopstatusid").Value) = enObjective_Status.FINAL_COMMITTED Then
                    dgvData.Columns(objdgcolhAdd.Index).Visible = False
                    dgvData.Columns(objdgcolhEdit.Index).Visible = False
                    dgvData.Columns(objdgcolhDelete.Index).Visible = False
                    Exit For
                Else
                    dgvData.Columns(objdgcolhAdd.Index).Visible = True
                    dgvData.Columns(objdgcolhEdit.Index).Visible = True
                    dgvData.Columns(objdgcolhDelete.Index).Visible = True
                    Exit For
                End If
            Next

            Dim objFMapping As New clsAssess_Field_Mapping
            Dim xTotalWeight As Double = 0
            xTotalWeight = objFMapping.GetTotalWeight(clsAssess_Field_Mapping.enWeightCheckType.CKT_COMPANY_LEVEL, CInt(cboPeriod.SelectedValue), 0, 0)
            If xTotalWeight > 0 Then
                objlblTotalWeight.Text = Language.getMessage(mstrModuleName, 30, "Total Weight Assigned :") & " " & xTotalWeight.ToString
            Else
                objlblTotalWeight.Text = ""
            End If
            objFMapping = Nothing

          
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Fill_Grid", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub Generate_MenuItems()
        Dim dsList As New DataSet
        Try
            Dim objFMaster As New clsAssess_Field_Master
            dsList = objFMaster.Get_Field_Mapping("List", , True)
            If dsList.Tables(0).Rows.Count > 0 Then
                cmnuAdd.Items.Clear() : cmnuEdit.Items.Clear() : cmnuDelete.Items.Clear()
                For i As Integer = 0 To dsList.Tables(0).Rows.Count - 1
                    Dim objbtnAdd As New ToolStripMenuItem : Dim objbtnEdt As New ToolStripMenuItem : Dim objbtnDel As New ToolStripMenuItem

                    objbtnAdd.Name = "objbtnAdd" & dsList.Tables(0).Rows(i).Item("fieldunkid").ToString
                    objbtnAdd.Text = Language.getMessage(mstrModuleName, 27, "Add") & " " & dsList.Tables(0).Rows(i)("fieldcaption").ToString
                    objbtnAdd.Tag = dsList.Tables(0).Rows(i)("fieldunkid").ToString & "|" & dsList.Tables(0).Rows(i)("ExOrder").ToString
                    cmnuAdd.Items.Add(objbtnAdd)
                    AddHandler objbtnAdd.Click, AddressOf objbtnAdd_Click

                    objbtnEdt.Name = "objbtnEdt" & dsList.Tables(0).Rows(i).Item("fieldunkid").ToString
                    objbtnEdt.Text = Language.getMessage(mstrModuleName, 28, "Edit") & " " & dsList.Tables(0).Rows(i)("fieldcaption").ToString
                    objbtnEdt.Tag = dsList.Tables(0).Rows(i)("fieldunkid").ToString & "|" & dsList.Tables(0).Rows(i)("ExOrder").ToString
                    cmnuEdit.Items.Add(objbtnEdt)
                    AddHandler objbtnEdt.Click, AddressOf objbtnEdt_Click

                    objbtnDel.Name = "objbtnDel" & dsList.Tables(0).Rows(i).Item("fieldunkid").ToString
                    objbtnDel.Text = Language.getMessage(mstrModuleName, 29, "Delete") & " " & dsList.Tables(0).Rows(i)("fieldcaption").ToString
                    objbtnDel.Tag = dsList.Tables(0).Rows(i)("fieldunkid").ToString & "|" & dsList.Tables(0).Rows(i)("ExOrder").ToString
                    cmnuDelete.Items.Add(objbtnDel)
                    AddHandler objbtnDel.Click, AddressOf objbtnDel_Click
                Next
            End If
            objFMaster = Nothing
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Generate_MenuItems", mstrModuleName)
        Finally
        End Try
    End Sub

    'S.SANDEEP [28 MAY 2015] -- START
    'ENHANCEMENT : NEW PRIVILEGE (Performance Assessment)
    Private Sub SetVisibility()
        Try
            objdgcolhAdd.Visible = User._Object.Privilege._AllowtoAddCompanyGoals
            objdgcolhEdit.Visible = User._Object.Privilege._AllowtoEditCompanyGoals
            objdgcolhDelete.Visible = User._Object.Privilege._AllowtoDeleteCompanyGoals
            mnuCommit.Enabled = User._Object.Privilege._AllowtoCommitCompanyGoals
            mnuUnlock.Enabled = User._Object.Privilege._AllowtoUnlockcommittedCompanyGoals
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetVisibility", mstrModuleName)
        Finally
        End Try
    End Sub
    'S.SANDEEP [28 MAY 2015] -- END

#End Region

#Region " Button's Events "

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Me.Close()
    End Sub

    Private Sub objbtnSearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearch.Click
        Try
            If CInt(cboPeriod.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 25, "Period is mandatory information. Please select Period to continue."), enMsgBoxStyle.Information)
                cboPeriod.Focus()
                Exit Sub
            End If
            Call Fill_Grid()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearch_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub objbtnReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnReset.Click
        Try
            cboPerspective.SelectedValue = 0
            cboStatus.SelectedValue = 0
            cboFieldValue1.SelectedValue = 0
            cboFieldValue2.SelectedValue = 0
            cboFieldValue3.SelectedValue = 0
            cboFieldValue4.SelectedValue = 0
            cboFieldValue5.SelectedValue = 0
            dtpStartDate.Checked = False : dtpEndDate.Checked = False
            dgvData.DataSource = Nothing
            cboPeriod.SelectedValue = 0
            cboAllocations.SelectedValue = 0
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnReset_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub objbtnSearchField1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchField1.Click, _
                                                                                                             objbtnSearchField2.Click, _
                                                                                                             objbtnSearchField3.Click, _
                                                                                                             objbtnSearchField4.Click, _
                                                                                                             objbtnSearchField5.Click, _
                                                                                                             objbtnSearchOwner.Click
        Dim frm As New frmCommonSearch
        Try
            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If

            Dim iCbo As ComboBox = Nothing
            Select Case CType(sender, eZee.Common.eZeeGradientButton).Name.ToUpper
                Case objbtnSearchField1.Name.ToUpper
                    iCbo = cboFieldValue1
                Case objbtnSearchField2.Name.ToUpper
                    iCbo = cboFieldValue2
                Case objbtnSearchField3.Name.ToUpper
                    iCbo = cboFieldValue3
                Case objbtnSearchField4.Name.ToUpper
                    iCbo = cboFieldValue4
                Case objbtnSearchField5.Name.ToUpper
                    iCbo = cboFieldValue5
                Case objbtnSearchOwner.Name.ToUpper
                    iCbo = cboOwner
            End Select
            With frm
                .ValueMember = iCbo.ValueMember
                .DisplayMember = iCbo.DisplayMember
                .DataSource = iCbo.DataSource
                If .DisplayDialog = True Then
                    iCbo.SelectedValue = .SelectedValue
                End If
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchField1_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    'S.SANDEEP [27-MAR-2017] -- START
    'ISSUE/ENHANCEMENT : PROVIDED SEARCHING, AS PACRA HAVING MANY PERSPECTIVES
    Private Sub objbtnSearchPerspective_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchPerspective.Click
        Dim frm As New frmCommonSearch
        Try
            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If
            With frm
                .ValueMember = cboPerspective.ValueMember
                .DisplayMember = cboPerspective.DisplayMember
                .DataSource = CType(cboPerspective.DataSource, DataTable)
                If .DisplayDialog Then
                    cboPerspective.SelectedValue = .SelectedValue
                    cboPerspective.Focus()
                End If
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchPerspective_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub
    'S.SANDEEP [27-MAR-2017] -- END

#End Region

#Region " Form's Event "

    Private Sub frmCoyFieldsList_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        objCoyField1 = New clsassess_coyfield1_master : objCoyField2 = New clsassess_coyfield2_master
        objCoyField3 = New clsassess_coyfield3_master : objCoyField4 = New clsassess_coyfield4_master
        objCoyField5 = New clsassess_coyfield5_master
        cmnuAdd = New ContextMenuStrip() : cmnuEdit = New ContextMenuStrip() : cmnuDelete = New ContextMenuStrip()
        Try
            Call Set_Logo(Me, gApplicationType)
            Language.setLanguage(Me.Name)
            Call OtherSettings()
            Call Generate_MenuItems()
            Call SetFormViewSetting()
            Call FillCombo()
            'S.SANDEEP [28 MAY 2015] -- START
            'ENHANCEMENT : NEW PRIVILEGE (Performance Assessment)
            Call SetVisibility()
            'S.SANDEEP [28 MAY 2015] -- END
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmCoyFieldsList_Load", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsassess_coyfield1_master.SetMessages()
            clsassess_coyfield2_master.SetMessages()
            clsassess_coyfield3_master.SetMessages()
            clsassess_coyfield4_master.SetMessages()
            clsassess_coyfield5_master.SetMessages()
            objfrm._Other_ModuleNames = "clsassess_coyfield1_master,clsassess_coyfield2_master,clsassess_coyfield3_master,clsassess_coyfield4_master,clsassess_coyfield5_master"
            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

#Region " Controls Events "

    Private Sub dgvData_CellFormatting(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellFormattingEventArgs) Handles dgvData.CellFormatting
        Try
            If e.RowIndex <= -1 Then Exit Sub
            Select Case e.ColumnIndex
                Case objdgcolhAdd.Index
                    dgvData.Rows(e.RowIndex).Cells(e.ColumnIndex).ToolTipText = Language.getMessage(mstrModuleName, 5, "Add")
                Case objdgcolhEdit.Index
                    dgvData.Rows(e.RowIndex).Cells(e.ColumnIndex).ToolTipText = Language.getMessage(mstrModuleName, 6, "Edit")
                Case objdgcolhDelete.Index
                    dgvData.Rows(e.RowIndex).Cells(e.ColumnIndex).ToolTipText = Language.getMessage(mstrModuleName, 7, "Delete")
            End Select
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgvData_CellFormatting", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub dgvData_CellMouseClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellMouseEventArgs) Handles dgvData.CellMouseClick
        Try
            If e.RowIndex <= -1 Then Exit Sub
            If e.Button = Windows.Forms.MouseButtons.Left Then
                If e.ColumnIndex < 3 Then
                    Select Case e.ColumnIndex
                        Case dgvData.Rows(e.RowIndex).Cells(objdgcolhAdd.Index).ColumnIndex
                            dgvData.CurrentCell = dgvData.Rows(e.RowIndex).Cells(e.ColumnIndex)
                            cmnuAdd.Show(dgvData, dgvData.PointToClient(Windows.Forms.Cursor.Position))
                        Case dgvData.Rows(e.RowIndex).Cells(objdgcolhEdit.Index).ColumnIndex
                            dgvData.CurrentCell = dgvData.Rows(e.RowIndex).Cells(e.ColumnIndex)
                            cmnuEdit.Show(dgvData, dgvData.PointToClient(Windows.Forms.Cursor.Position))
                        Case dgvData.Rows(e.RowIndex).Cells(objdgcolhDelete.Index).ColumnIndex
                            dgvData.CurrentCell = dgvData.Rows(e.RowIndex).Cells(e.ColumnIndex)
                            cmnuDelete.Show(dgvData, dgvData.PointToClient(Windows.Forms.Cursor.Position))
                    End Select
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgvData_CellMouseClick", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub objbtnDel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Dim frm As New frmReasonSelection
        Try
            Dim iVoidReason As String = String.Empty

            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If

            Dim iMsg As String = Language.getMessage(mstrModuleName, 23, "You are about to remove the information from company level, this will delete all child linked to this parent") & vbCrLf & Language.getMessage(mstrModuleName, 24, "Are you sure you want to delete?")

            If eZeeMsgBox.Show(iMsg, enMsgBoxStyle.Information + enMsgBoxStyle.YesNo) = Windows.Forms.DialogResult.No Then Exit Sub

            Select Case CInt(sender.tag.ToString.Split("|")(1))
                Case 1
                    If CInt(dgvData.CurrentRow.Cells("objcoyfield1unkid").Value) > 0 Then
                        frm.displayDialog(enVoidCategoryType.ASSESSMENT, iVoidReason)
                        If iVoidReason.Length <= 0 Then
                            Exit Sub
                        End If
                        objCoyField1._Isvoid = True
                        objCoyField1._Voiddatetime = ConfigParameter._Object._CurrentDateAndTime
                        objCoyField1._Voidreason = iVoidReason
                        objCoyField1._Voiduserunkid = User._Object._Userunkid

                        'S.SANDEEP [28-May-2018] -- START
                        'ISSUE/ENHANCEMENT : {Audit Trails} 
                        objCoyField1._FormName = mstrModuleName
                        objCoyField1._LoginEmployeeunkid = 0
                        objCoyField1._ClientIP = getIP()
                        objCoyField1._HostName = getHostName()
                        objCoyField1._FromWeb = False
                        objCoyField1._AuditUserId = User._Object._Userunkid
                        objCoyField1._CompanyUnkid = Company._Object._Companyunkid
                        objCoyField1._AuditDate = ConfigParameter._Object._CurrentDateAndTime
                        'S.SANDEEP [28-May-2018] -- END

                        If objCoyField1.Delete(CInt(dgvData.CurrentRow.Cells("objcoyfield1unkid").Value)) = True Then
                            Call Fill_Grid()
                        Else
                            If objCoyField1._Message <> "" Then
                                eZeeMsgBox.Show(objCoyField1._Message, enMsgBoxStyle.Information)
                                Exit Sub
                            End If
                        End If
                    Else
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Sorry, there is no") & _
                                         " " & dgvData.Columns(dgvData.CurrentRow.Cells("objField1").ColumnIndex).HeaderText & " " & _
                                        Language.getMessage(mstrModuleName, 2, "defined. Please define") & _
                                         " " & dgvData.Columns(dgvData.CurrentRow.Cells("objField1").ColumnIndex).HeaderText & " " & _
                                        Language.getMessage(mstrModuleName, 4, "in order to perform delete operation."), enMsgBoxStyle.Information)
                        Exit Sub
                    End If
                Case 2
                    If CInt(dgvData.CurrentRow.Cells("objcoyfield2unkid").Value) > 0 Then
                        frm.displayDialog(enVoidCategoryType.ASSESSMENT, iVoidReason)
                        If iVoidReason.Length <= 0 Then
                            Exit Sub
                        End If
                        objCoyField2._Isvoid = True
                        objCoyField2._Voiddatetime = ConfigParameter._Object._CurrentDateAndTime
                        objCoyField2._Voidreason = iVoidReason
                        objCoyField2._Voiduserunkid = User._Object._Userunkid
                        'S.SANDEEP [28-May-2018] -- START
                        'ISSUE/ENHANCEMENT : {Audit Trails} 
                        objCoyField2._FormName = mstrModuleName
                        objCoyField2._LoginEmployeeunkid = 0
                        objCoyField2._ClientIP = getIP()
                        objCoyField2._HostName = getHostName()
                        objCoyField2._FromWeb = False
                        objCoyField2._AuditUserId = User._Object._Userunkid
                        objCoyField2._CompanyUnkid = Company._Object._Companyunkid
                        objCoyField2._AuditDate = ConfigParameter._Object._CurrentDateAndTime
                        'S.SANDEEP [28-May-2018] -- END
                        If objCoyField2.Delete(CInt(dgvData.CurrentRow.Cells("objcoyfield2unkid").Value)) = True Then
                            Call Fill_Grid()
                        End If
                    Else
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Sorry, there is no") & _
                                         " " & dgvData.Columns(dgvData.CurrentRow.Cells("objField2").ColumnIndex).HeaderText & " " & _
                                        Language.getMessage(mstrModuleName, 2, "defined. Please define") & _
                                         " " & dgvData.Columns(dgvData.CurrentRow.Cells("objField2").ColumnIndex).HeaderText & " " & _
                                        Language.getMessage(mstrModuleName, 4, "in order to perform delete operation."), enMsgBoxStyle.Information)
                        Exit Sub
                    End If
                Case 3
                    If CInt(dgvData.CurrentRow.Cells("objcoyfield3unkid").Value) > 0 Then
                        frm.displayDialog(enVoidCategoryType.ASSESSMENT, iVoidReason)
                        If iVoidReason.Length <= 0 Then
                            Exit Sub
                        End If
                        objCoyField3._Isvoid = True
                        objCoyField3._Voiddatetime = ConfigParameter._Object._CurrentDateAndTime
                        objCoyField3._Voidreason = iVoidReason
                        objCoyField3._Voiduserunkid = User._Object._Userunkid
                        'S.SANDEEP [28-May-2018] -- START
                        'ISSUE/ENHANCEMENT : {Audit Trails} 
                        objCoyField3._FormName = mstrModuleName
                        objCoyField3._LoginEmployeeunkid = 0
                        objCoyField3._ClientIP = getIP()
                        objCoyField3._HostName = getHostName()
                        objCoyField3._FromWeb = False
                        objCoyField3._AuditUserId = User._Object._Userunkid
                        objCoyField3._CompanyUnkid = Company._Object._Companyunkid
                        objCoyField3._AuditDate = ConfigParameter._Object._CurrentDateAndTime
                        'S.SANDEEP [28-May-2018] -- END
                        If objCoyField3.Delete(CInt(dgvData.CurrentRow.Cells("objcoyfield3unkid").Value)) = True Then
                            Call Fill_Grid()
                        End If
                    Else
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Sorry, there is no") & _
                                         " " & dgvData.Columns(dgvData.CurrentRow.Cells("objField3").ColumnIndex).HeaderText & " " & _
                                        Language.getMessage(mstrModuleName, 2, "defined. Please define") & _
                                         " " & dgvData.Columns(dgvData.CurrentRow.Cells("objField3").ColumnIndex).HeaderText & " " & _
                                        Language.getMessage(mstrModuleName, 4, "in order to perform delete operation."), enMsgBoxStyle.Information)
                        Exit Sub
                    End If
                Case 4
                    If CInt(dgvData.CurrentRow.Cells("objcoyfield4unkid").Value) > 0 Then
                        frm.displayDialog(enVoidCategoryType.ASSESSMENT, iVoidReason)
                        If iVoidReason.Length <= 0 Then
                            Exit Sub
                        End If
                        objCoyField4._Isvoid = True
                        objCoyField4._Voiddatetime = ConfigParameter._Object._CurrentDateAndTime
                        objCoyField4._Voidreason = iVoidReason
                        objCoyField4._Voiduserunkid = User._Object._Userunkid
                        'S.SANDEEP [28-May-2018] -- START
                        'ISSUE/ENHANCEMENT : {Audit Trails} 
                        objCoyField4._FormName = mstrModuleName
                        objCoyField4._LoginEmployeeunkid = 0
                        objCoyField4._ClientIP = getIP()
                        objCoyField4._HostName = getHostName()
                        objCoyField4._FromWeb = False
                        objCoyField4._AuditUserId = User._Object._Userunkid
                        objCoyField4._CompanyUnkid = Company._Object._Companyunkid
                        objCoyField4._AuditDate = ConfigParameter._Object._CurrentDateAndTime
                        'S.SANDEEP [28-May-2018] -- END
                        If objCoyField4.Delete(CInt(dgvData.CurrentRow.Cells("objcoyfield4unkid").Value)) = True Then
                            Call Fill_Grid()
                        End If
                    Else
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Sorry, there is no") & _
                                         " " & dgvData.Columns(dgvData.CurrentRow.Cells("objField4").ColumnIndex).HeaderText & " " & _
                                        Language.getMessage(mstrModuleName, 2, "defined. Please define") & _
                                         " " & dgvData.Columns(dgvData.CurrentRow.Cells("objField4").ColumnIndex).HeaderText & " " & _
                                        Language.getMessage(mstrModuleName, 4, "in order to perform delete operation."), enMsgBoxStyle.Information)
                        Exit Sub
                    End If
                Case 5
                    If CInt(dgvData.CurrentRow.Cells("objcoyfield5unkid").Value) > 0 Then
                        frm.displayDialog(enVoidCategoryType.ASSESSMENT, iVoidReason)
                        If iVoidReason.Length <= 0 Then
                            Exit Sub
                        End If
                        objCoyField5._Isvoid = True
                        objCoyField5._Voiddatetime = ConfigParameter._Object._CurrentDateAndTime
                        objCoyField5._Voidreason = iVoidReason
                        objCoyField5._Voiduserunkid = User._Object._Userunkid
                        'S.SANDEEP [28-May-2018] -- START
                        'ISSUE/ENHANCEMENT : {Audit Trails} 
                        objCoyField5._FormName = mstrModuleName
                        objCoyField5._LoginEmployeeunkid = 0
                        objCoyField5._ClientIP = getIP()
                        objCoyField5._HostName = getHostName()
                        objCoyField5._FromWeb = False
                        objCoyField5._AuditUserId = User._Object._Userunkid
                        objCoyField5._CompanyUnkid = Company._Object._Companyunkid
                        objCoyField5._AuditDate = ConfigParameter._Object._CurrentDateAndTime
                        'S.SANDEEP [28-May-2018] -- END
                        If objCoyField5.Delete(CInt(dgvData.CurrentRow.Cells("objcoyfield5unkid").Value)) = True Then
                            Call Fill_Grid()
                        End If
                    Else
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Sorry, there is no") & _
                                         " " & dgvData.Columns(dgvData.CurrentRow.Cells("objField5").ColumnIndex).HeaderText & " " & _
                                        Language.getMessage(mstrModuleName, 2, "defined. Please define") & _
                                         " " & dgvData.Columns(dgvData.CurrentRow.Cells("objField5").ColumnIndex).HeaderText & " " & _
                                        Language.getMessage(mstrModuleName, 4, "in order to perform delete operation."), enMsgBoxStyle.Information)
                        Exit Sub
                    End If
            End Select
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnDel_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub objbtnEdt_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Try
            If CInt(cboPeriod.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 25, "Period is mandatory information. Please select Period to continue."), enMsgBoxStyle.Information)
                cboPeriod.Focus()
                Exit Sub
            End If

            Select Case CInt(sender.tag.ToString.Split("|")(1))
                Case 1
                    If CInt(dgvData.CurrentRow.Cells("objcoyfield1unkid").Value) > 0 Then
                        Dim frm As New objfrmAddEditCoyField1

                        If User._Object._Isrighttoleft = True Then
                            frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                            frm.RightToLeftLayout = True
                            Call Language.ctlRightToLeftlayOut(frm)
                        End If

                        If frm.displayDialog(CInt(dgvData.CurrentRow.Cells("objcoyfield1unkid").Value), enAction.EDIT_ONE, CInt(sender.tag.ToString.Split("|")(0)), CInt(cboPeriod.SelectedValue)) Then
                            Call Fill_Grid()
                        End If
                    Else
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Sorry, there is no") & _
                                         " " & dgvData.Columns(dgvData.CurrentRow.Cells("objField1").ColumnIndex).HeaderText & " " & _
                                        Language.getMessage(mstrModuleName, 2, "defined. Please define") & _
                                         " " & dgvData.Columns(dgvData.CurrentRow.Cells("objField1").ColumnIndex).HeaderText & " " & _
                                        Language.getMessage(mstrModuleName, 3, "in order to perform edit operation."), enMsgBoxStyle.Information)
                        Exit Sub
                    End If
                Case 2
                    If CInt(dgvData.CurrentRow.Cells("objcoyfield2unkid").Value) > 0 Then
                        Dim frm As New objfrmAddEditCoyField2

                        If User._Object._Isrighttoleft = True Then
                            frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                            frm.RightToLeftLayout = True
                            Call Language.ctlRightToLeftlayOut(frm)
                        End If

                        If frm.displayDialog(CInt(dgvData.CurrentRow.Cells("objcoyfield2unkid").Value), enAction.EDIT_ONE, CInt(sender.tag.ToString.Split("|")(0)), CInt(cboPeriod.SelectedValue), CInt(dgvData.CurrentRow.Cells("objcoyfield1unkid").Value)) Then
                            Call Fill_Grid()
                        End If
                    Else
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Sorry, there is no") & _
                                         " " & dgvData.Columns(dgvData.CurrentRow.Cells("objField2").ColumnIndex).HeaderText & " " & _
                                        Language.getMessage(mstrModuleName, 2, "defined. Please define") & _
                                         " " & dgvData.Columns(dgvData.CurrentRow.Cells("objField2").ColumnIndex).HeaderText & " " & _
                                        Language.getMessage(mstrModuleName, 3, "in order to perform edit operation."), enMsgBoxStyle.Information)
                        Exit Sub
                    End If
                Case 3
                    If CInt(dgvData.CurrentRow.Cells("objcoyfield3unkid").Value) > 0 Then
                        Dim frm As New objfrmAddEditCoyField3

                        If User._Object._Isrighttoleft = True Then
                            frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                            frm.RightToLeftLayout = True
                            Call Language.ctlRightToLeftlayOut(frm)
                        End If

                        If frm.displayDialog(CInt(dgvData.CurrentRow.Cells("objcoyfield3unkid").Value), enAction.EDIT_ONE, CInt(sender.tag.ToString.Split("|")(0)), CInt(cboPeriod.SelectedValue), CInt(dgvData.CurrentRow.Cells("objcoyfield2unkid").Value), CInt(dgvData.CurrentRow.Cells("objcoyfield1unkid").Value)) Then
                            Call Fill_Grid()
                        End If
                    Else
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Sorry, there is no") & _
                                         " " & dgvData.Columns(dgvData.CurrentRow.Cells("objField3").ColumnIndex).HeaderText & " " & _
                                        Language.getMessage(mstrModuleName, 2, "defined. Please define") & _
                                         " " & dgvData.Columns(dgvData.CurrentRow.Cells("objField3").ColumnIndex).HeaderText & " " & _
                                        Language.getMessage(mstrModuleName, 3, "in order to perform edit operation."), enMsgBoxStyle.Information)
                        Exit Sub
                    End If
                Case 4
                    If CInt(dgvData.CurrentRow.Cells("objcoyfield4unkid").Value) > 0 Then
                        Dim frm As New objfrmAddEditCoyField4

                        If User._Object._Isrighttoleft = True Then
                            frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                            frm.RightToLeftLayout = True
                            Call Language.ctlRightToLeftlayOut(frm)
                        End If

                        If frm.displayDialog(CInt(dgvData.CurrentRow.Cells("objcoyfield4unkid").Value), enAction.EDIT_ONE, CInt(sender.tag.ToString.Split("|")(0)), CInt(cboPeriod.SelectedValue), CInt(dgvData.CurrentRow.Cells("objcoyfield3unkid").Value), CInt(dgvData.CurrentRow.Cells("objcoyfield2unkid").Value)) Then
                            Call Fill_Grid()
                        End If
                    Else
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Sorry, there is no") & _
                                         " " & dgvData.Columns(dgvData.CurrentRow.Cells("objField4").ColumnIndex).HeaderText & " " & _
                                        Language.getMessage(mstrModuleName, 2, "defined. Please define") & _
                                         " " & dgvData.Columns(dgvData.CurrentRow.Cells("objField4").ColumnIndex).HeaderText & " " & _
                                        Language.getMessage(mstrModuleName, 3, "in order to perform edit operation."), enMsgBoxStyle.Information)
                        Exit Sub
                    End If
                Case 5
                    If CInt(dgvData.CurrentRow.Cells("objcoyfield5unkid").Value) > 0 Then
                        Dim frm As New objfrmAddEditCoyField5

                        If User._Object._Isrighttoleft = True Then
                            frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                            frm.RightToLeftLayout = True
                            Call Language.ctlRightToLeftlayOut(frm)
                        End If

                        If frm.displayDialog(CInt(dgvData.CurrentRow.Cells("objcoyfield5unkid").Value), enAction.EDIT_ONE, CInt(sender.tag.ToString.Split("|")(0)), CInt(cboPeriod.SelectedValue), CInt(dgvData.CurrentRow.Cells("objcoyfield4unkid").Value), CInt(dgvData.CurrentRow.Cells("objcoyfield3unkid").Value)) Then
                            Call Fill_Grid()
                        End If
                    Else
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Sorry, there is no") & _
                                         " " & dgvData.Columns(dgvData.CurrentRow.Cells("objField5").ColumnIndex).HeaderText & " " & _
                                        Language.getMessage(mstrModuleName, 2, "defined. Please define") & _
                                         " " & dgvData.Columns(dgvData.CurrentRow.Cells("objField5").ColumnIndex).HeaderText & " " & _
                                        Language.getMessage(mstrModuleName, 3, "in order to perform edit operation."), enMsgBoxStyle.Information)
                        Exit Sub
                    End If
            End Select
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnEdt_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub objbtnAdd_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Try
            If CInt(cboPeriod.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 25, "Period is mandatory information. Please select Period to continue."), enMsgBoxStyle.Information)
                cboPeriod.Focus()
                Exit Sub
            End If
            Select Case CInt(sender.tag.ToString.Split("|")(1))
                Case 1
                    Dim frm As New objfrmAddEditCoyField1

                    If User._Object._Isrighttoleft = True Then
                        frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                        frm.RightToLeftLayout = True
                        Call Language.ctlRightToLeftlayOut(frm)
                    End If

                    If frm.displayDialog(-1, enAction.ADD_CONTINUE, CInt(sender.tag.ToString.Split("|")(0)), CInt(cboPeriod.SelectedValue)) Then
                        Call Fill_Grid()
                    End If
                Case 2
                    Dim frm As New objfrmAddEditCoyField2

                    If User._Object._Isrighttoleft = True Then
                        frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                        frm.RightToLeftLayout = True
                        Call Language.ctlRightToLeftlayOut(frm)
                    End If

                    'If ConfigParameter._Object._CascadingTypeId = enPACascading.STRICT_CASCADING Then
                    If frm.displayDialog(-1, enAction.ADD_CONTINUE, CInt(sender.tag.ToString.Split("|")(0)), CInt(cboPeriod.SelectedValue), CInt(dgvData.CurrentRow.Cells("objcoyfield1unkid").Value)) Then
                        Call Fill_Grid()
                    End If
                    'Else
                    'If frm.displayDialog(-1, enAction.ADD_CONTINUE, CInt(sender.tag.ToString.Split("|")(0))) Then
                    'Call Fill_Grid()
                    'End If
                    'End If
                Case 3
                    Dim frm As New objfrmAddEditCoyField3

                    If User._Object._Isrighttoleft = True Then
                        frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                        frm.RightToLeftLayout = True
                        Call Language.ctlRightToLeftlayOut(frm)
                    End If

                    'If ConfigParameter._Object._CascadingTypeId = enPACascading.STRICT_CASCADING Then
                    If frm.displayDialog(-1, enAction.ADD_CONTINUE, CInt(sender.tag.ToString.Split("|")(0)), CInt(cboPeriod.SelectedValue), CInt(dgvData.CurrentRow.Cells("objcoyfield2unkid").Value), CInt(dgvData.CurrentRow.Cells("objcoyfield1unkid").Value)) Then
                        Call Fill_Grid()
                    End If
                    'Else
                    'If frm.displayDialog(-1, enAction.ADD_CONTINUE, CInt(sender.tag.ToString.Split("|")(0))) Then
                    '    Call Fill_Grid()
                    'End If
                    'End If

                Case 4
                    Dim frm As New objfrmAddEditCoyField4

                    If User._Object._Isrighttoleft = True Then
                        frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                        frm.RightToLeftLayout = True
                        Call Language.ctlRightToLeftlayOut(frm)
                    End If

                    'If ConfigParameter._Object._CascadingTypeId = enPACascading.STRICT_CASCADING Then
                    If frm.displayDialog(-1, enAction.ADD_CONTINUE, CInt(sender.tag.ToString.Split("|")(0)), CInt(cboPeriod.SelectedValue), CInt(dgvData.CurrentRow.Cells("objcoyfield3unkid").Value), CInt(dgvData.CurrentRow.Cells("objcoyfield2unkid").Value)) Then
                        Call Fill_Grid()
                    End If
                    'Else
                    'If frm.displayDialog(-1, enAction.ADD_CONTINUE, CInt(sender.tag.ToString.Split("|")(0))) Then
                    '    Call Fill_Grid()
                    'End If
                    'End If

                Case 5
                    Dim frm As New objfrmAddEditCoyField5

                    If User._Object._Isrighttoleft = True Then
                        frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                        frm.RightToLeftLayout = True
                        Call Language.ctlRightToLeftlayOut(frm)
                    End If

                    'If ConfigParameter._Object._CascadingTypeId = enPACascading.STRICT_CASCADING Then
                    If frm.displayDialog(-1, enAction.ADD_CONTINUE, CInt(sender.tag.ToString.Split("|")(0)), CInt(cboPeriod.SelectedValue), CInt(dgvData.CurrentRow.Cells("objcoyfield4unkid").Value), CInt(dgvData.CurrentRow.Cells("objcoyfield3unkid").Value)) Then
                        Call Fill_Grid()
                    End If
                    'Else
                    'If frm.displayDialog(-1, enAction.ADD_CONTINUE, CInt(sender.tag.ToString.Split("|")(0))) Then
                    '    Call Fill_Grid()
                    'End If
                    'End If
            End Select
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnAdd_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub mnuCommit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuCommit.Click
        Try
            Me.Cursor = Cursors.WaitCursor

            If CInt(cboPeriod.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 25, "Period is mandatory information. Please select Period to continue."), enMsgBoxStyle.Information)
                cboPeriod.Focus()
                Exit Sub
            End If

            If dgvData.RowCount <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 10, "Sorry, there is no goal defined in order to commit."), enMsgBoxStyle.Information)
                Exit Sub
            End If

            Dim iMsg As String = String.Empty

            iMsg = objFMaster.Allow_Commit(clsAssess_Field_Master.enCommitMode.COY_COMMIT)

            If iMsg.Trim.Length > 0 Then
                eZeeMsgBox.Show(iMsg, enMsgBoxStyle.Information)
                Exit Sub
            End If

            Dim iLast_StatusId As Integer = 0 : Dim iMsgText As String = String.Empty
            iLast_StatusId = objCoyField1.GetLastStatus(CInt(cboPeriod.SelectedValue))

            If iLast_StatusId = enObjective_Status.FINAL_COMMITTED Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 11, "Sorry, you cannot set the same operational status again. Reason : Company goals are already committed."), enMsgBoxStyle.Information)
                Exit Sub
            End If


            'S.SANDEEP [28-May-2018] -- START
            'ISSUE/ENHANCEMENT : {Audit Trails} 
            objCoyField1._FormName = mstrModuleName
            objCoyField1._LoginEmployeeunkid = 0
            objCoyField1._ClientIP = getIP()
            objCoyField1._HostName = getHostName()
            objCoyField1._FromWeb = False
            objCoyField1._AuditUserId = User._Object._Userunkid
            objCoyField1._CompanyUnkid = Company._Object._Companyunkid
            objCoyField1._AuditDate = ConfigParameter._Object._CurrentDateAndTime
            'S.SANDEEP [28-May-2018] -- END

            Dim objFMapping As New clsAssess_Field_Mapping
            iMsg = objFMapping.Is_Weight_Set_For_Commiting(clsAssess_Field_Mapping.enWeightCheckType.CKT_COMPANY_LEVEL, CInt(cboPeriod.SelectedValue), 0, 0)
            If iMsg.Trim.Length > 0 Then
                eZeeMsgBox.Show(iMsg, enMsgBoxStyle.Information)
                objFMapping = Nothing
                Exit Sub
            End If
            If objFMapping IsNot Nothing Then objFMapping = Nothing

            With objCoyField1
                ._FormName = mstrModuleName
                ._LoginEmployeeunkid = 0
                ._ClientIP = getIP()
                ._HostName = getHostName()
                ._FromWeb = False
                ._AuditUserId = User._Object._Userunkid
                ._CompanyUnkid = Company._Object._Companyunkid
                ._AuditDate = ConfigParameter._Object._CurrentDateAndTime
            End With

            Select Case ConfigParameter._Object._CascadingTypeId
                Case enPACascading.STRICT_CASCADING, enPACascading.LOOSE_CASCADING
                    iMsgText = Language.getMessage(mstrModuleName, 12, "You are about to commit the company goals. This will tranfer all goals to goal owners.") & vbCrLf & _
                               Language.getMessage(mstrModuleName, 13, "Due to this you will not be allowed to do any operation(s) on company goals.") & vbCrLf & _
                               Language.getMessage(mstrModuleName, 14, "Do you wish to continue?")

                    If eZeeMsgBox.Show(iMsgText, enMsgBoxStyle.Information + enMsgBoxStyle.YesNo) = Windows.Forms.DialogResult.No Then Exit Sub

                    Me.SuspendLayout()
                    RemoveHandler gfrmMDI.tmrReminder.Tick, AddressOf gfrmMDI.tmrReminder_Tick

                    If objCoyField1.Transfer_Goals_ToAllocation(FinancialYear._Object._DatabaseName, CInt(cboPeriod.SelectedValue)) Then
                        If objCoyField1.Update_Status(enObjective_Status.FINAL_COMMITTED, "", User._Object._Userunkid, CInt(cboPeriod.SelectedValue)) = True Then
                            Call Fill_Grid()
                        End If
                    End If

                    AddHandler gfrmMDI.tmrReminder.Tick, AddressOf gfrmMDI.tmrReminder_Tick
                    Me.ResumeLayout()

                Case Else
                    iMsgText = Language.getMessage(mstrModuleName, 22, "You are about to commit the company goals.") & vbCrLf & _
                               Language.getMessage(mstrModuleName, 13, "Due to this you will not be allowed to do any operation(s) on company goals.") & vbCrLf & _
                               Language.getMessage(mstrModuleName, 14, "Do you wish to continue?")

                    If eZeeMsgBox.Show(iMsgText, enMsgBoxStyle.Information + enMsgBoxStyle.YesNo) = Windows.Forms.DialogResult.No Then Exit Sub

                    If objCoyField1.Update_Status(enObjective_Status.FINAL_COMMITTED, "", User._Object._Userunkid, CInt(cboPeriod.SelectedValue)) = True Then
                        Call Fill_Grid()
                    End If
            End Select

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "mnuCommit_Click", mstrModuleName)
        Finally
            Me.Cursor = Cursors.Default
        End Try
    End Sub

    Private Sub mnuUnlock_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuUnlock.Click
        Try
            If dgvData.RowCount <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 15, "Sorry, there is no goal defined in order to unlock."), enMsgBoxStyle.Information)
                Exit Sub
            End If
            Dim iLast_StatusId As Integer = 0 : Dim iMsgText As String = String.Empty

            'S.SANDEEP [28-May-2018] -- START
            'ISSUE/ENHANCEMENT : {Audit Trails} 
            objCoyField1._FormName = mstrModuleName
            objCoyField1._LoginEmployeeunkid = 0
            objCoyField1._ClientIP = getIP()
            objCoyField1._HostName = getHostName()
            objCoyField1._FromWeb = False
            objCoyField1._AuditUserId = User._Object._Userunkid
            objCoyField1._CompanyUnkid = Company._Object._Companyunkid
            objCoyField1._AuditDate = ConfigParameter._Object._CurrentDateAndTime
            'S.SANDEEP [28-May-2018] -- END

            iLast_StatusId = objCoyField1.GetLastStatus(CInt(cboPeriod.SelectedValue))

            If iLast_StatusId <> enObjective_Status.FINAL_COMMITTED Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 16, "Sorry you cannot unlock company goals. Reson : Company goals are not committed."), enMsgBoxStyle.Information)
                Exit Sub
            End If

            If iLast_StatusId = enObjective_Status.OPEN_CHANGES Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 17, "Sorry, you cannot set the same operational status again. Reason : Company goals are already uncommitted."), enMsgBoxStyle.Information)
                Exit Sub
            End If


            'S.SANDEEP [27 Jan 2016] -- START
            iMsgText = objCoyField1.AllowedToUnlock(CInt(cboPeriod.SelectedValue))
            If iMsgText <> "" Then
                eZeeMsgBox.Show(iMsgText, enMsgBoxStyle.Information)
                Exit Sub
            End If
            iMsgText = ""
            'S.SANDEEP [27 Jan 2016] -- END


            iMsgText = Language.getMessage(mstrModuleName, 18, "You are about to unlock the company goals. This will result in opening of all operation on company goals.") & vbCrLf & _
                       Language.getMessage(mstrModuleName, 19, "Do you wish to continue?")

            If eZeeMsgBox.Show(iMsgText, enMsgBoxStyle.Information + enMsgBoxStyle.YesNo) = Windows.Forms.DialogResult.No Then Exit Sub

            Dim iUnlockComments As String = ""

            iUnlockComments = InputBox(Language.getMessage(mstrModuleName, 20, "Enter reason to unlock"), Me.Text)

            If iUnlockComments.Trim.Length <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 21, "Sorry, Unlock reason is mandaroty information."), enMsgBoxStyle.Information)
                Exit Sub
            End If

            If objCoyField1.Update_Status(enObjective_Status.OPEN_CHANGES, iUnlockComments, User._Object._Userunkid, CInt(cboPeriod.SelectedValue)) = True Then
                Call Fill_Grid()
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "mnuUnlock_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub cboFieldValue1_DropDown(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboFieldValue1.DropDown, cboFieldValue2.DropDown, cboFieldValue3.DropDown, cboFieldValue4.DropDown, cboFieldValue5.DropDown
        Try
            mblnDropDownClosed = False
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboFieldValue1_DropDown", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub cboFieldValue1_DropDownClosed(ByVal sender As Object, ByVal e As EventArgs) Handles cboFieldValue1.DropDownClosed, cboFieldValue2.DropDownClosed, cboFieldValue3.DropDownClosed, cboFieldValue4.DropDownClosed, cboFieldValue5.DropDownClosed
        Try
            Dim iCbo As ComboBox = Nothing
            Select Case CType(sender, ComboBox).Name.ToUpper
                Case cboFieldValue1.Name.ToUpper
                    iCbo = cboFieldValue1
                Case cboFieldValue2.Name.ToUpper
                    iCbo = cboFieldValue2
                Case cboFieldValue3.Name.ToUpper
                    iCbo = cboFieldValue3
                Case cboFieldValue4.Name.ToUpper
                    iCbo = cboFieldValue4
                Case cboFieldValue5.Name.ToUpper
                    iCbo = cboFieldValue5
            End Select
            ToolTip1.Hide(iCbo) : mblnDropDownClosed = True
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboFieldValue1_DropDownClosed", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub cboFieldValue1_DrawItem(ByVal sender As Object, ByVal e As DrawItemEventArgs) Handles cboFieldValue1.DrawItem, cboFieldValue2.DrawItem, cboFieldValue3.DrawItem, cboFieldValue4.DrawItem, cboFieldValue5.DrawItem
        Try
            Dim iCbo As ComboBox = Nothing
            Select Case CType(sender, ComboBox).Name.ToUpper
                Case cboFieldValue1.Name.ToUpper
                    iCbo = cboFieldValue1
                Case cboFieldValue2.Name.ToUpper
                    iCbo = cboFieldValue2
                Case cboFieldValue3.Name.ToUpper
                    iCbo = cboFieldValue3
                Case cboFieldValue4.Name.ToUpper
                    iCbo = cboFieldValue4
                Case cboFieldValue5.Name.ToUpper
                    iCbo = cboFieldValue5
            End Select
            If e.Index < 0 Then
                Return
            End If
            Dim text As String = iCbo.GetItemText(iCbo.Items(e.Index))
            e.DrawBackground()
            Using br As New SolidBrush(e.ForeColor)
                e.Graphics.DrawString(text, e.Font, br, e.Bounds)
            End Using
            If (e.State And DrawItemState.Selected) = DrawItemState.Selected Then
                If mblnDropDownClosed = False Then
                    ToolTip1.Show(text, iCbo, e.Bounds.Right, e.Bounds.Bottom)
                End If
            End If
            e.DrawFocusRectangle()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboFieldValue1_DrawItem", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub cboAllocations_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboAllocations.SelectedIndexChanged
        Try
            If CInt(cboAllocations.SelectedValue) > 0 Then
                objlblCaption.Text = "" : cboOwner.Enabled = True : objbtnSearchOwner.Enabled = True
                Dim dList As New DataSet
                objlblCaption.Text = cboAllocations.Text
                cboOwner.DataSource = Nothing
                Select Case CInt(cboAllocations.SelectedValue)
                    Case enAllocation.BRANCH
                        Dim objBranch As New clsStation
                        dList = objBranch.getComboList("List", True)
                        With cboOwner
                            .ValueMember = "stationunkid"
                            .DisplayMember = "name"
                            .DataSource = dList.Tables(0).Copy
                            .SelectedValue = 0
                        End With
                    Case enAllocation.DEPARTMENT_GROUP
                        Dim objDeptGrp As New clsDepartmentGroup
                        dList = objDeptGrp.getComboList("List", True)
                        With cboOwner
                            .ValueMember = "deptgroupunkid"
                            .DisplayMember = "name"
                            .DataSource = dList.Tables(0)
                            .SelectedValue = 0
                        End With
                    Case enAllocation.DEPARTMENT
                        Dim objDept As New clsDepartment
                        dList = objDept.getComboList("List", True)
                        With cboOwner
                            .ValueMember = "departmentunkid"
                            .DisplayMember = "name"
                            .DataSource = dList.Tables(0)
                            .SelectedValue = 0
                        End With
                    Case enAllocation.SECTION_GROUP
                        Dim objSecGrp As New clsSectionGroup
                        dList = objSecGrp.getComboList("List", True)
                        With cboOwner
                            .ValueMember = "sectiongroupunkid"
                            .DisplayMember = "name"
                            .DataSource = dList.Tables(0)
                            .SelectedValue = 0
                        End With
                    Case enAllocation.SECTION
                        Dim objSec As New clsSections
                        dList = objSec.getComboList("List", True)
                        With cboOwner
                            .ValueMember = "sectionunkid"
                            .DisplayMember = "name"
                            .DataSource = dList.Tables(0)
                            .SelectedValue = 0
                        End With
                    Case enAllocation.UNIT_GROUP
                        Dim objUnitGrp As New clsUnitGroup
                        dList = objUnitGrp.getComboList("List", True)
                        With cboOwner
                            .ValueMember = "unitgroupunkid"
                            .DisplayMember = "name"
                            .DataSource = dList.Tables(0)
                            .SelectedValue = 0
                        End With
                    Case enAllocation.UNIT
                        Dim objUnit As New clsUnits
                        dList = objUnit.getComboList("List", True)
                        With cboOwner
                            .ValueMember = "unitunkid"
                            .DisplayMember = "name"
                            .DataSource = dList.Tables(0)
                            .SelectedValue = 0
                        End With
                    Case enAllocation.TEAM
                        Dim objTeam As New clsTeams
                        dList = objTeam.getComboList("List", True)
                        With cboOwner
                            .ValueMember = "teamunkid"
                            .DisplayMember = "name"
                            .DataSource = dList.Tables(0)
                            .SelectedValue = 0
                        End With
                    Case enAllocation.JOB_GROUP
                        Dim objJobGrp As New clsJobGroup
                        dList = objJobGrp.getComboList("List", True)
                        With cboOwner
                            .ValueMember = "jobgroupunkid"
                            .DisplayMember = "name"
                            .DataSource = dList.Tables(0)
                            .SelectedValue = 0
                        End With
                    Case enAllocation.JOBS
                        Dim objJob As New clsJobs
                        dList = objJob.getComboList("List", True)
                        With cboOwner
                            .ValueMember = "jobunkid"
                            .DisplayMember = "name"
                            .DataSource = dList.Tables(0)
                            .SelectedValue = 0
                        End With
                    Case enAllocation.CLASS_GROUP
                        Dim objClsGrp As New clsClassGroup
                        dList = objClsGrp.getComboList("List", True)
                        With cboOwner
                            .ValueMember = "classgroupunkid"
                            .DisplayMember = "name"
                            .DataSource = dList.Tables(0)
                            .SelectedValue = 0
                        End With
                    Case enAllocation.CLASSES
                        Dim objCls As New clsClass
                        dList = objCls.getComboList("List", True)
                        With cboOwner
                            .ValueMember = "classesunkid"
                            .DisplayMember = "name"
                            .DataSource = dList.Tables(0)
                            .SelectedValue = 0
                        End With
                End Select
            Else
                objlblCaption.Text = "" : cboOwner.Enabled = False : objbtnSearchOwner.Enabled = False
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboAllocations_SelectedIndexChanged", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub cboPeriod_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboPeriod.SelectedIndexChanged
        Try
            Dim dsList As New DataSet
            dsList = objCoyField1.getComboList(CInt(cboPeriod.SelectedValue), "List", True)
            With cboFieldValue1
                .ValueMember = "Id"
                .DisplayMember = "Name"
                .DataSource = dsList.Tables("List")
                .SelectedValue = 0
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub cboFieldValue1_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboFieldValue1.SelectedIndexChanged
        Try
            Dim dsList As New DataSet
            dsList = objCoyField2.getComboList("List", True, CInt(cboFieldValue1.SelectedValue))
            With cboFieldValue2
                .ValueMember = "Id"
                .DisplayMember = "Name"
                .DataSource = dsList.Tables("List")
                .SelectedValue = 0
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboFieldValue1_SelectedIndexChanged", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub cboFieldValue2_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboFieldValue2.SelectedIndexChanged
        Try
            Dim dsList As New DataSet
            dsList = objCoyField3.getComboList("List", True, CInt(cboFieldValue2.SelectedValue))
            With cboFieldValue3
                .ValueMember = "Id"
                .DisplayMember = "Name"
                .DataSource = dsList.Tables("List")
                .SelectedValue = 0
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboFieldValue2_SelectedIndexChanged", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub cboFieldValue3_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboFieldValue3.SelectedIndexChanged
        Try
            Dim dsList As New DataSet
            dsList = objCoyField4.getComboList("List", True, CInt(cboFieldValue3.SelectedValue))
            With cboFieldValue4
                .ValueMember = "Id"
                .DisplayMember = "Name"
                .DataSource = dsList.Tables("List")
                .SelectedValue = 0
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboFieldValue3_SelectedIndexChanged", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub cboFieldValue4_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboFieldValue4.SelectedIndexChanged
        Try
            Dim dsList As New DataSet
            dsList = objCoyField5.getComboList("List", True, CInt(cboFieldValue4.SelectedValue))
            With cboFieldValue5
                .ValueMember = "Id"
                .DisplayMember = "Name"
                .DataSource = dsList.Tables("List")
                .SelectedValue = 0
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboFieldValue4_SelectedIndexChanged", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Private Sub OtherSettings()
		Try
			Me.SuspendLayout()
			
			Call SetLanguage()
			
			Me.gbFilterCriteria.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbFilterCriteria.ForeColor = GUI._eZeeContainerHeaderForeColor 


			Me.btnClose.GradientBackColor = GUI._ButttonBackColor 
			Me.btnClose.GradientForeColor = GUI._ButttonFontColor

			Me.btnOperation.GradientBackColor = GUI._ButttonBackColor 
			Me.btnOperation.GradientForeColor = GUI._ButttonFontColor


			Me.ResumeLayout()
		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "OtherSettings", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetLanguage()
		Try
			Me.Text = Language._Object.getCaption(Me.Name, Me.Text)
			
			Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
			Me.gbFilterCriteria.Text = Language._Object.getCaption(Me.gbFilterCriteria.Name, Me.gbFilterCriteria.Text)
			Me.lblPerspective.Text = Language._Object.getCaption(Me.lblPerspective.Name, Me.lblPerspective.Text)
			Me.lblEndDate.Text = Language._Object.getCaption(Me.lblEndDate.Name, Me.lblEndDate.Text)
			Me.lblStartDate.Text = Language._Object.getCaption(Me.lblStartDate.Name, Me.lblStartDate.Text)
			Me.lblStatus.Text = Language._Object.getCaption(Me.lblStatus.Name, Me.lblStatus.Text)
			Me.btnOperation.Text = Language._Object.getCaption(Me.btnOperation.Name, Me.btnOperation.Text)
			Me.mnuCommit.Text = Language._Object.getCaption(Me.mnuCommit.Name, Me.mnuCommit.Text)
			Me.mnuUnlock.Text = Language._Object.getCaption(Me.mnuUnlock.Name, Me.mnuUnlock.Text)
			Me.lblGoalOwner.Text = Language._Object.getCaption(Me.lblGoalOwner.Name, Me.lblGoalOwner.Text)
			Me.lblPeriod.Text = Language._Object.getCaption(Me.lblPeriod.Name, Me.lblPeriod.Text)

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetLanguage", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "Sorry, there is no")
			Language.setMessage(mstrModuleName, 2, "defined. Please define")
			Language.setMessage(mstrModuleName, 3, "in order to perform edit operation.")
			Language.setMessage(mstrModuleName, 4, "in order to perform delete operation.")
			Language.setMessage(mstrModuleName, 5, "Add")
			Language.setMessage(mstrModuleName, 6, "Edit")
			Language.setMessage(mstrModuleName, 7, "Delete")
			Language.setMessage(mstrModuleName, 8, "Sorry, No data defined for asseessment caption settings screen.")
			Language.setMessage(mstrModuleName, 9, "Sorry no field is mapped with the selected period.")
			Language.setMessage(mstrModuleName, 10, "Sorry, there is no goal defined in order to commit.")
			Language.setMessage(mstrModuleName, 11, "Sorry, you cannot set the same operational status again. Reason : Company goals are already committed.")
			Language.setMessage(mstrModuleName, 12, "You are about to commit the company goals. This will tranfer all goals to goal owners.")
			Language.setMessage(mstrModuleName, 13, "Due to this you will not be allowed to do any operation(s) on company goals.")
			Language.setMessage(mstrModuleName, 14, "Do you wish to continue?")
			Language.setMessage(mstrModuleName, 15, "Sorry, there is no goal defined in order to unlock.")
			Language.setMessage(mstrModuleName, 16, "Sorry you cannot unlock company goals. Reson : Company goals are not committed.")
			Language.setMessage(mstrModuleName, 17, "Sorry, you cannot set the same operational status again. Reason : Company goals are already uncommitted.")
			Language.setMessage(mstrModuleName, 18, "You are about to unlock the company goals. This will result in opening of all operation on company goals.")
			Language.setMessage(mstrModuleName, 19, "Do you wish to continue?")
			Language.setMessage(mstrModuleName, 20, "Enter reason to unlock")
			Language.setMessage(mstrModuleName, 21, "Sorry, Unlock reason is mandaroty information.")
			Language.setMessage(mstrModuleName, 22, "You are about to commit the company goals.")
			Language.setMessage(mstrModuleName, 23, "You are about to remove the information from company level, this will delete all child linked to this parent")
			Language.setMessage(mstrModuleName, 24, "Are you sure you want to delete?")
			Language.setMessage(mstrModuleName, 25, "Period is mandatory information. Please select Period to continue.")
			Language.setMessage(mstrModuleName, 26, "Select")
			Language.setMessage(mstrModuleName, 27, "Add")
			Language.setMessage(mstrModuleName, 28, "Edit")
			Language.setMessage(mstrModuleName, 29, "Delete")
			Language.setMessage(mstrModuleName, 30, "Total Weight Assigned :")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class