﻿Option Strict On

#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data

#End Region

Public Class frmBSC_Planning

#Region " Private Variables "

    Private ReadOnly mstrModuleName As String = "frmBSC_Planning"
    Private mintObjectiveUnkid As Integer = -1
    Private mstrObjectiveName As String = String.Empty
    Private ObjEvaluation As New clsBSC_Analysis_Master
    'S.SANDEEP [ 05 MARCH 2012 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Private objObjective As clsObjective_Master
    'S.SANDEEP [ 05 MARCH 2012 ] -- END

#End Region

#Region " Display Dialog "

    Public Function displayDialog(ByRef intUnkId As Integer, ByVal mstrObjective As String) As Boolean
        Try
            mintObjectiveUnkid = intUnkId
            mstrObjectiveName = mstrObjective

            Me.ShowDialog()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "displayDialog", mstrModuleName)
        End Try
    End Function

#End Region

#Region " Private Methods "

    Private Sub Fill_List()
        Dim dtTable As DataTable
        Try
            dtTable = ObjEvaluation.GetBSCPlanning(mintObjectiveUnkid)
            lvPlanning.Items.Clear()

            Dim lvItem As ListViewItem

            For Each dRow As DataRow In dtTable.Rows
                lvItem = New ListViewItem

                lvItem.Text = dRow.Item("KPI").ToString
                lvItem.SubItems.Add(dRow.Item("Target").ToString)
                lvItem.SubItems.Add(dRow.Item("Initiative").ToString)

                lvPlanning.Items.Add(lvItem)
            Next

            lvPlanning.GridLines = False

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Fill_List", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

#Region " Form's Events "

    Private Sub frmBSC_Planning_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        ObjEvaluation = New clsBSC_Analysis_Master
        'S.SANDEEP [ 05 MARCH 2012 ] -- START
        'ENHANCEMENT : TRA CHANGES
        objObjective = New clsObjective_Master
        'S.SANDEEP [ 05 MARCH 2012 ] -- END
        Try
            txtObjective.Text = mstrObjectiveName
            objObjective._Objectiveunkid = mintObjectiveUnkid

            If CBool(objObjective._Isfinal) = True Then
                lnkAddKPI.Enabled = False
                lnkAddTargets.Enabled = False
                lnkIntiatives.Enabled = False
            Else
                lnkAddKPI.Enabled = True
                lnkAddTargets.Enabled = True
                lnkIntiatives.Enabled = True
            End If

            Call Fill_List()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmBSC_Planning_Load", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsObjective_Master.SetMessages()
            objfrm._Other_ModuleNames = "clsObjective_Master"
            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

#Region " Links Events "

    Private Sub lnkAddKPI_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lnkAddKPI.LinkClicked
        Dim frm As New frmBSC_KPIMeasureAddEdit
        Try
            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If
            frm.displayDialog(-1, enAction.ADD_ONE)
            Call Fill_List()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lnkAddKPI_LinkClicked", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub lnkIntiatives_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lnkIntiatives.LinkClicked
        Dim frm As New frmBSC_InitiativeAddEdit
        Try
            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If
            frm.displayDialog(-1, enAction.ADD_ONE)
            Call Fill_List()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lnkIntiatives_LinkClicked", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub lnkAddTargets_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lnkAddTargets.LinkClicked
        Dim frm As New frmBSC_TargetAddEdit
        Try
            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If
            frm.displayDialog(-1, enAction.ADD_ONE)
            Call Fill_List()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lnkAddTargets_LinkClicked", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

#End Region

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Private Sub OtherSettings()
        Try
            Me.SuspendLayout()

            Call SetLanguage()

            Me.btnClose.GradientBackColor = GUI._ButttonBackColor
            Me.btnClose.GradientForeColor = GUI._ButttonFontColor


            Me.ResumeLayout()
        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "OtherSettings", mstrModuleName)
        End Try
    End Sub


    Private Sub SetLanguage()
        Try
            Me.Text = Language._Object.getCaption(Me.Name, Me.Text)

            Me.colhKPI.Text = Language._Object.getCaption(CStr(Me.colhKPI.Tag), Me.colhKPI.Text)
            Me.colhTargets.Text = Language._Object.getCaption(CStr(Me.colhTargets.Tag), Me.colhTargets.Text)
            Me.colhinitiatives.Text = Language._Object.getCaption(CStr(Me.colhinitiatives.Tag), Me.colhinitiatives.Text)
            Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
            Me.lblObjective.Text = Language._Object.getCaption(Me.lblObjective.Name, Me.lblObjective.Text)
            Me.lnkIntiatives.Text = Language._Object.getCaption(Me.lnkIntiatives.Name, Me.lnkIntiatives.Text)
            Me.lnkAddTargets.Text = Language._Object.getCaption(Me.lnkAddTargets.Name, Me.lnkAddTargets.Text)
            Me.lnkAddKPI.Text = Language._Object.getCaption(Me.lnkAddKPI.Name, Me.lnkAddKPI.Text)

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
        End Try
    End Sub


    Private Sub SetMessages()
        Try

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class