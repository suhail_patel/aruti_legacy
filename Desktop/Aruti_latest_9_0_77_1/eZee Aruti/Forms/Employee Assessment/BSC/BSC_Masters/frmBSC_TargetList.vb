﻿Option Strict On

#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data
#End Region



Public Class frmBSC_TargetList

#Region " Private Varaibles "

    Private objTargetsMaster As clstarget_master
    Private ReadOnly mstrModuleName As String = "frmBSC_TargetList"
    'S.SANDEEP [ 28 DEC 2012 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Dim objWSetting As New clsWeight_Setting(True)
    Dim intWidth As Integer = 125
    'S.SANDEEP [ 28 DEC 2012 ] -- END

#End Region

#Region " Private Function "

    Private Sub SetVisibility()
        Try

            'S.SANDEEP [28 MAY 2015] -- START
            'ENHANCEMENT : NEW PRIVILEGE (Performance Assessment)

            ''S.SANDEEP [ 16 MAY 2012 ] -- START
            ''ENHANCEMENT : TRA USER PRIVILEGE CHANGES
            'btnNew.Enabled = User._Object.Privilege._AllowToAddBSCTargets
            'btnEdit.Enabled = User._Object.Privilege._AllowToEditBSCTargets
            'btnDelete.Enabled = User._Object.Privilege._AllowToDeleteBSCTargets
            ''S.SANDEEP [ 16 MAY 2012 ] -- END

            'S.SANDEEP [28 MAY 2015] -- END



            'S.SANDEEP [ 28 DEC 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            If objWSetting._Weight_Optionid <> enWeight_Options.WEIGHT_EACH_ITEM Then
                If objWSetting._Weight_Typeid <> enWeight_Types.WEIGHT_FIELD3 Then
                    colhWeight.Width = 0
                    intWidth = 185
                    colhDescription.Width = intWidth
                End If                
            End If
            'S.SANDEEP [ 28 DEC 2012 ] -- END

            'S.SANDEEP [ 20 JULY 2013 ] -- START
            'ENHANCEMENT : TRA CHANGES
            If ConfigParameter._Object._IsBSC_ByEmployee = True Then
                btnOperation.Visible = True
            Else
                btnOperation.Visible = False
            End If
            'S.SANDEEP [ 20 JULY 2013 ] -- END
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetVisibility", mstrModuleName)
        End Try
    End Sub

    Private Sub FillCombo()
        Dim dsList As New DataSet
        'S.SANDEEP [ 28 DEC 2012 ] -- START
        'ENHANCEMENT : TRA CHANGES
        Dim objPeriod As New clscommom_period_Tran
        'S.SANDEEP [ 28 DEC 2012 ] -- END
        Try

            'Pinkal (20-Jan-2012) -- Start
            'Enhancement : TRA Changes

            If ConfigParameter._Object._IsBSC_ByEmployee Then

                Dim objEmployee As New clsEmployee_Master
                'Sohail (23 Nov 2012) -- Start
                'TRA - ENHANCEMENT
                'dsList = objEmployee.GetEmployeeList("Employee", True, True, , , , , , , , , , , , ConfigParameter._Object._CurrentDateAndTime.Date, ConfigParameter._Object._CurrentDateAndTime.Date)

                'S.SANDEEP [04 JUN 2015] -- START
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS

                'If ConfigParameter._Object._IsIncludeInactiveEmp = False Then
                '    dsList = objEmployee.GetEmployeeList("Employee", True, True, , , , , , , , , , , , eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate))
                'Else
                '    dsList = objEmployee.GetEmployeeList("Employee", True, True, , , , , , , , , , , , ConfigParameter._Object._CurrentDateAndTime.Date, ConfigParameter._Object._CurrentDateAndTime.Date)
                'End If
                ''Sohail (23 Nov 2012) -- End
                'With cboEmployee
                '    .ValueMember = "employeeunkid"
                '    .DisplayMember = "employeename"
                '    .DataSource = dsList.Tables(0)
                'End With

                'S.SANDEEP [04 JUN 2015] -- END

            Else

            Dim objObjective As New clsObjective_Master
            dsList = objObjective.getComboList("List", True)
            With cboObjective
                .ValueMember = "id"
                .DisplayMember = "name"
                .DataSource = dsList.Tables("List")
            End With
            End If
            'Pinkal (20-Jan-2012) -- End

            'S.SANDEEP [ 12 JUNE 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            Dim objKPI As New clsKPI_Master
            dsList = objKPI.getComboList("KPI", True)
            With cboKPI
                .ValueMember = "id"
                .DisplayMember = "name"
                .DataSource = dsList.Tables("KPI")
            End With
            'S.SANDEEP [ 12 JUNE 2012 ] -- END

            'S.SANDEEP [ 28 DEC 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES

            'S.SANDEEP [ 09 JULY 2013 ] -- START
            'ENHANCEMENT : OTHER CHANGES
            'dsList = objPeriod.getListForCombo(enModuleReference.Assessment, FinancialYear._Object._YearUnkid, "List", True, enStatusType.Open)
            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'dsList = objPeriod.getListForCombo(enModuleReference.Assessment, 0, "List", True, enStatusType.Open)
            dsList = objPeriod.getListForCombo(enModuleReference.Assessment, 0, FinancialYear._Object._DatabaseName, FinancialYear._Object._Database_Start_Date, "List", True, enStatusType.Open)
            'Sohail (21 Aug 2015) -- End
            'S.SANDEEP [ 09 JULY 2013 ] -- END
            With cboPeriod
                .ValueMember = "periodunkid"
                .DisplayMember = "name"
                .DataSource = dsList.Tables(0)
            End With
            'S.SANDEEP [ 28 DEC 2012 ] -- END

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        End Try
    End Sub

    Private Sub FillList()
        Dim strSearching As String = ""
        Dim dsList As New DataSet
        Dim dtTable As New DataTable
        Try
            If CInt(cboPeriod.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 9, "Sorry Period is mandatory information. Please select period to continue."), enMsgBoxStyle.Information)
                Exit Sub
            End If

            'S.SANDEEP [28 MAY 2015] -- START
            'ENHANCEMENT : NEW PRIVILEGE (Performance Assessment)
            'If User._Object.Privilege._AllowToViewBSCTargetsList = True Then  'Pinkal (09-Jul-2012) -- Start
            'S.SANDEEP [28 MAY 2015] -- END



            dsList = objTargetsMaster.GetList("List")


            'Pinkal (20-Jan-2012) -- Start
            'Enhancement : TRA Changes

            If ConfigParameter._Object._IsBSC_ByEmployee Then
                If CInt(cboEmployee.SelectedValue) > 0 Then
                    strSearching &= "AND employeeunkid = '" & CInt(cboEmployee.SelectedValue) & "' "
                End If
            End If

            'Pinkal (20-Jan-2012) -- End


            If CInt(cboObjective.SelectedValue) > 0 Then
                strSearching &= "AND objectiveunkid = '" & CInt(cboObjective.SelectedValue) & "' "
            End If


                If CInt(cboPeriod.SelectedValue) > 0 Then
                    strSearching &= "AND periodunkid = '" & CInt(cboPeriod.SelectedValue) & "' "
                End If



            'Pinkal (20-Jan-2012) -- Start
            'Enhancement : TRA Changes

            'S.SANDEEP [ 12 JUNE 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            If CInt(cboKPI.SelectedValue) > 0 Then
                strSearching &= "AND kpiunkid = '" & CInt(cboKPI.SelectedValue) & "' "
            End If
            'S.SANDEEP [ 12 JUNE 2012 ] -- END


            If ConfigParameter._Object._IsBSC_ByEmployee Then
                If strSearching.Length > 0 Then
                    strSearching = strSearching.Substring(3)
                        'S.SANDEEP [ 31 AUG 2012 ] -- START
                        'ENHANCEMENT : TRA CHANGES
                        'dtTable = New DataView(dsList.Tables("List"), strSearching, "employee asc", DataViewRowState.CurrentRows).ToTable
                        dtTable = New DataView(dsList.Tables("List"), strSearching, "perspectiveunkid,ocode,kcode", DataViewRowState.CurrentRows).ToTable
                        'S.SANDEEP [ 31 AUG 2012 ] -- END
                Else
                        'S.SANDEEP [ 31 AUG 2012 ] -- START
                        'ENHANCEMENT : TRA CHANGES
                        'dtTable = New DataView(dsList.Tables("List"), "", "employee asc", DataViewRowState.CurrentRows).ToTable
                        dtTable = New DataView(dsList.Tables("List"), "", "perspectiveunkid,ocode,kcode", DataViewRowState.CurrentRows).ToTable
                        'S.SANDEEP [ 31 AUG 2012 ] -- END
            End If
            Else
            If strSearching.Length > 0 Then
                strSearching = strSearching.Substring(3)
                        'S.SANDEEP [ 31 AUG 2012 ] -- START
                        'ENHANCEMENT : TRA CHANGES
                        'dtTable = New DataView(dsList.Tables("List"), strSearching, "", DataViewRowState.CurrentRows).ToTable
                        dtTable = New DataView(dsList.Tables("List"), strSearching, "perspectiveunkid,ocode,kcode", DataViewRowState.CurrentRows).ToTable
                        'S.SANDEEP [ 31 AUG 2012 ] -- END
            Else
                        'S.SANDEEP [ 31 AUG 2012 ] -- START
                        'ENHANCEMENT : TRA CHANGES
                        'dtTable = dsList.Tables("List")
                        dtTable = New DataView(dsList.Tables("List"), "", "perspectiveunkid,ocode,kcode", DataViewRowState.CurrentRows).ToTable
                        'S.SANDEEP [ 31 AUG 2012 ] -- END
            End If
            End If

            'Pinkal (20-Jan-2012) -- End
            

            Dim lvItem As ListViewItem

            lvTarget.Items.Clear()

            For Each drRow As DataRow In dtTable.Rows
                lvItem = New ListViewItem

                lvItem.Text = drRow.Item("objective").ToString


                'S.SANDEEP [ 12 JUNE 2012 ] -- START
                'ENHANCEMENT : TRA CHANGES

                'S.SANDEEP [ 05 MARCH 2012 ] -- START
                'ENHANCEMENT : TRA CHANGES
                lvItem.SubItems.Add(drRow.Item("KPI").ToString)
                'lvItem.SubItems.Add("")
                'S.SANDEEP [ 05 MARCH 2012 ] -- END

                'S.SANDEEP [ 12 JUNE 2012 ] -- END

                

                lvItem.SubItems.Add(drRow.Item("code").ToString)
                lvItem.SubItems.Add(drRow.Item("name").ToString)
                lvItem.SubItems.Add(drRow.Item("description").ToString)
                lvItem.Tag = drRow.Item("targetunkid")

                If ConfigParameter._Object._IsBSC_ByEmployee Then
                        lvItem.SubItems.Add(drRow.Item("employee").ToString & Language.getMessage(mstrModuleName, 7, " -> Period : ") & drRow.Item("Period").ToString)
                Else
                        lvItem.SubItems.Add(drRow.Item("Period").ToString)
                End If

                If CBool(drRow.Item("isfinal")) = True Then
                    lvItem.ForeColor = Color.Blue
                End If

                    If CInt(drRow.Item("STypId")) = enObjective_Status.SUBMIT_APPROVAL Then
                        lvItem.ForeColor = Color.Green
                    End If

                    'S.SANDEEP [ 28 DEC 2012 ] -- START
                    'ENHANCEMENT : TRA CHANGES
                    lvItem.SubItems.Add(drRow.Item("target_weight").ToString)
                    'S.SANDEEP [ 28 DEC 2012 ] -- END

                lvTarget.Items.Add(lvItem)
            Next

            If ConfigParameter._Object._IsBSC_ByEmployee Then
                lvTarget.GroupingColumn = objColhEmployee
                lvTarget.DisplayGroups(True)
            End If


                'S.SANDEEP [ 28 DEC 2012 ] -- START
                'ENHANCEMENT : TRA CHANGES
                'If lvTarget.Items.Count > 8 Then
                '    colhDescription.Width = 160 - 18
                'Else
                '    colhDescription.Width = 160
                'End If
                If lvTarget.Items.Count > 16 Then
                    colhDescription.Width = intWidth - 18
            Else
                    colhDescription.Width = intWidth
            End If
                'S.SANDEEP [ 28 DEC 2012 ] -- END

                

            'End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Fill_List", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

#Region " Form's Events "

    Private Sub frmBSC_TargetList_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        objTargetsMaster = Nothing
    End Sub


    Private Sub frmBSC_TargetList_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles Me.KeyPress
        If Asc(e.KeyChar) = 27 Then
            Me.Close()
        End If
    End Sub


    Private Sub frmBSC_TargetList_KeyUp(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyUp
        If e.KeyCode = Keys.Delete And lvTarget.Focused = True Then
            Call btnDelete.PerformClick()
        End If
    End Sub

    Private Sub frmBSC_TargetList_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        objTargetsMaster = New clstarget_master
        Try
            Call Set_Logo(Me, gApplicationType)

            Call SetVisibility()

            Language.setLanguage(Me.Name)

            Call OtherSettings()


            'Pinkal (20-Jan-2012) -- Start
            'Enhancement : TRA Changes
            If ConfigParameter._Object._IsBSC_ByEmployee Then
                pnlEmployee.Visible = True
            Else
                pnlEmployee.Visible = False
            End If
            lvTarget.GridLines = False
            'Pinkal (20-Jan-2012) -- End

            Call FillCombo()

            If lvTarget.Items.Count > 0 Then lvTarget.Items(0).Selected = True
            lvTarget.Select()

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmBSC_TargetList_Load", mstrModuleName)
        End Try
    End Sub

    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsObjective_Master.SetMessages()
            objfrm._Other_ModuleNames = "clstarget_master"
            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub
#End Region

#Region " Buttons "

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Me.Close()
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "btnClose_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDelete.Click
        If lvTarget.SelectedItems.Count < 1 Then
            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please select Targets from the list to perform further operation on it."), enMsgBoxStyle.Information) '?1
            lvTarget.Select()
            Exit Sub
        End If

        Try

            If objTargetsMaster.isUsed(CInt(lvTarget.SelectedItems(0).Tag)) Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Sorry, You cannot delete this Target. Reason: This Target is in use."), enMsgBoxStyle.Information) '?2
                lvTarget.Select()
                Exit Sub
            End If

            If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Are you sure you want to delete this Targets?"), CType(enMsgBoxStyle.Question + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.Yes Then
                objTargetsMaster.Delete(CInt(lvTarget.SelectedItems(0).Tag))
                If objTargetsMaster._Message <> "" Then
                    eZeeMsgBox.Show(objTargetsMaster._Message, enMsgBoxStyle.Information)
                Else
                    lvTarget.SelectedItems(0).Remove()
                End If

                If lvTarget.Items.Count <= 0 Then
                    Exit Try
                End If

            End If
            lvTarget.Select()
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "btnDelete_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnEdit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEdit.Click
        If lvTarget.SelectedItems.Count < 1 Then
            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please select Targets from the list to perform further operation on it."), enMsgBoxStyle.Information) '?1
            lvTarget.Select()
            Exit Sub
        End If
        Dim frm As New frmBSC_TargetAddEdit
        Try

            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If

            If frm.displayDialog(CInt(lvTarget.SelectedItems(0).Tag), enAction.EDIT_ONE) Then
                Call FillList()
            End If
            frm = Nothing
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "btnEdit_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub btnNew_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNew.Click
        Dim frm As New frmBSC_TargetAddEdit
        Try

            'S.SANDEEP [ 28 DEC 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            If objWSetting.Is_Setting_Present(-1) = False Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Sorry, No weightage setting defined for Balance Score Card. Please define weightage setting."), enMsgBoxStyle.Information)
                Exit Sub
            End If
            'S.SANDEEP [ 28 DEC 2012 ] -- END

            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If


            If frm.displayDialog(-1, enAction.ADD_CONTINUE) Then
                Call FillList()
            End If

        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "btnNew_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub objbtnSearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearch.Click
        Try
            Call FillList()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearch_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnReset.Click
        Try

            'Pinkal (20-Jan-2012) -- Start
            'Enhancement : TRA Changes
            If ConfigParameter._Object._IsBSC_ByEmployee Then
                cboEmployee.SelectedValue = 0
            End If
            'Pinkal (20-Jan-2012) -- End

            cboObjective.SelectedValue = 0

            'S.SANDEEP [ 12 JUNE 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            cboKPI.SelectedValue = 0
            cboPeriod.SelectedValue = 0
            'S.SANDEEP [ 12 JUNE 2012 ] -- END
            lvTarget.Items.Clear()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnReset_Click", mstrModuleName)
        End Try
    End Sub

    'Pinkal (20-Jan-2012) -- Start
    'Enhancement : TRA Changes
    Private Sub objbtnSearchEmployee_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchEmployee.Click
        Dim frm As New frmCommonSearch
        Try
            With frm
                .ValueMember = cboEmployee.ValueMember
                .DisplayMember = cboEmployee.DisplayMember
                .CodeMember = "employeecode"
                .DataSource = CType(cboEmployee.DataSource, DataTable)
            End With

            If frm.DisplayDialog Then
                cboEmployee.SelectedValue = frm.SelectedValue
                cboEmployee.Focus()
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchEmployee_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub
    'Pinkal (20-Jan-2012) -- End

    'S.SANDEEP [ 12 JUNE 2012 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Private Sub objbtnSearchKPI_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchKPI.Click
        Dim frm As New frmCommonSearch
        Try
            With frm
                .ValueMember = cboKPI.ValueMember
                .DisplayMember = cboKPI.DisplayMember
                .DataSource = CType(cboKPI.DataSource, DataTable)
            End With

            If frm.DisplayDialog Then
                cboKPI.SelectedValue = frm.SelectedValue
                cboKPI.Focus()
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchKPI_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub objbtnSearchObjective_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchObjective.Click
        Dim frm As New frmCommonSearch
        Try
            With frm
                .ValueMember = cboObjective.ValueMember
                .DisplayMember = cboObjective.DisplayMember
                .DataSource = CType(cboObjective.DataSource, DataTable)
            End With

            If frm.DisplayDialog Then
                cboObjective.SelectedValue = frm.SelectedValue
                cboObjective.Focus()
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchObjective_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub
    'S.SANDEEP [ 12 JUNE 2012 ] -- END

#End Region

#Region "Combobox Event"

    Private Sub cboEmployee_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboEmployee.SelectedIndexChanged, cboPeriod.SelectedIndexChanged
        Try
            Dim objObjective As New clsObjective_Master
            Dim dsList As DataSet = objObjective.getComboList("List", True, , CInt(cboEmployee.SelectedValue), CInt(cboPeriod.SelectedValue))
            cboObjective.DisplayMember = "name"
            cboObjective.ValueMember = "id"
            cboObjective.DataSource = dsList.Tables("List")
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboEmployee_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub
#End Region

    'S.SANDEEP [ 05 MARCH 2012 ] -- START
    'ENHANCEMENT : TRA CHANGES
#Region " ListView Events "

    Private Sub lvTarget_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lvTarget.SelectedIndexChanged
        Try
            If lvTarget.SelectedItems.Count <= 0 Then Exit Sub
            If lvTarget.SelectedItems(0).ForeColor = Color.Blue Or lvTarget.SelectedItems(0).ForeColor = Color.Green Then
                btnEdit.Enabled = False : btnDelete.Enabled = False
            Else
                btnEdit.Enabled = True : btnDelete.Enabled = True
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lvTarget_SelectedIndexChanged", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region
    'S.SANDEEP [ 05 MARCH 2012 ] -- END

    'S.SANDEEP [ 20 JULY 2013 ] -- START
    'ENHANCEMENT : TRA CHANGES
#Region " Controls Events "

    Private Sub mnuGetFileFormat_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuGetFileFormat.Click
        'Dim IExcel As New ExcelData
        Dim dsList As New DataSet
        Dim path As String = String.Empty
        Dim strFilePath As String = String.Empty
        Dim dlgSaveFile As New SaveFileDialog
        Dim ObjFile As System.IO.FileInfo
        Try

            dlgSaveFile.Filter = "Execl files(*.xlsx)|*.xlsx"
            If dlgSaveFile.ShowDialog = Windows.Forms.DialogResult.OK Then
                ObjFile = New System.IO.FileInfo(dlgSaveFile.FileName)
                strFilePath = ObjFile.DirectoryName & "\"
                strFilePath &= ObjFile.Name.Substring(0, ObjFile.Name.Length - 4) & "_" & eZeeDate.convertDate(Now)
                strFilePath &= ObjFile.Extension
                Dim dTable As New DataTable

                dTable.Columns.Add("Employeecode", System.Type.GetType("System.String")) : dTable.Columns.Add("Objective_Code", System.Type.GetType("System.String")) : dTable.Columns.Add("KPI_Code", System.Type.GetType("System.String"))
                dTable.Columns.Add("Target_Code", System.Type.GetType("System.String")) : dTable.Columns.Add("Target_Name", System.Type.GetType("System.String")) : dTable.Columns.Add("Period", System.Type.GetType("System.String"))
                dTable.Columns.Add("Weight", System.Type.GetType("System.Decimal"))

                dsList.Tables.Add(dTable.Copy)
                'IExcel.Export(strFilePath, dsList)
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 8, "Template Exported Successfully."), enMsgBoxStyle.Information)
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "mnuGetFileFormat_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub mnuImportTargets_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuImportTargets.Click
        Try
            Dim frm As New frmImport_Target_Wizard
            frm.ShowDialog()
            Call FillList()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "mnuImportTargets_Click", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region
    'S.SANDEEP [ 20 JULY 2013 ] -- END

	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Private Sub OtherSettings()
		Try
			Me.SuspendLayout()
			
			Call SetLanguage()
			
			Me.gbFilterCriteria.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbFilterCriteria.ForeColor = GUI._eZeeContainerHeaderForeColor 

            Me.eZeeHeader.GradientColor1 = GUI._HeaderBackColor1
			Me.eZeeHeader.GradientColor2 = GUI._HeaderBackColor2 
			Me.eZeeHeader.BorderColor = GUI._HeaderBorderColor 
			Me.eZeeHeader.HeaderTextForeColor = GUI._HeaderTitleFontColor 
			Me.eZeeHeader.DescriptionForeColor = GUI._HeaderMessageFontColor 


			Me.btnDelete.GradientBackColor = GUI._ButttonBackColor 
			Me.btnDelete.GradientForeColor = GUI._ButttonFontColor

			Me.btnEdit.GradientBackColor = GUI._ButttonBackColor 
			Me.btnEdit.GradientForeColor = GUI._ButttonFontColor

			Me.btnNew.GradientBackColor = GUI._ButttonBackColor 
			Me.btnNew.GradientForeColor = GUI._ButttonFontColor

			Me.btnClose.GradientBackColor = GUI._ButttonBackColor 
			Me.btnClose.GradientForeColor = GUI._ButttonFontColor

			Me.btnOperation.GradientBackColor = GUI._ButttonBackColor 
			Me.btnOperation.GradientForeColor = GUI._ButttonFontColor


			Me.ResumeLayout()
		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "OtherSettings", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetLanguage()
		Try
			Me.Text = Language._Object.getCaption(Me.Name, Me.Text)
			
			Me.colhObjective.Text = Language._Object.getCaption(CStr(Me.colhObjective.Tag), Me.colhObjective.Text)
			Me.colhCode.Text = Language._Object.getCaption(CStr(Me.colhCode.Tag), Me.colhCode.Text)
			Me.colhName.Text = Language._Object.getCaption(CStr(Me.colhName.Tag), Me.colhName.Text)
			Me.colhDescription.Text = Language._Object.getCaption(CStr(Me.colhDescription.Tag), Me.colhDescription.Text)
			Me.gbFilterCriteria.Text = Language._Object.getCaption(Me.gbFilterCriteria.Name, Me.gbFilterCriteria.Text)
			Me.lblObjective.Text = Language._Object.getCaption(Me.lblObjective.Name, Me.lblObjective.Text)
			Me.btnDelete.Text = Language._Object.getCaption(Me.btnDelete.Name, Me.btnDelete.Text)
			Me.btnEdit.Text = Language._Object.getCaption(Me.btnEdit.Name, Me.btnEdit.Text)
			Me.btnNew.Text = Language._Object.getCaption(Me.btnNew.Name, Me.btnNew.Text)
			Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
			Me.eZeeHeader.Title = Language._Object.getCaption(Me.eZeeHeader.Name & "_Title" , Me.eZeeHeader.Title)
			Me.eZeeHeader.Message = Language._Object.getCaption(Me.eZeeHeader.Name & "_Message" , Me.eZeeHeader.Message)
			Me.colhKPI.Text = Language._Object.getCaption(CStr(Me.colhKPI.Tag), Me.colhKPI.Text)
			Me.lblEmployee.Text = Language._Object.getCaption(Me.lblEmployee.Name, Me.lblEmployee.Text)
			Me.lblKPI.Text = Language._Object.getCaption(Me.lblKPI.Name, Me.lblKPI.Text)
			Me.colhWeight.Text = Language._Object.getCaption(CStr(Me.colhWeight.Tag), Me.colhWeight.Text)
			Me.lblPeriod.Text = Language._Object.getCaption(Me.lblPeriod.Name, Me.lblPeriod.Text)
			Me.btnOperation.Text = Language._Object.getCaption(Me.btnOperation.Name, Me.btnOperation.Text)
			Me.mnuGetFileFormat.Text = Language._Object.getCaption(Me.mnuGetFileFormat.Name, Me.mnuGetFileFormat.Text)
			Me.mnuImportTargets.Text = Language._Object.getCaption(Me.mnuImportTargets.Name, Me.mnuImportTargets.Text)

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetLanguage", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "Please select Targets from the list to perform further operation on it.")
			Language.setMessage(mstrModuleName, 2, "Sorry, You cannot delete this Target. Reason: This Target is in use.")
			Language.setMessage(mstrModuleName, 3, "Are you sure you want to delete this Targets?")
			Language.setMessage(mstrModuleName, 4, "Sorry, No weightage setting defined for Balance Score Card. Please define weightage setting.")
			Language.setMessage(mstrModuleName, 7, " -> Period :")
			Language.setMessage(mstrModuleName, 8, "Template Exported Successfully.")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
    End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class