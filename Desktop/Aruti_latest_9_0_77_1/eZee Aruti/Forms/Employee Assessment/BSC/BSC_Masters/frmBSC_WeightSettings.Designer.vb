﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmBSC_WeightSettings
    Inherits eZee.Common.eZeeForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmBSC_WeightSettings))
        Me.Panel1 = New System.Windows.Forms.Panel
        Me.objFooter = New eZee.Common.eZeeFooter
        Me.btnSave = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnClose = New eZee.Common.eZeeLightButton(Me.components)
        Me.gbWeightSetting = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.radObjective = New System.Windows.Forms.RadioButton
        Me.radInitiative = New System.Windows.Forms.RadioButton
        Me.radKPI = New System.Windows.Forms.RadioButton
        Me.radTargets = New System.Windows.Forms.RadioButton
        Me.radOption2 = New System.Windows.Forms.RadioButton
        Me.radOption1 = New System.Windows.Forms.RadioButton
        Me.objgbTypes = New System.Windows.Forms.GroupBox
        Me.Panel1.SuspendLayout()
        Me.objFooter.SuspendLayout()
        Me.gbWeightSetting.SuspendLayout()
        Me.objgbTypes.SuspendLayout()
        Me.SuspendLayout()
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(Me.gbWeightSetting)
        Me.Panel1.Controls.Add(Me.objFooter)
        Me.Panel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel1.Location = New System.Drawing.Point(0, 0)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(317, 226)
        Me.Panel1.TabIndex = 0
        '
        'objFooter
        '
        Me.objFooter.BorderColor = System.Drawing.Color.Silver
        Me.objFooter.Controls.Add(Me.btnSave)
        Me.objFooter.Controls.Add(Me.btnClose)
        Me.objFooter.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.objFooter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objFooter.GradiantStyle = eZee.Common.eZeeFooter.GradientStyle.Up
        Me.objFooter.GradientColor1 = System.Drawing.SystemColors.Control
        Me.objFooter.GradientColor2 = System.Drawing.SystemColors.Control
        Me.objFooter.Location = New System.Drawing.Point(0, 171)
        Me.objFooter.Name = "objFooter"
        Me.objFooter.Size = New System.Drawing.Size(317, 55)
        Me.objFooter.TabIndex = 2
        '
        'btnSave
        '
        Me.btnSave.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnSave.BackColor = System.Drawing.Color.White
        Me.btnSave.BackgroundImage = CType(resources.GetObject("btnSave.BackgroundImage"), System.Drawing.Image)
        Me.btnSave.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnSave.BorderColor = System.Drawing.Color.Empty
        Me.btnSave.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnSave.FlatAppearance.BorderSize = 0
        Me.btnSave.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnSave.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSave.ForeColor = System.Drawing.Color.Black
        Me.btnSave.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnSave.GradientForeColor = System.Drawing.Color.Black
        Me.btnSave.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSave.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnSave.Location = New System.Drawing.Point(107, 13)
        Me.btnSave.Name = "btnSave"
        Me.btnSave.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSave.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnSave.Size = New System.Drawing.Size(97, 30)
        Me.btnSave.TabIndex = 7
        Me.btnSave.Text = "&Save"
        Me.btnSave.UseVisualStyleBackColor = True
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnClose.BackColor = System.Drawing.Color.White
        Me.btnClose.BackgroundImage = CType(resources.GetObject("btnClose.BackgroundImage"), System.Drawing.Image)
        Me.btnClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnClose.BorderColor = System.Drawing.Color.Empty
        Me.btnClose.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnClose.FlatAppearance.BorderSize = 0
        Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClose.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.ForeColor = System.Drawing.Color.Black
        Me.btnClose.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnClose.GradientForeColor = System.Drawing.Color.Black
        Me.btnClose.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Location = New System.Drawing.Point(210, 13)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Size = New System.Drawing.Size(97, 30)
        Me.btnClose.TabIndex = 8
        Me.btnClose.Text = "&Close"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'gbWeightSetting
        '
        Me.gbWeightSetting.BorderColor = System.Drawing.Color.Black
        Me.gbWeightSetting.Checked = False
        Me.gbWeightSetting.CollapseAllExceptThis = False
        Me.gbWeightSetting.CollapsedHoverImage = Nothing
        Me.gbWeightSetting.CollapsedNormalImage = Nothing
        Me.gbWeightSetting.CollapsedPressedImage = Nothing
        Me.gbWeightSetting.CollapseOnLoad = False
        Me.gbWeightSetting.Controls.Add(Me.objgbTypes)
        Me.gbWeightSetting.Controls.Add(Me.radOption2)
        Me.gbWeightSetting.Controls.Add(Me.radOption1)
        Me.gbWeightSetting.Dock = System.Windows.Forms.DockStyle.Fill
        Me.gbWeightSetting.ExpandedHoverImage = Nothing
        Me.gbWeightSetting.ExpandedNormalImage = Nothing
        Me.gbWeightSetting.ExpandedPressedImage = Nothing
        Me.gbWeightSetting.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbWeightSetting.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbWeightSetting.HeaderHeight = 25
        Me.gbWeightSetting.HeaderMessage = ""
        Me.gbWeightSetting.HeaderMessageFont = New System.Drawing.Font("Microsoft Sans Serif", 8.25!)
        Me.gbWeightSetting.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbWeightSetting.HeightOnCollapse = 0
        Me.gbWeightSetting.LeftTextSpace = 0
        Me.gbWeightSetting.Location = New System.Drawing.Point(0, 0)
        Me.gbWeightSetting.Name = "gbWeightSetting"
        Me.gbWeightSetting.OpenHeight = 300
        Me.gbWeightSetting.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbWeightSetting.ShowBorder = True
        Me.gbWeightSetting.ShowCheckBox = False
        Me.gbWeightSetting.ShowCollapseButton = False
        Me.gbWeightSetting.ShowDefaultBorderColor = True
        Me.gbWeightSetting.ShowDownButton = False
        Me.gbWeightSetting.ShowHeader = True
        Me.gbWeightSetting.Size = New System.Drawing.Size(317, 171)
        Me.gbWeightSetting.TabIndex = 3
        Me.gbWeightSetting.Temp = 0
        Me.gbWeightSetting.Text = "Weighted Setting"
        Me.gbWeightSetting.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'radObjective
        '
        Me.radObjective.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.radObjective.Location = New System.Drawing.Point(16, 20)
        Me.radObjective.Name = "radObjective"
        Me.radObjective.Size = New System.Drawing.Size(109, 17)
        Me.radObjective.TabIndex = 115
        Me.radObjective.TabStop = True
        Me.radObjective.Text = "Objective"
        Me.radObjective.UseVisualStyleBackColor = True
        '
        'radInitiative
        '
        Me.radInitiative.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.radInitiative.Location = New System.Drawing.Point(131, 43)
        Me.radInitiative.Name = "radInitiative"
        Me.radInitiative.Size = New System.Drawing.Size(109, 17)
        Me.radInitiative.TabIndex = 118
        Me.radInitiative.TabStop = True
        Me.radInitiative.Text = "Initiative/Actions"
        Me.radInitiative.UseVisualStyleBackColor = True
        '
        'radKPI
        '
        Me.radKPI.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.radKPI.Location = New System.Drawing.Point(131, 20)
        Me.radKPI.Name = "radKPI"
        Me.radKPI.Size = New System.Drawing.Size(109, 17)
        Me.radKPI.TabIndex = 116
        Me.radKPI.TabStop = True
        Me.radKPI.Text = "KPI"
        Me.radKPI.UseVisualStyleBackColor = True
        '
        'radTargets
        '
        Me.radTargets.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.radTargets.Location = New System.Drawing.Point(16, 43)
        Me.radTargets.Name = "radTargets"
        Me.radTargets.Size = New System.Drawing.Size(109, 17)
        Me.radTargets.TabIndex = 117
        Me.radTargets.TabStop = True
        Me.radTargets.Text = "Targets"
        Me.radTargets.UseVisualStyleBackColor = True
        '
        'radOption2
        '
        Me.radOption2.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.radOption2.Location = New System.Drawing.Point(15, 62)
        Me.radOption2.Name = "radOption2"
        Me.radOption2.Size = New System.Drawing.Size(286, 17)
        Me.radOption2.TabIndex = 121
        Me.radOption2.TabStop = True
        Me.radOption2.Text = "Weighted Based On"
        Me.radOption2.UseVisualStyleBackColor = True
        '
        'radOption1
        '
        Me.radOption1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.radOption1.Location = New System.Drawing.Point(15, 36)
        Me.radOption1.Name = "radOption1"
        Me.radOption1.Size = New System.Drawing.Size(286, 17)
        Me.radOption1.TabIndex = 120
        Me.radOption1.TabStop = True
        Me.radOption1.Text = "Each BSC item has to be weighted."
        Me.radOption1.UseVisualStyleBackColor = True
        '
        'objgbTypes
        '
        Me.objgbTypes.Controls.Add(Me.radObjective)
        Me.objgbTypes.Controls.Add(Me.radTargets)
        Me.objgbTypes.Controls.Add(Me.radInitiative)
        Me.objgbTypes.Controls.Add(Me.radKPI)
        Me.objgbTypes.Location = New System.Drawing.Point(34, 86)
        Me.objgbTypes.Name = "objgbTypes"
        Me.objgbTypes.Size = New System.Drawing.Size(267, 73)
        Me.objgbTypes.TabIndex = 122
        Me.objgbTypes.TabStop = False
        '
        'frmBSC_WeightSettings
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(317, 226)
        Me.Controls.Add(Me.Panel1)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmBSC_WeightSettings"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Weight Settings"
        Me.Panel1.ResumeLayout(False)
        Me.objFooter.ResumeLayout(False)
        Me.gbWeightSetting.ResumeLayout(False)
        Me.objgbTypes.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents objFooter As eZee.Common.eZeeFooter
    Friend WithEvents btnSave As eZee.Common.eZeeLightButton
    Friend WithEvents btnClose As eZee.Common.eZeeLightButton
    Friend WithEvents gbWeightSetting As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents radObjective As System.Windows.Forms.RadioButton
    Friend WithEvents radInitiative As System.Windows.Forms.RadioButton
    Friend WithEvents radKPI As System.Windows.Forms.RadioButton
    Friend WithEvents radOption2 As System.Windows.Forms.RadioButton
    Friend WithEvents radTargets As System.Windows.Forms.RadioButton
    Friend WithEvents radOption1 As System.Windows.Forms.RadioButton
    Friend WithEvents objgbTypes As System.Windows.Forms.GroupBox
End Class
