﻿Option Strict On

#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data
Imports System.Globalization

#End Region

Public Class frmAppraisalRefrenceList
    Private ReadOnly mstrModuleName As String = "frmAppraisalRefrenceList"

#Region " Private Function(s) & Method(s) "

    Private Sub FillCombo()
        Dim dsList As DataSet = Nothing
        Dim objASEMaster As New clsAppraisal_ShortList_Master
        Try
            dsList = objASEMaster.getComboList("List", True)
            With cboRefrenceNo
                .ValueMember = "shortlistunkid"
                .DisplayMember = "referenceno"
                .DataSource = dsList.Tables("List")
            End With

            'S.SANDEEP [ 14 AUG 2013 ] -- START
            'ENHANCEMENT : TRA CHANGES
            Dim objPeriod As New clscommom_period_Tran
            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'dsList = objPeriod.getListForCombo(enModuleReference.Assessment, 0, "List", True, enStatusType.Open)
            dsList = objPeriod.getListForCombo(enModuleReference.Assessment, 0, FinancialYear._Object._DatabaseName, FinancialYear._Object._Database_Start_Date, "List", True, enStatusType.Open)
            'Sohail (21 Aug 2015) -- End
            With cboPeriod
                .ValueMember = "periodunkid"
                .DisplayMember = "name"
                .DataSource = dsList.Tables("List")
            End With
            objPeriod = Nothing
            'S.SANDEEP [ 14 AUG 2013 ] -- END

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub FillList()
        Dim strSearching As String = ""
        Dim dtList As DataTable = Nothing
        Dim objASEMaster As New clsAppraisal_ShortList_Master
        Try

            If User._Object.Privilege._AllowToViewApprisalAnalysisList = True Then                 'Pinkal (02-Jul-2012) -- Start

                Dim dsList As DataSet = objASEMaster.GetList("List", True)

                If CInt(cboRefrenceNo.SelectedValue) > 0 Then
                    strSearching &= "AND shortlistunkid = '" & CInt(cboRefrenceNo.SelectedValue) & "' "
                End If

                'S.SANDEEP [ 14 AUG 2013 ] -- START
                'ENHANCEMENT : TRA CHANGES
                If CInt(cboPeriod.SelectedValue) > 0 Then
                    strSearching &= "AND periodunkid = '" & CInt(cboPeriod.SelectedValue) & "' "
                End If

                If txtRemark.Text.Trim.Length > 0 Then
                    strSearching &= "AND remark LIKE '%" & txtRemark.Text & "%' "
                End If
                'S.SANDEEP [ 14 AUG 2013 ] -- END

                If dtpFromDate.Checked = True AndAlso dtpToDate.Checked = True Then
                    strSearching &= "AND referencedate >= '" & eZeeDate.convertDate(dtpFromDate.Value) & "' AND referencedate <= '" & eZeeDate.convertDate(dtpToDate.Value) & "'"
                End If

                If strSearching.Length > 0 Then
                    strSearching = strSearching.Substring(3)
                    dtList = New DataView(dsList.Tables(0), strSearching, "", DataViewRowState.CurrentRows).ToTable
                Else
                    dtList = dsList.Tables(0)
                End If

                lvApprisalList.Items.Clear()
                Dim lvItem As ListViewItem = Nothing

                For Each dr As DataRow In dtList.Rows
                    lvItem = New ListViewItem

                    lvItem.Text = eZeeDate.convertDate(dr.Item("referencedate").ToString).ToShortDateString
                    lvItem.SubItems.Add(dr.Item("referenceno").ToString)
                    'S.SANDEEP [ 05 MARCH 2012 ] -- START
                    'ENHANCEMENT : TRA CHANGES
                    lvItem.SubItems.Add(dr.Item("remark").ToString)
                    lvItem.SubItems.Add(dr.Item("Period").ToString)
                    'S.SANDEEP [ 05 MARCH 2012 ] -- END
                    lvItem.Tag = dr.Item("shortlistunkid")

                    lvApprisalList.Items.Add(lvItem)
                Next

                'S.SANDEEP [ 05 MARCH 2012 ] -- START
                'ENHANCEMENT : TRA CHANGES
                If lvApprisalList.Items.Count > 1 Then
                    colhRemark.Width = 385 - 20
                Else
                    colhRemark.Width = 385
                End If

                lvApprisalList.GroupingColumn = objcolhPeriod
                lvApprisalList.DisplayGroups(True)
                'S.SANDEEP [ 05 MARCH 2012 ] -- END

            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "", mstrModuleName)
        Finally
        End Try
    End Sub

    'S.SANDEEP [ 16 MAY 2012 ] -- START
    'ENHANCEMENT : TRA USER PRIVILEGE CHANGES
    Private Sub SetVisibility()
        Try
            btnNew.Enabled = User._Object.Privilege._AllowToAddAppraisalAnalysis
            btnDelete.Enabled = User._Object.Privilege._AllowToDeleteAppraisalAnalysis
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetVisibility", mstrModuleName)
        End Try
    End Sub
    'S.SANDEEP [ 16 MAY 2012 ] -- END


#End Region

#Region " Form's Event(s) "

    Private Sub frmAppraisalRefrenceList_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            Call Set_Logo(Me, gApplicationType)
            Language.setLanguage(Me.Name)
            Call OtherSettings()
            'S.SANDEEP [ 16 MAY 2012 ] -- START
            'ENHANCEMENT : TRA USER PRIVILEGE CHANGES
            Call SetVisibility()
            'S.SANDEEP [ 16 MAY 2012 ] -- END

            FillCombo()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmAppraisalRefrenceList_Load", mstrModuleName)
        End Try
    End Sub

    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()
            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub

#End Region

#Region " Button's Event(s) "

    Private Sub objbtnSearchRefNo_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchRefNo.Click
        Dim objfrm As New frmCommonSearch
        Dim dtList As DataTable
        Try
            dtList = CType(cboRefrenceNo.DataSource, DataTable)
            With cboRefrenceNo
                objfrm.DataSource = CType(cboRefrenceNo.DataSource, DataTable)
                objfrm.ValueMember = .ValueMember
                'S.SANDEEP [ 14 AUG 2013 ] -- START
                'ENHANCEMENT : TRA CHANGES
                'objfrm.DisplayMember = .DisplayMember
                objfrm.DisplayMember = "remark"
                objfrm.CodeMember = .DisplayMember.ToString
                'S.SANDEEP [ 14 AUG 2013 ] -- END
                If objfrm.DisplayDialog Then
                    .SelectedValue = objfrm.SelectedValue
                End If
                .Focus()
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchRefNo_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnSearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearch.Click
        Try
            FillList()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearch_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnReset.Click
        Try
            cboRefrenceNo.SelectedValue = 0
            dtpFromDate.Checked = False : dtpToDate.Checked = False
            'S.SANDEEP [ 14 AUG 2013 ] -- START
            'ENHANCEMENT : TRA CHANGES
            cboPeriod.SelectedValue = 0 : txtRemark.Text = ""
            'S.SANDEEP [ 14 AUG 2013 ] -- END
            Call FillList()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnReset_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnNew_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNew.Click
        Try
            Dim frm As New frmShortlistingEmployee
            frm.displayDialog(enAction.ADD_ONE)
            Call FillList()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnNew_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub btnDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDelete.Click
        If lvApprisalList.SelectedItems.Count < 1 Then
            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please select Employee Filter from the list to perform further operation."), enMsgBoxStyle.Information) '?1
            lvApprisalList.Select()
            Exit Sub
        End If

        Try
            Dim intSelectedIndex As Integer
            intSelectedIndex = lvApprisalList.SelectedItems(0).Index

            If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Are you sure you want to delete this Employee Filter?"), CType(enMsgBoxStyle.Question + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.Yes Then

                'S.SANDEEP [ 19 JULY 2012 ] -- START
                'ENHANCEMENT : TRA CHANGES
                'Dim objShortMst As New clsshortlist_master
                Dim objShortMst As New clsAppraisal_ShortList_Master
                'S.SANDEEP [ 19 JULY 2012 ] -- END

                'S.SANDEEP [ 14 AUG 2013 ] -- START
                'ENHANCEMENT : TRA CHANGES
                If objShortMst.isUsed(CInt(lvApprisalList.SelectedItems(0).Tag)) = True Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Sorry, You cannot delete this Employee Filter. Reason : Appraisal Action is already applied to some of the employees in this filter."), enMsgBoxStyle.Information)
                    Exit Sub
                End If
                'S.SANDEEP [ 14 AUG 2013 ] -- END


                Dim frm As New frmReasonSelection
                Dim mstrVoidReason As String = String.Empty
                'S.SANDEEP [ 19 JULY 2012 ] -- START
                'ENHANCEMENT : TRA CHANGES
                'frm.displayDialog(enVoidCategoryType.APPLICANT, mstrVoidReason)
                frm.displayDialog(enVoidCategoryType.ASSESSMENT, mstrVoidReason)
                'S.SANDEEP [ 19 JULY 2012 ] -- END
                If mstrVoidReason.Length <= 0 Then
                    Exit Sub
                Else
                    objShortMst._Voidreason = mstrVoidReason
                End If
                objShortMst._Voiddatetime = ConfigParameter._Object._CurrentDateAndTime
                objShortMst._Voiduserunkid = User._Object._Userunkid
                With objShortMst
                    ._FormName = mstrModuleName
                    ._LoginEmployeeunkid = 0
                    ._ClientIP = getIP()
                    ._HostName = getHostName()
                    ._FromWeb = False
                    ._AuditUserId = User._Object._Userunkid
._CompanyUnkid = Company._Object._Companyunkid
                    ._AuditDate = ConfigParameter._Object._CurrentDateAndTime
                End With
                objShortMst.Delete(CInt(lvApprisalList.SelectedItems(0).Tag))
                lvApprisalList.SelectedItems(0).Remove()

                If lvApprisalList.Items.Count <= 0 Then
                    Exit Try
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnDelete_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Me.Close()
    End Sub

    'S.SANDEEP [ 14 AUG 2013 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Private Sub objbtnSearchPeriod_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchPeriod.Click
        Dim frm As New frmCommonSearch
        Try
            With cboPeriod
                frm.DataSource = CType(cboPeriod.DataSource, DataTable)
                frm.ValueMember = .ValueMember
                frm.DisplayMember = .DisplayMember
                If frm.DisplayDialog Then
                    .SelectedValue = frm.SelectedValue
                End If
                .Focus()
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchPeriod_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub
    'S.SANDEEP [ 14 AUG 2013 ] -- END

#End Region


    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Private Sub OtherSettings()
        Try
            Me.SuspendLayout()
            
            Call SetLanguage()

            Me.gbEmployeeInfo.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbEmployeeInfo.ForeColor = GUI._eZeeContainerHeaderForeColor


            Me.eZeeHeader.GradientColor1 = GUI._HeaderBackColor1
            Me.eZeeHeader.GradientColor2 = GUI._HeaderBackColor2
            Me.eZeeHeader.BorderColor = GUI._HeaderBorderColor
            Me.eZeeHeader.HeaderTextForeColor = GUI._HeaderTitleFontColor
            Me.eZeeHeader.DescriptionForeColor = GUI._HeaderMessageFontColor


            Me.btnNew.GradientBackColor = GUI._ButttonBackColor
            Me.btnNew.GradientForeColor = GUI._ButttonFontColor

            Me.btnDelete.GradientBackColor = GUI._ButttonBackColor
            Me.btnDelete.GradientForeColor = GUI._ButttonFontColor

            Me.btnClose.GradientBackColor = GUI._ButttonBackColor
            Me.btnClose.GradientForeColor = GUI._ButttonFontColor


            Me.ResumeLayout()
        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "OtherSettings", mstrModuleName)
        End Try
    End Sub


    Private Sub SetLanguage()
        Try
            Me.Text = Language._Object.getCaption(Me.Name, Me.Text)

            Me.eZeeHeader.Title = Language._Object.getCaption(Me.eZeeHeader.Name & "_Title", Me.eZeeHeader.Title)
            Me.eZeeHeader.Message = Language._Object.getCaption(Me.eZeeHeader.Name & "_Message", Me.eZeeHeader.Message)
            Me.btnNew.Text = Language._Object.getCaption(Me.btnNew.Name, Me.btnNew.Text)
            Me.btnDelete.Text = Language._Object.getCaption(Me.btnDelete.Name, Me.btnDelete.Text)
            Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
            Me.gbEmployeeInfo.Text = Language._Object.getCaption(Me.gbEmployeeInfo.Name, Me.gbEmployeeInfo.Text)
            Me.lblTo.Text = Language._Object.getCaption(Me.lblTo.Name, Me.lblTo.Text)
            Me.lblAssessmentdate.Text = Language._Object.getCaption(Me.lblAssessmentdate.Name, Me.lblAssessmentdate.Text)
            Me.lblRefrenceNo.Text = Language._Object.getCaption(Me.lblRefrenceNo.Name, Me.lblRefrenceNo.Text)
            Me.colhDate.Text = Language._Object.getCaption(CStr(Me.colhDate.Tag), Me.colhDate.Text)
            Me.colhRefrenceNo.Text = Language._Object.getCaption(CStr(Me.colhRefrenceNo.Tag), Me.colhRefrenceNo.Text)
            Me.colhRemark.Text = Language._Object.getCaption(CStr(Me.colhRemark.Tag), Me.colhRemark.Text)
            Me.lblPeriod.Text = Language._Object.getCaption(Me.lblPeriod.Name, Me.lblPeriod.Text)
            Me.lblRemark.Text = Language._Object.getCaption(Me.lblRemark.Name, Me.lblRemark.Text)

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
        End Try
    End Sub


    Private Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Please select Employee Filter from the list to perform further operation.")
            Language.setMessage(mstrModuleName, 2, "Are you sure you want to delete this Employee Filter?")
            Language.setMessage(mstrModuleName, 3, "Sorry, You cannot delete this Employee Filter. Reason : Appraisal Action is already applied to some of the employees in this filter.")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class