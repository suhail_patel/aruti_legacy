﻿Option Strict On

#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data
Imports System.IO
Imports System.Text

#End Region

Public Class frmShortlistingEmployee
    Private mstrModuleName As String = "frmShortlistingEmployee"

#Region " Private Variables "

    Private mSize As Size
    Private mblnCancel As Boolean = True
    Private menAction As enAction = enAction.ADD_ONE
    Private mdtFilter As DataTable
    Private dsCondition As New DataSet
    Private mdtGeneral As DataTable
    Private objMShortList As clsAppraisal_ShortList_Master
    Private objMFilter As New clsAppraisal_Filter
    Private dBSCPRow() As DataRow = Nothing
    Private dGEIERow() As DataRow = Nothing
    Private iCount As Integer = 0
    Dim dsFilteredEmp As New DataSet
    Private mdtPerspective As DataTable

#End Region

#Region " Display Dialog "

    Public Function displayDialog(ByVal eAction As enAction) As Boolean
        Try
            menAction = eAction
            Me.ShowDialog()
            Return Not mblnCancel
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "displayDialog", mstrModuleName)
        End Try
    End Function

#End Region

#Region " Private Methods & Funtions "

    Private Sub FillCombo()
        Dim objMCondition As New clsMasterData
        Dim objPeriod As New clscommom_period_Tran
        Dim dsList As New DataSet
        Try
            'Nilay (10-Nov-2016) -- Start
            'Enhancement : New EFT report : EFT ECO bank for OFFGRID
            'dsCondition = objMCondition.GetCondition(True)
            dsCondition = objMCondition.GetCondition(True, True, False, False, False)
            'Nilay (10-Nov-2016) -- End
            With cboBSCTotalCondition
                .ValueMember = "id"
                .DisplayMember = "Name"
                .DataSource = dsCondition.Tables(0).Copy
                .SelectedValue = 0
            End With

            With cboGETotalCondition
                .ValueMember = "id"
                .DisplayMember = "Name"
                .DataSource = dsCondition.Tables(0).Copy
                .SelectedValue = 0
            End With

            With cboOverallCondition
                .ValueMember = "id"
                .DisplayMember = "Name"
                .DataSource = dsCondition.Tables(0).Copy
                .SelectedValue = 0
            End With

            RemoveHandler cboPeriod.SelectedIndexChanged, AddressOf cboPeriod_SelectedIndexChanged
            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'dsList = objPeriod.getListForCombo(enModuleReference.Assessment, 0, "List", True, enStatusType.Open)
            dsList = objPeriod.getListForCombo(enModuleReference.Assessment, 0, FinancialYear._Object._DatabaseName, FinancialYear._Object._Database_Start_Date, "List", True, enStatusType.Open)
            'Sohail (21 Aug 2015) -- End
            With cboPeriod
                .ValueMember = "periodunkid"
                .DisplayMember = "name"
                .DataSource = dsList.Tables("List")
                .SelectedValue = 0
            End With
            AddHandler cboPeriod.SelectedIndexChanged, AddressOf cboPeriod_SelectedIndexChanged

            dsList = objMCondition.GetAD_Parameter_List(False, "List")
            With cboAppointment
                .ValueMember = "Id"
                .DisplayMember = "Name"
                .DataSource = dsList.Tables("List")
                .SelectedValue = 1
            End With

            Dim xCItems() As ComboBoxValue = {New ComboBoxValue(Language.getMessage(mstrModuleName, 111, "Overall Score"), enApprFilterTypeId.APRL_OVER_ALL), _
                                              New ComboBoxValue(Language.getMessage(mstrModuleName, 112, "Self Score"), enApprFilterTypeId.APRL_EMPLOYEE), _
                                              New ComboBoxValue(Language.getMessage(mstrModuleName, 113, "Assessor Score"), enApprFilterTypeId.APRL_ASSESSOR), _
                                              New ComboBoxValue(Language.getMessage(mstrModuleName, 114, "Reviewer Score"), enApprFilterTypeId.APRL_REVIEWER)}

            With cboFinalResult
                .Items.Clear()
                .Items.AddRange(xCItems)
                .SelectedIndex = 0
                .SelectedValue = CType(cboFinalResult.SelectedItem, ComboBoxValue).Value
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub Fill_Grid()
        Try
            Call Generate_Table()

            dgvData.AutoGenerateColumns = False

            dgcolhItems.DataPropertyName = "Items"
            dgcolhScore.DataPropertyName = "Score"
            objdgcolhIsGrp.DataPropertyName = "IsGrp"
            objdgcolhGrpId.DataPropertyName = "GrpId"
            objdgcolhItemId.DataPropertyName = "ItemId"

            dgcolhCondition.ValueMember = "Id"
            dgcolhCondition.DisplayMember = "Name"
            dgcolhCondition.DataSource = dsCondition.Tables(0).Copy

            dgcolhCondition.DataPropertyName = "Id"

            dgvData.DataSource = mdtGeneral

            dgvData.Refresh()

            Dim pCell As New clsMergeCell
            For Each dgvRow As DataGridViewRow In dgvData.Rows
                If CBool(dgvRow.Cells(objdgcolhIsGrp.Index).Value) = True Then
                    dgvRow.ReadOnly = True
                    If dgvRow.Cells(objdgcolhCollaps.Index).Value Is Nothing Then
                        dgvRow.Cells(objdgcolhCollaps.Index).Value = "-"
                        dgvRow.Cells(objdgcolhCollaps.Index).Style.ForeColor = Color.White
                    End If
                    dgvRow.Cells(objdgcolhCollaps.Index).Style.BackColor = Color.Gray
                    dgvRow.Cells(objdgcolhCollaps.Index).Style.SelectionBackColor = Color.Gray
                    dgvRow.Cells(objdgcolhCollaps.Index).Style.ForeColor = Color.White

                    pCell.MakeMerge(dgvData, dgvRow.Index, 1, dgvRow.Cells.Count - 1, Color.Gray, Color.White, dgvRow.Cells(dgcolhItems.Index).Value.ToString, "", picStayView.Image)
                Else
                    dgvRow.Cells(objdgcolhCollaps.Index).Value = ""
                    dgvRow.Cells(dgcolhCondition.Index).Value = 0
                End If
            Next

            'S.SANDEEP [04 MAR 2015] -- START

            'Call Generate_Perspective_Table()

            'dgvPerspective.AutoGenerateColumns = False
            'dgcolhPerspective.DataPropertyName = "Item"
            'dgcolhPScore.DataPropertyName = "Score"
            'objdgcolhPerspectiveId.DataPropertyName = "PerId"

            'dgcolhPCondition.ValueMember = "Id"
            'dgcolhPCondition.DisplayMember = "Name"
            'dgcolhPCondition.DataSource = dsCondition.Tables(0).Copy
            'dgcolhPCondition.DataPropertyName = "Id"

            'dgvPerspective.DataSource = mdtPerspective
            'dgvPerspective.Refresh()

            'S.SANDEEP [04 MAR 2015] -- END


        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Fill_Grid", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub Generate_Table()
        Try
            mdtGeneral = New DataTable("GE")

            mdtGeneral.Columns.Add("Items", System.Type.GetType("System.String")).DefaultValue = ""
            mdtGeneral.Columns.Add("Score", System.Type.GetType("System.String")).DefaultValue = ""
            mdtGeneral.Columns.Add("IsGrp", System.Type.GetType("System.Boolean")).DefaultValue = False
            mdtGeneral.Columns.Add("GrpId", System.Type.GetType("System.Int32")).DefaultValue = -1
            mdtGeneral.Columns.Add("ItemId", System.Type.GetType("System.Int32")).DefaultValue = -1
            mdtGeneral.Columns.Add("Id", System.Type.GetType("System.Int32")).DefaultValue = -1

            Dim objAssessGrp As New clsassess_group_master
            Dim objAssessItem As New clsassess_item_master

            Dim dsGrp As New DataSet

            dsGrp = objAssessGrp.getListForCombo("List")

            Dim dGRow As DataRow = Nothing

            For Each dRow As DataRow In dsGrp.Tables("List").Rows
                dGRow = mdtGeneral.NewRow

                dGRow.Item("Items") = dRow.Item("name")
                dGRow.Item("IsGrp") = True
                dGRow.Item("GrpId") = dRow.Item("assessgroupunkid")

                mdtGeneral.Rows.Add(dGRow)

                Dim dtItem As DataTable
                Dim objCompetenciesMaster As New clsassess_competencies_master
                dtItem = objCompetenciesMaster.GetCompetenciesForShortlisting(CInt(dRow.Item("assessgroupunkid")), CInt(cboPeriod.SelectedValue))
                For Each dIRow As DataRow In dtItem.Rows
                    dGRow = mdtGeneral.NewRow
                    dGRow.Item("Items") = Space(6) & dIRow.Item("Name").ToString
                    dGRow.Item("IsGrp") = False
                    dGRow.Item("GrpId") = dRow.Item("assessgroupunkid")
                    dGRow.Item("ItemId") = dIRow.Item("Id")
                    mdtGeneral.Rows.Add(dGRow)
                Next
                objCompetenciesMaster = Nothing

                dtItem.Dispose()
            Next

            dsGrp.Dispose()

            objAssessGrp = Nothing
            objAssessItem = Nothing

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Generate_Table", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub Generate_Perspective_Table()
        Try
            mdtPerspective = New DataTable("BSC")

            mdtPerspective.Columns.Add("ischeck", System.Type.GetType("System.Boolean")).DefaultValue = 0
            mdtPerspective.Columns.Add("Item", System.Type.GetType("System.String")).DefaultValue = ""
            mdtPerspective.Columns.Add("Score", System.Type.GetType("System.String")).DefaultValue = ""
            mdtPerspective.Columns.Add("PerId", System.Type.GetType("System.Int32")).DefaultValue = 0
            mdtPerspective.Columns.Add("Id", System.Type.GetType("System.Int32")).DefaultValue = 0

            Dim objPrMaster As New clsassess_perspective_master
            Dim dsList As New DataSet
            dsList = objPrMaster.getComboList("List", False)
            For Each dRow As DataRow In dsList.Tables(0).Rows
                Dim xRow As DataRow = mdtPerspective.NewRow()

                xRow.Item("Item") = dRow.Item("Name")
                xRow.Item("PerId") = dRow.Item("Id")

                mdtPerspective.Rows.Add(xRow)
            Next
            objPrMaster = Nothing : dsList.Dispose()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Generate_Perspective_Table", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub SetVisibility()
        Try

            'Shani(06-Feb-2016) -- Start
            'PA Changes Given By Glory for CCBRT
            btnSave.Enabled = User._Object.Privilege._AllowToSaveExportAppraisalAnalysis
            'Shani(06-Feb-2016) -- End

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetVisibility", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Function IsValidData() As Boolean
        Dim blnFlag As Boolean = False
        Try
            If CInt(cboPeriod.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 21, "Period is mandatory information. Please select Period to continue."), enMsgBoxStyle.Information)
                cboPeriod.Focus()
                'S.SANDEEP [19 FEB 2015] -- START
                Return False
                'S.SANDEEP [19 FEB 2015] -- END
            End If

            If gbSummaryFilter.Checked Then
                If txtBSCTotalScore.Text.Trim.Length <= 0 AndAlso txtGETotalScore.Text.Trim.Length <= 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Sorry, you cannot add this filter as no value is provided to match."), enMsgBoxStyle.Information)
                    txtBSCTotalScore.Focus()
                    Return False
                End If
                If radSummaryAND.Checked Or radSummaryOR.Checked Then
                    If txtBSCTotalScore.Text.Trim.Length > 0 AndAlso txtGETotalScore.Text.Trim.Length <= 0 Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Sorry, you cannot add this filter as General Evaluation Total Score value is not provided to match."), enMsgBoxStyle.Information)
                        txtBSCTotalScore.Focus()
                        Return False
                    End If
                    If txtBSCTotalScore.Text.Trim.Length <= 0 AndAlso txtGETotalScore.Text.Trim.Length > 0 Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Sorry, you cannot add this filter as Balance Score Card value is not provided to match."), enMsgBoxStyle.Information)
                        txtBSCTotalScore.Focus()
                        Return False
                    End If
                End If

                If txtBSCTotalScore.Text.Trim.Length > 0 AndAlso txtGETotalScore.Text.Trim.Length > 0 Then
                    If radSummaryAND.Checked = False AndAlso radSummaryOR.Checked = False Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Sorry, you cannot add this filter as no operation is set between BSC and General Evaluation."), enMsgBoxStyle.Information)
                        radSummaryOR.Focus()
                        Return False
                    End If
                End If
            End If

            If gbDetailFilter.Checked Then

                'S.SANDEEP [04 MAR 2015] -- START

                Dim dTemp() As DataRow = mdtGeneral.Select("Id > '-1' AND Score <> ''")
                If dTemp.Length > 0 Then
                    iCount = dTemp.Length
                    blnFlag = True
                Else
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 115, "Sorry you cannot add this filter as Competencies Item(s) score is not provided."), enMsgBoxStyle.Information)
                    Return False
                End If

                If iCount > 1 Then
                    If radDetailAND.Checked = False AndAlso radDetailOR.Checked = False Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 116, "Sorry you cannot add this filter as no operation set between Competencies Item(s)."), enMsgBoxStyle.Information)
                        radDetailOR.Focus()
                        iCount = -1
                        Return False
                    End If
                End If

                If iCount = 1 Then
                    If radDetailAND.Checked = True Or radDetailOR.Checked = True Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 117, "Sorry you cannot add this filter as operation cannot be performed on single Competencies Item(s)."), enMsgBoxStyle.Information)
                        iCount = -1
                        Return False
                    End If
                End If

                'Dim dTemp() As DataRow = mdtPerspective.Select("Id > '-1' AND Score <> ''")
                'If dTemp.Length > 0 Then
                '    iCount = dTemp.Length
                '    blnFlag = True
                'Else
                '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 10, "Sorry you cannot add this filter as BSC Perspective score is not provided."), enMsgBoxStyle.Information)
                '    Return False
                'End If

                'If iCount > 1 Then
                '    If radDetailAND.Checked = False AndAlso radDetailOR.Checked = False Then
                '        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 12, "Sorry you cannot add this filter as no operation set between prespective."), enMsgBoxStyle.Information)
                '        radDetailOR.Focus()
                '        iCount = -1
                '        Return False
                '    End If
                'End If

                'If iCount = 1 Then
                '    If radDetailAND.Checked = True Or radDetailOR.Checked = True Then
                '        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 13, "Sorry you cannot add this filter as operation cannot be performed on single perspective."), enMsgBoxStyle.Information)
                '        iCount = -1
                '        Return False
                '    End If
                'End If

                'dTemp = mdtGeneral.Select("Id > '-1' AND Score <> ''")

                'If radDetailAND.Checked Or radDetailOR.Checked Then
                '    If blnFlag = True AndAlso dTemp.Length <= 0 Then
                '        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 9, "Sorry you cannot add this filter as General Evaluation Item score is not provided."), enMsgBoxStyle.Information)
                '        Return False
                '    End If

                '    If blnFlag = False AndAlso dTemp.Length >= 0 Then
                '        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 10, "Sorry you cannot add this filter as BSC Perspective score is not provided."), enMsgBoxStyle.Information)
                '        Return False
                '    End If
                'End If

                'If blnFlag = True AndAlso dTemp.Length > 0 Then
                '    If radDetailAND.Checked = False AndAlso radDetailOR.Checked = False Then
                '        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 11, "Sorry, you cannot add this filter as no operation is set between BSC Perspective and General Evaluation Items."), enMsgBoxStyle.Information)
                '        Return False
                '    End If
                'End If
                'S.SANDEEP [04 MAR 2015] -- END
            End If

            If gbOverAllScore.Checked = True Then
                If txtOverallScore.Decimal <= 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 22, "Sorry, you cannot add this filter as Overall Score value is not provided to match."), enMsgBoxStyle.Information)
                    txtOverallScore.Focus()
                    Return False
                End If
            End If

            Return True

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "IsValidData", mstrModuleName)
        Finally
        End Try
    End Function

    Private Sub Set_TextValue()
        Try
            txtBSCTotalScore.Text = ""
            txtGETotalScore.Text = ""
            txtOverallScore.Text = ""
            cboAppointment.SelectedValue = 1
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Set_TextValue", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub Fill_FilterList()
        Try
            lvFilterCriteria.Items.Clear()
            Dim lvItem As ListViewItem

            For Each dtRow As DataRow In mdtFilter.Rows
                If CStr(IIf(IsDBNull(dtRow.Item("AUD")), "A", dtRow.Item("AUD"))) <> "D" Then
                    lvItem = New ListViewItem

                    lvItem.UseItemStyleForSubItems = False

                    lvItem.Text = dtRow.Item("filter_criteria").ToString            'FILTER CRITERIA
                    lvItem.SubItems.Add(dtRow.Item("filter_value").ToString)        'FILTER VALUE
                    lvItem.SubItems.Add(dtRow.Item("contition").ToString)           'FILTER CONDITION
                    lvItem.SubItems.Add(dtRow.Item("filter_refid").ToString)        'FILTER CRITERIA ID
                    lvItem.SubItems.Add(dtRow.Item("conditionunkid").ToString)      'FILTER CONDITION ID
                    lvItem.SubItems.Add(dtRow.Item("operator").ToString)            'OPERATOR ID
                    lvItem.SubItems.Add(dtRow.Item("GUID").ToString)                'GUID
                    lvItem.SubItems.Add(dtRow.Item("filter_grp").ToString)          'GROUP NAME
                    lvItem.SubItems.Add(dtRow.Item("operation").ToString, Color.Blue, lvItem.BackColor, New Font(Me.Font, FontStyle.Bold))           'OPERATOR

                    lvItem.Tag = dtRow.Item("filterunkid").ToString                 'FILTERUNKID

                    lvFilterCriteria.Items.Add(lvItem)
                End If
            Next

            If lvFilterCriteria.Items.Count > 2 Then
                colhFieldName.Width = 285 - 20
            Else
                colhFieldName.Width = 285
            End If

            lvFilterCriteria.GroupingColumn = objcolhGrpName
            lvFilterCriteria.DisplayGroups(True)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Fill_FilterList", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub Set_SummaryFilter()
        Dim blnFlag As Boolean = False
        Try
            Dim dRow As DataRow = Nothing

            Dim dBSC() As DataRow = mdtFilter.Select("filter_refid = '" & clsAppraisal_Filter.Filter_Refid.TOTAL_BSC_SCORE & "' AND AUD <> 'D'")
            Dim dGE() As DataRow = mdtFilter.Select("filter_refid = '" & clsAppraisal_Filter.Filter_Refid.TOTAL_GE_SCORE & "' AND AUD <> 'D'")

            If dBSC.Length <= 0 AndAlso txtBSCTotalScore.Text.Trim.Length > 0 Then
                dRow = mdtFilter.NewRow

                dRow.Item("filterunkid") = -1
                dRow.Item("shortlistunkid") = -1
                dRow.Item("filter_refid") = clsAppraisal_Filter.Filter_Refid.TOTAL_BSC_SCORE
                dRow.Item("filter_value") = CDbl(txtBSCTotalScore.Text)
                dRow.Item("conditionunkid") = cboBSCTotalCondition.SelectedValue
                dRow.Item("referenceunkid") = -1
                dRow.Item("issummary") = True

                If blnFlag = False Then
                    If radSummaryAND.Checked = True Then
                        dRow.Item("operator") = clsAppraisal_Filter.Operater_ModeId.OP_AND
                    ElseIf radSummaryOR.Checked = True Then
                        dRow.Item("operator") = clsAppraisal_Filter.Operater_ModeId.OP_OR
                    End If
                End If

                'S.SANDEEP [19 FEB 2015] -- START
                'dRow.Item("isself_score") = chkSelf_Assess.Checked
                'dRow.Item("isassessor_score") = chkAssessor_Assess.Checked
                'dRow.Item("isreviewer_score") = chkReviewer_Assess.Checked
                dRow.Item("filtertypeid") = CInt(CType(cboFinalResult.SelectedItem, ComboBoxValue).Value)
                'S.SANDEEP [19 FEB 2015] -- END
                dRow.Item("AUD") = "A"
                dRow.Item("GUID") = Guid.NewGuid.ToString
                dRow.Item("filter_criteria") = elBalanceScorecard.Text
                dRow.Item("col_tag") = elBalanceScorecard.Tag

                Select Case CInt(cboBSCTotalCondition.SelectedValue)
                    Case 0
                        dRow.Item("contition") = "="
                    Case Else
                        dRow.Item("contition") = cboBSCTotalCondition.Text
                End Select

                If blnFlag = False Then
                    If radSummaryAND.Checked = True Then
                        dRow.Item("operation") = radSummaryAND.Text
                    ElseIf radSummaryOR.Checked = True Then
                        dRow.Item("operation") = radSummaryOR.Text
                    End If
                    blnFlag = True
                End If

                dRow.Item("filter_grp") = gbSummaryFilter.Text

                mdtFilter.Rows.Add(dRow)

                txtBSCTotalScore.Enabled = False : cboBSCTotalCondition.Enabled = False

            ElseIf dBSC.Length > 0 Then
                If radSummaryAND.Checked = True Then
                    dBSC(0).Item("operation") = radSummaryAND.Text
                    dBSC(0).Item("operator") = clsAppraisal_Filter.Operater_ModeId.OP_AND
                ElseIf radSummaryOR.Checked = True Then
                    dBSC(0).Item("operation") = radSummaryOR.Text
                    dBSC(0).Item("operator") = clsAppraisal_Filter.Operater_ModeId.OP_OR
                End If
                blnFlag = True
            End If

            If dGE.Length <= 0 AndAlso txtGETotalScore.Text.Trim.Length > 0 Then
                dRow = mdtFilter.NewRow

                dRow.Item("filterunkid") = -1
                dRow.Item("shortlistunkid") = -1
                dRow.Item("filter_refid") = clsAppraisal_Filter.Filter_Refid.TOTAL_GE_SCORE
                dRow.Item("filter_value") = CDbl(txtGETotalScore.Text)
                dRow.Item("conditionunkid") = cboGETotalCondition.SelectedValue
                dRow.Item("referenceunkid") = -1
                dRow.Item("issummary") = True

                If blnFlag = False Then
                    If radSummaryAND.Checked = True Then
                        dRow.Item("operator") = clsAppraisal_Filter.Operater_ModeId.OP_AND
                    ElseIf radSummaryOR.Checked = True Then
                        dRow.Item("operator") = clsAppraisal_Filter.Operater_ModeId.OP_OR
                    End If
                End If

                'S.SANDEEP [19 FEB 2015] -- START
                'dRow.Item("isself_score") = chkSelf_Assess.Checked
                'dRow.Item("isassessor_score") = chkAssessor_Assess.Checked
                'dRow.Item("isreviewer_score") = chkReviewer_Assess.Checked
                dRow.Item("filtertypeid") = CInt(CType(cboFinalResult.SelectedItem, ComboBoxValue).Value)
                'S.SANDEEP [19 FEB 2015] -- END

                dRow.Item("AUD") = "A"
                dRow.Item("GUID") = Guid.NewGuid.ToString
                dRow.Item("filter_criteria") = elGeneralEvaluation.Text
                dRow.Item("col_tag") = elGeneralEvaluation.Tag

                Select Case CInt(cboGETotalCondition.SelectedValue)
                    Case 0
                        dRow.Item("contition") = "="
                    Case Else
                        dRow.Item("contition") = cboGETotalCondition.Text
                End Select

                If blnFlag = False Then
                    If radSummaryAND.Checked = True Then
                        dRow.Item("operation") = radSummaryAND.Text
                    ElseIf radSummaryOR.Checked = True Then
                        dRow.Item("operation") = radSummaryOR.Text
                    End If
                End If

                dRow.Item("filter_grp") = gbSummaryFilter.Text

                mdtFilter.Rows.Add(dRow)

                txtGETotalScore.Enabled = False : cboGETotalCondition.Enabled = False
            End If


        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub Set_DetailFilter()
        Dim blnFlag As Boolean = False
        Try
            Dim dRow As DataRow = Nothing

            'S.SANDEEP [04 MAR 2015] -- START
            Dim dTemp() As DataRow = Nothing
            'Dim dTemp() As DataRow = mdtPerspective.Select("Id > '-1' AND Score <> ''")

            'If dTemp.Length > 0 Then
            '    Dim dItem() As DataRow = Nothing
            '    For i As Integer = 0 To dTemp.Length - 1
            '        dItem = mdtFilter.Select("filter_refid = '" & clsAppraisal_Filter.Filter_Refid.BSC_PERSPECTIVE & "' AND referenceunkid = '" & CInt(dTemp(i)("PerId")) & "' AND AUD <> 'D'")
            '        If dItem.Length <= 0 Then
            '            dRow = mdtFilter.NewRow

            '            dRow.Item("filterunkid") = -1
            '            dRow.Item("shortlistunkid") = -1
            '            dRow.Item("filter_refid") = clsAppraisal_Filter.Filter_Refid.BSC_PERSPECTIVE
            '            dRow.Item("filter_value") = CDbl(dTemp(i)("Score"))
            '            dRow.Item("conditionunkid") = CDbl(dTemp(i)("Id"))
            '            dRow.Item("referenceunkid") = CInt(dTemp(i)("PerId"))
            '            dRow.Item("issummary") = False
            '            If radDetailAND.Checked = True Then
            '                dRow.Item("operator") = clsAppraisal_Filter.Operater_ModeId.OP_AND
            '            ElseIf radDetailOR.Checked = True Then
            '                dRow.Item("operator") = clsAppraisal_Filter.Operater_ModeId.OP_OR
            '            End If

            '            'S.SANDEEP [19 FEB 2015] -- START
            '            'dRow.Item("isself_score") = chkSelf_Assess.Checked
            '            'dRow.Item("isassessor_score") = chkAssessor_Assess.Checked
            '            'dRow.Item("isreviewer_score") = chkReviewer_Assess.Checked
            '            dRow.Item("filtertypeid") = CInt(CType(cboFinalResult.SelectedItem, ComboBoxValue).Value)
            '            'S.SANDEEP [19 FEB 2015] -- END

            '            dRow.Item("AUD") = "A"
            '            dRow.Item("GUID") = Guid.NewGuid.ToString
            '            dRow.Item("filter_criteria") = Trim(CStr(dTemp(i)("Item")))
            '            dRow.Item("col_tag") = CStr(dTemp(i)("PerId"))
            '            If radDetailAND.Checked = True Then
            '                dRow.Item("operation") = radDetailAND.Text
            '            ElseIf radDetailOR.Checked = True Then
            '                dRow.Item("operation") = radDetailOR.Text
            '            End If
            '            dRow.Item("filter_grp") = gbDetailFilter.Text
            '            Select Case CInt(CDbl(dTemp(i)("Id")))
            '                Case 0
            '                    dRow.Item("contition") = "="
            '                Case 1
            '                    dRow.Item("contition") = Language.getMessage("clsMasterData", 408, ">")
            '                Case 2
            '                    dRow.Item("contition") = Language.getMessage("clsMasterData", 310, "<")
            '                Case 3
            '                    dRow.Item("contition") = Language.getMessage("clsMasterData", 311, ">=")
            '                Case 4
            '                    dRow.Item("contition") = Language.getMessage("clsMasterData", 312, "<=")
            '            End Select
            '            mdtFilter.Rows.Add(dRow)
            '        End If
            '    Next
            'End If

            'S.SANDEEP [04 MAR 2015] -- END


            dTemp = mdtGeneral.Select("Id > '-1' AND Score <> ''")

            If dTemp.Length > 0 Then
                Dim dItem() As DataRow = Nothing


                For i As Integer = 0 To dTemp.Length - 1
                    dItem = mdtFilter.Select("filter_refid = '" & clsAppraisal_Filter.Filter_Refid.GENEAL_EVALUATION & "' AND referenceunkid = '" & CInt(dTemp(i)("ItemId")) & "' AND AUD <> 'D'")
                    If dItem.Length <= 0 Then
                        dRow = mdtFilter.NewRow

                        dRow.Item("filterunkid") = -1
                        dRow.Item("shortlistunkid") = -1
                        dRow.Item("filter_refid") = clsAppraisal_Filter.Filter_Refid.GENEAL_EVALUATION
                        dRow.Item("filter_value") = CDbl(dTemp(i)("Score"))
                        dRow.Item("conditionunkid") = CDbl(dTemp(i)("Id"))
                        dRow.Item("referenceunkid") = CInt(dTemp(i)("ItemId"))
                        dRow.Item("issummary") = False
                        dRow.Item("operator") = clsAppraisal_Filter.Operater_ModeId.OP_AND

                        'S.SANDEEP [19 FEB 2015] -- START
                        'dRow.Item("isself_score") = chkSelf_Assess.Checked
                        'dRow.Item("isassessor_score") = chkAssessor_Assess.Checked
                        'dRow.Item("isreviewer_score") = chkReviewer_Assess.Checked
                        dRow.Item("filtertypeid") = CInt(CType(cboFinalResult.SelectedItem, ComboBoxValue).Value)
                        'S.SANDEEP [19 FEB 2015] -- END

                        dRow.Item("AUD") = "A"
                        dRow.Item("GUID") = Guid.NewGuid.ToString
                        dRow.Item("filter_criteria") = Trim(CStr(dTemp(i)("Items")))
                        dRow.Item("col_tag") = CStr(dTemp(i)("ItemId"))
                        dRow.Item("operation") = radSummaryAND.Text
                        dRow.Item("filter_grp") = gbDetailFilter.Text
                        Select Case CInt(CDbl(dTemp(i)("Id")))
                            Case 0
                                dRow.Item("contition") = "="
                                'S.SANDEEP |24-DEC-2019| -- START
                                'ISSUE/ENHANCEMENT : SCORE ROUNDING OPTION
                                'Case 1
                                '    dRow.Item("contition") = Language.getMessage("clsMasterData", 408, ">")
                                'Case 2
                                '    dRow.Item("contition") = Language.getMessage("clsMasterData", 310, "<")
                                'Case 3
                                '    dRow.Item("contition") = Language.getMessage("clsMasterData", 311, ">=")
                                'Case 4
                                '    dRow.Item("contition") = Language.getMessage("clsMasterData", 312, "<=")
                            Case 1
                                dRow.Item("contition") = ">"
                            Case 2
                                dRow.Item("contition") = "<"
                            Case 3
                                dRow.Item("contition") = ">="
                            Case 4
                                dRow.Item("contition") = "<="
                                'S.SANDEEP |24-DEC-2019| -- END
                        End Select
                        mdtFilter.Rows.Add(dRow)
                    End If
                Next
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Set_DetailFilter", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Function Export_to_Excel(ByVal BlnIsSave As Boolean, ByVal flFileName As String, ByVal SavePath As String, ByVal objDataReader As System.Data.DataTable, ByVal objDataFilter As DataTable) As Boolean
        Dim strBuilder As New StringBuilder
        Dim blnFlag As Boolean = False
        Try

            strBuilder.Append(" <TABLE BORDER=0 WIDTH=140%> " & vbCrLf)
            strBuilder.Append(" <TR ALIGN = CENTER VALIGN=TOP> " & vbCrLf)
            strBuilder.Append(" <TD BORDER=1 COLSPAN ='" & objDataReader.Columns.Count & "' ALIGN='MIDDLE'><FONT SIZE=3><B>" & Language.getMessage(mstrModuleName, 16, "Employee Short Listing") & "</B></FONT></TD>" & vbCrLf)
            strBuilder.Append(" </TR> " & vbCrLf)
            strBuilder.Append(" </TABLE> " & vbCrLf)
            strBuilder.Append(" <BR> " & vbCrLf)

            If BlnIsSave Then
                strBuilder.Append(" <TABLE BORDER=0 WIDTH=140%> " & vbCrLf)
                strBuilder.Append(" <TR> " & vbCrLf)
                strBuilder.Append(" <TD WIDTH='10%'> " & vbCrLf)
                strBuilder.Append(" <FONT SIZE=3><B>")
                strBuilder.Append(" Reference No : " & objMShortList._Referenceno.ToString & vbCrLf)
                strBuilder.Append(" </FONT></B>")
                strBuilder.Append(" </TD> " & vbCrLf)
                strBuilder.Append(" </TR> " & vbCrLf)
                strBuilder.Append(" </TABLE> " & vbCrLf)
                strBuilder.Append(" <BR> " & vbCrLf)
            End If

            'S.SANDEEP [19 FEB 2015] -- START
            If CInt(CType(cboFinalResult.SelectedItem, ComboBoxValue).Value) > 0 Then
                strBuilder.Append(" <TABLE BORDER=0 WIDTH=140%> " & vbCrLf)
                strBuilder.Append(" <TR> " & vbCrLf)
                strBuilder.Append(" <TD WIDTH='10%'> " & vbCrLf)
                strBuilder.Append(" <FONT SIZE=3><B>")
                strBuilder.Append(lblFinalResult.Text & " :  " & CType(cboFinalResult.SelectedItem, ComboBoxValue).Display & " " & vbCrLf)
                strBuilder.Append(" </FONT></B>")
                strBuilder.Append(" </TD> " & vbCrLf)
                strBuilder.Append(" </TR> " & vbCrLf)
                strBuilder.Append(" </TABLE> " & vbCrLf)
                strBuilder.Append(" <BR> " & vbCrLf)
            End If
            'S.SANDEEP [19 FEB 2015] -- END

            Select Case CInt(cboAppointment.SelectedValue)
                Case enAD_Report_Parameter.APP_DATE_FROM
                    If dtpDate1.Checked = True AndAlso dtpDate2.Checked = True Then
                        strBuilder.Append(" <TABLE BORDER=0 WIDTH=140%> " & vbCrLf)
                        strBuilder.Append(" <TR> " & vbCrLf)
                        strBuilder.Append(" <TD WIDTH='10%'> " & vbCrLf)
                        strBuilder.Append(" <FONT SIZE=3><B>")
                        strBuilder.Append(" Appointed Date " & cboAppointment.Text & " :  " & dtpDate1.Value.Date & " " & " To : " & dtpDate2.Value.Date & " " & vbCrLf)
                        strBuilder.Append(" </FONT></B>")
                        strBuilder.Append(" </TD> " & vbCrLf)
                        strBuilder.Append(" </TR> " & vbCrLf)
                        strBuilder.Append(" </TABLE> " & vbCrLf)
                        strBuilder.Append(" <BR> " & vbCrLf)
                    End If
                Case enAD_Report_Parameter.APP_DATE_BEFORE
                    If dtpDate1.Checked = True Then
                        strBuilder.Append(" <TABLE BORDER=0 WIDTH=140%> " & vbCrLf)
                        strBuilder.Append(" <TR> " & vbCrLf)
                        strBuilder.Append(" <TD WIDTH='10%'> " & vbCrLf)
                        strBuilder.Append(" <FONT SIZE=3><B>")
                        strBuilder.Append(" Appointed Date " & cboAppointment.Text & " :  " & dtpDate1.Value.Date & " " & vbCrLf)
                        strBuilder.Append(" </FONT></B>")
                        strBuilder.Append(" </TD> " & vbCrLf)
                        strBuilder.Append(" </TR> " & vbCrLf)
                        strBuilder.Append(" </TABLE> " & vbCrLf)
                        strBuilder.Append(" <BR> " & vbCrLf)
                    End If
                Case enAD_Report_Parameter.APP_DATE_AFTER
                    If dtpDate1.Checked = True Then
                        strBuilder.Append(" <TABLE BORDER=0 WIDTH=140%> " & vbCrLf)
                        strBuilder.Append(" <TR> " & vbCrLf)
                        strBuilder.Append(" <TD WIDTH='10%'> " & vbCrLf)
                        strBuilder.Append(" <FONT SIZE=3><B>")
                        strBuilder.Append(" Appointed Date " & cboAppointment.Text & "  :  " & dtpDate1.Value.Date & " " & vbCrLf)
                        strBuilder.Append(" </FONT></B>")
                        strBuilder.Append(" </TD> " & vbCrLf)
                        strBuilder.Append(" </TR> " & vbCrLf)
                        strBuilder.Append(" </TABLE> " & vbCrLf)
                        strBuilder.Append(" <BR> " & vbCrLf)
                    End If
            End Select

            strBuilder.Append(" <TABLE BORDER=1 BORDERCOLOR =BLACK CELLSPACING =0 CELLPADDING =3 WIDTH=140%> " & vbCrLf)
            strBuilder.Append(" <TR ALIGN = CENTER VALIGN=TOP> " & vbCrLf)
            strBuilder.Append(" <TD COLSPAN ='4' ALIGN='LEFT'><FONT SIZE=2><B>" & Language.getMessage(mstrModuleName, 17, "Employee(s): ") & "</B></FONT></TD>" & vbCrLf)
            strBuilder.Append(" </TR> " & vbCrLf)

            strBuilder.Append(" <TR ALIGN = CENTER VALIGN=TOP> " & vbCrLf)
            For j As Integer = 0 To objDataReader.Columns.Count - 2
                If objDataReader.Columns(j).Caption = "" Then Continue For
                strBuilder.Append("<TD BORDER=1 WIDTH='60%' ALIGN='MIDDLE'><FONT SIZE=2><B>" & objDataReader.Columns(j).Caption & "</B></FONT></TD>" & vbCrLf)
            Next
            strBuilder.Append(" </TR> " & vbCrLf)

            Dim mDicEmp As New Dictionary(Of Integer, Integer)

            For i As Integer = 0 To objDataReader.Rows.Count - 1
                If mDicEmp.ContainsKey(CInt(objDataReader.Rows(i)("employeeunkid"))) Then Continue For
                mDicEmp.Add(CInt(objDataReader.Rows(i)("employeeunkid")), CInt(objDataReader.Rows(i)("employeeunkid")))

                strBuilder.Append(" <TR> " & vbCrLf)
                For k As Integer = 0 To objDataReader.Columns.Count - 2
                    If objDataReader.Columns(k).Caption = "" Then Continue For

                    If objDataReader.Columns(k).DataType Is Type.GetType("System.DateTime") Then
                        If objDataReader.Rows(i)(k).ToString() <> "" Then
                            strBuilder.Append("<TD BORDER=1 BORDERCOLOR=BLACK ALIGN='LEFT'><FONT SIZE=2>" & CDate(objDataReader.Rows(i)(k)).ToShortDateString() & "</FONT></TD>" & vbCrLf)
                        Else
                            strBuilder.Append("<TD BORDER=1 BORDERCOLOR=BLACK ALIGN='LEFT'><FONT SIZE=2> </FONT></TD>" & vbCrLf)
                        End If
                    Else
                        'S.SANDEEP [19 FEB 2015] -- START
                        'strBuilder.Append("<TD BORDER=1 BORDERCOLOR=BLACK ALIGN='LEFT'><FONT SIZE=2>" & objDataReader.Rows(i)(k).ToString() & "</FONT></TD>" & vbCrLf)
                        If objDataReader.Columns(k).ExtendedProperties("xScore") IsNot Nothing Then
                            strBuilder.Append("<TD BORDER=1 BORDERCOLOR=BLACK ALIGN='RIGHT'><FONT SIZE=2>" & objDataReader.Compute("SUM(tscore)", "employeeunkid = '" & objDataReader.Rows(i)("employeeunkid").ToString & "'").ToString & "</FONT></TD>" & vbCrLf)
                        Else
                            strBuilder.Append("<TD BORDER=1 BORDERCOLOR=BLACK ALIGN='LEFT'><FONT SIZE=2>" & objDataReader.Rows(i)(k).ToString() & "</FONT></TD>" & vbCrLf)
                        End If
                        'S.SANDEEP [19 FEB 2015] -- END
                    End If
                Next
                strBuilder.Append(" </TR> " & vbCrLf)
            Next
            strBuilder.Append(" </TABLE> " & vbCrLf)
            strBuilder.Append(" <BR> " & vbCrLf)

            strBuilder.Append(" <TABLE BORDER=1 BORDERCOLOR =BLACK CELLSPACING =0 CELLPADDING =3 WIDTH=140%> " & vbCrLf)
            strBuilder.Append(" <TR ALIGN = CENTER VALIGN=TOP> " & vbCrLf)
            strBuilder.Append(" <TD COLSPAN ='4' ALIGN='LEFT'><FONT SIZE=2><B>" & Language.getMessage(mstrModuleName, 20, "Filters(s): ") & "</B></FONT></TD>" & vbCrLf)
            strBuilder.Append(" </TR> " & vbCrLf)

            strBuilder.Append(" <TR> " & vbCrLf)
            For j As Integer = 0 To objDataFilter.Columns.Count - 1
                If objDataFilter.Columns(j).Caption = "" Then Continue For
                strBuilder.Append("<TD BORDER=1 WIDTH='60%' ALIGN='MIDDLE'><FONT SIZE=2><B>" & objDataFilter.Columns(j).Caption & "</B></FONT></TD>" & vbCrLf)
            Next
            strBuilder.Append(" </TR> " & vbCrLf)


            For i As Integer = 0 To objDataFilter.Rows.Count - 1

                If objDataFilter.Rows(i)("AUD").ToString = "D" Then Continue For

                strBuilder.Append(" <TR> " & vbCrLf)
                For k As Integer = 0 To objDataFilter.Columns.Count - 2
                    If objDataFilter.Columns(k).Caption = "" Then Continue For
                    strBuilder.Append("<TD BORDER=1 BORDERCOLOR=BLACK ALIGN='LEFT'><FONT SIZE=2>" & objDataFilter.Rows(i)(k).ToString() & "</FONT></TD>" & vbCrLf)
                Next
            Next


            strBuilder.Append(" </TABLE> " & vbCrLf)

            strBuilder.Append(" </HTML> " & vbCrLf)

            If SavePath.LastIndexOf("\") = SavePath.Length - 1 Then
                SavePath = SavePath.Remove(SavePath.LastIndexOf("\"))
            End If

            blnFlag = SaveExcelfile(SavePath & "\" & flFileName, strBuilder)

            Return blnFlag

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Export_to_Excel", mstrModuleName)
        Finally
        End Try
    End Function

    Private Function SaveExcelfile(ByVal fpath As String, ByVal sb As System.Text.StringBuilder) As Boolean
        Dim fsFile As New FileStream(fpath, FileMode.Create, FileAccess.Write)
        Dim strWriter As New StreamWriter(fsFile)
        Try
            With strWriter
                .BaseStream.Seek(0, SeekOrigin.End)
                .WriteLine(sb)
                .Close()
            End With
            Return True
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SaveExcelfile", mstrModuleName)
            Return False
        Finally
            sb = Nothing
            strWriter = Nothing
            fsFile = Nothing
        End Try
    End Function

    Private Sub Set_OverallFilter()
        Dim blnFlag As Boolean = False
        Try
            Dim dRow As DataRow = Nothing
            Dim dOF() As DataRow = mdtFilter.Select("filter_refid = '" & clsAppraisal_Filter.Filter_Refid.OVERALL_SCORE & "' AND AUD <> 'D'")
            If dOF.Length <= 0 AndAlso txtOverallScore.Text.Trim.Length > 0 Then
                dRow = mdtFilter.NewRow
                dRow.Item("filterunkid") = -1
                dRow.Item("shortlistunkid") = -1
                dRow.Item("filter_refid") = clsAppraisal_Filter.Filter_Refid.OVERALL_SCORE
                dRow.Item("filter_value") = CDbl(txtOverallScore.Text)
                dRow.Item("conditionunkid") = cboOverallCondition.SelectedValue
                dRow.Item("referenceunkid") = -1
                dRow.Item("issummary") = True
                dRow.Item("operator") = clsAppraisal_Filter.Operater_ModeId.OP_AND

                'S.SANDEEP [19 FEB 2015] -- START
                'dRow.Item("isself_score") = chkSelf_Assess.Checked
                'dRow.Item("isassessor_score") = chkAssessor_Assess.Checked
                'dRow.Item("isreviewer_score") = chkReviewer_Assess.Checked
                dRow.Item("filtertypeid") = CInt(CType(cboFinalResult.SelectedItem, ComboBoxValue).Value)
                'S.SANDEEP [19 FEB 2015] -- END

                dRow.Item("AUD") = "A"
                dRow.Item("GUID") = Guid.NewGuid.ToString
                dRow.Item("filter_criteria") = gbOverAllScore.Text
                dRow.Item("col_tag") = gbOverAllScore.Text
                Select Case CInt(cboOverallCondition.SelectedValue)
                    Case 0
                        dRow.Item("contition") = "="
                    Case Else
                        dRow.Item("contition") = cboOverallCondition.Text
                End Select
                dRow.Item("filter_grp") = gbOverAllScore.Text
                mdtFilter.Rows.Add(dRow)
                txtOverallScore.Enabled = False : cboOverallCondition.Enabled = False
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Set_OverallFilter", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

#Region " Form's Events "

    Private Sub frmShortlistingEmployee_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        objMShortList = New clsAppraisal_ShortList_Master
        objMFilter = New clsAppraisal_Filter
        Try
            Call Set_Logo(Me, gApplicationType)
            Language.setLanguage(Me.Name)
            Call OtherSettings()
            lvFilterCriteria.GridLines = False : lvEmployee.GridLines = False
            Call Set_TextValue()
            Call SetVisibility()
            Call FillCombo()
            Call Fill_Grid()
            mdtFilter = objMFilter._DataTable
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmShortlistingEmployee_Load", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

#Region " Button's Events "

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Me.Close()
    End Sub

    Private Sub btnAdd_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAdd.Click
        Dim blnFlag As Boolean = False
        Try
            If IsValidData() = False Then Exit Sub
            If txtBSCTotalScore.Text.Trim.Length > 0 AndAlso txtGETotalScore.Text.Trim.Length > 0 Then
                gbSummOperation.Enabled = False : gbSummaryFilter.Enabled = False
            End If

            If txtOverallScore.Text.Trim.Length > 0 Then
                gbOverAllScore.Enabled = False
            End If
            'If gbSummaryFilter.Checked = True AndAlso gbDetailFilter.Checked = True AndAlso gbOverAllScore.Checked = True Then
            '    Call Set_OverallFilter() : Call Set_SummaryFilter() : Call Set_DetailFilter()
            'ElseIf gbSummaryFilter.Checked = True AndAlso gbDetailFilter.Checked = False AndAlso gbOverAllScore.Checked = False Then
            '    Call Set_SummaryFilter()
            'ElseIf gbSummaryFilter.Checked = False AndAlso gbDetailFilter.Checked = True AndAlso gbOverAllScore.Checked = False Then
            '    Call Set_DetailFilter()
            'ElseIf gbSummaryFilter.Checked = False AndAlso gbDetailFilter.Checked = False AndAlso gbOverAllScore.Checked = True Then
            '    Call Set_OverallFilter()
            'End If

            If gbOverAllScore.Checked = True Then
                Call Set_OverallFilter()
            End If

            If gbSummaryFilter.Checked = True Then
                Call Set_SummaryFilter()
            End If

            If gbDetailFilter.Checked = True Then
                Call Set_DetailFilter()
            End If


            'S.SANDEEP [04 MAR 2015] -- START
            'dBSCPRow = mdtFilter.Select("filter_refid = '" & clsAppraisal_Filter.Filter_Refid.BSC_PERSPECTIVE & "' AND AUD <> 'D'")
            'S.SANDEEP [04 MAR 2015] -- END

            dGEIERow = mdtFilter.Select("filter_refid = '" & clsAppraisal_Filter.Filter_Refid.GENEAL_EVALUATION & "' AND AUD <> 'D'")

            'S.SANDEEP [04 MAR 2015] -- START
            If dGEIERow.Length > 0 Then
                radDetailAND.Enabled = False
                radDetailOR.Enabled = False
                objbtnResetPerspective.Enabled = False
            End If

            'If dBSCPRow.Length > 0 AndAlso dGEIERow.Length > 0 Then
            '    gbDetailOperation.Enabled = False
            'End If
            'S.SANDEEP [04 MAR 2015] -- END



            If mdtFilter.Rows.Count > 0 Then
                Dim dDetail() As DataRow = mdtFilter.Select("issummary=false", "filter_refid")

                For i As Integer = 0 To dDetail.Length - 1
                    Select Case CInt(dDetail(i)("filter_refid"))
                        Case clsAppraisal_Filter.Filter_Refid.GENEAL_EVALUATION
                            'S.SANDEEP [04 MAR 2015] -- START

                            'If blnFlag = False Then
                            '    If radDetailAND.Checked = True Then
                            '        dDetail(i - 1).Item("operator") = clsAppraisal_Filter.Operater_ModeId.OP_AND
                            '        dDetail(i - 1).Item("operation") = radDetailAND.Text
                            '        dDetail(i - 1).AcceptChanges()
                            '    ElseIf radDetailOR.Checked = True Then
                            '        dDetail(i - 1).Item("operator") = clsAppraisal_Filter.Operater_ModeId.OP_OR
                            '        dDetail(i - 1).Item("operation") = radDetailOR.Text
                            '        dDetail(i - 1).AcceptChanges()
                            '    End If
                            '    blnFlag = True
                            'End If
                            'dDetail(i).Item("operator") = clsAppraisal_Filter.Operater_ModeId.OP_AND
                            'dDetail(i).Item("operation") = radDetailAND.Text
                            'dDetail(i).AcceptChanges()


                            If radDetailAND.Checked = True Then
                                dDetail(i).Item("operator") = clsAppraisal_Filter.Operater_ModeId.OP_AND
                                dDetail(i).Item("operation") = radDetailAND.Text
                            ElseIf radDetailOR.Checked = True Then
                                dDetail(i).Item("operator") = clsAppraisal_Filter.Operater_ModeId.OP_OR
                                dDetail(i).Item("operation") = radDetailOR.Text
                            End If
                            dDetail(i).AcceptChanges()
                            'S.SANDEEP [04 MAR 2015] -- END

                    End Select
                Next

                Dim dView As DataView = mdtFilter.DefaultView
                dView.Sort = "filter_refid"
                mdtFilter = dView.ToTable

                mdtFilter.Rows(mdtFilter.Rows.Count - 1)("operator") = -1
                mdtFilter.Rows(mdtFilter.Rows.Count - 1)("operation") = ""
                mdtFilter.AcceptChanges()
            End If

            Call Fill_FilterList()

            If mdtFilter.Rows.Count > 0 Then
                cboFinalResult.Enabled = False
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnAdd_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub btnRemove_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnRemove.Click
        Try
            If lvFilterCriteria.SelectedItems.Count > 0 Then
                Dim drTemp() As DataRow = Nothing
                If CInt(lvFilterCriteria.SelectedItems(0).Tag) = -1 Then
                    drTemp = mdtFilter.Select("GUID = '" & lvFilterCriteria.SelectedItems(0).SubItems(objcolhGuid.Index).Text & "'")
                Else
                    drTemp = mdtFilter.Select("filterunkid = '" & CInt(lvFilterCriteria.SelectedItems(0).Tag) & "'")
                End If

                If drTemp.Length > 0 Then
                    '/* THIS IS FOR ENABLE THE DISABLED CONTROLS WHEN THEY ARE REMOVED FROM LIST */
                    Select Case CInt(drTemp(0)("filter_refid"))
                        Case clsAppraisal_Filter.Filter_Refid.TOTAL_BSC_SCORE
                            txtBSCTotalScore.Enabled = True : cboBSCTotalCondition.Enabled = True
                            gbSummOperation.Enabled = True : gbSummaryFilter.Enabled = True
                            Call objbtnSummaryReset_Click(sender, e)
                        Case clsAppraisal_Filter.Filter_Refid.TOTAL_GE_SCORE
                            txtGETotalScore.Enabled = True : cboGETotalCondition.Enabled = True
                            gbSummOperation.Enabled = True : gbSummaryFilter.Enabled = True
                            Call objbtnSummaryReset_Click(sender, e)
                        Case clsAppraisal_Filter.Filter_Refid.GENEAL_EVALUATION
                            dGEIERow = mdtFilter.Select("filter_refid = '" & clsAppraisal_Filter.Filter_Refid.GENEAL_EVALUATION & "' AND AUD <> 'D' AND referenceunkid <> '" & CInt(drTemp(0)("referenceunkid")) & "'")
                            If dGEIERow.Length <= 0 Then
                                'S.SANDEEP [04 MAR 2015] -- START
                                'gbDetailOperation.Enabled = True
                                pnlDetailCondition.Enabled = True
                                radDetailAND.Enabled = True
                                radDetailOR.Enabled = True
                                'S.SANDEEP [04 MAR 2015] -- END
                                Call objbtnDetailReset_Click(sender, e)
                            End If
                            'S.SANDEEP [ 14 AUG 2013 ] -- START
                            'ENHANCEMENT : TRA CHANGES
                        Case clsAppraisal_Filter.Filter_Refid.OVERALL_SCORE
                            txtOverallScore.Enabled = True : cboOverallCondition.Enabled = True
                            gbOverAllScore.Enabled = True
                            'S.SANDEEP [ 14 AUG 2013 ] -- END
                    End Select


                    Select Case CInt(drTemp(0)("filter_refid"))
                        Case clsAppraisal_Filter.Filter_Refid.TOTAL_BSC_SCORE
                            drTemp(0)("AUD") = "D"
                        Case clsAppraisal_Filter.Filter_Refid.TOTAL_GE_SCORE
                            Dim dTemp() As DataRow = mdtFilter.Select("filter_refid = '" & clsAppraisal_Filter.Filter_Refid.TOTAL_BSC_SCORE & "' AND AUD <> 'D'")
                            If dTemp.Length > 0 Then
                                dTemp(0)("operator") = -1
                                dTemp(0)("operation") = ""
                                dTemp(0).AcceptChanges()
                            End If
                            drTemp(0)("AUD") = "D"
                        Case clsAppraisal_Filter.Filter_Refid.BSC_PERSPECTIVE
                            Select Case CInt(drTemp(0)("operator"))
                                Case clsAppraisal_Filter.Operater_ModeId.OP_AND, clsAppraisal_Filter.Operater_ModeId.OP_OR
                                    Dim iMax As Integer = -1
                                    If IsDBNull(mdtFilter.Compute("MAX(referenceunkid)", "filter_refid = '" & clsAppraisal_Filter.Filter_Refid.BSC_PERSPECTIVE & "' AND AUD <> 'D'")) = False Then
                                        iMax = CInt(mdtFilter.Compute("MAX(referenceunkid)", "filter_refid = '" & clsAppraisal_Filter.Filter_Refid.BSC_PERSPECTIVE & "' AND AUD <> 'D'"))
                                    End If
                                    If iMax > 0 Then
                                        If CInt(drTemp(0)("referenceunkid")) = iMax Then
                                            Dim dTemp() As DataRow = mdtFilter.Select("filter_refid = '" & clsAppraisal_Filter.Filter_Refid.BSC_PERSPECTIVE & "' AND AUD <> 'D' AND referenceunkid <> '" & CInt(drTemp(0)("referenceunkid")) & "'")
                                            If dTemp.Length > 0 Then
                                                dTemp(dTemp.Length - 1)("operator") = drTemp(0)("operator")
                                                dTemp(dTemp.Length - 1)("operation") = drTemp(0)("operation")
                                                dTemp(dTemp.Length - 1).AcceptChanges()
                                            End If
                                        End If
                                    End If
                                    drTemp(0)("AUD") = "D"
                                Case Else
                                    Dim dTemp() As DataRow = mdtFilter.Select("filter_refid = '" & clsAppraisal_Filter.Filter_Refid.BSC_PERSPECTIVE & "' AND AUD <> 'D' AND referenceunkid <> '" & CInt(drTemp(0)("referenceunkid")) & "'")
                                    If dTemp.Length > 0 Then
                                        dTemp(dTemp.Length - 1)("operator") = -1
                                        dTemp(dTemp.Length - 1)("operation") = ""
                                        dTemp(dTemp.Length - 1).AcceptChanges()
                                    End If
                                    drTemp(0)("AUD") = "D"
                            End Select

                        Case clsAppraisal_Filter.Filter_Refid.GENEAL_EVALUATION

                            'S.SANDEEP [04 MAR 2015] -- START

                            'Select Case CInt(drTemp(0)("operator"))
                            '    Case clsAppraisal_Filter.Operater_ModeId.OP_AND, clsAppraisal_Filter.Operater_ModeId.OP_OR
                            '        drTemp(0)("AUD") = "D"
                            '    Case Else
                            '        Dim dTemp() As DataRow = mdtFilter.Select("filter_refid = '" & clsAppraisal_Filter.Filter_Refid.GENEAL_EVALUATION & "' AND AUD <> 'D' AND referenceunkid <> '" & CInt(drTemp(0)("referenceunkid")) & "'")
                            '        If dTemp.Length > 0 Then
                            '            dTemp(dTemp.Length - 1)("operator") = -1
                            '            dTemp(dTemp.Length - 1)("operation") = ""
                            '            dTemp(dTemp.Length - 1).AcceptChanges()
                            '        ElseIf dTemp.Length <= 0 Then
                            '            Dim iMax As Integer = -1
                            '            If IsDBNull(mdtFilter.Compute("MAX(referenceunkid)", "filter_refid = '" & clsAppraisal_Filter.Filter_Refid.BSC_PERSPECTIVE & "' AND AUD <> 'D'")) = False Then
                            '                iMax = CInt(mdtFilter.Compute("MAX(referenceunkid)", "filter_refid = '" & clsAppraisal_Filter.Filter_Refid.BSC_PERSPECTIVE & "' AND AUD <> 'D'"))
                            '            End If
                            '            If iMax > 0 Then
                            '                Dim dRow() As DataRow = mdtFilter.Select("filter_refid = '" & clsAppraisal_Filter.Filter_Refid.BSC_PERSPECTIVE & "' AND AUD <> 'D' AND referenceunkid = '" & CInt(iMax) & "'")
                            '                If dRow.Length > 0 Then
                            '                    dRow(0)("operator") = drTemp(0)("operator")
                            '                    dRow(0)("operation") = drTemp(0)("operation")
                            '                    drTemp(0)("AUD") = "D"
                            '                End If
                            '            End If
                            '        End If
                            '        drTemp(0)("AUD") = "D"
                            'End Select

                            Select Case CInt(drTemp(0)("operator"))
                                Case clsAppraisal_Filter.Operater_ModeId.OP_AND, clsAppraisal_Filter.Operater_ModeId.OP_OR
                                    Dim iMax As Integer = -1
                                    If IsDBNull(mdtFilter.Compute("MAX(referenceunkid)", "filter_refid = '" & clsAppraisal_Filter.Filter_Refid.GENEAL_EVALUATION & "' AND AUD <> 'D'")) = False Then
                                        iMax = CInt(mdtFilter.Compute("MAX(referenceunkid)", "filter_refid = '" & clsAppraisal_Filter.Filter_Refid.GENEAL_EVALUATION & "' AND AUD <> 'D'"))
                                    End If
                                    If iMax > 0 Then
                                        If CInt(drTemp(0)("referenceunkid")) = iMax Then
                                            Dim dTemp() As DataRow = mdtFilter.Select("filter_refid = '" & clsAppraisal_Filter.Filter_Refid.GENEAL_EVALUATION & "' AND AUD <> 'D' AND referenceunkid <> '" & CInt(drTemp(0)("referenceunkid")) & "'")
                                            If dTemp.Length > 0 Then
                                                dTemp(dTemp.Length - 1)("operator") = drTemp(0)("operator")
                                                dTemp(dTemp.Length - 1)("operation") = drTemp(0)("operation")
                                                dTemp(dTemp.Length - 1).AcceptChanges()
                                            End If
                                        End If
                                    End If
                                    drTemp(0)("AUD") = "D"
                                Case Else

                                    Dim dTemp() As DataRow = mdtFilter.Select("filter_refid = '" & clsAppraisal_Filter.Filter_Refid.GENEAL_EVALUATION & "' AND AUD <> 'D' AND referenceunkid <> '" & CInt(drTemp(0)("referenceunkid")) & "'")
                                    If dTemp.Length > 0 Then
                                        dTemp(dTemp.Length - 1)("operator") = -1
                                        dTemp(dTemp.Length - 1)("operation") = ""
                                        dTemp(dTemp.Length - 1).AcceptChanges()
                                    End If
                                    drTemp(0)("AUD") = "D"
                            End Select

                            'S.SANDEEP [04 MAR 2015] -- END
                        Case clsAppraisal_Filter.Filter_Refid.OVERALL_SCORE
                            drTemp(0)("AUD") = "D"
                    End Select
                End If
                Call Fill_FilterList()
            End If

            If lvFilterCriteria.Items.Count <= 0 Then
                cboFinalResult.Enabled = True
                lvEmployee.Items.Clear()
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnRemove_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub btnApplyFilter_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnApplyFilter.Click
        Try
            'Dim xTotalScore As Decimal = 0
            'Dim objEvaluation As New clsevaluation_analysis_master
            'xTotalScore = objEvaluation.Compute_Score(enAssessmentMode.SELF_ASSESSMENT, True, ConfigParameter._Object._ScoringOptionId, enAssess_Computation_Formulas.POST_TO_PAYROLL_VALUE, 133, 7)

            If CInt(cboAppointment.SelectedValue) = enAD_Report_Parameter.APP_DATE_FROM Then
                If dtpDate1.Checked = True AndAlso dtpDate2.Checked = False Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 23, "Sorry, Appointment To Date is mandatory information. Please set Appointment To Date."), enMsgBoxStyle.Information)
                    dtpDate2.Focus()
                    Exit Sub
                ElseIf dtpDate1.Checked = False AndAlso dtpDate2.Checked = True Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 24, "Sorry, Appointment From Date is mandatory information. Please set Appointment From Date."), enMsgBoxStyle.Information)
                    dtpDate1.Focus()
                    Exit Sub
                ElseIf dtpDate1.Checked = True AndAlso dtpDate2.Checked = True Then
                    If dtpDate2.Value.Date < dtpDate1.Value.Date Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 25, "Sorry, Appointment To Date cannot be less then Appointment From Date."), enMsgBoxStyle.Information)
                        dtpDate2.Focus()
                        Exit Sub
                    End If
                End If
            End If

            objMFilter._AppointmentTypeId = CInt(cboAppointment.SelectedValue)
            If dtpDate1.Checked = True Then
                objMFilter._Date1 = dtpDate1.Value.Date
            End If

            If dtpDate2.Visible = True Then
                If dtpDate2.Checked = True Then
                    objMFilter._Date2 = dtpDate2.Value.Date
                End If
            End If

            'S.SANDEEP [19 FEB 2015] -- START
            'dsFilteredEmp = objMFilter.GetFilteredEmployee(mdtFilter, CInt(cboPeriod.SelectedValue))

            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'dsFilteredEmp = objMFilter.GetFilteredEmployee(mdtFilter, CInt(cboPeriod.SelectedValue), CType(CInt(CType(cboFinalResult.SelectedItem, ComboBoxValue).Value), enApprFilterTypeId), ConfigParameter._Object._ScoringOptionId)

            'Shani (23-Nov-2016) -- Start
            'Enhancement - Add New PA Enhancement (Agreed Score,exc) Given by Akiba,KBC,TNP exc...
            'dsFilteredEmp = objMFilter.GetFilteredEmployee(mdtFilter, CInt(cboPeriod.SelectedValue), CType(CInt(CType(cboFinalResult.SelectedItem, ComboBoxValue).Value), enApprFilterTypeId), ConfigParameter._Object._ScoringOptionId, eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
            '                                               ConfigParameter._Object._Self_Assign_Competencies) 'S.SANDEEP [08 Jan 2016] -- START{ConfigParameter._Object._Self_Assign_Competencies} -- END

            'S.SANDEEP |27-MAY-2019| -- START
            'ISSUE/ENHANCEMENT : [Score Calibration Process in Performance & Assessment Module]
            'dsFilteredEmp = objMFilter.GetFilteredEmployee(mdtFilter, CInt(cboPeriod.SelectedValue), CType(CInt(CType(cboFinalResult.SelectedItem, ComboBoxValue).Value), enApprFilterTypeId), ConfigParameter._Object._ScoringOptionId, eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
            '                                               ConfigParameter._Object._Self_Assign_Competencies, ConfigParameter._Object._IsUseAgreedScore)

            dsFilteredEmp = objMFilter.GetFilteredEmployee(mdtFilter, CInt(cboPeriod.SelectedValue), CType(CInt(CType(cboFinalResult.SelectedItem, ComboBoxValue).Value), enApprFilterTypeId), ConfigParameter._Object._ScoringOptionId, eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                           ConfigParameter._Object._Self_Assign_Competencies, ConfigParameter._Object._IsUseAgreedScore, ConfigParameter._Object._IsCalibrationSettingActive)
            'S.SANDEEP |27-MAY-2019| -- END

            'Shani (23-Nov-2016) -- End
            'S.SANDEEP [04 JUN 2015] -- END

            'S.SANDEEP [19 FEB 2015] -- END

            lvEmployee.Items.Clear()

            For Each drRow As DataRow In dsFilteredEmp.Tables(0).Rows
                Dim lvItem As New ListViewItem
                If lvEmployee.FindItemWithText(CStr(drRow.Item("employeecode"))) Is Nothing Then
                    Dim xScore As Decimal = CDec(dsFilteredEmp.Tables(0).Compute("SUM(tscore)", "employeecode = '" & drRow.Item("employeecode").ToString & "'"))
                    'S.SANDEEP [04-AUG-2017] -- START
                    'ISSUE/ENHANCEMENT : CHANGE THE SCORE PICKING METHOD
                    Dim iEcode As String = drRow.Item("employeecode").ToString()
                    Dim iCount As Integer = CInt(dsFilteredEmp.Tables(0).Select("employeecode = '" & drRow.Item("employeecode").ToString & "'").Length)
                    If iCount > 1 Then
                        xScore = CDec(Format(CDec(xScore / iCount), "#######################0.#0"))
                    End If
                    'S.SANDEEP [04-AUG-2017] -- END
                    lvItem.Text = drRow.Item("employeecode").ToString
                    lvItem.SubItems.Add(drRow.Item("ENAME").ToString)
                    lvItem.SubItems.Add(CDate(drRow.Item("Date")).ToShortDateString)
                    lvItem.SubItems.Add(drRow.Item("EMAIL").ToString)
                    lvItem.SubItems.Add(drRow.Item("F_RefId").ToString)
                    lvItem.SubItems.Add(drRow.Item("JOB_TITLE").ToString)
                    lvItem.SubItems.Add(drRow.Item("DEPT").ToString)
                    'S.SANDEEP [19 FEB 2015] -- START
                    'lvItem.SubItems.Add(drRow.Item("SCORE").ToString)
                    lvItem.SubItems.Add(Format(CDec(xScore), "#######################0.#0"))
                    'S.SANDEEP [19 FEB 2015] -- END
                    lvItem.Tag = drRow.Item("EGUID")

                    lvEmployee.Items.Add(lvItem)
                End If
            Next

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnApplyFilter_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Dim blnflag As Boolean = False
        Try
            If lvEmployee.Items.Count <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 14, "Sorry, there is no employee in the list to export."), enMsgBoxStyle.Information)
                Exit Sub
            End If

            If txtRemark.Text.Trim.Length <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 19, "Remark cannot be blank. Remark is mandatory information."), enMsgBoxStyle.Information)
                txtRemark.Focus()
                Exit Sub
            End If

            objMShortList._Referencedate = ConfigParameter._Object._CurrentDateAndTime
            objMShortList._Userunkid = User._Object._Userunkid
            objMShortList._Referenceno = Now.Millisecond.ToString & ConfigParameter._Object._CurrentDateAndTime.Year.ToString & ConfigParameter._Object._CurrentDateAndTime.ToString("hhmmss") & ConfigParameter._Object._CurrentDateAndTime.Date.Day.ToString("0#") & ConfigParameter._Object._CurrentDateAndTime.Date.Month.ToString("0#")
            objMShortList._Remark = txtRemark.Text
            objMShortList._Periodunkid = CInt(cboPeriod.SelectedValue)


            'S.SANDEEP [28-May-2018] -- START
            'ISSUE/ENHANCEMENT : {Audit Trails} 
            objMShortList._FormName = mstrModuleName
            objMShortList._LoginEmployeeunkid = 0
            objMShortList._ClientIP = getIP()
            objMShortList._HostName = getHostName()
            objMShortList._FromWeb = False
            objMShortList._AuditUserId = User._Object._Userunkid
objMShortList._CompanyUnkid = Company._Object._Companyunkid
            objMShortList._AuditDate = ConfigParameter._Object._CurrentDateAndTime
            'S.SANDEEP [28-May-2018] -- END

            blnflag = objMShortList.Insert(mdtFilter, dsFilteredEmp)

            If blnflag = False And objMShortList._Message <> "" Then
                eZeeMsgBox.Show(objMShortList._Message, enMsgBoxStyle.Information)
            Else
                dsFilteredEmp.Tables(0).Columns.Remove("AUD")
            End If

            Dim objFile As New SaveFileDialog
            objFile.OverwritePrompt = True
            objFile.Filter = "Xls Files (*.xls) |*.xls"

            If objFile.ShowDialog() = Windows.Forms.DialogResult.OK Then
                Dim obj As New FileInfo(objFile.FileName)
                Dim mstrFilePath As String = objFile.FileName.Substring(0, objFile.FileName.LastIndexOf(CChar("\")))
                If Export_to_Excel(True, obj.Name, mstrFilePath, dsFilteredEmp.Tables(0), mdtFilter) Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 15, "Data Exported successfully."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                    Call ReportFunction.Open_ExportedFile(ConfigParameter._Object._OpenAfterExport, mstrFilePath)
                End If
            End If

            If blnflag Then
                mblnCancel = False
                Me.Close()
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnSave_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub btnExport_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnExport.Click
        Try

            If lvEmployee.Items.Count <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 14, "Sorry, there is no employee in the list to export."), enMsgBoxStyle.Information)
                Exit Sub
            End If

            Dim objFile As New SaveFileDialog
            objFile.OverwritePrompt = True
            objFile.Filter = "Xls Files (*.xls) |*.xls"

            If objFile.ShowDialog() = Windows.Forms.DialogResult.OK Then
                Dim obj As New FileInfo(objFile.FileName)
                Dim mstrFilePath As String = objFile.FileName.Substring(0, objFile.FileName.LastIndexOf(CChar("\")))
                If Export_to_Excel(False, obj.Name, mstrFilePath, dsFilteredEmp.Tables(0), mdtFilter) Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 15, "Data Exported successfully."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                    Call ReportFunction.Open_ExportedFile(ConfigParameter._Object._OpenAfterExport, mstrFilePath)
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

#Region " Controls "

    Private Sub gbDetailFilter_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles gbDetailFilter.CheckedChanged
        Try
            Call SetVisibility()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "gbDetailFilter_CheckedChanged", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub dgvData_CellContentClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvData.CellContentClick, dgvData.CellContentDoubleClick
        Try
            Dim i As Integer
            If e.RowIndex = -1 Then Exit Sub

            If Me.dgvData.IsCurrentCellDirty Then
                Me.dgvData.CommitEdit(DataGridViewDataErrorContexts.Commit)
            End If

            If CBool(dgvData.Rows(e.RowIndex).Cells(objdgcolhIsGrp.Index).Value) = True Then
                Select Case CInt(e.ColumnIndex)
                    Case 0
                        If dgvData.Rows(e.RowIndex).Cells(objdgcolhCollaps.Index).Value Is "-" Then
                            dgvData.Rows(e.RowIndex).Cells(objdgcolhCollaps.Index).Value = "+"
                        Else
                            dgvData.Rows(e.RowIndex).Cells(objdgcolhCollaps.Index).Value = "-"
                        End If

                        For i = e.RowIndex + 1 To dgvData.RowCount - 1
                            If CInt(dgvData.Rows(e.RowIndex).Cells(objdgcolhGrpId.Index).Value) = CInt(dgvData.Rows(i).Cells(objdgcolhGrpId.Index).Value) Then
                                If dgvData.Rows(i).Visible = False Then
                                    dgvData.Rows(i).Visible = True
                                Else
                                    dgvData.Rows(i).Visible = False
                                End If
                            Else
                                Exit For
                            End If
                        Next
                End Select
            Else

            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgvData_CellContentClick", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub dgvData_DataError(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewDataErrorEventArgs) Handles dgvData.DataError

    End Sub

    Private Sub dgvData_CellEnter(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvData.CellEnter
        Try
            If e.ColumnIndex = dgcolhCondition.Index Or e.ColumnIndex = dgcolhScore.Index Then
                SendKeys.Send("{F2}")
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgvData_CellEnter", mstrModuleName)
        End Try
    End Sub

    Private Sub dgvData_EditingControlShowing(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewEditingControlShowingEventArgs) Handles dgvData.EditingControlShowing
        Dim txt As TextBox
        Try
            If dgvData.CurrentCell.ColumnIndex = dgcolhScore.Index AndAlso TypeOf e.Control Is TextBox Then
                txt = CType(e.Control, TextBox)
                RemoveHandler txt.KeyPress, AddressOf tb_keypress

                AddHandler txt.KeyPress, AddressOf tb_keypress
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgvData_EditingControlShowing", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnSummaryReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSummaryReset.Click
        Try
            radSummaryAND.Checked = False : radSummaryOR.Checked = False
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSummaryReset_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub objbtnDetailReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Try
            radDetailAND.Checked = False
            radDetailOR.Checked = False
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnDetailReset_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub objbtnResetPerspective_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnResetPerspective.Click
        Try
            radDetailAND.Checked = False
            radDetailOR.Checked = False
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnResetPerspective_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub cboPeriod_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Call Fill_Grid()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboPeriod_SelectedIndexChanged", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub cboAppointment_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboAppointment.SelectedIndexChanged
        Try
            objlblCaption.Text = cboAppointment.Text
            Select Case CInt(cboAppointment.SelectedValue)
                Case enAD_Report_Parameter.APP_DATE_FROM
                    If dtpDate2.Visible = False Then dtpDate2.Visible = True
                    If lblTo.Visible = False Then lblTo.Visible = True
                Case enAD_Report_Parameter.APP_DATE_BEFORE
                    dtpDate2.Visible = False : lblTo.Visible = False
                Case enAD_Report_Parameter.APP_DATE_AFTER
                    dtpDate2.Visible = False : lblTo.Visible = False
            End Select
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboAppointment_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

    'S.SANDEEP [04 MAR 2015] -- START

    'Private Sub dgvPerspective_DataError(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewDataErrorEventArgs)

    'End Sub

    'Private Sub dgvPerspective_CellEnter(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs)
    '    Try
    '        If e.ColumnIndex = dgcolhPCondition.Index Or e.ColumnIndex = dgcolhPScore.Index Then
    '            SendKeys.Send("{F2}")
    '        End If
    '    Catch ex As Exception
    '        DisplayError.Show("-1", ex.Message, "dgvPerspective_CellEnter", mstrModuleName)
    '    End Try
    'End Sub

    'Private Sub dgvPerspective_EditingControlShowing(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewEditingControlShowingEventArgs)
    '    Dim txt As TextBox
    '    Try
    '        If dgvData.CurrentCell.ColumnIndex = dgcolhPScore.Index AndAlso TypeOf e.Control Is TextBox Then
    '            txt = CType(e.Control, TextBox)
    '            RemoveHandler txt.KeyPress, AddressOf tb_keypress

    '            AddHandler txt.KeyPress, AddressOf tb_keypress
    '        End If
    '    Catch ex As Exception
    '        DisplayError.Show("-1", ex.Message, "dgvPerspective_EditingControlShowing", mstrModuleName)
    '    End Try
    'End Sub

    'S.SANDEEP [04 MAR 2015] -- END

   

#End Region

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Private Sub OtherSettings()
		Try
			Me.SuspendLayout()
			
			Call SetLanguage()
			
			Me.gbRemarks.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbRemarks.ForeColor = GUI._eZeeContainerHeaderForeColor 

			Me.gbPeriod.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbPeriod.ForeColor = GUI._eZeeContainerHeaderForeColor 

			Me.gbSummaryFilter.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbSummaryFilter.ForeColor = GUI._eZeeContainerHeaderForeColor 

			Me.gbDetailFilter.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbDetailFilter.ForeColor = GUI._eZeeContainerHeaderForeColor 

			Me.gbOverAllScore.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbOverAllScore.ForeColor = GUI._eZeeContainerHeaderForeColor 


			Me.btnClose.GradientBackColor = GUI._ButttonBackColor 
			Me.btnClose.GradientForeColor = GUI._ButttonFontColor

			Me.btnAdd.GradientBackColor = GUI._ButttonBackColor 
			Me.btnAdd.GradientForeColor = GUI._ButttonFontColor

			Me.btnApplyFilter.GradientBackColor = GUI._ButttonBackColor 
			Me.btnApplyFilter.GradientForeColor = GUI._ButttonFontColor

			Me.btnRemove.GradientBackColor = GUI._ButttonBackColor 
			Me.btnRemove.GradientForeColor = GUI._ButttonFontColor

			Me.btnSave.GradientBackColor = GUI._ButttonBackColor 
			Me.btnSave.GradientForeColor = GUI._ButttonFontColor

			Me.btnExport.GradientBackColor = GUI._ButttonBackColor 
			Me.btnExport.GradientForeColor = GUI._ButttonFontColor


			Me.ResumeLayout()
		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "OtherSettings", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetLanguage()
		Try
			Me.Text = Language._Object.getCaption(Me.Name, Me.Text)
			
			Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
			Me.btnAdd.Text = Language._Object.getCaption(Me.btnAdd.Name, Me.btnAdd.Text)
			Me.btnApplyFilter.Text = Language._Object.getCaption(Me.btnApplyFilter.Name, Me.btnApplyFilter.Text)
			Me.btnRemove.Text = Language._Object.getCaption(Me.btnRemove.Name, Me.btnRemove.Text)
			Me.colhFieldName.Text = Language._Object.getCaption(CStr(Me.colhFieldName.Tag), Me.colhFieldName.Text)
			Me.colhValue.Text = Language._Object.getCaption(CStr(Me.colhValue.Tag), Me.colhValue.Text)
			Me.colhCondition.Text = Language._Object.getCaption(CStr(Me.colhCondition.Tag), Me.colhCondition.Text)
			Me.colhOperation.Text = Language._Object.getCaption(CStr(Me.colhOperation.Tag), Me.colhOperation.Text)
			Me.colhEcode.Text = Language._Object.getCaption(CStr(Me.colhEcode.Tag), Me.colhEcode.Text)
			Me.colhEName.Text = Language._Object.getCaption(CStr(Me.colhEName.Tag), Me.colhEName.Text)
			Me.colhAppDate.Text = Language._Object.getCaption(CStr(Me.colhAppDate.Tag), Me.colhAppDate.Text)
			Me.colhEmail.Text = Language._Object.getCaption(CStr(Me.colhEmail.Tag), Me.colhEmail.Text)
			Me.btnSave.Text = Language._Object.getCaption(Me.btnSave.Name, Me.btnSave.Text)
			Me.btnExport.Text = Language._Object.getCaption(Me.btnExport.Name, Me.btnExport.Text)
			Me.gbRemarks.Text = Language._Object.getCaption(Me.gbRemarks.Name, Me.gbRemarks.Text)
			Me.gbPeriod.Text = Language._Object.getCaption(Me.gbPeriod.Name, Me.gbPeriod.Text)
			Me.lblPeriod.Text = Language._Object.getCaption(Me.lblPeriod.Name, Me.lblPeriod.Text)
			Me.gbSummaryFilter.Text = Language._Object.getCaption(Me.gbSummaryFilter.Name, Me.gbSummaryFilter.Text)
			Me.gbSummOperation.Text = Language._Object.getCaption(Me.gbSummOperation.Name, Me.gbSummOperation.Text)
			Me.radSummaryAND.Text = Language._Object.getCaption(Me.radSummaryAND.Name, Me.radSummaryAND.Text)
			Me.radSummaryOR.Text = Language._Object.getCaption(Me.radSummaryOR.Name, Me.radSummaryOR.Text)
			Me.elGeneralEvaluation.Text = Language._Object.getCaption(Me.elGeneralEvaluation.Name, Me.elGeneralEvaluation.Text)
			Me.lblGETotalCondition.Text = Language._Object.getCaption(Me.lblGETotalCondition.Name, Me.lblGETotalCondition.Text)
			Me.lblBSCTotalScore.Text = Language._Object.getCaption(Me.lblBSCTotalScore.Name, Me.lblBSCTotalScore.Text)
			Me.elBalanceScorecard.Text = Language._Object.getCaption(Me.elBalanceScorecard.Name, Me.elBalanceScorecard.Text)
			Me.lblGETotalScore.Text = Language._Object.getCaption(Me.lblGETotalScore.Name, Me.lblGETotalScore.Text)
			Me.lblBSCTotalCondition.Text = Language._Object.getCaption(Me.lblBSCTotalCondition.Name, Me.lblBSCTotalCondition.Text)
			Me.gbDetailFilter.Text = Language._Object.getCaption(Me.gbDetailFilter.Name, Me.gbDetailFilter.Text)
			Me.EZeeLine1.Text = Language._Object.getCaption(Me.EZeeLine1.Name, Me.EZeeLine1.Text)
			Me.colhJob.Text = Language._Object.getCaption(CStr(Me.colhJob.Tag), Me.colhJob.Text)
			Me.colhDepartment.Text = Language._Object.getCaption(CStr(Me.colhDepartment.Tag), Me.colhDepartment.Text)
			Me.colhOverallScore.Text = Language._Object.getCaption(CStr(Me.colhOverallScore.Tag), Me.colhOverallScore.Text)
			Me.gbOverAllScore.Text = Language._Object.getCaption(Me.gbOverAllScore.Name, Me.gbOverAllScore.Text)
			Me.lblOverallScore.Text = Language._Object.getCaption(Me.lblOverallScore.Name, Me.lblOverallScore.Text)
			Me.lblOverallCondition.Text = Language._Object.getCaption(Me.lblOverallCondition.Name, Me.lblOverallCondition.Text)
			Me.lblAppointment.Text = Language._Object.getCaption(Me.lblAppointment.Name, Me.lblAppointment.Text)
			Me.lblTo.Text = Language._Object.getCaption(Me.lblTo.Name, Me.lblTo.Text)
			Me.dgcolhItems.HeaderText = Language._Object.getCaption(Me.dgcolhItems.Name, Me.dgcolhItems.HeaderText)
			Me.dgcolhScore.HeaderText = Language._Object.getCaption(Me.dgcolhScore.Name, Me.dgcolhScore.HeaderText)
			Me.dgcolhCondition.HeaderText = Language._Object.getCaption(Me.dgcolhCondition.Name, Me.dgcolhCondition.HeaderText)
			Me.lblFinalResult.Text = Language._Object.getCaption(Me.lblFinalResult.Name, Me.lblFinalResult.Text)
			Me.radDetailAND.Text = Language._Object.getCaption(Me.radDetailAND.Name, Me.radDetailAND.Text)
			Me.radDetailOR.Text = Language._Object.getCaption(Me.radDetailOR.Name, Me.radDetailOR.Text)

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetLanguage", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "Sorry, you cannot add this filter as no value is provided to match.")
			Language.setMessage(mstrModuleName, 2, "Sorry, you cannot add this filter as General Evaluation Total Score value is not provided to match.")
			Language.setMessage(mstrModuleName, 3, "Sorry, you cannot add this filter as Balance Score Card value is not provided to match.")
			Language.setMessage(mstrModuleName, 4, "Sorry, you cannot add this filter as no operation is set between BSC and General Evaluation.")
			Language.setMessage(mstrModuleName, 14, "Sorry, there is no employee in the list to export.")
			Language.setMessage(mstrModuleName, 15, "Data Exported successfully.")
			Language.setMessage(mstrModuleName, 16, "Employee Short Listing")
			Language.setMessage(mstrModuleName, 17, "Employee(s):")
			Language.setMessage(mstrModuleName, 19, "Remark cannot be blank. Remark is mandatory information.")
			Language.setMessage(mstrModuleName, 20, "Filters(s):")
			Language.setMessage(mstrModuleName, 21, "Period is mandatory information. Please select Period to continue.")
			Language.setMessage(mstrModuleName, 22, "Sorry, you cannot add this filter as Overall Score value is not provided to match.")
			Language.setMessage(mstrModuleName, 23, "Sorry, Appointment To Date is mandatory information. Please set Appointment To Date.")
			Language.setMessage(mstrModuleName, 24, "Sorry, Appointment From Date is mandatory information. Please set Appointment From Date.")
			Language.setMessage(mstrModuleName, 25, "Sorry, Appointment To Date cannot be less then Appointment From Date.")
			Language.setMessage(mstrModuleName, 111, "Overall Score")
			Language.setMessage(mstrModuleName, 112, "Self Score")
			Language.setMessage(mstrModuleName, 113, "Assessor Score")
			Language.setMessage(mstrModuleName, 114, "Reviewer Score")
			Language.setMessage(mstrModuleName, 115, "Sorry you cannot add this filter as Competencies Item(s) score is not provided.")
			Language.setMessage(mstrModuleName, 116, "Sorry you cannot add this filter as no operation set between Competencies Item(s).")
			Language.setMessage(mstrModuleName, 117, "Sorry you cannot add this filter as operation cannot be performed on single Competencies Item(s).")
			Language.setMessage("clsMasterData", 310, "<")
			Language.setMessage("clsMasterData", 311, ">=")
			Language.setMessage("clsMasterData", 312, "<=")
			Language.setMessage("clsMasterData", 408, ">")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class

'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''' AFTER DISCUSSION OF IMPLEMENTING FORMULA WITH ANDREW MUGA DATED : 19 FEB 2015
'#Region " Private Variables "

'    Private mSize As Size
'    Private mblnCancel As Boolean = True
'    Private menAction As enAction = enAction.ADD_ONE
'    Private mdtFilter As DataTable
'    Private dsCondition As New DataSet
'    Private mdtGeneral As DataTable
'    Private objMShortList As clsAppraisal_ShortList_Master
'    Private objMFilter As New clsAppraisal_Filter
'    Private dBSCPRow() As DataRow = Nothing
'    Private dGEIERow() As DataRow = Nothing
'    Private iCount As Integer = 0
'    Dim dsFilteredEmp As New DataSet
'    'S.SANDEEP [ 05 NOV 2014 ] -- START
'    Private mdtPerspective As DataTable
'    'S.SANDEEP [ 05 NOV 2014 ] -- END

'#End Region

'#Region " Display Dialog "

'    Public Function displayDialog(ByVal eAction As enAction) As Boolean
'        Try
'            menAction = eAction
'            Me.ShowDialog()
'            Return Not mblnCancel
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "displayDialog", mstrModuleName)
'        End Try
'    End Function

'#End Region

'#Region " Private Methods & Funtions "

'    Private Sub FillCombo()
'        Dim objMCondition As New clsMasterData

'        'S.SANDEEP [ 05 MARCH 2012 ] -- START
'        'ENHANCEMENT : TRA CHANGES
'        Dim objPeriod As New clscommom_period_Tran
'        Dim dsList As DataSet
'        'S.SANDEEP [ 05 MARCH 2012 ] -- END
'        'S.SANDEEP [ 22 OCT 2013 ] -- START
'        Dim objMaster As New clsMasterData
'        'S.SANDEEP [ 22 OCT 2013 ] -- END
'        Try
'            dsCondition = objMCondition.GetCondition(True)
'            'With cboBCondition
'            '    .ValueMember = "id"
'            '    .DisplayMember = "Name"
'            '    .DataSource = dsCondition.Tables(0).Copy
'            '    .SelectedValue = 0
'            'End With

'            With cboBSCTotalCondition
'                .ValueMember = "id"
'                .DisplayMember = "Name"
'                .DataSource = dsCondition.Tables(0).Copy
'                .SelectedValue = 0
'            End With

'            'With cboCCondition
'            '    .ValueMember = "id"
'            '    .DisplayMember = "Name"
'            '    .DataSource = dsCondition.Tables(0).Copy
'            '    .SelectedValue = 0
'            'End With

'            'With cboFCondition
'            '    .ValueMember = "id"
'            '    .DisplayMember = "Name"
'            '    .DataSource = dsCondition.Tables(0).Copy
'            '    .SelectedValue = 0
'            'End With

'            With cboGETotalCondition
'                .ValueMember = "id"
'                .DisplayMember = "Name"
'                .DataSource = dsCondition.Tables(0).Copy
'                .SelectedValue = 0
'            End With

'            'With cboOCondition
'            '    .ValueMember = "id"
'            '    .DisplayMember = "Name"
'            '    .DataSource = dsCondition.Tables(0).Copy
'            '    .SelectedValue = 0
'            'End With

'            'S.SANDEEP [ 14 AUG 2013 ] -- START
'            'ENHANCEMENT : TRA CHANGES
'            With cboOverallCondition
'                .ValueMember = "id"
'                .DisplayMember = "Name"
'                .DataSource = dsCondition.Tables(0).Copy
'                .SelectedValue = 0
'            End With
'            'S.SANDEEP [ 14 AUG 2013 ] -- END



'            'S.SANDEEP [ 05 MARCH 2012 ] -- START
'            'ENHANCEMENT : TRA CHANGES

'            'S.SANDEEP [ 13 AUG 2013 ] -- START
'            'ENHANCEMENT : TRA CHANGES
'            'dsList = objPeriod.getListForCombo(enModuleReference.Assessment, FinancialYear._Object._YearUnkid, "List", True)
'            'With cboPeriod
'            '    .ValueMember = "periodunkid"
'            '    .DisplayMember = "name"
'            '    .DataSource = dsList.Tables("List")
'            '    .SelectedValue = 0
'            'End With
'            RemoveHandler cboPeriod.SelectedIndexChanged, AddressOf cboPeriod_SelectedIndexChanged
'            dsList = objPeriod.getListForCombo(enModuleReference.Assessment, 0, "List", True, enStatusType.Open)
'            With cboPeriod
'                .ValueMember = "periodunkid"
'                .DisplayMember = "name"
'                .DataSource = dsList.Tables("List")
'                .SelectedValue = 0
'            End With
'            AddHandler cboPeriod.SelectedIndexChanged, AddressOf cboPeriod_SelectedIndexChanged
'            'S.SANDEEP [ 13 AUG 2013 ] -- END


'            'S.SANDEEP [ 05 MARCH 2012 ] -- END


'            'S.SANDEEP [ 22 OCT 2013 ] -- START
'            dsList = objMaster.GetAD_Parameter_List(False, "List")
'            With cboAppointment
'                .ValueMember = "Id"
'                .DisplayMember = "Name"
'                .DataSource = dsList.Tables("List")
'                .SelectedValue = 1
'            End With
'            'S.SANDEEP [ 22 OCT 2013 ] -- END

'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
'        Finally
'            objMCondition = Nothing
'        End Try
'    End Sub

'    Private Sub Fill_Grid()
'        Try
'            Call Generate_Table()

'            dgvData.AutoGenerateColumns = False

'            dgcolhItems.DataPropertyName = "Items"
'            dgcolhScore.DataPropertyName = "Score"
'            objdgcolhIsGrp.DataPropertyName = "IsGrp"
'            objdgcolhGrpId.DataPropertyName = "GrpId"
'            objdgcolhItemId.DataPropertyName = "ItemId"

'            dgcolhCondition.ValueMember = "Id"
'            dgcolhCondition.DisplayMember = "Name"
'            'dgcolhCondition.DisplayStyle = DataGridViewComboBoxDisplayStyle.ComboBox
'            'dgcolhCondition.DropDownWidth = 120
'            dgcolhCondition.DataSource = dsCondition.Tables(0).Copy

'            dgcolhCondition.DataPropertyName = "Id"

'            dgvData.DataSource = mdtGeneral

'            dgvData.Refresh()

'            Dim pCell As New clsMergeCell
'            For Each dgvRow As DataGridViewRow In dgvData.Rows
'                If CBool(dgvRow.Cells(objdgcolhIsGrp.Index).Value) = True Then
'                    dgvRow.ReadOnly = True
'                    If dgvRow.Cells(objdgcolhCollaps.Index).Value Is Nothing Then
'                        dgvRow.Cells(objdgcolhCollaps.Index).Value = "-"
'                        dgvRow.Cells(objdgcolhCollaps.Index).Style.ForeColor = Color.White
'                    End If
'                    dgvRow.Cells(objdgcolhCollaps.Index).Style.BackColor = Color.Gray
'                    dgvRow.Cells(objdgcolhCollaps.Index).Style.SelectionBackColor = Color.Gray
'                    dgvRow.Cells(objdgcolhCollaps.Index).Style.ForeColor = Color.White

'                    pCell.MakeMerge(dgvData, dgvRow.Index, 1, dgvRow.Cells.Count - 1, Color.Gray, Color.White, dgvRow.Cells(dgcolhItems.Index).Value.ToString, "", picStayView.Image)
'                Else
'                    dgvRow.Cells(objdgcolhCollaps.Index).Value = ""
'                    dgvRow.Cells(dgcolhCondition.Index).Value = 0
'                End If
'            Next

'            Call Generate_Perspective_Table()

'            dgvPerspective.AutoGenerateColumns = False
'            dgcolhPerspective.DataPropertyName = "Item"
'            dgcolhPScore.DataPropertyName = "Score"
'            objdgcolhPerspectiveId.DataPropertyName = "PerId"

'            dgcolhPCondition.ValueMember = "Id"
'            dgcolhPCondition.DisplayMember = "Name"
'            dgcolhPCondition.DataSource = dsCondition.Tables(0).Copy
'            dgcolhPCondition.DataPropertyName = "Id"

'            dgvPerspective.DataSource = mdtPerspective
'            dgvPerspective.Refresh()
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "Fill_Grid", mstrModuleName)
'        Finally
'        End Try
'    End Sub

'    Private Sub Generate_Table()
'        Try
'            mdtGeneral = New DataTable("GE")

'            mdtGeneral.Columns.Add("Items", System.Type.GetType("System.String")).DefaultValue = ""
'            mdtGeneral.Columns.Add("Score", System.Type.GetType("System.String")).DefaultValue = ""
'            mdtGeneral.Columns.Add("IsGrp", System.Type.GetType("System.Boolean")).DefaultValue = False
'            mdtGeneral.Columns.Add("GrpId", System.Type.GetType("System.Int32")).DefaultValue = -1
'            mdtGeneral.Columns.Add("ItemId", System.Type.GetType("System.Int32")).DefaultValue = -1
'            mdtGeneral.Columns.Add("Id", System.Type.GetType("System.Int32")).DefaultValue = -1

'            Dim objAssessGrp As New clsassess_group_master
'            Dim objAssessItem As New clsassess_item_master

'            Dim dsGrp As New DataSet

'            dsGrp = objAssessGrp.getListForCombo("List")

'            Dim dGRow As DataRow = Nothing

'            For Each dRow As DataRow In dsGrp.Tables("List").Rows
'                dGRow = mdtGeneral.NewRow

'                dGRow.Item("Items") = dRow.Item("name")
'                dGRow.Item("IsGrp") = True
'                dGRow.Item("GrpId") = dRow.Item("assessgroupunkid")

'                mdtGeneral.Rows.Add(dGRow)

'                'S.SANDEEP [ 05 NOV 2014 ] -- START
'                'Dim dsItem As New DataSet
'                ''S.SANDEEP [ 28 DEC 2012 ] -- START
'                ''ENHANCEMENT : TRA CHANGES
'                ''dsItem = objAssessItem.getListForCombo("List", False, CInt(dRow.Item("assessgroupunkid")), True)
'                'dsItem = objAssessItem.getListForCombo("List", False, CInt(dRow.Item("assessgroupunkid")), True, CInt(cboPeriod.SelectedValue))
'                ''S.SANDEEP [ 28 DEC 2012 ] -- END

'                'For Each dIRow As DataRow In dsItem.Tables("List").Rows
'                '    dGRow = mdtGeneral.NewRow

'                '    dGRow.Item("Items") = Space(6) & dIRow.Item("name").ToString
'                '    dGRow.Item("IsGrp") = False
'                '    dGRow.Item("GrpId") = dRow.Item("assessgroupunkid")
'                '    dGRow.Item("ItemId") = dIRow.Item("assessitemunkid")

'                '    mdtGeneral.Rows.Add(dGRow)
'                'Next
'                Dim dtItem As DataTable
'                Dim objCompetenciesMaster As New clsassess_competencies_master
'                dtItem = objCompetenciesMaster.GetCompetenciesForShortlisting(CInt(dRow.Item("assessgroupunkid")), CInt(cboPeriod.SelectedValue))
'                For Each dIRow As DataRow In dtItem.Rows
'                    dGRow = mdtGeneral.NewRow
'                    dGRow.Item("Items") = Space(6) & dIRow.Item("Name").ToString
'                    dGRow.Item("IsGrp") = False
'                    dGRow.Item("GrpId") = dRow.Item("assessgroupunkid")
'                    dGRow.Item("ItemId") = dIRow.Item("Id")
'                    mdtGeneral.Rows.Add(dGRow)
'                Next
'                objCompetenciesMaster = Nothing
'                'S.SANDEEP [ 05 NOV 2014 ] -- END

'                dtItem.Dispose()
'            Next

'            dsGrp.Dispose()

'            objAssessGrp = Nothing
'            objAssessItem = Nothing

'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "Generate_Table", mstrModuleName)
'        Finally
'        End Try
'    End Sub

'    'S.SANDEEP [ 05 NOV 2014 ] -- START
'    Private Sub Generate_Perspective_Table()
'        Try
'            mdtPerspective = New DataTable("BSC")

'            mdtPerspective.Columns.Add("ischeck", System.Type.GetType("System.Boolean")).DefaultValue = 0
'            mdtPerspective.Columns.Add("Item", System.Type.GetType("System.String")).DefaultValue = ""
'            mdtPerspective.Columns.Add("Score", System.Type.GetType("System.String")).DefaultValue = ""
'            mdtPerspective.Columns.Add("PerId", System.Type.GetType("System.Int32")).DefaultValue = 0
'            mdtPerspective.Columns.Add("Id", System.Type.GetType("System.Int32")).DefaultValue = 0

'            Dim objPrMaster As New clsassess_perspective_master
'            Dim dsList As New DataSet
'            dsList = objPrMaster.getComboList("List", False)
'            For Each dRow As DataRow In dsList.Tables(0).Rows
'                Dim xRow As DataRow = mdtPerspective.NewRow()

'                xRow.Item("Item") = dRow.Item("Name")
'                xRow.Item("PerId") = dRow.Item("Id")

'                mdtPerspective.Rows.Add(xRow)
'            Next
'            objPrMaster = Nothing : dsList.Dispose()
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "Generate_Perspective_Table", mstrModuleName)
'        Finally
'        End Try
'    End Sub
'    'S.SANDEEP [ 05 NOV 2014 ] -- END

'    Private Sub SetVisibility()
'        Try
'            'If chkFinancial.Enabled = True Then
'            '    txtFTotalScore.Enabled = chkFinancial.Checked
'            '    cboFCondition.Enabled = chkFinancial.Checked
'            'End If

'            'If chkBusinessProcess.Enabled = True Then
'            '    txtBTotalScore.Enabled = chkBusinessProcess.Checked
'            '    cboBCondition.Enabled = chkBusinessProcess.Checked
'            'End If

'            'If chkCustomer.Enabled = True Then
'            '    txtCTotalScore.Enabled = chkCustomer.Checked
'            '    cboCCondition.Enabled = chkCustomer.Checked
'            'End If

'            'If chkOrgCapacity.Enabled = True Then
'            '    txtOTotalScore.Enabled = chkOrgCapacity.Checked
'            '    cboOCondition.Enabled = chkOrgCapacity.Checked
'            'End If

'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "SetVisibility", mstrModuleName)
'        Finally
'        End Try
'    End Sub

'    Private Function IsValidData() As Boolean
'        Dim blnFlag As Boolean = False
'        Try
'            'S.SANDEEP [ 05 MARCH 2012 ] -- START
'            'ENHANCEMENT : TRA CHANGES
'            If CInt(cboPeriod.SelectedValue) <= 0 Then
'                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 21, "Period is mandatory information. Please select Period to continue."), enMsgBoxStyle.Information)
'                cboPeriod.Focus()
'                Return False
'            End If
'            'S.SANDEEP [ 05 MARCH 2012 ] -- END


'            'S.SANDEEP [ 04 FEB 2012 ] -- START
'            'ENHANCEMENT : TRA CHANGES
'            'If chkSelf_Assess.Checked = True AndAlso chkAssessor_Assess.Checked = True AndAlso chkReviewer_Assess.Checked = True Then
'            '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 18, "Please select atleast one assessment mode to add filter."), enMsgBoxStyle.Information)
'            '    Return False
'            'End If

'            If chkSelf_Assess.Checked = False AndAlso chkAssessor_Assess.Checked = False AndAlso chkReviewer_Assess.Checked = False Then
'                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 18, "Please select atleast one assessment mode to add filter."), enMsgBoxStyle.Information)
'                Return False
'            End If
'            'S.SANDEEP [ 04 FEB 2012 ] -- END




'            If gbSummaryFilter.Checked Then
'                If txtBSCTotalScore.Text.Trim.Length <= 0 AndAlso txtGETotalScore.Text.Trim.Length <= 0 Then
'                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Sorry, you cannot add this filter as no value is provided to match."), enMsgBoxStyle.Information)
'                    txtBSCTotalScore.Focus()
'                    Return False
'                End If


'                If radSummaryAND.Checked Or radSummaryOR.Checked Then
'                    If txtBSCTotalScore.Text.Trim.Length > 0 AndAlso txtGETotalScore.Text.Trim.Length <= 0 Then
'                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Sorry, you cannot add this filter as General Evaluation Total Score value is not provided to match."), enMsgBoxStyle.Information)
'                        txtBSCTotalScore.Focus()
'                        Return False
'                    End If
'                    If txtBSCTotalScore.Text.Trim.Length <= 0 AndAlso txtGETotalScore.Text.Trim.Length > 0 Then
'                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Sorry, you cannot add this filter as Balance Score Card value is not provided to match."), enMsgBoxStyle.Information)
'                        txtBSCTotalScore.Focus()
'                        Return False
'                    End If
'                End If

'                If txtBSCTotalScore.Text.Trim.Length > 0 AndAlso txtGETotalScore.Text.Trim.Length > 0 Then
'                    If radSummaryAND.Checked = False AndAlso radSummaryOR.Checked = False Then
'                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Sorry, you cannot add this filter as no operation is set between BSC and General Evaluation."), enMsgBoxStyle.Information)
'                        radSummaryOR.Focus()
'                        Return False
'                    End If
'                End If
'            End If

'            If gbDetailFilter.Checked Then

'                'If chkFinancial.CheckState = CheckState.Checked Then
'                '    If txtFTotalScore.Text.Trim.Length <= 0 Then
'                '        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "Sorry you cannot add this filter as Financial total score is not provided."), enMsgBoxStyle.Information)
'                '        txtFTotalScore.Focus()
'                '        Return False
'                '    End If
'                '    blnFlag = True
'                '    iCount += 1
'                'End If

'                'If chkCustomer.CheckState = CheckState.Checked Then
'                '    If txtCTotalScore.Text.Trim.Length <= 0 Then
'                '        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 6, "Sorry you cannot add this filter as Customer total score is not provided."), enMsgBoxStyle.Information)
'                '        txtFTotalScore.Focus()
'                '        Return False
'                '    End If
'                '    blnFlag = True
'                '    iCount += 1
'                'End If

'                'If chkBusinessProcess.CheckState = CheckState.Checked Then
'                '    If txtBTotalScore.Text.Trim.Length <= 0 Then
'                '        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 7, "Sorry you cannot add this filter as Business Process total score is not provided."), enMsgBoxStyle.Information)
'                '        txtFTotalScore.Focus()
'                '        Return False
'                '    End If
'                '    blnFlag = True
'                '    iCount += 1
'                'End If

'                'If chkOrgCapacity.CheckState = CheckState.Checked Then
'                '    If txtOTotalScore.Text.Trim.Length <= 0 Then
'                '        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 8, "Sorry you cannot add this filter as Organization Capacity total score is not provided."), enMsgBoxStyle.Information)
'                '        txtFTotalScore.Focus()
'                '        Return False
'                '    End If
'                '    blnFlag = True
'                '    iCount += 1
'                'End If

'                Dim dTemp() As DataRow = mdtPerspective.Select("Id > '-1' AND Score <> ''")
'                If dTemp.Length > 0 Then
'                    iCount = dTemp.Length
'                    blnFlag = True
'                Else
'                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 10, "Sorry you cannot add this filter as BSC Perspective score is not provided."), enMsgBoxStyle.Information)
'                        Return False
'                    End If

'                If iCount > 1 Then
'                    If radPerspectiveAND.Checked = False AndAlso radPerspectiveOR.Checked = False Then
'                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 12, "Sorry you cannot add this filter as no operation set between prespective."), enMsgBoxStyle.Information)
'                        radPerspectiveOR.Focus()
'                        iCount = -1
'                        Return False
'                    End If
'                End If

'                If iCount = 1 Then
'                    If radPerspectiveAND.Checked = True Or radPerspectiveOR.Checked = True Then
'                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 13, "Sorry you cannot add this filter as operation cannot be performed on single perspective."), enMsgBoxStyle.Information)
'                        iCount = -1
'                        Return False
'                    End If
'                End If

'                dTemp = mdtGeneral.Select("Id > '-1' AND Score <> ''")

'                If radDetailAND.Checked Or radDetailOR.Checked Then
'                    If blnFlag = True AndAlso dTemp.Length <= 0 Then
'                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 9, "Sorry you cannot add this filter as General Evaluation Item score is not provided."), enMsgBoxStyle.Information)
'                        Return False
'                    End If

'                    If blnFlag = False AndAlso dTemp.Length >= 0 Then
'                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 10, "Sorry you cannot add this filter as BSC Perspective score is not provided."), enMsgBoxStyle.Information)
'                        Return False
'                    End If
'                End If

'                If blnFlag = True AndAlso dTemp.Length > 0 Then
'                    If radDetailAND.Checked = False AndAlso radDetailOR.Checked = False Then
'                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 11, "Sorry, you cannot add this filter as no operation is set between BSC Perspective and General Evaluation Items."), enMsgBoxStyle.Information)
'                        Return False
'                    End If
'                End If

'            End If


'            'S.SANDEEP [ 14 AUG 2013 ] -- START
'            'ENHANCEMENT : TRA CHANGES
'            If gbOverAllScore.Checked = True Then
'                If txtOverallScore.Decimal <= 0 Then
'                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 22, "Sorry, you cannot add this filter as Overall Score value is not provided to match."), enMsgBoxStyle.Information)
'                    txtOverallScore.Focus()
'                    Return False
'                End If
'            End If
'            'S.SANDEEP [ 14 AUG 2013 ] -- END

'            Return True
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "IsValidData", mstrModuleName)
'        Finally
'        End Try
'    End Function

'    Private Sub Set_TextValue()
'        Try
'            txtBSCTotalScore.Text = ""
'            'txtBTotalScore.Text = ""
'            'txtCTotalScore.Text = ""
'            'txtFTotalScore.Text = ""
'            txtGETotalScore.Text = ""
'            'txtOTotalScore.Text = ""
'            'S.SANDEEP [ 14 AUG 2013 ] -- START
'            'ENHANCEMENT : TRA CHANGES
'            txtOverallScore.Text = ""
'            'S.SANDEEP [ 14 AUG 2013 ] -- END
'            'S.SANDEEP [ 22 OCT 2013 ] -- START
'            cboAppointment.SelectedValue = 1
'            'S.SANDEEP [ 22 OCT 2013 ] -- END
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "Set_TextValue", mstrModuleName)
'        Finally
'        End Try
'    End Sub

'    Private Sub Fill_FilterList()
'        Try
'            lvFilterCriteria.Items.Clear()
'            Dim lvItem As ListViewItem

'            For Each dtRow As DataRow In mdtFilter.Rows
'                If CStr(IIf(IsDBNull(dtRow.Item("AUD")), "A", dtRow.Item("AUD"))) <> "D" Then
'                    lvItem = New ListViewItem

'                    lvItem.UseItemStyleForSubItems = False

'                    lvItem.Text = dtRow.Item("filter_criteria").ToString            'FILTER CRITERIA
'                    lvItem.SubItems.Add(dtRow.Item("filter_value").ToString)        'FILTER VALUE
'                    lvItem.SubItems.Add(dtRow.Item("contition").ToString)           'FILTER CONDITION
'                    lvItem.SubItems.Add(dtRow.Item("filter_refid").ToString)        'FILTER CRITERIA ID
'                    lvItem.SubItems.Add(dtRow.Item("conditionunkid").ToString)      'FILTER CONDITION ID
'                    lvItem.SubItems.Add(dtRow.Item("operator").ToString)            'OPERATOR ID
'                    lvItem.SubItems.Add(dtRow.Item("GUID").ToString)                'GUID
'                    lvItem.SubItems.Add(dtRow.Item("filter_grp").ToString)          'GROUP NAME
'                    lvItem.SubItems.Add(dtRow.Item("operation").ToString, Color.Blue, lvItem.BackColor, New Font(Me.Font, FontStyle.Bold))           'OPERATOR

'                    lvItem.Tag = dtRow.Item("filterunkid").ToString                 'FILTERUNKID

'                    lvFilterCriteria.Items.Add(lvItem)
'                End If
'            Next

'            If lvFilterCriteria.Items.Count > 2 Then
'                colhFieldName.Width = 285 - 20
'            Else
'                colhFieldName.Width = 285
'            End If

'            lvFilterCriteria.GroupingColumn = objcolhGrpName
'            lvFilterCriteria.DisplayGroups(True)

'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "Fill_FilterList", mstrModuleName)
'        Finally
'        End Try
'    End Sub

'    Private Sub Set_SummaryFilter()
'        Dim blnFlag As Boolean = False
'        Try
'            Dim dRow As DataRow = Nothing

'            Dim dBSC() As DataRow = mdtFilter.Select("filter_refid = '" & clsAppraisal_Filter.Filter_Refid.TOTAL_BSC_SCORE & "' AND AUD <> 'D'")
'            Dim dGE() As DataRow = mdtFilter.Select("filter_refid = '" & clsAppraisal_Filter.Filter_Refid.TOTAL_GE_SCORE & "' AND AUD <> 'D'")

'            If dBSC.Length <= 0 AndAlso txtBSCTotalScore.Text.Trim.Length > 0 Then
'                dRow = mdtFilter.NewRow

'                dRow.Item("filterunkid") = -1
'                dRow.Item("shortlistunkid") = -1
'                dRow.Item("filter_refid") = clsAppraisal_Filter.Filter_Refid.TOTAL_BSC_SCORE
'                dRow.Item("filter_value") = CDbl(txtBSCTotalScore.Text)
'                dRow.Item("conditionunkid") = cboBSCTotalCondition.SelectedValue
'                dRow.Item("referenceunkid") = -1
'                dRow.Item("issummary") = True

'                If blnFlag = False Then
'                    If radSummaryAND.Checked = True Then
'                        dRow.Item("operator") = clsAppraisal_Filter.Operater_ModeId.OP_AND
'                    ElseIf radSummaryOR.Checked = True Then
'                        dRow.Item("operator") = clsAppraisal_Filter.Operater_ModeId.OP_OR
'                    End If
'                End If

'                dRow.Item("isself_score") = chkSelf_Assess.Checked
'                dRow.Item("isassessor_score") = chkAssessor_Assess.Checked
'                dRow.Item("isreviewer_score") = chkReviewer_Assess.Checked
'                dRow.Item("AUD") = "A"
'                dRow.Item("GUID") = Guid.NewGuid.ToString
'                dRow.Item("filter_criteria") = elBalanceScorecard.Text
'                dRow.Item("col_tag") = elBalanceScorecard.Tag

'                Select Case CInt(cboBSCTotalCondition.SelectedValue)
'                    Case 0
'                        dRow.Item("contition") = "="
'                    Case Else
'                        dRow.Item("contition") = cboBSCTotalCondition.Text
'                End Select

'                If blnFlag = False Then
'                    If radSummaryAND.Checked = True Then
'                        dRow.Item("operation") = radSummaryAND.Text
'                    ElseIf radSummaryOR.Checked = True Then
'                        dRow.Item("operation") = radSummaryOR.Text
'                    End If
'                    blnFlag = True
'                End If

'                dRow.Item("filter_grp") = gbSummaryFilter.Text

'                mdtFilter.Rows.Add(dRow)

'                txtBSCTotalScore.Enabled = False : cboBSCTotalCondition.Enabled = False

'            ElseIf dBSC.Length > 0 Then
'                If radSummaryAND.Checked = True Then
'                    dBSC(0).Item("operation") = radSummaryAND.Text
'                    dBSC(0).Item("operator") = clsAppraisal_Filter.Operater_ModeId.OP_AND
'                ElseIf radSummaryOR.Checked = True Then
'                    dBSC(0).Item("operation") = radSummaryOR.Text
'                    dBSC(0).Item("operator") = clsAppraisal_Filter.Operater_ModeId.OP_OR
'                End If
'                blnFlag = True
'            End If

'            If dGE.Length <= 0 AndAlso txtGETotalScore.Text.Trim.Length > 0 Then
'                dRow = mdtFilter.NewRow

'                dRow.Item("filterunkid") = -1
'                dRow.Item("shortlistunkid") = -1
'                dRow.Item("filter_refid") = clsAppraisal_Filter.Filter_Refid.TOTAL_GE_SCORE
'                dRow.Item("filter_value") = CDbl(txtGETotalScore.Text)
'                dRow.Item("conditionunkid") = cboGETotalCondition.SelectedValue
'                dRow.Item("referenceunkid") = -1
'                dRow.Item("issummary") = True

'                If blnFlag = False Then
'                    If radSummaryAND.Checked = True Then
'                        dRow.Item("operator") = clsAppraisal_Filter.Operater_ModeId.OP_AND
'                    ElseIf radSummaryOR.Checked = True Then
'                        dRow.Item("operator") = clsAppraisal_Filter.Operater_ModeId.OP_OR
'                    End If
'                End If

'                dRow.Item("isself_score") = chkSelf_Assess.Checked
'                dRow.Item("isassessor_score") = chkAssessor_Assess.Checked
'                dRow.Item("isreviewer_score") = chkReviewer_Assess.Checked
'                dRow.Item("AUD") = "A"
'                dRow.Item("GUID") = Guid.NewGuid.ToString
'                dRow.Item("filter_criteria") = elGeneralEvaluation.Text
'                dRow.Item("col_tag") = elGeneralEvaluation.Tag

'                Select Case CInt(cboGETotalCondition.SelectedValue)
'                    Case 0
'                        dRow.Item("contition") = "="
'                    Case Else
'                        dRow.Item("contition") = cboGETotalCondition.Text
'                End Select

'                If blnFlag = False Then
'                    If radSummaryAND.Checked = True Then
'                        dRow.Item("operation") = radSummaryAND.Text
'                    ElseIf radSummaryOR.Checked = True Then
'                        dRow.Item("operation") = radSummaryOR.Text
'                    End If
'                End If

'                dRow.Item("filter_grp") = gbSummaryFilter.Text

'                mdtFilter.Rows.Add(dRow)

'                txtGETotalScore.Enabled = False : cboGETotalCondition.Enabled = False
'            End If


'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "", mstrModuleName)
'        Finally
'        End Try
'    End Sub

'    Private Sub Set_DetailFilter()
'        Dim blnFlag As Boolean = False
'        Try
'            Dim dRow As DataRow = Nothing

'            'Dim dFRow() As DataRow = mdtFilter.Select("filter_refid = '" & clsAppraisal_Filter.Filter_Refid.BSC_PERSPECTIVE & "' AND referenceunkid = '1' AND AUD <> 'D'")
'            'Dim dCRow() As DataRow = mdtFilter.Select("filter_refid = '" & clsAppraisal_Filter.Filter_Refid.BSC_PERSPECTIVE & "' AND referenceunkid = '2' AND AUD <> 'D'")
'            'Dim dBRow() As DataRow = mdtFilter.Select("filter_refid = '" & clsAppraisal_Filter.Filter_Refid.BSC_PERSPECTIVE & "' AND referenceunkid = '3' AND AUD <> 'D'")
'            'Dim dORow() As DataRow = mdtFilter.Select("filter_refid = '" & clsAppraisal_Filter.Filter_Refid.BSC_PERSPECTIVE & "' AND referenceunkid = '4' AND AUD <> 'D'")

'            'If chkFinancial.Checked = True Then
'            '    If dFRow.Length <= 0 AndAlso txtFTotalScore.Text.Trim.Length > 0 Then
'            '        dRow = mdtFilter.NewRow

'            '        dRow.Item("filterunkid") = -1
'            '        dRow.Item("shortlistunkid") = -1
'            '        dRow.Item("filter_refid") = clsAppraisal_Filter.Filter_Refid.BSC_PERSPECTIVE
'            '        dRow.Item("filter_value") = CDbl(txtFTotalScore.Text)
'            '        dRow.Item("conditionunkid") = cboFCondition.SelectedValue
'            '        dRow.Item("referenceunkid") = 1
'            '        dRow.Item("issummary") = False
'            '        If radPerspectiveAND.Checked = True Then
'            '            dRow.Item("operator") = clsAppraisal_Filter.Operater_ModeId.OP_AND
'            '        ElseIf radPerspectiveOR.Checked = True Then
'            '            dRow.Item("operator") = clsAppraisal_Filter.Operater_ModeId.OP_OR
'            '        End If
'            '        dRow.Item("isself_score") = chkSelf_Assess.Checked
'            '        dRow.Item("isassessor_score") = chkAssessor_Assess.Checked
'            '        dRow.Item("isreviewer_score") = chkReviewer_Assess.Checked
'            '        dRow.Item("AUD") = "A"
'            '        dRow.Item("GUID") = Guid.NewGuid.ToString
'            '        dRow.Item("filter_criteria") = chkFinancial.Text
'            '        dRow.Item("col_tag") = chkFinancial.Tag
'            '        If radPerspectiveAND.Checked = True Then
'            '            dRow.Item("operation") = radPerspectiveAND.Text
'            '        ElseIf radPerspectiveOR.Checked = True Then
'            '            dRow.Item("operation") = radPerspectiveOR.Text
'            '        End If
'            '        dRow.Item("filter_grp") = gbDetailFilter.Text
'            '        Select Case CInt(cboFCondition.SelectedValue)
'            '            Case 0
'            '                dRow.Item("contition") = "="
'            '            Case Else
'            '                dRow.Item("contition") = cboFCondition.Text
'            '        End Select
'            '        mdtFilter.Rows.Add(dRow)
'            '        chkFinancial.Enabled = False : txtFTotalScore.Enabled = False : cboFCondition.Enabled = False
'            '    End If
'            'End If

'            'If chkCustomer.Checked Then
'            '    If dCRow.Length <= 0 AndAlso txtCTotalScore.Text.Trim.Length > 0 Then
'            '        dRow = mdtFilter.NewRow

'            '        dRow.Item("filterunkid") = -1
'            '        dRow.Item("shortlistunkid") = -1
'            '        dRow.Item("filter_refid") = clsAppraisal_Filter.Filter_Refid.BSC_PERSPECTIVE
'            '        dRow.Item("filter_value") = CDbl(txtCTotalScore.Text)
'            '        dRow.Item("conditionunkid") = cboCCondition.SelectedValue
'            '        dRow.Item("referenceunkid") = 2
'            '        dRow.Item("issummary") = False
'            '        If radPerspectiveAND.Checked = True Then
'            '            dRow.Item("operator") = clsAppraisal_Filter.Operater_ModeId.OP_AND
'            '        ElseIf radPerspectiveOR.Checked = True Then
'            '            dRow.Item("operator") = clsAppraisal_Filter.Operater_ModeId.OP_OR
'            '        End If
'            '        dRow.Item("isself_score") = chkSelf_Assess.Checked
'            '        dRow.Item("isassessor_score") = chkAssessor_Assess.Checked
'            '        dRow.Item("isreviewer_score") = chkReviewer_Assess.Checked
'            '        dRow.Item("AUD") = "A"
'            '        dRow.Item("GUID") = Guid.NewGuid.ToString
'            '        dRow.Item("filter_criteria") = chkCustomer.Text
'            '        dRow.Item("col_tag") = chkCustomer.Tag
'            '        If radPerspectiveAND.Checked = True Then
'            '            dRow.Item("operation") = radPerspectiveAND.Text
'            '        ElseIf radPerspectiveOR.Checked = True Then
'            '            dRow.Item("operation") = radPerspectiveOR.Text
'            '        End If
'            '        dRow.Item("filter_grp") = gbDetailFilter.Text
'            '        Select Case CInt(cboCCondition.SelectedValue)
'            '            Case 0
'            '                dRow.Item("contition") = "="
'            '            Case Else
'            '                dRow.Item("contition") = cboCCondition.Text
'            '        End Select
'            '        mdtFilter.Rows.Add(dRow)
'            '        chkCustomer.Enabled = False : txtCTotalScore.Enabled = False : cboCCondition.Enabled = False
'            '    End If
'            'End If

'            'If chkBusinessProcess.Checked Then
'            '    If dBRow.Length <= 0 AndAlso txtBTotalScore.Text.Trim.Length > 0 Then
'            '        dRow = mdtFilter.NewRow

'            '        dRow.Item("filterunkid") = -1
'            '        dRow.Item("shortlistunkid") = -1
'            '        dRow.Item("filter_refid") = clsAppraisal_Filter.Filter_Refid.BSC_PERSPECTIVE
'            '        dRow.Item("filter_value") = CDbl(txtBTotalScore.Text)
'            '        dRow.Item("conditionunkid") = cboBCondition.SelectedValue
'            '        dRow.Item("referenceunkid") = 3
'            '        dRow.Item("issummary") = False
'            '        If radPerspectiveAND.Checked = True Then
'            '            dRow.Item("operator") = clsAppraisal_Filter.Operater_ModeId.OP_AND
'            '        ElseIf radPerspectiveOR.Checked = True Then
'            '            dRow.Item("operator") = clsAppraisal_Filter.Operater_ModeId.OP_OR
'            '        End If
'            '        dRow.Item("isself_score") = chkSelf_Assess.Checked
'            '        dRow.Item("isassessor_score") = chkAssessor_Assess.Checked
'            '        dRow.Item("isreviewer_score") = chkReviewer_Assess.Checked
'            '        dRow.Item("AUD") = "A"
'            '        dRow.Item("GUID") = Guid.NewGuid.ToString
'            '        dRow.Item("filter_criteria") = chkBusinessProcess.Text
'            '        dRow.Item("col_tag") = chkBusinessProcess.Tag
'            '        If radPerspectiveAND.Checked = True Then
'            '            dRow.Item("operation") = radPerspectiveAND.Text
'            '        ElseIf radPerspectiveOR.Checked = True Then
'            '            dRow.Item("operation") = radPerspectiveOR.Text
'            '        End If
'            '        dRow.Item("filter_grp") = gbDetailFilter.Text
'            '        Select Case CInt(cboBCondition.SelectedValue)
'            '            Case 0
'            '                dRow.Item("contition") = "="
'            '            Case Else
'            '                dRow.Item("contition") = cboBCondition.Text
'            '        End Select
'            '        mdtFilter.Rows.Add(dRow)
'            '        chkBusinessProcess.Enabled = False : txtBTotalScore.Enabled = False : cboBCondition.Enabled = False
'            '    End If
'            'End If

'            'If chkOrgCapacity.Checked Then
'            '    If dORow.Length <= 0 AndAlso txtOTotalScore.Text.Trim.Length > 0 Then
'            '        dRow = mdtFilter.NewRow

'            '        dRow.Item("filterunkid") = -1
'            '        dRow.Item("shortlistunkid") = -1
'            '        dRow.Item("filter_refid") = clsAppraisal_Filter.Filter_Refid.BSC_PERSPECTIVE
'            '        dRow.Item("filter_value") = CDbl(txtOTotalScore.Text)
'            '        dRow.Item("conditionunkid") = cboOCondition.SelectedValue
'            '        dRow.Item("referenceunkid") = 4
'            '        dRow.Item("issummary") = False
'            '        If radPerspectiveAND.Checked = True Then
'            '            dRow.Item("operator") = clsAppraisal_Filter.Operater_ModeId.OP_AND
'            '        ElseIf radPerspectiveOR.Checked = True Then
'            '            dRow.Item("operator") = clsAppraisal_Filter.Operater_ModeId.OP_OR
'            '        End If
'            '        dRow.Item("isself_score") = chkSelf_Assess.Checked
'            '        dRow.Item("isassessor_score") = chkAssessor_Assess.Checked
'            '        dRow.Item("isreviewer_score") = chkReviewer_Assess.Checked
'            '        dRow.Item("AUD") = "A"
'            '        dRow.Item("GUID") = Guid.NewGuid.ToString
'            '        dRow.Item("filter_criteria") = chkOrgCapacity.Text
'            '        dRow.Item("col_tag") = chkOrgCapacity.Tag
'            '        If radPerspectiveAND.Checked = True Then
'            '            dRow.Item("operation") = radPerspectiveAND.Text
'            '        ElseIf radPerspectiveOR.Checked = True Then
'            '            dRow.Item("operation") = radPerspectiveOR.Text
'            '        End If
'            '        dRow.Item("filter_grp") = gbDetailFilter.Text
'            '        Select Case CInt(cboOCondition.SelectedValue)
'            '            Case 0
'            '                dRow.Item("contition") = "="
'            '            Case Else
'            '                dRow.Item("contition") = cboOCondition.Text
'            '        End Select
'            '        mdtFilter.Rows.Add(dRow)
'            '        chkOrgCapacity.Enabled = False : txtOTotalScore.Enabled = False : cboOCondition.Enabled = False
'            '    End If
'            'End If
'            Dim dTemp() As DataRow = mdtPerspective.Select("Id > '-1' AND Score <> ''")

'            If dTemp.Length > 0 Then
'                Dim dItem() As DataRow = Nothing
'                For i As Integer = 0 To dTemp.Length - 1
'                    dItem = mdtFilter.Select("filter_refid = '" & clsAppraisal_Filter.Filter_Refid.BSC_PERSPECTIVE & "' AND referenceunkid = '" & CInt(dTemp(i)("PerId")) & "' AND AUD <> 'D'")
'                    If dItem.Length <= 0 Then
'                    dRow = mdtFilter.NewRow

'                    dRow.Item("filterunkid") = -1
'                    dRow.Item("shortlistunkid") = -1
'                    dRow.Item("filter_refid") = clsAppraisal_Filter.Filter_Refid.BSC_PERSPECTIVE
'                        dRow.Item("filter_value") = CDbl(dTemp(i)("Score"))
'                        dRow.Item("conditionunkid") = CDbl(dTemp(i)("Id"))
'                        dRow.Item("referenceunkid") = CInt(dTemp(i)("PerId"))
'                    dRow.Item("issummary") = False
'                    If radPerspectiveAND.Checked = True Then
'                        dRow.Item("operator") = clsAppraisal_Filter.Operater_ModeId.OP_AND
'                    ElseIf radPerspectiveOR.Checked = True Then
'                        dRow.Item("operator") = clsAppraisal_Filter.Operater_ModeId.OP_OR
'                    End If
'                    dRow.Item("isself_score") = chkSelf_Assess.Checked
'                    dRow.Item("isassessor_score") = chkAssessor_Assess.Checked
'                    dRow.Item("isreviewer_score") = chkReviewer_Assess.Checked
'                    dRow.Item("AUD") = "A"
'                    dRow.Item("GUID") = Guid.NewGuid.ToString
'                        dRow.Item("filter_criteria") = Trim(CStr(dTemp(i)("Item")))
'                        dRow.Item("col_tag") = CStr(dTemp(i)("PerId"))
'                    If radPerspectiveAND.Checked = True Then
'                        dRow.Item("operation") = radPerspectiveAND.Text
'                    ElseIf radPerspectiveOR.Checked = True Then
'                        dRow.Item("operation") = radPerspectiveOR.Text
'                    End If
'                    dRow.Item("filter_grp") = gbDetailFilter.Text
'                        Select Case CInt(CDbl(dTemp(i)("Id")))
'                        Case 0
'                            dRow.Item("contition") = "="
'                            Case 1
'                                dRow.Item("contition") = Language.getMessage("clsMasterData", 408, ">")
'                            Case 2
'                                dRow.Item("contition") = Language.getMessage("clsMasterData", 310, "<")
'                            Case 3
'                                dRow.Item("contition") = Language.getMessage("clsMasterData", 311, ">=")
'                            Case 4
'                                dRow.Item("contition") = Language.getMessage("clsMasterData", 312, "<=")
'                    End Select
'                    mdtFilter.Rows.Add(dRow)
'                End If
'                Next
'            End If

'            dTemp = mdtGeneral.Select("Id > '-1' AND Score <> ''")

'            If dTemp.Length > 0 Then
'                Dim dItem() As DataRow = Nothing


'                For i As Integer = 0 To dTemp.Length - 1
'                    dItem = mdtFilter.Select("filter_refid = '" & clsAppraisal_Filter.Filter_Refid.GENEAL_EVALUATION & "' AND referenceunkid = '" & CInt(dTemp(i)("ItemId")) & "' AND AUD <> 'D'")
'                    If dItem.Length <= 0 Then
'                        dRow = mdtFilter.NewRow

'                        dRow.Item("filterunkid") = -1
'                        dRow.Item("shortlistunkid") = -1
'                        dRow.Item("filter_refid") = clsAppraisal_Filter.Filter_Refid.GENEAL_EVALUATION
'                        dRow.Item("filter_value") = CDbl(dTemp(i)("Score"))
'                        dRow.Item("conditionunkid") = CDbl(dTemp(i)("Id"))
'                        dRow.Item("referenceunkid") = CInt(dTemp(i)("ItemId"))
'                        dRow.Item("issummary") = False
'                        dRow.Item("operator") = clsAppraisal_Filter.Operater_ModeId.OP_AND
'                        dRow.Item("isself_score") = chkSelf_Assess.Checked
'                        dRow.Item("isassessor_score") = chkAssessor_Assess.Checked
'                        dRow.Item("isreviewer_score") = chkReviewer_Assess.Checked
'                        dRow.Item("AUD") = "A"
'                        dRow.Item("GUID") = Guid.NewGuid.ToString
'                        dRow.Item("filter_criteria") = Trim(CStr(dTemp(i)("Items")))
'                        dRow.Item("col_tag") = CStr(dTemp(i)("ItemId"))
'                        dRow.Item("operation") = radSummaryAND.Text
'                        dRow.Item("filter_grp") = gbDetailFilter.Text
'                        Select Case CInt(CDbl(dTemp(i)("Id")))
'                            Case 0
'                                dRow.Item("contition") = "="
'                            Case 1
'                                dRow.Item("contition") = Language.getMessage("clsMasterData", 408, ">")
'                            Case 2
'                                dRow.Item("contition") = Language.getMessage("clsMasterData", 310, "<")
'                            Case 3
'                                dRow.Item("contition") = Language.getMessage("clsMasterData", 311, ">=")
'                            Case 4
'                                dRow.Item("contition") = Language.getMessage("clsMasterData", 312, "<=")
'                        End Select
'                        mdtFilter.Rows.Add(dRow)
'                    End If
'                Next
'            End If

'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "Set_DetailFilter", mstrModuleName)
'        Finally
'        End Try
'    End Sub

'    'Private Function Generate_SummaryFilter_String() As String
'    '    Dim StrFilterString As String = String.Empty
'    '    Try
'    '        Dim dtTemp() As DataRow = Nothing
'    '        dtTemp = mdtFilter.Select("issummary = true AND AUD <>'D'")
'    '        If dtTemp.Length > 0 Then
'    '            For i As Integer = 0 To dtTemp.Length - 1
'    '                Select Case i
'    '                    Case 0
'    '                        StrFilterString &= " AND (" & dtTemp(i)("col_tag").ToString & " " & dtTemp(i)("contition").ToString & " '" & dtTemp(i)("filter_value").ToString & "' " & IIf(CInt(dtTemp(i)("operator")) <= 0, "", dtTemp(i)("operation")).ToString
'    '                    Case Else
'    '                        StrFilterString &= " " & dtTemp(i)("col_tag").ToString & " " & dtTemp(i)("contition").ToString & " '" & dtTemp(i)("filter_value").ToString & "' " & IIf(CInt(dtTemp(i)("operator")) <= 0, "", dtTemp(i)("operation")).ToString & ") "
'    '                End Select
'    '            Next
'    '        End If

'    '        If StrFilterString.Trim.Length > 0 Then
'    '            If chkSelf_Assess.Checked = True AndAlso chkAssessor_Assess.Checked = True AndAlso chkReviewer_Assess.Checked = True Then
'    '                StrFilterString &= " AND assessmodeid IN(" & enAssessmentMode.SELF_ASSESSMENT & "," & enAssessmentMode.APPRAISER_ASSESSMENT & "," & enAssessmentMode.REVIEWER_ASSESSMENT & ")"
'    '            ElseIf chkSelf_Assess.Checked = True AndAlso chkAssessor_Assess.Checked = True AndAlso chkReviewer_Assess.Checked = False Then
'    '                StrFilterString &= " AND assessmodeid IN(" & enAssessmentMode.SELF_ASSESSMENT & "," & enAssessmentMode.APPRAISER_ASSESSMENT & ")"
'    '            ElseIf chkSelf_Assess.Checked = True AndAlso chkAssessor_Assess.Checked = False AndAlso chkReviewer_Assess.Checked = True Then
'    '                StrFilterString &= " AND assessmodeid IN(" & enAssessmentMode.SELF_ASSESSMENT & "," & enAssessmentMode.REVIEWER_ASSESSMENT & ")"
'    '            ElseIf chkSelf_Assess.Checked = False AndAlso chkAssessor_Assess.Checked = True AndAlso chkReviewer_Assess.Checked = True Then
'    '                StrFilterString &= " AND assessmodeid IN(" & enAssessmentMode.APPRAISER_ASSESSMENT & "," & enAssessmentMode.REVIEWER_ASSESSMENT & ")"
'    '            ElseIf chkSelf_Assess.Checked = True AndAlso chkAssessor_Assess.Checked = False AndAlso chkReviewer_Assess.Checked = False Then
'    '                StrFilterString &= " AND assessmodeid IN(" & enAssessmentMode.SELF_ASSESSMENT & ")"
'    '            ElseIf chkSelf_Assess.Checked = False AndAlso chkAssessor_Assess.Checked = True AndAlso chkReviewer_Assess.Checked = False Then
'    '                StrFilterString &= " AND assessmodeid IN(" & enAssessmentMode.APPRAISER_ASSESSMENT & ")"
'    '            ElseIf chkSelf_Assess.Checked = False AndAlso chkAssessor_Assess.Checked = False AndAlso chkReviewer_Assess.Checked = False Then
'    '                StrFilterString &= " AND assessmodeid IN(" & enAssessmentMode.REVIEWER_ASSESSMENT & ")"
'    '            End If
'    '        End If

'    '        Return StrFilterString
'    '    Catch ex As Exception
'    '        DisplayError.Show("-1", ex.Message, "Generate_DetailFilter_String", mstrModuleName)
'    '        Return StrFilterString
'    '    Finally
'    '    End Try
'    'End Function

'    'Private Function Generate_DetailFilter_String() As String
'    '    Dim StrDetailString As String = String.Empty
'    '    Try

'    '        Dim dtTemp() As DataRow = Nothing

'    '        dtTemp = mdtFilter.Select("issummary = false AND AUD <> 'D' AND filter_refid = '" & clsAppraisal_Filter.Filter_Refid.BSC_PERSPECTIVE & "'")

'    '        Dim iCnt As Integer = dtTemp.Length - 1
'    '        Dim StrCon As String = String.Empty

'    '        If dtTemp.Length > 0 Then
'    '            For i As Integer = 0 To dtTemp.Length - 1
'    '                Select Case i
'    '                    Case 0
'    '                        StrDetailString &= " OR (" & dtTemp(i)("col_tag").ToString & " " & dtTemp(i)("contition").ToString & " '" & dtTemp(i)("filter_value").ToString & "' " & IIf(CInt(dtTemp(i)("operator")) <= 0, "", dtTemp(i)("operation")).ToString
'    '                    Case Else
'    '                        If i <> iCnt Then
'    '                            StrDetailString &= " " & dtTemp(i)("col_tag").ToString & " " & dtTemp(i)("contition").ToString & " '" & dtTemp(i)("filter_value").ToString & "' " & IIf(CInt(dtTemp(i)("operator")) <= 0, "", dtTemp(i)("operation")).ToString & " "
'    '                        Else
'    '                            StrDetailString &= " " & dtTemp(i)("col_tag").ToString & " " & dtTemp(i)("contition").ToString & " '" & dtTemp(i)("filter_value").ToString & "')"
'    '                            StrCon = dtTemp(i)("operation").ToString
'    '                        End If
'    '                End Select
'    '            Next
'    '        End If

'    '        dtTemp = mdtFilter.Select("issummary = false AND AUD <> 'D' AND filter_refid = '" & clsAppraisal_Filter.Filter_Refid.GENEAL_EVALUATION & "'")

'    '        If dtTemp.Length > 0 Then
'    '            For i As Integer = 0 To dtTemp.Length - 1
'    '                Select Case i
'    '                    Case 0
'    '                        StrDetailString &= " " & StrCon & " (" & dtTemp(i)("col_tag").ToString & " " & dtTemp(i)("contition").ToString & " '" & dtTemp(i)("filter_value").ToString & "' " & IIf(CInt(dtTemp(i)("operator")) <= 0, "", dtTemp(i)("operation")).ToString
'    '                    Case Else
'    '                        StrDetailString &= " " & dtTemp(i)("col_tag").ToString & " " & dtTemp(i)("contition").ToString & " '" & dtTemp(i)("filter_value").ToString & "' "
'    '                End Select
'    '            Next
'    '            StrDetailString &= ")"
'    '        End If

'    '        If StrDetailString.Trim.Length > 0 Then
'    '            If chkSelf_Assess.Checked = True AndAlso chkAssessor_Assess.Checked = True AndAlso chkReviewer_Assess.Checked = True Then
'    '                StrDetailString &= " AND assessmodeid IN (" & enAssessmentMode.SELF_ASSESSMENT & "," & enAssessmentMode.APPRAISER_ASSESSMENT & "," & enAssessmentMode.REVIEWER_ASSESSMENT & ")"
'    '            ElseIf chkSelf_Assess.Checked = True AndAlso chkAssessor_Assess.Checked = True AndAlso chkReviewer_Assess.Checked = False Then
'    '                StrDetailString &= " AND assessmodeid IN (" & enAssessmentMode.SELF_ASSESSMENT & "," & enAssessmentMode.APPRAISER_ASSESSMENT & ")"
'    '            ElseIf chkSelf_Assess.Checked = True AndAlso chkAssessor_Assess.Checked = False AndAlso chkReviewer_Assess.Checked = True Then
'    '                StrDetailString &= " AND assessmodeid IN (" & enAssessmentMode.SELF_ASSESSMENT & "," & enAssessmentMode.REVIEWER_ASSESSMENT & ")"
'    '            ElseIf chkSelf_Assess.Checked = False AndAlso chkAssessor_Assess.Checked = True AndAlso chkReviewer_Assess.Checked = True Then
'    '                StrDetailString &= " AND assessmodeid IN (" & enAssessmentMode.APPRAISER_ASSESSMENT & "," & enAssessmentMode.REVIEWER_ASSESSMENT & ")"
'    '            ElseIf chkSelf_Assess.Checked = True AndAlso chkAssessor_Assess.Checked = False AndAlso chkReviewer_Assess.Checked = False Then
'    '                StrDetailString &= " AND assessmodeid IN (" & enAssessmentMode.SELF_ASSESSMENT & ")"
'    '            ElseIf chkSelf_Assess.Checked = False AndAlso chkAssessor_Assess.Checked = True AndAlso chkReviewer_Assess.Checked = False Then
'    '                StrDetailString &= " AND assessmodeid IN (" & enAssessmentMode.APPRAISER_ASSESSMENT & ")"
'    '            ElseIf chkSelf_Assess.Checked = False AndAlso chkAssessor_Assess.Checked = False AndAlso chkReviewer_Assess.Checked = False Then
'    '                StrDetailString &= " AND assessmodeid IN (" & enAssessmentMode.REVIEWER_ASSESSMENT & ")"
'    '            End If
'    '        End If


'    '        Return StrDetailString
'    '    Catch ex As Exception
'    '        DisplayError.Show("-1", ex.Message, "Generate_DetailFilter_String", mstrModuleName)
'    '        Return StrDetailString
'    '    Finally
'    '    End Try
'    'End Function

'    Private Function Export_to_Excel(ByVal BlnIsSave As Boolean, ByVal flFileName As String, ByVal SavePath As String, ByVal objDataReader As System.Data.DataTable, ByVal objDataFilter As DataTable) As Boolean
'        Dim strBuilder As New StringBuilder
'        Dim blnFlag As Boolean = False
'        Try

'            strBuilder.Append(" <TABLE BORDER=0 WIDTH=140%> " & vbCrLf)
'            strBuilder.Append(" <TR ALIGN = CENTER VALIGN=TOP> " & vbCrLf)
'            strBuilder.Append(" <TD BORDER=1 COLSPAN ='" & objDataReader.Columns.Count & "' ALIGN='MIDDLE'><FONT SIZE=3><B>" & Language.getMessage(mstrModuleName, 16, "Employee Short Listing") & "</B></FONT></TD>" & vbCrLf)
'            strBuilder.Append(" </TR> " & vbCrLf)
'            strBuilder.Append(" </TABLE> " & vbCrLf)
'            strBuilder.Append(" <BR> " & vbCrLf)

'            If BlnIsSave Then
'                strBuilder.Append(" <TABLE BORDER=0 WIDTH=140%> " & vbCrLf)
'                strBuilder.Append(" <TR> " & vbCrLf)
'                strBuilder.Append(" <TD WIDTH='10%'> " & vbCrLf)
'                strBuilder.Append(" <FONT SIZE=3><B>")
'                strBuilder.Append(" Reference No : " & objMShortList._Referenceno.ToString & vbCrLf)
'                strBuilder.Append(" </FONT></B>")
'                strBuilder.Append(" </TD> " & vbCrLf)
'                strBuilder.Append(" </TR> " & vbCrLf)
'                strBuilder.Append(" </TABLE> " & vbCrLf)
'                strBuilder.Append(" <BR> " & vbCrLf)
'            End If

'            'S.SANDEEP [ 22 OCT 2013 ] -- START
'            Select Case CInt(cboAppointment.SelectedValue)
'                Case enAD_Report_Parameter.APP_DATE_FROM
'                    If dtpDate1.Checked = True AndAlso dtpDate2.Checked = True Then
'                        strBuilder.Append(" <TABLE BORDER=0 WIDTH=140%> " & vbCrLf)
'                        strBuilder.Append(" <TR> " & vbCrLf)
'                        strBuilder.Append(" <TD WIDTH='10%'> " & vbCrLf)
'                        strBuilder.Append(" <FONT SIZE=3><B>")
'                        strBuilder.Append(" Appointed Date " & cboAppointment.Text & " :  " & dtpDate1.Value.Date & " " & " To : " & dtpDate2.Value.Date & " " & vbCrLf)
'                        strBuilder.Append(" </FONT></B>")
'                        strBuilder.Append(" </TD> " & vbCrLf)
'                        strBuilder.Append(" </TR> " & vbCrLf)
'                        strBuilder.Append(" </TABLE> " & vbCrLf)
'                        strBuilder.Append(" <BR> " & vbCrLf)
'                    End If
'                Case enAD_Report_Parameter.APP_DATE_BEFORE
'                    If dtpDate1.Checked = True Then
'                        strBuilder.Append(" <TABLE BORDER=0 WIDTH=140%> " & vbCrLf)
'                        strBuilder.Append(" <TR> " & vbCrLf)
'                        strBuilder.Append(" <TD WIDTH='10%'> " & vbCrLf)
'                        strBuilder.Append(" <FONT SIZE=3><B>")
'                        strBuilder.Append(" Appointed Date " & cboAppointment.Text & " :  " & dtpDate1.Value.Date & " " & vbCrLf)
'                        strBuilder.Append(" </FONT></B>")
'                        strBuilder.Append(" </TD> " & vbCrLf)
'                        strBuilder.Append(" </TR> " & vbCrLf)
'                        strBuilder.Append(" </TABLE> " & vbCrLf)
'                        strBuilder.Append(" <BR> " & vbCrLf)
'                    End If
'                Case enAD_Report_Parameter.APP_DATE_AFTER
'                    If dtpDate1.Checked = True Then
'                        strBuilder.Append(" <TABLE BORDER=0 WIDTH=140%> " & vbCrLf)
'                        strBuilder.Append(" <TR> " & vbCrLf)
'                        strBuilder.Append(" <TD WIDTH='10%'> " & vbCrLf)
'                        strBuilder.Append(" <FONT SIZE=3><B>")
'                        strBuilder.Append(" Appointed Date " & cboAppointment.Text & "  :  " & dtpDate1.Value.Date & " " & vbCrLf)
'                        strBuilder.Append(" </FONT></B>")
'                        strBuilder.Append(" </TD> " & vbCrLf)
'                        strBuilder.Append(" </TR> " & vbCrLf)
'                        strBuilder.Append(" </TABLE> " & vbCrLf)
'                        strBuilder.Append(" <BR> " & vbCrLf)
'                    End If
'            End Select
'            'S.SANDEEP [ 22 OCT 2013 ] -- END

'            strBuilder.Append(" <TABLE BORDER=1 BORDERCOLOR =BLACK CELLSPACING =0 CELLPADDING =3 WIDTH=140%> " & vbCrLf)
'            strBuilder.Append(" <TR ALIGN = CENTER VALIGN=TOP> " & vbCrLf)
'            strBuilder.Append(" <TD COLSPAN ='4' ALIGN='LEFT'><FONT SIZE=2><B>" & Language.getMessage(mstrModuleName, 17, "Employee(s): ") & "</B></FONT></TD>" & vbCrLf)
'            strBuilder.Append(" </TR> " & vbCrLf)

'            strBuilder.Append(" <TR ALIGN = CENTER VALIGN=TOP> " & vbCrLf)
'            For j As Integer = 0 To objDataReader.Columns.Count - 2
'                If objDataReader.Columns(j).Caption = "" Then Continue For
'                strBuilder.Append("<TD BORDER=1 WIDTH='60%' ALIGN='MIDDLE'><FONT SIZE=2><B>" & objDataReader.Columns(j).Caption & "</B></FONT></TD>" & vbCrLf)
'            Next
'            strBuilder.Append(" </TR> " & vbCrLf)

'            'S.SANDEEP [ 05 MARCH 2012 ] -- START
'            'ENHANCEMENT : TRA CHANGES
'            'Dim intEmpId As Integer = 0
'            Dim mDicEmp As New Dictionary(Of Integer, Integer)
'            'S.SANDEEP [ 05 MARCH 2012 ] -- END




'            For i As Integer = 0 To objDataReader.Rows.Count - 1
'                'S.SANDEEP [ 05 MARCH 2012 ] -- START
'                'ENHANCEMENT : TRA CHANGES
'                'If intEmpId <> CInt(objDataReader.Rows(i)("employeeunkid")) Then
'                '    intEmpId = CInt(objDataReader.Rows(i)("employeeunkid"))
'                'Else
'                '    Continue For
'                'End If
'                If mDicEmp.ContainsKey(CInt(objDataReader.Rows(i)("employeeunkid"))) Then Continue For
'                mDicEmp.Add(CInt(objDataReader.Rows(i)("employeeunkid")), CInt(objDataReader.Rows(i)("employeeunkid")))
'                'S.SANDEEP [ 05 MARCH 2012 ] -- END


'                strBuilder.Append(" <TR> " & vbCrLf)
'                For k As Integer = 0 To objDataReader.Columns.Count - 2
'                    If objDataReader.Columns(k).Caption = "" Then Continue For

'                    If objDataReader.Columns(k).DataType Is Type.GetType("System.DateTime") Then
'                        If objDataReader.Rows(i)(k).ToString() <> "" Then
'                            strBuilder.Append("<TD BORDER=1 BORDERCOLOR=BLACK ALIGN='LEFT'><FONT SIZE=2>" & CDate(objDataReader.Rows(i)(k)).ToShortDateString() & "</FONT></TD>" & vbCrLf)
'                        Else
'                            strBuilder.Append("<TD BORDER=1 BORDERCOLOR=BLACK ALIGN='LEFT'><FONT SIZE=2> </FONT></TD>" & vbCrLf)
'                        End If
'                    Else
'                        strBuilder.Append("<TD BORDER=1 BORDERCOLOR=BLACK ALIGN='LEFT'><FONT SIZE=2>" & objDataReader.Rows(i)(k).ToString() & "</FONT></TD>" & vbCrLf)
'                    End If
'                Next
'                strBuilder.Append(" </TR> " & vbCrLf)
'            Next
'            strBuilder.Append(" </TABLE> " & vbCrLf)
'            strBuilder.Append(" <BR> " & vbCrLf)

'            strBuilder.Append(" <TABLE BORDER=1 BORDERCOLOR =BLACK CELLSPACING =0 CELLPADDING =3 WIDTH=140%> " & vbCrLf)
'            strBuilder.Append(" <TR ALIGN = CENTER VALIGN=TOP> " & vbCrLf)
'            strBuilder.Append(" <TD COLSPAN ='4' ALIGN='LEFT'><FONT SIZE=2><B>" & Language.getMessage(mstrModuleName, 20, "Filters(s): ") & "</B></FONT></TD>" & vbCrLf)
'            strBuilder.Append(" </TR> " & vbCrLf)

'            strBuilder.Append(" <TR> " & vbCrLf)
'            For j As Integer = 0 To objDataFilter.Columns.Count - 1
'                If objDataFilter.Columns(j).Caption = "" Then Continue For
'                strBuilder.Append("<TD BORDER=1 WIDTH='60%' ALIGN='MIDDLE'><FONT SIZE=2><B>" & objDataFilter.Columns(j).Caption & "</B></FONT></TD>" & vbCrLf)
'            Next
'            strBuilder.Append(" </TR> " & vbCrLf)


'            For i As Integer = 0 To objDataFilter.Rows.Count - 1

'                If objDataFilter.Rows(i)("AUD").ToString = "D" Then Continue For

'                strBuilder.Append(" <TR> " & vbCrLf)
'                For k As Integer = 0 To objDataFilter.Columns.Count - 2
'                    If objDataFilter.Columns(k).Caption = "" Then Continue For
'                    strBuilder.Append("<TD BORDER=1 BORDERCOLOR=BLACK ALIGN='LEFT'><FONT SIZE=2>" & objDataFilter.Rows(i)(k).ToString() & "</FONT></TD>" & vbCrLf)
'                Next
'            Next


'            strBuilder.Append(" </TABLE> " & vbCrLf)

'            strBuilder.Append(" </HTML> " & vbCrLf)

'            If SavePath.LastIndexOf("\") = SavePath.Length - 1 Then
'                SavePath = SavePath.Remove(SavePath.LastIndexOf("\"))
'            End If

'            blnFlag = SaveExcelfile(SavePath & "\" & flFileName, strBuilder)

'            Return blnFlag

'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "Export_to_Excel", mstrModuleName)
'        Finally
'        End Try
'    End Function

'    Private Function SaveExcelfile(ByVal fpath As String, ByVal sb As System.Text.StringBuilder) As Boolean
'        Dim fsFile As New FileStream(fpath, FileMode.Create, FileAccess.Write)
'        Dim strWriter As New StreamWriter(fsFile)
'        Try
'            With strWriter
'                .BaseStream.Seek(0, SeekOrigin.End)
'                .WriteLine(sb)
'                .Close()
'            End With
'            Return True
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "SaveExcelfile", mstrModuleName)
'            Return False
'        Finally
'            sb = Nothing
'            strWriter = Nothing
'            fsFile = Nothing
'        End Try
'    End Function


'    'S.SANDEEP [ 14 AUG 2013 ] -- START
'    'ENHANCEMENT : TRA CHANGES
'    Private Sub Set_OverallFilter()
'        Dim blnFlag As Boolean = False
'        Try
'            Dim dRow As DataRow = Nothing
'            Dim dOF() As DataRow = mdtFilter.Select("filter_refid = '" & clsAppraisal_Filter.Filter_Refid.OVERALL_SCORE & "' AND AUD <> 'D'")
'            If dOF.Length <= 0 AndAlso txtOverallScore.Text.Trim.Length > 0 Then
'                dRow = mdtFilter.NewRow
'                dRow.Item("filterunkid") = -1
'                dRow.Item("shortlistunkid") = -1
'                dRow.Item("filter_refid") = clsAppraisal_Filter.Filter_Refid.OVERALL_SCORE
'                dRow.Item("filter_value") = CDbl(txtOverallScore.Text)
'                dRow.Item("conditionunkid") = cboOverallCondition.SelectedValue
'                dRow.Item("referenceunkid") = -1
'                dRow.Item("issummary") = True
'                dRow.Item("operator") = clsAppraisal_Filter.Operater_ModeId.OP_AND
'                dRow.Item("isself_score") = chkSelf_Assess.Checked
'                dRow.Item("isassessor_score") = chkAssessor_Assess.Checked
'                dRow.Item("isreviewer_score") = chkReviewer_Assess.Checked
'                dRow.Item("AUD") = "A"
'                dRow.Item("GUID") = Guid.NewGuid.ToString
'                dRow.Item("filter_criteria") = gbOverAllScore.Text
'                dRow.Item("col_tag") = gbOverAllScore.Text
'                Select Case CInt(cboOverallCondition.SelectedValue)
'                    Case 0
'                        dRow.Item("contition") = "="
'                    Case Else
'                        dRow.Item("contition") = cboOverallCondition.Text
'                End Select
'                dRow.Item("filter_grp") = gbOverAllScore.Text
'                mdtFilter.Rows.Add(dRow)
'                txtOverallScore.Enabled = False : cboOverallCondition.Enabled = False
'            End If
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "Set_OverallFilter", mstrModuleName)
'        Finally
'        End Try
'    End Sub
'    'S.SANDEEP [ 14 AUG 2013 ] -- END

'#End Region

'#Region " Form's Events "

'    Private Sub frmShortlistingEmployee_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
'        objMShortList = New clsAppraisal_ShortList_Master
'        objMFilter = New clsAppraisal_Filter
'        Try
'            Call Set_Logo(Me, gApplicationType)
'            Language.setLanguage(Me.Name)
'            Call OtherSettings()

'            chkSelf_Assess.Checked = True
'            chkAssessor_Assess.Checked = True
'            chkReviewer_Assess.Checked = True

'            lvFilterCriteria.GridLines = False : lvEmployee.GridLines = False

'            Call Set_TextValue()

'            Call SetVisibility()

'            Call FillCombo()
'            Call Fill_Grid()

'            mdtFilter = objMFilter._DataTable

'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "frmShortlistingEmployee_Load", mstrModuleName)
'        Finally
'        End Try
'    End Sub

'#End Region

'#Region " Button's Events "

'    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
'        Me.Close()
'    End Sub

'    Private Sub btnAdd_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAdd.Click
'        Dim blnFlag As Boolean = False
'        Try

'            If IsValidData() = False Then Exit Sub

'            If txtBSCTotalScore.Text.Trim.Length > 0 AndAlso txtGETotalScore.Text.Trim.Length > 0 Then
'                gbSummOperation.Enabled = False : gbSummaryFilter.Enabled = False
'            End If

'            'S.SANDEEP [ 14 AUG 2013 ] -- START
'            'ENHANCEMENT : TRA CHANGES
'            'If gbSummaryFilter.Checked = True AndAlso gbDetailFilter.Checked = True Then
'            '    Call Set_SummaryFilter() : Call Set_DetailFilter()
'            'ElseIf gbSummaryFilter.Checked = True AndAlso gbDetailFilter.Checked = False Then
'            '    Call Set_SummaryFilter()
'            'ElseIf gbSummaryFilter.Checked = False AndAlso gbDetailFilter.Checked = True Then
'            '    Call Set_DetailFilter()
'            'End If
'            If txtOverallScore.Text.Trim.Length > 0 Then
'                gbOverAllScore.Enabled = False
'            End If
'            If gbSummaryFilter.Checked = True AndAlso gbDetailFilter.Checked = True AndAlso gbOverAllScore.Checked = True Then
'                Call Set_OverallFilter() : Call Set_SummaryFilter() : Call Set_DetailFilter()
'            ElseIf gbSummaryFilter.Checked = True AndAlso gbDetailFilter.Checked = False AndAlso gbOverAllScore.Checked = False Then
'                Call Set_SummaryFilter()
'            ElseIf gbSummaryFilter.Checked = False AndAlso gbDetailFilter.Checked = True AndAlso gbOverAllScore.Checked = False Then
'                Call Set_DetailFilter()
'            ElseIf gbSummaryFilter.Checked = False AndAlso gbDetailFilter.Checked = False AndAlso gbOverAllScore.Checked = True Then
'                Call Set_OverallFilter()
'            End If
'            'S.SANDEEP [ 14 AUG 2013 ] -- END




'            dBSCPRow = mdtFilter.Select("filter_refid = '" & clsAppraisal_Filter.Filter_Refid.BSC_PERSPECTIVE & "' AND AUD <> 'D'")
'            dGEIERow = mdtFilter.Select("filter_refid = '" & clsAppraisal_Filter.Filter_Refid.GENEAL_EVALUATION & "' AND AUD <> 'D'")

'            If dBSCPRow.Length > 0 Then
'                radPerspectiveAND.Enabled = False
'                radPerspectiveOR.Enabled = False
'                objbtnResetPerspective.Enabled = False
'            End If

'            If dBSCPRow.Length > 0 AndAlso dGEIERow.Length > 0 Then
'                gbDetailOperation.Enabled = False
'            End If

'            If mdtFilter.Rows.Count > 0 Then
'                Dim dDetail() As DataRow = mdtFilter.Select("issummary=false", "filter_refid")

'                For i As Integer = 0 To dDetail.Length - 1
'                    Select Case CInt(dDetail(i)("filter_refid"))
'                        Case clsAppraisal_Filter.Filter_Refid.GENEAL_EVALUATION
'                            If blnFlag = False Then
'                                If radDetailAND.Checked = True Then
'                                    dDetail(i - 1).Item("operator") = clsAppraisal_Filter.Operater_ModeId.OP_AND
'                                    dDetail(i - 1).Item("operation") = radDetailAND.Text
'                                    dDetail(i - 1).AcceptChanges()
'                                ElseIf radDetailOR.Checked = True Then
'                                    dDetail(i - 1).Item("operator") = clsAppraisal_Filter.Operater_ModeId.OP_OR
'                                    dDetail(i - 1).Item("operation") = radDetailOR.Text
'                                    dDetail(i - 1).AcceptChanges()
'                                End If
'                                blnFlag = True
'                            End If
'                            dDetail(i).Item("operator") = clsAppraisal_Filter.Operater_ModeId.OP_AND
'                            dDetail(i).Item("operation") = radDetailAND.Text
'                            dDetail(i).AcceptChanges()
'                    End Select
'                Next

'                Dim dView As DataView = mdtFilter.DefaultView
'                dView.Sort = "filter_refid"
'                mdtFilter = dView.ToTable

'                mdtFilter.Rows(mdtFilter.Rows.Count - 1)("operator") = -1
'                mdtFilter.Rows(mdtFilter.Rows.Count - 1)("operation") = ""
'                mdtFilter.AcceptChanges()
'            End If

'            Call Fill_FilterList()

'            If mdtFilter.Rows.Count > 0 Then
'                chkSelf_Assess.Enabled = False
'                chkAssessor_Assess.Enabled = False
'                chkReviewer_Assess.Enabled = False
'            End If
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "btnAdd_Click", mstrModuleName)
'        Finally
'        End Try
'    End Sub

'    Private Sub btnRemove_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnRemove.Click
'        Try
'            If lvFilterCriteria.SelectedItems.Count > 0 Then
'                Dim drTemp() As DataRow = Nothing
'                If CInt(lvFilterCriteria.SelectedItems(0).Tag) = -1 Then
'                    drTemp = mdtFilter.Select("GUID = '" & lvFilterCriteria.SelectedItems(0).SubItems(objcolhGuid.Index).Text & "'")
'                Else
'                    drTemp = mdtFilter.Select("filterunkid = '" & CInt(lvFilterCriteria.SelectedItems(0).Tag) & "'")
'                End If

'                If drTemp.Length > 0 Then
'                    '/* THIS IS FOR ENABLE THE DISABLED CONTROLS WHEN THEY ARE REMOVED FROM LIST */
'                    Select Case CInt(drTemp(0)("filter_refid"))
'                        Case clsAppraisal_Filter.Filter_Refid.TOTAL_BSC_SCORE
'                            txtBSCTotalScore.Enabled = True : cboBSCTotalCondition.Enabled = True
'                            gbSummOperation.Enabled = True : gbSummaryFilter.Enabled = True
'                            Call objbtnSummaryReset_Click(sender, e)
'                        Case clsAppraisal_Filter.Filter_Refid.TOTAL_GE_SCORE
'                            txtGETotalScore.Enabled = True : cboGETotalCondition.Enabled = True
'                            gbSummOperation.Enabled = True : gbSummaryFilter.Enabled = True
'                            Call objbtnSummaryReset_Click(sender, e)
'                        Case clsAppraisal_Filter.Filter_Refid.BSC_PERSPECTIVE
'                            'Select Case CInt(drTemp(0)("referenceunkid"))
'                            '    Case 1  'FINANCIAL
'                            '        chkFinancial.Enabled = True
'                            '        txtFTotalScore.Enabled = True
'                            '        cboFCondition.Enabled = True
'                            '    Case 2  'CUSTOMER
'                            '        chkCustomer.Enabled = True
'                            '        txtCTotalScore.Enabled = True
'                            '        cboCCondition.Enabled = True
'                            '    Case 3  'BUSINESS PROCESS
'                            '        chkBusinessProcess.Enabled = True
'                            '        txtBTotalScore.Enabled = True
'                            '        cboBCondition.Enabled = True
'                            '    Case 4  'ORGANIZATION CAPACITY
'                            '        chkOrgCapacity.Enabled = True
'                            '        txtOTotalScore.Enabled = True
'                            '        cboOCondition.Enabled = True
'                            'End Select
'                            'dBSCPRow = mdtFilter.Select("filter_refid = '" & clsAppraisal_Filter.Filter_Refid.BSC_PERSPECTIVE & "' AND AUD <> 'D' AND referenceunkid <> '" & CInt(drTemp(0)("referenceunkid")) & "'")
'                            'If dBSCPRow.Length <= 0 Then
'                            '    gbDetailOperation.Enabled = True
'                            '    radPerspectiveAND.Enabled = True : radPerspectiveOR.Enabled = True
'                            '    objbtnResetPerspective.Enabled = True
'                            '    Call objbtnResetPerspective_Click(sender, e)
'                            '    Call objbtnDetailReset_Click(sender, e)
'                            'End If
'                        Case clsAppraisal_Filter.Filter_Refid.GENEAL_EVALUATION
'                            dGEIERow = mdtFilter.Select("filter_refid = '" & clsAppraisal_Filter.Filter_Refid.GENEAL_EVALUATION & "' AND AUD <> 'D' AND referenceunkid <> '" & CInt(drTemp(0)("referenceunkid")) & "'")
'                            If dGEIERow.Length <= 0 Then
'                                gbDetailOperation.Enabled = True
'                                Call objbtnDetailReset_Click(sender, e)
'                            End If
'                            'S.SANDEEP [ 14 AUG 2013 ] -- START
'                            'ENHANCEMENT : TRA CHANGES
'                        Case clsAppraisal_Filter.Filter_Refid.OVERALL_SCORE
'                            txtOverallScore.Enabled = True : cboOverallCondition.Enabled = True
'                            gbOverAllScore.Enabled = True
'                            'S.SANDEEP [ 14 AUG 2013 ] -- END
'                    End Select


'                    Select Case CInt(drTemp(0)("filter_refid"))
'                        Case clsAppraisal_Filter.Filter_Refid.TOTAL_BSC_SCORE
'                            drTemp(0)("AUD") = "D"
'                        Case clsAppraisal_Filter.Filter_Refid.TOTAL_GE_SCORE
'                            Dim dTemp() As DataRow = mdtFilter.Select("filter_refid = '" & clsAppraisal_Filter.Filter_Refid.TOTAL_BSC_SCORE & "' AND AUD <> 'D'")
'                            If dTemp.Length > 0 Then
'                                dTemp(0)("operator") = -1
'                                dTemp(0)("operation") = ""
'                                dTemp(0).AcceptChanges()
'                            End If
'                            drTemp(0)("AUD") = "D"
'                        Case clsAppraisal_Filter.Filter_Refid.BSC_PERSPECTIVE
'                            Select Case CInt(drTemp(0)("operator"))
'                                Case clsAppraisal_Filter.Operater_ModeId.OP_AND, clsAppraisal_Filter.Operater_ModeId.OP_OR
'                                    Dim iMax As Integer = -1
'                                    If IsDBNull(mdtFilter.Compute("MAX(referenceunkid)", "filter_refid = '" & clsAppraisal_Filter.Filter_Refid.BSC_PERSPECTIVE & "' AND AUD <> 'D'")) = False Then
'                                        iMax = CInt(mdtFilter.Compute("MAX(referenceunkid)", "filter_refid = '" & clsAppraisal_Filter.Filter_Refid.BSC_PERSPECTIVE & "' AND AUD <> 'D'"))
'                                    End If
'                                    If iMax > 0 Then
'                                        If CInt(drTemp(0)("referenceunkid")) = iMax Then
'                                            Dim dTemp() As DataRow = mdtFilter.Select("filter_refid = '" & clsAppraisal_Filter.Filter_Refid.BSC_PERSPECTIVE & "' AND AUD <> 'D' AND referenceunkid <> '" & CInt(drTemp(0)("referenceunkid")) & "'")
'                                            If dTemp.Length > 0 Then
'                                                dTemp(dTemp.Length - 1)("operator") = drTemp(0)("operator")
'                                                dTemp(dTemp.Length - 1)("operation") = drTemp(0)("operation")
'                                                dTemp(dTemp.Length - 1).AcceptChanges()
'                                            End If
'                                        End If
'                                    End If
'                                    drTemp(0)("AUD") = "D"
'                                Case Else
'                                    Dim dTemp() As DataRow = mdtFilter.Select("filter_refid = '" & clsAppraisal_Filter.Filter_Refid.BSC_PERSPECTIVE & "' AND AUD <> 'D' AND referenceunkid <> '" & CInt(drTemp(0)("referenceunkid")) & "'")
'                                    If dTemp.Length > 0 Then
'                                        dTemp(dTemp.Length - 1)("operator") = -1
'                                        dTemp(dTemp.Length - 1)("operation") = ""
'                                        dTemp(dTemp.Length - 1).AcceptChanges()
'                                    End If
'                                    drTemp(0)("AUD") = "D"
'                            End Select

'                        Case clsAppraisal_Filter.Filter_Refid.GENEAL_EVALUATION
'                            Select Case CInt(drTemp(0)("operator"))
'                                Case clsAppraisal_Filter.Operater_ModeId.OP_AND, clsAppraisal_Filter.Operater_ModeId.OP_OR
'                                    drTemp(0)("AUD") = "D"
'                                Case Else
'                                    Dim dTemp() As DataRow = mdtFilter.Select("filter_refid = '" & clsAppraisal_Filter.Filter_Refid.GENEAL_EVALUATION & "' AND AUD <> 'D' AND referenceunkid <> '" & CInt(drTemp(0)("referenceunkid")) & "'")
'                                    If dTemp.Length > 0 Then
'                                        dTemp(dTemp.Length - 1)("operator") = -1
'                                        dTemp(dTemp.Length - 1)("operation") = ""
'                                        dTemp(dTemp.Length - 1).AcceptChanges()
'                                    ElseIf dTemp.Length <= 0 Then
'                                        Dim iMax As Integer = -1
'                                        If IsDBNull(mdtFilter.Compute("MAX(referenceunkid)", "filter_refid = '" & clsAppraisal_Filter.Filter_Refid.BSC_PERSPECTIVE & "' AND AUD <> 'D'")) = False Then
'                                            iMax = CInt(mdtFilter.Compute("MAX(referenceunkid)", "filter_refid = '" & clsAppraisal_Filter.Filter_Refid.BSC_PERSPECTIVE & "' AND AUD <> 'D'"))
'                                        End If
'                                        If iMax > 0 Then
'                                            Dim dRow() As DataRow = mdtFilter.Select("filter_refid = '" & clsAppraisal_Filter.Filter_Refid.BSC_PERSPECTIVE & "' AND AUD <> 'D' AND referenceunkid = '" & CInt(iMax) & "'")
'                                            If dRow.Length > 0 Then
'                                                dRow(0)("operator") = drTemp(0)("operator")
'                                                dRow(0)("operation") = drTemp(0)("operation")
'                                                drTemp(0)("AUD") = "D"
'                                            End If
'                                        End If
'                                    End If
'                                    drTemp(0)("AUD") = "D"
'                            End Select
'                            'S.SANDEEP [ 14 AUG 2013 ] -- START
'                            'ENHANCEMENT : TRA CHANGES
'                        Case clsAppraisal_Filter.Filter_Refid.OVERALL_SCORE
'                            drTemp(0)("AUD") = "D"
'                            'S.SANDEEP [ 14 AUG 2013 ] -- END
'                    End Select
'                End If
'                Call Fill_FilterList()
'            End If

'            If lvFilterCriteria.Items.Count <= 0 Then
'                chkSelf_Assess.Enabled = True
'                chkAssessor_Assess.Enabled = True
'                chkReviewer_Assess.Enabled = True
'                lvEmployee.Items.Clear()
'            End If

'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "btnRemove_Click", mstrModuleName)
'        Finally
'        End Try
'    End Sub

'    Private Sub btnApplyFilter_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnApplyFilter.Click
'        Try

'            'S.SANDEEP [ 22 OCT 2013 ] -- START
'            If CInt(cboAppointment.SelectedValue) = enAD_Report_Parameter.APP_DATE_FROM Then
'                If dtpDate1.Checked = True AndAlso dtpDate2.Checked = False Then
'                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 23, "Sorry, Appointment To Date is mandatory information. Please set Appointment To Date."), enMsgBoxStyle.Information)
'                    dtpDate2.Focus()
'                    Exit Sub
'                ElseIf dtpDate1.Checked = False AndAlso dtpDate2.Checked = True Then
'                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 24, "Sorry, Appointment From Date is mandatory information. Please set Appointment From Date."), enMsgBoxStyle.Information)
'                    dtpDate1.Focus()
'                    Exit Sub
'                ElseIf dtpDate1.Checked = True AndAlso dtpDate2.Checked = True Then
'                    If dtpDate2.Value.Date < dtpDate1.Value.Date Then
'                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 25, "Sorry, Appointment To Date cannot be less then Appointment From Date."), enMsgBoxStyle.Information)
'                        dtpDate2.Focus()
'                        Exit Sub
'                    End If
'                End If
'            End If

'            objMFilter._AppointmentTypeId = CInt(cboAppointment.SelectedValue)            
'            If dtpDate1.Checked = True Then
'                objMFilter._Date1 = dtpDate1.Value.Date
'            End If

'            If dtpDate2.Visible = True Then
'                If dtpDate2.Checked = True Then
'                    objMFilter._Date2 = dtpDate2.Value.Date
'                End If
'            End If

'            'S.SANDEEP [ 22 OCT 2013 ] -- END

'            dsFilteredEmp = objMFilter.GetFilteredEmployee(mdtFilter, CInt(cboPeriod.SelectedValue))


'            lvEmployee.Items.Clear()

'            For Each drRow As DataRow In dsFilteredEmp.Tables(0).Rows
'                Dim lvItem As New ListViewItem

'                If lvEmployee.FindItemWithText(CStr(drRow.Item("employeecode"))) Is Nothing Then
'                    lvItem.Text = drRow.Item("employeecode").ToString
'                    lvItem.SubItems.Add(drRow.Item("ENAME").ToString)
'                    'S.SANDEEP [ 14 AUG 2013 ] -- START
'                    'ENHANCEMENT : TRA CHANGES
'                    'lvItem.SubItems.Add(drRow.Item("Date").ToString)
'                    lvItem.SubItems.Add(CDate(drRow.Item("Date")).ToShortDateString)
'                    'S.SANDEEP [ 14 AUG 2013 ] -- END
'                    lvItem.SubItems.Add(drRow.Item("EMAIL").ToString)
'                    lvItem.SubItems.Add(drRow.Item("F_RefId").ToString)
'                    'S.SANDEEP [ 14 AUG 2013 ] -- START
'                    'ENHANCEMENT : TRA CHANGES
'                    lvItem.SubItems.Add(drRow.Item("JOB_TITLE").ToString)
'                    lvItem.SubItems.Add(drRow.Item("DEPT").ToString)
'                    lvItem.SubItems.Add(drRow.Item("SCORE").ToString)
'                    'S.SANDEEP [ 14 AUG 2013 ] -- END

'                    lvItem.Tag = drRow.Item("EGUID")

'                    lvEmployee.Items.Add(lvItem)
'                End If
'            Next

'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "btnApplyFilter_Click", mstrModuleName)
'        Finally
'        End Try
'    End Sub

'    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
'        Dim blnflag As Boolean = False
'        Try
'            If lvEmployee.Items.Count <= 0 Then
'                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 14, "Sorry, there is no employee in the list to export."), enMsgBoxStyle.Information)
'                Exit Sub
'            End If

'            'S.SANDEEP [ 05 MARCH 2012 ] -- START
'            'ENHANCEMENT : TRA CHANGES
'            If txtRemark.Text.Trim.Length <= 0 Then
'                'S.SANDEEP [ 14 AUG 2013 ] -- START
'                'ENHANCEMENT : TRA CHANGES
'                'If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 19, "You have not specified the remark. Do you wish to save & export without remark?"), CType(enMsgBoxStyle.Information + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.No Then
'                '    txtRemark.Focus()
'                '    Exit Sub
'                'End If
'                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 19, "Remark cannot be blank. Remark is mandatory information."), enMsgBoxStyle.Information)
'                    txtRemark.Focus()
'                    Exit Sub
'                'S.SANDEEP [ 14 AUG 2013 ] -- END
'            End If
'            'S.SANDEEP [ 05 MARCH 2012 ] -- END

'            objMShortList._Referencedate = ConfigParameter._Object._CurrentDateAndTime
'            objMShortList._Userunkid = User._Object._Userunkid
'            objMShortList._Referenceno = Now.Millisecond.ToString & ConfigParameter._Object._CurrentDateAndTime.Year.ToString & ConfigParameter._Object._CurrentDateAndTime.ToString("hhmmss") & ConfigParameter._Object._CurrentDateAndTime.Date.Day.ToString("0#") & ConfigParameter._Object._CurrentDateAndTime.Date.Month.ToString("0#")
'            'S.SANDEEP [ 05 MARCH 2012 ] -- START
'            'ENHANCEMENT : TRA CHANGES
'            objMShortList._Remark = txtRemark.Text
'            objMShortList._Periodunkid = CInt(cboPeriod.SelectedValue)
'            'S.SANDEEP [ 05 MARCH 2012 ] -- END

'            blnflag = objMShortList.Insert(mdtFilter, dsFilteredEmp)

'            If blnflag = False And objMShortList._Message <> "" Then
'                eZeeMsgBox.Show(objMShortList._Message, enMsgBoxStyle.Information)
'                'S.SANDEEP [ 14 AUG 2013 ] -- START
'                'ENHANCEMENT : TRA CHANGES
'            Else
'                dsFilteredEmp.Tables(0).Columns.Remove("AUD")
'                'S.SANDEEP [ 14 AUG 2013 ] -- END
'            End If

'            Dim objFile As New SaveFileDialog
'            objFile.OverwritePrompt = True
'            objFile.Filter = "Xls Files (*.xls) |*.xls"

'            If objFile.ShowDialog() = Windows.Forms.DialogResult.OK Then
'                Dim obj As New FileInfo(objFile.FileName)
'                Dim mstrFilePath As String = objFile.FileName.Substring(0, objFile.FileName.LastIndexOf(CChar("\")))
'                If Export_to_Excel(True, obj.Name, mstrFilePath, dsFilteredEmp.Tables(0), mdtFilter) Then
'                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 15, "Data Exported successfully."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
'                    Call ReportFunction.Open_ExportedFile(ConfigParameter._Object._OpenAfterExport, mstrFilePath)
'                End If
'            End If

'            If blnflag Then
'                mblnCancel = False
'                Me.Close()
'            End If

'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "btnSave_Click", mstrModuleName)
'        Finally
'        End Try
'    End Sub

'    Private Sub btnExport_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnExport.Click
'        Try

'            If lvEmployee.Items.Count <= 0 Then
'                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 14, "Sorry, there is no employee in the list to export."), enMsgBoxStyle.Information)
'                Exit Sub
'            End If

'            Dim objFile As New SaveFileDialog
'            objFile.OverwritePrompt = True
'            objFile.Filter = "Xls Files (*.xls) |*.xls"

'            If objFile.ShowDialog() = Windows.Forms.DialogResult.OK Then
'                Dim obj As New FileInfo(objFile.FileName)
'                Dim mstrFilePath As String = objFile.FileName.Substring(0, objFile.FileName.LastIndexOf(CChar("\")))
'                If Export_to_Excel(False, obj.Name, mstrFilePath, dsFilteredEmp.Tables(0), mdtFilter) Then
'                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 15, "Data Exported successfully."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
'                    Call ReportFunction.Open_ExportedFile(ConfigParameter._Object._OpenAfterExport, mstrFilePath)
'                End If
'            End If
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "", mstrModuleName)
'        Finally
'        End Try
'    End Sub

'#End Region

'#Region " Controls "

'    Private Sub gbDetailFilter_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles gbDetailFilter.CheckedChanged
'        Try
'            Call SetVisibility()
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "gbDetailFilter_CheckedChanged", mstrModuleName)
'        Finally
'        End Try
'    End Sub

'    Private Sub dgvData_CellContentClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvData.CellContentClick, dgvData.CellContentDoubleClick
'        Try
'            Dim i As Integer
'            If e.RowIndex = -1 Then Exit Sub

'            If Me.dgvData.IsCurrentCellDirty Then
'                Me.dgvData.CommitEdit(DataGridViewDataErrorContexts.Commit)
'            End If

'            If CBool(dgvData.Rows(e.RowIndex).Cells(objdgcolhIsGrp.Index).Value) = True Then
'                Select Case CInt(e.ColumnIndex)
'                    Case 0
'                        If dgvData.Rows(e.RowIndex).Cells(objdgcolhCollaps.Index).Value Is "-" Then
'                            dgvData.Rows(e.RowIndex).Cells(objdgcolhCollaps.Index).Value = "+"
'                        Else
'                            dgvData.Rows(e.RowIndex).Cells(objdgcolhCollaps.Index).Value = "-"
'                        End If

'                        For i = e.RowIndex + 1 To dgvData.RowCount - 1
'                            If CInt(dgvData.Rows(e.RowIndex).Cells(objdgcolhGrpId.Index).Value) = CInt(dgvData.Rows(i).Cells(objdgcolhGrpId.Index).Value) Then
'                                If dgvData.Rows(i).Visible = False Then
'                                    dgvData.Rows(i).Visible = True
'                                Else
'                                    dgvData.Rows(i).Visible = False
'                                End If
'                            Else
'                                Exit For
'                            End If
'                        Next
'                End Select
'            Else

'            End If
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "dgvData_CellContentClick", mstrModuleName)
'        Finally
'        End Try
'    End Sub

'    Private Sub dgvData_DataError(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewDataErrorEventArgs) Handles dgvData.DataError

'    End Sub

'    Private Sub dgvData_CellEnter(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvData.CellEnter
'        Try
'            If e.ColumnIndex = dgcolhCondition.Index Or e.ColumnIndex = dgcolhScore.Index Then
'                SendKeys.Send("{F2}")
'            End If
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "dgvData_CellEnter", mstrModuleName)
'        End Try
'    End Sub

'    Private Sub dgvData_EditingControlShowing(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewEditingControlShowingEventArgs) Handles dgvData.EditingControlShowing
'        Dim txt As TextBox
'        Try
'            If dgvData.CurrentCell.ColumnIndex = dgcolhScore.Index AndAlso TypeOf e.Control Is TextBox Then
'                txt = CType(e.Control, TextBox)
'                RemoveHandler txt.KeyPress, AddressOf tb_keypress

'                AddHandler txt.KeyPress, AddressOf tb_keypress
'            End If
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "dgvData_EditingControlShowing", mstrModuleName)
'        End Try
'    End Sub

'    'Private Sub chkOrgCapacity_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkOrgCapacity.CheckedChanged
'    '    Try
'    '        Call SetVisibility()
'    '    Catch ex As Exception
'    '        DisplayError.Show("-1", ex.Message, "chkOrgCapacity_CheckedChanged", mstrModuleName)
'    '    Finally
'    '    End Try
'    'End Sub

'    'Private Sub chkBusinessProcess_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkBusinessProcess.CheckedChanged
'    '    Try
'    '        Call SetVisibility()
'    '    Catch ex As Exception
'    '        DisplayError.Show("-1", ex.Message, "chkBusinessProcess_CheckedChanged", mstrModuleName)
'    '    Finally
'    '    End Try
'    'End Sub

'    'Private Sub chkCustomer_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkCustomer.CheckedChanged
'    '    Try
'    '        Call SetVisibility()
'    '    Catch ex As Exception
'    '        DisplayError.Show("-1", ex.Message, "chkCustomer_CheckedChanged", mstrModuleName)
'    '    Finally
'    '    End Try
'    'End Sub

'    'Private Sub chkFinancial_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkFinancial.CheckedChanged
'    '    Try
'    '        Call SetVisibility()
'    '    Catch ex As Exception
'    '        DisplayError.Show("-1", ex.Message, "chkFinancial_CheckedChanged", mstrModuleName)
'    '    Finally
'    '    End Try
'    'End Sub

'    Private Sub objbtnSummaryReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSummaryReset.Click
'        Try
'            radSummaryAND.Checked = False : radSummaryOR.Checked = False
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "objbtnSummaryReset_Click", mstrModuleName)
'        Finally
'        End Try
'    End Sub

'    Private Sub objbtnDetailReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnDetailReset.Click
'        Try
'            radDetailAND.Checked = False
'            radDetailOR.Checked = False
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "objbtnDetailReset_Click", mstrModuleName)
'        Finally
'        End Try
'    End Sub

'    Private Sub objbtnResetPerspective_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnResetPerspective.Click
'        Try
'            radPerspectiveAND.Checked = False
'            radPerspectiveOR.Checked = False
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "objbtnResetPerspective_Click", mstrModuleName)
'        Finally
'        End Try
'    End Sub

'    'S.SANDEEP [ 05 MARCH 2012 ] -- START
'    'ENHANCEMENT : TRA CHANGES
'    Private Sub objbtnSearchPeriod_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchPeriod.Click
'        Dim frm As New frmCommonSearch
'        Try
'            With frm
'                .ValueMember = cboPeriod.ValueMember
'                .DisplayMember = cboPeriod.DisplayMember
'                .DataSource = CType(cboPeriod.DataSource, DataTable)
'            End With
'            If frm.DisplayDialog Then
'                cboPeriod.SelectedValue = frm.SelectedValue
'                cboPeriod.Focus()
'            End If
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "objbtnSearchPeriod_Click", mstrModuleName)
'        Finally
'            If frm IsNot Nothing Then frm.Dispose()
'        End Try
'    End Sub
'    'S.SANDEEP [ 05 MARCH 2012 ] -- END

'    'S.SANDEEP [ 13 AUG 2013 ] -- START
'    'ENHANCEMENT : TRA CHANGES
'    Private Sub cboPeriod_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
'        Try
'            Call Fill_Grid()
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "cboPeriod_SelectedIndexChanged", mstrModuleName)
'        Finally
'        End Try
'    End Sub
'    'S.SANDEEP [ 13 AUG 2013 ] -- END

'    'S.SANDEEP [ 22 OCT 2013 ] -- START
'    Private Sub cboAppointment_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboAppointment.SelectedIndexChanged
'        Try
'            objlblCaption.Text = cboAppointment.Text
'            Select Case CInt(cboAppointment.SelectedValue)
'                Case enAD_Report_Parameter.APP_DATE_FROM
'                    If dtpDate2.Visible = False Then dtpDate2.Visible = True
'                    If lblTo.Visible = False Then lblTo.Visible = True
'                Case enAD_Report_Parameter.APP_DATE_BEFORE
'                    dtpDate2.Visible = False : lblTo.Visible = False
'                Case enAD_Report_Parameter.APP_DATE_AFTER
'                    dtpDate2.Visible = False : lblTo.Visible = False
'            End Select
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "cboAppointment_SelectedIndexChanged", mstrModuleName)
'        End Try
'    End Sub
'    'S.SANDEEP [ 22 OCT 2013 ] -- END

'    'S.SANDEEP [ 05 NOV 2014 ] -- START
'    Private Sub dgvPerspective_DataError(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewDataErrorEventArgs) Handles dgvPerspective.DataError

'    End Sub

'    Private Sub dgvPerspective_CellEnter(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvPerspective.CellEnter
'        Try
'            If e.ColumnIndex = dgcolhPCondition.Index Or e.ColumnIndex = dgcolhPScore.Index Then
'                SendKeys.Send("{F2}")
'            End If
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "dgvPerspective_CellEnter", mstrModuleName)
'        End Try
'    End Sub

'    Private Sub dgvPerspective_EditingControlShowing(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewEditingControlShowingEventArgs) Handles dgvPerspective.EditingControlShowing
'        Dim txt As TextBox
'        Try
'            If dgvData.CurrentCell.ColumnIndex = dgcolhPScore.Index AndAlso TypeOf e.Control Is TextBox Then
'                txt = CType(e.Control, TextBox)
'                RemoveHandler txt.KeyPress, AddressOf tb_keypress

'                AddHandler txt.KeyPress, AddressOf tb_keypress
'            End If
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "dgvPerspective_EditingControlShowing", mstrModuleName)
'        End Try
'    End Sub
'    'S.SANDEEP [ 05 NOV 2014 ] -- END

'#End Region