﻿Option Strict On

#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data

#End Region

Public Class frmAppraisalSetups
    Private ReadOnly mstrModuleName As String = "frmAppraisalSetups"
    Private objAppRating As clsAppraisal_Rating
    Private mdtTran As DataTable

#Region " Private Function(s) & Method(s) "

    Private Sub Fill_Combo()
        Dim dsList As DataSet = Nothing
        Dim objCMaster As New clsCommon_Master
        Try
            dsList = objCMaster.getComboList(clsCommon_Master.enCommonMaster.APPRAISAL_ACTIONS, True, "List")
            With cboAppraisalAction
                .ValueMember = "masterunkid"
                .DisplayMember = "name"
                .DataSource = dsList.Tables("List")
                .SelectedValue = 0
            End With

            'Shani (26-Sep-2016) -- Start
            'Enhancement -Changes PA Flow And Add New screen Given By (Andrew)
            dsList = objCMaster.getComboList(clsCommon_Master.enCommonMaster.GE_APPRAISAL_ACTIONS, True, "List")
            With cboGEAppraisalAction
                .ValueMember = "masterunkid"
                .DisplayMember = "name"
                .DataSource = dsList.Tables("List")
                .SelectedValue = 0
            End With

            dsList = objCMaster.getComboList(clsCommon_Master.enCommonMaster.BSC_APPRAISAL_ACTIONS, True, "List")
            With cboBSCAppraisalAction
                .ValueMember = "masterunkid"
                .DisplayMember = "name"
                .DataSource = dsList.Tables("List")
                .SelectedValue = 0
            End With
            'Shani (26-Sep-2016) -- End


        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Fill_Combo", mstrModuleName)
        Finally
            dsList.Dispose() : objCMaster = Nothing
        End Try
    End Sub

    Private Sub Fill_Data()
        Try
            lvRatings.Items.Clear()
            Dim lvItem As ListViewItem
            For Each dtRow As DataRow In mdtTran.Rows
                If CStr(IIf(IsDBNull(dtRow.Item("AUD")), "A", dtRow.Item("AUD"))) <> "D" Then
                    lvItem = New ListViewItem

                    lvItem.Text = Format(CDec(dtRow.Item("score_from")), ConfigParameter._Object._CurrencyFormat)
                    lvItem.SubItems.Add(Format(CDec(dtRow.Item("score_to")), ConfigParameter._Object._CurrencyFormat))
                    lvItem.SubItems.Add(dtRow.Item("grade_award").ToString)
                    lvItem.SubItems.Add(dtRow.Item("action").ToString)
                    'Shani (26-Sep-2016) -- Start
                    'Enhancement -Changes PA Flow And Add New screen Given By (Andrew)
                    lvItem.SubItems.Add(dtRow.Item("bsc_action").ToString)
                    lvItem.SubItems.Add(dtRow.Item("ge_action").ToString)
                    'Shani (26-Sep-2016) -- End
                    lvItem.SubItems.Add(dtRow.Item("GUID").ToString)
                    lvItem.SubItems.Add(dtRow.Item("appactionunkid").ToString)
                    'Shani (26-Sep-2016) -- Start
                    'Enhancement -Changes PA Flow And Add New screen Given By (Andrew)
                    lvItem.SubItems.Add(dtRow.Item("goal_appr_actionunkid").ToString)
                    lvItem.SubItems.Add(dtRow.Item("competence_appr_actionunkid").ToString)
                    'Shani (26-Sep-2016) -- End

                    'S.SANDEEP |27-JUL-2019| -- START
                    'ISSUE/ENHANCEMENT : CALIBRATION COMMENTS FROM NMB
                    lvItem.SubItems.Add(Format(CDec(dtRow.Item("distribution")), "#######################0.#0").ToString)
                    'S.SANDEEP |27-JUL-2019| -- END

                    lvItem.Tag = dtRow.Item("appratingunkid").ToString

                    lvRatings.Items.Add(lvItem)

                End If
            Next
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Fill_Data", mstrModuleName)
        Finally
        End Try
    End Sub


    'Varsha Rana (17-Oct-2017) -- Start
    'Enhancement - Give user privileges in payroll module.
    Private Sub SetVisibility()
        Try
            btnSave.Enabled = User._Object.Privilege._AllowToSaveAppraisalSetup
            btnAdd.Enabled = User._Object.Privilege._AllowToSaveAppraisalSetup
            btnEdit.Enabled = User._Object.Privilege._AllowToEditAppraisalSetup
            btnDelete.Enabled = User._Object.Privilege._AllowToDeleteAppraisalSetup
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetVisibility", mstrModuleName)
        End Try
    End Sub
    'Varsha Rana (17-Oct-2017) -- End

    Private Sub SetColor()
        Try
            txtScoreFrom.BackColor = GUI.ColorComp
            txtScoreTo.BackColor = GUI.ColorComp
            txtGrade_Award.BackColor = GUI.ColorComp
            cboAppraisalAction.BackColor = GUI.ColorOptional
            'Shani (26-Sep-2016) -- Start
            'Enhancement -Changes PA Flow And Add New screen Given By (Andrew)
            cboGEAppraisalAction.BackColor = GUI.ColorOptional
            cboBSCAppraisalAction.BackColor = GUI.ColorOptional
            'Shani (26-Sep-2016) -- End

            'S.SANDEEP |27-JUL-2019| -- START
            'ISSUE/ENHANCEMENT : CALIBRATION COMMENTS FROM NMB
            txtPercentageDistribution.BackColor = GUI.ColorOptional
            'S.SANDEEP |27-JUL-2019| -- END

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetColor", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Function Is_Validate() As Boolean
        Try
            If txtScoreTo.Decimal <= txtScoreFrom.Decimal Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Overall To Score cannot be less or equal to Overall From Score."), enMsgBoxStyle.Information)
                txtScoreTo.Focus()
                Return False
            End If

            If txtGrade_Award.Text.Trim.Length <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Grade/Award cannot be blank. Grade/Award is mandatory information."), enMsgBoxStyle.Information)
                txtGrade_Award.Focus()
                Return False
            End If
            Return True
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Is_Validate", mstrModuleName)
        Finally
        End Try
    End Function

    Private Sub Reset_Value()
        Try
            txtGrade_Award.Text = ""
            txtScoreFrom.Text = ""
            txtScoreTo.Text = ""
            cboAppraisalAction.SelectedValue = 0

            'Shani (26-Sep-2016) -- Start
            'Enhancement -Changes PA Flow And Add New screen Given By (Andrew)
            cboGEAppraisalAction.SelectedValue = 0
            cboBSCAppraisalAction.SelectedValue = 0
            'Shani (26-Sep-2016) -- End

            'S.SANDEEP |27-JUL-2019| -- START
            'ISSUE/ENHANCEMENT : CALIBRATION COMMENTS FROM NMB
            txtPercentageDistribution.Decimal = 0
            'S.SANDEEP |27-JUL-2019| -- END

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Reset_Value", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

#Region " Form's Events "

    Private Sub frmAppraisalSetups_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        Try
            objAppRating = Nothing
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmAppraisalSetups_FormClosed", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub frmAppraisalSetups_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        objAppRating = New clsAppraisal_Rating
        Try
            Call Language.setLanguage(Me.Name)
            Call OtherSettings()
            Call Set_Logo(Me, gApplicationType)
            Call SetColor()
            Call Fill_Combo()
            mdtTran = objAppRating._DataTable
            Call Fill_Data()

           

            'Varsha Rana (17-Oct-2017) -- Start
            'Enhancement - Give user privileges
            'Shani(06-Feb-2016) -- Start
            'PA Changes Given By Glory for CCBRT
            'btnSave.Enabled = User._Object.Privilege._AllowToSaveAppraisalSetup
            'Shani(06-Feb-2016) -- End
            Call SetVisibility()
            'Varsha Rana (17-Oct-2017) -- End

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmAppraisalSetups_Load", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            'clsAppraisal_Rating.SetMessages()
            'objfrm._Other_ModuleNames = "clsAppraisal_Rating"
            objfrm._Other_ModuleNames = ""

            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub

#End Region

#Region " Button's Events "

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnClose_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Try
            If lvRatings.Items.Count <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "Sorry, Please added atleast one appraisal rating to save."), enMsgBoxStyle.Information)
                Exit Sub
            End If
            Dim blnFlag As Boolean = False

            'S.SANDEEP [28-May-2018] -- START
            'ISSUE/ENHANCEMENT : {Audit Trails} 
            objAppRating._FormName = mstrModuleName
            objAppRating._LoginEmployeeunkid = 0
            objAppRating._ClientIP = getIP()
            objAppRating._HostName = getHostName()
            objAppRating._FromWeb = False
            objAppRating._AuditUserId = User._Object._Userunkid
objAppRating._CompanyUnkid = Company._Object._Companyunkid
            objAppRating._AuditDate = ConfigParameter._Object._CurrentDateAndTime
            'S.SANDEEP [28-May-2018] -- END

            blnFlag = objAppRating.InsertUpdateDelete()
            If blnFlag = True Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 6, "Appraisal Ratings successfully saved."), enMsgBoxStyle.Information)
                Me.Close()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnSave_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub btnAdd_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAdd.Click
        Try
            If Is_Validate() = False Then Exit Sub
            Dim dtmp() As DataRow = Nothing
            dtmp = mdtTran.Select("grade_award = '" & txtGrade_Award.Text.Trim & "' AND AUD <> 'D'")
            If dtmp.Length > 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Sorry, Provided Grade/Award is already added to the list."), enMsgBoxStyle.Information)
                Exit Sub
            End If
            If mdtTran.Rows.Count > 0 Then
                Dim iMax As Decimal = CDec(mdtTran.Compute("MAX(score_to)", "AUD <> 'D'"))
                If iMax >= txtScoreFrom.Decimal Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Sorry, Rating should be greater than Previous Rating."), enMsgBoxStyle.Information)
                    Exit Sub
                End If
            End If

            Dim dtRow As DataRow = Nothing : dtRow = mdtTran.NewRow

            dtRow.Item("appratingunkid") = -1
            dtRow.Item("trandatetime") = ConfigParameter._Object._CurrentDateAndTime
            dtRow.Item("score_from") = txtScoreFrom.Decimal
            dtRow.Item("score_to") = txtScoreTo.Decimal
            dtRow.Item("grade_award") = txtGrade_Award.Text.Trim
            dtRow.Item("appactionunkid") = CInt(cboAppraisalAction.SelectedValue)
            dtRow.Item("userunkid") = User._Object._Userunkid
            dtRow.Item("isvoid") = False
            dtRow.Item("voiduserunkid") = -1
            dtRow.Item("voiddatetime") = DBNull.Value
            dtRow.Item("voidreason") = ""
            If CInt(cboAppraisalAction.SelectedValue) > 0 Then
                dtRow.Item("action") = cboAppraisalAction.Text
            Else
                dtRow.Item("action") = ""
            End If

            'Shani (26-Sep-2016) -- Start
            'Enhancement -Changes PA Flow And Add New screen Given By (Andrew)
            dtRow.Item("goal_appr_actionunkid") = CInt(cboBSCAppraisalAction.SelectedValue)
            If CInt(cboBSCAppraisalAction.SelectedValue) > 0 Then
                dtRow.Item("bsc_action") = cboBSCAppraisalAction.Text
            Else
                dtRow.Item("bsc_action") = ""
            End If

            dtRow.Item("competence_appr_actionunkid") = CInt(cboGEAppraisalAction.SelectedValue)
            If CInt(cboGEAppraisalAction.SelectedValue) > 0 Then
                dtRow.Item("ge_action") = cboGEAppraisalAction.Text
            Else
                dtRow.Item("ge_action") = ""
            End If
            'Shani (26-Sep-2016) -- End


            dtRow.Item("AUD") = "A"
            dtRow.Item("GUID") = Guid.NewGuid.ToString

            'S.SANDEEP |27-JUL-2019| -- START
            'ISSUE/ENHANCEMENT : CALIBRATION COMMENTS FROM NMB
            dtRow.Item("distribution") = txtPercentageDistribution.Decimal
            'S.SANDEEP |27-JUL-2019| -- END

            mdtTran.Rows.Add(dtRow)

            Call Fill_Data()
            Call Reset_Value()
            txtScoreFrom.Focus()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnAdd_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub btnEdit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEdit.Click
        Try
            If lvRatings.SelectedItems.Count > 0 Then
                Dim drTemp As DataRow()
                Dim dtmp() As DataRow = Nothing
                If CInt(lvRatings.SelectedItems(0).Tag) <= 0 Then
                    drTemp = mdtTran.Select("GUID = '" & lvRatings.SelectedItems(0).SubItems(objcolhGUID.Index).Text & "'")
                    dtmp = mdtTran.Select("grade_award = '" & txtGrade_Award.Text.Trim & "' AND AUD <> 'D' AND GUID <> '" & lvRatings.SelectedItems(0).SubItems(objcolhGUID.Index).Text & "'")
                Else
                    drTemp = mdtTran.Select("appratingunkid = '" & CInt(lvRatings.SelectedItems(0).Tag) & "'")
                    dtmp = mdtTran.Select("grade_award = '" & txtGrade_Award.Text.Trim & "' AND AUD <> 'D' AND appratingunkid <> '" & CInt(lvRatings.SelectedItems(0).Tag) & "'")
                End If
                If dtmp.Length > 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Sorry, Provided Grade/Award is already added to the list."), enMsgBoxStyle.Information)
                    Exit Sub
                End If
                If drTemp.Length > 0 Then
                    With drTemp(0)
                        .Item("appratingunkid") = .Item("appratingunkid")
                        .Item("trandatetime") = .Item("trandatetime")
                        .Item("score_from") = txtScoreFrom.Decimal
                        .Item("score_to") = txtScoreTo.Decimal
                        .Item("grade_award") = txtGrade_Award.Text.Trim
                        .Item("appactionunkid") = CInt(cboAppraisalAction.SelectedValue)
                        .Item("userunkid") = User._Object._Userunkid
                        .Item("isvoid") = False
                        .Item("voiduserunkid") = -1
                        .Item("voiddatetime") = DBNull.Value
                        .Item("voidreason") = ""
                        If CInt(cboAppraisalAction.SelectedValue) > 0 Then
                            .Item("action") = cboAppraisalAction.Text
                        Else
                            .Item("action") = ""
                        End If

                        'Shani (26-Sep-2016) -- Start
                        'Enhancement -Changes PA Flow And Add New screen Given By (Andrew)
                        .Item("goal_appr_actionunkid") = CInt(cboBSCAppraisalAction.SelectedValue)
                        If CInt(cboBSCAppraisalAction.SelectedValue) > 0 Then
                            .Item("bsc_action") = cboBSCAppraisalAction.Text
                        Else
                            .Item("bsc_action") = ""
                        End If

                        .Item("competence_appr_actionunkid") = CInt(cboGEAppraisalAction.SelectedValue)
                        If CInt(cboGEAppraisalAction.SelectedValue) > 0 Then
                            .Item("ge_action") = cboGEAppraisalAction.Text
                        Else
                            .Item("ge_action") = ""
                        End If
                        'Shani (26-Sep-2016) -- End

                        If IsDBNull(.Item("AUD")) Or CStr(.Item("AUD")).ToString.Trim = "" Then
                            .Item("AUD") = "U"
                        End If
                        .Item("GUID") = Guid.NewGuid.ToString

                        'S.SANDEEP |27-JUL-2019| -- START
                        'ISSUE/ENHANCEMENT : CALIBRATION COMMENTS FROM NMB
                        .Item("distribution") = txtPercentageDistribution.Decimal
                        'S.SANDEEP |27-JUL-2019| -- END

                    End With
                    mdtTran.AcceptChanges()
                    Call Fill_Data()
                    Call Reset_Value()
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnEdit_Click", mstrModuleName)
        Finally
            btnAdd.Enabled = True
        End Try
    End Sub

    Private Sub btnDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDelete.Click
        Try
            If mdtTran.Rows.Count > 0 Then
                Dim dtmp() As DataRow = Nothing
                If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 7, "Are you sure you want to delete the ratings?"), CType(enMsgBoxStyle.Information + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.Yes Then
                    dtmp = mdtTran.Select("appratingunkid > 0")
                    If dtmp.Length > 0 Then
                        Dim frm As New frmReasonSelection : Dim sVoidReason As String = String.Empty
                        frm.displayDialog(enVoidCategoryType.ASSESSMENT, sVoidReason)
                        If sVoidReason.Trim.Length <= 0 Then Exit Sub
                        For iRow As Integer = 0 To dtmp.Length - 1
                            dtmp(iRow).Item("isvoid") = True
                            dtmp(iRow).Item("voiduserunkid") = User._Object._Userunkid
                            dtmp(iRow).Item("voiddatetime") = ConfigParameter._Object._CurrentDateAndTime
                            dtmp(iRow).Item("voidreason") = sVoidReason
                            dtmp(iRow).Item("AUD") = "D"
                            dtmp(iRow).Item("GUID") = Guid.NewGuid.ToString
                        Next
                    End If
                    dtmp = mdtTran.Select("appratingunkid <= 0")
                    If dtmp.Length > 0 Then
                        For iRow As Integer = 0 To dtmp.Length - 1
                            dtmp(iRow).Item("AUD") = "D"
                            dtmp(iRow).Item("GUID") = Guid.NewGuid.ToString
                        Next
                    End If
                    mdtTran.AcceptChanges()

                    'S.SANDEEP [28-May-2018] -- START
                    'ISSUE/ENHANCEMENT : {Audit Trails} 
                    objAppRating._FormName = mstrModuleName
                    objAppRating._LoginEmployeeunkid = 0
                    objAppRating._ClientIP = getIP()
                    objAppRating._HostName = getHostName()
                    objAppRating._FromWeb = False
                    objAppRating._AuditUserId = User._Object._Userunkid
objAppRating._CompanyUnkid = Company._Object._Companyunkid
                    objAppRating._AuditDate = ConfigParameter._Object._CurrentDateAndTime
                    'S.SANDEEP [28-May-2018] -- END

                    If objAppRating.InsertUpdateDelete() = True Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 8, "Appraisal Ratings deleted successfully."), enMsgBoxStyle.Information)
                        mdtTran.Rows.Clear()
                        Call Fill_Data()
                    End If
                Else
                    Exit Sub
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnDelete_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub objbtnSearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearch.Click
        Dim frm As New frmCommonSearch
        Try
            With frm
                .ValueMember = cboAppraisalAction.ValueMember
                .DisplayMember = cboAppraisalAction.DisplayMember
                .CodeMember = ""
                .DataSource = CType(cboAppraisalAction.DataSource, DataTable)
            End With
            If frm.DisplayDialog Then
                cboAppraisalAction.SelectedValue = frm.SelectedValue
                cboAppraisalAction.Focus()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearch_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

#End Region

#Region " Control's Events "

    Private Sub lvRatings_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lvRatings.SelectedIndexChanged
        Try
            If lvRatings.SelectedItems.Count > 0 Then
                txtScoreFrom.Text = lvRatings.SelectedItems(0).Text
                txtScoreTo.Text = lvRatings.SelectedItems(0).SubItems(colhScoreTo.Index).Text
                txtGrade_Award.Text = lvRatings.SelectedItems(0).SubItems(colhGrade.Index).Text
                cboAppraisalAction.SelectedValue = lvRatings.SelectedItems(0).SubItems(objColhActionId.Index).Text

                'Shani (26-Sep-2016) -- Start
                'Enhancement -Changes PA Flow And Add New screen Given By (Andrew)
                cboBSCAppraisalAction.SelectedValue = lvRatings.SelectedItems(0).SubItems(objColhBSCActionId.Index).Text
                cboGEAppraisalAction.SelectedValue = lvRatings.SelectedItems(0).SubItems(objColhGEActionId.Index).Text
                'Shani (26-Sep-2016) -- End

                'S.SANDEEP |27-JUL-2019| -- START
                'ISSUE/ENHANCEMENT : CALIBRATION COMMENTS FROM NMB
                txtPercentageDistribution.Text = lvRatings.SelectedItems(0).SubItems(dgcolhPctDistribution.Index).Text
                'S.SANDEEP |27-JUL-2019| -- END

                txtScoreFrom.Enabled = False
                txtScoreTo.Enabled = False
                btnAdd.Enabled = False
            Else
                Call Reset_Value()
                txtScoreFrom.Enabled = True
                txtScoreTo.Enabled = True
                btnAdd.Enabled = True
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lvRatings_SelectedIndexChanged", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Private Sub OtherSettings()
        Try
            Me.SuspendLayout()
            
            Call SetLanguage()

            Me.gbAppraisalSetup.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbAppraisalSetup.ForeColor = GUI._eZeeContainerHeaderForeColor


            Me.btnSave.GradientBackColor = GUI._ButttonBackColor
            Me.btnSave.GradientForeColor = GUI._ButttonFontColor

            Me.btnClose.GradientBackColor = GUI._ButttonBackColor
            Me.btnClose.GradientForeColor = GUI._ButttonFontColor

            Me.btnAdd.GradientBackColor = GUI._ButttonBackColor
            Me.btnAdd.GradientForeColor = GUI._ButttonFontColor

            Me.btnDelete.GradientBackColor = GUI._ButttonBackColor
            Me.btnDelete.GradientForeColor = GUI._ButttonFontColor

            Me.btnEdit.GradientBackColor = GUI._ButttonBackColor
            Me.btnEdit.GradientForeColor = GUI._ButttonFontColor


            Me.ResumeLayout()
        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "OtherSettings", mstrModuleName)
        End Try
    End Sub


    Private Sub SetLanguage()
        Try
            Me.Text = Language._Object.getCaption(Me.Name, Me.Text)

            Me.btnSave.Text = Language._Object.getCaption(Me.btnSave.Name, Me.btnSave.Text)
            Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
            Me.gbAppraisalSetup.Text = Language._Object.getCaption(Me.gbAppraisalSetup.Name, Me.gbAppraisalSetup.Text)
            Me.lblTotalScoreT.Text = Language._Object.getCaption(Me.lblTotalScoreT.Name, Me.lblTotalScoreT.Text)
            Me.lblTotalScoreF.Text = Language._Object.getCaption(Me.lblTotalScoreF.Name, Me.lblTotalScoreF.Text)
            Me.lblGrade.Text = Language._Object.getCaption(Me.lblGrade.Name, Me.lblGrade.Text)
            Me.lblAppraisalAction.Text = Language._Object.getCaption(Me.lblAppraisalAction.Name, Me.lblAppraisalAction.Text)
            Me.btnAdd.Text = Language._Object.getCaption(Me.btnAdd.Name, Me.btnAdd.Text)
            Me.btnDelete.Text = Language._Object.getCaption(Me.btnDelete.Name, Me.btnDelete.Text)
            Me.elLine.Text = Language._Object.getCaption(Me.elLine.Name, Me.elLine.Text)
            Me.colhScoreFrom.Text = Language._Object.getCaption(CStr(Me.colhScoreFrom.Tag), Me.colhScoreFrom.Text)
            Me.colhScoreTo.Text = Language._Object.getCaption(CStr(Me.colhScoreTo.Tag), Me.colhScoreTo.Text)
            Me.colhGrade.Text = Language._Object.getCaption(CStr(Me.colhGrade.Tag), Me.colhGrade.Text)
            Me.colhAction.Text = Language._Object.getCaption(CStr(Me.colhAction.Tag), Me.colhAction.Text)
            Me.btnEdit.Text = Language._Object.getCaption(Me.btnEdit.Name, Me.btnEdit.Text)
            Me.lblBSCAppraisalAction.Text = Language._Object.getCaption(Me.lblBSCAppraisalAction.Name, Me.lblBSCAppraisalAction.Text)
            Me.lblGEAppraisalAction.Text = Language._Object.getCaption(Me.lblGEAppraisalAction.Name, Me.lblGEAppraisalAction.Text)
            Me.colhGEAction.Text = Language._Object.getCaption(CStr(Me.colhGEAction.Tag), Me.colhGEAction.Text)
            Me.colhBSCAction.Text = Language._Object.getCaption(CStr(Me.colhBSCAction.Tag), Me.colhBSCAction.Text)

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
        End Try
    End Sub


    Private Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Overall To Score cannot be less or equal to Overall From Score.")
            Language.setMessage(mstrModuleName, 2, "Grade/Award cannot be blank. Grade/Award is mandatory information.")
            Language.setMessage(mstrModuleName, 3, "Sorry, Provided Grade/Award is already added to the list.")
            Language.setMessage(mstrModuleName, 4, "Sorry, Rating should be greater than Previous Rating.")
            Language.setMessage(mstrModuleName, 5, "Sorry, Please added atleast one appraisal rating to save.")
            Language.setMessage(mstrModuleName, 6, "Appraisal Ratings successfully saved.")
            Language.setMessage(mstrModuleName, 7, "Are you sure you want to delete the ratings?")
            Language.setMessage(mstrModuleName, 8, "Appraisal Ratings deleted successfully.")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class