﻿#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data

#End Region

Public Class frmAddCustomValue

#Region " Private Variables "

    Private mstrModuleName As String = "frmAddCustomValue"
    Private mblnCancel As Boolean = True
    Private mintAnalysisUnkid As Integer = -1
    Private mintPeriodId As Integer = 0
    Private objCAssessMst As clsevaluation_analysis_master
    Private mdtCustEvaluation As DataTable
    Private iTextCtrl As System.Windows.Forms.TextBox
    Private iDateCtrl As System.Windows.Forms.MonthCalendar
    Private iNumrCtrl As eZee.TextBox.IntegerTextBox
    Private iCombCtrl As System.Windows.Forms.ComboBox
    Private mintHeaderId As Integer = -1
    Private mstrHeaderName As String = String.Empty
    Private menAction As enAction = enAction.ADD_ONE
    Private dtCItems As DataTable
    Private mintEmployeeId As Integer = 0
    Private mDefaultHieght As Integer = -1
    Private mblnIsMultiple As Boolean = False
    Private iEditingGUID As String
    Private menAssess As enAssessmentMode

    'SHANI [21 Mar 2015]-START
    'Issue : Fixing Issues Sent By Aandrew in PA Testing.
    Private mdtAssessDate As Date = Nothing
    'SHANI [21 Mar 2015]--END 

#End Region

#Region " Display Dialog "

    Public Function displayDialog(ByVal iAnalysisId As Integer, ByVal iPeriodId As Integer, _
                                  ByVal iAssessMst As clsevaluation_analysis_master, ByVal iHeaderId As Integer, _
                                  ByVal iHeaderName As String, ByRef iEvalData As DataTable, _
                                  ByVal eAction As enAction, ByVal iEmployeeId As Integer, _
                                  ByVal iMultipleVal As Boolean, ByVal iGUID As String, ByVal iAssess As enAssessmentMode, ByVal dtAssessDate As Date) As Boolean 'SHANI [21 MAR 2015]-[dtAssessDate]
        Try
            mintAnalysisUnkid = iAnalysisId
            mintPeriodId = iPeriodId
            objCAssessMst = iAssessMst
            mintHeaderId = iHeaderId
            mstrHeaderName = iHeaderName
            mintEmployeeId = iEmployeeId
            mdtCustEvaluation = iEvalData
            mblnIsMultiple = iMultipleVal
            iEditingGUID = iGUID
            menAssess = iAssess
            menAction = eAction

            'SHANI [21 Mar 2015]-START
            'Issue : Fixing Issues Sent By Aandrew in PA Testing.
            mdtAssessDate = dtAssessDate
            'SHANI [21 Mar 2015]--END 

            objlblCustomHeader.Text = iHeaderName

            Me.ShowDialog()

            iEvalData = mdtCustEvaluation
            Return Not mblnCancel
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "displayDialog", mstrModuleName)
        Finally
        End Try
    End Function

#End Region

#Region " Private Methods "

    Private Sub Fill_Items()
        Try
            RemoveHandler dgvItems.CellLeave, AddressOf dgvItems_CellLeave
            dtCItems = objCAssessMst.Get_CItems_ForAddEdit(mintPeriodId, mintHeaderId, menAssess)

            'S.SANDEEP [12 OCT 2016] -- START
            dtCItems = dtCItems.Select("", "isdefaultentry DESC").CopyToDataTable
            'S.SANDEEP [12 OCT 2016] -- END


            If menAction = enAction.EDIT_ONE AndAlso iEditingGUID <> "" Then
                Dim dtmp() As DataRow = mdtCustEvaluation.Select("customanalysistranguid = '" & iEditingGUID & "' AND AUD <> 'D'")
                If dtmp.Length > 0 Then
                    For iEdit As Integer = 0 To dtmp.Length - 1
                        Dim xRow() As DataRow = dtCItems.Select("customitemunkid = '" & dtmp(iEdit).Item("customitemunkid") & "'")
                        If xRow.Length > 0 Then
                            Select Case CInt(dtmp(iEdit).Item("itemtypeid"))
                                Case clsassess_custom_items.enCustomType.SELECTION
                                    Select Case CInt(dtmp(iEdit).Item("selectionmodeid"))
                                        'S.SANDEEP [04 OCT 2016] -- START
                                        Case clsassess_custom_items.enSelectionMode.TRAINING_OBJECTIVE, clsassess_custom_items.enSelectionMode.JOB_CAPABILITIES_COURSES, clsassess_custom_items.enSelectionMode.CAREER_DEVELOPMENT_COURSES
                                            'Case clsassess_custom_items.enSelectionMode.TRAINING_OBJECTIVE
                                            'S.SANDEEP [04 OCT 2016] -- END
                                            xRow(0).Item("selectedid") = dtmp(iEdit).Item("custom_value")
                                        Case clsassess_custom_items.enSelectionMode.EMPLOYEE_COMPETENCIES
                                            xRow(0).Item("selectedid") = dtmp(iEdit).Item("custom_value")
                                        Case clsassess_custom_items.enSelectionMode.EMPLOYEE_GOALS
                                            xRow(0).Item("selectedid") = dtmp(iEdit).Item("custom_value")
                                            'S.SANDEEP |16-AUG-2019| -- START
                                            'ISSUE/ENHANCEMENT : {ARUTI-877|Ref#0003997}
                                        Case clsassess_custom_items.enSelectionMode.PERFORMANCE_CUSTOM_ITEM
                                            xRow(0).Item("selectedid") = dtmp(iEdit).Item("custom_value")
                                            'S.SANDEEP |16-AUG-2019| -- END

                                    End Select
                                Case clsassess_custom_items.enCustomType.DATE_SELECTION
                                    If dtmp(iEdit).Item("custom_value").ToString.Trim.Length > 0 Then
                                        xRow(0).Item("ddate") = eZeeDate.convertDate(dtmp(iEdit).Item("custom_value"))
                                    End If
                            End Select
                            xRow(0).Item("custom_value") = dtmp(iEdit).Item("dispaly_value")
                        End If
                    Next
                End If
                'Shani (26-Sep-2016) -- Start
                'Enhancement -Changes PA Flow And Add New screen Given By (Andrew)
            Else
                Dim dtmp() As DataRow = mdtCustEvaluation.Select("customanalysistranguid = '" & iEditingGUID & "' AND AUD <> 'D' AND isdefaultentry = 1 ")
                If dtmp.Length > 0 Then
                    For iEdit As Integer = 0 To dtmp.Length - 1
                        Dim xRow() As DataRow = dtCItems.Select("customitemunkid = '" & dtmp(iEdit).Item("customitemunkid") & "'")
                        If xRow.Length > 0 Then
                            xRow(0).Item("custom_value") = dtmp(iEdit).Item("dispaly_value")
                        End If
                    Next
                End If
                'Shani (26-Sep-2016) -- End
            End If

            dgvItems.AutoGenerateColumns = False
            dgcolhItems.DataPropertyName = "custom_item"
            dgcolhValue.DataPropertyName = "custom_value"
            objdgcolhItemTypeId.DataPropertyName = "itemtypeid"
            objdgcolhSelectedId.DataPropertyName = "selectedid"
            objdgcolhSelectionModeId.DataPropertyName = "selectionmodeid"
            objdgcolhdate.DataPropertyName = "ddate"
            objdgcolhReadOnly.DataPropertyName = "rOnly"

            'Shani (26-Sep-2016) -- Start
            'Enhancement -Changes PA Flow And Add New screen Given By (Andrew)
            objdgcolhIsSystemEntry.DataPropertyName = "isdefaultentry"
            'Shani (26-Sep-2016) -- End

            'S.SANDEEP |08-JAN-2019| -- START
            objdgcolhiscompletedtraining.DataPropertyName = "iscompletedtraining"
            'S.SANDEEP |08-JAN-2019| -- END


            dgvItems.DataSource = dtCItems
            mDefaultHieght = dgvItems.Rows(0).Height
            'For Each dgRow As DataGridViewRow In dgvItems.Rows
            '    dgRow.Height = (mDefaultHieght * 2)
            'Next
            AddHandler dgvItems.CellLeave, AddressOf dgvItems_CellLeave
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Fill_Items", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

#Region " Form's Events "

    Private Sub frmAddCustomValue_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            Call Set_Logo(Me, gApplicationType)
            Language.setLanguage(Me.Name)
            Call OtherSettings()
            Call Fill_Items()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmAddCustomValue_Load", mstrModuleName)
        Finally
        End Try
    End Sub

    'S.SANDEEP |25-MAR-2019| -- START
    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub
    'S.SANDEEP |25-MAR-2019| -- END

#End Region

#Region " Button's Events "

    Private Sub btnAdd_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAdd.Click
        Try
            Dim dtmp() As DataRow = Nothing
            If dtCItems IsNot Nothing Then
                dtCItems.AcceptChanges()

                'Shani(24-Feb-2016) -- Start
                'dtmp = dtCItems.Select("custom_value <> ''")
                'If dtmp.Length <= 0 Then
                '   eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please enter value for atleast one custom item in order to add."), enMsgBoxStyle.Information)
                '   Exit Sub
                'End If
                dtmp = dtCItems.Select("custom_value = '' AND rOnly = FALSE ")
                Dim strMsg As String = ""

                If dtmp.Length > 0 Then
                    For Each dRow As DataRow In dtmp
                        strMsg &= vbCrLf & "* " & dRow.Item("custom_item").ToString
                    Next
                End If

                If strMsg.Trim.Length > 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Sorry, Please enter all mandatory information mentioned below in order to save.") & strMsg, enMsgBoxStyle.Information)
                    Exit Sub
                End If

                'Shani (23-Nov-2016) -- Start
                'Enhancement - Add New PA Enhancement (Agreed Score,exc) Given by Akiba,KBC,TNP exc...
                dtmp = dtCItems.Select("rOnly = FALSE ")
                If dtmp.Length <= 0 Then
                    Exit Sub
                End If
                'Shani (23-Nov123-2016-2016) -- End

                dtmp = dtCItems.Select("")
            End If

            'Shani (23-Nov-2016) -- Start
            'Enhancement - Add New PA Enhancement (Agreed Score,exc) Given by Akiba,KBC,TNP exc...
            'If dtmp.Length > 0 Then
            If dtmp IsNot Nothing AndAlso dtmp.Length > 0 Then
                'Shani (23-Nov123-2016-2016) -- End
                If menAction <> enAction.EDIT_ONE Then
                    Dim iGUID As String = ""
                    'Shani (26-Sep-2016) -- Start
                    'Enhancement -Changes PA Flow And Add New screen Given By (Andrew)
                    'If iEditingGUID.Trim.Length > 0 Then
                    '    iGUID = iEditingGUID
                    'Else
                    '    iGUID = Guid.NewGuid.ToString
                    'End If
                        iGUID = Guid.NewGuid.ToString
                    'Shani (26-Sep-2016) -- End
                    For iR As Integer = 0 To dtmp.Length - 1
                        Dim dRow As DataRow = mdtCustEvaluation.NewRow
                        dRow.Item("customanalysistranguid") = iGUID
                        dRow.Item("analysisunkid") = mintAnalysisUnkid
                        dRow.Item("customitemunkid") = dtmp(iR).Item("customitemunkid")
                        dRow.Item("periodunkid") = mintPeriodId
                        Select Case CInt(dtmp(iR).Item("itemtypeid"))
                            Case clsassess_custom_items.enCustomType.FREE_TEXT
                                dRow.Item("custom_value") = dtmp(iR).Item("custom_value")
                            Case clsassess_custom_items.enCustomType.SELECTION
                                dRow.Item("custom_value") = dtmp(iR).Item("selectedid")
                            Case clsassess_custom_items.enCustomType.DATE_SELECTION
                                If dtmp(iR).Item("ddate").ToString.Trim.Length > 0 Then
                                    dRow.Item("custom_value") = eZeeDate.convertDate(dtmp(iR).Item("ddate"))
                                Else
                                    dRow.Item("custom_value") = ""
                                End If
                            Case clsassess_custom_items.enCustomType.NUMERIC_DATA
                                dRow.Item("custom_value") = dtmp(iR).Item("custom_value")
                        End Select
                        dRow.Item("custom_header") = mstrHeaderName
                        dRow.Item("custom_item") = dtmp(iR).Item("custom_item")
                        dRow.Item("dispaly_value") = dtmp(iR).Item("custom_value")
                        dRow.Item("AUD") = "A"
                        dRow.Item("itemtypeid") = dtmp(iR).Item("itemtypeid")
                        dRow.Item("selectionmodeid") = dtmp(iR).Item("selectionmodeid")
                        dRow.Item("ismanual") = True
                        mdtCustEvaluation.Rows.Add(dRow)
                    Next
                Else
                    If dtmp.Length > 0 Then
                        For iEdit As Integer = 0 To dtmp.Length - 1
                            Dim xRow() As DataRow = mdtCustEvaluation.Select("customitemunkid = '" & dtmp(iEdit).Item("customitemunkid") & "' AND customanalysistranguid = '" & iEditingGUID & "' AND AUD <> 'D'")
                            If xRow.Length > 0 Then
                                xRow(0).Item("customanalysistranguid") = xRow(0).Item("customanalysistranguid")
                                xRow(0).Item("analysisunkid") = xRow(0).Item("analysisunkid")
                                xRow(0).Item("customitemunkid") = dtmp(iEdit).Item("customitemunkid")
                                xRow(0).Item("periodunkid") = mintPeriodId
                                Select Case CInt(dtmp(iEdit).Item("itemtypeid"))
                                    Case clsassess_custom_items.enCustomType.FREE_TEXT
                                        xRow(0).Item("custom_value") = dtmp(iEdit).Item("custom_value")
                                    Case clsassess_custom_items.enCustomType.SELECTION
                                        xRow(0).Item("custom_value") = dtmp(iEdit).Item("selectedid")
                                    Case clsassess_custom_items.enCustomType.DATE_SELECTION
                                        If dtmp(iEdit).Item("ddate").ToString.Trim.Length > 0 Then
                                            xRow(0).Item("custom_value") = eZeeDate.convertDate(dtmp(iEdit).Item("ddate"))
                                        Else
                                            xRow(0).Item("custom_value") = ""
                                        End If
                                    Case clsassess_custom_items.enCustomType.NUMERIC_DATA
                                        xRow(0).Item("custom_value") = dtmp(iEdit).Item("custom_value")
                                End Select
                                xRow(0).Item("custom_header") = mstrHeaderName
                                xRow(0).Item("custom_item") = dtmp(iEdit).Item("custom_item")
                                xRow(0).Item("dispaly_value") = dtmp(iEdit).Item("custom_value")
                                'S.SANDEEP [29 DEC 2015] -- START
                                'xRow(0).Item("AUD") = "U"
                                If xRow(0).Item("analysisunkid") <= 0 Then
                                    xRow(0).Item("AUD") = "A"
                                Else
                                xRow(0).Item("AUD") = "U"
                                End If
                                'S.SANDEEP [29 DEC 2015] -- END
                                xRow(0).Item("itemtypeid") = dtmp(iEdit).Item("itemtypeid")
                                xRow(0).Item("selectionmodeid") = dtmp(iEdit).Item("selectionmodeid")

                                'Shani (26-Sep-2016) -- Start
                                'Enhancement -Changes PA Flow And Add New screen Given By (Andrew)
                            Else
                                Dim dtRow As DataRow = mdtCustEvaluation.NewRow
                                dtRow.Item("customanalysistranguid") = iEditingGUID
                                dtRow.Item("analysisunkid") = mintAnalysisUnkid
                                dtRow.Item("customitemunkid") = dtmp(iEdit).Item("customitemunkid")
                                dtRow.Item("periodunkid") = mintPeriodId
                                Select Case CInt(dtmp(iEdit).Item("itemtypeid"))
                                    Case clsassess_custom_items.enCustomType.FREE_TEXT
                                        dtRow.Item("custom_value") = dtmp(iEdit).Item("custom_value")
                                    Case clsassess_custom_items.enCustomType.SELECTION
                                        dtRow.Item("custom_value") = dtmp(iEdit).Item("selectedid")
                                    Case clsassess_custom_items.enCustomType.DATE_SELECTION
                                        If dtmp(iEdit).Item("ddate").ToString.Trim.Length > 0 Then
                                            dtRow.Item("custom_value") = eZeeDate.convertDate(dtmp(iEdit).Item("ddate"))
                                        Else
                                            dtRow.Item("custom_value") = ""
                                        End If
                                    Case clsassess_custom_items.enCustomType.NUMERIC_DATA
                                        dtRow.Item("custom_value") = dtmp(iEdit).Item("custom_value")
                                End Select
                                dtRow.Item("custom_header") = mstrHeaderName
                                dtRow.Item("custom_item") = dtmp(iEdit).Item("custom_item")
                                dtRow.Item("dispaly_value") = dtmp(iEdit).Item("custom_value")
                                dtRow.Item("AUD") = "A"
                                dtRow.Item("itemtypeid") = dtmp(iEdit).Item("itemtypeid")
                                dtRow.Item("selectionmodeid") = dtmp(iEdit).Item("selectionmodeid")
                                Dim iRow() = mdtCustEvaluation.Select("customanalysistranguid = '" & iEditingGUID & "' ")
                                dtRow.Item("ismanual") = iRow(0)("ismanual")
                                mdtCustEvaluation.Rows.InsertAt(dtRow, mdtCustEvaluation.Rows.IndexOf(iRow(iRow.Length - 1)) + 1)
                            End If
                        Next
                    End If
                End If
            End If
            If mblnIsMultiple = True AndAlso menAction <> enAction.EDIT_ONE Then
                Call Fill_Items()
            Else
                Call btnClose_Click(sender, e)
            End If
            Call Fill_Items()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnAdd_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            mblnCancel = False
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnClose_Click", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

#Region " Control's Events "

    Private Sub dgvItems_CellEnter(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvItems.CellEnter
        Try
            If e.RowIndex = -1 Then Exit Sub
            If e.ColumnIndex = dgvItems.Rows(e.RowIndex).Cells(dgcolhValue.Index).ColumnIndex Then

                'Shani (26-Sep-2016) -- Start
                'Enhancement -Changes PA Flow And Add New screen Given By (Andrew)
                'If CBool(dgvItems.Rows(e.RowIndex).Cells(objdgcolhReadOnly.Index).Value) = True Then Exit Sub
                If CBool(dgvItems.Rows(e.RowIndex).Cells(objdgcolhReadOnly.Index).Value) = True OrElse CBool(dgvItems.Rows(e.RowIndex).Cells(objdgcolhIsSystemEntry.Index).Value) = True Then
                    dgvItems.Rows(e.RowIndex).Height = 40
                    Exit Sub
                End If
                'Shani (26-Sep-2016) -- End

                RemoveHandler dgvItems.CellEnter, AddressOf dgvItems_CellEnter
                Dim iLoc As Point
                Select Case CInt(dgvItems.Rows(e.RowIndex).Cells(objdgcolhItemTypeId.Index).Value)
                    Case clsassess_custom_items.enCustomType.FREE_TEXT
                        iTextCtrl = New System.Windows.Forms.TextBox
                        iTextCtrl.AcceptsReturn = True : iTextCtrl.AcceptsTab = True
                        iTextCtrl.Multiline = True : iTextCtrl.Height = 40 : iTextCtrl.ScrollBars = ScrollBars.Vertical
                        iTextCtrl.Width = dgvItems.Rows(e.RowIndex).Cells(dgcolhValue.Index).Size.Width
                        dgvItems.Rows(e.RowIndex).Height = iTextCtrl.Height
                        'iTextCtrl.Size = dgvItems.Rows(e.RowIndex).Cells(dgcolhValue.Index).Size
                        iLoc = dgvItems.GetCellDisplayRectangle(dgcolhValue.Index, e.RowIndex, False).Location
                        iTextCtrl.Location = iLoc
                        If Me.dgvItems.Controls.Contains(iTextCtrl) = False Then
                            Me.dgvItems.Controls.Add(iTextCtrl)

                            iTextCtrl.Visible = True
                            iTextCtrl.Select()
                            iTextCtrl.Focus()
                        End If

                        RemoveHandler iTextCtrl.TextChanged, AddressOf iTextCtrl_TextChanged
                        If dgvItems.Rows(e.RowIndex).Cells(dgcolhValue.Index).Value <> "" Then
                            iTextCtrl.Text = dgvItems.Rows(e.RowIndex).Cells(dgcolhValue.Index).Value
                        End If
                        AddHandler iTextCtrl.TextChanged, AddressOf iTextCtrl_TextChanged
                    Case clsassess_custom_items.enCustomType.SELECTION
                        iCombCtrl = New System.Windows.Forms.ComboBox
                        iCombCtrl.DropDownStyle = ComboBoxStyle.DropDown
                        iCombCtrl.Size = dgvItems.Rows(e.RowIndex).Cells(dgcolhValue.Index).Size
                        iLoc = dgvItems.GetCellDisplayRectangle(dgcolhValue.Index, e.RowIndex, False).Location
                        iCombCtrl.Location = iLoc
                        Me.dgvItems.Controls.Add(iCombCtrl) : iCombCtrl.Visible = True
                        iCombCtrl.Select()
                        iCombCtrl.DataSource = Nothing
                        'With iCombCtrl
                        '    .DropDownStyle = ComboBoxStyle.DropDownList
                        '    .DropDownWidth = 300
                        'End With
                        Select Case CInt(dgvItems.Rows(e.RowIndex).Cells(objdgcolhSelectionModeId.Index).Value)
                            Case clsassess_custom_items.enSelectionMode.TRAINING_OBJECTIVE
                                'S.SANDEEP |08-JAN-2019| -- START
                                'Dim objCMaster As New clsCommon_Master
                                'Dim dsList As New DataSet
                                'dsList = objCMaster.getComboList(clsCommon_Master.enCommonMaster.TRAINING_COURSEMASTER, True, "List")
                                'With iCombCtrl
                                '    .ValueMember = "masterunkid"
                                '    .DisplayMember = "name"
                                '    .DataSource = dsList.Tables(0)
                                '    .SelectedValue = 0
                                '    .AutoCompleteMode = AutoCompleteMode.Suggest
                                '    .AutoCompleteSource = AutoCompleteSource.ListItems
                                'End With
                                'objCMaster = Nothing
                                Dim dsList As New DataSet
                                If CBool(dgvItems.Rows(e.RowIndex).Cells(objdgcolhiscompletedtraining.Index).Value) = False Then
                                Dim objCMaster As New clsCommon_Master
                                dsList = objCMaster.getComboList(clsCommon_Master.enCommonMaster.TRAINING_COURSEMASTER, True, "List")
                                    objCMaster = Nothing
                                Else
                                    Dim objEvalCItem As New clsevaluation_analysis_master
                                    dsList = objEvalCItem.GetCompletedTrainingListForCustomItem(mintEmployeeId)
                                    objEvalCItem = Nothing
                                End If
                                With iCombCtrl
                                    .ValueMember = "masterunkid"
                                    .DisplayMember = "name"
                                    .DataSource = dsList.Tables(0)
                                    .SelectedValue = 0
                                    .AutoCompleteMode = AutoCompleteMode.Suggest
                                    .AutoCompleteSource = AutoCompleteSource.ListItems
                                End With
                                'S.SANDEEP |08-JAN-2019| -- END

                                'S.SANDEEP [04 OCT 2016] -- START
                            Case clsassess_custom_items.enSelectionMode.JOB_CAPABILITIES_COURSES, clsassess_custom_items.enSelectionMode.CAREER_DEVELOPMENT_COURSES
                                'S.SANDEEP |08-JAN-2019| -- START
                                'Dim objCMaster As New clsCommon_Master
                                'Dim dsList As New DataSet
                                'dsList = objCMaster.getComboList(clsCommon_Master.enCommonMaster.TRAINING_COURSEMASTER, True, "List")
                                'Dim dtab As DataTable = Nothing
                                'If CInt(dgvItems.Rows(e.RowIndex).Cells(objdgcolhSelectionModeId.Index).Value) = clsassess_custom_items.enSelectionMode.JOB_CAPABILITIES_COURSES Then
                                '    dtab = New DataView(dsList.Tables(0), "coursetypeid IN (99,1)", "", DataViewRowState.CurrentRows).ToTable
                                'ElseIf CInt(dgvItems.Rows(e.RowIndex).Cells(objdgcolhSelectionModeId.Index).Value) = clsassess_custom_items.enSelectionMode.CAREER_DEVELOPMENT_COURSES Then
                                '    dtab = New DataView(dsList.Tables(0), "coursetypeid IN (99,2)", "", DataViewRowState.CurrentRows).ToTable
                                'End If
                                'With iCombCtrl
                                '    .ValueMember = "masterunkid"
                                '    .DisplayMember = "name"
                                '    .DataSource = dtab
                                '    .SelectedValue = 0
                                '    .AutoCompleteMode = AutoCompleteMode.Suggest
                                '    .AutoCompleteSource = AutoCompleteSource.ListItems
                                'End With
                                'objCMaster = Nothing

                                Dim dsList As New DataSet
                                Dim dtab As DataTable = Nothing

                                If CBool(dgvItems.Rows(e.RowIndex).Cells(objdgcolhiscompletedtraining.Index).Value) = False Then
                                Dim objCMaster As New clsCommon_Master
                                dsList = objCMaster.getComboList(clsCommon_Master.enCommonMaster.TRAINING_COURSEMASTER, True, "List")
                                    objCMaster = Nothing
                                Else
                                    Dim objEvalCItem As New clsevaluation_analysis_master
                                    dsList = objEvalCItem.GetCompletedTrainingListForCustomItem(mintEmployeeId)
                                    objEvalCItem = Nothing
                                End If

                                If CInt(dgvItems.Rows(e.RowIndex).Cells(objdgcolhSelectionModeId.Index).Value) = clsassess_custom_items.enSelectionMode.JOB_CAPABILITIES_COURSES Then
                                    dtab = New DataView(dsList.Tables(0), "coursetypeid IN (99,1)", "", DataViewRowState.CurrentRows).ToTable
                                ElseIf CInt(dgvItems.Rows(e.RowIndex).Cells(objdgcolhSelectionModeId.Index).Value) = clsassess_custom_items.enSelectionMode.CAREER_DEVELOPMENT_COURSES Then
                                    dtab = New DataView(dsList.Tables(0), "coursetypeid IN (99,2)", "", DataViewRowState.CurrentRows).ToTable
                                End If
                                With iCombCtrl
                                    .ValueMember = "masterunkid"
                                    .DisplayMember = "name"
                                    .DataSource = dtab
                                    .SelectedValue = 0
                                    .AutoCompleteMode = AutoCompleteMode.Suggest
                                    .AutoCompleteSource = AutoCompleteSource.ListItems
                                End With
                                'S.SANDEEP |08-JAN-2019| -- END


                                'S.SANDEEP [04 OCT 2016] -- END
                            Case clsassess_custom_items.enSelectionMode.EMPLOYEE_COMPETENCIES
                                Dim objCompetency As New clsassess_competencies_master
                                Dim dsList As New DataSet
                                'S.SANDEEP [29 JAN 2015] -- START
                                'dsList = objCompetency.getAssigned_Competencies_List(mintEmployeeId, mintPeriodId, True)

                                'S.SANDEEP [04 JUN 2015] -- START
                                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                                'dsList = objCompetency.getAssigned_Competencies_List(mintEmployeeId, mintPeriodId, True, ConfigParameter._Object._Self_Assign_Competencies)
                                dsList = objCompetency.getAssigned_Competencies_List(mintEmployeeId, mintPeriodId, eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), True, ConfigParameter._Object._Self_Assign_Competencies)
                                'S.SANDEEP [04 JUN 2015] -- END

                                'S.SANDEEP [29 JAN 2015] -- END
                                With iCombCtrl
                                    .ValueMember = "Id"
                                    .DisplayMember = "Name"
                                    .DataSource = dsList.Tables(0)
                                    .SelectedValue = 0
                                    .AutoCompleteMode = AutoCompleteMode.Suggest
                                    .AutoCompleteSource = AutoCompleteSource.ListItems
                                End With
                                objCompetency = Nothing
                            Case clsassess_custom_items.enSelectionMode.EMPLOYEE_GOALS
                                Dim objEmpField1 As New clsassess_empfield1_master
                                Dim dsList As New DataSet
                                dsList = objEmpField1.getComboList(mintEmployeeId, mintPeriodId, "List", True, True)
                                With iCombCtrl
                                    .ValueMember = "Id"
                                    .DisplayMember = "Name"
                                    .DataSource = dsList.Tables(0)
                                    .SelectedValue = 0
                                    .AutoCompleteMode = AutoCompleteMode.Suggest
                                    .AutoCompleteSource = AutoCompleteSource.ListItems
                                End With
                                objEmpField1 = Nothing


                                'S.SANDEEP |16-AUG-2019| -- START
                                'ISSUE/ENHANCEMENT : {ARUTI-877|Ref#0003997}
                            Case clsassess_custom_items.enSelectionMode.PERFORMANCE_CUSTOM_ITEM
                                Dim dsList As New DataSet
                                If CBool(dgvItems.Rows(e.RowIndex).Cells(objdgcolhiscompletedtraining.Index).Value) = False Then
                                    Dim objCMaster As New clsCommon_Master
                                    dsList = objCMaster.getComboList(clsCommon_Master.enCommonMaster.PERFORMANCE_CUSTOM_ITEM, True, "List")
                                    objCMaster = Nothing
                                Else
                                    Dim objEvalCItem As New clsevaluation_analysis_master
                                    dsList = objEvalCItem.GetCompletedTrainingListForCustomItem(mintEmployeeId)
                                    objEvalCItem = Nothing
                                End If
                                With iCombCtrl
                                    .ValueMember = "masterunkid"
                                    .DisplayMember = "name"
                                    .DataSource = dsList.Tables(0)
                                    .SelectedValue = 0
                                    .AutoCompleteMode = AutoCompleteMode.Suggest
                                    .AutoCompleteSource = AutoCompleteSource.ListItems
                                End With
                                'S.SANDEEP |16-AUG-2019| -- END

                        End Select
                        RemoveHandler iCombCtrl.SelectedIndexChanged, AddressOf iCombCtrl_SelectedIndexChanged
                        'S.SANDEEP |16-AUG-2019| -- START
                        RemoveHandler iCombCtrl.DrawItem, AddressOf iCombCtrl_DrawItem
                        RemoveHandler iCombCtrl.DropDownClosed, AddressOf iCombCtrl_DropDownClosed
                        RemoveHandler iCombCtrl.Leave, AddressOf iCombCtrl_Leave
                        'S.SANDEEP |16-AUG-2019| -- END

                        If dgvItems.Rows(e.RowIndex).Cells(dgcolhValue.Index).Value <> "" Then
                            iCombCtrl.SelectedValue = dgvItems.Rows(e.RowIndex).Cells(objdgcolhSelectedId.Index).Value
                        End If

                        AddHandler iCombCtrl.SelectedIndexChanged, AddressOf iCombCtrl_SelectedIndexChanged
                        'S.SANDEEP |16-AUG-2019| -- START
                        iCombCtrl.DrawMode = DrawMode.OwnerDrawFixed
                        AddHandler iCombCtrl.DrawItem, AddressOf iCombCtrl_DrawItem
                        AddHandler iCombCtrl.DropDownClosed, AddressOf iCombCtrl_DropDownClosed
                        AddHandler iCombCtrl.Leave, AddressOf iCombCtrl_Leave
                        'S.SANDEEP |16-AUG-2019| -- END
                    Case clsassess_custom_items.enCustomType.DATE_SELECTION
                        iDateCtrl = New System.Windows.Forms.MonthCalendar
                        With iDateCtrl
                            .SetDate(ConfigParameter._Object._CurrentDateAndTime)
                        End With
                        iDateCtrl.Size = dgvItems.Rows(e.RowIndex).Cells(dgcolhValue.Index).Size
                        iLoc = dgvItems.GetCellDisplayRectangle(dgcolhValue.Index, e.RowIndex, False).Location

                        If (dgvItems.Bottom - 10) > iLoc.Y AndAlso iLoc.Y > 200 Then
                            iLoc = (New Point(iLoc.X, iLoc.Y - 150))
                        Else
                            iLoc = (New Point(iLoc.X, iLoc.Y))
                        End If

                        iDateCtrl.Location = iLoc
                        Me.dgvItems.Controls.Add(iDateCtrl) : iDateCtrl.Visible = True
                        iDateCtrl.Select()

                        RemoveHandler iDateCtrl.DateSelected, AddressOf iDateCtrl_DateSelected
                        If dgvItems.Rows(e.RowIndex).Cells(dgcolhValue.Index).Value <> "" Then
                            iDateCtrl.SetDate(dgvItems.Rows(e.RowIndex).Cells(objdgcolhdate.Index).Value)
                        End If
                        AddHandler iDateCtrl.DateSelected, AddressOf iDateCtrl_DateSelected

                    Case clsassess_custom_items.enCustomType.NUMERIC_DATA
                        iNumrCtrl = New eZee.TextBox.IntegerTextBox
                        With iNumrCtrl
                            .AllowNegative = False
                            .MaxDecimalPlaces = 2
                            .TextAlign = HorizontalAlignment.Right
                        End With
                        iNumrCtrl.Size = dgvItems.Rows(e.RowIndex).Cells(dgcolhValue.Index).Size
                        iLoc = dgvItems.GetCellDisplayRectangle(dgcolhValue.Index, e.RowIndex, False).Location
                        iNumrCtrl.Location = iLoc
                        Me.dgvItems.Controls.Add(iNumrCtrl) : iNumrCtrl.Visible = True
                        iNumrCtrl.Select()
                        RemoveHandler iNumrCtrl.TextChanged, AddressOf iNumrCtrl_TextChanged
                        If dgvItems.Rows(e.RowIndex).Cells(dgcolhValue.Index).Value <> "" Then
                            iNumrCtrl.Text = dgvItems.Rows(e.RowIndex).Cells(dgcolhValue.Index).Value
                        End If
                        AddHandler iNumrCtrl.TextChanged, AddressOf iNumrCtrl_TextChanged
                End Select
                AddHandler dgvItems.CellEnter, AddressOf dgvItems_CellEnter
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgvItems_CellEnter", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub dgvItems_CellLeave(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvItems.CellLeave
        Try
            If e.ColumnIndex = dgvItems.Rows(e.RowIndex).Cells(dgcolhValue.Index).ColumnIndex Then
                Select Case CInt(dgvItems.Rows(e.RowIndex).Cells(objdgcolhItemTypeId.Index).Value)
                    Case clsassess_custom_items.enCustomType.FREE_TEXT
                        If iTextCtrl IsNot Nothing Then
                            RemoveHandler iTextCtrl.TextChanged, AddressOf iTextCtrl_TextChanged
                            'If dgvItems.Rows(e.RowIndex).Cells(dgcolhValue.Index).Value.ToString.Trim.Length <= 0 Then dgvItems.Rows(e.RowIndex).Height = mDefaultHieght
                            iTextCtrl.Text = "" : iTextCtrl.Visible = False : dgvItems.Controls.Remove(iTextCtrl)
                            AddHandler iTextCtrl.TextChanged, AddressOf iTextCtrl_TextChanged
                        End If
                    Case clsassess_custom_items.enCustomType.SELECTION
                        If iCombCtrl IsNot Nothing Then
                            RemoveHandler iCombCtrl.SelectedIndexChanged, AddressOf iCombCtrl_SelectedIndexChanged
                            iCombCtrl.SelectedValue = 0 : iCombCtrl.Visible = False : dgvItems.Controls.Remove(iCombCtrl)
                            AddHandler iCombCtrl.SelectedIndexChanged, AddressOf iCombCtrl_SelectedIndexChanged
                        End If
                    Case clsassess_custom_items.enCustomType.DATE_SELECTION
                        If iDateCtrl IsNot Nothing Then
                            'RemoveHandler iDateCtrl.ValueChanged, AddressOf iDateCtrl_ValueChanged

                            RemoveHandler iDateCtrl.DateSelected, AddressOf iDateCtrl_DateSelected
                            iDateCtrl.SetDate(ConfigParameter._Object._CurrentDateAndTime) : iDateCtrl.Visible = False : dgvItems.Controls.Remove(iDateCtrl)
                            AddHandler iDateCtrl.DateSelected, AddressOf iDateCtrl_DateSelected

                            'AddHandler iDateCtrl.ValueChanged, AddressOf iDateCtrl_ValueChanged
                        End If
                    Case clsassess_custom_items.enCustomType.NUMERIC_DATA
                        If iNumrCtrl IsNot Nothing Then
                            RemoveHandler iNumrCtrl.TextChanged, AddressOf iNumrCtrl_TextChanged
                            iNumrCtrl.Text = "" : iNumrCtrl.Visible = False : dgvItems.Controls.Remove(iNumrCtrl)
                            AddHandler iNumrCtrl.TextChanged, AddressOf iNumrCtrl_TextChanged
                        End If
                End Select
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgvItems_CellLeave", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub iTextCtrl_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Try
            dgvItems.CurrentRow.Cells(dgcolhValue.Index).Value = iTextCtrl.Text
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "iTextCtrl_TextChanged", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub iCombCtrl_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Try
            If CInt(iCombCtrl.SelectedValue) > 0 Then
                dgvItems.CurrentRow.Cells(dgcolhValue.Index).Value = iCombCtrl.Text
                dgvItems.CurrentRow.Cells(objdgcolhSelectedId.Index).Value = iCombCtrl.SelectedValue
            Else
                dgvItems.CurrentRow.Cells(dgcolhValue.Index).Value = ""
                dgvItems.CurrentRow.Cells(objdgcolhSelectedId.Index).Value = 0
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "iCombCtrl_SelectedIndexChanged", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub iDateCtrl_DateSelected(ByVal sender As Object, ByVal e As System.Windows.Forms.DateRangeEventArgs)
        Try

            'SHANI [21 Mar 2015]-START
            'Issue : Fixing Issues Sent By Aandrew in PA Testing.
            If mdtAssessDate >= iDateCtrl.SelectionStart.Date Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Sorry, Selected date should be greter than the assessment date selected. "), enMsgBoxStyle.Information)
                Exit Sub
            End If
            'SHANI [21 Mar 2015]--END 

            dgvItems.CurrentRow.Cells(dgcolhValue.Index).Value = iDateCtrl.SelectionStart.Date
            dgvItems.CurrentRow.Cells(objdgcolhdate.Index).Value = iDateCtrl.SelectionStart.Date
            iDateCtrl.Visible = False
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "iDateCtrl_DateSelected", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub iNumrCtrl_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Try
            dgvItems.CurrentRow.Cells(dgcolhValue.Index).Value = iNumrCtrl.Text
            dgvItems.CurrentRow.Cells(dgcolhValue.Index).Style.Alignment = DataGridViewContentAlignment.MiddleRight
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "iNumrCtrl_TextChanged", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub dgvItems_Scroll(ByVal sender As Object, ByVal e As System.Windows.Forms.ScrollEventArgs) Handles dgvItems.Scroll
        Try
            If iTextCtrl IsNot Nothing Then
                If dgvItems.Controls.Contains(iTextCtrl) AndAlso iTextCtrl.Visible = True Then
                    iTextCtrl.Visible = False
                End If
            End If
            If iDateCtrl IsNot Nothing Then
                If dgvItems.Controls.Contains(iDateCtrl) AndAlso iDateCtrl.Visible = True Then
                    iDateCtrl.Visible = False
                End If
            End If
            If iCombCtrl IsNot Nothing Then
                If dgvItems.Controls.Contains(iCombCtrl) AndAlso iCombCtrl.Visible = True Then
                    iCombCtrl.Visible = False
                End If
            End If
            If iNumrCtrl IsNot Nothing Then
                If dgvItems.Controls.Contains(iNumrCtrl) AndAlso iNumrCtrl.Visible = True Then
                    iNumrCtrl.Visible = False
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgvItems_Scroll", mstrModuleName)
        Finally
        End Try
    End Sub

    'S.SANDEEP |16-AUG-2019| -- START
    'ISSUE/ENHANCEMENT : {ARUTI-877|Ref#0003997}
    Private Sub iCombCtrl_DrawItem(ByVal sender As Object, ByVal e As DrawItemEventArgs)
        Try
            If e.Index < 0 Then
                Return
            End If

            Dim dt As DataTable = iCombCtrl.DataSource
            Dim irow As DataRow() = Nothing

            Dim ToolTiptext As String = String.Empty
            Dim text As String = iCombCtrl.GetItemText(iCombCtrl.Items(e.Index))
            e.DrawBackground()

            irow = dt.Select("name = '" & text & "' ")


            If IsNothing(irow) = False AndAlso irow.Length > 0 Then
                ToolTiptext = irow(0)("description").ToString()
            End If


            Using br As SolidBrush = New SolidBrush(e.ForeColor)
                e.Graphics.DrawString(text, e.Font, br, e.Bounds)
            End Using

            If (e.State And DrawItemState.Selected) = DrawItemState.Selected Then
                ToolTip1.Show(ToolTiptext, iCombCtrl, e.Bounds.Right, e.Bounds.Bottom)
            End If

            e.DrawFocusRectangle()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "iCombCtrl_DrawItem", mstrModuleName)
        End Try
    End Sub

    Private Sub iCombCtrl_Leave(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Try
            ToolTip1.Hide(iCombCtrl)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "iCombCtrl_Leave", mstrModuleName)
        End Try
    End Sub

    Private Sub iCombCtrl_DropDownClosed(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Try
            ToolTip1.Hide(iCombCtrl)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "iCombCtrl_DropDownClosed", mstrModuleName)
        End Try
    End Sub
    'S.SANDEEP |16-AUG-2019| -- END

#End Region

    'Language & UI Settings
	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Private Sub OtherSettings()
		Try
			Me.SuspendLayout()
			
			Call SetLanguage()
			
			Me.btnAdd.GradientBackColor = GUI._ButttonBackColor 
			Me.btnAdd.GradientForeColor = GUI._ButttonFontColor

			Me.btnClose.GradientBackColor = GUI._ButttonBackColor 
			Me.btnClose.GradientForeColor = GUI._ButttonFontColor


			Me.ResumeLayout()
		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "OtherSettings", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetLanguage()
		Try
			Me.Text = Language._Object.getCaption(Me.Name, Me.Text)
			
			Me.btnAdd.Text = Language._Object.getCaption(Me.btnAdd.Name, Me.btnAdd.Text)
			Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
			Me.dgcolhItems.HeaderText = Language._Object.getCaption(Me.dgcolhItems.Name, Me.dgcolhItems.HeaderText)
			Me.dgcolhValue.HeaderText = Language._Object.getCaption(Me.dgcolhValue.Name, Me.dgcolhValue.HeaderText)

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetLanguage", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetMessages()
		Try
	    Language.setMessage(mstrModuleName, 1, "Please enter value for atleast one custom item in order to add.")
            Language.setMessage(mstrModuleName, 2, "Sorry, Selected date should be greter than the assessment date selected.")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region '</Language>
    'Language & UI Settings
End Class