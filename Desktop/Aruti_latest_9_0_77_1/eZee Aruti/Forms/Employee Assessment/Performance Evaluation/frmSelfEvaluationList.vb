﻿'ENHANCEMENT : ANY CHANGES IN DESKTOP SHOULD ALSO BE IMPLEMENTED IN WEB WITHOUT FAIL.
Option Strict On

#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data

#End Region

Public Class frmSelfEvaluationList

#Region " Private Variables "

    Private ReadOnly mstrModuleName As String = "frmSelfEvaluationList"
    Private objEvaluation As clsevaluation_analysis_master
    Private mstrAdvanceFilter As String = String.Empty

#End Region

#Region " Private Methods "

    Private Sub FillCombo()
        Dim objYear As New clsMasterData
        Dim objPeriod As New clscommom_period_Tran
        Dim objEmployee As New clsEmployee_Master
        Dim dsFill As New DataSet
        Try
            dsFill = objYear.getComboListPAYYEAR(FinancialYear._Object._YearUnkid, FinancialYear._Object._FinancialYear_Name, Company._Object._Companyunkid, "Year", True, True)

            With cboYear
                .ValueMember = "Id"
                .DisplayMember = "name"
                .DataSource = dsFill.Tables("Year")
                .SelectedValue = 0
            End With

            dsFill = objPeriod.getListForCombo(enModuleReference.Assessment, 0, FinancialYear._Object._DatabaseName, FinancialYear._Object._Database_Start_Date, "Period", True, 0)

            With cboPeriod
                .ValueMember = "periodunkid"
                .DisplayMember = "name"
                .DataSource = dsFill.Tables("Period")
                .SelectedValue = 0
            End With

            dsFill = objEmployee.GetEmployeeList(FinancialYear._Object._DatabaseName, _
                                                 User._Object._Userunkid, _
                                                 FinancialYear._Object._YearUnkid, _
                                                 Company._Object._Companyunkid, _
                                                 eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                 eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                 ConfigParameter._Object._UserAccessModeSetting, _
                                                 True, ConfigParameter._Object._IsIncludeInactiveEmp, "List", True)

            With cboEmployee
                .ValueMember = "employeeunkid"
                .DisplayMember = "employeename"
                .DataSource = dsFill.Tables("List")
                .SelectedValue = 0
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Function UpdateScore(ByVal dr As DataRow) As Boolean
        Try
            If dr.Item("competency_value").ToString.Trim.Length > 0 Then
                For Each xVal As String In dr.Item("competency_value").ToString.Split(CChar(","))
                    If CInt(dr.Item("assessgroupunkid")) = CInt(xVal.Split(CChar("|"))(0)) Then
                        dr.Item("total_score") = CDec(xVal.Split(CChar("|"))(1)).ToString("#######################0.#0")
                    End If
                Next
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "UpdateScore", mstrModuleName)
        Finally
        End Try
        Return True
    End Function

    Private Sub FillList()
        Dim dsList As New DataSet
        Dim dtTable As DataTable
        Dim StrSearching As String = String.Empty
        Try
            If User._Object.Privilege._AllowtoViewSelfEvaluationList = True Then

                If CInt(cboPeriod.SelectedValue) <= 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 13, "Sorry, Period is mandatory information. Please select period to fill list."), enMsgBoxStyle.Information)
                    Exit Sub
                End If

                If CInt(cboEmployee.SelectedValue) > 0 Then
                    StrSearching &= "AND hrevaluation_analysis_master.selfemployeeunkid = " & CInt(cboEmployee.SelectedValue)
                End If

                If CInt(cboYear.SelectedValue) > 0 Then
                    StrSearching &= "AND cfcommon_period_tran.yearunkid = " & CInt(cboYear.SelectedValue)
                End If

                If CInt(cboPeriod.SelectedValue) > 0 Then
                    StrSearching &= "AND hrevaluation_analysis_master.periodunkid = " & CInt(cboPeriod.SelectedValue)
                End If

                If dtpAssessmentdate.Checked = True Then
                    StrSearching &= "AND CONVERT(CHAR(8),hrevaluation_analysis_master.assessmentdate,112) = '" & eZeeDate.convertDate(dtpAssessmentdate.Value) & "'"
                End If

                If chkShowCommited.CheckState = CheckState.Checked And chkShowUncommited.CheckState = CheckState.Unchecked Then
                    StrSearching &= "AND ISNULL(iscommitted,0) = 1 "
                ElseIf chkShowCommited.CheckState = CheckState.Unchecked And chkShowUncommited.CheckState = CheckState.Checked Then
                    StrSearching &= "AND ISNULL(iscommitted,0) = 0 "
                End If

                If mstrAdvanceFilter.Length > 0 Then
                    StrSearching &= "AND " & mstrAdvanceFilter
                End If

                If StrSearching.Length > 0 Then
                    StrSearching = StrSearching.Substring(3)
                End If
                'S.SANDEEP |27-MAY-2019| -- START
                'ISSUE/ENHANCEMENT : [Score Calibration Process in Performance & Assessment Module]
                'dsList = objEvaluation.GetList(FinancialYear._Object._DatabaseName, _
                '                               User._Object._Userunkid, _
                '                               FinancialYear._Object._YearUnkid, _
                '                               Company._Object._Companyunkid, _
                '                               eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                '                               eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                '                               ConfigParameter._Object._UserAccessModeSetting, True, _
                '                               ConfigParameter._Object._IsIncludeInactiveEmp, "List", _
                '                               enAssessmentMode.SELF_ASSESSMENT, CInt(cboPeriod.SelectedValue), StrSearching)
                dsList = objEvaluation.GetList(FinancialYear._Object._DatabaseName, _
                                               User._Object._Userunkid, _
                                               FinancialYear._Object._YearUnkid, _
                                               Company._Object._Companyunkid, _
                                               eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                               eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                               ConfigParameter._Object._UserAccessModeSetting, True, _
                                               ConfigParameter._Object._IsIncludeInactiveEmp, "List", _
                                               enAssessmentMode.SELF_ASSESSMENT, CInt(cboPeriod.SelectedValue), ConfigParameter._Object._IsCalibrationSettingActive, StrSearching)
                'S.SANDEEP |27-MAY-2019| -- END



                dtTable = New DataView(dsList.Tables(0), "", "EmpName,smodeid", DataViewRowState.CurrentRows).ToTable

                dtTable.AsEnumerable().ToList.ForEach(Function(x) UpdateScore(x))

                dgvSelfAssessList.AutoGenerateColumns = False

                dgcolhemp.DataPropertyName = "EmpName"
                dgcolhyear.DataPropertyName = "YearName"
                dgcolhperiod.DataPropertyName = "PName"
                dgcolhassessdate.DataPropertyName = "assessmentdate"
                dgcolhmode.DataPropertyName = "smode"
                dgcolhscore.DataPropertyName = "total_score"
                objdgcolhperiodid.DataPropertyName = "periodunkid"
                objdgcolhstatusid.DataPropertyName = "Sid"
                objdgcolhempid.DataPropertyName = "selfemployeeunkid"
                objdgcolhanalysisid.DataPropertyName = "analysisunkid"
                objdgcolhiscommitted.DataPropertyName = "iscommitted"
                objdgcolhsmodeid.DataPropertyName = "smodeid"
                objdgcolhassessgroupunkid.DataPropertyName = "assessgroupunkid"

                dgvSelfAssessList.DataSource = dtTable

            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillList", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub SetVisibility()
        Try
            'S.SANDEEP [28 MAY 2015] -- START
            'ENHANCEMENT : NEW PRIVILEGE (Performance Assessment)
            'btnNew.Enabled = User._Object.Privilege._AddEmployeeAssessment
            'btnEdit.Enabled = User._Object.Privilege._EditEmployeeAssessment
            'btnDelete.Enabled = User._Object.Privilege._DeleteEmployeeAssessment
            'btnUnlockCommit.Enabled = User._Object.Privilege._Allow_UnlockCommittedGeneralAssessment

            btnNew.Enabled = User._Object.Privilege._AllowtoAddSelfEvaluation
            btnEdit.Enabled = User._Object.Privilege._AllowtoEditSelfEvaluation
            btnDelete.Enabled = User._Object.Privilege._AllowtoDeleteSelfEvaluation
            btnUnlockCommit.Enabled = User._Object.Privilege._AllowtoUnlockcommittedSelfEvaluation
            'S.SANDEEP [28 MAY 2015] -- END

            chkShowCommited.CheckState = CheckState.Checked
            'S.SANDEEP [04 JUN 2015] -- START
            chkShowUncommited.CheckState = CheckState.Checked
            'S.SANDEEP [04 JUN 2015] -- END

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetVisibility", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Form's Events "

    Private Sub frmSelfEvaluationList_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        objEvaluation = New clsevaluation_analysis_master
        Try
            Call Set_Logo(Me, gApplicationType)
            Language.setLanguage(Me.Name)
            Call OtherSettings()
            Call SetVisibility()
            FillCombo()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmSelfEvaluationList_Load", mstrModuleName)
        End Try
    End Sub

    Private Sub frmSelfEvaluationList_KeyPress(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles MyBase.KeyPress
        Try
            If Asc(e.KeyChar) = 13 Then
                SendKeys.Send("{TAB}")
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmSelfEvaluationList_KeyPress", mstrModuleName)
        End Try
    End Sub

    Private Sub frmSelfEvaluationList_FormClosed(ByVal sender As System.Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles MyBase.FormClosed
        objEvaluation = Nothing
    End Sub

    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsevaluation_analysis_master.SetMessages()
            objfrm._Other_ModuleNames = "clsevaluation_analysis_master"
            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub

#End Region

#Region " Button's Events "

    Private Sub btnNew_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNew.Click
        Dim frm As New frmPerformanceEvaluation
        Try
            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If

            If frm.displayDialog(-1, enAction.ADD_CONTINUE, enAssessmentMode.SELF_ASSESSMENT) Then
                Call FillList()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnNew_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub btnEdit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEdit.Click
        If dgvSelfAssessList.SelectedRows.Count <= 0 Then
            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please select at least one Assessment to perform operation."), enMsgBoxStyle.Information)
            Exit Sub
        End If

        If CInt(dgvSelfAssessList.SelectedRows(0).Cells(objdgcolhstatusid.Index).Value) = enStatusType.Close Then
            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Sorry, you cannot edit this information. Reason : Period is closed."), enMsgBoxStyle.Information)
            Exit Sub
        End If

        'S.SANDEEP |25-MAR-2019| -- START
        If CBool(dgvSelfAssessList.SelectedRows(0).Cells(objdgcolhiscommitted.Index).Value) = True Then
            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 14, "Sorry, you cannot do the edit operation. Reason : Assessment is submitted."), enMsgBoxStyle.Information)
            Exit Sub
        End If
        'S.SANDEEP |25-MAR-2019| -- END

        'Shani (24-May-2016) -- Start

        'S.SANDEEP |12-FEB-2019| -- START
        'ISSUE/ENHANCEMENT : {Performance Assessment Changes}
        'If objEvaluation.CheckComputionIsExist(CInt(dgvSelfAssessList.SelectedRows(0).Cells(objdgcolhanalysisid.Index).Value), _
        '                                       enAssessmentMode.SELF_ASSESSMENT, _
        '                                       CInt(dgvSelfAssessList.SelectedRows(0).Cells(objdgcolhperiodid.Index).Value)) = True Then

        '    If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 11, "Sorry, you cannot edit General Assessment for the selected period. Reason : Compute score process is already done for the selected period. Are you sure you want to Void computation score?"), enMsgBoxStyle.OkCancel) = Windows.Forms.DialogResult.OK Then
        '        Dim frmVoid As New frmReasonSelection
        '        Dim sVoidReason As String = String.Empty
        '        If User._Object._Isrighttoleft = True Then
        '            frmVoid.RightToLeft = Windows.Forms.RightToLeft.Yes
        '            frmVoid.RightToLeftLayout = True
        '            Call Language.ctlRightToLeftlayOut(frmVoid)
        '        End If
        '        frmVoid.displayDialog(enVoidCategoryType.ASSESSMENT, sVoidReason)
        '        If sVoidReason.Trim.Length <= 0 Then Exit Sub

        '        Dim objComputeMst As New clsComputeScore_master
        '        objComputeMst._Isvoid = True
        '        objComputeMst._Voiddatetime = ConfigParameter._Object._CurrentDateAndTime
        '        objComputeMst._Voidreason = sVoidReason
        '        objComputeMst._Voiduserunkid = User._Object._Userunkid
        '        If objComputeMst.DeleteEmployeeWise(CInt(dgvSelfAssessList.SelectedRows(0).Cells(objdgcolhanalysisid.Index).Value), _
        '                                            CInt(dgvSelfAssessList.SelectedRows(0).Cells(objdgcolhempid.Index).Value), _
        '                                            CInt(dgvSelfAssessList.SelectedRows(0).Cells(objdgcolhperiodid.Index).Value), enAssessmentMode.SELF_ASSESSMENT) = True Then
        '            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 9, "Successfully voided Computation score"), enMsgBoxStyle.Information)
        '        Else
        '            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 10, "Fail void Computation score process"), enMsgBoxStyle.Information)
        '            Exit Sub
        '        End If
        '    Else
        '        Exit Sub
        '    End If
        'End If
        'S.SANDEEP |12-FEB-2019| -- END

'                Dim objComputeMst As New clsComputeScore_master
'                objComputeMst._Isvoid = True
'                objComputeMst._Voiddatetime = ConfigParameter._Object._CurrentDateAndTime
'                objComputeMst._Voidreason = sVoidReason
'                objComputeMst._Voiduserunkid = User._Object._Userunkid
'                With objComputeMst
'                    ._FormName = mstrModuleName
'                    ._LoginEmployeeunkid = 0
'                    ._ClientIP = getIP()
'                    ._HostName = getHostName()
'                    ._FromWeb = False
'                    ._AuditUserId = User._Object._Userunkid
'._CompanyUnkid = Company._Object._Companyunkid
       '             ._AuditDate = ConfigParameter._Object._CurrentDateAndTime
       '         End With
       '         If objComputeMst.DeleteEmployeeWise(CInt(dgvSelfAssessList.SelectedRows(0).Cells(objdgcolhanalysisid.Index).Value), _
       '                                             CInt(dgvSelfAssessList.SelectedRows(0).Cells(objdgcolhempid.Index).Value), _
       '                                             CInt(dgvSelfAssessList.SelectedRows(0).Cells(objdgcolhperiodid.Index).Value), enAssessmentMode.SELF_ASSESSMENT) = True Then
       '             eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 9, "Successfully voided Computation score"), enMsgBoxStyle.Information)
       '         Else
       '             eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 10, "Fail void Computation score process"), enMsgBoxStyle.Information)
       ''             Exit Sub
       '         End If
       '     Else
       '         Exit Sub
       '     End If
       ' End If
        'Shani (24-May-2016) -- End

        Dim frm As New frmPerformanceEvaluation
        Try
            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If

            If frm.displayDialog(CInt(dgvSelfAssessList.SelectedRows(0).Cells(objdgcolhanalysisid.Index).Value), enAction.EDIT_ONE, enAssessmentMode.SELF_ASSESSMENT) Then
                Call FillList()
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub btnDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDelete.Click
        If dgvSelfAssessList.SelectedRows.Count <= 0 Then
            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please select at least one Assessment to perform operation."), enMsgBoxStyle.Information)
            Exit Sub
        End If

        If CInt(dgvSelfAssessList.SelectedRows(0).Cells(objdgcolhstatusid.Index).Value) = enStatusType.Close Then
            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Sorry, you cannot delete this information. Reason : Period is closed."), enMsgBoxStyle.Information)
            Exit Sub
        End If

        'S.SANDEEP |25-MAR-2019| -- START
        If CBool(dgvSelfAssessList.SelectedRows(0).Cells(objdgcolhiscommitted.Index).Value) = True Then
            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 15, "Sorry, you cannot do the delete operation. Reason : Assessment is submitted."), enMsgBoxStyle.Information)
            Exit Sub
        End If
        'S.SANDEEP |25-MAR-2019| -- END

        Try
            'Shani (24-May-2016) -- Start

            'S.SANDEEP |12-FEB-2019| -- START
            'ISSUE/ENHANCEMENT : {Performance Assessment Changes}
            'If objEvaluation.CheckComputionIsExist(CInt(dgvSelfAssessList.SelectedRows(0).Cells(objdgcolhanalysisid.Index).Value), _
            '                                       enAssessmentMode.SELF_ASSESSMENT, _
            '                                       CInt(dgvSelfAssessList.SelectedRows(0).Cells(objdgcolhperiodid.Index).Value)) = True Then

            '    If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 12, "Sorry, you cannot delete General Assessment for the selected period. Reason : Compute score process is already done for the selected period. Are you sure you want to Void computation score?"), enMsgBoxStyle.OkCancel) = Windows.Forms.DialogResult.OK Then
            '        Dim frmVoid As New frmReasonSelection
            '        Dim sVoidReason As String = String.Empty
            '        If User._Object._Isrighttoleft = True Then
            '            frmVoid.RightToLeft = Windows.Forms.RightToLeft.Yes
            '            frmVoid.RightToLeftLayout = True
            '            Call Language.ctlRightToLeftlayOut(frmVoid)
            '        End If
            '        frmVoid.displayDialog(enVoidCategoryType.ASSESSMENT, sVoidReason)
            '        If sVoidReason.Trim.Length <= 0 Then Exit Sub

            '        Dim objComputeMst As New clsComputeScore_master
            '        objComputeMst._Isvoid = True
            '        objComputeMst._Voiddatetime = ConfigParameter._Object._CurrentDateAndTime
            '        objComputeMst._Voidreason = sVoidReason
            '        objComputeMst._Voiduserunkid = User._Object._Userunkid
            '        If objComputeMst.DeleteEmployeeWise(CInt(dgvSelfAssessList.SelectedRows(0).Cells(objdgcolhanalysisid.Index).Value), _
            '                                            CInt(dgvSelfAssessList.SelectedRows(0).Cells(objdgcolhempid.Index).Value), _
            '                                            CInt(dgvSelfAssessList.SelectedRows(0).Cells(objdgcolhperiodid.Index).Value), enAssessmentMode.SELF_ASSESSMENT) = True Then
            '            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 9, "Successfully voided Computation score"), enMsgBoxStyle.Information)
            '        Else
            '            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 10, "Fail void Computation score process"), enMsgBoxStyle.Information)
            '            Exit Sub
            '        End If
            '    Else
            '        Exit Sub
            '    End If
            'End If
            'S.SANDEEP |12-FEB-2019| -- END

            'Shani (24-May-2016) -- End

            If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Are you sure you want to delete this Performance Evaluation?"), CType(enMsgBoxStyle.YesNo + enMsgBoxStyle.Information, enMsgBoxStyle)) = Windows.Forms.DialogResult.Yes Then
                Dim frm As New frmReasonSelection
                Dim mstrVoidReason As String = String.Empty
                frm.displayDialog(enVoidCategoryType.ASSESSMENT, mstrVoidReason)
                If mstrVoidReason.Length <= 0 Then
                    Exit Sub
                Else
                    objEvaluation._Voidreason = mstrVoidReason
                End If
                frm = Nothing
                objEvaluation._Isvoid = True
                objEvaluation._Voiddatetime = ConfigParameter._Object._CurrentDateAndTime
                objEvaluation._Voiduserunkid = User._Object._Userunkid


              '  'S.SANDEEP [28-May-2018] -- START
              '  'ISSUE/ENHANCEMENT : {Audit Trails} 
                objEvaluation._FormName = mstrModuleName
                objEvaluation._LoginEmployeeunkid = 0
                objEvaluation._ClientIP = getIP()
                objEvaluation._HostName = getHostName()
                objEvaluation._FromWeb = False
                objEvaluation._AuditUserId = User._Object._Userunkid
objEvaluation._CompanyUnkid = Company._Object._Companyunkid
                objEvaluation._AuditDate = ConfigParameter._Object._CurrentDateAndTime
                'S.SANDEEP [28-May-2018] -- END

                objEvaluation.Delete(CInt(dgvSelfAssessList.SelectedRows(0).Cells(objdgcolhanalysisid.Index).Value), enAssessmentMode.SELF_ASSESSMENT, CInt(dgvSelfAssessList.SelectedRows(0).Cells(objdgcolhperiodid.Index).Value))

                If objEvaluation._Message <> "" Then
                    eZeeMsgBox.Show(objEvaluation._Message, enMsgBoxStyle.Information)
                Else
                    Call FillList()
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnDelete_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Me.Close()
    End Sub

    Private Sub btnUnlockCommit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnUnlockCommit.Click
        Dim sVoidReason As String = String.Empty
        Try
            If dgvSelfAssessList.SelectedRows.Count > 0 Then
                If objEvaluation.isExist(enAssessmentMode.APPRAISER_ASSESSMENT, _
                                             CInt(dgvSelfAssessList.SelectedRows(0).Cells(objdgcolhempid.Index).Value), _
                                             CInt(dgvSelfAssessList.SelectedRows(0).Cells(objdgcolhperiodid.Index).Value)) = True Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "Sorry, you cannot unlock General Assessment for the selected period. Reason : Assessment is already done for the selected period by Assessor(s)."), enMsgBoxStyle.Information)
                    Exit Sub
                End If

                If objEvaluation.isExist(enAssessmentMode.REVIEWER_ASSESSMENT, _
                                             CInt(dgvSelfAssessList.SelectedRows(0).Cells(objdgcolhempid.Index).Value), _
                                             CInt(dgvSelfAssessList.SelectedRows(0).Cells(objdgcolhperiodid.Index).Value)) = True Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 6, "Sorry, you cannot unlock General Assessment for the selected period. Reason : Assessment is already done for the selected period by Reviewer."), enMsgBoxStyle.Information)
                    Exit Sub
                End If


                'Shani (24-May-2016) -- Start

                'S.SANDEEP |12-FEB-2019| -- START
                'ISSUE/ENHANCEMENT : {Performance Assessment Changes}
                'If objEvaluation.CheckComputionIsExist(CInt(dgvSelfAssessList.SelectedRows(0).Cells(objdgcolhanalysisid.Index).Value), _
                '                                       enAssessmentMode.SELF_ASSESSMENT, _
                '                                       CInt(dgvSelfAssessList.SelectedRows(0).Cells(objdgcolhperiodid.Index).Value)) = True Then

                '    If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 8, "Sorry, you cannot unlock General Assessment for the selected period. Reason : Compute score process is already done for the selected period. Are you sure you want to Void computation score?"), enMsgBoxStyle.OkCancel) = Windows.Forms.DialogResult.OK Then
                '        Dim frm As New frmReasonSelection
                '        If User._Object._Isrighttoleft = True Then
                '            frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                '            frm.RightToLeftLayout = True
                '            Call Language.ctlRightToLeftlayOut(frm)
                '        End If
                '        frm.displayDialog(enVoidCategoryType.ASSESSMENT, sVoidReason)
                '        If sVoidReason.Trim.Length <= 0 Then Exit Sub

                '        Dim objComputeMst As New clsComputeScore_master
                '        objComputeMst._Isvoid = True
                '        objComputeMst._Voiddatetime = ConfigParameter._Object._CurrentDateAndTime
                '        objComputeMst._Voidreason = sVoidReason
                '        objComputeMst._Voiduserunkid = User._Object._Userunkid
                '        If objComputeMst.DeleteEmployeeWise(CInt(dgvSelfAssessList.SelectedRows(0).Cells(objdgcolhanalysisid.Index).Value), _
                '                                            CInt(dgvSelfAssessList.SelectedRows(0).Cells(objdgcolhempid.Index).Value), _
                '                                            CInt(dgvSelfAssessList.SelectedRows(0).Cells(objdgcolhperiodid.Index).Value), enAssessmentMode.SELF_ASSESSMENT) = True Then
                '            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 9, "Successfully voided Computation score"), enMsgBoxStyle.Information)
                '        Else
                '            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 10, "Fail void Computation score process"), enMsgBoxStyle.Information)
                '            Exit Sub
                '        End If
                '    Else
                '        Exit Sub
                '    End If
                'End If
                'S.SANDEEP |12-FEB-2019| -- END

                'Shani (24-May-2016) -- End

                'S.SANDEEP |13-NOV-2019| -- START
                'ISSUE/ENHANCEMENT : CALIBRATION ISSUES
                Dim strMessage As String = String.Empty
                'S.SANDEEP |13-NOV-2019| -- END
                If CInt(dgvSelfAssessList.SelectedRows(0).Cells(objdgcolhstatusid.Index).Value) = enStatusType.Open Then
                    'S.SANDEEP |13-NOV-2019| -- START
                    'ISSUE/ENHANCEMENT : CALIBRATION ISSUES
                    'If objEvaluation.Unlock_Commit(CInt(dgvSelfAssessList.SelectedRows(0).Cells(objdgcolhperiodid.Index).Value), _
                    '                                   CInt(dgvSelfAssessList.SelectedRows(0).Cells(objdgcolhempid.Index).Value)) = True Then
                    If objEvaluation.Unlock_Commit(CInt(dgvSelfAssessList.SelectedRows(0).Cells(objdgcolhperiodid.Index).Value), _
                                                       CInt(dgvSelfAssessList.SelectedRows(0).Cells(objdgcolhempid.Index).Value), strMessage) = True Then
                        'S.SANDEEP |13-NOV-2019| -- END
                        objEvaluation._Analysisunkid = CInt(dgvSelfAssessList.SelectedRows(0).Cells(objdgcolhanalysisid.Index).Value)
                        objEvaluation._Iscommitted = False
                        objEvaluation._Committeddatetime = Nothing
                        'S.SANDEEP [27-APR-2017] -- START
                        'ISSUE/ENHANCEMENT : OPTIMIZING PERFORMANCE MODULE
                        'objEvaluation.Update()
                        objEvaluation.Update(Nothing, Nothing, Nothing, 0, Nothing, False, False, Nothing, False)
                        'S.SANDEEP [27-APR-2017] -- END

                        If objEvaluation._Message <> "" Then
                            eZeeMsgBox.Show(objEvaluation._Message)
                        End If
                        Call FillList()
                    Else
                        'S.SANDEEP |13-NOV-2019| -- START
                        'ISSUE/ENHANCEMENT : CALIBRATION ISSUES
                        'eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 7, "Sorry, you cannot unlock this committed information. Reason : Its already linked with Appraisal."), enMsgBoxStyle.Information)
                        If strMessage.Trim.Length > 0 Then eZeeMsgBox.Show(strMessage, enMsgBoxStyle.Information)
                        'S.SANDEEP |13-NOV-2019| -- END
                        Exit Sub
                    End If
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnUnlockCommit_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub objbtnReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnReset.Click
        Try
            cboEmployee.SelectedValue = 0
            cboPeriod.SelectedValue = 0
            cboYear.SelectedValue = 0
            dtpAssessmentdate.Checked = False
            chkShowCommited.CheckState = CheckState.Checked
            'S.SANDEEP [04 JUN 2015] -- START
            chkShowUncommited.CheckState = CheckState.Checked
            'S.SANDEEP [04 JUN 2015] -- END
            mstrAdvanceFilter = String.Empty
            Call FillList()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnReset_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnSearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearch.Click
        Try
            Call FillList()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearch_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnSearchEmployee_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchEmployee.Click
        Dim frm As New frmCommonSearch
        Try
            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If

            frm.ValueMember = cboEmployee.ValueMember
            frm.DisplayMember = cboEmployee.DisplayMember
            frm.CodeMember = "employeecode"
            frm.DataSource = CType(cboEmployee.DataSource, DataTable)
            If frm.DisplayDialog Then
                cboEmployee.SelectedValue = frm.SelectedValue
                cboEmployee.Focus()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchEmployee_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

#End Region

#Region " Link Event(s) "

    Private Sub lnkAllocation_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lnkAllocation.LinkClicked
        Try
            Dim frm As New frmAdvanceSearch
            frm.ShowDialog()
            mstrAdvanceFilter = frm._GetFilterString
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lnkAllocation_LinkClicked", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " DataGrid Events "

    Private Sub dgvSelfAssessList_CellPainting(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellPaintingEventArgs) Handles dgvSelfAssessList.CellPainting
        Try
            If e.RowIndex <= -1 Then Exit Sub
            If CBool(dgvSelfAssessList.Rows(e.RowIndex).Cells(objdgcolhiscommitted.Index).Value) = True Then
                dgvSelfAssessList.Rows(e.RowIndex).DefaultCellStyle.ForeColor = Color.Blue
            End If
            If dgvSelfAssessList.Columns(e.ColumnIndex).DataPropertyName = dgcolhassessdate.DataPropertyName Then
                If dgvSelfAssessList.Rows(e.RowIndex).Cells(e.ColumnIndex).Value IsNot Nothing AndAlso IsDBNull(dgvSelfAssessList.Rows(e.RowIndex).Cells(e.ColumnIndex).Value) = False AndAlso dgvSelfAssessList.Rows(e.RowIndex).Cells(e.ColumnIndex).Value.ToString().Trim.Length > 0 AndAlso IsDate(dgvSelfAssessList.Rows(e.RowIndex).Cells(e.ColumnIndex).Value) = False Then
                    dgvSelfAssessList.Rows(e.RowIndex).Cells(e.ColumnIndex).Value = eZeeDate.convertDate(dgvSelfAssessList.Rows(e.RowIndex).Cells(e.ColumnIndex).Value.ToString()).ToShortDateString
                End If
            End If
            'If dgvSelfAssessList.Columns(e.ColumnIndex).DataPropertyName = dgcolhscore.DataPropertyName Then
            '    Dim xScore As List(Of Decimal)
            '    If dtComputeScore.AsEnumerable.Where(Function(x) x.Field(Of Integer)("employeeunkid") = CInt(dgvSelfAssessList.Rows(e.RowIndex).Cells(objdgcolhempid.Index).Value)).Count > 0 Then
            '        If CInt(dgvSelfAssessList.Rows(e.RowIndex).Cells(objdgcolhsmodeid.Index).Value) = 1 Then
            '            xScore = dtComputeScore.AsEnumerable().Where(Function(x) x.Field(Of Integer)("formula_typeid") = enAssess_Computation_Formulas.BSC_EMP_TOTAL_SCORE _
            '                                                         And x.Field(Of Integer)("assessmodeid") = enAssessmentMode.SELF_ASSESSMENT And x.Field(Of Integer)("employeeunkid") = CInt(dgvSelfAssessList.Rows(e.RowIndex).Cells(objdgcolhempid.Index).Value)) _
            '                                                        .Select(Function(x) x.Field(Of Decimal)("formula_value")).ToList
            '            If xScore IsNot Nothing AndAlso xScore.Count > 0 Then
            '                dgvSelfAssessList.Rows(e.RowIndex).Cells(dgcolhscore.Index).Value = xScore(0).ToString("#########0.#0")
            '            End If
            '        Else
            '            Dim xValue As String = dtComputeScore.AsEnumerable().Where(Function(x) x.Field(Of Integer)("formula_typeid") = enAssess_Computation_Formulas.CMP_EMP_TOTAL_SCORE _
            '                                                         And x.Field(Of Integer)("assessmodeid") = enAssessmentMode.SELF_ASSESSMENT And x.Field(Of Integer)("employeeunkid") = CInt(dgvSelfAssessList.Rows(e.RowIndex).Cells(objdgcolhempid.Index).Value)) _
            '                                                        .Select(Function(x) x.Field(Of String)("competency_value")).ToList.First()
            '            If xValue.Contains(",") = False Then
            '                xScore = dtComputeScore.AsEnumerable().Where(Function(x) x.Field(Of Integer)("formula_typeid") = enAssess_Computation_Formulas.CMP_EMP_TOTAL_SCORE _
            '                                                         And x.Field(Of Integer)("assessmodeid") = enAssessmentMode.SELF_ASSESSMENT And x.Field(Of Integer)("employeeunkid") = CInt(dgvSelfAssessList.Rows(e.RowIndex).Cells(objdgcolhempid.Index).Value)) _
            '                                                        .Select(Function(x) x.Field(Of Decimal)("formula_value")).ToList
            '                If xScore IsNot Nothing AndAlso xScore.Count > 0 Then
            '                    dgvSelfAssessList.Rows(e.RowIndex).Cells(dgcolhscore.Index).Value = xScore(0).ToString("#########0.#0")
            '                End If
            '            Else
            '                For Each xVal As String In xValue.Split(CChar(","))
            '                    If CInt(dgvSelfAssessList.Rows(e.RowIndex).Cells(objdgcolhassessgroupunkid.Index).Value) = CInt(xVal.Split(CChar("|"))(0)) Then
            '                        dgvSelfAssessList.Rows(e.RowIndex).Cells(dgcolhscore.Index).Value = CDec(xVal.Split(CChar("|"))(1)).ToString("#########0.#0")
            '                    End If
            '                Next
            '            End If
            '        End If
            '    End If
            'Else
            '    dgvSelfAssessList.Rows(e.RowIndex).Cells(dgcolhscore.Index).Value = 0
            'End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgvSelfAssessList_CellPainting", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub dgvSelfAssessList_DataBindingComplete(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewBindingCompleteEventArgs) Handles dgvSelfAssessList.DataBindingComplete
        If dgvSelfAssessList.SelectedRows.Count <= 0 Then Exit Sub
        If dgvSelfAssessList.SelectedRows(0).DefaultCellStyle.ForeColor = Color.Blue Then
            btnEdit.Enabled = False : btnDelete.Enabled = False : btnUnlockCommit.Enabled = True
        Else
            btnEdit.Enabled = True : btnDelete.Enabled = True : btnUnlockCommit.Enabled = False
        End If
    End Sub

    Private Sub dgvSelfAssessList_DataError(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewDataErrorEventArgs) Handles dgvSelfAssessList.DataError

    End Sub

    Private Sub dgvSelfAssessList_SelectionChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles dgvSelfAssessList.SelectionChanged
        Try
            If dgvSelfAssessList.SelectedRows.Count <= 0 Then Exit Sub
            If dgvSelfAssessList.SelectedRows(0).DefaultCellStyle.ForeColor = Color.Blue Then
                btnEdit.Enabled = False : btnDelete.Enabled = False : btnUnlockCommit.Enabled = True
            Else
                btnEdit.Enabled = True : btnDelete.Enabled = True : btnUnlockCommit.Enabled = False
            End If
            btnUnlockCommit.Enabled = User._Object.Privilege._AllowtoUnlockcommittedSelfEvaluation
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgvSelfAssessList_SelectionChanged", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Private Sub OtherSettings()
        Try
            Me.SuspendLayout()
           
            Call SetLanguage()

            Me.gbEmployeeInfo.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbEmployeeInfo.ForeColor = GUI._eZeeContainerHeaderForeColor

            Me.eZeeHeader.GradientColor1 = GUI._HeaderBackColor1
            Me.eZeeHeader.GradientColor2 = GUI._HeaderBackColor2
            Me.eZeeHeader.BorderColor = GUI._HeaderBorderColor
            Me.eZeeHeader.HeaderTextForeColor = GUI._HeaderTitleFontColor
            Me.eZeeHeader.DescriptionForeColor = GUI._HeaderMessageFontColor


            Me.btnUnlockCommit.GradientBackColor = GUI._ButttonBackColor
            Me.btnUnlockCommit.GradientForeColor = GUI._ButttonFontColor

            Me.btnOperation.GradientBackColor = GUI._ButttonBackColor
            Me.btnOperation.GradientForeColor = GUI._ButttonFontColor

            Me.btnNew.GradientBackColor = GUI._ButttonBackColor
            Me.btnNew.GradientForeColor = GUI._ButttonFontColor

            Me.btnDelete.GradientBackColor = GUI._ButttonBackColor
            Me.btnDelete.GradientForeColor = GUI._ButttonFontColor

            Me.btnEdit.GradientBackColor = GUI._ButttonBackColor
            Me.btnEdit.GradientForeColor = GUI._ButttonFontColor

            Me.btnClose.GradientBackColor = GUI._ButttonBackColor
            Me.btnClose.GradientForeColor = GUI._ButttonFontColor


            Me.ResumeLayout()
        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "OtherSettings", mstrModuleName)
        End Try
    End Sub


    Private Sub SetLanguage()
        Try
            Me.Text = Language._Object.getCaption(Me.Name, Me.Text)

            Me.eZeeHeader.Title = Language._Object.getCaption(Me.eZeeHeader.Name & "_Title", Me.eZeeHeader.Title)
            Me.eZeeHeader.Message = Language._Object.getCaption(Me.eZeeHeader.Name & "_Message", Me.eZeeHeader.Message)
            Me.mnuUnlockCommitted.Text = Language._Object.getCaption(Me.mnuUnlockCommitted.Name, Me.mnuUnlockCommitted.Text)
            Me.mnuGetFileFormat.Text = Language._Object.getCaption(Me.mnuGetFileFormat.Name, Me.mnuGetFileFormat.Text)
            Me.mnuImportSelfAssessment.Text = Language._Object.getCaption(Me.mnuImportSelfAssessment.Name, Me.mnuImportSelfAssessment.Text)
            Me.btnUnlockCommit.Text = Language._Object.getCaption(Me.btnUnlockCommit.Name, Me.btnUnlockCommit.Text)
            Me.btnOperation.Text = Language._Object.getCaption(Me.btnOperation.Name, Me.btnOperation.Text)
            Me.btnNew.Text = Language._Object.getCaption(Me.btnNew.Name, Me.btnNew.Text)
            Me.btnDelete.Text = Language._Object.getCaption(Me.btnDelete.Name, Me.btnDelete.Text)
            Me.btnEdit.Text = Language._Object.getCaption(Me.btnEdit.Name, Me.btnEdit.Text)
            Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
            Me.gbEmployeeInfo.Text = Language._Object.getCaption(Me.gbEmployeeInfo.Name, Me.gbEmployeeInfo.Text)
            Me.chkShowUncommited.Text = Language._Object.getCaption(Me.chkShowUncommited.Name, Me.chkShowUncommited.Text)
            Me.chkShowCommited.Text = Language._Object.getCaption(Me.chkShowCommited.Name, Me.chkShowCommited.Text)
            Me.lnkAllocation.Text = Language._Object.getCaption(Me.lnkAllocation.Name, Me.lnkAllocation.Text)
            Me.lblAssessmentdate.Text = Language._Object.getCaption(Me.lblAssessmentdate.Name, Me.lblAssessmentdate.Text)
            Me.lblEmployee.Text = Language._Object.getCaption(Me.lblEmployee.Name, Me.lblEmployee.Text)
            Me.lblYears.Text = Language._Object.getCaption(Me.lblYears.Name, Me.lblYears.Text)
            Me.lblAssessmentPeriods.Text = Language._Object.getCaption(Me.lblAssessmentPeriods.Name, Me.lblAssessmentPeriods.Text)
            Me.dgvSelfAssessList.Text = Language._Object.getCaption(Me.dgvSelfAssessList.Name, Me.dgvSelfAssessList.Text)
            Me.dgcolhemp.HeaderText = Language._Object.getCaption(Me.dgcolhemp.Name, Me.dgcolhemp.HeaderText)
            Me.dgcolhyear.HeaderText = Language._Object.getCaption(Me.dgcolhyear.Name, Me.dgcolhyear.HeaderText)
            Me.dgcolhperiod.HeaderText = Language._Object.getCaption(Me.dgcolhperiod.Name, Me.dgcolhperiod.HeaderText)
            Me.dgcolhassessdate.HeaderText = Language._Object.getCaption(Me.dgcolhassessdate.Name, Me.dgcolhassessdate.HeaderText)
            Me.dgcolhmode.HeaderText = Language._Object.getCaption(Me.dgcolhmode.Name, Me.dgcolhmode.HeaderText)
            Me.dgcolhscore.HeaderText = Language._Object.getCaption(Me.dgcolhscore.Name, Me.dgcolhscore.HeaderText)

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
        End Try
    End Sub


    Private Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Please select at least one Assessment to perform operation.")
            Language.setMessage(mstrModuleName, 2, "Sorry, you cannot edit this information. Reason : Period is closed.")
            Language.setMessage(mstrModuleName, 3, "Sorry, you cannot delete this information. Reason : Period is closed.")
            Language.setMessage(mstrModuleName, 4, "Are you sure you want to delete this Performance Evaluation?")
            Language.setMessage(mstrModuleName, 5, "Sorry, you cannot unlock General Assessment for the selected period. Reason : Assessment is already done for the selected period by Assessor(s).")
            Language.setMessage(mstrModuleName, 6, "Sorry, you cannot unlock General Assessment for the selected period. Reason : Assessment is already done for the selected period by Reviewer.")
            Language.setMessage(mstrModuleName, 7, "Sorry, you cannot unlock this committed information. Reason : Its already linked with Appraisal.")
            Language.setMessage(mstrModuleName, 8, "Sorry, you cannot unlock General Assessment for the selected period. Reason : Compute score process is already done for the selected period. Are you sure you want to Void computation score?")
            Language.setMessage(mstrModuleName, 9, "Successfully voided Computation score")
            Language.setMessage(mstrModuleName, 10, "Fail void Computation score process")
            Language.setMessage(mstrModuleName, 11, "Sorry, you cannot edit General Assessment for the selected period. Reason : Compute score process is already done for the selected period. Are you sure you want to Void computation score?")
            Language.setMessage(mstrModuleName, 12, "Sorry, you cannot delete General Assessment for the selected period. Reason : Compute score process is already done for the selected period. Are you sure you want to Void computation score?")
            Language.setMessage(mstrModuleName, 13, "Sorry, Period is mandatory information. Please select period to fill list.")
            Language.setMessage(mstrModuleName, 14, "Sorry, you cannot do the edit operation. Reason : Assessment is submitted.")
            Language.setMessage(mstrModuleName, 15, "Sorry, you cannot do the delete operation. Reason : Assessment is submitted.")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class


'*************************************** S.SANDEEP [27-APR-2017] -- ISSUE/ENHANCEMENT : OPTIMIZING PERFORMANCE MODULE ***************************************
'Public Class frmSelfEvaluationList

'#Region " Private Variables "

'    Private ReadOnly mstrModuleName As String = "frmSelfEvaluationList"
'    Private objEvaluation As clsevaluation_analysis_master
'    Private mstrAdvanceFilter As String = String.Empty

'#End Region

'#Region " Private Methods "

'    Private Sub FillCombo()
'        Dim objYear As New clsMasterData
'        Dim objPeriod As New clscommom_period_Tran
'        Dim objEmployee As New clsEmployee_Master
'        Dim dsFill As New DataSet
'        Try

'            'S.SANDEEP [04 JUN 2015] -- START
'            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
'            'dsFill = objYear.getComboListPAYYEAR("Year", True, , , , True)
'            dsFill = objYear.getComboListPAYYEAR(FinancialYear._Object._YearUnkid, FinancialYear._Object._FinancialYear_Name, Company._Object._Companyunkid, "Year", True, True)
'            'S.SANDEEP [04 JUN 2015] -- END

'            With cboYear
'                .ValueMember = "Id"
'                .DisplayMember = "name"
'                .DataSource = dsFill.Tables("Year")
'                .SelectedValue = 0
'            End With

'            'Sohail (21 Aug 2015) -- Start
'            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
'            'dsFill = objPeriod.getListForCombo(enModuleReference.Assessment, 0, "Period", True, 0)
'            dsFill = objPeriod.getListForCombo(enModuleReference.Assessment, 0, FinancialYear._Object._DatabaseName, FinancialYear._Object._Database_Start_Date, "Period", True, 0)
'            'Sohail (21 Aug 2015) -- End
'            With cboPeriod
'                .ValueMember = "periodunkid"
'                .DisplayMember = "name"
'                .DataSource = dsFill.Tables("Period")
'                .SelectedValue = 0
'            End With

'            'S.SANDEEP [04 JUN 2015] -- START
'            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
'            'If ConfigParameter._Object._IsIncludeInactiveEmp = False Then
'            '    dsFill = objEmployee.GetEmployeeList("List", True, , , , , , , , , , , , , eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate))
'            'Else
'            '    dsFill = objEmployee.GetEmployeeList("List", True, )
'            'End If
'            dsFill = objEmployee.GetEmployeeList(FinancialYear._Object._DatabaseName, _
'                                                 User._Object._Userunkid, _
'                                                 FinancialYear._Object._YearUnkid, _
'                                                 Company._Object._Companyunkid, _
'                                                 eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
'                                                 eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
'                                                 ConfigParameter._Object._UserAccessModeSetting, _
'                                                 True, ConfigParameter._Object._IsIncludeInactiveEmp, "List", True)

'            'S.SANDEEP [04 JUN 2015] -- END

'            With cboEmployee
'                .ValueMember = "employeeunkid"
'                .DisplayMember = "employeename"
'                .DataSource = dsFill.Tables("List")
'                .SelectedValue = 0
'            End With
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
'        Finally
'        End Try
'    End Sub

'    Private Sub FillList()
'        Dim dsList As New DataSet
'        Dim dtTable As DataTable
'        Dim StrSearching As String = String.Empty
'        Try
'            'S.SANDEEP [28 MAY 2015] -- START
'            'ENHANCEMENT : NEW PRIVILEGE (Performance Assessment)
'            'If User._Object.Privilege._AllowToViewSelfAssessmentList = True Then
'            If User._Object.Privilege._AllowtoViewSelfEvaluationList = True Then
'                'S.SANDEEP [28 MAY 2015] -- END


'                'S.SANDEEP [04 JUN 2015] -- START
'                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS

'                'dsList = objEvaluation.GetList("List", enAssessmentMode.SELF_ASSESSMENT)

'                'If CInt(cboEmployee.SelectedValue) > 0 Then
'                '    StrSearching &= "AND selfemployeeunkid = " & CInt(cboEmployee.SelectedValue)
'                'End If

'                'If CInt(cboYear.SelectedValue) > 0 Then
'                '    StrSearching &= "AND yearunkid = " & CInt(cboYear.SelectedValue)
'                'End If

'                'If CInt(cboPeriod.SelectedValue) > 0 Then
'                '    StrSearching &= "AND periodunkid = " & CInt(cboPeriod.SelectedValue)
'                'End If

'                'If dtpAssessmentdate.Checked = True Then
'                '    StrSearching &= "AND assessmentdate = '" & eZeeDate.convertDate(dtpAssessmentdate.Value) & "'"
'                'End If

'                'If chkShowCommited.CheckState = CheckState.Checked And chkShowUncommited.CheckState = CheckState.Unchecked Then
'                '    StrSearching &= "AND iscommitted = " & True & " "
'                'ElseIf chkShowCommited.CheckState = CheckState.Unchecked And chkShowUncommited.CheckState = CheckState.Checked Then
'                '    StrSearching &= "AND iscommitted = " & False & " "
'                'End If

'                'If mstrAdvanceFilter.Length > 0 Then
'                '    StrSearching &= "AND " & mstrAdvanceFilter
'                'End If

'                'If StrSearching.Length > 0 Then
'                '    StrSearching = StrSearching.Substring(3)
'                '    dtTable = New DataView(dsList.Tables(0), StrSearching, "EmpName", DataViewRowState.CurrentRows).ToTable
'                'Else
'                '    dtTable = New DataView(dsList.Tables(0), "", "EmpName", DataViewRowState.CurrentRows).ToTable
'                'End If

'                If CInt(cboEmployee.SelectedValue) > 0 Then
'                    StrSearching &= "AND hrevaluation_analysis_master.selfemployeeunkid = " & CInt(cboEmployee.SelectedValue)
'                End If

'                If CInt(cboYear.SelectedValue) > 0 Then
'                    StrSearching &= "AND cfcommon_period_tran.yearunkid = " & CInt(cboYear.SelectedValue)
'                End If

'                If CInt(cboPeriod.SelectedValue) > 0 Then
'                    StrSearching &= "AND hrevaluation_analysis_master.periodunkid = " & CInt(cboPeriod.SelectedValue)
'                End If

'                If dtpAssessmentdate.Checked = True Then
'                    StrSearching &= "AND CONVERT(CHAR(8),hrevaluation_analysis_master.assessmentdate,112) = '" & eZeeDate.convertDate(dtpAssessmentdate.Value) & "'"
'                End If

'                If chkShowCommited.CheckState = CheckState.Checked And chkShowUncommited.CheckState = CheckState.Unchecked Then
'                    'Shani (24-May-2016) -- Start
'                    'StrSearching &= "AND ISNULL(iscommitted,0) = " & True & " "
'                    StrSearching &= "AND ISNULL(iscommitted,0) = 1 "
'                    'Shani (24-May-2016) -- End
'                ElseIf chkShowCommited.CheckState = CheckState.Unchecked And chkShowUncommited.CheckState = CheckState.Checked Then
'                    'Shani (24-May-2016) -- Start
'                    'StrSearching &= "AND ISNULL(iscommitted,0) = " & False & " "
'                    StrSearching &= "AND ISNULL(iscommitted,0) = 0 "
'                    'Shani (24-May-2016) -- End
'                End If

'                If mstrAdvanceFilter.Length > 0 Then
'                    StrSearching &= "AND " & mstrAdvanceFilter
'                End If

'                If StrSearching.Length > 0 Then
'                    StrSearching = StrSearching.Substring(3)
'                End If

'                dsList = objEvaluation.GetList(FinancialYear._Object._DatabaseName, _
'                                               User._Object._Userunkid, _
'                                               FinancialYear._Object._YearUnkid, _
'                                               Company._Object._Companyunkid, _
'                                               eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
'                                               eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
'                                               ConfigParameter._Object._UserAccessModeSetting, True, _
'                                               ConfigParameter._Object._IsIncludeInactiveEmp, "List", _
'                                               enAssessmentMode.SELF_ASSESSMENT, StrSearching)

'                dtTable = New DataView(dsList.Tables(0), "", "EmpName", DataViewRowState.CurrentRows).ToTable

'                'S.SANDEEP [04 JUN 2015] -- END

'                lvAssesmentList.Items.Clear()

'                'For Each dtRow As DataRow In dtTable.Rows
'                '    Dim lvItem As New ListViewItem

'                '    lvItem.Text = dtRow.Item("smode").ToString
'                '    lvItem.SubItems.Add(dtRow.Item("YearName").ToString)
'                '    lvItem.SubItems.Add(dtRow.Item("PName").ToString) : lvItem.SubItems(colhPeriod.Index).Tag = dtRow.Item("Sid")
'                '    lvItem.SubItems.Add(eZeeDate.convertDate(dtRow.Item("assessmentdate").ToString).ToShortDateString)
'                '    lvItem.SubItems.Add(dtRow.Item("EmpName").ToString)

'                '    'S.SANDEEP [21 JAN 2015] -- START
'                '    ''S.SANDEEP [ 01 JAN 2015 ] -- START
'                '    ''If ConfigParameter._Object._ConsiderItemWeightAsNumber Then
'                '    ''    lvItem.SubItems.Add(dtRow.Item("wscore").ToString)
'                '    ''Else
'                '    ''    lvItem.SubItems.Add(dtRow.Item("rscore").ToString)
'                '    ''End If
'                ''    lvItem.SubItems.Add(dtRow.Item("rscore").ToString)
'                '    ''S.SANDEEP [ 01 JAN 2015 ] -- END
'                '    Dim xTotalScore As Decimal = 0
'                '    Select Case CInt(dtRow.Item("smodeid"))
'                '        Case 1  'BSC
'                '            xTotalScore = objEvaluation.Compute_Score(enAssessmentMode.SELF_ASSESSMENT, True, ConfigParameter._Object._ScoringOptionId, enAssess_Computation_Formulas.BSC_EMP_TOTAL_SCORE, CInt(dtRow.Item("selfemployeeunkid")), CInt(dtRow.Item("periodunkid")))
'                '        Case 2  'ASSESSMENT GROUP
'                '            xTotalScore = objEvaluation.Compute_Score(enAssessmentMode.SELF_ASSESSMENT, False, ConfigParameter._Object._ScoringOptionId, enAssess_Computation_Formulas.CMP_EMP_TOTAL_SCORE, CInt(dtRow.Item("selfemployeeunkid")), CInt(dtRow.Item("periodunkid")), CInt(dtRow.Item("assessgroupunkid")))
'                '    End Select
'                '    lvItem.SubItems.Add(xTotalScore.ToString("#########0.#0"))
'                '    'S.SANDEEP [21 JAN 2015] -- END

'                '    lvItem.SubItems.Add(dtRow.Item("periodunkid").ToString)
'                '    lvItem.SubItems.Add(dtRow.Item("selfemployeeunkid").ToString)

'                '    If CBool(dtRow.Item("iscommitted")) = True Then
'                '        lvItem.ForeColor = Color.Blue
'                '    End If

'                '    lvItem.Tag = dtRow.Item("analysisunkid")

'                '    lvAssesmentList.Items.Add(lvItem)
'                'Next
'                'lvAssesmentList.GroupingColumn = objcolhEmployee
'                'lvAssesmentList.DisplayGroups(True)


'                Dim lvItems As ListViewItem() = New ListViewItem(dtTable.Rows.Count - 1) {}
'                For index As Integer = 0 To dtTable.Rows.Count - 1
'                    Dim lvItem As New ListViewItem(dtTable.Rows(index).Item("smode").ToString)
'                    lvItem.SubItems.Add(dtTable.Rows(index).Item("YearName").ToString)
'                    lvItem.SubItems.Add(dtTable.Rows(index).Item("PName").ToString) : lvItem.SubItems(colhPeriod.Index).Tag = dtTable.Rows(index).Item("Sid")
'                    lvItem.SubItems.Add(eZeeDate.convertDate(dtTable.Rows(index).Item("assessmentdate").ToString).ToShortDateString)
'                    lvItem.SubItems.Add(dtTable.Rows(index).Item("EmpName").ToString)
'                    Dim xTotalScore As Decimal = 0

'                    'S.SANDEEP [04 JUN 2015] -- START
'                    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
'                    'Select Case CInt(dtTable.Rows(index).Item("smodeid"))
'                    '    Case 1  'BSC
'                    '        xTotalScore = objEvaluation.Compute_Score(enAssessmentMode.SELF_ASSESSMENT, True, ConfigParameter._Object._ScoringOptionId, enAssess_Computation_Formulas.BSC_EMP_TOTAL_SCORE, CInt(dtTable.Rows(index).Item("selfemployeeunkid")), CInt(dtTable.Rows(index).Item("periodunkid")))
'                    '    Case 2  'ASSESSMENT GROUP
'                    '        xTotalScore = objEvaluation.Compute_Score(enAssessmentMode.SELF_ASSESSMENT, False, ConfigParameter._Object._ScoringOptionId, enAssess_Computation_Formulas.CMP_EMP_TOTAL_SCORE, CInt(dtTable.Rows(index).Item("selfemployeeunkid")), CInt(dtTable.Rows(index).Item("periodunkid")), CInt(dtTable.Rows(index).Item("assessgroupunkid")))
'                    'End Select

'                    'Shani (23-Nov-2016) -- Start
'                    'Enhancement - Add New PA Enhancement (Agreed Score,exc) Given by Akiba,KBC,TNP exc...
'                    'Select Case CInt(dtTable.Rows(index).Item("smodeid"))
'                    '    Case 1  'BSC
'                    '        xTotalScore = objEvaluation.Compute_Score(enAssessmentMode.SELF_ASSESSMENT, True, ConfigParameter._Object._ScoringOptionId, enAssess_Computation_Formulas.BSC_EMP_TOTAL_SCORE, eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), CInt(dtTable.Rows(index).Item("selfemployeeunkid")), CInt(dtTable.Rows(index).Item("periodunkid")), , , , , ConfigParameter._Object._Self_Assign_Competencies) 'S.SANDEEP [08 Jan 2016] -- START{ConfigParameter._Object._Self_Assign_Competencies} -- END
'                    '    Case 2  'ASSESSMENT GROUP
'                    '        xTotalScore = objEvaluation.Compute_Score(enAssessmentMode.SELF_ASSESSMENT, False, ConfigParameter._Object._ScoringOptionId, enAssess_Computation_Formulas.CMP_EMP_TOTAL_SCORE, eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), CInt(dtTable.Rows(index).Item("selfemployeeunkid")), CInt(dtTable.Rows(index).Item("periodunkid")), CInt(dtTable.Rows(index).Item("assessgroupunkid")), , , , ConfigParameter._Object._Self_Assign_Competencies) 'S.SANDEEP [08 Jan 2016] -- START{ConfigParameter._Object._Self_Assign_Competencies} -- END
'                    'End Select
'                    Select Case CInt(dtTable.Rows(index).Item("smodeid"))
'                        Case 1  'BSC
'                            xTotalScore = objEvaluation.Compute_Score(xAssessMode:=enAssessmentMode.SELF_ASSESSMENT, _
'                                                                      IsBalanceScoreCard:=True, _
'                                                                      xScoreOptId:=ConfigParameter._Object._ScoringOptionId, _
'                                                                      xCompute_Formula:=enAssess_Computation_Formulas.BSC_EMP_TOTAL_SCORE, _
'                                                                      xEmployeeAsOnDate:=eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
'                                                                      xEmployeeId:=CInt(dtTable.Rows(index).Item("selfemployeeunkid")), _
'                                                                      xPeriodId:=CInt(dtTable.Rows(index).Item("periodunkid")), _
'                                                                      xUsedAgreedScore:=ConfigParameter._Object._IsUseAgreedScore, _
'                                                                      xSelfAssignedCompetencies:=ConfigParameter._Object._Self_Assign_Competencies)
'                        Case 2  'ASSESSMENT GROUP
'                            xTotalScore = objEvaluation.Compute_Score(xAssessMode:=enAssessmentMode.SELF_ASSESSMENT, _
'                                                                      IsBalanceScoreCard:=False, _
'                                                                      xScoreOptId:=ConfigParameter._Object._ScoringOptionId, _
'                                                                      xCompute_Formula:=enAssess_Computation_Formulas.CMP_EMP_TOTAL_SCORE, _
'                                                                      xEmployeeAsOnDate:=eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
'                                                                      xEmployeeId:=CInt(dtTable.Rows(index).Item("selfemployeeunkid")), _
'                                                                      xPeriodId:=CInt(dtTable.Rows(index).Item("periodunkid")), _
'                                                                      xUsedAgreedScore:=ConfigParameter._Object._IsUseAgreedScore, _
'                                                                      xAssessGrpId:=CInt(dtTable.Rows(index).Item("assessgroupunkid")), _
'                                                                      xSelfAssignedCompetencies:=ConfigParameter._Object._Self_Assign_Competencies)
'                    End Select
'                    'Shani (23-Nov-2016) -- End

'                    'S.SANDEEP [04 JUN 2015] -- END

'                    lvItem.SubItems.Add(xTotalScore.ToString("#########0.#0"))
'                    lvItem.SubItems.Add(dtTable.Rows(index).Item("periodunkid").ToString)
'                    lvItem.SubItems.Add(dtTable.Rows(index).Item("selfemployeeunkid").ToString)
'                    If CBool(dtTable.Rows(index).Item("iscommitted")) = True Then
'                        lvItem.ForeColor = Color.Blue
'                    End If
'                    lvItem.Tag = dtTable.Rows(index).Item("analysisunkid")
'                    lvItems(index) = lvItem
'                Next

'                lvAssesmentList.BeginUpdate()
'                lvAssesmentList.Items.AddRange(lvItems)

'                lvAssesmentList.GroupingColumn = objcolhEmployee
'                lvAssesmentList.DisplayGroups(True)
'                lvAssesmentList.EndUpdate()

'                If lvAssesmentList.Items.Count > 2 Then
'                    colhMode.Width = 250 - 20
'                Else
'                    colhMode.Width = 250
'                End If
'                'S.SANDEEP [04 JUN 2015] -- END

'            End If
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "FillList", mstrModuleName)
'        Finally
'        End Try
'    End Sub

'    Private Sub SetVisibility()
'        Try
'            'S.SANDEEP [28 MAY 2015] -- START
'            'ENHANCEMENT : NEW PRIVILEGE (Performance Assessment)
'            'btnNew.Enabled = User._Object.Privilege._AddEmployeeAssessment
'            'btnEdit.Enabled = User._Object.Privilege._EditEmployeeAssessment
'            'btnDelete.Enabled = User._Object.Privilege._DeleteEmployeeAssessment
'            'btnUnlockCommit.Enabled = User._Object.Privilege._Allow_UnlockCommittedGeneralAssessment

'            btnNew.Enabled = User._Object.Privilege._AllowtoAddSelfEvaluation
'            btnEdit.Enabled = User._Object.Privilege._AllowtoEditSelfEvaluation
'            btnDelete.Enabled = User._Object.Privilege._AllowtoDeleteSelfEvaluation
'            btnUnlockCommit.Enabled = User._Object.Privilege._AllowtoUnlockcommittedSelfEvaluation
'            'S.SANDEEP [28 MAY 2015] -- END

'            chkShowCommited.CheckState = CheckState.Checked
'            'S.SANDEEP [04 JUN 2015] -- START
'            chkShowUncommited.CheckState = CheckState.Checked
'            'S.SANDEEP [04 JUN 2015] -- END

'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "SetVisibility", mstrModuleName)
'        End Try
'    End Sub

'#End Region

'#Region " Form's Events "

'    Private Sub frmSelfEvaluationList_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
'        objEvaluation = New clsevaluation_analysis_master
'        Try
'            Call Set_Logo(Me, gApplicationType)
'            Language.setLanguage(Me.Name)
'            Call OtherSettings()
'            Call SetVisibility()
'            FillCombo()
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "frmSelfEvaluationList_Load", mstrModuleName)
'        End Try
'    End Sub

'    Private Sub frmSelfEvaluationList_KeyPress(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles MyBase.KeyPress
'        Try
'            If Asc(e.KeyChar) = 13 Then
'                SendKeys.Send("{TAB}")
'            End If
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "frmSelfEvaluationList_KeyPress", mstrModuleName)
'        End Try
'    End Sub

'    Private Sub frmSelfEvaluationList_FormClosed(ByVal sender As System.Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles MyBase.FormClosed
'        objEvaluation = Nothing
'    End Sub

'    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
'        Dim objfrm As New frmLanguage
'        Try
'            If User._Object._Isrighttoleft = True Then
'                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
'                objfrm.RightToLeftLayout = True
'                Call Language.ctlRightToLeftlayOut(objfrm)
'            End If

'            Call SetMessages()

'            clsevaluation_analysis_master.SetMessages()
'            objfrm._Other_ModuleNames = "clsevaluation_analysis_master"
'            objfrm.displayDialog(Me)

'            Call SetLanguage()

'        Catch ex As System.Exception
'            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
'        Finally
'            objfrm.Dispose()
'            objfrm = Nothing
'        End Try
'    End Sub

'#End Region

'#Region " Button's Events "

'    Private Sub btnNew_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNew.Click
'        Dim frm As New frmPerformanceEvaluation
'        Try
'            If User._Object._Isrighttoleft = True Then
'                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
'                frm.RightToLeftLayout = True
'                Call Language.ctlRightToLeftlayOut(frm)
'            End If

'            If frm.displayDialog(-1, enAction.ADD_CONTINUE, enAssessmentMode.SELF_ASSESSMENT) Then
'                Call FillList()
'            End If
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "btnNew_Click", mstrModuleName)
'        Finally
'            If frm IsNot Nothing Then frm.Dispose()
'        End Try
'    End Sub

'    Private Sub btnEdit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEdit.Click
'        If lvAssesmentList.SelectedItems.Count <= 0 Then
'            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please select at least one Assessment to perform operation."), enMsgBoxStyle.Information)
'            Exit Sub
'        End If

'        If CInt(lvAssesmentList.SelectedItems(0).SubItems(colhPeriod.Index).Tag) = enStatusType.Close Then
'            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Sorry, you cannot edit this information. Reason : Period is closed."), enMsgBoxStyle.Information)
'            Exit Sub
'        End If

'        'Shani (24-May-2016) -- Start
'        If objEvaluation.CheckComputionIsExist(CInt(lvAssesmentList.SelectedItems(0).Tag), _
'                                               enAssessmentMode.SELF_ASSESSMENT, _
'                                               CInt(lvAssesmentList.SelectedItems(0).SubItems(objcolhPeriodId.Index).Text)) = True Then

'            If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 11, "Sorry, you cannot edit General Assessment for the selected period. Reason : Compute score process is already done for the selected period. Are you sure you want to Void computation score?"), enMsgBoxStyle.OkCancel) = Windows.Forms.DialogResult.OK Then
'                Dim frmVoid As New frmReasonSelection
'                Dim sVoidReason As String = String.Empty
'                If User._Object._Isrighttoleft = True Then
'                    frmVoid.RightToLeft = Windows.Forms.RightToLeft.Yes
'                    frmVoid.RightToLeftLayout = True
'                    Call Language.ctlRightToLeftlayOut(frmVoid)
'                End If
'                frmVoid.displayDialog(enVoidCategoryType.ASSESSMENT, sVoidReason)
'                If sVoidReason.Trim.Length <= 0 Then Exit Sub

'                Dim objComputeMst As New clsComputeScore_master
'                objComputeMst._Isvoid = True
'                objComputeMst._Voiddatetime = ConfigParameter._Object._CurrentDateAndTime
'                objComputeMst._Voidreason = sVoidReason
'                objComputeMst._Voiduserunkid = User._Object._Userunkid
'                If objComputeMst.DeleteEmployeeWise(CInt(lvAssesmentList.SelectedItems(0).Tag), _
'                                                    CInt(lvAssesmentList.SelectedItems(0).SubItems(objcolhEmpId.Index).Text), _
'                                                    CInt(lvAssesmentList.SelectedItems(0).SubItems(objcolhPeriodId.Index).Text), enAssessmentMode.SELF_ASSESSMENT) = True Then
'                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 9, "Successfully voided Computation score"), enMsgBoxStyle.Information)
'                Else
'                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 10, "Fail void Computation score process"), enMsgBoxStyle.Information)
'                    Exit Sub
'                End If
'            Else
'                Exit Sub
'            End If
'        End If
'        'Shani (24-May-2016) -- End

'        Dim frm As New frmPerformanceEvaluation
'        Try
'            If User._Object._Isrighttoleft = True Then
'                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
'                frm.RightToLeftLayout = True
'                Call Language.ctlRightToLeftlayOut(frm)
'            End If

'            If frm.displayDialog(CInt(lvAssesmentList.SelectedItems(0).Tag), enAction.EDIT_ONE, enAssessmentMode.SELF_ASSESSMENT) Then
'                Call FillList()
'            End If

'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "", mstrModuleName)
'        Finally
'            If frm IsNot Nothing Then frm.Dispose()
'        End Try
'    End Sub

'    Private Sub btnDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDelete.Click
'        If lvAssesmentList.SelectedItems.Count <= 0 Then
'            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please select at least one Assessment to perform operation."), enMsgBoxStyle.Information)
'            Exit Sub
'        End If

'        If CInt(lvAssesmentList.SelectedItems(0).SubItems(colhPeriod.Index).Tag) = enStatusType.Close Then
'            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Sorry, you cannot delete this information. Reason : Period is closed."), enMsgBoxStyle.Information)
'            Exit Sub
'        End If
'        Try
'            'Shani (24-May-2016) -- Start
'            If objEvaluation.CheckComputionIsExist(CInt(lvAssesmentList.SelectedItems(0).Tag), _
'                                                   enAssessmentMode.SELF_ASSESSMENT, _
'                                                   CInt(lvAssesmentList.SelectedItems(0).SubItems(objcolhPeriodId.Index).Text)) = True Then

'                If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 12, "Sorry, you cannot delete General Assessment for the selected period. Reason : Compute score process is already done for the selected period. Are you sure you want to Void computation score?"), enMsgBoxStyle.OkCancel) = Windows.Forms.DialogResult.OK Then
'                    Dim frmVoid As New frmReasonSelection
'                    Dim sVoidReason As String = String.Empty
'                    If User._Object._Isrighttoleft = True Then
'                        frmVoid.RightToLeft = Windows.Forms.RightToLeft.Yes
'                        frmVoid.RightToLeftLayout = True
'                        Call Language.ctlRightToLeftlayOut(frmVoid)
'                    End If
'                    frmVoid.displayDialog(enVoidCategoryType.ASSESSMENT, sVoidReason)
'                    If sVoidReason.Trim.Length <= 0 Then Exit Sub

'                    Dim objComputeMst As New clsComputeScore_master
'                    objComputeMst._Isvoid = True
'                    objComputeMst._Voiddatetime = ConfigParameter._Object._CurrentDateAndTime
'                    objComputeMst._Voidreason = sVoidReason
'                    objComputeMst._Voiduserunkid = User._Object._Userunkid
'                    If objComputeMst.DeleteEmployeeWise(CInt(lvAssesmentList.SelectedItems(0).Tag), _
'                                                        CInt(lvAssesmentList.SelectedItems(0).SubItems(objcolhEmpId.Index).Text), _
'                                                        CInt(lvAssesmentList.SelectedItems(0).SubItems(objcolhPeriodId.Index).Text), enAssessmentMode.SELF_ASSESSMENT) = True Then
'                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 9, "Successfully voided Computation score"), enMsgBoxStyle.Information)
'                    Else
'                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 10, "Fail void Computation score process"), enMsgBoxStyle.Information)
'                        Exit Sub
'                    End If
'                Else
'                    Exit Sub
'                End If
'            End If
'            'Shani (24-May-2016) -- End

'            If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Are you sure you want to delete this Performance Evaluation?"), CType(enMsgBoxStyle.YesNo + enMsgBoxStyle.Information, enMsgBoxStyle)) = Windows.Forms.DialogResult.Yes Then
'                Dim frm As New frmReasonSelection
'                Dim mstrVoidReason As String = String.Empty
'                frm.displayDialog(enVoidCategoryType.ASSESSMENT, mstrVoidReason)
'                If mstrVoidReason.Length <= 0 Then
'                    Exit Sub
'                Else
'                    objEvaluation._Voidreason = mstrVoidReason
'                End If
'                frm = Nothing
'                objEvaluation._Isvoid = True
'                objEvaluation._Voiddatetime = ConfigParameter._Object._CurrentDateAndTime
'                objEvaluation._Voiduserunkid = User._Object._Userunkid

'                objEvaluation.Delete(CInt(lvAssesmentList.SelectedItems(0).Tag), enAssessmentMode.SELF_ASSESSMENT, CInt(lvAssesmentList.SelectedItems(0).SubItems(objcolhPeriodId.Index).Text))

'                If objEvaluation._Message <> "" Then
'                    eZeeMsgBox.Show(objEvaluation._Message, enMsgBoxStyle.Information)
'                Else
'                    Call FillList()
'                End If
'            End If
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "btnDelete_Click", mstrModuleName)
'        Finally
'        End Try
'    End Sub

'    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
'        Me.Close()
'    End Sub

'    Private Sub btnUnlockCommit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnUnlockCommit.Click
'        Dim sVoidReason As String = String.Empty
'        Try
'            If lvAssesmentList.SelectedItems.Count > 0 Then
'                If objEvaluation.isExist(enAssessmentMode.APPRAISER_ASSESSMENT, _
'                                             CInt(lvAssesmentList.SelectedItems(0).SubItems(objcolhEmpId.Index).Text), _
'                                             CInt(lvAssesmentList.SelectedItems(0).SubItems(objcolhPeriodId.Index).Text)) = True Then
'                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "Sorry, you cannot unlock General Assessment for the selected period. Reason : Assessment is already done for the selected period by Assessor(s)."), enMsgBoxStyle.Information)
'                    Exit Sub
'                End If

'                If objEvaluation.isExist(enAssessmentMode.REVIEWER_ASSESSMENT, _
'                                             CInt(lvAssesmentList.SelectedItems(0).SubItems(objcolhEmpId.Index).Text), _
'                                             CInt(lvAssesmentList.SelectedItems(0).SubItems(objcolhPeriodId.Index).Text)) = True Then
'                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 6, "Sorry, you cannot unlock General Assessment for the selected period. Reason : Assessment is already done for the selected period by Reviewer."), enMsgBoxStyle.Information)
'                    Exit Sub
'                End If


'                'Shani (24-May-2016) -- Start
'                If objEvaluation.CheckComputionIsExist(CInt(lvAssesmentList.SelectedItems(0).Tag), _
'                                                       enAssessmentMode.SELF_ASSESSMENT, _
'                                                       CInt(lvAssesmentList.SelectedItems(0).SubItems(objcolhPeriodId.Index).Text)) = True Then

'                    If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 8, "Sorry, you cannot unlock General Assessment for the selected period. Reason : Compute score process is already done for the selected period. Are you sure you want to Void computation score?"), enMsgBoxStyle.OkCancel) = Windows.Forms.DialogResult.OK Then
'                        Dim frm As New frmReasonSelection
'                        If User._Object._Isrighttoleft = True Then
'                            frm.RightToLeft = Windows.Forms.RightToLeft.Yes
'                            frm.RightToLeftLayout = True
'                            Call Language.ctlRightToLeftlayOut(frm)
'                        End If
'                        frm.displayDialog(enVoidCategoryType.ASSESSMENT, sVoidReason)
'                        If sVoidReason.Trim.Length <= 0 Then Exit Sub

'                        Dim objComputeMst As New clsComputeScore_master
'                        objComputeMst._Isvoid = True
'                        objComputeMst._Voiddatetime = ConfigParameter._Object._CurrentDateAndTime
'                        objComputeMst._Voidreason = sVoidReason
'                        objComputeMst._Voiduserunkid = User._Object._Userunkid
'                        If objComputeMst.DeleteEmployeeWise(CInt(lvAssesmentList.SelectedItems(0).Tag), _
'                                                            CInt(lvAssesmentList.SelectedItems(0).SubItems(objcolhEmpId.Index).Text), _
'                                                            CInt(lvAssesmentList.SelectedItems(0).SubItems(objcolhPeriodId.Index).Text), enAssessmentMode.SELF_ASSESSMENT) = True Then
'                            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 9, "Successfully voided Computation score"), enMsgBoxStyle.Information)
'                        Else
'                            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 10, "Fail void Computation score process"), enMsgBoxStyle.Information)
'                            Exit Sub
'                        End If
'                    Else
'                        Exit Sub
'                    End If
'                End If
'                'Shani (24-May-2016) -- End


'                If CInt(lvAssesmentList.SelectedItems(0).SubItems(colhPeriod.Index).Tag) = enStatusType.Open Then
'                    If objEvaluation.Unlock_Commit(CInt(lvAssesmentList.SelectedItems(0).SubItems(objcolhPeriodId.Index).Text), _
'                                                       CInt(lvAssesmentList.SelectedItems(0).SubItems(objcolhEmpId.Index).Text)) = True Then
'                        objEvaluation._Analysisunkid = CInt(lvAssesmentList.SelectedItems(0).Tag)
'                        objEvaluation._Iscommitted = False
'                        objEvaluation._Committeddatetime = Nothing
'                        objEvaluation.Update()
'                        If objEvaluation._Message <> "" Then
'                            eZeeMsgBox.Show(objEvaluation._Message)
'                        End If
'                        Call FillList()
'                    Else
'                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 7, "Sorry, you cannot unlock this committed information. Reason : Its already linked with Appraisal."), enMsgBoxStyle.Information)
'                        Exit Sub
'                    End If
'                End If
'            End If
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "btnUnlockCommit_Click", mstrModuleName)
'        Finally
'        End Try
'    End Sub

'    Private Sub objbtnReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnReset.Click
'        Try
'            cboEmployee.SelectedValue = 0
'            cboPeriod.SelectedValue = 0
'            cboYear.SelectedValue = 0
'            dtpAssessmentdate.Checked = False
'            chkShowCommited.CheckState = CheckState.Checked
'            'S.SANDEEP [04 JUN 2015] -- START
'            chkShowUncommited.CheckState = CheckState.Checked
'            'S.SANDEEP [04 JUN 2015] -- END
'            mstrAdvanceFilter = String.Empty
'            Call FillList()
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "objbtnReset_Click", mstrModuleName)
'        End Try
'    End Sub

'    Private Sub objbtnSearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearch.Click
'        Try
'            Call FillList()
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "objbtnSearch_Click", mstrModuleName)
'        End Try
'    End Sub

'    Private Sub objbtnSearchEmployee_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchEmployee.Click
'        Dim frm As New frmCommonSearch
'        Try
'            If User._Object._Isrighttoleft = True Then
'                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
'                frm.RightToLeftLayout = True
'                Call Language.ctlRightToLeftlayOut(frm)
'            End If

'            frm.ValueMember = cboEmployee.ValueMember
'            frm.DisplayMember = cboEmployee.DisplayMember
'            frm.CodeMember = "employeecode"
'            frm.DataSource = CType(cboEmployee.DataSource, DataTable)
'            If frm.DisplayDialog Then
'                cboEmployee.SelectedValue = frm.SelectedValue
'                cboEmployee.Focus()
'            End If
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "objbtnSearchEmployee_Click", mstrModuleName)
'        Finally
'            If frm IsNot Nothing Then frm.Dispose()
'        End Try
'    End Sub

'#End Region

'#Region " Controls "

'    Private Sub lnkAllocation_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lnkAllocation.LinkClicked
'        Try
'            Dim frm As New frmAdvanceSearch
'            frm.ShowDialog()
'            mstrAdvanceFilter = frm._GetFilterString
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "lnkAllocation_LinkClicked", mstrModuleName)
'        End Try
'    End Sub

'    Private Sub lvAssesmentList_ItemChecked(ByVal sender As Object, ByVal e As System.Windows.Forms.ItemCheckedEventArgs) Handles lvAssesmentList.ItemChecked
'        Try
'            If lvAssesmentList.CheckedItems.Count <= 0 Then Exit Sub
'            If e.Item.ForeColor = Color.Blue Then e.Item.Checked = False
'            If CInt(e.Item.SubItems(colhPeriod.Index).Tag) = enStatusType.Close Then e.Item.Checked = False
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "lvAssesmentList_ItemChecked", mstrModuleName)
'        End Try
'    End Sub

'    Private Sub lvAssesmentList_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lvAssesmentList.SelectedIndexChanged
'        Try
'            If lvAssesmentList.SelectedItems.Count <= 0 Then Exit Sub
'            If lvAssesmentList.SelectedItems(0).ForeColor = Color.Blue Then
'                btnEdit.Enabled = False : btnDelete.Enabled = False : btnUnlockCommit.Enabled = True
'            Else
'                btnEdit.Enabled = True : btnDelete.Enabled = True : btnUnlockCommit.Enabled = False
'            End If
'            'S.SANDEEP [28 MAY 2015] -- START
'            'ENHANCEMENT : NEW PRIVILEGE (Performance Assessment)
'            'btnUnlockCommit.Enabled = User._Object.Privilege._Allow_UnlockCommittedGeneralAssessment
'            btnUnlockCommit.Enabled = User._Object.Privilege._AllowtoUnlockcommittedSelfEvaluation
'            'S.SANDEEP [28 MAY 2015] -- END
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "lvAssessorList_SelectedIndexChanged", mstrModuleName)
'        Finally
'        End Try
'    End Sub

'#End Region

'    '<Language> This Auto Generated Text Please Do Not Modify it.
'#Region " Language & UI Settings "
'    Private Sub OtherSettings()
'        Try
'            Me.SuspendLayout()

'            Call SetLanguage()

'            Me.gbEmployeeInfo.GradientColor = GUI._eZeeContainerHeaderBackColor
'            Me.gbEmployeeInfo.ForeColor = GUI._eZeeContainerHeaderForeColor

'            Me.eZeeHeader.GradientColor1 = GUI._HeaderBackColor1
'            Me.eZeeHeader.GradientColor2 = GUI._HeaderBackColor2
'            Me.eZeeHeader.BorderColor = GUI._HeaderBorderColor
'            Me.eZeeHeader.HeaderTextForeColor = GUI._HeaderTitleFontColor
'            Me.eZeeHeader.DescriptionForeColor = GUI._HeaderMessageFontColor


'            Me.btnUnlockCommit.GradientBackColor = GUI._ButttonBackColor
'            Me.btnUnlockCommit.GradientForeColor = GUI._ButttonFontColor

'            Me.btnOperation.GradientBackColor = GUI._ButttonBackColor
'            Me.btnOperation.GradientForeColor = GUI._ButttonFontColor

'            Me.btnNew.GradientBackColor = GUI._ButttonBackColor
'            Me.btnNew.GradientForeColor = GUI._ButttonFontColor

'            Me.btnDelete.GradientBackColor = GUI._ButttonBackColor
'            Me.btnDelete.GradientForeColor = GUI._ButttonFontColor

'            Me.btnEdit.GradientBackColor = GUI._ButttonBackColor
'            Me.btnEdit.GradientForeColor = GUI._ButttonFontColor

'            Me.btnClose.GradientBackColor = GUI._ButttonBackColor
'            Me.btnClose.GradientForeColor = GUI._ButttonFontColor


'            Me.ResumeLayout()
'        Catch Ex As Exception
'            DisplayError.Show("-1", Ex.Message, "OtherSettings", mstrModuleName)
'        End Try
'    End Sub


'    Private Sub SetLanguage()
'        Try
'            Me.Text = Language._Object.getCaption(Me.Name, Me.Text)

'            Me.eZeeHeader.Title = Language._Object.getCaption(Me.eZeeHeader.Name & "_Title", Me.eZeeHeader.Title)
'            Me.eZeeHeader.Message = Language._Object.getCaption(Me.eZeeHeader.Name & "_Message", Me.eZeeHeader.Message)
'            Me.mnuUnlockCommitted.Text = Language._Object.getCaption(Me.mnuUnlockCommitted.Name, Me.mnuUnlockCommitted.Text)
'            Me.mnuGetFileFormat.Text = Language._Object.getCaption(Me.mnuGetFileFormat.Name, Me.mnuGetFileFormat.Text)
'            Me.mnuImportSelfAssessment.Text = Language._Object.getCaption(Me.mnuImportSelfAssessment.Name, Me.mnuImportSelfAssessment.Text)
'            Me.btnUnlockCommit.Text = Language._Object.getCaption(Me.btnUnlockCommit.Name, Me.btnUnlockCommit.Text)
'            Me.btnOperation.Text = Language._Object.getCaption(Me.btnOperation.Name, Me.btnOperation.Text)
'            Me.btnNew.Text = Language._Object.getCaption(Me.btnNew.Name, Me.btnNew.Text)
'            Me.btnDelete.Text = Language._Object.getCaption(Me.btnDelete.Name, Me.btnDelete.Text)
'            Me.btnEdit.Text = Language._Object.getCaption(Me.btnEdit.Name, Me.btnEdit.Text)
'            Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
'            Me.gbEmployeeInfo.Text = Language._Object.getCaption(Me.gbEmployeeInfo.Name, Me.gbEmployeeInfo.Text)
'            Me.chkShowUncommited.Text = Language._Object.getCaption(Me.chkShowUncommited.Name, Me.chkShowUncommited.Text)
'            Me.chkShowCommited.Text = Language._Object.getCaption(Me.chkShowCommited.Name, Me.chkShowCommited.Text)
'            Me.lnkAllocation.Text = Language._Object.getCaption(Me.lnkAllocation.Name, Me.lnkAllocation.Text)
'            Me.lblAssessmentdate.Text = Language._Object.getCaption(Me.lblAssessmentdate.Name, Me.lblAssessmentdate.Text)
'            Me.lblEmployee.Text = Language._Object.getCaption(Me.lblEmployee.Name, Me.lblEmployee.Text)
'            Me.lblYears.Text = Language._Object.getCaption(Me.lblYears.Name, Me.lblYears.Text)
'            Me.lblAssessmentPeriods.Text = Language._Object.getCaption(Me.lblAssessmentPeriods.Name, Me.lblAssessmentPeriods.Text)
'            Me.colhYear.Text = Language._Object.getCaption(CStr(Me.colhYear.Tag), Me.colhYear.Text)
'            Me.colhPeriod.Text = Language._Object.getCaption(CStr(Me.colhPeriod.Tag), Me.colhPeriod.Text)
'            Me.colhAssessmentdate.Text = Language._Object.getCaption(CStr(Me.colhAssessmentdate.Tag), Me.colhAssessmentdate.Text)
'            Me.colhPercent.Text = Language._Object.getCaption(CStr(Me.colhPercent.Tag), Me.colhPercent.Text)
'            Me.colhMode.Text = Language._Object.getCaption(CStr(Me.colhMode.Tag), Me.colhMode.Text)

'        Catch Ex As Exception
'            DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
'        End Try
'    End Sub


'    Private Sub SetMessages()
'        Try
'            Language.setMessage(mstrModuleName, 1, "Please select at least one Assessment to perform operation.")
'            Language.setMessage(mstrModuleName, 2, "Sorry, you cannot edit this information. Reason : Period is closed.")
'            Language.setMessage(mstrModuleName, 3, "Sorry, you cannot delete this information. Reason : Period is closed.")
'            Language.setMessage(mstrModuleName, 4, "Are you sure you want to delete this Performance Evaluation?")
'            Language.setMessage(mstrModuleName, 5, "Sorry, you cannot unlock General Assessment for the selected period. Reason : Assessment is already done for the selected period by Assessor(s).")
'            Language.setMessage(mstrModuleName, 6, "Sorry, you cannot unlock General Assessment for the selected period. Reason : Assessment is already done for the selected period by Reviewer.")
'            Language.setMessage(mstrModuleName, 7, "Sorry, you cannot unlock this committed information. Reason : Its already linked with Appraisal.")
'            Language.setMessage(mstrModuleName, 8, "Sorry, you cannot unlock General Assessment for the selected period. Reason : Compute score process is already done for the selected period. Are you sure you want to Void computation score?")
'            Language.setMessage(mstrModuleName, 9, "Successfully voided Computation score")
'            Language.setMessage(mstrModuleName, 10, "Fail void Computation score process")
'            Language.setMessage(mstrModuleName, 11, "Sorry, you cannot edit General Assessment for the selected period. Reason : Compute score process is already done for the selected period. Are you sure you want to Void computation score?")
'            Language.setMessage(mstrModuleName, 12, "Sorry, you cannot delete General Assessment for the selected period. Reason : Compute score process is already done for the selected period. Are you sure you want to Void computation score?")

'        Catch Ex As Exception
'            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
'        End Try
'    End Sub
'#End Region 'Language & UI Settings
'    '</Language>
'End Class