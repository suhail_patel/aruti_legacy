﻿#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data

#End Region

Public Class frmPerformanceEvaluation

#Region " Private Variables "

    Private mstrModuleName As String = "frmPerformanceEvaluation"
    Private mblnCancel As Boolean = True
    Private menAction As enAction = enAction.ADD_ONE
    Private objEAnalysisMst As clsevaluation_analysis_master
    Private objGoalsTran As clsgoal_analysis_tran
    Private objCAssessTran As clscompetency_analysis_tran
    Private objCCustomTran As clscompeteny_customitem_tran
    Private mintAssessAnalysisUnkid As Integer = -1
    Private mintEmplId As Integer = 0
    Private mintPeriodId As Integer = 0
    Private mintYearUnkid As Integer = 0
    Private mintAssessorId As Integer = 0
    Private menAssess As enAssessmentMode
    Private mdtBSC_Evaluation As DataTable
    Private mdtGE_Evaluation As DataTable
    Private mdtCustomEvaluation As DataTable
    Private dtBSC_TabularGrid As New DataTable
    Private dtGE_TabularGrid As New DataTable
    Private dtCustomTabularGrid As New DataTable
    Private iWeightTotal As Decimal = 0
    Private dsHeaders As New DataSet
    Private iHeaderId As Integer = 0
    Private iExOrdr As Integer = 0
    Private iLinkedFieldId As Integer
    Private iMappingUnkid As Integer
    Private pCell As New clsMergeCell
    Private img As Image = New Drawing.Bitmap(1, 1).Clone
    Private mblIsFormLoad As Boolean = False
    Private xTotalWeightAssigned As Decimal = 0
    Private objCmpMaster As clsassess_computation_master
    'S.SANDEEP [21 JAN 2015] -- START
    Private mdecItemWeight As Decimal = 0
    Private mdecMaxScale As Decimal = 0
    'S.SANDEEP [21 JAN 2015] -- END

    'S.SANDEEP [31 AUG 2016] -- START
    'ENHANCEMENT : INCLUSION OF GRADES ON ASSESSMENT GROUP {BY ANDREW}
    Private imgBlankIcon As Image = My.Resources.blankImage
    Private imgInforIcon As Image = My.Resources.information
    'S.SANDEEP [31 AUG 2016] -- START

    'S.SANDEEP |13-NOV-2020| -- START
    'ISSUE/ENHANCEMENT : COMPETENCIES
    Private mintEvaluationTypeId As Integer = clsevaluation_analysis_master.enPAEvalTypeId.EO_BOTH
    'S.SANDEEP |13-NOV-2020| -- END

#End Region

#Region " Display Dialog "

    Public Function displayDialog(ByRef intUnkid As Integer, ByVal eAction As enAction, ByVal eAssess As enAssessmentMode) As Boolean
        Try
            mintAssessAnalysisUnkid = intUnkid
            menAction = eAction
            menAssess = eAssess
            txtInstruction.Text = ConfigParameter._Object._Assessment_Instructions
            objbtnNext.Enabled = False : objbtnBack.Enabled = False
            objlblCaption.Text = Language.getMessage(mstrModuleName, 1, "Instructions")
            objpnlInstruction.Visible = True : objpnlInstruction.Dock = DockStyle.Fill
            objpnlInstruction.BringToFront()
            'btnSave.Enabled = False : btnSaveCommit.Enabled = False

            objlblValue1.Visible = False : objlblValue2.Visible = False
            objlblValue3.Visible = False : objlblValue4.Visible = False

            'S.SANDEEP [04 MAR 2015] -- START
            lnkCopyScore.Enabled = False
            If eAssess = enAssessmentMode.REVIEWER_ASSESSMENT Then
                lnkCopyScore.Visible = True
            Else
                lnkCopyScore.Visible = False
            End If
            'S.SANDEEP [04 MAR 2015] -- END

            'S.SANDEEP [25-JAN-2017] -- START
            'ISSUE/ENHANCEMENT : Pefromance Module Left Out Changes
            If lnkCopyScore.Visible = True Then lnkViewApproveRejectRemark.Location = New Point(569, 4)
            'S.SANDEEP [25-JAN-2017] -- END

            Me.ShowDialog()
            intUnkid = mintAssessAnalysisUnkid
            Return Not mblnCancel
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "displayDialog", mstrModuleName)
        Finally
        End Try
    End Function

#End Region

#Region " Private Methods "

    '--------------------------------------------- COMMON METHODS FOR ALL
    Private Sub SetVisibility()
        Try
            If menAssess = enAssessmentMode.SELF_ASSESSMENT Then
                cboAssessor.Enabled = False
                lblAssessor.Enabled = False
                objbtnSearchAssessor.Enabled = False
                radInternalAssessor.Checked = False : radExternalAssessor.Checked = False
                radInternalAssessor.Enabled = False : radExternalAssessor.Enabled = False
                lblReviewer.Visible = False
                cboReviewer.Visible = False
                objbtnSearchReviewer.Visible = False
            ElseIf menAssess = enAssessmentMode.APPRAISER_ASSESSMENT Then
                cboAssessor.Visible = True
                lblAssessor.Visible = True
                objbtnSearchAssessor.Enabled = True
                lblReviewer.Visible = False
                cboReviewer.Visible = False
                objbtnSearchReviewer.Visible = False
                radInternalAssessor.Checked = True
                radInternalAssessor.Enabled = True : radExternalAssessor.Enabled = True
            ElseIf menAssess = enAssessmentMode.REVIEWER_ASSESSMENT Then
                cboAssessor.Visible = False
                lblAssessor.Visible = False
                objbtnSearchAssessor.Visible = False
                lblReviewer.Visible = True
                lblReviewer.Visible = True
                cboReviewer.Visible = True
                objbtnSearchReviewer.Visible = True
                radInternalAssessor.Checked = False : radExternalAssessor.Checked = False
                radInternalAssessor.Enabled = False : radExternalAssessor.Enabled = False
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetVisibility", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub SetColor()
        Try
            cboEmployee.BackColor = GUI.ColorComp
            cboPeriod.BackColor = GUI.ColorComp
            cboAssessor.BackColor = GUI.ColorComp
            cboReviewer.BackColor = GUI.ColorComp
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetColor", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub FillCombo()
        Dim dsCombos As New DataSet
        Dim objEmp As New clsEmployee_Master
        Dim objPeriod As New clscommom_period_Tran
        Try
            If menAssess = enAssessmentMode.SELF_ASSESSMENT Then

                'S.SANDEEP [04 JUN 2015] -- START
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                'If ConfigParameter._Object._IsIncludeInactiveEmp = False Then
                '    dsCombos = objEmp.GetEmployeeList("Emp", True, , , , , , , , , , , , , eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate))
                'Else
                '    dsCombos = objEmp.GetEmployeeList("Emp", True, , , , , , , , , , , , , ConfigParameter._Object._CurrentDateAndTime, ConfigParameter._Object._CurrentDateAndTime)
                'End If
                dsCombos = objEmp.GetEmployeeList(FinancialYear._Object._DatabaseName, _
                                                 User._Object._Userunkid, _
                                                 FinancialYear._Object._YearUnkid, _
                                                 Company._Object._Companyunkid, _
                                                 eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                 eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                 ConfigParameter._Object._UserAccessModeSetting, _
                                                 True, ConfigParameter._Object._IsIncludeInactiveEmp, "Emp", True)
                'S.SANDEEP [04 JUN 2015] -- END
                With cboEmployee
                    .ValueMember = "employeeunkid"
                    .DisplayMember = "employeename"
                    .DataSource = dsCombos.Tables(0)
                    .SelectedValue = 0
                End With
            ElseIf menAssess = enAssessmentMode.REVIEWER_ASSESSMENT Then

                'S.SANDEEP [04 JUN 2015] -- START
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                'dsCombos = objEAnalysisMst.getAssessorComboList("Assessor", True, True)

                dsCombos = objEAnalysisMst.getAssessorComboList(FinancialYear._Object._DatabaseName, _
                                                             User._Object._Userunkid, _
                                                             eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                             eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), True, _
                                                             ConfigParameter._Object._IsIncludeInactiveEmp, "Assessor", clsAssessor.enARVisibilityTypeId.VISIBLE, True, True) 'S.SANDEEP [27 DEC 2016] -- START {clsAssessor.enARVisibilityTypeId.VISIBLE}-- END
                'S.SANDEEP [04 JUN 2015] -- END

                With cboReviewer
                    .ValueMember = "Id"
                    .DisplayMember = "Name"
                    .DataSource = dsCombos.Tables("Assessor")
                    .SelectedValue = 0
                End With
            End If

            RemoveHandler cboPeriod.SelectedIndexChanged, AddressOf cboPeriod_SelectedIndexChanged
            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'dsCombos = objPeriod.getListForCombo(enModuleReference.Assessment, 0, "APeriod", True, 1)
            dsCombos = objPeriod.getListForCombo(enModuleReference.Assessment, 0, FinancialYear._Object._DatabaseName, FinancialYear._Object._Database_Start_Date, "APeriod", True, 1)
            'Sohail (21 Aug 2015) -- End
            With cboPeriod
                .ValueMember = "periodunkid"
                .DisplayMember = "name"
                .DataSource = dsCombos.Tables("APeriod")
                .SelectedValue = 0
            End With
            AddHandler cboPeriod.SelectedIndexChanged, AddressOf cboPeriod_SelectedIndexChanged

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub PanelVisibility()
        Try
            Dim xVal As Integer = -1
            If iHeaderId >= 0 Then
                objlblCaption.Text = dsHeaders.Tables(0).Rows(iHeaderId).Item("Name").ToString
                xVal = dsHeaders.Tables(0).Rows(iHeaderId).Item("Id")
            Else
                objlblCaption.Text = Language.getMessage(mstrModuleName, 1, "Instructions")
                xVal = -1
            End If
            'Select Case iHeaderId
            '    Case 0, -1
            '        btnSave.Enabled = False : btnSaveCommit.Enabled = False
            '    Case Else
            '        btnSave.Enabled = True
            'End Select
            'If iHeaderId >= dsHeaders.Tables(0).Rows.Count - 1 Then
            '    btnSaveCommit.Enabled = True
            'Else
            '    btnSaveCommit.Enabled = False
            'End If
            Select Case xVal
                Case -1 'Instruction
                    objpnlBSC.Visible = False
                    objpnlGE.Visible = False
                    objpnlCItems.Visible = False

                    objpnlInstruction.Visible = True
                    objpnlInstruction.Dock = DockStyle.Fill
                    objpnlInstruction.BringToFront()

                Case -3  'Balance Score Card
                    objpnlInstruction.Visible = False
                    objpnlGE.Visible = False
                    objpnlCItems.Visible = False

                    If mdtBSC_Evaluation.Rows.Count <= 0 Or menAction = enAction.EDIT_ONE Then
                        Call Fill_BSC_Evaluation()
                    End If
                    Call SetTotals(dgvBSC)
                    objpnlBSC.Visible = True
                    objpnlBSC.Dock = DockStyle.Fill
                    objpnlBSC.BringToFront()

                Case -2  'Competencies
                    objpnlInstruction.Visible = False
                    objpnlBSC.Visible = False
                    objpnlCItems.Visible = False

                    If mdtGE_Evaluation.Rows.Count <= 0 Or menAction = enAction.EDIT_ONE Then
                        Call Fill_GE_Evaluation()
                    End If
                    Call SetTotals(dgvGE)
                    objpnlGE.Visible = True
                    objpnlGE.Dock = DockStyle.Fill
                    objpnlGE.BringToFront()

                    '    'S.SANDEEP [25-JAN-2017] -- START
                    '    'ISSUE/ENHANCEMENT :  Pefromance Module Left Out Changes
                    'Case 99999  'APPROVE REJECT ASSESSMENT
                    '    objpnlInstruction.Visible = False
                    '    objpnlBSC.Visible = False
                    '    objpnlCItems.Visible = False
                    '    objpnlGE.Visible = False

                    '    'S.SANDEEP [25-JAN-2017] -- END

                Case Else  'Dynamic Custom Headers
                    objpnlInstruction.Visible = False
                    objpnlBSC.Visible = False
                    objpnlGE.Visible = False

                    Call Fill_Custom_Grid()
                    Call Fill_Custom_Evaluation_Data()
                    objpnlCItems.Visible = True
                    objpnlCItems.Dock = DockStyle.Fill
                    objpnlCItems.BringToFront()

            End Select
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "PanelVisibility", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Function Validation() As Boolean
        Try
            If CInt(cboEmployee.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 16, "Emplolyee is compulsory information.Please Select Emplolyee."), enMsgBoxStyle.Information)
                cboEmployee.Select()
                Return False
            ElseIf CInt(cboPeriod.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 17, "Period is compulsory information.Please Select Period."), enMsgBoxStyle.Information)
                cboPeriod.Select()
                Return False
            ElseIf CInt(cboAssessor.SelectedValue) = 0 And menAssess = enAssessmentMode.APPRAISER_ASSESSMENT Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 18, "Assessors is compulsory information.Please Select Assessors."), enMsgBoxStyle.Information)
                cboAssessor.Select()
                Return False
            End If

            'S.SANDEEP [09 APR 2015] -- START
            'Dim dsYr As New DataSet : Dim oCompany As New clsCompany_Master
            'dsYr = oCompany.GetFinancialYearList(Company._Object._Companyunkid, User._Object._Userunkid, "List", mintYearUnkid)
            'If dsYr.Tables("List").Rows.Count > 0 Then
            '    If dtpAssessdate.Value.Date > eZeeDate.convertDate(dsYr.Tables("List").Rows(0).Item("end_date").ToString).Date Or _
            '       dtpAssessdate.Value.Date < eZeeDate.convertDate(dsYr.Tables("List").Rows(0).Item("start_date").ToString).Date Then
            '        Dim strMsg As String = Language.getMessage(mstrModuleName, 19, "Assessment date should be in between ") & eZeeDate.convertDate(dsYr.Tables("List").Rows(0).Item("start_date").ToString).Date & _
            '                               Language.getMessage(mstrModuleName, 20, " And ") & eZeeDate.convertDate(dsYr.Tables("List").Rows(0).Item("end_date").ToString).Date
            '        eZeeMsgBox.Show(strMsg, enMsgBoxStyle.Information)
            '        dtpAssessdate.Focus()
            '        Return False
            '    End If
            'Else
            '    If dtpAssessdate.Value.Date > FinancialYear._Object._Database_End_Date.Date Or _
            '       dtpAssessdate.Value.Date < FinancialYear._Object._Database_Start_Date.Date Then
            '        Dim strMsg As String = Language.getMessage(mstrModuleName, 19, "Assessment date should be in between ") & FinancialYear._Object._Database_Start_Date.Date & _
            '                               Language.getMessage(mstrModuleName, 20, " And ") & FinancialYear._Object._Database_End_Date.Date
            '        eZeeMsgBox.Show(strMsg, enMsgBoxStyle.Information)
            '        dtpAssessdate.Focus()
            '        Return False
            '    End If
            'End If
            Dim objPrd As New clscommom_period_Tran
            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'objPrd._Periodunkid = CInt(cboPeriod.SelectedValue)
            objPrd._Periodunkid(FinancialYear._Object._DatabaseName) = CInt(cboPeriod.SelectedValue)
            'Sohail (21 Aug 2015) -- End
            If dtpAssessdate.Value.Date <= objPrd._Start_Date.Date Then
                Dim strMsg As String = Language.getMessage(mstrModuleName, 36, "Sorry, assessment date should be greater than") & " ( " & objPrd._Start_Date.Date.ToShortDateString & " )."
                eZeeMsgBox.Show(strMsg, enMsgBoxStyle.Information)
                dtpAssessdate.Focus()
                objPrd = Nothing
                Return False
            End If
            objPrd = Nothing
            'S.SANDEEP [09 APR 2015] -- END


            If menAssess = enAssessmentMode.REVIEWER_ASSESSMENT Then
                If CInt(cboReviewer.SelectedValue) <= 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 21, "Reviewer is mandatory information. Please create Reviewer first. Assessment -> Setups -> Reviewer."), enMsgBoxStyle.Information)
                    cboReviewer.Focus()
                    Return False
                End If
            End If

            Return True

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Validation", mstrModuleName)
        Finally
        End Try
    End Function

    Private Sub SetValue()
        Try
            objEAnalysisMst._Periodunkid = CInt(cboPeriod.SelectedValue)
            Select Case menAssess
                Case enAssessmentMode.SELF_ASSESSMENT
                    objEAnalysisMst._Selfemployeeunkid = CInt(cboEmployee.SelectedValue)
                    objEAnalysisMst._Assessedemployeeunkid = -1
                    objEAnalysisMst._Assessormasterunkid = -1
                    objEAnalysisMst._Assessoremployeeunkid = -1
                    objEAnalysisMst._Reviewerunkid = -1
                    objEAnalysisMst._Assessmodeid = enAssessmentMode.SELF_ASSESSMENT
                Case enAssessmentMode.APPRAISER_ASSESSMENT
                    objEAnalysisMst._Selfemployeeunkid = -1
                    objEAnalysisMst._Assessedemployeeunkid = CInt(cboEmployee.SelectedValue)
                    If radInternalAssessor.Checked = True Then
                        objEAnalysisMst._Assessormasterunkid = CInt(cboAssessor.SelectedValue)
                        Dim intEmployeeId As Integer = -1
                        intEmployeeId = objEAnalysisMst.GetAssessorEmpId(CInt(cboAssessor.SelectedValue))
                        objEAnalysisMst._Assessoremployeeunkid = intEmployeeId
                    ElseIf radExternalAssessor.Checked = True Then
                        objEAnalysisMst._Ext_Assessorunkid = CInt(cboAssessor.SelectedValue)
                        objEAnalysisMst._Assessormasterunkid = -1
                        objEAnalysisMst._Assessoremployeeunkid = -1
                    End If
                    objEAnalysisMst._Reviewerunkid = -1
                    objEAnalysisMst._Assessmodeid = enAssessmentMode.APPRAISER_ASSESSMENT
                Case enAssessmentMode.REVIEWER_ASSESSMENT
                    objEAnalysisMst._Selfemployeeunkid = -1
                    objEAnalysisMst._Assessormasterunkid = -1
                    objEAnalysisMst._Assessoremployeeunkid = -1
                    objEAnalysisMst._Assessedemployeeunkid = CInt(cboEmployee.SelectedValue)
                    objEAnalysisMst._Reviewerunkid = User._Object._Userunkid
                    objEAnalysisMst._Assessmodeid = enAssessmentMode.REVIEWER_ASSESSMENT
                    objEAnalysisMst._Assessormasterunkid = CInt(cboReviewer.SelectedValue)
                    Dim intEmployeeId As Integer = -1
                    intEmployeeId = objEAnalysisMst.GetAssessorEmpId(CInt(cboReviewer.SelectedValue))
                    objEAnalysisMst._Reviewerunkid = intEmployeeId
            End Select
            objEAnalysisMst._Assessmentdate = dtpAssessdate.Value
            objEAnalysisMst._Userunkid = User._Object._Userunkid
            If objEAnalysisMst._Committeddatetime <> Nothing Then
                objEAnalysisMst._Committeddatetime = objEAnalysisMst._Committeddatetime
            Else
                objEAnalysisMst._Committeddatetime = Nothing
            End If
            Select Case menAction
                Case enAction.ADD_ONE, enAction.ADD_CONTINUE
                    objEAnalysisMst._Isvoid = False
                    objEAnalysisMst._Voiduserunkid = -1
                    objEAnalysisMst._Voiddatetime = Nothing
                    objEAnalysisMst._Voidreason = ""
                Case enAction.EDIT_ONE
                    objEAnalysisMst._Isvoid = objEAnalysisMst._Isvoid
                    objEAnalysisMst._Voiduserunkid = objEAnalysisMst._Voiduserunkid
                    objEAnalysisMst._Voiddatetime = objEAnalysisMst._Voiddatetime
                    objEAnalysisMst._Voidreason = objEAnalysisMst._Voidreason
            End Select
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetValue", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub GetValue()
        Try
            Select Case menAssess
                Case enAssessmentMode.SELF_ASSESSMENT
                    cboEmployee.SelectedValue = objEAnalysisMst._Selfemployeeunkid
                Case enAssessmentMode.APPRAISER_ASSESSMENT
                    If objEAnalysisMst._Ext_Assessorunkid > 0 Then
                        radExternalAssessor.Checked = True
                        cboAssessor.SelectedValue = objEAnalysisMst._Ext_Assessorunkid
                    Else
                        radInternalAssessor.Checked = True
                        cboAssessor.SelectedValue = objEAnalysisMst._Assessormasterunkid
                    End If
                    cboEmployee.SelectedValue = objEAnalysisMst._Assessedemployeeunkid
                Case enAssessmentMode.REVIEWER_ASSESSMENT
                    cboReviewer.SelectedValue = objEAnalysisMst._Assessormasterunkid
                    cboEmployee.SelectedValue = objEAnalysisMst._Assessedemployeeunkid
            End Select
            cboPeriod.SelectedValue = objEAnalysisMst._Periodunkid
            If objEAnalysisMst._Assessmentdate <> Nothing Then
                dtpAssessdate.Value = objEAnalysisMst._Assessmentdate
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetValue", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub SetTotals(ByVal iDG As DataGridView)
        Try
            Select Case iDG.Name.ToUpper
                Case dgvBSC.Name.ToUpper
                    objlblValue1.Text = Language.getMessage(mstrModuleName, 5, "Total Weight :") & " " & CDbl(iWeightTotal).ToString
                    objlblValue1.Visible = True : objlblValue2.Visible = True

                    'S.SANDEEP [21 JAN 2015] -- START
                    'If menAssess = enAssessmentMode.SELF_ASSESSMENT Then
                    '    If mdtBSC_Evaluation IsNot Nothing AndAlso mdtBSC_Evaluation.Rows.Count > 0 Then
                    '        objlblValue2.Text = Language.getMessage(mstrModuleName, 6, "Self Score :") & " " & CDbl(mdtBSC_Evaluation.Compute("SUM(computed_value)", "AUD <> 'D'")).ToString
                    '    End If
                    '    objlblValue3.Visible = False : objlblValue4.Visible = False
                    'ElseIf menAssess = enAssessmentMode.APPRAISER_ASSESSMENT Then
                    '    If dtBSC_TabularGrid.Rows.Count > 0 Then objlblValue2.Text = Language.getMessage(mstrModuleName, 6, "Self Score :") & " " & CDbl(dtBSC_TabularGrid.Compute("SUM(escore)", "")).ToString
                    '    If mdtBSC_Evaluation IsNot Nothing AndAlso mdtBSC_Evaluation.Rows.Count > 0 Then
                    '        objlblValue3.Text = Language.getMessage(mstrModuleName, 7, "Assessor Score :") & " " & CDbl(mdtBSC_Evaluation.Compute("SUM(computed_value)", "AUD <> 'D'")).ToString
                    '    End If
                    '    objlblValue3.Visible = True : objlblValue4.Visible = False
                    'ElseIf menAssess = enAssessmentMode.REVIEWER_ASSESSMENT Then
                    '    If dtBSC_TabularGrid.Rows.Count > 0 Then objlblValue2.Text = Language.getMessage(mstrModuleName, 6, "Self Score :") & " " & CDbl(dtBSC_TabularGrid.Compute("SUM(escore)", "")).ToString
                    '    If dtBSC_TabularGrid.Rows.Count > 0 Then objlblValue3.Text = Language.getMessage(mstrModuleName, 7, "Assessor Score :") & " " & CDbl(dtBSC_TabularGrid.Compute("SUM(ascore)", "")).ToString

                    '    If mdtBSC_Evaluation IsNot Nothing AndAlso mdtBSC_Evaluation.Rows.Count > 0 Then
                    '        objlblValue4.Text = Language.getMessage(mstrModuleName, 8, "Reviewer Score :") & " " & CDbl(mdtBSC_Evaluation.Compute("SUM(computed_value)", "AUD <> 'D'")).ToString
                    '    End If
                    '    objlblValue3.Visible = True : objlblValue4.Visible = True
                    'End If

                    If menAssess = enAssessmentMode.SELF_ASSESSMENT Then
                        If mdtBSC_Evaluation IsNot Nothing AndAlso mdtBSC_Evaluation.Rows.Count > 0 Then

                            'S.SANDEEP [04 JUN 2015] -- START
                            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                            'objlblValue2.Text = Language.getMessage(mstrModuleName, 6, "Self Score :") & " " & objEAnalysisMst.Compute_Score(menAssess, True, ConfigParameter._Object._ScoringOptionId, enAssess_Computation_Formulas.BSC_EMP_TOTAL_SCORE, cboEmployee.SelectedValue, cboPeriod.SelectedValue, , mdtBSC_Evaluation)

                            'Shani (23-Nov-2016) -- Start
                            'Enhancement - Add New PA Enhancement (Agreed Score,exc) Given by Akiba,KBC,TNP exc...
                            'objlblValue2.Text = Language.getMessage(mstrModuleName, 6, "Self Score :") & " " & objEAnalysisMst.Compute_Score(menAssess, True, ConfigParameter._Object._ScoringOptionId, enAssess_Computation_Formulas.BSC_EMP_TOTAL_SCORE, eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), cboEmployee.SelectedValue, cboPeriod.SelectedValue, , mdtBSC_Evaluation, , , ConfigParameter._Object._Self_Assign_Competencies) 'S.SANDEEP [08 Jan 2016] -- START{ConfigParameter._Object._Self_Assign_Competencies} -- END
                            objlblValue2.Text = Language.getMessage(mstrModuleName, 6, "Self Score :") & " " & _
                            objEAnalysisMst.Compute_Score(xAssessMode:=menAssess, _
                                                          IsBalanceScoreCard:=True, _
                                                          xScoreOptId:=ConfigParameter._Object._ScoringOptionId, _
                                                          xCompute_Formula:=enAssess_Computation_Formulas.BSC_EMP_TOTAL_SCORE, _
                                                          xEmployeeAsOnDate:=eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                          xEmployeeId:=cboEmployee.SelectedValue, _
                                                          xPeriodId:=cboPeriod.SelectedValue, _
                                                          xUsedAgreedScore:=ConfigParameter._Object._IsUseAgreedScore, _
                                                          xDataTable:=mdtBSC_Evaluation, _
                                                          xSelfAssignedCompetencies:=ConfigParameter._Object._Self_Assign_Competencies)

                            'Shani (23-Nov-2016) -- End


                            'S.SANDEEP [04 JUN 2015] -- END

                        End If
                        objlblValue3.Visible = False : objlblValue4.Visible = False
                    ElseIf menAssess = enAssessmentMode.APPRAISER_ASSESSMENT Then
                        'S.SANDEEP [04 JUN 2015] -- START
                        'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                        'If dtBSC_TabularGrid.Rows.Count > 0 Then objlblValue2.Text = Language.getMessage(mstrModuleName, 6, "Self Score :") & " " & objEAnalysisMst.Compute_Score(enAssessmentMode.SELF_ASSESSMENT, True, ConfigParameter._Object._ScoringOptionId, enAssess_Computation_Formulas.BSC_EMP_TOTAL_SCORE, cboEmployee.SelectedValue, cboPeriod.SelectedValue, , dtBSC_TabularGrid)
                        'If mdtBSC_Evaluation IsNot Nothing AndAlso mdtBSC_Evaluation.Rows.Count > 0 Then
                        '    objlblValue3.Text = Language.getMessage(mstrModuleName, 7, "Assessor Score :") & " " & objEAnalysisMst.Compute_Score(menAssess, True, ConfigParameter._Object._ScoringOptionId, enAssess_Computation_Formulas.BSC_ASR_TOTAL_SCORE, cboEmployee.SelectedValue, cboPeriod.SelectedValue, , mdtBSC_Evaluation, cboAssessor.SelectedValue)
                        'End If

                        'Shani (23-Nov-2016) -- Start
                        'Enhancement - Add New PA Enhancement (Agreed Score,exc) Given by Akiba,KBC,TNP exc...
                        'If dtBSC_TabularGrid.Rows.Count > 0 Then objlblValue2.Text = Language.getMessage(mstrModuleName, 6, "Self Score :") & " " & objEAnalysisMst.Compute_Score(enAssessmentMode.SELF_ASSESSMENT, True, ConfigParameter._Object._ScoringOptionId, enAssess_Computation_Formulas.BSC_EMP_TOTAL_SCORE, eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), cboEmployee.SelectedValue, cboPeriod.SelectedValue, , dtBSC_TabularGrid, , , ConfigParameter._Object._Self_Assign_Competencies) 'S.SANDEEP [08 Jan 2016] -- START{ConfigParameter._Object._Self_Assign_Competencies} -- END
                        'If mdtBSC_Evaluation IsNot Nothing AndAlso mdtBSC_Evaluation.Rows.Count > 0 Then
                        '    objlblValue3.Text = Language.getMessage(mstrModuleName, 7, "Assessor Score :") & " " & objEAnalysisMst.Compute_Score(menAssess, True, ConfigParameter._Object._ScoringOptionId, enAssess_Computation_Formulas.BSC_ASR_TOTAL_SCORE, eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), cboEmployee.SelectedValue, cboPeriod.SelectedValue, , mdtBSC_Evaluation, cboAssessor.SelectedValue, , ConfigParameter._Object._Self_Assign_Competencies) 'S.SANDEEP [08 Jan 2016] -- START{ConfigParameter._Object._Self_Assign_Competencies} -- END
                        'End If

                        If dtBSC_TabularGrid.Rows.Count > 0 Then objlblValue2.Text = Language.getMessage(mstrModuleName, 6, "Self Score :") & " " & _
                        objEAnalysisMst.Compute_Score(xAssessMode:=enAssessmentMode.SELF_ASSESSMENT, _
                                                      IsBalanceScoreCard:=True, _
                                                      xScoreOptId:=ConfigParameter._Object._ScoringOptionId, _
                                                      xCompute_Formula:=enAssess_Computation_Formulas.BSC_EMP_TOTAL_SCORE, _
                                                      xEmployeeAsOnDate:=eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                      xEmployeeId:=cboEmployee.SelectedValue, _
                                                      xPeriodId:=cboPeriod.SelectedValue, _
                                                      xUsedAgreedScore:=ConfigParameter._Object._IsUseAgreedScore, _
                                                      xDataTable:=dtBSC_TabularGrid, _
                                                      xSelfAssignedCompetencies:=ConfigParameter._Object._Self_Assign_Competencies)
                        If mdtBSC_Evaluation IsNot Nothing AndAlso mdtBSC_Evaluation.Rows.Count > 0 Then
                            objlblValue3.Text = Language.getMessage(mstrModuleName, 7, "Assessor Score :") & " " & _
                            objEAnalysisMst.Compute_Score(xAssessMode:=menAssess, _
                                                          IsBalanceScoreCard:=True, _
                                                          xScoreOptId:=ConfigParameter._Object._ScoringOptionId, _
                                                          xCompute_Formula:=enAssess_Computation_Formulas.BSC_ASR_TOTAL_SCORE, _
                                                          xEmployeeAsOnDate:=eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                          xEmployeeId:=cboEmployee.SelectedValue, _
                                                          xPeriodId:=cboPeriod.SelectedValue, _
                                                          xUsedAgreedScore:=ConfigParameter._Object._IsUseAgreedScore, _
                                                          xDataTable:=mdtBSC_Evaluation, _
                                                          xAssessorReviewerId:=cboAssessor.SelectedValue, _
                                                          xSelfAssignedCompetencies:=ConfigParameter._Object._Self_Assign_Competencies)
                        End If
                        'Shani (23-Nov-2016) -- End

                        'S.SANDEEP [04 JUN 2015] -- END
                        objlblValue3.Visible = True : objlblValue4.Visible = False
                    ElseIf menAssess = enAssessmentMode.REVIEWER_ASSESSMENT Then

                        'S.SANDEEP [04 JUN 2015] -- START
                        'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                        'If dtBSC_TabularGrid.Rows.Count > 0 Then objlblValue2.Text = Language.getMessage(mstrModuleName, 6, "Self Score :") & " " & objEAnalysisMst.Compute_Score(enAssessmentMode.SELF_ASSESSMENT, True, ConfigParameter._Object._ScoringOptionId, enAssess_Computation_Formulas.BSC_EMP_TOTAL_SCORE, cboEmployee.SelectedValue, cboPeriod.SelectedValue, , dtBSC_TabularGrid)
                        'If dtBSC_TabularGrid.Rows.Count > 0 Then objlblValue3.Text = Language.getMessage(mstrModuleName, 7, "Assessor Score :") & " " & objEAnalysisMst.Compute_Score(enAssessmentMode.APPRAISER_ASSESSMENT, True, ConfigParameter._Object._ScoringOptionId, enAssess_Computation_Formulas.BSC_ASR_TOTAL_SCORE, cboEmployee.SelectedValue, cboPeriod.SelectedValue, , dtBSC_TabularGrid)

                        'If mdtBSC_Evaluation IsNot Nothing AndAlso mdtBSC_Evaluation.Rows.Count > 0 Then
                        '    objlblValue4.Text = Language.getMessage(mstrModuleName, 8, "Reviewer Score :") & " " & objEAnalysisMst.Compute_Score(menAssess, True, ConfigParameter._Object._ScoringOptionId, enAssess_Computation_Formulas.BSC_REV_TOTAL_SCORE, cboEmployee.SelectedValue, cboPeriod.SelectedValue, , mdtBSC_Evaluation, cboReviewer.SelectedValue)
                        'End If

                        'Shani (23-Nov-2016) -- Start
                        'Enhancement - Add New PA Enhancement (Agreed Score,exc) Given by Akiba,KBC,TNP exc...
                        'If dtBSC_TabularGrid.Rows.Count > 0 Then objlblValue2.Text = Language.getMessage(mstrModuleName, 6, "Self Score :") & " " & objEAnalysisMst.Compute_Score(enAssessmentMode.SELF_ASSESSMENT, True, ConfigParameter._Object._ScoringOptionId, enAssess_Computation_Formulas.BSC_EMP_TOTAL_SCORE, eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), cboEmployee.SelectedValue, cboPeriod.SelectedValue, , dtBSC_TabularGrid, , , ) 'S.SANDEEP [08 Jan 2016] -- START{ConfigParameter._Object._Self_Assign_Competencies} -- END
                        'If dtBSC_TabularGrid.Rows.Count > 0 Then objlblValue3.Text = Language.getMessage(mstrModuleName, 7, "Assessor Score :") & " " & objEAnalysisMst.Compute_Score(enAssessmentMode.APPRAISER_ASSESSMENT, True, ConfigParameter._Object._ScoringOptionId, enAssess_Computation_Formulas.BSC_ASR_TOTAL_SCORE, eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), cboEmployee.SelectedValue, cboPeriod.SelectedValue, , dtBSC_TabularGrid, , , ConfigParameter._Object._Self_Assign_Competencies) 'S.SANDEEP [08 Jan 2016] -- START{ConfigParameter._Object._Self_Assign_Competencies} -- END

                        'If mdtBSC_Evaluation IsNot Nothing AndAlso mdtBSC_Evaluation.Rows.Count > 0 Then
                        '    objlblValue4.Text = Language.getMessage(mstrModuleName, 8, "Reviewer Score :") & " " & objEAnalysisMst.Compute_Score(menAssess, True, ConfigParameter._Object._ScoringOptionId, enAssess_Computation_Formulas.BSC_REV_TOTAL_SCORE, eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), cboEmployee.SelectedValue, cboPeriod.SelectedValue, , mdtBSC_Evaluation, cboReviewer.SelectedValue, , ) 'S.SANDEEP [08 Jan 2016] -- START{ConfigParameter._Object._Self_Assign_Competencies} -- END
                        'End If
                        If dtBSC_TabularGrid.Rows.Count > 0 Then objlblValue2.Text = Language.getMessage(mstrModuleName, 6, "Self Score :") & " " & _
                        objEAnalysisMst.Compute_Score(xAssessMode:=enAssessmentMode.SELF_ASSESSMENT, _
                                                      IsBalanceScoreCard:=True, _
                                                      xScoreOptId:=ConfigParameter._Object._ScoringOptionId, _
                                                      xCompute_Formula:=enAssess_Computation_Formulas.BSC_EMP_TOTAL_SCORE, _
                                                      xEmployeeAsOnDate:=eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                      xEmployeeId:=cboEmployee.SelectedValue, _
                                                      xPeriodId:=cboPeriod.SelectedValue, _
                                                      xUsedAgreedScore:=ConfigParameter._Object._IsUseAgreedScore, _
                                                      xDataTable:=dtBSC_TabularGrid)
                        If dtBSC_TabularGrid.Rows.Count > 0 Then objlblValue3.Text = Language.getMessage(mstrModuleName, 7, "Assessor Score :") & " " & _
                        objEAnalysisMst.Compute_Score(xAssessMode:=enAssessmentMode.APPRAISER_ASSESSMENT, _
                                                      IsBalanceScoreCard:=True, _
                                                      xScoreOptId:=ConfigParameter._Object._ScoringOptionId, _
                                                      xCompute_Formula:=enAssess_Computation_Formulas.BSC_ASR_TOTAL_SCORE, _
                                                      xEmployeeAsOnDate:=eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                      xEmployeeId:=cboEmployee.SelectedValue, _
                                                      xPeriodId:=cboPeriod.SelectedValue, _
                                                      xUsedAgreedScore:=ConfigParameter._Object._IsUseAgreedScore, _
                                                      xDataTable:=dtBSC_TabularGrid, _
                                                      xSelfAssignedCompetencies:=ConfigParameter._Object._Self_Assign_Competencies)

                        If mdtBSC_Evaluation IsNot Nothing AndAlso mdtBSC_Evaluation.Rows.Count > 0 Then
                            objlblValue4.Text = Language.getMessage(mstrModuleName, 8, "Reviewer Score :") & " " & _
                            objEAnalysisMst.Compute_Score(xAssessMode:=menAssess, _
                                                          IsBalanceScoreCard:=True, _
                                                          xScoreOptId:=ConfigParameter._Object._ScoringOptionId, _
                                                          xCompute_Formula:=enAssess_Computation_Formulas.BSC_REV_TOTAL_SCORE, _
                                                          xEmployeeAsOnDate:=eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                          xEmployeeId:=cboEmployee.SelectedValue, _
                                                          xPeriodId:=cboPeriod.SelectedValue, _
                                                          xUsedAgreedScore:=ConfigParameter._Object._IsUseAgreedScore, _
                                                          xDataTable:=mdtBSC_Evaluation, _
                                                          xAssessorReviewerId:=cboReviewer.SelectedValue)
                        End If
                        'Shani (23-Nov-2016) -- End

                        'S.SANDEEP [04 JUN 2015] -- END

                        objlblValue3.Visible = True : objlblValue4.Visible = True
                    End If
                    'S.SANDEEP [21 JAN 2015] -- END



                Case dgvGE.Name.ToUpper

                    objlblValue1.Text = Language.getMessage(mstrModuleName, 5, "Total Weight :") & " " & CDbl(iWeightTotal).ToString
                    objlblValue1.Visible = True : objlblValue2.Visible = True

                    'S.SANDEEP [21 JAN 2015] -- START
                    'If menAssess = enAssessmentMode.SELF_ASSESSMENT Then
                    '    If mdtGE_Evaluation IsNot Nothing AndAlso mdtGE_Evaluation.Rows.Count > 0 Then
                    '        objlblValue2.Text = Language.getMessage(mstrModuleName, 6, "Self Score :") & " " & CDbl(mdtGE_Evaluation.Compute("SUM(computed_value)", "AUD <> 'D'")).ToString
                    '    End If
                    '    objlblValue3.Visible = False : objlblValue4.Visible = False
                    'ElseIf menAssess = enAssessmentMode.APPRAISER_ASSESSMENT Then
                    '    If dtGE_TabularGrid.Rows.Count > 0 Then objlblValue2.Text = Language.getMessage(mstrModuleName, 6, "Self Score :") & " " & CDbl(dtGE_TabularGrid.Compute("SUM(escore)", "")).ToString
                    '    If mdtGE_Evaluation IsNot Nothing AndAlso mdtGE_Evaluation.Rows.Count > 0 Then
                    '        objlblValue3.Text = Language.getMessage(mstrModuleName, 7, "Assessor Score :") & " " & CDbl(mdtGE_Evaluation.Compute("SUM(computed_value)", "AUD <> 'D'")).ToString
                    '    End If
                    '    objlblValue3.Visible = True : objlblValue4.Visible = False
                    'ElseIf menAssess = enAssessmentMode.REVIEWER_ASSESSMENT Then
                    '    If dtGE_TabularGrid.Rows.Count > 0 Then objlblValue2.Text = Language.getMessage(mstrModuleName, 6, "Self Score :") & " " & CDbl(dtGE_TabularGrid.Compute("SUM(escore)", "")).ToString
                    '    If dtGE_TabularGrid.Rows.Count > 0 Then objlblValue3.Text = Language.getMessage(mstrModuleName, 7, "Assessor Score :") & " " & CDbl(dtGE_TabularGrid.Compute("SUM(ascore)", "")).ToString

                    '    If mdtGE_Evaluation IsNot Nothing AndAlso mdtGE_Evaluation.Rows.Count > 0 Then
                    '        objlblValue4.Text = Language.getMessage(mstrModuleName, 8, "Reviewer Score :") & " " & CDbl(mdtGE_Evaluation.Compute("SUM(computed_value)", "AUD <> 'D'")).ToString
                    '    End If
                    '    objlblValue3.Visible = True : objlblValue4.Visible = True
                    'End If

                    If menAssess = enAssessmentMode.SELF_ASSESSMENT Then
                        If mdtGE_Evaluation IsNot Nothing AndAlso mdtGE_Evaluation.Rows.Count > 0 Then
                            'S.SANDEEP [04 JUN 2015] -- START
                            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                            'objlblValue2.Text = Language.getMessage(mstrModuleName, 6, "Self Score :") & " " & objEAnalysisMst.Compute_Score(enAssessmentMode.SELF_ASSESSMENT, False, ConfigParameter._Object._ScoringOptionId, enAssess_Computation_Formulas.CMP_EMP_TOTAL_SCORE, cboEmployee.SelectedValue, cboPeriod.SelectedValue, , mdtGE_Evaluation)

                            'Shani (23-Nov-2016) -- Start
                            'Enhancement - Add New PA Enhancement (Agreed Score,exc) Given by Akiba,KBC,TNP exc...
                            'objlblValue2.Text = Language.getMessage(mstrModuleName, 6, "Self Score :") & " " & objEAnalysisMst.Compute_Score(enAssessmentMode.SELF_ASSESSMENT, False, ConfigParameter._Object._ScoringOptionId, enAssess_Computation_Formulas.CMP_EMP_TOTAL_SCORE, eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), cboEmployee.SelectedValue, cboPeriod.SelectedValue, , mdtGE_Evaluation, , , ConfigParameter._Object._Self_Assign_Competencies) 'S.SANDEEP [08 Jan 2016] -- START{ConfigParameter._Object._Self_Assign_Competencies} -- END
                            objlblValue2.Text = Language.getMessage(mstrModuleName, 6, "Self Score :") & " " & _
                            objEAnalysisMst.Compute_Score(xAssessMode:=enAssessmentMode.SELF_ASSESSMENT, _
                                                          IsBalanceScoreCard:=False, _
                                                          xScoreOptId:=ConfigParameter._Object._ScoringOptionId, _
                                                          xCompute_Formula:=enAssess_Computation_Formulas.CMP_EMP_TOTAL_SCORE, _
                                                          xEmployeeAsOnDate:=eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                          xEmployeeId:=cboEmployee.SelectedValue, _
                                                          xPeriodId:=cboPeriod.SelectedValue, _
                                                          xUsedAgreedScore:=ConfigParameter._Object._IsUseAgreedScore, _
                                                          xDataTable:=mdtGE_Evaluation, _
                                                          xSelfAssignedCompetencies:=ConfigParameter._Object._Self_Assign_Competencies)
                            'Shani (23-Nov-2016) -- End


                            'S.SANDEEP [04 JUN 2015] -- END
                        End If
                        objlblValue3.Visible = False : objlblValue4.Visible = False
                    ElseIf menAssess = enAssessmentMode.APPRAISER_ASSESSMENT Then

                        'S.SANDEEP [04 JUN 2015] -- START
                        'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                        'If dtGE_TabularGrid.Rows.Count > 0 Then objlblValue2.Text = Language.getMessage(mstrModuleName, 6, "Self Score :") & " " & objEAnalysisMst.Compute_Score(enAssessmentMode.SELF_ASSESSMENT, False, ConfigParameter._Object._ScoringOptionId, enAssess_Computation_Formulas.CMP_EMP_TOTAL_SCORE, cboEmployee.SelectedValue, cboPeriod.SelectedValue, , dtGE_TabularGrid)
                        'objlblValue3.Text = ""
                        'If mdtGE_Evaluation IsNot Nothing AndAlso mdtGE_Evaluation.Rows.Count > 0 Then
                        '    objlblValue3.Text = Language.getMessage(mstrModuleName, 7, "Assessor Score :") & " " & objEAnalysisMst.Compute_Score(menAssess, False, ConfigParameter._Object._ScoringOptionId, enAssess_Computation_Formulas.CMP_ASR_TOTAL_SCORE, cboEmployee.SelectedValue, cboPeriod.SelectedValue, , mdtGE_Evaluation, cboAssessor.SelectedValue)
                        'End If


                        'Shani (23-Nov-2016) -- Start
                        'Enhancement - Add New PA Enhancement (Agreed Score,exc) Given by Akiba,KBC,TNP exc...
                        'If dtGE_TabularGrid.Rows.Count > 0 Then objlblValue2.Text = Language.getMessage(mstrModuleName, 6, "Self Score :") & " " & objEAnalysisMst.Compute_Score(enAssessmentMode.SELF_ASSESSMENT, False, ConfigParameter._Object._ScoringOptionId, enAssess_Computation_Formulas.CMP_EMP_TOTAL_SCORE, eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), cboEmployee.SelectedValue, cboPeriod.SelectedValue, , dtGE_TabularGrid, , , ConfigParameter._Object._Self_Assign_Competencies) 'S.SANDEEP [08 Jan 2016] -- START{ConfigParameter._Object._Self_Assign_Competencies} -- END
                        If dtGE_TabularGrid.Rows.Count > 0 Then objlblValue2.Text = Language.getMessage(mstrModuleName, 6, "Self Score :") & " " & _
                        objEAnalysisMst.Compute_Score(xAssessMode:=enAssessmentMode.SELF_ASSESSMENT, _
                                                      IsBalanceScoreCard:=False, _
                                                      xScoreOptId:=ConfigParameter._Object._ScoringOptionId, _
                                                      xCompute_Formula:=enAssess_Computation_Formulas.CMP_EMP_TOTAL_SCORE, _
                                                      xEmployeeAsOnDate:=eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                      xEmployeeId:=cboEmployee.SelectedValue, _
                                                      xPeriodId:=cboPeriod.SelectedValue, _
                                                      xUsedAgreedScore:=ConfigParameter._Object._IsUseAgreedScore, _
                                                      xDataTable:=dtGE_TabularGrid, _
                                                      xSelfAssignedCompetencies:=ConfigParameter._Object._Self_Assign_Competencies)
                        'Shani (23-Nov-2016) -- End


                        objlblValue3.Text = ""
                        If mdtGE_Evaluation IsNot Nothing AndAlso mdtGE_Evaluation.Rows.Count > 0 Then

                            'Shani (23-Nov-2016) -- Start
                            'Enhancement - Add New PA Enhancement (Agreed Score,exc) Given by Akiba,KBC,TNP exc...
                            'objlblValue3.Text = Language.getMessage(mstrModuleName, 7, "Assessor Score :") & " " & objEAnalysisMst.Compute_Score(menAssess, False, ConfigParameter._Object._ScoringOptionId, enAssess_Computation_Formulas.CMP_ASR_TOTAL_SCORE, eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), cboEmployee.SelectedValue, cboPeriod.SelectedValue, , mdtGE_Evaluation, cboAssessor.SelectedValue, , ConfigParameter._Object._Self_Assign_Competencies) 'S.SANDEEP [08 Jan 2016] -- START{ConfigParameter._Object._Self_Assign_Competencies} -- END
                            objlblValue3.Text = Language.getMessage(mstrModuleName, 7, "Assessor Score :") & " " & _
                            objEAnalysisMst.Compute_Score(xAssessMode:=menAssess, _
                                                          IsBalanceScoreCard:=False, _
                                                          xScoreOptId:=ConfigParameter._Object._ScoringOptionId, _
                                                          xCompute_Formula:=enAssess_Computation_Formulas.CMP_ASR_TOTAL_SCORE, _
                                                          xEmployeeAsOnDate:=eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                          xEmployeeId:=cboEmployee.SelectedValue, _
                                                          xPeriodId:=cboPeriod.SelectedValue, _
                                                          xUsedAgreedScore:=ConfigParameter._Object._IsUseAgreedScore, _
                                                          xDataTable:=mdtGE_Evaluation, _
                                                          xAssessorReviewerId:=cboAssessor.SelectedValue, _
                                                          xSelfAssignedCompetencies:=ConfigParameter._Object._Self_Assign_Competencies)
                            'Shani (23-Nov-2016) -- End


                        End If
                        'S.SANDEEP [04 JUN 2015] -- END
                        objlblValue3.Visible = True : objlblValue4.Visible = False
                    ElseIf menAssess = enAssessmentMode.REVIEWER_ASSESSMENT Then

                        'S.SANDEEP [04 JUN 2015] -- START
                        'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                        'If dtGE_TabularGrid.Rows.Count > 0 Then objlblValue2.Text = Language.getMessage(mstrModuleName, 6, "Self Score :") & " " & objEAnalysisMst.Compute_Score(enAssessmentMode.SELF_ASSESSMENT, False, ConfigParameter._Object._ScoringOptionId, enAssess_Computation_Formulas.CMP_EMP_TOTAL_SCORE, cboEmployee.SelectedValue, cboPeriod.SelectedValue, , dtGE_TabularGrid)
                        'If dtGE_TabularGrid.Rows.Count > 0 Then objlblValue3.Text = Language.getMessage(mstrModuleName, 7, "Assessor Score :") & " " & objEAnalysisMst.Compute_Score(enAssessmentMode.APPRAISER_ASSESSMENT, False, ConfigParameter._Object._ScoringOptionId, enAssess_Computation_Formulas.CMP_ASR_TOTAL_SCORE, cboEmployee.SelectedValue, cboPeriod.SelectedValue, , dtGE_TabularGrid)
                        'objlblValue4.Text = ""
                        'If mdtGE_Evaluation IsNot Nothing AndAlso mdtGE_Evaluation.Rows.Count > 0 Then
                        '    objlblValue4.Text = Language.getMessage(mstrModuleName, 8, "Reviewer Score :") & " " & objEAnalysisMst.Compute_Score(menAssess, False, ConfigParameter._Object._ScoringOptionId, enAssess_Computation_Formulas.CMP_REV_TOTAL_SCORE, cboEmployee.SelectedValue, cboPeriod.SelectedValue, , mdtGE_Evaluation, cboReviewer.SelectedValue)
                        'End If

                        'Shani (23-Nov-2016) -- Start
                        'Enhancement - Add New PA Enhancement (Agreed Score,exc) Given by Akiba,KBC,TNP exc...
                        'If dtGE_TabularGrid.Rows.Count > 0 Then objlblValue2.Text = Language.getMessage(mstrModuleName, 6, "Self Score :") & " " & objEAnalysisMst.Compute_Score(enAssessmentMode.SELF_ASSESSMENT, False, ConfigParameter._Object._ScoringOptionId, enAssess_Computation_Formulas.CMP_EMP_TOTAL_SCORE, eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), cboEmployee.SelectedValue, cboPeriod.SelectedValue, , dtGE_TabularGrid, , , ConfigParameter._Object._Self_Assign_Competencies) 'S.SANDEEP [08 Jan 2016] -- START{ConfigParameter._Object._Self_Assign_Competencies} -- END
                        'If dtGE_TabularGrid.Rows.Count > 0 Then objlblValue3.Text = Language.getMessage(mstrModuleName, 7, "Assessor Score :") & " " & objEAnalysisMst.Compute_Score(enAssessmentMode.APPRAISER_ASSESSMENT, False, ConfigParameter._Object._ScoringOptionId, enAssess_Computation_Formulas.CMP_ASR_TOTAL_SCORE, eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), cboEmployee.SelectedValue, cboPeriod.SelectedValue, , dtGE_TabularGrid, , , ConfigParameter._Object._Self_Assign_Competencies) 'S.SANDEEP [08 Jan 2016] -- START{ConfigParameter._Object._Self_Assign_Competencies} -- END
                        'objlblValue4.Text = ""
                        'If mdtGE_Evaluation IsNot Nothing AndAlso mdtGE_Evaluation.Rows.Count > 0 Then
                        '    objlblValue4.Text = Language.getMessage(mstrModuleName, 8, "Reviewer Score :") & " " & objEAnalysisMst.Compute_Score(menAssess, False, ConfigParameter._Object._ScoringOptionId, enAssess_Computation_Formulas.CMP_REV_TOTAL_SCORE, eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), cboEmployee.SelectedValue, cboPeriod.SelectedValue, , mdtGE_Evaluation, cboReviewer.SelectedValue, , ConfigParameter._Object._Self_Assign_Competencies) 'S.SANDEEP [08 Jan 2016] -- START{ConfigParameter._Object._Self_Assign_Competencies} -- END
                        'End If
                        If dtGE_TabularGrid.Rows.Count > 0 Then objlblValue2.Text = Language.getMessage(mstrModuleName, 6, "Self Score :") & " " & _
                        objEAnalysisMst.Compute_Score(xAssessMode:=enAssessmentMode.SELF_ASSESSMENT, _
                                                      IsBalanceScoreCard:=False, _
                                                      xScoreOptId:=ConfigParameter._Object._ScoringOptionId, _
                                                      xCompute_Formula:=enAssess_Computation_Formulas.CMP_EMP_TOTAL_SCORE, _
                                                      xEmployeeAsOnDate:=eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                      xEmployeeId:=cboEmployee.SelectedValue, _
                                                      xPeriodId:=cboPeriod.SelectedValue, _
                                                      xUsedAgreedScore:=ConfigParameter._Object._IsUseAgreedScore, _
                                                      xDataTable:=dtGE_TabularGrid, _
                                                      xSelfAssignedCompetencies:=ConfigParameter._Object._Self_Assign_Competencies)
                        If dtGE_TabularGrid.Rows.Count > 0 Then objlblValue3.Text = Language.getMessage(mstrModuleName, 7, "Assessor Score :") & " " & _
                        objEAnalysisMst.Compute_Score(xAssessMode:=enAssessmentMode.APPRAISER_ASSESSMENT, _
                                                      IsBalanceScoreCard:=False, _
                                                      xScoreOptId:=ConfigParameter._Object._ScoringOptionId, _
                                                      xCompute_Formula:=enAssess_Computation_Formulas.CMP_ASR_TOTAL_SCORE, _
                                                      xEmployeeAsOnDate:=eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                      xEmployeeId:=cboEmployee.SelectedValue, _
                                                      xPeriodId:=cboPeriod.SelectedValue, _
                                                      xUsedAgreedScore:=ConfigParameter._Object._IsUseAgreedScore, _
                                                      xDataTable:=dtGE_TabularGrid, _
                                                      xSelfAssignedCompetencies:=ConfigParameter._Object._Self_Assign_Competencies)
                        objlblValue4.Text = ""
                        If mdtGE_Evaluation IsNot Nothing AndAlso mdtGE_Evaluation.Rows.Count > 0 Then
                            objlblValue4.Text = Language.getMessage(mstrModuleName, 8, "Reviewer Score :") & " " & _
                            objEAnalysisMst.Compute_Score(xAssessMode:=menAssess, _
                                                          IsBalanceScoreCard:=False, _
                                                          xScoreOptId:=ConfigParameter._Object._ScoringOptionId, _
                                                          xCompute_Formula:=enAssess_Computation_Formulas.CMP_REV_TOTAL_SCORE, _
                                                          xEmployeeAsOnDate:=eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                          xEmployeeId:=cboEmployee.SelectedValue, _
                                                          xPeriodId:=cboPeriod.SelectedValue, _
                                                          xUsedAgreedScore:=ConfigParameter._Object._IsUseAgreedScore, _
                                                          xDataTable:=mdtGE_Evaluation, _
                                                          xAssessorReviewerId:=cboReviewer.SelectedValue, _
                                                          xSelfAssignedCompetencies:=ConfigParameter._Object._Self_Assign_Competencies)
                        End If
                        'Shani (23-Nov-2016) -- End

                        'S.SANDEEP [04 JUN 2015] -- END


                        objlblValue3.Visible = True : objlblValue4.Visible = True
                    End If
                    'S.SANDEEP [21 JAN 2015] -- END


            End Select
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Function Is_Already_Assessed() As Boolean
        Try
            Select Case menAssess
                Case enAssessmentMode.SELF_ASSESSMENT
                    If objEAnalysisMst.isExist(menAssess, CInt(cboEmployee.SelectedValue), CInt(cboPeriod.SelectedValue), , , mintAssessAnalysisUnkid) = True Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 12, "Sorry, You have already assessed some items of particular perspective. Please open in edit mode and add new items."), enMsgBoxStyle.Information)
                        Return False
                    End If
                Case enAssessmentMode.APPRAISER_ASSESSMENT
                    If ConfigParameter._Object._AllowAssessor_Before_Emp = False Then
                        If objEAnalysisMst.isExist(enAssessmentMode.SELF_ASSESSMENT, CInt(cboEmployee.SelectedValue), CInt(cboPeriod.SelectedValue), , , -1) = False Then
                            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 13, "Sorry, You cannot do assessment for the particular employee for the selected period." & vbCrLf & _
                                                                "Reason: As per the setting set in configuration,You are not allowed to do assessment unless employee do his self assessment for the selected period."), enMsgBoxStyle.Information)
                            Return False
                        End If
                    End If
                    If objEAnalysisMst.isExist(menAssess, CInt(cboEmployee.SelectedValue), CInt(cboPeriod.SelectedValue), CInt(cboAssessor.SelectedValue), , mintAssessAnalysisUnkid, radExternalAssessor.Checked) = True Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 14, "Sorry, You have already assessed some items of particular perspective. Please open in edit mode and add new items."), enMsgBoxStyle.Information)
                        Return False
                    End If
                Case enAssessmentMode.REVIEWER_ASSESSMENT
                    If ConfigParameter._Object._AllowAssessor_Before_Emp = False Then
                        If objEAnalysisMst.isExist(enAssessmentMode.SELF_ASSESSMENT, CInt(cboEmployee.SelectedValue), CInt(cboPeriod.SelectedValue), , , -1) = False Then
                            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 13, "Sorry, You cannot do assessment for the particular employee for the selected period." & vbCrLf & _
                                                                "Reason: As per the setting set in configuration,You are not allowed to do assessment unless employee do his self assessment for the selected period."), enMsgBoxStyle.Information)
                            Return False
                        End If

                        If objEAnalysisMst.isExist(enAssessmentMode.APPRAISER_ASSESSMENT, CInt(cboEmployee.SelectedValue), CInt(cboPeriod.SelectedValue), , , -1) = False Then
                            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 15, "Sorry, You cannot do assessment for the particular employee for the selected period." & vbCrLf & _
                                                                "Reason: As per the setting set in configuration,You are not allowed to do assessment unless assessor does assessment for the selected employee and period."), enMsgBoxStyle.Information)
                            Return False
                        End If
                    End If
                    If objEAnalysisMst.isExist(menAssess, CInt(cboEmployee.SelectedValue), CInt(cboPeriod.SelectedValue), , CInt(cboReviewer.SelectedValue), mintAssessAnalysisUnkid) = True Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 14, "Sorry, You have already assessed some items of particular perspective. Please open in edit mode and add new items."), enMsgBoxStyle.Information)
                        Return False
                    End If
            End Select
            Return True
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Is_Already_Assessed", mstrModuleName)
        Finally
        End Try
    End Function

    '--------------------------------------------- BALANCE SCORE CARD EVALUATION METHODS
    Private Sub SetBSC_GridCols_Tags()
        Try
            Dim objFMst As New clsAssess_Field_Master
            Dim dFld As New DataSet : dFld.Tables.Add(objFMst.GetFieldsForViewSetting())
            If dFld.Tables(0).Rows.Count > 0 Then
                Dim xCol As DataGridViewColumn = Nothing
                Dim iExOrder As Integer = -1
                For Each xRow As DataRow In dFld.Tables(0).Rows
                    iExOrder = -1
                    iExOrder = objFMst.Get_Field_ExOrder(xRow.Item("Id"))
                    xCol = Nothing
                    Select Case iExOrder
                        Case 1
                            objdgcolhBSCField1.Tag = xRow.Item("Id")
                            xCol = objdgcolhBSCField1
                        Case 2
                            objdgcolhBSCField2.Tag = xRow.Item("Id")
                            xCol = objdgcolhBSCField2
                        Case 3
                            objdgcolhBSCField3.Tag = xRow.Item("Id")
                            xCol = objdgcolhBSCField3
                        Case 4
                            objdgcolhBSCField4.Tag = xRow.Item("Id")
                            xCol = objdgcolhBSCField4
                        Case 5
                            objdgcolhBSCField5.Tag = xRow.Item("Id")
                            xCol = objdgcolhBSCField5
                        Case 6
                            objdgcolhBSCField6.Tag = xRow.Item("Id")
                            xCol = objdgcolhBSCField6
                        Case 7
                            objdgcolhBSCField7.Tag = xRow.Item("Id")
                            xCol = objdgcolhBSCField7
                        Case 8
                            objdgcolhBSCField8.Tag = xRow.Item("Id")
                            xCol = objdgcolhBSCField8
                    End Select
                    Select Case xRow.Item("Id")
                        Case clsAssess_Field_Master.enOtherInfoField.ST_DATE
                            dgcolhSDate.Tag = xRow.Item("Id")
                            xCol = dgcolhSDate
                        Case clsAssess_Field_Master.enOtherInfoField.ED_DATE
                            dgcolhEDate.Tag = xRow.Item("Id")
                            xCol = dgcolhEDate
                        Case clsAssess_Field_Master.enOtherInfoField.PCT_COMPLETE
                            dgcolhCompleted.Tag = xRow.Item("Id")
                            xCol = dgcolhCompleted
                        Case clsAssess_Field_Master.enOtherInfoField.STATUS
                            dgcolhStatus.Tag = xRow.Item("Id")
                            xCol = dgcolhStatus
                            'S.SANDEEP [11-OCT-2018] -- START
                        Case clsAssess_Field_Master.enOtherInfoField.GOAL_TYPE
                            dgcolhGoalType.Tag = xRow.Item("Id")
                            xCol = dgcolhGoalType
                        Case clsAssess_Field_Master.enOtherInfoField.GOAL_VALUE
                            dgcolhGoalvalue.Tag = xRow.Item("Id")
                            xCol = dgcolhGoalvalue
                            'S.SANDEEP [11-OCT-2018] -- END
                    End Select
                    If xCol IsNot Nothing Then
                        xCol.Width = objFMst.GetColumnWidth(clsAssess_Field_Master.enColWidthType.COL_EVAL, xRow.Item("Id"))
                    End If
                Next
            End If
            objFMst = Nothing
            'dgcolhSDate.Tag = clsAssess_Field_Master.enOtherInfoField.ST_DATE
            'dgcolhEDate.Tag = clsAssess_Field_Master.enOtherInfoField.ED_DATE
            'dgcolhCompleted.Tag = clsAssess_Field_Master.enOtherInfoField.PCT_COMPLETE
            'dgcolhStatus.Tag = clsAssess_Field_Master.enOtherInfoField.STATUS
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetBSC_GridCols_Tags", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub Fill_BSC_Evaluation()
        Try
            'S.SANDEEP [23 APR 2015] -- START
            'RemoveHandler dgvBSC.CellValueChanged, AddressOf dgvBSC_CellValueChanged
            'S.SANDEEP [23 APR 2015] -- END


            'Shani (26-Sep-2016) -- Start
            'Enhancement -Changes PA Flow And Add New screen Given By (Andrew)
            'dtBSC_TabularGrid = objEAnalysisMst.Get_BSC_Evaluation_Data(menAssess, CInt(cboEmployee.SelectedValue), CInt(cboPeriod.SelectedValue), ConfigParameter._Object._CascadingTypeId, CInt(cboAssessor.SelectedValue), CInt(cboReviewer.SelectedValue), -1)

            'S.SANDEEP [04-AUG-2017] -- START
            'ISSUE/ENHANCEMENT : STOP ENTRY POSTING FOR TERMINATED EMPLOYEE
            'dtBSC_TabularGrid = objEAnalysisMst.Get_BSC_Evaluation_Data(menAssess, CInt(cboEmployee.SelectedValue), CInt(cboPeriod.SelectedValue), ConfigParameter._Object._CascadingTypeId, CInt(cboAssessor.SelectedValue), CInt(cboReviewer.SelectedValue), -1, ConfigParameter._Object._EnableBSCAutomaticRating, ConfigParameter._Object._ScoringOptionId)

            'S.SANDEEP |17-MAY-2021| -- START
            'ISSUE/ENHANCEMENT : SUNBIRD CHANGES FOR SCORES
            'dtBSC_TabularGrid = objEAnalysisMst.Get_BSC_Evaluation_Data(menAssess, CInt(cboEmployee.SelectedValue), CInt(cboPeriod.SelectedValue), ConfigParameter._Object._CascadingTypeId, CInt(cboAssessor.SelectedValue), CInt(cboReviewer.SelectedValue), -1, ConfigParameter._Object._EnableBSCAutomaticRating, ConfigParameter._Object._ScoringOptionId, FinancialYear._Object._DatabaseName)
            dtBSC_TabularGrid = objEAnalysisMst.Get_BSC_Evaluation_Data(menAssess, CInt(cboEmployee.SelectedValue), CInt(cboPeriod.SelectedValue), ConfigParameter._Object._CascadingTypeId, CInt(cboAssessor.SelectedValue), CInt(cboReviewer.SelectedValue), -1, ConfigParameter._Object._EnableBSCAutomaticRating, ConfigParameter._Object._ScoringOptionId, FinancialYear._Object._DatabaseName, ConfigParameter._Object._DontAllowRatingBeyond100)
            'S.SANDEEP |17-MAY-2021| -- END

            'S.SANDEEP [04-AUG-2017] -- END

            'Shani (26-Sep-2016) -- End



            Call SetBSC_GridCols_Tags()

            dgvBSC.AutoGenerateColumns = False
            objdgcolhBSCField1.DataPropertyName = "Field1"
            objdgcolhBSCField2.DataPropertyName = "Field2"
            objdgcolhBSCField3.DataPropertyName = "Field3"
            objdgcolhBSCField4.DataPropertyName = "Field4"
            objdgcolhBSCField5.DataPropertyName = "Field5"
            objdgcolhBSCField6.DataPropertyName = "Field6"
            objdgcolhBSCField7.DataPropertyName = "Field7"
            objdgcolhBSCField8.DataPropertyName = "Field8"
            dgcolhSDate.DataPropertyName = "St_Date"
            dgcolhEDate.DataPropertyName = "Ed_Date"
            dgcolhCompleted.DataPropertyName = "pct_complete"
            dgcolhStatus.DataPropertyName = "CStatus"
            objdgcolhScaleMasterId.DataPropertyName = "scalemasterunkid"

            'S.SANDEEP [11-OCT-2018] -- START
            dgcolhGoalType.DataPropertyName = "dgoaltype"
            dgcolhGoalvalue.DataPropertyName = "dgoalvalue"
            dgcolhGoalvalue.HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleRight
            'S.SANDEEP [11-OCT-2018] -- END

            If ConfigParameter._Object._ScoringOptionId = enScoringOption.SC_SCALE_BASED Then
                dgcolhBSCScore.DataPropertyName = "score"
                dgcolhBSCScore.DefaultCellStyle.SelectionBackColor = Color.White
                dgcolhBSCScore.DefaultCellStyle.SelectionForeColor = Color.White
                dgcolhBSCWeight.Visible = False
            Else
                dgcolhBSCScore.Visible = False
                dgcolhBSCWeight.Visible = True
            End If

            dgcolheselfBSC.DataPropertyName = "eself"
            dgcolheremarkBSC.DataPropertyName = "eremark"

            If menAssess = enAssessmentMode.APPRAISER_ASSESSMENT Then
                dgcolhaselfBSC.DataPropertyName = "aself"
                dgcolharemarkBSC.DataPropertyName = "aremark"
                dgcolheselfBSC.ReadOnly = True : dgcolheremarkBSC.ReadOnly = True
                'Shani (23-Nov-2016) -- Start
                'Enhancement - Add New PA Enhancement (Agreed Score,exc) Given by Akiba,KBC,TNP exc...
                dgcolhAgreedScoreBSC.DataPropertyName = "agreedscore"
                dgcolhAgreedScoreBSC.Visible = ConfigParameter._Object._IsUseAgreedScore
                'Shani (23-Nov-2016) -- End
            Else
                dgcolhaselfBSC.Visible = False
                dgcolharemarkBSC.Visible = False
                dgcolhAgreedScoreBSC.Visible = False
            End If

            If menAssess = enAssessmentMode.REVIEWER_ASSESSMENT Then
                dgcolhaselfBSC.DataPropertyName = "aself"
                dgcolharemarkBSC.DataPropertyName = "aremark"
                dgcolhrselfBSC.DataPropertyName = "rself"
                dgcolhrremarkBSC.DataPropertyName = "rremark"
                dgcolheselfBSC.ReadOnly = True : dgcolheremarkBSC.ReadOnly = True
                dgcolhaselfBSC.ReadOnly = True : dgcolharemarkBSC.ReadOnly = True
                dgcolhaselfBSC.Visible = True
                dgcolharemarkBSC.Visible = True

                'Shani (23-Nov-2016) -- Start
                'Enhancement - Add New PA Enhancement (Agreed Score,exc) Given by Akiba,KBC,TNP exc...
                dgcolhAgreedScoreBSC.DataPropertyName = "agreedscore"

                If ConfigParameter._Object._IsUseAgreedScore Then
                    dgcolhAgreedScoreBSC.ReadOnly = True
                    'S.SANDEEP |31-MAY-2019| -- START
                    'ISSUE/ENHANCEMENT : [PA SETTING CHANGES]
                Else
                    dgcolhAgreedScoreBSC.Visible = False
                    'Shani(14-FEB-2017) -- End
                End If
                'S.SANDEEP |31-MAY-2019| -- END

                'Shani (23-Nov-2016) -- End
                'Shani(14-FEB-2017) -- Start
                'Enhancement - Add New PA Enhancement (Agreed Score,exc) Given by Akiba,KBC,TNP exc...
                'dgcolhAgreedScoreBSC.Visible = True
                If ConfigParameter._Object._ReviewerScoreSetting = enReviewerScoreSetting.ReviewerScoreNotNecessary Then
                    'S.SANDEEP |31-MAY-2019| -- START
                    'ISSUE/ENHANCEMENT : [PA SETTING CHANGES]
                    'dgcolhAgreedScoreBSC.Visible = True
                    'S.SANDEEP |31-MAY-2019| -- END

                    dgcolhrselfBSC.Visible = False
                ElseIf ConfigParameter._Object._ReviewerScoreSetting = enReviewerScoreSetting.ReviewerScoreNeeded Then
                    'S.SANDEEP |31-MAY-2019| -- START
                    'ISSUE/ENHANCEMENT : [PA SETTING CHANGES]
                    'dgcolhAgreedScoreBSC.Visible = False
                    'S.SANDEEP |31-MAY-2019| -- END

                    dgcolhrselfBSC.Visible = True
                End If
            Else
                dgcolhrselfBSC.Visible = False
                dgcolhrremarkBSC.Visible = False
            End If

            dgcolhBSCWeight.DataPropertyName = "Weight"
            objdgcolhGrpIdBSC.DataPropertyName = "GrpId"
            objdgcolhIsGrpBSC.DataPropertyName = "IsGrp"
            dgvBSC.DataSource = dtBSC_TabularGrid

            dgcolhBSCWeight.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
            dgcolhBSCWeight.HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleRight

            If dtBSC_TabularGrid.Columns(objdgcolhBSCField1.DataPropertyName) IsNot Nothing Then
                objdgcolhBSCField1.HeaderText = dtBSC_TabularGrid.Columns(objdgcolhBSCField1.DataPropertyName).Caption
                objdgcolhBSCField1.Visible = True
            Else
                objdgcolhBSCField1.Visible = False
            End If
            If dtBSC_TabularGrid.Columns(objdgcolhBSCField2.DataPropertyName) IsNot Nothing Then
                objdgcolhBSCField2.HeaderText = dtBSC_TabularGrid.Columns(objdgcolhBSCField2.DataPropertyName).Caption
                objdgcolhBSCField2.Visible = True
            Else
                objdgcolhBSCField2.Visible = False
            End If
            If dtBSC_TabularGrid.Columns(objdgcolhBSCField3.DataPropertyName) IsNot Nothing Then
                objdgcolhBSCField3.HeaderText = dtBSC_TabularGrid.Columns(objdgcolhBSCField3.DataPropertyName).Caption
                objdgcolhBSCField3.Visible = True
            Else
                objdgcolhBSCField3.Visible = False
            End If
            If dtBSC_TabularGrid.Columns(objdgcolhBSCField4.DataPropertyName) IsNot Nothing Then
                objdgcolhBSCField4.HeaderText = dtBSC_TabularGrid.Columns(objdgcolhBSCField4.DataPropertyName).Caption
                objdgcolhBSCField4.Visible = True
            Else
                objdgcolhBSCField4.Visible = False
            End If
            If dtBSC_TabularGrid.Columns(objdgcolhBSCField5.DataPropertyName) IsNot Nothing Then
                objdgcolhBSCField5.HeaderText = dtBSC_TabularGrid.Columns(objdgcolhBSCField5.DataPropertyName).Caption
                objdgcolhBSCField5.Visible = True
            Else
                objdgcolhBSCField5.Visible = False
            End If
            If dtBSC_TabularGrid.Columns.Contains(objdgcolhBSCField6.DataPropertyName) Then
                If dtBSC_TabularGrid.Columns(objdgcolhBSCField6.DataPropertyName) IsNot Nothing Then
                    objdgcolhBSCField6.HeaderText = dtBSC_TabularGrid.Columns(objdgcolhBSCField6.DataPropertyName).Caption
                    objdgcolhBSCField6.Visible = True
                Else
                    objdgcolhBSCField6.Visible = False
                End If
            Else
                objdgcolhBSCField6.Visible = False
            End If
            If dtBSC_TabularGrid.Columns.Contains(objdgcolhBSCField7.DataPropertyName) Then
                If dtBSC_TabularGrid.Columns(objdgcolhBSCField6.DataPropertyName) IsNot Nothing Then
                    objdgcolhBSCField7.HeaderText = dtBSC_TabularGrid.Columns(objdgcolhBSCField7.DataPropertyName).Caption
                    objdgcolhBSCField7.Visible = True
                Else
                    objdgcolhBSCField7.Visible = False
                End If
            Else
                objdgcolhBSCField7.Visible = False
            End If
            If dtBSC_TabularGrid.Columns.Contains(objdgcolhBSCField8.DataPropertyName) Then
                If dtBSC_TabularGrid.Columns(objdgcolhBSCField6.DataPropertyName) IsNot Nothing Then
                    objdgcolhBSCField8.HeaderText = dtBSC_TabularGrid.Columns(objdgcolhBSCField8.DataPropertyName).Caption
                    objdgcolhBSCField8.Visible = True
                Else
                    objdgcolhBSCField8.Visible = False
                End If
            Else
                objdgcolhBSCField8.Visible = False
            End If

            dgcolheselfBSC.HeaderText = dtBSC_TabularGrid.Columns(dgcolheselfBSC.DataPropertyName).Caption
            dgcolheremarkBSC.HeaderText = dtBSC_TabularGrid.Columns(dgcolheremarkBSC.DataPropertyName).Caption


            If dgcolhaselfBSC.DataPropertyName.Trim.Length > 0 Then
                dgcolhaselfBSC.HeaderText = dtBSC_TabularGrid.Columns(dgcolhaselfBSC.DataPropertyName).Caption
            End If
            If dgcolharemarkBSC.DataPropertyName.Trim.Length > 0 Then
                dgcolharemarkBSC.HeaderText = dtBSC_TabularGrid.Columns(dgcolharemarkBSC.DataPropertyName).Caption
            End If
            '
            If dgcolhrselfBSC.DataPropertyName.Trim.Length > 0 Then
                'Shani(14-FEB-2017) -- Start
                'Enhancement - Add New PA Enhancement (Agreed Score,exc) Given by Akiba,KBC,TNP exc...
                'dgcolhrselfBSC.HeaderText = dtBSC_TabularGrid.Columns(dgcolhrselfBSC.DataPropertyName).Caption
                If ConfigParameter._Object._IsUseAgreedScore Then
                    dgcolhrselfBSC.HeaderText = "Agreed Score"
                Else
                    dgcolhrselfBSC.HeaderText = dtBSC_TabularGrid.Columns(dgcolhrselfBSC.DataPropertyName).Caption
                End If
                'Shani(14-FEB-2017) -- End
            End If
            If dgcolhrremarkBSC.DataPropertyName.Trim.Length > 0 Then
                'Shani(14-FEB-2017) -- Start
                'Enhancement - Add New PA Enhancement (Agreed Score,exc) Given by Akiba,KBC,TNP exc...
                'dgcolhrremarkBSC.HeaderText = dtBSC_TabularGrid.Columns(dgcolhrremarkBSC.DataPropertyName).Caption
                If ConfigParameter._Object._IsUseAgreedScore AndAlso ConfigParameter._Object._ReviewerScoreSetting = enReviewerScoreSetting.ReviewerScoreNeeded Then
                    dgcolhrremarkBSC.HeaderText = "Agreed Remark"
                Else
                    dgcolhrremarkBSC.HeaderText = dtBSC_TabularGrid.Columns(dgcolhrremarkBSC.DataPropertyName).Caption
                End If
                'Shani(14-FEB-2017) -- End
            End If

            If ConfigParameter._Object._ViewTitles_InEvaluation.Trim.Length > 0 Then
                Dim iEval() As String = ConfigParameter._Object._ViewTitles_InEvaluation.Split("|")
                If iEval IsNot Nothing Then
                    For Each xCol As DataGridViewColumn In dgvBSC.Columns
                        If xCol.Tag IsNot Nothing Then
                            If Array.IndexOf(iEval, xCol.Tag.ToString) < 0 Then
                                xCol.Visible = False
                            End If
                        End If
                    Next
                End If
            End If

            'S.SANDEEP [07 FEB 2015] -- START
            Dim xWidth As Integer = 0
            Dim objFMst As New clsAssess_Field_Master
            xWidth = objFMst.GetColumnWidth(clsAssess_Field_Master.enColWidthType.COL_EVAL, clsAssess_Field_Master.enOtherInfoField.EMP_REMARK)
            If xWidth > 0 Then dgcolheremarkBSC.Width = xWidth

            xWidth = objFMst.GetColumnWidth(clsAssess_Field_Master.enColWidthType.COL_EVAL, clsAssess_Field_Master.enOtherInfoField.ASR_REMARK)
            If xWidth > 0 Then dgcolharemarkBSC.Width = xWidth

            xWidth = objFMst.GetColumnWidth(clsAssess_Field_Master.enColWidthType.COL_EVAL, clsAssess_Field_Master.enOtherInfoField.REV_REMARK)
            If xWidth > 0 Then dgcolhrremarkBSC.Width = xWidth

            objFMst = Nothing
            'S.SANDEEP [07 FEB 2015] -- END

            For Each dgvRow As DataGridViewRow In dgvBSC.Rows
                If menAction = enAction.EDIT_ONE Then
                    Dim dtmp() As DataRow = Nothing
                    Select Case iExOrdr
                        Case enWeight_Types.WEIGHT_FIELD1
                            dtmp = mdtBSC_Evaluation.Select("empfield1unkid = '" & dtBSC_TabularGrid.Rows(dgvRow.Index).Item("empfield1unkid").ToString & "' AND AUD <> 'D' ")
                        Case enWeight_Types.WEIGHT_FIELD2
                            dtmp = mdtBSC_Evaluation.Select("empfield2unkid = '" & dtBSC_TabularGrid.Rows(dgvRow.Index).Item("empfield2unkid").ToString & "' AND AUD <> 'D' ")
                        Case enWeight_Types.WEIGHT_FIELD3
                            dtmp = mdtBSC_Evaluation.Select("empfield3unkid = '" & dtBSC_TabularGrid.Rows(dgvRow.Index).Item("empfield3unkid").ToString & "' AND AUD <> 'D' ")
                        Case enWeight_Types.WEIGHT_FIELD4
                            dtmp = mdtBSC_Evaluation.Select("empfield4unkid = '" & dtBSC_TabularGrid.Rows(dgvRow.Index).Item("empfield4unkid").ToString & "' AND AUD <> 'D' ")
                        Case enWeight_Types.WEIGHT_FIELD5
                            dtmp = mdtBSC_Evaluation.Select("empfield5unkid = '" & dtBSC_TabularGrid.Rows(dgvRow.Index).Item("empfield5unkid").ToString & "' AND AUD <> 'D' ")
                    End Select
                    If dtmp.Length > 0 Then
                        If menAssess = enAssessmentMode.SELF_ASSESSMENT Then
                            dgvRow.Cells(dgcolheselfBSC.Index).Value = CDec(dtmp(0).Item("result"))
                            dgvRow.Cells(dgcolheremarkBSC.Index).Value = dtmp(0).Item("remark")
                        ElseIf menAssess = enAssessmentMode.APPRAISER_ASSESSMENT Then
                            dgvRow.Cells(dgcolhaselfBSC.Index).Value = CDec(dtmp(0).Item("result"))
                            dgvRow.Cells(dgcolharemarkBSC.Index).Value = dtmp(0).Item("remark")
                            'Shani (23-Nov-2016) -- Start
                            'Enhancement - Add New PA Enhancement (Agreed Score,exc) Given by Akiba,KBC,TNP exc...
                            dgvRow.Cells(dgcolhAgreedScoreBSC.Index).Value = dtmp(0).Item("agreed_score")
                            'Shani (23-Nov-2016) -- End
                        ElseIf menAssess = enAssessmentMode.REVIEWER_ASSESSMENT Then
                            dgvRow.Cells(dgcolhrselfBSC.Index).Value = CDec(dtmp(0).Item("result"))
                            dgvRow.Cells(dgcolhrremarkBSC.Index).Value = dtmp(0).Item("remark")

                            'If ConfigParameter._Object._ReviewerScoreSetting = enReviewerScoreSetting.ReviewerScoreNeeded AndAlso _
                            '   ConfigParameter._Object._IsUseAgreedScore = True Then
                            '    dgvRow.Cells(dgcolhrselfBSC.Index).Value = CDec(dtmp(0).Item("agreed_score"))
                            'End If

                        End If
                    End If
                ElseIf menAction <> enAction.EDIT_ONE Then
                    'S.SANDEEP [07 FEB 2015] -- START
                    'If menAssess = enAssessmentMode.SELF_ASSESSMENT Then
                    '    dgvRow.Cells(dgcolheselfBSC.Index).Value = 0
                    'ElseIf menAssess = enAssessmentMode.APPRAISER_ASSESSMENT Then
                    '    dgvRow.Cells(dgcolhaselfBSC.Index).Value = 0
                    'ElseIf menAssess = enAssessmentMode.REVIEWER_ASSESSMENT Then
                    '    dgvRow.Cells(dgcolhrselfBSC.Index).Value = 0
                    'End If
                    'S.SANDEEP [07 FEB 2015] -- END

                    'Shani(14-FEB-2017) -- Start
                    'Enhancement - Add New PA Enhancement (Agreed Score,exc) Given by Akiba,KBC,TNP exc...
                    'S.SANDEEP |31-MAY-2019| -- START
                    'ISSUE/ENHANCEMENT : [PA SETTING CHANGES]
                    'If menAssess = enAssessmentMode.REVIEWER_ASSESSMENT Then
                    '    If ConfigParameter._Object._ReviewerScoreSetting = enReviewerScoreSetting.ReviewerScoreNeeded AndAlso _
                    '       ConfigParameter._Object._IsUseAgreedScore = True AndAlso _
                    '       CBool(dgvRow.Cells(objdgcolhIsGrpBSC.Index).Value) = False AndAlso dgvRow.Cells(dgcolhAgreedScoreBSC.Index).Value.ToString.Trim.Length > 0 Then
                    '        dgvRow.Cells(dgcolhrselfBSC.Index).Value = CDec(dgvRow.Cells(dgcolhAgreedScoreBSC.Index).Value)
                    '    End If

                    'End If
                    'S.SANDEEP |31-MAY-2019| -- END
                    'Shani(14-FEB-2017) -- End

                End If
            Next
            If ConfigParameter._Object._DontAllowToEditScoreGenbySys Then
                If menAssess = enAssessmentMode.SELF_ASSESSMENT Then
                    dgvBSC.Columns(dgcolheselfBSC.Index).ReadOnly = True
                ElseIf menAssess = enAssessmentMode.APPRAISER_ASSESSMENT Then
                    dgvBSC.Columns(dgcolhaselfBSC.Index).ReadOnly = True
                    If ConfigParameter._Object._IsUseAgreedScore Then
                        'Shani (23-Nov-2016) -- Start
                        'Enhancement - Add New PA Enhancement (Agreed Score,exc) Given by Akiba,KBC,TNP exc...
                        dgvBSC.Columns(dgcolhAgreedScoreBSC.Index).ReadOnly = True
                        'Shani (23-Nov-2016) -- End
                    End If
                ElseIf menAssess = enAssessmentMode.REVIEWER_ASSESSMENT Then
                    dgvBSC.Columns(dgcolhrselfBSC.Index).ReadOnly = True
                End If
            End If


            'Shani (26-Sep-2016) -- Start
            'Enhancement -Changes PA Flow And Add New screen Given By (Andrew)
            If menAction <> enAction.EDIT_ONE AndAlso ConfigParameter._Object._EnableBSCAutomaticRating Then
                Dim dRow As DataRow
                For Each dtRow As DataRow In dtBSC_TabularGrid.Select("IsGrp = 'False'")
                    dRow = mdtBSC_Evaluation.NewRow
                    dRow.Item("analysistranunkid") = -1
                    dRow.Item("analysisunkid") = -1
                    dRow.Item("empfield1unkid") = CInt(dtRow.Item("empfield1unkid"))
                    dRow.Item("remark") = ""
                    dRow.Item("AUD") = "A"
                    dRow.Item("GUID") = Guid.NewGuid.ToString
                    dRow.Item("isvoid") = False
                    dRow.Item("voiduserunkid") = -1
                    dRow.Item("voiddatetime") = DBNull.Value
                    dRow.Item("voidreason") = ""
                    dRow.Item("empfield2unkid") = CInt(dtRow.Item("empfield2unkid"))
                    dRow.Item("empfield3unkid") = CInt(dtRow.Item("empfield3unkid"))
                    dRow.Item("empfield4unkid") = CInt(dtRow.Item("empfield4unkid"))
                    dRow.Item("empfield5unkid") = CInt(dtRow.Item("empfield5unkid"))
                    dRow.Item("perspectiveunkid") = CInt(dtRow.Item("perspectiveunkid"))
                    dRow.Item("iPeriodId") = cboPeriod.SelectedValue
                    dRow.Item("iEmployeeId") = cboEmployee.SelectedValue
                    Select Case iExOrdr
                        Case enWeight_Types.WEIGHT_FIELD1
                            dRow.Item("iItemUnkid") = dRow.Item("empfield1unkid")
                        Case enWeight_Types.WEIGHT_FIELD2
                            dRow.Item("iItemUnkid") = dRow.Item("empfield2unkid")
                        Case enWeight_Types.WEIGHT_FIELD3
                            dRow.Item("iItemUnkid") = dRow.Item("empfield3unkid")
                        Case enWeight_Types.WEIGHT_FIELD4
                            dRow.Item("iItemUnkid") = dRow.Item("empfield4unkid")
                        Case enWeight_Types.WEIGHT_FIELD5
                            dRow.Item("iItemUnkid") = dRow.Item("empfield5unkid")
                    End Select

                    If menAssess = enAssessmentMode.SELF_ASSESSMENT Then
                        dRow.Item("iScore") = dtRow.Item("eself")
                        dRow.Item("result") = dtRow.Item("eself")
                        dRow.Item("item_weight") = dtRow.Item("eitem_weight")
                        dRow.Item("max_scale") = dtRow.Item("emax_scale")
                    ElseIf menAssess = enAssessmentMode.APPRAISER_ASSESSMENT Then
                        dRow.Item("iScore") = dtRow.Item("aself")
                        dRow.Item("result") = dtRow.Item("aself")
                        dRow.Item("item_weight") = dtRow.Item("aitem_weight")
                        dRow.Item("max_scale") = dtRow.Item("amax_scale")
                        'Shani (23-Nov-2016) -- Start
                        'Enhancement - Add New PA Enhancement (Agreed Score,exc) Given by Akiba,KBC,TNP exc...
                        If ConfigParameter._Object._IsUseAgreedScore Then
                            dRow.Item("agreed_score") = dtRow.Item("aself")
                            dtRow.Item("agreed_score") = dtRow.Item("aself")
                            dtRow.Item("agreedscore") = dtRow.Item("aself")
                        End If
                        'Shani (23-Nov-2016) -- End
                    ElseIf menAssess = enAssessmentMode.REVIEWER_ASSESSMENT Then
                        dRow.Item("iScore") = dtRow.Item("rself")
                        dRow.Item("result") = dtRow.Item("rself")
                        dRow.Item("item_weight") = dtRow.Item("ritem_weight")
                        dRow.Item("max_scale") = dtRow.Item("rmax_scale")
                    End If
                    mdtBSC_Evaluation.Rows.Add(dRow)
                    mdecMaxScale = 0 : mdecMaxScale = 0

                Next
                'Shani(14-FEB-2017) -- Start
                'Enhancement - Add New PA Enhancement (Agreed Score,exc) Given by Akiba,KBC,TNP exc...
            Else
                If menAction <> enAction.EDIT_ONE AndAlso menAssess = enAssessmentMode.REVIEWER_ASSESSMENT AndAlso ConfigParameter._Object._IsUseAgreedScore Then
                    Dim dRow As DataRow
                    For Each dtRow As DataRow In dtBSC_TabularGrid.Select("IsGrp = 'False'")
                        dRow = mdtBSC_Evaluation.NewRow
                        dRow.Item("analysistranunkid") = -1
                        dRow.Item("analysisunkid") = -1
                        dRow.Item("empfield1unkid") = CInt(dtRow.Item("empfield1unkid"))
                        dRow.Item("remark") = ""
                        dRow.Item("AUD") = "A"
                        dRow.Item("GUID") = Guid.NewGuid.ToString
                        dRow.Item("isvoid") = False
                        dRow.Item("voiduserunkid") = -1
                        dRow.Item("voiddatetime") = DBNull.Value
                        dRow.Item("voidreason") = ""
                        dRow.Item("empfield2unkid") = CInt(dtRow.Item("empfield2unkid"))
                        dRow.Item("empfield3unkid") = CInt(dtRow.Item("empfield3unkid"))
                        dRow.Item("empfield4unkid") = CInt(dtRow.Item("empfield4unkid"))
                        dRow.Item("empfield5unkid") = CInt(dtRow.Item("empfield5unkid"))
                        dRow.Item("perspectiveunkid") = CInt(dtRow.Item("perspectiveunkid"))
                        dRow.Item("iPeriodId") = cboPeriod.SelectedValue
                        dRow.Item("iEmployeeId") = cboEmployee.SelectedValue
                        Select Case iExOrdr
                            Case enWeight_Types.WEIGHT_FIELD1
                                dRow.Item("iItemUnkid") = dRow.Item("empfield1unkid")
                            Case enWeight_Types.WEIGHT_FIELD2
                                dRow.Item("iItemUnkid") = dRow.Item("empfield2unkid")
                            Case enWeight_Types.WEIGHT_FIELD3
                                dRow.Item("iItemUnkid") = dRow.Item("empfield3unkid")
                            Case enWeight_Types.WEIGHT_FIELD4
                                dRow.Item("iItemUnkid") = dRow.Item("empfield4unkid")
                            Case enWeight_Types.WEIGHT_FIELD5
                                dRow.Item("iItemUnkid") = dRow.Item("empfield5unkid")
                        End Select

                        dRow.Item("iScore") = dtRow.Item("agreed_score")
                        dRow.Item("result") = dtRow.Item("agreed_score")
                        dRow.Item("item_weight") = dtRow.Item("ritem_weight")
                        dRow.Item("max_scale") = dtRow.Item("rmax_scale")
                        mdtBSC_Evaluation.Rows.Add(dRow)
                        mdecMaxScale = 0 : mdecMaxScale = 0
                    Next
                End If
                'Shani(14-FEB-2017) -- End
            End If
            'Shani (26-Sep-2016) -- End



            dgvBSC.ColumnHeadersHeight = 35
            Call SetTotals(dgvBSC)

            'S.SANDEEP [23 APR 2015] -- START
            'AddHandler dgvBSC.CellValueChanged, AddressOf dgvBSC_CellValueChanged
            'S.SANDEEP [23 APR 2015] -- END
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Fill_BSC_Evaluation", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub Set_BSC_Grid_Style()
        Try
            'S.SANDEEP [23 APR 2015] -- START
            'RemoveHandler dgvBSC.CellValueChanged, AddressOf dgvBSC_CellValueChanged
            'S.SANDEEP [23 APR 2015] -- END
            RemoveHandler dgvBSC.CellEnter, AddressOf dgvBSC_CellEnter

            iWeightTotal = 0
            For Each dgvRow As DataGridViewRow In dgvBSC.Rows
                If IsNumeric(dgvRow.Cells(dgcolhBSCWeight.Index).Value) Then
                    iWeightTotal = iWeightTotal + CDec(dgvRow.Cells(dgcolhBSCWeight.Index).Value)
                End If
                If dgvRow.Cells(dgcolhBSCWeight.Index).Value.ToString.Trim.Length <= 0 AndAlso CBool(dgvRow.Cells(objdgcolhIsGrpBSC.Index).Value) = False Then
                    pCell.MakeMerge(dgvBSC, dgvRow.Index, dgvRow.Cells(dgcolhBSCScore.Index).ColumnIndex, dgvRow.Cells(dgcolhBSCScore.Index).ColumnIndex, Color.White, Color.White, Nothing, "", picStayView.Image)
                    If menAssess = enAssessmentMode.SELF_ASSESSMENT Then
                        pCell.MakeMerge(dgvBSC, dgvRow.Index, dgvRow.Cells(dgcolheselfBSC.Index).ColumnIndex, dgvRow.Cells(dgcolheselfBSC.Index).ColumnIndex, Color.White, Color.White, Nothing, "", picStayView.Image)
                        'Shani (23-Nov-2016) -- Start
                        'Enhancement - Add New PA Enhancement (Agreed Score,exc) Given by Akiba,KBC,TNP exc...
                        pCell.MakeMerge(dgvBSC, dgvRow.Index, dgvRow.Cells(dgcolheremarkBSC.Index).ColumnIndex, dgvRow.Cells(dgcolheremarkBSC.Index).ColumnIndex, Color.White, Color.White, Nothing, "", picStayView.Image)
                        'Shani (23-Nov123-2016-2016) -- End
                    ElseIf menAssess = enAssessmentMode.APPRAISER_ASSESSMENT Then
                        pCell.MakeMerge(dgvBSC, dgvRow.Index, dgvRow.Cells(dgcolheselfBSC.Index).ColumnIndex, dgvRow.Cells(dgcolheselfBSC.Index).ColumnIndex, Color.White, Color.White, Nothing, "", picStayView.Image)
                        pCell.MakeMerge(dgvBSC, dgvRow.Index, dgvRow.Cells(dgcolhaselfBSC.Index).ColumnIndex, dgvRow.Cells(dgcolhaselfBSC.Index).ColumnIndex, Color.White, Color.White, Nothing, "", picStayView.Image)
                        'Shani (23-Nov-2016) -- Start
                        'Enhancement - Add New PA Enhancement (Agreed Score,exc) Given by Akiba,KBC,TNP exc...
                        pCell.MakeMerge(dgvBSC, dgvRow.Index, dgvRow.Cells(dgcolhAgreedScoreBSC.Index).ColumnIndex, dgvRow.Cells(dgcolhAgreedScoreBSC.Index).ColumnIndex, Color.White, Color.White, Nothing, "", picStayView.Image)

                        pCell.MakeMerge(dgvBSC, dgvRow.Index, dgvRow.Cells(dgcolheremarkBSC.Index).ColumnIndex, dgvRow.Cells(dgcolheremarkBSC.Index).ColumnIndex, Color.White, Color.White, Nothing, "", picStayView.Image)
                        pCell.MakeMerge(dgvBSC, dgvRow.Index, dgvRow.Cells(dgcolharemarkBSC.Index).ColumnIndex, dgvRow.Cells(dgcolharemarkBSC.Index).ColumnIndex, Color.White, Color.White, Nothing, "", picStayView.Image)
                        'Shani (23-Nov-2016) -- End
                    ElseIf menAssess = enAssessmentMode.REVIEWER_ASSESSMENT Then
                        pCell.MakeMerge(dgvBSC, dgvRow.Index, dgvRow.Cells(dgcolheselfBSC.Index).ColumnIndex, dgvRow.Cells(dgcolheselfBSC.Index).ColumnIndex, Color.White, Color.White, Nothing, "", picStayView.Image)
                        pCell.MakeMerge(dgvBSC, dgvRow.Index, dgvRow.Cells(dgcolhaselfBSC.Index).ColumnIndex, dgvRow.Cells(dgcolhaselfBSC.Index).ColumnIndex, Color.White, Color.White, Nothing, "", picStayView.Image)
                        pCell.MakeMerge(dgvBSC, dgvRow.Index, dgvRow.Cells(dgcolhrselfBSC.Index).ColumnIndex, dgvRow.Cells(dgcolhrselfBSC.Index).ColumnIndex, Color.White, Color.White, Nothing, "", picStayView.Image)
                        'Shani (23-Nov-2016) -- Start
                        'Enhancement - Add New PA Enhancement (Agreed Score,exc) Given by Akiba,KBC,TNP exc...
                        pCell.MakeMerge(dgvBSC, dgvRow.Index, dgvRow.Cells(dgcolhAgreedScoreBSC.Index).ColumnIndex, dgvRow.Cells(dgcolhAgreedScoreBSC.Index).ColumnIndex, Color.White, Color.White, Nothing, "", picStayView.Image)

                        pCell.MakeMerge(dgvBSC, dgvRow.Index, dgvRow.Cells(dgcolheremarkBSC.Index).ColumnIndex, dgvRow.Cells(dgcolheremarkBSC.Index).ColumnIndex, Color.White, Color.White, Nothing, "", picStayView.Image)
                        pCell.MakeMerge(dgvBSC, dgvRow.Index, dgvRow.Cells(dgcolharemarkBSC.Index).ColumnIndex, dgvRow.Cells(dgcolharemarkBSC.Index).ColumnIndex, Color.White, Color.White, Nothing, "", picStayView.Image)
                        pCell.MakeMerge(dgvBSC, dgvRow.Index, dgvRow.Cells(dgcolhrremarkBSC.Index).ColumnIndex, dgvRow.Cells(dgcolhrremarkBSC.Index).ColumnIndex, Color.White, Color.White, Nothing, "", picStayView.Image)
                        'Shani (23-Nov-2016) -- End
                    End If
                    dgvRow.ReadOnly = True
                ElseIf dgvRow.Cells(dgcolhBSCWeight.Index).Value.ToString.Trim.Length <= 0 AndAlso CBool(dgvRow.Cells(objdgcolhIsGrpBSC.Index).Value) = True Then
                    Dim xVal As Integer = 1
                    If objdgcolhBSCField1.Visible = True Then
                        xVal = objdgcolhBSCField1.Index
                    ElseIf objdgcolhBSCField2.Visible = True Then
                        xVal = objdgcolhBSCField2.Index
                    ElseIf objdgcolhBSCField3.Visible = True Then
                        xVal = objdgcolhBSCField3.Index
                    ElseIf objdgcolhBSCField4.Visible = True Then
                        xVal = objdgcolhBSCField4.Index
                    ElseIf objdgcolhBSCField5.Visible = True Then
                        xVal = objdgcolhBSCField5.Index
                    ElseIf objdgcolhBSCField6.Visible = True Then
                        xVal = objdgcolhBSCField6.Index
                    ElseIf objdgcolhBSCField7.Visible = True Then
                        xVal = objdgcolhBSCField7.Index
                    ElseIf objdgcolhBSCField8.Visible = True Then
                        xVal = objdgcolhBSCField8.Index
                    End If
                    If ConfigParameter._Object._ScoringOptionId = enScoringOption.SC_WEIGHTED_BASED Then
                        'pCell.MakeMerge(dgvBSC, dgvRow.Index, dgvRow.Cells(dgcolhBSCScore.Index).ColumnIndex, dgvRow.Cells.Count - 1, Color.Gray, Color.White, Nothing, dgvRow.Cells(objdgcolhBSCField1.Index).Value, picStayView.Image)
                        pCell.MakeMerge(dgvBSC, dgvRow.Index, xVal, dgvRow.Cells.Count - 1, Color.Gray, Color.White, Nothing, dgvRow.Cells(objdgcolhBSCField1.Index).Value, picStayView.Image)
                    Else
                        pCell.MakeMerge(dgvBSC, dgvRow.Index, xVal, dgvRow.Cells.Count - 1, Color.Gray, Color.White, Nothing, dgvRow.Cells(objdgcolhBSCField1.Index).Value, picStayView.Image)
                    End If
                    dgvRow.ReadOnly = True
                End If

                If CBool(dgvRow.Cells(objdgcolhIsGrpBSC.Index).Value) = True Then
                    dgvRow.ReadOnly = True
                    If dgvRow.Cells(objdgcolhBSCCollaps.Index).Value Is Nothing Then
                        dgvRow.Cells(objdgcolhBSCCollaps.Index).Value = "-"
                        dgvRow.Cells(objdgcolhBSCCollaps.Index).Style.ForeColor = Color.White
                    End If
                    dgvRow.DefaultCellStyle.BackColor = Color.Gray
                    dgvRow.DefaultCellStyle.SelectionBackColor = Color.Gray
                    dgvRow.DefaultCellStyle.ForeColor = Color.White
                Else
                    dgvRow.Cells(objdgcolhBSCCollaps.Index).Value = ""
                End If
            Next

            AddHandler dgvBSC.CellEnter, AddressOf dgvBSC_CellEnter
            'S.SANDEEP [23 APR 2015] -- START
            'AddHandler dgvBSC.CellValueChanged, AddressOf dgvBSC_CellValueChanged
            'S.SANDEEP [23 APR 2015] -- END
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Set_BSC_Grid_Data_Style", mstrModuleName)
        Finally
        End Try
    End Sub

    'Shani (23-Nov-2016) -- Start
    'Enhancement - Add New PA Enhancement (Agreed Score,exc) Given by Akiba,KBC,TNP exc...
    'Private Sub Evaluated_Data_BSC(ByVal iResult As Decimal, ByVal iRemark As String, Optional ByVal isRemarkChanged As Boolean = False)
    Private Sub Evaluated_Data_BSC(ByVal iResult As Decimal, ByVal iRemark As String, Optional ByVal isRemarkChanged As Boolean = False, Optional ByVal iAgreedScore As Decimal = 0)
        'Shani (23-Nov-2016) -- End
        Try
            Dim dRow As DataRow = Nothing
            Dim dTemp() As DataRow = Nothing
            'Select Case iExOrdr
            '    Case enWeight_Types.WEIGHT_FIELD1
            '        dTemp = mdtBSC_Evaluation.Select("empfield1unkid = '" & dtBSC_TabularGrid.Rows(dgvBSC.CurrentRow.Index).Item("empfield1unkid").ToString & "' AND AUD <> 'D' ")
            '    Case enWeight_Types.WEIGHT_FIELD2
            '        dTemp = mdtBSC_Evaluation.Select("empfield2unkid = '" & dtBSC_TabularGrid.Rows(dgvBSC.CurrentRow.Index).Item("empfield2unkid").ToString & "' AND AUD <> 'D' ")
            '    Case enWeight_Types.WEIGHT_FIELD3
            '        dTemp = mdtBSC_Evaluation.Select("empfield3unkid = '" & dtBSC_TabularGrid.Rows(dgvBSC.CurrentRow.Index).Item("empfield3unkid").ToString & "' AND AUD <> 'D' ")
            '    Case enWeight_Types.WEIGHT_FIELD4
            '        dTemp = mdtBSC_Evaluation.Select("empfield4unkid = '" & dtBSC_TabularGrid.Rows(dgvBSC.CurrentRow.Index).Item("empfield4unkid").ToString & "' AND AUD <> 'D' ")
            '    Case enWeight_Types.WEIGHT_FIELD5
            '        dTemp = mdtBSC_Evaluation.Select("empfield5unkid = '" & dtBSC_TabularGrid.Rows(dgvBSC.CurrentRow.Index).Item("empfield5unkid").ToString & "' AND AUD <> 'D' ")
            'End Select

            dTemp = Get_Old_Data_BSC()

            'S.SANDEEP [ 20 DEC 2014 ] -- START
            'If mdtBSC_Evaluation IsNot Nothing AndAlso mdtBSC_Evaluation.Rows.Count > 0 Then
            '    If CDbl(mdtBSC_Evaluation.Compute("SUM(result)", "AUD <> 'D' ")) > xTotalWeightAssigned Then
            '        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 9, "Sorry you cannot go beyond the total weight assigned."), enMsgBoxStyle.Information)
            '        If menAssess = enAssessmentMode.SELF_ASSESSMENT Then
            '            dgvBSC.CurrentRow.Cells(dgcolheselfBSC.Index).Value = 0
            '        ElseIf menAssess = enAssessmentMode.APPRAISER_ASSESSMENT Then
            '            dgvBSC.CurrentRow.Cells(dgcolhaselfBSC.Index).Value = 0
            '        ElseIf menAssess = enAssessmentMode.REVIEWER_ASSESSMENT Then
            '            dgvBSC.CurrentRow.Cells(dgcolhrselfBSC.Index).Value = 0
            '        End If
            '        Exit Sub
            '    End If
            'End If
            If ConfigParameter._Object._ScoringOptionId = enScoringOption.SC_WEIGHTED_BASED Then
                If mdtBSC_Evaluation IsNot Nothing AndAlso mdtBSC_Evaluation.Rows.Count > 0 Then
                    'S.SANDEEP |08-JAN-2019| -- START
                    'If CDbl(mdtBSC_Evaluation.Compute("SUM(result)", "AUD <> 'D' ")) > xTotalWeightAssigned Then
                    '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 9, "Sorry you cannot go beyond the total weight assigned."), enMsgBoxStyle.Information)
                    '    If menAssess = enAssessmentMode.SELF_ASSESSMENT Then
                    '        dgvBSC.CurrentRow.Cells(dgcolheselfBSC.Index).Value = 0
                    '    ElseIf menAssess = enAssessmentMode.APPRAISER_ASSESSMENT Then
                    '        'Shani (23-Nov-2016) -- Start
                    '        'Enhancement - Add New PA Enhancement (Agreed Score,exc) Given by Akiba,KBC,TNP exc...
                    '        'dgvBSC.CurrentRow.Cells(dgcolhaselfBSC.Index).Value = 0
                    '        If dgvBSC.CurrentCell.ColumnIndex = dgcolhAgreedScoreBSC.Index Then
                    '            dgvBSC.CurrentRow.Cells(dgcolhAgreedScoreBSC.Index).Value = 0
                    '        Else
                    '            dgvBSC.CurrentRow.Cells(dgcolhaselfBSC.Index).Value = 0
                    '        End If
                    '        'Shani (23-Nov-2016) -- End
                    '    ElseIf menAssess = enAssessmentMode.REVIEWER_ASSESSMENT Then
                    '        dgvBSC.CurrentRow.Cells(dgcolhrselfBSC.Index).Value = 0
                    '    End If
                    '    Exit Sub
                    'End If
                    If ConfigParameter._Object._EnableBSCAutomaticRating = False Then
                        If CDbl(mdtBSC_Evaluation.Compute("SUM(result)", "AUD <> 'D' ")) > xTotalWeightAssigned Then
                            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 9, "Sorry you cannot go beyond the total weight assigned."), enMsgBoxStyle.Information)
                            If menAssess = enAssessmentMode.SELF_ASSESSMENT Then
                                dgvBSC.CurrentRow.Cells(dgcolheselfBSC.Index).Value = 0
                            ElseIf menAssess = enAssessmentMode.APPRAISER_ASSESSMENT Then
                                'Shani (23-Nov-2016) -- Start
                                'Enhancement - Add New PA Enhancement (Agreed Score,exc) Given by Akiba,KBC,TNP exc...
                                'dgvBSC.CurrentRow.Cells(dgcolhaselfBSC.Index).Value = 0
                                If dgvBSC.CurrentCell.ColumnIndex = dgcolhAgreedScoreBSC.Index Then
                                    dgvBSC.CurrentRow.Cells(dgcolhAgreedScoreBSC.Index).Value = 0
                                Else
                                    dgvBSC.CurrentRow.Cells(dgcolhaselfBSC.Index).Value = 0
                                End If
                                'Shani (23-Nov-2016) -- End
                            ElseIf menAssess = enAssessmentMode.REVIEWER_ASSESSMENT Then
                                dgvBSC.CurrentRow.Cells(dgcolhrselfBSC.Index).Value = 0
                            End If
                            Exit Sub
                        End If
                    End If
                    'S.SANDEEP |08-JAN-2019| -- END
                End If
            End If
            'S.SANDEEP [ 20 DEC 2014 ] -- END


            'S.SANDEEP [21 JAN 2015] -- START
            ''S.SANDEEP [ 01 JAN 2015 ] -- START
            'Dim xComputedScore As Decimal = 0
            'Dim xFieldId As Integer = -1 : Dim xFormula As String = String.Empty
            'Dim xFormulaModes As enAssess_Computation_Formulas

            'Select Case menAssess
            '    Case enAssessmentMode.SELF_ASSESSMENT
            '        xFormulaModes = enAssess_Computation_Formulas.BSC_EMP_TOTAL_SCORE
            '    Case enAssessmentMode.APPRAISER_ASSESSMENT
            '        xFormulaModes = enAssess_Computation_Formulas.BSC_ASR_TOTAL_SCORE
            '    Case enAssessmentMode.REVIEWER_ASSESSMENT
            '        xFormulaModes = enAssess_Computation_Formulas.BSC_REV_TOTAL_SCORE
            'End Select
            ''S.SANDEEP [ 01 JAN 2015 ] -- END
            'S.SANDEEP [21 JAN 2015] -- END



            If dTemp IsNot Nothing AndAlso dTemp.Length <= 0 Then
                dRow = mdtBSC_Evaluation.NewRow
                dRow.Item("analysistranunkid") = -1
                dRow.Item("analysisunkid") = mintAssessAnalysisUnkid
                dRow.Item("empfield1unkid") = CInt(dtBSC_TabularGrid.Rows(dgvBSC.CurrentRow.Index).Item("empfield1unkid"))
                dRow.Item("result") = iResult
                dRow.Item("remark") = iRemark
                dRow.Item("AUD") = "A"
                dRow.Item("GUID") = Guid.NewGuid.ToString
                dRow.Item("isvoid") = False
                dRow.Item("voiduserunkid") = -1
                dRow.Item("voiddatetime") = DBNull.Value
                dRow.Item("voidreason") = ""
                dRow.Item("empfield2unkid") = CInt(dtBSC_TabularGrid.Rows(dgvBSC.CurrentRow.Index).Item("empfield2unkid"))
                dRow.Item("empfield3unkid") = CInt(dtBSC_TabularGrid.Rows(dgvBSC.CurrentRow.Index).Item("empfield3unkid"))
                dRow.Item("empfield4unkid") = CInt(dtBSC_TabularGrid.Rows(dgvBSC.CurrentRow.Index).Item("empfield4unkid"))
                dRow.Item("empfield5unkid") = CInt(dtBSC_TabularGrid.Rows(dgvBSC.CurrentRow.Index).Item("empfield5unkid"))
                dRow.Item("perspectiveunkid") = CInt(dtBSC_TabularGrid.Rows(dgvBSC.CurrentRow.Index).Item("perspectiveunkid"))

                'S.SANDEEP [21 JAN 2015] -- START
                Select Case iExOrdr
                    Case enWeight_Types.WEIGHT_FIELD1
                        If dRow.Item("empfield1unkid") <= 0 Then Exit Sub
                    Case enWeight_Types.WEIGHT_FIELD2
                        If dRow.Item("empfield2unkid") <= 0 Then Exit Sub
                    Case enWeight_Types.WEIGHT_FIELD3
                        If dRow.Item("empfield3unkid") <= 0 Then Exit Sub
                    Case enWeight_Types.WEIGHT_FIELD4
                        If dRow.Item("empfield4unkid") <= 0 Then Exit Sub
                    Case enWeight_Types.WEIGHT_FIELD5
                        If dRow.Item("empfield5unkid") <= 0 Then Exit Sub
                End Select
                ''S.SANDEEP [ 01 JAN 2015 ] -- START
                'Select Case iExOrdr
                '    Case enWeight_Types.WEIGHT_FIELD1
                '        xFieldId = dRow.Item("empfield1unkid")
                '    Case enWeight_Types.WEIGHT_FIELD2
                '        xFieldId = dRow.Item("empfield2unkid")
                '    Case enWeight_Types.WEIGHT_FIELD3
                '        xFieldId = dRow.Item("empfield3unkid")
                '    Case enWeight_Types.WEIGHT_FIELD4
                '        xFieldId = dRow.Item("empfield4unkid")
                '    Case enWeight_Types.WEIGHT_FIELD5
                '        xFieldId = dRow.Item("empfield5unkid")
                'End Select

                'xComputedScore = objCmpMaster.Process_Assessment_Formula(xFormulaModes, _
                '                                                         CInt(cboPeriod.SelectedValue), _
                '                                                         xFieldId, _
                '                                                         CInt(cboEmployee.SelectedValue), _
                '                                                         ConfigParameter._Object._ScoringOptionId, _
                '                                                         iResult, _
                '                                                         menAssess, _
                '                                                         xFormula)
                'dRow.Item("computed_value") = xComputedScore
                'dRow.Item("formula_used") = xFormula
                ''S.SANDEEP [ 01 JAN 2015 ] -- END
                If isRemarkChanged = False Then
                    dRow.Item("item_weight") = mdecItemWeight
                    dRow.Item("max_scale") = mdecMaxScale
                End If
                dRow.Item("iPeriodId") = cboPeriod.SelectedValue
                Select Case iExOrdr
                    Case enWeight_Types.WEIGHT_FIELD1
                        dRow.Item("iItemUnkid") = dRow.Item("empfield1unkid")
                    Case enWeight_Types.WEIGHT_FIELD2
                        dRow.Item("iItemUnkid") = dRow.Item("empfield2unkid")
                    Case enWeight_Types.WEIGHT_FIELD3
                        dRow.Item("iItemUnkid") = dRow.Item("empfield3unkid")
                    Case enWeight_Types.WEIGHT_FIELD4
                        dRow.Item("iItemUnkid") = dRow.Item("empfield4unkid")
                    Case enWeight_Types.WEIGHT_FIELD5
                        dRow.Item("iItemUnkid") = dRow.Item("empfield5unkid")
                End Select
                dRow.Item("iEmployeeId") = cboEmployee.SelectedValue
                dRow.Item("iScore") = iResult
                'S.SANDEEP [21 JAN 2015] -- END

                'Shani (23-Nov-2016) -- Start
                'Enhancement - Add New PA Enhancement (Agreed Score,exc) Given by Akiba,KBC,TNP exc...
                dRow.Item("agreed_score") = iAgreedScore
                'Shani (23-Nov-2016) -- End


                mdtBSC_Evaluation.Rows.Add(dRow)
                'S.SANDEEP [21 JAN 2015] -- START
                mdecMaxScale = 0 : mdecMaxScale = 0
                'S.SANDEEP [21 JAN 2015] -- END
            Else
                If menAction <> enAction.EDIT_ONE Then
                    dTemp(0).Item("analysistranunkid") = -1
                Else
                    dTemp(0).Item("analysistranunkid") = dTemp(0).Item("analysistranunkid")
                End If
                dTemp(0).Item("analysisunkid") = mintAssessAnalysisUnkid
                dTemp(0).Item("empfield1unkid") = CInt(dtBSC_TabularGrid.Rows(dgvBSC.CurrentRow.Index).Item("empfield1unkid"))
                dTemp(0).Item("result") = iResult
                dTemp(0).Item("remark") = iRemark
                If IsDBNull(dTemp(0).Item("AUD")) Or CStr(dTemp(0).Item("AUD")).ToString.Trim = "" Then
                    dTemp(0).Item("AUD") = "U"
                End If
                dTemp(0).Item("GUID") = Guid.NewGuid.ToString
                dTemp(0).Item("isvoid") = False
                dTemp(0).Item("voiduserunkid") = -1
                dTemp(0).Item("voiddatetime") = DBNull.Value
                dTemp(0).Item("voidreason") = ""
                dTemp(0).Item("empfield2unkid") = CInt(dtBSC_TabularGrid.Rows(dgvBSC.CurrentRow.Index).Item("empfield2unkid"))
                dTemp(0).Item("empfield3unkid") = CInt(dtBSC_TabularGrid.Rows(dgvBSC.CurrentRow.Index).Item("empfield3unkid"))
                dTemp(0).Item("empfield4unkid") = CInt(dtBSC_TabularGrid.Rows(dgvBSC.CurrentRow.Index).Item("empfield4unkid"))
                dTemp(0).Item("empfield5unkid") = CInt(dtBSC_TabularGrid.Rows(dgvBSC.CurrentRow.Index).Item("empfield5unkid"))
                dTemp(0).Item("perspectiveunkid") = CInt(dtBSC_TabularGrid.Rows(dgvBSC.CurrentRow.Index).Item("perspectiveunkid"))


                'S.SANDEEP [21 JAN 2015] -- START
                ''S.SANDEEP [ 01 JAN 2015 ] -- START
                'Select Case iExOrdr
                '    Case enWeight_Types.WEIGHT_FIELD1
                '        xFieldId = dTemp(0).Item("empfield1unkid")
                '    Case enWeight_Types.WEIGHT_FIELD2
                '        xFieldId = dTemp(0).Item("empfield2unkid")
                '    Case enWeight_Types.WEIGHT_FIELD3
                '        xFieldId = dTemp(0).Item("empfield3unkid")
                '    Case enWeight_Types.WEIGHT_FIELD4
                '        xFieldId = dTemp(0).Item("empfield4unkid")
                '    Case enWeight_Types.WEIGHT_FIELD5
                '        xFieldId = dTemp(0).Item("empfield5unkid")
                'End Select

                'xComputedScore = objCmpMaster.Process_Assessment_Formula(xFormulaModes, _
                '                                                         CInt(cboPeriod.SelectedValue), _
                '                                                         xFieldId, _
                '                                                         CInt(cboEmployee.SelectedValue), _
                '                                                         ConfigParameter._Object._ScoringOptionId, _
                '                                                         iResult, _
                '                                                         menAssess, _
                '                                                         xFormula)
                'dTemp(0).Item("computed_value") = xComputedScore
                'dTemp(0).Item("formula_used") = xFormula
                ''S.SANDEEP [ 01 JAN 2015 ] -- END
                If isRemarkChanged = False Then
                    dTemp(0).Item("item_weight") = mdecItemWeight
                    dTemp(0).Item("max_scale") = mdecMaxScale
                End If

                dTemp(0).Item("iPeriodId") = cboPeriod.SelectedValue
                Select Case iExOrdr
                    Case enWeight_Types.WEIGHT_FIELD1
                        dTemp(0).Item("iItemUnkid") = dTemp(0).Item("empfield1unkid")
                    Case enWeight_Types.WEIGHT_FIELD2
                        dTemp(0).Item("iItemUnkid") = dTemp(0).Item("empfield2unkid")
                    Case enWeight_Types.WEIGHT_FIELD3
                        dTemp(0).Item("iItemUnkid") = dTemp(0).Item("empfield3unkid")
                    Case enWeight_Types.WEIGHT_FIELD4
                        dTemp(0).Item("iItemUnkid") = dTemp(0).Item("empfield4unkid")
                    Case enWeight_Types.WEIGHT_FIELD5
                        dTemp(0).Item("iItemUnkid") = dTemp(0).Item("empfield5unkid")
                End Select
                dTemp(0).Item("iEmployeeId") = cboEmployee.SelectedValue
                dTemp(0).Item("iScore") = iResult
                'S.SANDEEP [21 JAN 2015] -- END
                'Shani (23-Nov-2016) -- Start
                'Enhancement - Add New PA Enhancement (Agreed Score,exc) Given by Akiba,KBC,TNP exc...
                dTemp(0).Item("agreed_score") = iAgreedScore
                'Shani (23-Nov-2016) -- End

                mdtBSC_Evaluation.AcceptChanges()
                'S.SANDEEP [21 JAN 2015] -- START
                mdecMaxScale = 0 : mdecMaxScale = 0
                'S.SANDEEP [21 JAN 2015] -- END
            End If

            Call SetTotals(dgvBSC)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Evaluated_Data_BSC", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Function isAll_Assessed_BSC(Optional ByVal blnFlag As Boolean = True) As Boolean
        Try
            If mdtBSC_Evaluation.Rows.Count <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 22, "Sorry, you cannot do Save Commit Operation as you have not assessed all item(s) in some objective."), enMsgBoxStyle.Information)
                Return False
            End If
            Dim dtmp1() As DataRow = Nothing
            Dim dtmp2() As DataRow = Nothing

            Dim dView As DataView = dtBSC_TabularGrid.DefaultView

            Select Case iExOrdr
                Case enWeight_Types.WEIGHT_FIELD1
                    'S.SANDEEP [ 18 DEC 2014 ] -- START
                    'dtmp1 = dView.ToTable(True, "empfield1unkid").Select("empfield1unkid > 0 AND Weight > 0")
                    'dtmp2 = mdtBSC_Evaluation.Select("empfield1unkid > 0")
                    dtmp1 = dView.ToTable(True, "empfield1unkid", "Weight").Select("empfield1unkid > 0 AND Weight <> ''")
                    dtmp2 = mdtBSC_Evaluation.Select("empfield1unkid > 0")
                    'S.SANDEEP [ 18 DEC 2014 ] -- END

                Case enWeight_Types.WEIGHT_FIELD2
                    'S.SANDEEP [ 18 DEC 2014 ] -- START
                    'dtmp1 = dView.ToTable(True, "empfield2unkid").Select("empfield2unkid > 0 AND Weight <> '' ")
                    'dtmp2 = mdtBSC_Evaluation.Select("empfield2unkid > 0")

                    dtmp1 = dView.ToTable(True, "empfield2unkid", "Weight").Select("empfield2unkid > 0 AND Weight <> '' ")
                    dtmp2 = mdtBSC_Evaluation.Select("empfield2unkid > 0")
                    'S.SANDEEP [ 18 DEC 2014 ] -- END

                Case enWeight_Types.WEIGHT_FIELD3
                    'S.SANDEEP [ 18 DEC 2014 ] -- START
                    'dtmp1 = dView.ToTable(True, "empfield3unkid").Select("empfield3unkid > 0 AND Weight > 0")
                    'dtmp2 = mdtBSC_Evaluation.Select("empfield3unkid > 0")

                    dtmp1 = dView.ToTable(True, "empfield3unkid", "Weight").Select("empfield3unkid > 0 AND Weight <> '' ")
                    dtmp2 = mdtBSC_Evaluation.Select("empfield3unkid > 0")
                    'S.SANDEEP [ 18 DEC 2014 ] -- END

                Case enWeight_Types.WEIGHT_FIELD4
                    'S.SANDEEP [ 18 DEC 2014 ] -- START
                    'dtmp1 = dView.ToTable(True, "empfield4unkid").Select("empfield4unkid > 0 AND Weight > 0")
                    'dtmp2 = mdtBSC_Evaluation.Select("empfield4unkid > 0")

                    dtmp1 = dView.ToTable(True, "empfield4unkid", "Weight").Select("empfield4unkid > 0 AND Weight <> '' ")
                    dtmp2 = mdtBSC_Evaluation.Select("empfield4unkid > 0")
                    'S.SANDEEP [ 18 DEC 2014 ] -- END

                Case enWeight_Types.WEIGHT_FIELD5
                    'S.SANDEEP [ 18 DEC 2014 ] -- START
                    'dtmp1 = dView.ToTable(True, "empfield5unkid").Select("empfield5unkid > 0 AND Weight > 0")
                    'dtmp2 = mdtBSC_Evaluation.Select("empfield5unkid > 0")

                    dtmp1 = dView.ToTable(True, "empfield5unkid", "Weight").Select("empfield5unkid > 0 AND Weight <> '' ")
                    dtmp2 = mdtBSC_Evaluation.Select("empfield5unkid > 0")
                    'S.SANDEEP [ 18 DEC 2014 ] -- END
            End Select

            If dtmp1.Length <> dtmp2.Length Then
                If blnFlag = True Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 22, "Sorry, you cannot do Save Commit Operation as you have not assessed all item(s) in some objective."), enMsgBoxStyle.Information)
                    Return False
                    'Shani(30-MAR-2016) -- Start
                Else
                    Return False
                    'Shani(30-MAR-2016) -- End
                End If
            End If

            Return True
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "isAll_Assessed_BSC", mstrModuleName)
        Finally
        End Try
    End Function

    Private Function Get_Old_Data_BSC() As DataRow()
        Dim xTemp() As DataRow = Nothing
        Try
            If mdtBSC_Evaluation IsNot Nothing Then
                Select Case iExOrdr
                    Case enWeight_Types.WEIGHT_FIELD1
                        xTemp = mdtBSC_Evaluation.Select("empfield1unkid = '" & dtBSC_TabularGrid.Rows(dgvBSC.CurrentRow.Index).Item("empfield1unkid").ToString & "' AND AUD <> 'D' ")
                    Case enWeight_Types.WEIGHT_FIELD2
                        xTemp = mdtBSC_Evaluation.Select("empfield2unkid = '" & dtBSC_TabularGrid.Rows(dgvBSC.CurrentRow.Index).Item("empfield2unkid").ToString & "' AND AUD <> 'D' ")
                    Case enWeight_Types.WEIGHT_FIELD3
                        xTemp = mdtBSC_Evaluation.Select("empfield3unkid = '" & dtBSC_TabularGrid.Rows(dgvBSC.CurrentRow.Index).Item("empfield3unkid").ToString & "' AND AUD <> 'D' ")
                    Case enWeight_Types.WEIGHT_FIELD4
                        xTemp = mdtBSC_Evaluation.Select("empfield4unkid = '" & dtBSC_TabularGrid.Rows(dgvBSC.CurrentRow.Index).Item("empfield4unkid").ToString & "' AND AUD <> 'D' ")
                    Case enWeight_Types.WEIGHT_FIELD5
                        xTemp = mdtBSC_Evaluation.Select("empfield5unkid = '" & dtBSC_TabularGrid.Rows(dgvBSC.CurrentRow.Index).Item("empfield5unkid").ToString & "' AND AUD <> 'D' ")
                End Select
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Get_Old_Data_BSC", mstrModuleName)
        Finally
        End Try
        Return xTemp
    End Function

    '--------------------------------------------- COMPETENCIES/GENERAL EVALUATION METHODS
    Private Sub Fill_GE_Evaluation()
        Try
            'S.SANDEEP [23 APR 2015] -- START
            'RemoveHandler dgvGE.CellValueChanged, AddressOf dgvGE_CellValueChanged
            'S.SANDEEP [23 APR 2015] -- END


            'S.SANDEEP [29 JAN 2015] -- START
            'dtGE_TabularGrid = objEAnalysisMst.Get_GE_Evaluation_Data(menAssess, CInt(cboEmployee.SelectedValue), CInt(cboPeriod.SelectedValue), CInt(cboAssessor.SelectedValue), CInt(cboReviewer.SelectedValue), ConfigParameter._Object._ConsiderItemWeightAsNumber, -1)

            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'dtGE_TabularGrid = objEAnalysisMst.Get_GE_Evaluation_Data(menAssess, CInt(cboEmployee.SelectedValue), CInt(cboPeriod.SelectedValue), CInt(cboAssessor.SelectedValue), CInt(cboReviewer.SelectedValue), ConfigParameter._Object._ConsiderItemWeightAsNumber, -1, ConfigParameter._Object._Self_Assign_Competencies)

            'S.SANDEEP [04-AUG-2017] -- START
            'ISSUE/ENHANCEMENT : MAKE COMMON METHOD TO GET EMPLOYEE ASSESSOR/REVIEWER
            'dtGE_TabularGrid = objEAnalysisMst.Get_GE_Evaluation_Data(menAssess, CInt(cboEmployee.SelectedValue), CInt(cboPeriod.SelectedValue), CInt(cboAssessor.SelectedValue), CInt(cboReviewer.SelectedValue), ConfigParameter._Object._ConsiderItemWeightAsNumber, -1, ConfigParameter._Object._Self_Assign_Competencies, eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate))
            dtGE_TabularGrid = objEAnalysisMst.Get_GE_Evaluation_Data(menAssess, CInt(cboEmployee.SelectedValue), CInt(cboPeriod.SelectedValue), CInt(cboAssessor.SelectedValue), CInt(cboReviewer.SelectedValue), ConfigParameter._Object._ConsiderItemWeightAsNumber, -1, ConfigParameter._Object._Self_Assign_Competencies, eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), FinancialYear._Object._DatabaseName)
            'S.SANDEEP [04-AUG-2017] -- END

            'S.SANDEEP [04 JUN 2015] -- END

            'S.SANDEEP [29 JAN 2015] -- END

            dgvGE.AutoGenerateColumns = False
            dgcolheval_itemGE.DataPropertyName = "eval_item"
            If ConfigParameter._Object._ScoringOptionId = enScoringOption.SC_SCALE_BASED Then
                dgcolhGEScore.DataPropertyName = "score"
                dgcolhGEScore.Visible = True
                dgcolhGEScore.DefaultCellStyle.SelectionBackColor = Color.White
                dgcolhGEScore.DefaultCellStyle.SelectionForeColor = Color.White
                dgcolhGEWeight.Visible = False
            Else
                dgcolhGEScore.Visible = False
                dgcolhGEWeight.Visible = True
            End If
            dgcolhGEWeight.DataPropertyName = "Weight"
            dgcolheselfGE.DataPropertyName = "eself"
            dgcolheremarkGE.DataPropertyName = "eremark"
            objdgcolhedisplayGE.Visible = ConfigParameter._Object._ConsiderItemWeightAsNumber
            If menAssess = enAssessmentMode.APPRAISER_ASSESSMENT Then
                dgcolhaselfGE.DataPropertyName = "aself"
                dgcolharemarkGE.DataPropertyName = "aremark"
                dgcolheselfGE.ReadOnly = True : dgcolheremarkGE.ReadOnly = True
                objdgcolhadisplayGE.Visible = ConfigParameter._Object._ConsiderItemWeightAsNumber
                'Shani (23-Nov-2016) -- Start
                'Enhancement - Add New PA Enhancement (Agreed Score,exc) Given by Akiba,KBC,TNP exc...
                dgcolhaAgreedScoreGE.DataPropertyName = "agreedscore"
                dgcolhaAgreedScoreGE.Visible = ConfigParameter._Object._IsUseAgreedScore
                'Shani (23-Nov-2016) -- End
            Else
                dgcolhaselfGE.Visible = False
                dgcolharemarkGE.Visible = False
                objdgcolhadisplayGE.Visible = False
                'Shani (23-Nov-2016) -- Start
                'Enhancement - Add New PA Enhancement (Agreed Score,exc) Given by Akiba,KBC,TNP exc...
                dgcolhaAgreedScoreGE.Visible = False
                'Shani (23-Nov-2016) -- End


            End If
            If menAssess = enAssessmentMode.REVIEWER_ASSESSMENT Then
                dgcolhaselfGE.DataPropertyName = "aself"
                dgcolharemarkGE.DataPropertyName = "aremark"
                dgcolhrselfGE.DataPropertyName = "rself"
                dgcolhrremarkGE.DataPropertyName = "rremark"
                dgcolheselfGE.ReadOnly = True : dgcolheremarkGE.ReadOnly = True
                dgcolhaselfGE.ReadOnly = True : dgcolharemarkGE.ReadOnly = True

                dgcolhaselfGE.Visible = True
                dgcolharemarkGE.Visible = True

                'Shani (23-Nov-2016) -- Start
                'Enhancement - Add New PA Enhancement (Agreed Score,exc) Given by Akiba,KBC,TNP exc...

                dgcolhaAgreedScoreGE.DataPropertyName = "agreedscore"
                If ConfigParameter._Object._IsUseAgreedScore Then
                    dgcolhaAgreedScoreGE.ReadOnly = True
                    'S.SANDEEP |31-MAY-2019| -- START
                    'ISSUE/ENHANCEMENT : [PA SETTING CHANGES]
                Else
                    dgcolhaAgreedScoreGE.Visible = False
                End If
                'S.SANDEEP |31-MAY-2019| -- END
                'Shani (23-Nov-2016) -- End

                'Shani(14-FEB-2017) -- Start
                'Enhancement - Add New PA Enhancement (Agreed Score,exc) Given by Akiba,KBC,TNP exc...
                'dgcolhaAgreedScoreGE.Visible = True
                If ConfigParameter._Object._ReviewerScoreSetting = enReviewerScoreSetting.ReviewerScoreNotNecessary Then
                    'S.SANDEEP |31-MAY-2019| -- START
                    'ISSUE/ENHANCEMENT : [PA SETTING CHANGES]
                    'dgcolhaAgreedScoreGE.Visible = True
                    'S.SANDEEP |31-MAY-2019| -- END
                    dgcolhrselfGE.Visible = False
                ElseIf ConfigParameter._Object._ReviewerScoreSetting = enReviewerScoreSetting.ReviewerScoreNeeded Then
                    'S.SANDEEP |31-MAY-2019| -- START
                    'ISSUE/ENHANCEMENT : [PA SETTING CHANGES]
                    'dgcolhaAgreedScoreGE.Visible = False
                    'S.SANDEEP |31-MAY-2019| -- END

                    dgcolhrselfGE.Visible = True
                End If
                'Shani(14-FEB-2017) -- End

                objdgcolhadisplayGE.Visible = ConfigParameter._Object._ConsiderItemWeightAsNumber
                objdgcolhrdisplayGE.Visible = ConfigParameter._Object._ConsiderItemWeightAsNumber
            Else
                dgcolhrselfGE.Visible = False
                dgcolhrremarkGE.Visible = False
                objdgcolhrdisplayGE.Visible = False
            End If

            objdgcolhscalemasterunkidGE.DataPropertyName = "scalemasterunkid"
            objdgcolhcompetenciesunkidGE.DataPropertyName = "competenciesunkid"
            objdgcolhassessgroupunkidGE.DataPropertyName = "assessgroupunkid"
            objdgcolhIsGrpGE.DataPropertyName = "IsGrp"
            objdgcolhIsPGrpGE.DataPropertyName = "IsPGrp"
            objdgcolhGrpIdGE.DataPropertyName = "GrpId"

            dgvGE.DataSource = dtGE_TabularGrid

            dgcolhGEWeight.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
            dgcolhGEWeight.HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleRight

            dgcolheselfGE.HeaderText = dtGE_TabularGrid.Columns(dgcolheselfGE.DataPropertyName).Caption
            dgcolheremarkGE.HeaderText = dtGE_TabularGrid.Columns(dgcolheremarkGE.DataPropertyName).Caption


            If dgcolhaselfGE.DataPropertyName.Trim.Length > 0 Then
                dgcolhaselfGE.HeaderText = dtGE_TabularGrid.Columns(dgcolhaselfGE.DataPropertyName).Caption
            End If
            If dgcolharemarkGE.DataPropertyName.Trim.Length > 0 Then
                dgcolharemarkGE.HeaderText = dtGE_TabularGrid.Columns(dgcolharemarkGE.DataPropertyName).Caption
            End If

            If dgcolhrselfGE.DataPropertyName.Trim.Length > 0 Then

                'Shani(14-FEB-2017) -- Start
                'Enhancement - Add New PA Enhancement (Agreed Score,exc) Given by Akiba,KBC,TNP exc...
                'dgcolhrselfGE.HeaderText = dtGE_TabularGrid.Columns(dgcolhrselfGE.DataPropertyName).Caption
                If ConfigParameter._Object._IsUseAgreedScore Then
                    dgcolhrselfGE.HeaderText = "Agreed Score"
                Else
                    dgcolhrselfGE.HeaderText = dtGE_TabularGrid.Columns(dgcolhrselfGE.DataPropertyName).Caption
                End If
                'Shani(14-FEB-2017) -- End
            End If
            If dgcolhrremarkGE.DataPropertyName.Trim.Length > 0 Then

                'Shani(14-FEB-2017) -- Start
                'Enhancement - Add New PA Enhancement (Agreed Score,exc) Given by Akiba,KBC,TNP exc...
                'dgcolhrremarkGE.HeaderText = dtGE_TabularGrid.Columns(dgcolhrremarkGE.DataPropertyName).Caption
                If ConfigParameter._Object._IsUseAgreedScore AndAlso ConfigParameter._Object._ReviewerScoreSetting = enReviewerScoreSetting.ReviewerScoreNeeded Then
                    dgcolhrremarkGE.HeaderText = "Agreed Remark"
                Else
                    dgcolhrremarkGE.HeaderText = dtGE_TabularGrid.Columns(dgcolhrremarkGE.DataPropertyName).Caption
                End If
                'Shani(14-FEB-2017) -- End
            End If

            'S.SANDEEP [07 FEB 2015] -- START
            Dim xWidth As Integer = 0
            Dim objFMst As New clsAssess_Field_Master

            xWidth = objFMst.GetColumnWidth(clsAssess_Field_Master.enColWidthType.COL_EVAL, clsAssess_Field_Master.enOtherInfoField.EMP_REMARK)
            If xWidth > 0 Then dgcolheremarkGE.Width = xWidth

            xWidth = objFMst.GetColumnWidth(clsAssess_Field_Master.enColWidthType.COL_EVAL, clsAssess_Field_Master.enOtherInfoField.ASR_REMARK)
            If xWidth > 0 Then dgcolharemarkGE.Width = xWidth

            xWidth = objFMst.GetColumnWidth(clsAssess_Field_Master.enColWidthType.COL_EVAL, clsAssess_Field_Master.enOtherInfoField.REV_REMARK)
            If xWidth > 0 Then dgcolhrremarkGE.Width = xWidth

            objFMst = Nothing
            'S.SANDEEP [07 FEB 2015] -- END

            For Each dgvRow As DataGridViewRow In dgvGE.Rows
                'If menAction = enAction.EDIT_ONE Then
                '    Dim dtmp() As DataRow = Nothing
                '    dtmp = mdtGE_Evaluation.Select("assessgroupunkid = '" & dtGE_TabularGrid.Rows(dgvRow.Index).Item("assessgroupunkid").ToString & "' AND competenciesunkid = '" & dtGE_TabularGrid.Rows(dgvRow.Index).Item("competenciesunkid").ToString & "' AND AUD <> 'D' ")
                '    If dtmp.Length > 0 Then
                '        If menAssess = enAssessmentMode.SELF_ASSESSMENT Then
                '            If ConfigParameter._Object._ConsiderItemWeightAsNumber Then
                '                dgvRow.Cells(objdgcolhedisplayGE.Index).Value = CDbl(dgvRow.Cells(dgcolhGEWeight.Index).Value) * CInt(dtmp(0).Item("result"))
                '            Else
                '                dgvRow.Cells(objdgcolhedisplayGE.Index).Value = 1 * CInt(dtmp(0).Item("result"))
                '                dtmp(0).Item("dresult") = CInt(dtmp(0).Item("result"))
                '            End If
                '        ElseIf menAssess = enAssessmentMode.APPRAISER_ASSESSMENT Then
                '            If ConfigParameter._Object._ConsiderItemWeightAsNumber Then
                '                dgvRow.Cells(objdgcolhedisplayGE.Index).Value = CDbl(dgvRow.Cells(dgcolhGEWeight.Index).Value) * CInt(dgvRow.Cells(dgcolheselfGE.Index).Value)
                '            Else
                '                dgvRow.Cells(objdgcolhedisplayGE.Index).Value = 1 * CInt(dgvRow.Cells(dgcolheselfGE.Index).Value)
                '                dtmp(0).Item("dresult") = CInt(dtmp(0).Item("result"))
                '            End If

                '            If ConfigParameter._Object._ConsiderItemWeightAsNumber Then
                '                dgvRow.Cells(objdgcolhadisplayGE.Index).Value = CDbl(dgvRow.Cells(dgcolhGEWeight.Index).Value) * CInt(dtmp(0).Item("result"))
                '            Else
                '                dgvRow.Cells(objdgcolhadisplayGE.Index).Value = 1 * CInt(dtmp(0).Item("result"))
                '                dtmp(0).Item("dresult") = CInt(dtmp(0).Item("result"))
                '            End If
                '        ElseIf menAssess = enAssessmentMode.REVIEWER_ASSESSMENT Then
                '            If ConfigParameter._Object._ConsiderItemWeightAsNumber Then
                '                dgvRow.Cells(objdgcolhedisplayGE.Index).Value = CDbl(dgvRow.Cells(dgcolhGEWeight.Index).Value) * CInt(dgvRow.Cells(dgcolheselfGE.Index).Value)
                '            Else
                '                dgvRow.Cells(objdgcolhedisplayGE.Index).Value = 1 * CInt(dgvRow.Cells(dgcolheselfGE.Index).Value)
                '                dtmp(0).Item("dresult") = CInt(dtmp(0).Item("result"))
                '            End If

                '            If ConfigParameter._Object._ConsiderItemWeightAsNumber Then
                '                dgvRow.Cells(objdgcolhadisplayGE.Index).Value = CDbl(dgvRow.Cells(dgcolhGEWeight.Index).Value) * CInt(dgvRow.Cells(dgcolhaselfGE.Index).Value)
                '            Else
                '                dgvRow.Cells(objdgcolhadisplayGE.Index).Value = 1 * CInt(dgvRow.Cells(dgcolhaselfGE.Index).Value)
                '                dtmp(0).Item("dresult") = CInt(dtmp(0).Item("result"))
                '            End If

                '            If ConfigParameter._Object._ConsiderItemWeightAsNumber Then
                '                dgvRow.Cells(objdgcolhrdisplayGE.Index).Value = CDbl(dgvRow.Cells(dgcolhGEWeight.Index).Value) * CInt(dtmp(0).Item("result"))
                '            Else
                '                dgvRow.Cells(objdgcolhrdisplayGE.Index).Value = 1 * CInt(dtmp(0).Item("result"))
                '                dtmp(0).Item("dresult") = CInt(dtmp(0).Item("result"))
                '            End If
                '        End If
                '    End If
                'Else
                If menAction <> enAction.EDIT_ONE Then
                    If menAssess = enAssessmentMode.SELF_ASSESSMENT Then
                        'S.SANDEEP [ 16 JAN 2015 ] -- START
                        'dgvRow.Cells(dgcolheselfGE.Index).Value = 0 : dgvRow.Cells(dgcolheremarkGE.Index).Value = ""
                        'S.SANDEEP [ 16 JAN 2015 ] -- END
                    ElseIf menAssess = enAssessmentMode.APPRAISER_ASSESSMENT Then
                        If ConfigParameter._Object._ConsiderItemWeightAsNumber Then
                            dgvRow.Cells(objdgcolhedisplayGE.Index).Value = CDbl(IIf(dgvRow.Cells(dgcolhGEWeight.Index).Value = "", 1, dgvRow.Cells(dgcolhGEWeight.Index).Value)) * CInt(IIf(dgvRow.Cells(dgcolheselfGE.Index).Value = "", 1, dgvRow.Cells(dgcolheselfGE.Index).Value))
                        Else
                            dgvRow.Cells(objdgcolhedisplayGE.Index).Value = 1 * CInt(IIf(dgvRow.Cells(dgcolheselfGE.Index).Value = "", 1, dgvRow.Cells(dgcolheselfGE.Index).Value))
                        End If
                        'S.SANDEEP [21 JAN 2015] -- START
                        'dgvRow.Cells(dgcolhaselfGE.Index).Value = 0 : dgvRow.Cells(dgcolharemarkGE.Index).Value = ""
                        'S.SANDEEP [21 JAN 2015] -- END
                    ElseIf menAssess = enAssessmentMode.REVIEWER_ASSESSMENT Then
                        If ConfigParameter._Object._ConsiderItemWeightAsNumber Then
                            dgvRow.Cells(objdgcolhedisplayGE.Index).Value = CDbl(IIf(dgvRow.Cells(dgcolhGEWeight.Index).Value = "", 1, dgvRow.Cells(dgcolhGEWeight.Index).Value)) * CInt(IIf(dgvRow.Cells(dgcolheselfGE.Index).Value = "", 1, dgvRow.Cells(dgcolheselfGE.Index).Value))
                        Else
                            dgvRow.Cells(objdgcolhedisplayGE.Index).Value = 1 * CInt(IIf(dgvRow.Cells(dgcolheselfGE.Index).Value = "", 1, dgvRow.Cells(dgcolheselfGE.Index).Value))
                        End If

                        If ConfigParameter._Object._ConsiderItemWeightAsNumber Then
                            dgvRow.Cells(objdgcolhadisplayGE.Index).Value = CDbl(IIf(dgvRow.Cells(dgcolhGEWeight.Index).Value = "", 1, dgvRow.Cells(dgcolhGEWeight.Index).Value)) * CInt(IIf(dgvRow.Cells(dgcolhaselfGE.Index).Value = "", 1, dgvRow.Cells(dgcolhaselfGE.Index).Value))
                        Else
                            dgvRow.Cells(objdgcolhadisplayGE.Index).Value = 1 * CInt(IIf(dgvRow.Cells(dgcolhaselfGE.Index).Value = "", 1, dgvRow.Cells(dgcolhaselfGE.Index).Value))
                        End If
                        'S.SANDEEP [21 JAN 2015] -- START
                        'dgvRow.Cells(dgcolhrselfGE.Index).Value = 0 : dgvRow.Cells(dgcolhrremarkGE.Index).Value = ""
                        'S.SANDEEP [21 JAN 2015] -- END

                        'Shani(14-FEB-2017) -- Start
                        'Enhancement - Add New PA Enhancement (Agreed Score,exc) Given by Akiba,KBC,TNP exc...
                        'S.SANDEEP |31-MAY-2019| -- START
                        'ISSUE/ENHANCEMENT : [PA SETTING CHANGES]
                        'If ConfigParameter._Object._ReviewerScoreSetting = enReviewerScoreSetting.ReviewerScoreNeeded AndAlso _
                        '       ConfigParameter._Object._IsUseAgreedScore = True AndAlso _
                        '       CBool(dgvRow.Cells(objdgcolhIsGrpGE.Index).Value) = False AndAlso _
                        '       CBool(dgvRow.Cells(objdgcolhIsPGrpGE.Index).Value) = False AndAlso dgvRow.Cells(dgcolhaAgreedScoreGE.Index).Value.ToString.Trim.Trim.Length > 0 Then
                        '    dgvRow.Cells(dgcolhrselfGE.Index).Value = CDec(dgvRow.Cells(dgcolhaAgreedScoreGE.Index).Value)
                        'End If
                        'S.SANDEEP |31-MAY-2019| -- END

                        'Shani(14-FEB-2017) -- End
                    End If
                End If
            Next

            'Shani(14-FEB-2017) -- Start
            'Enhancement - Add New PA Enhancement (Agreed Score,exc) Given by Akiba,KBC,TNP exc...
            If menAction <> enAction.EDIT_ONE AndAlso menAssess = enAssessmentMode.REVIEWER_ASSESSMENT AndAlso ConfigParameter._Object._IsUseAgreedScore Then
                Dim dRow As DataRow
                For Each dtRow As DataRow In dtGE_TabularGrid.Select("IsGrp = False AND IsPGrp = False")
                    dRow = mdtGE_Evaluation.NewRow
                    Dim iDecWeight As Decimal = 0 : Dim iGEAgreedScore As Decimal = 0
                    Decimal.TryParse(dtRow.Item("Weight"), iDecWeight)
                    Decimal.TryParse(dtRow.Item("agreed_score"), iGEAgreedScore)
                    dRow.Item("analysistranunkid") = -1
                    dRow.Item("analysisunkid") = mintAssessAnalysisUnkid
                    dRow.Item("assessgroupunkid") = dtRow.Item("assessgroupunkid")
                    dRow.Item("competenciesunkid") = dtRow.Item("competenciesunkid")
                    dRow.Item("result") = iGEAgreedScore
                    dRow.Item("remark") = ""
                    dRow.Item("AUD") = "A"
                    dRow.Item("GUID") = Guid.NewGuid.ToString
                    dRow.Item("isvoid") = False
                    dRow.Item("voiduserunkid") = -1
                    dRow.Item("voiddatetime") = DBNull.Value
                    dRow.Item("voidreason") = ""
                    dRow.Item("dresult") = iGEAgreedScore * iDecWeight
                    dRow.Item("item_weight") = iDecWeight
                    dRow.Item("max_scale") = 0
                    dRow.Item("iPeriodId") = cboPeriod.SelectedValue
                    dRow.Item("iEmployeeId") = cboEmployee.SelectedValue
                    dRow.Item("iScore") = CDec(dtRow.Item("agreed_score"))
                    dRow.Item("iItemUnkid") = dRow.Item("competenciesunkid")
                    dRow.Item("agreed_score") = 0
                    mdtGE_Evaluation.Rows.Add(dRow)
                    mdecItemWeight = 0 : mdecMaxScale = 0
                Next
            End If
            'Shani(14-FEB-2017) -- End

            dgvGE.Refresh()
            dgvGE.ColumnHeadersHeight = 35
            Call SetTotals(dgvGE)


            'S.SANDEEP [23 APR 2015] -- START
            'AddHandler dgvGE.CellValueChanged, AddressOf dgvGE_CellValueChanged
            'S.SANDEEP [23 APR 2015] -- END
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Fill_GE_Evaluation", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub Set_Grid_Style_GE()
        Try
            'S.SANDEEP [23 APR 2015] -- START
            'RemoveHandler dgvGE.CellValueChanged, AddressOf dgvGE_CellValueChanged
            'S.SANDEEP [23 APR 2015] -- END

            RemoveHandler dgvGE.CellEnter, AddressOf dgvGE_CellEnter

            iWeightTotal = 0
            For Each dgvRow As DataGridViewRow In dgvGE.Rows
                If IsNumeric(dgvRow.Cells(dgcolhGEWeight.Index).Value) Then
                    iWeightTotal = iWeightTotal + CDec(dgvRow.Cells(dgcolhGEWeight.Index).Value)
                End If

                'S.SANDEEP [31 AUG 2016] -- START
                'ENHANCEMENT : INCLUSION OF GRADES ON ASSESSMENT GROUP {BY ANDREW}
                If CBool(dgvRow.Cells(objdgcolhIsPGrpGE.Index).Value) = True Then
                    dgvRow.Cells(objdgcolhInfo.Index).Value = imgBlankIcon
                Else
                    dgvRow.Cells(objdgcolhInfo.Index).Value = imgInforIcon
                End If
                'S.SANDEEP [31 AUG 2016] -- START

                If CBool(dgvRow.Cells(objdgcolhIsPGrpGE.Index).Value) = True Then
                    pCell.MakeMerge(dgvGE, dgvRow.Index, 0, 0, Color.White, Color.White, Nothing, "", picStayView.Image)
                    pCell.MakeMerge(dgvGE, dgvRow.Index, dgvRow.Cells(dgcolhGEScore.Index).ColumnIndex, dgvRow.Cells.Count - 1, Color.White, Color.White, Nothing, "", picStayView.Image)
                    dgvRow.ReadOnly = True
                    dgvRow.DefaultCellStyle.Font = New Font(Me.dgvGE.Font, FontStyle.Bold)
                    dgvRow.ReadOnly = True
                ElseIf dgvRow.Cells(dgcolhGEWeight.Index).Value.ToString.Trim.Length <= 0 AndAlso CBool(dgvRow.Cells(objdgcolhIsGrpGE.Index).Value) = False Then
                    pCell.MakeMerge(dgvGE, dgvRow.Index, dgvRow.Cells(dgcolhGEScore.Index).ColumnIndex, dgvRow.Cells(dgcolhGEScore.Index).ColumnIndex, Color.White, Color.White, Nothing, "", picStayView.Image)
                    If menAssess = enAssessmentMode.SELF_ASSESSMENT Then
                        pCell.MakeMerge(dgvGE, dgvRow.Index, dgvRow.Cells(dgcolheselfGE.Index).ColumnIndex, dgvRow.Cells(dgcolheselfGE.Index).ColumnIndex, Color.White, Color.White, Nothing, "", picStayView.Image)

                        'Shani (23-Nov-2016) -- Start
                        'Enhancement - Add New PA Enhancement (Agreed Score,exc) Given by Akiba,KBC,TNP exc...
                        pCell.MakeMerge(dgvGE, dgvRow.Index, dgvRow.Cells(dgcolheremarkGE.Index).ColumnIndex, dgvRow.Cells(dgcolheremarkGE.Index).ColumnIndex, Color.White, Color.White, Nothing, "", picStayView.Image)
                        'Shani (23-Nov123-2016-2016) -- End


                    ElseIf menAssess = enAssessmentMode.APPRAISER_ASSESSMENT Then
                        pCell.MakeMerge(dgvGE, dgvRow.Index, dgvRow.Cells(dgcolheselfGE.Index).ColumnIndex, dgvRow.Cells(dgcolheselfGE.Index).ColumnIndex, Color.White, Color.White, Nothing, "", picStayView.Image)
                        pCell.MakeMerge(dgvGE, dgvRow.Index, dgvRow.Cells(dgcolhaselfGE.Index).ColumnIndex, dgvRow.Cells(dgcolhaselfGE.Index).ColumnIndex, Color.White, Color.White, Nothing, "", picStayView.Image)
                        'Shani (23-Nov-2016) -- Start
                        'Enhancement - Add New PA Enhancement (Agreed Score,exc) Given by Akiba,KBC,TNP exc...
                        pCell.MakeMerge(dgvGE, dgvRow.Index, dgvRow.Cells(dgcolhaAgreedScoreGE.Index).ColumnIndex, dgvRow.Cells(dgcolhaAgreedScoreGE.Index).ColumnIndex, Color.White, Color.White, Nothing, "", picStayView.Image)

                        pCell.MakeMerge(dgvGE, dgvRow.Index, dgvRow.Cells(dgcolheremarkGE.Index).ColumnIndex, dgvRow.Cells(dgcolheremarkGE.Index).ColumnIndex, Color.White, Color.White, Nothing, "", picStayView.Image)
                        pCell.MakeMerge(dgvGE, dgvRow.Index, dgvRow.Cells(dgcolharemarkGE.Index).ColumnIndex, dgvRow.Cells(dgcolharemarkGE.Index).ColumnIndex, Color.White, Color.White, Nothing, "", picStayView.Image)
                        'Shani (23-Nov-2016) -- End
                    ElseIf menAssess = enAssessmentMode.REVIEWER_ASSESSMENT Then
                        pCell.MakeMerge(dgvGE, dgvRow.Index, dgvRow.Cells(dgcolheselfGE.Index).ColumnIndex, dgvRow.Cells(dgcolheselfGE.Index).ColumnIndex, Color.White, Color.White, Nothing, "", picStayView.Image)
                        pCell.MakeMerge(dgvGE, dgvRow.Index, dgvRow.Cells(dgcolhaselfGE.Index).ColumnIndex, dgvRow.Cells(dgcolhaselfGE.Index).ColumnIndex, Color.White, Color.White, Nothing, "", picStayView.Image)
                        pCell.MakeMerge(dgvGE, dgvRow.Index, dgvRow.Cells(dgcolhrselfGE.Index).ColumnIndex, dgvRow.Cells(dgcolhrselfGE.Index).ColumnIndex, Color.White, Color.White, Nothing, "", picStayView.Image)
                        'Shani (23-Nov-2016) -- Start
                        'Enhancement - Add New PA Enhancement (Agreed Score,exc) Given by Akiba,KBC,TNP exc...
                        pCell.MakeMerge(dgvGE, dgvRow.Index, dgvRow.Cells(dgcolhaAgreedScoreGE.Index).ColumnIndex, dgvRow.Cells(dgcolhaAgreedScoreGE.Index).ColumnIndex, Color.White, Color.White, Nothing, "", picStayView.Image)

                        pCell.MakeMerge(dgvGE, dgvRow.Index, dgvRow.Cells(dgcolheremarkGE.Index).ColumnIndex, dgvRow.Cells(dgcolheremarkGE.Index).ColumnIndex, Color.White, Color.White, Nothing, "", picStayView.Image)
                        pCell.MakeMerge(dgvGE, dgvRow.Index, dgvRow.Cells(dgcolharemarkGE.Index).ColumnIndex, dgvRow.Cells(dgcolharemarkGE.Index).ColumnIndex, Color.White, Color.White, Nothing, "", picStayView.Image)
                        pCell.MakeMerge(dgvGE, dgvRow.Index, dgvRow.Cells(dgcolhrremarkGE.Index).ColumnIndex, dgvRow.Cells(dgcolhrremarkGE.Index).ColumnIndex, Color.White, Color.White, Nothing, "", picStayView.Image)
                        'Shani (23-Nov-2016) -- End
                    End If
                    dgvRow.ReadOnly = True
                ElseIf dgvRow.Cells(dgcolhGEWeight.Index).Value.ToString.Trim.Length <= 0 AndAlso CBool(dgvRow.Cells(objdgcolhIsGrpGE.Index).Value) = True Then
                    If ConfigParameter._Object._ScoringOptionId = enScoringOption.SC_WEIGHTED_BASED Then
                        pCell.MakeMerge(dgvGE, dgvRow.Index, dgvRow.Cells(dgcolhGEScore.Index).ColumnIndex, dgvRow.Cells.Count - 1, Color.Gray, Color.White, Nothing, "", picStayView.Image)
                    Else
                        pCell.MakeMerge(dgvGE, dgvRow.Index, 2, dgvRow.Cells.Count - 1, Color.Gray, Color.White, Nothing, "", picStayView.Image)
                    End If
                    dgvRow.ReadOnly = True
                ElseIf dgvRow.Cells(dgcolhGEWeight.Index).Value.ToString.Trim.Length > 0 AndAlso CBool(dgvRow.Cells(objdgcolhIsGrpGE.Index).Value) = True Then
                    pCell.MakeMerge(dgvGE, dgvRow.Index, dgvRow.Cells(dgcolhGEScore.Index).ColumnIndex, dgvRow.Cells.Count - 1, Color.Gray, Color.White, Nothing, "", picStayView.Image)
                    dgvRow.ReadOnly = True
                End If

                If CBool(dgvRow.Cells(objdgcolhIsGrpGE.Index).Value) = True Then
                    dgvRow.ReadOnly = True
                    If dgvRow.Cells(objdgcolhGECollapse.Index).Value Is Nothing Then
                        dgvRow.Cells(objdgcolhGECollapse.Index).Value = "-"
                        dgvRow.Cells(objdgcolhGECollapse.Index).Style.ForeColor = Color.White
                    End If
                    dgvRow.DefaultCellStyle.BackColor = Color.Gray
                    dgvRow.DefaultCellStyle.SelectionBackColor = Color.Gray
                    dgvRow.DefaultCellStyle.SelectionForeColor = Color.White
                    dgvRow.DefaultCellStyle.ForeColor = Color.White
                Else
                    dgvRow.Cells(objdgcolhGECollapse.Index).Value = ""
                End If

                If menAction = enAction.EDIT_ONE Then
                    Dim dtmp() As DataRow = Nothing
                    dtmp = mdtGE_Evaluation.Select("assessgroupunkid = '" & dtGE_TabularGrid.Rows(dgvRow.Index).Item("assessgroupunkid").ToString & "' AND competenciesunkid = '" & dtGE_TabularGrid.Rows(dgvRow.Index).Item("competenciesunkid").ToString & "' AND AUD <> 'D' ")
                    If dtmp.Length > 0 Then
                        If menAssess = enAssessmentMode.SELF_ASSESSMENT Then
                            If ConfigParameter._Object._ConsiderItemWeightAsNumber Then
                                dgvRow.Cells(objdgcolhedisplayGE.Index).Value = CDbl(dgvRow.Cells(dgcolhGEWeight.Index).Value) * CInt(dtmp(0).Item("result"))
                            Else
                                dgvRow.Cells(objdgcolhedisplayGE.Index).Value = 1 * CInt(dtmp(0).Item("result"))
                                dtmp(0).Item("dresult") = CInt(dtmp(0).Item("result"))
                            End If
                        ElseIf menAssess = enAssessmentMode.APPRAISER_ASSESSMENT Then
                            'S.SANDEEP [29 OCT 2015] -- START
                            'ISSUE : ASSESSOR NOT ABLE TO SEE HIS EMPLOYEE IF DEPARTMENT IS DIFFERENT AS OF USER ACCESS VOLTAMP
                            'If ConfigParameter._Object._ConsiderItemWeightAsNumber Then
                            '    dgvRow.Cells(objdgcolhedisplayGE.Index).Value = CDbl(dgvRow.Cells(dgcolhGEWeight.Index).Value) * CInt(dgvRow.Cells(dgcolheselfGE.Index).Value)
                            'Else
                            '    dgvRow.Cells(objdgcolhedisplayGE.Index).Value = 1 * CInt(dgvRow.Cells(dgcolheselfGE.Index).Value)
                            '    dtmp(0).Item("dresult") = CInt(dtmp(0).Item("result"))
                            'End If

                            If ConfigParameter._Object._ConsiderItemWeightAsNumber Then
                                dgvRow.Cells(objdgcolhedisplayGE.Index).Value = CDbl(dgvRow.Cells(dgcolhGEWeight.Index).Value) * CInt(IIf(dgvRow.Cells(dgcolheselfGE.Index).Value.ToString = "", 0, dgvRow.Cells(dgcolheselfGE.Index).Value))
                            Else
                                dgvRow.Cells(objdgcolhedisplayGE.Index).Value = 1 * CInt(IIf(dgvRow.Cells(dgcolheselfGE.Index).Value.ToString = "", 0, dgvRow.Cells(dgcolheselfGE.Index).Value))
                                dtmp(0).Item("dresult") = CInt(dtmp(0).Item("result"))
                            End If
                            'S.SANDEEP [29 OCT 2015] -- END


                            If ConfigParameter._Object._ConsiderItemWeightAsNumber Then
                                dgvRow.Cells(objdgcolhadisplayGE.Index).Value = CDbl(dgvRow.Cells(dgcolhGEWeight.Index).Value) * CInt(dtmp(0).Item("result"))
                            Else
                                dgvRow.Cells(objdgcolhadisplayGE.Index).Value = 1 * CInt(dtmp(0).Item("result"))
                                dtmp(0).Item("dresult") = CInt(dtmp(0).Item("result"))
                            End If
                        ElseIf menAssess = enAssessmentMode.REVIEWER_ASSESSMENT Then

                            'S.SANDEEP [04 JUN 2015] -- START
                            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                            'If ConfigParameter._Object._ConsiderItemWeightAsNumber Then
                            '    dgvRow.Cells(objdgcolhedisplayGE.Index).Value = CDbl(dgvRow.Cells(dgcolhGEWeight.Index).Value) * CInt(dgvRow.Cells(dgcolheselfGE.Index).Value)
                            'Else
                            '    dgvRow.Cells(objdgcolhedisplayGE.Index).Value = 1 * CInt(dgvRow.Cells(dgcolheselfGE.Index).Value)
                            '    dtmp(0).Item("dresult") = CInt(dtmp(0).Item("result"))
                            'End If

                            'If ConfigParameter._Object._ConsiderItemWeightAsNumber Then
                            '    dgvRow.Cells(objdgcolhadisplayGE.Index).Value = CDbl(dgvRow.Cells(dgcolhGEWeight.Index).Value) * CInt(dgvRow.Cells(dgcolhaselfGE.Index).Value)
                            'Else
                            '    dgvRow.Cells(objdgcolhadisplayGE.Index).Value = 1 * CInt(dgvRow.Cells(dgcolhaselfGE.Index).Value)
                            '    dtmp(0).Item("dresult") = CInt(dtmp(0).Item("result"))
                            'End If

                            If ConfigParameter._Object._ConsiderItemWeightAsNumber Then
                                dgvRow.Cells(objdgcolhedisplayGE.Index).Value = CDbl(dgvRow.Cells(dgcolhGEWeight.Index).Value) * CInt(IIf(dgvRow.Cells(dgcolheselfGE.Index).Value.ToString = "", 0, dgvRow.Cells(dgcolheselfGE.Index).Value))
                            Else
                                dgvRow.Cells(objdgcolhedisplayGE.Index).Value = 1 * CInt(IIf(dgvRow.Cells(dgcolheselfGE.Index).Value.ToString = "", 0, dgvRow.Cells(dgcolheselfGE.Index).Value))
                                dtmp(0).Item("dresult") = CInt(dtmp(0).Item("result"))
                            End If

                            If ConfigParameter._Object._ConsiderItemWeightAsNumber Then
                                dgvRow.Cells(objdgcolhadisplayGE.Index).Value = CDbl(dgvRow.Cells(dgcolhGEWeight.Index).Value) * CInt(IIf(dgvRow.Cells(dgcolhaselfGE.Index).Value.ToString = "", 0, dgvRow.Cells(dgcolhaselfGE.Index).Value))
                            Else
                                dgvRow.Cells(objdgcolhadisplayGE.Index).Value = 1 * CInt(IIf(dgvRow.Cells(dgcolhaselfGE.Index).Value.ToString = "", 0, dgvRow.Cells(dgcolhaselfGE.Index).Value))
                                dtmp(0).Item("dresult") = CInt(dtmp(0).Item("result"))
                            End If
                            'S.SANDEEP [04 JUN 2015] -- END


                            If ConfigParameter._Object._ConsiderItemWeightAsNumber Then
                                dgvRow.Cells(objdgcolhrdisplayGE.Index).Value = CDbl(dgvRow.Cells(dgcolhGEWeight.Index).Value) * CInt(dtmp(0).Item("result"))
                            Else
                                dgvRow.Cells(objdgcolhrdisplayGE.Index).Value = 1 * CInt(dtmp(0).Item("result"))
                                dtmp(0).Item("dresult") = CInt(dtmp(0).Item("result"))
                            End If
                        End If
                    End If
                End If
            Next

            'S.SANDEEP [23 APR 2015] -- START
            'AddHandler dgvGE.CellValueChanged, AddressOf dgvGE_CellValueChanged
            'S.SANDEEP [23 APR 2015] -- END

            AddHandler dgvGE.CellEnter, AddressOf dgvGE_CellEnter

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Set_Grid_Style_Data_GE", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub Evaluated_Data_GE(ByVal iResult As Decimal, ByVal iRemark As String, Optional ByVal isRemarkChanged As Boolean = False, Optional ByVal iAgreedScore As Decimal = 0)
        'Shani (23-Nov-2016) -- [iAgreedScore]

        Dim dRow As DataRow = Nothing
        Dim dTemp() As DataRow = Nothing
        Try
            Dim iDecWeight As Decimal = 0
            Decimal.TryParse(CStr(dgvGE.CurrentRow.Cells(dgcolhGEWeight.Index).Value), iDecWeight)
            If iDecWeight <= 0 Then iDecWeight = 1
            If ConfigParameter._Object._ConsiderItemWeightAsNumber = False Then iDecWeight = 1

            'Dim dtTemp() As DataRow = mdtGE_Evaluation.Select("assessgroupunkid = '" & CInt(dgvGE.CurrentRow.Cells(objdgcolhassessgroupunkidGE.Index).Value) & "' AND competenciesunkid = '" & _
            '                                                  CInt(dgvGE.CurrentRow.Cells(objdgcolhcompetenciesunkidGE.Index).Value) & "' AND AUD <> 'D' ")

            Dim dtTemp() As DataRow = Nothing
            dtTemp = Get_Old_Data_GE()

            'S.SANDEEP [21 JAN 2015] -- START
            ''S.SANDEEP [ 01 JAN 2015 ] -- START
            'Dim xComputedScore As Decimal = 0
            'Dim xFieldId As Integer = -1 : Dim xFormula As String = String.Empty
            'Dim xFormulaModes As enAssess_Computation_Formulas

            'Select Case menAssess
            '    Case enAssessmentMode.SELF_ASSESSMENT
            '        xFormulaModes = enAssess_Computation_Formulas.CMP_EMP_TOTAL_SCORE
            '    Case enAssessmentMode.APPRAISER_ASSESSMENT
            '        xFormulaModes = enAssess_Computation_Formulas.CMP_ASR_TOTAL_SCORE
            '    Case enAssessmentMode.REVIEWER_ASSESSMENT
            '        xFormulaModes = enAssess_Computation_Formulas.CMP_REV_TOTAL_SCORE
            'End Select
            ''S.SANDEEP [ 01 JAN 2015 ] -- END
            'S.SANDEEP [21 JAN 2015] -- END



            If dtTemp IsNot Nothing AndAlso dtTemp.Length <= 0 Then
                dRow = mdtGE_Evaluation.NewRow
                dRow.Item("analysistranunkid") = -1
                dRow.Item("analysisunkid") = mintAssessAnalysisUnkid
                dRow.Item("assessgroupunkid") = CInt(dgvGE.CurrentRow.Cells(objdgcolhassessgroupunkidGE.Index).Value)
                dRow.Item("competenciesunkid") = CInt(dgvGE.CurrentRow.Cells(objdgcolhcompetenciesunkidGE.Index).Value)
                dRow.Item("result") = iResult
                dRow.Item("remark") = iRemark
                dRow.Item("AUD") = "A"
                dRow.Item("GUID") = Guid.NewGuid.ToString
                dRow.Item("isvoid") = False
                dRow.Item("voiduserunkid") = -1
                dRow.Item("voiddatetime") = DBNull.Value
                dRow.Item("voidreason") = ""
                dRow.Item("dresult") = iResult * iDecWeight

                'S.SANDEEP [21 JAN 2015] -- START
                ''S.SANDEEP [ 01 JAN 2015 ] -- START
                'xFieldId = dRow.Item("competenciesunkid")
                'xComputedScore = objCmpMaster.Process_Assessment_Formula(xFormulaModes, _
                '                                                         CInt(cboPeriod.SelectedValue), _
                '                                                         xFieldId, _
                '                                                         CInt(cboEmployee.SelectedValue), _
                '                                                         ConfigParameter._Object._ScoringOptionId, _
                '                                                         iResult, _
                '                                                         menAssess, _
                '                                                         xFormula, dRow.Item("assessgroupunkid"))
                'dRow.Item("computed_value") = xComputedScore
                'dRow.Item("formula_used") = xFormula
                ''S.SANDEEP [ 01 JAN 2015 ] -- END
                If isRemarkChanged = False Then
                    dRow.Item("item_weight") = mdecItemWeight
                    dRow.Item("max_scale") = mdecMaxScale
                End If

                dRow.Item("iPeriodId") = cboPeriod.SelectedValue
                dRow.Item("iEmployeeId") = cboEmployee.SelectedValue
                dRow.Item("iScore") = iResult
                dRow.Item("iItemUnkid") = dRow.Item("competenciesunkid")
                'S.SANDEEP [21 JAN 2015] -- END

                'Shani (23-Nov-2016) -- Start
                'Enhancement - Add New PA Enhancement (Agreed Score,exc) Given by Akiba,KBC,TNP exc...
                dRow.Item("agreed_score") = iAgreedScore
                'Shani (23-Nov-2016) -- End

                mdtGE_Evaluation.Rows.Add(dRow)

                'S.SANDEEP [21 JAN 2015] -- START
                mdecItemWeight = 0 : mdecMaxScale = 0
                'S.SANDEEP [21 JAN 2015] -- END

            Else
                dtTemp(0).Item("analysistranunkid") = dtTemp(0).Item("analysistranunkid")
                dtTemp(0).Item("analysisunkid") = mintAssessAnalysisUnkid
                dtTemp(0).Item("assessgroupunkid") = CInt(dgvGE.CurrentRow.Cells(objdgcolhassessgroupunkidGE.Index).Value)
                dtTemp(0).Item("competenciesunkid") = CInt(dgvGE.CurrentRow.Cells(objdgcolhcompetenciesunkidGE.Index).Value)
                dtTemp(0).Item("result") = iResult
                dtTemp(0).Item("remark") = iRemark
                If IsDBNull(dtTemp(0).Item("AUD")) Or CStr(dtTemp(0).Item("AUD")).ToString.Trim = "" Then
                    dtTemp(0).Item("AUD") = "U"
                End If
                dtTemp(0).Item("GUID") = Guid.NewGuid.ToString
                dtTemp(0).Item("isvoid") = False
                dtTemp(0).Item("voiduserunkid") = -1
                dtTemp(0).Item("voiddatetime") = DBNull.Value
                dtTemp(0).Item("voidreason") = ""
                dtTemp(0).Item("dresult") = iResult * iDecWeight

                'S.SANDEEP [21 JAN 2015] -- START
                ''S.SANDEEP [ 01 JAN 2015 ] -- START
                'xFieldId = dtTemp(0).Item("competenciesunkid")
                'xComputedScore = objCmpMaster.Process_Assessment_Formula(xFormulaModes, _
                '                                                         CInt(cboPeriod.SelectedValue), _
                '                                                         xFieldId, _
                '                                                         CInt(cboEmployee.SelectedValue), _
                '                                                         ConfigParameter._Object._ScoringOptionId, _
                '                                                         iResult, _
                '                                                         menAssess, _
                '                                                         xFormula, dtTemp(0).Item("assessgroupunkid"))
                'dtTemp(0).Item("computed_value") = xComputedScore
                'dtTemp(0).Item("formula_used") = xFormula
                ''S.SANDEEP [ 01 JAN 2015 ] -- END

                If isRemarkChanged = False Then
                    dtTemp(0).Item("item_weight") = mdecItemWeight
                    dtTemp(0).Item("max_scale") = mdecMaxScale
                End If

                dtTemp(0).Item("iPeriodId") = cboPeriod.SelectedValue
                dtTemp(0).Item("iEmployeeId") = cboEmployee.SelectedValue
                dtTemp(0).Item("iScore") = iResult
                dtTemp(0).Item("iItemUnkid") = dtTemp(0).Item("competenciesunkid")
                'S.SANDEEP [21 JAN 2015] -- END

                'Shani (23-Nov-2016) -- Start
                'Enhancement - Add New PA Enhancement (Agreed Score,exc) Given by Akiba,KBC,TNP exc...
                dtTemp(0).Item("agreed_score") = iAgreedScore
                'Shani (23-Nov-2016) -- End

                dtTemp(0).AcceptChanges()

                'S.SANDEEP [21 JAN 2015] -- START
                mdecItemWeight = 0 : mdecMaxScale = 0
                'S.SANDEEP [21 JAN 2015] -- END

            End If

            'S.SANDEEP [ 20 DEC 2014 ] -- START
            'If mdtGE_Evaluation IsNot Nothing AndAlso mdtGE_Evaluation.Rows.Count > 0 Then
            '    Dim objAGrp As New clsassess_group_master
            '    objAGrp._Assessgroupunkid = CInt(dgvGE.CurrentRow.Cells(objdgcolhassessgroupunkidGE.Index).Value)
            '    If CDbl(mdtGE_Evaluation.Compute("SUM(result)", "assessgroupunkid = '" & CInt(dgvGE.CurrentRow.Cells(objdgcolhassessgroupunkidGE.Index).Value) & "' AND AUD <> 'D' ")) > objAGrp._Weight Then
            '        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 27, "Sorry you cannot go beyond the total weight assigned for the particular assessment group weight."), enMsgBoxStyle.Information)
            '        dtTemp = mdtGE_Evaluation.Select("assessgroupunkid = '" & CInt(dgvGE.CurrentRow.Cells(objdgcolhassessgroupunkidGE.Index).Value) & "' AND competenciesunkid = '" & _
            '                                         CInt(dgvGE.CurrentRow.Cells(objdgcolhcompetenciesunkidGE.Index).Value) & "' AND AUD <> 'D' ")

            '        If menAssess = enAssessmentMode.SELF_ASSESSMENT Then
            '            dgvGE.CurrentRow.Cells(dgcolheselfGE.Index).Value = 0
            '        ElseIf menAssess = enAssessmentMode.APPRAISER_ASSESSMENT Then
            '            dgvGE.CurrentRow.Cells(dgcolhaselfGE.Index).Value = 0
            '        ElseIf menAssess = enAssessmentMode.REVIEWER_ASSESSMENT Then
            '            dgvGE.CurrentRow.Cells(dgcolhrselfGE.Index).Value = 0
            '        End If
            '        If dtTemp.Length > 0 Then
            '            dtTemp(0).Item("result") = 0
            '        End If
            '        Exit Sub
            '    End If
            '    objAGrp = Nothing
            'End If
            If ConfigParameter._Object._ScoringOptionId = enScoringOption.SC_WEIGHTED_BASED Then
                If mdtGE_Evaluation IsNot Nothing AndAlso mdtGE_Evaluation.Rows.Count > 0 Then
                    Dim objAGrp As New clsassess_group_master
                    objAGrp._Assessgroupunkid = CInt(dgvGE.CurrentRow.Cells(objdgcolhassessgroupunkidGE.Index).Value)
                    If CDbl(mdtGE_Evaluation.Compute("SUM(result)", "assessgroupunkid = '" & CInt(dgvGE.CurrentRow.Cells(objdgcolhassessgroupunkidGE.Index).Value) & "' AND AUD <> 'D' ")) > objAGrp._Weight Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 27, "Sorry you cannot go beyond the total weight assigned for the particular assessment group weight."), enMsgBoxStyle.Information)
                        dtTemp = mdtGE_Evaluation.Select("assessgroupunkid = '" & CInt(dgvGE.CurrentRow.Cells(objdgcolhassessgroupunkidGE.Index).Value) & "' AND competenciesunkid = '" & _
                                                         CInt(dgvGE.CurrentRow.Cells(objdgcolhcompetenciesunkidGE.Index).Value) & "' AND AUD <> 'D' ")
                        If menAssess = enAssessmentMode.SELF_ASSESSMENT Then
                            dgvGE.CurrentRow.Cells(dgcolheselfGE.Index).Value = 0
                        ElseIf menAssess = enAssessmentMode.APPRAISER_ASSESSMENT Then
                            dgvGE.CurrentRow.Cells(dgcolhaselfGE.Index).Value = 0
                        ElseIf menAssess = enAssessmentMode.REVIEWER_ASSESSMENT Then
                            dgvGE.CurrentRow.Cells(dgcolhrselfGE.Index).Value = 0
                        End If
                        If dtTemp.Length > 0 Then
                            dtTemp(0).Item("result") = 0
                        End If
                        Exit Sub
                    End If
                    objAGrp = Nothing
                End If
            End If
            'S.SANDEEP [ 20 DEC 2014 ] -- END

            If ConfigParameter._Object._ConsiderItemWeightAsNumber Then
                If iDecWeight > 0 Then
                    Select Case menAssess
                        Case enAssessmentMode.SELF_ASSESSMENT
                            dgvGE.CurrentRow.Cells(objdgcolhedisplayGE.Index).Value = iDecWeight * iResult
                        Case enAssessmentMode.APPRAISER_ASSESSMENT
                            dgvGE.CurrentRow.Cells(objdgcolhadisplayGE.Index).Value = iDecWeight * iResult
                        Case enAssessmentMode.REVIEWER_ASSESSMENT
                            dgvGE.CurrentRow.Cells(objdgcolhrdisplayGE.Index).Value = iDecWeight * iResult
                    End Select
                End If
            End If

            Call SetTotals(dgvGE)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Evaluated_Data_GE", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Function isAll_Assessed_GE(Optional ByVal blnFlag As Boolean = True) As Boolean
        Try
            If mdtGE_Evaluation.Rows.Count <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 23, "Sorry, you cannot do Save Commit Operation as you have not assessed all item(s) in some assessment group."), enMsgBoxStyle.Information)
                Return False
            End If
            Dim dtmp1() As DataRow = Nothing
            Dim dtmp2() As DataRow = Nothing
            Dim dView As DataView = dtGE_TabularGrid.DefaultView
            Dim dMView As DataView = mdtGE_Evaluation.DefaultView
            dtmp1 = dView.ToTable(True, "assessgroupunkid").Select("assessgroupunkid > 0")
            dtmp2 = dMView.ToTable(True, "assessgroupunkid").Select("assessgroupunkid > 0")
            If dtmp1.Length <> dtmp2.Length Then
                If blnFlag = True Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 24, "Sorry, you cannot do Save Commit Operation as you have not assessed all assessment group."), enMsgBoxStyle.Information)
                    Return False
                End If
            End If
            dtmp1 = dView.ToTable.Select("assessgroupunkid > 0 AND IsGrp = False AND IsPGrp = False")
            'dtmp2 = dMView.ToTable.Select("assessgroupunkid > 0 AND result <> 0 ")
            dtmp2 = dMView.ToTable.Select("assessgroupunkid > 0")
            If dtmp1.Length <> dtmp2.Length Then
                If blnFlag = True Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 25, "Sorry, you cannot do Save Commit Operation as you have not assessed all item in assessment group."), enMsgBoxStyle.Information)
                    Return False
                    'Shani(30-MAR-2016) -- Start
                Else
                    Return False
                    'Shani(30-MAR-2016) -- End
                End If
            End If
            Return True
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "isAll_Assessed_GE", mstrModuleName)
        Finally
        End Try
    End Function

    Private Function Get_Old_Data_GE() As DataRow()
        Dim xTemp() As DataRow = Nothing
        Try
            If mdtGE_Evaluation IsNot Nothing Then
                xTemp = mdtGE_Evaluation.Select("assessgroupunkid = '" & CInt(dgvGE.CurrentRow.Cells(objdgcolhassessgroupunkidGE.Index).Value) & "' AND competenciesunkid = '" & _
                                                              CInt(dgvGE.CurrentRow.Cells(objdgcolhcompetenciesunkidGE.Index).Value) & "' AND AUD <> 'D' ")
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "", mstrModuleName)
        Finally
        End Try
        Return xTemp
    End Function

    '--------------------------------------------- CUSTOM ITEMS EVALUATION METHODS
    Private Sub Fill_Custom_Grid(Optional ByVal iFromAddEdit As Boolean = False)
        Try
            If iHeaderId >= 0 Then
                If iFromAddEdit = False Then
                    'Shani (26-Sep-2016) -- Start
                    'Enhancement -Changes PA Flow And Add New screen Given By (Andrew)
                    'dtCustomTabularGrid = objEAnalysisMst.Get_Custom_Items_List(CInt(cboPeriod.SelectedValue), CInt(dsHeaders.Tables(0).Rows(iHeaderId).Item("Id")), mintAssessAnalysisUnkid, menAssess, CInt(cboEmployee.SelectedValue), menAction)
                    dtCustomTabularGrid = objEAnalysisMst.Get_Custom_Items_List(CInt(cboPeriod.SelectedValue), CInt(dsHeaders.Tables(0).Rows(iHeaderId).Item("Id")), mintAssessAnalysisUnkid, menAssess, CInt(cboEmployee.SelectedValue), menAction, ConfigParameter._Object._IsCompanyNeedReviewer)
                    'Shani (26-Sep-2016) -- End
                Else
                    Dim iRow() As DataRow = dtCustomTabularGrid.Select("periodunkid <= 0")
                    For i As Integer = 0 To iRow.Length - 1
                        dtCustomTabularGrid.Rows.Remove(iRow(i))
                    Next
                    RemoveHandler dgvItems.DataBindingComplete, AddressOf dgvItems_DataBindingComplete
                    dtCustomTabularGrid.AcceptChanges()
                    AddHandler dgvItems.DataBindingComplete, AddressOf dgvItems_DataBindingComplete
                End If
                If dtCustomTabularGrid.Rows.Count > 0 Then
                    Dim query = From Cols In dgvItems.Columns Where Cols.Index > 2 Select Cols
                    If query.ToList().Count > 0 Then
                        For Each idx As DataGridViewColumn In query.ToList()
                            dgvItems.Columns.Remove(idx)
                        Next
                    End If

                    dgvItems.DataSource = Nothing
                    dgvItems.AutoGenerateColumns = False
                    Dim iColName As String = String.Empty
                    For Each dCol As DataColumn In dtCustomTabularGrid.Columns
                        iColName = "" : iColName = "obj" & dCol.ColumnName
                        If dgvItems.Columns.Contains(iColName) = True Then Continue For
                        Dim dgvCol As New DataGridViewTextBoxColumn()
                        dgvCol.Name = iColName
                        'dgvCol.Width = 200
                        dgvCol.SortMode = DataGridViewColumnSortMode.NotSortable
                        'S.SANDEEP [07 AUG 2015] -- START
                        'dgvCol.AutoSizeMode = DataGridViewAutoSizeColumnMode.ColumnHeader
                        dgvCol.Resizable = DataGridViewTriState.True
                        'S.SANDEEP [07 AUG 2015] -- END
                        dgvCol.ReadOnly = True
                        dgvCol.DataPropertyName = dCol.ColumnName
                        dgvCol.HeaderText = dCol.Caption
                        If dCol.Caption.Length <= 0 Then
                            dgvCol.Visible = False
                        End If
                        dgvItems.Columns.Add(dgvCol)
                        If dgvCol.Name = "objHeader_Name" Then dgvCol.Visible = False
                    Next
                    dgvItems.DataSource = dtCustomTabularGrid

                    objdgcolhAdd.DefaultCellStyle.SelectionBackColor = Color.White
                    objdgcolhAdd.DefaultCellStyle.SelectionForeColor = Color.White

                    objdgcolhEdit.DefaultCellStyle.SelectionBackColor = Color.White
                    objdgcolhEdit.DefaultCellStyle.SelectionForeColor = Color.White

                    objdgcolhDelete.DefaultCellStyle.SelectionBackColor = Color.White
                    objdgcolhDelete.DefaultCellStyle.SelectionForeColor = Color.White
                End If

            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Fill_Custom_Grid", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub Fill_Custom_Evaluation_Data()
        Try
            If mdtCustomEvaluation IsNot Nothing AndAlso mdtCustomEvaluation.Rows.Count > 0 Then
                RemoveHandler dgvItems.DataBindingComplete, AddressOf dgvItems_DataBindingComplete

                'Shani (26-Sep-2016) -- Start
                'Enhancement -Changes PA Flow And Add New screen Given By (Andrew)
                ''Dim dtEval As DataTable
                ''dtEval = New DataView(mdtCustomEvaluation, "custom_header = '" & dsHeaders.Tables(0).Rows(iHeaderId).Item("Name") & "' AND AUD <> 'D'", "", DataViewRowState.CurrentRows).ToTable
                'Dim dtEval As DataView
                'dtEval = New DataView(mdtCustomEvaluation, "custom_header = '" & dsHeaders.Tables(0).Rows(iHeaderId).Item("Name") & "' AND AUD <> 'D'", "customanalysistranguid", DataViewRowState.CurrentRows)

                'dtCustomTabularGrid.Rows.Clear()
                'Dim dFRow As DataRow = Nothing
                'Dim iCustomId, iNewCustomId As Integer : iCustomId = 0 : iNewCustomId = 0
                'For Each dRow As DataRowView In dtEval
                '    If dRow.Item("AUD") = "D" Then Continue For
                '    If iCustomId <= 0 Then iCustomId = dRow.Item("customitemunkid")
                '    iNewCustomId = dRow.Item("customitemunkid")
                '    If iCustomId = iNewCustomId Then
                '        dFRow = dtCustomTabularGrid.NewRow
                '        dtCustomTabularGrid.Rows.Add(dFRow)
                '        'Shani (26-Sep-2016) -- Start
                '        'Enhancement -Changes PA Flow And Add New screen Given By (Andrew)
                '        If menAction <> enAction.EDIT_ONE Then
                '            'dFRow.Item("GUID") = dRow.Item("customanalysistranguid")
                '            dFRow.Item("GUID") = System.Guid.NewGuid().ToString
                '        End If
                '        'Shani (26-Sep-2016) -- End
                '    End If
                '    Select Case CInt(dRow.Item("itemtypeid"))
                '        Case clsassess_custom_items.enCustomType.FREE_TEXT
                '            dFRow.Item("CItem_" & dRow.Item("customitemunkid").ToString) = dRow.Item("custom_value")
                '        Case clsassess_custom_items.enCustomType.SELECTION
                '            If dRow.Item("custom_value").ToString.Trim.Length > 0 Then
                '                Select Case CInt(dRow.Item("selectionmodeid"))
                '                    'S.SANDEEP [04 OCT 2016] -- START
                '                    'Case clsassess_custom_items.enSelectionMode.TRAINING_OBJECTIVE
                '                    Case clsassess_custom_items.enSelectionMode.TRAINING_OBJECTIVE, clsassess_custom_items.enSelectionMode.JOB_CAPABILITIES_COURSES, clsassess_custom_items.enSelectionMode.CAREER_DEVELOPMENT_COURSES
                '                        'S.SANDEEP [04 OCT 2016] -- END
                '                        Dim objCMaster As New clsCommon_Master
                '                        objCMaster._Masterunkid = CInt(dRow.Item("custom_value"))
                '                        dFRow.Item("CItem_" & dRow.Item("customitemunkid").ToString) = objCMaster._Name
                '                        objCMaster = Nothing
                '                    Case clsassess_custom_items.enSelectionMode.EMPLOYEE_COMPETENCIES
                '                        Dim objCMaster As New clsassess_competencies_master
                '                        objCMaster._Competenciesunkid = CInt(dRow.Item("custom_value"))
                '                        dFRow.Item("CItem_" & dRow.Item("customitemunkid").ToString) = objCMaster._Name
                '                        objCMaster = Nothing
                '                    Case clsassess_custom_items.enSelectionMode.EMPLOYEE_GOALS
                '                        Dim objEmpField1 As New clsassess_empfield1_master
                '                        objEmpField1._Empfield1unkid = CInt(dRow.Item("custom_value"))
                '                        dFRow.Item("CItem_" & dRow.Item("customitemunkid").ToString) = objEmpField1._Field_Data
                '                        objEmpField1 = Nothing
                '                End Select
                '            End If
                '        Case clsassess_custom_items.enCustomType.DATE_SELECTION
                '            If dRow.Item("custom_value").ToString.Trim.Length > 0 Then
                '                dFRow.Item("CItem_" & dRow.Item("customitemunkid").ToString) = eZeeDate.convertDate(dRow.Item("custom_value").ToString).ToShortDateString
                '            End If
                '        Case clsassess_custom_items.enCustomType.NUMERIC_DATA
                '            If dRow.Item("custom_value").ToString.Trim.Length > 0 Then
                '                If IsNumeric(dRow.Item("custom_value")) Then
                '                    dFRow.Item("CItem_" & dRow.Item("customitemunkid").ToString) = CDbl(dRow.Item("custom_value")).ToString
                '                End If
                '            End If
                '    End Select
                '    dFRow.Item("CItem_" & dRow.Item("customitemunkid").ToString & "Id") = dRow.Item("customitemunkid")
                '    dFRow.Item("periodunkid") = dRow.Item("periodunkid")
                '    dFRow.Item("itemtypeid") = dRow.Item("itemtypeid")
                '    dFRow.Item("selectionmodeid") = dRow.Item("selectionmodeid")

                '    'Shani (26-Sep-2016) -- Start
                '    'Enhancement -Changes PA Flow And Add New screen Given By (Andrew)
                '    'dFRow.Item("GUID") = dRow.Item("customanalysistranguid")
                '    If menAction <> enAction.EDIT_ONE Then
                '        dRow.Item("customanalysistranguid") = dFRow.Item("GUID")
                '    Else
                '        dFRow.Item("GUID") = dRow.Item("customanalysistranguid")
                '    End If
                '    'Shani (26-Sep-2016) -- End
                '    dFRow.Item("analysisunkid") = dRow.Item("analysisunkid")
                'Next
                dtCustomTabularGrid.Rows.Clear()
                Dim strGUIDArray() As String = Nothing

                Dim dR = mdtCustomEvaluation.AsEnumerable().Where(Function(x) x.Field(Of String)("custom_header") = dsHeaders.Tables(0).Rows(iHeaderId).Item("Name").ToString And x.Field(Of String)("AUD") <> "D")
                If dR.Count > 0 Then
                    strGUIDArray = dR.Cast(Of DataRow).Select(Function(x) x.Field(Of String)("customanalysistranguid")).Distinct().ToArray()
                End If

                If strGUIDArray IsNot Nothing AndAlso strGUIDArray.Length > 0 Then
                    Dim dFRow As DataRow = Nothing
                    For Each Str As String In strGUIDArray
                        dFRow = dtCustomTabularGrid.NewRow
                        dtCustomTabularGrid.Rows.Add(dFRow)
                        For Each dRow As DataRow In mdtCustomEvaluation.Select("customanalysistranguid = '" & Str & "' AND AUD <> 'D' ")
                            Select Case CInt(dRow.Item("itemtypeid"))
                                Case clsassess_custom_items.enCustomType.FREE_TEXT
                                    dFRow.Item("CItem_" & dRow.Item("customitemunkid").ToString) = dRow.Item("custom_value")
                                Case clsassess_custom_items.enCustomType.SELECTION
                                    If dRow.Item("custom_value").ToString.Trim.Length > 0 Then
                                        Select Case CInt(dRow.Item("selectionmodeid"))
                                            Case clsassess_custom_items.enSelectionMode.TRAINING_OBJECTIVE, clsassess_custom_items.enSelectionMode.JOB_CAPABILITIES_COURSES, clsassess_custom_items.enSelectionMode.CAREER_DEVELOPMENT_COURSES
                                                Dim objCMaster As New clsCommon_Master
                                                objCMaster._Masterunkid = CInt(dRow.Item("custom_value"))
                                                dFRow.Item("CItem_" & dRow.Item("customitemunkid").ToString) = objCMaster._Name
                                                objCMaster = Nothing
                                            Case clsassess_custom_items.enSelectionMode.EMPLOYEE_COMPETENCIES
                                                Dim objCMaster As New clsassess_competencies_master
                                                objCMaster._Competenciesunkid = CInt(dRow.Item("custom_value"))
                                                dFRow.Item("CItem_" & dRow.Item("customitemunkid").ToString) = objCMaster._Name
                                                objCMaster = Nothing
                                            Case clsassess_custom_items.enSelectionMode.EMPLOYEE_GOALS
                                                Dim objEmpField1 As New clsassess_empfield1_master
                                                objEmpField1._Empfield1unkid = CInt(dRow.Item("custom_value"))
                                                dFRow.Item("CItem_" & dRow.Item("customitemunkid").ToString) = objEmpField1._Field_Data
                                                objEmpField1 = Nothing

                                                'S.SANDEEP |16-AUG-2019| -- START
                                                'ISSUE/ENHANCEMENT : {ARUTI-877|Ref#0003997}
                                            Case clsassess_custom_items.enSelectionMode.PERFORMANCE_CUSTOM_ITEM
                                                Dim objCMaster As New clsCommon_Master
                                                objCMaster._Masterunkid = CInt(dRow.Item("custom_value"))
                                                dFRow.Item("CItem_" & dRow.Item("customitemunkid").ToString) = objCMaster._Name
                                                objCMaster = Nothing
                                                'S.SANDEEP |16-AUG-2019| -- END
                                        End Select
                                    End If
                                Case clsassess_custom_items.enCustomType.DATE_SELECTION
                                    If dRow.Item("custom_value").ToString.Trim.Length > 0 Then
                                        dFRow.Item("CItem_" & dRow.Item("customitemunkid").ToString) = eZeeDate.convertDate(dRow.Item("custom_value").ToString).ToShortDateString
                                    End If
                                Case clsassess_custom_items.enCustomType.NUMERIC_DATA
                                    If dRow.Item("custom_value").ToString.Trim.Length > 0 Then
                                        If IsNumeric(dRow.Item("custom_value")) Then
                                            dFRow.Item("CItem_" & dRow.Item("customitemunkid").ToString) = CDbl(dRow.Item("custom_value")).ToString
                                        End If
                                    End If
                            End Select
                            dFRow.Item("CItem_" & dRow.Item("customitemunkid").ToString & "Id") = dRow.Item("customitemunkid")
                            dFRow.Item("periodunkid") = dRow.Item("periodunkid")
                            dFRow.Item("itemtypeid") = dRow.Item("itemtypeid")
                            dFRow.Item("selectionmodeid") = dRow.Item("selectionmodeid")
                            dFRow.Item("GUID") = dRow.Item("customanalysistranguid")
                            dFRow.Item("analysisunkid") = dRow.Item("analysisunkid")
                            dFRow.Item("ismanual") = dRow.Item("ismanual")
                        Next
                    Next
                End If
                'Shani (26-Sep-2016) -- End

                mdtCustomEvaluation.AcceptChanges()
                If dtCustomTabularGrid.Rows.Count <= 0 Then
                    dtCustomTabularGrid.Rows.Add(dtCustomTabularGrid.NewRow)
                    dtCustomTabularGrid.Rows(0).Item("periodunkid") = CInt(cboPeriod.SelectedValue)
                End If
                AddHandler dgvItems.DataBindingComplete, AddressOf dgvItems_DataBindingComplete
                Call Fill_Custom_Grid(True)
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Fill_Custom_Evaluation_Data", mstrModuleName)
        Finally
        End Try
    End Sub


    'Shani(23-FEB-2017) -- Start
    'Enhancement - Add new custom item saving setting requested by (aga khan)
    Private Function isAll_Assessed_CustomItem(Optional ByVal blnFlag As Boolean = True) As Boolean
        Dim dsCustomItem As DataSet
        Try
            dsCustomItem = (New clsassess_custom_items).GetCutomItemList(CInt(cboPeriod.SelectedValue), menAssess)
            If dsCustomItem IsNot Nothing AndAlso dsCustomItem.Tables(0).Rows.Count > 0 Then
                If mdtCustomEvaluation.Rows.Count <= 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 42, "Sorry, you cannot do Save Commit Operation as you have not assessed all item(s) in some objective."), enMsgBoxStyle.Information)
                    Return False
                End If

                Dim ArrCustomItem = mdtCustomEvaluation.AsEnumerable().Where(Function(x) x.Field(Of String)("dispaly_value") <> "" AndAlso x.Field(Of Boolean)("isvoid") = False).Select(Function(x) x.Field(Of Integer)("customitemunkid")).Distinct.ToArray

                Dim ar = dsCustomItem.Tables(0).AsEnumerable().Where(Function(x) Not ArrCustomItem.Contains(x.Field(Of Integer)("customitemunkid")))

                If ar.Count > 0 Then
                    If blnFlag = True Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 43, "Sorry, you cannot do Save Commit Operation as you have not assessed all item(s) in some objective."), enMsgBoxStyle.Information)
                        Return False
                    Else
                        Return False
                    End If
                End If
            End If

            Return True
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "isAll_Assessed_BSC", mstrModuleName)
        Finally
        End Try
    End Function
    'Shani(23-FEB-2017) -- End


    'S.SANDEEP |08-JAN-2019| -- START
    Private Function LockEmployee() As Boolean
        Dim objLockEmp As New clsassess_plan_eval_lockunlock
        Try
            Dim mintLockId As Integer = 0
            Dim mblnisLock As Boolean = False
            Dim dtNextLockDate As Date = Nothing
            If objLockEmp.isExist(CInt(cboEmployee.SelectedValue), CInt(cboPeriod.SelectedValue), clsassess_plan_eval_lockunlock.enAssementLockType.ASSESSMENT, "", mblnisLock, mintLockId, dtNextLockDate) Then
                If mintLockId > 0 And mblnisLock = True Then Return True
            End If

            If objLockEmp.isValidDate(CInt(cboEmployee.SelectedValue), CInt(cboPeriod.SelectedValue), clsassess_plan_eval_lockunlock.enAssementLockType.ASSESSMENT, ConfigParameter._Object._CurrentDateAndTime) = True Then
                Return False
            End If

            objLockEmp._AssessorMasterunkid = 0
            objLockEmp._Assessmodeid = enAssessmentMode.SELF_ASSESSMENT
            objLockEmp._Auditdatetime = ConfigParameter._Object._CurrentDateAndTime
            objLockEmp._Audittype = enAuditType.ADD
            objLockEmp._Audituserunkid = User._Object._Userunkid
            objLockEmp._Employeeunkid = CInt(cboEmployee.SelectedValue)
            objLockEmp._Form_Name = mstrModuleName
            objLockEmp._Hostname = getHostName()
            objLockEmp._Ip = getIP()
            objLockEmp._Islock = True
            objLockEmp._Isunlocktenure = False
            objLockEmp._Isweb = False
            objLockEmp._Locktypeid = clsassess_plan_eval_lockunlock.enAssementLockType.ASSESSMENT
            objLockEmp._Lockunlockdatetime = ConfigParameter._Object._CurrentDateAndTime
            objLockEmp._Lockuserunkid = User._Object._Userunkid
            objLockEmp._Loginemployeeunkid = 0
            objLockEmp._Nextlockdatetime = Nothing
            objLockEmp._Periodunkid = CInt(cboPeriod.SelectedValue)
            objLockEmp._Unlockdays = 0
            objLockEmp._Unlockreason = ""
            objLockEmp._Unlockuserunkid = 0
            If objLockEmp.Insert() = False Then
                If objLockEmp._Message.Trim.Length > 0 Then
                    eZeeMsgBox.Show(objLockEmp._Message, enMsgBoxStyle.Information)
                End If
            End If
            Return True
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "LockEmployee", mstrModuleName)
        Finally
            objLockEmp = Nothing
        End Try
    End Function
    'S.SANDEEP |08-JAN-2019| -- END

#End Region

#Region " Form's Events "

    Private Sub frmPerformanceEvaluation_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        objEAnalysisMst = New clsevaluation_analysis_master
        objGoalsTran = New clsgoal_analysis_tran
        objCAssessTran = New clscompetency_analysis_tran
        objCCustomTran = New clscompeteny_customitem_tran
        objCmpMaster = New clsassess_computation_master
        Try
            Call Set_Logo(Me, gApplicationType)
            Language.setLanguage(Me.Name)
            Call OtherSettings()
            Call SetVisibility()
            Call SetColor()
            Call FillCombo()
            If menAction = enAction.EDIT_ONE Then
                objEAnalysisMst._Analysisunkid = mintAssessAnalysisUnkid
                cboEmployee.Enabled = False
                objbtnSearchEmployee.Enabled = False
                cboAssessor.Enabled = False
                objbtnSearchAssessor.Enabled = False
                cboPeriod.Enabled = False
                radExternalAssessor.Enabled = False
                radInternalAssessor.Enabled = False
                cboReviewer.Enabled = False
                objbtnSearchReviewer.Enabled = False
                objbtnSearch.Enabled = False
                objbtnReset.Enabled = False
            End If
            Call GetValue()
            objGoalsTran._AnalysisUnkid = mintAssessAnalysisUnkid
            mdtBSC_Evaluation = objGoalsTran._DataTable

            'S.SANDEEP [09 OCT 2015] -- START
            objCAssessTran._SelfAssignCompetencies = ConfigParameter._Object._Self_Assign_Competencies
            'S.SANDEEP [09 OCT 2015] -- END
            objCAssessTran._ConsiderWeightAsNumber = ConfigParameter._Object._ConsiderItemWeightAsNumber
            objCAssessTran._AnalysisUnkid = mintAssessAnalysisUnkid
            mdtGE_Evaluation = objCAssessTran._DataTable

            objCCustomTran._AnalysisUnkid = mintAssessAnalysisUnkid
            'S.SANDEEP [29 DEC 2015] -- START
            'objCCustomTran._EmployeeId = CInt(cboEmployee.SelectedValue)

            'Shani (26-Sep-2016) -- Start
            'Enhancement -Changes PA Flow And Add New screen Given By (Andrew)
            'objCCustomTran._AllowCustomItemInPlanning = ConfigParameter._Object._IncludeCustomItemInPlanning
            objCCustomTran._IsCompanyNeedReviewer = ConfigParameter._Object._IsCompanyNeedReviewer
            'Shani (26-Sep-2016) -- End

            objCCustomTran._EmployeeId = CInt(cboEmployee.SelectedValue)
            objCCustomTran._PeriodId = CInt(cboPeriod.SelectedValue)
            'S.SANDEEP [29 DEC 2015] -- END
            mdtCustomEvaluation = objCCustomTran._DataTable

            If menAction = enAction.EDIT_ONE Then
                Call Fill_BSC_Evaluation()
                Call Fill_GE_Evaluation()
                Call Fill_Custom_Grid()
                'Call Fill_Custom_Evaluation_Data()
                Call objbtnSearch_Click(sender, e)
            End If
            mblIsFormLoad = True
            'S.SANDEEP |31-MAY-2019| -- START
            'ISSUE/ENHANCEMENT : [PA SETTING CHANGES]
            If ConfigParameter._Object._ReviewerScoreSetting = enReviewerScoreSetting.ReviewerScoreNotNecessary Then
                lnkCopyScore.Visible = False
            End If
            'S.SANDEEP |31-MAY-2019| -- END
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmPerformanceEvaluation_Load", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub frmPerformanceEvaluation_KeyDown(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles MyBase.KeyDown
        Try
            If e.Control = True And e.KeyCode = Keys.S Then
                'S.SANDEEP [ 01 JAN 2015 ] -- START
                'btnSave_Click(sender, e)
                btnSave_Click(btnSave, e)
                'S.SANDEEP [ 01 JAN 2015 ] -- END
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmPerformanceEvaluation_KeyDown", mstrModuleName)
        End Try
    End Sub

    Private Sub frmPerformanceEvaluation_KeyPress(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles MyBase.KeyPress
        Try
            If Asc(e.KeyChar) = 13 Then
                SendKeys.Send("{TAB}")
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmPerformanceEvaluation_KeyPress", mstrModuleName)
        End Try
    End Sub

    Private Sub frmPerformanceEvaluation_FormClosed(ByVal sender As System.Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles MyBase.FormClosed
        objEAnalysisMst = Nothing
    End Sub

    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsevaluation_analysis_master.SetMessages()
            objfrm._Other_ModuleNames = "clsevaluation_analysis_master"
            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub

#End Region

#Region " Button's Events "

    'S.SANDEEP [25-JAN-2017] -- START
    'ISSUE/ENHANCEMENT :
    'Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click, btnSaveCommit.Click
    '    Dim blnFlag As Boolean = False
    '    Dim blnIsBSC_Set As Boolean = False
    '    Dim blnIsGE_Set As Boolean = False
    '    Try
    '        'S.SANDEEP [23 MAR 2015] -- START
    '        If Validation() = False Then Exit Sub
    '        'S.SANDEEP [23 MAR 2015] -- END

    '        If dsHeaders.Tables.Count <= 0 Then Exit Sub
    '        If dsHeaders.Tables(0).Rows.Count > 0 Then
    '            'S.SANDEEP [23 MAR 2015] -- START
    '            'If Validation() = False Then Exit Sub
    '            'S.SANDEEP [23 MAR 2015] -- END
    '            Dim dtmp() As DataRow = Nothing

    '            'S.SANDEEP [23 MAR 2015] -- START
    '            Dim blnIsOneAssessed As Boolean = False
    '            If mdtBSC_Evaluation IsNot Nothing Then
    '                dtmp = dsHeaders.Tables(0).Select("Id=-3")
    '                If dtmp.Length > 0 Then
    '                    blnIsBSC_Set = True
    '                    dtmp = mdtBSC_Evaluation.Select("AUD <> 'D'")
    '                    If dtmp.Length > 0 Then
    '                        blnIsOneAssessed = True
    '                    End If
    '                End If
    '            End If

    '            If mdtGE_Evaluation IsNot Nothing Then
    '                dtmp = dsHeaders.Tables(0).Select("Id=-2")
    '                If dtmp.Length > 0 Then
    '                    blnIsGE_Set = True
    '                    dtmp = mdtGE_Evaluation.Select("AUD <> 'D'")
    '                    If dtmp.Length > 0 Then
    '                        blnIsOneAssessed = True
    '                    End If
    '                End If
    '            End If

    '            If blnIsOneAssessed = False Then
    '                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 35, "Sorry, Please assess atleast one item from Balance Score Card Or Competencies Evaluation in order to save."), enMsgBoxStyle.Information)
    '                Exit Sub
    '            End If

    '            'If mdtBSC_Evaluation IsNot Nothing Then
    '            '    dtmp = dsHeaders.Tables(0).Select("Id=-3")
    '            '    If dtmp.Length > 0 Then
    '            '        blnIsBSC_Set = True
    '            '        dtmp = mdtBSC_Evaluation.Select("AUD <> 'D'")
    '            '        If dtmp.Length <= 0 Then
    '            '            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 30, "Sorry, Please assess atleast one item from Balance Score Card Evaluation in order to save."), enMsgBoxStyle.Information)
    '            '            Exit Sub
    '            '        End If
    '            '    End If
    '            'End If

    '            'If mdtGE_Evaluation IsNot Nothing Then
    '            '    dtmp = dsHeaders.Tables(0).Select("Id=-2")
    '            '    If dtmp.Length > 0 Then
    '            '        blnIsGE_Set = True
    '            '        dtmp = mdtGE_Evaluation.Select("AUD <> 'D'")
    '            '        If dtmp.Length <= 0 Then
    '            '            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 34, "Sorry, Please assess atleast one item from Competencies Evaluation in order to save."), enMsgBoxStyle.Information)
    '            '            Exit Sub
    '            '        End If
    '            '    End If
    '            'End If
    '            'S.SANDEEP [23 MAR 2015] -- END

    '            If mdtCustomEvaluation IsNot Nothing Then
    '                If dsHeaders IsNot Nothing AndAlso dsHeaders.Tables(0).Rows.Count > 0 Then
    '                    dtmp = dsHeaders.Tables(0).Select("Id > 0")
    '                    'S.SANDEEP [ 18 DEC 2014 ] -- START
    '                    'Dim dCEval As DataView = mdtCustomEvaluation.DefaultView
    '                    Dim dCEval As DataView = New DataView(mdtCustomEvaluation, "periodunkid = '" & CInt(cboPeriod.SelectedValue) & "' AND AUD <> 'D' AND isdefaultentry = 0 ", "", DataViewRowState.CurrentRows)
    '                    'S.SANDEEP [ 18 DEC 2014 ] -- END
    '                    If dtmp.Length <> dCEval.ToTable(True, "custom_header").Rows.Count Then

    '                        'S.SANDEEP [09 OCT 2015] -- START
    '                        'If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 31, "You have some custom items along with evaluation items, which you have not assessed. Would you like to assess then before saving this evaluation?"), enMsgBoxStyle.Information + enMsgBoxStyle.YesNo) = Windows.Forms.DialogResult.Yes Then
    '                        '    objbtnNext_Click(New Object(), New EventArgs())
    '                        '    Exit Sub
    '                        'End If

    '                        'Shani (26-Sep-2016) -- Start
    '                        'Enhancement -Changes PA Flow And Add New screen Given By (Andrew)
    '                        'If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 31, "You have some custom items along with evaluation items, which you have not assessed. Do you want to save without assessing them?"), enMsgBoxStyle.Information + enMsgBoxStyle.YesNo) = Windows.Forms.DialogResult.No Then

    '                        'Shani (26-Sep-2016) -- Start
    '                        'Enhancement -Changes PA Flow And Add New screen Given By (Andrew)
    '                        'Dim strHNmae = dCEval.ToTable(True, "custom_header").AsEnumerable().Select(Function(x) x.Field(Of String)("custom_header")).ToList
    '                        'Dim strHeaderNameCsv As String = String.Join(",", dtmp.Cast(Of DataRow).Where(Function(x) Not strHNmae.Contains(x.Field(Of String)("Name"))).Select(Function(X) X.Field(Of String)("Name")).ToArray)
    '                        'Dim strmsg As String = Language.getMessage(mstrModuleName, 38, "You have not completed items under")
    '                        'strmsg &= " " & strHeaderNameCsv & ". "
    '                        'strmsg &= Language.getMessage(mstrModuleName, 39, "Do you want to save without completing them?")
    '                        Dim strmsg As String = Language.getMessage(mstrModuleName, 38, "You still have some assessment items not evaluated. Do you want to save now and complete them later?")

    '                        If eZeeMsgBox.Show(strmsg, enMsgBoxStyle.Information + enMsgBoxStyle.YesNo) = Windows.Forms.DialogResult.No Then
    '                            'Shani (26-Sep-2016) -- End 
    '                            objbtnNext_Click(New Object(), New EventArgs())
    '                            Exit Sub
    '                        End If
    '                        'S.SANDEEP [09 OCT 2015] -- END
    '                    End If
    '                End If
    '            End If


    '            'S.SANDEEP [23 MAR 2015] -- START
    '            'Call SetValue()
    '            'S.SANDEEP [23 MAR 2015] -- END
    '            Select Case CType(sender, eZee.Common.eZeeLightButton).Name.ToUpper
    '                Case "BTNSAVECOMMIT"
    '                    If ConfigParameter._Object._IsAllowFinalSave = False Then

    '                        If blnIsBSC_Set = True Then If isAll_Assessed_BSC() = False Then Exit Sub
    '                        If blnIsGE_Set = True Then If isAll_Assessed_GE() = False Then Exit Sub

    '                    ElseIf ConfigParameter._Object._IsAllowFinalSave = True Then
    '                        'S.SANDEEP [01 OCT 2015] -- START
    '                        'If isAll_Assessed_BSC(False) = False Then
    '                        '    If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 32, "There are some Assessement Items which are still not Assessed for particular Group.If you Press Yes,you will Commit this group.Do you want to continue?"), CType(enMsgBoxStyle.Information + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.No Then
    '                        '        Exit Sub
    '                        '    End If
    '                        'End If

    '                        'If isAll_Assessed_GE(False) = False Then
    '                        '    If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 32, "There are some Assessement Items which are still not Assessed for particular Group.If you Press Yes,you will Commit this group.Do you want to continue?"), CType(enMsgBoxStyle.Information + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.No Then
    '                        '        Exit Sub
    '                        '    End If
    '                        'End If
    '                        Dim blnWarnFlag As Boolean = False
    '                        If isAll_Assessed_BSC(False) = False Then 'Shani(30-MAR-2016)
    '                            If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 32, "There are some Assessement Items which are still not Assessed for particular Group.If you Press Yes,you will Commit this group.Do you want to continue?"), CType(enMsgBoxStyle.Information + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.No Then
    '                                Exit Sub
    '                            End If
    '                            blnWarnFlag = True
    '                        End If
    '                        If blnWarnFlag = False Then
    '                            If isAll_Assessed_GE(False) = False Then 'Shani(30-MAR-2016)
    '                                If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 32, "There are some Assessement Items which are still not Assessed for particular Group.If you Press Yes,you will Commit this group.Do you want to continue?"), CType(enMsgBoxStyle.Information + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.No Then
    '                                    Exit Sub
    '                                End If
    '                            End If
    '                        End If
    '                        'S.SANDEEP [01 OCT 2015] -- END
    '                    End If
    '                    'S.SANDEEP [23 MAR 2015] -- START
    '                    'objEAnalysisMst._Iscommitted = True
    '                    'objEAnalysisMst._Committeddatetime = ConfigParameter._Object._CurrentDateAndTime
    '                    'S.SANDEEP [23 MAR 2015] -- END
    '            End Select


    '            'S.SANDEEP [23 MAR 2015] -- START
    '            Call SetValue()
    '            Select Case CType(sender, eZee.Common.eZeeLightButton).Name.ToUpper
    '                Case "BTNSAVECOMMIT"
    '                    objEAnalysisMst._Iscommitted = True
    '                    objEAnalysisMst._Committeddatetime = ConfigParameter._Object._CurrentDateAndTime
    '            End Select
    '            'S.SANDEEP [23 MAR 2015] -- END

    '            If menAction = enAction.EDIT_ONE Then
    '                blnFlag = objEAnalysisMst.Update(mdtBSC_Evaluation, mdtGE_Evaluation, mdtCustomEvaluation)
    '            Else
    '                blnFlag = objEAnalysisMst.Insert(mdtBSC_Evaluation, mdtGE_Evaluation, mdtCustomEvaluation)
    '            End If

    '            If blnFlag = False And objEAnalysisMst._Message <> "" Then
    '                eZeeMsgBox.Show(objEAnalysisMst._Message, enMsgBoxStyle.Information)
    '            End If

    '            If blnFlag Then
    '                If CType(sender, eZee.Common.eZeeLightButton).Name.ToUpper = "BTNSAVECOMMIT" AndAlso objEAnalysisMst._Iscommitted = True Then
    '                    Select Case menAssess
    '                        Case enAssessmentMode.SELF_ASSESSMENT
    '                            'Sohail (21 Aug 2015) -- Start
    '                            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
    '                            'objEAnalysisMst.Email_Notification(enAssessmentMode.SELF_ASSESSMENT, _
    '                            '                                   CInt(cboEmployee.SelectedValue), _
    '                            '                                   CInt(cboPeriod.SelectedValue), _
    '                            '                                   ConfigParameter._Object._IsCompanyNeedReviewer, , , , _
    '                            '                                   enLogin_Mode.DESKTOP, 0)
    '                            objEAnalysisMst.Email_Notification(enAssessmentMode.SELF_ASSESSMENT, _
    '                                                               CInt(cboEmployee.SelectedValue), _
    '                                                               CInt(cboPeriod.SelectedValue), _
    '                                                              ConfigParameter._Object._IsCompanyNeedReviewer, FinancialYear._Object._DatabaseName, , , , _
    '                                                               enLogin_Mode.DESKTOP, 0)
    '                            'Sohail (21 Aug 2015) -- End
    '                        Case enAssessmentMode.APPRAISER_ASSESSMENT
    '                            'Sohail (21 Aug 2015) -- Start
    '                            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
    '                            'objEAnalysisMst.Email_Notification(enAssessmentMode.APPRAISER_ASSESSMENT, _
    '                            '                                   CInt(cboEmployee.SelectedValue), _
    '                            '                                   CInt(cboPeriod.SelectedValue), _
    '                            '                                   ConfigParameter._Object._IsCompanyNeedReviewer, , , _
    '                            '                                   cboAssessor.Text, enLogin_Mode.DESKTOP, 0)
    '                            objEAnalysisMst.Email_Notification(enAssessmentMode.APPRAISER_ASSESSMENT, _
    '                                                               CInt(cboEmployee.SelectedValue), _
    '                                                               CInt(cboPeriod.SelectedValue), _
    '                                                              ConfigParameter._Object._IsCompanyNeedReviewer, FinancialYear._Object._DatabaseName, , , _
    '                                                               cboAssessor.Text, enLogin_Mode.DESKTOP, 0)
    '                            'Sohail (21 Aug 2015) -- End
    '                        Case enAssessmentMode.REVIEWER_ASSESSMENT
    '                            'Sohail (21 Aug 2015) -- Start
    '                            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
    '                            'objEAnalysisMst.Email_Notification(enAssessmentMode.REVIEWER_ASSESSMENT, _
    '                            '                                   CInt(cboEmployee.SelectedValue), _
    '                            '                                   CInt(cboPeriod.SelectedValue), _
    '                            '                                   ConfigParameter._Object._IsCompanyNeedReviewer, , , _
    '                            '                                   cboReviewer.Text, enLogin_Mode.DESKTOP, 0)
    '                            objEAnalysisMst.Email_Notification(enAssessmentMode.REVIEWER_ASSESSMENT, _
    '                                                               CInt(cboEmployee.SelectedValue), _
    '                                                               CInt(cboPeriod.SelectedValue), _
    '                                                              ConfigParameter._Object._IsCompanyNeedReviewer, FinancialYear._Object._DatabaseName, , , _
    '                                                               cboReviewer.Text, enLogin_Mode.DESKTOP, 0)
    '                            'Sohail (21 Aug 2015) -- End
    '                    End Select
    '                End If
    '                mblnCancel = False
    '                If menAction = enAction.ADD_CONTINUE Then
    '                    objEAnalysisMst = Nothing
    '                    objEAnalysisMst = New clsevaluation_analysis_master
    '                    Call GetValue()
    '                    objlblValue1.Visible = False : objlblValue2.Visible = False
    '                    objlblValue3.Visible = False : objlblValue4.Visible = False
    '                    mdtBSC_Evaluation.Rows.Clear() : mdtGE_Evaluation.Rows.Clear() : mdtCustomEvaluation.Rows.Clear()

    '                    'Shani (26-Sep-2016) -- Start
    '                    'Enhancement -Changes PA Flow And Add New screen Given By (Andrew)
    '                    'dgvBSC.DataSource = Nothing : dgvGE.DataSource = Nothing : dgvItems.DataSource = Nothing
    '                    If dgvBSC.DataSource IsNot Nothing Then
    '                        CType(dgvBSC.DataSource, DataTable).Dispose()
    '                    End If

    '                    If dgvGE.DataSource IsNot Nothing Then
    '                        CType(dgvGE.DataSource, DataTable).Dispose()
    '                    End If

    '                    If dgvItems.DataSource IsNot Nothing Then
    '                        CType(dgvItems.DataSource, DataTable).Dispose()
    '                    End If
    '                    'Shani (26-Sep-2016) -- End
    '                    iHeaderId = -1 : Call PanelVisibility()
    '                    Call SetVisibility()
    '                Else
    '                    Me.Close()
    '                End If
    '            End If
    '        End If
    '    Catch ex As Exception
    '        DisplayError.Show("-1", ex.Message, "btnSave_Click", mstrModuleName)
    '    Finally
    '    End Try
    'End Sub
    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click, btnSaveCommit.Click
        Dim blnFlag As Boolean = False
        Dim blnIsBSC_Set As Boolean = False
        Dim blnIsGE_Set As Boolean = False
        Try
            If Validation() = False Then Exit Sub

            'S.SANDEEP |14-MAR-2019| -- START
            'ISSUE/ENHANCEMENT : {#0003481|ARUTI-567}
            Select Case menAssess
                Case enAssessmentMode.SELF_ASSESSMENT
                    If ConfigParameter._Object._MakeEmpAssessCommentsMandatory Then
                        If mdtBSC_Evaluation IsNot Nothing AndAlso mdtBSC_Evaluation.Rows.Count > 0 Then
                            If mdtBSC_Evaluation.AsEnumerable().Where(Function(x) x.Field(Of String)("AUD") <> "D" And x.Field(Of String)("remark") = "").Count > 0 Then
                                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 51, "Sorry, As per the setting set on configuration, Remark are mandatory information for each score you set for Objective/Targets. Please provide remark to continue. "), enMsgBoxStyle.Information)
                                Exit Sub
                            End If
                        End If
                        If mdtGE_Evaluation IsNot Nothing AndAlso mdtGE_Evaluation.Rows.Count > 0 Then
                            If mdtGE_Evaluation.AsEnumerable().Where(Function(x) x.Field(Of String)("AUD") <> "D" And x.Field(Of String)("remark") = "").Count > 0 Then
                                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 52, "Sorry, As per the setting set on configuration, Remark are mandatory information for each score you set for competencies. Please provide remark to continue. "), enMsgBoxStyle.Information)
                                Exit Sub
                            End If
                        End If
                    End If
                Case enAssessmentMode.APPRAISER_ASSESSMENT
                    If ConfigParameter._Object._MakeAsrAssessCommentsMandatory Then
                        If mdtBSC_Evaluation IsNot Nothing AndAlso mdtBSC_Evaluation.Rows.Count > 0 Then
                            If mdtBSC_Evaluation.AsEnumerable().Where(Function(x) x.Field(Of String)("AUD") <> "D" And x.Field(Of String)("remark") = "").Count > 0 Then
                                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 51, "Sorry, As per the setting set on configuration, Remark are mandatory information for each score you set for Objective/Targets. Please provide remark to continue. "), enMsgBoxStyle.Information)
                                Exit Sub
                            End If
                        End If
                        If mdtGE_Evaluation IsNot Nothing AndAlso mdtGE_Evaluation.Rows.Count > 0 Then
                            If mdtGE_Evaluation.AsEnumerable().Where(Function(x) x.Field(Of String)("AUD") <> "D" And x.Field(Of String)("remark") = "").Count > 0 Then
                                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 52, "Sorry, As per the setting set on configuration, Remark are mandatory information for each score you set for competencies. Please provide remark to continue. "), enMsgBoxStyle.Information)
                                Exit Sub
                            End If
                        End If
                    End If
                Case enAssessmentMode.REVIEWER_ASSESSMENT
                    If ConfigParameter._Object._MakeRevAssessCommentsMandatory Then
                        If mdtBSC_Evaluation IsNot Nothing AndAlso mdtBSC_Evaluation.Rows.Count > 0 Then
                            If mdtBSC_Evaluation.AsEnumerable().Where(Function(x) x.Field(Of String)("AUD") <> "D" And x.Field(Of String)("remark") = "").Count > 0 Then
                                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 51, "Sorry, As per the setting set on configuration, Remark are mandatory information for each score you set for Objective/Targets. Please provide remark to continue. "), enMsgBoxStyle.Information)
                                Exit Sub
                            End If
                        End If
                        If mdtGE_Evaluation IsNot Nothing AndAlso mdtGE_Evaluation.Rows.Count > 0 Then
                            If mdtGE_Evaluation.AsEnumerable().Where(Function(x) x.Field(Of String)("AUD") <> "D" And x.Field(Of String)("remark") = "").Count > 0 Then
                                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 52, "Sorry, As per the setting set on configuration, Remark are mandatory information for each score you set for competencies. Please provide remark to continue. "), enMsgBoxStyle.Information)
                                Exit Sub
                            End If
                        End If
                    End If
            End Select
            'S.SANDEEP |14-MAR-2019| -- END

            If dsHeaders.Tables.Count <= 0 Then Exit Sub
            If dsHeaders.Tables(0).Rows.Count > 0 Then
                Dim dtmp() As DataRow = Nothing
                Dim blnIsOneAssessed As Boolean = False

                If mdtBSC_Evaluation IsNot Nothing Then
                    dtmp = dsHeaders.Tables(0).Select("Id=-3")
                    If dtmp.Length > 0 Then
                        blnIsBSC_Set = True
                        dtmp = mdtBSC_Evaluation.Select("AUD <> 'D'")
                        If dtmp.Length > 0 Then
                            'Shani(14-FEB-2017) -- Start
                            'Enhancement - Add New PA Enhancement (Agreed Score,exc) Given by Akiba,KBC,TNP exc...
                            'blnIsOneAssessed = True
                            'S.SANDEEP |31-MAY-2019| -- START
                            'ISSUE/ENHANCEMENT : [PA SETTING CHANGES]
                            'If ConfigParameter._Object._IsUseAgreedScore = True AndAlso ConfigParameter._Object._ReviewerScoreSetting = enReviewerScoreSetting.ReviewerScoreNotNecessary Then
                            '    blnIsOneAssessed = False
                            'Else
                            '    blnIsOneAssessed = True
                            'End If
                            If menAssess = enAssessmentMode.REVIEWER_ASSESSMENT Then
                                If ConfigParameter._Object._ReviewerScoreSetting = enReviewerScoreSetting.ReviewerScoreNeeded Then
                                    If mdtBSC_Evaluation IsNot Nothing AndAlso mdtBSC_Evaluation.Rows.Count > 0 Then
                                        If mdtBSC_Evaluation.AsEnumerable().Where(Function(x) x.Field(Of String)("AUD") <> "D" And x.Field(Of Decimal)("result") < 0).Count > 0 Then
                                            blnIsOneAssessed = False
                                        Else
                                            blnIsOneAssessed = True
                                        End If
                                    End If
                                Else
                                    blnIsOneAssessed = True
                                End If
                            Else
                                blnIsOneAssessed = True
                            End If
                            'S.SANDEEP |31-MAY-2019| -- END

                            'Shani(14-FEB-2017) -- End
                        End If
                    End If
                End If

                If mdtGE_Evaluation IsNot Nothing Then
                    dtmp = dsHeaders.Tables(0).Select("Id=-2")
                    If dtmp.Length > 0 Then
                        blnIsGE_Set = True
                        dtmp = mdtGE_Evaluation.Select("AUD <> 'D'")
                        If dtmp.Length > 0 Then
                            'Shani(14-FEB-2017) -- Start
                            'Enhancement - Add New PA Enhancement (Agreed Score,exc) Given by Akiba,KBC,TNP exc...
                            'blnIsOneAssessed = True
                            'S.SANDEEP |31-MAY-2019| -- START
                            'ISSUE/ENHANCEMENT : [PA SETTING CHANGES]
                            'If ConfigParameter._Object._IsUseAgreedScore = True AndAlso ConfigParameter._Object._ReviewerScoreSetting = enReviewerScoreSetting.ReviewerScoreNotNecessary Then
                            '    blnIsOneAssessed = False
                            'Else
                            '    blnIsOneAssessed = True
                            'End If
                            If menAssess = enAssessmentMode.REVIEWER_ASSESSMENT Then
                                If ConfigParameter._Object._ReviewerScoreSetting = enReviewerScoreSetting.ReviewerScoreNeeded Then
                                    If mdtGE_Evaluation IsNot Nothing AndAlso mdtGE_Evaluation.Rows.Count > 0 Then
                                        If mdtGE_Evaluation.AsEnumerable().Where(Function(x) x.Field(Of String)("AUD") <> "D" And x.Field(Of Decimal)("result") <= 0).Count > 0 Then
                                            blnIsOneAssessed = False
                                        Else
                                            blnIsOneAssessed = True
                                        End If
                                    End If
                                Else
                                    blnIsOneAssessed = True
                                End If
                            Else
                                blnIsOneAssessed = True
                            End If
                            'S.SANDEEP |31-MAY-2019| -- END

                            'Shani(14-FEB-2017) -- End
                        End If
                    End If
                End If

                If blnIsOneAssessed = False Then
                    'S.SANDEEP [01-OCT-2018] -- START
                    'ISSUE/ENHANCEMENT : {Ref#2586|ARUTI-}
                    'eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 35, "Sorry, Please assess atleast one item from Balance Score Card Or Competencies Evaluation in order to save."), enMsgBoxStyle.Information)
                    If blnIsGE_Set = True AndAlso blnIsBSC_Set = True Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 35, "Sorry, Please assess atleast one item from Balance Score Card Or Competencies Evaluation in order to save."), enMsgBoxStyle.Information)
                    ElseIf blnIsBSC_Set = True Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 46, "Sorry, Please assess atleast one item from Balance Score Card Evaluation in order to save."), enMsgBoxStyle.Information)
                    ElseIf blnIsGE_Set = True Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 47, "Sorry, Please assess atleast one item from Competencies Evaluation in order to save."), enMsgBoxStyle.Information)
                    End If
                    'S.SANDEEP [01-OCT-2018] -- END
                    Exit Sub
                End If

                'Shani(23-FEB-2017) -- Start
                'Enhancement - Add new custom item saving setting requested by (aga khan)
                'If mdtCustomEvaluation IsNot Nothing Then
                '    If dsHeaders IsNot Nothing AndAlso dsHeaders.Tables(0).Rows.Count > 0 Then
                '        dtmp = dsHeaders.Tables(0).Select("Id > 0")
                '        Dim dCEval As DataView = New DataView(mdtCustomEvaluation, "periodunkid = '" & CInt(cboPeriod.SelectedValue) & "' AND AUD <> 'D' AND isdefaultentry = 0 ", "", DataViewRowState.CurrentRows)
                '        If dtmp.Length <> dCEval.ToTable(True, "custom_header").Rows.Count Then
                '            Dim strmsg As String = Language.getMessage(mstrModuleName, 38, "You still have some assessment items not evaluated. Do you want to save now and complete them later?")
                '            If eZeeMsgBox.Show(strmsg, enMsgBoxStyle.Information + enMsgBoxStyle.YesNo) = Windows.Forms.DialogResult.No Then
                '                objbtnNext_Click(New Object(), New EventArgs())
                '                Exit Sub
                '            End If
                '        End If
                '    End If
                'End If
                'Shani(23-FEB-2017) -- End
                Select Case CType(sender, eZee.Common.eZeeLightButton).Name.ToUpper
                    Case "BTNSAVECOMMIT"

                        'S.SANDEEP |20-NOV-2019| -- START
                        'ISSUE/ENHANCEMENT : ASSESSMENT DATE SET TO FUTURE 
                        If dtpAssessdate.Value.Date > Now.Date Then
                            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 58, "Sorry, you cannot commit assessment for future date. Please set the assessment date as today date."), enMsgBoxStyle.Information)
                            Exit Sub
                        End If
                        'S.SANDEEP |20-NOV-2019| -- END


                        If ConfigParameter._Object._IsAllowFinalSave = False Then

                            If blnIsBSC_Set = True Then If isAll_Assessed_BSC() = False Then Exit Sub
                            If blnIsGE_Set = True Then If isAll_Assessed_GE() = False Then Exit Sub

                        ElseIf ConfigParameter._Object._IsAllowFinalSave = True Then
                            Dim blnWarnFlag As Boolean = False
                            If isAll_Assessed_BSC(False) = False Then
                                If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 32, "There are some Assessement Items which are still not Assessed for particular Group.If you Press Yes,you will Commit this group.Do you want to continue?"), CType(enMsgBoxStyle.Information + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.No Then
                                    Exit Sub
                                End If
                                blnWarnFlag = True
                            End If
                            If blnWarnFlag = False Then
                                If isAll_Assessed_GE(False) = False Then
                                    If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 32, "There are some Assessement Items which are still not Assessed for particular Group.If you Press Yes,you will Commit this group.Do you want to continue?"), CType(enMsgBoxStyle.Information + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.No Then
                                        Exit Sub
                                    End If
                                End If
                            End If
                        End If
                        'Shani(23-FEB-2017) -- Start
                        'Enhancement - Add new custom item saving setting requested by (aga khan)
                        If ConfigParameter._Object._IsAllowCustomItemFinalSave = False Then
                            If isAll_Assessed_CustomItem() = False Then Exit Sub
                        Else
                            If isAll_Assessed_CustomItem(False) = False Then
                                Dim strmsg As String = Language.getMessage(mstrModuleName, 38, "You still have some assessment items not evaluated. Do you want to save now and complete them later?")
                                If eZeeMsgBox.Show(strmsg, enMsgBoxStyle.Information + enMsgBoxStyle.YesNo) = Windows.Forms.DialogResult.No Then
                                    objbtnNext_Click(New Object(), New EventArgs())
                                    Exit Sub
                                End If
                            End If
                        End If
                    Case Else
                        If mdtCustomEvaluation IsNot Nothing Then
                            If dsHeaders IsNot Nothing AndAlso dsHeaders.Tables(0).Rows.Count > 0 Then
                                dtmp = dsHeaders.Tables(0).Select("Id > 0")
                                Dim dCEval As DataView = New DataView(mdtCustomEvaluation, "periodunkid = '" & CInt(cboPeriod.SelectedValue) & "' AND AUD <> 'D' AND isdefaultentry = 0 ", "", DataViewRowState.CurrentRows)
                                If dtmp.Length <> dCEval.ToTable(True, "custom_header").Rows.Count Then
                                    Dim strmsg As String = Language.getMessage(mstrModuleName, 38, "You still have some assessment items not evaluated. Do you want to save now and complete them later?")
                                    If eZeeMsgBox.Show(strmsg, enMsgBoxStyle.Information + enMsgBoxStyle.YesNo) = Windows.Forms.DialogResult.No Then
                                        objbtnNext_Click(New Object(), New EventArgs())
                                        Exit Sub
                                    End If
                                End If
                            End If
                        End If
                        'Shani(23-FEB-2017) -- End

                End Select

                Call SetValue()

                Select Case CType(sender, eZee.Common.eZeeLightButton).Name.ToUpper
                    Case "BTNSAVECOMMIT"
                        objEAnalysisMst._Iscommitted = True
                        objEAnalysisMst._Committeddatetime = ConfigParameter._Object._CurrentDateAndTime
                End Select

                'S.SANDEEP [28-May-2018] -- START
                'ISSUE/ENHANCEMENT : {Audit Trails} 
                objEAnalysisMst._FormName = mstrModuleName
                objEAnalysisMst._LoginEmployeeunkid = 0
                objEAnalysisMst._ClientIP = getIP()
                objEAnalysisMst._HostName = getHostName()
                objEAnalysisMst._FromWeb = False
                objEAnalysisMst._AuditUserId = User._Object._Userunkid
                objEAnalysisMst._CompanyUnkid = Company._Object._Companyunkid
                objEAnalysisMst._AuditDate = ConfigParameter._Object._CurrentDateAndTime
                'S.SANDEEP [28-May-2018] -- END

                If menAction = enAction.EDIT_ONE Then
                    'S.SANDEEP [27-APR-2017] -- START
                    'ISSUE/ENHANCEMENT : OPTIMIZING PERFORMANCE MODULE
                    'blnFlag = objEAnalysisMst.Update(mdtBSC_Evaluation, mdtGE_Evaluation, mdtCustomEvaluation)
                    blnFlag = objEAnalysisMst.Update(mdtBSC_Evaluation, mdtGE_Evaluation, mdtCustomEvaluation, ConfigParameter._Object._ScoringOptionId, eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), ConfigParameter._Object._IsUseAgreedScore, ConfigParameter._Object._Self_Assign_Competencies)
                    'S.SANDEEP [27-APR-2017] -- END
                Else
                    'S.SANDEEP [27-APR-2017] -- START
                    'ISSUE/ENHANCEMENT : OPTIMIZING PERFORMANCE MODULE
                    'blnFlag = objEAnalysisMst.Insert(mdtBSC_Evaluation, mdtGE_Evaluation, mdtCustomEvaluation)
                    blnFlag = objEAnalysisMst.Insert(mdtBSC_Evaluation, mdtGE_Evaluation, mdtCustomEvaluation, ConfigParameter._Object._ScoringOptionId, eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), ConfigParameter._Object._IsUseAgreedScore, ConfigParameter._Object._Self_Assign_Competencies)
                    'S.SANDEEP [27-APR-2017] -- END
                End If

                If blnFlag = False And objEAnalysisMst._Message <> "" Then
                    eZeeMsgBox.Show(objEAnalysisMst._Message, enMsgBoxStyle.Information)
                End If

                If blnFlag Then
                    If CType(sender, eZee.Common.eZeeLightButton).Name.ToUpper = "BTNSAVECOMMIT" AndAlso objEAnalysisMst._Iscommitted = True Then

                        'S.SANDEEP |25-MAR-2019| -- START
                        Select Case menAssess
                            Case enAssessmentMode.SELF_ASSESSMENT
                                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 53, "Your assessment has been submitted successfully."), enMsgBoxStyle.Information)
                            Case enAssessmentMode.APPRAISER_ASSESSMENT
                                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 54, "Assessment has been submitted successfully."), enMsgBoxStyle.Information)
                        End Select
                        'S.SANDEEP |25-MAR-2019| -- END

                        Select Case menAssess
                            Case enAssessmentMode.SELF_ASSESSMENT
                                objEAnalysisMst.Email_Notification(enAssessmentMode.SELF_ASSESSMENT, _
                                                                   CInt(cboEmployee.SelectedValue), _
                                                                   CInt(cboPeriod.SelectedValue), _
                                                                  ConfigParameter._Object._IsCompanyNeedReviewer, FinancialYear._Object._DatabaseName, , , , _
                                                                   enLogin_Mode.DESKTOP, 0)
                            Case enAssessmentMode.APPRAISER_ASSESSMENT
                                objEAnalysisMst.Email_Notification(enAssessmentMode.APPRAISER_ASSESSMENT, _
                                                                   CInt(cboEmployee.SelectedValue), _
                                                                   CInt(cboPeriod.SelectedValue), _
                                                                  ConfigParameter._Object._IsCompanyNeedReviewer, FinancialYear._Object._DatabaseName, , , _
                                                                   cboAssessor.Text, enLogin_Mode.DESKTOP, 0)
                            Case enAssessmentMode.REVIEWER_ASSESSMENT
                                objEAnalysisMst.Email_Notification(enAssessmentMode.REVIEWER_ASSESSMENT, _
                                                                   CInt(cboEmployee.SelectedValue), _
                                                                   CInt(cboPeriod.SelectedValue), _
                                                                  ConfigParameter._Object._IsCompanyNeedReviewer, FinancialYear._Object._DatabaseName, , , _
                                                                   cboReviewer.Text, enLogin_Mode.DESKTOP, 0)
                        End Select
                        'S.SANDEEP |30-MAR-2019| -- START
                    Else
                        Select Case menAssess
                            Case enAssessmentMode.SELF_ASSESSMENT
                                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 55, "Your assessment has been saved successfully."), enMsgBoxStyle.Information)
                            Case enAssessmentMode.APPRAISER_ASSESSMENT
                                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 56, "Assessment has been saved successfully."), enMsgBoxStyle.Information)
                        End Select
                        'S.SANDEEP |30-MAR-2019| -- END
                    End If
                    mblnCancel = False
                    If menAction = enAction.ADD_CONTINUE Then
                        objEAnalysisMst = Nothing
                        objEAnalysisMst = New clsevaluation_analysis_master
                        Call GetValue()
                        objlblValue1.Visible = False : objlblValue2.Visible = False
                        objlblValue3.Visible = False : objlblValue4.Visible = False
                        mdtBSC_Evaluation.Rows.Clear() : mdtGE_Evaluation.Rows.Clear() : mdtCustomEvaluation.Rows.Clear()
                        If dgvBSC.DataSource IsNot Nothing Then
                            CType(dgvBSC.DataSource, DataTable).Dispose()
                        End If
                        If dgvGE.DataSource IsNot Nothing Then
                            CType(dgvGE.DataSource, DataTable).Dispose()
                        End If
                        If dgvItems.DataSource IsNot Nothing Then
                            CType(dgvItems.DataSource, DataTable).Dispose()
                        End If
                        iHeaderId = -1 : Call PanelVisibility()
                        Call SetVisibility()
                    Else
                        Me.Close()
                    End If
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnSave_Click", mstrModuleName)
        Finally
        End Try
    End Sub
    'S.SANDEEP [25-JAN-2017] -- END


    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnClose_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnReset.Click
        Try
            objlblValue1.Visible = False : objlblValue2.Visible = False
            objlblValue3.Visible = False : objlblValue4.Visible = False
            If menAssess = enAssessmentMode.SELF_ASSESSMENT Then
                cboEmployee.SelectedValue = 0
                cboPeriod.SelectedValue = 0
                dgvBSC.DataSource = Nothing
                dgvGE.DataSource = Nothing
                dgvItems.DataSource = Nothing
            ElseIf menAssess = enAssessmentMode.APPRAISER_ASSESSMENT Then
                cboAssessor.SelectedValue = 0
                cboEmployee.SelectedValue = 0
                cboPeriod.SelectedValue = 0
                dgvBSC.DataSource = Nothing
                dgvGE.DataSource = Nothing
                dgvItems.DataSource = Nothing
            ElseIf menAssess = enAssessmentMode.REVIEWER_ASSESSMENT Then
                cboReviewer.SelectedValue = 0
                cboEmployee.SelectedValue = 0
                cboPeriod.SelectedValue = 0
                dgvBSC.DataSource = Nothing
                dgvGE.DataSource = Nothing
                dgvItems.DataSource = Nothing
            End If

            'S.SANDEEP [04 MAR 2015] -- START
            If lnkCopyScore.Visible = True Then lnkCopyScore.Enabled = False
            'S.SANDEEP [04 MAR 2015] -- END

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnReset_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub objbtnSearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearch.Click
        Try
            If CInt(cboEmployee.SelectedValue) <= 0 Or _
               CInt(cboPeriod.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Please set following information [Employee,Period] to view assessment."), enMsgBoxStyle.Information)
                Exit Sub
            End If

            'S.SANDEEP |08-JAN-2019| -- START
            If menAction <> enAction.EDIT_ONE AndAlso menAssess = enAssessmentMode.SELF_ASSESSMENT Then
                Dim objPeriod As New clscommom_period_Tran
                Dim intDaysBefore As Integer = 0 : Dim iEndDate As DateTime = Nothing
                objPeriod._Periodunkid(FinancialYear._Object._DatabaseName) = CInt(cboPeriod.SelectedValue)
                intDaysBefore = objPeriod._LockDaysBefore
                If intDaysBefore > 0 Then
                    iEndDate = objPeriod._End_Date.AddDays(-(intDaysBefore))
                End If
                objPeriod = Nothing
                If iEndDate <> Nothing Then
                    If ConfigParameter._Object._CurrentDateAndTime.Date > iEndDate Then
                        If LockEmployee() Then
                            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 49, "Sorry, you cannot do assessment as last date for assessment has been passed.") & Language.getMessage(mstrModuleName, 50, "Please contact your administrator/manager to do futher operation on it."), enMsgBoxStyle.Information)
                            Exit Sub
                        End If
                    End If
                End If
            End If
            'S.SANDEEP |08-JAN-2019| -- END


            'S.SANDEEP [25-JAN-2017] -- START
            'ISSUE/ENHANCEMENT : Pefromance Module Left Out Changes
            Dim objAckEval As New clsevaluation_acknowledgement_tran
            If (objAckEval.MakeLinkVisible(CInt(cboEmployee.SelectedValue), CInt(cboPeriod.SelectedValue)) = True) Then
                lnkViewApproveRejectRemark.Visible = True
            Else
                lnkViewApproveRejectRemark.Visible = False
            End If
            'S.SANDEEP [25-JAN-2017] -- END



            If (Is_Already_Assessed() = False) Then
                Exit Sub
            End If

            'S.SANDEEP [04 JUN 2015] -- START
            'If dtpAssessdate.Value.Date > FinancialYear._Object._Database_End_Date.Date Then
            '    Dim dsYr As New DataSet : Dim oCompany As New clsCompany_Master
            '    dsYr = oCompany.GetFinancialYearList(Company._Object._Companyunkid, User._Object._Userunkid, "List", mintYearUnkid)
            '    If dsYr.Tables("List").Rows.Count > 0 Then
            '        If dtpAssessdate.Value.Date > eZeeDate.convertDate(dsYr.Tables("List").Rows(0).Item("end_date").ToString).Date Or _
            '           dtpAssessdate.Value.Date < eZeeDate.convertDate(dsYr.Tables("List").Rows(0).Item("start_date").ToString).Date Then
            '            Dim strMsg As String = Language.getMessage(mstrModuleName, 19, "Assessment date should be in between ") & eZeeDate.convertDate(dsYr.Tables("List").Rows(0).Item("start_date").ToString).Date & _
            '                                   Language.getMessage(mstrModuleName, 20, " And ") & eZeeDate.convertDate(dsYr.Tables("List").Rows(0).Item("end_date").ToString).Date
            '            eZeeMsgBox.Show(strMsg, enMsgBoxStyle.Information)
            '            dtpAssessdate.Focus()
            '            Exit Sub
            '        End If
            '    Else
            '        If dtpAssessdate.Value.Date > FinancialYear._Object._Database_End_Date.Date Or _
            '           dtpAssessdate.Value.Date < FinancialYear._Object._Database_Start_Date.Date Then
            '            Dim strMsg As String = Language.getMessage(mstrModuleName, 19, "Assessment date should be in between ") & FinancialYear._Object._Database_Start_Date.Date & _
            '                                   Language.getMessage(mstrModuleName, 20, " And ") & FinancialYear._Object._Database_End_Date.Date
            '            eZeeMsgBox.Show(strMsg, enMsgBoxStyle.Information)
            '            dtpAssessdate.Focus()
            '            Exit Sub
            '        End If
            '    End If
            'End If

            Dim objPrd As New clscommom_period_Tran
            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'objPrd._Periodunkid = CInt(cboPeriod.SelectedValue)
            objPrd._Periodunkid(FinancialYear._Object._DatabaseName) = CInt(cboPeriod.SelectedValue)
            'Sohail (21 Aug 2015) -- End
            If dtpAssessdate.Value.Date <= objPrd._Start_Date.Date Then
                Dim strMsg As String = Language.getMessage(mstrModuleName, 36, "Sorry, assessment date should be greater than") & " ( " & objPrd._Start_Date.Date.ToShortDateString & " )."
                eZeeMsgBox.Show(strMsg, enMsgBoxStyle.Information)
                dtpAssessdate.Focus()
                objPrd = Nothing
                Exit Sub
            End If
            objPrd = Nothing

            If ConfigParameter._Object._AllowAssessor_Before_Emp = False Then
                Dim strMsg As String = String.Empty
                If menAssess <> enAssessmentMode.SELF_ASSESSMENT Then
                    strMsg = objEAnalysisMst.IsValidAssessmentDate(menAssess, dtpAssessdate.Value.Date, CInt(cboPeriod.SelectedValue), CInt(cboEmployee.SelectedValue))
                    If strMsg <> "" Then
                        eZeeMsgBox.Show(strMsg, enMsgBoxStyle.Information)
                        Exit Sub
                    End If
                End If
            End If
            'S.SANDEEP [04 JUN 2015] -- END

            If dsHeaders.Tables(0).Rows.Count > 0 Then
                'S.SANDEEP |18-JAN-2019| -- START
                'objbtnNext.Enabled = True : iHeaderId = -1 : objbtnNext_Click(sender, e)
                objbtnNext.Enabled = True : iHeaderId = -1
                'S.SANDEEP |18-JAN-2019| -- END
                objbtnBack.Enabled = True
            End If

            'S.SANDEEP [04 MAR 2015] -- START
            If lnkCopyScore.Visible = True Then lnkCopyScore.Enabled = True
            'S.SANDEEP [04 MAR 2015] -- END

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearch_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub objbtnSearchEmployee_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchEmployee.Click
        Dim frm As New frmCommonSearch
        Try
            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If

            If cboEmployee.DataSource IsNot Nothing Then
                frm.ValueMember = cboEmployee.ValueMember
                frm.DisplayMember = cboEmployee.DisplayMember
                Select Case menAssess
                    Case enAssessmentMode.SELF_ASSESSMENT
                        frm.CodeMember = "employeecode"
                    Case enAssessmentMode.APPRAISER_ASSESSMENT, enAssessmentMode.REVIEWER_ASSESSMENT
                        frm.CodeMember = "Code"
                End Select
                frm.DataSource = CType(cboEmployee.DataSource, DataTable)
                If frm.DisplayDialog Then
                    cboEmployee.SelectedValue = frm.SelectedValue
                    cboEmployee.Focus()
                End If
            End If


        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchEmployee_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub objbtnSearchAssessor_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchAssessor.Click
        Dim frm As New frmCommonSearch
        Try
            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If

            frm.ValueMember = cboAssessor.ValueMember
            frm.DisplayMember = cboAssessor.DisplayMember
            frm.CodeMember = "Code"
            frm.DataSource = CType(cboAssessor.DataSource, DataTable)
            If frm.DisplayDialog Then
                cboAssessor.SelectedValue = frm.SelectedValue
                cboAssessor.Focus()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchAssessor_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub objbtnSearchReviewer_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles objbtnSearchReviewer.Click
        Dim frm As New frmCommonSearch
        Try
            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If

            frm.ValueMember = cboReviewer.ValueMember
            frm.DisplayMember = cboReviewer.DisplayMember
            frm.CodeMember = "Code"
            frm.DataSource = CType(cboReviewer.DataSource, DataTable)
            If frm.DisplayDialog Then
                cboReviewer.SelectedValue = frm.SelectedValue
                cboReviewer.Focus()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchReviewer_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub objbtnNext_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnNext.Click
        Try
            If dsHeaders.Tables(0).Rows.Count > 0 Then
                If iHeaderId < dsHeaders.Tables(0).Rows.Count - 1 Then
                    iHeaderId += 1
                    objbtnBack.Enabled = True
                End If
                If iHeaderId >= dsHeaders.Tables(0).Rows.Count - 1 Then
                    objbtnNext.Enabled = False
                End If
                Call PanelVisibility()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnNext_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub objbtnBack_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnBack.Click
        Try
            If dsHeaders.Tables(0).Rows.Count > 0 Then
                iHeaderId = iHeaderId - 1
                If iHeaderId <= -1 Then
                    objbtnBack.Enabled = False
                    objbtnNext.Enabled = True
                End If
                If iHeaderId >= dsHeaders.Tables(0).Rows.Count - 1 Then
                    objbtnNext.Enabled = False
                Else
                    objbtnNext.Enabled = True
                End If
                Call PanelVisibility()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnBack_Click", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

#Region " Combo's Events "

    Private Sub cboAssessor_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboAssessor.SelectedIndexChanged
        Try
            If menAssess = enAssessmentMode.SELF_ASSESSMENT Then Exit Sub
            If CInt(cboAssessor.SelectedValue) > 0 Then
                Dim dsList As New DataSet
                If radInternalAssessor.Checked = True Then
                    'S.SANDEEP [04 JUN 2015] -- START
                    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                    'dsList = objEAnalysisMst.getEmployeeBasedAssessor(CInt(cboAssessor.SelectedValue), "AEmp", True)

                    dsList = objEAnalysisMst.getEmployeeBasedAssessor(FinancialYear._Object._DatabaseName, _
                                                                      User._Object._Userunkid, _
                                                                      eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                                      eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), True, _
                                                                      ConfigParameter._Object._IsIncludeInactiveEmp, CInt(cboAssessor.SelectedValue), "AEmp", True)
                    'S.SANDEEP [04 JUN 2015] -- END
                ElseIf radExternalAssessor.Checked = True Then
                    Dim objExtAssessor As New clsexternal_assessor_master
                    dsList = objExtAssessor.GetEmpBasedOnExtAssessor(CInt(cboAssessor.SelectedValue), "AEmp", True)
                End If
                With cboEmployee
                    .ValueMember = "Id"
                    .DisplayMember = "Name"
                    .DataSource = dsList.Tables("AEmp")
                    .SelectedValue = 0
                End With
            Else
                cboEmployee.DataSource = Nothing
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboAssessor_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub cboReviewer_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboReviewer.SelectedIndexChanged
        Try
            If menAssess = enAssessmentMode.SELF_ASSESSMENT Then Exit Sub
            If CInt(cboReviewer.SelectedValue) > 0 Then
                Dim dsList As New DataSet
                'S.SANDEEP [04 JUN 2015] -- START
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                'dsList = objEAnalysisMst.getEmployeeBasedAssessor(CInt(cboReviewer.SelectedValue), "REmp", True)

                dsList = objEAnalysisMst.getEmployeeBasedAssessor(FinancialYear._Object._DatabaseName, _
                                                                  User._Object._Userunkid, _
                                                                  eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                                  eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), True, _
                                                                  ConfigParameter._Object._IsIncludeInactiveEmp, _
                                                                  CInt(cboReviewer.SelectedValue), "REmp", True)
                'S.SANDEEP [04 JUN 2015] -- END

                With cboEmployee
                    .ValueMember = "Id"
                    .DisplayMember = "Name"
                    .DataSource = dsList.Tables("REmp")
                    .SelectedValue = 0
                End With
            Else
                cboEmployee.DataSource = Nothing
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboReviewer_SelectedIndexChanged", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub cboPeriod_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboPeriod.SelectedIndexChanged, cboEmployee.SelectedIndexChanged
        Try
            Dim objPrd As New clscommom_period_Tran
            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'objPrd._Periodunkid = CInt(cboPeriod.SelectedValue)
            objPrd._Periodunkid(FinancialYear._Object._DatabaseName) = CInt(cboPeriod.SelectedValue)
            'Sohail (21 Aug 2015) -- End
            mintYearUnkid = objPrd._Yearunkid
            objPrd = Nothing

            Dim objMapping As New clsAssess_Field_Mapping
            iLinkedFieldId = objMapping.Get_Map_FieldId(CInt(cboPeriod.SelectedValue))
            iMappingUnkid = objMapping.Get_MappingUnkId(CInt(cboPeriod.SelectedValue))
            objMapping._Mappingunkid = iMappingUnkid
            xTotalWeightAssigned = objMapping._Weight
            objMapping = Nothing

            Dim objFMaster As New clsAssess_Field_Master
            iExOrdr = objFMaster.Get_Field_ExOrder(iLinkedFieldId, True)
            objFMaster = Nothing

            'Dim objScaleMapping As New clsassess_scalemapping_tran
            'objScaleMapping._Periodunkid = CInt(cboPeriod.SelectedValue)
            'If iMappingUnkid = objScaleMapping._Mappingunkid Then
            '    iScaleMasterId = objScaleMapping._Scalemasterunkid
            'End If
            'objScaleMapping = Nothing

            'S.SANDEEP [29 DEC 2015] -- START
            'objCCustomTran._EmployeeId = CInt(cboEmployee.SelectedValue)

            'Shani (26-Sep-2016) -- Start
            'Enhancement -Changes PA Flow And Add New screen Given By (Andrew)
            'objCCustomTran._AllowCustomItemInPlanning = ConfigParameter._Object._IncludeCustomItemInPlanning
            objCCustomTran._IsCompanyNeedReviewer = ConfigParameter._Object._IsCompanyNeedReviewer
            'Shani (26-Sep-2016) -- End

            objCCustomTran._EmployeeId = CInt(cboEmployee.SelectedValue)
            objCCustomTran._PeriodId = CInt(cboPeriod.SelectedValue)
            'S.SANDEEP [29 DEC 2015] -- END
            mdtCustomEvaluation = objCCustomTran._DataTable
            'If menAction <> enAction.EDIT_ONE Then
            '    mdtCustomEvaluation.Rows.Clear()
            'End If

            If CInt(cboPeriod.SelectedValue) > 0 Then
                If ConfigParameter._Object._Perf_EvaluationOrder.Trim.Length > 0 Then
                    Dim objCHeader As New clsassess_custom_header
                    dsHeaders = objCHeader.getComboList(CInt(cboPeriod.SelectedValue), False, "List", True)
                    dsHeaders.Tables(0).Rows.Clear()
                    Dim xRow As DataRow = Nothing
                    Dim iOrdr() As String = ConfigParameter._Object._Perf_EvaluationOrder.Split("|")
                    If iOrdr.Length > 0 Then
                        Select Case CInt(iOrdr(0))
                            Case enEvaluationOrder.PE_BSC_SECTION
                                xRow = dsHeaders.Tables(0).NewRow

                                'Shani (26-Sep-2016) -- Start
                                'Enhancement -Changes PA Flow And Add New screen Given By (Andrew)
                                'xRow.Item("Id") = -3 : xRow.Item("Name") = Language.getMessage(mstrModuleName, 3, "Balance Score Card Evaluation") : xRow.Item("periodunkid") = CInt(cboPeriod.SelectedValue)
                                xRow.Item("Id") = -3 : xRow.Item("Name") = Language.getMessage(mstrModuleName, 40, "Objectives/Goals/Targets") : xRow.Item("periodunkid") = CInt(cboPeriod.SelectedValue)
                                'Shani (26-Sep-2016) -- End


                                dsHeaders.Tables(0).Rows.InsertAt(xRow, 0)
                            Case enEvaluationOrder.PE_COMPETENCY_SECTION
                                xRow = dsHeaders.Tables(0).NewRow
                                xRow.Item("Id") = -2 : xRow.Item("Name") = Language.getMessage(mstrModuleName, 4, "Competencies Evaluation") : xRow.Item("periodunkid") = CInt(cboPeriod.SelectedValue)
                                dsHeaders.Tables(0).Rows.InsertAt(xRow, 0)
                            Case enEvaluationOrder.PE_CUSTOM_SECTION
                                dsHeaders = objCHeader.getComboList(CInt(cboPeriod.SelectedValue), False, "List", True)
                        End Select
                        Select Case CInt(iOrdr(1))
                            Case enEvaluationOrder.PE_BSC_SECTION
                                xRow = dsHeaders.Tables(0).NewRow

                                'Shani (26-Sep-2016) -- Start
                                'Enhancement -Changes PA Flow And Add New screen Given By (Andrew)
                                'xRow.Item("Id") = -3 : xRow.Item("Name") = Language.getMessage(mstrModuleName, 3, "Balance Score Card Evaluation") : xRow.Item("periodunkid") = CInt(cboPeriod.SelectedValue)
                                xRow.Item("Id") = -3 : xRow.Item("Name") = Language.getMessage(mstrModuleName, 40, "Objectives/Goals/Targets") : xRow.Item("periodunkid") = CInt(cboPeriod.SelectedValue)
                                'Shani (26-Sep-2016) -- End


                                If dsHeaders.Tables(0).Rows.Count > 0 Then
                                    dsHeaders.Tables(0).Rows.InsertAt(xRow, dsHeaders.Tables(0).Rows.Count)
                                Else
                                    dsHeaders.Tables(0).Rows.InsertAt(xRow, 0)
                                End If
                            Case enEvaluationOrder.PE_COMPETENCY_SECTION
                                xRow = dsHeaders.Tables(0).NewRow
                                xRow.Item("Id") = -2 : xRow.Item("Name") = Language.getMessage(mstrModuleName, 4, "Competencies Evaluation") : xRow.Item("periodunkid") = CInt(cboPeriod.SelectedValue)
                                If dsHeaders.Tables(0).Rows.Count > 0 Then
                                    dsHeaders.Tables(0).Rows.InsertAt(xRow, dsHeaders.Tables(0).Rows.Count)
                                Else
                                    dsHeaders.Tables(0).Rows.InsertAt(xRow, 0)
                                End If
                            Case enEvaluationOrder.PE_CUSTOM_SECTION
                                If dsHeaders.Tables(0).Rows.Count > 0 Then
                                    Dim dsList As New DataSet
                                    dsList = objCHeader.getComboList(CInt(cboPeriod.SelectedValue), False, "List", True)
                                    dsHeaders.Tables(0).Merge(dsList.Tables(0), True)
                                Else
                                    dsHeaders = objCHeader.getComboList(CInt(cboPeriod.SelectedValue), False, "List", True)
                                End If
                        End Select
                        Select Case CInt(iOrdr(2))
                            Case enEvaluationOrder.PE_BSC_SECTION
                                xRow = dsHeaders.Tables(0).NewRow

                                'Shani (26-Sep-2016) -- Start
                                'Enhancement -Changes PA Flow And Add New screen Given By (Andrew)
                                'xRow.Item("Id") = -3 : xRow.Item("Name") = Language.getMessage(mstrModuleName, 3, "Balance Score Card Evaluation") : xRow.Item("periodunkid") = CInt(cboPeriod.SelectedValue)
                                xRow.Item("Id") = -3 : xRow.Item("Name") = Language.getMessage(mstrModuleName, 40, "Objectives/Goals/Targets") : xRow.Item("periodunkid") = CInt(cboPeriod.SelectedValue)
                                'Shani (26-Sep-2016) -- End


                                If dsHeaders.Tables(0).Rows.Count > 0 Then
                                    dsHeaders.Tables(0).Rows.InsertAt(xRow, dsHeaders.Tables(0).Rows.Count)
                                Else
                                    dsHeaders.Tables(0).Rows.InsertAt(xRow, 0)
                                End If
                            Case enEvaluationOrder.PE_COMPETENCY_SECTION
                                xRow = dsHeaders.Tables(0).NewRow
                                xRow.Item("Id") = -2 : xRow.Item("Name") = Language.getMessage(mstrModuleName, 4, "Competencies Evaluation") : xRow.Item("periodunkid") = CInt(cboPeriod.SelectedValue)
                                If dsHeaders.Tables(0).Rows.Count > 0 Then
                                    dsHeaders.Tables(0).Rows.InsertAt(xRow, dsHeaders.Tables(0).Rows.Count)
                                Else
                                    dsHeaders.Tables(0).Rows.InsertAt(xRow, 0)
                                End If
                            Case enEvaluationOrder.PE_CUSTOM_SECTION
                                If dsHeaders.Tables(0).Rows.Count > 0 Then
                                    Dim dsList As New DataSet
                                    dsList = objCHeader.getComboList(CInt(cboPeriod.SelectedValue), False, "List", True)
                                    dsHeaders.Tables(0).Merge(dsList.Tables(0), True)
                                Else
                                    dsHeaders = objCHeader.getComboList(CInt(cboPeriod.SelectedValue), False, "List", True)
                                End If
                        End Select
                    End If
                    objCHeader = Nothing
                End If

                ''S.SANDEEP [25-JAN-2017] -- START
                ''ISSUE/ENHANCEMENT : Pefromance Module Left Out Changes
                'If dsHeaders.Tables(0).Rows.Count > 0 Then
                '    Dim aRow = dsHeaders.Tables(0).NewRow
                '    aRow.Item("Id") = 99999
                '    aRow.Item("Name") = Language.getMessage(mstrModuleName, 42, "Approve/Reject Performance Evaluation Done")
                '    If dsHeaders.Tables(0).Rows.Count > 0 Then
                '        dsHeaders.Tables(0).Rows.InsertAt(aRow, dsHeaders.Tables(0).Rows.Count)
                '    Else
                '        dsHeaders.Tables(0).Rows.InsertAt(aRow, 0)
                '    End If
                'End If
                ''S.SANDEEP [25-JAN-2017] -- END


                'If ConfigParameter._Object._Perf_EvaluationOrder.Trim.Length > 0 Then
                '    Dim xRow As DataRow = Nothing
                '    Dim iOrdr() As String = ConfigParameter._Object._Perf_EvaluationOrder.Split("|")
                '    If iOrdr.Length > 0 Then
                '        Select Case CInt(iOrdr(0))
                '            Case enEvaluationOrder.PE_BSC_SECTION
                '                xRow = dsHeaders.Tables(0).NewRow
                '                xRow.Item("Id") = -3 : xRow.Item("Name") = Language.getMessage(mstrModuleName, 3, "Balance Score Card Evaluation") : xRow.Item("periodunkid") = CInt(cboPeriod.SelectedValue)
                '                dsHeaders.Tables(0).Rows.InsertAt(xRow, 0)
                '            Case enEvaluationOrder.PE_COMPETENCY_SECTION
                '                xRow = dsHeaders.Tables(0).NewRow
                '                xRow.Item("Id") = -2 : xRow.Item("Name") = Language.getMessage(mstrModuleName, 4, "Competencies Evaluation") : xRow.Item("periodunkid") = CInt(cboPeriod.SelectedValue)
                '                dsHeaders.Tables(0).Rows.InsertAt(xRow, 0)
                '            Case enEvaluationOrder.PE_CUSTOM_SECTION
                '                Dim objCHeader As New clsassess_custom_header
                '                dsHeaders = objCHeader.getComboList(CInt(cboPeriod.SelectedValue), False, "List")
                '                objCHeader = Nothing
                '        End Select

                '        Select Case CInt(iOrdr(1))
                '            Case enEvaluationOrder.PE_BSC_SECTION
                '                xRow = dsHeaders.Tables(0).NewRow
                '                xRow.Item("Id") = -3 : xRow.Item("Name") = Language.getMessage(mstrModuleName, 3, "Balance Score Card Evaluation") : xRow.Item("periodunkid") = CInt(cboPeriod.SelectedValue)
                '                If iOrdr(0) = enEvaluationOrder.PE_CUSTOM_SECTION Then
                '                    dsHeaders.Tables(0).Rows.InsertAt(xRow, dsHeaders.Tables(0).Rows.Count)
                '                Else
                '                    dsHeaders.Tables(0).Rows.InsertAt(xRow, 1)
                '                End If
                '            Case enEvaluationOrder.PE_COMPETENCY_SECTION
                '                xRow = dsHeaders.Tables(0).NewRow
                '                xRow.Item("Id") = -2 : xRow.Item("Name") = Language.getMessage(mstrModuleName, 4, "Competencies Evaluation") : xRow.Item("periodunkid") = CInt(cboPeriod.SelectedValue)
                '                If iOrdr(0) = enEvaluationOrder.PE_CUSTOM_SECTION Then
                '                    dsHeaders.Tables(0).Rows.InsertAt(xRow, dsHeaders.Tables(0).Rows.Count)
                '                Else
                '                    dsHeaders.Tables(0).Rows.InsertAt(xRow, 1)
                '                End If
                '            Case enEvaluationOrder.PE_CUSTOM_SECTION
                '                Dim objCHeader As New clsassess_custom_header
                '                dsHeaders = objCHeader.getComboList(CInt(cboPeriod.SelectedValue), False, "List")
                '                objCHeader = Nothing
                '        End Select

                '        Select Case CInt(iOrdr(2))
                '            Case enEvaluationOrder.PE_BSC_SECTION
                '                xRow = dsHeaders.Tables(0).NewRow
                '                xRow.Item("Id") = -3 : xRow.Item("Name") = Language.getMessage(mstrModuleName, 3, "Balance Score Card Evaluation") : xRow.Item("periodunkid") = CInt(cboPeriod.SelectedValue)
                '                dsHeaders.Tables(0).Rows.InsertAt(xRow, dsHeaders.Tables(0).Rows.Count)
                '            Case enEvaluationOrder.PE_COMPETENCY_SECTION
                '                xRow = dsHeaders.Tables(0).NewRow
                '                xRow.Item("Id") = -2 : xRow.Item("Name") = Language.getMessage(mstrModuleName, 4, "Competencies Evaluation") : xRow.Item("periodunkid") = CInt(cboPeriod.SelectedValue)
                '                dsHeaders.Tables(0).Rows.InsertAt(xRow, dsHeaders.Tables(0).Rows.Count)
                '            Case enEvaluationOrder.PE_CUSTOM_SECTION
                '                Dim objCHeader As New clsassess_custom_header
                '                dsHeaders = objCHeader.getComboList(CInt(cboPeriod.SelectedValue), False, "List")
                '                objCHeader = Nothing
                '        End Select
                '    End If



                ' = dsHeaders.Tables(0).NewRow

                'xRow.Item("Id") = -2 : xRow.Item("Name") = Language.getMessage(mstrModuleName, 3, "Balance Score Card Evaluation") : xRow.Item("periodunkid") = CInt(cboPeriod.SelectedValue)
                'dsHeaders.Tables(0).Rows.InsertAt(xRow, 0)

                'xRow = dsHeaders.Tables(0).NewRow
                'xRow.Item("Id") = -1 : xRow.Item("Name") = Language.getMessage(mstrModuleName, 4, "Competencies Evaluation") : xRow.Item("periodunkid") = CInt(cboPeriod.SelectedValue)
                'dsHeaders.Tables(0).Rows.InsertAt(xRow, 1)
            Else
                If dsHeaders IsNot Nothing AndAlso dsHeaders.Tables.Count > 0 Then
                    dsHeaders.Tables(0).Rows.Clear()
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboPeriod_SelectedIndexChanged", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

#Region " Radio Buttons Events "

    Private Sub radInternalAssessor_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles radInternalAssessor.CheckedChanged
        Try
            If radInternalAssessor.Checked = True Then
                cboAssessor.DataSource = Nothing
                'S.SANDEEP [04 JUN 2015] -- START
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                'Dim dsCombos As DataSet = objEAnalysisMst.getAssessorComboList("Assessor", True)
                Dim dsCombos As DataSet = objEAnalysisMst.getAssessorComboList(FinancialYear._Object._DatabaseName, _
                                                                               User._Object._Userunkid, _
                                                                               eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                                               eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), True, _
                                                                               ConfigParameter._Object._IsIncludeInactiveEmp, "Assessor", clsAssessor.enARVisibilityTypeId.VISIBLE, True, False) 'S.SANDEEP [27 DEC 2016] -- START {clsAssessor.enARVisibilityTypeId.VISIBLE}-- END

                'S.SANDEEP [04 JUN 2015] -- END

                With cboAssessor
                    .ValueMember = "Id"
                    .DisplayMember = "Name"
                    .DataSource = dsCombos.Tables("Assessor")
                    .SelectedValue = 0
                End With
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "radInternalAssessor_CheckedChanged", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub radExternalAssessor_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles radExternalAssessor.CheckedChanged
        Try
            If radExternalAssessor.Checked = True Then
                cboAssessor.DataSource = Nothing
                Dim objExtAssessor As New clsexternal_assessor_master
                Dim dsCombos As DataSet = objExtAssessor.GetDisplayNameComboList("Assessor", True)
                With cboAssessor
                    .ValueMember = "Id"
                    .DisplayMember = "Name"
                    .DataSource = dsCombos.Tables("Assessor")
                    .SelectedValue = 0
                End With
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "radExternalAssessor_CheckedChanged", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

#Region " Link Event "

    Private Sub objlnkClose_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles objlnkClose.LinkClicked
        objgbScoreGuide.Visible = False
        If objpnlBSC.Visible = True Then
            dgvBSC.Enabled = True
        ElseIf objpnlGE.Visible = True Then
            dgvGE.Enabled = True
        ElseIf objpnlCItems.Visible = True Then
            dgvItems.Enabled = True
        End If
    End Sub

    'S.SANDEEP [31 AUG 2016] -- START
    'ENHANCEMENT : INCLUSION OF GRADES ON ASSESSMENT GROUP {BY ANDREW}
    Private Sub objlnkCloseDesc_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles objlnkCloseDesc.LinkClicked
        Try
            objgbDescription.Visible = False
            If objpnlBSC.Visible = True Then
                dgvBSC.Enabled = True
            ElseIf objpnlGE.Visible = True Then
                dgvGE.Enabled = True
            ElseIf objpnlCItems.Visible = True Then
                dgvItems.Enabled = True
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objlnkCloseDesc_LinkClicked", mstrModuleName)
        Finally
        End Try
    End Sub
    'S.SANDEEP [31 AUG 2016] -- START


    'S.SANDEEP [04 MAR 2015] -- START
    Private Sub lnkCopyScore_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lnkCopyScore.LinkClicked
        Dim decReslut As Decimal = 0
        Try
            '################################## COPY SCORE BSC DATA ##################################> START
            If objpnlBSC.Visible = True Then

                'Shani (28-Apr-2016) -- Start
                If (Is_Already_Assessed() = False) Then
                    Exit Sub
                End If
                'Shani (28-Apr-2016) -- End
                For Each dgvBRow As DataGridViewRow In dgvBSC.Rows
                    dgvBSC.CurrentCell = dgvBRow.Cells(0)
                    If CBool(dgvBRow.Cells(objdgcolhIsGrpBSC.Index).Value) = False Then
                        dgvBRow.Cells(dgcolhrselfBSC.Index).Value = dgvBRow.Cells(dgcolhaselfBSC.Index).Value
                        'dgvBRow.Cells(dgcolhrremarkBSC.Index).Value = dgvBRow.Cells(dgcolharemarkBSC.Index).Value
                        Decimal.TryParse(dgvBRow.Cells(dgcolhrselfBSC.Index).Value, decReslut)
                        'Shani (28-Apr-2016) -- Start
                        Call Evaluated_Data_BSC(decReslut, dgvBRow.Cells(dgcolhrremarkBSC.Index).Value)
                        'Shani (28-Apr-2016) -- End
                    End If
                Next
            End If
            '################################## COPY SCORE BSC DATA ##################################> END

            '################################## COPY SCORE COMPETENCIES DATA ##################################> START
            If objpnlGE.Visible = True Then
                'Shani (28-Apr-2016) -- Start
                If (Is_Already_Assessed() = False) Then
                    Exit Sub
                End If
                'Shani (28-Apr-2016) -- End
                For Each dgvCRow As DataGridViewRow In dgvGE.Rows
                    dgvGE.CurrentCell = dgvCRow.Cells(0)

                    'Shani (28-Apr-2016) -- Start
                    'If CBool(dgvCRow.Cells(objdgcolhIsGrpGE.Index).Value) = False Then
                    If CBool(dgvCRow.Cells(objdgcolhIsGrpGE.Index).Value) = False AndAlso CBool(dgvCRow.Cells(objdgcolhIsPGrpGE.Index).Value) = False Then
                        'Shani (28-Apr-2016) -- End
                        dgvCRow.Cells(dgcolhrselfGE.Index).Value = dgvCRow.Cells(dgcolhaselfGE.Index).Value
                        'dgvCRow.Cells(dgcolhrremarkGE.Index).Value = dgvCRow.Cells(dgcolharemarkGE.Index).Value
                        Decimal.TryParse(dgvCRow.Cells(dgcolhrselfGE.Index).Value, decReslut)
                        'Shani (28-Apr-2016) -- Start
                        Call Evaluated_Data_GE(decReslut, dgvCRow.Cells(dgcolhrremarkGE.Index).Value)
                        'Shani (28-Apr-2016) -- End
                    End If
                Next
            End If
            '################################## COPY SCORE COMPETENCIES DATA ##################################> END
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lnkCopyScore_LinkClicked", mstrModuleName)
        Finally
        End Try
    End Sub
    'S.SANDEEP [04 MAR 2015] -- END


    'S.SANDEEP [25-JAN-2017] -- START
    'ISSUE/ENHANCEMENT : Pefromance Module Left Out Changes
    Private Sub lnkViewApproveRejectRemark_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lnkViewApproveRejectRemark.LinkClicked
        Try
            gbRemarks.Location = New Point(246, 98)
            gbRemarks.BringToFront()
            gbRemarks.Visible = True
            If objpnlBSC.Visible = True Then
                dgvBSC.Enabled = True
            ElseIf objpnlGE.Visible = True Then
                dgvGE.Enabled = True
            ElseIf objpnlCItems.Visible = True Then
                dgvItems.Enabled = True
            End If
            Dim dtExist As DataTable : Dim objAckEval As New clsevaluation_acknowledgement_tran
            dtExist = objAckEval.IsExists(CInt(cboEmployee.SelectedValue), CInt(cboPeriod.SelectedValue))
            objlblAsrCmts.Text = Language.getMessage(mstrModuleName, 48, "Assessor Assessment")
            objlblRvrCmts.Text = Language.getMessage(mstrModuleName, 44, "Reviewer Assessment")
            If dtExist.Rows.Count > 0 Then
                For Each row As DataRow In dtExist.Rows
                    Select Case row.Item("assessmodeid")
                        Case enAssessmentMode.APPRAISER_ASSESSMENT
                            If row.Item("statusunkid") = clsevaluation_acknowledgement_tran.enEvaluationAcknowledgement.AGREE Then
                                objlblAsrCmts.Text &= " | " & Language.getMessage(mstrModuleName, 45, "Status") & " : " & row.Item("ack_status").ToString
                            ElseIf row.Item("statusunkid") = clsevaluation_acknowledgement_tran.enEvaluationAcknowledgement.DISAGREE Then
                                objlblAsrCmts.Text &= " | " & Language.getMessage(mstrModuleName, 45, "Status") & " : " & row.Item("ack_status").ToString
                            End If
                            txtAssessorCmts.Text = row.Item("acknowledgement_comments")
                        Case enAssessmentMode.REVIEWER_ASSESSMENT
                            If row.Item("statusunkid") = clsevaluation_acknowledgement_tran.enEvaluationAcknowledgement.AGREE Then
                                objlblRvrCmts.Text &= " | " & Language.getMessage(mstrModuleName, 45, "Status") & " : " & row.Item("ack_status").ToString
                            ElseIf row.Item("statusunkid") = clsevaluation_acknowledgement_tran.enEvaluationAcknowledgement.DISAGREE Then
                                objlblRvrCmts.Text &= " | " & Language.getMessage(mstrModuleName, 45, "Status") & " : " & row.Item("ack_status").ToString
                            End If
                            txtReviewerCmts.Text = row.Item("acknowledgement_comments")
                    End Select
                Next
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lnkViewApproveRejectRemark_LinkClicked", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub objlnkCloseRemark_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles objlnkCloseRemark.LinkClicked
        Try
            gbRemarks.Visible = False
            If objpnlBSC.Visible = True Then
                dgvBSC.Enabled = True
            ElseIf objpnlGE.Visible = True Then
                dgvGE.Enabled = True
            ElseIf objpnlCItems.Visible = True Then
                dgvItems.Enabled = True
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objlnkCloseRemark_LinkClicked", mstrModuleName)
        Finally
        End Try
    End Sub
    'S.SANDEEP [25-JAN-2017] -- END

#End Region

#Region " DataGrid Events "

    '----------------------------------------------- BALANCE SCORE CARD EVALUATION
    Private Sub dgvBSC_DataBindingComplete(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewBindingCompleteEventArgs) Handles dgvBSC.DataBindingComplete
        Try
            'If menAction = enAction.EDIT_ONE Then
            Call Set_BSC_Grid_Style()
            'End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgvBSC_DataBindingComplete", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub dgvBSC_DataError(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewDataErrorEventArgs) Handles dgvBSC.DataError

    End Sub

    Private Sub dgvBSC_CellEnter(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvBSC.CellEnter
        Try
            If menAssess = enAssessmentMode.SELF_ASSESSMENT Then
                If e.ColumnIndex = dgcolheselfBSC.Index Or e.ColumnIndex = dgcolheremarkBSC.Index Then
                    SendKeys.Send("{F2}")
                End If
            ElseIf menAssess = enAssessmentMode.APPRAISER_ASSESSMENT Then

                'Shani (23-Nov-2016) -- Start
                'Enhancement - Add New PA Enhancement (Agreed Score,exc) Given by Akiba,KBC,TNP exc...
                'If e.ColumnIndex = dgcolhaselfBSC.Index Or e.ColumnIndex = dgcolharemarkBSC.Index Then
                If e.ColumnIndex = dgcolhaselfBSC.Index Or e.ColumnIndex = dgcolharemarkBSC.Index Or e.ColumnIndex = dgcolhAgreedScoreBSC.Index Then
                    'Shani (23-Nov-2016) -- End
                    SendKeys.Send("{F2}")
                End If
            ElseIf menAssess = enAssessmentMode.REVIEWER_ASSESSMENT Then
                If e.ColumnIndex = dgcolhrselfBSC.Index Or e.ColumnIndex = dgcolhrremarkBSC.Index Then
                    SendKeys.Send("{F2}")
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgvBSC_CellEnter", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub dgvBSC_CellContentClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvBSC.CellContentClick, dgvBSC.CellContentDoubleClick
        Try
            Dim i As Integer
            If e.RowIndex = -1 Then Exit Sub

            If Me.dgvBSC.IsCurrentCellDirty Then
                Me.dgvBSC.CommitEdit(DataGridViewDataErrorContexts.Commit)
            End If
            If CBool(dgvBSC.Rows(e.RowIndex).Cells(objdgcolhIsGrpBSC.Index).Value) = True Then
                Select Case CInt(e.ColumnIndex)
                    Case objdgcolhBSCCollaps.Index
                        If dgvBSC.Rows(e.RowIndex).Cells(objdgcolhBSCCollaps.Index).Value Is "-" Then
                            dgvBSC.Rows(e.RowIndex).Cells(objdgcolhBSCCollaps.Index).Value = "+"
                        Else
                            dgvBSC.Rows(e.RowIndex).Cells(objdgcolhBSCCollaps.Index).Value = "-"
                        End If

                        For i = e.RowIndex + 1 To dgvBSC.RowCount - 1
                            If CInt(dgvBSC.Rows(e.RowIndex).Cells(objdgcolhGrpIdBSC.Index).Value) = CInt(dgvBSC.Rows(i).Cells(objdgcolhGrpIdBSC.Index).Value) Then
                                If dgvBSC.Rows(i).Visible = False Then
                                    dgvBSC.Rows(i).Visible = True
                                Else
                                    dgvBSC.Rows(i).Visible = False
                                End If
                            Else
                                Exit For
                            End If
                        Next
                End Select
            Else
                Select Case CInt(e.ColumnIndex)
                    Case dgcolhBSCScore.Index
                        If CInt(dgvBSC.Rows(e.RowIndex).Cells(objdgcolhScaleMasterId.Index).Value) > 0 Then
                            Dim dsList As New DataSet
                            Dim objScaleMaster As New clsAssessment_Scale
                            dsList = objScaleMaster.GetList("List", CInt(cboPeriod.SelectedValue), CInt(dgvBSC.Rows(e.RowIndex).Cells(objdgcolhScaleMasterId.Index).Value))
                            lvScaleList.Items.Clear()
                            For Each drow As DataRow In dsList.Tables(0).Rows
                                Dim lvitem As New ListViewItem
                                lvitem.Text = drow.Item("scale").ToString
                                lvitem.SubItems.Add(drow.Item("description").ToString)
                                lvScaleList.Items.Add(lvitem)
                            Next
                            If lvScaleList.Items.Count >= 9 Then
                                colhDescription.Width = 395 - 20
                            Else
                                colhDescription.Width = 395
                            End If
                            objgbScoreGuide.Location = New Point(316, 147)
                            objgbScoreGuide.BringToFront()
                        Else
                            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 26, "Sorry no score guide defined for the selected item."), enMsgBoxStyle.Information)
                            Exit Sub
                        End If
                        objgbScoreGuide.Visible = True : dgvBSC.Enabled = False
                End Select
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgvBSC_CellContentClick", mstrModuleName)
        Finally
        End Try
    End Sub

    'S.SANDEEP [23 APR 2015] -- START

    '    Private Sub dgvBSC_CellValueChanged(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvBSC.CellValueChanged
    '        Try
    '            If mblIsFormLoad = False Then Exit Sub

    '            If e.RowIndex <= -1 Then Exit Sub
    '            Dim idgvResult, idgvRemark As DataGridViewColumn
    '            idgvResult = Nothing : idgvRemark = Nothing

    '            If menAssess = enAssessmentMode.SELF_ASSESSMENT Then
    '                idgvResult = dgcolheselfBSC
    '                idgvRemark = dgcolheremarkBSC
    '            ElseIf menAssess = enAssessmentMode.APPRAISER_ASSESSMENT Then
    '                idgvResult = dgcolhaselfBSC
    '                idgvRemark = dgcolharemarkBSC
    '            ElseIf menAssess = enAssessmentMode.REVIEWER_ASSESSMENT Then
    '                idgvResult = dgcolhrselfBSC
    '                idgvRemark = dgcolhrremarkBSC
    '            End If

    '            'S.SANDEEP [21 JAN 2015] -- START
    '            If IsNumeric(dgvBSC.Rows(e.RowIndex).Cells(dgcolhBSCWeight.Index).Value) Then
    '                mdecItemWeight = CDec(dgvBSC.Rows(e.RowIndex).Cells(dgcolhBSCWeight.Index).Value)
    '            End If
    '            'S.SANDEEP [21 JAN 2015] -- END

    '            If e.ColumnIndex = idgvRemark.Index Then
    '                If dgvBSC.Rows(e.RowIndex).Cells(idgvRemark.Index).Value.ToString.Trim.Length > 0 Then
    '                    'S.SANDEEP [21 JAN 2015] -- START
    '                    'Call Evaluated_Data_BSC(Convert.ToDecimal(dgvBSC.Rows(e.RowIndex).Cells(idgvResult.Index).Value), CStr(dgvBSC.Rows(e.RowIndex).Cells(idgvRemark.Index).Value))
    '                    Call Evaluated_Data_BSC(Convert.ToDecimal(dgvBSC.Rows(e.RowIndex).Cells(idgvResult.Index).Value), CStr(dgvBSC.Rows(e.RowIndex).Cells(idgvRemark.Index).Value), True)
    '                    'S.SANDEEP [21 JAN 2015] -- END
    '                End If
    '            ElseIf e.ColumnIndex = idgvResult.Index Then
    '                Dim iDecVal As Decimal
    '                Decimal.TryParse(dgvBSC.Rows(e.RowIndex).Cells(idgvResult.Index).Value.ToString, iDecVal)
    '                If IsNumeric(iDecVal) Then
    '                    Select Case CInt(ConfigParameter._Object._ScoringOptionId)
    '                        Case enScoringOption.SC_WEIGHTED_BASED
    '                            'S.SANDEEP [ 17 DEC 2014 ] -- START
    '                            'If CDec(iDecVal) > CDec(dgvBSC.CurrentRow.Cells(dgcolhGEWeight.Index).Value) Then
    '                            '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 10, "Sorry, you cannot set more result value than assigned weight."), enMsgBoxStyle.Information)
    '                            '    Dim xtmp() As DataRow = Nothing
    '                            '    xtmp = Get_Old_Data_BSC()
    '                            '    If xtmp IsNot Nothing AndAlso xtmp.Length > 0 Then
    '                            '        dgvBSC.Rows(e.RowIndex).Cells(idgvResult.Index).Value = xtmp(0)("result")
    '                            '    Else
    '                            '        dgvBSC.Rows(e.RowIndex).Cells(idgvResult.Index).Value = "0"
    '                            '    End If
    '                            '    Exit Sub
    '                            'Else
    '                            '    GoTo iValid
    '                            'End If
    '                            If dgvBSC.CurrentRow Is Nothing Then Exit Sub
    '                            If CBool(dgvBSC.Rows(e.RowIndex).Cells(objdgcolhIsGrpBSC.Index).Value) = True Then Exit Sub
    '                            If CDec(iDecVal) > CDec(dgvBSC.Rows(e.RowIndex).Cells(dgcolhBSCWeight.Index).Value) Then
    '                                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 10, "Sorry, you cannot set more result value than assigned weight."), enMsgBoxStyle.Information)
    '                                Dim xtmp() As DataRow = Nothing
    '                                xtmp = Get_Old_Data_BSC()
    '                                If xtmp IsNot Nothing AndAlso xtmp.Length > 0 Then
    '                                    dgvBSC.Rows(e.RowIndex).Cells(idgvResult.Index).Value = xtmp(0)("result")
    '                                Else
    '                                    dgvBSC.Rows(e.RowIndex).Cells(idgvResult.Index).Value = "0"
    '                                End If
    '                                Exit Sub
    '                            Else
    '                                GoTo iValid
    '                            End If
    '                            'S.SANDEEP [ 17 DEC 2014 ] -- END
    '                        Case enScoringOption.SC_SCALE_BASED
    '                            If CBool(dgvBSC.Rows(e.RowIndex).Cells(objdgcolhIsGrpBSC.Index).Value) = True Then Exit Sub
    '                            Decimal.TryParse(dgvBSC.Rows(e.RowIndex).Cells(idgvResult.Index).Value.ToString, iDecVal)
    '                            If IsNumeric(iDecVal) Then
    '                                If CInt(dgvBSC.Rows(e.RowIndex).Cells(objdgcolhScaleMasterId.Index).Value) > 0 Then
    '                                    Dim dsScore_Guide As New DataSet
    '                                    Dim objScaleMaster As New clsAssessment_Scale
    '                                    dsScore_Guide = objScaleMaster.GetList("List", CInt(cboPeriod.SelectedValue), CInt(dgvBSC.Rows(e.RowIndex).Cells(objdgcolhScaleMasterId.Index).Value))
    '                                    objScaleMaster = Nothing

    '                                    'S.SANDEEP [21 JAN 2015] -- START
    '                                    mdecMaxScale = dsScore_Guide.Tables(0).Compute("MAX(scale)", "")
    '                                    'S.SANDEEP [21 JAN 2015] -- END

    '                                    Dim dtmp() As DataRow = dsScore_Guide.Tables(0).Select("scale = " & CDec(iDecVal))
    '                                    If dtmp.Length <= 0 Then
    '                                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 28, "Sorry, particular scale is not defined. Please refer score guide for the list of score(s) defined."), enMsgBoxStyle.Information)
    '                                        Dim xtmp() As DataRow = Nothing
    '                                        xtmp = Get_Old_Data_BSC()
    '                                        If xtmp IsNot Nothing AndAlso xtmp.Length > 0 Then
    '                                            dgvBSC.Rows(e.RowIndex).Cells(idgvResult.Index).Value = xtmp(0)("result")
    '                                        Else
    '                                            'S.SANDEEP [23 APR 2015] -- START
    '                                            'dgvBSC.Rows(e.RowIndex).Cells(idgvResult.Index).Value = "0"
    '                                            If dgvBSC.Rows(e.RowIndex).Cells(idgvResult.Index).Value <> "0" Then
    '                                                dgvBSC.Rows(e.RowIndex).Cells(idgvResult.Index).Value = "0"
    '                                            End If
    '                                            'S.SANDEEP [23 APR 2015] -- END
    '                                        End If
    '                                        Exit Sub
    '                                    Else
    '                                        GoTo iValid
    '                                    End If
    '                                Else
    '                                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 29, "Sorry, no scale is not defined. Please define scale."), enMsgBoxStyle.Information)
    '                                    Exit Sub
    '                                End If
    '                            Else
    '                                Exit Sub
    '                            End If
    '                    End Select
    'iValid:             If Validation() = False Then Exit Sub
    '                    'S.SANDEEP [ 01 JAN 2015 ] -- START
    '                    'If Is_Already_Assessed() = False Then
    '                    '    Exit Sub
    '                    'End If
    '                    If (Is_Already_Assessed() = False) Then
    '                        Exit Sub
    '                    End If
    '                    'S.SANDEEP [ 01 JAN 2015 ] -- END
    '                    Call Evaluated_Data_BSC(iDecVal, dgvBSC.CurrentRow.Cells(idgvRemark.Index).Value.ToString)
    '                End If
    '            End If
    '        Catch ex As Exception
    '            DisplayError.Show("-1", ex.Message, "dgvBSC_CellValueChanged", mstrModuleName)
    '        Finally
    '        End Try
    '    End Sub

    Private Sub dgvBSC_CellValidating(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellValidatingEventArgs) Handles dgvBSC.CellValidating
        Try
            If mblIsFormLoad = False Then Exit Sub
            If e.RowIndex <= -1 Then Exit Sub
            Dim idgvResult, idgvRemark, idgvAgreedScore As DataGridViewColumn
            idgvResult = Nothing : idgvRemark = Nothing : idgvAgreedScore = Nothing

            If menAssess = enAssessmentMode.SELF_ASSESSMENT Then
                idgvResult = dgcolheselfBSC
                idgvRemark = dgcolheremarkBSC
            ElseIf menAssess = enAssessmentMode.APPRAISER_ASSESSMENT Then
                idgvResult = dgcolhaselfBSC
                idgvRemark = dgcolharemarkBSC
                If ConfigParameter._Object._IsUseAgreedScore Then
                    idgvAgreedScore = dgcolhAgreedScoreBSC
                End If
            ElseIf menAssess = enAssessmentMode.REVIEWER_ASSESSMENT Then
                idgvResult = dgcolhrselfBSC
                idgvRemark = dgcolhrremarkBSC
            End If

            If IsNumeric(dgvBSC.Rows(e.RowIndex).Cells(dgcolhBSCWeight.Index).Value) Then
                mdecItemWeight = CDec(dgvBSC.Rows(e.RowIndex).Cells(dgcolhBSCWeight.Index).Value)
            End If

            If dgvBSC.IsCurrentCellDirty Then
                dgvBSC.CommitEdit(DataGridViewDataErrorContexts.Commit)
            End If

            If e.ColumnIndex = idgvRemark.Index Then
                If dgvBSC.Rows(e.RowIndex).Cells(idgvRemark.Index).Value.ToString.Trim.Length > 0 Then
                    If idgvAgreedScore IsNot Nothing Then
                        'S.SANDEEP |31-MAY-2019| -- START
                        'ISSUE/ENHANCEMENT : [PA SETTING CHANGES]
                        If IsNumeric(dgvBSC.Rows(e.RowIndex).Cells(idgvAgreedScore.Index).Value) Then
                            Call Evaluated_Data_BSC(Convert.ToDecimal(dgvBSC.Rows(e.RowIndex).Cells(idgvResult.Index).Value), CStr(dgvBSC.Rows(e.RowIndex).Cells(idgvRemark.Index).Value), True, Convert.ToDecimal(dgvBSC.Rows(e.RowIndex).Cells(idgvAgreedScore.Index).Value))
                        Else
                            Call Evaluated_Data_BSC(Convert.ToDecimal(dgvBSC.Rows(e.RowIndex).Cells(idgvResult.Index).Value), CStr(dgvBSC.Rows(e.RowIndex).Cells(idgvRemark.Index).Value), True, 0)
                        End If
                    Else
                        If idgvResult.Visible = True Then
                            Call Evaluated_Data_BSC(Convert.ToDecimal(dgvBSC.Rows(e.RowIndex).Cells(idgvResult.Index).Value), CStr(dgvBSC.Rows(e.RowIndex).Cells(idgvRemark.Index).Value), True)
                        Else
                            Call Evaluated_Data_BSC(0, CStr(dgvBSC.Rows(e.RowIndex).Cells(idgvRemark.Index).Value), True)
                        End If
                        'S.SANDEEP |31-MAY-2019| -- END

                    End If
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgvBSC_CellValidating", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub bsctxt_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Try

            Dim iText As TextBox = Nothing : iText = CType(sender, TextBox)

            'S.SANDEEP [03 SEP 2015] -- START
            If iText.Text.Trim.Length <= 0 Then Exit Sub
            'S.SANDEEP [03 SEP 2015] -- END

            Dim iDecVal As Decimal : Decimal.TryParse(iText.Text, iDecVal)
            Dim idgvResult, idgvRemark As DataGridViewColumn

            idgvResult = Nothing : idgvRemark = Nothing

            If menAssess = enAssessmentMode.SELF_ASSESSMENT Then
                idgvResult = dgcolheselfBSC
                idgvRemark = dgcolheremarkBSC
            ElseIf menAssess = enAssessmentMode.APPRAISER_ASSESSMENT Then
                idgvResult = dgcolhaselfBSC
                idgvRemark = dgcolharemarkBSC
                'Shani (23-Nov-2016) -- Start
                'Enhancement - Add New PA Enhancement (Agreed Score,exc) Given by Akiba,KBC,TNP exc...
                'If ConfigParameter._Object._UseAgreedScore Then
                '    idgvAgreedScore = dgcolhAgreedScore
                'End If
                'Shani (23-Nov-2016) -- End
            ElseIf menAssess = enAssessmentMode.REVIEWER_ASSESSMENT Then
                idgvResult = dgcolhrselfBSC
                idgvRemark = dgcolhrremarkBSC
            End If

            'S.SANDEEP [03 SEP 2015] -- START
            'RemoveHandler iText.TextChanged, AddressOf bsctxt_TextChanged
            'S.SANDEEP [03 SEP 2015] -- END

            If IsNumeric(dgvBSC.CurrentRow.Cells(dgcolhBSCWeight.Index).Value) Then
                mdecItemWeight = CDec(dgvBSC.CurrentRow.Cells(dgcolhBSCWeight.Index).Value)
            End If

            If IsNumeric(iDecVal) Then
                Select Case CInt(ConfigParameter._Object._ScoringOptionId)
                    Case enScoringOption.SC_WEIGHTED_BASED
                        If dgvBSC.CurrentRow Is Nothing Then Exit Sub
                        If CBool(dgvBSC.CurrentRow.Cells(objdgcolhIsGrpBSC.Index).Value) = True Then Exit Sub

                        If CDec(iDecVal) > CDec(dgvBSC.CurrentRow.Cells(dgcolhBSCWeight.Index).Value) Then
                            'S.SANDEEP |08-JAN-2019| -- START
                            'eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 10, "Sorry, you cannot set more result value than assigned weight."), enMsgBoxStyle.Information)
                            'Dim xtmp() As DataRow = Nothing
                            'xtmp = Get_Old_Data_BSC()
                            'If xtmp IsNot Nothing AndAlso xtmp.Length > 0 Then
                            '    'Shani (23-Nov-2016) -- Start
                            '    'Enhancement - Add New PA Enhancement (Agreed Score,exc) Given by Akiba,KBC,TNP exc...
                            '    'dgvBSC.CurrentRow.Cells(idgvResult.Index).Value = xtmp(0)("result")
                            '    If menAssess = enAssessmentMode.APPRAISER_ASSESSMENT Then
                            '        If dgvBSC.CurrentCell.ColumnIndex = dgcolhAgreedScoreBSC.Index Then
                            '            dgvBSC.CurrentRow.Cells(dgcolhAgreedScoreBSC.Index).Value = xtmp(0)("agreed_score")
                            '        Else
                            '            dgvBSC.CurrentRow.Cells(idgvResult.Index).Value = xtmp(0)("result")
                            '        End If
                            '    Else
                            '        dgvBSC.CurrentRow.Cells(idgvResult.Index).Value = xtmp(0)("result")
                            '    End If
                            '    'Shani (23-Nov-2016) -- End
                            'Else
                            '    'RemoveHandler iText.TextChanged, AddressOf bsctxt_TextChanged
                            '    iText.Text = ""
                            '    'AddHandler iText.TextChanged, AddressOf bsctxt_TextChanged
                            'End If
                            If ConfigParameter._Object._EnableBSCAutomaticRating = False Then
                                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 10, "Sorry, you cannot set more result value than assigned weight."), enMsgBoxStyle.Information)
                                Dim xtmp() As DataRow = Nothing
                                xtmp = Get_Old_Data_BSC()
                                If xtmp IsNot Nothing AndAlso xtmp.Length > 0 Then
                                    'Shani (23-Nov-2016) -- Start
                                    'Enhancement - Add New PA Enhancement (Agreed Score,exc) Given by Akiba,KBC,TNP exc...
                                    'dgvBSC.CurrentRow.Cells(idgvResult.Index).Value = xtmp(0)("result")
                                    If menAssess = enAssessmentMode.APPRAISER_ASSESSMENT Then
                                        If dgvBSC.CurrentCell.ColumnIndex = dgcolhAgreedScoreBSC.Index Then
                                            dgvBSC.CurrentRow.Cells(dgcolhAgreedScoreBSC.Index).Value = xtmp(0)("agreed_score")
                                        Else
                                            dgvBSC.CurrentRow.Cells(idgvResult.Index).Value = xtmp(0)("result")
                                        End If
                                    Else
                                        dgvBSC.CurrentRow.Cells(idgvResult.Index).Value = xtmp(0)("result")
                                    End If
                                    'Shani (23-Nov-2016) -- End
                                Else
                                    'RemoveHandler iText.TextChanged, AddressOf bsctxt_TextChanged
                                    iText.Text = ""
                                End If
                                'S.SANDEEP |17-MAY-2021| -- START
                                'ISSUE/ENHANCEMENT : SUNBIRD CHANGES FOR SCORES
                            Else
                                If ConfigParameter._Object._DontAllowToEditScoreGenbySys = False Then
                                    If ConfigParameter._Object._DontAllowRatingBeyond100 Then
                                        If CDec(iDecVal) > 100 Then
                                            Dim xtmp() As DataRow = Nothing
                                            xtmp = Get_Old_Data_BSC()
                                            If xtmp IsNot Nothing AndAlso xtmp.Length > 0 Then
                                                If menAssess = enAssessmentMode.APPRAISER_ASSESSMENT Then
                                                    If dgvBSC.CurrentCell.ColumnIndex = dgcolhAgreedScoreBSC.Index Then
                                                        dgvBSC.CurrentRow.Cells(dgcolhAgreedScoreBSC.Index).Value = xtmp(0)("agreed_score")
                                                    Else
                                                        dgvBSC.CurrentRow.Cells(idgvResult.Index).Value = xtmp(0)("result")
                                                    End If
                                                Else
                                                    iText.Text = dgvBSC.CurrentRow.Cells(idgvResult.Index).Value
                                                End If
                                            Else
                                                iText.Text = ""
                                            End If
                                        End If
                                    End If
                                End If
                            End If
                            'S.SANDEEP |08-JAN-2019| -- END
                            Exit Sub
                        End If
                    Case enScoringOption.SC_SCALE_BASED
                        If CBool(dgvBSC.CurrentRow.Cells(objdgcolhIsGrpBSC.Index).Value) = True Then Exit Sub
                        'Shani (26-Sep-2016) -- Start
                        'Enhancement -Changes PA Flow And Add New screen Given By (Andrew)
                        'If IsNumeric(iDecVal) Then
                        If IsNumeric(iDecVal) AndAlso iDecVal > 0 Then
                            'Shani (26-Sep-2016) -- End
                            If CInt(dgvBSC.CurrentRow.Cells(objdgcolhScaleMasterId.Index).Value) > 0 Then
                                Dim dsScore_Guide As New DataSet
                                Dim objScaleMaster As New clsAssessment_Scale
                                dsScore_Guide = objScaleMaster.GetList("List", CInt(cboPeriod.SelectedValue), CInt(dgvBSC.CurrentRow.Cells(objdgcolhScaleMasterId.Index).Value))
                                objScaleMaster = Nothing

                                'Shani (26-Sep-2016) -- Start
                                'Enhancement -Changes PA Flow And Add New screen Given By (Andrew)
                                Try
                                    mdecMaxScale = dsScore_Guide.Tables(0).Compute("MAX(scale)", "")
                                Catch ex As Exception
                                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 41, "No scale define in the system"), enMsgBoxStyle.Information)
                                    Exit Sub
                                End Try
                                'Shani (26-Sep-2016) -- End

                                Dim dtmp() As DataRow = dsScore_Guide.Tables(0).Select("scale = " & CDec(iDecVal))
                                If dtmp.Length <= 0 Then
                                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 28, "Sorry, particular scale is not defined. Please refer score guide for the list of score(s) defined."), enMsgBoxStyle.Information)
                                    Dim xtmp() As DataRow = Nothing
                                    xtmp = Get_Old_Data_BSC()
                                    If xtmp IsNot Nothing AndAlso xtmp.Length > 0 Then
                                        'Shani (26-Sep-2016) -- Start
                                        'Enhancement -Changes PA Flow And Add New screen Given By (Andrew)
                                        'iText.Text = xtmp(0)("result")
                                        If CDec(xtmp(0)("result")) = CDec(iDecVal) Then
                                            iText.Text = ""
                                        Else
                                            'Shani (23-Nov-2016) -- Start
                                            'Enhancement - Add New PA Enhancement (Agreed Score,exc) Given by Akiba,KBC,TNP exc...
                                            'iText.Text = xtmp(0)("result")
                                            If menAssess = enAssessmentMode.APPRAISER_ASSESSMENT Then
                                                If dgvBSC.CurrentCell.ColumnIndex = dgcolhAgreedScoreBSC.Index Then
                                                    iText.Text = xtmp(0)("agreed_score")
                                                Else
                                                    iText.Text = xtmp(0)("result")
                                                End If
                                            Else
                                                iText.Text = xtmp(0)("result")
                                            End If
                                            'Shani (23-Nov-2016) -- End
                                            iText.SelectAll()
                                        End If
                                        'Shani (26-Sep-2016) -- End
                                    Else
                                        'RemoveHandler iText.TextChanged, AddressOf bsctxt_TextChanged
                                        iText.Text = ""
                                        'AddHandler iText.TextChanged, AddressOf bsctxt_TextChanged
                                    End If
                                    Exit Sub
                                Else
                                    GoTo iValid
                                End If
                            Else
                                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 29, "Sorry, scale is not defined. Please define scale."), enMsgBoxStyle.Information)
                                Exit Sub
                            End If
                        Else
                            Exit Sub
                        End If
                End Select
iValid:         If Validation() = False Then Exit Sub
                If (Is_Already_Assessed() = False) Then
                    Exit Sub
                End If

                'Shani (23-Nov-2016) -- Start
                'Enhancement - Add New PA Enhancement (Agreed Score,exc) Given by Akiba,KBC,TNP exc...
                'Call Evaluated_Data_BSC(iDecVal, dgvBSC.CurrentRow.Cells(idgvRemark.Index).Value.ToString)
                If menAssess = enAssessmentMode.APPRAISER_ASSESSMENT Then
                    Dim iVal As Decimal = 0
                    If dgvBSC.CurrentCell.ColumnIndex = dgcolhAgreedScoreBSC.Index Then
                        Decimal.TryParse(dgvBSC.CurrentRow.Cells(idgvResult.Index).Value, iVal)
                        Call Evaluated_Data_BSC(iVal, dgvBSC.CurrentRow.Cells(idgvRemark.Index).Value.ToString, , iDecVal)
                    Else
                        Decimal.TryParse(dgvBSC.CurrentRow.Cells(dgcolhAgreedScoreBSC.Index).Value, iVal)
                        Call Evaluated_Data_BSC(iDecVal, dgvBSC.CurrentRow.Cells(idgvRemark.Index).Value.ToString, , iVal)
                    End If
                Else
                    Call Evaluated_Data_BSC(iDecVal, dgvBSC.CurrentRow.Cells(idgvRemark.Index).Value.ToString)
                End If
                'Shani (23-Nov-2016) -- End
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "bsctxt_TextChanged", mstrModuleName)
        Finally

        End Try
    End Sub

    Private Sub dgvBSC_EditingControlShowing(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewEditingControlShowingEventArgs) Handles dgvBSC.EditingControlShowing
        Dim bsctxt As TextBox = Nothing
        Try
            Dim idvgCol As DataGridViewColumn = Nothing
            If menAssess = enAssessmentMode.SELF_ASSESSMENT Then
                idvgCol = dgcolheselfBSC
            ElseIf menAssess = enAssessmentMode.APPRAISER_ASSESSMENT Then
                If dgvBSC.CurrentCell.ColumnIndex = dgcolhAgreedScoreBSC.Index Then
                    idvgCol = dgcolhAgreedScoreBSC
                Else
                    idvgCol = dgcolhaselfBSC
                End If

            ElseIf menAssess = enAssessmentMode.REVIEWER_ASSESSMENT Then
                idvgCol = dgcolhrselfBSC
            End If
            If dgvBSC.CurrentCell.ColumnIndex = idvgCol.Index AndAlso TypeOf e.Control Is TextBox Then
                bsctxt = CType(e.Control, TextBox)
                RemoveHandler bsctxt.TextChanged, AddressOf bsctxt_TextChanged
                AddHandler bsctxt.TextChanged, AddressOf bsctxt_TextChanged
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgvBSC_EditingControlShowing", mstrModuleName)
        Finally
        End Try
    End Sub
    'S.SANDEEP [23 APR 2015] -- END


    '----------------------------------------------- GENERAL/COMPETENCIES EVALUATION

    Private Sub dgvGE_DataError(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewDataErrorEventArgs) Handles dgvGE.DataError

    End Sub

    Private Sub dgvGE_DataBindingComplete(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewBindingCompleteEventArgs) Handles dgvGE.DataBindingComplete
        Try
            'If menAction = enAction.EDIT_ONE Then
            Call Set_Grid_Style_GE()
            'End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgvGE_DataBindingComplete", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub dgvGE_CellEnter(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvGE.CellEnter
        Try
            If menAssess = enAssessmentMode.SELF_ASSESSMENT Then
                If e.ColumnIndex = dgcolheselfGE.Index Or e.ColumnIndex = dgcolheremarkGE.Index Then
                    SendKeys.Send("{F2}")
                End If
            ElseIf menAssess = enAssessmentMode.APPRAISER_ASSESSMENT Then

                'Shani (23-Nov-2016) -- Start
                'Enhancement - Add New PA Enhancement (Agreed Score,exc) Given by Akiba,KBC,TNP exc...
                'If e.ColumnIndex = dgcolhaselfGE.Index Or e.ColumnIndex = dgcolharemarkGE.Index Then
                If e.ColumnIndex = dgcolhaselfGE.Index Or e.ColumnIndex = dgcolharemarkGE.Index Or e.ColumnIndex = dgcolhaAgreedScoreGE.Index Then
                    'Shani (23-Nov-2016) -- End
                    SendKeys.Send("{F2}")
                End If
            ElseIf menAssess = enAssessmentMode.REVIEWER_ASSESSMENT Then
                If e.ColumnIndex = dgcolhrselfGE.Index Or e.ColumnIndex = dgcolhrremarkGE.Index Then
                    SendKeys.Send("{F2}")
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgvGE_CellEnter", mstrModuleName)
        End Try
    End Sub

    Private Sub dgvGE_CellContentClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvGE.CellContentClick, dgvGE.CellContentDoubleClick
        Try
            Dim i As Integer
            If e.RowIndex = -1 Then Exit Sub

            If Me.dgvGE.IsCurrentCellDirty Then
                Me.dgvGE.CommitEdit(DataGridViewDataErrorContexts.Commit)
            End If

            If CBool(dgvGE.Rows(e.RowIndex).Cells(objdgcolhIsPGrpGE.Index).Value) = True Then
                dgvGE.Rows(e.RowIndex).Selected = False
            End If

            If CBool(dgvGE.Rows(e.RowIndex).Cells(objdgcolhIsGrpGE.Index).Value) = True Then
                Select Case CInt(e.ColumnIndex)
                    Case objdgcolhGECollapse.Index
                        If dgvGE.Rows(e.RowIndex).Cells(objdgcolhGECollapse.Index).Value Is "-" Then
                            dgvGE.Rows(e.RowIndex).Cells(objdgcolhGECollapse.Index).Value = "+"
                        Else
                            dgvGE.Rows(e.RowIndex).Cells(objdgcolhGECollapse.Index).Value = "-"
                        End If

                        For i = e.RowIndex + 1 To dgvGE.RowCount - 1
                            If CInt(dgvGE.Rows(e.RowIndex).Cells(objdgcolhGrpIdGE.Index).Value) = CInt(dgvGE.Rows(i).Cells(objdgcolhGrpIdGE.Index).Value) Then
                                If dgvGE.Rows(i).Visible = False Then
                                    dgvGE.Rows(i).Visible = True
                                Else
                                    dgvGE.Rows(i).Visible = False
                                End If
                            Else
                                Exit For
                            End If
                        Next
                        'S.SANDEEP [31 AUG 2016] -- START
                        'ENHANCEMENT : INCLUSION OF GRADES ON ASSESSMENT GROUP {BY ANDREW}
                    Case objdgcolhInfo.Index
                        Dim objCMaster As New clsCommon_Master
                        objCMaster._Masterunkid = CInt(dgvGE.Rows(e.RowIndex).Cells(objdgcolhGrpIdGE.Index).Value)
                        txtDescription.Text = ""
                        txtDescription.Text = objCMaster._Description
                        objCMaster = Nothing

                        objgbDescription.Location = New Point(316, 147)
                        objgbDescription.BringToFront()
                        objgbDescription.Visible = True : dgvGE.Enabled = False
                        'S.SANDEEP [31 AUG 2016] -- START
                End Select
            Else
                Select Case CInt(e.ColumnIndex)
                    Case dgcolhGEScore.Index
                        If CInt(dgvGE.Rows(e.RowIndex).Cells(objdgcolhscalemasterunkidGE.Index).Value) > 0 Then
                            Dim dsScore_Guide As New DataSet
                            Dim objScaleMaster As New clsAssessment_Scale
                            dsScore_Guide = objScaleMaster.GetList("List", CInt(cboPeriod.SelectedValue), CInt(dgvGE.Rows(e.RowIndex).Cells(objdgcolhscalemasterunkidGE.Index).Value))
                            objScaleMaster = Nothing
                            lvScaleList.Items.Clear()
                            For Each drow As DataRow In dsScore_Guide.Tables(0).Rows
                                Dim lvitem As New ListViewItem
                                lvitem.Text = drow.Item("scale").ToString
                                lvitem.SubItems.Add(drow.Item("description").ToString)
                                lvScaleList.Items.Add(lvitem)
                            Next
                            If lvScaleList.Items.Count >= 9 Then
                                colhDescription.Width = 395 - 20
                            Else
                                colhDescription.Width = 395
                            End If
                            objgbScoreGuide.Location = New Point(316, 147)
                            objgbScoreGuide.BringToFront()
                            objgbScoreGuide.Visible = True : dgvGE.Enabled = False
                        Else
                            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 26, "Sorry no score guide defined for the selected item."), enMsgBoxStyle.Information)
                            Exit Sub
                        End If
                        'S.SANDEEP [31 AUG 2016] -- START
                        'ENHANCEMENT : INCLUSION OF GRADES ON ASSESSMENT GROUP {BY ANDREW}
                    Case objdgcolhInfo.Index
                        If dgvGE.Rows(e.RowIndex).Cells(objdgcolhInfo.Index).Value IsNot imgBlankIcon Then
                            Dim objCmpMst As New clsassess_competencies_master
                            objCmpMst._Competenciesunkid = CInt(dgvGE.Rows(e.RowIndex).Cells(objdgcolhcompetenciesunkidGE.Index).Value)
                            txtDescription.Text = ""
                            txtDescription.Text = objCmpMst._Description
                            objCmpMst = Nothing

                            objgbDescription.Location = New Point(316, 147)
                            objgbDescription.BringToFront()
                            objgbDescription.Visible = True : dgvGE.Enabled = False
                        End If
                        'S.SANDEEP [31 AUG 2016] -- START
                End Select
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgvGE_CellContentClick", mstrModuleName)
        Finally
        End Try
    End Sub

    'S.SANDEEP [23 APR 2015] -- START

    '    Private Sub dgvGE_CellValueChanged(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvGE.CellValueChanged
    '        Try
    '            If mblIsFormLoad = False Then Exit Sub

    '            If e.RowIndex <= -1 Then Exit Sub
    '            Dim idgvResult, idgvRemark As DataGridViewColumn
    '            idgvResult = Nothing : idgvRemark = Nothing
    '            If menAssess = enAssessmentMode.SELF_ASSESSMENT Then
    '                idgvResult = dgcolheselfGE
    '                idgvRemark = dgcolheremarkGE
    '            ElseIf menAssess = enAssessmentMode.APPRAISER_ASSESSMENT Then
    '                idgvResult = dgcolhaselfGE
    '                idgvRemark = dgcolharemarkGE
    '            ElseIf menAssess = enAssessmentMode.REVIEWER_ASSESSMENT Then
    '                idgvResult = dgcolhrselfGE
    '                idgvRemark = dgcolhrremarkGE
    '            End If

    '            'S.SANDEEP [21 JAN 2015] -- START
    '            If IsNumeric(dgvGE.Rows(e.RowIndex).Cells(dgcolhGEWeight.Index).Value) Then
    '                mdecItemWeight = CDec(dgvGE.Rows(e.RowIndex).Cells(dgcolhGEWeight.Index).Value)
    '            End If
    '            'S.SANDEEP [21 JAN 2015] -- END

    '            If e.ColumnIndex = idgvRemark.Index Then
    '                If dgvGE.Rows(e.RowIndex).Cells(idgvRemark.Index).Value.ToString.Trim.Length > 0 Then
    '                    'S.SANDEEP [21 JAN 2015] -- START
    '                    'Call Evaluated_Data_GE(Convert.ToDecimal(dgvGE.Rows(e.RowIndex).Cells(idgvResult.Index).Value), CStr(dgvGE.Rows(e.RowIndex).Cells(idgvRemark.Index).Value))
    '                    Call Evaluated_Data_GE(Convert.ToDecimal(dgvGE.Rows(e.RowIndex).Cells(idgvResult.Index).Value), CStr(dgvGE.Rows(e.RowIndex).Cells(idgvRemark.Index).Value), True)
    '                    'S.SANDEEP [21 JAN 2015] -- END
    '                End If
    '            ElseIf e.ColumnIndex = idgvResult.Index Then
    '                Dim iDecVal As Decimal
    '                Decimal.TryParse(dgvGE.Rows(e.RowIndex).Cells(idgvResult.Index).Value.ToString, iDecVal)
    '                If IsNumeric(iDecVal) Then
    '                    Select Case CInt(ConfigParameter._Object._ScoringOptionId)
    '                        Case enScoringOption.SC_WEIGHTED_BASED
    '                            'S.SANDEEP [ 17 DEC 2014 ] -- START
    '                            'If CDec(iDecVal) > CDec(dgvGE.CurrentRow.Cells(dgcolhGEWeight.Index).Value) Then
    '                            '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 10, "Sorry, you cannot set more result value than assigned weight."), enMsgBoxStyle.Information)
    '                            '    dgvGE.Rows(e.RowIndex).Cells(idgvResult.Index).Value = "0" : Exit Sub
    '                            'Else
    '                            '    GoTo iValid
    '                            'End If
    '                            If dgvGE.CurrentRow Is Nothing Then Exit Sub
    '                            If CBool(dgvGE.Rows(e.RowIndex).Cells(objdgcolhIsPGrpGE.Index).Value) = True Or CBool(dgvGE.Rows(e.RowIndex).Cells(objdgcolhIsGrpGE.Index).Value) = True Then Exit Sub
    '                            If CDec(iDecVal) > CDec(dgvGE.Rows(e.RowIndex).Cells(dgcolhGEWeight.Index).Value) Then
    '                                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 10, "Sorry, you cannot set more result value than assigned weight."), enMsgBoxStyle.Information)
    '                                Dim xtmp() As DataRow = Nothing
    '                                xtmp = Get_Old_Data_GE()
    '                                If xtmp IsNot Nothing AndAlso xtmp.Length > 0 Then
    '                                    dgvGE.Rows(e.RowIndex).Cells(idgvResult.Index).Value = xtmp(0)("result")
    '                                Else
    '                                    dgvGE.Rows(e.RowIndex).Cells(idgvResult.Index).Value = "0"
    '                                End If
    '                                Exit Sub
    '                            Else
    '                                GoTo iValid
    '                            End If
    '                            'S.SANDEEP [ 17 DEC 2014 ] -- END
    '                        Case enScoringOption.SC_SCALE_BASED
    '                            If CBool(dgvGE.Rows(e.RowIndex).Cells(objdgcolhIsPGrpGE.Index).Value) = True Or CBool(dgvGE.Rows(e.RowIndex).Cells(objdgcolhIsGrpGE.Index).Value) = True Then Exit Sub
    '                            Decimal.TryParse(dgvGE.Rows(e.RowIndex).Cells(idgvResult.Index).Value.ToString, iDecVal)
    '                            If IsNumeric(iDecVal) Then
    '                                If CInt(dgvGE.Rows(e.RowIndex).Cells(objdgcolhscalemasterunkidGE.Index).Value) > 0 Then
    '                                    Dim dsScore_Guide As New DataSet
    '                                    Dim objScaleMaster As New clsAssessment_Scale
    '                                    dsScore_Guide = objScaleMaster.GetList("List", CInt(cboPeriod.SelectedValue), CInt(dgvGE.Rows(e.RowIndex).Cells(objdgcolhscalemasterunkidGE.Index).Value))
    '                                    objScaleMaster = Nothing

    '                                    'S.SANDEEP [21 JAN 2015] -- START
    '                                    mdecMaxScale = dsScore_Guide.Tables(0).Compute("MAX(scale)", "")
    '                                    'S.SANDEEP [21 JAN 2015] -- END

    '                                    Dim dtmp() As DataRow = dsScore_Guide.Tables(0).Select("scale = " & CDec(iDecVal))
    '                                    If dtmp.Length <= 0 Then
    '                                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 28, "Sorry, particular scale is not defined. Please refer score guide for the list of score(s) defined."), enMsgBoxStyle.Information)
    '                                        Dim xtmp() As DataRow = Nothing
    '                                        xtmp = Get_Old_Data_GE()
    '                                        If xtmp IsNot Nothing AndAlso xtmp.Length > 0 Then
    '                                            dgvGE.Rows(e.RowIndex).Cells(idgvResult.Index).Value = xtmp(0)("result")
    '                                        End If
    '                                        Exit Sub
    '                                    Else
    '                                        GoTo iValid
    '                                    End If
    '                                Else
    '                                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 29, "Sorry, no scale is not defined. Please define scale."), enMsgBoxStyle.Information)
    '                                    'Dim xtmp() As DataRow = Nothing
    '                                    'xtmp = Get_Old_Data_GE()
    '                                    'If xtmp IsNot Nothing AndAlso xtmp.Length > 0 Then
    '                                    '    dgvGE.Rows(e.RowIndex).Cells(idgvResult.Index).Value = xtmp(0)("result")
    '                                    'Else
    '                                    '    dgvGE.Rows(e.RowIndex).Cells(idgvResult.Index).Value = "0"
    '                                    'End If
    '                                    Exit Sub
    '                                End If
    '                            Else
    '                                Exit Sub
    '                            End If
    '                    End Select
    'iValid:             If Validation() = False Then Exit Sub
    '                    'S.SANDEEP [ 01 JAN 2015 ] -- START
    '                    'If Is_Already_Assessed() = False Then
    '                    '    Exit Sub
    '                    'End If

    '                    If (Is_Already_Assessed() = False) Then
    '                        Exit Sub
    '                    End If
    '                    'S.SANDEEP [ 01 JAN 2015 ] -- END
    '                    Call Evaluated_Data_GE(iDecVal, dgvGE.CurrentRow.Cells(idgvRemark.Index).Value.ToString)
    '                End If
    '            End If
    '        Catch ex As Exception
    '            DisplayError.Show("-1", ex.Message, "dgvGE_CellValueChanged", mstrModuleName)
    '        Finally
    '        End Try
    '    End Sub

    Private Sub getxt_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Try
            Dim iText As TextBox = Nothing : iText = CType(sender, TextBox)

            'S.SANDEEP [03 SEP 2015] -- START
            If iText.Text.Trim.Length <= 0 Then Exit Sub
            'S.SANDEEP [03 SEP 2015] -- END

            Dim iDecVal As Decimal : Decimal.TryParse(iText.Text, iDecVal)
            Dim idgvResult, idgvRemark As DataGridViewColumn

            idgvResult = Nothing : idgvRemark = Nothing

            If menAssess = enAssessmentMode.SELF_ASSESSMENT Then
                idgvResult = dgcolheselfGE
                idgvRemark = dgcolheremarkGE
            ElseIf menAssess = enAssessmentMode.APPRAISER_ASSESSMENT Then
                idgvResult = dgcolhaselfGE
                idgvRemark = dgcolharemarkGE
            ElseIf menAssess = enAssessmentMode.REVIEWER_ASSESSMENT Then
                idgvResult = dgcolhrselfGE
                idgvRemark = dgcolhrremarkGE
            End If

            'S.SANDEEP [03 SEP 2015] -- START
            'RemoveHandler iText.TextChanged, AddressOf getxt_TextChanged
            'S.SANDEEP [03 SEP 2015] -- END


            If IsNumeric(dgvGE.CurrentRow.Cells(dgcolhGEWeight.Index).Value) Then
                mdecItemWeight = CDec(dgvGE.CurrentRow.Cells(dgcolhGEWeight.Index).Value)
            End If

            If IsNumeric(iDecVal) Then
                Select Case CInt(ConfigParameter._Object._ScoringOptionId)
                    Case enScoringOption.SC_WEIGHTED_BASED
                        If dgvGE.CurrentRow Is Nothing Then Exit Sub
                        If CBool(dgvGE.CurrentRow.Cells(objdgcolhIsPGrpGE.Index).Value) = True Or CBool(dgvGE.CurrentRow.Cells(objdgcolhIsGrpGE.Index).Value) = True Then Exit Sub
                        If CDec(iDecVal) > CDec(dgvGE.CurrentRow.Cells(dgcolhGEWeight.Index).Value) Then
                            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 10, "Sorry, you cannot set more result value than assigned weight."), enMsgBoxStyle.Information)
                            Dim xtmp() As DataRow = Nothing
                            xtmp = Get_Old_Data_GE()
                            If xtmp IsNot Nothing AndAlso xtmp.Length > 0 Then

                                'Shani (23-Nov-2016) -- Start
                                'Enhancement - Add New PA Enhancement (Agreed Score,exc) Given by Akiba,KBC,TNP exc...
                                'dgvGE.CurrentRow.Cells(idgvResult.Index).Value = xtmp(0)("result")
                                If menAssess = enAssessmentMode.APPRAISER_ASSESSMENT Then
                                    If dgvBSC.CurrentCell.ColumnIndex = dgcolhaAgreedScoreGE.Index Then
                                        dgvGE.CurrentRow.Cells(dgcolhaAgreedScoreGE.Index).Value = xtmp(0)("agreedscore")
                                    Else
                                        dgvGE.CurrentRow.Cells(idgvResult.Index).Value = xtmp(0)("result")
                                    End If
                                Else
                                    dgvGE.CurrentRow.Cells(idgvResult.Index).Value = xtmp(0)("result")
                                End If
                                'Shani (23-Nov-2016) -- End
                            Else
                                'RemoveHandler iText.TextChanged, AddressOf getxt_TextChanged
                                iText.Text = ""
                                'AddHandler iText.TextChanged, AddressOf getxt_TextChanged
                            End If
                            Exit Sub
                        Else
                            GoTo iValid
                        End If
                    Case enScoringOption.SC_SCALE_BASED
                        If CBool(dgvGE.CurrentRow.Cells(objdgcolhIsPGrpGE.Index).Value) = True Or CBool(dgvGE.CurrentRow.Cells(objdgcolhIsGrpGE.Index).Value) = True Then Exit Sub

                        'Shani (23-Nov-2016) -- Start
                        'Enhancement - Add New PA Enhancement (Agreed Score,exc) Given by Akiba,KBC,TNP exc...
                        'If IsNumeric(iDecVal) Then

                        'S.SANDEEP [07-APR-2017] -- START
                        'ISSUE/ENHANCEMENT : Allowed Zero Score is Removed (BY Mistake)
                        'If IsNumeric(iDecVal) AndAlso iDecVal > 0 Then
                        If IsNumeric(iDecVal) Then
                            'S.SANDEEP [07-APR-2017] -- END
                            'Shani (23-Nov-2016) -- End
                            If CInt(dgvGE.CurrentRow.Cells(objdgcolhscalemasterunkidGE.Index).Value) > 0 Then
                                Dim dsScore_Guide As New DataSet
                                Dim objScaleMaster As New clsAssessment_Scale
                                dsScore_Guide = objScaleMaster.GetList("List", CInt(cboPeriod.SelectedValue), CInt(dgvGE.CurrentRow.Cells(objdgcolhscalemasterunkidGE.Index).Value))
                                objScaleMaster = Nothing

                                'Shani (23-Nov-2016) -- Start
                                'Enhancement - Add New PA Enhancement (Agreed Score,exc) Given by Akiba,KBC,TNP exc...
                                ' mdecMaxScale = dsScore_Guide.Tables(0).Compute("MAX(scale)", "")
                                Try
                                    mdecMaxScale = dsScore_Guide.Tables(0).Compute("MAX(scale)", "")
                                Catch ex As Exception
                                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 41, "No scale define in the system"), enMsgBoxStyle.Information)
                                    Exit Sub
                                End Try
                                'Shani (23-Nov-2016) -- End

                                Dim dtmp() As DataRow = dsScore_Guide.Tables(0).Select("scale = " & CDec(iDecVal))
                                If dtmp.Length <= 0 Then
                                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 28, "Sorry, particular scale is not defined. Please refer score guide for the list of score(s) defined."), enMsgBoxStyle.Information)
                                    Dim xtmp() As DataRow = Nothing
                                    xtmp = Get_Old_Data_GE()
                                    If xtmp IsNot Nothing AndAlso xtmp.Length > 0 Then

                                        'Shani (23-Nov-2016) -- Start
                                        'Enhancement - Add New PA Enhancement (Agreed Score,exc) Given by Akiba,KBC,TNP exc...
                                        'dgvGE.CurrentRow.Cells(idgvResult.Index).Value = xtmp(0)("result")
                                        If menAssess = enAssessmentMode.APPRAISER_ASSESSMENT Then
                                            If dgvGE.CurrentCell.ColumnIndex = dgcolhaAgreedScoreGE.Index Then
                                                dgvGE.CurrentRow.Cells(dgcolhaAgreedScoreGE.Index).Value = xtmp(0)("result")
                                            Else
                                                dgvGE.CurrentRow.Cells(idgvResult.Index).Value = xtmp(0)("result")
                                            End If
                                        Else
                                            dgvGE.CurrentRow.Cells(idgvResult.Index).Value = xtmp(0)("result")
                                        End If
                                        'Shani (23-Nov-2016) -- End
                                    Else
                                        'RemoveHandler iText.TextChanged, AddressOf getxt_TextChanged
                                        iText.Text = ""
                                        'AddHandler iText.TextChanged, AddressOf getxt_TextChanged
                                    End If
                                    Exit Sub
                                Else
                                    GoTo iValid
                                End If
                            Else
                                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 29, "Sorry, scale is not defined. Please define scale."), enMsgBoxStyle.Information)
                                Exit Sub
                            End If
                        Else
                            Exit Sub
                        End If
                End Select
iValid:         If Validation() = False Then Exit Sub
                If (Is_Already_Assessed() = False) Then
                    Exit Sub
                End If

                'Shani (23-Nov-2016) -- Start
                'Enhancement - Add New PA Enhancement (Agreed Score,exc) Given by Akiba,KBC,TNP exc...
                'Call Evaluated_Data_GE(iDecVal, dgvGE.CurrentRow.Cells(idgvRemark.Index).Value.ToString)
                If menAssess = enAssessmentMode.APPRAISER_ASSESSMENT Then
                    Dim iVal As Decimal = 0
                    If dgvGE.CurrentCell.ColumnIndex = dgcolhaAgreedScoreGE.Index Then
                        Decimal.TryParse(dgvGE.CurrentRow.Cells(idgvResult.Index).Value, iVal)
                        Call Evaluated_Data_GE(iVal, dgvGE.CurrentRow.Cells(idgvRemark.Index).Value.ToString, , iDecVal)
                    Else
                        Decimal.TryParse(dgvGE.CurrentRow.Cells(dgcolhaAgreedScoreGE.Index).Value, iVal)
                        Call Evaluated_Data_GE(iDecVal, dgvGE.CurrentRow.Cells(idgvRemark.Index).Value.ToString, , iVal)
                    End If
                Else
                    Call Evaluated_Data_GE(iDecVal, dgvGE.CurrentRow.Cells(idgvRemark.Index).Value.ToString)
                End If
                'Shani (23-Nov-2016) -- End
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "getxt_TextChanged", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub dgvGE_CellValidating(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellValidatingEventArgs) Handles dgvGE.CellValidating
        Try
            If mblIsFormLoad = False Then Exit Sub

            If e.RowIndex <= -1 Then Exit Sub
            Dim idgvResult, idgvRemark, idgvAgreedScore As DataGridViewColumn 'S.SANDEEP |31-MAY-2019| -- START [PA SETTING CHANGES]   -- END
            idgvResult = Nothing : idgvRemark = Nothing : idgvAgreedScore = Nothing 'S.SANDEEP |31-MAY-2019| -- START [PA SETTING CHANGES]   -- END
            If menAssess = enAssessmentMode.SELF_ASSESSMENT Then
                idgvResult = dgcolheselfGE
                idgvRemark = dgcolheremarkGE
            ElseIf menAssess = enAssessmentMode.APPRAISER_ASSESSMENT Then
                idgvResult = dgcolhaselfGE
                idgvRemark = dgcolharemarkGE
                'S.SANDEEP |31-MAY-2019| -- START
                'ISSUE/ENHANCEMENT : [PA SETTING CHANGES]
                If ConfigParameter._Object._IsUseAgreedScore Then
                    idgvAgreedScore = dgcolhaAgreedScoreGE
                End If
                'S.SANDEEP |31-MAY-2019| -- END

            ElseIf menAssess = enAssessmentMode.REVIEWER_ASSESSMENT Then
                idgvResult = dgcolhrselfGE
                idgvRemark = dgcolhrremarkGE
            End If

            If IsNumeric(dgvGE.Rows(e.RowIndex).Cells(dgcolhGEWeight.Index).Value) Then
                mdecItemWeight = CDec(dgvGE.Rows(e.RowIndex).Cells(dgcolhGEWeight.Index).Value)
            End If

            'S.SANDEEP |31-MAY-2019| -- START
            'ISSUE/ENHANCEMENT : [PA SETTING CHANGES]
            'If dgvBSC.IsCurrentCellDirty Then
            'dgvBSC.CommitEdit(DataGridViewDataErrorContexts.Commit)
            'End If
            If dgvGE.IsCurrentCellDirty Then
                dgvGE.CommitEdit(DataGridViewDataErrorContexts.Commit)
            End If
            'S.SANDEEP |31-MAY-2019| -- END


            If e.ColumnIndex = idgvRemark.Index Then
                If dgvGE.Rows(e.RowIndex).Cells(idgvRemark.Index).Value.ToString.Trim.Length > 0 Then
                    'S.SANDEEP |31-MAY-2019| -- START
                    'ISSUE/ENHANCEMENT : [PA SETTING CHANGES]
                    If idgvAgreedScore IsNot Nothing Then
                        If IsNumeric(dgvGE.Rows(e.RowIndex).Cells(idgvAgreedScore.Index).Value) Then
                            Call Evaluated_Data_GE(Convert.ToDecimal(dgvGE.Rows(e.RowIndex).Cells(idgvResult.Index).Value), CStr(dgvGE.Rows(e.RowIndex).Cells(idgvRemark.Index).Value), True, Convert.ToDecimal(dgvGE.Rows(e.RowIndex).Cells(idgvAgreedScore.Index).Value))
                        Else
                            Call Evaluated_Data_GE(Convert.ToDecimal(dgvGE.Rows(e.RowIndex).Cells(idgvResult.Index).Value), CStr(dgvGE.Rows(e.RowIndex).Cells(idgvRemark.Index).Value), True, 0)
                        End If
                    Else
                        If idgvResult.Visible = True Then
                            Call Evaluated_Data_GE(Convert.ToDecimal(dgvGE.Rows(e.RowIndex).Cells(idgvResult.Index).Value), CStr(dgvGE.Rows(e.RowIndex).Cells(idgvRemark.Index).Value), True)
                        Else
                            Call Evaluated_Data_GE(0, CStr(dgvGE.Rows(e.RowIndex).Cells(idgvRemark.Index).Value), True)
                        End If
                    End If
                    'S.SANDEEP |31-MAY-2019| -- END
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgvGE_CellValidating", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub dgvGE_EditingControlShowing(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewEditingControlShowingEventArgs) Handles dgvGE.EditingControlShowing
        Dim getxt As TextBox = Nothing
        Try
            Dim idvgCol As DataGridViewColumn = Nothing
            If menAssess = enAssessmentMode.SELF_ASSESSMENT Then
                idvgCol = dgcolheselfGE
            ElseIf menAssess = enAssessmentMode.APPRAISER_ASSESSMENT Then

                'Shani (23-Nov-2016) -- Start
                'Enhancement - Add New PA Enhancement (Agreed Score,exc) Given by Akiba,KBC,TNP exc...
                'idvgCol = dgcolhaselfGE
                If dgvGE.CurrentCell.ColumnIndex = dgcolhaAgreedScoreGE.Index Then
                    idvgCol = dgcolhaAgreedScoreGE
                Else
                    idvgCol = dgcolhaselfGE
                End If
                'Shani (23-Nov-2016) -- End

            ElseIf menAssess = enAssessmentMode.REVIEWER_ASSESSMENT Then
                idvgCol = dgcolhrselfGE
            End If
            If dgvGE.CurrentCell.ColumnIndex = idvgCol.Index AndAlso TypeOf e.Control Is TextBox Then
                getxt = CType(e.Control, TextBox)
                RemoveHandler getxt.TextChanged, AddressOf getxt_TextChanged
                AddHandler getxt.TextChanged, AddressOf getxt_TextChanged
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgvGE_EditingControlShowing", mstrModuleName)
        Finally
        End Try
    End Sub
    'S.SANDEEP [23 APR 2015] -- END




    '----------------------------------------------- CUSTOM ITEMS EVALUATION
    Private Sub dgvItems_DataBindingComplete(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewBindingCompleteEventArgs) Handles dgvItems.DataBindingComplete
        Try
            For Each dgvRow As DataGridViewRow In dgvItems.Rows
                If CInt(dgvRow.Cells("objanalysisunkid").Value) > 0 Then
                    If CInt(dgvRow.Cells("objanalysisunkid").Value) <> mintAssessAnalysisUnkid Then
                        dgvRow.Cells(objdgcolhDelete.Index).Value = img
                        Dim objEMaster As New clsevaluation_analysis_master
                        objEMaster._Analysisunkid = CInt(dgvRow.Cells("objanalysisunkid").Value)
                        If menAssess < objEMaster._Assessmodeid Then
                            dgvRow.Cells(objdgcolhEdit.Index).Value = img
                        End If
                        objEMaster = Nothing
                    End If
                Else
                    dgvRow.Cells(objdgcolhAdd.Index).Value = My.Resources.add_16
                    dgvRow.Cells(objdgcolhDelete.Index).Value = My.Resources.remove
                End If
            Next
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgvItems_DataBindingComplete", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub dgvItems_CellContentClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvItems.CellContentClick, dgvItems.CellContentDoubleClick
        Dim frm As New frmAddCustomValue
        Try

            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If

            If e.RowIndex = -1 Then Exit Sub

            If Me.dgvItems.IsCurrentCellDirty Then
                Me.dgvItems.CommitEdit(DataGridViewDataErrorContexts.Commit)
            End If
            Select Case e.ColumnIndex
                Case objdgcolhAdd.Index
                    If mdtCustomEvaluation IsNot Nothing AndAlso mdtCustomEvaluation.Rows.Count > 0 Then
                        Dim xRow() As DataRow = mdtCustomEvaluation.Select("custom_header = '" & dgvItems.Rows(e.RowIndex).Cells("objHeader_Name").Value & "' AND AUD <> 'D'")
                        If xRow.Length > 0 AndAlso CBool(dgvItems.Rows(e.RowIndex).Cells("objIs_Allow_Multiple").Value) = False Then
                            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 33, "Sorry, this particular custom header is not set for allow multiple entries when defined."), enMsgBoxStyle.Information)
                            Exit Sub
                        End If
                    End If
                    If frm.displayDialog(mintAssessAnalysisUnkid, CInt(cboPeriod.SelectedValue), _
                                         objEAnalysisMst, dgvItems.Rows(e.RowIndex).Cells("objHeader_Id").Value, _
                                         dgvItems.Rows(e.RowIndex).Cells("objHeader_Name").Value, mdtCustomEvaluation, _
                                         enAction.ADD_ONE, CInt(cboEmployee.SelectedValue), dgvItems.Rows(e.RowIndex).Cells("objIs_Allow_Multiple").Value, dgvItems.Rows(e.RowIndex).Cells("objGUID").Value, menAssess, dtpAssessdate.Value.Date) Then

                        'Shani (26-Sep-2016) -- [dgvItems.Rows(e.RowIndex).Cells("objGUID").Value]
                        'SHANI [21 MAR 2015]-[menAssess,dtpAssessdate.Value.Date]

                        Call Fill_Custom_Evaluation_Data()
                    End If
                Case objdgcolhEdit.Index
                    'S.SANDEEP |30-MAR-2019| -- START
                    'If dgvItems.Rows(e.RowIndex).Cells("objGUID").Value.ToString.Length <= 0 Then Exit Sub
                    If dgvItems.Rows(e.RowIndex).Cells("objGUID").Value.ToString.Length <= 0 Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 57, "Sorry, No comment(s) has been added in order to edit."), enMsgBoxStyle.Information)
                        Exit Sub
                    End If
                    'S.SANDEEP |30-MAR-2019| -- END
                    If frm.displayDialog(CInt(IIf(mintAssessAnalysisUnkid <= 0, dgvItems.Rows(e.RowIndex).Cells("objanalysisunkid").Value, mintAssessAnalysisUnkid)), CInt(cboPeriod.SelectedValue), _
                                         objEAnalysisMst, dgvItems.Rows(e.RowIndex).Cells("objHeader_Id").Value, _
                                         dgvItems.Rows(e.RowIndex).Cells("objHeader_Name").Value, mdtCustomEvaluation, _
                                         enAction.EDIT_ONE, CInt(cboEmployee.SelectedValue), dgvItems.Rows(e.RowIndex).Cells("objIs_Allow_Multiple").Value, _
                                         dgvItems.Rows(e.RowIndex).Cells("objGUID").Value, menAssess, dtpAssessdate.Value.Date) Then 'SHANI [21 MAR 2015]-[dtpAssessdate.Value.Date]
                        Call Fill_Custom_Evaluation_Data()
                    End If
                Case objdgcolhDelete.Index
                    If mdtCustomEvaluation IsNot Nothing AndAlso mdtCustomEvaluation.Rows.Count > 0 Then
                        Dim xRow() As DataRow = Nothing

                        If CBool(dgvItems.Rows(e.RowIndex).Cells("objismanual").Value) Then
                            xRow = mdtCustomEvaluation.Select("customanalysistranguid = '" & dgvItems.Rows(e.RowIndex).Cells("objGUID").Value.ToString & "' AND AUD <> 'D'")
                        Else
                            xRow = mdtCustomEvaluation.Select("customanalysistranguid = '" & dgvItems.Rows(e.RowIndex).Cells("objGUID").Value.ToString & "' AND AUD <> 'D' AND isdefaultentry = 'False' ")
                        End If

                        If xRow.Length > 0 Then
                            Dim ofrm As New frmReasonSelection

                            If User._Object._Isrighttoleft = True Then
                                ofrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                                ofrm.RightToLeftLayout = True
                                Call Language.ctlRightToLeftlayOut(ofrm)
                            End If

                            Dim iVoidReason As String = String.Empty
                            ofrm.displayDialog(enVoidCategoryType.ASSESSMENT, iVoidReason)

                            If iVoidReason.Trim.Length <= 0 Then
                                frm.Dispose()
                                Exit Sub
                            End If
                            If frm IsNot Nothing Then frm.Dispose()

                            For x As Integer = 0 To xRow.Length - 1
                                'Shani (26-Sep-2016) -- Start
                                'Enhancement -Changes PA Flow And Add New screen Given By (Andrew)
                                'If CBool(xRow(x).Item("isdefaultentry")) = True Then Continue For
                                'Shani (26-Sep-2016) -- End
                                xRow(x).Item("isvoid") = True
                                xRow(x).Item("voiduserunkid") = User._Object._Userunkid
                                xRow(x).Item("voiddatetime") = ConfigParameter._Object._CurrentDateAndTime
                                xRow(x).Item("voidreason") = iVoidReason
                                xRow(x).Item("AUD") = "D"
                            Next
                        End If
                        Call Fill_Custom_Evaluation_Data()
                    End If
            End Select
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgvItems_CellContentClick", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

#End Region

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Private Sub OtherSettings()
        Try
            Me.SuspendLayout()

            Call SetLanguage()

            Me.gbBSCEvaluation.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbBSCEvaluation.ForeColor = GUI._eZeeContainerHeaderForeColor

            Me.objgbScoreGuide.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.objgbScoreGuide.ForeColor = GUI._eZeeContainerHeaderForeColor

            Me.objgbDescription.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.objgbDescription.ForeColor = GUI._eZeeContainerHeaderForeColor

            Me.gbRemarks.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbRemarks.ForeColor = GUI._eZeeContainerHeaderForeColor


            Me.objbtnBack.GradientBackColor = GUI._ButttonBackColor
            Me.objbtnBack.GradientForeColor = GUI._ButttonFontColor

            Me.objbtnNext.GradientBackColor = GUI._ButttonBackColor
            Me.objbtnNext.GradientForeColor = GUI._ButttonFontColor

            Me.btnSaveCommit.GradientBackColor = GUI._ButttonBackColor
            Me.btnSaveCommit.GradientForeColor = GUI._ButttonFontColor

            Me.btnSave.GradientBackColor = GUI._ButttonBackColor
            Me.btnSave.GradientForeColor = GUI._ButttonFontColor

            Me.btnClose.GradientBackColor = GUI._ButttonBackColor
            Me.btnClose.GradientForeColor = GUI._ButttonFontColor


            Me.ResumeLayout()
        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "OtherSettings", mstrModuleName)
        End Try
    End Sub


    Private Sub SetLanguage()
        Try
            Me.Text = Language._Object.getCaption(Me.Name, Me.Text)

            Me.gbBSCEvaluation.Text = Language._Object.getCaption(Me.gbBSCEvaluation.Name, Me.gbBSCEvaluation.Text)
            Me.radExternalAssessor.Text = Language._Object.getCaption(Me.radExternalAssessor.Name, Me.radExternalAssessor.Text)
            Me.lblAssessor.Text = Language._Object.getCaption(Me.lblAssessor.Name, Me.lblAssessor.Text)
            Me.radInternalAssessor.Text = Language._Object.getCaption(Me.radInternalAssessor.Name, Me.radInternalAssessor.Text)
            Me.lblReviewer.Text = Language._Object.getCaption(Me.lblReviewer.Name, Me.lblReviewer.Text)
            Me.lblAssessDate.Text = Language._Object.getCaption(Me.lblAssessDate.Name, Me.lblAssessDate.Text)
            Me.lblEmployee.Text = Language._Object.getCaption(Me.lblEmployee.Name, Me.lblEmployee.Text)
            Me.lblPeriod.Text = Language._Object.getCaption(Me.lblPeriod.Name, Me.lblPeriod.Text)
            Me.btnSaveCommit.Text = Language._Object.getCaption(Me.btnSaveCommit.Name, Me.btnSaveCommit.Text)
            Me.btnSave.Text = Language._Object.getCaption(Me.btnSave.Name, Me.btnSave.Text)
            Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
            Me.colhScale.Text = Language._Object.getCaption(CStr(Me.colhScale.Tag), Me.colhScale.Text)
            Me.colhDescription.Text = Language._Object.getCaption(CStr(Me.colhDescription.Tag), Me.colhDescription.Text)
            Me.lnkCopyScore.Text = Language._Object.getCaption(Me.lnkCopyScore.Name, Me.lnkCopyScore.Text)
            Me.DataGridViewTextBoxColumn1.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn1.Name, Me.DataGridViewTextBoxColumn1.HeaderText)
            Me.DataGridViewTextBoxColumn2.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn2.Name, Me.DataGridViewTextBoxColumn2.HeaderText)
            Me.DataGridViewTextBoxColumn3.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn3.Name, Me.DataGridViewTextBoxColumn3.HeaderText)
            Me.DataGridViewTextBoxColumn4.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn4.Name, Me.DataGridViewTextBoxColumn4.HeaderText)
            Me.DataGridViewTextBoxColumn5.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn5.Name, Me.DataGridViewTextBoxColumn5.HeaderText)
            Me.DataGridViewTextBoxColumn6.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn6.Name, Me.DataGridViewTextBoxColumn6.HeaderText)
            Me.DataGridViewTextBoxColumn7.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn7.Name, Me.DataGridViewTextBoxColumn7.HeaderText)
            Me.DataGridViewTextBoxColumn8.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn8.Name, Me.DataGridViewTextBoxColumn8.HeaderText)
            Me.DataGridViewTextBoxColumn9.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn9.Name, Me.DataGridViewTextBoxColumn9.HeaderText)
            Me.DataGridViewTextBoxColumn10.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn10.Name, Me.DataGridViewTextBoxColumn10.HeaderText)
            Me.DataGridViewTextBoxColumn11.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn11.Name, Me.DataGridViewTextBoxColumn11.HeaderText)
            Me.DataGridViewTextBoxColumn12.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn12.Name, Me.DataGridViewTextBoxColumn12.HeaderText)
            Me.DataGridViewTextBoxColumn13.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn13.Name, Me.DataGridViewTextBoxColumn13.HeaderText)
            Me.DataGridViewTextBoxColumn14.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn14.Name, Me.DataGridViewTextBoxColumn14.HeaderText)
            Me.DataGridViewTextBoxColumn15.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn15.Name, Me.DataGridViewTextBoxColumn15.HeaderText)
            Me.DataGridViewTextBoxColumn16.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn16.Name, Me.DataGridViewTextBoxColumn16.HeaderText)
            Me.DataGridViewTextBoxColumn17.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn17.Name, Me.DataGridViewTextBoxColumn17.HeaderText)
            Me.DataGridViewTextBoxColumn18.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn18.Name, Me.DataGridViewTextBoxColumn18.HeaderText)
            Me.DataGridViewTextBoxColumn19.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn19.Name, Me.DataGridViewTextBoxColumn19.HeaderText)
            Me.DataGridViewTextBoxColumn20.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn20.Name, Me.DataGridViewTextBoxColumn20.HeaderText)
            Me.DataGridViewTextBoxColumn21.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn21.Name, Me.DataGridViewTextBoxColumn21.HeaderText)
            Me.DataGridViewTextBoxColumn22.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn22.Name, Me.DataGridViewTextBoxColumn22.HeaderText)
            Me.DataGridViewTextBoxColumn23.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn23.Name, Me.DataGridViewTextBoxColumn23.HeaderText)
            Me.DataGridViewTextBoxColumn24.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn24.Name, Me.DataGridViewTextBoxColumn24.HeaderText)
            Me.DataGridViewTextBoxColumn25.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn25.Name, Me.DataGridViewTextBoxColumn25.HeaderText)
            Me.DataGridViewTextBoxColumn26.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn26.Name, Me.DataGridViewTextBoxColumn26.HeaderText)
            Me.DataGridViewTextBoxColumn27.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn27.Name, Me.DataGridViewTextBoxColumn27.HeaderText)
            Me.DataGridViewTextBoxColumn28.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn28.Name, Me.DataGridViewTextBoxColumn28.HeaderText)
            Me.DataGridViewTextBoxColumn29.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn29.Name, Me.DataGridViewTextBoxColumn29.HeaderText)
            Me.DataGridViewTextBoxColumn30.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn30.Name, Me.DataGridViewTextBoxColumn30.HeaderText)
            Me.DataGridViewTextBoxColumn31.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn31.Name, Me.DataGridViewTextBoxColumn31.HeaderText)
            Me.DataGridViewTextBoxColumn32.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn32.Name, Me.DataGridViewTextBoxColumn32.HeaderText)
            Me.DataGridViewTextBoxColumn33.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn33.Name, Me.DataGridViewTextBoxColumn33.HeaderText)
            Me.DataGridViewTextBoxColumn34.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn34.Name, Me.DataGridViewTextBoxColumn34.HeaderText)
            Me.DataGridViewTextBoxColumn35.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn35.Name, Me.DataGridViewTextBoxColumn35.HeaderText)
            Me.dgcolheval_itemGE.HeaderText = Language._Object.getCaption(Me.dgcolheval_itemGE.Name, Me.dgcolheval_itemGE.HeaderText)
            Me.dgcolhGEWeight.HeaderText = Language._Object.getCaption(Me.dgcolhGEWeight.Name, Me.dgcolhGEWeight.HeaderText)
            Me.dgcolhGEScore.HeaderText = Language._Object.getCaption(Me.dgcolhGEScore.Name, Me.dgcolhGEScore.HeaderText)
            Me.dgcolheselfGE.HeaderText = Language._Object.getCaption(Me.dgcolheselfGE.Name, Me.dgcolheselfGE.HeaderText)
            Me.dgcolheremarkGE.HeaderText = Language._Object.getCaption(Me.dgcolheremarkGE.Name, Me.dgcolheremarkGE.HeaderText)
            Me.dgcolhaselfGE.HeaderText = Language._Object.getCaption(Me.dgcolhaselfGE.Name, Me.dgcolhaselfGE.HeaderText)
            Me.dgcolharemarkGE.HeaderText = Language._Object.getCaption(Me.dgcolharemarkGE.Name, Me.dgcolharemarkGE.HeaderText)
            Me.dgcolhaAgreedScoreGE.HeaderText = Language._Object.getCaption(Me.dgcolhaAgreedScoreGE.Name, Me.dgcolhaAgreedScoreGE.HeaderText)
            Me.dgcolhrselfGE.HeaderText = Language._Object.getCaption(Me.dgcolhrselfGE.Name, Me.dgcolhrselfGE.HeaderText)
            Me.dgcolhrremarkGE.HeaderText = Language._Object.getCaption(Me.dgcolhrremarkGE.Name, Me.dgcolhrremarkGE.HeaderText)
            Me.lnkViewApproveRejectRemark.Text = Language._Object.getCaption(Me.lnkViewApproveRejectRemark.Name, Me.lnkViewApproveRejectRemark.Text)
            Me.gbRemarks.Text = Language._Object.getCaption(Me.gbRemarks.Name, Me.gbRemarks.Text)
            Me.dgcolhSDate.HeaderText = Language._Object.getCaption(Me.dgcolhSDate.Name, Me.dgcolhSDate.HeaderText)
            Me.dgcolhEDate.HeaderText = Language._Object.getCaption(Me.dgcolhEDate.Name, Me.dgcolhEDate.HeaderText)
            Me.dgcolhCompleted.HeaderText = Language._Object.getCaption(Me.dgcolhCompleted.Name, Me.dgcolhCompleted.HeaderText)
            Me.dgcolhStatus.HeaderText = Language._Object.getCaption(Me.dgcolhStatus.Name, Me.dgcolhStatus.HeaderText)
            Me.dgcolhGoalType.HeaderText = Language._Object.getCaption(Me.dgcolhGoalType.Name, Me.dgcolhGoalType.HeaderText)
            Me.dgcolhGoalvalue.HeaderText = Language._Object.getCaption(Me.dgcolhGoalvalue.Name, Me.dgcolhGoalvalue.HeaderText)
            Me.dgcolhBSCScore.HeaderText = Language._Object.getCaption(Me.dgcolhBSCScore.Name, Me.dgcolhBSCScore.HeaderText)
            Me.dgcolhBSCWeight.HeaderText = Language._Object.getCaption(Me.dgcolhBSCWeight.Name, Me.dgcolhBSCWeight.HeaderText)
            Me.dgcolheselfBSC.HeaderText = Language._Object.getCaption(Me.dgcolheselfBSC.Name, Me.dgcolheselfBSC.HeaderText)
            Me.dgcolheremarkBSC.HeaderText = Language._Object.getCaption(Me.dgcolheremarkBSC.Name, Me.dgcolheremarkBSC.HeaderText)
            Me.dgcolhaselfBSC.HeaderText = Language._Object.getCaption(Me.dgcolhaselfBSC.Name, Me.dgcolhaselfBSC.HeaderText)
            Me.dgcolharemarkBSC.HeaderText = Language._Object.getCaption(Me.dgcolharemarkBSC.Name, Me.dgcolharemarkBSC.HeaderText)
            Me.dgcolhAgreedScoreBSC.HeaderText = Language._Object.getCaption(Me.dgcolhAgreedScoreBSC.Name, Me.dgcolhAgreedScoreBSC.HeaderText)
            Me.dgcolhrselfBSC.HeaderText = Language._Object.getCaption(Me.dgcolhrselfBSC.Name, Me.dgcolhrselfBSC.HeaderText)
            Me.dgcolhrremarkBSC.HeaderText = Language._Object.getCaption(Me.dgcolhrremarkBSC.Name, Me.dgcolhrremarkBSC.HeaderText)

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
        End Try
    End Sub


    Private Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Instructions")
            Language.setMessage(mstrModuleName, 2, "Please set following information [Employee,Period] to view assessment.")
            Language.setMessage(mstrModuleName, 4, "Competencies Evaluation")
            Language.setMessage(mstrModuleName, 5, "Total Weight :")
            Language.setMessage(mstrModuleName, 6, "Self Score :")
            Language.setMessage(mstrModuleName, 7, "Assessor Score :")
            Language.setMessage(mstrModuleName, 8, "Reviewer Score :")
            Language.setMessage(mstrModuleName, 9, "Sorry you cannot go beyond the total weight assigned.")
            Language.setMessage(mstrModuleName, 10, "Sorry, you cannot set more result value than assigned weight.")
            Language.setMessage(mstrModuleName, 12, "Sorry, You have already assessed some items of particular perspective. Please open in edit mode and add new items.")
            Language.setMessage(mstrModuleName, 13, "Sorry, You cannot do assessment for the particular employee for the selected period." & vbCrLf & _
                                                                         "Reason: As per the setting set in configuration,You are not allowed to do assessment unless employee do his self assessment for the selected period.")
            Language.setMessage(mstrModuleName, 14, "Sorry, You have already assessed some items of particular perspective. Please open in edit mode and add new items.")
            Language.setMessage(mstrModuleName, 15, "Sorry, You cannot do assessment for the particular employee for the selected period." & vbCrLf & _
                                                                         "Reason: As per the setting set in configuration,You are not allowed to do assessment unless assessor does assessment for the selected employee and period.")
            Language.setMessage(mstrModuleName, 16, "Emplolyee is compulsory information.Please Select Emplolyee.")
            Language.setMessage(mstrModuleName, 17, "Period is compulsory information.Please Select Period.")
            Language.setMessage(mstrModuleName, 18, "Assessors is compulsory information.Please Select Assessors.")
            Language.setMessage(mstrModuleName, 21, "Reviewer is mandatory information. Please create Reviewer first. Assessment -> Setups -> Reviewer.")
            Language.setMessage(mstrModuleName, 22, "Sorry, you cannot do Save Commit Operation as you have not assessed all item(s) in some objective.")
            Language.setMessage(mstrModuleName, 23, "Sorry, you cannot do Save Commit Operation as you have not assessed all item(s) in some assessment group.")
            Language.setMessage(mstrModuleName, 24, "Sorry, you cannot do Save Commit Operation as you have not assessed all assessment group.")
            Language.setMessage(mstrModuleName, 25, "Sorry, you cannot do Save Commit Operation as you have not assessed all item in assessment group.")
            Language.setMessage(mstrModuleName, 26, "Sorry no score guide defined for the selected item.")
            Language.setMessage(mstrModuleName, 27, "Sorry you cannot go beyond the total weight assigned for the particular assessment group weight.")
            Language.setMessage(mstrModuleName, 28, "Sorry, particular scale is not defined. Please refer score guide for the list of score(s) defined.")
            Language.setMessage(mstrModuleName, 29, "Sorry, scale is not defined. Please define scale.")
            Language.setMessage(mstrModuleName, 32, "There are some Assessement Items which are still not Assessed for particular Group.If you Press Yes,you will Commit this group.Do you want to continue?")
            Language.setMessage(mstrModuleName, 33, "Sorry, this particular custom header is not set for allow multiple entries when defined.")
            Language.setMessage(mstrModuleName, 35, "Sorry, Please assess atleast one item from Balance Score Card Or Competencies Evaluation in order to save.")
            Language.setMessage(mstrModuleName, 36, "Sorry, assessment date should be greater than")
            Language.setMessage(mstrModuleName, 38, "You still have some assessment items not evaluated. Do you want to save now and complete them later?")
            Language.setMessage(mstrModuleName, 40, "Objectives/Goals/Targets")
            Language.setMessage(mstrModuleName, 41, "No scale define in the system")
            Language.setMessage(mstrModuleName, 42, "Sorry, you cannot do Save Commit Operation as you have not assessed all item(s) in some objective.")
            Language.setMessage(mstrModuleName, 43, "Sorry, you cannot do Save Commit Operation as you have not assessed all item(s) in some objective.")
            Language.setMessage(mstrModuleName, 44, "Reviewer Assessment")
            Language.setMessage(mstrModuleName, 45, "Status")
            Language.setMessage(mstrModuleName, 46, "Sorry, Please assess atleast one item from Balance Score Card Evaluation in order to save.")
            Language.setMessage(mstrModuleName, 47, "Sorry, Please assess atleast one item from Competencies Evaluation in order to save.")
            Language.setMessage(mstrModuleName, 48, "Assessor Assessment")
            Language.setMessage(mstrModuleName, 49, "Sorry, you cannot do assessment as last date for assessment has been passed.")
            Language.setMessage(mstrModuleName, 50, "Please contact your administrator/manager to do futher operation on it.")
            Language.setMessage(mstrModuleName, 51, "Sorry, As per the setting set on configuration, Remark are mandatory information for each score you set for Objective/Targets. Please provide remark to continue.")
            Language.setMessage(mstrModuleName, 52, "Sorry, As per the setting set on configuration, Remark are mandatory information for each score you set for competencies. Please provide remark to continue.")
            Language.setMessage(mstrModuleName, 53, "Your assessment has been submitted successfully.")
            Language.setMessage(mstrModuleName, 54, "Assessment has been submitted successfully.")
            Language.setMessage(mstrModuleName, 55, "Your assessment has been saved successfully.")
            Language.setMessage(mstrModuleName, 56, "Assessment has been saved successfully.")
            Language.setMessage(mstrModuleName, 57, "Sorry, No comment(s) has been added in order to edit.")
            Language.setMessage(mstrModuleName, 58, "Sorry, you cannot commit assessment for future date. Please set the assessment date as today date.")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class

'#Region " Textbox Events "

'Private Sub bsctxt_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
'    Try
'        Dim iText As TextBox
'        iText = CType(sender, TextBox)
'        Dim iDecVal As Decimal
'        Decimal.TryParse(iText.Text, iDecVal)
'        Dim idgvRemark As DataGridViewColumn = Nothing
'        If menAssess = enAssessmentMode.SELF_ASSESSMENT Then
'            idgvRemark = dgcolheremarkBSC
'        ElseIf menAssess = enAssessmentMode.APPRAISER_ASSESSMENT Then
'            idgvRemark = dgcolharemarkBSC
'        ElseIf menAssess = enAssessmentMode.REVIEWER_ASSESSMENT Then
'            idgvRemark = dgcolhrremarkBSC
'        End If

'        Select Case CInt(ConfigParameter._Object._ScoringOptionId)
'            Case enScoringOption.SC_WEIGHTED_BASED
'                If CDec(iDecVal) > CDec(dgvBSC.CurrentRow.Cells(dgcolhBSCWeight.Index).Value) Then
'                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 10, "Sorry, you cannot set more result value than assigned weight."), enMsgBoxStyle.Information)
'                    iText.Text = "0" : Exit Sub
'                Else
'                    GoTo iValid
'                End If
'            Case enScoringOption.SC_SCALE_BASED
'                If CDec(iDecVal) > 0 Then
'                    Dim dsScore_Guide As New DataSet
'                    Dim objScaleMaster As New clsAssessment_Scale
'                    dsScore_Guide = objScaleMaster.GetList("List", CInt(cboPeriod.SelectedValue), CInt(dgvBSC.CurrentRow.Cells(objdgcolhScaleMasterId.Index).Value))
'                    objScaleMaster = Nothing
'                    Dim dtmp() As DataRow = dsScore_Guide.Tables(0).Select("scale = " & CDec(iDecVal))
'                    If dtmp.Length <= 0 Then
'                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 11, "Sorry, particular scale is not defined. Please refer score guide for the list of score(s) defined."), enMsgBoxStyle.Information)
'                        iText.Text = "0" : Exit Sub
'                    Else
'                        GoTo iValid
'                    End If
'                End If
'        End Select

'iValid: If Validation() = False Then Exit Sub
'        If Is_Already_Assessed() = False Then
'            iText.Text = "0"
'            Exit Sub
'        End If
'        Call Evaluated_Data_BSC(iDecVal, dgvBSC.CurrentRow.Cells(idgvRemark.Index).Value.ToString)
'    Catch ex As Exception
'        DisplayError.Show("-1", ex.Message, "bsctxt_TextChanged", mstrModuleName)
'    Finally
'    End Try
'End Sub

'#End Region
'Private Sub dgvBSC_EditingControlShowing(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewEditingControlShowingEventArgs) Handles dgvBSC.EditingControlShowing
'    Dim bsctxt As TextBox = Nothing
'    Try
'        Dim idvgCol As DataGridViewColumn = Nothing
'        If menAssess = enAssessmentMode.SELF_ASSESSMENT Then
'            idvgCol = dgcolheselfBSC
'        ElseIf menAssess = enAssessmentMode.APPRAISER_ASSESSMENT Then
'            idvgCol = dgcolhaselfBSC
'        ElseIf menAssess = enAssessmentMode.REVIEWER_ASSESSMENT Then
'            idvgCol = dgcolhrselfBSC
'        End If
'        If dgvBSC.CurrentCell.ColumnIndex = idvgCol.Index AndAlso TypeOf e.Control Is TextBox Then
'            bsctxt = CType(e.Control, TextBox)
'            RemoveHandler bsctxt.TextChanged, AddressOf bsctxt_TextChanged
'            AddHandler bsctxt.TextChanged, AddressOf bsctxt_TextChanged
'        End If
'    Catch ex As Exception
'        DisplayError.Show("-1", ex.Message, "dgvBSC_EditingControlShowing", mstrModuleName)
'    Finally
'    End Try
'End Sub