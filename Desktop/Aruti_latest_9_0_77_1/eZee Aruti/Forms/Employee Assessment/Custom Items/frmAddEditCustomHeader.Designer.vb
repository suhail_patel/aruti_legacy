﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmAddEditCustomHeader
    Inherits eZee.Common.eZeeForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmAddEditCustomHeader))
        Me.pnlMain = New System.Windows.Forms.Panel
        Me.gbCustomHeader = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.nudMinScore = New System.Windows.Forms.NumericUpDown
        Me.lblPastPeriod = New System.Windows.Forms.Label
        Me.cboPastPeriod = New System.Windows.Forms.ComboBox
        Me.lblMinScore = New System.Windows.Forms.Label
        Me.cboScale = New System.Windows.Forms.ComboBox
        Me.chkCompetencies_from_PastPeriod = New System.Windows.Forms.CheckBox
        Me.chkMatchStructureToCompetencies = New System.Windows.Forms.CheckBox
        Me.chkIncludeCustomItemInPlanning = New System.Windows.Forms.CheckBox
        Me.EZeeLine1 = New eZee.Common.eZeeLine
        Me.objbtnOtherLanguage = New eZee.Common.eZeeGradientButton
        Me.chkMultiple = New System.Windows.Forms.CheckBox
        Me.chkShowinMSS = New System.Windows.Forms.CheckBox
        Me.chkShowinESS = New System.Windows.Forms.CheckBox
        Me.lblCustomHeader = New System.Windows.Forms.Label
        Me.txtName = New eZee.TextBox.AlphanumericTextBox
        Me.objbtnSearchPeriod = New eZee.Common.eZeeGradientButton
        Me.lblPeriod = New System.Windows.Forms.Label
        Me.cboPeriod = New System.Windows.Forms.ComboBox
        Me.objFooter = New eZee.Common.eZeeFooter
        Me.btnSave = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnClose = New eZee.Common.eZeeLightButton(Me.components)
        Me.objlblPer = New System.Windows.Forms.Label
        Me.pnlMain.SuspendLayout()
        Me.gbCustomHeader.SuspendLayout()
        CType(Me.nudMinScore, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.objFooter.SuspendLayout()
        Me.SuspendLayout()
        '
        'pnlMain
        '
        Me.pnlMain.Controls.Add(Me.gbCustomHeader)
        Me.pnlMain.Controls.Add(Me.objFooter)
        Me.pnlMain.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pnlMain.Location = New System.Drawing.Point(0, 0)
        Me.pnlMain.Name = "pnlMain"
        Me.pnlMain.Size = New System.Drawing.Size(439, 400)
        Me.pnlMain.TabIndex = 0
        '
        'gbCustomHeader
        '
        Me.gbCustomHeader.BorderColor = System.Drawing.Color.Black
        Me.gbCustomHeader.Checked = False
        Me.gbCustomHeader.CollapseAllExceptThis = False
        Me.gbCustomHeader.CollapsedHoverImage = Nothing
        Me.gbCustomHeader.CollapsedNormalImage = Nothing
        Me.gbCustomHeader.CollapsedPressedImage = Nothing
        Me.gbCustomHeader.CollapseOnLoad = False
        Me.gbCustomHeader.Controls.Add(Me.objlblPer)
        Me.gbCustomHeader.Controls.Add(Me.nudMinScore)
        Me.gbCustomHeader.Controls.Add(Me.lblPastPeriod)
        Me.gbCustomHeader.Controls.Add(Me.cboPastPeriod)
        Me.gbCustomHeader.Controls.Add(Me.lblMinScore)
        Me.gbCustomHeader.Controls.Add(Me.cboScale)
        Me.gbCustomHeader.Controls.Add(Me.chkCompetencies_from_PastPeriod)
        Me.gbCustomHeader.Controls.Add(Me.chkMatchStructureToCompetencies)
        Me.gbCustomHeader.Controls.Add(Me.chkIncludeCustomItemInPlanning)
        Me.gbCustomHeader.Controls.Add(Me.EZeeLine1)
        Me.gbCustomHeader.Controls.Add(Me.objbtnOtherLanguage)
        Me.gbCustomHeader.Controls.Add(Me.chkMultiple)
        Me.gbCustomHeader.Controls.Add(Me.chkShowinMSS)
        Me.gbCustomHeader.Controls.Add(Me.chkShowinESS)
        Me.gbCustomHeader.Controls.Add(Me.lblCustomHeader)
        Me.gbCustomHeader.Controls.Add(Me.txtName)
        Me.gbCustomHeader.Controls.Add(Me.objbtnSearchPeriod)
        Me.gbCustomHeader.Controls.Add(Me.lblPeriod)
        Me.gbCustomHeader.Controls.Add(Me.cboPeriod)
        Me.gbCustomHeader.Dock = System.Windows.Forms.DockStyle.Fill
        Me.gbCustomHeader.ExpandedHoverImage = Nothing
        Me.gbCustomHeader.ExpandedNormalImage = Nothing
        Me.gbCustomHeader.ExpandedPressedImage = Nothing
        Me.gbCustomHeader.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbCustomHeader.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbCustomHeader.HeaderHeight = 25
        Me.gbCustomHeader.HeaderMessage = ""
        Me.gbCustomHeader.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbCustomHeader.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbCustomHeader.HeightOnCollapse = 0
        Me.gbCustomHeader.LeftTextSpace = 0
        Me.gbCustomHeader.Location = New System.Drawing.Point(0, 0)
        Me.gbCustomHeader.Name = "gbCustomHeader"
        Me.gbCustomHeader.OpenHeight = 119
        Me.gbCustomHeader.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbCustomHeader.ShowBorder = True
        Me.gbCustomHeader.ShowCheckBox = False
        Me.gbCustomHeader.ShowCollapseButton = False
        Me.gbCustomHeader.ShowDefaultBorderColor = True
        Me.gbCustomHeader.ShowDownButton = False
        Me.gbCustomHeader.ShowHeader = True
        Me.gbCustomHeader.Size = New System.Drawing.Size(439, 345)
        Me.gbCustomHeader.TabIndex = 5
        Me.gbCustomHeader.Temp = 0
        Me.gbCustomHeader.Text = "Custom Header"
        Me.gbCustomHeader.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'nudMinScore
        '
        Me.nudMinScore.DecimalPlaces = 2
        Me.nudMinScore.Enabled = False
        Me.nudMinScore.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.nudMinScore.Location = New System.Drawing.Point(107, 312)
        Me.nudMinScore.Name = "nudMinScore"
        Me.nudMinScore.Size = New System.Drawing.Size(67, 21)
        Me.nudMinScore.TabIndex = 265
        '
        'lblPastPeriod
        '
        Me.lblPastPeriod.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPastPeriod.Location = New System.Drawing.Point(15, 248)
        Me.lblPastPeriod.Name = "lblPastPeriod"
        Me.lblPastPeriod.Size = New System.Drawing.Size(330, 16)
        Me.lblPastPeriod.TabIndex = 263
        Me.lblPastPeriod.Text = "Pull Competencies from this Period"
        Me.lblPastPeriod.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboPastPeriod
        '
        Me.cboPastPeriod.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboPastPeriod.Enabled = False
        Me.cboPastPeriod.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboPastPeriod.FormattingEnabled = True
        Me.cboPastPeriod.Location = New System.Drawing.Point(107, 269)
        Me.cboPastPeriod.Name = "cboPastPeriod"
        Me.cboPastPeriod.Size = New System.Drawing.Size(209, 21)
        Me.cboPastPeriod.TabIndex = 262
        '
        'lblMinScore
        '
        Me.lblMinScore.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblMinScore.Location = New System.Drawing.Point(18, 293)
        Me.lblMinScore.Name = "lblMinScore"
        Me.lblMinScore.Size = New System.Drawing.Size(298, 16)
        Me.lblMinScore.TabIndex = 260
        Me.lblMinScore.Text = "Where Employee Score is less or equal to"
        Me.lblMinScore.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboScale
        '
        Me.cboScale.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboScale.Enabled = False
        Me.cboScale.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboScale.FormattingEnabled = True
        Me.cboScale.Location = New System.Drawing.Point(107, 312)
        Me.cboScale.Name = "cboScale"
        Me.cboScale.Size = New System.Drawing.Size(209, 21)
        Me.cboScale.TabIndex = 259
        '
        'chkCompetencies_from_PastPeriod
        '
        Me.chkCompetencies_from_PastPeriod.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkCompetencies_from_PastPeriod.Location = New System.Drawing.Point(107, 246)
        Me.chkCompetencies_from_PastPeriod.Name = "chkCompetencies_from_PastPeriod"
        Me.chkCompetencies_from_PastPeriod.Size = New System.Drawing.Size(290, 17)
        Me.chkCompetencies_from_PastPeriod.TabIndex = 258
        Me.chkCompetencies_from_PastPeriod.Text = "Populate with Competencies from Past Period"
        Me.chkCompetencies_from_PastPeriod.UseVisualStyleBackColor = True
        Me.chkCompetencies_from_PastPeriod.Visible = False
        '
        'chkMatchStructureToCompetencies
        '
        Me.chkMatchStructureToCompetencies.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkMatchStructureToCompetencies.Location = New System.Drawing.Point(107, 223)
        Me.chkMatchStructureToCompetencies.Name = "chkMatchStructureToCompetencies"
        Me.chkMatchStructureToCompetencies.Size = New System.Drawing.Size(290, 17)
        Me.chkMatchStructureToCompetencies.TabIndex = 257
        Me.chkMatchStructureToCompetencies.Text = "Use competencies structure for this Header"
        Me.chkMatchStructureToCompetencies.UseVisualStyleBackColor = True
        '
        'chkIncludeCustomItemInPlanning
        '
        Me.chkIncludeCustomItemInPlanning.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkIncludeCustomItemInPlanning.Location = New System.Drawing.Point(107, 200)
        Me.chkIncludeCustomItemInPlanning.Name = "chkIncludeCustomItemInPlanning"
        Me.chkIncludeCustomItemInPlanning.Size = New System.Drawing.Size(290, 17)
        Me.chkIncludeCustomItemInPlanning.TabIndex = 256
        Me.chkIncludeCustomItemInPlanning.Text = "Include Custom Items in Planning"
        Me.chkIncludeCustomItemInPlanning.UseVisualStyleBackColor = True
        '
        'EZeeLine1
        '
        Me.EZeeLine1.LineOrientation = eZee.Common.eZeeLine.Orientation.Horizontal
        Me.EZeeLine1.Location = New System.Drawing.Point(15, 111)
        Me.EZeeLine1.Name = "EZeeLine1"
        Me.EZeeLine1.Size = New System.Drawing.Size(382, 17)
        Me.EZeeLine1.TabIndex = 254
        Me.EZeeLine1.Text = "Allow Header For"
        Me.EZeeLine1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objbtnOtherLanguage
        '
        Me.objbtnOtherLanguage.BackColor = System.Drawing.Color.Transparent
        Me.objbtnOtherLanguage.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnOtherLanguage.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnOtherLanguage.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnOtherLanguage.BorderSelected = False
        Me.objbtnOtherLanguage.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnOtherLanguage.Image = Global.Aruti.Main.My.Resources.Resources.OtherLanguage_16
        Me.objbtnOtherLanguage.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnOtherLanguage.Location = New System.Drawing.Point(403, 34)
        Me.objbtnOtherLanguage.Name = "objbtnOtherLanguage"
        Me.objbtnOtherLanguage.Size = New System.Drawing.Size(21, 21)
        Me.objbtnOtherLanguage.TabIndex = 253
        '
        'chkMultiple
        '
        Me.chkMultiple.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkMultiple.Location = New System.Drawing.Point(107, 177)
        Me.chkMultiple.Name = "chkMultiple"
        Me.chkMultiple.Size = New System.Drawing.Size(290, 17)
        Me.chkMultiple.TabIndex = 252
        Me.chkMultiple.Text = "Allow multiple entries"
        Me.chkMultiple.UseVisualStyleBackColor = True
        '
        'chkShowinMSS
        '
        Me.chkShowinMSS.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkShowinMSS.Location = New System.Drawing.Point(107, 154)
        Me.chkShowinMSS.Name = "chkShowinMSS"
        Me.chkShowinMSS.Size = New System.Drawing.Size(290, 17)
        Me.chkShowinMSS.TabIndex = 251
        Me.chkShowinMSS.Text = "Display in Manager Self Service"
        Me.chkShowinMSS.UseVisualStyleBackColor = True
        '
        'chkShowinESS
        '
        Me.chkShowinESS.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkShowinESS.Location = New System.Drawing.Point(107, 131)
        Me.chkShowinESS.Name = "chkShowinESS"
        Me.chkShowinESS.Size = New System.Drawing.Size(290, 17)
        Me.chkShowinESS.TabIndex = 250
        Me.chkShowinESS.Text = "Display in Employee Self Service"
        Me.chkShowinESS.UseVisualStyleBackColor = True
        '
        'lblCustomHeader
        '
        Me.lblCustomHeader.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCustomHeader.Location = New System.Drawing.Point(12, 39)
        Me.lblCustomHeader.Name = "lblCustomHeader"
        Me.lblCustomHeader.Size = New System.Drawing.Size(89, 16)
        Me.lblCustomHeader.TabIndex = 243
        Me.lblCustomHeader.Text = "Custom Header"
        Me.lblCustomHeader.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtName
        '
        Me.txtName.BackColor = System.Drawing.SystemColors.Window
        Me.txtName.Flags = 0
        Me.txtName.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtName.InvalidChars = New Char(-1) {}
        Me.txtName.Location = New System.Drawing.Point(107, 34)
        Me.txtName.Multiline = True
        Me.txtName.Name = "txtName"
        Me.txtName.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.txtName.Size = New System.Drawing.Size(290, 74)
        Me.txtName.TabIndex = 242
        '
        'objbtnSearchPeriod
        '
        Me.objbtnSearchPeriod.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchPeriod.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchPeriod.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchPeriod.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchPeriod.BorderSelected = False
        Me.objbtnSearchPeriod.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchPeriod.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objbtnSearchPeriod.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchPeriod.Location = New System.Drawing.Point(403, 34)
        Me.objbtnSearchPeriod.Name = "objbtnSearchPeriod"
        Me.objbtnSearchPeriod.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchPeriod.TabIndex = 238
        Me.objbtnSearchPeriod.Visible = False
        '
        'lblPeriod
        '
        Me.lblPeriod.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPeriod.Location = New System.Drawing.Point(12, 36)
        Me.lblPeriod.Name = "lblPeriod"
        Me.lblPeriod.Size = New System.Drawing.Size(89, 16)
        Me.lblPeriod.TabIndex = 236
        Me.lblPeriod.Text = "Period"
        Me.lblPeriod.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.lblPeriod.Visible = False
        '
        'cboPeriod
        '
        Me.cboPeriod.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboPeriod.DropDownWidth = 450
        Me.cboPeriod.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboPeriod.FormattingEnabled = True
        Me.cboPeriod.Location = New System.Drawing.Point(107, 34)
        Me.cboPeriod.Name = "cboPeriod"
        Me.cboPeriod.Size = New System.Drawing.Size(290, 21)
        Me.cboPeriod.TabIndex = 235
        Me.cboPeriod.Visible = False
        '
        'objFooter
        '
        Me.objFooter.BorderColor = System.Drawing.Color.Silver
        Me.objFooter.Controls.Add(Me.btnSave)
        Me.objFooter.Controls.Add(Me.btnClose)
        Me.objFooter.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.objFooter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objFooter.GradiantStyle = eZee.Common.eZeeFooter.GradientStyle.Up
        Me.objFooter.GradientColor1 = System.Drawing.SystemColors.Control
        Me.objFooter.GradientColor2 = System.Drawing.SystemColors.Control
        Me.objFooter.Location = New System.Drawing.Point(0, 345)
        Me.objFooter.Name = "objFooter"
        Me.objFooter.Size = New System.Drawing.Size(439, 55)
        Me.objFooter.TabIndex = 4
        '
        'btnSave
        '
        Me.btnSave.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnSave.BackColor = System.Drawing.Color.White
        Me.btnSave.BackgroundImage = CType(resources.GetObject("btnSave.BackgroundImage"), System.Drawing.Image)
        Me.btnSave.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnSave.BorderColor = System.Drawing.Color.Empty
        Me.btnSave.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnSave.FlatAppearance.BorderSize = 0
        Me.btnSave.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnSave.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSave.ForeColor = System.Drawing.Color.Black
        Me.btnSave.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnSave.GradientForeColor = System.Drawing.Color.Black
        Me.btnSave.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSave.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnSave.Location = New System.Drawing.Point(227, 13)
        Me.btnSave.Name = "btnSave"
        Me.btnSave.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSave.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnSave.Size = New System.Drawing.Size(97, 30)
        Me.btnSave.TabIndex = 0
        Me.btnSave.Text = "&Save"
        Me.btnSave.UseVisualStyleBackColor = True
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnClose.BackColor = System.Drawing.Color.White
        Me.btnClose.BackgroundImage = CType(resources.GetObject("btnClose.BackgroundImage"), System.Drawing.Image)
        Me.btnClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnClose.BorderColor = System.Drawing.Color.Empty
        Me.btnClose.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnClose.FlatAppearance.BorderSize = 0
        Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClose.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.ForeColor = System.Drawing.Color.Black
        Me.btnClose.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnClose.GradientForeColor = System.Drawing.Color.Black
        Me.btnClose.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Location = New System.Drawing.Point(330, 13)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Size = New System.Drawing.Size(97, 30)
        Me.btnClose.TabIndex = 1
        Me.btnClose.Text = "&Close"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'objlblPer
        '
        Me.objlblPer.AutoSize = True
        Me.objlblPer.Location = New System.Drawing.Point(180, 314)
        Me.objlblPer.Name = "objlblPer"
        Me.objlblPer.Size = New System.Drawing.Size(20, 13)
        Me.objlblPer.TabIndex = 267
        Me.objlblPer.Text = "%"
        '
        'frmAddEditCustomHeader
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(439, 400)
        Me.Controls.Add(Me.pnlMain)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmAddEditCustomHeader"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Add/Edit Custom Header"
        Me.pnlMain.ResumeLayout(False)
        Me.gbCustomHeader.ResumeLayout(False)
        Me.gbCustomHeader.PerformLayout()
        CType(Me.nudMinScore, System.ComponentModel.ISupportInitialize).EndInit()
        Me.objFooter.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents pnlMain As System.Windows.Forms.Panel
    Friend WithEvents objFooter As eZee.Common.eZeeFooter
    Friend WithEvents btnSave As eZee.Common.eZeeLightButton
    Friend WithEvents btnClose As eZee.Common.eZeeLightButton
    Friend WithEvents gbCustomHeader As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents lblCustomHeader As System.Windows.Forms.Label
    Friend WithEvents txtName As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents objbtnSearchPeriod As eZee.Common.eZeeGradientButton
    Friend WithEvents lblPeriod As System.Windows.Forms.Label
    Friend WithEvents cboPeriod As System.Windows.Forms.ComboBox
    Friend WithEvents chkMultiple As System.Windows.Forms.CheckBox
    Friend WithEvents chkShowinMSS As System.Windows.Forms.CheckBox
    Friend WithEvents chkShowinESS As System.Windows.Forms.CheckBox
    Friend WithEvents objbtnOtherLanguage As eZee.Common.eZeeGradientButton
    Friend WithEvents EZeeLine1 As eZee.Common.eZeeLine
    Friend WithEvents chkCompetencies_from_PastPeriod As System.Windows.Forms.CheckBox
    Friend WithEvents chkMatchStructureToCompetencies As System.Windows.Forms.CheckBox
    Friend WithEvents chkIncludeCustomItemInPlanning As System.Windows.Forms.CheckBox
    Friend WithEvents cboScale As System.Windows.Forms.ComboBox
    Friend WithEvents lblMinScore As System.Windows.Forms.Label
    Friend WithEvents lblPastPeriod As System.Windows.Forms.Label
    Friend WithEvents cboPastPeriod As System.Windows.Forms.ComboBox
    Friend WithEvents nudMinScore As System.Windows.Forms.NumericUpDown
    Friend WithEvents objlblPer As System.Windows.Forms.Label
End Class
