﻿Option Strict On

#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data

#End Region

Public Class frmCustomHeaderList

#Region " Private Variable "

    Private ReadOnly mstrModuleName As String = "frmCustomHeaderList"
    Private objCHeader As clsassess_custom_header

#End Region

#Region " Private Methods "

    Private Sub FillCombo()
        Dim dsCombo As New DataSet
        Dim objPeriod As New clscommom_period_Tran
        Try
            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'dsCombo = objPeriod.getListForCombo(enModuleReference.Assessment, 0, "List", True, enStatusType.Open)
            dsCombo = objPeriod.getListForCombo(enModuleReference.Assessment, 0, FinancialYear._Object._DatabaseName, FinancialYear._Object._Database_Start_Date, "List", True, enStatusType.Open)
            'Sohail (21 Aug 2015) -- End
            With cboPeriod
                .ValueMember = "periodunkid"
                .DisplayMember = "name"
                .DataSource = dsCombo.Tables(0)
                .SelectedValue = 0
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub SetVisibility()
        Try
            'S.SANDEEP [28 MAY 2015] -- START
            'ENHANCEMENT : NEW PRIVILEGE (Performance Assessment)
            btnNew.Enabled = User._Object.Privilege._AllowtoAddCustomHeaders
            btnEdit.Enabled = User._Object.Privilege._AllowtoEditCustomHeaders
            btnDelete.Enabled = User._Object.Privilege._AllowtoDeleteCustomHeaders
            'S.SANDEEP [28 MAY 2015] -- END
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetVisibility", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub Fill_List()
        Dim dsList As New DataSet
        Dim iSearch As String = String.Empty
        Dim dtTable As DataTable = Nothing
        Try
            dsList = objCHeader.GetList("List")

            lvCustomHeaders.Items.Clear()

            If chkMultiple.Checked = True Then
                iSearch &= "AND is_allow_multiple = '" & chkMultiple.Checked & "' "
            End If

            If chkShowinESS.Checked = True Then
                iSearch &= "AND is_allow_ess = '" & chkShowinESS.Checked & "' "
            End If

            If chkShowinMSS.Checked = True Then
                iSearch &= "AND is_allow_mss = '" & chkShowinMSS.Checked & "' "
            End If


            If iSearch.Trim.Length > 0 Then
                iSearch = iSearch.Substring(3)
                dtTable = New DataView(dsList.Tables(0), iSearch, "", DataViewRowState.CurrentRows).ToTable
            Else
                dtTable = New DataView(dsList.Tables(0), "", "", DataViewRowState.CurrentRows).ToTable
            End If

            For Each dtRow As DataRow In dtTable.Rows
                Dim lvItem As New ListViewItem

                lvItem.Text = dtRow.Item("name").ToString               'CUSTOM HEADER
                lvItem.SubItems.Add(dtRow.Item("ess").ToString)         'SHOW IN ESS
                lvItem.SubItems.Add(dtRow.Item("mss").ToString)         'SHOW IN MSS
                lvItem.SubItems.Add(dtRow.Item("multiple").ToString)    'ALLOW MULTIPLE

                lvItem.Tag = dtRow.Item("customheaderunkid").ToString

                lvCustomHeaders.Items.Add(lvItem)
            Next

            If lvCustomHeaders.Items.Count >= 2 Then
                colhCustomHdr.Width = 385 - 20
            Else
                colhCustomHdr.Width = 385
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Fill_List", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

#Region " Form's Event "

    Private Sub frmCustomHeaderList_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        objCHeader = New clsassess_custom_header
        Try
            Call Set_Logo(Me, gApplicationType)
            Language.setLanguage(Me.Name)
            Call OtherSettings()
            Call FillCombo()
            Call SetVisibility()

            If lvCustomHeaders.Items.Count > 0 Then lvCustomHeaders.Items(0).Selected = True
            lvCustomHeaders.Select()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmCustomHeaderList_Load", mstrModuleName)
        End Try
    End Sub

    Private Sub frmCustomHeaderList_KeyUp(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles MyBase.KeyUp
        Try
            If e.KeyCode = Keys.Delete And lvCustomHeaders.Focused = True Then
                Call btnDelete.PerformClick()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmCustomHeaderList_KeyUp", mstrModuleName)
        End Try
    End Sub

    Private Sub frmCustomHeaderList_FormClosed(ByVal sender As System.Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles MyBase.FormClosed
        objCHeader = Nothing
    End Sub

    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsassess_custom_header.SetMessages()
            objfrm._Other_ModuleNames = "clsassess_custom_header"
            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub

#End Region

#Region " Button's Event "

    Private Sub btnNew_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNew.Click
        Dim frm As New frmAddEditCustomHeader
        Try
            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If
            If frm.displayDialog(-1, enAction.ADD_CONTINUE) Then
                Call Fill_List()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnNew_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub btnEdit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEdit.Click
        Dim frm As New frmAddEditCustomHeader
        Try
            If lvCustomHeaders.SelectedItems.Count <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please select Custom Header from the list to perform further operation."), enMsgBoxStyle.Information) '?1
                lvCustomHeaders.Select()
                Exit Sub
            End If

            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If

            If frm.displayDialog(CInt(lvCustomHeaders.SelectedItems(0).Tag), enAction.EDIT_ONE) Then
                Call Fill_List()
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnEdit_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub btnDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDelete.Click
        Try
            If lvCustomHeaders.SelectedItems.Count <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please select Custom Header from the list to perform further operation."), enMsgBoxStyle.Information) '?1
                lvCustomHeaders.Select()
                Exit Sub
            End If

            If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Are you sure you want to delete this Custom Header?"), CType(enMsgBoxStyle.Question + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.Yes Then

                'S.SANDEEP [28-May-2018] -- START
                'ISSUE/ENHANCEMENT : {Audit Trails} 
                objCHeader._FormName = mstrModuleName
                objCHeader._LoginEmployeeunkid = 0
                objCHeader._ClientIP = getIP()
                objCHeader._HostName = getHostName()
                objCHeader._FromWeb = False
                objCHeader._AuditUserId = User._Object._Userunkid
objCHeader._CompanyUnkid = Company._Object._Companyunkid
                objCHeader._AuditDate = ConfigParameter._Object._CurrentDateAndTime
                'S.SANDEEP [28-May-2018] -- END

                If objCHeader.Delete(CInt(lvCustomHeaders.SelectedItems(0).Tag), User._Object._Userunkid) = True Then
                    lvCustomHeaders.SelectedItems(0).Remove()

                    'Shani (26-Sep-2016) -- Start
                    'Enhancement -Changes PA Flow And Add New screen Given By (Andrew)
                ElseIf objCHeader._Message <> "" Then
                    eZeeMsgBox.Show(objCHeader._Message, enMsgBoxStyle.Information)
                    'Shani (26-Sep-2016) -- End
                End If
            End If
            lvCustomHeaders.Select()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnDelete_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub objbtnSearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearch.Click
        Try
            Call Fill_List()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub objbtnReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnReset.Click
        Try
            cboPeriod.SelectedValue = 0
            chkMultiple.Checked = False : chkShowinESS.Checked = False : chkShowinMSS.Checked = False
            lvCustomHeaders.Items.Clear()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnReset_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnClose_Click", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

#Region " Control's Events "

    Private Sub lvCustomHeaders_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lvCustomHeaders.SelectedIndexChanged
        Try
            If lvCustomHeaders.SelectedItems.Count > 0 Then
                If lvCustomHeaders.SelectedItems(0).ForeColor = Color.Gray Then
                    btnEdit.Enabled = False : btnDelete.Enabled = False
                Else
                    btnEdit.Enabled = True : btnDelete.Enabled = True
                End If
            End If

            'Varsha Rana (17-Oct-2017) -- Start
            'Enhancement - Give user privileges 
            Call SetVisibility()
            'Varsha Rana (17-Oct-2017) -- End

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lvCustomHeaders_SelectedIndexChanged", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Private Sub OtherSettings()
        Try
            Me.SuspendLayout()
            
            Call SetLanguage()

            Me.gbFilterCriteria.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbFilterCriteria.ForeColor = GUI._eZeeContainerHeaderForeColor


            Me.eZeeHeader.GradientColor1 = GUI._HeaderBackColor1
            Me.eZeeHeader.GradientColor2 = GUI._HeaderBackColor2
            Me.eZeeHeader.BorderColor = GUI._HeaderBorderColor
            Me.eZeeHeader.HeaderTextForeColor = GUI._HeaderTitleFontColor
            Me.eZeeHeader.DescriptionForeColor = GUI._HeaderMessageFontColor


            Me.btnDelete.GradientBackColor = GUI._ButttonBackColor
            Me.btnDelete.GradientForeColor = GUI._ButttonFontColor

            Me.btnEdit.GradientBackColor = GUI._ButttonBackColor
            Me.btnEdit.GradientForeColor = GUI._ButttonFontColor

            Me.btnNew.GradientBackColor = GUI._ButttonBackColor
            Me.btnNew.GradientForeColor = GUI._ButttonFontColor

            Me.btnClose.GradientBackColor = GUI._ButttonBackColor
            Me.btnClose.GradientForeColor = GUI._ButttonFontColor


            Me.ResumeLayout()
        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "OtherSettings", mstrModuleName)
        End Try
    End Sub


    Private Sub SetLanguage()
        Try
            Me.Text = Language._Object.getCaption(Me.Name, Me.Text)

            Me.eZeeHeader.Title = Language._Object.getCaption(Me.eZeeHeader.Name & "_Title", Me.eZeeHeader.Title)
            Me.eZeeHeader.Message = Language._Object.getCaption(Me.eZeeHeader.Name & "_Message", Me.eZeeHeader.Message)
            Me.btnDelete.Text = Language._Object.getCaption(Me.btnDelete.Name, Me.btnDelete.Text)
            Me.btnEdit.Text = Language._Object.getCaption(Me.btnEdit.Name, Me.btnEdit.Text)
            Me.btnNew.Text = Language._Object.getCaption(Me.btnNew.Name, Me.btnNew.Text)
            Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
            Me.gbFilterCriteria.Text = Language._Object.getCaption(Me.gbFilterCriteria.Name, Me.gbFilterCriteria.Text)
            Me.lblPeriod.Text = Language._Object.getCaption(Me.lblPeriod.Name, Me.lblPeriod.Text)
            Me.chkMultiple.Text = Language._Object.getCaption(Me.chkMultiple.Name, Me.chkMultiple.Text)
            Me.chkShowinMSS.Text = Language._Object.getCaption(Me.chkShowinMSS.Name, Me.chkShowinMSS.Text)
            Me.chkShowinESS.Text = Language._Object.getCaption(Me.chkShowinESS.Name, Me.chkShowinESS.Text)
            Me.colhCustomHdr.Text = Language._Object.getCaption(CStr(Me.colhCustomHdr.Tag), Me.colhCustomHdr.Text)
            Me.colhESS.Text = Language._Object.getCaption(CStr(Me.colhESS.Tag), Me.colhESS.Text)
            Me.colhMSS.Text = Language._Object.getCaption(CStr(Me.colhMSS.Tag), Me.colhMSS.Text)
            Me.colhMultiple.Text = Language._Object.getCaption(CStr(Me.colhMultiple.Tag), Me.colhMultiple.Text)

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
        End Try
    End Sub


    Private Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Please select Custom Header from the list to perform further operation.")
            Language.setMessage(mstrModuleName, 2, "Are you sure you want to delete this Custom Header?")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class