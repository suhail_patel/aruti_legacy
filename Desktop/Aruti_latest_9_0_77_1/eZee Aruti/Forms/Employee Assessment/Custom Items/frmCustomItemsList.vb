﻿Option Strict On

#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data

#End Region

Public Class frmCustomItemsList

#Region " Private Variable "

    Private ReadOnly mstrModuleName As String = "frmCustomItemsList"
    Private objCFields As clsassess_custom_items

#End Region

#Region " Private Methods "

    Private Sub FillCombo()
        Dim objPeriod As New clscommom_period_Tran
        Dim dsCombo As New DataSet
        Try
            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'dsCombo = objPeriod.getListForCombo(enModuleReference.Assessment, 0, "List", True, 1)
            dsCombo = objPeriod.getListForCombo(enModuleReference.Assessment, 0, FinancialYear._Object._DatabaseName, FinancialYear._Object._Database_Start_Date, "List", True, 1)
            'Sohail (21 Aug 2015) -- End
            With cboPeriod
                .ValueMember = "periodunkid"
                .DisplayMember = "name"
                .DataSource = dsCombo.Tables(0)
                .SelectedValue = 0
            End With

            dsCombo = objCFields.GetList_CustomTypes("List", True)
            With cboItemType
                .ValueMember = "Id"
                .DisplayMember = "Name"
                .DataSource = dsCombo.Tables(0)
                .SelectedIndex = 0
            End With

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub SetVisibility()
        Try
            'S.SANDEEP [28 MAY 2015] -- START
            'ENHANCEMENT : NEW PRIVILEGE (Performance Assessment)
            btnNew.Enabled = User._Object.Privilege._AllowtoAddCustomItems
            btnEdit.Enabled = User._Object.Privilege._AllowtoEditCustomItems
            btnDelete.Enabled = User._Object.Privilege._AllowtoDeleteCustomItems
            'S.SANDEEP [28 MAY 2015] -- END
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetVisibility", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub Fill_List()
        Dim iSearch As String = String.Empty
        Dim mdtTable As DataTable
        Dim dsList As New DataSet
        Try
            dsList = objCFields.GetList("List")

            lvCustomItems.Items.Clear()

            If CInt(cboCustomHeader.SelectedValue) > 0 Then
                iSearch &= "AND customheaderunkid = '" & CInt(cboCustomHeader.SelectedValue) & "' "
            End If

            If CInt(cboPeriod.SelectedValue) > 0 Then
                iSearch &= "AND periodunkid = '" & CInt(cboPeriod.SelectedValue) & "' "
            End If

            If CInt(cboItemType.SelectedValue) > 0 Then
                iSearch &= "AND itemtypeid = '" & CInt(cboItemType.SelectedValue) & "' "
            End If
            Dim strViewFltr As String = String.Empty
            If chkSelfAssessment.Checked = True Then
                strViewFltr &= "," & 0 'INTENSIONALLY PASSED - TO BE ALLOWED FOR ALL
            End If
            If chkAssessorAssessment.Checked = True Then
                strViewFltr &= "," & enAssessmentMode.APPRAISER_ASSESSMENT
            End If
            If chkReviewerAssessment.Checked = True Then
                strViewFltr &= "," & enAssessmentMode.REVIEWER_ASSESSMENT
            End If
            If strViewFltr.Trim.Length > 0 Then
                strViewFltr = Mid(strViewFltr, 2)
                iSearch &= "AND viewmodeid IN(" & strViewFltr & ") "
            End If

            If iSearch.Trim.Length > 0 Then
                iSearch = iSearch.Substring(3)
                mdtTable = New DataView(dsList.Tables(0), iSearch, "", DataViewRowState.CurrentRows).ToTable
            Else
                mdtTable = New DataView(dsList.Tables(0), "", "", DataViewRowState.CurrentRows).ToTable
            End If

            For Each dRow As DataRow In mdtTable.Rows
                Dim lvItem As New ListViewItem

                lvItem.Text = dRow.Item("pname").ToString
                lvItem.SubItems.Add(dRow.Item("custom_item").ToString)
                lvItem.SubItems.Add(dRow.Item("iType").ToString)
                lvItem.SubItems.Add(dRow.Item("cheader").ToString)
                lvItem.SubItems.Add(dRow.Item("periodunkid").ToString)
                lvItem.SubItems.Add(dRow.Item("iSType").ToString)
                lvItem.SubItems.Add(dRow.Item("iViewType").ToString)

                If CInt(dRow.Item("statusid")) = enStatusType.Close Then lvItem.ForeColor = Color.Gray

                lvItem.Tag = dRow.Item("customitemunkid")

                lvCustomItems.Items.Add(lvItem)
            Next

            If lvCustomItems.Items.Count >= 2 Then
                colhCustomItems.Width = 350 - 20
            Else
                colhCustomItems.Width = 350
            End If

            lvCustomItems.GroupingColumn = objcolhGroup
            lvCustomItems.DisplayGroups(True)

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Fill_List", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

#Region " Form's Event "

    Private Sub frmCustomItemsList_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        objCFields = New clsassess_custom_items
        Try
            Call Set_Logo(Me, gApplicationType)
            Language.setLanguage(Me.Name)
            Call OtherSettings()
            Call FillCombo()
            Call SetVisibility()
            objlblNotes.Text = Language.getMessage(mstrModuleName, 4, "Note: You are allowed to define only 10 Custom Items per Custom Header.")
            If lvCustomItems.Items.Count > 0 Then lvCustomItems.Items(0).Selected = True
            lvCustomItems.Select()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmCustomItemsList_Load", mstrModuleName)
        End Try
    End Sub

    Private Sub frmCustomItemsList_KeyUp(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles MyBase.KeyUp
        Try
            If e.KeyCode = Keys.Delete And lvCustomItems.Focused = True Then
                Call btnDelete.PerformClick()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmCustomItemsList_KeyUp", mstrModuleName)
        End Try
    End Sub

    Private Sub frmCustomItemsList_FormClosed(ByVal sender As System.Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles MyBase.FormClosed
        objCFields = Nothing
    End Sub

    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsassess_custom_items.SetMessages()
            objfrm._Other_ModuleNames = "clsassess_custom_items"
            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub

#End Region

#Region " Button's Event "

    Private Sub btnNew_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNew.Click
        Dim frm As New frmAddEditCustomItems
        Try
            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If
            If frm.displayDialog(-1, enAction.ADD_CONTINUE) Then
                Call Fill_List()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnNew_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub btnEdit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEdit.Click
        Dim frm As New frmAddEditCustomItems
        Try
            If lvCustomItems.SelectedItems.Count <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please select Custom Item from the list to perform further operation."), enMsgBoxStyle.Information) '?1
                lvCustomItems.Select()
                Exit Sub
            End If

            If lvCustomItems.SelectedItems(0).ForeColor = Color.Gray Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "Sorry, You cannot edit this Custom Item. Reason Period is already closed."), enMsgBoxStyle.Information) '?1
                lvCustomItems.Select()
                Exit Sub
            End If

            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If

            If frm.displayDialog(CInt(lvCustomItems.SelectedItems(0).Tag), enAction.EDIT_ONE) Then
                Call Fill_List()
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnEdit_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub btnDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDelete.Click
        Try
            If lvCustomItems.SelectedItems.Count <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please select Custom Item from the list to perform further operation."), enMsgBoxStyle.Information) '?1
                lvCustomItems.Select()
                Exit Sub
            End If

            If lvCustomItems.SelectedItems(0).ForeColor = Color.Gray Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 6, "Sorry, You cannot delete this Custom Item. Reason Period is already closed."), enMsgBoxStyle.Information) '?1
                lvCustomItems.Select()
                Exit Sub
            End If

            If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Are you sure you want to delete this Custom Item?"), CType(enMsgBoxStyle.Question + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.Yes Then

                'S.SANDEEP [28-May-2018] -- START
                'ISSUE/ENHANCEMENT : {Audit Trails} 
                objCFields._FormName = mstrModuleName
                objCFields._LoginEmployeeunkid = 0
                objCFields._ClientIP = getIP()
                objCFields._HostName = getHostName()
                objCFields._FromWeb = False
                objCFields._AuditUserId = User._Object._Userunkid
objCFields._CompanyUnkid = Company._Object._Companyunkid
                objCFields._AuditDate = ConfigParameter._Object._CurrentDateAndTime
                'S.SANDEEP [28-May-2018] -- END

                If objCFields.Delete(CInt(lvCustomItems.SelectedItems(0).Tag), User._Object._Userunkid) = True Then
                    lvCustomItems.SelectedItems(0).Remove()

                    'Shani (26-Sep-2016) -- Start
                    'Enhancement -Changes PA Flow And Add New screen Given By (Andrew)
                ElseIf objCFields._Message <> "" Then
                    eZeeMsgBox.Show(objCFields._Message, enMsgBoxStyle.Information)
                    Exit Sub
                    'Shani (26-Sep-2016) -- End
                End If

            End If
            lvCustomItems.Select()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnDelete_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub objbtnSearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearch.Click
        Try
            If CInt(cboPeriod.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Period is mandatory information. Please select Period to continue."), enMsgBoxStyle.Information)
                cboPeriod.Focus()
                Exit Sub
            End If
            Call Fill_List()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub objbtnReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnReset.Click
        Try
            cboPeriod.SelectedValue = 0
            cboCustomHeader.SelectedValue = 0
            cboItemType.SelectedValue = 0
            chkSelfAssessment.Checked = False
            chkAssessorAssessment.Checked = False
            chkReviewerAssessment.Checked = False
            lvCustomItems.Items.Clear()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnReset_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnClose_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub objbtnSearchGroup_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchGroup.Click
        Dim frm As New frmCommonSearch
        Try
            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If
            With frm
                .ValueMember = cboCustomHeader.ValueMember
                .DisplayMember = cboCustomHeader.DisplayMember
                .DataSource = CType(cboCustomHeader.DataSource, DataTable)
                If .DisplayDialog Then
                    cboCustomHeader.SelectedValue = .SelectedValue
                    cboCustomHeader.Focus()
                End If
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchGroup_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

#End Region

#Region " Control's Events "

    Private Sub cboPeriod_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboPeriod.SelectedIndexChanged
        Try
            Dim dsList As New DataSet
            Dim objCHeader As New clsassess_custom_header
            dsList = objCHeader.getComboList(CInt(cboPeriod.SelectedValue), True, "List")
            With cboCustomHeader
                .ValueMember = "Id"
                .DisplayMember = "Name"
                .DataSource = dsList.Tables(0)
                .SelectedValue = 0
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboPeriod_SelectedIndexChanged", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub lvCustomItems_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lvCustomItems.SelectedIndexChanged
        Try
            If lvCustomItems.SelectedItems.Count > 0 Then
                If lvCustomItems.SelectedItems(0).ForeColor = Color.Gray Then
                    btnEdit.Enabled = False : btnDelete.Enabled = False
                Else
                    btnEdit.Enabled = True : btnDelete.Enabled = True
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lvCustomHeaders_SelectedIndexChanged", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Private Sub OtherSettings()
        Try
            Me.SuspendLayout()
           
            Call SetLanguage()

            Me.gbFilterCriteria.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbFilterCriteria.ForeColor = GUI._eZeeContainerHeaderForeColor


           
            Me.eZeeHeader.GradientColor1 = GUI._HeaderBackColor1
            Me.eZeeHeader.GradientColor2 = GUI._HeaderBackColor2
            Me.eZeeHeader.BorderColor = GUI._HeaderBorderColor
            Me.eZeeHeader.HeaderTextForeColor = GUI._HeaderTitleFontColor
            Me.eZeeHeader.DescriptionForeColor = GUI._HeaderMessageFontColor


            Me.btnDelete.GradientBackColor = GUI._ButttonBackColor
            Me.btnDelete.GradientForeColor = GUI._ButttonFontColor

            Me.btnEdit.GradientBackColor = GUI._ButttonBackColor
            Me.btnEdit.GradientForeColor = GUI._ButttonFontColor

            Me.btnNew.GradientBackColor = GUI._ButttonBackColor
            Me.btnNew.GradientForeColor = GUI._ButttonFontColor

            Me.btnClose.GradientBackColor = GUI._ButttonBackColor
            Me.btnClose.GradientForeColor = GUI._ButttonFontColor


            Me.ResumeLayout()
        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "OtherSettings", mstrModuleName)
        End Try
    End Sub


    Private Sub SetLanguage()
        Try
            Me.Text = Language._Object.getCaption(Me.Name, Me.Text)

            Me.btnDelete.Text = Language._Object.getCaption(Me.btnDelete.Name, Me.btnDelete.Text)
            Me.btnEdit.Text = Language._Object.getCaption(Me.btnEdit.Name, Me.btnEdit.Text)
            Me.btnNew.Text = Language._Object.getCaption(Me.btnNew.Name, Me.btnNew.Text)
            Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
            Me.gbFilterCriteria.Text = Language._Object.getCaption(Me.gbFilterCriteria.Name, Me.gbFilterCriteria.Text)
            Me.lblPeriod.Text = Language._Object.getCaption(Me.lblPeriod.Name, Me.lblPeriod.Text)
            Me.lblCustomHeader.Text = Language._Object.getCaption(Me.lblCustomHeader.Name, Me.lblCustomHeader.Text)
            Me.eZeeHeader.Title = Language._Object.getCaption(Me.eZeeHeader.Name & "_Title", Me.eZeeHeader.Title)
            Me.eZeeHeader.Message = Language._Object.getCaption(Me.eZeeHeader.Name & "_Message", Me.eZeeHeader.Message)
            Me.colhPeriod.Text = Language._Object.getCaption(CStr(Me.colhPeriod.Tag), Me.colhPeriod.Text)
            Me.colhCustomItems.Text = Language._Object.getCaption(CStr(Me.colhCustomItems.Tag), Me.colhCustomItems.Text)
            Me.colhItemType.Text = Language._Object.getCaption(CStr(Me.colhItemType.Tag), Me.colhItemType.Text)
            Me.colhMode.Text = Language._Object.getCaption(CStr(Me.colhMode.Tag), Me.colhMode.Text)
            Me.lblItemType.Text = Language._Object.getCaption(Me.lblItemType.Name, Me.lblItemType.Text)
            Me.lblViewMode.Text = Language._Object.getCaption(Me.lblViewMode.Name, Me.lblViewMode.Text)
            Me.colhViewMode.Text = Language._Object.getCaption(CStr(Me.colhViewMode.Tag), Me.colhViewMode.Text)
            Me.chkReviewerAssessment.Text = Language._Object.getCaption(Me.chkReviewerAssessment.Name, Me.chkReviewerAssessment.Text)
            Me.chkAssessorAssessment.Text = Language._Object.getCaption(Me.chkAssessorAssessment.Name, Me.chkAssessorAssessment.Text)
            Me.chkSelfAssessment.Text = Language._Object.getCaption(Me.chkSelfAssessment.Name, Me.chkSelfAssessment.Text)

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
        End Try
    End Sub


    Private Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Please select Custom Item from the list to perform further operation.")
            Language.setMessage(mstrModuleName, 2, "Are you sure you want to delete this Custom Item?")
            Language.setMessage(mstrModuleName, 3, "Period is mandatory information. Please select Period to continue.")
            Language.setMessage(mstrModuleName, 4, "Note: You are allowed to define only 10 Custom Items per Custom Header.")
            Language.setMessage(mstrModuleName, 5, "Sorry, You cannot edit this Custom Item. Reason Period is already closed.")
            Language.setMessage(mstrModuleName, 6, "Sorry, You cannot delete this Custom Item. Reason Period is already closed.")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class