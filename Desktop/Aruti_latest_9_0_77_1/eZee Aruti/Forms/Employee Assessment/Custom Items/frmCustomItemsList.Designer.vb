﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmCustomItemsList
    Inherits eZee.Common.eZeeForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmCustomItemsList))
        Me.pnlMain = New System.Windows.Forms.Panel
        Me.lvCustomItems = New eZee.Common.eZeeListView(Me.components)
        Me.colhPeriod = New System.Windows.Forms.ColumnHeader
        Me.colhCustomItems = New System.Windows.Forms.ColumnHeader
        Me.colhItemType = New System.Windows.Forms.ColumnHeader
        Me.objcolhGroup = New System.Windows.Forms.ColumnHeader
        Me.objcolhPeriodId = New System.Windows.Forms.ColumnHeader
        Me.colhMode = New System.Windows.Forms.ColumnHeader
        Me.colhViewMode = New System.Windows.Forms.ColumnHeader
        Me.eZeeHeader = New eZee.Common.eZeeHeader
        Me.gbFilterCriteria = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.chkReviewerAssessment = New System.Windows.Forms.CheckBox
        Me.chkAssessorAssessment = New System.Windows.Forms.CheckBox
        Me.chkSelfAssessment = New System.Windows.Forms.CheckBox
        Me.lblViewMode = New System.Windows.Forms.Label
        Me.cboItemType = New System.Windows.Forms.ComboBox
        Me.lblItemType = New System.Windows.Forms.Label
        Me.lblPeriod = New System.Windows.Forms.Label
        Me.cboPeriod = New System.Windows.Forms.ComboBox
        Me.cboCustomHeader = New System.Windows.Forms.ComboBox
        Me.objbtnSearchGroup = New eZee.Common.eZeeGradientButton
        Me.lblCustomHeader = New System.Windows.Forms.Label
        Me.objbtnReset = New eZee.Common.eZeeSearchResetButton(Me.components)
        Me.objbtnSearch = New eZee.Common.eZeeSearchResetButton(Me.components)
        Me.objFooter = New eZee.Common.eZeeFooter
        Me.objlblNotes = New System.Windows.Forms.Label
        Me.btnDelete = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnEdit = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnNew = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnClose = New eZee.Common.eZeeLightButton(Me.components)
        Me.pnlMain.SuspendLayout()
        Me.gbFilterCriteria.SuspendLayout()
        CType(Me.objbtnReset, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.objbtnSearch, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.objFooter.SuspendLayout()
        Me.SuspendLayout()
        '
        'pnlMain
        '
        Me.pnlMain.Controls.Add(Me.lvCustomItems)
        Me.pnlMain.Controls.Add(Me.eZeeHeader)
        Me.pnlMain.Controls.Add(Me.gbFilterCriteria)
        Me.pnlMain.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pnlMain.Location = New System.Drawing.Point(0, 0)
        Me.pnlMain.Name = "pnlMain"
        Me.pnlMain.Size = New System.Drawing.Size(792, 461)
        Me.pnlMain.TabIndex = 0
        '
        'lvCustomItems
        '
        Me.lvCustomItems.BackColorOnChecked = False
        Me.lvCustomItems.ColumnHeaders = Nothing
        Me.lvCustomItems.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.colhPeriod, Me.colhCustomItems, Me.colhItemType, Me.objcolhGroup, Me.objcolhPeriodId, Me.colhMode, Me.colhViewMode})
        Me.lvCustomItems.CompulsoryColumns = ""
        Me.lvCustomItems.FullRowSelect = True
        Me.lvCustomItems.GridLines = True
        Me.lvCustomItems.GroupingColumn = Nothing
        Me.lvCustomItems.HideSelection = False
        Me.lvCustomItems.Location = New System.Drawing.Point(8, 155)
        Me.lvCustomItems.MinColumnWidth = 50
        Me.lvCustomItems.MultiSelect = False
        Me.lvCustomItems.Name = "lvCustomItems"
        Me.lvCustomItems.OptionalColumns = ""
        Me.lvCustomItems.ShowMoreItem = False
        Me.lvCustomItems.ShowSaveItem = False
        Me.lvCustomItems.ShowSelectAll = True
        Me.lvCustomItems.ShowSizeAllColumnsToFit = True
        Me.lvCustomItems.Size = New System.Drawing.Size(772, 245)
        Me.lvCustomItems.Sortable = True
        Me.lvCustomItems.TabIndex = 12
        Me.lvCustomItems.UseCompatibleStateImageBehavior = False
        Me.lvCustomItems.View = System.Windows.Forms.View.Details
        '
        'colhPeriod
        '
        Me.colhPeriod.Tag = "colhPeriod"
        Me.colhPeriod.Text = "Period"
        Me.colhPeriod.Width = 100
        '
        'colhCustomItems
        '
        Me.colhCustomItems.Tag = "colhCustomItems"
        Me.colhCustomItems.Text = "Custom Item"
        Me.colhCustomItems.Width = 350
        '
        'colhItemType
        '
        Me.colhItemType.Tag = "colhItemType"
        Me.colhItemType.Text = "ItemType"
        Me.colhItemType.Width = 100
        '
        'objcolhGroup
        '
        Me.objcolhGroup.Tag = "objcolhGroup"
        Me.objcolhGroup.Width = 0
        '
        'objcolhPeriodId
        '
        Me.objcolhPeriodId.Tag = "objcolhPeriodId"
        Me.objcolhPeriodId.Text = ""
        Me.objcolhPeriodId.Width = 0
        '
        'colhMode
        '
        Me.colhMode.Tag = "colhMode"
        Me.colhMode.Text = "Selection Mode"
        Me.colhMode.Width = 115
        '
        'colhViewMode
        '
        Me.colhViewMode.Tag = "colhViewMode"
        Me.colhViewMode.Text = "Visible In"
        Me.colhViewMode.Width = 100
        '
        'eZeeHeader
        '
        Me.eZeeHeader.BackColor = System.Drawing.SystemColors.Control
        Me.eZeeHeader.BorderColor = System.Drawing.SystemColors.ControlDark
        Me.eZeeHeader.DescriptionForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.eZeeHeader.Dock = System.Windows.Forms.DockStyle.Top
        Me.eZeeHeader.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.eZeeHeader.GradiantStyle = eZee.Common.eZeeHeader.GradientStyle.Central
        Me.eZeeHeader.GradientColor1 = System.Drawing.SystemColors.Window
        Me.eZeeHeader.GradientColor2 = System.Drawing.SystemColors.Control
        Me.eZeeHeader.HeaderTextForeColor = System.Drawing.SystemColors.ControlText
        Me.eZeeHeader.Icon = Nothing
        Me.eZeeHeader.Location = New System.Drawing.Point(0, 0)
        Me.eZeeHeader.Message = ""
        Me.eZeeHeader.Name = "eZeeHeader"
        Me.eZeeHeader.Size = New System.Drawing.Size(792, 58)
        Me.eZeeHeader.TabIndex = 11
        Me.eZeeHeader.Title = "Custom Items List"
        '
        'gbFilterCriteria
        '
        Me.gbFilterCriteria.BorderColor = System.Drawing.Color.Black
        Me.gbFilterCriteria.Checked = False
        Me.gbFilterCriteria.CollapseAllExceptThis = False
        Me.gbFilterCriteria.CollapsedHoverImage = Nothing
        Me.gbFilterCriteria.CollapsedNormalImage = Nothing
        Me.gbFilterCriteria.CollapsedPressedImage = Nothing
        Me.gbFilterCriteria.CollapseOnLoad = False
        Me.gbFilterCriteria.Controls.Add(Me.chkReviewerAssessment)
        Me.gbFilterCriteria.Controls.Add(Me.chkAssessorAssessment)
        Me.gbFilterCriteria.Controls.Add(Me.chkSelfAssessment)
        Me.gbFilterCriteria.Controls.Add(Me.lblViewMode)
        Me.gbFilterCriteria.Controls.Add(Me.cboItemType)
        Me.gbFilterCriteria.Controls.Add(Me.lblItemType)
        Me.gbFilterCriteria.Controls.Add(Me.lblPeriod)
        Me.gbFilterCriteria.Controls.Add(Me.cboPeriod)
        Me.gbFilterCriteria.Controls.Add(Me.cboCustomHeader)
        Me.gbFilterCriteria.Controls.Add(Me.objbtnSearchGroup)
        Me.gbFilterCriteria.Controls.Add(Me.lblCustomHeader)
        Me.gbFilterCriteria.Controls.Add(Me.objbtnReset)
        Me.gbFilterCriteria.Controls.Add(Me.objbtnSearch)
        Me.gbFilterCriteria.ExpandedHoverImage = Nothing
        Me.gbFilterCriteria.ExpandedNormalImage = Nothing
        Me.gbFilterCriteria.ExpandedPressedImage = Nothing
        Me.gbFilterCriteria.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbFilterCriteria.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbFilterCriteria.HeaderHeight = 25
        Me.gbFilterCriteria.HeaderMessage = ""
        Me.gbFilterCriteria.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbFilterCriteria.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbFilterCriteria.HeightOnCollapse = 0
        Me.gbFilterCriteria.LeftTextSpace = 0
        Me.gbFilterCriteria.Location = New System.Drawing.Point(8, 64)
        Me.gbFilterCriteria.Name = "gbFilterCriteria"
        Me.gbFilterCriteria.OpenHeight = 71
        Me.gbFilterCriteria.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbFilterCriteria.ShowBorder = True
        Me.gbFilterCriteria.ShowCheckBox = False
        Me.gbFilterCriteria.ShowCollapseButton = False
        Me.gbFilterCriteria.ShowDefaultBorderColor = True
        Me.gbFilterCriteria.ShowDownButton = False
        Me.gbFilterCriteria.ShowHeader = True
        Me.gbFilterCriteria.Size = New System.Drawing.Size(772, 85)
        Me.gbFilterCriteria.TabIndex = 10
        Me.gbFilterCriteria.Temp = 0
        Me.gbFilterCriteria.Text = "Filter Criteria"
        Me.gbFilterCriteria.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'chkReviewerAssessment
        '
        Me.chkReviewerAssessment.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkReviewerAssessment.Location = New System.Drawing.Point(290, 60)
        Me.chkReviewerAssessment.Name = "chkReviewerAssessment"
        Me.chkReviewerAssessment.Size = New System.Drawing.Size(93, 17)
        Me.chkReviewerAssessment.TabIndex = 359
        Me.chkReviewerAssessment.Text = "Reviewer"
        Me.chkReviewerAssessment.UseVisualStyleBackColor = True
        '
        'chkAssessorAssessment
        '
        Me.chkAssessorAssessment.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkAssessorAssessment.Location = New System.Drawing.Point(191, 60)
        Me.chkAssessorAssessment.Name = "chkAssessorAssessment"
        Me.chkAssessorAssessment.Size = New System.Drawing.Size(93, 17)
        Me.chkAssessorAssessment.TabIndex = 358
        Me.chkAssessorAssessment.Text = "Assessor"
        Me.chkAssessorAssessment.UseVisualStyleBackColor = True
        '
        'chkSelfAssessment
        '
        Me.chkSelfAssessment.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkSelfAssessment.Location = New System.Drawing.Point(92, 60)
        Me.chkSelfAssessment.Name = "chkSelfAssessment"
        Me.chkSelfAssessment.Size = New System.Drawing.Size(93, 17)
        Me.chkSelfAssessment.TabIndex = 357
        Me.chkSelfAssessment.Text = "All"
        Me.chkSelfAssessment.UseVisualStyleBackColor = True
        '
        'lblViewMode
        '
        Me.lblViewMode.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblViewMode.Location = New System.Drawing.Point(11, 60)
        Me.lblViewMode.Name = "lblViewMode"
        Me.lblViewMode.Size = New System.Drawing.Size(75, 15)
        Me.lblViewMode.TabIndex = 356
        Me.lblViewMode.Text = "View Mode"
        Me.lblViewMode.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboItemType
        '
        Me.cboItemType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboItemType.DropDownWidth = 450
        Me.cboItemType.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboItemType.FormattingEnabled = True
        Me.cboItemType.Location = New System.Drawing.Point(621, 33)
        Me.cboItemType.Name = "cboItemType"
        Me.cboItemType.Size = New System.Drawing.Size(136, 21)
        Me.cboItemType.TabIndex = 355
        '
        'lblItemType
        '
        Me.lblItemType.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblItemType.Location = New System.Drawing.Point(550, 35)
        Me.lblItemType.Name = "lblItemType"
        Me.lblItemType.Size = New System.Drawing.Size(65, 17)
        Me.lblItemType.TabIndex = 354
        Me.lblItemType.Text = "Item Type"
        Me.lblItemType.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblPeriod
        '
        Me.lblPeriod.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPeriod.Location = New System.Drawing.Point(11, 36)
        Me.lblPeriod.Name = "lblPeriod"
        Me.lblPeriod.Size = New System.Drawing.Size(75, 15)
        Me.lblPeriod.TabIndex = 351
        Me.lblPeriod.Text = "Period"
        Me.lblPeriod.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboPeriod
        '
        Me.cboPeriod.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboPeriod.DropDownWidth = 200
        Me.cboPeriod.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboPeriod.FormattingEnabled = True
        Me.cboPeriod.Location = New System.Drawing.Point(92, 33)
        Me.cboPeriod.Name = "cboPeriod"
        Me.cboPeriod.Size = New System.Drawing.Size(140, 21)
        Me.cboPeriod.TabIndex = 352
        '
        'cboCustomHeader
        '
        Me.cboCustomHeader.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboCustomHeader.DropDownWidth = 600
        Me.cboCustomHeader.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboCustomHeader.FormattingEnabled = True
        Me.cboCustomHeader.Location = New System.Drawing.Point(326, 33)
        Me.cboCustomHeader.Name = "cboCustomHeader"
        Me.cboCustomHeader.Size = New System.Drawing.Size(191, 21)
        Me.cboCustomHeader.TabIndex = 349
        '
        'objbtnSearchGroup
        '
        Me.objbtnSearchGroup.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchGroup.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchGroup.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchGroup.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchGroup.BorderSelected = False
        Me.objbtnSearchGroup.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchGroup.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objbtnSearchGroup.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchGroup.Location = New System.Drawing.Point(523, 33)
        Me.objbtnSearchGroup.Name = "objbtnSearchGroup"
        Me.objbtnSearchGroup.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchGroup.TabIndex = 350
        '
        'lblCustomHeader
        '
        Me.lblCustomHeader.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCustomHeader.Location = New System.Drawing.Point(238, 36)
        Me.lblCustomHeader.Name = "lblCustomHeader"
        Me.lblCustomHeader.Size = New System.Drawing.Size(82, 15)
        Me.lblCustomHeader.TabIndex = 348
        Me.lblCustomHeader.Text = "Custom Header"
        Me.lblCustomHeader.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objbtnReset
        '
        Me.objbtnReset.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.objbtnReset.BackColor = System.Drawing.Color.Transparent
        Me.objbtnReset.ButtonType = eZee.Common.eZeeSearchResetButton.EnumButtonType.Reset
        Me.objbtnReset.Image = CType(resources.GetObject("objbtnReset.Image"), System.Drawing.Image)
        Me.objbtnReset.Location = New System.Drawing.Point(745, 0)
        Me.objbtnReset.Name = "objbtnReset"
        Me.objbtnReset.ResultMessage = ""
        Me.objbtnReset.SearchMessage = ""
        Me.objbtnReset.Size = New System.Drawing.Size(24, 24)
        Me.objbtnReset.TabIndex = 8
        Me.objbtnReset.TabStop = False
        '
        'objbtnSearch
        '
        Me.objbtnSearch.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.objbtnSearch.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearch.ButtonType = eZee.Common.eZeeSearchResetButton.EnumButtonType.Search
        Me.objbtnSearch.Image = CType(resources.GetObject("objbtnSearch.Image"), System.Drawing.Image)
        Me.objbtnSearch.Location = New System.Drawing.Point(722, 0)
        Me.objbtnSearch.Name = "objbtnSearch"
        Me.objbtnSearch.ResultMessage = ""
        Me.objbtnSearch.SearchMessage = ""
        Me.objbtnSearch.Size = New System.Drawing.Size(24, 24)
        Me.objbtnSearch.TabIndex = 7
        Me.objbtnSearch.TabStop = False
        '
        'objFooter
        '
        Me.objFooter.BorderColor = System.Drawing.Color.Silver
        Me.objFooter.Controls.Add(Me.objlblNotes)
        Me.objFooter.Controls.Add(Me.btnDelete)
        Me.objFooter.Controls.Add(Me.btnEdit)
        Me.objFooter.Controls.Add(Me.btnNew)
        Me.objFooter.Controls.Add(Me.btnClose)
        Me.objFooter.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.objFooter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objFooter.GradiantStyle = eZee.Common.eZeeFooter.GradientStyle.Up
        Me.objFooter.GradientColor1 = System.Drawing.SystemColors.Control
        Me.objFooter.GradientColor2 = System.Drawing.SystemColors.Control
        Me.objFooter.Location = New System.Drawing.Point(0, 406)
        Me.objFooter.Name = "objFooter"
        Me.objFooter.Size = New System.Drawing.Size(792, 55)
        Me.objFooter.TabIndex = 8
        '
        'objlblNotes
        '
        Me.objlblNotes.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objlblNotes.ForeColor = System.Drawing.Color.Maroon
        Me.objlblNotes.Location = New System.Drawing.Point(12, 13)
        Me.objlblNotes.Name = "objlblNotes"
        Me.objlblNotes.Size = New System.Drawing.Size(356, 30)
        Me.objlblNotes.TabIndex = 8
        '
        'btnDelete
        '
        Me.btnDelete.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnDelete.BackColor = System.Drawing.Color.White
        Me.btnDelete.BackgroundImage = CType(resources.GetObject("btnDelete.BackgroundImage"), System.Drawing.Image)
        Me.btnDelete.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnDelete.BorderColor = System.Drawing.Color.Empty
        Me.btnDelete.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnDelete.FlatAppearance.BorderSize = 0
        Me.btnDelete.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnDelete.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnDelete.ForeColor = System.Drawing.Color.Black
        Me.btnDelete.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnDelete.GradientForeColor = System.Drawing.Color.Black
        Me.btnDelete.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnDelete.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnDelete.Location = New System.Drawing.Point(580, 13)
        Me.btnDelete.Name = "btnDelete"
        Me.btnDelete.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnDelete.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnDelete.Size = New System.Drawing.Size(97, 30)
        Me.btnDelete.TabIndex = 6
        Me.btnDelete.Text = "&Delete"
        Me.btnDelete.UseVisualStyleBackColor = True
        '
        'btnEdit
        '
        Me.btnEdit.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnEdit.BackColor = System.Drawing.Color.White
        Me.btnEdit.BackgroundImage = CType(resources.GetObject("btnEdit.BackgroundImage"), System.Drawing.Image)
        Me.btnEdit.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnEdit.BorderColor = System.Drawing.Color.Empty
        Me.btnEdit.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnEdit.FlatAppearance.BorderSize = 0
        Me.btnEdit.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnEdit.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnEdit.ForeColor = System.Drawing.Color.Black
        Me.btnEdit.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnEdit.GradientForeColor = System.Drawing.Color.Black
        Me.btnEdit.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnEdit.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnEdit.Location = New System.Drawing.Point(477, 13)
        Me.btnEdit.Name = "btnEdit"
        Me.btnEdit.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnEdit.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnEdit.Size = New System.Drawing.Size(97, 30)
        Me.btnEdit.TabIndex = 5
        Me.btnEdit.Text = "&Edit"
        Me.btnEdit.UseVisualStyleBackColor = True
        '
        'btnNew
        '
        Me.btnNew.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnNew.BackColor = System.Drawing.Color.White
        Me.btnNew.BackgroundImage = CType(resources.GetObject("btnNew.BackgroundImage"), System.Drawing.Image)
        Me.btnNew.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnNew.BorderColor = System.Drawing.Color.Empty
        Me.btnNew.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnNew.FlatAppearance.BorderSize = 0
        Me.btnNew.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnNew.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnNew.ForeColor = System.Drawing.Color.Black
        Me.btnNew.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnNew.GradientForeColor = System.Drawing.Color.Black
        Me.btnNew.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnNew.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnNew.Location = New System.Drawing.Point(374, 13)
        Me.btnNew.Name = "btnNew"
        Me.btnNew.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnNew.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnNew.Size = New System.Drawing.Size(97, 30)
        Me.btnNew.TabIndex = 4
        Me.btnNew.Text = "&New"
        Me.btnNew.UseVisualStyleBackColor = True
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnClose.BackColor = System.Drawing.Color.White
        Me.btnClose.BackgroundImage = CType(resources.GetObject("btnClose.BackgroundImage"), System.Drawing.Image)
        Me.btnClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnClose.BorderColor = System.Drawing.Color.Empty
        Me.btnClose.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnClose.FlatAppearance.BorderSize = 0
        Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClose.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.ForeColor = System.Drawing.Color.Black
        Me.btnClose.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnClose.GradientForeColor = System.Drawing.Color.Black
        Me.btnClose.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Location = New System.Drawing.Point(683, 13)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Size = New System.Drawing.Size(97, 30)
        Me.btnClose.TabIndex = 7
        Me.btnClose.Text = "&Close"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'frmCustomItemsList
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(792, 461)
        Me.Controls.Add(Me.objFooter)
        Me.Controls.Add(Me.pnlMain)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmCustomItemsList"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Custom Items List"
        Me.pnlMain.ResumeLayout(False)
        Me.gbFilterCriteria.ResumeLayout(False)
        CType(Me.objbtnReset, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.objbtnSearch, System.ComponentModel.ISupportInitialize).EndInit()
        Me.objFooter.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents pnlMain As System.Windows.Forms.Panel
    Friend WithEvents objFooter As eZee.Common.eZeeFooter
    Friend WithEvents btnDelete As eZee.Common.eZeeLightButton
    Friend WithEvents btnEdit As eZee.Common.eZeeLightButton
    Friend WithEvents btnNew As eZee.Common.eZeeLightButton
    Friend WithEvents btnClose As eZee.Common.eZeeLightButton
    Friend WithEvents gbFilterCriteria As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents lblPeriod As System.Windows.Forms.Label
    Friend WithEvents cboPeriod As System.Windows.Forms.ComboBox
    Friend WithEvents cboCustomHeader As System.Windows.Forms.ComboBox
    Friend WithEvents lblCustomHeader As System.Windows.Forms.Label
    Friend WithEvents objbtnSearchGroup As eZee.Common.eZeeGradientButton
    Friend WithEvents objbtnReset As eZee.Common.eZeeSearchResetButton
    Friend WithEvents objbtnSearch As eZee.Common.eZeeSearchResetButton
    Friend WithEvents eZeeHeader As eZee.Common.eZeeHeader
    Friend WithEvents lvCustomItems As eZee.Common.eZeeListView
    Friend WithEvents colhPeriod As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhCustomItems As System.Windows.Forms.ColumnHeader
    Friend WithEvents objcolhGroup As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhItemType As System.Windows.Forms.ColumnHeader
    Friend WithEvents objcolhPeriodId As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhMode As System.Windows.Forms.ColumnHeader
    Friend WithEvents cboItemType As System.Windows.Forms.ComboBox
    Friend WithEvents lblItemType As System.Windows.Forms.Label
    Friend WithEvents lblViewMode As System.Windows.Forms.Label
    Friend WithEvents colhViewMode As System.Windows.Forms.ColumnHeader
    Friend WithEvents chkReviewerAssessment As System.Windows.Forms.CheckBox
    Friend WithEvents chkAssessorAssessment As System.Windows.Forms.CheckBox
    Friend WithEvents chkSelfAssessment As System.Windows.Forms.CheckBox
    Friend WithEvents objlblNotes As System.Windows.Forms.Label
End Class
