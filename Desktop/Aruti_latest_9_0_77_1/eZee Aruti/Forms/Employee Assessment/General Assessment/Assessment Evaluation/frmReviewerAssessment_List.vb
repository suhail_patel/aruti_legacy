﻿'ENHANCEMENT : ANY CHANGES IN DESKTOP SHOULD ALSO BE IMPLEMENTED IN WEB WITHOUT FAIL.
Option Strict On

#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data

#End Region

Public Class frmReviewerAssessment_List

#Region " Private Variables "

    Private ReadOnly mstrModuleName As String = "frmReviewerAssessment_List"
    Private objAssessor As clsassess_analysis_master
    'S.SANDEEP [ 05 MARCH 2012 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Private mstrAdvanceFilter As String = String.Empty
    'S.SANDEEP [ 05 MARCH 2012 ] -- END

#End Region

#Region " Private Methods "

    Private Sub FillCombo()
        Dim dsFill As DataSet = Nothing
        Try
            dsFill = Nothing
            Dim objYear As New clsMasterData

            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'dsFill = objYear.getComboListPAYYEAR("Year", True, , , , True)
            dsFill = objYear.getComboListPAYYEAR(FinancialYear._Object._YearUnkid, FinancialYear._Object._FinancialYear_Name, Company._Object._Companyunkid, "Year", True, True)
            'S.SANDEEP [04 JUN 2015] -- END

            cboYear.ValueMember = "Id"
            cboYear.DisplayMember = "name"
            cboYear.DataSource = dsFill.Tables("Year")

            dsFill = Nothing
            Dim objPeriod As New clscommom_period_Tran
            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'dsFill = objPeriod.getListForCombo(enModuleReference.Assessment, CInt(cboYear.SelectedValue), "Period", True, 0)
            dsFill = objPeriod.getListForCombo(enModuleReference.Assessment, CInt(cboYear.SelectedValue), FinancialYear._Object._DatabaseName, FinancialYear._Object._Database_Start_Date, "Period", True, 0)
            'Sohail (21 Aug 2015) -- End
            cboPeriod.ValueMember = "periodunkid"
            cboPeriod.DisplayMember = "name"
            cboPeriod.DataSource = dsFill.Tables("Period")



            'S.SANDEEP [ 13 NOV 2013 ] -- START
            'dsFill = Nothing
            'Dim objEmployee As New clsEmployee_Master

            ''Anjan (17 Apr 2012)-Start
            ''ENHANCEMENT : TRA COMMENTS on Andrew sir's Request
            ''dsFill = objEmployee.GetEmployeeList("List", True, Not ConfigParameter._Object._IsIncludeInactiveEmp)
            'If ConfigParameter._Object._IsIncludeInactiveEmp = False Then
            '    dsFill = objEmployee.GetEmployeeList("List", True, , , , , , , , , , , , , eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate))
            'Else
            '    dsFill = objEmployee.GetEmployeeList("List", True, , , , , , , , , , , , , ConfigParameter._Object._CurrentDateAndTime, ConfigParameter._Object._CurrentDateAndTime)
            'End If
            ''Anjan (17 Apr 2012)-End 


            'Dim dtView As DataView = dsFill.Tables(0).DefaultView
            'If ConfigParameter._Object._ReviewerInfo <> Nothing Then
            '    dtView.RowFilter = "employeeunkid NOT IN(" & ConfigParameter._Object._ReviewerInfo.Split(CChar("|"))(0) & ")"
            'End If

            'cboEmployee.ValueMember = "employeeunkid"
            'cboEmployee.DisplayMember = "employeename"
            'cboEmployee.DataSource = dtView.ToTable
            'S.SANDEEP [ 13 NOV 2013 ] -- END





            'S.SANDEEP [ 14 AUG 2013 ] -- START
            'ENHANCEMENT : TRA CHANGES
            Dim objAssessGroup As New clsassess_group_master
            dsFill = objAssessGroup.getListForCombo("Group", True)
            cboGroup.ValueMember = "assessgroupunkid"
            cboGroup.DisplayMember = "name"
            cboGroup.DataSource = dsFill.Tables(0)
            'S.SANDEEP [ 14 AUG 2013 ] -- END

            'S.SANDEEP [ 13 NOV 2013 ] -- START
            Dim objReviewr As New clsassess_analysis_master
            dsFill = objReviewr.getAssessorComboList("List", True, True, User._Object._Userunkid)
            With cboReviewer
                .ValueMember = "Id"
                .DisplayMember = "Name"
                .DataSource = dsFill.Tables("List")
                .SelectedValue = 0
            End With
            'S.SANDEEP [ 13 NOV 2013 ] -- END
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        End Try
    End Sub

    Private Sub FillList()
        Dim lvItem As ListViewItem
        Dim dsList As New DataSet
        Dim dtTable As DataTable
        Dim StrSearching As String = String.Empty
        Try

            'S.SANDEEP [28 MAY 2015] -- START
            'ENHANCEMENT : NEW PRIVILEGE (Performance Assessment)
            'If User._Object.Privilege._AllowToViewReviewerAssessmentList = True Then                'Pinkal (02-Jul-2012) -- Start
            'S.SANDEEP [28 MAY 2015] -- END



            dsList = objAssessor.GetList("List", enAssessmentMode.REVIEWER_ASSESSMENT)

            'S.SANDEEP [ 04 FEB 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            Dim dsPercent As New DataSet
            Dim blnFlag As Boolean = False

                'S.SANDEEP [ 22 OCT 2013 ] -- START
                'ENHANCEMENT : ENHANCEMENT
                'dsPercent = objAssessor.Get_Percentage(enAssessmentMode.REVIEWER_ASSESSMENT)
                dsPercent = objAssessor.Get_Percentage(enAssessmentMode.REVIEWER_ASSESSMENT, ConfigParameter._Object._ConsiderItemWeightAsNumber)
                'S.SANDEEP [ 22 OCT 2013 ] -- END

            If dsPercent.Tables(0).Rows.Count > 0 Then
                blnFlag = True
            End If
            'S.SANDEEP [ 04 FEB 2012 ] -- END


                'S.SANDEEP [ 13 NOV 2013 ] -- START
                If CInt(cboReviewer.SelectedValue) > 0 Then
                    StrSearching &= "AND assessormasterunkid = " & CInt(cboReviewer.SelectedValue)
                End If
                'S.SANDEEP [ 13 NOV 2013 ] -- END

            If CInt(cboEmployee.SelectedValue) > 0 Then
                StrSearching &= "AND assessedemployeeunkid = " & CInt(cboEmployee.SelectedValue)
            End If

            If CInt(cboYear.SelectedValue) > 0 Then
                StrSearching &= "AND yearunkid = " & CInt(cboYear.SelectedValue)
            End If

            If CInt(cboPeriod.SelectedValue) > 0 Then
                StrSearching &= "AND periodunkid = " & CInt(cboPeriod.SelectedValue)
            End If

            If dtpAssessmentdate.Checked = True Then
                StrSearching &= "AND assessmentdate = '" & eZeeDate.convertDate(dtpAssessmentdate.Value) & "'"
            End If

            If chkShowCommited.CheckState = CheckState.Checked And chkShowUncommited.CheckState = CheckState.Unchecked Then
                    StrSearching &= "AND iscommitted = " & True & " "
            ElseIf chkShowCommited.CheckState = CheckState.Unchecked And chkShowUncommited.CheckState = CheckState.Checked Then
                    StrSearching &= "AND iscommitted = " & False & " "
            End If

                'S.SANDEEP [ 14 AUG 2013 ] -- START
                'ENHANCEMENT : TRA CHANGES
                If CInt(cboGroup.SelectedValue) > 0 Then
                    StrSearching &= "AND assessgroupunkid = '" & CInt(cboGroup.SelectedValue) & "' "
                End If
                'S.SANDEEP [ 14 AUG 2013 ] -- END

            'S.SANDEEP [ 05 MARCH 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            If mstrAdvanceFilter.Length > 0 Then
                StrSearching &= "AND " & mstrAdvanceFilter
            End If
            'S.SANDEEP [ 05 MARCH 2012 ] -- END

            If StrSearching.Length > 0 Then
                StrSearching = StrSearching.Substring(3)
                dtTable = New DataView(dsList.Tables(0), StrSearching, "Reviewer", DataViewRowState.CurrentRows).ToTable
            Else
                dtTable = New DataView(dsList.Tables(0), "", "Reviewer", DataViewRowState.CurrentRows).ToTable
            End If

            lvReviewerList.Items.Clear()

            For Each dtRow As DataRow In dtTable.Rows
                lvItem = New ListViewItem

                lvItem.Text = dtRow.Item("EmpName").ToString
                lvItem.SubItems.Add(eZeeDate.convertDate(dtRow.Item("assessmentdate").ToString).ToShortDateString)
                lvItem.SubItems.Add(dtRow.Item("PName").ToString)
                lvItem.SubItems(colhAssessmentPeriod.Index).Tag = dtRow.Item("Sid")
                lvItem.SubItems.Add(dtRow.Item("YearName").ToString)
                lvItem.SubItems.Add(dtRow.Item("Reviewer").ToString)

                lvItem.Tag = dtRow.Item("analysisunkid")

                If CBool(dtRow.Item("iscommitted")) = True Then
                    lvItem.ForeColor = Color.Blue
                End If

                'S.SANDEEP [ 04 FEB 2012 ] -- START
                'ENHANCEMENT : TRA CHANGES
                If blnFlag Then
                        'S.SANDEEP [ 14 AUG 2013 ] -- START
                        'ENHANCEMENT : TRA CHANGES
                        'Dim dTemp() As DataRow = dsPercent.Tables(0).Select("EmpId = '" & CInt(dtRow.Item("assessedemployeeunkid")) & "' AND Pid = '" & CInt(dtRow.Item("periodunkid")) & "'")
                        Dim dTemp() As DataRow = dsPercent.Tables(0).Select("EmpId = '" & CInt(dtRow.Item("assessedemployeeunkid")) & "' AND Pid = '" & CInt(dtRow.Item("periodunkid")) & "' AND analysisunkid = '" & CInt(dtRow.Item("analysisunkid")) & "'")
                        'S.SANDEEP [ 14 AUG 2013 ] -- END
                        If dTemp.Length > 0 Then
                            lvItem.SubItems.Add(dTemp(0)("TotalPercent").ToString)
                            'S.SANDEEP [ 28 JAN 2014 ] -- START
                        Else
                            lvItem.SubItems.Add("")
                            'S.SANDEEP [ 28 JAN 2014 ] -- END
                        End If
                    End If
                    'S.SANDEEP [ 04 FEB 2012 ] -- END

                'S.SANDEEP [ 05 MARCH 2012 ] -- START
                'ENHANCEMENT : TRA CHANGES
                lvItem.SubItems.Add(dtRow.Item("periodunkid").ToString)
                'S.SANDEEP [ 05 MARCH 2012 ] -- END

                    'S.SANDEEP [ 14 AUG 2013 ] -- START
                    'ENHANCEMENT : TRA CHANGES
                    lvItem.SubItems.Add(dtRow.Item("Assessment_Group").ToString)
                    'S.SANDEEP [ 14 AUG 2013 ] -- END


                    'S.SANDEEP [ 10 SEPT 2013 ] -- START
                    'ENHANCEMENT : TRA CHANGES
                    lvItem.SubItems.Add(dtRow.Item("assessedemployeeunkid").ToString)
                    'S.SANDEEP [ 10 SEPT 2013 ] -- END



                lvReviewerList.Items.Add(lvItem)
            Next

            lvReviewerList.GridLines = False

            lvReviewerList.GroupingColumn = colhAssessor
            lvReviewerList.DisplayGroups(True)

            If lvReviewerList.Items.Count > 4 Then
                    colhEmployee.Width = 220 - 20
            Else
                    colhEmployee.Width = 220
            End If

            'End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillList", mstrModuleName)
        End Try
    End Sub

    Private Sub SetVisibility()

        Try

            'S.SANDEEP [28 MAY 2015] -- START
            'ENHANCEMENT : NEW PRIVILEGE (Performance Assessment)

            ''S.SANDEEP [ 16 MAY 2012 ] -- START
            ''ENHANCEMENT : TRA USER PRIVILEGE CHANGES
            'btnNew.Enabled = User._Object.Privilege._AllowToAddReviewerGeneralAssessment
            'btnEdit.Enabled = User._Object.Privilege._AllowToEditReviewerGeneralAssessment
            'btnDelete.Enabled = User._Object.Privilege._AllowToDeleteReviewerGeneralAssessment
            ''S.SANDEEP [ 16 MAY 2012 ] -- END

            ''S.SANDEEP [ 05 MARCH 2012 ] -- START
            ''ENHANCEMENT : TRA CHANGES
            'btnUnlockCommit.Enabled = User._Object.Privilege._Allow_UnlockCommittedGeneralAssessment
            ''S.SANDEEP [ 05 MARCH 2012 ] -- END

            'S.SANDEEP [28 MAY 2015] -- END



            chkShowCommited.CheckState = CheckState.Checked
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetVisibility", mstrModuleName)
        End Try

    End Sub


#End Region

#Region " Form's Events "

    Private Sub frmReviewerAssessment_List_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        objAssessor = New clsassess_analysis_master
        Try

            Call Set_Logo(Me, gApplicationType)
            Call FillCombo()
            Call SetVisibility()

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmReviewerAssessment_List_Load", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Buttons "

    Private Sub btnNew_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNew.Click
        'S.SANDEEP [ 24 APR 2014 ] -- START
        'Dim frm As New frmEvaluation
        Dim frm As New frmTabular_Evaluation
        'S.SANDEEP [ 24 APR 2014 ] -- END
        Try

            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If

            If frm.displayDialog(-1, enAction.ADD_CONTINUE, enAssessmentMode.REVIEWER_ASSESSMENT) Then
                Call FillList()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnNew_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub btnEdit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEdit.Click
        If lvReviewerList.SelectedItems.Count <= 0 Then
            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please select atleast one Assessment to perform Operation on it."), enMsgBoxStyle.Information)
            Exit Sub
        End If

        If CInt(lvReviewerList.SelectedItems(0).SubItems(colhAssessmentPeriod.Index).Tag) = enStatusType.Close Then
            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 6, "Sorry, you cannot edit this information. Reason : Period is closed."), enMsgBoxStyle.Information)
            Exit Sub
        End If

        'S.SANDEEP [ 24 APR 2014 ] -- START
        'Dim frm As New frmEvaluation
        Dim frm As New frmTabular_Evaluation
        'S.SANDEEP [ 24 APR 2014 ] -- END
        Try

            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If

            If frm.displayDialog(CInt(lvReviewerList.SelectedItems(0).Tag), enAction.EDIT_ONE, enAssessmentMode.REVIEWER_ASSESSMENT) Then
                Call FillList()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnEdit_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub btnDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDelete.Click
        If lvReviewerList.SelectedItems.Count <= 0 Then
            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please select atleast one Assessment to perform Operation on it."), enMsgBoxStyle.Information)
            Exit Sub
        End If
        Try
            If CInt(lvReviewerList.SelectedItems(0).SubItems(colhAssessmentPeriod.Index).Tag) = enStatusType.Close Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Sorry, you cannot delete this information. Reason : Period is closed."), enMsgBoxStyle.Information)
                Exit Sub
            End If

            If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Are you sure you want to delete this Assessment?"), CType(enMsgBoxStyle.YesNo + enMsgBoxStyle.Information, enMsgBoxStyle)) = Windows.Forms.DialogResult.Yes Then
                Dim frm As New frmReasonSelection
                Dim mstrVoidReason As String = String.Empty

                If User._Object._Isrighttoleft = True Then
                    frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                    frm.RightToLeftLayout = True
                    Call Language.ctlRightToLeftlayOut(frm)
                End If

                frm.displayDialog(enVoidCategoryType.ASSESSMENT, mstrVoidReason)
                If mstrVoidReason.Length <= 0 Then
                    Exit Sub
                Else
                    objAssessor._Voidreason = mstrVoidReason
                End If
                frm = Nothing
                objAssessor._Isvoid = True
                objAssessor._Voiddatetime = ConfigParameter._Object._CurrentDateAndTime
                objAssessor._Voiduserunkid = User._Object._Userunkid

                'S.SANDEEP [ 01 JUL 2014 ] -- START
                'objAssessor.Delete(CInt(lvReviewerList.SelectedItems(0).Tag))
                objAssessor.Delete(CInt(lvReviewerList.SelectedItems(0).Tag), CInt(lvReviewerList.SelectedItems(0).SubItems(objcolhPeriodId.Index).Text))
                'S.SANDEEP [ 01 JUL 2014 ] -- END

                If objAssessor._Message <> "" Then
                    eZeeMsgBox.Show(objAssessor._Message, enMsgBoxStyle.Information)
                Else
                    lvReviewerList.SelectedItems(0).Remove()
                End If

            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnDelete_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnClose_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnSearchEmployee_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchEmployee.Click
        Dim frm As New frmCommonSearch
        Try
            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If

            frm.ValueMember = cboEmployee.ValueMember
            frm.DisplayMember = cboEmployee.DisplayMember

            'S.SANDEEP [ 13 NOV 2013 ] -- START
            'frm.CodeMember = "employeecode"
            frm.CodeMember = "Code"
            'S.SANDEEP [ 13 NOV 2013 ] -- END
            frm.DataSource = CType(cboEmployee.DataSource, DataTable)

            If frm.DisplayDialog Then
                cboEmployee.SelectedValue = frm.SelectedValue
                cboEmployee.Focus()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchEmployee_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub objbtnSearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearch.Click
        Try
            Call FillList()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearch_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnReset.Click
        Try
            cboEmployee.SelectedValue = 0
            cboPeriod.SelectedValue = 0
            cboYear.SelectedValue = 0
            dtpAssessmentdate.Checked = False
            chkShowCommited.CheckState = CheckState.Checked
            'S.SANDEEP [ 05 MARCH 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            mstrAdvanceFilter = String.Empty
            'S.SANDEEP [ 05 MARCH 2012 ] -- END

            Call FillList()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnReset_Click", mstrModuleName)
        End Try
    End Sub

    'S.SANDEEP [ 05 MARCH 2012 ] -- START
    'ENHANCEMENT : TRA CHANGES
    'Private Sub btnMakeCommit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnMakeCommit.Click
    '    Try
    '        If lvReviewerList.CheckedItems.Count <= 0 Then
    '            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 7, "Please check atleast one information to set as commit."), enMsgBoxStyle.Information)
    '            Exit Sub
    '        End If

    '        Dim objAnalysis As New clsassess_analysis_master
    '        For Each LVI As ListViewItem In lvReviewerList.CheckedItems
    '            objAnalysis._Analysisunkid = CInt(LVI.Tag)
    '            objAnalysis._Iscommitted = True
    '            objAnalysis.Update()
    '        Next
    '        objAnalysis = Nothing
    '        Call FillList()
    '    Catch ex As Exception
    '        DisplayError.Show("-1", ex.Message, "btnMakeCommit_Click", mstrModuleName)
    '    Finally
    '    End Try
    'End Sub
    'Private Sub btnUnlockCommit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnUnlockCommit.Click
    '    Try
    '        If lvReviewerList.SelectedItems.Count > 0 Then
    '            If CInt(lvReviewerList.SelectedItems(0).SubItems(colhAssessmentPeriod.Index).Tag) = enStatusType.Open Then
    '                If objAssessor.Unlock_Commit(CInt(lvReviewerList.SelectedItems(0).SubItems(objcolhPeriodId.Index).Text)) = True Then
    '                    objAssessor._Analysisunkid = CInt(lvReviewerList.SelectedItems(0).Tag)
    '                    objAssessor._Iscommitted = False
    '                    'S.SANDEEP [ 14 JUNE 2012 ] -- START
    '                    'ENHANCEMENT : TRA CHANGES
    '                    objAssessor._Committeddatetime = Nothing
    '                    'S.SANDEEP [ 14 JUNE 2012 ] -- END
    '                    objAssessor.Update()
    '                    If objAssessor._Message <> "" Then
    '                        eZeeMsgBox.Show(objAssessor._Message)
    '                    End If
    '                    Call FillList()
    '                Else
    '                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 9, "Sorry, you cannot unlock this commited information. Reason : Its already linked with Appraisal."), enMsgBoxStyle.Information)
    '                    Exit Sub
    '                End If
    '            Else
    '                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 10, "Sorry, you cannot unlock this commited information. Reason : Period is already Closed."), enMsgBoxStyle.Information)
    '                Exit Sub
    '            End If
    '        End If
    '    Catch ex As Exception
    '        DisplayError.Show("-1", ex.Message, "btnUnlockCommit_Click", mstrModuleName)
    '    Finally
    '    End Try
    'End Sub
    'S.SANDEEP [ 05 MARCH 2012 ] -- END

    'S.SANDEEP [ 14 AUG 2013 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Private Sub objbtnSearchGroup_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchGroup.Click
        Dim frm As New frmCommonSearch
        Try
            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If

            frm.ValueMember = cboGroup.ValueMember
            frm.DisplayMember = cboGroup.DisplayMember
            frm.CodeMember = ""
            frm.DataSource = CType(cboGroup.DataSource, DataTable)
            If frm.DisplayDialog Then
                cboGroup.SelectedValue = frm.SelectedValue
                cboGroup.Focus()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchGroup_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub
    'S.SANDEEP [ 14 AUG 2013 ] -- END

    'S.SANDEEP [ 13 NOV 2013 ] -- START
    Private Sub objbtnSearchReviewer_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchReviewer.Click
        Dim frm As New frmCommonSearch
        Try
            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If
            frm.ValueMember = cboReviewer.ValueMember
            frm.DisplayMember = cboReviewer.DisplayMember
            frm.CodeMember = "Code"
            frm.DataSource = CType(cboReviewer.DataSource, DataTable)
            If frm.DisplayDialog Then
                cboReviewer.SelectedValue = frm.SelectedValue
                cboReviewer.Focus()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchReviewer_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub
    'S.SANDEEP [ 13 NOV 2013 ] -- END


#End Region

#Region " Controls "

    Private Sub lvReviewerList_ItemChecked(ByVal sender As Object, ByVal e As System.Windows.Forms.ItemCheckedEventArgs) Handles lvReviewerList.ItemChecked
        Try
            If lvReviewerList.CheckedItems.Count <= 0 Then Exit Sub
            If e.Item.ForeColor = Color.Blue Then e.Item.Checked = False
            If CInt(e.Item.SubItems(colhAssessmentPeriod.Index).Tag) = enStatusType.Close Then e.Item.Checked = False
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lvReviewerList_ItemChecked", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub lvReviewerList_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lvReviewerList.SelectedIndexChanged
        Try
            If lvReviewerList.SelectedItems.Count <= 0 Then Exit Sub
            If lvReviewerList.SelectedItems(0).ForeColor = Color.Blue Then
                btnEdit.Enabled = False : btnDelete.Enabled = False : btnUnlockCommit.Enabled = True 'S.SANDEEP [ 05 MARCH 2012 btnMakeCommit.Enabled = False ] -- START -- END
            Else
                btnEdit.Enabled = True : btnDelete.Enabled = True : btnUnlockCommit.Enabled = False 'S.SANDEEP [ 05 MARCH 2012 btnMakeCommit.Enabled = True] -- START -- END
            End If
            'S.SANDEEP [ 05 MARCH 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            'btnUnlockCommit.Enabled = User._Object.Privilege._Allow_UnlockCommittedGeneralAssessment
            'S.SANDEEP [ 05 MARCH 2012 ] -- END
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lvReviewerList_SelectedIndexChanged", mstrModuleName)
        Finally
        End Try
    End Sub

    'S.SANDEEP [ 05 MARCH 2012 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Private Sub lnkAllocation_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lnkAllocation.LinkClicked
        Try
            Dim frm As New frmAdvanceSearch
            frm.ShowDialog()
            mstrAdvanceFilter = frm._GetFilterString
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lnkAllocation_LinkClicked", mstrModuleName)
        End Try
    End Sub
    'S.SANDEEP [ 05 MARCH 2012 ] -- END


    'S.SANDEEP [ 14 AUG 2013 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Private Sub mnuUnlockCommitted_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuUnlockCommitted.Click
        Try
            If lvReviewerList.SelectedItems.Count > 0 Then
                If CInt(lvReviewerList.SelectedItems(0).SubItems(colhAssessmentPeriod.Index).Tag) = enStatusType.Open Then
                    'S.SANDEEP [ 10 SEPT 2013 ] -- START
                    'ENHANCEMENT : TRA CHANGES
                    'If objAssessor.Unlock_Commit(CInt(lvReviewerList.SelectedItems(0).SubItems(objcolhPeriodId.Index).Text)) = True Then
                    If objAssessor.Unlock_Commit(CInt(lvReviewerList.SelectedItems(0).SubItems(objcolhPeriodId.Index).Text), _
                                                 CInt(lvReviewerList.SelectedItems(0).SubItems(objcolhEmpId.Index).Text)) = True Then
                        'S.SANDEEP [ 10 SEPT 2013 ] -- END
                        objAssessor._Analysisunkid = CInt(lvReviewerList.SelectedItems(0).Tag)
                        objAssessor._Iscommitted = False
                        objAssessor._Committeddatetime = Nothing
                        objAssessor.Update()
                        If objAssessor._Message <> "" Then
                            eZeeMsgBox.Show(objAssessor._Message)
                        End If
                        Call FillList()
                    Else
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Sorry, you cannot unlock this commited information. Reason : Its already linked with Appraisal."), enMsgBoxStyle.Information)
                        Exit Sub
                    End If
                Else
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "Sorry, you cannot unlock this commited information. Reason : Period is already Closed."), enMsgBoxStyle.Information)
                    Exit Sub
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "mnuUnlockCommitted_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub mnuGetFileFormat_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuGetFileFormat.Click
        'S.SANDEEP [12-Jan-2018] -- START
        'ISSUE/ENHANCEMENT : REF-ID # 0001843
        'Dim IExcel As New ExcelData
        'S.SANDEEP [12-Jan-2018] -- END
        Dim dsList As New DataSet
        Dim path As String = String.Empty
        Dim strFilePath As String = String.Empty
        Dim dlgSaveFile As New SaveFileDialog
        Dim ObjFile As System.IO.FileInfo
        Try
            dlgSaveFile.Filter = "Execl files(*.xlsx)|*.xlsx"
            If dlgSaveFile.ShowDialog = Windows.Forms.DialogResult.OK Then
                ObjFile = New System.IO.FileInfo(dlgSaveFile.FileName)
                strFilePath = ObjFile.DirectoryName & "\"
                strFilePath &= ObjFile.Name

                Dim dTable As New DataTable

                dTable.Columns.Add("Assess_Period") : dTable.Columns.Add("Reviewer_Code") : dTable.Columns.Add("Employee_Code") : dTable.Columns.Add("Assess_Group_Code")
                dTable.Columns.Add("Assess_Item_Code") : dTable.Columns.Add("Assess_SubItem_Code") : dTable.Columns.Add("Result")
                dTable.Columns.Add("Improvement") : dTable.Columns.Add("Activity") : dTable.Columns.Add("Support_Required")
                dTable.Columns.Add("Other_Training") : dTable.Columns.Add("Time_Frame_Date") : dTable.Columns.Add("Training_Learning_Objective")
                dTable.Columns.Add("Personal_Improvement") : dTable.Columns.Add("Personal_Activity") : dTable.Columns.Add("Personal_Support_Required")
                dTable.Columns.Add("Personal_Other_Training") : dTable.Columns.Add("Personal_Time_Frame_Date") : dTable.Columns.Add("Personal_Training_Learning_Objective")
                dTable.Columns.Add("Remark1") : dTable.Columns.Add("Remark2")

                dsList.Tables.Add(dTable.Copy)
                'S.SANDEEP [12-Jan-2018] -- START
                'ISSUE/ENHANCEMENT : REF-ID # 0001843
                'IExcel.Export(strFilePath, dsList)
                OpenXML_Export(strFilePath, dsList)
                'S.SANDEEP [12-Jan-2018] -- END
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 7, "Template Exported Successfully."), enMsgBoxStyle.Information)
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "mnuGetFileFormat_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub mnuImportAssessment_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuImportAssessment.Click
        Dim frm As New frmImportReviewerAssessment
        Try
            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If
            frm.ShowDialog()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "mnuImportAssessment_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub
    'S.SANDEEP [ 14 AUG 2013 ] -- END

    'S.SANDEEP [ 13 NOV 2013 ] -- START
    Private Sub cboAssessor_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboReviewer.SelectedIndexChanged
        Try
            Dim objAssessor As New clsassess_analysis_master
            Dim dsList As DataSet = objAssessor.getEmployeeBasedAssessor(CInt(cboReviewer.SelectedValue), "List", True)
            cboEmployee.DisplayMember = "NAME"
            cboEmployee.ValueMember = "Id"
            cboEmployee.DataSource = dsList.Tables(0)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboAssessor_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub
    'S.SANDEEP [ 13 NOV 2013 ] -- END

#End Region

	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Private Sub OtherSettings()
		Try
			Me.SuspendLayout()
			
			Call SetLanguage()
			
			Me.gbEmployeeInfo.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbEmployeeInfo.ForeColor = GUI._eZeeContainerHeaderForeColor 

			Me.eZeeHeader.GradientColor1 = GUI._HeaderBackColor1 
			Me.eZeeHeader.GradientColor2 = GUI._HeaderBackColor2 
			Me.eZeeHeader.BorderColor = GUI._HeaderBorderColor 
			Me.eZeeHeader.HeaderTextForeColor = GUI._HeaderTitleFontColor 
			Me.eZeeHeader.DescriptionForeColor = GUI._HeaderMessageFontColor 


			Me.btnNew.GradientBackColor = GUI._ButttonBackColor 
			Me.btnNew.GradientForeColor = GUI._ButttonFontColor

			Me.btnDelete.GradientBackColor = GUI._ButttonBackColor 
			Me.btnDelete.GradientForeColor = GUI._ButttonFontColor

			Me.btnEdit.GradientBackColor = GUI._ButttonBackColor 
			Me.btnEdit.GradientForeColor = GUI._ButttonFontColor

			Me.btnClose.GradientBackColor = GUI._ButttonBackColor 
			Me.btnClose.GradientForeColor = GUI._ButttonFontColor

			Me.btnUnlockCommit.GradientBackColor = GUI._ButttonBackColor 
			Me.btnUnlockCommit.GradientForeColor = GUI._ButttonFontColor

			Me.btnOperation.GradientBackColor = GUI._ButttonBackColor 
			Me.btnOperation.GradientForeColor = GUI._ButttonFontColor


			Me.ResumeLayout()
		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "OtherSettings", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetLanguage()
		Try
			Me.Text = Language._Object.getCaption(Me.Name, Me.Text)
			
			Me.eZeeHeader.Title = Language._Object.getCaption(Me.eZeeHeader.Name & "_Title" , Me.eZeeHeader.Title)
			Me.eZeeHeader.Message = Language._Object.getCaption(Me.eZeeHeader.Name & "_Message" , Me.eZeeHeader.Message)
			Me.gbEmployeeInfo.Text = Language._Object.getCaption(Me.gbEmployeeInfo.Name, Me.gbEmployeeInfo.Text)
			Me.chkShowUncommited.Text = Language._Object.getCaption(Me.chkShowUncommited.Name, Me.chkShowUncommited.Text)
			Me.chkShowCommited.Text = Language._Object.getCaption(Me.chkShowCommited.Name, Me.chkShowCommited.Text)
			Me.lblAssessmentdate.Text = Language._Object.getCaption(Me.lblAssessmentdate.Name, Me.lblAssessmentdate.Text)
			Me.lblEmployee.Text = Language._Object.getCaption(Me.lblEmployee.Name, Me.lblEmployee.Text)
			Me.lblYears.Text = Language._Object.getCaption(Me.lblYears.Name, Me.lblYears.Text)
			Me.lblAssessmentPeriods.Text = Language._Object.getCaption(Me.lblAssessmentPeriods.Name, Me.lblAssessmentPeriods.Text)
			Me.colhEmployee.Text = Language._Object.getCaption(CStr(Me.colhEmployee.Tag), Me.colhEmployee.Text)
			Me.colhDate.Text = Language._Object.getCaption(CStr(Me.colhDate.Tag), Me.colhDate.Text)
			Me.colhAssessmentPeriod.Text = Language._Object.getCaption(CStr(Me.colhAssessmentPeriod.Tag), Me.colhAssessmentPeriod.Text)
			Me.colhAssessmentYear.Text = Language._Object.getCaption(CStr(Me.colhAssessmentYear.Tag), Me.colhAssessmentYear.Text)
			Me.colhAssessor.Text = Language._Object.getCaption(CStr(Me.colhAssessor.Tag), Me.colhAssessor.Text)
			Me.btnNew.Text = Language._Object.getCaption(Me.btnNew.Name, Me.btnNew.Text)
			Me.btnDelete.Text = Language._Object.getCaption(Me.btnDelete.Name, Me.btnDelete.Text)
			Me.btnEdit.Text = Language._Object.getCaption(Me.btnEdit.Name, Me.btnEdit.Text)
			Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
			Me.colhPercent.Text = Language._Object.getCaption(CStr(Me.colhPercent.Tag), Me.colhPercent.Text)
			Me.btnUnlockCommit.Text = Language._Object.getCaption(Me.btnUnlockCommit.Name, Me.btnUnlockCommit.Text)
			Me.lnkAllocation.Text = Language._Object.getCaption(Me.lnkAllocation.Name, Me.lnkAllocation.Text)
			Me.colhAssessGroup.Text = Language._Object.getCaption(CStr(Me.colhAssessGroup.Tag), Me.colhAssessGroup.Text)
			Me.lblGroupCode.Text = Language._Object.getCaption(Me.lblGroupCode.Name, Me.lblGroupCode.Text)
			Me.btnOperation.Text = Language._Object.getCaption(Me.btnOperation.Name, Me.btnOperation.Text)
			Me.mnuUnlockCommitted.Text = Language._Object.getCaption(Me.mnuUnlockCommitted.Name, Me.mnuUnlockCommitted.Text)
			Me.mnuGetFileFormat.Text = Language._Object.getCaption(Me.mnuGetFileFormat.Name, Me.mnuGetFileFormat.Text)
			Me.mnuImportAssessment.Text = Language._Object.getCaption(Me.mnuImportAssessment.Name, Me.mnuImportAssessment.Text)

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetLanguage", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "Please select atleast one Assessment to perform Operation on it.")
			Language.setMessage(mstrModuleName, 2, "Are you sure you want to delete this Assessment?")
			Language.setMessage(mstrModuleName, 3, "Sorry, you cannot delete this information. Reason : Period is closed.")
			Language.setMessage(mstrModuleName, 4, "Sorry, you cannot unlock this commited information. Reason : Its already linked with Appraisal.")
			Language.setMessage(mstrModuleName, 5, "Sorry, you cannot unlock this commited information. Reason : Period is already Closed.")
			Language.setMessage(mstrModuleName, 6, "Sorry, you cannot edit this information. Reason : Period is closed.")
			Language.setMessage(mstrModuleName, 7, "Template Exported Successfully.")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class