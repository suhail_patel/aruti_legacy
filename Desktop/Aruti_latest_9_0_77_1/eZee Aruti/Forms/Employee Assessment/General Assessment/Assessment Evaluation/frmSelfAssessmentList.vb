﻿'ENHANCEMENT : ANY CHANGES IN DESKTOP SHOULD ALSO BE IMPLEMENTED IN WEB WITHOUT FAIL.
Option Strict On

Imports eZeeCommonLib
Imports Aruti.Data

'Last Message Index = 5

Public Class frmSelfAssessmentList

#Region " Private Variables "
    Private ReadOnly mstrModuleName As String = "frmSelfAssessmentList"
    Private objSelfAssessment As clsassess_analysis_master
    Friend mblnLogin As Boolean
    'S.SANDEEP [ 05 MARCH 2012 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Private mstrAdvanceFilter As String = String.Empty
    'S.SANDEEP [ 05 MARCH 2012 ] -- END

#End Region

#Region " Private Methods "

    Private Sub FillCombo()
        Dim dsFill As DataSet = Nothing
        Try
            'FOR YEAR 
            dsFill = Nothing
            Dim objYear As New clsMasterData

            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'dsFill = objYear.getComboListPAYYEAR("Year", True, , , , True)
            dsFill = objYear.getComboListPAYYEAR(FinancialYear._Object._YearUnkid, FinancialYear._Object._FinancialYear_Name, Company._Object._Companyunkid, "Year", True, True)
            'S.SANDEEP [04 JUN 2015] -- END

            cboYear.ValueMember = "Id"
            cboYear.DisplayMember = "name"
            cboYear.DataSource = dsFill.Tables("Year")

            'FOR Period
            dsFill = Nothing
            Dim objPeriod As New clscommom_period_Tran
            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'dsFill = objPeriod.getListForCombo(enModuleReference.Assessment, CInt(cboYear.SelectedValue), "Period", True, 0)
            dsFill = objPeriod.getListForCombo(enModuleReference.Assessment, CInt(cboYear.SelectedValue), FinancialYear._Object._DatabaseName, FinancialYear._Object._Database_Start_Date, "Period", True, 0)
            'Sohail (21 Aug 2015) -- End
            cboPeriod.ValueMember = "periodunkid"
            cboPeriod.DisplayMember = "name"
            cboPeriod.DataSource = dsFill.Tables("Period")


            'FOR EMPLOYEE
            dsFill = Nothing
            Dim objEmployee As New clsEmployee_Master

            'S.SANDEEP [ 29 JUNE 2011 ] -- START
            'ISSUE : INACTIVE EMPLOYEE(S) COMING ON LIST
            'dsFill = objEmployee.GetEmployeeList("List", True, True)
            'Sohail (06 Jan 2012) -- Start
            'TRA - ENHANCEMENT
            'dsFill = objEmployee.GetEmployeeList("List", True, Not ConfigParameter._Object._IsIncludeInactiveEmp)

            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS

            'If ConfigParameter._Object._IsIncludeInactiveEmp = False Then
            '    dsFill = objEmployee.GetEmployeeList("List", True, , , , , , , , , , , , , eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate))
            'Else
            '    dsFill = objEmployee.GetEmployeeList("List", True, )
            'End If
            ''Sohail (06 Jan 2012) -- End
            ''S.SANDEEP [ 29 JUNE 2011 ] -- END 

            'cboEmployee.ValueMember = "employeeunkid"
            'cboEmployee.DisplayMember = "employeename"
            'cboEmployee.DataSource = dsFill.Tables("List")

            'S.SANDEEP [04 JUN 2015] -- END

            'S.SANDEEP [ 14 AUG 2013 ] -- START
            'ENHANCEMENT : TRA CHANGES
            Dim objAssessGroup As New clsassess_group_master
            dsFill = objAssessGroup.getListForCombo("Group", True)
            cboGroup.ValueMember = "assessgroupunkid"
            cboGroup.DisplayMember = "name"
            cboGroup.DataSource = dsFill.Tables(0)
            'S.SANDEEP [ 14 AUG 2013 ] -- END


        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        End Try
    End Sub

    Private Sub FillList()
        Dim lvItem As ListViewItem
        Dim dsList As New DataSet
        Dim dtTable As DataTable
        Dim StrSearching As String = String.Empty
        Try

            'S.SANDEEP [28 MAY 2015] -- START
            'ENHANCEMENT : NEW PRIVILEGE (Performance Assessment)
            'If User._Object.Privilege._AllowToViewSelfAssessmentList = True Then   'Pinkal (02-Jul-2012) -- Start
            'S.SANDEEP [28 MAY 2015] -- END



            dsList = objSelfAssessment.GetList("List", enAssessmentMode.SELF_ASSESSMENT)

            'S.SANDEEP [ 04 FEB 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            Dim dsPercent As New DataSet
            Dim blnFlag As Boolean = False

                'S.SANDEEP [ 22 OCT 2013 ] -- START
                'ENHANCEMENT : ENHANCEMENT
                'dsPercent = objSelfAssessment.Get_Percentage(enAssessmentMode.SELF_ASSESSMENT)
                dsPercent = objSelfAssessment.Get_Percentage(enAssessmentMode.SELF_ASSESSMENT, ConfigParameter._Object._ConsiderItemWeightAsNumber)
                'S.SANDEEP [ 22 OCT 2013 ] -- END

            If dsPercent.Tables(0).Rows.Count > 0 Then
                blnFlag = True
            End If
            'S.SANDEEP [ 04 FEB 2012 ] -- END

            If CInt(cboEmployee.SelectedValue) > 0 Then
                StrSearching &= "AND selfemployeeunkid = " & CInt(cboEmployee.SelectedValue)
            End If

            If CInt(cboYear.SelectedValue) > 0 Then
                StrSearching &= "AND yearunkid = " & CInt(cboYear.SelectedValue)
            End If

            If CInt(cboPeriod.SelectedValue) > 0 Then
                StrSearching &= "AND periodunkid = " & CInt(cboPeriod.SelectedValue)
            End If

            If dtpAssessmentdate.Checked = True Then
                StrSearching &= "AND assessmentdate = '" & eZeeDate.convertDate(dtpAssessmentdate.Value) & "'"
            End If

            'S.SANDEEP [ 22 JULY 2011 ] -- START
            'ENHANCEMENT : WEB & DESKTOP COMMIT OPTION TO BE INCLUDED
            If chkShowCommited.CheckState = CheckState.Checked And chkShowUncommited.CheckState = CheckState.Unchecked Then
                    StrSearching &= "AND iscommitted = " & True & " "
            ElseIf chkShowCommited.CheckState = CheckState.Unchecked And chkShowUncommited.CheckState = CheckState.Checked Then
                    StrSearching &= "AND iscommitted = " & False & " "
            End If
            'S.SANDEEP [ 22 JULY 2011 ] -- END 

                'S.SANDEEP [ 14 AUG 2013 ] -- START
                'ENHANCEMENT : TRA CHANGES
                If CInt(cboGroup.SelectedValue) > 0 Then
                    StrSearching &= "AND assessgroupunkid = '" & CInt(cboGroup.SelectedValue) & "' "
                End If
                'S.SANDEEP [ 14 AUG 2013 ] -- END

            'S.SANDEEP [ 05 MARCH 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            If mstrAdvanceFilter.Length > 0 Then
                StrSearching &= "AND " & mstrAdvanceFilter
            End If
            'S.SANDEEP [ 05 MARCH 2012 ] -- END

            If StrSearching.Length > 0 Then
                StrSearching = StrSearching.Substring(3)
                dtTable = New DataView(dsList.Tables(0), StrSearching, "EmpName", DataViewRowState.CurrentRows).ToTable
            Else
                dtTable = New DataView(dsList.Tables(0), "", "EmpName", DataViewRowState.CurrentRows).ToTable
            End If

            lvAssesmentList.Items.Clear()

            For Each dtRow As DataRow In dtTable.Rows
                lvItem = New ListViewItem

                lvItem.Text = dtRow.Item("YearName").ToString
                lvItem.SubItems.Add(dtRow.Item("PName").ToString)
                'S.SANDEEP [ 22 JULY 2011 ] -- START
                'ENHANCEMENT : WEB & DESKTOP COMMIT OPTION TO BE INCLUDED
                lvItem.SubItems(colhPeriod.Index).Tag = dtRow.Item("Sid")
                'S.SANDEEP [ 22 JULY 2011 ] -- END 
                lvItem.SubItems.Add(eZeeDate.convertDate(dtRow.Item("assessmentdate").ToString).ToShortDateString)
                lvItem.SubItems.Add(dtRow.Item("EmpName").ToString)
                lvItem.Tag = dtRow.Item("analysisunkid")

                'S.SANDEEP [ 22 JULY 2011 ] -- START
                'ENHANCEMENT : WEB & DESKTOP COMMIT OPTION TO BE INCLUDED
                If CBool(dtRow.Item("iscommitted")) = True Then
                    lvItem.ForeColor = Color.Blue
                End If
                'S.SANDEEP [ 22 JULY 2011 ] -- END 

                'S.SANDEEP [ 04 FEB 2012 ] -- START
                'ENHANCEMENT : TRA CHANGES
                If blnFlag Then
                        'S.SANDEEP [ 14 AUG 2013 ] -- START
                        'ENHANCEMENT : TRA CHANGES
                        'Dim dTemp() As DataRow = dsPercent.Tables(0).Select("EmpId = '" & CInt(dtRow.Item("selfemployeeunkid")) & "' AND Pid = '" & CInt(dtRow.Item("periodunkid")) & "'")
                        Dim dTemp() As DataRow = dsPercent.Tables(0).Select("EmpId = '" & CInt(dtRow.Item("selfemployeeunkid")) & "' AND Pid = '" & CInt(dtRow.Item("periodunkid")) & "' AND analysisunkid = '" & CInt(dtRow.Item("analysisunkid")) & "'")
                        'S.SANDEEP [ 14 AUG 2013 ] -- END
                        If dTemp.Length > 0 Then
                            lvItem.SubItems.Add(dTemp(0)("TotalPercent").ToString)
                            'S.SANDEEP [ 28 JAN 2014 ] -- START
                        Else
                            lvItem.SubItems.Add("")
                            'S.SANDEEP [ 28 JAN 2014 ] -- END
                        End If
                    End If
                    'S.SANDEEP [ 04 FEB 2012 ] -- END

                'S.SANDEEP [ 05 MARCH 2012 ] -- START
                'ENHANCEMENT : TRA CHANGES
                lvItem.SubItems.Add(dtRow.Item("periodunkid").ToString)
                'S.SANDEEP [ 05 MARCH 2012 ] -- END


                    'S.SANDEEP [ 14 AUG 2013 ] -- START
                    'ENHANCEMENT : TRA CHANGES
                    lvItem.SubItems.Add(dtRow.Item("Assessment_Group").ToString)
                    lvItem.SubItems.Add(dtRow.Item("yearunkid").ToString)
                    lvItem.SubItems.Add(dtRow.Item("selfemployeeunkid").ToString)
                    lvItem.SubItems.Add(dtRow.Item("assessgroupunkid").ToString)
                    'S.SANDEEP [ 14 AUG 2013 ] -- END

                lvAssesmentList.Items.Add(lvItem)
            Next



            'S.SANDEEP [ 04 FEB 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            ''S.SANDEEP [ 30 May 2011 ] -- START
            'lvAssesmentList.GroupingColumn = colhEmployee
            'lvAssesmentList.DisplayGroups(True)
            ''S.SANDEEP [ 22 JULY 2011 ] -- START
            ''ENHANCEMENT : WEB & DESKTOP COMMIT OPTION TO BE INCLUDED
            ''colhPeriod.Width += colhEmployee.Width - 20
            'colhEmployee.Width = CInt(IIf(colhEmployee.Width > 0, colhEmployee.Width - 20, 0))
            'colhPeriod.Width += colhEmployee.Width
            ''S.SANDEEP [ 22 JULY 2011 ] -- END 
            'colhEmployee.Width = 0
            ''S.SANDEEP [ 30 May 2011 ] -- END 
            lvAssesmentList.GroupingColumn = objcolhEmployee
            lvAssesmentList.DisplayGroups(True)
            'S.SANDEEP [ 04 FEB 2012 ] -- ENDk


            'End If



            lvAssesmentList.GridLines = False

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillList", mstrModuleName)
        End Try
    End Sub

    Private Sub SetVisibility()

        Try
            'S.SANDEEP [28 MAY 2015] -- START
            'ENHANCEMENT : NEW PRIVILEGE (Performance Assessment)

            'btnNew.Enabled = User._Object.Privilege._AddEmployeeAssessment
            'btnEdit.Enabled = User._Object.Privilege._EditEmployeeAssessment
            'btnDelete.Enabled = User._Object.Privilege._DeleteEmployeeAssessment

            ''S.SANDEEP [ 22 JULY 2011 ] -- START
            ''ENHANCEMENT : WEB & DESKTOP COMMIT OPTION TO BE INCLUDED
            'chkShowCommited.CheckState = CheckState.Checked
            ''S.SANDEEP [ 22 JULY 2011 ] -- END 

            ''S.SANDEEP [ 05 MARCH 2012 ] -- START
            ''ENHANCEMENT : TRA CHANGES
            'btnUnlockCommit.Enabled = User._Object.Privilege._Allow_UnlockCommittedGeneralAssessment
            ''S.SANDEEP [ 05 MARCH 2012 ] -- END

            'S.SANDEEP [28 MAY 2015] -- END

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetVisibility", mstrModuleName)
        End Try

    End Sub


#End Region

#Region " Form's Events "

    Private Sub frmSelfAssessmentList_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        objSelfAssessment = New clsassess_analysis_master
        Try
            Call Set_Logo(Me, gApplicationType)
            Language.setLanguage(Me.Name)
            Call SetVisibility()
            FillCombo()
            'Call FillList()
            If lvAssesmentList.Items.Count > 0 Then lvAssesmentList.Items(0).Selected = True
            lvAssesmentList.Select()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmSelfAssessmentList_Load", mstrModuleName)
        End Try
    End Sub

    Private Sub frmSelfAssessmentList_KeyPress(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles MyBase.KeyPress
        Try
            If Asc(e.KeyChar) = 13 Then
                SendKeys.Send("{TAB}")
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmSelfAssessmentList_KeyPress", mstrModuleName)
        End Try
    End Sub

    Private Sub frmSelfAssessmentList_FormClosed(ByVal sender As System.Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles MyBase.FormClosed
        objSelfAssessment = Nothing
    End Sub

#End Region

#Region " Button's Events "

    Private Sub btnNew_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNew.Click
        'S.SANDEEP [ 29 DEC 2011 ] -- START
        'ENHANCEMENT : TRA CHANGES 
        'TYPE : EMPLOYEMENT CONTRACT PROCESS
        'Dim objfrm As New frmAssessmentAnalysis
        'Try
        '    'objfrm.Text = Language.getMessage(mstrModuleName, 1, "Add/Edit Self Assessment")
        '    'objfrm.gbAssesorsInfo.Text = Language.getMessage(mstrModuleName, 2, "Self Assessment information")

        '    objfrm.Text = "Add/Edit Self Assessment"
        '    objfrm.gbAssesorsInfo.Text = "Self Assessment information"

        '    If objfrm.displayDialog(-1, enAction.ADD_CONTINUE, True) Then
        '        Call FillList()
        '    End If
        'Catch ex As Exception
        '    DisplayError.Show("-1", ex.Message, "btnNew_Click", mstrModuleName)
        'Finally
        '    If objfrm IsNot Nothing Then objfrm.Dispose()
        'End Try

        'S.SANDEEP [ 24 APR 2014 ] -- START
        'Dim frm As New frmEvaluation
        Dim frm As New frmTabular_Evaluation
        'S.SANDEEP [ 24 APR 2014 ] -- END
        Try

            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If

            If frm.displayDialog(-1, enAction.ADD_CONTINUE, enAssessmentMode.SELF_ASSESSMENT) Then
                Call FillList()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnNew_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
        'S.SANDEEP [ 29 DEC 2011 ] -- END
    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Me.Close()
    End Sub

    Private Sub btnDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDelete.Click
        If lvAssesmentList.SelectedItems.Count <= 0 Then
            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please select at least one Assessment to perform operation on it."), enMsgBoxStyle.Information)
            Exit Sub
        End If
        Try
            'S.SANDEEP [ 22 JULY 2011 ] -- START
            'ENHANCEMENT : WEB & DESKTOP COMMIT OPTION TO BE INCLUDED
            If CInt(lvAssesmentList.SelectedItems(0).SubItems(colhPeriod.Index).Tag) = enStatusType.Close Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Sorry, you cannot delete this information. Reason : Period is closed."), enMsgBoxStyle.Information)
                Exit Sub
            End If
            'S.SANDEEP [ 22 JULY 2011 ] -- END 

            If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Are you sure you want to delete this Assessment?"), CType(enMsgBoxStyle.YesNo + enMsgBoxStyle.Information, enMsgBoxStyle)) = Windows.Forms.DialogResult.Yes Then
                Dim frm As New frmReasonSelection
                Dim mstrVoidReason As String = String.Empty
                frm.displayDialog(enVoidCategoryType.ASSESSMENT, mstrVoidReason)
                If mstrVoidReason.Length <= 0 Then
                    Exit Sub
                Else
                    objSelfAssessment._Voidreason = mstrVoidReason
                End If
                frm = Nothing
                objSelfAssessment._Isvoid = True
                objSelfAssessment._Voiddatetime = ConfigParameter._Object._CurrentDateAndTime
                objSelfAssessment._Voiduserunkid = User._Object._Userunkid

                'S.SANDEEP [ 01 JUL 2014 ] -- START
                'objSelfAssessment.Delete(CInt(lvAssesmentList.SelectedItems(0).Tag))
                objSelfAssessment.Delete(CInt(lvAssesmentList.SelectedItems(0).Tag), CInt(lvAssesmentList.SelectedItems(0).SubItems(objcolhPeriodId.Index).Text))
                'S.SANDEEP [ 01 JUL 2014 ] -- END

                If objSelfAssessment._Message <> "" Then
                    eZeeMsgBox.Show(objSelfAssessment._Message, enMsgBoxStyle.Information)
                Else
                    lvAssesmentList.SelectedItems(0).Remove()
                End If

            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnDelete_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnEdit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEdit.Click
        'S.SANDEEP [ 29 DEC 2011 ] -- START
        'ENHANCEMENT : TRA CHANGES 
        'TYPE : EMPLOYEMENT CONTRACT PROCESS
        'Dim objfrm As New frmAssessmentAnalysis
        'Try
        '    If lvAssesmentList.SelectedItems.Count <= 0 Then
        '        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 8, "Please select at least one Assessment to perform operation on it."), enMsgBoxStyle.Information)
        '        Exit Sub
        '    End If



        '    'S.SANDEEP [ 22 JULY 2011 ] -- START
        '    'ENHANCEMENT : WEB & DESKTOP COMMIT OPTION TO BE INCLUDED
        '    If CInt(lvAssesmentList.SelectedItems(0).SubItems(colhPeriod.Index).Tag) = enStatusType.Close Then
        '        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "Sorry, you cannot edit this information. Reason : Period is closed."), enMsgBoxStyle.Information)
        '        Exit Sub
        '    End If
        '    'S.SANDEEP [ 22 JULY 2011 ] -- END 

        '    objfrm.Text = Language.getMessage(mstrModuleName, 1, "Add/Edit Self Assessment")
        '    objfrm.gbAssesorsInfo.Text = Language.getMessage(mstrModuleName, 2, "Self Assessment information")
        '    If objfrm.displayDialog(CInt(lvAssesmentList.SelectedItems(0).Tag), enAction.EDIT_ONE, True) Then
        '        Call FillList()
        '    End If
        'Catch ex As Exception
        '    DisplayError.Show("-1", ex.Message, "btnEdit_Click", mstrModuleName)
        'Finally
        '    If objfrm IsNot Nothing Then objfrm.Dispose()
        'End Try

            If lvAssesmentList.SelectedItems.Count <= 0 Then
            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "Please select at least one Assessment to perform operation on it."), enMsgBoxStyle.Information)
                Exit Sub
            End If

            If CInt(lvAssesmentList.SelectedItems(0).SubItems(colhPeriod.Index).Tag) = enStatusType.Close Then
            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Sorry, you cannot edit this information. Reason : Period is closed."), enMsgBoxStyle.Information)
                Exit Sub
            End If

        'S.SANDEEP [ 24 APR 2014 ] -- START
        'Dim frm As New frmEvaluation
        Dim frm As New frmTabular_Evaluation
        'S.SANDEEP [ 24 APR 2014 ] -- END
        Try

            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If

            If frm.displayDialog(CInt(lvAssesmentList.SelectedItems(0).Tag), enAction.EDIT_ONE, enAssessmentMode.SELF_ASSESSMENT) Then
                Call FillList()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
        'S.SANDEEP [ 29 DEC 2011 ] -- END
    End Sub

    Private Sub objbtnReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnReset.Click
        Try

            cboEmployee.SelectedValue = 0
            cboPeriod.SelectedValue = 0
            cboYear.SelectedValue = 0
            dtpAssessmentdate.Checked = False
            'S.SANDEEP [ 22 JULY 2011 ] -- START
            'ENHANCEMENT : WEB & DESKTOP COMMIT OPTION TO BE INCLUDED
            chkShowCommited.CheckState = CheckState.Checked
            'S.SANDEEP [ 22 JULY 2011 ] -- END 

            'S.SANDEEP [ 05 MARCH 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            mstrAdvanceFilter = String.Empty
            'S.SANDEEP [ 05 MARCH 2012 ] -- END

            Call FillList()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnReset_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnSearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearch.Click
        Try
            Call FillList()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearch_Click", mstrModuleName)
        End Try
    End Sub

    'S.SANDEEP [ 22 JULY 2011 ] -- START
    'ENHANCEMENT : WEB & DESKTOP COMMIT OPTION TO BE INCLUDED

    'S.SANDEEP [ 05 MARCH 2012 ] -- START
    'ENHANCEMENT : TRA CHANGES
    'Private Sub btnMakeCommit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnMakeCommit.Click
    '    Try
    '        If lvAssesmentList.CheckedItems.Count <= 0 Then
    '            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 7, "Please check at least one information to set as commit."), enMsgBoxStyle.Information)
    '            Exit Sub
    '        End If

    '        Dim objAnalysis As New clsassess_analysis_master
    '        For Each LVI As ListViewItem In lvAssesmentList.CheckedItems
    '            objAnalysis._Analysisunkid = CInt(LVI.Tag)
    '            objAnalysis._Iscommitted = True
    '            objAnalysis.Update()
    '        Next
    '        objAnalysis = Nothing
    '        Call FillList()
    '    Catch ex As Exception
    '        DisplayError.Show("-1", ex.Message, "btnMakeCommit_Click", mstrModuleName)
    '    Finally
    '    End Try
    'End Sub
    'S.SANDEEP [ 05 MARCH 2012 ] -- END
    'Private Sub btnUnlockCommit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnUnlockCommit.Click
    '    Try
    '        If lvAssesmentList.SelectedItems.Count > 0 Then

    '            'S.SANDEEP [ 14 AUG 2013 ] -- START
    '            'ENHANCEMENT : TRA CHANGES
    '            If objSelfAssessment.isExist(enAssessmentMode.APPRAISER_ASSESSMENT, _
    '                                         CInt(lvAssesmentList.SelectedItems(0).SubItems(objcolhEmpId.Index).Text), _
    '                                         CInt(lvAssesmentList.SelectedItems(0).SubItems(objcolhYearId.Index).Text), _
    '                                         CInt(lvAssesmentList.SelectedItems(0).SubItems(objcolhPeriodId.Index).Text), _
    '                                         CInt(lvAssesmentList.SelectedItems(0).SubItems(objcolhGrpId.Index).Text)) = True Then
    '                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 11, "Sorry, you cannot unlock General Assessment for the selected period. Reason : Assessment is already done for the selected period by Assessor(s)."), enMsgBoxStyle.Information)
    '                Exit Sub
    '            End If

    '            If objSelfAssessment.isExist(enAssessmentMode.REVIEWER_ASSESSMENT, _
    '                                         CInt(lvAssesmentList.SelectedItems(0).SubItems(objcolhEmpId.Index).Text), _
    '                                         CInt(lvAssesmentList.SelectedItems(0).SubItems(objcolhYearId.Index).Text), _
    '                                         CInt(lvAssesmentList.SelectedItems(0).SubItems(objcolhPeriodId.Index).Text), _
    '                                         CInt(lvAssesmentList.SelectedItems(0).SubItems(objcolhGrpId.Index).Text)) = True Then
    '                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 12, "Sorry, you cannot unlock General Assessment for the selected period. Reason : Assessment is already done for the selected period by Reviewer."), enMsgBoxStyle.Information)
    '                Exit Sub
    '            End If
    '            'S.SANDEEP [ 14 AUG 2013 ] -- END

    '            If CInt(lvAssesmentList.SelectedItems(0).SubItems(colhPeriod.Index).Tag) = enStatusType.Open Then
    '                If objSelfAssessment.Unlock_Commit(CInt(lvAssesmentList.SelectedItems(0).SubItems(objcolhPeriodId.Index).Text)) = True Then
    '                    objSelfAssessment._Analysisunkid = CInt(lvAssesmentList.SelectedItems(0).Tag)
    '                    objSelfAssessment._Iscommitted = False
    '                    'S.SANDEEP [ 14 JUNE 2012 ] -- START
    '                    'ENHANCEMENT : TRA CHANGES
    '                    objSelfAssessment._Committeddatetime = Nothing
    '                    'S.SANDEEP [ 14 JUNE 2012 ] -- END
    '                    objSelfAssessment.Update()
    '                    If objSelfAssessment._Message <> "" Then
    '                        eZeeMsgBox.Show(objSelfAssessment._Message)
    '                    End If
    '                    Call FillList()
    '                Else
    '                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 9, "Sorry, you cannot unlock this commited information. Reason : Its already linked with Appraisal."), enMsgBoxStyle.Information)
    '                    Exit Sub
    '                End If
    '            Else
    '                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 10, "Sorry, you cannot unlock this commited information. Reason : Period is already Closed."), enMsgBoxStyle.Information)
    '                Exit Sub
    '            End If
    '        End If
    '    Catch ex As Exception
    '        DisplayError.Show("-1", ex.Message, "btnUnlockCommit_Click", mstrModuleName)
    '    Finally
    '    End Try
    'End Sub

    Private Sub objbtnSearchEmployee_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchEmployee.Click
        Dim frm As New frmCommonSearch
        Try
            'Anjan (02 Sep 2011)-Start
            'Issue : Including Language Settings.
            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If
            'Anjan (02 Sep 2011)-End 


            frm.ValueMember = cboEmployee.ValueMember
            frm.DisplayMember = cboEmployee.DisplayMember
            frm.CodeMember = "employeecode"
            frm.DataSource = CType(cboEmployee.DataSource, DataTable)
            If frm.DisplayDialog Then
                cboEmployee.SelectedValue = frm.SelectedValue
                cboEmployee.Focus()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchEmployee_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub
    'S.SANDEEP [ 22 JULY 2011 ] -- END 

    'S.SANDEEP [ 14 AUG 2013 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Private Sub objbtnSearchGroup_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchGroup.Click
        Dim frm As New frmCommonSearch
        Try
            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If

            frm.ValueMember = cboGroup.ValueMember
            frm.DisplayMember = cboGroup.DisplayMember
            frm.CodeMember = ""
            frm.DataSource = CType(cboGroup.DataSource, DataTable)
            If frm.DisplayDialog Then
                cboGroup.SelectedValue = frm.SelectedValue
                cboGroup.Focus()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchGroup_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub
    'S.SANDEEP [ 14 AUG 2013 ] -- END

#End Region

    'S.SANDEEP [ 22 JULY 2011 ] -- START
    'ENHANCEMENT : WEB & DESKTOP COMMIT OPTION TO BE INCLUDED
#Region " Controls "

    Private Sub lvAssesmentList_ItemChecked(ByVal sender As Object, ByVal e As System.Windows.Forms.ItemCheckedEventArgs) Handles lvAssesmentList.ItemChecked
        Try
            If lvAssesmentList.CheckedItems.Count <= 0 Then Exit Sub
            If e.Item.ForeColor = Color.Blue Then e.Item.Checked = False
            If CInt(e.Item.SubItems(colhPeriod.Index).Tag) = enStatusType.Close Then e.Item.Checked = False
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lvAssesmentList_ItemChecked", mstrModuleName)
        End Try
    End Sub

    Private Sub lvAssesmentList_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lvAssesmentList.SelectedIndexChanged
        Try
            If lvAssesmentList.SelectedItems.Count <= 0 Then Exit Sub
            If lvAssesmentList.SelectedItems(0).ForeColor = Color.Blue Then
                btnEdit.Enabled = False : btnDelete.Enabled = False : btnUnlockCommit.Enabled = True 'S.SANDEEP [ 05 MARCH 2012 btnUnlockCommit.Enabled = False ] -- START END
            Else
                btnEdit.Enabled = True : btnDelete.Enabled = True : btnUnlockCommit.Enabled = False 'S.SANDEEP [ 05 MARCH 2012 btnUnlockCommit.Enabled = True ] -- START END
            End If
            'S.SANDEEP [ 05 MARCH 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            'btnUnlockCommit.Enabled = User._Object.Privilege._Allow_UnlockCommittedGeneralAssessment
            'S.SANDEEP [ 05 MARCH 2012 ] -- END
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lvAssessorList_SelectedIndexChanged", mstrModuleName)
        Finally
        End Try
    End Sub

    'S.SANDEEP [ 05 MARCH 2012 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Private Sub lnkAllocation_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lnkAllocation.LinkClicked
        Try
            Dim frm As New frmAdvanceSearch
            frm.ShowDialog()
            mstrAdvanceFilter = frm._GetFilterString
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lnkAllocation_LinkClicked", mstrModuleName)
        End Try
    End Sub
    'S.SANDEEP [ 05 MARCH 2012 ] -- END

    'S.SANDEEP [ 14 AUG 2013 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Private Sub mnuUnlockCommitted_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuUnlockCommitted.Click
        Try
            If lvAssesmentList.SelectedItems.Count > 0 Then

                If objSelfAssessment.isExist(enAssessmentMode.APPRAISER_ASSESSMENT, _
                                             CInt(lvAssesmentList.SelectedItems(0).SubItems(objcolhEmpId.Index).Text), _
                                             CInt(lvAssesmentList.SelectedItems(0).SubItems(objcolhYearId.Index).Text), _
                                             CInt(lvAssesmentList.SelectedItems(0).SubItems(objcolhPeriodId.Index).Text), _
                                             CInt(lvAssesmentList.SelectedItems(0).SubItems(objcolhGrpId.Index).Text)) = True Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 8, "Sorry, you cannot unlock General Assessment for the selected period. Reason : Assessment is already done for the selected period by Assessor(s)."), enMsgBoxStyle.Information)
                    Exit Sub
                End If

                If objSelfAssessment.isExist(enAssessmentMode.REVIEWER_ASSESSMENT, _
                                             CInt(lvAssesmentList.SelectedItems(0).SubItems(objcolhEmpId.Index).Text), _
                                             CInt(lvAssesmentList.SelectedItems(0).SubItems(objcolhYearId.Index).Text), _
                                             CInt(lvAssesmentList.SelectedItems(0).SubItems(objcolhPeriodId.Index).Text), _
                                             CInt(lvAssesmentList.SelectedItems(0).SubItems(objcolhGrpId.Index).Text)) = True Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 9, "Sorry, you cannot unlock General Assessment for the selected period. Reason : Assessment is already done for the selected period by Reviewer."), enMsgBoxStyle.Information)
                    Exit Sub
                End If
                

                If CInt(lvAssesmentList.SelectedItems(0).SubItems(colhPeriod.Index).Tag) = enStatusType.Open Then
                    'S.SANDEEP [ 10 SEPT 2013 ] -- START
                    'ENHANCEMENT : TRA CHANGES
                    'If objSelfAssessment.Unlock_Commit(CInt(lvAssesmentList.SelectedItems(0).SubItems(objcolhPeriodId.Index).Text)) = True Then
                    If objSelfAssessment.Unlock_Commit(CInt(lvAssesmentList.SelectedItems(0).SubItems(objcolhPeriodId.Index).Text), _
                                                       CInt(lvAssesmentList.SelectedItems(0).SubItems(objcolhEmpId.Index).Text)) = True Then
                        'S.SANDEEP [ 10 SEPT 2013 ] -- END
                        objSelfAssessment._Analysisunkid = CInt(lvAssesmentList.SelectedItems(0).Tag)
                        objSelfAssessment._Iscommitted = False
                        objSelfAssessment._Committeddatetime = Nothing
                        objSelfAssessment.Update()
                        If objSelfAssessment._Message <> "" Then
                            eZeeMsgBox.Show(objSelfAssessment._Message)
                        End If
                        Call FillList()
                    Else
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 6, "Sorry, you cannot unlock this commited information. Reason : Its already linked with Appraisal."), enMsgBoxStyle.Information)
                        Exit Sub
                    End If
                Else
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 7, "Sorry, you cannot unlock this commited information. Reason : Period is already Closed."), enMsgBoxStyle.Information)
                    Exit Sub
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "mnuUnlockCommitted_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub mnuGetFileFormat_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuGetFileFormat.Click
        'S.SANDEEP [12-Jan-2018] -- START
        'ISSUE/ENHANCEMENT : REF-ID # 0001843
        'Dim IExcel As New ExcelData
        'S.SANDEEP [12-Jan-2018] -- END
        Dim dsList As New DataSet
        Dim path As String = String.Empty
        Dim strFilePath As String = String.Empty
        Dim dlgSaveFile As New SaveFileDialog
        Dim ObjFile As System.IO.FileInfo
        Try
            dlgSaveFile.Filter = "Execl files(*.xlsx)|*.xlsx"
            If dlgSaveFile.ShowDialog = Windows.Forms.DialogResult.OK Then
                ObjFile = New System.IO.FileInfo(dlgSaveFile.FileName)
                strFilePath = ObjFile.DirectoryName & "\"
                strFilePath &= ObjFile.Name

                Dim dTable As New DataTable

                dTable.Columns.Add("Assess_Period") : dTable.Columns.Add("Employee_Code") : dTable.Columns.Add("Assess_Group_Code")
                dTable.Columns.Add("Assess_Item_Code") : dTable.Columns.Add("Assess_SubItem_Code") : dTable.Columns.Add("Result")
                dTable.Columns.Add("Improvement") : dTable.Columns.Add("Activity") : dTable.Columns.Add("Support_Required")
                dTable.Columns.Add("Other_Training") : dTable.Columns.Add("Time_Frame_Date") : dTable.Columns.Add("Training_Learning_Objective")

                dsList.Tables.Add(dTable.Copy)
                'S.SANDEEP [12-Jan-2018] -- START
                'ISSUE/ENHANCEMENT : REF-ID # 0001843
                'IExcel.Export(strFilePath, dsList)
                OpenXML_Export(strFilePath, dsList)
                'S.SANDEEP [12-Jan-2018] -- END
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 10, "Template Exported Successfully."), enMsgBoxStyle.Information)
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "mnuGetFileFormat_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub mnuImportSelfAssessment_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuImportSelfAssessment.Click
        Dim frm As New frmImportSelfAssessment
        Try
            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If
            frm.ShowDialog()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "mnuImportSelfAssessment_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub
    'S.SANDEEP [ 14 AUG 2013 ] -- END

#End Region
    'S.SANDEEP [ 22 JULY 2011 ] -- END 

    

   
	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Private Sub OtherSettings()
		Try
			Me.SuspendLayout()
			
			Call SetLanguage()
			
			Me.gbEmployeeInfo.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbEmployeeInfo.ForeColor = GUI._eZeeContainerHeaderForeColor 

            Me.eZeeHeader.GradientColor1 = GUI._HeaderBackColor1
			Me.eZeeHeader.GradientColor2 = GUI._HeaderBackColor2 
			Me.eZeeHeader.BorderColor = GUI._HeaderBorderColor 
			Me.eZeeHeader.HeaderTextForeColor = GUI._HeaderTitleFontColor 
			Me.eZeeHeader.DescriptionForeColor = GUI._HeaderMessageFontColor 


			Me.btnDelete.GradientBackColor = GUI._ButttonBackColor 
			Me.btnDelete.GradientForeColor = GUI._ButttonFontColor

			Me.btnEdit.GradientBackColor = GUI._ButttonBackColor 
			Me.btnEdit.GradientForeColor = GUI._ButttonFontColor

			Me.btnClose.GradientBackColor = GUI._ButttonBackColor 
			Me.btnClose.GradientForeColor = GUI._ButttonFontColor

			Me.btnNew.GradientBackColor = GUI._ButttonBackColor 
			Me.btnNew.GradientForeColor = GUI._ButttonFontColor

			Me.btnUnlockCommit.GradientBackColor = GUI._ButttonBackColor 
			Me.btnUnlockCommit.GradientForeColor = GUI._ButttonFontColor

			Me.btnOperation.GradientBackColor = GUI._ButttonBackColor 
			Me.btnOperation.GradientForeColor = GUI._ButttonFontColor


			Me.ResumeLayout()
		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "OtherSettings", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetLanguage()
		Try
			Me.Text = Language._Object.getCaption(Me.Name, Me.Text)
			
			Me.gbEmployeeInfo.Text = Language._Object.getCaption(Me.gbEmployeeInfo.Name, Me.gbEmployeeInfo.Text)
			Me.lblAssessmentdate.Text = Language._Object.getCaption(Me.lblAssessmentdate.Name, Me.lblAssessmentdate.Text)
			Me.lblEmployee.Text = Language._Object.getCaption(Me.lblEmployee.Name, Me.lblEmployee.Text)
			Me.lblYears.Text = Language._Object.getCaption(Me.lblYears.Name, Me.lblYears.Text)
			Me.lblAssessmentPeriods.Text = Language._Object.getCaption(Me.lblAssessmentPeriods.Name, Me.lblAssessmentPeriods.Text)
			Me.colhYear.Text = Language._Object.getCaption(CStr(Me.colhYear.Tag), Me.colhYear.Text)
			Me.colhPeriod.Text = Language._Object.getCaption(CStr(Me.colhPeriod.Tag), Me.colhPeriod.Text)
			Me.eZeeHeader.Title = Language._Object.getCaption(Me.eZeeHeader.Name & "_Title" , Me.eZeeHeader.Title)
			Me.eZeeHeader.Message = Language._Object.getCaption(Me.eZeeHeader.Name & "_Message" , Me.eZeeHeader.Message)
			Me.btnDelete.Text = Language._Object.getCaption(Me.btnDelete.Name, Me.btnDelete.Text)
			Me.btnEdit.Text = Language._Object.getCaption(Me.btnEdit.Name, Me.btnEdit.Text)
			Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
			Me.colhAssessmentdate.Text = Language._Object.getCaption(CStr(Me.colhAssessmentdate.Tag), Me.colhAssessmentdate.Text)
			Me.btnNew.Text = Language._Object.getCaption(Me.btnNew.Name, Me.btnNew.Text)
			Me.btnUnlockCommit.Text = Language._Object.getCaption(Me.btnUnlockCommit.Name, Me.btnUnlockCommit.Text)
			Me.chkShowUncommited.Text = Language._Object.getCaption(Me.chkShowUncommited.Name, Me.chkShowUncommited.Text)
			Me.chkShowCommited.Text = Language._Object.getCaption(Me.chkShowCommited.Name, Me.chkShowCommited.Text)
			Me.colhPercent.Text = Language._Object.getCaption(CStr(Me.colhPercent.Tag), Me.colhPercent.Text)
			Me.lnkAllocation.Text = Language._Object.getCaption(Me.lnkAllocation.Name, Me.lnkAllocation.Text)
			Me.colhAssessGroup.Text = Language._Object.getCaption(CStr(Me.colhAssessGroup.Tag), Me.colhAssessGroup.Text)
			Me.lblGroupCode.Text = Language._Object.getCaption(Me.lblGroupCode.Name, Me.lblGroupCode.Text)
			Me.btnOperation.Text = Language._Object.getCaption(Me.btnOperation.Name, Me.btnOperation.Text)
			Me.mnuUnlockCommitted.Text = Language._Object.getCaption(Me.mnuUnlockCommitted.Name, Me.mnuUnlockCommitted.Text)
			Me.mnuGetFileFormat.Text = Language._Object.getCaption(Me.mnuGetFileFormat.Name, Me.mnuGetFileFormat.Text)
			Me.mnuImportSelfAssessment.Text = Language._Object.getCaption(Me.mnuImportSelfAssessment.Name, Me.mnuImportSelfAssessment.Text)

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetLanguage", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "Please select at least one Assessment to perform operation on it.")
			Language.setMessage(mstrModuleName, 2, "Are you sure you want to delete this Assessment?")
			Language.setMessage(mstrModuleName, 3, "Sorry, you cannot edit this information. Reason : Period is closed.")
			Language.setMessage(mstrModuleName, 4, "Sorry, you cannot delete this information. Reason : Period is closed.")
			Language.setMessage(mstrModuleName, 5, "Please select at least one Assessment to perform operation on it.")
			Language.setMessage(mstrModuleName, 6, "Sorry, you cannot unlock this commited information. Reason : Its already linked with Appraisal.")
			Language.setMessage(mstrModuleName, 7, "Sorry, you cannot unlock this commited information. Reason : Period is already Closed.")
			Language.setMessage(mstrModuleName, 8, "Sorry, you cannot unlock General Assessment for the selected period. Reason : Assessment is already done for the selected period by Assessor(s).")
			Language.setMessage(mstrModuleName, 9, "Sorry, you cannot unlock General Assessment for the selected period. Reason : Assessment is already done for the selected period by Reviewer.")
			Language.setMessage(mstrModuleName, 10, "Template Exported Successfully.")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class

'Option Strict On

'Imports eZeeCommonLib
'Imports Aruti.Data

''Last Message Index = 5

'Public Class frmSelfAssessmentList

'#Region " Private Variables "
'    Private ReadOnly mstrModuleName As String = "frmSelfAssessmentList"
'    Private objSelfAssessment As clsassess_analysis_master
'    Friend mblnLogin As Boolean

'#End Region

'#Region " Private Methods "

'    Private Sub FillList()

'        Dim dsList As New DataSet
'        Dim StrSearching As String = String.Empty
'        Dim dtTable As DataTable
'        Dim lvItem As ListViewItem
'        Try

'            dsList = objSelfAssessment.GetList("List", mblnLogin, True)

'            If CInt(cboEmployee.SelectedValue) > 0 Then
'                StrSearching &= "AND employeeunkid = " & CInt(cboEmployee.SelectedValue) & " "
'            End If

'            If CInt(cboYear.SelectedValue) > 0 Then
'                StrSearching &= "AND yearunkid = " & CInt(cboYear.SelectedValue) & " "
'            End If

'            If CInt(cboPeriod.SelectedValue) > 0 Then
'                StrSearching &= "AND periodunkid = " & CInt(cboPeriod.SelectedValue) & " "
'            End If

'            If dtpAssessmentdate.Checked Then
'                StrSearching &= "AND selfassessmentdate = '" & eZeeDate.convertDate(dtpAssessmentdate.Value) & "' "
'            End If

'            If StrSearching.Length > 0 Then
'                StrSearching = StrSearching.Substring(3)
'                dtTable = New DataView(dsList.Tables("List"), StrSearching, "", DataViewRowState.CurrentRows).ToTable
'            Else
'                dtTable = dsList.Tables("List")
'            End If
'            lvAssesmentList.Items.Clear()
'            For Each dtRow As DataRow In dtTable.Rows
'                lvItem = New ListViewItem
'                lvItem.Tag = CInt(dtRow("analysisunkid"))
'                lvItem.Text = dtRow("employeename").ToString
'                lvItem.SubItems.Add(dtRow("year").ToString)
'                lvItem.SubItems.Add(dtRow("period_name").ToString)
'                lvItem.SubItems.Add(eZeeDate.convertDate(dtRow("selfassessmentdate").ToString).ToShortDateString)
'                lvItem.SubItems.Add(dtRow("yearunkid").ToString)
'                lvItem.SubItems.Add(dtRow("periodunkid").ToString)
'                lvItem.SubItems.Add(dtRow("employeeunkid").ToString)
'                lvAssesmentList.Items.Add(lvItem)
'            Next

'            If lvAssesmentList.Items.Count > 10 Then
'                colhAssessmentdate.Width = 118 - 18
'            Else
'                colhAssessmentdate.Width = 118
'            End If

'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "FillList", mstrModuleName)
'        End Try
'    End Sub

'    Private Sub FillCombo()
'        Dim dsFill As DataSet = Nothing
'        Try
'            'FOR YEAR 
'            dsFill = Nothing
'            Dim objYear As New clsMasterData
'            dsFill = objYear.getComboListPAYYEAR("Year", True)
'            cboYear.ValueMember = "Id"
'            cboYear.DisplayMember = "name"
'            cboYear.DataSource = dsFill.Tables("Year")

'            'FOR Period
'            dsFill = Nothing
'            Dim objPeriod As New clscommom_period_Tran
'            dsFill = objPeriod.getListForCombo(enModuleReference.Assessment, CInt(cboYear.SelectedValue), "Period", True, 0)
'            cboPeriod.ValueMember = "periodunkid"
'            cboPeriod.DisplayMember = "name"
'            cboPeriod.DataSource = dsFill.Tables("Period")


'            'FOR EMPLOYEE
'            dsFill = Nothing
'            Dim objEmployee As New clsEmployee_Master
'            dsFill = objEmployee.GetEmployeeList("List", True, True)
'            cboEmployee.ValueMember = "employeeunkid"
'            cboEmployee.DisplayMember = "employeename"
'            cboEmployee.DataSource = dsFill.Tables("List")

'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
'        End Try
'    End Sub

'#End Region

'#Region " Form's Events "

'    Private Sub frmSelfAssessmentList_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
'        objSelfAssessment = New clsassess_analysis_master
'        Try
'            Language.setLanguage(Me.Name)
'            FillCombo()
'            FillList()
'            If lvAssesmentList.Items.Count > 0 Then lvAssesmentList.Items(0).Selected = True
'            lvAssesmentList.Select()
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "frmSelfAssessmentList_Load", mstrModuleName)
'        End Try
'    End Sub

'    Private Sub frmSelfAssessmentList_KeyPress(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles MyBase.KeyPress
'        Try
'            If Asc(e.KeyChar) = 13 Then
'                SendKeys.Send("{TAB}")
'            End If
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "frmSelfAssessmentList_KeyPress", mstrModuleName)
'        End Try
'    End Sub

'    Private Sub frmSelfAssessmentList_FormClosed(ByVal sender As System.Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles MyBase.FormClosed
'        objSelfAssessment = Nothing
'    End Sub

'#End Region

'#Region " Button's Events "

'    Private Sub btnNew_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNew.Click
'        Try
'            'Dim objFrm As New frmSelfAssessment_AddEdit
'            'If objFrm.displayDialog(-1, False, enAction.ADD_CONTINUE) Then
'            '    FillList()
'            'End If

'            Dim objfrm As New frmAssessmentAnalysis
'            objfrm.mblnIsSelfAssessment = True
'            objfrm.Text = Language.getMessage(mstrModuleName, 1, "Add/Edit Self Assessment")
'            objfrm.gbAssesorsInfo.Text = Language.getMessage(mstrModuleName, 2, "Self Assessment information")
'            If objfrm.displayDialog(-1, enAction.ADD_ONE, -1, -1) Then
'                FillList()
'            End If
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "btnNew_Click", mstrModuleName)
'        End Try
'    End Sub

'    Private Sub btnEdit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEdit.Click
'        If lvAssesmentList.SelectedItems.Count < 1 Then
'            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please select Employee from the list to perform further operation on it."), enMsgBoxStyle.Information) '?1
'            lvAssesmentList.Select()
'            Exit Sub
'        End If

'        Dim objAssessTran As New clsassess_analysis_Tran
'        If objAssessTran.GetAnalysisAssesors(CInt(lvAssesmentList.SelectedItems(0).Tag)) = True Then
'            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "You can't edit this assessment.Reason: This Assessment is already assess by assessor(s)."), enMsgBoxStyle.Information)
'            Exit Sub
'        End If

'        Dim objfrm As New frmAssessmentAnalysis
'        objfrm.mblnIsSelfAssessment = True
'        objfrm.Text = Language.getMessage(mstrModuleName, 1, "Add/Edit Self Assessment")
'        objfrm.gbAssesorsInfo.Text = Language.getMessage(mstrModuleName, 2, "Self Assessment information")
'        Try
'            Dim intSelectedIndex As Integer
'            intSelectedIndex = lvAssesmentList.SelectedItems(0).Index
'            If objfrm.displayDialog(CInt(lvAssesmentList.SelectedItems(0).Tag), enAction.EDIT_ONE, CInt(lvAssesmentList.SelectedItems(0).SubItems(colhEmployeeunkid.Index).Text), -1) Then
'                FillList()
'            End If
'            objfrm = Nothing
'            lvAssesmentList.Items(intSelectedIndex).Selected = True
'            lvAssesmentList.EnsureVisible(intSelectedIndex)
'            lvAssesmentList.Select()
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "btnEdit_Click", mstrModuleName)
'        End Try
'    End Sub

'    Private Sub btnDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDelete.Click
'        If lvAssesmentList.SelectedItems.Count < 1 Then
'            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please select Employee from the list to perform further operation on it."), enMsgBoxStyle.Information) '?1
'            lvAssesmentList.Select()
'            Exit Sub
'        End If
'        Dim objAssessTran As New clsassess_analysis_Tran

'        If objAssessTran.GetAnalysisAssesors(CInt(lvAssesmentList.SelectedItems(0).Tag)) = True Then
'            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "You can't delete this assessment.Reason: This Assessment is already assess by assessor(s)."), enMsgBoxStyle.Information)
'            Exit Sub
'        End If

'        If objSelfAssessment.isUsed(CInt(lvAssesmentList.SelectedItems(0).Tag)) Then
'            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Sorry, You cannot delete this Employee Assessment. Reason: This Employee Assessment is in use."), enMsgBoxStyle.Information) '?2
'            lvAssesmentList.Select()
'            Exit Sub
'        End If
'        Try
'            Dim intSelectedIndex As Integer
'            intSelectedIndex = lvAssesmentList.SelectedItems(0).Index

'            If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Are you sure you want to delete this Employee Assessment?"), CType(enMsgBoxStyle.Question + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.Yes Then

'                'Sandeep [ 16 Oct 2010 ] -- Start
'                'objSelfAssessment._Voidreason = "VOID REASON"
'                'objSelfAssessment._Voiddatetime = Now
'                objSelfAssessment._Isvoid = True
'                objSelfAssessment._Voiddatetime = ConfigParameter._Object._CurrentDateAndTime
'                Dim frm As New frmReasonSelection
'                Dim mstrVoidReason As String = String.Empty
'                frm.displayDialog(enVoidCategoryType.ASSESSMENT, mstrVoidReason)
'                If mstrVoidReason.Length <= 0 Then
'                    Exit Sub
'                Else
'                    objSelfAssessment._Voidreason = mstrVoidReason
'                End If
'                'Sandeep [ 16 Oct 2010 ] -- End

'                objSelfAssessment._Voiduserunkid = User._Object._Userunkid
'                objSelfAssessment.Delete(CInt(lvAssesmentList.SelectedItems(0).Tag))
'                lvAssesmentList.SelectedItems(0).Remove()

'                If lvAssesmentList.Items.Count <= 0 Then
'                    Exit Try
'                End If

'                If lvAssesmentList.Items.Count = intSelectedIndex Then
'                    intSelectedIndex = lvAssesmentList.Items.Count - 1
'                    lvAssesmentList.Items(intSelectedIndex).Selected = True
'                    lvAssesmentList.EnsureVisible(intSelectedIndex)
'                ElseIf lvAssesmentList.Items.Count <> 0 Then
'                    lvAssesmentList.Items(intSelectedIndex).Selected = True
'                    lvAssesmentList.EnsureVisible(intSelectedIndex)
'                End If
'            End If
'            lvAssesmentList.Select()
'        Catch ex As Exception
'            Call DisplayError.Show("-1", ex.Message, "btnDelete_Click", mstrModuleName)
'        End Try
'    End Sub

'    Private Sub objbtnSearchEmployee_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchEmployee.Click
'        Try
'            Dim objfrm As New frmCommonSearch
'            With objfrm
'                .DataSource = CType(cboEmployee.DataSource, DataTable)
'                .ValueMember = cboEmployee.ValueMember
'                .DisplayMember = cboEmployee.DisplayMember
'                objfrm.CodeMember = "employeecode"
'            End With

'            If objfrm.DisplayDialog() Then
'                cboEmployee.SelectedValue = objfrm.SelectedValue
'            End If

'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "objbtnSearchEmployee_Click", mstrModuleName)
'        End Try
'    End Sub

'    Private Sub objbtnSearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearch.Click
'        Try
'            FillList()
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "objbtnSearch_Click", mstrModuleName)
'        End Try
'    End Sub

'    Private Sub objbtnReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnReset.Click
'        Try
'            If cboEmployee.Items.Count > 0 Then cboEmployee.SelectedIndex = 0
'            If cboYear.Items.Count > 0 Then cboYear.SelectedIndex = 0
'            If cboPeriod.Items.Count > 0 Then cboPeriod.SelectedIndex = 0
'            If dtpAssessmentdate.Checked Then dtpAssessmentdate.Checked = False
'            FillList()
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "objbtnReset_Click", mstrModuleName)
'        End Try
'    End Sub

'    Private Sub btnAssesorAssessment_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
'        Try
'            If lvAssesmentList.SelectedItems.Count < 1 Then
'                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please select Employee from the list to perform further operation on it."), enMsgBoxStyle.Information) '?1
'                lvAssesmentList.Select()
'                Exit Sub
'            End If

'            Dim objFrm As New frmAssessmentAnalysis
'            objFrm.mintperiodunkid = CInt(lvAssesmentList.SelectedItems(0).SubItems(colhPeriodunkid.Index).Text)
'            objFrm.mintyearunkid = CInt(lvAssesmentList.SelectedItems(0).SubItems(colhYearunkid.Index).Text)
'            objFrm.displayDialog(CInt(lvAssesmentList.SelectedItems(0).Tag), enAction.ADD_ONE, CInt(lvAssesmentList.SelectedItems(0).SubItems(colhEmployeeunkid.Index).Text), -1)

'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "btnAssesorAssessment_Click", mstrModuleName)
'        End Try
'    End Sub

'    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
'        Me.Close()
'    End Sub

'#End Region

'End Class


