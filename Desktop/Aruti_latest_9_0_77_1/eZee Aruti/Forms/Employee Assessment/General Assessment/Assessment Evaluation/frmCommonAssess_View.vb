﻿Option Strict On

#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data
Imports System.Math

#End Region

Public Class frmCommonAssess_View

#Region " Private Variables "

    Private ReadOnly mstrModuleName As String = "frmCommonAssess_View"
    Private mdsList As DataSet
    Private mintEmployeeId As Integer = -1
    Private mintAssessGroupId As Integer = -1
    Private mintYearId As Integer = -1
    Private mintPeriodId As Integer = -1
    Private objAssessMaster As clsassess_analysis_master

#End Region

#Region " Display Dialog "

    Public Function displayDialog(ByVal strName As String, _
                                  ByVal strAssessGrp As String, _
                                  ByVal intEmployeeId As Integer, _
                                  ByVal intAssessGrp As Integer, _
                                  ByVal intYearId As Integer, _
                                  ByVal intPeriodId As Integer) As Boolean
        Try
            objlblCaption.Text = strName
            objlblCaption1.Text = strAssessGrp
            mintEmployeeId = intEmployeeId
            mintAssessGroupId = intAssessGrp
            mintYearId = intYearId
            mintPeriodId = intPeriodId

            Me.ShowDialog()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "displayDialog", mstrModuleName)
        Finally
        End Try
    End Function

#End Region

#Region " Private Variables "

    Private Sub FillAssessmentResult()
        Try

            RemoveHandler lvAssessments.ItemSelectionChanged, AddressOf lvAssessments_ItemSelectionChanged

            If mdsList.Tables(0).Columns.Count > 0 Then
                Dim dtTable As New DataTable("Data")
                dtTable = mdsList.Tables(0).Copy
                dtTable.Rows.Clear()
                Dim mdecValue() As Decimal
                Dim mdecValueTotal() As Decimal : mdecValueTotal = New Decimal(mdsList.Tables(0).Columns.Count) {}
                Dim mdicIsNew As New Dictionary(Of Integer, String)
                For Each dtRow As DataRow In mdsList.Tables(0).Rows
                    mdecValue = New Decimal(mdsList.Tables(0).Columns.Count) {}
                    If mdicIsNew.ContainsKey(CInt(dtRow.Item("assessitemunkid"))) Then Continue For
                    mdicIsNew.Add(CInt(dtRow.Item("assessitemunkid")), CStr(dtRow.Item("MainGrp")))
                    Dim dtTemp() As DataRow = mdsList.Tables(0).Select("assessitemunkid = '" & CInt(dtRow.Item("assessitemunkid")) & "'")
                    If dtTemp.Length > 0 Then
                        For i As Integer = 0 To dtTemp.Length - 1
                            For j As Integer = 0 To dtTemp(i).ItemArray.Length - 1
                                Select Case j
                                    Case 0, 1, 3, 4, 5
                                    Case Else
                                        If j Mod 2 = 0 Then
                                            If IsDBNull(dtTemp(i)(j)) = False Then
                                                If IsNumeric(dtTemp(i)(j)) Then
                                                    mdecValue(j) = mdecValue(j) + CDec(dtTemp(i)(j))
                                                    mdecValueTotal(j) = mdecValueTotal(j) + CDec(dtTemp(i)(j))
                                                End If
                                            End If
                                        End If
                                End Select
                            Next
                            dtTable.ImportRow(dtTemp(i))
                        Next
                    End If

                    Dim dRow As DataRow = dtTable.NewRow

                    For i As Integer = 0 To mdecValue.Length - 1
                        If mdecValue(i) <= 0 Then
                            Select Case i
                                Case 0
                                    dRow.Item(i) = mdicIsNew(CInt(dtRow.Item("assessitemunkid")))
                                Case 1
                                    dRow.Item(i) = "Group Total : "
                                Case 4, 5
                                    dRow.Item(i) = "-1"
                            End Select
                        Else
                            dRow.Item(i) = mdecValue(i)
                        End If
                    Next
                    dtTable.Rows.Add(dRow)
                Next

                Dim drRow As DataRow = dtTable.NewRow

                For i As Integer = 0 To mdecValueTotal.Length - 1
                    If mdecValueTotal(i) <= 0 Then
                        Select Case i
                            Case 0
                                drRow.Item(i) = ""
                            Case 1
                                drRow.Item(i) = "Grand Total : "
                            Case 4, 5
                                drRow.Item(i) = "-2"
                        End Select
                    Else
                        drRow.Item(i) = mdecValueTotal(i)
                    End If
                Next

                dtTable.Rows.Add(drRow)

                Dim strColumns As String = String.Empty
                Dim iCntColumns As Integer = 0
                For Each dCol As DataColumn In dtTable.Columns
                    Select Case iCntColumns
                        Case 0, 4, 5
                            lvAssessments.Columns.Add(dCol.Caption, 0, HorizontalAlignment.Left)
                        Case 1
                            lvAssessments.Columns.Add(dCol.Caption, 500, HorizontalAlignment.Left)
                        Case Else
                            lvAssessments.Columns.Add(dCol.Caption, 120, HorizontalAlignment.Left)
                    End Select
                    strColumns &= "," & dCol.ColumnName
                    iCntColumns += 1
                Next

                strColumns = Mid(strColumns, 2)
                Dim strColumnName() As String = strColumns.Split(CChar(","))

                For Each dtRow As DataRow In dtTable.Rows
                    Dim lvItem As New ListViewItem
                    For i As Integer = 0 To iCntColumns - 1
                        Select Case i
                            Case 0
                                lvItem.Text = dtRow.Item(strColumnName(i)).ToString
                            Case Else
                                lvItem.SubItems.Add(dtRow.Item(strColumnName(i)).ToString)
                        End Select
                    Next
                    lvAssessments.Items.Add(lvItem)
                Next

                lvAssessments.GroupingColumn = lvAssessments.Columns(0)
                lvAssessments.DisplayGroups(True)

                For Each lvItem As ListViewItem In lvAssessments.Items
                    If CInt(lvItem.SubItems(4).Text) = -1 Then
                        lvItem.BackColor = Color.SteelBlue
                        lvItem.ForeColor = Color.White
                        lvItem.Font = New Font(Me.Font, FontStyle.Bold)
                    ElseIf CInt(lvItem.SubItems(4).Text) = -2 Then
                        lvItem.BackColor = Color.Maroon
                        lvItem.ForeColor = Color.White
                        lvItem.Font = New Font(Me.Font, FontStyle.Bold)
                    End If
                Next
            End If

            AddHandler lvAssessments.ItemSelectionChanged, AddressOf lvAssessments_ItemSelectionChanged

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillAssessmentResult", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub FillRemarks(ByVal lvListView As eZee.Common.eZeeListView)
        Try
            lvListView.Items.Clear() : lvListView.GridLines = False
            Select Case CType(lvListView, eZee.Common.eZeeListView).Name.ToUpper
                Case "LVIMPROVEMENT"
                    mdsList = objAssessMaster.GetRemarksTran(True, mintEmployeeId, mintAssessGroupId, mintYearId, mintPeriodId)
                    For Each dRow As DataRow In mdsList.Tables(0).Rows
                        Dim lvItem As New ListViewItem

                        lvItem.Text = dRow.Item("Major_Area").ToString
                        lvItem.SubItems.Add(dRow.Item("Action_Activity").ToString)
                        lvItem.SubItems.Add(dRow.Item("Support").ToString)
                        lvItem.SubItems.Add(dRow.Item("Course_Master").ToString)
                        If dRow.Item("TimeFrame").ToString.Trim.Length > 0 Then
                            lvItem.SubItems.Add(eZeeDate.convertDate(dRow.Item("TimeFrame").ToString).ToShortDateString)
                        Else
                            lvItem.SubItems.Add("")
                        End If
                        lvItem.SubItems.Add(dRow.Item("Other").ToString)

                        lvListView.Items.Add(lvItem)
                        lvItem = Nothing
                    Next
                Case "LVPERSONALDEVELOP"
                    mdsList = objAssessMaster.GetRemarksTran(False, mintEmployeeId, mintAssessGroupId, mintYearId, mintPeriodId)

                    For Each dRow As DataRow In mdsList.Tables(0).Rows
                        Dim lvItem As New ListViewItem

                        lvItem.Text = dRow.Item("Major_Area").ToString
                        lvItem.SubItems.Add(dRow.Item("Action_Activity").ToString)
                        lvItem.SubItems.Add(dRow.Item("Support").ToString)
                        lvItem.SubItems.Add(dRow.Item("Course_Master").ToString)
                        If dRow.Item("TimeFrame").ToString.Trim.Length > 0 Then
                            lvItem.SubItems.Add(eZeeDate.convertDate(dRow.Item("TimeFrame").ToString).ToShortDateString)
                        Else
                            lvItem.SubItems.Add("")
                        End If
                        lvItem.SubItems.Add(dRow.Item("Other").ToString)
                        lvItem.SubItems.Add(dRow.Item("Appraiser").ToString)

                        lvListView.Items.Add(lvItem)
                        lvItem = Nothing
                    Next
                    lvListView.GroupingColumn = objcolhAppraiser
                    lvListView.DisplayGroups(True)
            End Select
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

#Region " Form's Events "

    Private Sub frmCommonAssess_View_Shown(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Shown
        Try
            Call FillAssessmentResult()
            Call FillRemarks(lvImprovement)
            Call FillRemarks(lvPersonalDevelop)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmCommonAssess_View_Shown", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub frmCommonAssess_View_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        objAssessMaster = New clsassess_analysis_master
        Try
            Call Set_Logo(Me, gApplicationType)
            Language.setLanguage(Me.Name)
            Call OtherSettings()

            mdsList = objAssessMaster.GetAssessmentResultView(mintEmployeeId, mintAssessGroupId, mintYearId, mintPeriodId)

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

#Region " Button's Events "

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnClose_Click", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

#Region " Control's Events "

    Private Sub lvAssessments_ItemSelectionChanged(ByVal sender As Object, ByVal e As System.Windows.Forms.ListViewItemSelectionChangedEventArgs) Handles lvAssessments.ItemSelectionChanged
        Try
            If lvAssessments.SelectedItems.Count > 0 Then
                If lvAssessments.SelectedItems(0).SubItems(4).Text = "-1" Or _
                   lvAssessments.SelectedItems(0).SubItems(4).Text = "-2" Then
                    e.Item.Selected = False
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lvAssessments_ItemSelectionChanged", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Private Sub OtherSettings()
        Try
            Me.SuspendLayout()

            Call SetLanguage()

            Me.gbAssessmentInfo.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbAssessmentInfo.ForeColor = GUI._eZeeContainerHeaderForeColor


            Me.btnClose.GradientBackColor = GUI._ButttonBackColor
            Me.btnClose.GradientForeColor = GUI._ButttonFontColor

            Me.ResumeLayout()
        Catch Ex As Exception
            DisplayError.Show("-1", ex.Message, "OtherSettings", mstrModuleName)
        End Try
    End Sub


    Private Sub SetLanguage()
        Try
            Me.Text = Language._Object.getCaption(Me.Name, Me.Text)

            Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
            Me.lblAssessGroup.Text = Language._Object.getCaption(Me.lblAssessGroup.Name, Me.lblAssessGroup.Text)
            Me.lblEmployee.Text = Language._Object.getCaption(Me.lblEmployee.Name, Me.lblEmployee.Text)
            Me.tabpAssessmentResults.Text = Language._Object.getCaption(Me.tabpAssessmentResults.Name, Me.tabpAssessmentResults.Text)
            Me.tabpAssessmentRemarks.Text = Language._Object.getCaption(Me.tabpAssessmentRemarks.Name, Me.tabpAssessmentRemarks.Text)
            Me.gbAssessmentInfo.Text = Language._Object.getCaption(Me.gbAssessmentInfo.Name, Me.gbAssessmentInfo.Text)
            Me.colhI_Improvement.Text = Language._Object.getCaption(CStr(Me.colhI_Improvement.Tag), Me.colhI_Improvement.Text)
            Me.colhI_ActivityReq.Text = Language._Object.getCaption(CStr(Me.colhI_ActivityReq.Tag), Me.colhI_ActivityReq.Text)
            Me.colhI_Support.Text = Language._Object.getCaption(CStr(Me.colhI_Support.Tag), Me.colhI_Support.Text)
            Me.colhI_Course.Text = Language._Object.getCaption(CStr(Me.colhI_Course.Tag), Me.colhI_Course.Text)
            Me.colhI_Timeframe.Text = Language._Object.getCaption(CStr(Me.colhI_Timeframe.Tag), Me.colhI_Timeframe.Text)
            Me.colhI_OtherTraning.Text = Language._Object.getCaption(CStr(Me.colhI_OtherTraning.Tag), Me.colhI_OtherTraning.Text)
            Me.colhP_Development.Text = Language._Object.getCaption(CStr(Me.colhP_Development.Tag), Me.colhP_Development.Text)
            Me.colhP_ActivityReq.Text = Language._Object.getCaption(CStr(Me.colhP_ActivityReq.Tag), Me.colhP_ActivityReq.Text)
            Me.colhP_Support.Text = Language._Object.getCaption(CStr(Me.colhP_Support.Tag), Me.colhP_Support.Text)
            Me.colhP_Course.Text = Language._Object.getCaption(CStr(Me.colhP_Course.Tag), Me.colhP_Course.Text)
            Me.colhP_Timeframe.Text = Language._Object.getCaption(CStr(Me.colhP_Timeframe.Tag), Me.colhP_Timeframe.Text)
            Me.colhP_OtherTraning.Text = Language._Object.getCaption(CStr(Me.colhP_OtherTraning.Tag), Me.colhP_OtherTraning.Text)
            Me.tabpSelf.Text = Language._Object.getCaption(Me.tabpSelf.Name, Me.tabpSelf.Text)
            Me.tabpAppriasers.Text = Language._Object.getCaption(Me.tabpAppriasers.Name, Me.tabpAppriasers.Text)

        Catch Ex As Exception
            DisplayError.Show("-1", ex.Message, "SetLanguage", mstrModuleName)
        End Try
    End Sub


    Private Sub SetMessages()
        Try

        Catch Ex As Exception
            DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>

End Class