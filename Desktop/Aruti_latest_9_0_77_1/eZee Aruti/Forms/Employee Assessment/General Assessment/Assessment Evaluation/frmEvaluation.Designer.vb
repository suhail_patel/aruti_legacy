﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmEvaluation
    Inherits eZee.Common.eZeeForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmEvaluation))
        Me.objFooter = New eZee.Common.eZeeFooter
        Me.btnSaveCommit = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnSave = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnClose = New eZee.Common.eZeeLightButton(Me.components)
        Me.tabcEvaluation = New System.Windows.Forms.TabControl
        Me.tabpCompetenceEvaluation = New System.Windows.Forms.TabPage
        Me.gbAssessmentInfo = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.lblWeight = New System.Windows.Forms.Label
        Me.txtWeightage = New System.Windows.Forms.TextBox
        Me.pnlContainer = New System.Windows.Forms.Panel
        Me.lblAssessDate = New System.Windows.Forms.Label
        Me.lblEmployee = New System.Windows.Forms.Label
        Me.dtpAssessdate = New System.Windows.Forms.DateTimePicker
        Me.lblYear = New System.Windows.Forms.Label
        Me.objbtnSearchEmployee = New eZee.Common.eZeeGradientButton
        Me.lblPeriod = New System.Windows.Forms.Label
        Me.cboYear = New System.Windows.Forms.ComboBox
        Me.cboEmployee = New System.Windows.Forms.ComboBox
        Me.cboPeriod = New System.Windows.Forms.ComboBox
        Me.cboAssessor = New System.Windows.Forms.ComboBox
        Me.lblAssessor = New System.Windows.Forms.Label
        Me.objbtnSearchAssessor = New eZee.Common.eZeeGradientButton
        Me.cboReviewer = New System.Windows.Forms.ComboBox
        Me.objbtnSearchReviewer = New eZee.Common.eZeeGradientButton
        Me.radExternalAssessor = New System.Windows.Forms.RadioButton
        Me.radInternalAssessor = New System.Windows.Forms.RadioButton
        Me.lnkViewAssessments = New System.Windows.Forms.LinkLabel
        Me.pnlEvaluationList = New System.Windows.Forms.Panel
        Me.lvEvaluation = New eZee.Common.eZeeListView(Me.components)
        Me.objdgcolhAssessItem = New System.Windows.Forms.ColumnHeader
        Me.colhAssessSubItem = New System.Windows.Forms.ColumnHeader
        Me.colhResult = New System.Windows.Forms.ColumnHeader
        Me.colhRemark = New System.Windows.Forms.ColumnHeader
        Me.objcolhAssessItemId = New System.Windows.Forms.ColumnHeader
        Me.objcolhSubItemId = New System.Windows.Forms.ColumnHeader
        Me.objcolhResultId = New System.Windows.Forms.ColumnHeader
        Me.objcolhGUID = New System.Windows.Forms.ColumnHeader
        Me.objcolhSortId = New System.Windows.Forms.ColumnHeader
        Me.btnDeleteEvaluation = New eZee.Common.eZeeLightButton(Me.components)
        Me.objLine3 = New eZee.Common.eZeeStraightLine
        Me.btnEditEvaluation = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnAddEvaluation = New eZee.Common.eZeeLightButton(Me.components)
        Me.objLine2 = New eZee.Common.eZeeStraightLine
        Me.objLine1 = New eZee.Common.eZeeStraightLine
        Me.lblRemark = New System.Windows.Forms.Label
        Me.txtRemark = New System.Windows.Forms.RichTextBox
        Me.objbtnSearchResult = New eZee.Common.eZeeGradientButton
        Me.objbtnSearchGroup = New eZee.Common.eZeeGradientButton
        Me.cboAssessGroup = New System.Windows.Forms.ComboBox
        Me.cboResultCode = New System.Windows.Forms.ComboBox
        Me.objbtnSearchSubItems = New eZee.Common.eZeeGradientButton
        Me.lblResult = New System.Windows.Forms.Label
        Me.lblAssessGroup = New System.Windows.Forms.Label
        Me.lblAssessmentItems = New System.Windows.Forms.Label
        Me.cboAssessItem = New System.Windows.Forms.ComboBox
        Me.lblSubItems = New System.Windows.Forms.Label
        Me.cboSubItems = New System.Windows.Forms.ComboBox
        Me.objbtnSearchItems = New eZee.Common.eZeeGradientButton
        Me.lblReviewer = New System.Windows.Forms.Label
        Me.tabpImporvementArea = New System.Windows.Forms.TabPage
        Me.gbImprovement = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.pnlImproveList = New System.Windows.Forms.Panel
        Me.lvImprovement = New eZee.Common.eZeeListView(Me.components)
        Me.colhI_Improvement = New System.Windows.Forms.ColumnHeader
        Me.colhI_ActivityReq = New System.Windows.Forms.ColumnHeader
        Me.colhI_Support = New System.Windows.Forms.ColumnHeader
        Me.colhI_Course = New System.Windows.Forms.ColumnHeader
        Me.colhI_Timeframe = New System.Windows.Forms.ColumnHeader
        Me.colhI_OtherTraning = New System.Windows.Forms.ColumnHeader
        Me.objcolhI_CourseId = New System.Windows.Forms.ColumnHeader
        Me.objcolhI_GUID = New System.Windows.Forms.ColumnHeader
        Me.objStLine5 = New eZee.Common.eZeeStraightLine
        Me.btnDeleteImprovement = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnEditImprovement = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnAddImprovement = New eZee.Common.eZeeLightButton(Me.components)
        Me.objStLine4 = New eZee.Common.eZeeStraightLine
        Me.objbtnSearchITraning = New eZee.Common.eZeeGradientButton
        Me.lblILearningObjective = New System.Windows.Forms.Label
        Me.lblITime = New System.Windows.Forms.Label
        Me.tabcImprovement = New System.Windows.Forms.TabControl
        Me.tabpImprovement = New System.Windows.Forms.TabPage
        Me.txtI_Improvement = New eZee.TextBox.AlphanumericTextBox
        Me.tapbActionRequired = New System.Windows.Forms.TabPage
        Me.txtI_Action = New eZee.TextBox.AlphanumericTextBox
        Me.tabpSupportRequired = New System.Windows.Forms.TabPage
        Me.txtI_Support = New eZee.TextBox.AlphanumericTextBox
        Me.tabpOtherTraining = New System.Windows.Forms.TabPage
        Me.txtI_OtherTraining = New eZee.TextBox.AlphanumericTextBox
        Me.cboITraining = New System.Windows.Forms.ComboBox
        Me.dtpITimeframe = New System.Windows.Forms.DateTimePicker
        Me.tabpPersonalDevelopment = New System.Windows.Forms.TabPage
        Me.gbPersonalDevelopment = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.pnlPersonalList = New System.Windows.Forms.Panel
        Me.lvPersonalDevelop = New eZee.Common.eZeeListView(Me.components)
        Me.colhP_Development = New System.Windows.Forms.ColumnHeader
        Me.colhP_ActivityReq = New System.Windows.Forms.ColumnHeader
        Me.colhP_Support = New System.Windows.Forms.ColumnHeader
        Me.colhP_Course = New System.Windows.Forms.ColumnHeader
        Me.colhP_Timeframe = New System.Windows.Forms.ColumnHeader
        Me.colhP_OtherTraning = New System.Windows.Forms.ColumnHeader
        Me.objcolhP_CourseId = New System.Windows.Forms.ColumnHeader
        Me.objcolhP_GUID = New System.Windows.Forms.ColumnHeader
        Me.objStLine7 = New eZee.Common.eZeeStraightLine
        Me.btnDeletePersonal = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnEditPersonal = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnAddPersonal = New eZee.Common.eZeeLightButton(Me.components)
        Me.objStLine6 = New eZee.Common.eZeeStraightLine
        Me.objbtnSearchPTraning = New eZee.Common.eZeeGradientButton
        Me.lblPLearningObjective = New System.Windows.Forms.Label
        Me.lblPTime = New System.Windows.Forms.Label
        Me.tabcPersonal = New System.Windows.Forms.TabControl
        Me.tabpDevelopment = New System.Windows.Forms.TabPage
        Me.txtP_Development = New eZee.TextBox.AlphanumericTextBox
        Me.tabpPAction = New System.Windows.Forms.TabPage
        Me.txtP_Action = New eZee.TextBox.AlphanumericTextBox
        Me.tabpPSupport = New System.Windows.Forms.TabPage
        Me.txtP_Support = New eZee.TextBox.AlphanumericTextBox
        Me.tabpPOtherTraining = New System.Windows.Forms.TabPage
        Me.txtP_OtherTraining = New eZee.TextBox.AlphanumericTextBox
        Me.cboPTraining = New System.Windows.Forms.ComboBox
        Me.dtpPTimeframe = New System.Windows.Forms.DateTimePicker
        Me.tabpReviewerComments = New System.Windows.Forms.TabPage
        Me.gbComments = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.elRemark1 = New eZee.Common.eZeeLine
        Me.pnlRemark2 = New System.Windows.Forms.Panel
        Me.txtRemark2 = New eZee.TextBox.AlphanumericTextBox
        Me.elRemark2 = New eZee.Common.eZeeLine
        Me.pnlRemark1 = New System.Windows.Forms.Panel
        Me.txtRemark1 = New eZee.TextBox.AlphanumericTextBox
        Me.pnlEvaluation = New System.Windows.Forms.Panel
        Me.lnkScoreGuide = New System.Windows.Forms.LinkLabel
        Me.objFooter.SuspendLayout()
        Me.tabcEvaluation.SuspendLayout()
        Me.tabpCompetenceEvaluation.SuspendLayout()
        Me.gbAssessmentInfo.SuspendLayout()
        Me.pnlContainer.SuspendLayout()
        Me.pnlEvaluationList.SuspendLayout()
        Me.tabpImporvementArea.SuspendLayout()
        Me.gbImprovement.SuspendLayout()
        Me.pnlImproveList.SuspendLayout()
        Me.tabcImprovement.SuspendLayout()
        Me.tabpImprovement.SuspendLayout()
        Me.tapbActionRequired.SuspendLayout()
        Me.tabpSupportRequired.SuspendLayout()
        Me.tabpOtherTraining.SuspendLayout()
        Me.tabpPersonalDevelopment.SuspendLayout()
        Me.gbPersonalDevelopment.SuspendLayout()
        Me.pnlPersonalList.SuspendLayout()
        Me.tabcPersonal.SuspendLayout()
        Me.tabpDevelopment.SuspendLayout()
        Me.tabpPAction.SuspendLayout()
        Me.tabpPSupport.SuspendLayout()
        Me.tabpPOtherTraining.SuspendLayout()
        Me.tabpReviewerComments.SuspendLayout()
        Me.gbComments.SuspendLayout()
        Me.pnlRemark2.SuspendLayout()
        Me.pnlRemark1.SuspendLayout()
        Me.pnlEvaluation.SuspendLayout()
        Me.SuspendLayout()
        '
        'objFooter
        '
        Me.objFooter.BorderColor = System.Drawing.Color.Silver
        Me.objFooter.Controls.Add(Me.btnSaveCommit)
        Me.objFooter.Controls.Add(Me.btnSave)
        Me.objFooter.Controls.Add(Me.btnClose)
        Me.objFooter.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.objFooter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objFooter.GradiantStyle = eZee.Common.eZeeFooter.GradientStyle.Up
        Me.objFooter.GradientColor1 = System.Drawing.SystemColors.Control
        Me.objFooter.GradientColor2 = System.Drawing.SystemColors.Control
        Me.objFooter.Location = New System.Drawing.Point(0, 461)
        Me.objFooter.Name = "objFooter"
        Me.objFooter.Size = New System.Drawing.Size(906, 55)
        Me.objFooter.TabIndex = 1
        '
        'btnSaveCommit
        '
        Me.btnSaveCommit.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnSaveCommit.BackColor = System.Drawing.Color.White
        Me.btnSaveCommit.BackgroundImage = CType(resources.GetObject("btnSaveCommit.BackgroundImage"), System.Drawing.Image)
        Me.btnSaveCommit.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnSaveCommit.BorderColor = System.Drawing.Color.Empty
        Me.btnSaveCommit.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnSaveCommit.FlatAppearance.BorderSize = 0
        Me.btnSaveCommit.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnSaveCommit.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSaveCommit.ForeColor = System.Drawing.Color.Black
        Me.btnSaveCommit.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnSaveCommit.GradientForeColor = System.Drawing.Color.Black
        Me.btnSaveCommit.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSaveCommit.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnSaveCommit.Location = New System.Drawing.Point(578, 13)
        Me.btnSaveCommit.Name = "btnSaveCommit"
        Me.btnSaveCommit.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSaveCommit.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnSaveCommit.Size = New System.Drawing.Size(110, 30)
        Me.btnSaveCommit.TabIndex = 0
        Me.btnSaveCommit.Text = "S&ave && Commit"
        Me.btnSaveCommit.UseVisualStyleBackColor = True
        '
        'btnSave
        '
        Me.btnSave.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnSave.BackColor = System.Drawing.Color.White
        Me.btnSave.BackgroundImage = CType(resources.GetObject("btnSave.BackgroundImage"), System.Drawing.Image)
        Me.btnSave.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnSave.BorderColor = System.Drawing.Color.Empty
        Me.btnSave.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnSave.FlatAppearance.BorderSize = 0
        Me.btnSave.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnSave.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSave.ForeColor = System.Drawing.Color.Black
        Me.btnSave.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnSave.GradientForeColor = System.Drawing.Color.Black
        Me.btnSave.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSave.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnSave.Location = New System.Drawing.Point(694, 13)
        Me.btnSave.Name = "btnSave"
        Me.btnSave.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSave.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnSave.Size = New System.Drawing.Size(97, 30)
        Me.btnSave.TabIndex = 1
        Me.btnSave.Text = "&Save"
        Me.btnSave.UseVisualStyleBackColor = True
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnClose.BackColor = System.Drawing.Color.White
        Me.btnClose.BackgroundImage = CType(resources.GetObject("btnClose.BackgroundImage"), System.Drawing.Image)
        Me.btnClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnClose.BorderColor = System.Drawing.Color.Empty
        Me.btnClose.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnClose.FlatAppearance.BorderSize = 0
        Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClose.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.ForeColor = System.Drawing.Color.Black
        Me.btnClose.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnClose.GradientForeColor = System.Drawing.Color.Black
        Me.btnClose.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Location = New System.Drawing.Point(797, 13)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Size = New System.Drawing.Size(97, 30)
        Me.btnClose.TabIndex = 2
        Me.btnClose.Text = "&Close"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'tabcEvaluation
        '
        Me.tabcEvaluation.Controls.Add(Me.tabpCompetenceEvaluation)
        Me.tabcEvaluation.Controls.Add(Me.tabpImporvementArea)
        Me.tabcEvaluation.Controls.Add(Me.tabpPersonalDevelopment)
        Me.tabcEvaluation.Controls.Add(Me.tabpReviewerComments)
        Me.tabcEvaluation.Dock = System.Windows.Forms.DockStyle.Fill
        Me.tabcEvaluation.Location = New System.Drawing.Point(0, 0)
        Me.tabcEvaluation.Multiline = True
        Me.tabcEvaluation.Name = "tabcEvaluation"
        Me.tabcEvaluation.SelectedIndex = 0
        Me.tabcEvaluation.Size = New System.Drawing.Size(906, 461)
        Me.tabcEvaluation.TabIndex = 0
        '
        'tabpCompetenceEvaluation
        '
        Me.tabpCompetenceEvaluation.Controls.Add(Me.gbAssessmentInfo)
        Me.tabpCompetenceEvaluation.Location = New System.Drawing.Point(4, 22)
        Me.tabpCompetenceEvaluation.Name = "tabpCompetenceEvaluation"
        Me.tabpCompetenceEvaluation.Size = New System.Drawing.Size(898, 435)
        Me.tabpCompetenceEvaluation.TabIndex = 0
        Me.tabpCompetenceEvaluation.Text = "Evaluation"
        Me.tabpCompetenceEvaluation.UseVisualStyleBackColor = True
        '
        'gbAssessmentInfo
        '
        Me.gbAssessmentInfo.BorderColor = System.Drawing.Color.Black
        Me.gbAssessmentInfo.Checked = False
        Me.gbAssessmentInfo.CollapseAllExceptThis = False
        Me.gbAssessmentInfo.CollapsedHoverImage = Nothing
        Me.gbAssessmentInfo.CollapsedNormalImage = Nothing
        Me.gbAssessmentInfo.CollapsedPressedImage = Nothing
        Me.gbAssessmentInfo.CollapseOnLoad = False
        Me.gbAssessmentInfo.Controls.Add(Me.lnkScoreGuide)
        Me.gbAssessmentInfo.Controls.Add(Me.lblWeight)
        Me.gbAssessmentInfo.Controls.Add(Me.txtWeightage)
        Me.gbAssessmentInfo.Controls.Add(Me.pnlContainer)
        Me.gbAssessmentInfo.Controls.Add(Me.cboAssessor)
        Me.gbAssessmentInfo.Controls.Add(Me.lblAssessor)
        Me.gbAssessmentInfo.Controls.Add(Me.objbtnSearchAssessor)
        Me.gbAssessmentInfo.Controls.Add(Me.cboReviewer)
        Me.gbAssessmentInfo.Controls.Add(Me.objbtnSearchReviewer)
        Me.gbAssessmentInfo.Controls.Add(Me.radExternalAssessor)
        Me.gbAssessmentInfo.Controls.Add(Me.radInternalAssessor)
        Me.gbAssessmentInfo.Controls.Add(Me.lnkViewAssessments)
        Me.gbAssessmentInfo.Controls.Add(Me.pnlEvaluationList)
        Me.gbAssessmentInfo.Controls.Add(Me.btnDeleteEvaluation)
        Me.gbAssessmentInfo.Controls.Add(Me.objLine3)
        Me.gbAssessmentInfo.Controls.Add(Me.btnEditEvaluation)
        Me.gbAssessmentInfo.Controls.Add(Me.btnAddEvaluation)
        Me.gbAssessmentInfo.Controls.Add(Me.objLine2)
        Me.gbAssessmentInfo.Controls.Add(Me.objLine1)
        Me.gbAssessmentInfo.Controls.Add(Me.lblRemark)
        Me.gbAssessmentInfo.Controls.Add(Me.txtRemark)
        Me.gbAssessmentInfo.Controls.Add(Me.objbtnSearchResult)
        Me.gbAssessmentInfo.Controls.Add(Me.objbtnSearchGroup)
        Me.gbAssessmentInfo.Controls.Add(Me.cboAssessGroup)
        Me.gbAssessmentInfo.Controls.Add(Me.cboResultCode)
        Me.gbAssessmentInfo.Controls.Add(Me.objbtnSearchSubItems)
        Me.gbAssessmentInfo.Controls.Add(Me.lblResult)
        Me.gbAssessmentInfo.Controls.Add(Me.lblAssessGroup)
        Me.gbAssessmentInfo.Controls.Add(Me.lblAssessmentItems)
        Me.gbAssessmentInfo.Controls.Add(Me.cboAssessItem)
        Me.gbAssessmentInfo.Controls.Add(Me.lblSubItems)
        Me.gbAssessmentInfo.Controls.Add(Me.cboSubItems)
        Me.gbAssessmentInfo.Controls.Add(Me.objbtnSearchItems)
        Me.gbAssessmentInfo.Controls.Add(Me.lblReviewer)
        Me.gbAssessmentInfo.Dock = System.Windows.Forms.DockStyle.Fill
        Me.gbAssessmentInfo.ExpandedHoverImage = Nothing
        Me.gbAssessmentInfo.ExpandedNormalImage = Nothing
        Me.gbAssessmentInfo.ExpandedPressedImage = Nothing
        Me.gbAssessmentInfo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbAssessmentInfo.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbAssessmentInfo.HeaderHeight = 25
        Me.gbAssessmentInfo.HeaderMessage = ""
        Me.gbAssessmentInfo.HeaderMessageFont = New System.Drawing.Font("Microsoft Sans Serif", 8.25!)
        Me.gbAssessmentInfo.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbAssessmentInfo.HeightOnCollapse = 0
        Me.gbAssessmentInfo.LeftTextSpace = 0
        Me.gbAssessmentInfo.Location = New System.Drawing.Point(0, 0)
        Me.gbAssessmentInfo.Name = "gbAssessmentInfo"
        Me.gbAssessmentInfo.OpenHeight = 300
        Me.gbAssessmentInfo.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbAssessmentInfo.ShowBorder = True
        Me.gbAssessmentInfo.ShowCheckBox = False
        Me.gbAssessmentInfo.ShowCollapseButton = False
        Me.gbAssessmentInfo.ShowDefaultBorderColor = True
        Me.gbAssessmentInfo.ShowDownButton = False
        Me.gbAssessmentInfo.ShowHeader = True
        Me.gbAssessmentInfo.Size = New System.Drawing.Size(898, 435)
        Me.gbAssessmentInfo.TabIndex = 0
        Me.gbAssessmentInfo.Temp = 0
        Me.gbAssessmentInfo.Text = "Evaluation Info."
        Me.gbAssessmentInfo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblWeight
        '
        Me.lblWeight.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblWeight.Location = New System.Drawing.Point(475, 161)
        Me.lblWeight.Name = "lblWeight"
        Me.lblWeight.Size = New System.Drawing.Size(68, 17)
        Me.lblWeight.TabIndex = 385
        Me.lblWeight.Text = "Weightage"
        Me.lblWeight.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtWeightage
        '
        Me.txtWeightage.BackColor = System.Drawing.Color.White
        Me.txtWeightage.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtWeightage.Location = New System.Drawing.Point(549, 159)
        Me.txtWeightage.Name = "txtWeightage"
        Me.txtWeightage.ReadOnly = True
        Me.txtWeightage.Size = New System.Drawing.Size(59, 21)
        Me.txtWeightage.TabIndex = 386
        Me.txtWeightage.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'pnlContainer
        '
        Me.pnlContainer.Controls.Add(Me.lblAssessDate)
        Me.pnlContainer.Controls.Add(Me.lblEmployee)
        Me.pnlContainer.Controls.Add(Me.dtpAssessdate)
        Me.pnlContainer.Controls.Add(Me.lblYear)
        Me.pnlContainer.Controls.Add(Me.objbtnSearchEmployee)
        Me.pnlContainer.Controls.Add(Me.lblPeriod)
        Me.pnlContainer.Controls.Add(Me.cboYear)
        Me.pnlContainer.Controls.Add(Me.cboEmployee)
        Me.pnlContainer.Controls.Add(Me.cboPeriod)
        Me.pnlContainer.Location = New System.Drawing.Point(5, 58)
        Me.pnlContainer.Name = "pnlContainer"
        Me.pnlContainer.Size = New System.Drawing.Size(344, 79)
        Me.pnlContainer.TabIndex = 3
        '
        'lblAssessDate
        '
        Me.lblAssessDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblAssessDate.Location = New System.Drawing.Point(179, 32)
        Me.lblAssessDate.Name = "lblAssessDate"
        Me.lblAssessDate.Size = New System.Drawing.Size(44, 15)
        Me.lblAssessDate.TabIndex = 5
        Me.lblAssessDate.Text = "Date"
        Me.lblAssessDate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblEmployee
        '
        Me.lblEmployee.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEmployee.Location = New System.Drawing.Point(3, 5)
        Me.lblEmployee.Name = "lblEmployee"
        Me.lblEmployee.Size = New System.Drawing.Size(66, 15)
        Me.lblEmployee.TabIndex = 0
        Me.lblEmployee.Text = "Employee"
        Me.lblEmployee.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'dtpAssessdate
        '
        Me.dtpAssessdate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpAssessdate.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpAssessdate.Location = New System.Drawing.Point(229, 29)
        Me.dtpAssessdate.Name = "dtpAssessdate"
        Me.dtpAssessdate.Size = New System.Drawing.Size(87, 21)
        Me.dtpAssessdate.TabIndex = 6
        '
        'lblYear
        '
        Me.lblYear.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblYear.Location = New System.Drawing.Point(3, 32)
        Me.lblYear.Name = "lblYear"
        Me.lblYear.Size = New System.Drawing.Size(66, 15)
        Me.lblYear.TabIndex = 3
        Me.lblYear.Text = "Year"
        Me.lblYear.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objbtnSearchEmployee
        '
        Me.objbtnSearchEmployee.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchEmployee.BorderSelected = False
        Me.objbtnSearchEmployee.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchEmployee.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objbtnSearchEmployee.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchEmployee.Location = New System.Drawing.Point(322, 2)
        Me.objbtnSearchEmployee.Name = "objbtnSearchEmployee"
        Me.objbtnSearchEmployee.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchEmployee.TabIndex = 2
        '
        'lblPeriod
        '
        Me.lblPeriod.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPeriod.Location = New System.Drawing.Point(3, 59)
        Me.lblPeriod.Name = "lblPeriod"
        Me.lblPeriod.Size = New System.Drawing.Size(66, 15)
        Me.lblPeriod.TabIndex = 7
        Me.lblPeriod.Text = "Period"
        Me.lblPeriod.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboYear
        '
        Me.cboYear.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboYear.DropDownWidth = 150
        Me.cboYear.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboYear.FormattingEnabled = True
        Me.cboYear.Location = New System.Drawing.Point(75, 29)
        Me.cboYear.Name = "cboYear"
        Me.cboYear.Size = New System.Drawing.Size(98, 21)
        Me.cboYear.TabIndex = 4
        '
        'cboEmployee
        '
        Me.cboEmployee.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboEmployee.DropDownWidth = 250
        Me.cboEmployee.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboEmployee.FormattingEnabled = True
        Me.cboEmployee.Location = New System.Drawing.Point(75, 2)
        Me.cboEmployee.Name = "cboEmployee"
        Me.cboEmployee.Size = New System.Drawing.Size(241, 21)
        Me.cboEmployee.TabIndex = 1
        '
        'cboPeriod
        '
        Me.cboPeriod.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboPeriod.DropDownWidth = 200
        Me.cboPeriod.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboPeriod.FormattingEnabled = True
        Me.cboPeriod.Location = New System.Drawing.Point(75, 56)
        Me.cboPeriod.Name = "cboPeriod"
        Me.cboPeriod.Size = New System.Drawing.Size(241, 21)
        Me.cboPeriod.TabIndex = 8
        '
        'cboAssessor
        '
        Me.cboAssessor.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboAssessor.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboAssessor.FormattingEnabled = True
        Me.cboAssessor.Location = New System.Drawing.Point(80, 33)
        Me.cboAssessor.Name = "cboAssessor"
        Me.cboAssessor.Size = New System.Drawing.Size(241, 21)
        Me.cboAssessor.TabIndex = 1
        '
        'lblAssessor
        '
        Me.lblAssessor.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblAssessor.Location = New System.Drawing.Point(8, 36)
        Me.lblAssessor.Name = "lblAssessor"
        Me.lblAssessor.Size = New System.Drawing.Size(66, 15)
        Me.lblAssessor.TabIndex = 0
        Me.lblAssessor.Text = "Assessor"
        Me.lblAssessor.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objbtnSearchAssessor
        '
        Me.objbtnSearchAssessor.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchAssessor.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchAssessor.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchAssessor.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchAssessor.BorderSelected = False
        Me.objbtnSearchAssessor.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchAssessor.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objbtnSearchAssessor.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchAssessor.Location = New System.Drawing.Point(326, 33)
        Me.objbtnSearchAssessor.Name = "objbtnSearchAssessor"
        Me.objbtnSearchAssessor.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchAssessor.TabIndex = 2
        '
        'cboReviewer
        '
        Me.cboReviewer.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboReviewer.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboReviewer.FormattingEnabled = True
        Me.cboReviewer.Location = New System.Drawing.Point(80, 33)
        Me.cboReviewer.Name = "cboReviewer"
        Me.cboReviewer.Size = New System.Drawing.Size(241, 21)
        Me.cboReviewer.TabIndex = 382
        '
        'objbtnSearchReviewer
        '
        Me.objbtnSearchReviewer.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchReviewer.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchReviewer.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchReviewer.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchReviewer.BorderSelected = False
        Me.objbtnSearchReviewer.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchReviewer.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objbtnSearchReviewer.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchReviewer.Location = New System.Drawing.Point(326, 33)
        Me.objbtnSearchReviewer.Name = "objbtnSearchReviewer"
        Me.objbtnSearchReviewer.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchReviewer.TabIndex = 383
        '
        'radExternalAssessor
        '
        Me.radExternalAssessor.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.radExternalAssessor.BackColor = System.Drawing.Color.Transparent
        Me.radExternalAssessor.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.radExternalAssessor.Location = New System.Drawing.Point(768, 3)
        Me.radExternalAssessor.Name = "radExternalAssessor"
        Me.radExternalAssessor.Size = New System.Drawing.Size(127, 17)
        Me.radExternalAssessor.TabIndex = 380
        Me.radExternalAssessor.TabStop = True
        Me.radExternalAssessor.Text = "External Assessor"
        Me.radExternalAssessor.UseVisualStyleBackColor = False
        '
        'radInternalAssessor
        '
        Me.radInternalAssessor.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.radInternalAssessor.BackColor = System.Drawing.Color.Transparent
        Me.radInternalAssessor.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.radInternalAssessor.Location = New System.Drawing.Point(635, 3)
        Me.radInternalAssessor.Name = "radInternalAssessor"
        Me.radInternalAssessor.Size = New System.Drawing.Size(127, 17)
        Me.radInternalAssessor.TabIndex = 379
        Me.radInternalAssessor.TabStop = True
        Me.radInternalAssessor.Text = "Internal Assessor"
        Me.radInternalAssessor.UseVisualStyleBackColor = False
        '
        'lnkViewAssessments
        '
        Me.lnkViewAssessments.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lnkViewAssessments.LinkBehavior = System.Windows.Forms.LinkBehavior.NeverUnderline
        Me.lnkViewAssessments.Location = New System.Drawing.Point(8, 161)
        Me.lnkViewAssessments.Name = "lnkViewAssessments"
        Me.lnkViewAssessments.Size = New System.Drawing.Size(143, 17)
        Me.lnkViewAssessments.TabIndex = 20
        Me.lnkViewAssessments.TabStop = True
        Me.lnkViewAssessments.Text = "View Assessement"
        Me.lnkViewAssessments.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'pnlEvaluationList
        '
        Me.pnlEvaluationList.Controls.Add(Me.lvEvaluation)
        Me.pnlEvaluationList.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.pnlEvaluationList.Location = New System.Drawing.Point(8, 191)
        Me.pnlEvaluationList.Name = "pnlEvaluationList"
        Me.pnlEvaluationList.Size = New System.Drawing.Size(882, 241)
        Me.pnlEvaluationList.TabIndex = 337
        '
        'lvEvaluation
        '
        Me.lvEvaluation.BackColorOnChecked = True
        Me.lvEvaluation.ColumnHeaders = Nothing
        Me.lvEvaluation.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.objdgcolhAssessItem, Me.colhAssessSubItem, Me.colhResult, Me.colhRemark, Me.objcolhAssessItemId, Me.objcolhSubItemId, Me.objcolhResultId, Me.objcolhGUID, Me.objcolhSortId})
        Me.lvEvaluation.CompulsoryColumns = ""
        Me.lvEvaluation.Dock = System.Windows.Forms.DockStyle.Fill
        Me.lvEvaluation.FullRowSelect = True
        Me.lvEvaluation.GridLines = True
        Me.lvEvaluation.GroupingColumn = Nothing
        Me.lvEvaluation.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable
        Me.lvEvaluation.HideSelection = False
        Me.lvEvaluation.Location = New System.Drawing.Point(0, 0)
        Me.lvEvaluation.MinColumnWidth = 50
        Me.lvEvaluation.MultiSelect = False
        Me.lvEvaluation.Name = "lvEvaluation"
        Me.lvEvaluation.OptionalColumns = ""
        Me.lvEvaluation.ShowMoreItem = False
        Me.lvEvaluation.ShowSaveItem = False
        Me.lvEvaluation.ShowSelectAll = True
        Me.lvEvaluation.ShowSizeAllColumnsToFit = True
        Me.lvEvaluation.Size = New System.Drawing.Size(882, 241)
        Me.lvEvaluation.Sortable = True
        Me.lvEvaluation.TabIndex = 0
        Me.lvEvaluation.UseCompatibleStateImageBehavior = False
        Me.lvEvaluation.View = System.Windows.Forms.View.Details
        '
        'objdgcolhAssessItem
        '
        Me.objdgcolhAssessItem.Tag = "objdgcolhAssessItem"
        Me.objdgcolhAssessItem.Text = ""
        Me.objdgcolhAssessItem.Width = 0
        '
        'colhAssessSubItem
        '
        Me.colhAssessSubItem.Tag = "colhAssessSubItem"
        Me.colhAssessSubItem.Text = "Assessment Items"
        Me.colhAssessSubItem.Width = 528
        '
        'colhResult
        '
        Me.colhResult.Tag = "colhResult"
        Me.colhResult.Text = ""
        Me.colhResult.Width = 150
        '
        'colhRemark
        '
        Me.colhRemark.Tag = "colhRemark"
        Me.colhRemark.Text = "Remark"
        Me.colhRemark.Width = 200
        '
        'objcolhAssessItemId
        '
        Me.objcolhAssessItemId.Tag = "objcolhAssessItemId"
        Me.objcolhAssessItemId.Text = ""
        Me.objcolhAssessItemId.Width = 0
        '
        'objcolhSubItemId
        '
        Me.objcolhSubItemId.Tag = "objcolhSubItemId"
        Me.objcolhSubItemId.Text = ""
        Me.objcolhSubItemId.Width = 0
        '
        'objcolhResultId
        '
        Me.objcolhResultId.Tag = "objcolhResultId"
        Me.objcolhResultId.Text = ""
        Me.objcolhResultId.Width = 0
        '
        'objcolhGUID
        '
        Me.objcolhGUID.Tag = "objcolhGUID"
        Me.objcolhGUID.Text = ""
        Me.objcolhGUID.Width = 0
        '
        'objcolhSortId
        '
        Me.objcolhSortId.Tag = "objcolhSortId"
        Me.objcolhSortId.Text = ""
        Me.objcolhSortId.Width = 0
        '
        'btnDeleteEvaluation
        '
        Me.btnDeleteEvaluation.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnDeleteEvaluation.BackColor = System.Drawing.Color.White
        Me.btnDeleteEvaluation.BackgroundImage = CType(resources.GetObject("btnDeleteEvaluation.BackgroundImage"), System.Drawing.Image)
        Me.btnDeleteEvaluation.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnDeleteEvaluation.BorderColor = System.Drawing.Color.Empty
        Me.btnDeleteEvaluation.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnDeleteEvaluation.FlatAppearance.BorderSize = 0
        Me.btnDeleteEvaluation.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnDeleteEvaluation.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnDeleteEvaluation.ForeColor = System.Drawing.Color.Black
        Me.btnDeleteEvaluation.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnDeleteEvaluation.GradientForeColor = System.Drawing.Color.Black
        Me.btnDeleteEvaluation.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnDeleteEvaluation.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnDeleteEvaluation.Location = New System.Drawing.Point(802, 154)
        Me.btnDeleteEvaluation.Name = "btnDeleteEvaluation"
        Me.btnDeleteEvaluation.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnDeleteEvaluation.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnDeleteEvaluation.Size = New System.Drawing.Size(88, 30)
        Me.btnDeleteEvaluation.TabIndex = 23
        Me.btnDeleteEvaluation.Text = "&Delete"
        Me.btnDeleteEvaluation.UseVisualStyleBackColor = True
        '
        'objLine3
        '
        Me.objLine3.BackColor = System.Drawing.Color.Transparent
        Me.objLine3.ForeColor = System.Drawing.SystemColors.AppWorkspace
        Me.objLine3.LineType = eZee.Common.StraightLineTypes.Horizontal
        Me.objLine3.Location = New System.Drawing.Point(11, 142)
        Me.objLine3.Name = "objLine3"
        Me.objLine3.Size = New System.Drawing.Size(879, 9)
        Me.objLine3.TabIndex = 331
        Me.objLine3.Text = "EZeeStraightLine2"
        '
        'btnEditEvaluation
        '
        Me.btnEditEvaluation.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnEditEvaluation.BackColor = System.Drawing.Color.White
        Me.btnEditEvaluation.BackgroundImage = CType(resources.GetObject("btnEditEvaluation.BackgroundImage"), System.Drawing.Image)
        Me.btnEditEvaluation.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnEditEvaluation.BorderColor = System.Drawing.Color.Empty
        Me.btnEditEvaluation.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnEditEvaluation.FlatAppearance.BorderSize = 0
        Me.btnEditEvaluation.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnEditEvaluation.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnEditEvaluation.ForeColor = System.Drawing.Color.Black
        Me.btnEditEvaluation.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnEditEvaluation.GradientForeColor = System.Drawing.Color.Black
        Me.btnEditEvaluation.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnEditEvaluation.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnEditEvaluation.Location = New System.Drawing.Point(708, 154)
        Me.btnEditEvaluation.Name = "btnEditEvaluation"
        Me.btnEditEvaluation.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnEditEvaluation.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnEditEvaluation.Size = New System.Drawing.Size(88, 30)
        Me.btnEditEvaluation.TabIndex = 22
        Me.btnEditEvaluation.Text = "&Edit"
        Me.btnEditEvaluation.UseVisualStyleBackColor = True
        '
        'btnAddEvaluation
        '
        Me.btnAddEvaluation.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnAddEvaluation.BackColor = System.Drawing.Color.White
        Me.btnAddEvaluation.BackgroundImage = CType(resources.GetObject("btnAddEvaluation.BackgroundImage"), System.Drawing.Image)
        Me.btnAddEvaluation.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnAddEvaluation.BorderColor = System.Drawing.Color.Empty
        Me.btnAddEvaluation.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnAddEvaluation.FlatAppearance.BorderSize = 0
        Me.btnAddEvaluation.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnAddEvaluation.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnAddEvaluation.ForeColor = System.Drawing.Color.Black
        Me.btnAddEvaluation.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnAddEvaluation.GradientForeColor = System.Drawing.Color.Black
        Me.btnAddEvaluation.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnAddEvaluation.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnAddEvaluation.Location = New System.Drawing.Point(614, 154)
        Me.btnAddEvaluation.Name = "btnAddEvaluation"
        Me.btnAddEvaluation.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnAddEvaluation.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnAddEvaluation.Size = New System.Drawing.Size(88, 30)
        Me.btnAddEvaluation.TabIndex = 21
        Me.btnAddEvaluation.Text = "&Add"
        Me.btnAddEvaluation.UseVisualStyleBackColor = True
        '
        'objLine2
        '
        Me.objLine2.BackColor = System.Drawing.Color.Transparent
        Me.objLine2.ForeColor = System.Drawing.SystemColors.AppWorkspace
        Me.objLine2.LineType = eZee.Common.StraightLineTypes.Vertical
        Me.objLine2.Location = New System.Drawing.Point(674, 33)
        Me.objLine2.Name = "objLine2"
        Me.objLine2.Size = New System.Drawing.Size(6, 102)
        Me.objLine2.TabIndex = 17
        '
        'objLine1
        '
        Me.objLine1.BackColor = System.Drawing.Color.Transparent
        Me.objLine1.ForeColor = System.Drawing.SystemColors.AppWorkspace
        Me.objLine1.LineType = eZee.Common.StraightLineTypes.Vertical
        Me.objLine1.Location = New System.Drawing.Point(354, 33)
        Me.objLine1.Name = "objLine1"
        Me.objLine1.Size = New System.Drawing.Size(6, 102)
        Me.objLine1.TabIndex = 4
        '
        'lblRemark
        '
        Me.lblRemark.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblRemark.Location = New System.Drawing.Point(686, 37)
        Me.lblRemark.Name = "lblRemark"
        Me.lblRemark.Size = New System.Drawing.Size(204, 15)
        Me.lblRemark.TabIndex = 18
        Me.lblRemark.Text = "Remark"
        '
        'txtRemark
        '
        Me.txtRemark.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtRemark.Location = New System.Drawing.Point(686, 60)
        Me.txtRemark.Name = "txtRemark"
        Me.txtRemark.ScrollBars = System.Windows.Forms.RichTextBoxScrollBars.ForcedVertical
        Me.txtRemark.Size = New System.Drawing.Size(204, 75)
        Me.txtRemark.TabIndex = 19
        Me.txtRemark.Text = ""
        '
        'objbtnSearchResult
        '
        Me.objbtnSearchResult.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchResult.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchResult.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchResult.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchResult.BorderSelected = False
        Me.objbtnSearchResult.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchResult.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objbtnSearchResult.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchResult.Location = New System.Drawing.Point(647, 114)
        Me.objbtnSearchResult.Name = "objbtnSearchResult"
        Me.objbtnSearchResult.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchResult.TabIndex = 16
        '
        'objbtnSearchGroup
        '
        Me.objbtnSearchGroup.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchGroup.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchGroup.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchGroup.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchGroup.BorderSelected = False
        Me.objbtnSearchGroup.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchGroup.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objbtnSearchGroup.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchGroup.Location = New System.Drawing.Point(647, 33)
        Me.objbtnSearchGroup.Name = "objbtnSearchGroup"
        Me.objbtnSearchGroup.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchGroup.TabIndex = 7
        '
        'cboAssessGroup
        '
        Me.cboAssessGroup.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboAssessGroup.DropDownWidth = 550
        Me.cboAssessGroup.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboAssessGroup.FormattingEnabled = True
        Me.cboAssessGroup.Location = New System.Drawing.Point(439, 33)
        Me.cboAssessGroup.Name = "cboAssessGroup"
        Me.cboAssessGroup.Size = New System.Drawing.Size(202, 21)
        Me.cboAssessGroup.TabIndex = 6
        '
        'cboResultCode
        '
        Me.cboResultCode.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboResultCode.DropDownWidth = 550
        Me.cboResultCode.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboResultCode.FormattingEnabled = True
        Me.cboResultCode.Location = New System.Drawing.Point(439, 114)
        Me.cboResultCode.Name = "cboResultCode"
        Me.cboResultCode.Size = New System.Drawing.Size(202, 21)
        Me.cboResultCode.TabIndex = 15
        '
        'objbtnSearchSubItems
        '
        Me.objbtnSearchSubItems.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchSubItems.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchSubItems.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchSubItems.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchSubItems.BorderSelected = False
        Me.objbtnSearchSubItems.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchSubItems.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objbtnSearchSubItems.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchSubItems.Location = New System.Drawing.Point(647, 87)
        Me.objbtnSearchSubItems.Name = "objbtnSearchSubItems"
        Me.objbtnSearchSubItems.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchSubItems.TabIndex = 13
        '
        'lblResult
        '
        Me.lblResult.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblResult.Location = New System.Drawing.Point(366, 117)
        Me.lblResult.Name = "lblResult"
        Me.lblResult.Size = New System.Drawing.Size(67, 15)
        Me.lblResult.TabIndex = 14
        Me.lblResult.Text = "Result"
        '
        'lblAssessGroup
        '
        Me.lblAssessGroup.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblAssessGroup.Location = New System.Drawing.Point(366, 36)
        Me.lblAssessGroup.Name = "lblAssessGroup"
        Me.lblAssessGroup.Size = New System.Drawing.Size(67, 15)
        Me.lblAssessGroup.TabIndex = 5
        Me.lblAssessGroup.Text = "Group"
        '
        'lblAssessmentItems
        '
        Me.lblAssessmentItems.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblAssessmentItems.Location = New System.Drawing.Point(366, 63)
        Me.lblAssessmentItems.Name = "lblAssessmentItems"
        Me.lblAssessmentItems.Size = New System.Drawing.Size(67, 15)
        Me.lblAssessmentItems.TabIndex = 8
        Me.lblAssessmentItems.Text = "Items"
        '
        'cboAssessItem
        '
        Me.cboAssessItem.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboAssessItem.DropDownWidth = 550
        Me.cboAssessItem.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboAssessItem.FormattingEnabled = True
        Me.cboAssessItem.Location = New System.Drawing.Point(439, 60)
        Me.cboAssessItem.Name = "cboAssessItem"
        Me.cboAssessItem.Size = New System.Drawing.Size(202, 21)
        Me.cboAssessItem.TabIndex = 9
        '
        'lblSubItems
        '
        Me.lblSubItems.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblSubItems.Location = New System.Drawing.Point(366, 90)
        Me.lblSubItems.Name = "lblSubItems"
        Me.lblSubItems.Size = New System.Drawing.Size(67, 15)
        Me.lblSubItems.TabIndex = 11
        Me.lblSubItems.Text = "Sub Items"
        '
        'cboSubItems
        '
        Me.cboSubItems.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboSubItems.DropDownWidth = 550
        Me.cboSubItems.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboSubItems.FormattingEnabled = True
        Me.cboSubItems.Location = New System.Drawing.Point(439, 87)
        Me.cboSubItems.Name = "cboSubItems"
        Me.cboSubItems.Size = New System.Drawing.Size(202, 21)
        Me.cboSubItems.TabIndex = 12
        '
        'objbtnSearchItems
        '
        Me.objbtnSearchItems.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchItems.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchItems.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchItems.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchItems.BorderSelected = False
        Me.objbtnSearchItems.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchItems.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objbtnSearchItems.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchItems.Location = New System.Drawing.Point(647, 60)
        Me.objbtnSearchItems.Name = "objbtnSearchItems"
        Me.objbtnSearchItems.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchItems.TabIndex = 10
        '
        'lblReviewer
        '
        Me.lblReviewer.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblReviewer.Location = New System.Drawing.Point(8, 36)
        Me.lblReviewer.Name = "lblReviewer"
        Me.lblReviewer.Size = New System.Drawing.Size(66, 15)
        Me.lblReviewer.TabIndex = 341
        Me.lblReviewer.Text = "Reviewer"
        Me.lblReviewer.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'tabpImporvementArea
        '
        Me.tabpImporvementArea.Controls.Add(Me.gbImprovement)
        Me.tabpImporvementArea.Location = New System.Drawing.Point(4, 22)
        Me.tabpImporvementArea.Name = "tabpImporvementArea"
        Me.tabpImporvementArea.Size = New System.Drawing.Size(898, 435)
        Me.tabpImporvementArea.TabIndex = 1
        Me.tabpImporvementArea.Text = "Improvement"
        Me.tabpImporvementArea.UseVisualStyleBackColor = True
        '
        'gbImprovement
        '
        Me.gbImprovement.BorderColor = System.Drawing.Color.Black
        Me.gbImprovement.Checked = False
        Me.gbImprovement.CollapseAllExceptThis = False
        Me.gbImprovement.CollapsedHoverImage = Nothing
        Me.gbImprovement.CollapsedNormalImage = Nothing
        Me.gbImprovement.CollapsedPressedImage = Nothing
        Me.gbImprovement.CollapseOnLoad = False
        Me.gbImprovement.Controls.Add(Me.pnlImproveList)
        Me.gbImprovement.Controls.Add(Me.objStLine5)
        Me.gbImprovement.Controls.Add(Me.btnDeleteImprovement)
        Me.gbImprovement.Controls.Add(Me.btnEditImprovement)
        Me.gbImprovement.Controls.Add(Me.btnAddImprovement)
        Me.gbImprovement.Controls.Add(Me.objStLine4)
        Me.gbImprovement.Controls.Add(Me.objbtnSearchITraning)
        Me.gbImprovement.Controls.Add(Me.lblILearningObjective)
        Me.gbImprovement.Controls.Add(Me.lblITime)
        Me.gbImprovement.Controls.Add(Me.tabcImprovement)
        Me.gbImprovement.Controls.Add(Me.cboITraining)
        Me.gbImprovement.Controls.Add(Me.dtpITimeframe)
        Me.gbImprovement.Dock = System.Windows.Forms.DockStyle.Fill
        Me.gbImprovement.ExpandedHoverImage = Nothing
        Me.gbImprovement.ExpandedNormalImage = Nothing
        Me.gbImprovement.ExpandedPressedImage = Nothing
        Me.gbImprovement.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbImprovement.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbImprovement.HeaderHeight = 25
        Me.gbImprovement.HeaderMessage = ""
        Me.gbImprovement.HeaderMessageFont = New System.Drawing.Font("Microsoft Sans Serif", 8.25!)
        Me.gbImprovement.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbImprovement.HeightOnCollapse = 0
        Me.gbImprovement.LeftTextSpace = 0
        Me.gbImprovement.Location = New System.Drawing.Point(0, 0)
        Me.gbImprovement.Name = "gbImprovement"
        Me.gbImprovement.OpenHeight = 300
        Me.gbImprovement.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbImprovement.ShowBorder = True
        Me.gbImprovement.ShowCheckBox = False
        Me.gbImprovement.ShowCollapseButton = False
        Me.gbImprovement.ShowDefaultBorderColor = True
        Me.gbImprovement.ShowDownButton = False
        Me.gbImprovement.ShowHeader = True
        Me.gbImprovement.Size = New System.Drawing.Size(898, 435)
        Me.gbImprovement.TabIndex = 0
        Me.gbImprovement.Temp = 0
        Me.gbImprovement.Text = "Areas of Improvement"
        Me.gbImprovement.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'pnlImproveList
        '
        Me.pnlImproveList.Controls.Add(Me.lvImprovement)
        Me.pnlImproveList.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.pnlImproveList.Location = New System.Drawing.Point(7, 174)
        Me.pnlImproveList.Name = "pnlImproveList"
        Me.pnlImproveList.Size = New System.Drawing.Size(883, 258)
        Me.pnlImproveList.TabIndex = 340
        '
        'lvImprovement
        '
        Me.lvImprovement.BackColorOnChecked = False
        Me.lvImprovement.ColumnHeaders = Nothing
        Me.lvImprovement.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.colhI_Improvement, Me.colhI_ActivityReq, Me.colhI_Support, Me.colhI_Course, Me.colhI_Timeframe, Me.colhI_OtherTraning, Me.objcolhI_CourseId, Me.objcolhI_GUID})
        Me.lvImprovement.CompulsoryColumns = ""
        Me.lvImprovement.Dock = System.Windows.Forms.DockStyle.Fill
        Me.lvImprovement.FullRowSelect = True
        Me.lvImprovement.GridLines = True
        Me.lvImprovement.GroupingColumn = Nothing
        Me.lvImprovement.HideSelection = False
        Me.lvImprovement.Location = New System.Drawing.Point(0, 0)
        Me.lvImprovement.MinColumnWidth = 50
        Me.lvImprovement.MultiSelect = False
        Me.lvImprovement.Name = "lvImprovement"
        Me.lvImprovement.OptionalColumns = ""
        Me.lvImprovement.ShowMoreItem = False
        Me.lvImprovement.ShowSaveItem = False
        Me.lvImprovement.ShowSelectAll = True
        Me.lvImprovement.ShowSizeAllColumnsToFit = True
        Me.lvImprovement.Size = New System.Drawing.Size(883, 258)
        Me.lvImprovement.Sortable = True
        Me.lvImprovement.TabIndex = 0
        Me.lvImprovement.UseCompatibleStateImageBehavior = False
        Me.lvImprovement.View = System.Windows.Forms.View.Details
        '
        'colhI_Improvement
        '
        Me.colhI_Improvement.Tag = "colhI_Improvement"
        Me.colhI_Improvement.Text = "Major Area for Improvement"
        Me.colhI_Improvement.Width = 160
        '
        'colhI_ActivityReq
        '
        Me.colhI_ActivityReq.Tag = "colhI_ActivityReq"
        Me.colhI_ActivityReq.Text = "Activity/Action Required to Meet Need"
        Me.colhI_ActivityReq.Width = 200
        '
        'colhI_Support
        '
        Me.colhI_Support.Tag = "colhI_Support"
        Me.colhI_Support.Text = "Support required from Assessor"
        Me.colhI_Support.Width = 180
        '
        'colhI_Course
        '
        Me.colhI_Course.Tag = "colhI_Course"
        Me.colhI_Course.Text = "Training or Learning Objective"
        Me.colhI_Course.Width = 160
        '
        'colhI_Timeframe
        '
        Me.colhI_Timeframe.Tag = "colhI_Timeframe"
        Me.colhI_Timeframe.Text = "Timeframe"
        Me.colhI_Timeframe.Width = 90
        '
        'colhI_OtherTraning
        '
        Me.colhI_OtherTraning.Tag = "colhI_OtherTraning"
        Me.colhI_OtherTraning.Text = "Other Training"
        Me.colhI_OtherTraning.Width = 160
        '
        'objcolhI_CourseId
        '
        Me.objcolhI_CourseId.Tag = "objcolhI_CourseId"
        Me.objcolhI_CourseId.Text = ""
        Me.objcolhI_CourseId.Width = 0
        '
        'objcolhI_GUID
        '
        Me.objcolhI_GUID.Tag = "objcolhI_GUID"
        Me.objcolhI_GUID.Text = ""
        Me.objcolhI_GUID.Width = 0
        '
        'objStLine5
        '
        Me.objStLine5.BackColor = System.Drawing.Color.Transparent
        Me.objStLine5.ForeColor = System.Drawing.SystemColors.AppWorkspace
        Me.objStLine5.LineType = eZee.Common.StraightLineTypes.Horizontal
        Me.objStLine5.Location = New System.Drawing.Point(7, 159)
        Me.objStLine5.Name = "objStLine5"
        Me.objStLine5.Size = New System.Drawing.Size(883, 9)
        Me.objStLine5.TabIndex = 10
        Me.objStLine5.Text = "EZeeStraightLine2"
        '
        'btnDeleteImprovement
        '
        Me.btnDeleteImprovement.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnDeleteImprovement.BackColor = System.Drawing.Color.White
        Me.btnDeleteImprovement.BackgroundImage = CType(resources.GetObject("btnDeleteImprovement.BackgroundImage"), System.Drawing.Image)
        Me.btnDeleteImprovement.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnDeleteImprovement.BorderColor = System.Drawing.Color.Empty
        Me.btnDeleteImprovement.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnDeleteImprovement.FlatAppearance.BorderSize = 0
        Me.btnDeleteImprovement.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnDeleteImprovement.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnDeleteImprovement.ForeColor = System.Drawing.Color.Black
        Me.btnDeleteImprovement.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnDeleteImprovement.GradientForeColor = System.Drawing.Color.Black
        Me.btnDeleteImprovement.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnDeleteImprovement.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnDeleteImprovement.Location = New System.Drawing.Point(677, 120)
        Me.btnDeleteImprovement.Name = "btnDeleteImprovement"
        Me.btnDeleteImprovement.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnDeleteImprovement.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnDeleteImprovement.Size = New System.Drawing.Size(88, 30)
        Me.btnDeleteImprovement.TabIndex = 9
        Me.btnDeleteImprovement.Text = "&Delete"
        Me.btnDeleteImprovement.UseVisualStyleBackColor = True
        '
        'btnEditImprovement
        '
        Me.btnEditImprovement.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnEditImprovement.BackColor = System.Drawing.Color.White
        Me.btnEditImprovement.BackgroundImage = CType(resources.GetObject("btnEditImprovement.BackgroundImage"), System.Drawing.Image)
        Me.btnEditImprovement.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnEditImprovement.BorderColor = System.Drawing.Color.Empty
        Me.btnEditImprovement.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnEditImprovement.FlatAppearance.BorderSize = 0
        Me.btnEditImprovement.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnEditImprovement.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnEditImprovement.ForeColor = System.Drawing.Color.Black
        Me.btnEditImprovement.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnEditImprovement.GradientForeColor = System.Drawing.Color.Black
        Me.btnEditImprovement.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnEditImprovement.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnEditImprovement.Location = New System.Drawing.Point(583, 120)
        Me.btnEditImprovement.Name = "btnEditImprovement"
        Me.btnEditImprovement.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnEditImprovement.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnEditImprovement.Size = New System.Drawing.Size(88, 30)
        Me.btnEditImprovement.TabIndex = 8
        Me.btnEditImprovement.Text = "&Edit"
        Me.btnEditImprovement.UseVisualStyleBackColor = True
        '
        'btnAddImprovement
        '
        Me.btnAddImprovement.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnAddImprovement.BackColor = System.Drawing.Color.White
        Me.btnAddImprovement.BackgroundImage = CType(resources.GetObject("btnAddImprovement.BackgroundImage"), System.Drawing.Image)
        Me.btnAddImprovement.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnAddImprovement.BorderColor = System.Drawing.Color.Empty
        Me.btnAddImprovement.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnAddImprovement.FlatAppearance.BorderSize = 0
        Me.btnAddImprovement.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnAddImprovement.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnAddImprovement.ForeColor = System.Drawing.Color.Black
        Me.btnAddImprovement.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnAddImprovement.GradientForeColor = System.Drawing.Color.Black
        Me.btnAddImprovement.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnAddImprovement.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnAddImprovement.Location = New System.Drawing.Point(489, 120)
        Me.btnAddImprovement.Name = "btnAddImprovement"
        Me.btnAddImprovement.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnAddImprovement.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnAddImprovement.Size = New System.Drawing.Size(88, 30)
        Me.btnAddImprovement.TabIndex = 7
        Me.btnAddImprovement.Text = "&Add"
        Me.btnAddImprovement.UseVisualStyleBackColor = True
        '
        'objStLine4
        '
        Me.objStLine4.BackColor = System.Drawing.Color.Transparent
        Me.objStLine4.ForeColor = System.Drawing.SystemColors.AppWorkspace
        Me.objStLine4.LineType = eZee.Common.StraightLineTypes.Vertical
        Me.objStLine4.Location = New System.Drawing.Point(471, 28)
        Me.objStLine4.Name = "objStLine4"
        Me.objStLine4.Size = New System.Drawing.Size(9, 126)
        Me.objStLine4.TabIndex = 2
        '
        'objbtnSearchITraning
        '
        Me.objbtnSearchITraning.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchITraning.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchITraning.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchITraning.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchITraning.BorderSelected = False
        Me.objbtnSearchITraning.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchITraning.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objbtnSearchITraning.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchITraning.Location = New System.Drawing.Point(869, 75)
        Me.objbtnSearchITraning.Name = "objbtnSearchITraning"
        Me.objbtnSearchITraning.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchITraning.TabIndex = 321
        '
        'lblILearningObjective
        '
        Me.lblILearningObjective.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblILearningObjective.Location = New System.Drawing.Point(486, 77)
        Me.lblILearningObjective.Name = "lblILearningObjective"
        Me.lblILearningObjective.Size = New System.Drawing.Size(161, 17)
        Me.lblILearningObjective.TabIndex = 5
        Me.lblILearningObjective.Text = "Training or Learning Objective"
        '
        'lblITime
        '
        Me.lblITime.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblITime.Location = New System.Drawing.Point(486, 50)
        Me.lblITime.Name = "lblITime"
        Me.lblITime.Size = New System.Drawing.Size(161, 17)
        Me.lblITime.TabIndex = 3
        Me.lblITime.Text = "Timeframe for Completion"
        '
        'tabcImprovement
        '
        Me.tabcImprovement.Controls.Add(Me.tabpImprovement)
        Me.tabcImprovement.Controls.Add(Me.tapbActionRequired)
        Me.tabcImprovement.Controls.Add(Me.tabpSupportRequired)
        Me.tabcImprovement.Controls.Add(Me.tabpOtherTraining)
        Me.tabcImprovement.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tabcImprovement.Location = New System.Drawing.Point(7, 28)
        Me.tabcImprovement.Name = "tabcImprovement"
        Me.tabcImprovement.SelectedIndex = 0
        Me.tabcImprovement.Size = New System.Drawing.Size(461, 126)
        Me.tabcImprovement.TabIndex = 1
        '
        'tabpImprovement
        '
        Me.tabpImprovement.Controls.Add(Me.txtI_Improvement)
        Me.tabpImprovement.Location = New System.Drawing.Point(4, 22)
        Me.tabpImprovement.Name = "tabpImprovement"
        Me.tabpImprovement.Size = New System.Drawing.Size(453, 100)
        Me.tabpImprovement.TabIndex = 0
        Me.tabpImprovement.Text = "Improvement"
        Me.tabpImprovement.UseVisualStyleBackColor = True
        '
        'txtI_Improvement
        '
        Me.txtI_Improvement.BackColor = System.Drawing.SystemColors.Window
        Me.txtI_Improvement.Dock = System.Windows.Forms.DockStyle.Fill
        Me.txtI_Improvement.Flags = 0
        Me.txtI_Improvement.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtI_Improvement.Location = New System.Drawing.Point(0, 0)
        Me.txtI_Improvement.Multiline = True
        Me.txtI_Improvement.Name = "txtI_Improvement"
        Me.txtI_Improvement.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.txtI_Improvement.Size = New System.Drawing.Size(453, 100)
        Me.txtI_Improvement.TabIndex = 0
        '
        'tapbActionRequired
        '
        Me.tapbActionRequired.Controls.Add(Me.txtI_Action)
        Me.tapbActionRequired.Location = New System.Drawing.Point(4, 22)
        Me.tapbActionRequired.Name = "tapbActionRequired"
        Me.tapbActionRequired.Size = New System.Drawing.Size(453, 100)
        Me.tapbActionRequired.TabIndex = 1
        Me.tapbActionRequired.Text = "Activity/Action Required"
        Me.tapbActionRequired.UseVisualStyleBackColor = True
        '
        'txtI_Action
        '
        Me.txtI_Action.BackColor = System.Drawing.SystemColors.Window
        Me.txtI_Action.Dock = System.Windows.Forms.DockStyle.Fill
        Me.txtI_Action.Flags = 0
        Me.txtI_Action.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtI_Action.Location = New System.Drawing.Point(0, 0)
        Me.txtI_Action.Multiline = True
        Me.txtI_Action.Name = "txtI_Action"
        Me.txtI_Action.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.txtI_Action.Size = New System.Drawing.Size(453, 100)
        Me.txtI_Action.TabIndex = 1
        '
        'tabpSupportRequired
        '
        Me.tabpSupportRequired.Controls.Add(Me.txtI_Support)
        Me.tabpSupportRequired.Location = New System.Drawing.Point(4, 22)
        Me.tabpSupportRequired.Name = "tabpSupportRequired"
        Me.tabpSupportRequired.Size = New System.Drawing.Size(453, 100)
        Me.tabpSupportRequired.TabIndex = 2
        Me.tabpSupportRequired.Text = "Support Required"
        Me.tabpSupportRequired.UseVisualStyleBackColor = True
        '
        'txtI_Support
        '
        Me.txtI_Support.BackColor = System.Drawing.SystemColors.Window
        Me.txtI_Support.Dock = System.Windows.Forms.DockStyle.Fill
        Me.txtI_Support.Flags = 0
        Me.txtI_Support.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtI_Support.Location = New System.Drawing.Point(0, 0)
        Me.txtI_Support.Multiline = True
        Me.txtI_Support.Name = "txtI_Support"
        Me.txtI_Support.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.txtI_Support.Size = New System.Drawing.Size(453, 100)
        Me.txtI_Support.TabIndex = 1
        '
        'tabpOtherTraining
        '
        Me.tabpOtherTraining.Controls.Add(Me.txtI_OtherTraining)
        Me.tabpOtherTraining.Location = New System.Drawing.Point(4, 22)
        Me.tabpOtherTraining.Name = "tabpOtherTraining"
        Me.tabpOtherTraining.Size = New System.Drawing.Size(453, 100)
        Me.tabpOtherTraining.TabIndex = 3
        Me.tabpOtherTraining.Text = "Other Training"
        Me.tabpOtherTraining.UseVisualStyleBackColor = True
        '
        'txtI_OtherTraining
        '
        Me.txtI_OtherTraining.BackColor = System.Drawing.SystemColors.Window
        Me.txtI_OtherTraining.Dock = System.Windows.Forms.DockStyle.Fill
        Me.txtI_OtherTraining.Flags = 0
        Me.txtI_OtherTraining.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtI_OtherTraining.Location = New System.Drawing.Point(0, 0)
        Me.txtI_OtherTraining.Multiline = True
        Me.txtI_OtherTraining.Name = "txtI_OtherTraining"
        Me.txtI_OtherTraining.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.txtI_OtherTraining.Size = New System.Drawing.Size(453, 100)
        Me.txtI_OtherTraining.TabIndex = 1
        '
        'cboITraining
        '
        Me.cboITraining.DisplayMember = "masterunkid"
        Me.cboITraining.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboITraining.DropDownWidth = 300
        Me.cboITraining.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboITraining.FormattingEnabled = True
        Me.cboITraining.Location = New System.Drawing.Point(653, 75)
        Me.cboITraining.Name = "cboITraining"
        Me.cboITraining.Size = New System.Drawing.Size(210, 21)
        Me.cboITraining.TabIndex = 6
        Me.cboITraining.ValueMember = "masterunkid"
        '
        'dtpITimeframe
        '
        Me.dtpITimeframe.CalendarFont = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpITimeframe.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpITimeframe.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpITimeframe.Location = New System.Drawing.Point(653, 48)
        Me.dtpITimeframe.Name = "dtpITimeframe"
        Me.dtpITimeframe.Size = New System.Drawing.Size(90, 21)
        Me.dtpITimeframe.TabIndex = 4
        '
        'tabpPersonalDevelopment
        '
        Me.tabpPersonalDevelopment.Controls.Add(Me.gbPersonalDevelopment)
        Me.tabpPersonalDevelopment.Location = New System.Drawing.Point(4, 22)
        Me.tabpPersonalDevelopment.Name = "tabpPersonalDevelopment"
        Me.tabpPersonalDevelopment.Size = New System.Drawing.Size(898, 435)
        Me.tabpPersonalDevelopment.TabIndex = 3
        Me.tabpPersonalDevelopment.Text = "Personal Developement"
        Me.tabpPersonalDevelopment.UseVisualStyleBackColor = True
        '
        'gbPersonalDevelopment
        '
        Me.gbPersonalDevelopment.BorderColor = System.Drawing.Color.Black
        Me.gbPersonalDevelopment.Checked = False
        Me.gbPersonalDevelopment.CollapseAllExceptThis = False
        Me.gbPersonalDevelopment.CollapsedHoverImage = Nothing
        Me.gbPersonalDevelopment.CollapsedNormalImage = Nothing
        Me.gbPersonalDevelopment.CollapsedPressedImage = Nothing
        Me.gbPersonalDevelopment.CollapseOnLoad = False
        Me.gbPersonalDevelopment.Controls.Add(Me.pnlPersonalList)
        Me.gbPersonalDevelopment.Controls.Add(Me.objStLine7)
        Me.gbPersonalDevelopment.Controls.Add(Me.btnDeletePersonal)
        Me.gbPersonalDevelopment.Controls.Add(Me.btnEditPersonal)
        Me.gbPersonalDevelopment.Controls.Add(Me.btnAddPersonal)
        Me.gbPersonalDevelopment.Controls.Add(Me.objStLine6)
        Me.gbPersonalDevelopment.Controls.Add(Me.objbtnSearchPTraning)
        Me.gbPersonalDevelopment.Controls.Add(Me.lblPLearningObjective)
        Me.gbPersonalDevelopment.Controls.Add(Me.lblPTime)
        Me.gbPersonalDevelopment.Controls.Add(Me.tabcPersonal)
        Me.gbPersonalDevelopment.Controls.Add(Me.cboPTraining)
        Me.gbPersonalDevelopment.Controls.Add(Me.dtpPTimeframe)
        Me.gbPersonalDevelopment.Dock = System.Windows.Forms.DockStyle.Fill
        Me.gbPersonalDevelopment.ExpandedHoverImage = Nothing
        Me.gbPersonalDevelopment.ExpandedNormalImage = Nothing
        Me.gbPersonalDevelopment.ExpandedPressedImage = Nothing
        Me.gbPersonalDevelopment.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbPersonalDevelopment.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbPersonalDevelopment.HeaderHeight = 25
        Me.gbPersonalDevelopment.HeaderMessage = ""
        Me.gbPersonalDevelopment.HeaderMessageFont = New System.Drawing.Font("Microsoft Sans Serif", 8.25!)
        Me.gbPersonalDevelopment.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbPersonalDevelopment.HeightOnCollapse = 0
        Me.gbPersonalDevelopment.LeftTextSpace = 0
        Me.gbPersonalDevelopment.Location = New System.Drawing.Point(0, 0)
        Me.gbPersonalDevelopment.Name = "gbPersonalDevelopment"
        Me.gbPersonalDevelopment.OpenHeight = 300
        Me.gbPersonalDevelopment.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbPersonalDevelopment.ShowBorder = True
        Me.gbPersonalDevelopment.ShowCheckBox = False
        Me.gbPersonalDevelopment.ShowCollapseButton = False
        Me.gbPersonalDevelopment.ShowDefaultBorderColor = True
        Me.gbPersonalDevelopment.ShowDownButton = False
        Me.gbPersonalDevelopment.ShowHeader = True
        Me.gbPersonalDevelopment.Size = New System.Drawing.Size(898, 435)
        Me.gbPersonalDevelopment.TabIndex = 0
        Me.gbPersonalDevelopment.Temp = 0
        Me.gbPersonalDevelopment.Text = "Personal Development Plan"
        Me.gbPersonalDevelopment.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'pnlPersonalList
        '
        Me.pnlPersonalList.Controls.Add(Me.lvPersonalDevelop)
        Me.pnlPersonalList.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.pnlPersonalList.Location = New System.Drawing.Point(7, 174)
        Me.pnlPersonalList.Name = "pnlPersonalList"
        Me.pnlPersonalList.Size = New System.Drawing.Size(884, 257)
        Me.pnlPersonalList.TabIndex = 352
        '
        'lvPersonalDevelop
        '
        Me.lvPersonalDevelop.BackColorOnChecked = False
        Me.lvPersonalDevelop.ColumnHeaders = Nothing
        Me.lvPersonalDevelop.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.colhP_Development, Me.colhP_ActivityReq, Me.colhP_Support, Me.colhP_Course, Me.colhP_Timeframe, Me.colhP_OtherTraning, Me.objcolhP_CourseId, Me.objcolhP_GUID})
        Me.lvPersonalDevelop.CompulsoryColumns = ""
        Me.lvPersonalDevelop.Dock = System.Windows.Forms.DockStyle.Fill
        Me.lvPersonalDevelop.FullRowSelect = True
        Me.lvPersonalDevelop.GridLines = True
        Me.lvPersonalDevelop.GroupingColumn = Nothing
        Me.lvPersonalDevelop.HideSelection = False
        Me.lvPersonalDevelop.Location = New System.Drawing.Point(0, 0)
        Me.lvPersonalDevelop.MinColumnWidth = 50
        Me.lvPersonalDevelop.MultiSelect = False
        Me.lvPersonalDevelop.Name = "lvPersonalDevelop"
        Me.lvPersonalDevelop.OptionalColumns = ""
        Me.lvPersonalDevelop.ShowMoreItem = False
        Me.lvPersonalDevelop.ShowSaveItem = False
        Me.lvPersonalDevelop.ShowSelectAll = True
        Me.lvPersonalDevelop.ShowSizeAllColumnsToFit = True
        Me.lvPersonalDevelop.Size = New System.Drawing.Size(884, 257)
        Me.lvPersonalDevelop.Sortable = True
        Me.lvPersonalDevelop.TabIndex = 0
        Me.lvPersonalDevelop.UseCompatibleStateImageBehavior = False
        Me.lvPersonalDevelop.View = System.Windows.Forms.View.Details
        '
        'colhP_Development
        '
        Me.colhP_Development.Tag = "colhP_Development"
        Me.colhP_Development.Text = "Major Area for Development"
        Me.colhP_Development.Width = 160
        '
        'colhP_ActivityReq
        '
        Me.colhP_ActivityReq.Tag = "colhP_ActivityReq"
        Me.colhP_ActivityReq.Text = "Activity/Action Required to Meet Need"
        Me.colhP_ActivityReq.Width = 200
        '
        'colhP_Support
        '
        Me.colhP_Support.Tag = "colhP_Support"
        Me.colhP_Support.Text = "Support required from Assessor"
        Me.colhP_Support.Width = 180
        '
        'colhP_Course
        '
        Me.colhP_Course.Tag = "colhP_Course"
        Me.colhP_Course.Text = "Training or Learning Objective"
        Me.colhP_Course.Width = 160
        '
        'colhP_Timeframe
        '
        Me.colhP_Timeframe.Tag = "colhP_Timeframe"
        Me.colhP_Timeframe.Text = "Timeframe"
        Me.colhP_Timeframe.Width = 90
        '
        'colhP_OtherTraning
        '
        Me.colhP_OtherTraning.Tag = "colhP_OtherTraning"
        Me.colhP_OtherTraning.Text = "Other Training"
        Me.colhP_OtherTraning.Width = 160
        '
        'objcolhP_CourseId
        '
        Me.objcolhP_CourseId.Tag = "objcolhP_CourseId"
        Me.objcolhP_CourseId.Text = ""
        Me.objcolhP_CourseId.Width = 0
        '
        'objcolhP_GUID
        '
        Me.objcolhP_GUID.Tag = "objcolhP_GUID"
        Me.objcolhP_GUID.Text = ""
        Me.objcolhP_GUID.Width = 0
        '
        'objStLine7
        '
        Me.objStLine7.BackColor = System.Drawing.Color.Transparent
        Me.objStLine7.ForeColor = System.Drawing.SystemColors.AppWorkspace
        Me.objStLine7.LineType = eZee.Common.StraightLineTypes.Horizontal
        Me.objStLine7.Location = New System.Drawing.Point(7, 159)
        Me.objStLine7.Name = "objStLine7"
        Me.objStLine7.Size = New System.Drawing.Size(883, 9)
        Me.objStLine7.TabIndex = 10
        Me.objStLine7.Text = "EZeeStraightLine2"
        '
        'btnDeletePersonal
        '
        Me.btnDeletePersonal.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnDeletePersonal.BackColor = System.Drawing.Color.White
        Me.btnDeletePersonal.BackgroundImage = CType(resources.GetObject("btnDeletePersonal.BackgroundImage"), System.Drawing.Image)
        Me.btnDeletePersonal.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnDeletePersonal.BorderColor = System.Drawing.Color.Empty
        Me.btnDeletePersonal.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnDeletePersonal.FlatAppearance.BorderSize = 0
        Me.btnDeletePersonal.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnDeletePersonal.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnDeletePersonal.ForeColor = System.Drawing.Color.Black
        Me.btnDeletePersonal.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnDeletePersonal.GradientForeColor = System.Drawing.Color.Black
        Me.btnDeletePersonal.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnDeletePersonal.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnDeletePersonal.Location = New System.Drawing.Point(677, 120)
        Me.btnDeletePersonal.Name = "btnDeletePersonal"
        Me.btnDeletePersonal.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnDeletePersonal.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnDeletePersonal.Size = New System.Drawing.Size(88, 30)
        Me.btnDeletePersonal.TabIndex = 9
        Me.btnDeletePersonal.Text = "&Delete"
        Me.btnDeletePersonal.UseVisualStyleBackColor = True
        '
        'btnEditPersonal
        '
        Me.btnEditPersonal.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnEditPersonal.BackColor = System.Drawing.Color.White
        Me.btnEditPersonal.BackgroundImage = CType(resources.GetObject("btnEditPersonal.BackgroundImage"), System.Drawing.Image)
        Me.btnEditPersonal.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnEditPersonal.BorderColor = System.Drawing.Color.Empty
        Me.btnEditPersonal.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnEditPersonal.FlatAppearance.BorderSize = 0
        Me.btnEditPersonal.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnEditPersonal.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnEditPersonal.ForeColor = System.Drawing.Color.Black
        Me.btnEditPersonal.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnEditPersonal.GradientForeColor = System.Drawing.Color.Black
        Me.btnEditPersonal.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnEditPersonal.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnEditPersonal.Location = New System.Drawing.Point(583, 120)
        Me.btnEditPersonal.Name = "btnEditPersonal"
        Me.btnEditPersonal.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnEditPersonal.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnEditPersonal.Size = New System.Drawing.Size(88, 30)
        Me.btnEditPersonal.TabIndex = 8
        Me.btnEditPersonal.Text = "&Edit"
        Me.btnEditPersonal.UseVisualStyleBackColor = True
        '
        'btnAddPersonal
        '
        Me.btnAddPersonal.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnAddPersonal.BackColor = System.Drawing.Color.White
        Me.btnAddPersonal.BackgroundImage = CType(resources.GetObject("btnAddPersonal.BackgroundImage"), System.Drawing.Image)
        Me.btnAddPersonal.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnAddPersonal.BorderColor = System.Drawing.Color.Empty
        Me.btnAddPersonal.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnAddPersonal.FlatAppearance.BorderSize = 0
        Me.btnAddPersonal.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnAddPersonal.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnAddPersonal.ForeColor = System.Drawing.Color.Black
        Me.btnAddPersonal.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnAddPersonal.GradientForeColor = System.Drawing.Color.Black
        Me.btnAddPersonal.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnAddPersonal.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnAddPersonal.Location = New System.Drawing.Point(489, 120)
        Me.btnAddPersonal.Name = "btnAddPersonal"
        Me.btnAddPersonal.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnAddPersonal.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnAddPersonal.Size = New System.Drawing.Size(88, 30)
        Me.btnAddPersonal.TabIndex = 7
        Me.btnAddPersonal.Text = "&Add"
        Me.btnAddPersonal.UseVisualStyleBackColor = True
        '
        'objStLine6
        '
        Me.objStLine6.BackColor = System.Drawing.Color.Transparent
        Me.objStLine6.ForeColor = System.Drawing.SystemColors.AppWorkspace
        Me.objStLine6.LineType = eZee.Common.StraightLineTypes.Vertical
        Me.objStLine6.Location = New System.Drawing.Point(471, 28)
        Me.objStLine6.Name = "objStLine6"
        Me.objStLine6.Size = New System.Drawing.Size(9, 126)
        Me.objStLine6.TabIndex = 1
        '
        'objbtnSearchPTraning
        '
        Me.objbtnSearchPTraning.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchPTraning.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchPTraning.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchPTraning.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchPTraning.BorderSelected = False
        Me.objbtnSearchPTraning.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchPTraning.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objbtnSearchPTraning.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchPTraning.Location = New System.Drawing.Point(869, 75)
        Me.objbtnSearchPTraning.Name = "objbtnSearchPTraning"
        Me.objbtnSearchPTraning.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchPTraning.TabIndex = 6
        '
        'lblPLearningObjective
        '
        Me.lblPLearningObjective.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPLearningObjective.Location = New System.Drawing.Point(486, 77)
        Me.lblPLearningObjective.Name = "lblPLearningObjective"
        Me.lblPLearningObjective.Size = New System.Drawing.Size(161, 17)
        Me.lblPLearningObjective.TabIndex = 4
        Me.lblPLearningObjective.Text = "Training or Learning Objective"
        '
        'lblPTime
        '
        Me.lblPTime.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPTime.Location = New System.Drawing.Point(486, 50)
        Me.lblPTime.Name = "lblPTime"
        Me.lblPTime.Size = New System.Drawing.Size(161, 17)
        Me.lblPTime.TabIndex = 2
        Me.lblPTime.Text = "Timeframe for Completion"
        '
        'tabcPersonal
        '
        Me.tabcPersonal.Controls.Add(Me.tabpDevelopment)
        Me.tabcPersonal.Controls.Add(Me.tabpPAction)
        Me.tabcPersonal.Controls.Add(Me.tabpPSupport)
        Me.tabcPersonal.Controls.Add(Me.tabpPOtherTraining)
        Me.tabcPersonal.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tabcPersonal.Location = New System.Drawing.Point(7, 28)
        Me.tabcPersonal.Name = "tabcPersonal"
        Me.tabcPersonal.SelectedIndex = 0
        Me.tabcPersonal.Size = New System.Drawing.Size(461, 126)
        Me.tabcPersonal.TabIndex = 0
        '
        'tabpDevelopment
        '
        Me.tabpDevelopment.Controls.Add(Me.txtP_Development)
        Me.tabpDevelopment.Location = New System.Drawing.Point(4, 22)
        Me.tabpDevelopment.Name = "tabpDevelopment"
        Me.tabpDevelopment.Size = New System.Drawing.Size(453, 100)
        Me.tabpDevelopment.TabIndex = 0
        Me.tabpDevelopment.Text = "Development"
        Me.tabpDevelopment.UseVisualStyleBackColor = True
        '
        'txtP_Development
        '
        Me.txtP_Development.BackColor = System.Drawing.SystemColors.Window
        Me.txtP_Development.Dock = System.Windows.Forms.DockStyle.Fill
        Me.txtP_Development.Flags = 0
        Me.txtP_Development.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtP_Development.Location = New System.Drawing.Point(0, 0)
        Me.txtP_Development.Multiline = True
        Me.txtP_Development.Name = "txtP_Development"
        Me.txtP_Development.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.txtP_Development.Size = New System.Drawing.Size(453, 100)
        Me.txtP_Development.TabIndex = 0
        '
        'tabpPAction
        '
        Me.tabpPAction.Controls.Add(Me.txtP_Action)
        Me.tabpPAction.Location = New System.Drawing.Point(4, 22)
        Me.tabpPAction.Name = "tabpPAction"
        Me.tabpPAction.Size = New System.Drawing.Size(453, 100)
        Me.tabpPAction.TabIndex = 1
        Me.tabpPAction.Text = "Activity/Action Required"
        Me.tabpPAction.UseVisualStyleBackColor = True
        '
        'txtP_Action
        '
        Me.txtP_Action.BackColor = System.Drawing.SystemColors.Window
        Me.txtP_Action.Dock = System.Windows.Forms.DockStyle.Fill
        Me.txtP_Action.Flags = 0
        Me.txtP_Action.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtP_Action.Location = New System.Drawing.Point(0, 0)
        Me.txtP_Action.Multiline = True
        Me.txtP_Action.Name = "txtP_Action"
        Me.txtP_Action.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.txtP_Action.Size = New System.Drawing.Size(453, 100)
        Me.txtP_Action.TabIndex = 1
        '
        'tabpPSupport
        '
        Me.tabpPSupport.Controls.Add(Me.txtP_Support)
        Me.tabpPSupport.Location = New System.Drawing.Point(4, 22)
        Me.tabpPSupport.Name = "tabpPSupport"
        Me.tabpPSupport.Size = New System.Drawing.Size(453, 100)
        Me.tabpPSupport.TabIndex = 2
        Me.tabpPSupport.Text = "Support Required"
        Me.tabpPSupport.UseVisualStyleBackColor = True
        '
        'txtP_Support
        '
        Me.txtP_Support.BackColor = System.Drawing.SystemColors.Window
        Me.txtP_Support.Dock = System.Windows.Forms.DockStyle.Fill
        Me.txtP_Support.Flags = 0
        Me.txtP_Support.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtP_Support.Location = New System.Drawing.Point(0, 0)
        Me.txtP_Support.Multiline = True
        Me.txtP_Support.Name = "txtP_Support"
        Me.txtP_Support.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.txtP_Support.Size = New System.Drawing.Size(453, 100)
        Me.txtP_Support.TabIndex = 1
        '
        'tabpPOtherTraining
        '
        Me.tabpPOtherTraining.Controls.Add(Me.txtP_OtherTraining)
        Me.tabpPOtherTraining.Location = New System.Drawing.Point(4, 22)
        Me.tabpPOtherTraining.Name = "tabpPOtherTraining"
        Me.tabpPOtherTraining.Size = New System.Drawing.Size(453, 100)
        Me.tabpPOtherTraining.TabIndex = 3
        Me.tabpPOtherTraining.Text = "Other Training"
        Me.tabpPOtherTraining.UseVisualStyleBackColor = True
        '
        'txtP_OtherTraining
        '
        Me.txtP_OtherTraining.BackColor = System.Drawing.SystemColors.Window
        Me.txtP_OtherTraining.Dock = System.Windows.Forms.DockStyle.Fill
        Me.txtP_OtherTraining.Flags = 0
        Me.txtP_OtherTraining.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtP_OtherTraining.Location = New System.Drawing.Point(0, 0)
        Me.txtP_OtherTraining.Multiline = True
        Me.txtP_OtherTraining.Name = "txtP_OtherTraining"
        Me.txtP_OtherTraining.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.txtP_OtherTraining.Size = New System.Drawing.Size(453, 100)
        Me.txtP_OtherTraining.TabIndex = 1
        '
        'cboPTraining
        '
        Me.cboPTraining.DisplayMember = "masterunkid"
        Me.cboPTraining.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboPTraining.DropDownWidth = 300
        Me.cboPTraining.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboPTraining.FormattingEnabled = True
        Me.cboPTraining.Location = New System.Drawing.Point(653, 75)
        Me.cboPTraining.Name = "cboPTraining"
        Me.cboPTraining.Size = New System.Drawing.Size(210, 21)
        Me.cboPTraining.TabIndex = 5
        Me.cboPTraining.ValueMember = "masterunkid"
        '
        'dtpPTimeframe
        '
        Me.dtpPTimeframe.CalendarFont = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpPTimeframe.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpPTimeframe.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpPTimeframe.Location = New System.Drawing.Point(653, 48)
        Me.dtpPTimeframe.Name = "dtpPTimeframe"
        Me.dtpPTimeframe.Size = New System.Drawing.Size(90, 21)
        Me.dtpPTimeframe.TabIndex = 3
        '
        'tabpReviewerComments
        '
        Me.tabpReviewerComments.Controls.Add(Me.gbComments)
        Me.tabpReviewerComments.Location = New System.Drawing.Point(4, 22)
        Me.tabpReviewerComments.Name = "tabpReviewerComments"
        Me.tabpReviewerComments.Size = New System.Drawing.Size(898, 435)
        Me.tabpReviewerComments.TabIndex = 2
        Me.tabpReviewerComments.Text = "Reviewer Comments"
        Me.tabpReviewerComments.UseVisualStyleBackColor = True
        '
        'gbComments
        '
        Me.gbComments.BorderColor = System.Drawing.Color.Black
        Me.gbComments.Checked = False
        Me.gbComments.CollapseAllExceptThis = False
        Me.gbComments.CollapsedHoverImage = Nothing
        Me.gbComments.CollapsedNormalImage = Nothing
        Me.gbComments.CollapsedPressedImage = Nothing
        Me.gbComments.CollapseOnLoad = False
        Me.gbComments.Controls.Add(Me.elRemark1)
        Me.gbComments.Controls.Add(Me.pnlRemark2)
        Me.gbComments.Controls.Add(Me.elRemark2)
        Me.gbComments.Controls.Add(Me.pnlRemark1)
        Me.gbComments.Dock = System.Windows.Forms.DockStyle.Fill
        Me.gbComments.ExpandedHoverImage = Nothing
        Me.gbComments.ExpandedNormalImage = Nothing
        Me.gbComments.ExpandedPressedImage = Nothing
        Me.gbComments.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbComments.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbComments.HeaderHeight = 25
        Me.gbComments.HeaderMessage = ""
        Me.gbComments.HeaderMessageFont = New System.Drawing.Font("Microsoft Sans Serif", 8.25!)
        Me.gbComments.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbComments.HeightOnCollapse = 0
        Me.gbComments.LeftTextSpace = 0
        Me.gbComments.Location = New System.Drawing.Point(0, 0)
        Me.gbComments.Name = "gbComments"
        Me.gbComments.OpenHeight = 300
        Me.gbComments.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbComments.ShowBorder = True
        Me.gbComments.ShowCheckBox = False
        Me.gbComments.ShowCollapseButton = False
        Me.gbComments.ShowDefaultBorderColor = True
        Me.gbComments.ShowDownButton = False
        Me.gbComments.ShowHeader = True
        Me.gbComments.Size = New System.Drawing.Size(898, 435)
        Me.gbComments.TabIndex = 0
        Me.gbComments.Temp = 0
        Me.gbComments.Text = "Comments"
        Me.gbComments.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'elRemark1
        '
        Me.elRemark1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.elRemark1.LineOrientation = eZee.Common.eZeeLine.Orientation.Horizontal
        Me.elRemark1.Location = New System.Drawing.Point(8, 36)
        Me.elRemark1.Name = "elRemark1"
        Me.elRemark1.Size = New System.Drawing.Size(868, 17)
        Me.elRemark1.TabIndex = 0
        Me.elRemark1.Text = "Comments and Recommendation by Head of Department/Deputy  Head of  Department/Man" & _
            "ager"
        '
        'pnlRemark2
        '
        Me.pnlRemark2.Controls.Add(Me.txtRemark2)
        Me.pnlRemark2.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.pnlRemark2.Location = New System.Drawing.Point(25, 57)
        Me.pnlRemark2.Name = "pnlRemark2"
        Me.pnlRemark2.Size = New System.Drawing.Size(851, 159)
        Me.pnlRemark2.TabIndex = 3
        '
        'txtRemark2
        '
        Me.txtRemark2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.txtRemark2.Flags = 0
        Me.txtRemark2.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtRemark2.Location = New System.Drawing.Point(0, 0)
        Me.txtRemark2.Multiline = True
        Me.txtRemark2.Name = "txtRemark2"
        Me.txtRemark2.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.txtRemark2.Size = New System.Drawing.Size(851, 159)
        Me.txtRemark2.TabIndex = 0
        '
        'elRemark2
        '
        Me.elRemark2.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.elRemark2.LineOrientation = eZee.Common.eZeeLine.Orientation.Horizontal
        Me.elRemark2.Location = New System.Drawing.Point(8, 230)
        Me.elRemark2.Name = "elRemark2"
        Me.elRemark2.Size = New System.Drawing.Size(868, 17)
        Me.elRemark2.TabIndex = 1
        Me.elRemark2.Text = "Comments and Recommendation by Deputy Commissioner General/Head Of Department/Dep" & _
            "uty Head of Department"
        '
        'pnlRemark1
        '
        Me.pnlRemark1.Controls.Add(Me.txtRemark1)
        Me.pnlRemark1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.pnlRemark1.Location = New System.Drawing.Point(25, 253)
        Me.pnlRemark1.Name = "pnlRemark1"
        Me.pnlRemark1.Size = New System.Drawing.Size(851, 171)
        Me.pnlRemark1.TabIndex = 1
        '
        'txtRemark1
        '
        Me.txtRemark1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.txtRemark1.Flags = 0
        Me.txtRemark1.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtRemark1.Location = New System.Drawing.Point(0, 0)
        Me.txtRemark1.Multiline = True
        Me.txtRemark1.Name = "txtRemark1"
        Me.txtRemark1.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.txtRemark1.Size = New System.Drawing.Size(851, 171)
        Me.txtRemark1.TabIndex = 0
        '
        'pnlEvaluation
        '
        Me.pnlEvaluation.Controls.Add(Me.tabcEvaluation)
        Me.pnlEvaluation.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pnlEvaluation.Location = New System.Drawing.Point(0, 0)
        Me.pnlEvaluation.Name = "pnlEvaluation"
        Me.pnlEvaluation.Size = New System.Drawing.Size(906, 461)
        Me.pnlEvaluation.TabIndex = 14
        '
        'lnkScoreGuide
        '
        Me.lnkScoreGuide.Enabled = False
        Me.lnkScoreGuide.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lnkScoreGuide.LinkBehavior = System.Windows.Forms.LinkBehavior.NeverUnderline
        Me.lnkScoreGuide.Location = New System.Drawing.Point(376, 161)
        Me.lnkScoreGuide.Name = "lnkScoreGuide"
        Me.lnkScoreGuide.Size = New System.Drawing.Size(93, 17)
        Me.lnkScoreGuide.TabIndex = 388
        Me.lnkScoreGuide.TabStop = True
        Me.lnkScoreGuide.Text = "Score &Guide"
        Me.lnkScoreGuide.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'frmEvaluation
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(906, 516)
        Me.Controls.Add(Me.pnlEvaluation)
        Me.Controls.Add(Me.objFooter)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmEvaluation"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Evaluation"
        Me.objFooter.ResumeLayout(False)
        Me.tabcEvaluation.ResumeLayout(False)
        Me.tabpCompetenceEvaluation.ResumeLayout(False)
        Me.gbAssessmentInfo.ResumeLayout(False)
        Me.gbAssessmentInfo.PerformLayout()
        Me.pnlContainer.ResumeLayout(False)
        Me.pnlEvaluationList.ResumeLayout(False)
        Me.tabpImporvementArea.ResumeLayout(False)
        Me.gbImprovement.ResumeLayout(False)
        Me.pnlImproveList.ResumeLayout(False)
        Me.tabcImprovement.ResumeLayout(False)
        Me.tabpImprovement.ResumeLayout(False)
        Me.tabpImprovement.PerformLayout()
        Me.tapbActionRequired.ResumeLayout(False)
        Me.tapbActionRequired.PerformLayout()
        Me.tabpSupportRequired.ResumeLayout(False)
        Me.tabpSupportRequired.PerformLayout()
        Me.tabpOtherTraining.ResumeLayout(False)
        Me.tabpOtherTraining.PerformLayout()
        Me.tabpPersonalDevelopment.ResumeLayout(False)
        Me.gbPersonalDevelopment.ResumeLayout(False)
        Me.pnlPersonalList.ResumeLayout(False)
        Me.tabcPersonal.ResumeLayout(False)
        Me.tabpDevelopment.ResumeLayout(False)
        Me.tabpDevelopment.PerformLayout()
        Me.tabpPAction.ResumeLayout(False)
        Me.tabpPAction.PerformLayout()
        Me.tabpPSupport.ResumeLayout(False)
        Me.tabpPSupport.PerformLayout()
        Me.tabpPOtherTraining.ResumeLayout(False)
        Me.tabpPOtherTraining.PerformLayout()
        Me.tabpReviewerComments.ResumeLayout(False)
        Me.gbComments.ResumeLayout(False)
        Me.pnlRemark2.ResumeLayout(False)
        Me.pnlRemark2.PerformLayout()
        Me.pnlRemark1.ResumeLayout(False)
        Me.pnlRemark1.PerformLayout()
        Me.pnlEvaluation.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents objFooter As eZee.Common.eZeeFooter
    Friend WithEvents btnSave As eZee.Common.eZeeLightButton
    Friend WithEvents btnClose As eZee.Common.eZeeLightButton
    Friend WithEvents tabcEvaluation As System.Windows.Forms.TabControl
    Friend WithEvents tabpCompetenceEvaluation As System.Windows.Forms.TabPage
    Friend WithEvents tabpImporvementArea As System.Windows.Forms.TabPage
    Friend WithEvents tabpReviewerComments As System.Windows.Forms.TabPage
    Friend WithEvents pnlEvaluation As System.Windows.Forms.Panel
    Friend WithEvents gbAssessmentInfo As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents cboSubItems As System.Windows.Forms.ComboBox
    Friend WithEvents lblSubItems As System.Windows.Forms.Label
    Friend WithEvents btnAddEvaluation As eZee.Common.eZeeLightButton
    Friend WithEvents btnEditEvaluation As eZee.Common.eZeeLightButton
    Friend WithEvents btnDeleteEvaluation As eZee.Common.eZeeLightButton
    Friend WithEvents gbImprovement As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents pnlRemark2 As System.Windows.Forms.Panel
    Friend WithEvents pnlRemark1 As System.Windows.Forms.Panel
    Friend WithEvents txtRemark2 As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents txtRemark1 As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents objbtnSearchSubItems As eZee.Common.eZeeGradientButton
    Friend WithEvents objbtnSearchItems As eZee.Common.eZeeGradientButton
    Friend WithEvents objbtnSearchGroup As eZee.Common.eZeeGradientButton
    Friend WithEvents objbtnSearchResult As eZee.Common.eZeeGradientButton
    Friend WithEvents tabpPersonalDevelopment As System.Windows.Forms.TabPage
    Friend WithEvents gbPersonalDevelopment As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents gbComments As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents elRemark1 As eZee.Common.eZeeLine
    Friend WithEvents elRemark2 As eZee.Common.eZeeLine
    Friend WithEvents lblAssessGroup As System.Windows.Forms.Label
    Friend WithEvents cboAssessGroup As System.Windows.Forms.ComboBox
    Friend WithEvents lblAssessmentItems As System.Windows.Forms.Label
    Friend WithEvents cboAssessItem As System.Windows.Forms.ComboBox
    Friend WithEvents cboResultCode As System.Windows.Forms.ComboBox
    Friend WithEvents lblResult As System.Windows.Forms.Label
    Friend WithEvents cboAssessor As System.Windows.Forms.ComboBox
    Friend WithEvents objbtnSearchAssessor As eZee.Common.eZeeGradientButton
    Friend WithEvents lblAssessor As System.Windows.Forms.Label
    Friend WithEvents lblRemark As System.Windows.Forms.Label
    Friend WithEvents txtRemark As System.Windows.Forms.RichTextBox
    Friend WithEvents objLine3 As eZee.Common.eZeeStraightLine
    Friend WithEvents lvEvaluation As eZee.Common.eZeeListView
    Friend WithEvents objLine2 As eZee.Common.eZeeStraightLine
    Friend WithEvents objLine1 As eZee.Common.eZeeStraightLine
    Friend WithEvents pnlContainer As System.Windows.Forms.Panel
    Friend WithEvents lblAssessDate As System.Windows.Forms.Label
    Friend WithEvents lblEmployee As System.Windows.Forms.Label
    Friend WithEvents dtpAssessdate As System.Windows.Forms.DateTimePicker
    Friend WithEvents lblYear As System.Windows.Forms.Label
    Friend WithEvents objbtnSearchEmployee As eZee.Common.eZeeGradientButton
    Friend WithEvents lblPeriod As System.Windows.Forms.Label
    Friend WithEvents cboYear As System.Windows.Forms.ComboBox
    Friend WithEvents cboPeriod As System.Windows.Forms.ComboBox
    Friend WithEvents cboEmployee As System.Windows.Forms.ComboBox
    Friend WithEvents lblILearningObjective As System.Windows.Forms.Label
    Friend WithEvents lblITime As System.Windows.Forms.Label
    Friend WithEvents tabcImprovement As System.Windows.Forms.TabControl
    Friend WithEvents tabpImprovement As System.Windows.Forms.TabPage
    Friend WithEvents tapbActionRequired As System.Windows.Forms.TabPage
    Friend WithEvents tabpSupportRequired As System.Windows.Forms.TabPage
    Friend WithEvents tabpOtherTraining As System.Windows.Forms.TabPage
    Friend WithEvents cboITraining As System.Windows.Forms.ComboBox
    Friend WithEvents dtpITimeframe As System.Windows.Forms.DateTimePicker
    Friend WithEvents btnDeleteImprovement As eZee.Common.eZeeLightButton
    Friend WithEvents btnEditImprovement As eZee.Common.eZeeLightButton
    Friend WithEvents btnAddImprovement As eZee.Common.eZeeLightButton
    Friend WithEvents objStLine4 As eZee.Common.eZeeStraightLine
    Friend WithEvents objbtnSearchITraning As eZee.Common.eZeeGradientButton
    Friend WithEvents objStLine5 As eZee.Common.eZeeStraightLine
    Friend WithEvents lvImprovement As eZee.Common.eZeeListView
    Friend WithEvents txtI_Improvement As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents txtI_Action As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents txtI_Support As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents txtI_OtherTraining As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents objStLine7 As eZee.Common.eZeeStraightLine
    Friend WithEvents btnDeletePersonal As eZee.Common.eZeeLightButton
    Friend WithEvents btnEditPersonal As eZee.Common.eZeeLightButton
    Friend WithEvents btnAddPersonal As eZee.Common.eZeeLightButton
    Friend WithEvents objStLine6 As eZee.Common.eZeeStraightLine
    Friend WithEvents objbtnSearchPTraning As eZee.Common.eZeeGradientButton
    Friend WithEvents lblPLearningObjective As System.Windows.Forms.Label
    Friend WithEvents lblPTime As System.Windows.Forms.Label
    Friend WithEvents tabcPersonal As System.Windows.Forms.TabControl
    Friend WithEvents tabpDevelopment As System.Windows.Forms.TabPage
    Friend WithEvents txtP_Development As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents tabpPAction As System.Windows.Forms.TabPage
    Friend WithEvents txtP_Action As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents tabpPSupport As System.Windows.Forms.TabPage
    Friend WithEvents txtP_Support As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents tabpPOtherTraining As System.Windows.Forms.TabPage
    Friend WithEvents txtP_OtherTraining As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents cboPTraining As System.Windows.Forms.ComboBox
    Friend WithEvents dtpPTimeframe As System.Windows.Forms.DateTimePicker
    Friend WithEvents btnSaveCommit As eZee.Common.eZeeLightButton
    Friend WithEvents pnlEvaluationList As System.Windows.Forms.Panel
    Friend WithEvents objdgcolhAssessItem As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhAssessSubItem As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhResult As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhRemark As System.Windows.Forms.ColumnHeader
    Friend WithEvents objcolhAssessItemId As System.Windows.Forms.ColumnHeader
    Friend WithEvents objcolhSubItemId As System.Windows.Forms.ColumnHeader
    Friend WithEvents objcolhResultId As System.Windows.Forms.ColumnHeader
    Friend WithEvents objcolhGUID As System.Windows.Forms.ColumnHeader
    Public WithEvents objcolhSortId As System.Windows.Forms.ColumnHeader
    Friend WithEvents pnlImproveList As System.Windows.Forms.Panel
    Friend WithEvents colhI_Improvement As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhI_ActivityReq As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhI_Support As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhI_Course As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhI_Timeframe As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhI_OtherTraning As System.Windows.Forms.ColumnHeader
    Friend WithEvents objcolhI_CourseId As System.Windows.Forms.ColumnHeader
    Friend WithEvents objcolhI_GUID As System.Windows.Forms.ColumnHeader
    Friend WithEvents pnlPersonalList As System.Windows.Forms.Panel
    Friend WithEvents lvPersonalDevelop As eZee.Common.eZeeListView
    Friend WithEvents colhP_Development As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhP_ActivityReq As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhP_Support As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhP_Course As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhP_Timeframe As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhP_OtherTraning As System.Windows.Forms.ColumnHeader
    Friend WithEvents objcolhP_CourseId As System.Windows.Forms.ColumnHeader
    Friend WithEvents objcolhP_GUID As System.Windows.Forms.ColumnHeader
    Friend WithEvents lnkViewAssessments As System.Windows.Forms.LinkLabel
    Friend WithEvents lblReviewer As System.Windows.Forms.Label
    Friend WithEvents radExternalAssessor As System.Windows.Forms.RadioButton
    Friend WithEvents radInternalAssessor As System.Windows.Forms.RadioButton
    Friend WithEvents cboReviewer As System.Windows.Forms.ComboBox
    Friend WithEvents objbtnSearchReviewer As eZee.Common.eZeeGradientButton
    Friend WithEvents lblWeight As System.Windows.Forms.Label
    Friend WithEvents txtWeightage As System.Windows.Forms.TextBox
    Friend WithEvents lnkScoreGuide As System.Windows.Forms.LinkLabel
End Class
