﻿Option Strict On

#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data

#End Region

Public Class frmAssignCompetencies

#Region " Private Variables "

    Private ReadOnly mstrModuleName As String = "frmAssignCompetencies"
    Private menAction As enAction = enAction.ADD_ONE
    Private mdtTran As DataTable
    Private objCompetence As clsassess_competencies_master
    Private objAssignCompetenceMst As clsassess_competence_assign_master
    Private objAssignCompetenceTrn As clsassess_competence_assign_tran
    Private mdtCompetenceTran As DataTable
    Private mintComptenceAssignMasterId As Integer = 0
    Private mblnCancel As Boolean = True
    Private mDecAssignedWeight As Decimal = 0
    Private iVoidReason As String = String.Empty

#End Region

#Region " Display Dialog "

    Public Function displayDialog(ByRef intUnkId As Integer, ByVal eAction As enAction) As Boolean
        Try
            mintComptenceAssignMasterId = intUnkId
            menAction = eAction

            Me.ShowDialog()

            intUnkId = mintComptenceAssignMasterId

            Return Not mblnCancel
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "displayDialog", mstrModuleName)
        End Try
    End Function

#End Region

#Region " Private Methods "

    Private Sub SetColor()
        Try
            cboGroup.BackColor = GUI.ColorComp
            cboPeriod.BackColor = GUI.ColorComp
            txtWeight.BackColor = GUI.ColorComp
            'S.SANDEEP [29 JAN 2015] -- START
            If ConfigParameter._Object._Self_Assign_Competencies = True Then
                cboEmployee.BackColor = GUI.ColorComp
            End If
            'S.SANDEEP [29 JAN 2015] -- END
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetColor", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub GetValue()
        Try
            'S.SANDEEP [29 JAN 2015] -- START
            If ConfigParameter._Object._Self_Assign_Competencies = True Then
                cboEmployee.SelectedValue = objAssignCompetenceMst._Employeeunkid
            End If
            'S.SANDEEP [29 JAN 2015] -- END
            cboGroup.SelectedValue = objAssignCompetenceMst._Assessgroupunkid
            cboPeriod.SelectedValue = objAssignCompetenceMst._Periodunkid
            txtWeight.Decimal = objAssignCompetenceMst._Weight

            'Shani(24-Feb-2016) -- Start
            chkZeroCompetenceWeight.Checked = objAssignCompetenceMst._IsZeroWeightAllowed
            'Shani(24-Feb-2016) -- End

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetValue", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub SetValue()
        Try
            objAssignCompetenceMst._Assessgroupunkid = CInt(cboGroup.SelectedValue)
            objAssignCompetenceMst._Periodunkid = CInt(cboPeriod.SelectedValue)
            objAssignCompetenceMst._Weight = txtWeight.Decimal
            'S.SANDEEP [29 JAN 2015] -- START
            objAssignCompetenceMst._Userunkid = User._Object._Userunkid
            objAssignCompetenceMst._Isvoid = False
            objAssignCompetenceMst._Voiddatetime = Nothing
            objAssignCompetenceMst._Voiduserunkid = -1
            If ConfigParameter._Object._Self_Assign_Competencies = True Then
                objAssignCompetenceMst._Employeeunkid = CInt(cboEmployee.SelectedValue)
            End If
            'S.SANDEEP [29 JAN 2015] -- END

            'S.SANDEEP [05 DEC 2015] -- START

            'Shani(24-Feb-2016) -- Start
            'If ConfigParameter._Object._SkipApprovalFlowInPlanning Then
            '    objAssignCompetenceMst._IsFinal = True
            'End If
            'Shani(24-Feb-2016) -- End
            'S.SANDEEP [05 DEC 2015] -- END

            'Shani(24-Feb-2016) -- Start
            objAssignCompetenceMst._IsZeroWeightAllowed = chkZeroCompetenceWeight.Checked
            'Shani(24-Feb-2016) -- End

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetValue", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub FillCombo()
        Dim dsCombo As New DataSet
        Dim objAGroup As New clsassess_group_master
        Dim objMaster As New clsMasterData
        'S.SANDEEP [29 JAN 2015] -- START
        Dim objPeriod As New clscommom_period_Tran
        Dim objEmployee As New clsEmployee_Master
        'S.SANDEEP [29 JAN 2015] -- END
        Try
            'S.SANDEEP [29 JAN 2015] -- START
            'dsCombo = objAGroup.getListForCombo("List", True)
            'With cboGroup
            '    .ValueMember = "assessgroupunkid"
            '    .DisplayMember = "name"
            '    .DataSource = dsCombo.Tables(0)
            '    .SelectedValue = 0
            'End With
            Dim dt As DataTable = Nothing
            dsCombo = objAGroup.getListForCombo("List", True)
            If ConfigParameter._Object._Self_Assign_Competencies = True Then
                dt = New DataView(dsCombo.Tables(0), "assessgroupunkid = 0", "", DataViewRowState.CurrentRows).ToTable
            Else
                dt = dsCombo.Tables(0)
            End If
            With cboGroup
                .ValueMember = "assessgroupunkid"
                .DisplayMember = "name"
                .DataSource = dt
                .SelectedValue = 0
            End With

            'dsCombo = objMaster.getComboListPAYYEAR("List", True, FinancialYear._Object._YearUnkid)
            'With cboYear
            '    .ValueMember = "Id"
            '    .DisplayMember = "name"
            '    .DataSource = dsCombo.Tables(0)
            '    .SelectedValue = 0
            'End With

            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'If ConfigParameter._Object._IsIncludeInactiveEmp = False Then
            '    dsCombo = objEmployee.GetEmployeeList("List", True, , , , , , , , , , , , , eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate))
            'Else
            '    dsCombo = objEmployee.GetEmployeeList("List", True, )
            'End If

            'S.SANDEEP [05 DEC 2015] -- START
            Dim strFilterQry As String = String.Empty
            If ConfigParameter._Object._SkipApprovalFlowInPlanning Then
                Dim csvIds As String = String.Empty
                Dim dsMapEmp As New DataSet
                Dim objEval As New clsevaluation_analysis_master

                dsMapEmp = objEval.getAssessorComboList(FinancialYear._Object._DatabaseName, _
                                                        User._Object._Userunkid, _
                                                        eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                        eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                        True, ConfigParameter._Object._IsIncludeInactiveEmp, "List", clsAssessor.enARVisibilityTypeId.VISIBLE, False, False) 'S.SANDEEP [27 DEC 2016] -- START {clsAssessor.enARVisibilityTypeId.VISIBLE}-- END

                strFilterQry = " hremployee_master.employeeunkid NOT IN "
                If dsMapEmp.Tables("List").Rows.Count > 0 Then
                    csvIds = String.Join(",", dsMapEmp.Tables("List").AsEnumerable().Select(Function(x) x.Field(Of Integer)("EmpId").ToString()).ToArray())
                End If
                If csvIds.Trim.Length > 0 Then
                    strFilterQry &= "(" & csvIds & ")"
                Else
                    strFilterQry = ""
                End If
                objEval = Nothing
            End If
            'S.SANDEEP [05 DEC 2015] -- END

            dsCombo = objEmployee.GetEmployeeList(FinancialYear._Object._DatabaseName, _
                                                  User._Object._Userunkid, _
                                                  FinancialYear._Object._YearUnkid, _
                                                  Company._Object._Companyunkid, _
                                                  eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                  eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                  ConfigParameter._Object._UserAccessModeSetting, _
                                                  True, ConfigParameter._Object._IsIncludeInactiveEmp, "List", True, , , , , , , , , , , , , , , strFilterQry) 'S.SANDEEP [05 DEC 2015] {strFilterQry} 
            'S.SANDEEP [04 JUN 2015] -- END

            With cboEmployee
                .ValueMember = "employeeunkid"
                .DisplayMember = "employeename"
                .DataSource = dsCombo.Tables("List")
                .SelectedValue = 0
            End With

            'S.SANDEEP [14 APR 2015] -- START
            'dsCombo = objPeriod.getListForCombo(enModuleReference.Assessment,FinancialYear._Object._YearUnkid, "List", True, enStatusType.Open)
            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'dsCombo = objPeriod.getListForCombo(enModuleReference.Assessment, 0, "List", True, enStatusType.Open)

            'S.SANDEEP |02-MAR-2021| -- START
            'ISSUE/ENHANCEMENT : Competencies Period Changes
            'dsCombo = objPeriod.getListForCombo(enModuleReference.Assessment, 0, FinancialYear._Object._DatabaseName, FinancialYear._Object._Database_Start_Date, "List", True, enStatusType.Open)
            Dim blnIsCompetence As Boolean = False
            If ConfigParameter._Object._RunCompetenceAssessmentSeparately Then
                blnIsCompetence = True
            End If
            dsCombo = objPeriod.getListForCombo(enModuleReference.Assessment, 0, FinancialYear._Object._DatabaseName, FinancialYear._Object._Database_Start_Date, "List", True, enStatusType.Open, False, blnIsCompetence, CBool(IIf(blnIsCompetence = True, True, False)))        
            'S.SANDEEP |02-MAR-2021| -- END


            'Sohail (21 Aug 2015) -- End
            'S.SANDEEP [14 APR 2015] -- END
            With cboPeriod
                .ValueMember = "periodunkid"
                .DisplayMember = "name"
                .DataSource = dsCombo.Tables(0)
                .SelectedValue = 0
            End With
            'S.SANDEEP [29 JAN 2015] -- END



        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub FillGrid()
        Try
            'S.SANDEEP [29 JAN 2015] -- START
            'If menAction = enAction.EDIT_ONE Then
            '    mdtTran = objCompetence.Grouped_Competencies(CInt(cboGroup.SelectedValue), CInt(cboPeriod.SelectedValue), 2)
            'Else
            '    mdtTran = objCompetence.Grouped_Competencies(CInt(cboGroup.SelectedValue), CInt(cboPeriod.SelectedValue), 1)
            'End If

            'S.SANDEEP [23 DEC 2015] -- START
            'If menAction = enAction.EDIT_ONE Then
            '    mdtTran = objCompetence.Grouped_Competencies(CInt(cboGroup.SelectedValue), CInt(cboPeriod.SelectedValue), 2, , CInt(cboEmployee.SelectedValue))
            'Else
            '    mdtTran = objCompetence.Grouped_Competencies(CInt(cboGroup.SelectedValue), CInt(cboPeriod.SelectedValue), 1, , CInt(cboEmployee.SelectedValue))
            'End If

            'S.SANDEEP [25-JAN-2017] -- START
            'ISSUE/ENHANCEMENT :
            'If menAction = enAction.EDIT_ONE Then
            '    mdtTran = objCompetence.Grouped_Competencies(CInt(cboGroup.SelectedValue), CInt(cboPeriod.SelectedValue), 2, , CInt(cboEmployee.SelectedValue), chkAssignByJobCompetencies.Checked, eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate))
            'Else
            '    mdtTran = objCompetence.Grouped_Competencies(CInt(cboGroup.SelectedValue), CInt(cboPeriod.SelectedValue), 1, , CInt(cboEmployee.SelectedValue), chkAssignByJobCompetencies.Checked, eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate))
            'End If

            If menAction = enAction.EDIT_ONE Then
                mdtTran = objCompetence.Grouped_Competencies(CInt(cboGroup.SelectedValue), CInt(cboPeriod.SelectedValue), 2, , CInt(cboEmployee.SelectedValue), ConfigParameter._Object._AssignByJobCompetencies, eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate))
            Else
                mdtTran = objCompetence.Grouped_Competencies(CInt(cboGroup.SelectedValue), CInt(cboPeriod.SelectedValue), 1, , CInt(cboEmployee.SelectedValue), ConfigParameter._Object._AssignByJobCompetencies, eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate))
            End If
            'S.SANDEEP [25-JAN-2017] -- END

            'S.SANDEEP [23 DEC 2015] -- END

            'S.SANDEEP [28 DEC 2015] -- START
            If mdtTran Is Nothing AndAlso objCompetence._Message <> "" Then
                eZeeMsgBox.Show(objCompetence._Message, enMsgBoxStyle.Information)
                btnSave.Enabled = False
                Exit Sub
            Else
                btnSave.Enabled = True
            End If

            'S.SANDEEP [28 DEC 2015] -- END

            'S.SANDEEP [29 JAN 2015] -- END
            If mdtTran.Rows.Count > 0 Then
                dgvData.AutoGenerateColumns = False
                objdgcolhCheck.DataPropertyName = "ischeck"
                objdgcolhGrpId.DataPropertyName = "GrpId"
                objdgcolhIsGrp.DataPropertyName = "IsGrp"
                dgcolhCompetencies.DataPropertyName = "competence"
                dgcolhScale.DataPropertyName = "scale"
                dgcolhWeight.DataPropertyName = "weight"
                objdgcolhAssignMasterId.DataPropertyName = "assigncompetenceunkid"
                objdgcolhAssignTranId.DataPropertyName = "assigncompetencetranunkid"
                objdgcolhGUID.DataPropertyName = "GUID"

                'S.SANDEEP [21 NOV 2015] -- START
                dgcolhLink.DataPropertyName = "linkcol"
                objdgcolhIsPGrp.DataPropertyName = "IsPGrp"
                objdgcolhPGrpId.DataPropertyName = "PGrId"
                objdgcolhcompetenciesunkid.DataPropertyName = "competenciesunkid"
                'S.SANDEEP [21 NOV 2015] -- END

                dgvData.DataSource = mdtTran

                dgcolhWeight.HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleRight

                Call SetGridColor()
                Call SetCollapseValue()

                'Shani(24-Feb-2016) -- Start
                'If txtWeight.Decimal <= 0 Then objdgcolhCheck.ReadOnly = True
                If User._Object.Privilege._AllowtoChangePreassignedCompetencies = False Then
                    objdgcolhCheck.ReadOnly = True
                End If
                'Shani(24-Feb-2016) -- End
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillGrid", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub SetGridColor()
        Try

            Dim dgvcsHeader As New DataGridViewCellStyle
            dgvcsHeader.ForeColor = Color.White
            dgvcsHeader.SelectionBackColor = Color.Gray
            dgvcsHeader.SelectionForeColor = Color.White
            dgvcsHeader.BackColor = Color.Gray

            'S.SANDEEP [21 NOV 2015] -- START
            Dim dgvcsGrpHeader As New DataGridViewCellStyle
            dgvcsGrpHeader.ForeColor = Color.White
            dgvcsGrpHeader.SelectionBackColor = Color.RoyalBlue
            dgvcsGrpHeader.SelectionForeColor = Color.White
            dgvcsGrpHeader.BackColor = Color.RoyalBlue

            Dim pCell As New clsMergeCell
            'S.SANDEEP [21 NOV 2015] -- END

            For i As Integer = 0 To dgvData.RowCount - 1

                'S.SANDEEP [21 NOV 2015] -- START
                'If CBool(dgvData.Rows(i).Cells(objdgcolhIsGrp.Index).Value) = True Then
                '    dgvData.Rows(i).DefaultCellStyle = dgvcsHeader
                '    dgvData.Rows(i).Cells(dgcolhWeight.Index).ReadOnly = True
                'Else
                '    If CBool(dgvData.Rows(i).Cells(objdgcolhCheck.Index).Value) = False Then
                '        dgvData.Rows(i).Cells(dgcolhWeight.Index).Style.BackColor = Color.Khaki
                '        dgvData.Rows(i).Cells(dgcolhWeight.Index).ReadOnly = True
                '    Else
                '        dgvData.Rows(i).Cells(dgcolhWeight.Index).Style.BackColor = Color.LightGreen
                '    End If
                'End If
                If CBool(dgvData.Rows(i).Cells(objdgcolhIsPGrp.Index).Value) = True Then
                    'dgvData.Rows(i).DefaultCellStyle = dgvcsGrpHeader
                    'dgvData.Rows(i).Cells(dgcolhWeight.Index).ReadOnly = True
                    'dgvData.Rows(i).Cells(dgcolhLink.Index).Value = ""
                    dgvData.Rows(i).ReadOnly = True
                    pCell.MakeMerge(dgvData, dgvData.Rows(i).Index, 0, dgvData.Rows(i).Cells.Count - 1, Color.RoyalBlue, Color.White, dgvData.Rows(i).Cells(dgcolhCompetencies.Index).Value.ToString, "", picStayView.Image)
                ElseIf CBool(dgvData.Rows(i).Cells(objdgcolhIsGrp.Index).Value) = True Then
                    dgvData.Rows(i).DefaultCellStyle = dgvcsHeader
                    dgvData.Rows(i).Cells(dgcolhWeight.Index).ReadOnly = True
                    'S.SANDEEP [23 DEC 2015] -- START
                    If ConfigParameter._Object._OnlyOneItemPerCompetencyCategory Then
                        dgvData.Rows(i).Cells(objdgcolhCheck.Index).ReadOnly = True
                    End If
                    'S.SANDEEP [23 DEC 2015] -- END
                Else
                    If CBool(dgvData.Rows(i).Cells(objdgcolhCheck.Index).Value) = False Then
                        dgvData.Rows(i).Cells(dgcolhWeight.Index).Style.BackColor = Color.Khaki
                        dgvData.Rows(i).Cells(dgcolhWeight.Index).ReadOnly = True
                        dgvData.Rows(i).Cells(dgcolhLink.Index).Style.ForeColor = Color.RoyalBlue
                    Else
                        dgvData.Rows(i).Cells(dgcolhWeight.Index).Style.BackColor = Color.LightGreen
                    End If
                End If
                'S.SANDEEP [21 NOV 2015] -- END
            Next
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetGridColor", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub SetCollapseValue()
        Try
            Dim objdgvsCollapseHeader As New DataGridViewCellStyle
            objdgvsCollapseHeader.Font = New Font(dgvData.Font.FontFamily, 9, FontStyle.Bold)
            objdgvsCollapseHeader.ForeColor = Color.White
            objdgvsCollapseHeader.BackColor = Color.Gray
            objdgvsCollapseHeader.SelectionBackColor = Color.Gray
            objdgvsCollapseHeader.Alignment = DataGridViewContentAlignment.MiddleCenter

            'S.SANDEEP [21 NOV 2015] -- START
            Dim objdgvcsGrpHeader As New DataGridViewCellStyle
            objdgvcsGrpHeader.ForeColor = Color.White
            objdgvcsGrpHeader.SelectionBackColor = Color.RoyalBlue
            objdgvcsGrpHeader.SelectionForeColor = Color.White
            objdgvcsGrpHeader.BackColor = Color.RoyalBlue
            'S.SANDEEP [21 NOV 2015] -- END

            For i As Integer = 0 To dgvData.RowCount - 1
                'S.SANDEEP [21 NOV 2015] -- START
                If CBool(dgvData.Rows(i).Cells(objdgcolhIsPGrp.Index).Value) = True Then
                    If dgvData.Rows(i).Cells(objdgcolhTree.Index).Value Is Nothing Then
                        dgvData.Rows(i).Cells(objdgcolhTree.Index).Value = ""
                    End If
                    dgvData.Rows(i).Cells(objdgcolhTree.Index).Style = objdgvcsGrpHeader
                ElseIf CBool(dgvData.Rows(i).Cells(objdgcolhIsGrp.Index).Value) = True Then
                    If dgvData.Rows(i).Cells(objdgcolhTree.Index).Value Is Nothing Then
                        dgvData.Rows(i).Cells(objdgcolhTree.Index).Value = "+"
                    End If
                    dgvData.Rows(i).Cells(objdgcolhTree.Index).Style = objdgvsCollapseHeader
                Else
                    dgvData.Rows(i).Visible = False
                End If
                'S.SANDEEP [21 NOV 2015] -- END
            Next
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetCollapseValue", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub Enable_Disable(ByVal iOpr As Boolean)
        Try
            cboGroup.Enabled = iOpr
            objbtnSearchGroup.Enabled = iOpr
            cboPeriod.Enabled = iOpr
            'S.SANDEEP [29 JAN 2015] -- START
            If ConfigParameter._Object._Self_Assign_Competencies = True Then
                cboEmployee.Enabled = iOpr
                objbtnSearchEmployee.Enabled = iOpr
            End If
            'S.SANDEEP [29 JAN 2015] -- END
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Enable_Disable", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Function Validation() As Boolean
        Try
            'S.SANDEEP [29 JAN 2015] -- START
            If ConfigParameter._Object._Self_Assign_Competencies = True Then
                If CInt(cboEmployee.SelectedValue) <= 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 7, "Sorry, Employee is mandatory information. Please select employee to continue."), enMsgBoxStyle.Information)
                    cboEmployee.Focus()
                    Return False
                End If
            End If
            'S.SANDEEP [29 JAN 2015] -- END

            If txtWeight.Decimal < mDecAssignedWeight Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "Sorry, Weight assigned to competencies exceeds the total weight set for the selected group. Please provide the correct weight."), enMsgBoxStyle.Information)
                txtWeight.Focus()
                Return False
            End If


            'SHANI (09 MAY 2015) -- Start 
            'Issue : Datatable is Nothing then get Error Object Reference not Set 
            If mdtTran Is Nothing Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 6, "Sorry, Some of the assigned competencies does not have weight assigned. Please assigned the weight to those checked competencies."), enMsgBoxStyle.Information)
                Return False
            End If
            'SHANI (09 MAY 2015) -- End

            'Shani(24-Feb-2016) -- Start
            '
            If mdtTran.Select("ischeck=True AND IsGrp=False").Length <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 9, "Sorry, Competencies are mandatory information. Please check atleast one competence item in order to save."), enMsgBoxStyle.Information)
                Return False
            End If
            'Shani(24-Feb-2016) -- End

            'S.SANDEEP [23 DEC 2015] -- START
            If ConfigParameter._Object._OnlyOneItemPerCompetencyCategory Then
                Dim tmp() As DataRow = mdtTran.Select("IsGrp=True")
                If tmp.Length > 0 Then
                    For index As Integer = 0 To tmp.Length - 1

                    Next
                End If
            End If
            'S.SANDEEP [23 DEC 2015] -- END

            'Shani(24-Feb-2016) -- Start
            '
            'Dim dtmp() As DataRow = mdtTran.Select("ischeck=True AND IsGrp=False AND weight = 0")
            'If dtmp.Length > 0 Then
            '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 6, "Sorry, Some of the assigned competencies does not have weight assigned. Please assigned the weight to those checked competencies."), enMsgBoxStyle.Information)
            '    Return False
            'End If
            If chkZeroCompetenceWeight.Checked = False Then
                Dim dtmp() As DataRow = mdtTran.Select("ischeck=True AND IsGrp=False AND weight = 0")
                If dtmp.Length > 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 6, "Sorry, Some of the assigned competencies does not have weight assigned. Please assigned the weight to those checked competencies."), enMsgBoxStyle.Information)
                    Return False
                End If
            End If
            'Shani(24-Feb-2016) -- End

            Return True
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Validation", mstrModuleName)
        Finally
        End Try
    End Function

    Private Sub Evaluated_Data(ByVal iWeight As Decimal, ByVal iRowIdx As Integer, Optional ByVal iDelete As Boolean = False)
        Try
            Dim dRow As DataRow = Nothing
            Dim dtmp() As DataRow = Nothing
            If menAction <> enAction.EDIT_ONE Then
                If CInt(dgvData.Rows(iRowIdx).Cells(objdgcolhAssignTranId.Index).Value) > 0 Then
                    dtmp = mdtCompetenceTran.Select("assigncompetencetranunkid = '" & CInt(dgvData.Rows(iRowIdx).Cells(objdgcolhAssignTranId.Index).Value) & "' AND AUD <> 'D' AND competenciesunkid = '" & CInt(mdtTran.Rows(dgvData.Rows(iRowIdx).Index).Item("competenciesunkid")) & "'")
                Else
                    dtmp = mdtCompetenceTran.Select("GUID = '" & CStr(dgvData.Rows(iRowIdx).Cells(objdgcolhGUID.Index).Value) & "' AND AUD <> 'D' AND competenciesunkid = '" & CInt(mdtTran.Rows(dgvData.Rows(iRowIdx).Index).Item("competenciesunkid")) & "'")
                End If
            Else
                dtmp = mdtCompetenceTran.Select("AUD <> 'D' AND competenciesunkid = '" & CInt(mdtTran.Rows(dgvData.Rows(iRowIdx).Index).Item("competenciesunkid")) & "'")
            End If
            If dtmp.Length <= 0 Then
                dRow = mdtCompetenceTran.NewRow
                dRow.Item("assigncompetencetranunkid") = -1
                dRow.Item("assigncompetenceunkid") = mintComptenceAssignMasterId
                dRow.Item("competenciesunkid") = mdtTran.Rows(dgvData.Rows(iRowIdx).Index).Item("competenciesunkid")
                dRow.Item("weight") = iWeight
                dRow.Item("isvoid") = False
                dRow.Item("voiduserunkid") = -1
                dRow.Item("voidreason") = ""
                dRow.Item("voiddatetime") = DBNull.Value
                dRow.Item("AUD") = "A"
                dRow.Item("GUID") = Guid.NewGuid.ToString
                dRow.Item("GrpId") = dgvData.Rows(iRowIdx).Cells(objdgcolhGrpId.Index).Value
                dgvData.Rows(iRowIdx).Cells(objdgcolhGUID.Index).Value = dRow.Item("GUID")
                mdtCompetenceTran.Rows.Add(dRow)
            Else
                dtmp(0).Item("assigncompetencetranunkid") = dtmp(0).Item("assigncompetencetranunkid")
                dtmp(0).Item("assigncompetenceunkid") = mintComptenceAssignMasterId
                dtmp(0).Item("competenciesunkid") = mdtTran.Rows(dgvData.Rows(iRowIdx).Index).Item("competenciesunkid")
                dtmp(0).Item("weight") = iWeight
                If iDelete = True Then
                    dtmp(0).Item("isvoid") = True
                    dtmp(0).Item("voidreason") = iVoidReason
                    dtmp(0).Item("voiduserunkid") = User._Object._Userunkid
                    dtmp(0).Item("voiddatetime") = ConfigParameter._Object._CurrentDateAndTime
                    dtmp(0).Item("AUD") = "D"
                Else
                    dtmp(0).Item("isvoid") = False
                    dtmp(0).Item("voiduserunkid") = -1
                    dtmp(0).Item("voidreason") = ""
                    dtmp(0).Item("voiddatetime") = DBNull.Value
                    If IsDBNull(dtmp(0).Item("AUD")) Or CStr(dtmp(0).Item("AUD")).ToString.Trim = "" Then
                        dtmp(0).Item("AUD") = "U"
                    End If
                End If
                dtmp(0).Item("GUID") = Guid.NewGuid.ToString
                dgvData.Rows(iRowIdx).Cells(objdgcolhGUID.Index).Value = dtmp(0).Item("GUID")
                dtmp(0).AcceptChanges()
            End If
            If mdtTran IsNot Nothing Then
                Dim mIdx As Integer = -1 : Dim dttmp() As DataRow
                dttmp = mdtTran.Select("GrpId = '" & dgvData.Rows(iRowIdx).Cells(objdgcolhGrpId.Index).Value.ToString & "' AND IsGrp=True")
                If dttmp.Length > 0 Then
                    mIdx = mdtTran.Rows.IndexOf(dttmp(0))
                    dgvData.Rows(mIdx).Cells(dgcolhWeight.Index).Value = mdtCompetenceTran.Compute("SUM(weight)", "GrpId = '" & dgvData.Rows(iRowIdx).Cells(objdgcolhGrpId.Index).Value.ToString & "' AND AUD <> 'D'")
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Evaluated_Data", mstrModuleName)
        Finally
        End Try
    End Sub

    'Shani(24-Feb-2016) -- Start
    Public Sub Generate_Competence_Table()
        Try
            If chkZeroCompetenceWeight.Checked = True Then
                If mdtCompetenceTran IsNot Nothing AndAlso mdtCompetenceTran.Rows.Count <= 0 Then
                    For Each xGridRow As DataGridViewRow In dgvData.Rows
                        If CBool(xGridRow.Cells(objdgcolhCheck.Index).Value) = True AndAlso CBool(xGridRow.Cells(objdgcolhIsGrp.Index).Value) = False Then
                            Call Evaluated_Data(0, xGridRow.Index)
                        End If
                    Next
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Generate_Competence_Table", mstrModuleName)
        End Try
    End Sub
    'Shani(24-Feb-2016) -- End

#End Region

#Region " Form's Events "

    Private Sub frmAssignCompetencies_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        objCompetence = New clsassess_competencies_master
        objAssignCompetenceMst = New clsassess_competence_assign_master
        objAssignCompetenceTrn = New clsassess_competence_assign_tran
        Try
            Call Set_Logo(Me, gApplicationType)
            Language.setLanguage(Me.Name)
            Call OtherSettings()
            Call SetColor()
            'S.SANDEEP [29 JAN 2015] -- START
            cboEmployee.Enabled = ConfigParameter._Object._Self_Assign_Competencies
            objbtnSearchEmployee.Enabled = ConfigParameter._Object._Self_Assign_Competencies

            'S.SANDEEP [23 DEC 2015] -- START

            'S.SANDEEP [25-JAN-2017] -- START
            'ISSUE/ENHANCEMENT :
            'If ConfigParameter._Object._Self_Assign_Competencies = False Then
            '    chkAssignByJobCompetencies.Checked = False
            '    chkAssignByJobCompetencies.Visible = False
            'Else
            '    chkAssignByJobCompetencies.Checked = True
            '    chkAssignByJobCompetencies.Visible = True
            'End If
            'S.SANDEEP [25-JAN-2017] -- END

            'S.SANDEEP [23 DEC 2015] -- END

            'S.SANDEEP [29 JAN 2015] -- END
            If menAction = enAction.EDIT_ONE Then
                objAssignCompetenceMst._Assigncompetenceunkid = mintComptenceAssignMasterId
                cboGroup.Enabled = False : objbtnSearchGroup.Enabled = False : cboPeriod.Enabled = False
                'S.SANDEEP [09 FEB 2015] -- START
                cboEmployee.Enabled = False : objbtnSearchEmployee.Enabled = False
                'S.SANDEEP [09 FEB 2015] -- END
            End If
            Call FillCombo()
            Call GetValue()
            If menAction = enAction.EDIT_ONE Then
                Call FillGrid()
            End If
            objAssignCompetenceTrn._Assigncompetenceunkid = mintComptenceAssignMasterId
            mdtCompetenceTran = objAssignCompetenceTrn._DataTable.Copy
            If mdtCompetenceTran.Rows.Count > 0 Then mDecAssignedWeight = CDec(mdtCompetenceTran.Compute("SUM(weight)", "AUD <>'D'"))
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmAssignCompetencies_Load", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()
            clsassess_competence_assign_master.SetMessages()
            objfrm._Other_ModuleNames = "clsassess_competence_assign_master"
            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub

#End Region

#Region " Button's Events "

    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Dim blnFlag As Boolean = False
        Try
            If Validation() = False Then Exit Sub
            Call SetValue()

            'Shani(24-Feb-2016) -- Start
            Call Generate_Competence_Table()
            'Shani(24-Feb-2016) -- End

            With objAssignCompetenceMst
                ._FormName = mstrModuleName
                ._LoginEmployeeunkid = 0
                ._ClientIP = getIP()
                ._HostName = getHostName()
                ._FromWeb = False
                ._AuditUserId = User._Object._Userunkid
._CompanyUnkid = Company._Object._Companyunkid
                ._AuditDate = ConfigParameter._Object._CurrentDateAndTime
            End With

            If menAction = enAction.EDIT_ONE Then
                blnFlag = objAssignCompetenceMst.Update(mdtCompetenceTran)
            Else
                blnFlag = objAssignCompetenceMst.Insert(mdtCompetenceTran)
            End If
            If blnFlag = False And objAssignCompetenceMst._Message <> "" Then
                eZeeMsgBox.Show(objAssignCompetenceMst._Message, enMsgBoxStyle.Information)
            End If
            If blnFlag Then
                mblnCancel = False
                If menAction = enAction.ADD_CONTINUE Then
                    objAssignCompetenceMst = New clsassess_competence_assign_master
                    objAssignCompetenceTrn = New clsassess_competence_assign_tran
                    mdtCompetenceTran.Rows.Clear() : mintComptenceAssignMasterId = 0
                    mDecAssignedWeight = 0 : iVoidReason = ""
                    Call objbtnReset_Click(sender, e)
                Else
                    mintComptenceAssignMasterId = objAssignCompetenceMst._Assigncompetenceunkid
                    Me.Close()
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnSave_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnClose_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub objbtnSearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearch.Click
        Try
            If CInt(cboGroup.SelectedValue) <= 0 Or CInt(cboPeriod.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Sorry, Assessment Group and Period are mandatory information. Please select Assessment Group and Period to continue."), enMsgBoxStyle.Information)
                Exit Sub
            End If

            'S.SANDEEP [09 FEB 2015] -- START

            'Shani(24-Feb-2016) -- Start
            'If objAssignCompetenceMst.isExist(CInt(cboGroup.SelectedValue), CInt(cboPeriod.SelectedValue), , CInt(cboEmployee.SelectedValue)) Then
            '    eZeeMsgBox.Show(Language.getMessage("clsassess_competence_assign_master", 1, "Sorry this group is already defined for the selected period. Please define new group."), enMsgBoxStyle.Information)
            '    Exit Sub
            'End If

            If menAction <> enAction.EDIT_ONE Then
                If objAssignCompetenceMst.isExist(CInt(cboGroup.SelectedValue), CInt(cboPeriod.SelectedValue), , CInt(cboEmployee.SelectedValue)) Then
                    eZeeMsgBox.Show(Language.getMessage("clsassess_competence_assign_master", 1, "Sorry this group is already defined for the selected period. Please define new group."), enMsgBoxStyle.Information)
                    Exit Sub
                End If
            End If
            'Shani(24-Feb-2016) -- End


            'S.SANDEEP [09 FEB 2015] -- END

            Call FillGrid()
            If menAction <> enAction.EDIT_ONE Then
                mdtCompetenceTran.Rows.Clear()
            End If
            Call Enable_Disable(False)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub objbtnReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnReset.Click
        Try
            cboGroup.SelectedValue = 0 : cboPeriod.SelectedValue = 0 : txtWeight.Decimal = 0
            dgvData.DataSource = Nothing : mDecAssignedWeight = 0 : iVoidReason = ""
            Call Enable_Disable(True) : If mdtTran IsNot Nothing Then mdtTran.Rows.Clear()
            'S.SANDEEP [29 JAN 2015] -- START
            If ConfigParameter._Object._Self_Assign_Competencies = True Then cboEmployee.SelectedValue = 0
            'S.SANDEEP [29 JAN 2015] -- END
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnReset_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub objbtnSearchGroup_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchGroup.Click
        Dim frm As New frmCommonSearch
        Try

            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If

            With frm
                .ValueMember = cboGroup.ValueMember
                .DisplayMember = cboGroup.DisplayMember
                .DataSource = CType(cboGroup.DataSource, DataTable)
                If .DisplayDialog Then
                    cboGroup.SelectedValue = .SelectedValue
                    cboGroup.Focus()
                End If
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchGroup_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    'S.SANDEEP [29 JAN 2015] -- START
    Private Sub objbtnSearchEmployee_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchEmployee.Click
        Dim frm As New frmCommonSearch
        Try
            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If

            With frm
                .ValueMember = cboEmployee.ValueMember
                .DisplayMember = cboEmployee.DisplayMember
                .CodeMember = "employeecode"
                .DataSource = CType(cboEmployee.DataSource, DataTable)
                If .DisplayDialog Then
                    cboEmployee.SelectedValue = .SelectedValue
                    cboEmployee.Focus()
                End If
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchEmployee_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub
    'S.SANDEEP [29 JAN 2015] -- END

#End Region

#Region " Controls "

    Private Sub cboGroup_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboGroup.SelectedIndexChanged
        Try
            If CInt(cboGroup.SelectedValue) > 0 Then
                Dim objAGroup As New clsassess_group_master
                objAGroup._Assessgroupunkid = CInt(cboGroup.SelectedValue)
                txtWeight.Decimal = objAGroup._Weight
                objAGroup = Nothing

                'Shani(24-Feb-2016) -- Start
                If txtWeight.Decimal > 0 Then
                    chkZeroCompetenceWeight.Checked = False : chkZeroCompetenceWeight.Visible = False
                Else
                    chkZeroCompetenceWeight.Visible = True
                End If
                'Shani(24-Feb-2016) -- End

            Else
                txtWeight.Decimal = 0
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboGroup_SelectedIndexChanged", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub dgvData_DataError(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewDataErrorEventArgs) Handles dgvData.DataError
        e.Cancel = True
    End Sub

    Private Sub dgvData_CellContentClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvData.CellContentClick, dgvData.CellContentDoubleClick
        Try
            Dim i As Integer
            If e.RowIndex = -1 Then Exit Sub
            If Me.dgvData.IsCurrentCellDirty Then
                Me.dgvData.CommitEdit(DataGridViewDataErrorContexts.Commit)
            End If

            'S.SANDEEP [21 NOV 2015] -- START
            If CBool(dgvData.Rows(e.RowIndex).Cells(objdgcolhIsPGrp.Index).Value) = False Then
                If CBool(dgvData.Rows(e.RowIndex).Cells(objdgcolhIsGrp.Index).Value) = True Then
                    Select Case CInt(e.ColumnIndex)
                        Case 0
                            If dgvData.Rows(e.RowIndex).Cells(objdgcolhTree.Index).Value Is "-" Then
                                dgvData.Rows(e.RowIndex).Cells(objdgcolhTree.Index).Value = "+"
                            Else
                                dgvData.Rows(e.RowIndex).Cells(objdgcolhTree.Index).Value = "-"
                            End If
                            For i = e.RowIndex + 1 To dgvData.RowCount - 1
                                If CInt(dgvData.Rows(e.RowIndex).Cells(objdgcolhGrpId.Index).Value) = CInt(dgvData.Rows(i).Cells(objdgcolhGrpId.Index).Value) Then
                                    If dgvData.Rows(i).Visible = False Then
                                        dgvData.Rows(i).Visible = True
                                    Else
                                        dgvData.Rows(i).Visible = False
                                    End If
                                Else
                                    Exit For
                                End If
                            Next
                        Case 1
                            'S.SANDEEP [23 DEC 2015] -- START
                            If ConfigParameter._Object._OnlyOneItemPerCompetencyCategory = True Then Exit Sub
                            'S.SANDEEP [23 DEC 2015] -- END
                            If dgvData.Columns(e.ColumnIndex).ReadOnly = True Then

                                'Shani(24-Feb-2016) -- Start
                                'If txtWeight.Decimal <= 0 Then
                                '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Sorry, Weight is mandatory information. Please provide Weight first in order to assign competencies to assessment group."), enMsgBoxStyle.Information)
                                '    Exit Sub
                                'End If
                                If CBool(dgvData.Rows(e.RowIndex).Cells(objdgcolhCheck.Index).Value) = True Then
                                    If txtWeight.Decimal <= 0 Then
                                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Sorry, Weight is mandatory information. Please provide Weight first in order to assign competencies to assessment group."), enMsgBoxStyle.Information)
                                        Exit Sub
                                    End If
                                End If
                                'Shani(24-Feb-2016) -- End


                            End If
                            For i = e.RowIndex + 1 To dgvData.RowCount - 1
                                Dim blnFlg As Boolean = CBool(dgvData.Rows(e.RowIndex).Cells(objdgcolhCheck.Index).Value)
                                If CInt(dgvData.Rows(e.RowIndex).Cells(objdgcolhGrpId.Index).Value) = CInt(dgvData.Rows(i).Cells(objdgcolhGrpId.Index).Value) Then
                                    dgvData.Rows(i).Cells(objdgcolhCheck.Index).Value = blnFlg
                                    If blnFlg = True Then
                                        dgvData.Rows(i).Cells(dgcolhWeight.Index).ReadOnly = False
                                        dgvData.Rows(i).Cells(dgcolhWeight.Index).Style.BackColor = Color.LightGreen
                                    Else
                                        If iVoidReason.Trim.Length <= 0 Then
                                            If CInt(dgvData.Rows(i).Cells(objdgcolhAssignTranId.Index).Value) > 0 Then
                                                If iVoidReason.Trim.Length <= 0 Then
                                                    Dim frm As New frmReasonSelection
                                                    If User._Object._Isrighttoleft = True Then
                                                        frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                                                        frm.RightToLeftLayout = True
                                                        Call Language.ctlRightToLeftlayOut(frm)
                                                    End If
                                                    frm.displayDialog(enVoidCategoryType.ASSESSMENT, iVoidReason)
                                                    If iVoidReason.Length <= 0 Then
                                                        mdtTran.Rows(dgvData.Rows(i).Index).Item("ischeck") = True
                                                        Exit Sub
                                                    End If
                                                End If
                                            End If
                                        End If
                                        dgvData.Rows(i).Cells(dgcolhWeight.Index).ReadOnly = True
                                        dgvData.Rows(i).Cells(dgcolhWeight.Index).Style.BackColor = Color.Khaki
                                        dgvData.Rows(i).Cells(dgcolhWeight.Index).Value = 0
                                        Call Evaluated_Data(CDec(dgvData.Rows(i).Cells(dgcolhWeight.Index).Value), dgvData.Rows(i).Index, True)
                                        mdtTran.Rows(i).Item("weight") = CDec(dgvData.Rows(i).Cells(dgcolhWeight.Index).Value)
                                        'S.SANDEEP [09 FEB 2015] -- START
                                        mDecAssignedWeight = CDec(mdtTran.Compute("SUM(weight)", "IsGrp = False"))

                                        'S.SANDEEP [09 FEB 2015] -- END
                                    End If
                                End If
                            Next
                    End Select
                ElseIf CBool(dgvData.Rows(e.RowIndex).Cells(objdgcolhIsGrp.Index).Value) = False Then
                    Select Case CInt(e.ColumnIndex)
                        Case 1
                            If dgvData.Columns(e.ColumnIndex).ReadOnly = True Then

                                'Shani(24-Feb-2016) -- Start
                                If CBool(dgvData.Rows(e.RowIndex).Cells(objdgcolhCheck.Index).Value) = True Then
                                    If txtWeight.Decimal <= 0 Then
                                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Sorry, Weight is mandatory information. Please provide Weight first in order to assign competencies to assessment group."), enMsgBoxStyle.Information)
                                        Exit Sub
                                    End If
                                End If
                                'Shani(24-Feb-2016) -- End
                            End If

                            If CBool(dgvData.Rows(e.RowIndex).Cells(objdgcolhCheck.Index).Value) = True Then
                                dgvData.Rows(e.RowIndex).Cells(dgcolhWeight.Index).ReadOnly = False
                                dgvData.Rows(e.RowIndex).Cells(dgcolhWeight.Index).Style.BackColor = Color.LightGreen
                                dgvData.CurrentCell = dgvData.Rows(e.RowIndex).Cells(dgcolhWeight.Index)
                            Else
                                If iVoidReason.Trim.Length <= 0 Then
                                    If CInt(dgvData.Rows(e.RowIndex).Cells(objdgcolhAssignTranId.Index).Value) > 0 Then
                                        If iVoidReason.Trim.Length <= 0 Then
                                            Dim frm As New frmReasonSelection
                                            If User._Object._Isrighttoleft = True Then
                                                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                                                frm.RightToLeftLayout = True
                                                Call Language.ctlRightToLeftlayOut(frm)
                                            End If
                                            frm.displayDialog(enVoidCategoryType.ASSESSMENT, iVoidReason)
                                            If iVoidReason.Length <= 0 Then
                                                mdtTran.Rows(dgvData.Rows(e.RowIndex).Index).Item("ischeck") = True
                                                Exit Sub
                                            End If
                                        End If
                                    End If
                                End If

                                dgvData.Rows(e.RowIndex).Cells(dgcolhWeight.Index).ReadOnly = True
                                dgvData.Rows(e.RowIndex).Cells(dgcolhWeight.Index).Style.BackColor = Color.Khaki
                                dgvData.Rows(e.RowIndex).Cells(dgcolhWeight.Index).Value = 0
                                Call Evaluated_Data(CDec(dgvData.Rows(e.RowIndex).Cells(dgcolhWeight.Index).Value), e.RowIndex, True)
                                mdtTran.Rows(e.RowIndex).Item("weight") = CDec(dgvData.Rows(e.RowIndex).Cells(dgcolhWeight.Index).Value)
                                'S.SANDEEP [09 FEB 2015] -- START
                                mDecAssignedWeight = CDec(mdtTran.Compute("SUM(weight)", "IsGrp = False"))

                                'S.SANDEEP [09 FEB 2015] -- END
                            End If
                    End Select
                End If

                Select Case e.ColumnIndex
                    Case dgcolhLink.Index
                        If dgvData.Rows(e.RowIndex).Cells(dgcolhLink.Index).Value.ToString <> "" Then
                            txtRemark.Text = ""
                            If CBool(dgvData.Rows(e.RowIndex).Cells(objdgcolhIsGrp.Index).Value) = True Then
                                Dim objCMst As New clsCommon_Master
                                objCMst._Masterunkid = CInt(dgvData.Rows(e.RowIndex).Cells(objdgcolhGrpId.Index).Value)
                                txtRemark.Text = objCMst._Description
                                objCMst = Nothing
                            Else
                                Dim objCMP As New clsassess_competencies_master
                                objCMP._Competenciesunkid = CInt(dgvData.Rows(e.RowIndex).Cells(objdgcolhcompetenciesunkid.Index).Value)
                                txtRemark.Text = objCMP._Description
                                objCMP = Nothing
                            End If
                            If txtRemark.Text.Trim.Length > 0 Then
                                gbDescription.Visible = True
                                dgvData.Enabled = False
                                objFooter.Enabled = False
                            Else
                                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 8, "Sorry, No description set for the selected information."), enMsgBoxStyle.Information)
                                Exit Sub
                            End If
                        End If
                End Select


                'Else
                '    For i = e.RowIndex + 1 To dgvData.RowCount - 1
                '        If CInt(dgvData.Rows(e.RowIndex).Cells(objdgcolhPGrpId.Index).Value) = CInt(dgvData.Rows(i).Cells(objdgcolhPGrpId.Index).Value) Then
                '            'If CInt(dgvData.Rows(e.RowIndex).Cells(objdgcolhGrpId.Index).Value) = CInt(dgvData.Rows(i).Cells(objdgcolhGrpId.Index).Value) Then
                '            '    If dgvData.Rows(i).Visible = False Then
                '            '        dgvData.Rows(i).Visible = True
                '            '    Else
                '            '        dgvData.Rows(i).Visible = False
                '            '    End If
                '            'Else
                '            '    Exit For
                '            'End If

                '        Else
                '            Exit For
                '        End If
                '    Next
            End If
            'S.SANDEEP [21 NOV 2015] -- END

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgvData_CellContentClick", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub dgvData_CellValueChanged(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvData.CellValueChanged
        Try
            If e.RowIndex <= -1 Then Exit Sub
            If e.ColumnIndex = dgcolhWeight.Index Then
                If CBool(dgvData.Rows(e.RowIndex).Cells(objdgcolhIsGrp.Index).Value) = False Then
                    mdtTran.Rows(e.RowIndex).Item("weight") = CDec(dgvData.Rows(e.RowIndex).Cells(dgcolhWeight.Index).Value)
                    'S.SANDEEP [09 FEB 2015] -- START
                    mDecAssignedWeight = CDec(mdtTran.Compute("SUM(weight)", "IsGrp = False"))

                    'S.SANDEEP [09 FEB 2015] -- END

                    'S.SANDEEP [29 JAN 2015] -- START
                    'If mDecAssignedWeight > 100 Then
                    '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Sorry you cannot assign more items. Reason the selected group exceeds the total weight of 100."), enMsgBoxStyle.Information)
                    '    mdtTran.Rows(e.RowIndex).Item("weight") = 0
                    '    Exit Sub
                    'Else
                    '    Call Evaluated_Data(CDec(mdtTran.Rows(e.RowIndex).Item("weight")), e.RowIndex)
                    'End If

                    If mDecAssignedWeight > txtWeight.Decimal Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Sorry you cannot assign more items. Reason the selected group exceeds the total weight of ") & txtWeight.Text, enMsgBoxStyle.Information)
                        mdtTran.Rows(e.RowIndex).Item("weight") = 0
                        Exit Sub
                    Else
                        Call Evaluated_Data(CDec(mdtTran.Rows(e.RowIndex).Item("weight")), e.RowIndex)
                    End If
                    'S.SANDEEP [29 JAN 2015] -- END
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgvData_CellValueChanged", mstrModuleName)
        Finally
        End Try
    End Sub

    'S.SANDEEP [29 JAN 2015] -- START
    Private Sub cboEmployee_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboEmployee.SelectedIndexChanged
        Try
            If ConfigParameter._Object._Self_Assign_Competencies = True Then
                Dim objAGroup As New clsassess_group_master
                Dim dt As DataTable = Nothing : Dim dsCombo As New DataSet
                Dim xGrpIds As String = String.Empty

                'S.SANDEEP [23 DEC 2015] -- START
                dsCombo = objAGroup.getListForCombo("List", True)
                'S.SANDEEP [23 DEC 2015] -- END

                If CInt(cboEmployee.SelectedValue) > 0 Then

                    'S.SANDEEP [04 JUN 2015] -- START
                    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                    'xGrpIds = objAGroup.GetCSV_AssessGroupIds(CInt(cboEmployee.SelectedValue))

                    'S.SANDEEP [19 DEC 2016] -- START
                    'xGrpIds = objAGroup.GetCSV_AssessGroupIds(CInt(cboEmployee.SelectedValue), eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate))
                    xGrpIds = objAGroup.GetCSV_AssessGroupIds(CInt(cboEmployee.SelectedValue), eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), CInt(cboPeriod.SelectedValue))
                    'S.SANDEEP [19 DEC 2016] -- END

                    'S.SANDEEP [04 JUN 2015] -- END

                    If xGrpIds.Trim.Length > 0 Then
                        xGrpIds &= ",0"
                    Else
                        xGrpIds = "0"
                    End If
                Else
                    xGrpIds = "0"
                End If
                dt = New DataView(dsCombo.Tables(0), "assessgroupunkid IN(" & xGrpIds & ")", "", DataViewRowState.CurrentRows).ToTable
                With cboGroup
                    .ValueMember = "assessgroupunkid"
                    .DisplayMember = "name"
                    .DataSource = dt
                    'S.SANDEEP [23 DEC 2015] -- START
                    '.SelectedValue = 0
                    If CInt(cboEmployee.SelectedValue) > 0 Then

                        'Shani(24-Feb-2016) -- Start
                        '.SelectedIndex = 1
                        If menAction <> enAction.EDIT_ONE Then
                            .SelectedIndex = 1
                        End If
                        'Shani(24-Feb-2016) -- End
                    Else
                        .SelectedIndex = 0
                    End If
                    'S.SANDEEP [23 DEC 2015] -- END
                End With
                objAGroup = Nothing
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboEmployee_SelectedIndexChanged", mstrModuleName)
        Finally
        End Try
    End Sub
    'Private Sub cboYear_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboYear.SelectedIndexChanged
    '    Try
    '        Dim dtCombo As DataTable
    '        Dim objPeriod As New clscommom_period_Tran
    '        Dim dsList As New DataSet
    '        If menAction <> enAction.EDIT_ONE Then
    '            dsList = objPeriod.getListForCombo(enModuleReference.Assessment, CInt(cboYear.SelectedValue), "List", True, enStatusType.Open)
    '            If CInt(cboYear.SelectedValue) <= 0 Then
    '                dtCombo = New DataView(dsList.Tables(0), "periodunkid IN(0)", "", DataViewRowState.CurrentRows).ToTable
    '            Else
    '                dtCombo = dsList.Tables(0).Copy
    '            End If
    '        Else
    '            dsList = objPeriod.getListForCombo(enModuleReference.Assessment, CInt(cboYear.SelectedValue), "List", True, enStatusType.Open)
    '            dtCombo = dsList.Tables(0).Copy
    '        End If
    '        With cboPeriod
    '            .ValueMember = "periodunkid"
    '            .DisplayMember = "name"
    '            .DataSource = dtCombo
    '            .SelectedValue = 0
    '        End With
    '    Catch ex As Exception
    '        DisplayError.Show("-1", ex.Message, "cboYear_SelectedIndexChanged", mstrModuleName)
    '    Finally
    '    End Try
    'End Sub

    'S.SANDEEP [29 JAN 2015] -- END

    Private Sub dgvData_CellEnter(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvData.CellEnter
        Try
            If e.ColumnIndex = dgcolhWeight.Index Then
                SendKeys.Send("{F2}")
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgvData_CellEnter", mstrModuleName)
        End Try
    End Sub

    Private Sub txtWeight_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtWeight.TextChanged
        Try

            'Shani(24-Feb-2016) -- Start
            '
            'If txtWeight.Decimal > 0 Then
            '    objdgcolhCheck.ReadOnly = False
            'Else
            '    objdgcolhCheck.ReadOnly = True
            'End If
            'Shani(24-Feb-2016) -- End
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "txtWeight_TextChanged", mstrModuleName)
        Finally
        End Try
    End Sub

    'S.SANDEEP [21 NOV 2015] -- START
    Private Sub lnkApplyAction_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lnkApplyAction.LinkClicked
        Try
            objFooter.Enabled = True
            dgvData.Enabled = True
            gbDescription.Visible = False
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lnkApplyAction_LinkClicked", mstrModuleName)
        Finally
        End Try
    End Sub
    'S.SANDEEP [21 NOV 2015] -- END

#End Region

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Private Sub OtherSettings()
        Try
            Me.SuspendLayout()
            
            Call SetLanguage()

            Me.gbAssignCompetencies.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbAssignCompetencies.ForeColor = GUI._eZeeContainerHeaderForeColor

            Me.gbDescription.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbDescription.ForeColor = GUI._eZeeContainerHeaderForeColor


            Me.btnSave.GradientBackColor = GUI._ButttonBackColor
            Me.btnSave.GradientForeColor = GUI._ButttonFontColor

            Me.btnClose.GradientBackColor = GUI._ButttonBackColor
            Me.btnClose.GradientForeColor = GUI._ButttonFontColor


            Me.ResumeLayout()
        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "OtherSettings", mstrModuleName)
        End Try
    End Sub


    Private Sub SetLanguage()
        Try
            Me.Text = Language._Object.getCaption(Me.Name, Me.Text)

            Me.btnSave.Text = Language._Object.getCaption(Me.btnSave.Name, Me.btnSave.Text)
            Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
            Me.lblPeriod.Text = Language._Object.getCaption(Me.lblPeriod.Name, Me.lblPeriod.Text)
            Me.lblGroupCode.Text = Language._Object.getCaption(Me.lblGroupCode.Name, Me.lblGroupCode.Text)
            Me.lblWeight.Text = Language._Object.getCaption(Me.lblWeight.Name, Me.lblWeight.Text)
            Me.gbAssignCompetencies.Text = Language._Object.getCaption(Me.gbAssignCompetencies.Name, Me.gbAssignCompetencies.Text)
            Me.DataGridViewTextBoxColumn1.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn1.Name, Me.DataGridViewTextBoxColumn1.HeaderText)
            Me.DataGridViewTextBoxColumn2.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn2.Name, Me.DataGridViewTextBoxColumn2.HeaderText)
            Me.DataGridViewTextBoxColumn3.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn3.Name, Me.DataGridViewTextBoxColumn3.HeaderText)
            Me.DataGridViewTextBoxColumn4.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn4.Name, Me.DataGridViewTextBoxColumn4.HeaderText)
            Me.DataGridViewTextBoxColumn5.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn5.Name, Me.DataGridViewTextBoxColumn5.HeaderText)
            Me.DataGridViewTextBoxColumn6.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn6.Name, Me.DataGridViewTextBoxColumn6.HeaderText)
            Me.DataGridViewTextBoxColumn7.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn7.Name, Me.DataGridViewTextBoxColumn7.HeaderText)
            Me.DataGridViewTextBoxColumn8.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn8.Name, Me.DataGridViewTextBoxColumn8.HeaderText)
            Me.lblEmployee.Text = Language._Object.getCaption(Me.lblEmployee.Name, Me.lblEmployee.Text)
            Me.DataGridViewTextBoxColumn9.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn9.Name, Me.DataGridViewTextBoxColumn9.HeaderText)
            Me.DataGridViewTextBoxColumn10.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn10.Name, Me.DataGridViewTextBoxColumn10.HeaderText)
            Me.DataGridViewTextBoxColumn11.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn11.Name, Me.DataGridViewTextBoxColumn11.HeaderText)
            Me.gbDescription.Text = Language._Object.getCaption(Me.gbDescription.Name, Me.gbDescription.Text)
            Me.lnkApplyAction.Text = Language._Object.getCaption(Me.lnkApplyAction.Name, Me.lnkApplyAction.Text)
            Me.EZeeGradientButton1.Text = Language._Object.getCaption(Me.EZeeGradientButton1.Name, Me.EZeeGradientButton1.Text)
            Me.lblComptenceGroup.Text = Language._Object.getCaption(Me.lblComptenceGroup.Name, Me.lblComptenceGroup.Text)
            Me.chkAssignByJobCompetencies.Text = Language._Object.getCaption(Me.chkAssignByJobCompetencies.Name, Me.chkAssignByJobCompetencies.Text)
            Me.dgcolhCompetencies.HeaderText = Language._Object.getCaption(Me.dgcolhCompetencies.Name, Me.dgcolhCompetencies.HeaderText)
            Me.dgcolhScale.HeaderText = Language._Object.getCaption(Me.dgcolhScale.Name, Me.dgcolhScale.HeaderText)
            Me.dgcolhWeight.HeaderText = Language._Object.getCaption(Me.dgcolhWeight.Name, Me.dgcolhWeight.HeaderText)
            Me.dgcolhLink.HeaderText = Language._Object.getCaption(Me.dgcolhLink.Name, Me.dgcolhLink.HeaderText)
            Me.chkZeroCompetenceWeight.Text = Language._Object.getCaption(Me.chkZeroCompetenceWeight.Name, Me.chkZeroCompetenceWeight.Text)

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
        End Try
    End Sub


    Private Sub SetMessages()
        Try
            Language.setMessage("clsassess_competence_assign_master", 1, "Sorry this group is already defined for the selected period. Please define new group.")
            Language.setMessage(mstrModuleName, 2, "Sorry, Assessment Group and Period are mandatory information. Please select Assessment Group and Period to continue.")
            Language.setMessage(mstrModuleName, 3, "Sorry, Weight is mandatory information. Please provide Weight first in order to assign competencies to assessment group.")
            Language.setMessage(mstrModuleName, 4, "Sorry you cannot assign more items. Reason the selected group exceeds the total weight of")
            Language.setMessage(mstrModuleName, 5, "Sorry, Weight assigned to competencies exceeds the total weight set for the selected group. Please provide the correct weight.")
            Language.setMessage(mstrModuleName, 6, "Sorry, Some of the assigned competencies does not have weight assigned. Please assigned the weight to those checked competencies.")
            Language.setMessage(mstrModuleName, 7, "Sorry, Employee is mandatory information. Please select employee to continue.")
            Language.setMessage(mstrModuleName, 8, "Sorry, No description set for the selected information.")
            Language.setMessage(mstrModuleName, 9, "Sorry, Competencies are mandatory information. Please check atleast one competence item in order to save.")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class