﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmCompetencies_AddEdit
    Inherits eZee.Common.eZeeForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmCompetencies_AddEdit))
        Me.pnlMain = New System.Windows.Forms.Panel
        Me.tabcCompetenciesInfo = New System.Windows.Forms.TabControl
        Me.tabpCompetenciesInfo = New System.Windows.Forms.TabPage
        Me.objbtnSearchGroup = New eZee.Common.eZeeGradientButton
        Me.objbtnAddGroup = New eZee.Common.eZeeGradientButton
        Me.lblComptenceGroup = New System.Windows.Forms.Label
        Me.cboComptenceGroup = New System.Windows.Forms.ComboBox
        Me.lblPeriod = New System.Windows.Forms.Label
        Me.cboPeriod = New System.Windows.Forms.ComboBox
        Me.objbtnOtherLanguage = New eZee.Common.eZeeGradientButton
        Me.tabcCompetencies = New System.Windows.Forms.TabControl
        Me.tabpName = New System.Windows.Forms.TabPage
        Me.txtName = New eZee.TextBox.AlphanumericTextBox
        Me.tabpDescription = New System.Windows.Forms.TabPage
        Me.txtDescription = New eZee.TextBox.AlphanumericTextBox
        Me.objbtnSearchScore = New eZee.Common.eZeeGradientButton
        Me.lblScore = New System.Windows.Forms.Label
        Me.cboScore = New System.Windows.Forms.ComboBox
        Me.objbtnSearchCategory = New eZee.Common.eZeeGradientButton
        Me.objbtnAddCategory = New eZee.Common.eZeeGradientButton
        Me.cboCategory = New System.Windows.Forms.ComboBox
        Me.lblCategory = New System.Windows.Forms.Label
        Me.tabpTrainingInfo = New System.Windows.Forms.TabPage
        Me.objbtnAdd = New eZee.Common.eZeeGradientButton
        Me.dgvData = New System.Windows.Forms.DataGridView
        Me.objdgcolhRemove = New System.Windows.Forms.DataGridViewImageColumn
        Me.dgcolhTrainingCourse = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgColhTranId = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhguid = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objbtnAddTrainingCourse = New eZee.Common.eZeeGradientButton
        Me.objbtnSearchTrainingCourse = New eZee.Common.eZeeGradientButton
        Me.lblTrainingCourse = New System.Windows.Forms.Label
        Me.cboTrainingCourse = New System.Windows.Forms.ComboBox
        Me.objFooter = New eZee.Common.eZeeFooter
        Me.btnSave = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnClose = New eZee.Common.eZeeLightButton(Me.components)
        Me.pnlMain.SuspendLayout()
        Me.tabcCompetenciesInfo.SuspendLayout()
        Me.tabpCompetenciesInfo.SuspendLayout()
        Me.tabcCompetencies.SuspendLayout()
        Me.tabpName.SuspendLayout()
        Me.tabpDescription.SuspendLayout()
        Me.tabpTrainingInfo.SuspendLayout()
        CType(Me.dgvData, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.objFooter.SuspendLayout()
        Me.SuspendLayout()
        '
        'pnlMain
        '
        Me.pnlMain.Controls.Add(Me.tabcCompetenciesInfo)
        Me.pnlMain.Controls.Add(Me.objFooter)
        Me.pnlMain.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pnlMain.Location = New System.Drawing.Point(0, 0)
        Me.pnlMain.Name = "pnlMain"
        Me.pnlMain.Size = New System.Drawing.Size(596, 381)
        Me.pnlMain.TabIndex = 0
        '
        'tabcCompetenciesInfo
        '
        Me.tabcCompetenciesInfo.Controls.Add(Me.tabpCompetenciesInfo)
        Me.tabcCompetenciesInfo.Controls.Add(Me.tabpTrainingInfo)
        Me.tabcCompetenciesInfo.Dock = System.Windows.Forms.DockStyle.Fill
        Me.tabcCompetenciesInfo.Location = New System.Drawing.Point(0, 0)
        Me.tabcCompetenciesInfo.Name = "tabcCompetenciesInfo"
        Me.tabcCompetenciesInfo.SelectedIndex = 0
        Me.tabcCompetenciesInfo.Size = New System.Drawing.Size(596, 326)
        Me.tabcCompetenciesInfo.TabIndex = 4
        '
        'tabpCompetenciesInfo
        '
        Me.tabpCompetenciesInfo.Controls.Add(Me.objbtnSearchGroup)
        Me.tabpCompetenciesInfo.Controls.Add(Me.objbtnAddGroup)
        Me.tabpCompetenciesInfo.Controls.Add(Me.lblComptenceGroup)
        Me.tabpCompetenciesInfo.Controls.Add(Me.cboComptenceGroup)
        Me.tabpCompetenciesInfo.Controls.Add(Me.lblPeriod)
        Me.tabpCompetenciesInfo.Controls.Add(Me.cboPeriod)
        Me.tabpCompetenciesInfo.Controls.Add(Me.objbtnOtherLanguage)
        Me.tabpCompetenciesInfo.Controls.Add(Me.tabcCompetencies)
        Me.tabpCompetenciesInfo.Controls.Add(Me.objbtnSearchScore)
        Me.tabpCompetenciesInfo.Controls.Add(Me.lblScore)
        Me.tabpCompetenciesInfo.Controls.Add(Me.cboScore)
        Me.tabpCompetenciesInfo.Controls.Add(Me.objbtnSearchCategory)
        Me.tabpCompetenciesInfo.Controls.Add(Me.objbtnAddCategory)
        Me.tabpCompetenciesInfo.Controls.Add(Me.cboCategory)
        Me.tabpCompetenciesInfo.Controls.Add(Me.lblCategory)
        Me.tabpCompetenciesInfo.Location = New System.Drawing.Point(4, 22)
        Me.tabpCompetenciesInfo.Name = "tabpCompetenciesInfo"
        Me.tabpCompetenciesInfo.Padding = New System.Windows.Forms.Padding(3)
        Me.tabpCompetenciesInfo.Size = New System.Drawing.Size(588, 300)
        Me.tabpCompetenciesInfo.TabIndex = 0
        Me.tabpCompetenciesInfo.Text = "Competencies Info"
        Me.tabpCompetenciesInfo.UseVisualStyleBackColor = True
        '
        'objbtnSearchGroup
        '
        Me.objbtnSearchGroup.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchGroup.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchGroup.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchGroup.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchGroup.BorderSelected = False
        Me.objbtnSearchGroup.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchGroup.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objbtnSearchGroup.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchGroup.Location = New System.Drawing.Point(549, 35)
        Me.objbtnSearchGroup.Name = "objbtnSearchGroup"
        Me.objbtnSearchGroup.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchGroup.TabIndex = 263
        '
        'objbtnAddGroup
        '
        Me.objbtnAddGroup.BackColor = System.Drawing.Color.Transparent
        Me.objbtnAddGroup.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnAddGroup.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnAddGroup.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnAddGroup.BorderSelected = False
        Me.objbtnAddGroup.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnAddGroup.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Add
        Me.objbtnAddGroup.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnAddGroup.Location = New System.Drawing.Point(522, 35)
        Me.objbtnAddGroup.Name = "objbtnAddGroup"
        Me.objbtnAddGroup.Size = New System.Drawing.Size(21, 21)
        Me.objbtnAddGroup.TabIndex = 262
        '
        'lblComptenceGroup
        '
        Me.lblComptenceGroup.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblComptenceGroup.Location = New System.Drawing.Point(10, 37)
        Me.lblComptenceGroup.Name = "lblComptenceGroup"
        Me.lblComptenceGroup.Size = New System.Drawing.Size(115, 17)
        Me.lblComptenceGroup.TabIndex = 261
        Me.lblComptenceGroup.Text = "Competence Group"
        Me.lblComptenceGroup.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboComptenceGroup
        '
        Me.cboComptenceGroup.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboComptenceGroup.DropDownWidth = 200
        Me.cboComptenceGroup.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboComptenceGroup.FormattingEnabled = True
        Me.cboComptenceGroup.Location = New System.Drawing.Point(131, 35)
        Me.cboComptenceGroup.Name = "cboComptenceGroup"
        Me.cboComptenceGroup.Size = New System.Drawing.Size(385, 21)
        Me.cboComptenceGroup.TabIndex = 260
        '
        'lblPeriod
        '
        Me.lblPeriod.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPeriod.Location = New System.Drawing.Point(10, 10)
        Me.lblPeriod.Name = "lblPeriod"
        Me.lblPeriod.Size = New System.Drawing.Size(115, 17)
        Me.lblPeriod.TabIndex = 258
        Me.lblPeriod.Text = "Period"
        Me.lblPeriod.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboPeriod
        '
        Me.cboPeriod.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboPeriod.DropDownWidth = 200
        Me.cboPeriod.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboPeriod.FormattingEnabled = True
        Me.cboPeriod.Location = New System.Drawing.Point(131, 8)
        Me.cboPeriod.Name = "cboPeriod"
        Me.cboPeriod.Size = New System.Drawing.Size(385, 21)
        Me.cboPeriod.TabIndex = 259
        '
        'objbtnOtherLanguage
        '
        Me.objbtnOtherLanguage.BackColor = System.Drawing.Color.Transparent
        Me.objbtnOtherLanguage.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnOtherLanguage.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnOtherLanguage.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnOtherLanguage.BorderSelected = False
        Me.objbtnOtherLanguage.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnOtherLanguage.Image = Global.Aruti.Main.My.Resources.Resources.OtherLanguage_16
        Me.objbtnOtherLanguage.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnOtherLanguage.Location = New System.Drawing.Point(522, 138)
        Me.objbtnOtherLanguage.Name = "objbtnOtherLanguage"
        Me.objbtnOtherLanguage.Size = New System.Drawing.Size(21, 21)
        Me.objbtnOtherLanguage.TabIndex = 257
        '
        'tabcCompetencies
        '
        Me.tabcCompetencies.Controls.Add(Me.tabpName)
        Me.tabcCompetencies.Controls.Add(Me.tabpDescription)
        Me.tabcCompetencies.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tabcCompetencies.Location = New System.Drawing.Point(8, 116)
        Me.tabcCompetencies.Name = "tabcCompetencies"
        Me.tabcCompetencies.SelectedIndex = 0
        Me.tabcCompetencies.Size = New System.Drawing.Size(508, 178)
        Me.tabcCompetencies.TabIndex = 256
        '
        'tabpName
        '
        Me.tabpName.Controls.Add(Me.txtName)
        Me.tabpName.Location = New System.Drawing.Point(4, 22)
        Me.tabpName.Name = "tabpName"
        Me.tabpName.Size = New System.Drawing.Size(500, 152)
        Me.tabpName.TabIndex = 0
        Me.tabpName.Text = "Competence"
        Me.tabpName.UseVisualStyleBackColor = True
        '
        'txtName
        '
        Me.txtName.BackColor = System.Drawing.SystemColors.Window
        Me.txtName.Dock = System.Windows.Forms.DockStyle.Fill
        Me.txtName.Flags = 0
        Me.txtName.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtName.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtName.Location = New System.Drawing.Point(0, 0)
        Me.txtName.Multiline = True
        Me.txtName.Name = "txtName"
        Me.txtName.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.txtName.Size = New System.Drawing.Size(500, 152)
        Me.txtName.TabIndex = 3
        '
        'tabpDescription
        '
        Me.tabpDescription.Controls.Add(Me.txtDescription)
        Me.tabpDescription.Location = New System.Drawing.Point(4, 22)
        Me.tabpDescription.Name = "tabpDescription"
        Me.tabpDescription.Size = New System.Drawing.Size(454, 107)
        Me.tabpDescription.TabIndex = 1
        Me.tabpDescription.Text = "Description"
        Me.tabpDescription.UseVisualStyleBackColor = True
        '
        'txtDescription
        '
        Me.txtDescription.BackColor = System.Drawing.SystemColors.Window
        Me.txtDescription.Dock = System.Windows.Forms.DockStyle.Fill
        Me.txtDescription.Flags = 0
        Me.txtDescription.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtDescription.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtDescription.Location = New System.Drawing.Point(0, 0)
        Me.txtDescription.Multiline = True
        Me.txtDescription.Name = "txtDescription"
        Me.txtDescription.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.txtDescription.Size = New System.Drawing.Size(454, 107)
        Me.txtDescription.TabIndex = 4
        '
        'objbtnSearchScore
        '
        Me.objbtnSearchScore.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchScore.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchScore.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchScore.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchScore.BorderSelected = False
        Me.objbtnSearchScore.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchScore.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objbtnSearchScore.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchScore.Location = New System.Drawing.Point(522, 89)
        Me.objbtnSearchScore.Name = "objbtnSearchScore"
        Me.objbtnSearchScore.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchScore.TabIndex = 255
        '
        'lblScore
        '
        Me.lblScore.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblScore.Location = New System.Drawing.Point(10, 91)
        Me.lblScore.Name = "lblScore"
        Me.lblScore.Size = New System.Drawing.Size(115, 17)
        Me.lblScore.TabIndex = 254
        Me.lblScore.Text = "Score/Scale"
        Me.lblScore.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboScore
        '
        Me.cboScore.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboScore.DropDownWidth = 450
        Me.cboScore.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboScore.FormattingEnabled = True
        Me.cboScore.Location = New System.Drawing.Point(131, 89)
        Me.cboScore.Name = "cboScore"
        Me.cboScore.Size = New System.Drawing.Size(385, 21)
        Me.cboScore.TabIndex = 253
        '
        'objbtnSearchCategory
        '
        Me.objbtnSearchCategory.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchCategory.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchCategory.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchCategory.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchCategory.BorderSelected = False
        Me.objbtnSearchCategory.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchCategory.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objbtnSearchCategory.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchCategory.Location = New System.Drawing.Point(549, 62)
        Me.objbtnSearchCategory.Name = "objbtnSearchCategory"
        Me.objbtnSearchCategory.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchCategory.TabIndex = 252
        '
        'objbtnAddCategory
        '
        Me.objbtnAddCategory.BackColor = System.Drawing.Color.Transparent
        Me.objbtnAddCategory.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnAddCategory.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnAddCategory.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnAddCategory.BorderSelected = False
        Me.objbtnAddCategory.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnAddCategory.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Add
        Me.objbtnAddCategory.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnAddCategory.Location = New System.Drawing.Point(522, 62)
        Me.objbtnAddCategory.Name = "objbtnAddCategory"
        Me.objbtnAddCategory.Size = New System.Drawing.Size(21, 21)
        Me.objbtnAddCategory.TabIndex = 251
        '
        'cboCategory
        '
        Me.cboCategory.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboCategory.DropDownWidth = 450
        Me.cboCategory.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboCategory.FormattingEnabled = True
        Me.cboCategory.Location = New System.Drawing.Point(131, 62)
        Me.cboCategory.Name = "cboCategory"
        Me.cboCategory.Size = New System.Drawing.Size(385, 21)
        Me.cboCategory.TabIndex = 249
        '
        'lblCategory
        '
        Me.lblCategory.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCategory.Location = New System.Drawing.Point(10, 64)
        Me.lblCategory.Name = "lblCategory"
        Me.lblCategory.Size = New System.Drawing.Size(115, 17)
        Me.lblCategory.TabIndex = 250
        Me.lblCategory.Text = "Competence Category"
        Me.lblCategory.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'tabpTrainingInfo
        '
        Me.tabpTrainingInfo.Controls.Add(Me.objbtnAdd)
        Me.tabpTrainingInfo.Controls.Add(Me.dgvData)
        Me.tabpTrainingInfo.Controls.Add(Me.objbtnAddTrainingCourse)
        Me.tabpTrainingInfo.Controls.Add(Me.objbtnSearchTrainingCourse)
        Me.tabpTrainingInfo.Controls.Add(Me.lblTrainingCourse)
        Me.tabpTrainingInfo.Controls.Add(Me.cboTrainingCourse)
        Me.tabpTrainingInfo.Location = New System.Drawing.Point(4, 22)
        Me.tabpTrainingInfo.Name = "tabpTrainingInfo"
        Me.tabpTrainingInfo.Padding = New System.Windows.Forms.Padding(3)
        Me.tabpTrainingInfo.Size = New System.Drawing.Size(588, 300)
        Me.tabpTrainingInfo.TabIndex = 1
        Me.tabpTrainingInfo.Text = "Add Training(s) Courses"
        Me.tabpTrainingInfo.UseVisualStyleBackColor = True
        '
        'objbtnAdd
        '
        Me.objbtnAdd.BackColor = System.Drawing.Color.Transparent
        Me.objbtnAdd.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnAdd.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnAdd.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnAdd.BorderSelected = False
        Me.objbtnAdd.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnAdd.Image = Global.Aruti.Main.My.Resources.Resources.Add_20
        Me.objbtnAdd.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnAdd.Location = New System.Drawing.Point(557, 8)
        Me.objbtnAdd.Name = "objbtnAdd"
        Me.objbtnAdd.Size = New System.Drawing.Size(21, 21)
        Me.objbtnAdd.TabIndex = 261
        '
        'dgvData
        '
        Me.dgvData.AllowUserToAddRows = False
        Me.dgvData.AllowUserToDeleteRows = False
        Me.dgvData.AllowUserToOrderColumns = True
        Me.dgvData.AllowUserToResizeColumns = False
        Me.dgvData.BackgroundColor = System.Drawing.Color.WhiteSmoke
        Me.dgvData.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.dgvData.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing
        Me.dgvData.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.objdgcolhRemove, Me.dgcolhTrainingCourse, Me.objdgColhTranId, Me.objdgcolhguid})
        Me.dgvData.Location = New System.Drawing.Point(13, 36)
        Me.dgvData.MultiSelect = False
        Me.dgvData.Name = "dgvData"
        Me.dgvData.RowHeadersVisible = False
        Me.dgvData.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing
        Me.dgvData.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvData.Size = New System.Drawing.Size(565, 258)
        Me.dgvData.TabIndex = 260
        '
        'objdgcolhRemove
        '
        Me.objdgcolhRemove.Frozen = True
        Me.objdgcolhRemove.HeaderText = ""
        Me.objdgcolhRemove.Image = Global.Aruti.Main.My.Resources.Resources.remove
        Me.objdgcolhRemove.Name = "objdgcolhRemove"
        Me.objdgcolhRemove.ReadOnly = True
        Me.objdgcolhRemove.Width = 25
        '
        'dgcolhTrainingCourse
        '
        Me.dgcolhTrainingCourse.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.dgcolhTrainingCourse.HeaderText = "Courses"
        Me.dgcolhTrainingCourse.Name = "dgcolhTrainingCourse"
        Me.dgcolhTrainingCourse.ReadOnly = True
        Me.dgcolhTrainingCourse.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgcolhTrainingCourse.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'objdgColhTranId
        '
        Me.objdgColhTranId.HeaderText = "objdgColhTranId"
        Me.objdgColhTranId.Name = "objdgColhTranId"
        Me.objdgColhTranId.ReadOnly = True
        Me.objdgColhTranId.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.objdgColhTranId.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.objdgColhTranId.Visible = False
        '
        'objdgcolhguid
        '
        Me.objdgcolhguid.HeaderText = "objdgcolhguid"
        Me.objdgcolhguid.Name = "objdgcolhguid"
        Me.objdgcolhguid.ReadOnly = True
        Me.objdgcolhguid.Visible = False
        '
        'objbtnAddTrainingCourse
        '
        Me.objbtnAddTrainingCourse.BackColor = System.Drawing.Color.Transparent
        Me.objbtnAddTrainingCourse.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnAddTrainingCourse.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnAddTrainingCourse.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnAddTrainingCourse.BorderSelected = False
        Me.objbtnAddTrainingCourse.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnAddTrainingCourse.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Add
        Me.objbtnAddTrainingCourse.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnAddTrainingCourse.Location = New System.Drawing.Point(483, 8)
        Me.objbtnAddTrainingCourse.Name = "objbtnAddTrainingCourse"
        Me.objbtnAddTrainingCourse.Size = New System.Drawing.Size(21, 21)
        Me.objbtnAddTrainingCourse.TabIndex = 259
        '
        'objbtnSearchTrainingCourse
        '
        Me.objbtnSearchTrainingCourse.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchTrainingCourse.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchTrainingCourse.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchTrainingCourse.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchTrainingCourse.BorderSelected = False
        Me.objbtnSearchTrainingCourse.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchTrainingCourse.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objbtnSearchTrainingCourse.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchTrainingCourse.Location = New System.Drawing.Point(510, 8)
        Me.objbtnSearchTrainingCourse.Name = "objbtnSearchTrainingCourse"
        Me.objbtnSearchTrainingCourse.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchTrainingCourse.TabIndex = 258
        '
        'lblTrainingCourse
        '
        Me.lblTrainingCourse.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTrainingCourse.Location = New System.Drawing.Point(10, 10)
        Me.lblTrainingCourse.Name = "lblTrainingCourse"
        Me.lblTrainingCourse.Size = New System.Drawing.Size(115, 17)
        Me.lblTrainingCourse.TabIndex = 257
        Me.lblTrainingCourse.Text = "Training Course"
        Me.lblTrainingCourse.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboTrainingCourse
        '
        Me.cboTrainingCourse.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboTrainingCourse.DropDownWidth = 450
        Me.cboTrainingCourse.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboTrainingCourse.FormattingEnabled = True
        Me.cboTrainingCourse.Location = New System.Drawing.Point(131, 8)
        Me.cboTrainingCourse.Name = "cboTrainingCourse"
        Me.cboTrainingCourse.Size = New System.Drawing.Size(346, 21)
        Me.cboTrainingCourse.TabIndex = 256
        '
        'objFooter
        '
        Me.objFooter.BorderColor = System.Drawing.Color.Silver
        Me.objFooter.Controls.Add(Me.btnSave)
        Me.objFooter.Controls.Add(Me.btnClose)
        Me.objFooter.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.objFooter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objFooter.GradiantStyle = eZee.Common.eZeeFooter.GradientStyle.Up
        Me.objFooter.GradientColor1 = System.Drawing.SystemColors.Control
        Me.objFooter.GradientColor2 = System.Drawing.SystemColors.Control
        Me.objFooter.Location = New System.Drawing.Point(0, 326)
        Me.objFooter.Name = "objFooter"
        Me.objFooter.Size = New System.Drawing.Size(596, 55)
        Me.objFooter.TabIndex = 2
        '
        'btnSave
        '
        Me.btnSave.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnSave.BackColor = System.Drawing.Color.White
        Me.btnSave.BackgroundImage = CType(resources.GetObject("btnSave.BackgroundImage"), System.Drawing.Image)
        Me.btnSave.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnSave.BorderColor = System.Drawing.Color.Empty
        Me.btnSave.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnSave.FlatAppearance.BorderSize = 0
        Me.btnSave.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnSave.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSave.ForeColor = System.Drawing.Color.Black
        Me.btnSave.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnSave.GradientForeColor = System.Drawing.Color.Black
        Me.btnSave.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSave.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnSave.Location = New System.Drawing.Point(384, 13)
        Me.btnSave.Name = "btnSave"
        Me.btnSave.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSave.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnSave.Size = New System.Drawing.Size(97, 30)
        Me.btnSave.TabIndex = 0
        Me.btnSave.Text = "&Save"
        Me.btnSave.UseVisualStyleBackColor = True
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnClose.BackColor = System.Drawing.Color.White
        Me.btnClose.BackgroundImage = CType(resources.GetObject("btnClose.BackgroundImage"), System.Drawing.Image)
        Me.btnClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnClose.BorderColor = System.Drawing.Color.Empty
        Me.btnClose.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnClose.FlatAppearance.BorderSize = 0
        Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClose.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.ForeColor = System.Drawing.Color.Black
        Me.btnClose.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnClose.GradientForeColor = System.Drawing.Color.Black
        Me.btnClose.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Location = New System.Drawing.Point(487, 13)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Size = New System.Drawing.Size(97, 30)
        Me.btnClose.TabIndex = 1
        Me.btnClose.Text = "&Close"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'frmCompetencies_AddEdit
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(596, 381)
        Me.Controls.Add(Me.pnlMain)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmCompetencies_AddEdit"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Add/Edit Competencies"
        Me.pnlMain.ResumeLayout(False)
        Me.tabcCompetenciesInfo.ResumeLayout(False)
        Me.tabpCompetenciesInfo.ResumeLayout(False)
        Me.tabcCompetencies.ResumeLayout(False)
        Me.tabpName.ResumeLayout(False)
        Me.tabpName.PerformLayout()
        Me.tabpDescription.ResumeLayout(False)
        Me.tabpDescription.PerformLayout()
        Me.tabpTrainingInfo.ResumeLayout(False)
        CType(Me.dgvData, System.ComponentModel.ISupportInitialize).EndInit()
        Me.objFooter.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents pnlMain As System.Windows.Forms.Panel
    Friend WithEvents objFooter As eZee.Common.eZeeFooter
    Friend WithEvents btnSave As eZee.Common.eZeeLightButton
    Friend WithEvents btnClose As eZee.Common.eZeeLightButton
    Friend WithEvents tabcCompetenciesInfo As System.Windows.Forms.TabControl
    Friend WithEvents tabpCompetenciesInfo As System.Windows.Forms.TabPage
    Friend WithEvents tabpTrainingInfo As System.Windows.Forms.TabPage
    Friend WithEvents objbtnSearchGroup As eZee.Common.eZeeGradientButton
    Friend WithEvents objbtnAddGroup As eZee.Common.eZeeGradientButton
    Friend WithEvents lblComptenceGroup As System.Windows.Forms.Label
    Friend WithEvents cboComptenceGroup As System.Windows.Forms.ComboBox
    Friend WithEvents lblPeriod As System.Windows.Forms.Label
    Friend WithEvents cboPeriod As System.Windows.Forms.ComboBox
    Friend WithEvents objbtnOtherLanguage As eZee.Common.eZeeGradientButton
    Friend WithEvents tabcCompetencies As System.Windows.Forms.TabControl
    Friend WithEvents tabpName As System.Windows.Forms.TabPage
    Friend WithEvents txtName As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents tabpDescription As System.Windows.Forms.TabPage
    Friend WithEvents txtDescription As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents objbtnSearchScore As eZee.Common.eZeeGradientButton
    Friend WithEvents lblScore As System.Windows.Forms.Label
    Friend WithEvents cboScore As System.Windows.Forms.ComboBox
    Friend WithEvents objbtnSearchCategory As eZee.Common.eZeeGradientButton
    Friend WithEvents objbtnAddCategory As eZee.Common.eZeeGradientButton
    Friend WithEvents cboCategory As System.Windows.Forms.ComboBox
    Friend WithEvents lblCategory As System.Windows.Forms.Label
    Friend WithEvents objbtnSearchTrainingCourse As eZee.Common.eZeeGradientButton
    Friend WithEvents lblTrainingCourse As System.Windows.Forms.Label
    Friend WithEvents cboTrainingCourse As System.Windows.Forms.ComboBox
    Friend WithEvents objbtnAddTrainingCourse As eZee.Common.eZeeGradientButton
    Friend WithEvents dgvData As System.Windows.Forms.DataGridView
    Friend WithEvents objbtnAdd As eZee.Common.eZeeGradientButton
    Friend WithEvents objdgcolhRemove As System.Windows.Forms.DataGridViewImageColumn
    Friend WithEvents dgcolhTrainingCourse As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgColhTranId As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhguid As System.Windows.Forms.DataGridViewTextBoxColumn
End Class
