﻿Option Strict On

#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data

#End Region

Public Class frmAssignedCompetenciesList

#Region " Private Variable "

    Private ReadOnly mstrModuleName As String = "frmAssignedCompetenciesList"
    Private objAssignedMaster As clsassess_competence_assign_master

#End Region

#Region " Private Methods "

    Private Sub FillCombo()
        Dim dsCombo As New DataSet
        Dim objAGroup As New clsassess_group_master
        Dim objPeriod As New clscommom_period_Tran
        Dim objCMaster As New clsCommon_Master
        'S.SANDEEP [29 JAN 2015] -- START
        Dim objEmployee As New clsEmployee_Master
        'S.SANDEEP [29 JAN 2015] -- END
        Try
            dsCombo = objAGroup.getListForCombo("List", True)
            With cboGroup
                .ValueMember = "assessgroupunkid"
                .DisplayMember = "name"
                .DataSource = dsCombo.Tables(0)
                .SelectedValue = 0
            End With
            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'dsCombo = objPeriod.getListForCombo(enModuleReference.Assessment, 0, "List", True, enStatusType.Open)

            'S.SANDEEP |02-MAR-2021| -- START
            'ISSUE/ENHANCEMENT : Competencies Period Changes
            'dsCombo = objPeriod.getListForCombo(enModuleReference.Assessment, 0, FinancialYear._Object._DatabaseName, FinancialYear._Object._Database_Start_Date, "List", True, enStatusType.Open)
            Dim blnIsCompetence As Boolean = False
            If ConfigParameter._Object._RunCompetenceAssessmentSeparately Then
                blnIsCompetence = True
            End If
            dsCombo = objPeriod.getListForCombo(enModuleReference.Assessment, 0, FinancialYear._Object._DatabaseName, FinancialYear._Object._Database_Start_Date, "List", True, enStatusType.Open, False, blnIsCompetence, CBool(IIf(blnIsCompetence = True, True, False)))            
            'S.SANDEEP |02-MAR-2021| -- END

            'Sohail (21 Aug 2015) -- End
            With cboPeriod
                .ValueMember = "periodunkid"
                .DisplayMember = "name"
                .DataSource = dsCombo.Tables(0)
                .SelectedValue = 0
            End With
            dsCombo = objCMaster.getComboList(clsCommon_Master.enCommonMaster.COMPETENCE_CATEGORIES, True, "List")
            With cboCCategory
                .ValueMember = "masterunkid"
                .DisplayMember = "name"
                .DataSource = dsCombo.Tables(0)
                .SelectedValue = 0
            End With
            With cboGroupBy
                .Items.Clear()
                .Items.Add(Language.getMessage(mstrModuleName, 6, "Assessment Group"))
                .Items.Add(Language.getMessage(mstrModuleName, 7, "Competence Category"))
                'S.SANDEEP [29 JAN 2015] -- START
                If ConfigParameter._Object._Self_Assign_Competencies = True Then
                    .Items.Add(Language.getMessage(mstrModuleName, 8, "Employee"))
                End If
                'S.SANDEEP [29 JAN 2015] -- END
                .SelectedIndex = 0
            End With

            'S.SANDEEP [29 JAN 2015] -- START

            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'If ConfigParameter._Object._IsIncludeInactiveEmp = False Then
            '    dsCombo = objEmployee.GetEmployeeList("List", True, , , , , , , , , , , , , eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate))
            'Else
            '    dsCombo = objEmployee.GetEmployeeList("List", True, )
            'End If
            dsCombo = objEmployee.GetEmployeeList(FinancialYear._Object._DatabaseName, _
                                                  User._Object._Userunkid, _
                                                  FinancialYear._Object._YearUnkid, _
                                                  Company._Object._Companyunkid, _
                                                  eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                  eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                  ConfigParameter._Object._UserAccessModeSetting, _
                                                  True, ConfigParameter._Object._IsIncludeInactiveEmp, "List", True)
            'S.SANDEEP [04 JUN 2015] -- END

            With cboEmployee
                .ValueMember = "employeeunkid"
                .DisplayMember = "employeename"
                .DataSource = dsCombo.Tables("List")
                .SelectedValue = 0
            End With
            'S.SANDEEP [29 JAN 2015] -- END

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub SetVisibility()
        Try
            'S.SANDEEP [28 MAY 2015] -- START
            'ENHANCEMENT : NEW PRIVILEGE (Performance Assessment)
            btnNew.Enabled = User._Object.Privilege._AllowtoAddAssignedCompetencies
            btnEdit.Enabled = User._Object.Privilege._AllowtoEditAssignedCompetencies
            btnDelete.Enabled = User._Object.Privilege._AllowtoDeleteAssignedCompetencies
            'S.SANDEEP [28 MAY 2015] -- END

            cboEmployee.Enabled = ConfigParameter._Object._Self_Assign_Competencies
            'S.SANDEEP [09 FEB 2015] -- START
            'objbtnSearch.Enabled = ConfigParameter._Object._Self_Assign_Competencies
            objbtnSearchEmployee.Enabled = ConfigParameter._Object._Self_Assign_Competencies
            'S.SANDEEP [09 FEB 2015] -- END
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetVisibility", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub Fill_List()
        Dim dsList As New DataSet
        Dim iSearch As String = String.Empty
        Dim dTable As DataTable = Nothing
        Try
            'S.SANDEEP [09 FEB 2015] -- START
            'If CInt(cboGroup.SelectedValue) <= 0 Or CInt(cboPeriod.SelectedValue) <= 0 Then
            '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Sorry, Group and Period are mandatory information. Please selecte Group and Period."), enMsgBoxStyle.Information)
            '    Exit Sub
            'End If
            If ConfigParameter._Object._Self_Assign_Competencies = True Then
                If CInt(cboEmployee.SelectedValue) <= 0 Or CInt(cboPeriod.SelectedValue) <= 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 9, "Sorry, Assessment Group and Period are mandatory information. Please select Assessment Group and Period to continue."), enMsgBoxStyle.Information)
                    Exit Sub
                End If
            Else
                If CInt(cboGroup.SelectedValue) <= 0 Or CInt(cboPeriod.SelectedValue) <= 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Sorry, Group and Period are mandatory information. Please selecte Group and Period."), enMsgBoxStyle.Information)
                    Exit Sub
                End If
            End If
            'S.SANDEEP [09 FEB 2015] -- END


            RemoveHandler lvAssignedCompetencies.ItemChecked, AddressOf lvAssignedCompetencies_ItemChecked
            RemoveHandler objChkAll.CheckedChanged, AddressOf objChkAll_CheckedChanged
            'S.SANDEEP [29 JAN 2015] -- START
            'dsList = objAssignedMaster.GetList("List", CInt(cboGroup.SelectedValue), CInt(cboPeriod.SelectedValue))

            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'dsList = objAssignedMaster.GetList("List", CInt(cboGroup.SelectedValue), CInt(cboPeriod.SelectedValue), CInt(cboEmployee.SelectedValue), ConfigParameter._Object._Self_Assign_Competencies)
            dsList = objAssignedMaster.GetList("List", eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), CInt(cboGroup.SelectedValue), CInt(cboPeriod.SelectedValue), CInt(cboEmployee.SelectedValue), ConfigParameter._Object._Self_Assign_Competencies)
            'S.SANDEEP [04 JUN 2015] -- END

            'S.SANDEEP [29 JAN 2015] -- END
            lvAssignedCompetencies.Items.Clear()

            If CInt(cboCCategory.SelectedValue) > 0 Then
                iSearch &= "AND masterunkid = '" & CInt(cboCCategory.SelectedValue) & "' "
            End If

            If CInt(cboCompetencies.SelectedValue) > 0 Then
                iSearch &= "AND competenciesunkid = '" & CInt(cboCompetencies.SelectedValue) & "' "
            End If

            If iSearch.Trim.Length > 0 Then
                iSearch = iSearch.Substring(3)
                dTable = New DataView(dsList.Tables(0), iSearch, "", DataViewRowState.CurrentRows).ToTable
            Else
                dTable = New DataView(dsList.Tables(0), "", "", DataViewRowState.CurrentRows).ToTable
            End If

            For Each dRow As DataRow In dTable.Rows
                Dim lvItem As New ListViewItem

                lvItem.Text = ""
                lvItem.SubItems.Add(dRow.Item("Period").ToString())
                lvItem.SubItems.Add(dRow.Item("competencies").ToString)
                lvItem.SubItems.Add(dRow.Item("assigned_weight").ToString)
                lvItem.SubItems.Add(dRow.Item("assigncompetencetranunkid").ToString)
                lvItem.SubItems.Add(dRow.Item("group_name").ToString)
                lvItem.SubItems.Add(dRow.Item("ccategory").ToString)
                'S.SANDEEP [29 JAN 2015] -- START
                If CInt(dRow.Item("statusid")) = enStatusType.Close Then
                    lvItem.ForeColor = Color.Gray
                End If
                lvItem.SubItems.Add(dRow.Item("empname").ToString)
                lvItem.SubItems.Add(dRow.Item("ename").ToString)
                If ConfigParameter._Object._Self_Assign_Competencies = True Then
                    'S.SANDEEP |10-AUG-2021| -- START
                    'If CBool(dRow.Item("isfinal")) = True Or CInt(dRow.Item("opstatusid")) = enObjective_Status.FINAL_SAVE Then
                    '    lvItem.ForeColor = Color.Blue
                    'ElseIf CInt(dRow.Item("opstatusid")) = enObjective_Status.SUBMIT_APPROVAL Then
                    '    lvItem.ForeColor = Color.Green
                    'End If
                    If CBool(dRow.Item("isfinal")) = True Or CInt(dRow.Item("opstatusid")) = enObjective_Status.FINAL_SAVE Then
                        lvItem.ForeColor = CType(IIf(ConfigParameter._Object._BSC_StatusColors.ContainsKey(enObjective_Status.FINAL_SAVE), Color.FromArgb(ConfigParameter._Object._BSC_StatusColors(enObjective_Status.FINAL_SAVE)), Color.Blue), Color)
                    ElseIf CInt(dRow.Item("opstatusid")) = enObjective_Status.SUBMIT_APPROVAL Then
                        lvItem.ForeColor = CType(IIf(ConfigParameter._Object._BSC_StatusColors.ContainsKey(enObjective_Status.SUBMIT_APPROVAL), Color.FromArgb(ConfigParameter._Object._BSC_StatusColors(enObjective_Status.SUBMIT_APPROVAL)), Color.Blue), Color)
                    End If
                    'S.SANDEEP |10-AUG-2021| -- END
                End If
                'S.SANDEEP [29 JAN 2015] -- END

                lvItem.Tag = dRow.Item("assigncompetenceunkid")

                lvAssignedCompetencies.Items.Add(lvItem)
            Next
            Select Case cboGroupBy.SelectedIndex
                Case 0  'ASSESSMENT GROUP
                    lvAssignedCompetencies.GroupingColumn = objcolhAGroup
                    lvAssignedCompetencies.SortBy(objcolhAGroup.Index, SortOrder.Ascending)
                Case 1  'COMPETENCE CATEGORY
                    lvAssignedCompetencies.GroupingColumn = objcolhCGroup
                    lvAssignedCompetencies.SortBy(objcolhCGroup.Index, SortOrder.Ascending)
                    'S.SANDEEP [29 JAN 2015] -- START
                Case 2  'EMPLOYEE WISE
                    lvAssignedCompetencies.GroupingColumn = objcolhEmployee
                    lvAssignedCompetencies.SortBy(objcolhEmployee.Index, SortOrder.Ascending)
                    'S.SANDEEP [29 JAN 2015] -- END
            End Select
            lvAssignedCompetencies.DisplayGroups(True)

            'S.SANDEEP [29 JAN 2015] -- START
            'If lvAssignedCompetencies.Items.Count > 4 Then
            '    colhCompetencies.Width = 470 - 20
            'Else
            '    colhCompetencies.Width = 470
            'End If
            Dim xWidth As Integer = 0
            If ConfigParameter._Object._Self_Assign_Competencies = False Then
                colhEmployee.Width = 0 : xWidth = 790
            Else
                xWidth = 600
            End If
            If lvAssignedCompetencies.Items.Count > 4 Then
                colhCompetencies.Width = xWidth - 20
            Else
                colhCompetencies.Width = xWidth
            End If
            'S.SANDEEP [29 JAN 2015] -- END



            AddHandler lvAssignedCompetencies.ItemChecked, AddressOf lvAssignedCompetencies_ItemChecked
            AddHandler objChkAll.CheckedChanged, AddressOf objChkAll_CheckedChanged

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Fill_List", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

#Region " Form's Event "

    Private Sub frmAssignedCompetenciesList_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        objAssignedMaster = New clsassess_competence_assign_master
        Try
            Call Set_Logo(Me, gApplicationType)
            Language.setLanguage(Me.Name)
            Call OtherSettings()
            Call FillCombo()
            Call SetVisibility()

            If lvAssignedCompetencies.Items.Count > 0 Then lvAssignedCompetencies.Items(0).Selected = True
            lvAssignedCompetencies.Select()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmAssignedCompetenciesList_Load", mstrModuleName)
        End Try
    End Sub

    Private Sub frmAssignedCompetenciesList_KeyUp(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles MyBase.KeyUp
        Try
            If e.KeyCode = Keys.Delete And lvAssignedCompetencies.Focused = True Then
                Call btnDelete.PerformClick()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmAssignedCompetenciesList_KeyUp", mstrModuleName)
        End Try
    End Sub

    Private Sub frmAssignedCompetenciesList_FormClosed(ByVal sender As System.Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles MyBase.FormClosed
        objAssignedMaster = Nothing
    End Sub

    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsassess_competence_assign_master.SetMessages()
            clsassess_competence_assign_tran.SetMessages()
            objfrm._Other_ModuleNames = "clsassess_competence_assign_master,clsassess_competence_assign_tran"
            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub

#End Region

#Region " Button's Event "

    Private Sub btnNew_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNew.Click
        Dim frm As New frmAssignCompetencies
        Try
            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If
            If frm.displayDialog(-1, enAction.ADD_CONTINUE) Then
                Call Fill_List()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnNew_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub btnEdit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEdit.Click
        Dim frm As New frmAssignCompetencies
        Try
            If lvAssignedCompetencies.SelectedItems.Count <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Please select Assigned Competence from the list to perform further operation."), enMsgBoxStyle.Information) '?1
                lvAssignedCompetencies.Select()
                Exit Sub
            End If

            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If

            If frm.displayDialog(CInt(lvAssignedCompetencies.SelectedItems(0).Tag), enAction.EDIT_ONE) Then
                Call Fill_List()
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnEdit_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub btnDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDelete.Click
        Try
            If lvAssignedCompetencies.CheckedItems.Count <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Please check at-least one Assigned Competence from the list to perform delete operation."), enMsgBoxStyle.Information) '?1
                lvAssignedCompetencies.Select()
                Exit Sub
            End If
            Dim iVoidReason As String = ""
            Dim iMsg As String = String.Empty
            If lvAssignedCompetencies.Items.Count = lvAssignedCompetencies.CheckedItems.Count Then
                iMsg = Language.getMessage(mstrModuleName, 4, "Are you sure you want to delete all items assigned to this group?")
                If eZeeMsgBox.Show(iMsg, CType(enMsgBoxStyle.Question + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.Yes Then
                    Dim frm As New frmReasonSelection
                    If User._Object._Isrighttoleft = True Then
                        frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                        frm.RightToLeftLayout = True
                        Call Language.ctlRightToLeftlayOut(frm)
                    End If
                    frm.displayDialog(enVoidCategoryType.ASSESSMENT, iVoidReason)
                    If iVoidReason.Length <= 0 Then
                        Exit Sub
                    End If
                    objAssignedMaster._Isvoid = True
                    objAssignedMaster._Voiddatetime = ConfigParameter._Object._CurrentDateAndTime
                    objAssignedMaster._Voidreason = iVoidReason
                    objAssignedMaster._Voiduserunkid = User._Object._Userunkid

                    'S.SANDEEP [28-May-2018] -- START
                    'ISSUE/ENHANCEMENT : {Audit Trails} 
                    objAssignedMaster._FormName = mstrModuleName
                    objAssignedMaster._LoginEmployeeunkid = 0
                    objAssignedMaster._ClientIP = getIP()
                    objAssignedMaster._HostName = getHostName()
                    objAssignedMaster._FromWeb = False
                    objAssignedMaster._AuditUserId = User._Object._Userunkid
objAssignedMaster._CompanyUnkid = Company._Object._Companyunkid
                    objAssignedMaster._AuditDate = ConfigParameter._Object._CurrentDateAndTime
                    'S.SANDEEP [28-May-2018] -- END

                    'S.SANDEEP [29 JAN 2015] -- START
                    'objAssignedMaster.Delete(CInt(lvAssignedCompetencies.CheckedItems(0).Tag))
                    If objAssignedMaster.Delete(CInt(lvAssignedCompetencies.CheckedItems(0).Tag)) = False Then
                        If objAssignedMaster._Message <> "" Then
                            eZeeMsgBox.Show(objAssignedMaster._Message, enMsgBoxStyle.Information)
                            Exit Sub
                        End If
                        'S.SANDEEP [29 JAN 2015] -- END
                    End If
                End If
            Else
                iMsg = Language.getMessage(mstrModuleName, 5, "Are you sure you want to delete all checked items?")
                If eZeeMsgBox.Show(iMsg, CType(enMsgBoxStyle.Question + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.Yes Then

                    Dim frm As New frmReasonSelection
                    If User._Object._Isrighttoleft = True Then
                        frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                        frm.RightToLeftLayout = True
                        Call Language.ctlRightToLeftlayOut(frm)
                    End If
                    frm.displayDialog(enVoidCategoryType.ASSESSMENT, iVoidReason)
                    If iVoidReason.Length <= 0 Then
                        Exit Sub
                    End If
                    Dim objAssignTran As New clsassess_competence_assign_tran

                    With objAssignTran
                        ._FormName = mstrModuleName
                        ._LoginEmployeeunkid = 0
                        ._ClientIP = getIP()
                        ._HostName = getHostName()
                        ._FromWeb = False
                        ._AuditUserId = User._Object._Userunkid
._CompanyUnkid = Company._Object._Companyunkid
                        ._AuditDate = ConfigParameter._Object._CurrentDateAndTime
                    End With

                    For Each lvItem As ListViewItem In lvAssignedCompetencies.CheckedItems
                        'S.SANDEEP [22 Jan 2016] -- START
                        If objAssignTran.Delete(CInt(lvItem.SubItems(objcolhAssingTranId.Index).Text), True, User._Object._Userunkid, iVoidReason, ConfigParameter._Object._CurrentDateAndTime, ConfigParameter._Object._Self_Assign_Competencies) = False Then
                            'If objAssignTran.Delete(CInt(lvItem.SubItems(objcolhAssingTranId.Index).Text), True, User._Object._Userunkid, iVoidReason, ConfigParameter._Object._CurrentDateAndTime) = False Then
                            'S.SANDEEP [22 Jan 2016] -- END

                            'S.SANDEEP [29 JAN 2015] -- START
                            'Exit For
                            If objAssignTran._Message <> "" Then
                                eZeeMsgBox.Show(objAssignTran._Message, enMsgBoxStyle.Information)
                                Exit Sub
                            End If
                            'S.SANDEEP [29 JAN 2015] -- END

                        End If
                    Next
                    objAssignTran = Nothing
                End If
            End If
            Call Fill_List()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnDelete_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnClose_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub objbtnSearchGroup_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchGroup.Click
        Dim frm As New frmCommonSearch
        Try

            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If

            With frm
                .ValueMember = cboGroup.ValueMember
                .DisplayMember = cboGroup.DisplayMember
                .DataSource = CType(cboGroup.DataSource, DataTable)
                If .DisplayDialog Then
                    cboGroup.SelectedValue = .SelectedValue
                    cboGroup.Focus()
                End If
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchGroup_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    'S.SANDEEP [29 JAN 2015] -- START
    Private Sub objbtnSearchEmployee_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchEmployee.Click
        Dim frm As New frmCommonSearch
        Try
            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If

            With frm
                .ValueMember = cboEmployee.ValueMember
                .DisplayMember = cboEmployee.DisplayMember
                .CodeMember = "employeecode"
                .DataSource = CType(cboEmployee.DataSource, DataTable)
                If .DisplayDialog Then
                    cboEmployee.SelectedValue = .SelectedValue
                    cboEmployee.Focus()
                End If
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchEmployee_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub
    'S.SANDEEP [29 JAN 2015] -- END

    Private Sub objbtnSearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearch.Click
        Try
            Call Fill_List()
            'Call objbtnSearch.ShowResult(CStr(lvAssignedCompetencies.Items.Count))
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub objbtnReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnReset.Click
        Try
            cboGroup.SelectedValue = 0 : cboPeriod.SelectedValue = 0
            lvAssignedCompetencies.Items.Clear()
            'Call objbtnSearch.ShowResult(CStr(lvAssignedCompetencies.Items.Count))
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnReset_Click", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

#Region " Control's Events "

    Private Sub objChkAll_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objChkAll.CheckedChanged
        Try
            RemoveHandler lvAssignedCompetencies.ItemChecked, AddressOf lvAssignedCompetencies_ItemChecked
            For Each item As ListViewItem In lvAssignedCompetencies.Items
                item.Checked = CBool(objChkAll.CheckState)
            Next
            AddHandler lvAssignedCompetencies.ItemChecked, AddressOf lvAssignedCompetencies_ItemChecked
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objChkAll_CheckedChanged", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub lvAssignedCompetencies_ItemChecked(ByVal sender As Object, ByVal e As System.Windows.Forms.ItemCheckedEventArgs) Handles lvAssignedCompetencies.ItemChecked
        Try
            RemoveHandler objChkAll.CheckedChanged, AddressOf objChkAll_CheckedChanged
            If lvAssignedCompetencies.CheckedItems.Count <= 0 Then
                objChkAll.CheckState = CheckState.Unchecked
            ElseIf lvAssignedCompetencies.CheckedItems.Count < lvAssignedCompetencies.Items.Count Then
                objChkAll.CheckState = CheckState.Indeterminate
            ElseIf lvAssignedCompetencies.CheckedItems.Count = lvAssignedCompetencies.Items.Count Then
                objChkAll.CheckState = CheckState.Checked
            End If
            AddHandler objChkAll.CheckedChanged, AddressOf objChkAll_CheckedChanged
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lvAssignedCompetencies_ItemChecked", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub cboCCategory_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboCCategory.SelectedIndexChanged
        Try
            Dim dsList As New DataSet
            Dim objCompetencies As New clsassess_competencies_master
            dsList = objCompetencies.getComboList(CInt(cboCCategory.SelectedValue), CInt(cboPeriod.SelectedValue), True, "List")
            With cboCompetencies
                .ValueMember = "Id"
                .DisplayMember = "Name"
                .DataSource = dsList.Tables(0)
                .SelectedValue = 0
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboCCategory_SelectedIndexChanged", mstrModuleName)
        Finally
        End Try
    End Sub

    'S.SANDEEP [29 JAN 2015] -- START
    Private Sub lvAssignedCompetencies_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lvAssignedCompetencies.SelectedIndexChanged
        Try
            If lvAssignedCompetencies.SelectedItems.Count > 0 Then
                Select Case lvAssignedCompetencies.SelectedItems(0).ForeColor
                    Case Color.Blue, Color.Green, Color.Gray
                        btnEdit.Enabled = False : btnDelete.Enabled = False
                End Select
            Else
                btnEdit.Enabled = True : btnDelete.Enabled = True
            End If

            'Varsha Rana (17-Oct-2017) -- Start
            'Enhancement - Give user privileges.
            Call SetVisibility()
            'Varsha Rana (17-Oct-2017) -- End

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lvAssignedCompetencies_SelectedIndexChanged", mstrModuleName)
        Finally
        End Try
    End Sub
    'S.SANDEEP [29 JAN 2015] -- END

#End Region

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Private Sub OtherSettings()
        Try
            Me.SuspendLayout()
            
            Call SetLanguage()

            Me.gbFilterCriteria.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbFilterCriteria.ForeColor = GUI._eZeeContainerHeaderForeColor


           
            Me.eZeeHeader.GradientColor1 = GUI._HeaderBackColor1
            Me.eZeeHeader.GradientColor2 = GUI._HeaderBackColor2
            Me.eZeeHeader.BorderColor = GUI._HeaderBorderColor
            Me.eZeeHeader.HeaderTextForeColor = GUI._HeaderTitleFontColor
            Me.eZeeHeader.DescriptionForeColor = GUI._HeaderMessageFontColor


            Me.btnDelete.GradientBackColor = GUI._ButttonBackColor
            Me.btnDelete.GradientForeColor = GUI._ButttonFontColor

            Me.btnEdit.GradientBackColor = GUI._ButttonBackColor
            Me.btnEdit.GradientForeColor = GUI._ButttonFontColor

            Me.btnNew.GradientBackColor = GUI._ButttonBackColor
            Me.btnNew.GradientForeColor = GUI._ButttonFontColor

            Me.btnClose.GradientBackColor = GUI._ButttonBackColor
            Me.btnClose.GradientForeColor = GUI._ButttonFontColor


            Me.ResumeLayout()
        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "OtherSettings", mstrModuleName)
        End Try
    End Sub


    Private Sub SetLanguage()
        Try
            Me.Text = Language._Object.getCaption(Me.Name, Me.Text)

            Me.btnDelete.Text = Language._Object.getCaption(Me.btnDelete.Name, Me.btnDelete.Text)
            Me.btnEdit.Text = Language._Object.getCaption(Me.btnEdit.Name, Me.btnEdit.Text)
            Me.btnNew.Text = Language._Object.getCaption(Me.btnNew.Name, Me.btnNew.Text)
            Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
            Me.eZeeHeader.Title = Language._Object.getCaption(Me.eZeeHeader.Name & "_Title", Me.eZeeHeader.Title)
            Me.eZeeHeader.Message = Language._Object.getCaption(Me.eZeeHeader.Name & "_Message", Me.eZeeHeader.Message)
            Me.gbFilterCriteria.Text = Language._Object.getCaption(Me.gbFilterCriteria.Name, Me.gbFilterCriteria.Text)
            Me.lblGroupCode.Text = Language._Object.getCaption(Me.lblGroupCode.Name, Me.lblGroupCode.Text)
            Me.lblPeriod.Text = Language._Object.getCaption(Me.lblPeriod.Name, Me.lblPeriod.Text)
            Me.colhPeriod.Text = Language._Object.getCaption(CStr(Me.colhPeriod.Tag), Me.colhPeriod.Text)
            Me.colhCompetencies.Text = Language._Object.getCaption(CStr(Me.colhCompetencies.Tag), Me.colhCompetencies.Text)
            Me.colhWeight.Text = Language._Object.getCaption(CStr(Me.colhWeight.Tag), Me.colhWeight.Text)
            Me.lblCCategory.Text = Language._Object.getCaption(Me.lblCCategory.Name, Me.lblCCategory.Text)
            Me.lblCompetencies.Text = Language._Object.getCaption(Me.lblCompetencies.Name, Me.lblCompetencies.Text)
            Me.lblGroupBy.Text = Language._Object.getCaption(Me.lblGroupBy.Name, Me.lblGroupBy.Text)
            Me.lblEmployee.Text = Language._Object.getCaption(Me.lblEmployee.Name, Me.lblEmployee.Text)
            Me.colhEmployee.Text = Language._Object.getCaption(CStr(Me.colhEmployee.Tag), Me.colhEmployee.Text)

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
        End Try
    End Sub


    Private Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Sorry, Group and Period are mandatory information. Please selecte Group and Period.")
            Language.setMessage(mstrModuleName, 2, "Please select Assigned Competence from the list to perform further operation.")
            Language.setMessage(mstrModuleName, 3, "Please check at-least one Assigned Competence from the list to perform delete operation.")
            Language.setMessage(mstrModuleName, 4, "Are you sure you want to delete all items assigned to this group?")
            Language.setMessage(mstrModuleName, 5, "Are you sure you want to delete all checked items?")
            Language.setMessage(mstrModuleName, 6, "Assessment Group")
            Language.setMessage(mstrModuleName, 7, "Competence Category")
            Language.setMessage(mstrModuleName, 8, "Employee")
            Language.setMessage(mstrModuleName, 9, "Sorry, Assessment Group and Period are mandatory information. Please select Assessment Group and Period to continue.")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class