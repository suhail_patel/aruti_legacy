﻿Option Strict On

#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data

#End Region

Public Class frmCompetenciesList

#Region " Private Variable "

    Private ReadOnly mstrModuleName As String = "frmCompetenciesList"
    Private objCompetencies As clsassess_competencies_master

#End Region

#Region " Private Methods "

    Private Sub FillCombo()
        Dim objCMaster As New clsCommon_Master
        Dim objCPeriod As New clscommom_period_Tran
        Dim dsCombo As New DataSet
        Try
            dsCombo = objCMaster.getComboList(clsCommon_Master.enCommonMaster.COMPETENCE_CATEGORIES, True, "List")
            With cboCategory
                .ValueMember = "masterunkid"
                .DisplayMember = "name"
                .DataSource = dsCombo.Tables(0)
                .SelectedValue = 0
            End With
            dsCombo = objCMaster.getComboList(clsCommon_Master.enCommonMaster.ASSESSMENT_SCALE_GROUP, True, "List")
            With cboScore
                .ValueMember = "masterunkid"
                .DisplayMember = "name"
                .DataSource = dsCombo.Tables(0)
                .SelectedValue = 0
            End With
            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'dsCombo = objCPeriod.getListForCombo(enModuleReference.Assessment, 0, "List", True, enStatusType.Open)

            'S.SANDEEP |02-MAR-2021| -- START
            'ISSUE/ENHANCEMENT : Competencies Period Changes
            'dsCombo = objCPeriod.getListForCombo(enModuleReference.Assessment, 0, FinancialYear._Object._DatabaseName, FinancialYear._Object._Database_Start_Date, "List", True, enStatusType.Open)
            Dim blnIsCompetence As Boolean = False
            If ConfigParameter._Object._RunCompetenceAssessmentSeparately Then
                blnIsCompetence = True
            End If            
            dsCombo = objCPeriod.getListForCombo(enModuleReference.Assessment, 0, FinancialYear._Object._DatabaseName, FinancialYear._Object._Database_Start_Date, "List", True, enStatusType.Open, False, blnIsCompetence, CBool(IIf(blnIsCompetence = True, True, False)))
            'S.SANDEEP |02-MAR-2021| -- END

            'Sohail (21 Aug 2015) -- End
            With cboPeriod
                .ValueMember = "periodunkid"
                .DisplayMember = "name"
                .DataSource = dsCombo.Tables(0)
                .SelectedValue = 0
            End With

            'S.SANDEEP [21 NOV 2015] -- START
            dsCombo = objCMaster.getComboList(clsCommon_Master.enCommonMaster.COMPETENCE_GROUP, True, "List")
            With cboComptenceGroup
                .ValueMember = "masterunkid"
                .DisplayMember = "name"
                .DataSource = dsCombo.Tables(0)
                .SelectedValue = 0
            End With

            With cboGroupBy
                .Items.Clear()
                .Items.Add(Language.getMessage(mstrModuleName, 4, "Competence Category"))
                .Items.Add(Language.getMessage(mstrModuleName, 5, "Competence Group"))
                .SelectedIndex = 0
            End With
            'S.SANDEEP [21 NOV 2015] -- END

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        Finally
            objCMaster = Nothing
        End Try
    End Sub

    Private Sub SetVisibility()
        Try
            'S.SANDEEP [28 MAY 2015] -- START
            'ENHANCEMENT : NEW PRIVILEGE (Performance Assessment)
            btnNew.Enabled = User._Object.Privilege._AllowtoAddCompetencies
            btnEdit.Enabled = User._Object.Privilege._AllowtoEditCompetencies
            btnDelete.Enabled = User._Object.Privilege._AllowtoDeleteCompetencies
            btnLoadLibrary.Enabled = User._Object.Privilege._AllowtoLoadfromCompetenciesLibrary
            'S.SANDEEP [28 MAY 2015] -- END
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetVisibility", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub Fill_List()
        Dim iSearch As String = String.Empty
        Dim mdtTable As DataTable
        Dim dsList As New DataSet
        Try
            dsList = objCompetencies.GetList("List")
            lvCompetencies.Items.Clear()

            If CInt(cboCategory.SelectedValue) > 0 Then
                iSearch &= "AND competence_categoryunkid = '" & CInt(cboCategory.SelectedValue) & "' "
            End If

            If CInt(cboScore.SelectedValue) > 0 Then
                iSearch &= "AND scalemasterunkid = '" & CInt(cboScore.SelectedValue) & "' "
            End If

            If CInt(cboPeriod.SelectedValue) > 0 Then
                iSearch &= "AND periodunkid = '" & CInt(cboPeriod.SelectedValue) & "' "
            End If

            'S.SANDEEP [21 NOV 2015] -- START
            If CInt(cboComptenceGroup.SelectedValue) > 0 Then
                'S.SANDEEP [29 DEC 2015] -- START
                'iSearch &= "AND competencegroupunkid = '" & CInt(cboGroupBy.SelectedValue) & "' "
                iSearch &= "AND competencegroupunkid = '" & CInt(cboComptenceGroup.SelectedValue) & "' "
                'S.SANDEEP [29 DEC 2015] -- END
            End If

            Dim strSortColName As String = "CCategory"
            Select Case cboGroupBy.SelectedIndex
                Case 0 : strSortColName = "CCategory"
                Case 1 : strSortColName = "competence_group"
            End Select
            'S.SANDEEP [21 NOV 2015] -- END


            If iSearch.Trim.Length > 0 Then
                iSearch = iSearch.Substring(3)
                mdtTable = New DataView(dsList.Tables(0), iSearch, strSortColName, DataViewRowState.CurrentRows).ToTable
            Else
                mdtTable = New DataView(dsList.Tables(0), "", strSortColName, DataViewRowState.CurrentRows).ToTable
            End If

            For Each dRow As DataRow In mdtTable.Rows
                Dim lvItem As New ListViewItem

                lvItem.Text = dRow.Item("CCategory").ToString

                lvItem.SubItems.Add(dRow.Item("Score").ToString)
                lvItem.SubItems.Add(dRow.Item("name").ToString)
                lvItem.SubItems.Add(dRow.Item("description").ToString)
                lvItem.SubItems.Add(dRow.Item("pname").ToString)

                'S.SANDEEP [21 NOV 2015] -- START
                lvItem.SubItems.Add(dRow.Item("competence_group").ToString)
                'S.SANDEEP [21 NOV 2015] -- END

                lvItem.Tag = dRow.Item("competenciesunkid").ToString

                lvCompetencies.Items.Add(lvItem)
            Next

            'S.SANDEEP [21 NOV 2015] -- START
            'lvCompetencies.GroupingColumn = objcolhCCategory
            Select Case cboGroupBy.SelectedIndex
                Case 0  'CATEGORY
                    lvCompetencies.GroupingColumn = colhCCategory
                    colhCGroup.Width = 110
                    colhCCategory.Width = 0
                Case 1  'GROUP
                    lvCompetencies.GroupingColumn = colhCGroup
                    colhCCategory.Width = 110
                    colhCGroup.Width = 0
            End Select
            'S.SANDEEP [21 NOV 2015] -- END

            lvCompetencies.DisplayGroups(True)

            If lvCompetencies.Items.Count >= 2 Then
                colhDescription.Width = 140 - 20
            Else
                colhDescription.Width = 140
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Fill_List", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

#Region " Form's Event "

    Private Sub frmCompetenciesList_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        objCompetencies = New clsassess_competencies_master
        Try
            Call Set_Logo(Me, gApplicationType)
            Language.setLanguage(Me.Name)
            Call OtherSettings()
            FillCombo()
            Call SetVisibility()

            If lvCompetencies.Items.Count > 0 Then lvCompetencies.Items(0).Selected = True
            lvCompetencies.Select()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmCompetenciesList_Load", mstrModuleName)
        End Try
    End Sub

    Private Sub frmCompetenciesList_KeyUp(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles MyBase.KeyUp
        Try
            If e.KeyCode = Keys.Delete And lvCompetencies.Focused = True Then
                Call btnDelete.PerformClick()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmCompetenciesList_KeyUp", mstrModuleName)
        End Try
    End Sub

    Private Sub frmCompetenciesList_FormClosed(ByVal sender As System.Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles MyBase.FormClosed
        objCompetencies = Nothing
    End Sub

    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsassess_competencies_master.SetMessages()
            objfrm._Other_ModuleNames = "clsassess_competencies_master"
            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub

#End Region

#Region " Button's Event "

    Private Sub btnNew_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNew.Click
        Dim frm As New frmCompetencies_AddEdit
        Try
            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If
            If frm.displayDialog(-1, enAction.ADD_CONTINUE) Then
                Call Fill_List()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnNew_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub btnEdit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEdit.Click
        Dim frm As New frmCompetencies_AddEdit
        Try
            If lvCompetencies.SelectedItems.Count <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please select Competence from the list to perform further operation on it."), enMsgBoxStyle.Information) '?1
                lvCompetencies.Select()
                Exit Sub
            End If

            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If
            If frm.displayDialog(CInt(lvCompetencies.SelectedItems(0).Tag), enAction.EDIT_ONE) Then
                Call Fill_List()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnEdit_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub btnDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDelete.Click
        Try
            If lvCompetencies.SelectedItems.Count <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please select Competence from the list to perform further operation on it."), enMsgBoxStyle.Information) '?1
                lvCompetencies.Select()
                Exit Sub
            End If

            If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Are you sure you want to delete this Competence Item?"), CType(enMsgBoxStyle.Question + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.Yes Then

                'S.SANDEEP [28-May-2018] -- START
                'ISSUE/ENHANCEMENT : {Audit Trails} 
                objCompetencies._FormName = mstrModuleName
                objCompetencies._LoginEmployeeunkid = 0
                objCompetencies._ClientIP = getIP()
                objCompetencies._HostName = getHostName()
                objCompetencies._FromWeb = False
                objCompetencies._AuditUserId = User._Object._Userunkid
objCompetencies._CompanyUnkid = Company._Object._Companyunkid
                objCompetencies._AuditDate = ConfigParameter._Object._CurrentDateAndTime
                'S.SANDEEP [28-May-2018] -- END

                'S.SANDEEP |29-MAR-2021| -- START
                'ISSUE/ENHANCEMENT : Competencies Training
                'If objCompetencies.Delete(CInt(lvCompetencies.SelectedItems(0).Tag)) = True Then
                Dim objCmptTraining As New clsassess_competencies_trainingcourses_tran
                Dim mdtCourses As DataTable

                objCmptTraining._Competenciesunkid = CInt(lvCompetencies.SelectedItems(0).Tag)
                mdtCourses = objCmptTraining._DataTable.Copy
                objCmptTraining = Nothing
                If mdtCourses IsNot Nothing AndAlso mdtCourses.Rows.Count > 0 Then
                    Dim iVoidReason As String = ""
                    Dim frm As New frmReasonSelection
                    If User._Object._Isrighttoleft = True Then
                        frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                        frm.RightToLeftLayout = True
                        Call Language.ctlRightToLeftlayOut(frm)
                    End If
                    frm.displayDialog(enVoidCategoryType.ASSESSMENT, iVoidReason)
                    If iVoidReason.Length <= 0 Then
                        Exit Sub
                    End If
                    For Each xR As DataRow In mdtCourses.Rows
                        xR("isvoid") = True
                        xR("voiduserunkid") = User._Object._Userunkid
                        xR("voiddatetime") = ConfigParameter._Object._CurrentDateAndTime
                        xR("voidreason") = iVoidReason
                        xR("AUD") = "D"
                        xR("auditdatetime") = ConfigParameter._Object._CurrentDateAndTime
                        xR("audituserunkid") = User._Object._Userunkid
                        xR("audittype") = enAuditType.DELETE
                        xR("ip") = getIP()
                        xR("host") = getHostName()
                        xR("formname") = mstrModuleName
                        xR("isweb") = False
                    Next
                End If

                If objCompetencies.Delete(CInt(lvCompetencies.SelectedItems(0).Tag), mdtCourses) = True Then
                    'S.SANDEEP |29-MAR-2021| -- END
                    lvCompetencies.SelectedItems(0).Remove()

                    'S.SANDEEP [22 SEP 2015] -- START
                Else
                    If objCompetencies._Message <> "" Then
                        eZeeMsgBox.Show(objCompetencies._Message, enMsgBoxStyle.Information)
                        Exit Sub
                    End If
                    'S.SANDEEP [22 SEP 2015] -- END
                End If
            End If
            lvCompetencies.Select()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnDelete_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnClose_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub objbtnSearchCategory_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchCategory.Click
        Dim frm As New frmCommonSearch
        Try
            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If
            With frm
                .ValueMember = cboCategory.ValueMember
                .DisplayMember = cboCategory.DisplayMember
                .DataSource = CType(cboCategory.DataSource, DataTable)
                If .DisplayDialog Then
                    cboCategory.SelectedValue = .SelectedValue
                    cboCategory.Focus()
                End If
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchCategory_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub objbtnSearchScore_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchScore.Click
        Dim frm As New frmCommonSearch
        Try
            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If
            With frm
                .ValueMember = cboScore.ValueMember
                .DisplayMember = cboScore.DisplayMember
                .DataSource = CType(cboScore.DataSource, DataTable)
                If .DisplayDialog Then
                    cboScore.SelectedValue = .SelectedValue
                    cboScore.Focus()
                End If
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchScore_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub objbtnSearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearch.Click
        Try
            Call Fill_List()
            Call objbtnSearch.ShowResult(CStr(lvCompetencies.Items.Count))
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub objbtnReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnReset.Click
        Try
            cboCategory.SelectedValue = 0 : cboScore.SelectedValue = 0
            lvCompetencies.Items.Clear()
            Call objbtnSearch.ShowResult(CStr(lvCompetencies.Items.Count))
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnReset_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub btnLoadLibrary_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnLoadLibrary.Click
        Dim frm As New frmLoadCompetencies
        Try
            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If
            If frm.displayDialog() Then
                Call Fill_List()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnLoadLibrary_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    'S.SANDEEP [21 NOV 2015] -- START
    Private Sub objbtnSearchGroup_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchGroup.Click
        Dim frm As New frmCommonSearch
        Try
            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If
            With frm
                .ValueMember = cboComptenceGroup.ValueMember
                .DisplayMember = cboComptenceGroup.DisplayMember
                .DataSource = CType(cboComptenceGroup.DataSource, DataTable)
                If .DisplayDialog Then
                    cboComptenceGroup.SelectedValue = .SelectedValue
                    cboComptenceGroup.Focus()
                End If
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchGroup_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub
    'S.SANDEEP [21 NOV 2015] -- END

#End Region

	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Private Sub OtherSettings()
		Try
			Me.SuspendLayout()
			
			Call SetLanguage()
			
			Me.gbFilterCriteria.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbFilterCriteria.ForeColor = GUI._eZeeContainerHeaderForeColor 

            Me.eZeeHeader.GradientColor1 = GUI._HeaderBackColor1
			Me.eZeeHeader.GradientColor2 = GUI._HeaderBackColor2 
			Me.eZeeHeader.BorderColor = GUI._HeaderBorderColor 
			Me.eZeeHeader.HeaderTextForeColor = GUI._HeaderTitleFontColor 
			Me.eZeeHeader.DescriptionForeColor = GUI._HeaderMessageFontColor 


			Me.btnDelete.GradientBackColor = GUI._ButttonBackColor 
			Me.btnDelete.GradientForeColor = GUI._ButttonFontColor

			Me.btnEdit.GradientBackColor = GUI._ButttonBackColor 
			Me.btnEdit.GradientForeColor = GUI._ButttonFontColor

			Me.btnNew.GradientBackColor = GUI._ButttonBackColor 
			Me.btnNew.GradientForeColor = GUI._ButttonFontColor

			Me.btnClose.GradientBackColor = GUI._ButttonBackColor 
			Me.btnClose.GradientForeColor = GUI._ButttonFontColor

            Me.btnLoadLibrary.GradientBackColor = GUI._ButttonBackColor
            Me.btnLoadLibrary.GradientForeColor = GUI._ButttonFontColor


			Me.ResumeLayout()
		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "OtherSettings", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetLanguage()
		Try
			Me.Text = Language._Object.getCaption(Me.Name, Me.Text)
			
			Me.eZeeHeader.Title = Language._Object.getCaption(Me.eZeeHeader.Name & "_Title" , Me.eZeeHeader.Title)
			Me.eZeeHeader.Message = Language._Object.getCaption(Me.eZeeHeader.Name & "_Message" , Me.eZeeHeader.Message)
			Me.gbFilterCriteria.Text = Language._Object.getCaption(Me.gbFilterCriteria.Name, Me.gbFilterCriteria.Text)
			Me.lblScoreScale.Text = Language._Object.getCaption(Me.lblScoreScale.Name, Me.lblScoreScale.Text)
			Me.lblCompetenciesCategory.Text = Language._Object.getCaption(Me.lblCompetenciesCategory.Name, Me.lblCompetenciesCategory.Text)
			Me.btnDelete.Text = Language._Object.getCaption(Me.btnDelete.Name, Me.btnDelete.Text)
			Me.btnEdit.Text = Language._Object.getCaption(Me.btnEdit.Name, Me.btnEdit.Text)
			Me.btnNew.Text = Language._Object.getCaption(Me.btnNew.Name, Me.btnNew.Text)
			Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
			Me.colhScale.Text = Language._Object.getCaption(CStr(Me.colhScale.Tag), Me.colhScale.Text)
			Me.colhName.Text = Language._Object.getCaption(CStr(Me.colhName.Tag), Me.colhName.Text)
			Me.colhDescription.Text = Language._Object.getCaption(CStr(Me.colhDescription.Tag), Me.colhDescription.Text)
            Me.lblPeriod.Text = Language._Object.getCaption(Me.lblPeriod.Name, Me.lblPeriod.Text)
            Me.colhPeriod.Text = Language._Object.getCaption(CStr(Me.colhPeriod.Tag), Me.colhPeriod.Text)
            Me.btnLoadLibrary.Text = Language._Object.getCaption(Me.btnLoadLibrary.Name, Me.btnLoadLibrary.Text)
            Me.lblGroupBy.Text = Language._Object.getCaption(Me.lblGroupBy.Name, Me.lblGroupBy.Text)
            Me.lblComptenceGroup.Text = Language._Object.getCaption(Me.lblComptenceGroup.Name, Me.lblComptenceGroup.Text)

		Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "Please select Competence from the list to perform further operation on it.")
			Language.setMessage(mstrModuleName, 3, "Are you sure you want to delete this Competence Item?")
            Language.setMessage(mstrModuleName, 4, "Competence Category")
            Language.setMessage(mstrModuleName, 5, "Competence Group")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>

End Class