﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmComputationFormula_AddEdit
    Inherits eZee.Common.eZeeForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmComputationFormula_AddEdit))
        Me.pnlMain = New System.Windows.Forms.Panel
        Me.gbCustomHeader = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.pnlOperation = New System.Windows.Forms.Panel
        Me.cboFunction = New System.Windows.Forms.ComboBox
        Me.lblFunction = New System.Windows.Forms.Label
        Me.lblFormulaFor = New System.Windows.Forms.Label
        Me.cboFormulaFor = New System.Windows.Forms.ComboBox
        Me.objbtnSearchFunction = New eZee.Common.eZeeGradientButton
        Me.btnDelete = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnAdd = New eZee.Common.eZeeLightButton(Me.components)
        Me.cboComputationVariables = New System.Windows.Forms.ComboBox
        Me.lblComputationVariables = New System.Windows.Forms.Label
        Me.objbtnSearchVariables = New eZee.Common.eZeeGradientButton
        Me.txtConstantValue = New eZee.TextBox.NumericTextBox
        Me.txtFormula = New System.Windows.Forms.TextBox
        Me.lvFormula = New eZee.Common.eZeeListView(Me.components)
        Me.colhFunction = New System.Windows.Forms.ColumnHeader
        Me.colhFormula = New System.Windows.Forms.ColumnHeader
        Me.colhPeriod = New System.Windows.Forms.ColumnHeader
        Me.objcolhEndDate = New System.Windows.Forms.ColumnHeader
        Me.objLine = New eZee.Common.eZeeLine
        Me.cboPeriod = New System.Windows.Forms.ComboBox
        Me.lblPeriod = New System.Windows.Forms.Label
        Me.objFooter = New eZee.Common.eZeeFooter
        Me.btnSave = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnClose = New eZee.Common.eZeeLightButton(Me.components)
        Me.pnlMain.SuspendLayout()
        Me.gbCustomHeader.SuspendLayout()
        Me.pnlOperation.SuspendLayout()
        Me.objFooter.SuspendLayout()
        Me.SuspendLayout()
        '
        'pnlMain
        '
        Me.pnlMain.Controls.Add(Me.gbCustomHeader)
        Me.pnlMain.Controls.Add(Me.objFooter)
        Me.pnlMain.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pnlMain.Location = New System.Drawing.Point(0, 0)
        Me.pnlMain.Name = "pnlMain"
        Me.pnlMain.Size = New System.Drawing.Size(617, 560)
        Me.pnlMain.TabIndex = 0
        '
        'gbCustomHeader
        '
        Me.gbCustomHeader.BorderColor = System.Drawing.Color.Black
        Me.gbCustomHeader.Checked = False
        Me.gbCustomHeader.CollapseAllExceptThis = False
        Me.gbCustomHeader.CollapsedHoverImage = Nothing
        Me.gbCustomHeader.CollapsedNormalImage = Nothing
        Me.gbCustomHeader.CollapsedPressedImage = Nothing
        Me.gbCustomHeader.CollapseOnLoad = False
        Me.gbCustomHeader.Controls.Add(Me.pnlOperation)
        Me.gbCustomHeader.Controls.Add(Me.txtFormula)
        Me.gbCustomHeader.Controls.Add(Me.lvFormula)
        Me.gbCustomHeader.Controls.Add(Me.objLine)
        Me.gbCustomHeader.Controls.Add(Me.cboPeriod)
        Me.gbCustomHeader.Controls.Add(Me.lblPeriod)
        Me.gbCustomHeader.Dock = System.Windows.Forms.DockStyle.Fill
        Me.gbCustomHeader.ExpandedHoverImage = Nothing
        Me.gbCustomHeader.ExpandedNormalImage = Nothing
        Me.gbCustomHeader.ExpandedPressedImage = Nothing
        Me.gbCustomHeader.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbCustomHeader.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbCustomHeader.HeaderHeight = 25
        Me.gbCustomHeader.HeaderMessage = ""
        Me.gbCustomHeader.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbCustomHeader.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbCustomHeader.HeightOnCollapse = 0
        Me.gbCustomHeader.LeftTextSpace = 0
        Me.gbCustomHeader.Location = New System.Drawing.Point(0, 0)
        Me.gbCustomHeader.Name = "gbCustomHeader"
        Me.gbCustomHeader.OpenHeight = 119
        Me.gbCustomHeader.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbCustomHeader.ShowBorder = True
        Me.gbCustomHeader.ShowCheckBox = False
        Me.gbCustomHeader.ShowCollapseButton = False
        Me.gbCustomHeader.ShowDefaultBorderColor = True
        Me.gbCustomHeader.ShowDownButton = False
        Me.gbCustomHeader.ShowHeader = True
        Me.gbCustomHeader.Size = New System.Drawing.Size(617, 505)
        Me.gbCustomHeader.TabIndex = 6
        Me.gbCustomHeader.Temp = 0
        Me.gbCustomHeader.Text = "Formula Creation"
        Me.gbCustomHeader.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'pnlOperation
        '
        Me.pnlOperation.Controls.Add(Me.cboFunction)
        Me.pnlOperation.Controls.Add(Me.lblFunction)
        Me.pnlOperation.Controls.Add(Me.lblFormulaFor)
        Me.pnlOperation.Controls.Add(Me.cboFormulaFor)
        Me.pnlOperation.Controls.Add(Me.objbtnSearchFunction)
        Me.pnlOperation.Controls.Add(Me.btnDelete)
        Me.pnlOperation.Controls.Add(Me.btnAdd)
        Me.pnlOperation.Controls.Add(Me.cboComputationVariables)
        Me.pnlOperation.Controls.Add(Me.lblComputationVariables)
        Me.pnlOperation.Controls.Add(Me.objbtnSearchVariables)
        Me.pnlOperation.Controls.Add(Me.txtConstantValue)
        Me.pnlOperation.Location = New System.Drawing.Point(3, 58)
        Me.pnlOperation.Name = "pnlOperation"
        Me.pnlOperation.Size = New System.Drawing.Size(602, 80)
        Me.pnlOperation.TabIndex = 2
        '
        'cboFunction
        '
        Me.cboFunction.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboFunction.DropDownWidth = 450
        Me.cboFunction.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboFunction.FormattingEnabled = True
        Me.cboFunction.Location = New System.Drawing.Point(146, 30)
        Me.cboFunction.Name = "cboFunction"
        Me.cboFunction.Size = New System.Drawing.Size(324, 21)
        Me.cboFunction.TabIndex = 257
        Me.cboFunction.Tag = ""
        '
        'lblFunction
        '
        Me.lblFunction.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblFunction.Location = New System.Drawing.Point(9, 33)
        Me.lblFunction.Name = "lblFunction"
        Me.lblFunction.Size = New System.Drawing.Size(131, 15)
        Me.lblFunction.TabIndex = 256
        Me.lblFunction.Text = "Function"
        '
        'lblFormulaFor
        '
        Me.lblFormulaFor.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblFormulaFor.Location = New System.Drawing.Point(9, 6)
        Me.lblFormulaFor.Name = "lblFormulaFor"
        Me.lblFormulaFor.Size = New System.Drawing.Size(131, 15)
        Me.lblFormulaFor.TabIndex = 357
        Me.lblFormulaFor.Text = "Create Formula For"
        Me.lblFormulaFor.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboFormulaFor
        '
        Me.cboFormulaFor.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboFormulaFor.DropDownWidth = 450
        Me.cboFormulaFor.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboFormulaFor.FormattingEnabled = True
        Me.cboFormulaFor.Location = New System.Drawing.Point(146, 3)
        Me.cboFormulaFor.Name = "cboFormulaFor"
        Me.cboFormulaFor.Size = New System.Drawing.Size(324, 21)
        Me.cboFormulaFor.TabIndex = 358
        '
        'objbtnSearchFunction
        '
        Me.objbtnSearchFunction.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchFunction.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchFunction.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchFunction.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchFunction.BorderSelected = False
        Me.objbtnSearchFunction.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchFunction.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objbtnSearchFunction.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchFunction.Location = New System.Drawing.Point(476, 30)
        Me.objbtnSearchFunction.Name = "objbtnSearchFunction"
        Me.objbtnSearchFunction.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchFunction.TabIndex = 258
        '
        'btnDelete
        '
        Me.btnDelete.BackColor = System.Drawing.Color.White
        Me.btnDelete.BackgroundImage = CType(resources.GetObject("btnDelete.BackgroundImage"), System.Drawing.Image)
        Me.btnDelete.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnDelete.BorderColor = System.Drawing.Color.Empty
        Me.btnDelete.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnDelete.FlatAppearance.BorderSize = 0
        Me.btnDelete.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnDelete.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnDelete.ForeColor = System.Drawing.Color.Black
        Me.btnDelete.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnDelete.GradientForeColor = System.Drawing.Color.Black
        Me.btnDelete.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnDelete.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnDelete.Location = New System.Drawing.Point(503, 44)
        Me.btnDelete.Name = "btnDelete"
        Me.btnDelete.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnDelete.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnDelete.Size = New System.Drawing.Size(97, 34)
        Me.btnDelete.TabIndex = 360
        Me.btnDelete.Text = "&Delete"
        Me.btnDelete.UseVisualStyleBackColor = True
        Me.btnDelete.Visible = False
        '
        'btnAdd
        '
        Me.btnAdd.BackColor = System.Drawing.Color.White
        Me.btnAdd.BackgroundImage = CType(resources.GetObject("btnAdd.BackgroundImage"), System.Drawing.Image)
        Me.btnAdd.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnAdd.BorderColor = System.Drawing.Color.Empty
        Me.btnAdd.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnAdd.FlatAppearance.BorderSize = 0
        Me.btnAdd.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnAdd.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnAdd.ForeColor = System.Drawing.Color.Black
        Me.btnAdd.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnAdd.GradientForeColor = System.Drawing.Color.Black
        Me.btnAdd.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnAdd.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnAdd.Location = New System.Drawing.Point(503, 3)
        Me.btnAdd.Name = "btnAdd"
        Me.btnAdd.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnAdd.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnAdd.Size = New System.Drawing.Size(97, 34)
        Me.btnAdd.TabIndex = 359
        Me.btnAdd.Text = "&Add"
        Me.btnAdd.UseVisualStyleBackColor = True
        '
        'cboComputationVariables
        '
        Me.cboComputationVariables.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboComputationVariables.DropDownWidth = 450
        Me.cboComputationVariables.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboComputationVariables.FormattingEnabled = True
        Me.cboComputationVariables.Location = New System.Drawing.Point(146, 57)
        Me.cboComputationVariables.Name = "cboComputationVariables"
        Me.cboComputationVariables.Size = New System.Drawing.Size(324, 21)
        Me.cboComputationVariables.TabIndex = 355
        Me.cboComputationVariables.Tag = ""
        '
        'lblComputationVariables
        '
        Me.lblComputationVariables.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblComputationVariables.Location = New System.Drawing.Point(9, 60)
        Me.lblComputationVariables.Name = "lblComputationVariables"
        Me.lblComputationVariables.Size = New System.Drawing.Size(131, 15)
        Me.lblComputationVariables.TabIndex = 256
        Me.lblComputationVariables.Text = "Computation Variables"
        '
        'objbtnSearchVariables
        '
        Me.objbtnSearchVariables.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchVariables.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchVariables.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchVariables.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchVariables.BorderSelected = False
        Me.objbtnSearchVariables.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchVariables.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objbtnSearchVariables.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchVariables.Location = New System.Drawing.Point(476, 57)
        Me.objbtnSearchVariables.Name = "objbtnSearchVariables"
        Me.objbtnSearchVariables.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchVariables.TabIndex = 356
        '
        'txtConstantValue
        '
        Me.txtConstantValue.AllowNegative = False
        Me.txtConstantValue.Decimal = New Decimal(New Integer() {0, 0, 0, 0})
        Me.txtConstantValue.DigitsInGroup = 0
        Me.txtConstantValue.Flags = 65536
        Me.txtConstantValue.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtConstantValue.Location = New System.Drawing.Point(146, 57)
        Me.txtConstantValue.MaxDecimalPlaces = 6
        Me.txtConstantValue.MaxWholeDigits = 21
        Me.txtConstantValue.Name = "txtConstantValue"
        Me.txtConstantValue.Prefix = ""
        Me.txtConstantValue.RangeMax = 1.7976931348623157E+308
        Me.txtConstantValue.RangeMin = -1.7976931348623157E+308
        Me.txtConstantValue.Size = New System.Drawing.Size(324, 21)
        Me.txtConstantValue.TabIndex = 361
        Me.txtConstantValue.Text = "0"
        Me.txtConstantValue.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtFormula
        '
        Me.txtFormula.BackColor = System.Drawing.SystemColors.Info
        Me.txtFormula.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtFormula.Location = New System.Drawing.Point(12, 152)
        Me.txtFormula.Multiline = True
        Me.txtFormula.Name = "txtFormula"
        Me.txtFormula.ReadOnly = True
        Me.txtFormula.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.txtFormula.Size = New System.Drawing.Size(591, 56)
        Me.txtFormula.TabIndex = 363
        '
        'lvFormula
        '
        Me.lvFormula.BackColorOnChecked = True
        Me.lvFormula.ColumnHeaders = Nothing
        Me.lvFormula.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.colhFunction, Me.colhFormula, Me.colhPeriod, Me.objcolhEndDate})
        Me.lvFormula.CompulsoryColumns = ""
        Me.lvFormula.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lvFormula.FullRowSelect = True
        Me.lvFormula.GridLines = True
        Me.lvFormula.GroupingColumn = Nothing
        Me.lvFormula.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable
        Me.lvFormula.HideSelection = False
        Me.lvFormula.Location = New System.Drawing.Point(12, 214)
        Me.lvFormula.MinColumnWidth = 50
        Me.lvFormula.MultiSelect = False
        Me.lvFormula.Name = "lvFormula"
        Me.lvFormula.OptionalColumns = ""
        Me.lvFormula.ShowMoreItem = False
        Me.lvFormula.ShowSaveItem = False
        Me.lvFormula.ShowSelectAll = True
        Me.lvFormula.ShowSizeAllColumnsToFit = True
        Me.lvFormula.Size = New System.Drawing.Size(591, 283)
        Me.lvFormula.Sortable = False
        Me.lvFormula.TabIndex = 361
        Me.lvFormula.UseCompatibleStateImageBehavior = False
        Me.lvFormula.View = System.Windows.Forms.View.Details
        '
        'colhFunction
        '
        Me.colhFunction.Tag = "colhFunction"
        Me.colhFunction.Text = "Function"
        Me.colhFunction.Width = 155
        '
        'colhFormula
        '
        Me.colhFormula.Tag = "colhFormula"
        Me.colhFormula.Text = "Formula"
        Me.colhFormula.Width = 430
        '
        'colhPeriod
        '
        Me.colhPeriod.Tag = "colhPeriod"
        Me.colhPeriod.Text = "Period"
        Me.colhPeriod.Width = 0
        '
        'objcolhEndDate
        '
        Me.objcolhEndDate.Tag = "objcolhEndDate"
        Me.objcolhEndDate.Text = ""
        Me.objcolhEndDate.Width = 0
        '
        'objLine
        '
        Me.objLine.LineOrientation = eZee.Common.eZeeLine.Orientation.Horizontal
        Me.objLine.Location = New System.Drawing.Point(12, 141)
        Me.objLine.Name = "objLine"
        Me.objLine.Size = New System.Drawing.Size(591, 8)
        Me.objLine.TabIndex = 44
        Me.objLine.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboPeriod
        '
        Me.cboPeriod.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboPeriod.DropDownWidth = 450
        Me.cboPeriod.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboPeriod.FormattingEnabled = True
        Me.cboPeriod.Location = New System.Drawing.Point(149, 35)
        Me.cboPeriod.Name = "cboPeriod"
        Me.cboPeriod.Size = New System.Drawing.Size(324, 21)
        Me.cboPeriod.TabIndex = 354
        '
        'lblPeriod
        '
        Me.lblPeriod.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPeriod.Location = New System.Drawing.Point(12, 38)
        Me.lblPeriod.Name = "lblPeriod"
        Me.lblPeriod.Size = New System.Drawing.Size(131, 15)
        Me.lblPeriod.TabIndex = 353
        Me.lblPeriod.Text = "Period"
        Me.lblPeriod.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objFooter
        '
        Me.objFooter.BorderColor = System.Drawing.Color.Silver
        Me.objFooter.Controls.Add(Me.btnSave)
        Me.objFooter.Controls.Add(Me.btnClose)
        Me.objFooter.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.objFooter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objFooter.GradiantStyle = eZee.Common.eZeeFooter.GradientStyle.Up
        Me.objFooter.GradientColor1 = System.Drawing.SystemColors.Control
        Me.objFooter.GradientColor2 = System.Drawing.SystemColors.Control
        Me.objFooter.Location = New System.Drawing.Point(0, 505)
        Me.objFooter.Name = "objFooter"
        Me.objFooter.Size = New System.Drawing.Size(617, 55)
        Me.objFooter.TabIndex = 5
        '
        'btnSave
        '
        Me.btnSave.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnSave.BackColor = System.Drawing.Color.White
        Me.btnSave.BackgroundImage = CType(resources.GetObject("btnSave.BackgroundImage"), System.Drawing.Image)
        Me.btnSave.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnSave.BorderColor = System.Drawing.Color.Empty
        Me.btnSave.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnSave.FlatAppearance.BorderSize = 0
        Me.btnSave.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnSave.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSave.ForeColor = System.Drawing.Color.Black
        Me.btnSave.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnSave.GradientForeColor = System.Drawing.Color.Black
        Me.btnSave.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSave.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnSave.Location = New System.Drawing.Point(405, 13)
        Me.btnSave.Name = "btnSave"
        Me.btnSave.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSave.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnSave.Size = New System.Drawing.Size(97, 30)
        Me.btnSave.TabIndex = 0
        Me.btnSave.Text = "&Save"
        Me.btnSave.UseVisualStyleBackColor = True
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnClose.BackColor = System.Drawing.Color.White
        Me.btnClose.BackgroundImage = CType(resources.GetObject("btnClose.BackgroundImage"), System.Drawing.Image)
        Me.btnClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnClose.BorderColor = System.Drawing.Color.Empty
        Me.btnClose.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnClose.FlatAppearance.BorderSize = 0
        Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClose.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.ForeColor = System.Drawing.Color.Black
        Me.btnClose.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnClose.GradientForeColor = System.Drawing.Color.Black
        Me.btnClose.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Location = New System.Drawing.Point(508, 13)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Size = New System.Drawing.Size(97, 30)
        Me.btnClose.TabIndex = 1
        Me.btnClose.Text = "&Close"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'frmComputationFormula_AddEdit
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(617, 560)
        Me.Controls.Add(Me.pnlMain)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmComputationFormula_AddEdit"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Formula Creation"
        Me.pnlMain.ResumeLayout(False)
        Me.gbCustomHeader.ResumeLayout(False)
        Me.gbCustomHeader.PerformLayout()
        Me.pnlOperation.ResumeLayout(False)
        Me.pnlOperation.PerformLayout()
        Me.objFooter.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents pnlMain As System.Windows.Forms.Panel
    Friend WithEvents objFooter As eZee.Common.eZeeFooter
    Friend WithEvents btnSave As eZee.Common.eZeeLightButton
    Friend WithEvents btnClose As eZee.Common.eZeeLightButton
    Friend WithEvents gbCustomHeader As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents cboFunction As System.Windows.Forms.ComboBox
    Friend WithEvents lblFunction As System.Windows.Forms.Label
    Friend WithEvents objbtnSearchFunction As eZee.Common.eZeeGradientButton
    Friend WithEvents objbtnSearchVariables As eZee.Common.eZeeGradientButton
    Friend WithEvents cboComputationVariables As System.Windows.Forms.ComboBox
    Friend WithEvents cboPeriod As System.Windows.Forms.ComboBox
    Friend WithEvents lblPeriod As System.Windows.Forms.Label
    Friend WithEvents lblComputationVariables As System.Windows.Forms.Label
    Friend WithEvents cboFormulaFor As System.Windows.Forms.ComboBox
    Friend WithEvents lblFormulaFor As System.Windows.Forms.Label
    Friend WithEvents objLine As eZee.Common.eZeeLine
    Friend WithEvents btnAdd As eZee.Common.eZeeLightButton
    Friend WithEvents btnDelete As eZee.Common.eZeeLightButton
    Friend WithEvents lvFormula As eZee.Common.eZeeListView
    Friend WithEvents colhFunction As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhFormula As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhPeriod As System.Windows.Forms.ColumnHeader
    Friend WithEvents objcolhEndDate As System.Windows.Forms.ColumnHeader
    Friend WithEvents txtFormula As System.Windows.Forms.TextBox
    Friend WithEvents pnlOperation As System.Windows.Forms.Panel
    Friend WithEvents txtConstantValue As eZee.TextBox.NumericTextBox
End Class
