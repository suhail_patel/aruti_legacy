﻿Option Strict On
#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data

#End Region

Public Class frmComputationFormula_AddEdit

#Region " Private Variable "

    Private ReadOnly mstrModuleName As String = "frmComputationFormula_AddEdit"
    Private mblnCancel As Boolean = True
    Private objComputation_master As clsassess_computation_master
    Private objComputation_tran As clsassess_computation_tran
    Private menAction As enAction = enAction.ADD_ONE
    Private mintComputationUnkid As Integer = -1
    Private mdtFormulaTran As DataTable
    Private mstrFormula_Captions As String = String.Empty

#End Region

#Region " Display Dialog "

    Public Function displayDialog(ByRef intUnkId As Integer, ByVal eAction As enAction, ByVal strFormula_Caption As String) As Boolean
        Try
            mintComputationUnkid = intUnkId
            menAction = eAction
            mstrFormula_Captions = strFormula_Caption

            Me.ShowDialog()

            intUnkId = mintComputationUnkid

            Return Not mblnCancel
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "displayDialog", mstrModuleName)
        Finally
        End Try
    End Function

#End Region

#Region " Private Methods "

    Private Sub SetColor()
        Try
            cboPeriod.BackColor = GUI.ColorComp
            cboComputationVariables.BackColor = GUI.ColorComp
            cboFormulaFor.BackColor = GUI.ColorComp
            cboFunction.BackColor = GUI.ColorComp
            txtConstantValue.BackColor = GUI.ColorComp
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetColor", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub SetValue()
        Try
            objComputation_master._Formula_Typeid = CInt(cboFormulaFor.SelectedValue)
            objComputation_master._Isvoid = False
            objComputation_master._Computation_Formula = CStr(txtFormula.Tag)
            objComputation_master._Periodunkid = CInt(cboPeriod.SelectedValue)
            objComputation_master._Voiddatetime = Nothing
            objComputation_master._Voidreason = ""
            objComputation_master._Voiduserunkid = -1
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetValue", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub GetValue()
        Try
            cboFormulaFor.SelectedValue = objComputation_master._Formula_Typeid
            cboPeriod.SelectedValue = objComputation_master._Periodunkid
            If mstrFormula_Captions.Trim.Length > 0 Then
                txtFormula.Text = mstrFormula_Captions
                txtFormula.Tag = objComputation_master._Computation_Formula
            End If
            If menAction = enAction.EDIT_ONE Then
                If objComputation_master._Computation_Formula.Trim.Length > 0 Then

                    Dim objCMaster As New clsMasterData
                    Dim dsComp As New DataSet
                    'S.SANDEEP [19 FEB 2015] -- START
                    Dim dsFrml As New DataSet
                    'S.SANDEEP [19 FEB 2015] -- END
                    dsComp = objCMaster.getComboListForAssessmentComputationVariables("List")
                    'S.SANDEEP [19 FEB 2015] -- START
                    dsFrml = objCMaster.getComboListForAssessmentComputationFunction("List")
                    'S.SANDEEP [19 FEB 2015] -- END
                    objCMaster = Nothing : Dim xNum As String = "" : Dim xFormula As String = objComputation_master._Computation_Formula

                    For i As Integer = 1 To objComputation_master._Computation_Formula.Length
                        If Mid(xFormula, i, 1) = "+" OrElse Mid(xFormula, i, 1) = "-" OrElse Mid(xFormula, i, 1) = "*" OrElse _
                           Mid(xFormula, i, 1) = "/" OrElse Mid(xFormula, i, 1) = "(" OrElse Mid(xFormula, i, 1) = ")" Then

                            If xNum.Trim.Length > 0 Then
                                cboFunction.SelectedValue = enAssess_Functions_Mode.PA_CONSTANT_VALUE
                                Dim dRow = mdtFormulaTran.NewRow

                                Dim objPrd As New clscommom_period_Tran
                                dRow.Item("periodunkid") = CInt(cboPeriod.SelectedValue)
                                'Sohail (21 Aug 2015) -- Start
                                'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                                'objPrd._Periodunkid = CInt(cboPeriod.SelectedValue)
                                objPrd._Periodunkid(FinancialYear._Object._DatabaseName) = CInt(cboPeriod.SelectedValue)
                                'Sohail (21 Aug 2015) -- End
                                dRow.Item("end_date") = eZeeDate.convertDate(objPrd._End_Date)
                                objPrd = Nothing
                                dRow.Item("headid") = "CV" 'Constant Value
                                dRow.Item("head") = xNum
                                dRow.Item("computationunkid") = mintComputationUnkid
                                dRow.Item("isvoid") = False
                                dRow.Item("voiduserunkid") = -1
                                dRow.Item("voiddatetime") = DBNull.Value
                                dRow.Item("voidreason") = String.Empty
                                dRow.Item("functionid") = CInt(cboFunction.SelectedValue)
                                dRow.Item("function") = cboFunction.Text

                                mdtFormulaTran.Rows.Add(dRow)

                                xNum = ""
                            End If

                            xNum = Trim(Mid(objComputation_master._Computation_Formula, i, 1))
                            If xNum = "+" Then
                                cboFunction.SelectedValue = enAssess_Functions_Mode.PLUS
                            ElseIf xNum = "-" Then
                                cboFunction.SelectedValue = enAssess_Functions_Mode.MINUS
                            ElseIf xNum = "*" Then
                                cboFunction.SelectedValue = enAssess_Functions_Mode.MULTIPLY
                            ElseIf xNum = "/" Then
                                cboFunction.SelectedValue = enAssess_Functions_Mode.DIVIDE
                            ElseIf xNum = "(" Then
                                cboFunction.SelectedValue = enAssess_Functions_Mode.OPEN_BRACKET
                            ElseIf xNum = ")" Then
                                cboFunction.SelectedValue = enAssess_Functions_Mode.CLOSE_BRACKET
                            End If

                            Dim dsRow As DataRow = mdtFormulaTran.NewRow
                            Dim objPeriod As New clscommom_period_Tran
                            dsRow.Item("periodunkid") = CInt(cboPeriod.SelectedValue)
                            'Sohail (21 Aug 2015) -- Start
                            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                            'objPeriod._Periodunkid = CInt(cboPeriod.SelectedValue)
                            objPeriod._Periodunkid(FinancialYear._Object._DatabaseName) = CInt(cboPeriod.SelectedValue)
                            'Sohail (21 Aug 2015) -- End
                            dsRow.Item("end_date") = eZeeDate.convertDate(objPeriod._End_Date)

                            Select Case CInt(cboFunction.SelectedValue)
                                Case enAssess_Functions_Mode.OPEN_BRACKET
                                    dsRow.Item("headid") = "("
                                    dsRow.Item("head") = "("
                                Case enAssess_Functions_Mode.CLOSE_BRACKET
                                    dsRow.Item("headid") = ")"
                                    dsRow.Item("head") = ")"
                                Case enAssess_Functions_Mode.PLUS
                                    dsRow.Item("headid") = "+"
                                    dsRow.Item("head") = "+"
                                Case enAssess_Functions_Mode.MINUS
                                    dsRow.Item("headid") = "-"
                                    dsRow.Item("head") = "-"
                                Case enAssess_Functions_Mode.MULTIPLY
                                    dsRow.Item("headid") = "*"
                                    dsRow.Item("head") = "*"
                                Case enAssess_Functions_Mode.DIVIDE
                                    dsRow.Item("headid") = "/"
                                    dsRow.Item("head") = "/"
                            End Select
                            dsRow.Item("computationunkid") = mintComputationUnkid
                            dsRow.Item("isvoid") = False
                            dsRow.Item("voiduserunkid") = -1
                            dsRow.Item("voiddatetime") = DBNull.Value
                            dsRow.Item("voidreason") = String.Empty
                            dsRow.Item("functionid") = CInt(cboFunction.SelectedValue)
                            dsRow.Item("function") = cboFunction.Text
                            mdtFormulaTran.Rows.Add(dsRow)
                            xNum = ""
                        ElseIf Mid(xFormula, i, 1) <> "#" Then
                            xNum += Trim(Mid(xFormula, i, 1))
                        Else
                            Dim xFrmlId As Integer = -1
                            If xNum <> "" Then
                                Dim dsRow As DataRow = mdtFormulaTran.NewRow
                                Dim objPeriod As New clscommom_period_Tran
                                dsRow.Item("periodunkid") = CInt(cboPeriod.SelectedValue)
                                'Sohail (21 Aug 2015) -- Start
                                'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                                'objPeriod._Periodunkid = CInt(cboPeriod.SelectedValue)
                                objPeriod._Periodunkid(FinancialYear._Object._DatabaseName) = CInt(cboPeriod.SelectedValue)
                                'Sohail (21 Aug 2015) -- End
                                dsRow.Item("end_date") = eZeeDate.convertDate(objPeriod._End_Date)

                                xFrmlId = objComputation_tran.PredefineFormula(CInt(xNum))
                                If xFrmlId > 0 Then
                                    Dim xtmp() As DataRow = dsFrml.Tables(0).Select("Id = '" & xNum.ToString.Substring(4) & "'")
                                    dsRow.Item("headid") = xtmp(0).Item("Id")
                                    dsRow.Item("head") = xtmp(0).Item("Name")
                                    dsRow.Item("computation_typeid") = xtmp(0).Item("Id")
                                    dsRow.Item("variable_name") = xtmp(0).Item("Name")
                                    cboFunction.SelectedValue = enAssess_Functions_Mode.PRE_DEF_FORMULA
                                    dsRow.Item("functionid") = CInt(cboFunction.SelectedValue)
                                    dsRow.Item("function") = cboFunction.Text
                                    dsRow.Item("computationunkid") = mintComputationUnkid
                                    dsRow.Item("isvoid") = False
                                    dsRow.Item("voiduserunkid") = -1
                                    dsRow.Item("voiddatetime") = DBNull.Value
                                    dsRow.Item("voidreason") = String.Empty
                                    mdtFormulaTran.Rows.Add(dsRow)
                                    xNum = ""
                                Else
                                    Dim xtmp() As DataRow = dsComp.Tables(0).Select("Id = '" & xNum & "'")
                                    dsRow.Item("headid") = xtmp(0).Item("Id")
                                    dsRow.Item("head") = xtmp(0).Item("Name")
                                    dsRow.Item("computation_typeid") = xtmp(0).Item("Id")
                                    dsRow.Item("variable_name") = xtmp(0).Item("Name")
                                    cboFunction.SelectedValue = enAssess_Functions_Mode.COMPUTATION_VARIABLES
                                    dsRow.Item("functionid") = CInt(cboFunction.SelectedValue)
                                    dsRow.Item("function") = cboFunction.Text
                                    dsRow.Item("computationunkid") = mintComputationUnkid
                                    dsRow.Item("isvoid") = False
                                    dsRow.Item("voiduserunkid") = -1
                                    dsRow.Item("voiddatetime") = DBNull.Value
                                    dsRow.Item("voidreason") = String.Empty
                                    mdtFormulaTran.Rows.Add(dsRow)
                                    xNum = ""
                                End If
                            End If
                        End If
                    Next

                    If xNum.Trim.Length > 0 Then
                        cboFunction.SelectedValue = enAssess_Functions_Mode.PA_CONSTANT_VALUE
                        Dim dRow = mdtFormulaTran.NewRow

                        Dim objPrd As New clscommom_period_Tran
                        dRow.Item("periodunkid") = CInt(cboPeriod.SelectedValue)
                        'Sohail (21 Aug 2015) -- Start
                        'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                        'objPrd._Periodunkid = CInt(cboPeriod.SelectedValue)
                        objPrd._Periodunkid(FinancialYear._Object._DatabaseName) = CInt(cboPeriod.SelectedValue)
                        'Sohail (21 Aug 2015) -- End
                        dRow.Item("end_date") = eZeeDate.convertDate(objPrd._End_Date)
                        objPrd = Nothing
                        dRow.Item("headid") = "CV" 'Constant Value
                        dRow.Item("head") = xNum
                        dRow.Item("computationunkid") = mintComputationUnkid
                        dRow.Item("isvoid") = False
                        dRow.Item("voiduserunkid") = -1
                        dRow.Item("voiddatetime") = DBNull.Value
                        dRow.Item("voidreason") = String.Empty
                        dRow.Item("functionid") = CInt(cboFunction.SelectedValue)
                        dRow.Item("function") = cboFunction.Text

                        mdtFormulaTran.Rows.Add(dRow)

                        xNum = ""
                    End If

                    Call FillList()
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetValue", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub FillCombo()
        Dim objMaster As New clsMasterData
        Dim objPeriod As New clscommom_period_Tran
        Dim dsCombo As New DataSet
        Try
            dsCombo = objMaster.getComboListForAssessmentFunctionVariables(True, "List")
            With cboFunction
                .ValueMember = "Id"
                .DisplayMember = "Name"
                .DataSource = dsCombo.Tables("List")
                .SelectedValue = 0
            End With

            dsCombo = objMaster.getComboListForAssessmentComputationFunction("List", True)
            With cboFormulaFor
                .ValueMember = "Id"
                .DisplayMember = "Name"
                .DataSource = dsCombo.Tables("List")
            End With
            'S.SANDEEP [14 APR 2015] -- START
            'dsCombo = objPeriod.getListForCombo(enModuleReference.Assessment, FinancialYear._Object._YearUnkid, "List", True, enStatusType.Open)
            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'dsCombo = objPeriod.getListForCombo(enModuleReference.Assessment, 0, "List", True, enStatusType.Open)
            dsCombo = objPeriod.getListForCombo(enModuleReference.Assessment, 0, FinancialYear._Object._DatabaseName, FinancialYear._Object._Database_Start_Date, "List", True, enStatusType.Open)
            'Sohail (21 Aug 2015) -- End
            'S.SANDEEP [14 APR 2015] -- END
            With cboPeriod
                .ValueMember = "periodunkid"
                .DisplayMember = "Name"
                .DataSource = dsCombo.Tables("List")
                .SelectedValue = 0
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        Finally
            dsCombo.Dispose() : objMaster = Nothing : objPeriod = Nothing
        End Try
    End Sub

    Private Function Valid_Formula() As Boolean
        Dim intFunction As Integer = CInt(cboFunction.SelectedValue)
        Dim intLastFunction As Integer = -1
        Dim intCntOpenBracket As Integer = 0
        Dim intCntCloseBracket As Integer = 0
        Dim dt As DataTable
        Try
            If intFunction <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please select an appropriate function."), enMsgBoxStyle.Information)
                cboFunction.Focus()
                Return False
            ElseIf (intFunction = enAssess_Functions_Mode.COMPUTATION_VARIABLES) AndAlso CInt(cboComputationVariables.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Please select an appropriate Computation Variable."), enMsgBoxStyle.Information)
                cboComputationVariables.Focus()
                Return False
                'S.SANDEEP [19 FEB 2015] -- START
            ElseIf (intFunction = enAssess_Functions_Mode.PA_CONSTANT_VALUE) AndAlso CInt(txtConstantValue.Decimal) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 15, "Please provide appropriate contant value."), enMsgBoxStyle.Information)
                cboComputationVariables.Focus()
                Return False
                'S.SANDEEP [19 FEB 2015] -- END
            ElseIf CInt(cboPeriod.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Please select Period."), enMsgBoxStyle.Information)
                cboPeriod.Focus()
                Return False
            ElseIf lvFormula.Items.Count = 0 Then
                Select Case CInt(cboFunction.SelectedValue)
                    Case enFunction.Plus, enFunction.Minus, enFunction.Multiply, enFunction.Divide, enFunction.Close_Bracket
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Function must begin with Computation Variable."), enMsgBoxStyle.Information)
                        cboFunction.Focus()
                        Return False
                End Select
            End If

            If CInt(cboFormulaFor.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 14, "Sorry, computation formula is mandatory information. Please select computation formula to continue."), enMsgBoxStyle.Information)
                cboFormulaFor.Focus()
                Return False
            End If

            'S.SANDEEP [ 16 JAN 2015 ] -- START
            'If CInt(cboComputationVariables.SelectedValue) > 0 Then
            '    Dim xtmp() As DataRow = mdtFormulaTran.Select("computation_typeid = '" & CInt(cboComputationVariables.SelectedValue) & "' AND AUD <> 'D'")
            '    If xtmp.Length > 0 Then
            '        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 13, "Sorry, computation variable is already added in the formula list."), enMsgBoxStyle.Information)
            '        Return False
            '    End If
            'End If
            'S.SANDEEP [ 16 JAN 2015 ] -- END

            dt = New DataView(mdtFormulaTran, "periodunkid = " & CInt(cboPeriod.SelectedValue) & " AND AUD <> 'D' ", "", DataViewRowState.CurrentRows).ToTable
            If dt.Rows.Count > 0 Then
                intLastFunction = CInt(dt.Rows(dt.Rows.Count - 1).Item("functionid"))
            End If

            If intLastFunction = enFunction.Open_Bracket And intFunction = enFunction.Close_Bracket Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "Invalid Close Bracket."), enMsgBoxStyle.Information)
                cboFunction.Focus()
                Return False
            ElseIf intLastFunction = enFunction.Close_Bracket And intFunction = enFunction.Open_Bracket Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 6, "Invalid Open Bracket."), enMsgBoxStyle.Information)
                cboFunction.Focus()
                Return False
            End If

            If intFunction = enFunction.Close_Bracket Then
                For Each dtRow As DataRow In dt.Rows
                    Select Case dtRow.Item("headid").ToString
                        Case "("
                            intCntOpenBracket += 1
                        Case ")"
                            intCntCloseBracket += 1
                    End Select
                Next

                If intCntOpenBracket <= intCntCloseBracket Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "Invalid Close Bracket."), enMsgBoxStyle.Information)
                    cboFunction.Focus()
                    Return False
                End If
            End If

            Select Case intLastFunction
                Case enAssess_Functions_Mode.COMPUTATION_VARIABLES, enAssess_Functions_Mode.CLOSE_BRACKET, _
                        enAssess_Functions_Mode.PA_CONSTANT_VALUE, enAssess_Functions_Mode.PRE_DEF_FORMULA 'S.SANDEEP [19 FEB 2015] -- START -- END

                    Select Case intFunction
                        Case enAssess_Functions_Mode.COMPUTATION_VARIABLES, enAssess_Functions_Mode.OPEN_BRACKET, _
                             enAssess_Functions_Mode.PA_CONSTANT_VALUE, enAssess_Functions_Mode.PRE_DEF_FORMULA 'S.SANDEEP [19 FEB 2015] -- START -- END

                            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 7, "Invalid Function selected."), enMsgBoxStyle.Information)
                            cboFunction.Focus()
                            Return False
                    End Select
                Case enAssess_Functions_Mode.PLUS, enAssess_Functions_Mode.MINUS, enAssess_Functions_Mode.MULTIPLY, enAssess_Functions_Mode.DIVIDE, enAssess_Functions_Mode.OPEN_BRACKET
                    Select Case intFunction
                        Case enAssess_Functions_Mode.PLUS, enAssess_Functions_Mode.MINUS, enAssess_Functions_Mode.MULTIPLY, enAssess_Functions_Mode.DIVIDE, enAssess_Functions_Mode.CLOSE_BRACKET
                            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 7, "Invalid Function selected."), enMsgBoxStyle.Information)
                            cboFunction.Focus()
                            Return False
                    End Select
            End Select



            Return True

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Valid_Formula", mstrModuleName)
        Finally
        End Try
    End Function

    Private Sub FillList()
        Dim lvItem As ListViewItem
        Try
            lvFormula.Items.Clear()
            mdtFormulaTran = New DataView(mdtFormulaTran, "AUD <> 'D' ", "end_date DESC", DataViewRowState.CurrentRows).ToTable
            For Each dtRow As DataRow In mdtFormulaTran.Rows
                lvItem = New ListViewItem

                lvItem.Text = dtRow.Item("function").ToString
                lvItem.Tag = dtRow.Item("functionid").ToString
                lvItem.SubItems.Add(dtRow.Item("head").ToString)
                lvItem.SubItems(colhFormula.Index).Tag = dtRow.Item("headid").ToString
                lvItem.SubItems.Add(dtRow.Item("period").ToString)
                lvItem.SubItems(colhPeriod.Index).Tag = CInt(dtRow.Item("periodunkid"))
                lvItem.SubItems.Add("") 'Period End Date
                lvItem.SubItems(objcolhEndDate.Index).Tag = dtRow.Item("end_date").ToString

                lvFormula.Items.Add(lvItem)
            Next

            lvFormula.GroupingColumn = colhPeriod
            lvFormula.DisplayGroups(True)

            If lvFormula.Items.Count > 3 Then
                colhFormula.Width = 430 - 18
            Else
                colhFormula.Width = 430
            End If
            Call DefineFormula()
            Call ResetControls()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillList", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Function DefineFormula() As Boolean
        Dim strFormulaId As String = ""
        Dim strFormula As String = ""
        Dim strPrevEndDate As String = ""
        Dim strPrevPeriodId As Integer = 0
        Try
            For i As Integer = 0 To lvFormula.Items.Count - 1
                Select Case CInt(lvFormula.Items(i).Tag)
                    Case enAssess_Functions_Mode.COMPUTATION_VARIABLES
                        strFormulaId = strFormulaId & "#" & lvFormula.Items(i).SubItems(1).Tag.ToString & "#"
                        strFormula = strFormula & lvFormula.Items(i).SubItems(1).Text
                    Case enAssess_Functions_Mode.OPEN_BRACKET, enAssess_Functions_Mode.CLOSE_BRACKET
                        strFormulaId = strFormulaId & lvFormula.Items(i).SubItems(1).Tag.ToString
                        strFormula = strFormula & lvFormula.Items(i).SubItems(1).Text
                        'S.SANDEEP [19 FEB 2015] -- START
                    Case enAssess_Functions_Mode.PA_CONSTANT_VALUE
                        strFormulaId = strFormulaId & lvFormula.Items(i).SubItems(1).Text
                        strFormula = strFormula & lvFormula.Items(i).SubItems(1).Text
                    Case enAssess_Functions_Mode.PRE_DEF_FORMULA
                        strFormulaId = strFormulaId & "#1000" & lvFormula.Items(i).SubItems(1).Tag.ToString & "#"
                        strFormula = strFormula & lvFormula.Items(i).SubItems(1).Text
                        'S.SANDEEP [19 FEB 2015] -- END
                    Case Else
                        strFormulaId = strFormulaId & lvFormula.Items(i).SubItems(1).Tag.ToString
                        strFormula = strFormula & " " & lvFormula.Items(i).SubItems(1).Text & " "
                End Select
            Next
            txtFormula.Text = strFormula
            txtFormula.Tag = strFormulaId
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "DefineFormula", mstrModuleName)
        Finally
        End Try
    End Function

    Private Sub ResetControls()
        Try
            If cboFunction.Items.Count > 0 Then cboFunction.SelectedValue = 0
            If cboComputationVariables.Items.Count > 0 Then cboComputationVariables.SelectedValue = 0
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "ResetControls", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

#Region " Form's Event "

    Private Sub frmComputationFormula_AddEdit_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        objComputation_master = New clsassess_computation_master
        objComputation_tran = New clsassess_computation_tran
        Try
            Call Set_Logo(Me, gApplicationType)
            Language.setLanguage(Me.Name)
            Call OtherSettings()
            Call SetColor()
            If menAction = enAction.EDIT_ONE Then
                objComputation_master._Computationunkid = mintComputationUnkid
            End If
            objComputation_tran._Computationunkid = mintComputationUnkid
            mdtFormulaTran = objComputation_tran._DataTable

            Call FillCombo()
            Call GetValue()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmComputationFormula_AddEdit_Load", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub frmComputationFormula_AddEdit_KeyPress(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles MyBase.KeyPress
        Try
            If Asc(e.KeyChar) = 13 Then
                SendKeys.Send("{TAB}")
                e.Handled = True
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmComputationFormula_AddEdit_KeyPress", mstrModuleName)
        End Try
    End Sub

    Private Sub frmComputationFormula_AddEdit_KeyDown(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles MyBase.KeyDown
        Try
            If e.Control = True And e.KeyCode = Keys.S Then
                btnSave_Click(sender, e)
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmComputationFormula_AddEdit_KeyDown", mstrModuleName)
        End Try
    End Sub

    Private Sub frmComputationFormula_AddEdit_FormClosed(ByVal sender As System.Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles MyBase.FormClosed
        objComputation_master = Nothing
    End Sub

    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsassess_computation_master.SetMessages()
            objfrm._Other_ModuleNames = "clsassess_computation_master"
            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub

#End Region

#Region " Button's Events "

    Private Sub btnAdd_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAdd.Click
        Dim objPeriod As New clscommom_period_Tran
        Dim dsRow As DataRow
        Try
            If Valid_Formula() = False Then Exit Sub
            dsRow = mdtFormulaTran.NewRow
            dsRow.Item("periodunkid") = CInt(cboPeriod.SelectedValue)
            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'objPeriod._Periodunkid = CInt(cboPeriod.SelectedValue)
            objPeriod._Periodunkid(FinancialYear._Object._DatabaseName) = CInt(cboPeriod.SelectedValue)
            'Sohail (21 Aug 2015) -- End
            dsRow.Item("end_date") = eZeeDate.convertDate(objPeriod._End_Date)
            dsRow.Item("functionid") = CInt(cboFunction.SelectedValue)
            dsRow.Item("function") = cboFunction.Text
            Select Case CInt(cboFunction.SelectedValue)
                Case enAssess_Functions_Mode.OPEN_BRACKET
                    dsRow.Item("headid") = "("
                    dsRow.Item("head") = "("
                Case enAssess_Functions_Mode.CLOSE_BRACKET
                    dsRow.Item("headid") = ")"
                    dsRow.Item("head") = ")"
                Case enAssess_Functions_Mode.PLUS
                    dsRow.Item("headid") = "+"
                    dsRow.Item("head") = "+"
                Case enAssess_Functions_Mode.MINUS
                    dsRow.Item("headid") = "-"
                    dsRow.Item("head") = "-"
                Case enAssess_Functions_Mode.MULTIPLY
                    dsRow.Item("headid") = "*"
                    dsRow.Item("head") = "*"
                Case enAssess_Functions_Mode.DIVIDE
                    dsRow.Item("headid") = "/"
                    dsRow.Item("head") = "/"
                Case enAssess_Functions_Mode.PA_CONSTANT_VALUE
                    dsRow.Item("headid") = "CV" 'Constant Value
                    dsRow.Item("head") = txtConstantValue.Decimal.ToString
                Case Else
                    If CInt(cboFunction.SelectedValue) = enAssess_Functions_Mode.PRE_DEF_FORMULA Then
                        dsRow.Item("formulaid") = enAssess_Functions_Mode.PRE_DEF_FORMULA
                        dsRow.Item("headid") = cboComputationVariables.SelectedValue
                        dsRow.Item("head") = cboComputationVariables.Text
                        dsRow.Item("computation_typeid") = CInt("1000" & cboComputationVariables.SelectedValue.ToString())
                        dsRow.Item("variable_name") = cboComputationVariables.Text
                    Else
                        dsRow.Item("headid") = cboComputationVariables.SelectedValue
                        dsRow.Item("head") = cboComputationVariables.Text
                        dsRow.Item("computation_typeid") = cboComputationVariables.SelectedValue
                        dsRow.Item("variable_name") = cboComputationVariables.Text
                    End If
            End Select
            dsRow.Item("computationunkid") = mintComputationUnkid
            dsRow.Item("isvoid") = False
            dsRow.Item("voiduserunkid") = -1
            dsRow.Item("voiddatetime") = DBNull.Value
            dsRow.Item("voidreason") = String.Empty
            dsRow.Item("AUD") = "A"
            dsRow.Item("GUID") = Guid.NewGuid.ToString

            mdtFormulaTran.Rows.Add(dsRow)

            txtConstantValue.Decimal = 0

            Call FillList()

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnAdd_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub btnDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDelete.Click
        Dim objPeriod As New clscommom_period_Tran
        Dim dsList As New DataSet
        Try
            If CInt(cboPeriod.SelectedValue) <= 0 AndAlso cboPeriod.Enabled = True Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 8, "Please select Period."), enMsgBoxStyle.Information)
                Exit Try
            End If
            Dim dt As DataTable = New DataView(mdtFormulaTran, "periodunkid = " & CInt(cboPeriod.SelectedValue) & " AND AUD <> 'D' ", "", DataViewRowState.CurrentRows).ToTable
            If dt.Rows.Count > 0 Then
                If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 9, "Are you sure you want to delete the Whole Formula for selected period?" & vbCrLf & _
                                                       "Due to this computation will be zero."), CType(enMsgBoxStyle.Question + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.Yes Then
                    lvFormula.Items.Clear()
                    txtFormula.Clear()
                    For Each dtRow As DataRow In mdtFormulaTran.Rows
                        If CInt(dtRow.Item("periodunkid")) = CInt(cboPeriod.SelectedValue) Then
                            dtRow.Item("AUD") = "D"
                        End If
                    Next
                    mdtFormulaTran.AcceptChanges()
                End If
                Call FillList()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnDelete_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Dim intFunction As Integer = CInt(cboFunction.SelectedValue)
        Dim intLastFunction As Integer = -1
        Dim intCntOpenBracket As Integer = 0
        Dim intCntCloseBracket As Integer = 0
        Dim dt As DataTable
        Dim blnFlag As Boolean = False
        Try
            If lvFormula.Items.Count = 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 10, "Please select atleast one function."), enMsgBoxStyle.Information)
                cboFunction.Focus()
                Exit Sub
            End If

            If lvFormula.Items.Count > 0 Then
                dt = New DataView(mdtFormulaTran, "periodunkid = " & CInt(cboPeriod.SelectedValue) & " AND AUD <> 'D' ", "", DataViewRowState.CurrentRows).ToTable
                If dt.Rows.Count > 0 Then
                    intLastFunction = CInt(dt.Rows(dt.Rows.Count - 1).Item("functionid"))
                Else
                    intLastFunction = -1
                End If

                Select Case intLastFunction
                    Case enAssess_Functions_Mode.PLUS, enAssess_Functions_Mode.MINUS, enAssess_Functions_Mode.MULTIPLY, enAssess_Functions_Mode.DIVIDE, enAssess_Functions_Mode.OPEN_BRACKET
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 11, "Sorry, Formula is incomplete."), enMsgBoxStyle.Information)
                        cboFunction.Focus()
                        Exit Sub
                End Select

                intCntOpenBracket = 0
                intCntCloseBracket = 0

                For Each dtRow As DataRow In dt.Rows
                    Select Case dtRow.Item("headid").ToString
                        Case "("
                            intCntOpenBracket += 1
                        Case ")"
                            intCntCloseBracket += 1
                    End Select
                Next

                If intCntOpenBracket <> intCntCloseBracket Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 12, "Unclosed Brackets. Please Add Brackets."), enMsgBoxStyle.Information)
                    cboFunction.Focus()
                    Exit Sub
                End If
            End If

            SetValue()

            'S.SANDEEP [28-May-2018] -- START
            'ISSUE/ENHANCEMENT : {Audit Trails} 
            objComputation_master._FormName = mstrModuleName
            objComputation_master._LoginEmployeeunkid = 0
            objComputation_master._ClientIP = getIP()
            objComputation_master._HostName = getHostName()
            objComputation_master._FromWeb = False
            objComputation_master._AuditUserId = User._Object._Userunkid
objComputation_master._CompanyUnkid = Company._Object._Companyunkid
            objComputation_master._AuditDate = ConfigParameter._Object._CurrentDateAndTime
            'S.SANDEEP [28-May-2018] -- END

            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'If menAction = enAction.EDIT_ONE Then
            '    blnFlag = objComputation_master.Update(mdtFormulaTran)
            'Else
            '    blnFlag = objComputation_master.Insert(mdtFormulaTran)
            'End If


            If menAction = enAction.EDIT_ONE Then
                blnFlag = objComputation_master.Update(mdtFormulaTran, User._Object._Userunkid)
            Else
                blnFlag = objComputation_master.Insert(mdtFormulaTran, User._Object._Userunkid)
            End If
            'S.SANDEEP [04 JUN 2015] -- END
            

            If blnFlag = False AndAlso objComputation_master._Message <> "" Then
                eZeeMsgBox.Show(objComputation_master._Message, enMsgBoxStyle.Information)
                Exit Sub
            End If

            lvFormula.Items.Clear()
            Call ResetControls()
            mdtFormulaTran.Rows.Clear()
            txtFormula.Text = "" : txtFormula.Tag = Nothing

            If menAction = enAction.EDIT_ONE Then
                Call btnClose_Click(sender, e)
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnSave_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            mblnCancel = False
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnClose_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub objbtnSearchFunction_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchFunction.Click
        Dim frm As New frmCommonSearch
        Try
            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If
            With frm
                .ValueMember = cboFunction.ValueMember
                .DisplayMember = cboFunction.DisplayMember
                .DataSource = CType(cboFunction.DataSource, DataTable)
                If .DisplayDialog Then
                    cboFunction.SelectedValue = .SelectedValue
                    cboFunction.Focus()
                End If
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchFunction_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub objbtnSearchVariables_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchVariables.Click
        Dim frm As New frmCommonSearch
        Try
            If cboComputationVariables.DataSource Is Nothing Then Exit Sub

            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If
            With frm
                .ValueMember = cboComputationVariables.ValueMember
                .DisplayMember = cboComputationVariables.DisplayMember
                .DataSource = CType(cboComputationVariables.DataSource, DataTable)
                If .DisplayDialog Then
                    cboComputationVariables.SelectedValue = .SelectedValue
                    cboComputationVariables.Focus()
                End If
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchVariables_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

#End Region

#Region " Combobox Events "

    Private Sub cboFunction_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboFunction.SelectedIndexChanged
        Try
            'S.SANDEEP [19 FEB 2015] -- START
            'If CInt(cboFunction.SelectedValue) = enAssess_Functions_Mode.COMPUTATION_VARIABLES Then
            '    cboComputationVariables.Enabled = True
            '    Dim objMaster As New clsMasterData
            '    Dim dsCombo As New DataSet
            '    dsCombo = objMaster.getComboListForAssessmentComputationVariables("List")
            '    With cboComputationVariables
            '        .ValueMember = "Id"
            '        .DisplayMember = "Name"
            '        .DataSource = dsCombo.Tables("List")
            '        .SelectedValue = enAssess_Computation_Types.BSC_ITEM_WEIGHT
            '    End With
            'Else
            '    cboComputationVariables.Enabled = False
            '    cboComputationVariables.DataSource = Nothing
            'End If
            If CInt(cboFunction.SelectedValue) = enAssess_Functions_Mode.COMPUTATION_VARIABLES Or _
               CInt(cboFunction.SelectedValue) = enAssess_Functions_Mode.PRE_DEF_FORMULA Then

                cboComputationVariables.Visible = True : objbtnSearchVariables.Visible = True
                txtConstantValue.Visible = False
                cboComputationVariables.Enabled = True : objbtnSearchVariables.Enabled = True

                Dim objMaster As New clsMasterData
                Dim dsCombo As New DataSet
                Dim xSource As DataTable = Nothing
                cboComputationVariables.DataSource = Nothing
                Select Case CInt(cboFunction.SelectedValue)
                    Case enAssess_Functions_Mode.COMPUTATION_VARIABLES
                        dsCombo = objMaster.getComboListForAssessmentComputationVariables("List")
                        xSource = dsCombo.Tables(0)
                    Case enAssess_Functions_Mode.PRE_DEF_FORMULA
                        dsCombo = objMaster.getComboListForAssessmentComputationFunction("List", True)
                        xSource = New DataView(dsCombo.Tables(0), "Id <> " & CInt(cboFormulaFor.SelectedValue), "", DataViewRowState.CurrentRows).ToTable
                End Select
                With cboComputationVariables
                    .ValueMember = "Id"
                    .DisplayMember = "Name"
                    .DataSource = xSource
                    Select Case CInt(cboFunction.SelectedValue)
                        Case enAssess_Functions_Mode.COMPUTATION_VARIABLES
                            .SelectedValue = enAssess_Computation_Types.BSC_ITEM_WEIGHT
                        Case Else
                            .SelectedValue = 0
                    End Select
                End With
            ElseIf CInt(cboFunction.SelectedValue) = enAssess_Functions_Mode.PA_CONSTANT_VALUE Then
                cboComputationVariables.Visible = False : objbtnSearchVariables.Visible = False
                txtConstantValue.Visible = True
            Else
                cboComputationVariables.Visible = True : objbtnSearchVariables.Visible = True
                txtConstantValue.Visible = False
                cboComputationVariables.Enabled = False
                objbtnSearchVariables.Enabled = False
                cboComputationVariables.DataSource = Nothing
            End If
            'S.SANDEEP [19 FEB 2015] -- END

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboFunction_SelectedIndexChanged", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub cboPeriod_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboPeriod.SelectedIndexChanged
        Try
            If CInt(cboPeriod.SelectedValue) > 0 Then
                pnlOperation.Enabled = True
            Else
                pnlOperation.Enabled = False
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboPeriod_SelectedIndexChanged", mstrModuleName)
        Finally
        End Try
    End Sub

    'S.SANDEEP [19 FEB 2015] -- START
    Private Sub cboFormulaFor_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboFormulaFor.SelectedIndexChanged
        Try
            If CInt(cboFunction.SelectedValue) = enAssess_Functions_Mode.PRE_DEF_FORMULA Then
                Call cboFunction_SelectedIndexChanged(New Object, New EventArgs)
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboFormulaFor_SelectedIndexChanged", mstrModuleName)
        Finally
        End Try
    End Sub
    'S.SANDEEP [19 FEB 2015] -- END

#End Region

	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Private Sub OtherSettings()
		Try
			Me.SuspendLayout()
			
			Call SetLanguage()
			
			Me.gbCustomHeader.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbCustomHeader.ForeColor = GUI._eZeeContainerHeaderForeColor 


			Me.btnSave.GradientBackColor = GUI._ButttonBackColor 
			Me.btnSave.GradientForeColor = GUI._ButttonFontColor

			Me.btnClose.GradientBackColor = GUI._ButttonBackColor 
			Me.btnClose.GradientForeColor = GUI._ButttonFontColor

			Me.btnAdd.GradientBackColor = GUI._ButttonBackColor 
			Me.btnAdd.GradientForeColor = GUI._ButttonFontColor

			Me.btnDelete.GradientBackColor = GUI._ButttonBackColor 
			Me.btnDelete.GradientForeColor = GUI._ButttonFontColor


			Me.ResumeLayout()
		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "OtherSettings", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetLanguage()
		Try
			Me.Text = Language._Object.getCaption(Me.Name, Me.Text)
			
			Me.btnSave.Text = Language._Object.getCaption(Me.btnSave.Name, Me.btnSave.Text)
			Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
			Me.gbCustomHeader.Text = Language._Object.getCaption(Me.gbCustomHeader.Name, Me.gbCustomHeader.Text)
			Me.lblFunction.Text = Language._Object.getCaption(Me.lblFunction.Name, Me.lblFunction.Text)
			Me.lblPeriod.Text = Language._Object.getCaption(Me.lblPeriod.Name, Me.lblPeriod.Text)
			Me.lblComputationVariables.Text = Language._Object.getCaption(Me.lblComputationVariables.Name, Me.lblComputationVariables.Text)
			Me.lblFormulaFor.Text = Language._Object.getCaption(Me.lblFormulaFor.Name, Me.lblFormulaFor.Text)
			Me.btnAdd.Text = Language._Object.getCaption(Me.btnAdd.Name, Me.btnAdd.Text)
			Me.btnDelete.Text = Language._Object.getCaption(Me.btnDelete.Name, Me.btnDelete.Text)
			Me.colhFunction.Text = Language._Object.getCaption(CStr(Me.colhFunction.Tag), Me.colhFunction.Text)
			Me.colhFormula.Text = Language._Object.getCaption(CStr(Me.colhFormula.Tag), Me.colhFormula.Text)
			Me.colhPeriod.Text = Language._Object.getCaption(CStr(Me.colhPeriod.Tag), Me.colhPeriod.Text)

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetLanguage", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "Please select an appropriate function.")
			Language.setMessage(mstrModuleName, 2, "Please select an appropriate Computation Variable.")
			Language.setMessage(mstrModuleName, 3, "Please select Period.")
			Language.setMessage(mstrModuleName, 4, "Function must begin with Computation Variable.")
			Language.setMessage(mstrModuleName, 5, "Invalid Close Bracket.")
			Language.setMessage(mstrModuleName, 6, "Invalid Open Bracket.")
			Language.setMessage(mstrModuleName, 7, "Invalid Function selected.")
			Language.setMessage(mstrModuleName, 8, "Please select Period.")
			Language.setMessage(mstrModuleName, 9, "Are you sure you want to delete the Whole Formula for selected period?" & vbCrLf & _
                                                       "Due to this computation will be zero.")
			Language.setMessage(mstrModuleName, 10, "Please select atleast one function.")
			Language.setMessage(mstrModuleName, 11, "Sorry, Formula is incomplete.")
			Language.setMessage(mstrModuleName, 12, "Unclosed Brackets. Please Add Brackets.")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>

End Class