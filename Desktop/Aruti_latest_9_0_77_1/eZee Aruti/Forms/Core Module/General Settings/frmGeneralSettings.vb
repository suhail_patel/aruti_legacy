﻿Option Strict On

#Region " Imports "
Imports eZeeCommonLib
Imports Aruti.Data
#End Region

Public Class frmGeneralSettings

#Region " Private Variables "
    Private ReadOnly mstrModuleName As String = "frmGeneralSettings"
    Private objGSettings As clsGeneralSettings
#End Region

#Region " Private  Medhods "
    Private Sub SetColor()
        Try
            cboBitPerSecond.BackColor = GUI.ColorOptional
            cboDataBits.BackColor = GUI.ColorOptional
            cboParity.BackColor = GUI.ColorOptional
            cboReaderType.BackColor = GUI.ColorOptional
            cboPort.BackColor = GUI.ColorOptional
            cboStopBits.BackColor = GUI.ColorOptional
            cboWorkingMode.BackColor = GUI.ColorOptional
            txtServer.BackColor = GUI.ColorOptional
            nudDelayTime.BackColor = GUI.ColorOptional
            nudRefreshInterval.BackColor = GUI.ColorOptional
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetColor", mstrModuleName)
        End Try
    End Sub

    Private Sub FillCombo()
        Dim dsList As New DataSet
        Dim objMaster As New clsMasterData
        Try
            dsList = objMaster.getBitPerSecond("Bits")
            With cboBitPerSecond
                .ValueMember = "id"
                .DisplayMember = "name"
                .DataSource = dsList.Tables("Bits")
                .SelectedValue = 0
            End With

            dsList = objMaster.getDataBit("DataBit")
            With cboDataBits
                .ValueMember = "id"
                .DisplayMember = "name"
                .DataSource = dsList.Tables("DataBit")
                .SelectedValue = 0
            End With

            dsList = objMaster.getParity("Parity")
            With cboParity
                .ValueMember = "id"
                .DisplayMember = "name"
                .DataSource = dsList.Tables("Parity")
                .SelectedValue = 0
            End With

            dsList = objMaster.getStopBit("StopBit")
            With cboStopBits
                .ValueMember = "id"
                .DisplayMember = "name"
                .DataSource = dsList.Tables("StopBit")
                .SelectedValue = 0
            End With

            dsList = objMaster.getComPortType("Port")
            With cboPort
                .ValueMember = "id"
                .DisplayMember = "name"
                .DataSource = dsList.Tables("Port")
                .SelectedValue = 0
            End With

            dsList = objMaster.getReaderType("Reader")
            With cboReaderType
                .ValueMember = "id"
                .DisplayMember = "name"
                .DataSource = dsList.Tables("Reader")
                .SelectedValue = 0
            End With

            dsList = objMaster.getWorkingMode("Mode")
            With cboWorkingMode
                .ValueMember = "id"
                .DisplayMember = "name"
                .DataSource = dsList.Tables("Mode")
                .SelectedValue = 0
            End With

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        Finally
            objMaster = Nothing
            dsList.Dispose()
        End Try
    End Sub

    Private Sub SetValue()
        Try
            objGSettings._Section = enArutiApplicatinType.Aruti_Payroll.ToString
            objGSettings._BitPerSecond = cboBitPerSecond.Text
            objGSettings._DataBits = cboDataBits.Text
            objGSettings._DelayTime = CStr(nudDelayTime.Value)
            objGSettings._Parity = cboParity.Text
            objGSettings._Port = CStr(cboPort.SelectedValue)
            objGSettings._ReaderType = cboReaderType.Text
            objGSettings._RefreshInterval = CStr(nudRefreshInterval.Value)
            objGSettings._ServerName = txtServer.Text
            objGSettings._StopBits = cboStopBits.Text
            objGSettings._WorkingMode = cboWorkingMode.Text
            'S.SANDEEP [ 10 NOV 2014 ] -- START
            objGSettings._AutoUpdate_Server = txtAutoUpdateServer.Text
            'S.SANDEEP [ 10 NOV 2014 ] -- END

            'S.SANDEEP [21-NOV-2017] -- START
            objGSettings._SQLPortNumber = CInt(txtSQLPort.Decimal)
            'S.SANDEEP [21-NOV-2017] -- END

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetValue", mstrModuleName)
        End Try
    End Sub

    Private Sub GetValue()
        Try
            objGSettings._Section = enArutiApplicatinType.Aruti_Payroll.ToString
            cboBitPerSecond.Text = objGSettings._BitPerSecond
            cboDataBits.Text = objGSettings._DataBits
            nudDelayTime.Value = CDec(objGSettings._DelayTime)
            cboParity.Text = objGSettings._Parity
            cboPort.Text = objGSettings._Port
            cboReaderType.Text = objGSettings._ReaderType
            nudRefreshInterval.Value = CDec(objGSettings._RefreshInterval)
            'S.SANDEEP [ 10 NOV 2014 ] -- START
            'txtServer.Text = objGSettings._ServerName
            RemoveHandler txtServer.TextChanged, AddressOf txtServer_TextChanged
            txtServer.Text = objGSettings._ServerName
            txtAutoUpdateServer.Text = objGSettings._AutoUpdate_Server
            AddHandler txtServer.TextChanged, AddressOf txtServer_TextChanged
            'S.SANDEEP [ 10 NOV 2014 ] -- END
            cboStopBits.Text = objGSettings._StopBits


            'Anjan [16 September 2014] -- Start
            'ENHANCEMENT : Implementing Language,requested by Andrew
            'cboWorkingMode.Text = objGSettings._WorkingMode
            If AppSettings._Object._IsClient = 1 Then
                cboWorkingMode.SelectedValue = 2
            Else
                cboWorkingMode.SelectedValue = 0
            End If
            cboWorkingMode.Text = cboWorkingMode.Text
            cboWorkingMode.Enabled = False
            'Anjan [16 September 2014] -- End

            'S.SANDEEP [21-NOV-2017] -- START
            txtSQLPort.Text = objGSettings._SQLPortNumber.ToString()
            'S.SANDEEP [21-NOV-2017] -- END

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetValue", mstrModuleName)
        End Try
    End Sub
#End Region

#Region " Forms Events "
    Private Sub frmGeneralSettings_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Call Set_Logo(Me, gApplicationType)
            If IO.File.Exists(System.AppDomain.CurrentDomain.BaseDirectory & "aruti.ini") = False Then
                IO.File.Create(System.AppDomain.CurrentDomain.BaseDirectory & "aruti.ini")
            End If

            objGSettings = New clsGeneralSettings
            Call FillCombo()
            Call GetValue()
            'Anjan (18 Dec 2010)-Start
            'Issue : This tab page is not in use now, so removed on run time.
            tabcGeneralInfomation.TabPages.Remove(tabpGeneralSettings)
            'Anjan (18 Dec 2010)-End
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmGeneralSettings_Load", mstrModuleName)
        End Try
    End Sub
#End Region

#Region " Buttons "
    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Try
            Call SetValue()
            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Information saved successfully."), enMsgBoxStyle.Information)
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnSave_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnClose_Click", mstrModuleName)
        End Try
    End Sub

    'Anjan [04 June 2014] -- Start
    'ENHANCEMENT : Implementing Language,requested by Andrew
    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub
    'Anjan [04 June 2014] -- End

    'S.SANDEEP [21-NOV-2017] -- START
    Private Sub objbtnInstructions_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnInstructions.Click
        Try
            Dim strMsg As String = String.Empty
            strMsg = "To Find SQL Current Port Number Follow Following Steps: " & vbCrLf
            strMsg &= "1. Go to Server Machine, And Press (Windows Key + R) to Activate Run Command Window. " & vbCrLf
            strMsg &= "2. Depending on the SQL Setup Installed, Type Following Command on Run Window " & vbCrLf
            strMsg &= "     a).SQL 2008 -> SQLServerManager10.msc " & vbCrLf
            strMsg &= "     b).SQL 2012 -> SQLServerManager11.msc " & vbCrLf
            strMsg &= "     c).SQL 2014 -> SQLServerManager12.msc " & vbCrLf
            strMsg &= "     d).SQL 2016 -> SQLServerManager13.msc " & vbCrLf
            strMsg &= "     e).SQL 2017 -> SQLServerManager14.msc " & vbCrLf
            strMsg &= "3. New Window will Open With Title as 'SQl Server Configuration Manager'. " & vbCrLf
            strMsg &= "4. In the above Mentioned Window Find 'SQL Server Network Configuration' -> Protocols for APAYROLL, and click it. " & vbCrLf
            strMsg &= "5. On the right pane, TCP/IP, Enable it, if it is disabled and double click it and go to IP Addresses Tab. " & vbCrLf
            strMsg &= "6. Scroll down to that Tab and Find Section Called 'IPALL'. " & vbCrLf
            strMsg &= "7. In the above mentioned section, there are 2 ports, " & vbCrLf
            strMsg &= "     a).TCP Port            -> This is static port default number is 1433 " & vbCrLf
            strMsg &= "     b).TCP Dynamic Ports   -> This is dynamic, It can have any number allocated by Operating system. " & vbCrLf
            strMsg &= "8. If 'TCP Port' is blank than take the value from 'TCP Dynamic Ports', and Paste on this screen and save. "

            eZeeMsgBox.Show(strMsg, enMsgBoxStyle.Information, Me.Text, False)

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnInstructions_Click", mstrModuleName)
        Finally
        End Try
    End Sub
    'S.SANDEEP [21-NOV-2017] -- END

#End Region

#Region " Controls "
    Private Sub cboWorkingMode_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboWorkingMode.SelectedIndexChanged
        Try
            txtServer.Enabled = False
            nudRefreshInterval.Enabled = False
            Select Case CInt(cboWorkingMode.SelectedValue)
                Case 0     'Stand Alone
                    txtServer.Enabled = False
                    txtServer.Text = "(Local)"
                    nudRefreshInterval.Enabled = False
                    'S.SANDEEP [ 10 NOV 2014 ] -- START
                    txtAutoUpdateServer.Enabled = False
                    'S.SANDEEP [ 10 NOV 2014 ] -- END
                Case 1     'Server
                    txtServer.Enabled = False
                    txtServer.Text = "(Local)"
                    nudRefreshInterval.Enabled = True
                    'S.SANDEEP [ 10 NOV 2014 ] -- START
                    txtAutoUpdateServer.Enabled = True
                    'S.SANDEEP [ 10 NOV 2014 ] -- END
                Case 2     'Client
                    txtServer.Enabled = True
                    nudRefreshInterval.Enabled = True
                    'S.SANDEEP [ 10 NOV 2014 ] -- START
                    txtAutoUpdateServer.Enabled = True
                    'S.SANDEEP [ 10 NOV 2014 ] -- END
            End Select
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboWorkingMode_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub cboReaderType_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboReaderType.SelectedIndexChanged
        Try
            Select Case CInt(cboReaderType.SelectedValue)
                Case enCardReaderType.Keyboard
                    nudDelayTime.Enabled = True
                Case Else
                    nudDelayTime.Enabled = False
            End Select
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboReaderType_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

    'S.SANDEEP [ 10 NOV 2014 ] -- START
    Private Sub txtServer_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtServer.TextChanged
        Try
            txtAutoUpdateServer.Text = txtServer.Text
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "txtServer_TextChanged", mstrModuleName)
        Finally
        End Try
    End Sub
    'S.SANDEEP [ 10 NOV 2014 ] -- END

#End Region

	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Private Sub OtherSettings()
		Try
			Me.SuspendLayout()
            Call SetLanguage()
			
			Me.gbClientServerSettings.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbClientServerSettings.ForeColor = GUI._eZeeContainerHeaderForeColor 

			Me.gbPortSettings.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbPortSettings.ForeColor = GUI._eZeeContainerHeaderForeColor 

			Me.gbReaderType.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbReaderType.ForeColor = GUI._eZeeContainerHeaderForeColor 


			
			Me.eZeeHeader.GradientColor1 = GUI._HeaderBackColor1 
			Me.eZeeHeader.GradientColor2 = GUI._HeaderBackColor2 
			Me.eZeeHeader.BorderColor = GUI._HeaderBorderColor 
			Me.eZeeHeader.HeaderTextForeColor = GUI._HeaderTitleFontColor 
			Me.eZeeHeader.DescriptionForeColor = GUI._HeaderMessageFontColor 


			Me.btnSave.GradientBackColor = GUI._ButttonBackColor 
			Me.btnSave.GradientForeColor = GUI._ButttonFontColor

			Me.btnClose.GradientBackColor = GUI._ButttonBackColor 
			Me.btnClose.GradientForeColor = GUI._ButttonFontColor


			Me.ResumeLayout()
		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "OtherSettings", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetLanguage()
		Try
			Me.Text = Language._Object.getCaption(Me.Name, Me.Text)
			
			Me.btnSave.Text = Language._Object.getCaption(Me.btnSave.Name, Me.btnSave.Text)
			Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
			Me.eZeeHeader.Title = Language._Object.getCaption(Me.eZeeHeader.Name & "_Title" , Me.eZeeHeader.Title)
			Me.eZeeHeader.Message = Language._Object.getCaption(Me.eZeeHeader.Name & "_Message" , Me.eZeeHeader.Message)
			Me.tabpGeneralSettings.Text = Language._Object.getCaption(Me.tabpGeneralSettings.Name, Me.tabpGeneralSettings.Text)
			Me.tabpClientServer.Text = Language._Object.getCaption(Me.tabpClientServer.Name, Me.tabpClientServer.Text)
			Me.gbClientServerSettings.Text = Language._Object.getCaption(Me.gbClientServerSettings.Name, Me.gbClientServerSettings.Text)
			Me.lblWorkingMode.Text = Language._Object.getCaption(Me.lblWorkingMode.Name, Me.lblWorkingMode.Text)
			Me.lblServer.Text = Language._Object.getCaption(Me.lblServer.Name, Me.lblServer.Text)
			Me.lblInterval.Text = Language._Object.getCaption(Me.lblInterval.Name, Me.lblInterval.Text)
			Me.lblMinutes.Text = Language._Object.getCaption(Me.lblMinutes.Name, Me.lblMinutes.Text)
			Me.gbPortSettings.Text = Language._Object.getCaption(Me.gbPortSettings.Name, Me.gbPortSettings.Text)
			Me.lblPort.Text = Language._Object.getCaption(Me.lblPort.Name, Me.lblPort.Text)
			Me.lblStopBits.Text = Language._Object.getCaption(Me.lblStopBits.Name, Me.lblStopBits.Text)
			Me.lblParity.Text = Language._Object.getCaption(Me.lblParity.Name, Me.lblParity.Text)
			Me.lblDataBits.Text = Language._Object.getCaption(Me.lblDataBits.Name, Me.lblDataBits.Text)
			Me.lblBitPerSettings.Text = Language._Object.getCaption(Me.lblBitPerSettings.Name, Me.lblBitPerSettings.Text)
			Me.gbReaderType.Text = Language._Object.getCaption(Me.gbReaderType.Name, Me.gbReaderType.Text)
			Me.lblReaderType.Text = Language._Object.getCaption(Me.lblReaderType.Name, Me.lblReaderType.Text)
			Me.lblDelayTime.Text = Language._Object.getCaption(Me.lblDelayTime.Name, Me.lblDelayTime.Text)
			Me.lblDelaySec.Text = Language._Object.getCaption(Me.lblDelaySec.Name, Me.lblDelaySec.Text)
			Me.lblChanges.Text = Language._Object.getCaption(Me.lblChanges.Name, Me.lblChanges.Text)

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetLanguage", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "Information saved successfully.")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class