﻿Option Strict On

Imports eZeeCommonLib
Imports Aruti.Data
Imports Aruti.Data.Language

Public Class frmCommonSelection
    
#Region " Private Variables "
    Private mstrModuleName As String = "frmCommonSelection"
    Private mintCompanyUnkid As Integer = 0
    Private mblnCancel As Boolean = True
    Private objCompany As clsCompany_Master
    Private mblnIsFinancialYear As Boolean = False
    Private mintYearUnkid As Integer = 0
#End Region

#Region " Properties "
    Public Property _IsFinancial_Year() As Boolean
        Get
            Return mblnIsFinancialYear
        End Get
        Set(ByVal value As Boolean)
            mblnIsFinancialYear = value
        End Set
    End Property
#End Region

#Region " Display Dialog "
    Public Function displayDialog() As Boolean
        Try
            Me.ShowDialog()
            Return Not mblnCancel
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "displayDialog", mstrModuleName)
        End Try
    End Function
#End Region

#Region " Form's Events "
    Private Sub frmCommonSelection_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed

        'Pinkal (12-Oct-2011) -- Start
        RemoveHandler clsApplicationIdleTimer.ApplicationIdle, AddressOf Application_Idle
        'Pinkal (12-Oct-2011) -- End

        objCompany = Nothing
    End Sub

    Private Sub frmCommonSelection_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles Me.KeyPress
        If Asc(e.KeyChar) = 13 Then
            Call btnOpen.PerformClick()
        ElseIf Asc(e.KeyChar) = 27 Then
            Me.Close()
        End If
    End Sub

    Private Sub frmCommonSelection_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try

            'Pinkal (21-Jun-2011) -- Start
            If User._Object.PWDOptions._IsApplication_Idle Then
                CheckForIllegalCrossThreadCalls = False
                RemoveHandler clsApplicationIdleTimer.ApplicationIdle, AddressOf Application_Idle
                AddHandler clsApplicationIdleTimer.ApplicationIdle, AddressOf Application_Idle
            End If

            'Pinkal (21-Jun-2011) -- End

            Call Set_Logo(Me, gApplicationType)
            objCompany = New clsCompany_Master
            If mblnIsFinancialYear = True Then
                lvCompany.Visible = False
                lvDatabase.Visible = True
                'chkDoNotNextTime.Visible = False
                Me.Text = Language.getMessage(mstrModuleName, 3, "Select Database")
                Me.eZeeHeader.Title = Language.getMessage(mstrModuleName, 3, "Select Database")
                Me.picSideImage.Image = Global.Aruti.Main.My.Resources.select_db
                Call FillYearList()
            Else
                Me.picSideImage.Image = Global.Aruti.Main.My.Resources.company
                lvCompany.Visible = True
                lvDatabase.Visible = False
                Call FillList()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmCommonSelection_Load", mstrModuleName)
        End Try
    End Sub
#End Region

#Region " Button's Events "
    Private Sub btnOpen_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnOpen.Click
        If mblnIsFinancialYear = False Then
            If lvCompany.SelectedItems.Count <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please select Company from the list."), enMsgBoxStyle.Information)
                Exit Sub
            End If
        Else
            If lvDatabase.SelectedItems.Count <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Please select Database from the list."), enMsgBoxStyle.Information)
                Exit Sub
            End If
        End If

        Dim objTempCompany As New clsCompany_Master
        Try
            If mblnIsFinancialYear = False Then
                mintCompanyUnkid = CInt(lvCompany.SelectedItems(0).Tag)

                objTempCompany._Companyunkid = mintCompanyUnkid

                If mintCompanyUnkid > 0 Then
                    Company._Object._Companyunkid = mintCompanyUnkid


                    'Pinkal (11-Dec-2018) -- Start
                    'Issue - Login page is taking time to login in NMB.
                    'ConfigParameter._Object.Refresh()
                    'Pinkal (11-Dec-2018) -- End


                    ConfigParameter._Object._Companyunkid = mintCompanyUnkid

                    'Anjan [ 05 Feb 2013 ] -- Start
                    'ENHANCEMENT : License changes for checking if someone enters employee from backend
                    Cursor.Current = Cursors.WaitCursor
                    'Sohail (04 Jun 2013) -- Start
                    'TRA - ENHANCEMENT
                    'Company._Object._Total_Active_Employee_AsOnFromDate = ConfigParameter._Object._CurrentDateAndTime.Date
                    'Company._Object._Total_Active_Employee_AsOnToDate = ConfigParameter._Object._CurrentDateAndTime.Date
                    Company._Object._Total_Active_Employee_AsOnDate = ConfigParameter._Object._CurrentDateAndTime.Date
                    'Sohail (04 Jun 2013) -- End
                    If ConfigParameter._Object._IsArutiDemo = False AndAlso Company._Object._Total_Active_Company > ConfigParameter._Object._NoOfCompany Then
                        Dim objAppSettings As New clsApplicationSettings
                        If objAppSettings._IsClient = 0 Then
                            btnUnlock.Visible = True
                        End If
                        objAppSettings = Nothing
                        Cursor.Current = Cursors.Default
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "Sorry, you cannot login to Aruti. Reason : You have exceeded number of company license limit. Please contact Aruti Support team for assistance."), enMsgBoxStyle.Information)
                        Exit Sub
                    End If
                    If ConfigParameter._Object._IsArutiDemo = False AndAlso Company._Object._Total_Active_Employee_ForAllCompany > ConfigParameter._Object._NoOfEmployees Then
                        Dim objAppSettings As New clsApplicationSettings
                        If objAppSettings._IsClient = 0 Then
                            btnUnlock.Visible = True
                        End If
                        objAppSettings = Nothing
                        Cursor.Current = Cursors.Default
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 6, "Sorry, you cannot login to Aruti. Reason : You have exceeded number of Employee license limit. Please contact Aruti Support team for assistance."), enMsgBoxStyle.Information)
                        Exit Sub
                    End If
                    Cursor.Current = Cursors.Default
                    'Anjan [ 05 Feb 2013 ] -- End



                    'Sandeep [ 01 MARCH 2011 ] -- Start
                    GUI.fmtCurrency = ConfigParameter._Object._CurrencyFormat
                    'Sandeep [ 01 MARCH 2011 ] -- End 


                    'S.SANDEEP [ 08 June 2011 ] -- START
                    Dim objAppSetting As New clsApplicationSettings
                    objAppSetting._LastCompanyId = mintCompanyUnkid
                    objAppSetting = Nothing
                    'S.SANDEEP [ 08 June 2011 ] -- START

                    mblnCancel = False
                    Me.Close()
                    frmSplash.Hide()
                End If
            Else
                mintYearUnkid = CInt(lvDatabase.SelectedItems(0).Tag)
                objTempCompany._YearUnkid = mintYearUnkid
                If mintYearUnkid > 0 Then
                    FinancialYear._Object._YearUnkid = mintYearUnkid



                    'S.SANDEEP [04 JUN 2015] -- START
                    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                    'Dim objMaster As New clsMasterData
                    'UserAccessLevel._AccessLevel = objMaster.GetUserAccessLevel
                    'S.SANDEEP [04 JUN 2015] -- END

                    'Sohail (17 Apr 2012) -- Start
                    'TRA - ENHANCEMENT
                    'Sohail (31 May 2012) -- Start
                    'TRA - ENHANCEMENT
                    'If UserAccessLevel._AccessLevel.Length > 0 Then
                    '    Select Case ConfigParameter._Object._UserAccessModeSetting
                    '        Case enAllocation.BRANCH
                    '            UserAccessLevel._AccessLevelFilterString = " AND  hremployee_master.stationunkid IN (" & UserAccessLevel._AccessLevel & ") "
                    '        Case enAllocation.DEPARTMENT_GROUP
                    '            UserAccessLevel._AccessLevelFilterString = " AND  hremployee_master.deptgroupunkid IN (" & UserAccessLevel._AccessLevel & ") "
                    '        Case enAllocation.DEPARTMENT
                    '            UserAccessLevel._AccessLevelFilterString = " AND  hremployee_master.departmentunkid IN (" & UserAccessLevel._AccessLevel & ") "
                    '        Case enAllocation.SECTION_GROUP
                    '            UserAccessLevel._AccessLevelFilterString = " AND  hremployee_master.sectiongroupunkid IN (" & UserAccessLevel._AccessLevel & ") "
                    '        Case enAllocation.SECTION
                    '            UserAccessLevel._AccessLevelFilterString = " AND  hremployee_master.sectionunkid IN (" & UserAccessLevel._AccessLevel & ") "
                    '        Case enAllocation.UNIT_GROUP
                    '            UserAccessLevel._AccessLevelFilterString = " AND  hremployee_master.unitgroupunkid IN (" & UserAccessLevel._AccessLevel & ") "
                    '        Case enAllocation.UNIT
                    '            UserAccessLevel._AccessLevelFilterString = " AND  hremployee_master.unitunkid IN (" & UserAccessLevel._AccessLevel & ") "
                    '        Case enAllocation.TEAM
                    '            UserAccessLevel._AccessLevelFilterString = " AND  hremployee_master.teamunkid IN (" & UserAccessLevel._AccessLevel & ") "
                    '        Case enAllocation.JOB_GROUP
                    '            UserAccessLevel._AccessLevelFilterString = " AND  hremployee_master.jobgroupunkid IN (" & UserAccessLevel._AccessLevel & ") "
                    '        Case enAllocation.JOBS
                    '            UserAccessLevel._AccessLevelFilterString = " AND hremployee_master.jobunkid IN (" & UserAccessLevel._AccessLevel & ") "
                    '    End Select
                    'Else
                    '    UserAccessLevel._AccessLevelFilterString = " "
                    'End If
                    'Sohail (31 May 2012) -- End
                    'Sohail (17 Apr 2012) -- End

'Pinkal (11-June-2011) -- Start

                    'S.SANDEEP [ 21 MAY 2012 ] -- START
                    'ENHANCEMENT : TRA USER PRIVILEGE CHANGES
                    'SetUserTracingInfo(False, enUserMode.Loging)
                    SetUserTracingInfo(False, enUserMode.Loging, enLogin_Mode.DESKTOP)
                    'S.SANDEEP [ 21 MAY 2012 ] -- END


                    'Pinkal (11-June-2011) -- End

                    'S.SANDEEP [ 08 June 2011 ] -- START
                    Dim objAppSetting As New clsApplicationSettings
                    objAppSetting._LastDatabaseId = mintYearUnkid
                    objAppSetting = Nothing
                    'S.SANDEEP [ 08 June 2011 ] -- START

                    mblnCancel = False
                    Me.Close()
                    frmSplash.Hide()
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnOpen_Click", mstrModuleName)
        Finally
            objTempCompany = Nothing
        End Try
    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Try
            mblnCancel = True
            Me.Close()
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "btnClose_Click", mstrModuleName)
        End Try
    End Sub

    'Sohail (04 May 2013) -- Start
    'TRA - ENHANCEMENT
    Private Sub btnUnlock_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnUnlock.Click
        Try
            Me.Hide()
            Dim frm As New frmNGLicense
            frm.ShowDialog()
            frm = Nothing
            Me.Close()
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "btnUnlock_Click", mstrModuleName)
        End Try
    End Sub
    'Sohail (04 May 2013) -- End
    'Anjan [04 June 2014] -- Start
    'ENHANCEMENT : Implementing Language,requested by Andrew
    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsCompany_Master.SetMessages()
            objfrm._Other_ModuleNames = "clsCompany_Master"

            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub
    'Anjan [04 June 2014] -- End
#End Region

#Region " Private Function "
    Private Sub FillList()
        Dim dsList As New DataSet
        Dim lvItem As ListViewItem
        Try
            dsList = objCompany.GetCompanyAcessList(User._Object._Userunkid, "Company")

            'Anjan (30 Aug 2011)-Start
            'Issue : Including multicompany license.
            'If ArtLic._Object.IsDemo = False Then
            '    If dsList.Tables(0).Rows.Count > CInt(ArtLic._Object.NoOfRooms) Then
            '        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 111, "This Company is registered for only ") & ArtLic._Object.NoOfRooms.ToString & Language.getMessage(mstrModuleName, 112, " Company(s) License. Please go to Aruti Configuration -> Company Creation and Delete non required company(s) else contact Aruti Support for More Company License."), enMsgBoxStyle.Information)
            '        Me.Close()
            '    End If
            'End If
            'Anjan (30 Aug 2011)-End 

            lvCompany.Items.Clear()
            For Each dtRow As DataRow In dsList.Tables("Company").Rows
                lvItem = New ListViewItem

                lvItem.Text = dtRow("code").ToString
                lvItem.SubItems.Add(dtRow("name").ToString)
                lvItem.Tag = dtRow("companyunkid").ToString

                lvCompany.Items.Add(lvItem)

                lvItem = Nothing
            Next


            'Sandeep [ 16 MAY 2011 ] -- Start
            If lvCompany.Items.Count > 9 Then
                colhProperty.Width = colhProperty.Width - 15
            Else
                colhProperty.Width = colhProperty.Width
            End If
            'Sandeep [ 16 MAY 2011 ] -- End 


            lvCompany.Select()
            If lvCompany.Items.Count > 0 Then lvCompany.Items(0).Selected = True

            'S.SANDEEP [ 08 June 2011 ] -- START
            Dim objAppSettings As New clsApplicationSettings
            For iCnt As Integer = 0 To lvCompany.Items.Count - 1
                'Sohail (19 Jan 2015) -- Start
                'Enhancement - Ensure Visible selected company.
                'If CInt(lvCompany.Items(iCnt).Tag) = objAppSettings._LastCompanyId Then lvCompany.Items(iCnt).Selected = True
                If CInt(lvCompany.Items(iCnt).Tag) = objAppSettings._LastCompanyId Then
                    lvCompany.Items(iCnt).Selected = True
                lvCompany.Items(iCnt).EnsureVisible()
                End If
                'Sohail (19 Jan 2015) -- End
            Next
            objAppSettings = Nothing
            'S.SANDEEP [ 08 June 2011 ] -- START

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillList", mstrModuleName)
        End Try
    End Sub

    Private Sub FillYearList()
        Dim dsList As New DataSet
        Dim lvItem As ListViewItem
        Try
            dsList = objCompany.GetFinancialYearList(Company._Object._Companyunkid, User._Object._Userunkid, "List")
            lvDatabase.Items.Clear()
            For Each dtRow As DataRow In dsList.Tables("List").Rows
                lvItem = New ListViewItem

                lvItem.Text = dtRow.Item("database_name").ToString
                lvItem.SubItems.Add(dtRow.Item("CompanyName").ToString)
                lvItem.Tag = dtRow.Item("yearunkid")
                'Sandeep ( 18 JAN 2011 ) -- START
                If CBool(dtRow.Item("isclosed")) = True Then lvItem.ForeColor = Color.Gray
                'Sandeep ( 18 JAN 2011 ) -- END 
                lvDatabase.Items.Add(lvItem)

            lvItem = Nothing
        Next
            lvDatabase.GroupingColumn = objcolCompany
            objcolCompany.Width = 0
            lvDatabase.DisplayGroups(True)

            'Sandeep [ 16 MAY 2011 ] -- Start
            If lvDatabase.Items.Count > 9 Then
                colhDatabase.Width = colhDatabase.Width - 15
            Else
                colhDatabase.Width = colhDatabase.Width
            End If
            'Sandeep [ 16 MAY 2011 ] -- End

            lvDatabase.Select()
            If lvDatabase.Items.Count > 0 Then lvDatabase.Items(0).Selected = True


            'S.SANDEEP [ 08 June 2011 ] -- START
            Dim objAppSettings As New clsApplicationSettings
            For iCnt As Integer = 0 To lvDatabase.Items.Count - 1
                If CInt(lvDatabase.Items(iCnt).Tag) = objAppSettings._LastDatabaseId Then lvDatabase.Items(iCnt).Selected = True
            Next
            objAppSettings = Nothing
            'S.SANDEEP [ 08 June 2011 ] -- START

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillYearList", mstrModuleName)
        End Try
    End Sub
#End Region

    'Pinkal (21-Jun-2011) -- Start

#Region "Private Event"

    Private Sub Application_Idle(ByVal e As clsApplicationIdleTimer.ApplicationIdleEventArgs)
        Try
            If User._Object.PWDOptions._IsApplication_Idle Then
                If e.IdleDuration.Minutes >= User._Object.PWDOptions._Idletime Then
                    If clsApplicationIdleTimer.LastIdealTime.Enabled = True Then
                        clsApplicationIdleTimer.LastIdealTime.Stop()
                        clsApplicationIdleTimer.LastIdealTime.Enabled = False

                        'Pinkal (12-Oct-2011) -- Start

                        'S.SANDEEP [14-May-2018] -- START
                        'ISSUE/ENHANCEMENT : {#2252|#ARUTI-162}
                        'Dim obj As New FrmMessageBox
                        'obj.LblMessage.Text = Language.getMessage(mstrModuleName, 4, "You are logged out from the system. Reason : Application exceeded Idle time set in configuration. In order to relogin to system, Press Ok.")
                        'MsgBox(Language.getMessage(mstrModuleName, 4, "You are logged out from the system. Reason : Application exceeded Idle time set in configuration. In order to relogin to system, Press Ok."), MsgBoxStyle.Information)
                        ''If CBool(obj.ShowDialog(Me)) Then
                        ''    blnIsIdealTimeSet = True
                        ''    Application.Restart()
                        ''End If
                        'blnIsIdealTimeSet = True
                        'Application.Restart()
                        Me.Enabled = False
                        Dim obj As New FrmMessageBox
                        obj.LblMessage.Text = Language.getMessage(mstrModuleName, 3, "You are logged out from the system. Reason : Application exceeded Idle time set in configuration. In order to relogin to system, Press Ok.")
                        obj.ShowDialog()
                            blnIsIdealTimeSet = True
                            Application.Restart()
                        'S.SANDEEP [14-May-2018] -- END

                        'If CBool(eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "You are logged out of the system. Reason : Application was idle for more time than set in configuration. In order to relogin into system Press Ok."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))) Then
                        '    Application.Restart()
                        'End If

                        'Pinkal (12-Oct-2011) -- End

                        
                    End If
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Application_Idle", mstrModuleName)
        End Try
    End Sub

#End Region

    'Pinkal (21-Jun-2011) -- End

	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Private Sub OtherSettings()
		Try
			Me.SuspendLayout()
			
			Call SetLanguage()
			
            Me.eZeeHeader.GradientColor1 = GUI._HeaderBackColor1
			Me.eZeeHeader.GradientColor2 = GUI._HeaderBackColor2 
			Me.eZeeHeader.BorderColor = GUI._HeaderBorderColor 
			Me.eZeeHeader.HeaderTextForeColor = GUI._HeaderTitleFontColor 
			Me.eZeeHeader.DescriptionForeColor = GUI._HeaderMessageFontColor 


			Me.btnCancel.GradientBackColor = GUI._ButttonBackColor 
			Me.btnCancel.GradientForeColor = GUI._ButttonFontColor

			Me.btnOpen.GradientBackColor = GUI._ButttonBackColor 
			Me.btnOpen.GradientForeColor = GUI._ButttonFontColor

			Me.btnUnlock.GradientBackColor = GUI._ButttonBackColor 
			Me.btnUnlock.GradientForeColor = GUI._ButttonFontColor


			Me.ResumeLayout()
		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "OtherSettings", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetLanguage()
		Try
			Me.Text = Language._Object.getCaption(Me.Name, Me.Text)
			
			Me.eZeeHeader.Title = Language._Object.getCaption(Me.eZeeHeader.Name & "_Title" , Me.eZeeHeader.Title)
			Me.eZeeHeader.Message = Language._Object.getCaption(Me.eZeeHeader.Name & "_Message" , Me.eZeeHeader.Message)
			Me.btnCancel.Text = Language._Object.getCaption(Me.btnCancel.Name, Me.btnCancel.Text)
			Me.btnOpen.Text = Language._Object.getCaption(Me.btnOpen.Name, Me.btnOpen.Text)
			Me.colhProperty.Text = Language._Object.getCaption(CStr(Me.colhProperty.Tag), Me.colhProperty.Text)
			Me.colhDatabase.Text = Language._Object.getCaption(CStr(Me.colhDatabase.Tag), Me.colhDatabase.Text)
			Me.colhcode.Text = Language._Object.getCaption(CStr(Me.colhcode.Tag), Me.colhcode.Text)
			Me.btnUnlock.Text = Language._Object.getCaption(Me.btnUnlock.Name, Me.btnUnlock.Text)

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetLanguage", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "Please select Company from the list.")
			Language.setMessage(mstrModuleName, 2, "Please select Database from the list.")
			Language.setMessage(mstrModuleName, 3, "Select Database")
			Language.setMessage(mstrModuleName, 4, "You are logged out from the system. Reason : Application exceeded Idle time set in configuration. In order to relogin to system, Press Ok.")
			Language.setMessage(mstrModuleName, 5, "Sorry, you cannot login to Aruti. Reason : You have exceeded number of company license limit. Please contact Aruti Support team for assistance.")
			Language.setMessage(mstrModuleName, 6, "Sorry, you cannot login to Aruti. Reason : You have exceeded number of Employee license limit. Please contact Aruti Support team for assistance.")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class