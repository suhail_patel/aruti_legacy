﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmProcessPayroll
    Inherits eZee.Common.eZeeForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmProcessPayroll))
        Me.pnlMainInfo = New System.Windows.Forms.Panel
        Me.eZeeHeader = New eZee.Common.eZeeHeader
        Me.gbFilterCriteria = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.lblPeriodName = New System.Windows.Forms.Label
        Me.bdgTotalOverDeduction = New Aruti.Data.Badge
        Me.gbProcessPayRollInfo = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.objlblTnAEndDate = New System.Windows.Forms.Label
        Me.lblTnAEndDate = New System.Windows.Forms.Label
        Me.chkUnProcessedEmpOnly = New System.Windows.Forms.CheckBox
        Me.cboModes = New System.Windows.Forms.ComboBox
        Me.lblModes = New System.Windows.Forms.Label
        Me.lblAsOnDate = New System.Windows.Forms.Label
        Me.dtpAsOnDate = New System.Windows.Forms.DateTimePicker
        Me.btnCostCenter = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnExempt = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnNew = New eZee.Common.eZeeLightButton(Me.components)
        Me.Panel1 = New System.Windows.Forms.Panel
        Me.lvED = New eZee.Common.eZeeListView(Me.components)
        Me.objcolhedunkid = New System.Windows.Forms.ColumnHeader
        Me.objcolhEmpUnkID = New System.Windows.Forms.ColumnHeader
        Me.colhTranHead = New System.Windows.Forms.ColumnHeader
        Me.colhTranHeadType = New System.Windows.Forms.ColumnHeader
        Me.colhCalcType = New System.Windows.Forms.ColumnHeader
        Me.colhAmount = New System.Windows.Forms.ColumnHeader
        Me.colhExempted = New System.Windows.Forms.ColumnHeader
        Me.colhCostCenter = New System.Windows.Forms.ColumnHeader
        Me.colhPeriod = New System.Windows.Forms.ColumnHeader
        Me.lnEmpPaySlipInfo = New eZee.Common.eZeeLine
        Me.btnEdit = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnDelete = New eZee.Common.eZeeLightButton(Me.components)
        Me.cboPayYear = New System.Windows.Forms.ComboBox
        Me.lblPayYear = New System.Windows.Forms.Label
        Me.chkExcludeWithPayment = New System.Windows.Forms.CheckBox
        Me.bdgUnpaid = New Aruti.Data.Badge
        Me.bdgTotalAuthorized = New Aruti.Data.Badge
        Me.bdgTotalPaid = New Aruti.Data.Badge
        Me.bdgTotalEmployee = New Aruti.Data.Badge
        Me.bdgProcessed = New Aruti.Data.Badge
        Me.bdgNotProcessed = New Aruti.Data.Badge
        Me.lnkAllocation = New System.Windows.Forms.LinkLabel
        Me.objbtnReset = New eZee.Common.eZeeSearchResetButton(Me.components)
        Me.objbtnSearch = New eZee.Common.eZeeSearchResetButton(Me.components)
        Me.objbtnSearchEmployee = New eZee.Common.eZeeGradientButton
        Me.cboEmployee = New System.Windows.Forms.ComboBox
        Me.lblEmployee = New System.Windows.Forms.Label
        Me.cboPayPeriod = New System.Windows.Forms.ComboBox
        Me.lblPayPeriod = New System.Windows.Forms.Label
        Me.objFooter = New eZee.Common.eZeeFooter
        Me.btnStop = New eZee.Common.eZeeLightButton(Me.components)
        Me.objlblProgress = New System.Windows.Forms.Label
        Me.lblSearchEmp = New System.Windows.Forms.Label
        Me.btnVoidPayroll = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnProcess = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnClose = New eZee.Common.eZeeLightButton(Me.components)
        Me.gbEmployeeList = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.objlblEmpCount = New System.Windows.Forms.Label
        Me.pnlEmployeeList = New System.Windows.Forms.Panel
        Me.objchkSelectAll = New System.Windows.Forms.CheckBox
        Me.dgEmployee = New System.Windows.Forms.DataGridView
        Me.objdgcolhCheck = New System.Windows.Forms.DataGridViewCheckBoxColumn
        Me.objdgcolhEmployeeunkid = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgColhEmpCode = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgColhEmployee = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhIsPaymentDone = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.txtSearchEmp = New System.Windows.Forms.TextBox
        Me.objbgwProcessPayroll = New System.ComponentModel.BackgroundWorker
        Me.DataGridViewTextBoxColumn1 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn2 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn3 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn4 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.pnlMainInfo.SuspendLayout()
        Me.gbFilterCriteria.SuspendLayout()
        Me.gbProcessPayRollInfo.SuspendLayout()
        Me.Panel1.SuspendLayout()
        CType(Me.objbtnReset, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.objbtnSearch, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.objFooter.SuspendLayout()
        Me.gbEmployeeList.SuspendLayout()
        Me.pnlEmployeeList.SuspendLayout()
        CType(Me.dgEmployee, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'pnlMainInfo
        '
        Me.pnlMainInfo.Controls.Add(Me.eZeeHeader)
        Me.pnlMainInfo.Controls.Add(Me.gbFilterCriteria)
        Me.pnlMainInfo.Controls.Add(Me.objFooter)
        Me.pnlMainInfo.Controls.Add(Me.gbEmployeeList)
        Me.pnlMainInfo.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pnlMainInfo.Location = New System.Drawing.Point(0, 0)
        Me.pnlMainInfo.Name = "pnlMainInfo"
        Me.pnlMainInfo.Size = New System.Drawing.Size(1018, 555)
        Me.pnlMainInfo.TabIndex = 0
        '
        'eZeeHeader
        '
        Me.eZeeHeader.BackColor = System.Drawing.SystemColors.Control
        Me.eZeeHeader.BorderColor = System.Drawing.SystemColors.ControlDark
        Me.eZeeHeader.DescriptionForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.eZeeHeader.Dock = System.Windows.Forms.DockStyle.Top
        Me.eZeeHeader.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.eZeeHeader.GradiantStyle = eZee.Common.eZeeHeader.GradientStyle.Central
        Me.eZeeHeader.GradientColor1 = System.Drawing.SystemColors.Window
        Me.eZeeHeader.GradientColor2 = System.Drawing.SystemColors.Control
        Me.eZeeHeader.HeaderTextForeColor = System.Drawing.SystemColors.ControlText
        Me.eZeeHeader.Icon = Nothing
        Me.eZeeHeader.Location = New System.Drawing.Point(0, 0)
        Me.eZeeHeader.Message = ""
        Me.eZeeHeader.Name = "eZeeHeader"
        Me.eZeeHeader.Size = New System.Drawing.Size(1018, 58)
        Me.eZeeHeader.TabIndex = 11
        Me.eZeeHeader.Title = "Process Payroll"
        '
        'gbFilterCriteria
        '
        Me.gbFilterCriteria.BorderColor = System.Drawing.Color.Black
        Me.gbFilterCriteria.Checked = False
        Me.gbFilterCriteria.CollapseAllExceptThis = False
        Me.gbFilterCriteria.CollapsedHoverImage = Nothing
        Me.gbFilterCriteria.CollapsedNormalImage = Nothing
        Me.gbFilterCriteria.CollapsedPressedImage = Nothing
        Me.gbFilterCriteria.CollapseOnLoad = False
        Me.gbFilterCriteria.Controls.Add(Me.lblPeriodName)
        Me.gbFilterCriteria.Controls.Add(Me.bdgTotalOverDeduction)
        Me.gbFilterCriteria.Controls.Add(Me.gbProcessPayRollInfo)
        Me.gbFilterCriteria.Controls.Add(Me.bdgUnpaid)
        Me.gbFilterCriteria.Controls.Add(Me.bdgTotalAuthorized)
        Me.gbFilterCriteria.Controls.Add(Me.bdgTotalPaid)
        Me.gbFilterCriteria.Controls.Add(Me.bdgTotalEmployee)
        Me.gbFilterCriteria.Controls.Add(Me.bdgProcessed)
        Me.gbFilterCriteria.Controls.Add(Me.bdgNotProcessed)
        Me.gbFilterCriteria.Controls.Add(Me.lnkAllocation)
        Me.gbFilterCriteria.Controls.Add(Me.objbtnReset)
        Me.gbFilterCriteria.Controls.Add(Me.objbtnSearch)
        Me.gbFilterCriteria.Controls.Add(Me.objbtnSearchEmployee)
        Me.gbFilterCriteria.Controls.Add(Me.cboEmployee)
        Me.gbFilterCriteria.Controls.Add(Me.lblEmployee)
        Me.gbFilterCriteria.Controls.Add(Me.cboPayPeriod)
        Me.gbFilterCriteria.Controls.Add(Me.lblPayPeriod)
        Me.gbFilterCriteria.ExpandedHoverImage = Nothing
        Me.gbFilterCriteria.ExpandedNormalImage = Nothing
        Me.gbFilterCriteria.ExpandedPressedImage = Nothing
        Me.gbFilterCriteria.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbFilterCriteria.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbFilterCriteria.HeaderHeight = 25
        Me.gbFilterCriteria.HeaderMessage = ""
        Me.gbFilterCriteria.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbFilterCriteria.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbFilterCriteria.HeightOnCollapse = 0
        Me.gbFilterCriteria.LeftTextSpace = 0
        Me.gbFilterCriteria.Location = New System.Drawing.Point(259, 66)
        Me.gbFilterCriteria.Name = "gbFilterCriteria"
        Me.gbFilterCriteria.OpenHeight = 300
        Me.gbFilterCriteria.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbFilterCriteria.ShowBorder = True
        Me.gbFilterCriteria.ShowCheckBox = False
        Me.gbFilterCriteria.ShowCollapseButton = False
        Me.gbFilterCriteria.ShowDefaultBorderColor = True
        Me.gbFilterCriteria.ShowDownButton = False
        Me.gbFilterCriteria.ShowHeader = True
        Me.gbFilterCriteria.Size = New System.Drawing.Size(747, 428)
        Me.gbFilterCriteria.TabIndex = 0
        Me.gbFilterCriteria.Temp = 0
        Me.gbFilterCriteria.Text = "Filter Criteria"
        Me.gbFilterCriteria.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblPeriodName
        '
        Me.lblPeriodName.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPeriodName.Location = New System.Drawing.Point(236, 1)
        Me.lblPeriodName.Name = "lblPeriodName"
        Me.lblPeriodName.Size = New System.Drawing.Size(275, 21)
        Me.lblPeriodName.TabIndex = 320
        Me.lblPeriodName.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'bdgTotalOverDeduction
        '
        Me.bdgTotalOverDeduction.BackColor = System.Drawing.Color.Transparent
        Me.bdgTotalOverDeduction.Cursor = System.Windows.Forms.Cursors.Hand
        Me.bdgTotalOverDeduction.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.bdgTotalOverDeduction.FontSize_Caption = 8.25!
        Me.bdgTotalOverDeduction.FontSize_Number = 16.0!
        Me.bdgTotalOverDeduction.Height_Caption = 17
        Me.bdgTotalOverDeduction.Location = New System.Drawing.Point(6, 366)
        Me.bdgTotalOverDeduction.Name = "bdgTotalOverDeduction"
        Me.bdgTotalOverDeduction.Size = New System.Drawing.Size(142, 50)
        Me.bdgTotalOverDeduction.TabIndex = 324
        Me.bdgTotalOverDeduction.Text_Caption = "Not Payable"
        Me.bdgTotalOverDeduction.Text_Number = ""
        '
        'gbProcessPayRollInfo
        '
        Me.gbProcessPayRollInfo.BorderColor = System.Drawing.Color.Black
        Me.gbProcessPayRollInfo.Checked = False
        Me.gbProcessPayRollInfo.CollapseAllExceptThis = False
        Me.gbProcessPayRollInfo.CollapsedHoverImage = Nothing
        Me.gbProcessPayRollInfo.CollapsedNormalImage = Nothing
        Me.gbProcessPayRollInfo.CollapsedPressedImage = Nothing
        Me.gbProcessPayRollInfo.CollapseOnLoad = False
        Me.gbProcessPayRollInfo.Controls.Add(Me.objlblTnAEndDate)
        Me.gbProcessPayRollInfo.Controls.Add(Me.lblTnAEndDate)
        Me.gbProcessPayRollInfo.Controls.Add(Me.chkUnProcessedEmpOnly)
        Me.gbProcessPayRollInfo.Controls.Add(Me.cboModes)
        Me.gbProcessPayRollInfo.Controls.Add(Me.lblModes)
        Me.gbProcessPayRollInfo.Controls.Add(Me.lblAsOnDate)
        Me.gbProcessPayRollInfo.Controls.Add(Me.dtpAsOnDate)
        Me.gbProcessPayRollInfo.Controls.Add(Me.btnCostCenter)
        Me.gbProcessPayRollInfo.Controls.Add(Me.btnExempt)
        Me.gbProcessPayRollInfo.Controls.Add(Me.btnNew)
        Me.gbProcessPayRollInfo.Controls.Add(Me.Panel1)
        Me.gbProcessPayRollInfo.Controls.Add(Me.lnEmpPaySlipInfo)
        Me.gbProcessPayRollInfo.Controls.Add(Me.btnEdit)
        Me.gbProcessPayRollInfo.Controls.Add(Me.btnDelete)
        Me.gbProcessPayRollInfo.Controls.Add(Me.cboPayYear)
        Me.gbProcessPayRollInfo.Controls.Add(Me.lblPayYear)
        Me.gbProcessPayRollInfo.Controls.Add(Me.chkExcludeWithPayment)
        Me.gbProcessPayRollInfo.ExpandedHoverImage = Nothing
        Me.gbProcessPayRollInfo.ExpandedNormalImage = Nothing
        Me.gbProcessPayRollInfo.ExpandedPressedImage = Nothing
        Me.gbProcessPayRollInfo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbProcessPayRollInfo.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbProcessPayRollInfo.HeaderHeight = 25
        Me.gbProcessPayRollInfo.HeaderMessage = ""
        Me.gbProcessPayRollInfo.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbProcessPayRollInfo.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbProcessPayRollInfo.HeightOnCollapse = 0
        Me.gbProcessPayRollInfo.LeftTextSpace = 0
        Me.gbProcessPayRollInfo.Location = New System.Drawing.Point(155, 58)
        Me.gbProcessPayRollInfo.Name = "gbProcessPayRollInfo"
        Me.gbProcessPayRollInfo.OpenHeight = 182
        Me.gbProcessPayRollInfo.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbProcessPayRollInfo.ShowBorder = True
        Me.gbProcessPayRollInfo.ShowCheckBox = False
        Me.gbProcessPayRollInfo.ShowCollapseButton = False
        Me.gbProcessPayRollInfo.ShowDefaultBorderColor = True
        Me.gbProcessPayRollInfo.ShowDownButton = False
        Me.gbProcessPayRollInfo.ShowHeader = True
        Me.gbProcessPayRollInfo.Size = New System.Drawing.Size(584, 358)
        Me.gbProcessPayRollInfo.TabIndex = 1
        Me.gbProcessPayRollInfo.Temp = 0
        Me.gbProcessPayRollInfo.Text = "Payroll Process Information"
        Me.gbProcessPayRollInfo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objlblTnAEndDate
        '
        Me.objlblTnAEndDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objlblTnAEndDate.Location = New System.Drawing.Point(463, 58)
        Me.objlblTnAEndDate.Name = "objlblTnAEndDate"
        Me.objlblTnAEndDate.Size = New System.Drawing.Size(95, 16)
        Me.objlblTnAEndDate.TabIndex = 166
        Me.objlblTnAEndDate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblTnAEndDate
        '
        Me.lblTnAEndDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTnAEndDate.Location = New System.Drawing.Point(384, 58)
        Me.lblTnAEndDate.Name = "lblTnAEndDate"
        Me.lblTnAEndDate.Size = New System.Drawing.Size(76, 16)
        Me.lblTnAEndDate.TabIndex = 165
        Me.lblTnAEndDate.Text = "TnA End Date"
        Me.lblTnAEndDate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'chkUnProcessedEmpOnly
        '
        Me.chkUnProcessedEmpOnly.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkUnProcessedEmpOnly.Location = New System.Drawing.Point(11, 58)
        Me.chkUnProcessedEmpOnly.Name = "chkUnProcessedEmpOnly"
        Me.chkUnProcessedEmpOnly.Size = New System.Drawing.Size(358, 17)
        Me.chkUnProcessedEmpOnly.TabIndex = 163
        Me.chkUnProcessedEmpOnly.Text = "Show Unprocessed Employee As On Last Date of Selected Period"
        Me.chkUnProcessedEmpOnly.UseVisualStyleBackColor = True
        '
        'cboModes
        '
        Me.cboModes.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboModes.DropDownWidth = 300
        Me.cboModes.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboModes.FormattingEnabled = True
        Me.cboModes.Location = New System.Drawing.Point(84, 34)
        Me.cboModes.Name = "cboModes"
        Me.cboModes.Size = New System.Drawing.Size(180, 21)
        Me.cboModes.TabIndex = 158
        '
        'lblModes
        '
        Me.lblModes.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblModes.Location = New System.Drawing.Point(11, 36)
        Me.lblModes.Name = "lblModes"
        Me.lblModes.Size = New System.Drawing.Size(67, 16)
        Me.lblModes.TabIndex = 159
        Me.lblModes.Text = "Modes"
        Me.lblModes.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblAsOnDate
        '
        Me.lblAsOnDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblAsOnDate.Location = New System.Drawing.Point(384, 36)
        Me.lblAsOnDate.Name = "lblAsOnDate"
        Me.lblAsOnDate.Size = New System.Drawing.Size(76, 16)
        Me.lblAsOnDate.TabIndex = 156
        Me.lblAsOnDate.Text = "As On Date"
        Me.lblAsOnDate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'dtpAsOnDate
        '
        Me.dtpAsOnDate.Checked = False
        Me.dtpAsOnDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpAsOnDate.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpAsOnDate.Location = New System.Drawing.Point(463, 34)
        Me.dtpAsOnDate.Name = "dtpAsOnDate"
        Me.dtpAsOnDate.Size = New System.Drawing.Size(108, 21)
        Me.dtpAsOnDate.TabIndex = 2
        '
        'btnCostCenter
        '
        Me.btnCostCenter.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnCostCenter.BackColor = System.Drawing.Color.White
        Me.btnCostCenter.BackgroundImage = CType(resources.GetObject("btnCostCenter.BackgroundImage"), System.Drawing.Image)
        Me.btnCostCenter.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnCostCenter.BorderColor = System.Drawing.Color.Empty
        Me.btnCostCenter.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnCostCenter.FlatAppearance.BorderSize = 0
        Me.btnCostCenter.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnCostCenter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnCostCenter.ForeColor = System.Drawing.Color.Black
        Me.btnCostCenter.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnCostCenter.GradientForeColor = System.Drawing.Color.Black
        Me.btnCostCenter.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnCostCenter.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnCostCenter.Location = New System.Drawing.Point(476, 322)
        Me.btnCostCenter.Name = "btnCostCenter"
        Me.btnCostCenter.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnCostCenter.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnCostCenter.Size = New System.Drawing.Size(97, 30)
        Me.btnCostCenter.TabIndex = 8
        Me.btnCostCenter.Text = "Cost Center"
        Me.btnCostCenter.UseVisualStyleBackColor = True
        '
        'btnExempt
        '
        Me.btnExempt.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnExempt.BackColor = System.Drawing.Color.White
        Me.btnExempt.BackgroundImage = CType(resources.GetObject("btnExempt.BackgroundImage"), System.Drawing.Image)
        Me.btnExempt.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnExempt.BorderColor = System.Drawing.Color.Empty
        Me.btnExempt.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnExempt.FlatAppearance.BorderSize = 0
        Me.btnExempt.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnExempt.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnExempt.ForeColor = System.Drawing.Color.Black
        Me.btnExempt.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnExempt.GradientForeColor = System.Drawing.Color.Black
        Me.btnExempt.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnExempt.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnExempt.Location = New System.Drawing.Point(373, 322)
        Me.btnExempt.Name = "btnExempt"
        Me.btnExempt.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnExempt.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnExempt.Size = New System.Drawing.Size(97, 30)
        Me.btnExempt.TabIndex = 7
        Me.btnExempt.Text = "E&xempt"
        Me.btnExempt.UseVisualStyleBackColor = True
        '
        'btnNew
        '
        Me.btnNew.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnNew.BackColor = System.Drawing.Color.White
        Me.btnNew.BackgroundImage = CType(resources.GetObject("btnNew.BackgroundImage"), System.Drawing.Image)
        Me.btnNew.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnNew.BorderColor = System.Drawing.Color.Empty
        Me.btnNew.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnNew.FlatAppearance.BorderSize = 0
        Me.btnNew.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnNew.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnNew.ForeColor = System.Drawing.Color.Black
        Me.btnNew.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnNew.GradientForeColor = System.Drawing.Color.Black
        Me.btnNew.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnNew.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnNew.Location = New System.Drawing.Point(64, 322)
        Me.btnNew.Name = "btnNew"
        Me.btnNew.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnNew.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnNew.Size = New System.Drawing.Size(97, 30)
        Me.btnNew.TabIndex = 4
        Me.btnNew.Text = "&New"
        Me.btnNew.UseVisualStyleBackColor = True
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(Me.lvED)
        Me.Panel1.Location = New System.Drawing.Point(11, 98)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(562, 218)
        Me.Panel1.TabIndex = 148
        '
        'lvED
        '
        Me.lvED.BackColorOnChecked = True
        Me.lvED.ColumnHeaders = Nothing
        Me.lvED.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.objcolhedunkid, Me.objcolhEmpUnkID, Me.colhTranHead, Me.colhTranHeadType, Me.colhCalcType, Me.colhAmount, Me.colhExempted, Me.colhCostCenter, Me.colhPeriod})
        Me.lvED.CompulsoryColumns = ""
        Me.lvED.Dock = System.Windows.Forms.DockStyle.Fill
        Me.lvED.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lvED.FullRowSelect = True
        Me.lvED.GridLines = True
        Me.lvED.GroupingColumn = Nothing
        Me.lvED.HideSelection = False
        Me.lvED.Location = New System.Drawing.Point(0, 0)
        Me.lvED.MinColumnWidth = 50
        Me.lvED.MultiSelect = False
        Me.lvED.Name = "lvED"
        Me.lvED.OptionalColumns = ""
        Me.lvED.ShowMoreItem = False
        Me.lvED.ShowSaveItem = False
        Me.lvED.ShowSelectAll = True
        Me.lvED.ShowSizeAllColumnsToFit = True
        Me.lvED.Size = New System.Drawing.Size(562, 218)
        Me.lvED.Sortable = True
        Me.lvED.TabIndex = 0
        Me.lvED.UseCompatibleStateImageBehavior = False
        Me.lvED.View = System.Windows.Forms.View.Details
        '
        'objcolhedunkid
        '
        Me.objcolhedunkid.Tag = "objcolhedunkid"
        Me.objcolhedunkid.Text = "edunkid"
        Me.objcolhedunkid.Width = 0
        '
        'objcolhEmpUnkID
        '
        Me.objcolhEmpUnkID.Tag = "objcolhEmpUnkID"
        Me.objcolhEmpUnkID.Text = "EmployeeUnkID"
        Me.objcolhEmpUnkID.Width = 0
        '
        'colhTranHead
        '
        Me.colhTranHead.Tag = "colhTranHead"
        Me.colhTranHead.Text = "Transaction Head"
        Me.colhTranHead.Width = 150
        '
        'colhTranHeadType
        '
        Me.colhTranHeadType.Tag = "colhTranHeadType"
        Me.colhTranHeadType.Text = "Transaction Head Type"
        Me.colhTranHeadType.Width = 160
        '
        'colhCalcType
        '
        Me.colhCalcType.Tag = "colhCalcType"
        Me.colhCalcType.Text = "Calculation Type"
        Me.colhCalcType.Width = 100
        '
        'colhAmount
        '
        Me.colhAmount.Tag = "colhAmount"
        Me.colhAmount.Text = "Amount"
        Me.colhAmount.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.colhAmount.Width = 120
        '
        'colhExempted
        '
        Me.colhExempted.Tag = "colhExempted"
        Me.colhExempted.Text = "Exempted"
        Me.colhExempted.Width = 65
        '
        'colhCostCenter
        '
        Me.colhCostCenter.Tag = "colhCostCenter"
        Me.colhCostCenter.Text = "Cost Center"
        Me.colhCostCenter.Width = 75
        '
        'colhPeriod
        '
        Me.colhPeriod.Tag = "colhPeriod"
        Me.colhPeriod.Text = "Period"
        Me.colhPeriod.Width = 0
        '
        'lnEmpPaySlipInfo
        '
        Me.lnEmpPaySlipInfo.LineOrientation = eZee.Common.eZeeLine.Orientation.Horizontal
        Me.lnEmpPaySlipInfo.Location = New System.Drawing.Point(8, 80)
        Me.lnEmpPaySlipInfo.Name = "lnEmpPaySlipInfo"
        Me.lnEmpPaySlipInfo.Size = New System.Drawing.Size(565, 15)
        Me.lnEmpPaySlipInfo.TabIndex = 3
        Me.lnEmpPaySlipInfo.Text = "Employee Earning and Deduction Information"
        '
        'btnEdit
        '
        Me.btnEdit.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnEdit.BackColor = System.Drawing.Color.White
        Me.btnEdit.BackgroundImage = CType(resources.GetObject("btnEdit.BackgroundImage"), System.Drawing.Image)
        Me.btnEdit.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnEdit.BorderColor = System.Drawing.Color.Empty
        Me.btnEdit.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnEdit.FlatAppearance.BorderSize = 0
        Me.btnEdit.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnEdit.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnEdit.ForeColor = System.Drawing.Color.Black
        Me.btnEdit.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnEdit.GradientForeColor = System.Drawing.Color.Black
        Me.btnEdit.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnEdit.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnEdit.Location = New System.Drawing.Point(167, 322)
        Me.btnEdit.Name = "btnEdit"
        Me.btnEdit.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnEdit.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnEdit.Size = New System.Drawing.Size(97, 30)
        Me.btnEdit.TabIndex = 5
        Me.btnEdit.Text = "&Edit"
        Me.btnEdit.UseVisualStyleBackColor = True
        '
        'btnDelete
        '
        Me.btnDelete.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnDelete.BackColor = System.Drawing.Color.White
        Me.btnDelete.BackgroundImage = CType(resources.GetObject("btnDelete.BackgroundImage"), System.Drawing.Image)
        Me.btnDelete.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnDelete.BorderColor = System.Drawing.Color.Empty
        Me.btnDelete.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnDelete.FlatAppearance.BorderSize = 0
        Me.btnDelete.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnDelete.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnDelete.ForeColor = System.Drawing.Color.Black
        Me.btnDelete.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnDelete.GradientForeColor = System.Drawing.Color.Black
        Me.btnDelete.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnDelete.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnDelete.Location = New System.Drawing.Point(270, 322)
        Me.btnDelete.Name = "btnDelete"
        Me.btnDelete.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnDelete.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnDelete.Size = New System.Drawing.Size(97, 30)
        Me.btnDelete.TabIndex = 6
        Me.btnDelete.Text = "&Delete"
        Me.btnDelete.UseVisualStyleBackColor = True
        '
        'cboPayYear
        '
        Me.cboPayYear.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboPayYear.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboPayYear.FormattingEnabled = True
        Me.cboPayYear.Location = New System.Drawing.Point(343, 56)
        Me.cboPayYear.Name = "cboPayYear"
        Me.cboPayYear.Size = New System.Drawing.Size(10, 21)
        Me.cboPayYear.TabIndex = 0
        Me.cboPayYear.Visible = False
        '
        'lblPayYear
        '
        Me.lblPayYear.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPayYear.Location = New System.Drawing.Point(351, 58)
        Me.lblPayYear.Name = "lblPayYear"
        Me.lblPayYear.Size = New System.Drawing.Size(10, 16)
        Me.lblPayYear.TabIndex = 139
        Me.lblPayYear.Text = "Pay Year"
        Me.lblPayYear.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.lblPayYear.Visible = False
        '
        'chkExcludeWithPayment
        '
        Me.chkExcludeWithPayment.AutoSize = True
        Me.chkExcludeWithPayment.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkExcludeWithPayment.Location = New System.Drawing.Point(242, 5)
        Me.chkExcludeWithPayment.Name = "chkExcludeWithPayment"
        Me.chkExcludeWithPayment.Size = New System.Drawing.Size(208, 17)
        Me.chkExcludeWithPayment.TabIndex = 161
        Me.chkExcludeWithPayment.Text = "Exclude Employees if Payment is done"
        Me.chkExcludeWithPayment.UseVisualStyleBackColor = True
        '
        'bdgUnpaid
        '
        Me.bdgUnpaid.BackColor = System.Drawing.Color.Transparent
        Me.bdgUnpaid.Cursor = System.Windows.Forms.Cursors.Hand
        Me.bdgUnpaid.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.bdgUnpaid.FontSize_Caption = 8.25!
        Me.bdgUnpaid.FontSize_Number = 16.0!
        Me.bdgUnpaid.Height_Caption = 17
        Me.bdgUnpaid.Location = New System.Drawing.Point(6, 198)
        Me.bdgUnpaid.Name = "bdgUnpaid"
        Me.bdgUnpaid.Size = New System.Drawing.Size(142, 50)
        Me.bdgUnpaid.TabIndex = 322
        Me.bdgUnpaid.Text_Caption = "Unpaid"
        Me.bdgUnpaid.Text_Number = ""
        '
        'bdgTotalAuthorized
        '
        Me.bdgTotalAuthorized.BackColor = System.Drawing.Color.Transparent
        Me.bdgTotalAuthorized.Cursor = System.Windows.Forms.Cursors.Hand
        Me.bdgTotalAuthorized.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.bdgTotalAuthorized.FontSize_Caption = 8.25!
        Me.bdgTotalAuthorized.FontSize_Number = 16.0!
        Me.bdgTotalAuthorized.Height_Caption = 17
        Me.bdgTotalAuthorized.Location = New System.Drawing.Point(6, 310)
        Me.bdgTotalAuthorized.Name = "bdgTotalAuthorized"
        Me.bdgTotalAuthorized.Size = New System.Drawing.Size(142, 50)
        Me.bdgTotalAuthorized.TabIndex = 318
        Me.bdgTotalAuthorized.Text_Caption = "Authorized"
        Me.bdgTotalAuthorized.Text_Number = ""
        '
        'bdgTotalPaid
        '
        Me.bdgTotalPaid.BackColor = System.Drawing.Color.Transparent
        Me.bdgTotalPaid.Cursor = System.Windows.Forms.Cursors.Hand
        Me.bdgTotalPaid.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.bdgTotalPaid.FontSize_Caption = 8.25!
        Me.bdgTotalPaid.FontSize_Number = 16.0!
        Me.bdgTotalPaid.Height_Caption = 17
        Me.bdgTotalPaid.Location = New System.Drawing.Point(6, 254)
        Me.bdgTotalPaid.Name = "bdgTotalPaid"
        Me.bdgTotalPaid.Size = New System.Drawing.Size(142, 50)
        Me.bdgTotalPaid.TabIndex = 317
        Me.bdgTotalPaid.Text_Caption = "Paid"
        Me.bdgTotalPaid.Text_Number = ""
        '
        'bdgTotalEmployee
        '
        Me.bdgTotalEmployee.BackColor = System.Drawing.Color.Transparent
        Me.bdgTotalEmployee.Cursor = System.Windows.Forms.Cursors.Hand
        Me.bdgTotalEmployee.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.bdgTotalEmployee.FontSize_Caption = 8.25!
        Me.bdgTotalEmployee.FontSize_Number = 16.0!
        Me.bdgTotalEmployee.Height_Caption = 17
        Me.bdgTotalEmployee.Location = New System.Drawing.Point(6, 142)
        Me.bdgTotalEmployee.Name = "bdgTotalEmployee"
        Me.bdgTotalEmployee.Size = New System.Drawing.Size(142, 50)
        Me.bdgTotalEmployee.TabIndex = 315
        Me.bdgTotalEmployee.Text_Caption = "Total Employees"
        Me.bdgTotalEmployee.Text_Number = ""
        '
        'bdgProcessed
        '
        Me.bdgProcessed.BackColor = System.Drawing.Color.Transparent
        Me.bdgProcessed.Cursor = System.Windows.Forms.Cursors.Hand
        Me.bdgProcessed.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.bdgProcessed.FontSize_Caption = 8.25!
        Me.bdgProcessed.FontSize_Number = 16.0!
        Me.bdgProcessed.Height_Caption = 17
        Me.bdgProcessed.Location = New System.Drawing.Point(6, 85)
        Me.bdgProcessed.Name = "bdgProcessed"
        Me.bdgProcessed.Size = New System.Drawing.Size(142, 50)
        Me.bdgProcessed.TabIndex = 314
        Me.bdgProcessed.Text_Caption = "Processed"
        Me.bdgProcessed.Text_Number = ""
        '
        'bdgNotProcessed
        '
        Me.bdgNotProcessed.BackColor = System.Drawing.Color.Transparent
        Me.bdgNotProcessed.Cursor = System.Windows.Forms.Cursors.Hand
        Me.bdgNotProcessed.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.bdgNotProcessed.FontSize_Caption = 8.25!
        Me.bdgNotProcessed.FontSize_Number = 16.0!
        Me.bdgNotProcessed.Height_Caption = 17
        Me.bdgNotProcessed.Location = New System.Drawing.Point(6, 29)
        Me.bdgNotProcessed.Name = "bdgNotProcessed"
        Me.bdgNotProcessed.Size = New System.Drawing.Size(142, 50)
        Me.bdgNotProcessed.TabIndex = 312
        Me.bdgNotProcessed.Text_Caption = "Not Processed"
        Me.bdgNotProcessed.Text_Number = ""
        '
        'lnkAllocation
        '
        Me.lnkAllocation.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lnkAllocation.BackColor = System.Drawing.Color.Transparent
        Me.lnkAllocation.Location = New System.Drawing.Point(585, 5)
        Me.lnkAllocation.Name = "lnkAllocation"
        Me.lnkAllocation.Size = New System.Drawing.Size(106, 13)
        Me.lnkAllocation.TabIndex = 310
        Me.lnkAllocation.TabStop = True
        Me.lnkAllocation.Text = "Advance Filter"
        Me.lnkAllocation.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'objbtnReset
        '
        Me.objbtnReset.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.objbtnReset.BackColor = System.Drawing.Color.Transparent
        Me.objbtnReset.ButtonType = eZee.Common.eZeeSearchResetButton.EnumButtonType.Reset
        Me.objbtnReset.Image = CType(resources.GetObject("objbtnReset.Image"), System.Drawing.Image)
        Me.objbtnReset.Location = New System.Drawing.Point(720, 0)
        Me.objbtnReset.Name = "objbtnReset"
        Me.objbtnReset.ResultMessage = ""
        Me.objbtnReset.SearchMessage = ""
        Me.objbtnReset.Size = New System.Drawing.Size(24, 24)
        Me.objbtnReset.TabIndex = 69
        Me.objbtnReset.TabStop = False
        '
        'objbtnSearch
        '
        Me.objbtnSearch.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.objbtnSearch.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearch.ButtonType = eZee.Common.eZeeSearchResetButton.EnumButtonType.Search
        Me.objbtnSearch.Image = CType(resources.GetObject("objbtnSearch.Image"), System.Drawing.Image)
        Me.objbtnSearch.Location = New System.Drawing.Point(697, 0)
        Me.objbtnSearch.Name = "objbtnSearch"
        Me.objbtnSearch.ResultMessage = ""
        Me.objbtnSearch.SearchMessage = ""
        Me.objbtnSearch.Size = New System.Drawing.Size(24, 24)
        Me.objbtnSearch.TabIndex = 68
        Me.objbtnSearch.TabStop = False
        '
        'objbtnSearchEmployee
        '
        Me.objbtnSearchEmployee.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchEmployee.BorderSelected = False
        Me.objbtnSearchEmployee.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchEmployee.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objbtnSearchEmployee.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchEmployee.Location = New System.Drawing.Point(711, 31)
        Me.objbtnSearchEmployee.Name = "objbtnSearchEmployee"
        Me.objbtnSearchEmployee.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchEmployee.TabIndex = 2
        '
        'cboEmployee
        '
        Me.cboEmployee.DropDownWidth = 300
        Me.cboEmployee.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboEmployee.FormattingEnabled = True
        Me.cboEmployee.Location = New System.Drawing.Point(239, 31)
        Me.cboEmployee.Name = "cboEmployee"
        Me.cboEmployee.Size = New System.Drawing.Size(466, 21)
        Me.cboEmployee.TabIndex = 1
        '
        'lblEmployee
        '
        Me.lblEmployee.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEmployee.Location = New System.Drawing.Point(155, 34)
        Me.lblEmployee.Name = "lblEmployee"
        Me.lblEmployee.Size = New System.Drawing.Size(77, 15)
        Me.lblEmployee.TabIndex = 85
        Me.lblEmployee.Text = "Employee"
        '
        'cboPayPeriod
        '
        Me.cboPayPeriod.DropDownWidth = 150
        Me.cboPayPeriod.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboPayPeriod.FormattingEnabled = True
        Me.cboPayPeriod.Location = New System.Drawing.Point(503, 31)
        Me.cboPayPeriod.Name = "cboPayPeriod"
        Me.cboPayPeriod.Size = New System.Drawing.Size(118, 21)
        Me.cboPayPeriod.TabIndex = 0
        Me.cboPayPeriod.Visible = False
        '
        'lblPayPeriod
        '
        Me.lblPayPeriod.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPayPeriod.Location = New System.Drawing.Point(503, 33)
        Me.lblPayPeriod.Name = "lblPayPeriod"
        Me.lblPayPeriod.Size = New System.Drawing.Size(71, 16)
        Me.lblPayPeriod.TabIndex = 141
        Me.lblPayPeriod.Text = "Pay Period"
        Me.lblPayPeriod.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.lblPayPeriod.Visible = False
        '
        'objFooter
        '
        Me.objFooter.BorderColor = System.Drawing.Color.Silver
        Me.objFooter.Controls.Add(Me.btnStop)
        Me.objFooter.Controls.Add(Me.objlblProgress)
        Me.objFooter.Controls.Add(Me.lblSearchEmp)
        Me.objFooter.Controls.Add(Me.btnVoidPayroll)
        Me.objFooter.Controls.Add(Me.btnProcess)
        Me.objFooter.Controls.Add(Me.btnClose)
        Me.objFooter.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.objFooter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objFooter.GradiantStyle = eZee.Common.eZeeFooter.GradientStyle.Up
        Me.objFooter.GradientColor1 = System.Drawing.SystemColors.Control
        Me.objFooter.GradientColor2 = System.Drawing.SystemColors.Control
        Me.objFooter.Location = New System.Drawing.Point(0, 500)
        Me.objFooter.Name = "objFooter"
        Me.objFooter.Size = New System.Drawing.Size(1018, 55)
        Me.objFooter.TabIndex = 3
        '
        'btnStop
        '
        Me.btnStop.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnStop.BackColor = System.Drawing.Color.White
        Me.btnStop.BackgroundImage = CType(resources.GetObject("btnStop.BackgroundImage"), System.Drawing.Image)
        Me.btnStop.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnStop.BorderColor = System.Drawing.Color.Empty
        Me.btnStop.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnStop.FlatAppearance.BorderSize = 0
        Me.btnStop.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnStop.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnStop.ForeColor = System.Drawing.Color.Black
        Me.btnStop.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnStop.GradientForeColor = System.Drawing.Color.Black
        Me.btnStop.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnStop.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnStop.Location = New System.Drawing.Point(691, 13)
        Me.btnStop.Name = "btnStop"
        Me.btnStop.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnStop.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnStop.Size = New System.Drawing.Size(109, 30)
        Me.btnStop.TabIndex = 3
        Me.btnStop.Text = "&Stop Process"
        Me.btnStop.UseVisualStyleBackColor = True
        Me.btnStop.Visible = False
        '
        'objlblProgress
        '
        Me.objlblProgress.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objlblProgress.Location = New System.Drawing.Point(496, 22)
        Me.objlblProgress.Name = "objlblProgress"
        Me.objlblProgress.Size = New System.Drawing.Size(141, 13)
        Me.objlblProgress.TabIndex = 14
        '
        'lblSearchEmp
        '
        Me.lblSearchEmp.BackColor = System.Drawing.Color.Transparent
        Me.lblSearchEmp.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblSearchEmp.Location = New System.Drawing.Point(10, 22)
        Me.lblSearchEmp.Name = "lblSearchEmp"
        Me.lblSearchEmp.Size = New System.Drawing.Size(98, 13)
        Me.lblSearchEmp.TabIndex = 13
        Me.lblSearchEmp.Text = "Search Employee"
        Me.lblSearchEmp.Visible = False
        '
        'btnVoidPayroll
        '
        Me.btnVoidPayroll.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnVoidPayroll.BackColor = System.Drawing.Color.White
        Me.btnVoidPayroll.BackgroundImage = CType(resources.GetObject("btnVoidPayroll.BackgroundImage"), System.Drawing.Image)
        Me.btnVoidPayroll.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnVoidPayroll.BorderColor = System.Drawing.Color.Empty
        Me.btnVoidPayroll.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnVoidPayroll.FlatAppearance.BorderSize = 0
        Me.btnVoidPayroll.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnVoidPayroll.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnVoidPayroll.ForeColor = System.Drawing.Color.Black
        Me.btnVoidPayroll.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnVoidPayroll.GradientForeColor = System.Drawing.Color.Black
        Me.btnVoidPayroll.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnVoidPayroll.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnVoidPayroll.Location = New System.Drawing.Point(305, 13)
        Me.btnVoidPayroll.Name = "btnVoidPayroll"
        Me.btnVoidPayroll.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnVoidPayroll.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnVoidPayroll.Size = New System.Drawing.Size(97, 30)
        Me.btnVoidPayroll.TabIndex = 2
        Me.btnVoidPayroll.Text = "&Void Payroll"
        Me.btnVoidPayroll.UseVisualStyleBackColor = True
        '
        'btnProcess
        '
        Me.btnProcess.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnProcess.BackColor = System.Drawing.Color.White
        Me.btnProcess.BackgroundImage = CType(resources.GetObject("btnProcess.BackgroundImage"), System.Drawing.Image)
        Me.btnProcess.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnProcess.BorderColor = System.Drawing.Color.Empty
        Me.btnProcess.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnProcess.FlatAppearance.BorderSize = 0
        Me.btnProcess.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnProcess.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnProcess.ForeColor = System.Drawing.Color.Black
        Me.btnProcess.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnProcess.GradientForeColor = System.Drawing.Color.Black
        Me.btnProcess.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnProcess.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnProcess.Location = New System.Drawing.Point(806, 13)
        Me.btnProcess.Name = "btnProcess"
        Me.btnProcess.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnProcess.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnProcess.Size = New System.Drawing.Size(97, 30)
        Me.btnProcess.TabIndex = 0
        Me.btnProcess.Text = "&Process"
        Me.btnProcess.UseVisualStyleBackColor = True
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnClose.BackColor = System.Drawing.Color.White
        Me.btnClose.BackgroundImage = CType(resources.GetObject("btnClose.BackgroundImage"), System.Drawing.Image)
        Me.btnClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnClose.BorderColor = System.Drawing.Color.Empty
        Me.btnClose.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnClose.FlatAppearance.BorderSize = 0
        Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClose.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.ForeColor = System.Drawing.Color.Black
        Me.btnClose.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnClose.GradientForeColor = System.Drawing.Color.Black
        Me.btnClose.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Location = New System.Drawing.Point(909, 13)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Size = New System.Drawing.Size(97, 30)
        Me.btnClose.TabIndex = 1
        Me.btnClose.Text = "&Close"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'gbEmployeeList
        '
        Me.gbEmployeeList.BorderColor = System.Drawing.Color.Black
        Me.gbEmployeeList.Checked = False
        Me.gbEmployeeList.CollapseAllExceptThis = False
        Me.gbEmployeeList.CollapsedHoverImage = Nothing
        Me.gbEmployeeList.CollapsedNormalImage = Nothing
        Me.gbEmployeeList.CollapsedPressedImage = Nothing
        Me.gbEmployeeList.CollapseOnLoad = False
        Me.gbEmployeeList.Controls.Add(Me.objlblEmpCount)
        Me.gbEmployeeList.Controls.Add(Me.pnlEmployeeList)
        Me.gbEmployeeList.ExpandedHoverImage = Nothing
        Me.gbEmployeeList.ExpandedNormalImage = Nothing
        Me.gbEmployeeList.ExpandedPressedImage = Nothing
        Me.gbEmployeeList.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbEmployeeList.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbEmployeeList.HeaderHeight = 25
        Me.gbEmployeeList.HeaderMessage = ""
        Me.gbEmployeeList.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbEmployeeList.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbEmployeeList.HeightOnCollapse = 0
        Me.gbEmployeeList.LeftTextSpace = 0
        Me.gbEmployeeList.Location = New System.Drawing.Point(12, 66)
        Me.gbEmployeeList.Name = "gbEmployeeList"
        Me.gbEmployeeList.OpenHeight = 300
        Me.gbEmployeeList.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbEmployeeList.ShowBorder = True
        Me.gbEmployeeList.ShowCheckBox = False
        Me.gbEmployeeList.ShowCollapseButton = False
        Me.gbEmployeeList.ShowDefaultBorderColor = True
        Me.gbEmployeeList.ShowDownButton = False
        Me.gbEmployeeList.ShowHeader = True
        Me.gbEmployeeList.Size = New System.Drawing.Size(241, 428)
        Me.gbEmployeeList.TabIndex = 2
        Me.gbEmployeeList.Temp = 0
        Me.gbEmployeeList.Text = "Employee List"
        Me.gbEmployeeList.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objlblEmpCount
        '
        Me.objlblEmpCount.BackColor = System.Drawing.Color.Transparent
        Me.objlblEmpCount.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objlblEmpCount.Location = New System.Drawing.Point(122, 5)
        Me.objlblEmpCount.Name = "objlblEmpCount"
        Me.objlblEmpCount.Size = New System.Drawing.Size(95, 16)
        Me.objlblEmpCount.TabIndex = 160
        Me.objlblEmpCount.Text = "( 0 )"
        Me.objlblEmpCount.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'pnlEmployeeList
        '
        Me.pnlEmployeeList.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.pnlEmployeeList.Controls.Add(Me.objchkSelectAll)
        Me.pnlEmployeeList.Controls.Add(Me.dgEmployee)
        Me.pnlEmployeeList.Controls.Add(Me.txtSearchEmp)
        Me.pnlEmployeeList.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.pnlEmployeeList.Location = New System.Drawing.Point(1, 26)
        Me.pnlEmployeeList.Name = "pnlEmployeeList"
        Me.pnlEmployeeList.Size = New System.Drawing.Size(238, 399)
        Me.pnlEmployeeList.TabIndex = 1
        '
        'objchkSelectAll
        '
        Me.objchkSelectAll.AutoSize = True
        Me.objchkSelectAll.Location = New System.Drawing.Point(8, 33)
        Me.objchkSelectAll.Name = "objchkSelectAll"
        Me.objchkSelectAll.Size = New System.Drawing.Size(15, 14)
        Me.objchkSelectAll.TabIndex = 18
        Me.objchkSelectAll.UseVisualStyleBackColor = True
        '
        'dgEmployee
        '
        Me.dgEmployee.AllowUserToAddRows = False
        Me.dgEmployee.AllowUserToDeleteRows = False
        Me.dgEmployee.AllowUserToResizeRows = False
        Me.dgEmployee.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.dgEmployee.BackgroundColor = System.Drawing.Color.WhiteSmoke
        Me.dgEmployee.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.dgEmployee.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.RaisedHorizontal
        Me.dgEmployee.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing
        Me.dgEmployee.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.objdgcolhCheck, Me.objdgcolhEmployeeunkid, Me.dgColhEmpCode, Me.dgColhEmployee, Me.objdgcolhIsPaymentDone})
        Me.dgEmployee.Location = New System.Drawing.Point(1, 28)
        Me.dgEmployee.Name = "dgEmployee"
        Me.dgEmployee.RowHeadersVisible = False
        Me.dgEmployee.RowHeadersWidth = 5
        Me.dgEmployee.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing
        Me.dgEmployee.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgEmployee.Size = New System.Drawing.Size(235, 371)
        Me.dgEmployee.TabIndex = 286
        '
        'objdgcolhCheck
        '
        Me.objdgcolhCheck.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None
        Me.objdgcolhCheck.HeaderText = ""
        Me.objdgcolhCheck.Name = "objdgcolhCheck"
        Me.objdgcolhCheck.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.objdgcolhCheck.Width = 25
        '
        'objdgcolhEmployeeunkid
        '
        Me.objdgcolhEmployeeunkid.HeaderText = "employeeunkid"
        Me.objdgcolhEmployeeunkid.Name = "objdgcolhEmployeeunkid"
        Me.objdgcolhEmployeeunkid.ReadOnly = True
        Me.objdgcolhEmployeeunkid.Visible = False
        '
        'dgColhEmpCode
        '
        Me.dgColhEmpCode.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None
        Me.dgColhEmpCode.HeaderText = "Emp. Code"
        Me.dgColhEmpCode.Name = "dgColhEmpCode"
        Me.dgColhEmpCode.ReadOnly = True
        Me.dgColhEmpCode.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgColhEmpCode.Width = 70
        '
        'dgColhEmployee
        '
        Me.dgColhEmployee.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.dgColhEmployee.HeaderText = "Employee Name"
        Me.dgColhEmployee.Name = "dgColhEmployee"
        Me.dgColhEmployee.ReadOnly = True
        '
        'objdgcolhIsPaymentDone
        '
        Me.objdgcolhIsPaymentDone.HeaderText = "IsPaymentDone"
        Me.objdgcolhIsPaymentDone.Name = "objdgcolhIsPaymentDone"
        Me.objdgcolhIsPaymentDone.ReadOnly = True
        Me.objdgcolhIsPaymentDone.Visible = False
        '
        'txtSearchEmp
        '
        Me.txtSearchEmp.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtSearchEmp.BackColor = System.Drawing.SystemColors.Window
        Me.txtSearchEmp.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtSearchEmp.Location = New System.Drawing.Point(1, 1)
        Me.txtSearchEmp.Name = "txtSearchEmp"
        Me.txtSearchEmp.Size = New System.Drawing.Size(234, 21)
        Me.txtSearchEmp.TabIndex = 12
        '
        'objbgwProcessPayroll
        '
        Me.objbgwProcessPayroll.WorkerReportsProgress = True
        Me.objbgwProcessPayroll.WorkerSupportsCancellation = True
        '
        'DataGridViewTextBoxColumn1
        '
        Me.DataGridViewTextBoxColumn1.HeaderText = "employeeunkid"
        Me.DataGridViewTextBoxColumn1.Name = "DataGridViewTextBoxColumn1"
        Me.DataGridViewTextBoxColumn1.ReadOnly = True
        Me.DataGridViewTextBoxColumn1.Visible = False
        '
        'DataGridViewTextBoxColumn2
        '
        Me.DataGridViewTextBoxColumn2.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None
        Me.DataGridViewTextBoxColumn2.HeaderText = "Emp. Code"
        Me.DataGridViewTextBoxColumn2.Name = "DataGridViewTextBoxColumn2"
        Me.DataGridViewTextBoxColumn2.ReadOnly = True
        Me.DataGridViewTextBoxColumn2.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.DataGridViewTextBoxColumn2.Width = 70
        '
        'DataGridViewTextBoxColumn3
        '
        Me.DataGridViewTextBoxColumn3.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.DataGridViewTextBoxColumn3.HeaderText = "Employee Name"
        Me.DataGridViewTextBoxColumn3.Name = "DataGridViewTextBoxColumn3"
        Me.DataGridViewTextBoxColumn3.ReadOnly = True
        '
        'DataGridViewTextBoxColumn4
        '
        Me.DataGridViewTextBoxColumn4.HeaderText = "IsPaymentDone"
        Me.DataGridViewTextBoxColumn4.Name = "DataGridViewTextBoxColumn4"
        Me.DataGridViewTextBoxColumn4.ReadOnly = True
        Me.DataGridViewTextBoxColumn4.Visible = False
        '
        'frmProcessPayroll
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1018, 555)
        Me.Controls.Add(Me.pnlMainInfo)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmProcessPayroll"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Process Payroll"
        Me.pnlMainInfo.ResumeLayout(False)
        Me.gbFilterCriteria.ResumeLayout(False)
        Me.gbProcessPayRollInfo.ResumeLayout(False)
        Me.gbProcessPayRollInfo.PerformLayout()
        Me.Panel1.ResumeLayout(False)
        CType(Me.objbtnReset, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.objbtnSearch, System.ComponentModel.ISupportInitialize).EndInit()
        Me.objFooter.ResumeLayout(False)
        Me.gbEmployeeList.ResumeLayout(False)
        Me.pnlEmployeeList.ResumeLayout(False)
        Me.pnlEmployeeList.PerformLayout()
        CType(Me.dgEmployee, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents pnlMainInfo As System.Windows.Forms.Panel
    Friend WithEvents eZeeHeader As eZee.Common.eZeeHeader
    Friend WithEvents gbProcessPayRollInfo As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents lvED As eZee.Common.eZeeListView
    Friend WithEvents lnEmpPaySlipInfo As eZee.Common.eZeeLine
    Friend WithEvents cboPayPeriod As System.Windows.Forms.ComboBox
    Friend WithEvents cboPayYear As System.Windows.Forms.ComboBox
    Friend WithEvents lblPayPeriod As System.Windows.Forms.Label
    Friend WithEvents lblPayYear As System.Windows.Forms.Label
    Friend WithEvents btnEdit As eZee.Common.eZeeLightButton
    Friend WithEvents btnDelete As eZee.Common.eZeeLightButton
    Friend WithEvents gbFilterCriteria As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents objbtnSearchEmployee As eZee.Common.eZeeGradientButton
    Friend WithEvents cboEmployee As System.Windows.Forms.ComboBox
    Friend WithEvents lblEmployee As System.Windows.Forms.Label
    Friend WithEvents objbtnReset As eZee.Common.eZeeSearchResetButton
    Friend WithEvents objbtnSearch As eZee.Common.eZeeSearchResetButton
    Friend WithEvents objFooter As eZee.Common.eZeeFooter
    Friend WithEvents btnProcess As eZee.Common.eZeeLightButton
    Friend WithEvents btnClose As eZee.Common.eZeeLightButton
    Friend WithEvents gbEmployeeList As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents pnlEmployeeList As System.Windows.Forms.Panel
    Friend WithEvents objchkSelectAll As System.Windows.Forms.CheckBox
    Friend WithEvents objcolhEmpUnkID As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhTranHead As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhTranHeadType As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhCalcType As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhAmount As System.Windows.Forms.ColumnHeader
    Friend WithEvents btnNew As eZee.Common.eZeeLightButton
    Friend WithEvents objcolhedunkid As System.Windows.Forms.ColumnHeader
    Friend WithEvents btnExempt As eZee.Common.eZeeLightButton
    Friend WithEvents colhExempted As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhCostCenter As System.Windows.Forms.ColumnHeader
    Friend WithEvents btnCostCenter As eZee.Common.eZeeLightButton
    Friend WithEvents dtpAsOnDate As System.Windows.Forms.DateTimePicker
    Friend WithEvents lblAsOnDate As System.Windows.Forms.Label
    Friend WithEvents colhPeriod As System.Windows.Forms.ColumnHeader
    Friend WithEvents cboModes As System.Windows.Forms.ComboBox
    Friend WithEvents lblModes As System.Windows.Forms.Label
    Friend WithEvents objlblEmpCount As System.Windows.Forms.Label
    Friend WithEvents btnVoidPayroll As eZee.Common.eZeeLightButton
    Friend WithEvents chkExcludeWithPayment As System.Windows.Forms.CheckBox
    Private WithEvents lblSearchEmp As System.Windows.Forms.Label
    Private WithEvents txtSearchEmp As System.Windows.Forms.TextBox
    Friend WithEvents chkUnProcessedEmpOnly As System.Windows.Forms.CheckBox
    Friend WithEvents objbgwProcessPayroll As System.ComponentModel.BackgroundWorker
    Friend WithEvents objlblProgress As System.Windows.Forms.Label
    Friend WithEvents lblTnAEndDate As System.Windows.Forms.Label
    Friend WithEvents objlblTnAEndDate As System.Windows.Forms.Label
    Friend WithEvents lnkAllocation As System.Windows.Forms.LinkLabel
    Friend WithEvents dgEmployee As System.Windows.Forms.DataGridView
    Friend WithEvents objdgcolhCheck As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents objdgcolhEmployeeunkid As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgColhEmpCode As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgColhEmployee As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhIsPaymentDone As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents btnStop As eZee.Common.eZeeLightButton
    Friend WithEvents DataGridViewTextBoxColumn1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn2 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn3 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn4 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents bdgNotProcessed As Aruti.Data.Badge
    Friend WithEvents bdgProcessed As Aruti.Data.Badge
    Friend WithEvents bdgTotalEmployee As Aruti.Data.Badge
    Friend WithEvents bdgTotalAuthorized As Aruti.Data.Badge
    Friend WithEvents bdgTotalPaid As Aruti.Data.Badge
    Friend WithEvents lblPeriodName As System.Windows.Forms.Label
    Friend WithEvents bdgUnpaid As Aruti.Data.Badge
    Friend WithEvents bdgTotalOverDeduction As Aruti.Data.Badge
End Class
