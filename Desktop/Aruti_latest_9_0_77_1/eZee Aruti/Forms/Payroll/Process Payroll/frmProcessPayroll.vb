﻿Option Strict On

Imports eZeeCommonLib
Imports Aruti.Data
Imports Aruti.Data.Language

Public Class frmProcessPayroll

#Region " Private Variables "
    Private ReadOnly mstrModuleName As String = "frmProcessPayroll"

    Private objTnALeave As clsTnALeaveTran
    Private objPayroll As clsPayrollProcessTran

    Private mdtPayPeriodStartDate As DateTime
    Private mdtPayPeriodEndDate As DateTime

    'Sohail (22 Aug 2012) -- Start
    'TRA - ENHANCEMENT
    Private mintCheckedEmployee As Integer = 0
    Private mblnProcessFailed As Boolean = False
    'Sohail (22 Aug 2012) -- End

    Private mstrAdvanceFilter As String = "" 'Sohail (02 Jul 2014) 
    'Sohail (26 Aug 2016) -- Start
    'Enhancement - 57.4 & 63.1 - Process Payroll Performance enhancement for TRA.
    Private dtStart As Date
    Private dvEmployee As DataView
    Private mintTotalEmployee As Integer = 0
    Private mintCount As Integer = 0
    Private mstrSearchEmpText As String = ""
    Private mblnStopPayrollProcess As Boolean = False
    'Sohail (26 Aug 2016) -- End
    'Sohail (26 Oct 2016) -- Start
    'Enhancement - Searchable Dropdown
    Private mstrSearchText As String = ""
    'Sohail (26 Oct 2016) -- End
#End Region

#Region " Private Methods "

    Private Sub SetColor()
        Try
            cboPayYear.BackColor = GUI.ColorComp
            cboPayPeriod.BackColor = GUI.ColorComp

            cboEmployee.BackColor = GUI.ColorOptional
            'Sohail (02 Jul 2014) -- Start
            'Enhancement - Pay Type and Pay Point allocation on Advance Filter.
            'cboDepartment.BackColor = GUI.ColorOptional
            'cboGrade.BackColor = GUI.ColorOptional
            'cboSections.BackColor = GUI.ColorOptional
            'Sohail (02 Jul 2014) -- End
            'cboAccess.BackColor = GUI.ColorOptional
            'Sohail (02 Jul 2014) -- Start
            'Enhancement - Pay Type and Pay Point allocation on Advance Filter.
            'cboClass.BackColor = GUI.ColorOptional
            'cboCostCenter.BackColor = GUI.ColorOptional
            'Sohail (02 Jul 2014) -- End
            'cboService.BackColor = GUI.ColorOptional
            'Sohail (02 Jul 2014) -- Start
            'Enhancement - Pay Type and Pay Point allocation on Advance Filter.
            'cboJob.BackColor = GUI.ColorOptional
            'cboPayPoint.BackColor = GUI.ColorOptional
            'cboUnit.BackColor = GUI.ColorOptional
            'Sohail (02 Jul 2014) -- End
            cboModes.BackColor = GUI.ColorComp 'Sohail (22 May 2012)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "", mstrModuleName)
        End Try
    End Sub

    Private Sub FillCombo()
        Dim objMaster As New clsMasterData
        'Sohail (02 Jul 2014) -- Start
        'Enhancement - Pay Type and Pay Point allocation on Advance Filter.
        'Dim objDept As New clsDepartment
        'Dim objGrade As New clsGrade
        'Dim objSection As New clsSections
        'Dim objAccess As New clsAccess
        'Dim objClass As New clsClass
        'Dim objCostCenter As New clscostcenter_master
        'Dim objService As New clsServices
        'Dim objJob As New clsJobs
        'Dim objPayPoint As New clspaypoint_master
        'Dim objUnit As New clsUnits
        'Sohail (02 Jul 2014) -- End
        Dim dsCombo As DataSet
        'Dim objEmployee As New clsEmployee_Master 'Sohail (06 Jan 2012)

        Try

            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'dsCombo = objMaster.getComboListPAYYEAR("PayYear", True)
            dsCombo = objMaster.getComboListPAYYEAR(FinancialYear._Object._YearUnkid, FinancialYear._Object._FinancialYear_Name, Company._Object._Companyunkid, "PayYear", True)
            'S.SANDEEP [04 JUN 2015] -- END

            With cboPayYear
                .BeginUpdate() 'Sohail (09 Oct 2010)
                .ValueMember = "Id"
                .DisplayMember = "Name"
                RemoveHandler cboPayYear.SelectedIndexChanged, AddressOf cboPayYear_SelectedIndexChanged
                .DataSource = dsCombo.Tables("PayYear")
                AddHandler cboPayYear.SelectedIndexChanged, AddressOf cboPayYear_SelectedIndexChanged
                'Sohail (04 Mar 2011) -- Start
                'If .Items.Count > 0 Then .SelectedValue = 0
                If .Items.Count > 1 Then
                    .SelectedValue = FinancialYear._Object._YearUnkid
                Else
                    .SelectedValue = 0
                End If
                'Sohail (04 Mar 2011) -- End
                .EndUpdate() 'Sohail (09 Oct 2010)
            End With

            'Sohail (26 Aug 2016) -- Start
            'Enhancement - 57.4 & 63.1 - Process Payroll Performance enhancement for TRA.
            'dsCombo = objMaster.getComboListForHeadType("HeadType")
            'With objTranHeadType
            '    .BeginUpdate() 'Sohail (09 Oct 2010)
            '    .ValueMember = "Id"
            '    .DisplayMember = "Name"
            '    .DataSource = dsCombo.Tables("HeadType")
            '    If .Items.Count > 0 Then .SelectedIndex = 0
            '    .EndUpdate() 'Sohail (09 Oct 2010)
            'End With
            'Sohail (26 Aug 2016) -- End

            'Sohail (06 Jan 2012) -- Start
            'TRA - ENHANCEMENT
            'dsCombo = objEmployee.GetEmployeeList("EmployeeList", True)
            'With cboEmployee
            '    .BeginUpdate() 'Sohail (09 Oct 2010)
            '    .ValueMember = "employeeunkid"
            '    .DisplayMember = "employeename"
            '    .DataSource = dsCombo.Tables("EmployeeList")
            '    If .Items.Count > 0 Then .SelectedIndex = 0
            '    .EndUpdate() 'Sohail (09 Oct 2010)
            'End With
            'Sohail (06 Jan 2012) -- End

            'Sohail (02 Jul 2014) -- Start
            'Enhancement - Pay Type and Pay Point allocation on Advance Filter.
            'dsCombo = objDept.getComboList("DepartmentList", True)
            'With cboDepartment
            '    .BeginUpdate() 'Sohail (09 Oct 2010)
            '    .ValueMember = "departmentunkid"
            '    .DisplayMember = "name"
            '    .DataSource = dsCombo.Tables("DepartmentList")
            '    If .Items.Count > 0 Then .SelectedIndex = 0
            '    .EndUpdate() 'Sohail (09 Oct 2010)
            'End With

            'dsCombo = objGrade.getComboList("GradeList", True)
            'With cboGrade
            '    .BeginUpdate() 'Sohail (09 Oct 2010)
            '    .ValueMember = "gradeunkid"
            '    .DisplayMember = "name"
            '    .DataSource = dsCombo.Tables("GradeList")
            '    If .Items.Count > 0 Then .SelectedIndex = 0
            '    .EndUpdate() 'Sohail (09 Oct 2010)
            'End With

            'dsCombo = objSection.getComboList("SectionList", True)
            'With cboSections
            '    .BeginUpdate() 'Sohail (09 Oct 2010)
            '    .ValueMember = "sectionunkid"
            '    .DisplayMember = "name"
            '    .DataSource = dsCombo.Tables("SectionList")
            '    If .Items.Count > 0 Then .SelectedIndex = 0
            '    .EndUpdate() 'Sohail (09 Oct 2010)
            'End With
            'Sohail (02 Jul 2014) -- End

            'dsCombo = objAccess.getComboList("AccessList", True)
            'With cboAccess
            '    .ValueMember = "accessunkid"
            '    .DisplayMember = "name"
            '    .DataSource = dsCombo.Tables("AccessList")
            '    If .Items.Count > 0 Then .SelectedIndex = 0
            'End With

            'Sohail (02 Jul 2014) -- Start
            'Enhancement - Pay Type and Pay Point allocation on Advance Filter.
            'dsCombo = objClass.getComboList("ClassList", True)
            'With cboClass
            '    .BeginUpdate() 'Sohail (09 Oct 2010)
            '    .ValueMember = "classesunkid"
            '    .DisplayMember = "name"
            '    .DataSource = dsCombo.Tables("ClassList")
            '    If .Items.Count > 0 Then .SelectedIndex = 0
            '    .EndUpdate() 'Sohail (09 Oct 2010)
            'End With

            'dsCombo = objCostCenter.getComboList("CostCenterList", True)
            'With cboCostCenter
            '    .BeginUpdate() 'Sohail (09 Oct 2010)
            '    .ValueMember = "CostCenterunkid"
            '    .DisplayMember = "CostCentername"
            '    .DataSource = dsCombo.Tables("CostCenterList")
            '    If .Items.Count > 0 Then .SelectedIndex = 0
            '    .EndUpdate() 'Sohail (09 Oct 2010)
            'End With
            'Sohail (02 Jul 2014) -- End

            'dsCombo = objService.getComboList("ServiceList", True)
            'With cboService
            '    .ValueMember = "Serviceunkid"
            '    .DisplayMember = "name"
            '    .DataSource = dsCombo.Tables("ServiceList")
            '    If .Items.Count > 0 Then .SelectedIndex = 0
            'End With

            'Sohail (02 Jul 2014) -- Start
            'Enhancement - Pay Type and Pay Point allocation on Advance Filter.
            'dsCombo = objJob.getComboList("JobList", True)
            'With cboJob
            '    .BeginUpdate() 'Sohail (09 Oct 2010)
            '    .ValueMember = "jobunkid"
            '    .DisplayMember = "name"
            '    .DataSource = dsCombo.Tables("JobList")
            '    If .Items.Count > 0 Then .SelectedIndex = 0
            '    .EndUpdate() 'Sohail (09 Oct 2010)
            'End With

            'dsCombo = objPayPoint.getListForCombo("PayPointList", True)
            'With cboPayPoint
            '    .BeginUpdate() 'Sohail (09 Oct 2010)
            '    .ValueMember = "paypointunkid"
            '    .DisplayMember = "name"
            '    .DataSource = dsCombo.Tables("PayPointList")
            '    If .Items.Count > 0 Then .SelectedIndex = 0
            '    .EndUpdate() 'Sohail (09 Oct 2010)
            'End With

            'dsCombo = objUnit.getComboList("UnitList", True)
            'With cboUnit
            '    .BeginUpdate() 'Sohail (09 Oct 2010)
            '    .ValueMember = "unitunkid"
            '    .DisplayMember = "name"
            '    .DataSource = dsCombo.Tables("UnitList")
            '    If .Items.Count > 0 Then .SelectedIndex = 0
            '    .EndUpdate() 'Sohail (09 Oct 2010)
            'End With
            'Sohail (02 Jul 2014) -- End

            'Sohail (22 May 2012) -- Start
            'TRA - ENHANCEMENT
            With cboModes
                .BeginUpdate()
                .Items.Clear()
                'Sohail (06 Aug 2019) -- Start
                'NMB Enhancement # - 76.1 - apply language on process payroll modes dropdown.
                '.Items.Add("Show Active Employees")
                '.Items.Add("Show Terminated Employees with Payroll Excluded")
                '.Items.Add("Show Terminated Employees without Payroll Excluded")
                .Items.Add(Language.getMessage(mstrModuleName, 36, "Show Active Employees"))
                .Items.Add(Language.getMessage(mstrModuleName, 37, "Show Terminated Employees with Payroll Excluded"))
                .Items.Add(Language.getMessage(mstrModuleName, 38, "Show Terminated Employees without Payroll Excluded"))
                'Sohail (06 Aug 2019) -- End
                'Sohail (03 May 2018) -- Start
                'Enhancement : Removing global objects and passing objDataOperation in process payroll in 72.1.
                '.SelectedIndex = 0
                RemoveHandler cboModes.SelectedIndexChanged, AddressOf cboModes_SelectedIndexChanged
                .SelectedIndex = 0
                AddHandler cboModes.SelectedIndexChanged, AddressOf cboModes_SelectedIndexChanged
                'Sohail (03 May 2018) -- End
                .EndUpdate()
            End With
            'Sohail (22 May 2012) -- End

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        Finally
            objMaster = Nothing
            'objEmployee = Nothing 'Sohail (06 Jan 2012)
            'Sohail (02 Jul 2014) -- Start
            'Enhancement - Pay Type and Pay Point allocation on Advance Filter.
            'objDept = Nothing
            'objGrade = Nothing
            'objSection = Nothing
            'objAccess = Nothing
            'objClass = Nothing
            'objCostCenter = Nothing
            'objService = Nothing
            'objJob = Nothing
            'objPayPoint = Nothing
            'objUnit = Nothing
            'Sohail (02 Jul 2014) -- End
        End Try
    End Sub

    Private Sub FillList(Optional ByVal blnOnlyProcessed As Boolean = False)
        'Sohail (03 May 2018) - [blnOnlyProcessed]
        Dim objEmployee As New clsEmployee_Master
        Dim objPayment As New clsPayment_tran
        Dim dsEmployee As New DataSet
        Dim dtTable As DataTable
        Dim intCount As Integer = 0
        Dim strEmployeeList As String = ""
        'Sohail (22 May 2012) -- Start
        'TRA - ENHANCEMENT
        Dim strFilter As String = ""
        'Sohail (26 Aug 2016) -- Start
        'Enhancement - 57.4 & 63.1 - Process Payroll Performance enhancement for TRA.
        'objlblEmpCount.Text = "( 0 )"
        objlblEmpCount.Text = "( 0 / 0 )"
        mintCount = 0
        'Sohail (26 Aug 2016) -- End
        'Sohail (22 May 2012) -- End
        'Sohail (12 Jul 2012) -- Start
        'TRA - ENHANCEMENT
        Dim strUnProcessedIDs As String = ""
        Dim strUnProcessedFilter As String = ""
        'Sohail (12 Jul 2012) -- End
        'Sohail (14 Feb 2019) -- Start
        'NMB Enhancement - 76.1 - Performance enhancement on process payroll employee list.
        Dim strTempTableSelectQry As String = ""
        Dim strTempTableJoinQry As String = ""
        Dim strTempTableDropQry As String = ""
        'Sohail (14 Feb 2019) -- End

        Try
            'Sohail (03 Nov 2010) -- Start
            'dsEmployee = objEmployee.GetEmployeeList("Employee", False, True _
            '                                          , CInt(cboEmployee.SelectedValue) _
            '                                          , CInt(cboDepartment.SelectedValue) _
            '                                          , CInt(cboSections.SelectedValue) _
            '                                          , CInt(cboUnit.SelectedValue) _
            '                                          , CInt(cboGrade.SelectedValue) _
            '                                          , 0 _
            '                                          , CInt(cboClass.SelectedValue) _
            '                                          , CInt(cboCostCenter.SelectedValue) _
            '                                          , 0 _
            '                                          , CInt(cboJob.SelectedValue) _
            '                                          , CInt(cboPayPoint.SelectedValue) _
            '                                        )
            'Sohail (03 Nov 2010) -- End

            'Dim lvItem As ListViewItem 'Sohail (26 Aug 2016)
            Dim lvArray As New List(Of ListViewItem) 'Sohail (03 Nov 2010)
            'Sohail (26 Aug 2016) -- Start
            'Enhancement - 57.4 & 63.1 - Process Payroll Performance enhancement for TRA.
            'lvEmployeeList.Items.Clear() 
            dgEmployee.DataSource = Nothing
            RemoveHandler txtSearchEmp.TextChanged, AddressOf txtSearchEmp_TextChanged
            Call SetDefaultSearchEmpText()
            AddHandler txtSearchEmp.TextChanged, AddressOf txtSearchEmp_TextChanged
            'Sohail (26 Aug 2016) -- End
            'Sohail (03 May 2018) -- Start
            'Enhancement : Removing global objects and passing objDataOperation in process payroll in 72.1.
            If txtSearchEmp.Focused = True Then
                txtSearchEmp.Text = ""
                txtSearchEmp.Font = New Font(Me.Font.FontFamily, Me.Font.Size, FontStyle.Regular)
                txtSearchEmp.ForeColor = Color.Black
            End If
            'Sohail (03 May 2018) -- End
            lvED.Items.Clear()
            If CInt(cboPayPeriod.SelectedValue) <= 0 Then Exit Try 'Sohail (18 Dec 2010)
            Cursor.Current = Cursors.WaitCursor 'Sohail (04 Mar 2011)
            'lvEmployeeList.BeginUpdate() 'Sohail (09 Oct 2010) 'Sohail (26 Aug 2016)

            Dim objDicPaidEmp As New Dictionary(Of Integer, Integer)
            If CInt(cboPayPeriod.SelectedValue) > 0 Then
                'Sohail (21 Aug 2015) -- Start
                'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                'Dim ds As DataSet = objPayment.GetListByPeriod("Payment", clsPayment_tran.enPaymentRefId.PAYSLIP, CInt(cboPayPeriod.SelectedValue), clsPayment_tran.enPayTypeId.PAYMENT)
                'Sohail (14 Feb 2019) -- Start
                'NMB Enhancement - 76.1 - Performance enhancement on process payroll employee list.
                'Dim ds As DataSet = objPayment.GetListByPeriod(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, mdtPayPeriodStartDate, mdtPayPeriodEndDate, ConfigParameter._Object._UserAccessModeSetting, True, ConfigParameter._Object._IsIncludeInactiveEmp, "Payment", clsPayment_tran.enPaymentRefId.PAYSLIP, CInt(cboPayPeriod.SelectedValue), clsPayment_tran.enPayTypeId.PAYMENT, , True)
                'Sohail (14 Feb 2019) -- End
                'Sohail (21 Aug 2015) -- End
                'Sohail (14 Feb 2019) -- Start
                'NMB Enhancement - 76.1 - Performance enhancement on process payroll employee list.
                'For Each dsRow As DataRow In ds.Tables("Payment").Rows
                '    With dsRow
                '        'Sohail (04 Mar 2011) -- Start
                '        If chkExcludeWithPayment.Checked = True Then
                '            If strEmployeeList.Length <= 0 Then
                '                strEmployeeList = .Item("employeeunkid").ToString
                '            Else
                '                strEmployeeList &= "," & .Item("employeeunkid").ToString
                '            End If
                '        End If
                '        objDicPaidEmp.Add(CInt(.Item("employeeunkid").ToString), CInt(.Item("referencetranunkid").ToString))
                '        'Sohail (04 Mar 2011) -- End
                '    End With
                'Next
                        If chkExcludeWithPayment.Checked = True Then
                    strTempTableSelectQry &= "SELECT     prpayment_tran.employeeunkid " & _
                                             ", prpayment_tran.referencetranunkid " & _
                                             "INTO #tblPayment " & _
                                             "FROM prpayment_tran " & _
                                             "WHERE isvoid = 0 " & _
                                                 "AND prpayment_tran.periodunkid = " & CInt(cboPayPeriod.SelectedValue) & " " & _
                                                "AND prpayment_tran.referenceid = " & clsPayment_tran.enPaymentRefId.PAYSLIP & " " & _
                                                "AND prpayment_tran.paytypeid = " & clsPayment_tran.enPayTypeId.PAYMENT & " "
                    'Sohail (04 Apr 2019) - [AND prpayment_tran.paytypeid =] = [AND prpayment_tran.referenceid =], [AND prpayment_tran.referenceid =] = [AND prpayment_tran.paytypeid =]

                    strTempTableJoinQry &= "LEFT JOIN #tblPayment ON #tblPayment.employeeunkid = hremployee_master.employeeunkid "

                    strTempTableDropQry &= "AND #tblPayment.employeeunkid IS NULL; DROP TABLE #tblPayment "
                            Else
                    Dim ds As DataSet = objPayment.GetListByPeriod(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, mdtPayPeriodStartDate, mdtPayPeriodEndDate, ConfigParameter._Object._UserAccessModeSetting, True, ConfigParameter._Object._IsIncludeInactiveEmp, "Payment", clsPayment_tran.enPaymentRefId.PAYSLIP, CInt(cboPayPeriod.SelectedValue), clsPayment_tran.enPayTypeId.PAYMENT, , True)
                    objDicPaidEmp = (From p In ds.Tables("Payment").AsEnumerable Select New With {.EmpId = CInt(p.Item("employeeunkid")), .RefTranId = CInt(p.Item("referencetranunkid"))}).ToDictionary(Function(x) x.EmpId, Function(y) y.RefTranId)
                            End If
                'Sohail (14 Feb 2019) -- End
            End If

            'Sohail (12 Jul 2012) -- Start
            'TRA - ENHANCEMENT
            'Sohail (03 May 2018) -- Start
            'Enhancement : Removing global objects and passing objDataOperation in process payroll in 72.1.
            'If chkUnProcessedEmpOnly.Checked = True Then
            If blnOnlyProcessed = False AndAlso chkUnProcessedEmpOnly.Checked = True Then
                'Sohail (03 May 2018) -- End
                'Sohail (21 Aug 2015) -- Start
                'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                'strUnProcessedIDs = objPayroll.Get_UnProcessed_EmployeeIDs(CInt(cboPayPeriod.SelectedValue), True)
                'Sohail (21 Aug 2015) -- Start
                'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                'strUnProcessedIDs = objPayroll.Get_UnProcessed_EmployeeIDs(CInt(cboPayPeriod.SelectedValue), True)
                strUnProcessedIDs = objPayroll.Get_UnProcessed_EmployeeIDs(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, mdtPayPeriodStartDate, mdtPayPeriodEndDate, ConfigParameter._Object._UserAccessModeSetting, True, ConfigParameter._Object._IsIncludeInactiveEmp, CInt(cboPayPeriod.SelectedValue), True, True, "")
                'Sohail (21 Aug 2015) -- End
                'Sohail (21 Aug 2015) -- End
            End If
            'Sohail (03 May 2018) -- Start
            'Enhancement : Removing global objects and passing objDataOperation in process payroll in 72.1.
            If blnOnlyProcessed = True Then
                strUnProcessedIDs = objPayroll.Get_Processed_EmployeeIDs(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, mdtPayPeriodStartDate, mdtPayPeriodEndDate, ConfigParameter._Object._UserAccessModeSetting, True, ConfigParameter._Object._IsIncludeInactiveEmp, CInt(cboPayPeriod.SelectedValue), True, True, "")
            End If
            'Sohail (03 May 2018) -- End
            If strUnProcessedIDs.Trim <> "" Then
                'Sohail (21 Aug 2015) -- Start
                'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                'strUnProcessedFilter = " employeeunkid IN ( " & strUnProcessedIDs & " ) "
                strUnProcessedFilter = " hremployee_master.employeeunkid IN ( " & strUnProcessedIDs & " ) "
                'Sohail (21 Aug 2015) -- End
                'Sohail (16 Jul 2012) -- Start
                'TRA - ENHANCEMENT
            Else
                If chkUnProcessedEmpOnly.Checked = True Then
                    'Sohail (21 Aug 2015) -- Start
                    'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                    'strUnProcessedFilter = " employeeunkid IN ( -999 ) "
                    strUnProcessedFilter = " hremployee_master.employeeunkid IN ( -999 ) "
                    'Sohail (21 Aug 2015) -- End
                End If
                'Sohail (16 Jul 2012) -- End
            End If
            'Sohail (12 Jul 2012) -- End

            'Sohail (03 Nov 2010) -- Start
            'Sohail (22 May 2012) -- Start
            'TRA - ENHANCEMENT
            Select Case cboModes.SelectedIndex
                Case 0 '*** Show Active Employees
                    'Sohail (22 May 2012) -- End

                    'Sohail (02 Jul 2014) -- Start
                    'Enhancement - Pay Type and Pay Point allocation on Advance Filter.
                    'dsEmployee = objEmployee.GetEmployeeList("Employee", False, True _
                    '                                              , CInt(cboEmployee.SelectedValue) _
                    '                                              , CInt(cboDepartment.SelectedValue) _
                    '                                              , CInt(cboSections.SelectedValue) _
                    '                                              , CInt(cboUnit.SelectedValue) _
                    '                                              , CInt(cboGrade.SelectedValue) _
                    '                                              , 0 _
                    '                                              , CInt(cboClass.SelectedValue) _
                    '                                              , CInt(cboCostCenter.SelectedValue) _
                    '                                              , 0 _
                    '                                              , CInt(cboJob.SelectedValue) _
                    '                                              , CInt(cboPayPoint.SelectedValue) _
                    '                                              , mdtPayPeriodStartDate _
                    '                                              , mdtPayPeriodEndDate _
                    '                                                    , , , , True, strUnProcessedFilter) 'Sohail (12 Jul 2012) - [strUnProcessedFilter]
                    'Sohail (12 Oct 2021) -- Start
                    'NMB Issue :  : Performance issue on process payroll.
                    'If mstrAdvanceFilter <> "" Then
                    '    If strUnProcessedFilter.Trim <> "" Then
                    '        strUnProcessedFilter &= " AND " & mstrAdvanceFilter
                    '    Else
                    '        strUnProcessedFilter = mstrAdvanceFilter
                    '    End If
                    'End If
                    'Sohail (12 Oct 2021) -- End
                    'Anjan [10 June 2015] -- Start
                    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS

                    ''dsEmployee = objEmployee.GetEmployeeList("Employee", False, True _
                    '                                              , CInt(cboEmployee.SelectedValue) _
                    '                                              , 0 _
                    '                                              , 0 _
                    '                                                      , 0 _
                    '                                                      , 0 _
                    '                                                      , 0 _
                    '                                                      , 0 _
                    '                                                      , 0 _
                    '                                                      , 0 _
                    '                                                      , 0 _
                    '                                                      , 0 _
                    '                                              , mdtPayPeriodStartDate _
                    '                                              , mdtPayPeriodEndDate _
                    '                                                    , , , , True, strUnProcessedFilter) 'Sohail (12 Jul 2012) - [strUnProcessedFilter]

                    'Sohail (04 Apr 2019) -- Start
                    'NMB Enhancement - 76.1 - Performance enhancement on process payroll employee list..
                    'dsEmployee = objEmployee.GetEmployeeList(FinancialYear._Object._DatabaseName, _
                    '                               User._Object._Userunkid, _
                    '                               FinancialYear._Object._YearUnkid, _
                    '                               Company._Object._Companyunkid, _
                    '                               mdtPayPeriodStartDate, _
                    '                               mdtPayPeriodEndDate, _
                    '                               ConfigParameter._Object._UserAccessModeSetting, _
                    '                               True, False, "Employee", False, CInt(cboEmployee.SelectedValue), , , , , , , , , , , , , True, strUnProcessedFilter)
                    dsEmployee = objEmployee.GetEmployeeList(FinancialYear._Object._DatabaseName, _
                                                   User._Object._Userunkid, _
                                                   FinancialYear._Object._YearUnkid, _
                                                   Company._Object._Companyunkid, _
                                                   mdtPayPeriodStartDate, _
                                                   mdtPayPeriodEndDate, _
                                                   ConfigParameter._Object._UserAccessModeSetting, _
                                                   True, False, "Employee", False, CInt(cboEmployee.SelectedValue), , , , , , , , , , , , , True, mstrAdvanceFilter, , , , strTempTableSelectQry, strTempTableJoinQry, strTempTableDropQry, blnApplyEmpExemptionFilter:=True, blnApplyEmpExemptionFilterForTerminated:=False, strFilter:=strUnProcessedFilter)
                    'Sohail (12 Oct 2021) - [strFilter]
                    'Sohail (31 Mar 2020) - [blnApplyEmpExemptionFilter, blnApplyEmpExemptionFilterForTerminated]
                    'Sohail (04 Apr 2019) -- End
                    'Anjan [10 June 2015] -- End
                    'Sohail (02 Jul 2014) -- End

                    'Sohail (22 May 2012) -- Start
                    'TRA - ENHANCEMENT
                Case 1 '*** Show Terminated Employees with Payroll Excluded

                    'Sohail (18 Apr 2016) -- Start
                    'Issue - 59.1 - Terminated employees were not coming in list even if Exclude from process payroll option ticked on employee master.
                    'strFilter = "isexclude_payroll = 1 " & _
                    '               "AND ( ISNULL(CONVERT(CHAR(8), termination_from_date, 112), '" & eZeeDate.convertDate(mdtPayPeriodEndDate.AddDays(1)) & "') <= @enddate " & _
                    '                     "OR ISNULL(CONVERT(CHAR(8), termination_to_date, 112), '" & eZeeDate.convertDate(mdtPayPeriodEndDate.AddDays(1)) & "') <= @enddate " & _
                    '                     "OR ISNULL(CONVERT(CHAR(8), empl_enddate, 112), '" & eZeeDate.convertDate(mdtPayPeriodEndDate.AddDays(1)) & "') <= @enddate " & _
                    '                   ") "
                    strFilter &= " ( ( ISNULL(TRM.IsExPayroll, 0) = 1 ) AND ( (CASE WHEN CONVERT(CHAR(8), TRM.LEAVING, 112) IS NULL THEN '" & eZeeDate.convertDate(mdtPayPeriodEndDate.AddDays(1)).ToString & "' ELSE CONVERT(CHAR(8), TRM.LEAVING, 112) END) <= '" & eZeeDate.convertDate(mdtPayPeriodEndDate).ToString & "' " & _
                               " OR (CASE WHEN CONVERT(CHAR(8), RET.RETIRE, 112) IS NULL THEN '" & eZeeDate.convertDate(mdtPayPeriodEndDate.AddDays(1)).ToString & "' ELSE CONVERT(CHAR(8), RET.RETIRE, 112) END) <= '" & eZeeDate.convertDate(mdtPayPeriodEndDate).ToString & "' " & _
                               " OR (CASE WHEN CONVERT(CHAR(8), TRM.EOC, 112) IS NULL THEN '" & eZeeDate.convertDate(mdtPayPeriodEndDate.AddDays(1)).ToString & "' ELSE CONVERT(CHAR(8), TRM.EOC, 112) END) <= '" & eZeeDate.convertDate(mdtPayPeriodEndDate).ToString & "' " & _
                               " OR " & DateDiff(DateInterval.Day, mdtPayPeriodStartDate, mdtPayPeriodEndDate.AddDays(1)) & " <= #TableExempt.TotalDays ) ) "
                    'Sohail (31 Mar 2020) - [OR " & DateDiff(DateInterval.Day, mdtPayPeriodStartDate, mdtPayPeriodEndDate.AddDays(1)) & " <= #TableExempt.TotalDays]
                    'Sohail (18 Apr 2016) -- End

                    'Sohail (12 Jul 2012) -- Start
                    'TRA - ENHANCEMENT
                    If strUnProcessedFilter.Trim <> "" Then
                        strFilter &= " AND " & strUnProcessedFilter
                    End If
                    'Sohail (12 Jul 2012) -- End

                    'Sohail (02 Jul 2014) -- Start
                    'Enhancement - Pay Type and Pay Point allocation on Advance Filter.
                    'dsEmployee = objEmployee.GetEmployeeList("Employee", False, True _
                    '                                              , CInt(cboEmployee.SelectedValue) _
                    '                                              , CInt(cboDepartment.SelectedValue) _
                    '                                              , CInt(cboSections.SelectedValue) _
                    '                                              , CInt(cboUnit.SelectedValue) _
                    '                                              , CInt(cboGrade.SelectedValue) _
                    '                                              , 0 _
                    '                                              , CInt(cboClass.SelectedValue) _
                    '                                              , CInt(cboCostCenter.SelectedValue) _
                    '                                              , 0 _
                    '                                              , CInt(cboJob.SelectedValue) _
                    '                                              , CInt(cboPayPoint.SelectedValue) _
                    '                                              , mdtPayPeriodStartDate _
                    '                                              , mdtPayPeriodEndDate _
                    '                                            , , , , False, strFilter)
                    'Sohail (12 Oct 2021) -- Start
                    'NMB Issue :  : Performance issue on process payroll.
                    'If mstrAdvanceFilter <> "" Then
                    '    If strFilter.Trim <> "" Then
                    '        strFilter &= " AND " & mstrAdvanceFilter
                    '    Else
                    '        strFilter = mstrAdvanceFilter
                    '    End If
                    'End If
                    'Sohail (12 Oct 2021) -- End
                    'Anjan [10 June 2015] -- Start
                    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                    'dsEmployee = objEmployee.GetEmployeeList("Employee", False, True _
                    '                                              , CInt(cboEmployee.SelectedValue) _
                    '                                              , 0 _
                    '                                              , 0 _
                    '                                              , 0 _
                    '                                              , 0 _
                    '                                              , 0 _
                    '                                              , 0 _
                    '                                              , 0 _
                    '                                              , 0 _
                    '                                              , 0 _
                    '                                              , 0 _
                    '                                              , mdtPayPeriodStartDate _
                    '                                              , mdtPayPeriodEndDate _
                    '                                            , , , , False, strFilter)
                    'Sohail (02 Jul 2014) -- End

                    'Sohail (18 Apr 2016) -- Start
                    'Enhancement - 59.1 - NET PAY ROUNDING ADJUSTMENT head for the difference of actual Net Pay and Rounded Net Pay for HERON Portico.
                    'dsEmployee = objEmployee.GetEmployeeList(FinancialYear._Object._DatabaseName, _
                    '                               User._Object._Userunkid, _
                    '                               FinancialYear._Object._YearUnkid, _
                    '                               Company._Object._Companyunkid, _
                    '                               mdtPayPeriodStartDate, _
                    '                               mdtPayPeriodEndDate, _
                    '                               ConfigParameter._Object._UserAccessModeSetting, _
                    '                               True, False, "Employee", False, CInt(cboEmployee.SelectedValue), , , , , , , , , , , , , True, strFilter)
                    'Sohail (04 Apr 2019) -- Start
                    'NMB Enhancement - 76.1 - Performance enhancement on process payroll employee list..
                    'dsEmployee = objEmployee.GetEmployeeList(FinancialYear._Object._DatabaseName, _
                    '                               User._Object._Userunkid, _
                    '                               FinancialYear._Object._YearUnkid, _
                    '                               Company._Object._Companyunkid, _
                    '                               mdtPayPeriodStartDate, _
                    '                               mdtPayPeriodEndDate, _
                    '                               ConfigParameter._Object._UserAccessModeSetting, _
                    '                               True, False, "Employee", False, CInt(cboEmployee.SelectedValue), , , , , , , , , , , , , False, strFilter)
                    dsEmployee = objEmployee.GetEmployeeList(FinancialYear._Object._DatabaseName, _
                                                   User._Object._Userunkid, _
                                                   FinancialYear._Object._YearUnkid, _
                                                   Company._Object._Companyunkid, _
                                                   mdtPayPeriodStartDate, _
                                                   mdtPayPeriodEndDate, _
                                                   ConfigParameter._Object._UserAccessModeSetting, _
                                                   True, False, "Employee", False, CInt(cboEmployee.SelectedValue), , , , , , , , , , , , , False, mstrAdvanceFilter, , , , strTempTableSelectQry, strTempTableJoinQry, strTempTableDropQry, True, True, strFilter)
                    'Sohail (12 Oct 2021) - [strFilter]
                    'Sohail (31 Mar 2020) - [blnApplyEmpExemptionFilter, blnApplyEmpExemptionFilterForTerminated]
                    'Sohail (04 Apr 2019) -- End
                    'Sohail (18 Apr 2016) -- End
                    'Anjan [10 June 2015] -- End


                Case 2 '*** Show Terminated Employees without Payroll Excluded

                    'Sohail (18 Apr 2016) -- Start
                    'Issue - 59.1 - Terminated employees were not coming in list even if Exclude from process payroll option not ticked on employee master.
                    'strFilter = "isexclude_payroll = 0 " & _
                    '               "AND ( ISNULL(CONVERT(CHAR(8), termination_from_date, 112), '" & eZeeDate.convertDate(mdtPayPeriodEndDate.AddDays(1)) & "') <= @enddate " & _
                    '                     "OR ISNULL(CONVERT(CHAR(8), termination_to_date, 112), '" & eZeeDate.convertDate(mdtPayPeriodEndDate.AddDays(1)) & "') <= @enddate " & _
                    '                     "OR ISNULL(CONVERT(CHAR(8), empl_enddate, 112), '" & eZeeDate.convertDate(mdtPayPeriodEndDate.AddDays(1)) & "') <= @enddate " & _
                    '                   ") "
                    'Sohail (21 Dec 2016) -- Start
                    'Issue - 64.1 - Terminated employees in current period with include process payroll were not coming in list even if Without Exclude from process payroll option selected on process payroll screen.
                    'Sohail (21 Dec 2016) -- End
                    'strFilter &= " ( ( ISNULL(TRM.IsExPayroll, 0) = 0 ) AND ( (CASE WHEN CONVERT(CHAR(8), TRM.LEAVING, 112) IS NULL THEN '" & eZeeDate.convertDate(mdtPayPeriodEndDate.AddDays(1)).ToString & "' ELSE CONVERT(CHAR(8), TRM.LEAVING, 112) END) <= '" & eZeeDate.convertDate(mdtPayPeriodEndDate).ToString & "' " & _
                    '           " AND (CASE WHEN CONVERT(CHAR(8), RET.RETIRE, 112) IS NULL THEN '" & eZeeDate.convertDate(mdtPayPeriodEndDate.AddDays(1)).ToString & "' ELSE CONVERT(CHAR(8), RET.RETIRE, 112) END) <= '" & eZeeDate.convertDate(mdtPayPeriodEndDate).ToString & "' " & _
                    '           " AND (CASE WHEN CONVERT(CHAR(8), TRM.EOC, 112) IS NULL THEN '" & eZeeDate.convertDate(mdtPayPeriodEndDate.AddDays(1)).ToString & "' ELSE CONVERT(CHAR(8), TRM.EOC, 112) END) <= '" & eZeeDate.convertDate(mdtPayPeriodEndDate).ToString & "' ) ) "
                    strFilter &= " ( ( ISNULL(TRM.IsExPayroll, 0) = 0 ) AND ( (CASE WHEN CONVERT(CHAR(8), TRM.LEAVING, 112) IS NULL THEN '" & eZeeDate.convertDate(mdtPayPeriodEndDate.AddDays(1)).ToString & "' ELSE CONVERT(CHAR(8), TRM.LEAVING, 112) END) <= '" & eZeeDate.convertDate(mdtPayPeriodEndDate).ToString & "' " & _
                               " OR (CASE WHEN CONVERT(CHAR(8), RET.RETIRE, 112) IS NULL THEN '" & eZeeDate.convertDate(mdtPayPeriodEndDate.AddDays(1)).ToString & "' ELSE CONVERT(CHAR(8), RET.RETIRE, 112) END) <= '" & eZeeDate.convertDate(mdtPayPeriodEndDate).ToString & "' " & _
                               " OR (CASE WHEN CONVERT(CHAR(8), TRM.EOC, 112) IS NULL THEN '" & eZeeDate.convertDate(mdtPayPeriodEndDate.AddDays(1)).ToString & "' ELSE CONVERT(CHAR(8), TRM.EOC, 112) END) <= '" & eZeeDate.convertDate(mdtPayPeriodEndDate).ToString & "' " & _
                               " OR " & DateDiff(DateInterval.Day, mdtPayPeriodStartDate, mdtPayPeriodEndDate.AddDays(1)) & " <= #TableExempt.TotalDays ) ) "
                    'Sohail (31 Mar 2020) - [OR " & DateDiff(DateInterval.Day, mdtPayPeriodStartDate, mdtPayPeriodEndDate.AddDays(1)) & " <= #TableExempt.TotalDays]
                    'Sohail (18 Apr 2016) -- End

                    'Sohail (12 Jul 2012) -- Start
                    'TRA - ENHANCEMENT
                    If strUnProcessedFilter.Trim <> "" Then
                        strFilter &= " AND " & strUnProcessedFilter
                    End If
                    'Sohail (12 Jul 2012) -- End

                    'Sohail (02 Jul 2014) -- Start
                    'Enhancement - Pay Type and Pay Point allocation on Advance Filter.
                    'dsEmployee = objEmployee.GetEmployeeList("Employee", False, True _
                    '                                              , CInt(cboEmployee.SelectedValue) _
                    '                                              , CInt(cboDepartment.SelectedValue) _
                    '                                              , CInt(cboSections.SelectedValue) _
                    '                                              , CInt(cboUnit.SelectedValue) _
                    '                                              , CInt(cboGrade.SelectedValue) _
                    '                                              , 0 _
                    '                                              , CInt(cboClass.SelectedValue) _
                    '                                              , CInt(cboCostCenter.SelectedValue) _
                    '                                              , 0 _
                    '                                              , CInt(cboJob.SelectedValue) _
                    '                                              , CInt(cboPayPoint.SelectedValue) _
                    '                                              , mdtPayPeriodStartDate _
                    '                                              , mdtPayPeriodEndDate _
                    '                                            , , , , False, strFilter)
                    'Sohail (12 Oct 2021) -- Start
                    'NMB Issue :  : Performance issue on process payroll.
                    'If mstrAdvanceFilter <> "" Then
                    '    If strFilter.Trim <> "" Then
                    '        strFilter &= " AND " & mstrAdvanceFilter
                    '    Else
                    '        strFilter = mstrAdvanceFilter
                    '    End If
                    'End If
                    'Sohail (12 Oct 2021) -- End
                    'Anjan [10 June 2015] -- Start
                    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                    'dsEmployee = objEmployee.GetEmployeeList("Employee", False, True _
                    '                                              , CInt(cboEmployee.SelectedValue) _
                    '                                              , 0 _
                    '                                              , 0 _
                    '                                              , 0 _
                    '                                              , 0 _
                    '                                              , 0 _
                    '                                              , 0 _
                    '                                              , 0 _
                    '                                              , 0 _
                    '                                              , 0 _
                    '                                              , 0 _
                    '                                              , mdtPayPeriodStartDate _
                    '                                              , mdtPayPeriodEndDate _
                    '                                            , , , , False, strFilter)
                    'Sohail (02 Jul 2014) -- End
                    'Sohail (04 Apr 2019) -- Start
                    'NMB Enhancement - 76.1 - Performance enhancement on process payroll employee list..
                    'dsEmployee = objEmployee.GetEmployeeList(FinancialYear._Object._DatabaseName, _
                    '                               User._Object._Userunkid, _
                    '                               FinancialYear._Object._YearUnkid, _
                    '                               Company._Object._Companyunkid, _
                    '                               mdtPayPeriodStartDate, _
                    '                               mdtPayPeriodEndDate, _
                    '                               ConfigParameter._Object._UserAccessModeSetting, _
                    '                               True, False, "Employee", False, CInt(cboEmployee.SelectedValue), , , , , , , , , , , , , False, strFilter)
                    dsEmployee = objEmployee.GetEmployeeList(FinancialYear._Object._DatabaseName, _
                                                   User._Object._Userunkid, _
                                                   FinancialYear._Object._YearUnkid, _
                                                   Company._Object._Companyunkid, _
                                                   mdtPayPeriodStartDate, _
                                                   mdtPayPeriodEndDate, _
                                                   ConfigParameter._Object._UserAccessModeSetting, _
                                                   True, False, "Employee", False, CInt(cboEmployee.SelectedValue), , , , , , , , , , , , , False, mstrAdvanceFilter, , , , strTempTableSelectQry, strTempTableJoinQry, strTempTableDropQry, True, True, strFilter)
                    'Sohail (12 Oct 2021) - [strFilter]
                    'Sohail (31 Mar 2020) - [blnApplyEmpExemptionFilter, blnApplyEmpExemptionFilterForTerminated]
                    'Sohail (04 Apr 2019) -- End
                    'Anjan [10 June 2015] -- End


            End Select
            'Sohail (22 May 2012) -- End
            'Sohail (03 Nov 2010) -- End

            'Sohail (14 Feb 2019) -- Start
            'NMB Enhancement - 76.1 - Performance enhancement on process payroll employee list.
            'If strEmployeeList.Length > 0 Then
            '    dtTable = New DataView(dsEmployee.Tables("Employee"), "employeeunkid NOT IN (" & strEmployeeList & ")", "", DataViewRowState.CurrentRows).ToTable
            'Else
            '    dtTable = New DataView(dsEmployee.Tables("Employee")).ToTable
            'End If
                dtTable = New DataView(dsEmployee.Tables("Employee")).ToTable
            'Sohail (14 Feb 2019) -- End

            'Sohail (21 Jun 2012) -- Start
            'TRA - ENHANCEMENT
            'lblEmpCount.Text = "( " & dtTable.Rows.Count.ToString & " )" 'Sohail (22 May 2012)
            'Sohail (26 Aug 2016) -- Start
            'Enhancement - 57.4 & 63.1 - Process Payroll Performance enhancement for TRA.
            'Dim dtRow As DataRow
            'Dim intRowsCount As Integer = dtTable.Rows.Count
            'objlblEmpCount.Text = "( " & intRowsCount.ToString & " )"
            mintTotalEmployee = dtTable.Rows.Count
            objlblEmpCount.Text = "( 0 / " & mintTotalEmployee.ToString & " )"
            'Sohail (26 Aug 2016) -- End
            'Sohail (21 Jun 2012) -- End

            'Sohail (26 Aug 2016) -- Start
            'Enhancement - 57.4 & 63.1 - Process Payroll Performance enhancement for TRA.
            ''Sohail (21 Jun 2012) -- Start
            ''TRA - ENHANCEMENT
            'For i As Integer = 0 To intRowsCount - 1
            '    'For Each dtRow As DataRow In dtTable.Rows
            '    dtRow = dtTable.Rows(i)
            '    'Sohail (21 Jun 2012) -- End
            '    lvItem = New ListViewItem
            '    With lvItem
            '        .Text = ""
            '        .Tag = dtRow.Item("employeeunkid").ToString
            '        .SubItems.Add(dtRow.Item("employeecode").ToString)
            '        .SubItems.Add(dtRow.Item("employeename").ToString)
            '        'Sohail (04 Mar 2011) -- Start
            '        If objDicPaidEmp.ContainsKey(CInt(.Tag)) = True Then
            '            .SubItems.Add("YES")
            '            .SubItems(colhIsPaymentDone.Index).Tag = objDicPaidEmp.Item(CInt(.Tag))
            '            .ForeColor = Color.Blue
            '        Else
            '            .SubItems.Add("NO")
            '            .SubItems(colhIsPaymentDone.Index).Tag = 0
            '            .ForeColor = Color.Black
            '        End If
            '        'Sohail (04 Mar 2011) -- End
            '        'Sohail (09 Oct 2010) -- Start
            '        'RemoveHandler lvEmployeeList.ItemChecked, AddressOf lvEmployeeList_ItemChecked
            '        'lvEmployeeList.Items.Add(lvItem) 'Sohail (03 Nov 2010)
            '        'AddHandler lvEmployeeList.ItemChecked, AddressOf lvEmployeeList_ItemChecked
            '        lvArray.Add(lvItem) 'Sohail (03 Nov 2010)
            '        'Sohail (09 Oct 2010) -- End
            '    End With
            'Next
            'RemoveHandler lvEmployeeList.ItemChecked, AddressOf lvEmployeeList_ItemChecked
            'lvEmployeeList.Items.AddRange(lvArray.ToArray) 'Sohail (03 Nov 2010)
            'AddHandler lvEmployeeList.ItemChecked, AddressOf lvEmployeeList_ItemChecked
            'If lvEmployeeList.Items.Count > 22 Then
            '    colhName.Width = 140 - 18
            'Else
            '    colhName.Width = 140
            'End If
            'lvArray = Nothing
            'lvEmployeeList.EndUpdate() 'Sohail (09 Oct 2010)
            If dtTable.Columns.Contains("IsChecked") = False Then
                Dim dtCol As New DataColumn("IsChecked", Type.GetType("System.Boolean"))
                dtCol.DefaultValue = False
                dtCol.AllowDBNull = False
                dtTable.Columns.Add(dtCol)
            End If
            If dtTable.Columns.Contains("IsPaymentDone") = False Then
                Dim dtCol As New DataColumn("IsPaymentDone", Type.GetType("System.Boolean"))
                dtCol.DefaultValue = False
                dtCol.AllowDBNull = False
                dtTable.Columns.Add(dtCol)

                If chkExcludeWithPayment.Checked = False Then 'Sohail (14 Feb 2019)
                For Each dtRow As DataRow In dtTable.Rows
                    If objDicPaidEmp.ContainsKey(CInt(dtRow.Item("employeeunkid"))) = True Then
                        dtRow.Item("IsPaymentDone") = True
                    Else
                        dtRow.Item("IsPaymentDone") = False
                    End If
                Next
                dtTable.AcceptChanges()
                End If 'Sohail (14 Feb 2019)
            End If

            dvEmployee = dtTable.DefaultView
            dgEmployee.AutoGenerateColumns = False
            objdgcolhCheck.DataPropertyName = "IsChecked"
            objdgcolhEmployeeunkid.DataPropertyName = "employeeunkid"
            dgColhEmpCode.DataPropertyName = "employeecode"
            dgColhEmployee.DataPropertyName = "employeename"
            objdgcolhIsPaymentDone.DataPropertyName = "IsPaymentDone"

            'Sohail (14 Feb 2019) -- Start
            'NMB Enhancement - 76.1 - Performance enhancement on process payroll employee list.
            RemoveHandler dgEmployee.SelectionChanged, AddressOf dgEmployee_SelectionChanged
            RemoveHandler dgEmployee.DataBindingComplete, AddressOf dgEmployee_DataBindingComplete
            'Sohail (14 Feb 2019) -- End
            dgEmployee.DataSource = dvEmployee
            'Sohail (14 Feb 2019) -- Start
            'NMB Enhancement - 76.1 - Performance enhancement on process payroll employee list.
            AddHandler dgEmployee.SelectionChanged, AddressOf dgEmployee_SelectionChanged
            AddHandler dgEmployee.DataBindingComplete, AddressOf dgEmployee_DataBindingComplete
            'Sohail (14 Feb 2019) -- End
            dvEmployee.Sort = "IsChecked DESC, employeename "
            'Sohail (26 Aug 2016) -- End

            Cursor.Current = Cursors.Default 'Sohail (01 Dec 2010)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillList", mstrModuleName)
        Finally
            objEmployee = Nothing
            objPayment = Nothing
            dsEmployee = Nothing
            dtTable = Nothing
            'lvEmployeeList.EndUpdate() 'Sohail (21 Aug 2015)
            Cursor.Current = Cursors.Default 'Sohail (04 Mar 2011)
        End Try
    End Sub

    Private Sub FillEDList(ByVal intEmpUnkID As Integer)
        Dim objED As New clsEarningDeduction
        Dim objTranHead As New clsTransactionHead
        Dim objCommonMaster As New clsMasterData
        Dim objEmpCostCenter As New clsemployee_costcenter_Tran
        Dim objEmployee As New clsEmployee_Master
        Dim objCostCenter As New clscostcenter_master
        Dim dsList As DataSet
        'Dim dsCalcType As DataSet 'Sohail (19 Mar 2016)
        Dim lvItem As ListViewItem
        'Sohail (18 Jan 2016) -- Start
        'Enhancement - Performance Enhancement for employee selection change on Process payroll screen in 58.1.
        Dim intDefaultCCenterID As Integer = 0
        Dim strDefaultCCenterName As String = ""
        'Sohail (18 Jan 2016) -- End

        Try
            lvED.Items.Clear()
            'Sohail (14 Jun 2011) -- Start
            'Changes : Display only approved ed heads.
            'dsList = objED.GetList("ED", intEmpUnkID, , , , , , , , , , , , CInt(cboPayPeriod.SelectedValue))
            'Sohail (13 Jan 2012) -- Start
            'TRA - ENHANCEMENT
            'dsList = objED.GetList("ED", intEmpUnkID, , , , , , , , , , , , CInt(cboPayPeriod.SelectedValue), enEDHeadApprovalStatus.Approved)
            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'dsList = objED.GetList("ED", intEmpUnkID.ToString, , , , , , , , , , , , CInt(cboPayPeriod.SelectedValue), enEDHeadApprovalStatus.Approved, , , , mdtPayPeriodEndDate)
            'Sohail (12 Oct 2021) -- Start
            'NMB Issue :  : Performance issue on process payroll.
            'dsList = objED.GetList(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, mdtPayPeriodStartDate, mdtPayPeriodEndDate, ConfigParameter._Object._UserAccessModeSetting, True, ConfigParameter._Object._IsIncludeInactiveEmp, "ED", True, intEmpUnkID.ToString, , CInt(cboPayPeriod.SelectedValue), enEDHeadApprovalStatus.Approved, , , mdtPayPeriodEndDate)
            dsList = objED.GetList(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, mdtPayPeriodStartDate, mdtPayPeriodEndDate, ConfigParameter._Object._UserAccessModeSetting, True, ConfigParameter._Object._IsIncludeInactiveEmp, "ED", False, intEmpUnkID.ToString, , CInt(cboPayPeriod.SelectedValue), enEDHeadApprovalStatus.Approved, , , mdtPayPeriodEndDate, , , , , False)
            'Sohail (12 Oct 2021) -- End
            'Sohail (21 Aug 2015) -- End
            'Sohail (13 Jan 2012) -- End
            'Sohail (14 Jun 2011) -- End

            'Sohail (18 Jan 2016) -- Start
            'Enhancement - Performance Enhancement for employee selection change on Process payroll screen in 58.1.
            'Sohail (29 Mar 2017) -- Start
            'CCBRT Enhancement - 66.1 - Employee cost center distribution for non-salary heads, Effective period option on Employee Cost Center.
            'Dim dsFill As DataSet = objEmpCostCenter.GetList(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, mdtPayPeriodStartDate, mdtPayPeriodEndDate, ConfigParameter._Object._UserAccessModeSetting, True, ConfigParameter._Object._IsIncludeInactiveEmp, "CostCenter", True, intEmpUnkID)
            'Sohail (12 Oct 2021) -- Start
            'NMB Issue :  : Performance issue on process payroll.
            'Dim dsFill As DataSet = objEmpCostCenter.GetList(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, mdtPayPeriodStartDate, mdtPayPeriodEndDate, ConfigParameter._Object._UserAccessModeSetting, True, ConfigParameter._Object._IsIncludeInactiveEmp, "CostCenter", True, intEmpUnkID, , , mdtPayPeriodEndDate)
            Dim dsFill As DataSet = objEmpCostCenter.GetList(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, mdtPayPeriodStartDate, mdtPayPeriodEndDate, ConfigParameter._Object._UserAccessModeSetting, True, ConfigParameter._Object._IsIncludeInactiveEmp, "CostCenter", False, intEmpUnkID, , , mdtPayPeriodEndDate, , , , , False)
            'Sohail (12 Oct 2021) -- End
            'Sohail (29 Mar 2017) -- End
            objEmployee._Employeeunkid(mdtPayPeriodEndDate) = intEmpUnkID
            If objEmployee._Costcenterunkid > 0 Then
                objCostCenter._Costcenterunkid = objEmployee._Costcenterunkid
                intDefaultCCenterID = objEmployee._Costcenterunkid
                strDefaultCCenterName = objCostCenter._Costcentername
            End If
            'Sohail (18 Jan 2016) -- End


            For Each dsRow As DataRow In dsList.Tables("ED").Rows
                lvItem = New ListViewItem
                lvItem.Text = dsRow.Item("edunkid").ToString
                lvItem.Tag = dsRow.Item("edunkid").ToString

                lvItem.SubItems.Add(dsRow.Item("employeename").ToString)
                lvItem.SubItems(1).Tag = dsRow.Item("employeeunkid").ToString

                'Sohail (18 Jan 2016) -- Start
                'Enhancement - Performance Enhancement for employee selection change on Process payroll screen in 58.1.
                ''Sohail (21 Aug 2015) -- Start
                ''Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                ''objTranHead._Tranheadunkid = CInt(dsRow.Item("tranheadunkid"))
                'objTranHead._Tranheadunkid(FinancialYear._Object._DatabaseName) = CInt(dsRow.Item("tranheadunkid"))
                ''Sohail (21 Aug 2015) -- End
                'lvItem.SubItems.Add(objTranHead._Trnheadname)
                'lvItem.SubItems(2).Tag = dsRow.Item("tranheadunkid").ToString

                'objTranHeadType.SelectedValue = objTranHead._Trnheadtype_Id
                'lvItem.SubItems.Add(objTranHeadType.Text)
                'lvItem.SubItems(3).Tag = objTranHead._Trnheadtype_Id.ToString

                'Select Case objTranHead._Typeof_id
                '    Case Is > 1
                '        dsCalcType = objCommonMaster.getComboListCalcType("CalcType", "2") 'Sohail (28 Jan 2012)
                '    Case Else
                '        'Sohail (28 Jan 2012) -- Start
                '        'TRA - ENHANCEMENT
                '        'dsCalcType = objCommonMaster.getComboListCalcType("CalcType", objTranHead._Typeof_id.ToString)
                '        dsCalcType = objCommonMaster.getComboListCalcType("CalcType", (enTypeOf.Salary & "," & enTypeOf.Allowance).ToString)
                '        'Sohail (28 Jan 2012) -- End
                'End Select

                'With objCalcType
                '    .ValueMember = "Id"
                '    .DisplayMember = "Name"
                '    .DataSource = dsCalcType.Tables("CalcType")
                '    If .Items.Count > 0 Then .SelectedIndex = 0
                'End With
                ''Sohail (06 Aug 2010) -- Start
                ''Sohail (11 Sep 2010) -- Start
                'objCalcType.SelectedValue = objTranHead._Calctype_Id
                ''If objTranHead._Calctype_Id = enCalcType.DEFINED_SALARY Or _
                ''    objTranHead._Calctype_Id = enCalcType.OnHourWorked Or _
                ''    objTranHead._Calctype_Id = enCalcType.OnAttendance Then

                ''    objCalcType.SelectedValue = enCalcType.OnAttendance
                ''Else
                ''objCalcType.SelectedValue = objTranHead._Calctype_Id
                ''End If
                ''Sohail (11 Sep 2010) -- End
                ''Sohail (06 Aug 2010) -- End
                'lvItem.SubItems.Add(objCalcType.Text)
                'objCalcType.SelectedValue = 0

                'lvItem.SubItems(4).Tag = objTranHead._Calctype_Id.ToString
                lvItem.SubItems.Add(dsRow.Item("trnheadname").ToString)
                lvItem.SubItems(colhTranHead.Index).Tag = dsRow.Item("tranheadunkid").ToString
                lvItem.SubItems.Add(dsRow.Item("trnheadtype_name").ToString)
                lvItem.SubItems(colhTranHeadType.Index).Tag = dsRow.Item("trnheadtype_id").ToString
                lvItem.SubItems.Add(dsRow.Item("calctype_name").ToString)
                lvItem.SubItems(colhCalcType.Index).Tag = dsRow.Item("calctype_id").ToString
                'Sohail (18 Jan 2016) -- End

                If CDec(dsRow.Item("amount").ToString) = 0 Then
                    lvItem.SubItems.Add("")
                Else
                    lvItem.SubItems.Add(Format(dsRow.Item("amount"), "#0.00"))
                End If

                If CInt(dsRow.Item("isexempt").ToString) = 0 Then
                    lvItem.SubItems.Add("No")
                Else
                    lvItem.SubItems.Add("Yes")
                End If

                'Sohail (18 Jan 2016) -- Start
                'Enhancement - Performance Enhancement for employee selection change on Process payroll screen in 58.1.
                ''Sohail (11 Sep 2010) -- Start
                ''Changed : Column added for Cost Center
                'Dim dsFill As DataSet
                ''Sohail (21 Aug 2015) -- Start
                ''Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                ''dsFill = objEmpCostCenter.GetList("CostCenter", True, intEmpUnkID, CInt(dsRow.Item("tranheadunkid")))
                'dsFill = objEmpCostCenter.GetList(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, mdtPayPeriodStartDate, mdtPayPeriodEndDate, ConfigParameter._Object._UserAccessModeSetting, True, ConfigParameter._Object._IsIncludeInactiveEmp, "CostCenter", True, intEmpUnkID, CInt(dsRow.Item("tranheadunkid")))
                ''Sohail (21 Aug 2015) -- End
                'If dsFill.Tables("CostCenter").Rows.Count > 0 Then
                '    lvItem.SubItems.Add(dsFill.Tables("CostCenter").Rows(0).Item("costcentername").ToString)
                '    lvItem.SubItems(colhCostCenter.Index).Tag = CInt(dsFill.Tables("CostCenter").Rows(0).Item("costcenterunkid").ToString)
                'Else
                '    'S.SANDEEP [04 JUN 2015] -- START
                '    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                '    'objEmployee._Employeeunkid = intEmpUnkID
                '    objEmployee._Employeeunkid(mdtPayPeriodEndDate) = intEmpUnkID
                '    'S.SANDEEP [04 JUN 2015] -- END

                '    If objEmployee._Costcenterunkid > 0 Then
                '        objCostCenter._Costcenterunkid = objEmployee._Costcenterunkid
                '        lvItem.SubItems.Add(objCostCenter._Costcentername)
                '        lvItem.SubItems(colhCostCenter.Index).Tag = objEmployee._Costcenterunkid
                '    Else
                '        lvItem.SubItems.Add("")
                '        lvItem.SubItems(colhCostCenter.Index).Tag = 0
                '    End If
                'End If
                ''Sohail (11 Sep 2010) -- End
                'Sohail (19 Mar 2016) -- Start
                'Dim lstCC As List(Of DataRow) = (From p In dsFill.Tables("CostCenter") Where (CInt(p.Item("tranheadunkid")) = CInt(dsRow.Item("tranheadunkid"))) Select (p)).ToList
                Dim intHeadID As Integer = CInt(dsRow.Item("tranheadunkid"))
                Dim lstCC As List(Of DataRow) = (From p In dsFill.Tables("CostCenter") Where (CInt(p.Item("tranheadunkid")) = intHeadID) Select (p)).ToList
                'Sohail (19 Mar 2016) -- End
                If lstCC.Count > 0 Then
                    'Sohail (29 Mar 2017) -- Start
                    'CCBRT Enhancement - 66.1 - Employee cost center distribution for non-salary heads, Effective period option on Employee Cost Center.
                    'lvItem.SubItems.Add(lstCC(0).Item("costcentername").ToString)
                    'Sohail (07 Feb 2019) -- Start
                    'NMB Enhancement - 76.1 - Employee cost centre should be distributed on all allocations instead of only on cost centre.
                    'Dim sCC As String = String.Join(", ", (From p In lstCC Select (p.Item("costcentername").ToString)).ToArray)
                    Dim sCC As String = String.Join(", ", (From p In lstCC Select (p.Item("allocationname").ToString)).ToArray)
                    'Sohail (07 Feb 2019) -- End
                    lvItem.SubItems.Add(sCC)
                    'Sohail (29 Mar 2017) -- End
                    lvItem.SubItems(colhCostCenter.Index).Tag = CInt(lstCC(0).Item("costcenterunkid").ToString)
                Else
                    lvItem.SubItems.Add(strDefaultCCenterName)
                    lvItem.SubItems(colhCostCenter.Index).Tag = intDefaultCCenterID
                End If
                'Sohail (18 Jan 2016) -- End

                'Sohail (28 Jan 2012) -- Start
                'TRA - ENHANCEMENT
                lvItem.SubItems.Add(dsRow.Item("period_name").ToString)
                lvItem.SubItems(colhPeriod.Index).Tag = CInt(dsRow.Item("periodunkid"))
                'Sohail (28 Jan 2012) -- End

                lvED.Items.Add(lvItem)
            Next

            If lvED.Items.Count > 9 Then
                colhTranHead.Width = 150 - 18
            Else
                colhTranHead.Width = 150
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillList", mstrModuleName)
        Finally
            objTranHead = Nothing
            objCommonMaster = Nothing
            objED = Nothing
            objEmpCostCenter = Nothing
            objEmployee = Nothing
            objCostCenter = Nothing
        End Try
    End Sub

    Private Sub CheckAllEmployee(ByVal blnCheckAll As Boolean)
        Try
            'Sohail (26 Aug 2016) -- Start
            'Enhancement - 57.4 & 63.1 - Process Payroll Performance enhancement for TRA.
            'For Each lvItem As ListViewItem In lvEmployeeList.Items
            '    RemoveHandler lvEmployeeList.ItemChecked, AddressOf lvEmployeeList_ItemChecked 'Sohail (03 Nov 2010)
            '    lvItem.Checked = blnCheckAll
            '    AddHandler lvEmployeeList.ItemChecked, AddressOf lvEmployeeList_ItemChecked 'Sohail (03 Nov 2010)
            'Next
            If dvEmployee IsNot Nothing Then
                For Each dr As DataRowView In dvEmployee
                    RemoveHandler dgEmployee.CellContentClick, AddressOf dgEmployee_CellContentClick
                    RemoveHandler dgEmployee.SelectionChanged, AddressOf dgEmployee_SelectionChanged
                    dr.Item("IsChecked") = blnCheckAll
                    dr.EndEdit()
                    AddHandler dgEmployee.CellContentClick, AddressOf dgEmployee_CellContentClick
                    AddHandler dgEmployee.SelectionChanged, AddressOf dgEmployee_SelectionChanged
                Next
                dvEmployee.ToTable.AcceptChanges()
                lvED.Items.Clear()

                Dim drRow As DataRow() = dvEmployee.Table.Select("IsChecked = 1")
                mintCount = drRow.Length
                objlblEmpCount.Text = "( " & mintCount.ToString & " / " & mintTotalEmployee.ToString & " )"
            End If
            'Sohail (26 Aug 2016) -- End
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "CheckAllEmployee", mstrModuleName)
        End Try
    End Sub

    Private Sub SetVisibility()

        Try
            btnNew.Enabled = User._Object.Privilege._AddEarningDeduction
            btnEdit.Enabled = User._Object.Privilege._EditEarningDeduction
            btnDelete.Enabled = User._Object.Privilege._DeleteEarningDeduction
            btnExempt.Enabled = User._Object.Privilege._AddEmployeeExemption
            btnCostCenter.Enabled = User._Object.Privilege._AddEmployeeCostCenter
            btnVoidPayroll.Enabled = User._Object.Privilege._AllowVoidPayroll 'Sohail (22 May 2012)
            'Sohail (18 Jun 2019) -- Start
            'Eko Supreme Nigeria Ltd - Support issue # 0003911 - 74.1 : User able to see Payslip l Eko Supreme Nigeria Ltd.
            btnProcess.Enabled = User._Object.Privilege._AllowProcessPayroll
            'Sohail (18 Jun 2019) -- End
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetVisibility", mstrModuleName)
        End Try

    End Sub

    'Sohail (06 Jan 2012) -- Start
    'TRA - ENHANCEMENT
    Private Sub FillEmployeeCombo()
        Dim objEmployee As New clsEmployee_Master
        Dim dsCombo As DataSet
        Try
            'Sohail (12 May 2012) -- Start
            'TRA - ENHANCEMENT
            'dsCombo = objEmployee.GetEmployeeList("Employee", True, , , , , , , , , , , , , mdtPayPeriodStartDate, mdtPayPeriodEndDate)

            'Anjan [10 June 2015] -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'dsCombo = objEmployee.GetEmployeeList("Employee", True, , , , , , , , , , , , , mdtPayPeriodStartDate, mdtPayPeriodEndDate, , , , True, mstrAdvanceFilter)

            dsCombo = objEmployee.GetEmployeeList(FinancialYear._Object._DatabaseName, _
                                       User._Object._Userunkid, _
                                       FinancialYear._Object._YearUnkid, _
                                       Company._Object._Companyunkid, _
                                       mdtPayPeriodStartDate, _
                                       mdtPayPeriodEndDate, _
                                       ConfigParameter._Object._UserAccessModeSetting, _
                                       True, False, "Employee", True, , , , , , , , , , , , , , True, mstrAdvanceFilter)

            'Anjan [10 June 2015] -- End

            'Sohail (02 Jul 2014) - [mstrAdvanceFilter]
            'Sohail (12 May 2012) -- End
            With cboEmployee
                .BeginUpdate()
                .ValueMember = "employeeunkid"
                .DisplayMember = "employeename"
                .DataSource = dsCombo.Tables("Employee")
                .SelectedValue = 0
                .EndUpdate()
            End With
            'Sohail (26 Oct 2016) -- Start
            'Enhancement - Searchable Dropdown
            Call SetDefaultSearchText(cboEmployee)
            'Sohail (26 Oct 2016) -- End
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillEmployeeCombo", mstrModuleName)
        Finally
            objEmployee = Nothing
        End Try

    End Sub
    'Sohail (06 Jan 2012) -- End

    'Sohail (26 Aug 2016) -- Start
    'Enhancement - 57.4 & 63.1 - Process Payroll Performance enhancement for TRA.
    Private Sub SetCheckBoxValue()
        Try

            RemoveHandler objchkSelectAll.CheckedChanged, AddressOf chkSelectAll_CheckedChanged

            Dim blnIsChecked As Boolean = Convert.ToBoolean(dgEmployee.CurrentRow.Cells(objdgcolhCheck.Index).Value)

            If blnIsChecked = True Then
                mintCount += 1
            Else
                mintCount -= 1
            End If

            objlblEmpCount.Text = "( " & mintCount.ToString & " / " & mintTotalEmployee.ToString & " )"

            If mintCount <= 0 Then
                objchkSelectAll.CheckState = CheckState.Unchecked
            ElseIf mintCount < dgEmployee.Rows.Count Then
                objchkSelectAll.CheckState = CheckState.Indeterminate
            ElseIf mintCount = dgEmployee.Rows.Count Then
                objchkSelectAll.CheckState = CheckState.Checked
            End If

            AddHandler objchkSelectAll.CheckedChanged, AddressOf chkSelectAll_CheckedChanged
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetCheckBoxValue", mstrModuleName)
        End Try

    End Sub

    Private Sub SetGridColour()
        Try

            Dim dgvRow As List(Of DataGridViewRow) = (From rw In dgEmployee.Rows.Cast(Of DataGridViewRow)() Where (rw.DefaultCellStyle.ForeColor = Color.Blue) Select (rw)).ToList
            For Each rw As DataGridViewRow In dgvRow
                rw.DefaultCellStyle.ForeColor = Color.Black
            Next

            dgvRow = (From rw In dgEmployee.Rows.Cast(Of DataGridViewRow)() Where (CBool(rw.Cells(objdgcolhIsPaymentDone.Index).Value) = True) Select (rw)).ToList
            For Each rw As DataGridViewRow In dgvRow
                rw.DefaultCellStyle.ForeColor = Color.Blue
            Next
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetGridColour", mstrModuleName)
        End Try
    End Sub

    Private Sub SetDefaultSearchEmpText()
        Try
            mstrSearchEmpText = lblSearchEmp.Text
            With txtSearchEmp
                .ForeColor = Color.Gray
                .Text = mstrSearchEmpText
                .Font = New Font(Me.Font.FontFamily, Me.Font.Size, FontStyle.Italic)
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "", mstrModuleName)
        End Try
    End Sub

    Private Sub EnableControls(ByVal blnEnable As Boolean)
        Try
            Dim lst As IEnumerable(Of Control) = pnlMainInfo.Controls.OfType(Of Control)().Where(Function(t) t.Name <> objFooter.Name)
            For Each ctrl In lst
                ctrl.Enabled = blnEnable
            Next
            lst = objFooter.Controls.OfType(Of Control).Where(Function(x) x.Name <> btnStop.Name)
            For Each ctrl In lst
                ctrl.Enabled = blnEnable
            Next
            If blnEnable = True Then
                btnStop.Visible = False
            Else
                btnStop.Visible = True
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "EnableControls", mstrModuleName)
        End Try
    End Sub
    'Sohail (26 Aug 2016) -- End

    'Sohail (26 Oct 2016) -- Start
    'Enhancement - Searchable Dropdown
    Private Sub SetDefaultSearchText(ByVal cbo As ComboBox)
        Try
            mstrSearchText = Language.getMessage(mstrModuleName, 34, "Type to Search")
            With cbo
                .ForeColor = Color.Gray
                .Text = mstrSearchText
                .Font = New Font(Me.Font.FontFamily, Me.Font.Size, FontStyle.Italic)
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetDefaultSearchText", mstrModuleName)
        End Try
    End Sub
    'Sohail (26 Oct 2016) -- End

    'Sohail (03 May 2018) -- Start
    'Enhancement : Removing global objects and passing objDataOperation in process payroll in 72.1.
    Private Sub ShowCount()
        Dim objPayroll As New clsPayrollProcessTran
        Dim ds As DataSet
        Try
            'Sohail (18 Jun 2019) -- Start
            'Eko Supreme Nigeria Ltd - Support issue # 0003911 - 74.1 : User able to see Payslip l Eko Supreme Nigeria Ltd.
            bdgNotProcessed.Enabled = User._Object.Privilege._AllowProcessPayroll
            bdgProcessed.Enabled = User._Object.Privilege._AllowProcessPayroll
            bdgTotalEmployee.Enabled = User._Object.Privilege._AllowProcessPayroll
            bdgUnpaid.Enabled = User._Object.Privilege._AllowGlobalPayment
            bdgTotalPaid.Enabled = User._Object.Privilege._DeletePayment
            bdgTotalAuthorized.Enabled = User._Object.Privilege._AllowAuthorizePayslipPayment
            'Sohail (18 Jun 2019) -- End

            ds = objPayroll.GetPayrollProcessCount("List", FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, mdtPayPeriodStartDate, mdtPayPeriodEndDate, ConfigParameter._Object._UserAccessModeSetting, True, ConfigParameter._Object._IsIncludeInactiveEmp, CInt(cboPayPeriod.SelectedValue), True, True, mstrAdvanceFilter, "", Nothing, True)
            'Sohail (31 Mar 2020) - [blnApplyEmpExemptionFilter]

            If ds.Tables(0).Rows.Count > 0 Then
                'Sohail (18 Jun 2019) -- Start
                'Eko Supreme Nigeria Ltd - Support issue # 0003911 - 74.1 : User able to see Payslip l Eko Supreme Nigeria Ltd.
                'bdgNotProcessed.Text_Number = ds.Tables(0).Rows(0)("TotalNotProcessed").ToString
                'bdgProcessed.Text_Number = ds.Tables(0).Rows(0)("TotalProcessed").ToString
                'bdgTotalEmployee.Text_Number = (CInt(ds.Tables(0).Rows(0)("TotalNotProcessed")) + CInt(ds.Tables(0).Rows(0)("TotalProcessed"))).ToString
                'bdgTotalPaid.Text_Number = ds.Tables(0).Rows(0)("TotalPaid").ToString
                'bdgTotalAuthorized.Text_Number = ds.Tables(0).Rows(0)("TotalAuthorized").ToString
                ''Sohail (04 Jun 2018) -- Start
                ''Internal Enhancement - Providing Summary count on process payroll screen and Payslip screen in 72.1.
                'bdgUnpaid.Text_Number = ds.Tables(0).Rows(0)("TotalUnpaid").ToString
                ''Sohail (04 Jun 2018) -- End
                If bdgNotProcessed.Enabled = True Then bdgNotProcessed.Text_Number = ds.Tables(0).Rows(0)("TotalNotProcessed").ToString
                If bdgProcessed.Enabled = True Then bdgProcessed.Text_Number = ds.Tables(0).Rows(0)("TotalProcessed").ToString
                If bdgTotalEmployee.Enabled = True Then bdgTotalEmployee.Text_Number = (CInt(ds.Tables(0).Rows(0)("TotalNotProcessed")) + CInt(ds.Tables(0).Rows(0)("TotalProcessed"))).ToString
                If bdgTotalPaid.Enabled = True Then bdgTotalPaid.Text_Number = ds.Tables(0).Rows(0)("TotalPaid").ToString
                If bdgTotalAuthorized.Enabled = True Then bdgTotalAuthorized.Text_Number = ds.Tables(0).Rows(0)("TotalAuthorized").ToString
                If bdgUnpaid.Enabled = True Then bdgUnpaid.Text_Number = ds.Tables(0).Rows(0)("TotalUnpaid").ToString
                'Sohail (18 Jun 2019) -- End
            End If

            'S.SANDEEP |07-MAR-2020| -- START
            'ISSUE/ENHANCEMENT : PAYROLL UAT
            ds = objPayroll.GetPayrollProcessedUnPaid("List", FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, mdtPayPeriodStartDate, mdtPayPeriodEndDate, ConfigParameter._Object._UserAccessModeSetting, True, ConfigParameter._Object._IsIncludeInactiveEmp, CInt(cboPayPeriod.SelectedValue), True, True, mstrAdvanceFilter, "", Nothing, True)
            If ds IsNot Nothing AndAlso ds.Tables(0).Rows.Count > 0 Then
                bdgTotalOverDeduction.Text_Number = ds.Tables(0).Rows(0)("TotalOverdeduction").ToString
            Else
                bdgTotalOverDeduction.Text_Number = "0"
            End If
            'S.SANDEEP |07-MAR-2020| -- END

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "ShowCount", mstrModuleName)
        End Try
    End Sub
    'Sohail (03 May 2018) -- End

#End Region

#Region " Form's Events "

    Private Sub frmProcessPayroll_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        Try
            objTnALeave = Nothing
            objPayroll = Nothing
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmProcessPayroll_FormClosed", mstrModuleName)
        End Try
    End Sub

    Private Sub frmProcessPayroll_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown
        Try
            Select Case e.KeyCode
                Case Keys.Return
                    SendKeys.Send("{TAB}")
            End Select
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmProcessPayroll_KeyDown", mstrModuleName)
        End Try
    End Sub

    Private Sub frmProcessPayroll_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        objTnALeave = New clsTnALeaveTran
        objPayroll = New clsPayrollProcessTran

        Try
            Call Set_Logo(Me, gApplicationType)
            'Anjan (02 Sep 2011)-Start
            'Issue : Including Language Settings.
            Language.setLanguage(Me.Name)
            Call OtherSettings()
            'Anjan (02 Sep 2011)-End 

            Call SetVisibility()
            Call SetColor()
            Call FillCombo()
            'Call FillList

            'Sohail (02 Apr 2018) -- Start
            'Enhancement : Exclude employees if payment is done to prevent re-process payroll done by mistake in 71.1.
            'Sohail (03 May 2018) -- Start
            'Enhancement : Removing global objects and passing objDataOperation in process payroll in 72.1.
            'chkExcludeWithPayment.Checked = True
            RemoveHandler chkExcludeWithPayment.CheckedChanged, AddressOf chkExcludeWithPayment_CheckedChanged
            chkExcludeWithPayment.Checked = True
            AddHandler chkExcludeWithPayment.CheckedChanged, AddressOf chkExcludeWithPayment_CheckedChanged
            'Sohail (03 May 2018) -- End
            'Sohail (02 Apr 2018) -- End

            'Sohail (26 Aug 2016) -- Start
            'Enhancement - 57.4 & 63.1 - Process Payroll Performance enhancement for TRA.
            Call SetDefaultSearchEmpText()
            'Sohail (26 Aug 2016) -- End

            'Sohail (03 May 2018) -- Start
            'Enhancement : Removing global objects and passing objDataOperation in process payroll in 72.1.
            Call ShowCount()
            'Sohail (03 May 2018) -- End

            'Sohail (07 Jan 2014) -- Start
            'Enhancement - Separate TnA Periods from Payroll Periods
            If ConfigParameter._Object._IsSeparateTnAPeriod = True Then
                lblTnAEndDate.Visible = True
                objlblTnAEndDate.Visible = True
            Else
                lblTnAEndDate.Visible = False
                objlblTnAEndDate.Visible = False
            End If
            'Sohail (07 Jan 2014) -- End

            'Hemant (31 July 2018) -- Start
            'Enhancement : Support Issue Id # 2410 - Changes to stop Payroll process of employees who had payment was already done 
            chkExcludeWithPayment.Checked = True
            chkExcludeWithPayment.Visible = False
            'Hemant (31 July 2018) -- End

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmProcessPayroll_Load", mstrModuleName)
        End Try
    End Sub

    'Anjan (02 Sep 2011)-Start
    'Issue : Including Language Settings.
    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsPayrollProcessTran.SetMessages()
            objfrm._Other_ModuleNames = "clsPayrollProcessTran"
            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub
    'Anjan (02 Sep 2011)-End 


#End Region

#Region " ComboBox's Events "
    Private Sub cboPayYear_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboPayYear.SelectedIndexChanged
        Dim objPeriod As New clscommom_period_Tran
        Dim dsCombo As DataSet = Nothing
        Dim dtTable As DataTable

        Try
            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'dsCombo = objPeriod.getListForCombo(enModuleReference.Payroll, CInt(cboPayYear.SelectedValue), "PayPeriod", True, enStatusType.Open)
            dsCombo = objPeriod.getListForCombo(enModuleReference.Payroll, CInt(cboPayYear.SelectedValue), FinancialYear._Object._DatabaseName, FinancialYear._Object._Database_Start_Date, "PayPeriod", True, enStatusType.Open)
            'Sohail (21 Aug 2015) -- End
            'Sohail (04 Mar 2011) -- Start
            'Changes : Allow to do process for current period too as per rutta's comment's to see the current status of salary.
            'dtTable = New DataView(dsCombo.Tables("PayPeriod"), "end_date <= '" & eZeeDate.convertDate(DateTime.Today) & "'", "end_date", DataViewRowState.CurrentRows).ToTable
            'Sohail (25 Jun 2020) -- Start
            'NMB Enhancement # : Allow to process payroll before xx days of start date for future period if Allow Period Closer Before XX Days setting is ticked.
            'dtTable = New DataView(dsCombo.Tables("PayPeriod"), "start_date <= '" & eZeeDate.convertDate(DateTime.Today) & "'", "end_date", DataViewRowState.CurrentRows).ToTable
            If ConfigParameter._Object._AllowToClosePeriodBefore Then
                dtTable = New DataView(dsCombo.Tables("PayPeriod"), "start_date <= '" & eZeeDate.convertDate(DateTime.Today.AddDays(CInt(ConfigParameter._Object._ClosePeriodDaysBefore) + 1)) & "'", "end_date", DataViewRowState.CurrentRows).ToTable
            Else
            dtTable = New DataView(dsCombo.Tables("PayPeriod"), "start_date <= '" & eZeeDate.convertDate(DateTime.Today) & "'", "end_date", DataViewRowState.CurrentRows).ToTable
            End If
            'Sohail (25 Jun 2020) -- End
            'Sohail (04 Mar 2011) -- End

            'Sohail (03 May 2018) -- Start
            'Enhancement : Removing global objects and passing objDataOperation in process payroll in 72.1.
            Dim intFirstOpenPeriodId As Integer = 0
            Dim objMaster As New clsMasterData
            intFirstOpenPeriodId = objMaster.getFirstPeriodID(CInt(enModuleReference.Payroll), FinancialYear._Object._YearUnkid, CInt(enStatusType.Open))
            'Sohail (03 May 2018) -- End

            With cboPayPeriod
                .ValueMember = "periodunkid"
                .DisplayMember = "Name"
                RemoveHandler cboPayPeriod.SelectedIndexChanged, AddressOf cboPayPeriod_SelectedIndexChanged
                .DataSource = dtTable
                AddHandler cboPayPeriod.SelectedIndexChanged, AddressOf cboPayPeriod_SelectedIndexChanged
                'Sohail (03 May 2018) -- Start
                'Enhancement : Removing global objects and passing objDataOperation in process payroll in 72.1.
                'If .Items.Count > 0 Then .SelectedValue = 0
                .SelectedValue = intFirstOpenPeriodId
                'Sohail (03 May 2018) -- End
            End With
            'Sohail (26 Oct 2016) -- Start
            'Enhancement - Searchable Dropdown
            Call SetDefaultSearchText(cboPayPeriod)
            'Sohail (26 Oct 2016) -- End
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboPayYear_SelectedIndexChanged", mstrModuleName)
        Finally
            objPeriod = Nothing
            dtTable = Nothing
            dsCombo = Nothing
        End Try
    End Sub

    Private Sub cboPayPeriod_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboPayPeriod.SelectedIndexChanged
        Dim objPeriod As New clscommom_period_Tran
        Dim dsList As DataSet = Nothing
        Try
            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'objPeriod._Periodunkid = CInt(cboPayPeriod.SelectedValue)
            objPeriod._Periodunkid(FinancialYear._Object._DatabaseName) = CInt(cboPayPeriod.SelectedValue)
            'Sohail (21 Aug 2015) -- End
            'Sohail (07 Jan 2014) -- Start
            'Enhancement - Separate TnA Periods from Payroll Periods
            dtpAsOnDate.Tag = objPeriod._TnA_EndDate
            'Sohail (07 Jan 2014) -- End
            mdtPayPeriodStartDate = objPeriod._Start_Date
            mdtPayPeriodEndDate = objPeriod._End_Date

            'Sohail (03 May 2018) -- Start
            'Enhancement : Removing global objects and passing objDataOperation in process payroll in 72.1.
            lblPeriodName.Text = objPeriod._Period_Name
            'Sohail (03 May 2018) -- End

            'Sohail (04 Mar 2011) -- Start
            If CInt(cboPayPeriod.SelectedValue) > 0 Then
                dtpAsOnDate.MinDate = DateTimePicker.MinimumDateTime.Date
                dtpAsOnDate.MaxDate = DateTimePicker.MaximumDateTime.Date
                dtpAsOnDate.MinDate = mdtPayPeriodStartDate

                'Anjan [19 Febreuary 2015] -- Start
                'Issue : Rutta wanted to set default end date of period in date as on coz if employee joins in between were getting missing out for payroll process as it was picking todays day.
                'If mdtPayPeriodEndDate > DateTime.Today.Date Then
                '    dtpAsOnDate.MaxDate = DateTime.Today.Date
                '    dtpAsOnDate.Value = DateTime.Today.Date
                'Else
                '    dtpAsOnDate.MaxDate = mdtPayPeriodEndDate
                '    dtpAsOnDate.Value = mdtPayPeriodEndDate
                'End If
                dtpAsOnDate.MaxDate = mdtPayPeriodEndDate
                dtpAsOnDate.Value = mdtPayPeriodEndDate
                'Anjan [19 Febreuary 2015] -- End

                'Sohail (21 Aug 2015) -- Start
                'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            Else
                mdtPayPeriodEndDate = eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate)
                mdtPayPeriodEndDate = eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate)
                'Sohail (21 Aug 2015) -- End
                'Sohail (26 Oct 2016) -- Start
                'Enhancement - Searchable Dropdown
                Call SetDefaultSearchText(cboPayPeriod)
                'Sohail (26 Oct 2016) -- End
            End If
            'Sohail (04 Mar 2011) -- End

            If objchkSelectAll.Checked = True Then objchkSelectAll.Checked = False 'Sohail (09 Oct 2010)
            'Sohail (06 Jan 2012) -- Start
            'TRA - ENHANCEMENT
            'Sohail (03 May 2018) -- Start
            'Enhancement : Removing global objects and passing objDataOperation in process payroll in 72.1.

            'S.SANDEEP |07-MAR-2020| -- START
            'ISSUE/ENHANCEMENT : PAYROLL UAT
            Call FillEmployeeCombo()
            'S.SANDEEP |07-MAR-2020| -- END

            'Sohail (03 May 2018) -- End
            'Sohail (06 Jan 2012) -- End
            'Sohail (03 May 2018) -- Start
            'Enhancement : Removing global objects and passing objDataOperation in process payroll in 72.1.
            'Call FillList()
            'Sohail (03 May 2018) -- End
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboPayPeriod_SelectedIndexChanged", mstrModuleName)
        Finally
            objPeriod = Nothing
            dsList = Nothing
        End Try
    End Sub

    'Sohail (22 May 2012) -- Start
    'TRA - ENHANCEMENT
    Private Sub cboModes_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboModes.SelectedIndexChanged
        Try
            RemoveHandler chkExcludeWithPayment.CheckedChanged, AddressOf chkExcludeWithPayment_CheckedChanged
            btnProcess.Enabled = False
            'Sohail (04 Apr 2019) -- Start
            'NMB Enhancement - 76.1 - Performance enhancement on process payroll employee list..
            'chkExcludeWithPayment.Checked = False
            'Sohail (04 Apr 2019) -- End
            chkExcludeWithPayment.Enabled = False
            Select Case cboModes.SelectedIndex
                Case 0 '*** Show Active Employees
                    btnProcess.Enabled = True
                    chkExcludeWithPayment.Enabled = True
                Case 1 '*** Show Terminated Employees with Payroll Excluded
                    chkExcludeWithPayment.Checked = True
                Case 2 '*** Show Terminated Employees without Payroll Excluded
                    btnProcess.Enabled = True
                    chkExcludeWithPayment.Checked = True
            End Select
            Call FillList()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboModes_SelectedIndexChanged", mstrModuleName)
        Finally
            RemoveHandler chkExcludeWithPayment.CheckedChanged, AddressOf chkExcludeWithPayment_CheckedChanged
            AddHandler chkExcludeWithPayment.CheckedChanged, AddressOf chkExcludeWithPayment_CheckedChanged
        End Try
    End Sub
    'Sohail (22 May 2012) -- End

    'Sohail (26 Aug 2016) -- Start
    'Enhancement - 57.4 & 63.1 - Process Payroll Performance enhancement for TRA.
    Private Sub cboEmployee_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles cboEmployee.KeyPress
        Try
            If (CInt(AscW(e.KeyChar)) >= 65 AndAlso CInt(AscW(e.KeyChar)) <= 90) Or (CInt(AscW(e.KeyChar)) >= 97 AndAlso CInt(AscW(e.KeyChar)) <= 122) Or (CInt(AscW(e.KeyChar)) >= 47 AndAlso CInt(AscW(e.KeyChar)) <= 57) Then
                Dim frm As New frmCommonSearch
                With frm
                    .ValueMember = cboEmployee.ValueMember
                    .DisplayMember = cboEmployee.DisplayMember
                    .DataSource = CType(cboEmployee.DataSource, DataTable)
                    .CodeMember = "employeecode"
                End With
                Dim c As Char = Convert.ToChar(e.KeyChar)
                frm.TypedText = c.ToString
                If frm.DisplayDialog Then
                    cboEmployee.SelectedValue = frm.SelectedValue
                    e.KeyChar = ChrW(Keys.ShiftKey)
                Else
                    cboEmployee.Text = ""
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboEmployee_KeyPress", mstrModuleName)
        End Try
    End Sub

    Private Sub cboEmployee_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboEmployee.SelectedIndexChanged
        Try
            'Sohail (26 Oct 2016) -- Start
            'Enhancement - Searchable Dropdown
            'If CInt(cboEmployee.SelectedValue) <= 0 Then cboEmployee.Text = ""
            If CInt(cboEmployee.SelectedValue) <= 0 Then Call SetDefaultSearchText(cboEmployee)
            'Sohail (26 Oct 2016) -- End
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboEmployee_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub
    'Sohail (26 Aug 2016) -- End

    'Sohail (26 Oct 2016) -- Start
    'Enhancement - Searchable Dropdown
    Private Sub cboPayPeriod_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles cboPayPeriod.KeyPress
        Try
            If (CInt(AscW(e.KeyChar)) >= 65 AndAlso CInt(AscW(e.KeyChar)) <= 90) Or (CInt(AscW(e.KeyChar)) >= 97 AndAlso CInt(AscW(e.KeyChar)) <= 122) Or (CInt(AscW(e.KeyChar)) >= 47 AndAlso CInt(AscW(e.KeyChar)) <= 57) Then
                Dim frm As New frmCommonSearch
                With frm
                    .ValueMember = cboPayPeriod.ValueMember
                    .DisplayMember = cboPayPeriod.DisplayMember
                    .DataSource = CType(cboPayPeriod.DataSource, DataTable)
                    .CodeMember = "code"
                End With
                Dim c As Char = Convert.ToChar(e.KeyChar)
                frm.TypedText = c.ToString
                If frm.DisplayDialog Then
                    cboPayPeriod.SelectedValue = frm.SelectedValue
                    e.KeyChar = ChrW(Keys.ShiftKey)
                Else
                    cboPayPeriod.Text = ""
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboPayPeriod_KeyPress", mstrModuleName)
        End Try
    End Sub

    Private Sub cboEmployee_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboEmployee.GotFocus
        Try
            With cboEmployee
                .ForeColor = Color.Black
                .Font = New Font(Me.Font.FontFamily, Me.Font.Size, FontStyle.Regular)

                If .Text = mstrSearchText Then
                    .Text = ""
                End If
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboEmployee_GotFocus", mstrModuleName)
        End Try
    End Sub

    Private Sub cboEmployee_Leave(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboEmployee.Leave
        Try
            If CInt(cboEmployee.SelectedValue) <= 0 Then
                Call SetDefaultSearchText(cboEmployee)
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboEmployee_Leave", mstrModuleName)
        End Try
    End Sub

    Private Sub cboPayPeriod_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboPayPeriod.GotFocus
        Try
            With cboPayPeriod
                .ForeColor = Color.Black
                .Font = New Font(Me.Font.FontFamily, Me.Font.Size, FontStyle.Regular)

                'Sohail (03 May 2018) -- Start
                'Enhancement : Removing global objects and passing objDataOperation in process payroll in 72.1.
                'If .Text = mstrSearchText Then
                If CInt(.SelectedValue) <= 0 Then
                    'Sohail (03 May 2018) -- End
                    .Text = ""
                End If
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboPayPeriod_GotFocus", mstrModuleName)
        End Try
    End Sub

    Private Sub cboPayPeriod_Leave(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboPayPeriod.Leave
        Try
            If CInt(cboPayPeriod.SelectedValue) <= 0 Then
                Call SetDefaultSearchText(cboPayPeriod)
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboPayPeriod_Leave", mstrModuleName)
        End Try
    End Sub
    'Sohail (26 Oct 2016) -- End
#End Region

#Region " Button's Events "
    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnClose_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnNew_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNew.Click
        Dim objFrm As New frmEarningDeduction_AddEdit
        Try
            'If User._Object._RightToLeft = True Then
            '    frm.RightToLeft = Windows.Forms.RightToLeft.Yes
            '    frm.RightToLeftLayout = True
            '    Call Language.ctlRightToLeftlayOut(frm)
            'End If
            If objFrm.displayDialog(-1, enAction.ADD_CONTINUE) Then
                'Sohail (26 Aug 2016) -- Start
                'Enhancement - 57.4 & 63.1 - Process Payroll Performance enhancement for TRA.
                'If lvEmployeeList.SelectedItems.Count = 0 Then Exit Sub
                'Call FillEDList(CInt(lvEmployeeList.Items(lvEmployeeList.SelectedItems(0).Index).Tag))
                If dgEmployee.SelectedRows.Count = 0 Then Exit Sub
                Call FillEDList(CInt(dgEmployee.SelectedRows(0).Cells(objdgcolhEmployeeunkid.Index).Value))
                'Sohail (26 Aug 2016) -- End
            End If
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "btnNew_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnEdit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEdit.Click, lvED.DoubleClick
        If lvED.SelectedItems.Count < 1 Then
            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "Please select Earning/Deduction from the list to perform further operation."), enMsgBoxStyle.Information) '?1
            lvED.Select()
            Exit Sub
        End If
        Dim frm As New frmEarningDeduction_AddEdit
        Dim objTranHead As New clsTransactionHead
        Try

            Dim intSelectedIndex As Integer
            intSelectedIndex = lvED.SelectedItems(0).Index

            'If User._Object._RightToLeft = True Then
            '    frm.RightToLeft = Windows.Forms.RightToLeft.Yes
            '    frm.RightToLeftLayout = True
            '    Call Language.ctlRightToLeftlayOut(frm)
            'End If

            'Sohail (28 Jan 2012) -- Start
            'TRA - ENHANCEMENT
            Dim objPeriod As New clscommom_period_Tran
            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'objPeriod._Periodunkid = CInt(lvED.SelectedItems(0).SubItems(colhPeriod.Index).Tag)
            objPeriod._Periodunkid(FinancialYear._Object._DatabaseName) = CInt(lvED.SelectedItems(0).SubItems(colhPeriod.Index).Tag)
            'Sohail (21 Aug 2015) -- End
            If objPeriod._Statusid = enStatusType.Close Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 19, "Sorry! You can not Edit/Delete this transaction. Reason: Period is closed."), enMsgBoxStyle.Information) '?1
                lvED.Select()
                Exit Sub
            End If
            'Sohail (28 Jan 2012) -- End

            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'objTranHead._Tranheadunkid = CInt(lvED.SelectedItems(0).SubItems(colhTranHead.Index).Tag)
            objTranHead._Tranheadunkid(FinancialYear._Object._DatabaseName) = CInt(lvED.SelectedItems(0).SubItems(colhTranHead.Index).Tag)
            'Sohail (21 Aug 2015) -- End
            If objTranHead._Typeof_id = enTypeOf.Salary Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 7, "Sorry, You can not Edit/Delete Salary Head. Please go to Employee Master to Edit/Delete this Head for this employee."), enMsgBoxStyle.Information)
                Exit Try
            End If

            If frm.displayDialog(CInt(lvED.SelectedItems(0).Tag), enAction.EDIT_ONE) Then
                'Sohail (26 Aug 2016) -- Start
                'Enhancement - 57.4 & 63.1 - Process Payroll Performance enhancement for TRA.
                'If lvEmployeeList.SelectedItems.Count = 0 Then Exit Sub
                'Call FillEDList(CInt(lvEmployeeList.Items(lvEmployeeList.SelectedItems(0).Index).Tag))
                If dgEmployee.SelectedRows.Count = 0 Then Exit Sub
                Call FillEDList(CInt(dgEmployee.SelectedRows(0).Cells(objdgcolhEmployeeunkid.Index).Value))
                'Sohail (26 Aug 2016) -- End
            End If


            lvED.Items(intSelectedIndex).Selected = True
            lvED.EnsureVisible(intSelectedIndex)
            lvED.Select()
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "btnEdit_Click", mstrModuleName)
        Finally
            frm = Nothing
            objTranHead = Nothing
        End Try
    End Sub

    Private Sub btnDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDelete.Click
        If lvED.SelectedItems.Count < 1 Then
            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "Please select Earning/Deduction from the list to perform further operation."), enMsgBoxStyle.Information) '?1
            lvED.Select()
            Exit Sub
        End If

        Dim objED As New clsEarningDeduction
        Dim objTranHead As New clsTransactionHead
        Try

            Dim intSelectedIndex As Integer
            intSelectedIndex = lvED.SelectedItems(0).Index

            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'objTranHead._Tranheadunkid = CInt(lvED.SelectedItems(0).SubItems(colhTranHead.Index).Tag)
            objTranHead._Tranheadunkid(FinancialYear._Object._DatabaseName) = CInt(lvED.SelectedItems(0).SubItems(colhTranHead.Index).Tag)
            'Sohail (21 Aug 2015) -- End
            If objTranHead._Typeof_id = enTypeOf.Salary Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 7, "Sorry, You can not Edit/Delete Salary Head. Please go to Employee Master to Edit/Delete this Head for this employee."), enMsgBoxStyle.Information)
                Exit Try
            End If

            'Sohail (28 Jan 2012) -- Start
            'TRA - ENHANCEMENT
            Dim objPeriod As New clscommom_period_Tran
            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'objPeriod._Periodunkid = CInt(lvED.SelectedItems(0).SubItems(colhPeriod.Index).Tag)
            objPeriod._Periodunkid(FinancialYear._Object._DatabaseName) = CInt(lvED.SelectedItems(0).SubItems(colhPeriod.Index).Tag)
            'Sohail (21 Aug 2015) -- End
            If objPeriod._Statusid = enStatusType.Close Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 19, "Sorry! You can not Edit/Delete this transaction. Reason: Period is closed."), enMsgBoxStyle.Information) '?1
                lvED.Select()
                Exit Sub
            End If
            'Sohail (28 Jan 2012) -- End

            If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 6, "Are you sure you want to delete this Earning Deduction?"), CType(enMsgBoxStyle.Question + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.Yes Then

                'Sandeep [ 16 Oct 2010 ] -- Start
                'objED.Void(CInt(lvED.SelectedItems(0).Tag), User._Object._Userunkid, Now, "") 'Sohail (14 Oct 2010)
                Dim frm As New frmReasonSelection
                Dim mstrVoidReason As String = String.Empty
                frm.displayDialog(enVoidCategoryType.PAYROLL, mstrVoidReason)
                If mstrVoidReason.Length <= 0 Then
                    Exit Sub
                End If
                With objED
                    ._FormName = mstrModuleName
                    ._LoginEmployeeUnkid = 0
                    ._ClientIP = getIP()
                    ._HostName = getHostName()
                    ._FromWeb = False
                    ._AuditUserId = User._Object._Userunkid
._CompanyUnkid = Company._Object._Companyunkid
                    ._AuditDate = ConfigParameter._Object._CurrentDateAndTime
                End With
                'Sohail (21 Aug 2015) -- Start
                'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                'objED.Void(CInt(lvED.SelectedItems(0).Tag), User._Object._Userunkid, ConfigParameter._Object._CurrentDateAndTime, mstrVoidReason)
                objED.Void(CInt(lvED.SelectedItems(0).Tag), User._Object._Userunkid, ConfigParameter._Object._CurrentDateAndTime, mstrVoidReason, FinancialYear._Object._DatabaseName)
                'Sohail (21 Aug 2015) -- End
                'Sandeep [ 16 Oct 2010 ] -- End 

                lvED.SelectedItems(0).Remove()

                If lvED.Items.Count <= 0 Then
                    Exit Try
                End If

                If lvED.Items.Count = intSelectedIndex Then
                    intSelectedIndex = lvED.Items.Count - 1
                    lvED.Items(intSelectedIndex).Selected = True
                    lvED.EnsureVisible(intSelectedIndex)
                ElseIf lvED.Items.Count <> 0 Then
                    lvED.Items(intSelectedIndex).Selected = True
                    lvED.EnsureVisible(intSelectedIndex)
                End If
            End If
            lvED.Select()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnDelete_Click", mstrModuleName)
        Finally
            objED = Nothing
            objTranHead = Nothing
        End Try
    End Sub

    Private Sub btnExempt_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnExempt.Click
        'Dim objFrm As New frmEmployee_Exemption_AddEdit
        Dim objTranHead As New clsTransactionHead
        Try
            'Sohail (26 Aug 2016) -- Start
            'Enhancement - 57.4 & 63.1 - Process Payroll Performance enhancement for TRA.
            'If lvEmployeeList.SelectedItems.Count = 0 Then
            If dgEmployee.SelectedRows.Count = 0 Then
                'Sohail (26 Aug 2016) -- End
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please select atleast one employee to process payroll."), enMsgBoxStyle.Information)
                Exit Try
            ElseIf CInt(cboPayYear.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Please select Pay Year. Pay Year is mandatory information."), enMsgBoxStyle.Information)
                cboPayYear.Focus()
                Exit Try
            ElseIf CInt(cboPayPeriod.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Please select Pay Period. Pay Period is mandatory information."), enMsgBoxStyle.Information)
                cboPayPeriod.Focus()
                Exit Try
            ElseIf lvED.SelectedItems.Count = 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 8, "Please select Transaction Head to Exempt."), enMsgBoxStyle.Information)
                Exit Try
            End If
            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'objTranHead._Tranheadunkid = CInt(lvED.Items(lvED.SelectedItems(0).Index).SubItems(colhTranHead.Index).Tag)
            objTranHead._Tranheadunkid(FinancialYear._Object._DatabaseName) = CInt(lvED.Items(lvED.SelectedItems(0).Index).SubItems(colhTranHead.Index).Tag)
            'Sohail (21 Aug 2015) -- End
            If objTranHead._Typeof_id = enTypeOf.Salary Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 9, "Sorry, You can not Exempt this Transaction Head. Reason: Type of Transaction Head is Salary."), enMsgBoxStyle.Information)
                Exit Try
            End If
            'If User._Object._RightToLeft = True Then
            '    frm.RightToLeft = Windows.Forms.RightToLeft.Yes
            '    frm.RightToLeftLayout = True
            '    Call Language.ctlRightToLeftlayOut(frm)
            'End If
            If lvED.Items(lvED.SelectedItems(0).Index).SubItems(colhExempted.Index).Text.ToUpper = "YES" Then
                Dim objFrm As New frmEmployee_Exemption_List
                'Sohail (26 Aug 2016) -- Start
                'Enhancement - 57.4 & 63.1 - Process Payroll Performance enhancement for TRA.
                'If objFrm.displayDialog(CInt(cboPayYear.SelectedValue), CInt(cboPayPeriod.SelectedValue), CInt(lvEmployeeList.Items(lvEmployeeList.SelectedItems(0).Index).Tag), CInt(lvED.Items(lvED.SelectedItems(0).Index).SubItems(colhTranHead.Index).Tag)) Then
                '    If lvEmployeeList.SelectedItems.Count = 0 Then Exit Sub
                '    Call FillEDList(CInt(lvEmployeeList.Items(lvEmployeeList.SelectedItems(0).Index).Tag))
                'End If
                If objFrm.displayDialog(CInt(cboPayYear.SelectedValue), CInt(cboPayPeriod.SelectedValue), CInt(dgEmployee.SelectedRows(0).Cells(objdgcolhEmployeeunkid.Index).Value), CInt(lvED.Items(lvED.SelectedItems(0).Index).SubItems(colhTranHead.Index).Tag)) Then
                    If dgEmployee.SelectedRows.Count = 0 Then Exit Sub
                    Call FillEDList(CInt(dgEmployee.SelectedRows(0).Cells(objdgcolhEmployeeunkid.Index).Value))
                End If
                'Sohail (26 Aug 2016) -- End
                objFrm = Nothing
            Else
                Dim objFrm As New frmEmployee_Exemption_AddEdit
                'Sohail (26 Aug 2016) -- Start
                'Enhancement - 57.4 & 63.1 - Process Payroll Performance enhancement for TRA.
                'If objFrm.displayDialog(-1, enAction.ADD_CONTINUE, CInt(cboPayYear.SelectedValue), CInt(cboPayPeriod.SelectedValue), CInt(lvEmployeeList.Items(lvEmployeeList.SelectedItems(0).Index).Tag), CInt(lvED.Items(lvED.SelectedItems(0).Index).SubItems(colhTranHead.Index).Tag)) Then
                '    If lvEmployeeList.SelectedItems.Count = 0 Then Exit Sub
                '    Call FillEDList(CInt(lvEmployeeList.Items(lvEmployeeList.SelectedItems(0).Index).Tag))
                'End If
                If objFrm.displayDialog(-1, enAction.ADD_CONTINUE, CInt(cboPayYear.SelectedValue), CInt(cboPayPeriod.SelectedValue), CInt(dgEmployee.SelectedRows(0).Cells(objdgcolhEmployeeunkid.Index).Value), CInt(lvED.Items(lvED.SelectedItems(0).Index).SubItems(colhTranHead.Index).Tag)) Then
                    If dgEmployee.SelectedRows.Count = 0 Then Exit Sub
                    Call FillEDList(CInt(dgEmployee.SelectedRows(0).Cells(objdgcolhEmployeeunkid.Index).Value))
                End If
                'Sohail (26 Aug 2016) -- End
                objFrm = Nothing
            End If

        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "btnExempt_Click", mstrModuleName)
        Finally
            objTranHead = Nothing
        End Try
    End Sub

    'Sohail (11 Sep 2010) -- Start
    Private Sub btnCostCenter_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCostCenter.Click
        Dim objEmpCostCenter As New clsemployee_costcenter_Tran
        Dim dsList As DataSet
        Try
            'Sohail (26 Aug 2016) -- Start
            'Enhancement - 57.4 & 63.1 - Process Payroll Performance enhancement for TRA.
            'If lvEmployeeList.SelectedItems.Count = 0 Then
            If dgEmployee.SelectedRows.Count = 0 Then
                'Sohail (26 Aug 2016) -- End
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please select atleast one employee to process payroll."), enMsgBoxStyle.Information)
                Exit Try
            ElseIf lvED.SelectedItems.Count = 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 10, "Please select Transaction Head to change Cost Center."), enMsgBoxStyle.Information)
                Exit Try
            End If

            'Sohail (29 Mar 2017) -- Start
            'CCBRT Enhancement - 66.1 - Employee cost center distribution for non-salary heads, Effective period option on Employee Cost Center.
            'Dim objFrm As New frmEmployeeCostCenter
            Dim objFrm As New frmEmpDistributedCostCenter_AddEdit
            'Sohail (29 Mar 2017) -- End
            'Sohail (26 Aug 2016) -- Start
            'Enhancement - 57.4 & 63.1 - Process Payroll Performance enhancement for TRA.
            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'dsList = objEmpCostCenter.GetList("CostCenter", True, CInt(lvEmployeeList.Items(lvEmployeeList.SelectedItems(0).Index).Tag), CInt(lvED.Items(lvED.SelectedItems(0).Index).SubItems(colhTranHead.Index).Tag))
            'dsList = objEmpCostCenter.GetList(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, mdtPayPeriodStartDate, mdtPayPeriodEndDate, ConfigParameter._Object._UserAccessModeSetting, True, ConfigParameter._Object._IsIncludeInactiveEmp, "CostCenter", True, CInt(lvEmployeeList.Items(lvEmployeeList.SelectedItems(0).Index).Tag), CInt(lvED.Items(lvED.SelectedItems(0).Index).SubItems(colhTranHead.Index).Tag))
            'dsList = objEmpCostCenter.GetList(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, mdtPayPeriodStartDate, mdtPayPeriodEndDate, ConfigParameter._Object._UserAccessModeSetting, True, ConfigParameter._Object._IsIncludeInactiveEmp, "CostCenter", True, CInt(dgEmployee.SelectedRows(0).Cells(objdgcolhEmployeeunkid.Index).Value), CInt(lvED.Items(lvED.SelectedItems(0).Index).SubItems(colhTranHead.Index).Tag))
            'Sohail (26 Aug 2016) -- End
            'Sohail (21 Aug 2015) -- End
            'Sohail (26 Aug 2016) -- Start
            'Enhancement - 57.4 & 63.1 - Process Payroll Performance enhancement for TRA.
            'If dsList.Tables("CostCenter").Rows.Count > 0 Then
            '    objFrm.displayDialog(-1, enAction.ADD_CONTINUE, CInt(lvEmployeeList.Items(lvEmployeeList.SelectedItems(0).Index).Tag), CInt(lvED.Items(lvED.SelectedItems(0).Index).SubItems(colhTranHead.Index).Tag), CInt(lvED.Items(lvED.SelectedItems(0).Index).SubItems(colhCostCenter.Index).Tag))
            'Else
            '    objFrm.displayDialog(-1, enAction.ADD_CONTINUE, CInt(lvEmployeeList.Items(lvEmployeeList.SelectedItems(0).Index).Tag), CInt(lvED.Items(lvED.SelectedItems(0).Index).SubItems(colhTranHead.Index).Tag))
            'End If
            'If lvEmployeeList.SelectedItems.Count = 0 Then Exit Sub
            'Call FillEDList(CInt(lvEmployeeList.Items(lvEmployeeList.SelectedItems(0).Index).Tag))
            'Sohail (29 Mar 2017) -- Start
            'CCBRT Enhancement - 66.1 - Employee cost center distribution for non-salary heads, Effective period option on Employee Cost Center.
            'dsList = objEmpCostCenter.GetList(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, mdtPayPeriodStartDate, mdtPayPeriodEndDate, ConfigParameter._Object._UserAccessModeSetting, True, ConfigParameter._Object._IsIncludeInactiveEmp, "CostCenter", True, CInt(dgEmployee.SelectedRows(0).Cells(objdgcolhEmployeeunkid.Index).Value), CInt(lvED.Items(lvED.SelectedItems(0).Index).SubItems(colhTranHead.Index).Tag))
            'If dsList.Tables("CostCenter").Rows.Count > 0 Then
            '    objFrm.displayDialog(-1, enAction.ADD_CONTINUE, CInt(dgEmployee.SelectedRows(0).Cells(objdgcolhEmployeeunkid.Index).Value), CInt(lvED.Items(lvED.SelectedItems(0).Index).SubItems(colhTranHead.Index).Tag), CInt(lvED.Items(lvED.SelectedItems(0).Index).SubItems(colhCostCenter.Index).Tag))
            'Else
            '    objFrm.displayDialog(-1, enAction.ADD_CONTINUE, CInt(dgEmployee.SelectedRows(0).Cells(objdgcolhEmployeeunkid.Index).Value), CInt(lvED.Items(lvED.SelectedItems(0).Index).SubItems(colhTranHead.Index).Tag))
            'End If
            dsList = objEmpCostCenter.GetList(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, mdtPayPeriodStartDate, mdtPayPeriodEndDate, ConfigParameter._Object._UserAccessModeSetting, True, ConfigParameter._Object._IsIncludeInactiveEmp, "CostCenter", True, CInt(dgEmployee.SelectedRows(0).Cells(objdgcolhEmployeeunkid.Index).Value), CInt(lvED.Items(lvED.SelectedItems(0).Index).SubItems(colhTranHead.Index).Tag), , mdtPayPeriodEndDate)
            If dsList.Tables("CostCenter").Rows.Count > 0 Then
                objFrm.displayDialog(-1, enAction.ADD_CONTINUE, CInt(dgEmployee.SelectedRows(0).Cells(objdgcolhEmployeeunkid.Index).Value), CInt(lvED.Items(lvED.SelectedItems(0).Index).SubItems(colhTranHead.Index).Tag), CInt(lvED.Items(lvED.SelectedItems(0).Index).SubItems(colhCostCenter.Index).Tag))
            Else
                objFrm.displayDialog(-1, enAction.ADD_CONTINUE, CInt(dgEmployee.SelectedRows(0).Cells(objdgcolhEmployeeunkid.Index).Value), CInt(lvED.Items(lvED.SelectedItems(0).Index).SubItems(colhTranHead.Index).Tag))
            End If
            'Sohail (29 Mar 2017) -- End
            If dgEmployee.SelectedRows.Count = 0 Then Exit Sub
            Call FillEDList(CInt(dgEmployee.SelectedRows(0).Cells(objdgcolhEmployeeunkid.Index).Value))
            'Sohail (26 Aug 2016) -- End
            objFrm = Nothing

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnCostCenter_Click", mstrModuleName)
        End Try
    End Sub
    'Sohail (11 Sep 2010) -- End

    Private Sub btnProcess_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnProcess.Click
        Dim objLoginTran As New clslogin_Tran
        Dim objMaster As New clsMasterData
        Dim dsList As DataSet
        Dim dtTable As DataTable
        Dim strEmpList As String = ""
        Dim blnFlag As Boolean = False
        Dim strMsg As String = ""
        Dim strEmpVoidList As String = "" 'Sohail (04 Mar 2011)
        Dim objED As New clsEarningDeduction 'Sohail (16 May 2012)
        Dim objSalary As New clsSalaryIncrement 'Sohail (24 Sep 2012)
        Dim mdtTnAStartDate As Date 'Sohail (07 Jan 2014)
        Try

            'Sohail (18 Nov 2016) -- Start
            'Enhancement -  64.1 - Insert Membership heads on Ed if not assigned.
            If dvEmployee Is Nothing Then Exit Try
            'Sohail (18 Nov 2016) -- End

            'Sohail (26 Aug 2016) -- Start
            'Enhancement - 57.4 & 63.1 - Process Payroll Performance enhancement for TRA.
            'mintCheckedEmployee = lvEmployeeList.CheckedItems.Count
            mintCheckedEmployee = dvEmployee.Table.Select("IsChecked = 1 ").Length
            'Sohail (26 Aug 2016) -- End
            If mintCheckedEmployee = 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please select atleast one employee to process payroll."), enMsgBoxStyle.Information) 'Sohail (28 Dec 2010)
                Exit Sub
            ElseIf CInt(cboPayYear.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Please select Pay Year. Pay Year is mandatory information."), enMsgBoxStyle.Information) 'Sohail (28 Dec 2010)
                cboPayYear.Focus()
                Exit Sub
            ElseIf CInt(cboPayPeriod.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Please select Pay Period. Pay Period is mandatory information."), enMsgBoxStyle.Information) 'Sohail (28 Dec 2010)
                cboPayPeriod.Focus()
                Exit Sub
            End If

            'Sohail (20 Feb 2012) -- Start
            'TRA - ENHANCEMENT
            Me.SuspendLayout()
            RemoveHandler gfrmMDI.tmrReminder.Tick, AddressOf gfrmMDI.tmrReminder_Tick
            'Sohail (20 Feb 2012) -- End

            'Sohail (11 Sep 2010) -- Start
            '***Checking for previous year status (open or close)
            dsList = objMaster.Get_Database_Year_List("Year", False, Company._Object._Companyunkid)
            dtTable = New DataView(dsList.Tables("Year"), "end_date < '" & eZeeDate.convertDate(FinancialYear._Object._Database_End_Date) & "'", "", DataViewRowState.CurrentRows).ToTable
            If dtTable.Rows.Count > 0 Then
                strMsg = Language.getMessage(mstrModuleName, 11, "Sorry, You cannot do process payroll at this time. Reason : Previous year [") & dtTable.Rows(0).Item("financialyear_name").ToString & Language.getMessage(mstrModuleName, 12, "] is still open. Please close previous year first.")
                eZeeMsgBox.Show(strMsg, enMsgBoxStyle.Information) 'Sohail (28 Dec 2010)
                Exit Try
            End If
            'Sohail (11 Sep 2010) -- End

            '*** Checking Previous Period Status ***
            Dim objPeriod As New clscommom_period_Tran
            'Sohail (07 Jan 2014) -- Start
            'Enhancement - Separate TnA Periods from Payroll Periods
            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'objPeriod._Periodunkid = CInt(cboPayPeriod.SelectedValue)
            objPeriod._Periodunkid(FinancialYear._Object._DatabaseName) = CInt(cboPayPeriod.SelectedValue)
            'Sohail (21 Aug 2015) -- End
            mdtTnAStartDate = objPeriod._TnA_StartDate
            'Sohail (07 Jan 2014) -- End
            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'dsList = objPeriod.GetList("Period", enModuleReference.Payroll, True, enStatusType.Open)
            dsList = objPeriod.GetList("Period", enModuleReference.Payroll, FinancialYear._Object._YearUnkid, FinancialYear._Object._Database_Start_Date, True, enStatusType.Open)
            'Sohail (21 Aug 2015) -- End
            dtTable = New DataView(dsList.Tables("Period"), "end_date < '" & eZeeDate.convertDate(mdtPayPeriodEndDate) & "'", "end_date", DataViewRowState.CurrentRows).ToTable
            objPeriod = Nothing

            If dtTable.Rows.Count > 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 14, "Sorry, Previous Period [") & dtTable.Rows(0).Item("period_name").ToString & Language.getMessage(mstrModuleName, 15, "] is still Open. Please Close Previous Period first."), enMsgBoxStyle.Information)
                Exit Try
            End If

            'Sohail (29 Mar 2013) -- Start
            'TRA - ENHANCEMENT
            'Sohail (07 Sep 2013) -- Start
            'TRA - ENHANCEMENT
            'For Each lvItem As ListViewItem In lvEmployeeList.CheckedItems
            '    If strEmpList.Length <= 0 Then
            '        strEmpList &= CInt(lvItem.Tag)
            '    Else
            '        strEmpList &= "," & CInt(lvItem.Tag)
            '    End If
            'Next
            'Sohail (26 Aug 2016) -- Start
            'Enhancement - 57.4 & 63.1 - Process Payroll Performance enhancement for TRA.
            'Dim allEmp As List(Of String) = (From lv In lvEmployeeList.CheckedItems.Cast(Of ListViewItem)() Select (lv.Tag.ToString)).ToList
            Dim allEmp As List(Of String) = (From p In dvEmployee.Table Where (CBool(p.Item("IsChecked")) = True) Select (p.Item("employeeunkid").ToString)).ToList
            'Sohail (26 Aug 2016) -- End
            strEmpList = String.Join(",", allEmp.ToArray)
            'Sohail (07 Sep 2013) -- End
            'Sohail (29 Mar 2013) -- End

            'Sohail (16 May 2012) -- Start
            'TRA - ENHANCEMENT
            '*** Check for any pending (not approved) transaction head on ED for selected period.
            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'dsList = objED.GetList("ED", strEmpList, , , , , , , , , , , , , enEDHeadApprovalStatus.Pending, , , , mdtPayPeriodEndDate) 'Sohail (30 Mar 2013) - [strEmpList]
            'Sohail (12 Oct 2021) -- Start
            'NMB Issue :  : Performance issue on process payroll.
            'dsList = objED.GetList(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, mdtPayPeriodStartDate, mdtPayPeriodEndDate, ConfigParameter._Object._UserAccessModeSetting, True, ConfigParameter._Object._IsIncludeInactiveEmp, "ED", False, strEmpList, , , enEDHeadApprovalStatus.Pending, , , mdtPayPeriodEndDate)
            dsList = objED.GetList(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, mdtPayPeriodStartDate, mdtPayPeriodEndDate, ConfigParameter._Object._UserAccessModeSetting, True, ConfigParameter._Object._IsIncludeInactiveEmp, "ED", False, strEmpList, , , enEDHeadApprovalStatus.Pending, , , mdtPayPeriodEndDate, , , , , False)
            'Sohail (12 Oct 2021) -- End
            'Sohail (21 Aug 2015) -- End
            If dsList.Tables("ED").Rows.Count > 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 20, "Sorry, Some Transaction Heads are in Pending status (not Approved) on Earning Deduction for Selected Period. To perform payroll process, please Approve those heads."), enMsgBoxStyle.Information)
                Exit Try
            End If
            'Sohail (16 May 2012) -- End

            'Sohail (25 Jun 2020) -- Start
            'NMB Enhancement # : Performance enhancement on Import earning deduction heads.
            'Sohail (12 Oct 2021) -- Start
            'NMB Issue :  : Performance issue on process payroll.
            'If objED.IsExist_UnAssigned_MembershipHeads(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, mdtPayPeriodStartDate, mdtPayPeriodEndDate, ConfigParameter._Object._UserAccessModeSetting, True, False, True, CInt(cboPayPeriod.SelectedValue), Nothing, "", strEmpList, 0, False) = True Then
            If objED.IsExist_UnAssigned_MembershipHeads(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, mdtPayPeriodStartDate, mdtPayPeriodEndDate, ConfigParameter._Object._UserAccessModeSetting, True, False, False, CInt(cboPayPeriod.SelectedValue), Nothing, "", strEmpList, 0, False) = True Then
                'Sohail (12 Oct 2021) -- End
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 40, "Sorry, Some active membership heads are not assigned on Earning Deductions. Please assign them from Earning Deduction List -> Operation -> Assign membership heads."), enMsgBoxStyle.Information)
                Exit Try
            End If

            Dim objTranHead As New clsTransactionHead
            dsList = objTranHead.GetList("List", -1, -1, False, True, enTranHeadActiveInActive.ACTIVE, False, " calctype_id IN (" & enCalcType.AsComputedValue & ", " & enCalcType.AsComputedOnWithINEXCESSOFTaxSlab & ")", True, True, True, 2, Nothing, mdtPayPeriodEndDate, " AND A.formulaid = '' ")
            If dsList.Tables(0).Rows.Count > 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 41, "Formula is not set on the Transaction Head. Please set formula from Transaction Head screen for") & " " & dsList.Tables(0).Rows(0).Item("trnheadname").ToString, enMsgBoxStyle.Information)
                Exit Try
            End If
            'Sohail (25 Jun 2020) -- End

            'Sohail (24 Sep 2012) -- Start
            'TRA - ENHANCEMENT
            '*** Check for any pending (not approved) Salary Increment for selected period.
            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'dsList = objSalary.GetList("Salary", strEmpList, , CInt(cboPayPeriod.SelectedValue), , , , , , , enSalaryChangeApprovalStatus.Pending) 'Sohail (30 Mar 2013) - [strEmpList]
            'Sohail (12 Oct 2021) -- Start
            'NMB Issue :  : Performance issue on process payroll.
            'dsList = objSalary.GetList(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, mdtPayPeriodStartDate, mdtPayPeriodEndDate, ConfigParameter._Object._UserAccessModeSetting, True, True, False, "Salary", Nothing, strEmpList, CInt(cboPayPeriod.SelectedValue), 0, , , enSalaryChangeApprovalStatus.Pending)
            dsList = objSalary.GetList(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, mdtPayPeriodStartDate, mdtPayPeriodEndDate, ConfigParameter._Object._UserAccessModeSetting, True, True, False, "Salary", Nothing, strEmpList, CInt(cboPayPeriod.SelectedValue), 0, , , enSalaryChangeApprovalStatus.Pending, , False)
            'Sohail (12 Oct 2021) -- End
            'Sohail (21 Aug 2015) -- End
            If dsList.Tables("Salary").Rows.Count > 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 21, "Sorry, Some Salary Changes are in Pending status (not Approved) on Salary Change for Selected Period. To perform payroll process, please Approve those Salary Changes."), enMsgBoxStyle.Information)
                Exit Try
            End If
            'Sohail (24 Sep 2012) -- End

            'Hemant (31 July 2018) -- Start
            'Enhancement : Support Issue Id # 2410 - Changes to stop Payroll process of employees who had payment was already done 
            Dim objPayment As New clsPayment_tran
            'Sohail (12 Oct 2021) -- Start
            'NMB Issue :  : Performance issue on process payroll.
            'Dim dsEmployeeList As DataSet = objPayment.GetListByPeriod(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, mdtPayPeriodStartDate, mdtPayPeriodEndDate, ConfigParameter._Object._UserAccessModeSetting, True, ConfigParameter._Object._IsIncludeInactiveEmp, "Payment", clsPayment_tran.enPaymentRefId.PAYSLIP, CInt(cboPayPeriod.SelectedValue), clsPayment_tran.enPayTypeId.PAYMENT, strEmpList, True)
            Dim dsEmployeeList As DataSet = objPayment.GetListByPeriod(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, mdtPayPeriodStartDate, mdtPayPeriodEndDate, ConfigParameter._Object._UserAccessModeSetting, True, ConfigParameter._Object._IsIncludeInactiveEmp, "Payment", clsPayment_tran.enPaymentRefId.PAYSLIP, CInt(cboPayPeriod.SelectedValue), clsPayment_tran.enPayTypeId.PAYMENT, strEmpList, False, , False)
            'Sohail (12 Oct 2021) -- End

            If dsEmployeeList.Tables(0).Rows.Count > 0 AndAlso chkExcludeWithPayment.Visible = False Then

                dsEmployeeList.Tables(0).Columns("empname").SetOrdinal(0)
                dsEmployeeList.Tables(0).Columns("employeecode").SetOrdinal(1)
                dsEmployeeList.Tables(0).Columns("empname").ColumnName = "Employee Name"
                dsEmployeeList.Tables(0).Columns("employeecode").ColumnName = "Employee Code"
                For i As Integer = 2 To dsEmployeeList.Tables(0).Columns.Count - 1
                    dsEmployeeList.Tables(0).Columns.RemoveAt(2)
                Next
                Dim objValid As New frmCommonValidationList
                objValid.displayDialog(False, Language.getMessage(mstrModuleName, 28, "Sorry, Payment of some of the employees is already done for this period."), dsEmployeeList.Tables(0))
                Exit Try

            End If
            'Hemant (31 July 2018) -- End

            'Sohail (27 Apr 2016) -- Start
            'Enhancement - 61.1 - Priority Order on Grade Group, Grade and Grade Level; Auto Salary Increment to next Grade Level as per Priority Order; Grade filter on Wages Table; Export and Import for Wages Table for KBC.
            '#23 (KBC & Kenya Project Comments List.xls) - Auto Salary incrment Per employee Anniversary,Employee Anniversay,All KBC Employees have Annivesary date for Salary increment,date is sent  when employee is confimed ,the salary is done automatically by the system when annivesary is due from current level to next level and stops at the Last level (notch),unless changed to higher Grade
            dsList = objSalary.GetPendingEmployeeList(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, mdtPayPeriodStartDate, mdtPayPeriodEndDate, ConfigParameter._Object._UserAccessModeSetting, True, True, "AutoSalary", mdtPayPeriodEndDate, False, "")
            If dsList.Tables("AutoSalary").Rows.Count > 0 Then
                'Sohail (23 May 2017) -- Start
                'Enhancement - 67.1 - Link budget with Payroll.
                'If (eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 28, "Sorry, Auto Salary Increment is not done for some of the employees for this period.") & " [" & dsList.Tables("AutoSalary").Rows(0).Item("employeecode").ToString & " : " & dsList.Tables("AutoSalary").Rows(0).Item("employeename").ToString & "]" & vbCrLf & vbCrLf & _
                '                Language.getMessage(mstrModuleName, 29, "Do you want to proceed?"), CType(enMsgBoxStyle.Information + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.No) Then Exit Try
                dsList.Tables("AutoSalary").Columns("employeename").SetOrdinal(0)
                dsList.Tables("AutoSalary").Columns("employeecode").SetOrdinal(1)
                dsList.Tables("AutoSalary").Columns("employeename").ColumnName = "Employee Name"
                dsList.Tables("AutoSalary").Columns("employeecode").ColumnName = "Employee Code"
                For i As Integer = 2 To dsList.Tables("AutoSalary").Columns.Count - 1
                    dsList.Tables("AutoSalary").Columns.RemoveAt(2)
                Next
                Dim objValid As New frmCommonValidationList
                If objValid.displayDialog(True, Language.getMessage(mstrModuleName, 39, "Sorry, Auto Salary Increment is not done for some of the employees for this period.") & vbCrLf & "  " & Language.getMessage(mstrModuleName, 29, "Do you want to proceed?"), dsList.Tables("AutoSalary")) = False Then Exit Try
                'Sohail (23 May 2017) -- End

            End If
            'Sohail (27 Apr 2016) -- End


            'Pinkal (15-Sep-2013) -- Start
            'Enhancement : TRA Changes

            '***Checking for Pending or Approved status of leave form in given open period 
            If ConfigParameter._Object._IsArutiDemo = True OrElse ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Leave_Management) Then
                If ConfigParameter._Object._ClosePayrollPeriodIfLeaveIssue Then
                    If mdtPayPeriodEndDate.Date = dtpAsOnDate.Value.Date Then
                        Dim objLeaveForm As New clsleaveform
                        dsList = objLeaveForm.GetPendingLeaveFormForPeriod(mdtPayPeriodStartDate, mdtPayPeriodEndDate, False, strEmpList)
                        If dsList.Tables(0).Rows.Count > 0 Then
                            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 27, "Some Employees leave form Start Date falls in this Period and their leave form(s) are in pending or approved status.Please issue or reject or reschedule their leave form."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                            Exit Try
                        End If
                    End If
                End If
            End If

            'Pinkal (15-Sep-2013) -- End



            '*** Checking for Absent Process by checking absent count in the period ***
            'Sohail (07 Jan 2014) -- Start
            'Enhancement - Separate TnA Periods from Payroll Periods
            'If objLoginTran.Is_Absent_Processed(mdtPayPeriodStartDate, mdtPayPeriodEndDate) = False Then
            'Sohail (02 Sep 2020) -- Start
            'NMB Enhancement # : Allow to do process payroll if Absent process is not done for NMB PLC only.
            'If objLoginTran.Is_Absent_Processed(mdtTnAStartDate, CDate(objlblTnAEndDate.Tag)) = False Then
            Dim objCGroup As New clsGroup_Master
            objCGroup._Groupunkid = 1
            If objLoginTran.Is_Absent_Processed(mdtTnAStartDate, CDate(objlblTnAEndDate.Tag)) = False AndAlso objCGroup._Groupname.ToUpper <> "NMB PLC" Then
                'Sohail (02 Sep 2020) -- End
                'Sohail (07 Jan 2014) -- End
                'strMsg = "Absent Process is not done yet for this period. If you do process payroll, you will not get proper result. " & _
                '"It is recommended that first do Absent process for this period before doing process payroll for this period." & vbCrLf & vbCrLf & _
                '"Do you stiil want to process payroll for this period?"
                strMsg = Language.getMessage(mstrModuleName, 13, "Sorry, Absent Process is not done yet for this period.It is recommended that first perform Absent process for this period from Time and Attendance Module.")

                'If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, strMsg), CType(enMsgBoxStyle.Question + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.No Then
                '    Exit Try
                'End If
                eZeeMsgBox.Show(strMsg, enMsgBoxStyle.Information) 'Sohail (28 Dec 2010)
                Exit Try
            End If


            'Sohail (04 Apr 2011) -- Start
            '*** Checking any loan / advance payment is pending for current period
            Dim objLoan As New clsLoan_Advance
            'Sohail (15 Dec 2015) -- Start
            'Enhancement - New Loan Calculation type Reducing balance with Fixed Pricipal EMI and new interest calculation Monthly apart from Daily for KBC.
            'If objLoan.Is_Payment_Done(CInt(cboPayPeriod.SelectedValue)) = False Then
            If objLoan.Is_Payment_Done(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, mdtPayPeriodStartDate, mdtPayPeriodEndDate, ConfigParameter._Object._UserAccessModeSetting, True, False, True, CInt(cboPayPeriod.SelectedValue)) = False Then
                'Sohail (15 Dec 2015) -- End
                strMsg = Language.getMessage(mstrModuleName, 16, "Payment is not done for some Loan/Advance whose deduction is going to start from this month. If you don't make payment, Loan/Advance deduction will not start.") & vbCrLf & vbCrLf & _
                        Language.getMessage(mstrModuleName, 17, "Do you want to proceed?")

                If eZeeMsgBox.Show(strMsg, CType(enMsgBoxStyle.Question + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.No Then
                    objLoan = Nothing
                    Exit Try
                End If
            End If
            objLoan = Nothing
            'Sohail (04 Apr 2011) -- End

            'Sohail (26 Aug 2016) -- Start
            'Enhancement - 57.4 & 63.1 - Process Payroll Performance enhancement for TRA.
            'If lvEmployeeList.Items.Count <> lvEmployeeList.CheckedItems.Count Then
            If dgEmployee.RowCount <> mintCheckedEmployee Then
                'Sohail (26 Aug 2016) -- End
                strMsg = Language.getMessage(mstrModuleName, 18, "You have not selected all employees from the list. It is recommended that you select all employees to process payroll.") & vbCrLf & vbCrLf & _
                        Language.getMessage(mstrModuleName, 17, "Do you want to proceed?")

                If eZeeMsgBox.Show(strMsg, CType(enMsgBoxStyle.Question + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.No Then
                    Exit Try
                End If
            End If
            Cursor.Current = Cursors.WaitCursor 'Sohail (01 Dec 2010)

            'Sohail (22 Aug 2012) -- Start
            'TRA - ENHANCEMENT
            'For Each lvItem As ListViewItem In lvEmployeeList.CheckedItems
            '    'For i As Integer = 0 To lvEmployeeList.CheckedItems.Count - 1 'Sohail (04 Mar 2011)
            '    If strEmpList.Length <= 0 Then
            '        'Sohail (04 Mar 2011) -- Start
            '        'strEmpList &= CInt(lvEmployeeList.CheckedItems(i).Tag)
            '        strEmpList &= CInt(lvItem.Tag)
            '        'Sohail (04 Mar 2011) -- End
            '    Else
            '        'Sohail (04 Mar 2011) -- Start
            '        'strEmpList &= "," & CInt(lvEmployeeList.CheckedItems(i).Tag)
            '        strEmpList &= "," & CInt(lvItem.Tag)
            '        'Sohail (04 Mar 2011) -- End
            '    End If
            '    'Sohail (04 Mar 2011) -- Start
            '    If lvItem.SubItems(colhIsPaymentDone.Index).Text.ToUpper <> "YES" Then
            '        If strEmpVoidList.Length <= 0 Then
            '            strEmpVoidList &= CInt(lvItem.Tag)
            '        Else
            '            strEmpVoidList &= "," & CInt(lvItem.Tag)
            '        End If
            '    End If
            '    'Sohail (04 Mar 2011) -- End
            'Next

            ''Sohail (14 Oct 2010) -- Start
            'objTnALeave._Userunkid = User._Object._Userunkid
            'objTnALeave._Voiduserunkid = User._Object._Userunkid
            ''Sohail (14 Oct 2010) -- End
            ''Sohail (04 Mar 2011) -- Start
            ''blnFlag = objTnALeave.InsertDelete(strEmpList, CInt(cboPayPeriod.SelectedValue))
            'objTnALeave._Processdate = dtpAsOnDate.Value
            ''Sohail (22 Aug 2012) -- Start
            ''TRA - ENHANCEMENT
            'blnFlag = objTnALeave.InsertDelete(strEmpList, strEmpVoidList, CInt(cboPayPeriod.SelectedValue))
            ''Sohail (22 Aug 2012) -- End
            ''Sohail (04 Mar 2011) -- End

            'Cursor.Current = Cursors.Default 'Sohail (01 Dec 2010)

            'If blnFlag = True Then
            '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Payroll Process completed successfully."), enMsgBoxStyle.Information)
            '    objchkSelectAll.Checked = False
            '    cboPayPeriod.SelectedValue = 0 'Sohail (12 Jul 2012)
            'End If

            objbgwProcessPayroll.RunWorkerAsync()
            'Sohail (22 Aug 2012) -- End
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnProcess_Click", mstrModuleName)
            'Sohail (27 Aug 2012) -- Start
            'TRA - ENHANCEMENT
            RemoveHandler gfrmMDI.tmrReminder.Tick, AddressOf gfrmMDI.tmrReminder_Tick
            AddHandler gfrmMDI.tmrReminder.Tick, AddressOf gfrmMDI.tmrReminder_Tick
            'Sohail (27 Aug 2012) -- End
        Finally
            objLoginTran = Nothing
            objED = Nothing 'Sohail (16 May 2012)
            objSalary = Nothing 'Sohail (24 Sep 2012)
            'Sohail (20 Feb 2012) -- Start
            'TRA - ENHANCEMENT
            Me.ResumeLayout()
            'Sohail (22 Aug 2012) -- Start
            'TRA - ENHANCEMENT
            'RemoveHandler gfrmMDI.tmrReminder.Tick, AddressOf gfrmMDI.tmrReminder_Tick
            'AddHandler gfrmMDI.tmrReminder.Tick, AddressOf gfrmMDI.tmrReminder_Tick
            'Sohail (22 Aug 2012) -- End
            'Sohail (20 Feb 2012) -- End
        End Try
    End Sub

    'Sohail (22 May 2012) -- Start
    'TRA - ENHANCEMENT
    Private Sub btnVoidPayroll_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnVoidPayroll.Click
        Dim objMaster As New clsMasterData
        Dim dsList As DataSet
        Dim dtTable As DataTable
        Dim strMsg As String = ""
        Dim strEmpList As String = ""

        Try
            'Sohail (04 Jun 2018) -- Start
            'Internal Issue : Object reference error on Void Payroll click if employee list is not generated in 72.1.
            If dvEmployee Is Nothing Then Exit Try
            'Sohail (04 Jun 2018) -- End
            'Sohail (26 Aug 2016) -- Start
            'Enhancement - 57.4 & 63.1 - Process Payroll Performance enhancement for TRA.
            'If lvEmployeeList.CheckedItems.Count = 0 Then
            'Sohail (01 Mar 2017) -- Start
            'Enhancement #24 -  65.1 - Export and Import option on Budget Codes.
            'If dgEmployee.SelectedRows.Count = 0 Then
            If dvEmployee.Table.Select("IsChecked = 1 ").Length <= 0 Then
                'Sohail (01 Mar 2017) -- End
                'Sohail (26 Aug 2016) -- End
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 23, "Please select atleast one employee to Void Payroll."), enMsgBoxStyle.Information)
                Exit Sub
            ElseIf CInt(cboPayYear.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Please select Pay Year. Pay Year is mandatory information."), enMsgBoxStyle.Information)
                cboPayYear.Focus()
                Exit Sub
            ElseIf CInt(cboPayPeriod.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Please select Pay Period. Pay Period is mandatory information."), enMsgBoxStyle.Information)
                cboPayPeriod.Focus()
                Exit Sub
            End If

            'Sohail (24 Aug 2012) -- Start
            'TRA - ENHANCEMENT - Do not allow to Void Payroll if Payment is Done.
            'Sohail (26 Aug 2016) -- Start
            'Enhancement - 57.4 & 63.1 - Process Payroll Performance enhancement for TRA.
            'For Each lvItem As ListViewItem In lvEmployeeList.CheckedItems
            '    If lvItem.ForeColor = Color.Blue Then
            '        strMsg = Language.getMessage(mstrModuleName, 22, "Sorry, You can not do Void Payroll for some of employees. Reason : Payment is already done for some of the employees.")
            '        eZeeMsgBox.Show(strMsg, enMsgBoxStyle.Information)
            '        Exit Try
            '    End If
            'Next
            If (From p In dvEmployee.Table Where (CBool(p.Item("IsChecked")) = True AndAlso CBool(p.Item("IsPaymentDone")) = True) Select (p)).Count() > 0 Then
                strMsg = Language.getMessage(mstrModuleName, 22, "Sorry, You can not do Void Payroll for some of employees. Reason : Payment is already done for some of the employees.")
                eZeeMsgBox.Show(strMsg, enMsgBoxStyle.Information)
                Exit Try
            End If
            'Sohail (26 Aug 2016) -- End
            'Sohail (24 Aug 2012) -- End

            Me.SuspendLayout()
            RemoveHandler gfrmMDI.tmrReminder.Tick, AddressOf gfrmMDI.tmrReminder_Tick

            '***Checking for previous year status (open or close)
            dsList = objMaster.Get_Database_Year_List("Year", False, Company._Object._Companyunkid)
            dtTable = New DataView(dsList.Tables("Year"), "end_date < '" & eZeeDate.convertDate(FinancialYear._Object._Database_End_Date) & "'", "", DataViewRowState.CurrentRows).ToTable
            If dtTable.Rows.Count > 0 Then
                strMsg = Language.getMessage(mstrModuleName, 24, "Sorry, You can not do Void Payroll at this time. Reason : Previous year [") & dtTable.Rows(0).Item("financialyear_name").ToString & Language.getMessage(mstrModuleName, 12, "] is still open. Please close previous year first.")
                eZeeMsgBox.Show(strMsg, enMsgBoxStyle.Information)
                Exit Try
            End If

            '*** Checking Previous Period Status ***
            Dim objPeriod As New clscommom_period_Tran
            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'dsList = objPeriod.GetList("Period", enModuleReference.Payroll, True, enStatusType.Open)
            dsList = objPeriod.GetList("Period", enModuleReference.Payroll, FinancialYear._Object._YearUnkid, FinancialYear._Object._Database_Start_Date, True, enStatusType.Open)
            'Sohail (21 Aug 2015) -- End
            dtTable = New DataView(dsList.Tables("Period"), "end_date < '" & eZeeDate.convertDate(mdtPayPeriodEndDate) & "'", "end_date", DataViewRowState.CurrentRows).ToTable
            objPeriod = Nothing

            If dtTable.Rows.Count > 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 14, "Sorry, Previous Period [") & dtTable.Rows(0).Item("period_name").ToString & Language.getMessage(mstrModuleName, 15, "] is still Open. Please Close Previous Period first."), enMsgBoxStyle.Information)
                Exit Try
            End If

            If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 26, "Are you sure you want to Void Payroll for selected Employees ?"), CType(enMsgBoxStyle.Question + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.No Then
                Exit Try
            End If

            Cursor.Current = Cursors.WaitCursor

            'Sohail (07 Sep 2013) -- Start
            'TRA - ENHANCEMENT
            'For Each lvItem As ListViewItem In lvEmployeeList.CheckedItems
            '    If strEmpList.Length <= 0 Then
            '        strEmpList &= CInt(lvItem.Tag)
            '    Else
            '        strEmpList &= "," & CInt(lvItem.Tag)
            '    End If
            'Next
            'Sohail (26 Aug 2016) -- Start
            'Enhancement - 57.4 & 63.1 - Process Payroll Performance enhancement for TRA.
            'Dim allEmp As List(Of String) = (From lv In lvEmployeeList.CheckedItems.Cast(Of ListViewItem)() Select (lv.Tag.ToString)).ToList
            Dim allEmp As List(Of String) = (From p In dvEmployee.Table Where (CBool(p.Item("IsChecked")) = True) Select (p.Item("employeeunkid").ToString)).ToList
            'Sohail (26 Aug 2016) -- End
            strEmpList = String.Join(",", allEmp.ToArray)
            'Sohail (07 Sep 2013) -- End

            Dim frm As New frmReasonSelection
            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If

            Dim mstrVoidReason As String = String.Empty
            frm.displayDialog(enVoidCategoryType.PAYROLL, mstrVoidReason)
            If mstrVoidReason.Length <= 0 Then
                Exit Try
            End If

            'S.SANDEEP [28-May-2018] -- START
            'ISSUE/ENHANCEMENT : {Audit Trails} 
            objTnALeave._FormName = mstrModuleName
            objTnALeave._LoginEmployeeUnkid = 0
            objTnALeave._ClientIP = getIP()
            objTnALeave._HostName = getHostName()
            objTnALeave._FromWeb = False
            objTnALeave._AuditUserId = User._Object._Userunkid
objTnALeave._CompanyUnkid = Company._Object._Companyunkid
            objTnALeave._AuditDate = ConfigParameter._Object._CurrentDateAndTime
            'S.SANDEEP [28-May-2018] -- END

            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'If objTnALeave.VoidPayroll(strEmpList, CInt(cboPayPeriod.SelectedValue), mdtPayPeriodStartDate, User._Object._Userunkid, ConfigParameter._Object._CurrentDateAndTime, mstrVoidReason) = False Then
            If objTnALeave.VoidPayroll(strEmpList, CInt(cboPayPeriod.SelectedValue), mdtPayPeriodStartDate, User._Object._Userunkid, ConfigParameter._Object._CurrentDateAndTime, mstrVoidReason, ConfigParameter._Object._ApplyPayPerActivity) = False Then
                'Sohail (21 Aug 2015) -- End
                eZeeMsgBox.Show(objPayroll._Message, enMsgBoxStyle.Information)
                Exit Try
            End If

            Cursor.Current = Cursors.Default
            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 25, "Void Payroll Process completed successfully."), enMsgBoxStyle.Information)
            objchkSelectAll.Checked = False

            'Sohail (03 May 2018) -- Start
            'Enhancement : Removing global objects and passing objDataOperation in process payroll in 72.1.
            dgEmployee.DataSource = Nothing
            objlblEmpCount.Text = "( 0 / 0 )"
            Call ShowCount()
            'Sohail (03 May 2018) -- End

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnVoidPayroll_Click", mstrModuleName)
        Finally
            Me.ResumeLayout()
            Cursor.Current = Cursors.Default
            RemoveHandler gfrmMDI.tmrReminder.Tick, AddressOf gfrmMDI.tmrReminder_Tick
            AddHandler gfrmMDI.tmrReminder.Tick, AddressOf gfrmMDI.tmrReminder_Tick
        End Try
    End Sub
    'Sohail (22 May 2012) -- End

    'Sohail (26 Aug 2016) -- Start
    'Enhancement - 57.4 & 63.1 - Process Payroll Performance enhancement for TRA.
    Private Sub btnStop_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnStop.Click
        Try
            If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 31, "If you Stop the Payroll Process, You have to do Payroll Process again for Remaining employees.") & vbCrLf & vbCrLf & Language.getMessage(mstrModuleName, 32, "Are you sure you want to Stop the Payroll Process?"), CType(enMsgBoxStyle.Information + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.No Then
                Exit Sub
            End If

            mblnStopPayrollProcess = True
            objbgwProcessPayroll.CancelAsync()

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnStop_Click", mstrModuleName)
        End Try
    End Sub
    'Sohail (26 Aug 2016) -- End

#End Region

    'Sohail (26 Aug 2016) -- Start
    'Enhancement - 57.4 & 63.1 - Process Payroll Performance enhancement for TRA.
    '#Region " Listview's Events "
    '    Private Sub lvEmployeeList_ItemChecked(ByVal sender As Object, ByVal e As System.Windows.Forms.ItemCheckedEventArgs) Handles lvEmployeeList.ItemChecked

    '        Try
    '            'Sohail (03 Nov 2010) -- Start
    '            'If lvEmployeeList.CheckedItems.Count = lvEmployeeList.Items.Count Then
    '            '    chkSelectAll.Checked = True
    '            'ElseIf lvEmployeeList.CheckedItems.Count = 0 Then
    '            '    chkSelectAll.Checked = False
    '            'End If
    '            If lvEmployeeList.CheckedItems.Count <= 0 Then
    '                RemoveHandler objchkSelectAll.CheckedChanged, AddressOf chkSelectAll_CheckedChanged
    '                objchkSelectAll.CheckState = CheckState.Unchecked
    '                AddHandler objchkSelectAll.CheckedChanged, AddressOf chkSelectAll_CheckedChanged
    '            ElseIf lvEmployeeList.CheckedItems.Count < lvEmployeeList.Items.Count Then
    '                RemoveHandler objchkSelectAll.CheckedChanged, AddressOf chkSelectAll_CheckedChanged
    '                objchkSelectAll.CheckState = CheckState.Indeterminate
    '                AddHandler objchkSelectAll.CheckedChanged, AddressOf chkSelectAll_CheckedChanged
    '            ElseIf lvEmployeeList.CheckedItems.Count = lvEmployeeList.Items.Count Then
    '                RemoveHandler objchkSelectAll.CheckedChanged, AddressOf chkSelectAll_CheckedChanged
    '                objchkSelectAll.CheckState = CheckState.Checked
    '                AddHandler objchkSelectAll.CheckedChanged, AddressOf chkSelectAll_CheckedChanged
    '            End If
    '            'Sohail (03 Nov 2010) -- End
    '        Catch ex As Exception
    '            DisplayError.Show("-1", ex.Message, "lvEmployeeList_ItemChecked", mstrModuleName)
    '        End Try
    '    End Sub

    '    Private Sub lvEmployeeList_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lvEmployeeList.SelectedIndexChanged
    '        Try
    '            With lvEmployeeList
    '                If .SelectedItems.Count = 0 Then Exit Sub
    '                Call FillEDList(CInt(.Items(.SelectedItems(0).Index).Tag))
    '            End With
    '        Catch ex As Exception
    '            DisplayError.Show("-1", ex.Message, "lvEmployeeList_SelectedIndexChanged", mstrModuleName)
    '        End Try
    '    End Sub
    '#End Region

#Region " GridView Events "

    Private Sub dgEmployee_CellContentClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgEmployee.CellContentClick, dgEmployee.CellContentDoubleClick
        'Try
        '    If e.RowIndex < 0 Then Exit Sub

        '    If e.ColumnIndex = objdgcolhCheck.Index Then
        '        If dgEmployee.IsCurrentCellDirty Then
        '            dgEmployee.CommitEdit(DataGridViewDataErrorContexts.Commit)
        '            dvEmployee.ToTable.AcceptChanges()
        '        End If

        '        SetCheckBoxValue()
        '    End If

        'Catch ex As Exception
        '    DisplayError.Show("-1", ex.Message, "dgEmployee_CellContentClick", mstrModuleName)
        'End Try
    End Sub

    Private Sub dgEmployee_CurrentCellDirtyStateChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles dgEmployee.CurrentCellDirtyStateChanged
        Try
            If dgEmployee.IsCurrentCellDirty Then
                dgEmployee.CommitEdit(DataGridViewDataErrorContexts.Commit)
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgEmployee_CurrentCellDirtyStateChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub dgEmployee_CellValueChanged(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgEmployee.CellValueChanged
        Try
            If e.RowIndex < 0 Then Exit Sub

            If e.ColumnIndex = objdgcolhCheck.Index Then
                SetCheckBoxValue()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgEmployee_CellValueChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub dgEmployee_SelectionChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles dgEmployee.SelectionChanged
        Try
            If dgEmployee.SelectedRows.Count = 0 Then Exit Sub
            Call FillEDList(CInt(dgEmployee.SelectedRows(0).Cells(objdgcolhEmployeeunkid.Index).Value))
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgEmployee_SelectionChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub dgEmployee_DataBindingComplete(ByVal sender As Object, ByVal e As System.EventArgs) Handles dgEmployee.DataBindingComplete
        Try
            If chkExcludeWithPayment.Checked = False Then 'Sohail (14 Feb 2019)
            Call SetGridColour()
            End If 'Sohail (14 Feb 2019)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgEmployee_DataBindingComplete", mstrModuleName)
        End Try
    End Sub

#End Region
    'Sohail (26 Aug 2016) -- End

#Region " CheckBox's Events "
    Private Sub chkSelectAll_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objchkSelectAll.CheckedChanged
        Try
            Call CheckAllEmployee(objchkSelectAll.Checked)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "chkSelectAll_CheckedChanged", mstrModuleName)
        End Try
    End Sub

    'Sohail (22 May 2012) -- Start
    'TRA - ENHANCEMENT
    Private Sub chkExcludeWithPayment_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkExcludeWithPayment.CheckedChanged
        Try
            Call FillList()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "chkExcludeWithPayment_CheckedChanged", mstrModuleName)
        End Try
    End Sub
    'Sohail (22 May 2012) -- End

    'Sohail (12 Jul 2012) -- Start
    'TRA - ENHANCEMENT
    Private Sub chkUnProcessedEmpOnly_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkUnProcessedEmpOnly.CheckedChanged
        Try
            Call FillList()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "chkUnProcessedEmpOnly_CheckedChanged", mstrModuleName)
        End Try
    End Sub
    'Sohail (12 Jul 2012) -- End
#End Region

#Region " Other Control's Events "
    Private Sub objbtnSearchEmployee_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchEmployee.Click
        Dim objfrm As New frmCommonSearch
        'Sohail (06 Jan 2012) -- Start
        'TRA - ENHANCEMENT
        'Dim objEmployee As New clsEmployee_Master
        'Dim dsList As DataSet
        'Sohail (06 Jan 2012) -- End
        Try
            'Anjan (02 Sep 2011)-Start
            'Issue : Including Language Settings.
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If
            'Anjan (02 Sep 2011)-End 

            'Sohail (06 Jan 2012) -- Start
            'TRA - ENHANCEMENT
            'dsList = objEmployee.GetEmployeeList("EmployeeList")
            'Sohail (06 Jan 2012) -- End
            With cboEmployee
                'Sohail (06 Jan 2012) -- Start
                'TRA - ENHANCEMENT
                'objfrm.DataSource = dsList.Tables("EmployeeList")
                objfrm.DataSource = CType(cboEmployee.DataSource, DataTable)
                'Sohail (06 Jan 2012) -- End
                objfrm.ValueMember = .ValueMember
                objfrm.DisplayMember = .DisplayMember
                objfrm.CodeMember = "employeecode"
                If objfrm.DisplayDialog Then
                    .SelectedValue = objfrm.SelectedValue
                End If
                .Focus()
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchEmployee_Click", mstrModuleName)
        Finally
            objfrm = Nothing
            'objEmployee = Nothing 'Sohail (06 Jan 2012)
        End Try
    End Sub

    Private Sub objbtnSearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearch.Click
        Try
            If objchkSelectAll.Checked = True Then objchkSelectAll.Checked = False 'Sohail (09 Oct 2010)
            Call FillList()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearch_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnReset.Click
        Try
            If cboEmployee.Items.Count > 0 Then cboEmployee.SelectedValue = 0
            'Sohail (02 Jul 2014) -- Start
            'Enhancement - Pay Type and Pay Point allocation on Advance Filter.
            'If cboDepartment.Items.Count > 0 Then cboDepartment.SelectedValue = 0
            'If cboGrade.Items.Count > 0 Then cboGrade.SelectedValue = 0
            'If cboSections.Items.Count > 0 Then cboSections.SelectedValue = 0
            ''If cboAccess.Items.Count > 0 Then cboAccess.SelectedValue = 0
            'If cboClass.Items.Count > 0 Then cboClass.SelectedValue = 0
            'If cboCostCenter.Items.Count > 0 Then cboCostCenter.SelectedValue = 0
            ''If cboService.Items.Count > 0 Then cboService.SelectedValue = 0
            'If cboJob.Items.Count > 0 Then cboJob.SelectedValue = 0
            'If cboPayPoint.Items.Count > 0 Then cboPayPoint.SelectedValue = 0
            'If cboUnit.Items.Count > 0 Then cboUnit.SelectedValue = 0
            mstrAdvanceFilter = ""
            'Sohail (02 Jul 2014) -- End

            'Sohail (26 Aug 2016) -- Start
            'Enhancement - 57.4 & 63.1 - Process Payroll Performance enhancement for TRA.
            'lvEmployeeList.Items.Clear() 
            dgEmployee.DataSource = Nothing
            'Sohail (26 Aug 2016) -- End
            lvED.Items.Clear()
            'Sohail (09 Oct 2010) -- Start
            If objchkSelectAll.Checked = True Then objchkSelectAll.Checked = False
            Call FillList()
            'Sohail (09 Oct 2010) -- End
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnReset_Click", mstrModuleName)
        End Try
    End Sub

    'Sohail (26 Aug 2016) -- Start
    'Enhancement - 57.4 & 63.1 - Process Payroll Performance enhancement for TRA.
    Private Sub txtSearchEmp_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtSearchEmp.GotFocus
        Try
            With txtSearchEmp
                .ForeColor = Color.Black
                .Font = New Font(Me.Font.FontFamily, Me.Font.Size, FontStyle.Regular)

                If .Text = mstrSearchEmpText Then
                    .Clear()
                End If
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "txtSearchEmp_GotFocus", mstrModuleName)
        End Try
    End Sub

    Private Sub txtSearchEmp_Leave(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtSearchEmp.Leave
        Try
            If txtSearchEmp.Text.Trim = "" Then
                Call SetDefaultSearchEmpText()
            End If
            Call dgEmployee_SelectionChanged(dgEmployee, New System.EventArgs)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "txtSearchEmp_Leave", mstrModuleName)
        End Try
    End Sub
    'Sohail (26 Aug 2016) -- End

    'Sohail (25 May 2012) -- Start
    'TRA - ENHANCEMENT
    Private Sub txtSearchEmp_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtSearchEmp.TextChanged
        Try
            'Sohail (26 Aug 2016) -- Start
            'Enhancement - 57.4 & 63.1 - Process Payroll Performance enhancement for TRA.
            'If lvEmployeeList.Items.Count <= 0 Then Exit Sub
            'lvEmployeeList.SelectedIndices.Clear()
            'Dim lvFoundItem As ListViewItem = lvEmployeeList.FindItemWithText(txtSearchEmp.Text, True, 0, True)
            'If lvFoundItem IsNot Nothing Then
            '    lvEmployeeList.TopItem = lvFoundItem
            '    lvFoundItem.Selected = True
            'End If
            If txtSearchEmp.Text.Trim = mstrSearchEmpText Then Exit Sub
            If dvEmployee IsNot Nothing Then
                RemoveHandler dgEmployee.SelectionChanged, AddressOf dgEmployee_SelectionChanged
                dvEmployee.RowFilter = "employeecode LIKE '%" & txtSearchEmp.Text.Replace("'", "''") & "%'  OR employeename LIKE '%" & txtSearchEmp.Text.Replace("'", "''") & "%'"
                dgEmployee.Refresh()
                AddHandler dgEmployee.SelectionChanged, AddressOf dgEmployee_SelectionChanged
            End If
            'Sohail (26 Aug 2016) -- End
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "txtSearchEmp_TextChanged", mstrModuleName)
        End Try
    End Sub
    'Sohail (25 May 2012) -- End

    'Sohail (22 Aug 2012) -- Start
    'TRA - ENHANCEMENT
    Private Sub objbgwProcessPayroll_DoWork(ByVal sender As Object, ByVal e As System.ComponentModel.DoWorkEventArgs) Handles objbgwProcessPayroll.DoWork
        Dim strEmpList As String = ""
        Dim strEmpVoidList As String = ""
        Try
            'Sohail (21 Jun 2013) -- Start
            'TRA - ENHANCEMENT
            'For Each lvItem As ListViewItem In lvEmployeeList.CheckedItems
            '    If strEmpList.Length <= 0 Then
            '        strEmpList &= CInt(lvItem.Tag)
            '    Else
            '        strEmpList &= "," & CInt(lvItem.Tag)
            '    End If
            '    If lvItem.SubItems(colhIsPaymentDone.Index).Text.ToUpper <> "YES" Then
            '        If strEmpVoidList.Length <= 0 Then
            '            strEmpVoidList &= CInt(lvItem.Tag)
            '        Else
            '            strEmpVoidList &= "," & CInt(lvItem.Tag)
            '        End If
            '    End If
            'Next
            'Sohail (26 Aug 2016) -- Start
            'Enhancement - 57.4 & 63.1 - Process Payroll Performance enhancement for TRA.
            'Dim allEmp As List(Of String) = (From lvItem As ListViewItem In lvEmployeeList.CheckedItems.Cast(Of ListViewItem)() Select (lvItem.Tag.ToString)).ToList()
            GC.Collect()
            dtStart = DateAndTime.Now
            Dim allEmp As List(Of String) = (From p In dvEmployee.Table Where (CBool(p.Item("IsChecked")) = True) Select (p.Item("employeeunkid").ToString)).ToList
            'Sohail (26 Aug 2016) -- End
            strEmpList = String.Join(",", allEmp.ToArray())
            'Sohail (26 Aug 2016) -- Start
            'Enhancement - 57.4 & 63.1 - Process Payroll Performance enhancement for TRA.
            'Dim unpaidEmp As List(Of String) = (From lvItem As ListViewItem In lvEmployeeList.CheckedItems.Cast(Of ListViewItem)() Where lvItem.SubItems(colhIsPaymentDone.Index).Text <> "YES" Select (lvItem.Tag.ToString)).ToList()
            Dim unpaidEmp As List(Of String) = (From p In dvEmployee.Table Where (CBool(p.Item("IsChecked")) = True AndAlso CBool(p.Item("IsPaymentDone")) = False) Select (p.Item("employeeunkid").ToString)).ToList
            'Sohail (26 Aug 2016) -- End
            strEmpVoidList = String.Join(",", unpaidEmp.ToArray())

            mblnProcessFailed = False
            Me.ControlBox = False
            'Sohail (26 Aug 2016) -- Start
            'Enhancement - 57.4 & 63.1 - Process Payroll Performance enhancement for TRA.
            'Me.Enabled = False
            Call EnableControls(False)
            'Sohail (26 Aug 2016) -- End

            objTnALeave._Userunkid = User._Object._Userunkid
            objTnALeave._Voiduserunkid = User._Object._Userunkid
            objTnALeave._Processdate = dtpAsOnDate.Value
            'Sohail (07 Jan 2014) -- Start
            'Enhancement - Separate TnA Periods from Payroll Periods
            objTnALeave._TnA_Processdate = CDate(objlblTnAEndDate.Tag)
            Dim objPeriod As New clscommom_period_Tran
            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'objPeriod._Periodunkid = CInt(cboPayPeriod.SelectedValue)
            objPeriod._Periodunkid(FinancialYear._Object._DatabaseName) = CInt(cboPayPeriod.SelectedValue)
            'Sohail (21 Aug 2015) -- End
            objTnALeave._TnA_Startdate = objPeriod._TnA_StartDate
            objTnALeave._TnA_Enddate = objPeriod._TnA_EndDate 'Sohail (18 Jun 2014)
            'Sohail (26 Aug 2016) -- Start
            'Enhancement - 57.4 & 63.1 - Process Payroll Performance enhancement for TRA.
            objTnALeave._Payperiodunkid = CInt(cboPayPeriod.SelectedValue)
            'Sohail (26 Aug 2016) -- End
            objPeriod = Nothing
            'Sohail (07 Jan 2014) -- End
            objTnALeave._RoundOff_Type = ConfigParameter._Object._RoundOff_Type 'Sohail (01 Mar 2016)
            'Sohail (24 Apr 2018) -- Start
            'Voltamp Enhancement : Each head shoud be rounded before using in addition in formula in 71.1.
            objTnALeave._FmtCurrency = GUI.fmtCurrency
            'Sohail (24 Apr 2018) -- End
            'Sohail (03 May 2018) -- Start
            'Enhancement : Removing global objects and passing objDataOperation in process payroll in 72.1.
            objTnALeave._Countryunkid = Company._Object._Countryunkid
            'Sohail (03 May 2018) -- End

            'Sohail (14 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - Give cost centre mapping on loan scheme, saving scheme screen and configuration and show cost center on JV from mapped cost centre on loan and saving screen for loan and saving tranactions.
            objTnALeave._Advance_CostCenterunkid = ConfigParameter._Object._Advance_CostCenterunkid
            'Sohail (14 Mar 2019) -- End
            'Sohail (09 Oct 2019) -- Start
            'NMB Enhancement # : option Exclude employees from payroll during Suspension period to prorate salary and flat rate heads on employee suspension will be given on configuration.
            objTnALeave._ExcludeEmpFromPayrollDuringSuspension = ConfigParameter._Object._ExcludeEmpFromPayrollDuringSuspension
            'Sohail (09 Oct 2019) -- End

            'S.SANDEEP [28-May-2018] -- START
            'ISSUE/ENHANCEMENT : {Audit Trails} 
            objTnALeave._FormName = mstrModuleName
            objTnALeave._LoginEmployeeUnkid = 0
            objTnALeave._ClientIP = getIP()
            objTnALeave._HostName = getHostName()
            objTnALeave._FromWeb = False
            objTnALeave._AuditUserId = User._Object._Userunkid
objTnALeave._CompanyUnkid = Company._Object._Companyunkid
            objTnALeave._AuditDate = ConfigParameter._Object._CurrentDateAndTime
            'S.SANDEEP [28-May-2018] -- END

            'Shani(24-Aug-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'To Change GetEmployeeShift_PolicyList Method set Active Employee Query wise.
            'objTnALeave.InsertDelete(strEmpList, strEmpVoidList, CInt(cboPayPeriod.SelectedValue), objbgwProcessPayroll)


            'Pinkal (18-Nov-2016) -- Start
            'Enhancement - Working on IBA Leave Requirement as per Mr.Rutta's Guidance.
            'objTnALeave.InsertDelete(FinancialYear._Object._DatabaseName, _
            '                         User._Object._Userunkid, _
            '                         FinancialYear._Object._YearUnkid, _
            '                         Company._Object._Companyunkid, _
            '                         ConfigParameter._Object._UserAccessModeSetting, _
            '                         strEmpList, _
            '                         strEmpVoidList, _
            '                         CInt(cboPayPeriod.SelectedValue), _
            '                         ConfigParameter._Object._BasicSalaryComputation, ConfigParameter._Object._CurrentDateAndTime, ConfigParameter._Object._ApplyPayPerActivity, FinancialYear._Object._Database_Start_Date, FinancialYear._Object._Database_End_Date, ConfigParameter._Object._LeaveBalanceSetting, FinancialYear._Object._IsFin_Close, ConfigParameter._Object._Base_CurrencyId, _
            '                         objbgwProcessPayroll)

            objTnALeave.InsertDelete(FinancialYear._Object._DatabaseName, _
                                     User._Object._Userunkid, _
                                     FinancialYear._Object._YearUnkid, _
                                     Company._Object._Companyunkid, _
                                     ConfigParameter._Object._UserAccessModeSetting, _
                                     strEmpList, _
                                     strEmpVoidList, _
                                     CInt(cboPayPeriod.SelectedValue), _
                                  ConfigParameter._Object._BasicSalaryComputation, ConfigParameter._Object._CurrentDateAndTime, _
                                  ConfigParameter._Object._ApplyPayPerActivity, FinancialYear._Object._Database_Start_Date, _
                                  FinancialYear._Object._Database_End_Date, ConfigParameter._Object._LeaveBalanceSetting, _
                                  FinancialYear._Object._IsFin_Close, ConfigParameter._Object._Base_CurrencyId, _
                                  ConfigParameter._Object._LeaveAccrueTenureSetting, ConfigParameter._Object._LeaveAccrueDaysAfterEachMonth, _
                                      ConfigParameter._Object._FlatRateHeadsComputation, _
                                     objbgwProcessPayroll)

            'Pinkal (18-Nov-2016) -- End


            'Shani(24-Aug-2015) -- End
        Catch ex As Exception
            mblnProcessFailed = True
            'Sohail (12 Dec 2015) -- Start
            'Enhancement - Showing Progress of closing period.
            RemoveHandler gfrmMDI.tmrReminder.Tick, AddressOf gfrmMDI.tmrReminder_Tick
            AddHandler gfrmMDI.tmrReminder.Tick, AddressOf gfrmMDI.tmrReminder_Tick
            clsApplicationIdleTimer.LastIdealTime.Start()
            'Sohail (12 Dec 2015) -- End
            DisplayError.Show("-1", ex.Message, "objbgwProcessPayroll_DoWork", mstrModuleName)
            'Sohail (12 Dec 2015) -- Start
            'Enhancement - Showing Progress of closing period.
        Finally
            RemoveHandler gfrmMDI.tmrReminder.Tick, AddressOf gfrmMDI.tmrReminder_Tick
            AddHandler gfrmMDI.tmrReminder.Tick, AddressOf gfrmMDI.tmrReminder_Tick
            clsApplicationIdleTimer.LastIdealTime.Start()
            'Sohail (12 Dec 2015) -- End
        End Try
    End Sub

    Private Sub objbgwProcessPayroll_ProgressChanged(ByVal sender As Object, ByVal e As System.ComponentModel.ProgressChangedEventArgs) Handles objbgwProcessPayroll.ProgressChanged
        Try
            objlblProgress.Text = "[ " & e.ProgressPercentage.ToString & " / " & mintCheckedEmployee & " ]"
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbgwProcessPayroll_ProgressChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub objbgwProcessPayroll_RunWorkerCompleted(ByVal sender As Object, ByVal e As System.ComponentModel.RunWorkerCompletedEventArgs) Handles objbgwProcessPayroll.RunWorkerCompleted
        Try

            RemoveHandler gfrmMDI.tmrReminder.Tick, AddressOf gfrmMDI.tmrReminder_Tick
            AddHandler gfrmMDI.tmrReminder.Tick, AddressOf gfrmMDI.tmrReminder_Tick
            clsApplicationIdleTimer.LastIdealTime.Start() 'Sohail (11 Dec 2015)

            If e.Cancelled = True Then

            ElseIf e.Error IsNot Nothing Then
                eZeeMsgBox.Show(e.Error.ToString, enMsgBoxStyle.Information)
            ElseIf mblnProcessFailed = True Then

            Else
                'Sohail (26 Aug 2016) -- Start
                'Enhancement - 57.4 & 63.1 - Process Payroll Performance enhancement for TRA.
                'eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Payroll Process completed successfully."), enMsgBoxStyle.Information)
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Payroll Process completed successfully.") & vbCrLf & "(" & Language.getMessage(mstrModuleName, 33, "In Seconds") & " " & CInt((Now - dtStart).TotalSeconds).ToString & " " & Language.getMessage(mstrModuleName, 30, "For") & " " & objlblProgress.Text & ")", enMsgBoxStyle.Information)
                'Sohail (26 Aug 2016) -- End

                'Sohail (23 May 2017) -- Start
                'Enhancement - 67.1 - Link budget with Payroll.
                Dim objBudget As New clsBudget_MasterNew
                Dim intBudgetId As Integer = 0
                Dim dsList As DataSet
                dsList = objBudget.GetComboList("List", False, True, )
                If dsList.Tables(0).Rows.Count > 0 Then
                    intBudgetId = CInt(dsList.Tables(0).Rows(0).Item("budgetunkid"))
                End If
                If intBudgetId > 0 Then
                    Dim mstrAnalysis_Fields As String = ""
                    Dim mstrAnalysis_Join As String = ""
                    Dim mstrAnalysis_OrderBy As String = "hremployee_master.employeeunkid"
                    Dim mstrReport_GroupName As String = ""
                    Dim mstrAnalysis_TableName As String = ""
                    Dim mstrAnalysis_CodeField As String = ""
                    objBudget._Budgetunkid = intBudgetId
                    Dim strEmpList As String = ""
                    Dim allEmp As List(Of String) = (From p In dvEmployee.Table Where (CBool(p.Item("IsChecked")) = True) Select (p.Item("employeeunkid").ToString)).ToList
                    strEmpList = String.Join(",", allEmp.ToArray())
                    Dim objBudgetCodes As New clsBudgetcodes_master
                    Dim dsEmp As DataSet = Nothing
                    If objBudget._Viewbyid = enBudgetViewBy.Allocation Then
                        Dim objEmp As New clsEmployee_Master
                        Dim objBudget_Tran As New clsBudget_TranNew
                        Dim mstrAllocationTranUnkIDs As String = objBudget_Tran.GetAllocationTranUnkIDs(intBudgetId)


                        frmViewAnalysis.GetAnalysisByDetails("frmViewAnalysis", objBudget._Allocationbyid, mstrAllocationTranUnkIDs, objBudget._Budget_date, "", mstrAnalysis_Fields, mstrAnalysis_Join, mstrAnalysis_OrderBy, "", mstrReport_GroupName, mstrAnalysis_TableName, mstrAnalysis_CodeField)

                        dsList = objBudgetCodes.GetEmployeeActivityPercentage(CInt(cboPayPeriod.SelectedValue), intBudgetId, 0, 0, mstrAllocationTranUnkIDs, 1)
                        dsEmp = objEmp.GetList(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, mdtPayPeriodStartDate, mdtPayPeriodEndDate, ConfigParameter._Object._UserAccessModeSetting, True, False, "List", , , , "hremployee_master.employeeunkid IN (" & strEmpList & ")")
                    Else
                        dsList = objBudgetCodes.GetEmployeeActivityPercentage(CInt(cboPayPeriod.SelectedValue), intBudgetId, 0, 0, strEmpList, 1)
                    End If

                    If dsList.Tables(0).Rows.Count > 0 Then
                        Dim dtTable As New DataTable
                        dtTable.Columns.Add("Activity Code", System.Type.GetType("System.String")).DefaultValue = ""
                        dtTable.Columns.Add("Current Balance", System.Type.GetType("System.Decimal")).DefaultValue = 0
                        dtTable.Columns.Add("Actual Salary", System.Type.GetType("System.Decimal")).DefaultValue = 0

                        Dim mdicActivity As New Dictionary(Of Integer, Decimal)
                        Dim mdicActivityCode As New Dictionary(Of Integer, String)
                        Dim objActivity As New clsfundactivity_Tran
                        Dim dsAct As DataSet = objActivity.GetList("Activity")
                        Dim objActAdjust As New clsFundActivityAdjustment_Tran
                        Dim dsActAdj As DataSet = objActAdjust.GetLastCurrentBalance("List", 0, mdtPayPeriodEndDate)
                        'mdicActivity = (From p In dsAct.Tables("Activity") Select New With {.Id = CInt(p.Item("fundactivityunkid")), .Total = CDec(p.Item("amount"))}).ToDictionary(Function(x) x.Id, Function(y) y.Total)
                        mdicActivity = (From p In dsActAdj.Tables("List") Select New With {.Id = CInt(p.Item("fundactivityunkid")), .Total = CDec(p.Item("newbalance"))}).ToDictionary(Function(x) x.Id, Function(y) y.Total)
                        mdicActivityCode = (From p In dsAct.Tables("Activity") Select New With {.Id = CInt(p.Item("fundactivityunkid")), .Code = p.Item("activity_code").ToString}).ToDictionary(Function(x) x.Id, Function(y) y.Code)
                        Dim dsBalance As DataSet = objTnALeave.Get_Balance_List(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, mdtPayPeriodStartDate, mdtPayPeriodEndDate, ConfigParameter._Object._UserAccessModeSetting, True, False, True, "prtnaleave_tran.balanceamount > 0", "list", strEmpList, CInt(cboPayPeriod.SelectedValue), "")
                        If objBudget._Viewbyid = enBudgetViewBy.Allocation AndAlso dsEmp IsNot Nothing Then
                            dsBalance.Tables(0).PrimaryKey = New DataColumn() {dsBalance.Tables(0).Columns("employeeunkid")}
                            dsBalance.Tables(0).Merge(dsEmp.Tables(0))
                        End If
                        Dim intActivityId As Integer
                        Dim decTotal As Decimal = 0
                        Dim decActBal As Decimal = 0
                        For Each pair In mdicActivityCode
                            intActivityId = pair.Key
                            decTotal = 0
                            decActBal = 0

                            If mdicActivity.ContainsKey(intActivityId) = True Then
                                decActBal = mdicActivity.Item(intActivityId)
                            End If

                            Dim lstRow As List(Of DataRow) = (From p In dsList.Tables(0) Where (CInt(p.Item("fundactivityunkid")) = intActivityId AndAlso CDec(p.Item("percentage")) > 0) Select (p)).ToList
                            If lstRow.Count > 0 Then
                                For Each dRow As DataRow In lstRow
                                    Dim intAllocUnkId As Integer = CInt(dRow.Item("Employeeunkid"))
                                    Dim decNetPay As Decimal = (From p In dsBalance.Tables(0) Where (CInt(p.Item(mstrAnalysis_OrderBy.Split(CChar("."))(1).Trim)) = intAllocUnkId AndAlso IsDBNull(p.Item("balanceamount")) = False) Select (CDec(p.Item("balanceamount")))).Sum
                                    decTotal += decNetPay * CDec(dRow.Item("percentage")) / 100
                                Next

                                If decTotal > decActBal Then
                                    'eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 20, "Actual Salary Payment will be exceeding to the Activity Current Balance.") & vbCrLf & vbCrLf & mdicActivityCode.Item(pair.Key).ToString & " Actual Salary : " & Format(decTotal, GUI.fmtCurrency) & vbCrLf & mdicActivityCode.Item(pair.Key).ToString & " Current Balance : " & Format(pair.Value, GUI.fmtCurrency))
                                    'Exit For
                                    Dim dr As DataRow = dtTable.NewRow
                                    dr.Item("Activity Code") = pair.Value
                                    dr.Item("Current Balance") = Format(decActBal, GUI.fmtCurrency)
                                    dr.Item("Actual Salary") = Format(decTotal, GUI.fmtCurrency)
                                    dtTable.Rows.Add(dr)
                                End If
                            End If
                        Next

                        If dtTable.Rows.Count > 0 Then
                            Dim objValid As New frmCommonValidationList
                            objValid.displayDialog(False, Language.getMessage(mstrModuleName, 35, "Actual Salary Payment will be exceeding to the Activity Current Balance."), dtTable)
                        End If
                    End If
                End If
                'Sohail (23 May 2017) -- End

                objchkSelectAll.Checked = False
                'Sohail (03 May 2018) -- Start
                'Enhancement : Removing global objects and passing objDataOperation in process payroll in 72.1.
                'cboPayPeriod.SelectedValue = 0
                dgEmployee.DataSource = Nothing
                objlblEmpCount.Text = "( 0 / 0 )"
                'Call ShowCount() 'Sohail (07 Jan 2020) 
                'Sohail (03 May 2018) -- End
                objlblProgress.Text = ""
            End If

            'Me.ControlBox = True
            ''Sohail (26 Aug 2016) -- Start
            ''Enhancement - 57.4 & 63.1 - Process Payroll Performance enhancement for TRA.
            ''Me.Enabled = True
            'Call EnableControls(True)
            'mblnStopPayrollProcess = False
            ''Sohail (26 Aug 2016) -- End

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbgwProcessPayroll_RunWorkerCompleted", mstrModuleName)
            'Sohail (23 May 2017) -- Start
            'Enhancement - 67.1 - Link budget with Payroll.
        Finally
            Me.ControlBox = True
            Call EnableControls(True)
            mblnStopPayrollProcess = False
            'Sohail (23 May 2017) -- End
            Call ShowCount() 'Sohail (07 Jan 2020)
        End Try
    End Sub
    'Sohail (22 Aug 2012) -- End

    'Sohail (07 Jan 2014) -- Start
    'Enhancement - Separate TnA Periods from Payroll Periods
    Private Sub dtpAsOnDate_ValueChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles dtpAsOnDate.ValueChanged
        Try
            If CInt(cboPayPeriod.SelectedValue) > 0 Then
                If dtpAsOnDate.Value.Date < CDate(dtpAsOnDate.Tag) Then
                    objlblTnAEndDate.Text = dtpAsOnDate.Value.Date.ToShortDateString
                    objlblTnAEndDate.Tag = dtpAsOnDate.Value.Date
                Else
                    objlblTnAEndDate.Text = CDate(dtpAsOnDate.Tag).ToShortDateString
                    objlblTnAEndDate.Tag = CDate(dtpAsOnDate.Tag)
                End If
            Else
                objlblTnAEndDate.Text = ""
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dtpAsOnDate_ValueChanged", mstrModuleName)
        End Try
    End Sub
    '    objlblTnAEndDate.Text = objPeriod._TnA_EndDate.ToShortDateString
    'Else
    '    objlblTnAEndDate.Text = ""
    'Sohail (07 Jan 2014) -- End

    'Sohail (02 Jul 2014) -- Start
    'Enhancement - Pay Type and Pay Point allocation on Advance Filter.
    Private Sub lnkAllocation_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lnkAllocation.LinkClicked
        Try
            Dim frm As New frmAdvanceSearch
            frm.ShowDialog()
            mstrAdvanceFilter = frm._GetFilterString
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lnkAllocation_LinkClicked", mstrModuleName)
        End Try
    End Sub
    'Sohail (02 Jul 2014) -- End

    'Sohail (03 May 2018) -- Start
    'Enhancement : Removing global objects and passing objDataOperation in process payroll in 72.1.
    Private Sub bdgNotProcessed_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles bdgNotProcessed.Click
        Try
            RemoveHandler chkUnProcessedEmpOnly.CheckedChanged, AddressOf chkUnProcessedEmpOnly_CheckedChanged
            chkUnProcessedEmpOnly.Checked = True
            AddHandler chkUnProcessedEmpOnly.CheckedChanged, AddressOf chkUnProcessedEmpOnly_CheckedChanged
            Call FillList()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "bdgNotProcessed_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub bdgProcessed_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles bdgProcessed.Click
        Try
            RemoveHandler chkUnProcessedEmpOnly.CheckedChanged, AddressOf chkUnProcessedEmpOnly_CheckedChanged
            chkUnProcessedEmpOnly.Checked = False
            AddHandler chkUnProcessedEmpOnly.CheckedChanged, AddressOf chkUnProcessedEmpOnly_CheckedChanged
            Call FillList(True)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "bdgProcessed_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub bdgTotalEmployee_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles bdgTotalEmployee.Click
        Try
            RemoveHandler chkUnProcessedEmpOnly.CheckedChanged, AddressOf chkUnProcessedEmpOnly_CheckedChanged
            chkUnProcessedEmpOnly.Checked = False
            AddHandler chkUnProcessedEmpOnly.CheckedChanged, AddressOf chkUnProcessedEmpOnly_CheckedChanged
            Call FillList()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "bdgProbdgTotalEmployee_Clickcessed_Click", mstrModuleName)
        End Try
    End Sub
    'Sohail (03 May 2018) -- End

    'Sohail (04 Jun 2018) -- Start
    'Internal Enhancement - Providing Summary count on process payroll screen and Payslip screen in 72.1.
    Private Sub bdgUnpaid_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles bdgUnpaid.Click
        Dim objFrm As New frmPayslipGlobalPayment
        Try
            'If User._Object._RightToLeft = True Then
            '    frm.RightToLeft = Windows.Forms.RightToLeft.Yes
            '    frm.RightToLeftLayout = True
            '    Call Language.ctlRightToLeftlayOut(frm)
            'End If

            If objFrm.DisplayDialog(enAction.ADD_CONTINUE) = True Then
                Call FillList()
                dgEmployee.DataSource = Nothing
                objlblEmpCount.Text = "( 0 / 0 )"
                Call ShowCount()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "bdgUnpaid_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub bdgTotalPaid_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles bdgTotalPaid.Click
        Dim frm As New frmGlobalVoidPayment
        Try
            'If User._Object._RightToLeft = True Then
            '    frm.RightToLeft = Windows.Forms.RightToLeft.Yes
            '    frm.RightToLeftLayout = True
            '    Call Language.ctlRightToLeftlayOut(frm)
            'End If

            If frm.displayDialog(clsPayment_tran.enPaymentRefId.PAYSLIP, clsPayment_tran.enPayTypeId.PAYMENT) Then
                Call FillList()
                dgEmployee.DataSource = Nothing
                objlblEmpCount.Text = "( 0 / 0 )"
                Call ShowCount()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "bdgTotalPaid_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub bdgTotalAuthorized_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles bdgTotalAuthorized.Click
        Dim objFrm As New frmAuthorizeUnauthorizePayment
        Try
            'If User._Object._RightToLeft = True Then
            '    frm.RightToLeft = Windows.Forms.RightToLeft.Yes
            '    frm.RightToLeftLayout = True
            '    Call Language.ctlRightToLeftlayOut(frm)
            'End If

            If objFrm.DisplayDialog(enAction.ADD_CONTINUE, True) = True Then
                Call FillList()
                dgEmployee.DataSource = Nothing
                objlblEmpCount.Text = "( 0 / 0 )"
                Call ShowCount()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "bdgTotalAuthorized_Click", mstrModuleName)
        Finally
            If objFrm IsNot Nothing Then objFrm.Dispose()
        End Try
    End Sub
    'Sohail (04 Jun 2018) -- End

    'S.SANDEEP |07-MAR-2020| -- START
    'ISSUE/ENHANCEMENT : PAYROLL UAT
    Private Sub bdgTotalOverDeduction_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles bdgTotalOverDeduction.Click
        Try
            Dim ds As New DataSet
            ds = objPayroll.GetPayrollProcessedUnPaid("List", FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, mdtPayPeriodStartDate, mdtPayPeriodEndDate, ConfigParameter._Object._UserAccessModeSetting, True, ConfigParameter._Object._IsIncludeInactiveEmp, CInt(cboPayPeriod.SelectedValue), True, True, mstrAdvanceFilter, "", Nothing)
            Dim objCommon As New frmCommonValidationList
            objCommon.Text = bdgTotalOverDeduction.Text_Caption
            Dim dtTable As DataTable : dtTable = ds.tables(0)
            objCommon.displayDialog(False, bdgTotalOverDeduction.Text_Caption, dtTable)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "bdgTotalOverDeduction_Click", mstrModuleName)
        Finally
        End Try
    End Sub
    'S.SANDEEP |07-MAR-2020| -- END

#End Region

#Region " Message List "
    '1, "Please select atleast one employee to process payroll."
    '2, "Please select Pay Year. Pay Year is mandatory information."
    '3, "Please select Pay Period. Pay Period is mandatory information."
    '5, "You have not selected all employee from the list. It is recommended that select all employee to process payroll." & vbCrLf & vbCrLf & _
    '           "Do you want to proceed?"
    '6, "Payroll Process completed successfully."
    '7, "Please select Earning Deduction from the list to perform further operation on it."
    '8, "Are you sure you want to delete this Earning Deduction?"
    '9, "Sorry, You can not Edit/Delete Salary Head. Please go to Employee Master to Edit/Delete this Head for this employee."
    '10, "Please select Transaction Head to Exempt.
    '11, "Sorry, You can not Exempt this Transaction Head. Reason: Type of Transaction Head is Salary.
    '12, "Sorry, You can not do process payroll at this time. Reason : Previous year (" & dtTable.Rows(0).Item("financialyear_name").ToString & ") is still open. Please close previous year first."
    '13, "Please select Transaction Head to change Cost Center."
    '14, "Payment is not done for some Loan/Advance whose deduction is going to be start from this month. If you don't make payment, Loan/Advance deduction will not start." & vbCrLf & vbCrLf & _
    '    "Do you want to proceed?"
#End Region




    
    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Private Sub OtherSettings()
        Try
            Me.SuspendLayout()
           
            Call SetLanguage()
			
			Me.gbProcessPayRollInfo.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbProcessPayRollInfo.ForeColor = GUI._eZeeContainerHeaderForeColor 

			Me.gbFilterCriteria.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbFilterCriteria.ForeColor = GUI._eZeeContainerHeaderForeColor 

			Me.gbEmployeeList.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbEmployeeList.ForeColor = GUI._eZeeContainerHeaderForeColor 


			
			Me.eZeeHeader.GradientColor1 = GUI._HeaderBackColor1 
			Me.eZeeHeader.GradientColor2 = GUI._HeaderBackColor2 
			Me.eZeeHeader.BorderColor = GUI._HeaderBorderColor 
			Me.eZeeHeader.HeaderTextForeColor = GUI._HeaderTitleFontColor 
			Me.eZeeHeader.DescriptionForeColor = GUI._HeaderMessageFontColor 


			Me.btnEdit.GradientBackColor = GUI._ButttonBackColor 
            Me.btnEdit.GradientForeColor = GUI._ButttonFontColor

			Me.btnDelete.GradientBackColor = GUI._ButttonBackColor 
            Me.btnDelete.GradientForeColor = GUI._ButttonFontColor

			Me.btnProcess.GradientBackColor = GUI._ButttonBackColor 
            Me.btnProcess.GradientForeColor = GUI._ButttonFontColor

			Me.btnClose.GradientBackColor = GUI._ButttonBackColor 
            Me.btnClose.GradientForeColor = GUI._ButttonFontColor

			Me.btnNew.GradientBackColor = GUI._ButttonBackColor 
            Me.btnNew.GradientForeColor = GUI._ButttonFontColor

			Me.btnExempt.GradientBackColor = GUI._ButttonBackColor 
            Me.btnExempt.GradientForeColor = GUI._ButttonFontColor

			Me.btnCostCenter.GradientBackColor = GUI._ButttonBackColor 
            Me.btnCostCenter.GradientForeColor = GUI._ButttonFontColor

			Me.btnVoidPayroll.GradientBackColor = GUI._ButttonBackColor 
            Me.btnVoidPayroll.GradientForeColor = GUI._ButttonFontColor

			Me.btnStop.GradientBackColor = GUI._ButttonBackColor 
            Me.btnStop.GradientForeColor = GUI._ButttonFontColor


            Me.ResumeLayout()
        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "OtherSettings", mstrModuleName)
        End Try
    End Sub
			
			
    Private Sub SetLanguage()
        Try
            Me.Text = Language._Object.getCaption(Me.Name, Me.Text)
			
			Me.eZeeHeader.Title = Language._Object.getCaption(Me.eZeeHeader.Name & "_Title" , Me.eZeeHeader.Title)
			Me.eZeeHeader.Message = Language._Object.getCaption(Me.eZeeHeader.Name & "_Message" , Me.eZeeHeader.Message)
            Me.gbProcessPayRollInfo.Text = Language._Object.getCaption(Me.gbProcessPayRollInfo.Name, Me.gbProcessPayRollInfo.Text)
            Me.lnEmpPaySlipInfo.Text = Language._Object.getCaption(Me.lnEmpPaySlipInfo.Name, Me.lnEmpPaySlipInfo.Text)
            Me.lblPayPeriod.Text = Language._Object.getCaption(Me.lblPayPeriod.Name, Me.lblPayPeriod.Text)
            Me.lblPayYear.Text = Language._Object.getCaption(Me.lblPayYear.Name, Me.lblPayYear.Text)
            Me.btnEdit.Text = Language._Object.getCaption(Me.btnEdit.Name, Me.btnEdit.Text)
            Me.btnDelete.Text = Language._Object.getCaption(Me.btnDelete.Name, Me.btnDelete.Text)
            Me.gbFilterCriteria.Text = Language._Object.getCaption(Me.gbFilterCriteria.Name, Me.gbFilterCriteria.Text)
            Me.lblEmployee.Text = Language._Object.getCaption(Me.lblEmployee.Name, Me.lblEmployee.Text)
            Me.btnProcess.Text = Language._Object.getCaption(Me.btnProcess.Name, Me.btnProcess.Text)
            Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
            Me.gbEmployeeList.Text = Language._Object.getCaption(Me.gbEmployeeList.Name, Me.gbEmployeeList.Text)
            Me.colhTranHead.Text = Language._Object.getCaption(CStr(Me.colhTranHead.Tag), Me.colhTranHead.Text)
            Me.colhTranHeadType.Text = Language._Object.getCaption(CStr(Me.colhTranHeadType.Tag), Me.colhTranHeadType.Text)
            Me.colhCalcType.Text = Language._Object.getCaption(CStr(Me.colhCalcType.Tag), Me.colhCalcType.Text)
            Me.colhAmount.Text = Language._Object.getCaption(CStr(Me.colhAmount.Tag), Me.colhAmount.Text)
            Me.btnNew.Text = Language._Object.getCaption(Me.btnNew.Name, Me.btnNew.Text)
            Me.btnExempt.Text = Language._Object.getCaption(Me.btnExempt.Name, Me.btnExempt.Text)
            Me.colhExempted.Text = Language._Object.getCaption(CStr(Me.colhExempted.Tag), Me.colhExempted.Text)
            Me.colhCostCenter.Text = Language._Object.getCaption(CStr(Me.colhCostCenter.Tag), Me.colhCostCenter.Text)
            Me.btnCostCenter.Text = Language._Object.getCaption(Me.btnCostCenter.Name, Me.btnCostCenter.Text)
            Me.lblAsOnDate.Text = Language._Object.getCaption(Me.lblAsOnDate.Name, Me.lblAsOnDate.Text)
            Me.colhPeriod.Text = Language._Object.getCaption(CStr(Me.colhPeriod.Tag), Me.colhPeriod.Text)
            Me.lblModes.Text = Language._Object.getCaption(Me.lblModes.Name, Me.lblModes.Text)
            Me.btnVoidPayroll.Text = Language._Object.getCaption(Me.btnVoidPayroll.Name, Me.btnVoidPayroll.Text)
            Me.chkExcludeWithPayment.Text = Language._Object.getCaption(Me.chkExcludeWithPayment.Name, Me.chkExcludeWithPayment.Text)
            Me.lblSearchEmp.Text = Language._Object.getCaption(Me.lblSearchEmp.Name, Me.lblSearchEmp.Text)
            Me.chkUnProcessedEmpOnly.Text = Language._Object.getCaption(Me.chkUnProcessedEmpOnly.Name, Me.chkUnProcessedEmpOnly.Text)
            Me.lblTnAEndDate.Text = Language._Object.getCaption(Me.lblTnAEndDate.Name, Me.lblTnAEndDate.Text)
            Me.lnkAllocation.Text = Language._Object.getCaption(Me.lnkAllocation.Name, Me.lnkAllocation.Text)
            Me.dgColhEmpCode.HeaderText = Language._Object.getCaption(Me.dgColhEmpCode.Name, Me.dgColhEmpCode.HeaderText)
            Me.dgColhEmployee.HeaderText = Language._Object.getCaption(Me.dgColhEmployee.Name, Me.dgColhEmployee.HeaderText)
            Me.btnStop.Text = Language._Object.getCaption(Me.btnStop.Name, Me.btnStop.Text)
			Me.DataGridViewTextBoxColumn1.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn1.Name, Me.DataGridViewTextBoxColumn1.HeaderText)
			Me.DataGridViewTextBoxColumn2.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn2.Name, Me.DataGridViewTextBoxColumn2.HeaderText)
			Me.DataGridViewTextBoxColumn3.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn3.Name, Me.DataGridViewTextBoxColumn3.HeaderText)
			Me.DataGridViewTextBoxColumn4.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn4.Name, Me.DataGridViewTextBoxColumn4.HeaderText)
			Me.bdgNotProcessed.Text = Language._Object.getCaption(Me.bdgNotProcessed.Name, Me.bdgNotProcessed.Text)
			Me.bdgProcessed.Text = Language._Object.getCaption(Me.bdgProcessed.Name, Me.bdgProcessed.Text)
			Me.bdgTotalEmployee.Text = Language._Object.getCaption(Me.bdgTotalEmployee.Name, Me.bdgTotalEmployee.Text)
			Me.bdgTotalAuthorized.Text = Language._Object.getCaption(Me.bdgTotalAuthorized.Name, Me.bdgTotalAuthorized.Text)
			Me.bdgTotalPaid.Text = Language._Object.getCaption(Me.bdgTotalPaid.Name, Me.bdgTotalPaid.Text)
			Me.lblPeriodName.Text = Language._Object.getCaption(Me.lblPeriodName.Name, Me.lblPeriodName.Text)
			Me.bdgUnpaid.Text = Language._Object.getCaption(Me.bdgUnpaid.Name, Me.bdgUnpaid.Text)
			Me.bdgTotalOverDeduction.Text = Language._Object.getCaption(Me.bdgTotalOverDeduction.Name, Me.bdgTotalOverDeduction.Text)

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
        End Try
    End Sub
			
			
    Private Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Please select atleast one employee to process payroll.")
            Language.setMessage(mstrModuleName, 2, "Please select Pay Year. Pay Year is mandatory information.")
            Language.setMessage(mstrModuleName, 3, "Please select Pay Period. Pay Period is mandatory information.")
            Language.setMessage(mstrModuleName, 4, "Payroll Process completed successfully.")
            Language.setMessage(mstrModuleName, 5, "Please select Earning/Deduction from the list to perform further operation.")
            Language.setMessage(mstrModuleName, 6, "Are you sure you want to delete this Earning Deduction?")
            Language.setMessage(mstrModuleName, 7, "Sorry, You can not Edit/Delete Salary Head. Please go to Employee Master to Edit/Delete this Head for this employee.")
            Language.setMessage(mstrModuleName, 8, "Please select Transaction Head to Exempt.")
            Language.setMessage(mstrModuleName, 9, "Sorry, You can not Exempt this Transaction Head. Reason: Type of Transaction Head is Salary.")
            Language.setMessage(mstrModuleName, 10, "Please select Transaction Head to change Cost Center.")
            Language.setMessage(mstrModuleName, 11, "Sorry, You cannot do process payroll at this time. Reason : Previous year [")
            Language.setMessage(mstrModuleName, 12, "] is still open. Please close previous year first.")
            Language.setMessage(mstrModuleName, 13, "Sorry, Absent Process is not done yet for this period.It is recommended that first perform Absent process for this period from Time and Attendance Module.")
            Language.setMessage(mstrModuleName, 14, "Sorry, Previous Period [")
            Language.setMessage(mstrModuleName, 15, "] is still Open. Please Close Previous Period first.")
            Language.setMessage(mstrModuleName, 16, "Payment is not done for some Loan/Advance whose deduction is going to start from this month. If you don't make payment, Loan/Advance deduction will not start.")
            Language.setMessage(mstrModuleName, 17, "Do you want to proceed?")
            Language.setMessage(mstrModuleName, 18, "You have not selected all employees from the list. It is recommended that you select all employees to process payroll.")
            Language.setMessage(mstrModuleName, 19, "Sorry! You can not Edit/Delete this transaction. Reason: Period is closed.")
            Language.setMessage(mstrModuleName, 20, "Sorry, Some Transaction Heads are in Pending status (not Approved) on Earning Deduction for Selected Period. To perform payroll process, please Approve those heads.")
            Language.setMessage(mstrModuleName, 21, "Sorry, Some Salary Changes are in Pending status (not Approved) on Salary Change for Selected Period. To perform payroll process, please Approve those Salary Changes.")
            Language.setMessage(mstrModuleName, 22, "Sorry, You can not do Void Payroll for some of employees. Reason : Payment is already done for some of the employees.")
            Language.setMessage(mstrModuleName, 23, "Please select atleast one employee to Void Payroll.")
            Language.setMessage(mstrModuleName, 24, "Sorry, You can not do Void Payroll at this time. Reason : Previous year [")
            Language.setMessage(mstrModuleName, 25, "Void Payroll Process completed successfully.")
            Language.setMessage(mstrModuleName, 26, "Are you sure you want to Void Payroll for selected Employees ?")
            Language.setMessage(mstrModuleName, 27, "Some Employees leave form Start Date falls in this Period and their leave form(s) are in pending or approved status.Please issue or reject or reschedule their leave form.")
			Language.setMessage(mstrModuleName, 28, "Sorry, Payment of some of the employees is already done for this period.")
            Language.setMessage(mstrModuleName, 29, "Do you want to proceed?")
            Language.setMessage(mstrModuleName, 30, "For")
            Language.setMessage(mstrModuleName, 31, "If you Stop the Payroll Process, You have to do Payroll Process again for Remaining employees.")
            Language.setMessage(mstrModuleName, 32, "Are you sure you want to Stop the Payroll Process?")
            Language.setMessage(mstrModuleName, 33, "In Seconds")
            Language.setMessage(mstrModuleName, 34, "Type to Search")
	    Language.setMessage(mstrModuleName, 35, "Actual Salary Payment will be exceeding to the Activity Current Balance.")
			Language.setMessage(mstrModuleName, 36, "Show Active Employees")
			Language.setMessage(mstrModuleName, 37, "Show Terminated Employees with Payroll Excluded")
			Language.setMessage(mstrModuleName, 38, "Show Terminated Employees without Payroll Excluded")
			Language.setMessage(mstrModuleName, 39, "Sorry, Auto Salary Increment is not done for some of the employees for this period.")
			Language.setMessage(mstrModuleName, 40, "Sorry, Some active membership heads are not assigned on Earning Deductions. Please assign them from Earning Deduction List -> Operation -> Assign membership heads.")
			Language.setMessage(mstrModuleName, 41, "Formula is not set on the Transaction Head. Please set formula from Transaction Head screen for")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class