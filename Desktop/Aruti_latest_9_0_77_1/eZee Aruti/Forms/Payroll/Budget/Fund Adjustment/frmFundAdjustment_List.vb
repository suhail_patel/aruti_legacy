﻿Option Strict On

Imports eZeeCommonLib
Imports Aruti.Data
Imports System.Threading

Public Class frmFundAdjustment_List

#Region " Private Variables "
    Private ReadOnly mstrModuleName As String = "frmFundAdjustment_List"
    Private objFundAdjustment As clsFundAdjustment_Tran
    Private trd As Thread
#End Region

#Region " Form's Events "

    Private Sub frmFundAdjustment_List_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        Me.Close()
    End Sub

    Private Sub frmFundAdjustment_List_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles Me.KeyPress
        Try
            If Asc(e.KeyChar) = 27 Then
                Me.Close()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmFundAdjustment_List_KeyPress", mstrModuleName)
        End Try
    End Sub

    Private Sub frmFundAdjustment_List_KeyUp(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyUp
        Try
            If e.KeyCode = Keys.Delete And dgvFundAdjustment.Focused = True Then
                Call btnDelete.PerformClick()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmFundAdjustment_List_KeyUp", mstrModuleName)
        End Try
    End Sub

    Private Sub frmFundAdjustment_List_LanguageClick(ByVal sender As Object, ByVal eventArgs As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsFundAdjustment_Tran.SetMessages()
            objfrm._Other_ModuleNames = "clsFundAdjustment_Tran"
            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmFundAdjustment_List_LanguageClick", mstrModuleName)
        End Try
    End Sub

    Private Sub frmFundAdjustment_List_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Call Set_Logo(Me, gApplicationType)
            Language.setLanguage(Me.Name)
            Call OtherSettings()
            Call SetVisibility()
            Call FillCombo()
            'Sohail (22 Nov 2016) -- Start
            'Enhancement #24 -  65.1 - Increase / decrease fund project code balance on activity adjustment as per comment given by andrew.
            Call InsertFromActivityAdjustment()
            'Sohail (22 Nov 2016) -- End
            Call FillList()

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmFundAdjustment_List_Load", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Private Methods "

    Private Sub FillCombo()
        Try
            Dim dsList As New DataSet
            Dim objFundProjectCode As New clsFundProjectCode

            dsList = objFundProjectCode.GetComboList("List", True)
            With cboProjectCode
                .ValueMember = "fundprojectcodeunkid"
                .DisplayMember = "fundprojectname"
                .DataSource = dsList.Tables("List")
                .SelectedValue = 0
            End With

            objFundProjectCode = Nothing
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        End Try
    End Sub

    'Sohail (22 Nov 2016) -- Start
    'Enhancement #24 -  65.1 - Increase / decrease fund project code balance on activity adjustment as per comment given by andrew.
    Private Sub InsertFromActivityAdjustment()
        objFundAdjustment = New clsFundAdjustment_Tran
        Try
            'S.SANDEEP [28-May-2018] -- START
            'ISSUE/ENHANCEMENT : {Audit Trails} 
            objFundAdjustment._FormName = mstrModuleName
            objFundAdjustment._LoginEmployeeunkid = 0
            objFundAdjustment._ClientIP = getIP()
            objFundAdjustment._HostName = getHostName()
            objFundAdjustment._FromWeb = False
            objFundAdjustment._AuditUserId = User._Object._Userunkid
objFundAdjustment._CompanyUnkid = Company._Object._Companyunkid
            objFundAdjustment._AuditDate = ConfigParameter._Object._CurrentDateAndTime
            'S.SANDEEP [28-May-2018] -- END

            If objFundAdjustment.InsertFromActivityAdjustment(ConfigParameter._Object._CurrentDateAndTime) = False Then
                If objFundAdjustment._Message <> "" Then
                    eZeeMsgBox.Show(objFundAdjustment._Message, enMsgBoxStyle.Information)
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "InsertFromActivityAdjustment", mstrModuleName)
        End Try
    End Sub
    'Sohail (22 Nov 2016) -- End

    Private Sub FillList()
        Try
            Dim mstrSearch As String = String.Empty
            Dim dsList As New DataSet
            objFundAdjustment = New clsFundAdjustment_Tran

            If User._Object.Privilege._AllowToViewFundAdjustment = False Then Exit Sub

            If CInt(cboProjectCode.SelectedValue) > 0 Then
                mstrSearch &= "AND bgfundprojectcode_master.fundprojectcodeunkid = " & CInt(cboProjectCode.SelectedValue) & " "
            End If

            If dtpTransactionFrom.Checked = True Then
                mstrSearch &= "AND CONVERT(CHAR(8),bgfundadjustment_tran.transactiondate,112) >= " & eZeeDate.convertDate(dtpTransactionFrom.Value) & " "
            End If

            If dtpTransactionTo.Checked = True Then
                mstrSearch &= "AND CONVERT(CHAR(8),bgfundadjustment_tran.transactiondate,112) <= " & eZeeDate.convertDate(dtpTransactionTo.Value) & " "
            End If

            If txtCurrentBalFrom.Text.Trim <> "" AndAlso txtCurrentBalFrom.Decimal > 0 Then
                mstrSearch &= "AND bgfundadjustment_tran.newbalance >= " & txtCurrentBalFrom.Decimal & " "
            End If

            If txtCurrentBalTo.Text.Trim <> "" AndAlso txtCurrentBalTo.Decimal > 0 Then
                mstrSearch &= "AND bgfundadjustment_tran.newbalance <= " & txtCurrentBalTo.Decimal & " "
            End If

            If mstrSearch.Trim.Length > 0 Then
                mstrSearch = mstrSearch.Substring(3)
            End If

            dsList = objFundAdjustment.GetList("List", mstrSearch)

            dgvFundAdjustment.AutoGenerateColumns = False

            dgcolhFundProjectCode.DataPropertyName = "fundprojectname"
            dgcolhTarnsactionDate.DataPropertyName = "transactiondate"
            dgcolhTarnsactionDate.DefaultCellStyle.Format = Globalization.CultureInfo.CurrentCulture.DateTimeFormat.ShortDatePattern
            dgcolhCurrentBal.DataPropertyName = "currentbalance"
            dgcolhCurrentBal.HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleRight
            dgcolhCurrentBal.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
            dgcolhCurrentBal.DefaultCellStyle.Format = GUI.fmtCurrency
            dgcolhIncrDecrAmount.DataPropertyName = "incrdecramount"
            dgcolhIncrDecrAmount.HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleRight
            dgcolhIncrDecrAmount.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
            dgcolhIncrDecrAmount.DefaultCellStyle.Format = GUI.fmtCurrency
            dgcolhNewBalance.DataPropertyName = "newbalance"
            dgcolhNewBalance.HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleRight
            dgcolhNewBalance.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
            dgcolhNewBalance.DefaultCellStyle.Format = GUI.fmtCurrency
            objdgcolhfundadjustmentunkid.DataPropertyName = "fundadjustmentunkid"
            objdgcolhfundProjectCodeunkid.DataPropertyName = "fundprojectcodeunkid"

            dgvFundAdjustment.DataSource = dsList.Tables("List")

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillList", mstrModuleName)
        Finally
            objbtnSearch.ShowResult(dgvFundAdjustment.RowCount.ToString)
        End Try
    End Sub

    Private Sub SetVisibility()
        Try
            btnNew.Enabled = User._Object.Privilege._AllowToAddFundAdjustment
            btnEdit.Enabled = User._Object.Privilege._AllowToEditFundAdjustment
            btnDelete.Enabled = User._Object.Privilege._AllowToDeleteFundAdjustment

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetVisibility", mstrModuleName)
        End Try
    End Sub

    Private Sub ResetParameters()
        Try
            cboProjectCode.SelectedValue = 0
            dtpTransactionFrom.Value = ConfigParameter._Object._CurrentDateAndTime.Date
            dtpTransactionFrom.Checked = False
            dtpTransactionTo.Value = ConfigParameter._Object._CurrentDateAndTime.Date
            dtpTransactionTo.Checked = False
            txtCurrentBalFrom.Decimal = 0
            txtCurrentBalTo.Decimal = 0

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "ResetParameters", mstrModuleName)
        End Try
    End Sub

    Private Function Set_Notification(ByVal strUserName As String _
                                      , ByVal strFundProjectName As String _
                                      , ByVal decNewBalance As Decimal _
                                      , ByVal strNotifyAmount As String _
                                      ) As String

        Dim StrMessage As New System.Text.StringBuilder
        Dim blnFlag As Boolean = False
        Try
            StrMessage.Append("<HTML><BODY>")
            StrMessage.Append(vbCrLf)

            If ConfigParameter._Object._Notify_Budget_Users.Trim.Length > 0 Then
                StrMessage.Append("<p><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#333399; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>")


                'Gajanan [27-Mar-2019] -- Start
                'Enhancement - Change Email Language
                'StrMessage.Append("Dear <b>" & strUserName & "</b>,</span></p>")
                StrMessage.Append(Language.getMessage("frmEmployeeMaster", 14, "Dear") & " " & "<b>" & getTitleCase(strUserName) & "</b></span></p>")
                'Gajanan [27-Mar-2019] -- End

                StrMessage.Append(vbCrLf)
                StrMessage.Append("<p><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#333399; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>")

                'Gajanan [27-Mar-2019] -- Start
                'Enhancement - Change Email Language

                'StrMessage.Append("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; This is to inform you that Current Balance is less than Notification amount that was set on Fund Project Code screen</b></span></p>")
                StrMessage.Append(" " & Language.getMessage(mstrModuleName, 15, "This is to inform you that Current Balance is less than Notification amount that was set on Fund Project Code screen") & "</b></span></p>")
                'Gajanan [27-Mar-2019] -- End

                StrMessage.Append(vbCrLf)


                'Gajanan [27-Mar-2019] -- Start
                'Enhancement - Change Email Language
                'StrMessage.Append("<TABLE border='1' style='margin-left:50px;width:502px;font-size:9.0pt;'>")
                StrMessage.Append("<TABLE border='1' style='width:502px;font-size:9.0pt;'>")
                'Gajanan [27-Mar-2019] -- End

                StrMessage.Append("<TR style='background-color:Purple;color:White; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & ";'>")

                StrMessage.Append("<TD align='center' style='width:300px'>")
                StrMessage.Append(Language.getMessage(mstrModuleName, 6, "Fund Project Name"))
                StrMessage.Append("</TD>")

                StrMessage.Append("<TD align='center' style='width:200px'>")
                StrMessage.Append(Language.getMessage(mstrModuleName, 7, "Current Balance"))
                StrMessage.Append("</TD>")

                StrMessage.Append("<TD align='center' style='width:300px'>")
                StrMessage.Append(Language.getMessage(mstrModuleName, 8, "Notification Amount"))
                StrMessage.Append("</TD>")

                StrMessage.Append("</TR>")
                '-----------------------------

                StrMessage.Append("<TR style = 'font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#333399;'>")
                StrMessage.Append("<TD style='width:300px'>")
                StrMessage.Append(strFundProjectName)
                StrMessage.Append("</TD>")

                StrMessage.Append("<TD align='right' style='width:200px'>")
                StrMessage.Append(decNewBalance.ToString(GUI.fmtCurrency))
                StrMessage.Append("</TD>")

                StrMessage.Append("<TD align='right' style='width:300px'>")
                StrMessage.Append(strNotifyAmount)
                StrMessage.Append("</TD>")

                StrMessage.Append("</TR>")
                '-----------------------------


                StrMessage.Append("</TABLE>")

                blnFlag = True




                'Gajanan [27-Mar-2019] -- Start
                'Enhancement - Change Email Language
                'StrMessage.Append("<p><span style='padding-top:25px;font-size:10.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#333399;margin-left:0px;margin-right:0px;margin-top:0px;margin-bottom:10px;'><b>&nbsp;&nbsp;&nbsp;&nbsp;&quot;POWERED BY ARUTI HR & PAYROLL MANAGEMENT SOFTWARE.&quot;</b>")
                StrMessage.Append("<p><span style='padding-top:25px;font-size:10.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#333399;margin-left:0px;margin-right:0px;margin-top:0px;margin-bottom:10px;'><b>&quot;POWERED BY ARUTI HR & PAYROLL MANAGEMENT SOFTWARE.&quot;</b>")
                'Gajanan [27-Mar-2019] -- End

                StrMessage.Append("</span></p>")
                StrMessage.Append("</BODY></HTML>")

                If blnFlag = False Then
                    StrMessage = StrMessage.Remove(0, StrMessage.Length)
                End If
            End If
            Return StrMessage.ToString
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "Set_Notification", mstrModuleName)
            Return ""
        End Try
    End Function

    Private Sub Send_Notification()
        Try

            If gobjEmailList.Count > 0 Then
                Dim objSendMail As New clsSendMail
                For Each obj In gobjEmailList
                    objSendMail._ToEmail = obj._EmailTo
                    objSendMail._Subject = obj._Subject
                    objSendMail._Message = obj._Message
                    objSendMail._FormName = obj._FormName
                    objSendMail._ClientIP = getIP()
                    objSendMail._HostName = getHostName()
                    objSendMail._FromWeb = False
                    objSendMail._AuditUserId = User._Object._Userunkid
objSendMail._CompanyUnkid = Company._Object._Companyunkid
                    objSendMail._AuditDate = ConfigParameter._Object._CurrentDateAndTime
                    objSendMail._LoginEmployeeunkid = -1
                    objSendMail._OperationModeId = enLogin_Mode.DESKTOP
                    objSendMail._UserUnkid = User._Object._Userunkid
                    objSendMail._SenderAddress = User._Object._Email
                    objSendMail._ModuleRefId = clsSendMail.enAT_VIEW_TYPE.PAYROLL_MGT
                    Try
                        'Sohail (30 Nov 2017) -- Start
                        'SUMATRA Enhancement – SUMATRA – issue # 0001669: Additional feature for Exchange Server configuration setting in 70.1.
                        'objSendMail.SendMail()
                        objSendMail.SendMail(Company._Object._Companyunkid)
                        'Sohail (30 Nov 2017) -- End
                    Catch ex As Exception

                    End Try
                Next
                gobjEmailList.Clear()
            End If
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "Send_Notification", mstrModuleName)
        Finally
            If gobjEmailList.Count > 0 Then
                gobjEmailList.Clear()
            End If
        End Try
    End Sub

#End Region

#Region " Button's Events "

    Private Sub btnNew_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnNew.Click
        Dim frm As New frmFundAdjustment_AddEdit
        Try
            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If

            If frm.displayDialog(-1, enAction.ADD_CONTINUE) Then
                Call FillList()
                Call FillCombo()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnNew_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnEdit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnEdit.Click
        Dim frm As New frmFundAdjustment_AddEdit
        Try
            If dgvFundAdjustment.RowCount <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please select atleast one row for further operation."), enMsgBoxStyle.Information)
                dgvFundAdjustment.Focus()
                Exit Sub
                'Sohail (16 Jun 2017) -- Start
                'Enhancement - 68.1 - C/F tables on close year.
            ElseIf CDate(dgvFundAdjustment.SelectedRows(0).Cells(dgcolhTarnsactionDate.Index).Value).Date < FinancialYear._Object._Database_Start_Date.Date Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 13, "Sorry, You cannot edit/delete this transaction. Reason: This is previous year transaction."), enMsgBoxStyle.Information)
                dgvFundAdjustment.Focus()
                Exit Sub
                'Sohail (16 Jun 2017) -- End
            End If

            If frm.displayDialog(CInt(dgvFundAdjustment.SelectedRows(0).Cells(objdgcolhfundadjustmentunkid.Index).Value), enAction.EDIT_ONE) Then
                Call FillList()
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnEdit_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDelete.Click
        Dim objFundActivity As New clsfundactivity_Tran
        Dim objFund As New clsFundProjectCode
        Try
            If dgvFundAdjustment.SelectedRows.Count <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please select atleast one row for further operation."), enMsgBoxStyle.Information)
                dgvFundAdjustment.Focus()
                Exit Try
                'Sohail (16 Jun 2017) -- Start
                'Enhancement - 68.1 - C/F tables on close year.
            ElseIf CDate(dgvFundAdjustment.SelectedRows(0).Cells(dgcolhTarnsactionDate.Index).Value).Date < FinancialYear._Object._Database_Start_Date.Date Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 13, "Sorry, You cannot edit/delete this transaction. Reason: This is previous year transaction."), enMsgBoxStyle.Information)
                dgvFundAdjustment.Focus()
                Exit Sub
                'Sohail (16 Jun 2017) -- End
            End If

            'Sohail (22 Nov 2016) -- Start
            'Enhancement #24 -  65.1 - Increase / decrease fund project code balance on activity adjustment as per comment given by andrew.
            'Dim mdecBalance As Decimal = 0
            'Dim strProjectCode As String = ""
            'Dim mdecActivityTotal As Decimal = objFundActivity.GetFundActivityTotal(CInt(dgvFundAdjustment.SelectedRows(0).Cells(objdgcolhfundProjectCodeunkid.Index).Value), mdecBalance, strProjectCode)

            'If mdecActivityTotal > CDec(dgvFundAdjustment.SelectedRows(0).Cells(dgcolhCurrentBal.Index).Value) Then
            '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Sorry, Total Amount of all activities in selected Fund Project Code should not be greater than selected Fund Project Code ceiling amount.") & vbCrLf & vbCrLf & strProjectCode & " Project Code Expected Balance : " & Format(CDec(dgvFundAdjustment.SelectedRows(0).Cells(dgcolhCurrentBal.Index).Value), GUI.fmtCurrency) & vbCrLf & "Total of Activities in the Project Code : " & Format(mdecActivityTotal, GUI.fmtCurrency), enMsgBoxStyle.Information) '?1
            '    Exit Try
            'End If
            Dim dtTransDate As Date = CDate(dgvFundAdjustment.SelectedRows(0).Cells(dgcolhTarnsactionDate.Index).Value)
            Dim objFundAdj As New clsFundAdjustment_Tran
            If objFundAdj.isLastTransaction(CInt(dgvFundAdjustment.SelectedRows(0).Cells(objdgcolhfundProjectCodeunkid.Index).Value), dtTransDate, CInt(dgvFundAdjustment.SelectedRows(0).Cells(objdgcolhfundadjustmentunkid.Index).Value)) = False Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 11, "Please define another Transaction date. Reason: Transaction date must be greater than last fund adjustments date.") & " " & dtTransDate.ToShortDateString, enMsgBoxStyle.Information)
                Exit Try
            End If

            'Sohail (23 May 2017) -- Start
            'Enhancement - 67.1 - Link budget with Payroll.
            'Dim objFundActAdj As New clsFundActivityAdjustment_Tran
            'If objFundActAdj.isLastTransaction(objFundActAdj._FundActivityunkid, dtTransDate, 0) = False Then
            '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 12, "Please define another Transaction date. Reason: Transaction date must be greater than last activity adjustments date.") & " " & dtTransDate.ToShortDateString, enMsgBoxStyle.Information)
            '    Exit Try
            'End If
            'Sohail (23 May 2017) -- End

            'Sohail (22 Nov 2016) -- End

            objFund._FundProjectCodeunkid = CInt(dgvFundAdjustment.SelectedRows(0).Cells(objdgcolhfundProjectCodeunkid.Index).Value)
            Dim decPerc As Decimal = 100
            If ConfigParameter._Object._Notify_Budget_FundSourcePercentage > 0 Then
                decPerc = ConfigParameter._Object._Notify_Budget_FundSourcePercentage
            End If
            If ((objFund._Notify_Amount * decPerc) / 100) > CDec(dgvFundAdjustment.SelectedRows(0).Cells(dgcolhCurrentBal.Index).Value) Then
                If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "New Balance is going to be less than Notification amount set on Fund Project Code screen.") & vbCrLf & vbCrLf & Language.getMessage(mstrModuleName, 5, "Do you want to continue?"), CType(enMsgBoxStyle.Information + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.No Then '?1
                    Exit Try
                End If
            End If

            If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Are you sure you want to Delete this Fund Project Code Adjustment?"), CType(enMsgBoxStyle.Question + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.Yes Then

                If gobjEmailList.Count > 0 AndAlso ConfigParameter._Object._Notify_Budget_Users.Trim.Length > 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 10, "Sending Email(s) process is in progress from other module. Please wait."), enMsgBoxStyle.Information)
                    Exit Try
                End If

                objFundAdjustment = New clsFundAdjustment_Tran

                Dim frm As New frmReasonSelection
                Dim mstrVoidReason As String = String.Empty
                frm.displayDialog(enVoidCategoryType.PAYROLL, mstrVoidReason)
                If mstrVoidReason.Length <= 0 Then
                    Exit Sub
                Else
                    objFundAdjustment._Voidreason = mstrVoidReason
                End If
                frm = Nothing

                objFundAdjustment._Isvoid = True
                objFundAdjustment._Voiddatetime = ConfigParameter._Object._CurrentDateAndTime
                objFundAdjustment._Voiduserunkid = User._Object._Userunkid


                'S.SANDEEP [28-May-2018] -- START
                'ISSUE/ENHANCEMENT : {Audit Trails} 
                objFundAdjustment._FormName = mstrModuleName
                objFundAdjustment._LoginEmployeeunkid = 0
                objFundAdjustment._ClientIP = getIP()
                objFundAdjustment._HostName = getHostName()
                objFundAdjustment._FromWeb = False
                objFundAdjustment._AuditUserId = User._Object._Userunkid
objFundAdjustment._CompanyUnkid = Company._Object._Companyunkid
                objFundAdjustment._AuditDate = ConfigParameter._Object._CurrentDateAndTime
                'S.SANDEEP [28-May-2018] -- END

                If objFundAdjustment.Delete(CInt(dgvFundAdjustment.SelectedRows(0).Cells(objdgcolhfundadjustmentunkid.Index).Value), _
                                            ConfigParameter._Object._CurrentDateAndTime, Nothing) Then


                    If ConfigParameter._Object._Notify_Budget_Users.Trim.Length > 0 Then
                        gobjEmailList = New List(Of clsEmailCollection)
                        Dim objUsr As New clsUserAddEdit : Dim StrMessage As String = String.Empty

                        For Each sId As String In ConfigParameter._Object._Notify_Budget_Users.Split(CChar(","))
                            If sId.Trim = "" Then Continue For

                            objUsr._Userunkid = CInt(sId)
                            Dim strUserName As String = objUsr._Firstname & " " & objUsr._Lastname
                            If strUserName.Trim = "" Then strUserName = objUsr._Username

                            'StrMessage = Set_Notification(objUsr._Firstname & " " & objUsr._Lastname, dgvFundAdjustment.SelectedRows(0).Cells(dgcolhFundName.Index).Value.ToString, CDec(dgvFundAdjustment.SelectedRows(0).Cells(dgcolhCurrentBal.Index).Value), Format((objFund._Notify_Amount * decPerc) / 100, GUI.fmtCurrency) & " (" & decPerc.ToString & " % of " & Format(objFund._Notify_Amount, GUI.fmtCurrency) & ")")
                            StrMessage = Set_Notification(strUserName, dgvFundAdjustment.SelectedRows(0).Cells(dgcolhFundProjectCode.Index).Value.ToString, CDec(dgvFundAdjustment.SelectedRows(0).Cells(dgcolhCurrentBal.Index).Value), Format((objFund._Notify_Amount * decPerc) / 100, GUI.fmtCurrency))

                            gobjEmailList.Add(New clsEmailCollection(objUsr._Email, dgvFundAdjustment.SelectedRows(0).Cells(dgcolhFundProjectCode.Index).Value.ToString & " " & Language.getMessage(mstrModuleName, 9, "Current Balance is getting low."), StrMessage, "", 0, "", "", 0, 0, 0, ""))
                        Next
                        objUsr = Nothing

                        trd = New Thread(AddressOf Send_Notification)
                        trd.IsBackground = True
                        trd.Start()
                    End If

                    Call FillList()
                    Call FillCombo()
                Else
                    If objFundAdjustment._Message <> "" Then
                        eZeeMsgBox.Show(objFundAdjustment._Message, enMsgBoxStyle.Information)
                        Exit Sub
                    End If
                End If

            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnDelete_Click", mstrModuleName)
        Finally
            objFundActivity = Nothing
        End Try
    End Sub

    Private Sub btnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnClose_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnSearchFundName_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchProjectCode.Click
        Try
            Dim objfrm As New frmCommonSearch

            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            With objfrm
                .ValueMember = cboProjectCode.ValueMember
                .DisplayMember = cboProjectCode.DisplayMember
                .DataSource = CType(cboProjectCode.DataSource, DataTable)
                .CodeMember = "fundcode"
            End With

            If objfrm.DisplayDialog Then
                cboProjectCode.SelectedValue = objfrm.SelectedValue
                cboProjectCode.Focus()
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchFundName_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnSearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearch.Click
        Try
            Call FillList()

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearch_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnReset.Click
        Try
            Call ResetParameters()
            Call FillList()

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnReset_Click", mstrModuleName)
        End Try
    End Sub

#End Region

#Region "DataGrid's Events"

    Private Sub dgvFundAdjustment_SelectionChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles dgvFundAdjustment.SelectionChanged
        Try
            If dgvFundAdjustment.SelectedRows.Count > 0 Then

                Dim blnEnable = CType(dgvFundAdjustment.DataSource, DataTable).AsEnumerable().Where(Function(x) x.Field(Of Date)("transactiondate") > _
                               CDate(dgvFundAdjustment.SelectedRows(0).Cells(dgcolhTarnsactionDate.Index).Value) _
                               And x.Field(Of Integer)("fundProjectCodeunkid") = CInt(dgvFundAdjustment.SelectedRows(0).Cells(objdgcolhfundProjectCodeunkid.Index).Value)).Count <= 0

                btnEdit.Enabled = blnEnable
                btnDelete.Enabled = blnEnable
            End If


        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgvFundAdjustment_SelectionChanged", mstrModuleName)
        End Try
    End Sub

#End Region



    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Private Sub OtherSettings()
        Try
            Me.SuspendLayout()
           
            Call SetLanguage()

            Me.gbFilterCriteria.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbFilterCriteria.ForeColor = GUI._eZeeContainerHeaderForeColor


            Me.btnDelete.GradientBackColor = GUI._ButttonBackColor
            Me.btnDelete.GradientForeColor = GUI._ButttonFontColor

            Me.btnEdit.GradientBackColor = GUI._ButttonBackColor
            Me.btnEdit.GradientForeColor = GUI._ButttonFontColor

            Me.btnNew.GradientBackColor = GUI._ButttonBackColor
            Me.btnNew.GradientForeColor = GUI._ButttonFontColor

            Me.btnClose.GradientBackColor = GUI._ButttonBackColor
            Me.btnClose.GradientForeColor = GUI._ButttonFontColor


            Me.ResumeLayout()
        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "OtherSettings", mstrModuleName)
        End Try
    End Sub


    Private Sub SetLanguage()
        Try
            Me.Text = Language._Object.getCaption(Me.Name, Me.Text)

            Me.gbFilterCriteria.Text = Language._Object.getCaption(Me.gbFilterCriteria.Name, Me.gbFilterCriteria.Text)
            Me.lblFundProjectCode.Text = Language._Object.getCaption(Me.lblFundProjectCode.Name, Me.lblFundProjectCode.Text)
            Me.dgvFundAdjustment.Text = Language._Object.getCaption(Me.dgvFundAdjustment.Name, Me.dgvFundAdjustment.Text)
            Me.btnDelete.Text = Language._Object.getCaption(Me.btnDelete.Name, Me.btnDelete.Text)
            Me.btnEdit.Text = Language._Object.getCaption(Me.btnEdit.Name, Me.btnEdit.Text)
            Me.btnNew.Text = Language._Object.getCaption(Me.btnNew.Name, Me.btnNew.Text)
            Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
            Me.lblTranFromDt.Text = Language._Object.getCaption(Me.lblTranFromDt.Name, Me.lblTranFromDt.Text)
            Me.lblTranToDt.Text = Language._Object.getCaption(Me.lblTranToDt.Name, Me.lblTranToDt.Text)
            Me.lblCurrentBalTo.Text = Language._Object.getCaption(Me.lblCurrentBalTo.Name, Me.lblCurrentBalTo.Text)
            Me.lblCurrentBalFrom.Text = Language._Object.getCaption(Me.lblCurrentBalFrom.Name, Me.lblCurrentBalFrom.Text)
            Me.dgcolhFundProjectCode.HeaderText = Language._Object.getCaption(Me.dgcolhFundProjectCode.Name, Me.dgcolhFundProjectCode.HeaderText)
            Me.dgcolhTarnsactionDate.HeaderText = Language._Object.getCaption(Me.dgcolhTarnsactionDate.Name, Me.dgcolhTarnsactionDate.HeaderText)
            Me.dgcolhCurrentBal.HeaderText = Language._Object.getCaption(Me.dgcolhCurrentBal.Name, Me.dgcolhCurrentBal.HeaderText)
            Me.dgcolhIncrDecrAmount.HeaderText = Language._Object.getCaption(Me.dgcolhIncrDecrAmount.Name, Me.dgcolhIncrDecrAmount.HeaderText)
            Me.dgcolhNewBalance.HeaderText = Language._Object.getCaption(Me.dgcolhNewBalance.Name, Me.dgcolhNewBalance.HeaderText)

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
        End Try
    End Sub


    Private Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Please select atleast one row for further operation.")
            Language.setMessage(mstrModuleName, 3, "Are you sure you want to Delete this Fund Project Code Adjustment?")
            Language.setMessage(mstrModuleName, 4, "New Balance is going to be less than Notification amount set on Fund Project Code screen.")
            Language.setMessage(mstrModuleName, 5, "Do you want to continue?")
            Language.setMessage(mstrModuleName, 6, "Fund Project Name")
            Language.setMessage(mstrModuleName, 7, "Current Balance")
            Language.setMessage(mstrModuleName, 8, "Notification Amount")
            Language.setMessage(mstrModuleName, 9, "Current Balance is getting low.")
            Language.setMessage(mstrModuleName, 10, "Sending Email(s) process is in progress from other module. Please wait.")
            Language.setMessage(mstrModuleName, 11, "Please define another Transaction date. Reason: Transaction date must be greater than last fund adjustments date.")
			Language.setMessage(mstrModuleName, 13, "Sorry, You cannot edit/delete this transaction. Reason: This is previous year transaction.")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class