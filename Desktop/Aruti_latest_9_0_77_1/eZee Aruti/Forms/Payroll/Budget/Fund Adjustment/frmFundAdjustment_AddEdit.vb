﻿Option Strict On

Imports eZeeCommonLib
Imports Aruti.Data
Imports System.Threading

Public Class frmFundAdjustment_AddEdit

#Region " Private Variables "
    Private ReadOnly mstrModuleName As String = "frmFundAdjustment_AddEdit"
    Private mblnCancel As Boolean = True
    Private menAction As enAction = enAction.ADD_ONE
    Private objFundAdjustment As clsFundAdjustment_Tran

    Private mintFundAdjustmentunkid As Integer = -1
    Private mintLastCurrentBal As Decimal = 0
    Private blnSendNotification As Boolean = False
    Private trd As Thread
#End Region

#Region " Display Dialog "

    Public Function displayDialog(ByVal intFundAdjustmentunkid As Integer, ByVal eAction As enAction) As Boolean
        Try
            mintFundAdjustmentunkid = intFundAdjustmentunkid
            menAction = eAction
            Me.ShowDialog()
            Return Not mblnCancel

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "displayDialog", mstrModuleName)
        End Try
    End Function

#End Region

#Region " Form's Events "

    Private Sub frmFundAdjustment_AddEdit_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        objFundAdjustment = Nothing
    End Sub

    Private Sub frmFundAdjustment_AddEdit_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown
        Try
            If e.Control = True And e.KeyCode = Windows.Forms.Keys.S Then
                Call btnSave.PerformClick()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmFundAdjustment_AddEdit_KeyDown", mstrModuleName)
        End Try
    End Sub

    Private Sub frmFundAdjustment_AddEdit_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles Me.KeyPress
        Try
            If Asc(e.KeyChar) = 13 Then
                Windows.Forms.SendKeys.Send("{Tab}")
                e.Handled = True
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmFundAdjustment_AddEdit_KeyPress", mstrModuleName)
        End Try
    End Sub

    Private Sub frmFundAdjustment_AddEdit_LanguageClick(ByVal sender As Object, ByVal eventArgs As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsFundAdjustment_Tran.SetMessages()
            objfrm._Other_ModuleNames = "clsFundAdjustment_Tran"
            objfrm.displayDialog(Me)

            Call SetLanguage()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmFundAdjustment_AddEdit_LanguageClick", mstrModuleName)
        End Try
    End Sub

    Private Sub frmFundAdjustment_AddEdit_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            objFundAdjustment = New clsFundAdjustment_Tran

            Call Set_Logo(Me, gApplicationType)
            Language.setLanguage(Me.Name)
            Call OtherSettings()
            Call SetColor()

            Call FillCombo()

            If menAction = enAction.EDIT_ONE Then
                objFundAdjustment._FundAdjustmentunkid = mintFundAdjustmentunkid
                cboFundProjectName.Enabled = False
                dtTransactionDate.Enabled = False
                objbtnSearchFundName.Enabled = False
                Call GetValue()
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmFundAdjustment_AddEdit_Load", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Private Methods "

    Private Sub FillCombo()
        Try
            Dim dsList As New DataSet
            Dim objFundProjectCode As New clsFundProjectCode

            dsList = objFundProjectCode.GetComboList("List", True)

            With cboFundProjectName
                .ValueMember = "fundprojectcodeunkid"
                .DisplayMember = "fundprojectname"
                .DataSource = dsList.Tables("List")
                .SelectedValue = 0
            End With

            objFundProjectCode = Nothing
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        End Try
    End Sub

    Private Sub SetColor()
        Try
            txtCurrentBal.BackColor = GUI.ColorOptional
            txtIncrDecrAmount.BackColor = GUI.ColorComp
            txtNewBalance.BackColor = GUI.ColorOptional
            txtRemark.BackColor = GUI.ColorOptional
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetColor", mstrModuleName)
        End Try
    End Sub

    Private Sub GetValue()
        Try
            cboFundProjectName.SelectedValue = objFundAdjustment._FundProjectCodeunkid
            'RemoveHandler cboFundName.SelectedIndexChanged, AddressOf cboFundName_SelectedIndexChanged
            If menAction = enAction.EDIT_ONE Then
                dtTransactionDate.Value = objFundAdjustment._TransactionDate.Date
            End If
            txtCurrentBal.Text = Format(objFundAdjustment._CurrentBalance, GUI.fmtCurrency)
            txtIncrDecrAmount.Text = Format(objFundAdjustment._IncrDecrAmount, GUI.fmtCurrency)
            txtNewBalance.Text = Format(objFundAdjustment._NewBalance, GUI.fmtCurrency)
            txtRemark.Text = objFundAdjustment._Remark
            'AddHandler cboFundName.SelectedIndexChanged, AddressOf cboFundName_SelectedIndexChanged

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetValue", mstrModuleName)
        End Try
    End Sub

    Private Sub SetValue()
        Try
            objFundAdjustment._FundProjectCodeunkid = CInt(cboFundProjectName.SelectedValue)
            objFundAdjustment._TransactionDate = dtTransactionDate.Value
            objFundAdjustment._CurrentBalance = txtCurrentBal.Decimal
            objFundAdjustment._IncrDecrAmount = txtIncrDecrAmount.Decimal
            objFundAdjustment._NewBalance = txtNewBalance.Decimal
            objFundAdjustment._Remark = txtRemark.Text.Trim
            objFundAdjustment._Userunkid = User._Object._Userunkid

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetValue", mstrModuleName)
        End Try
    End Sub

    Private Function IsValidate() As Boolean
        Dim objFundActivity As New clsfundactivity_Tran
        Dim objFundProjectCode As New clsFundProjectCode
        Try
            If CInt(cboFundProjectName.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Fund Project Name is compulsory information."), enMsgBoxStyle.Information)
                cboFundProjectName.Focus()
                Return False
            End If

            If txtIncrDecrAmount.Decimal = 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Please define Increment / Decrement amount greater or less than 0."), enMsgBoxStyle.Information)
                txtIncrDecrAmount.Focus()
                Return False
            End If

            If CDec(txtCurrentBal.Decimal + txtIncrDecrAmount.Decimal) < 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "The Calculated balance after increment / decrement amount should not be less than 0."), enMsgBoxStyle.Information)
                txtIncrDecrAmount.Focus()
                Return False
            End If

            'Sohail (16 Jun 2017) -- Start
            'Enhancement - 68.1 - C/F tables on close year.
            If dtTransactionDate.Value.Date < FinancialYear._Object._Database_Start_Date.Date OrElse dtTransactionDate.Value.Date > FinancialYear._Object._Database_End_Date.Date Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 12, "Sorry, Transaction date should be in between financial year start and end date."), enMsgBoxStyle.Information)
                dtTransactionDate.Focus()
                Return False
            End If
            'Sohail (16 Jun 2017) -- End


            'Sohail (22 Nov 2016) -- Start
            'Enhancement #24 -  65.1 - Increase / decrease fund project code balance on activity adjustment as per comment given by andrew.
            'Dim mdecBalance As Decimal = 0
            'Dim strProjectCode As String = ""
            'Dim mdecActivityTotal As Decimal = objFundActivity.GetFundActivityTotal(CInt(cboFundProjectName.SelectedValue), mdecBalance, strProjectCode)

            'If mdecActivityTotal > ((mdecBalance - objFundAdjustment._IncrDecrAmount) + txtIncrDecrAmount.Decimal) Then
            '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Sorry, Total Amount of all activities in selected Fund Project Code should not be greater than selected Fund Project Code ceiling amount.") & vbCrLf & vbCrLf & strProjectCode & " Project Code Expected Balance : " & Format(((mdecBalance - objFundAdjustment._IncrDecrAmount) + txtIncrDecrAmount.Decimal), GUI.fmtCurrency) & vbCrLf & "Total of Activities in the Project Code : " & Format(mdecActivityTotal, GUI.fmtCurrency), enMsgBoxStyle.Information) '?1
            '    txtIncrDecrAmount.Focus()
            '    Return False
            'End If
            'Sohail (22 Nov 2016) -- End

            objFundProjectCode._FundProjectCodeunkid = CInt(cboFundProjectName.SelectedValue)
            Dim decPerc As Decimal = 100
            If ConfigParameter._Object._Notify_Budget_FundSourcePercentage > 0 Then
                decPerc = ConfigParameter._Object._Notify_Budget_FundSourcePercentage
            End If
            If ((objFundProjectCode._Notify_Amount * decPerc) / 100) > (txtNewBalance.Decimal) Then
                If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "New Balance is going to be less than Notification amount set on Fund Project Code screen.") & vbCrLf & vbCrLf & Language.getMessage(mstrModuleName, 6, "Do you want to continue?"), CType(enMsgBoxStyle.Information + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.No Then '?1
                    txtIncrDecrAmount.Focus()
                    Return False
                Else
                    blnSendNotification = True
                End If
            End If

            If gobjEmailList.Count > 0 AndAlso ConfigParameter._Object._Notify_Budget_Users.Trim.Length > 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 10, "Sending Email(s) process is in progress from other module. Please wait."), enMsgBoxStyle.Information)
                Return False
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "IsValidate", mstrModuleName)
        Finally
            objFundActivity = Nothing
            objFundProjectCode = Nothing
        End Try
        Return True
    End Function

    Private Function Set_Notification(ByVal strUserName As String _
                                      , ByVal strFundName As String _
                                      , ByVal decNewBalance As Decimal _
                                      , ByVal strNotifyAmount As String _
                                      ) As String

        Dim StrMessage As New System.Text.StringBuilder
        Dim blnFlag As Boolean = False
        Try
            StrMessage.Append("<HTML><BODY>")
            StrMessage.Append(vbCrLf)

            If ConfigParameter._Object._Notify_Budget_Users.Trim.Length > 0 Then
                StrMessage.Append("<p><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#333399; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>")

                'Gajanan [27-Mar-2019] -- Start
                'Enhancement - Change Email Language
                'StrMessage.Append("Dear <b>" & strUserName & "</b>,</span></p>")
                StrMessage.Append(Language.getMessage(mstrModuleName, 13, "Dear") & " " & "<b>" & getTitleCase(strUserName) & "</b></span></p>")
                'Gajanan [27-Mar-2019] -- End

                StrMessage.Append(vbCrLf)
                StrMessage.Append("<p><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#333399; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>")

                'Gajanan [27-Mar-2019] -- Start
                'Enhancement - Change Email Language

                'StrMessage.Append("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; This is to inform you that Current Balance is less than Notification amount that was set on Fund Project Code screen.</b></span></p>")
                StrMessage.Append(Language.getMessage(mstrModuleName, 14, "This is to inform you that Current Balance is less than Notification amount that was set on Fund Project Code screen.") & "</b></span></p>")
                'Gajanan [27-Mar-2019] -- End

                StrMessage.Append(vbCrLf)


                'Gajanan [27-Mar-2019] -- Start
                'Enhancement - Change Email Language

                'StrMessage.Append("<TABLE border='1' style='margin-left:50px;width:502px;font-size:9.0pt;'>")
                StrMessage.Append("<TABLE border='1' style='width:502px;font-size:9.0pt;'>")
                'Gajanan [27-Mar-2019] -- End

                StrMessage.Append("<TR style='background-color:Purple;color:White; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & ";'>")

                StrMessage.Append("<TD align='center' style='width:300px'>")
                StrMessage.Append(Language.getMessage(mstrModuleName, 11, "Fund Name"))
                StrMessage.Append("</TD>")

                StrMessage.Append("<TD align='center' style='width:200px'>")
                StrMessage.Append(Language.getMessage(mstrModuleName, 7, "Current Balance"))
                StrMessage.Append("</TD>")

                StrMessage.Append("<TD align='center' style='width:300px'>")
                StrMessage.Append(Language.getMessage(mstrModuleName, 8, "Notification Amount"))
                StrMessage.Append("</TD>")

                StrMessage.Append("</TR>")
                '-----------------------------

                StrMessage.Append("<TR style = 'font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#333399;'>")
                StrMessage.Append("<TD style='width:300px'>")
                StrMessage.Append(strFundName)
                StrMessage.Append("</TD>")

                StrMessage.Append("<TD align='right' style='width:200px'>")
                StrMessage.Append(decNewBalance.ToString(GUI.fmtCurrency))
                StrMessage.Append("</TD>")

                StrMessage.Append("<TD align='right' style='width:300px'>")
                StrMessage.Append(strNotifyAmount)
                StrMessage.Append("</TD>")

                StrMessage.Append("</TR>")
                '-----------------------------


                StrMessage.Append("</TABLE>")

                blnFlag = True


                'Gajanan [27-Mar-2019] -- Start
                'Enhancement - Change Email Language
                'StrMessage.Append("<p><span style='padding-top:25px;font-size:10.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#333399;margin-left:0px;margin-right:0px;margin-top:0px;margin-bottom:10px;'><b>&nbsp;&nbsp;&nbsp;&nbsp;&quot;POWERED BY ARUTI HR & PAYROLL MANAGEMENT SOFTWARE.&quot;</b>")
                StrMessage.Append("<p><center style='padding-top:25px;font-size:10.0pt;font-family:&quot;Verdana&quot;,&quot;Sans-Serif&quot;;color:#333399;margin-left:0px;margin-right:0px;margin-top:0px;margin-bottom:10px'><b>&quot;POWERED BY ARUTI HR &amp; PAYROLL MANAGEMENT SOFTWARE.&quot;</b></center></p>")

                'Gajanan [27-Mar-2019] -- End

                StrMessage.Append("</span></p>")
                StrMessage.Append("</BODY></HTML>")

                If blnFlag = False Then
                    StrMessage = StrMessage.Remove(0, StrMessage.Length)
                End If
            End If
            Return StrMessage.ToString
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "Set_Notification", mstrModuleName)
            Return ""
        End Try
    End Function

    Private Sub Send_Notification()
        Try

            If gobjEmailList.Count > 0 Then
                Dim objSendMail As New clsSendMail
                For Each obj In gobjEmailList
                    objSendMail._ToEmail = obj._EmailTo
                    objSendMail._Subject = obj._Subject
                    objSendMail._Message = obj._Message
                    objSendMail._FormName = obj._FormName
                    objSendMail._ClientIP = getIP()
                    objSendMail._HostName = getHostName()
                    objSendMail._FromWeb = False
                    objSendMail._AuditUserId = User._Object._Userunkid
objSendMail._CompanyUnkid = Company._Object._Companyunkid
                    objSendMail._AuditDate = ConfigParameter._Object._CurrentDateAndTime
                    objSendMail._LoginEmployeeunkid = -1
                    objSendMail._OperationModeId = enLogin_Mode.DESKTOP
                    objSendMail._UserUnkid = User._Object._Userunkid
                    objSendMail._SenderAddress = User._Object._Email
                    objSendMail._ModuleRefId = clsSendMail.enAT_VIEW_TYPE.PAYROLL_MGT
                    Try
                        'Sohail (30 Nov 2017) -- Start
                        'SUMATRA Enhancement – SUMATRA – issue # 0001669: Additional feature for Exchange Server configuration setting in 70.1.
                        'objSendMail.SendMail()
                        objSendMail.SendMail(Company._Object._Companyunkid)
                        'Sohail (30 Nov 2017) -- End
                    Catch ex As Exception

                    End Try
                Next
                gobjEmailList.Clear()
            End If
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "Send_Notification", mstrModuleName)
        Finally
            If gobjEmailList.Count > 0 Then
                gobjEmailList.Clear()
            End If
        End Try
    End Sub

#End Region

#Region "Button's Events"

    Private Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Dim blnFlag As Boolean = False
        Try
            blnSendNotification = False
            If IsValidate() = False Then Exit Sub

            Call SetValue()

            'S.SANDEEP [28-May-2018] -- START
            'ISSUE/ENHANCEMENT : {Audit Trails} 
            objFundAdjustment._FormName = mstrModuleName
            objFundAdjustment._LoginEmployeeunkid = 0
            objFundAdjustment._ClientIP = getIP()
            objFundAdjustment._HostName = getHostName()
            objFundAdjustment._FromWeb = False
            objFundAdjustment._AuditUserId = User._Object._Userunkid
objFundAdjustment._CompanyUnkid = Company._Object._Companyunkid
            objFundAdjustment._AuditDate = ConfigParameter._Object._CurrentDateAndTime
            'S.SANDEEP [28-May-2018] -- END

            If menAction = enAction.EDIT_ONE Then
                blnFlag = objFundAdjustment.Update(ConfigParameter._Object._CurrentDateAndTime, Nothing)
            Else
                blnFlag = objFundAdjustment.Insert(ConfigParameter._Object._CurrentDateAndTime, Nothing)
            End If

            If blnFlag = False And objFundAdjustment._Message <> "" Then
                eZeeMsgBox.Show(objFundAdjustment._Message, enMsgBoxStyle.Information)
                Exit Sub
            End If

            If blnFlag = True AndAlso blnSendNotification = True Then
                If ConfigParameter._Object._Notify_Budget_Users.Trim.Length > 0 Then
                    gobjEmailList = New List(Of clsEmailCollection)
                    Dim objUsr As New clsUserAddEdit : Dim StrMessage As String = String.Empty

                    Dim objFundProjectCode As New clsFundProjectCode
                    objFundProjectCode._FundProjectCodeunkid = CInt(cboFundProjectName.SelectedValue)
                    Dim decPerc As Decimal = 100
                    If ConfigParameter._Object._Notify_Budget_FundSourcePercentage > 0 Then
                        decPerc = ConfigParameter._Object._Notify_Budget_FundSourcePercentage
                    End If

                    For Each sId As String In ConfigParameter._Object._Notify_Budget_Users.Split(CChar(","))
                        If sId.Trim = "" Then Continue For

                        objUsr._Userunkid = CInt(sId)
                        Dim strUserName As String = objUsr._Firstname & " " & objUsr._Lastname
                        If strUserName.Trim = "" Then strUserName = objUsr._Username

                        'StrMessage = Set_Notification(objUsr._Firstname & " " & objUsr._Lastname, cboFundName.Text, txtNewBalance.Decimal, Format((objFund._Notify_Amount * decPerc) / 100, GUI.fmtCurrency) & " (" & decPerc.ToString & " % of " & Format(objFund._Notify_Amount, GUI.fmtCurrency) & ")")
                        StrMessage = Set_Notification(strUserName, cboFundProjectName.Text, txtNewBalance.Decimal, Format((objFundProjectCode._Notify_Amount * decPerc) / 100, GUI.fmtCurrency))

                        gobjEmailList.Add(New clsEmailCollection(objUsr._Email, cboFundProjectName.Text & " " & Language.getMessage(mstrModuleName, 9, "Current Balance is getting low."), StrMessage, "", 0, "", "", 0, 0, 0, ""))
                    Next
                    objUsr = Nothing

                    trd = New Thread(AddressOf Send_Notification)
                    trd.IsBackground = True
                    trd.Start()
                End If
            End If

            If blnFlag = True Then
                mblnCancel = False
                If menAction = enAction.ADD_CONTINUE Then
                    objFundAdjustment = New clsFundAdjustment_Tran
                    cboFundProjectName.SelectedValue = 0
                    txtRemark.Text = ""
                    'Call GetValue()
                Else
                    mintFundAdjustmentunkid = objFundAdjustment._FundAdjustmentunkid
                    Me.Close()
                End If
            End If




        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "", mstrModuleName)
        End Try
    End Sub

    Private Sub btnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnClose_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnSearchFundName_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchFundName.Click
        Try
            Dim objfrm As New frmCommonSearch

            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            With objfrm
                .ValueMember = cboFundProjectName.ValueMember
                .DisplayMember = cboFundProjectName.DisplayMember
                .DataSource = CType(cboFundProjectName.DataSource, DataTable)
                .CodeMember = "fundprojectcode"
            End With

            If objfrm.DisplayDialog Then
                cboFundProjectName.SelectedValue = objfrm.SelectedValue
                cboFundProjectName.Focus()
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchFundName_Click", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " ComboBox's Events "

    Private Sub cboFundProjectName_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboFundProjectName.SelectedIndexChanged
        Dim dsList As New DataSet
        Try
            If CInt(cboFundProjectName.SelectedValue) > 0 Then
                dsList = objFundAdjustment.GetLastCurrentBalance("List", CInt(cboFundProjectName.SelectedValue))
                If dsList.Tables("List") IsNot Nothing And dsList.Tables("List").Rows.Count > 0 Then
                    txtCurrentBal.Decimal = CDec(Format(CDec(dsList.Tables("List").Rows(0).Item("newbalance").ToString), GUI.fmtCurrency))
                    If menAction = enAction.EDIT_ONE Then dtTransactionDate.Value = CDate(dsList.Tables("List").Rows(0).Item("transactiondate")).Date
                Else
                    txtCurrentBal.Decimal = 0
                    txtIncrDecrAmount.Decimal = 0
                End If
            Else
                txtCurrentBal.Decimal = 0
                txtIncrDecrAmount.Decimal = 0
            End If

            Call txtIncrDecrAmount_Validated(txtIncrDecrAmount, New EventArgs())

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboFundProjectName_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " TextBox's Events "

    Private Sub txtIncrDecrAmount_Validated(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtIncrDecrAmount.Validated
        Try
            txtNewBalance.Decimal = CDec(txtCurrentBal.Decimal + txtIncrDecrAmount.Decimal)

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "txtIncrDecrAmount_Validated", mstrModuleName)
        End Try
    End Sub

#End Region

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Private Sub OtherSettings()
        Try
            Me.SuspendLayout()
           
            Call SetLanguage()

            Me.gbProjectCodeAdjustments.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbProjectCodeAdjustments.ForeColor = GUI._eZeeContainerHeaderForeColor


            Me.btnClose.GradientBackColor = GUI._ButttonBackColor
            Me.btnClose.GradientForeColor = GUI._ButttonFontColor

            Me.btnSave.GradientBackColor = GUI._ButttonBackColor
            Me.btnSave.GradientForeColor = GUI._ButttonFontColor


            Me.ResumeLayout()
        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "OtherSettings", mstrModuleName)
        End Try
    End Sub


    Private Sub SetLanguage()
        Try
            Me.Text = Language._Object.getCaption(Me.Name, Me.Text)

            Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
            Me.btnSave.Text = Language._Object.getCaption(Me.btnSave.Name, Me.btnSave.Text)
            Me.gbProjectCodeAdjustments.Text = Language._Object.getCaption(Me.gbProjectCodeAdjustments.Name, Me.gbProjectCodeAdjustments.Text)
            Me.lblNewBalance.Text = Language._Object.getCaption(Me.lblNewBalance.Name, Me.lblNewBalance.Text)
            Me.lblCurrentBal.Text = Language._Object.getCaption(Me.lblCurrentBal.Name, Me.lblCurrentBal.Text)
            Me.lblTransactionDate.Text = Language._Object.getCaption(Me.lblTransactionDate.Name, Me.lblTransactionDate.Text)
            Me.lblFundProjectName.Text = Language._Object.getCaption(Me.lblFundProjectName.Name, Me.lblFundProjectName.Text)
            Me.lblIncDecAmount.Text = Language._Object.getCaption(Me.lblIncDecAmount.Name, Me.lblIncDecAmount.Text)
            Me.lblRemark.Text = Language._Object.getCaption(Me.lblRemark.Name, Me.lblRemark.Text)

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
        End Try
    End Sub


    Private Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Fund Project Name is compulsory information.")
            Language.setMessage(mstrModuleName, 2, "Please define Increment / Decrement amount greater or less than 0.")
            Language.setMessage(mstrModuleName, 3, "The Calculated balance after increment / decrement amount should not be less than 0.")
            Language.setMessage(mstrModuleName, 5, "New Balance is going to be less than Notification amount set on Fund Project Code screen.")
            Language.setMessage(mstrModuleName, 6, "Do you want to continue?")
            Language.setMessage(mstrModuleName, 7, "Current Balance")
            Language.setMessage(mstrModuleName, 8, "Notification Amount")
            Language.setMessage(mstrModuleName, 9, "Current Balance is getting low.")
            Language.setMessage(mstrModuleName, 10, "Sending Email(s) process is in progress from other module. Please wait.")
            Language.setMessage(mstrModuleName, 11, "Fund Name")
	    Language.setMessage(mstrModuleName, 12, "Sorry, Transaction date should be in between financial year start and end date.")
            Language.setMessage(mstrModuleName, 13, "Dear")
			Language.setMessage(mstrModuleName, 14, "This is to inform you that Current Balance is less than Notification amount that was set on Fund Project Code screen.")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class