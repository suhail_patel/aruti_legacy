﻿Option Strict On

#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data

#End Region

Public Class frmImportBudgetCodesMapping

#Region " Private Variables "
    Private ReadOnly mstrModuleName As String = "frmImportBudgetCodesMapping"
    Private dsData As New DataSet
    Private mblnCancel As Boolean = True
    Private dtTable As DataTable
    Private dsDataList As New DataSet
    Private dtColumns As DataTable
    Dim objEmp As clsEmployee_Master

    Private mintBudgetCodesUnkid As Integer = 0
    Private mintBudgetUnkid As Integer = 0
    Private mintPeriodUnkid As Integer = 0
    Private mdtPeriodStartDate As DateTime
    Private mdtPeriodEndDate As DateTime
#End Region

#Region " Properties "

    Public ReadOnly Property _DataTable() As DataTable
        Get
            Return dtTable
        End Get
    End Property

    Public ReadOnly Property _BudgetCodesUnkid() As Integer
        Get
            Return mintBudgetCodesUnkid
        End Get
    End Property

    Public ReadOnly Property _BudgetUnkid() As Integer
        Get
            Return mintBudgetUnkid
        End Get
    End Property

    Public ReadOnly Property _PeriodUnkid() As Integer
        Get
            Return mintPeriodUnkid
        End Get
    End Property
#End Region

#Region " Display Dialog "

    Public Function displayDialog(ByVal dsList As DataSet) As Boolean
        Try
            dsData = dsList
            Me.ShowDialog()
            Return Not mblnCancel
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "displayDialog", mstrModuleName)
        End Try
    End Function

#End Region

#Region " Private Methods "

    Private Sub SetColor()
        Try
            cboEmployeeCode.BackColor = GUI.ColorComp
            cboPeriod.BackColor = GUI.ColorComp
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetColor", mstrModuleName)
        End Try
    End Sub
    Private Sub FilCombo()
        Dim dsCombos As New DataSet
        Dim objPeriod As New clscommom_period_Tran
        Try
            
            'dsCombos = objPeriod.getListForCombo(enModuleReference.Payroll, FinancialYear._Object._YearUnkid, FinancialYear._Object._DatabaseName, FinancialYear._Object._Database_Start_Date, "Period", True, enStatusType.Open)
            dsCombos = objPeriod.getListForCombo(enModuleReference.Payroll, FinancialYear._Object._YearUnkid, FinancialYear._Object._DatabaseName, FinancialYear._Object._Database_Start_Date, "Period", True, )
            With cboPeriod
                .BeginUpdate()
                .ValueMember = "periodunkid"
                .DisplayMember = "name"
                .DataSource = dsCombos.Tables("Period")
                .SelectedValue = 0
                .EndUpdate()
            End With

            dtColumns = New DataTable
            dtColumns.Columns.Add("id", System.Type.GetType("System.Int32")).DefaultValue = 0
            dtColumns.Columns.Add("name", System.Type.GetType("System.String")).DefaultValue = ""

            Dim dr As DataRow
            dr = dtColumns.NewRow
            dr.Item("id") = -1
            dr.Item("name") = Language.getMessage(mstrModuleName, 1, "Select")
            dtColumns.Rows.Add(dr)

            For Each dtCol As DataColumn In dsData.Tables(0).Columns
                dr = dtColumns.NewRow

                dr.Item("id") = dtCol.Ordinal
                dr.Item("name") = dtCol.ColumnName

                dtColumns.Rows.Add(dr)
            Next

            With cboEmployeeCode
                .ValueMember = "id"
                .DisplayMember = "name"
                .DataSource = dtColumns.Copy
                .SelectedIndex = -1
            End With

            With cboBudgetCode
                .ValueMember = "id"
                .DisplayMember = "name"
                .DataSource = dtColumns.Copy
                .SelectedIndex = -1
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FiilCombo", mstrModuleName)
        Finally
            objPeriod = Nothing
        End Try
    End Sub

    Private Sub SetData()
        Try
            dtTable = New DataTable("BudgetCode")

            dtTable.Columns.Add("IsChecked", System.Type.GetType("System.Boolean")).DefaultValue = False
            dtTable.Columns.Add("budgetcodesunkid", System.Type.GetType("System.Int32")).DefaultValue = -1
            dtTable.Columns.Add("budgetunkid", System.Type.GetType("System.Int32")).DefaultValue = -1
            dtTable.Columns.Add("budget_code", System.Type.GetType("System.String")).DefaultValue = ""
            dtTable.Columns("budget_code").Caption = "Budget Code"
            dtTable.Columns.Add("periodunkid", System.Type.GetType("System.Int32")).DefaultValue = -1
            dtTable.Columns.Add("period_code", System.Type.GetType("System.String")).DefaultValue = ""
            dtTable.Columns("period_code").Caption = "Period Code"
            dtTable.Columns.Add("period_name", System.Type.GetType("System.String")).DefaultValue = ""
            dtTable.Columns("period_name").Caption = "Period Name"
            dtTable.Columns.Add("allocationtranunkid", System.Type.GetType("System.Int32")).DefaultValue = -1
            dtTable.Columns.Add("allocationtrancode", System.Type.GetType("System.String")).DefaultValue = ""
            dtTable.Columns("allocationtrancode").Caption = "Code"
            dtTable.Columns.Add("allocationtranname", System.Type.GetType("System.String")).DefaultValue = ""
            dtTable.Columns("allocationtranname").Caption = "Name"

            Dim objProjectCode As New clsFundProjectCode
            Dim ds As DataSet = objProjectCode.GetComboList("ProjectCode", False)
            For Each dsRow As DataRow In ds.Tables(0).Rows
                dtTable.Columns.Add("|_" & dsRow.Item("fundprojectcodeunkid").ToString, System.Type.GetType("System.Decimal")).DefaultValue = -1
                dtTable.Columns.Add("||_" & dsRow.Item("fundprojectcodeunkid").ToString, System.Type.GetType("System.Int32")).DefaultValue = -1
                dtTable.Columns.Add("||__" & dsRow.Item("fundprojectcodeunkid").ToString, System.Type.GetType("System.String")).DefaultValue = ""
                dtTable.Columns("||__" & dsRow.Item("fundprojectcodeunkid").ToString).Caption = "Activity code"
            Next

            dtTable.Columns.Add("rowtypeid", System.Type.GetType("System.Int32")).DefaultValue = 0
            dtTable.Columns.Add("Message", System.Type.GetType("System.String")).DefaultValue = ""

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetData", mstrModuleName)
        End Try
    End Sub

    Private Sub SetupGrid()
        Dim objProjectCode As New clsFundProjectCode
        Dim objActivity As New clsfundactivity_Tran
        Dim ds As DataSet
        Try

            dgvMapping.AutoGenerateColumns = False

            objcolhFundprojectcodeunkid.DataPropertyName = "fundprojectcodeunkid"
            colhProjectCodes.DataPropertyName = "fundprojectcode"

            ds = objProjectCode.GetComboList("ProjectCode", True, )

            With colhPercentage
                .DisplayStyle = DataGridViewComboBoxDisplayStyle.Nothing
                .DisplayMember = "name"
                .ValueMember = "id"
                .DataSource = dtColumns.Copy
                .DataPropertyName = "Percentage"
            End With
            objcolhPercentageid.DataPropertyName = "Percentage"

            Dim dtTable As DataTable = ds.Tables(0).Copy
            dtTable.Rows.RemoveAt(0)
            Dim dtCol As DataColumn

            dtCol = New DataColumn("Percentage", System.Type.GetType("System.Int32"))
            dtCol.DefaultValue = -1
            dtCol.AllowDBNull = False
            dtTable.Columns.Add(dtCol)

            dtCol = New DataColumn("Activity", System.Type.GetType("System.Int32"))
            dtCol.DefaultValue = -1
            dtCol.AllowDBNull = False
            dtTable.Columns.Add(dtCol)

            With colhActivityCode
                .DisplayStyle = DataGridViewComboBoxDisplayStyle.Nothing
                .DisplayMember = "name"
                .ValueMember = "id"
                .DataSource = dtColumns.Copy
                .DataPropertyName = "Activity"
            End With
            objcolhActivityid.DataPropertyName = "Activity"
           
            dgvMapping.DataSource = dtTable
           
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetupGrid", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Form's Events "

    Private Sub frmImportBudgetCodesMapping_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        Try
            objEmp = Nothing
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmImportBudgetCodesMapping_FormClosed", mstrModuleName)
        End Try
    End Sub

    Private Sub frmImportBudgetCodesMapping_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        objEmp = New clsEmployee_Master
        Try
            Call Set_Logo(Me, gApplicationType)

            Language.setLanguage(Me.Name)
            Call OtherSettings()

            Call SetColor()
            Call FilCombo()

            Call SetupGrid()
            Call SetData()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmImportBudgetCodesMapping_Load", mstrModuleName)
        End Try
    End Sub

    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsBudgetcodes_master.SetMessages()
            clsBudgetcodes_Tran.SetMessages()
            objfrm._Other_ModuleNames = "clsBudgetcodes_master, clsBudgetcodes_Tran"

            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub


#End Region

#Region " ComboBox's Events "


    Private Sub cboEmployeeCode_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboEmployeeCode.SelectedIndexChanged, cboBudgetCode.SelectedIndexChanged
        Try
            'If CInt(cboEmployeeCode.SelectedIndex) < 0 OrElse CInt(cboBudgetCode.SelectedIndex) <= 0 Then
            '    dgvMapping.Rows.Clear()
            '    Exit Sub
            'End If
            'Call SetupGrid()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboEmployeeCode_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub cboBudgetCode_Validating(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles cboBudgetCode.Validating, cboEmployeeCode.Validating
        Try
            Dim cb As ComboBox = TryCast(sender, ComboBox)
            If cb IsNot Nothing Then
                If CInt(cb.SelectedValue) > 0 Then
                    If cb.Name = cboEmployeeCode.Name Then
                        If CInt(cb.SelectedValue) = CInt(cboBudgetCode.SelectedValue) Then
                            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Sorry! This Column is already Mapped with Budget Code.") & vbCrLf & Language.getMessage(mstrModuleName, 3, "Please select different Field."), enMsgBoxStyle.Information)
                            cb.SelectedIndex = 0
                            e.Cancel = True
                            Exit Try
                        End If
                    Else
                        If CInt(cb.SelectedValue) = CInt(cboEmployeeCode.SelectedValue) Then
                            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Sorry! This Column is already Mapped with Employee / Allocation Code.") & vbCrLf & Language.getMessage(mstrModuleName, 5, "Please select different Field."), enMsgBoxStyle.Information)
                            cb.SelectedIndex = 0
                            e.Cancel = True
                            Exit Try
                        End If
                    End If

                    For Each dRow As DataGridViewRow In dgvMapping.Rows
                        If Convert.ToInt32(dRow.Cells(colhPercentage.Index).Value) = CInt(cb.SelectedValue) Then
                            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 6, "Sorry! This Column is already Mapped with Project Codes (%) for") & " " & dRow.Cells(colhProjectCodes.Index).Value.ToString & vbCrLf & Language.getMessage(mstrModuleName, 7, "Please select different Field."), enMsgBoxStyle.Information)
                            cb.SelectedIndex = 0
                            e.Cancel = True
                            Exit Try
                        ElseIf Convert.ToInt32(dRow.Cells(colhActivityCode.Index).Value) = CInt(cb.SelectedValue) Then
                            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 8, "Sorry! This Column is already Mapped with Activity for Project Code") & " " & dRow.Cells(colhProjectCodes.Index).Value.ToString & vbCrLf & Language.getMessage(mstrModuleName, 9, "Please select different Field."), enMsgBoxStyle.Information)
                            cb.SelectedIndex = 0
                            e.Cancel = True
                            Exit Try
                        End If
                    Next
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboBudgetCode_Validating", mstrModuleName)
        End Try
    End Sub

    Private Sub cboPeriod_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboPeriod.SelectedIndexChanged
        Dim objPeriod As New clscommom_period_Tran
        Dim dsList As DataSet = Nothing
        Try
            If CInt(cboPeriod.SelectedValue) > 0 Then
                objPeriod._Periodunkid(FinancialYear._Object._DatabaseName) = CInt(cboPeriod.SelectedValue)
                mdtPeriodStartDate = objPeriod._Start_Date
                mdtPeriodEndDate = objPeriod._End_Date
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboPeriod_SelectedIndexChanged", mstrModuleName)
        Finally
            objPeriod = Nothing
            dsList = Nothing
        End Try
    End Sub
#End Region

#Region " Buttons Events "

    Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Try
            mblnCancel = True
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnCancel_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnOk_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnOk.Click
        Dim objPeriod As New clscommom_period_Tran
        Dim objBudgetCode As New clsBudgetcodes_master
        Dim objBudget As New clsBudget_MasterNew
        Dim objProjectCode As New clsFundProjectCode
        Dim objActivity As New clsfundactivity_Tran
        Dim dsEmployee As DataSet = Nothing
        Dim dsAllocation As DataSet = Nothing
        Dim intDefaultBudgetAllocationById As Integer = 0
        Dim dRows() As DataRow

        Dim strAmount As String = ""
        Dim strColumn As String = ""
        Dim objTnA As New clsTnALeaveTran

        Try
            If CInt(cboPeriod.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 10, "Please select Period. Period is mandatory information."), enMsgBoxStyle.Information)
                cboPeriod.Focus()
                Exit Try
            ElseIf CInt(cboBudgetCode.SelectedIndex) < 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 11, "Please select budget code column."), enMsgBoxStyle.Information)
                cboBudgetCode.Focus()
                Exit Try
            ElseIf CInt(cboEmployeeCode.SelectedIndex) < 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 12, "Please select employee code column."), enMsgBoxStyle.Information)
                cboEmployeeCode.Focus()
                Exit Try
            End If

            objPeriod._Periodunkid(FinancialYear._Object._DatabaseName) = CInt(cboPeriod.SelectedValue)
            'If objPeriod._Statusid = enStatusType.Close Then
            '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 13, "Sorry, This period is closed."), enMsgBoxStyle.Information)
            '    cboPeriod.Focus()
            '    Exit Try
            'End If

            Dim blnMapFound As Boolean = True
            For Each dgRow As DataGridViewRow In dgvMapping.Rows
                If Convert.ToInt32(dgRow.Cells(colhPercentage.Index).Value) < 0 Then
                    blnMapFound = False
                    Exit For
                ElseIf Convert.ToInt32(dgRow.Cells(colhActivityCode.Index).Value) < 0 Then
                    blnMapFound = False
                    Exit For
                End If
            Next
            If blnMapFound = False Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 14, "Please Map Project Codes / Activity Code columns."), enMsgBoxStyle.Information)
                dgvMapping.Focus()
                Exit Try
            End If

            mblnCancel = True

            mintPeriodUnkid = CInt(cboPeriod.SelectedValue)

            Dim ds As DataSet = objBudget.GetComboList("Lis", False, True)
            If ds.Tables(0).Rows.Count > 0 Then
                objBudget._Budgetunkid = CInt(ds.Tables(0).Rows(0).Item("budgetunkid"))
                intDefaultBudgetAllocationById = objBudget._Allocationbyid
            Else
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 26, "Sorry, There is no Default budget set. Please set Default budget."), enMsgBoxStyle.Information)
                Exit Try
            End If


            Select Case intDefaultBudgetAllocationById

                Case 0 'Employee

                    'Sohail (24 Mar 2017) -- Start
                    'Enhancement - 65.1 - Active employee as on period start and end date instead of budget date.
                    'dsEmployee = objEmp.GetEmployeeList(FinancialYear._Object._DatabaseName, _
                    '                  User._Object._Userunkid, _
                    '                  FinancialYear._Object._YearUnkid, _
                    '                  Company._Object._Companyunkid, _
                    '                  objBudget._Budget_date, _
                    '                  objBudget._Budget_date, _
                    '                  ConfigParameter._Object._UserAccessModeSetting, _
                    '                  True, False, "Employee", False)
                    dsEmployee = objEmp.GetEmployeeList(FinancialYear._Object._DatabaseName, _
                                      User._Object._Userunkid, _
                                      FinancialYear._Object._YearUnkid, _
                                      Company._Object._Companyunkid, _
                                      mdtPeriodStartDate, _
                                      mdtPeriodEndDate, _
                                      ConfigParameter._Object._UserAccessModeSetting, _
                                      True, False, "Employee", False)
                    'Sohail (24 Mar 2017) -- End

                Case enAnalysisReport.Branch

                    Dim objStation As New clsStation
                    dsAllocation = objStation.GetList("List", True)

                Case enAnalysisReport.Department

                    Dim objDept As New clsDepartment
                    dsAllocation = objDept.GetList("List", True)

                Case enAnalysisReport.Section

                    Dim objSection As New clsSections
                    dsAllocation = objSection.GetList("List", True)

                Case enAnalysisReport.Unit

                    Dim objUnit As New clsUnits
                    dsAllocation = objUnit.GetList("List", True)

                Case enAnalysisReport.Job

                    Dim objJob As New clsJobs
                    dsAllocation = objJob.GetList("List", True)

                Case enAnalysisReport.CostCenter

                    Dim objCostCenter As New clscostcenter_master
                    dsAllocation = objCostCenter.getComboList("List", False)

                Case enAnalysisReport.SectionGroup

                    Dim objSectionGroup As New clsSectionGroup
                    dsAllocation = objSectionGroup.GetList("List", True)

                Case enAnalysisReport.UnitGroup

                    Dim objUnitGroup As New clsUnitGroup
                    dsAllocation = objUnitGroup.GetList("List", True)

                Case enAnalysisReport.Team

                    Dim objTeam As New clsTeams
                    dsAllocation = objTeam.GetList("List", True)

                Case enAnalysisReport.DepartmentGroup

                    Dim objDeptGroup As New clsDepartmentGroup
                    dsAllocation = objDeptGroup.GetList("List", True)

                Case enAnalysisReport.JobGroup

                    Dim objJobGroup As New clsJobGroup
                    dsAllocation = objJobGroup.GetList("List", True)

                Case enAnalysisReport.ClassGroup

                    Dim objClassGroup As New clsClassGroup
                    dsAllocation = objClassGroup.GetList("List", True)

                Case enAnalysisReport.Classs

                    Dim objClass As New clsClass
                    dsAllocation = objClass.GetList("List", True)

                Case enAnalysisReport.GradeGroup

                    Dim objGradeGroup As New clsGradeGroup
                    dsAllocation = objGradeGroup.GetList("List", True)

                Case enAnalysisReport.Grade

                    Dim objGrade As New clsGrade
                    dsAllocation = objGrade.GetList("List", True)

                Case enAnalysisReport.GradeLevel

                    Dim objGradeLevel As New clsGradeLevel
                    dsAllocation = objGradeLevel.GetList("List", True)

                Case Else

            End Select

            For Each dsRow As DataRow In dsData.Tables(0).Rows
                Dim dRow As DataRow
                dRows = Nothing

                Dim intBudgetUnkid As Integer = 0
                Dim intBudgetCodesUnkid As Integer = 0
                Dim intAllocationtranunkid As Integer = 0
                Dim intProjectCodeunkid As Integer = 0
                Dim intFundActivityunkid As Integer = 0

                dRow = dtTable.NewRow

                dRow.Item("periodunkid") = CInt(cboPeriod.SelectedValue)
                dRow.Item("period_code") = objPeriod._Period_Code
                dRow.Item("period_name") = cboPeriod.Text

                If objBudgetCode.isExists("AND bgbudget_master.budget_code = '" & dsRow.Item(cboBudgetCode.Text).ToString.Replace("'", "''") & "' AND bgbudgetcodes_master.periodunkid = " & CInt(cboPeriod.SelectedValue) & " ", intBudgetCodesUnkid, intBudgetUnkid) = False Then

                    If objBudget.isExists("AND bgbudget_master.budget_code = '" & dsRow.Item(cboBudgetCode.Text).ToString.Replace("'", "''") & "' ", intBudgetUnkid) = False Then
                    If CInt(dRow.Item("rowtypeid")) = 0 Then
                        dRow.Item("rowtypeid") = 1 'Budget not found
                        dRow.Item("Message") = Language.getMessage(mstrModuleName, 15, "Budget not found.")
                    End If
                Else
                        If mintBudgetUnkid = 0 AndAlso mintBudgetCodesUnkid = 0 Then
                        mintBudgetCodesUnkid = intBudgetCodesUnkid
                        mintBudgetUnkid = intBudgetUnkid
                    End If
                        If mintBudgetUnkid <> intBudgetUnkid OrElse mintBudgetCodesUnkid <> intBudgetCodesUnkid Then
                            If CInt(dRow.Item("rowtypeid")) = 0 Then
                                dRow.Item("rowtypeid") = 6 'only one budget at a time
                                dRow.Item("Message") = Language.getMessage(mstrModuleName, 16, "Sorry, You can import for only one budget at a time.")
                            End If
                        End If
                    End If
                    
                Else
                    If mintBudgetUnkid = 0 AndAlso mintBudgetCodesUnkid = 0 Then
                        mintBudgetCodesUnkid = intBudgetCodesUnkid
                        mintBudgetUnkid = intBudgetUnkid
                    End If
                    If mintBudgetUnkid <> intBudgetUnkid OrElse mintBudgetCodesUnkid <> intBudgetCodesUnkid Then
                        If CInt(dRow.Item("rowtypeid")) = 0 Then
                            dRow.Item("rowtypeid") = 6 'only one budget at a time
                            dRow.Item("Message") = Language.getMessage(mstrModuleName, 16, "Sorry, You can import for only one budget at a time.")
                        End If
                    End If
                End If
                dRow.Item("budget_code") = dsRow.Item(cboBudgetCode.Text).ToString
                dRow.Item("budgetcodesunkid") = intBudgetCodesUnkid
                dRow.Item("budgetunkid") = intBudgetUnkid

                objBudget._Budgetunkid = intBudgetUnkid

                If intDefaultBudgetAllocationById <> objBudget._Allocationbyid Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 27, "Sorry, Allocation By of Default budget and Allocation By of Budget from file must be same."), enMsgBoxStyle.Information)
                    Exit Try
                End If

                Select Case objBudget._Allocationbyid

                    Case 0 'Employee

                        dRows = dsEmployee.Tables(0).Select("employeecode = '" & dsRow.Item(cboEmployeeCode.Text).ToString & "' ")
                        If dRows IsNot Nothing AndAlso dRows.Length > 0 Then
                            intAllocationtranunkid = CInt(dRows(0).Item("employeeunkid"))
                            dRow.Item("allocationtranname") = dRows(0).Item("employeename").ToString
                        Else
                            intAllocationtranunkid = objEmp.GetEmployeeUnkidFromEmpCode(dsRow.Item(cboEmployeeCode.Text).ToString)
                            If intAllocationtranunkid <= 0 Then
                                dRow.Item("allocationtranname") = ""
                                If CInt(dRow.Item("rowtypeid")) = 0 Then
                                    dRow.Item("rowtypeid") = 2 'Employee code not found
                                    dRow.Item("Message") = Language.getMessage(mstrModuleName, 17, "Employee code not found.")
                                End If
                            Else
                                objEmp._Employeeunkid(mdtPeriodEndDate) = intAllocationtranunkid
                                dRow.Item("allocationtranname") = objEmp._Firstname & " " & objEmp._Surname
                                If CInt(dRow.Item("rowtypeid")) = 0 Then
                                    dRow.Item("rowtypeid") = 3 'Inactive employee
                                    dRow.Item("Message") = Language.getMessage(mstrModuleName, 18, "Employee is inactive.")
                                End If
                            End If
                        End If

                    Case enAnalysisReport.Branch

                        dRows = dsAllocation.Tables(0).Select("code = '" & dsRow.Item(cboEmployeeCode.Text).ToString & "' ")
                        If dRows IsNot Nothing AndAlso dRows.Length > 0 Then
                            intAllocationtranunkid = CInt(dRows(0).Item("stationunkid"))
                            dRow.Item("allocationtranname") = dRows(0).Item("name").ToString
                        Else
                            dRow.Item("allocationtranname") = ""
                            If CInt(dRow.Item("rowtypeid")) = 0 Then
                                dRow.Item("rowtypeid") = 2 'Branch code not found
                                dRow.Item("Message") = Language.getMessage(mstrModuleName, 28, "Branch code not found.")
                            End If
                        End If

                    Case enAnalysisReport.Department

                        dRows = dsAllocation.Tables(0).Select("code = '" & dsRow.Item(cboEmployeeCode.Text).ToString & "' ")
                        If dRows IsNot Nothing AndAlso dRows.Length > 0 Then
                            intAllocationtranunkid = CInt(dRows(0).Item("departmentunkid"))
                            dRow.Item("allocationtranname") = dRows(0).Item("name").ToString
                        Else
                            dRow.Item("allocationtranname") = ""
                            If CInt(dRow.Item("rowtypeid")) = 0 Then
                                dRow.Item("rowtypeid") = 2 'Department code not found
                                dRow.Item("Message") = Language.getMessage(mstrModuleName, 29, "Department code not found.")
                            End If
                        End If

                    Case enAnalysisReport.Section

                        dRows = dsAllocation.Tables(0).Select("code = '" & dsRow.Item(cboEmployeeCode.Text).ToString & "' ")
                        If dRows IsNot Nothing AndAlso dRows.Length > 0 Then
                            intAllocationtranunkid = CInt(dRows(0).Item("sectionunkid"))
                            dRow.Item("allocationtranname") = dRows(0).Item("name").ToString
                        Else
                            dRow.Item("allocationtranname") = ""
                            If CInt(dRow.Item("rowtypeid")) = 0 Then
                                dRow.Item("rowtypeid") = 2 'Section code not found
                                dRow.Item("Message") = Language.getMessage(mstrModuleName, 30, "Section code not found.")
                            End If
                        End If

                    Case enAnalysisReport.Unit

                        dRows = dsAllocation.Tables(0).Select("code = '" & dsRow.Item(cboEmployeeCode.Text).ToString & "' ")
                        If dRows IsNot Nothing AndAlso dRows.Length > 0 Then
                            intAllocationtranunkid = CInt(dRows(0).Item("unitunkid"))
                            dRow.Item("allocationtranname") = dRows(0).Item("name").ToString
                        Else
                            dRow.Item("allocationtranname") = ""
                            If CInt(dRow.Item("rowtypeid")) = 0 Then
                                dRow.Item("rowtypeid") = 2 'Unit code not found
                                dRow.Item("Message") = Language.getMessage(mstrModuleName, 31, "Unit code not found.")
                            End If
                        End If

                    Case enAnalysisReport.Job

                        dRows = dsAllocation.Tables(0).Select("code = '" & dsRow.Item(cboEmployeeCode.Text).ToString & "' ")
                        If dRows IsNot Nothing AndAlso dRows.Length > 0 Then
                            intAllocationtranunkid = CInt(dRows(0).Item("jobunkid"))
                            dRow.Item("allocationtranname") = dRows(0).Item("JobName").ToString
                        Else
                            dRow.Item("allocationtranname") = ""
                            If CInt(dRow.Item("rowtypeid")) = 0 Then
                                dRow.Item("rowtypeid") = 2 'Job code not found
                                dRow.Item("Message") = Language.getMessage(mstrModuleName, 32, "Job code not found.")
                            End If
                        End If

                    Case enAnalysisReport.CostCenter

                        dRows = dsAllocation.Tables(0).Select("costcentercode = '" & dsRow.Item(cboEmployeeCode.Text).ToString & "' ")
                        If dRows IsNot Nothing AndAlso dRows.Length > 0 Then
                            intAllocationtranunkid = CInt(dRows(0).Item("costcenterunkid"))
                            dRow.Item("allocationtranname") = dRows(0).Item("costcentername").ToString
                        Else
                            dRow.Item("allocationtranname") = ""
                            If CInt(dRow.Item("rowtypeid")) = 0 Then
                                dRow.Item("rowtypeid") = 2 'Cost Center code not found
                                dRow.Item("Message") = Language.getMessage(mstrModuleName, 33, "Cost Center code not found.")
                            End If
                        End If

                    Case enAnalysisReport.SectionGroup

                        dRows = dsAllocation.Tables(0).Select("code = '" & dsRow.Item(cboEmployeeCode.Text).ToString & "' ")
                        If dRows IsNot Nothing AndAlso dRows.Length > 0 Then
                            intAllocationtranunkid = CInt(dRows(0).Item("sectiongroupunkid"))
                            dRow.Item("allocationtranname") = dRows(0).Item("name").ToString
                        Else
                            dRow.Item("allocationtranname") = ""
                            If CInt(dRow.Item("rowtypeid")) = 0 Then
                                dRow.Item("rowtypeid") = 2 'Section Group code not found
                                dRow.Item("Message") = Language.getMessage(mstrModuleName, 34, "Section Group code not found.")
                            End If
                        End If

                    Case enAnalysisReport.UnitGroup

                        dRows = dsAllocation.Tables(0).Select("code = '" & dsRow.Item(cboEmployeeCode.Text).ToString & "' ")
                        If dRows IsNot Nothing AndAlso dRows.Length > 0 Then
                            intAllocationtranunkid = CInt(dRows(0).Item("unitgroupunkid"))
                            dRow.Item("allocationtranname") = dRows(0).Item("name").ToString
                        Else
                            dRow.Item("allocationtranname") = ""
                            If CInt(dRow.Item("rowtypeid")) = 0 Then
                                dRow.Item("rowtypeid") = 2 'Unit Group code not found
                                dRow.Item("Message") = Language.getMessage(mstrModuleName, 35, "Unit Group code not found.")
                            End If
                        End If

                    Case enAnalysisReport.Team

                        dRows = dsAllocation.Tables(0).Select("code = '" & dsRow.Item(cboEmployeeCode.Text).ToString & "' ")
                        If dRows IsNot Nothing AndAlso dRows.Length > 0 Then
                            intAllocationtranunkid = CInt(dRows(0).Item("teamunkid"))
                            dRow.Item("allocationtranname") = dRows(0).Item("name").ToString
                        Else
                            dRow.Item("allocationtranname") = ""
                            If CInt(dRow.Item("rowtypeid")) = 0 Then
                                dRow.Item("rowtypeid") = 2 'Team code not found
                                dRow.Item("Message") = Language.getMessage(mstrModuleName, 36, "Team code not found.")
                            End If
                        End If

                    Case enAnalysisReport.DepartmentGroup

                        dRows = dsAllocation.Tables(0).Select("code = '" & dsRow.Item(cboEmployeeCode.Text).ToString & "' ")
                        If dRows IsNot Nothing AndAlso dRows.Length > 0 Then
                            intAllocationtranunkid = CInt(dRows(0).Item("deptgroupunkid"))
                            dRow.Item("allocationtranname") = dRows(0).Item("name").ToString
                        Else
                            dRow.Item("allocationtranname") = ""
                            If CInt(dRow.Item("rowtypeid")) = 0 Then
                                dRow.Item("rowtypeid") = 2 'Department Group code not found
                                dRow.Item("Message") = Language.getMessage(mstrModuleName, 37, "Department Group code not found.")
                            End If
                        End If

                    Case enAnalysisReport.JobGroup

                        dRows = dsAllocation.Tables(0).Select("code = '" & dsRow.Item(cboEmployeeCode.Text).ToString & "' ")
                        If dRows IsNot Nothing AndAlso dRows.Length > 0 Then
                            intAllocationtranunkid = CInt(dRows(0).Item("jobgroupunkid"))
                            dRow.Item("allocationtranname") = dRows(0).Item("name").ToString
                        Else
                            dRow.Item("allocationtranname") = ""
                            If CInt(dRow.Item("rowtypeid")) = 0 Then
                                dRow.Item("rowtypeid") = 2 'Job Group code not found
                                dRow.Item("Message") = Language.getMessage(mstrModuleName, 38, "Job Group code not found.")
                            End If
                        End If

                    Case enAnalysisReport.ClassGroup

                        dRows = dsAllocation.Tables(0).Select("code = '" & dsRow.Item(cboEmployeeCode.Text).ToString & "' ")
                        If dRows IsNot Nothing AndAlso dRows.Length > 0 Then
                            intAllocationtranunkid = CInt(dRows(0).Item("classgroupunkid"))
                            dRow.Item("allocationtranname") = dRows(0).Item("name").ToString
                        Else
                            dRow.Item("allocationtranname") = ""
                            If CInt(dRow.Item("rowtypeid")) = 0 Then
                                dRow.Item("rowtypeid") = 2 'Class Group code not found
                                dRow.Item("Message") = Language.getMessage(mstrModuleName, 39, "Class Group code not found.")
                            End If
                        End If

                    Case enAnalysisReport.Classs

                        dRows = dsAllocation.Tables(0).Select("code = '" & dsRow.Item(cboEmployeeCode.Text).ToString & "' ")
                        If dRows IsNot Nothing AndAlso dRows.Length > 0 Then
                            intAllocationtranunkid = CInt(dRows(0).Item("classesunkid"))
                            dRow.Item("allocationtranname") = dRows(0).Item("name").ToString
                        Else
                            dRow.Item("allocationtranname") = ""
                            If CInt(dRow.Item("rowtypeid")) = 0 Then
                                dRow.Item("rowtypeid") = 2 'Class code not found
                                dRow.Item("Message") = Language.getMessage(mstrModuleName, 40, "Class code not found.")
                            End If
                        End If

                    Case enAnalysisReport.GradeGroup

                        dRows = dsAllocation.Tables(0).Select("code = '" & dsRow.Item(cboEmployeeCode.Text).ToString & "' ")
                        If dRows IsNot Nothing AndAlso dRows.Length > 0 Then
                            intAllocationtranunkid = CInt(dRows(0).Item("gradegroupunkid"))
                            dRow.Item("allocationtranname") = dRows(0).Item("name").ToString
                        Else
                            dRow.Item("allocationtranname") = ""
                            If CInt(dRow.Item("rowtypeid")) = 0 Then
                                dRow.Item("rowtypeid") = 2 'Grade Group code not found
                                dRow.Item("Message") = Language.getMessage(mstrModuleName, 41, "Grade Group code not found.")
                            End If
                        End If

                    Case enAnalysisReport.Grade

                        dRows = dsAllocation.Tables(0).Select("code = '" & dsRow.Item(cboEmployeeCode.Text).ToString & "' ")
                        If dRows IsNot Nothing AndAlso dRows.Length > 0 Then
                            intAllocationtranunkid = CInt(dRows(0).Item("gradeunkid"))
                            dRow.Item("allocationtranname") = dRows(0).Item("name").ToString
                        Else
                            dRow.Item("allocationtranname") = ""
                            If CInt(dRow.Item("rowtypeid")) = 0 Then
                                dRow.Item("rowtypeid") = 2 'Grade code not found
                                dRow.Item("Message") = Language.getMessage(mstrModuleName, 42, "Grade code not found.")
                            End If
                        End If

                    Case enAnalysisReport.GradeLevel

                        dRows = dsAllocation.Tables(0).Select("code = '" & dsRow.Item(cboEmployeeCode.Text).ToString & "' ")
                        If dRows IsNot Nothing AndAlso dRows.Length > 0 Then
                            intAllocationtranunkid = CInt(dRows(0).Item("gradelevelunkid"))
                            dRow.Item("allocationtranname") = dRows(0).Item("name").ToString
                        Else
                            dRow.Item("allocationtranname") = ""
                            If CInt(dRow.Item("rowtypeid")) = 0 Then
                                dRow.Item("rowtypeid") = 2 'Grade Level code not found
                                dRow.Item("Message") = Language.getMessage(mstrModuleName, 43, "Grade Level code not found.")
                            End If
                        End If

                    Case Else
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 99, "Sorry, Import feature is not available right now for this allocation."), enMsgBoxStyle.Information)
                        Exit Sub

                End Select

                dRow.Item("allocationtranunkid") = intAllocationtranunkid
                dRow.Item("allocationtrancode") = dsRow.Item(cboEmployeeCode.Text).ToString

                For Each dgRow As DataGridViewRow In dgvMapping.Rows

                    objProjectCode = New clsFundProjectCode
                    intProjectCodeunkid = Convert.ToInt32(dgRow.Cells(objcolhFundprojectcodeunkid.Index).Value)
                    objProjectCode._FundProjectCodeunkid = intProjectCodeunkid

                    Dim decPerc As Decimal = 0

                    Decimal.TryParse(dsRow.Item(CInt(dgRow.Cells(colhPercentage.Index).Value.ToString)).ToString, decPerc)

                    dRow.Item("|_" & intProjectCodeunkid.ToString) = decPerc

                    If decPerc = 0 AndAlso dsRow.Item(CInt(dgRow.Cells(objcolhActivityid.Index).Value)).ToString.Trim = "" Then
                        dRow.Item("||__" & intProjectCodeunkid.ToString) = ""
                    Else
                        If objActivity.isExists(" AND activity_code = '" & dsRow.Item(CInt(dgRow.Cells(objcolhActivityid.Index).Value)).ToString & "' ", intFundActivityunkid) = True Then
                            objActivity = New clsfundactivity_Tran
                            objActivity._Fundactivityunkid = intFundActivityunkid
                            dRow.Item("||__" & intProjectCodeunkid.ToString) = objActivity._Activity_Code

                            If objActivity.isExists(" AND activity_code = '" & dsRow.Item(CInt(dgRow.Cells(objcolhActivityid.Index).Value)).ToString & "' AND fundprojectcodeunkid = " & intProjectCodeunkid & " ", intFundActivityunkid) = True Then

                            Else
                                If CInt(dRow.Item("rowtypeid")) = 0 Then
                                    dRow.Item("rowtypeid") = 4 'Activity not found
                                    dRow.Item("Message") = Language.getMessage(mstrModuleName, 19, "Activity under given fund project code not found.")
                                End If
                            End If
                        Else
                            If CInt(dRow.Item("rowtypeid")) = 0 Then
                                dRow.Item("rowtypeid") = 5 'Activity not found
                                dRow.Item("Message") = Language.getMessage(mstrModuleName, 20, "Activity not found.")
                            End If
                            dRow.Item("||__" & intProjectCodeunkid.ToString) = ""
                        End If
                    End If
                    dRow.Item("||_" & intProjectCodeunkid.ToString) = intFundActivityunkid

                    
                Next

                dtTable.Rows.Add(dRow)

            Next

            If dtTable.Rows.Count > 0 Then
                mblnCancel = False
                Me.Close()
            Else
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 21, "Sorry, Some project codes, activity codes or Employee Codes are not matching from file!"), enMsgBoxStyle.Exclamation)
            End If

        Catch ex As Exception
            If ex.InnerException.Message.ToString = "Input string was not in a correct format." Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 22, "Invalid Data [Non-Numeric amount ") & strAmount & Language.getMessage(mstrModuleName, 23, "] found in File for column of  ") & strColumn & ".", enMsgBoxStyle.Information)
            Else
                DisplayError.Show("-1", ex.Message, "btnOk_Click", mstrModuleName)
            End If
        Finally
            dsEmployee = Nothing
        End Try
    End Sub
#End Region

#Region " Other Control's Events "

    Private Sub lnkAutoMap_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lnkAutoMap.LinkClicked
        Try
            Dim dTable As DataTable = dtColumns

            Dim rw As DataRow = (From p In dTable Where (p.Item("name").ToString.ToUpper Like "*" & lblBudgetCode.Text.ToUpper & "*") Select (p)).FirstOrDefault
            If rw IsNot Nothing Then
                If CInt(cboEmployeeCode.SelectedValue) = CInt(rw.Item("id")) Then
                    'eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Sorry! This Fields is already Mapped with ") & cboEmployeeCode.Text & Language.getMessage(mstrModuleName, 5, " Fields From File.") & vbCrLf & Language.getMessage(mstrModuleName, 6, "Please select different Field."), enMsgBoxStyle.Information)
                    cboEmployeeCode.SelectedValue = -1
                    cboBudgetCode.SelectedValue = CInt(rw.Item("id"))
                Else
                    For Each dRow As DataGridViewRow In dgvMapping.Rows
                        If Convert.ToInt32(dRow.Cells(colhPercentage.Index).Value) = CInt(rw.Item("id")) Then
                            'eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Sorry! This Column is already Mapped.") & vbCrLf & Language.getMessage(mstrModuleName, 6, "Please select different Field."), enMsgBoxStyle.Information)
                            dRow.Cells(colhPercentage.Index).Value = -1
                            'Exit For
                        ElseIf Convert.ToInt32(dRow.Cells(colhActivityCode.Index).Value) = CInt(rw.Item("id")) Then
                            'eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Sorry! This Column is already Mapped.") & vbCrLf & Language.getMessage(mstrModuleName, 6, "Please select different Field."), enMsgBoxStyle.Information)
                            dRow.Cells(colhActivityCode.Index).Value = -1
                            'Exit For
                        End If
                    Next

                    cboBudgetCode.SelectedValue = CInt(rw.Item("id"))
                End If
            End If

            rw = (From p In dTable Where (p.Item("name").ToString.ToUpper Like "*" & lblEmployeeCode.Text.Replace(".", "").ToUpper & "*") Select (p)).FirstOrDefault
            If rw IsNot Nothing Then
                If CInt(cboBudgetCode.SelectedValue) = CInt(rw.Item("id")) Then
                    'eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Sorry! This Fields is already Mapped with ") & cboBudgetCode.Text & Language.getMessage(mstrModuleName, 5, " Fields From File.") & vbCrLf & Language.getMessage(mstrModuleName, 6, "Please select different Field."), enMsgBoxStyle.Information)
                    cboBudgetCode.SelectedValue = -1
                    cboEmployeeCode.SelectedValue = CInt(rw.Item("id"))
                Else
                    For Each dRow As DataGridViewRow In dgvMapping.Rows
                        If Convert.ToInt32(dRow.Cells(colhPercentage.Index).Value) = CInt(rw.Item("id")) Then
                            'eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Sorry! This Column is already Mapped.") & vbCrLf & Language.getMessage(mstrModuleName, 6, "Please select different Field."), enMsgBoxStyle.Information)
                            dRow.Cells(colhPercentage.Index).Value = -1
                            'Exit For
                        ElseIf Convert.ToInt32(dRow.Cells(colhActivityCode.Index).Value) = CInt(rw.Item("id")) Then
                            'eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Sorry! This Column is already Mapped.") & vbCrLf & Language.getMessage(mstrModuleName, 6, "Please select different Field."), enMsgBoxStyle.Information)
                            dRow.Cells(colhActivityCode.Index).Value = -1
                            'Exit For
                        End If
                    Next

                    cboEmployeeCode.SelectedValue = CInt(rw.Item("id"))
                End If
            End If

            For Each dRow As DataGridViewRow In dgvMapping.Rows
                Dim strName As String = dRow.Cells(colhProjectCodes.Index).Value.ToString & " (%)"
                Dim strActvityName As String = dRow.Cells(colhProjectCodes.Index).Value.ToString & " Activity Code"
                Dim dgvrow As DataGridViewRow = dRow

                Dim row As DataRow = (From p In dTable Where (p.Item("name").ToString.ToUpper Like "*" & strName.ToUpper & "*") Select (p)).FirstOrDefault
                If row IsNot Nothing Then
                    
                    For Each dR As DataGridViewRow In dgvMapping.Rows
                        If dR.Index <> dRow.Index AndAlso Convert.ToInt32(dRow.Cells(colhPercentage.Index).Value) = CInt(rw.Item("id")) Then
                            'eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Sorry! This Column is already Mapped.") & vbCrLf & Language.getMessage(mstrModuleName, 6, "Please select different Field."), enMsgBoxStyle.Information)
                            dR.Cells(colhPercentage.Index).Value = -1
                            'Exit For
                        ElseIf Convert.ToInt32(dRow.Cells(colhActivityCode.Index).Value) = CInt(rw.Item("id")) Then
                            'eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Sorry! This Column is already Mapped.") & vbCrLf & Language.getMessage(mstrModuleName, 6, "Please select different Field."), enMsgBoxStyle.Information)
                            dR.Cells(colhActivityCode.Index).Value = -1
                            'Exit For
                        End If
                    Next
                    dRow.Cells(colhPercentage.Index).Value = CInt(row.Item("id"))
                End If

                row = (From p In dTable Where (p.Item("name").ToString.ToUpper Like "*" & strActvityName.ToUpper & "*") Select (p)).FirstOrDefault
                If row IsNot Nothing Then
                    
                    For Each dR As DataGridViewRow In dgvMapping.Rows
                        If Convert.ToInt32(dRow.Cells(colhPercentage.Index).Value) = CInt(rw.Item("id")) Then
                            dR.Cells(colhPercentage.Index).Value = -1
                        ElseIf dR.Index <> dRow.Index AndAlso Convert.ToInt32(dRow.Cells(colhActivityCode.Index).Value) = CInt(rw.Item("id")) Then
                            dR.Cells(colhActivityCode.Index).Value = -1
                        End If
                    Next
                    dRow.Cells(colhActivityCode.Index).Value = CInt(row.Item("id"))
                End If
            Next
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lnkAutoMap_LinkClicked", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Datagridview's Events "

    Private Sub dgvMapping_CellClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvMapping.CellClick
        Try
            
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgvMapping_CellClick", mstrModuleName)
        End Try
    End Sub

    Private Sub dgvMapping_CellEnter(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvMapping.CellEnter
        Try
            If e.RowIndex <= -1 Then Exit Sub

            If e.ColumnIndex = colhPercentage.Index OrElse e.ColumnIndex = colhActivityCode.Index Then
                SendKeys.Send("{F2}")
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgvMapping_CellEnter", mstrModuleName)
        End Try
    End Sub

    Private Sub dgvMapping_CellMouseMove(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellMouseEventArgs) Handles dgvMapping.CellMouseMove
        Try
            If e.RowIndex < 0 Then Exit Sub

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgvMapping_CellMouseMove", mstrModuleName)
        End Try
    End Sub

    Private Sub dgvMapping_DataError(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewDataErrorEventArgs) Handles dgvMapping.DataError
        Try
            eZeeMsgBox.Show(e.Exception.Message, enMsgBoxStyle.Critical)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgvMapping_DataError", mstrModuleName)
        End Try
    End Sub

    Private Sub dgvMapping_EditingControlShowing(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewEditingControlShowingEventArgs) Handles dgvMapping.EditingControlShowing
        Try

            If dgvMapping.CurrentCell.ColumnIndex = colhPercentage.Index OrElse dgvMapping.CurrentCell.ColumnIndex = colhActivityCode.Index Then
                Dim cb As ComboBox = TryCast(e.Control, ComboBox)
                If cb IsNot Nothing Then
                    cb.DropDownStyle = ComboBoxStyle.DropDown
                    cb.AutoCompleteMode = AutoCompleteMode.SuggestAppend

                    RemoveHandler cb.Validating, AddressOf TranHead_Validating

                    'Select Case dgvMapping.CurrentCell.ColumnIndex
                    '    Case colhPercentage.Index
                    AddHandler cb.Validating, AddressOf TranHead_Validating
                    'End Select
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgvMapping_EditingControlShowing", mstrModuleName)
        End Try
    End Sub

    Private Sub TranHead_Validating(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs)
        Try
            Dim cb As ComboBox = TryCast(sender, ComboBox)
            If cb IsNot Nothing Then
                If cb.SelectedIndex <> 0 Then
                    If Convert.ToInt32(cb.SelectedValue) = CInt(cboBudgetCode.SelectedValue) Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Sorry! This Column is already Mapped with Budget Code.") & vbCrLf & Language.getMessage(mstrModuleName, 3, "Please select different Field."), enMsgBoxStyle.Information)
                        cb.SelectedIndex = 0
                        e.Cancel = True
                        SendKeys.Send("{ESC}")
                        SendKeys.Send("{F2}")
                        Exit Try
                    ElseIf Convert.ToInt32(cb.SelectedValue) = CInt(cboEmployeeCode.SelectedValue) Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Sorry! This Column is already Mapped with Employee / Allocation Code.") & vbCrLf & Language.getMessage(mstrModuleName, 5, "Please select different Field."), enMsgBoxStyle.Information)
                        cb.SelectedIndex = 0
                        e.Cancel = True
                        SendKeys.Send("{ESC}")
                        SendKeys.Send("{F2}")
                        Exit Try
                    End If

                    For Each dRow As DataGridViewRow In dgvMapping.Rows
                        If (dgvMapping.CurrentCell.ColumnIndex <> colhPercentage.Index OrElse dgvMapping.CurrentRow.Index <> dRow.Index) AndAlso Convert.ToInt32(dRow.Cells(colhPercentage.Index).Value) = Convert.ToInt32(cb.SelectedValue) Then
                            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 24, "Sorry! This Column is already Mapped.") & vbCrLf & Language.getMessage(mstrModuleName, 7, "Please select different Field."), enMsgBoxStyle.Information)
                            dgvMapping.CurrentCell.Value = -1
                            cb.SelectedIndex = 0
                            e.Cancel = True
                            SendKeys.Send("{ESC}")
                            SendKeys.Send("{F2}")
                            Exit For
                        ElseIf (dgvMapping.CurrentCell.ColumnIndex <> colhActivityCode.Index OrElse dgvMapping.CurrentRow.Index <> dRow.Index) AndAlso Convert.ToInt32(dRow.Cells(colhActivityCode.Index).Value) = Convert.ToInt32(cb.SelectedValue) Then
                            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 24, "Sorry! This Column is already Mapped.") & vbCrLf & Language.getMessage(mstrModuleName, 25, "Please select different Field."), enMsgBoxStyle.Information)
                            dgvMapping.CurrentCell.Value = -1
                            cb.SelectedIndex = 0
                            e.Cancel = True
                            SendKeys.Send("{ESC}")
                            SendKeys.Send("{F2}")
                            Exit For
                        End If
                    Next
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "TranHead_Validating", mstrModuleName)
        End Try
    End Sub

#End Region


	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Private Sub OtherSettings()
		Try
			Me.SuspendLayout()
			 
			Call SetLanguage()
			
			Me.gbFieldMapping.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbFieldMapping.ForeColor = GUI._eZeeContainerHeaderForeColor 


			Me.btnCancel.GradientBackColor = GUI._ButttonBackColor 
			Me.btnCancel.GradientForeColor = GUI._ButttonFontColor

			Me.btnOk.GradientBackColor = GUI._ButttonBackColor 
			Me.btnOk.GradientForeColor = GUI._ButttonFontColor


			Me.ResumeLayout()
		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "OtherSettings", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetLanguage()
		Try
			Me.Text = Language._Object.getCaption(Me.Name, Me.Text)
			
			Me.gbFieldMapping.Text = Language._Object.getCaption(Me.gbFieldMapping.Name, Me.gbFieldMapping.Text)
			Me.lblPeriod.Text = Language._Object.getCaption(Me.lblPeriod.Name, Me.lblPeriod.Text)
			Me.lblEmployeeCode.Text = Language._Object.getCaption(Me.lblEmployeeCode.Name, Me.lblEmployeeCode.Text)
			Me.lnkAutoMap.Text = Language._Object.getCaption(Me.lnkAutoMap.Name, Me.lnkAutoMap.Text)
			Me.btnCancel.Text = Language._Object.getCaption(Me.btnCancel.Name, Me.btnCancel.Text)
			Me.btnOk.Text = Language._Object.getCaption(Me.btnOk.Name, Me.btnOk.Text)
			Me.DataGridViewTextBoxColumn1.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn1.Name, Me.DataGridViewTextBoxColumn1.HeaderText)
			Me.DataGridViewTextBoxColumn2.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn2.Name, Me.DataGridViewTextBoxColumn2.HeaderText)
			Me.DataGridViewTextBoxColumn3.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn3.Name, Me.DataGridViewTextBoxColumn3.HeaderText)
			Me.lblBudgetCode.Text = Language._Object.getCaption(Me.lblBudgetCode.Name, Me.lblBudgetCode.Text)
			Me.colhProjectCodes.HeaderText = Language._Object.getCaption(Me.colhProjectCodes.Name, Me.colhProjectCodes.HeaderText)
			Me.colhPercentage.HeaderText = Language._Object.getCaption(Me.colhPercentage.Name, Me.colhPercentage.HeaderText)
			Me.colhActivityCode.HeaderText = Language._Object.getCaption(Me.colhActivityCode.Name, Me.colhActivityCode.HeaderText)
			Me.DataGridViewTextBoxColumn4.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn4.Name, Me.DataGridViewTextBoxColumn4.HeaderText)

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetLanguage", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "Select")
			Language.setMessage(mstrModuleName, 2, "Sorry! This Column is already Mapped with Budget Code.")
			Language.setMessage(mstrModuleName, 3, "Please select different Field.")
			Language.setMessage(mstrModuleName, 4, "Sorry! This Column is already Mapped with Employee / Allocation Code.")
			Language.setMessage(mstrModuleName, 5, "Please select different Field.")
			Language.setMessage(mstrModuleName, 6, "Sorry! This Column is already Mapped with Project Codes (%) for")
			Language.setMessage(mstrModuleName, 7, "Please select different Field.")
			Language.setMessage(mstrModuleName, 8, "Sorry! This Column is already Mapped with Activity for Project Code")
			Language.setMessage(mstrModuleName, 9, "Please select different Field.")
			Language.setMessage(mstrModuleName, 10, "Please select Period. Period is mandatory information.")
			Language.setMessage(mstrModuleName, 11, "Please select budget code column.")
			Language.setMessage(mstrModuleName, 12, "Please select employee code column.")
			Language.setMessage(mstrModuleName, 14, "Please Map Project Codes / Activity Code columns.")
			Language.setMessage(mstrModuleName, 15, "Budget not found.")
			Language.setMessage(mstrModuleName, 16, "Sorry, You can import for only one budget at a time.")
			Language.setMessage(mstrModuleName, 17, "Employee code not found.")
			Language.setMessage(mstrModuleName, 18, "Employee is inactive.")
			Language.setMessage(mstrModuleName, 19, "Activity under given fund project code not found.")
			Language.setMessage(mstrModuleName, 20, "Activity not found.")
			Language.setMessage(mstrModuleName, 21, "Sorry, Some project codes, activity codes or Employee Codes are not matching from file!")
			Language.setMessage(mstrModuleName, 22, "Invalid Data [Non-Numeric amount")
			Language.setMessage(mstrModuleName, 23, "] found in File for column of")
			Language.setMessage(mstrModuleName, 24, "Sorry! This Column is already Mapped.")
			Language.setMessage(mstrModuleName, 25, "Please select different Field.")
			Language.setMessage(mstrModuleName, 26, "Sorry, There is no Default budget set. Please set Default budget.")
			Language.setMessage(mstrModuleName, 27, "Sorry, Allocation By of Default budget and Allocation By of Budget from file must be same.")
			Language.setMessage(mstrModuleName, 28, "Branch code not found.")
			Language.setMessage(mstrModuleName, 29, "Department code not found.")
			Language.setMessage(mstrModuleName, 30, "Section code not found.")
			Language.setMessage(mstrModuleName, 31, "Unit code not found.")
			Language.setMessage(mstrModuleName, 32, "Job code not found.")
			Language.setMessage(mstrModuleName, 33, "Cost Center code not found.")
			Language.setMessage(mstrModuleName, 34, "Section Group code not found.")
			Language.setMessage(mstrModuleName, 35, "Unit Group code not found.")
			Language.setMessage(mstrModuleName, 36, "Team code not found.")
			Language.setMessage(mstrModuleName, 37, "Department Group code not found.")
			Language.setMessage(mstrModuleName, 38, "Job Group code not found.")
			Language.setMessage(mstrModuleName, 39, "Class Group code not found.")
			Language.setMessage(mstrModuleName, 40, "Class code not found.")
			Language.setMessage(mstrModuleName, 41, "Grade Group code not found.")
			Language.setMessage(mstrModuleName, 42, "Grade code not found.")
			Language.setMessage(mstrModuleName, 43, "Grade Level code not found.")
			Language.setMessage(mstrModuleName, 99, "Sorry, Import feature is not available right now for this allocation.")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class