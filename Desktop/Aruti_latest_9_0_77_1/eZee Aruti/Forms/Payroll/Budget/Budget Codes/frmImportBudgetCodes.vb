﻿Option Strict On

#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data
Imports System.IO

#End Region

Public Class frmImportBudgetCodes

#Region " Private Variables "

    Private ReadOnly mstrModuleName As String = "frmImportBudgetCodes"
    Private mblnCancel As Boolean = True
    Private dsList As New DataSet
    Private mdtTable As New DataTable
    Private mdtFilteredTable As DataTable
    'S.SANDEEP [12-Jan-2018] -- START
    'ISSUE/ENHANCEMENT : REF-ID # 0001843
    'Private objIExcel As ExcelData
    'S.SANDEEP [12-Jan-2018] -- END
    Private m_Dataview As DataView
    Private mdicFund As New Dictionary(Of Integer, Decimal)
    Private mintBudgetCodesUnkid As Integer = 0
    Private mintBudgetUnkid As Integer = 0
    Private mintPeriodUnkid As Integer = 0

    Private mblnCopyPreviousEDSlab As Boolean
#End Region

#Region " Display Dialog "

    Public Function displayDialog() As Boolean
        Try
            Me.ShowDialog()
            Return Not mblnCancel
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "displayDialog", mstrModuleName)
        End Try
    End Function

#End Region

#Region " Private Methods "

    Private Sub SetColor()
        Try
           
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetColor", mstrModuleName)
        End Try
    End Sub

    Private Sub FillCombo()
        Dim objMaster As New clsMasterData
        Dim dsCombos As New DataSet
        Try
            

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "", mstrModuleName)
        End Try
    End Sub

    Private Sub FillGirdView()
        Dim objFundProjectCode As New clsFundProjectCode
        Dim objActivity As New clsfundactivity_Tran
        Dim dCol As DataGridViewTextBoxColumn
        Dim dColChk As DataGridViewCheckBoxColumn
        Try
            With dgvImportInfo
                .DataSource = Nothing
                .AutoGenerateColumns = False
                .ColumnHeadersHeightSizeMode = DataGridViewColumnHeadersHeightSizeMode.EnableResizing
                If .Columns.Count <= 0 Then
                    .ColumnHeadersHeight = .ColumnHeadersHeight * 2
                End If
                .ColumnHeadersDefaultCellStyle.Alignment = DataGridViewContentAlignment.BottomCenter

                If Not .DataSource Is Nothing Then .DataSource = Nothing
                .Columns.Clear()
                .Rows.Clear()

                dColChk = New DataGridViewCheckBoxColumn
                dColChk.Name = "colhIsChecked"
                dColChk.HeaderText = ""
                dColChk.SortMode = DataGridViewColumnSortMode.NotSortable
                dColChk.Width = 25
                dColChk.Frozen = True
                dColChk.Visible = True
                dColChk.ReadOnly = False
                dColChk.DataPropertyName = "IsChecked"
                .Columns.Add(dColChk)

                dCol = New DataGridViewTextBoxColumn
                dCol.Name = "colhBudgetcodesunkid"
                dCol.HeaderText = "Id"
                dCol.SortMode = DataGridViewColumnSortMode.NotSortable
                dCol.Width = 100
                dCol.Frozen = True
                dCol.Visible = False
                dCol.ReadOnly = True
                dCol.DataPropertyName = "budgetcodesunkid"
                .Columns.Add(dCol)

                dCol = New DataGridViewTextBoxColumn
                dCol.Name = "colhBudgetunkid"
                dCol.HeaderText = "Id"
                dCol.SortMode = DataGridViewColumnSortMode.NotSortable
                dCol.Width = 100
                dCol.Frozen = True
                dCol.Visible = False
                dCol.ReadOnly = True
                dCol.DataPropertyName = "budgetunkid"
                .Columns.Add(dCol)

                dCol = New DataGridViewTextBoxColumn
                dCol.Name = "colhBudget_code"
                dCol.HeaderText = "Budget Code"
                dCol.SortMode = DataGridViewColumnSortMode.NotSortable
                dCol.Width = 100
                dCol.Frozen = True
                dCol.Visible = True
                dCol.ReadOnly = True
                dCol.DataPropertyName = "budget_code"
                .Columns.Add(dCol)

                dCol = New DataGridViewTextBoxColumn
                dCol.Name = "colhperiodunkid"
                dCol.HeaderText = "Id"
                dCol.SortMode = DataGridViewColumnSortMode.NotSortable
                dCol.Width = 100
                dCol.Frozen = True
                dCol.Visible = False
                dCol.ReadOnly = True
                dCol.DataPropertyName = "periodunkid"
                .Columns.Add(dCol)

                dCol = New DataGridViewTextBoxColumn
                dCol.Name = "colhperiod_code"
                dCol.HeaderText = "Period Code"
                dCol.SortMode = DataGridViewColumnSortMode.NotSortable
                dCol.Width = 100
                dCol.Frozen = True
                dCol.Visible = False
                dCol.ReadOnly = True
                dCol.DataPropertyName = "period_code"
                .Columns.Add(dCol)

                dCol = New DataGridViewTextBoxColumn
                dCol.Name = "colhperiod_name"
                dCol.HeaderText = "Period Name"
                dCol.SortMode = DataGridViewColumnSortMode.NotSortable
                dCol.Width = 100
                dCol.Frozen = True
                dCol.Visible = True
                dCol.ReadOnly = True
                dCol.DataPropertyName = "period_name"
                .Columns.Add(dCol)

                dCol = New DataGridViewTextBoxColumn
                dCol.Name = "colhAllocationID"
                dCol.HeaderText = "Id"
                dCol.SortMode = DataGridViewColumnSortMode.NotSortable
                dCol.Width = 100
                dCol.Frozen = True
                dCol.Visible = False
                dCol.ReadOnly = True
                dCol.DataPropertyName = "allocationtranunkid"
                .Columns.Add(dCol)


                'Adding DataGrid Columns
                dCol = New DataGridViewTextBoxColumn
                dCol.Name = "colhAllocation"
                dCol.HeaderText = "Emp. / Allocation name"
                dCol.SortMode = DataGridViewColumnSortMode.NotSortable
                dCol.Width = 180
                dCol.Frozen = True
                dCol.ReadOnly = True
                dCol.DataPropertyName = "allocationtranname"
                .Columns.Add(dCol)

                'dCol = New DataGridViewTextBoxColumn
                'dCol.Name = "colhGrossSalary"
                'dCol.HeaderText = Language.getMessage(mstrModuleName, 2, "Gross Salary Budget")
                'dCol.SortMode = DataGridViewColumnSortMode.NotSortable
                'dCol.Width = 150
                'dCol.Frozen = False
                'dCol.ReadOnly = True
                'dCol.DataPropertyName = "budgetamount"
                'dCol.HeaderCell.Style.Alignment = DataGridViewContentAlignment.BottomCenter
                'dCol.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
                'dCol.DefaultCellStyle.Format = GUI.fmtCurrency
                '.Columns.Add(dCol)

                'Sohail (13 Oct 2017) -- Start
                'CCBRT Enhancement - 70.1 - (Ref. Id - 110, 111) - for all Four reports the user to be able to select a range of consecutive periods so as to be able to see the total. This Feature will enable the user to select more than one default budget so as to get a consolidated report.
                'dsList = objActivity.GetList("Activity")
                dsList = objActivity.GetList("Activity", , True)
                'Sohail (13 Oct 2017) -- End
                'mdicActivity = (From p In dsList.Tables("Activity") Select New With {.Id = CInt(p.Item("fundactivityunkid")), .Total = CDec(p.Item("amount"))}).ToDictionary(Function(x) x.Id, Function(y) y.Total)
                'mdicActivityCode = (From p In dsList.Tables("Activity") Select New With {.Id = CInt(p.Item("fundactivityunkid")), .Code = p.Item("activity_code").ToString}).ToDictionary(Function(x) x.Id, Function(y) y.Code)
                'Sohail (13 Oct 2017) -- Start
                'CCBRT Enhancement - 70.1 - (Ref. Id - 110, 111) - for all Four reports the user to be able to select a range of consecutive periods so as to be able to see the total. This Feature will enable the user to select more than one default budget so as to get a consolidated report.
                'dsList = objFundProjectCode.GetList("FundProjectCode")
                dsList = objFundProjectCode.GetList("FundProjectCode", , True)
                'Sohail (13 Oct 2017) -- End
                mdicFund = (From p In dsList.Tables("FundProjectCode") Select New With {.Id = CInt(p.Item("fundprojectcodeunkid")), .Total = CDec(p.Item("currentceilingbalance"))}).ToDictionary(Function(x) x.Id, Function(y) y.Total)
                'mdicFundCode = (From p In dsList.Tables("FundProjectCode") Select New With {.Id = CInt(p.Item("fundprojectcodeunkid")), .Code = p.Item("fundprojectcode").ToString}).ToDictionary(Function(x) x.Id, Function(y) y.Code)
                For Each dsRow As DataRow In dsList.Tables("FundProjectCode").Rows

                    'Adding DataGrid Columns
                    dCol = New DataGridViewTextBoxColumn
                    dCol.Name = "colhFund" & dsRow.Item("fundprojectcodeunkid").ToString
                    dCol.HeaderText = "Project Code (%)" 'dsRow.Item("project_code").ToString & " (%)"
                    dCol.Tag = dsRow.Item("fundprojectcode").ToString
                    dCol.SortMode = DataGridViewColumnSortMode.NotSortable
                    dCol.Width = 100
                    dCol.Frozen = False
                    dCol.ReadOnly = True
                    dCol.DataPropertyName = "|_" & dsRow.Item("fundprojectcodeunkid").ToString
                    dCol.HeaderCell.Style.Alignment = DataGridViewContentAlignment.BottomLeft
                    dCol.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
                    dCol.DefaultCellStyle.Format = GUI.fmtCurrency
                    .Columns.Add(dCol)

                    'dsCombo = objActivity.GetComboList("Activity", True, CInt(dsRow.Item("fundprojectcodeunkid")))
                    'Dim d_row() As DataRow = dsCombo.Tables(0).Select("fundactivityunkid = 0")
                    'If d_row.Length > 0 Then
                    '    d_row(0).Item("activityname") = ""
                    '    dsCombo.Tables(0).AcceptChanges()
                    'End If
                    'dColCombo = New DataGridViewComboBoxColumn
                    'dColCombo.Name = "colhActivity" & dsRow.Item("fundprojectcodeunkid").ToString
                    'dColCombo.HeaderText = "Activity Code"
                    'dColCombo.Tag = dsRow.Item("fundprojectcode").ToString
                    'dColCombo.SortMode = DataGridViewColumnSortMode.NotSortable
                    'dColCombo.Width = 100
                    'dColCombo.Frozen = False
                    'dColCombo.ReadOnly = False
                    'dColCombo.DataPropertyName = "||_" & dsRow.Item("fundprojectcodeunkid").ToString
                    'dColCombo.HeaderCell.Style.Alignment = DataGridViewContentAlignment.BottomLeft
                    'dColCombo.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft
                    'dColCombo.ValueMember = "fundactivityunkid"
                    'dColCombo.DisplayMember = "activitycode"
                    'dColCombo.DataSource = dsCombo.Tables(0)
                    'dColCombo.DataPropertyName = "||_" & dsRow.Item("fundprojectcodeunkid").ToString
                    'dColCombo.DisplayStyle = DataGridViewComboBoxDisplayStyle.Nothing
                    '.Columns.Add(dColCombo)

                    dCol = New DataGridViewTextBoxColumn
                    dCol.Name = "colhActivity" & dsRow.Item("fundprojectcodeunkid").ToString
                    dCol.HeaderText = "Activity Code"
                    dCol.Tag = dsRow.Item("fundprojectcode").ToString
                    dCol.SortMode = DataGridViewColumnSortMode.NotSortable
                    dCol.Width = 100
                    dCol.Frozen = False
                    dCol.ReadOnly = True
                    dCol.DataPropertyName = "||__" & dsRow.Item("fundprojectcodeunkid").ToString
                    dCol.HeaderCell.Style.Alignment = DataGridViewContentAlignment.BottomLeft
                    dCol.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
                    'dCol.DefaultCellStyle.Format = GUI.fmtCurrency
                    .Columns.Add(dCol)

                    'Adding DataGrid Columns
                    dCol = New DataGridViewTextBoxColumn
                    dCol.Name = "objcolhActivity" & dsRow.Item("fundprojectcodeunkid").ToString
                    dCol.HeaderText = "Activity Code"
                    dCol.Tag = dsRow.Item("fundprojectcode").ToString
                    dCol.SortMode = DataGridViewColumnSortMode.NotSortable
                    dCol.Width = 0
                    dCol.Frozen = False
                    dCol.ReadOnly = True
                    dCol.Visible = False
                    dCol.DataPropertyName = "||_" & dsRow.Item("fundprojectcodeunkid").ToString
                    dCol.HeaderCell.Style.Alignment = DataGridViewContentAlignment.BottomLeft
                    dCol.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft
                    'dCol.DefaultCellStyle.Format = GUI.fmtCurrency
                    .Columns.Add(dCol)
                Next

                'Adding DataGrid Columns
                'dCol = New DataGridViewTextBoxColumn
                'dCol.Name = "objisgroup"
                'dCol.HeaderText = "isgroup"
                'dCol.SortMode = DataGridViewColumnSortMode.NotSortable
                'dCol.Width = 50
                'dCol.Frozen = False
                'dCol.ReadOnly = True
                'dCol.Visible = False
                'dCol.DataPropertyName = dCol.Name
                'dCol.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
                '.Columns.Add(dCol)

                dCol = New DataGridViewTextBoxColumn
                dCol.Name = "colhTotal"
                dCol.HeaderText = Language.getMessage(mstrModuleName, 3, "Total")
                dCol.SortMode = DataGridViewColumnSortMode.NotSortable
                dCol.Width = 100
                dCol.Frozen = False
                dCol.ReadOnly = True
                dCol.DataPropertyName = "colhTotal"
                dCol.HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleRight
                dCol.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
                dCol.DefaultCellStyle.Format = GUI.fmtCurrency
                .Columns.Add(dCol)

                Dim strExpression As String = ""
                For Each pair In mdicFund
                    strExpression &= " + [|_" & pair.Key.ToString & "]"
                Next
                If strExpression.Trim <> "" Then
                    mdtTable.Columns.Add("colhTotal", System.Type.GetType("System.Decimal"), strExpression.Substring(2)).DefaultValue = 0
                End If

                dCol = New DataGridViewTextBoxColumn
                dCol.Name = "colhMessage"
                dCol.HeaderText = Language.getMessage(mstrModuleName, 8, "Remark")
                dCol.SortMode = DataGridViewColumnSortMode.NotSortable
                dCol.Width = 200
                dCol.Frozen = False
                dCol.ReadOnly = True
                dCol.DataPropertyName = "Message"
                dCol.HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleLeft
                dCol.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft
                .Columns.Add(dCol)

            End With


            m_Dataview = New DataView(mdtTable)

            m_Dataview.RowFilter = "rowtypeid <> 0 "
            If m_Dataview.Count > 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "Some transactions will not be imported. To Review Unsuccessful data Please click on Unsuccessful Data in operation menu button."), enMsgBoxStyle.Information)
            End If

            m_Dataview.RowFilter = "rowtypeid = 0 "

            dgvImportInfo.DataSource = m_Dataview
            dgvImportInfo.Refresh()

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillGirdView", mstrModuleName)
        End Try
    End Sub


#End Region

#Region " Form's Events "

    Private Sub frmImportBudgetCodes_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'S.SANDEEP [12-Jan-2018] -- START
        'ISSUE/ENHANCEMENT : REF-ID # 0001843
        'objIExcel = New ExcelData
        'S.SANDEEP [12-Jan-2018] -- END
        Try
            Call Set_Logo(Me, gApplicationType)
            Language.setLanguage(Me.Name)
            Call OtherSettings()
            Call SetColor()
            txtFilePath.ReadOnly = True
            Call FillCombo()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmImportBudgetCodes_Load", mstrModuleName)
        End Try
    End Sub

    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsBudgetcodes_master.SetMessages()
            clsBudgetcodes_Tran.SetMessages()
            objfrm._Other_ModuleNames = "clsBudgetcodes_master, clsBudgetcodes_Tran"
            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub

#End Region

#Region " Buttons "

    Private Sub objbtnSet_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSet.Click

    End Sub

    Private Sub objbtnOpenFile_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnOpenFile.Click
        Dim ofdlgOpen As New OpenFileDialog
        Dim ObjFile As FileInfo
        Try
            ofdlgOpen.InitialDirectory = ConfigParameter._Object._ExportDataPath
            ofdlgOpen.Filter = "XML files (*.xml)|*.xml|Excel files(*.xlsx)|*.xlsx"
            ofdlgOpen.FilterIndex = 2
            If ofdlgOpen.ShowDialog = Windows.Forms.DialogResult.OK Then
                ObjFile = New FileInfo(ofdlgOpen.FileName)
                txtFilePath.Text = ofdlgOpen.FileName
                Select Case ofdlgOpen.FilterIndex
                    Case 1
                        dsList.ReadXml(txtFilePath.Text)
                    Case 2
                        'S.SANDEEP [12-Jan-2018] -- START
                        'ISSUE/ENHANCEMENT : REF-ID # 0001843
                        'dsList = objIExcel.Import(txtFilePath.Text)
                        dsList = OpenXML_Import(txtFilePath.Text)
                        'S.SANDEEP [12-Jan-2018] -- END
                End Select
                Dim frm As New frmImportBudgetCodesMapping
                If frm.displayDialog(dsList) = False Then
                    mblnCancel = False
                    Exit Sub
                End If
                mdtTable = frm._DataTable
                mintBudgetCodesUnkid = frm._BudgetCodesUnkid
                mintBudgetUnkid = frm._BudgetUnkid
                mintPeriodUnkid = frm._PeriodUnkid

                'm_Dataview = New DataView(mdtTable)
                'm_Dataview.RowFilter = "rowtypeid = 0 "

                Call FillGirdView()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnOpenFile_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Try
            mblnCancel = True
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnCancel_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnImport_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnImport.Click
        Dim objBudgetCodes As New clsBudgetcodes_master
        Dim objBudget As New clsBudget_MasterNew
        Dim objBudget_Tran As New clsBudget_TranNew
        Dim objBudgetPeriod As New clsBudgetperiod_Tran
        Dim objPeriod As New clscommom_period_Tran
        Dim objBudgetHeadMapping As New clsBudgetformulaheadmapping_Tran
        Dim blnFlag As Boolean = False
        Dim dsBudget As DataSet

        mblnCancel = True

        If mdtTable.Rows.Count <= 0 Then
            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Sorry, you cannot import this file. Reason : No transactions are not there in this file to import."), enMsgBoxStyle.Information)
            Exit Sub
        End If

        m_Dataview.RowFilter = "rowtypeid = 0 "
        mdtFilteredTable = m_Dataview.ToTable

        If mdtFilteredTable.Rows.Count <= 0 Then
            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Sorry, you cannot import this file. Reason : Some project codes, activity codes or Employee Codes are not there in system."), enMsgBoxStyle.Information)
            Exit Sub
        End If

        mdtFilteredTable = New DataView(m_Dataview.ToTable, "rowtypeid = 0 AND  IsChecked = 1 ", "", DataViewRowState.CurrentRows).ToTable
        If mdtFilteredTable.Rows.Count <= 0 Then
            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 6, "Please tick atleast one transaction from list to Import."), enMsgBoxStyle.Information)
            Exit Sub
        End If


        Try
            objBudget._Budgetunkid = mintBudgetUnkid
            Dim mintViewIdx As Integer = objBudget._Allocationbyid
            Dim mintViewById As Integer = objBudget._Viewbyid
            Dim mdtBudgetDate As Date = objBudget._Budget_date.Date
            Dim mstrAllocationTranUnkIDs As String = objBudget_Tran.GetAllocationTranUnkIDs(mintBudgetUnkid)
            Dim mdicHeadMapping As Dictionary(Of Integer, Integer) = objBudgetHeadMapping.GetHeadMapping(mintBudgetUnkid)
            Dim mstrEmployeeIDs As String = ""
            Dim mstrAnalysis_Fields As String = ""
            Dim mstrAnalysis_Join As String = ""
            Dim mstrAnalysis_OrderBy As String = ""
            Dim mstrReport_GroupName As String = ""
            Dim mstrAnalysis_TableName As String = ""
            Dim mstrAnalysis_CodeField As String = ""
            Dim mstrPeriodIdList As String = objBudgetPeriod.GetPeriodIDs(mintBudgetUnkid)
            Dim strTranHeadIDList As String = String.Join(",", (From p In mdicHeadMapping Select (p.Key.ToString)).ToArray)
            Dim mstrPreviousPeriodDBName As String = objPeriod.GetDatabaseName(objBudget._PreviousPeriodyearunkid, Company._Object._Companyunkid)
            Dim dsAllHeads As DataSet = Nothing
            Dim dtViewHead As DataView
            'Sohail (02 Aug 2017) -- Start
            'Enhancement - 69.1 - New Report 'Monthly Employee Budget Variance Analysis Report'.
            objPeriod._Periodunkid(FinancialYear._Object._DatabaseName) = mintPeriodUnkid
            Dim dtPeriodStart As Date = objPeriod._Start_Date
            Dim dtPeriodEnd As Date = objPeriod._End_Date
            'Sohail (02 Aug 2017) -- End

            Dim lst_Row As List(Of DataRow) = (From p In mdtFilteredTable Where (CInt(p.Item("allocationtranunkid")) <> -1 AndAlso CDec(p.Item("colhTotal")) <> 100) Select (p)).ToList
            If lst_Row.Count > 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Sorry, Total Percentage should be 100 for all transactions."), enMsgBoxStyle.Information)
                Exit Try
            End If

            'Sohail (31 Jul 2019) -- Start
            'PACT Support Issue # 0002617 - 76.1 - Budget module should not allow to change activity percentage when budget timesheet is applied.
            If mintViewById = enBudgetViewBy.Employee AndAlso mintBudgetCodesUnkid > 0 Then

                Dim strEmpIDs As String = String.Join(",", (From p In mdtFilteredTable Select (p.Item("allocationtranunkid").ToString)).ToArray)

                Dim objEmpTS As New clsBudgetEmp_timesheet
                Dim ds As DataSet = objEmpTS.GetEmployeeTimesheetList(intPeriodID:=mintPeriodUnkid _
                                                                      , blnIsVoid:=False _
                                                                      , strFilter:=" ltbemployee_timesheet.employeeunkid IN (" & strEmpIDs & ") " _
                                                                      )

                If ds.Tables(0).Rows.Count > 0 Then
                    Dim dt As DataTable = New DataView(ds.Tables(0)).ToTable(True, "EmployeeName", "employeecode")
                    dt.Columns("EmployeeName").SetOrdinal(0)
                    dt.Columns("employeecode").SetOrdinal(1)
                    dt.Columns("EmployeeName").ColumnName = "Emp. Name"
                    dt.Columns("employeecode").ColumnName = "Emp. Code"
                    Dim objValid As New frmCommonValidationList
                    objValid.displayDialog(False, Language.getMessage(mstrModuleName, 9, "Sorry, Budget timesheet is already applied for some of the employees in selected period."), dt)
                    Exit Try
                End If
            End If
            'Sohail (31 Jul 2019) -- End

            dsList = objBudgetHeadMapping.GetListByBudgetUnkId("HeadMapping", mintBudgetUnkid, ConfigParameter._Object._CurrentDateAndTime, , False)
            Dim lstRow As List(Of DataRow) = (From p In dsList.Tables("HeadMapping") Where (mdicHeadMapping.ContainsKey(CInt(p.Item("tranheadunkid"))) = True) Select (p)).ToList

            For Each dsRow As DataRow In lstRow
                dsRow.Item("IsChecked") = True
            Next
            dsList.Tables("HeadMapping").AcceptChanges()
            dtViewHead = New DataView(dsList.Tables("HeadMapping"))

            Dim dtHead As DataTable = New DataView(dtViewHead.Table, "IsChecked = 1 ", "", DataViewRowState.CurrentRows).ToTable

            If CInt(mintViewById) = enBudgetViewBy.Allocation Then
                frmViewAnalysis.GetAnalysisByDetails("frmViewAnalysis", mintViewIdx, mstrAllocationTranUnkIDs, mdtBudgetDate.Date, "", mstrAnalysis_Fields, mstrAnalysis_Join, mstrAnalysis_OrderBy, "", mstrReport_GroupName, mstrAnalysis_TableName, mstrAnalysis_CodeField)
                If mintViewIdx = 0 Then mintViewIdx = -1
            ElseIf CInt(mintViewById) = enBudgetViewBy.Employee Then
                mstrEmployeeIDs = objBudget_Tran.getEmployeeIds(mintBudgetUnkid)
                frmEmpSelection.GetAnalysisByDetails("frmEmpSelection", mstrEmployeeIDs, "", mstrAnalysis_Fields, mstrAnalysis_Join, mstrAnalysis_OrderBy, "", mstrReport_GroupName, mstrAnalysis_TableName, mstrAnalysis_CodeField)
                mintViewIdx = 0
                mstrAllocationTranUnkIDs = mstrEmployeeIDs
            End If

            Cursor.Current = Cursors.WaitCursor

            'dsBudget = objBudgetCodes.GetDataGridList(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, objBudget._Budget_date, objBudget._Budget_date, ConfigParameter._Object._UserAccessModeSetting, True, False, objBudget._Allocationbyid, mstrAnalysis_Fields, mstrAnalysis_Join, mstrAnalysis_TableName, mstrAnalysis_OrderBy, mstrPeriodIdList, strTranHeadIDList, mintBudgetUnkid, mstrPreviousPeriodDBName, mintViewById, enBudgetPresentation.TransactionWise, CInt(objBudget._Whotoincludeid), "Budget", mintPeriodUnkid, True, "", dsAllHeads, True, mstrAnalysis_CodeField)
            'Sohail (02 Aug 2017) -- Start
            'Enhancement - 69.1 - New Report 'Monthly Employee Budget Variance Analysis Report'.
            'dsBudget = objBudgetCodes.GetDataGridList(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, objBudget._Budget_date, objBudget._Budget_date, ConfigParameter._Object._UserAccessModeSetting, True, False, objBudget._Allocationbyid, mstrAnalysis_Fields, mstrAnalysis_Join, mstrAnalysis_TableName, mstrAnalysis_OrderBy, mstrPeriodIdList, strTranHeadIDList, mintBudgetUnkid, mstrPreviousPeriodDBName, mintViewById, objBudget._Presentationmodeid, CInt(objBudget._Whotoincludeid), "Budget", mintPeriodUnkid, True, "", dsAllHeads, True, mstrAnalysis_CodeField)
            dsBudget = objBudgetCodes.GetDataGridList(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, dtPeriodStart, dtPeriodEnd, ConfigParameter._Object._UserAccessModeSetting, True, False, objBudget._Allocationbyid, mstrAnalysis_Fields, mstrAnalysis_Join, mstrAnalysis_TableName, mstrAnalysis_OrderBy, mstrPeriodIdList, strTranHeadIDList, mintBudgetUnkid, mstrPreviousPeriodDBName, mintViewById, objBudget._Presentationmodeid, CInt(objBudget._Whotoincludeid), "Budget", mintPeriodUnkid, True, "", dsAllHeads, True, mstrAnalysis_CodeField)
            'Sohail (02 Aug 2017) -- End

            Dim dtBudget As DataTable = New DataView(dsBudget.Tables(0), "Id <> -1 AND GName <> '' ", "", DataViewRowState.CurrentRows).ToTable

            Dim strExpression As String = ""
            For Each pair In mdicFund
                strExpression &= " + [|_" & pair.Key.ToString & "]"
            Next
            If strExpression.Trim <> "" Then
                dsBudget.Tables(0).Columns.Add("colhTotal", System.Type.GetType("System.Decimal"), strExpression.Substring(2)).DefaultValue = 0
            End If

            Dim dtAllHead As DataTable = New DataView(dsAllHeads.Tables(0), "Id <> -1 AND GName <> '' ", "", DataViewRowState.CurrentRows).ToTable
            Dim strFundIDs() As String = (From p In dsBudget.Tables(0).Columns.Cast(Of DataColumn)() Where (p.ColumnName.StartsWith("|_")) Select (p.ToString)).ToArray

            For Each dtRow As DataRow In mdtFilteredTable.Rows

                Dim dr() As DataRow = dtAllHead.Select("Id = " & CInt(dtRow.Item("allocationtranunkid")) & " AND allocationtranunkid = " & CInt(dtRow.Item("allocationtranunkid")) & " ")
                For Each dRow As DataRow In dr
                    For Each col As String In strFundIDs
                        dRow.Item(col) = CDec(dtRow.Item(col))
                        dRow.Item(col.Replace("|_", "||_")) = CInt(dtRow.Item(col.Replace("|_", "||_")))
                    Next
                Next

            Next
            dtAllHead.AcceptChanges()
            'lst_Row = (From p In dsBudget.Tables(0) Where (CInt(p.Item("Id")) <> -1 AndAlso CDec(p.Item("colhTotal")) <> 100) Select (p)).ToList
            'If lst_Row.Count > 0 Then
            '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Sorry, Total Percentage should be 100 for all transactions."), enMsgBoxStyle.Information)
            '    Exit Try
            'End If

            If mintBudgetCodesUnkid > 0 Then
                objBudgetCodes._Budgetcodesunkid = mintBudgetCodesUnkid
            End If
            objBudgetCodes._Budgetunkid = mintBudgetUnkid
            objBudgetCodes._PeriodUnkId = mintPeriodUnkid
            objBudgetCodes._Userunkid = User._Object._Userunkid
            With objBudgetCodes
                ._FormName = mstrModuleName
                ._LoginEmployeeunkid = 0
                ._ClientIP = getIP()
                ._HostName = getHostName()
                ._FromWeb = False
                ._AuditUserId = User._Object._Userunkid
._CompanyUnkid = Company._Object._Companyunkid
                ._AuditDate = ConfigParameter._Object._CurrentDateAndTime
            End With
            If mintBudgetCodesUnkid > 0 Then
                'Sohail (19 Apr 2017) -- Start
                'MST Enhancement - 66.1 - applying user access filter on payroll budget.
                'blnFlag = objBudgetCodes.UpdateAll(dtAllHead, dtHead, mstrPeriodIdList, ConfigParameter._Object._CurrentDateAndTime)
                'Sohail (03 May 2017) -- Start
                'Enhancement - 66.1 - Allow to include newly hired employees on budget codes screen and allow to map employees with planned employees.
                'Dim strBudgettranUnkIds As String = String.Join(",", (From p In dtAllHead Select (p.Item("budgettranunkid").ToString)).Distinct().ToArray)
                'blnFlag = objBudgetCodes.UpdateAll(dtAllHead, dtHead, mstrPeriodIdList, ConfigParameter._Object._CurrentDateAndTime, strBudgettranUnkIds)
                Dim strBudgetCodestranUnkIds As String = String.Join(",", (From p In dtAllHead Select (p.Item("budgetcodestranunkid").ToString)).Distinct().ToArray)
                blnFlag = objBudgetCodes.UpdateAll(dtAllHead, dtHead, mstrPeriodIdList, ConfigParameter._Object._CurrentDateAndTime, strBudgetCodestranUnkIds)
                'Sohail (03 May 2017) -- End
                'Sohail (19 Apr 2017) -- End
            Else
                blnFlag = objBudgetCodes.InsertAll(dtAllHead, dtHead, mstrPeriodIdList, ConfigParameter._Object._CurrentDateAndTime)
            End If

            If blnFlag = False And objBudgetCodes._Message <> "" Then
                eZeeMsgBox.Show(objBudgetCodes._Message, enMsgBoxStyle.Information)
                Exit Sub
            End If

            If blnFlag = True Then
                Cursor.Current = Cursors.Default
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 7, "Budget Codes imported successfully!"), enMsgBoxStyle.Information)
                mblnCancel = False
                Me.Close()
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnImport_Click", mstrModuleName)
        Finally
            Cursor.Current = Cursors.Default
        End Try
    End Sub


    Private Sub mnuShowSuccessful_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuShowSuccessful.Click
        Try
            btnImport.Enabled = True
            If m_Dataview Is Nothing Then Exit Sub

            m_Dataview.RowFilter = "rowtypeid = 0 "

            For Each dtRow As DataRowView In m_Dataview
                dtRow.Item("IsChecked") = False
            Next

            dgvImportInfo.DataSource = m_Dataview
            'Call FillGirdView()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "mnuShowSuccessful_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub mnuShowUnsuccessful_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuShowUnsuccessful.Click
        Try
            btnImport.Enabled = False
            If m_Dataview Is Nothing Then Exit Sub

            m_Dataview.RowFilter = "rowtypeid <> 0 "

            For Each dtRow As DataRowView In m_Dataview
                dtRow.Item("IsChecked") = False
            Next

            dgvImportInfo.DataSource = m_Dataview
            'Call FillGirdView()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "mnuShowUnsuccessful_Click", mstrModuleName)
        End Try
    End Sub


    Private Sub btnExport_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnExport.Click
        Try
            If m_Dataview Is Nothing Then Exit Sub

            Dim savDialog As New SaveFileDialog
            savDialog.Filter = "Execl files(*.xlsx)|*.xlsx"
            If savDialog.ShowDialog = Windows.Forms.DialogResult.OK Then
                If modGlobal.Export_ErrorList(savDialog.FileName, m_Dataview.ToTable, "Import Employee Wizard") = True Then
                    Process.Start(savDialog.FileName)
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnExport_Click", mstrModuleName)
        End Try
    End Sub
#End Region

#Region " Controls "

    Private Sub objchkSelectAll_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objchkSelectAll.CheckedChanged
        Try
            If m_Dataview Is Nothing Then Exit Sub

            For Each dtRow As DataRowView In m_Dataview
                dtRow.Item("IsChecked") = objchkSelectAll.Checked
            Next
            dgvImportInfo.DataSource = m_Dataview
            dgvImportInfo.Refresh()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objchkSelectAll_CheckedChanged", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " DataGridView Events "
    Private Sub dgvImportInfo_CellPainting(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellPaintingEventArgs) Handles dgvImportInfo.CellPainting
        Try
            If e.RowIndex = -1 AndAlso e.ColumnIndex > -1 Then
                Dim r2 As Rectangle = e.CellBounds
                r2.Y = CInt(r2.Y + e.CellBounds.Height / 2)
                r2.Height = CInt(e.CellBounds.Height / 2)
                e.PaintBackground(r2, True)
                e.PaintContent(r2)
                e.Handled = True
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgvImportInfo_CellPainting", mstrModuleName)
        End Try
    End Sub

    Private Sub dgvImportInfo_ColumnWidthChanged(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewColumnEventArgs) Handles dgvImportInfo.ColumnWidthChanged
        Try
            Dim rtHeader As Rectangle = Me.dgvImportInfo.DisplayRectangle
            rtHeader.Height = CInt(Me.dgvImportInfo.ColumnHeadersHeight / 2)
            Me.dgvImportInfo.Invalidate(rtHeader)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgvImportInfo_ColumnWidthChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub dgvImportInfo_DataBindingComplete(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewBindingCompleteEventArgs) Handles dgvImportInfo.DataBindingComplete
        Try
            If mdtTable Is Nothing Then Exit Try

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgvImportInfo_DataBindingComplete", mstrModuleName)
        Finally
            objbtnSearch.ShowResult("( " & m_Dataview.Count & " / " & m_Dataview.Table.Rows.Count.ToString & " )")
        End Try
    End Sub

    Private Sub dgvImportInfo_DataError(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewDataErrorEventArgs) Handles dgvImportInfo.DataError
        Try

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgvImportInfo_DataError", mstrModuleName)
        End Try
    End Sub

    Private Sub dgvImportInfo_Paint(ByVal sender As Object, ByVal e As System.Windows.Forms.PaintEventArgs) Handles dgvImportInfo.Paint
        Try
            If mdtTable Is Nothing OrElse dgvImportInfo.Columns.Count <= 2 Then Exit Sub

            Dim j As Integer = dgvImportInfo.Columns("colhFund" & mdicFund.ElementAt(0).Key.ToString).Index
            While j < dgvImportInfo.Columns("colhFund" & mdicFund.ElementAt(0).Key.ToString).Index + (mdicFund.Count * 3)
                Dim r1 As Rectangle = Me.dgvImportInfo.GetCellDisplayRectangle(j, -1, True)
                Dim w2 As Integer = Me.dgvImportInfo.GetCellDisplayRectangle(j + 1, -1, True).Width
                r1.X += 1
                r1.Y += 1
                r1.Width = r1.Width + w2 - 2
                r1.Height = CInt(r1.Height / 2 - 2)
                e.Graphics.FillRectangle(New SolidBrush(Me.dgvImportInfo.ColumnHeadersDefaultCellStyle.BackColor), r1)
                Dim format As New StringFormat()
                format.Alignment = StringAlignment.Center
                format.LineAlignment = StringAlignment.Center
                e.Graphics.DrawString(dgvImportInfo.Columns(j).Tag.ToString, Me.dgvImportInfo.ColumnHeadersDefaultCellStyle.Font, New SolidBrush(Me.dgvImportInfo.ColumnHeadersDefaultCellStyle.ForeColor), r1, format)
                j += 3
            End While
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgvImportInfo_Paint", mstrModuleName)
        End Try
    End Sub

    Private Sub dgvImportInfo_Scroll(ByVal sender As Object, ByVal e As System.Windows.Forms.ScrollEventArgs) Handles dgvImportInfo.Scroll
        Try
            Dim rtHeader As Rectangle = Me.dgvImportInfo.DisplayRectangle
            rtHeader.Height = CInt(Me.dgvImportInfo.ColumnHeadersHeight / 2)
            Me.dgvImportInfo.Invalidate(rtHeader)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgvImportInfo_Scroll", mstrModuleName)
        End Try
    End Sub

#End Region


	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Private Sub OtherSettings()
		Try
			Me.SuspendLayout()
			 
			Call SetLanguage()
			
			Me.gbFileInfo.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbFileInfo.ForeColor = GUI._eZeeContainerHeaderForeColor 


			Me.objbtnSet.GradientBackColor = GUI._ButttonBackColor 
			Me.objbtnSet.GradientForeColor = GUI._ButttonFontColor

			Me.btnExport.GradientBackColor = GUI._ButttonBackColor 
			Me.btnExport.GradientForeColor = GUI._ButttonFontColor

			Me.btnHeadOperations.GradientBackColor = GUI._ButttonBackColor 
			Me.btnHeadOperations.GradientForeColor = GUI._ButttonFontColor

			Me.btnCancel.GradientBackColor = GUI._ButttonBackColor 
			Me.btnCancel.GradientForeColor = GUI._ButttonFontColor

			Me.btnImport.GradientBackColor = GUI._ButttonBackColor 
			Me.btnImport.GradientForeColor = GUI._ButttonFontColor


			Me.ResumeLayout()
		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "OtherSettings", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetLanguage()
		Try
			Me.Text = Language._Object.getCaption(Me.Name, Me.Text)
			
			Me.gbFileInfo.Text = Language._Object.getCaption(Me.gbFileInfo.Name, Me.gbFileInfo.Text)
			Me.lblFileName.Text = Language._Object.getCaption(Me.lblFileName.Name, Me.lblFileName.Text)
			Me.btnExport.Text = Language._Object.getCaption(Me.btnExport.Name, Me.btnExport.Text)
			Me.btnHeadOperations.Text = Language._Object.getCaption(Me.btnHeadOperations.Name, Me.btnHeadOperations.Text)
			Me.btnCancel.Text = Language._Object.getCaption(Me.btnCancel.Name, Me.btnCancel.Text)
			Me.btnImport.Text = Language._Object.getCaption(Me.btnImport.Name, Me.btnImport.Text)
			Me.radApplytoChecked.Text = Language._Object.getCaption(Me.radApplytoChecked.Name, Me.radApplytoChecked.Text)
			Me.elMandatoryInfo.Text = Language._Object.getCaption(Me.elMandatoryInfo.Name, Me.elMandatoryInfo.Text)
			Me.radApplySelected.Text = Language._Object.getCaption(Me.radApplySelected.Name, Me.radApplySelected.Text)
			Me.lblHeadTypeId.Text = Language._Object.getCaption(Me.lblHeadTypeId.Name, Me.lblHeadTypeId.Text)
			Me.radApplytoAll.Text = Language._Object.getCaption(Me.radApplytoAll.Name, Me.radApplytoAll.Text)
			Me.lblCalcType.Text = Language._Object.getCaption(Me.lblCalcType.Name, Me.lblCalcType.Text)
			Me.lblTypeOfId.Text = Language._Object.getCaption(Me.lblTypeOfId.Name, Me.lblTypeOfId.Text)
			Me.DataGridViewTextBoxColumn1.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn1.Name, Me.DataGridViewTextBoxColumn1.HeaderText)
			Me.DataGridViewTextBoxColumn2.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn2.Name, Me.DataGridViewTextBoxColumn2.HeaderText)
			Me.DataGridViewTextBoxColumn3.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn3.Name, Me.DataGridViewTextBoxColumn3.HeaderText)
			Me.DataGridViewTextBoxColumn4.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn4.Name, Me.DataGridViewTextBoxColumn4.HeaderText)
			Me.DataGridViewTextBoxColumn5.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn5.Name, Me.DataGridViewTextBoxColumn5.HeaderText)
			Me.DataGridViewTextBoxColumn6.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn6.Name, Me.DataGridViewTextBoxColumn6.HeaderText)
			Me.mnuShowSuccessful.Text = Language._Object.getCaption(Me.mnuShowSuccessful.Name, Me.mnuShowSuccessful.Text)
			Me.mnuShowUnsuccessful.Text = Language._Object.getCaption(Me.mnuShowUnsuccessful.Name, Me.mnuShowUnsuccessful.Text)

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetLanguage", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "Sorry, you cannot import this file. Reason : No transactions are not there in this file to import.")
			Language.setMessage(mstrModuleName, 2, "Sorry, you cannot import this file. Reason : Some project codes, activity codes or Employee Codes are not there in system.")
			Language.setMessage(mstrModuleName, 3, "Total")
			Language.setMessage(mstrModuleName, 4, "Sorry, Total Percentage should be 100 for all transactions.")
			Language.setMessage(mstrModuleName, 5, "Some transactions will not be imported. To Review Unsuccessful data Please click on Unsuccessful Data in operation menu button.")
			Language.setMessage(mstrModuleName, 6, "Please tick atleast one transaction from list to Import.")
			Language.setMessage(mstrModuleName, 7, "Budget Codes imported successfully!")
			Language.setMessage(mstrModuleName, 8, "Remark")
			Language.setMessage(mstrModuleName, 9, "Sorry, Budget timesheet is already applied for some of the employees in selected period.")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class