﻿Option Strict On

Imports eZeeCommonLib
Imports Aruti.Data
Imports System.Threading

Public Class frmFundActivityAdjustment_List

#Region " Private Variables "
    Private ReadOnly mstrModuleName As String = "frmFundActivityAdjustment_List"
    Private objFundActivityAdjustment As clsFundActivityAdjustment_Tran
    Private trd As Thread
#End Region

#Region " Form's Events "

    Private Sub frmFundActivityAdjustment_List_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        Me.Close()
    End Sub

    Private Sub frmFundActivityAdjustment_List_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles Me.KeyPress
        Try
            If Asc(e.KeyChar) = 27 Then
                Me.Close()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmFundActivityAdjustment_List_KeyPress", mstrModuleName)
        End Try
    End Sub

    Private Sub frmFundActivityAdjustment_List_KeyUp(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyUp
        Try
            If e.KeyCode = Keys.Delete And dgvFundActivityAdjustment.Focused = True Then
                Call btnDelete.PerformClick()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmFundActivityAdjustment_List_KeyUp", mstrModuleName)
        End Try
    End Sub

    Private Sub frmFundActivityAdjustment_List_LanguageClick(ByVal sender As Object, ByVal eventArgs As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsFundActivityAdjustment_Tran.SetMessages()
            objfrm._Other_ModuleNames = "clsFundActivityAdjustment_Tran"
            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmFundActivityAdjustment_List_LanguageClick", mstrModuleName)
        End Try
    End Sub

    Private Sub frmFundActivityAdjustment_List_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Call Set_Logo(Me, gApplicationType)
            Language.setLanguage(Me.Name)
            Call OtherSettings()
            Call SetVisibility()
            Call FillCombo()
            Call FillList()

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmFundActivityAdjustment_List_Load", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Private Methods "

    Private Sub FillCombo()
        Try
            Dim dsList As New DataSet
            Dim objFundActivity As New clsfundactivity_Tran

            dsList = objFundActivity.GetComboList("List", True)
            With cboActivityName
                .ValueMember = "fundactivityunkid"
                .DisplayMember = "activityname"
                .DataSource = dsList.Tables("List")
                .SelectedValue = 0
            End With

            objFundActivity = Nothing
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        End Try
    End Sub

    Private Sub FillList()
        Try
            Dim mstrSearch As String = String.Empty
            Dim dsList As New DataSet
            objFundActivityAdjustment = New clsFundActivityAdjustment_Tran

            If User._Object.Privilege._AllowToViewFundActivityAdjustment = False Then Exit Sub

            If CInt(cboActivityName.SelectedValue) > 0 Then
                mstrSearch &= "AND bgFundActivityAdjustment_tran.fundactivityunkid = " & CInt(cboActivityName.SelectedValue) & " "
            End If

            If dtpTransactionFrom.Checked = True Then
                mstrSearch &= "AND CONVERT(CHAR(8),bgFundActivityAdjustment_tran.transactiondate,112) >= " & eZeeDate.convertDate(dtpTransactionFrom.Value) & " "
            End If

            If dtpTransactionTo.Checked = True Then
                mstrSearch &= "AND CONVERT(CHAR(8),bgFundActivityAdjustment_tran.transactiondate,112) <= " & eZeeDate.convertDate(dtpTransactionTo.Value) & " "
            End If

            If txtCurrentBalFrom.Text.Trim <> "" AndAlso txtCurrentBalFrom.Decimal > 0 Then
                mstrSearch &= "AND bgFundActivityAdjustment_tran.newbalance >= " & txtCurrentBalFrom.Decimal & " "
            End If

            If txtCurrentBalTo.Text.Trim <> "" AndAlso txtCurrentBalTo.Decimal > 0 Then
                mstrSearch &= "AND bgFundActivityAdjustment_tran.newbalance <= " & txtCurrentBalTo.Decimal & " "
            End If

            If mstrSearch.Trim.Length > 0 Then
                mstrSearch = mstrSearch.Substring(3)
            End If

            dsList = objFundActivityAdjustment.GetList("List", mstrSearch)

            dgvFundActivityAdjustment.AutoGenerateColumns = False

            'Sohail (22 Nov 2016) -- Start
            'Enhancement #24 -  65.1 - budget changes (Add Activity Code Column for differentiating those activities having the same name on the activity list screen).
            dgcolhProjectName.DataPropertyName = "fundprojectname"
            dgcolhActivityCode.DataPropertyName = "activity_code"
            'Sohail (22 Nov 2016) -- End
            dgcolhActivityName.DataPropertyName = "activity_name"
            dgcolhTarnsactionDate.DataPropertyName = "transactiondate"
            dgcolhTarnsactionDate.DefaultCellStyle.Format = Globalization.CultureInfo.CurrentCulture.DateTimeFormat.ShortDatePattern
            dgcolhCurrentBal.DataPropertyName = "currentbalance"
            dgcolhCurrentBal.HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleRight
            dgcolhCurrentBal.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
            dgcolhCurrentBal.DefaultCellStyle.Format = GUI.fmtCurrency
            dgcolhIncrDecrAmount.DataPropertyName = "incrdecramount"
            dgcolhIncrDecrAmount.HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleRight
            dgcolhIncrDecrAmount.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
            dgcolhIncrDecrAmount.DefaultCellStyle.Format = GUI.fmtCurrency
            dgcolhNewBalance.DataPropertyName = "newbalance"
            dgcolhNewBalance.HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleRight
            dgcolhNewBalance.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
            dgcolhNewBalance.DefaultCellStyle.Format = GUI.fmtCurrency
            objdgcolhFundActivityAdjustmentunkid.DataPropertyName = "FundActivityAdjustmentunkid"
            objdgcolhfundactivityunkid.DataPropertyName = "fundactivityunkid"
            'Sohail (23 May 2017) -- Start
            'Enhancement - 67.1 - Link budget with Payroll.
            objdgcolhPaymenttranunkid.DataPropertyName = "paymenttranunkid"
            objdgcolhGlobalvocunkid.DataPropertyName = "globalvocunkid"
            'Sohail (23 May 2017) -- End

            dgvFundActivityAdjustment.DataSource = dsList.Tables("List")

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillList", mstrModuleName)
        Finally
            objbtnSearch.ShowResult(dgvFundActivityAdjustment.RowCount.ToString)
        End Try
    End Sub

    Private Sub SetVisibility()
        Try
            btnNew.Enabled = User._Object.Privilege._AllowToAddFundActivityAdjustment
            btnEdit.Enabled = User._Object.Privilege._AllowToEditFundActivityAdjustment
            btnDelete.Enabled = User._Object.Privilege._AllowToDeleteFundActivityAdjustment

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetVisibility", mstrModuleName)
        End Try
    End Sub

    Private Sub ResetParameters()
        Try
            cboActivityName.SelectedValue = 0
            dtpTransactionFrom.Value = ConfigParameter._Object._CurrentDateAndTime.Date
            dtpTransactionFrom.Checked = False
            dtpTransactionTo.Value = ConfigParameter._Object._CurrentDateAndTime.Date
            dtpTransactionTo.Checked = False
            txtCurrentBalFrom.Decimal = 0
            txtCurrentBalTo.Decimal = 0

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "ResetParameters", mstrModuleName)
        End Try
    End Sub

    Private Function Set_Notification(ByVal strUserName As String _
                                  , ByVal strActivityName As String _
                                  , ByVal decNewBalance As Decimal _
                                  , ByVal strNotifyAmount As String _
                                  ) As String

        Dim StrMessage As New System.Text.StringBuilder
        Dim blnFlag As Boolean = False
        Try
            StrMessage.Append("<HTML><BODY>")
            StrMessage.Append(vbCrLf)

            If ConfigParameter._Object._Notify_Budget_Users.Trim.Length > 0 Then
                StrMessage.Append("<p><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#333399; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>")

                'Gajanan [27-Mar-2019] -- Start
                'Enhancement - Change Email Language
                'StrMessage.Append("Dear <b>" & strUserName & "</b>,</span></p>")
                StrMessage.Append(Language.getMessage(mstrModuleName, 14, "Dear") & " " & "<b>" & getTitleCase(strUserName) & "</b></span></p>")
                'Gajanan [27-Mar-2019] -- End

                StrMessage.Append(vbCrLf)
                StrMessage.Append("<p><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#333399; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>")

                'Gajanan [27-Mar-2019] -- Start
                'Enhancement - Change Email Language

                'StrMessage.Append("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; This is to inform you that Current Balance is less than Notification amount that was set on Fund Adjustment screen</b></span></p>")
                StrMessage.Append(" " & Language.getMessage(mstrModuleName, 15, "This is to inform you that Current Balance is less than Notification amount that was set on Fund Adjustment screen") & "</b></span></p>")
                'Gajanan [27-Mar-2019] -- End

                StrMessage.Append(vbCrLf)


                'Gajanan [27-Mar-2019] -- Start
                'Enhancement - Change Email Language

                'StrMessage.Append("<TABLE border='1' style='margin-left:50px;width:502px;font-size:9.0pt;'>")
                StrMessage.Append("<TABLE border='1' style='width:502px;font-size:9.0pt;'>")
                'Gajanan [27-Mar-2019] -- End

                StrMessage.Append("<TR style='background-color:Purple;color:White; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & ";'>")

                StrMessage.Append("<TD align='center' style='width:300px'>")
                StrMessage.Append(Language.getMessage(mstrModuleName, 6, "Fund Name"))
                StrMessage.Append("</TD>")

                StrMessage.Append("<TD align='center' style='width:200px'>")
                StrMessage.Append(Language.getMessage(mstrModuleName, 7, "Current Balance"))
                StrMessage.Append("</TD>")

                StrMessage.Append("<TD align='center' style='width:300px'>")
                StrMessage.Append(Language.getMessage(mstrModuleName, 8, "Notification Amount"))
                StrMessage.Append("</TD>")

                StrMessage.Append("</TR>")
                '-----------------------------

                StrMessage.Append("<TR style = 'font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#333399;'>")
                StrMessage.Append("<TD style='width:300px'>")
                StrMessage.Append(strActivityName)
                StrMessage.Append("</TD>")

                StrMessage.Append("<TD align='right' style='width:200px'>")
                StrMessage.Append(decNewBalance.ToString(GUI.fmtCurrency))
                StrMessage.Append("</TD>")

                StrMessage.Append("<TD align='right' style='width:300px'>")
                StrMessage.Append(strNotifyAmount)
                StrMessage.Append("</TD>")

                StrMessage.Append("</TR>")
                '-----------------------------


                StrMessage.Append("</TABLE>")

                blnFlag = True


                'Gajanan [27-Mar-2019] -- Start
                'Enhancement - Change Email Language

                'StrMessage.Append("<p><span style='padding-top:25px;font-size:10.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#333399;margin-left:0px;margin-right:0px;margin-top:0px;margin-bottom:10px;'><b>&nbsp;&nbsp;&nbsp;&nbsp;&quot;POWERED BY ARUTI HR & PAYROLL MANAGEMENT SOFTWARE.&quot;</b>")
                StrMessage.Append("<p><span style='padding-top:25px;font-size:10.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#333399;margin-left:0px;margin-right:0px;margin-top:0px;margin-bottom:10px;'><b>&quot;POWERED BY ARUTI HR & PAYROLL MANAGEMENT SOFTWARE.&quot;</b>")

                'Gajanan [27-Mar-2019] -- End

                StrMessage.Append("</span></p>")
                StrMessage.Append("</BODY></HTML>")

                If blnFlag = False Then
                    StrMessage = StrMessage.Remove(0, StrMessage.Length)
                End If
            End If
            Return StrMessage.ToString
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "Set_Notification", mstrModuleName)
            Return ""
        End Try
    End Function

    Private Sub Send_Notification()
        Try

            If gobjEmailList.Count > 0 Then
                Dim objSendMail As New clsSendMail
                For Each obj In gobjEmailList
                    objSendMail._ToEmail = obj._EmailTo
                    objSendMail._Subject = obj._Subject
                    objSendMail._Message = obj._Message
                    objSendMail._FormName = obj._FormName
                    objSendMail._ClientIP = getIP()
                    objSendMail._HostName = getHostName()
                    objSendMail._FromWeb = False
                    objSendMail._AuditUserId = User._Object._Userunkid
objSendMail._CompanyUnkid = Company._Object._Companyunkid
                    objSendMail._AuditDate = ConfigParameter._Object._CurrentDateAndTime
                    objSendMail._LoginEmployeeunkid = -1
                    objSendMail._OperationModeId = enLogin_Mode.DESKTOP
                    objSendMail._UserUnkid = User._Object._Userunkid
                    objSendMail._SenderAddress = User._Object._Email
                    objSendMail._ModuleRefId = clsSendMail.enAT_VIEW_TYPE.PAYROLL_MGT
                    Try
                        'Sohail (30 Nov 2017) -- Start
                        'SUMATRA Enhancement – SUMATRA – issue # 0001669: Additional feature for Exchange Server configuration setting in 70.1.
                        'objSendMail.SendMail()
                        objSendMail.SendMail(Company._Object._Companyunkid)
                        'Sohail (30 Nov 2017) -- End
                    Catch ex As Exception

                    End Try
                Next
                gobjEmailList.Clear()
            End If
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "Send_Notification", mstrModuleName)
        Finally
            If gobjEmailList.Count > 0 Then
                gobjEmailList.Clear()
            End If
        End Try
    End Sub
#End Region

#Region " Button's Events "

    Private Sub btnNew_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnNew.Click
        Dim frm As New frmFundActivityAdjustment_AddEdit
        Try
            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If

            If frm.displayDialog(-1, enAction.ADD_CONTINUE) Then
                Call FillList()
                Call FillCombo()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnNew_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnEdit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnEdit.Click
        Dim frm As New frmFundActivityAdjustment_AddEdit
        Try
            If dgvFundActivityAdjustment.RowCount <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please select atleast one row for further operation."), enMsgBoxStyle.Information)
                dgvFundActivityAdjustment.Focus()
                Exit Sub
                'Sohail (16 Jun 2017) -- Start
                'Enhancement - 68.1 - C/F tables on close year.
            ElseIf CDate(dgvFundActivityAdjustment.SelectedRows(0).Cells(dgcolhTarnsactionDate.Index).Value).Date < FinancialYear._Object._Database_Start_Date.Date Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 13, "Sorry, You cannot edit/delete this transaction. Reason: This is previous year transaction."), enMsgBoxStyle.Information)
                dgvFundActivityAdjustment.Focus()
                Exit Sub
                'Sohail (16 Jun 2017) -- End
            End If

            'Sohail (23 May 2017) -- Start
            'Enhancement - 67.1 - Link budget with Payroll.
            If CInt(dgvFundActivityAdjustment.SelectedRows(0).Cells(objdgcolhPaymenttranunkid.Index).Value) > 0 OrElse CInt(dgvFundActivityAdjustment.SelectedRows(0).Cells(objdgcolhGlobalvocunkid.Index).Value) > 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 12, "Sorry, You cannot edit/delete this adjustment. Please void the associate payment from payslip payment screen."), enMsgBoxStyle.Information)
                Exit Try
            End If
            'Sohail (23 May 2017) -- End

            If frm.displayDialog(CInt(dgvFundActivityAdjustment.SelectedRows(0).Cells(objdgcolhFundActivityAdjustmentunkid.Index).Value), enAction.EDIT_ONE) Then
                Call FillList()
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnEdit_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDelete.Click
        Dim objFundActivity As New clsfundactivity_Tran
        Try
            If dgvFundActivityAdjustment.SelectedRows.Count <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please select atleast one row for further operation."), enMsgBoxStyle.Information)
                dgvFundActivityAdjustment.Focus()
                Exit Try
                'Sohail (16 Jun 2017) -- Start
                'Enhancement - 68.1 - C/F tables on close year.
            ElseIf CDate(dgvFundActivityAdjustment.SelectedRows(0).Cells(dgcolhTarnsactionDate.Index).Value).Date < FinancialYear._Object._Database_Start_Date.Date Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 13, "Sorry, You cannot edit/delete this transaction. Reason: This is previous year transaction."), enMsgBoxStyle.Information)
                dgvFundActivityAdjustment.Focus()
                Exit Sub
                'Sohail (16 Jun 2017) -- End
            End If


            objFundActivity._Fundactivityunkid = CInt(dgvFundActivityAdjustment.SelectedRows(0).Cells(objdgcolhfundactivityunkid.Index).Value)
            'Sohail (22 Nov 2016) -- Start
            'Enhancement #24 -  65.1 - Increase / decrease fund project code balance on activity adjustment as per comment given by andrew.
            'Dim mdecBalance As Decimal = 0
            'Dim strProjectCode As String = ""
            'Dim mdecActivityTotal As Decimal = objFundActivity.GetFundActivityTotal(objFundActivity._FundProjectCodeunkid, mdecBalance, strProjectCode)

            'If (mdecActivityTotal - CDec(dgvFundActivityAdjustment.SelectedRows(0).Cells(dgcolhIncrDecrAmount.Index).Value)) > mdecBalance Then
            '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Sorry, Total Amount of all activities in selected Fund Project Code should not be greater than selected Fund Project Code ceiling amount.") & vbCrLf & vbCrLf & strProjectCode & " Project Code Balance : " & Format(mdecBalance, GUI.fmtCurrency) & vbCrLf & "Expected Total of Activities in the Project Code : " & Format((mdecActivityTotal - CDec(dgvFundActivityAdjustment.SelectedRows(0).Cells(dgcolhIncrDecrAmount.Index).Value)), GUI.fmtCurrency), enMsgBoxStyle.Information) '?1
            '    Exit Try
            'End If
            Dim dtTransDate As Date = CDate(dgvFundActivityAdjustment.SelectedRows(0).Cells(dgcolhTarnsactionDate.Index).Value)
            Dim objFundActAdj As New clsFundActivityAdjustment_Tran
            If objFundActAdj.isLastTransaction(CInt(dgvFundActivityAdjustment.SelectedRows(0).Cells(objdgcolhfundactivityunkid.Index).Value), dtTransDate, CInt(dgvFundActivityAdjustment.SelectedRows(0).Cells(objdgcolhFundActivityAdjustmentunkid.Index).Value)) = False Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 11, "Please define another Transaction date. Reason: Transaction date must be greater than last activity adjustments date.") & " " & dtTransDate.ToShortDateString, enMsgBoxStyle.Information)
                Exit Try
            End If

            Dim objFundAdj As New clsFundAdjustment_Tran
            If objFundAdj.isLastTransaction(objFundActivity._FundProjectCodeunkid, dtTransDate, 0) = False Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Please define another Transaction date. Reason: Transaction date must be greater than last fund adjustments date.") & " " & dtTransDate.ToShortDateString, enMsgBoxStyle.Information)
                Exit Try
            End If
            'Sohail (22 Nov 2016) -- End

            'Sohail (23 May 2017) -- Start
            'Enhancement - 67.1 - Link budget with Payroll.
            If CInt(dgvFundActivityAdjustment.SelectedRows(0).Cells(objdgcolhPaymenttranunkid.Index).Value) > 0 OrElse CInt(dgvFundActivityAdjustment.SelectedRows(0).Cells(objdgcolhGlobalvocunkid.Index).Value) > 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 12, "Sorry, You cannot edit/delete this adjustment. Please void the associate payment from payslip payment screen."), enMsgBoxStyle.Information)
                Exit Try
            End If
            'Sohail (23 May 2017) -- End

            Dim decPerc As Decimal = 100
            If ConfigParameter._Object._Notify_Budget_FundActivityPercentage > 0 Then
                decPerc = ConfigParameter._Object._Notify_Budget_FundActivityPercentage
            End If
            If ((objFundActivity._Notify_Amount * decPerc) / 100) > CDec(dgvFundActivityAdjustment.SelectedRows(0).Cells(dgcolhCurrentBal.Index).Value) Then
                If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "New Balance is going to be less than Notification amount set on Fund Activity screen.") & vbCrLf & vbCrLf & Language.getMessage(mstrModuleName, 5, "Do you want to continue?"), CType(enMsgBoxStyle.Information + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.No Then '?1
                    Exit Try
                End If
            End If

            If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Are you sure you want to Delete this Fund Activity?"), CType(enMsgBoxStyle.Question + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.Yes Then

                If gobjEmailList.Count > 0 AndAlso ConfigParameter._Object._Notify_Budget_Users.Trim.Length > 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 10, "Sending Email(s) process is in progress from other module. Please wait."), enMsgBoxStyle.Information)
                    Exit Try
                End If

                objFundActivityAdjustment = New clsFundActivityAdjustment_Tran

                Dim frm As New frmReasonSelection
                Dim mstrVoidReason As String = String.Empty
                frm.displayDialog(enVoidCategoryType.PAYROLL, mstrVoidReason)
                If mstrVoidReason.Length <= 0 Then
                    Exit Sub
                Else
                    objFundActivityAdjustment._Voidreason = mstrVoidReason
                End If
                frm = Nothing

                objFundActivityAdjustment._Isvoid = True
                objFundActivityAdjustment._Voiddatetime = ConfigParameter._Object._CurrentDateAndTime
                objFundActivityAdjustment._Voiduserunkid = User._Object._Userunkid


                'S.SANDEEP [28-May-2018] -- START
                'ISSUE/ENHANCEMENT : {Audit Trails} 
                objFundActivityAdjustment._FormName = mstrModuleName
                objFundActivityAdjustment._LoginEmployeeunkid = 0
                objFundActivityAdjustment._ClientIP = getIP()
                objFundActivityAdjustment._HostName = getHostName()
                objFundActivityAdjustment._FromWeb = False
                objFundActivityAdjustment._AuditUserId = User._Object._Userunkid
objFundActivityAdjustment._CompanyUnkid = Company._Object._Companyunkid
                objFundActivityAdjustment._AuditDate = ConfigParameter._Object._CurrentDateAndTime
                'S.SANDEEP [28-May-2018] -- END

                If objFundActivityAdjustment.Delete(CInt(dgvFundActivityAdjustment.SelectedRows(0).Cells(objdgcolhFundActivityAdjustmentunkid.Index).Value), _
                                            ConfigParameter._Object._CurrentDateAndTime, Nothing) Then


                    If ConfigParameter._Object._Notify_Budget_Users.Trim.Length > 0 Then
                        gobjEmailList = New List(Of clsEmailCollection)
                        Dim objUsr As New clsUserAddEdit : Dim StrMessage As String = String.Empty

                        For Each sId As String In ConfigParameter._Object._Notify_Budget_Users.Split(CChar(","))
                            If sId.Trim = "" Then Continue For

                            objUsr._Userunkid = CInt(sId)
                            Dim strUserName As String = objUsr._Firstname & " " & objUsr._Lastname
                            If strUserName.Trim = "" Then strUserName = objUsr._Username

                            'StrMessage = Set_Notification(objUsr._Firstname & " " & objUsr._Lastname, dgvFundActivityAdjustment.SelectedRows(0).Cells(dgcolhActivityName.Index).Value.ToString, CDec(dgvFundActivityAdjustment.SelectedRows(0).Cells(dgcolhCurrentBal.Index).Value), Format((objFundActivity._Notify_Amount * decPerc) / 100, GUI.fmtCurrency) & " (" & decPerc.ToString & " % of " & Format(objFundActivity._Notify_Amount, GUI.fmtCurrency) & ")")
                            StrMessage = Set_Notification(strUserName, dgvFundActivityAdjustment.SelectedRows(0).Cells(dgcolhActivityName.Index).Value.ToString, CDec(dgvFundActivityAdjustment.SelectedRows(0).Cells(dgcolhCurrentBal.Index).Value), Format((objFundActivity._Notify_Amount * decPerc) / 100, GUI.fmtCurrency))

                            gobjEmailList.Add(New clsEmailCollection(objUsr._Email, dgvFundActivityAdjustment.SelectedRows(0).Cells(dgcolhActivityName.Index).Value.ToString & " " & Language.getMessage(mstrModuleName, 9, "Current Balance is getting low."), StrMessage, "", 0, "", "", 0, 0, 0, ""))
                        Next
                        objUsr = Nothing

                        trd = New Thread(AddressOf Send_Notification)
                        trd.IsBackground = True
                        trd.Start()
                    End If
                    Call FillList()
                    Call FillCombo()
                Else
                    If objFundActivityAdjustment._Message <> "" Then
                        eZeeMsgBox.Show(objFundActivityAdjustment._Message, enMsgBoxStyle.Information)
                        Exit Sub
                    End If
                End If

            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnDelete_Click", mstrModuleName)
        Finally
            objFundActivity = Nothing
        End Try
    End Sub

    Private Sub btnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnClose_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnSearchActivityName_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchActivityName.Click
        Try
            Dim objfrm As New frmCommonSearch

            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            With objfrm
                .ValueMember = cboActivityName.ValueMember
                .DisplayMember = cboActivityName.DisplayMember
                .DataSource = CType(cboActivityName.DataSource, DataTable)
                .CodeMember = "activitycode"
            End With

            If objfrm.DisplayDialog Then
                cboActivityName.SelectedValue = objfrm.SelectedValue
                cboActivityName.Focus()
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchFundName_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnSearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearch.Click
        Try
            Call FillList()

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearch_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnReset.Click
        Try
            Call ResetParameters()
            Call FillList()

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnReset_Click", mstrModuleName)
        End Try
    End Sub

#End Region

#Region "DataGrid's Events"

    Private Sub dgvFundActivityAdjustment_SelectionChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles dgvFundActivityAdjustment.SelectionChanged
        Try
            If dgvFundActivityAdjustment.SelectedRows.Count > 0 Then

                Dim blnEnable = CType(dgvFundActivityAdjustment.DataSource, DataTable).AsEnumerable().Where(Function(x) x.Field(Of Date)("transactiondate") > _
                               CDate(dgvFundActivityAdjustment.SelectedRows(0).Cells(dgcolhTarnsactionDate.Index).Value) _
                               And x.Field(Of Integer)("fundactivityunkid") = CInt(dgvFundActivityAdjustment.SelectedRows(0).Cells(objdgcolhfundactivityunkid.Index).Value)).Count <= 0

                btnEdit.Enabled = blnEnable
                btnDelete.Enabled = blnEnable
            End If


        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgvFundActivityAdjustment_SelectionChanged", mstrModuleName)
        End Try
    End Sub

#End Region



    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Private Sub OtherSettings()
        Try
            Me.SuspendLayout()
           
            Call SetLanguage()

            Me.gbFilterCriteria.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbFilterCriteria.ForeColor = GUI._eZeeContainerHeaderForeColor


            Me.btnDelete.GradientBackColor = GUI._ButttonBackColor
            Me.btnDelete.GradientForeColor = GUI._ButttonFontColor

            Me.btnEdit.GradientBackColor = GUI._ButttonBackColor
            Me.btnEdit.GradientForeColor = GUI._ButttonFontColor

            Me.btnNew.GradientBackColor = GUI._ButttonBackColor
            Me.btnNew.GradientForeColor = GUI._ButttonFontColor

            Me.btnClose.GradientBackColor = GUI._ButttonBackColor
            Me.btnClose.GradientForeColor = GUI._ButttonFontColor


            Me.ResumeLayout()
        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "OtherSettings", mstrModuleName)
        End Try
    End Sub


    Private Sub SetLanguage()
        Try
            Me.Text = Language._Object.getCaption(Me.Name, Me.Text)

            Me.gbFilterCriteria.Text = Language._Object.getCaption(Me.gbFilterCriteria.Name, Me.gbFilterCriteria.Text)
            Me.lblActivityName.Text = Language._Object.getCaption(Me.lblActivityName.Name, Me.lblActivityName.Text)
            Me.dgvFundActivityAdjustment.Text = Language._Object.getCaption(Me.dgvFundActivityAdjustment.Name, Me.dgvFundActivityAdjustment.Text)
            Me.btnDelete.Text = Language._Object.getCaption(Me.btnDelete.Name, Me.btnDelete.Text)
            Me.btnEdit.Text = Language._Object.getCaption(Me.btnEdit.Name, Me.btnEdit.Text)
            Me.btnNew.Text = Language._Object.getCaption(Me.btnNew.Name, Me.btnNew.Text)
            Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
            Me.lblTranFromDt.Text = Language._Object.getCaption(Me.lblTranFromDt.Name, Me.lblTranFromDt.Text)
            Me.lblTranToDt.Text = Language._Object.getCaption(Me.lblTranToDt.Name, Me.lblTranToDt.Text)
            Me.lblCurrentBalTo.Text = Language._Object.getCaption(Me.lblCurrentBalTo.Name, Me.lblCurrentBalTo.Text)
            Me.lblCurrentBalFrom.Text = Language._Object.getCaption(Me.lblCurrentBalFrom.Name, Me.lblCurrentBalFrom.Text)
            Me.dgcolhProjectName.HeaderText = Language._Object.getCaption(Me.dgcolhProjectName.Name, Me.dgcolhProjectName.HeaderText)
            Me.dgcolhActivityCode.HeaderText = Language._Object.getCaption(Me.dgcolhActivityCode.Name, Me.dgcolhActivityCode.HeaderText)
            Me.dgcolhActivityName.HeaderText = Language._Object.getCaption(Me.dgcolhActivityName.Name, Me.dgcolhActivityName.HeaderText)
            Me.dgcolhTarnsactionDate.HeaderText = Language._Object.getCaption(Me.dgcolhTarnsactionDate.Name, Me.dgcolhTarnsactionDate.HeaderText)
            Me.dgcolhCurrentBal.HeaderText = Language._Object.getCaption(Me.dgcolhCurrentBal.Name, Me.dgcolhCurrentBal.HeaderText)
            Me.dgcolhIncrDecrAmount.HeaderText = Language._Object.getCaption(Me.dgcolhIncrDecrAmount.Name, Me.dgcolhIncrDecrAmount.HeaderText)
            Me.dgcolhNewBalance.HeaderText = Language._Object.getCaption(Me.dgcolhNewBalance.Name, Me.dgcolhNewBalance.HeaderText)

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
        End Try
    End Sub


    Private Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Please select atleast one row for further operation.")
            Language.setMessage(mstrModuleName, 2, "Please define another Transaction date. Reason: Transaction date must be greater than last fund adjustments date.")
            Language.setMessage(mstrModuleName, 3, "Are you sure you want to Delete this Fund Activity?")
            Language.setMessage(mstrModuleName, 4, "New Balance is going to be less than Notification amount set on Fund Activity screen.")
            Language.setMessage(mstrModuleName, 5, "Do you want to continue?")
            Language.setMessage(mstrModuleName, 6, "Fund Name")
            Language.setMessage(mstrModuleName, 7, "Current Balance")
            Language.setMessage(mstrModuleName, 8, "Notification Amount")
            Language.setMessage(mstrModuleName, 9, "Current Balance is getting low.")
            Language.setMessage(mstrModuleName, 10, "Sending Email(s) process is in progress from other module. Please wait.")
            Language.setMessage(mstrModuleName, 11, "Please define another Transaction date. Reason: Transaction date must be greater than last activity adjustments date.")
	    Language.setMessage(mstrModuleName, 12, "Sorry, You cannot edit/delete this adjustment. Please void the associate payment from payslip payment screen.")
	    Language.setMessage(mstrModuleName, 13, "Sorry, You cannot edit/delete this transaction. Reason: This is previous year transaction.")
			Language.setMessage(mstrModuleName, 14, "Dear")
			Language.setMessage(mstrModuleName, 15, "This is to inform you that Current Balance is less than Notification amount that was set on Fund Adjustment screen")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class