﻿Option Strict On

#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data

#End Region

Public Class frmPlannedEmployeeMapping

#Region " Private Variables "
    Private ReadOnly mstrModuleName As String = "frmPlannedEmployeeMapping"
    Private mblnCancel As Boolean = True

    Private mdtPeriodStartDate As DateTime
    Private mdtPeriodEndDate As DateTime
    Private mdvTable As DataView
    Private marrEmp() As String
#End Region

#Region " Private Methods "

    Private Sub SetColor()
        Try
            cboBudget.BackColor = GUI.ColorComp
            cboPeriod.BackColor = GUI.ColorComp
            cboJob.BackColor = GUI.ColorOptional
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetColor", mstrModuleName)
        End Try
    End Sub

    Private Sub FilCombo()
        Dim objBudgetMaster As New clsBudget_MasterNew
        Dim objPeriod As New clscommom_period_Tran
        Dim objMaster As New clsMasterData
        Dim dsCombos As New DataSet
        Dim mdtTable As DataTable
        Try

            dsCombos = objPeriod.getListForCombo(enModuleReference.Payroll, FinancialYear._Object._YearUnkid, FinancialYear._Object._DatabaseName, FinancialYear._Object._Database_Start_Date, "Period", True, enStatusType.Open)
            'dsCombos = objPeriod.getListForCombo(enModuleReference.Payroll, FinancialYear._Object._YearUnkid, FinancialYear._Object._DatabaseName, FinancialYear._Object._Database_Start_Date, "Period", True, )

            Dim intFirstPeriodId As Integer = objMaster.getFirstPeriodID(enModuleReference.Payroll, FinancialYear._Object._YearUnkid, enStatusType.Open)
            If intFirstPeriodId > 0 Then
                mdtTable = New DataView(dsCombos.Tables(0), "periodunkid = " & intFirstPeriodId & " ", "", DataViewRowState.CurrentRows).ToTable
            Else
                mdtTable = New DataView(dsCombos.Tables(0), "periodunkid = 0 ", "", DataViewRowState.CurrentRows).ToTable
            End If

            With cboPeriod
                .BeginUpdate()
                .ValueMember = "periodunkid"
                .DisplayMember = "name"
                .DataSource = mdtTable
                If .Items.Count > 0 Then .SelectedIndex = 0
                .EndUpdate()
            End With

            dsCombos = objBudgetMaster.GetComboList("List", True, True, "viewbyid = " & enBudgetViewBy.Employee & " AND whotoincludeid = " & enBudgetWhoToInclude.AllAsPerManPowerPlan & " ", )
            With cboBudget
                .ValueMember = "budgetunkid"
                .DisplayMember = "Name"
                .DataSource = dsCombos.Tables("List")
                .SelectedValue = 0
            End With


        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FiilCombo", mstrModuleName)
        Finally
            objBudgetMaster = Nothing
            objPeriod = Nothing
            objMaster = Nothing
        End Try
    End Sub

    Private Sub FillJob()
        Dim objBudgetTran As New clsBudget_TranNew
        Dim ds As DataSet
        Try

            If CInt(cboBudget.SelectedValue) <= 0 OrElse CInt(cboPeriod.SelectedValue) <= 0 Then Exit Try

            
            ds = objBudgetTran.GetListForPlannedJobMapping("List", CInt(cboBudget.SelectedValue), 0)

            Dim dt As DataTable = ds.Tables(0).DefaultView.ToTable(True, "jobunkid", "job_name")

            Dim dr As DataRow = dt.NewRow
            dr.Item("jobunkid") = 0
            dr.Item("job_name") = "Select"
            dt.Rows.InsertAt(dr, 0)

            With cboJob
                .ValueMember = "jobunkid"
                .DisplayMember = "job_name"
                .DataSource = dt
                .SelectedValue = 0
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetupGrid", mstrModuleName)
        Finally
            objBudgetTran = Nothing
        End Try
    End Sub

    Private Sub SetupGrid()
        Dim objBudgetTran As New clsBudget_TranNew
        Dim objEmployee As New clsEmployee_Master
        Dim ds As DataSet
        Try

            dgvMapping.AutoGenerateColumns = False
            dgvMapping.DataSource = Nothing

            'If CInt(cboBudget.SelectedValue) <= 0 OrElse CInt(cboPeriod.SelectedValue) <= 0 OrElse CInt(cboJob.SelectedValue) <= 0 Then Exit Try
            If CInt(cboBudget.SelectedValue) <= 0 OrElse CInt(cboPeriod.SelectedValue) <= 0 Then Exit Try

            objcolhBudgetUnkid.DataPropertyName = "budgetunkid"
            objcolhAllocationtranunkid.DataPropertyName = "allocationtranunkid"
            objcolhJobunkid.DataPropertyName = "jobunkid"
            colhJobName.DataPropertyName = "GName"
            objcolhJobName.DataPropertyName = "job_name"

            ds = objBudgetTran.GetListForPlannedJobMapping("List", CInt(cboBudget.SelectedValue), CInt(cboJob.SelectedValue))

            If marrEmp.Length = 0 Then
                ReDim marrEmp(1)
                marrEmp(0) = "-1"
            End If
            'Dim dsEmp As DataSet = objEmployee.GetEmployeeList(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, mdtPeriodStartDate, mdtPeriodEndDate, ConfigParameter._Object._UserAccessModeSetting, True, False, "List", True, , , , , , , , , , CInt(cboJob.SelectedValue))
            Dim dsEmp As DataSet = objEmployee.GetListForDynamicField(" " & clsEmployee_Master.EmpColEnum.Col_Code & ", " & clsEmployee_Master.EmpColEnum.Col_Employee_Name & ", " & clsEmployee_Master.EmpColEnum.Col_Job & " ", FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, mdtPeriodStartDate, mdtPeriodEndDate, ConfigParameter._Object._UserAccessModeSetting, True, False, "List", , True, "hremployee_master.employeeunkid NOT IN (" & String.Join(",", marrEmp) & ")")
            'Dim dsEmp As DataSet = objEmployee.GetList(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, mdtPeriodStartDate, mdtPeriodEndDate, ConfigParameter._Object._UserAccessModeSetting, True, False, "List", )
            dsEmp.Tables(0).Columns(2).ColumnName = "employeename"
            dsEmp.Tables(0).Columns(2).Caption = "employeename"
            dsEmp.Tables(0).Columns(3).ColumnName = "JobName"
            dsEmp.Tables(0).Columns(3).Caption = "JobName"
            Dim dr As DataRow = dsEmp.Tables(0).NewRow
            dr.Item("employeeunkid") = 0
            dr.Item("employeename") = "Select"
            dr.Item("JobName") = ""
            dsEmp.Tables(0).Rows.InsertAt(dr, 0)

            mdvTable = New DataView(dsEmp.Tables(0))
            With colhEmployee
                .DisplayStyle = DataGridViewComboBoxDisplayStyle.Nothing
                .DisplayMember = "employeename"
                .ValueMember = "employeeunkid"
                .DataSource = mdvTable

                .DataPropertyName = "employeeunkid"
            End With
            objcolhEmployeeunkid.DataPropertyName = "employeeunkid"

            Dim dtTable As DataTable = ds.Tables(0).Copy
            Dim dtCol As DataColumn

            dtCol = New DataColumn("employeeunkid", System.Type.GetType("System.Int32"))
            dtCol.DefaultValue = 0
            dtCol.AllowDBNull = False
            dtTable.Columns.Add(dtCol)


            dgvMapping.DataSource = dtTable

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetupGrid", mstrModuleName)
        Finally
            objBudgetTran = Nothing
            objEmployee = Nothing
        End Try
    End Sub

#End Region

#Region " Form's Events "

    Private Sub frmPlannedEmployeeMapping_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        Try

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmPlannedEmployeeMapping_FormClosed", mstrModuleName)
        End Try
    End Sub

    Private Sub frmPlannedEmployeeMapping_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Call Set_Logo(Me, gApplicationType)

            Language.setLanguage(Me.Name)
            Call OtherSettings()

            Call SetColor()
            Call FilCombo()

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmPlannedEmployeeMapping_Load", mstrModuleName)
        End Try
    End Sub

    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsBudget_MasterNew.SetMessages()
            clsBudget_TranNew.SetMessages()
            objfrm._Other_ModuleNames = "clsBudget_MasterNew, clsBudget_TranNew"

            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub

#End Region

#Region " ComboBox's Events "

    Private Sub cboPeriod_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboPeriod.SelectedIndexChanged
        Dim objPeriod As New clscommom_period_Tran
        Dim dsList As DataSet = Nothing
        Try
            If CInt(cboPeriod.SelectedValue) > 0 Then
                objPeriod._Periodunkid(FinancialYear._Object._DatabaseName) = CInt(cboPeriod.SelectedValue)
                mdtPeriodStartDate = objPeriod._Start_Date
                mdtPeriodEndDate = objPeriod._End_Date
            End If
            Call FillJob()
            Call SetupGrid()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboPeriod_SelectedIndexChanged", mstrModuleName)
        Finally
            objPeriod = Nothing
            dsList = Nothing
        End Try
    End Sub

    Private Sub cboBudget_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboBudget.SelectedIndexChanged
        Try
            If CInt(cboBudget.SelectedValue) > 0 Then
                Dim objBudgetTran As New clsBudget_TranNew
                Dim dsList As DataSet
                dsList = objBudgetTran.GetList("List", CInt(cboBudget.SelectedValue))
                marrEmp = (From p In dsList.Tables(0) Where (CInt(p.Item("allocationtranunkid")) > 0) Select (p.Item("allocationtranunkid").ToString)).Distinct.ToArray
            End If
            Call FillJob()
            Call SetupGrid()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboBudget_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub cboJob_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboJob.SelectedIndexChanged
        Try
            Call SetupGrid()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboJob_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Buttons Events "

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            mblnCancel = True
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnClose_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Dim objBudgetTran As New clsBudget_TranNew
        Try
            If dgvMapping.Rows.Count <= 0 Then Exit Try

            If CType(dgvMapping.DataSource, DataTable).Select("employeeunkid > 0 ").Length <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please map atleast one employee."), enMsgBoxStyle.Information)
                Exit Try
            End If

            Dim dtFilter As DataTable = CType(dgvMapping.DataSource, DataTable).Select("employeeunkid > 0 ").CopyToDataTable

            With objBudgetTran
                ._FormName = mstrModuleName
                ._LoginEmployeeunkid = 0
                ._ClientIP = getIP()
                ._HostName = getHostName()
                ._FromWeb = False
                ._AuditUserId = User._Object._Userunkid
._CompanyUnkid = Company._Object._Companyunkid
                ._AuditDate = ConfigParameter._Object._CurrentDateAndTime
            End With

            If objBudgetTran.UpdatePlannedEmployees(CInt(cboBudget.SelectedValue), CInt(cboPeriod.SelectedValue), dtFilter, ConfigParameter._Object._CurrentDateAndTime, Nothing) = False Then
                eZeeMsgBox.Show(objBudgetTran._Message, enMsgBoxStyle.Information)
            Else
                mblnCancel = False
                Me.Close()
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnSave_Click", mstrModuleName)
        Finally
            objBudgetTran = Nothing
        End Try
    End Sub

#End Region

#Region " Datagridview's Events "

    Private Sub dgvMapping_CellEnter(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvMapping.CellEnter
        Try
            If e.RowIndex <= -1 Then Exit Sub

            If e.ColumnIndex = colhEmployee.Index Then
                mdvTable.RowFilter = "employeeunkid = 0 OR JobName = '" & dgvMapping.CurrentRow.Cells(objcolhJobName.Index).Value.ToString & "' "

                Dim c As DataGridViewComboBoxCell = CType(dgvMapping.CurrentCell, DataGridViewComboBoxCell)
                c.DataSource = mdvTable.ToTable.Copy

                SendKeys.Send("{F2}")
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgvMapping_CellEnter", mstrModuleName)
        End Try
    End Sub

    Private Sub dgvMapping_DataError(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewDataErrorEventArgs) Handles dgvMapping.DataError
        Try
            eZeeMsgBox.Show(e.Exception.Message, enMsgBoxStyle.Critical)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgvMapping_DataError", mstrModuleName)
        End Try
    End Sub

    Private Sub dgvMapping_EditingControlShowing(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewEditingControlShowingEventArgs) Handles dgvMapping.EditingControlShowing
        Try

            If dgvMapping.CurrentCell.ColumnIndex = colhEmployee.Index Then

                Dim cb As ComboBox = TryCast(e.Control, ComboBox)
                If cb IsNot Nothing Then
                    cb.DropDownStyle = ComboBoxStyle.DropDown
                    cb.AutoCompleteMode = AutoCompleteMode.SuggestAppend
                    cb.AutoCompleteSource = AutoCompleteSource.ListItems


                    RemoveHandler cb.Validating, AddressOf TranHead_Validating

                    'Select Case dgvMapping.CurrentCell.ColumnIndex
                    '    Case colhPercentage.Index
                    AddHandler cb.Validating, AddressOf TranHead_Validating
                    'End Select
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgvMapping_EditingControlShowing", mstrModuleName)
        End Try
    End Sub

    Private Sub TranHead_Validating(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs)
        Try
            Dim cb As ComboBox = TryCast(sender, ComboBox)
            mdvTable.RowFilter = ""
            Dim intValue As Integer = 0
            If cb IsNot Nothing Then
                intValue = CInt(cb.SelectedValue)
                If cb.SelectedIndex <> 0 Then

                    For Each dRow As DataGridViewRow In dgvMapping.Rows

                        If (dgvMapping.CurrentCell.ColumnIndex <> colhEmployee.Index OrElse dgvMapping.CurrentRow.Index <> dRow.Index) AndAlso Convert.ToInt32(dRow.Cells(colhEmployee.Index).Value) = Convert.ToInt32(cb.SelectedValue) Then
                            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Sorry! This employee is already Mapped.") & vbCrLf & Language.getMessage(mstrModuleName, 3, "Please select different employee."), enMsgBoxStyle.Information)
                            dgvMapping.CurrentCell.Value = 0
                            cb.SelectedIndex = 0
                            e.Cancel = True
                            SendKeys.Send("{ESC}")
                            SendKeys.Send("{F2}")
                            Exit For
                        End If

                    Next

                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "TranHead_Validating", mstrModuleName)
        End Try
    End Sub

#End Region

    
	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Private Sub OtherSettings()
		Try
			Me.SuspendLayout()
			
			Call SetLanguage()
			
			Me.gbFieldMapping.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbFieldMapping.ForeColor = GUI._eZeeContainerHeaderForeColor 


			Me.btnClose.GradientBackColor = GUI._ButttonBackColor 
			Me.btnClose.GradientForeColor = GUI._ButttonFontColor

			Me.btnSave.GradientBackColor = GUI._ButttonBackColor 
			Me.btnSave.GradientForeColor = GUI._ButttonFontColor


			Me.ResumeLayout()
		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "OtherSettings", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetLanguage()
		Try
			Me.Text = Language._Object.getCaption(Me.Name, Me.Text)
			
			Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
			Me.btnSave.Text = Language._Object.getCaption(Me.btnSave.Name, Me.btnSave.Text)
			Me.gbFieldMapping.Text = Language._Object.getCaption(Me.gbFieldMapping.Name, Me.gbFieldMapping.Text)
			Me.lblBudgetCode.Text = Language._Object.getCaption(Me.lblBudgetCode.Name, Me.lblBudgetCode.Text)
			Me.lblPeriod.Text = Language._Object.getCaption(Me.lblPeriod.Name, Me.lblPeriod.Text)
			Me.lblJob.Text = Language._Object.getCaption(Me.lblJob.Name, Me.lblJob.Text)
			Me.colhJobName.HeaderText = Language._Object.getCaption(Me.colhJobName.Name, Me.colhJobName.HeaderText)
			Me.colhEmployee.HeaderText = Language._Object.getCaption(Me.colhEmployee.Name, Me.colhEmployee.HeaderText)

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetLanguage", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "Please map atleast one employee.")
			Language.setMessage(mstrModuleName, 2, "Sorry! This employee is already Mapped.")
			Language.setMessage(mstrModuleName, 3, "Please select different employee.")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class