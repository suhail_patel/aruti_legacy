﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmImportBudget
    Inherits eZee.Common.eZeeForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmImportBudget))
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Me.DataGridViewTextBoxColumn1 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.btnImport = New eZee.Common.eZeeLightButton(Me.components)
        Me.DataGridViewTextBoxColumn2 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.gbFileInfo = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.objbtnReset = New eZee.Common.eZeeSearchResetButton(Me.components)
        Me.objbtnSearch = New eZee.Common.eZeeSearchResetButton(Me.components)
        Me.objbtnOpenFile = New eZee.Common.eZeeGradientButton
        Me.txtFilePath = New System.Windows.Forms.TextBox
        Me.lblFileName = New System.Windows.Forms.Label
        Me.radApplytoChecked = New System.Windows.Forms.RadioButton
        Me.elMandatoryInfo = New eZee.Common.eZeeLine
        Me.radApplySelected = New System.Windows.Forms.RadioButton
        Me.lblHeadTypeId = New System.Windows.Forms.Label
        Me.radApplytoAll = New System.Windows.Forms.RadioButton
        Me.btnCancel = New eZee.Common.eZeeLightButton(Me.components)
        Me.DataGridViewTextBoxColumn3 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.cboHeadTypeId = New System.Windows.Forms.ComboBox
        Me.lblCalcType = New System.Windows.Forms.Label
        Me.lblTypeOfId = New System.Windows.Forms.Label
        Me.DataGridViewTextBoxColumn5 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn4 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.cboCalcTypeId = New System.Windows.Forms.ComboBox
        Me.cboTypeOfId = New System.Windows.Forms.ComboBox
        Me.objchkSelectAll = New System.Windows.Forms.CheckBox
        Me.mnuShowUnsuccessful = New System.Windows.Forms.ToolStripMenuItem
        Me.dgvImportInfo = New System.Windows.Forms.DataGridView
        Me.objbtnSet = New eZee.Common.eZeeLightButton(Me.components)
        Me.objLine2 = New eZee.Common.eZeeStraightLine
        Me.pnlMainInfo = New System.Windows.Forms.Panel
        Me.objLine1 = New eZee.Common.eZeeStraightLine
        Me.objefFormFooter = New eZee.Common.eZeeFooter
        Me.btnExport = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnHeadOperations = New eZee.Common.eZeeSplitButton
        Me.cmnuOperations = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.mnuShowSuccessful = New System.Windows.Forms.ToolStripMenuItem
        Me.DataGridViewTextBoxColumn6 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.gbFileInfo.SuspendLayout()
        CType(Me.objbtnReset, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.objbtnSearch, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dgvImportInfo, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.pnlMainInfo.SuspendLayout()
        Me.objefFormFooter.SuspendLayout()
        Me.cmnuOperations.SuspendLayout()
        Me.SuspendLayout()
        '
        'DataGridViewTextBoxColumn1
        '
        Me.DataGridViewTextBoxColumn1.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.DataGridViewTextBoxColumn1.HeaderText = "Employee Code"
        Me.DataGridViewTextBoxColumn1.Name = "DataGridViewTextBoxColumn1"
        Me.DataGridViewTextBoxColumn1.ReadOnly = True
        '
        'btnImport
        '
        Me.btnImport.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnImport.BackColor = System.Drawing.Color.White
        Me.btnImport.BackgroundImage = CType(resources.GetObject("btnImport.BackgroundImage"), System.Drawing.Image)
        Me.btnImport.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnImport.BorderColor = System.Drawing.Color.Empty
        Me.btnImport.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnImport.FlatAppearance.BorderSize = 0
        Me.btnImport.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnImport.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnImport.ForeColor = System.Drawing.Color.Black
        Me.btnImport.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnImport.GradientForeColor = System.Drawing.Color.Black
        Me.btnImport.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnImport.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnImport.Location = New System.Drawing.Point(782, 13)
        Me.btnImport.Name = "btnImport"
        Me.btnImport.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnImport.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnImport.Size = New System.Drawing.Size(97, 30)
        Me.btnImport.TabIndex = 0
        Me.btnImport.Text = "&Import"
        Me.btnImport.UseVisualStyleBackColor = False
        '
        'DataGridViewTextBoxColumn2
        '
        Me.DataGridViewTextBoxColumn2.HeaderText = "Trans. Head Code"
        Me.DataGridViewTextBoxColumn2.Name = "DataGridViewTextBoxColumn2"
        Me.DataGridViewTextBoxColumn2.ReadOnly = True
        Me.DataGridViewTextBoxColumn2.Width = 130
        '
        'gbFileInfo
        '
        Me.gbFileInfo.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.gbFileInfo.BorderColor = System.Drawing.Color.Black
        Me.gbFileInfo.Checked = False
        Me.gbFileInfo.CollapseAllExceptThis = False
        Me.gbFileInfo.CollapsedHoverImage = Nothing
        Me.gbFileInfo.CollapsedNormalImage = Nothing
        Me.gbFileInfo.CollapsedPressedImage = Nothing
        Me.gbFileInfo.CollapseOnLoad = False
        Me.gbFileInfo.Controls.Add(Me.objbtnReset)
        Me.gbFileInfo.Controls.Add(Me.objbtnSearch)
        Me.gbFileInfo.Controls.Add(Me.objbtnOpenFile)
        Me.gbFileInfo.Controls.Add(Me.txtFilePath)
        Me.gbFileInfo.Controls.Add(Me.lblFileName)
        Me.gbFileInfo.ExpandedHoverImage = Nothing
        Me.gbFileInfo.ExpandedNormalImage = Nothing
        Me.gbFileInfo.ExpandedPressedImage = Nothing
        Me.gbFileInfo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbFileInfo.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbFileInfo.HeaderHeight = 25
        Me.gbFileInfo.HeaderMessage = ""
        Me.gbFileInfo.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbFileInfo.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbFileInfo.HeightOnCollapse = 0
        Me.gbFileInfo.LeftTextSpace = 0
        Me.gbFileInfo.Location = New System.Drawing.Point(12, 12)
        Me.gbFileInfo.Name = "gbFileInfo"
        Me.gbFileInfo.OpenHeight = 300
        Me.gbFileInfo.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbFileInfo.ShowBorder = True
        Me.gbFileInfo.ShowCheckBox = False
        Me.gbFileInfo.ShowCollapseButton = False
        Me.gbFileInfo.ShowDefaultBorderColor = True
        Me.gbFileInfo.ShowDownButton = False
        Me.gbFileInfo.ShowHeader = True
        Me.gbFileInfo.Size = New System.Drawing.Size(970, 67)
        Me.gbFileInfo.TabIndex = 2
        Me.gbFileInfo.Temp = 0
        Me.gbFileInfo.Text = "File Name"
        Me.gbFileInfo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objbtnReset
        '
        Me.objbtnReset.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.objbtnReset.BackColor = System.Drawing.Color.Transparent
        Me.objbtnReset.ButtonType = eZee.Common.eZeeSearchResetButton.EnumButtonType.Reset
        Me.objbtnReset.Image = CType(resources.GetObject("objbtnReset.Image"), System.Drawing.Image)
        Me.objbtnReset.Location = New System.Drawing.Point(942, 0)
        Me.objbtnReset.Name = "objbtnReset"
        Me.objbtnReset.ResultMessage = ""
        Me.objbtnReset.SearchMessage = ""
        Me.objbtnReset.Size = New System.Drawing.Size(25, 24)
        Me.objbtnReset.TabIndex = 204
        Me.objbtnReset.TabStop = False
        Me.objbtnReset.Visible = False
        '
        'objbtnSearch
        '
        Me.objbtnSearch.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.objbtnSearch.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearch.ButtonType = eZee.Common.eZeeSearchResetButton.EnumButtonType.Search
        Me.objbtnSearch.Image = CType(resources.GetObject("objbtnSearch.Image"), System.Drawing.Image)
        Me.objbtnSearch.Location = New System.Drawing.Point(917, 0)
        Me.objbtnSearch.Name = "objbtnSearch"
        Me.objbtnSearch.ResultMessage = ""
        Me.objbtnSearch.SearchMessage = ""
        Me.objbtnSearch.Size = New System.Drawing.Size(23, 24)
        Me.objbtnSearch.TabIndex = 203
        Me.objbtnSearch.TabStop = False
        Me.objbtnSearch.Visible = False
        '
        'objbtnOpenFile
        '
        Me.objbtnOpenFile.BackColor = System.Drawing.Color.Transparent
        Me.objbtnOpenFile.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnOpenFile.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnOpenFile.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnOpenFile.BorderSelected = False
        Me.objbtnOpenFile.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnOpenFile.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(177, Byte))
        Me.objbtnOpenFile.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objbtnOpenFile.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnOpenFile.Location = New System.Drawing.Point(918, 34)
        Me.objbtnOpenFile.Name = "objbtnOpenFile"
        Me.objbtnOpenFile.Size = New System.Drawing.Size(21, 21)
        Me.objbtnOpenFile.TabIndex = 5
        '
        'txtFilePath
        '
        Me.txtFilePath.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtFilePath.Location = New System.Drawing.Point(133, 34)
        Me.txtFilePath.Name = "txtFilePath"
        Me.txtFilePath.Size = New System.Drawing.Size(779, 21)
        Me.txtFilePath.TabIndex = 3
        '
        'lblFileName
        '
        Me.lblFileName.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblFileName.Location = New System.Drawing.Point(33, 35)
        Me.lblFileName.Name = "lblFileName"
        Me.lblFileName.Size = New System.Drawing.Size(94, 17)
        Me.lblFileName.TabIndex = 3
        Me.lblFileName.Text = "File Name"
        Me.lblFileName.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'radApplytoChecked
        '
        Me.radApplytoChecked.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.radApplytoChecked.Location = New System.Drawing.Point(389, 295)
        Me.radApplytoChecked.Name = "radApplytoChecked"
        Me.radApplytoChecked.Size = New System.Drawing.Size(133, 17)
        Me.radApplytoChecked.TabIndex = 15
        Me.radApplytoChecked.TabStop = True
        Me.radApplytoChecked.Text = "Apply To Checked"
        Me.radApplytoChecked.UseVisualStyleBackColor = True
        Me.radApplytoChecked.Visible = False
        '
        'elMandatoryInfo
        '
        Me.elMandatoryInfo.LineOrientation = eZee.Common.eZeeLine.Orientation.Horizontal
        Me.elMandatoryInfo.Location = New System.Drawing.Point(23, 219)
        Me.elMandatoryInfo.Name = "elMandatoryInfo"
        Me.elMandatoryInfo.Size = New System.Drawing.Size(644, 17)
        Me.elMandatoryInfo.TabIndex = 6
        Me.elMandatoryInfo.Text = "Mandatory Information"
        Me.elMandatoryInfo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.elMandatoryInfo.Visible = False
        '
        'radApplySelected
        '
        Me.radApplySelected.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.radApplySelected.Location = New System.Drawing.Point(389, 268)
        Me.radApplySelected.Name = "radApplySelected"
        Me.radApplySelected.Size = New System.Drawing.Size(133, 17)
        Me.radApplySelected.TabIndex = 13
        Me.radApplySelected.TabStop = True
        Me.radApplySelected.Text = "Apply to Selected"
        Me.radApplySelected.UseVisualStyleBackColor = True
        Me.radApplySelected.Visible = False
        '
        'lblHeadTypeId
        '
        Me.lblHeadTypeId.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblHeadTypeId.Location = New System.Drawing.Point(45, 241)
        Me.lblHeadTypeId.Name = "lblHeadTypeId"
        Me.lblHeadTypeId.Size = New System.Drawing.Size(94, 17)
        Me.lblHeadTypeId.TabIndex = 7
        Me.lblHeadTypeId.Text = "Head Type Id"
        Me.lblHeadTypeId.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.lblHeadTypeId.Visible = False
        '
        'radApplytoAll
        '
        Me.radApplytoAll.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.radApplytoAll.Location = New System.Drawing.Point(389, 241)
        Me.radApplytoAll.Name = "radApplytoAll"
        Me.radApplytoAll.Size = New System.Drawing.Size(133, 17)
        Me.radApplytoAll.TabIndex = 14
        Me.radApplytoAll.TabStop = True
        Me.radApplytoAll.Text = "Apply to All"
        Me.radApplytoAll.UseVisualStyleBackColor = True
        Me.radApplytoAll.Visible = False
        '
        'btnCancel
        '
        Me.btnCancel.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnCancel.BackColor = System.Drawing.Color.White
        Me.btnCancel.BackgroundImage = CType(resources.GetObject("btnCancel.BackgroundImage"), System.Drawing.Image)
        Me.btnCancel.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnCancel.BorderColor = System.Drawing.Color.Empty
        Me.btnCancel.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnCancel.FlatAppearance.BorderSize = 0
        Me.btnCancel.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnCancel.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnCancel.ForeColor = System.Drawing.Color.Black
        Me.btnCancel.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnCancel.GradientForeColor = System.Drawing.Color.Black
        Me.btnCancel.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnCancel.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnCancel.Location = New System.Drawing.Point(885, 13)
        Me.btnCancel.Name = "btnCancel"
        Me.btnCancel.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnCancel.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnCancel.Size = New System.Drawing.Size(97, 30)
        Me.btnCancel.TabIndex = 1
        Me.btnCancel.Text = "&Close"
        Me.btnCancel.UseVisualStyleBackColor = False
        '
        'DataGridViewTextBoxColumn3
        '
        DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        Me.DataGridViewTextBoxColumn3.DefaultCellStyle = DataGridViewCellStyle2
        Me.DataGridViewTextBoxColumn3.HeaderText = "Amount"
        Me.DataGridViewTextBoxColumn3.Name = "DataGridViewTextBoxColumn3"
        Me.DataGridViewTextBoxColumn3.ReadOnly = True
        Me.DataGridViewTextBoxColumn3.Width = 120
        '
        'cboHeadTypeId
        '
        Me.cboHeadTypeId.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboHeadTypeId.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboHeadTypeId.FormattingEnabled = True
        Me.cboHeadTypeId.Location = New System.Drawing.Point(151, 239)
        Me.cboHeadTypeId.Name = "cboHeadTypeId"
        Me.cboHeadTypeId.Size = New System.Drawing.Size(207, 21)
        Me.cboHeadTypeId.TabIndex = 8
        Me.cboHeadTypeId.Visible = False
        '
        'lblCalcType
        '
        Me.lblCalcType.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCalcType.Location = New System.Drawing.Point(45, 295)
        Me.lblCalcType.Name = "lblCalcType"
        Me.lblCalcType.Size = New System.Drawing.Size(94, 17)
        Me.lblCalcType.TabIndex = 12
        Me.lblCalcType.Text = "Calculation Type"
        Me.lblCalcType.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.lblCalcType.Visible = False
        '
        'lblTypeOfId
        '
        Me.lblTypeOfId.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTypeOfId.Location = New System.Drawing.Point(45, 268)
        Me.lblTypeOfId.Name = "lblTypeOfId"
        Me.lblTypeOfId.Size = New System.Drawing.Size(94, 17)
        Me.lblTypeOfId.TabIndex = 9
        Me.lblTypeOfId.Text = "Type Of Id"
        Me.lblTypeOfId.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.lblTypeOfId.Visible = False
        '
        'DataGridViewTextBoxColumn5
        '
        Me.DataGridViewTextBoxColumn5.HeaderText = "Type Of"
        Me.DataGridViewTextBoxColumn5.Name = "DataGridViewTextBoxColumn5"
        Me.DataGridViewTextBoxColumn5.ReadOnly = True
        '
        'DataGridViewTextBoxColumn4
        '
        Me.DataGridViewTextBoxColumn4.HeaderText = "Head Type"
        Me.DataGridViewTextBoxColumn4.Name = "DataGridViewTextBoxColumn4"
        Me.DataGridViewTextBoxColumn4.ReadOnly = True
        Me.DataGridViewTextBoxColumn4.Width = 120
        '
        'cboCalcTypeId
        '
        Me.cboCalcTypeId.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboCalcTypeId.FormattingEnabled = True
        Me.cboCalcTypeId.Location = New System.Drawing.Point(151, 293)
        Me.cboCalcTypeId.Name = "cboCalcTypeId"
        Me.cboCalcTypeId.Size = New System.Drawing.Size(207, 21)
        Me.cboCalcTypeId.TabIndex = 11
        Me.cboCalcTypeId.Visible = False
        '
        'cboTypeOfId
        '
        Me.cboTypeOfId.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboTypeOfId.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboTypeOfId.FormattingEnabled = True
        Me.cboTypeOfId.Location = New System.Drawing.Point(151, 266)
        Me.cboTypeOfId.Name = "cboTypeOfId"
        Me.cboTypeOfId.Size = New System.Drawing.Size(207, 21)
        Me.cboTypeOfId.TabIndex = 10
        Me.cboTypeOfId.Visible = False
        '
        'objchkSelectAll
        '
        Me.objchkSelectAll.AutoSize = True
        Me.objchkSelectAll.Location = New System.Drawing.Point(19, 91)
        Me.objchkSelectAll.Name = "objchkSelectAll"
        Me.objchkSelectAll.Size = New System.Drawing.Size(15, 14)
        Me.objchkSelectAll.TabIndex = 19
        Me.objchkSelectAll.UseVisualStyleBackColor = True
        '
        'mnuShowUnsuccessful
        '
        Me.mnuShowUnsuccessful.Name = "mnuShowUnsuccessful"
        Me.mnuShowUnsuccessful.Size = New System.Drawing.Size(202, 22)
        Me.mnuShowUnsuccessful.Text = "Show Unsuccessful Data"
        '
        'dgvImportInfo
        '
        Me.dgvImportInfo.AllowUserToAddRows = False
        Me.dgvImportInfo.AllowUserToDeleteRows = False
        Me.dgvImportInfo.AllowUserToResizeRows = False
        Me.dgvImportInfo.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.dgvImportInfo.BackgroundColor = System.Drawing.Color.White
        Me.dgvImportInfo.Location = New System.Drawing.Point(12, 85)
        Me.dgvImportInfo.MultiSelect = False
        Me.dgvImportInfo.Name = "dgvImportInfo"
        Me.dgvImportInfo.RowHeadersVisible = False
        Me.dgvImportInfo.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvImportInfo.Size = New System.Drawing.Size(970, 466)
        Me.dgvImportInfo.TabIndex = 5
        '
        'objbtnSet
        '
        Me.objbtnSet.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.objbtnSet.BackColor = System.Drawing.Color.White
        Me.objbtnSet.BackgroundImage = CType(resources.GetObject("objbtnSet.BackgroundImage"), System.Drawing.Image)
        Me.objbtnSet.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.objbtnSet.BorderColor = System.Drawing.Color.Empty
        Me.objbtnSet.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.objbtnSet.FlatAppearance.BorderSize = 0
        Me.objbtnSet.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.objbtnSet.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objbtnSet.ForeColor = System.Drawing.Color.Black
        Me.objbtnSet.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.objbtnSet.GradientForeColor = System.Drawing.Color.Black
        Me.objbtnSet.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.objbtnSet.HoverGradientForeColor = System.Drawing.Color.Black
        Me.objbtnSet.Location = New System.Drawing.Point(827, 282)
        Me.objbtnSet.Name = "objbtnSet"
        Me.objbtnSet.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.objbtnSet.PressedGradientForeColor = System.Drawing.Color.Black
        Me.objbtnSet.Size = New System.Drawing.Size(114, 30)
        Me.objbtnSet.TabIndex = 18
        Me.objbtnSet.Text = "&Set"
        Me.objbtnSet.UseVisualStyleBackColor = False
        Me.objbtnSet.Visible = False
        '
        'objLine2
        '
        Me.objLine2.BackColor = System.Drawing.Color.Transparent
        Me.objLine2.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objLine2.LineType = eZee.Common.StraightLineTypes.Vertical
        Me.objLine2.Location = New System.Drawing.Point(528, 239)
        Me.objLine2.Name = "objLine2"
        Me.objLine2.Size = New System.Drawing.Size(19, 75)
        Me.objLine2.TabIndex = 17
        Me.objLine2.Visible = False
        '
        'pnlMainInfo
        '
        Me.pnlMainInfo.Controls.Add(Me.objchkSelectAll)
        Me.pnlMainInfo.Controls.Add(Me.dgvImportInfo)
        Me.pnlMainInfo.Controls.Add(Me.objbtnSet)
        Me.pnlMainInfo.Controls.Add(Me.objLine2)
        Me.pnlMainInfo.Controls.Add(Me.gbFileInfo)
        Me.pnlMainInfo.Controls.Add(Me.objLine1)
        Me.pnlMainInfo.Controls.Add(Me.objefFormFooter)
        Me.pnlMainInfo.Controls.Add(Me.radApplytoChecked)
        Me.pnlMainInfo.Controls.Add(Me.elMandatoryInfo)
        Me.pnlMainInfo.Controls.Add(Me.radApplySelected)
        Me.pnlMainInfo.Controls.Add(Me.lblHeadTypeId)
        Me.pnlMainInfo.Controls.Add(Me.radApplytoAll)
        Me.pnlMainInfo.Controls.Add(Me.cboHeadTypeId)
        Me.pnlMainInfo.Controls.Add(Me.lblCalcType)
        Me.pnlMainInfo.Controls.Add(Me.lblTypeOfId)
        Me.pnlMainInfo.Controls.Add(Me.cboCalcTypeId)
        Me.pnlMainInfo.Controls.Add(Me.cboTypeOfId)
        Me.pnlMainInfo.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pnlMainInfo.Location = New System.Drawing.Point(0, 0)
        Me.pnlMainInfo.Name = "pnlMainInfo"
        Me.pnlMainInfo.Size = New System.Drawing.Size(994, 612)
        Me.pnlMainInfo.TabIndex = 3
        '
        'objLine1
        '
        Me.objLine1.BackColor = System.Drawing.Color.Transparent
        Me.objLine1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objLine1.LineType = eZee.Common.StraightLineTypes.Vertical
        Me.objLine1.Location = New System.Drawing.Point(364, 239)
        Me.objLine1.Name = "objLine1"
        Me.objLine1.Size = New System.Drawing.Size(19, 75)
        Me.objLine1.TabIndex = 16
        Me.objLine1.Visible = False
        '
        'objefFormFooter
        '
        Me.objefFormFooter.BorderColor = System.Drawing.Color.Silver
        Me.objefFormFooter.Controls.Add(Me.btnExport)
        Me.objefFormFooter.Controls.Add(Me.btnHeadOperations)
        Me.objefFormFooter.Controls.Add(Me.btnCancel)
        Me.objefFormFooter.Controls.Add(Me.btnImport)
        Me.objefFormFooter.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.objefFormFooter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objefFormFooter.GradiantStyle = eZee.Common.eZeeFooter.GradientStyle.Up
        Me.objefFormFooter.GradientColor1 = System.Drawing.Color.Transparent
        Me.objefFormFooter.GradientColor2 = System.Drawing.Color.Transparent
        Me.objefFormFooter.Location = New System.Drawing.Point(0, 557)
        Me.objefFormFooter.Name = "objefFormFooter"
        Me.objefFormFooter.Size = New System.Drawing.Size(994, 55)
        Me.objefFormFooter.TabIndex = 1
        '
        'btnExport
        '
        Me.btnExport.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnExport.BackColor = System.Drawing.Color.White
        Me.btnExport.BackgroundImage = CType(resources.GetObject("btnExport.BackgroundImage"), System.Drawing.Image)
        Me.btnExport.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnExport.BorderColor = System.Drawing.Color.Empty
        Me.btnExport.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnExport.FlatAppearance.BorderSize = 0
        Me.btnExport.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnExport.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnExport.ForeColor = System.Drawing.Color.Black
        Me.btnExport.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnExport.GradientForeColor = System.Drawing.Color.Black
        Me.btnExport.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnExport.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnExport.Location = New System.Drawing.Point(128, 13)
        Me.btnExport.Name = "btnExport"
        Me.btnExport.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnExport.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnExport.Size = New System.Drawing.Size(97, 30)
        Me.btnExport.TabIndex = 3
        Me.btnExport.Text = "&Export"
        Me.btnExport.UseVisualStyleBackColor = False
        Me.btnExport.Visible = False
        '
        'btnHeadOperations
        '
        Me.btnHeadOperations.BorderColor = System.Drawing.Color.Black
        Me.btnHeadOperations.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnHeadOperations.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnHeadOperations.GradientForeColor = System.Drawing.SystemColors.WindowText
        Me.btnHeadOperations.Location = New System.Drawing.Point(12, 13)
        Me.btnHeadOperations.Name = "btnHeadOperations"
        Me.btnHeadOperations.ShowDefaultBorderColor = True
        Me.btnHeadOperations.Size = New System.Drawing.Size(110, 30)
        Me.btnHeadOperations.SplitButtonMenu = Me.cmnuOperations
        Me.btnHeadOperations.TabIndex = 2
        Me.btnHeadOperations.Text = "Operations"
        '
        'cmnuOperations
        '
        Me.cmnuOperations.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mnuShowSuccessful, Me.mnuShowUnsuccessful})
        Me.cmnuOperations.Name = "cmnuOperations"
        Me.cmnuOperations.Size = New System.Drawing.Size(203, 48)
        '
        'mnuShowSuccessful
        '
        Me.mnuShowSuccessful.Name = "mnuShowSuccessful"
        Me.mnuShowSuccessful.Size = New System.Drawing.Size(202, 22)
        Me.mnuShowSuccessful.Text = "Show Successful Data"
        '
        'DataGridViewTextBoxColumn6
        '
        Me.DataGridViewTextBoxColumn6.HeaderText = "Calc Type"
        Me.DataGridViewTextBoxColumn6.Name = "DataGridViewTextBoxColumn6"
        Me.DataGridViewTextBoxColumn6.ReadOnly = True
        Me.DataGridViewTextBoxColumn6.Width = 120
        '
        'frmImportBudget
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(994, 612)
        Me.Controls.Add(Me.pnlMainInfo)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmImportBudget"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Import Payroll Budget"
        Me.gbFileInfo.ResumeLayout(False)
        Me.gbFileInfo.PerformLayout()
        CType(Me.objbtnReset, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.objbtnSearch, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dgvImportInfo, System.ComponentModel.ISupportInitialize).EndInit()
        Me.pnlMainInfo.ResumeLayout(False)
        Me.pnlMainInfo.PerformLayout()
        Me.objefFormFooter.ResumeLayout(False)
        Me.cmnuOperations.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents DataGridViewTextBoxColumn1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents btnImport As eZee.Common.eZeeLightButton
    Friend WithEvents DataGridViewTextBoxColumn2 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents gbFileInfo As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents objbtnReset As eZee.Common.eZeeSearchResetButton
    Friend WithEvents objbtnSearch As eZee.Common.eZeeSearchResetButton
    Friend WithEvents objbtnOpenFile As eZee.Common.eZeeGradientButton
    Friend WithEvents txtFilePath As System.Windows.Forms.TextBox
    Friend WithEvents lblFileName As System.Windows.Forms.Label
    Friend WithEvents radApplytoChecked As System.Windows.Forms.RadioButton
    Friend WithEvents elMandatoryInfo As eZee.Common.eZeeLine
    Friend WithEvents radApplySelected As System.Windows.Forms.RadioButton
    Friend WithEvents lblHeadTypeId As System.Windows.Forms.Label
    Friend WithEvents radApplytoAll As System.Windows.Forms.RadioButton
    Friend WithEvents btnCancel As eZee.Common.eZeeLightButton
    Friend WithEvents DataGridViewTextBoxColumn3 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents cboHeadTypeId As System.Windows.Forms.ComboBox
    Friend WithEvents lblCalcType As System.Windows.Forms.Label
    Friend WithEvents lblTypeOfId As System.Windows.Forms.Label
    Friend WithEvents DataGridViewTextBoxColumn5 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn4 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents cboCalcTypeId As System.Windows.Forms.ComboBox
    Friend WithEvents cboTypeOfId As System.Windows.Forms.ComboBox
    Friend WithEvents objchkSelectAll As System.Windows.Forms.CheckBox
    Friend WithEvents mnuShowUnsuccessful As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents dgvImportInfo As System.Windows.Forms.DataGridView
    Friend WithEvents objbtnSet As eZee.Common.eZeeLightButton
    Friend WithEvents objLine2 As eZee.Common.eZeeStraightLine
    Friend WithEvents pnlMainInfo As System.Windows.Forms.Panel
    Friend WithEvents objLine1 As eZee.Common.eZeeStraightLine
    Friend WithEvents objefFormFooter As eZee.Common.eZeeFooter
    Friend WithEvents btnExport As eZee.Common.eZeeLightButton
    Friend WithEvents btnHeadOperations As eZee.Common.eZeeSplitButton
    Friend WithEvents cmnuOperations As System.Windows.Forms.ContextMenuStrip
    Friend WithEvents mnuShowSuccessful As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents DataGridViewTextBoxColumn6 As System.Windows.Forms.DataGridViewTextBoxColumn
End Class
