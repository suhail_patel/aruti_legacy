﻿Option Strict On

Imports eZeeCommonLib
Imports Aruti.Data

Public Class frmBudgetList

#Region " Private Variables "
    Private ReadOnly mstrModuleName As String = "frmBudgetList"
    Private objBudgetMaster As clsBudget_MasterNew

#End Region

#Region " Private Methods "

    Private Sub FillCombo()
        Dim objMaster As New clsMasterData
        Dim dsList As DataSet
        Try

            dsList = objBudgetMaster.GetComboList("List", True)
            With cboBudget
                .ValueMember = "budgetunkid"
                .DisplayMember = "Name"
                .DataSource = dsList.Tables("List")
                .SelectedValue = 0
            End With

            dsList = objMaster.getApprovalStatus("Status", True, True, True, True, True, True)
            With cboStatus
                .ValueMember = "Id"
                .DisplayMember = "Name"
                .DataSource = dsList.Tables("Status")
                .SelectedValue = 0
            End With

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        End Try
    End Sub

    Private Sub FillList()
        Dim strSearch As String = String.Empty
        Dim dsList As DataSet
        Try


            If User._Object.Privilege._AllowToViewBudget = False Then Exit Sub

            If CInt(cboBudget.SelectedValue) > 0 Then
                strSearch &= " AND bgbudget_master.budgetunkid = " & CInt(cboBudget.SelectedValue) & " "
            End If

            If CInt(cboStatus.SelectedValue) > 0 Then
                strSearch &= " AND bgbudget_master.budget_statusunkid = " & CInt(cboStatus.SelectedValue) & " "
            End If

            dsList = objBudgetMaster.GetList("List", strSearch)

            dgvBudgetList.AutoGenerateColumns = False

            objcolhBudgetUnkid.DataPropertyName = "budgetunkid"
            colhBudgetCode.DataPropertyName = "budget_code"
            colhBudgetName.DataPropertyName = "budget_name"
            objcolhPayyearUnkid.DataPropertyName = "payyearunkid"
            colhPayyear.DataPropertyName = "payyear"
            objcolhViewById.DataPropertyName = "viewbyid"
            colhViewBy.DataPropertyName = "ViewBy"
            objcolhAllocationById.DataPropertyName = "allocationbyid"
            colhAllocationBy.DataPropertyName = "AllocationBy"
            objcolhPresentationmodeId.DataPropertyName = "presentationmodeid"
            colhPresentationmode.DataPropertyName = "PresentationMode"
            objcolhWhotoIncludeId.DataPropertyName = "whotoincludeid"
            colhWhotoInclude.DataPropertyName = "WhoToInclude"
            objcolhSalaryLevelId.DataPropertyName = "salarylevelid"
            colhSalaryLevel.DataPropertyName = "SalaryLevel"
            colhIsDefault.DataPropertyName = "isdefaultYesNo"
            objcolhIsDefault.DataPropertyName = "isdefault"
            colhIsApproved.DataPropertyName = "budget_status"
            objcolhIsApproved.DataPropertyName = "budget_statusunkid"

            dgvBudgetList.DataSource = dsList.Tables("List")

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillList", mstrModuleName)
        Finally
            objbtnSearch.ShowResult(dgvBudgetList.RowCount.ToString)
        End Try
    End Sub

    Private Sub SetVisibility()
        Try
            btnNew.Enabled = User._Object.Privilege._AllowToAddBudget
            btnEdit.Enabled = User._Object.Privilege._AllowToEditBudget
            btnDelete.Enabled = User._Object.Privilege._AllowToDeleteBudget

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetVisibility", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Form's Events "

    Private Sub frmBudgetList_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        Me.Close()
    End Sub

    Private Sub frmBudgetList_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles Me.KeyPress
        Try
            If Asc(e.KeyChar) = 27 Then
                Me.Close()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmBudgetList_KeyPress", mstrModuleName)
        End Try
    End Sub

    Private Sub frmBudgetList_KeyUp(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyUp
        Try
            If e.KeyCode = Keys.Delete And dgvBudgetList.Focused = True Then
                Call btnDelete.PerformClick()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmBudgetList_KeyUp", mstrModuleName)
        End Try
    End Sub

    Private Sub frmBudgetList_LanguageClick(ByVal sender As Object, ByVal eventArgs As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsBudget_MasterNew.SetMessages()
            clsBudget_TranNew.SetMessages()
            objfrm._Other_ModuleNames = "clsBudget_MasterNew, clsBudget_TranNew"
            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "frmBudgetList_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub

    Private Sub frmBudgetList_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        objBudgetMaster = New clsBudget_MasterNew
        Try
            Call Set_Logo(Me, gApplicationType)
            Language.setLanguage(Me.Name)
            Call OtherSettings()
            Call SetVisibility()
            Call FillCombo()
            Call FillList()

        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "frmBudgetList_Load", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Button's Events "

    Private Sub btnNew_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnNew.Click
        Dim frm As New frmBudgetAddEdit
        Try
            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If

            If frm.displayDialog(-1, enAction.ADD_ONE) Then
                Call FillList()
                Call FillCombo()
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnNew_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnEdit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnEdit.Click
        Dim frm As New frmBudgetAddEdit
        Try
            If dgvBudgetList.SelectedRows.Count <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please select atleast one transaction for further operation."), enMsgBoxStyle.Information)
                dgvBudgetList.Focus()
                Exit Sub
                'ElseIf CInt(dgvBudgetList.SelectedRows(0).Cells(objcolhIsApproved.Index).Value) = enApprovalStatus.APPROVED Then
                '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "Sorry, You cannot do further operation on this budget. Reason : This budget is already approved."), enMsgBoxStyle.Information)
                '    dgvBudgetList.Focus()
                '    Exit Sub
                'ElseIf CInt(dgvBudgetList.SelectedRows(0).Cells(objcolhIsApproved.Index).Value) = enApprovalStatus.REJECTED Then
                '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 6, "Sorry, You cannot do further operation on this budget. Reason : This budget is already rejected."), enMsgBoxStyle.Information)
                '    dgvBudgetList.Focus()
                '    Exit Sub
            End If

            If frm.displayDialog(CInt(dgvBudgetList.SelectedRows(0).Cells(objcolhBudgetUnkid.Index).Value), enAction.EDIT_ONE) Then
                Call FillList()
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnEdit_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDelete.Click

        Try
            If dgvBudgetList.SelectedRows.Count <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please select atleast one transaction for further operation."), enMsgBoxStyle.Information)
                dgvBudgetList.Focus()
                Exit Sub
            ElseIf CInt(dgvBudgetList.SelectedRows(0).Cells(objcolhIsApproved.Index).Value) = enApprovalStatus.APPROVED Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "Sorry, You cannot do further operation on this budget. Reason : This budget is already approved."), enMsgBoxStyle.Information)
                dgvBudgetList.Focus()
                Exit Sub
            ElseIf CInt(dgvBudgetList.SelectedRows(0).Cells(objcolhIsApproved.Index).Value) = enApprovalStatus.REJECTED Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 6, "Sorry, You cannot do further operation on this budget. Reason : This budget is already rejected."), enMsgBoxStyle.Information)
                dgvBudgetList.Focus()
                Exit Sub
            End If

            If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Are you sure you want to Delete this Budget transaction?"), CType(enMsgBoxStyle.Question + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.Yes Then

                objBudgetMaster = New clsBudget_MasterNew

                Dim frm As New frmReasonSelection
                Dim mstrVoidReason As String = String.Empty
                frm.displayDialog(enVoidCategoryType.PAYROLL, mstrVoidReason)
                If mstrVoidReason.Length <= 0 Then
                    Exit Sub
                Else
                    objBudgetMaster._Voidreason = mstrVoidReason
                End If
                frm = Nothing

                objBudgetMaster._Isvoid = True
                objBudgetMaster._Voiddatetime = ConfigParameter._Object._CurrentDateAndTime
                objBudgetMaster._Voiduserunkid = User._Object._Userunkid


                'S.SANDEEP [28-May-2018] -- START
                'ISSUE/ENHANCEMENT : {Audit Trails} 
                objBudgetMaster._FormName = mstrModuleName
                objBudgetMaster._LoginEmployeeunkid = 0
                objBudgetMaster._ClientIP = getIP()
                objBudgetMaster._HostName = getHostName()
                objBudgetMaster._FromWeb = False
                objBudgetMaster._AuditUserId = User._Object._Userunkid
objBudgetMaster._CompanyUnkid = Company._Object._Companyunkid
                objBudgetMaster._AuditDate = ConfigParameter._Object._CurrentDateAndTime
                'S.SANDEEP [28-May-2018] -- END

                If objBudgetMaster.Void(CInt(dgvBudgetList.SelectedRows(0).Cells(objcolhBudgetUnkid.Index).Value), ConfigParameter._Object._CurrentDateAndTime) Then
                    Call FillList()
                    Call FillCombo()
                Else
                    If objBudgetMaster._Message <> "" Then
                        eZeeMsgBox.Show(objBudgetMaster._Message, enMsgBoxStyle.Information)
                        Exit Sub
                    End If
                End If
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnDelete_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Me.Close()
    End Sub

    Private Sub objbtnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles objbtnSearch.Click
        Try
            Call FillList()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearch_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnReset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles objbtnReset.Click
        Try
            cboBudget.SelectedValue = 0
            cboStatus.SelectedValue = 0
            Call FillList()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnReset_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnSearchBudget_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles objbtnSearchBudget.Click
        Try
            Dim objfrm As New frmCommonSearch

            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            With objfrm
                .ValueMember = cboBudget.ValueMember
                .DisplayMember = cboBudget.DisplayMember
                .DataSource = CType(cboBudget.DataSource, DataTable)
                .CodeMember = "Code"
            End With

            If objfrm.DisplayDialog Then
                cboBudget.SelectedValue = objfrm.SelectedValue
                cboBudget.Focus()
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchBudget_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub mnuBudgetApproval_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuBudgetApproval.Click
        Try
            Dim objFrm As New frmApproveDisapproveBudgetList
            objFrm.ShowDialog()
            Call FillList()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "mnuBudgetApproval_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub mnuSetAsDefault_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuSetAsDefault.Click
        Try
            If dgvBudgetList.SelectedRows.Count <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please select atleast one transaction for further operation."), enMsgBoxStyle.Information)
                dgvBudgetList.Focus()
                Exit Sub
            ElseIf CBool(dgvBudgetList.SelectedRows(0).Cells(objcolhIsDefault.Index).Value) = True Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Sorry, This budget is already a Default budget."), enMsgBoxStyle.Information)
                dgvBudgetList.Focus()
                Exit Sub
            ElseIf CInt(dgvBudgetList.SelectedRows(0).Cells(objcolhIsApproved.Index).Value) <> enApprovalStatus.APPROVED Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 7, "Sorry, You cannot set this budget as Default budget. Reason : This budget is not approved yet."), enMsgBoxStyle.Information)
                dgvBudgetList.Focus()
                Exit Sub
            End If


            'S.SANDEEP [28-May-2018] -- START
            'ISSUE/ENHANCEMENT : {Audit Trails} 
            objBudgetMaster._FormName = mstrModuleName
            objBudgetMaster._LoginEmployeeunkid = 0
            objBudgetMaster._ClientIP = getIP()
            objBudgetMaster._HostName = getHostName()
            objBudgetMaster._FromWeb = False
            objBudgetMaster._AuditUserId = User._Object._Userunkid
objBudgetMaster._CompanyUnkid = Company._Object._Companyunkid
            objBudgetMaster._AuditDate = ConfigParameter._Object._CurrentDateAndTime
            'S.SANDEEP [28-May-2018] -- END

            If objBudgetMaster.UpdateIsDefault(CInt(dgvBudgetList.SelectedRows(0).Cells(objcolhBudgetUnkid.Index).Value), True, User._Object._Userunkid, ConfigParameter._Object._CurrentDateAndTime) = False Then
                If objBudgetMaster._Message <> "" Then
                    eZeeMsgBox.Show(objBudgetMaster._Message, enMsgBoxStyle.Information)
                End If
            Else
                dgvBudgetList.Tag = dgvBudgetList.SelectedRows(0).Index
                Call FillList()
                dgvBudgetList.ClearSelection()
                dgvBudgetList.Rows(CInt(dgvBudgetList.Tag)).Selected = True
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "mnuSetAsDefault_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub mnuClearDefault_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuClearDefault.Click
        Try
            If dgvBudgetList.SelectedRows.Count <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please select atleast one transaction for further operation."), enMsgBoxStyle.Information)
                dgvBudgetList.Focus()
                Exit Sub
            ElseIf CBool(dgvBudgetList.SelectedRows(0).Cells(objcolhIsDefault.Index).Value) = False Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Sorry, This budget is not a Default budget."), enMsgBoxStyle.Information)
                dgvBudgetList.Focus()
                Exit Sub
            End If


            'S.SANDEEP [28-May-2018] -- START
            'ISSUE/ENHANCEMENT : {Audit Trails} 
            objBudgetMaster._FormName = mstrModuleName
            objBudgetMaster._LoginEmployeeunkid = 0
            objBudgetMaster._ClientIP = getIP()
            objBudgetMaster._HostName = getHostName()
            objBudgetMaster._FromWeb = False
            objBudgetMaster._AuditUserId = User._Object._Userunkid
objBudgetMaster._CompanyUnkid = Company._Object._Companyunkid
            objBudgetMaster._AuditDate = ConfigParameter._Object._CurrentDateAndTime
            'S.SANDEEP [28-May-2018] -- END

            If objBudgetMaster.UpdateIsDefault(CInt(dgvBudgetList.SelectedRows(0).Cells(objcolhBudgetUnkid.Index).Value), False, User._Object._Userunkid, ConfigParameter._Object._CurrentDateAndTime) = False Then
                If objBudgetMaster._Message <> "" Then
                    eZeeMsgBox.Show(objBudgetMaster._Message, enMsgBoxStyle.Information)
                End If
            Else
                dgvBudgetList.Tag = dgvBudgetList.SelectedRows(0).Index
                Call FillList()
                dgvBudgetList.ClearSelection()
                dgvBudgetList.Rows(CInt(dgvBudgetList.Tag)).Selected = True
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "mnuClearDefault_Click", mstrModuleName)
        End Try
    End Sub

    'Sohail (06 May 2017) -- Start
    'MST Enhancement - 66.1 - Allow to map employees with planned employees in payroll budget.
    Private Sub mnuPlannedEmployeeMapping_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles mnuPlannedEmployeeMapping.Click
        Try
            Dim objFrm As New frmPlannedEmployeeMapping
            objFrm.ShowDialog()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "mnuBudgetApproval_Click", mstrModuleName)
        End Try
    End Sub
    'Sohail (06 May 2017) -- End

#End Region




	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Private Sub OtherSettings()
		Try
			Me.SuspendLayout()
			
			Call SetLanguage()
			
			Me.gbEmployeeExempetion.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbEmployeeExempetion.ForeColor = GUI._eZeeContainerHeaderForeColor 


			
			Me.eZeeHeader.GradientColor1 = GUI._HeaderBackColor1 
			Me.eZeeHeader.GradientColor2 = GUI._HeaderBackColor2 
			Me.eZeeHeader.BorderColor = GUI._HeaderBorderColor 
			Me.eZeeHeader.HeaderTextForeColor = GUI._HeaderTitleFontColor 
			Me.eZeeHeader.DescriptionForeColor = GUI._HeaderMessageFontColor 


			Me.btnDelete.GradientBackColor = GUI._ButttonBackColor 
			Me.btnDelete.GradientForeColor = GUI._ButttonFontColor

			Me.btnEdit.GradientBackColor = GUI._ButttonBackColor 
			Me.btnEdit.GradientForeColor = GUI._ButttonFontColor

			Me.btnNew.GradientBackColor = GUI._ButttonBackColor 
			Me.btnNew.GradientForeColor = GUI._ButttonFontColor

			Me.btnClose.GradientBackColor = GUI._ButttonBackColor 
			Me.btnClose.GradientForeColor = GUI._ButttonFontColor

			Me.btnOperations.GradientBackColor = GUI._ButttonBackColor 
			Me.btnOperations.GradientForeColor = GUI._ButttonFontColor


			Me.ResumeLayout()
		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "OtherSettings", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetLanguage()
		Try
			Me.Text = Language._Object.getCaption(Me.Name, Me.Text)
			
			Me.btnDelete.Text = Language._Object.getCaption(Me.btnDelete.Name, Me.btnDelete.Text)
			Me.btnEdit.Text = Language._Object.getCaption(Me.btnEdit.Name, Me.btnEdit.Text)
			Me.btnNew.Text = Language._Object.getCaption(Me.btnNew.Name, Me.btnNew.Text)
			Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
			Me.eZeeHeader.Title = Language._Object.getCaption(Me.eZeeHeader.Name & "_Title" , Me.eZeeHeader.Title)
			Me.eZeeHeader.Message = Language._Object.getCaption(Me.eZeeHeader.Name & "_Message" , Me.eZeeHeader.Message)
			Me.gbEmployeeExempetion.Text = Language._Object.getCaption(Me.gbEmployeeExempetion.Name, Me.gbEmployeeExempetion.Text)
			Me.lblBudget.Text = Language._Object.getCaption(Me.lblBudget.Name, Me.lblBudget.Text)
			Me.btnOperations.Text = Language._Object.getCaption(Me.btnOperations.Name, Me.btnOperations.Text)
			Me.mnuSetAsDefault.Text = Language._Object.getCaption(Me.mnuSetAsDefault.Name, Me.mnuSetAsDefault.Text)
			Me.mnuClearDefault.Text = Language._Object.getCaption(Me.mnuClearDefault.Name, Me.mnuClearDefault.Text)
			Me.mnuBudgetApproval.Text = Language._Object.getCaption(Me.mnuBudgetApproval.Name, Me.mnuBudgetApproval.Text)
			Me.ToolStripSeparator1.Text = Language._Object.getCaption(Me.ToolStripSeparator1.Name, Me.ToolStripSeparator1.Text)
			Me.colhBudgetCode.HeaderText = Language._Object.getCaption(Me.colhBudgetCode.Name, Me.colhBudgetCode.HeaderText)
			Me.colhBudgetName.HeaderText = Language._Object.getCaption(Me.colhBudgetName.Name, Me.colhBudgetName.HeaderText)
			Me.colhPayyear.HeaderText = Language._Object.getCaption(Me.colhPayyear.Name, Me.colhPayyear.HeaderText)
			Me.colhViewBy.HeaderText = Language._Object.getCaption(Me.colhViewBy.Name, Me.colhViewBy.HeaderText)
			Me.colhAllocationBy.HeaderText = Language._Object.getCaption(Me.colhAllocationBy.Name, Me.colhAllocationBy.HeaderText)
			Me.colhPresentationmode.HeaderText = Language._Object.getCaption(Me.colhPresentationmode.Name, Me.colhPresentationmode.HeaderText)
			Me.colhWhotoInclude.HeaderText = Language._Object.getCaption(Me.colhWhotoInclude.Name, Me.colhWhotoInclude.HeaderText)
			Me.colhSalaryLevel.HeaderText = Language._Object.getCaption(Me.colhSalaryLevel.Name, Me.colhSalaryLevel.HeaderText)
			Me.colhIsDefault.HeaderText = Language._Object.getCaption(Me.colhIsDefault.Name, Me.colhIsDefault.HeaderText)
			Me.colhIsApproved.HeaderText = Language._Object.getCaption(Me.colhIsApproved.Name, Me.colhIsApproved.HeaderText)
			Me.lblStatus.Text = Language._Object.getCaption(Me.lblStatus.Name, Me.lblStatus.Text)

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetLanguage", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "Please select atleast one transaction for further operation.")
			Language.setMessage(mstrModuleName, 2, "Are you sure you want to Delete this Budget transaction?")
			Language.setMessage(mstrModuleName, 3, "Sorry, This budget is already a Default budget.")
			Language.setMessage(mstrModuleName, 4, "Sorry, This budget is not a Default budget.")
			Language.setMessage(mstrModuleName, 5, "Sorry, You cannot do further operation on this budget. Reason : This budget is already approved.")
			Language.setMessage(mstrModuleName, 6, "Sorry, You cannot do further operation on this budget. Reason : This budget is already rejected.")
			Language.setMessage(mstrModuleName, 7, "Sorry, You cannot set this budget as Default budget. Reason : This budget is not approved yet.")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class