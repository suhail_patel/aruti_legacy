﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmBudgetPreparation_Wizard
    Inherits eZee.Common.eZeeForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmBudgetPreparation_Wizard))
        Me.wizBudget = New eZee.Common.eZeeWizard
        Me.wizpFinish = New eZee.Common.eZeeWizardPage(Me.components)
        Me.Panel4 = New System.Windows.Forms.Panel
        Me.gbStep7 = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.lblStep7_Finish = New System.Windows.Forms.Label
        Me.picStep7_Finish = New System.Windows.Forms.PictureBox
        Me.wizpPrepareBudget = New eZee.Common.eZeeWizardPage(Me.components)
        Me.gbStep6 = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.Panel3 = New System.Windows.Forms.Panel
        Me.dgvStep6_Budget = New System.Windows.Forms.DataGridView
        Me.lblStep6_Desc = New System.Windows.Forms.Label
        Me.wizpSelectPreviusPeriod = New eZee.Common.eZeeWizardPage(Me.components)
        Me.gbStep5 = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.Panel2 = New System.Windows.Forms.Panel
        Me.objchkSelectAll_Period = New System.Windows.Forms.CheckBox
        Me.lvStep5_PayPeriod = New System.Windows.Forms.ListView
        Me.objcolh_Period_Check = New System.Windows.Forms.ColumnHeader
        Me.colh_Period_PayYear = New System.Windows.Forms.ColumnHeader
        Me.colh_Period_PayPeriod = New System.Windows.Forms.ColumnHeader
        Me.colh_Period_StartDate = New System.Windows.Forms.ColumnHeader
        Me.colh_Period_EndDate = New System.Windows.Forms.ColumnHeader
        Me.lblStep5_Desc = New System.Windows.Forms.Label
        Me.wizpSelectTrnHead = New eZee.Common.eZeeWizardPage(Me.components)
        Me.gbStep4 = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.Panel1 = New System.Windows.Forms.Panel
        Me.cboTrnHeadType = New System.Windows.Forms.ComboBox
        Me.objchkSelectAll = New System.Windows.Forms.CheckBox
        Me.lvStep4_TrnHead = New System.Windows.Forms.ListView
        Me.objcolhCheck = New System.Windows.Forms.ColumnHeader
        Me.colhTrnHeadName = New System.Windows.Forms.ColumnHeader
        Me.colhTrnHeadType = New System.Windows.Forms.ColumnHeader
        Me.lblStep4_Desc = New System.Windows.Forms.Label
        Me.wizpAllocationBy = New eZee.Common.eZeeWizardPage(Me.components)
        Me.gbStep3 = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.gbStep3_AllocationBy = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.pnlAllocationBy = New System.Windows.Forms.Panel
        Me.radStation = New System.Windows.Forms.RadioButton
        Me.radPayPoint = New System.Windows.Forms.RadioButton
        Me.radCostCenter = New System.Windows.Forms.RadioButton
        Me.radSection = New System.Windows.Forms.RadioButton
        Me.radClassnClassGrp = New System.Windows.Forms.RadioButton
        Me.radAccess = New System.Windows.Forms.RadioButton
        Me.radGrade = New System.Windows.Forms.RadioButton
        Me.radJobnJobGrp = New System.Windows.Forms.RadioButton
        Me.radDepInDepGroup = New System.Windows.Forms.RadioButton
        Me.radSectionDept = New System.Windows.Forms.RadioButton
        Me.lblStep3_Desc = New System.Windows.Forms.Label
        Me.picStep3_GroupBy = New System.Windows.Forms.PictureBox
        Me.wizpSelectPeriod = New eZee.Common.eZeeWizardPage(Me.components)
        Me.gbStep2 = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.cboStep2_PayPeriod = New System.Windows.Forms.ComboBox
        Me.cboStep2_PayYear = New System.Windows.Forms.ComboBox
        Me.lblStep2_PayPeriod = New System.Windows.Forms.Label
        Me.lblStep2_PayYear = New System.Windows.Forms.Label
        Me.lblStep2_Desc = New System.Windows.Forms.Label
        Me.pic_Step2_SelectPeriod = New System.Windows.Forms.PictureBox
        Me.wizpWelcome = New eZee.Common.eZeeWizardPage(Me.components)
        Me.gbStep1 = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.lbl_Step1_Welcome = New System.Windows.Forms.Label
        Me.pic_Step1_Welcome = New System.Windows.Forms.PictureBox
        Me.EZeeHeader = New eZee.Common.eZeeHeader
        Me.gbBudgetSteps = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.pnlStep6 = New System.Windows.Forms.Panel
        Me.picSide_Step6_Check = New System.Windows.Forms.PictureBox
        Me.lblStep6 = New System.Windows.Forms.Label
        Me.pnlStep7 = New System.Windows.Forms.Panel
        Me.lblStep7 = New System.Windows.Forms.Label
        Me.pnlStep3 = New System.Windows.Forms.Panel
        Me.picSide_Step3_Check = New System.Windows.Forms.PictureBox
        Me.lblStep3 = New System.Windows.Forms.Label
        Me.pnlStep5 = New System.Windows.Forms.Panel
        Me.picSide_Step5_Check = New System.Windows.Forms.PictureBox
        Me.lblStep5 = New System.Windows.Forms.Label
        Me.pnlStep4 = New System.Windows.Forms.Panel
        Me.picSide_Step4_Check = New System.Windows.Forms.PictureBox
        Me.lblStep4 = New System.Windows.Forms.Label
        Me.pnlStep2 = New System.Windows.Forms.Panel
        Me.picSide_Step2_Check = New System.Windows.Forms.PictureBox
        Me.lblStep2 = New System.Windows.Forms.Label
        Me.pnlStep1 = New System.Windows.Forms.Panel
        Me.picSide_Step1_Check = New System.Windows.Forms.PictureBox
        Me.lblStep1 = New System.Windows.Forms.Label
        Me.objFooterSteps = New eZee.Common.eZeeFooter
        Me.DataGridViewTextBoxColumn1 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn2 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.wizBudget.SuspendLayout()
        Me.wizpFinish.SuspendLayout()
        Me.Panel4.SuspendLayout()
        Me.gbStep7.SuspendLayout()
        CType(Me.picStep7_Finish, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.wizpPrepareBudget.SuspendLayout()
        Me.gbStep6.SuspendLayout()
        Me.Panel3.SuspendLayout()
        CType(Me.dgvStep6_Budget, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.wizpSelectPreviusPeriod.SuspendLayout()
        Me.gbStep5.SuspendLayout()
        Me.Panel2.SuspendLayout()
        Me.wizpSelectTrnHead.SuspendLayout()
        Me.gbStep4.SuspendLayout()
        Me.Panel1.SuspendLayout()
        Me.wizpAllocationBy.SuspendLayout()
        Me.gbStep3.SuspendLayout()
        Me.gbStep3_AllocationBy.SuspendLayout()
        Me.pnlAllocationBy.SuspendLayout()
        CType(Me.picStep3_GroupBy, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.wizpSelectPeriod.SuspendLayout()
        Me.gbStep2.SuspendLayout()
        CType(Me.pic_Step2_SelectPeriod, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.wizpWelcome.SuspendLayout()
        Me.gbStep1.SuspendLayout()
        CType(Me.pic_Step1_Welcome, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.gbBudgetSteps.SuspendLayout()
        Me.pnlStep6.SuspendLayout()
        CType(Me.picSide_Step6_Check, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.pnlStep7.SuspendLayout()
        Me.pnlStep3.SuspendLayout()
        CType(Me.picSide_Step3_Check, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.pnlStep5.SuspendLayout()
        CType(Me.picSide_Step5_Check, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.pnlStep4.SuspendLayout()
        CType(Me.picSide_Step4_Check, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.pnlStep2.SuspendLayout()
        CType(Me.picSide_Step2_Check, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.pnlStep1.SuspendLayout()
        CType(Me.picSide_Step1_Check, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'wizBudget
        '
        Me.wizBudget.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.wizBudget.Controls.Add(Me.wizpAllocationBy)
        Me.wizBudget.Controls.Add(Me.wizpSelectPeriod)
        Me.wizBudget.Controls.Add(Me.wizpWelcome)
        Me.wizBudget.Controls.Add(Me.wizpFinish)
        Me.wizBudget.Controls.Add(Me.wizpPrepareBudget)
        Me.wizBudget.Controls.Add(Me.wizpSelectPreviusPeriod)
        Me.wizBudget.Controls.Add(Me.wizpSelectTrnHead)
        Me.wizBudget.Dock = System.Windows.Forms.DockStyle.None
        Me.wizBudget.HeaderFont = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.wizBudget.HeaderImage = Nothing
        Me.wizBudget.HeaderTitleFont = New System.Drawing.Font("Tahoma", 10.25!, System.Drawing.FontStyle.Bold)
        Me.wizBudget.Location = New System.Drawing.Point(220, 66)
        Me.wizBudget.Name = "wizBudget"
        Me.wizBudget.Pages.AddRange(New eZee.Common.eZeeWizardPage() {Me.wizpWelcome, Me.wizpSelectPeriod, Me.wizpAllocationBy, Me.wizpSelectTrnHead, Me.wizpSelectPreviusPeriod, Me.wizpPrepareBudget, Me.wizpFinish})
        Me.wizBudget.SaveEnabled = True
        Me.wizBudget.SaveText = "Save && Finish"
        Me.wizBudget.SaveVisible = False
        Me.wizBudget.SetSaveIndexBeforeFinishIndex = False
        Me.wizBudget.Size = New System.Drawing.Size(639, 507)
        Me.wizBudget.TabIndex = 2
        Me.wizBudget.WelcomeFont = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.wizBudget.WelcomeImage = Nothing
        Me.wizBudget.WelcomeTitleFont = New System.Drawing.Font("Tahoma", 18.25!, System.Drawing.FontStyle.Bold)
        '
        'wizpFinish
        '
        Me.wizpFinish.BackgroundImage = Global.Aruti.Main.My.Resources.Resources.eZeeLogo
        Me.wizpFinish.Controls.Add(Me.Panel4)
        Me.wizpFinish.Location = New System.Drawing.Point(0, 0)
        Me.wizpFinish.Name = "wizpFinish"
        Me.wizpFinish.Size = New System.Drawing.Size(639, 459)
        Me.wizpFinish.Style = eZee.Common.eZeeWizardPageStyle.Finish
        Me.wizpFinish.TabIndex = 12
        '
        'Panel4
        '
        Me.Panel4.Controls.Add(Me.gbStep7)
        Me.Panel4.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel4.Location = New System.Drawing.Point(0, 0)
        Me.Panel4.Name = "Panel4"
        Me.Panel4.Size = New System.Drawing.Size(639, 459)
        Me.Panel4.TabIndex = 0
        '
        'gbStep7
        '
        Me.gbStep7.BorderColor = System.Drawing.Color.Black
        Me.gbStep7.Checked = False
        Me.gbStep7.CollapseAllExceptThis = False
        Me.gbStep7.CollapsedHoverImage = Nothing
        Me.gbStep7.CollapsedNormalImage = Nothing
        Me.gbStep7.CollapsedPressedImage = Nothing
        Me.gbStep7.CollapseOnLoad = False
        Me.gbStep7.Controls.Add(Me.lblStep7_Finish)
        Me.gbStep7.Controls.Add(Me.picStep7_Finish)
        Me.gbStep7.ExpandedHoverImage = Nothing
        Me.gbStep7.ExpandedNormalImage = Nothing
        Me.gbStep7.ExpandedPressedImage = Nothing
        Me.gbStep7.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbStep7.ForeColor = System.Drawing.SystemColors.ControlText
        Me.gbStep7.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbStep7.HeaderHeight = 25
        Me.gbStep7.HeaderMessage = ""
        Me.gbStep7.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbStep7.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbStep7.HeightOnCollapse = 0
        Me.gbStep7.LeftTextSpace = 0
        Me.gbStep7.Location = New System.Drawing.Point(14, 7)
        Me.gbStep7.Name = "gbStep7"
        Me.gbStep7.OpenHeight = 300
        Me.gbStep7.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbStep7.ShowBorder = True
        Me.gbStep7.ShowCheckBox = False
        Me.gbStep7.ShowCollapseButton = False
        Me.gbStep7.ShowDefaultBorderColor = True
        Me.gbStep7.ShowDownButton = False
        Me.gbStep7.ShowHeader = True
        Me.gbStep7.Size = New System.Drawing.Size(610, 446)
        Me.gbStep7.TabIndex = 2
        Me.gbStep7.Temp = 0
        Me.gbStep7.Text = "Finish - Step 7 of 7"
        Me.gbStep7.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblStep7_Finish
        '
        Me.lblStep7_Finish.Font = New System.Drawing.Font("Tahoma", 24.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblStep7_Finish.ForeColor = System.Drawing.SystemColors.ControlDark
        Me.lblStep7_Finish.Location = New System.Drawing.Point(277, 53)
        Me.lblStep7_Finish.Name = "lblStep7_Finish"
        Me.lblStep7_Finish.Size = New System.Drawing.Size(291, 345)
        Me.lblStep7_Finish.TabIndex = 154
        Me.lblStep7_Finish.Text = "The Budget Preparation Wizard Completed Successfully"
        Me.lblStep7_Finish.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'picStep7_Finish
        '
        Me.picStep7_Finish.BackColor = System.Drawing.Color.WhiteSmoke
        Me.picStep7_Finish.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.picStep7_Finish.Image = Global.Aruti.Main.My.Resources.Resources.budget_preparation_wiz
        Me.picStep7_Finish.Location = New System.Drawing.Point(13, 34)
        Me.picStep7_Finish.Name = "picStep7_Finish"
        Me.picStep7_Finish.Size = New System.Drawing.Size(225, 401)
        Me.picStep7_Finish.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.picStep7_Finish.TabIndex = 1
        Me.picStep7_Finish.TabStop = False
        '
        'wizpPrepareBudget
        '
        Me.wizpPrepareBudget.Controls.Add(Me.gbStep6)
        Me.wizpPrepareBudget.Location = New System.Drawing.Point(0, 0)
        Me.wizpPrepareBudget.Name = "wizpPrepareBudget"
        Me.wizpPrepareBudget.Size = New System.Drawing.Size(428, 208)
        Me.wizpPrepareBudget.Style = eZee.Common.eZeeWizardPageStyle.Custom
        Me.wizpPrepareBudget.TabIndex = 11
        '
        'gbStep6
        '
        Me.gbStep6.BorderColor = System.Drawing.Color.Black
        Me.gbStep6.Checked = False
        Me.gbStep6.CollapseAllExceptThis = False
        Me.gbStep6.CollapsedHoverImage = Nothing
        Me.gbStep6.CollapsedNormalImage = Nothing
        Me.gbStep6.CollapsedPressedImage = Nothing
        Me.gbStep6.CollapseOnLoad = False
        Me.gbStep6.Controls.Add(Me.Panel3)
        Me.gbStep6.Controls.Add(Me.lblStep6_Desc)
        Me.gbStep6.ExpandedHoverImage = Nothing
        Me.gbStep6.ExpandedNormalImage = Nothing
        Me.gbStep6.ExpandedPressedImage = Nothing
        Me.gbStep6.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbStep6.ForeColor = System.Drawing.SystemColors.ControlText
        Me.gbStep6.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbStep6.HeaderHeight = 25
        Me.gbStep6.HeaderMessage = ""
        Me.gbStep6.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbStep6.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbStep6.HeightOnCollapse = 0
        Me.gbStep6.LeftTextSpace = 0
        Me.gbStep6.Location = New System.Drawing.Point(14, 7)
        Me.gbStep6.Name = "gbStep6"
        Me.gbStep6.OpenHeight = 300
        Me.gbStep6.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbStep6.ShowBorder = True
        Me.gbStep6.ShowCheckBox = False
        Me.gbStep6.ShowCollapseButton = False
        Me.gbStep6.ShowDefaultBorderColor = True
        Me.gbStep6.ShowDownButton = False
        Me.gbStep6.ShowHeader = True
        Me.gbStep6.Size = New System.Drawing.Size(610, 446)
        Me.gbStep6.TabIndex = 5
        Me.gbStep6.Temp = 0
        Me.gbStep6.Text = "Budget Allocation - Step 6 of 7"
        Me.gbStep6.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Panel3
        '
        Me.Panel3.Controls.Add(Me.dgvStep6_Budget)
        Me.Panel3.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Panel3.Location = New System.Drawing.Point(17, 72)
        Me.Panel3.Name = "Panel3"
        Me.Panel3.Size = New System.Drawing.Size(578, 354)
        Me.Panel3.TabIndex = 157
        '
        'dgvStep6_Budget
        '
        Me.dgvStep6_Budget.AllowUserToAddRows = False
        Me.dgvStep6_Budget.AllowUserToDeleteRows = False
        Me.dgvStep6_Budget.AllowUserToResizeColumns = False
        Me.dgvStep6_Budget.AllowUserToResizeRows = False
        Me.dgvStep6_Budget.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvStep6_Budget.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dgvStep6_Budget.Location = New System.Drawing.Point(0, 0)
        Me.dgvStep6_Budget.Name = "dgvStep6_Budget"
        Me.dgvStep6_Budget.RowHeadersVisible = False
        Me.dgvStep6_Budget.Size = New System.Drawing.Size(578, 354)
        Me.dgvStep6_Budget.TabIndex = 0
        '
        'lblStep6_Desc
        '
        Me.lblStep6_Desc.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblStep6_Desc.Location = New System.Drawing.Point(14, 32)
        Me.lblStep6_Desc.Name = "lblStep6_Desc"
        Me.lblStep6_Desc.Size = New System.Drawing.Size(581, 45)
        Me.lblStep6_Desc.TabIndex = 156
        Me.lblStep6_Desc.Text = "Select Previous Pay Periods for which you want do display costing for selected Tr" & _
            "ansaction Heads. This will help you in the allocation of the fund for current Bu" & _
            "dget transaction head."
        '
        'wizpSelectPreviusPeriod
        '
        Me.wizpSelectPreviusPeriod.Controls.Add(Me.gbStep5)
        Me.wizpSelectPreviusPeriod.Location = New System.Drawing.Point(0, 0)
        Me.wizpSelectPreviusPeriod.Name = "wizpSelectPreviusPeriod"
        Me.wizpSelectPreviusPeriod.Size = New System.Drawing.Size(428, 208)
        Me.wizpSelectPreviusPeriod.Style = eZee.Common.eZeeWizardPageStyle.Custom
        Me.wizpSelectPreviusPeriod.TabIndex = 10
        '
        'gbStep5
        '
        Me.gbStep5.BorderColor = System.Drawing.Color.Black
        Me.gbStep5.Checked = False
        Me.gbStep5.CollapseAllExceptThis = False
        Me.gbStep5.CollapsedHoverImage = Nothing
        Me.gbStep5.CollapsedNormalImage = Nothing
        Me.gbStep5.CollapsedPressedImage = Nothing
        Me.gbStep5.CollapseOnLoad = False
        Me.gbStep5.Controls.Add(Me.Panel2)
        Me.gbStep5.Controls.Add(Me.lblStep5_Desc)
        Me.gbStep5.ExpandedHoverImage = Nothing
        Me.gbStep5.ExpandedNormalImage = Nothing
        Me.gbStep5.ExpandedPressedImage = Nothing
        Me.gbStep5.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbStep5.ForeColor = System.Drawing.SystemColors.ControlText
        Me.gbStep5.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbStep5.HeaderHeight = 25
        Me.gbStep5.HeaderMessage = ""
        Me.gbStep5.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbStep5.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbStep5.HeightOnCollapse = 0
        Me.gbStep5.LeftTextSpace = 0
        Me.gbStep5.Location = New System.Drawing.Point(14, 7)
        Me.gbStep5.Name = "gbStep5"
        Me.gbStep5.OpenHeight = 300
        Me.gbStep5.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbStep5.ShowBorder = True
        Me.gbStep5.ShowCheckBox = False
        Me.gbStep5.ShowCollapseButton = False
        Me.gbStep5.ShowDefaultBorderColor = True
        Me.gbStep5.ShowDownButton = False
        Me.gbStep5.ShowHeader = True
        Me.gbStep5.Size = New System.Drawing.Size(610, 446)
        Me.gbStep5.TabIndex = 3
        Me.gbStep5.Temp = 0
        Me.gbStep5.Text = "Select Previous Pay Periods - Step 5 of 7"
        Me.gbStep5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Panel2
        '
        Me.Panel2.Controls.Add(Me.objchkSelectAll_Period)
        Me.Panel2.Controls.Add(Me.lvStep5_PayPeriod)
        Me.Panel2.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Panel2.Location = New System.Drawing.Point(17, 72)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(577, 365)
        Me.Panel2.TabIndex = 157
        '
        'objchkSelectAll_Period
        '
        Me.objchkSelectAll_Period.AutoSize = True
        Me.objchkSelectAll_Period.Location = New System.Drawing.Point(8, 4)
        Me.objchkSelectAll_Period.Name = "objchkSelectAll_Period"
        Me.objchkSelectAll_Period.Size = New System.Drawing.Size(15, 14)
        Me.objchkSelectAll_Period.TabIndex = 163
        Me.objchkSelectAll_Period.UseVisualStyleBackColor = True
        '
        'lvStep5_PayPeriod
        '
        Me.lvStep5_PayPeriod.CheckBoxes = True
        Me.lvStep5_PayPeriod.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.objcolh_Period_Check, Me.colh_Period_PayYear, Me.colh_Period_PayPeriod, Me.colh_Period_StartDate, Me.colh_Period_EndDate})
        Me.lvStep5_PayPeriod.Dock = System.Windows.Forms.DockStyle.Fill
        Me.lvStep5_PayPeriod.GridLines = True
        Me.lvStep5_PayPeriod.Location = New System.Drawing.Point(0, 0)
        Me.lvStep5_PayPeriod.Name = "lvStep5_PayPeriod"
        Me.lvStep5_PayPeriod.Size = New System.Drawing.Size(577, 365)
        Me.lvStep5_PayPeriod.TabIndex = 162
        Me.lvStep5_PayPeriod.UseCompatibleStateImageBehavior = False
        Me.lvStep5_PayPeriod.View = System.Windows.Forms.View.Details
        '
        'objcolh_Period_Check
        '
        Me.objcolh_Period_Check.Text = ""
        Me.objcolh_Period_Check.Width = 30
        '
        'colh_Period_PayYear
        '
        Me.colh_Period_PayYear.Tag = "colh_Period_PayYear"
        Me.colh_Period_PayYear.Text = "Pay Year"
        Me.colh_Period_PayYear.Width = 150
        '
        'colh_Period_PayPeriod
        '
        Me.colh_Period_PayPeriod.Tag = "colh_Period_PayPeriod"
        Me.colh_Period_PayPeriod.Text = "Pay Period"
        Me.colh_Period_PayPeriod.Width = 150
        '
        'colh_Period_StartDate
        '
        Me.colh_Period_StartDate.Tag = "colh_Period_StartDate"
        Me.colh_Period_StartDate.Text = "Start Date"
        Me.colh_Period_StartDate.Width = 120
        '
        'colh_Period_EndDate
        '
        Me.colh_Period_EndDate.Tag = "colh_Period_EndDate"
        Me.colh_Period_EndDate.Text = "End Date"
        Me.colh_Period_EndDate.Width = 120
        '
        'lblStep5_Desc
        '
        Me.lblStep5_Desc.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblStep5_Desc.Location = New System.Drawing.Point(14, 32)
        Me.lblStep5_Desc.Name = "lblStep5_Desc"
        Me.lblStep5_Desc.Size = New System.Drawing.Size(580, 33)
        Me.lblStep5_Desc.TabIndex = 156
        Me.lblStep5_Desc.Text = "Select Previous Pay Periods for which you want to see the budget allocation for s" & _
            "elected Transaction Heads. This will help you in the allocation of the fund for " & _
            "current Budget transaction head."
        '
        'wizpSelectTrnHead
        '
        Me.wizpSelectTrnHead.Controls.Add(Me.gbStep4)
        Me.wizpSelectTrnHead.Location = New System.Drawing.Point(0, 0)
        Me.wizpSelectTrnHead.Name = "wizpSelectTrnHead"
        Me.wizpSelectTrnHead.Size = New System.Drawing.Size(428, 208)
        Me.wizpSelectTrnHead.Style = eZee.Common.eZeeWizardPageStyle.Custom
        Me.wizpSelectTrnHead.TabIndex = 9
        '
        'gbStep4
        '
        Me.gbStep4.BorderColor = System.Drawing.Color.Black
        Me.gbStep4.Checked = False
        Me.gbStep4.CollapseAllExceptThis = False
        Me.gbStep4.CollapsedHoverImage = Nothing
        Me.gbStep4.CollapsedNormalImage = Nothing
        Me.gbStep4.CollapsedPressedImage = Nothing
        Me.gbStep4.CollapseOnLoad = False
        Me.gbStep4.Controls.Add(Me.Panel1)
        Me.gbStep4.Controls.Add(Me.lblStep4_Desc)
        Me.gbStep4.ExpandedHoverImage = Nothing
        Me.gbStep4.ExpandedNormalImage = Nothing
        Me.gbStep4.ExpandedPressedImage = Nothing
        Me.gbStep4.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbStep4.ForeColor = System.Drawing.SystemColors.ControlText
        Me.gbStep4.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbStep4.HeaderHeight = 25
        Me.gbStep4.HeaderMessage = ""
        Me.gbStep4.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbStep4.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbStep4.HeightOnCollapse = 0
        Me.gbStep4.LeftTextSpace = 0
        Me.gbStep4.Location = New System.Drawing.Point(14, 7)
        Me.gbStep4.Name = "gbStep4"
        Me.gbStep4.OpenHeight = 300
        Me.gbStep4.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbStep4.ShowBorder = True
        Me.gbStep4.ShowCheckBox = False
        Me.gbStep4.ShowCollapseButton = False
        Me.gbStep4.ShowDefaultBorderColor = True
        Me.gbStep4.ShowDownButton = False
        Me.gbStep4.ShowHeader = True
        Me.gbStep4.Size = New System.Drawing.Size(610, 446)
        Me.gbStep4.TabIndex = 2
        Me.gbStep4.Temp = 0
        Me.gbStep4.Text = "Select Transaction Heads - Step 4 of 7"
        Me.gbStep4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(Me.cboTrnHeadType)
        Me.Panel1.Controls.Add(Me.objchkSelectAll)
        Me.Panel1.Controls.Add(Me.lvStep4_TrnHead)
        Me.Panel1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Panel1.Location = New System.Drawing.Point(17, 72)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(577, 365)
        Me.Panel1.TabIndex = 157
        '
        'cboTrnHeadType
        '
        Me.cboTrnHeadType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboTrnHeadType.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboTrnHeadType.FormattingEnabled = True
        Me.cboTrnHeadType.Location = New System.Drawing.Point(515, 22)
        Me.cboTrnHeadType.Name = "cboTrnHeadType"
        Me.cboTrnHeadType.Size = New System.Drawing.Size(46, 21)
        Me.cboTrnHeadType.TabIndex = 159
        Me.cboTrnHeadType.Visible = False
        '
        'objchkSelectAll
        '
        Me.objchkSelectAll.AutoSize = True
        Me.objchkSelectAll.Location = New System.Drawing.Point(8, 4)
        Me.objchkSelectAll.Name = "objchkSelectAll"
        Me.objchkSelectAll.Size = New System.Drawing.Size(15, 14)
        Me.objchkSelectAll.TabIndex = 161
        Me.objchkSelectAll.UseVisualStyleBackColor = True
        '
        'lvStep4_TrnHead
        '
        Me.lvStep4_TrnHead.CheckBoxes = True
        Me.lvStep4_TrnHead.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.objcolhCheck, Me.colhTrnHeadName, Me.colhTrnHeadType})
        Me.lvStep4_TrnHead.Dock = System.Windows.Forms.DockStyle.Fill
        Me.lvStep4_TrnHead.GridLines = True
        Me.lvStep4_TrnHead.Location = New System.Drawing.Point(0, 0)
        Me.lvStep4_TrnHead.Name = "lvStep4_TrnHead"
        Me.lvStep4_TrnHead.Size = New System.Drawing.Size(577, 365)
        Me.lvStep4_TrnHead.TabIndex = 160
        Me.lvStep4_TrnHead.UseCompatibleStateImageBehavior = False
        Me.lvStep4_TrnHead.View = System.Windows.Forms.View.Details
        '
        'objcolhCheck
        '
        Me.objcolhCheck.Tag = "objcolhCheck"
        Me.objcolhCheck.Text = ""
        Me.objcolhCheck.Width = 30
        '
        'colhTrnHeadName
        '
        Me.colhTrnHeadName.Tag = "colhTrnHeadName"
        Me.colhTrnHeadName.Text = "Transaction Head Name"
        Me.colhTrnHeadName.Width = 290
        '
        'colhTrnHeadType
        '
        Me.colhTrnHeadType.Tag = "colhTrnHeadType"
        Me.colhTrnHeadType.Text = "Transaction Head Type"
        Me.colhTrnHeadType.Width = 250
        '
        'lblStep4_Desc
        '
        Me.lblStep4_Desc.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblStep4_Desc.Location = New System.Drawing.Point(14, 32)
        Me.lblStep4_Desc.Name = "lblStep4_Desc"
        Me.lblStep4_Desc.Size = New System.Drawing.Size(580, 45)
        Me.lblStep4_Desc.TabIndex = 156
        Me.lblStep4_Desc.Text = "Select Transaction Heads which you want to include in Budget Preparation. Transac" & _
            "tion Head can be Expense / Revenue or Liabilities / Assets"
        '
        'wizpAllocationBy
        '
        Me.wizpAllocationBy.Controls.Add(Me.gbStep3)
        Me.wizpAllocationBy.Location = New System.Drawing.Point(0, 0)
        Me.wizpAllocationBy.Name = "wizpAllocationBy"
        Me.wizpAllocationBy.Size = New System.Drawing.Size(639, 459)
        Me.wizpAllocationBy.Style = eZee.Common.eZeeWizardPageStyle.Custom
        Me.wizpAllocationBy.TabIndex = 13
        '
        'gbStep3
        '
        Me.gbStep3.BorderColor = System.Drawing.Color.Black
        Me.gbStep3.Checked = False
        Me.gbStep3.CollapseAllExceptThis = False
        Me.gbStep3.CollapsedHoverImage = Nothing
        Me.gbStep3.CollapsedNormalImage = Nothing
        Me.gbStep3.CollapsedPressedImage = Nothing
        Me.gbStep3.CollapseOnLoad = False
        Me.gbStep3.Controls.Add(Me.gbStep3_AllocationBy)
        Me.gbStep3.Controls.Add(Me.lblStep3_Desc)
        Me.gbStep3.Controls.Add(Me.picStep3_GroupBy)
        Me.gbStep3.ExpandedHoverImage = Nothing
        Me.gbStep3.ExpandedNormalImage = Nothing
        Me.gbStep3.ExpandedPressedImage = Nothing
        Me.gbStep3.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbStep3.ForeColor = System.Drawing.SystemColors.ControlText
        Me.gbStep3.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbStep3.HeaderHeight = 25
        Me.gbStep3.HeaderMessage = ""
        Me.gbStep3.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbStep3.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbStep3.HeightOnCollapse = 0
        Me.gbStep3.LeftTextSpace = 0
        Me.gbStep3.Location = New System.Drawing.Point(14, 7)
        Me.gbStep3.Name = "gbStep3"
        Me.gbStep3.OpenHeight = 300
        Me.gbStep3.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbStep3.ShowBorder = True
        Me.gbStep3.ShowCheckBox = False
        Me.gbStep3.ShowCollapseButton = False
        Me.gbStep3.ShowDefaultBorderColor = True
        Me.gbStep3.ShowDownButton = False
        Me.gbStep3.ShowHeader = True
        Me.gbStep3.Size = New System.Drawing.Size(610, 446)
        Me.gbStep3.TabIndex = 3
        Me.gbStep3.Temp = 0
        Me.gbStep3.Text = "Budget Allocation By - Step 3 of 7"
        Me.gbStep3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'gbStep3_AllocationBy
        '
        Me.gbStep3_AllocationBy.BorderColor = System.Drawing.Color.Black
        Me.gbStep3_AllocationBy.Checked = False
        Me.gbStep3_AllocationBy.CollapseAllExceptThis = False
        Me.gbStep3_AllocationBy.CollapsedHoverImage = Nothing
        Me.gbStep3_AllocationBy.CollapsedNormalImage = Nothing
        Me.gbStep3_AllocationBy.CollapsedPressedImage = Nothing
        Me.gbStep3_AllocationBy.CollapseOnLoad = False
        Me.gbStep3_AllocationBy.Controls.Add(Me.pnlAllocationBy)
        Me.gbStep3_AllocationBy.ExpandedHoverImage = Nothing
        Me.gbStep3_AllocationBy.ExpandedNormalImage = Nothing
        Me.gbStep3_AllocationBy.ExpandedPressedImage = Nothing
        Me.gbStep3_AllocationBy.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbStep3_AllocationBy.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbStep3_AllocationBy.HeaderHeight = 25
        Me.gbStep3_AllocationBy.HeaderMessage = ""
        Me.gbStep3_AllocationBy.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbStep3_AllocationBy.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbStep3_AllocationBy.HeightOnCollapse = 0
        Me.gbStep3_AllocationBy.LeftTextSpace = 0
        Me.gbStep3_AllocationBy.Location = New System.Drawing.Point(285, 74)
        Me.gbStep3_AllocationBy.Name = "gbStep3_AllocationBy"
        Me.gbStep3_AllocationBy.OpenHeight = 182
        Me.gbStep3_AllocationBy.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbStep3_AllocationBy.ShowBorder = True
        Me.gbStep3_AllocationBy.ShowCheckBox = False
        Me.gbStep3_AllocationBy.ShowCollapseButton = False
        Me.gbStep3_AllocationBy.ShowDefaultBorderColor = True
        Me.gbStep3_AllocationBy.ShowDownButton = False
        Me.gbStep3_AllocationBy.ShowHeader = True
        Me.gbStep3_AllocationBy.Size = New System.Drawing.Size(282, 349)
        Me.gbStep3_AllocationBy.TabIndex = 156
        Me.gbStep3_AllocationBy.Temp = 0
        Me.gbStep3_AllocationBy.Text = "Allocation By"
        Me.gbStep3_AllocationBy.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'pnlAllocationBy
        '
        Me.pnlAllocationBy.AutoScroll = True
        Me.pnlAllocationBy.Controls.Add(Me.radStation)
        Me.pnlAllocationBy.Controls.Add(Me.radPayPoint)
        Me.pnlAllocationBy.Controls.Add(Me.radCostCenter)
        Me.pnlAllocationBy.Controls.Add(Me.radSection)
        Me.pnlAllocationBy.Controls.Add(Me.radClassnClassGrp)
        Me.pnlAllocationBy.Controls.Add(Me.radAccess)
        Me.pnlAllocationBy.Controls.Add(Me.radGrade)
        Me.pnlAllocationBy.Controls.Add(Me.radJobnJobGrp)
        Me.pnlAllocationBy.Controls.Add(Me.radDepInDepGroup)
        Me.pnlAllocationBy.Controls.Add(Me.radSectionDept)
        Me.pnlAllocationBy.Location = New System.Drawing.Point(2, 26)
        Me.pnlAllocationBy.Name = "pnlAllocationBy"
        Me.pnlAllocationBy.Size = New System.Drawing.Size(277, 320)
        Me.pnlAllocationBy.TabIndex = 124
        '
        'radStation
        '
        Me.radStation.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.radStation.Location = New System.Drawing.Point(24, 150)
        Me.radStation.Name = "radStation"
        Me.radStation.Size = New System.Drawing.Size(193, 17)
        Me.radStation.TabIndex = 134
        Me.radStation.Text = "Branch"
        Me.radStation.UseVisualStyleBackColor = True
        '
        'radPayPoint
        '
        Me.radPayPoint.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.radPayPoint.Location = New System.Drawing.Point(24, 187)
        Me.radPayPoint.Name = "radPayPoint"
        Me.radPayPoint.Size = New System.Drawing.Size(193, 17)
        Me.radPayPoint.TabIndex = 133
        Me.radPayPoint.Text = "Pay Point"
        Me.radPayPoint.UseVisualStyleBackColor = True
        Me.radPayPoint.Visible = False
        '
        'radCostCenter
        '
        Me.radCostCenter.Checked = True
        Me.radCostCenter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.radCostCenter.Location = New System.Drawing.Point(24, 92)
        Me.radCostCenter.Name = "radCostCenter"
        Me.radCostCenter.Size = New System.Drawing.Size(193, 17)
        Me.radCostCenter.TabIndex = 132
        Me.radCostCenter.TabStop = True
        Me.radCostCenter.Text = "Cost Center"
        Me.radCostCenter.UseVisualStyleBackColor = True
        '
        'radSection
        '
        Me.radSection.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.radSection.Location = New System.Drawing.Point(24, 49)
        Me.radSection.Name = "radSection"
        Me.radSection.Size = New System.Drawing.Size(193, 17)
        Me.radSection.TabIndex = 131
        Me.radSection.Text = "Section"
        Me.radSection.UseVisualStyleBackColor = True
        Me.radSection.Visible = False
        '
        'radClassnClassGrp
        '
        Me.radClassnClassGrp.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.radClassnClassGrp.Location = New System.Drawing.Point(24, 288)
        Me.radClassnClassGrp.Name = "radClassnClassGrp"
        Me.radClassnClassGrp.Size = New System.Drawing.Size(193, 17)
        Me.radClassnClassGrp.TabIndex = 130
        Me.radClassnClassGrp.Text = "Class in Class Group"
        Me.radClassnClassGrp.UseVisualStyleBackColor = True
        Me.radClassnClassGrp.Visible = False
        '
        'radAccess
        '
        Me.radAccess.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.radAccess.Location = New System.Drawing.Point(24, 26)
        Me.radAccess.Name = "radAccess"
        Me.radAccess.Size = New System.Drawing.Size(193, 17)
        Me.radAccess.TabIndex = 129
        Me.radAccess.Text = "Access"
        Me.radAccess.UseVisualStyleBackColor = True
        Me.radAccess.Visible = False
        '
        'radGrade
        '
        Me.radGrade.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.radGrade.Location = New System.Drawing.Point(24, 3)
        Me.radGrade.Name = "radGrade"
        Me.radGrade.Size = New System.Drawing.Size(193, 17)
        Me.radGrade.TabIndex = 128
        Me.radGrade.Text = "Grade"
        Me.radGrade.UseVisualStyleBackColor = True
        Me.radGrade.Visible = False
        '
        'radJobnJobGrp
        '
        Me.radJobnJobGrp.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.radJobnJobGrp.Location = New System.Drawing.Point(24, 265)
        Me.radJobnJobGrp.Name = "radJobnJobGrp"
        Me.radJobnJobGrp.Size = New System.Drawing.Size(193, 17)
        Me.radJobnJobGrp.TabIndex = 127
        Me.radJobnJobGrp.Text = "Job in Job Group"
        Me.radJobnJobGrp.UseVisualStyleBackColor = True
        Me.radJobnJobGrp.Visible = False
        '
        'radDepInDepGroup
        '
        Me.radDepInDepGroup.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.radDepInDepGroup.Location = New System.Drawing.Point(24, 210)
        Me.radDepInDepGroup.Name = "radDepInDepGroup"
        Me.radDepInDepGroup.Size = New System.Drawing.Size(193, 17)
        Me.radDepInDepGroup.TabIndex = 124
        Me.radDepInDepGroup.Text = "Department in Department Group"
        Me.radDepInDepGroup.UseVisualStyleBackColor = True
        Me.radDepInDepGroup.Visible = False
        '
        'radSectionDept
        '
        Me.radSectionDept.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.radSectionDept.Location = New System.Drawing.Point(24, 233)
        Me.radSectionDept.Name = "radSectionDept"
        Me.radSectionDept.Size = New System.Drawing.Size(193, 17)
        Me.radSectionDept.TabIndex = 126
        Me.radSectionDept.Text = "Section in Department"
        Me.radSectionDept.UseVisualStyleBackColor = True
        Me.radSectionDept.Visible = False
        '
        'lblStep3_Desc
        '
        Me.lblStep3_Desc.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblStep3_Desc.Location = New System.Drawing.Point(253, 34)
        Me.lblStep3_Desc.Name = "lblStep3_Desc"
        Me.lblStep3_Desc.Size = New System.Drawing.Size(340, 37)
        Me.lblStep3_Desc.TabIndex = 154
        Me.lblStep3_Desc.Text = "Select Category on which you want to allocate the budget."
        '
        'picStep3_GroupBy
        '
        Me.picStep3_GroupBy.BackColor = System.Drawing.Color.WhiteSmoke
        Me.picStep3_GroupBy.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.picStep3_GroupBy.Image = Global.Aruti.Main.My.Resources.Resources.budget_preparation_wiz
        Me.picStep3_GroupBy.Location = New System.Drawing.Point(13, 34)
        Me.picStep3_GroupBy.Name = "picStep3_GroupBy"
        Me.picStep3_GroupBy.Size = New System.Drawing.Size(225, 401)
        Me.picStep3_GroupBy.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.picStep3_GroupBy.TabIndex = 1
        Me.picStep3_GroupBy.TabStop = False
        '
        'wizpSelectPeriod
        '
        Me.wizpSelectPeriod.Controls.Add(Me.gbStep2)
        Me.wizpSelectPeriod.Location = New System.Drawing.Point(0, 0)
        Me.wizpSelectPeriod.Name = "wizpSelectPeriod"
        Me.wizpSelectPeriod.Size = New System.Drawing.Size(639, 459)
        Me.wizpSelectPeriod.Style = eZee.Common.eZeeWizardPageStyle.Custom
        Me.wizpSelectPeriod.TabIndex = 8
        Me.wizpSelectPeriod.Title = "WelCome"
        '
        'gbStep2
        '
        Me.gbStep2.BorderColor = System.Drawing.Color.Black
        Me.gbStep2.Checked = False
        Me.gbStep2.CollapseAllExceptThis = False
        Me.gbStep2.CollapsedHoverImage = Nothing
        Me.gbStep2.CollapsedNormalImage = Nothing
        Me.gbStep2.CollapsedPressedImage = Nothing
        Me.gbStep2.CollapseOnLoad = False
        Me.gbStep2.Controls.Add(Me.cboStep2_PayPeriod)
        Me.gbStep2.Controls.Add(Me.cboStep2_PayYear)
        Me.gbStep2.Controls.Add(Me.lblStep2_PayPeriod)
        Me.gbStep2.Controls.Add(Me.lblStep2_PayYear)
        Me.gbStep2.Controls.Add(Me.lblStep2_Desc)
        Me.gbStep2.Controls.Add(Me.pic_Step2_SelectPeriod)
        Me.gbStep2.ExpandedHoverImage = Nothing
        Me.gbStep2.ExpandedNormalImage = Nothing
        Me.gbStep2.ExpandedPressedImage = Nothing
        Me.gbStep2.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbStep2.ForeColor = System.Drawing.SystemColors.ControlText
        Me.gbStep2.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbStep2.HeaderHeight = 25
        Me.gbStep2.HeaderMessage = ""
        Me.gbStep2.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbStep2.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbStep2.HeightOnCollapse = 0
        Me.gbStep2.LeftTextSpace = 0
        Me.gbStep2.Location = New System.Drawing.Point(14, 7)
        Me.gbStep2.Name = "gbStep2"
        Me.gbStep2.OpenHeight = 300
        Me.gbStep2.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbStep2.ShowBorder = True
        Me.gbStep2.ShowCheckBox = False
        Me.gbStep2.ShowCollapseButton = False
        Me.gbStep2.ShowDefaultBorderColor = True
        Me.gbStep2.ShowDownButton = False
        Me.gbStep2.ShowHeader = True
        Me.gbStep2.Size = New System.Drawing.Size(610, 446)
        Me.gbStep2.TabIndex = 2
        Me.gbStep2.Temp = 0
        Me.gbStep2.Text = "Select Period - Step 2 of 7"
        Me.gbStep2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboStep2_PayPeriod
        '
        Me.cboStep2_PayPeriod.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboStep2_PayPeriod.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboStep2_PayPeriod.FormattingEnabled = True
        Me.cboStep2_PayPeriod.Location = New System.Drawing.Point(427, 228)
        Me.cboStep2_PayPeriod.Name = "cboStep2_PayPeriod"
        Me.cboStep2_PayPeriod.Size = New System.Drawing.Size(123, 21)
        Me.cboStep2_PayPeriod.TabIndex = 157
        '
        'cboStep2_PayYear
        '
        Me.cboStep2_PayYear.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboStep2_PayYear.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboStep2_PayYear.FormattingEnabled = True
        Me.cboStep2_PayYear.Location = New System.Drawing.Point(427, 201)
        Me.cboStep2_PayYear.Name = "cboStep2_PayYear"
        Me.cboStep2_PayYear.Size = New System.Drawing.Size(123, 21)
        Me.cboStep2_PayYear.TabIndex = 156
        '
        'lblStep2_PayPeriod
        '
        Me.lblStep2_PayPeriod.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblStep2_PayPeriod.Location = New System.Drawing.Point(332, 230)
        Me.lblStep2_PayPeriod.Name = "lblStep2_PayPeriod"
        Me.lblStep2_PayPeriod.Size = New System.Drawing.Size(89, 16)
        Me.lblStep2_PayPeriod.TabIndex = 159
        Me.lblStep2_PayPeriod.Text = "Pay Period"
        Me.lblStep2_PayPeriod.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblStep2_PayYear
        '
        Me.lblStep2_PayYear.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblStep2_PayYear.Location = New System.Drawing.Point(332, 203)
        Me.lblStep2_PayYear.Name = "lblStep2_PayYear"
        Me.lblStep2_PayYear.Size = New System.Drawing.Size(89, 16)
        Me.lblStep2_PayYear.TabIndex = 158
        Me.lblStep2_PayYear.Text = "Pay Year"
        Me.lblStep2_PayYear.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblStep2_Desc
        '
        Me.lblStep2_Desc.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblStep2_Desc.Location = New System.Drawing.Point(244, 34)
        Me.lblStep2_Desc.Name = "lblStep2_Desc"
        Me.lblStep2_Desc.Size = New System.Drawing.Size(350, 62)
        Me.lblStep2_Desc.TabIndex = 154
        Me.lblStep2_Desc.Text = "Select Pay Year and Pay Period for which you want to Prepare Budget"
        '
        'pic_Step2_SelectPeriod
        '
        Me.pic_Step2_SelectPeriod.BackColor = System.Drawing.Color.WhiteSmoke
        Me.pic_Step2_SelectPeriod.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.pic_Step2_SelectPeriod.Image = Global.Aruti.Main.My.Resources.Resources.budget_preparation_wiz
        Me.pic_Step2_SelectPeriod.Location = New System.Drawing.Point(13, 34)
        Me.pic_Step2_SelectPeriod.Name = "pic_Step2_SelectPeriod"
        Me.pic_Step2_SelectPeriod.Size = New System.Drawing.Size(225, 401)
        Me.pic_Step2_SelectPeriod.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.pic_Step2_SelectPeriod.TabIndex = 1
        Me.pic_Step2_SelectPeriod.TabStop = False
        '
        'wizpWelcome
        '
        Me.wizpWelcome.Controls.Add(Me.gbStep1)
        Me.wizpWelcome.Description = "Description"
        Me.wizpWelcome.Location = New System.Drawing.Point(0, 0)
        Me.wizpWelcome.Name = "wizpWelcome"
        Me.wizpWelcome.Size = New System.Drawing.Size(639, 459)
        Me.wizpWelcome.Style = eZee.Common.eZeeWizardPageStyle.Custom
        Me.wizpWelcome.TabIndex = 7
        '
        'gbStep1
        '
        Me.gbStep1.BorderColor = System.Drawing.Color.Black
        Me.gbStep1.Checked = False
        Me.gbStep1.CollapseAllExceptThis = False
        Me.gbStep1.CollapsedHoverImage = Nothing
        Me.gbStep1.CollapsedNormalImage = Nothing
        Me.gbStep1.CollapsedPressedImage = Nothing
        Me.gbStep1.CollapseOnLoad = False
        Me.gbStep1.Controls.Add(Me.lbl_Step1_Welcome)
        Me.gbStep1.Controls.Add(Me.pic_Step1_Welcome)
        Me.gbStep1.ExpandedHoverImage = Nothing
        Me.gbStep1.ExpandedNormalImage = Nothing
        Me.gbStep1.ExpandedPressedImage = Nothing
        Me.gbStep1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbStep1.ForeColor = System.Drawing.SystemColors.ControlText
        Me.gbStep1.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbStep1.HeaderHeight = 25
        Me.gbStep1.HeaderMessage = ""
        Me.gbStep1.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbStep1.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbStep1.HeightOnCollapse = 0
        Me.gbStep1.LeftTextSpace = 0
        Me.gbStep1.Location = New System.Drawing.Point(14, 7)
        Me.gbStep1.Name = "gbStep1"
        Me.gbStep1.OpenHeight = 300
        Me.gbStep1.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbStep1.ShowBorder = True
        Me.gbStep1.ShowCheckBox = False
        Me.gbStep1.ShowCollapseButton = False
        Me.gbStep1.ShowDefaultBorderColor = True
        Me.gbStep1.ShowDownButton = False
        Me.gbStep1.ShowHeader = True
        Me.gbStep1.Size = New System.Drawing.Size(610, 446)
        Me.gbStep1.TabIndex = 1
        Me.gbStep1.Temp = 0
        Me.gbStep1.Text = "Welcome - Step 1 of 7"
        Me.gbStep1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lbl_Step1_Welcome
        '
        Me.lbl_Step1_Welcome.Font = New System.Drawing.Font("Tahoma", 24.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_Step1_Welcome.ForeColor = System.Drawing.SystemColors.ControlDark
        Me.lbl_Step1_Welcome.Location = New System.Drawing.Point(279, 53)
        Me.lbl_Step1_Welcome.Name = "lbl_Step1_Welcome"
        Me.lbl_Step1_Welcome.Size = New System.Drawing.Size(291, 345)
        Me.lbl_Step1_Welcome.TabIndex = 154
        Me.lbl_Step1_Welcome.Text = "Welcome to Budget Preparation Wizard..."
        Me.lbl_Step1_Welcome.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'pic_Step1_Welcome
        '
        Me.pic_Step1_Welcome.BackColor = System.Drawing.Color.WhiteSmoke
        Me.pic_Step1_Welcome.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.pic_Step1_Welcome.Image = Global.Aruti.Main.My.Resources.Resources.budget_preparation_wiz
        Me.pic_Step1_Welcome.Location = New System.Drawing.Point(13, 34)
        Me.pic_Step1_Welcome.Name = "pic_Step1_Welcome"
        Me.pic_Step1_Welcome.Size = New System.Drawing.Size(225, 401)
        Me.pic_Step1_Welcome.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.pic_Step1_Welcome.TabIndex = 1
        Me.pic_Step1_Welcome.TabStop = False
        '
        'EZeeHeader
        '
        Me.EZeeHeader.BackColor = System.Drawing.SystemColors.Control
        Me.EZeeHeader.BorderColor = System.Drawing.SystemColors.ControlDark
        Me.EZeeHeader.DescriptionForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.EZeeHeader.Dock = System.Windows.Forms.DockStyle.Top
        Me.EZeeHeader.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.EZeeHeader.GradiantStyle = eZee.Common.eZeeHeader.GradientStyle.Central
        Me.EZeeHeader.GradientColor1 = System.Drawing.SystemColors.Window
        Me.EZeeHeader.GradientColor2 = System.Drawing.SystemColors.Control
        Me.EZeeHeader.HeaderTextForeColor = System.Drawing.SystemColors.ControlText
        Me.EZeeHeader.Icon = Nothing
        Me.EZeeHeader.Location = New System.Drawing.Point(0, 0)
        Me.EZeeHeader.Message = "The Budget Preparation Wizard will help you in the preparation of the budget."
        Me.EZeeHeader.Name = "EZeeHeader"
        Me.EZeeHeader.Size = New System.Drawing.Size(856, 60)
        Me.EZeeHeader.TabIndex = 3
        Me.EZeeHeader.Title = "Prepare Budget"
        '
        'gbBudgetSteps
        '
        Me.gbBudgetSteps.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.gbBudgetSteps.BackColor = System.Drawing.Color.WhiteSmoke
        Me.gbBudgetSteps.BorderColor = System.Drawing.Color.Black
        Me.gbBudgetSteps.Checked = False
        Me.gbBudgetSteps.CollapseAllExceptThis = False
        Me.gbBudgetSteps.CollapsedHoverImage = Nothing
        Me.gbBudgetSteps.CollapsedNormalImage = Nothing
        Me.gbBudgetSteps.CollapsedPressedImage = Nothing
        Me.gbBudgetSteps.CollapseOnLoad = False
        Me.gbBudgetSteps.Controls.Add(Me.pnlStep6)
        Me.gbBudgetSteps.Controls.Add(Me.pnlStep7)
        Me.gbBudgetSteps.Controls.Add(Me.pnlStep3)
        Me.gbBudgetSteps.Controls.Add(Me.pnlStep5)
        Me.gbBudgetSteps.Controls.Add(Me.pnlStep4)
        Me.gbBudgetSteps.Controls.Add(Me.pnlStep2)
        Me.gbBudgetSteps.Controls.Add(Me.pnlStep1)
        Me.gbBudgetSteps.Controls.Add(Me.objFooterSteps)
        Me.gbBudgetSteps.ExpandedHoverImage = Nothing
        Me.gbBudgetSteps.ExpandedNormalImage = Nothing
        Me.gbBudgetSteps.ExpandedPressedImage = Nothing
        Me.gbBudgetSteps.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbBudgetSteps.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbBudgetSteps.HeaderHeight = 25
        Me.gbBudgetSteps.HeaderMessage = ""
        Me.gbBudgetSteps.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbBudgetSteps.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbBudgetSteps.HeightOnCollapse = 0
        Me.gbBudgetSteps.LeftTextSpace = 0
        Me.gbBudgetSteps.Location = New System.Drawing.Point(12, 73)
        Me.gbBudgetSteps.Name = "gbBudgetSteps"
        Me.gbBudgetSteps.OpenHeight = 300
        Me.gbBudgetSteps.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbBudgetSteps.ShowBorder = True
        Me.gbBudgetSteps.ShowCheckBox = False
        Me.gbBudgetSteps.ShowCollapseButton = False
        Me.gbBudgetSteps.ShowDefaultBorderColor = True
        Me.gbBudgetSteps.ShowDownButton = False
        Me.gbBudgetSteps.ShowHeader = True
        Me.gbBudgetSteps.Size = New System.Drawing.Size(216, 500)
        Me.gbBudgetSteps.TabIndex = 4
        Me.gbBudgetSteps.Temp = 0
        Me.gbBudgetSteps.Text = "Budget Steps"
        Me.gbBudgetSteps.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'pnlStep6
        '
        Me.pnlStep6.Controls.Add(Me.picSide_Step6_Check)
        Me.pnlStep6.Controls.Add(Me.lblStep6)
        Me.pnlStep6.Location = New System.Drawing.Point(3, 275)
        Me.pnlStep6.Name = "pnlStep6"
        Me.pnlStep6.Size = New System.Drawing.Size(210, 41)
        Me.pnlStep6.TabIndex = 9
        '
        'picSide_Step6_Check
        '
        Me.picSide_Step6_Check.Image = CType(resources.GetObject("picSide_Step6_Check.Image"), System.Drawing.Image)
        Me.picSide_Step6_Check.Location = New System.Drawing.Point(10, 13)
        Me.picSide_Step6_Check.Name = "picSide_Step6_Check"
        Me.picSide_Step6_Check.Size = New System.Drawing.Size(16, 16)
        Me.picSide_Step6_Check.TabIndex = 160
        Me.picSide_Step6_Check.TabStop = False
        '
        'lblStep6
        '
        Me.lblStep6.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblStep6.Location = New System.Drawing.Point(32, 13)
        Me.lblStep6.Name = "lblStep6"
        Me.lblStep6.Size = New System.Drawing.Size(175, 16)
        Me.lblStep6.TabIndex = 159
        Me.lblStep6.Text = "6.  Budget Allocation"
        Me.lblStep6.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'pnlStep7
        '
        Me.pnlStep7.Controls.Add(Me.lblStep7)
        Me.pnlStep7.Location = New System.Drawing.Point(3, 322)
        Me.pnlStep7.Name = "pnlStep7"
        Me.pnlStep7.Size = New System.Drawing.Size(210, 41)
        Me.pnlStep7.TabIndex = 7
        '
        'lblStep7
        '
        Me.lblStep7.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblStep7.Location = New System.Drawing.Point(32, 13)
        Me.lblStep7.Name = "lblStep7"
        Me.lblStep7.Size = New System.Drawing.Size(175, 16)
        Me.lblStep7.TabIndex = 159
        Me.lblStep7.Text = "7.  Finish"
        Me.lblStep7.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'pnlStep3
        '
        Me.pnlStep3.Controls.Add(Me.picSide_Step3_Check)
        Me.pnlStep3.Controls.Add(Me.lblStep3)
        Me.pnlStep3.Location = New System.Drawing.Point(3, 134)
        Me.pnlStep3.Name = "pnlStep3"
        Me.pnlStep3.Size = New System.Drawing.Size(210, 41)
        Me.pnlStep3.TabIndex = 6
        '
        'picSide_Step3_Check
        '
        Me.picSide_Step3_Check.Image = CType(resources.GetObject("picSide_Step3_Check.Image"), System.Drawing.Image)
        Me.picSide_Step3_Check.Location = New System.Drawing.Point(10, 13)
        Me.picSide_Step3_Check.Name = "picSide_Step3_Check"
        Me.picSide_Step3_Check.Size = New System.Drawing.Size(16, 16)
        Me.picSide_Step3_Check.TabIndex = 160
        Me.picSide_Step3_Check.TabStop = False
        '
        'lblStep3
        '
        Me.lblStep3.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblStep3.Location = New System.Drawing.Point(32, 13)
        Me.lblStep3.Name = "lblStep3"
        Me.lblStep3.Size = New System.Drawing.Size(175, 16)
        Me.lblStep3.TabIndex = 159
        Me.lblStep3.Text = "3.  Allocation By"
        Me.lblStep3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'pnlStep5
        '
        Me.pnlStep5.Controls.Add(Me.picSide_Step5_Check)
        Me.pnlStep5.Controls.Add(Me.lblStep5)
        Me.pnlStep5.Location = New System.Drawing.Point(3, 228)
        Me.pnlStep5.Name = "pnlStep5"
        Me.pnlStep5.Size = New System.Drawing.Size(210, 41)
        Me.pnlStep5.TabIndex = 5
        '
        'picSide_Step5_Check
        '
        Me.picSide_Step5_Check.Image = CType(resources.GetObject("picSide_Step5_Check.Image"), System.Drawing.Image)
        Me.picSide_Step5_Check.Location = New System.Drawing.Point(10, 13)
        Me.picSide_Step5_Check.Name = "picSide_Step5_Check"
        Me.picSide_Step5_Check.Size = New System.Drawing.Size(16, 16)
        Me.picSide_Step5_Check.TabIndex = 160
        Me.picSide_Step5_Check.TabStop = False
        '
        'lblStep5
        '
        Me.lblStep5.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblStep5.Location = New System.Drawing.Point(32, 13)
        Me.lblStep5.Name = "lblStep5"
        Me.lblStep5.Size = New System.Drawing.Size(175, 16)
        Me.lblStep5.TabIndex = 159
        Me.lblStep5.Text = "5.  Select Previous Periods"
        Me.lblStep5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'pnlStep4
        '
        Me.pnlStep4.Controls.Add(Me.picSide_Step4_Check)
        Me.pnlStep4.Controls.Add(Me.lblStep4)
        Me.pnlStep4.Location = New System.Drawing.Point(3, 181)
        Me.pnlStep4.Name = "pnlStep4"
        Me.pnlStep4.Size = New System.Drawing.Size(210, 41)
        Me.pnlStep4.TabIndex = 4
        '
        'picSide_Step4_Check
        '
        Me.picSide_Step4_Check.Image = CType(resources.GetObject("picSide_Step4_Check.Image"), System.Drawing.Image)
        Me.picSide_Step4_Check.Location = New System.Drawing.Point(10, 13)
        Me.picSide_Step4_Check.Name = "picSide_Step4_Check"
        Me.picSide_Step4_Check.Size = New System.Drawing.Size(16, 16)
        Me.picSide_Step4_Check.TabIndex = 160
        Me.picSide_Step4_Check.TabStop = False
        '
        'lblStep4
        '
        Me.lblStep4.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblStep4.Location = New System.Drawing.Point(32, 13)
        Me.lblStep4.Name = "lblStep4"
        Me.lblStep4.Size = New System.Drawing.Size(175, 16)
        Me.lblStep4.TabIndex = 159
        Me.lblStep4.Text = "4.  Select Transaction Heads"
        Me.lblStep4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'pnlStep2
        '
        Me.pnlStep2.Controls.Add(Me.picSide_Step2_Check)
        Me.pnlStep2.Controls.Add(Me.lblStep2)
        Me.pnlStep2.Location = New System.Drawing.Point(3, 87)
        Me.pnlStep2.Name = "pnlStep2"
        Me.pnlStep2.Size = New System.Drawing.Size(210, 41)
        Me.pnlStep2.TabIndex = 3
        '
        'picSide_Step2_Check
        '
        Me.picSide_Step2_Check.Image = CType(resources.GetObject("picSide_Step2_Check.Image"), System.Drawing.Image)
        Me.picSide_Step2_Check.Location = New System.Drawing.Point(10, 13)
        Me.picSide_Step2_Check.Name = "picSide_Step2_Check"
        Me.picSide_Step2_Check.Size = New System.Drawing.Size(16, 16)
        Me.picSide_Step2_Check.TabIndex = 160
        Me.picSide_Step2_Check.TabStop = False
        '
        'lblStep2
        '
        Me.lblStep2.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblStep2.Location = New System.Drawing.Point(32, 13)
        Me.lblStep2.Name = "lblStep2"
        Me.lblStep2.Size = New System.Drawing.Size(175, 16)
        Me.lblStep2.TabIndex = 159
        Me.lblStep2.Text = "2.  Select Period"
        Me.lblStep2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'pnlStep1
        '
        Me.pnlStep1.Controls.Add(Me.picSide_Step1_Check)
        Me.pnlStep1.Controls.Add(Me.lblStep1)
        Me.pnlStep1.Location = New System.Drawing.Point(3, 40)
        Me.pnlStep1.Name = "pnlStep1"
        Me.pnlStep1.Size = New System.Drawing.Size(210, 41)
        Me.pnlStep1.TabIndex = 2
        '
        'picSide_Step1_Check
        '
        Me.picSide_Step1_Check.Image = Global.Aruti.Main.My.Resources.Resources.Check1_16
        Me.picSide_Step1_Check.Location = New System.Drawing.Point(10, 13)
        Me.picSide_Step1_Check.Name = "picSide_Step1_Check"
        Me.picSide_Step1_Check.Size = New System.Drawing.Size(16, 16)
        Me.picSide_Step1_Check.TabIndex = 160
        Me.picSide_Step1_Check.TabStop = False
        '
        'lblStep1
        '
        Me.lblStep1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblStep1.Location = New System.Drawing.Point(32, 13)
        Me.lblStep1.Name = "lblStep1"
        Me.lblStep1.Size = New System.Drawing.Size(175, 16)
        Me.lblStep1.TabIndex = 159
        Me.lblStep1.Text = "1.  Welcome"
        Me.lblStep1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objFooterSteps
        '
        Me.objFooterSteps.BorderColor = System.Drawing.Color.Silver
        Me.objFooterSteps.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.objFooterSteps.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objFooterSteps.GradiantStyle = eZee.Common.eZeeFooter.GradientStyle.Up
        Me.objFooterSteps.GradientColor1 = System.Drawing.SystemColors.Control
        Me.objFooterSteps.GradientColor2 = System.Drawing.SystemColors.Control
        Me.objFooterSteps.Location = New System.Drawing.Point(0, 450)
        Me.objFooterSteps.Name = "objFooterSteps"
        Me.objFooterSteps.Size = New System.Drawing.Size(216, 50)
        Me.objFooterSteps.TabIndex = 1
        '
        'DataGridViewTextBoxColumn1
        '
        Me.DataGridViewTextBoxColumn1.Frozen = True
        Me.DataGridViewTextBoxColumn1.HeaderText = "Trn. Head"
        Me.DataGridViewTextBoxColumn1.Name = "DataGridViewTextBoxColumn1"
        Me.DataGridViewTextBoxColumn1.ReadOnly = True
        '
        'DataGridViewTextBoxColumn2
        '
        Me.DataGridViewTextBoxColumn2.Frozen = True
        Me.DataGridViewTextBoxColumn2.HeaderText = "Expense/Revenue"
        Me.DataGridViewTextBoxColumn2.Name = "DataGridViewTextBoxColumn2"
        Me.DataGridViewTextBoxColumn2.ReadOnly = True
        '
        'frmBudgetPreparation_Wizard
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(856, 573)
        Me.Controls.Add(Me.gbBudgetSteps)
        Me.Controls.Add(Me.EZeeHeader)
        Me.Controls.Add(Me.wizBudget)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmBudgetPreparation_Wizard"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Budget Preparation"
        Me.wizBudget.ResumeLayout(False)
        Me.wizpFinish.ResumeLayout(False)
        Me.Panel4.ResumeLayout(False)
        Me.gbStep7.ResumeLayout(False)
        CType(Me.picStep7_Finish, System.ComponentModel.ISupportInitialize).EndInit()
        Me.wizpPrepareBudget.ResumeLayout(False)
        Me.gbStep6.ResumeLayout(False)
        Me.Panel3.ResumeLayout(False)
        CType(Me.dgvStep6_Budget, System.ComponentModel.ISupportInitialize).EndInit()
        Me.wizpSelectPreviusPeriod.ResumeLayout(False)
        Me.gbStep5.ResumeLayout(False)
        Me.Panel2.ResumeLayout(False)
        Me.Panel2.PerformLayout()
        Me.wizpSelectTrnHead.ResumeLayout(False)
        Me.gbStep4.ResumeLayout(False)
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        Me.wizpAllocationBy.ResumeLayout(False)
        Me.gbStep3.ResumeLayout(False)
        Me.gbStep3_AllocationBy.ResumeLayout(False)
        Me.pnlAllocationBy.ResumeLayout(False)
        CType(Me.picStep3_GroupBy, System.ComponentModel.ISupportInitialize).EndInit()
        Me.wizpSelectPeriod.ResumeLayout(False)
        Me.gbStep2.ResumeLayout(False)
        CType(Me.pic_Step2_SelectPeriod, System.ComponentModel.ISupportInitialize).EndInit()
        Me.wizpWelcome.ResumeLayout(False)
        Me.gbStep1.ResumeLayout(False)
        CType(Me.pic_Step1_Welcome, System.ComponentModel.ISupportInitialize).EndInit()
        Me.gbBudgetSteps.ResumeLayout(False)
        Me.pnlStep6.ResumeLayout(False)
        CType(Me.picSide_Step6_Check, System.ComponentModel.ISupportInitialize).EndInit()
        Me.pnlStep7.ResumeLayout(False)
        Me.pnlStep3.ResumeLayout(False)
        CType(Me.picSide_Step3_Check, System.ComponentModel.ISupportInitialize).EndInit()
        Me.pnlStep5.ResumeLayout(False)
        CType(Me.picSide_Step5_Check, System.ComponentModel.ISupportInitialize).EndInit()
        Me.pnlStep4.ResumeLayout(False)
        CType(Me.picSide_Step4_Check, System.ComponentModel.ISupportInitialize).EndInit()
        Me.pnlStep2.ResumeLayout(False)
        CType(Me.picSide_Step2_Check, System.ComponentModel.ISupportInitialize).EndInit()
        Me.pnlStep1.ResumeLayout(False)
        CType(Me.picSide_Step1_Check, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents wizBudget As eZee.Common.eZeeWizard
    Friend WithEvents EZeeHeader As eZee.Common.eZeeHeader
    Friend WithEvents wizpWelcome As eZee.Common.eZeeWizardPage
    Friend WithEvents gbStep1 As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents gbBudgetSteps As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents wizpSelectPeriod As eZee.Common.eZeeWizardPage
    Friend WithEvents gbStep2 As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents cboStep2_PayPeriod As System.Windows.Forms.ComboBox
    Friend WithEvents cboStep2_PayYear As System.Windows.Forms.ComboBox
    Friend WithEvents lblStep2_PayPeriod As System.Windows.Forms.Label
    Friend WithEvents lblStep2_PayYear As System.Windows.Forms.Label
    Friend WithEvents lblStep2_Desc As System.Windows.Forms.Label
    Friend WithEvents pic_Step2_SelectPeriod As System.Windows.Forms.PictureBox
    Friend WithEvents lbl_Step1_Welcome As System.Windows.Forms.Label
    Friend WithEvents pic_Step1_Welcome As System.Windows.Forms.PictureBox
    Friend WithEvents wizpSelectTrnHead As eZee.Common.eZeeWizardPage
    Friend WithEvents wizpSelectPreviusPeriod As eZee.Common.eZeeWizardPage
    Friend WithEvents wizpPrepareBudget As eZee.Common.eZeeWizardPage
    Friend WithEvents gbStep4 As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents lblStep4_Desc As System.Windows.Forms.Label
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents gbStep5 As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents Panel2 As System.Windows.Forms.Panel
    Friend WithEvents lblStep5_Desc As System.Windows.Forms.Label
    Friend WithEvents gbStep6 As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents Panel3 As System.Windows.Forms.Panel
    Friend WithEvents lblStep6_Desc As System.Windows.Forms.Label
    Friend WithEvents dgvStep6_Budget As System.Windows.Forms.DataGridView
    Friend WithEvents wizpFinish As eZee.Common.eZeeWizardPage
    Friend WithEvents Panel4 As System.Windows.Forms.Panel
    Friend WithEvents gbStep7 As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents lblStep7_Finish As System.Windows.Forms.Label
    Friend WithEvents picStep7_Finish As System.Windows.Forms.PictureBox
    Friend WithEvents DataGridViewTextBoxColumn1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn2 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objFooterSteps As eZee.Common.eZeeFooter
    Friend WithEvents pnlStep1 As System.Windows.Forms.Panel
    Friend WithEvents lblStep1 As System.Windows.Forms.Label
    Friend WithEvents picSide_Step1_Check As System.Windows.Forms.PictureBox
    Friend WithEvents pnlStep2 As System.Windows.Forms.Panel
    Friend WithEvents picSide_Step2_Check As System.Windows.Forms.PictureBox
    Friend WithEvents lblStep2 As System.Windows.Forms.Label
    Friend WithEvents pnlStep3 As System.Windows.Forms.Panel
    Friend WithEvents picSide_Step3_Check As System.Windows.Forms.PictureBox
    Friend WithEvents lblStep3 As System.Windows.Forms.Label
    Friend WithEvents pnlStep5 As System.Windows.Forms.Panel
    Friend WithEvents picSide_Step5_Check As System.Windows.Forms.PictureBox
    Friend WithEvents lblStep5 As System.Windows.Forms.Label
    Friend WithEvents pnlStep4 As System.Windows.Forms.Panel
    Friend WithEvents picSide_Step4_Check As System.Windows.Forms.PictureBox
    Friend WithEvents lblStep4 As System.Windows.Forms.Label
    Friend WithEvents pnlStep7 As System.Windows.Forms.Panel
    Friend WithEvents lblStep7 As System.Windows.Forms.Label
    Friend WithEvents wizpAllocationBy As eZee.Common.eZeeWizardPage
    Friend WithEvents gbStep3 As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents lblStep3_Desc As System.Windows.Forms.Label
    Friend WithEvents picStep3_GroupBy As System.Windows.Forms.PictureBox
    Friend WithEvents gbStep3_AllocationBy As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents pnlAllocationBy As System.Windows.Forms.Panel
    Friend WithEvents radPayPoint As System.Windows.Forms.RadioButton
    Friend WithEvents radCostCenter As System.Windows.Forms.RadioButton
    Friend WithEvents radSection As System.Windows.Forms.RadioButton
    Friend WithEvents radClassnClassGrp As System.Windows.Forms.RadioButton
    Friend WithEvents radAccess As System.Windows.Forms.RadioButton
    Friend WithEvents radGrade As System.Windows.Forms.RadioButton
    Friend WithEvents radJobnJobGrp As System.Windows.Forms.RadioButton
    Friend WithEvents radDepInDepGroup As System.Windows.Forms.RadioButton
    Friend WithEvents radSectionDept As System.Windows.Forms.RadioButton
    Friend WithEvents pnlStep6 As System.Windows.Forms.Panel
    Friend WithEvents picSide_Step6_Check As System.Windows.Forms.PictureBox
    Friend WithEvents lblStep6 As System.Windows.Forms.Label
    Friend WithEvents radStation As System.Windows.Forms.RadioButton
    Friend WithEvents cboTrnHeadType As System.Windows.Forms.ComboBox
    Friend WithEvents lvStep4_TrnHead As System.Windows.Forms.ListView
    Friend WithEvents objcolhCheck As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhTrnHeadName As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhTrnHeadType As System.Windows.Forms.ColumnHeader
    Friend WithEvents objchkSelectAll As System.Windows.Forms.CheckBox
    Friend WithEvents objchkSelectAll_Period As System.Windows.Forms.CheckBox
    Friend WithEvents lvStep5_PayPeriod As System.Windows.Forms.ListView
    Friend WithEvents objcolh_Period_Check As System.Windows.Forms.ColumnHeader
    Friend WithEvents colh_Period_PayYear As System.Windows.Forms.ColumnHeader
    Friend WithEvents colh_Period_PayPeriod As System.Windows.Forms.ColumnHeader
    Friend WithEvents colh_Period_StartDate As System.Windows.Forms.ColumnHeader
    Friend WithEvents colh_Period_EndDate As System.Windows.Forms.ColumnHeader
End Class
