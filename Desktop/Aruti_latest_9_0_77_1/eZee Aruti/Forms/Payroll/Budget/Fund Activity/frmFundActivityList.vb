﻿Option Strict On

#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data

#End Region

Public Class frmFundActivityList

#Region " Private Varaibles "

    Private objFundActivity As clsfundactivity_Tran
    Private ReadOnly mstrModuleName As String = "frmFundActivityList"

#End Region

#Region " Private Function "

    Private Sub SetVisibility()
        Try
            btnNew.Enabled = User._Object.Privilege._AllowToAddFundActivity
            btnEdit.Enabled = User._Object.Privilege._AllowToEditFundActivity
            btnDelete.Enabled = User._Object.Privilege._AllowToDeleteFundActivity
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetVisibility", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub FillCombo()
        Try
            Dim objFundProjectCode As New clsFundProjectCode
            Dim dsList As DataSet = objFundProjectCode.GetComboList("List", True)
            cboFundProjectCode.ValueMember = "fundprojectcodeunkid"
            cboFundProjectCode.DisplayMember = "fundprojectname"
            cboFundProjectCode.DataSource = dsList.Tables(0)
            cboFundProjectCode.SelectedValue = 0
            objFundProjectCode = Nothing
            dsList = Nothing

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub FillList()
        Dim dsList As New DataSet
        Dim strSearch As String = ""
        Try

            If User._Object.Privilege._AllowToViewFundActivity = False Then Exit Sub

            If CInt(cboFundProjectCode.SelectedValue) > 0 Then
                strSearch &= "AND bgfundactivity_tran.fundprojectcodeunkid = '" & CInt(cboFundProjectCode.SelectedValue) & "' "
            End If

            If CInt(cboFundActivity.SelectedValue) > 0 Then
                strSearch &= "AND bgfundactivity_tran.fundactivityunkid = '" & CInt(cboFundActivity.SelectedValue) & "' "
            End If

            If strSearch.Length > 0 Then
                strSearch = strSearch.Substring(3)
            End If

            dsList = objFundActivity.GetList("List", strSearch)

            lvFundActivity.Items.Clear()
            lvFundActivity.GridLines = False

            For Each dRow As DataRow In dsList.Tables(0).Rows
                Dim lvItem As New ListViewItem
                lvItem.Text = dRow("fundprojectname").ToString
                lvItem.SubItems.Add(dRow("activity_code").ToString)
                lvItem.SubItems.Add(dRow("activity_name").ToString)
                lvItem.SubItems.Add(Format(CDec(dRow("amount")), GUI.fmtCurrency))
                lvItem.SubItems.Add(Format(CDec(dRow("notify_amount")), GUI.fmtCurrency))
                lvItem.Tag = CInt(dRow("fundactivityunkid"))
                lvFundActivity.Items.Add(lvItem)
            Next

            lvFundActivity.GroupingColumn = ColhFundProjectCode
            lvFundActivity.DisplayGroups(True)

            If lvFundActivity.Items.Count > 4 Then
                colhActivityName.Width = 250 - 20
            Else
                colhActivityName.Width = 250
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillList", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

#Region " Form's Events "

    Private Sub frmFundActivityList_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        objFundActivity = New clsfundactivity_Tran
        Try
            Call Set_Logo(Me, gApplicationType)
            Language.setLanguage(Me.Name)
            Call OtherSettings()
            'Varsha Rana (17-Oct-2017) -- Start
            'Enhancement - Give user privileges in payroll module.
            Call SetVisibility()
            'Varsha Rana (17-Oct-2017) -- End
            Call FillCombo()
            If lvFundActivity.Items.Count > 0 Then lvFundActivity.Items(0).Selected = True
            lvFundActivity.Select()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmFundActivityList_Load", mstrModuleName)
        End Try
    End Sub

    Private Sub frmFundActivityList_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        objFundActivity = Nothing
    End Sub

    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsfundactivity_Tran.SetMessages()
            objfrm._Other_ModuleNames = "clsfundactivity_Tran"
            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub

#End Region

#Region " Buttons "

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Me.Close()
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "btnClose_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnNew_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNew.Click
        Dim frm As New frmFundActivityAddEdit
        Try
            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If

            If frm.displayDialog(-1, enAction.ADD_CONTINUE) Then
                Call FillList()
            End If
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "btnNew_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub btnEdit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEdit.Click

        If lvFundActivity.SelectedItems.Count < 1 Then
            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please select Fund Activity from the list to perform further operation."), enMsgBoxStyle.Information) '?1
            lvFundActivity.Select()
            Exit Sub
        End If

        Dim frm As New frmFundActivityAddEdit
        Try
            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If

            If frm.displayDialog(CInt(lvFundActivity.SelectedItems(0).Tag), enAction.EDIT_ONE) Then
                Call FillList()
            End If
            frm = Nothing
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "btnEdit_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub btnDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDelete.Click
        If lvFundActivity.SelectedItems.Count < 1 Then
            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please select Fund Activity from the list to perform further operation."), enMsgBoxStyle.Information) '?1
            lvFundActivity.Select()
            Exit Sub
        End If
        Try
            'Sohail (02 Sep 2016) -- Start
            'Enhancement -  63.1 - New Budget Redesign for Marie Stopes.
            Dim strFrmName As String = ""
            If objFundActivity.isUsed(CInt(lvFundActivity.SelectedItems(0).Tag), strFrmName) = True Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Sorry, You cannot delete this Fund Activity. Reason: This Fund Activity is in use") & strFrmName, enMsgBoxStyle.Information)
                lvFundActivity.Select()
                Exit Try
            End If
            'Sohail (02 Sep 2016) -- End

            If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Are you sure you want to delete this Fund Activity?"), CType(enMsgBoxStyle.Question + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.Yes Then

                Dim frm As New frmReasonSelection
                Dim mstrVoidReason As String = String.Empty
                frm.displayDialog(enVoidCategoryType.PAYROLL, mstrVoidReason)
                If mstrVoidReason.Length <= 0 Then
                    Exit Sub
                End If
                frm = Nothing
                objFundActivity._Fundactivityunkid = CInt(lvFundActivity.SelectedItems(0).Tag)
                objFundActivity._Voidreason = mstrVoidReason
                objFundActivity._Voiduserunkid = User._Object._Userunkid

                'S.SANDEEP [28-May-2018] -- START
                'ISSUE/ENHANCEMENT : {Audit Trails} 
                objFundActivity._FormName = mstrModuleName
                objFundActivity._LoginEmployeeunkid = 0
                objFundActivity._ClientIP = getIP()
                objFundActivity._HostName = getHostName()
                objFundActivity._FromWeb = False
                objFundActivity._AuditUserId = User._Object._Userunkid
objFundActivity._CompanyUnkid = Company._Object._Companyunkid
                objFundActivity._AuditDate = ConfigParameter._Object._CurrentDateAndTime
                'S.SANDEEP [28-May-2018] -- END

                objFundActivity.Delete(CInt(lvFundActivity.SelectedItems(0).Tag))
                If objFundActivity._Message <> "" Then
                    eZeeMsgBox.Show(objFundActivity._Message, enMsgBoxStyle.Information)
                Else
                    lvFundActivity.SelectedItems(0).Remove()
                End If
            End If
            lvFundActivity.Select()
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "btnDelete_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnSearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearch.Click
        Try
            Call FillList()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearch_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnSearchFundProjectCode_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchFundProjectCode.Click
        Try
            Dim objfrm As New frmCommonSearch

            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            With objfrm
                .ValueMember = cboFundProjectCode.ValueMember
                .DisplayMember = cboFundProjectCode.DisplayMember
                .DataSource = CType(cboFundProjectCode.DataSource, DataTable)
                .CodeMember = "fundprojectcode"
            End With

            If objfrm.DisplayDialog Then
                cboFundProjectCode.SelectedValue = objfrm.SelectedValue
                cboFundProjectCode.Focus()
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchFundProjectCode_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnSearchFundActivity_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchFundActivity.Click
        Try
            Dim objfrm As New frmCommonSearch

            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            With objfrm
                .ValueMember = cboFundActivity.ValueMember
                .DisplayMember = cboFundActivity.DisplayMember
                .DataSource = CType(cboFundActivity.DataSource, DataTable)
                .CodeMember = "activitycode"
            End With

            If objfrm.DisplayDialog Then
                cboFundActivity.SelectedValue = objfrm.SelectedValue
                cboFundActivity.Focus()
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchFundActivity_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnReset.Click
        Try
            cboFundProjectCode.SelectedValue = 0
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnReset_Click", mstrModuleName)
        End Try
    End Sub


#End Region

#Region "Combobox Event"

    Private Sub cboFundProjectCode_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboFundProjectCode.SelectedIndexChanged
        Try
            Dim dsList As DataSet = objFundActivity.GetComboList("List", True, CInt(cboFundProjectCode.SelectedValue))
            cboFundActivity.ValueMember = "fundactivityunkid"
            cboFundActivity.DisplayMember = "activityname"
            cboFundActivity.DataSource = dsList.Tables(0)
            cboFundActivity.SelectedValue = 0
            dsList = Nothing
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "cboFundProjectCode_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

#End Region





    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Private Sub OtherSettings()
        Try
            Me.SuspendLayout()
           
            Call SetLanguage()

            Me.gbFilterCriteria.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbFilterCriteria.ForeColor = GUI._eZeeContainerHeaderForeColor


            Me.btnDelete.GradientBackColor = GUI._ButttonBackColor
            Me.btnDelete.GradientForeColor = GUI._ButttonFontColor

            Me.btnEdit.GradientBackColor = GUI._ButttonBackColor
            Me.btnEdit.GradientForeColor = GUI._ButttonFontColor

            Me.btnNew.GradientBackColor = GUI._ButttonBackColor
            Me.btnNew.GradientForeColor = GUI._ButttonFontColor

            Me.btnClose.GradientBackColor = GUI._ButttonBackColor
            Me.btnClose.GradientForeColor = GUI._ButttonFontColor


            Me.ResumeLayout()
        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "OtherSettings", mstrModuleName)
        End Try
    End Sub


    Private Sub SetLanguage()
        Try
            Me.Text = Language._Object.getCaption(Me.Name, Me.Text)

            Me.gbFilterCriteria.Text = Language._Object.getCaption(Me.gbFilterCriteria.Name, Me.gbFilterCriteria.Text)
            Me.lblFundProjectCode.Text = Language._Object.getCaption(Me.lblFundProjectCode.Name, Me.lblFundProjectCode.Text)
            Me.btnDelete.Text = Language._Object.getCaption(Me.btnDelete.Name, Me.btnDelete.Text)
            Me.btnEdit.Text = Language._Object.getCaption(Me.btnEdit.Name, Me.btnEdit.Text)
            Me.btnNew.Text = Language._Object.getCaption(Me.btnNew.Name, Me.btnNew.Text)
            Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
            Me.ColhFundProjectCode.Text = Language._Object.getCaption(CStr(Me.ColhFundProjectCode.Tag), Me.ColhFundProjectCode.Text)
            Me.colhActivityCode.Text = Language._Object.getCaption(CStr(Me.colhActivityCode.Tag), Me.colhActivityCode.Text)
            Me.colhActivityName.Text = Language._Object.getCaption(CStr(Me.colhActivityName.Tag), Me.colhActivityName.Text)
            Me.colhCurrentBalance.Text = Language._Object.getCaption(CStr(Me.colhCurrentBalance.Tag), Me.colhCurrentBalance.Text)
            Me.LblFundActivity.Text = Language._Object.getCaption(Me.LblFundActivity.Name, Me.LblFundActivity.Text)
            Me.colhNotifyAmount.Text = Language._Object.getCaption(CStr(Me.colhNotifyAmount.Tag), Me.colhNotifyAmount.Text)

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
        End Try
    End Sub


    Private Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Please select Fund Activity from the list to perform further operation.")
            Language.setMessage(mstrModuleName, 2, "Are you sure you want to delete this Fund Activity?")
            Language.setMessage(mstrModuleName, 3, "Sorry, You cannot delete this Fund Activity. Reason: This Fund Activity is in use")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class