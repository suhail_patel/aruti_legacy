﻿Option Strict On

Imports eZeeCommonLib
Imports Aruti.Data

Public Class frmBudgetFormula

#Region " Private Variables "

    Private ReadOnly mstrModuleName As String = "frmBudgetFormula"
    Private mblnCancel As Boolean = True
    Private mdtFormula As DataTable
    Private mdtFormula_Tranhead As DataTable
    Private objBudgetformula As clsBudgetformula_master
    Private menAction As enAction = enAction.ADD_ONE
    Private mintFormulaTranHeadUnkid As Integer = -1
    Dim dtTable As DataTable = Nothing

#End Region

#Region " Display Dialog "

    Public Function displayDialog(ByRef intUnkId As Integer, ByVal eAction As enAction) As Boolean
        Try
            mintFormulaTranHeadUnkid = intUnkId
            menAction = eAction
            Me.ShowDialog()
            intUnkId = mintFormulaTranHeadUnkid
            Return Not mblnCancel
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "displayDialog", mstrModuleName)
        End Try
    End Function

#End Region

#Region " Private Methods "

    Private Sub setColor()
        Try
            txtHeadCode.BackColor = GUI.ColorComp
            txtHeadName.BackColor = GUI.ColorComp
            cboFunction.BackColor = GUI.ColorComp
            cboTrnHead.BackColor = GUI.ColorComp
            cboSalaryHead.BackColor = GUI.ColorOptional
            txtConstantValue.BackColor = GUI.ColorComp
            cboDefaultValue.BackColor = GUI.ColorComp
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "setColor", mstrModuleName)
        End Try
    End Sub

    Private Sub GetValue()
        Dim temp As String = ""
        Dim strEndDate As String = ""
        Dim dtRow As DataRow
        Dim mstrFormulaID As String = ""
        Try
            txtHeadCode.Text = objBudgetformula._Formula_Code
            txtHeadName.Text = objBudgetformula._Formula_Name


            For Each dsRow As DataRow In mdtFormula.Rows
                mstrFormulaID = dsRow.Item("functionid").ToString
                strEndDate = dsRow.Item("effectivedate").ToString()
                temp = ""

                For i As Integer = 1 To mstrFormulaID.Length
                    If Mid(mstrFormulaID, i, 1) = "+" OrElse Mid(mstrFormulaID, i, 1) = "-" OrElse Mid(mstrFormulaID, i, 1) = "*" OrElse Mid(mstrFormulaID, i, 1) = "/" OrElse _
                        Mid(mstrFormulaID, i, 1) = "(" OrElse Mid(mstrFormulaID, i, 1) = ")" Then

                        If temp.Trim.Length > 0 Then

                            cboFunction.SelectedValue = enFunction.CONSTANT_VALUE

                            dtRow = dtTable.NewRow
                            dtRow.Item("effectivedate") = strEndDate
                            dtRow.Item("functionid") = cboFunction.SelectedValue
                            dtRow.Item("function") = cboFunction.Text
                            dtRow.Item("headid") = temp
                            dtRow.Item("head") = temp
                            dtRow.Item("defaultid") = 0
                            dtRow.Item("default") = String.Empty
                            dtRow.Item("AUD") = ""
                            dtTable.Rows.Add(dtRow)

                        End If  ' temp.Trim.Length > 0 Then

                        temp = Trim(Mid(mstrFormulaID, i, 1))

                        If temp = "+" Then
                            cboFunction.SelectedValue = enFunction.Plus
                        ElseIf temp = "-" Then
                            cboFunction.SelectedValue = enFunction.Minus
                        ElseIf temp = "*" Then
                            cboFunction.SelectedValue = enFunction.Multiply
                        ElseIf temp = "/" Then
                            cboFunction.SelectedValue = enFunction.Divide
                        ElseIf temp = "(" Then
                            cboFunction.SelectedValue = enFunction.Open_Bracket
                        ElseIf temp = ")" Then
                            cboFunction.SelectedValue = enFunction.Close_Bracket
                        End If


                        dtRow = dtTable.NewRow
                        dtRow.Item("effectivedate") = strEndDate
                        dtRow.Item("functionid") = cboFunction.SelectedValue
                        dtRow.Item("function") = cboFunction.Text
                        dtRow.Item("headid") = temp
                        dtRow.Item("head") = temp
                        dtRow.Item("defaultid") = 0
                        dtRow.Item("default") = String.Empty
                        dtRow.Item("AUD") = ""
                        dtTable.Rows.Add(dtRow)

                        temp = ""
                        cboFunction.SelectedValue = 0

                    ElseIf Mid(mstrFormulaID, i, 1) <> "#" Then
                        temp += Trim(Mid(mstrFormulaID, i, 1))
                    Else

                        If temp <> "" Then

                            dtRow = dtTable.NewRow
                            Select Case temp.ToUpper

                                Case "BSL"  'FOR BASIC SALARY

                                    If temp.ToUpper = "BSL" Then
                                        cboFunction.SelectedValue = enFunction.BASIC_SALARY
                                    End If
                                    dtRow.Item("effectivedate") = strEndDate
                                    dtRow.Item("functionid") = cboFunction.SelectedValue
                                    dtRow.Item("function") = cboFunction.Text
                                    dtRow.Item("headid") = temp
                                    dtRow.Item("head") = temp
                                    dtRow.Item("headid") = temp.ToUpper
                                    dtRow.Item("head") = cboFunction.Text
                                    dtRow.Item("AUD") = ""
                                    dtTable.Rows.Add(dtRow)

                                Case Else   'FOR BASIC SALARY + ANY OTHER SALARY HEAD

                                    If temp.Length >= 5 AndAlso temp.Substring(0, 4) = "BSL|" Then
                                        cboFunction.SelectedValue = enFunction.BASIC_SALARY
                                        cboSalaryHead.SelectedValue = CInt(temp.Substring(4))

                                        dtRow.Item("effectivedate") = strEndDate
                                        dtRow.Item("functionid") = cboFunction.SelectedValue
                                        dtRow.Item("function") = cboFunction.Text
                                        dtRow.Item("headid") = temp.ToUpper
                                        dtRow.Item("head") = cboSalaryHead.Text
                                        Dim dRow() As DataRow
                                        dRow = mdtFormula_Tranhead.Select("tranheadunkid = " & CInt(temp.Substring(4)) & " AND AUD <> 'D' ")

                                        If dRow.Length > 0 Then
                                            cboDefaultValue.SelectedValue = CInt(dRow(0).Item("defaultid"))
                                            dtRow.Item("defaultid") = cboDefaultValue.SelectedValue
                                            dtRow.Item("default") = cboDefaultValue.Text
                                        Else
                                            cboDefaultValue.SelectedValue = 0
                                            dtRow.Item("defaultid") = cboDefaultValue.SelectedValue
                                            dtRow.Item("default") = cboDefaultValue.Text
                                        End If

                                        dtRow.Item("AUD") = ""
                                        dtTable.Rows.Add(dtRow)

                                        cboSalaryHead.SelectedValue = 0

                                    Else  'FOR TRANSACTION HEAD

                                        cboFunction.SelectedValue = enFunction.Transaction_Head
                                        cboTrnHead.SelectedValue = CInt(temp)

                                        dtRow.Item("effectivedate") = strEndDate
                                        dtRow.Item("functionid") = cboFunction.SelectedValue
                                        dtRow.Item("function") = cboFunction.Text
                                        dtRow.Item("headid") = cboTrnHead.SelectedValue
                                        dtRow.Item("head") = cboTrnHead.Text
                                        Dim dRow() As DataRow
                                        dRow = mdtFormula_Tranhead.Select("tranheadunkid = " & CInt(temp) & " AND AUD <> 'D' ")

                                        If dRow.Length > 0 Then
                                            cboDefaultValue.SelectedValue = CInt(dRow(0).Item("defaultid"))
                                            dtRow.Item("defaultid") = cboDefaultValue.SelectedValue
                                            dtRow.Item("default") = cboDefaultValue.Text
                                        Else
                                            cboDefaultValue.SelectedValue = 0
                                            dtRow.Item("defaultid") = cboDefaultValue.SelectedValue
                                            dtRow.Item("default") = cboDefaultValue.Text
                                        End If

                                        dtRow.Item("AUD") = ""
                                        dtTable.Rows.Add(dtRow)

                                        cboTrnHead.SelectedValue = 0

                                    End If

                            End Select

                            temp = ""

                        End If ' temp <> ""

                    End If  'Main Mid End if

                Next

                If temp.Trim.Length > 0 Then
                    cboFunction.SelectedValue = enFunction.CONSTANT_VALUE
                    dtRow = dtTable.NewRow
                    dtRow.Item("effectivedate") = strEndDate
                    dtRow.Item("functionid") = cboFunction.SelectedValue
                    dtRow.Item("function") = cboFunction.Text
                    dtRow.Item("headid") = temp
                    dtRow.Item("head") = temp
                    dtRow.Item("defaultid") = 0
                    dtRow.Item("default") = String.Empty
                    dtRow.Item("AUD") = ""
                    dtTable.Rows.Add(dtRow)
                    cboFunction.SelectedValue = 0
                End If

            Next

            'mdtFormula = dtTable

            Call FillList()
            Call DefineFormula()

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetValue", mstrModuleName)
        End Try
    End Sub

    Private Sub SetValue()
        Try
            objBudgetformula._Formula_Code = txtHeadCode.Text.Trim
            objBudgetformula._Formula_Name = txtHeadName.Text.Trim

            Dim mdtFormulaTran As DataTable = Nothing
            'Dim drRow As DataRow = Nothing
            'mdtFormulaTran = mdtFormula.Clone
            'drRow = mdtFormulaTran.NewRow
            'drRow("effectivedate") = eZeeDate.convertDate(dtpCumulativeStartDate.Value.Date)
            'drRow("functionid") = txtFormula.Tag.ToString()
            'drRow("function") = txtFormula.Text.ToString()
            'drRow("headid") = ""
            'drRow("head") = ""
            'drRow("defaultid") = CInt(cboDefaultValue.SelectedValue)
            'drRow("default") = cboDefaultValue.Text
            'drRow("AUD") = "A"
            'mdtFormulaTran.Rows.Add(drRow)


            objBudgetformula._FormulaTran = mdtFormula
            objBudgetformula._Formula_Tranhead = mdtFormula_Tranhead
            objBudgetformula._Isvoid = False
            objBudgetformula._Userunkid = User._Object._Userunkid
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetValue", mstrModuleName)
        End Try
    End Sub

    Private Sub FillCombo()
        Dim objMaster As New clsMasterData
        Dim objFormulaMst As New clsBudgetformula_master
        Dim objTranHead As New clsTransactionHead
        Dim dsCombo As DataSet
        Dim dtTable As DataTable = Nothing
        Dim objPeriod As New clscommom_period_Tran

        Try
            dsCombo = objMaster.getComboListFormula("Formula")
            dtTable = New DataView(dsCombo.Tables("Formula"), "Id IN (0, " & enFunction.BASIC_SALARY & "," & enFunction.Transaction_Head & ", " & enFunction.Plus & ", " & enFunction.Minus & ", " & enFunction.Multiply & ", " & enFunction.Divide & ", " & enFunction.Open_Bracket & ", " & enFunction.Close_Bracket & ", " & enFunction.CONSTANT_VALUE & ")", "", DataViewRowState.CurrentRows).ToTable
            With cboFunction
                .ValueMember = "Id"
                .DisplayMember = "Name"
                .DataSource = dtTable
                .SelectedValue = 0
            End With

            dsCombo = objFormulaMst.getComboList("FormulaHead", True)
            dtTable = New DataView(dsCombo.Tables(0), CStr(IIf(mintFormulaTranHeadUnkid > 0, "ID <>" & mintFormulaTranHeadUnkid, "")), "", DataViewRowState.CurrentRows).ToTable
            With cboTrnHead
                .DataSource = Nothing
                .ValueMember = "Id"
                .DisplayMember = "Name"
                .DataSource = dtTable
                .SelectedValue = 0
            End With

            dsCombo = objTranHead.getComboList(FinancialYear._Object._DatabaseName, "Salary", True, , , enTypeOf.Salary)
            With cboSalaryHead
                .DataSource = Nothing
                .ValueMember = "tranheadunkid"
                .DisplayMember = "Name"
                .DataSource = dsCombo.Tables("Salary")
                If .Items.Count > 0 Then .SelectedValue = 0
            End With

            dsCombo = objMaster.getComboListHeadFormulaDefaultValue("DefaultValue")
            With cboDefaultValue
                .ValueMember = "Id"
                .DisplayMember = "Name"
                .DataSource = dsCombo.Tables("DefaultValue")
                .SelectedValue = 0
            End With


        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        Finally
            objFormulaMst = Nothing
            objTranHead = Nothing
            objMaster = Nothing
            objPeriod = Nothing
        End Try
    End Sub

    Private Sub ResetControls()
        Try
            cboDefaultValue.SelectedValue = 0
            cboSalaryHead.SelectedValue = 0
            cboTrnHead.SelectedValue = 0
            txtConstantValue.Decimal = 0
            cboFunction.SelectedValue = 0
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "ResetControls", mstrModuleName)
        End Try
    End Sub

    'Private Sub FillSimpleSlabList()
    '    Try
    '        lvTrnHeadSlabSimple.Items.Clear()
    '        Dim lvItems As ListViewItem
    '        Dim drItem As DataRow

    '        'Sohail (21 Nov 2011) -- Start
    '        mdtSimpleSlab = New DataView(mdtSimpleSlab, "", "end_date DESC, amountupto ASC", DataViewRowState.CurrentRows).ToTable
    '        'Sohail (21 Nov 2011) -- End

    '        For Each drItem In mdtSimpleSlab.Rows
    '            If CStr(IIf(IsDBNull(drItem.Item("AUD")), "A", drItem.Item("AUD").ToString)) <> "D" Then
    '                lvItems = New ListViewItem
    '                lvItems.Text = drItem.Item("tranheadunkid").ToString
    '                lvItems.Tag = drItem.Item("tranheadslabtranunkid").ToString

    '                lvItems.SubItems.Add(Format(drItem.Item("amountupto"), GUI.fmtCurrency)) 'Sohail (11 May 2011)

    '                objSlabType.SelectedValue = CInt(drItem.Item("slabtype").ToString)
    '                lvItems.SubItems.Add(objSlabType.Text)
    '                objSlabType.SelectedValue = 0
    '                lvItems.SubItems(2).Tag = CInt(drItem.Item("slabtype").ToString)

    '                'Sohail (24 Sep 2013) -- Start
    '                'TRA - ENHANCEMENT
    '                'lvItems.SubItems.Add(Format(drItem.Item("valuebasis"), GUI.fmtCurrency)) 'Sohail (11 May 2011)
    '                If CInt(drItem.Item("slabtype").ToString) = enPaymentBy.Fromula Then
    '                    lvItems.SubItems.Add(drItem.Item("formula").ToString)
    '                    lvItems.SubItems(colhSimpleValueBasis.Index).Tag = drItem.Item("formulaid").ToString
    '                Else
    '                    lvItems.SubItems.Add(Format(drItem.Item("valuebasis"), GUI.fmtCurrency))
    '                    lvItems.SubItems(colhSimpleValueBasis.Index).Tag = CDbl(drItem.Item("valuebasis"))
    '                End If
    '                'Sohail (24 Sep 2013) -- End

    '                lvItems.SubItems.Add(drItem.Item("GUID").ToString)
    '                'Sohail (21 Nov 2011) -- Start
    '                lvItems.SubItems.Add(drItem.Item("period_name").ToString)
    '                lvItems.SubItems(colhSimplePeriod.Index).Tag = CInt(drItem.Item("periodunkid"))
    '                lvItems.SubItems.Add(drItem.Item("end_date").ToString)
    '                'Sohail (21 Nov 2011) -- End

    '                lvTrnHeadSlabSimple.Items.Add(lvItems)
    '            End If
    '        Next
    '        'Sohail (21 Nov 2011) -- Start
    '        lvTrnHeadSlabSimple.GroupingColumn = colhSimplePeriod
    '        lvTrnHeadSlabSimple.DisplayGroups(True)
    '        lvTrnHeadSlabSimple.GridLines = False

    '        If lvTrnHeadSlabSimple.Items.Count > 3 Then
    '            colhSimpleValueBasis.Width = 160 - 18
    '        Else
    '            colhSimpleValueBasis.Width = 160
    '        End If
    '        'Sohail (21 Nov 2011) -- End

    '    Catch ex As Exception
    '        DisplayError.Show("-1", ex.Message, "FillSimpleList", mstrModuleName)
    '    End Try
    'End Sub

    'Private Sub FillTaxSlabList()
    '    Try
    '        lvTrnHeadSlabInExcessof.Items.Clear()
    '        Dim lvItems As ListViewItem
    '        Dim drItem As DataRow

    '        'Sohail (21 Nov 2011) -- Start
    '        mdtTaxSlab = New DataView(mdtTaxSlab, "", "end_date DESC, amountupto ASC", DataViewRowState.CurrentRows).ToTable
    '        'Sohail (21 Nov 2011) -- End

    '        For Each drItem In mdtTaxSlab.Rows
    '            If CStr(IIf(IsDBNull(drItem.Item("AUD")), "A", drItem.Item("AUD").ToString)) <> "D" Then
    '                lvItems = New ListViewItem
    '                lvItems.Text = drItem.Item("tranheadunkid").ToString
    '                lvItems.Tag = drItem.Item("tranheadtaxslabtranunkid").ToString

    '                lvItems.SubItems.Add(Format(drItem.Item("amountupto"), GUI.fmtCurrency)) 'Sohail (11 May 2011)
    '                lvItems.SubItems.Add(Format(drItem.Item("fixedamount"), GUI.fmtCurrency)) 'Sohail (11 May 2011)
    '                lvItems.SubItems.Add(Format(drItem.Item("ratepayable"), GUI.fmtCurrency)) 'Sohail (11 May 2011)
    '                lvItems.SubItems.Add(Format(drItem.Item("inexcessof"), GUI.fmtCurrency)) 'Sohail (11 May 2011)
    '                lvItems.SubItems.Add(drItem.Item("GUID").ToString)
    '                'Sohail (21 Nov 2011) -- Start
    '                lvItems.SubItems.Add(drItem.Item("period_name").ToString)
    '                lvItems.SubItems(colhTaxPeriod.Index).Tag = CInt(drItem.Item("periodunkid"))
    '                lvItems.SubItems.Add(drItem.Item("end_date").ToString)
    '                'Sohail (21 Nov 2011) -- End
    '                lvTrnHeadSlabInExcessof.Items.Add(lvItems)
    '            End If
    '        Next
    '        'Sohail (21 Nov 2011) -- Start
    '        lvTrnHeadSlabInExcessof.GroupingColumn = colhTaxPeriod
    '        lvTrnHeadSlabInExcessof.DisplayGroups(True)
    '        lvTrnHeadSlabInExcessof.GridLines = False

    '        If lvTrnHeadSlabInExcessof.Items.Count > 3 Then
    '            colhTaxRatePayable.Width = 105 - 18
    '        Else
    '            colhTaxRatePayable.Width = 105
    '        End If
    '        'Sohail (21 Nov 2011) -- End
    '    Catch ex As Exception
    '        DisplayError.Show("-1", ex.Message, "FillAccessOfList", mstrModuleName)
    '    End Try
    'End Sub

    'Private Sub ResetSimpleSlab()
    '    Try
    '        txtSimpleSlabAmtUpto.Text = ""
    '        cboSimpleSlabAmountType.SelectedValue = 0
    '        txtSimpleSlabValueBasis.Text = ""
    '        btnAdd.Enabled = True
    '        cboSimpleSlabPeriod.Enabled = True 'Sohail (21 Nov 2011)
    '        'Sohail (24 Sep 2013) -- Start
    '        'TRA - ENHANCEMENT
    '        txtSlabFormula.Text = ""
    '        txtSlabFormula.Tag = ""
    '        cboSimpleSlabAmountType.Enabled = True
    '        'Sohail (24 Sep 2013) -- End
    '    Catch ex As Exception
    '        DisplayError.Show("-1", ex.Message, "ResetSimpleSlab", mstrModuleName)
    '    End Try
    'End Sub

    'Private Sub ResetTaxSlab()
    '    Try
    '        txtTaxSlabAmtUpto.Text = ""
    '        txtTaxSlabFixedAmount.Text = ""
    '        txtTaxSlabRatePayable.Text = ""
    '        btnAdd.Enabled = True
    '        cboTaxSlabPeriod.Enabled = True 'Sohail (21 Nov 2011)
    '    Catch ex As Exception
    '        DisplayError.Show("-1", ex.Message, "ResetTaxSlab", mstrModuleName)
    '    End Try
    'End Sub
#End Region

#Region " Private Functions "

    Private Function IsValidInput() As Boolean
        Dim strMsg As String = ""
        Try
            If txtHeadCode.Text.Trim = "" Then
                strMsg = Language.getMessage(mstrModuleName, 1, "Sorry, Transaction Head Code can not be blank. Transaction Head is a compulsory information.")
                eZeeMsgBox.Show(strMsg, enMsgBoxStyle.Information)
                txtHeadCode.Focus()
                Return False
            ElseIf txtHeadName.Text.Trim = "" Then
                strMsg = Language.getMessage(mstrModuleName, 2, "Sorry, Transaction Head Name can not be blank. Transaction Head is a compulsory information.")
                eZeeMsgBox.Show(strMsg, enMsgBoxStyle.Information)
                txtHeadName.Focus()
                Return False
            End If
            Return True
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "IsValidInput", mstrModuleName)
        End Try
    End Function

    Private Function IsValidFormula() As Boolean
        Dim intFunction As Integer = CInt(cboFunction.SelectedValue)
        Dim intLastFunction As Integer = -1
        Dim intCntOpenBracket As Integer = 0
        Dim intCntCloseBracket As Integer = 0
        Dim dt As DataTable

        Try

            If intFunction <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 11, "Please select an appropriate function."), enMsgBoxStyle.Information)
                cboFunction.Focus()
                Exit Function

            ElseIf (intFunction = enFunction.Transaction_Head OrElse intFunction = enFunction.CUMULATIVE) AndAlso CInt(cboTrnHead.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 12, "Please select an appropriate Transaction Head."), enMsgBoxStyle.Information)
                cboTrnHead.Focus()
                Exit Function

            ElseIf intFunction = enFunction.CONSTANT_VALUE AndAlso txtConstantValue.Decimal = 0 Then
                If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 13, "Are you sure you want to enter ZERO as Constant Value"), CType(enMsgBoxStyle.Information + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.No Then
                    txtConstantValue.Focus()
                    Exit Function
                End If

            ElseIf mdtFormula.Select("effectivedate = '" & eZeeDate.convertDate(dtpCumulativeStartDate.Value.Date) & "' AND AUD <> 'D' ").Length = 0 Then
                Select Case CInt(cboFunction.SelectedValue)
                    Case enFunction.Plus, enFunction.Minus, enFunction.Multiply, enFunction.Divide, enFunction.Close_Bracket
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Function must begin with Transaction Head/Basic Salary/Constant Value."), enMsgBoxStyle.Information)
                        cboFunction.Focus()
                        Exit Function
                End Select
            End If


            dt = New DataView(dtTable, "effectivedate = '" & eZeeDate.convertDate(dtpCumulativeStartDate.Value.Date) & "' AND AUD <> 'D' ", "", DataViewRowState.CurrentRows).ToTable
            If dt.Rows.Count > 0 Then
                intLastFunction = CInt(dt.Rows(dt.Rows.Count - 1).Item("functionid"))
            End If

            If intLastFunction = enFunction.Open_Bracket And intFunction = enFunction.Close_Bracket Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Invalid Close Bracket."), enMsgBoxStyle.Information)
                cboFunction.Focus()
                Exit Function
            ElseIf intLastFunction = enFunction.Close_Bracket And intFunction = enFunction.Open_Bracket Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "Invalid Open Bracket."), enMsgBoxStyle.Information)
                cboFunction.Focus()
                Exit Function
            End If

            If intFunction = enFunction.Close_Bracket Then
                For Each dtRow As DataRow In dt.Rows
                    Select Case dtRow.Item("headid").ToString
                        Case "("
                            intCntOpenBracket += 1
                        Case ")"
                            intCntCloseBracket += 1
                    End Select
                Next

                If intCntOpenBracket <= intCntCloseBracket Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Invalid Close Bracket."), enMsgBoxStyle.Information)
                    cboFunction.Focus()
                    Exit Function
                End If
            End If

            Select Case intLastFunction
                Case enFunction.Transaction_Head, enFunction.Close_Bracket, enFunction.CONSTANT_VALUE, enFunction.BASIC_SALARY
                    Select Case intFunction
                        Case enFunction.Transaction_Head, enFunction.Open_Bracket, enFunction.CONSTANT_VALUE, enFunction.BASIC_SALARY
                            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 6, "Invalid Function selected."), enMsgBoxStyle.Information)
                            cboFunction.Focus()
                            Exit Function
                    End Select
                Case enFunction.Plus, enFunction.Minus, enFunction.Multiply, enFunction.Divide, enFunction.Open_Bracket
                    Select Case intFunction
                        Case enFunction.Plus, enFunction.Minus, enFunction.Multiply, enFunction.Divide, enFunction.Close_Bracket
                            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 6, "Invalid Function selected."), enMsgBoxStyle.Information)
                            cboFunction.Focus()
                            Exit Function
                    End Select
            End Select

            Return True
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "IsValidFormula", mstrModuleName)
        End Try
    End Function

    Private Sub FillList()
        Dim lvItem As ListViewItem
        Try
            lvFormula.Items.Clear()

            dtTable = New DataView(dtTable, "AUD <> 'D' ", "effectivedate DESC", DataViewRowState.CurrentRows).ToTable
            For Each dtRow As DataRow In dtTable.Rows

                lvItem = New ListViewItem
                lvItem.Text = dtRow.Item("function").ToString
                lvItem.Tag = dtRow.Item("functionid").ToString
                lvItem.SubItems.Add(dtRow.Item("head").ToString)
                lvItem.SubItems(colhFormula.Index).Tag = dtRow.Item("headid").ToString
                lvItem.SubItems.Add(dtRow.Item("default").ToString)
                lvItem.SubItems(colhDefaultValue.Index).Tag = CInt(dtRow.Item("defaultid"))
                lvItem.SubItems.Add(eZeeDate.convertDate(dtRow.Item("effectivedate").ToString).ToShortDateString)
                lvFormula.Items.Add(lvItem)
            Next

            lvFormula.GroupingColumn = colhEndDate
            lvFormula.DisplayGroups(True)

            If lvFormula.Items.Count > 3 Then
                colhFormula.Width = 220 - 18
            Else
                colhFormula.Width = 220
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillList", mstrModuleName)
        End Try
    End Sub

    Private Function DefineFormula() As Boolean 'To stop adding item in list while error occures
        Dim strFormulaId As String = ""
        Dim strFormula As String = ""
        Dim strPrevEndDate As String = ""

        Try

            'Dim dtTable As DataTable = mdtFormula

            For i As Integer = 0 To lvFormula.Items.Count - 1

                If strPrevEndDate <> eZeeDate.convertDate(CDate(lvFormula.Items(i).SubItems(colhEndDate.Index).Text)).ToString() Then

                    If strPrevEndDate <> "" Then
                        Dim dRow() As DataRow
                        Dim dtRow As DataRow
                        dRow = mdtFormula.Select("effectivedate = '" & strPrevEndDate & "' AND AUD <> 'D' ")

                        If dRow.Length <= 0 Then
                            dtRow = mdtFormula.NewRow
                            dtRow.Item("effectivedate") = strPrevEndDate
                            dtRow.Item("functionid") = strFormulaId
                            dtRow.Item("function") = strFormula
                            dtRow.Item("defaultid") = 0
                            dtRow.Item("default") = String.Empty
                            dtRow.Item("AUD") = "A"
                            mdtFormula.Rows.Add(dtRow)
                        Else
                            For Each dr As DataRow In dRow
                                dr.Item("functionid") = strFormulaId
                                dr.Item("function") = strFormula
                                If dr.Item("AUD").ToString <> "A" Then
                                    dr.Item("AUD") = "U"
                                End If
                            Next
                            mdtFormula.AcceptChanges()
                        End If
                    End If
                    strFormulaId = ""
                    strFormula = ""
                End If

                Select Case CInt(lvFormula.Items(i).Tag)

                    Case enFunction.BASIC_SALARY, enFunction.Transaction_Head

                        strFormulaId = strFormulaId & "#" & lvFormula.Items(i).SubItems(colhFormula.Index).Tag.ToString & "#"
                        strFormula = strFormula & lvFormula.Items(i).SubItems(colhFormula.Index).Text

                        'For removing space around brackets
                    Case enFunction.Open_Bracket, enFunction.Close_Bracket
                        strFormulaId = strFormulaId & lvFormula.Items(i).SubItems(colhFormula.Index).Tag.ToString
                        strFormula = strFormula & lvFormula.Items(i).SubItems(colhFormula.Index).Text

                    Case enFunction.CONSTANT_VALUE
                        strFormulaId = strFormulaId & lvFormula.Items(i).SubItems(colhFormula.Index).Text
                        strFormula = strFormula & lvFormula.Items(i).SubItems(colhFormula.Index).Text

                    Case Else
                        strFormulaId = strFormulaId & lvFormula.Items(i).SubItems(colhFormula.Index).Tag.ToString
                        strFormula = strFormula & " " & lvFormula.Items(i).SubItems(colhFormula.Index).Text & " "
                End Select

                If i = lvFormula.Items.Count - 1 Then 'If Last List View Item
                    Dim dRow() As DataRow
                    Dim dtRow As DataRow
                    dRow = mdtFormula.Select("effectivedate = '" & eZeeDate.convertDate(CDate(lvFormula.Items(i).SubItems(colhEndDate.Index).Text)).ToString() & "' AND AUD <> 'D' ")
                    If dRow.Length <= 0 Then
                        dtRow = mdtFormula.NewRow
                        dtRow.Item("effectivedate") = eZeeDate.convertDate(CDate(lvFormula.Items(i).SubItems(colhEndDate.Index).Text)).ToString()
                        dtRow.Item("functionid") = strFormulaId
                        dtRow.Item("function") = strFormula
                        dtRow.Item("defaultid") = 0
                        dtRow.Item("default") = String.Empty
                        dtRow.Item("AUD") = "A"
                        mdtFormula.Rows.Add(dtRow)
                    Else
                        For Each dr As DataRow In dRow
                            dr.Item("functionid") = strFormulaId
                            dr.Item("function") = strFormula
                            If dr.Item("AUD").ToString <> "A" Then
                                dr.Item("AUD") = "U"
                            End If
                        Next
                        mdtFormula.AcceptChanges()
                    End If
                End If
                strPrevEndDate = eZeeDate.convertDate(CDate(lvFormula.Items(i).SubItems(colhEndDate.Index).Text)).ToString()
            Next


            If lvFormula.Items.Count > 0 Then
                Dim dRow() As DataRow
                dRow = mdtFormula.Select("effectivedate = '" & eZeeDate.convertDate(CDate(lvFormula.Items(0).SubItems(colhEndDate.Index).Text).Date) & "' AND AUD <> 'D' ")
                If dRow.Length > 0 Then
                    txtFormula.Tag = dRow(0)("functionid").ToString()
                    txtFormula.Text = dRow(0)("function").ToString()
                Else
                    txtFormula.Tag = ""
                    txtFormula.Text = ""
                End If
            Else
                txtFormula.Tag = ""
                txtFormula.Text = ""
            End If

            'mdtFormula = dtTable

            Return True
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "DefineFormula", mstrModuleName)
        End Try
    End Function

    'Private Function IsValidSimpleSlab(ByVal ctrlName As String) As Boolean
    '    Try
    '        If CInt(cboComputeOn.SelectedValue) = enComputeOn.ComputeOnSpecifiedFormula And txtFormula.Text = "" Then
    '            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 8, "Please enter an appropriate formula."), enMsgBoxStyle.Information)
    '            txtFormula.Focus() 'Sohail (24 Sep 2013)
    '            Exit Function
    '            'Sohail (03 Sep 2010) -- Start
    '            'ElseIf txtSimpleSlabAmtUpto.Text = "" Then
    '            '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 10, "Sorry, Amount upto can not be Zero."), enMsgBoxStyle.Information)
    '            '    txtSimpleSlabAmtUpto.Focus()
    '            '    Exit Function
    '            'Sohail (03 Sep 2010) -- End
    '            'Sohail (16 Oct 2010) -- Start
    '            'ElseIf txtSimpleSlabAmtUpto.Decimal = 0 Then 'Sohail (03 Sep 2010)
    '        ElseIf txtSimpleSlabAmtUpto.Decimal < 0 Then 'Sohail (11 May 2011)
    '            'eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 10, "Sorry, Amount upto can not be Zero."), enMsgBoxStyle.Information)
    '            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 10, "Sorry, Amount upto cannot be less than Zero."), enMsgBoxStyle.Information)
    '            'Sohail (16 Oct 2010) -- End
    '            txtSimpleSlabAmtUpto.Focus()
    '            Exit Function
    '        ElseIf CInt(cboSimpleSlabAmountType.SelectedValue) <= 0 Then
    '            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 11, "Please select Slab Type."), enMsgBoxStyle.Information)
    '            cboSimpleSlabAmountType.Focus()
    '            Exit Function
    '            'Sohail (16 Oct 2010) -- Start
    '            'ElseIf txtSimpleSlabValueBasis.Decimal = 0 Then 'Sohail (03 Sep 2010)
    '        ElseIf CInt(cboSimpleSlabAmountType.SelectedValue) = enPaymentBy.Value AndAlso txtSimpleSlabValueBasis.Decimal < 0 Then 'Sohail (11 May 2011)
    '            'eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 12, "Sorry, Value Basis can not be Zero."), enMsgBoxStyle.Information)
    '            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 12, "Sorry, Value Basis cannot be less then Zero."), enMsgBoxStyle.Information)
    '            'Sohail (16 Oct 2010) -- End
    '            txtSimpleSlabValueBasis.Focus()
    '            Exit Function
    '        ElseIf CInt(cboSimpleSlabAmountType.SelectedValue) = enPaymentBy.Percentage Then
    '            If txtSimpleSlabValueBasis.Decimal < 0 Or txtSimpleSlabValueBasis.Decimal > 100 Then 'Sohail (03 Sep 2010)
    '                'Allow percentage above 100 for over time calculation (200% of hourly salary)
    '                If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 13, "Sorry, Percentage should be in between 1 to 100.") & vbCr & vbCr & Language.getMessage(mstrModuleName, 20, "Do you want to proceed?"), CType(enMsgBoxStyle.Question + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.None Then
    '                    txtSimpleSlabValueBasis.Focus()
    '                    Exit Function
    '                End If
    '            End If
    '            'Sohail (24 Sep 2013) -- Start
    '            'TRA - ENHANCEMENT
    '        ElseIf CInt(cboSimpleSlabAmountType.SelectedValue) = enPaymentBy.Fromula AndAlso CInt(cboComputeOn.SelectedValue) <> enComputeOn.ComputeOnSpecifiedFormula Then
    '            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 7, "Please select Compute On."), enMsgBoxStyle.Information)
    '            cboComputeOn.Focus()
    '            Exit Function
    '        ElseIf ctrlName <> objbtnAddSlabFormula.Name AndAlso CInt(cboSimpleSlabAmountType.SelectedValue) = enPaymentBy.Fromula AndAlso txtSlabFormula.Text.Trim = "" Then
    '            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 8, "Please enter an appropriate formula."), enMsgBoxStyle.Information)
    '            txtSlabFormula.Focus()
    '            Exit Function
    '            'Sohail (24 Sep 2013) -- End
    '        End If
    '        'Sohail (21 Nov 2011) -- Start
    '        If cboSimpleSlabPeriod.SelectedValue Is Nothing Then
    '            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 15, "Sorry! This Period is closed."), enMsgBoxStyle.Information)
    '            Exit Function
    '        ElseIf CInt(cboSimpleSlabPeriod.SelectedValue) <= 0 Then
    '            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 6, "Please select Period."), enMsgBoxStyle.Information)
    '            cboSimpleSlabPeriod.Focus()
    '            Exit Function
    '        ElseIf mintPeriod_Status = enStatusType.Close Then
    '            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 15, "Sorry! This Period is closed."), enMsgBoxStyle.Information)
    '            cboSimpleSlabPeriod.Focus()
    '            Exit Function
    '        End If
    '        'Sohail (21 Nov 2011) -- End

    '        Return True
    '    Catch ex As Exception
    '        DisplayError.Show("-1", ex.Message, "IsValidSimpleSlab", mstrModuleName)
    '    End Try
    'End Function

    'Private Function IsValidTaxSlab() As Boolean
    '    Try
    '        If CInt(cboComputeOn.SelectedValue) = enComputeOn.ComputeOnSpecifiedFormula And txtFormula.Text = "" Then
    '            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 8, "Please enter an appropriate formula."), enMsgBoxStyle.Information)
    '            Exit Function
    '            'Sohail (03 Sep 2010) -- Start
    '            'ElseIf txtTaxSlabAmtUpto.Text = "" Then
    '            '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 10, "Sorry, Amount upto can not be Zero."), enMsgBoxStyle.Information)
    '            '    txtTaxSlabAmtUpto.Focus()
    '            '    Exit Function
    '            'Sohail (03 Sep 2010) -- End
    '            'Sohail (16 Oct 2010) -- Start
    '            'ElseIf txtSimpleSlabAmtUpto.Decimal = 0 Then 'Sohail (03 Sep 2010)
    '        ElseIf txtSimpleSlabAmtUpto.Decimal < 0 Then 'Sohail (11 May 2011)
    '            'eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 10, "Sorry, Amount upto can not be Zero."), enMsgBoxStyle.Information)
    '            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 10, "Sorry, Amount upto cannot be less than Zero."), enMsgBoxStyle.Information)
    '            txtTaxSlabAmtUpto.Focus()
    '            Exit Function
    '            'Sohail (03 Sep 2010) -- Start
    '            'ElseIf txtTaxSlabFixedAmount.Text = "" Then
    '            '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 15, "Sorry, Fixed Amount can not be Zero."), enMsgBoxStyle.Information)
    '            '    txtTaxSlabFixedAmount.Focus()
    '            '    Exit Function
    '            'Sohail (03 Sep 2010) -- End
    '            'ElseIf cdec(txtTaxSlabFixedAmount.Text) = 0 Then
    '            '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 15, "Sorry, Fixed Amount can not be Zero."), enMsgBoxStyle.Information)
    '            '    txtTaxSlabFixedAmount.Focus()
    '            '    Exit Function
    '            'Sohail (03 Sep 2010) -- Start
    '            'ElseIf txtTaxSlabRatePayable.Text = "" Then
    '            '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 16, "Sorry, Rate Payable can not be Zero."), enMsgBoxStyle.Information)
    '            '    txtTaxSlabRatePayable.Focus()
    '            '    Exit Function
    '            'Sohail (03 Sep 2010) -- End
    '        ElseIf txtTaxSlabRatePayable.Decimal < 0 Or txtTaxSlabRatePayable.Decimal > 100 Then 'Sohail (03 Sep 2010)
    '            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 13, "Sorry, Percentage should be in between 1 to 100."), enMsgBoxStyle.Information)
    '            txtTaxSlabRatePayable.Focus()
    '            Exit Function
    '        End If
    '        'Sohail (21 Nov 2011) -- Start
    '        If cboTaxSlabPeriod.SelectedValue Is Nothing Then
    '            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 15, "Sorry! This Period is closed."), enMsgBoxStyle.Information)
    '            Exit Function
    '        ElseIf CInt(cboTaxSlabPeriod.SelectedValue) <= 0 Then
    '            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 6, "Please select Period."), enMsgBoxStyle.Information)
    '            cboTaxSlabPeriod.Focus()
    '            Exit Function
    '        ElseIf mintPeriod_Status = enStatusType.Close Then
    '            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 15, "Sorry! This Period is closed."), enMsgBoxStyle.Information)
    '            cboTaxSlabPeriod.Focus()
    '            Exit Function
    '        End If
    '        'Sohail (21 Nov 2011) -- End

    '        Return True
    '    Catch ex As Exception
    '        DisplayError.Show("-1", ex.Message, "IsValidTaxSlab", mstrModuleName)
    '    End Try
    'End Function
#End Region

#Region " Form's Events "

    Private Sub frmBudgetFormula_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        Try
            objBudgetformula = Nothing
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmBudgetFormula_FormClosed", mstrModuleName)
        End Try
    End Sub

    Private Sub frmBudgetFormula_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown
        Try
            Select Case e.KeyCode
                Case Keys.Return
                    SendKeys.Send("{TAB}")
                Case Keys.S
                    If e.Control = True Then
                        Call btnSave.PerformClick()
                    End If
            End Select
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmBudgetFormula_KeyDown", mstrModuleName)
        End Try
    End Sub

    Private Sub frmBudgetFormula_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        objBudgetformula = New clsBudgetformula_master
        Try
            Call Set_Logo(Me, gApplicationType)
            Call Language.setLanguage(Me.Name)
            Language.setLanguage(Me.Name)
            Call OtherSettings()
            Call setColor()
            Call FillCombo()

            'dtpCumulativeStartDate.MinDate = FinancialYear._Object._Database_Start_Date.Date
            'dtpCumulativeStartDate.MaxDate = FinancialYear._Object._Database_End_Date.Date

            dtTable = objBudgetformula._FormulaTran
            If menAction = enAction.EDIT_ONE Then
                objBudgetformula._Budgetformulaunkid = mintFormulaTranHeadUnkid
            End If

            mdtFormula = objBudgetformula._FormulaTran
            mdtFormula_Tranhead = objBudgetformula._Formula_Tranhead
            Call GetValue()

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmBudgetFormula_Load", mstrModuleName)
        End Try

    End Sub

    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsBudgetformula_master.SetMessages()
            clsBudgetformula_Tran.SetMessages()
            clsBudgetformula_head_tran.SetMessages()
            objfrm._Other_ModuleNames = "clsBudgetformula_master,clsBudgetformula_Tran,clsBudgetformula_head_tran"
            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub

#End Region

#Region " Button's Events "

    Private Sub btnAdd_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAdd.Click
        Dim objPeriod As New clscommom_period_Tran
        Dim dsRow As DataRow = Nothing
        Try


            If IsValidInput() = False Then Exit Sub

            If CInt(cboFunction.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 17, "Function is compulsory information.Please select function."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                cboFunction.Select()
                Exit Sub
            End If

            If IsValidFormula() = False Then Exit Sub

            dtpCumulativeStartDate.Enabled = False

            dsRow = dtTable.NewRow

            dsRow.Item("effectivedate") = eZeeDate.convertDate(dtpCumulativeStartDate.Value.Date)
            dsRow.Item("functionid") = CInt(cboFunction.SelectedValue)
            dsRow.Item("function") = cboFunction.Text

            If CInt(cboFunction.SelectedValue) = enFunction.Open_Bracket Then
                dsRow.Item("headid") = "("
                dsRow.Item("head") = "("
            ElseIf CInt(cboFunction.SelectedValue) = enFunction.Close_Bracket Then
                dsRow.Item("headid") = ")"
                dsRow.Item("head") = ")"
            ElseIf CInt(cboFunction.SelectedValue) = enFunction.Plus Then
                dsRow.Item("headid") = "+"
                dsRow.Item("head") = "+"
            ElseIf CInt(cboFunction.SelectedValue) = enFunction.Minus Then
                dsRow.Item("headid") = "-"
                dsRow.Item("head") = "-"
            ElseIf CInt(cboFunction.SelectedValue) = enFunction.Multiply Then
                dsRow.Item("headid") = "*"
                dsRow.Item("head") = "*"
            ElseIf CInt(cboFunction.SelectedValue) = enFunction.Divide Then
                dsRow.Item("headid") = "/"
                dsRow.Item("head") = "/"

            ElseIf CInt(cboFunction.SelectedValue) = enFunction.BASIC_SALARY AndAlso CInt(cboSalaryHead.SelectedValue) <= 0 Then
                dsRow.Item("headid") = "BSL" 'Basic Salary
                dsRow.Item("head") = cboFunction.Text

            ElseIf CInt(cboFunction.SelectedValue) = enFunction.CONSTANT_VALUE Then
                dsRow.Item("headid") = "CV" 'Constant Value
                dsRow.Item("head") = txtConstantValue.Decimal.ToString

            ElseIf CInt(cboFunction.SelectedValue) = enFunction.BASIC_SALARY AndAlso CInt(cboSalaryHead.SelectedValue) > 0 Then
                dsRow.Item("headid") = "BSL|" & cboSalaryHead.SelectedValue.ToString
                dsRow.Item("head") = cboSalaryHead.Text

                'Dim dtRow As DataRow = mdtFormula_Tranhead.NewRow
                'dtRow.Item("effectivedate") = eZeeDate.convertDate(dtpCumulativeStartDate.Value.Date)
                'dtRow.Item("tranheadunkid") = CInt(cboSalaryHead.SelectedValue)
                'dtRow.Item("defaultid") = CInt(cboDefaultValue.SelectedValue)
                'dtRow.Item("default") = cboDefaultValue.Text
                'dtRow.Item("userunkid") = User._Object._Userunkid
                'dtRow.Item("isvoid") = False
                'dtRow.Item("voiduserunkid") = -1
                'dtRow.Item("voiddatetime") = DBNull.Value
                'dtRow.Item("voidreason") = ""
                'dtRow.Item("AUD") = "A"
                'mdtFormula_Tranhead.Rows.Add(dtRow)

            Else
                dsRow.Item("headid") = cboTrnHead.SelectedValue
                dsRow.Item("head") = cboTrnHead.Text

                Dim dtRow As DataRow = mdtFormula_Tranhead.NewRow
                dtRow.Item("effectivedate") = eZeeDate.convertDate(dtpCumulativeStartDate.Value.Date)
                dtRow.Item("tranheadunkid") = CInt(cboTrnHead.SelectedValue)
                dtRow.Item("defaultid") = CInt(cboDefaultValue.SelectedValue)
                dtRow.Item("default") = cboDefaultValue.Text
                dtRow.Item("userunkid") = User._Object._Userunkid
                dtRow.Item("isvoid") = False
                dtRow.Item("voiduserunkid") = -1
                dtRow.Item("voiddatetime") = DBNull.Value
                dtRow.Item("voidreason") = ""
                dtRow.Item("AUD") = "A"
                mdtFormula_Tranhead.Rows.Add(dtRow)
            End If

            If cboDefaultValue.Visible = True Then
                dsRow.Item("defaultid") = CInt(cboDefaultValue.SelectedValue)
                dsRow.Item("default") = cboDefaultValue.Text
            Else
                dsRow.Item("defaultid") = 0
                dsRow.Item("default") = String.Empty
            End If

            dsRow.Item("AUD") = "A"
            dtTable.Rows.Add(dsRow)

            Call FillList()
            If DefineFormula() = False Then
                lvFormula.Items.RemoveAt(lvFormula.Items.Count - 1)
                Exit Sub
            End If

            Call ResetControls()

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnAdd_Click", mstrModuleName)
        Finally
            objPeriod = Nothing
        End Try
    End Sub

    Private Sub btnDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDelete.Click
        Try
            If lvFormula.SelectedItems.Count < 1 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 16, "Please select formula from the list to perform further operation on it."), enMsgBoxStyle.Information) '?1
                lvFormula.Select()
                Exit Sub
            End If

            Dim dt As DataTable = New DataView(mdtFormula, "effectivedate = '" & eZeeDate.convertDate(dtpCumulativeStartDate.Value.Date) & "' AND AUD <> 'D' ", "", DataViewRowState.CurrentRows).ToTable

            If dt.Rows.Count > 0 Then
                If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 10, "Are you sure you want to delete the Whole Formula for selected date?"), CType(enMsgBoxStyle.Question + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.Yes Then
                    lvFormula.Items.Clear()
                    txtFormula.Clear()
                    For Each dtRow As DataRow In mdtFormula.Rows
                        If dtRow.Item("effectivedate").ToString() = eZeeDate.convertDate(dtpCumulativeStartDate.Value.Date).ToString() Then
                            dtRow.Item("AUD") = "D"
                        End If
                    Next
                    mdtFormula.AcceptChanges()
                End If

                If mdtFormula_Tranhead IsNot Nothing Then
                    For Each dtRow As DataRow In mdtFormula_Tranhead.Rows
                        If dtRow.Item("effectivedate").ToString() = eZeeDate.convertDate(dtpCumulativeStartDate.Value.Date).ToString() Then
                            dtRow.Item("AUD") = "D"
                        End If
                    Next
                    mdtFormula_Tranhead.AcceptChanges()
                End If

                If dtTable IsNot Nothing Then
                    For Each dtRow As DataRow In dtTable.Rows
                        If dtRow.Item("effectivedate").ToString() = eZeeDate.convertDate(dtpCumulativeStartDate.Value.Date).ToString() Then
                            dtRow.Item("AUD") = "D"
                        End If
                    Next
                    dtTable.AcceptChanges()
                End If

            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnDelete_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Dim blnFlag As Boolean = False
        Dim intFunction As Integer = CInt(cboFunction.SelectedValue)
        Dim intLastFunction As Integer = -1
        Dim intCntOpenBracket As Integer = 0
        Dim intCntCloseBracket As Integer = 0
        Dim dt As DataTable
        Try
            If IsValidInput() = False Then Exit Sub

            If lvFormula.Items.Count = 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 7, "Please select atleast one function."), enMsgBoxStyle.Information)
                cboFunction.Focus()
                Exit Sub
            End If

            If lvFormula.Items.Count > 0 Then
                For Each dsRow As DataRow In dtTable.Rows
                    dt = New DataView(dtTable, "effectivedate = '" & eZeeDate.convertDate(dtpCumulativeStartDate.Value.Date) & "' AND AUD <> 'D' ", "", DataViewRowState.CurrentRows).ToTable

                    If dt.Rows.Count > 0 Then
                        intLastFunction = CInt(dt.Rows(dt.Rows.Count - 1).Item("functionid"))
                    Else
                        intLastFunction = -1
                    End If

                    Select Case intLastFunction
                        Case enFunction.Plus, enFunction.Minus, enFunction.Multiply, enFunction.Divide, enFunction.Open_Bracket
                            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 8, "Sorry, Formula is incomplete."), enMsgBoxStyle.Information)
                            cboFunction.Focus()
                            Exit Sub
                    End Select

                    intCntOpenBracket = 0
                    intCntCloseBracket = 0

                    For Each dtRow As DataRow In dt.Rows
                        Select Case dtRow.Item("headid").ToString
                            Case "("
                                intCntOpenBracket += 1
                            Case ")"
                                intCntCloseBracket += 1
                        End Select
                    Next

                    If intCntOpenBracket <> intCntCloseBracket Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 9, "Unclosed Brackets. Please Add Brackets."), enMsgBoxStyle.Information)
                        cboFunction.Focus()
                        Exit Sub
                    End If

                Next
            End If

            Call SetValue()

            'S.SANDEEP [28-May-2018] -- START
            'ISSUE/ENHANCEMENT : {Audit Trails} 
            objBudgetformula._FormName = mstrModuleName
            objBudgetformula._LoginEmployeeunkid = 0
            objBudgetformula._ClientIP = getIP()
            objBudgetformula._HostName = getHostName()
            objBudgetformula._FromWeb = False
            objBudgetformula._AuditUserId = User._Object._Userunkid
objBudgetformula._CompanyUnkid = Company._Object._Companyunkid
            objBudgetformula._AuditDate = ConfigParameter._Object._CurrentDateAndTime
            'S.SANDEEP [28-May-2018] -- END

            If menAction = enAction.ADD_CONTINUE OrElse menAction = enAction.ADD_ONE Then
                blnFlag = objBudgetformula.Insert()
            Else
                blnFlag = objBudgetformula.Update()
            End If

            If blnFlag = False And objBudgetformula._Message <> "" Then
                eZeeMsgBox.Show(objBudgetformula._Message, enMsgBoxStyle.Information)
            End If

            If blnFlag Then
                mblnCancel = False
                If menAction = enAction.ADD_CONTINUE Then
                    objBudgetformula = Nothing
                    objBudgetformula = New clsBudgetformula_master
                    mdtFormula = objBudgetformula._FormulaTran
                    mdtFormula_Tranhead = objBudgetformula._Formula_Tranhead
                    dtpCumulativeStartDate.Enabled = True
                    dtpCumulativeStartDate.Value = ConfigParameter._Object._CurrentDateAndTime.Date
                    FillCombo()
                    dtTable.Rows.Clear()
                    Call GetValue()
                    txtHeadCode.Focus()
                Else
                    Me.Close()
                End If
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnSave_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnClose_Click", mstrModuleName)
        End Try
    End Sub

    'Nilay (26-Aug-2016) -- Start
    'For Other Language ADD Name1, Name2 
    Private Sub objbtnOtherLanguage_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnOtherLanguage.Click
        Dim objFrm As New NameLanguagePopup_Form
        Try
            If User._Object._Isrighttoleft = True Then
                objFrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objFrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objFrm)
            End If

            Call objFrm.displayDialog(txtHeadName.Text, objBudgetformula._Formula_Name1, objBudgetformula._Formula_Name2)

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnOtherLanguage_Click", mstrModuleName)
        End Try
    End Sub
    'Nilay (26-Aug-2016) -- END

#End Region

#Region " ComboBox's Events "

    Private Sub cboFunction_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboFunction.SelectedIndexChanged
        Try
            cboTrnHead.SelectedValue = 0
            cboTrnHead.Visible = True
            cboSalaryHead.SelectedValue = 0
            cboSalaryHead.Visible = False
            txtConstantValue.Visible = False
            objbtnSearchTranHead.Enabled = False
            cboDefaultValue.SelectedValue = 0
            cboDefaultValue.Visible = False
            lblDefaultValue.Visible = False

            Select Case CInt(cboFunction.SelectedValue)
                Case enFunction.Transaction_Head
                    cboTrnHead.Enabled = True
                    'cboDefaultValue.Visible = True
                    'lblDefaultValue.Visible = True
                    lblTrnHead_Leave.Text = Language.getMessage(mstrModuleName, 14, "Budget Head")
                    cboTrnHead.BringToFront()
                    objbtnSearchTranHead.Enabled = True
                    objbtnSearchTranHead.Visible = True

                Case enFunction.CONSTANT_VALUE
                    cboTrnHead.Visible = False
                    cboSalaryHead.Visible = False
                    txtConstantValue.Visible = True
                    lblTrnHead_Leave.Text = Language.getMessage(mstrModuleName, 15, "Constant Value")
                    txtConstantValue.BringToFront()
                    objbtnSearchTranHead.Visible = False

                Case enFunction.BASIC_SALARY
                    cboSalaryHead.Visible = True
                    lblTrnHead_Leave.Text = Language.getMessage(mstrModuleName, 14, "Budget Head")
                    cboSalaryHead.BringToFront()
                    'cboDefaultValue.Visible = True
                    'lblDefaultValue.Visible = True
                    objbtnSearchTranHead.Enabled = False
                    objbtnSearchTranHead.Visible = False

                Case Else
                    cboTrnHead.Enabled = False
                    objbtnSearchTranHead.Enabled = False
                    lblTrnHead_Leave.Text = Language.getMessage(mstrModuleName, 14, "Budget Head")
            End Select

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboFunction_SelectedIndexChanged", mstrModuleName)
        Finally
        End Try

    End Sub

#End Region

#Region " Listview's Events "

    Private Sub lvFormula_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lvFormula.SelectedIndexChanged
        Try
            If lvFormula.SelectedItems.Count > 0 Then
                dtpCumulativeStartDate.Value = CDate(lvFormula.SelectedItems(0).SubItems(colhEndDate.Index).Text).Date
                dtpCumulativeStartDate.Enabled = False


                Dim dRow() As DataRow
                dRow = mdtFormula.Select("effectivedate = '" & eZeeDate.convertDate(CDate(lvFormula.Items(lvFormula.SelectedItems(0).Index).SubItems(colhEndDate.Index).Text).Date).ToString() & "' AND AUD <> 'D' ")
                If dRow.Length > 0 Then
                    txtFormula.Text = dRow(0).Item("head").ToString
                Else
                    txtFormula.Text = ""
                End If
            Else
                dtpCumulativeStartDate.Value = ConfigParameter._Object._CurrentDateAndTime.Date
                dtpCumulativeStartDate.Enabled = True
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lvFormula_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " TextBox's Events "

    Private Sub txtFormula_MouseMove(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtFormula.MouseMove
        Try
            ToolTip1.SetToolTip(txtFormula, txtFormula.Text)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "txtFormula_MouseMove", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Other Control's Events "

    Private Sub objbtnSearchTranHead_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchTranHead.Click
        Dim objfrm As New frmCommonSearch
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If


            With cboTrnHead
                objfrm.DataSource = CType(cboTrnHead.DataSource, DataTable) 'dtTable
                objfrm.ValueMember = .ValueMember
                objfrm.DisplayMember = .DisplayMember
                objfrm.CodeMember = "code"
                If objfrm.DisplayDialog Then
                    .SelectedValue = objfrm.SelectedValue
                End If
                .Focus()
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchEmployee_Click", mstrModuleName)
        Finally
            objfrm = Nothing
        End Try
    End Sub

    Private Sub objbtnSearchFunction_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchFunction.Click
        Dim objfrm As New frmCommonSearch
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            With cboFunction
                objfrm.DataSource = CType(.DataSource, DataTable)
                objfrm.ValueMember = .ValueMember
                objfrm.DisplayMember = .DisplayMember
                If objfrm.DisplayDialog Then
                    .SelectedValue = objfrm.SelectedValue
                End If
                .Focus()
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchFunction_Click", mstrModuleName)
        End Try
    End Sub


#End Region

#Region " Check Box's Events "

#End Region

	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Private Sub OtherSettings()
		Try
			Me.SuspendLayout()
			
			Call SetLanguage()
			
			Me.gbTrnHeadInfo.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbTrnHeadInfo.ForeColor = GUI._eZeeContainerHeaderForeColor 


			Me.btnAdd.GradientBackColor = GUI._ButttonBackColor 
			Me.btnAdd.GradientForeColor = GUI._ButttonFontColor

			Me.btnDelete.GradientBackColor = GUI._ButttonBackColor 
			Me.btnDelete.GradientForeColor = GUI._ButttonFontColor

			Me.btnSave.GradientBackColor = GUI._ButttonBackColor 
			Me.btnSave.GradientForeColor = GUI._ButttonFontColor

			Me.btnClose.GradientBackColor = GUI._ButttonBackColor 
			Me.btnClose.GradientForeColor = GUI._ButttonFontColor


			Me.ResumeLayout()
		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "OtherSettings", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetLanguage()
		Try
			Me.Text = Language._Object.getCaption(Me.Name, Me.Text)
			
			Me.gbTrnHeadInfo.Text = Language._Object.getCaption(Me.gbTrnHeadInfo.Name, Me.gbTrnHeadInfo.Text)
			Me.lblName.Text = Language._Object.getCaption(Me.lblName.Name, Me.lblName.Text)
			Me.lblCode.Text = Language._Object.getCaption(Me.lblCode.Name, Me.lblCode.Text)
			Me.lblSimpleSlabPeriod.Text = Language._Object.getCaption(Me.lblSimpleSlabPeriod.Name, Me.lblSimpleSlabPeriod.Text)
			Me.lblDefaultValue.Text = Language._Object.getCaption(Me.lblDefaultValue.Name, Me.lblDefaultValue.Text)
			Me.lblFormula.Text = Language._Object.getCaption(Me.lblFormula.Name, Me.lblFormula.Text)
			Me.colhFunction.Text = Language._Object.getCaption(CStr(Me.colhFunction.Tag), Me.colhFunction.Text)
			Me.colhFormula.Text = Language._Object.getCaption(CStr(Me.colhFormula.Tag), Me.colhFormula.Text)
			Me.colhDefaultValue.Text = Language._Object.getCaption(CStr(Me.colhDefaultValue.Tag), Me.colhDefaultValue.Text)
			Me.colhEndDate.Text = Language._Object.getCaption(CStr(Me.colhEndDate.Tag), Me.colhEndDate.Text)
			Me.btnAdd.Text = Language._Object.getCaption(Me.btnAdd.Name, Me.btnAdd.Text)
			Me.btnDelete.Text = Language._Object.getCaption(Me.btnDelete.Name, Me.btnDelete.Text)
			Me.lblFunction.Text = Language._Object.getCaption(Me.lblFunction.Name, Me.lblFunction.Text)
			Me.lblTrnHead_Leave.Text = Language._Object.getCaption(Me.lblTrnHead_Leave.Name, Me.lblTrnHead_Leave.Text)
			Me.btnSave.Text = Language._Object.getCaption(Me.btnSave.Name, Me.btnSave.Text)
			Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
			Me.lnAddComputation.Text = Language._Object.getCaption(Me.lnAddComputation.Name, Me.lnAddComputation.Text)

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetLanguage", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "Sorry, Transaction Head Code can not be blank. Transaction Head is a compulsory information.")
			Language.setMessage(mstrModuleName, 2, "Sorry, Transaction Head Name can not be blank. Transaction Head is a compulsory information.")
			Language.setMessage(mstrModuleName, 3, "Function must begin with Transaction Head/Basic Salary/Constant Value.")
			Language.setMessage(mstrModuleName, 4, "Invalid Close Bracket.")
			Language.setMessage(mstrModuleName, 5, "Invalid Open Bracket.")
			Language.setMessage(mstrModuleName, 6, "Invalid Function selected.")
			Language.setMessage(mstrModuleName, 7, "Please select atleast one function.")
			Language.setMessage(mstrModuleName, 8, "Sorry, Formula is incomplete.")
			Language.setMessage(mstrModuleName, 9, "Unclosed Brackets. Please Add Brackets.")
			Language.setMessage(mstrModuleName, 10, "Are you sure you want to delete the Whole Formula for selected date?")
			Language.setMessage(mstrModuleName, 11, "Please select an appropriate function.")
			Language.setMessage(mstrModuleName, 12, "Please select an appropriate Transaction Head.")
			Language.setMessage(mstrModuleName, 13, "Are you sure you want to enter ZERO as Constant Value")
			Language.setMessage(mstrModuleName, 14, "Budget Head")
			Language.setMessage(mstrModuleName, 15, "Constant Value")
			Language.setMessage(mstrModuleName, 16, "Please select formula from the list to perform further operation on it.")
			Language.setMessage(mstrModuleName, 17, "Function is compulsory information.Please select function.")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class