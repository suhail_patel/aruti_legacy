﻿Option Strict On
Imports eZeeCommonLib
Imports Aruti.Data

Public Class frmBudgetFormulaList

#Region " Private Variables "
    Private objBudgetMst As clsBudgetformula_master
    Private ReadOnly mstrModuleName As String = "frmBudgetFormulaList"

#End Region

#Region " Private Methods "

    Private Sub FillCombo(Optional ByVal blnRefreshList As Boolean = False)
        Dim objFormulaMst As New clsBudgetformula_master
        Dim dsCombo As DataSet
        Try
            dsCombo = objFormulaMst.getComboList("List", True)
            cboTrnHead.ValueMember = "Id"
            cboTrnHead.DisplayMember = "Name"
            cboTrnHead.DataSource = dsCombo.Tables(0)
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "FillList", mstrModuleName)
        Finally
            objFormulaMst = Nothing
        End Try
    End Sub

    Private Sub FillList()

        Dim objFormulaMst As New clsBudgetformula_master
        Dim strSearching As String = ""
        Try
            lvTrnHeadList.Items.Clear()
            If User._Object.Privilege._AllowToViewBudgetFormula = False Then Exit Sub


            Dim dsList As DataSet = objFormulaMst.GetList("List", True)

            If CInt(cboTrnHead.SelectedValue) > 0 Then
                strSearching = "AND budgetformulaunkid =" & CInt(cboTrnHead.SelectedValue) & " "
            End If

            Dim dtTranHeadList As DataTable = Nothing
            If strSearching.Length > 0 Then
                strSearching = strSearching.Substring(3)
                dtTranHeadList = New DataView(dsList.Tables("List"), strSearching, "", DataViewRowState.CurrentRows).ToTable
            Else
                dtTranHeadList = dsList.Tables("List")
            End If



            Dim lvItem As ListViewItem
            For Each dtRow As DataRow In dtTranHeadList.Rows
                lvItem = New ListViewItem
                lvItem.Text = dtRow.Item("formula_code").ToString
                lvItem.Tag = dtRow.Item("budgetformulaunkid").ToString
                lvItem.SubItems.Add(dtRow.Item("formula_name").ToString())
                lvTrnHeadList.Items.Add(lvItem)
            Next

            If lvTrnHeadList.Items.Count > 10 Then
                colhName.Width = 290 - 18
            Else
                colhName.Width = 290
            End If

        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "FillList", mstrModuleName)
        Finally
            objFormulaMst = Nothing
        End Try
    End Sub

    Private Sub SetColor()
        Try
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetColor", mstrModuleName)
        End Try
    End Sub

    Private Sub SetVisibility()
        Try
            btnNew.Enabled = User._Object.Privilege._AllowToAddBudgetFormula
            btnEdit.Enabled = User._Object.Privilege._AllowToEditBudgetFormula
            btnDelete.Enabled = User._Object.Privilege._AllowToDeleteBudgetFormula
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetVisibility", mstrModuleName)
        End Try

    End Sub

#End Region

#Region " Form's Events "

    Private Sub frmBudgetFormulaList_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs)
        Try
            Select Case e.KeyCode
                Case Keys.Return
                    SendKeys.Send("{TAB}")
                Case Keys.Escape
                    Me.Close()
                Case Keys.Delete
                    Call btnDelete.PerformClick()
            End Select
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmBudgetFormulaList_KeyDown", mstrModuleName)
        End Try
    End Sub

    Private Sub frmBudgetFormulaList_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.Load
        objBudgetMst = New clsBudgetformula_master
        Try
            Call Set_Logo(Me, gApplicationType)
            Call Language.setLanguage(Me.Name)
            Call OtherSettings()
            Call SetVisibility()
            Call FillCombo()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmBudgetFormulaList_Load", mstrModuleName)
        End Try
    End Sub

    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsTransactionHead.SetMessages()
            clsBudgetformula_master.SetMessages()
            objfrm._Other_ModuleNames = "clsTransactionHead,clsBudgetformula_master"
            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub

#End Region

#Region " Button's Events "

    Private Sub objbtnSearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearch.Click
        Try
            Call FillList()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearch_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnReset.Click
        Try
            cboTrnHead.SelectedValue = 0
            lvTrnHeadList.Items.Clear()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnReset_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnSearchTrnHead_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchTrnHead.Click
        Dim objfrm As New frmCommonSearch
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            With cboTrnHead
                objfrm.DataSource = CType(cboTrnHead.DataSource, DataTable) 'dtTable
                objfrm.ValueMember = .ValueMember
                objfrm.DisplayMember = .DisplayMember
                objfrm.CodeMember = "formula_code"
                If objfrm.DisplayDialog Then
                    .SelectedValue = objfrm.SelectedValue
                End If
                .Focus()
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchTrnHead_Click", mstrModuleName)
        Finally
            objfrm = Nothing
        End Try
    End Sub

    Private Sub btnNew_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNew.Click
        Try
            Dim objFrm As New frmBudgetFormula
            If User._Object._Isrighttoleft = True Then
                objFrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objFrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objFrm)
            End If

            If objFrm.displayDialog(-1, enAction.ADD_CONTINUE) Then
                Call FillList()
                Call FillCombo(True)
            End If
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "btnNew_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnEdit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnEdit.Click, lvTrnHeadList.DoubleClick
        Try
            If lvTrnHeadList.SelectedItems.Count < 1 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please select Budget Formula Head from the list to perform further operation on it."), enMsgBoxStyle.Information) '?1
                lvTrnHeadList.Select()
                Exit Sub
            End If
            Dim objbudgetFormula As New frmBudgetFormula
            Try
                Dim intSelectedIndex As Integer
                intSelectedIndex = lvTrnHeadList.SelectedItems(0).Index
                If objbudgetFormula.displayDialog(CInt(lvTrnHeadList.SelectedItems(0).Tag), enAction.EDIT_ONE) Then
                    Call FillList()
                End If
                If lvTrnHeadList.Items.Count > 0 Then
                    lvTrnHeadList.Items(intSelectedIndex).Selected = True
                    lvTrnHeadList.EnsureVisible(intSelectedIndex)
                End If
                lvTrnHeadList.Select()
            Catch ex As Exception
                Call DisplayError.Show("-1", ex.Message, "btnEdit_Click", mstrModuleName)
            Finally
                If objbudgetFormula IsNot Nothing Then objbudgetFormula.Dispose()
            End Try
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnEdit_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDelete.Click
        If lvTrnHeadList.SelectedItems.Count < 1 Then
            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please select Budget Formula Head from the list to perform further operation on it."), enMsgBoxStyle.Information) '?1
            lvTrnHeadList.Select()
            Exit Sub
        End If
        Try
            Dim intSelectedIndex As Integer
            intSelectedIndex = lvTrnHeadList.SelectedItems(0).Index

            If objBudgetMst.isUsed(CInt(lvTrnHeadList.SelectedItems(0).Tag)) = True Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Sorry, This Budget head is already in use."), enMsgBoxStyle.Information)
                lvTrnHeadList.Select()
                Exit Sub
            End If

            If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Are you sure you want to delete this Budget Formula Head?"), CType(enMsgBoxStyle.Question + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.Yes Then
                Dim frm As New frmReasonSelection
                Dim mstrVoidReason As String = String.Empty
                frm.displayDialog(enVoidCategoryType.PAYROLL, mstrVoidReason)
                If mstrVoidReason.Length <= 0 Then
                    Exit Sub
                End If
                frm = Nothing

                objBudgetMst._Budgetformulaunkid = CInt(lvTrnHeadList.SelectedItems(0).Tag)
                objBudgetMst._Voiddatetime = ConfigParameter._Object._CurrentDateAndTime
                objBudgetMst._Voiduserunkid = User._Object._Userunkid
                objBudgetMst._Voidreason = mstrVoidReason


                'S.SANDEEP [28-May-2018] -- START
                'ISSUE/ENHANCEMENT : {Audit Trails} 
                objBudgetMst._FormName = mstrModuleName
                objBudgetMst._LoginEmployeeunkid = 0
                objBudgetMst._ClientIP = getIP()
                objBudgetMst._HostName = getHostName()
                objBudgetMst._FromWeb = False
                objBudgetMst._AuditUserId = User._Object._Userunkid
objBudgetMst._CompanyUnkid = Company._Object._Companyunkid
                objBudgetMst._AuditDate = ConfigParameter._Object._CurrentDateAndTime
                'S.SANDEEP [28-May-2018] -- END

                If objBudgetMst.Delete() = False Then
                    eZeeMsgBox.Show(objBudgetMst._Message, enMsgBoxStyle.Information)
                    Exit Sub
                End If

                lvTrnHeadList.SelectedItems(0).Remove()

                If lvTrnHeadList.Items.Count <= 0 Then
                    Exit Try
                End If

                If lvTrnHeadList.Items.Count = intSelectedIndex Then
                    intSelectedIndex = lvTrnHeadList.Items.Count - 1
                    lvTrnHeadList.Items(intSelectedIndex).Selected = True
                    lvTrnHeadList.EnsureVisible(intSelectedIndex)
                ElseIf lvTrnHeadList.Items.Count <> 0 Then
                    lvTrnHeadList.Items(intSelectedIndex).Selected = True
                    lvTrnHeadList.EnsureVisible(intSelectedIndex)
                End If

            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnDelete_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Me.Close()
    End Sub


#End Region

#Region " Other Control's Events "
   
#End Region


	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Private Sub OtherSettings()
		Try
			Me.SuspendLayout()
			
			Call SetLanguage()
			
			Me.gbFilterCriteria.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbFilterCriteria.ForeColor = GUI._eZeeContainerHeaderForeColor 


			Me.btnDelete.GradientBackColor = GUI._ButttonBackColor 
			Me.btnDelete.GradientForeColor = GUI._ButttonFontColor

			Me.btnClose.GradientBackColor = GUI._ButttonBackColor 
			Me.btnClose.GradientForeColor = GUI._ButttonFontColor

			Me.btnEdit.GradientBackColor = GUI._ButttonBackColor 
			Me.btnEdit.GradientForeColor = GUI._ButttonFontColor

			Me.btnNew.GradientBackColor = GUI._ButttonBackColor 
			Me.btnNew.GradientForeColor = GUI._ButttonFontColor


			Me.ResumeLayout()
		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "OtherSettings", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetLanguage()
		Try
			Me.Text = Language._Object.getCaption(Me.Name, Me.Text)
			
			Me.colhCode.Text = Language._Object.getCaption(CStr(Me.colhCode.Tag), Me.colhCode.Text)
			Me.colhName.Text = Language._Object.getCaption(CStr(Me.colhName.Tag), Me.colhName.Text)
			Me.gbFilterCriteria.Text = Language._Object.getCaption(Me.gbFilterCriteria.Name, Me.gbFilterCriteria.Text)
			Me.lblTrnHead.Text = Language._Object.getCaption(Me.lblTrnHead.Name, Me.lblTrnHead.Text)
			Me.btnDelete.Text = Language._Object.getCaption(Me.btnDelete.Name, Me.btnDelete.Text)
			Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
			Me.btnEdit.Text = Language._Object.getCaption(Me.btnEdit.Name, Me.btnEdit.Text)
			Me.btnNew.Text = Language._Object.getCaption(Me.btnNew.Name, Me.btnNew.Text)

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetLanguage", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "Please select Budget Formula Head from the list to perform further operation on it.")
			Language.setMessage(mstrModuleName, 2, "Are you sure you want to delete this Budget Formula Head?")
			Language.setMessage(mstrModuleName, 1, "Sorry, This Budget head is already in use.")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class