﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmFundSource_List
    Inherits eZee.Common.eZeeForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmFundSource_List))
        Me.objFooter = New eZee.Common.eZeeFooter
        Me.btnDelete = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnEdit = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnNew = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnClose = New eZee.Common.eZeeLightButton(Me.components)
        Me.gbFilterCriteria = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.objbtnReset = New eZee.Common.eZeeSearchResetButton(Me.components)
        Me.objbtnSearch = New eZee.Common.eZeeSearchResetButton(Me.components)
        Me.cboFundName = New System.Windows.Forms.ComboBox
        Me.objbtnSearchFund = New eZee.Common.eZeeGradientButton
        Me.lblFundName = New System.Windows.Forms.Label
        Me.dgvFundSource = New System.Windows.Forms.DataGridView
        Me.dgcolhFundCode = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhFundName = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhProjectCode = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhCurrentCeilingBal = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhExpiryDate = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhNotifyAmount = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhfundsourceunkid = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objFooter.SuspendLayout()
        Me.gbFilterCriteria.SuspendLayout()
        CType(Me.objbtnReset, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.objbtnSearch, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dgvFundSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'objFooter
        '
        Me.objFooter.BorderColor = System.Drawing.Color.Silver
        Me.objFooter.Controls.Add(Me.btnDelete)
        Me.objFooter.Controls.Add(Me.btnEdit)
        Me.objFooter.Controls.Add(Me.btnNew)
        Me.objFooter.Controls.Add(Me.btnClose)
        Me.objFooter.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.objFooter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objFooter.GradiantStyle = eZee.Common.eZeeFooter.GradientStyle.Up
        Me.objFooter.GradientColor1 = System.Drawing.SystemColors.Control
        Me.objFooter.GradientColor2 = System.Drawing.SystemColors.Control
        Me.objFooter.Location = New System.Drawing.Point(0, 383)
        Me.objFooter.Name = "objFooter"
        Me.objFooter.Size = New System.Drawing.Size(785, 55)
        Me.objFooter.TabIndex = 3
        '
        'btnDelete
        '
        Me.btnDelete.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnDelete.BackColor = System.Drawing.Color.White
        Me.btnDelete.BackgroundImage = CType(resources.GetObject("btnDelete.BackgroundImage"), System.Drawing.Image)
        Me.btnDelete.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnDelete.BorderColor = System.Drawing.Color.Empty
        Me.btnDelete.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnDelete.FlatAppearance.BorderSize = 0
        Me.btnDelete.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnDelete.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnDelete.ForeColor = System.Drawing.Color.Black
        Me.btnDelete.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnDelete.GradientForeColor = System.Drawing.Color.Black
        Me.btnDelete.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnDelete.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnDelete.Location = New System.Drawing.Point(573, 13)
        Me.btnDelete.Name = "btnDelete"
        Me.btnDelete.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnDelete.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnDelete.Size = New System.Drawing.Size(97, 30)
        Me.btnDelete.TabIndex = 2
        Me.btnDelete.Text = "&Delete"
        Me.btnDelete.UseVisualStyleBackColor = True
        '
        'btnEdit
        '
        Me.btnEdit.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnEdit.BackColor = System.Drawing.Color.White
        Me.btnEdit.BackgroundImage = CType(resources.GetObject("btnEdit.BackgroundImage"), System.Drawing.Image)
        Me.btnEdit.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnEdit.BorderColor = System.Drawing.Color.Empty
        Me.btnEdit.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnEdit.FlatAppearance.BorderSize = 0
        Me.btnEdit.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnEdit.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnEdit.ForeColor = System.Drawing.Color.Black
        Me.btnEdit.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnEdit.GradientForeColor = System.Drawing.Color.Black
        Me.btnEdit.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnEdit.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnEdit.Location = New System.Drawing.Point(470, 13)
        Me.btnEdit.Name = "btnEdit"
        Me.btnEdit.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnEdit.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnEdit.Size = New System.Drawing.Size(97, 30)
        Me.btnEdit.TabIndex = 1
        Me.btnEdit.Text = "&Edit"
        Me.btnEdit.UseVisualStyleBackColor = True
        '
        'btnNew
        '
        Me.btnNew.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnNew.BackColor = System.Drawing.Color.White
        Me.btnNew.BackgroundImage = CType(resources.GetObject("btnNew.BackgroundImage"), System.Drawing.Image)
        Me.btnNew.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnNew.BorderColor = System.Drawing.Color.Empty
        Me.btnNew.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnNew.FlatAppearance.BorderSize = 0
        Me.btnNew.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnNew.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnNew.ForeColor = System.Drawing.Color.Black
        Me.btnNew.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnNew.GradientForeColor = System.Drawing.Color.Black
        Me.btnNew.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnNew.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnNew.Location = New System.Drawing.Point(367, 13)
        Me.btnNew.Name = "btnNew"
        Me.btnNew.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnNew.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnNew.Size = New System.Drawing.Size(97, 30)
        Me.btnNew.TabIndex = 0
        Me.btnNew.Text = "&New"
        Me.btnNew.UseVisualStyleBackColor = True
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnClose.BackColor = System.Drawing.Color.White
        Me.btnClose.BackgroundImage = CType(resources.GetObject("btnClose.BackgroundImage"), System.Drawing.Image)
        Me.btnClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnClose.BorderColor = System.Drawing.Color.Empty
        Me.btnClose.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnClose.FlatAppearance.BorderSize = 0
        Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClose.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.ForeColor = System.Drawing.Color.Black
        Me.btnClose.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnClose.GradientForeColor = System.Drawing.Color.Black
        Me.btnClose.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Location = New System.Drawing.Point(676, 13)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Size = New System.Drawing.Size(97, 30)
        Me.btnClose.TabIndex = 3
        Me.btnClose.Text = "&Close"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'gbFilterCriteria
        '
        Me.gbFilterCriteria.BorderColor = System.Drawing.Color.Black
        Me.gbFilterCriteria.Checked = False
        Me.gbFilterCriteria.CollapseAllExceptThis = False
        Me.gbFilterCriteria.CollapsedHoverImage = Nothing
        Me.gbFilterCriteria.CollapsedNormalImage = Nothing
        Me.gbFilterCriteria.CollapsedPressedImage = Nothing
        Me.gbFilterCriteria.CollapseOnLoad = False
        Me.gbFilterCriteria.Controls.Add(Me.objbtnReset)
        Me.gbFilterCriteria.Controls.Add(Me.objbtnSearch)
        Me.gbFilterCriteria.Controls.Add(Me.cboFundName)
        Me.gbFilterCriteria.Controls.Add(Me.objbtnSearchFund)
        Me.gbFilterCriteria.Controls.Add(Me.lblFundName)
        Me.gbFilterCriteria.ExpandedHoverImage = Nothing
        Me.gbFilterCriteria.ExpandedNormalImage = Nothing
        Me.gbFilterCriteria.ExpandedPressedImage = Nothing
        Me.gbFilterCriteria.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbFilterCriteria.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbFilterCriteria.HeaderHeight = 25
        Me.gbFilterCriteria.HeaderMessage = ""
        Me.gbFilterCriteria.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbFilterCriteria.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbFilterCriteria.HeightOnCollapse = 0
        Me.gbFilterCriteria.LeftTextSpace = 0
        Me.gbFilterCriteria.Location = New System.Drawing.Point(12, 12)
        Me.gbFilterCriteria.Name = "gbFilterCriteria"
        Me.gbFilterCriteria.OpenHeight = 300
        Me.gbFilterCriteria.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbFilterCriteria.ShowBorder = True
        Me.gbFilterCriteria.ShowCheckBox = False
        Me.gbFilterCriteria.ShowCollapseButton = False
        Me.gbFilterCriteria.ShowDefaultBorderColor = True
        Me.gbFilterCriteria.ShowDownButton = False
        Me.gbFilterCriteria.ShowHeader = True
        Me.gbFilterCriteria.Size = New System.Drawing.Size(760, 65)
        Me.gbFilterCriteria.TabIndex = 4
        Me.gbFilterCriteria.Temp = 0
        Me.gbFilterCriteria.Text = "Filter Criteria"
        Me.gbFilterCriteria.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objbtnReset
        '
        Me.objbtnReset.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.objbtnReset.BackColor = System.Drawing.Color.Transparent
        Me.objbtnReset.ButtonType = eZee.Common.eZeeSearchResetButton.EnumButtonType.Reset
        Me.objbtnReset.Image = CType(resources.GetObject("objbtnReset.Image"), System.Drawing.Image)
        Me.objbtnReset.Location = New System.Drawing.Point(734, 0)
        Me.objbtnReset.Name = "objbtnReset"
        Me.objbtnReset.ResultMessage = ""
        Me.objbtnReset.SearchMessage = ""
        Me.objbtnReset.Size = New System.Drawing.Size(24, 24)
        Me.objbtnReset.TabIndex = 69
        Me.objbtnReset.TabStop = False
        '
        'objbtnSearch
        '
        Me.objbtnSearch.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.objbtnSearch.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearch.ButtonType = eZee.Common.eZeeSearchResetButton.EnumButtonType.Search
        Me.objbtnSearch.Image = CType(resources.GetObject("objbtnSearch.Image"), System.Drawing.Image)
        Me.objbtnSearch.Location = New System.Drawing.Point(710, 0)
        Me.objbtnSearch.Name = "objbtnSearch"
        Me.objbtnSearch.ResultMessage = ""
        Me.objbtnSearch.SearchMessage = ""
        Me.objbtnSearch.Size = New System.Drawing.Size(24, 24)
        Me.objbtnSearch.TabIndex = 68
        Me.objbtnSearch.TabStop = False
        '
        'cboFundName
        '
        Me.cboFundName.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend
        Me.cboFundName.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.cboFundName.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboFundName.FormattingEnabled = True
        Me.cboFundName.Location = New System.Drawing.Point(110, 34)
        Me.cboFundName.Name = "cboFundName"
        Me.cboFundName.Size = New System.Drawing.Size(188, 21)
        Me.cboFundName.TabIndex = 0
        '
        'objbtnSearchFund
        '
        Me.objbtnSearchFund.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchFund.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchFund.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchFund.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchFund.BorderSelected = False
        Me.objbtnSearchFund.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchFund.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objbtnSearchFund.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchFund.Location = New System.Drawing.Point(304, 34)
        Me.objbtnSearchFund.Name = "objbtnSearchFund"
        Me.objbtnSearchFund.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchFund.TabIndex = 88
        '
        'lblFundName
        '
        Me.lblFundName.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblFundName.Location = New System.Drawing.Point(8, 37)
        Me.lblFundName.Name = "lblFundName"
        Me.lblFundName.Size = New System.Drawing.Size(94, 15)
        Me.lblFundName.TabIndex = 85
        Me.lblFundName.Text = "Fund Name"
        '
        'dgvFundSource
        '
        Me.dgvFundSource.AllowUserToAddRows = False
        Me.dgvFundSource.AllowUserToDeleteRows = False
        Me.dgvFundSource.AllowUserToResizeRows = False
        Me.dgvFundSource.BackgroundColor = System.Drawing.Color.WhiteSmoke
        Me.dgvFundSource.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.dgvFundSource.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.[Single]
        Me.dgvFundSource.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing
        Me.dgvFundSource.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.dgcolhFundCode, Me.dgcolhFundName, Me.dgcolhProjectCode, Me.dgcolhCurrentCeilingBal, Me.dgcolhExpiryDate, Me.dgcolhNotifyAmount, Me.objdgcolhfundsourceunkid})
        Me.dgvFundSource.Location = New System.Drawing.Point(12, 83)
        Me.dgvFundSource.MultiSelect = False
        Me.dgvFundSource.Name = "dgvFundSource"
        Me.dgvFundSource.ReadOnly = True
        Me.dgvFundSource.RowHeadersVisible = False
        Me.dgvFundSource.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvFundSource.Size = New System.Drawing.Size(760, 294)
        Me.dgvFundSource.TabIndex = 5
        '
        'dgcolhFundCode
        '
        Me.dgcolhFundCode.HeaderText = "Code"
        Me.dgcolhFundCode.Name = "dgcolhFundCode"
        Me.dgcolhFundCode.ReadOnly = True
        '
        'dgcolhFundName
        '
        Me.dgcolhFundName.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.dgcolhFundName.HeaderText = "Fund Name"
        Me.dgcolhFundName.Name = "dgcolhFundName"
        Me.dgcolhFundName.ReadOnly = True
        '
        'dgcolhProjectCode
        '
        Me.dgcolhProjectCode.HeaderText = "Project Code"
        Me.dgcolhProjectCode.Name = "dgcolhProjectCode"
        Me.dgcolhProjectCode.ReadOnly = True
        Me.dgcolhProjectCode.Visible = False
        '
        'dgcolhCurrentCeilingBal
        '
        Me.dgcolhCurrentCeilingBal.HeaderText = "Current Ceiling Balance"
        Me.dgcolhCurrentCeilingBal.Name = "dgcolhCurrentCeilingBal"
        Me.dgcolhCurrentCeilingBal.ReadOnly = True
        Me.dgcolhCurrentCeilingBal.Visible = False
        Me.dgcolhCurrentCeilingBal.Width = 175
        '
        'dgcolhExpiryDate
        '
        Me.dgcolhExpiryDate.HeaderText = "Expiry Date"
        Me.dgcolhExpiryDate.Name = "dgcolhExpiryDate"
        Me.dgcolhExpiryDate.ReadOnly = True
        Me.dgcolhExpiryDate.Visible = False
        Me.dgcolhExpiryDate.Width = 120
        '
        'dgcolhNotifyAmount
        '
        Me.dgcolhNotifyAmount.HeaderText = "Notify Amount"
        Me.dgcolhNotifyAmount.Name = "dgcolhNotifyAmount"
        Me.dgcolhNotifyAmount.ReadOnly = True
        Me.dgcolhNotifyAmount.Visible = False
        '
        'objdgcolhfundsourceunkid
        '
        Me.objdgcolhfundsourceunkid.HeaderText = "objdgcolhfundsourceunkid"
        Me.objdgcolhfundsourceunkid.Name = "objdgcolhfundsourceunkid"
        Me.objdgcolhfundsourceunkid.ReadOnly = True
        Me.objdgcolhfundsourceunkid.Visible = False
        Me.objdgcolhfundsourceunkid.Width = 5
        '
        'frmFundSource_List
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(785, 438)
        Me.Controls.Add(Me.dgvFundSource)
        Me.Controls.Add(Me.gbFilterCriteria)
        Me.Controls.Add(Me.objFooter)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmFundSource_List"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "List of Fund Sources"
        Me.objFooter.ResumeLayout(False)
        Me.gbFilterCriteria.ResumeLayout(False)
        CType(Me.objbtnReset, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.objbtnSearch, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dgvFundSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents objFooter As eZee.Common.eZeeFooter
    Friend WithEvents btnDelete As eZee.Common.eZeeLightButton
    Friend WithEvents btnEdit As eZee.Common.eZeeLightButton
    Friend WithEvents btnNew As eZee.Common.eZeeLightButton
    Friend WithEvents btnClose As eZee.Common.eZeeLightButton
    Friend WithEvents gbFilterCriteria As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents objbtnReset As eZee.Common.eZeeSearchResetButton
    Friend WithEvents objbtnSearch As eZee.Common.eZeeSearchResetButton
    Friend WithEvents cboFundName As System.Windows.Forms.ComboBox
    Friend WithEvents objbtnSearchFund As eZee.Common.eZeeGradientButton
    Friend WithEvents lblFundName As System.Windows.Forms.Label
    Friend WithEvents dgvFundSource As System.Windows.Forms.DataGridView
    Friend WithEvents dgcolhFundCode As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhFundName As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhProjectCode As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhCurrentCeilingBal As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhExpiryDate As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhNotifyAmount As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhfundsourceunkid As System.Windows.Forms.DataGridViewTextBoxColumn
End Class
