﻿Option Strict On
Imports eZeeCommonLib
Imports Aruti.Data
Imports System.Threading

Public Class frmApproveDisapproveBudget

#Region " Private Variables "
    Private ReadOnly mstrModuleName As String = "frmApproveDisapproveBudget"

    Private mblnCancel As Boolean = True
    Private menAction As enAction = enAction.ADD_ONE
    Private trd As Thread

    Private objBudgetApproval As clsBudget_approval_tran
    Private objBudget As clsBudget_MasterNew

    Private mintBudgetapprovaltranunkid As Integer
    Private mintBudgetunkid As Integer
    Private mintCurrApprovalStatus As Integer

    Private mintCurrLevelPriority As Integer = -1
    Private mintLowerLevelPriority As Integer = -99 'Keep -99
    Private mintMinPriority As Integer = -1
    Private mintMaxPriority As Integer = -1
    Private mintLowerPriorityForFirstLevel As Integer = -1
    Private mintCurrLevelID As Integer = -1

    Private mdicAllocation As New Dictionary(Of Integer, String)
    Private mdicStatus As New Dictionary(Of Integer, String)
    Private mstrAdvanceFilter As String = ""
#End Region

#Region " Display Dialogue "
    Public Function DisplayDialog(ByVal Action As enAction, ByVal intBudgetapprovaltranunkid As Integer, ByVal intBudgetunkid As Integer, ByVal intCurrApprovalStatus As Integer) As Boolean
        Try
            menAction = Action
            mintBudgetapprovaltranunkid = intBudgetapprovaltranunkid
            mintBudgetunkid = intBudgetunkid
            mintCurrApprovalStatus = intCurrApprovalStatus

            Me.ShowDialog()

            Return Not mblnCancel
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "DisplayDialog", mstrModuleName)
        End Try
    End Function
#End Region

#Region " Private Methods & Functions "
    Private Sub SetColor()
        Try
            cboStatus.BackColor = GUI.ColorComp
            txtRemarks.BackColor = GUI.ColorOptional
            txtBudgetCode.BackColor = GUI.ColorComp
            txtBudgetName.BackColor = GUI.ColorComp
            txtBudgetYear.BackColor = GUI.ColorComp
            txtViewBy.BackColor = GUI.ColorComp
            txtPresentation.BackColor = GUI.ColorComp
            txtWhoToInclude.BackColor = GUI.ColorComp
            txtBudgetDate.BackColor = GUI.ColorComp
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetColor", mstrModuleName)
        End Try
    End Sub

    Private Sub FillCombo()
        Dim objMaster As New clsMasterData
        Dim objApproverMap As New clsbudget_approver_mapping
        Dim objApproverLevel As New clsbudgetapproverlevel_master
        Dim dsCombo As DataSet
        Dim dtTable As DataTable

        Try

            'dsCombo = objMaster.GetEAllocation_Notification("List")
            'mdicAllocation = (From p In dsCombo.Tables("List").AsEnumerable Select New With {Key .ID = CInt(p.Item("Id")), Key .NAME = p.Item("name").ToString}).ToDictionary(Function(x) x.ID, Function(x) x.NAME)

            dsCombo = objMaster.getApprovalStatus("Status", , , True, True, True, True)
            mdicStatus = (From p In dsCombo.Tables("Status").AsEnumerable Select New With {Key .ID = CInt(p.Item("Id")), Key .NAME = p.Item("name").ToString}).ToDictionary(Function(x) x.ID, Function(x) x.NAME)

            If mintCurrApprovalStatus = enApprovalStatus.PENDING Then
                dsCombo = objMaster.getApprovalStatus("ApprovalStatus", False, , , True, True)
                dtTable = New DataView(dsCombo.Tables("ApprovalStatus")).ToTable
            ElseIf mintCurrApprovalStatus = enApprovalStatus.APPROVED Then
                dsCombo = objMaster.getApprovalStatus("ApprovalStatus", False, , , , , True)
                dtTable = New DataView(dsCombo.Tables("ApprovalStatus")).ToTable
            Else
                dtTable = New DataView(dsCombo.Tables("ApprovalStatus"), "ID <> " & mintCurrApprovalStatus & " ", "", DataViewRowState.CurrentRows).ToTable
            End If
            With cboStatus
                .ValueMember = "Id"
                .DisplayMember = "name"
                .DataSource = dtTable
                If .Items.Count > 0 Then .SelectedIndex = 0
            End With

            dsCombo = objApproverMap.GetList("ApproverLevel", User._Object._Userunkid)
            If dsCombo.Tables("ApproverLevel").Rows.Count > 0 Then
                mintCurrLevelPriority = CInt(dsCombo.Tables("ApproverLevel").Rows(0).Item("priority"))
                mintLowerLevelPriority = objApproverLevel.GetLowerLevelPriority(mintCurrLevelPriority)
                mintMinPriority = objApproverLevel.GetMinPriority()
                mintMaxPriority = objApproverLevel.GetMaxPriority()
                mintLowerPriorityForFirstLevel = objApproverLevel.GetLowerLevelPriority(mintMinPriority)

                If mintCurrLevelPriority >= 0 Then
                    mintCurrLevelID = CInt(dsCombo.Tables("ApproverLevel").Rows(0).Item("levelunkid"))
                    objApproverName.Text = dsCombo.Tables("ApproverLevel").Rows(0).Item("approver").ToString
                    objApproverLevelVal.Text = dsCombo.Tables("ApproverLevel").Rows(0).Item("levelname").ToString
                    objLevelPriorityVal.Text = mintCurrLevelPriority.ToString
                End If
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        Finally
            objMaster = Nothing
            objApproverMap = Nothing
            objApproverLevel = Nothing
        End Try
    End Sub

    Private Sub FillInfo()
        Dim objMaster As New clsMasterData
        Dim ds As DataSet
        Try
            ds = objMaster.getComboListForBudgetViewBy("ViewBy")
            Dim dicViewBy As Dictionary(Of Integer, String) = (From p In ds.Tables("ViewBy") Select New With {.id = CInt(p.Item("id")), .name = p.Item("name").ToString}).ToDictionary(Function(x) x.id, Function(x) x.name)
            ds = objMaster.getComboListForBudgetPresentation("Presentation")
            Dim dicPresentation As Dictionary(Of Integer, String) = (From p In ds.Tables("Presentation") Select New With {.id = CInt(p.Item("id")), .name = p.Item("name").ToString}).ToDictionary(Function(x) x.id, Function(x) x.name)
            ds = objMaster.getComboListForBudgetWhoToInclude("WhoToInclude")
            Dim dicWhoToInclude As Dictionary(Of Integer, String) = (From p In ds.Tables("WhoToInclude") Select New With {.id = CInt(p.Item("id")), .name = p.Item("name").ToString}).ToDictionary(Function(x) x.id, Function(x) x.name)

            txtBudgetCode.Text = objBudget._Budget_Code
            txtBudgetName.Text = objBudget._Budget_Name
            txtBudgetYear.Text = objBudget._PayyearName
            If dicViewBy.ContainsKey(objBudget._Viewbyid) = True Then
                txtViewBy.Text = dicViewBy.Item(objBudget._Viewbyid)
            Else
                txtViewBy.Text = ""
            End If
            If dicPresentation.ContainsKey(objBudget._Presentationmodeid) = True Then
                txtPresentation.Text = dicPresentation.Item(objBudget._Presentationmodeid)
            Else
                txtPresentation.Text = ""
            End If
            If dicWhoToInclude.ContainsKey(objBudget._Whotoincludeid) = True Then
                txtWhoToInclude.Text = dicWhoToInclude.Item(objBudget._Whotoincludeid)
            Else
                txtWhoToInclude.Text = ""
            End If
            txtBudgetDate.Text = objBudget._Budget_date.ToShortDateString


        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillInfo", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub SetValue()
        Try
            objBudgetApproval._Budgetunkid = mintBudgetunkid
            objBudgetApproval._Levelunkid = mintCurrLevelID
            objBudgetApproval._Priority = mintCurrLevelPriority
            objBudgetApproval._Approval_Date = ConfigParameter._Object._CurrentDateAndTime
            objBudgetApproval._Statusunkid = CInt(cboStatus.SelectedValue)
            objBudgetApproval._Remarks = txtRemarks.Text.Trim
            objBudgetApproval._Userunkid = User._Object._Userunkid
            objBudgetApproval._Isvoid = False
            objBudgetApproval._Voiduserunkid = -1
            objBudgetApproval._Voiddatetime = Nothing
            objBudgetApproval._Voidreason = ""

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetValue", mstrModuleName)
        End Try
    End Sub

    Private Function IsValid() As Boolean
        Try
            If CInt(cboStatus.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please select Approval Status."), enMsgBoxStyle.Information)
                cboStatus.Focus()
                Return False
            ElseIf mintCurrLevelPriority < 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Sorry! You can not not Approve / Disapprove Budget. Please contact Administrator."), enMsgBoxStyle.Information)
                Return False
            End If

            If gobjEmailList.Count > 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Sending Email(s) process is in progress from other module. Please wait for some time."), enMsgBoxStyle.Information)
                Return False
            End If

            Return True

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "IsValid", mstrModuleName)
        End Try
    End Function

    Private Sub Send_Notification()
        Try
            If gobjEmailList.Count > 0 Then
                Dim objSendMail As New clsSendMail
                For Each obj In gobjEmailList
                    objSendMail._ToEmail = obj._EmailTo
                    objSendMail._Subject = obj._Subject
                    objSendMail._Message = obj._Message
                    objSendMail._FormName = obj._FormName
                    objSendMail._ClientIP = getIP()
                    objSendMail._HostName = getHostName()
                    objSendMail._FromWeb = False
                    objSendMail._AuditUserId = User._Object._Userunkid
objSendMail._CompanyUnkid = Company._Object._Companyunkid
                    objSendMail._AuditDate = ConfigParameter._Object._CurrentDateAndTime
                    objSendMail._LoginEmployeeunkid = obj._LogEmployeeUnkid
                    objSendMail._OperationModeId = obj._OperationModeId
                    objSendMail._UserUnkid = obj._UserUnkid
                    objSendMail._SenderAddress = obj._SenderAddress
                    objSendMail._ModuleRefId = obj._ModuleRefId
                    Try
                        'Sohail (30 Nov 2017) -- Start
                        'SUMATRA Enhancement – SUMATRA – issue # 0001669: Additional feature for Exchange Server configuration setting in 70.1.
                        'objSendMail.SendMail()
                        objSendMail.SendMail(Company._Object._Companyunkid)
                        'Sohail (30 Nov 2017) -- End
                    Catch ex As Exception

                    End Try
                Next
                gobjEmailList.Clear()
            End If
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "Send_Notification", mstrModuleName)
        Finally
            If gobjEmailList.Count > 0 Then
                gobjEmailList.Clear()
            End If
        End Try
    End Sub
#End Region

#Region " Form's Events "
    Private Sub frmApproveDisapproveBudget_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        Try
            objBudgetApproval = Nothing
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmApproveDisapproveBudget_FormClosed", mstrModuleName)
        End Try
    End Sub

    Private Sub frmApproveDisapproveBudget_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown
        Try
            Select Case e.KeyCode
                Case Keys.Return
                    SendKeys.Send("{TAB}")
                    e.Handled = True
            End Select
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmApproveDisapproveBudget_KeyDown", mstrModuleName)
        End Try
    End Sub

    Private Sub frmApproveDisapproveBudget_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        objBudgetApproval = New clsBudget_approval_tran
        objBudget = New clsBudget_MasterNew
        Try
            Call Set_Logo(Me, gApplicationType)

            Call Language.setLanguage(Me.Name)
            Call OtherSettings()

            objBudgetApproval._Budgetapprovaltranunkid = mintBudgetapprovaltranunkid
            objBudget._Budgetunkid = mintBudgetunkid

            Call SetColor()
            Call FillCombo()
            Call FillInfo()


        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmApproveDisapproveBudget_Load", mstrModuleName)
        End Try
    End Sub

    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsBudget_approval_tran.SetMessages()
            objfrm._Other_ModuleNames = "clsBudget_approval_tran"
            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub

#End Region

#Region " ComboBox's Events "

#End Region


#Region " Button's Events "
    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnClose_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Dim blnResult As Boolean
        Dim intIsApproved As Integer = 0
        Dim strMsg As String = ""
        Dim strSuccessMsg As String = "" 'Sohail (10 Feb 2017)

        Try
            If IsValid() = False Then Exit Sub

            If CInt(cboStatus.SelectedValue) = enApprovalStatus.APPROVED Then
                If mintMaxPriority > 0 AndAlso mintMaxPriority = mintCurrLevelPriority Then
                    strMsg = Language.getMessage(mstrModuleName, 4, "Are you sure you want to Final Approve this Budget?")
                Else
                    strMsg = Language.getMessage(mstrModuleName, 5, "Are you sure you want to Approve this Budget?")
                End If
                strSuccessMsg = Language.getMessage(mstrModuleName, 8, "Budget is Approved successfully.") 'Sohail (10 Feb 2017)
            ElseIf CInt(cboStatus.SelectedValue) = enApprovalStatus.CANCELLED Then
                strMsg = Language.getMessage(mstrModuleName, 6, "Are you sure you want to Cancel this Budget?")
                strSuccessMsg = Language.getMessage(mstrModuleName, 9, "Budget is Cancelled successfully.") 'Sohail (10 Feb 2017)
            ElseIf CInt(cboStatus.SelectedValue) = enApprovalStatus.REJECTED Then
                strMsg = Language.getMessage(mstrModuleName, 7, "Are you sure you want to Reject this Budget?")
                strSuccessMsg = Language.getMessage(mstrModuleName, 10, "Budget is Rejected successfully.") 'Sohail (10 Feb 2017)
            End If

            If eZeeMsgBox.Show(strMsg, CType(enMsgBoxStyle.Question + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.No Then
                Exit Try
            End If

            objBudgetApproval._Budgetapprovaltranunkid = mintBudgetapprovaltranunkid
            Call SetValue()

            Dim mstrDisApprovalReason As String = String.Empty
            If CInt(cboStatus.SelectedValue) = enApprovalStatus.REJECTED Then
                Dim frm As New frmReasonSelection
                If User._Object._Isrighttoleft = True Then
                    frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                    frm.RightToLeftLayout = True
                    Call Language.ctlRightToLeftlayOut(frm)
                End If


                frm.displayDialog(enVoidCategoryType.PAYROLL, mstrDisApprovalReason)
                If mstrDisApprovalReason.Length <= 0 Then
                    Exit Try
                End If
            End If
            objBudgetApproval._Disapprovalreason = mstrDisApprovalReason


            If mintMaxPriority > 0 AndAlso mintMaxPriority = mintCurrLevelPriority AndAlso CInt(cboStatus.SelectedValue) = enApprovalStatus.APPROVED Then
                intIsApproved = enApprovalStatus.APPROVED
            ElseIf CInt(cboStatus.SelectedValue) = enApprovalStatus.REJECTED Then
                intIsApproved = enApprovalStatus.REJECTED
            ElseIf CInt(cboStatus.SelectedValue) = enApprovalStatus.CANCELLED Then
                intIsApproved = enApprovalStatus.CANCELLED
            ElseIf CInt(cboStatus.SelectedValue) = enApprovalStatus.PUBLISHED Then
                intIsApproved = enApprovalStatus.PUBLISHED
            End If


            'S.SANDEEP [28-May-2018] -- START
            'ISSUE/ENHANCEMENT : {Audit Trails} 
            objBudgetApproval._FormName = mstrModuleName
            objBudgetApproval._LoginEmployeeunkid = 0
            objBudgetApproval._ClientIP = getIP()
            objBudgetApproval._HostName = getHostName()
            objBudgetApproval._FromWeb = False
            objBudgetApproval._AuditUserId = User._Object._Userunkid
objBudgetApproval._CompanyUnkid = Company._Object._Companyunkid
            objBudgetApproval._AuditDate = ConfigParameter._Object._CurrentDateAndTime
            'S.SANDEEP [28-May-2018] -- END

            blnResult = objBudgetApproval.UpdateBudgetApproval(mintBudgetapprovaltranunkid, User._Object._Userunkid, ConfigParameter._Object._CurrentDateAndTime, intIsApproved)


            If blnResult = False And objBudgetApproval._Message <> "" Then
                eZeeMsgBox.Show(objBudgetApproval._Message, enMsgBoxStyle.Information)
            Else


                objBudgetApproval.SendMailToApprover(CInt(cboStatus.SelectedValue), User._Object._Userunkid, mintBudgetapprovaltranunkid, mintBudgetunkid, txtBudgetCode.Text, txtRemarks.Text.Trim, Company._Object._Companyunkid, ConfigParameter._Object._IsArutiDemo, ConfigParameter._Object._ArutiSelfServiceURL, FinancialYear._Object._YearUnkid, ConfigParameter._Object._UserAccessModeSetting, enLogin_Mode.DESKTOP, 0)
                'Sohail (10 Feb 2017) -- Start
                'eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 8, "Budget Approval saved successfully."), enMsgBoxStyle.Information)
                eZeeMsgBox.Show(strSuccessMsg, enMsgBoxStyle.Information)
                'Sohail (10 Feb 2017) -- End

                trd = New Thread(AddressOf Send_Notification)
                trd.IsBackground = True
                trd.Start()

                mblnCancel = False
                If menAction = enAction.ADD_CONTINUE Then
                    cboStatus.SelectedIndex = 0
                    txtRemarks.Text = ""
                Else
                    mintBudgetapprovaltranunkid = objBudgetApproval._Budgetapprovaltranunkid
                    Me.Close()
                End If


                mblnCancel = False
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnSave_Click", mstrModuleName)
        End Try
    End Sub
#End Region

#Region " Other Controls Events "


#End Region

	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Private Sub OtherSettings()
		Try
			Me.SuspendLayout()
			
			Call SetLanguage()
			
			Me.gbBudgetReqApprovalnfo.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbBudgetReqApprovalnfo.ForeColor = GUI._eZeeContainerHeaderForeColor 

			Me.gbBudgetList.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbBudgetList.ForeColor = GUI._eZeeContainerHeaderForeColor 


			
			Me.eZeeHeader.GradientColor1 = GUI._HeaderBackColor1 
			Me.eZeeHeader.GradientColor2 = GUI._HeaderBackColor2 
			Me.eZeeHeader.BorderColor = GUI._HeaderBorderColor 
			Me.eZeeHeader.HeaderTextForeColor = GUI._HeaderTitleFontColor 
			Me.eZeeHeader.DescriptionForeColor = GUI._HeaderMessageFontColor 


			Me.btnSave.GradientBackColor = GUI._ButttonBackColor 
			Me.btnSave.GradientForeColor = GUI._ButttonFontColor

			Me.btnClose.GradientBackColor = GUI._ButttonBackColor 
			Me.btnClose.GradientForeColor = GUI._ButttonFontColor


			Me.ResumeLayout()
		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "OtherSettings", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetLanguage()
		Try
			Me.Text = Language._Object.getCaption(Me.Name, Me.Text)
			
			Me.gbBudgetReqApprovalnfo.Text = Language._Object.getCaption(Me.gbBudgetReqApprovalnfo.Name, Me.gbBudgetReqApprovalnfo.Text)
			Me.lblApprovalStatus.Text = Language._Object.getCaption(Me.lblApprovalStatus.Name, Me.lblApprovalStatus.Text)
			Me.lblRemarks.Text = Language._Object.getCaption(Me.lblRemarks.Name, Me.lblRemarks.Text)
			Me.lblApproverInfo.Text = Language._Object.getCaption(Me.lblApproverInfo.Name, Me.lblApproverInfo.Text)
			Me.lblApprover.Text = Language._Object.getCaption(Me.lblApprover.Name, Me.lblApprover.Text)
			Me.lblLevelPriority.Text = Language._Object.getCaption(Me.lblLevelPriority.Name, Me.lblLevelPriority.Text)
			Me.lblApproverLevel.Text = Language._Object.getCaption(Me.lblApproverLevel.Name, Me.lblApproverLevel.Text)
			Me.gbBudgetList.Text = Language._Object.getCaption(Me.gbBudgetList.Name, Me.gbBudgetList.Text)
			Me.eZeeHeader.Title = Language._Object.getCaption(Me.eZeeHeader.Name & "_Title" , Me.eZeeHeader.Title)
			Me.eZeeHeader.Message = Language._Object.getCaption(Me.eZeeHeader.Name & "_Message" , Me.eZeeHeader.Message)
			Me.btnSave.Text = Language._Object.getCaption(Me.btnSave.Name, Me.btnSave.Text)
			Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
			Me.lblBudgetDate.Text = Language._Object.getCaption(Me.lblBudgetDate.Name, Me.lblBudgetDate.Text)
			Me.lblBudgetName.Text = Language._Object.getCaption(Me.lblBudgetName.Name, Me.lblBudgetName.Text)
			Me.lblBudgetCode.Text = Language._Object.getCaption(Me.lblBudgetCode.Name, Me.lblBudgetCode.Text)
			Me.lblBudgetYear.Text = Language._Object.getCaption(Me.lblBudgetYear.Name, Me.lblBudgetYear.Text)
			Me.lblWhoToInclude.Text = Language._Object.getCaption(Me.lblWhoToInclude.Name, Me.lblWhoToInclude.Text)
			Me.lblPresentation.Text = Language._Object.getCaption(Me.lblPresentation.Name, Me.lblPresentation.Text)
			Me.lblViewBy.Text = Language._Object.getCaption(Me.lblViewBy.Name, Me.lblViewBy.Text)

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetLanguage", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "Please select Approval Status.")
			Language.setMessage(mstrModuleName, 2, "Sorry! You can not not Approve / Disapprove Budget. Please contact Administrator.")
			Language.setMessage(mstrModuleName, 3, "Sending Email(s) process is in progress from other module. Please wait for some time.")
			Language.setMessage(mstrModuleName, 4, "Are you sure you want to Final Approve this Budget?")
			Language.setMessage(mstrModuleName, 5, "Are you sure you want to Approve this Budget?")
			Language.setMessage(mstrModuleName, 6, "Are you sure you want to Cancel this Budget?")
			Language.setMessage(mstrModuleName, 7, "Are you sure you want to Reject this Budget?")
			Language.setMessage(mstrModuleName, 8, "Budget is Approved successfully.")
			Language.setMessage(mstrModuleName, 9, "Budget is Cancelled successfully.")
			Language.setMessage(mstrModuleName, 10, "Budget is Rejected successfully.")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class