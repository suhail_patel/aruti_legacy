﻿Option Strict On

Imports eZeeCommonLib
Imports Aruti.Data

Public Class frmBatchPosting_AddEdit

#Region " Private Variables "
    Private ReadOnly mstrModuleName As String = "frmBatchPosting_AddEdit"
    Private mblnCancel As Boolean = True
    Private menAction As enAction = enAction.ADD_ONE
    Private objBatchPosting As clsBatchPosting
    Private objBatchPostingTran As clsBatchPostingTran
    Dim mintBatchPostingUnkid As Integer = 0
    'Dim mintBatchPostingTranUnkid As Integer = 0
    Private mdtTran As DataTable

    Dim mdtPeriodStart As DateTime = Nothing
    Dim mdtPeriodEnd As DateTime = Nothing
    Private mblnIsPosted As Boolean = False 'Sohail (24 Feb 2016)
#End Region

#Region " Display Dialog "
    Public Function displayDialog(ByRef intUnkId As Integer, ByVal eAction As enAction, ByVal blnIsPosted As Boolean) As Boolean
        'Sohail (24 Feb 2016) - [blnIsPosted]
        Try
            mintBatchPostingUnkid = intUnkId
            'mintBatchPostingTranUnkid = intTranUnkId
            menAction = eAction
            mblnIsPosted = blnIsPosted 'Sohail (24 Feb 2016)

            Me.ShowDialog()

            intUnkId = mintBatchPostingUnkid
            'intTranUnkId = mintBatchPostingTranUnkid

            Return Not mblnCancel

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "displayDialog", mstrModuleName)
        End Try
    End Function
#End Region

#Region " Private Methods "

    Private Sub SetColor()
        Try
            txtBatchPostingNo.BackColor = GUI.ColorComp
            cboPeriod.BackColor = GUI.ColorComp
            'Sohail (24 Feb 2016) -- Start
            'Enhancement - Batch Posting changes as per online doc in 58.1 SP. (#15 (KBC & Kenya Project Comments List.xls))
            'txtEmployeeCode.BackColor = GUI.ColorComp
            'txtTranHeadCode.BackColor = GUI.ColorComp
            cboEmployee.BackColor = GUI.ColorComp
            cboTranhead.BackColor = GUI.ColorComp
            txtBatchCode.BackColor = GUI.ColorComp
            txtBatchName.BackColor = GUI.ColorComp
            txtDescription.BackColor = GUI.ColorOptional
            'Sohail (24 Feb 2016) -- End
            txtAmount.BackColor = GUI.ColorComp
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetColor", mstrModuleName)
        End Try
    End Sub

    Private Sub GetValue()
        'Dim objEmp As New clsEmployee_Master
        'Dim objTranHead As New clsTransactionHead
        Try
            txtBatchPostingNo.Text = objBatchPosting._Batchno
            'Sohail (24 Feb 2016) -- Start
            'Enhancement - Batch Posting changes as per online doc in 58.1 SP. (#15 (KBC & Kenya Project Comments List.xls))
            txtBatchCode.Text = objBatchPosting._BatchCode
            txtBatchName.Text = objBatchPosting._BatchName
            txtDescription.Text = objBatchPosting._Description
            'Sohail (24 Feb 2016) -- End
            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'objBatchPostingTran._Empbatchpostingunkid = objBatchPosting._Empbatchpostingunkid
            objBatchPostingTran.GetBatchPosting_Tran(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, mdtPeriodStart, mdtPeriodEnd, ConfigParameter._Object._UserAccessModeSetting, True, ConfigParameter._Object._IsIncludeInactiveEmp, objBatchPosting._Empbatchpostingunkid, True, "")
            'Sohail (21 Aug 2015) -- End
            mdtTran = objBatchPostingTran._DataTable
            Call FillList()

            'If menAction = enAction.EDIT_ONE AndAlso lvEmpBatchPosting.SelectedItems.Count > 0 Then
            '    cboPeriod.SelectedValue = CInt(lvEmpBatchPosting.SelectedItems(0).SubItems(colhPeriod.Index).Tag)
            '    txtEmployeeCode.Text = lvEmpBatchPosting.SelectedItems(0).SubItems(colhEmpCode.Index).Text
            '    txtEmployeeCode.Tag = CInt(lvEmpBatchPosting.SelectedItems(0).SubItems(colhEmpCode.Index).Tag)
            '    txtTranHeadCode.Text = lvEmpBatchPosting.SelectedItems(0).SubItems(colhTranHeadCode.Index).Text
            '    txtTranHeadCode.Tag = CInt(lvEmpBatchPosting.SelectedItems(0).SubItems(colhTranHeadCode.Index).Tag)
            '    txtAmount.Text = lvEmpBatchPosting.SelectedItems(0).SubItems(colhAmount.Index).Text

            '    btnAdd.Enabled = False
            '    cboPeriod.Enabled = False
            '    txtEmployeeCode.Enabled = False
            '    txtTranHeadCode.Enabled = False
            '    objbtnSearchEmployee.Enabled = False
            '    objbtnSearchTranHead.Enabled = False
            'End If
            If menAction = enAction.EDIT_ONE AndAlso lvEmpBatchPosting.Items.Count > 0 Then
                cboPeriod.SelectedValue = CInt(lvEmpBatchPosting.Items(0).SubItems(colhPeriod.Index).Tag)
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetValue", mstrModuleName)
        End Try
    End Sub

    Private Sub SetValue()
        Try
            objBatchPosting._Batchno = txtBatchPostingNo.Text.Trim
            objBatchPosting._Isposted = False
            objBatchPosting._Userunkid = User._Object._Userunkid
            'Sohail (24 Feb 2016) -- Start
            'Enhancement - Batch Posting changes as per online doc in 58.1 SP. (#15 (KBC & Kenya Project Comments List.xls))
            'Sohail (14 Nov 2017) -- Start
            'Enhancement - 70.1 - Automate Batch Code generation,Add this to Number setting for both Auto increment or Manual generated. (Ref. No. : 119) | Hide Batch code option on Batch Posting Screen.
            'objBatchPosting._BatchCode = txtBatchCode.Text.Trim
            objBatchPosting._BatchCode = txtBatchPostingNo.Text.Trim
            'Sohail (14 Nov 2017) -- End
            objBatchPosting._BatchName = txtBatchName.Text.Trim
            objBatchPosting._Description = txtDescription.Text.Trim
            'Sohail (24 Feb 2016) -- End
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetValue", mstrModuleName)
        End Try
    End Sub
    Private Sub FillCombo()
        Dim objPeriod As New clscommom_period_Tran
        'Sohail (24 Feb 2016) -- Start
        'Enhancement - Batch Posting changes as per online doc in 58.1 SP. (#15 (KBC & Kenya Project Comments List.xls))
        Dim objMaster As New clsMasterData
        Dim objTranHead As New clsTransactionHead
        'Sohail (24 Feb 2016) -- End
        Dim dsCombo As DataSet
        Try
            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'dsCombo = objPeriod.getListForCombo(enModuleReference.Payroll, FinancialYear._Object._YearUnkid, "Period", True, enStatusType.Open)
            dsCombo = objPeriod.getListForCombo(enModuleReference.Payroll, FinancialYear._Object._YearUnkid, FinancialYear._Object._DatabaseName, FinancialYear._Object._Database_Start_Date, "Period", True, enStatusType.Open)
            'Sohail (21 Aug 2015) -- End
            With cboPeriod
                .ValueMember = "periodunkid"
                .DisplayMember = "name"
                .DataSource = dsCombo.Tables("Period")
                'Sohail (24 Feb 2016) -- Start
                'Enhancement - Batch Posting changes as per online doc in 58.1 SP. (#15 (KBC & Kenya Project Comments List.xls))
                '.SelectedValue = 0
                .SelectedValue = objMaster.getFirstPeriodID(enModuleReference.Payroll, FinancialYear._Object._YearUnkid, enStatusType.Open)
                'Sohail (24 Feb 2016) -- End
            End With

            'Sohail (24 Feb 2016) -- Start
            'Enhancement - Batch Posting changes as per online doc in 58.1 SP. (#15 (KBC & Kenya Project Comments List.xls))
            dsCombo = objTranHead.getComboList(FinancialYear._Object._DatabaseName, "TranHead", False, , enCalcType.FlatRate_Others, , , True, "typeof_id <> " & enTypeOf.Salary & "")
            With cboTranhead
                .ValueMember = "tranheadunkid"
                .DisplayMember = "name"
                .DataSource = dsCombo.Tables("TranHead")
                .SelectedIndex = -1
            End With
            'Sohail (24 Feb 2016) -- End

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
            'Sohail (24 Feb 2016) -- Start
            'Enhancement - Batch Posting changes as per online doc in 58.1 SP. (#15 (KBC & Kenya Project Comments List.xls))
        Finally
            objPeriod = Nothing
            objMaster = Nothing
            objTranHead = Nothing
            'Sohail (24 Feb 2016) -- End
        End Try
    End Sub

    Private Function IsValid() As Boolean
        Try
            'Sohail (19 Dec 2012) -- Start
            'TRA - ENHANCEMENT
            If ConfigParameter._Object._BatchPostingNotype = 0 AndAlso txtBatchPostingNo.Text.Trim = "" Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 8, "Sorry! Batch Posting No. cannot be blank. Batch Posting No. is mandatory information."), enMsgBoxStyle.Information)
                txtBatchPostingNo.Focus()
                Return False
                'Sohail (19 Dec 2012) -- End
            ElseIf CInt(cboPeriod.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Please select period."), enMsgBoxStyle.Information)
                cboPeriod.Focus()
                Return False
                'Sohail (24 Feb 2016) -- Start
                'Enhancement - Batch Posting changes as per online doc in 58.1 SP. (#15 (KBC & Kenya Project Comments List.xls))
                'ElseIf txtEmployeeCode.Text.Trim = "" Then
                '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Please enter employee code."), enMsgBoxStyle.Information)
                '    txtEmployeeCode.Focus()
                '    Return False
                'ElseIf txtTranHeadCode.Text.Trim = "" Then
                '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "Please enter transaction head code."), enMsgBoxStyle.Information)
                '    txtTranHeadCode.Focus()
                '    Return False
                'Sohail (14 Nov 2017) -- Start
                'Enhancement - 70.1 - Automate Batch Code generation,Add this to Number setting for both Auto increment or Manual generated. (Ref. No. : 119) | Hide Batch code option on Batch Posting Screen.
                'ElseIf txtBatchCode.Text.Trim = "" Then
                '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 10, "Please enter Batch code."), enMsgBoxStyle.Information)
                '    txtBatchCode.Focus()
                '    Return False
                'Sohail (14 Nov 2017) -- End
            ElseIf txtBatchName.Text.Trim = "" Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 11, "Please enter Batch name."), enMsgBoxStyle.Information)
                txtBatchName.Focus()
                Return False
            ElseIf CInt(cboEmployee.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Please select employee."), enMsgBoxStyle.Information)
                cboEmployee.Focus()
                Return False
            ElseIf CInt(cboTranhead.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "Please select transaction head."), enMsgBoxStyle.Information)
                cboTranhead.Focus()
                Return False
                'Sohail (24 Feb 2016) -- End
            ElseIf txtAmount.Decimal = 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 6, "Please enter amount."), enMsgBoxStyle.Information)
                txtAmount.Focus()
                Return False
            End If

            Return True

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "IsValid", mstrModuleName)
        End Try
    End Function

    Private Sub FillList()
        Dim decTotal As Decimal = 0 'Sohail (24 Feb 2016)
        Try
            lvEmpBatchPosting.Items.Clear()
            Dim lvArray As New List(Of ListViewItem)
            lvEmpBatchPosting.BeginUpdate()

            mdtTran = New DataView(mdtTran, "", "enddate, employeecode", DataViewRowState.CurrentRows).ToTable

            Dim lvItem As ListViewItem
            For Each dtRow As DataRow In mdtTran.Rows
                'Sohail (24 Feb 2016) -- Start
                'Enhancement - Batch Posting changes as per online doc in 58.1 SP. (#15 (KBC & Kenya Project Comments List.xls))
                'If CStr(IIf(IsDBNull(dtRow.Item("AUD")), "A", dtRow.Item("AUD"))) <> "D" AndAlso CBool(dtRow.Item("isposted")) = False Then
                If CStr(IIf(IsDBNull(dtRow.Item("AUD")), "A", dtRow.Item("AUD"))) <> "D" Then
                    'Sohail (24 Feb 2016) -- End
                    lvItem = New ListViewItem

                    lvItem.Text = dtRow.Item("empbatchpostingtranunkid").ToString
                    lvItem.Tag = dtRow.Item("empbatchpostingunkid")

                    lvItem.SubItems.Add(dtRow.Item("periodname").ToString)                                     'Period
                    lvItem.SubItems(colhPeriod.Index).Tag = CInt(dtRow.Item("periodunkid").ToString)           'PeriodUnkid

                    lvItem.SubItems.Add(dtRow.Item("employeecode").ToString)                                   'Employee
                    lvItem.SubItems(colhEmpCode.Index).Tag = CInt(dtRow.Item("employeeunkid").ToString)        'EmployeeUnkid



                    'Anjan [09 October 2014] -- Start
                    'ENHANCEMENT : Implementing Language,requested by Andrew
                    lvItem.SubItems.Add(dtRow.Item("EmpName").ToString)                                        'Employee name
                    'Anjan [09 October 2014] -- End


                    lvItem.SubItems.Add(dtRow.Item("trnheadcode").ToString)                                    'Tranhead Code
                    lvItem.SubItems(colhTranHeadCode.Index).Tag = CInt(dtRow.Item("tranheadunkid").ToString)   'Tranhead Unkid

                    'Sohail (24 Feb 2016) -- Start
                    'Enhancement - Batch Posting changes as per online doc in 58.1 SP. (#15 (KBC & Kenya Project Comments List.xls))
                    lvItem.SubItems.Add(dtRow.Item("TranHeadName").ToString)
                    'Sohail (24 Feb 2016) -- End

                    lvItem.SubItems.Add(Format(CDec(dtRow.Item("amount")), GUI.fmtCurrency))                   'amount
                    'Sohail (24 Feb 2016) -- Start
                    'Enhancement - Batch Posting changes as per online doc in 58.1 SP. (#15 (KBC & Kenya Project Comments List.xls))
                    decTotal += CDec(dtRow.Item("amount"))
                    'Sohail (24 Feb 2016) -- End

                    lvItem.SubItems.Add(dtRow.Item("GUID").ToString)                                           'GUID

                    'If CInt(dtRow.Item("empbatchpostingtranunkid")) = mintBatchPostingTranUnkid Then
                    '    lvItem.Selected = True
                    '    lvItem.EnsureVisible()
                    'End If
                    lvArray.Add(lvItem)

                    lvItem = Nothing
                End If
            Next
            RemoveHandler lvEmpBatchPosting.SelectedIndexChanged, AddressOf lvEmpBatchPosting_SelectedIndexChanged
            lvEmpBatchPosting.Items.AddRange(lvArray.ToArray)
            AddHandler lvEmpBatchPosting.SelectedIndexChanged, AddressOf lvEmpBatchPosting_SelectedIndexChanged

            'lvEmpBatchPosting.GroupingColumn = colhPeriod
            'lvEmpBatchPosting.DisplayGroups(True)
            'lvEmpBatchPosting.GridLines = False

            lvEmpBatchPosting.EndUpdate()

            If lvEmpBatchPosting.SelectedItems.Count > 0 Then lvEmpBatchPosting.EnsureVisible(lvEmpBatchPosting.SelectedItems(0).Index)

            If mdtTran.Rows.Count > 0 Then
                txtBatchPostingNo.Enabled = False
                cboPeriod.Enabled = False
            End If

            'Sohail (24 Feb 2016) -- Start
            'Enhancement - Batch Posting changes as per online doc in 58.1 SP. (#15 (KBC & Kenya Project Comments List.xls))
            'If lvEmpBatchPosting.Items.Count > 11 Then
            '    colhEmpCode.Width = 190 - 18
            'Else
            '    colhEmpCode.Width = 190
            'End If
            'Sohail (24 Feb 2016) -- End

            Call ResetValue()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillList", mstrModuleName)
            'Sohail (24 Feb 2016) -- Start
            'Enhancement - Batch Posting changes as per online doc in 58.1 SP. (#15 (KBC & Kenya Project Comments List.xls))
        Finally
            txtTotal.Text = Format(decTotal, GUI.fmtCurrency)
            'Sohail (24 Feb 2016) -- End
        End Try
    End Sub

    Private Sub ResetValue()
        Try
            'txtEmployeeCode.Text = ""
            'txtTranHeadCode.Text = "" 'Sohail (24 Feb 2016)
            txtAmount.Decimal = 0
            btnAdd.Enabled = True
            'cboPeriod.Enabled = True
            'Sohail (24 Feb 2016) -- Start
            'Enhancement - Batch Posting changes as per online doc in 58.1 SP. (#15 (KBC & Kenya Project Comments List.xls))
            'txtEmployeeCode.Enabled = True
            'txtTranHeadCode.Enabled = True
            'objbtnSearchEmployee.Enabled = True
            'objbtnSearchTranHead.Enabled = True
            cboEmployee.Enabled = True
            cboTranhead.Enabled = True
            'Sohail (24 Feb 2016) -- End
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "ResetValue", mstrModuleName)
        End Try
    End Sub

    Private Sub SetVisibility()
        Try
            'Sohail (19 Dec 2012) -- Start
            'TRA - ENHANCEMENT - Auto No. Generation
            If ConfigParameter._Object._BatchPostingNotype = 1 Then
                txtBatchPostingNo.Enabled = False
            Else
                txtBatchPostingNo.Enabled = True
            End If
            'Sohail (19 Dec 2012) -- End

            btnAdd.Visible = User._Object.Privilege._AllowToAddBatchPosting
            btnEdit.Visible = User._Object.Privilege._AllowToEditBatchPosting
            btnDelete.Visible = User._Object.Privilege._AllowToDeleteBatchPosting

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetVisibility", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Form's Events "
    Private Sub frmBatchPosting_AddEdit_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        Try
            objBatchPosting = Nothing
            objBatchPostingTran = Nothing
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmBatchPosting_AddEdit_FormClosed", mstrModuleName)
        End Try
    End Sub

    Private Sub frmBatchPosting_AddEdit_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown
        Try
            If e.KeyCode = Keys.Return Then
                SendKeys.Send("{TAB}")
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmBatchPosting_AddEdit_KeyDown", mstrModuleName)
        End Try
    End Sub

    Private Sub frmBatchPosting_AddEdit_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        objBatchPosting = New clsBatchPosting
        objBatchPostingTran = New clsBatchPostingTran
        Try
            Call Set_Logo(Me, gApplicationType)
            Language.setLanguage(Me.Name)
            Call OtherSettings()

            Call SetVisibility()
            Call SetColor()
            Call FillCombo()

            If menAction = enAction.EDIT_ONE Then
                objBatchPosting._Empbatchpostingunkid = mintBatchPostingUnkid
            Else
                cboPeriod.Focus()
            End If

            Call GetValue()

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmBatchPosting_AddEdit_Load", mstrModuleName)
        End Try
    End Sub

    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsBatchPosting.SetMessages()
            'clsBatchPostingTran.SetMessages()
            objfrm._Other_ModuleNames = "clsBatchPosting,clsBatchPostingTran"
            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub
#End Region

#Region " Button's Events "

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnClose_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAdd.Click
        Try
            If IsValid() = False Then Exit Sub

            'Sohail (24 Feb 2016) -- Start
            'Enhancement - Batch Posting changes as per online doc in 58.1 SP. (#15 (KBC & Kenya Project Comments List.xls))
            'Dim dtRow As DataRow() = mdtTran.Select("periodunkid = " & CInt(cboPeriod.SelectedValue) & " AND employeeunkid = " & CInt(txtEmployeeCode.Tag) & " AND tranheadunkid = " & CInt(txtTranHeadCode.Tag) & " AND (AUD <> 'D' OR AUD IS NULL) ")
            Dim dtRow As DataRow() = mdtTran.Select("periodunkid = " & CInt(cboPeriod.SelectedValue) & " AND employeeunkid = " & CInt(cboEmployee.SelectedValue) & " AND tranheadunkid = " & CInt(cboTranhead.SelectedValue) & " AND (AUD <> 'D' OR AUD IS NULL) ")
            'Sohail (24 Feb 2016) -- End

            If dtRow.Length > 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 9, "Sorry! Selected Transaction Head for selected Employee is already added to the list for selected period."), enMsgBoxStyle.Information)
                Exit Sub
            End If

            Dim dtEBRow As DataRow
            dtEBRow = mdtTran.NewRow
            With dtEBRow

                .Item("empbatchpostingtranunkid") = -1
                .Item("empbatchpostingunkid") = mintBatchPostingUnkid
                .Item("batchno") = txtBatchPostingNo.Text.Trim
                .Item("periodunkid") = CInt(cboPeriod.SelectedValue)
                .Item("periodname") = cboPeriod.Text
                'Sohail (24 Feb 2016) -- Start
                'Enhancement - Batch Posting changes as per online doc in 58.1 SP. (#15 (KBC & Kenya Project Comments List.xls))
                '.Item("employeeunkid") = CInt(txtEmployeeCode.Tag)
                '.Item("employeecode") = txtEmployeeCode.Text
                '.Item("tranheadunkid") = CInt(txtTranHeadCode.Tag)
                '.Item("trnheadcode") = txtTranHeadCode.Text
                .Item("employeeunkid") = CInt(cboEmployee.SelectedValue)
                .Item("employeecode") = cboEmployee.Tag.ToString
                .Item("tranheadunkid") = CInt(cboTranhead.SelectedValue)
                .Item("trnheadcode") = cboTranhead.Tag.ToString
                .Item("TranHeadName") = cboTranhead.Text
                'Sohail (24 Feb 2016) -- End
                .Item("amount") = CDec(Format(txtAmount.Decimal, GUI.fmtCurrency))
                .Item("isposted") = False
                .Item("enddate") = eZeeDate.convertDate(mdtPeriodEnd)
                .Item("userunkid") = User._Object._Userunkid
                'Anjan [09 October 2014] -- Start
                'ENHANCEMENT : included Employee name column,requested by Rutta in Email - Item #405.
                'Sohail (24 Feb 2016) -- Start
                'Enhancement - Batch Posting changes as per online doc in 58.1 SP. (#15 (KBC & Kenya Project Comments List.xls))
                '.Item("EmpName") = objbtnSearchEmployee.Tag.ToString
                .Item("EmpName") = cboEmployee.Text
                'Sohail (24 Feb 2016) -- End
                'Anjan [09 October 2014] -- End

                dtEBRow.Item("AUD") = "A"
                dtEBRow.Item("GUID") = Guid.NewGuid().ToString
            End With

            mdtTran.Rows.Add(dtEBRow)

            Call FillList()

            'Sohail (24 Feb 2016) -- Start
            'Enhancement - Batch Posting changes as per online doc in 58.1 SP. (#15 (KBC & Kenya Project Comments List.xls))
            'txtEmployeeCode.Focus()
            cboEmployee.Focus()
            'Sohail (24 Feb 2016) -- End

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnAdd_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnEdit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnEdit.Click
        Try
            If lvEmpBatchPosting.SelectedItems.Count > 0 Then
                Dim drTemp As DataRow()
                If CInt(lvEmpBatchPosting.Items(CInt(lvEmpBatchPosting.SelectedItems(0).Index)).Text) = -1 Then
                    drTemp = mdtTran.Select("GUID = '" & lvEmpBatchPosting.Items(CInt(lvEmpBatchPosting.SelectedItems(0).Index)).SubItems(colhGUID.Index).Text & "'")
                Else
                    drTemp = mdtTran.Select("empbatchpostingtranunkid = " & CInt(lvEmpBatchPosting.Items(CInt(lvEmpBatchPosting.SelectedItems(0).Index)).Text))
                End If
                If drTemp.Length > 0 Then
                    If IsValid() = False Then Exit Sub

                    With drTemp(0)
                        .Item("empbatchpostingtranunkid") = CInt(lvEmpBatchPosting.Items(CInt(lvEmpBatchPosting.SelectedItems(0).Index)).Text)
                        .Item("empbatchpostingunkid") = mintBatchPostingUnkid
                        .Item("batchno") = txtBatchPostingNo.Text.Trim
                        .Item("periodunkid") = CInt(cboPeriod.SelectedValue)
                        .Item("periodname") = cboPeriod.Text
                        'Sohail (24 Feb 2016) -- Start
                        'Enhancement - Batch Posting changes as per online doc in 58.1 SP. (#15 (KBC & Kenya Project Comments List.xls))
                        '.Item("employeeunkid") = CInt(txtEmployeeCode.Tag)
                        '.Item("employeecode") = txtEmployeeCode.Text
                        '.Item("tranheadunkid") = CInt(txtTranHeadCode.Tag)
                        '.Item("trnheadcode") = txtTranHeadCode.Text
                        .Item("employeeunkid") = CInt(cboEmployee.SelectedValue)
                        .Item("employeecode") = cboEmployee.Tag.ToString
                        .Item("tranheadunkid") = CInt(cboTranhead.SelectedValue)
                        .Item("trnheadcode") = cboTranhead.Tag.ToString
                        .Item("TranHeadName") = cboTranhead.Text
                        'Sohail (24 Feb 2016) -- End
                        .Item("amount") = CDec(Format(txtAmount.Decimal, GUI.fmtCurrency))
                        .Item("isposted") = False
                        .Item("enddate") = eZeeDate.convertDate(mdtPeriodEnd)
                        .Item("userunkid") = User._Object._Userunkid
                        'Anjan [09 October 2014] -- Start
                        'ENHANCEMENT : included Employee name column,requested by Rutta in Email - Item #405.
                        'Sohail (24 Feb 2016) -- Start
                        'Enhancement - Batch Posting changes as per online doc in 58.1 SP. (#15 (KBC & Kenya Project Comments List.xls))
                        '.Item("EmpName") = objbtnSearchEmployee.Tag.ToString
                        .Item("EmpName") = cboEmployee.Tag.ToString
                        'Sohail (24 Feb 2016) -- End
                        'Anjan [09 October 2014] -- End

                        If IsDBNull(.Item("AUD")) OrElse CStr(.Item("AUD")).ToString.Trim = "" Then
                            .Item("AUD") = "U"
                        End If
                        .Item("GUID") = Guid.NewGuid().ToString
                        .AcceptChanges()
                    End With
                    Call FillList()
                End If

                'If mdtTran.Rows.Count > 0 Then
                '    cboPeriod.Enabled = False
                'Else
                '    cboPeriod.Enabled = True
                'End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnEdit_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDelete.Click
        Try
            If lvEmpBatchPosting.SelectedItems.Count > 0 Then
                Dim drTemp As DataRow()
                If CInt(lvEmpBatchPosting.Items(CInt(lvEmpBatchPosting.SelectedItems(0).Index)).Text) = -1 Then
                    drTemp = mdtTran.Select("GUID = '" & lvEmpBatchPosting.Items(CInt(lvEmpBatchPosting.SelectedItems(0).Index)).SubItems(colhGUID.Index).Text & "'")
                Else
                    drTemp = mdtTran.Select("empbatchpostingtranunkid = " & CInt(lvEmpBatchPosting.Items(CInt(lvEmpBatchPosting.SelectedItems(0).Index)).Text) & "")
                End If
                If drTemp.Length > 0 Then
                    drTemp(0).Item("AUD") = "D"
                    Call FillList()
                End If

                'If mdtTran.Rows.Count > 0 Then
                '    cboPeriod.Enabled = False
                'Else
                '    cboPeriod.Enabled = True
                'End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnDelete_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Dim blnFlag As Boolean = False
        Try
            If lvEmpBatchPosting.Items.Count <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 7, "Please add atleast one employee batch posting transaction."), enMsgBoxStyle.Information)
                Exit Sub
                'Sohail (24 Feb 2016) -- Start
                'Enhancement - Batch Posting changes as per online doc in 58.1 SP. (#15 (KBC & Kenya Project Comments List.xls))
                'Sohail (14 Nov 2017) -- Start
                'Enhancement - 70.1 - Automate Batch Code generation,Add this to Number setting for both Auto increment or Manual generated. (Ref. No. : 119) | Hide Batch code option on Batch Posting Screen.
                'ElseIf txtBatchCode.Text.Trim = "" Then
                '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 10, "Please enter Batch code."), enMsgBoxStyle.Information)
                '    txtBatchCode.Focus()
                '    Exit Sub
                'Sohail (14 Nov 2017) -- End
            ElseIf txtBatchName.Text.Trim = "" Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 11, "Please enter Batch name."), enMsgBoxStyle.Information)
                txtBatchName.Focus()
                Exit Sub
                'Sohail (24 Feb 2016) -- End
            End If

            Call SetValue()
            'S.SANDEEP [28-May-2018] -- START
            'ISSUE/ENHANCEMENT : {Audit Trails} 
            objBatchPosting._FormName = mstrModuleName
            objBatchPosting._LoginEmployeeunkid = 0
            objBatchPosting._ClientIP = getIP()
            objBatchPosting._HostName = getHostName()
            objBatchPosting._FromWeb = False
            objBatchPosting._AuditUserId = User._Object._Userunkid
objBatchPosting._CompanyUnkid = Company._Object._Companyunkid
            objBatchPosting._AuditDate = ConfigParameter._Object._CurrentDateAndTime
            'S.SANDEEP [28-May-2018] -- END
            If menAction = enAction.EDIT_ONE Then
                'Sohail (24 Feb 2016) -- Start
                'Enhancement - Batch Posting changes as per online doc in 58.1 SP. (#15 (KBC & Kenya Project Comments List.xls))
                If mblnIsPosted = True Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 12, "Sorry, you cannot Update this Batch transaction. Reason: This Batch is already Posted to Payroll."), enMsgBoxStyle.Information)
                    Exit Sub
                End If
                'Sohail (24 Feb 2016) -- End
                'Sohail (21 Aug 2015) -- Start
                'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                'blnFlag = objBatchPosting.Update(mdtTran)
                blnFlag = objBatchPosting.Update(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, mdtPeriodStart, mdtPeriodEnd, ConfigParameter._Object._UserAccessModeSetting, True, ConfigParameter._Object._IsIncludeInactiveEmp, mdtTran, "List", True, "")
                'Sohail (21 Aug 2015) -- End
            Else
                'Sohail (21 Aug 2015) -- Start
                'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                'blnFlag = objBatchPosting.Insert(mdtTran)
                blnFlag = objBatchPosting.Insert(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, mdtPeriodStart, mdtPeriodEnd, ConfigParameter._Object._UserAccessModeSetting, True, ConfigParameter._Object._IsIncludeInactiveEmp, mdtTran, ConfigParameter._Object._BatchPostingNotype, ConfigParameter._Object._BatchPostingPrifix, "List", True)
                'Sohail (21 Aug 2015) -- End
            End If


            If blnFlag = False And objBatchPosting._Message <> "" Then
                eZeeMsgBox.Show(objBatchPosting._Message, enMsgBoxStyle.Information)
            End If

            If blnFlag Then
                mblnCancel = False
                If menAction = enAction.ADD_CONTINUE Then
                    objBatchPosting = Nothing
                    objBatchPosting = New clsBatchPosting
                    Call GetValue()
                    lvEmpBatchPosting.Items.Clear()
                    txtBatchPostingNo.Focus()
                Else
                    mintBatchPostingUnkid = objBatchPosting._Empbatchpostingunkid
                    Me.Close()
                End If
            End If

            If blnFlag Then
                mblnCancel = False
                Me.Close()
            End If



        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnSave_Click", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Combobox's Events "
    Private Sub cboPeriod_SelectedValueChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboPeriod.SelectedValueChanged
        Dim objPeriod As New clscommom_period_Tran
        'Sohail (24 Feb 2016) -- Start
        'Enhancement - Batch Posting changes as per online doc in 58.1 SP. (#15 (KBC & Kenya Project Comments List.xls))
        Dim objEmp As New clsEmployee_Master
        Dim dsCombo As DataSet
        'Sohail (24 Feb 2016) -- End
        Try
            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'objPeriod._Periodunkid = CInt(cboPeriod.SelectedValue)
            objPeriod._Periodunkid(FinancialYear._Object._DatabaseName) = CInt(cboPeriod.SelectedValue)
            'Sohail (21 Aug 2015) -- End
            mdtPeriodStart = objPeriod._Start_Date
            mdtPeriodEnd = objPeriod._End_Date
            'Sohail (24 Feb 2016) -- Start
            'Enhancement - Batch Posting changes as per online doc in 58.1 SP. (#15 (KBC & Kenya Project Comments List.xls))
            'txtEmployeeCode.Text = ""
            If CInt(cboPeriod.SelectedValue) <= 0 Then
                mdtPeriodStart = eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate)
                mdtPeriodEnd = eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate)
            End If
            cboEmployee.DataSource = Nothing
            dsCombo = objEmp.GetEmployeeList(FinancialYear._Object._DatabaseName, _
                                          User._Object._Userunkid, _
                                          FinancialYear._Object._YearUnkid, _
                                          Company._Object._Companyunkid, _
                                          mdtPeriodStart, _
                                          mdtPeriodEnd, _
                                          ConfigParameter._Object._UserAccessModeSetting, _
                                          True, False, "Emp", False)
            With cboEmployee
                .DisplayMember = "employeename"
                .ValueMember = "employeeunkid"
                .DataSource = dsCombo.Tables("Emp")
                .SelectedIndex = -1
            End With
            'Sohail (24 Feb 2016) -- End

            'objBatchPosting._PeriodUnkId = CInt(cboPeriod.SelectedValue)
            'mdtTran = objBatchPosting._DataTable

            'If menAction = enAction.EDIT_ONE AndAlso mintBatchPostingTranUnkid > 0 Then
            '    mdtTran = New DataView(mdtTran, "empbatchpostingtranunkid = " & mintBatchPostingTranUnkid & " ", "", DataViewRowState.CurrentRows).ToTable
            'End If
            'Call FillList()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboPeriod_SelectedValueChanged", mstrModuleName)
            'Sohail (24 Feb 2016) -- Start
            'Enhancement - Batch Posting changes as per online doc in 58.1 SP. (#15 (KBC & Kenya Project Comments List.xls))
        Finally
            objPeriod = Nothing
            objEmp = Nothing
            'Sohail (24 Feb 2016) -- End
        End Try
    End Sub

    'Sohail (24 Feb 2016) -- Start
    'Enhancement - Batch Posting changes as per online doc in 58.1 SP. (#15 (KBC & Kenya Project Comments List.xls))
    Private Sub cboPeriod_KeyPress(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles cboPeriod.KeyPress
        Try
            If (CInt(AscW(e.KeyChar)) >= 65 AndAlso CInt(AscW(e.KeyChar)) <= 90) Or (CInt(AscW(e.KeyChar)) >= 97 AndAlso CInt(AscW(e.KeyChar)) <= 122) Or (CInt(AscW(e.KeyChar)) >= 47 AndAlso CInt(AscW(e.KeyChar)) <= 57) Then
                Dim frm As New frmCommonSearch
                With frm
                    .ValueMember = cboPeriod.ValueMember
                    .DisplayMember = cboPeriod.DisplayMember
                    .DataSource = CType(cboPeriod.DataSource, DataTable)
                    .CodeMember = "code"
                End With
                Dim c As Char = Convert.ToChar(e.KeyChar)
                frm.TypedText = c.ToString
                If frm.DisplayDialog Then
                    cboPeriod.SelectedValue = frm.SelectedValue
                    e.KeyChar = ChrW(Keys.ShiftKey)
                Else
                    cboPeriod.Text = ""
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboPeriod_KeyPress", mstrModuleName)
        End Try
    End Sub

    Private Sub cboEmployee_KeyPress(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles cboEmployee.KeyPress
        Try
            If (CInt(AscW(e.KeyChar)) >= 65 AndAlso CInt(AscW(e.KeyChar)) <= 90) Or (CInt(AscW(e.KeyChar)) >= 97 AndAlso CInt(AscW(e.KeyChar)) <= 122) Or (CInt(AscW(e.KeyChar)) >= 47 AndAlso CInt(AscW(e.KeyChar)) <= 57) Then
                Dim frm As New frmCommonSearch
                With frm
                    .ValueMember = cboEmployee.ValueMember
                    .DisplayMember = cboEmployee.DisplayMember
                    .DataSource = CType(cboEmployee.DataSource, DataTable)
                    .CodeMember = "employeecode"
                End With
                Dim c As Char = Convert.ToChar(e.KeyChar)
                frm.TypedText = c.ToString
                If frm.DisplayDialog Then
                    cboEmployee.SelectedValue = frm.SelectedValue
                    cboEmployee.Tag = frm.SelectedAlias
                    e.KeyChar = ChrW(Keys.ShiftKey)
                Else
                    cboEmployee.Text = ""
                    cboEmployee.Tag = ""
                    'e.KeyChar = Nothing
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboEmployee_KeyPress", mstrModuleName)
        End Try
    End Sub

    Private Sub cboTranhead_KeyPress(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles cboTranhead.KeyPress
        Try
            If (CInt(AscW(e.KeyChar)) >= 65 AndAlso CInt(AscW(e.KeyChar)) <= 90) Or (CInt(AscW(e.KeyChar)) >= 97 AndAlso CInt(AscW(e.KeyChar)) <= 122) Or (CInt(AscW(e.KeyChar)) >= 47 AndAlso CInt(AscW(e.KeyChar)) <= 57) Then
                Dim frm As New frmCommonSearch
                With frm
                    .ValueMember = cboTranhead.ValueMember
                    .DisplayMember = cboTranhead.DisplayMember
                    .DataSource = CType(cboTranhead.DataSource, DataTable)
                    .CodeMember = "code"
                End With
                Dim c As Char = Convert.ToChar(e.KeyChar)
                frm.TypedText = c.ToString
                If frm.DisplayDialog Then
                    cboTranhead.SelectedValue = frm.SelectedValue
                    cboTranhead.Tag = frm.SelectedAlias
                    e.KeyChar = ChrW(Keys.ShiftKey)
                Else
                    cboTranhead.Text = ""
                    cboTranhead.Tag = ""
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboTranhead_KeyPress", mstrModuleName)
        End Try
    End Sub
    'Sohail (24 Feb 2016) -- End

#End Region

#Region " Textbox's Events "

    'Sohail (24 Feb 2016) -- Start
    'Enhancement - Batch Posting changes as per online doc in 58.1 SP. (#15 (KBC & Kenya Project Comments List.xls))
    'Private Sub txtEmployeeCode_Validating(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs)
    '    Dim objEmp As New clsEmployee_Master
    '    Dim dsCombo As DataSet
    '    Try
    '        If txtEmployeeCode.Text.Trim = "" Then Exit Try


    '        'Anjan [10 June 2015] -- Start
    '        'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
    '        'dsCombo = objEmp.GetEmployeeList("employee", False, , , , , , , , , , , , , mdtPeriodStart, mdtPeriodEnd, , , , , "employeecode = '" & txtEmployeeCode.Text & "'", True)

    '        'Nilay (10-Feb-2016) -- Start
    '        'dsCombo = objEmp.GetEmployeeList(FinancialYear._Object._DatabaseName, _
    '        '                               User._Object._Userunkid, _
    '        '                               FinancialYear._Object._YearUnkid, _
    '        '                               Company._Object._Companyunkid, _
    '        '                               mdtPeriodStart, _
    '        '                               mdtPeriodEnd, _
    '        '                               ConfigParameter._Object._UserAccessModeSetting, _
    '        '                               True, False, "employee", False, , , , , , , , , , , , , , , "employeecode = '" & txtEmployeeCode.Text & "'")

    '        Dim strFilter As String = "hremployee_master.employeecode = '" & txtEmployeeCode.Text & "'"
    '        dsCombo = objEmp.GetEmployeeList(FinancialYear._Object._DatabaseName, _
    '                                       User._Object._Userunkid, _
    '                                       FinancialYear._Object._YearUnkid, _
    '                                       Company._Object._Companyunkid, _
    '                                       mdtPeriodStart, _
    '                                       mdtPeriodEnd, _
    '                                       ConfigParameter._Object._UserAccessModeSetting, _
    '                                       True, False, "employee", False, , , , , , , , , , , , , , , strFilter)
    '        'Nilay (10-Feb-2016) -- End

    '        'Anjan [10 June 2015] -- End

    '        If dsCombo.Tables("employee").Rows.Count <= 0 Then
    '            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please enter proper employee code."), enMsgBoxStyle.Information)
    '            txtEmployeeCode.Focus()
    '            e.Cancel = True
    '            Exit Try
    '        Else
    '            txtEmployeeCode.Tag = CInt(dsCombo.Tables("employee").Rows(0).Item("employeeunkid"))
    '            'Anjan [09 October 2014] -- Start
    '            'ENHANCEMENT : included Employee name column,requested by Rutta in Email - Item #405.
    '            objbtnSearchEmployee.Tag = dsCombo.Tables("employee").Rows(0).Item("employeename").ToString
    '            'Anjan [09 October 2014] -- End
    '        End If

    '    Catch ex As Exception
    '        DisplayError.Show("-1", ex.Message, "txtEmployeeCode_Validating", mstrModuleName)
    '    Finally
    '        objEmp = Nothing
    '    End Try
    'End Sub

    'Private Sub txtTranHeadCode_Validating(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs)
    '    Dim objTranHead As New clsTransactionHead
    '    Dim dsCombo As DataSet
    '    Dim dtTable As DataTable
    '    Try
    '        If txtTranHeadCode.Text.Trim = "" Then Exit Try

    '        'Sohail (21 Aug 2015) -- Start
    '        'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
    '        'dsCombo = objTranHead.getComboList("TranHead", False, , enCalcType.FlatRate_Others, , , True)
    '        dsCombo = objTranHead.getComboList(FinancialYear._Object._DatabaseName, "TranHead", False, , enCalcType.FlatRate_Others, , , True)
    '        'Sohail (21 Aug 2015) -- End
    '        dtTable = New DataView(dsCombo.Tables("TranHead"), "typeof_id <> " & enTypeOf.Salary & " AND code = '" & txtTranHeadCode.Text & "'", "", DataViewRowState.CurrentRows).ToTable

    '        If dtTable.Rows.Count <= 0 Then
    '            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Please enter proper Flat Rate transaction head code."), enMsgBoxStyle.Information)
    '            txtTranHeadCode.Focus()
    '            e.Cancel = True
    '            Exit Try
    '        Else
    '            txtTranHeadCode.Tag = CInt(dtTable.Rows(0).Item("tranheadunkid"))
    '        End If

    '    Catch ex As Exception
    '        DisplayError.Show("-1", ex.Message, "txtTranHeadCode_Validating", mstrModuleName)
    '    Finally
    '        objTranHead = Nothing
    '    End Try
    'End Sub
    'Sohail (24 Feb 2016) -- End
#End Region

#Region " Listview Events "
    Private Sub lvEmpBatchPosting_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles lvEmpBatchPosting.SelectedIndexChanged
        Try
            If lvEmpBatchPosting.SelectedItems.Count > 0 Then

                cboPeriod.SelectedValue = CInt(lvEmpBatchPosting.SelectedItems(0).SubItems(colhPeriod.Index).Tag)
                'Sohail (24 Feb 2016) -- Start
                'Enhancement - Batch Posting changes as per online doc in 58.1 SP. (#15 (KBC & Kenya Project Comments List.xls))
                'txtEmployeeCode.Text = lvEmpBatchPosting.SelectedItems(0).SubItems(colhEmpCode.Index).Text
                'txtEmployeeCode.Tag = CInt(lvEmpBatchPosting.SelectedItems(0).SubItems(colhEmpCode.Index).Tag)
                'txtTranHeadCode.Text = lvEmpBatchPosting.SelectedItems(0).SubItems(colhTranHeadCode.Index).Text
                'txtTranHeadCode.Tag = CInt(lvEmpBatchPosting.SelectedItems(0).SubItems(colhTranHeadCode.Index).Tag)
                cboEmployee.SelectedValue = CInt(lvEmpBatchPosting.SelectedItems(0).SubItems(colhEmpCode.Index).Tag)
                cboEmployee.Tag = lvEmpBatchPosting.SelectedItems(0).SubItems(colhEmpCode.Index).Text
                cboTranhead.SelectedValue = CInt(lvEmpBatchPosting.SelectedItems(0).SubItems(colhTranHeadCode.Index).Tag)
                cboTranhead.Tag = lvEmpBatchPosting.SelectedItems(0).SubItems(colhTranHeadCode.Index).Text
                'Sohail (24 Feb 2016) -- End
                txtAmount.Text = lvEmpBatchPosting.SelectedItems(0).SubItems(colhAmount.Index).Text
                'objbtnSearchEmployee.Tag = lvEmpBatchPosting.SelectedItems(0).SubItems(colhEmpName.Index).Text 'Sohail (23 Apr 2015) 'Sohail (24 Feb 2016)

                btnAdd.Enabled = False
                cboPeriod.Enabled = False
                'Sohail (24 Feb 2016) -- Start
                'Enhancement - Batch Posting changes as per online doc in 58.1 SP. (#15 (KBC & Kenya Project Comments List.xls))
                'txtEmployeeCode.Enabled = False
                'txtTranHeadCode.Enabled = False
                'objbtnSearchEmployee.Enabled = False
                'objbtnSearchTranHead.Enabled = False
                cboEmployee.Enabled = False
                cboTranhead.Enabled = False
                'Sohail (24 Feb 2016) -- End
            Else
                Call ResetValue()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lvEmpBatchPosting_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub
#End Region

#Region " Other Controls's Events "
    'Sohail (24 Feb 2016) -- Start
    'Enhancement - Batch Posting changes as per online doc in 58.1 SP. (#15 (KBC & Kenya Project Comments List.xls))
    'Private Sub objbtnSearchEmployee_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
    '    Dim objFrm As New frmCommonSearch
    '    Dim objEmp As New clsEmployee_Master
    '    Dim dsCombo As DataSet
    '    Try
    '        If User._Object._Isrighttoleft = True Then
    '            objFrm.RightToLeft = Windows.Forms.RightToLeft.Yes
    '            objFrm.RightToLeftLayout = True
    '            Call Language.ctlRightToLeftlayOut(objFrm)
    '        End If



    '        'Anjan [10 June 2015] -- Start
    '        'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
    '        'dsCombo = objEmp.GetEmployeeList("Emp", False, , , , , , , , , , , , , mdtPeriodStart, mdtPeriodEnd)

    '        dsCombo = objEmp.GetEmployeeList(FinancialYear._Object._DatabaseName, _
    '                                       User._Object._Userunkid, _
    '                                       FinancialYear._Object._YearUnkid, _
    '                                       Company._Object._Companyunkid, _
    '                                       mdtPeriodStart, _
    '                                       mdtPeriodEnd, _
    '                                       ConfigParameter._Object._UserAccessModeSetting, _
    '                                       True, False, "Emp", False)
    '        'Anjan [10 June 2015] -- End

    '        With objFrm
    '            .DataSource = dsCombo.Tables("Emp")
    '            .ValueMember = "employeeunkid"
    '            .DisplayMember = "employeename"
    '            .CodeMember = "employeecode"
    '        End With

    '        If objFrm.DisplayDialog Then
    '            txtEmployeeCode.Text = objFrm.SelectedAlias
    '            txtEmployeeCode.Tag = CInt(objFrm.SelectedValue)
    '            'Anjan [09 October 2014] -- Start
    '            'ENHANCEMENT : included Employee name column,requested by Rutta in Email - Item #405.
    '            objbtnSearchEmployee.Tag = objFrm.SelectedText
    '            'Anjan [09 October 2014] -- End
    '        End If

    '    Catch ex As Exception
    '        DisplayError.Show("-1", ex.Message, "objbtnSearchEmployee_Click", mstrModuleName)
    '    Finally
    '        objEmp = Nothing
    '    End Try
    'End Sub

    'Private Sub objbtnSearchTranHead_Click(ByVal sender As Object, ByVal e As System.EventArgs)
    '    Dim objTranHead As New clsTransactionHead
    '    Dim objFrm As New frmCommonSearch
    '    Dim dsCombo As DataSet
    '    Dim dtTable As DataTable
    '    Try
    '        If User._Object._Isrighttoleft = True Then
    '            objFrm.RightToLeft = Windows.Forms.RightToLeft.Yes
    '            objFrm.RightToLeftLayout = True
    '            Call Language.ctlRightToLeftlayOut(objFrm)
    '        End If

    '        'Sohail (21 Aug 2015) -- Start
    '        'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
    '        'dsCombo = objTranHead.getComboList("TranHead", False, , enCalcType.FlatRate_Others, , , True)
    '        dsCombo = objTranHead.getComboList(FinancialYear._Object._DatabaseName, "TranHead", False, , enCalcType.FlatRate_Others, , , True)
    '        'Sohail (21 Aug 2015) -- End
    '        dtTable = New DataView(dsCombo.Tables("TranHead"), "typeof_id <> " & enTypeOf.Salary & "", "", DataViewRowState.CurrentRows).ToTable
    '        With objFrm
    '            .DataSource = dtTable
    '            .ValueMember = "tranheadunkid"
    '            .DisplayMember = "name"
    '            .CodeMember = "code"
    '        End With

    '        If objFrm.DisplayDialog Then
    '            txtTranHeadCode.Text = objFrm.SelectedAlias
    '            txtTranHeadCode.Tag = CInt(objFrm.SelectedValue)
    '        End If
    '    Catch ex As Exception
    '        DisplayError.Show("-1", ex.Message, "objbtnSearchTranHead_Click", mstrModuleName)
    '    End Try
    'End Sub
    'Sohail (24 Feb 2016) -- End

    Private Sub txtAmount_Leave(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtAmount.Leave
        Try
            btnAdd.Focus()
            If btnAdd.Enabled = False Then btnEdit.Focus()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "", mstrModuleName)
        End Try
    End Sub
#End Region


	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Private Sub OtherSettings()
		Try
			Me.SuspendLayout()
			
			Call SetLanguage()
			
			Me.gbBatchPosting.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbBatchPosting.ForeColor = GUI._eZeeContainerHeaderForeColor 


			
			Me.eZeeHeader.GradientColor1 = GUI._HeaderBackColor1 
			Me.eZeeHeader.GradientColor2 = GUI._HeaderBackColor2 
			Me.eZeeHeader.BorderColor = GUI._HeaderBorderColor 
			Me.eZeeHeader.HeaderTextForeColor = GUI._HeaderTitleFontColor 
			Me.eZeeHeader.DescriptionForeColor = GUI._HeaderMessageFontColor 


			Me.btnDelete.GradientBackColor = GUI._ButttonBackColor 
			Me.btnDelete.GradientForeColor = GUI._ButttonFontColor

			Me.btnEdit.GradientBackColor = GUI._ButttonBackColor 
			Me.btnEdit.GradientForeColor = GUI._ButttonFontColor

			Me.btnAdd.GradientBackColor = GUI._ButttonBackColor 
			Me.btnAdd.GradientForeColor = GUI._ButttonFontColor

			Me.btnSave.GradientBackColor = GUI._ButttonBackColor 
			Me.btnSave.GradientForeColor = GUI._ButttonFontColor

			Me.btnClose.GradientBackColor = GUI._ButttonBackColor 
			Me.btnClose.GradientForeColor = GUI._ButttonFontColor


			Me.ResumeLayout()
		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "OtherSettings", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetLanguage()
		Try
			Me.Text = Language._Object.getCaption(Me.Name, Me.Text)
			
			Me.gbBatchPosting.Text = Language._Object.getCaption(Me.gbBatchPosting.Name, Me.gbBatchPosting.Text)
			Me.lblTranHead.Text = Language._Object.getCaption(Me.lblTranHead.Name, Me.lblTranHead.Text)
			Me.lblAmount.Text = Language._Object.getCaption(Me.lblAmount.Name, Me.lblAmount.Text)
			Me.lblEmployee.Text = Language._Object.getCaption(Me.lblEmployee.Name, Me.lblEmployee.Text)
			Me.lblPayPeriod.Text = Language._Object.getCaption(Me.lblPayPeriod.Name, Me.lblPayPeriod.Text)
			Me.eZeeHeader.Title = Language._Object.getCaption(Me.eZeeHeader.Name & "_Title" , Me.eZeeHeader.Title)
			Me.eZeeHeader.Message = Language._Object.getCaption(Me.eZeeHeader.Name & "_Message" , Me.eZeeHeader.Message)
			Me.btnDelete.Text = Language._Object.getCaption(Me.btnDelete.Name, Me.btnDelete.Text)
			Me.btnEdit.Text = Language._Object.getCaption(Me.btnEdit.Name, Me.btnEdit.Text)
			Me.btnAdd.Text = Language._Object.getCaption(Me.btnAdd.Name, Me.btnAdd.Text)
			Me.colhID.Text = Language._Object.getCaption(CStr(Me.colhID.Tag), Me.colhID.Text)
			Me.colhPeriod.Text = Language._Object.getCaption(CStr(Me.colhPeriod.Tag), Me.colhPeriod.Text)
			Me.colhEmpCode.Text = Language._Object.getCaption(CStr(Me.colhEmpCode.Tag), Me.colhEmpCode.Text)
			Me.colhTranHeadCode.Text = Language._Object.getCaption(CStr(Me.colhTranHeadCode.Tag), Me.colhTranHeadCode.Text)
			Me.colhAmount.Text = Language._Object.getCaption(CStr(Me.colhAmount.Tag), Me.colhAmount.Text)
			Me.colhGUID.Text = Language._Object.getCaption(CStr(Me.colhGUID.Tag), Me.colhGUID.Text)
			Me.btnSave.Text = Language._Object.getCaption(Me.btnSave.Name, Me.btnSave.Text)
			Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
			Me.lblBatchPostingNo.Text = Language._Object.getCaption(Me.lblBatchPostingNo.Name, Me.lblBatchPostingNo.Text)
			Me.colhEmpName.Text = Language._Object.getCaption(CStr(Me.colhEmpName.Tag), Me.colhEmpName.Text)
			Me.lblBatchCode.Text = Language._Object.getCaption(Me.lblBatchCode.Name, Me.lblBatchCode.Text)
			Me.lblBatchName.Text = Language._Object.getCaption(Me.lblBatchName.Name, Me.lblBatchName.Text)
			Me.lblDescription.Text = Language._Object.getCaption(Me.lblDescription.Name, Me.lblDescription.Text)
			Me.colhTranHeadName.Text = Language._Object.getCaption(CStr(Me.colhTranHeadName.Tag), Me.colhTranHeadName.Text)
			Me.lblTotal.Text = Language._Object.getCaption(Me.lblTotal.Name, Me.lblTotal.Text)

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetLanguage", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 3, "Please select period.")
			Language.setMessage(mstrModuleName, 4, "Please select employee.")
			Language.setMessage(mstrModuleName, 5, "Please select transaction head.")
			Language.setMessage(mstrModuleName, 6, "Please enter amount.")
			Language.setMessage(mstrModuleName, 7, "Please add atleast one employee batch posting transaction.")
			Language.setMessage(mstrModuleName, 8, "Sorry! Batch Posting No. cannot be blank. Batch Posting No. is mandatory information.")
			Language.setMessage(mstrModuleName, 9, "Sorry! Selected Transaction Head for selected Employee is already added to the list for selected period.")
			Language.setMessage(mstrModuleName, 10, "Please enter Batch code.")
			Language.setMessage(mstrModuleName, 11, "Please enter Batch name.")
			Language.setMessage(mstrModuleName, 12, "Sorry, you cannot Update this Batch transaction. Reason: This Batch is already Posted to Payroll.")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class