﻿Option Strict On
Imports eZeeCommonLib
Imports Aruti.Data

Public Class frmPayslipGlobalPayment

#Region " Private Variables "
    Private ReadOnly mstrModuleName As String = "frmPayslipGlobalPayment"

    Private mblnCancel As Boolean = True
    Private menAction As enAction = enAction.ADD_ONE

    Private objPaymentTran As clsPayment_tran

    Private mdtPayPeriodStartDate As DateTime
    Private mdtPayPeriodEndDate As DateTime
    'Sohail (07 May 2015) -- Start
    'Enhancement - New Loan Redesign with allow to change interest rate, tenure, topup, receipt.
    Private mdtPaymentDatePeriodStartDate As DateTime
    Private mdtPaymentDatePeriodEndDate As DateTime
    Private mintBaseCountryId As Integer = 0
    'Sohail (07 May 2015) -- End
    Private mdtTable As DataTable = Nothing
    Private mdecTotAmt As Decimal = 0 'Sohail (11 May 2011)

    'Sandeep [ 17 DEC 2010 ] -- Start
    Private mdtDenomeTable As DataTable
    'Sandeep [ 17 DEC 2010 ] -- End 

    'Sohail (08 Oct 2011) -- Start
    'For base currency Balance Amount
    Private mstrBaseCurrSign As String
    Private mintBaseCurrId As Integer
    Private mintPaidCurrId As Integer
    Private mdecBaseExRate As Decimal
    Private mdecPaidExRate As Decimal
    Private mdecBaseCurrBalanceAmt As Decimal
    Private mdecPaidCurrBalanceAmt As Decimal
    Private mdecPaidCurrBalFormatedAmt As Decimal 'displayed on txtAmount textbox.
    'Sohail (08 Oct 2011) -- End
    'Sohail (30 Apr 2019) -- Start
    'BBLMS Issue - Support Issue Id # 0003781 - 74.1 - Payslip global payment takes hours to process help needed urgently.
    Private dtStart As Date
    Private mintCheckedEmployee As Integer = 0
    Private mblnProcessFailed As Boolean = False
    'Sohail (30 Apr 2019) -- End

    Private mstrAdvanceFilter As String = "" 'Sohail (07 Dec 2013)
#End Region

#Region " Display Dialogue "
    Public Function DisplayDialog(ByVal Action As enAction) As Boolean
        Try
            menAction = Action

            Me.ShowDialog()

            Return Not mblnCancel
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "DisplayDialog", mstrModuleName)
        End Try
    End Function
#End Region

#Region " Private Methods "
    Private Sub SetColor()
        Try
            cboEmployee.BackColor = GUI.ColorOptional
            cboPayPoint.BackColor = GUI.ColorOptional
            cboPayType.BackColor = GUI.ColorOptional
            'Sohail (07 Dec 2013) -- Start
            'Enhancement - OMAN
            'cboDepartment.BackColor = GUI.ColorOptional
            'cboGrade.BackColor = GUI.ColorOptional
            'cboSections.BackColor = GUI.ColorOptional
            'cboClass.BackColor = GUI.ColorOptional
            'cboCostCenter.BackColor = GUI.ColorOptional
            'cboJob.BackColor = GUI.ColorOptional
            'cboUnit.BackColor = GUI.ColorOptional
            'Sohail (07 Dec 2013) -- End

            cboPayYear.BackColor = GUI.ColorComp
            cboPayPeriod.BackColor = GUI.ColorComp
            txtVoucher.BackColor = GUI.ColorComp
            cboPaymentMode.BackColor = GUI.ColorComp
            cboBankGroup.BackColor = GUI.ColorComp
            cboBranch.BackColor = GUI.ColorComp
            txtChequeNo.BackColor = GUI.ColorComp
            txtCashPerc.BackColor = GUI.ColorComp
            cboCurrency.BackColor = GUI.ColorComp 'Sohail (08 Oct 2011)
            cboAccountNo.BackColor = GUI.ColorComp 'Sohail (21 Jul 2012)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetColor", mstrModuleName)
        End Try
    End Sub

    Private Sub SetValue()
        Try
            objPaymentTran._Voucherno = txtVoucher.Text.Trim 'Sohail (15 Dec 2010)
            'Sohail (30 Oct 2018) -- Start
            'FDRC Enhancement - Global Payment performance issue on FDRC database in 75.1.
            objPaymentTran._Paymentrefid = CInt(clsPayment_tran.enPaymentRefId.PAYSLIP)
            objPaymentTran._PaymentTypeId = CInt(clsPayment_tran.enPayTypeId.PAYMENT)
            objPaymentTran._Paymentmodeid = CInt(cboPaymentMode.SelectedValue)
            objPaymentTran._Periodunkid = CInt(cboPayPeriod.SelectedValue) 'Sohail (29 Nov 2018)
            'Sohail (30 Oct 2018) -- End

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetValue", mstrModuleName)
        End Try
    End Sub

    Private Sub FillCombo()
        Dim objMaster As New clsMasterData
        Dim objPayPoint As New clspaypoint_master
        Dim objCommon As New clsCommon_Master
        'Sohail (07 Dec 2013) -- Start
        'Enhancement - OMAN
        'Dim objDept As New clsDepartment
        'Dim objGrade As New clsGrade
        'Dim objSection As New clsSections
        'Dim objAccess As New clsAccess
        'Dim objClass As New clsClass
        'Dim objCostCenter As New clscostcenter_master
        'Dim objService As New clsServices
        'Dim objJob As New clsJobs
        'Dim objUnit As New clsUnits
        Dim objBank As New clspayrollgroup_master
        'Sohail (07 Dec 2013) -- End
        'Dim objEmployee As New clsEmployee_Master 'Sohail (06 Jan 2012)
        'Sohail (16 Jul 2012) -- Start
        'TRA - ENHANCEMENT
        'Dim objBankGrp As New clspayrollgroup_master
        Dim objCompanyBank As New clsCompany_Bank_tran
        'Sohail (16 Jul 2012) -- End
        Dim dsCombo As DataSet
        Dim objExRate As New clsExchangeRate 'Sohail (08 Oct 2011)
        Dim objPeriod As New clscommom_period_Tran 'Sohail (07 May 2015)
        Dim intFirstPeriodID As Integer = 0 'Sohail (24 Apr 2019)
        Try

            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'dsCombo = objMaster.getComboListPAYYEAR("PayYear", True)
            dsCombo = objMaster.getComboListPAYYEAR(FinancialYear._Object._YearUnkid, FinancialYear._Object._FinancialYear_Name, Company._Object._Companyunkid, "PayYear", True)
            'S.SANDEEP [04 JUN 2015] -- END

            With cboPayYear
                .BeginUpdate()
                .ValueMember = "Id"
                .DisplayMember = "Name"
                .DataSource = dsCombo.Tables("PayYear")
                If .Items.Count > 0 Then .SelectedValue = 0
                .EndUpdate()
            End With

            'Sohail (24 Apr 2019) -- Start
            'Enhancement - 76.1 - Set first open period in period selection on global payslip payment screen.
            intFirstPeriodID = objMaster.getFirstPeriodID(enModuleReference.Payroll, FinancialYear._Object._YearUnkid, enStatusType.Open)
            'Sohail (24 Apr 2019) -- End

            'Sohail (07 May 2015) -- Start
            'Enhancement - New Loan Redesign with allow to change interest rate, tenure, topup, receipt.

            'Nilay (10-Oct-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'dsCombo = objPeriod.getListForCombo(enModuleReference.Payroll, FinancialYear._Object._YearUnkid, "PayPeriod", True, enStatusType.Open, False)
            dsCombo = objPeriod.getListForCombo(enModuleReference.Payroll, FinancialYear._Object._YearUnkid, _
                                                FinancialYear._Object._DatabaseName, _
                                                FinancialYear._Object._Database_Start_Date, _
                                                "PayPeriod", True, enStatusType.Open, False)
            'Nilay (10-Oct-2015) -- End

            With cboPayPeriod
                .ValueMember = "periodunkid"
                .DisplayMember = "Name"
                .DataSource = dsCombo.Tables("PayPeriod")
                'Sohail (24 Apr 2019) -- Start
                'Enhancement - 76.1 - Set first open period in period selection on global payslip payment screen.
                'If .Items.Count > 0 Then .SelectedValue = 0
                If .Items.Count > 0 Then .SelectedValue = intFirstPeriodID
                'Sohail (24 Apr 2019) -- End
            End With
            With cboPaymentDatePeriod
                .ValueMember = "periodunkid"
                .DisplayMember = "Name"
                .DataSource = dsCombo.Tables("PayPeriod").Copy
                'Sohail (24 Apr 2019) -- Start
                'Enhancement - 76.1 - Set first open period in period selection on global payslip payment screen.
                'If .Items.Count > 0 Then .SelectedValue = 0
                If .Items.Count > 0 Then .SelectedValue = intFirstPeriodID
                'Sohail (24 Apr 2019) -- End
            End With
            'Sohail (07 May 2015) -- End

            dsCombo = objPayPoint.getListForCombo("PayPointList", True)
            With cboPayPoint
                .BeginUpdate()
                .ValueMember = "paypointunkid"
                .DisplayMember = "name"
                .DataSource = dsCombo.Tables("PayPointList")
                If .Items.Count > 0 Then .SelectedValue = 0
                .EndUpdate()
            End With

            'Sohail (11 Jan 2014) -- Start
            'Enhancement - PayPoint, PayType filter on Global Payment
            dsCombo = objCommon.getComboList(clsCommon_Master.enCommonMaster.PAY_TYPE, True, "PayType")
            With cboPayType
                .BeginUpdate()
                .ValueMember = "masterunkid"
                .DisplayMember = "name"
                .DataSource = dsCombo.Tables("PayType")
                .SelectedValue = 0
                .EndUpdate()
            End With
            'Sohail (11 Jan 2014) -- End

            'Sohail (06 Jan 2012) -- Start
            'TRA - ENHANCEMENT
            'dsCombo = objEmployee.GetEmployeeList("EmployeeList", True)
            'With cboEmployee
            '    .BeginUpdate()
            '    .ValueMember = "employeeunkid"
            '    .DisplayMember = "employeename"
            '    .DataSource = dsCombo.Tables("EmployeeList")
            '    If .Items.Count > 0 Then .SelectedValue = 0
            '    .EndUpdate()
            'End With
            'Sohail (06 Jan 2012) -- End

            'Sohail (07 Dec 2013) -- Start
            'Enhancement - OMAN
            'dsCombo = objDept.getComboList("DepartmentList", True)
            'With cboDepartment
            '    .BeginUpdate()
            '    .ValueMember = "departmentunkid"
            '    .DisplayMember = "name"
            '    .DataSource = dsCombo.Tables("DepartmentList")
            '    If .Items.Count > 0 Then .SelectedValue = 0
            '    .EndUpdate()
            'End With

            'dsCombo = objGrade.getComboList("GradeList", True)
            'With cboGrade
            '    .BeginUpdate()
            '    .ValueMember = "gradeunkid"
            '    .DisplayMember = "name"
            '    .DataSource = dsCombo.Tables("GradeList")
            '    If .Items.Count > 0 Then .SelectedValue = 0
            '    .EndUpdate()
            'End With

            'dsCombo = objSection.getComboList("SectionList", True)
            'With cboSections
            '    .BeginUpdate()
            '    .ValueMember = "sectionunkid"
            '    .DisplayMember = "name"
            '    .DataSource = dsCombo.Tables("SectionList")
            '    If .Items.Count > 0 Then .SelectedValue = 0
            '    .EndUpdate()
            'End With

            'dsCombo = objClass.getComboList("ClassList", True)
            'With cboClass
            '    .BeginUpdate()
            '    .ValueMember = "classesunkid"
            '    .DisplayMember = "name"
            '    .DataSource = dsCombo.Tables("ClassList")
            '    If .Items.Count > 0 Then .SelectedValue = 0
            '    .EndUpdate()
            'End With

            'dsCombo = objCostCenter.getComboList("CostCenterList", True)
            'With cboCostCenter
            '    .BeginUpdate()
            '    .ValueMember = "CostCenterunkid"
            '    .DisplayMember = "CostCentername"
            '    .DataSource = dsCombo.Tables("CostCenterList")
            '    If .Items.Count > 0 Then .SelectedValue = 0
            '    .EndUpdate()
            'End With

            'dsCombo = objJob.getComboList("JobList", True)
            'With cboJob
            '    .BeginUpdate()
            '    .ValueMember = "jobunkid"
            '    .DisplayMember = "name"
            '    .DataSource = dsCombo.Tables("JobList")
            '    If .Items.Count > 0 Then .SelectedValue = 0
            '    .EndUpdate()
            'End With

            'dsCombo = objUnit.getComboList("UnitList", True)
            'With cboUnit
            '    .BeginUpdate()
            '    .ValueMember = "unitunkid"
            '    .DisplayMember = "name"
            '    .DataSource = dsCombo.Tables("UnitList")
            '    If .Items.Count > 0 Then .SelectedValue = 0
            '    .EndUpdate()
            'End With
            'Sohail (07 Dec 2013) -- End

            dsCombo = objMaster.GetPaymentMode("PayMode", True) 'Sohail (15 Dec 2010)
            With cboPaymentMode
                .ValueMember = "Id"
                .DisplayMember = "Name"
                .DataSource = dsCombo.Tables("PayMode")
                .SelectedValue = 0
            End With

            'Sohail (16 Jul 2012) -- Start
            'TRA - ENHANCEMENT - Now Show bank from Company master
            'dsCombo = objBankGrp.getListForCombo(enGroupType.BankGroup, "BankGrp", True)
            'With cboBankGroup
            '    .ValueMember = "groupmasterunkid"
            '    .DisplayMember = "name"
            '    .DataSource = dsCombo.Tables("BankGrp")
            '    .SelectedValue = 0
            'End With
            dsCombo = objCompanyBank.GetComboList(Company._Object._Companyunkid, 1, "BankGrp")
            With cboBankGroup
                .ValueMember = "id"
                .DisplayMember = "name"
                .DataSource = dsCombo.Tables("BankGrp")
                .SelectedValue = 0
            End With
            'Sohail (16 Jul 2012) -- End

            'Sohail (08 Oct 2011) -- Start
            dsCombo = objExRate.getComboList("ExRate", False)
            If dsCombo.Tables(0).Rows.Count > 0 Then
                Dim dtTable As DataTable = New DataView(dsCombo.Tables("ExRate"), "isbasecurrency = 1 ", "", DataViewRowState.CurrentRows).ToTable
                If dtTable.Rows.Count > 0 Then
                    mstrBaseCurrSign = dtTable.Rows(0).Item("currency_sign").ToString
                    mintBaseCurrId = CInt(dtTable.Rows(0).Item("exchangerateunkid"))
                    'Sohail (07 May 2015) -- Start
                    'Enhancement - New Loan Redesign with allow to change interest rate, tenure, topup, receipt.
                    mintBaseCountryId = CInt(dtTable.Rows(0).Item("countryunkid"))
                Else
                    mintBaseCountryId = 0
                    'Sohail (07 May 2015) -- End
                End If

                With cboCurrency
                    'Sohail (03 Sep 2012) -- Start
                    'TRA - ENHANCEMENT
                    '.ValueMember = "exchangerateunkid"
                    .ValueMember = "countryunkid"
                    'Sohail (03 Sep 2012) -- End
                    .DisplayMember = "currency_sign"
                    .DataSource = dsCombo.Tables("ExRate")
                    'Sohail (07 May 2015) -- Start
                    'Enhancement - New Loan Redesign with allow to change interest rate, tenure, topup, receipt.
                    '.SelectedIndex = 0
                    If mintBaseCountryId > 0 Then
                        .SelectedValue = mintBaseCountryId
                    Else
                        .SelectedIndex = 0
                    End If
                    'Sohail (07 May 2015) -- End
                End With
            End If
            'Sohail (08 Oct 2011) -- End

            'Sohail (07 Dec 2013) -- Start
            'Enhancement - OMAN
            dsCombo = objBank.getListForCombo(enPayrollGroupType.Bank, "List", True)
            With cboEmpBank
                .ValueMember = "groupmasterunkid"
                .DisplayMember = "name"
                .DataSource = dsCombo.Tables("List")
                .SelectedValue = 0
            End With
            'Sohail (07 Dec 2013) -- End

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        Finally
            objMaster = Nothing
            objPayPoint = Nothing
            objCommon = Nothing
            'objEmployee = Nothing 'Sohail (06 Jan 2012)
            'Sohail (07 Dec 2013) -- Start
            'Enhancement - OMAN
            'objDept = Nothing
            'objGrade = Nothing
            'objSection = Nothing
            'objAccess = Nothing
            'objClass = Nothing
            'objCostCenter = Nothing
            'objService = Nothing
            'objJob = Nothing
            'objUnit = Nothing
            objBank = Nothing
            'Sohail (07 Dec 2013) -- End
            'objBankGrp = Nothing 'Sohail (16 Jul 2012)
            objExRate = Nothing 'Sohail (08 Oct 2011)
            objPeriod = Nothing 'Sohail (07 May 2015)
        End Try
    End Sub

    Private Sub FillList()
        Dim objEmployee As New clsEmployee_Master
        Dim objPayment As New clsPayment_tran
        Dim objTnALeave As New clsTnALeaveTran
        Dim objEmpBank As New clsEmployeeBanks
        Dim dsEmployee As New DataSet
        Dim dsList As DataSet
        Dim dtTable As DataTable
        Dim dtTemp As DataTable
        Dim intCount As Integer = 0
        Dim strEmployeeList As String = ""
        Dim strEmpListWithBank As String = ""
        Dim strFilter As String = ""
        Dim decAmt As Decimal = 0 'Sohail (11 May 2011)
        Dim strVoucherNo As String
        Dim intTnALeaveUnkID As Integer
        Dim strTmp As String = ""
        Dim decTotAmt As Decimal 'Sohail (25 Jan 2011), 'Sohail (11 May 2011)
        Dim decBaseAmt As Decimal 'Sohail (08 Oct 2011)
        Dim decRondingMultile As Decimal = 0  'Sohail (22 Oct 2013)
        Dim decTotPaymentAmt As Decimal = 0 'Sohail (07 Dec 2013)
        Dim strEmpFilter As String = "" 'Sohail (11 Jan 2014)
        Try
            objlblEmpCount.Text = "( 0 )" 'Sohail (10 Dec 2013)
            'Sohail (15 Dec 2010) -- Start
            lvEmployeeList.Items.Clear()
            objchkSelectAll.Checked = False 'Sohail (13 Feb 2013)
            'Sohail (18 Dec 2010) -- Start
            objlblColor.Visible = False
            'lblMessage.Visible = False 'Sohail (22 Oct 2013)
            'Sohail (18 Dec 2010) -- End
            If CInt(cboPayPeriod.SelectedValue) <= 0 OrElse CInt(cboPaymentMode.SelectedValue) <= 0 Then Exit Try
            'Sohail (15 Dec 2010) -- End
            If mdecBaseExRate = 0 AndAlso mdecPaidExRate = 0 Then Exit Try 'Sohail (03 Sep 2012) - [No exchange rate found for current currency for current payment date.]
            Cursor.Current = Cursors.WaitCursor 'Sohail (04 Mar 2011)

            'Sohail (11 Jan 2014) -- Start
            'Enhancement - PayPoint, PayType filter on Global Payment
            strEmpFilter = mstrAdvanceFilter
            If CInt(cboPayType.SelectedValue) > 0 Then
                If strEmpFilter.Trim <> "" Then
                    strEmpFilter &= " AND hremployee_master.paytypeunkid = " & CInt(cboPayType.SelectedValue) & " "
                Else
                    strEmpFilter = " hremployee_master.paytypeunkid = " & CInt(cboPayType.SelectedValue) & " "
                End If
            End If
            'Sohail (11 Jan 2014) -- End

            'Sohail (12 Jan 2015) -- Start
            'Enhancement - Allow to employee bank period wise.
            If CInt(cboEmployee.SelectedValue) > 0 Then
                If strEmpFilter.Trim <> "" Then
                    strEmpFilter &= " AND hremployee_master.employeeunkid = " & CInt(cboEmployee.SelectedValue) & " "
                Else
                    strEmpFilter = " hremployee_master.employeeunkid = " & CInt(cboEmployee.SelectedValue) & " "
                End If
            End If
            'Sohail (12 Jan 2015) -- End

            'Sohail (07 Dec 2013) -- Start
            'Enhancement - OMAN
            'dsEmployee = objEmployee.GetEmployeeList("Employee", False, True _
            '                                          , CInt(cboEmployee.SelectedValue) _
            '                                          , CInt(cboDepartment.SelectedValue) _
            '                                          , CInt(cboSections.SelectedValue) _
            '                                          , CInt(cboUnit.SelectedValue) _
            '                                          , CInt(cboGrade.SelectedValue) _
            '                                          , 0 _
            '                                          , CInt(cboClass.SelectedValue) _
            '                                          , CInt(cboCostCenter.SelectedValue) _
            '                                          , 0 _
            '                                          , CInt(cboJob.SelectedValue) _
            '                                          , CInt(cboPayPoint.SelectedValue) _
            '                                          , mdtPayPeriodStartDate, mdtPayPeriodEndDate _
            '                                        ) 'Sohail (06 Jan 2012) - [mdtPayPeriodStartDate, mdtPayPeriodEndDate]

            'Anjan [10 June 2015] -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'dsEmployee = objEmployee.GetEmployeeList("Employee", False, True _
            '                                          , CInt(cboEmployee.SelectedValue) _
            '                                          , 0 _
            '                                          , 0 _
            '                                          , 0 _
            '                                          , 0 _
            '                                          , 0 _
            '                                          , 0 _
            '                                          , 0 _
            '                                          , 0 _
            '                                          , 0 _
            '                                          , CInt(cboPayPoint.SelectedValue) _
            '                                          , mdtPayPeriodStartDate, mdtPayPeriodEndDate _
            '                                          , , , , , strEmpFilter _
            '                                        )
            dsEmployee = objEmployee.GetEmployeeList(FinancialYear._Object._DatabaseName, _
                                           User._Object._Userunkid, _
                                           FinancialYear._Object._YearUnkid, _
                                           Company._Object._Companyunkid, _
                                           mdtPayPeriodStartDate, _
                                           mdtPayPeriodEndDate, _
                                           ConfigParameter._Object._UserAccessModeSetting, _
                                           True, False, "Employee", False, CInt(cboEmployee.SelectedValue), , , , , , , , , , CInt(cboPayPoint.SelectedValue), , , , strEmpFilter)
            'Anjan [10 June 2015] -- End

            'Sohail (07 Dec 2013) -- End
            Dim lvItem As ListViewItem
            'Dim lvArray As New List(Of ListViewItem) 'Sohail (03 Nov 2010)
            'lvEmployeeList.Items.Clear() 'Sohail (15 Dec 2010)
            lvEmployeeList.BeginUpdate()

            '*** Get list of Employee from Employee Master
            If CInt(cboPayPeriod.SelectedValue) > 0 Then
                'Sohail (07 Dec 2013) -- Start
                'Enhancement - OMAN
                'For Each dsRow As DataRow In dsEmployee.Tables("Employee").Rows
                '    With dsRow
                '        If strEmployeeList.Length <= 0 Then
                '            strEmployeeList = .Item("employeeunkid").ToString
                '        Else
                '            strEmployeeList &= "," & .Item("employeeunkid").ToString
                '        End If
                '    End With
                'Next
                Dim allEmp As List(Of String) = (From p In dsEmployee.Tables("Employee") Select (p.Item("employeeunkid").ToString)).ToList
                strEmployeeList = String.Join(",", allEmp.ToArray)
                'Sohail (07 Dec 2013) -- End
            End If
            'Sohail (28 Dec 2010) -- Start
            'dsList = objTnALeave.Get_Balance_List("Balance", , CInt(cboPayYear.SelectedValue), CInt(cboPayPeriod.SelectedValue), "prtnaleave_tran.payperiodunkid, hremployee_master.firstname")
            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'dsList = objTnALeave.Get_Balance_List("Balance", strEmployeeList, , CInt(cboPayPeriod.SelectedValue), "prtnaleave_tran.payperiodunkid, hremployee_master.firstname")

            'Nilay (10-Feb-2016) -- Start
            'dsList = objTnALeave.Get_Balance_List(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, mdtPayPeriodStartDate, mdtPayPeriodEndDate, ConfigParameter._Object._UserAccessModeSetting, True, ConfigParameter._Object._IsIncludeInactiveEmp, True, "", "Balance", strEmployeeList, CInt(cboPayPeriod.SelectedValue), "prtnaleave_tran.payperiodunkid, hremployee_master.firstname")
            'Sohail (30 Oct 2018) -- Start
            'FDRC Enhancement - Global Payment performance issue on FDRC database in 75.1.
            'dsList = objTnALeave.Get_Balance_List(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, mdtPayPeriodStartDate, mdtPayPeriodEndDate, ConfigParameter._Object._UserAccessModeSetting, True, False, True, "", "Balance", strEmployeeList, CInt(cboPayPeriod.SelectedValue), "prtnaleave_tran.payperiodunkid, hremployee_master.firstname")
            dsList = objTnALeave.Get_Balance_List(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, mdtPayPeriodStartDate, mdtPayPeriodEndDate, ConfigParameter._Object._UserAccessModeSetting, True, False, True, "", "Balance", strEmployeeList, CInt(cboPayPeriod.SelectedValue), "prtnaleave_tran.payperiodunkid, emp.employeename")
            'Sohail (30 Oct 2018) -- End
            'Nilay (10-Feb-2016) -- End

            'Sohail (21 Aug 2015) -- End
            'Sohail (28 Dec 2010) -- End

            'Sohail (15 Dec 2010) -- Start
            '*** Get list of Employee who has bank account
            'If CInt(cboPaymentMode.SelectedValue) > 0 Then
            '    If CInt(cboPaymentMode.SelectedValue) <> enPaymentMode.CASH Then 
            '        strEmpListWithBank = objEmpBank.Get_DIST_EmployeeList_WithBanks
            '    End If 'Sohail (15 Dec 2010)
            'End If
            'If strEmployeeList.Trim.Length > 0 Then
            '    strFilter = "employeeunkid IN (" & strEmployeeList & ")"
            'End If
            'If CInt(cboPaymentMode.SelectedValue) <> enPaymentMode.CASH AndAlso strEmpListWithBank.Trim.Length > 0 Then
            '    strTmp = "0,"
            'ElseIf CInt(cboPaymentMode.SelectedValue) <> enPaymentMode.CASH AndAlso strEmpListWithBank.Trim.Length <= 0 Then
            '    strTmp = "0"
            'End If
            'If strEmpListWithBank.Trim.Length > 0 AndAlso strEmployeeList.Trim.Length <= 0 Then
            '    strFilter = "employeeunkid IN (" & strTmp & strEmpListWithBank & ")"
            'ElseIf strEmpListWithBank.Trim.Length > 0 AndAlso strEmployeeList.Trim.Length > 0 Then
            '    strFilter &= " AND employeeunkid IN (" & strTmp & strEmpListWithBank & ")"
            'ElseIf CInt(cboPaymentMode.SelectedValue) <> enPaymentMode.CASH Then
            '    If strEmpListWithBank.Trim.Length <= 0 AndAlso strEmployeeList.Trim.Length > 0 Then
            '        strFilter &= " AND employeeunkid IN (" & strTmp & strEmpListWithBank & ")"
            '    ElseIf strEmpListWithBank.Trim.Length <= 0 AndAlso strEmployeeList.Trim.Length > 0 Then
            '        strFilter &= "employeeunkid IN (" & strTmp & strEmpListWithBank & ")"
            '    End If
            'End If

            '*** Get list of Employee who has bank account
            'Sohail (07 Dec 2013) -- Start
            'Enhancement - OMAN
            'strEmpListWithBank = objEmpBank.Get_DIST_EmployeeList_WithBanks
            'Sohail (12 Jan 2015) -- Start
            'Enhancement - Allow to employee bank period wise.
            If CInt(cboPaymentMode.SelectedValue) = enPaymentMode.CHEQUE OrElse CInt(cboPaymentMode.SelectedValue) = enPaymentMode.TRANSFER Then
                strEmpListWithBank = objEmpBank.Get_DIST_EmployeeList_WithBanks(CInt(cboEmpBank.SelectedValue), CInt(cboEmpBranch.SelectedValue), mdtPayPeriodEndDate)
            Else
                strEmpListWithBank = objEmpBank.Get_DIST_EmployeeList_WithBanks(, , mdtPayPeriodEndDate)
            End If
            'Sohail (12 Jan 2015) -- End
            'Sohail (07 Dec 2013) -- End
            If strEmpListWithBank.Trim.Length > 0 Then
                strTmp = "0,"
            ElseIf strEmpListWithBank.Trim.Length <= 0 Then
                strTmp = "0"
            End If
            strEmpListWithBank = strTmp & strEmpListWithBank
            If strEmployeeList.Trim.Length > 0 Then
                strFilter = "employeeunkid IN (" & strEmployeeList & ")"
                'Sohail (11 Jan 2014) -- Start
                'Enhancement - PayPoint, PayType filter on Global Payment
            Else
                strFilter = " 1 = 2 "
                'Sohail (11 Jan 2014) -- End
            End If
            If CInt(cboPaymentMode.SelectedValue) > 0 Then
                If CInt(cboPaymentMode.SelectedValue) = enPaymentMode.CASH Then
                    If strEmployeeList.Trim.Length > 0 Then
                        strFilter &= " AND employeeunkid NOT IN (" & strEmpListWithBank & ")"
                    ElseIf strEmployeeList.Trim.Length <= 0 Then
                        strFilter = " employeeunkid NOT IN (" & strEmpListWithBank & ")"
                    End If
                ElseIf CInt(cboPaymentMode.SelectedValue) = enPaymentMode.CHEQUE OrElse CInt(cboPaymentMode.SelectedValue) = enPaymentMode.TRANSFER Then
                    If strEmployeeList.Trim.Length > 0 Then
                        strFilter &= " AND employeeunkid IN (" & strEmpListWithBank & ")"
                    Else
                        strFilter = " employeeunkid IN (" & strEmpListWithBank & ")"
                    End If
                ElseIf CInt(cboPaymentMode.SelectedValue) = enPaymentMode.CASH_AND_CHEQUE Then
                    'List of Both Bank A/C Holder as well as Non Bank A/C Holder Employee
                End If
            End If
            'Sohail (15 Dec 2010) -- End

            'Sohail (17 Oct 2012) -- Start
            'TRA - ENHANCEMENT
            If strFilter.Trim <> "" Then
                'Sohail (18 Jan 2013) -- Start
                'TRA - ENHANCEMENT - Issue : Employee was not coming if net pay is zero though opening balance is greater than zero.
                'strFilter &= " AND total_amount > 0 "
                strFilter &= " AND total_amount + openingbalance > 0 "
                'Sohail (18 Jan 2013) -- End
            Else
                'Sohail (18 Jan 2013) -- Start
                'TRA - ENHANCEMENT - Issue : Employee was not coming if net pay is zero though opening balance is greater than zero.
                'strFilter = " total_amount > 0 "
                strFilter = " total_amount + openingbalance > 0 "
                'Sohail (18 Jan 2013) -- End
            End If
            'Sohail (17 Oct 2012) -- End

            'Sohail (04 Mar 2011) -- Start
            'Issue : for when there is no shift then employees were not coming in list for global payment.
            'If strFilter.Trim.Length > 0 Then
            '    strFilter &= " AND daysinperiod > 0 AND totalpayabledays > 0"
            'Else
            '    strFilter = " daysinperiod > 0 AND totalpayabledays > 0"
            'End If
            'Sohail (04 Mar 2011) -- End

            dtTable = New DataView(dsList.Tables("Balance"), strFilter, "employeename", DataViewRowState.CurrentRows).ToTable

            'Sohail (22 Oct 2013) -- Start
            'TRA - ENHANCEMENT
            Select Case CInt(ConfigParameter._Object._PaymentRoundingMultiple)
                Case enPaymentRoundingMultiple._1
                    decRondingMultile = 1
                Case enPaymentRoundingMultiple._5
                    decRondingMultile = 5
                Case enPaymentRoundingMultiple._10
                    decRondingMultile = 10
                Case enPaymentRoundingMultiple._50
                    decRondingMultile = 50
                Case enPaymentRoundingMultiple._100
                    decRondingMultile = 100
                    'Sohail (08 Apr 2016) -- Start
                    'Issue - 58.1 - Allow Rounding to 200 for Payslip Payment Rounding Multiple option for Mabibo.
                Case enPaymentRoundingMultiple._200
                    decRondingMultile = 200
                    'Sohail (08 Apr 2016) -- End
                Case enPaymentRoundingMultiple._500
                    decRondingMultile = 500
                Case enPaymentRoundingMultiple._1000
                    decRondingMultile = 1000
            End Select
            'Sohail (22 Oct 2013) -- End

            For Each dtRow As DataRow In dtTable.Rows
                lvItem = New ListViewItem
                With lvItem
                    .Text = ""
                    .Tag = dtRow.Item("employeeunkid").ToString
                    .SubItems.Add(dtRow.Item("employeecode").ToString)
                    .SubItems.Add(dtRow.Item("employeename").ToString)

                    decAmt = 0
                    decTotAmt = 0 'Sohail (25 Jan 2011)
                    strVoucherNo = ""
                    intTnALeaveUnkID = 0
                    If CInt(cboPayPeriod.SelectedValue) > 0 Then

                        dtTemp = New DataView(dtTable, "employeeunkid = " & CInt(.Tag) & "", "", DataViewRowState.CurrentRows).ToTable

                        If dtTemp.Rows.Count > 0 Then
                            decAmt = CDec(dtTemp.Rows(0).Item("balanceamount")) 'Sohail (11 May 2011)
                            decTotAmt = CDec(dtTemp.Rows(0).Item("total_amount")) 'Sohail (25 Jan 2011), 'Sohail (11 May 2011)
                            strVoucherNo = dtTemp.Rows(0).Item("voucherno").ToString
                            intTnALeaveUnkID = CInt(dtTemp.Rows(0).Item("tnaleavetranunkid"))

                            'Sohail (22 Oct 2013) -- Start
                            'TRA - ENHANCEMENT
                            'Sohail (17 Mar 2020) -- Start
                            'PLASCO LTD Enhancement # 0004621 : Assist to put Rounding off in both base currency and Usd payment net payment.
                            'Select Case CInt(ConfigParameter._Object._PaymentRoundingType)
                            '    Case enPaymentRoundingType.NONE
                            '        decAmt = CDec(dtTemp.Rows(0).Item("balanceamount"))
                            '    Case enPaymentRoundingType.AUTOMATIC
                            '        If decRondingMultile / 2 > CDec(dtTemp.Rows(0).Item("balanceamount")) Mod decRondingMultile Then
                            '            decAmt = CDec(dtTemp.Rows(0).Item("balanceamount")) - CDec(CDec(dtTemp.Rows(0).Item("balanceamount")) Mod decRondingMultile)
                            '        Else
                            '            If CDec(CDec(dtTemp.Rows(0).Item("balanceamount")) Mod decRondingMultile) <> 0 Then
                            '                decAmt = CDec(dtTemp.Rows(0).Item("balanceamount")) - CDec(CDec(dtTemp.Rows(0).Item("balanceamount")) Mod decRondingMultile) + decRondingMultile
                            '            End If
                            '        End If
                            '    Case enPaymentRoundingType.UP
                            '        If CDec(CDec(dtTemp.Rows(0).Item("balanceamount")) Mod decRondingMultile) <> 0 Then
                            '            decAmt = CDec(dtTemp.Rows(0).Item("balanceamount")) - CDec(CDec(dtTemp.Rows(0).Item("balanceamount")) Mod decRondingMultile) + decRondingMultile
                            '        End If
                            '    Case enPaymentRoundingType.DOWN
                            '        decAmt = CDec(dtTemp.Rows(0).Item("balanceamount")) - CDec(CDec(dtTemp.Rows(0).Item("balanceamount")) Mod decRondingMultile)
                            'End Select
                            ''Sohail (22 Oct 2013) -- End

                            ''Sohail (21 Mar 2014) -- Start
                            ''Enhancement - Set Limit for C/F Netpay balance and Payment JV Report
                            'If ConfigParameter._Object._CFRoundingAbove > 0 AndAlso Math.Abs((CDec(dtTemp.Rows(0).Item("balanceamount")) - decAmt)) <= ConfigParameter._Object._CFRoundingAbove Then
                            '    .SubItems(colhCode.Index).Tag = CDec(dtTemp.Rows(0).Item("balanceamount")) - decAmt '*** Rounding Adjustment
                            'Else
                            '    .SubItems(colhCode.Index).Tag = 0 '*** Rounding Adjustment
                            'End If
                            ''Sohail (21 Mar 2014) -- End

                            ''Sohail (08 Oct 2011) -- Start
                            'decBaseAmt = decAmt
                            'If mdecBaseExRate <> 0 Then
                            '    decAmt = decAmt * mdecPaidExRate / mdecBaseExRate
                            'Else
                            '    decAmt = decAmt * mdecPaidExRate
                            'End If
                            ''Sohail (08 Oct 2011) -- End
                            If mdecBaseExRate <> 0 Then
                                decAmt = decAmt * mdecPaidExRate / mdecBaseExRate
                            Else
                                decAmt = decAmt * mdecPaidExRate
                            End If

                            Dim dicBalanceAmt As Decimal = decAmt
                            Select Case CInt(ConfigParameter._Object._PaymentRoundingType)
                                Case enPaymentRoundingType.NONE
                                    decAmt = dicBalanceAmt
                                Case enPaymentRoundingType.AUTOMATIC
                                    If decRondingMultile / 2 > dicBalanceAmt Mod decRondingMultile Then
                                        decAmt = dicBalanceAmt - CDec(dicBalanceAmt Mod decRondingMultile)
                                    Else
                                        If CDec(dicBalanceAmt Mod decRondingMultile) <> 0 Then
                                            decAmt = dicBalanceAmt - CDec(dicBalanceAmt Mod decRondingMultile) + decRondingMultile
                                        End If
                                    End If
                                Case enPaymentRoundingType.UP
                                    If CDec(dicBalanceAmt Mod decRondingMultile) <> 0 Then
                                        decAmt = dicBalanceAmt - CDec(dicBalanceAmt Mod decRondingMultile) + decRondingMultile
                                    End If
                                Case enPaymentRoundingType.DOWN
                                    decAmt = dicBalanceAmt - CDec(dicBalanceAmt Mod decRondingMultile)
                            End Select

                            If mdecBaseExRate <> 0 Then
                                decBaseAmt = decAmt * mdecBaseExRate / mdecPaidExRate
                            Else
                                decBaseAmt = decAmt * mdecBaseExRate
                            End If

                            If ConfigParameter._Object._CFRoundingAbove > 0 AndAlso Math.Abs((CDec(dtTemp.Rows(0).Item("balanceamount")) - decBaseAmt)) <= ConfigParameter._Object._CFRoundingAbove Then
                                .SubItems(colhCode.Index).Tag = CDec(dtTemp.Rows(0).Item("balanceamount")) - decBaseAmt '*** Rounding Adjustment
                            Else
                                .SubItems(colhCode.Index).Tag = 0 '*** Rounding Adjustment
                            End If
                            'Sohail (17 Mar 2020) -- End

                        End If
                    End If
                    .SubItems.Add(Format(decAmt, GUI.fmtCurrency)) 'Sohail (01 Dec 2010)
                    .SubItems(colhAmount.Index).Tag = decAmt
                    .SubItems.Add(strVoucherNo)
                    'Sohail (30 Oct 2018) -- Start
                    'FDRC Enhancement - Global Payment performance issue on FDRC database in 75.1.
                    .SubItems(colhVoucherNo.Index).Tag = dtRow.Item("costcenterunkid").ToString
                    'Sohail (30 Oct 2018) -- End
                    .SubItems.Add(intTnALeaveUnkID.ToString)
                    .SubItems.Add(decBaseAmt.ToString) 'Sohail (08 Oct 2011)

                    'Sohail (18 Dec 2010) -- Start
                    '*** Show Employee with Zero Balance Salary

                    'Sohail (25 Jan 2011) -- Start
                    'If dblAmt > 0 OrElse (dblAmt = 0 AndAlso _
                    '                      (CInt(cboPaymentMode.SelectedValue) = enPaymentMode.CASH OrElse CInt(cboPaymentMode.SelectedValue) = enPaymentMode.CASH_AND_CHEQUE)) Then
                    'Sohail (04 Mar 2011) -- Start
                    'Changes : Do not show Zero or Negative salary for payment as they are carry forward to next period
                    'If dblAmt > 0 OrElse (dblAmt <= 0 AndAlso _
                    '                      (CInt(cboPaymentMode.SelectedValue) = enPaymentMode.CASH OrElse _
                    '                       CInt(cboPaymentMode.SelectedValue) = enPaymentMode.CASH_AND_CHEQUE)) Then
                    'Sohail (04 Mar 2011) -- End
                    'Sohail (25 Jan 2011) -- End

                    '*** Show those Employee only whose Balance Amount > 0
                    If decAmt > 0 Then 'Sohail (04 Mar 2011)
                        'Sohail (18 Dec 2010) -- End


                        'Sohail (03 Nov 2010) -- Start
                        'lvArray.Add(lvItem)
                        'Sohail (18 Dec 2010) -- Start
                        'Sohail (04 Mar 2011) -- Start
                        'If dblTotAmt <= 0 Then 'Sohail (25 Jan 2011)
                        '    If (New clsPayment_tran).IsPaymentDone(clsPayment_tran.enPayTypeId.PAYMENT, intTnALeaveUnkID) = False Then 'Sohail (25 Jan 2011)
                        '        lvItem.BackColor = Color.Red
                        '        lvItem.ForeColor = Color.Yellow

                        '        lblColor.Visible = True
                        '        lblMessage.Visible = True
                        '        'Sohail (25 Jan 2011) -- Start
                        '    Else
                        '        Continue For
                        '    End If
                        '    'Sohail (25 Jan 2011) -- End
                        'End If
                        'Sohail (04 Mar 2011) -- End

                        'Sohail (07 Dec 2013) -- Start
                        'Enhancement - OMAN
                        decTotPaymentAmt += decAmt
                        If txtCutOffAmount.Decimal > 0 AndAlso txtCutOffAmount.Decimal < decTotPaymentAmt Then
                            Exit For
                        End If
                        'Sohail (07 Dec 2013) -- End

                        RemoveHandler lvEmployeeList.ItemChecked, AddressOf lvEmployeeList_ItemChecked 'Sohail (12 Oct 2011)
                        lvEmployeeList.Items.Add(lvItem)
                        AddHandler lvEmployeeList.ItemChecked, AddressOf lvEmployeeList_ItemChecked 'Sohail (12 Oct 2011)

                        'Sohail (18 Dec 2010) -- End
                        'Sohail (03 Nov 2010) -- End
                    End If

                End With
            Next
            'lvEmployeeList.Items.AddRange(lvArray.ToArray) 'Sohail (03 Nov 2010)
            If lvEmployeeList.Items.Count > 19 Then
                colhName.Width = 150 - 18
            Else
                colhName.Width = 150
            End If
            objlblEmpCount.Text = "( " & lvEmployeeList.Items.Count.ToString & " )" 'Sohail (10 Dec 2013)
            lvEmployeeList.EndUpdate()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillList", mstrModuleName)
        Finally
            objEmployee = Nothing
            objPayment = Nothing
            objTnALeave = Nothing
            objEmpBank = Nothing
            dsEmployee = Nothing
            dtTable = Nothing
            dtTemp = Nothing
            Cursor.Current = Cursors.Default 'Sohail (04 Mar 2011)
            lvEmployeeList.EndUpdate()
        End Try
    End Sub

    Private Sub CheckAllEmployee(ByVal blnCheckAll As Boolean)
        Try
            For Each lvItem As ListViewItem In lvEmployeeList.Items
                RemoveHandler lvEmployeeList.ItemChecked, AddressOf lvEmployeeList_ItemChecked 'Sohail (03 Nov 2010)
                lvItem.Checked = blnCheckAll
                AddHandler lvEmployeeList.ItemChecked, AddressOf lvEmployeeList_ItemChecked 'Sohail (03 Nov 2010)
            Next
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "CheckAllEmployee", mstrModuleName)
        End Try
    End Sub

    'Sohail (06 Jan 2012) -- Start
    'TRA - ENHANCEMENT
    Private Sub FillEmployeeCombo()
        Dim objEmployee As New clsEmployee_Master
        Dim dsCombo As DataSet
        Try

            'Anjan [10 June 2015] -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'dsCombo = objEmployee.GetEmployeeList("Employee", True, , , , , , , , , , , , , mdtPayPeriodStartDate, mdtPayPeriodEndDate)

            dsCombo = objEmployee.GetEmployeeList(FinancialYear._Object._DatabaseName, _
                                           User._Object._Userunkid, _
                                           FinancialYear._Object._YearUnkid, _
                                           Company._Object._Companyunkid, _
                                           mdtPayPeriodStartDate, _
                                           mdtPayPeriodEndDate, _
                                           ConfigParameter._Object._UserAccessModeSetting, _
                                           True, False, "Employee", True)
            'Anjan [10 June 2015] -- End
            With cboEmployee
                .BeginUpdate()
                .ValueMember = "employeeunkid"
                .DisplayMember = "employeename"
                .DataSource = dsCombo.Tables("Employee")
                .SelectedValue = 0
                .EndUpdate()
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillEmployeeCombo", mstrModuleName)
        Finally
            objEmployee = Nothing
        End Try

    End Sub
    'Sohail (06 Jan 2012) -- End
#End Region

#Region " Private Functions "
    Private Function IsValid() As Boolean
        Dim dtTable As DataTable 'Sohail (29 Jun 2012)
        'Sohail (16 Jul 2012) -- Start
        'TRA - ENHANCEMENT
        Dim objEmpBank As New clsEmployeeBanks
        Dim ds As DataSet
        'Sohail (16 Jul 2012) -- End
        Try
            'Sohail (07 May 2015) -- Start
            'Enhancement - New Loan Redesign with allow to change interest rate, tenure, topup, receipt.
            'If CInt(cboPayYear.SelectedValue) <= 0 Then
            '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please select Pay Year. Pay Year is mandatory information."), enMsgBoxStyle.Information)
            '    cboPayYear.Focus()
            '    Return False
            'Sohail (07 May 2015) -- End
            If CInt(cboPayPeriod.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Please select Pay Period. Pay Period is mandatory information."), enMsgBoxStyle.Information)
                cboPayPeriod.Focus()
                Return False
                'Sohail (07 May 2015) -- Start
                'Enhancement - New Loan Redesign with allow to change interest rate, tenure, topup, receipt.
                'ElseIf dtpPaymentDate.Value.Date < mdtPayPeriodStartDate OrElse dtpPaymentDate.Value.Date > FinancialYear._Object._Database_End_Date.Date Then
                '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Payment Date should be in between ") & mdtPayPeriodStartDate.ToShortDateString & Language.getMessage(mstrModuleName, 11, " And ") & FinancialYear._Object._Database_End_Date.ToShortDateString & ".", enMsgBoxStyle.Information)
                '    dtpPaymentDate.Focus()
                '    Return False
            ElseIf CInt(cboPaymentDatePeriod.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 19, "Please select Payment Date Period. Payment Date Period is mandatory information."), enMsgBoxStyle.Information)
                cboPaymentDatePeriod.Focus()
                Return False
            ElseIf dtpPaymentDate.Value.Date < mdtPaymentDatePeriodStartDate OrElse dtpPaymentDate.Value.Date > mdtPaymentDatePeriodEndDate Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Payment Date should be between ") & mdtPaymentDatePeriodStartDate.ToShortDateString & Language.getMessage(mstrModuleName, 11, " And ") & mdtPaymentDatePeriodEndDate.ToShortDateString & ".", enMsgBoxStyle.Information)
                dtpPaymentDate.Focus()
                Return False
                'Sohail (07 May 2015) -- End
                'Sohail (15 Dec 2010) -- Start
            ElseIf ConfigParameter._Object._PaymentVocNoType = 0 AndAlso txtVoucher.Text.Trim = "" Then
                'ElseIf txtVoucher.Text.Trim = "" Then
                'Sohail (15 Dec 2010) -- End
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Please enter Payment Voucher. Payment Voucher is mandatory information."), enMsgBoxStyle.Information)
                txtVoucher.Focus()
                Return False
            ElseIf CInt(cboPaymentMode.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "Please select Payment Mode. Payment Mode is mandatory information."), enMsgBoxStyle.Information)
                cboPaymentMode.Focus()
                Return False
                'Sohail (15 Dec 2010) -- Start
                'Sohail (21 Apr 2020) -- Start
                'CCBRT Enhancement # 0004659 : Assist to Enable to do Partial Payment by using Both modes of Payment i.e cheque,cash and Transfe.
                'ElseIf (CInt(cboPaymentMode.SelectedValue) = enPaymentMode.CASH OrElse CInt(cboPaymentMode.SelectedValue) = enPaymentMode.CASH_AND_CHEQUE) AndAlso (txtCashPerc.Decimal <= 0 Or txtCashPerc.Decimal > 100) Then
            ElseIf (txtCashPerc.Decimal <= 0 OrElse txtCashPerc.Decimal > 100) Then
                'Sohail (21 Apr 2020) -- End
                'ElseIf CInt(cboPaymentMode.SelectedValue) = enPaymentMode.CASH AndAlso (txtCashPerc.Decimal <= 0 Or txtCashPerc.Decimal > 100) Then
                'Sohail (15 Dec 2010) -- End
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 6, "Sorry, Percentage should be between 1 and 100."), enMsgBoxStyle.Information)
                txtCashPerc.Focus()
                Return False
            ElseIf CInt(cboPaymentMode.SelectedValue) = enPaymentMode.CHEQUE OrElse CInt(cboPaymentMode.SelectedValue) = enPaymentMode.TRANSFER Then
                If CInt(cboBankGroup.SelectedValue) <= 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 7, "Please select Bank Group. Bank Group is mandatory information."), enMsgBoxStyle.Information)
                    cboBankGroup.Focus()
                    Return False
                ElseIf CInt(cboBranch.SelectedValue) <= 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 8, "Please select Bank Branch. Bank Branch is mandatory information."), enMsgBoxStyle.Information)
                    cboBranch.Focus()
                    Return False
                    'Sohail (21 Jul 2012) -- Start
                    'TRA - ENHANCEMENT
                ElseIf CInt(cboAccountNo.SelectedValue) <= 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 16, "Please select Bank Account. Bank Account is mandatory information."), enMsgBoxStyle.Information)
                    cboAccountNo.Focus()
                    Return False
                    'Sohail (21 Jul 2012) -- End
                ElseIf txtChequeNo.Text.Trim = "" Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 9, "Please enter Cheque No., Cheque No. is mandatory information."), enMsgBoxStyle.Information)
                    txtChequeNo.Focus()
                    Return False
                End If
            End If
            If lvEmployeeList.CheckedItems.Count <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 10, "Please check atleast one employee to make Payment."), enMsgBoxStyle.Information)
                txtChequeNo.Focus()
                Return False
                'Sohail (18 Dec 2010) -- Start
                'Issue : Allow Zero Value Salary to be paid
                'ElseIf txtTotalPaymentAmount.Decimal <= 0 Then
                '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 11, "Sorry, Total Payment Amount should be greater then Zero."), enMsgBoxStyle.Information)
                '    txtChequeNo.Focus()
                '    Return False
                'Sohail (18 Dec 2010) -- End
            End If
            'Sohail (16 Jul 2012) -- Start
            'TRA - ENHANCEMENT
            If CInt(cboPaymentMode.SelectedValue) = enPaymentMode.CHEQUE OrElse CInt(cboPaymentMode.SelectedValue) = enPaymentMode.TRANSFER Then
                Dim strEmpIDs As String = ""
                'Sohail (21 Apr 2014) -- Start
                'Enhancement - Salary Distribution by Amount and bank priority.
                'For Each lvItem As ListViewItem In lvEmployeeList.CheckedItems
                '    If strEmpIDs = "" Then
                '        strEmpIDs = lvItem.Tag.ToString
                '    Else
                '        strEmpIDs += "," & lvItem.Tag.ToString
                '    End If
                'Next
                Dim lst As List(Of String) = (From lv In lvEmployeeList.CheckedItems.Cast(Of ListViewItem)() Select (lv.Tag.ToString)).ToList
                strEmpIDs = String.Join(",", lst.ToArray)
                'Sohail (21 Apr 2014) -- End

                'Sohail (21 Aug 2015) -- Start
                'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                'ds = objEmpBank.GetEmployeeWithOverDistribution("List", strEmpIDs)

                'Nilay (10-Feb-2016) -- Start
                'ds = objEmpBank.GetEmployeeWithOverDistribution(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), ConfigParameter._Object._UserAccessModeSetting, True, True, False, "", "List", strEmpIDs)
                ds = objEmpBank.GetEmployeeWithOverDistribution(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, mdtPayPeriodStartDate, mdtPayPeriodEndDate, ConfigParameter._Object._UserAccessModeSetting, True, True, False, "", "List", strEmpIDs)
                'Nilay (10-Feb-2016) -- End

                'Sohail (21 Aug 2015) -- End
                If ds.Tables("List").Rows.Count > 0 Then
                    'Sohail (02 Oct 2014) -- Start
                    'Enhancement - Show employee name in message.
                    'eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 15, "Sorry! Some of the selected employees are having Total Salary Distribution percentage Not Equal to 100 in Employee Bank."), enMsgBoxStyle.Information)
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 15, "Sorry! Some of the selected employees including") & " " & ds.Tables("List").Rows(0).Item("employeename").ToString & " " & Language.getMessage(mstrModuleName, 18, "are having Total Salary Distribution percentage Not Equal to 100 in Employee Bank."), enMsgBoxStyle.Information)
                    'Sohail (02 Oct 2014) -- End
                    cboPaymentMode.Focus()
                    Exit Function
                End If
            End If
            'Sohail (16 Jul 2012) -- End

            'Sohail (29 Jun 2012) -- Start
            'TRA - ENHANCEMENT
            If ConfigParameter._Object._DoNotAllowOverDeductionForPayment = True Then
                dtTable = (New clsTnALeaveTran).GetOverDeductionList("List", CInt(cboPayPeriod.SelectedValue), False)
                If dtTable.Rows.Count > 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 14, "Sorry! You can not do Payment now. Reason : Some of the employees have been Over Deducted this month."), enMsgBoxStyle.Information)
                    Return False
                End If
            End If
            'Sohail (29 Jun 2012) -- End

            'Sohail (28 May 2014) -- Start
            'Enhancement - Staff Requisition.
            If gobjEmailList.Count > 0 AndAlso ConfigParameter._Object._SetPayslipPaymentApproval = True Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 17, "Sending Email(s) process is in progress from other module. Please wait."), enMsgBoxStyle.Information)
                Return False
            End If
            'Sohail (28 May 2014) -- End

            Return True
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "IsValid", mstrModuleName)
        End Try
    End Function

    Private Function CreateDatatable() As Boolean
        Try
            mdtTable = New DataTable
            With mdtTable

                .Columns.Add(New DataColumn("voucherno", Type.GetType("System.String")))
                .Columns.Add(New DataColumn("periodunkid", Type.GetType("System.Int32")))
                .Columns.Add(New DataColumn("paymentdate", Type.GetType("System.DateTime")))
                .Columns.Add(New DataColumn("employeeunkid", Type.GetType("System.Int32")))
                .Columns.Add(New DataColumn("Paymentrefid", Type.GetType("System.Int32")))
                .Columns.Add(New DataColumn("Paymentmodeid", Type.GetType("System.Int32")))
                .Columns.Add(New DataColumn("Branchunkid", Type.GetType("System.Int32")))
                .Columns.Add(New DataColumn("Chequeno", Type.GetType("System.String")))
                .Columns.Add(New DataColumn("Paymentbyid", Type.GetType("System.Int32")))
                .Columns.Add(New DataColumn("Percentage", Type.GetType("System.Decimal")))
                .Columns.Add(New DataColumn("Amount", Type.GetType("System.Decimal"))) 'Sohail (11 May 2011)
                .Columns.Add(New DataColumn("Voucherref", Type.GetType("System.String")))
                .Columns.Add(New DataColumn("Payreftranunkid", Type.GetType("System.Int32")))
                .Columns.Add(New DataColumn("Userunkid", Type.GetType("System.Int32")))
                .Columns.Add(New DataColumn("Isvoid", Type.GetType("System.Boolean")))
                .Columns.Add(New DataColumn("Voiduserunkid", Type.GetType("System.Int32")))
                .Columns.Add(New DataColumn("Voiddatetime", Type.GetType("System.DateTime")))
                .Columns.Add(New DataColumn("PaymentTypeId", Type.GetType("System.Int32")))
                .Columns.Add(New DataColumn("IsReceipt", Type.GetType("System.Int32")))
                .Columns.Add(New DataColumn("Voidreason", Type.GetType("System.String")))
                .Columns.Add(New DataColumn("Isglobalpayment", Type.GetType("System.Boolean")))
                'Sohail (08 Oct 2011) -- Start
                .Columns.Add(New DataColumn("basecurrencyid", Type.GetType("System.Int32")))
                .Columns.Add(New DataColumn("baseexchangerate", Type.GetType("System.Decimal")))
                .Columns.Add(New DataColumn("paidcurrencyid", Type.GetType("System.Int32")))
                .Columns.Add(New DataColumn("expaidrate", Type.GetType("System.Decimal")))
                .Columns.Add(New DataColumn("expaidamt", Type.GetType("System.Decimal")))
                'Sohail (08 Oct 2011) -- End
                .Columns.Add(New DataColumn("Accountno", Type.GetType("System.String"))) 'Sohail (21 Jul 2012)
                .Columns.Add(New DataColumn("countryunkid", Type.GetType("System.Int32"))) 'Sohail (03 Sep 2012)
                .Columns.Add("isapproved", Type.GetType("System.Boolean")).DefaultValue = False 'Sohail (13 Feb 2013)
                'Sohail (22 Oct 2013) -- Start
                'TRA - ENHANCEMENT
                .Columns.Add("roundingtypeid", Type.GetType("System.Int32")).DefaultValue = 0
                .Columns.Add("roundingmultipleid", Type.GetType("System.Int32")).DefaultValue = 0
                'Sohail (22 Oct 2013) -- End
                .Columns.Add("roundingadjustment", Type.GetType("System.Decimal")).DefaultValue = 0 'Sohail (21 Mar 2014)
                .Columns.Add("remarks", Type.GetType("System.String")).DefaultValue = "" 'Sohail (24 Dec 2014)
                .Columns.Add(New DataColumn("paymentdate_periodunkid", Type.GetType("System.Int32"))) 'Sohail (07 May 2015)
                .Columns.Add(New DataColumn("default_costcenterunkid", Type.GetType("System.Int32"))) 'Sohail (30 Oct 2018)
            End With

            Return True
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "CreateDatatable", mstrModuleName)
        End Try
    End Function

    'Sohail (18 Jun 2019) -- Start
    'Eko Supreme Nigeria Ltd - Support issue # 0003911 - 74.1 : User able to see Payslip l Eko Supreme Nigeria Ltd.
    Private Sub SetVisibility()
        Try
            btnProcess.Enabled = User._Object.Privilege._AllowGlobalPayment
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetVisibility", mstrModuleName)
        End Try
    End Sub
    'Sohail (18 Jun 2019) -- End
#End Region

#Region " Form's Events "

    Private Sub frmPayslipGlobalPayment_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        Try
            'objSalary = Nothing
            objPaymentTran = Nothing 'Sohail (15 Dec 2010)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmPayslipGlobalPayment_FormClosed", mstrModuleName)
        End Try
    End Sub

    Private Sub frmPayslipGlobalPayment_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown
        Try
            Select Case e.KeyCode
                Case Keys.Return
                    SendKeys.Send("{TAB}")
                    e.Handled = True
            End Select
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmPayslipGlobalPayment_KeyDown", mstrModuleName)
        End Try
    End Sub

    Private Sub frmPayslipGlobalPayment_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        objPaymentTran = New clsPayment_tran
        Try
            Call Set_Logo(Me, gApplicationType)

            Call Language.setLanguage(Me.Name)
            Call OtherSettings()

            Call SetColor()
            Call FillCombo()
            'Sohail (22 Oct 2013) -- Start
            'TRA - ENHANCEMENT
            'lblMessage.Text = Language.getMessage(mstrModuleName, 13, "Please make payment first for Overdeduction or Zero Balance Payslip(s). If you do not make payment, then deduction for Loan/Advance/Savings/Tax deduction will not be deducted from the Employee(s) Salary.") 'Sohail (25 Jan 2011)
            Dim decRondingMultile As Decimal = 0
            Select Case CInt(ConfigParameter._Object._PaymentRoundingMultiple)
                Case enPaymentRoundingMultiple._1
                    decRondingMultile = 1
                Case enPaymentRoundingMultiple._5
                    decRondingMultile = 5
                Case enPaymentRoundingMultiple._10
                    decRondingMultile = 10
                Case enPaymentRoundingMultiple._50
                    decRondingMultile = 50
                Case enPaymentRoundingMultiple._100
                    decRondingMultile = 100
                    'Sohail (08 Apr 2016) -- Start
                    'Issue - 58.1 - Allow Rounding to 200 for Payslip Payment Rounding Multiple option for Mabibo.
                Case enPaymentRoundingMultiple._200
                    decRondingMultile = 200
                    'Sohail (08 Apr 2016) -- End
                Case enPaymentRoundingMultiple._500
                    decRondingMultile = 500
                Case enPaymentRoundingMultiple._1000
                    decRondingMultile = 1000
            End Select
            Select Case CInt(ConfigParameter._Object._PaymentRoundingType)
                Case enPaymentRoundingType.AUTOMATIC
                    lblMessage.Text = "Payment Rounding Type : Automatic, Rounding To : " & decRondingMultile.ToString
                Case enPaymentRoundingType.UP
                    lblMessage.Text = "Payment Rounding Type : Up, Rounding To : " & decRondingMultile.ToString
                Case enPaymentRoundingType.DOWN
                    lblMessage.Text = "Payment Rounding Type : Down, Rounding To : " & decRondingMultile.ToString
                Case Else
                    lblMessage.Text = ""
            End Select
            'Sohail (22 Oct 2013) -- End

            'Sohail (15 Dec 2010) -- Start
            If ConfigParameter._Object._PaymentVocNoType = 1 Then 'AutoIncrement
                txtVoucher.Enabled = False
            Else
                txtVoucher.Enabled = True
            End If
            'Sohail (15 Dec 2010) -- End
            'Sohail (18 Jun 2019) -- Start
            'Eko Supreme Nigeria Ltd - Support issue # 0003911 - 74.1 : User able to see Payslip l Eko Supreme Nigeria Ltd.
            Call SetVisibility()
            'Sohail (18 Jun 2019) -- End
            'Hemant (17 Oct 2019) -- Start
            lnkPayrollVarianceReport.Visible = clsArutiReportClass.Is_Report_Assigned(enArutiReport.PayrollVariance, User._Object._Userunkid, Company._Object._Companyunkid)
            lnkPayrollTotalVarianceReport.Visible = clsArutiReportClass.Is_Report_Assigned(enArutiReport.PayrollTotalVariance, User._Object._Userunkid, Company._Object._Companyunkid)
            'Hemant (17 Oct 2019) -- End
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmPayslipGlobalPayment_Load", mstrModuleName)
        End Try
    End Sub

    'Anjan (02 Sep 2011)-Start
    'Issue : Including Language Settings.
    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub
    'Anjan (02 Sep 2011)-End 

#End Region

#Region " ComboBox's Events "
    Private Sub cboPayYear_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboPayYear.SelectedIndexChanged 'Sohail (03 Nov 2010)
        Dim objPeriod As New clscommom_period_Tran
        Dim dsCombo As DataSet = Nothing

        Try
            'Sohail (11 Jan 2014) -- Start
            'Enhancement - PayPoint, PayType filter on Global Payment
            'dsCombo = objPeriod.getListForCombo(enModuleReference.Payroll, CInt(cboPayYear.SelectedValue), "PayPeriod", True, enStatusType.Open, True) 'Sohail (28 Dec 2010), 'Sohail (04 Mar 2011) - [get only open period]
            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'dsCombo = objPeriod.getListForCombo(enModuleReference.Payroll, CInt(cboPayYear.SelectedValue), "PayPeriod", True, enStatusType.Open, False)
            dsCombo = objPeriod.getListForCombo(enModuleReference.Payroll, CInt(cboPayYear.SelectedValue), FinancialYear._Object._DatabaseName, FinancialYear._Object._Database_Start_Date, "PayPeriod", True, enStatusType.Open, False)
            'Sohail (21 Aug 2015) -- End
            'Sohail (11 Jan 2014) -- End
            With cboPayPeriod
                .ValueMember = "periodunkid"
                .DisplayMember = "Name"
                .DataSource = dsCombo.Tables("PayPeriod")
                If .Items.Count > 0 Then .SelectedValue = 0
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboPayYear_SelectedIndexChanged", mstrModuleName)
        Finally
            objPeriod = Nothing
            dsCombo = Nothing
        End Try
    End Sub

    Private Sub cboPayPeriod_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboPayPeriod.SelectedIndexChanged 'Sohail (03 Nov 2010)
        Dim objPeriod As New clscommom_period_Tran
        Dim dsList As DataSet = Nothing
        Try
            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'objPeriod._Periodunkid = CInt(cboPayPeriod.SelectedValue)
            objPeriod._Periodunkid(FinancialYear._Object._DatabaseName) = CInt(cboPayPeriod.SelectedValue)
            'Sohail (21 Aug 2015) -- End
            mdtPayPeriodStartDate = objPeriod._Start_Date
            mdtPayPeriodEndDate = objPeriod._End_Date

            If objchkSelectAll.Checked = True Then objchkSelectAll.Checked = False
            'Sohail (06 Jan 2012) -- Start
            'TRA - ENHANCEMENT
            Call FillEmployeeCombo()
            'Sohail (06 Jan 2012) -- End
            Call FillList()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboPayPeriod_SelectedIndexChanged", mstrModuleName)
        Finally
            objPeriod = Nothing
            dsList = Nothing
        End Try
    End Sub

    'Sohail (07 May 2015) -- Start
    'Enhancement - New Loan Redesign with allow to change interest rate, tenure, topup, receipt.
    Private Sub cboPaymentDatePeriod_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboPaymentDatePeriod.SelectedIndexChanged
        Dim objPeriod As New clscommom_period_Tran
        Dim dsList As DataSet = Nothing
        Try
            'Nilay (10-Oct-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'objPeriod._Periodunkid = CInt(cboPaymentDatePeriod.SelectedValue)
            objPeriod._Periodunkid(FinancialYear._Object._DatabaseName) = CInt(cboPaymentDatePeriod.SelectedValue)
            'Nilay (10-Oct-2015) -- End

            mdtPaymentDatePeriodStartDate = objPeriod._Start_Date
            mdtPaymentDatePeriodEndDate = objPeriod._End_Date
            'Sohail (07 May 2015) -- Start
            'Enhancement - New Loan Redesign with allow to change interest rate, tenure, topup, receipt.
            If CInt(cboPaymentDatePeriod.SelectedValue) > 0 Then
                'Sohail (14 Dec 2018) -- Start
                'Twaweza Issue : Payment Time not coming on payroll report prepared by.       
                'dtpPaymentDate.Value = objPeriod._End_Date
                'Hemant (19 jan 2019) -- Start
                'Issue - 74.1 - On Payroll Report, Prepared By Date (Payment date) is coming as Future date (31st Jan 2019) even if payment is done on before period end date (20th  Jan 2019)
                'dtpPaymentDate.Value = objPeriod._End_Date.AddMinutes(DateDiff(DateInterval.Minute, DateAndTime.Now.Date, Now))
                If eZeeDate.convertDate(ConfigParameter._Object._CurrentDateAndTime) < eZeeDate.convertDate(objPeriod._Start_Date) Then
                    dtpPaymentDate.Value = objPeriod._Start_Date.AddMinutes(DateDiff(DateInterval.Minute, DateAndTime.Now.Date, Now))
                ElseIf eZeeDate.convertDate(ConfigParameter._Object._CurrentDateAndTime) > eZeeDate.convertDate(objPeriod._End_Date) Then
                dtpPaymentDate.Value = objPeriod._End_Date.AddMinutes(DateDiff(DateInterval.Minute, DateAndTime.Now.Date, Now))
                ElseIf eZeeDate.convertDate(ConfigParameter._Object._CurrentDateAndTime) > eZeeDate.convertDate(objPeriod._Start_Date) AndAlso eZeeDate.convertDate(ConfigParameter._Object._CurrentDateAndTime) < eZeeDate.convertDate(objPeriod._End_Date) Then
                    dtpPaymentDate.Value = ConfigParameter._Object._CurrentDateAndTime
                Else
                    dtpPaymentDate.Value = objPeriod._End_Date.AddMinutes(DateDiff(DateInterval.Minute, DateAndTime.Now.Date, Now))
                End If
                'Hemant (19 jan 2019) -- End

                'Sohail (14 Dec 2018) -- End
            End If
            'Sohail (07 May 2015) -- End

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboPaymentDatePeriod_SelectedIndexChanged", mstrModuleName)
        Finally
            objPeriod = Nothing
            dsList = Nothing
        End Try
    End Sub
    'Sohail (07 May 2015) -- End

    Private Sub cboBankGroup_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboBankGroup.SelectedIndexChanged 'Sohail (15 Dec 2010)
        Dim dsCombos As New DataSet
        'Sohail (16 Jul 2012) -- Start
        'TRA - ENHANCEMENT
        'Dim objBranch As New clsbankbranch_master
        Dim objCompanyBank As New clsCompany_Bank_tran
        'Sohail (16 Jul 2012) -- End
        Try
            'Sohail (16 Jul 2012) -- Start
            'TRA - ENHANCEMENT
            'dsCombos = objBranch.getListForCombo("Branch", True, CInt(cboBankGroup.SelectedValue))
            'With cboBranch
            '    .ValueMember = "branchunkid"
            '    .DisplayMember = "name"
            '    .DataSource = dsCombos.Tables("Branch")
            '    'If mintPaymentTranId = -1 Then
            '    '    .SelectedValue = 0
            '    'Else
            '    '    .SelectedValue = objPaymentTran._Branchunkid
            '    'End If
            'End With
            dsCombos = objCompanyBank.GetComboList(Company._Object._Companyunkid, 2, "Branch", " cfbankbranch_master.bankgroupunkid = " & CInt(cboBankGroup.SelectedValue) & " ")
            With cboBranch
                .ValueMember = "id"
                .DisplayMember = "name"
                .DataSource = dsCombos.Tables("Branch")
                .SelectedValue = 0
            End With
            'Sohail (16 Jul 2012) -- End
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboBankGroup_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub cboPaymentMode_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboPaymentMode.SelectedIndexChanged 'Sohail (15 Dec 2010)
        Try

            Select Case CInt(cboPaymentMode.SelectedValue)
                'Sohail (15 Dec 2010) -- Start
                'Case enPaymentMode.CASH
                Case enPaymentMode.CASH, enPaymentMode.CASH_AND_CHEQUE
                    'Sohail (15 Dec 2010) -- End
                    cboBankGroup.SelectedValue = 0
                    cboBankGroup.Enabled = False
                    cboBranch.SelectedValue = 0
                    cboBranch.Enabled = False
                    txtChequeNo.Text = ""
                    txtChequeNo.Enabled = False
                    txtCashPerc.Text = "100.00"

                    lblCashDescr.Visible = True
                    'Sohail (15 Dec 2010) -- Start
                    'lblCashPerc.Visible = True
                    'txtCashPerc.Visible = True
                    txtCashPerc.Enabled = True
                    Select Case CInt(cboPaymentMode.SelectedValue)
                        Case enPaymentMode.CASH
                            lblACHolder.Text = "To Non Bank A/C Holder Only"
                        Case enPaymentMode.CASH_AND_CHEQUE
                            lblACHolder.Text = "Cash Payment To All Employee"
                    End Select
                    'Sohail (15 Dec 2010) -- End
                    'Sohail (21 Jul 2012) -- Start
                    'TRA - ENHANCEMENT
                    cboAccountNo.SelectedValue = 0
                    cboAccountNo.Enabled = False
                    'Sohail (21 Jul 2012) -- End
                Case Else
                    cboBankGroup.Enabled = True
                    cboBranch.Enabled = True
                    txtChequeNo.Enabled = True
                    'Sohail (21 Apr 2020) -- Start
                    'CCBRT Enhancement # 0004659 : Assist to Enable to do Partial Payment by using Both modes of Payment i.e cheque,cash and Transfe.
                    'lblCashDescr.Visible = False
                    lblCashDescr.Visible = True
                    'Sohail (21 Apr 2020) -- End
                    'Sohail (15 Dec 2010) -- Start
                    'lblCashPerc.Visible = False
                    'txtCashPerc.Visible = False
                    txtCashPerc.Text = "100.00"
                    'Sohail (21 Apr 2020) -- Start
                    'CCBRT Enhancement # 0004659 : Assist to Enable to do Partial Payment by using Both modes of Payment i.e cheque,cash and Transfe.
                    'txtCashPerc.Enabled = False
                    txtCashPerc.Enabled = True
                    'Sohail (21 Apr 2020) -- End
                    Select Case CInt(cboPaymentMode.SelectedValue)
                        Case enPaymentMode.CHEQUE, enPaymentMode.TRANSFER
                            lblACHolder.Text = "To Bank A/C Holder Only"
                        Case Else
                            lblACHolder.Text = ""
                    End Select
                    'Sohail (15 Dec 2010) -- End
                    cboAccountNo.Enabled = True 'Sohail (21 Jul 2012)
            End Select
            If objchkSelectAll.Checked = True Then objchkSelectAll.Checked = False
            Call FillList()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboPaymentMode_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

    'Sohail (08 Oct 2011) -- Start
    Private Sub cboCurrency_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboCurrency.SelectedIndexChanged _
                                                                                                                    , dtpPaymentDate.ValueChanged
        Dim objExRate As New clsExchangeRate
        Dim dsList As DataSet
        Dim dtTable As DataTable
        Try
            objlblExRate.Text = ""
            lblTotalPaymentAmount.Text = "Total Payment Amt."
            'Sohail (03 Sep 2012) -- Start
            'TRA - ENHANCEMENT
            'dsList = objExRate.GetList("ExRate", True)
            'dtTable = New DataView(dsList.Tables("ExRate"), "exchangerateunkid = " & CInt(cboCurrency.SelectedValue) & " ", "", DataViewRowState.CurrentRows).ToTable
            dsList = objExRate.GetList("ExRate", True, , , CInt(cboCurrency.SelectedValue), True, dtpPaymentDate.Value, True)
            dtTable = New DataView(dsList.Tables("ExRate")).ToTable
            'Sohail (03 Sep 2012) -- End
            If dtTable.Rows.Count > 0 Then
                mdecBaseExRate = CDec(dtTable.Rows(0).Item("exchange_rate1"))
                mdecPaidExRate = CDec(dtTable.Rows(0).Item("exchange_rate2"))
                mintPaidCurrId = CInt(dtTable.Rows(0).Item("exchangerateunkid"))
                objlblExRate.Text = Format(mdecBaseExRate, GUI.fmtCurrency) & " " & mstrBaseCurrSign & " = " & mdecPaidExRate.ToString & " " & dtTable.Rows(0).Item("currency_sign").ToString & " "
                lblTotalPaymentAmount.Text &= " (" & dtTable.Rows(0).Item("currency_sign").ToString & ")"
                'Sohail (03 Sep 2012) -- Start
                'TRA - ENHANCEMENT
            Else
                mdecBaseExRate = 0
                mdecPaidExRate = 0
                'Sohail (03 Sep 2012) -- End
            End If
            Call FillList()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboCurrency_SelectedIndexChanged", mstrModuleName)
        End Try

    End Sub
    'Sohail (08 Oct 2011) -- End

    'Sohail (21 Jul 2012) -- Start
    'TRA - ENHANCEMENT
    Private Sub cboBranch_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboBranch.SelectedIndexChanged
        Dim objCompanyBank As New clsCompany_Bank_tran
        Dim dsCombos As New DataSet
        Try
            dsCombos = objCompanyBank.GetComboListBankAccount("Account", Company._Object._Companyunkid, True, CInt(cboBankGroup.SelectedValue), CInt(cboBranch.SelectedValue))
            With cboAccountNo
                .ValueMember = "id"
                .DisplayMember = "Name"
                .DataSource = dsCombos.Tables("Account")
                .SelectedValue = 0
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboBranch_SelectedIndexChanged", mstrModuleName)
        Finally
            objCompanyBank = Nothing
        End Try
    End Sub
    'Sohail (21 Jul 2012) -- End

    'Sohail (07 Dec 2013) -- Start
    'Enhancement - OMAN
    Private Sub cboEmpBank_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboEmpBank.SelectedIndexChanged
        Dim objBankBranch As New clsbankbranch_master
        Dim dsList As New DataSet
        Try
            dsList = objBankBranch.getListForCombo("List", True, CInt(cboEmpBank.SelectedValue))
            With cboEmpBranch
                .ValueMember = "branchunkid"
                .DisplayMember = "name"
                .DataSource = dsList.Tables("List")
                .SelectedValue = 0
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboEmpBank_SelectedIndexChanged", mstrModuleName)
        Finally
            objBankBranch = Nothing
            dsList.Dispose()
            dsList = Nothing
        End Try
    End Sub
    'Sohail (07 Dec 2013) -- End

#End Region

#Region " Button's Events "
    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnClose_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnProcess_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnProcess.Click
        Dim blnFlag As Boolean = False
        Dim dtRow As DataRow
        Dim strEmpIDs As String = "" 'Sohail (18 Dec 2010)
        'Sohail (20 Feb 2013) -- Start
        'TRA - ENHANCEMENT
        Dim strEmployeeIDs As String = ""
        Dim mintBankPaid As Integer = 0
        Dim mintCashPaid As Integer = 0
        Dim decBankPaid As Decimal = 0
        Dim decCashPaid As Decimal = 0
        'Sohail (20 Feb 2013) -- End
        'Nilay (01-Apr-2016) -- Start
        'ENHANCEMENT - Approval Process in Loan Other Operations
        Dim objlnOtherOpApprovalTran As New clsloanotherop_approval_tran
        'Nilay (01-Apr-2016) -- End

        Try
            If IsValid() = False Then Exit Sub

            If CreateDatatable() = False Then Exit Sub

            'Sohail (15 Dec 2010) -- Start
            Call SetValue()
            'Sohail (15 Dec 2010) -- End

            For Each lvItem As ListViewItem In lvEmployeeList.CheckedItems
                With mdtTable
                    dtRow = .NewRow()
                    dtRow.Item("voucherno") = txtVoucher.Text.Trim
                    dtRow.Item("periodunkid") = CInt(cboPayPeriod.SelectedValue)
                    dtRow.Item("paymentdate") = dtpPaymentDate.Value
                    dtRow.Item("employeeunkid") = CInt(lvItem.Tag)
                    dtRow.Item("Paymentrefid") = clsPayment_tran.enPaymentRefId.PAYSLIP
                    'dtRow.Item("Paymentmodeid") = CInt(cboPaymentMode.SelectedValue) 'Sohail (28 Dec 2010)
                    dtRow.Item("Branchunkid") = CInt(cboBranch.SelectedValue)
                    dtRow.Item("Chequeno") = txtChequeNo.Text.Trim
                    If CInt(cboPaymentMode.SelectedValue) = enPaymentMode.CASH OrElse CInt(cboPaymentMode.SelectedValue) = enPaymentMode.CASH_AND_CHEQUE Then 'Sohail (15 Dec 2010)
                        dtRow.Item("Paymentmodeid") = enPaymentMode.CASH 'Sohail (28 Dec 2010)
                        dtRow.Item("Paymentbyid") = enPaymentBy.Percentage
                        dtRow.Item("Percentage") = txtCashPerc.Decimal
                        'Sohail (08 Oct 2011) -- Start
                        'dtRow.Item("Amount") = CDec(lvItem.SubItems(colhAmount.Index).Tag) * txtCashPerc.Decimal / 100 'Sohail (11 May 2011)
                        dtRow.Item("Amount") = CDec(lvItem.SubItems(colhBaseAmount.Index).Text) * txtCashPerc.Decimal / 100
                        'Sohail (08 Oct 2011) -- End


                        'Sohail (18 Dec 2010) -- Start
                        If CDec(dtRow.Item("Amount")) > 0 Then 'Sohail (11 May 2011)
                            If strEmpIDs.Trim.Length = 0 Then
                                strEmpIDs = lvItem.Tag.ToString
                            Else
                                strEmpIDs &= "," & lvItem.Tag.ToString
                            End If
                        End If
                        'Sohail (18 Dec 2010) -- End

                        mintCashPaid += 1 'Sohail (20 Feb 2013)
                        'Sohail (21 Feb 2017) -- Start
                        'TRA Issue - 64.1 - Global Payment Total is not matching with Payroll Summary Net Pay and JV Net Pay account.
                        'decCashPaid += CDec(lvItem.SubItems(colhAmount.Index).Text) 'Sohail (23 Feb 2013)
                        decCashPaid += CDec(lvItem.SubItems(colhAmount.Index).Tag)
                        'Sohail (21 Feb 2017) -- End
                    Else
                        dtRow.Item("Paymentmodeid") = CInt(cboPaymentMode.SelectedValue) 'Sohail (28 Dec 2010)
                        dtRow.Item("Paymentbyid") = enPaymentBy.Value
                        dtRow.Item("Percentage") = 0
                        'Sohail (08 Nov 2011) -- Start
                        'dtRow.Item("Amount") = CDec(lvItem.SubItems(colhAmount.Index).Tag)
                        'Sohail (21 Apr 2020) -- Start
                        'CCBRT Enhancement # 0004659 : Assist to Enable to do Partial Payment by using Both modes of Payment i.e cheque,cash and Transfe.
                        'dtRow.Item("Amount") = CDec(lvItem.SubItems(colhBaseAmount.Index).Text)
                        dtRow.Item("Amount") = CDec(lvItem.SubItems(colhBaseAmount.Index).Text) * txtCashPerc.Decimal / 100
                        'Sohail (21 Apr 2020) -- End
                        'Sohail (08 Nov 2011) -- End

                        mintBankPaid += 1 'Sohail (20 Feb 2013)
                        'Sohail (21 Feb 2017) -- Start
                        'TRA Issue - 64.1 - Global Payment Total is not matching with Payroll Summary Net Pay and JV Net Pay account.
                        'decBankPaid += CDec(lvItem.SubItems(colhAmount.Index).Text) 'Sohail (23 Feb 2013)
                        decBankPaid += CDec(lvItem.SubItems(colhAmount.Index).Tag)
                        'Sohail (21 Feb 2017) -- End
                    End If
                    dtRow.Item("Voucherref") = lvItem.SubItems(colhVoucherNo.Index).Text
                    dtRow.Item("Payreftranunkid") = CInt(lvItem.SubItems(colhTnAleaveUnkID.Index).Text)
                    dtRow.Item("Userunkid") = User._Object._Userunkid
                    dtRow.Item("Isvoid") = False
                    dtRow.Item("Voiduserunkid") = 0
                    dtRow.Item("Voiddatetime") = DBNull.Value
                    dtRow.Item("PaymentTypeId") = clsPayment_tran.enPayTypeId.PAYMENT
                    dtRow.Item("IsReceipt") = False
                    dtRow.Item("Voidreason") = ""
                    dtRow.Item("Isglobalpayment") = True
                    'Sohail (08 Oct 2011) -- Start
                    dtRow.Item("basecurrencyid") = mintBaseCurrId
                    dtRow.Item("baseexchangerate") = mdecBaseExRate
                    dtRow.Item("paidcurrencyid") = mintPaidCurrId
                    dtRow.Item("expaidrate") = mdecPaidExRate
                    'dtRow.Item("expaidamt") = CDec(lvItem.SubItems(colhAmount.Index).Tag) * txtCashPerc.Decimal / 100
                    'Sohail (21 Feb 2017) -- Start
                    'TRA Issue - 64.1 - Global Payment Total is not matching with Payroll Summary Net Pay and JV Net Pay account.
                    'dtRow.Item("expaidamt") = CDec(lvItem.SubItems(colhAmount.Index).Text) * txtCashPerc.Decimal / 100
                    dtRow.Item("expaidamt") = CDec(lvItem.SubItems(colhAmount.Index).Tag) * txtCashPerc.Decimal / 100
                    'Sohail (21 Feb 2017) -- End
                    'Sohail (08 Oct 2011) -- End
                    If CInt(cboAccountNo.SelectedValue) > 0 Then
                        dtRow.Item("Accountno") = cboAccountNo.Text 'Sohail (21 Jul 2012)
                    Else
                        dtRow.Item("Accountno") = "" 'Sohail (21 Jul 2012)
                    End If
                    dtRow.Item("countryunkid") = CInt(cboCurrency.SelectedValue) 'Sohail (03 Sep 2012)
                    dtRow.Item("isapproved") = False 'Sohail (13 Feb 2013)
                    'Sohail (22 Oct 2013) -- Start
                    'TRA - ENHANCEMENT
                    dtRow.Item("roundingtypeid") = ConfigParameter._Object._PaymentRoundingType
                    dtRow.Item("roundingmultipleid") = ConfigParameter._Object._PaymentRoundingMultiple
                    'Sohail (22 Oct 2013) -- End
                    dtRow.Item("roundingadjustment") = CDec(lvItem.SubItems(colhCode.Index).Tag) 'Sohail (21 Mar 2014)
                    dtRow.Item("remarks") = txtRemarks.Text.Trim 'Sohail (24 Dec 2014)
                    dtRow.Item("paymentdate_periodunkid") = CInt(cboPaymentDatePeriod.SelectedValue)

                    'Sohail (30 Oct 2018) -- Start
                    'FDRC Enhancement - Global Payment performance issue on FDRC database in 75.1.
                    dtRow.Item("default_costcenterunkid") = CInt(lvItem.SubItems(colhVoucherNo.Index).Tag)
                    'Sohail (30 Oct 2018) -- End

                    'Sohail (20 Feb 2013) -- Start
                    'TRA - ENHANCEMENT
                    If strEmployeeIDs.Trim = "" Then
                        strEmployeeIDs = lvItem.Tag.ToString
                    Else
                        strEmployeeIDs &= "," & lvItem.Tag.ToString
                    End If
                    'Sohail (20 Feb 2013) -- End

                    .Rows.Add(dtRow)
                End With
            Next

            'Nilay (01-Apr-2016) -- Start
            'ENHANCEMENT - Approval Process in Loan Other Operations
            If ConfigParameter._Object._IsArutiDemo = True Then
                blnFlag = objlnOtherOpApprovalTran.IsLoanParameterApprovalPending(CInt(cboPayPeriod.SelectedValue), strEmployeeIDs)
                If blnFlag = False Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 20, "Sorry. Some of Operation(s) either Interest Rate/EMI Tenure/Topup Amount are still pending for Approval process in current pay period. In order to process Payment, please either Approve or Reject pending operations for selected pay period."), enMsgBoxStyle.Information)
                    Exit Sub
                End If
            Else
                If CBool(ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Loan_and_Savings_Management)) = True Then
                    blnFlag = objlnOtherOpApprovalTran.IsLoanParameterApprovalPending(CInt(cboPayPeriod.SelectedValue), strEmployeeIDs)
                    If blnFlag = False Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 20, "Sorry. Some of Operation(s) either Interest Rate/EMI Tenure/Topup Amount are still pending for Approval process in current pay period. In order to process Payment, please either Approve or Reject pending operations for selected pay period."), enMsgBoxStyle.Information)
                        Exit Sub
                    End If
                End If
            End If
            'Nilay (01-Apr-2016) -- End

            'Sohail (23 May 2017) -- Start
            'Enhancement - 67.1 - Link budget with Payroll.
            Dim objBudget As New clsBudget_MasterNew
            Dim intBudgetId As Integer = 0
            Dim dsList As DataSet
            Dim dtActAdj As New DataTable
            dsList = objBudget.GetComboList("List", False, True, )
            If dsList.Tables(0).Rows.Count > 0 Then
                intBudgetId = CInt(dsList.Tables(0).Rows(0).Item("budgetunkid"))
            End If
            If intBudgetId > 0 Then
                Dim mstrAnalysis_Fields As String = ""
                Dim mstrAnalysis_Join As String = ""
                Dim mstrAnalysis_OrderBy As String = "hremployee_master.employeeunkid"
                Dim mstrReport_GroupName As String = ""
                Dim mstrAnalysis_TableName As String = ""
                Dim mstrAnalysis_CodeField As String = ""
                objBudget._Budgetunkid = intBudgetId
                Dim strEmpList As String = ""
                Dim allEmp As List(Of String) = (From p In mdtTable Select (p.Item("employeeunkid").ToString)).ToList
                strEmpList = String.Join(",", allEmp.ToArray())
                Dim objBudgetCodes As New clsBudgetcodes_master
                Dim dsEmp As DataSet = Nothing
                If objBudget._Viewbyid = enBudgetViewBy.Allocation Then
                    Dim objEmp As New clsEmployee_Master
                    Dim objBudget_Tran As New clsBudget_TranNew
                    Dim mstrAllocationTranUnkIDs As String = objBudget_Tran.GetAllocationTranUnkIDs(intBudgetId)

                    frmViewAnalysis.GetAnalysisByDetails("frmViewAnalysis", objBudget._Allocationbyid, mstrAllocationTranUnkIDs, objBudget._Budget_date, "", mstrAnalysis_Fields, mstrAnalysis_Join, mstrAnalysis_OrderBy, "", mstrReport_GroupName, mstrAnalysis_TableName, mstrAnalysis_CodeField)

                    dsList = objBudgetCodes.GetEmployeeActivityPercentage(CInt(cboPayPeriod.SelectedValue), intBudgetId, 0, 0, mstrAllocationTranUnkIDs, 1)
                    dsEmp = objEmp.GetList(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, mdtPayPeriodStartDate, mdtPayPeriodEndDate, ConfigParameter._Object._UserAccessModeSetting, True, False, "List", , , , "hremployee_master.employeeunkid IN (" & strEmpList & ")")
                Else
                    dsList = objBudgetCodes.GetEmployeeActivityPercentage(CInt(cboPayPeriod.SelectedValue), intBudgetId, 0, 0, strEmpList, 1)
                End If

                If dsList.Tables(0).Rows.Count > 0 Then
                    dtActAdj.Columns.Add("fundactivityunkid", System.Type.GetType("System.Int32")).DefaultValue = 0
                    dtActAdj.Columns.Add("transactiondate", System.Type.GetType("System.String")).DefaultValue = ""
                    dtActAdj.Columns.Add("remark", System.Type.GetType("System.String")).DefaultValue = ""
                    dtActAdj.Columns.Add("isexeed", System.Type.GetType("System.Boolean")).DefaultValue = False
                    dtActAdj.Columns.Add("Activity Code", System.Type.GetType("System.String")).DefaultValue = ""
                    dtActAdj.Columns.Add("Current Balance", System.Type.GetType("System.Decimal")).DefaultValue = 0
                    dtActAdj.Columns.Add("Actual Salary", System.Type.GetType("System.Decimal")).DefaultValue = 0

                    Dim mdicActivity As New Dictionary(Of Integer, Decimal)
                    Dim mdicActivityCode As New Dictionary(Of Integer, String)
                    Dim objActivity As New clsfundactivity_Tran
                    Dim dsAct As DataSet = objActivity.GetList("Activity")
                    Dim objActAdjust As New clsFundActivityAdjustment_Tran
                    Dim dsActAdj As DataSet = objActAdjust.GetLastCurrentBalance("List", 0, mdtPayPeriodEndDate)
                    mdicActivity = (From p In dsActAdj.Tables("List") Select New With {.Id = CInt(p.Item("fundactivityunkid")), .Total = CDec(p.Item("newbalance"))}).ToDictionary(Function(x) x.Id, Function(y) y.Total)
                    mdicActivityCode = (From p In dsAct.Tables("Activity") Select New With {.Id = CInt(p.Item("fundactivityunkid")), .Code = p.Item("activity_code").ToString}).ToDictionary(Function(x) x.Id, Function(y) y.Code)
                    'Dim dsBalance As DataSet = objTnALeave.Get_Balance_List(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, mdtPayPeriodStartDate, mdtPayPeriodEndDate, ConfigParameter._Object._UserAccessModeSetting, True, False, True, "prtnaleave_tran.balanceamount > 0", "list", strEmpList, CInt(cboPayPeriod.SelectedValue), "")
                    If objBudget._Viewbyid = enBudgetViewBy.Allocation AndAlso dsEmp IsNot Nothing Then
                        mdtTable.PrimaryKey = New DataColumn() {mdtTable.Columns("employeeunkid")}
                        mdtTable.Merge(dsEmp.Tables(0))
                    End If
                    Dim intActivityId As Integer
                    Dim decTotal As Decimal = 0
                    Dim decActBal As Decimal = 0
                    Dim blnShowValidationList As Boolean = False
                    Dim blnIsExeed As Boolean = False
                    For Each pair In mdicActivityCode
                        intActivityId = pair.Key
                        decTotal = 0
                        decActBal = 0
                        blnIsExeed = False

                        If mdicActivity.ContainsKey(intActivityId) = True Then
                            decActBal = mdicActivity.Item(intActivityId)
                        End If

                        Dim lstRow As List(Of DataRow) = (From p In dsList.Tables(0) Where (CInt(p.Item("fundactivityunkid")) = intActivityId AndAlso CDec(p.Item("percentage")) > 0) Select (p)).ToList
                        If lstRow.Count > 0 Then
                            For Each dRow As DataRow In lstRow
                                Dim intAllocUnkId As Integer = CInt(dRow.Item("Employeeunkid"))
                                Dim decNetPay As Decimal = (From p In mdtTable Where (CInt(p.Item(mstrAnalysis_OrderBy.Split(CChar("."))(1).Trim)) = intAllocUnkId AndAlso IsDBNull(p.Item("Amount")) = False) Select (CDec(p.Item("Amount")))).Sum
                                decTotal += decNetPay * CDec(dRow.Item("percentage")) / 100
                            Next

                            If decTotal > decActBal Then
                                blnShowValidationList = True
                                blnIsExeed = True
                            End If
                            Dim dr As DataRow = dtActAdj.NewRow
                            dr.Item("fundactivityunkid") = intActivityId
                            dr.Item("transactiondate") = eZeeDate.convertDate(mdtPayPeriodEndDate).ToString()
                            dr.Item("remark") = Language.getMessage(mstrModuleName, 21, "Salary payments for the period of") & " " & cboPayPeriod.Text
                            dr.Item("isexeed") = blnIsExeed
                            dr.Item("Activity Code") = pair.Value
                            dr.Item("Current Balance") = Format(decActBal, GUI.fmtCurrency)
                            dr.Item("Actual Salary") = Format(decTotal, GUI.fmtCurrency)
                            dtActAdj.Rows.Add(dr)
                        End If
                    Next

                    If blnShowValidationList = True Then
                        Dim dr() As DataRow = dtActAdj.Select("isexeed = 0 ")
                        For Each r In dr
                            dtActAdj.Rows.Remove(r)
                        Next
                        dtActAdj.AcceptChanges()
                        dtActAdj.Columns.Remove("fundactivityunkid")
                        dtActAdj.Columns.Remove("transactiondate")
                        dtActAdj.Columns.Remove("remark")
                        dtActAdj.Columns.Remove("isexeed")
                        Dim objValid As New frmCommonValidationList
                        objValid.displayDialog(False, Language.getMessage(mstrModuleName, 22, "Sorry, Actual Salary Payment will be exceeding to the Activity Current Balance."), dtActAdj)
                        Exit Try
                    End If
                End If
            End If
            'Sohail (23 May 2017) -- End

            'Sohail (18 Dec 2010) -- Start
            If ConfigParameter._Object._IsDenominationCompulsory = True Then
                If strEmpIDs.Trim.Length > 0 AndAlso (CInt(cboPaymentMode.SelectedValue) = enPaymentMode.CASH OrElse CInt(cboPaymentMode.SelectedValue) = enPaymentMode.CASH_AND_CHEQUE) Then
                    If menAction = enAction.ADD_CONTINUE Then
                        Dim frm As New frmCashDenomination
                        'Sohail (12 Oct 2011) -- Start
                        'If frm.displayDialog(-1, CInt(cboPayPeriod.SelectedValue), strEmpIDs, txtCashPerc.Decimal, enAction.ADD_ONE) = False Then
                        'Sohail (03 Sep 2012) -- Start
                        'TRA - ENHANCEMENT
                        'If frm.displayDialog(-1, CInt(cboPayPeriod.SelectedValue), strEmpIDs, txtCashPerc.Decimal, enAction.ADD_ONE, mintPaidCurrId, mdecBaseExRate, mdecPaidExRate) = False Then
                        'Sohail (21 Aug 2015) -- Start
                        'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                        'If frm.displayDialog(-1, CInt(cboPayPeriod.SelectedValue), strEmpIDs, txtCashPerc.Decimal, enAction.ADD_ONE, mintPaidCurrId, mdecBaseExRate, mdecPaidExRate, CInt(cboCurrency.SelectedValue)) = False Then
                        If frm.displayDialog(-1, CInt(cboPayPeriod.SelectedValue), strEmpIDs, txtCashPerc.Decimal, enAction.ADD_ONE, mdtPayPeriodStartDate, mdtPayPeriodEndDate, mintPaidCurrId, mdecBaseExRate, mdecPaidExRate, CInt(cboCurrency.SelectedValue)) = False Then
                            'Sohail (21 Aug 2015) -- End
                            'Sohail (03 Sep 2012) -- End
                            'Sohail (12 Oct 2011) -- End
                            Exit Sub
                        End If
                        mdtDenomeTable = frm._CashDataSource
                        objPaymentTran._Denom_Table = mdtDenomeTable
                    End If
                End If
            End If
            'Sohail (18 Dec 2010) -- End

            'Sohail (30 Oct 2018) -- Start
            'FDRC Enhancement - Global Payment performance issue on FDRC database in 75.1.
            'Sohail (30 Apr 2019) -- Start
            'BBLMS Issue - Support Issue Id # 0003781 - 74.1 - Payslip global payment takes hours to process help needed urgently.
            'Dim dtStart As Date = DateAndTime.Now

            'S.SANDEEP [28-May-2018] -- START
            'ISSUE/ENHANCEMENT : {Audit Trails} 
            'objPaymentTran._FormName = mstrModuleName
            'objPaymentTran._LoginEmployeeUnkid = 0
            'objPaymentTran._ClientIP = getIP()
            'objPaymentTran._HostName = getHostName()
            'objPaymentTran._FromWeb = False
            'objPaymentTran._AuditUserId = User._Object._Userunkid
'objPaymentTran._CompanyUnkid = Company._Object._Companyunkid
            'objPaymentTran._AuditDate = ConfigParameter._Object._CurrentDateAndTime
            'S.SANDEEP [28-May-2018] -- END

            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'blnFlag = objPaymentTran.InsertAll(mdtTable)

            'Nilay (10-Feb-2016) -- Start
            'blnFlag = objPaymentTran.InsertAll(mdtTable, ConfigParameter._Object._PaymentVocNoType, FinancialYear._Object._DatabaseName, Company._Object._Companyunkid, FinancialYear._Object._YearUnkid, _
            '                                  mdtPayPeriodStartDate, mdtPayPeriodEndDate, _
            '                                  ConfigParameter._Object._UserAccessModeSetting, True, ConfigParameter._Object._IsIncludeInactiveEmp, _
            '                                  User._Object._Userunkid, ConfigParameter._Object._CurrentDateAndTime, True, "List", "")
            'Sohail (23 May 2017) -- Start
            'Enhancement - 67.1 - Link budget with Payroll.
            'blnFlag = objPaymentTran.InsertAll(mdtTable, ConfigParameter._Object._PaymentVocNoType, FinancialYear._Object._DatabaseName, Company._Object._Companyunkid, FinancialYear._Object._YearUnkid, _
            '                                  mdtPayPeriodStartDate, mdtPayPeriodEndDate, ConfigParameter._Object._UserAccessModeSetting, True, False, _
            '                                   User._Object._Userunkid, ConfigParameter._Object._CurrentDateAndTime, True, "List", "")
            'Sohail (30 Oct 2018) -- Start
            'FDRC Enhancement - Global Payment performance issue on FDRC database in 75.1.
            'blnFlag = objPaymentTran.InsertAll(mdtTable, ConfigParameter._Object._PaymentVocNoType, FinancialYear._Object._DatabaseName, Company._Object._Companyunkid, FinancialYear._Object._YearUnkid, _
            '                                  mdtPayPeriodStartDate, mdtPayPeriodEndDate, ConfigParameter._Object._UserAccessModeSetting, True, False, _
            '                                   User._Object._Userunkid, ConfigParameter._Object._CurrentDateAndTime, True, "List", "", dtActAdj)
            'blnFlag = objPaymentTran.InsertAll(mdtTable, ConfigParameter._Object._PaymentVocNoType, FinancialYear._Object._DatabaseName, Company._Object._Companyunkid, FinancialYear._Object._YearUnkid, _
            '                                  mdtPayPeriodStartDate, mdtPayPeriodEndDate, ConfigParameter._Object._UserAccessModeSetting, True, False, _
            '                                   User._Object._Userunkid, ConfigParameter._Object._CurrentDateAndTime, True, "List", "", dtActAdj, strEmployeeIDs)
            'Sohail (30 Oct 2018) -- End
            'Sohail (23 May 2017) -- End
            'Nilay (10-Feb-2016) -- End


            'Sohail (21 Aug 2015) -- End

'            If blnFlag = False And objPaymentTran._Message <> "" Then
'                eZeeMsgBox.Show(objPaymentTran._Message, enMsgBoxStyle.Information)

                'Sohail (20 Feb 2013) -- Start
                'TRA - ENHANCEMENT
'            Else
'                If ConfigParameter._Object._SetPayslipPaymentApproval = True Then
'                    Cursor.Current = Cursors.WaitCursor 'Sohail (11 Mar 2013)

                    'S.SANDEEP [28-May-2018] -- START
                    'ISSUE/ENHANCEMENT : {Audit Trails} 
                    objPaymentTran._FormName = mstrModuleName
                    objPaymentTran._LoginEmployeeUnkid = 0
                    objPaymentTran._ClientIP = getIP()
                    objPaymentTran._HostName = getHostName()
                    objPaymentTran._FromWeb = False
                    objPaymentTran._AuditUserId = User._Object._Userunkid
            objPaymentTran._Companyunkid = Company._Object._Companyunkid
                    objPaymentTran._AuditDate = ConfigParameter._Object._CurrentDateAndTime
                    'S.SANDEEP [28-May-2018] -- END

                    'Sohail (18 Feb 2014) -- Start
                    'Enhancement - AGKN
                    'objPaymentTran.SendMailToApprover(True, User._Object._Userunkid, strEmployeeIDs, CInt(cboPayPeriod.SelectedValue), False, mintBankPaid, mintCashPaid, (mintBankPaid + mintCashPaid), decBankPaid, decCashPaid, (decBankPaid + decCashPaid), cboCurrency.Text, "", GUI.fmtCurrency)

                    'S.SANDEEP [ 28 JAN 2014 ] -- START
                    'objPaymentTran.SendMailToApprover(True, User._Object._Userunkid, strEmployeeIDs, CInt(cboPayPeriod.SelectedValue), False, mintBankPaid, mintCashPaid, (mintBankPaid + mintCashPaid), decBankPaid, decCashPaid, (decBankPaid + decCashPaid), cboCurrency.Text, "", GUI.fmtCurrency, Company._Object._Companyunkid, FinancialYear._Object._YearUnkid, FinancialYear._Object._DatabaseName, mdtTable)

                    'Sohail (17 Dec 2014) -- Start
                    'Enhancement - Provide Link for Payroll approval/Authorize Payslip Payment.
                    'objPaymentTran.SendMailToApprover(True, User._Object._Userunkid, strEmployeeIDs, CInt(cboPayPeriod.SelectedValue), False, mintBankPaid, mintCashPaid, (mintBankPaid + mintCashPaid), decBankPaid, decCashPaid, (decBankPaid + decCashPaid), cboCurrency.Text, "", GUI.fmtCurrency, Company._Object._Companyunkid, FinancialYear._Object._YearUnkid, FinancialYear._Object._DatabaseName, mdtTable, enLogin_Mode.DESKTOP, 0)
                    'Sohail (24 Dec 2014) -- Start
                    'Enhancement - Provide Remark option on Payslip Payment and Global Payslip Payment.
                    'objPaymentTran.SendMailToApprover(True, User._Object._Userunkid, strEmployeeIDs, CInt(cboPayPeriod.SelectedValue), False, mintBankPaid, mintCashPaid, (mintBankPaid + mintCashPaid), decBankPaid, decCashPaid, (decBankPaid + decCashPaid), cboCurrency.Text, "", GUI.fmtCurrency, Company._Object._Companyunkid, FinancialYear._Object._YearUnkid, FinancialYear._Object._DatabaseName, mdtTable, enLogin_Mode.DESKTOP, 0, CInt(cboCurrency.SelectedValue), ConfigParameter._Object._ArutiSelfServiceURL)
                    'Sohail (16 Nov 2016) -- Start
                    'Enhancement - 64.1 - Get User Access Level Employees from new employee transfer table and employee categorization table.
                    'objPaymentTran.SendMailToApprover(True, User._Object._Userunkid, strEmployeeIDs, CInt(cboPayPeriod.SelectedValue), False, mintBankPaid, mintCashPaid, (mintBankPaid + mintCashPaid), decBankPaid, decCashPaid, (decBankPaid + decCashPaid), cboCurrency.Text, txtRemarks.Text.Trim, GUI.fmtCurrency, Company._Object._Companyunkid, FinancialYear._Object._YearUnkid, FinancialYear._Object._DatabaseName, mdtTable, enLogin_Mode.DESKTOP, 0, CInt(cboCurrency.SelectedValue), ConfigParameter._Object._ArutiSelfServiceURL)
                    objPaymentTran.SendMailToApprover(True, User._Object._Userunkid, strEmployeeIDs, CInt(cboPayPeriod.SelectedValue), False, mintBankPaid, mintCashPaid, (mintBankPaid + mintCashPaid), decBankPaid, decCashPaid, (decBankPaid + decCashPaid), cboCurrency.Text, txtRemarks.Text.Trim, GUI.fmtCurrency, Company._Object._Companyunkid, FinancialYear._Object._YearUnkid, FinancialYear._Object._DatabaseName, mdtPayPeriodStartDate, mdtPayPeriodEndDate, ConfigParameter._Object._UserAccessModeSetting, True, False, mdtTable, enLogin_Mode.DESKTOP, 0, CInt(cboCurrency.SelectedValue), ConfigParameter._Object._ArutiSelfServiceURL)
                    'Sohail (16 Nov 2016) -- End
                    'Sohail (24 Dec 2014) -- End
                    'Sohail (17 Dec 2014) -- End
                    'S.SANDEEP [ 28 JAN 2014 ] -- END
                    'Sohail (13 Dec 2017) -- Start
                    'SUMATRA Enhancement – SUMATRA – issue # 0001669: Additional feature for Exchange Server configuration setting in 70.1.
                    RemoveHandler gfrmMDI.tmrReminder.Tick, AddressOf gfrmMDI.tmrReminder_Tick
                    AddHandler gfrmMDI.tmrReminder.Tick, AddressOf gfrmMDI.tmrReminder_Tick
                    'Sohail (13 Dec 2017) -- End

                    'Sohail (18 Feb 2014) -- End
                    Cursor.Current = Cursors.Default 'Sohail (11 Mar 2013)

                'Sohail (20 Feb 2013) -- End



            'If blnFlag Then
            '    mblnCancel = False
            '    If menAction = enAction.ADD_CONTINUE Then
            '        objPaymentTran = Nothing
            '        objPaymentTran = New clsPayment_tran
            '        cboPayYear.SelectedValue = 0
            '        txtVoucher.Text = ""
            '        cboPaymentMode.SelectedValue = 0
            '        cboBankGroup.SelectedValue = 0
            '        cboBranch.SelectedValue = 0
            '        txtChequeNo.Text = ""
            '        'Sohail (03 Nov 2010) -- Start
            '        'Call CheckAllEmployee(False)
            '        objchkSelectAll.CheckState = CheckState.Unchecked
            '        'Sohail (03 Nov 2010) -- End
            '        txtTotalPaymentAmount.Text = Format(0, GUI.fmtCurrency) 'Sohail (01 Dec 2010)
            '        txtRemarks.Text = "" 'Sohail (24 Dec 2014) 
            '        cboPayYear.Focus()
            '    Else
            '        Me.Close()
            '    End If
            '    'Sohail (30 Oct 2018) -- Start
            '    'FDRC Enhancement - Global Payment performance issue on FDRC database in 75.1.
            '    'eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 12, "Payment Process completed successfully."), enMsgBoxStyle.Information)
            '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 12, "Payment Process completed successfully."), enMsgBoxStyle.Information, eZeeMsgBox.MessageBoxButton.DefaultTitle & " [in " & CInt((DateAndTime.Now - dtStart).TotalSeconds).ToString & " Seconds.]")
            '    'Sohail (30 Oct 2018) -- End
            'End If
            Dim objPW As New clsPaymentBGWorker
            objPW.mdtActAdj = dtActAdj
            objPW.mstrEmployeeIDs = strEmployeeIDs
            objPW.mintBankPaid = mintBankPaid
            objPW.mintCashPaid = mintCashPaid
            objPW.mdecBankPaid = decBankPaid
            objPW.mdecCashPaid = decCashPaid
            mintCheckedEmployee = lvEmployeeList.CheckedItems.Count

            RemoveHandler gfrmMDI.tmrReminder.Tick, AddressOf gfrmMDI.tmrReminder_Tick
            objbgwProcess.RunWorkerAsync(objPW)
            'Sohail (30 Apr 2019) -- End
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnProcess_Click", mstrModuleName)
        End Try
    End Sub
#End Region

#Region " Listview's Events "
    Private Sub lvEmployeeList_ItemChecked(ByVal sender As Object, ByVal e As System.Windows.Forms.ItemCheckedEventArgs) Handles lvEmployeeList.ItemChecked
        Dim decTotAmt As Decimal 'Sohail (11 May 2011)
        Try
            'Sohail (03 Nov 2010) -- Start
            'If lvEmployeeList.CheckedItems.Count = lvEmployeeList.Items.Count Then
            '    chkSelectAll.Checked = True
            'ElseIf lvEmployeeList.CheckedItems.Count = 0 Then
            '    chkSelectAll.Checked = False
            'End If
            decTotAmt = 0
            If lvEmployeeList.CheckedItems.Count <= 0 Then
                RemoveHandler objchkSelectAll.CheckedChanged, AddressOf chkSelectAll_CheckedChanged
                objchkSelectAll.CheckState = CheckState.Unchecked
                AddHandler objchkSelectAll.CheckedChanged, AddressOf chkSelectAll_CheckedChanged
            ElseIf lvEmployeeList.CheckedItems.Count < lvEmployeeList.Items.Count Then
                RemoveHandler objchkSelectAll.CheckedChanged, AddressOf chkSelectAll_CheckedChanged
                objchkSelectAll.CheckState = CheckState.Indeterminate
                AddHandler objchkSelectAll.CheckedChanged, AddressOf chkSelectAll_CheckedChanged
                For Each lvItem As ListViewItem In lvEmployeeList.CheckedItems
                    If lvItem.Checked = True Then
                        'Sohail (15 Dec 2010) -- Start
                        'dblTotAmt += cdec(lvItem.SubItems(colhAmount.Index).Text)
                        'decTotAmt += CDec(lvItem.SubItems(colhAmount.Index).Tag) 'Sohail (11 May 2011)
                        'Sohail (21 Feb 2017) -- Start
                        'TRA Issue - 64.1 - Global Payment Total is not matching with Payroll Summary Net Pay and JV Net Pay account.
                        'decTotAmt += CDec(lvItem.SubItems(colhAmount.Index).Text) 'Sohail (11 May 2011)
                        decTotAmt += CDec(lvItem.SubItems(colhAmount.Index).Tag)
                        'Sohail (21 Feb 2017) -- End
                        'Sohail (15 Dec 2010) -- End
                    End If
                Next
            ElseIf lvEmployeeList.CheckedItems.Count = lvEmployeeList.Items.Count Then
                RemoveHandler objchkSelectAll.CheckedChanged, AddressOf chkSelectAll_CheckedChanged
                objchkSelectAll.CheckState = CheckState.Checked
                AddHandler objchkSelectAll.CheckedChanged, AddressOf chkSelectAll_CheckedChanged
                For Each lvItem As ListViewItem In lvEmployeeList.CheckedItems
                    If lvItem.Checked = True Then
                        'Sohail (15 Dec 2010) -- Start
                        'dblTotAmt += cdec(lvItem.SubItems(colhAmount.Index).Text)
                        'decTotAmt += CDec(lvItem.SubItems(colhAmount.Index).Tag) 'Sohail (11 May 2011)
                        'Sohail (21 Feb 2017) -- Start
                        'TRA Issue - 64.1 - Global Payment Total is not matching with Payroll Summary Net Pay and JV Net Pay account.
                        'decTotAmt += CDec(lvItem.SubItems(colhAmount.Index).Text) 'Sohail (11 May 2011)
                        decTotAmt += CDec(lvItem.SubItems(colhAmount.Index).Tag)
                        'Sohail (21 Feb 2017) -- End
                        'Sohail (15 Dec 2010) -- End
                    End If
                Next
            End If
            'dblTotAmt = 0
            'For Each lvItem As ListViewItem In lvEmployeeList.CheckedItems
            '    If lvItem.Checked = True Then
            '        dblTotAmt += cdec(lvItem.SubItems(colhAmount.Index).Text)
            '    End If
            'Next
            'Sohail (03 Nov 2010) -- End
            mdecTotAmt = decTotAmt
            'Sohail (15 Dec 2010) -- Start
            'If CInt(cboPaymentMode.SelectedValue) = enPaymentMode.CASH OrElse CInt(cboPaymentMode.SelectedValue) = enPaymentMode.CASH_AND_CHEQUE Then 'Sohail (21 Apr 2020)
                'If CInt(cboPaymentMode.SelectedValue) = enPaymentMode.CASH Then
                'Sohail (15 Dec 2010) -- End
                decTotAmt = decTotAmt * txtCashPerc.Decimal / 100 'Sohail (11 May 2011)
            'End If 'Sohail (21 Apr 2020)
            txtTotalPaymentAmount.Text = Format(decTotAmt, GUI.fmtCurrency) 'Sohail (01 Dec 2010)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lvEmployeeList_ItemChecked", mstrModuleName)
        End Try
    End Sub
#End Region

#Region " CheckBox's Events "
    Private Sub chkSelectAll_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objchkSelectAll.CheckedChanged
        Dim decTotAmt As Decimal = 0 'Sohail (11 May 2011)
        Try
            Call CheckAllEmployee(objchkSelectAll.Checked)
            'Sohail (03 Nov 2010) -- Start
            For Each lvItem As ListViewItem In lvEmployeeList.CheckedItems
                If lvItem.Checked = True Then
                    'Sohail (15 Dec 2010) -- Start
                    'dblTotAmt += cdec(lvItem.SubItems(colhAmount.Index).Text)
                    'decTotAmt += CDec(lvItem.SubItems(colhAmount.Index).Tag) 'Sohail (11 May 2011)
                    'Sohail (21 Feb 2017) -- Start
                    'TRA Issue - 64.1 - Global Payment Total is not matching with Payroll Summary Net Pay and JV Net Pay account.
                    'decTotAmt += CDec(lvItem.SubItems(colhAmount.Index).Text) 'Sohail (11 May 2011)
                    decTotAmt += CDec(lvItem.SubItems(colhAmount.Index).Tag) 'Sohail (11 May 2011)
                    'Sohail (21 Feb 2017) -- End
                    'Sohail (15 Dec 2010) -- End
                End If
            Next
            mdecTotAmt = decTotAmt
            'Sohail (15 Dec 2010) -- Start
            'If CInt(cboPaymentMode.SelectedValue) = enPaymentMode.CASH OrElse CInt(cboPaymentMode.SelectedValue) = enPaymentMode.CASH_AND_CHEQUE Then 'Sohail (21 Apr 2020)
                'If CInt(cboPaymentMode.SelectedValue) = enPaymentMode.CASH Then
                'Sohail (15 Dec 2010) -- End
                decTotAmt = decTotAmt * txtCashPerc.Decimal / 100 'Sohail (11 May 2011)
            'End If 'Sohail (21 Apr 2020)
            txtTotalPaymentAmount.Text = Format(decTotAmt, GUI.fmtCurrency) 'Sohail (01 Dec 2010)
            'Sohail (03 Nov 2010) -- End
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "chkSelectAll_CheckedChanged", mstrModuleName)
        End Try
    End Sub
#End Region

#Region " Textbox's Events "
    Private Sub txtCashPerc_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtCashPerc.LostFocus
        Dim decTotAmt As Decimal = mdecTotAmt 'Sohail (11 May 2011)
        Try
            txtCashPerc.Text = Format(txtCashPerc.Decimal, "00.00")
            If lvEmployeeList.CheckedItems.Count > 0 Then
                decTotAmt = decTotAmt * txtCashPerc.Decimal / 100 'Sohail (11 May 2011)
            End If
            txtTotalPaymentAmount.Text = Format(decTotAmt, GUI.fmtCurrency) 'Sohail (01 Dec 2010)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "txtCashPerc_LostFocus", mstrModuleName)
        End Try
    End Sub
#End Region

#Region " Other Control's Events "
    Private Sub objbtnSearchEmployee_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchEmployee.Click
        Dim objfrm As New frmCommonSearch
        'Sohail (06 Jan 2012) -- Start
        'TRA - ENHANCEMENT
        'Dim objEmployee As New clsEmployee_Master
        'Dim dsList As DataSet
        'Sohail (06 Jan 2012) -- End
        Try
            'Anjan (02 Sep 2011)-Start
            'Issue : Including Language Settings.
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If
            'Anjan (02 Sep 2011)-End

            'Sohail (06 Jan 2012) -- Start
            'TRA - ENHANCEMENT
            'dsList = objEmployee.GetEmployeeList("EmployeeList")
            'Sohail (06 Jan 2012) -- End
            With cboEmployee
                'Sohail (06 Jan 2012) -- Start
                'TRA - ENHANCEMENT
                'objfrm.DataSource = dsList.Tables("EmployeeList")
                objfrm.DataSource = CType(cboEmployee.DataSource, DataTable)
                'Sohail (06 Jan 2012) -- End
                objfrm.ValueMember = .ValueMember
                objfrm.DisplayMember = .DisplayMember
                objfrm.CodeMember = "employeecode"
                If objfrm.DisplayDialog Then
                    .SelectedValue = objfrm.SelectedValue
                End If
                .Focus()
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchEmployee_Click", mstrModuleName)
        Finally
            objfrm = Nothing
            'objEmployee = Nothing 'Sohail (06 Jan 2012)
        End Try
    End Sub

    Private Sub objbtnSearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearch.Click
        Try
            If objchkSelectAll.Checked = True Then objchkSelectAll.Checked = False
            Call FillList()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearch_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnReset.Click
        Try
            If cboEmployee.Items.Count > 0 Then cboEmployee.SelectedValue = 0
            If cboPayPoint.Items.Count > 0 Then cboPayPoint.SelectedValue = 0
            If cboPayType.Items.Count > 0 Then cboPayType.SelectedValue = 0
            'Sohail (07 Dec 2013) -- Start
            'Enhancement - OMAN
            'If cboDepartment.Items.Count > 0 Then cboDepartment.SelectedValue = 0
            'If cboGrade.Items.Count > 0 Then cboGrade.SelectedValue = 0
            'If cboSections.Items.Count > 0 Then cboSections.SelectedValue = 0
            'If cboClass.Items.Count > 0 Then cboClass.SelectedValue = 0
            'If cboCostCenter.Items.Count > 0 Then cboCostCenter.SelectedValue = 0
            'If cboJob.Items.Count > 0 Then cboJob.SelectedValue = 0
            'If cboUnit.Items.Count > 0 Then cboUnit.SelectedValue = 0
            mstrAdvanceFilter = ""
            'Sohail (07 Dec 2013) -- End

            lvEmployeeList.Items.Clear()
            If objchkSelectAll.Checked = True Then objchkSelectAll.Checked = False
            Call FillList()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnReset_Click", mstrModuleName)
        End Try
    End Sub

    'Sohail (07 Dec 2013) -- Start
    'Enhancement - OMAN
    Private Sub lnkAdvanceFilter_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lnkAdvanceFilter.LinkClicked
        Try
            Dim frm As New frmAdvanceSearch
            frm.ShowDialog()
            mstrAdvanceFilter = frm._GetFilterString
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lnkAdvanceFilter_LinkClicked", mstrModuleName)
        End Try
    End Sub
    'Sohail (07 Dec 2013) -- End

    'Sohail (30 Apr 2019) -- Start
    'BBLMS Issue - Support Issue Id # 0003781 - 74.1 - Payslip global payment takes hours to process help needed urgently.
    Private Sub objbgwProcess_DoWork(ByVal sender As Object, ByVal e As System.ComponentModel.DoWorkEventArgs) Handles objbgwProcess.DoWork
        Try
            mblnProcessFailed = False
            Me.ControlBox = False
            Me.Enabled = False

            GC.Collect()
            Dim arg As clsPaymentBGWorker = CType(e.Argument, clsPaymentBGWorker)
            dtStart = DateAndTime.Now

 'S.SANDEEP [28-May-2018] -- START
            'ISSUE/ENHANCEMENT : {Audit Trails} 
            objPaymentTran._FormName = mstrModuleName
            objPaymentTran._LoginEmployeeUnkid = 0
            objPaymentTran._ClientIP = getIP()
            objPaymentTran._HostName = getHostName()
            objPaymentTran._FromWeb = False
            objPaymentTran._AuditUserId = User._Object._Userunkid
objPaymentTran._CompanyUnkid = Company._Object._Companyunkid
            objPaymentTran._AuditDate = ConfigParameter._Object._CurrentDateAndTime
            'S.SANDEEP [28-May-2018] -- END


            If objPaymentTran.InsertAll(mdtTable, ConfigParameter._Object._PaymentVocNoType, FinancialYear._Object._DatabaseName, Company._Object._Companyunkid, FinancialYear._Object._YearUnkid, _
                                                  mdtPayPeriodStartDate, mdtPayPeriodEndDate, ConfigParameter._Object._UserAccessModeSetting, True, False, _
                                                   User._Object._Userunkid, ConfigParameter._Object._CurrentDateAndTime, True, "List", "", arg.mdtActAdj, arg.mstrEmployeeIDs, objbgwProcess, ConfigParameter._Object._CurrencyFormat) = True Then
                'Sohail (26 Oct 2020) - [strFmtCurrency]

                If ConfigParameter._Object._SetPayslipPaymentApproval = True Then



                    'S.SANDEEP [28-May-2018] -- START
                    'ISSUE/ENHANCEMENT : {Audit Trails} 
                    objPaymentTran._FormName = mstrModuleName
                    objPaymentTran._LoginEmployeeUnkid = 0
                    objPaymentTran._ClientIP = getIP()
                    objPaymentTran._HostName = getHostName()
                    objPaymentTran._FromWeb = False
                    objPaymentTran._AuditUserId = User._Object._Userunkid
objPaymentTran._CompanyUnkid = Company._Object._Companyunkid
                    objPaymentTran._AuditDate = ConfigParameter._Object._CurrentDateAndTime
                    'S.SANDEEP [28-May-2018] -- END

                    objPaymentTran.SendMailToApprover(True, User._Object._Userunkid, arg.mstrEmployeeIDs, CInt(cboPayPeriod.SelectedValue), False, arg.mintBankPaid, arg.mintCashPaid, (arg.mintBankPaid + arg.mintCashPaid), arg.mdecBankPaid, arg.mdecCashPaid, (arg.mdecBankPaid + arg.mdecCashPaid), cboCurrency.Text, txtRemarks.Text.Trim, GUI.fmtCurrency, Company._Object._Companyunkid, FinancialYear._Object._YearUnkid, FinancialYear._Object._DatabaseName, mdtPayPeriodStartDate, mdtPayPeriodEndDate, ConfigParameter._Object._UserAccessModeSetting, True, False, mdtTable, enLogin_Mode.DESKTOP, 0, CInt(cboCurrency.SelectedValue), ConfigParameter._Object._ArutiSelfServiceURL)

                End If

                mblnCancel = False
                'Sohail (26 Jul 2019) -- Start
                'CAMUSAT TANZANIA - Issue # 0004018 - 76.1 - Payment process done success but employees still appear unpaid.
            Else
                mblnProcessFailed = True
                If objPaymentTran._Message <> "" Then
                    eZeeMsgBox.Show(objPaymentTran._Message, enMsgBoxStyle.Information)
                End If
                'Sohail (26 Jul 2019) -- End
            End If



        Catch ex As Exception
            mblnProcessFailed = True
            DisplayError.Show("-1", ex.Message, "objbgwProcess_DoWork", mstrModuleName)
        End Try
    End Sub

    Private Sub objbgwProcess_ProgressChanged(ByVal sender As Object, ByVal e As System.ComponentModel.ProgressChangedEventArgs) Handles objbgwProcess.ProgressChanged
        Try
            objlblProgress.Text = "[ " & e.ProgressPercentage.ToString & " / " & mintCheckedEmployee & " ]"
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbgwProcess_ProgressChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub objbgwProcess_RunWorkerCompleted(ByVal sender As Object, ByVal e As System.ComponentModel.RunWorkerCompletedEventArgs) Handles objbgwProcess.RunWorkerCompleted
        Try
            RemoveHandler gfrmMDI.tmrReminder.Tick, AddressOf gfrmMDI.tmrReminder_Tick
            AddHandler gfrmMDI.tmrReminder.Tick, AddressOf gfrmMDI.tmrReminder_Tick

            If e.Cancelled = True Then

            ElseIf e.Error IsNot Nothing Then
                eZeeMsgBox.Show(e.Error.ToString, enMsgBoxStyle.Information)
            ElseIf mblnProcessFailed = True Then

            Else

                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 12, "Payment Process completed successfully."), enMsgBoxStyle.Information, eZeeMsgBox.MessageBoxButton.DefaultTitle & " [in " & CInt((DateAndTime.Now - dtStart).TotalSeconds).ToString & " Seconds.]")

                objchkSelectAll.Checked = False
                cboPayPeriod.SelectedValue = 0
                objlblProgress.Text = ""

                If menAction = enAction.ADD_CONTINUE Then
                    objPaymentTran = Nothing
                    objPaymentTran = New clsPayment_tran
                    cboPayYear.SelectedValue = 0
                    txtVoucher.Text = ""
                    cboPaymentMode.SelectedValue = 0
                    cboBankGroup.SelectedValue = 0
                    cboBranch.SelectedValue = 0
                    txtChequeNo.Text = ""
                    objchkSelectAll.CheckState = CheckState.Unchecked
                    txtTotalPaymentAmount.Text = Format(0, GUI.fmtCurrency)
                    txtRemarks.Text = ""
                    cboPayYear.Focus()
                Else
                    Me.Close()
                End If

            End If

            Me.ControlBox = True
            Me.Enabled = True

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbgwProcess_RunWorkerCompleted", mstrModuleName)
        End Try
    End Sub
    'Sohail (30 Apr 2019) -- End

#End Region

#Region " Message List "
    '1, "Please select Pay Year. Pay Year is mandatory information."
    '2, "Please select Pay Period. Pay Period is mandatory information."
    '3, "Payment Date should be in between " & mdtPayPeriodStartDate.ToShortDateString & " And " & FinancialYear._Object._Database_End_Date.ToShortDateString & "."
    '4, "Please enter Payment Voucher. Payment Voucher is mandatory information."
    '5, "Please select Payment Mode. Payment Mode is mandatory information."
    '6, "Sorry, Percentage should be in between 1 to 100."
    '7, "Please select Bank Group. Bank Group is mandatory information."
    '8, "Please select Bank Branch. Bank Branch is mandatory information."
    '9, "Please enter Cheque No., Cheque No. is mandatory information."
    '10, "Please check atleast one employee to make Payment."
    '11, "Sorry, Total Payment Amount should be greater then Zero."
    '12, "Payment Process completed succesfully."
#End Region

#Region "Link Button's Events "
    'Hemant (17 Oct 2019) -- Start
    Private Sub lnkPayrollVarianceReport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkPayrollVarianceReport.Click
        Try
            Dim frm As New ArutiReports.frmPayrollVariance
            frm.FormBorderStyle = Windows.Forms.FormBorderStyle.FixedSingle
            frm.MaximizeBox = True
            frm.MaximizeBox = False
            frm.StartPosition = FormStartPosition.CenterParent
            frm.WindowState = FormWindowState.Maximized
            frm.ShowDialog()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lnkPayrollVarianceReport_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub lnkPayrollTotalVarianceReport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkPayrollTotalVarianceReport.Click
        Try
            Dim frm As New ArutiReports.frmPayrollTotalVariance
            frm.FormBorderStyle = Windows.Forms.FormBorderStyle.FixedSingle
            frm.MaximizeBox = True
            frm.MaximizeBox = False
            frm.StartPosition = FormStartPosition.CenterParent
            frm.WindowState = FormWindowState.Maximized
            frm.ShowDialog()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lnkPayrollTotalVarianceReport_Click", mstrModuleName)
        End Try
    End Sub
    'Hemant (17 Oct 2019) -- End
#End Region

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Private Sub OtherSettings()
        Try
            Me.SuspendLayout()
           
            Call SetLanguage()

            Me.gbPaymentInfo.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbPaymentInfo.ForeColor = GUI._eZeeContainerHeaderForeColor

            Me.gbFilterCriteria.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbFilterCriteria.ForeColor = GUI._eZeeContainerHeaderForeColor

            Me.gbEmployeeList.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbEmployeeList.ForeColor = GUI._eZeeContainerHeaderForeColor

            Me.gbAdvanceAmountInfo.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbAdvanceAmountInfo.ForeColor = GUI._eZeeContainerHeaderForeColor


            Me.eZeeHeader.GradientColor1 = GUI._HeaderBackColor1
            Me.eZeeHeader.GradientColor2 = GUI._HeaderBackColor2
            Me.eZeeHeader.BorderColor = GUI._HeaderBorderColor
            Me.eZeeHeader.HeaderTextForeColor = GUI._HeaderTitleFontColor
            Me.eZeeHeader.DescriptionForeColor = GUI._HeaderMessageFontColor


            Me.btnProcess.GradientBackColor = GUI._ButttonBackColor
            Me.btnProcess.GradientForeColor = GUI._ButttonFontColor

            Me.btnClose.GradientBackColor = GUI._ButttonBackColor
            Me.btnClose.GradientForeColor = GUI._ButttonFontColor


            Me.ResumeLayout()
        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "OtherSettings", mstrModuleName)
        End Try
    End Sub


    Private Sub SetLanguage()
        Try
            Me.Text = Language._Object.getCaption(Me.Name, Me.Text)

            Me.eZeeHeader.Title = Language._Object.getCaption(Me.eZeeHeader.Name & "_Title", Me.eZeeHeader.Title)
            Me.eZeeHeader.Message = Language._Object.getCaption(Me.eZeeHeader.Name & "_Message", Me.eZeeHeader.Message)
            Me.gbPaymentInfo.Text = Language._Object.getCaption(Me.gbPaymentInfo.Name, Me.gbPaymentInfo.Text)
            Me.lblPayPeriod.Text = Language._Object.getCaption(Me.lblPayPeriod.Name, Me.lblPayPeriod.Text)
            Me.lblPayYear.Text = Language._Object.getCaption(Me.lblPayYear.Name, Me.lblPayYear.Text)
            Me.btnProcess.Text = Language._Object.getCaption(Me.btnProcess.Name, Me.btnProcess.Text)
            Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
            Me.gbFilterCriteria.Text = Language._Object.getCaption(Me.gbFilterCriteria.Name, Me.gbFilterCriteria.Text)
            Me.lblEmployee.Text = Language._Object.getCaption(Me.lblEmployee.Name, Me.lblEmployee.Text)
            Me.gbEmployeeList.Text = Language._Object.getCaption(Me.gbEmployeeList.Name, Me.gbEmployeeList.Text)
            Me.colhCheck.Text = Language._Object.getCaption(CStr(Me.colhCheck.Tag), Me.colhCheck.Text)
            Me.colhCode.Text = Language._Object.getCaption(CStr(Me.colhCode.Tag), Me.colhCode.Text)
            Me.colhName.Text = Language._Object.getCaption(CStr(Me.colhName.Tag), Me.colhName.Text)
            Me.lblPaymentDate.Text = Language._Object.getCaption(Me.lblPaymentDate.Name, Me.lblPaymentDate.Text)
            Me.lblPaymentMode.Text = Language._Object.getCaption(Me.lblPaymentMode.Name, Me.lblPaymentMode.Text)
            Me.lblCheque.Text = Language._Object.getCaption(Me.lblCheque.Name, Me.lblCheque.Text)
            Me.lblBranch.Text = Language._Object.getCaption(Me.lblBranch.Name, Me.lblBranch.Text)
            Me.lblBankGroup.Text = Language._Object.getCaption(Me.lblBankGroup.Name, Me.lblBankGroup.Text)
            Me.colhAmount.Text = Language._Object.getCaption(CStr(Me.colhAmount.Tag), Me.colhAmount.Text)
            Me.gbAdvanceAmountInfo.Text = Language._Object.getCaption(Me.gbAdvanceAmountInfo.Name, Me.gbAdvanceAmountInfo.Text)
            Me.lblTotalPaymentAmount.Text = Language._Object.getCaption(Me.lblTotalPaymentAmount.Name, Me.lblTotalPaymentAmount.Text)
            Me.lblAgainstVoucher.Text = Language._Object.getCaption(Me.lblAgainstVoucher.Name, Me.lblAgainstVoucher.Text)
            Me.lblCashDescr.Text = Language._Object.getCaption(Me.lblCashDescr.Name, Me.lblCashDescr.Text)
            Me.lblCashPerc.Text = Language._Object.getCaption(Me.lblCashPerc.Name, Me.lblCashPerc.Text)
            Me.colhVoucherNo.Text = Language._Object.getCaption(CStr(Me.colhVoucherNo.Tag), Me.colhVoucherNo.Text)
            Me.colhTnAleaveUnkID.Text = Language._Object.getCaption(CStr(Me.colhTnAleaveUnkID.Tag), Me.colhTnAleaveUnkID.Text)
            Me.lblACHolder.Text = Language._Object.getCaption(Me.lblACHolder.Name, Me.lblACHolder.Text)
            Me.lblMessage.Text = Language._Object.getCaption(Me.lblMessage.Name, Me.lblMessage.Text)
            Me.lblCurrency.Text = Language._Object.getCaption(Me.lblCurrency.Name, Me.lblCurrency.Text)
            Me.colhBaseAmount.Text = Language._Object.getCaption(CStr(Me.colhBaseAmount.Tag), Me.colhBaseAmount.Text)
            Me.lblAccountNo.Text = Language._Object.getCaption(Me.lblAccountNo.Name, Me.lblAccountNo.Text)
            Me.lnkAdvanceFilter.Text = Language._Object.getCaption(Me.lnkAdvanceFilter.Name, Me.lnkAdvanceFilter.Text)
            Me.lblEmpBank.Text = Language._Object.getCaption(Me.lblEmpBank.Name, Me.lblEmpBank.Text)
            Me.lblEmpBranch.Text = Language._Object.getCaption(Me.lblEmpBranch.Name, Me.lblEmpBranch.Text)
            Me.lblCutOffAmount.Text = Language._Object.getCaption(Me.lblCutOffAmount.Name, Me.lblCutOffAmount.Text)
            Me.lblPayPoint.Text = Language._Object.getCaption(Me.lblPayPoint.Name, Me.lblPayPoint.Text)
            Me.lblPayType.Text = Language._Object.getCaption(Me.lblPayType.Name, Me.lblPayType.Text)
            Me.lblRemarks.Text = Language._Object.getCaption(Me.lblRemarks.Name, Me.lblRemarks.Text)
            Me.lblPaymentDatePeriod.Text = Language._Object.getCaption(Me.lblPaymentDatePeriod.Name, Me.lblPaymentDatePeriod.Text)
            Me.lnkPayrollVarianceReport.Text = Language._Object.getCaption(Me.lnkPayrollVarianceReport.Name, Me.lnkPayrollVarianceReport.Text)
            Me.lnkPayrollTotalVarianceReport.Text = Language._Object.getCaption(Me.lnkPayrollTotalVarianceReport.Name, Me.lnkPayrollTotalVarianceReport.Text)

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
        End Try
    End Sub


    Private Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 2, "Please select Pay Period. Pay Period is mandatory information.")
            Language.setMessage(mstrModuleName, 3, "Payment Date should be between")
            Language.setMessage(mstrModuleName, 4, "Please enter Payment Voucher. Payment Voucher is mandatory information.")
            Language.setMessage(mstrModuleName, 5, "Please select Payment Mode. Payment Mode is mandatory information.")
            Language.setMessage(mstrModuleName, 6, "Sorry, Percentage should be between 1 and 100.")
            Language.setMessage(mstrModuleName, 7, "Please select Bank Group. Bank Group is mandatory information.")
            Language.setMessage(mstrModuleName, 8, "Please select Bank Branch. Bank Branch is mandatory information.")
            Language.setMessage(mstrModuleName, 9, "Please enter Cheque No., Cheque No. is mandatory information.")
            Language.setMessage(mstrModuleName, 10, "Please check atleast one employee to make Payment.")
            Language.setMessage(mstrModuleName, 11, " And")
            Language.setMessage(mstrModuleName, 12, "Payment Process completed successfully.")
            Language.setMessage(mstrModuleName, 14, "Sorry! You can not do Payment now. Reason : Some of the employees have been Over Deducted this month.")
            Language.setMessage(mstrModuleName, 15, "Sorry! Some of the selected employees including")
            Language.setMessage(mstrModuleName, 16, "Please select Bank Account. Bank Account is mandatory information.")
            Language.setMessage(mstrModuleName, 17, "Sending Email(s) process is in progress from other module. Please wait.")
            Language.setMessage(mstrModuleName, 18, "are having Total Salary Distribution percentage Not Equal to 100 in Employee Bank.")
            Language.setMessage(mstrModuleName, 19, "Please select Payment Date Period. Payment Date Period is mandatory information.")
            Language.setMessage(mstrModuleName, 20, "Sorry. Some of Operation(s) either Interest Rate/EMI Tenure/Topup Amount are still pending for Approval process in current pay period. In order to process Payment, please either Approve or Reject pending operations for selected pay period.")
	    Language.setMessage(mstrModuleName, 21, "Salary payments for the period of")
	    Language.setMessage(mstrModuleName, 22, "Sorry, Actual Salary Payment will be exceeding to the Activity Current Balance.")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class