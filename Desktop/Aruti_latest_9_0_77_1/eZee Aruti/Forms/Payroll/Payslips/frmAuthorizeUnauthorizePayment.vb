﻿Option Strict On
Imports eZeeCommonLib
Imports Aruti.Data
Imports System.Threading

Public Class frmAuthorizeUnauthorizePayment

#Region " Private Variables "
    Private ReadOnly mstrModuleName As String = "frmAuthorizeUnauthorizePayment"

    Private mblnCancel As Boolean = True
    Private menAction As enAction = enAction.ADD_ONE

    Private objPaymentTran As clsPayment_tran

    Private mdtPayPeriodStartDate As Date
    Private mdtPayPeriodEndDate As Date
    Private mdtTable As DataTable = Nothing
    Private mdecTotAmt As Decimal = 0 'Sohail (11 May 2011)

    Private mstrBaseCurrSign As String
    Private mintBaseCurrId As Integer
    Private mintPaidCurrId As Integer
    Private mdecBaseExRate As Decimal
    Private mdecPaidExRate As Decimal
    Private mdecBaseCurrBalanceAmt As Decimal
    Private mdecPaidCurrBalanceAmt As Decimal
    Private mdecPaidCurrBalFormatedAmt As Decimal

    Private mblnFromAuthorize As Boolean
    Private mdicCurrency As New Dictionary(Of Integer, String)
    Private mstrPaymentTranIDs As String = ""

    'Sohail (09 Mar 2013) -- Start
    'TRA - ENHANCEMENT
    Private mintBankPayment As Integer = 0
    Private mintCashPayment As Integer = 0
    Private mdecBankPayment As Decimal = 0
    Private mdecCashPayment As Decimal = 0
    'Private dicNotification As New Dictionary(Of String, String) 'Sohail (10 Mar 2014)
    Private trd As Thread
    'Sohail (09 Mar 2013) -- End
    'Sohail (26 May 2017) -- Start
    'Issue - 67.1 - Authorize payment total not matching with global payment total.
    Private dvEmployee As DataView
    Private mintTotalEmployee As Integer = 0
    Private mintCount As Integer = 0
    Private mstrSearchEmpText As String = ""
    'Sohail (26 May 2017) -- End

    Private mstrAdvanceFilter As String = "" 'Sohail (18 Feb 2014)
#End Region

#Region " Display Dialogue "
    Public Function DisplayDialog(ByVal Action As enAction, ByVal blnFromAuthorize As Boolean) As Boolean
        Try
            menAction = Action
            mblnFromAuthorize = blnFromAuthorize

            Me.ShowDialog()

            Return Not mblnCancel
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "DisplayDialog", mstrModuleName)
        End Try
    End Function
#End Region

#Region " Private Methods & Functions "
    Private Sub SetColor()
        Try
            cboPayPeriod.BackColor = GUI.ColorComp
            cboCurrency.BackColor = GUI.ColorComp
            txtRemarks.BackColor = GUI.ColorOptional 'Sohail (27 Feb 2013)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetColor", mstrModuleName)
        End Try
    End Sub

    Private Sub FillCombo()
        Dim objExRate As New clsExchangeRate
        Dim objPeriod As New clscommom_period_Tran
        Dim dsCombo As DataSet

        Try

            'Sohail (21 Jan 2014) -- Start
            'Enhancement - Oman
            'dsCombo = objPeriod.getListForCombo(enModuleReference.Payroll, FinancialYear._Object._YearUnkid, "PayPeriod", True, enStatusType.Open, True)
            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'dsCombo = objPeriod.getListForCombo(enModuleReference.Payroll, FinancialYear._Object._YearUnkid, "PayPeriod", True, enStatusType.Open, False)
            dsCombo = objPeriod.getListForCombo(enModuleReference.Payroll, FinancialYear._Object._YearUnkid, FinancialYear._Object._DatabaseName, FinancialYear._Object._Database_Start_Date, "PayPeriod", True, enStatusType.Open, False)
            'Sohail (21 Aug 2015) -- End
            'Sohail (21 Jan 2014) -- End
            With cboPayPeriod
                .ValueMember = "periodunkid"
                .DisplayMember = "Name"
                .DataSource = dsCombo.Tables("PayPeriod")
                .SelectedValue = 0
            End With

            'Sohail (16 Oct 2019) -- Start
            'NMB Enhancement # : if user does not have privilege to see amount/salary, system should disable checkbox in list, he can either tick or untick all on Approval / Authorize / Payslip / Global Void Payment Screen for Automatic Integration between Payroll and Finance modules,Online Approval workflow for Payroll runs.
            Dim intBaseCountryId As Integer = 0
            'Sohail (16 Oct 2019) -- End
            dsCombo = objExRate.getComboList("ExRate", False)
            If dsCombo.Tables(0).Rows.Count > 0 Then
                Dim dtTable As DataTable = New DataView(dsCombo.Tables("ExRate"), "isbasecurrency = 1 ", "", DataViewRowState.CurrentRows).ToTable
                If dtTable.Rows.Count > 0 Then
                    mstrBaseCurrSign = dtTable.Rows(0).Item("currency_sign").ToString
                    mintBaseCurrId = CInt(dtTable.Rows(0).Item("exchangerateunkid"))
                    'Sohail (16 Oct 2019) -- Start
                    'NMB Enhancement # : if user does not have privilege to see amount/salary, system should disable checkbox in list, he can either tick or untick all on Approval / Authorize / Payslip / Global Void Payment Screen for Automatic Integration between Payroll and Finance modules,Online Approval workflow for Payroll runs.
                    intBaseCountryId = CInt(dtTable.Rows(0).Item("countryunkid"))
                    'Sohail (16 Oct 2019) -- End
                End If

                With cboCurrency
                    'Sohail (11 Sep 2012) -- Start
                    'TRA - ENHANCEMENT
                    '.ValueMember = "exchangerateunkid"
                    .ValueMember = "countryunkid"
                    'Sohail (11 Sep 2012) -- End
                    .DisplayMember = "currency_sign"
                    .DataSource = dsCombo.Tables("ExRate")
                    'Sohail (16 Oct 2019) -- Start
                    'NMB Enhancement # : if user does not have privilege to see amount/salary, system should disable checkbox in list, he can either tick or untick all on Approval / Authorize / Payslip / Global Void Payment Screen for Automatic Integration between Payroll and Finance modules,Online Approval workflow for Payroll runs.
                    '.SelectedIndex = 0
                    If intBaseCountryId > 0 Then
                        .SelectedValue = intBaseCountryId
                    Else
                    .SelectedIndex = 0
                    End If
                    'Sohail (16 Oct 2019) -- End
                End With

                For Each dsRow As DataRow In dsCombo.Tables(0).Rows
                    If mdicCurrency.ContainsKey(CInt(dsRow.Item("exchangerateunkid"))) = False Then
                        mdicCurrency.Add(CInt(dsRow.Item("exchangerateunkid")), dsRow.Item("currency_sign").ToString)
                    End If
                Next
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        Finally
            objPeriod = Nothing
            objExRate = Nothing
        End Try
    End Sub

    Private Sub FillList()
        Dim dsList As DataSet
        Dim strFilter As String = ""
        Dim decTotAmt As Decimal = 0
        'Pinkal (18-Jul-2012) -- Start
        'Enhancement : TRA Changes
        'Sohail (09 Mar 2013) -- Start
        'TRA - ENHANCEMENT
        'Dim mintBankPayment As Integer = 0
        'Dim mintCashPayment As Integer = 0
        mintBankPayment = 0
        mintCashPayment = 0
        mdecBankPayment = 0
        mdecCashPayment = 0
        'Sohail (09 Mar 2013) -- End
        'Pinkal (18-Jul-2012) -- End

        Try
            mstrPaymentTranIDs = ""
            'Sohail (26 May 2017) -- Start
            'Issue - 67.1 - Authorize payment total not matching with global payment total.
            'lvEmployeeList.Items.Clear()
            objlblEmpCount.Text = "( 0 / 0 )"
            mintCount = 0
            dgEmployee.DataSource = Nothing
            RemoveHandler txtSearchEmp.TextChanged, AddressOf txtSearchEmp_TextChanged
            Call SetDefaultSearchEmpText()
            AddHandler txtSearchEmp.TextChanged, AddressOf txtSearchEmp_TextChanged
            'Sohail (26 May 2017) -- End
            txtTotalAmount.Text = Format(decTotAmt, GUI.fmtCurrency)


            If CInt(cboPayPeriod.SelectedValue) <= 0 Then
                'Pinkal (18-Jul-2012) -- Start
                'Enhancement : TRA Changes
                objBankpaidVal.Text = mintBankPayment.ToString()
                objCashpaidVal.Text = mintCashPayment.ToString()
                objTotalpaidVal.Text = (mintBankPayment + mintCashPayment).ToString()
                'Pinkal (18-Jul-2012) -- End
                Exit Try
            End If

            Cursor.Current = Cursors.WaitCursor

            'Sohail (26 May 2017) -- Start
            'Issue - 67.1 - Authorize payment total not matching with global payment total.
            'Dim lvItem As ListViewItem
            'Dim lvArray As New List(Of ListViewItem)
            'lvEmployeeList.BeginUpdate()
            'Sohail (26 May 2017) -- End

            'Nilay (18-Nov-2015) -- Start
            'strFilter = "prpayment_tran.periodunkid = " & CInt(cboPayPeriod.SelectedValue) & " " & _
            '            "AND countryunkid = " & CInt(cboCurrency.SelectedValue) & " " 'Sohail (11 Sep 2012) - [paidcurrencyid = countryunkid]

            strFilter = "prpayment_tran.periodunkid = " & CInt(cboPayPeriod.SelectedValue) & " " & _
                        "AND prpayment_tran.countryunkid = " & CInt(cboCurrency.SelectedValue) & " " 'Sohail (11 Sep 2012) - [paidcurrencyid = countryunkid]
            'Nilay (18-Nov-2015) -- End

            If mblnFromAuthorize = True Then
                strFilter &= "AND ISNULL(isauthorized, 0) = 0 "
                If ConfigParameter._Object._SetPayslipPaymentApproval = True Then
                    strFilter &= "AND ISNULL(prpayment_tran.isapproved, 0) = 1 " 'Sohail (13 Feb 2013)
                End If

            Else
                strFilter &= "AND ISNULL(isauthorized, 0) = 1 "
            End If

            'Sohail (18 Feb 2014) -- Start
            'Enhancement - AGKN
            If cboPmtVoucher.SelectedIndex > 0 Then
                strFilter &= " AND (prpayment_tran.voucherno = '" & cboPmtVoucher.Text & "' OR prglobalvoc_master.globalvocno = '" & cboPmtVoucher.Text & "') "
            End If
            If mstrAdvanceFilter.Trim <> "" Then
                strFilter &= " AND " & mstrAdvanceFilter
            End If
            'Sohail (18 Feb 2014) -- End

            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'dsList = objPaymentTran.GetList("List", clsPayment_tran.enPaymentRefId.PAYSLIP, 0, clsPayment_tran.enPayTypeId.PAYMENT, strFilter)
            dsList = objPaymentTran.GetList(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, mdtPayPeriodStartDate, mdtPayPeriodEndDate, ConfigParameter._Object._UserAccessModeSetting, True, ConfigParameter._Object._IsIncludeInactiveEmp, "List", clsPayment_tran.enPaymentRefId.PAYSLIP, 0, clsPayment_tran.enPayTypeId.PAYMENT, True, strFilter)
            'Sohail (21 Aug 2015) -- End
            'Sohail (26 May 2017) -- Start
            'Issue - 67.1 - Authorize payment total not matching with global payment total.
            mintTotalEmployee = dsList.Tables("List").Rows.Count
            objlblEmpCount.Text = "( 0 / " & mintTotalEmployee.ToString & " )"

            If dsList.Tables(0).Columns.Contains("IsChecked") = False Then
                Dim dtCol As DataColumn
                dtCol = New DataColumn("IsChecked", System.Type.GetType("System.Boolean"))
                dtCol.DefaultValue = False
                dtCol.AllowDBNull = False
                dsList.Tables(0).Columns.Add(dtCol)
            End If
            'Sohail (26 May 2017) -- End

            Dim intCount As Integer = dsList.Tables("List").Rows.Count
            'Sohail (26 May 2017) -- Start
            'Issue - 67.1 - Authorize payment total not matching with global payment total.
            'Dim dtRow As DataRow
            'For i As Integer = 0 To intCount - 1
            '    dtRow = dsList.Tables("List").Rows(i)
            '    lvItem = New ListViewItem

            '    With lvItem
            '        'Sohail (10 Mar 2014) -- Start
            '        'Enhancement - Check list box on Payment Approval and Authorize Payment Screen.
            '        '.Text = dtRow.Item("employeecode").ToString
            '        '.Tag = dtRow.Item("paymenttranunkid").ToString
            '        .Text = ""
            '        .Tag = dtRow.Item("paymenttranunkid").ToString

            '        .SubItems.Add(dtRow.Item("employeecode").ToString)
            '        .SubItems(colhCode.Index).Tag = CInt(dtRow.Item("paymentmode")) 'Payment Mode ID
            '        'Sohail (10 Mar 2014) -- End

            '        .SubItems.Add(dtRow.Item("EmpName").ToString)
            '        .SubItems(colhName.Index).Tag = CInt(dtRow.Item("employeeunkid")) 'Sohail (10 Mar 2014)

            '        .SubItems.Add(Format(CDec(dtRow.Item("expaidamt")), GUI.fmtCurrency))
            '        .SubItems(colhAmount.Index).Tag = CDec(dtRow.Item("expaidamt"))
            '        decTotAmt += CDec(dtRow.Item("expaidamt"))

            '        If mdicCurrency.ContainsKey(CInt(dtRow.Item("paidcurrencyid"))) = True Then
            '            .SubItems.Add(mdicCurrency.Item(CInt(dtRow.Item("paidcurrencyid"))))
            '        Else
            '            .SubItems.Add(mstrBaseCurrSign)
            '        End If

            '    End With

            '    'Sohail (10 Mar 2014) -- Start
            '    'Enhancement - Check list box on Payment Approval and Authorize Payment Screen.
            '    'If mstrPaymentTranIDs = "" Then
            '    '    mstrPaymentTranIDs = dtRow.Item("paymenttranunkid").ToString
            '    'Else
            '    '    mstrPaymentTranIDs &= ", " & dtRow.Item("paymenttranunkid").ToString
            '    'End If
            '    'Sohail (10 Mar 2014) -- End

            '    'Pinkal (18-Jul-2012) -- Start
            '    'Enhancement : TRA Changes
            '    If CInt(dtRow.Item("paymentmode")) = enPaymentMode.CASH Or CInt(dtRow.Item("paymentmode")) = enPaymentMode.CASH_AND_CHEQUE Then
            '        mintCashPayment += 1
            '        'mdecCashPayment += CDec(dtRow.Item("expaidamt")) 'Sohail (09 Mar 2013)
            '    ElseIf CInt(dtRow.Item("paymentmode")) = enPaymentMode.CHEQUE Or CInt(dtRow.Item("paymentmode")) = enPaymentMode.TRANSFER Then
            '        mintBankPayment += 1
            '        'mdecBankPayment += CDec(dtRow.Item("expaidamt")) 'Sohail (09 Mar 2013)
            '    End If

            '    'Pinkal (18-Jul-2012) -- End


            '    lvArray.Add(lvItem)
            'Next

            'RemoveHandler lvEmployeeList.ItemChecked, AddressOf lvEmployeeList_ItemChecked 'Sohail (10 Mar 2014)
            'lvEmployeeList.Items.AddRange(lvArray.ToArray)
            'AddHandler lvEmployeeList.ItemChecked, AddressOf lvEmployeeList_ItemChecked 'Sohail (10 Mar 2014)
            'If lvEmployeeList.Items.Count > 19 Then
            '    colhName.Width = 120 - 18
            'Else
            '    colhName.Width = 120
            'End If
            ''txtTotalAmount.Text = Format(decTotAmt, GUI.fmtCurrency) 'Sohail (10 Mar 2014)
            'lvEmployeeList.EndUpdate()
            dvEmployee = dsList.Tables("List").DefaultView
            dgEmployee.AutoGenerateColumns = False
            objdgcolhCheck.DataPropertyName = "IsChecked"
            objdgcolhEmployeeunkid.DataPropertyName = "employeeunkid"
            dgColhEmpCode.DataPropertyName = "employeecode"
            dgColhEmployee.DataPropertyName = "EmpName"
            dgcolhAmount.DataPropertyName = "expaidamt"
            dgcolhAmount.DefaultCellStyle.Format = GUI.fmtCurrency
            objdgcolhAmount.DataPropertyName = "expaidamt"
            dgcolhCurrency.DataPropertyName = "paidcurrencysign"
            objdgcolhPaymenttranunkid.DataPropertyName = "paymenttranunkid"
            'Sohail (16 Oct 2019) -- Start
            'NMB Enhancement # : if user does not have privilege to see amount/salary, system should disable checkbox in list, he can either tick or untick all on Approval / Authorize / Payslip / Global Void Payment Screen for Automatic Integration between Payroll and Finance modules,Online Approval workflow for Payroll runs.
            objdgcolhCheck.ReadOnly = Not User._Object.Privilege._AllowToViewPaidAmount
            dgcolhAmount.Visible = User._Object.Privilege._AllowToViewPaidAmount
            txtSearchEmp.Enabled = User._Object.Privilege._AllowToViewPaidAmount
            'Sohail (16 Oct 2019) -- End

            dgEmployee.DataSource = dvEmployee
            dvEmployee.Sort = "IsChecked DESC, EmpName "

            mintCashPayment = (From p In dsList.Tables("List") Where (CInt(p.Item("paymentmode")) = enPaymentMode.CASH OrElse CInt(p.Item("paymentmode")) = enPaymentMode.CASH_AND_CHEQUE) Select (p)).Count
            mintBankPayment = (From p In dsList.Tables("List") Where (CInt(p.Item("paymentmode")) = enPaymentMode.CHEQUE OrElse CInt(p.Item("paymentmode")) = enPaymentMode.TRANSFER) Select (p)).Count
            'Sohail (26 May 2017) -- End

            'Pinkal (18-Jul-2012) -- Start
            'Enhancement : TRA Changes
            objBankpaidVal.Text = mintBankPayment.ToString()
            objCashpaidVal.Text = mintCashPayment.ToString()
            objTotalpaidVal.Text = (mintBankPayment + mintCashPayment).ToString()
            'Pinkal (18-Jul-2012) -- End


            Cursor.Current = Cursors.Default
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillList", mstrModuleName)
        Finally
            Cursor.Current = Cursors.Default
        End Try
    End Sub

    'Sohail (10 Mar 2014) -- Start
    'Enhancement - Check list box on Payment Approval and Authorize Payment Screen.
    Private Sub CheckAllEmployee(ByVal blnCheckAll As Boolean)
        Try
            'Sohail (26 May 2017) -- Start
            'Issue - 67.1 - Authorize payment total not matching with global payment total.
            'For Each lvItem As ListViewItem In lvEmployeeList.Items
            '    RemoveHandler lvEmployeeList.ItemChecked, AddressOf lvEmployeeList_ItemChecked
            '    lvItem.Checked = blnCheckAll
            '    AddHandler lvEmployeeList.ItemChecked, AddressOf lvEmployeeList_ItemChecked
            'Next
            Dim decTotAmt As Decimal = 0
            If dvEmployee IsNot Nothing Then
                For Each dr As DataRowView In dvEmployee
                    dr.Item("IsChecked") = blnCheckAll
                    dr.EndEdit()
            Next
                dvEmployee.ToTable.AcceptChanges()

                Dim drRow As DataRow() = dvEmployee.Table.Select("IsChecked = 1")
                mintCount = drRow.Length
                objlblEmpCount.Text = "( " & mintCount.ToString & " / " & mintTotalEmployee.ToString & " )"

                mdecTotAmt = (From p In dvEmployee.Table Where (CBool(p.Item("IsChecked")) = True) Select CDec(p.Item("expaidamt"))).Sum()
            End If
            txtTotalAmount.Text = Format(mdecTotAmt, GUI.fmtCurrency)
            'Sohail (26 May 2017) -- End
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "CheckAllEmployee", mstrModuleName)
        End Try
    End Sub
    'Sohail (10 Mar 2014) -- End

    Private Function IsValid() As Boolean
        Try
            If CInt(cboPayPeriod.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Please select Pay Period. Pay Period is mandatory information."), enMsgBoxStyle.Information)
                Return False
            ElseIf CInt(cboCurrency.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Please select Currency. Currency is mandatory information."), enMsgBoxStyle.Information)
                Return False
                'Sohail (10 Mar 2014) -- Start
                'Enhancement - Check list box on Payment Approval and Authorize Payment Screen.
                'ElseIf lvEmployeeList.Items.Count <= 0 Then
                'eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "Sorry! There is no payment available to Authorize / Void Authorized."), enMsgBoxStyle.Information)
                'Sohail (26 May 2017) -- Start
                'Issue - 67.1 - Authorize payment total not matching with global payment total.
                'ElseIf lvEmployeeList.CheckedItems.Count <= 0 Then
            ElseIf dvEmployee.Table.Select("IsChecked = 1 ").Length <= 0 Then
                'Sohail (26 May 2017) -- End
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "Please select atleast one employee to Authorize / Void Authorized."), enMsgBoxStyle.Information)
                'Sohail (10 Mar 2014) -- End
                Return False
            End If

            'Sohail (10 Mar 2014) -- Start
            'Enhancement - Send Email Notification to lower levels when payment disapproved.
            If gobjEmailList.Count > 0 AndAlso ConfigParameter._Object._Notify_Payroll_Users.Trim.Length > 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 15, "Sending Email(s) process is in progress from other module. Please wait."), enMsgBoxStyle.Information)
                Return False
            End If
            'Sohail (10 Mar 2014) -- End

            Return True

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "IsValid", mstrModuleName)
        End Try
    End Function

    'Sohail (26 May 2017) -- Start
    'Issue - 67.1 - Authorize payment total not matching with global payment total.
    Private Sub SetCheckBoxValue()
        Try

            RemoveHandler objchkSelectAll.CheckedChanged, AddressOf chkSelectAll_CheckedChanged

            Dim blnIsChecked As Boolean = Convert.ToBoolean(dgEmployee.CurrentRow.Cells(objdgcolhCheck.Index).Value)

            If blnIsChecked = True Then
                mintCount += 1
            Else
                mintCount -= 1
            End If

            objlblEmpCount.Text = "( " & mintCount.ToString & " / " & mintTotalEmployee.ToString & " )"

            If mintCount <= 0 Then
                objchkSelectAll.CheckState = CheckState.Unchecked
            ElseIf mintCount < dgEmployee.Rows.Count Then
                objchkSelectAll.CheckState = CheckState.Indeterminate
            ElseIf mintCount = dgEmployee.Rows.Count Then
                objchkSelectAll.CheckState = CheckState.Checked
            End If

            mdecTotAmt = (From p In dvEmployee.Table Where (CBool(p.Item("IsChecked")) = True) Select CDec(p.Item("expaidamt"))).Sum()

            txtTotalAmount.Text = Format(mdecTotAmt, GUI.fmtCurrency)

            AddHandler objchkSelectAll.CheckedChanged, AddressOf chkSelectAll_CheckedChanged
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetCheckBoxValue", mstrModuleName)
        End Try

    End Sub

    Private Sub SetDefaultSearchEmpText()
        Try
            mstrSearchEmpText = lblSearchEmp.Text
            With txtSearchEmp
                .ForeColor = Color.Gray
                .Text = mstrSearchEmpText
                .Font = New Font(Me.Font.FontFamily, Me.Font.Size, FontStyle.Italic)
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "", mstrModuleName)
        End Try
    End Sub
    'Sohail (26 May 2017) -- End

    'Sohail (09 Mar 2013) -- Start
    'TRA - ENHANCEMENT
    Private Function Set_Notification(ByVal strUserName As String _
                                      , ByVal strAuthorizeUserName As String _
                                      , ByVal strPeriodName As String _
                                      , ByVal intBankPaid As Integer _
                                      , ByVal intCashPaid As Integer _
                                      , ByVal intTotalPaid As Integer _
                                      , ByVal decBankAmount As Decimal _
                                      , ByVal decCashAmount As Decimal _
                                      , ByVal decTotalAmount As Decimal _
                                      , ByVal strCurrencySign As String _
                                      , ByVal strRemarks As String _
                                      ) As String
        Dim StrMessage As New System.Text.StringBuilder
        'Dim objEmployee As New clsEmployee_Master
        Dim blnFlag As Boolean = False
        Try
            'objEmployee._Employeeunkid = CInt(cboEmployee.SelectedValue)
            StrMessage.Append("<HTML><BODY>")
            StrMessage.Append(vbCrLf)

            If ConfigParameter._Object._Notify_Payroll_Users.Trim.Length > 0 Then
                StrMessage.Append("<p><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#333399; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>")

                'Gajanan [27-Mar-2019] -- Start
                'Enhancement - Change Email Language
                'StrMessage.Append("Dear <b>" & strUserName & "</b>,</span></p>")
                StrMessage.Append(Language.getMessage(mstrModuleName, 21, "Dear") & " " & "<b>" & getTitleCase(strUserName) & "</b></span></p>")
                'Gajanan [27-Mar-2019] -- End



                StrMessage.Append(vbCrLf)
                StrMessage.Append("<p><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#333399; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>")
                'Sohail (10 Mar 2014) -- Start
                'Enhancement - Send Email Notification to lower levels when payment disapproved.
                'StrMessage.Append("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; This is to inform you that Payslip Payment has been authorized for the period of <b>" & strPeriodName & " </b> for <b>" & (intCashPaid + intBankPaid).ToString & "</b> employees. This Payment has been authorized by <b>" & strAuthorizeUserName & "</b> from Machine : <b>" & getHostName.ToString & "</b> and IPAddress : <b>" & getIP.ToString & "</b></span></p>")


                'Gajanan [27-Mar-2019] -- Start
                'Enhancement - Change Email Language

                If mblnFromAuthorize = True Then
                    'StrMessage.Append("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; This is to inform you that Payslip Payment has been authorized for the period of <b>" & strPeriodName & " </b> for <b>" & (intCashPaid + intBankPaid).ToString & "</b> employees. This Payment has been authorized by <b>" & strAuthorizeUserName & "</b> from Machine : <b>" & getHostName.ToString & "</b> and IPAddress : <b>" & getIP.ToString & "</b></span></p>")
                    StrMessage.Append(Language.getMessage(mstrModuleName, 22, "This is to inform you that Payslip Payment has been authorized for the period of") & " <b>" & strPeriodName & " </b> " & Language.getMessage(mstrModuleName, 23, "for") & " <b>" & (intCashPaid + intBankPaid).ToString & "</b> " & Language.getMessage(mstrModuleName, 24, "employees. This Payment has been authorized by") & " <b>" & getTitleCase(strAuthorizeUserName) & "</b> " & Language.getMessage(mstrModuleName, 25, "from Machine") & " <b>" & getHostName.ToString & "</b> " & Language.getMessage(mstrModuleName, 27, "and IPAddress") & " <b>" & getIP.ToString & "</b></span></p>")
                Else
                    'StrMessage.Append("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; This is to inform you that Payslip Payment Authorization has been voided for the period of <b>" & strPeriodName & " </b> for <b>" & (intCashPaid + intBankPaid).ToString & "</b> employees. This Payment Authorization has been voided by <b>" & strAuthorizeUserName & "</b> from Machine : <b>" & getHostName.ToString & "</b> and IPAddress : <b>" & getIP.ToString & "</b></span></p>")
                    StrMessage.Append(" " & Language.getMessage(mstrModuleName, 28, "This is to inform you that Payslip Payment Authorization has been voided for the period of") & " <b>" & strPeriodName & " </b>" & Language.getMessage(mstrModuleName, 23, "for") & (intCashPaid + intBankPaid).ToString & "</b> " & Language.getMessage(mstrModuleName, 29, "employees. This Payment Authorization has been voided by") & " <b>" & getTitleCase(strAuthorizeUserName) & "</b>  " & Language.getMessage(mstrModuleName, 25, "from Machine") & " <b>" & getHostName.ToString & "</b> " & Language.getMessage(mstrModuleName, 27, "and IPAddress") & " <b>" & getIP.ToString & "</b></span></p>")
                End If
                'Gajanan [27-Mar-2019] -- End
                'Sohail (10 Mar 2014) -- End
                StrMessage.Append(vbCrLf)

                'Sohail (10 Mar 2014) -- Start
                'Enhancement - Send Email Notification to lower levels when payment disapproved.
                'For Each sId As String In ConfigParameter._Object._Notify_Payroll_Users.Split(CChar("||"))(0).Split(CChar(","))
                '    Select Case CInt(sId)
                '        Case enNotificationPayroll.AUTHORIZED_PAYEMNT
                'Sohail (10 Mar 2014) -- End


                'Gajanan [27-Mar-2019] -- Start
                'Enhancement - Change Email Language
                'StrMessage.Append("<TABLE border='1' style='margin-left:50px;width:502px;font-size:9.0pt;'>")
                StrMessage.Append("<TABLE border='1' style='width:502px;font-size:9.0pt;'>")
                'Gajanan [27-Mar-2019] -- End


                StrMessage.Append("<TR style='background-color:Purple;color:White; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & ";'>")

                StrMessage.Append("<TD align='center' style='width:300px'>")
                StrMessage.Append(Language.getMessage(mstrModuleName, 6, "Particulars"))
                StrMessage.Append("</TD>")

                StrMessage.Append("<TD align='center' style='width:200px'>")
                StrMessage.Append(Language.getMessage(mstrModuleName, 7, "Emp. Count"))
                StrMessage.Append("</TD>")

                StrMessage.Append("<TD align='center' style='width:200px'>")
                StrMessage.Append(Language.getMessage(mstrModuleName, 8, "Amount") & " (" & strCurrencySign & ")")
                StrMessage.Append("</TD>")

                StrMessage.Append("</TR>")
                '-----------------------------

                StrMessage.Append("<TR style = 'font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#333399;'>")
                StrMessage.Append("<TD style='width:300px'>")
                StrMessage.Append(Language.getMessage(mstrModuleName, 9, "Total Bank Payment"))
                StrMessage.Append("</TD>")

                StrMessage.Append("<TD align='center' style='width:200px'>")
                StrMessage.Append(intBankPaid.ToString)
                StrMessage.Append("</TD>")

                StrMessage.Append("<TD align='right' style='width:200px'>")
                StrMessage.Append(decBankAmount.ToString(GUI.fmtCurrency))
                StrMessage.Append("</TD>")

                StrMessage.Append("</TR>")
                '-----------------------------

                StrMessage.Append("<TR style = 'font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#333399;'>")
                StrMessage.Append("<TD style='width:300px'>")
                StrMessage.Append(Language.getMessage(mstrModuleName, 10, "Total Cash Payment"))
                StrMessage.Append("</TD>")

                StrMessage.Append("<TD width='200px' align='center'>")
                StrMessage.Append(intCashPaid.ToString)
                StrMessage.Append("</TD>")

                StrMessage.Append("<TD width='200px' align='right'>")
                StrMessage.Append(decCashAmount.ToString(GUI.fmtCurrency))
                StrMessage.Append("</TD>")

                StrMessage.Append("</TR>")

                StrMessage.Append("<TR style = 'font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#333399;'>")
                StrMessage.Append("<TD style='width:300px'>")
                StrMessage.Append(Language.getMessage(mstrModuleName, 11, "Total Payment"))
                StrMessage.Append("</TD>")

                StrMessage.Append("<TD align='center' style='width:200px'>")
                StrMessage.Append(intTotalPaid.ToString)
                StrMessage.Append("</TD>")

                StrMessage.Append("<TD align='right' style='width:200px'>")
                StrMessage.Append(decTotalAmount.ToString(GUI.fmtCurrency))
                StrMessage.Append("</TD>")

                StrMessage.Append("</TR>")
                '-----------------------------

                StrMessage.Append("<TR style = 'font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#333399;'>")
                StrMessage.Append("<TD style='width:200px'>")
                StrMessage.Append(Language.getMessage(mstrModuleName, 12, "Remarks"))
                StrMessage.Append("</TD>")

                StrMessage.Append("<TD colspan='2' >")
                StrMessage.Append(strRemarks)
                StrMessage.Append("</TD>")

                StrMessage.Append("</TR>")

                StrMessage.Append("</TABLE>")

                blnFlag = True

                'Sohail (10 Mar 2014) -- Start
                'Enhancement - Send Email Notification to lower levels when payment disapproved.
                '    End Select
                'Next
                'Sohail (10 Mar 2014) -- End


                'Gajanan [27-Mar-2019] -- Start
                'Enhancement - Change Email Language

                'StrMessage.Append("<p><span style='padding-top:25px;font-size:10.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#333399;margin-left:0px;margin-right:0px;margin-top:0px;margin-bottom:10px;'><b>&nbsp;&nbsp;&nbsp;&nbsp;&quot;POWERED BY ARUTI HR & PAYROLL MANAGEMENT SOFTWARE.&quot;</b>")
                StrMessage.Append("<p><center style='padding-top:25px;font-size:10.0pt;font-family:&quot;Verdana&quot;,&quot;Sans-Serif&quot;;color:#333399;margin-left:0px;margin-right:0px;margin-top:0px;margin-bottom:10px'><b>&quot;POWERED BY ARUTI HR &amp; PAYROLL MANAGEMENT SOFTWARE.&quot;</b></center></p>")
                'Gajanan [27-Mar-2019] -- End

                StrMessage.Append("</span></p>")
                StrMessage.Append("</BODY></HTML>")

                If blnFlag = False Then
                    StrMessage = StrMessage.Remove(0, StrMessage.Length)
                End If
            End If
            Return StrMessage.ToString
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "Set_Notification", mstrModuleName)
            Return ""
        End Try
    End Function

    Private Sub Send_Notification()
        Try
            'Sohail (10 Mar 2014) -- Start
            'Enhancement - Send Email Notification to lower levels when payment disapproved.
            'If dicNotification.Keys.Count > 0 Then
            '    Dim objSendMail As New clsSendMail
            '    For Each sKey As String In dicNotification.Keys
            '        If dicNotification(sKey).Trim.Length > 0 Then
            '            objSendMail._ToEmail = sKey
            '            objSendMail._Subject = Language.getMessage(mstrModuleName, 13, "Notification of Payslip Payment Authorized.")
            '            objSendMail._Message = dicNotification(sKey)
            '            objSendMail.SendMail()
            '        End If
            '    Next
            'End If
            If gobjEmailList.Count > 0 Then
                Dim objSendMail As New clsSendMail
                For Each obj In gobjEmailList
                    objSendMail._ToEmail = obj._EmailTo
                    objSendMail._Subject = obj._Subject
                    objSendMail._Message = obj._Message
                    'S.SANDEEP [ 28 JAN 2014 ] -- START
                    'Sohail (17 Dec 2014) -- Start
                    'Enhancement - Provide Link for Payroll approval/Authorize Payslip Payment.
                    'objSendMail._Form_Name = mstrModuleName
                    objSendMail._FormName = obj._FormName
                    objSendMail._ClientIP = getIP()
                    objSendMail._HostName = getHostName()
                    objSendMail._FromWeb = False
                    objSendMail._AuditUserId = User._Object._Userunkid
objSendMail._CompanyUnkid = Company._Object._Companyunkid
                    objSendMail._AuditDate = ConfigParameter._Object._CurrentDateAndTime
                    'Sohail (17 Dec 2014) -- End
                    objSendMail._LogEmployeeUnkid = -1
                    objSendMail._OperationModeId = enLogin_Mode.DESKTOP
                    objSendMail._UserUnkid = User._Object._Userunkid
                    objSendMail._SenderAddress = User._Object._Email
                    objSendMail._ModuleRefId = clsSendMail.enAT_VIEW_TYPE.PAYROLL_MGT
                    'S.SANDEEP [ 28 JAN 2014 ] -- END
                    Try
                        'Sohail (30 Nov 2017) -- Start
                        'SUMATRA Enhancement – SUMATRA – issue # 0001669: Additional feature for Exchange Server configuration setting in 70.1.
                        'objSendMail.SendMail()
                        objSendMail.SendMail(Company._Object._Companyunkid)
                        'Sohail (30 Nov 2017) -- End
                    Catch ex As Exception

                    End Try
                Next
                gobjEmailList.Clear()
            End If
            'Sohail (10 Mar 2014) -- End
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "Send_Notification", mstrModuleName)
        Finally
            If gobjEmailList.Count > 0 Then
                gobjEmailList.Clear()
            End If
        End Try
    End Sub
    'Sohail (09 Mar 2013) -- End

    'Sohail (18 Jun 2019) -- Start
    'Eko Supreme Nigeria Ltd - Support issue # 0003911 - 74.1 : User able to see Payslip l Eko Supreme Nigeria Ltd.
    Private Sub SetVisibility()
        Try
            If mblnFromAuthorize = True Then
                btnProcess.Enabled = User._Object.Privilege._AllowAuthorizePayslipPayment
            Else
                btnProcess.Enabled = User._Object.Privilege._AllowVoidAuthorizedPayslipPayment
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetVisibility", mstrModuleName)
        End Try
    End Sub
    'Sohail (18 Jun 2019) -- End

#End Region

#Region " Form's Events "
    Private Sub frmAuthorizeUnauthorizePayment_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        Try
            objPaymentTran = Nothing
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmAuthorizeUnauthorizePayment_FormClosed", mstrModuleName)
        End Try
    End Sub

    Private Sub frmAuthorizeUnauthorizePayment_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown
        Try
            Select Case e.KeyCode
                Case Keys.Return
                    SendKeys.Send("{TAB}")
                    e.Handled = True
            End Select
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmAuthorizeUnauthorizePayment_KeyDown", mstrModuleName)
        End Try
    End Sub

    Private Sub frmAuthorizeUnauthorizePayment_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        objPaymentTran = New clsPayment_tran
        Try
            Call Set_Logo(Me, gApplicationType)

            Call Language.setLanguage(Me.Name)
            Call OtherSettings()

            Call SetColor()
            Call FillCombo()

            'Sohail (26 May 2017) -- Start
            'Issue - 67.1 - Authorize payment total not matching with global payment total.
            Call SetDefaultSearchEmpText()
            'Sohail (26 May 2017) -- End

            If mblnFromAuthorize = True Then
                Me.Text = Language.getMessage(mstrModuleName, 16, "Authorize Payment")
                eZeeHeader.Title = Language.getMessage(mstrModuleName, 16, "Authorize Payment")
                btnProcess.Text = Language.getMessage(mstrModuleName, 17, "Authorize Payment")
                lblRemarks.Text = Language.getMessage(mstrModuleName, 18, "Remarks") 'Sohail (27 Feb 2013)
            Else
                Me.Text = Language.getMessage(mstrModuleName, 19, "Void Authorized Payment")
                eZeeHeader.Title = Language.getMessage(mstrModuleName, 19, "Void Authorized Payment")
                btnProcess.Text = Language.getMessage(mstrModuleName, 20, "Void Authorized Payment")
                lblRemarks.Text = Language.getMessage(mstrModuleName, 18, "Remarks") 'Sohail (27 Feb 2013)
            End If

            'Hemant (29 Dec 2018) -- Start
            'Enhancement - On payroll authorizattion screen, provide a link to payroll variance report. When user clicks on the link, he/she to view the payroll variance report of the month that has been processed in 76.1.
            lnkPayrollVarianceReport.Visible = clsArutiReportClass.Is_Report_Assigned(enArutiReport.PayrollVariance, User._Object._Userunkid, Company._Object._Companyunkid)
            'Hemant (29 Dec 2018) -- End
            'Hemant (17 Oct 2019) -- Start
            lnkPayrollTotalVarianceReport.Visible = clsArutiReportClass.Is_Report_Assigned(enArutiReport.PayrollTotalVariance, User._Object._Userunkid, Company._Object._Companyunkid)
            'Hemant (17 Oct 2019) -- End
            'Sohail (18 Jun 2019) -- Start
            'Eko Supreme Nigeria Ltd - Support issue # 0003911 - 74.1 : User able to see Payslip l Eko Supreme Nigeria Ltd.
            Call SetVisibility()
            'Sohail (18 Jun 2019) -- End
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmAuthorizeUnauthorizePayment_Load", mstrModuleName)
        End Try
    End Sub

    'Sohail (10 Jul 2014) -- Start
    'Enhancement - Custom Language.
    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsPayment_authorize_tran.SetMessages()
            objfrm._Other_ModuleNames = "clsPayment_authorize_tran"
            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub
    'Sohail (10 Jul 2014) -- End

#End Region

#Region " ComboBox's Events "
    Private Sub cboPayPeriod_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboPayPeriod.SelectedIndexChanged
        Try
            'Sohail (18 Feb 2014) -- Start
            'Enhancement - AGKN
            If CInt(cboPayPeriod.SelectedValue) > 0 Then
                Dim objPayment As New clsPayment_tran
                Dim dsCombo As DataSet
                dsCombo = objPayment.Get_DIST_VoucherNo("Voucher", True, clsPayment_tran.enPaymentRefId.PAYSLIP, clsPayment_tran.enPayTypeId.PAYMENT, FinancialYear._Object._DatabaseName, CInt(cboPayPeriod.SelectedValue))
                With cboPmtVoucher
                    .ValueMember = "voucherno"
                    .DisplayMember = "voucherno"
                    .DataSource = dsCombo.Tables("Voucher")
                    .SelectedIndex = 0
                End With
                objPayment = Nothing
                'Sohail (15 Feb 2016) -- Start
                'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                mdtPayPeriodStartDate = eZeeDate.convertDate(CType(cboPayPeriod.SelectedItem, DataRowView).Item("start_date").ToString)
                mdtPayPeriodEndDate = eZeeDate.convertDate(CType(cboPayPeriod.SelectedItem, DataRowView).Item("end_date").ToString)
            Else
                mdtPayPeriodStartDate = eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate)
                mdtPayPeriodEndDate = eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate)
                'Sohail (15 Feb 2016) -- End
            End If
            'Sohail (18 Feb 2014) -- End
            Call FillList()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboPayPeriod_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub cboCurrency_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboCurrency.SelectedIndexChanged
        Try
            Call FillList()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboCurrency_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub
#End Region

    'Sohail (10 Mar 2014) -- Start
    'Enhancement - Check list box on Payment Approval and Authorize Payment Screen.
    'Sohail (26 May 2017) -- Start
    'Issue - 67.1 - Authorize payment total not matching with global payment total.
    '#Region " Listview's Events "
    '    Private Sub lvEmployeeList_ItemChecked(ByVal sender As Object, ByVal e As System.Windows.Forms.ItemCheckedEventArgs)
    '        Dim decTotAmt As Decimal
    '        Try
    '            decTotAmt = 0

    '            If lvEmployeeList.CheckedItems.Count <= 0 Then
    '                RemoveHandler objchkSelectAll.CheckedChanged, AddressOf chkSelectAll_CheckedChanged
    '                objchkSelectAll.CheckState = CheckState.Unchecked
    '                AddHandler objchkSelectAll.CheckedChanged, AddressOf chkSelectAll_CheckedChanged
    '            ElseIf lvEmployeeList.CheckedItems.Count < lvEmployeeList.Items.Count Then
    '                RemoveHandler objchkSelectAll.CheckedChanged, AddressOf chkSelectAll_CheckedChanged
    '                objchkSelectAll.CheckState = CheckState.Indeterminate
    '                AddHandler objchkSelectAll.CheckedChanged, AddressOf chkSelectAll_CheckedChanged
    '            ElseIf lvEmployeeList.CheckedItems.Count = lvEmployeeList.Items.Count Then
    '                RemoveHandler objchkSelectAll.CheckedChanged, AddressOf chkSelectAll_CheckedChanged
    '                objchkSelectAll.CheckState = CheckState.Checked
    '                AddHandler objchkSelectAll.CheckedChanged, AddressOf chkSelectAll_CheckedChanged
    '            End If

    '            'Sohail (26 May 2017) -- Start
    '            'Issue - 67.1 - Authorize payment total not matching with global payment total.
    '            'mdecTotAmt = (From p In lvEmployeeList.CheckedItems.Cast(Of ListViewItem)() Select CDec(p.SubItems(colhAmount.Index).Text)).Sum()
    '            mdecTotAmt = (From p In lvEmployeeList.CheckedItems.Cast(Of ListViewItem)() Select CDec(p.SubItems(colhAmount.Index).Tag)).Sum()
    '            'Sohail (26 May 2017) -- End

    '            txtTotalAmount.Text = Format(mdecTotAmt, GUI.fmtCurrency)

    '        Catch ex As Exception
    '            DisplayError.Show("-1", ex.Message, "lvEmployeeList_ItemChecked", mstrModuleName)
    '        End Try
    '    End Sub
    '#End Region
#Region " GridView Events "

    Private Sub dgEmployee_CurrentCellDirtyStateChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles dgEmployee.CurrentCellDirtyStateChanged
        Try
            If dgEmployee.IsCurrentCellDirty Then
                dgEmployee.CommitEdit(DataGridViewDataErrorContexts.Commit)
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgEmployee_CurrentCellDirtyStateChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub dgEmployee_CellValueChanged(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgEmployee.CellValueChanged
        Try
            If e.RowIndex < 0 Then Exit Sub

            If e.ColumnIndex = objdgcolhCheck.Index Then
                SetCheckBoxValue()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgEmployee_CellValueChanged", mstrModuleName)
        End Try
    End Sub
#End Region
    'Sohail (26 May 2017) -- End

#Region " CheckBox's Events "
    Private Sub chkSelectAll_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objchkSelectAll.CheckedChanged
        Dim decTotAmt As Decimal = 0

        Try
            Call CheckAllEmployee(objchkSelectAll.Checked)
            'Sohail (26 May 2017) -- Start
            'Issue - 67.1 - Authorize payment total not matching with global payment total.
            'mdecTotAmt = (From p In lvEmployeeList.CheckedItems.Cast(Of ListViewItem)() Select CDec(p.SubItems(colhAmount.Index).Text)).Sum()

            'txtTotalAmount.Text = Format(mdecTotAmt, GUI.fmtCurrency)
            'Sohail (26 May 2017) -- End

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "chkSelectAll_CheckedChanged", mstrModuleName)
        End Try
    End Sub
#End Region
    'Sohail (10 Mar 2014) -- End

#Region " Button's Events "
    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnClose_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnProcess_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnProcess.Click
        Dim blnResult As Boolean
        Try
            If IsValid() = False Then Exit Sub

            'Sohail (10 Mar 2014) -- Start
            'Enhancement - Check list box on Payment Approval and Authorize Payment Screen.
            'Sohail (26 May 2017) -- Start
            'Issue - 67.1 - Authorize payment total not matching with global payment total.
            'Dim arrList As List(Of String) = (From p In lvEmployeeList.CheckedItems.Cast(Of ListViewItem)() Select (p.Tag.ToString)).ToList
            Dim arrList As List(Of String) = (From p In dvEmployee.Table Where (CBool(p.Item("IsChecked")) = True) Select (p.Item("paymenttranunkid").ToString)).ToList
            'Sohail (26 May 2017) -- End
            mstrPaymentTranIDs = String.Join(",", arrList.ToArray)
            'Sohail (10 Mar 2014) -- End

            'S.SANDEEP [28-May-2018] -- START
            'ISSUE/ENHANCEMENT : {Audit Trails} 
            objPaymentTran._FormName = mstrModuleName
            objPaymentTran._LoginEmployeeUnkid = 0
            objPaymentTran._ClientIP = getIP()
            objPaymentTran._HostName = getHostName()
            objPaymentTran._FromWeb = False
            objPaymentTran._AuditUserId = User._Object._Userunkid
objPaymentTran._CompanyUnkid = Company._Object._Companyunkid
            objPaymentTran._AuditDate = ConfigParameter._Object._CurrentDateAndTime
            'S.SANDEEP [28-May-2018] -- END
Dim dtStart As Date = DateAndTime.Now
            blnResult = objPaymentTran.UpdatePaymentAuthorization(mblnFromAuthorize, mstrPaymentTranIDs, User._Object._Userunkid, True, txtRemarks.Text.Trim)
            'Sohail (27 Feb 2013) - [txtRemarks.Text.Trim]

            If blnResult = False And objPaymentTran._Message <> "" Then
                eZeeMsgBox.Show(objPaymentTran._Message, enMsgBoxStyle.Information)
            Else
                'Sohail (04 Jun 2018) -- Start
                'Internal Enhancement - Providing Summary count on process payroll screen and Payslip screen in 72.1.
                mblnCancel = False
                'Sohail (04 Jun 2018) -- End
                If mblnFromAuthorize = True Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Payment Authorization process completed successfully."), enMsgBoxStyle.Information, eZeeMsgBox.MessageBoxButton.DefaultTitle & " [in " & CInt((DateAndTime.Now - dtStart).TotalSeconds).ToString & " Seconds.]")
                Else
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Void Authorized Payment process completed successfully."), enMsgBoxStyle.Information, eZeeMsgBox.MessageBoxButton.DefaultTitle & " [in " & CInt((DateAndTime.Now - dtStart).TotalSeconds).ToString & " Seconds.]")
                End If

                'Sohail (09 Mar 2013) -- Start
                'TRA - ENHANCEMENT
                'dicNotification.Clear() 'Sohail (10 Mar 2014)
                If ConfigParameter._Object._Notify_Payroll_Users.Trim.Length > 0 Then

                    'Sohail (10 Mar 2014) -- Start
                    'Enhancement - Send Email Notification to lower levels when payment disapproved.
                    Dim objMaster As New clsMasterData
                    gobjEmailList = New List(Of clsEmailCollection)
                    Dim strIDs As String = ""
                    If mblnFromAuthorize = True Then
                        If ConfigParameter._Object._Notify_Payroll_Users.Split(CChar("||"))(0).Split(CChar(",")).Contains(CStr(enNotificationPayroll.AUTHORIZED_PAYEMNT)) = True Then
                            strIDs = ConfigParameter._Object._Notify_Payroll_Users.Split(CChar("||"))(2)
                        End If
                    Else
                        ''Send notification to those users who has privilede to Authorize Payment again.
                        'Dim objApproverMap As New clspayment_approver_mapping
                        'strIDs = objApproverMap.GetAuthorizePaymentUserUnkIDs()
                        If ConfigParameter._Object._Notify_Payroll_Users.Split(CChar("||"))(0).Split(CChar(",")).Contains(CStr(enNotificationPayroll.VOID_AUTHORIZED_PAYEMNT)) = True Then
                            strIDs = ConfigParameter._Object._Notify_Payroll_Users.Split(CChar("||"))(2)
                        End If
                    End If
                    'Sohail (10 Mar 2014) -- End

                    Dim objUsr As New clsUserAddEdit : Dim StrMessage As String = String.Empty
                    'Sohail (10 Mar 2014) -- Start
                    'Enhancement - Send Email Notification to lower levels when payment disapproved.
                    'For Each sId As String In ConfigParameter._Object._Notify_Payroll_Users.Split(CChar("||"))(2).Split(CChar(","))
                    For Each sId As String In strIDs.Split(CChar(","))
                        'Sohail (10 Mar 2014) -- End
                        If sId.Trim = "" Then Continue For 'Sohail (15 Mar 2014)

                        objUsr._Userunkid = CInt(sId)

                        'Sohail (10 Mar 2014) -- Start
                        'Enhancement - Check list box on Payment Approval and Authorize Payment Screen.
                        'Sohail (16 Nov 2016) -- Start
                        'Enhancement - 64.1 - Get User Access Level Employees from new employee transfer table and employee categorization table.
                        'Dim strAppEmpIDs As String = objMaster.GetUserAccessLevelEmployees(CInt(sId), Company._Object._Companyunkid, FinancialYear._Object._YearUnkid, FinancialYear._Object._DatabaseName)
                        Dim strAppEmpIDs As String = objMaster.GetUserAccessLevelEmployees(FinancialYear._Object._DatabaseName, CInt(sId), FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, mdtPayPeriodStartDate, mdtPayPeriodEndDate, ConfigParameter._Object._UserAccessModeSetting, True, False)
                        'Sohail (16 Nov 2016) -- End

                        'Sohail (26 Apr 2017) -- Start
                        'AKFKU Issue - 66.1 - Emails were going to the users who dont have access to authorized employees due to brackets were missing for OrElse block.
                        'Dim lstBankPaid As List(Of ListViewItem) = (From p In lvEmployeeList.CheckedItems.Cast(Of ListViewItem)() Where strAppEmpIDs.Split(CChar(",")).Contains(p.SubItems(colhName.Index).Tag.ToString) AndAlso CInt(p.SubItems(colhCode.Index).Tag) = enPaymentMode.CHEQUE OrElse CInt(p.SubItems(colhCode.Index).Tag) = enPaymentMode.TRANSFER Select p).ToList
                        'mintBankPayment = lstBankPaid.Count

                        'mdecBankPayment = (From p In lvEmployeeList.CheckedItems.Cast(Of ListViewItem)() Where strAppEmpIDs.Split(CChar(",")).Contains(p.SubItems(colhName.Index).Tag.ToString) AndAlso CInt(p.SubItems(colhCode.Index).Tag) = enPaymentMode.CHEQUE OrElse CInt(p.SubItems(colhCode.Index).Tag) = enPaymentMode.TRANSFER Select CDec(p.SubItems(colhAmount.Index).Tag)).Sum()


                        'Dim lstCashPaid As List(Of ListViewItem) = (From p In lvEmployeeList.CheckedItems.Cast(Of ListViewItem)() Where strAppEmpIDs.Split(CChar(",")).Contains(p.SubItems(colhName.Index).Tag.ToString) AndAlso CInt(p.SubItems(colhCode.Index).Tag) = enPaymentMode.CASH OrElse CInt(p.SubItems(colhCode.Index).Tag) = enPaymentMode.CASH_AND_CHEQUE Select p).ToList
                        'mintCashPayment = lstCashPaid.Count

                        'mdecCashPayment = (From p In lvEmployeeList.CheckedItems.Cast(Of ListViewItem)() Where strAppEmpIDs.Split(CChar(",")).Contains(p.SubItems(colhName.Index).Tag.ToString) AndAlso CInt(p.SubItems(colhCode.Index).Tag) = enPaymentMode.CASH OrElse CInt(p.SubItems(colhCode.Index).Tag) = enPaymentMode.CASH_AND_CHEQUE Select CDec(p.SubItems(colhAmount.Index).Tag)).Sum()
                        'Sohail (26 May 2017) -- Start
                        'Issue - 67.1 - Authorize payment total not matching with global payment total.
                        'Dim lstBankPaid As List(Of ListViewItem) = (From p In lvEmployeeList.CheckedItems.Cast(Of ListViewItem)() Where strAppEmpIDs.Split(CChar(",")).Contains(p.SubItems(colhName.Index).Tag.ToString) AndAlso (CInt(p.SubItems(colhCode.Index).Tag) = enPaymentMode.CHEQUE OrElse CInt(p.SubItems(colhCode.Index).Tag) = enPaymentMode.TRANSFER) Select p).ToList
                        Dim lstBankPaid As List(Of DataRow) = (From p In dvEmployee.Table Where (CBool(p.Item("IsChecked")) = True AndAlso strAppEmpIDs.Split(CChar(",")).Contains(p.Item("employeeunkid").ToString) AndAlso (CInt(p.Item("paymentmode")) = enPaymentMode.CHEQUE OrElse CInt(p.Item("paymentmode")) = enPaymentMode.TRANSFER)) Select p).ToList
                        'Sohail (26 May 2017) -- End
                        mintBankPayment = lstBankPaid.Count

                        'Sohail (26 May 2017) -- Start
                        'Issue - 67.1 - Authorize payment total not matching with global payment total.
                        'mdecBankPayment = (From p In lvEmployeeList.CheckedItems.Cast(Of ListViewItem)() Where strAppEmpIDs.Split(CChar(",")).Contains(p.SubItems(colhName.Index).Tag.ToString) AndAlso (CInt(p.SubItems(colhCode.Index).Tag) = enPaymentMode.CHEQUE OrElse CInt(p.SubItems(colhCode.Index).Tag) = enPaymentMode.TRANSFER) Select CDec(p.SubItems(colhAmount.Index).Tag)).Sum()
                        mdecBankPayment = (From p In dvEmployee.Table Where (CBool(p.Item("IsChecked")) = True AndAlso strAppEmpIDs.Split(CChar(",")).Contains(p.Item("employeeunkid").ToString) AndAlso (CInt(p.Item("paymentmode")) = enPaymentMode.CHEQUE OrElse CInt(p.Item("paymentmode")) = enPaymentMode.TRANSFER)) Select CDec(p.Item("expaidamt"))).Sum()
                        'Sohail (26 May 2017) -- End

                        'Sohail (26 May 2017) -- Start
                        'Issue - 67.1 - Authorize payment total not matching with global payment total.
                        'Dim lstCashPaid As List(Of ListViewItem) = (From p In lvEmployeeList.CheckedItems.Cast(Of ListViewItem)() Where strAppEmpIDs.Split(CChar(",")).Contains(p.SubItems(colhName.Index).Tag.ToString) AndAlso (CInt(p.SubItems(colhCode.Index).Tag) = enPaymentMode.CASH OrElse CInt(p.SubItems(colhCode.Index).Tag) = enPaymentMode.CASH_AND_CHEQUE) Select p).ToList
                        Dim lstCashPaid As List(Of DataRow) = (From p In dvEmployee.Table Where (CBool(p.Item("IsChecked")) = True AndAlso strAppEmpIDs.Split(CChar(",")).Contains(p.Item("employeeunkid").ToString) AndAlso (CInt(p.Item("paymentmode")) = enPaymentMode.CASH OrElse CInt(p.Item("paymentmode")) = enPaymentMode.CASH_AND_CHEQUE)) Select p).ToList
                        'Sohail (26 May 2017) -- End
                        mintCashPayment = lstCashPaid.Count

                        'Sohail (26 May 2017) -- Start
                        'Issue - 67.1 - Authorize payment total not matching with global payment total.
                        'mdecCashPayment = (From p In lvEmployeeList.CheckedItems.Cast(Of ListViewItem)() Where strAppEmpIDs.Split(CChar(",")).Contains(p.SubItems(colhName.Index).Tag.ToString) AndAlso (CInt(p.SubItems(colhCode.Index).Tag) = enPaymentMode.CASH OrElse CInt(p.SubItems(colhCode.Index).Tag) = enPaymentMode.CASH_AND_CHEQUE) Select CDec(p.SubItems(colhAmount.Index).Tag)).Sum()
                        mdecCashPayment = (From p In dvEmployee.Table Where (CBool(p.Item("IsChecked")) = True AndAlso strAppEmpIDs.Split(CChar(",")).Contains(p.Item("employeeunkid").ToString) AndAlso (CInt(p.Item("paymentmode")) = enPaymentMode.CASH OrElse CInt(p.Item("paymentmode")) = enPaymentMode.CASH_AND_CHEQUE)) Select CDec(p.Item("expaidamt"))).Sum()
                        'Sohail (26 May 2017) -- End

                        If (mintBankPayment + mintCashPayment) <= 0 Then Continue For
                        'Sohail (26 Apr 2017) -- End
                        'Sohail (10 Mar 2014) -- End


                        StrMessage = Set_Notification(objUsr._Firstname & " " & objUsr._Lastname, User._Object._Firstname & " " & User._Object._Lastname, cboPayPeriod.Text, mintBankPayment, mintCashPayment, (mintBankPayment + mintCashPayment), mdecBankPayment, mdecCashPayment, (mdecBankPayment + mdecCashPayment), cboCurrency.Text, txtRemarks.Text.Trim)
                        'Sohail (10 Mar 2014) -- Start
                        'Enhancement - Send Email Notification to lower levels when payment disapproved.
                        'If dicNotification.ContainsKey(objUsr._Email) = False Then
                        '    dicNotification.Add(objUsr._Email, StrMessage)
                        'End If
                        If mblnFromAuthorize = True Then
                            'Sohail (21 Apr 2014) -- Start
                            'Enhancement - Salary Distribution by Amount and bank priority.
                            'gobjEmailList.Add(New clsEmailCollection(objUsr._Email, Language.getMessage(mstrModuleName, 13, "Notification of Payslip Payment Authorized."), StrMessage))
                            gobjEmailList.Add(New clsEmailCollection(objUsr._Email, Language.getMessage(mstrModuleName, 13, "Notification of Payslip Payment Authorized."), StrMessage, "", 0, "", "", 0, 0, 0, ""))
                            'Sohail (21 Apr 2014) -- End
                        Else
                            'Sohail (21 Apr 2014) -- Start
                            'Enhancement - Salary Distribution by Amount and bank priority.
                            'gobjEmailList.Add(New clsEmailCollection(objUsr._Email, Language.getMessage(mstrModuleName, 14, "Notification of Payslip Payment Authorization Voided."), StrMessage))
                            gobjEmailList.Add(New clsEmailCollection(objUsr._Email, Language.getMessage(mstrModuleName, 14, "Notification of Payslip Payment Authorization Voided."), StrMessage, "", 0, "", "", 0, 0, 0, ""))
                            'Sohail (21 Apr 2014) -- End
                        End If
                        'Sohail (10 Mar 2014) -- End

                    Next
                    objUsr = Nothing

                    trd = New Thread(AddressOf Send_Notification)
                    trd.IsBackground = True
                    trd.Start()
                End If
                'Sohail (09 Mar 2013) -- End

                cboPayPeriod.SelectedValue = 0
                txtRemarks.Text = "" 'Sohail (27 Feb 2013)
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnProcess_Click", mstrModuleName)
        End Try
    End Sub
#End Region

    'Sohail (18 Feb 2014) -- Start
    'Enhancement - AGKN
#Region " Other Controls Events "
    Private Sub objbtnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles objbtnSearch.Click
        Try
            Call FillList()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearch_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnReset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles objbtnReset.Click
        Try
            If cboPmtVoucher.Items.Count > 0 Then cboPmtVoucher.SelectedIndex = 0
            mstrAdvanceFilter = ""
            'Sohail (26 May 2017) -- Start
            'Issue - 67.1 - Authorize payment total not matching with global payment total.
            dgEmployee.DataSource = Nothing
            'Sohail (26 May 2017) -- End
            Call FillList()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnReset_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub lnkAdvanceFilter_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkAdvanceFilter.Click
        Try
            Dim frm As New frmAdvanceSearch
            frm.ShowDialog()
            mstrAdvanceFilter = frm._GetFilterString
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lnkAdvanceFilter_Click", mstrModuleName)
        End Try
    End Sub
    'Hemant (29 Dec 2018) -- Start
    'Enhancement - On payroll authorizattion screen, provide a link to payroll variance report. When user clicks on the link, he/she to view the payroll variance report of the month that has been processed in 76.1.
    Private Sub lnkPayrollVarianceReport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkPayrollVarianceReport.Click
        Try
            Dim frm As New ArutiReports.frmPayrollVariance
            frm.FormBorderStyle = Windows.Forms.FormBorderStyle.FixedSingle
            frm.MaximizeBox = True
            frm.MaximizeBox = False
            frm.StartPosition = FormStartPosition.CenterParent
            frm.WindowState = FormWindowState.Maximized
            frm.ShowDialog()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lnkPayrollVarianceReport_Click", mstrModuleName)
        End Try
    End Sub
    'Hemant (29 Dec 2018) -- End

     'Hemant (17 Oct 2019) -- Start
    Private Sub lnkPayrollTotalVarianceReport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkPayrollTotalVarianceReport.Click
        Try
            Dim frm As New ArutiReports.frmPayrollTotalVariance
            frm.FormBorderStyle = Windows.Forms.FormBorderStyle.FixedSingle
            frm.MaximizeBox = True
            frm.MaximizeBox = False
            frm.StartPosition = FormStartPosition.CenterParent
            frm.WindowState = FormWindowState.Maximized
            frm.ShowDialog()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lnkPayrollTotalVarianceReport_Click", mstrModuleName)
        End Try
    End Sub
    'Hemant (17 Oct 2019) -- End

    'Sohail (26 May 2017) -- Start
    'Issue - 67.1 - Authorize payment total not matching with global payment total.
    Private Sub txtSearchEmp_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtSearchEmp.GotFocus
        Try
            With txtSearchEmp
                .ForeColor = Color.Black
                .Font = New Font(Me.Font.FontFamily, Me.Font.Size, FontStyle.Regular)

                If .Text = mstrSearchEmpText Then
                    .Clear()
                End If
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "txtSearchEmp_GotFocus", mstrModuleName)
        End Try
    End Sub

    Private Sub txtSearchEmp_Leave(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtSearchEmp.Leave
        Try
            If txtSearchEmp.Text.Trim = "" Then
                Call SetDefaultSearchEmpText()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "txtSearchEmp_Leave", mstrModuleName)
        End Try
    End Sub

    Private Sub txtSearchEmp_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtSearchEmp.TextChanged
        Try
            If txtSearchEmp.Text.Trim = mstrSearchEmpText Then Exit Sub
            If dvEmployee IsNot Nothing Then
                dvEmployee.RowFilter = "employeecode LIKE '%" & txtSearchEmp.Text.Replace("'", "''") & "%'  OR EmpName LIKE '%" & txtSearchEmp.Text.Replace("'", "''") & "%'"
                dgEmployee.Refresh()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "txtSearchEmp_TextChanged", mstrModuleName)
        End Try
    End Sub
    'Sohail (26 May 2017) -- End
#End Region
    'Sohail (18 Feb 2014) -- End

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Private Sub OtherSettings()
        Try
            Me.SuspendLayout()
            
            Call SetLanguage()

            Me.gbAmountInfo.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbAmountInfo.ForeColor = GUI._eZeeContainerHeaderForeColor

            Me.gbPaymentInfo.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbPaymentInfo.ForeColor = GUI._eZeeContainerHeaderForeColor

            Me.gbPaymentModeInfo.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbPaymentModeInfo.ForeColor = GUI._eZeeContainerHeaderForeColor

			Me.gbEmployeeList.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbEmployeeList.ForeColor = GUI._eZeeContainerHeaderForeColor 



            Me.eZeeHeader.GradientColor1 = GUI._HeaderBackColor1
            Me.eZeeHeader.GradientColor2 = GUI._HeaderBackColor2
            Me.eZeeHeader.BorderColor = GUI._HeaderBorderColor
            Me.eZeeHeader.HeaderTextForeColor = GUI._HeaderTitleFontColor
            Me.eZeeHeader.DescriptionForeColor = GUI._HeaderMessageFontColor


            Me.btnProcess.GradientBackColor = GUI._ButttonBackColor
            Me.btnProcess.GradientForeColor = GUI._ButttonFontColor

            Me.btnClose.GradientBackColor = GUI._ButttonBackColor
            Me.btnClose.GradientForeColor = GUI._ButttonFontColor


            Me.ResumeLayout()
        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "OtherSettings", mstrModuleName)
        End Try
    End Sub


    Private Sub SetLanguage()
        Try
            Me.Text = Language._Object.getCaption(Me.Name, Me.Text)

            Me.gbAmountInfo.Text = Language._Object.getCaption(Me.gbAmountInfo.Name, Me.gbAmountInfo.Text)
            Me.lblTotalAmount.Text = Language._Object.getCaption(Me.lblTotalAmount.Name, Me.lblTotalAmount.Text)
			Me.eZeeHeader.Title = Language._Object.getCaption(Me.eZeeHeader.Name & "_Title" , Me.eZeeHeader.Title)
			Me.eZeeHeader.Message = Language._Object.getCaption(Me.eZeeHeader.Name & "_Message" , Me.eZeeHeader.Message)
            Me.gbPaymentInfo.Text = Language._Object.getCaption(Me.gbPaymentInfo.Name, Me.gbPaymentInfo.Text)
            Me.lblCurrency.Text = Language._Object.getCaption(Me.lblCurrency.Name, Me.lblCurrency.Text)
            Me.lblPayPeriod.Text = Language._Object.getCaption(Me.lblPayPeriod.Name, Me.lblPayPeriod.Text)
            Me.btnProcess.Text = Language._Object.getCaption(Me.btnProcess.Name, Me.btnProcess.Text)
            Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
            Me.gbPaymentModeInfo.Text = Language._Object.getCaption(Me.gbPaymentModeInfo.Name, Me.gbPaymentModeInfo.Text)
            Me.lblCashPaid.Text = Language._Object.getCaption(Me.lblCashPaid.Name, Me.lblCashPaid.Text)
            Me.lblbankPaid.Text = Language._Object.getCaption(Me.lblbankPaid.Name, Me.lblbankPaid.Text)
            Me.lblTotalPaid.Text = Language._Object.getCaption(Me.lblTotalPaid.Name, Me.lblTotalPaid.Text)
            Me.lblRemarks.Text = Language._Object.getCaption(Me.lblRemarks.Name, Me.lblRemarks.Text)
            Me.lnkAdvanceFilter.Text = Language._Object.getCaption(Me.lnkAdvanceFilter.Name, Me.lnkAdvanceFilter.Text)
            Me.lblPmtVoucher.Text = Language._Object.getCaption(Me.lblPmtVoucher.Name, Me.lblPmtVoucher.Text)
			Me.gbEmployeeList.Text = Language._Object.getCaption(Me.gbEmployeeList.Name, Me.gbEmployeeList.Text)
			Me.lblSearchEmp.Text = Language._Object.getCaption(Me.lblSearchEmp.Name, Me.lblSearchEmp.Text)
			Me.dgColhEmpCode.HeaderText = Language._Object.getCaption(Me.dgColhEmpCode.Name, Me.dgColhEmpCode.HeaderText)
			Me.dgColhEmployee.HeaderText = Language._Object.getCaption(Me.dgColhEmployee.Name, Me.dgColhEmployee.HeaderText)
			Me.dgcolhAmount.HeaderText = Language._Object.getCaption(Me.dgcolhAmount.Name, Me.dgcolhAmount.HeaderText)
			Me.dgcolhCurrency.HeaderText = Language._Object.getCaption(Me.dgcolhCurrency.Name, Me.dgcolhCurrency.HeaderText)
			Me.lnkPayrollVarianceReport.Text = Language._Object.getCaption(Me.lnkPayrollVarianceReport.Name, Me.lnkPayrollVarianceReport.Text)
            Me.lnkPayrollTotalVarianceReport.Text = Language._Object.getCaption(Me.lnkPayrollTotalVarianceReport.Name, Me.lnkPayrollTotalVarianceReport.Text)

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
        End Try
    End Sub


    Private Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Payment Authorization process completed successfully.")
            Language.setMessage(mstrModuleName, 2, "Void Authorized Payment process completed successfully.")
            Language.setMessage(mstrModuleName, 3, "Please select Pay Period. Pay Period is mandatory information.")
            Language.setMessage(mstrModuleName, 4, "Please select Currency. Currency is mandatory information.")
            Language.setMessage(mstrModuleName, 5, "Please select atleast one employee to Authorize / Void Authorized.")
            Language.setMessage(mstrModuleName, 6, "Particulars")
            Language.setMessage(mstrModuleName, 7, "Emp. Count")
            Language.setMessage(mstrModuleName, 8, "Amount")
            Language.setMessage(mstrModuleName, 9, "Total Bank Payment")
            Language.setMessage(mstrModuleName, 10, "Total Cash Payment")
            Language.setMessage(mstrModuleName, 11, "Total Payment")
            Language.setMessage(mstrModuleName, 12, "Remarks")
            Language.setMessage(mstrModuleName, 13, "Notification of Payslip Payment Authorized.")
            Language.setMessage(mstrModuleName, 14, "Notification of Payslip Payment Authorization Voided.")
            Language.setMessage(mstrModuleName, 15, "Sending Email(s) process is in progress from other module. Please wait.")
            Language.setMessage(mstrModuleName, 16, "Authorize Payment")
            Language.setMessage(mstrModuleName, 17, "Authorize Payment")
            Language.setMessage(mstrModuleName, 18, "Remarks")
            Language.setMessage(mstrModuleName, 19, "Void Authorized Payment")
            Language.setMessage(mstrModuleName, 20, "Void Authorized Payment")
			Language.setMessage(mstrModuleName, 21, "Dear")
			Language.setMessage(mstrModuleName, 22, "This is to inform you that Payslip Payment has been authorized for the period of")
			Language.setMessage(mstrModuleName, 23, "for")
			Language.setMessage(mstrModuleName, 24, "employees. This Payment has been authorized by")
			Language.setMessage(mstrModuleName, 25, "from Machine")
			Language.setMessage(mstrModuleName, 27, "and IPAddress")
			Language.setMessage(mstrModuleName, 28, "This is to inform you that Payslip Payment Authorization has been voided for the period of")
			Language.setMessage(mstrModuleName, 29, "employees. This Payment Authorization has been voided by")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class