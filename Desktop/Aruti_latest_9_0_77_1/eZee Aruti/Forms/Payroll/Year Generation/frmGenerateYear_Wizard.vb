﻿Option Strict On

Imports eZeeCommonLib
Imports Aruti.Data

Public Class frmGenerateYear_Wizard

#Region " Private Variables "
    Private ReadOnly mstrModuleName As String = "frmGenerateYear_Wizard"

    Private mintCurrYearUnkID As Integer
    Private mdtCurrYearStartDate As Date
    Private mdtCurrYearEndDate As Date
    Private mdtNewYearStartDate As Date
    Private mdtNewYearEndDate As Date
    'Sohail (09 Nov 2013) -- Start
    'TRA - ENHANCEMENT
    Private mintLastPeriodUnkID As Integer
    Private mdtLastPeriodStartDate As Date
    Private mdtLastPeriodEndDate As Date
    'Sohail (09 Nov 2013) -- End
    Private mdtLastPeriodTnAEndDate As Date 'Sohail (07 Jan 2014)

    Private mblnCancel As Boolean = True

    Private objCloseYear As clsCloseYear

    Private mdtLeave As DataTable
    Private mdtPayroll As DataTable
    Private mdtProcessPendingLoan As DataTable
    Private mdtLoan As DataTable
    Private mdtSavings As DataTable
    Private mdtApplicant As DataTable 'Sohail (03 Nov 2010)

    Private mstrLeave As String
    Private mstrLeaveTypeIDs As String  ' Pinkal (30-May-2013)
    Private mstrPayroll As String
    Private mstrProcessPendingLoan As String
    Private mstrLoan As String
    Private mstrSavings As String
    Private mstrApplicants As String = ""

    Dim menLeavAction As clsCloseYear.enLeaveAction 'Sohail (10 Feb 2012)

    Private WithEvents objBackupDatabase As New eZeeDatabase

    'Sohail (06 Jan 2016) -- Start
    'Enhancement - Show Close Year Process Logs on Close Year Wizard.
    Private mblnProcessFailed As Boolean = False
    Private mstrCloseYearLogFileName As String = ""
    'Sohail (06 Jan 2016) -- End

#End Region

#Region " Display Dialog "
    Public Function displayDialog(ByVal intCurrYearID As Integer, ByVal dtCurrYrStartDt As Date, ByVal dtCurrYrEndDt As Date) As Boolean
        Try
            mintCurrYearUnkID = intCurrYearID
            mdtCurrYearStartDate = dtCurrYrStartDt
            mdtCurrYearEndDate = dtCurrYrEndDt

            mdtNewYearStartDate = DateAdd(DateInterval.Year, 1, mdtCurrYearStartDate)
            mdtNewYearEndDate = DateAdd(DateInterval.Year, 1, mdtCurrYearEndDate)

            Me.ShowDialog()

            Return Not mblnCancel
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "displayDialog", mstrModuleName)
        End Try
    End Function
#End Region

#Region " Private Methods "
    Public Sub SetDate()
        Try
            dtpCurrStartdate.Value = mdtCurrYearStartDate
            dtpCurrStartdate.MinDate = mdtCurrYearStartDate
            dtpCurrStartdate.MaxDate = mdtCurrYearStartDate

            dtpCurrEnddate.Value = mdtCurrYearEndDate
            dtpCurrEnddate.MinDate = mdtCurrYearEndDate
            dtpCurrEnddate.MaxDate = mdtCurrYearEndDate

            dtpNewStartdate.Value = mdtNewYearStartDate
            dtpNewStartdate.MinDate = mdtNewYearStartDate
            dtpNewStartdate.MaxDate = mdtNewYearStartDate

            dtpNewEnddate.Value = mdtNewYearEndDate
            dtpNewEnddate.MinDate = mdtNewYearEndDate
            dtpNewEnddate.MaxDate = mdtNewYearEndDate

            'Sohail (04 Mar 2011) -- Start
            dtpPeriodEnddate.Value = mdtNewYearStartDate.AddMonths(1).AddDays(-1)
            dtpPeriodEnddate.MinDate = mdtNewYearStartDate
            dtpPeriodEnddate.MaxDate = mdtNewYearEndDate
            'Sohail (04 Mar 2011) -- End
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetDate", mstrModuleName)
        End Try
    End Sub

    Private Function CreateTransactionDataTable() As DataTable
        Dim dtTable As New DataTable
        Try
            With dtTable
                .Columns.Add(New DataColumn("PeriodID", System.Type.GetType("System.Int32")))
                .Columns.Add(New DataColumn("Period", System.Type.GetType("System.String")))
                .Columns.Add(New DataColumn("EmpID", System.Type.GetType("System.Int32")))
                .Columns.Add(New DataColumn("EmpCode", System.Type.GetType("System.String")))
                .Columns.Add(New DataColumn("EmpName", System.Type.GetType("System.String")))
                .Columns.Add(New DataColumn("HeadID", System.Type.GetType("System.Int32")))
                .Columns.Add(New DataColumn("Head", System.Type.GetType("System.String")))
                .Columns.Add(New DataColumn("HeadTypeID", System.Type.GetType("System.Int32")))
                .Columns.Add(New DataColumn("HeadType", System.Type.GetType("System.String")))
                .Columns.Add(New DataColumn("Balance", System.Type.GetType("System.Decimal"))) 'Sohail (11 May 2011)
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "CreateTransactionDataTable", mstrModuleName)
        End Try
        Return dtTable
    End Function

    'Private Sub CreateLeaveDataTable()
    '    mdtLeave = New DataTable
    '    Try
    '        With mdtLeave
    '            .Columns.Add(New DataColumn("LeaveBalanceID", System.Type.GetType("System.Int32")))
    '            .Columns.Add(New DataColumn("EmpID", System.Type.GetType("System.Int32")))
    '            .Columns.Add(New DataColumn("EmpCode", System.Type.GetType("System.String")))
    '            .Columns.Add(New DataColumn("EmpName", System.Type.GetType("System.String")))
    '            .Columns.Add(New DataColumn("Balance", System.Type.GetType("System.Decimal")))
    '        End With
    '    Catch ex As Exception
    '        DisplayError.Show("-1", ex.Message, "CreateTransactionDataTable", mstrModuleName)
    '    End Try
    'End Sub

    'Sohail (03 Nov 2010) -- Start
    'Changes : Loan, Advance, Savings on seperate list view.
    'Public Sub FillTransactionList()
    '    Dim objTnALeave As New clsTnALeaveTran
    '    Dim objLoanAdvance As New clsLoan_Advance
    '    Dim objPendigLoan As New clsProcess_pending_loan
    '    Dim objSavingTran As New clsSaving_Tran
    '    Dim dsList As DataSet
    '    Dim mdtTransaction As DataTable
    '    Dim dtRow As DataRow
    '    Dim lvItem As ListViewItem
    '    Dim sTime As DateTime = Now
    '    Try

    '        lvTransaction.Items.Clear()
    '        mstrPayroll = ""
    '        mstrProcessPendingLoan = ""
    '        mstrLoan = ""
    '        mstrSavings = ""

    '        mdtTransaction = CreateTransactionDataTable()

    '        '*** Get list of unpaid salary ***
    '        dsList = objTnALeave.Get_Balance_List("Balance")
    '        mdtPayroll = New DataView(dsList.Tables("Balance"), "", "", DataViewRowState.CurrentRows).ToTable
    '        For Each dsRow As DataRow In mdtPayroll.Rows
    '            dtRow = mdtTransaction.NewRow

    '            dtRow.Item("PeriodID") = CInt(dsRow.Item("payperiodunkid").ToString)
    '            dtRow.Item("Period") = dsRow.Item("PeriodName").ToString
    '            dtRow.Item("EmpID") = CInt(dsRow.Item("employeeunkid").ToString)
    '            dtRow.Item("EmpCode") = dsRow.Item("employeecode").ToString
    '            dtRow.Item("EmpName") = dsRow.Item("employeename").ToString
    '            dtRow.Item("HeadID") = CInt(dsRow.Item("tnaleavetranunkid").ToString)
    '            dtRow.Item("Head") = "Salary - " & dsRow.Item("PeriodName").ToString
    '            dtRow.Item("HeadTypeID") = 1
    '            dtRow.Item("HeadType") = "Salary"
    '            dtRow.Item("Balance") = cdec(dsRow.Item("balanceamount").ToString)
    '            mdtTransaction.Rows.Add(dtRow)

    '            If mstrPayroll = "" Then
    '                mstrPayroll = dsRow.Item("tnaleavetranunkid").ToString
    '            Else
    '                mstrPayroll &= ", " & dsRow.Item("tnaleavetranunkid").ToString
    '            End If
    '        Next

    '        '*** Get list of process pending loan ***
    '        dsList = objPendigLoan.GetList("PendingLoan", 1) '1=Pending
    '        mdtProcessPendingLoan = New DataView(dsList.Tables("PendingLoan"), "", "", DataViewRowState.CurrentRows).ToTable
    '        For Each dsRow As DataRow In mdtProcessPendingLoan.Rows
    '            dtRow = mdtTransaction.NewRow

    '            dtRow.Item("PeriodID") = 0 'No period field in process pending table
    '            dtRow.Item("Period") = "Process Pending"
    '            dtRow.Item("EmpID") = CInt(dsRow.Item("employeeunkid").ToString)
    '            dtRow.Item("EmpCode") = dsRow.Item("employeeunkid").ToString 'No employeecode field in Getlist
    '            dtRow.Item("EmpName") = dsRow.Item("EmpName").ToString
    '            dtRow.Item("HeadID") = CInt(dsRow.Item("processpendingloanunkid").ToString)
    '            dtRow.Item("Head") = dsRow.Item("LoanScheme").ToString
    '            If CBool(dsRow.Item("isloan").ToString) = True Then
    '                dtRow.Item("HeadTypeID") = 2
    '                dtRow.Item("HeadType") = "Loan - Pending"
    '            Else
    '                dtRow.Item("HeadTypeID") = 3
    '                dtRow.Item("HeadType") = "Advance - Pending"
    '            End If
    '            dtRow.Item("Balance") = cdec(dsRow.Item("Amount").ToString)
    '            mdtTransaction.Rows.Add(dtRow)

    '            If mstrProcessPendingLoan = "" Then
    '                mstrProcessPendingLoan = dsRow.Item("processpendingloanunkid").ToString
    '            Else
    '                mstrProcessPendingLoan &= ", " & dsRow.Item("processpendingloanunkid").ToString
    '            End If
    '        Next

    '        '*** Get list of loan receivable
    '        dsList = objLoanAdvance.GetList("Loan")
    '        mdtLoan = New DataView(dsList.Tables("Loan"), "loan_statusunkid <> 4 AND balance_amount > 0", "", DataViewRowState.CurrentRows).ToTable '4=Completed
    '        For Each dsRow As DataRow In mdtLoan.Rows

    '            dtRow = mdtTransaction.NewRow

    '            dtRow.Item("PeriodID") = CInt(dsRow.Item("periodunkid").ToString)
    '            dtRow.Item("Period") = dsRow.Item("PeriodName").ToString
    '            dtRow.Item("EmpID") = CInt(dsRow.Item("employeeunkid").ToString)
    '            dtRow.Item("EmpCode") = dsRow.Item("employeeunkid").ToString 'No employeecode field in Getlist
    '            dtRow.Item("EmpName") = dsRow.Item("EmpName").ToString
    '            dtRow.Item("HeadID") = CInt(dsRow.Item("loanadvancetranunkid").ToString)
    '            dtRow.Item("Head") = dsRow.Item("LoanScheme").ToString
    '            If CBool(dsRow.Item("isloan").ToString) = True Then
    '                dtRow.Item("HeadTypeID") = 4
    '                dtRow.Item("HeadType") = "Loan - Receivable"
    '            Else
    '                dtRow.Item("HeadTypeID") = 5
    '                dtRow.Item("HeadType") = "Advance - Receivable"
    '            End If
    '            dtRow.Item("Balance") = cdec(dsRow.Item("balance_amount").ToString)
    '            mdtTransaction.Rows.Add(dtRow)

    '            If mstrLoan = "" Then
    '                mstrLoan = dsRow.Item("loanadvancetranunkid").ToString
    '            Else
    '                mstrLoan &= ", " & dsRow.Item("loanadvancetranunkid").ToString
    '            End If
    '        Next

    '        '*** Get list of loan advance payable
    '        dsList = objLoanAdvance.Get_Unpaid_LoanAdvance_List("LoanPayable")
    '        For Each dsRow As DataRow In dsList.Tables("LoanPayable").Rows
    '            dtRow = mdtTransaction.NewRow

    '            dtRow.Item("PeriodID") = CInt(dsRow.Item("periodunkid").ToString)
    '            dtRow.Item("Period") = dsRow.Item("PeriodName").ToString
    '            dtRow.Item("EmpID") = CInt(dsRow.Item("employeeunkid").ToString)
    '            dtRow.Item("EmpCode") = dsRow.Item("employeecode").ToString
    '            dtRow.Item("EmpName") = dsRow.Item("EmpName").ToString
    '            dtRow.Item("HeadID") = CInt(dsRow.Item("loanadvancetranunkid").ToString)
    '            dtRow.Item("Head") = dsRow.Item("LoanScheme").ToString
    '            If CBool(dsRow.Item("isloan").ToString) = True Then
    '                dtRow.Item("HeadTypeID") = 6
    '                dtRow.Item("HeadType") = "Loan - Payable"
    '            Else
    '                dtRow.Item("HeadTypeID") = 7
    '                dtRow.Item("HeadType") = "Advance - Payable"
    '            End If
    '            dtRow.Item("Balance") = cdec(dsRow.Item("UnpaidLoan").ToString)
    '            mdtTransaction.Rows.Add(dtRow)

    '            'Checking in mdtLoan whether loanadvancetranunkid is already exist or not
    '            Dim drTemp As DataRow() = mdtLoan.Select("loanadvancetranunkid = " & CInt(dsRow.Item("loanadvancetranunkid").ToString) & "")
    '            If drTemp.Length = 0 Then
    '                Dim dRow As DataRow = mdtLoan.NewRow

    '                dRow.Item("VocNo") = dsRow.Item("voucherno").ToString
    '                dRow.Item("EmpName") = dsRow.Item("EmpName").ToString
    '                dRow.Item("LoanScheme") = dsRow.Item("LoanScheme").ToString
    '                dRow.Item("Loan_Advance") = dsRow.Item("Loan_Advance").ToString
    '                dRow.Item("Loan_Advanceunkid") = CInt(dsRow.Item("Loan_Advanceunkid").ToString)
    '                dRow.Item("Amount") = cdec(dsRow.Item("Amount").ToString)
    '                dRow.Item("PeriodName") = dsRow.Item("PeriodName").ToString
    '                dRow.Item("loan_statusunkid") = CInt(dsRow.Item("loan_statusunkid").ToString)
    '                dRow.Item("periodunkid") = CInt(dsRow.Item("periodunkid").ToString)
    '                dRow.Item("loanadvancetranunkid") = CInt(dsRow.Item("loanadvancetranunkid").ToString)
    '                dRow.Item("employeeunkid") = CInt(dsRow.Item("employeeunkid").ToString)
    '                dRow.Item("loanschemeunkid") = CInt(dsRow.Item("loanschemeunkid").ToString)
    '                dRow.Item("isloan") = CBool(dsRow.Item("isloan").ToString)
    '                dRow.Item("effective_date") = eZeeDate.convertDate(dsRow.Item("effective_date").ToString)
    '                dRow.Item("balance_amount") = cdec(dsRow.Item("balance_amount").ToString)
    '                dRow.Item("emi_amount") = cdec(dsRow.Item("emi_amount").ToString)
    '                dRow.Item("advance_amount") = cdec(dsRow.Item("advance_amount").ToString)
    '                dRow.Item("processpendingloanunkid") = CInt(dsRow.Item("processpendingloanunkid").ToString)
    '                mdtLoan.Rows.Add(dRow)

    '                If mstrLoan = "" Then
    '                    mstrLoan = dsRow.Item("loanadvancetranunkid").ToString
    '                Else
    '                    mstrLoan &= ", " & dsRow.Item("loanadvancetranunkid").ToString
    '                End If
    '            End If
    '        Next

    '        '*** Get list of unpaid Saving scheme
    '        dsList = objSavingTran.GetList("Savings")
    '        'Sohail (03 Nov 2010) -- Start
    '        'mdtSavings = New DataView(dsList.Tables("Savings"), "savingstatus <> 4", "", DataViewRowState.CurrentRows).ToTable '4=Completed
    '        mdtSavings = New DataView(dsList.Tables("Savings"), "savingstatus <> 4 or (savingstatus = 4 and balance_amount > 0)", "", DataViewRowState.CurrentRows).ToTable '4=Completed
    '        'Sohail (03 Nov 2010) -- End
    '        For Each dsRow As DataRow In mdtSavings.Rows
    '            dtRow = mdtTransaction.NewRow

    '            dtRow.Item("PeriodID") = CInt(dsRow.Item("payperiodunkid").ToString)
    '            dtRow.Item("Period") = dsRow.Item("period").ToString
    '            dtRow.Item("EmpID") = CInt(dsRow.Item("employeeunkid").ToString)
    '            dtRow.Item("EmpCode") = dsRow.Item("employeeunkid").ToString 'No employeecode field in Getlist
    '            dtRow.Item("EmpName") = dsRow.Item("employeename").ToString
    '            dtRow.Item("HeadID") = CInt(dsRow.Item("savingtranunkid").ToString)
    '            dtRow.Item("Head") = dsRow.Item("savingscheme").ToString
    '            dtRow.Item("HeadTypeID") = 8
    '            dtRow.Item("HeadType") = "Savings - Payable"
    '            dtRow.Item("Balance") = cdec(dsRow.Item("balance_amount").ToString)
    '            mdtTransaction.Rows.Add(dtRow)

    '            If mstrSavings = "" Then
    '                mstrSavings = dsRow.Item("savingtranunkid").ToString
    '            Else
    '                mstrSavings &= ", " & dsRow.Item("savingtranunkid").ToString
    '            End If
    '        Next

    '        '*** Fill Listview from Data Table ***
    '        Dim dtView As DataView = New DataView(mdtTransaction)
    '        dtView.Sort = "PeriodID ASC, EmpName ASC, HeadTypeID ASC"
    '        mdtTransaction = dtView.ToTable
    '        Dim lvArray As New List(Of ListViewItem) 'Sohail (03 Nov 2010)

    '        For Each dRow As DataRow In mdtTransaction.Rows
    '            lvItem = New ListViewItem

    '            lvItem.Text = dRow.Item("Period").ToString
    '            lvItem.Tag = dRow.Item("PeriodID").ToString

    '            lvItem.SubItems.Add(dRow.Item("EmpCode").ToString)
    '            lvItem.SubItems(colhEmployeeCode.Index).Tag = dRow.Item("EmpID").ToString

    '            lvItem.SubItems.Add(dRow.Item("EmpName").ToString)

    '            lvItem.SubItems.Add(dRow.Item("Head").ToString)
    '            lvItem.SubItems(colhHead.Index).Tag = dRow.Item("HeadID").ToString 'Sohail (03 Nov 2010)

    '            lvItem.SubItems.Add(dRow.Item("HeadType").ToString)
    '            lvItem.SubItems(colhHeadType.Index).Tag = dRow.Item("HeadTypeID").ToString 'Sohail (03 Nov 2010)

    '            lvItem.SubItems.Add(Format(dRow.Item("Balance"), "#0.00"))

    '            'Sohail (03 Nov 2010) -- Start
    '            'lvTransaction.Items.Add(lvItem)
    '            lvArray.Add(lvItem)
    '            'Sohail (03 Nov 2010) -- End
    '        Next
    '        lvTransaction.Items.AddRange(lvArray.ToArray) 'Sohail (03 Nov 2010)
    '        lvTransaction.EndUpdate() 'Sohail (03 Nov 2010)
    '        lvTransaction.GroupingColumn = colhPeriod
    '        lvTransaction.DisplayGroups(True)

    '        If lvTransaction.Items.Count > 7 Then
    '            colhBalance.Width = 160 - 18
    '        Else
    '            colhBalance.Width = 160
    '        End If

    '        Me.Text = Now.Subtract(sTime).TotalSeconds.ToString & " Seconds : "
    '    Catch ex As Exception
    '        DisplayError.Show("-1", ex.Message, "FillTranHeadList", mstrModuleName)
    '    Finally
    '        objTnALeave = Nothing
    '        objLoanAdvance = Nothing
    '        objPendigLoan = Nothing
    '        objSavingTran = Nothing
    '        dsList = Nothing
    '    End Try
    'End Sub
    Public Sub FillTransactionList()
        Dim objTnALeave As New clsTnALeaveTran
        Dim dsList As DataSet
        Dim lvItem As ListViewItem
        'Sohail (04 Mar 2011) -- Start
        Dim dtTable As DataTable
        Dim mintPeriodUnkId As Integer = 0
        'Sohail (04 Mar 2011) -- End

        Try
            lvTransaction.Items.Clear()
            Dim lvArray As New List(Of ListViewItem)
            lvTransaction.BeginUpdate()
            mstrPayroll = ""

            'Sohail (04 Mar 2011) -- Start
            ''*** Get list of unpaid salary ***
            'dsList = objTnALeave.Get_Balance_List("Balance")
            'mdtPayroll = New DataView(dsList.Tables("Balance"), "", "payperiodunkid ASC, employeename ASC", DataViewRowState.CurrentRows).ToTable
            '*** Carry Forward closing balance to new period
            Dim objPeriod As New clscommom_period_Tran
            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'dsList = objPeriod.GetList("Period", enModuleReference.Payroll, True, enStatusType.Close)
            dsList = objPeriod.GetList("Period", enModuleReference.Payroll, FinancialYear._Object._YearUnkid, FinancialYear._Object._Database_Start_Date, True, enStatusType.Close)
            'Sohail (21 Aug 2015) -- End
            dtTable = New DataView(dsList.Tables("Period"), "end_date = '" & eZeeDate.convertDate(FinancialYear._Object._Database_End_Date) & "'", "", DataViewRowState.CurrentRows).ToTable
            If dtTable.Rows.Count > 0 Then
                mintPeriodUnkId = CInt(dtTable.Rows(0).Item("periodunkid").ToString)
                'Sohail (09 Nov 2013) -- Start
                'TRA - ENHANCEMENT
                mintLastPeriodUnkID = mintPeriodUnkId
                mdtLastPeriodStartDate = eZeeDate.convertDate(dtTable.Rows(0).Item("start_date").ToString)
                mdtLastPeriodEndDate = eZeeDate.convertDate(dtTable.Rows(0).Item("end_date").ToString)
                mdtLastPeriodTnAEndDate = eZeeDate.convertDate(dtTable.Rows(0).Item("tna_enddate").ToString) 'Sohail (07 Jan 2014)
            Else
                mintLastPeriodUnkID = 0
                mdtLastPeriodStartDate = Nothing
                mdtLastPeriodEndDate = Nothing
                'Sohail (09 Nov 2013) -- End
                mdtLastPeriodTnAEndDate = Nothing 'Sohail (07 Jan 2014)
            End If

            'Sohail (12 Jan 2015) -- Start
            'Issue - Not all loans were carry forwarding due to UserAccessFilter if close year is done user who do not have all Dept/Class previledge.
            'dsList = objTnALeave.GetList("OldBalance", , , , , , , , , , , , , mintPeriodUnkId)
            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'dsList = objTnALeave.GetList("OldBalance", , , , , , , , , , , , , mintPeriodUnkId, , , , , " AND 1 = 1 ")
            dsList = objTnALeave.GetList(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, mdtLastPeriodStartDate, mdtLastPeriodEndDate, ConfigParameter._Object._UserAccessModeSetting, True, True, False, "OldBalance", , mintPeriodUnkId, , , )
            'Sohail (21 Aug 2015) -- End
            'Sohail (12 Jan 2015) -- End
            mdtPayroll = New DataView(dsList.Tables("OldBalance"), "balanceamount <> 0", "", DataViewRowState.CurrentRows).ToTable
            'Sohail (04 Mar 2011) -- End
            For Each dtRow As DataRow In mdtPayroll.Rows
                lvItem = New ListViewItem

                lvItem.Text = dtRow.Item("PeriodName").ToString
                lvItem.Tag = CInt(dtRow.Item("payperiodunkid").ToString)

                lvItem.SubItems.Add(dtRow.Item("employeecode").ToString)
                lvItem.SubItems(colhEmployeeCode.Index).Tag = CInt(dtRow.Item("employeeunkid").ToString)
                lvItem.SubItems.Add(dtRow.Item("employeename").ToString)

                lvItem.SubItems.Add("Salary - " & dtRow.Item("PeriodName").ToString)
                lvItem.SubItems(colhHead.Index).Tag = CInt(dtRow.Item("tnaleavetranunkid").ToString)

                lvItem.SubItems.Add("Salary")
                lvItem.SubItems(colhHeadType.Index).Tag = 1

                lvItem.SubItems.Add(Format(dtRow.Item("balanceamount"), GUI.fmtCurrency)) 'Sohail (01 Dec 2010)
                lvItem.SubItems(colhBalance.Index).Tag = CDec(dtRow.Item("balanceamount").ToString) 'Sohail (11 May 2011)

                lvArray.Add(lvItem)

                If mstrPayroll = "" Then
                    mstrPayroll = dtRow.Item("tnaleavetranunkid").ToString
                Else
                    mstrPayroll &= "," & dtRow.Item("tnaleavetranunkid").ToString
                End If
            Next

            lvTransaction.Items.AddRange(lvArray.ToArray)
            lvTransaction.EndUpdate()
            lvTransaction.GroupingColumn = colhPeriod
            lvTransaction.DisplayGroups(True)

            If lvTransaction.Items.Count > 7 Then
                colhBalance.Width = 160 - 18
            Else
                colhBalance.Width = 160
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillTransactionList", mstrModuleName)
        Finally
            objTnALeave = Nothing
            dsList = Nothing
        End Try
    End Sub

    Private Sub FillLoanSavings()
        Dim objLoanAdvance As New clsLoan_Advance
        Dim objPendigLoan As New clsProcess_pending_loan
        Dim objSavingTran As New clsSaving_Tran
        Dim dsList As DataSet
        Dim mdtTransaction As DataTable
        Dim dtRow As DataRow
        Dim lvItem As ListViewItem
        Try
            lvLoanSavings.Items.Clear()
            Dim lvArray As New List(Of ListViewItem)
            lvLoanSavings.BeginUpdate()
            mstrProcessPendingLoan = ""
            mstrLoan = ""
            mstrSavings = ""

            mdtTransaction = CreateTransactionDataTable()

            '*** Get list of process pending loan ***
            'Sohail (12 Jan 2015) -- Start
            'Issue - Not all loans were carry forwarding due to UserAccessFilter if close year is done user who do not have all Dept/Class previledge.
            'dsList = objPendigLoan.GetList("PendingLoan", 1) '1=Pending

            'Nilay (10-Oct-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'dsList = objPendigLoan.GetList("PendingLoan", 1, , , " AND 1 = 1 ") '1=Pending
            'Sohail (06 Jan 2016) -- Start
            'Enhancement - Show Close Year Process Logs on Close Year Wizard.
            'dsList = objPendigLoan.GetList(FinancialYear._Object._DatabaseName, _
            '                               User._Object._Userunkid, _
            '                               FinancialYear._Object._YearUnkid, _
            '                               Company._Object._Companyunkid, _
            '                               eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
            '                               eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
            '                               ConfigParameter._Object._UserAccessModeSetting, _
            '                               True, _
            '                               ConfigParameter._Object._IsIncludeInactiveEmp, _
            '                               "PendingLoan", 1, " AND 1 = 1 ") '1=Pending
            dsList = objPendigLoan.GetList(FinancialYear._Object._DatabaseName, _
                                          User._Object._Userunkid, _
                                          FinancialYear._Object._YearUnkid, _
                                          Company._Object._Companyunkid, _
                                          eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                          eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                          ConfigParameter._Object._UserAccessModeSetting, _
                                          True, _
                                          ConfigParameter._Object._IsIncludeInactiveEmp, _
                                          "PendingLoan", 1, , False) '1=Pending
            'Sohail (06 Jan 2016) -- End
            'Nilay (10-Oct-2015) -- End


            'Sohail (12 Jan 2015) -- End
            mdtProcessPendingLoan = New DataView(dsList.Tables("PendingLoan")).ToTable
            For Each dsRow As DataRow In mdtProcessPendingLoan.Rows
                dtRow = mdtTransaction.NewRow

                dtRow.Item("PeriodID") = 0 'No period field in process pending table
                dtRow.Item("Period") = "Process Pending Loan/Advance"
                dtRow.Item("EmpID") = CInt(dsRow.Item("employeeunkid").ToString)
                dtRow.Item("EmpCode") = dsRow.Item("employeeunkid").ToString 'No employeecode field in Getlist
                dtRow.Item("EmpName") = dsRow.Item("EmpName").ToString
                dtRow.Item("HeadID") = CInt(dsRow.Item("processpendingloanunkid").ToString)
                dtRow.Item("Head") = dsRow.Item("LoanScheme").ToString
                If CBool(dsRow.Item("isloan").ToString) = True Then
                    dtRow.Item("HeadTypeID") = 1
                    dtRow.Item("HeadType") = "Loan - Pending"
                Else
                    dtRow.Item("HeadTypeID") = 2
                    dtRow.Item("HeadType") = "Advance - Pending"
                End If
                dtRow.Item("Balance") = CDec(dsRow.Item("Amount").ToString) 'Sohail (11 May 2011)
                mdtTransaction.Rows.Add(dtRow)

                If mstrProcessPendingLoan = "" Then
                    mstrProcessPendingLoan = dsRow.Item("processpendingloanunkid").ToString
                Else
                    mstrProcessPendingLoan &= "," & dsRow.Item("processpendingloanunkid").ToString
                End If
            Next


            '*** Get list of processed and approved but Unassigned loan/advance
            'Sohail (12 Jan 2015) -- Start
            'Issue - Not all loans were carry forwarding due to UserAccessFilter if close year is done user who do not have all Dept/Class previledge.
            'dsList = objPendigLoan.Get_UnAssigned_ProcessPending_List("UnAssigneLoan")

            'Nilay (10-Oct-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'dsList = objPendigLoan.Get_UnAssigned_ProcessPending_List("UnAssigneLoan", " AND 1 = 1 ")
            'Sohail (06 Jan 2016) -- Start
            'Enhancement - Show Close Year Process Logs on Close Year Wizard.
            dsList = objPendigLoan.Get_UnAssigned_ProcessPending_List(FinancialYear._Object._DatabaseName, _
                                                                      User._Object._Userunkid, _
                                                                      FinancialYear._Object._YearUnkid, _
                                                                      Company._Object._Companyunkid, _
                                                                      eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                                      eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                                      ConfigParameter._Object._UserAccessModeSetting, _
                                                                      True, _
                                                                      ConfigParameter._Object._IsIncludeInactiveEmp, _
                                                                      "UnAssigneLoan", , False)
            'Sohail (06 Jan 2016) -- End
            'Nilay (10-Oct-2015) -- End

            'Sohail (12 Jan 2015) -- End
            For Each dsRow As DataRow In dsList.Tables("UnAssigneLoan").Rows
                dtRow = mdtTransaction.NewRow

                dtRow.Item("PeriodID") = 0 'No period field in process pending table
                dtRow.Item("Period") = "Unassigned Loan/Advance"
                dtRow.Item("EmpID") = CInt(dsRow.Item("employeeunkid").ToString)
                dtRow.Item("EmpCode") = dsRow.Item("employeeunkid").ToString 'No employeecode field in Getlist
                dtRow.Item("EmpName") = dsRow.Item("EmpName").ToString
                dtRow.Item("HeadID") = CInt(dsRow.Item("processpendingloanunkid").ToString)
                dtRow.Item("Head") = dsRow.Item("LoanScheme").ToString
                If CBool(dsRow.Item("isloan").ToString) = True Then
                    dtRow.Item("HeadTypeID") = 3
                    dtRow.Item("HeadType") = "Loan - Unassigned"
                Else
                    dtRow.Item("HeadTypeID") = 4
                    dtRow.Item("HeadType") = "Advance - Unassigned"
                End If
                dtRow.Item("Balance") = CDec(dsRow.Item("approved_amount").ToString) 'Sohail (11 May 2011)
                mdtTransaction.Rows.Add(dtRow)

                If mstrProcessPendingLoan = "" Then
                    mstrProcessPendingLoan = dsRow.Item("processpendingloanunkid").ToString
                Else
                    mstrProcessPendingLoan &= ", " & dsRow.Item("processpendingloanunkid").ToString
                End If
            Next

            '*** Get list of loan receivable
            'Sohail (12 Jan 2015) -- Start
            'Issue - Not all loans were carry forwarding due to UserAccessFilter if close year is done user who do not have all Dept/Class previledge.
            'dsList = objLoanAdvance.GetList("Loan")

            'Nilay (10-Oct-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'dsList = objLoanAdvance.GetList("Loan", , , , , , " AND 1 = 1 ")
            'Sohail (06 Jan 2016) -- Start
            'Enhancement - Show Close Year Process Logs on Close Year Wizard.
            'dsList = objLoanAdvance.GetList(FinancialYear._Object._DatabaseName, _
            '                                User._Object._Userunkid, _
            '                                FinancialYear._Object._YearUnkid, _
            '                                Company._Object._Companyunkid, _
            '                                ConfigParameter._Object._IsIncludeInactiveEmp, _
            '                                eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
            '                                eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
            '                                eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
            '                                ConfigParameter._Object._UserAccessModeSetting, _
            '                                "Loan", " AND 1 = 1 ")
            dsList = objLoanAdvance.GetList(FinancialYear._Object._DatabaseName, _
                                            User._Object._Userunkid, _
                                            FinancialYear._Object._YearUnkid, _
                                            Company._Object._Companyunkid, _
                                            ConfigParameter._Object._IsIncludeInactiveEmp, _
                                            eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                            eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                            eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                            ConfigParameter._Object._UserAccessModeSetting, _
                                            "Loan", , , False)
            'Sohail (06 Jan 2016) -- End
            'Nilay (10-Oct-2015) -- End


            'Sohail (12 Jan 2015) -- End
            mdtLoan = New DataView(dsList.Tables("Loan"), "loan_statusunkid <> 4 AND balance_amount > 0", "", DataViewRowState.CurrentRows).ToTable '4=Completed
            For Each dsRow As DataRow In mdtLoan.Rows

                dtRow = mdtTransaction.NewRow

                dtRow.Item("PeriodID") = CInt(dsRow.Item("periodunkid").ToString)
                dtRow.Item("Period") = dsRow.Item("PeriodName").ToString
                dtRow.Item("EmpID") = CInt(dsRow.Item("employeeunkid").ToString)
                dtRow.Item("EmpCode") = dsRow.Item("employeeunkid").ToString 'No employeecode field in Getlist
                dtRow.Item("EmpName") = dsRow.Item("EmpName").ToString
                dtRow.Item("HeadID") = CInt(dsRow.Item("loanadvancetranunkid").ToString)
                dtRow.Item("Head") = dsRow.Item("LoanScheme").ToString
                If CBool(dsRow.Item("isloan").ToString) = True Then
                    dtRow.Item("HeadTypeID") = 5
                    dtRow.Item("HeadType") = "Loan - Receivable"
                Else
                    dtRow.Item("HeadTypeID") = 6
                    dtRow.Item("HeadType") = "Advance - Receivable"
                End If
                dtRow.Item("Balance") = CDec(dsRow.Item("balance_amount").ToString) 'Sohail (11 May 2011)
                mdtTransaction.Rows.Add(dtRow)

                If mstrLoan = "" Then
                    mstrLoan = dsRow.Item("loanadvancetranunkid").ToString
                Else
                    mstrLoan &= ", " & dsRow.Item("loanadvancetranunkid").ToString
                End If
            Next

            '*** Get list of loan advance payable
            'Sohail (12 Jan 2015) -- Start
            'Issue - Not all loans were carry forwarding due to UserAccessFilter if close year is done user who do not have all Dept/Class previledge.
            'dsList = objLoanAdvance.Get_Unpaid_LoanAdvance_List("LoanPayable")
            'Sohail (15 Dec 2015) -- Start
            'Enhancement - New Loan Calculation type Reducing balance with Fixed Pricipal EMI and new interest calculation Monthly apart from Daily for KBC.
            'dsList = objLoanAdvance.Get_Unpaid_LoanAdvance_List("LoanPayable", " AND 1 = 1 ")
            dsList = objLoanAdvance.Get_Unpaid_LoanAdvance_List(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), ConfigParameter._Object._UserAccessModeSetting, True, True, False, "LoanPayable")
            'Sohail (15 Dec 2015) -- End
            'Sohail (12 Jan 2015) -- End
            For Each dsRow As DataRow In dsList.Tables("LoanPayable").Rows
                dtRow = mdtTransaction.NewRow

                dtRow.Item("PeriodID") = CInt(dsRow.Item("periodunkid").ToString)
                dtRow.Item("Period") = dsRow.Item("PeriodName").ToString
                dtRow.Item("EmpID") = CInt(dsRow.Item("employeeunkid").ToString)
                dtRow.Item("EmpCode") = dsRow.Item("employeecode").ToString
                dtRow.Item("EmpName") = dsRow.Item("EmpName").ToString
                dtRow.Item("HeadID") = CInt(dsRow.Item("loanadvancetranunkid").ToString)
                dtRow.Item("Head") = dsRow.Item("LoanScheme").ToString
                If CBool(dsRow.Item("isloan").ToString) = True Then
                    dtRow.Item("HeadTypeID") = 7
                    dtRow.Item("HeadType") = "Loan - Payable"
                Else
                    dtRow.Item("HeadTypeID") = 8
                    dtRow.Item("HeadType") = "Advance - Payable"
                End If
                dtRow.Item("Balance") = CDec(dsRow.Item("UnpaidLoan").ToString) 'Sohail (11 May 2011)
                mdtTransaction.Rows.Add(dtRow)

                'Checking in mdtLoan whether loanadvancetranunkid is already exist or not
                Dim drTemp As DataRow() = mdtLoan.Select("loanadvancetranunkid = " & CInt(dsRow.Item("loanadvancetranunkid").ToString) & "")
                If drTemp.Length = 0 Then
                    Dim dRow As DataRow = mdtLoan.NewRow

                    dRow.Item("VocNo") = dsRow.Item("voucherno").ToString
                    dRow.Item("EmpName") = dsRow.Item("EmpName").ToString
                    dRow.Item("LoanScheme") = dsRow.Item("LoanScheme").ToString
                    dRow.Item("Loan_Advance") = dsRow.Item("Loan_Advance").ToString
                    dRow.Item("Loan_Advanceunkid") = CInt(dsRow.Item("Loan_Advanceunkid").ToString)
                    dRow.Item("Amount") = CDec(dsRow.Item("Amount").ToString) 'Sohail (11 May 2011)
                    dRow.Item("PeriodName") = dsRow.Item("PeriodName").ToString
                    dRow.Item("loan_statusunkid") = CInt(dsRow.Item("loan_statusunkid").ToString)
                    dRow.Item("periodunkid") = CInt(dsRow.Item("periodunkid").ToString)
                    dRow.Item("loanadvancetranunkid") = CInt(dsRow.Item("loanadvancetranunkid").ToString)
                    dRow.Item("employeeunkid") = CInt(dsRow.Item("employeeunkid").ToString)
                    dRow.Item("loanschemeunkid") = CInt(dsRow.Item("loanschemeunkid").ToString)
                    dRow.Item("isloan") = CBool(dsRow.Item("isloan").ToString)
                    dRow.Item("effective_date") = eZeeDate.convertDate(dsRow.Item("effective_date").ToString)
                    dRow.Item("balance_amount") = CDec(dsRow.Item("balance_amount").ToString) 'Sohail (11 May 2011)
                    dRow.Item("emi_amount") = CDec(dsRow.Item("emi_amount").ToString) 'Sohail (11 May 2011)
                    dRow.Item("advance_amount") = CDec(dsRow.Item("advance_amount").ToString) 'Sohail (11 May 2011)
                    dRow.Item("processpendingloanunkid") = CInt(dsRow.Item("processpendingloanunkid").ToString)
                    mdtLoan.Rows.Add(dRow)

                    If mstrLoan = "" Then
                        mstrLoan = dsRow.Item("loanadvancetranunkid").ToString
                    Else
                        mstrLoan &= ", " & dsRow.Item("loanadvancetranunkid").ToString
                    End If
                End If
            Next

            '*** Get list of unpaid Saving scheme
            'Sohail (12 Jan 2015) -- Start
            'Issue - Not all loans were carry forwarding due to UserAccessFilter if close year is done user who do not have all Dept/Class previledge.
            'dsList = objSavingTran.GetList("Savings")

            'Shani(24-Aug-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'Change Saving GetList method for get active Employee saving
            'dsList = objSavingTran.GetList("Savings", , , , , , , " AND 1 = 1 ")
            'Sohail (06 Jan 2016) -- Start
            'Enhancement - Show Close Year Process Logs on Close Year Wizard.
            'dsList = objSavingTran.GetList(FinancialYear._Object._DatabaseName, _
            '                               User._Object._Userunkid, _
            '                               FinancialYear._Object._YearUnkid, _
            '                               Company._Object._Companyunkid, _
            '                               eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
            '                               eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
            '                               ConfigParameter._Object._UserAccessModeSetting, _
            '                               ConfigParameter._Object._IsIncludeInactiveEmp, _
            '                               "Savings")
            dsList = objSavingTran.GetList(FinancialYear._Object._DatabaseName, _
                                          User._Object._Userunkid, _
                                          FinancialYear._Object._YearUnkid, _
                                          Company._Object._Companyunkid, _
                                          eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                          eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                          ConfigParameter._Object._UserAccessModeSetting, _
                                          ConfigParameter._Object._IsIncludeInactiveEmp, _
                                          "Savings", , , , , , , , False)
            'Sohail (06 Jan 2016) -- End
            'Shani(24-Aug-2015) -- End

            'Sohail (12 Jan 2015) -- End
            'Sohail (03 Nov 2010) -- Start
            'mdtSavings = New DataView(dsList.Tables("Savings"), "savingstatus <> 4", "", DataViewRowState.CurrentRows).ToTable '4=Completed
            mdtSavings = New DataView(dsList.Tables("Savings"), "savingstatus <> 4 or (savingstatus = 4 and balance_amount > 0)", "", DataViewRowState.CurrentRows).ToTable '4=Completed
            'Sohail (03 Nov 2010) -- End
            For Each dsRow As DataRow In mdtSavings.Rows
                dtRow = mdtTransaction.NewRow

                dtRow.Item("PeriodID") = CInt(dsRow.Item("payperiodunkid").ToString)
                dtRow.Item("Period") = dsRow.Item("period").ToString
                dtRow.Item("EmpID") = CInt(dsRow.Item("employeeunkid").ToString)
                dtRow.Item("EmpCode") = dsRow.Item("employeeunkid").ToString 'No employeecode field in Getlist
                dtRow.Item("EmpName") = dsRow.Item("employeename").ToString
                dtRow.Item("HeadID") = CInt(dsRow.Item("savingtranunkid").ToString)
                dtRow.Item("Head") = dsRow.Item("savingscheme").ToString
                dtRow.Item("HeadTypeID") = 9
                dtRow.Item("HeadType") = "Savings - Payable"
                dtRow.Item("Balance") = CDec(dsRow.Item("balance_amount").ToString) 'Sohail (11 May 2011)
                mdtTransaction.Rows.Add(dtRow)

                If mstrSavings = "" Then
                    mstrSavings = dsRow.Item("savingtranunkid").ToString
                Else
                    mstrSavings &= ", " & dsRow.Item("savingtranunkid").ToString
                End If
            Next

            '*** Fill Listview from Data Table ***
            Dim dtView As DataView = New DataView(mdtTransaction)
            dtView.Sort = "PeriodID ASC, EmpName ASC, HeadType ASC"
            mdtTransaction = dtView.ToTable

            For Each dRow As DataRow In mdtTransaction.Rows
                lvItem = New ListViewItem

                lvItem.Text = dRow.Item("Period").ToString
                lvItem.Tag = dRow.Item("PeriodID").ToString

                lvItem.SubItems.Add(dRow.Item("EmpCode").ToString)
                lvItem.SubItems(colhLoanEmpCode.Index).Tag = dRow.Item("EmpID").ToString

                lvItem.SubItems.Add(dRow.Item("EmpName").ToString)

                lvItem.SubItems.Add(dRow.Item("Head").ToString)
                lvItem.SubItems(colhLoanHead.Index).Tag = dRow.Item("HeadID").ToString

                lvItem.SubItems.Add(dRow.Item("HeadType").ToString)
                lvItem.SubItems(colhLoanHeadType.Index).Tag = dRow.Item("HeadTypeID").ToString

                lvItem.SubItems.Add(Format(dRow.Item("Balance"), GUI.fmtCurrency)) 'Sohail (01 Dec 2010)
                lvItem.SubItems(colhLoanBalance.Index).Tag = CDec(dRow.Item("Balance").ToString) 'Sohail (11 May 2011)

                lvArray.Add(lvItem)
            Next
            lvLoanSavings.Items.AddRange(lvArray.ToArray)
            lvLoanSavings.EndUpdate()
            lvLoanSavings.GroupingColumn = colhLoanPeriod
            lvLoanSavings.DisplayGroups(True)

            If lvLoanSavings.Items.Count > 7 Then
                colhLoanBalance.Width = 160 - 18
            Else
                colhLoanBalance.Width = 160
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillLoanSavings", mstrModuleName)
        Finally
            objLoanAdvance = Nothing
            objPendigLoan = Nothing
            objSavingTran = Nothing
            dsList = Nothing
        End Try
    End Sub

    Public Sub FillApplicantList()
        Dim objApplicantMaster As New clsApplicant_master
        Dim dsList As DataSet
        Dim lvItem As ListViewItem
        'Dim dtTable As DataTable

        Try
            lvApplicants.Items.Clear()
            Dim lvArray As New List(Of ListViewItem)
            lvApplicants.BeginUpdate()
            mstrApplicants = ""

            '*** Get list of Final Applicants who aren't imported to employee master ***
            dsList = objApplicantMaster.GetList("Applicant")
            mdtApplicant = New DataView(dsList.Tables("Applicant"), "ISNULL(isimport,0) = 0", "applicantunkid ASC", DataViewRowState.CurrentRows).ToTable
            For Each dtRow As DataRow In mdtApplicant.Rows
                lvItem = New ListViewItem

                lvItem.Text = dtRow.Item("applicant_code").ToString
                lvItem.Tag = CInt(dtRow.Item("applicantunkid").ToString)

                lvItem.SubItems.Add(dtRow.Item("applicantname").ToString)

                'lvArray.Add(lvItem) 'Anjan/sohail (06 July 2015) commented as TRA was getting out of memory expection error.
                lvApplicants.Items.Add(lvItem)

                If mstrApplicants = "" Then
                    mstrApplicants = dtRow.Item("applicantunkid").ToString
                Else
                    mstrApplicants &= "," & dtRow.Item("applicantunkid").ToString
                End If

            Next
            'lvApplicants.Items.AddRange(lvArray.ToArray) 'Anjan/sohail (06 July 2015) commented as TRA was getting out of memory expection error.
            lvApplicants.EndUpdate()

            If lvApplicants.Items.Count > 20 Then
                colhApplicantName.Width = 440 - 18
            Else
                colhApplicantName.Width = 440
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillApplicantList", mstrModuleName)
        Finally
            objApplicantMaster = Nothing
            dsList = Nothing
        End Try
    End Sub
    'Sohail (03 Nov 2010) -- End

    Private Sub FillLeaveList()
        Dim objLeaveBalance As New clsleavebalance_tran
        Dim dsList As DataSet = Nothing
        Dim lvItem As ListViewItem
        Try

            lvLeave.Items.Clear()
            'Sohail (03 Nov 2010) -- Start
            Dim lvArray As New List(Of ListViewItem)
            lvLeave.BeginUpdate()
            'Sohail (03 Nov 2010) -- End
            mstrLeave = ""
            'Call CreateLeaveDataTable()



            'Pinkal (24-May-2013) -- Start
            'Enhancement : TRA Changes

            mstrLeaveTypeIDs = ""

            'dsList = objLeaveBalance.GetList("Leave")

            'Pinkal (02-Jan-2015) -- Start
            'Enhancement - CHANGES FOR GET ONLY ACTIVE EMPLYEES BALANCES.

            'If ConfigParameter._Object._LeaveBalanceSetting = enLeaveBalanceSetting.Financial_Year Then
            '    dsList = objLeaveBalance.GetList("Leave", True)
            'ElseIf ConfigParameter._Object._LeaveBalanceSetting = enLeaveBalanceSetting.ELC Then
            '    dsList = objLeaveBalance.GetList("Leave", True, False, -1, True, True)
            'End If


            'Pinkal (24-Aug-2015) -- Start
            'Enhancement - WORKING ON ACTIVE EMPLOYEE CONDITION.

            'If ConfigParameter._Object._LeaveBalanceSetting = enLeaveBalanceSetting.Financial_Year Then
            '    dsList = objLeaveBalance.GetList("Leave", True, , , , , , , eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate))
            'ElseIf ConfigParameter._Object._LeaveBalanceSetting = enLeaveBalanceSetting.ELC Then
            '    dsList = objLeaveBalance.GetList("Leave", True, False, -1, True, True, , , eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate))
            'End If

            If ConfigParameter._Object._LeaveBalanceSetting = enLeaveBalanceSetting.Financial_Year Then
                dsList = objLeaveBalance.GetList("Leave", FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid _
                                                                 , Company._Object._Companyunkid, ConfigParameter._Object._EmployeeAsOnDate, ConfigParameter._Object._UserAccessModeSetting _
                                                                 , True, ConfigParameter._Object._IsIncludeInactiveEmp, True, True, False, -1, False, False, False, "", Nothing)
            ElseIf ConfigParameter._Object._LeaveBalanceSetting = enLeaveBalanceSetting.ELC Then
                dsList = objLeaveBalance.GetList("Leave", FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid _
                                                                , Company._Object._Companyunkid, ConfigParameter._Object._EmployeeAsOnDate, ConfigParameter._Object._UserAccessModeSetting _
                                                                , True, ConfigParameter._Object._IsIncludeInactiveEmp, True, True, False, -1, True, True, False, "", Nothing)
            End If

            'Pinkal (24-Aug-2015) -- End

            'Pinkal (02-Jan-2015) -- End

            'Pinkal (24-May-2013) -- End




            'Pinkal (06-May-2013) -- Start
            'Enhancement : TRA Changes
            'mdtLeave = New DataView(dsList.Tables("Leave"), "ispaid = 1", "employeeunkid", DataViewRowState.CurrentRows).ToTable

            'Pinkal (24-May-2013) -- Start
            'Enhancement : TRA Changes
            'mdtLeave = New DataView(dsList.Tables("Leave"), "ispaid = 1 AND isshortleave = 0", "employeeunkid", DataViewRowState.CurrentRows).ToTable
            mdtLeave = New DataView(dsList.Tables("Leave"), "ispaid = 1 AND isshortleave = 0", "leavetypeunkid,employeeunkid", DataViewRowState.CurrentRows).ToTable
            'Pinkal (24-May-2013) -- End


            'Pinkal (06-May-2013) -- End


            'Pinkal (24-May-2013) -- Start
            'Enhancement : TRA Changes
            Dim objLeaveType As New clsleavetype_master
            Dim mintLeaveTypeID As Integer = 0
            'Pinkal (24-May-2013) -- End


            For Each dtRow As DataRow In mdtLeave.Rows
                'dtRow = mdtLeave.NewRow

                'dtRow.Item("LeaveBalanceID") = CInt(dsRow.Item("leavebalanceunkid").ToString)
                'dtRow.Item("EmpID") = CInt(dsRow.Item("employeeunkid").ToString)
                'dtRow.Item("EmpCode") = dsRow.Item("employeeunkid").ToString 'No employeecode field in Getlist
                'dtRow.Item("EmpName") = dsRow.Item("EmployeeName").ToString
                'dtRow.Item("Balance") = cdec(dsRow.Item("balance").ToString)
                'mdtTransaction.Rows.Add(dtRow)

                lvItem = New ListViewItem
                lvItem.Text = dtRow.Item("leavebalanceunkid").ToString
                lvItem.Tag = CInt(dtRow.Item("leavebalanceunkid").ToString)
                'Sohail (03 Nov 2010) -- Start
                lvItem.SubItems.Add(dtRow.Item("LeaveName").ToString)
                lvItem.SubItems(colhLeaveTypeName.Index).Tag = CInt(dtRow.Item("leavetypeunkid").ToString)
                'Sohail (03 Nov 2010) -- End
                lvItem.SubItems.Add(dtRow.Item("EmployeeName").ToString)
                lvItem.SubItems(colhEmployee.Index).Tag = CInt(dtRow.Item("employeeunkid").ToString)


                'Pinkal (18-Jun-2013) -- Start
                'Enhancement : TRA Changes
                If ConfigParameter._Object._LeaveBalanceSetting = enLeaveBalanceSetting.Financial_Year Then
                    lvItem.SubItems.Add(Format(CDec(dtRow.Item("accrue_amount")) - CDec(dtRow.Item("issue_amount")), GUI.fmtCurrency)) 'Sohail (01 Dec 2010)
                ElseIf ConfigParameter._Object._LeaveBalanceSetting = enLeaveBalanceSetting.ELC Then
                lvItem.SubItems.Add(Format(dtRow.Item("remaining_bal"), GUI.fmtCurrency)) 'Sohail (01 Dec 2010)
                End If
                'Pinkal (18-Jun-2013) -- End




                'Pinkal (24-May-2013) -- Start
                'Enhancement : TRA Changes

                'S.SANDEEP [ 04 DEC 2013 ] -- START
                'objLeaveType._Leavetypeunkid = CInt(dtRow.Item("leavetypeunkid").ToString)
                'If objLeaveType._IsNoAction = False Then
                '    lvItem.SubItems.Add(Format(objLeaveType._CFAmount, GUI.fmtCurrency))
                'Else

                '    'Pinkal (18-Jun-2013) -- Start
                '    'Enhancement : TRA Changes
                '    If ConfigParameter._Object._LeaveBalanceSetting = enLeaveBalanceSetting.Financial_Year Then
                '        lvItem.SubItems.Add(Format(CDec(dtRow.Item("accrue_amount")) - CDec(dtRow.Item("issue_amount")), GUI.fmtCurrency)) 'Sohail (01 Dec 2010)
                '    ElseIf ConfigParameter._Object._LeaveBalanceSetting = enLeaveBalanceSetting.ELC Then
                '        lvItem.SubItems.Add(Format(dtRow.Item("remaining_bal"), GUI.fmtCurrency))
                '    End If
                '    'Pinkal (18-Jun-2013) -- End
                'End If
                If CBool(dtRow.Item("isnoaction")) = False Then
                    'Pinkal (03-Jan-2014) -- Start
                    'Enhancement : Oman Changes
                    'lvItem.SubItems.Add(Format(dtRow.Item("cfamount"), GUI.fmtCurrency))

                    'Pinkal (02-Jan-2015) -- Start
                    'Enhancement - CHANGES FOR GET ONLY ACTIVE EMPLYEES BALANCES.

                    'If CDec(dtRow.Item("cfamount")) <= CDec(dtRow.Item("remaining_bal")) Then
                    '    lvItem.SubItems.Add(Format(dtRow.Item("cfamount"), GUI.fmtCurrency))
                    'ElseIf CDec(dtRow.Item("cfamount")) >= CDec(dtRow.Item("remaining_bal")) Then
                    '    lvItem.SubItems.Add(Format(dtRow.Item("remaining_bal"), GUI.fmtCurrency))
                    'End If
                    If CDec(dtRow.Item("cfamount")) = 0 Then
                        lvItem.SubItems.Add(Format(dtRow.Item("cfamount"), GUI.fmtCurrency))
                    ElseIf CDec(dtRow.Item("cfamount")) <= CDec(dtRow.Item("remaining_bal")) Then
                    lvItem.SubItems.Add(Format(dtRow.Item("cfamount"), GUI.fmtCurrency))
                    ElseIf CDec(dtRow.Item("cfamount")) >= CDec(dtRow.Item("remaining_bal")) Then
                        lvItem.SubItems.Add(Format(dtRow.Item("remaining_bal"), GUI.fmtCurrency))
                    End If

                    'Pinkal (02-Jan-2015) -- End

                    'Pinkal (03-Jan-2014) -- End



                Else
                    If ConfigParameter._Object._LeaveBalanceSetting = enLeaveBalanceSetting.Financial_Year Then
                        lvItem.SubItems.Add(Format(CDec(dtRow.Item("accrue_amount")) - CDec(dtRow.Item("issue_amount")), GUI.fmtCurrency))
                    ElseIf ConfigParameter._Object._LeaveBalanceSetting = enLeaveBalanceSetting.ELC Then
                    lvItem.SubItems.Add(Format(dtRow.Item("remaining_bal"), GUI.fmtCurrency))
                End If
                End If
                'S.SANDEEP [ 04 DEC 2013 ] -- END




                'Pinkal (24-May-2013) -- End


                'Sohail (03 Nov 2010) -- Start
                'lvLeave.Items.Add(lvItem)
                lvArray.Add(lvItem)
                'Sohail (03 Nov 2010) -- End

                If mstrLeave = "" Then
                    mstrLeave = dtRow.Item("leavebalanceunkid").ToString
                Else
                    mstrLeave &= ", " & dtRow.Item("leavebalanceunkid").ToString
                End If


                'Pinkal (24-May-2013) -- Start
                'Enhancement : TRA Changes

                If mintLeaveTypeID <> CInt(dtRow.Item("leavetypeunkid")) Then
                    If mstrLeaveTypeIDs = "" Then
                        mstrLeaveTypeIDs = dtRow.Item("leavetypeunkid").ToString
                    Else
                        mstrLeaveTypeIDs &= ", " & dtRow.Item("leavetypeunkid").ToString
                    End If
                    mintLeaveTypeID = CInt(dtRow.Item("leavetypeunkid"))
                End If
                'Pinkal (24-May-2013) -- End


            Next
            'Sohail (03 Nov 2010) -- Start
            lvLeave.Items.AddRange(lvArray.ToArray)
            lvLeave.EndUpdate()
            lvLeave.GroupingColumn = colhLeaveTypeName
            lvLeave.DisplayGroups(True)


            'Pinkal (24-May-2013) -- Start
            'Enhancement : TRA Changes

            'If lvLeave.Items.Count > 7 Then
            '    colhLeaveBalance.Width = 160 - 18
            'Else
            '    colhLeaveBalance.Width = 160
            'End If

            If lvLeave.Items.Count > 7 Then
                colhCFLeaveAmount.Width = 100 - 18
            Else
                colhCFLeaveAmount.Width = 160
            End If

            'Pinkal (24-May-2013) -- End

            'Sohail (03 Nov 2010) -- End
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillLeaveList", mstrModuleName)
        Finally
            objLeaveBalance = Nothing
        End Try
    End Sub

#End Region

#Region " Private Function "
    
    'Sohail (24 Jun 2013) -- Start
    'TRA - ENHANCEMENT - Create View and Users for TRA sharepoint.
    'Private Function CreateUserViewSynonymsForSharePointTRAoracle() As Boolean
    '    Dim objDataOperation As clsDataOperation
    '    Dim objMaster As New clsMasterData
    '    Dim dsList As DataSet
    '    Dim strQ As String = ""
    '    Dim mstrCurrYearDatabaseName As String = ""
    '    Dim mstrNewYearDatabaseName As String = ""
    '    Dim exForce As Exception
    '    Try

    '        objDataOperation = New clsDataOperation

    '        mstrCurrYearDatabaseName = FinancialYear._Object._DatabaseName
    '        dsList = objMaster.Get_Database_Year_List("List", False, Company._Object._Companyunkid)

    '        If dsList.Tables("List").Rows.Count > 0 Then
    '            mstrNewYearDatabaseName = dsList.Tables("List").Rows(0).Item("database_name").ToString
    '        Else
    '            Return True
    '            Exit Function
    '        End If

    '        strQ = "SELECT TABLE_NAME FROM INFORMATION_SCHEMA.VIEWS WHERE TABLE_NAME = 'vwempdetail_oracle' "

    '        If objDataOperation.RecordCount(strQ) > 0 AndAlso objDataOperation.RecordCount("SELECT * FROM sys.database_principals WHERE type_desc = 'DATABASE_ROLE' AND name = 'oracleview'") > 0 Then
    '            strQ = "exec sp_helprolemember  'oracleview' "

    '            Dim ds As DataSet = objDataOperation.ExecQuery(strQ, "UserList")
    '            Dim users As List(Of String) = (From p In ds.Tables(0).AsEnumerable() Select (p.Item("MemberName").ToString)).ToList

    '            If users.ToList.Count > 0 Then

    '                Dim strSelectFields As String
    '                strSelectFields = "(employeeunkid " & _
    '                                  ", employeecode " & _
    '                                  ", titleunkid " & _
    '                                  ", firstname " & _
    '                                  ", surname " & _
    '                                  ", othername " & _
    '                                  ", appointeddate " & _
    '                                  ", gender " & _
    '                                  ", gendertype " & _
    '                                  ", employmenttypeunkid " & _
    '                                  ", employmenttype " & _
    '                                  ", paytypeunkid " & _
    '                                  ", paytype " & _
    '                                  ", paypointunkid " & _
    '                                  ", paypointname " & _
    '                                  ", loginname " & _
    '                                  ", password " & _
    '                                  ", email " & _
    '                                  ", displayname " & _
    '                                  ", shiftunkid " & _
    '                                  ", shiftname " & _
    '                                  ", birthdate " & _
    '                                  ", birth_ward " & _
    '                                  ", birthcertificateno " & _
    '                                  ", birthstateunkid " & _
    '                                  ", birthstatename " & _
    '                                  ", birthcountryunkid " & _
    '                                  ", birthcountryname " & _
    '                                  ", birthcityunkid " & _
    '                                  ", birthcityname " & _
    '                                  ", birth_village " & _
    '                                  ", work_permit_no " & _
    '                                  ", workcountryunkid " & _
    '                                  ", workcountryname " & _
    '                                  ", work_permit_issue_place " & _
    '                                  ", work_permit_issue_date " & _
    '                                  ", work_permit_expiry_date " & _
    '                                  ", complexionunkid " & _
    '                                  ", complexionname " & _
    '                                  ", bloodgroupunkid " & _
    '                                  ", bloodgroupname " & _
    '                                  ", eyecolorunkid " & _
    '                                  ", eyecolorname " & _
    '                                  ", nationalityunkid " & _
    '                                  ", nationalityname " & _
    '                                  ", ethnicityunkid " & _
    '                                  ", ethnicityname " & _
    '                                  ", religionunkid " & _
    '                                  ", religionname " & _
    '                                  ", hairunkid " & _
    '                                  ", hairname " & _
    '                                  ", language1unkid " & _
    '                                  ", language1name " & _
    '                                  ", language2unkid " & _
    '                                  ", language2name " & _
    '                                  ", language3unkid " & _
    '                                  ", language3name " & _
    '                                  ", language4unkid " & _
    '                                  ", language4name " & _
    '                                  ", extra_tel_no " & _
    '                                  ", height " & _
    '                                  ", weight " & _
    '                                  ", maritalstatusunkid " & _
    '                                  ", maritalstatusname " & _
    '                                  ", anniversary_date " & _
    '                                  ", sports_hobbies " & _
    '                                  ", present_address1 " & _
    '                                  ", present_address2 " & _
    '                                  ", present_countryunkid " & _
    '                                  ", present_countryname " & _
    '                                  ", present_postcodeunkid " & _
    '                                  ", present_postcodename " & _
    '                                  ", present_stateunkid " & _
    '                                  ", present_statename " & _
    '                                  ", present_provicnce " & _
    '                                  ", present_post_townunkid " & _
    '                                  ", present_post_townname " & _
    '                                  ", present_road " & _
    '                                  ", present_estate " & _
    '                                  ", present_plotNo " & _
    '                                  ", present_mobile " & _
    '                                  ", present_alternateno " & _
    '                                  ", present_tel_no " & _
    '                                  ", present_fax " & _
    '                                  ", present_email " & _
    '                                  ", domicile_address1 " & _
    '                                  ", domicile_address2 " & _
    '                                  ", domicile_countryunkid " & _
    '                                  ", domicile_countryname " & _
    '                                  ", domicile_postcodeunkid " & _
    '                                  ", domicile_postcodename " & _
    '                                  ", domicile_stateunkid " & _
    '                                  ", domicile_statename " & _
    '                                  ", domicile_provicnce " & _
    '                                  ", domicile_post_townunkid " & _
    '                                  ", domicile_post_townname " & _
    '                                  ", domicile_road " & _
    '                                  ", domicile_estate " & _
    '                                  ", domicile_plotno " & _
    '                                  ", domicile_mobile " & _
    '                                  ", domicile_alternateno " & _
    '                                  ", domicile_tel_no " & _
    '                                  ", domicile_fax " & _
    '                                  ", domicile_email " & _
    '                                  ", emer_con_firstname " & _
    '                                  ", emer_con_lastname " & _
    '                                  ", emer_con_address " & _
    '                                  ", emer_con_countryunkid " & _
    '                                  ", emer_con_countryname " & _
    '                                  ", emer_con_postcodeunkid " & _
    '                                  ", emer_con_postcodename " & _
    '                                  ", emer_con_state " & _
    '                                  ", emer_con_statename " & _
    '                                  ", emer_con_provicnce " & _
    '                                  ", emer_con_post_townunkid " & _
    '                                  ", emer_con_post_townname " & _
    '                                  ", emer_con_road " & _
    '                                  ", emer_con_estate " & _
    '                                  ", emer_con_plotno " & _
    '                                  ", emer_con_mobile " & _
    '                                  ", emer_con_alternateno " & _
    '                                  ", emer_con_tel_no " & _
    '                                  ", emer_con_fax " & _
    '                                  ", emer_con_email " & _
    '                                  ", stationunkid " & _
    '                                  ", stationname " & _
    '                                  ", deptgroupunkid " & _
    '                                  ", deptgroupname " & _
    '                                  ", departmentunkid " & _
    '                                  ", departmentname " & _
    '                                  ", sectionunkid " & _
    '                                  ", sectionname " & _
    '                                  ", unitunkid " & _
    '                                  ", unitname " & _
    '                                  ", jobgroupunkid " & _
    '                                  ", jobgroupname " & _
    '                                  ", jobunkid " & _
    '                                  ", jobname " & _
    '                                  ", gradegroupunkid " & _
    '                                  ", gradegroupname " & _
    '                                  ", gradeunkid " & _
    '                                  ", gradename " & _
    '                                  ", gradelevelunkid " & _
    '                                  ", gradelevelname " & _
    '                                  ", accessunkid " & _
    '                                  ", classgroupunkid " & _
    '                                  ", classgroupname " & _
    '                                  ", classunkid " & _
    '                                  ", classname " & _
    '                                  ", serviceunkid " & _
    '                                  ", costcenterunkid " & _
    '                                  ", costcentername " & _
    '                                  ", tranhedunkid " & _
    '                                  ", tranhedname " & _
    '                                  ", actionreasonunkid " & _
    '                                  ", actionreasonname " & _
    '                                  ", suspended_from_date " & _
    '                                  ", suspended_to_date " & _
    '                                  ", probation_from_date " & _
    '                                  ", probation_to_date " & _
    '                                  ", termination_from_date " & _
    '                                  ", termination_to_date " & _
    '                                  ", remark " & _
    '                                  ", emer_con_firstname2 " & _
    '                                  ", emer_con_lastname2 " & _
    '                                  ", emer_con_address2 " & _
    '                                  ", emer_con_countryunkid2 " & _
    '                                  ", emer_con_countryname2 " & _
    '                                  ", emer_con_postcodeunkid2 " & _
    '                                  ", emer_con_postcodename2 " & _
    '                                  ", emer_con_state2 " & _
    '                                  ", emer_con_statename2 " & _
    '                                  ", emer_con_provicnce2 " & _
    '                                  ", emer_con_post_townunkid2 " & _
    '                                  ", emer_con_post_townname2 " & _
    '                                  ", emer_con_road2 " & _
    '                                  ", emer_con_estate2 " & _
    '                                  ", emer_con_plotno2 " & _
    '                                  ", emer_con_mobile2 " & _
    '                                  ", emer_con_alternateno2 " & _
    '                                  ", emer_con_tel_no2 " & _
    '                                  ", emer_con_fax2 " & _
    '                                  ", emer_con_email2 " & _
    '                                  ", emer_con_firstname3 " & _
    '                                  ", emer_con_lastname3 " & _
    '                                  ", emer_con_address3 " & _
    '                                  ", emer_con_countryunkid3 " & _
    '                                  ", emer_con_countryname3 " & _
    '                                  ", emer_con_postcodeunkid3 " & _
    '                                  ", emer_con_postcodename3 " & _
    '                                  ", emer_con_state3 " & _
    '                                  ", emer_con_statename3 " & _
    '                                  ", emer_con_provicnce3 " & _
    '                                  ", emer_con_post_townunkid3 " & _
    '                                  ", emer_con_post_townname3 " & _
    '                                  ", emer_con_road3 " & _
    '                                  ", emer_con_estate3 " & _
    '                                  ", emer_con_plotno3 " & _
    '                                  ", emer_con_mobile3 " & _
    '                                  ", emer_con_alternateno3 " & _
    '                                  ", emer_con_tel_no3 " & _
    '                                  ", emer_con_fax3 " & _
    '                                  ", emer_con_email3 " & _
    '                                  ", sectiongroupunkid " & _
    '                                  ", sectiongroupname " & _
    '                                  ", unitgroupunkid " & _
    '                                  ", unitgroupname " & _
    '                                  ", teamunkid " & _
    '                                  ", teamname " & _
    '                                  ", isclear " & _
    '                                  ", confirmation_date " & _
    '                                  ", empl_enddate " & _
    '                                  ", allocationreason " & _
    '                                  ", reinstatement_date " & _
    '                                  ", isexclude_payroll " & _
    '                                  ", isapproved " & _
    '                                  ", activestatus ) "

    '                Dim strCompanyCode As String = Company._Object._Code
    '                For iCnt As Integer = 0 To 1

    '                    If iCnt = 0 Then
    '                        strQ = " USE " & mstrCurrYearDatabaseName

    '                        objDataOperation.ExecNonQuery(strQ)
    '                        If objDataOperation.ErrorMessage <> "" Then
    '                            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
    '                            Throw exForce
    '                        End If

    '                    ElseIf iCnt = 1 Then
    '                        strQ = " USE " & mstrNewYearDatabaseName

    '                        objDataOperation.ExecNonQuery(strQ)
    '                        If objDataOperation.ErrorMessage <> "" Then
    '                            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
    '                            Throw exForce
    '                        End If

    '                        strQ = "IF EXISTS (SELECT TABLE_NAME FROM INFORMATION_SCHEMA.VIEWS WHERE TABLE_NAME = 'vwupdateempdetail_oracle') " & _
    '                           " DROP VIEW vwupdateempdetail_oracle "

    '                        objDataOperation.ExecNonQuery(strQ)
    '                        If objDataOperation.ErrorMessage <> "" Then
    '                            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
    '                            Throw exForce
    '                        End If

    '                        strQ = "EXEC('CREATE VIEW [vwempdetail_oracle] AS " & _
    '                          "SELECT  hremployee_master.employeeunkid " & _
    '                                    ", hremployee_master.employeecode " & _
    '                                    ", hremployee_master.titleunkid " & _
    '                                    ", hremployee_master.firstname " & _
    '                                    ", hremployee_master.surname " & _
    '                                    ", hremployee_master.othername " & _
    '                                    ", hremployee_master.appointeddate " & _
    '                                    ", hremployee_master.gender " & _
    '                                    ", CASE WHEN gender = 1 THEN ''Male'' " & _
    '                                           "WHEN gender = 2 THEN ''Female'' " & _
    '                                           "WHEN gender = 3 THEN ''Other'' " & _
    '                                           "ELSE '''' " & _
    '                                      "END AS gendertype " & _
    '                                    ", hremployee_master.employmenttypeunkid " & _
    '                                    ", ISNULL(emptype.NAME, '''') AS employmenttype " & _
    '                                    ", hremployee_master.paytypeunkid " & _
    '                                    ", ISNULL(ptype.NAME, '''') AS paytype " & _
    '                                    ", hremployee_master.paypointunkid " & _
    '                                    ", ISNULL(prpaypoint_master.paypointname, '''') AS paypointname " & _
    '                                    ", hremployee_master.loginname " & _
    '                                    ", hremployee_master.password " & _
    '                                    ", hremployee_master.email " & _
    '                                    ", hremployee_master.displayname " & _
    '                                    ", hremployee_master.shiftunkid " & _
    '                                    ", ISNULL(tnashift_master.shiftname, '''') AS shiftname " & _
    '                                    ", hremployee_master.birthdate " & _
    '                                    ", hremployee_master.birth_ward " & _
    '                                    ", hremployee_master.birthcertificateno " & _
    '                                    ", hremployee_master.birthstateunkid " & _
    '                                    ", ISNULL(bst.NAME, '''') AS birthstatename " & _
    '                                    ", hremployee_master.birthcountryunkid " & _
    '                                    ", ISNULL(cct.country_name, '''') AS birthcountryname " & _
    '                                    ", hremployee_master.birthcityunkid " & _
    '                                    ", ISNULL(ccty.NAME, '''') AS birthcityname " & _
    '                                    ", hremployee_master.birth_village " & _
    '                                    ", hremployee_master.work_permit_no " & _
    '                                    ", hremployee_master.workcountryunkid " & _
    '                                    ", ISNULL(wct.country_name, '''') AS workcountryname " & _
    '                                    ", hremployee_master.work_permit_issue_place " & _
    '                                    ", hremployee_master.work_permit_issue_date " & _
    '                                    ", hremployee_master.work_permit_expiry_date " & _
    '                                    ", hremployee_master.complexionunkid " & _
    '                                    ", ISNULL(ctype.NAME, '''') AS complexionname " & _
    '                                    ", hremployee_master.bloodgroupunkid " & _
    '                                    ", ISNULL(btype.NAME, '''') AS bloodgroupname " & _
    '                                    ", hremployee_master.eyecolorunkid " & _
    '                                    ", ISNULL(etype.NAME, '''') AS eyecolorname " & _
    '                                    ", hremployee_master.nationalityunkid " & _
    '                                    ", ISNULL(nct.country_name, '''') AS nationalityname " & _
    '                                    ", hremployee_master.ethnicityunkid " & _
    '                                    ", ISNULL(entype.NAME, '''') AS ethnicityname " & _
    '                                    ", hremployee_master.religionunkid " & _
    '                                    ", ISNULL(rtype.NAME, '''') AS religionname " & _
    '                                    ", hremployee_master.hairunkid " & _
    '                                    ", ISNULL(htype.NAME, '''') AS hairname " & _
    '                                    ", hremployee_master.language1unkid " & _
    '                                    ", ISNULL(l1type.NAME, '''') AS language1name " & _
    '                                    ", hremployee_master.language2unkid " & _
    '                                    ", ISNULL(l2type.NAME, '''') AS language2name " & _
    '                                    ", hremployee_master.language3unkid " & _
    '                                    ", ISNULL(l3type.NAME, '''') AS language3name " & _
    '                                    ", hremployee_master.language4unkid " & _
    '                                    ", ISNULL(l4type.NAME, '''') AS language4name " & _
    '                                    ", hremployee_master.extra_tel_no " & _
    '                                    ", hremployee_master.height " & _
    '                                    ", hremployee_master.weight " & _
    '                                    ", hremployee_master.maritalstatusunkid " & _
    '                                    ", ISNULL(mtype.NAME, '''') AS maritalstatusname " & _
    '                                    ", hremployee_master.anniversary_date " & _
    '                                    ", hremployee_master.sports_hobbies " & _
    '                                    ", hremployee_master.present_address1 " & _
    '                                    ", hremployee_master.present_address2 " & _
    '                                    ", hremployee_master.present_countryunkid " & _
    '                                    ", ISNULL(pct.country_name, '''') AS present_countryname " & _
    '                                    ", hremployee_master.present_postcodeunkid " & _
    '                                    ", ISNULL(pz.zipcode_no, '''') AS present_postcodename " & _
    '                                    ", hremployee_master.present_stateunkid " & _
    '                                    ", ISNULL(pst.NAME, '''') AS present_statename " & _
    '                                    ", hremployee_master.present_provicnce " & _
    '                                    ", hremployee_master.present_post_townunkid " & _
    '                                    ", ISNULL(pcy.NAME, '''') AS present_post_townname " & _
    '                                    ", hremployee_master.present_road " & _
    '                                    ", hremployee_master.present_estate " & _
    '                                    ", hremployee_master.present_plotNo " & _
    '                                    ", hremployee_master.present_mobile " & _
    '                                    ", hremployee_master.present_alternateno " & _
    '                                    ", hremployee_master.present_tel_no " & _
    '                                    ", hremployee_master.present_fax " & _
    '                                    ", hremployee_master.present_email " & _
    '                                    ", hremployee_master.domicile_address1 " & _
    '                                    ", hremployee_master.domicile_address2 " & _
    '                                    ", hremployee_master.domicile_countryunkid " & _
    '                                    ", ISNULL(dct.country_name, '''') AS domicile_countryname " & _
    '                                    ", hremployee_master.domicile_postcodeunkid " & _
    '                                    ", ISNULL(dz.zipcode_no, '''') AS domicile_postcodename " & _
    '                                    ", hremployee_master.domicile_stateunkid " & _
    '                                    ", ISNULL(dst.NAME, '''') AS domicile_statename " & _
    '                                    ", hremployee_master.domicile_provicnce " & _
    '                                    ", hremployee_master.domicile_post_townunkid " & _
    '                                    ", ISNULL(dcy.NAME, '''') AS domicile_post_townname " & _
    '                                    ", hremployee_master.domicile_road " & _
    '                                    ", hremployee_master.domicile_estate " & _
    '                                    ", hremployee_master.domicile_plotno " & _
    '                                    ", hremployee_master.domicile_mobile " & _
    '                                    ", hremployee_master.domicile_alternateno " & _
    '                                    ", hremployee_master.domicile_tel_no " & _
    '                                    ", hremployee_master.domicile_fax " & _
    '                                    ", hremployee_master.domicile_email " & _
    '                                    ", hremployee_master.emer_con_firstname " & _
    '                                    ", hremployee_master.emer_con_lastname " & _
    '                                    ", hremployee_master.emer_con_address " & _
    '                                    ", hremployee_master.emer_con_countryunkid " & _
    '                                    ", ISNULL(ect.country_name, '''') AS emer_con_countryname " & _
    '                                    ", hremployee_master.emer_con_postcodeunkid " & _
    '                                    ", ISNULL(ez.zipcode_no, '''') AS emer_con_postcodename " & _
    '                                    ", hremployee_master.emer_con_state " & _
    '                                    ", ISNULL(est.NAME, '''') AS emer_con_statename " & _
    '                                    ", hremployee_master.emer_con_provicnce " & _
    '                                    ", hremployee_master.emer_con_post_townunkid " & _
    '                                    ", ISNULL(ecy.NAME, '''') AS emer_con_post_townname " & _
    '                                    ", hremployee_master.emer_con_road " & _
    '                                    ", hremployee_master.emer_con_estate " & _
    '                                    ", hremployee_master.emer_con_plotno " & _
    '                                    ", hremployee_master.emer_con_mobile " & _
    '                                    ", hremployee_master.emer_con_alternateno " & _
    '                                    ", hremployee_master.emer_con_tel_no " & _
    '                                    ", hremployee_master.emer_con_fax " & _
    '                                    ", hremployee_master.emer_con_email " & _
    '                                    ", hremployee_master.stationunkid " & _
    '                                    ", ISNULL(hrstation_master.name, '''') AS stationname " & _
    '                                    ", hremployee_master.deptgroupunkid " & _
    '                                    ", ISNULL(hrdepartment_group_master.name, '''') AS deptgroupname " & _
    '                                    ", hremployee_master.departmentunkid " & _
    '                                    ", ISNULL(hrdepartment_master.name, '''') AS departmentname " & _
    '                                    ", hremployee_master.sectionunkid " & _
    '                                    ", ISNULL(hrsection_master.name, '''') AS sectionname " & _
    '                                    ", hremployee_master.unitunkid " & _
    '                                    ", ISNULL(hrunit_master.name, '''') AS unitname " & _
    '                                    ", hremployee_master.jobgroupunkid " & _
    '                                    ", ISNULL(hrjobgroup_master.name, '''') AS jobgroupname " & _
    '                                    ", hremployee_master.jobunkid " & _
    '                                    ", ISNULL(hrjob_master.job_name, '''') AS jobname " & _
    '                                    ", hremployee_master.gradegroupunkid " & _
    '                                    ", ISNULL(hrgradegroup_master.name, '''') AS gradegroupname " & _
    '                                    ", hremployee_master.gradeunkid " & _
    '                                    ", ISNULL(hrgrade_master.name, '''') AS gradename " & _
    '                                    ", hremployee_master.gradelevelunkid " & _
    '                                    ", ISNULL(hrgradelevel_master.name, '''') AS gradelevelname " & _
    '                                    ", hremployee_master.accessunkid " & _
    '                                    ", hremployee_master.classgroupunkid " & _
    '                                    ", ISNULL(hrclassgroup_master.name, '''') AS classgroupname " & _
    '                                    ", hremployee_master.classunkid " & _
    '                                    ", ISNULL(hrclasses_master.name, '''') AS classname " & _
    '                                    ", hremployee_master.serviceunkid " & _
    '                                    ", hremployee_master.costcenterunkid " & _
    '                                    ", ISNULL(prcostcenter_master.costcentername, '''') AS costcentername " & _
    '                                    ", hremployee_master.tranhedunkid " & _
    '                                    ", ISNULL(prtranhead_master.trnheadname, '''') AS tranhedname " & _
    '                                    ", hremployee_master.actionreasonunkid " & _
    '                                    ", ISNULL(hraction_reason_master.reason_action, '''') AS actionreasonname " & _
    '                                    ", hremployee_master.suspended_from_date " & _
    '                                    ", hremployee_master.suspended_to_date " & _
    '                                    ", hremployee_master.probation_from_date " & _
    '                                    ", hremployee_master.probation_to_date " & _
    '                                    ", hremployee_master.termination_from_date " & _
    '                                    ", hremployee_master.termination_to_date " & _
    '                                    ", hremployee_master.remark " & _
    '                                    ", hremployee_master.emer_con_firstname2 " & _
    '                                    ", hremployee_master.emer_con_lastname2 " & _
    '                                    ", hremployee_master.emer_con_address2 " & _
    '                                    ", hremployee_master.emer_con_countryunkid2 " & _
    '                                    ", ISNULL(ect2.country_name, '''') AS emer_con_countryname2 " & _
    '                                    ", hremployee_master.emer_con_postcodeunkid2 " & _
    '                                    ", ISNULL(ez2.zipcode_no, '''') AS emer_con_postcodename2 " & _
    '                                    ", hremployee_master.emer_con_state2 " & _
    '                                    ", ISNULL(est2.NAME, '''') AS emer_con_statename2 " & _
    '                                    ", hremployee_master.emer_con_provicnce2 " & _
    '                                    ", hremployee_master.emer_con_post_townunkid2 " & _
    '                                    ", ISNULL(ecy2.NAME, '''') AS emer_con_post_townname2 " & _
    '                                    ", hremployee_master.emer_con_road2 " & _
    '                                    ", hremployee_master.emer_con_estate2 " & _
    '                                    ", hremployee_master.emer_con_plotno2 " & _
    '                                    ", hremployee_master.emer_con_mobile2 " & _
    '                                    ", hremployee_master.emer_con_alternateno2 " & _
    '                                    ", hremployee_master.emer_con_tel_no2 " & _
    '                                    ", hremployee_master.emer_con_fax2 " & _
    '                                    ", hremployee_master.emer_con_email2 " & _
    '                                    ", hremployee_master.emer_con_firstname3 " & _
    '                                    ", hremployee_master.emer_con_lastname3 " & _
    '                                    ", hremployee_master.emer_con_address3 " & _
    '                                    ", hremployee_master.emer_con_countryunkid3 " & _
    '                                    ", ISNULL(ect3.country_name, '''') AS emer_con_countryname3 " & _
    '                                    ", hremployee_master.emer_con_postcodeunkid3 " & _
    '                                    ", ISNULL(ez3.zipcode_no, '''') AS emer_con_postcodename3 " & _
    '                                    ", hremployee_master.emer_con_state3 " & _
    '                                    ", ISNULL(est3.NAME, '''') AS emer_con_statename3 " & _
    '                                    ", hremployee_master.emer_con_provicnce3 " & _
    '                                    ", hremployee_master.emer_con_post_townunkid3 " & _
    '                                    ", ISNULL(ecy3.NAME, '''') AS emer_con_post_townname3 " & _
    '                                    ", hremployee_master.emer_con_road3 " & _
    '                                    ", hremployee_master.emer_con_estate3 " & _
    '                                    ", hremployee_master.emer_con_plotno3 " & _
    '                                    ", hremployee_master.emer_con_mobile3 " & _
    '                                    ", hremployee_master.emer_con_alternateno3 " & _
    '                                    ", hremployee_master.emer_con_tel_no3 " & _
    '                                    ", hremployee_master.emer_con_fax3 " & _
    '                                    ", hremployee_master.emer_con_email3 " & _
    '                                    ", hremployee_master.sectiongroupunkid " & _
    '                                    ", ISNULL(hrsectiongroup_master.name, '''') AS sectiongroupname " & _
    '                                    ", hremployee_master.unitgroupunkid " & _
    '                                    ", ISNULL(hrunitgroup_master.name, '''') AS unitgroupname " & _
    '                                    ", hremployee_master.teamunkid " & _
    '                                    ", ISNULL(hrteam_master.name, '''') AS teamname " & _
    '                                    ", hremployee_master.isclear " & _
    '                                    ", hremployee_master.confirmation_date " & _
    '                                    ", hremployee_master.empl_enddate " & _
    '                                    ", hremployee_master.allocationreason " & _
    '                                    ", hremployee_master.reinstatement_date " & _
    '                                    ", hremployee_master.isexclude_payroll " & _
    '                                    ", hremployee_master.isapproved " & _
    '                                    ", CASE WHEN CONVERT(CHAR(8), appointeddate, 112) <= CONVERT(CHAR(8), GETDATE(), 112) " & _
    '                                                "AND ISNULL(CONVERT(CHAR(8), termination_from_date, 112), " & _
    '                                                           "CONVERT(CHAR(8), GETDATE(), 112)) >= CONVERT(CHAR(8), GETDATE(), 112) " & _
    '                                                "AND ISNULL(CONVERT(CHAR(8), termination_to_date, 112), " & _
    '                                                           "CONVERT(CHAR(8), GETDATE(), 112)) >= CONVERT(CHAR(8), GETDATE(), 112) " & _
    '                                                "AND ISNULL(CONVERT(CHAR(8), empl_enddate, 112), " & _
    '                                                           "CONVERT(CHAR(8), GETDATE(), 112)) >= CONVERT(CHAR(8), GETDATE(), 112) " & _
    '                                           "THEN 1 " & _
    '                                           "ELSE 0 " & _
    '                                      "END AS activestatus " & _
    '                              "FROM    hremployee_master " & _
    '                                      "LEFT JOIN cfcommon_master AS emptype ON emptype.masterunkid = hremployee_master.employmenttypeunkid " & _
    '                                                                              "AND emptype.mastertype = 8 " & _
    '                                      "LEFT JOIN cfcommon_master AS ptype ON ptype.masterunkid = hremployee_master.paytypeunkid " & _
    '                                                                            "AND ptype.mastertype = 17 " & _
    '                                      "LEFT JOIN prpaypoint_master ON prpaypoint_master.paypointunkid = hremployee_master.paypointunkid " & _
    '                                      "LEFT JOIN tnashift_master ON tnashift_master.shiftunkid = hremployee_master.shiftunkid " & _
    '                                      "LEFT JOIN hrmsConfiguration..cfstate_master AS bst ON bst.stateunkid = hremployee_master.birthstateunkid " & _
    '                                      "LEFT JOIN hrmsConfiguration..cfcountry_master AS cct ON cct.countryunkid = hremployee_master.birthcountryunkid " & _
    '                                      "LEFT JOIN hrmsConfiguration..cfcity_master AS ccty ON ccty.cityunkid = hremployee_master.birthcityunkid " & _
    '                                      "LEFT JOIN hrmsConfiguration..cfcountry_master AS wct ON wct.countryunkid = hremployee_master.workcountryunkid " & _
    '                                      "LEFT JOIN cfcommon_master AS ctype ON ctype.masterunkid = hremployee_master.complexionunkid " & _
    '                                                                            "AND ctype.mastertype = 6 " & _
    '                                      "LEFT JOIN cfcommon_master AS btype ON btype.masterunkid = hremployee_master.bloodgroupunkid " & _
    '                                                                            "AND btype.mastertype = 5 " & _
    '                                      "LEFT JOIN cfcommon_master AS etype ON etype.masterunkid = hremployee_master.eyecolorunkid " & _
    '                                                                            "AND etype.mastertype = 10 " & _
    '                                      "LEFT JOIN hrmsConfiguration..cfcountry_master AS nct ON nct.countryunkid = hremployee_master.nationalityunkid " & _
    '                                      "LEFT JOIN cfcommon_master AS entype ON entype.masterunkid = hremployee_master.ethnicityunkid " & _
    '                                                                             "AND entype.mastertype = 9 " & _
    '                                      "LEFT JOIN cfcommon_master AS rtype ON rtype.masterunkid = hremployee_master.religionunkid " & _
    '                                                                            "AND rtype.mastertype = 20 " & _
    '                                      "LEFT JOIN cfcommon_master AS htype ON htype.masterunkid = hremployee_master.hairunkid " & _
    '                                                                            "AND htype.mastertype = 11 " & _
    '                                      "LEFT JOIN cfcommon_master AS l1type ON l1type.masterunkid = hremployee_master.language1unkid " & _
    '                                                                             "AND l1type.mastertype = 14 " & _
    '                                      "LEFT JOIN cfcommon_master AS l2type ON l2type.masterunkid = hremployee_master.language2unkid " & _
    '                                                                             "AND l1type.mastertype = 14 " & _
    '                                      "LEFT JOIN cfcommon_master AS l3type ON l3type.masterunkid = hremployee_master.language3unkid " & _
    '                                                                             "AND l1type.mastertype = 14 " & _
    '                                      "LEFT JOIN cfcommon_master AS l4type ON l4type.masterunkid = hremployee_master.language4unkid " & _
    '                                                                             "AND l1type.mastertype = 14 " & _
    '                                      "LEFT JOIN cfcommon_master AS mtype ON mtype.masterunkid = hremployee_master.maritalstatusunkid " & _
    '                                                                            "AND mtype.mastertype = 15 " & _
    '                                      "LEFT JOIN hrmsConfiguration..cfcountry_master AS pct ON pct.countryunkid = hremployee_master.present_countryunkid " & _
    '                                      "LEFT JOIN hrmsConfiguration..cfzipcode_master AS pz ON pz.zipcodeunkid = hremployee_master.present_postcodeunkid " & _
    '                                      "LEFT JOIN hrmsConfiguration..cfstate_master AS pst ON pst.stateunkid = hremployee_master.present_stateunkid " & _
    '                                      "LEFT JOIN hrmsConfiguration..cfcity_master AS pcy ON pcy.cityunkid = hremployee_master.present_post_townunkid " & _
    '                                      "LEFT JOIN hrmsConfiguration..cfcountry_master AS dct ON dct.countryunkid = hremployee_master.domicile_countryunkid " & _
    '                                      "LEFT JOIN hrmsConfiguration..cfzipcode_master AS dz ON dz.zipcodeunkid = hremployee_master.domicile_postcodeunkid " & _
    '                                      "LEFT JOIN hrmsConfiguration..cfstate_master AS dst ON dst.stateunkid = hremployee_master.domicile_stateunkid " & _
    '                                      "LEFT JOIN hrmsConfiguration..cfcity_master AS dcy ON dcy.cityunkid = hremployee_master.domicile_post_townunkid " & _
    '                                      "LEFT JOIN hrmsConfiguration..cfcountry_master AS ect ON ect.countryunkid = hremployee_master.emer_con_countryunkid " & _
    '                                      "LEFT JOIN hrmsConfiguration..cfzipcode_master AS ez ON ez.zipcodeunkid = hremployee_master.emer_con_postcodeunkid " & _
    '                                      "LEFT JOIN hrmsConfiguration..cfstate_master AS est ON est.stateunkid = hremployee_master.emer_con_state " & _
    '                                      "LEFT JOIN hrmsConfiguration..cfcity_master AS ecy ON ecy.cityunkid = hremployee_master.emer_con_post_townunkid " & _
    '                                      "LEFT JOIN hrmsConfiguration..cfcountry_master AS ect2 ON ect2.countryunkid = hremployee_master.emer_con_countryunkid2 " & _
    '                                      "LEFT JOIN hrmsConfiguration..cfzipcode_master AS ez2 ON ez2.zipcodeunkid = hremployee_master.emer_con_postcodeunkid2 " & _
    '                                      "LEFT JOIN hrmsConfiguration..cfstate_master AS est2 ON est2.stateunkid = hremployee_master.emer_con_state2 " & _
    '                                      "LEFT JOIN hrmsConfiguration..cfcity_master AS ecy2 ON ecy2.cityunkid = hremployee_master.emer_con_post_townunkid2 " & _
    '                                      "LEFT JOIN hrmsConfiguration..cfcountry_master AS ect3 ON ect3.countryunkid = hremployee_master.emer_con_countryunkid3 " & _
    '                                      "LEFT JOIN hrmsConfiguration..cfzipcode_master AS ez3 ON ez3.zipcodeunkid = hremployee_master.emer_con_postcodeunkid3 " & _
    '                                      "LEFT JOIN hrmsConfiguration..cfstate_master AS est3 ON est3.stateunkid = hremployee_master.emer_con_state3 " & _
    '                                      "LEFT JOIN hrmsConfiguration..cfcity_master AS ecy3 ON ecy3.cityunkid = hremployee_master.emer_con_post_townunkid3 " & _
    '                                      "LEFT JOIN hrstation_master ON hrstation_master.stationunkid = hremployee_master.stationunkid " & _
    '                                      "LEFT JOIN hrdepartment_group_master ON hrdepartment_group_master.deptgroupunkid = hremployee_master.deptgroupunkid " & _
    '                                      "LEFT JOIN hrdepartment_master ON hrdepartment_master.departmentunkid = hremployee_master.departmentunkid " & _
    '                                      "LEFT JOIN hrsection_master ON hrsection_master.sectionunkid = hremployee_master.sectionunkid " & _
    '                                      "LEFT JOIN hrunit_master ON hrunit_master.unitunkid = hremployee_master.unitunkid " & _
    '                                      "LEFT JOIN hrjobgroup_master ON hrjobgroup_master.jobgroupunkid = hremployee_master.jobgroupunkid " & _
    '                                      "LEFT JOIN hrjob_master ON hrjob_master.jobunkid = hremployee_master.jobunkid " & _
    '                                      "LEFT JOIN hrgradegroup_master ON hrgradegroup_master.gradegroupunkid = hremployee_master.gradegroupunkid " & _
    '                                      "LEFT JOIN hrgrade_master ON hrgrade_master.gradeunkid = hremployee_master.gradeunkid " & _
    '                                      "LEFT JOIN hrgradelevel_master ON hrgradelevel_master.gradelevelunkid = hremployee_master.gradelevelunkid " & _
    '                                      "LEFT JOIN hrclassgroup_master ON hrclassgroup_master.classgroupunkid = hremployee_master.classgroupunkid " & _
    '                                      "LEFT JOIN hrclasses_master ON hrclasses_master.classesunkid = hremployee_master.classunkid " & _
    '                                      "LEFT JOIN prcostcenter_master ON prcostcenter_master.costcenterunkid = hremployee_master.costcenterunkid " & _
    '                                      "LEFT JOIN prtranhead_master ON prtranhead_master.tranheadunkid = hremployee_master.tranhedunkid " & _
    '                                      "LEFT JOIN hraction_reason_master ON hraction_reason_master.actionreasonunkid = hremployee_master.actionreasonunkid " & _
    '                                      "LEFT JOIN hrteam_master ON hremployee_master.teamunkid = hrteam_master.teamunkid " & _
    '                                      "LEFT JOIN hrunitgroup_master ON hremployee_master.unitgroupunkid = hrunitgroup_master.unitgroupunkid " & _
    '                                      "LEFT JOIN hrsectiongroup_master ON hremployee_master.sectiongroupunkid = hrsectiongroup_master.sectiongroupunkid ') "

    '                        objDataOperation.ExecNonQuery(strQ)
    '                        If objDataOperation.ErrorMessage <> "" Then
    '                            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
    '                            Throw exForce
    '                        End If

    '                        strQ = "IF EXISTS (SELECT TABLE_NAME FROM INFORMATION_SCHEMA.VIEWS WHERE TABLE_NAME = 'vwupdateempdetail_oracle') " & _
    '                                    " DROP VIEW vwupdateempdetail_oracle "

    '                        objDataOperation.ExecNonQuery(strQ)
    '                        If objDataOperation.ErrorMessage <> "" Then
    '                            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
    '                            Throw exForce
    '                        End If


    '                        strQ = "EXEC('CREATE VIEW vwupdateempdetail_oracle " & _
    '                                 "AS " & _
    '                                 "SELECT employeeunkid, password FROM dbo.hremployee_master') "

    '                        objDataOperation.ExecNonQuery(strQ)
    '                        If objDataOperation.ErrorMessage <> "" Then
    '                            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
    '                            Throw exForce
    '                        End If
    '                    End If


    '                    For Each strUserName In users

    '                        strQ = "IF  EXISTS(SELECT * FROM sys.synonyms WHERE name = '" & strCompanyCode & "EmployeeDetail' ) DROP SYNONYM  " & strCompanyCode & "EmployeeDetail " & _
    '                               " BEGIN " & _
    '                                    " CREATE SYNONYM " & strCompanyCode & "EmployeeDetail " & _
    '                            "FOR " & mstrNewYearDatabaseName & "..vwempdetail_oracle " & _
    '                               "End " & _
    '                              "IF  EXISTS(SELECT * FROM sys.synonyms WHERE name = '" & strCompanyCode & "UpdateEmployee' ) DROP SYNONYM  " & strCompanyCode & "UpdateEmployee " & _
    '                               " BEGIN " & _
    '                                " CREATE SYNONYM " & strCompanyCode & "UpdateEmployee " & _
    '                                "FOR " & mstrNewYearDatabaseName & "..vwupdateempdetail_oracle " & _
    '                               "End " & _
    '                              "IF NOT EXISTS(SELECT * FROM sys.database_principals WHERE type_desc = 'DATABASE_ROLE' AND name = 'oracleview') " & _
    '                               "BEGIN " & _
    '                               "CREATE ROLE oracleview  " & _
    '                               "END " & _
    '                               "IF NOT EXISTS(SELECT * FROM sys.database_principals WHERE type_desc = 'DATABASE_ROLE' AND name = 'oracleviewupdate') " & _
    '                               "BEGIN " & _
    '                               "CREATE ROLE oracleviewupdate  " & _
    '                               "END " & _
    '                               "IF EXISTS(SELECT * FROM sys.syslogins WHERE name = '" & strUserName & "') " & _
    '                               "BEGIN " & _
    '                                    "IF NOT EXISTS(SELECT * FROM sys.sysusers WHERE issqluser =1 AND islogin = 1 AND name = '" & strUserName & "') " & _
    '                                    "BEGIN " & _
    '                                        "CREATE USER " & strUserName & " FOR LOGIN " & strUserName & " " & _
    '                                    "END " & _
    '                               "END " & _
    '                                "BEGIN " & _
    '                                   "GRANT SELECT ON " & strCompanyCode & "EmployeeDetail TO oracleview " & _
    '                                   "GRANT SELECT ON vwempdetail_oracle " & strSelectFields & " TO oracleview " & _
    '                                   "GRANT SELECT ON " & strCompanyCode & "UpdateEmployee TO oracleviewupdate " & _
    '                                   "GRANT SELECT ON vwupdateempdetail_oracle (employeeunkid, password) TO oracleviewupdate " & _
    '                                   "GRANT UPDATE ON " & strCompanyCode & "UpdateEmployee TO oracleviewupdate " & _
    '                                   "GRANT UPDATE ON vwupdateempdetail_oracle (password) TO oracleviewupdate " & _
    '                                   "EXEC sp_addrolemember  'oracleview','" & strUserName & "'" & _
    '                                   "EXEC sp_addrolemember  'oracleviewupdate','" & strUserName & "'" & _
    '                               "END "

    '                        objDataOperation.ExecNonQuery(strQ)
    '                        If objDataOperation.ErrorMessage <> "" Then
    '                            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
    '                            Throw exForce
    '                        End If

    '                    Next
    '                Next

    '                eZeeDatabase.change_database(mstrCurrYearDatabaseName)

    '            End If
    '        End If

    '        Return True

    '    Catch ex As Exception
    '        DisplayError.Show("-1", ex.Message, "CreateUserViewSynonymsForSharePointTRA", mstrModuleName)
    '    Finally
    '        exForce = Nothing
    '        objDataOperation = Nothing
    '        objMaster = Nothing
    '    End Try
    'End Function
    Private Function CreateUserViewSynonymsForSharePointTRA() As Boolean
        Dim objDataOperation As clsDataOperation
        Dim objMaster As New clsMasterData
        Dim dsList As DataSet
        Dim strQ As String = ""
        Dim mstrCurrYearDatabaseName As String = ""
        Dim mstrNewYearDatabaseName As String = ""
        Dim exForce As Exception
        Try

            objDataOperation = New clsDataOperation

            mstrCurrYearDatabaseName = FinancialYear._Object._DatabaseName
            dsList = objMaster.Get_Database_Year_List("List", False, Company._Object._Companyunkid)

            If dsList.Tables("List").Rows.Count > 0 Then
                mstrNewYearDatabaseName = dsList.Tables("List").Rows(0).Item("database_name").ToString
            Else
                Return True
                Exit Function
            End If

            strQ = "SELECT TABLE_NAME FROM INFORMATION_SCHEMA.VIEWS WHERE TABLE_NAME = 'vwEmployeeDetail' "

            If objDataOperation.RecordCount(strQ) > 0 AndAlso objDataOperation.RecordCount("SELECT * FROM sys.database_principals WHERE type_desc = 'DATABASE_ROLE' AND name = 'selectview'") > 0 Then
                strQ = "exec sp_helprolemember  'selectview' "

                Dim ds As DataSet = objDataOperation.ExecQuery(strQ, "UserList")
                Dim users As List(Of String) = (From p In ds.Tables(0).AsEnumerable() Select (p.Item("MemberName").ToString)).ToList

                If users.ToList.Count > 0 Then

                    'Anjan [29 June 2016] -- Start
                    'ENHANCEMENT : close year change for linking synonym from new database to old database 
                    Dim dsyear As DataSet = objMaster.Get_Database_Year_List("year", True, Company._Object._YearUnkid)
                    'Anjan [29 June 2016] -- End



                    Dim strCompanyCode As String = Company._Object._Code

                    'Anjan [29 June 2016] -- Start
                    'ENHANCEMENT : close year change for linking synonym from new database to old database 
                    'For iCnt As Integer = 0 To 1
                    Dim dtRow As DataRow = Nothing
                    For iCnt As Integer = 0 To dsyear.Tables("year").Rows.Count
                        If iCnt < dsyear.Tables("year").Rows.Count Then
                            dtRow = dsyear.Tables("year").Rows(iCnt)
                        End If
                     'Anjan [29 June 2016] -- End                       


                    'Anjan [29 June 2016] -- Start
                    'ENHANCEMENT : close year change for linking synonym from new database to old database 
                        'If iCnt = 0 Then

                        '    strQ = " USE " & mstrCurrYearDatabaseName

                        '    objDataOperation.ExecNonQuery(strQ)
                        '    If objDataOperation.ErrorMessage <> "" Then
                        '        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        '        Throw exForce
                        '    End If

                        'ElseIf iCnt = 1 Then
                        '    strQ = " USE " & mstrNewYearDatabaseName

                        '    objDataOperation.ExecNonQuery(strQ)
                        '    If objDataOperation.ErrorMessage <> "" Then
                        '        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        '        Throw exForce
                        '    End If

                        '    strQ = "IF EXISTS (SELECT TABLE_NAME FROM INFORMATION_SCHEMA.VIEWS WHERE TABLE_NAME = 'vwEmployeeDetail') " & _
                        '             " DROP VIEW vwEmployeeDetail "

                        '    objDataOperation.ExecNonQuery(strQ)
                        '    If objDataOperation.ErrorMessage <> "" Then
                        '        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        '        Throw exForce
                        '    End If

                        '    strQ = "EXEC('CREATE VIEW [vwEmployeeDetail] AS " & _
                        '                  "SELECT  employeecode as code " & _
                        '                         ",ISNULL(firstname,'''') +'' ''+ ISNULL(othername,'''') +'' ''+ISNULL(surname,'''') AS employeename " & _
                        '                         ",ISNULL(hrgradegroup_master.name,'''') AS gradegroup " & _
                        '                                 ",ISNULL(hrgrade_master.name,'''') AS grade " & _
                        '                                 ",ISNULL(hrgradelevel_master.name,'''') AS gradelevel " & _
                        '                         ",ISNULL(hrdepartment_master.name,'''') as department " & _
                        '                         ",ISNULL(hrstation_master.name,'''') as station " & _
                        '                         ",ISNULL(hrjob_master.job_name,'''') as designation " & _
                        '                         ",ISNULL(hrclassgroup_master.name,'''') as region " & _
                        '                  "FROM hremployee_master " & _
                        '                  "LEFT JOIN hrgradegroup_master ON hremployee_master.gradegroupunkid = hrgradegroup_master.gradegroupunkid " & _
                        '                        "LEFT JOIN hrgrade_master ON hrgrade_master.gradeunkid=hremployee_master.gradeunkid " & _
                        '                        "LEFT JOIN hrgradelevel_master ON hremployee_master.gradelevelunkid = hrgradelevel_master.gradelevelunkid " & _
                        '                  "LEFT JOIN hrdepartment_master ON hrdepartment_master.departmentunkid=hremployee_master.departmentunkid " & _
                        '                  "LEFT JOIN hrstation_master ON hrstation_master.stationunkid=hremployee_master.stationunkid " & _
                        '                  "LEFT JOIN hrjob_master ON hrjob_master.jobunkid=hremployee_master.jobunkid " & _
                        '                  "LEFT JOIN hrclassgroup_master ON hrclassgroup_master.classgroupunkid = hremployee_master.classgroupunkid " & _
                        '                  "WHERE CONVERT(CHAR(8),appointeddate,112) <= CONVERT(CHAR(8),GETDATE(),112) " & _
                        '                                           "AND ISNULL(CONVERT(CHAR(8),termination_from_date,112),CONVERT(CHAR(8),GETDATE(),112)) >= CONVERT(CHAR(8),GETDATE(),112) " & _
                        '                                           "AND ISNULL(CONVERT(CHAR(8),termination_to_date,112),CONVERT(CHAR(8),GETDATE(),112)) >= CONVERT(CHAR(8),GETDATE(),112) " & _
                        '                                          "AND ISNULL(CONVERT(CHAR(8), empl_enddate,112), CONVERT(CHAR(8),GETDATE(),112)) >= CONVERT(CHAR(8),GETDATE(),112)') "

                        '    objDataOperation.ExecNonQuery(strQ)
                        '    If objDataOperation.ErrorMessage <> "" Then
                        '        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        '        Throw exForce
                        '    End If
                        If iCnt < dsyear.Tables("year").Rows.Count Then

                            strQ = " USE " & dtRow.Item("database_name").ToString
                           'Anjan [29 June 2016] -- End

                            objDataOperation.ExecNonQuery(strQ)
                            If objDataOperation.ErrorMessage <> "" Then
                                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                Throw exForce
                            End If

                    'Anjan [29 June 2016] -- Start
                    'ENHANCEMENT : close year change for linking synonym from new database to old database 
                        'ElseIf iCnt = 1 Then
                        Else
                     'Anjan [29 June 2016] -- End

                            strQ = " USE " & mstrNewYearDatabaseName

                            objDataOperation.ExecNonQuery(strQ)
                            If objDataOperation.ErrorMessage <> "" Then
                                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                Throw exForce
                            End If

                            strQ = "IF EXISTS (SELECT TABLE_NAME FROM INFORMATION_SCHEMA.VIEWS WHERE TABLE_NAME = 'vwEmployeeDetail') " & _
                                     " DROP VIEW vwEmployeeDetail "

                            objDataOperation.ExecNonQuery(strQ)
                            If objDataOperation.ErrorMessage <> "" Then
                                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                Throw exForce
                            End If

                            strQ = "EXEC('CREATE VIEW [vwEmployeeDetail] AS " & _
                                          "SELECT  employeecode as code " & _
                                                 ",ISNULL(firstname,'''') +'' ''+ ISNULL(othername,'''') +'' ''+ISNULL(surname,'''') AS employeename " & _
                                                 ",ISNULL(hrgradegroup_master.name,'''') AS gradegroup " & _
                                                         ",ISNULL(hrgrade_master.name,'''') AS grade " & _
                                                         ",ISNULL(hrgradelevel_master.name,'''') AS gradelevel " & _
                                                 ",ISNULL(hrdepartment_master.name,'''') as department " & _
                                                 ",ISNULL(hrstation_master.name,'''') as station " & _
                                                 ",ISNULL(hrjob_master.job_name,'''') as designation " & _
                                                 ",ISNULL(hrclassgroup_master.name,'''') as region " & _
                                          "FROM hremployee_master " & _
                                          "LEFT JOIN hrgradegroup_master ON hremployee_master.gradegroupunkid = hrgradegroup_master.gradegroupunkid " & _
                                                "LEFT JOIN hrgrade_master ON hrgrade_master.gradeunkid=hremployee_master.gradeunkid " & _
                                                "LEFT JOIN hrgradelevel_master ON hremployee_master.gradelevelunkid = hrgradelevel_master.gradelevelunkid " & _
                                          "LEFT JOIN hrdepartment_master ON hrdepartment_master.departmentunkid=hremployee_master.departmentunkid " & _
                                          "LEFT JOIN hrstation_master ON hrstation_master.stationunkid=hremployee_master.stationunkid " & _
                                          "LEFT JOIN hrjob_master ON hrjob_master.jobunkid=hremployee_master.jobunkid " & _
                                          "LEFT JOIN hrclassgroup_master ON hrclassgroup_master.classgroupunkid = hremployee_master.classgroupunkid " & _
                                          "WHERE CONVERT(CHAR(8),appointeddate,112) <= CONVERT(CHAR(8),GETDATE(),112) " & _
                                                                   "AND ISNULL(CONVERT(CHAR(8),termination_from_date,112),CONVERT(CHAR(8),GETDATE(),112)) >= CONVERT(CHAR(8),GETDATE(),112) " & _
                                                                   "AND ISNULL(CONVERT(CHAR(8),termination_to_date,112),CONVERT(CHAR(8),GETDATE(),112)) >= CONVERT(CHAR(8),GETDATE(),112) " & _
                                                                  "AND ISNULL(CONVERT(CHAR(8), empl_enddate,112), CONVERT(CHAR(8),GETDATE(),112)) >= CONVERT(CHAR(8),GETDATE(),112)') "

                            objDataOperation.ExecNonQuery(strQ)
                            If objDataOperation.ErrorMessage <> "" Then
                                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                Throw exForce
                            End If

                        End If


                        For Each strUserName In users

                            strQ = "IF  EXISTS(SELECT * FROM sys.synonyms WHERE name = 'vw" & strCompanyCode & "') DROP SYNONYM  vw" & strCompanyCode & "  " & _
                                       "BEGIN " & _
                                    " CREATE SYNONYM vw" & strCompanyCode & " " & _
                                    "FOR " & mstrNewYearDatabaseName & "..vwEmployeeDetail " & _
                                       "End " & _
                                      "IF NOT EXISTS(SELECT * FROM sys.database_principals WHERE type_desc = 'DATABASE_ROLE' AND name = 'selectview') " & _
                                       "BEGIN " & _
                                       "CREATE ROLE selectview  " & _
                                       "END " & _
                                       "IF EXISTS(SELECT * FROM sys.syslogins WHERE name = '" & strUserName & "') " & _
                                       "BEGIN " & _
                                            "IF NOT EXISTS(SELECT * FROM sys.sysusers WHERE issqluser =1 AND islogin = 1 AND name = '" & strUserName & "') " & _
                                            "BEGIN " & _
                                                "CREATE USER " & strUserName & " FOR LOGIN " & strUserName & " " & _
                                            "END " & _
                                            "BEGIN " & _
                                               "GRANT SELECT ON vw" & strCompanyCode & " TO selectview " & _
                                               "GRANT SELECT ON vwEmployeeDetail TO selectview " & _
                                               "EXEC sp_addrolemember  'selectview','" & strUserName & "'" & _
                                             "END " & _
                                       "END "

                            objDataOperation.ExecNonQuery(strQ)
                            If objDataOperation.ErrorMessage <> "" Then
                                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                Throw exForce
                            End If

                        Next
                    Next

                End If
            End If

            Return True

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "CreateUserViewSynonymsForSharePointTRA", mstrModuleName)
        Finally
            exForce = Nothing
            objDataOperation = Nothing
            objMaster = Nothing
        End Try
    End Function
    'Sohail (24 Jun 2013) -- End


    'S.SANDEEP [ 25 JUNE 2013 ] -- START
    'ENHANCEMENT : OTHER CHANGES
    Private Function CreateUserViewSynonymsFor_IDMS() As Boolean
        Dim objDataOperation As clsDataOperation
        Dim objMaster As New clsMasterData
        Dim dsList As DataSet
        Dim strQ As String = ""
        Dim mstrCurrYearDatabaseName As String = ""
        Dim mstrNewYearDatabaseName As String = ""
        Dim exForce As Exception
        Try
            objDataOperation = New clsDataOperation

            If objDataOperation.RecordCount("SELECT * FROM sys.databases WHERE UPPER(name) = 'IDM_DATA'") <= 0 Then
                Return True
            End If

            'strQ = "USE [IDM_DATA] "

            'objDataOperation.ExecNonQuery(strQ)

            'If objDataOperation.ErrorMessage <> "" Then
            '    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
            '    Throw exForce
            'End If

            'strQ = "EXECUTE SP_CONFIGURE 'show advanced options', 1; " & _
            '       "RECONFIGURE WITH OVERRIDE; " & _
            '       "EXEC SP_CONFIGURE 'Ole Automation Procedures', 1; " & _
            '       "RECONFIGURE WITH OVERRIDE; "

            'objDataOperation.ExecNonQuery(strQ)

            'If objDataOperation.ErrorMessage <> "" Then
            '    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
            '    Throw exForce
            'End If

            'Dim sPath As String = IO.Path.GetFullPath(My.Computer.FileSystem.SpecialDirectories.Temp)
            'sPath = GetShortPath(sPath)

            'strQ = "DECLARE @IMG_PATH VARBINARY(MAX) " & _
            '       "DECLARE @ObjectToken INT " & _
            '       "SELECT @IMG_PATH = content FROM sys.assembly_files WHERE assembly_id = 65536 " & _
            '       "    EXEC sp_OACreate 'ADODB.Stream', @ObjectToken OUTPUT " & _
            '       "    EXEC sp_OASetProperty @ObjectToken, 'Type', 1 " & _
            '       "    EXEC sp_OAMethod @ObjectToken, 'Open' " & _
            '       "    EXEC sp_OAMethod @ObjectToken, 'Write', NULL, @IMG_PATH " & _
            '       "    EXEC sp_OAMethod @ObjectToken, 'SaveToFile', NULL, '" & sPath & "\ArutiSQL_Lib.dll', 2 " & _
            '       "    EXEC sp_OAMethod @ObjectToken, 'Close' " & _
            '       "    EXEC sp_OADestroy @ObjectToken "

            'objDataOperation.ExecNonQuery(strQ)

            'If objDataOperation.ErrorMessage <> "" Then
            '    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
            '    Throw exForce
            'End If

            mstrCurrYearDatabaseName = FinancialYear._Object._DatabaseName

            strQ = "USE " & mstrCurrYearDatabaseName
            objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            dsList = objMaster.Get_Database_Year_List("List", False, Company._Object._Companyunkid)

            If dsList.Tables("List").Rows.Count > 0 Then
                mstrNewYearDatabaseName = dsList.Tables("List").Rows(0).Item("database_name").ToString
            Else
                Return True
                Exit Function
            End If

            If objDataOperation.RecordCount("SELECT * FROM sys.database_principals WHERE type_desc = 'DATABASE_ROLE' AND name = 'oracleview'") > 0 Then
                strQ = "exec sp_helprolemember  'oracleview' "
                Dim ds As DataSet = objDataOperation.ExecQuery(strQ, "UserList")
                Dim users As List(Of String) = (From p In ds.Tables(0).AsEnumerable() Select (p.Item("MemberName").ToString)).ToList
                If users.ToList.Count > 0 Then
                    strQ = " USE " & mstrNewYearDatabaseName
                    objDataOperation.ExecNonQuery(strQ)
                    If objDataOperation.ErrorMessage <> "" Then
                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If
                    Dim strtranSelectFields As String = "(employeeunkid ,employeecode ,titleunkid ,firstname ,surname ,othername ,appointeddate ,gender ,employmenttypeunkid ,paytypeunkid " & _
                                                        ",paypointunkid ,loginname ,password ,email ,displayname ,shiftunkid ,birthdate ,birth_ward ,birthcertificateno ,birthstateunkid " & _
                                                        ",birthcountryunkid ,birthcityunkid ,birth_village ,work_permit_no ,workcountryunkid ,work_permit_issue_place ,work_permit_issue_date " & _
                                                        ",work_permit_expiry_date ,complexionunkid ,bloodgroupunkid ,eyecolorunkid ,nationalityunkid ,ethnicityunkid ,religionunkid ,hairunkid " & _
                                                        ",language1unkid ,language2unkid ,language3unkid ,language4unkid ,extra_tel_no ,height ,weight ,maritalstatusunkid ,anniversary_date " & _
                                                        ",sports_hobbies ,present_address1 ,present_address2 ,present_countryunkid ,present_postcodeunkid ,present_stateunkid ,present_provicnce " & _
                                                        ",present_post_townunkid ,present_road ,present_estate ,present_plotNo ,present_mobile ,present_alternateno ,present_tel_no ,present_fax " & _
                                                        ",present_email ,domicile_address1 ,domicile_address2 ,domicile_countryunkid ,domicile_postcodeunkid ,domicile_stateunkid ,domicile_provicnce " & _
                                                        ",domicile_post_townunkid ,domicile_road ,domicile_estate ,domicile_plotno ,domicile_mobile ,domicile_alternateno ,domicile_tel_no ,domicile_fax " & _
                                                        ",domicile_email ,emer_con_firstname ,emer_con_lastname ,emer_con_address ,emer_con_countryunkid ,emer_con_postcodeunkid ,emer_con_state " & _
                                                        ",emer_con_provicnce ,emer_con_post_townunkid ,emer_con_road ,emer_con_estate ,emer_con_plotno ,emer_con_mobile ,emer_con_alternateno " & _
                                                        ",emer_con_tel_no ,emer_con_fax ,emer_con_email ,stationunkid ,deptgroupunkid ,departmentunkid ,sectionunkid ,unitunkid ,jobgroupunkid " & _
                                                        ",jobunkid ,gradegroupunkid ,gradeunkid ,gradelevelunkid ,accessunkid ,classgroupunkid ,classunkid ,serviceunkid ,costcenterunkid ,tranhedunkid " & _
                                                        ",actionreasonunkid ,suspended_from_date ,suspended_to_date ,probation_from_date ,probation_to_date ,termination_from_date ,termination_to_date " & _
                                                        ",remark ,isactive ,emer_con_firstname2 ,emer_con_lastname2 ,emer_con_address2 ,emer_con_countryunkid2 ,emer_con_postcodeunkid2 ,emer_con_state2 " & _
                                                        ",emer_con_provicnce2 ,emer_con_post_townunkid2 ,emer_con_road2 ,emer_con_estate2 ,emer_con_plotno2 ,emer_con_mobile2 ,emer_con_alternateno2 " & _
                                                        ",emer_con_tel_no2 ,emer_con_fax2 ,emer_con_email2 ,emer_con_firstname3 ,emer_con_lastname3 ,emer_con_address3 ,emer_con_countryunkid3 " & _
                                                        ",emer_con_postcodeunkid3 ,emer_con_state3 ,emer_con_provicnce3 ,emer_con_post_townunkid3 ,emer_con_road3 ,emer_con_estate3 ,emer_con_plotno3 " & _
                                                        ",emer_con_mobile3 ,emer_con_alternateno3 ,emer_con_tel_no3 ,emer_con_fax3 ,emer_con_email3 ,sectiongroupunkid ,unitgroupunkid " & _
                                                        ",teamunkid ,confirmation_date ,empl_enddate ,isclear ,allocationreason ,reinstatement_date ,isexclude_payroll ,isapproved ,companyunkid) "
                    For Each strUserName In users
                        strQ = "IF NOT EXISTS(SELECT * FROM sys.database_principals WHERE type_desc = 'DATABASE_ROLE' AND name = 'oracleview') " & _
                           "BEGIN " & _
                           "    CREATE ROLE oracleview  " & _
                           "END " & _
                           "IF NOT EXISTS(SELECT * FROM sys.database_principals WHERE type_desc = 'DATABASE_ROLE' AND name = 'oracleviewupdate') " & _
                           "BEGIN " & _
                           "    CREATE ROLE oracleviewupdate  " & _
                           "END " & _
                           "IF EXISTS(SELECT * FROM sys.syslogins WHERE name = '" & strUserName & "') " & _
                           "BEGIN " & _
                           "    IF NOT EXISTS(SELECT * FROM sys.sysusers WHERE issqluser =1 AND islogin = 1 AND name = '" & strUserName & "') " & _
                           "    BEGIN " & _
                           "        CREATE USER " & strUserName & " FOR LOGIN " & strUserName & " " & _
                           "    END " & _
                           "    BEGIN " & _
                           "        GRANT SELECT ON " & mstrNewYearDatabaseName & "..hremployee_master " & strtranSelectFields & " To oracleview " & _
                           "        GRANT SELECT ON " & mstrNewYearDatabaseName & "..hremployee_reportto (employeeunkid,reporttoemployeeunkid,ishierarchy) To oracleview " & _
                           "        GRANT UPDATE ON " & mstrNewYearDatabaseName & "..hremployee_master (password,employeeunkid,companyunkid) To oracleview " & _
                           "        GRANT INSERT ON " & mstrNewYearDatabaseName & "..athremployee_master To oracleview " & _
                           "        EXEC sp_addrolemember  'oracleview','" & strUserName & "'" & _
                           "        EXEC sp_addrolemember  'oracleviewupdate','" & strUserName & "'" & _
                           "    END " & _
                           "END "
                        objDataOperation.ExecNonQuery(strQ)

                        If objDataOperation.ErrorMessage <> "" Then
                            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                            Throw exForce
                        End If

                    Next

                    'Enabling CLR
                    strQ = "EXEC sp_configure 'clr enable', 1 " & _
                           "RECONFIGURE; "

                    objDataOperation.ExecNonQuery(strQ)

                    If objDataOperation.ErrorMessage <> "" Then
                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If

                    'Setting Database Trustworthy
                    strQ = "ALTER DATABASE " & mstrNewYearDatabaseName & " SET TRUSTWORTHY ON "

                    objDataOperation.ExecNonQuery(strQ)

                    If objDataOperation.ErrorMessage <> "" Then
                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If

                    'Registering Assembly
                    strQ = "IF NOT EXISTS (SELECT * FROM sys.assemblies WHERE name='EncDec') " & _
                           "BEGIN " & _
                           "  CREATE ASSEMBLY EncDec " & _
                           "  AUTHORIZATION dbo " & _
                           "  FROM '" & AppSettings._Object._ApplicationPath & "ArutiSQL_Lib.dll' " & _
                           "  WITH PERMISSION_SET = UNSAFE " & _
                           "END "

                    objDataOperation.ExecNonQuery(strQ)

                    If objDataOperation.ErrorMessage <> "" Then
                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If

                    'Generation Encrypt Function
                    strQ = "IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[EncryptString]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT')) " & _
                           "BEGIN " & _
                           "EXECUTE dbo.sp_executesql @statement = N'CREATE FUNCTION [dbo].[EncryptString](@PlainText [nvarchar](max), @Key [nvarchar](max)) " & _
                           "RETURNS [nvarchar](max) WITH EXECUTE AS CALLER " & _
                           "AS " & _
                           "EXTERNAL NAME [EncDec].[Security].[Encrypt]' " & _
                           "END "

                    objDataOperation.ExecNonQuery(strQ)

                    If objDataOperation.ErrorMessage <> "" Then
                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If

                    'Generation Decrypt Function
                    strQ = "IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[DecryptString]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT')) " & _
                           "BEGIN " & _
                           "EXECUTE dbo.sp_executesql @statement = N'CREATE FUNCTION [dbo].[DecryptString](@PlainText [nvarchar](max), @Key [nvarchar](max)) " & _
                           "RETURNS [nvarchar](max) WITH EXECUTE AS CALLER " & _
                           "AS " & _
                           "    EXTERNAL NAME [EncDec].[Security].[Decrypt]' " & _
                           "END "

                    objDataOperation.ExecNonQuery(strQ)

                    If objDataOperation.ErrorMessage <> "" Then
                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If

                    'Generating IP_Address Function
                    strQ = "IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GetIPAddress]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT')) " & _
                           "BEGIN " & _
                           "EXECUTE dbo.sp_executesql @statement = N'CREATE FUNCTION [dbo].[GetIPAddress](@Host [nvarchar](max)) " & _
                           "RETURNS [nvarchar](max) WITH EXECUTE AS CALLER " & _
                           "AS " & _
                           "    EXTERNAL NAME [EncDec].[Security].[GetIpaddress]' " & _
                           "END "
                    objDataOperation.ExecNonQuery(strQ)

                    If objDataOperation.ErrorMessage <> "" Then
                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If

                    Dim StrValue As String = " hremployee_master.employeeunkid AS employeeunkid " & _
                                             ",hremployee_master.employeecode AS employeecode " & _
                                             ",ISNULL(title.NAME, '''') AS title " & _
                                             ",hremployee_master.firstname AS firstname " & _
                                             ",hremployee_master.surname AS surname " & _
                                             ",hremployee_master.othername AS othername " & _
                                             ",hremployee_master.appointeddate AS appointeddate " & _
                                             ",CASE WHEN hremployee_master.gender = 1 THEN ''Male'' " & _
                                             "      WHEN hremployee_master.gender = 2 THEN ''Female'' " & _
                                             "      WHEN hremployee_master.gender = 3 THEN ''Other'' " & _
                                             "ELSE '''' " & _
                                             "END AS gendertype " & _
                                             ",ISNULL(emptype.NAME, '''') AS employmenttype " & _
                                             ",ISNULL(ptype.NAME, '''') AS paytype " & _
                                             ",ISNULL(prpaypoint_master.paypointname,'''') AS paypointname " & _
                                             ",hremployee_master.loginname AS loginname " & _
                                             ",'''' AS password " & _
                                             ",hremployee_master.email AS email " & _
                                             ",hremployee_master.displayname AS displayname " & _
                                             ",ISNULL(tnashift_master.shiftname, '''') AS shiftname " & _
                                             ",hremployee_master.birthdate AS birthdate " & _
                                             ",hremployee_master.birth_ward AS birth_ward " & _
                                             ",hremployee_master.birthcertificateno AS birthcertificateno " & _
                                             ",ISNULL(bst.NAME, '''') AS birthstatename " & _
                                             ",ISNULL(cct.country_name, '''') AS birthcountryname " & _
                                             ",ISNULL(ccty.NAME, '''') AS birthcityname " & _
                                             ",hremployee_master.birth_village AS birth_village " & _
                                             ",hremployee_master.work_permit_no AS work_permit_no " & _
                                             ",ISNULL(wct.country_name, '''') AS workcountryname " & _
                                             ",hremployee_master.work_permit_issue_place AS work_permit_issue_place " & _
                                             ",hremployee_master.work_permit_issue_date AS work_permit_issue_date " & _
                                             ",hremployee_master.work_permit_expiry_date AS work_permit_expiry_date " & _
                                             ",ISNULL(ctype.NAME, '''') AS complexionname " & _
                                             ",ISNULL(btype.NAME, '''') AS bloodgroupname " & _
                                             ",ISNULL(etype.NAME, '''') AS eyecolorname " & _
                                             ",ISNULL(nct.country_name, '''') AS nationalityname " & _
                                             ",ISNULL(entype.NAME, '''') AS ethnicityname " & _
                                             ",ISNULL(rtype.NAME, '''') AS religionname " & _
                                             ",ISNULL(htype.NAME, '''') AS hairname " & _
                                             ",ISNULL(l1type.NAME, '''') AS language1name " & _
                                             ",ISNULL(l2type.NAME, '''') AS language2name " & _
                                             ",ISNULL(l3type.NAME, '''') AS language3name " & _
                                             ",ISNULL(l4type.NAME, '''') AS language4name " & _
                                             ",hremployee_master.extra_tel_no AS extra_tel_no " & _
                                             ",hremployee_master.height AS height " & _
                                             ",hremployee_master.weight AS weight " & _
                                             ",ISNULL(mtype.NAME, '''') AS maritalstatusname " & _
                                             ",hremployee_master.anniversary_date AS anniversary_date " & _
                                             ",hremployee_master.sports_hobbies AS sports_hobbies " & _
                                             ",hremployee_master.present_address1 AS present_address1 " & _
                                             ",hremployee_master.present_address2 AS present_address2 " & _
                                             ",ISNULL(pct.country_name, '''') AS present_countryname " & _
                                             ",ISNULL(pz.zipcode_no, '''') AS present_postcodename " & _
                                             ",ISNULL(pst.NAME, '''') AS present_statename " & _
                                             ",hremployee_master.present_provicnce AS present_provicnce " & _
                                             ",ISNULL(pcy.NAME, '''') AS present_post_townname " & _
                                             ",hremployee_master.present_road AS present_road " & _
                                             ",hremployee_master.present_estate AS present_estate " & _
                                             ",hremployee_master.present_plotNo AS present_plotNo " & _
                                             ",hremployee_master.present_mobile AS present_mobile " & _
                                             ",hremployee_master.present_alternateno AS present_alternateno " & _
                                             ",hremployee_master.present_tel_no AS present_tel_no " & _
                                             ",hremployee_master.present_fax AS present_fax " & _
                                             ",hremployee_master.present_email AS present_email " & _
                                             ",hremployee_master.domicile_address1 AS domicile_address1 " & _
                                             ",hremployee_master.domicile_address2 AS domicile_address2 " & _
                                             ",ISNULL(dct.country_name, '''') AS domicile_countryname " & _
                                             ",ISNULL(dz.zipcode_no, '''') AS domicile_postcodename " & _
                                             ",ISNULL(dst.NAME, '''') AS domicile_statename " & _
                                             ",hremployee_master.domicile_provicnce AS domicile_provicnce " & _
                                             ",ISNULL(dcy.NAME, '''') AS domicile_post_townname " & _
                                             ",hremployee_master.domicile_road AS domicile_road " & _
                                             ",hremployee_master.domicile_estate AS domicile_estate " & _
                                             ",hremployee_master.domicile_plotno AS domicile_plotno " & _
                                             ",hremployee_master.domicile_mobile AS domicile_mobile " & _
                                             ",hremployee_master.domicile_alternateno AS domicile_alternateno " & _
                                             ",hremployee_master.domicile_tel_no AS domicile_tel_no " & _
                                             ",hremployee_master.domicile_fax AS domicile_fax " & _
                                             ",hremployee_master.domicile_email AS domicile_email " & _
                                             ",hremployee_master.emer_con_firstname AS emer_con_firstname " & _
                                             ",hremployee_master.emer_con_lastname AS emer_con_lastname " & _
                                             ",hremployee_master.emer_con_address AS emer_con_address " & _
                                             ",ISNULL(ect.country_name, '''') AS emer_con_countryname " & _
                                             ",ISNULL(ez.zipcode_no, '''') AS emer_con_postcodename " & _
                                             ",hremployee_master.emer_con_state AS emer_con_state " & _
                                             ",ISNULL(est.NAME, '''') AS emer_con_statename " & _
                                             ",hremployee_master.emer_con_provicnce AS emer_con_provicnce " & _
                                             ",ISNULL(ecy.NAME, '''') AS emer_con_post_townname " & _
                                             ",hremployee_master.emer_con_road AS emer_con_road " & _
                                             ",hremployee_master.emer_con_estate AS emer_con_estate " & _
                                             ",hremployee_master.emer_con_plotno AS emer_con_plotno " & _
                                             ",hremployee_master.emer_con_mobile AS emer_con_mobile " & _
                                             ",hremployee_master.emer_con_alternateno AS emer_con_alternateno " & _
                                             ",hremployee_master.emer_con_tel_no AS emer_con_tel_no " & _
                                             ",hremployee_master.emer_con_fax AS emer_con_fax " & _
                                             ",hremployee_master.emer_con_email AS emer_con_email " & _
                                             ",ISNULL(hrstation_master.name, '''') AS stationname " & _
                                             ",ISNULL(hrdepartment_group_master.name,'''') AS deptgroupname " & _
                                             ",ISNULL(hrdepartment_master.name, '''') AS departmentname " & _
                                             ",ISNULL(hrsectiongroup_master.name, '''') AS sectiongroupname " & _
                                             ",ISNULL(hrsection_master.name, '''') AS sectionname " & _
                                             ",ISNULL(hrunitgroup_master.name, '''') AS unitgroupname " & _
                                             ",ISNULL(hrunit_master.name, '''') AS unitname " & _
                                             ",ISNULL(hrteam_master.name, '''') AS teamname " & _
                                             ",ISNULL(hrjobgroup_master.name, '''') AS jobgroupname " & _
                                             ",ISNULL(hrjob_master.job_name, '''') AS jobname " & _
                                             ",ISNULL(hrgradegroup_master.name, '''') AS gradegroupname " & _
                                             ",ISNULL(hrgrade_master.name, '''') AS gradename " & _
                                             ",ISNULL(hrgradelevel_master.name, '''') AS gradelevelname " & _
                                             ",ISNULL(hrclassgroup_master.name, '''') AS classgroupname " & _
                                             ",ISNULL(hrclasses_master.name, '''') AS classname " & _
                                             ",ISNULL(prcostcenter_master.costcentername,'''') AS costcentername " & _
                                             ",ISNULL(prtranhead_master.trnheadname,'''') AS tranhedname " & _
                                             ",ISNULL(TR.name,'''') AS actionreasonname " & _
                                             ",hremployee_master.suspended_from_date AS suspended_from_date " & _
                                             ",hremployee_master.suspended_to_date AS suspended_to_date " & _
                                             ",hremployee_master.probation_from_date AS probation_from_date " & _
                                             ",hremployee_master.probation_to_date AS probation_to_date " & _
                                             ",hremployee_master.termination_from_date AS termination_from_date " & _
                                             ",hremployee_master.termination_to_date AS termination_to_date " & _
                                             ",hremployee_master.remark AS remark " & _
                                             ",hremployee_master.emer_con_firstname2 AS emer_con_firstname2 " & _
                                             ",hremployee_master.emer_con_lastname2 AS emer_con_lastname2 " & _
                                             ",hremployee_master.emer_con_address2 AS emer_con_address2 " & _
                                             ",ISNULL(ect2.country_name, '''') AS emer_con_countryname2 " & _
                                             ",ISNULL(ez2.zipcode_no, '''') AS emer_con_postcodename2 " & _
                                             ",ISNULL(est2.NAME, '''') AS emer_con_statename2 " & _
                                             ",hremployee_master.emer_con_provicnce2 AS emer_con_provicnce2 " & _
                                             ",ISNULL(ecy2.NAME, '''') AS emer_con_post_townname2 " & _
                                             ",hremployee_master.emer_con_road2 AS emer_con_road2 " & _
                                             ",hremployee_master.emer_con_estate2 AS emer_con_estate2 " & _
                                             ",hremployee_master.emer_con_plotno2 AS emer_con_plotno2 " & _
                                             ",hremployee_master.emer_con_mobile2 AS emer_con_mobile2 " & _
                                             ",hremployee_master.emer_con_alternateno2 AS emer_con_alternateno2 " & _
                                             ",hremployee_master.emer_con_tel_no2 AS emer_con_tel_no2 " & _
                                             ",hremployee_master.emer_con_fax2 AS emer_con_fax2 " & _
                                             ",hremployee_master.emer_con_email2 AS emer_con_email2 " & _
                                             ",hremployee_master.emer_con_firstname3 AS emer_con_firstname3 " & _
                                             ",hremployee_master.emer_con_lastname3 AS emer_con_lastname3 " & _
                                             ",hremployee_master.emer_con_address3 AS emer_con_address3 " & _
                                             ",ISNULL(ect3.country_name, '''') AS emer_con_countryname3 " & _
                                             ",ISNULL(ez3.zipcode_no, '''') AS emer_con_postcodename3 " & _
                                             ",ISNULL(est3.NAME, '''') AS emer_con_statename3 " & _
                                             ",hremployee_master.emer_con_provicnce3 AS emer_con_provicnce3 " & _
                                             ",ISNULL(ecy3.NAME, '''') AS emer_con_post_townname3 " & _
                                             ",hremployee_master.emer_con_road3 AS emer_con_road3 " & _
                                             ",hremployee_master.emer_con_estate3 AS emer_con_estate3 " & _
                                             ",hremployee_master.emer_con_plotno3 AS emer_con_plotno3 " & _
                                             ",hremployee_master.emer_con_mobile3 AS emer_con_mobile3 " & _
                                             ",hremployee_master.emer_con_alternateno3 AS emer_con_alternateno3 " & _
                                             ",hremployee_master.emer_con_tel_no3 AS emer_con_tel_no3 " & _
                                             ",hremployee_master.emer_con_fax3 AS emer_con_fax3 " & _
                                             ",hremployee_master.emer_con_email3 AS emer_con_email3 " & _
                                             ",hremployee_master.isclear AS isclear " & _
                                             ",hremployee_master.confirmation_date AS confirmation_date " & _
                                             ",hremployee_master.empl_enddate AS empl_enddate " & _
                                             ",hremployee_master.allocationreason AS allocationreason " & _
                                             ",hremployee_master.reinstatement_date AS reinstatement_date " & _
                                             ",hremployee_master.isexclude_payroll AS isexclude_payroll " & _
                                             ",hremployee_master.isapproved AS isapproved " & _
                                             ",CASE WHEN CONVERT(CHAR(8), hremployee_master.appointeddate, 112) <= CONVERT(CHAR(8), GETDATE(), 112) " & _
                                             "AND ISNULL(CONVERT(CHAR(8), hremployee_master.termination_from_date, 112), " & _
                                             "CONVERT(CHAR(8), GETDATE(), 112)) >= CONVERT(CHAR(8), GETDATE(), 112) " & _
                                             "AND ISNULL(CONVERT(CHAR(8), hremployee_master.termination_to_date, 112), " & _
                                             "CONVERT(CHAR(8), GETDATE(), 112)) >= CONVERT(CHAR(8), GETDATE(), 112) " & _
                                             "AND ISNULL(CONVERT(CHAR(8), hremployee_master.empl_enddate, 112), " & _
                                             "CONVERT(CHAR(8), GETDATE(), 112)) >= CONVERT(CHAR(8), GETDATE(), 112) " & _
                                             "THEN 1 " & _
                                             "ELSE 0 " & _
                                             "END AS activestatus " & _
                                             ",hremployee_master.titleunkid AS titleunkid " & _
                                             ",hremployee_master.gender AS gender " & _
                                             ",hremployee_master.employmenttypeunkid AS employmenttypeunkid " & _
                                             ",hremployee_master.paytypeunkid AS paytypeunkid " & _
                                             ",hremployee_master.paypointunkid AS paypointunkid " & _
                                             ",hremployee_master.shiftunkid AS shiftunkid " & _
                                             ",hremployee_master.birthstateunkid AS birthstateunkid " & _
                                             ",hremployee_master.birthcountryunkid AS birthcountryunkid " & _
                                             ",hremployee_master.birthcityunkid AS birthcityunkid " & _
                                             ",hremployee_master.workcountryunkid AS workcountryunkid " & _
                                             ",hremployee_master.complexionunkid AS complexionunkid " & _
                                             ",hremployee_master.bloodgroupunkid AS bloodgroupunkid " & _
                                             ",hremployee_master.eyecolorunkid AS eyecolorunkid " & _
                                             ",hremployee_master.nationalityunkid AS nationalityunkid " & _
                                             ",hremployee_master.ethnicityunkid AS ethnicityunkid " & _
                                             ",hremployee_master.religionunkid AS religionunkid " & _
                                             ",hremployee_master.hairunkid AS hairunkid " & _
                                             ",hremployee_master.language1unkid AS language1unkid " & _
                                             ",hremployee_master.language2unkid AS language2unkid " & _
                                             ",hremployee_master.language3unkid AS language3unkid " & _
                                             ",hremployee_master.language4unkid AS language4unkid " & _
                                             ",hremployee_master.maritalstatusunkid AS maritalstatusunkid " & _
                                             ",hremployee_master.present_countryunkid AS present_countryunkid " & _
                                             ",hremployee_master.present_postcodeunkid AS present_postcodeunkid " & _
                                             ",hremployee_master.present_stateunkid AS present_stateunkid " & _
                                             ",hremployee_master.present_post_townunkid AS present_post_townunkid " & _
                                             ",hremployee_master.domicile_countryunkid AS domicile_countryunkid " & _
                                             ",hremployee_master.domicile_postcodeunkid AS domicile_postcodeunkid " & _
                                             ",hremployee_master.domicile_stateunkid AS domicile_stateunkid " & _
                                             ",hremployee_master.domicile_post_townunkid AS domicile_post_townunkid " & _
                                             ",hremployee_master.emer_con_countryunkid AS emer_con_countryunkid " & _
                                             ",hremployee_master.emer_con_postcodeunkid AS emer_con_postcodeunkid " & _
                                             ",hremployee_master.emer_con_post_townunkid AS emer_con_post_townunkid " & _
                                             ",hremployee_master.stationunkid AS stationunkid " & _
                                             ",hremployee_master.deptgroupunkid AS deptgroupunkid " & _
                                             ",hremployee_master.departmentunkid AS departmentunkid " & _
                                             ",hremployee_master.sectiongroupunkid AS sectiongroupunkid " & _
                                             ",hremployee_master.sectionunkid AS sectionunkid " & _
                                             ",hremployee_master.unitgroupunkid AS unitgroupunkid " & _
                                             ",hremployee_master.unitunkid AS unitunkid " & _
                                             ",hremployee_master.teamunkid AS teamunkid " & _
                                             ",hremployee_master.jobgroupunkid AS jobgroupunkid " & _
                                             ",hremployee_master.jobunkid AS jobunkid " & _
                                             ",hremployee_master.gradegroupunkid AS gradegroupunkid " & _
                                             ",hremployee_master.gradeunkid AS gradeunkid " & _
                                             ",hremployee_master.gradelevelunkid AS gradelevelunkid " & _
                                             ",hremployee_master.classgroupunkid AS classgroupunkid " & _
                                             ",hremployee_master.classunkid AS classunkid " & _
                                             ",hremployee_master.costcenterunkid AS costcenterunkid " & _
                                             ",hremployee_master.tranhedunkid AS tranhedunkid " & _
                                             ",hremployee_master.actionreasonunkid AS actionreasonunkid " & _
                                             ",hremployee_master.emer_con_countryunkid2 AS emer_con_countryunkid2 " & _
                                             ",hremployee_master.emer_con_postcodeunkid2 AS emer_con_postcodeunkid2 " & _
                                             ",hremployee_master.emer_con_state2 AS emer_con_state2 " & _
                                             ",hremployee_master.emer_con_post_townunkid2 AS emer_con_post_townunkid2 " & _
                                             ",hremployee_master.emer_con_countryunkid3 AS emer_con_countryunkid3 " & _
                                             ",hremployee_master.emer_con_postcodeunkid3 AS emer_con_postcodeunkid3 " & _
                                             ",hremployee_master.emer_con_state3 AS emer_con_state3 " & _
                                             ",hremployee_master.emer_con_post_townunkid3 AS emer_con_post_townunkid3 " & _
                                             ",hremployee_master.companyunkid AS companyunkid " & _
                                             ",reportingemployeecode AS reportingemployeecode " & _
                                             ",reportingtoemployee AS reportingtoemployee " & _
                                             ",GETDATE() AS modify_date "

                    Dim StrJoins As String = "LEFT JOIN cfcommon_master AS title ON title.masterunkid = hremployee_master.titleunkid AND title.mastertype = " & clsCommon_Master.enCommonMaster.TITLE & " " & _
                                             "LEFT JOIN cfcommon_master AS emptype ON emptype.masterunkid = hremployee_master.employmenttypeunkid AND emptype.mastertype = " & clsCommon_Master.enCommonMaster.EMPLOYEMENT_TYPE & " " & _
                                             "LEFT JOIN cfcommon_master AS ptype ON ptype.masterunkid = hremployee_master.paytypeunkid AND ptype.mastertype = " & clsCommon_Master.enCommonMaster.PAY_TYPE & " " & _
                                             "LEFT JOIN prpaypoint_master ON prpaypoint_master.paypointunkid = hremployee_master.paypointunkid " & _
                                             "LEFT JOIN tnashift_master ON tnashift_master.shiftunkid = hremployee_master.shiftunkid " & _
                                             "LEFT JOIN hrmsConfiguration..cfstate_master AS bst ON bst.stateunkid = hremployee_master.birthstateunkid " & _
                                             "LEFT JOIN hrmsConfiguration..cfcountry_master AS cct ON cct.countryunkid = hremployee_master.birthcountryunkid " & _
                                             "LEFT JOIN hrmsConfiguration..cfcity_master AS ccty ON ccty.cityunkid = hremployee_master.birthcityunkid " & _
                                             "LEFT JOIN hrmsConfiguration..cfcountry_master AS wct ON wct.countryunkid = hremployee_master.workcountryunkid " & _
                                             "LEFT JOIN cfcommon_master AS ctype ON ctype.masterunkid = hremployee_master.complexionunkid AND ctype.mastertype = " & clsCommon_Master.enCommonMaster.COMPLEXION & " " & _
                                             "LEFT JOIN cfcommon_master AS btype ON btype.masterunkid = hremployee_master.bloodgroupunkid AND btype.mastertype = " & clsCommon_Master.enCommonMaster.BLOOD_GROUP & " " & _
                                             "LEFT JOIN cfcommon_master AS etype ON etype.masterunkid = hremployee_master.eyecolorunkid AND etype.mastertype = " & clsCommon_Master.enCommonMaster.EYES_COLOR & " " & _
                                             "LEFT JOIN hrmsConfiguration..cfcountry_master AS nct ON nct.countryunkid = hremployee_master.nationalityunkid " & _
                                             "LEFT JOIN cfcommon_master AS entype ON entype.masterunkid = hremployee_master.ethnicityunkid AND entype.mastertype = " & clsCommon_Master.enCommonMaster.ETHNICITY & " " & _
                                             "LEFT JOIN cfcommon_master AS rtype ON rtype.masterunkid = hremployee_master.religionunkid AND rtype.mastertype = " & clsCommon_Master.enCommonMaster.RELIGION & " " & _
                                             "LEFT JOIN cfcommon_master AS htype ON htype.masterunkid = hremployee_master.hairunkid AND htype.mastertype = " & clsCommon_Master.enCommonMaster.HAIR_COLOR & " " & _
                                             "LEFT JOIN cfcommon_master AS l1type ON l1type.masterunkid = hremployee_master.language1unkid AND l1type.mastertype = " & clsCommon_Master.enCommonMaster.LANGUAGES & " " & _
                                             "LEFT JOIN cfcommon_master AS l2type ON l2type.masterunkid = hremployee_master.language2unkid AND l1type.mastertype = " & clsCommon_Master.enCommonMaster.LANGUAGES & " " & _
                                             "LEFT JOIN cfcommon_master AS l3type ON l3type.masterunkid = hremployee_master.language3unkid AND l1type.mastertype = " & clsCommon_Master.enCommonMaster.LANGUAGES & " " & _
                                             "LEFT JOIN cfcommon_master AS l4type ON l4type.masterunkid = hremployee_master.language4unkid AND l1type.mastertype = " & clsCommon_Master.enCommonMaster.LANGUAGES & " " & _
                                             "LEFT JOIN cfcommon_master AS mtype ON mtype.masterunkid = hremployee_master.maritalstatusunkid AND mtype.mastertype = " & clsCommon_Master.enCommonMaster.MARRIED_STATUS & " " & _
                                             "LEFT JOIN hrmsConfiguration..cfcountry_master AS pct ON pct.countryunkid = hremployee_master.present_countryunkid " & _
                                             "LEFT JOIN hrmsConfiguration..cfzipcode_master AS pz ON pz.zipcodeunkid = hremployee_master.present_postcodeunkid " & _
                                             "LEFT JOIN hrmsConfiguration..cfstate_master AS pst ON pst.stateunkid = hremployee_master.present_stateunkid " & _
                                             "LEFT JOIN hrmsConfiguration..cfcity_master AS pcy ON pcy.cityunkid = hremployee_master.present_post_townunkid " & _
                                             "LEFT JOIN hrmsConfiguration..cfcountry_master AS dct ON dct.countryunkid = hremployee_master.domicile_countryunkid " & _
                                             "LEFT JOIN hrmsConfiguration..cfzipcode_master AS dz ON dz.zipcodeunkid = hremployee_master.domicile_postcodeunkid " & _
                                             "LEFT JOIN hrmsConfiguration..cfstate_master AS dst ON dst.stateunkid = hremployee_master.domicile_stateunkid " & _
                                             "LEFT JOIN hrmsConfiguration..cfcity_master AS dcy ON dcy.cityunkid = hremployee_master.domicile_post_townunkid " & _
                                             "LEFT JOIN hrmsConfiguration..cfcountry_master AS ect ON ect.countryunkid = hremployee_master.emer_con_countryunkid " & _
                                             "LEFT JOIN hrmsConfiguration..cfzipcode_master AS ez ON ez.zipcodeunkid = hremployee_master.emer_con_postcodeunkid " & _
                                             "LEFT JOIN hrmsConfiguration..cfstate_master AS est ON est.stateunkid = hremployee_master.emer_con_state " & _
                                             "LEFT JOIN hrmsConfiguration..cfcity_master AS ecy ON ecy.cityunkid = hremployee_master.emer_con_post_townunkid " & _
                                             "LEFT JOIN hrmsConfiguration..cfcountry_master AS ect2 ON ect2.countryunkid = hremployee_master.emer_con_countryunkid2 " & _
                                             "LEFT JOIN hrmsConfiguration..cfzipcode_master AS ez2 ON ez2.zipcodeunkid = hremployee_master.emer_con_postcodeunkid2 " & _
                                             "LEFT JOIN hrmsConfiguration..cfstate_master AS est2 ON est2.stateunkid = hremployee_master.emer_con_state2 " & _
                                             "LEFT JOIN hrmsConfiguration..cfcity_master AS ecy2 ON ecy2.cityunkid = hremployee_master.emer_con_post_townunkid2 " & _
                                             "LEFT JOIN hrmsConfiguration..cfcountry_master AS ect3 ON ect3.countryunkid = hremployee_master.emer_con_countryunkid3 " & _
                                             "LEFT JOIN hrmsConfiguration..cfzipcode_master AS ez3 ON ez3.zipcodeunkid = hremployee_master.emer_con_postcodeunkid3 " & _
                                             "LEFT JOIN hrmsConfiguration..cfstate_master AS est3 ON est3.stateunkid = hremployee_master.emer_con_state3 " & _
                                             "LEFT JOIN hrmsConfiguration..cfcity_master AS ecy3 ON ecy3.cityunkid = hremployee_master.emer_con_post_townunkid3 " & _
                                             "LEFT JOIN hrstation_master ON hrstation_master.stationunkid = hremployee_master.stationunkid " & _
                                             "LEFT JOIN hrdepartment_group_master ON hrdepartment_group_master.deptgroupunkid = hremployee_master.deptgroupunkid " & _
                                             "LEFT JOIN hrdepartment_master ON hrdepartment_master.departmentunkid = hremployee_master.departmentunkid " & _
                                             "LEFT JOIN hrsection_master ON hrsection_master.sectionunkid = hremployee_master.sectionunkid " & _
                                             "LEFT JOIN hrunit_master ON hrunit_master.unitunkid = hremployee_master.unitunkid " & _
                                             "LEFT JOIN hrjobgroup_master ON hrjobgroup_master.jobgroupunkid = hremployee_master.jobgroupunkid " & _
                                             "LEFT JOIN hrjob_master ON hrjob_master.jobunkid = hremployee_master.jobunkid " & _
                                             "LEFT JOIN hrgradegroup_master ON hrgradegroup_master.gradegroupunkid = hremployee_master.gradegroupunkid " & _
                                             "LEFT JOIN hrgrade_master ON hrgrade_master.gradeunkid = hremployee_master.gradeunkid " & _
                                             "LEFT JOIN hrgradelevel_master ON hrgradelevel_master.gradelevelunkid = hremployee_master.gradelevelunkid " & _
                                             "LEFT JOIN hrclassgroup_master ON hrclassgroup_master.classgroupunkid = hremployee_master.classgroupunkid " & _
                                             "LEFT JOIN hrclasses_master ON hrclasses_master.classesunkid = hremployee_master.classunkid " & _
                                             "LEFT JOIN prcostcenter_master ON prcostcenter_master.costcenterunkid = hremployee_master.costcenterunkid " & _
                                             "LEFT JOIN prtranhead_master ON prtranhead_master.tranheadunkid = hremployee_master.tranhedunkid " & _
                                             "LEFT JOIN cfcommon_master AS TR ON TR.masterunkid = hremployee_master.actionreasonunkid AND TR.mastertype = '" & clsCommon_Master.enCommonMaster.TERMINATION & "'  " & _
                                             "LEFT JOIN hrteam_master ON hremployee_master.teamunkid = hrteam_master.teamunkid " & _
                                             "LEFT JOIN hrunitgroup_master ON hremployee_master.unitgroupunkid = hrunitgroup_master.unitgroupunkid " & _
                                             "LEFT JOIN hrsectiongroup_master ON hremployee_master.sectiongroupunkid = hrsectiongroup_master.sectiongroupunkid " & _
                                             "LEFT JOIN " & _
                                             "( " & _
                                                 "SELECT " & _
                                                    " hremployee_reportto.employeeunkid AS EmpId " & _
                                                    ",remp.employeecode AS reportingemployeecode " & _
                                                    ",ISNULL(remp.firstname,'''')+'' ''+ISNULL(remp.othername,'''')+'' ''+ISNULL(remp.surname,'''') AS reportingtoemployee " & _
                                                   "FROM hremployee_reportto " & _
                                                   " JOIN hremployee_master AS remp ON hremployee_reportto.reporttoemployeeunkid = remp.employeeunkid " & _
                                                   "WHERE ishierarchy = 1  AND isvoid = 0 " & _
                                             ")AS A ON A.EmpId = hremployee_master.employeeunkid "

                    'S.SANDEEP [09 APR 2015] -- START
                    ' LEFT JOIN hraction_reason_master ON hraction_reason_master.actionreasonunkid = hremployee_master.actionreasonunkid ------ REMOVED
                    ' LEFT JOIN cfcommon_master AS TR ON TR.masterunkid = hremployee_master.actionreasonunkid AND TR.mastertype = '" & clsCommon_Master.enCommonMaster.TERMINATION & "' ----- ADDED
                    'S.SANDEEP [09 APR 2015] -- END

                    Dim StrUpdate As String = " IDM_DATA..hremployee_master.employeeunkid = B.employeeunkid " & _
                                              ",IDM_DATA..hremployee_master.employeecode = B.employeecode " & _
                                              ",IDM_DATA..hremployee_master.title = B.title " & _
                                              ",IDM_DATA..hremployee_master.firstname = B.firstname " & _
                                              ",IDM_DATA..hremployee_master.surname = B.surname " & _
                                              ",IDM_DATA..hremployee_master.othername = B.othername " & _
                                              ",IDM_DATA..hremployee_master.appointeddate = B.appointeddate " & _
                                              ",IDM_DATA..hremployee_master.gendertype = B.gendertype " & _
                                              ",IDM_DATA..hremployee_master.employmenttype = B.employmenttype " & _
                                              ",IDM_DATA..hremployee_master.paytype = B.paytype " & _
                                              ",IDM_DATA..hremployee_master.paypointname = B.paypointname " & _
                                              ",IDM_DATA..hremployee_master.loginname = B.loginname " & _
                                              ",IDM_DATA..hremployee_master.email = B.email " & _
                                              ",IDM_DATA..hremployee_master.displayname = B.displayname " & _
                                              ",IDM_DATA..hremployee_master.shiftname = B.shiftname " & _
                                              ",IDM_DATA..hremployee_master.birthdate = B.birthdate " & _
                                              ",IDM_DATA..hremployee_master.birth_ward = B.birth_ward " & _
                                              ",IDM_DATA..hremployee_master.birthcertificateno = B.birthcertificateno " & _
                                              ",IDM_DATA..hremployee_master.birthstatename = B.birthstatename " & _
                                              ",IDM_DATA..hremployee_master.birthcountryname = B.birthcountryname " & _
                                              ",IDM_DATA..hremployee_master.birthcityname = B.birthcityname " & _
                                              ",IDM_DATA..hremployee_master.birth_village = B.birth_village " & _
                                              ",IDM_DATA..hremployee_master.work_permit_no = B.work_permit_no " & _
                                              ",IDM_DATA..hremployee_master.workcountryname = B.workcountryname " & _
                                              ",IDM_DATA..hremployee_master.work_permit_issue_place = B.work_permit_issue_place " & _
                                              ",IDM_DATA..hremployee_master.work_permit_issue_date = B.work_permit_issue_date " & _
                                              ",IDM_DATA..hremployee_master.work_permit_expiry_date = B.work_permit_expiry_date " & _
                                              ",IDM_DATA..hremployee_master.complexionname = B.complexionname " & _
                                              ",IDM_DATA..hremployee_master.bloodgroupname = B.bloodgroupname " & _
                                              ",IDM_DATA..hremployee_master.eyecolorname = B.eyecolorname " & _
                                              ",IDM_DATA..hremployee_master.nationalityname = B.nationalityname " & _
                                              ",IDM_DATA..hremployee_master.ethnicityname = B.ethnicityname " & _
                                              ",IDM_DATA..hremployee_master.religionname = B.religionname " & _
                                              ",IDM_DATA..hremployee_master.hairname = B.hairname " & _
                                              ",IDM_DATA..hremployee_master.language1name = B.language1name " & _
                                              ",IDM_DATA..hremployee_master.language2name = B.language2name " & _
                                              ",IDM_DATA..hremployee_master.language3name = B.language3name " & _
                                              ",IDM_DATA..hremployee_master.language4name = B.language4name " & _
                                              ",IDM_DATA..hremployee_master.extra_tel_no = B.extra_tel_no " & _
                                              ",IDM_DATA..hremployee_master.height = B.height " & _
                                              ",IDM_DATA..hremployee_master.weight = B.weight " & _
                                              ",IDM_DATA..hremployee_master.maritalstatusname = B.maritalstatusname " & _
                                              ",IDM_DATA..hremployee_master.anniversary_date = B.anniversary_date " & _
                                              ",IDM_DATA..hremployee_master.sports_hobbies = B.sports_hobbies " & _
                                              ",IDM_DATA..hremployee_master.present_address1 = B.present_address1 " & _
                                              ",IDM_DATA..hremployee_master.present_address2 = B.present_address2 " & _
                                              ",IDM_DATA..hremployee_master.present_countryname = B.present_countryname " & _
                                              ",IDM_DATA..hremployee_master.present_postcodename = B.present_postcodename " & _
                                              ",IDM_DATA..hremployee_master.present_statename = B.present_statename " & _
                                              ",IDM_DATA..hremployee_master.present_provicnce = B.present_provicnce " & _
                                              ",IDM_DATA..hremployee_master.present_post_townname = B.present_post_townname " & _
                                              ",IDM_DATA..hremployee_master.present_road = B.present_road " & _
                                              ",IDM_DATA..hremployee_master.present_estate = B.present_estate " & _
                                              ",IDM_DATA..hremployee_master.present_plotNo = B.present_plotNo " & _
                                              ",IDM_DATA..hremployee_master.present_mobile = B.present_mobile " & _
                                              ",IDM_DATA..hremployee_master.present_alternateno = B.present_alternateno " & _
                                              ",IDM_DATA..hremployee_master.present_tel_no = B.present_tel_no " & _
                                              ",IDM_DATA..hremployee_master.present_fax = B.present_fax " & _
                                              ",IDM_DATA..hremployee_master.present_email = B.present_email " & _
                                              ",IDM_DATA..hremployee_master.domicile_address1 = B.domicile_address1 " & _
                                              ",IDM_DATA..hremployee_master.domicile_address2 = B.domicile_address2 " & _
                                              ",IDM_DATA..hremployee_master.domicile_countryname = B.domicile_countryname " & _
                                              ",IDM_DATA..hremployee_master.domicile_postcodename = B.domicile_postcodename " & _
                                              ",IDM_DATA..hremployee_master.domicile_statename = B.domicile_statename " & _
                                              ",IDM_DATA..hremployee_master.domicile_provicnce = B.domicile_provicnce " & _
                                              ",IDM_DATA..hremployee_master.domicile_post_townname = B.domicile_post_townname " & _
                                              ",IDM_DATA..hremployee_master.domicile_road = B.domicile_road " & _
                                              ",IDM_DATA..hremployee_master.domicile_estate = B.domicile_estate " & _
                                              ",IDM_DATA..hremployee_master.domicile_plotno = B.domicile_plotno " & _
                                              ",IDM_DATA..hremployee_master.domicile_mobile = B.domicile_mobile " & _
                                              ",IDM_DATA..hremployee_master.domicile_alternateno = B.domicile_alternateno " & _
                                              ",IDM_DATA..hremployee_master.domicile_tel_no = B.domicile_tel_no " & _
                                              ",IDM_DATA..hremployee_master.domicile_fax = B.domicile_fax " & _
                                              ",IDM_DATA..hremployee_master.domicile_email = B.domicile_email " & _
                                              ",IDM_DATA..hremployee_master.emer_con_firstname = B.emer_con_firstname " & _
                                              ",IDM_DATA..hremployee_master.emer_con_lastname = B.emer_con_lastname " & _
                                              ",IDM_DATA..hremployee_master.emer_con_address = B.emer_con_address " & _
                                              ",IDM_DATA..hremployee_master.emer_con_countryname = B.emer_con_countryname " & _
                                              ",IDM_DATA..hremployee_master.emer_con_postcodename = B.emer_con_postcodename " & _
                                              ",IDM_DATA..hremployee_master.emer_con_state = B.emer_con_state " & _
                                              ",IDM_DATA..hremployee_master.emer_con_statename = B.emer_con_statename " & _
                                              ",IDM_DATA..hremployee_master.emer_con_provicnce = B.emer_con_provicnce " & _
                                              ",IDM_DATA..hremployee_master.emer_con_post_townname = B.emer_con_post_townname " & _
                                              ",IDM_DATA..hremployee_master.emer_con_road = B.emer_con_road " & _
                                              ",IDM_DATA..hremployee_master.emer_con_estate = B.emer_con_estate " & _
                                              ",IDM_DATA..hremployee_master.emer_con_plotno = B.emer_con_plotno " & _
                                              ",IDM_DATA..hremployee_master.emer_con_mobile = B.emer_con_mobile " & _
                                              ",IDM_DATA..hremployee_master.emer_con_alternateno = B.emer_con_alternateno " & _
                                              ",IDM_DATA..hremployee_master.emer_con_tel_no = B.emer_con_tel_no " & _
                                              ",IDM_DATA..hremployee_master.emer_con_fax = B.emer_con_fax " & _
                                              ",IDM_DATA..hremployee_master.emer_con_email = B.emer_con_email " & _
                                              ",IDM_DATA..hremployee_master.stationname = B.stationname " & _
                                              ",IDM_DATA..hremployee_master.deptgroupname = B.deptgroupname " & _
                                              ",IDM_DATA..hremployee_master.departmentname = B.departmentname " & _
                                              ",IDM_DATA..hremployee_master.sectiongroupname = B.sectiongroupname " & _
                                              ",IDM_DATA..hremployee_master.sectionname = B.sectionname " & _
                                              ",IDM_DATA..hremployee_master.unitgroupname = B.unitgroupname " & _
                                              ",IDM_DATA..hremployee_master.unitname = B.unitname " & _
                                              ",IDM_DATA..hremployee_master.teamname = B.teamname " & _
                                              ",IDM_DATA..hremployee_master.jobgroupname = B.jobgroupname " & _
                                              ",IDM_DATA..hremployee_master.jobname = B.jobname " & _
                                              ",IDM_DATA..hremployee_master.gradegroupname = B.gradegroupname " & _
                                              ",IDM_DATA..hremployee_master.gradename = B.gradename " & _
                                              ",IDM_DATA..hremployee_master.gradelevelname = B.gradelevelname " & _
                                              ",IDM_DATA..hremployee_master.classgroupname = B.classgroupname " & _
                                              ",IDM_DATA..hremployee_master.classname = B.classname " & _
                                              ",IDM_DATA..hremployee_master.costcentername = B.costcentername " & _
                                              ",IDM_DATA..hremployee_master.tranhedname = B.tranhedname " & _
                                              ",IDM_DATA..hremployee_master.actionreasonname = B.actionreasonname " & _
                                              ",IDM_DATA..hremployee_master.suspended_from_date = B.suspended_from_date " & _
                                              ",IDM_DATA..hremployee_master.suspended_to_date = B.suspended_to_date " & _
                                              ",IDM_DATA..hremployee_master.probation_from_date = B.probation_from_date " & _
                                              ",IDM_DATA..hremployee_master.probation_to_date = B.probation_to_date " & _
                                              ",IDM_DATA..hremployee_master.termination_from_date = B.termination_from_date " & _
                                              ",IDM_DATA..hremployee_master.termination_to_date = B.termination_to_date " & _
                                              ",IDM_DATA..hremployee_master.remark = B.remark " & _
                                              ",IDM_DATA..hremployee_master.emer_con_firstname2 = B.emer_con_firstname2 " & _
                                              ",IDM_DATA..hremployee_master.emer_con_lastname2 = B.emer_con_lastname2 " & _
                                              ",IDM_DATA..hremployee_master.emer_con_address2 = B.emer_con_address2 " & _
                                              ",IDM_DATA..hremployee_master.emer_con_countryname2 = B.emer_con_countryname2 " & _
                                              ",IDM_DATA..hremployee_master.emer_con_postcodename2 = B.emer_con_postcodename2 " & _
                                              ",IDM_DATA..hremployee_master.emer_con_statename2 = B.emer_con_statename2 " & _
                                              ",IDM_DATA..hremployee_master.emer_con_provicnce2 = B.emer_con_provicnce2 " & _
                                              ",IDM_DATA..hremployee_master.emer_con_post_townname2 = B.emer_con_post_townname2 " & _
                                              ",IDM_DATA..hremployee_master.emer_con_road2 = B.emer_con_road2 " & _
                                              ",IDM_DATA..hremployee_master.emer_con_estate2 = B.emer_con_estate2 " & _
                                              ",IDM_DATA..hremployee_master.emer_con_plotno2 = B.emer_con_plotno2 " & _
                                              ",IDM_DATA..hremployee_master.emer_con_mobile2 = B.emer_con_mobile2 " & _
                                              ",IDM_DATA..hremployee_master.emer_con_alternateno2 = B.emer_con_alternateno2 " & _
                                              ",IDM_DATA..hremployee_master.emer_con_tel_no2 = B.emer_con_tel_no2 " & _
                                              ",IDM_DATA..hremployee_master.emer_con_fax2 = B.emer_con_fax2 " & _
                                              ",IDM_DATA..hremployee_master.emer_con_email2 = B.emer_con_email2 " & _
                                              ",IDM_DATA..hremployee_master.emer_con_firstname3 = B.emer_con_firstname3 " & _
                                              ",IDM_DATA..hremployee_master.emer_con_lastname3 = B.emer_con_lastname3 " & _
                                              ",IDM_DATA..hremployee_master.emer_con_address3 = B.emer_con_address3 " & _
                                              ",IDM_DATA..hremployee_master.emer_con_countryname3 = B.emer_con_countryname3 " & _
                                              ",IDM_DATA..hremployee_master.emer_con_postcodename3 = B.emer_con_postcodename3 " & _
                                              ",IDM_DATA..hremployee_master.emer_con_statename3 = B.emer_con_statename3 " & _
                                              ",IDM_DATA..hremployee_master.emer_con_provicnce3 = B.emer_con_provicnce3 " & _
                                              ",IDM_DATA..hremployee_master.emer_con_post_townname3 = B.emer_con_post_townname3 " & _
                                              ",IDM_DATA..hremployee_master.emer_con_road3 = B.emer_con_road3 " & _
                                              ",IDM_DATA..hremployee_master.emer_con_estate3 = B.emer_con_estate3 " & _
                                              ",IDM_DATA..hremployee_master.emer_con_plotno3 = B.emer_con_plotno3 " & _
                                              ",IDM_DATA..hremployee_master.emer_con_mobile3 = B.emer_con_mobile3 " & _
                                              ",IDM_DATA..hremployee_master.emer_con_alternateno3 = B.emer_con_alternateno3 " & _
                                              ",IDM_DATA..hremployee_master.emer_con_tel_no3 = B.emer_con_tel_no3 " & _
                                              ",IDM_DATA..hremployee_master.emer_con_fax3 = B.emer_con_fax3 " & _
                                              ",IDM_DATA..hremployee_master.emer_con_email3 = B.emer_con_email3 " & _
                                              ",IDM_DATA..hremployee_master.isclear = B.isclear " & _
                                              ",IDM_DATA..hremployee_master.confirmation_date = B.confirmation_date " & _
                                              ",IDM_DATA..hremployee_master.empl_enddate = B.empl_enddate " & _
                                              ",IDM_DATA..hremployee_master.allocationreason = B.allocationreason " & _
                                              ",IDM_DATA..hremployee_master.reinstatement_date = B.reinstatement_date " & _
                                              ",IDM_DATA..hremployee_master.isexclude_payroll = B.isexclude_payroll " & _
                                              ",IDM_DATA..hremployee_master.isapproved = B.isapproved " & _
                                              ",IDM_DATA..hremployee_master.activestatus = B.activestatus " & _
                                              ",IDM_DATA..hremployee_master.titleunkid = B.titleunkid " & _
                                              ",IDM_DATA..hremployee_master.gender = B.gender " & _
                                              ",IDM_DATA..hremployee_master.employmenttypeunkid = B.employmenttypeunkid " & _
                                              ",IDM_DATA..hremployee_master.paytypeunkid = B.paytypeunkid " & _
                                              ",IDM_DATA..hremployee_master.paypointunkid = B.paypointunkid " & _
                                              ",IDM_DATA..hremployee_master.shiftunkid = B.shiftunkid " & _
                                              ",IDM_DATA..hremployee_master.birthstateunkid = B.birthstateunkid " & _
                                              ",IDM_DATA..hremployee_master.birthcountryunkid = B.birthcountryunkid " & _
                                              ",IDM_DATA..hremployee_master.birthcityunkid = B.birthcityunkid " & _
                                              ",IDM_DATA..hremployee_master.workcountryunkid = B.workcountryunkid " & _
                                              ",IDM_DATA..hremployee_master.complexionunkid = B.complexionunkid " & _
                                              ",IDM_DATA..hremployee_master.bloodgroupunkid = B.bloodgroupunkid " & _
                                              ",IDM_DATA..hremployee_master.eyecolorunkid = B.eyecolorunkid " & _
                                              ",IDM_DATA..hremployee_master.nationalityunkid = B.nationalityunkid " & _
                                              ",IDM_DATA..hremployee_master.ethnicityunkid = B.ethnicityunkid " & _
                                              ",IDM_DATA..hremployee_master.religionunkid = B.religionunkid " & _
                                              ",IDM_DATA..hremployee_master.hairunkid = B.hairunkid " & _
                                              ",IDM_DATA..hremployee_master.language1unkid = B.language1unkid " & _
                                              ",IDM_DATA..hremployee_master.language2unkid = B.language2unkid " & _
                                              ",IDM_DATA..hremployee_master.language3unkid = B.language3unkid " & _
                                              ",IDM_DATA..hremployee_master.language4unkid = B.language4unkid " & _
                                              ",IDM_DATA..hremployee_master.maritalstatusunkid = B.maritalstatusunkid " & _
                                              ",IDM_DATA..hremployee_master.present_countryunkid = B.present_countryunkid " & _
                                              ",IDM_DATA..hremployee_master.present_postcodeunkid = B.present_postcodeunkid " & _
                                              ",IDM_DATA..hremployee_master.present_stateunkid = B.present_stateunkid " & _
                                              ",IDM_DATA..hremployee_master.present_post_townunkid = B.present_post_townunkid " & _
                                              ",IDM_DATA..hremployee_master.domicile_countryunkid = B.domicile_countryunkid " & _
                                              ",IDM_DATA..hremployee_master.domicile_postcodeunkid = B.domicile_postcodeunkid " & _
                                              ",IDM_DATA..hremployee_master.domicile_stateunkid = B.domicile_stateunkid " & _
                                              ",IDM_DATA..hremployee_master.domicile_post_townunkid = B.domicile_post_townunkid " & _
                                              ",IDM_DATA..hremployee_master.emer_con_countryunkid = B.emer_con_countryunkid " & _
                                              ",IDM_DATA..hremployee_master.emer_con_postcodeunkid = B.emer_con_postcodeunkid " & _
                                              ",IDM_DATA..hremployee_master.emer_con_post_townunkid = B.emer_con_post_townunkid " & _
                                              ",IDM_DATA..hremployee_master.stationunkid = B.stationunkid " & _
                                              ",IDM_DATA..hremployee_master.deptgroupunkid = B.deptgroupunkid " & _
                                              ",IDM_DATA..hremployee_master.departmentunkid = B.departmentunkid " & _
                                              ",IDM_DATA..hremployee_master.sectiongroupunkid = B.sectiongroupunkid " & _
                                              ",IDM_DATA..hremployee_master.sectionunkid = B.sectionunkid " & _
                                              ",IDM_DATA..hremployee_master.unitgroupunkid = B.unitgroupunkid " & _
                                              ",IDM_DATA..hremployee_master.unitunkid = B.unitunkid " & _
                                              ",IDM_DATA..hremployee_master.teamunkid = B.teamunkid " & _
                                              ",IDM_DATA..hremployee_master.jobgroupunkid = B.jobgroupunkid " & _
                                              ",IDM_DATA..hremployee_master.jobunkid = B.jobunkid " & _
                                              ",IDM_DATA..hremployee_master.gradegroupunkid = B.gradegroupunkid " & _
                                              ",IDM_DATA..hremployee_master.gradeunkid = B.gradeunkid " & _
                                              ",IDM_DATA..hremployee_master.gradelevelunkid = B.gradelevelunkid " & _
                                              ",IDM_DATA..hremployee_master.classgroupunkid = B.classgroupunkid " & _
                                              ",IDM_DATA..hremployee_master.classunkid = B.classunkid " & _
                                              ",IDM_DATA..hremployee_master.costcenterunkid = B.costcenterunkid " & _
                                              ",IDM_DATA..hremployee_master.tranhedunkid = B.tranhedunkid " & _
                                              ",IDM_DATA..hremployee_master.actionreasonunkid = B.actionreasonunkid " & _
                                              ",IDM_DATA..hremployee_master.emer_con_countryunkid2 = B.emer_con_countryunkid2 " & _
                                              ",IDM_DATA..hremployee_master.emer_con_postcodeunkid2 = B.emer_con_postcodeunkid2 " & _
                                              ",IDM_DATA..hremployee_master.emer_con_state2 = B.emer_con_state2 " & _
                                              ",IDM_DATA..hremployee_master.emer_con_post_townunkid2 = B.emer_con_post_townunkid2 " & _
                                              ",IDM_DATA..hremployee_master.emer_con_countryunkid3 = B.emer_con_countryunkid3 " & _
                                              ",IDM_DATA..hremployee_master.emer_con_postcodeunkid3 = B.emer_con_postcodeunkid3 " & _
                                              ",IDM_DATA..hremployee_master.emer_con_state3 = B.emer_con_state3 " & _
                                              ",IDM_DATA..hremployee_master.emer_con_post_townunkid3 = B.emer_con_post_townunkid3 " & _
                                              ",IDM_DATA..hremployee_master.companyunkid = B.companyunkid " & _
                                              ",IDM_DATA..hremployee_master.reportingemployeecode = B.reportingemployeecode " & _
                                              ",IDM_DATA..hremployee_master.reportingtoemployee = B.reportingtoemployee " & _
                                              ",IDM_DATA..hremployee_master.modify_date = B.modify_date "

                    Dim sUpdateCol As String = "UPDATE(employeeunkid) OR  UPDATE(employeecode) OR  UPDATE(titleunkid) OR  UPDATE(firstname) OR  UPDATE(surname) OR  UPDATE(othername) OR  UPDATE(appointeddate) OR  UPDATE(gender) OR  UPDATE(employmenttypeunkid) OR  UPDATE(paytypeunkid) OR  UPDATE(paypointunkid) OR  UPDATE(loginname) OR  UPDATE(email) OR  UPDATE(displayname) OR  UPDATE(shiftunkid) OR  UPDATE(birthdate) OR  UPDATE(birth_ward) OR  UPDATE(birthcertificateno) OR  UPDATE(birthstateunkid) OR  UPDATE(birthcountryunkid) OR  UPDATE(birthcityunkid) OR  UPDATE(birth_village) OR  UPDATE(work_permit_no) OR  UPDATE(workcountryunkid) OR  UPDATE(work_permit_issue_place) OR  UPDATE(work_permit_issue_date) OR  UPDATE(work_permit_expiry_date) OR  UPDATE(complexionunkid) OR  UPDATE(bloodgroupunkid) OR  UPDATE(eyecolorunkid) OR  UPDATE(nationalityunkid) OR  UPDATE(ethnicityunkid) OR  UPDATE(religionunkid) OR  UPDATE(hairunkid) OR  UPDATE(language1unkid) OR  UPDATE(language2unkid) OR  UPDATE(language3unkid) OR  UPDATE(language4unkid) OR  UPDATE(extra_tel_no) OR  UPDATE(height) OR  UPDATE(weight) OR  UPDATE(maritalstatusunkid) OR  UPDATE(anniversary_date) OR  UPDATE(sports_hobbies) OR  UPDATE(present_address1) OR  UPDATE(present_address2) OR  UPDATE(present_countryunkid) OR  UPDATE(present_postcodeunkid) OR  UPDATE(present_stateunkid) OR  UPDATE(present_provicnce) OR  UPDATE(present_post_townunkid) OR  UPDATE(present_road) OR  UPDATE(present_estate) OR  UPDATE(present_plotNo) OR  UPDATE(present_mobile) OR  UPDATE(present_alternateno) OR  UPDATE(present_tel_no) OR  UPDATE(present_fax) OR  UPDATE(present_email) OR  UPDATE(domicile_address1) OR  UPDATE(domicile_address2) OR  UPDATE(domicile_countryunkid) OR  UPDATE(domicile_postcodeunkid) OR  UPDATE(domicile_stateunkid) OR  UPDATE(domicile_provicnce) OR  UPDATE(domicile_post_townunkid) OR  UPDATE(domicile_road) OR  UPDATE(domicile_estate) OR  UPDATE(domicile_plotno) OR  UPDATE(domicile_mobile) OR  UPDATE(domicile_alternateno) OR  UPDATE(domicile_tel_no) OR  UPDATE(domicile_fax) OR  UPDATE(domicile_email) OR  UPDATE(emer_con_firstname) OR  UPDATE(emer_con_lastname) OR  UPDATE(emer_con_address) OR  UPDATE(emer_con_countryunkid) OR  UPDATE(emer_con_postcodeunkid) OR  UPDATE(emer_con_state) OR  UPDATE(emer_con_provicnce) OR  UPDATE(emer_con_post_townunkid) OR  UPDATE(emer_con_road) OR  UPDATE(emer_con_estate) OR  UPDATE(emer_con_plotno) OR  UPDATE(emer_con_mobile) OR  UPDATE(emer_con_alternateno) OR  UPDATE(emer_con_tel_no) OR  UPDATE(emer_con_fax) OR  UPDATE(emer_con_email) OR  UPDATE(stationunkid) OR  UPDATE(deptgroupunkid) OR  UPDATE(departmentunkid) OR  UPDATE(sectionunkid) OR  UPDATE(unitunkid) OR  UPDATE(jobgroupunkid) OR  UPDATE(jobunkid) OR  UPDATE(gradegroupunkid) OR  UPDATE(gradeunkid) OR  UPDATE(gradelevelunkid) OR  UPDATE(accessunkid) OR  UPDATE(classgroupunkid) OR  UPDATE(classunkid) OR  UPDATE(serviceunkid) OR  UPDATE(costcenterunkid) OR  UPDATE(tranhedunkid) OR  UPDATE(actionreasonunkid) OR  UPDATE(suspended_from_date) OR  UPDATE(suspended_to_date) OR  UPDATE(probation_from_date) OR  UPDATE(probation_to_date) OR  UPDATE(termination_from_date) OR  UPDATE(termination_to_date) OR  UPDATE(remark) OR  UPDATE(isactive) OR  UPDATE(emer_con_firstname2) OR  UPDATE(emer_con_lastname2) OR  UPDATE(emer_con_address2) OR  UPDATE(emer_con_countryunkid2) OR  UPDATE(emer_con_postcodeunkid2) OR  UPDATE(emer_con_state2) OR  UPDATE(emer_con_provicnce2) OR  UPDATE(emer_con_post_townunkid2) OR  UPDATE(emer_con_road2) OR  UPDATE(emer_con_estate2) OR  UPDATE(emer_con_plotno2) OR  UPDATE(emer_con_mobile2) OR  UPDATE(emer_con_alternateno2) OR  UPDATE(emer_con_tel_no2) OR  UPDATE(emer_con_fax2) OR  UPDATE(emer_con_email2) OR  UPDATE(emer_con_firstname3) OR  UPDATE(emer_con_lastname3) OR  UPDATE(emer_con_address3) OR  UPDATE(emer_con_countryunkid3) OR  UPDATE(emer_con_postcodeunkid3) OR  UPDATE(emer_con_state3) OR  UPDATE(emer_con_provicnce3) OR  UPDATE(emer_con_post_townunkid3) OR  UPDATE(emer_con_road3) OR  UPDATE(emer_con_estate3) OR  UPDATE(emer_con_plotno3) OR  UPDATE(emer_con_mobile3) OR  UPDATE(emer_con_alternateno3) OR  UPDATE(emer_con_tel_no3) OR  UPDATE(emer_con_fax3) OR  UPDATE(emer_con_email3) OR  UPDATE(sectiongroupunkid) OR  UPDATE(unitgroupunkid) OR  UPDATE(teamunkid) OR  UPDATE(confirmation_date) OR  UPDATE(empl_enddate) OR  UPDATE(isclear) OR  UPDATE(allocationreason) OR  UPDATE(reinstatement_date) OR  UPDATE(isexclude_payroll) OR  UPDATE(isapproved) OR  UPDATE(companyunkid)"

                    Dim StrFields As String = "  employeeunkid , employeecode , title , firstname , surname , othername , appointeddate , gendertype , employmenttype " & _
                                              ", paytype , paypointname , loginname , password , email , displayname , shiftname , birthdate , birth_ward , birthcertificateno " & _
                                              ", birthstatename , birthcountryname , birthcityname , birth_village , work_permit_no , workcountryname , work_permit_issue_place " & _
                                              ", work_permit_issue_date , work_permit_expiry_date , complexionname , bloodgroupname , eyecolorname , nationalityname " & _
                                              ", ethnicityname , religionname , hairname , language1name , language2name , language3name , language4name , extra_tel_no " & _
                                              ", height , weight , maritalstatusname , anniversary_date , sports_hobbies , present_address1 , present_address2 , present_countryname " & _
                                              ", present_postcodename , present_statename , present_provicnce , present_post_townname , present_road , present_estate " & _
                                              ", present_plotNo , present_mobile , present_alternateno , present_tel_no , present_fax , present_email , domicile_address1 " & _
                                              ", domicile_address2 , domicile_countryname , domicile_postcodename , domicile_statename , domicile_provicnce , domicile_post_townname " & _
                                              ", domicile_road , domicile_estate , domicile_plotno , domicile_mobile , domicile_alternateno , domicile_tel_no , domicile_fax " & _
                                              ", domicile_email , emer_con_firstname , emer_con_lastname , emer_con_address , emer_con_countryname , emer_con_postcodename , emer_con_state " & _
                                              ", emer_con_statename , emer_con_provicnce , emer_con_post_townname , emer_con_road , emer_con_estate , emer_con_plotno , emer_con_mobile " & _
                                              ", emer_con_alternateno , emer_con_tel_no , emer_con_fax , emer_con_email , stationname , deptgroupname , departmentname , sectiongroupname " & _
                                              ", sectionname , unitgroupname , unitname , teamname , jobgroupname , jobname , gradegroupname , gradename , gradelevelname , classgroupname " & _
                                              ", classname , costcentername , tranhedname , actionreasonname , suspended_from_date , suspended_to_date , probation_from_date , probation_to_date " & _
                                              ", termination_from_date , termination_to_date , remark , emer_con_firstname2 , emer_con_lastname2 , emer_con_address2 , emer_con_countryname2 " & _
                                              ", emer_con_postcodename2 , emer_con_statename2 , emer_con_provicnce2 , emer_con_post_townname2 , emer_con_road2 , emer_con_estate2 , emer_con_plotno2 " & _
                                              ", emer_con_mobile2 , emer_con_alternateno2 , emer_con_tel_no2 , emer_con_fax2 , emer_con_email2 , emer_con_firstname3 , emer_con_lastname3 " & _
                                              ", emer_con_address3 , emer_con_countryname3 , emer_con_postcodename3 , emer_con_statename3 , emer_con_provicnce3 , emer_con_post_townname3 " & _
                                              ", emer_con_road3 , emer_con_estate3 , emer_con_plotno3 , emer_con_mobile3 , emer_con_alternateno3 , emer_con_tel_no3 , emer_con_fax3 " & _
                                              ", emer_con_email3 , isclear , confirmation_date , empl_enddate , allocationreason , reinstatement_date , isexclude_payroll , isapproved " & _
                                              ", activestatus , titleunkid , gender , employmenttypeunkid , paytypeunkid , paypointunkid , shiftunkid , birthstateunkid , birthcountryunkid " & _
                                              ", birthcityunkid , workcountryunkid , complexionunkid , bloodgroupunkid , eyecolorunkid , nationalityunkid , ethnicityunkid , religionunkid " & _
                                              ", hairunkid , language1unkid , language2unkid , language3unkid , language4unkid , maritalstatusunkid , present_countryunkid , present_postcodeunkid " & _
                                              ", present_stateunkid , present_post_townunkid , domicile_countryunkid , domicile_postcodeunkid , domicile_stateunkid , domicile_post_townunkid " & _
                                              ", emer_con_countryunkid , emer_con_postcodeunkid , emer_con_post_townunkid , stationunkid , deptgroupunkid , departmentunkid , sectiongroupunkid " & _
                                              ", sectionunkid , unitgroupunkid , unitunkid , teamunkid , jobgroupunkid , jobunkid , gradegroupunkid , gradeunkid , gradelevelunkid , classgroupunkid " & _
                                              ", classunkid , costcenterunkid , tranhedunkid , actionreasonunkid , emer_con_countryunkid2 , emer_con_postcodeunkid2 , emer_con_state2 " & _
                                              ", emer_con_post_townunkid2 , emer_con_countryunkid3 , emer_con_postcodeunkid3 , emer_con_state3 , emer_con_post_townunkid3 , companyunkid " & _
                                              ", reportingemployeecode , reportingtoemployee , modify_date "

                    strQ = "IF EXISTS(SELECT * FROM sys.objects WHERE name = 'trg_IU_Emp' AND type = 'TR') " & _
                           "BEGIN " & _
                           "    DROP TRIGGER trg_IU_Emp " & _
                           "END "

                    objDataOperation.ExecNonQuery(strQ)

                    If objDataOperation.ErrorMessage <> "" Then
                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If

                    strQ = "IF NOT EXISTS (SELECT * FROM sys.triggers WHERE object_id = OBJECT_ID(N'[dbo].[trg_IU_Emp]')) " & _
                           "EXEC dbo.sp_executesql @statement = N'CREATE TRIGGER [dbo].[trg_IU_Emp] ON [dbo].[hremployee_master] " & _
                           "AFTER INSERT,UPDATE " & _
                           "AS " & _
                           "BEGIN " & _
                           "    IF EXISTS (SELECT * FROM sys.databases WHERE name = ''IDM_DATA'') " & _
                           "    BEGIN " & _
                           "        SELECT * INTO #TempUpdate FROM INSERTED " & _
                           "        IF EXISTS(SELECT employeecode FROM hremployee_master WHERE employeecode <> '''') " & _
                           "        BEGIN " & _
                           "        IF NOT EXISTS (SELECT * FROM IDM_DATA..hremployee_master JOIN #TempUpdate ON #TempUpdate.employeeunkid = IDM_DATA..hremployee_master.employeeunkid " & _
                           "                       AND #TempUpdate.companyunkid = IDM_DATA..hremployee_master.companyunkid) " & _
                           "        BEGIN " & _
                           "            INSERT  INTO IDM_DATA..hremployee_master " & _
                           "                (" & StrFields & ") " & _
                           "            SELECT " & StrValue & " " & _
                           "            FROM hremployee_master " & _
                           "            " & StrJoins & " " & _
                           "            INNER JOIN #TempUpdate ON #TempUpdate.employeeunkid = hremployee_master.employeeunkid " & _
                           "            AND #TempUpdate.companyunkid = hremployee_master.companyunkid " & _
                           "            END " & _
                           "        END " & _
                           "        IF EXISTS (SELECT * FROM IDM_DATA..hremployee_master JOIN #TempUpdate ON #TempUpdate.employeeunkid = IDM_DATA..hremployee_master.employeeunkid AND #TempUpdate.companyunkid = IDM_DATA..hremployee_master.companyunkid) " & _
                           "        BEGIN " & _
                           "            IF (" & sUpdateCol & ") " & _
                           "            BEGIN " & _
                           "                UPDATE  IDM_DATA..hremployee_master SET " & _
                           "                        " & StrUpdate & " " & _
                           "                FROM ( SELECT " & _
                           "                        " & StrValue & " " & _
                           "                       FROM hremployee_master " & _
                           "                        " & StrJoins & " " & _
                           "                       INNER JOIN #TempUpdate ON #TempUpdate.employeeunkid = hremployee_master.employeeunkid " & _
                           "                          AND #TempUpdate.companyunkid = hremployee_master.companyunkid " & _
                           "                     ) AS B WHERE B.employeeunkid = IDM_DATA..hremployee_master.employeeunkid AND B.companyunkid = IDM_DATA..hremployee_master.companyunkid " & _
                           "            END " & _
                           "        END " & _
                           "        DROP TABLE #TempUpdate " & _
                           "      END " & _
                           "END' "

                    objDataOperation.ExecNonQuery(strQ)

                    If objDataOperation.ErrorMessage <> "" Then
                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If

                    strQ = "IF EXISTS (SELECT * FROM sys.objects WHERE name = 'trg_IUD_Reporting' AND type = 'TR') " & _
                           "BEGIN " & _
                           "    DROP TRIGGER trg_IUD_Reporting " & _
                           "END "

                    objDataOperation.ExecNonQuery(strQ)

                    If objDataOperation.ErrorMessage <> "" Then
                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If

                    strQ = "IF NOT EXISTS (SELECT * FROM sys.triggers WHERE object_id = OBJECT_ID(N'[dbo].[trg_IUD_Reporting]')) " & _
                           "EXEC dbo.sp_executesql @statement = N'CREATE TRIGGER [dbo].[trg_IUD_Reporting] ON [dbo].[hremployee_reportto] " & _
                           "AFTER INSERT,UPDATE,DELETE " & _
                           "AS " & _
                           "BEGIN " & _
                           "    IF EXISTS(SELECT * FROM sys.databases WHERE name = ''IDM_DATA'') " & _
                           "    BEGIN " & _
                           "        DECLARE @CompId AS INT " & _
                           "        SET @CompId = (SELECT companyunkid FROM hrmsConfiguration..cffinancial_year_tran WHERE isclosed = 0 AND UPPER(database_name) = (SELECT UPPER(DB_NAME()))) " & _
                           "        SELECT * INTO #TempReport FROM INSERTED " & _
                           "        UPDATE IDM_DATA..hremployee_master " & _
                           "            SET reportingemployeecode = A.reportingemployeecode,reportingtoemployee= A.reportingtoemployee, modify_date = GetDate() " & _
                           "        FROM " & _
                           "        ( " & _
                           "            SELECT " & _
                           "                 #TempReport.employeeunkid AS EmpId " & _
                           "                ,remp.employeecode AS reportingemployeecode " & _
                           "                ,ISNULL(remp.firstname,'''')+'' ''+ISNULL(remp.othername,'''')+'' ''+ISNULL(remp.surname,'''') AS reportingtoemployee " & _
                           "            FROM #TempReport " & _
                           "                JOIN hremployee_master AS remp ON #TempReport.reporttoemployeeunkid = remp.employeeunkid " & _
                           "            WHERE ishierarchy = 1  AND isvoid = 0 " & _
                           "        ) AS A WHERE A.EmpId = IDM_DATA..hremployee_master.employeeunkid AND IDM_DATA..hremployee_master.companyunkid = @CompId " & _
                           "        DROP TABLE #TempReport " & _
                           "    END " & _
                           "END' "

                    objDataOperation.ExecNonQuery(strQ)

                    If objDataOperation.ErrorMessage <> "" Then
                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If
                End If
            End If
            Return True
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "", mstrModuleName)
        Finally
        End Try
    End Function
    'S.SANDEEP [ 25 JUNE 2013 ] -- END

#End Region

#Region " Form's Events "

    Private Sub frmGenerateYear_Wizard_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        Try
            objCloseYear = Nothing
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmGenerateYear_Wizard_FormClosed", mstrModuleName)
        End Try
    End Sub

    'Sohail (04 Mar 2011) -- Start
    Private Sub frmGenerateYear_Wizard_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown
        Try
            Select Case e.KeyCode
                Case Keys.Return
                    SendKeys.Send("{TAB}")
            End Select
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmGenerateYear_Wizard_KeyDown", mstrModuleName)
        End Try
    End Sub
    'Sohail (04 Mar 2011) -- End

    Private Sub frmGenerateYear_Wizard_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        objCloseYear = New clsCloseYear
        Try
            Call Set_Logo(Me, gApplicationType)

            'Anjan (02 Sep 2011)-Start
            'Issue : Including Language Settings.
            Language.setLanguage(Me.Name)
            Call OtherSettings()
            'Anjan (02 Sep 2011)-End

            'Sohail (03 Nov 2010) -- Start
            'lblStep6_Desc.Text = "List of Payslip Transactions for each employee which is being carry forwarded to the new year. " & _
            '                    "This list contains unpaid salary, unpaid loan/advance, unreceived loan installment, unpaid savings"
            lblStep3_Desc.Text = "List of Loan, Advance and Savings Transactions for each employee which are being carry forwarded to the new year. This list contains Unpaid Loan/Advance, Unreceived Loan Installment, Unpaid Employee Savings."
            lblStep4_Desc.Text = "List of Eligible Applicants which are  imported to employee master will not be carry forward to the new year."
            lblStep6_Desc.Text = "List of Payslip Transactions for each employee which are being carry forwarded to the new year. This list contains Unpaid Salary."
            'Sohail (03 Nov 2010) -- End
            
            Cursor.Current = Cursors.WaitCursor 'Sohail (01 Dec 2010)

            'Sohail (09 JUL 2015) -- Start
            'Enhancement : Now do not copy previous year whole table. Just coopy Last period ED Slab which will be mandatory from now.
            chkCopyPreviousEDSlab.Checked = True
            chkCopyPreviousEDSlab.Enabled = False
            'Sohail (09 JUL 2015) -- End 
            Call SetDate()
            Call FillLeaveList()
            Call FillTransactionList()
            'Sohail (03 Nov 2010) -- Start
            Call FillLoanSavings()
            Call FillApplicantList()
            'Sohail (03 Nov 2010) -- End
            Cursor.Current = Cursors.Default 'Sohail (01 Dec 2010)


            'Sohail (06 Jan 2016) -- Start
            'Enhancement - Show Close Year Process Logs on Close Year Wizard.
            rtbCloseYearLog.Location = lvTransaction.Location
            rtbCloseYearLog.Size = lvTransaction.Size
            rtbCloseYearLog.Text = ""
            objlblProgress.Location = lblStep6_Desc.Location
            objlblProgress.Size = lblStep6_Desc.Size
            objlblProgress.Text = ""
            'Sohail (06 Jan 2016) -- End


            'Pinkal (24-May-2013) -- Start
            'Enhancement : TRA Changes

            'S.SANDEEP [ 08 June 2011 ] -- START
            'ISSUE : LEAVE C/F OPTIONS
            'gbLeave_BFOption.Enabled = False
            'Select Case ConfigParameter._Object._LeaveCF_OptionId
            '    Case 1  'NO ACTION
            '        rdNoAction.Checked = True
            '        txtMaxLeave.Visible = False
            '    Case 2  'SET ZERO
            '        rdSetZero.Checked = True
            '        txtMaxLeave.Visible = False
            '    Case 3  'SET MAX
            '        rdSetMaximum.Checked = True
            '        txtMaxLeave.Visible = True
            '        txtMaxLeave.Text = CStr(ConfigParameter._Object._LeaveMaxValue)
            'End Select
            'S.SANDEEP [ 08 June 2011 ] -- END

            'Pinkal (24-May-2013) -- End

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmGenerateYear_Wizard_Load", mstrModuleName)
        End Try
    End Sub
    'Anjan (02 Sep 2011)-Start
    'Issue : Including Language Settings.
    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsCloseYear.SetMessages()
            objfrm._Other_ModuleNames = "clsCloseYear"
            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub
    'Anjan (02 Sep 2011)-End 

#End Region

#Region " ComboBox's Events "

#End Region

#Region " Button's Events "

#End Region

#Region " Listview's Events "

#End Region

#Region " Other Control's Events "

    Private Sub wzGenerateNewYear_BeforeSwitchPages(ByVal sender As System.Object, ByVal e As eZee.Common.eZeeWizard.BeforeSwitchPagesEventArgs) Handles wzGenerateNewYear.BeforeSwitchPages
        Dim blnFlag As Boolean = False
        Try
            Select Case e.OldIndex
                Case wzGenerateNewYear.Pages.IndexOf(wpWelcome)
                    If e.NewIndex > e.OldIndex Then
                        picSide_Step1_Check.Visible = True
                    End If

                Case wzGenerateNewYear.Pages.IndexOf(wpYearinfo)
                    If e.NewIndex > e.OldIndex Then
                        picSide_Step2_Check.Visible = True
                        'Sohail (04 Mar 2011) -- Start
                        If txtCode.Text.Trim = "" Then
                            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Please enter New Period Code."), enMsgBoxStyle.Information)
                            txtCode.Focus()
                            e.Cancel = True
                            Exit Try
                        ElseIf txtName.Text.Trim = "" Then
                            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Please enter New Period Name."), enMsgBoxStyle.Information)
                            txtName.Focus()
                            e.Cancel = True
                            Exit Try
                        Else
                            Dim objPeriod As New clscommom_period_Tran
                            objPeriod._Modulerefid = enModuleReference.Payroll
                            If objPeriod.isExist(txtCode.Text.Trim) = True Then
                                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "This Period Code is already defined. Please define new Period Code."), enMsgBoxStyle.Information)
                                txtCode.Focus()
                                e.Cancel = True
                                Exit Try
                            ElseIf objPeriod.isExist("", txtName.Text.Trim) = True Then
                                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "This Period Name is already defined. Please define new Period Name."), enMsgBoxStyle.Information)
                                txtName.Focus()
                                e.Cancel = True
                                Exit Try
                            End If
                            objPeriod = Nothing
                        End If
                        'Sohail (04 Mar 2011) -- End


                        'Sohail (18 Jan 2012) -- Start
                        'TRA - ENHANCEMENT
                        If chkCopyPreviousEDSlab.Checked = True Then
                            If (eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 8, "Copy Previous ED Slab is ticked. This will copy Previous Period Slab.") & vbCrLf & vbCrLf & Language.getMessage(mstrModuleName, 12, "Do you want to continue?"), CType(enMsgBoxStyle.Question + enMsgBoxStyle.YesNo, enMsgBoxStyle))) = Windows.Forms.DialogResult.No Then
                                e.Cancel = True
                                Exit Try
                            End If
                        End If
                        'Sohail (18 Jan 2012) -- End
                    Else
                        picSide_Step1_Check.Visible = False
                    End If

                Case wzGenerateNewYear.Pages.IndexOf(wpConfiguration)
                    If e.NewIndex > e.OldIndex Then
                        picSide_Step3_Check.Visible = True
                    Else
                        picSide_Step2_Check.Visible = False
                    End If

                Case wzGenerateNewYear.Pages.IndexOf(wpMasterInformation)
                    If e.NewIndex > e.OldIndex Then
                        picSide_Step4_Check.Visible = True
                    Else
                        picSide_Step3_Check.Visible = False
                    End If

                Case wzGenerateNewYear.Pages.IndexOf(wpLeave_BF)
                    If e.NewIndex > e.OldIndex Then


                        'Pinkal (24-May-2013) -- Start
                        'Enhancement : TRA Changes

                        'Sohail (01 Dec 2010) -- Start
                        'If rdSetMaximum.Checked = True AndAlso txtMaxLeave.Decimal <= 0 Then 'Sohail (11 May 2011)
                        '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Sorry, Maximum Leave should be greater than zero."), enMsgBoxStyle.Information)
                        '    txtMaxLeave.Focus()
                        '    e.Cancel = True
                        '    Exit Sub
                        'End If
                        'Sohail (01 Dec 2010) -- End

                        'Pinkal (24-May-2013) -- End

                        picSide_Step5_Check.Visible = True
                        'Sohail (06 Jan 2016) -- Start
                        'Enhancement - Show Close Year Process Logs on Close Year Wizard.
                        objlblProgress.Visible = False
                        rtbCloseYearLog.Visible = False
                        'Sohail (06 Jan 2016) -- End
                    Else
                        picSide_Step4_Check.Visible = False
                    End If

                Case wzGenerateNewYear.Pages.IndexOf(wpTransaction)
                    If e.NewIndex > e.OldIndex Then
                        If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Are you sure you want to Close this Year?"), CType(enMsgBoxStyle.Question + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.No Then
                            e.Cancel = True
                            Exit Sub
                        End If

                        'Sohail (10 Feb 2012) -- Start
                        'TRA - ENHANCEMENT
                        Dim objConfig = New clsConfigOptions
                        objConfig._Companyunkid = Company._Object._Companyunkid
                        If objConfig._DatabaseExportPath = "" Then
                            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 6, "Database backup ") & vbCrLf & _
                                                     Language.getMessage(mstrModuleName, 7, "path has not been set. ") & vbCrLf & _
                                                   Language.getMessage(mstrModuleName, 11, "Backup of that database(s) cannot be taken.") & vbCrLf & _
                                                  Language.getMessage(mstrModuleName, 9, "You can set the Database backup path from ") & vbCrLf & _
                                                  Language.getMessage(mstrModuleName, 10, "Aruti Configuration -> Option -> Path."))
                            e.Cancel = True
                            Exit Sub
                        End If

                        Cursor.Current = Cursors.WaitCursor

                        'Sohail (06 Jan 2016) -- Start
                        'Enhancement - Show Close Year Process Logs on Close Year Wizard.
                        objlblProgress.Text = ""
                        rtbCloseYearLog.ResetText()
                        objlblProgress.Visible = True
                        rtbCloseYearLog.Visible = True
                        'Dim objAppSettings As New clsApplicationSettings
                        'Dim strFinalPath As String = String.Empty
                        'Dim StrFilePath As String = objAppSettings._ApplicationPath & "Data\Close Year Backup"
                        'objAppSettings = Nothing

                        'If Not System.IO.Directory.Exists(StrFilePath) Then
                        '    System.IO.Directory.CreateDirectory(StrFilePath)
                        'End If

                        ''Pinkal (04-Apr-2013) -- Start
                        ''Enhancement : TRA Changes

                        ''For i As Integer = 0 To 1
                        ''If i = 0 Then
                        ''    eZeeDatabase.change_database(FinancialYear._Object._ConfigDatabaseName)
                        ''    System.IO.Directory.CreateDirectory(StrFilePath & "\" & FinancialYear._Object._ConfigDatabaseName)
                        ''    strFinalPath = StrFilePath & "\" & FinancialYear._Object._ConfigDatabaseName
                        ''ElseIf i = 1 Then
                        ''    eZeeDatabase.change_database(FinancialYear._Object._DatabaseName)
                        ''    System.IO.Directory.CreateDirectory(StrFilePath & "\" & FinancialYear._Object._DatabaseName)
                        ''    strFinalPath = StrFilePath & "\" & FinancialYear._Object._DatabaseName
                        ''End If

                        'For i As Integer = 0 To 2
                        '    If i = 0 Then
                        '        eZeeDatabase.change_database(FinancialYear._Object._ConfigDatabaseName)
                        '        System.IO.Directory.CreateDirectory(StrFilePath & "\" & FinancialYear._Object._ConfigDatabaseName)
                        '        strFinalPath = StrFilePath & "\" & FinancialYear._Object._ConfigDatabaseName
                        '    ElseIf i = 2 Then   'this database has to be always to be taken at last and i number has to be higher in order
                        '        eZeeDatabase.change_database(FinancialYear._Object._DatabaseName)
                        '        System.IO.Directory.CreateDirectory(StrFilePath & "\" & FinancialYear._Object._DatabaseName)
                        '        strFinalPath = StrFilePath & "\" & FinancialYear._Object._DatabaseName
                        '    ElseIf i = 1 Then
                        '        Dim objBkUp As New clsBackup
                        '        If objBkUp.Is_DatabasePresent("arutiimages") Then
                        '            eZeeDatabase.change_database("arutiimages")
                        '            System.IO.Directory.CreateDirectory(StrFilePath & "\" & "arutiimages")
                        '            strFinalPath = StrFilePath & "\" & "arutiimages"
                        '        Else
                        '            strFinalPath = ""
                        '        End If
                        '        objBkUp = Nothing
                        '    End If

                        '    If strFinalPath <> "" Then

                        '        strFinalPath = objBackupDatabase.Backup(strFinalPath, General_Settings._Object._ServerName)

                        '    Dim objBackup As New clsBackup
                        '    If System.IO.File.Exists(strFinalPath) Then

                        '            objBackup._Backup_Date = DateAndTime.Now 'ConfigParameter._Object._CurrentDateAndTime
                        '        objBackup._Backup_Path = strFinalPath
                        '        If i = 0 Then
                        '            objBackup._Companyunkid = -1
                        '            objBackup._Yearunkid = -1
                        '            objBackup._Isconfiguration = True
                        '        ElseIf i = 1 Then
                        '            objBackup._Companyunkid = Company._Object._Companyunkid
                        '            objBackup._Yearunkid = FinancialYear._Object._YearUnkid
                        '            objBackup._Isconfiguration = False
                        '        End If
                        '        objBackup._Userunkid = User._Object._Userunkid

                        '        Call objBackup.Insert()
                        '    End If

                        '    End If
                        'Next

                        ''Pinkal (04-Apr-2013) -- End

                        ''Sohail (10 Feb 2012) -- End


                        ''Pinkal (24-May-2013) -- Start
                        ''Enhancement : TRA Changes

                        ''If rdNoAction.Checked = True Then
                        ''    menLeavAction = clsCloseYear.enLeaveAction.No_Action
                        ''ElseIf rdSetZero.Checked = True Then
                        ''    menLeavAction = clsCloseYear.enLeaveAction.Set_Zero
                        ''ElseIf rdSetMaximum.Checked = True Then
                        ''    menLeavAction = clsCloseYear.enLeaveAction.Set_Maximun
                        ''End If

                        ''Pinkal (24-May-2013) -- End

                        'objCloseYear._CurrYearUnkID = mintCurrYearUnkID
                        'objCloseYear._CurrYearStartDate = mdtCurrYearStartDate
                        'objCloseYear._CurrYearEndDate = mdtCurrYearEndDate
                        'objCloseYear._NewYearStartDate = mdtNewYearStartDate
                        'objCloseYear._NewYearEndDate = mdtNewYearEndDate
                        'objCloseYear._CopyLastPeriodEDSlab = chkCopyPreviousEDSlab.Checked 'Sohail (21 Jul 2012)
                        'objCloseYear._Datasource_Leave = mdtLeave
                        'objCloseYear._Datasource_ProcessPendingLoan = mdtProcessPendingLoan
                        'objCloseYear._Datasource_Loan = mdtLoan
                        'objCloseYear._Datasource_Payroll = mdtPayroll
                        'objCloseYear._Datasource_Savings = mdtSavings
                        'objCloseYear._Datasource_Applicants = mdtApplicant 'Sohail (03 Nov 2010)
                        'objCloseYear._Leave_IDs = mstrLeave.Replace(" ", "")
                        ''Sohail (09 Nov 2013) -- Start
                        ''TRA - ENHANCEMENT
                        'objCloseYear._LastPeriodUnkID = mintLastPeriodUnkID
                        'objCloseYear._LastPeriodStartDate = mdtLastPeriodStartDate
                        'objCloseYear._LastPeriodEndDate = mdtLastPeriodEndDate
                        ''Sohail (09 Nov 2013) -- End
                        'objCloseYear._LastPeriodTnAEndDate = mdtLastPeriodTnAEndDate

                        ''Pinkal (24-May-2013) -- Start
                        ''Enhancement : TRA Changes
                        ''objCloseYear._LeaveAction = menLeavAction
                        'objCloseYear._LeaveTypeIDs = mstrLeaveTypeIDs.Replace(" ", "")
                        ''Pinkal (24-May-2013) -- End


                        'objCloseYear._Payroll_IDs = mstrPayroll.Replace(" ", "")
                        'objCloseYear._ProcessPendingLoan_IDs = mstrProcessPendingLoan.Replace(" ", "")
                        'objCloseYear._Loan_IDs = mstrLoan.Replace(" ", "")
                        'objCloseYear._Savings_IDs = mstrSavings.Replace(" ", "")
                        'objCloseYear._Applicants_IDs = mstrApplicants.Replace(" ", "") 'Sohail (03 Nov 2010)


                        ''Pinkal (24-May-2013) -- Start
                        ''Enhancement : TRA Changes

                        ''Sohail (01 Dec 2010) -- Start
                        ''If rdSetMaximum.Checked = True Then
                        ''    objCloseYear._MaximumLeave = txtMaxLeave.Decimal 'Sohail (11 May 2011)
                        ''Else
                        ''    objCloseYear._MaximumLeave = 0
                        ''End If
                        ''Sohail (01 Dec 2010) -- End

                        ''Pinkal (24-May-2013) -- End

                        ''Sohail (04 Mar 2011) -- Start
                        'objCloseYear._Period_Code = txtCode.Text.Trim
                        'objCloseYear._Period_Name = txtName.Text.Trim
                        'objCloseYear._Period_EndDate = dtpPeriodEnddate.Value
                        ''Sohail (04 Mar 2011) -- End

                        ''Sohail (21 Aug 2015) -- Start
                        ''Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                        ''blnFlag = objCloseYear.Close_Year()
                        'Dim objAppSetting As New clsApplicationSettings
                        'blnFlag = objCloseYear.Close_Year(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, FinancialYear._Object._Database_Start_Date, FinancialYear._Object._Database_End_Date, ConfigParameter._Object._UserAccessModeSetting, False, True, Company._Object._Code, objAppSetting._ApplicationPath, ConfigParameter._Object._LeaveBalanceSetting, FinancialYear._Object._Database_End_Date, ConfigParameter._Object._CurrentDateAndTime, False, "")
                        ''Sohail (21 Aug 2015) -- End

                        'If blnFlag = False And objCloseYear._Message <> "" Then
                        '    eZeeMsgBox.Show(objCloseYear._Message, enMsgBoxStyle.Information)
                        'End If
                        'If blnFlag = False Then
                        '    e.Cancel = True
                        '    Exit Sub
                        'End If
                        'mblnCancel = False
                        'picSide_Step6_Check.Visible = True
                        ''Sohail (01 Feb 2011) -- Start
                        'Dim intYearID As Integer = FinancialYear._Object._YearUnkid
                        'FinancialYear.Refresh()
                        'FinancialYear._Object._YearUnkid = intYearID
                        ''Sohail (01 Feb 2011) -- End
                        ''Sohail (28 Dec 2010) -- Start
                        'gfrmMDI.mnuPayrollView.Enabled = False
                        'gfrmMDI.mnuReportView.PerformClick()
                        ''Sohail (28 Dec 2010) -- End

                        ''Sohail (24 Jun 2013) -- Start
                        ''TRA - ENHANCEMENT
                        'blnFlag = CreateUserViewSynonymsForSharePointTRA()
                        ''Sohail (24 Jun 2013) -- End

                        ''S.SANDEEP [ 25 JUNE 2013 ] -- START
                        ''ENHANCEMENT : OTHER CHANGES
                        'blnFlag = CreateUserViewSynonymsFor_IDMS()
                        ''S.SANDEEP [ 25 JUNE 2013 ] -- END
                        e.Cancel = True
                        objbgwCloseYear.RunWorkerAsync()
                        'Sohail (06 Jan 2016) -- End
                       
                    Else
                        picSide_Step5_Check.Visible = False
                    End If

            End Select
        Catch ex As Exception
            'Sohail (10 Feb 2012) -- Start
            'TRA - ENHANCEMENT
            e.Cancel = True
            Cursor.Current = Cursors.Default
            'Sohail (10 Feb 2012) -- End
            eZeeCommonLib.eZeeDatabase.change_database(FinancialYear._Object._DatabaseName)
            DisplayError.Show("-1", ex.Message, "wzGenerateNewYear_BeforeSwitchPages", mstrModuleName)
        End Try
    End Sub
#End Region

    'Sohail (06 Jan 2016) -- Start
    'Enhancement - Show Close Year Process Logs on Close Year Wizard.
#Region " Background Worker Events "

    Private Sub objbgwCloseYear_DoWork(ByVal sender As System.Object, ByVal e As System.ComponentModel.DoWorkEventArgs) Handles objbgwCloseYear.DoWork
        Dim blnFlag As Boolean = False
        Try
            mblnProcessFailed = False
            Me.ControlBox = False
            'Me.Enabled = False
            wzGenerateNewYear.Enabled = False

            RemoveHandler gfrmMDI.tmrReminder.Tick, AddressOf gfrmMDI.tmrReminder_Tick
            clsApplicationIdleTimer.LastIdealTime.Stop()


            Dim objAppSettings As New clsApplicationSettings
            Dim strFinalPath As String = String.Empty
            Dim StrFilePath As String = objAppSettings._ApplicationPath & "Data\Close Year Backup"

            If Not System.IO.Directory.Exists(StrFilePath) Then
                System.IO.Directory.CreateDirectory(StrFilePath)
            End If

            mstrCloseYearLogFileName = objAppSettings._ApplicationPath & "CloseYearLog_" & Format(DateAndTime.Now.Date, "yyyyMMdd") & ".log"
            objAppSettings = Nothing

            IO.File.AppendAllText(mstrCloseYearLogFileName, "--------------------------------------------------------------------" & vbCrLf)
            IO.File.AppendAllText(mstrCloseYearLogFileName, "Close Year Process Started at " & Format(Now, "dd-MMM-yyyy HH:mm:ss") & " ..." & vbCrLf)
            IO.File.AppendAllText(mstrCloseYearLogFileName, "--------------------------------------------------------------------" & vbCrLf)
            IO.File.AppendAllText(mstrCloseYearLogFileName, "Taking Daatabase backup..." & vbCrLf)
            rtbCloseYearLog.ResetText()
            rtbCloseYearLog.LoadFile(mstrCloseYearLogFileName, RichTextBoxStreamType.PlainText)
            rtbCloseYearLog.SelectionStart = rtbCloseYearLog.TextLength
            rtbCloseYearLog.ScrollToCaret()

            For i As Integer = 0 To 2
                If i = 0 Then
                    eZeeDatabase.change_database(FinancialYear._Object._ConfigDatabaseName)
                    If Not System.IO.Directory.Exists(StrFilePath & "\" & FinancialYear._Object._ConfigDatabaseName) Then
                        System.IO.Directory.CreateDirectory(StrFilePath & "\" & FinancialYear._Object._ConfigDatabaseName)
                    End If
                    strFinalPath = StrFilePath & "\" & FinancialYear._Object._ConfigDatabaseName
                    IO.File.AppendAllText(mstrCloseYearLogFileName, "Taking Daatabase backup [" & FinancialYear._Object._ConfigDatabaseName & "]..." & vbCrLf)
                    rtbCloseYearLog.ResetText()
                    rtbCloseYearLog.LoadFile(mstrCloseYearLogFileName, RichTextBoxStreamType.PlainText)
                    rtbCloseYearLog.SelectionStart = rtbCloseYearLog.TextLength
                    rtbCloseYearLog.ScrollToCaret()

                ElseIf i = 2 Then 'this database has to be always to be taken at last and i number has to be higher in order
                    eZeeDatabase.change_database(FinancialYear._Object._DatabaseName)
                    If Not System.IO.Directory.Exists(StrFilePath & "\" & FinancialYear._Object._DatabaseName) Then
                        System.IO.Directory.CreateDirectory(StrFilePath & "\" & FinancialYear._Object._DatabaseName)
                    End If
                    strFinalPath = StrFilePath & "\" & FinancialYear._Object._DatabaseName
                    IO.File.AppendAllText(mstrCloseYearLogFileName, "Taking Daatabase backup [" & FinancialYear._Object._DatabaseName & "]..." & vbCrLf)
                    rtbCloseYearLog.ResetText()
                    rtbCloseYearLog.LoadFile(mstrCloseYearLogFileName, RichTextBoxStreamType.PlainText)
                    rtbCloseYearLog.SelectionStart = rtbCloseYearLog.TextLength
                    rtbCloseYearLog.ScrollToCaret()

                ElseIf i = 1 Then
                    Dim objBkUp As New clsBackup
                    If objBkUp.Is_DatabasePresent("arutiimages") Then
                        eZeeDatabase.change_database("arutiimages")
                        If Not System.IO.Directory.Exists(StrFilePath & "\" & "arutiimages") Then
                            System.IO.Directory.CreateDirectory(StrFilePath & "\" & "arutiimages")
                        End If
                        strFinalPath = StrFilePath & "\" & "arutiimages"
                        IO.File.AppendAllText(mstrCloseYearLogFileName, "Taking Daatabase backup [arutiimages]..." & vbCrLf)
                        rtbCloseYearLog.ResetText()
                        rtbCloseYearLog.LoadFile(mstrCloseYearLogFileName, RichTextBoxStreamType.PlainText)
                        rtbCloseYearLog.SelectionStart = rtbCloseYearLog.TextLength
                        rtbCloseYearLog.ScrollToCaret()
                    Else
                        strFinalPath = ""
                    End If
                    objBkUp = Nothing
                End If
                Me.Refresh()

                If strFinalPath <> "" Then

                    strFinalPath = objBackupDatabase.Backup(strFinalPath, General_Settings._Object._ServerName)

                    Dim objBackup As New clsBackup
                    If System.IO.File.Exists(strFinalPath) Then

                        objBackup._Backup_Date = DateAndTime.Now
                        objBackup._Backup_Path = strFinalPath
                        If i = 0 Then
                            objBackup._Companyunkid = -1
                            objBackup._Yearunkid = -1
                            objBackup._Isconfiguration = True
                        ElseIf i = 1 Then
                            objBackup._Companyunkid = Company._Object._Companyunkid
                            objBackup._Yearunkid = FinancialYear._Object._YearUnkid
                            objBackup._Isconfiguration = False
                        End If
                        objBackup._Userunkid = User._Object._Userunkid

                        Call objBackup.Insert()
                    End If

                End If
            Next

            objCloseYear._CurrYearUnkID = mintCurrYearUnkID
            objCloseYear._CurrYearStartDate = mdtCurrYearStartDate
            objCloseYear._CurrYearEndDate = mdtCurrYearEndDate
            objCloseYear._NewYearStartDate = mdtNewYearStartDate
            objCloseYear._NewYearEndDate = mdtNewYearEndDate
            objCloseYear._CopyLastPeriodEDSlab = chkCopyPreviousEDSlab.Checked
            objCloseYear._Datasource_Leave = mdtLeave
            objCloseYear._Datasource_ProcessPendingLoan = mdtProcessPendingLoan
            objCloseYear._Datasource_Loan = mdtLoan
            objCloseYear._Datasource_Payroll = mdtPayroll
            objCloseYear._Datasource_Savings = mdtSavings
            objCloseYear._Datasource_Applicants = mdtApplicant
            objCloseYear._Leave_IDs = mstrLeave.Replace(" ", "")
            objCloseYear._LastPeriodUnkID = mintLastPeriodUnkID
            objCloseYear._LastPeriodStartDate = mdtLastPeriodStartDate
            objCloseYear._LastPeriodEndDate = mdtLastPeriodEndDate
            objCloseYear._LastPeriodTnAEndDate = mdtLastPeriodTnAEndDate
            objCloseYear._LeaveTypeIDs = mstrLeaveTypeIDs.Replace(" ", "")
            objCloseYear._Payroll_IDs = mstrPayroll.Replace(" ", "")
            objCloseYear._ProcessPendingLoan_IDs = mstrProcessPendingLoan.Replace(" ", "")
            objCloseYear._Loan_IDs = mstrLoan.Replace(" ", "")
            objCloseYear._Savings_IDs = mstrSavings.Replace(" ", "")
            objCloseYear._Applicants_IDs = mstrApplicants.Replace(" ", "")

            objCloseYear._Period_Code = txtCode.Text.Trim
            objCloseYear._Period_Name = txtName.Text.Trim
            objCloseYear._Period_EndDate = dtpPeriodEnddate.Value

            Dim objAppSetting As New clsApplicationSettings

            'S.SANDEEP [28-May-2018] -- START
            'ISSUE/ENHANCEMENT : {Audit Trails} 
            objCloseYear._FormName = mstrModuleName
            objCloseYear._LoginEmployeeunkid = 0
            objCloseYear._ClientIP = getIP()
            objCloseYear._HostName = getHostName()
            objCloseYear._FromWeb = False
            objCloseYear._AuditUserId = User._Object._Userunkid
objCloseYear._CompanyUnkid = Company._Object._Companyunkid
            objCloseYear._AuditDate = ConfigParameter._Object._CurrentDateAndTime
            'S.SANDEEP [28-May-2018] -- END

            blnFlag = objCloseYear.Close_Year(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, FinancialYear._Object._Database_Start_Date, FinancialYear._Object._Database_End_Date, ConfigParameter._Object._UserAccessModeSetting, False, True, Company._Object._Code, objAppSetting._ApplicationPath, ConfigParameter._Object._LeaveBalanceSetting, FinancialYear._Object._Database_End_Date, ConfigParameter._Object._CurrentDateAndTime, False, "", mstrCloseYearLogFileName, objbgwCloseYear, rtbCloseYearLog)

            If blnFlag = False And objCloseYear._Message <> "" Then
                eZeeMsgBox.Show(objCloseYear._Message, enMsgBoxStyle.Information)
            End If
            If blnFlag = False Then
                eZeeCommonLib.eZeeDatabase.change_database(FinancialYear._Object._DatabaseName)
                mblnProcessFailed = True
                Exit Try
            End If
            mblnCancel = False

            Dim intYearID As Integer = FinancialYear._Object._YearUnkid
            FinancialYear.Refresh()
            FinancialYear._Object._YearUnkid = intYearID

            blnFlag = CreateUserViewSynonymsForSharePointTRA()

            blnFlag = CreateUserViewSynonymsFor_IDMS()

            Cursor.Current = Cursors.Default


        Catch ex As Exception
            mblnProcessFailed = True
            objbgwCloseYear.CancelAsync()
            DisplayError.Show("-1", ex.Message, "objbgwCloseYear_DoWork", mstrModuleName)
            eZeeCommonLib.eZeeDatabase.change_database(FinancialYear._Object._DatabaseName)
            RemoveHandler gfrmMDI.tmrReminder.Tick, AddressOf gfrmMDI.tmrReminder_Tick
            AddHandler gfrmMDI.tmrReminder.Tick, AddressOf gfrmMDI.tmrReminder_Tick
            clsApplicationIdleTimer.LastIdealTime.Start()

            Me.ControlBox = True
            'Me.Enabled = True
            wzGenerateNewYear.Enabled = True
        Finally
            'RemoveHandler gfrmMDI.tmrReminder.Tick, AddressOf gfrmMDI.tmrReminder_Tick
            'AddHandler gfrmMDI.tmrReminder.Tick, AddressOf gfrmMDI.tmrReminder_Tick
            'clsApplicationIdleTimer.LastIdealTime.Start()
        End Try
    End Sub

    Private Sub objbgwCloseYear_ProgressChanged(ByVal sender As System.Object, ByVal e As System.ComponentModel.ProgressChangedEventArgs) Handles objbgwCloseYear.ProgressChanged
        Try
            If e.ProgressPercentage = 0 Then
                'rtbCloseYearLog.ResetText()
                'rtbCloseYearLog.LoadFile(mstrCloseYearLogFileName, RichTextBoxStreamType.PlainText)
                'rtbCloseYearLog.SelectionStart = rtbCloseYearLog.TextLength
                'rtbCloseYearLog.ScrollToCaret()
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbgwCloseYear_ProgressChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub objbgwCloseYear_RunWorkerCompleted(ByVal sender As System.Object, ByVal e As System.ComponentModel.RunWorkerCompletedEventArgs) Handles objbgwCloseYear.RunWorkerCompleted
        Try

            If e.Cancelled = True Then

            ElseIf e.Error IsNot Nothing Then
                eZeeMsgBox.Show(e.Error.ToString, enMsgBoxStyle.Information)
            ElseIf mblnProcessFailed = True Then
                objlblProgress.Visible = False
            Else
                picSide_Step6_Check.Visible = True
                wzGenerateNewYear.SelectedPage = wpFinish
                objlblProgress.Text = ""
                rtbCloseYearLog.ResetText()

                rtbCloseYearLog.Visible = False
                objlblProgress.Visible = False

                gfrmMDI.mnuPayrollView.Enabled = False
                gfrmMDI.mnuReportView.PerformClick()

                eZeeCommonLib.eZeeDatabase.change_database(FinancialYear._Object._DatabaseName)
            End If

            Me.ControlBox = True
            'Me.Enabled = True
            wzGenerateNewYear.Enabled = True

            RemoveHandler gfrmMDI.tmrReminder.Tick, AddressOf gfrmMDI.tmrReminder_Tick
            AddHandler gfrmMDI.tmrReminder.Tick, AddressOf gfrmMDI.tmrReminder_Tick
            clsApplicationIdleTimer.LastIdealTime.Start()

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbgwCloseYear_RunWorkerCompleted", mstrModuleName)
        End Try
    End Sub

#End Region
    'Sohail (06 Jan 2016) -- End

    'Sohail (01 Dec 2010) -- Start
#Region " Radio Button's Events "

    'S.SANDEEP [ 08 June 2011 ] -- START
    'ISSUE : LEAVE C/F OPTIONS
    'Private Sub rdSetMaximum_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles rdSetMaximum.CheckedChanged, rdNoAction.CheckedChanged, rdSetZero.CheckedChanged
    '    Try
    '        If rdSetMaximum.Checked = True Then
    '            txtMaxLeave.Visible = True
    '            txtMaxLeave.Focus()
    '        Else
    '            txtMaxLeave.Visible = False
    '        End If
    '    Catch ex As Exception
    '        DisplayError.Show("-1", ex.Message, "rdSetMaximum_CheckedChanged", mstrModuleName)
    '    End Try
    'End Sub
    'S.SANDEEP [ 08 June 2011 ] -- END

#End Region
    'Sohail (01 Dec 2010) -- End

#Region " Message List "
    '1, "Are you sure you want to Close this Year?"
    '2, "Sorry, Maximum Leave should be greater than zero."
    '3, "Please enter New Period Code."
    '4, "Please enter New Period Name."
    '5, "This Period Code is already defined. Please define new Period Code."
    '6, "This Period Name is already defined. Please define new Period Name."
#End Region


	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Private Sub OtherSettings()
		Try
			Me.SuspendLayout()
			 
			Call SetLanguage()
			
			Me.gbNewYearSteps.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbNewYearSteps.ForeColor = GUI._eZeeContainerHeaderForeColor 

			Me.gbStep1.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbStep1.ForeColor = GUI._eZeeContainerHeaderForeColor 

			Me.gbstep2.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbstep2.ForeColor = GUI._eZeeContainerHeaderForeColor 

			Me.gbStep3.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbStep3.ForeColor = GUI._eZeeContainerHeaderForeColor 

			Me.gbStep4.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbStep4.ForeColor = GUI._eZeeContainerHeaderForeColor 

			Me.EZeeCollapsibleContainer1.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.EZeeCollapsibleContainer1.ForeColor = GUI._eZeeContainerHeaderForeColor 

			Me.gbLeave_BF.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbLeave_BF.ForeColor = GUI._eZeeContainerHeaderForeColor 

			Me.gbLoan.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbLoan.ForeColor = GUI._eZeeContainerHeaderForeColor 


			
			Me.EZeeHeader.GradientColor1 = GUI._HeaderBackColor1 
			Me.EZeeHeader.GradientColor2 = GUI._HeaderBackColor2 
			Me.EZeeHeader.BorderColor = GUI._HeaderBorderColor 
			Me.EZeeHeader.HeaderTextForeColor = GUI._HeaderTitleFontColor 
			Me.EZeeHeader.DescriptionForeColor = GUI._HeaderMessageFontColor 


			Me.objbuttonBack.GradientBackColor = GUI._ButttonBackColor 
			Me.objbuttonBack.GradientForeColor = GUI._ButttonFontColor

			Me.objbuttonCancel.GradientBackColor = GUI._ButttonBackColor 
			Me.objbuttonCancel.GradientForeColor = GUI._ButttonFontColor

			Me.objbuttonNext.GradientBackColor = GUI._ButttonBackColor 
			Me.objbuttonNext.GradientForeColor = GUI._ButttonFontColor


			Me.ResumeLayout()
		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "OtherSettings", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetLanguage()
		Try
			Me.Text = Language._Object.getCaption(Me.Name, Me.Text)
			
			Me.EZeeHeader.Title = Language._Object.getCaption(Me.EZeeHeader.Name & "_Title" , Me.EZeeHeader.Title)
			Me.EZeeHeader.Message = Language._Object.getCaption(Me.EZeeHeader.Name & "_Message" , Me.EZeeHeader.Message)
			Me.gbNewYearSteps.Text = Language._Object.getCaption(Me.gbNewYearSteps.Name, Me.gbNewYearSteps.Text)
			Me.lblStep6.Text = Language._Object.getCaption(Me.lblStep6.Name, Me.lblStep6.Text)
			Me.lblStep7.Text = Language._Object.getCaption(Me.lblStep7.Name, Me.lblStep7.Text)
			Me.lblStep3.Text = Language._Object.getCaption(Me.lblStep3.Name, Me.lblStep3.Text)
			Me.lblStep5.Text = Language._Object.getCaption(Me.lblStep5.Name, Me.lblStep5.Text)
			Me.lblStep4.Text = Language._Object.getCaption(Me.lblStep4.Name, Me.lblStep4.Text)
			Me.lblStep2.Text = Language._Object.getCaption(Me.lblStep2.Name, Me.lblStep2.Text)
			Me.lblStep1.Text = Language._Object.getCaption(Me.lblStep1.Name, Me.lblStep1.Text)
			Me.wzGenerateNewYear.CancelText = Language._Object.getCaption(Me.wzGenerateNewYear.Name & "_CancelText" , Me.wzGenerateNewYear.CancelText)
			Me.wzGenerateNewYear.NextText = Language._Object.getCaption(Me.wzGenerateNewYear.Name & "_NextText" , Me.wzGenerateNewYear.NextText)
			Me.wzGenerateNewYear.BackText = Language._Object.getCaption(Me.wzGenerateNewYear.Name & "_BackText" , Me.wzGenerateNewYear.BackText)
			Me.wzGenerateNewYear.FinishText = Language._Object.getCaption(Me.wzGenerateNewYear.Name & "_FinishText" , Me.wzGenerateNewYear.FinishText)
			Me.gbStep1.Text = Language._Object.getCaption(Me.gbStep1.Name, Me.gbStep1.Text)
			Me.lblStep1Message.Text = Language._Object.getCaption(Me.lblStep1Message.Name, Me.lblStep1Message.Text)
			Me.gbstep2.Text = Language._Object.getCaption(Me.gbstep2.Name, Me.gbstep2.Text)
			Me.lblCurrStartdate.Text = Language._Object.getCaption(Me.lblCurrStartdate.Name, Me.lblCurrStartdate.Text)
			Me.eZeeLine1.Text = Language._Object.getCaption(Me.eZeeLine1.Name, Me.eZeeLine1.Text)
			Me.lblCurrEnddate.Text = Language._Object.getCaption(Me.lblCurrEnddate.Name, Me.lblCurrEnddate.Text)
			Me.lblNewEnddate.Text = Language._Object.getCaption(Me.lblNewEnddate.Name, Me.lblNewEnddate.Text)
			Me.lblNewStartdate.Text = Language._Object.getCaption(Me.lblNewStartdate.Name, Me.lblNewStartdate.Text)
			Me.EZeeLine2.Text = Language._Object.getCaption(Me.EZeeLine2.Name, Me.EZeeLine2.Text)
			Me.gbStep3.Text = Language._Object.getCaption(Me.gbStep3.Name, Me.gbStep3.Text)
			Me.gbStep4.Text = Language._Object.getCaption(Me.gbStep4.Name, Me.gbStep4.Text)
			Me.EZeeCollapsibleContainer1.Text = Language._Object.getCaption(Me.EZeeCollapsibleContainer1.Name, Me.EZeeCollapsibleContainer1.Text)
			Me.lblStep7_Finish.Text = Language._Object.getCaption(Me.lblStep7_Finish.Name, Me.lblStep7_Finish.Text)
			Me.gbLeave_BF.Text = Language._Object.getCaption(Me.gbLeave_BF.Name, Me.gbLeave_BF.Text)
			Me.gbLoan.Text = Language._Object.getCaption(Me.gbLoan.Name, Me.gbLoan.Text)
			Me.EZeeLine5.Text = Language._Object.getCaption(Me.EZeeLine5.Name, Me.EZeeLine5.Text)
			Me.colhEmployeeCode.Text = Language._Object.getCaption(CStr(Me.colhEmployeeCode.Tag), Me.colhEmployeeCode.Text)
			Me.colhEmployeeName.Text = Language._Object.getCaption(CStr(Me.colhEmployeeName.Tag), Me.colhEmployeeName.Text)
			Me.colhHead.Text = Language._Object.getCaption(CStr(Me.colhHead.Tag), Me.colhHead.Text)
			Me.colhBalance.Text = Language._Object.getCaption(CStr(Me.colhBalance.Tag), Me.colhBalance.Text)
			Me.lblStep6_Desc.Text = Language._Object.getCaption(Me.lblStep6_Desc.Name, Me.lblStep6_Desc.Text)
			Me.lblStep4_Desc.Text = Language._Object.getCaption(Me.lblStep4_Desc.Name, Me.lblStep4_Desc.Text)
			Me.colhHeadType.Text = Language._Object.getCaption(CStr(Me.colhHeadType.Tag), Me.colhHeadType.Text)
			Me.colhPeriod.Text = Language._Object.getCaption(CStr(Me.colhPeriod.Tag), Me.colhPeriod.Text)
			Me.colhLvBalanceID.Text = Language._Object.getCaption(CStr(Me.colhLvBalanceID.Tag), Me.colhLvBalanceID.Text)
			Me.colhEmployee.Text = Language._Object.getCaption(CStr(Me.colhEmployee.Tag), Me.colhEmployee.Text)
			Me.colhLeaveBalance.Text = Language._Object.getCaption(CStr(Me.colhLeaveBalance.Tag), Me.colhLeaveBalance.Text)
			Me.lblStep3_Desc.Text = Language._Object.getCaption(Me.lblStep3_Desc.Name, Me.lblStep3_Desc.Text)
			Me.colhLoanPeriod.Text = Language._Object.getCaption(CStr(Me.colhLoanPeriod.Tag), Me.colhLoanPeriod.Text)
			Me.colhLoanEmpName.Text = Language._Object.getCaption(CStr(Me.colhLoanEmpName.Tag), Me.colhLoanEmpName.Text)
			Me.colhLoanHead.Text = Language._Object.getCaption(CStr(Me.colhLoanHead.Tag), Me.colhLoanHead.Text)
			Me.colhLoanHeadType.Text = Language._Object.getCaption(CStr(Me.colhLoanHeadType.Tag), Me.colhLoanHeadType.Text)
			Me.colhLoanBalance.Text = Language._Object.getCaption(CStr(Me.colhLoanBalance.Tag), Me.colhLoanBalance.Text)
			Me.colhLeaveTypeName.Text = Language._Object.getCaption(CStr(Me.colhLeaveTypeName.Tag), Me.colhLeaveTypeName.Text)
			Me.colhApplicantCode.Text = Language._Object.getCaption(CStr(Me.colhApplicantCode.Tag), Me.colhApplicantCode.Text)
			Me.colhApplicantName.Text = Language._Object.getCaption(CStr(Me.colhApplicantName.Tag), Me.colhApplicantName.Text)
			Me.EZeeLine3.Text = Language._Object.getCaption(Me.EZeeLine3.Name, Me.EZeeLine3.Text)
			Me.lblEnddate.Text = Language._Object.getCaption(Me.lblEnddate.Name, Me.lblEnddate.Text)
			Me.lblName.Text = Language._Object.getCaption(Me.lblName.Name, Me.lblName.Text)
			Me.lblCode.Text = Language._Object.getCaption(Me.lblCode.Name, Me.lblCode.Text)
			Me.chkCopyPreviousEDSlab.Text = Language._Object.getCaption(Me.chkCopyPreviousEDSlab.Name, Me.chkCopyPreviousEDSlab.Text)
			Me.colhCFLeaveAmount.Text = Language._Object.getCaption(CStr(Me.colhCFLeaveAmount.Tag), Me.colhCFLeaveAmount.Text)
			Me.colhLoanEmpCode.Text = Language._Object.getCaption(CStr(Me.colhLoanEmpCode.Tag), Me.colhLoanEmpCode.Text)

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetLanguage", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "Are you sure you want to Close this Year?")
			Language.setMessage(mstrModuleName, 2, "Please enter New Period Code.")
			Language.setMessage(mstrModuleName, 3, "Please enter New Period Name.")
			Language.setMessage(mstrModuleName, 4, "This Period Code is already defined. Please define new Period Code.")
			Language.setMessage(mstrModuleName, 5, "This Period Name is already defined. Please define new Period Name.")
			Language.setMessage(mstrModuleName, 6, "Database backup")
			Language.setMessage(mstrModuleName, 7, "path has not been set.")
			Language.setMessage(mstrModuleName, 8, "Copy Previous ED Slab is ticked. This will copy Previous Period Slab.")
			Language.setMessage(mstrModuleName, 9, "You can set the Database backup path from")
			Language.setMessage(mstrModuleName, 10, "Aruti Configuration -> Option -> Path.")
			Language.setMessage(mstrModuleName, 11, "Backup of that database(s) cannot be taken.")
			Language.setMessage(mstrModuleName, 12, "Do you want to continue?")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class