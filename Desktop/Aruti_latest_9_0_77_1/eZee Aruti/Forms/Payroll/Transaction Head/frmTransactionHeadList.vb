﻿Option Strict On
Imports eZeeCommonLib
Imports Aruti.Data

Public Class frmTransactionHeadList

#Region " Private Variables "
    Private objTransactionHead As clsTransactionHead
    Private objCommonFunction As New clsMasterData
    Private ReadOnly mstrModuleName As String = "frmTransactionHeadList"

    'Sohail (17 Sep 2014) -- Start
    'Enhancement - System generated Base, OT and PH heads for PPA Activity.
    Private mdicHeadType As New Dictionary(Of Integer, String)
    Private mdicTypeOf As New Dictionary(Of Integer, String)
    Private mdicCalcType As New Dictionary(Of Integer, String)
    'Sohail (17 Sep 2014) -- End

#End Region

#Region " Private Methods "

    Private Sub FillList()
        Dim objMaster As New clsMasterData
        Dim dsTransactionHead As New DataSet
        'Sohail (17 Sep 2014) -- Start
        'Enhancement - System generated Base, OT and PH heads for PPA Activity.
        'Dim dsList As DataSet
        'Dim dsCalcType As DataSet
        'Sohail (17 Sep 2014) -- End
        Try

            If User._Object.Privilege._AllowToViewTransactionHeadsList = True Then                'Pinkal (02-Jul-2012) -- Start

            'Sohail (21 Jun 2012) -- Start
            'TRA - ENHANCEMENT
            Dim lvArray As New List(Of ListViewItem)
            lvTrnHeadList.Items.Clear()
            Cursor.Current = Cursors.WaitCursor
            'Sohail (21 Jun 2012) -- End

            'Sohail (15 Feb 2012) -- Start
            'TRA - ENHANCEMENT
            'dsTransactionHead = objTransactionHead.GetList("TranHead", CInt(cboTrnHead.SelectedValue), CInt(cboTrnHeadType.SelectedValue))
                'Sohail (17 Sep 2014) -- Start
                'Enhancement - System generated Base, OT and PH heads for PPA Activity.
                'dsTransactionHead = objTransactionHead.GetList("TranHead", CInt(cboTrnHead.SelectedValue), CInt(cboTrnHeadType.SelectedValue), , , CInt(cboActiveInactive.SelectedValue))
                'Sohail (18 Apr 2016) -- Start
                'Enhancement - 59.1 - NET PAY ROUNDING ADJUSTMENT head for the difference of actual Net Pay and Rounded Net Pay for HERON Portico.
                'dsTransactionHead = objTransactionHead.GetList("TranHead", CInt(cboTrnHead.SelectedValue), CInt(cboTrnHeadType.SelectedValue), , , CInt(cboActiveInactive.SelectedValue), , , , True)

                'S.SANDEEP [12-JUL-2018] -- START
                'ISSUE/ENHANCEMENT : {Ref#223|ARUTI-239}
                'dsTransactionHead = objTransactionHead.GetList("TranHead", CInt(cboTrnHead.SelectedValue), CInt(cboTrnHeadType.SelectedValue), , , CInt(cboActiveInactive.SelectedValue), , , , True, True)
                'Hemant (22 Aug 2020) -- Start
                'NMB Enhancement : Allow to edit payable heads on head master list to map default cost center
                'dsTransactionHead = objTransactionHead.GetList("TranHead", CInt(cboTrnHead.SelectedValue), CInt(cboTrnHeadType.SelectedValue), , , CInt(cboActiveInactive.SelectedValue), , , , True, True, 0)
                dsTransactionHead = objTransactionHead.GetList("TranHead", CInt(cboTrnHead.SelectedValue), CInt(cboTrnHeadType.SelectedValue), , True, CInt(cboActiveInactive.SelectedValue), , , , True, True, 0)
                'Hemant (22 Aug 2020) -- End

                'S.SANDEEP [12-JUL-2018] -- END

                'Sohail (18 Apr 2016) -- End
                'Sohail (17 Sep 2014) -- End

            btnDelete.Visible = False
            btnActive.Visible = False
            'Sohail (15 Feb 2012) -- End

            Dim lvItem As ListViewItem

            'lvTrnHeadList.Items.Clear()
            For Each drRow As DataRow In dsTransactionHead.Tables(0).Rows
                lvItem = New ListViewItem
                lvItem.Text = drRow("trnheadcode").ToString
                lvItem.Tag = drRow("tranheadunkid")

                lvItem.SubItems.Add(drRow("trnheadname").ToString)

                'lvItem.SubItems.Add(drRow("trnheadtype_id").ToString)
                    'Sohail (17 Sep 2014) -- Start
                    'Enhancement - System generated Base, OT and PH heads for PPA Activity.
                    'objcboHeadType.SelectedValue = CInt(drRow("trnheadtype_id").ToString)
                    'lvItem.SubItems.Add(objcboHeadType.Text)
                    lvItem.SubItems.Add(mdicHeadType.Item(CInt(drRow("trnheadtype_id").ToString)))
                    lvItem.SubItems(colhTranHeadType.Index).Tag = CInt(drRow("trnheadtype_id").ToString)
                    'Sohail (17 Sep 2014) -- End

                    'Sohail (17 Sep 2014) -- Start
                    'Enhancement - System generated Base, OT and PH heads for PPA Activity.
                    'dsList = objMaster.getComboListTypeOf("TypeOf", CInt(drRow("trnheadtype_id").ToString))
                    'With objTypeOf
                    '    .ValueMember = "Id"
                    '    .DisplayMember = "Name"
                    '    .DataSource = dsList.Tables("TypeOf")
                    '    If .Items.Count > 0 Then .SelectedIndex = 0
                    'End With
                    'objTypeOf.SelectedValue = CInt(drRow("typeof_id").ToString)
                    'lvItem.SubItems.Add(objTypeOf.Text)
                    'lvItem.SubItems(colhTypeOf.Index).Tag = CInt(drRow("typeof_id").ToString)
                    lvItem.SubItems.Add(mdicTypeOf.Item(CInt(drRow("typeof_id").ToString)))
                    lvItem.SubItems(colhTypeOf.Index).Tag = CInt(drRow("typeof_id").ToString)
                    'Sohail (17 Sep 2014) -- End

                    'Sohail (17 Sep 2014) -- Start
                    'Enhancement - System generated Base, OT and PH heads for PPA Activity.
                    'Select Case CInt(drRow("typeof_id").ToString)
                    '    Case Is > 1
                    '        dsCalcType = objMaster.getComboListCalcType("CalcType", "2") 'Sohail (28 Jan 2012)
                    '    Case Else
                    '        'Sohail (28 Jan 2012) -- Start
                    '        'TRA - ENHANCEMENT
                    '        'dsCalcType = objMaster.getComboListCalcType("CalcType", CInt(drRow("typeof_id").ToString))
                    '        dsCalcType = objMaster.getComboListCalcType("CalcType", (enTypeOf.Salary & "," & enTypeOf.Allowance).ToString)
                    '        'Sohail (28 Jan 2012) -- End
                    'End Select
                    'With objCalcType
                    '    .ValueMember = "Id"
                    '    .DisplayMember = "Name"
                    '    .DataSource = dsCalcType.Tables("CalcType")
                    '    If .Items.Count > 0 Then .SelectedIndex = 0
                    'End With
                    ''Sohail (06 Aug 2010) -- Start
                    ''Sohail (11 Sep 2010) -- Start
                    'objCalcType.SelectedValue = CInt(drRow("calctype_id").ToString)
                    ''If CInt(drRow("calctype_id").ToString) = enCalcType.DEFINED_SALARY Or _
                    ''    CInt(drRow("calctype_id").ToString) = enCalcType.OnHourWorked Or _
                    ''    CInt(drRow("calctype_id").ToString) = enCalcType.OnAttendance Then

                    ''    objCalcType.SelectedValue = enCalcType.OnAttendance
                    ''Else
                    ''objCalcType.SelectedValue = CInt(drRow("calctype_id").ToString)
                    ''End If
                    ''Sohail (11 Sep 2010) -- End
                    ''Sohail (06 Aug 2010) -- End
                    'lvItem.SubItems.Add(objCalcType.Text)
                    'lvItem.SubItems(colhCalcType.Index).Tag = CInt(drRow("calctype_id").ToString) 'Sohail (23 Dec 2010)
                    lvItem.SubItems.Add(mdicCalcType.Item(CInt(drRow("calctype_id").ToString)))
                    lvItem.SubItems(colhCalcType.Index).Tag = CInt(drRow("calctype_id").ToString)
                    'Sohail (17 Sep 2014) -- End

                'lvItem.SubItems.Add(cboTrnHeadType.Items(drRow("trnheadtype_id").ToString).ToString)

                    'Sohail (18 Feb 2019) -- Start
                    'NMB Enhancement - 76.1 - Provide setting on transaction head to mark as default heads.
                    lvItem.SubItems.Add(drRow("isdefaultYesNo").ToString)
                    lvItem.SubItems(colhDefault.Index).Tag = CBool(drRow("isdefault"))
                    'Sohail (18 Feb 2019) -- End

                'Sohail (21 Jun 2012) -- Start
                'TRA - ENHANCEMENT
                'lvTrnHeadList.Items.Add(lvItem)
                lvArray.Add(lvItem)
                'Sohail (21 Jun 2012) -- End
            Next

            'Sohail (21 Jun 2012) -- Start
            'TRA - ENHANCEMENT
            lvTrnHeadList.Items.AddRange(lvArray.ToArray)
            lvArray = Nothing
            'Sohail (21 Jun 2012) -- End

            If lvTrnHeadList.Items.Count > 17 Then
                colhCalcType.Width = 190 - 18
            Else
                colhCalcType.Width = 190
            End If

            'Sohail (15 Feb 2012) -- Start
            'TRA - ENHANCEMENT
            If CInt(cboActiveInactive.SelectedValue) = enTranHeadActiveInActive.ACTIVE Then
                btnDelete.Visible = True
            ElseIf CInt(cboActiveInactive.SelectedValue) = enTranHeadActiveInActive.INACTIVE Then
                btnActive.Visible = True
            End If
            'Sohail (15 Feb 2012) -- End
            Cursor.Current = Cursors.Default

            End If

        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "FillList", mstrModuleName)
        Finally
            objMaster = Nothing
            Call objbtnSearch.ShowResult(CStr(lvTrnHeadList.Items.Count)) 'Sohail (17 Sep 2014)
        End Try
    End Sub

    Private Sub FillCombo(Optional ByVal blnRefreshList As Boolean = False)
        Dim objTranHead As New clsTransactionHead
        Dim objMaster As New clsMasterData 'Sohail (15 Feb 2012)
        Dim dsCombo As DataSet
        Try
            If blnRefreshList = False Then
                dsCombo = objCommonFunction.getComboListForHeadType("HeadType")
                With cboTrnHeadType
                    .ValueMember = "Id"
                    .DisplayMember = "Name"
                    .DataSource = dsCombo.Tables("HeadType")
                    If .Items.Count > 0 Then .SelectedIndex = 0
                End With
                'Sohail (17 Sep 2014) -- Start
                'Enhancement - System generated Base, OT and PH heads for PPA Activity.
                'dsCombo = objCommonFunction.getComboListForHeadType("HeadType")
                'With objcboHeadType
                '    .ValueMember = "Id"
                '    .DisplayMember = "Name"
                '    .DataSource = dsCombo.Tables("HeadType")
                '    If .Items.Count > 0 Then .SelectedIndex = 0
                'End With
                mdicHeadType = (From p In dsCombo.Tables(0).AsEnumerable Select New With {Key .ID = CInt(p.Item("Id")), Key .NAME = p.Item("NAME").ToString}).ToDictionary(Function(x) x.ID, Function(x) x.NAME)

                dsCombo = objCommonFunction.getComboListTypeOf("TypeOf", , True)
                mdicTypeOf = (From p In dsCombo.Tables(0).AsEnumerable Select New With {Key .ID = CInt(p.Item("Id")), Key .NAME = p.Item("NAME").ToString}).ToDictionary(Function(x) x.ID, Function(x) x.NAME)

                'Hemant (22 Aug 2020) -- Start
                'NMB Enhancement : Allow to edit payable heads on head master list to map default cost center
                'dsCombo = objCommonFunction.getComboListCalcType("CalcType", , True)
                dsCombo = objCommonFunction.getComboListCalcType("CalcType", , True, , , , , True)
                'Hemant (22 Aug 2020) -- End
                mdicCalcType = (From p In dsCombo.Tables(0).AsEnumerable Select New With {Key .ID = CInt(p.Item("Id")), Key .NAME = p.Item("NAME").ToString}).ToDictionary(Function(x) x.ID, Function(x) x.NAME)
                'Sohail (17 Sep 2014) -- End
            End If

            'Sohail (17 Sep 2014) -- Start
            'Enhancement - System generated Base, OT and PH heads for PPA Activity.
            'dsCombo = objTranHead.getComboList("TranHeadList", True)
            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'dsCombo = objTranHead.getComboList("TranHeadList", True, , , , , , , , , True)
            'Sohail (18 Apr 2016) -- Start
            'Enhancement - 59.1 - NET PAY ROUNDING ADJUSTMENT head for the difference of actual Net Pay and Rounded Net Pay for HERON Portico.
            'dsCombo = objTranHead.getComboList(FinancialYear._Object._DatabaseName, "TranHeadList", True, , , , , , , , True)

            'S.SANDEEP [12-JUL-2018] -- START
            'ISSUE/ENHANCEMENT : {Ref#223|ARUTI-239}
            'dsCombo = objTranHead.getComboList(FinancialYear._Object._DatabaseName, "TranHeadList", True, , , , , , , , True, True)
            'Hemant (22 Aug 2020) -- Start
            'NMB Enhancement : Allow to edit payable heads on head master list to map default cost center
            'dsCombo = objTranHead.getComboList(FinancialYear._Object._DatabaseName, "TranHeadList", True, , , , , , , , True, True, 0)
            dsCombo = objTranHead.getComboList(FinancialYear._Object._DatabaseName, "TranHeadList", True, , , , True, , , , True, True, 0)
            'Hemant (22 Aug 2020) -- End
            'S.SANDEEP [12-JUL-2018] -- END

            'Sohail (18 Apr 2016) -- End
            'Sohail (21 Aug 2015) -- End
            'Sohail (17 Sep 2014) -- End
            With cboTrnHead
                .DisplayMember = "name"
                .ValueMember = "tranheadunkid"
                .DataSource = dsCombo.Tables("TranHeadList")
                If blnRefreshList = False Then
                    If .Items.Count > 0 Then .SelectedIndex = 0
                End If
            End With

            'Sohail (15 Feb 2012) -- Start
            'TRA - ENHANCEMENT
            dsCombo = objMaster.getComboListTranHeadActiveInActive("ActiveInactive", False)
            With cboActiveInactive
                .DisplayMember = "Name"
                .ValueMember = "Id"
                .DataSource = dsCombo.Tables("ActiveInactive")
                .SelectedIndex = 0
            End With
            'Sohail (15 Feb 2012) -- End

        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "FillList", mstrModuleName)
        Finally
            objTranHead = Nothing
        End Try
    End Sub

    Private Sub SetColor()
        Try
            cboTrnHead.BackColor = GUI.ColorOptional
            cboTrnHeadType.BackColor = GUI.ColorOptional
            cboActiveInactive.BackColor = GUI.ColorComp 'Sohail (15 Feb 2012)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetColor", mstrModuleName)
        End Try
    End Sub

    Private Sub SetVisibility()

        Try
            btnNew.Enabled = User._Object.Privilege._AddTransactionHead
            btnEdit.Enabled = User._Object.Privilege._EditTransactionHead

            'Sohail (18 Feb 2019) -- Start
            'NMB Enhancement - 76.1 - Provide setting on transaction head to mark as default heads.
            'btnImportTranhead.Enabled = User._Object.Privilege._AllowImportTransactionHead
            'btnGenerateHeads.Enabled = User._Object.Privilege._AddTransactionHead 'Sohail (24 Jun 2011)
            mnuImportTranhead.Enabled = User._Object.Privilege._AllowImportTransactionHead
            mnuGenerateHeads.Enabled = User._Object.Privilege._AddTransactionHead
            mnuSetAsDefault.Enabled = User._Object.Privilege._EditTransactionHead
            mnuClearDefault.Enabled = User._Object.Privilege._EditTransactionHead
            'Sohail (18 Feb 2019) -- End

            'Anjan (25 Oct 2012)-Start
            'ENHANCEMENT : TRA COMMENTS on Andrew's Request
            btnActive.Enabled = User._Object.Privilege._AllowtoSetTranHeadActive
            'Anjan (25 Oct 2012)-End 


            'Varsha Rana (17-Oct-2017) -- Start
            'Enhancement - Give user privileges.
            'btnDelete.Enabled = User._Object.Privilege._DeleteTransactionHead
            btnDelete.Enabled = User._Object.Privilege._AllowToSetTranHeadInactive
            btnVoid.Enabled = User._Object.Privilege._DeleteTransactionHead
            'Varsha Rana (17-Oct-2017) -- End


        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetVisibility", mstrModuleName)
        End Try

    End Sub
#End Region

#Region " Form's Events "

    Private Sub frmTransactionHeadList_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown
        Try
            Select Case e.KeyCode
                Case Keys.Return
                    SendKeys.Send("{TAB}")
                Case Keys.Escape
                    Me.Close()
                Case Keys.Delete
                    Call btnDelete.PerformClick()
            End Select
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmTransactionHeadList_KeyDown", mstrModuleName)
        End Try
    End Sub

    Private Sub frmTransactionHeadList_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        objTransactionHead = New clsTransactionHead
        Try
            Call Set_Logo(Me, gApplicationType)
            Call Language.setLanguage(Me.Name)
            'Anjan (02 Sep 2011)-Start
            'Issue : Including Language Settings.
            Call OtherSettings()
            'Anjan (02 Sep 2011)-End 
            Call SetVisibility()
            Call SetColor()
            Call FillCombo()
            Call FillList()

            'If cboTrnHeadType.Items.Count > 0 Then cboTrnHeadType.SelectedIndex = 0
            If lvTrnHeadList.Items.Count > 0 Then lvTrnHeadList.Items(0).Selected = True
            lvTrnHeadList.Select()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmTransactionHeadList_Load", mstrModuleName)
        End Try
    End Sub
    'Anjan (02 Sep 2011)-Start
    'Issue : Including Language Settings.
    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsTransactionHead.SetMessages()
            objfrm._Other_ModuleNames = "clsTransactionHead"
            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub
    'Anjan (02 Sep 2011)-End 
#End Region

#Region " Button's Events "

    Private Sub btnNew_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNew.Click
        Try
            Dim objFrm As New frmTransactionHead_AddEdit
            'Anjan (02 Sep 2011)-Start
            'Issue : Including Language Settings.
            If User._Object._Isrighttoleft = True Then
                objFrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objFrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objFrm)
            End If
            'Anjan (02 Sep 2011)-End 

            If objFrm.displayDialog(-1, enAction.ADD_CONTINUE) Then
                Call FillList()
                Call FillCombo(True)
            End If
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "btnNew_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnEdit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnEdit.Click, lvTrnHeadList.DoubleClick
        'Sohail (24 Jun 2011) -- Start
        If User._Object.Privilege._EditTransactionHead = False Then Exit Sub
        'Sohail (24 Jun 2011) -- End
        If lvTrnHeadList.SelectedItems.Count < 1 Then
            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please select Transaction Head from the list to perform further operation."), enMsgBoxStyle.Information) '?1
            lvTrnHeadList.Select()
            Exit Sub
        End If
        'Sohail (15 Feb 2012) -- Start
        'TRA - ENHANCEMENT
        If CInt(cboActiveInactive.SelectedValue) = enTranHeadActiveInActive.INACTIVE Then
            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "Sorry! You can not Edit this Trasaction Head. This Trasaction Head is Inactive."), enMsgBoxStyle.Information) '?1
            lvTrnHeadList.Select()
            Exit Sub
        End If
        'Sohail (15 Feb 2012) -- End
        Dim frm As New frmTransactionHead_AddEdit

        'Anjan (02 Sep 2011)-Start
        'Issue : Including Language Settings.
        If User._Object._Isrighttoleft = True Then
            frm.RightToLeft = Windows.Forms.RightToLeft.Yes
            frm.RightToLeftLayout = True
            Call Language.ctlRightToLeftlayOut(frm)
        End If
        'Anjan (02 Sep 2011)-End 


        Try
            Dim intSelectedIndex As Integer
            intSelectedIndex = lvTrnHeadList.SelectedItems(0).Index

            If frm.displayDialog(CInt(lvTrnHeadList.SelectedItems(0).Tag), enAction.EDIT_ONE) Then
                Call FillList()
                Call FillCombo(True)
            End If
            frm = Nothing

            lvTrnHeadList.Items(intSelectedIndex).Selected = True
            lvTrnHeadList.EnsureVisible(intSelectedIndex)
            lvTrnHeadList.Select()
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "btnEdit_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub btnDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDelete.Click

        'Gajanan [1-July-2020] -- Start
        'Enhancement #ARUTI-1276 Assist to provide Email notification to some selected users when Transaction head is EDITED or ADDED
        Dim blnFlag As Boolean = False
        'Gajanan [1-July-2020] -- End

        If lvTrnHeadList.SelectedItems.Count < 1 Then
            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please select Transaction Head from the list to perform further operation."), enMsgBoxStyle.Information) '?1
            lvTrnHeadList.Select()
            Exit Sub
        End If
        'Sohail (23 Dec 2010) -- Start
        Select Case CInt(lvTrnHeadList.SelectedItems(0).SubItems(colhCalcType.Index).Tag)
            Case enCalcType.OnAttendance, enCalcType.DEFINED_SALARY, enCalcType.NET_PAY, enCalcType.TOTAL_EARNING, enCalcType.TOTAL_DEDUCTION, enCalcType.TAXABLE_EARNING_TOTAL, enCalcType.NON_TAXABLE_EARNING_TOTAL, enCalcType.NON_CASH_BENEFIT_TOTAL, enCalcType.NET_PAY_ROUNDING_ADJUSTMENT, enCalcType.EMPLOYER_CONTRIBUTION_PAYABLE 'Sohail (18 Jan 2012)
                'Hemant (22 Aug 2020) -- [enCalcType.EMPLOYER_CONTRIBUTION_PAYABLE]
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Sorry, You cannot delete this Transaction Head. Reason: This is Default Transaction Head."), enMsgBoxStyle.Information) '?2
                lvTrnHeadList.Select()
                Exit Sub
        End Select
        'Sohail (18 Apr 2016) - [NET_PAY_ROUNDING_ADJUSTMENT]
        'Sohail (23 Dec 2010) -- End

        'Sohail (17 Sep 2014) -- Start
        'Enhancement - System generated Base, OT and PH heads for PPA Activity.
        If CInt(lvTrnHeadList.SelectedItems(0).SubItems(colhTypeOf.Index).Tag) = enTypeOf.PAY_PER_ACTIVITY Then
            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 9, "Sorry, You cannot delete this PPA Transaction Head. Please go to PPA Activity Master to delete this Transaction Head."), enMsgBoxStyle.Information) '?2
            lvTrnHeadList.Select()
            Exit Sub
        End If
        'Sohail (17 Sep 2014) -- End

        'Sohail (15 Feb 2012) -- Start
        'TRA - ENHANCEMENT
        If CInt(cboActiveInactive.SelectedValue) = enTranHeadActiveInActive.INACTIVE Then
            Exit Sub
        End If
        'Sohail (15 Feb 2012) -- End

        'Sohail (18 Feb 2019) -- Start
        'NMB Enhancement - 76.1 - Provide setting on transaction head to mark as default heads.
        'If objTransactionHead.isUsed(CInt(lvTrnHeadList.SelectedItems(0).Tag)) Then
        '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Sorry, You cannot Inactive this Transaction Head. Reason: This Transaction Head is in use."), enMsgBoxStyle.Information) '?2
        '    lvTrnHeadList.Select()
        '    Exit Sub
        'End If
        Dim ds As DataSet = objTransactionHead.isUsedList(CInt(lvTrnHeadList.SelectedItems(0).Tag))
        If ds.Tables(0).Rows.Count > 0 Then
            ds.Tables(0).Columns.RemoveAt(0)
            ds.Tables(0).Columns.RemoveAt(0)
            Dim objValid As New frmCommonValidationList
            objValid.displayDialog(False, Language.getMessage(mstrModuleName, 2, "Sorry, You cannot Inactive this Transaction Head. Reason: This Transaction Head is in use."), ds.Tables(0))
            Exit Sub
        End If
        'Sohail (18 Feb 2019) -- End        

        Try
            Dim intSelectedIndex As Integer
            intSelectedIndex = lvTrnHeadList.SelectedItems(0).Index

            If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Are you sure you want to make Inactive this Transaction Head?"), CType(enMsgBoxStyle.Question + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.Yes Then

                'Sandeep [ 16 Oct 2010 ] -- Start
                'objTransactionHead.Void(CInt(lvTrnHeadList.SelectedItems(0).Tag), User._Object._Userunkid, Now, "") 'Sohail (14 Oct 2010)
                Dim frm As New frmReasonSelection
                Dim mstrVoidReason As String = String.Empty
                frm.displayDialog(enVoidCategoryType.PAYROLL, mstrVoidReason)
                If mstrVoidReason.Length <= 0 Then
                    Exit Sub
                End If

                'S.SANDEEP [28-May-2018] -- START
                'ISSUE/ENHANCEMENT : {Audit Trails} 
                objTransactionHead._FormName = mstrModuleName
                objTransactionHead._LoginEmployeeunkid = 0
                objTransactionHead._ClientIP = getIP()
                objTransactionHead._HostName = getHostName()
                objTransactionHead._FromWeb = False
                objTransactionHead._AuditUserId = User._Object._Userunkid
objTransactionHead._CompanyUnkid = Company._Object._Companyunkid
                objTransactionHead._AuditDate = ConfigParameter._Object._CurrentDateAndTime
                'S.SANDEEP [28-May-2018] -- END

                'Sohail (09 Nov 2013) -- Start
                'TRA - ENHANCEMENT
                'objTransactionHead.Void(CInt(lvTrnHeadList.SelectedItems(0).Tag), User._Object._Userunkid, ConfigParameter._Object._CurrentDateAndTime, mstrVoidReason)
                blnFlag = objTransactionHead.MakeActiveInactive(False, CInt(lvTrnHeadList.SelectedItems(0).Tag), User._Object._Userunkid, ConfigParameter._Object._CurrentDateAndTime, mstrVoidReason)
                'Sohail (09 Nov 2013) -- End
                'Sandeep [ 16 Oct 2010 ] -- End 


                'Gajanan [1-July-2020] -- Start
                'Enhancement #ARUTI-1276 Assist to provide Email notification to some selected users when Transaction head is EDITED or ADDED
                If blnFlag = False And objTransactionHead._Message <> "" Then
                    eZeeMsgBox.Show(objTransactionHead._Message, enMsgBoxStyle.Information)
                Else

                    Dim objConfig As New clsConfigOptions
                    Dim EmailUserids As String = objConfig.GetKeyValue(ConfigParameter._Object._Companyunkid, "AddEditTransectionHeadNotificationUserIds", Nothing)
                    Dim Ip As String = getIP()
                    Dim Hostname As String = getHostName()
                    objTransactionHead.SendMailTo_User(EmailUserids, ConfigParameter._Object._Companyunkid, mstrForm_Name, _
                                                         User._Object._Userunkid, clsEmployeeDataApproval.enOperationType.DELETED, _
                                                         lvTrnHeadList.SelectedItems(0).SubItems(colhName.Index).Text, Ip, Hostname, _
                                                         enLogin_Mode.DESKTOP, -1, -1, False, Nothing, False, enTranHeadActiveInActive.INACTIVE, True)
                lvTrnHeadList.SelectedItems(0).Remove()

                End If

                If lvTrnHeadList.Items.Count <= 0 Then
                    Exit Try
                End If

                If lvTrnHeadList.Items.Count = intSelectedIndex Then
                    intSelectedIndex = lvTrnHeadList.Items.Count - 1
                    lvTrnHeadList.Items(intSelectedIndex).Selected = True
                    lvTrnHeadList.EnsureVisible(intSelectedIndex)
                ElseIf lvTrnHeadList.Items.Count <> 0 Then
                    lvTrnHeadList.Items(intSelectedIndex).Selected = True
                    lvTrnHeadList.EnsureVisible(intSelectedIndex)
                End If

                'Gajanan [1-July-2020] -- End


            End If
            lvTrnHeadList.Select()
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "btnDelete_Click", mstrModuleName)
        End Try
    End Sub

    'Sohail (15 Feb 2012) -- Start
    'TRA - ENHANCEMENT
    Private Sub btnActive_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnActive.Click
        'Gajanan [1-July-2020] -- Start
        'Enhancement #ARUTI-1276 Assist to provide Email notification to some selected users when Transaction head is EDITED or ADDED
        Dim blnFlag As Boolean = False
        'Gajanan [1-July-2020] -- End

        If lvTrnHeadList.SelectedItems.Count < 1 Then
            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please select Transaction Head from the list to perform further operation."), enMsgBoxStyle.Information) '?1
            lvTrnHeadList.Select()
            Exit Sub
        End If
        If CInt(cboActiveInactive.SelectedValue) = enTranHeadActiveInActive.ACTIVE Then
            Exit Sub
        End If
        Dim blnResult As Boolean
        Try

            If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 6, "Are you sure you want to make Active this Transaction Head?"), CType(enMsgBoxStyle.Question + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.No Then
                Exit Sub
            End If

                'S.SANDEEP [28-May-2018] -- START
                'ISSUE/ENHANCEMENT : {Audit Trails} 
                objTransactionHead._FormName = mstrModuleName
                objTransactionHead._LoginEmployeeunkid = 0
                objTransactionHead._ClientIP = getIP()
                objTransactionHead._HostName = getHostName()
                objTransactionHead._FromWeb = False
                objTransactionHead._AuditUserId = User._Object._Userunkid
objTransactionHead._CompanyUnkid = Company._Object._Companyunkid
                objTransactionHead._AuditDate = ConfigParameter._Object._CurrentDateAndTime
                'S.SANDEEP [28-May-2018] -- END

            'Sohail (09 Nov 2013) -- Start
            'TRA - ENHANCEMENT
            'blnResult = objTransactionHead.MakeActive(CInt(lvTrnHeadList.SelectedItems(0).Tag), User._Object._Userunkid)
            blnResult = objTransactionHead.MakeActiveInactive(True, CInt(lvTrnHeadList.SelectedItems(0).Tag), User._Object._Userunkid, Nothing, "")
            'Sohail (09 Nov 2013) -- End
            If blnResult = False AndAlso objTransactionHead._Message <> "" Then
                eZeeMsgBox.Show(objTransactionHead._Message, enMsgBoxStyle.Information)
            Else

                'Gajanan [1-July-2020] -- Start
                'Enhancement #ARUTI-1276 Assist to provide Email notification to some selected users when Transaction head is EDITED or ADDED
                Dim objConfig As New clsConfigOptions
                Dim EmailUserids As String = objConfig.GetKeyValue(ConfigParameter._Object._Companyunkid, "AddEditTransectionHeadNotificationUserIds", Nothing)
                Dim Ip As String = getIP()
                Dim Hostname As String = getHostName()
                objTransactionHead.SendMailTo_User(EmailUserids, ConfigParameter._Object._Companyunkid, mstrForm_Name, _
                                                     User._Object._Userunkid, clsEmployeeDataApproval.enOperationType.ADDED, _
                                                     lvTrnHeadList.SelectedItems(0).SubItems(colhName.Index).Text, Ip, Hostname, _
                                                     enLogin_Mode.DESKTOP, -1, -1, False, Nothing, False, enTranHeadActiveInActive.ACTIVE, True)
                'Gajanan [1-July-2020] -- End
                Call FillList()
                Call FillCombo(True)
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnActive_Click", mstrModuleName)
        End Try
    End Sub
    'Sohail (15 Feb 2012) -- End

    'Sohail (09 Nov 2013) -- Start
    'TRA - ENHANCEMENT
    Private Sub btnVoid_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnVoid.Click
        'Gajanan [1-July-2020] -- Start
        'Enhancement #ARUTI-1276 Assist to provide Email notification to some selected users when Transaction head is EDITED or ADDED
        Dim blnFlag As Boolean = False
        'Gajanan [1-July-2020] -- End

        If lvTrnHeadList.SelectedItems.Count <= 0 Then
            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please select Transaction Head from the list to perform further operation."), enMsgBoxStyle.Information) '?1
            lvTrnHeadList.Select()
            Exit Sub
        End If
        Select Case CInt(lvTrnHeadList.SelectedItems(0).SubItems(colhCalcType.Index).Tag)
            Case enCalcType.OnAttendance, enCalcType.DEFINED_SALARY, enCalcType.NET_PAY, enCalcType.TOTAL_EARNING, enCalcType.TOTAL_DEDUCTION, enCalcType.TAXABLE_EARNING_TOTAL, enCalcType.NON_TAXABLE_EARNING_TOTAL, enCalcType.NON_CASH_BENEFIT_TOTAL, enCalcType.NET_PAY_ROUNDING_ADJUSTMENT, enCalcType.EMPLOYER_CONTRIBUTION_PAYABLE 'Sohail (18 Jan 2012)
                'Hemant (22 Aug 2020) -- [enCalcType.EMPLOYER_CONTRIBUTION_PAYABLE]
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Sorry, You cannot delete this Transaction Head. Reason: This is Default Transaction Head."), enMsgBoxStyle.Information) '?2
                lvTrnHeadList.Select()
                Exit Sub
        End Select
        'Sohail (18 Apr 2016) - [NET_PAY_ROUNDING_ADJUSTMENT]

        'Sohail (17 Sep 2014) -- Start
        'Enhancement - System generated Base, OT and PH heads for PPA Activity.
        If CInt(lvTrnHeadList.SelectedItems(0).SubItems(colhTypeOf.Index).Tag) = enTypeOf.PAY_PER_ACTIVITY Then
            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Sorry, You cannot delete this Transaction Head. Reason: This is Default Transaction Head."), enMsgBoxStyle.Information) '?2
            lvTrnHeadList.Select()
            Exit Sub
        End If
        'Sohail (17 Sep 2014) -- End

        If CInt(cboActiveInactive.SelectedValue) = enTranHeadActiveInActive.INACTIVE Then
            Exit Sub
        End If

        'Sohail (18 Feb 2019) -- Start
        'NMB Enhancement - 76.1 - Provide setting on transaction head to mark as default heads.
        'If objTransactionHead.isUsed(CInt(lvTrnHeadList.SelectedItems(0).Tag)) Then
        '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 7, "Sorry, You cannot Void this Transaction Head. Reason: This Transaction Head is in use."), enMsgBoxStyle.Information) '?2
        '    lvTrnHeadList.Select()
        '    Exit Sub
        'End If
        Dim ds As DataSet = objTransactionHead.isUsedList(CInt(lvTrnHeadList.SelectedItems(0).Tag))
        If ds.Tables(0).Rows.Count > 0 Then
            ds.Tables(0).Columns.RemoveAt(0)
            ds.Tables(0).Columns.RemoveAt(0)
            Dim objValid As New frmCommonValidationList
            objValid.displayDialog(False, Language.getMessage(mstrModuleName, 7, "Sorry, You cannot Void this Transaction Head. Reason: This Transaction Head is in use."), ds.Tables(0))
            Exit Sub
        End If
        'Sohail (18 Feb 2019) -- End

        Try
            Dim intSelectedIndex As Integer
            intSelectedIndex = lvTrnHeadList.SelectedItems(0).Index

            If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 8, "Are you sure you want to make Void this Transaction Head?"), CType(enMsgBoxStyle.Question + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.Yes Then

                Dim frm As New frmReasonSelection
                Dim mstrVoidReason As String = String.Empty
                frm.displayDialog(enVoidCategoryType.PAYROLL, mstrVoidReason)
                If mstrVoidReason.Length <= 0 Then
                    Exit Sub
                End If

                'S.SANDEEP [28-May-2018] -- START
                'ISSUE/ENHANCEMENT : {Audit Trails} 
                objTransactionHead._FormName = mstrModuleName
                objTransactionHead._LoginEmployeeunkid = 0
                objTransactionHead._ClientIP = getIP()
                objTransactionHead._HostName = getHostName()
                objTransactionHead._FromWeb = False
                objTransactionHead._AuditUserId = User._Object._Userunkid
objTransactionHead._CompanyUnkid = Company._Object._Companyunkid
                objTransactionHead._AuditDate = ConfigParameter._Object._CurrentDateAndTime
                'S.SANDEEP [28-May-2018] -- END

                'Sohail (21 Aug 2015) -- Start
                'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                'objTransactionHead.Void(CInt(lvTrnHeadList.SelectedItems(0).Tag), User._Object._Userunkid, ConfigParameter._Object._CurrentDateAndTime, mstrVoidReason)
                blnFlag = objTransactionHead.Void(FinancialYear._Object._DatabaseName, CInt(lvTrnHeadList.SelectedItems(0).Tag), User._Object._Userunkid, ConfigParameter._Object._CurrentDateAndTime, mstrVoidReason)
                'Sohail (21 Aug 2015) -- End


                'Gajanan [1-July-2020] -- Start
                'Enhancement #ARUTI-1276 Assist to provide Email notification to some selected users when Transaction head is EDITED or ADDED
                If blnFlag = False And objTransactionHead._Message <> "" Then
                    eZeeMsgBox.Show(objTransactionHead._Message, enMsgBoxStyle.Information)
                Else

                    Dim objConfig As New clsConfigOptions
                    Dim EmailUserids As String = objConfig.GetKeyValue(ConfigParameter._Object._Companyunkid, "AddEditTransectionHeadNotificationUserIds", Nothing)
                    Dim Ip As String = getIP()
                    Dim Hostname As String = getHostName()
                    objTransactionHead.SendMailTo_User(EmailUserids, ConfigParameter._Object._Companyunkid, mstrForm_Name, _
                                                         User._Object._Userunkid, clsEmployeeDataApproval.enOperationType.DELETED, _
                                                         lvTrnHeadList.SelectedItems(0).SubItems(colhName.Index).Text, Ip, Hostname, _
                                                         enLogin_Mode.DESKTOP)
                lvTrnHeadList.SelectedItems(0).Remove()

                End If

                'Gajanan [1-July-2020] -- End

                If lvTrnHeadList.Items.Count <= 0 Then
                    Exit Try
                End If

                If lvTrnHeadList.Items.Count = intSelectedIndex Then
                    intSelectedIndex = lvTrnHeadList.Items.Count - 1
                    lvTrnHeadList.Items(intSelectedIndex).Selected = True
                    lvTrnHeadList.EnsureVisible(intSelectedIndex)
                ElseIf lvTrnHeadList.Items.Count <> 0 Then
                    lvTrnHeadList.Items(intSelectedIndex).Selected = True
                    lvTrnHeadList.EnsureVisible(intSelectedIndex)
                End If
            End If
            lvTrnHeadList.Select()
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "btnVoid_Click", mstrModuleName)
        End Try
    End Sub
    'Sohail (09 Nov 2013) -- End

    'Pinkal(22-Dec-2010)--- Start

    Private Sub btnImportTranhead_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuImportTranhead.Click
        'Sohail (18 Feb 2019) - [btnImportTranhead.Click] = [mnuImportTranhead.Click]
        Try
            Dim objfrm As New frmImportTranHeads
            objfrm.ShowDialog()

            'Pinkal (10-Mar-2011) -- Start
            FillCombo()
            FillList()
            'Pinkal (10-Mar-2011) -- End
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnImportTranhead_Click", mstrModuleName)
        End Try
    End Sub

    'Pinkal(22-Dec-2010)--- End

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Me.Close()
    End Sub

    'Sohail (26 May 2011) -- Start
    Private Sub btnGenerateHeads_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuGenerateHeads.Click
        'Sohail (18 Feb 2019) - [btnGenerateHeads.Click] = [mnuGenerateHeads.Click]
        Try
            Dim objFrm As New frmGenerateTranHead
            If objFrm.displayDialog(-1, enAction.ADD_ONE) = True Then
                Call FillCombo()
                Call FillList()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnGenerateHeads_Click", mstrModuleName)
        End Try
    End Sub
    'Sohail (26 May 2011) -- End

    'Sohail (18 Feb 2019) -- Start
    'NMB Enhancement - 76.1 - Provide setting on transaction head to mark as default heads.
    Private Sub mnuSetAsDefault_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuSetAsDefault.Click, mnuClearDefault.Click
        Dim blnSetDefault As Boolean = False
        Try
            Dim mnu As ToolStripMenuItem = DirectCast(sender, ToolStripMenuItem)
            If mnu.Name = mnuSetAsDefault.Name Then
                blnSetDefault = True
            End If
            If lvTrnHeadList.SelectedItems.Count <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please select Transaction Head from the list to perform further operation."), enMsgBoxStyle.Information)
                lvTrnHeadList.Focus()
                Exit Sub
            ElseIf CInt(lvTrnHeadList.SelectedItems(0).SubItems(colhTypeOf.Index).Tag) = enTypeOf.Salary Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 10, "Sorry, You cannot set Salary transaction head as Default transaction head."), enMsgBoxStyle.Information)
                lvTrnHeadList.Focus()
                Exit Sub
            ElseIf blnSetDefault = True AndAlso CBool(lvTrnHeadList.SelectedItems(0).SubItems(colhDefault.Index).Tag) = True Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 11, "Sorry, This transaction head is already set as Default transaction head."), enMsgBoxStyle.Information)
                lvTrnHeadList.Focus()
                Exit Sub
            ElseIf blnSetDefault = False AndAlso CBool(lvTrnHeadList.SelectedItems(0).SubItems(colhDefault.Index).Tag) = False Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 12, "Sorry, This transaction head is already not set as Default transaction head."), enMsgBoxStyle.Information)
                lvTrnHeadList.Focus()
                Exit Sub
            ElseIf CInt(lvTrnHeadList.SelectedItems(0).SubItems(colhCalcType.Index).Tag) = enCalcType.ROUNDING_ADJUSTMENT OrElse CInt(lvTrnHeadList.SelectedItems(0).SubItems(colhCalcType.Index).Tag) = enCalcType.NET_PAY_ROUNDING_ADJUSTMENT _
                    OrElse CInt(lvTrnHeadList.SelectedItems(0).SubItems(colhCalcType.Index).Tag) = enCalcType.PAY_PER_ACTIVITY OrElse CInt(lvTrnHeadList.SelectedItems(0).SubItems(colhCalcType.Index).Tag) = enCalcType.PAY_PER_ACTIVITY_OT OrElse CInt(lvTrnHeadList.SelectedItems(0).SubItems(colhCalcType.Index).Tag) = enCalcType.PAY_PER_ACTIVITY_PH Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 13, "Sorry, You cannot set this transaction head as Default transaction head as it cannot be assigned manually."), enMsgBoxStyle.Information)
                lvTrnHeadList.Focus()
                Exit Sub
            End If

            Dim ds As DataSet = objTransactionHead.isUsedList(CInt(lvTrnHeadList.SelectedItems(0).Tag))
            If ds.Tables(0).Rows.Count > 0 Then
                If ds.Tables(0).Select("TableName = 'hrmembership_master' ").Length > 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 14, "Sorry, You cannot set this head as Default transaction head as it is mapped with Membership."), enMsgBoxStyle.Information)
                    lvTrnHeadList.Focus()
                    Exit Sub
                End If
            End If

            If objTransactionHead.UpdateIsDefault(blnSetDefault, CInt(lvTrnHeadList.SelectedItems(0).Tag), User._Object._Userunkid, ConfigParameter._Object._CurrentDateAndTime) = False Then
                If objTransactionHead._Message <> "" Then
                    eZeeMsgBox.Show(objTransactionHead._Message, enMsgBoxStyle.Information)
                End If
            Else
                lvTrnHeadList.Tag = lvTrnHeadList.SelectedItems(0).Index
                Call FillList()
                lvTrnHeadList.Items(CInt(lvTrnHeadList.Tag)).Selected = True
                lvTrnHeadList.EnsureVisible(CInt(lvTrnHeadList.Tag))
                lvTrnHeadList.Select()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "mnuSetAsDefault_Click", mstrModuleName)
        End Try
    End Sub
    'Sohail (18 Feb 2019) -- End

#End Region

    'Sohail (15 Feb 2012) -- Start
    'TRA - ENHANCEMENT
#Region " Combobox's Events "
    Private Sub cboActiveInactive_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboActiveInactive.SelectedIndexChanged
        Try
            Call FillList()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboActiveInactive_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub
#End Region
    'Sohail (15 Feb 2012) -- End

#Region " Other Control's Events "
    Private Sub objbtnSearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearch.Click
        Try
            Call FillList()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearch_Click", mstrModuleName)
        End Try

    End Sub

    Private Sub objbtnReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnReset.Click
        Try
            If cboTrnHead.Items.Count > 0 Then cboTrnHead.SelectedIndex = 0
            If cboTrnHeadType.Items.Count > 0 Then cboTrnHeadType.SelectedIndex = 0
            Call FillList()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnReset_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnSearchTrnHead_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchTrnHead.Click
        Dim objfrm As New frmCommonSearch
        'Sohail (17 Sep 2014) -- Start
        'Enhancement - System generated Base, OT and PH heads for PPA Activity.
        'Dim objTranHead As New clsTransactionHead
        'Dim dsList As DataSet
        'Sohail (17 Sep 2014) -- End
        Try
            'Anjan (02 Sep 2011)-Start
            'Issue : Including Language Settings.
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If
            'Anjan (02 Sep 2011)-End 

            'dsList = objTranHead.getComboList("TranHeadList") 'Sohail (17 Sep 2014)
            With cboTrnHead
                'Sohail (17 Sep 2014) -- Start
                'Enhancement - System generated Base, OT and PH heads for PPA Activity.
                'objfrm.DataSource = dsList.Tables("TranHeadList")
                objfrm.DataSource = CType(.DataSource, DataTable)
                'Sohail (17 Sep 2014) -- End
                objfrm.ValueMember = .ValueMember
                objfrm.DisplayMember = .DisplayMember
                objfrm.CodeMember = "Code"
                If objfrm.DisplayDialog Then
                    .SelectedValue = objfrm.SelectedValue
                End If
                .Focus()
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchTrnHead_Click", mstrModuleName)
        Finally
            objfrm = Nothing
            'objTranHead = Nothing 'Sohail (17 Sep 2014)
        End Try
    End Sub
#End Region

#Region " Message List "
    '1, "Please select Transaction Head from the list to perform further operation on it."
    '2, "Sorry, You cannot delete this Transaction Head. Reason: This Transaction Head is in use."
    '3, "Are you sure you want to delete this Transaction Head?"
    '4, "Sorry, You cannot delete this Transaction Head. Reason: This is Default Transaction Head."
#End Region

	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Private Sub OtherSettings()
		Try
			Me.SuspendLayout()
			
			Call SetLanguage()
			
			Me.gbFilterCriteria.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbFilterCriteria.ForeColor = GUI._eZeeContainerHeaderForeColor 

            Me.eZeeHeader.GradientColor1 = GUI._HeaderBackColor1
			Me.eZeeHeader.GradientColor2 = GUI._HeaderBackColor2 
			Me.eZeeHeader.BorderColor = GUI._HeaderBorderColor 
			Me.eZeeHeader.HeaderTextForeColor = GUI._HeaderTitleFontColor 
			Me.eZeeHeader.DescriptionForeColor = GUI._HeaderMessageFontColor 


			Me.btnDelete.GradientBackColor = GUI._ButttonBackColor 
			Me.btnDelete.GradientForeColor = GUI._ButttonFontColor

			Me.btnClose.GradientBackColor = GUI._ButttonBackColor 
			Me.btnClose.GradientForeColor = GUI._ButttonFontColor

			Me.btnEdit.GradientBackColor = GUI._ButttonBackColor 
			Me.btnEdit.GradientForeColor = GUI._ButttonFontColor

			Me.btnNew.GradientBackColor = GUI._ButttonBackColor 
			Me.btnNew.GradientForeColor = GUI._ButttonFontColor

			Me.btnActive.GradientBackColor = GUI._ButttonBackColor 
			Me.btnActive.GradientForeColor = GUI._ButttonFontColor

			Me.btnVoid.GradientBackColor = GUI._ButttonBackColor 
			Me.btnVoid.GradientForeColor = GUI._ButttonFontColor

			Me.btnOperations.GradientBackColor = GUI._ButttonBackColor 
			Me.btnOperations.GradientForeColor = GUI._ButttonFontColor


			Me.ResumeLayout()
		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "OtherSettings", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetLanguage()
		Try
			Me.Text = Language._Object.getCaption(Me.Name, Me.Text)
			
			Me.eZeeHeader.Title = Language._Object.getCaption(Me.eZeeHeader.Name & "_Title" , Me.eZeeHeader.Title)
			Me.eZeeHeader.Message = Language._Object.getCaption(Me.eZeeHeader.Name & "_Message" , Me.eZeeHeader.Message)
			Me.colhCode.Text = Language._Object.getCaption(CStr(Me.colhCode.Tag), Me.colhCode.Text)
			Me.colhName.Text = Language._Object.getCaption(CStr(Me.colhName.Tag), Me.colhName.Text)
			Me.colhTranHeadType.Text = Language._Object.getCaption(CStr(Me.colhTranHeadType.Tag), Me.colhTranHeadType.Text)
			Me.gbFilterCriteria.Text = Language._Object.getCaption(Me.gbFilterCriteria.Name, Me.gbFilterCriteria.Text)
			Me.lblTrnHead.Text = Language._Object.getCaption(Me.lblTrnHead.Name, Me.lblTrnHead.Text)
			Me.lblTrnHeadType.Text = Language._Object.getCaption(Me.lblTrnHeadType.Name, Me.lblTrnHeadType.Text)
			Me.btnDelete.Text = Language._Object.getCaption(Me.btnDelete.Name, Me.btnDelete.Text)
			Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
			Me.btnEdit.Text = Language._Object.getCaption(Me.btnEdit.Name, Me.btnEdit.Text)
			Me.btnNew.Text = Language._Object.getCaption(Me.btnNew.Name, Me.btnNew.Text)
			Me.colhTypeOf.Text = Language._Object.getCaption(CStr(Me.colhTypeOf.Tag), Me.colhTypeOf.Text)
			Me.colhCalcType.Text = Language._Object.getCaption(CStr(Me.colhCalcType.Tag), Me.colhCalcType.Text)
			Me.lblActiveInactive.Text = Language._Object.getCaption(Me.lblActiveInactive.Name, Me.lblActiveInactive.Text)
			Me.btnActive.Text = Language._Object.getCaption(Me.btnActive.Name, Me.btnActive.Text)
			Me.btnVoid.Text = Language._Object.getCaption(Me.btnVoid.Name, Me.btnVoid.Text)
			Me.mnuSetAsDefault.Text = Language._Object.getCaption(Me.mnuSetAsDefault.Name, Me.mnuSetAsDefault.Text)
			Me.mnuClearDefault.Text = Language._Object.getCaption(Me.mnuClearDefault.Name, Me.mnuClearDefault.Text)
			Me.mnuImportTranhead.Text = Language._Object.getCaption(Me.mnuImportTranhead.Name, Me.mnuImportTranhead.Text)
			Me.mnuGenerateHeads.Text = Language._Object.getCaption(Me.mnuGenerateHeads.Name, Me.mnuGenerateHeads.Text)
			Me.btnOperations.Text = Language._Object.getCaption(Me.btnOperations.Name, Me.btnOperations.Text)
			Me.colhDefault.Text = Language._Object.getCaption(CStr(Me.colhDefault.Tag), Me.colhDefault.Text)

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetLanguage", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "Please select Transaction Head from the list to perform further operation on it.")
			Language.setMessage(mstrModuleName, 2, "Sorry, You cannot Inactive this Transaction Head. Reason: This Transaction Head is in use.")
			Language.setMessage(mstrModuleName, 3, "Are you sure you want to make Inactive this Transaction Head?")
			Language.setMessage(mstrModuleName, 4, "Sorry, You cannot delete this Transaction Head. Reason: This is Default Transaction Head.")
			Language.setMessage(mstrModuleName, 5, "Sorry! You can not Edit this Trasaction Head. This Trasaction Head is Inactive.")
			Language.setMessage(mstrModuleName, 6, "Are you sure you want to make Active this Transaction Head?")
			Language.setMessage(mstrModuleName, 7, "Sorry, You cannot Void this Transaction Head. Reason: This Transaction Head is in use.")
			Language.setMessage(mstrModuleName, 8, "Are you sure you want to make Void this Transaction Head?")
			Language.setMessage(mstrModuleName, 9, "Sorry, You cannot delete this PPA Transaction Head. Please go to PPA Activity Master to delete this Transaction Head.")
			Language.setMessage(mstrModuleName, 10, "Sorry, You cannot set Salary transaction head as Default transaction head.")
			Language.setMessage(mstrModuleName, 11, "Sorry, This transaction head is already set as Default transaction head.")
			Language.setMessage(mstrModuleName, 12, "Sorry, This transaction head is already not set as Default transaction head.")
			Language.setMessage(mstrModuleName, 13, "Sorry, You cannot set this transaction head as Default transaction head as it cannot be assigned manually.")
			Language.setMessage(mstrModuleName, 14, "Sorry, You cannot set this head as Default transaction head as it is mapped with Membership.")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class