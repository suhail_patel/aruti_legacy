﻿Option Strict On

Imports eZeeCommonLib
Imports Aruti.Data

Public Class frmTransactionHead_AddEdit

#Region " Private Variables "
    Private ReadOnly mstrModuleName As String = "frmTransactionHead_AddEdit"

    Private mblnCancel As Boolean = True

    Private objTransactionHead As clsTransactionHead

    Private objTranSimpleSlab As clsTranheadSlabTran
    Private mdtSimpleSlab As DataTable

    Private objTranTaxSlab As clsTranheadInexcessslabTran
    Private mdtTaxSlab As DataTable

    Private objFormula As clsTranheadFormulaTran
    Private mdtFormula As DataTable
    'Sohail (03 Sep 2012) -- Start
    'TRA - ENHANCEMENT
    Private objFormulaCurrency As clsTranheadFormulaCurrencyTran
    Private mdtFormulaCurrency As DataTable
    'Sohail (03 Sep 2012) -- End
    'Sohail (09 Oct 2012) -- Start
    'TRA - ENHANCEMENT
    Private objFormulaLeave As clsTranheadFormulaLeaveTran
    Private mdtFormulaLeave As DataTable
    'Sohail (09 Oct 2012) -- End

    'Sohail (12 Apr 2013) -- Start
    'TRA - ENHANCEMENT
    Private objFormulaSlabTran As clsTranhead_formula_slab_Tran
    Private mdtFormulaSlab As DataTable
    'Sohail (12 Apr 2013) -- End
    'Sohail (21 Jun 2013) -- Start
    'TRA - ENHANCEMENT
    Private objFormulaMeasurementUnit As clsTranhead_formula_measurement_unit_Tran
    Private mdtFormulaMeasurementUnit As DataTable
    Private objFormulaActivityUnit As clsTranhead_formula_activity_unit_Tran
    Private mdtFormulaActivityUnit As DataTable
    Private objFormulaMeasurementAmount As clsTranhead_formula_measurement_amount_Tran
    Private mdtFormulaMeasurementAmount As DataTable
    Private objFormulaActivityAmount As clsTranhead_formula_activity_amount_Tran
    Private mdtFormulaActivityAmount As DataTable
    'Sohail (21 Jun 2013) -- End

    'Sohail (29 Oct 2013) -- Start
    'TRA - ENHANCEMENT
    Private objFormulaShift As clsTranheadFormulaShiftTran
    Private mdtFormulaShift As DataTable
    'Sohail (29 Oct 2013) -- End
    'Sohail (09 Nov 2013) -- Start
    'TRA - ENHANCEMENT
    Private objFormulaCumulative As clsTranheadFormulaCumulativeTran
    Private mdtFormulaCumulative As DataTable
    'Sohail (09 Nov 2013) -- End

    'Sohail (02 Oct 2014) -- Start
    'Enhancement - No Of Active Inactive Dependants RELATION WISE.
    Private objFormulaCMaster As clsTranheadFormulaCMasterTran
    Private mdtFormulaCMaster As DataTable
    'Sohail (02 Oct 2014) -- End

    'Sohail (12 Nov 2014) -- Start
    'Voltamp Enhancement - Quentity Total and Amount Total for Leave Claim Request in transaction head formula.
    Private objFormulaCRExpense As clsTranheadFormulaCRExpenseTran
    Private mdtFormulaCRExpense As DataTable
    'Sohail (12 Nov 2014) -- End

    'Sohail (24 Oct 2017) -- Start
    'CCBRT Enhancement - 70.1 - (Ref. Id - 73) - Add a loan function (like the leave function) in payroll where you can select loan scheme, interest or EMI, total loan amount, etc.
    Private objFormulaLoanScheme As clsTranheadFormulaLoanSchemeTran
    Private mdtFormulaLoanScheme As DataTable
    'Sohail (24 Oct 2017) -- End

    Private menAction As enAction = enAction.ADD_ONE
    Private mintTranHeadUnkid As Integer = -1
    Private mintItemIndex As Integer = -1
    Private mblnIsFromEmployee As Boolean = False
    'Anjan (01 Nov 2010)-Start
    Private mblnIsFromBenefit As Boolean = False
    'Anjan (01 Nov 2010)-End

    'Sohail (21 Nov 2011) -- Start
    Private mdtPeriod_startdate As DateTime
    Private mdtPeriod_enddate As DateTime
    Private mintPeriod_Status As Integer
    'Sohail (21 Nov 2011) -- End
#End Region

#Region " Display Dialog "
    Public Function displayDialog(ByRef intUnkId As Integer, ByVal eAction As enAction, Optional ByVal IsFromEmp As Boolean = False, Optional ByVal IsFromBenefit As Boolean = False) As Boolean 'Anjan (01 Nov 2010)
        Try
            mintTranHeadUnkid = intUnkId
            menAction = eAction
            mblnIsFromEmployee = IsFromEmp
            mblnIsFromBenefit = IsFromBenefit
            Me.ShowDialog()

            intUnkId = mintTranHeadUnkid

            Return Not mblnCancel
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "displayDialog", mstrModuleName)
        End Try
    End Function
#End Region

#Region " Private Methods "
    Private Sub setColor()
        Try
            txtHeadCode.BackColor = GUI.ColorComp
            txtHeadName.BackColor = GUI.ColorComp
            cboTrnHeadType.BackColor = GUI.ColorComp
            cboTypeOf.BackColor = GUI.ColorComp
            cboCalcType.BackColor = GUI.ColorComp
            cboComputeOn.BackColor = GUI.ColorComp

            txtSimpleSlabAmtUpto.BackColor = GUI.ColorComp
            cboSimpleSlabAmountType.BackColor = GUI.ColorComp
            txtSimpleSlabValueBasis.BackColor = GUI.ColorComp
            'Sohail (21 Nov 2011) -- Start
            cboSimpleSlabPeriod.BackColor = GUI.ColorComp
            cboTaxSlabPeriod.BackColor = GUI.ColorComp
            'Sohail (21 Nov 2011) -- End

            txtTaxSlabAmtUpto.BackColor = GUI.ColorComp
            txtTaxSlabFixedAmount.BackColor = GUI.ColorComp
            txtTaxSlabRatePayable.BackColor = GUI.ColorComp
            'Hemant (06 Jul 2020) -- Start
            'ENHANCEMENT (NMB): Posting a transaction head to one cost centre for all employees without mapping on employee cost centre screen (cost center dropdown list on head master and claim expense master and pick cost center from head master instead of employee master on process payroll if not mapped on employee cost center screen)
            cboDefaultCostCenter.BackColor = GUI.ColorComp
            'Hemant (06 Jul 2020) -- End
            'Sohail (03 Dec 2020) -- Start
            'Freight Forwarders Kenya Ltd Enhancement : # OLD-51 : Provide rounding options at transaction head level.
            cboRoundOffTypeId.BackColor = GUI.ColorOptional
            'Sohail (03 Dec 2020) -- End
            'Sohail (11 Dec 2020) -- Start
            'Netis Togo Enhancement # OLD-218 : - Give option to Round Up/Round Down/Auto on transaction head next to Rounding OFF Type.
            cboRoundOffModeId.BackColor = GUI.ColorOptional
            'Sohail (11 Dec 2020) -- End
            nudPriority.BackColor = GUI.ColorOptional 'Sohail (08 May 2021)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "setColor", mstrModuleName)
        End Try
    End Sub

    Private Sub GetValue()
        Try
            txtHeadCode.Text = objTransactionHead._Trnheadcode
            txtHeadName.Text = objTransactionHead._Trnheadname
            cboTrnHeadType.SelectedValue = objTransactionHead._Trnheadtype_Id
            cboTypeOf.SelectedValue = objTransactionHead._Typeof_id
            'Sohail (06 Aug 2010) -- Start
            'Sohail (11 Sep 2010) -- Start
            cboCalcType.SelectedValue = objTransactionHead._Calctype_Id
            'If objTransactionHead._Calctype_Id = enCalcType.DEFINED_SALARY Or _
            '    objTransactionHead._Calctype_Id = enCalcType.OnHourWorked Or _
            '    objTransactionHead._Calctype_Id = enCalcType.OnAttendance Then

            '    cboCalcType.SelectedValue = enCalcType.OnAttendance
            'Else
            'cboCalcType.SelectedValue = objTransactionHead._Calctype_Id
            'End If
            'Sohail (11 Sep 2010) -- End
            'Sohail (06 Aug 2010) -- End

            RemoveHandler chkRecurrent.CheckedChanged, AddressOf chkRecurrent_CheckedChanged 'Sohail (26 May 2011)
            chkRecurrent.Checked = objTransactionHead._Isrecurrent
            AddHandler chkRecurrent.CheckedChanged, AddressOf chkRecurrent_CheckedChanged 'Sohail (26 May 2011)
            chkAppearOnPayslip.Checked = objTransactionHead._Isappearonpayslip
            chkTaxable.Checked = objTransactionHead._Istaxable 'Sohail (18 Nov 2010) 
            chkTaxRelief.Checked = objTransactionHead._Istaxrelief 'Sohail (19 Nov 2010)
            chkCumulative.Checked = objTransactionHead._Iscumulative 'Sohail (23 Apr 2011)
            chkNonCashBenefit.Checked = objTransactionHead._Isnoncashbenefit 'Sohail (18 May 2013)
            chkMonetary.Checked = objTransactionHead._Ismonetary 'Sohail (18 Jun 2013)
            cboComputeOn.SelectedValue = objTransactionHead._Computeon_Id
            txtFormula.Text = objTransactionHead._Formula
            txtFormula.Tag = objTransactionHead._Formulaid
            chkBasicsalaryasotherearning.Checked = objTransactionHead._Isbasicsalaryasotherearning 'Sohail (09 Sep 2017)
            'Sohail (28 Jan 2019) -- Start
            'NMB Enhancement - 76.1 - Option "Prorate Flat Rate head" on Transaction Head for Recurrent Head.
            chkProratedFlatRateHead.Checked = objTransactionHead._Isproratedhead
            'Sohail (28 Jan 2019) -- End
            'Sohail (03 Dec 2020) -- Start
            'Freight Forwarders Kenya Ltd Enhancement : # OLD-51 : Provide rounding options at transaction head level.
            cboRoundOffTypeId.SelectedValue = objTransactionHead._RoundOffTypeId
            'Sohail (03 Dec 2020) -- End
            'Sohail (11 Dec 2020) -- Start
            'Netis Togo Enhancement # OLD-218 : - Give option to Round Up/Round Down/Auto on transaction head next to Rounding OFF Type.
            cboRoundOffModeId.SelectedValue = objTransactionHead._RoundOffModeId
            'Sohail (11 Dec 2020) -- End
            nudPriority.Value = objTransactionHead._Compute_Priority 'Sohail (08 May 2021)
            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'objFormula._Formulatranheadunkid = objTransactionHead._Tranheadunkid
            objFormula._Formulatranheadunkid = objTransactionHead._Tranheadunkid(FinancialYear._Object._DatabaseName)
            'Sohail (21 Aug 2015) -- End
            objTransactionHead._FormulaDatasource = objFormula._Datasource
            'Sohail (03 Sep 2012) -- Start
            'TRA - ENHANCEMENT
            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'objFormulaCurrency._Formulatranheadunkid = objTransactionHead._Tranheadunkid
            objFormulaCurrency._Formulatranheadunkid = objTransactionHead._Tranheadunkid(FinancialYear._Object._DatabaseName)
            'Sohail (21 Aug 2015) -- End
            objTransactionHead._FormulaCurrencyDatasource = objFormulaCurrency._Datasource
            'Sohail (03 Sep 2012) -- End
            'Sohail (09 Oct 2012) -- Start
            'TRA - ENHANCEMENT
            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'objFormulaLeave._Formulatranheadunkid = objTransactionHead._Tranheadunkid
            objFormulaLeave._Formulatranheadunkid = objTransactionHead._Tranheadunkid(FinancialYear._Object._DatabaseName)
            'Sohail (21 Aug 2015) -- End
            objTransactionHead._FormulaLeaveDatasource = objFormulaLeave._Datasource
            'Sohail (09 Oct 2012) -- End
            'Sohail (21 Jun 2013) -- Start
            'TRA - ENHANCEMENT
            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'objFormulaMeasurementUnit._Formulatranheadunkid = objTransactionHead._Tranheadunkid
            objFormulaMeasurementUnit._Formulatranheadunkid = objTransactionHead._Tranheadunkid(FinancialYear._Object._DatabaseName)
            'Sohail (21 Aug 2015) -- End
            objTransactionHead._FormulaMeasurementUnitDatasource = objFormulaMeasurementUnit._Datasource
            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'objFormulaActivityUnit._Formulatranheadunkid = objTransactionHead._Tranheadunkid
            objFormulaActivityUnit._Formulatranheadunkid = objTransactionHead._Tranheadunkid(FinancialYear._Object._DatabaseName)
            'Sohail (21 Aug 2015) -- End
            objTransactionHead._FormulaActivityUnitDatasource = objFormulaActivityUnit._Datasource
            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'objFormulaMeasurementAmount._Formulatranheadunkid = objTransactionHead._Tranheadunkid
            objFormulaMeasurementAmount._Formulatranheadunkid = objTransactionHead._Tranheadunkid(FinancialYear._Object._DatabaseName)
            'Sohail (21 Aug 2015) -- End
            objTransactionHead._FormulaMeasurementAmountDatasource = objFormulaMeasurementAmount._Datasource
            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'objFormulaActivityAmount._Formulatranheadunkid = objTransactionHead._Tranheadunkid 
            objFormulaActivityAmount._Formulatranheadunkid = objTransactionHead._Tranheadunkid(FinancialYear._Object._DatabaseName)
            'Sohail (21 Aug 2015) -- End
            objTransactionHead._FormulaActivityAmountDatasource = objFormulaActivityAmount._Datasource
            'Sohail (21 Jun 2013) -- End
            'Sohail (29 Oct 2013) -- Start
            'TRA - ENHANCEMENT
            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'objFormulaShift._Formulatranheadunkid = objTransactionHead._Tranheadunkid
            objFormulaShift._Formulatranheadunkid = objTransactionHead._Tranheadunkid(FinancialYear._Object._DatabaseName)
            'Sohail (21 Aug 2015) -- End
            objTransactionHead._FormulaShiftDatasource = objFormulaShift._Datasource
            'Sohail (29 Oct 2013) -- End
            'Sohail (09 Nov 2013) -- Start
            'TRA - ENHANCEMENT
            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'objFormulaCumulative._Formulatranheadunkid = objTransactionHead._Tranheadunkid
            objFormulaCumulative._Formulatranheadunkid = objTransactionHead._Tranheadunkid(FinancialYear._Object._DatabaseName)
            'Sohail (21 Aug 2015) -- End
            objTransactionHead._FormulaCumulativeDatasource = objFormulaCumulative._Datasource
            'Sohail (09 Nov 2013) -- End
            'Sohail (02 Oct 2014) -- Start
            'Enhancement - No Of Active Inactive Dependants RELATION WISE.
            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'objFormulaCMaster._Formulatranheadunkid = objTransactionHead._Tranheadunkid
            objFormulaCMaster._Formulatranheadunkid = objTransactionHead._Tranheadunkid(FinancialYear._Object._DatabaseName)
            'Sohail (21 Aug 2015) -- End
            objTransactionHead._FormulaCMasterDatasource = objFormulaCMaster._Datasource
            'Sohail (02 Oct 2014) -- End

            'Sohail (12 Nov 2014) -- Start
            'Voltamp Enhancement - Quentity Total and Amount Total for Leave Claim Request in transaction head formula.
            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'objFormulaCRExpense._Formulatranheadunkid = objTransactionHead._Tranheadunkid
            objFormulaCRExpense._Formulatranheadunkid = objTransactionHead._Tranheadunkid(FinancialYear._Object._DatabaseName)
            'Sohail (21 Aug 2015) -- End
            objTransactionHead._FormulaCRExpenseDatasource = objFormulaCRExpense._Datasource
            'Sohail (12 Nov 2014) -- End

            'Sohail (24 Oct 2017) -- Start
            'CCBRT Enhancement - 70.1 - (Ref. Id - 73) - Add a loan function (like the leave function) in payroll where you can select loan scheme, interest or EMI, total loan amount, etc.
            objFormulaLoanScheme._Formulatranheadunkid = objTransactionHead._Tranheadunkid(FinancialYear._Object._DatabaseName)
            objTransactionHead._FormulaloanschemeDatasource = objFormulaLoanScheme._Datasource
            'Sohail (24 Oct 2017) -- End

            'Sohail (12 Apr 2013) -- Start
            'TRA - ENHANCEMENT
            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'objFormulaSlabTran._Tranheadunkid = objTransactionHead._Tranheadunkid
            objFormulaSlabTran._Tranheadunkid = objTransactionHead._Tranheadunkid(FinancialYear._Object._DatabaseName)
            'Sohail (21 Aug 2015) -- End
            mdtFormulaSlab = objFormulaSlabTran._Datasource
            mdtFormulaSlab = New DataView(mdtFormulaSlab, "", "end_date DESC", DataViewRowState.CurrentRows).ToTable
            objTransactionHead._FormulaPeriodWiseSlabDatasource = objFormulaSlabTran._Datasource
            'Sohail (12 Apr 2013) -- End

            'S.SANDEEP [12-JUL-2018] -- START
            'ISSUE/ENHANCEMENT : {Ref#223|ARUTI-239}
            chkIsPerformance.Checked = objTransactionHead._IsPerformanceAppraisalHead
            'S.SANDEEP [12-JUL-2018] -- END

            'Hemant (06 Jul 2020) -- Start
            'ENHANCEMENT (NMB): Posting a transaction head to one cost centre for all employees without mapping on employee cost centre screen (cost center dropdown list on head master and claim expense master and pick cost center from head master instead of employee master on process payroll if not mapped on employee cost center screen)
            cboDefaultCostCenter.SelectedValue = objTransactionHead._CostCenterUnkId
            'Hemant (06 Jul 2020) -- End

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetValue", mstrModuleName)
        End Try
    End Sub

    Private Sub SetValue()
        Try
            objTransactionHead._Trnheadcode = txtHeadCode.Text.Trim
            objTransactionHead._Trnheadname = txtHeadName.Text.Trim
            objTransactionHead._Trnheadtype_Id = CInt(cboTrnHeadType.SelectedValue)
            objTransactionHead._Typeof_id = CInt(cboTypeOf.SelectedValue)
            objTransactionHead._Calctype_Id = CInt(cboCalcType.SelectedValue)
            'Sohail (20 Apr 2011) -- Start
            'objTransactionHead._Isrecurrent = chkRecurrent.Checked
            'Hemant (22 Aug 2020) -- Start
            'NMB Enhancement : Allow to edit payable heads on head master list to map default cost center
            If CInt(cboCalcType.SelectedValue) = enCalcType.EMPLOYER_CONTRIBUTION_PAYABLE Then
                objTransactionHead._Isrecurrent = False
                objTransactionHead._Isproratedhead = False
                'Hemant (22 Aug 2020) -- End
            ElseIf CInt(cboCalcType.SelectedValue) <> enCalcType.FlatRate_Others Then
                objTransactionHead._Isrecurrent = True
                'Sohail (28 Jan 2019) -- Start
                'NMB Enhancement - 76.1 - Option "Prorate Flat Rate head" on Transaction Head for Recurrent Head.
                objTransactionHead._Isproratedhead = False
                'Sohail (28 Jan 2019) -- End
            Else
                objTransactionHead._Isrecurrent = chkRecurrent.Checked
                'Sohail (28 Jan 2019) -- Start
                'NMB Enhancement - 76.1 - Option "Prorate Flat Rate head" on Transaction Head for Recurrent Head.
                If chkRecurrent.Checked = False Then
                    objTransactionHead._Isproratedhead = False
                Else
                    objTransactionHead._Isproratedhead = chkProratedFlatRateHead.Checked
                End If
                'Sohail (28 Jan 2019) -- End
            End If
            'Sohail (20 Apr 2011) -- End
            objTransactionHead._Isappearonpayslip = chkAppearOnPayslip.Checked
            objTransactionHead._Computeon_Id = CInt(cboComputeOn.SelectedValue)
            objTransactionHead._Formula = txtFormula.Text
            objTransactionHead._Formulaid = CStr(txtFormula.Tag)
            objTransactionHead._Userunkid = User._Object._Userunkid 'Sohail (14 Oct 2010)
            objTransactionHead._Istaxable = chkTaxable.Checked 'Sohail (18 Nov 2010) 
            objTransactionHead._Istaxrelief = chkTaxRelief.Checked 'Sohail (19 Nov 2010)
            objTransactionHead._Iscumulative = chkCumulative.Checked 'Sohail (23 Apr 2011)
            objTransactionHead._Isnoncashbenefit = chkNonCashBenefit.Checked 'Sohail (18 May 2013)
            objTransactionHead._Ismonetary = chkMonetary.Checked 'Sohail (18 Jun 2013)
            objTransactionHead._Isactive = True  'Sohail (09 Nov 2013)
            objTransactionHead._Isbasicsalaryasotherearning = chkBasicsalaryasotherearning.Checked 'Sohail (09 Sep 2017)
            'S.SANDEEP [12-JUL-2018] -- START
            'ISSUE/ENHANCEMENT : {Ref#223|ARUTI-239}
            objTransactionHead._IsPerformanceAppraisalHead = chkIsPerformance.Checked
            'S.SANDEEP [12-JUL-2018] -- END
            'Hemant (06 Jul 2020) -- Start
            'ENHANCEMENT (NMB): Posting a transaction head to one cost centre for all employees without mapping on employee cost centre screen (cost center dropdown list on head master and claim expense master and pick cost center from head master instead of employee master on process payroll if not mapped on employee cost center screen)
            objTransactionHead._CostCenterUnkId = CInt(cboDefaultCostCenter.SelectedValue)
            'Hemant (06 Jul 2020) -- End
            'Sohail (03 Dec 2020) -- Start
            'Freight Forwarders Kenya Ltd Enhancement : # OLD-51 : Provide rounding options at transaction head level.
            objTransactionHead._RoundOffTypeId = CDbl(cboRoundOffTypeId.SelectedValue)
            'Sohail (03 Dec 2020) -- End
            'Sohail (11 Dec 2020) -- Start
            'Netis Togo Enhancement # OLD-218 : - Give option to Round Up/Round Down/Auto on transaction head next to Rounding OFF Type.
            objTransactionHead._RoundOffModeId = CInt(cboRoundOffModeId.SelectedValue)
            'Sohail (11 Dec 2020) -- End
            objTransactionHead._Compute_Priority = CInt(nudPriority.Value)   'Sohail (08 May 2021)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetValue", mstrModuleName)
        End Try
    End Sub

    Private Sub FillCombo()
        Dim objMaster As New clsMasterData
        Dim dsCombo As DataSet
        'Sohail (21 Nov 2011) -- Start
        Dim objPeriod As New clscommom_period_Tran
        Dim intCurrPeriodID As Integer
        'Sohail (21 Nov 2011) -- End

        Try
            dsCombo = objMaster.getComboListForHeadType("HeadType")
            With cboTrnHeadType
                .ValueMember = "Id"
                .DisplayMember = "Name"
                .DataSource = dsCombo.Tables("HeadType")
                If .Items.Count > 0 Then .SelectedValue = 0
            End With

            'Sohail (09 Aug 2010) -- Start
            'dsCombo = objMaster.getComboListComputedOn("ComputeOn")
            'With cboComputeOn
            '    .ValueMember = "Id"
            '    .DisplayMember = "Name"
            '    .DataSource = dsCombo.Tables("ComputeOn")
            '    If .Items.Count > 0 Then .SelectedValue = 0
            'End With
            'Sohail (09 Aug 2010) -- End

            'Sohail (24 Sep 2013) -- Start
            'TRA - ENHANCEMENT
            'dsCombo = objMaster.GetPaymentBy("AmountType")
            dsCombo = objMaster.GetPaymentBy("AmountType", True)
            'Sohail (24 Sep 2013) -- End
            With cboSimpleSlabAmountType
                .ValueMember = "Id"
                .DisplayMember = "Name"
                .DataSource = dsCombo.Tables("AmountType")
                If .Items.Count > 0 Then .SelectedValue = 0
            End With

            With objSlabType
                .ValueMember = "Id"
                .DisplayMember = "Name"
                .DataSource = cboSimpleSlabAmountType.DataSource
                If .Items.Count > 0 Then .SelectedValue = 0
            End With

            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'intCurrPeriodID = objMaster.getFirstPeriodID(enModuleReference.Payroll, enStatusType.Open) 'Sohail (12 Apr 2013)
            intCurrPeriodID = objMaster.getFirstPeriodID(enModuleReference.Payroll, FinancialYear._Object._YearUnkid, enStatusType.Open) 'Sohail (12 Apr 2013)
            'S.SANDEEP [04 JUN 2015] -- END

            'Sohail (21 Nov 2011) -- Start
            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'dsCombo = objPeriod.getListForCombo(enModuleReference.Payroll, FinancialYear._Object._YearUnkid, "Period", True, enStatusType.Open)
            dsCombo = objPeriod.getListForCombo(enModuleReference.Payroll, FinancialYear._Object._YearUnkid, FinancialYear._Object._DatabaseName, FinancialYear._Object._Database_Start_Date, "Period", True, enStatusType.Open)
            'Sohail (21 Aug 2015) -- End
            With cboSimpleSlabPeriod
                .ValueMember = "periodunkid"
                .DisplayMember = "Name"
                .DataSource = dsCombo.Tables("Period")
                .SelectedValue = intCurrPeriodID 'Sohail (12 Apr 2013)
            End With
            'Sohail (12 Apr 2013) -- Start
            'TRA - ENHANCEMENT
            'intCurrPeriodID = objMaster.getCurrentPeriodID(enModuleReference.Payroll, DateTime.Today.Date)
            'cboSimpleSlabPeriod.SelectedValue = objMaster.getCurrentPeriodID(enModuleReference.Payroll, DateTime.Today.Date)
            'Sohail (12 Apr 2013) -- End

            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'dsCombo = objPeriod.getListForCombo(enModuleReference.Payroll, FinancialYear._Object._YearUnkid, "TaxPeriod", True, enStatusType.Open)
            dsCombo = objPeriod.getListForCombo(enModuleReference.Payroll, FinancialYear._Object._YearUnkid, FinancialYear._Object._DatabaseName, FinancialYear._Object._Database_Start_Date, "TaxPeriod", True, enStatusType.Open)
            'Sohail (21 Aug 2015) -- End
            With cboTaxSlabPeriod
                .ValueMember = "periodunkid"
                .DisplayMember = "Name"
                .DataSource = dsCombo.Tables("TaxPeriod")
                .SelectedValue = intCurrPeriodID 'Sohail (12 Apr 2013)
            End With
            'cboTaxSlabPeriod.SelectedValue = objMaster.getCurrentPeriodID(enModuleReference.Payroll, DateTime.Today.Date) 'Sohail (12 Apr 2013)
            'Sohail (21 Nov 2011) -- End

            'Hemant (06 Jul 2020) -- Start
            'ENHANCEMENT (NMB): Posting a transaction head to one cost centre for all employees without mapping on employee cost centre screen (cost center dropdown list on head master and claim expense master and pick cost center from head master instead of employee master on process payroll if not mapped on employee cost center screen)
            dsCombo = Nothing
            Dim objCostcenter As New clscostcenter_master
            dsCombo = objCostcenter.getComboList("CostCenter", True)
            cboDefaultCostCenter.ValueMember = "costcenterunkid"
            cboDefaultCostCenter.DisplayMember = "costcentername"
            cboDefaultCostCenter.DataSource = dsCombo.Tables("CostCenter")
            'Hemant (06 Jul 2020) -- End

            'Sohail (03 Dec 2020) -- Start
            'Freight Forwarders Kenya Ltd Enhancement : # OLD-51 : Provide rounding options at transaction head level.
            dsCombo = objMaster.getListRoundOffType("List")
            With cboRoundOffTypeId
                .ValueMember = "id"
                .DisplayMember = "name"
                .DataSource = dsCombo.Tables("List")
                'Sohail (28 Jun 2021) -- Start
                'Enhancement : : New rounding option 0.01 for net pay and transaction head.
                '.SelectedValue = 200
                .SelectedValue = 100
                'Sohail (28 Jun 2021) -- End
            End With
            'Sohail (03 Dec 2020) -- End

            'Sohail (11 Dec 2020) -- Start
            'Netis Togo Enhancement # OLD-218 : - Give option to Round Up/Round Down/Auto on transaction head next to Rounding OFF Type.
            dsCombo = objMaster.getComboListPaymentRoundingType("List")
            With cboRoundOffModeId
                .ValueMember = "id"
                .DisplayMember = "name"
                .DataSource = dsCombo.Tables("List")
                .SelectedValue = CInt(enPaymentRoundingType.AUTOMATIC)
            End With
            'Sohail (11 Dec 2020) -- End

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        Finally
            objMaster = Nothing
            dsCombo = Nothing
        End Try
    End Sub

    Private Sub FillSimpleSlabList()
        Try
            lvTrnHeadSlabSimple.Items.Clear()
            Dim lvItems As ListViewItem
            Dim drItem As DataRow

            'Sohail (21 Nov 2011) -- Start
            mdtSimpleSlab = New DataView(mdtSimpleSlab, "", "end_date DESC, amountupto ASC", DataViewRowState.CurrentRows).ToTable
            'Sohail (21 Nov 2011) -- End

            For Each drItem In mdtSimpleSlab.Rows
                If CStr(IIf(IsDBNull(drItem.Item("AUD")), "A", drItem.Item("AUD").ToString)) <> "D" Then
                    lvItems = New ListViewItem
                    lvItems.Text = drItem.Item("tranheadunkid").ToString
                    lvItems.Tag = drItem.Item("tranheadslabtranunkid").ToString

                    lvItems.SubItems.Add(Format(drItem.Item("amountupto"), GUI.fmtCurrency)) 'Sohail (11 May 2011)

                    objSlabType.SelectedValue = CInt(drItem.Item("slabtype").ToString)
                    lvItems.SubItems.Add(objSlabType.Text)
                    objSlabType.SelectedValue = 0
                    lvItems.SubItems(2).Tag = CInt(drItem.Item("slabtype").ToString)

                    'Sohail (24 Sep 2013) -- Start
                    'TRA - ENHANCEMENT
                    'lvItems.SubItems.Add(Format(drItem.Item("valuebasis"), GUI.fmtCurrency)) 'Sohail (11 May 2011)
                    If CInt(drItem.Item("slabtype").ToString) = enPaymentBy.Fromula Then
                        lvItems.SubItems.Add(drItem.Item("formula").ToString)
                        lvItems.SubItems(colhSimpleValueBasis.Index).Tag = drItem.Item("formulaid").ToString
                    Else
                        lvItems.SubItems.Add(Format(drItem.Item("valuebasis"), GUI.fmtCurrency))
                        lvItems.SubItems(colhSimpleValueBasis.Index).Tag = CDbl(drItem.Item("valuebasis"))
                    End If
                    'Sohail (24 Sep 2013) -- End

                    lvItems.SubItems.Add(drItem.Item("GUID").ToString)
                    'Sohail (21 Nov 2011) -- Start
                    lvItems.SubItems.Add(drItem.Item("period_name").ToString)
                    lvItems.SubItems(colhSimplePeriod.Index).Tag = CInt(drItem.Item("periodunkid"))
                    lvItems.SubItems.Add(drItem.Item("end_date").ToString)
                    'Sohail (21 Nov 2011) -- End

                    lvTrnHeadSlabSimple.Items.Add(lvItems)
                End If
            Next
            'Sohail (21 Nov 2011) -- Start
            lvTrnHeadSlabSimple.GroupingColumn = colhSimplePeriod
            lvTrnHeadSlabSimple.DisplayGroups(True)
            lvTrnHeadSlabSimple.GridLines = False

            If lvTrnHeadSlabSimple.Items.Count > 3 Then
                colhSimpleValueBasis.Width = 160 - 18
            Else
                colhSimpleValueBasis.Width = 160
            End If
            'Sohail (21 Nov 2011) -- End

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillSimpleList", mstrModuleName)
        End Try
    End Sub

    Private Sub FillTaxSlabList()
        Try
            lvTrnHeadSlabInExcessof.Items.Clear()
            Dim lvItems As ListViewItem
            Dim drItem As DataRow

            'Sohail (21 Nov 2011) -- Start
            mdtTaxSlab = New DataView(mdtTaxSlab, "", "end_date DESC, amountupto ASC", DataViewRowState.CurrentRows).ToTable
            'Sohail (21 Nov 2011) -- End

            For Each drItem In mdtTaxSlab.Rows
                If CStr(IIf(IsDBNull(drItem.Item("AUD")), "A", drItem.Item("AUD").ToString)) <> "D" Then
                    lvItems = New ListViewItem
                    lvItems.Text = drItem.Item("tranheadunkid").ToString
                    lvItems.Tag = drItem.Item("tranheadtaxslabtranunkid").ToString

                    lvItems.SubItems.Add(Format(drItem.Item("amountupto"), GUI.fmtCurrency)) 'Sohail (11 May 2011)
                    lvItems.SubItems.Add(Format(drItem.Item("fixedamount"), GUI.fmtCurrency)) 'Sohail (11 May 2011)
                    lvItems.SubItems.Add(Format(drItem.Item("ratepayable"), GUI.fmtCurrency)) 'Sohail (11 May 2011)
                    lvItems.SubItems.Add(Format(drItem.Item("inexcessof"), GUI.fmtCurrency)) 'Sohail (11 May 2011)
                    lvItems.SubItems.Add(drItem.Item("GUID").ToString)
                    'Sohail (21 Nov 2011) -- Start
                    lvItems.SubItems.Add(drItem.Item("period_name").ToString)
                    lvItems.SubItems(colhTaxPeriod.Index).Tag = CInt(drItem.Item("periodunkid"))
                    lvItems.SubItems.Add(drItem.Item("end_date").ToString)
                    'Sohail (21 Nov 2011) -- End
                    lvTrnHeadSlabInExcessof.Items.Add(lvItems)
                End If
            Next
            'Sohail (21 Nov 2011) -- Start
            lvTrnHeadSlabInExcessof.GroupingColumn = colhTaxPeriod
            lvTrnHeadSlabInExcessof.DisplayGroups(True)
            lvTrnHeadSlabInExcessof.GridLines = False

            If lvTrnHeadSlabInExcessof.Items.Count > 3 Then
                colhTaxRatePayable.Width = 105 - 18
            Else
                colhTaxRatePayable.Width = 105
            End If
            'Sohail (21 Nov 2011) -- End
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillAccessOfList", mstrModuleName)
        End Try
    End Sub

    Private Sub ResetSimpleSlab()
        Try
            txtSimpleSlabAmtUpto.Text = ""
            cboSimpleSlabAmountType.SelectedValue = 0
            txtSimpleSlabValueBasis.Text = ""
            btnAdd.Enabled = True
            cboSimpleSlabPeriod.Enabled = True 'Sohail (21 Nov 2011)
            'Sohail (24 Sep 2013) -- Start
            'TRA - ENHANCEMENT
            txtSlabFormula.Text = ""
            txtSlabFormula.Tag = ""
            cboSimpleSlabAmountType.Enabled = True
            'Sohail (24 Sep 2013) -- End
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "ResetSimpleSlab", mstrModuleName)
        End Try
    End Sub

    Private Sub ResetTaxSlab()
        Try
            txtTaxSlabAmtUpto.Text = ""
            txtTaxSlabFixedAmount.Text = ""
            txtTaxSlabRatePayable.Text = ""
            btnAdd.Enabled = True
            cboTaxSlabPeriod.Enabled = True 'Sohail (21 Nov 2011)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "ResetTaxSlab", mstrModuleName)
        End Try
    End Sub
#End Region

#Region " Private Functions "
    Private Function IsValidInput() As Boolean
        Dim strMsg As String = ""
        Try
            If txtHeadCode.Text.Trim = "" Then
                strMsg = Language.getMessage(mstrModuleName, 1, "Sorry, Transaction Head Code can not be blank. Transaction Head is a compulsory information.")
                eZeeMsgBox.Show(strMsg, enMsgBoxStyle.Information)
                txtHeadCode.Focus()
                Exit Function
            ElseIf txtHeadName.Text.Trim = "" Then
                strMsg = Language.getMessage(mstrModuleName, 2, "Sorry, Transaction Head Name can not be blank. Transaction Head is a compulsory information.")
                eZeeMsgBox.Show(strMsg, enMsgBoxStyle.Information)
                txtHeadName.Focus()
                Exit Function
            ElseIf CInt(cboTrnHeadType.SelectedValue) <= 0 Then
                strMsg = Language.getMessage(mstrModuleName, 3, "Please select Transaction Head Type.")
                eZeeMsgBox.Show(strMsg, enMsgBoxStyle.Information)
                cboTrnHeadType.Focus()
                Exit Function
            ElseIf CInt(cboTypeOf.SelectedValue) <= 0 Then
                strMsg = Language.getMessage(mstrModuleName, 4, "Please select Type of Transaction.")
                eZeeMsgBox.Show(strMsg, enMsgBoxStyle.Information)
                cboTypeOf.Focus()
                Exit Function
            ElseIf CInt(cboCalcType.SelectedValue) <= 0 Then
                strMsg = Language.getMessage(mstrModuleName, 5, "Please select Calculation Type.")
                eZeeMsgBox.Show(strMsg, enMsgBoxStyle.Information)
                cboCalcType.Focus()
                Exit Function
            ElseIf cboComputeOn.Enabled = True And CInt(cboComputeOn.SelectedValue) <= 0 Then
                strMsg = Language.getMessage(mstrModuleName, 7, "Please select Compute On.")
                eZeeMsgBox.Show(strMsg, enMsgBoxStyle.Information)
                cboComputeOn.Focus()
                Exit Function
            ElseIf CInt(cboComputeOn.SelectedValue) = enComputeOn.ComputeOnSpecifiedFormula And txtFormula.Text = "" Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 8, "Please enter an appropriate formula."), enMsgBoxStyle.Information)
                Exit Function
            ElseIf CInt(cboCalcType.SelectedValue) = enCalcType.AsComputedValue And lvTrnHeadSlabSimple.Items.Count = 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 9, "Please Add atleast one computation information."), enMsgBoxStyle.Information)
                txtSimpleSlabAmtUpto.Focus()
                Exit Function
            ElseIf CInt(cboCalcType.SelectedValue) = enCalcType.AsComputedOnWithINEXCESSOFTaxSlab And lvTrnHeadSlabInExcessof.Items.Count = 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 9, "Please Add atleast one computation information."), enMsgBoxStyle.Information)
                txtTaxSlabAmtUpto.Focus()
                Exit Function
            End If

            Return True
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "IsValidInput", mstrModuleName)
        End Try
    End Function

    Private Function IsValidSimpleSlab(ByVal ctrlName As String) As Boolean
        Try
            If CInt(cboComputeOn.SelectedValue) = enComputeOn.ComputeOnSpecifiedFormula And txtFormula.Text = "" Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 8, "Please enter an appropriate formula."), enMsgBoxStyle.Information)
                txtFormula.Focus() 'Sohail (24 Sep 2013)
                Exit Function
                'Sohail (03 Sep 2010) -- Start
                'ElseIf txtSimpleSlabAmtUpto.Text = "" Then
                '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 10, "Sorry, Amount upto can not be Zero."), enMsgBoxStyle.Information)
                '    txtSimpleSlabAmtUpto.Focus()
                '    Exit Function
                'Sohail (03 Sep 2010) -- End
                'Sohail (16 Oct 2010) -- Start
                'ElseIf txtSimpleSlabAmtUpto.Decimal = 0 Then 'Sohail (03 Sep 2010)
            ElseIf txtSimpleSlabAmtUpto.Decimal < 0 Then 'Sohail (11 May 2011)
                'eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 10, "Sorry, Amount upto can not be Zero."), enMsgBoxStyle.Information)
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 10, "Sorry, Amount upto cannot be less than Zero."), enMsgBoxStyle.Information)
                'Sohail (16 Oct 2010) -- End
                txtSimpleSlabAmtUpto.Focus()
                Exit Function
            ElseIf CInt(cboSimpleSlabAmountType.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 11, "Please select Slab Type."), enMsgBoxStyle.Information)
                cboSimpleSlabAmountType.Focus()
                Exit Function
                'Sohail (16 Oct 2010) -- Start
                'ElseIf txtSimpleSlabValueBasis.Decimal = 0 Then 'Sohail (03 Sep 2010)
            ElseIf CInt(cboSimpleSlabAmountType.SelectedValue) = enPaymentBy.Value AndAlso txtSimpleSlabValueBasis.Decimal < 0 Then 'Sohail (11 May 2011)
                'eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 12, "Sorry, Value Basis can not be Zero."), enMsgBoxStyle.Information)
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 12, "Sorry, Value Basis cannot be less then Zero."), enMsgBoxStyle.Information)
                'Sohail (16 Oct 2010) -- End
                txtSimpleSlabValueBasis.Focus()
                Exit Function
            ElseIf CInt(cboSimpleSlabAmountType.SelectedValue) = enPaymentBy.Percentage Then
                If txtSimpleSlabValueBasis.Decimal < 0 Or txtSimpleSlabValueBasis.Decimal > 100 Then 'Sohail (03 Sep 2010)
                    'Allow percentage above 100 for over time calculation (200% of hourly salary)
                    If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 13, "Sorry, Percentage should be between 1 and 100.") & vbCr & vbCr & Language.getMessage(mstrModuleName, 20, "Do you want to proceed?"), CType(enMsgBoxStyle.Question + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.None Then
                        txtSimpleSlabValueBasis.Focus()
                        Exit Function
                    End If
                End If
                'Sohail (24 Sep 2013) -- Start
                'TRA - ENHANCEMENT
            ElseIf CInt(cboSimpleSlabAmountType.SelectedValue) = enPaymentBy.Fromula AndAlso CInt(cboComputeOn.SelectedValue) <> enComputeOn.ComputeOnSpecifiedFormula Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 7, "Please select Compute On."), enMsgBoxStyle.Information)
                cboComputeOn.Focus()
                Exit Function
            ElseIf ctrlName <> objbtnAddSlabFormula.Name AndAlso CInt(cboSimpleSlabAmountType.SelectedValue) = enPaymentBy.Fromula AndAlso txtSlabFormula.Text.Trim = "" Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 8, "Please enter an appropriate formula."), enMsgBoxStyle.Information)
                txtSlabFormula.Focus()
                Exit Function
                'Sohail (24 Sep 2013) -- End
            End If
            'Sohail (21 Nov 2011) -- Start
            If cboSimpleSlabPeriod.SelectedValue Is Nothing Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 15, "Sorry! This Period is closed."), enMsgBoxStyle.Information)
                Exit Function
            ElseIf CInt(cboSimpleSlabPeriod.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 6, "Please select Period."), enMsgBoxStyle.Information)
                cboSimpleSlabPeriod.Focus()
                Exit Function
            ElseIf mintPeriod_Status = enStatusType.Close Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 15, "Sorry! This Period is closed."), enMsgBoxStyle.Information)
                cboSimpleSlabPeriod.Focus()
                Exit Function
            End If
            'Sohail (21 Nov 2011) -- End

            Return True
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "IsValidSimpleSlab", mstrModuleName)
        End Try
    End Function

    Private Function IsValidTaxSlab() As Boolean
        Try
            If CInt(cboComputeOn.SelectedValue) = enComputeOn.ComputeOnSpecifiedFormula And txtFormula.Text = "" Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 8, "Please enter an appropriate formula."), enMsgBoxStyle.Information)
                Exit Function
                'Sohail (03 Sep 2010) -- Start
                'ElseIf txtTaxSlabAmtUpto.Text = "" Then
                '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 10, "Sorry, Amount upto can not be Zero."), enMsgBoxStyle.Information)
                '    txtTaxSlabAmtUpto.Focus()
                '    Exit Function
                'Sohail (03 Sep 2010) -- End
                'Sohail (16 Oct 2010) -- Start
                'ElseIf txtSimpleSlabAmtUpto.Decimal = 0 Then 'Sohail (03 Sep 2010)
            ElseIf txtSimpleSlabAmtUpto.Decimal < 0 Then 'Sohail (11 May 2011)
                'eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 10, "Sorry, Amount upto can not be Zero."), enMsgBoxStyle.Information)
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 10, "Sorry, Amount upto cannot be less than Zero."), enMsgBoxStyle.Information)
                txtTaxSlabAmtUpto.Focus()
                Exit Function
                'Sohail (03 Sep 2010) -- Start
                'ElseIf txtTaxSlabFixedAmount.Text = "" Then
                '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 15, "Sorry, Fixed Amount can not be Zero."), enMsgBoxStyle.Information)
                '    txtTaxSlabFixedAmount.Focus()
                '    Exit Function
                'Sohail (03 Sep 2010) -- End
                'ElseIf cdec(txtTaxSlabFixedAmount.Text) = 0 Then
                '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 15, "Sorry, Fixed Amount can not be Zero."), enMsgBoxStyle.Information)
                '    txtTaxSlabFixedAmount.Focus()
                '    Exit Function
                'Sohail (03 Sep 2010) -- Start
                'ElseIf txtTaxSlabRatePayable.Text = "" Then
                '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 16, "Sorry, Rate Payable can not be Zero."), enMsgBoxStyle.Information)
                '    txtTaxSlabRatePayable.Focus()
                '    Exit Function
                'Sohail (03 Sep 2010) -- End
            ElseIf txtTaxSlabRatePayable.Decimal < 0 Or txtTaxSlabRatePayable.Decimal > 100 Then 'Sohail (03 Sep 2010)
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 13, "Sorry, Percentage should be between 1 and 100."), enMsgBoxStyle.Information)
                txtTaxSlabRatePayable.Focus()
                Exit Function
            End If
            'Sohail (21 Nov 2011) -- Start
            If cboTaxSlabPeriod.SelectedValue Is Nothing Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 15, "Sorry! This Period is closed."), enMsgBoxStyle.Information)
                Exit Function
            ElseIf CInt(cboTaxSlabPeriod.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 6, "Please select Period."), enMsgBoxStyle.Information)
                cboTaxSlabPeriod.Focus()
                Exit Function
            ElseIf mintPeriod_Status = enStatusType.Close Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 15, "Sorry! This Period is closed."), enMsgBoxStyle.Information)
                cboTaxSlabPeriod.Focus()
                Exit Function
            End If
            'Sohail (21 Nov 2011) -- End

            Return True
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "IsValidTaxSlab", mstrModuleName)
        End Try
    End Function
#End Region

#Region " Form's Events "

    Private Sub frmTransactionHead_AddEdit_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        Try
            objTransactionHead = Nothing
            objTranSimpleSlab = Nothing
            objTranTaxSlab = Nothing
            objFormula = Nothing
            objFormulaCurrency = Nothing 'Sohail (03 Sep 2012)
            objFormulaLeave = Nothing 'Sohail (09 Oct 2012)
            'Sohail (21 Jun 2013) -- Start
            'TRA - ENHANCEMENT
            objFormulaMeasurementUnit = Nothing
            objFormulaActivityUnit = Nothing
            objFormulaMeasurementAmount = Nothing
            objFormulaActivityAmount = Nothing
            'Sohail (21 Jun 2013) -- End
            objFormulaShift = Nothing 'Sohail (29 Oct 2013)
            objFormulaCumulative = Nothing 'Sohail (09 Nov 2013)
            objFormulaCMaster = Nothing 'Sohail (02 Oct 2014)
            objFormulaCRExpense = Nothing 'Sohail (12 Nov 2014)
            objFormulaLoanScheme = Nothing 'Sohail (24 Oct 2017)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmTransactionHead_AddEdit_FormClosed", mstrModuleName)
        End Try
    End Sub

    Private Sub frmTransactionHead_AddEdit_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown
        Try
            Select Case e.KeyCode
                Case Keys.Return
                    SendKeys.Send("{TAB}")
                Case Keys.S
                    If e.Control = True Then
                        Call btnSave.PerformClick()
                    End If
            End Select
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmTransactionHead_AddEdit_KeyDown", mstrModuleName)
        End Try
    End Sub

    Private Sub frmTransactionHead_AddEdit_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        objTransactionHead = New clsTransactionHead
        objTranSimpleSlab = New clsTranheadSlabTran
        objTranTaxSlab = New clsTranheadInexcessslabTran
        objFormula = New clsTranheadFormulaTran
        objFormulaCurrency = New clsTranheadFormulaCurrencyTran 'Sohail (03 Sep 2012)
        objFormulaLeave = New clsTranheadFormulaLeaveTran 'Sohail (09 Oct 2012)
        objFormulaSlabTran = New clsTranhead_formula_slab_Tran 'Sohail (12 Apr 2012)
        'Sohail (21 Jun 2013) -- Start
        'TRA - ENHANCEMENT
        objFormulaMeasurementUnit = New clsTranhead_formula_measurement_unit_Tran
        objFormulaActivityUnit = New clsTranhead_formula_activity_unit_Tran
        objFormulaMeasurementAmount = New clsTranhead_formula_measurement_amount_Tran
        objFormulaActivityAmount = New clsTranhead_formula_activity_amount_Tran
        'Sohail (21 Jun 2013) -- End
        objFormulaShift = New clsTranheadFormulaShiftTran  'Sohail (29 Oct 2013)
        objFormulaCumulative = New clsTranheadFormulaCumulativeTran 'Sohail (09 Nov 2013)
        objFormulaCMaster = New clsTranheadFormulaCMasterTran 'Sohail (02 Oct 2014)
        objFormulaCRExpense = New clsTranheadFormulaCRExpenseTran 'Sohail (12 Nov 2014)
        objFormulaLoanScheme = New clsTranheadFormulaLoanSchemeTran 'Sohail (24 Oct 2017)

        Try
            Call Set_Logo(Me, gApplicationType)
            Call Language.setLanguage(Me.Name)
            'Anjan (02 Sep 2011)-Start
            'Issue : Including Language Settings.
            Language.setLanguage(Me.Name)
            Call OtherSettings()
            'Anjan (02 Sep 2011)-End 
            Call setColor()
            Call FillCombo()

            chkRecurrent.Checked = True 'Sohail (20 Apr 2011)

            If menAction = enAction.EDIT_ONE Then
                'Sohail (21 Aug 2015) -- Start
                'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                'objTransactionHead._Tranheadunkid = mintTranHeadUnkid
                objTransactionHead._Tranheadunkid(FinancialYear._Object._DatabaseName) = mintTranHeadUnkid
                'Sohail (21 Aug 2015) -- End
                'Sohail (23 Dec 2010) -- Start
                'cboTrnHeadType.Enabled = False
                'cboTypeOf.Enabled = False
                'cboCalcType.Enabled = False
                'cboComputeOn.Enabled = False
                If objTransactionHead._Calctype_Id = enCalcType.OnAttendance OrElse objTransactionHead._Calctype_Id = enCalcType.DEFINED_SALARY _
                   OrElse objTransactionHead._Calctype_Id = enCalcType.NET_PAY OrElse objTransactionHead._Calctype_Id = enCalcType.TOTAL_EARNING OrElse objTransactionHead._Calctype_Id = enCalcType.TOTAL_DEDUCTION _
                   OrElse objTransactionHead._Calctype_Id = enCalcType.TAXABLE_EARNING_TOTAL OrElse objTransactionHead._Calctype_Id = enCalcType.NON_TAXABLE_EARNING_TOTAL _
                   OrElse objTransactionHead._Calctype_Id = enCalcType.NON_CASH_BENEFIT_TOTAL _
                   OrElse objTransactionHead._Typeof_id = enTypeOf.PAY_PER_ACTIVITY _
                   OrElse objTransactionHead._Typeof_id = enTypeOf.NET_PAY_ROUNDING_ADJUSTMENT _
                   OrElse objTransactionHead.isUsed(mintTranHeadUnkid) = True _
                   OrElse objTransactionHead._Calctype_Id = enCalcType.EMPLOYER_CONTRIBUTION_PAYABLE _
                   Then 'Sohail (18 Jan 2012)
                    'Hemant (22 Aug 2020) -- [EMPLOYER_CONTRIBUTION_PAYABLE]
                    'Sohail (18 Apr 2016) - [NET_PAY_ROUNDING_ADJUSTMENT]
                    'Sohail (18 May 2013) - [NON_CASH_BENEFIT_TOTAL]

                    cboTrnHeadType.Enabled = False
                    cboTypeOf.Enabled = False
                    cboCalcType.Enabled = False
                    cboComputeOn.Enabled = False
                Else
                    cboTrnHeadType.Enabled = True
                    cboTypeOf.Enabled = True
                    cboCalcType.Enabled = True
                    cboComputeOn.Enabled = True
                End If
                'Sohail (23 Dec 2010) -- End

                'Sohail (03 Dec 2020) -- Start
                'Freight Forwarders Kenya Ltd Enhancement : # OLD-51 : Provide rounding options at transaction head level.
                If objTransactionHead._Calctype_Id = enCalcType.NET_PAY OrElse objTransactionHead._Calctype_Id = enCalcType.TOTAL_EARNING OrElse objTransactionHead._Calctype_Id = enCalcType.TOTAL_DEDUCTION OrElse objTransactionHead._Calctype_Id = enCalcType.TAXABLE_EARNING_TOTAL OrElse objTransactionHead._Calctype_Id = enCalcType.NON_TAXABLE_EARNING_TOTAL OrElse objTransactionHead._Calctype_Id = enCalcType.NON_CASH_BENEFIT_TOTAL Then
                    cboRoundOffTypeId.Enabled = False
                    cboRoundOffModeId.Enabled = False 'Sohail (11 Dec 2020)
                Else
                    cboRoundOffTypeId.Enabled = True
                    cboRoundOffModeId.Enabled = True 'Sohail (11 Dec 2020)
                End If
                'Sohail (03 Dec 2020) -- End

            End If

            Call GetValue()

            If mblnIsFromEmployee = True Then
                cboTrnHeadType.SelectedValue = enTranHeadType.EarningForEmployees
                cboTypeOf.SelectedValue = enTypeOf.Salary
                cboTrnHeadType.Enabled = False
                cboTypeOf.Enabled = False
            End If
            If mblnIsFromBenefit = True Then
                cboTrnHeadType.SelectedValue = enTranHeadType.EarningForEmployees
                cboTypeOf.SelectedValue = enTypeOf.BENEFIT
                cboTrnHeadType.Enabled = False
                cboTypeOf.Enabled = False
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmTransactionHead_AddEdit_Load", mstrModuleName)
        End Try

    End Sub

    'Anjan (02 Sep 2011)-Start
    'Issue : Including Language Settings.
    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsTransactionHead.SetMessages()
            objfrm._Other_ModuleNames = "clsTransactionHead"
            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub
    'Anjan (02 Sep 2011)-End 

#End Region

#Region " Button's Events "
    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnClose_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnAdd_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAdd.Click
        Dim decPrevAmt As Decimal = 0 'Sohail (11 May 2011)
        Dim strLastPeriodEndDate As String 'Sohail (21 Nov 2011)
        Try
            If CInt(cboCalcType.SelectedValue) = enCalcType.AsComputedValue Or CInt(cboCalcType.SelectedValue) = enCalcType.OverTimeHours Or CInt(cboCalcType.SelectedValue) = enCalcType.ShortHours Then
                If IsValidSimpleSlab(btnAdd.Name) = False Then Exit Sub

                If lvTrnHeadSlabSimple.Items.Count > 0 Then
                    'Sohail (21 Nov 2011) -- Start
                    strLastPeriodEndDate = lvTrnHeadSlabSimple.Items(0).SubItems(colhSimpleEndDate.Index).Text
                    If mdtPeriod_enddate < eZeeDate.convertDate(strLastPeriodEndDate) Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 16, "Sorry! Slab for this Period is already defined."), enMsgBoxStyle.Information)
                        cboSimpleSlabPeriod.Focus()
                        Exit Sub
                    End If
                    'Sohail (21 Nov 2011) -- End

                    'Sohail (21 Nov 2011) -- Start
                    'decPrevAmt = CDec(lvTrnHeadSlabSimple.Items(lvTrnHeadSlabSimple.Items.Count - 1).SubItems(colhSimpleAmtUpto.Index).Text) 'Sohail (11 May 2011)
                    Dim dRow() As DataRow = mdtSimpleSlab.Select("end_date = '" & eZeeDate.convertDate(mdtPeriod_enddate) & "'", "amountupto DESC")
                    If dRow.Length > 0 Then
                        Decimal.TryParse(dRow(0).Item("amountupto").ToString, decPrevAmt)
                    End If
                    'Sohail (21 Nov 2011) -- End
                    If decPrevAmt >= CDec(txtSimpleSlabAmtUpto.Text) Then 'Sohail (11 May 2011)
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 14, "Sorry, Amount should be greater than Previous Amount."), enMsgBoxStyle.Information)
                        txtSimpleSlabAmtUpto.Focus()
                        Exit Sub
                    End If
                End If

                Dim drSlab As DataRow
                drSlab = mdtSimpleSlab.NewRow()
                With drSlab
                    'Sohail (24 Sep 2013) -- Start
                    'TRA - ENHANCEMENT
                    '.Item("tranheadslabtranunkid") = -1
                    .Item("tranheadslabtranunkid") = mdtSimpleSlab.Rows.Count + 1 - ((mdtSimpleSlab.Rows.Count + 1) * 2)
                    'Sohail (24 Sep 2013) -- End
                    .Item("tranheadunkid") = mintTranHeadUnkid
                    .Item("amountupto") = txtSimpleSlabAmtUpto.Decimal 'Sohail (03 Sep 2010), 'Sohail (11 May 2011)
                    .Item("slabtype") = cboSimpleSlabAmountType.SelectedValue
                    .Item("valuebasis") = txtSimpleSlabValueBasis.Decimal 'Sohail (03 Sep 2010), 'Sohail (11 May 2011)
                    .Item("userunkid") = User._Object._Userunkid 'Sohail (21 Nov 2011)
                    .Item("isvoid") = False
                    .Item("voiduserunkid") = -1
                    .Item("voiddatetime") = DBNull.Value
                    .Item("voidreason") = ""
                    .Item("GUID") = Guid.NewGuid.ToString
                    .Item("AUD") = "A"
                    'Sohail (21 Nov 2011) -- Start
                    .Item("periodunkid") = CInt(cboSimpleSlabPeriod.SelectedValue)
                    .Item("period_name") = cboSimpleSlabPeriod.Text
                    .Item("end_date") = eZeeDate.convertDate(mdtPeriod_enddate)
                    'Sohail (24 Sep 2013) -- Start
                    'TRA - ENHANCEMENT
                    If CInt(cboSimpleSlabAmountType.SelectedValue) = enPaymentBy.Fromula Then
                        .Item("formula") = txtSlabFormula.Text
                        .Item("formulaid") = txtSlabFormula.Tag
                    Else
                        .Item("formula") = ""
                        .Item("formulaid") = ""
                    End If
                    'Sohail (24 Sep 2013) -- End
                    'Sohail (21 Nov 2011) -- End
                End With
                mdtSimpleSlab.Rows.Add(drSlab)

                Call FillSimpleSlabList()
                Call ResetSimpleSlab()

                txtSimpleSlabAmtUpto.Focus()
            ElseIf CInt(cboCalcType.SelectedValue) = enCalcType.AsComputedOnWithINEXCESSOFTaxSlab Then
                If IsValidTaxSlab() = False Then Exit Sub

                If lvTrnHeadSlabInExcessof.Items.Count > 0 Then
                    'Sohail (21 Nov 2011) -- Start
                    strLastPeriodEndDate = lvTrnHeadSlabInExcessof.Items(0).SubItems(colhTaxEndDate.Index).Text
                    If mdtPeriod_enddate < eZeeDate.convertDate(strLastPeriodEndDate) Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 16, "Sorry! Slab for this Period is already defined."), enMsgBoxStyle.Information)
                        cboTaxSlabPeriod.Focus()
                        Exit Sub
                    End If
                    'Sohail (21 Nov 2011) -- End

                    'Sohail (21 Nov 2011) -- Start
                    'decPrevAmt = CDec(lvTrnHeadSlabInExcessof.Items(lvTrnHeadSlabInExcessof.Items.Count - 1).SubItems(colhTaxAmountUpto.Index).Text) 'Sohail (11 May 2011)
                    Dim dRow() As DataRow = mdtTaxSlab.Select("end_date = '" & eZeeDate.convertDate(mdtPeriod_enddate) & "'", "amountupto DESC")
                    If dRow.Length > 0 Then
                        Decimal.TryParse(dRow(0).Item("amountupto").ToString, decPrevAmt)
                    End If
                    'Sohail (21 Nov 2011) -- End
                    If decPrevAmt >= CDec(txtTaxSlabAmtUpto.Text) Then 'Sohail (11 May 2011)
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 14, "Sorry, Amount should be greater than Previous Amount."), enMsgBoxStyle.Information)
                        txtTaxSlabAmtUpto.Focus()
                        Exit Sub
                    End If
                End If

                Dim drSlab As DataRow
                drSlab = mdtTaxSlab.NewRow()
                With drSlab
                    .Item("tranheadtaxslabtranunkid") = -1
                    .Item("tranheadunkid") = mintTranHeadUnkid
                    .Item("amountupto") = txtTaxSlabAmtUpto.Decimal 'Sohail (03 Sep 2010), 'Sohail (11 May 2011)
                    .Item("fixedamount") = txtTaxSlabFixedAmount.Decimal 'Sohail (03 Sep 2010), 'Sohail (11 May 2011)
                    .Item("ratepayable") = txtTaxSlabRatePayable.Decimal 'Sohail (03 Sep 2010)
                    .Item("inexcessof") = decPrevAmt
                    .Item("userunkid") = User._Object._Userunkid 'Sohail (21 Nov 2011)
                    .Item("isvoid") = False
                    .Item("voiduserunkid") = -1
                    .Item("voiddatetime") = DBNull.Value
                    .Item("voidreason") = ""
                    .Item("GUID") = Guid.NewGuid.ToString
                    .Item("AUD") = "A"
                    'Sohail (21 Nov 2011) -- Start
                    .Item("periodunkid") = CInt(cboTaxSlabPeriod.SelectedValue)
                    .Item("period_name") = cboTaxSlabPeriod.Text
                    .Item("end_date") = eZeeDate.convertDate(mdtPeriod_enddate)
                    'Sohail (21 Nov 2011) -- End
                End With
                mdtTaxSlab.Rows.Add(drSlab)

                Call FillTaxSlabList()
                Call ResetTaxSlab()

                txtTaxSlabAmtUpto.Focus()
            End If


        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnAdd_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnEdit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEdit.Click
        Dim decAmtUpto As Decimal = 0
        Dim decPrevAmt As Decimal = 0
        Dim decNextAmt As Decimal = 0
        Dim strLastPeriodEndDate As String 'Sohail (21 Nov 2011)
        Try
            If CInt(cboCalcType.SelectedValue) = enCalcType.AsComputedValue Or CInt(cboCalcType.SelectedValue) = enCalcType.OverTimeHours Or CInt(cboCalcType.SelectedValue) = enCalcType.ShortHours Then
                If lvTrnHeadSlabSimple.SelectedItems.Count > 0 Then
                    mintItemIndex = lvTrnHeadSlabSimple.SelectedItems(0).Index

                    If IsValidSimpleSlab(btnEdit.Name) = False Then Exit Sub
                    decAmtUpto = CDec(txtSimpleSlabAmtUpto.Decimal) 'Sohail (11 May 2011), 'Sohail (21 Nov 2011)

                    strLastPeriodEndDate = lvTrnHeadSlabSimple.Items(0).SubItems(colhSimpleEndDate.Index).Text 'Sohail (21 Nov 2011)

                    If mintItemIndex > 0 AndAlso CInt(cboSimpleSlabPeriod.SelectedValue) = CInt(lvTrnHeadSlabSimple.Items(mintItemIndex - 1).SubItems(colhSimplePeriod.Index).Tag) Then
                        decPrevAmt = CDec(lvTrnHeadSlabSimple.Items(mintItemIndex - 1).SubItems(colhSimpleAmtUpto.Index).Text) 'Sohail (11 May 2011)
                        'Sohail (21 Nov 2011) -- Start
                        'If decPrevAmt >= decAmtUpto AndAlso CInt(cboSimpleSlabPeriod.SelectedValue) = CInt(lvTrnHeadSlabSimple.Items(0).SubItems(colhSimplePeriod.Index).Tag) Then
                        If decPrevAmt >= decAmtUpto Then
                            'Sohail (21 Nov 2011) -- End
                            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 14, "Sorry, Amount should be greater than Previous Amount."), enMsgBoxStyle.Information)
                            txtSimpleSlabAmtUpto.Focus()
                            Exit Sub
                        End If
                    End If
                    'Sohail (21 Nov 2011) -- Start
                    If mintItemIndex < lvTrnHeadSlabSimple.Items.Count - 1 AndAlso CInt(lvTrnHeadSlabSimple.Items(mintItemIndex + 1).SubItems(colhSimplePeriod.Index).Tag) = CInt(cboSimpleSlabPeriod.SelectedValue) Then
                        'If mintItemIndex < lvTrnHeadSlabSimple.Items.Count - 1 Then
                        'Sohail (21 Nov 2011) -- End
                        decNextAmt = CDec(lvTrnHeadSlabSimple.Items(mintItemIndex + 1).SubItems(colhSimpleAmtUpto.Index).Text) 'Sohail (11 May 2011)
                        If decAmtUpto >= decNextAmt Then
                            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 17, "Sorry, Amount should be less than next Amount."), enMsgBoxStyle.Information)
                            txtSimpleSlabAmtUpto.Focus()
                            Exit Sub
                        End If
                    End If

                    Dim drTemp As DataRow()

                    If CInt(lvTrnHeadSlabSimple.Items(mintItemIndex).Tag) = -1 Then
                        drTemp = mdtSimpleSlab.Select("guid = '" & lvTrnHeadSlabSimple.Items(mintItemIndex).SubItems(colhSimpleGUID.Index).Text & "'")
                    Else
                        drTemp = mdtSimpleSlab.Select("tranheadslabtranunkid = " & CInt(lvTrnHeadSlabSimple.Items(mintItemIndex).Tag))
                    End If

                    If drTemp.Length > 0 Then
                        With drTemp(0)
                            .Item("tranheadslabtranunkid") = lvTrnHeadSlabSimple.Items(mintItemIndex).Tag
                            .Item("tranheadunkid") = mintTranHeadUnkid
                            .Item("amountupto") = txtSimpleSlabAmtUpto.Decimal 'Sohail (03 Sep 2010), 'Sohail (11 May 2011)
                            .Item("slabtype") = cboSimpleSlabAmountType.SelectedValue
                            .Item("valuebasis") = txtSimpleSlabValueBasis.Decimal 'Sohail (03 Sep 2010), 'Sohail (11 May 2011)
                            .Item("userunkid") = User._Object._Userunkid 'Sohail (21 Nov 2011)
                            .Item("isvoid") = False
                            .Item("voiduserunkid") = -1
                            .Item("voiddatetime") = DBNull.Value
                            .Item("voidreason") = ""
                            .Item("GUID") = Guid.NewGuid.ToString
                            If IsDBNull(.Item("AUD")) Or .Item("AUD").ToString.Trim = "" Then
                                .Item("AUD") = "U"
                            End If
                            'Sohail (21 Nov 2011) -- Start
                            .Item("periodunkid") = CInt(cboSimpleSlabPeriod.SelectedValue)
                            .Item("period_name") = cboSimpleSlabPeriod.Text
                            .Item("end_date") = eZeeDate.convertDate(mdtPeriod_enddate)
                            'Sohail (21 Nov 2011) -- End
                            'Sohail (24 Sep 2013) -- Start
                            'TRA - ENHANCEMENT
                            If CInt(cboSimpleSlabAmountType.SelectedValue) = enPaymentBy.Fromula Then
                                .Item("formula") = txtSlabFormula.Text
                                .Item("formulaid") = txtSlabFormula.Tag
                            Else
                                .Item("formula") = ""
                                .Item("formulaid") = ""
                            End If
                            'Sohail (24 Sep 2013) -- End
                            .AcceptChanges()
                        End With
                        Call FillSimpleSlabList()
                        Call ResetSimpleSlab()
                    End If
                End If

            ElseIf CInt(cboCalcType.SelectedValue) = enCalcType.AsComputedOnWithINEXCESSOFTaxSlab Then
                If lvTrnHeadSlabInExcessof.SelectedItems.Count > 0 Then
                    mintItemIndex = lvTrnHeadSlabInExcessof.SelectedItems(0).Index

                    If IsValidTaxSlab() = False Then Exit Sub
                    decAmtUpto = CDec(txtTaxSlabAmtUpto.Decimal) 'Sohail (11 May 2011), 'Sohail (21 Nov 2011)

                    'Sohail (21 Nov 2011) -- Start
                    If mintItemIndex > 0 AndAlso CInt(cboTaxSlabPeriod.SelectedValue) = CInt(lvTrnHeadSlabInExcessof.Items(mintItemIndex - 1).SubItems(colhTaxPeriod.Index).Tag) Then
                        'If mintItemIndex > 0 Then
                        'Sohail (21 Nov 2011) -- End
                        decPrevAmt = CDec(lvTrnHeadSlabInExcessof.Items(mintItemIndex - 1).SubItems(colhTaxAmountUpto.Index).Text) 'Sohail (11 May 2011)
                        If decPrevAmt >= decAmtUpto Then
                            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 14, "Sorry, Amount should be greater than Previous Amount."), enMsgBoxStyle.Information)
                            txtTaxSlabAmtUpto.Focus()
                            Exit Sub
                        End If
                    End If
                    'Sohail (21 Nov 2011) -- Start
                    If mintItemIndex < lvTrnHeadSlabInExcessof.Items.Count - 1 AndAlso CInt(lvTrnHeadSlabInExcessof.Items(mintItemIndex + 1).SubItems(colhTaxPeriod.Index).Tag) = CInt(cboTaxSlabPeriod.SelectedValue) Then
                        'If mintItemIndex < lvTrnHeadSlabInExcessof.Items.Count - 1 Then
                        'Sohail (21 Nov 2011) -- End
                        decNextAmt = CDec(lvTrnHeadSlabInExcessof.Items(mintItemIndex + 1).SubItems(colhTaxAmountUpto.Index).Text) 'Sohail (11 May 2011)
                        If decAmtUpto >= decNextAmt Then
                            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 17, "Sorry, Amount should be less than next Amount."), enMsgBoxStyle.Information)
                            txtTaxSlabAmtUpto.Focus()
                            Exit Sub
                        End If
                    End If

                    Dim drTemp As DataRow()

                    If CInt(lvTrnHeadSlabInExcessof.Items(mintItemIndex).Tag) = -1 Then
                        drTemp = mdtTaxSlab.Select("guid = '" & lvTrnHeadSlabInExcessof.Items(mintItemIndex).SubItems(colhTaxGUID.Index).Text & "'")
                    Else
                        drTemp = mdtTaxSlab.Select("tranheadtaxslabtranunkid = " & CInt(lvTrnHeadSlabInExcessof.Items(mintItemIndex).Tag))
                    End If

                    If drTemp.Length > 0 Then
                        With drTemp(0)
                            .Item("tranheadtaxslabtranunkid") = lvTrnHeadSlabInExcessof.Items(mintItemIndex).Tag
                            .Item("tranheadunkid") = mintTranHeadUnkid
                            .Item("amountupto") = txtTaxSlabAmtUpto.Decimal 'Sohail (03 Sep 2010), 'Sohail (11 May 2011)
                            .Item("fixedamount") = txtTaxSlabFixedAmount.Decimal 'Sohail (03 Sep 2010), 'Sohail (11 May 2011)
                            .Item("ratepayable") = txtTaxSlabRatePayable.Decimal 'Sohail (03 Sep 2010)
                            .Item("inexcessof") = decPrevAmt
                            .Item("userunkid") = User._Object._Userunkid 'Sohail (21 Nov 2011)
                            .Item("isvoid") = False
                            .Item("voiduserunkid") = -1
                            .Item("voiddatetime") = DBNull.Value
                            .Item("voidreason") = ""
                            .Item("GUID") = Guid.NewGuid.ToString
                            If IsDBNull(.Item("AUD")) Or .Item("AUD").ToString.Trim = "" Then
                                .Item("AUD") = "U"
                            End If
                            'Sohail (21 Nov 2011) -- Start
                            .Item("periodunkid") = CInt(cboTaxSlabPeriod.SelectedValue)
                            .Item("period_name") = cboTaxSlabPeriod.Text
                            .Item("end_date") = eZeeDate.convertDate(mdtPeriod_enddate)
                            'Sohail (21 Nov 2011) -- End
                            .AcceptChanges()
                        End With

                        If decNextAmt > 0 Then
                            If CInt(lvTrnHeadSlabInExcessof.Items(mintItemIndex + 1).Tag) = -1 Then
                                drTemp = mdtTaxSlab.Select("guid = '" & lvTrnHeadSlabInExcessof.Items(mintItemIndex + 1).SubItems(colhTaxGUID.Index).Text & "'")
                            Else
                                drTemp = mdtTaxSlab.Select("tranheadtaxslabtranunkid = " & CInt(lvTrnHeadSlabInExcessof.Items(mintItemIndex + 1).Tag))
                            End If

                            If drTemp.Length > 0 Then
                                With drTemp(0)
                                    .Item("inexcessof") = decAmtUpto
                                    .AcceptChanges()
                                End With
                            End If
                        End If
                        Call FillTaxSlabList()
                        Call ResetTaxSlab()
                    End If
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnEdit_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDelete.Click
        Try
            If CInt(cboCalcType.SelectedValue) = enCalcType.AsComputedValue Or CInt(cboCalcType.SelectedValue) = enCalcType.OverTimeHours Or CInt(cboCalcType.SelectedValue) = enCalcType.ShortHours Then

                If lvTrnHeadSlabSimple.SelectedItems.Count > 0 Then
                    mintItemIndex = lvTrnHeadSlabSimple.SelectedItems(0).Index

                    'Sohail (21 Nov 2011) -- Start
                    If cboSimpleSlabPeriod.SelectedValue Is Nothing Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 15, "Sorry! This Period is closed."), enMsgBoxStyle.Information)
                        Exit Sub
                    End If
                    'Sohail (21 Nov 2011) -- End

                    If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 18, "Are you sure you want to delete this slab?"), CType(enMsgBoxStyle.Question + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.Yes Then
                        Dim drTemp As DataRow()

                        'Sohail (24 Sep 2013) -- Start
                        'TRA - ENHANCEMENT
                        drTemp = mdtFormula.Select("tranheadslabtranunkid = " & CInt(lvTrnHeadSlabSimple.Items(mintItemIndex).Tag))
                        If drTemp.Length > 0 Then
                            For Each drRow As DataRow In drTemp
                                drRow.Item("AUD") = "D"
                            Next
                            drTemp(0).AcceptChanges()
                        End If

                        drTemp = mdtFormulaLeave.Select("tranheadslabtranunkid = " & CInt(lvTrnHeadSlabSimple.Items(mintItemIndex).Tag))
                        If drTemp.Length > 0 Then
                            For Each drRow As DataRow In drTemp
                                drRow.Item("AUD") = "D"
                            Next
                            drTemp(0).AcceptChanges()
                        End If

                        drTemp = mdtFormulaCurrency.Select("tranheadslabtranunkid = " & CInt(lvTrnHeadSlabSimple.Items(mintItemIndex).Tag))
                        If drTemp.Length > 0 Then
                            For Each drRow As DataRow In drTemp
                                drRow.Item("AUD") = "D"
                            Next
                            drTemp(0).AcceptChanges()
                        End If

                        drTemp = mdtFormulaMeasurementUnit.Select("tranheadslabtranunkid = " & CInt(lvTrnHeadSlabSimple.Items(mintItemIndex).Tag))
                        If drTemp.Length > 0 Then
                            For Each drRow As DataRow In drTemp
                                drRow.Item("AUD") = "D"
                            Next
                            drTemp(0).AcceptChanges()
                        End If

                        drTemp = mdtFormulaMeasurementAmount.Select("tranheadslabtranunkid = " & CInt(lvTrnHeadSlabSimple.Items(mintItemIndex).Tag))
                        If drTemp.Length > 0 Then
                            For Each drRow As DataRow In drTemp
                                drRow.Item("AUD") = "D"
                            Next
                            drTemp(0).AcceptChanges()
                        End If

                        drTemp = mdtFormulaActivityUnit.Select("tranheadslabtranunkid = " & CInt(lvTrnHeadSlabSimple.Items(mintItemIndex).Tag))
                        If drTemp.Length > 0 Then
                            For Each drRow As DataRow In drTemp
                                drRow.Item("AUD") = "D"
                            Next
                            drTemp(0).AcceptChanges()
                        End If

                        drTemp = mdtFormulaActivityAmount.Select("tranheadslabtranunkid = " & CInt(lvTrnHeadSlabSimple.Items(mintItemIndex).Tag))
                        If drTemp.Length > 0 Then
                            For Each drRow As DataRow In drTemp
                                drRow.Item("AUD") = "D"
                            Next
                            drTemp(0).AcceptChanges()
                        End If
                        'Sohail (24 Sep 2013) -- End

                        'Sohail (29 Oct 2013) -- Start
                        'TRA - ENHANCEMENT
                        drTemp = mdtFormulaShift.Select("tranheadslabtranunkid = " & CInt(lvTrnHeadSlabSimple.Items(mintItemIndex).Tag))
                        If drTemp.Length > 0 Then
                            For Each drRow As DataRow In drTemp
                                drRow.Item("AUD") = "D"
                            Next
                            drTemp(0).AcceptChanges()
                        End If
                        'Sohail (29 Oct 2013) -- End

                        'Sohail (09 Nov 2013) -- Start
                        'TRA - ENHANCEMENT
                        drTemp = mdtFormulaCumulative.Select("tranheadslabtranunkid = " & CInt(lvTrnHeadSlabSimple.Items(mintItemIndex).Tag))
                        If drTemp.Length > 0 Then
                            For Each drRow As DataRow In drTemp
                                drRow.Item("AUD") = "D"
                            Next
                            drTemp(0).AcceptChanges()
                        End If
                        'Sohail (09 Nov 2013) -- End

                        'Sohail (11 Sep 2014) -- Start
                        'Enhancement - New Payslip Template 10 for FINCA DRC.
                        drTemp = mdtFormulaCMaster.Select("tranheadslabtranunkid = " & CInt(lvTrnHeadSlabSimple.Items(mintItemIndex).Tag))
                        If drTemp.Length > 0 Then
                            For Each drRow As DataRow In drTemp
                                drRow.Item("AUD") = "D"
                            Next
                            drTemp(0).AcceptChanges()
                        End If
                        'Sohail (11 Sep 2014) -- End

                        'Sohail (12 Nov 2014) -- Start
                        'Voltamp Enhancement - Quentity Total and Amount Total for Leave Claim Request in transaction head formula.
                        drTemp = mdtFormulaCRExpense.Select("tranheadslabtranunkid = " & CInt(lvTrnHeadSlabSimple.Items(mintItemIndex).Tag))
                        If drTemp.Length > 0 Then
                            For Each drRow As DataRow In drTemp
                                drRow.Item("AUD") = "D"
                            Next
                            drTemp(0).AcceptChanges()
                        End If
                        'Sohail (12 Nov 2014) -- End

                        'Sohail (24 Oct 2017) -- Start
                        'CCBRT Enhancement - 70.1 - (Ref. Id - 73) - Add a loan function (like the leave function) in payroll where you can select loan scheme, interest or EMI, total loan amount, etc.
                        drTemp = mdtFormulaLoanScheme.Select("tranheadslabtranunkid = " & CInt(lvTrnHeadSlabSimple.Items(mintItemIndex).Tag))
                        If drTemp.Length > 0 Then
                            For Each drRow As DataRow In drTemp
                                drRow.Item("AUD") = "D"
                            Next
                            drTemp(0).AcceptChanges()
                        End If
                        'Sohail (24 Oct 2017) -- End

                        If CInt(lvTrnHeadSlabSimple.Items(mintItemIndex).Tag) = -1 Then
                            drTemp = mdtSimpleSlab.Select("guid = '" & lvTrnHeadSlabSimple.Items(mintItemIndex).SubItems(colhSimpleGUID.Index).Text & "'")
                        Else
                            drTemp = mdtSimpleSlab.Select("tranheadslabtranunkid = " & CInt(lvTrnHeadSlabSimple.Items(mintItemIndex).Tag))
                        End If

                        If drTemp.Length > 0 Then
                            drTemp(0).Item("AUD") = "D"
                            drTemp(0).AcceptChanges()
                            Call FillSimpleSlabList()
                            Call ResetSimpleSlab()
                        End If

                    End If
                End If

            ElseIf CInt(cboCalcType.SelectedValue) = enCalcType.AsComputedOnWithINEXCESSOFTaxSlab Then
                If lvTrnHeadSlabInExcessof.SelectedItems.Count > 0 Then
                    mintItemIndex = lvTrnHeadSlabInExcessof.SelectedItems(0).Index

                    'Sohail (21 Nov 2011) -- Start
                    If cboTaxSlabPeriod.SelectedValue Is Nothing Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 15, "Sorry! This Period is closed."), enMsgBoxStyle.Information)
                        Exit Sub
                    End If
                    'Sohail (21 Nov 2011) -- End

                    If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 18, "Are you sure you want to delete this slab?"), CType(enMsgBoxStyle.Question + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.Yes Then
                        Dim drTemp As DataRow()

                        If CInt(lvTrnHeadSlabInExcessof.Items(mintItemIndex).Tag) = -1 Then
                            drTemp = mdtTaxSlab.Select("guid = '" & lvTrnHeadSlabInExcessof.Items(mintItemIndex).SubItems(colhTaxGUID.Index).Text & "'")
                        Else
                            drTemp = mdtTaxSlab.Select("tranheadtaxslabtranunkid = " & CInt(lvTrnHeadSlabInExcessof.Items(mintItemIndex).Tag))
                        End If

                        If drTemp.Length > 0 Then
                            drTemp(0).Item("AUD") = "D"
                            drTemp(0).AcceptChanges()

                            If mintItemIndex < lvTrnHeadSlabInExcessof.Items.Count - 1 Then
                                If CInt(lvTrnHeadSlabInExcessof.Items(mintItemIndex + 1).Tag) = -1 Then
                                    drTemp = mdtTaxSlab.Select("guid = '" & lvTrnHeadSlabInExcessof.Items(mintItemIndex + 1).SubItems(colhTaxGUID.Index).Text & "'")
                                Else
                                    drTemp = mdtTaxSlab.Select("tranheadtaxslabtranunkid = " & CInt(lvTrnHeadSlabInExcessof.Items(mintItemIndex + 1).Tag))
                                End If

                                If drTemp.Length > 0 Then
                                    With drTemp(0)
                                        .Item("inexcessof") = CDec(lvTrnHeadSlabInExcessof.Items(mintItemIndex).SubItems(colhTaxInExcessOf.Index).Text) 'Sohail (11 May 2011)
                                        .AcceptChanges()
                                    End With
                                End If
                            End If

                            Call FillTaxSlabList()
                            Call ResetTaxSlab()
                        End If
                    End If

                End If
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnDelete_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Dim blnFlag As Boolean = False


        'Gajanan [1-July-2020] -- Start
        'Enhancement #ARUTI-1276 Assist to provide Email notification to some selected users when Transaction head is EDITED or ADDED
        Dim blnSendNotification As Boolean = False
        'Gajanan [1-July-2020] -- End


        Try
            If IsValidInput() = False Then Exit Sub

            Call SetValue()

            'S.SANDEEP [28-May-2018] -- START
            'ISSUE/ENHANCEMENT : {Audit Trails} 
            objTransactionHead._FormName = mstrModuleName
            objTransactionHead._LoginEmployeeunkid = 0
            objTransactionHead._ClientIP = getIP()
            objTransactionHead._HostName = getHostName()
            objTransactionHead._FromWeb = False
            objTransactionHead._AuditUserId = User._Object._Userunkid
objTransactionHead._CompanyUnkid = Company._Object._Companyunkid
            objTransactionHead._AuditDate = ConfigParameter._Object._CurrentDateAndTime
            'S.SANDEEP [28-May-2018] -- END

            If menAction = enAction.EDIT_ONE Then
                If CInt(cboCalcType.SelectedValue) = enCalcType.AsComputedValue Or CInt(cboCalcType.SelectedValue) = enCalcType.OverTimeHours Or CInt(cboCalcType.SelectedValue) = enCalcType.ShortHours Then
                    'Sohail (21 Aug 2015) -- Start
                    'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                    'blnFlag = objTransactionHead.Update(CInt(cboCalcType.SelectedValue), mdtSimpleSlab)
                    blnFlag = objTransactionHead.Update(FinancialYear._Object._DatabaseName, CInt(cboCalcType.SelectedValue), ConfigParameter._Object._CurrentDateAndTime, mdtSimpleSlab, Nothing, blnSendNotification)
                    'Sohail (21 Aug 2015) -- End
                ElseIf CInt(cboCalcType.SelectedValue) = enCalcType.AsComputedOnWithINEXCESSOFTaxSlab Then
                    'Sohail (21 Aug 2015) -- Start
                    'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                    'blnFlag = objTransactionHead.Update(CInt(cboCalcType.SelectedValue), mdtTaxSlab)
                    blnFlag = objTransactionHead.Update(FinancialYear._Object._DatabaseName, CInt(cboCalcType.SelectedValue), ConfigParameter._Object._CurrentDateAndTime, mdtTaxSlab, Nothing, blnSendNotification)
                    'Sohail (21 Aug 2015) -- End
                Else
                    'Sohail (21 Aug 2015) -- Start
                    'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                    'blnFlag = objTransactionHead.Update(0)
                    blnFlag = objTransactionHead.Update(FinancialYear._Object._DatabaseName, 0, ConfigParameter._Object._CurrentDateAndTime, Nothing, Nothing, blnSendNotification)
                    'Sohail (21 Aug 2015) -- End
                End If

            Else
                If CInt(cboCalcType.SelectedValue) = enCalcType.AsComputedValue Or CInt(cboCalcType.SelectedValue) = enCalcType.OverTimeHours Or CInt(cboCalcType.SelectedValue) = enCalcType.ShortHours Then
                    'Sohail (21 Aug 2015) -- Start
                    'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                    'blnFlag = objTransactionHead.Insert(CInt(cboCalcType.SelectedValue), mdtSimpleSlab)
                    blnFlag = objTransactionHead.Insert(CInt(cboCalcType.SelectedValue), ConfigParameter._Object._CurrentDateAndTime, mdtSimpleSlab)
                    'Sohail (21 Aug 2015) -- End
                ElseIf CInt(cboCalcType.SelectedValue) = enCalcType.AsComputedOnWithINEXCESSOFTaxSlab Then
                    'Sohail (21 Aug 2015) -- Start
                    'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                    'blnFlag = objTransactionHead.Insert(CInt(cboCalcType.SelectedValue), mdtTaxSlab)
                    blnFlag = objTransactionHead.Insert(CInt(cboCalcType.SelectedValue), ConfigParameter._Object._CurrentDateAndTime, mdtTaxSlab)
                    'Sohail (21 Aug 2015) -- End
                Else
                    'Sohail (21 Aug 2015) -- Start
                    'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                    'blnFlag = objTransactionHead.Insert(0)
                    blnFlag = objTransactionHead.Insert(0, ConfigParameter._Object._CurrentDateAndTime)
                    'Sohail (21 Aug 2015) -- End
                End If

            End If

            If blnFlag = False And objTransactionHead._Message <> "" Then
                eZeeMsgBox.Show(objTransactionHead._Message, enMsgBoxStyle.Information)
            'Gajanan [1-July-2020] -- Start
            'Enhancement #ARUTI-1276 Assist to provide Email notification to some selected users when Transaction head is EDITED or ADDED
            Else
                Dim objConfig As New clsConfigOptions
                Dim EmailUserids As String = objConfig.GetKeyValue(ConfigParameter._Object._Companyunkid, "AddEditTransectionHeadNotificationUserIds", Nothing)
                Dim Ip As String = getIP()
                Dim Hostname As String = getHostName()


                If blnSendNotification Then


                    If menAction = enAction.EDIT_ONE Then
                        objTransactionHead.SendMailTo_User(EmailUserids, ConfigParameter._Object._Companyunkid, mstrForm_Name, _
                                                           User._Object._Userunkid, clsEmployeeDataApproval.enOperationType.EDITED, _
                                                           txtHeadName.Text.Trim, Ip, Hostname, enLogin_Mode.DESKTOP)

                    Else
                        objTransactionHead.SendMailTo_User(EmailUserids, ConfigParameter._Object._Companyunkid, mstrForm_Name, _
                                                   User._Object._Userunkid, clsEmployeeDataApproval.enOperationType.ADDED, _
                                                   txtHeadName.Text.Trim, Ip, Hostname, enLogin_Mode.DESKTOP)

                    End If
                End If
            'Gajanan [1-July-2020] -- End

            End If



            If blnFlag Then
                mblnCancel = False
                If menAction = enAction.ADD_CONTINUE Then
                    lvTrnHeadSlabSimple.Items.Clear()
                    lvTrnHeadSlabInExcessof.Items.Clear()
                    objTransactionHead = Nothing
                    objTransactionHead = New clsTransactionHead
                    objTranSimpleSlab = Nothing
                    objTranSimpleSlab = New clsTranheadSlabTran
                    Call GetValue()
                    txtHeadCode.Focus()
                Else
                    'Sohail (21 Aug 2015) -- Start
                    'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                    'mintTranHeadUnkid = objTransactionHead._Tranheadunkid
                    mintTranHeadUnkid = objTransactionHead._Tranheadunkid(FinancialYear._Object._DatabaseName)
                    'Sohail (21 Aug 2015) -- End
                    Me.Close()
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnSave_Click", mstrModuleName)
        End Try
    End Sub
#End Region

#Region " ComboBox's Events "
    Private Sub cboTrnHeadType_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboTrnHeadType.SelectedIndexChanged 'Sohail (23 Dec 2010)
        Dim objMaster As New clsMasterData
        Dim dsList As DataSet
        Dim dtTable As DataTable
        Try
            dsList = objMaster.getComboListTypeOf("TypeOf", CInt(cboTrnHeadType.SelectedValue), , True)
            'Sohail (23 Dec 2010) -- Start
            If menAction = enAction.EDIT_ONE AndAlso cboTrnHeadType.Enabled = False Then
                dtTable = New DataView(dsList.Tables("TypeOf")).ToTable 'Don't allow user to make SALARY heads
            Else
                'Sohail (28 Jan 2012) -- Start
                'TRA - ENHANCEMENT
                'dtTable = New DataView(dsList.Tables("TypeOf"), "ID <> " & enTypeOf.Salary & "", "", DataViewRowState.CurrentRows).ToTable 'Don't allow user to make SALARY heads
                'Sohail (17 Sep 2014) -- Start
                'Enhancement - System generated Base, OT and PH heads for PPA Activity.
                'dtTable = New DataView(dsList.Tables("TypeOf")).ToTable
                If menAction = enAction.EDIT_ONE Then
                    'Sohail (06 Jun 2015) -- Start
                    'Enhancement - Don't allow user to make PAY PER ACTIVITY heads when non PPA Headsa are not in use.
                    'dtTable = New DataView(dsList.Tables("TypeOf"), "", "", DataViewRowState.CurrentRows).ToTable
                    If objTransactionHead.isUsed(mintTranHeadUnkid) = True Then
                        dtTable = New DataView(dsList.Tables("TypeOf"), "", "", DataViewRowState.CurrentRows).ToTable
                    Else
                        'Sohail (18 Apr 2016) -- Start
                        'Enhancement - 59.1 - NET PAY ROUNDING ADJUSTMENT head for the difference of actual Net Pay and Rounded Net Pay for HERON Portico.
                        'dtTable = New DataView(dsList.Tables("TypeOf"), "ID <> " & enTypeOf.PAY_PER_ACTIVITY & "", "", DataViewRowState.CurrentRows).ToTable 'Don't allow user to make PAY PER ACTIVITY heads
                        dtTable = New DataView(dsList.Tables("TypeOf"), "ID NOT IN (" & enTypeOf.PAY_PER_ACTIVITY & ", " & enTypeOf.NET_PAY_ROUNDING_ADJUSTMENT & ") ", "", DataViewRowState.CurrentRows).ToTable 'Don't allow user to make PAY PER ACTIVITY heads
                        'Sohail (18 Apr 2016) -- End
                    End If
                    'Sohail (06 Jun 2015) -- End
                Else
                    'Sohail (18 Apr 2016) -- Start
                    'Enhancement - 59.1 - NET PAY ROUNDING ADJUSTMENT head for the difference of actual Net Pay and Rounded Net Pay for HERON Portico.
                    'dtTable = New DataView(dsList.Tables("TypeOf"), "ID <> " & enTypeOf.PAY_PER_ACTIVITY & "", "", DataViewRowState.CurrentRows).ToTable 'Don't allow user to make PAY PER ACTIVITY heads
                    dtTable = New DataView(dsList.Tables("TypeOf"), "ID NOT IN (" & enTypeOf.PAY_PER_ACTIVITY & ", " & enTypeOf.NET_PAY_ROUNDING_ADJUSTMENT & ") ", "", DataViewRowState.CurrentRows).ToTable 'Don't allow user to make PAY PER ACTIVITY heads
                    'Sohail (18 Apr 2016) -- End
                End If
                'Sohail (17 Sep 2014) -- End
                'Sohail (28 Jan 2012) -- End
            End If

            'Sohail (23 Dec 2010) -- End
            With cboTypeOf
                .ValueMember = "Id"
                .DisplayMember = "Name"
                'Sohail (23 Dec 2010) -- Start
                '.DataSource = dsList.Tables("TypeOf")
                .DataSource = dtTable
                'Sohail (23 Dec 2010) -- End
                If .Items.Count > 0 Then .SelectedIndex = 0
            End With

            'Sohail (10 Sep 2010) -- Start
            'Sohail (12 Aug 2010) -- Start
            'If CInt(cboTrnHeadType.SelectedValue) = enTranHeadType.Informational Then
            '    cboTypeOf.SelectedValue = enTypeOf.Informational
            '    cboTypeOf.Enabled = False
            'Else
            '    cboTypeOf.Enabled = True
            'End If
            'Sohail (12 Aug 2010) -- End
            'Sohail (23 Dec 2010) -- Start
            'If menAction = enAction.EDIT_ONE Then
            '    cboTypeOf.Enabled = False 
            'Else
            If CInt(cboTrnHeadType.SelectedValue) = enTranHeadType.Informational Then
                'Sohail (23 Dec 2010) -- End
                cboTypeOf.SelectedValue = enTypeOf.Informational

                'S.SANDEEP [ 04 SEP 2012 ] -- START
                'ENHANCEMENT : TRA CHANGES
                'PURPOSE : INCLUSION OF SALARY HEAD IN INFORMATIONAL HEAD AND GIVE SELECTION TO USER
                'cboTypeOf.Enabled = False
                'S.SANDEEP [ 04 SEP 2012 ] -- END


                'Sohail (23 Dec 2010) -- Start
            ElseIf menAction = enAction.EDIT_ONE AndAlso (objTransactionHead._Typeof_id = enTypeOf.Salary OrElse objTransactionHead.isUsed(mintTranHeadUnkid) = True) Then
                cboTypeOf.Enabled = False
                'Sohail (23 Dec 2010) -- End
                'Sohail (17 Sep 2014) -- Start
                'Enhancement - System generated Base, OT and PH heads for PPA Activity.
                'Sohail (18 Apr 2016) -- Start
                'Enhancement - 59.1 - NET PAY ROUNDING ADJUSTMENT head for the difference of actual Net Pay and Rounded Net Pay for HERON Portico.
                'ElseIf menAction = enAction.EDIT_ONE AndAlso objTransactionHead._Typeof_id = enTypeOf.PAY_PER_ACTIVITY Then
            ElseIf menAction = enAction.EDIT_ONE AndAlso (objTransactionHead._Typeof_id = enTypeOf.PAY_PER_ACTIVITY OrElse objTransactionHead._Typeof_id = enTypeOf.NET_PAY_ROUNDING_ADJUSTMENT) Then
                'Sohail (18 Apr 2016) -- End
                cboTypeOf.Enabled = False
                'Sohail (17 Sep 2014) -- End
            Else
                cboTypeOf.Enabled = True
            End If
            'Sohail (10 Sep 2010) -- End

            'Sohail (18 Jan 2012) -- Start
            'TRA - ENHANCEMENT
            If CInt(cboTrnHeadType.SelectedValue) = enTranHeadType.EarningForEmployees Then
                chkTaxable.Enabled = True
                'chkNonCashBenefit.Enabled = True 'Sohail (18 May 2013) 'Sohail (14 Jun 2013)
            Else
                chkTaxable.Checked = False
                chkTaxable.Enabled = False
                'Sohail (18 May 2013) -- Start
                'TRA - ENHANCEMENT
                'Sohail (14 Jun 2013) -- Start
                'TRA - ENHANCEMENT
                'chkNonCashBenefit.Checked = False
                'chkNonCashBenefit.Enabled = False
                'Sohail (14 Jun 2013) -- End
                'Sohail (18 May 2013) -- End
            End If
            'Sohail (18 Jan 2012) -- End

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboTrnHeadType_SelectedValueChanged", mstrModuleName)
        Finally
            objMaster = Nothing
        End Try
    End Sub

    Private Sub cboTypeOf_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboTypeOf.SelectedIndexChanged 'Sohail (23 Dec 2010)
        Dim objMaster As New clsMasterData
        Dim dsList As DataSet
        Dim dtTable As DataTable
        'Sohail (18 Jan 2012) -- Start
        'TRA - ENHANCEMENT
        'Dim strDefaultHeadCalcType As String = enCalcType.NET_PAY & "," & enCalcType.TOTAL_EARNING & "," & enCalcType.TOTAL_DEDUCTION
        Dim strDefaultHeadCalcType As String = enCalcType.NET_PAY & "," & enCalcType.TOTAL_EARNING & "," & enCalcType.TOTAL_DEDUCTION & "," & enCalcType.TAXABLE_EARNING_TOTAL & "," & enCalcType.NON_TAXABLE_EARNING_TOTAL & "," & enCalcType.NON_CASH_BENEFIT_TOTAL
        'Sohail (18 May 2013) - [NON_CASH_BENEFIT_TOTAL]
        'Sohail (18 Jan 2012) -- End
        Dim strPPACalcType As String = enCalcType.PAY_PER_ACTIVITY & "," & enCalcType.PAY_PER_ACTIVITY_OT & "," & enCalcType.PAY_PER_ACTIVITY_PH 'Sohail (17 Sep 2014)
        Dim strNetPayRoundingAdjustmentCalcType As String = CStr(enCalcType.NET_PAY_ROUNDING_ADJUSTMENT) 'Sohail (18 Apr 2016)
        Try
            'chkRecurrent.Checked = True 'Sohail (20 Apr 2011)
            Select Case CInt(cboTypeOf.SelectedValue)
                Case Is > 1
                    'Sohail (18 Apr 2016) -- Start
                    'Enhancement - 59.1 - NET PAY ROUNDING ADJUSTMENT head for the difference of actual Net Pay and Rounded Net Pay for HERON Portico.
                    'dsList = objMaster.getComboListCalcType("CalcType", "2", , True) 'Sohail (28 Jan 2012)
                    'Hemant (22 Aug 2020) -- Start
                    'NMB Enhancement : Allow to edit payable heads on head master list to map default cost center
                    If menAction = enAction.EDIT_ONE AndAlso objTransactionHead._Calctype_Id = enCalcType.EMPLOYER_CONTRIBUTION_PAYABLE Then
                        dsList = objMaster.getComboListCalcType("CalcType", "2", , True, , True, , True)
                    Else
                        'Hemant (22 Aug 2020) -- End
                    dsList = objMaster.getComboListCalcType("CalcType", "2", , True, , True)
                    End If 'Hemant (22 Aug 2020)
                    'Sohail (18 Apr 2016) -- End
                    'Sohail (20 Apr 2011) -- Start
                    'If CInt(cboTypeOf.SelectedValue) = enTypeOf.Bonus Then
                    '    chkRecurrent.Checked = False
                    'End If
                    'Sohail (20 Apr 2011) -- End
                    If CInt(cboTypeOf.SelectedValue) = enTypeOf.Other_Earnings Then
                        'Sohail (23 Dec 2010) -- Start
                        'dtTable = New DataView(dsList.Tables("CalcType"), "Id <> " & enCalcType.ShortHours & " ", "", DataViewRowState.CurrentRows).ToTable
                        'Sohail (17 Sep 2014) -- Start
                        'Enhancement - System generated Base, OT and PH heads for PPA Activity.
                        'dtTable = New DataView(dsList.Tables("CalcType"), "Id NOT IN (" & enCalcType.ShortHours & "," & strDefaultHeadCalcType & ") ", "", DataViewRowState.CurrentRows).ToTable
                        'Sohail (18 Apr 2016) -- Start
                        'Enhancement - 59.1 - NET PAY ROUNDING ADJUSTMENT head for the difference of actual Net Pay and Rounded Net Pay for HERON Portico.
                        'dtTable = New DataView(dsList.Tables("CalcType"), "Id NOT IN (" & enCalcType.ShortHours & "," & strDefaultHeadCalcType & "," & strPPACalcType & ") ", "", DataViewRowState.CurrentRows).ToTable
                        dtTable = New DataView(dsList.Tables("CalcType"), "Id NOT IN (" & enCalcType.ShortHours & "," & strDefaultHeadCalcType & "," & strPPACalcType & "," & strNetPayRoundingAdjustmentCalcType & ") ", "", DataViewRowState.CurrentRows).ToTable
                        'Sohail (18 Apr 2016) -- End
                        'Sohail (17 Sep 2014) -- End
                        'Sohail (23 Dec 2010) -- End
                    ElseIf CInt(cboTypeOf.SelectedValue) = enTypeOf.Other_Deductions_Emp Then
                        'Sohail (23 Dec 2010) -- Start
                        'dtTable = New DataView(dsList.Tables("CalcType"), "Id <> " & enCalcType.OverTimeHours & " ", "", DataViewRowState.CurrentRows).ToTable
                        'Sohail (17 Sep 2014) -- Start
                        'Enhancement - System generated Base, OT and PH heads for PPA Activity.
                        'dtTable = New DataView(dsList.Tables("CalcType"), "Id NOT IN (" & enCalcType.OverTimeHours & "," & strDefaultHeadCalcType & ") ", "", DataViewRowState.CurrentRows).ToTable
                        'Sohail (18 Apr 2016) -- Start
                        'Enhancement - 59.1 - NET PAY ROUNDING ADJUSTMENT head for the difference of actual Net Pay and Rounded Net Pay for HERON Portico.
                        'dtTable = New DataView(dsList.Tables("CalcType"), "Id NOT IN (" & enCalcType.OverTimeHours & "," & strDefaultHeadCalcType & "," & strPPACalcType & ") ", "", DataViewRowState.CurrentRows).ToTable
                        dtTable = New DataView(dsList.Tables("CalcType"), "Id NOT IN (" & enCalcType.OverTimeHours & "," & strDefaultHeadCalcType & "," & strPPACalcType & "," & strNetPayRoundingAdjustmentCalcType & ") ", "", DataViewRowState.CurrentRows).ToTable
                        'Sohail (18 Apr 2016) -- End
                        'Sohail (17 Sep 2014) -- End
                    ElseIf CInt(cboTypeOf.SelectedValue) = enTypeOf.Informational Then
                        If menAction = enAction.EDIT_ONE Then
                            Select Case objTransactionHead._Calctype_Id
                                Case enCalcType.NET_PAY, enCalcType.TOTAL_EARNING, enCalcType.TOTAL_DEDUCTION, enCalcType.TAXABLE_EARNING_TOTAL, enCalcType.NON_TAXABLE_EARNING_TOTAL, enCalcType.NON_CASH_BENEFIT_TOTAL 'Sohail (18 Jan 2012) 
                                    'Sohail (17 Sep 2014) -- Start
                                    'Enhancement - System generated Base, OT and PH heads for PPA Activity.
                                    'dtTable = New DataView(dsList.Tables("CalcType"), "Id NOT IN (" & enCalcType.ShortHours & "," & enCalcType.OverTimeHours & ") ", "", DataViewRowState.CurrentRows).ToTable
                                    'Sohail (18 Apr 2016) -- Start
                                    'Enhancement - 59.1 - NET PAY ROUNDING ADJUSTMENT head for the difference of actual Net Pay and Rounded Net Pay for HERON Portico.
                                    'dtTable = New DataView(dsList.Tables("CalcType"), "Id NOT IN (" & enCalcType.ShortHours & "," & enCalcType.OverTimeHours & "," & strPPACalcType & ") ", "", DataViewRowState.CurrentRows).ToTable
                                    dtTable = New DataView(dsList.Tables("CalcType"), "Id NOT IN (" & enCalcType.ShortHours & "," & enCalcType.OverTimeHours & "," & strPPACalcType & "," & strNetPayRoundingAdjustmentCalcType & ") ", "", DataViewRowState.CurrentRows).ToTable
                                    'Sohail (18 Apr 2016) -- End
                                    'Sohail (17 Sep 2014) -- End
                                Case Else
                                    'Sohail (17 Sep 2014) -- Start
                                    'Enhancement - System generated Base, OT and PH heads for PPA Activity.
                                    'dtTable = New DataView(dsList.Tables("CalcType"), "Id NOT IN (" & enCalcType.ShortHours & "," & enCalcType.OverTimeHours & "," & strDefaultHeadCalcType & ") ", "", DataViewRowState.CurrentRows).ToTable
                                    'Sohail (18 Apr 2016) -- Start
                                    'Enhancement - 59.1 - NET PAY ROUNDING ADJUSTMENT head for the difference of actual Net Pay and Rounded Net Pay for HERON Portico.
                                    'dtTable = New DataView(dsList.Tables("CalcType"), "Id NOT IN (" & enCalcType.ShortHours & "," & enCalcType.OverTimeHours & "," & strDefaultHeadCalcType & "," & strPPACalcType & ") ", "", DataViewRowState.CurrentRows).ToTable
                                    dtTable = New DataView(dsList.Tables("CalcType"), "Id NOT IN (" & enCalcType.ShortHours & "," & enCalcType.OverTimeHours & "," & strDefaultHeadCalcType & "," & strPPACalcType & "," & strNetPayRoundingAdjustmentCalcType & ") ", "", DataViewRowState.CurrentRows).ToTable
                                    'Sohail (18 Apr 2016) -- End
                                    'Sohail (17 Sep 2014) -- End
                            End Select
                        Else
                            'Sohail (17 Sep 2014) -- Start
                            'Enhancement - System generated Base, OT and PH heads for PPA Activity.
                            'dtTable = New DataView(dsList.Tables("CalcType"), "Id NOT IN (" & enCalcType.ShortHours & "," & enCalcType.OverTimeHours & "," & strDefaultHeadCalcType & ") ", "", DataViewRowState.CurrentRows).ToTable
                            'Sohail (18 Apr 2016) -- Start
                            'Enhancement - 59.1 - NET PAY ROUNDING ADJUSTMENT head for the difference of actual Net Pay and Rounded Net Pay for HERON Portico.
                            'dtTable = New DataView(dsList.Tables("CalcType"), "Id NOT IN (" & enCalcType.ShortHours & "," & enCalcType.OverTimeHours & "," & strDefaultHeadCalcType & "," & strPPACalcType & ") ", "", DataViewRowState.CurrentRows).ToTable
                            dtTable = New DataView(dsList.Tables("CalcType"), "Id NOT IN (" & enCalcType.ShortHours & "," & enCalcType.OverTimeHours & "," & strDefaultHeadCalcType & "," & strPPACalcType & "," & strNetPayRoundingAdjustmentCalcType & ") ", "", DataViewRowState.CurrentRows).ToTable
                            'Sohail (18 Apr 2016) -- End
                            'Sohail (17 Sep 2014) -- End
                        End If
                        'Sohail (23 Dec 2010) -- End

                        'Sohail (17 Sep 2014) -- Start
                        'Enhancement - System generated Base, OT and PH heads for PPA Activity.
                    ElseIf CInt(cboTypeOf.SelectedValue) = enTypeOf.PAY_PER_ACTIVITY Then
                        If menAction = enAction.EDIT_ONE Then
                            dtTable = New DataView(dsList.Tables("CalcType"), "Id NOT IN (" & enCalcType.ShortHours & "," & enCalcType.OverTimeHours & "," & strDefaultHeadCalcType & ") ", "", DataViewRowState.CurrentRows).ToTable
                        Else
                            dtTable = New DataView(dsList.Tables("CalcType"), "Id NOT IN (" & enCalcType.ShortHours & "," & enCalcType.OverTimeHours & "," & strDefaultHeadCalcType & "," & strPPACalcType & ") ", "", DataViewRowState.CurrentRows).ToTable
                        End If
                        'Sohail (17 Sep 2014) -- End

                        'Sohail (18 Apr 2016) -- Start
                        'Enhancement - 59.1 - NET PAY ROUNDING ADJUSTMENT head for the difference of actual Net Pay and Rounded Net Pay for HERON Portico.
                    ElseIf CInt(cboTypeOf.SelectedValue) = enTypeOf.NET_PAY_ROUNDING_ADJUSTMENT Then
                        If menAction = enAction.EDIT_ONE Then
                            dtTable = New DataView(dsList.Tables("CalcType"), "Id NOT IN (" & enCalcType.ShortHours & "," & enCalcType.OverTimeHours & "," & strDefaultHeadCalcType & ") ", "", DataViewRowState.CurrentRows).ToTable
                        Else
                            dtTable = New DataView(dsList.Tables("CalcType"), "Id NOT IN (" & enCalcType.ShortHours & "," & enCalcType.OverTimeHours & "," & strDefaultHeadCalcType & "," & strPPACalcType & "," & strNetPayRoundingAdjustmentCalcType & ") ", "", DataViewRowState.CurrentRows).ToTable
                        End If
                        'Sohail (18 Apr 2016) -- End

                    Else
                        'Sohail (17 Sep 2014) -- Start
                        'Enhancement - System generated Base, OT and PH heads for PPA Activity.
                        'dtTable = New DataView(dsList.Tables("CalcType"), "Id NOT IN (" & enCalcType.ShortHours & "," & enCalcType.OverTimeHours & "," & strDefaultHeadCalcType & ") ", "", DataViewRowState.CurrentRows).ToTable
                        'Sohail (18 Apr 2016) -- Start
                        'Enhancement - 59.1 - NET PAY ROUNDING ADJUSTMENT head for the difference of actual Net Pay and Rounded Net Pay for HERON Portico.
                        'dtTable = New DataView(dsList.Tables("CalcType"), "Id NOT IN (" & enCalcType.ShortHours & "," & enCalcType.OverTimeHours & "," & strDefaultHeadCalcType & "," & strPPACalcType & ") ", "", DataViewRowState.CurrentRows).ToTable
                        dtTable = New DataView(dsList.Tables("CalcType"), "Id NOT IN (" & enCalcType.ShortHours & "," & enCalcType.OverTimeHours & "," & strDefaultHeadCalcType & "," & strPPACalcType & "," & strNetPayRoundingAdjustmentCalcType & ") ", "", DataViewRowState.CurrentRows).ToTable
                        'Sohail (18 Apr 2016) -- End
                        'Sohail (17 Sep 2014) -- End
                    End If
                Case Else 'Type of - Salary
                    'Sohail (28 Jan 2012) -- Start
                    'TRA - ENHANCEMENT
                    'dsList = objMaster.getComboListCalcType("CalcType", CInt(cboTypeOf.SelectedValue))
                    'dtTable = New DataView(dsList.Tables("CalcType"), "", "", DataViewRowState.CurrentRows).ToTable
                    'Hemant (22 Aug 2020) -- Start
                    'NMB Enhancement : Allow to edit payable heads on head master list to map default cost center
                    If menAction = enAction.EDIT_ONE AndAlso objTransactionHead._Calctype_Id = enCalcType.EMPLOYER_CONTRIBUTION_PAYABLE Then
                        dsList = objMaster.getComboListCalcType("CalcType", (enTypeOf.Salary & "," & enTypeOf.Allowance).ToString, , , , , , True)
                    Else
                        'Hemant (22 Aug 2020) -- End
                    dsList = objMaster.getComboListCalcType("CalcType", (enTypeOf.Salary & "," & enTypeOf.Allowance).ToString)
                    End If  'Hemant (22 Aug 2020)
                    If menAction = enAction.EDIT_ONE AndAlso objTransactionHead._Calctype_Id = enCalcType.OnAttendance OrElse objTransactionHead._Calctype_Id = enCalcType.DEFINED_SALARY Then
                        'Sohail (17 Sep 2014) -- Start
                        'Enhancement - System generated Base, OT and PH heads for PPA Activity.
                        'dtTable = New DataView(dsList.Tables("CalcType"), "OthId IN (0, " & enTypeOf.Salary & ") OR (OthId = " & enTypeOf.Allowance & " AND Id NOT IN (" & enCalcType.ShortHours & "," & enCalcType.OverTimeHours & "," & strDefaultHeadCalcType & ")) ", "", DataViewRowState.CurrentRows).ToTable
                        'Sohail (18 Apr 2016) -- Start
                        'Enhancement - 59.1 - NET PAY ROUNDING ADJUSTMENT head for the difference of actual Net Pay and Rounded Net Pay for HERON Portico.
                        'dtTable = New DataView(dsList.Tables("CalcType"), "OthId IN (0, " & enTypeOf.Salary & ") OR (OthId = " & enTypeOf.Allowance & " AND Id NOT IN (" & enCalcType.ShortHours & "," & enCalcType.OverTimeHours & "," & strDefaultHeadCalcType & "," & strPPACalcType & ")) ", "", DataViewRowState.CurrentRows).ToTable
                        dtTable = New DataView(dsList.Tables("CalcType"), "OthId IN (0, " & enTypeOf.Salary & ") OR (OthId = " & enTypeOf.Allowance & " AND Id NOT IN (" & enCalcType.ShortHours & "," & enCalcType.OverTimeHours & "," & strDefaultHeadCalcType & "," & strPPACalcType & "," & strNetPayRoundingAdjustmentCalcType & ")) ", "", DataViewRowState.CurrentRows).ToTable
                        'Sohail (18 Apr 2016) -- End
                        'Sohail (17 Sep 2014) -- End
                    Else
                        'S.SANDEEP [ 04 SEP 2012 ] -- START
                        'ENHANCEMENT : TRA CHANGES
                        'PURPOSE : INCLUSION OF SALARY HEAD IN INFORMATIONAL HEAD AND GIVE SELECTION TO USER
                        'dtTable = New DataView(dsList.Tables("CalcType"), "(OthId IN (0, " & enTypeOf.Salary & ") AND Id NOT IN (" & enCalcType.DEFINED_SALARY & "," & enCalcType.OnAttendance & "," & enCalcType.OnHourWorked & ")) OR (OthId = " & enTypeOf.Allowance & " AND Id NOT IN (" & enCalcType.ShortHours & "," & enCalcType.OverTimeHours & "," & strDefaultHeadCalcType & ")) ", "", DataViewRowState.CurrentRows).ToTable
                        If CInt(cboTrnHeadType.SelectedValue) = enTranHeadType.Informational AndAlso CInt(cboTypeOf.SelectedValue) = enTypeOf.Salary Then
                            dtTable = New DataView(dsList.Tables("CalcType"), "Id IN(0," & enCalcType.FlatRate_Others & ")", "", DataViewRowState.CurrentRows).ToTable
                        Else
                            'Sohail (17 Sep 2014) -- Start
                            'Enhancement - System generated Base, OT and PH heads for PPA Activity.
                            'dtTable = New DataView(dsList.Tables("CalcType"), "(OthId IN (0, " & enTypeOf.Salary & ") AND Id NOT IN (" & enCalcType.DEFINED_SALARY & "," & enCalcType.OnAttendance & "," & enCalcType.OnHourWorked & ")) OR (OthId = " & enTypeOf.Allowance & " AND Id NOT IN (" & enCalcType.ShortHours & "," & enCalcType.OverTimeHours & "," & strDefaultHeadCalcType & ")) ", "", DataViewRowState.CurrentRows).ToTable
                            'Sohail (18 Apr 2016) -- Start
                            'Enhancement - 59.1 - NET PAY ROUNDING ADJUSTMENT head for the difference of actual Net Pay and Rounded Net Pay for HERON Portico.
                            'dtTable = New DataView(dsList.Tables("CalcType"), "(OthId IN (0, " & enTypeOf.Salary & ") AND Id NOT IN (" & enCalcType.DEFINED_SALARY & "," & enCalcType.OnAttendance & "," & enCalcType.OnHourWorked & ")) OR (OthId = " & enTypeOf.Allowance & " AND Id NOT IN (" & enCalcType.ShortHours & "," & enCalcType.OverTimeHours & "," & strDefaultHeadCalcType & "," & strPPACalcType & ")) ", "", DataViewRowState.CurrentRows).ToTable
                            dtTable = New DataView(dsList.Tables("CalcType"), "(OthId IN (0, " & enTypeOf.Salary & ") AND Id NOT IN (" & enCalcType.DEFINED_SALARY & "," & enCalcType.OnAttendance & "," & enCalcType.OnHourWorked & ")) OR (OthId = " & enTypeOf.Allowance & " AND Id NOT IN (" & enCalcType.ShortHours & "," & enCalcType.OverTimeHours & "," & strDefaultHeadCalcType & "," & strPPACalcType & "," & strNetPayRoundingAdjustmentCalcType & ")) ", "", DataViewRowState.CurrentRows).ToTable
                            'Sohail (18 Apr 2016) -- End
                            'Sohail (17 Sep 2014) -- End
                        End If
                        'S.SANDEEP [ 04 SEP 2012 ] -- END
                    End If
                    'Sohail (28 Jan 2012) -- End
            End Select

            'Sohail (09 Sep 2017) -- Start
            'Enhancement - 69.1 - Option to set transaction head basic salary as other earning.
            If CInt(cboTypeOf.SelectedValue) = CInt(enTypeOf.Other_Earnings) Then
                chkBasicsalaryasotherearning.Enabled = True
            Else
                If chkBasicsalaryasotherearning.Checked = True Then chkBasicsalaryasotherearning.Checked = False
                chkBasicsalaryasotherearning.Enabled = False
            End If
            'Sohail (09 Sep 2017) -- End

            With cboCalcType
                .ValueMember = "Id"
                .DisplayMember = "Name"
                .DataSource = dtTable
                If .Items.Count > 0 Then .SelectedIndex = 0
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboTypeOf_SelectedValueChanged", mstrModuleName)
        Finally
            objMaster = Nothing
            dsList = Nothing
        End Try
    End Sub

    Private Sub cboCalcType_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboCalcType.SelectedIndexChanged 'Sohail (23 Dec 2010)
        Dim objMaster As New clsMasterData
        Dim dsCombo As DataSet
        Dim dtTable As DataTable = Nothing
        Try
            gbComputationInfo.Enabled = False
            'Sohail (28 Jan 2019) -- Start
            'NMB Enhancement - 76.1 - Option "Prorate Flat Rate head" on Transaction Head for Recurrent Head.
            'chkRecurrent.Visible = False 'Sohail (20 Apr 2011)
            chkRecurrent.Enabled = False
            'Sohail (28 Jan 2019) -- End

            Select Case CInt(cboCalcType.SelectedValue)
                Case enCalcType.AsComputedValue, enCalcType.AsComputedOnWithINEXCESSOFTaxSlab, enCalcType.OverTimeHours, enCalcType.ShortHours
                    gbComputationInfo.Enabled = True
                    'Sohail (09 Aug 2010) -- Start
                    dsCombo = objMaster.getComboListComputedOn("ComputeOn")
                    'Sohail (09 Aug 2010) -- End

                    If CInt(cboCalcType.SelectedValue) = enCalcType.AsComputedValue Or _
                        CInt(cboCalcType.SelectedValue) = enCalcType.OverTimeHours Or _
                        CInt(cboCalcType.SelectedValue) = enCalcType.ShortHours Then

                        'Sohail (21 Aug 2015) -- Start
                        'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                        'objTranSimpleSlab._Tranheadunkid = objTransactionHead._Tranheadunkid
                        objTranSimpleSlab._Tranheadunkid = objTransactionHead._Tranheadunkid(FinancialYear._Object._DatabaseName)
                        'Sohail (21 Aug 2015) -- End
                        mdtSimpleSlab = objTranSimpleSlab._Datasource
                        Call FillSimpleSlabList()

                        'Sohail (21 Aug 2015) -- Start
                        'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                        'objFormula._Formulatranheadunkid = objTransactionHead._Tranheadunkid
                        objFormula._Formulatranheadunkid = objTransactionHead._Tranheadunkid(FinancialYear._Object._DatabaseName)
                        'Sohail (21 Aug 2015) -- End
                        mdtFormula = objFormula._Datasource
                        'Sohail (03 Sep 2012) -- Start
                        'TRA - ENHANCEMENT
                        'Sohail (21 Aug 2015) -- Start
                        'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                        'objFormulaCurrency._Formulatranheadunkid = objTransactionHead._Tranheadunkid
                        objFormulaCurrency._Formulatranheadunkid = objTransactionHead._Tranheadunkid(FinancialYear._Object._DatabaseName)
                        'Sohail (21 Aug 2015) -- End
                        mdtFormulaCurrency = objFormulaCurrency._Datasource
                        'Sohail (03 Sep 2012) -- End
                        'Sohail (09 Oct 2012) -- Start
                        'TRA - ENHANCEMENT
                        'Sohail (21 Aug 2015) -- Start
                        'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                        'objFormulaLeave._Formulatranheadunkid = objTransactionHead._Tranheadunkid
                        objFormulaLeave._Formulatranheadunkid = objTransactionHead._Tranheadunkid(FinancialYear._Object._DatabaseName)
                        'Sohail (21 Aug 2015) -- End
                        mdtFormulaLeave = objFormulaLeave._Datasource
                        'Sohail (09 Oct 2012) -- End

                        'Sohail (21 Jun 2013) -- Start
                        'TRA - ENHANCEMENT
                        'Sohail (21 Aug 2015) -- Start
                        'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                        'objFormulaMeasurementUnit._Formulatranheadunkid = objTransactionHead._Tranheadunkid
                        objFormulaMeasurementUnit._Formulatranheadunkid = objTransactionHead._Tranheadunkid(FinancialYear._Object._DatabaseName)
                        'Sohail (21 Aug 2015) -- End
                        mdtFormulaMeasurementUnit = objFormulaMeasurementUnit._Datasource
                        'Sohail (21 Aug 2015) -- Start
                        'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                        'objFormulaActivityUnit._Formulatranheadunkid = objTransactionHead._Tranheadunkid
                        objFormulaActivityUnit._Formulatranheadunkid = objTransactionHead._Tranheadunkid(FinancialYear._Object._DatabaseName)
                        'Sohail (21 Aug 2015) -- End
                        mdtFormulaActivityUnit = objFormulaActivityUnit._Datasource
                        'Sohail (21 Aug 2015) -- Start
                        'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                        'objFormulaMeasurementAmount._Formulatranheadunkid = objTransactionHead._Tranheadunkid
                        objFormulaMeasurementAmount._Formulatranheadunkid = objTransactionHead._Tranheadunkid(FinancialYear._Object._DatabaseName)

                        'Sohail (21 Aug 2015) -- End
                        mdtFormulaMeasurementAmount = objFormulaMeasurementAmount._Datasource
                        'Sohail (21 Aug 2015) -- Start
                        'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                        'objFormulaActivityAmount._Formulatranheadunkid = objTransactionHead._Tranheadunkid
                        objFormulaActivityAmount._Formulatranheadunkid = objTransactionHead._Tranheadunkid(FinancialYear._Object._DatabaseName)
                        'Sohail (21 Aug 2015) -- End
                        mdtFormulaActivityAmount = objFormulaActivityAmount._Datasource
                        'Sohail (21 Jun 2013) -- End

                        'Sohail (29 Oct 2013) -- Start
                        'TRA - ENHANCEMENT
                        'Sohail (21 Aug 2015) -- Start
                        'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                        'objFormulaShift._Formulatranheadunkid = objTransactionHead._Tranheadunkid
                        objFormulaShift._Formulatranheadunkid = objTransactionHead._Tranheadunkid(FinancialYear._Object._DatabaseName)
                        'Sohail (21 Aug 2015) -- End
                        mdtFormulaShift = objFormulaShift._Datasource
                        'Sohail (29 Oct 2013) -- End
                        'Sohail (09 Nov 2013) -- Start
                        'TRA - ENHANCEMENT
                        'Sohail (21 Aug 2015) -- Start
                        'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                        'objFormulaCumulative._Formulatranheadunkid = objTransactionHead._Tranheadunkid
                        objFormulaCumulative._Formulatranheadunkid = objTransactionHead._Tranheadunkid(FinancialYear._Object._DatabaseName)
                        'Sohail (21 Aug 2015) -- End
                        mdtFormulaCumulative = objFormulaCumulative._Datasource
                        'Sohail (09 Nov 2013) -- End

                        'Sohail (02 Oct 2014) -- Start
                        'Enhancement - No Of Active Inactive Dependants RELATION WISE.
                        'Sohail (21 Aug 2015) -- Start
                        'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                        'objFormulaCMaster._Formulatranheadunkid = objTransactionHead._Tranheadunkid
                        objFormulaCMaster._Formulatranheadunkid = objTransactionHead._Tranheadunkid(FinancialYear._Object._DatabaseName)
                        'Sohail (21 Aug 2015) -- End
                        mdtFormulaCMaster = objFormulaCMaster._Datasource
                        'Sohail (02 Oct 2014) -- End

                        'Sohail (12 Nov 2014) -- Start
                        'Voltamp Enhancement - Quentity Total and Amount Total for Leave Claim Request in transaction head formula.
                        'Sohail (21 Aug 2015) -- Start
                        'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                        'objFormulaCRExpense._Formulatranheadunkid = objTransactionHead._Tranheadunkid
                        objFormulaCRExpense._Formulatranheadunkid = objTransactionHead._Tranheadunkid(FinancialYear._Object._DatabaseName)
                        'Sohail (21 Aug 2015) -- End
                        mdtFormulaCRExpense = objFormulaCRExpense._Datasource
                        'Sohail (12 Nov 2014) -- End

                        'Sohail (24 Oct 2017) -- Start
                        'CCBRT Enhancement - 70.1 - (Ref. Id - 73) - Add a loan function (like the leave function) in payroll where you can select loan scheme, interest or EMI, total loan amount, etc.
                        objFormulaLoanScheme._Formulatranheadunkid = objTransactionHead._Tranheadunkid(FinancialYear._Object._DatabaseName)
                        mdtFormulaLoanScheme = objFormulaLoanScheme._Datasource
                        'Sohail (24 Oct 2017) -- End

                        pnlSimpleSlab.BringToFront()

                        'Sohail (09 Aug 2010) -- Start
                        If CInt(cboCalcType.SelectedValue) = enCalcType.OverTimeHours Or _
                            CInt(cboCalcType.SelectedValue) = enCalcType.ShortHours Then

                            lblSimleSlabValueBasis.Text = "Value Basis Per Hour"
                            dtTable = New DataView(dsCombo.Tables("ComputeOn"), "Id IN (0, " & enComputeOn.ComputeOnSpecifiedFormula & ")", "", DataViewRowState.CurrentRows).ToTable
                        Else
                            lblSimleSlabValueBasis.Text = "Value Basis"
                            dtTable = New DataView(dsCombo.Tables("ComputeOn"), "", "", DataViewRowState.CurrentRows).ToTable
                        End If

                        'Sohail (09 Aug 2010) -- End
                    ElseIf CInt(cboCalcType.SelectedValue) = enCalcType.AsComputedOnWithINEXCESSOFTaxSlab Then

                        'Sohail (21 Aug 2015) -- Start
                        'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                        'objTranTaxSlab._Tranheadunkid = objTransactionHead._Tranheadunkid
                        objTranTaxSlab._Tranheadunkid = objTransactionHead._Tranheadunkid(FinancialYear._Object._DatabaseName)
                        'Sohail (21 Aug 2015) -- End
                        mdtTaxSlab = objTranTaxSlab._Datasource
                        Call FillTaxSlabList()

                        'Sohail (21 Aug 2015) -- Start
                        'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                        'objFormula._Formulatranheadunkid = objTransactionHead._Tranheadunkid
                        objFormula._Formulatranheadunkid = objTransactionHead._Tranheadunkid(FinancialYear._Object._DatabaseName)
                        'Sohail (21 Aug 2015) -- End
                        mdtFormula = objFormula._Datasource
                        'Sohail (03 Sep 2012) -- Start
                        'TRA - ENHANCEMENT
                        'Sohail (21 Aug 2015) -- Start
                        'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                        'objFormulaCurrency._Formulatranheadunkid = objTransactionHead._Tranheadunkid
                        objFormulaCurrency._Formulatranheadunkid = objTransactionHead._Tranheadunkid(FinancialYear._Object._DatabaseName)
                        'Sohail (21 Aug 2015) -- End
                        mdtFormulaCurrency = objFormulaCurrency._Datasource
                        'Sohail (03 Sep 2012) -- End
                        'Sohail (09 Oct 2012) -- Start
                        'TRA - ENHANCEMENT
                        'Sohail (21 Aug 2015) -- Start
                        'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                        'objFormulaLeave._Formulatranheadunkid = objTransactionHead._Tranheadunkid
                        objFormulaLeave._Formulatranheadunkid = objTransactionHead._Tranheadunkid(FinancialYear._Object._DatabaseName)
                        'Sohail (21 Aug 2015) -- End
                        mdtFormulaLeave = objFormulaLeave._Datasource
                        'Sohail (09 Oct 2012) -- End

                        'Sohail (21 Jun 2013) -- Start
                        'TRA - ENHANCEMENT
                        'Sohail (21 Aug 2015) -- Start
                        'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                        'objFormulaMeasurementUnit._Formulatranheadunkid = objTransactionHead._Tranheadunkid
                        objFormulaMeasurementUnit._Formulatranheadunkid = objTransactionHead._Tranheadunkid(FinancialYear._Object._DatabaseName)
                        'Sohail (21 Aug 2015) -- End
                        mdtFormulaMeasurementUnit = objFormulaMeasurementUnit._Datasource
                        'Sohail (21 Aug 2015) -- Start
                        'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                        'objFormulaActivityUnit._Formulatranheadunkid = objTransactionHead._Tranheadunkid
                        objFormulaActivityUnit._Formulatranheadunkid = objTransactionHead._Tranheadunkid(FinancialYear._Object._DatabaseName)
                        'Sohail (21 Aug 2015) -- End
                        mdtFormulaActivityUnit = objFormulaActivityUnit._Datasource
                        'Sohail (21 Aug 2015) -- Start
                        'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                        'objFormulaMeasurementAmount._Formulatranheadunkid = objTransactionHead._Tranheadunkid
                        objFormulaMeasurementAmount._Formulatranheadunkid = objTransactionHead._Tranheadunkid(FinancialYear._Object._DatabaseName)
                        'Sohail (21 Aug 2015) -- End
                        mdtFormulaMeasurementAmount = objFormulaMeasurementAmount._Datasource
                        'Sohail (21 Aug 2015) -- Start
                        'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                        'objFormulaActivityAmount._Formulatranheadunkid = objTransactionHead._Tranheadunkid
                        objFormulaActivityAmount._Formulatranheadunkid = objTransactionHead._Tranheadunkid(FinancialYear._Object._DatabaseName)
                        'Sohail (21 Aug 2015) -- End
                        mdtFormulaActivityAmount = objFormulaActivityAmount._Datasource
                        'Sohail (21 Jun 2013) -- End

                        'Sohail (29 Oct 2013) -- Start
                        'TRA - ENHANCEMENT
                        'Sohail (21 Aug 2015) -- Start
                        'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                        'objFormulaShift._Formulatranheadunkid = objTransactionHead._Tranheadunkid
                        objFormulaShift._Formulatranheadunkid = objTransactionHead._Tranheadunkid(FinancialYear._Object._DatabaseName)
                        'Sohail (21 Aug 2015) -- End

                        mdtFormulaShift = objFormulaShift._Datasource
                        'Sohail (29 Oct 2013) -- End
                        'Sohail (09 Nov 2013) -- Start
                        'TRA - ENHANCEMENT
                        'Sohail (21 Aug 2015) -- Start
                        'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                        'objFormulaCumulative._Formulatranheadunkid = objTransactionHead._Tranheadunkid
                        objFormulaCumulative._Formulatranheadunkid = objTransactionHead._Tranheadunkid(FinancialYear._Object._DatabaseName)
                        'Sohail (21 Aug 2015) -- End
                        mdtFormulaCumulative = objFormulaCumulative._Datasource
                        'Sohail (09 Nov 2013) -- End

                        'Sohail (02 Oct 2014) -- Start
                        'Enhancement - No Of Active Inactive Dependants RELATION WISE.
                        'Sohail (21 Aug 2015) -- Start
                        'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                        'objFormulaCMaster._Formulatranheadunkid = objTransactionHead._Tranheadunkid
                        objFormulaCMaster._Formulatranheadunkid = objTransactionHead._Tranheadunkid(FinancialYear._Object._DatabaseName)
                        'Sohail (21 Aug 2015) -- End
                        mdtFormulaCMaster = objFormulaCMaster._Datasource
                        'Sohail (02 Oct 2014) -- End

                        'Sohail (12 Nov 2014) -- Start
                        'Voltamp Enhancement - Quentity Total and Amount Total for Leave Claim Request in transaction head formula.
                        'Sohail (21 Aug 2015) -- Start
                        'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                        'objFormulaCRExpense._Formulatranheadunkid = objTransactionHead._Tranheadunkid
                        objFormulaCRExpense._Formulatranheadunkid = objTransactionHead._Tranheadunkid(FinancialYear._Object._DatabaseName)
                        'Sohail (21 Aug 2015) -- End
                        mdtFormulaCRExpense = objFormulaCRExpense._Datasource
                        'Sohail (12 Nov 2014) -- End

                        'Sohail (24 Oct 2017) -- Start
                        'CCBRT Enhancement - 70.1 - (Ref. Id - 73) - Add a loan function (like the leave function) in payroll where you can select loan scheme, interest or EMI, total loan amount, etc.
                        objFormulaLoanScheme._Formulatranheadunkid = objTransactionHead._Tranheadunkid(FinancialYear._Object._DatabaseName)
                        mdtFormulaLoanScheme = objFormulaLoanScheme._Datasource
                        'Sohail (24 Oct 2017) -- End

                        pnlTaxSlab.BringToFront()

                        'Sohail (09 Aug 2010) -- Start
                        dtTable = New DataView(dsCombo.Tables("ComputeOn"), "", "", DataViewRowState.CurrentRows).ToTable
                        'Sohail (09 Aug 2010) -- End
                    End If
                    pnlAddEditDelete.BringToFront()

                    'Sohail (09 Aug 2010) -- Start
                    With cboComputeOn
                        .ValueMember = "Id"
                        .DisplayMember = "Name"
                        .DataSource = dtTable
                        If .Items.Count > 0 Then .SelectedValue = 0
                    End With
                    'Sohail (09 Aug 2010) -- End
                Case enCalcType.FlatRate_Others
                    'Sohail (03 Nov 2010) -- Start
                    cboComputeOn.SelectedValue = 0
                    lvTrnHeadSlabSimple.Items.Clear()
                    lvTrnHeadSlabInExcessof.Items.Clear()
                    'Sohail (03 Nov 2010) -- End
                    'Sohail (23 Dec 2010) -- Start
                    'cboComputeOn.SelectedValue = 0
                    'lvTrnHeadSlabSimple.Items.Clear()
                    'lvTrnHeadSlabInExcessof.Items.Clear()
                    'Sohail (23 Dec 2010) -- End
                    'Sohail (28 Jan 2019) -- Start
                    'NMB Enhancement - 76.1 - Option "Prorate Flat Rate head" on Transaction Head for Recurrent Head.
                    'chkRecurrent.Visible = True 'Sohail (20 Apr 2011)
                    chkRecurrent.Enabled = True 'Sohail (20 Apr 2011)
                    'Sohail (28 Jan 2019) -- End
                    'Sohail (01 Dec 2010) -- Start
                Case Else
                    txtFormula.Text = ""
                    txtFormula.Tag = "" 'Sohail (23 Dec 2010)
                    'Sohail (01 Dec 2010) -- End
                    'Sohail (23 Dec 2010) -- Start
                    cboComputeOn.SelectedValue = 0
                    lvTrnHeadSlabSimple.Items.Clear()
                    lvTrnHeadSlabInExcessof.Items.Clear()
                    'Sohail (23 Dec 2010) -- End
            End Select
            'Sohail (28 Jan 2012) -- Start
            'TRA - ENHANCEMENT
            If CInt(cboTypeOf.SelectedValue) = enTypeOf.Salary Then
                chkRecurrent.Checked = True
                'Sohail (28 Jan 2019) -- Start
                'NMB Enhancement - 76.1 - Option "Prorate Flat Rate head" on Transaction Head for Recurrent Head.
                'chkRecurrent.Visible = False
                chkRecurrent.Enabled = False
                'Sohail (28 Jan 2019) -- End
            End If
            'Sohail (28 Jan 2012) -- End

            'Sohail (14 Jun 2013) -- Start
            'TRA - ENHANCEMENT
            If CInt(cboTrnHeadType.SelectedValue) = enTranHeadType.Informational AndAlso _
                    Not (CInt(cboCalcType.SelectedValue) = enCalcType.NET_PAY _
                        OrElse CInt(cboCalcType.SelectedValue) = enCalcType.TOTAL_EARNING _
                        OrElse CInt(cboCalcType.SelectedValue) = enCalcType.TOTAL_DEDUCTION _
                        OrElse CInt(cboCalcType.SelectedValue) = enCalcType.TAXABLE_EARNING_TOTAL _
                        OrElse CInt(cboCalcType.SelectedValue) = enCalcType.NON_TAXABLE_EARNING_TOTAL _
                        OrElse CInt(cboCalcType.SelectedValue) = enCalcType.NON_CASH_BENEFIT_TOTAL _
                        OrElse CInt(cboCalcType.SelectedValue) = enCalcType.EMPLOYER_CONTRIBUTION_PAYABLE) _
                        Then
                'Hemant (22 Aug 2020) -- [ OrElse CInt(cboCalcType.SelectedValue) = enCalcType.EMPLOYER_CONTRIBUTION_PAYABLE)]

                chkNonCashBenefit.Enabled = True
            Else
                chkNonCashBenefit.Checked = False
                chkNonCashBenefit.Enabled = False
            End If
            'Sohail (14 Jun 2013) -- End

            'Hemant (22 Aug 2020) -- Start
            'NMB Enhancement : Allow to edit payable heads on head master list to map default cost center
            If CInt(cboTrnHeadType.SelectedValue) = enTranHeadType.Informational Then
                Select Case CInt(cboCalcType.SelectedValue)
                    Case enCalcType.NET_PAY, enCalcType.TOTAL_EARNING, enCalcType.TOTAL_DEDUCTION, enCalcType.EMPLOYER_CONTRIBUTION_PAYABLE, enCalcType.TAXABLE_EARNING_TOTAL, enCalcType.NON_TAXABLE_EARNING_TOTAL
                        chkTaxRelief.Enabled = False
                    Case Else
                        chkTaxRelief.Enabled = True
                End Select
            Else
                chkTaxRelief.Checked = False
                chkTaxRelief.Enabled = False
            End If
            'Hemant (22 Aug 2020) -- End


            'Sohail (18 Jun 2013) -- Start
            'TRA - ENHANCEMENT
            If CInt(cboTrnHeadType.SelectedValue) = enTranHeadType.Informational Then
                Select Case CInt(cboCalcType.SelectedValue)
                    Case enCalcType.NET_PAY, enCalcType.TOTAL_EARNING, enCalcType.TOTAL_DEDUCTION, enCalcType.EMPLOYER_CONTRIBUTION_PAYABLE, enCalcType.TAXABLE_EARNING_TOTAL, enCalcType.NON_TAXABLE_EARNING_TOTAL, enCalcType.NON_CASH_BENEFIT_TOTAL
                        chkMonetary.Enabled = False
                    Case Else
                        chkMonetary.Enabled = True
                End Select
            Else
                chkMonetary.Checked = False
                chkMonetary.Enabled = False
            End If
            'Sohail (18 Jun 2013) -- End

            'Hemant (22 Aug 2020) -- Start
            'NMB Enhancement : Allow to edit payable heads on head master list to map default cost center
            If CInt(cboTrnHeadType.SelectedValue) = enTranHeadType.Informational Then
                Select Case CInt(cboCalcType.SelectedValue)
                    Case enCalcType.NET_PAY, enCalcType.TOTAL_EARNING, enCalcType.TOTAL_DEDUCTION, enCalcType.EMPLOYER_CONTRIBUTION_PAYABLE, enCalcType.TAXABLE_EARNING_TOTAL, enCalcType.NON_TAXABLE_EARNING_TOTAL
                        chkIsPerformance.Enabled = False
                    Case Else
                        chkIsPerformance.Enabled = True
                End Select
            Else
                chkIsPerformance.Checked = False
                chkIsPerformance.Enabled = False
            End If
            'Hemant (22 Aug 2020) -- End

            'Sohail (28 Jan 2019) -- Start
            'NMB Enhancement - 76.1 - Option "Prorate Flat Rate head" on Transaction Head for Recurrent Head.
            Call chkRecurrent_CheckedChanged(chkRecurrent, New System.EventArgs)
            'Sohail (28 Jan 2019) -- End

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboCalcType_SelectedValueChanged", mstrModuleName)
        Finally
            objMaster = Nothing
            dsCombo = Nothing
        End Try
    End Sub

    Private Sub cboComputeOn_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboComputeOn.SelectedIndexChanged 'Sohail (23 Dec 2010)
        Try
            If CInt(cboComputeOn.SelectedValue) = enComputeOn.ComputeOnSpecifiedFormula Then
                objbtnAddComputation.Enabled = True
            Else
                objbtnAddComputation.Enabled = False
                txtFormula.Text = ""
                txtFormula.Tag = "" 'Sohail (23 Dec 2010)
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboComputeOn_SelectedValueChanged", mstrModuleName)
        End Try
    End Sub

    'Sohail (21 Nov 2011) -- Start
    Private Sub cboSimpleSlabPeriod_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboSimpleSlabPeriod.SelectedIndexChanged
        Dim objPeriod As New clscommom_period_Tran
        Try
            If CInt(cboSimpleSlabPeriod.SelectedValue) > 0 Then
                'Sohail (21 Aug 2015) -- Start
                'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                'objPeriod._Periodunkid = CInt(cboSimpleSlabPeriod.SelectedValue)
                objPeriod._Periodunkid(FinancialYear._Object._DatabaseName) = CInt(cboSimpleSlabPeriod.SelectedValue)
                'Sohail (21 Aug 2015) -- End
                mdtPeriod_startdate = objPeriod._Start_Date
                mdtPeriod_enddate = objPeriod._End_Date
                mintPeriod_Status = objPeriod._Statusid
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboSimpleSlabPeriod_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub cboTaxSlabPeriod_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboTaxSlabPeriod.SelectedIndexChanged
        Dim objPeriod As New clscommom_period_Tran
        Try
            If CInt(cboTaxSlabPeriod.SelectedValue) > 0 Then
                'Sohail (21 Aug 2015) -- Start
                'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                'objPeriod._Periodunkid = CInt(cboTaxSlabPeriod.SelectedValue)
                objPeriod._Periodunkid(FinancialYear._Object._DatabaseName) = CInt(cboTaxSlabPeriod.SelectedValue)
                'Sohail (21 Aug 2015) -- End
                mdtPeriod_startdate = objPeriod._Start_Date
                mdtPeriod_enddate = objPeriod._End_Date
                mintPeriod_Status = objPeriod._Statusid
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboTaxSlabPeriod_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub
    'Sohail (21 Nov 2011) -- End

    'Sohail (24 Sep 2013) -- Start
    'TRA - ENHANCEMENT
    Private Sub cboSimpleSlabAmountType_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboSimpleSlabAmountType.SelectedIndexChanged
        Try
            If CInt(cboSimpleSlabAmountType.SelectedValue) = enPaymentBy.Fromula Then
                txtSlabFormula.Location = txtSimpleSlabValueBasis.Location
                txtSlabFormula.Visible = True
                txtSlabFormula.BringToFront()
                objbtnAddSlabFormula.Enabled = True
            Else
                txtSlabFormula.Visible = False
                objbtnAddSlabFormula.Enabled = False
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboSimpleSlabAmountType_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub
    'Sohail (24 Sep 2013) -- End
#End Region

#Region " Listview's Events "
    Private Sub lvTrnHeadSlabSimple_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lvTrnHeadSlabSimple.SelectedIndexChanged
        Try
            If lvTrnHeadSlabSimple.SelectedItems.Count > 0 Then
                mintItemIndex = lvTrnHeadSlabSimple.SelectedItems(0).Index

                txtSimpleSlabAmtUpto.Text = Format(CDec(lvTrnHeadSlabSimple.Items(mintItemIndex).SubItems(colhSimpleAmtUpto.Index).Text), GUI.fmtCurrency) 'Sohail (11 May 2011)
                cboSimpleSlabAmountType.SelectedValue = CInt(lvTrnHeadSlabSimple.Items(mintItemIndex).SubItems(colhsimpleSlabType.Index).Tag)
                'Sohail (24 Sep 2013) -- Start
                'TRA - ENHANCEMENT
                'txtSimpleSlabValueBasis.Text = Format(CDec(lvTrnHeadSlabSimple.Items(mintItemIndex).SubItems(colhSimpleValueBasis.Index).Text), GUI.fmtCurrency) 'Sohail (11 May 2011)
                If CInt(cboSimpleSlabAmountType.SelectedValue) = enPaymentBy.Fromula Then
                    txtSimpleSlabValueBasis.Text = ""
                    txtSlabFormula.Text = lvTrnHeadSlabSimple.Items(mintItemIndex).SubItems(colhSimpleValueBasis.Index).Text
                    txtSlabFormula.Tag = lvTrnHeadSlabSimple.Items(mintItemIndex).SubItems(colhSimpleValueBasis.Index).Tag
                    cboSimpleSlabAmountType.Enabled = False
                Else
                    txtSimpleSlabValueBasis.Text = Format(CDec(lvTrnHeadSlabSimple.Items(mintItemIndex).SubItems(colhSimpleValueBasis.Index).Text), GUI.fmtCurrency)
                    txtSlabFormula.Text = ""
                    txtSlabFormula.Tag = ""
                End If
                'Sohail (24 Sep 2013) -- End
                'Sohail (21 Nov 2011) -- Start
                cboSimpleSlabPeriod.SelectedValue = CInt(lvTrnHeadSlabSimple.Items(mintItemIndex).SubItems(colhSimplePeriod.Index).Tag)
                cboSimpleSlabPeriod.Enabled = False
                'Sohail (21 Nov 2011) -- End

                btnAdd.Enabled = False
            Else
                Call ResetSimpleSlab()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lvTrnHeadSlabSimple_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub lvTrnHeadSlabInExcessof_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles lvTrnHeadSlabInExcessof.SelectedIndexChanged
        Try
            If lvTrnHeadSlabInExcessof.SelectedItems.Count > 0 Then
                mintItemIndex = lvTrnHeadSlabInExcessof.SelectedItems(0).Index

                txtTaxSlabAmtUpto.Text = Format(CDec(lvTrnHeadSlabInExcessof.Items(mintItemIndex).SubItems(colhTaxAmountUpto.Index).Text), GUI.fmtCurrency) 'Sohail (11 May 2011)
                txtTaxSlabFixedAmount.Text = Format(CDec(lvTrnHeadSlabInExcessof.Items(mintItemIndex).SubItems(colhTaxFixedAmount.Index).Text), GUI.fmtCurrency) 'Sohail (11 May 2011)
                txtTaxSlabRatePayable.Text = Format(CDec(lvTrnHeadSlabInExcessof.Items(mintItemIndex).SubItems(colhTaxRatePayable.Index).Text), GUI.fmtCurrency) 'Sohail (11 May 2011)
                'Sohail (21 Nov 2011) -- Start
                cboTaxSlabPeriod.SelectedValue = CInt(lvTrnHeadSlabInExcessof.Items(mintItemIndex).SubItems(colhTaxPeriod.Index).Tag)
                cboTaxSlabPeriod.Enabled = False
                'Sohail (21 Nov 2011) -- End

                btnAdd.Enabled = False
            Else
                Call ResetTaxSlab()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lvTrnHeadSlabInExcessof_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub
#End Region

#Region " TextBox's Events "
    Private Sub txtSimpleSlabAmtUpto_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtSimpleSlabAmtUpto.LostFocus
        Try
            If txtSimpleSlabAmtUpto.SelectionLength <= 0 Then Exit Sub
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "txtSimpleSlabAmtUpto_TextChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub txtSimpleSlabValueBasis_Validated(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtSimpleSlabValueBasis.Validated
        Try
            If txtSimpleSlabValueBasis.SelectionLength <= 0 Then Exit Sub
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "txtSimpleSlabValueBasis_Validated", mstrModuleName)
        End Try
    End Sub

    Private Sub txtTaxSlabAmtUpto_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtTaxSlabAmtUpto.TextChanged
        Try
            If txtTaxSlabAmtUpto.SelectionLength <= 0 Then Exit Sub
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "txtTaxSlabAmtUpto_TextChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub txtTaxSlabFixedAmount_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtTaxSlabFixedAmount.TextChanged
        Try
            If txtTaxSlabFixedAmount.SelectionLength <= 0 Then Exit Sub
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "txtTaxSlabFixedAmount_TextChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub txtTaxSlabRatePayable_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtTaxSlabRatePayable.TextChanged
        Try
            If txtTaxSlabRatePayable.SelectionLength <= 0 Then Exit Sub
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "txtTaxSlabRatePayable_TextChanged", mstrModuleName)
        End Try
    End Sub
    'Sohail (11 May 2011) -- End

    'Sohail (24 Sep 2013) -- Start
    'TRA - ENHANCEMENT
    Private Sub txtFormula_MouseMove(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtFormula.MouseMove
        Try
            ToolTip1.SetToolTip(txtFormula, txtFormula.Text)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "txtFormula_MouseMove", mstrModuleName)
        End Try
    End Sub
    'Sohail (24 Sep 2013) -- End

#End Region

#Region " Other Control's Events "
    Private Sub objbtnAddComputation_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnAddComputation.Click

        'Sohail (12 Mar 2012) -- Start
        'TRA - ENHANCEMENT
        If CInt(cboTrnHeadType.SelectedValue) <= 0 Then
            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Please select Transaction Head Type."), enMsgBoxStyle.Information)
            cboTrnHeadType.Focus()
            Exit Sub
        End If
        'Sohail (12 Mar 2012) -- End

        Dim objFrm As New frmAdditionalComputation_AddEdit
        Dim moTempAction As enAction
        Try

            'Anjan (02 Sep 2011)-Start
            'Issue : Including Language Settings.
            If User._Object._Isrighttoleft = True Then
                objFrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objFrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objFrm)
            End If
            'Anjan (02 Sep 2011)-End 

            objFrm._Formula = txtFormula.Text
            If txtFormula.Tag Is Nothing Then
                objFrm._FormulaID = ""
            Else
                objFrm._FormulaID = txtFormula.Tag.ToString
            End If
            'Sohail (12 Apr 2013) -- Start
            'TRA - ENHANCEMENT
            Dim dt As DataTable
            dt = mdtFormulaSlab.Copy
            objFrm._DatasourcePeriodWiseSlab = dt
            'Sohail (12 Apr 2013) -- End
            If txtFormula.Text = "" Then
                moTempAction = enAction.ADD_ONE
                objFrm._Datasource = objTransactionHead._FormulaDatasource
                objFrm._DatasourceCurrency = objTransactionHead._FormulaCurrencyDatasource 'Sohail (03 Sep 2012)
                objFrm._DatasourcePaidUnpaidLeave = objTransactionHead._FormulaLeaveDatasource 'Sohail (09 Oct 2012)
                'Sohail (21 Jun 2013) -- Start
                'TRA - ENHANCEMENT
                objFrm._DatasourceMeasurementUnit = objTransactionHead._FormulaMeasurementUnitDatasource
                objFrm._DatasourceActivityUnit = objTransactionHead._FormulaActivityUnitDatasource
                objFrm._DatasourceMeasurementAmount = objTransactionHead._FormulaMeasurementAmountDatasource
                objFrm._DatasourceActivityAmount = objTransactionHead._FormulaActivityAmountDatasource
                'Sohail (21 Jun 2013) -- End
                objFrm._DatasourceShift = objTransactionHead._FormulaShiftDatasource 'Sohail (29 Oct 2013)
                objFrm._DatasourceCumulative = objTransactionHead._FormulaCumulativeDatasource 'Sohail (09 Nov 2013)
                objFrm._DatasourceCommonMaster = objTransactionHead._FormulaCMasterDatasource 'Sohail (02 Oct 2014)
                objFrm._DatasourceCRExpense = objTransactionHead._FormulaCRExpenseDatasource 'Sohail (12 Nov 2014)
                objFrm._DatasourceLoanScheme = objTransactionHead._FormulaLoanSchemeDatasource 'Sohail (24 Oct 2017)
            Else
                moTempAction = enAction.EDIT_ONE
                objFrm._Datasource = objTransactionHead._FormulaDatasource
                objFrm._DatasourceCurrency = objTransactionHead._FormulaCurrencyDatasource 'Sohail (03 Sep 2012)
                objFrm._DatasourcePaidUnpaidLeave = objTransactionHead._FormulaLeaveDatasource 'Sohail (09 Oct 2012)
                'Sohail (21 Jun 2013) -- Start
                'TRA - ENHANCEMENT
                objFrm._DatasourceMeasurementUnit = objTransactionHead._FormulaMeasurementUnitDatasource
                objFrm._DatasourceActivityUnit = objTransactionHead._FormulaActivityUnitDatasource
                objFrm._DatasourceMeasurementAmount = objTransactionHead._FormulaMeasurementAmountDatasource
                objFrm._DatasourceActivityAmount = objTransactionHead._FormulaActivityAmountDatasource
                'Sohail (21 Jun 2013) -- End
                objFrm._DatasourceShift = objTransactionHead._FormulaShiftDatasource 'Sohail (29 Oct 2013)
                objFrm._DatasourceCumulative = objTransactionHead._FormulaCumulativeDatasource 'Sohail (09 Nov 2013)
                objFrm._DatasourceCommonMaster = objTransactionHead._FormulaCMasterDatasource 'Sohail (02 Oct 2014)
                objFrm._DatasourceCRExpense = objTransactionHead._FormulaCRExpenseDatasource 'Sohail (12 Nov 2014)
                objFrm._DatasourceLoanScheme = objTransactionHead._FormulaLoanSchemeDatasource 'Sohail (24 Oct 2017)
            End If

            'Sohail (23 Dec 2010) -- Start
            If objFrm.displayDialog(mintTranHeadUnkid, moTempAction, CInt(cboTrnHeadType.SelectedValue), CInt(cboTypeOf.SelectedValue), CInt(cboCalcType.SelectedValue)) Then
                'If objFrm.displayDialog(mintTranHeadUnkid, moTempAction) Then
                'Sohail (23 Dec 2010) -- End
                txtFormula.Text = objFrm._Formula
                txtFormula.Tag = objFrm._FormulaID
                objTransactionHead._FormulaDatasource = objFrm._Datasource
                objTransactionHead._FormulaCurrencyDatasource = objFrm._DatasourceCurrency 'Sohail (03 Sep 2012)
                objTransactionHead._FormulaLeaveDatasource = objFrm._DatasourcePaidUnpaidLeave 'Sohail (09 Oct 2012)
                'Sohail (21 Jun 2013) -- Start
                'TRA - ENHANCEMENT
                objTransactionHead._FormulaMeasurementUnitDatasource = objFrm._DatasourceMeasurementUnit
                objTransactionHead._FormulaActivityUnitDatasource = objFrm._DatasourceActivityUnit
                objTransactionHead._FormulaMeasurementAmountDatasource = objFrm._DatasourceMeasurementAmount
                objTransactionHead._FormulaActivityAmountDatasource = objFrm._DatasourceActivityAmount
                'Sohail (21 Jun 2013) -- End
                objTransactionHead._FormulaShiftDatasource = objFrm._DatasourceShift 'Sohail (29 Oct 2013)
                objTransactionHead._FormulaCumulativeDatasource = objFrm._DatasourceCumulative 'Sohail (09 Nov 2013)
                objTransactionHead._FormulaCMasterDatasource = objFrm._DatasourceCommonMaster 'Sohail (02 Oct 2014)
                objTransactionHead._FormulaCRExpenseDatasource = objFrm._DatasourceCRExpense 'Sohail (12 Nov 2014)
                objTransactionHead._FormulaLoanSchemeDatasource = objFrm._DatasourceLoanScheme 'Sohail (24 Oct 2017)
                'Sohail (12 Apr 2013) -- Start
                'TRA - ENHANCEMENT
                mdtFormulaSlab = objFrm._DatasourcePeriodWiseSlab
                objTransactionHead._FormulaPeriodWiseSlabDatasource = objFrm._DatasourcePeriodWiseSlab
                'Sohail (12 Apr 2013) -- End
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnAddComputation_Click", mstrModuleName)
        Finally
            objFrm = Nothing
        End Try
    End Sub

    Private Sub objbtnOtherLanguage_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnOtherLanguage.Click
        Dim objFrm As New NameLanguagePopup_Form
        Try
            'Anjan (02 Sep 2011)-Start
            'Issue : Including Language Settings.
            If User._Object._Isrighttoleft = True Then
                objFrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objFrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objFrm)
            End If
            'Anjan (02 Sep 2011)-End 
            Call objFrm.displayDialog(txtHeadName.Text, objTransactionHead._Trnheadname1, objTransactionHead._Trnheadname2)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnOtherLanguage_Click", mstrModuleName)
        End Try
    End Sub

    'Sohail (21 Nov 2011) -- Start
    Private Sub lnkCopySlab_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lnkCopySlab.LinkClicked
        Dim drRow() As DataRow
        Dim drSlab As DataRow
        Dim strPrevEndDate As String = ""
        Try
            If CInt(cboCalcType.SelectedValue) = enCalcType.AsComputedValue OrElse CInt(cboCalcType.SelectedValue) = enCalcType.OverTimeHours OrElse CInt(cboCalcType.SelectedValue) = enCalcType.ShortHours Then
                If CInt(cboSimpleSlabPeriod.SelectedValue) <= 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 6, "Please select Period."), enMsgBoxStyle.Information)
                    cboSimpleSlabPeriod.Focus()
                    Exit Sub
                ElseIf mintPeriod_Status = enStatusType.Close Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 15, "Sorry! This Period is closed."), enMsgBoxStyle.Information)
                    cboSimpleSlabPeriod.Focus()
                    Exit Sub
                End If
                drRow = mdtSimpleSlab.Select("end_date > '" & eZeeDate.convertDate(mdtPeriod_startdate) & "' AND AUD <> 'D'")
                If drRow.Length > 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 16, "Sorry! Slab for this Period is already defined."), enMsgBoxStyle.Information)
                    cboSimpleSlabPeriod.Focus()
                    Exit Sub
                End If

                drRow = mdtSimpleSlab.Select("AUD <> 'D'", "end_date DESC, amountupto ASC")
                If drRow.Length > 0 Then
                    For Each dRow As DataRow In drRow
                        If strPrevEndDate <> "" AndAlso strPrevEndDate <> dRow.Item("end_date").ToString Then
                            Exit For
                        End If

                        drSlab = mdtSimpleSlab.NewRow()
                        With drSlab
                            'Sohail (02 Nov 2013) -- Start
                            'TRA - ENHANCEMENT
                            '.Item("tranheadslabtranunkid") = -1
                            .Item("tranheadslabtranunkid") = mdtSimpleSlab.Rows.Count + 1 - ((mdtSimpleSlab.Rows.Count + 1) * 2)
                            'Sohail (02 Nov 2013) -- End
                            .Item("tranheadunkid") = mintTranHeadUnkid
                            .Item("amountupto") = CDec(dRow.Item("amountupto").ToString)
                            .Item("slabtype") = CInt(dRow.Item("slabtype").ToString)
                            .Item("valuebasis") = CDec(dRow.Item("valuebasis").ToString)
                            .Item("userunkid") = User._Object._Userunkid
                            .Item("isvoid") = False
                            .Item("voiduserunkid") = -1
                            .Item("voiddatetime") = DBNull.Value
                            .Item("voidreason") = ""
                            .Item("GUID") = Guid.NewGuid.ToString
                            .Item("AUD") = "A"
                            .Item("periodunkid") = CInt(cboSimpleSlabPeriod.SelectedValue)
                            .Item("period_name") = cboSimpleSlabPeriod.Text
                            .Item("end_date") = eZeeDate.convertDate(mdtPeriod_enddate)
                            'Sohail (02 Nov 2013) -- Start
                            'TRA - ENHANCEMENT
                            .Item("formula") = dRow.Item("formula").ToString
                            .Item("formulaid") = dRow.Item("formulaid").ToString
                            'Sohail (02 Nov 2013) -- End
                        End With
                        mdtSimpleSlab.Rows.Add(drSlab)

                        strPrevEndDate = dRow.Item("end_date").ToString

                        'Sohail (02 Nov 2013) -- Start
                        'TRA - ENHANCEMENT
                        Dim dr_Row() As DataRow = Nothing
                        Dim d_row As DataRow = Nothing
                        If dRow.Item("formula").ToString.Trim <> "" AndAlso dRow.Item("formula").ToString.Trim <> "" Then
                            If mdtFormula IsNot Nothing Then
                                dr_Row = mdtFormula.Select("periodunkid = " & CInt(dRow.Item("periodunkid")) & " AND tranheadslabtranunkid <> 0 AND tranheadslabtranunkid = " & CInt(dRow.Item("tranheadslabtranunkid")) & " ")
                                For Each ds_row As DataRow In dr_Row
                                    d_row = mdtFormula.NewRow
                                    With d_row
                                        .Item("formulatranheadunkid") = mintTranHeadUnkid
                                        .Item("tranheadunkid") = CInt(ds_row.Item("tranheadunkid"))
                                        .Item("isvoid") = False
                                        .Item("voiduserunkid") = -1
                                        .Item("voiddatetime") = DBNull.Value
                                        .Item("voidreason") = String.Empty
                                        .Item("defaultvalue_id") = CInt(ds_row.Item("defaultvalue_id"))
                                        .Item("periodunkid") = CInt(cboSimpleSlabPeriod.SelectedValue)
                                        .Item("tranheadslabtranunkid") = CInt(drSlab.Item("tranheadslabtranunkid"))
                                        .Item("AUD") = "A"
                                    End With
                                    mdtFormula.Rows.Add(d_row)
                                Next
                            End If
                        End If

                        If mdtFormulaCurrency IsNot Nothing Then
                            dr_Row = mdtFormulaCurrency.Select("periodunkid = " & CInt(dRow.Item("periodunkid")) & " AND tranheadslabtranunkid <> 0 AND tranheadslabtranunkid = " & CInt(dRow.Item("tranheadslabtranunkid")) & " ")
                            For Each ds_row As DataRow In dr_Row
                                d_row = mdtFormulaCurrency.NewRow
                                With d_row
                                    .Item("formulatranheadunkid") = mintTranHeadUnkid
                                    .Item("exchangerateunkid") = CInt(ds_row.Item("exchangerateunkid"))
                                    .Item("isvoid") = False
                                    .Item("voiduserunkid") = -1
                                    .Item("voiddatetime") = DBNull.Value
                                    .Item("voidreason") = String.Empty
                                    .Item("defaultvalue_id") = CInt(ds_row.Item("defaultvalue_id"))
                                    .Item("periodunkid") = CInt(cboSimpleSlabPeriod.SelectedValue)
                                    .Item("tranheadslabtranunkid") = CInt(drSlab.Item("tranheadslabtranunkid"))
                                    .Item("AUD") = "A"
                                End With
                                mdtFormulaCurrency.Rows.Add(d_row)
                            Next
                        End If

                        If mdtFormulaLeave IsNot Nothing Then
                            dr_Row = mdtFormulaLeave.Select("periodunkid = " & CInt(dRow.Item("periodunkid")) & " AND tranheadslabtranunkid <> 0 AND tranheadslabtranunkid = " & CInt(dRow.Item("tranheadslabtranunkid")) & " ")
                            For Each ds_row As DataRow In dr_Row
                                d_row = mdtFormulaLeave.NewRow
                                With d_row
                                    .Item("formulatranheadunkid") = mintTranHeadUnkid
                                    .Item("leavetypeunkid") = CInt(ds_row.Item("leavetypeunkid"))
                                    .Item("isvoid") = False
                                    .Item("voiduserunkid") = -1
                                    .Item("voiddatetime") = DBNull.Value
                                    .Item("voidreason") = String.Empty
                                    .Item("defaultvalue_id") = CInt(ds_row.Item("defaultvalue_id"))
                                    .Item("periodunkid") = CInt(cboSimpleSlabPeriod.SelectedValue)
                                    .Item("tranheadslabtranunkid") = CInt(drSlab.Item("tranheadslabtranunkid"))
                                    .Item("AUD") = "A"
                                End With
                                mdtFormulaLeave.Rows.Add(d_row)
                            Next
                        End If

                        If mdtFormulaMeasurementUnit IsNot Nothing Then
                            dr_Row = mdtFormulaMeasurementUnit.Select("periodunkid = " & CInt(dRow.Item("periodunkid")) & " AND tranheadslabtranunkid <> 0 AND tranheadslabtranunkid = " & CInt(dRow.Item("tranheadslabtranunkid")) & " ")
                            For Each ds_row As DataRow In dr_Row
                                d_row = mdtFormulaMeasurementUnit.NewRow
                                With d_row
                                    .Item("formulatranheadunkid") = mintTranHeadUnkid
                                    .Item("measureunkid") = CInt(ds_row.Item("measureunkid"))
                                    .Item("isvoid") = False
                                    .Item("voiduserunkid") = -1
                                    .Item("voiddatetime") = DBNull.Value
                                    .Item("voidreason") = String.Empty
                                    .Item("defaultvalue_id") = CInt(ds_row.Item("defaultvalue_id"))
                                    .Item("periodunkid") = CInt(cboSimpleSlabPeriod.SelectedValue)
                                    .Item("tranheadslabtranunkid") = CInt(drSlab.Item("tranheadslabtranunkid"))
                                    .Item("AUD") = "A"
                                End With
                                mdtFormulaMeasurementUnit.Rows.Add(d_row)
                            Next
                        End If

                        If mdtFormulaActivityUnit IsNot Nothing Then
                            dr_Row = mdtFormulaActivityUnit.Select("periodunkid = " & CInt(dRow.Item("periodunkid")) & " AND tranheadslabtranunkid <> 0 AND tranheadslabtranunkid = " & CInt(dRow.Item("tranheadslabtranunkid")) & " ")
                            For Each ds_row As DataRow In dr_Row
                                d_row = mdtFormulaActivityUnit.NewRow
                                With d_row
                                    .Item("formulatranheadunkid") = mintTranHeadUnkid
                                    .Item("activityunkid") = CInt(ds_row.Item("activityunkid"))
                                    .Item("isvoid") = False
                                    .Item("voiduserunkid") = -1
                                    .Item("voiddatetime") = DBNull.Value
                                    .Item("voidreason") = String.Empty
                                    .Item("defaultvalue_id") = CInt(ds_row.Item("defaultvalue_id"))
                                    .Item("periodunkid") = CInt(cboSimpleSlabPeriod.SelectedValue)
                                    .Item("tranheadslabtranunkid") = CInt(drSlab.Item("tranheadslabtranunkid"))
                                    .Item("AUD") = "A"
                                End With
                                mdtFormulaActivityUnit.Rows.Add(d_row)
                            Next
                        End If

                        If mdtFormulaMeasurementAmount IsNot Nothing Then
                            dr_Row = mdtFormulaMeasurementAmount.Select("periodunkid = " & CInt(dRow.Item("periodunkid")) & " AND tranheadslabtranunkid <> 0 AND tranheadslabtranunkid = " & CInt(dRow.Item("tranheadslabtranunkid")) & " ")
                            For Each ds_row As DataRow In dr_Row
                                d_row = mdtFormulaMeasurementAmount.NewRow
                                With d_row
                                    .Item("formulatranheadunkid") = mintTranHeadUnkid
                                    .Item("measureunkid") = CInt(ds_row.Item("measureunkid"))
                                    .Item("isvoid") = False
                                    .Item("voiduserunkid") = -1
                                    .Item("voiddatetime") = DBNull.Value
                                    .Item("voidreason") = String.Empty
                                    .Item("defaultvalue_id") = CInt(ds_row.Item("defaultvalue_id"))
                                    .Item("periodunkid") = CInt(cboSimpleSlabPeriod.SelectedValue)
                                    .Item("tranheadslabtranunkid") = CInt(drSlab.Item("tranheadslabtranunkid"))
                                    .Item("AUD") = "A"
                                End With
                                mdtFormulaMeasurementAmount.Rows.Add(d_row)
                            Next
                        End If

                        If mdtFormulaActivityAmount IsNot Nothing Then
                            dr_Row = mdtFormulaActivityAmount.Select("periodunkid = " & CInt(dRow.Item("periodunkid")) & " AND tranheadslabtranunkid <> 0 AND tranheadslabtranunkid = " & CInt(dRow.Item("tranheadslabtranunkid")) & " ")
                            For Each ds_row As DataRow In dr_Row
                                d_row = mdtFormulaActivityAmount.NewRow
                                With d_row
                                    .Item("formulatranheadunkid") = mintTranHeadUnkid
                                    .Item("activityunkid") = CInt(ds_row.Item("activityunkid"))
                                    .Item("isvoid") = False
                                    .Item("voiduserunkid") = -1
                                    .Item("voiddatetime") = DBNull.Value
                                    .Item("voidreason") = String.Empty
                                    .Item("defaultvalue_id") = CInt(ds_row.Item("defaultvalue_id"))
                                    .Item("periodunkid") = CInt(cboSimpleSlabPeriod.SelectedValue)
                                    .Item("tranheadslabtranunkid") = CInt(drSlab.Item("tranheadslabtranunkid"))
                                    .Item("AUD") = "A"
                                End With
                                mdtFormulaActivityAmount.Rows.Add(d_row)
                            Next
                        End If

                        If mdtFormulaShift IsNot Nothing Then
                            dr_Row = mdtFormulaShift.Select("periodunkid = " & CInt(dRow.Item("periodunkid")) & " AND tranheadslabtranunkid <> 0 AND tranheadslabtranunkid = " & CInt(dRow.Item("tranheadslabtranunkid")) & " ")
                            For Each ds_row As DataRow In dr_Row
                                d_row = mdtFormulaShift.NewRow
                                With d_row
                                    .Item("formulatranheadunkid") = mintTranHeadUnkid
                                    .Item("shiftunkid") = CInt(ds_row.Item("shiftunkid"))
                                    .Item("isvoid") = False
                                    .Item("voiduserunkid") = -1
                                    .Item("voiddatetime") = DBNull.Value
                                    .Item("voidreason") = String.Empty
                                    .Item("defaultvalue_id") = CInt(ds_row.Item("defaultvalue_id"))
                                    .Item("periodunkid") = CInt(cboSimpleSlabPeriod.SelectedValue)
                                    .Item("tranheadslabtranunkid") = CInt(drSlab.Item("tranheadslabtranunkid"))
                                    .Item("AUD") = "A"
                                End With
                                mdtFormulaShift.Rows.Add(d_row)
                            Next
                        End If
                        'Sohail (02 Nov 2013) -- End

                        'Sohail (09 Nov 2013) -- Start
                        'TRA - ENHANCEMENT
                        If mdtFormulaCumulative IsNot Nothing Then
                            dr_Row = mdtFormulaCumulative.Select("periodunkid = " & CInt(dRow.Item("periodunkid")) & " AND tranheadslabtranunkid <> 0 AND tranheadslabtranunkid = " & CInt(dRow.Item("tranheadslabtranunkid")) & " ")
                            For Each ds_row As DataRow In dr_Row
                                d_row = mdtFormulaCumulative.NewRow
                                With d_row
                                    .Item("formulatranheadunkid") = mintTranHeadUnkid
                                    .Item("tranheadunkid") = CInt(ds_row.Item("tranheadunkid"))
                                    .Item("isvoid") = False
                                    .Item("voiduserunkid") = -1
                                    .Item("voiddatetime") = DBNull.Value
                                    .Item("voidreason") = String.Empty
                                    .Item("defaultvalue_id") = CInt(ds_row.Item("defaultvalue_id"))
                                    .Item("periodunkid") = CInt(cboSimpleSlabPeriod.SelectedValue)
                                    .Item("tranheadslabtranunkid") = CInt(drSlab.Item("tranheadslabtranunkid"))
                                    .Item("AUD") = "A"
                                End With
                                mdtFormulaCumulative.Rows.Add(d_row)
                            Next
                        End If
                        'Sohail (09 Nov 2013) -- End

                        'Sohail (02 Oct 2014) -- Start
                        'Enhancement - No Of Active Inactive Dependants RELATION WISE.
                        If mdtFormulaCMaster IsNot Nothing Then
                            dr_Row = mdtFormulaCMaster.Select("periodunkid = " & CInt(dRow.Item("periodunkid")) & " AND tranheadslabtranunkid <> 0 AND tranheadslabtranunkid = " & CInt(dRow.Item("tranheadslabtranunkid")) & " ")
                            For Each ds_row As DataRow In dr_Row
                                d_row = mdtFormulaCMaster.NewRow
                                With d_row
                                    .Item("formulatranheadunkid") = mintTranHeadUnkid
                                    .Item("masterunkid") = CInt(ds_row.Item("masterunkid"))
                                    .Item("isvoid") = False
                                    .Item("voiduserunkid") = -1
                                    .Item("voiddatetime") = DBNull.Value
                                    .Item("voidreason") = String.Empty
                                    .Item("defaultvalue_id") = CInt(ds_row.Item("defaultvalue_id"))
                                    .Item("periodunkid") = CInt(cboSimpleSlabPeriod.SelectedValue)
                                    .Item("tranheadslabtranunkid") = CInt(drSlab.Item("tranheadslabtranunkid"))
                                    .Item("AUD") = "A"
                                End With
                                mdtFormulaCMaster.Rows.Add(d_row)
                            Next
                        End If
                        'Sohail (02 Oct 2014) -- End

                        'Sohail (12 Nov 2014) -- Start
                        'Voltamp Enhancement - Quentity Total and Amount Total for Leave Claim Request in transaction head formula.
                        If mdtFormulaCRExpense IsNot Nothing Then
                            dr_Row = mdtFormulaCRExpense.Select("periodunkid = " & CInt(dRow.Item("periodunkid")) & " AND tranheadslabtranunkid <> 0 AND tranheadslabtranunkid = " & CInt(dRow.Item("tranheadslabtranunkid")) & " ")
                            For Each ds_row As DataRow In dr_Row
                                d_row = mdtFormulaCRExpense.NewRow
                                With d_row
                                    .Item("formulatranheadunkid") = mintTranHeadUnkid
                                    .Item("expenseunkid") = CInt(ds_row.Item("expenseunkid"))
                                    .Item("isvoid") = False
                                    .Item("voiduserunkid") = -1
                                    .Item("voiddatetime") = DBNull.Value
                                    .Item("voidreason") = String.Empty
                                    .Item("defaultvalue_id") = CInt(ds_row.Item("defaultvalue_id"))
                                    .Item("periodunkid") = CInt(cboSimpleSlabPeriod.SelectedValue)
                                    .Item("tranheadslabtranunkid") = CInt(drSlab.Item("tranheadslabtranunkid"))
                                    .Item("AUD") = "A"
                                End With
                                mdtFormulaCRExpense.Rows.Add(d_row)
                            Next
                        End If
                        'Sohail (12 Nov 2014) -- End

                        'Sohail (24 Oct 2017) -- Start
                        'CCBRT Enhancement - 70.1 - (Ref. Id - 73) - Add a loan function (like the leave function) in payroll where you can select loan scheme, interest or EMI, total loan amount, etc.
                        If mdtFormulaLoanScheme IsNot Nothing Then
                            dr_Row = mdtFormulaLoanScheme.Select("periodunkid = " & CInt(dRow.Item("periodunkid")) & " AND tranheadslabtranunkid <> 0 AND tranheadslabtranunkid = " & CInt(dRow.Item("tranheadslabtranunkid")) & " ")
                            For Each ds_row As DataRow In dr_Row
                                d_row = mdtFormulaLoanScheme.NewRow
                                With d_row
                                    .Item("formulatranheadunkid") = mintTranHeadUnkid
                                    .Item("LoanSchemeunkid") = CInt(ds_row.Item("LoanSchemeunkid"))
                                    .Item("isvoid") = False
                                    .Item("voiduserunkid") = -1
                                    .Item("voiddatetime") = DBNull.Value
                                    .Item("voidreason") = String.Empty
                                    .Item("defaultvalue_id") = CInt(ds_row.Item("defaultvalue_id"))
                                    .Item("periodunkid") = CInt(cboSimpleSlabPeriod.SelectedValue)
                                    .Item("tranheadslabtranunkid") = CInt(drSlab.Item("tranheadslabtranunkid"))
                                    .Item("AUD") = "A"
                                End With
                                mdtFormulaLoanScheme.Rows.Add(d_row)
                            Next
                        End If
                        'Sohail (24 Oct 2017) -- End

                    Next
                    Call FillSimpleSlabList()
                    Call ResetSimpleSlab()
                End If

            ElseIf CInt(cboCalcType.SelectedValue) = enCalcType.AsComputedOnWithINEXCESSOFTaxSlab Then
                If CInt(cboTaxSlabPeriod.SelectedValue) <= 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 6, "Please select Period."), enMsgBoxStyle.Information)
                    cboTaxSlabPeriod.Focus()
                    Exit Sub
                ElseIf mintPeriod_Status = enStatusType.Close Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 15, "Sorry! This Period is closed."), enMsgBoxStyle.Information)
                    cboTaxSlabPeriod.Focus()
                    Exit Sub
                End If
                drRow = mdtTaxSlab.Select("end_date > '" & eZeeDate.convertDate(mdtPeriod_startdate) & "' AND AUD <> 'D'")
                If drRow.Length > 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 16, "Sorry! Slab for this Period is already defined."), enMsgBoxStyle.Information)
                    cboTaxSlabPeriod.Focus()
                    Exit Sub
                End If

                drRow = mdtTaxSlab.Select("AUD <> 'D'", "end_date DESC, amountupto ASC")
                If drRow.Length > 0 Then
                    For Each dRow As DataRow In drRow
                        If strPrevEndDate <> "" AndAlso strPrevEndDate <> dRow.Item("end_date").ToString Then
                            Exit For
                        End If

                        drSlab = mdtTaxSlab.NewRow()
                        With drSlab
                            .Item("tranheadtaxslabtranunkid") = -1
                            .Item("tranheadunkid") = mintTranHeadUnkid
                            .Item("amountupto") = CDec(dRow.Item("amountupto").ToString)
                            .Item("fixedamount") = CDec(dRow.Item("fixedamount").ToString)
                            .Item("ratepayable") = CDec(dRow.Item("ratepayable").ToString)
                            .Item("inexcessof") = CDec(dRow.Item("inexcessof").ToString)
                            .Item("userunkid") = User._Object._Userunkid
                            .Item("isvoid") = False
                            .Item("voiduserunkid") = -1
                            .Item("voiddatetime") = DBNull.Value
                            .Item("voidreason") = ""
                            .Item("GUID") = Guid.NewGuid.ToString
                            .Item("AUD") = "A"
                            .Item("periodunkid") = CInt(cboTaxSlabPeriod.SelectedValue)
                            .Item("period_name") = cboTaxSlabPeriod.Text
                            .Item("end_date") = eZeeDate.convertDate(mdtPeriod_enddate)
                        End With
                        mdtTaxSlab.Rows.Add(drSlab)

                        strPrevEndDate = dRow.Item("end_date").ToString
                    Next
                    Call FillTaxSlabList()
                    Call ResetTaxSlab()
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lnkCopySlab_LinkClicked", mstrModuleName)
        End Try
    End Sub
    'Sohail (21 Nov 2011) -- End

    'Sohail (24 Sep 2013) -- Start
    'TRA - ENHANCEMENT
    Private Sub objbtnAddSlabFormula_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnAddSlabFormula.Click
        Dim moTempAction As enAction
        Dim decPrevAmt As Decimal = 0
        Dim decAmtUpto As Decimal = 0
        Dim decNextAmt As Decimal = 0
        Dim strLastPeriodEndDate As String
        Dim intTranheadslabtranunkid As Integer = 0

        Try
            If IsValidSimpleSlab(objbtnAddSlabFormula.Name) = False Then Exit Sub

            If lvTrnHeadSlabSimple.Items.Count > 0 Then
                strLastPeriodEndDate = lvTrnHeadSlabSimple.Items(0).SubItems(colhSimpleEndDate.Index).Text

                If txtSlabFormula.Text = "" Then
                    If mdtPeriod_enddate < eZeeDate.convertDate(strLastPeriodEndDate) Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 16, "Sorry! Slab for this Period is already defined."), enMsgBoxStyle.Information)
                        cboSimpleSlabPeriod.Focus()
                        Exit Sub
                    End If

                    Dim dRow() As DataRow = mdtSimpleSlab.Select("end_date = '" & eZeeDate.convertDate(mdtPeriod_enddate) & "' AND AUD <> 'D'", "amountupto DESC")
                    If dRow.Length > 0 Then
                        Decimal.TryParse(dRow(0).Item("amountupto").ToString, decPrevAmt)
                    End If

                    If decPrevAmt >= CDec(txtSimpleSlabAmtUpto.Text) Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 14, "Sorry, Amount should be greater than Previous Amount."), enMsgBoxStyle.Information)
                        txtSimpleSlabAmtUpto.Focus()
                        Exit Sub
                    End If
                Else
                    mintItemIndex = lvTrnHeadSlabSimple.SelectedItems(0).Index
                    decAmtUpto = CDec(txtSimpleSlabAmtUpto.Decimal)

                    If mintItemIndex > 0 AndAlso CInt(cboSimpleSlabPeriod.SelectedValue) = CInt(lvTrnHeadSlabSimple.Items(mintItemIndex - 1).SubItems(colhSimplePeriod.Index).Tag) Then
                        decPrevAmt = CDec(lvTrnHeadSlabSimple.Items(mintItemIndex - 1).SubItems(colhSimpleAmtUpto.Index).Text)
                        If decPrevAmt >= decAmtUpto Then
                            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 14, "Sorry, Amount should be greater than Previous Amount."), enMsgBoxStyle.Information)
                            txtSimpleSlabAmtUpto.Focus()
                            Exit Sub
                        End If
                    End If

                    If mintItemIndex < lvTrnHeadSlabSimple.Items.Count - 1 AndAlso CInt(lvTrnHeadSlabSimple.Items(mintItemIndex + 1).SubItems(colhSimplePeriod.Index).Tag) = CInt(cboSimpleSlabPeriod.SelectedValue) Then
                        decNextAmt = CDec(lvTrnHeadSlabSimple.Items(mintItemIndex + 1).SubItems(colhSimpleAmtUpto.Index).Text)
                        If decAmtUpto >= decNextAmt Then
                            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 17, "Sorry, Amount should be less than next Amount."), enMsgBoxStyle.Information)
                            txtSimpleSlabAmtUpto.Focus()
                            Exit Sub
                        End If
                    End If
                End If

            End If


            Dim objFrm As New frmAdditionalComputation_AddEdit

            If User._Object._Isrighttoleft = True Then
                objFrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objFrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objFrm)
            End If

            objFrm._Formula = txtSlabFormula.Text
            If txtSlabFormula.Tag Is Nothing Then
                objFrm._FormulaID = ""
            Else
                objFrm._FormulaID = txtSlabFormula.Tag.ToString
            End If


            If txtSlabFormula.Text = "" Then
                moTempAction = enAction.ADD_ONE
                objFrm._Datasource = objTransactionHead._FormulaDatasource
                objFrm._DatasourceCurrency = objTransactionHead._FormulaCurrencyDatasource
                objFrm._DatasourcePaidUnpaidLeave = objTransactionHead._FormulaLeaveDatasource
                objFrm._DatasourceMeasurementUnit = objTransactionHead._FormulaMeasurementUnitDatasource
                objFrm._DatasourceActivityUnit = objTransactionHead._FormulaActivityUnitDatasource
                objFrm._DatasourceMeasurementAmount = objTransactionHead._FormulaMeasurementAmountDatasource
                objFrm._DatasourceActivityAmount = objTransactionHead._FormulaActivityAmountDatasource
                objFrm._DatasourceShift = objTransactionHead._FormulaShiftDatasource 'Sohail (29 Oct 2013)
                objFrm._DatasourceCumulative = objTransactionHead._FormulaCumulativeDatasource 'Sohail (09 Nov 2013)
                objFrm._DatasourceCommonMaster = objTransactionHead._FormulaCMasterDatasource 'Sohail (02 Oct 2014)
                objFrm._DatasourceCRExpense = objTransactionHead._FormulaCRExpenseDatasource 'Sohail (12 Nov 2014)
                objFrm._DatasourceLoanScheme = objTransactionHead._FormulaLoanSchemeDatasource 'Sohail (24 Oct 2017)
                objFrm._DatasourceSlabFormula = mdtFormulaSlab.Clone
                intTranheadslabtranunkid = mdtSimpleSlab.Rows.Count + 1 - ((mdtSimpleSlab.Rows.Count + 1) * 2)
            Else
                moTempAction = enAction.EDIT_ONE
                objFrm._Datasource = objTransactionHead._FormulaDatasource
                objFrm._DatasourceCurrency = objTransactionHead._FormulaCurrencyDatasource
                objFrm._DatasourcePaidUnpaidLeave = objTransactionHead._FormulaLeaveDatasource
                objFrm._DatasourceMeasurementUnit = objTransactionHead._FormulaMeasurementUnitDatasource
                objFrm._DatasourceActivityUnit = objTransactionHead._FormulaActivityUnitDatasource
                objFrm._DatasourceMeasurementAmount = objTransactionHead._FormulaMeasurementAmountDatasource
                objFrm._DatasourceActivityAmount = objTransactionHead._FormulaActivityAmountDatasource
                objFrm._DatasourceShift = objTransactionHead._FormulaShiftDatasource 'Sohail (29 Oct 2013)
                objFrm._DatasourceCumulative = objTransactionHead._FormulaCumulativeDatasource 'Sohail (09 Nov 2013)
                objFrm._DatasourceCommonMaster = objTransactionHead._FormulaCMasterDatasource 'Sohail (02 Oct 2014)
                objFrm._DatasourceCRExpense = objTransactionHead._FormulaCRExpenseDatasource 'Sohail (12 Nov 2014)
                objFrm._DatasourceLoanScheme = objTransactionHead._FormulaLoanSchemeDatasource 'Sohail (24 Oct 2017)
                intTranheadslabtranunkid = CInt(lvTrnHeadSlabSimple.Items(mintItemIndex).Tag)

                Dim dt As DataTable = mdtFormulaSlab.Clone
                Dim dtRow As DataRow = dt.NewRow
                dtRow.Item("tranheadformulaslabtranunkid") = CInt(lvTrnHeadSlabSimple.Items(mintItemIndex).Tag)
                dtRow.Item("tranheadunkid") = mintTranHeadUnkid
                dtRow.Item("periodunkid") = CInt(cboSimpleSlabPeriod.SelectedValue)
                dtRow.Item("end_date") = lvTrnHeadSlabSimple.Items(mintItemIndex).SubItems(colhSimpleEndDate.Index).Text
                If CInt(cboSimpleSlabAmountType.SelectedValue) = enPaymentBy.Fromula Then
                    dtRow.Item("formula") = lvTrnHeadSlabSimple.Items(mintItemIndex).SubItems(colhSimpleValueBasis.Index).Text
                    dtRow.Item("formulaid") = lvTrnHeadSlabSimple.Items(mintItemIndex).SubItems(colhSimpleValueBasis.Index).Tag
                Else
                    dtRow.Item("formula") = ""
                    dtRow.Item("formulaid") = ""
                End If
                dtRow.Item("userunkid") = User._Object._Userunkid
                dtRow.Item("isvoid") = False
                dtRow.Item("voiduserunkid") = -1
                dtRow.Item("voiddatetime") = DBNull.Value
                dtRow.Item("voidreason") = ""
                If IsDBNull(dtRow.Item("AUD")) OrElse dtRow.Item("AUD").ToString.Trim = "" Then
                    dtRow.Item("AUD") = "U"
                Else
                    dtRow.Item("AUD") = "A"
                End If

                dt.Rows.Add(dtRow)
                objFrm._DatasourceSlabFormula = dt
            End If

            If objFrm.displayDialog(mintTranHeadUnkid, moTempAction, CInt(cboTrnHeadType.SelectedValue), CInt(cboTypeOf.SelectedValue), CInt(cboCalcType.SelectedValue), True, CInt(cboSimpleSlabPeriod.SelectedValue), intTranheadslabtranunkid) Then
                txtSlabFormula.Text = objFrm._Formula
                txtSlabFormula.Tag = objFrm._FormulaID
                objTransactionHead._FormulaDatasource = objFrm._Datasource
                objTransactionHead._FormulaCurrencyDatasource = objFrm._DatasourceCurrency
                objTransactionHead._FormulaLeaveDatasource = objFrm._DatasourcePaidUnpaidLeave
                objTransactionHead._FormulaMeasurementUnitDatasource = objFrm._DatasourceMeasurementUnit
                objTransactionHead._FormulaActivityUnitDatasource = objFrm._DatasourceActivityUnit
                objTransactionHead._FormulaMeasurementAmountDatasource = objFrm._DatasourceMeasurementAmount
                objTransactionHead._FormulaActivityAmountDatasource = objFrm._DatasourceActivityAmount
                objTransactionHead._FormulaShiftDatasource = objFrm._DatasourceShift 'Sohail (29 Oct 2013)
                objTransactionHead._FormulaCumulativeDatasource = objFrm._DatasourceCumulative 'Sohail (09 Nov 2013)
                objTransactionHead._FormulaCMasterDatasource = objFrm._DatasourceCommonMaster 'Sohail (02 Oct 2014)
                objTransactionHead._FormulaCRExpenseDatasource = objFrm._DatasourceCRExpense 'Sohail (12 Nov 2014)
                objTransactionHead._FormulaLoanSchemeDatasource = objFrm._DatasourceLoanScheme 'Sohail (24 Oct 2017)
                'mdtFormulaSlab = objFrm._DatasourcePeriodWiseSlab
                'objTransactionHead._FormulaPeriodWiseSlabDatasource = objFrm._DatasourcePeriodWiseSlab
                'objFrm._DatasourceSlabFormula = mdtFormulaSlab.Clone

                If moTempAction <> enAction.EDIT_ONE Then
                    Dim drSlab As DataRow
                    drSlab = mdtSimpleSlab.NewRow()
                    With drSlab
                        'Sohail (24 Sep 2013) -- Start
                        'TRA - ENHANCEMENT
                        '.Item("tranheadslabtranunkid") = -1
                        .Item("tranheadslabtranunkid") = intTranheadslabtranunkid
                        'Sohail (24 Sep 2013) -- End
                        .Item("tranheadunkid") = mintTranHeadUnkid
                        .Item("amountupto") = txtSimpleSlabAmtUpto.Decimal
                        .Item("slabtype") = cboSimpleSlabAmountType.SelectedValue
                        .Item("valuebasis") = txtSimpleSlabValueBasis.Decimal
                        .Item("userunkid") = User._Object._Userunkid
                        .Item("isvoid") = False
                        .Item("voiduserunkid") = -1
                        .Item("voiddatetime") = DBNull.Value
                        .Item("voidreason") = ""
                        .Item("GUID") = Guid.NewGuid.ToString
                        .Item("AUD") = "A"
                        .Item("periodunkid") = CInt(cboSimpleSlabPeriod.SelectedValue)
                        .Item("period_name") = cboSimpleSlabPeriod.Text
                        .Item("end_date") = eZeeDate.convertDate(mdtPeriod_enddate)
                        .Item("formula") = objFrm._Formula
                        .Item("formulaid") = objFrm._FormulaID
                    End With
                    mdtSimpleSlab.Rows.Add(drSlab)
                Else
                    Dim drTemp As DataRow()

                    If CInt(lvTrnHeadSlabSimple.Items(mintItemIndex).Tag) = -1 Then
                        drTemp = mdtSimpleSlab.Select("guid = '" & lvTrnHeadSlabSimple.Items(mintItemIndex).SubItems(colhSimpleGUID.Index).Text & "'")
                    Else
                        drTemp = mdtSimpleSlab.Select("tranheadslabtranunkid = " & CInt(lvTrnHeadSlabSimple.Items(mintItemIndex).Tag))
                    End If

                    If drTemp.Length > 0 Then
                        With drTemp(0)
                            .Item("tranheadslabtranunkid") = lvTrnHeadSlabSimple.Items(mintItemIndex).Tag
                            .Item("tranheadunkid") = mintTranHeadUnkid
                            .Item("amountupto") = txtSimpleSlabAmtUpto.Decimal
                            .Item("slabtype") = cboSimpleSlabAmountType.SelectedValue
                            .Item("valuebasis") = txtSimpleSlabValueBasis.Decimal
                            .Item("userunkid") = User._Object._Userunkid
                            .Item("isvoid") = False
                            .Item("voiduserunkid") = -1
                            .Item("voiddatetime") = DBNull.Value
                            .Item("voidreason") = ""
                            .Item("GUID") = Guid.NewGuid.ToString
                            If IsDBNull(.Item("AUD")) Or .Item("AUD").ToString.Trim = "" Then
                                .Item("AUD") = "U"
                            End If
                            .Item("periodunkid") = CInt(cboSimpleSlabPeriod.SelectedValue)
                            .Item("period_name") = cboSimpleSlabPeriod.Text
                            .Item("end_date") = eZeeDate.convertDate(mdtPeriod_enddate)
                            .Item("formula") = objFrm._Formula
                            .Item("formulaid") = objFrm._FormulaID
                            .AcceptChanges()
                        End With
                    End If
                End If

                Call FillSimpleSlabList()
                Call ResetSimpleSlab()
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnAddSlabFormula_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub txtFormula_MouseMove(ByVal sender As System.Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles txtFormula.MouseMove
        Try
            ToolTip1.SetToolTip(txtFormula, txtFormula.Text)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "txtFormula_MouseMove", mstrModuleName)
        End Try
    End Sub

    Private Sub txtSlabFormula_MouseMove(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles txtSlabFormula.MouseMove
        Try
            ToolTip1.SetToolTip(txtSlabFormula, txtSlabFormula.Text)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "txtSlabFormula_MouseMove", mstrModuleName)
        End Try
    End Sub
    'Sohail (24 Sep 2013) -- End

#End Region

    'Sohail (19 Nov 2010) -- Start
#Region " Check Box's Events "
    Private Sub chkTaxable_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkTaxable.CheckedChanged
        Try
            If chkTaxable.Checked = True Then
                chkTaxRelief.Checked = False
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "chkTaxable_CheckedChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub chkTaxRelief_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkTaxRelief.CheckedChanged
        Try
            If chkTaxRelief.Checked = True Then
                chkTaxable.Checked = False
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "chkTaxRelief_CheckedChanged", mstrModuleName)
        End Try
    End Sub
    'Sohail (20 Apr 2011) -- Start
    Private Sub chkRecurrent_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkRecurrent.CheckedChanged

        Try
            If chkRecurrent.CheckState = CheckState.Unchecked Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 19, "Non Recurrent Feature will be in affect for one time only, as on period close process it will be reset to ZERO. If user wants to use again then will have to assign value on Earning Deduction for related period."), enMsgBoxStyle.Information)
            End If

            'Sohail (28 Jan 2019) -- Start
            'NMB Enhancement - 76.1 - Option "Prorate Flat Rate head" on Transaction Head for Recurrent Head.
            If CInt(cboCalcType.SelectedValue) = CInt(enCalcType.FlatRate_Others) AndAlso chkRecurrent.Checked = True Then
                chkProratedFlatRateHead.Enabled = True
            Else
                If chkProratedFlatRateHead.Checked = True Then chkProratedFlatRateHead.Checked = False
                chkProratedFlatRateHead.Enabled = False
            End If
            'Sohail (28 Jan 2019) -- End

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "chkRecurrent_CheckedChanged", mstrModuleName)
        End Try

    End Sub
    'Sohail (20 Apr 2011) -- End
#End Region
    'Sohail (19 Nov 2010) -- End

#Region " Message List "
    '1, "Sorry, Transaction Head Code can not be blank. Transaction Head is a compulsory information."
    '2, "Sorry, Transaction Head Name can not be blank. Transaction Head is a compulsory information."
    '3, "Please select Transaction Head Type."
    '4, "Please select Type of Transaction."
    '5, "Please select Calculation Type."
    '6, "Please select Calculation Period."
    '7, "Please select Compute On."
    '8, "Please enter an appropriate formula."
    '9, "Please Add atlease one computation information."
    '10, "Sorry, Amount upto can not be less then Zero."
    '11, "Please select Slab Type."
    '12, "Sorry, Value Basis can not be less then Zero."
    '13, "Sorry, Percentage should be in between 1 to 100." & vbCr & vbCr & "Do you want to proceed?"
    '14, "Sorry, Amount should be greater than Previous Amount."
    '15, "Sorry, Fixed Amount can not be Zero."
    '16, "Sorry, Rate Payable can not be Zero."
    '17, "Sorry, Amount should be less than next Amount."
    '18, "Are you sure you want to delete this slab?"
    '19, "Non Recurrent Feature will be in affect for one time only, as at period close process it will be reset to ZERO. If user wants to use again then will have to assign value on Earning Deduction for related period."
    '21, "Please select Period."
    '22, "Sorry! This Period is closed."
    '23, "Sorry! Slab for this Period is already defined."
#End Region

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Private Sub OtherSettings()
        Try
            Me.SuspendLayout()
           
            Call SetLanguage()

            Me.gbComputationInfo.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbComputationInfo.ForeColor = GUI._eZeeContainerHeaderForeColor

            Me.gbTrnHeadInfo.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbTrnHeadInfo.ForeColor = GUI._eZeeContainerHeaderForeColor

           
			
            Me.eZeeHeader.GradientColor1 = GUI._HeaderBackColor1
            Me.eZeeHeader.GradientColor2 = GUI._HeaderBackColor2
            Me.eZeeHeader.BorderColor = GUI._HeaderBorderColor
            Me.eZeeHeader.HeaderTextForeColor = GUI._HeaderTitleFontColor
            Me.eZeeHeader.DescriptionForeColor = GUI._HeaderMessageFontColor


            Me.btnSave.GradientBackColor = GUI._ButttonBackColor
            Me.btnSave.GradientForeColor = GUI._ButttonFontColor

            Me.btnClose.GradientBackColor = GUI._ButttonBackColor
            Me.btnClose.GradientForeColor = GUI._ButttonFontColor

            Me.btnEdit.GradientBackColor = GUI._ButttonBackColor
            Me.btnEdit.GradientForeColor = GUI._ButttonFontColor

            Me.btnDelete.GradientBackColor = GUI._ButttonBackColor
            Me.btnDelete.GradientForeColor = GUI._ButttonFontColor

            Me.btnAdd.GradientBackColor = GUI._ButttonBackColor
            Me.btnAdd.GradientForeColor = GUI._ButttonFontColor


            Me.ResumeLayout()
        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "OtherSettings", mstrModuleName)
        End Try
    End Sub


    Private Sub SetLanguage()
        Try
            Me.Text = Language._Object.getCaption(Me.Name, Me.Text)

			Me.eZeeHeader.Title = Language._Object.getCaption(Me.eZeeHeader.Name & "_Title" , Me.eZeeHeader.Title)
			Me.eZeeHeader.Message = Language._Object.getCaption(Me.eZeeHeader.Name & "_Message" , Me.eZeeHeader.Message)
            Me.gbComputationInfo.Text = Language._Object.getCaption(Me.gbComputationInfo.Name, Me.gbComputationInfo.Text)
            Me.lblComputeOn.Text = Language._Object.getCaption(Me.lblComputeOn.Name, Me.lblComputeOn.Text)
            Me.lblSpecifiedFormula.Text = Language._Object.getCaption(Me.lblSpecifiedFormula.Name, Me.lblSpecifiedFormula.Text)
            Me.gbTrnHeadInfo.Text = Language._Object.getCaption(Me.gbTrnHeadInfo.Name, Me.gbTrnHeadInfo.Text)
            Me.chkAppearOnPayslip.Text = Language._Object.getCaption(Me.chkAppearOnPayslip.Name, Me.chkAppearOnPayslip.Text)
            Me.lnOtherInfo.Text = Language._Object.getCaption(Me.lnOtherInfo.Name, Me.lnOtherInfo.Text)
            Me.lblCalcType.Text = Language._Object.getCaption(Me.lblCalcType.Name, Me.lblCalcType.Text)
            Me.lblType.Text = Language._Object.getCaption(Me.lblType.Name, Me.lblType.Text)
            Me.lblName.Text = Language._Object.getCaption(Me.lblName.Name, Me.lblName.Text)
            Me.lblCode.Text = Language._Object.getCaption(Me.lblCode.Name, Me.lblCode.Text)
            Me.btnSave.Text = Language._Object.getCaption(Me.btnSave.Name, Me.btnSave.Text)
            Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
            Me.chkRecurrent.Text = Language._Object.getCaption(Me.chkRecurrent.Name, Me.chkRecurrent.Text)
            Me.lblTypeOf.Text = Language._Object.getCaption(Me.lblTypeOf.Name, Me.lblTypeOf.Text)
            Me.lblTaxSlabAmountUpto.Text = Language._Object.getCaption(Me.lblTaxSlabAmountUpto.Name, Me.lblTaxSlabAmountUpto.Text)
            Me.lblTaxSlabFixedAmount.Text = Language._Object.getCaption(Me.lblTaxSlabFixedAmount.Name, Me.lblTaxSlabFixedAmount.Text)
            Me.lblTaxSlabValueBasis.Text = Language._Object.getCaption(Me.lblTaxSlabValueBasis.Name, Me.lblTaxSlabValueBasis.Text)
            Me.lblSimleSlabAmtUpto.Text = Language._Object.getCaption(Me.lblSimleSlabAmtUpto.Name, Me.lblSimleSlabAmtUpto.Text)
            Me.lblSimleSlabValueBasis.Text = Language._Object.getCaption(Me.lblSimleSlabValueBasis.Name, Me.lblSimleSlabValueBasis.Text)
            Me.lblSimleSlabAmountType.Text = Language._Object.getCaption(Me.lblSimleSlabAmountType.Name, Me.lblSimleSlabAmountType.Text)
            Me.btnEdit.Text = Language._Object.getCaption(Me.btnEdit.Name, Me.btnEdit.Text)
            Me.btnDelete.Text = Language._Object.getCaption(Me.btnDelete.Name, Me.btnDelete.Text)
            Me.btnAdd.Text = Language._Object.getCaption(Me.btnAdd.Name, Me.btnAdd.Text)
            Me.colhTranHeadunkid.Text = Language._Object.getCaption(CStr(Me.colhTranHeadunkid.Tag), Me.colhTranHeadunkid.Text)
            Me.colhSimpleAmtUpto.Text = Language._Object.getCaption(CStr(Me.colhSimpleAmtUpto.Tag), Me.colhSimpleAmtUpto.Text)
            Me.colhsimpleSlabType.Text = Language._Object.getCaption(CStr(Me.colhsimpleSlabType.Tag), Me.colhsimpleSlabType.Text)
            Me.colhSimpleValueBasis.Text = Language._Object.getCaption(CStr(Me.colhSimpleValueBasis.Tag), Me.colhSimpleValueBasis.Text)
            Me.colhSimpleGUID.Text = Language._Object.getCaption(CStr(Me.colhSimpleGUID.Tag), Me.colhSimpleGUID.Text)
            Me.colhtrantaxslabunkid.Text = Language._Object.getCaption(CStr(Me.colhtrantaxslabunkid.Tag), Me.colhtrantaxslabunkid.Text)
            Me.colhTaxAmountUpto.Text = Language._Object.getCaption(CStr(Me.colhTaxAmountUpto.Tag), Me.colhTaxAmountUpto.Text)
            Me.colhTaxFixedAmount.Text = Language._Object.getCaption(CStr(Me.colhTaxFixedAmount.Tag), Me.colhTaxFixedAmount.Text)
            Me.colhTaxRatePayable.Text = Language._Object.getCaption(CStr(Me.colhTaxRatePayable.Tag), Me.colhTaxRatePayable.Text)
            Me.colhTaxInExcessOf.Text = Language._Object.getCaption(CStr(Me.colhTaxInExcessOf.Tag), Me.colhTaxInExcessOf.Text)
            Me.colhTaxGUID.Text = Language._Object.getCaption(CStr(Me.colhTaxGUID.Tag), Me.colhTaxGUID.Text)
            Me.chkTaxable.Text = Language._Object.getCaption(Me.chkTaxable.Name, Me.chkTaxable.Text)
            Me.chkTaxRelief.Text = Language._Object.getCaption(Me.chkTaxRelief.Name, Me.chkTaxRelief.Text)
            Me.chkCumulative.Text = Language._Object.getCaption(Me.chkCumulative.Name, Me.chkCumulative.Text)
            Me.colhSimplePeriod.Text = Language._Object.getCaption(CStr(Me.colhSimplePeriod.Tag), Me.colhSimplePeriod.Text)
            Me.lblSimpleSlabPeriod.Text = Language._Object.getCaption(Me.lblSimpleSlabPeriod.Name, Me.lblSimpleSlabPeriod.Text)
            Me.lnkCopySlab.Text = Language._Object.getCaption(Me.lnkCopySlab.Name, Me.lnkCopySlab.Text)
            Me.colhSimpleEndDate.Text = Language._Object.getCaption(CStr(Me.colhSimpleEndDate.Tag), Me.colhSimpleEndDate.Text)
            Me.lblTaxSlabPeriod.Text = Language._Object.getCaption(Me.lblTaxSlabPeriod.Name, Me.lblTaxSlabPeriod.Text)
            Me.colhTaxPeriod.Text = Language._Object.getCaption(CStr(Me.colhTaxPeriod.Tag), Me.colhTaxPeriod.Text)
            Me.colhTaxEndDate.Text = Language._Object.getCaption(CStr(Me.colhTaxEndDate.Tag), Me.colhTaxEndDate.Text)
            Me.chkNonCashBenefit.Text = Language._Object.getCaption(Me.chkNonCashBenefit.Name, Me.chkNonCashBenefit.Text)
            Me.chkMonetary.Text = Language._Object.getCaption(Me.chkMonetary.Name, Me.chkMonetary.Text)
			Me.chkBasicsalaryasotherearning.Text = Language._Object.getCaption(Me.chkBasicsalaryasotherearning.Name, Me.chkBasicsalaryasotherearning.Text)
			Me.chkIsPerformance.Text = Language._Object.getCaption(Me.chkIsPerformance.Name, Me.chkIsPerformance.Text)
			Me.chkProratedFlatRateHead.Text = Language._Object.getCaption(Me.chkProratedFlatRateHead.Name, Me.chkProratedFlatRateHead.Text)
			Me.lblDefaultCostCenter.Text = Language._Object.getCaption(Me.lblDefaultCostCenter.Name, Me.lblDefaultCostCenter.Text)
			Me.lblRoundOffTypeId.Text = Language._Object.getCaption(Me.lblRoundOffTypeId.Name, Me.lblRoundOffTypeId.Text)
			Me.lblPRemark.Text = Language._Object.getCaption(Me.lblPRemark.Name, Me.lblPRemark.Text)
			Me.lblPriority.Text = Language._Object.getCaption(Me.lblPriority.Name, Me.lblPriority.Text)

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
        End Try
    End Sub


    Private Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Sorry, Transaction Head Code can not be blank. Transaction Head is a compulsory information.")
            Language.setMessage(mstrModuleName, 2, "Sorry, Transaction Head Name can not be blank. Transaction Head is a compulsory information.")
            Language.setMessage(mstrModuleName, 3, "Please select Transaction Head Type.")
            Language.setMessage(mstrModuleName, 4, "Please select Type of Transaction.")
            Language.setMessage(mstrModuleName, 5, "Please select Calculation Type.")
            Language.setMessage(mstrModuleName, 6, "Please select Period.")
            Language.setMessage(mstrModuleName, 7, "Please select Compute On.")
            Language.setMessage(mstrModuleName, 8, "Please enter an appropriate formula.")
            Language.setMessage(mstrModuleName, 9, "Please Add atleast one computation information.")
            Language.setMessage(mstrModuleName, 10, "Sorry, Amount upto cannot be less than Zero.")
            Language.setMessage(mstrModuleName, 11, "Please select Slab Type.")
            Language.setMessage(mstrModuleName, 12, "Sorry, Value Basis cannot be less then Zero.")
            Language.setMessage(mstrModuleName, 13, "Sorry, Percentage should be between 1 and 100.")
            Language.setMessage(mstrModuleName, 14, "Sorry, Amount should be greater than Previous Amount.")
            Language.setMessage(mstrModuleName, 15, "Sorry! This Period is closed.")
            Language.setMessage(mstrModuleName, 16, "Sorry! Slab for this Period is already defined.")
            Language.setMessage(mstrModuleName, 17, "Sorry, Amount should be less than next Amount.")
            Language.setMessage(mstrModuleName, 18, "Are you sure you want to delete this slab?")
            Language.setMessage(mstrModuleName, 19, "Non Recurrent Feature will be in affect for one time only, as on period close process it will be reset to ZERO. If user wants to use again then will have to assign value on Earning Deduction for related period.")
            Language.setMessage(mstrModuleName, 20, "Do you want to proceed?")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class