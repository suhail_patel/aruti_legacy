﻿Option Strict On

Imports eZeeCommonLib
Imports Aruti.Data

Public Class frmBankEDIList

#Region " Private Variable "
    Private objBabkEDI As clsBankEdi_master
    Private ReadOnly mstrModuleName As String = "frmBankEDIList"

#End Region

#Region " Private Methods "
    Private Sub SetColor()
        Try

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetColor", mstrModuleName)
        End Try
    End Sub

    Private Sub FillList()
        Dim dsList As DataSet
        Try

            'Varsha Rana (17-Oct-2017) -- Start
            'Enhancement - Give user privileges.
            If User._Object.Privilege._AllowToViewBankEDIList = False Then Exit Sub
            'Varsha Rana (17-Oct-2017) -- End


            dsList = objBabkEDI.GetList("BankEDI")

            Dim lvItem As ListViewItem

            lvBankEDI.Items.Clear()
            For Each drRow As DataRow In dsList.Tables("BankEDI").Rows
                lvItem = New ListViewItem
                lvItem.Text = drRow("edicode").ToString
                lvItem.Tag = drRow("bankediunkid").ToString

                lvItem.SubItems.Add(drRow("ediname").ToString)

                lvBankEDI.Items.Add(lvItem)
            Next

            If lvBankEDI.Items.Count > 16 Then
                colhBankEDIName.Width = 565 - 18
            Else
                colhBankEDIName.Width = 565
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillList", mstrModuleName)
        End Try
    End Sub

    'Varsha Rana (17-Oct-2017) -- Start
    'Enhancement - Give user privileges
    Private Sub SetVisibility()

        Try

            btnNew.Enabled = User._Object.Privilege._AddBankEDI
            btnEdit.Enabled = User._Object.Privilege._EditBankEDI
            btnDelete.Enabled = User._Object.Privilege._DeleteBankEDI

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetVisibility", mstrModuleName)
        End Try

    End Sub
    'Varsha Rana (17-Oct-2017) -- End


#End Region

#Region " Form's Event "

    Private Sub frmBankEDIList_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        Try
            objBabkEDI = Nothing
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmBankEDIList_FormClosed", mstrModuleName)
        End Try
    End Sub

    Private Sub frmBankEDIList_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown
        Try
            Select Case e.KeyCode
                Case Keys.Delete
                    btnDelete.PerformClick()
                Case Keys.Escape
                    btnClose.PerformClick()
                Case Keys.Return
                    SendKeys.Send("{TAB}")
            End Select
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmBankEDIList_KeyDown", mstrModuleName)
        End Try
    End Sub

    Private Sub frmBankEDIList_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        objBabkEDI = New clsBankEdi_master
        Try
            Call Set_Logo(Me, gApplicationType)
            'Anjan (02 Sep 2011)-Start
            'Issue : Including Language Settings.
            Language.setLanguage(Me.Name)
            Call OtherSettings()
            'Anjan (02 Sep 2011)-End 
            Language.setLanguage(Me.Name)
            Call SetColor()
            Call FillList()

            'Varsha Rana (17-Oct-2017) -- Start
            'Enhancement - Give user privileges.
            Call SetVisibility()
            'Varsha Rana (17-Oct-2017) -- End

            If lvBankEDI.Items.Count > 0 Then lvBankEDI.Items(0).Selected = True
            lvBankEDI.Select()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmBankEDIList_Load", mstrModuleName)
        End Try
    End Sub
    'Anjan (02 Sep 2011)-Start
    'Issue : Including Language Settings.
    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsBankEdi_master.SetMessages()
            objfrm._Other_ModuleNames = "clsBankEdi_master"
            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub
    'Anjan (02 Sep 2011)-End 
#End Region

#Region " Button's Event "
    Private Sub btnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnClose_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnNew_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNew.Click
        Try
            Dim objFrm As New frmBankEDI_AddEdit
            'Anjan (02 Sep 2011)-Start
            'Issue : Including Language Settings.
            If User._Object._Isrighttoleft = True Then
                objFrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objFrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objFrm)
            End If
            'Anjan (02 Sep 2011)-End 

            If objFrm.displayDialog(-1, enAction.ADD_CONTINUE) Then
                FillList()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnNew_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnEdit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEdit.Click, lvBankEDI.DoubleClick
        Try
            If lvBankEDI.SelectedItems.Count <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please select Bank EDI from the list to perform further operation on it."), enMsgBoxStyle.Information) '?1
                lvBankEDI.Select()
                Exit Sub
            End If

            Dim objFrm As New frmBankEDI_AddEdit

            'Anjan (02 Sep 2011)-Start
            'Issue : Including Language Settings.
            If User._Object._Isrighttoleft = True Then
                objFrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objFrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objFrm)
            End If
            'Anjan (02 Sep 2011)-End 

            Dim intSelectedIndex As Integer
            intSelectedIndex = lvBankEDI.SelectedItems(0).Index
            If objFrm.displayDialog(CInt(lvBankEDI.SelectedItems(0).Tag), enAction.EDIT_ONE) Then
                Call FillList()
            End If
            objFrm = Nothing

            lvBankEDI.Items(intSelectedIndex).Selected = True
            lvBankEDI.EnsureVisible(intSelectedIndex)
            lvBankEDI.Select()
            If objFrm IsNot Nothing Then objFrm.Dispose()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnEdit_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDelete.Click
        Try
            If lvBankEDI.SelectedItems.Count <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please select Bank EDI from the list to perform further operation on it."), enMsgBoxStyle.Information) '?1
                lvBankEDI.Select()
                Exit Sub
            End If
            'If objBabkEDI.isUsed(CInt(lvBankEDI.SelectedItems(0).Tag)) Then
            '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Sorry, You cannot delete this Bank EDI. Reason: This Bank EDI is in use."), enMsgBoxStyle.Information) '?2
            '    lvBankEDI.Select()
            '    Exit Sub
            'End If
            Dim intSelectedIndex As Integer
            intSelectedIndex = lvBankEDI.SelectedItems(0).Index

            If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Are you sure you want to delete this Bank EDI Information?"), CType(enMsgBoxStyle.Question + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.Yes Then

                'S.SANDEEP [28-May-2018] -- START
                'ISSUE/ENHANCEMENT : {Audit Trails} 
                objBabkEDI._FormName = mstrModuleName
                objBabkEDI._LoginEmployeeunkid = 0
                objBabkEDI._ClientIP = getIP()
                objBabkEDI._HostName = getHostName()
                objBabkEDI._FromWeb = False
                objBabkEDI._AuditUserId = User._Object._Userunkid
objBabkEDI._CompanyUnkid = Company._Object._Companyunkid
                objBabkEDI._AuditDate = ConfigParameter._Object._CurrentDateAndTime
                'S.SANDEEP [28-May-2018] -- END

                objBabkEDI.Void(CInt(lvBankEDI.SelectedItems(0).Tag), User._Object._Userunkid, Now, "") 'Sohail (14 Oct 2010)
                lvBankEDI.SelectedItems(0).Remove()

                If lvBankEDI.Items.Count <= 0 Then
                    Exit Try
                End If

                If lvBankEDI.Items.Count = intSelectedIndex Then
                    intSelectedIndex = lvBankEDI.Items.Count - 1
                    lvBankEDI.Items(intSelectedIndex).Selected = True
                    lvBankEDI.EnsureVisible(intSelectedIndex)
                ElseIf lvBankEDI.Items.Count <> 0 Then
                    lvBankEDI.Items(intSelectedIndex).Selected = True
                    lvBankEDI.EnsureVisible(intSelectedIndex)
                End If
            End If
            lvBankEDI.Select()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnDelete_Click", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Message List "
    '1, "Please select Bank EDI from the list to perform further operation on it."
    '2, "Sorry, You cannot delete this Bank EDI. Reason: This Bank EDI is in use."
    '3, "Are you sure you want to delete this Bank EDI Information?"
#End Region
   
	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Private Sub OtherSettings()
		Try
			Me.SuspendLayout()
			
			Call SetLanguage()
			
			
			Me.eZeeHeader.GradientColor1 = GUI._HeaderBackColor1 
			Me.eZeeHeader.GradientColor2 = GUI._HeaderBackColor2 
			Me.eZeeHeader.BorderColor = GUI._HeaderBorderColor 
			Me.eZeeHeader.HeaderTextForeColor = GUI._HeaderTitleFontColor 
			Me.eZeeHeader.DescriptionForeColor = GUI._HeaderMessageFontColor 


			Me.btnDelete.GradientBackColor = GUI._ButttonBackColor 
			Me.btnDelete.GradientForeColor = GUI._ButttonFontColor

			Me.btnEdit.GradientBackColor = GUI._ButttonBackColor 
			Me.btnEdit.GradientForeColor = GUI._ButttonFontColor

			Me.btnNew.GradientBackColor = GUI._ButttonBackColor 
			Me.btnNew.GradientForeColor = GUI._ButttonFontColor

			Me.btnClose.GradientBackColor = GUI._ButttonBackColor 
			Me.btnClose.GradientForeColor = GUI._ButttonFontColor


			Me.ResumeLayout()
		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "OtherSettings", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetLanguage()
		Try
			Me.Text = Language._Object.getCaption(Me.Name, Me.Text)
			
			Me.eZeeHeader.Title = Language._Object.getCaption(Me.eZeeHeader.Name & "_Title" , Me.eZeeHeader.Title)
			Me.eZeeHeader.Message = Language._Object.getCaption(Me.eZeeHeader.Name & "_Message" , Me.eZeeHeader.Message)
			Me.colhBankEDICode.Text = Language._Object.getCaption(CStr(Me.colhBankEDICode.Tag), Me.colhBankEDICode.Text)
			Me.colhBankEDIName.Text = Language._Object.getCaption(CStr(Me.colhBankEDIName.Tag), Me.colhBankEDIName.Text)
			Me.btnDelete.Text = Language._Object.getCaption(Me.btnDelete.Name, Me.btnDelete.Text)
			Me.btnEdit.Text = Language._Object.getCaption(Me.btnEdit.Name, Me.btnEdit.Text)
			Me.btnNew.Text = Language._Object.getCaption(Me.btnNew.Name, Me.btnNew.Text)
			Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetLanguage", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "Please select Bank EDI from the list to perform further operation on it.")
			Language.setMessage(mstrModuleName, 2, "Are you sure you want to delete this Bank EDI Information?")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class