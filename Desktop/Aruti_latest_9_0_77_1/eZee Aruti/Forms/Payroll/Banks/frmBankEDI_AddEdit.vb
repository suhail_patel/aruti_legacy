﻿Option Strict On

Imports eZeeCommonLib
Imports Aruti.Data

Public Class frmBankEDI_AddEdit

#Region " Private Variable "
    Private ReadOnly mstrModuleName As String = "frmBankEDI_AddEdit"
    Private mblnCancel As Boolean = True
    Private objBankEDI As clsBankEdi_master
    Private menAction As enAction = enAction.ADD_ONE
    Private mintBankEDIUnkid As Integer = -1

#End Region

#Region " Display Dialog "

    Public Function displayDialog(ByRef intUnkId As Integer, ByVal eAction As enAction) As Boolean
        Try
            mintBankEDIUnkid = intUnkId
            menAction = eAction

            Me.ShowDialog()

            intUnkId = mintBankEDIUnkid

            Return Not mblnCancel
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "displayDialog", mstrModuleName)
        End Try
    End Function

#End Region

#Region " Private Methods "
    Private Sub SetColor()
        Try
            txtCode.BackColor = GUI.ColorComp
            txtName.BackColor = GUI.ColorComp

            'Header
            txtHeaderID.BackColor = GUI.ColorComp
            txtFileTypeChar.BackColor = GUI.ColorComp
            txtFileID.BackColor = GUI.ColorComp
            txtHeaderCompanyName.BackColor = GUI.ColorComp
            txtWorkCode.BackColor = GUI.ColorComp
            txtHeaderClearCode.BackColor = GUI.ColorComp
            txtSender.BackColor = GUI.ColorComp
            txtReceiver.BackColor = GUI.ColorComp
            txtVersion.BackColor = GUI.ColorComp

            'Datarecord
            txtPaymentID.BackColor = GUI.ColorComp
            txtSalaryCode.BackColor = GUI.ColorComp
            txtProcessRef.BackColor = GUI.ColorComp
            txtContraCode.BackColor = GUI.ColorComp
            txtDataCompanyName.BackColor = GUI.ColorComp
            cboTrnHead.BackColor = GUI.ColorComp
            cboCurrency.BackColor = GUI.ColorComp

            'Debit Bank
            cboBank.BackColor = GUI.ColorComp
            cboBranch.BackColor = GUI.ColorComp
            cboAccType.BackColor = GUI.ColorComp
            txtAccNo.BackColor = GUI.ColorComp

            'Trailer info
            txtTrailerID.BackColor = GUI.ColorComp
            txtTrailerClearCode.BackColor = GUI.ColorComp
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetColor", mstrModuleName)
        End Try
    End Sub

    Private Sub GetValue()
        Try
            With objBankEDI
                txtCode.Text = ._Edicode
                txtName.Text = ._Ediname
                txtHeaderID.Text = ._Headerid
                txtFileTypeChar.Text = ._Filetype
                If ._Processdate = Nothing Then
                    'Sohail (28 Dec 2010) -- Start
                    'dtpProcessDate.Value = DateTime.Today
                    dtpProcessDate.Value = CDate(IIf(DateTime.Today.Date > FinancialYear._Object._Database_End_Date, FinancialYear._Object._Database_End_Date, DateTime.Today.Date))
                    'Sohail (28 Dec 2010) -- End
                Else
                    dtpProcessDate.Value = ._Processdate
                End If
                txtHeaderClearCode.Text = ._Headerclearcode
                txtFileID.Text = ._Fileid
                txtHeaderCompanyName.Text = ._Headercompname
                txtSender.Text = ._Senderid
                txtReceiver.Text = ._Receiver
                txtWorkCode.Text = ._Workcode
                txtVersion.Text = ._Version
                txtTrailerID.Text = ._Trailerid
                txtTrailerClearCode.Text = ._Trailerclearcode
                txtPaymentID.Text = ._Paymentid
                txtSalaryCode.Text = ._Salarycode
                txtProcessRef.Text = ._Processref
                txtContraCode.Text = ._Contracode
                If ._Paydate = Nothing Then
                    dtpPayDate.Value = DateTime.Today
                Else
                    dtpPayDate.Value = ._Paydate
                End If

                txtDataCompanyName.Text = ._Datacompname
                cboTrnHead.SelectedValue = ._Tranheadunkid
                cboCurrency.SelectedValue = ._Currencyunkid
                cboBank.SelectedValue = ._Bankgroupunkid
                cboBranch.SelectedValue = ._Branchunkid
                txtAccNo.Text = ._Bankaccno
                cboAccType.SelectedValue = ._Accounttypeunkid

            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetValue", mstrModuleName)
        End Try
    End Sub

    Private Sub SetValue()
        Try
            With objBankEDI
                ._Edicode = txtCode.Text.Trim
                ._Ediname = txtName.Text.Trim
                ._Headerid = txtHeaderID.Text.Trim
                ._Filetype = txtFileTypeChar.Text.Trim
                ._Processdate = dtpProcessDate.Value
                ._Headerclearcode = txtHeaderClearCode.Text.Trim
                ._Fileid = txtFileID.Text.Trim
                ._Headercompname = txtHeaderCompanyName.Text.Trim
                ._Senderid = txtSender.Text.Trim
                ._Receiver = txtReceiver.Text.Trim
                ._Workcode = txtWorkCode.Text.Trim
                ._Version = txtVersion.Text.Trim
                ._Trailerid = txtTrailerID.Text.Trim
                ._Trailerclearcode = txtContraCode.Text.Trim
                ._Paymentid = txtPaymentID.Text.Trim
                ._Salarycode = txtSalaryCode.Text.Trim
                ._Processref = txtProcessRef.Text.Trim
                ._Contracode = txtContraCode.Text.Trim
                ._Paydate = dtpPayDate.Value
                ._Datacompname = txtDataCompanyName.Text.Trim
                ._Tranheadunkid = CInt(cboTrnHead.SelectedValue)
                ._Currencyunkid = CInt(cboCurrency.SelectedValue)
                ._Bankgroupunkid = CInt(cboBank.SelectedValue)
                ._Branchunkid = CInt(cboBranch.SelectedValue)
                ._Bankaccno = txtAccNo.Text.Trim
                ._Accounttypeunkid = CInt(cboAccType.SelectedValue)

                ._Userunkid = User._Object._Userunkid 'Sohail (14 Oct 2010)

            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetValue", mstrModuleName)
        End Try
    End Sub

    Private Sub FillCombo()
        Dim objTranHead As New clsTransactionHead
        Dim objCurrency As New clsExchangeRate
        Dim objBank As New clspayrollgroup_master
        Dim objAccType As New clsBankAccType
        Dim dsCombo As DataSet
        Try
            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'dsCombo = objTranHead.getComboList("Tranhead", True)
            dsCombo = objTranHead.getComboList(FinancialYear._Object._DatabaseName, "Tranhead", True)
            'Sohail (21 Aug 2015) -- End
            With cboTrnHead
                .ValueMember = "tranheadunkid"
                .DisplayMember = "Name"
                .DataSource = dsCombo.Tables("Tranhead")
                .SelectedValue = 0
            End With

            dsCombo = objCurrency.getComboList("Cuurency", True)
            With cboCurrency
                .ValueMember = "exchangerateunkid"
                .DisplayMember = "currency_name"
                .DataSource = dsCombo.Tables("Cuurency")
                .SelectedValue = 0
            End With

            dsCombo = objBank.getListForCombo(enGroupType.BankGroup, "Bank", True)
            With cboBank
                .ValueMember = "groupmasterunkid"
                .DisplayMember = "Name"
                .DataSource = dsCombo.Tables("Bank")
                .SelectedValue = 0
            End With

            dsCombo = objAccType.getComboList(True, "AccType")
            With cboAccType
                .ValueMember = "accounttypeunkid"
                .DisplayMember = "Name"
                .DataSource = dsCombo.Tables("AccType")
                .SelectedValue = 0
            End With

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        Finally
            objTranHead = Nothing
            objCurrency = Nothing
            objBank = Nothing
            objAccType = Nothing
        End Try
    End Sub
#End Region

#Region " Private Functions "
    Private Function IsValidate() As Boolean
        Try
            If txtCode.Text.Trim = "" Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please enter EDI Code. EDI Code is mandatory information."), enMsgBoxStyle.Information)
                txtCode.Focus()
                Exit Function
            ElseIf txtName.Text.Trim = "" Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Please enter EDI Name. EDI Name is mandatory information."), enMsgBoxStyle.Information)
                txtName.Focus()
                Exit Function
            End If
            Return True
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "IsValidate", mstrModuleName)
        End Try
    End Function
#End Region

#Region " ComboBox's Events "
    Private Sub cboBank_SelectedValueChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboBank.SelectedValueChanged
        Dim objBranch As New clsbankbranch_master
        Dim dsCombo As DataSet
        Try
            dsCombo = objBranch.getListForCombo("Branch", True, CInt(cboBank.SelectedValue))
            With cboBranch
                .ValueMember = "branchunkid"
                .DisplayMember = "Name"
                .DataSource = dsCombo.Tables("Branch")
                .SelectedValue = 0
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboBank_SelectedValueChanged", mstrModuleName)
        Finally
            objBranch = Nothing
        End Try
    End Sub
#End Region

#Region " Form's Event "

    Private Sub frmBankEDI_AddEdit_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        Try
            objBankEDI = Nothing
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmBankEDI_AddEdit_FormClosed", mstrModuleName)
        End Try
    End Sub

    Private Sub frmBankEDI_AddEdit_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown
        Try
            Select Case e.KeyCode
                Case Keys.Return
                    SendKeys.Send("{TAB}")
                    e.Handled = True
                Case Keys.S
                    If e.Control = True Then
                        btnSave.PerformClick()
                    End If
            End Select
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmBankEDI_AddEdit_KeyDown", mstrModuleName)
        End Try
    End Sub

    Private Sub frmBankEDI_AddEdit_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        objBankEDI = New clsBankEdi_master
        Try
            Call Set_Logo(Me, gApplicationType)


            'Anjan (02 Sep 2011)-Start
            'Issue : Including Language Settings.
            Language.setLanguage(Me.Name)
            Call OtherSettings()
            'Anjan (02 Sep 2011)-End 

            Call SetColor()
            Call FillCombo()

            'Sohail (01 Dec 2010) -- Start
            dtpProcessDate.MinDate = FinancialYear._Object._Database_Start_Date
            dtpProcessDate.MaxDate = FinancialYear._Object._Database_End_Date
            'Sohail (01 Dec 2010) -- End

            If menAction = enAction.EDIT_ONE Then
                objBankEDI._Bankediunkid = mintBankEDIUnkid
            End If

            Call GetValue()

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmBankEDI_AddEdit_Load", mstrModuleName)
        End Try
    End Sub

    'Anjan (02 Sep 2011)-Start
    'Issue : Including Language Settings.
    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsBankEdi_master.SetMessages()
            objfrm._Other_ModuleNames = "clsBankEdi_master"
            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub
    'Anjan (02 Sep 2011)-End 

#End Region

#Region " Button's Event "
    Private Sub btnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnClose_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Dim blnFlag As Boolean = False
        Try
            If IsValidate() = False Then Exit Sub

            Call SetValue()

            'S.SANDEEP [28-May-2018] -- START
            'ISSUE/ENHANCEMENT : {Audit Trails} 
            objBankEDI._FormName = mstrModuleName
            objBankEDI._LoginEmployeeunkid = 0
            objBankEDI._ClientIP = getIP()
            objBankEDI._HostName = getHostName()
            objBankEDI._FromWeb = False
            objBankEDI._AuditUserId = User._Object._Userunkid
objBankEDI._CompanyUnkid = Company._Object._Companyunkid
            objBankEDI._AuditDate = ConfigParameter._Object._CurrentDateAndTime
            'S.SANDEEP [28-May-2018] -- END

            If menAction = enAction.EDIT_ONE Then
                blnFlag = objBankEDI.Update()
            Else
                blnFlag = objBankEDI.Insert()
            End If

            If blnFlag = False And objBankEDI._Message <> "" Then
                eZeeMsgBox.Show(objBankEDI._Message, enMsgBoxStyle.Information)
            End If

            If blnFlag Then
                mblnCancel = False
                If menAction = enAction.ADD_CONTINUE Then
                    objBankEDI = Nothing
                    objBankEDI = New clsBankEdi_master
                    Call GetValue()
                    txtCode.Select()
                Else
                    mintBankEDIUnkid = objBankEDI._Bankediunkid
                    Me.Close()
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnSave_Click", mstrModuleName)
        End Try
    End Sub
#End Region

#Region " Other Controls Events "
    Private Sub objbtnOtherLanguage_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnOtherLanguage.Click
        Dim objFrm As New NameLanguagePopup_Form
        Try
            'Anjan (02 Sep 2011)-Start
            'Issue : Including Language Settings.
            If User._Object._Isrighttoleft = True Then
                objFrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objFrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objFrm)
            End If
            'Anjan (02 Sep 2011)-End 
            Call objFrm.displayDialog(txtName.Text, objBankEDI._Ediname1, objBankEDI._Ediname2)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnOtherLanguage_Click", mstrModuleName)
        End Try
    End Sub
#End Region

#Region " Message List "
    '1, "Please enter EDI Code. EDI Code is mandatory information."
    '2, "Please enter EDI Name. EDI Name is mandatory information."
#End Region


    'Sandeep [ 17 DEC 2010 ] -- Start
    Private Sub objbtnSearchTranHead_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchTranHead.Click
        Dim frm As New frmCommonSearch
        Try
            'Anjan (02 Sep 2011)-Start
            'Issue : Including Language Settings.
            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If
            'Anjan (02 Sep 2011)-End 

            frm.ValueMember = cboTrnHead.ValueMember
            frm.DisplayMember = cboTrnHead.DisplayMember
            frm.DataSource = CType(cboTrnHead.DataSource, DataTable)
            frm.CodeMember = "code"
            If frm.DisplayDialog Then
                cboTrnHead.SelectedValue = frm.SelectedValue
                cboTrnHead.Focus()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchTranHead_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub objbtnAddAccountType_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnAddAccountType.Click
        Dim frm As New frmBankAccountType_AddEdit
        Dim intRefId As Integer = -1
        Try
            'Anjan (02 Sep 2011)-Start
            'Issue : Including Language Settings.
            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If
            'Anjan (02 Sep 2011)-End 

            frm.displayDialog(intRefId, enAction.ADD_ONE)
            If intRefId > 0 Then
                Dim dsList As New DataSet
                Dim objAccType As New clsBankAccType
                dsList = objAccType.getComboList(True, "AccType")
                With cboAccType
                    .ValueMember = "accounttypeunkid"
                    .DisplayMember = "name"
                    .DataSource = dsList.Tables("AccType")
                    .SelectedValue = intRefId
                End With
                dsList.Dispose()
                objAccType = Nothing
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnAddAccountType_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub objbtnAddBranch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnAddBranch.Click
        Dim frm As New frmBankBranch_AddEdit
        Dim intRefId As Integer = -1
        Try
            'Anjan (02 Sep 2011)-Start
            'Issue : Including Language Settings.
            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If
            'Anjan (02 Sep 2011)-End 

            frm.displayDialog(intRefId, enAction.ADD_ONE)
            If intRefId > 0 Then
                Dim dsList As New DataSet
                Dim objBranch As New clsbankbranch_master
                dsList = objBranch.getListForCombo("Branch", True, CInt(cboBank.SelectedValue))
                With cboBranch
                    .ValueMember = "branchunkid"
                    .DisplayMember = "name"
                    .DataSource = dsList.Tables("Branch")
                    .SelectedValue = intRefId
                End With
                dsList.Dispose()
                objBranch = Nothing
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnAddBranch_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub
    'Sandeep [ 17 DEC 2010 ] -- End 

	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Private Sub OtherSettings()
		Try
			Me.SuspendLayout()
            Call SetLanguage()
			
			Me.gbTrailerInfo.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbTrailerInfo.ForeColor = GUI._eZeeContainerHeaderForeColor 

			Me.gbDataRecord.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbDataRecord.ForeColor = GUI._eZeeContainerHeaderForeColor 

			Me.gbHeaderInfo.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbHeaderInfo.ForeColor = GUI._eZeeContainerHeaderForeColor 

			Me.gbBankEDIInformation.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbBankEDIInformation.ForeColor = GUI._eZeeContainerHeaderForeColor 


			Me.EZeeHeader.GradientColor1 = GUI._HeaderBackColor1 
			Me.EZeeHeader.GradientColor2 = GUI._HeaderBackColor2 
			Me.EZeeHeader.BorderColor = GUI._HeaderBorderColor 
			Me.EZeeHeader.HeaderTextForeColor = GUI._HeaderTitleFontColor 
			Me.EZeeHeader.DescriptionForeColor = GUI._HeaderMessageFontColor 


			Me.btnClose.GradientBackColor = GUI._ButttonBackColor 
			Me.btnClose.GradientForeColor = GUI._ButttonFontColor

			Me.btnSave.GradientBackColor = GUI._ButttonBackColor 
			Me.btnSave.GradientForeColor = GUI._ButttonFontColor


			Me.ResumeLayout()
		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "OtherSettings", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetLanguage()
		Try
			Me.Text = Language._Object.getCaption(Me.Name, Me.Text)
			
			Me.lblAccNo.Text = Language._Object.getCaption(Me.lblAccNo.Name, Me.lblAccNo.Text)
			Me.elDebitBankInfo.Text = Language._Object.getCaption(Me.elDebitBankInfo.Name, Me.elDebitBankInfo.Text)
			Me.lblAccType.Text = Language._Object.getCaption(Me.lblAccType.Name, Me.lblAccType.Text)
			Me.lblBranch.Text = Language._Object.getCaption(Me.lblBranch.Name, Me.lblBranch.Text)
			Me.lblBank.Text = Language._Object.getCaption(Me.lblBank.Name, Me.lblBank.Text)
			Me.lblSalaryCode.Text = Language._Object.getCaption(Me.lblSalaryCode.Name, Me.lblSalaryCode.Text)
			Me.gbTrailerInfo.Text = Language._Object.getCaption(Me.gbTrailerInfo.Name, Me.gbTrailerInfo.Text)
			Me.lblTrailerClearCode.Text = Language._Object.getCaption(Me.lblTrailerClearCode.Name, Me.lblTrailerClearCode.Text)
			Me.lblTrailerID.Text = Language._Object.getCaption(Me.lblTrailerID.Name, Me.lblTrailerID.Text)
			Me.lblCurrency.Text = Language._Object.getCaption(Me.lblCurrency.Name, Me.lblCurrency.Text)
			Me.lblTrnHead.Text = Language._Object.getCaption(Me.lblTrnHead.Name, Me.lblTrnHead.Text)
			Me.lblPayDate.Text = Language._Object.getCaption(Me.lblPayDate.Name, Me.lblPayDate.Text)
			Me.gbDataRecord.Text = Language._Object.getCaption(Me.gbDataRecord.Name, Me.gbDataRecord.Text)
			Me.lblContraCode.Text = Language._Object.getCaption(Me.lblContraCode.Name, Me.lblContraCode.Text)
			Me.lblProcessRef.Text = Language._Object.getCaption(Me.lblProcessRef.Name, Me.lblProcessRef.Text)
			Me.lblDataCompanyName.Text = Language._Object.getCaption(Me.lblDataCompanyName.Name, Me.lblDataCompanyName.Text)
			Me.lblPaymentID.Text = Language._Object.getCaption(Me.lblPaymentID.Name, Me.lblPaymentID.Text)
			Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
			Me.btnSave.Text = Language._Object.getCaption(Me.btnSave.Name, Me.btnSave.Text)
			Me.gbHeaderInfo.Text = Language._Object.getCaption(Me.gbHeaderInfo.Name, Me.gbHeaderInfo.Text)
			Me.lblVersion.Text = Language._Object.getCaption(Me.lblVersion.Name, Me.lblVersion.Text)
			Me.lblWorkCode.Text = Language._Object.getCaption(Me.lblWorkCode.Name, Me.lblWorkCode.Text)
			Me.lblReceiver.Text = Language._Object.getCaption(Me.lblReceiver.Name, Me.lblReceiver.Text)
			Me.lblSender.Text = Language._Object.getCaption(Me.lblSender.Name, Me.lblSender.Text)
			Me.lblHeaderCompanyName.Text = Language._Object.getCaption(Me.lblHeaderCompanyName.Name, Me.lblHeaderCompanyName.Text)
			Me.lblFileID.Text = Language._Object.getCaption(Me.lblFileID.Name, Me.lblFileID.Text)
			Me.lblHeaderClearCode.Text = Language._Object.getCaption(Me.lblHeaderClearCode.Name, Me.lblHeaderClearCode.Text)
			Me.lblProcessDate.Text = Language._Object.getCaption(Me.lblProcessDate.Name, Me.lblProcessDate.Text)
			Me.lblFileTypeChar.Text = Language._Object.getCaption(Me.lblFileTypeChar.Name, Me.lblFileTypeChar.Text)
			Me.lblHeaderID.Text = Language._Object.getCaption(Me.lblHeaderID.Name, Me.lblHeaderID.Text)
			Me.EZeeHeader.Title = Language._Object.getCaption(Me.EZeeHeader.Name & "_Title" , Me.EZeeHeader.Title)
			Me.EZeeHeader.Message = Language._Object.getCaption(Me.EZeeHeader.Name & "_Message" , Me.EZeeHeader.Message)
			Me.gbBankEDIInformation.Text = Language._Object.getCaption(Me.gbBankEDIInformation.Name, Me.gbBankEDIInformation.Text)
			Me.lblName.Text = Language._Object.getCaption(Me.lblName.Name, Me.lblName.Text)
			Me.lblCode.Text = Language._Object.getCaption(Me.lblCode.Name, Me.lblCode.Text)

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetLanguage", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "Please enter EDI Code. EDI Code is mandatory information.")
			Language.setMessage(mstrModuleName, 2, "Please enter EDI Name. EDI Name is mandatory information.")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class