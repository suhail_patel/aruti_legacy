﻿Option Strict On

Imports eZeeCommonLib
Imports Aruti.Data
Public Class frmClosePayroll

#Region " Private Variables "
    Private ReadOnly mstrModuleName As String = "frmClosePayroll"

    Private mdtPayYearStartDate As DateTime
    Private mdtPayYearEndDate As DateTime
    Private mdtPayPeriodStartDate As DateTime
    Private mdtPayPeriodEndDate As DateTime
    'Sohail (07 Jan 2014) -- Start
    'Enhancement - Separate TnA Periods from Payroll Periods
    Private mdtTnAStartDate As DateTime
    Private mdtTnAEndDate As DateTime
    'Sohail (07 Jan 2014) -- End
    'Sohail (21 Jul 2012) -- Start
    'TRA - ENHANCEMENT
    Private mdtNextPayPeriodStartDate As DateTime
    Private mdtNextPayPeriodEndDate As DateTime
    'Sohail (21 Jul 2012) -- End
    Private WithEvents objBackupDatabase As New eZeeDatabase 'Sohail (20 Feb 2012)
    'Sohail (12 Dec 2015) -- Start
    'Enhancement - Showing Progress of closing period.
    Private mblnProcessFailed As Boolean = False
    'Sohail (12 Dec 2015) -- End
#End Region

#Region " Private Methods "
    Private Sub SetColor()
        Try
            cboPayYearClose.BackColor = GUI.ColorComp
            cboPayPeriodClose.BackColor = GUI.ColorComp
            cboPayPeriodOpen.BackColor = GUI.ColorComp 'Sohail (04 Mar 2011)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetColor", mstrModuleName)
        End Try
    End Sub

    Private Sub FillCombo()
        Dim objYear As New clsMasterData
        Dim dsCombo As DataSet
        Try
            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'dsCombo = objYear.getComboListPAYYEAR("Year", True)
            dsCombo = objYear.getComboListPAYYEAR(FinancialYear._Object._YearUnkid, FinancialYear._Object._FinancialYear_Name, Company._Object._Companyunkid, "Year", True)
            'S.SANDEEP [04 JUN 2015] -- END

            With cboPayYearClose
                .ValueMember = "Id"
                .DisplayMember = "name"
                .DataSource = dsCombo.Tables("Year")
                If .Items.Count > 0 Then .SelectedValue = 0
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        Finally
            objYear = Nothing
        End Try
    End Sub


    Private Sub SetVisibility()

        Try
            btnClosePeriod.Enabled = User._Object.Privilege._AllowClosePeriod
            btnCloseYear.Enabled = User._Object.Privilege._AllowCloseYear

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetVisibility", mstrModuleName)
        End Try

    End Sub

#End Region

#Region " Private Functions "

    Private Function IsValid() As Boolean
        Try
            If CInt(cboPayYearClose.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please select Pay Year. Pay Year is mandatory information."), enMsgBoxStyle.Information)
                cboPayYearClose.Focus()
                Exit Function
                'Sohail (16 Oct 2010) -- Start
                'ElseIf CInt(cboPayPeriodClose.SelectedValue) <= 0 Then
                '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Please select Pay Period. Pay Period is mandatory information."), enMsgBoxStyle.Information)
                '    cboPayPeriodClose.Focus()
                '    Exit Function
                'Sohail (16 Oct 2010) -- End
            End If


            Return True
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "IsValid", mstrModuleName)
        End Try
    End Function
#End Region

#Region " Form's Evnets "

    Private Sub frmClosePayroll_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown
        Try
            If e.KeyCode = Keys.Return Then
                SendKeys.Send("{TAB}")
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmClosePayroll_KeyDown", mstrModuleName)
        End Try
    End Sub

    Private Sub frmClosePayroll_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            Call Set_Logo(Me, gApplicationType)

            'Anjan (02 Sep 2011)-Start
            'Issue : Including Language Settings.
            Language.setLanguage(Me.Name)
            Call OtherSettings()
            'Anjan (02 Sep 2011)-End 

            Call SetVisibility()
            Call SetColor()
            Call FillCombo()
            lvEmployee.Items.Clear()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmClosePayroll_Load", mstrModuleName)
        End Try
    End Sub
    'Anjan (02 Sep 2011)-Start
    'Issue : Including Language Settings.
    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            'Sohail (10 Jul 2014) -- Start
            'Enhancement - Custom Language.
            'clsClosePeriod.SetMessages()
            'objfrm._Other_ModuleNames = "clsClosePeriod"
            'Sohail (10 Jul 2014) -- End
            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub
    'Anjan (02 Sep 2011)-End 

#End Region

#Region " ComboBox's Events "
    Private Sub cboPayYearClose_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboPayYearClose.SelectedIndexChanged
        'Dim objCompany As New clsCompany_Master
        Dim objPeriod As New clscommom_period_Tran
        Dim dsCombo As DataSet
        Dim dtTable As DataTable

        Try

            'Sohail (11 Sep 2010) -- Start
            'Changes : Now get pay year start and end date from financialyear table not from company table
            'objCompany.GetFinancialDate(1)
            'mdtPayYearStartDate = objCompany._Financial_Start_Date
            'mdtPayYearEndDate = objCompany._Financial_End_Date
            mdtPayYearStartDate = FinancialYear._Object._Database_Start_Date
            mdtPayYearEndDate = FinancialYear._Object._Database_End_Date
            'Sohail (11 Sep 2010) -- End

            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'dsCombo = objPeriod.getListForCombo(enModuleReference.Payroll, CInt(cboPayYearClose.SelectedValue), "Period", True, enStatusType.Open)
            dsCombo = objPeriod.getListForCombo(enModuleReference.Payroll, CInt(cboPayYearClose.SelectedValue), FinancialYear._Object._DatabaseName, FinancialYear._Object._Database_Start_Date, "Period", True, enStatusType.Open)
            'Sohail (21 Aug 2015) -- End
            'Gajanan [02-June-2020] -- Start
            'Enhancement Configure Option That Allow Period Closer Before XX Days.
            'dtTable = New DataView(dsCombo.Tables("Period"), "end_date <= '" & eZeeDate.convertDate(DateTime.Today) & "'", "end_date", DataViewRowState.CurrentRows).ToTable
            dtTable = New DataView(dsCombo.Tables("Period"), "start_date <= '" & eZeeDate.convertDate(DateTime.Today) & "'", "end_date", DataViewRowState.CurrentRows).ToTable
            'Gajanan [02-June-2020] -- End

            With cboPayPeriodClose
                .ValueMember = "periodunkid"
                .DisplayMember = "Name"
                .DataSource = dtTable
                If .Items.Count > 0 Then .SelectedValue = 0
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboPayYearClose_SelectedIndexChanged", mstrModuleName)
        Finally
            'objCompany = Nothing
            objPeriod = Nothing
            dtTable = Nothing
            dsCombo = Nothing
        End Try
    End Sub

    Private Sub cboPayPeriodClose_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboPayPeriodClose.SelectedIndexChanged
        Dim objPeriod As New clscommom_period_Tran
        Dim dsList As DataSet = Nothing
        'Sohail (04 Mar 2011) -- Start
        Dim objPayroll As New clsPayrollProcessTran
        Dim lvItem As ListViewItem
        'Sohail (04 Mar 2011) -- End
        'ArtLic._Object = New ArutiLic(False)
        Try
            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'objPeriod._Periodunkid = CInt(cboPayPeriodClose.SelectedValue)
            objPeriod._Periodunkid(FinancialYear._Object._DatabaseName) = CInt(cboPayPeriodClose.SelectedValue)
            'Sohail (21 Aug 2015) -- End
            mdtPayPeriodStartDate = objPeriod._Start_Date
            mdtPayPeriodEndDate = objPeriod._End_Date
            'Sohail (07 Jan 2014) -- Start
            'Enhancement - Separate TnA Periods from Payroll Periods
            mdtTnAStartDate = objPeriod._TnA_StartDate
            mdtTnAEndDate = objPeriod._TnA_EndDate
            'Sohail (07 Jan 2014) -- End

            'Sohail (04 Mar 2011) -- Start
            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'dsList = objPeriod.GetNextPeriod("NextPeriod", enModuleReference.Payroll, CInt(cboPayPeriodClose.SelectedValue))
            dsList = objPeriod.GetNextPeriod("NextPeriod", enModuleReference.Payroll, CInt(cboPayPeriodClose.SelectedValue), FinancialYear._Object._YearUnkid)
            'Sohail (21 Aug 2015) -- End
            With cboPayPeriodOpen
                .ValueMember = "periodunkid"
                .DisplayMember = "period_name"
                .DataSource = dsList.Tables("NextPeriod")
                If .Items.Count > 0 Then .SelectedIndex = 0
            End With
            If mdtPayPeriodEndDate = FinancialYear._Object._Database_End_Date Then
                lblPayPeriodOpen.Visible = False
                cboPayPeriodOpen.Visible = False
                'Sohail (21 Jul 2012) -- Start
                'TRA - ENHANCEMENT
                chkCopyPreviousEDSlab.Checked = False
                chkCopyPreviousEDSlab.Enabled = False
                'Sohail (21 Jul 2012) -- End
            Else
                lblPayPeriodOpen.Visible = True
                cboPayPeriodOpen.Visible = True
                chkCopyPreviousEDSlab.Enabled = True 'Sohail (21 Jul 2012)
            End If
            'Sohail (21 Jul 2012) -- Start
            'TRA - ENHANCEMENT
            If CInt(cboPayPeriodClose.SelectedValue) = 0 Then
                'chkCopyPreviousEDSlab.Checked = False
                chkCopyPreviousEDSlab.Enabled = False

                'Sohail (21 Aug 2015) -- Start
                'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                mdtPayPeriodEndDate = eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate)
                mdtPayPeriodEndDate = eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate)
                'Sohail (21 Aug 2015) -- End
            End If
            'Sohail (21 Jul 2012) -- End

            '*** Get List of Unprocessed employee ***
            lvEmployee.Items.Clear()


            If CInt(cboPayPeriodClose.SelectedValue) <= 0 Then Exit Try
            'Sohail (12 May 2012) -- Start
            'TRA - ENHANCEMENT
            'dsList = objPayroll.Get_UnProcessed_Employee("Employee", CInt(cboPayPeriodClose.SelectedValue)) 'Sohail (03 Nov 2010
            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'dsList = objPayroll.Get_UnProcessed_Employee("Employee", CInt(cboPayPeriodClose.SelectedValue), True)
            dsList = objPayroll.Get_UnProcessed_Employee(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, mdtPayPeriodStartDate, mdtPayPeriodEndDate, ConfigParameter._Object._UserAccessModeSetting, True, False, "Employee", CInt(cboPayPeriodClose.SelectedValue), True, False, "")
            'Sohail (21 Aug 2015) -- End
            'Sohail (12 May 2012) -- End
            For Each dsRow As DataRow In dsList.Tables("Employee").Rows
                lvItem = New ListViewItem
                lvItem.Text = dsRow.Item("employeecode").ToString
                lvItem.Tag = dsRow.Item("employeeunkid").ToString

                lvItem.SubItems.Add(dsRow.Item("employeename").ToString)

                lvEmployee.Items.Add(lvItem)
            Next
            'Sohail (04 Mar 2011) -- End

            'Anjan [14 Mar 2014] -- Start
            'ENHANCEMENT : Requested by Rutta
            lnEmployeeList.Text = Language.getMessage(mstrModuleName, 32, "Unprocessed Employee List") & " (" & lvEmployee.Items.Count.ToString & ") "
            'Anjan [14 Mar 2014 ] -- End

            'S.SANDEEP [ 15 April 2013 ] -- START
            'ENHANCEMENT : LICENSE CHANGES

            'Anjan (09 Mar 2012)-Start
            'Anjan (15 Dec 2012)-Start
            'ENHANCEMENT : TRA COMMENTS on Rutta's Request
            If ConfigParameter._Object._IsArutiDemo = False Then
                If Not ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Payroll_Management) Then
                    chkCopyPreviousEDSlab.Checked = False
                End If
            End If
            'Anjan (15 Dec 2012)-End 
            'Anjan (09 Mar 2012)-End

            'S.SANDEEP [ 15 April 2013 ] -- END

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboPayPeriodClose_SelectedIndexChanged", mstrModuleName)
        Finally
            objPeriod = Nothing
            dsList = Nothing
            objPayroll = Nothing
        End Try
    End Sub

    'Sohail (21 Jul 2012) -- Start
    'TRA - ENHANCEMENT
    Private Sub cboPayPeriodOpen_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboPayPeriodOpen.SelectedIndexChanged
        Dim objPeriod As New clscommom_period_Tran
        Try
            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'objPeriod._Periodunkid = CInt(cboPayPeriodOpen.SelectedValue)
            objPeriod._Periodunkid(FinancialYear._Object._DatabaseName) = CInt(cboPayPeriodOpen.SelectedValue)
            'Sohail (21 Aug 2015) -- End
            mdtNextPayPeriodStartDate = objPeriod._Start_Date
            mdtNextPayPeriodEndDate = objPeriod._End_Date
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboPayPeriodOpen_SelectedIndexChanged", mstrModuleName)
        Finally
            objPeriod = Nothing
        End Try
    End Sub
    'Sohail (21 Jul 2012) -- End
#End Region

#Region " Button's Events "
    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnClose_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnClosePeriod_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClosePeriod.Click
        Dim objPeriod As New clscommom_period_Tran
        'Dim objPayroll As New clsPayrollProcessTran 'Sohail (04 Mar 2011)
        Dim objLoginTran As New clslogin_Tran
        Dim objMaster As New clsMasterData
        Dim dsList As DataSet
        Dim dtTable As DataTable
        'Dim lvItem As ListViewItem 'Sohail (04 Mar 2011)
        Dim objTnALeave As New clsTnALeaveTran 'Sohail (14 Apr 2011)
        Dim strMsg As String = ""
        'Sohail (24 Sep 2012) -- Start
        'TRA - ENHANCEMENT
        Dim objED As New clsEarningDeduction
        Dim objSalary As New clsSalaryIncrement
        'Sohail (24 Sep 2012) -- End
        Dim objBatchPosting As New clsBatchPosting 'Sohail (05 Dec 2012)
        ' ArtLic._Object = New ArutiLic(False)
        objlblProgress.Text = "" 'Sohail (12 Dec 2015) 
        'Nilay (01-Apr-2016) -- Start
        'ENHANCEMENT - Approval Process in Loan Other Operations
        Dim objlnOtherOpApprovalTran As New clsloanotherop_approval_tran
        'Nilay (01-Apr-2016) -- End

        Try

            If IsValid() = False Then Exit Try

            'Gajanan [02-June-2020] -- Start
            'Enhancement Configure Option That Allow Period Closer Before XX Days.
            objPeriod._Periodunkid(FinancialYear._Object._DatabaseName) = CInt(cboPayPeriodClose.SelectedValue)
            If ConfigParameter._Object._AllowToClosePeriodBefore Then
                If objPeriod._Statusid = 1 AndAlso objPeriod._End_Date.Date > ConfigParameter._Object._CurrentDateAndTime.Date Then
                    If DateDiff(DateInterval.Day, ConfigParameter._Object._CurrentDateAndTime.Date, objPeriod._End_Date.Date) > CInt(ConfigParameter._Object._ClosePeriodDaysBefore) Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 47, "Sorry, You cannot close this period now. Reason: As per the company policy, you will able to close period on or after") & " " & objPeriod._End_Date.AddDays(CInt(ConfigParameter._Object._ClosePeriodDaysBefore) * -1).Date & ".", enMsgBoxStyle.Information)
                        Exit Sub
                    Else
                        If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 48, "Period end date is not reached yet, Are you sure you want to continue?"), CType(enMsgBoxStyle.Question + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.No Then
                            Exit Sub
                        End If
                    End If
                End If
            ElseIf objPeriod._End_Date.Date > ConfigParameter._Object._CurrentDateAndTime.Date Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 47, "Sorry, You cannot close this period now. Reason: As per the company policy, you will able to close period on or after") & " " & objPeriod._End_Date.Date & ".", enMsgBoxStyle.Information)
                Exit Sub
            End If
            'Gajanan [02-June-2020] -- End

            If CInt(cboPayPeriodClose.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Please select Pay Period to close. Pay Period is mandatory information."), enMsgBoxStyle.Information)
                cboPayPeriodClose.Focus()
                Exit Try
            End If

            'Sohail (04 Mar 2011) -- Start
            If cboPayPeriodOpen.Visible = True AndAlso CInt(cboPayPeriodOpen.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Please select Pay Period to be open. Pay Period is mandatory information."), enMsgBoxStyle.Information)
                cboPayPeriodOpen.Focus()
                Exit Try
            End If
            'Sohail (04 Mar 2011) -- End

            'Sohail (11 Sep 2010) -- Start
            '***Checking for previous year status (open or close)
            dsList = objMaster.Get_Database_Year_List("Year", False, Company._Object._Companyunkid)
            dtTable = New DataView(dsList.Tables("Year"), "end_date < '" & eZeeDate.convertDate(FinancialYear._Object._Database_End_Date) & "'", "", DataViewRowState.CurrentRows).ToTable
            If dtTable.Rows.Count > 0 Then
                strMsg = Language.getMessage(mstrModuleName, 18, "Sorry, You cannot do this process at this time. Reason : Previous year [") & dtTable.Rows(0).Item("financialyear_name").ToString & Language.getMessage(mstrModuleName, 4, "] is still open. Please close previous year first.")
                eZeeMsgBox.Show(strMsg, enMsgBoxStyle.Information)
                Exit Try
            End If
            'Sohail (11 Sep 2010) -- End

            '*** Checking Previous Period Status ***
            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'dsList = objPeriod.GetList("Period", enModuleReference.Payroll, True, enStatusType.Open)
            dsList = objPeriod.GetList("Period", enModuleReference.Payroll, FinancialYear._Object._YearUnkid, FinancialYear._Object._Database_Start_Date, True, enStatusType.Open)
            'Sohail (21 Aug 2015) -- End
            dtTable = New DataView(dsList.Tables("Period"), "end_date < '" & eZeeDate.convertDate(mdtPayPeriodEndDate) & "'", "end_date", DataViewRowState.CurrentRows).ToTable

            If dtTable.Rows.Count > 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "Sorry, Previous Period [") & dtTable.Rows(0).Item("period_name").ToString & Language.getMessage(mstrModuleName, 6, "] is still Open. Please Close Previous Period first."), enMsgBoxStyle.Information)
                cboPayPeriodClose.Focus()
                Exit Try
            End If

            '*** Check for any pending (not approved) Salary Increment for selected period.
            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'dsList = objSalary.GetList("Salary", , , CInt(cboPayPeriodClose.SelectedValue), , , , , , , enSalaryChangeApprovalStatus.Pending)
            dsList = objSalary.GetList(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, mdtPayPeriodStartDate, mdtPayPeriodEndDate, ConfigParameter._Object._UserAccessModeSetting, True, True, False, "Salary", Nothing, , CInt(cboPayPeriodClose.SelectedValue), 0, , , enSalaryChangeApprovalStatus.Pending)
            'Sohail (21 Aug 2015) -- End
            If dsList.Tables("Salary").Rows.Count > 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 23, "Sorry, Some Salary Changes are in Pending status (not Approved) for Selected Period. To perform payroll process, please Approve those Salary Changes."), enMsgBoxStyle.Information)
                Exit Try
            End If
            'Sohail (24 Sep 2012) -- End

            'Sohail (27 Apr 2016) -- Start
            'Enhancement - 61.1 - Priority Order on Grade Group, Grade and Grade Level; Auto Salary Increment to next Grade Level as per Priority Order; Grade filter on Wages Table; Export and Import for Wages Table for KBC.
            '#23 (KBC & Kenya Project Comments List.xls) - Auto Salary incrment Per employee Anniversary,Employee Anniversay,All KBC Employees have Annivesary date for Salary increment,date is sent  when employee is confimed ,the salary is done automatically by the system when annivesary is due from current level to next level and stops at the Last level (notch),unless changed to higher Grade

            'Nilay (26-Aug-2016) -- Start
            'dsList = objSalary.GetPendingEmployeeList(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, mdtPayPeriodStartDate, mdtPayPeriodEndDate, ConfigParameter._Object._UserAccessModeSetting, True, True, "AutoSalary", mdtPayPeriodEndDate, False, "")
            'If dsList.Tables("AutoSalary").Rows.Count > 0 Then
            '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 35, "Sorry, Auto Salary Increment is not done for some of the employees for this period.") & " [" & dsList.Tables("AutoSalary").Rows(0).Item("employeecode").ToString & " : " & dsList.Tables("AutoSalary").Rows(0).Item("employeename").ToString & "]" & vbCrLf & vbCrLf & _
            '                    Language.getMessage(mstrModuleName, 36, "Please do Auto Salary Increment for those employees whose Salary Increment is not done for this period."), enMsgBoxStyle.Information)
            '    Exit Try
            'End If
            'Sohail (22 Sep 2016) -- Start
            'Enhancement - 63.1 - Show pending auto salary increment employee list on close period.
            'If ConfigParameter._Object._IsAllowToClosePeriod = False Then
            '    dsList = objSalary.GetPendingEmployeeList(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, mdtPayPeriodStartDate, mdtPayPeriodEndDate, ConfigParameter._Object._UserAccessModeSetting, True, True, "AutoSalary", mdtPayPeriodEndDate, False, "")
            '    If dsList.Tables("AutoSalary").Rows.Count > 0 Then
            '        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 35, "Sorry, Auto Salary Increment is not done for some of the employees for this period.") & " [" & dsList.Tables("AutoSalary").Rows(0).Item("employeecode").ToString & " : " & dsList.Tables("AutoSalary").Rows(0).Item("employeename").ToString & "]" & vbCrLf & vbCrLf & _
            '                        Language.getMessage(mstrModuleName, 36, "Please do Auto Salary Increment for those employees whose Salary Increment is not done for this period."), enMsgBoxStyle.Information)
            '        Exit Try
            '    End If
            'End If
            'Sohail (22 Sep 2016) -- End
            'Nilay (26-Aug-2016) -- END

            'Sohail (27 Apr 2016) -- End

            'Nilay (01-Apr-2016) -- Start
            'ENHANCEMENT - Approval Process in Loan Other Operations
            If ConfigParameter._Object._IsArutiDemo = True Then

                Dim blnFlag As Boolean
                blnFlag = objlnOtherOpApprovalTran.IsLoanParameterApprovalPending(CInt(cboPayPeriodClose.SelectedValue))
                If blnFlag = False Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 34, "Sorry. Some of Operation(s) either Interest Rate/EMI Tenure/Topup Amount are still pending for current period Approval process. In order to close period, please either Approve or Reject pending operations for selected period."), enMsgBoxStyle.Information)
                    Exit Try
                End If

            Else
                If CBool(ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Loan_and_Savings_Management)) = True Then

                    Dim blnFlag As Boolean
                    blnFlag = objlnOtherOpApprovalTran.IsLoanParameterApprovalPending(CInt(cboPayPeriodClose.SelectedValue))
                    If blnFlag = False Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 34, "Sorry. Some of Operation(s) either Interest Rate/EMI Tenure/Topup Amount are still pending for current period Approval process. In order to close period, please either Approve or Reject pending operations for selected period."), enMsgBoxStyle.Information)
                        Exit Try
                    End If

                End If
            End If
            'Nilay (01-Apr-2016) -- End

            'Anjan (15 Dec 2012)-Start
            'ENHANCEMENT : TRA COMMENTS on Rutta's Request for checking payroll license coz this is set to general module.
            If ConfigParameter._Object._IsArutiDemo = True Then

                'Sohail (24 Sep 2012) -- Start
                'TRA - ENHANCEMENT
                '*** Check for any pending (not approved) transaction head on ED for selected period.
                'Sohail (21 Aug 2015) -- Start
                'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                'dsList = objED.GetList("ED", , , , , , , , , , , , , , enEDHeadApprovalStatus.Pending, , , , mdtPayPeriodEndDate)
                dsList = objED.GetList(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, mdtPayPeriodStartDate, mdtPayPeriodEndDate, ConfigParameter._Object._UserAccessModeSetting, True, ConfigParameter._Object._IsIncludeInactiveEmp, "ED", False, , , , enEDHeadApprovalStatus.Pending, , , mdtPayPeriodEndDate)
                'Sohail (21 Aug 2015) -- End
                If dsList.Tables("ED").Rows.Count > 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 26, "Sorry, Some Transaction Heads are in Pending status (not Approved) on Earning Deduction for Selected Period. To perform payroll process, please Approve those heads."), enMsgBoxStyle.Information)
                    Exit Try
                End If

                'Sohail (05 Dec 2012) -- Start
                'TRA - ENHANCEMENT
                'Sohail (21 Aug 2015) -- Start
                'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                'dsList = objBatchPosting.GetList("BatchPosting", "", CInt(cboPayPeriodClose.SelectedValue), , , enEDBatchPostingStatus.Not_Posted)
                'Sohail (24 Feb 2016) -- Start
                'Enhancement - Batch Posting changes as per online doc in 58.1 SP. (#15 (KBC & Kenya Project Comments List.xls))
                'dsList = objBatchPosting.GetList(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, mdtPayPeriodStartDate, mdtPayPeriodEndDate, ConfigParameter._Object._UserAccessModeSetting, True, ConfigParameter._Object._IsIncludeInactiveEmp, "BatchPosting", False, "", CInt(cboPayPeriodClose.SelectedValue), , , enEDBatchPostingStatus.Not_Posted)
                dsList = objBatchPosting.GetList(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, mdtPayPeriodStartDate, mdtPayPeriodEndDate, ConfigParameter._Object._UserAccessModeSetting, True, ConfigParameter._Object._IsIncludeInactiveEmp, "BatchPosting", False, 0, CInt(cboPayPeriodClose.SelectedValue), , , enEDBatchPostingStatus.Not_Posted)
                'Sohail (24 Feb 2016) -- End
                'Sohail (21 Aug 2015) -- End
                If dsList.Tables("BatchPosting").Rows.Count > 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 24, "Sorry, Some Transaction Heads are in Pending status (not Posted to ED) on Batch Posting for Selected Period. To perform payroll process, please Post those heads."), enMsgBoxStyle.Information)
                    Exit Try
                End If
                'Sohail (05 Dec 2012) -- End

                'Sohail (02 Jul 2012) -- Start
                'TRA - ENHANCEMENT
                Dim objPaymentTran As New clsPayment_tran
                'Sohail (21 Aug 2015) -- Start
                'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                'dsList = objPaymentTran.GetList("List", clsPayment_tran.enPaymentRefId.PAYSLIP, 0, clsPayment_tran.enPayTypeId.PAYMENT, "prpayment_tran.periodunkid = " & CInt(cboPayPeriodClose.SelectedValue) & " AND ISNULL(isauthorized, 0) = 0 ")
                dsList = objPaymentTran.GetList(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, mdtPayPeriodStartDate, mdtPayPeriodEndDate, ConfigParameter._Object._UserAccessModeSetting, True, ConfigParameter._Object._IsIncludeInactiveEmp, "List", clsPayment_tran.enPaymentRefId.PAYSLIP, 0, clsPayment_tran.enPayTypeId.PAYMENT, False, "prpayment_tran.periodunkid = " & CInt(cboPayPeriodClose.SelectedValue) & " AND ISNULL(isauthorized, 0) = 0 ")
                'Sohail (21 Aug 2015) -- End
                If dsList.Tables("List").Rows.Count > 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 25, "Sorry, Some payslip payment(s) are not Authorized yet. Please Authorize payslip payment."), enMsgBoxStyle.Information)
                    cboPayPeriodClose.Focus()
                    Exit Try
                End If
                'Sohail (02 Jul 2012) -- End

                If lvEmployee.Items.Count > 0 Then
                    'strMsg = "Payroll Process is not done yet for some of the employees in this period. If you close this period, you will not be able to process payroll for this period in future. " & _
                    '"It is recommended that first process payroll for the listed employee before closing this period." & vbCrLf & vbCrLf & _
                    '"Do you stiil want to close this period?"
                    strMsg = Language.getMessage(mstrModuleName, 8, "Sorry, Payroll Process is not done yet for the date ") & mdtPayPeriodEndDate.ToShortDateString & Language.getMessage(mstrModuleName, 9, " for some of the employees in this period.") & vbCrLf & _
                    Language.getMessage(mstrModuleName, 10, "It is recommended that first process payroll for the listed employee for the date ") & mdtPayPeriodEndDate.ToShortDateString & Language.getMessage(mstrModuleName, 11, " before closing this period.") 'Sohail (04 Mar 2011)

                    'If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, strMsg), CType(enMsgBoxStyle.Question + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.Yes Then
                    'objPeriod._Periodunkid = CInt(cboPayPeriodClose.SelectedValue)
                    'objPeriod._Statusid = enStatusType.Close
                    'objPeriod.Update()

                    'Call FillCombo()
                    'End If
                    'Sohail (24 Jun 2019) -- Start
                    'NMB Enhancement - REF # - 76.1 - TC012: There should be payroll authorization check before payroll batch posting to flex cube.
                    'eZeeMsgBox.Show(strMsg, enMsgBoxStyle.Information)
                    If eZeeMsgBox.Show(strMsg & vbCrLf & vbCrLf & Language.getMessage(mstrModuleName, 46, "Do you want to process Payroll?"), CType(enMsgBoxStyle.Information + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.Yes Then
                        Dim objFrm As New frmProcessPayroll
                        objFrm.ShowDialog()
                    End If
                    'Sohail (24 Jun 2019) -- End
                    Exit Try
                End If


                lvEmployee.Items.Clear()

                'Sohail (14 Apr 2011) -- Start
                'Sohail (21 Aug 2015) -- Start
                'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                'dsList = objTnALeave.Get_Balance_List("Balance", , , CInt(cboPayPeriodClose.SelectedValue), "")
                dsList = objTnALeave.Get_Balance_List(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, mdtPayPeriodStartDate, mdtPayPeriodEndDate, ConfigParameter._Object._UserAccessModeSetting, True, ConfigParameter._Object._IsIncludeInactiveEmp, False, "", "Balance", , CInt(cboPayPeriodClose.SelectedValue), "")
                'Sohail (21 Aug 2015) -- End
                dtTable = New DataView(dsList.Tables("Balance"), "balanceamount > 0", "", DataViewRowState.CurrentRows).ToTable
                If dtTable.Rows.Count > 0 Then
                    'Sohail (21 Mar 2013) -- Start
                    'TRA - ENHANCEMENT
                    'If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 20, "Some payslip's are still unpaid in this period.") & vbCrLf & vbCrLf & Language.getMessage(mstrModuleName, 12, "Do you want to proceed?"), CType(enMsgBoxStyle.YesNo + enMsgBoxStyle.Question, enMsgBoxStyle)) = Windows.Forms.DialogResult.No Then
                    '    Exit Try
                    'End If
                    If ConfigParameter._Object._SetPaySlipPaymentMandatory = True Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 27, "Sorry, Some payslip's are still unpaid in this period. Please make payment for unpaid payslip."), enMsgBoxStyle.Information)
                        Exit Try
                    Else
                        If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 20, "Some payslip's are still unpaid in this period.") & vbCrLf & vbCrLf & Language.getMessage(mstrModuleName, 12, "Do you want to proceed?"), CType(enMsgBoxStyle.YesNo + enMsgBoxStyle.Question, enMsgBoxStyle)) = Windows.Forms.DialogResult.No Then
                            Exit Try
                        End If
                    End If
                    'Sohail (21 Mar 2013) -- End
                End If
                'Sohail (14 Apr 2011) -- End
            Else
                If ConfigParameter._Object._IsArutiDemo = True OrElse ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Payroll_Management) Then 'Anjan (09 Mar 2012)-Start
                    'Sohail (24 Sep 2012) -- Start
                    'TRA - ENHANCEMENT
                    '*** Check for any pending (not approved) transaction head on ED for selected period.
                    'Sohail (21 Aug 2015) -- Start
                    'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                    'dsList = objED.GetList("ED", , , , , , , , , , , , , , enEDHeadApprovalStatus.Pending, , , , mdtPayPeriodEndDate)
                    dsList = objED.GetList(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, mdtPayPeriodStartDate, mdtPayPeriodEndDate, ConfigParameter._Object._UserAccessModeSetting, True, ConfigParameter._Object._IsIncludeInactiveEmp, "ED", False, , , , enEDHeadApprovalStatus.Pending, , , mdtPayPeriodEndDate)
                    'Sohail (21 Aug 2015) -- End
                    If dsList.Tables("ED").Rows.Count > 0 Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 26, "Sorry, Some Transaction Heads are in Pending status (not Approved) on Earning Deduction for Selected Period. To perform payroll process, please Approve those heads."), enMsgBoxStyle.Information)
                        Exit Try
                    End If

                    'Sohail (05 Dec 2012) -- Start
                    'TRA - ENHANCEMENT
                    'Sohail (21 Aug 2015) -- Start
                    'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                    'dsList = objBatchPosting.GetList("BatchPosting", "", CInt(cboPayPeriodClose.SelectedValue), , , enEDBatchPostingStatus.Not_Posted)
                    'Sohail (24 Feb 2016) -- Start
                    'Enhancement - Batch Posting changes as per online doc in 58.1 SP. (#15 (KBC & Kenya Project Comments List.xls))
                    'dsList = objBatchPosting.GetList(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, mdtPayPeriodStartDate, mdtPayPeriodEndDate, ConfigParameter._Object._UserAccessModeSetting, True, ConfigParameter._Object._IsIncludeInactiveEmp, "BatchPosting", False, "", CInt(cboPayPeriodClose.SelectedValue), , , enEDBatchPostingStatus.Not_Posted)
                    dsList = objBatchPosting.GetList(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, mdtPayPeriodStartDate, mdtPayPeriodEndDate, ConfigParameter._Object._UserAccessModeSetting, True, ConfigParameter._Object._IsIncludeInactiveEmp, "BatchPosting", False, 0, CInt(cboPayPeriodClose.SelectedValue), , , enEDBatchPostingStatus.Not_Posted)
                    'Sohail (24 Feb 2016) -- End
                    'Sohail (21 Aug 2015) -- End
                    If dsList.Tables("BatchPosting").Rows.Count > 0 Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 24, "Sorry, Some Transaction Heads are in Pending status (not Posted to ED) on Batch Posting for Selected Period. To perform payroll process, please Post those heads."), enMsgBoxStyle.Information)
                        Exit Try
                    End If
                    'Sohail (05 Dec 2012) -- End

                    'Sohail (02 Jul 2012) -- Start
                    'TRA - ENHANCEMENT
                    Dim objPaymentTran As New clsPayment_tran
                    'Sohail (21 Aug 2015) -- Start
                    ''Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                    'dsList = objPaymentTran.GetList("List", clsPayment_tran.enPaymentRefId.PAYSLIP, 0, clsPayment_tran.enPayTypeId.PAYMENT, "prpayment_tran.periodunkid = " & CInt(cboPayPeriodClose.SelectedValue) & " AND ISNULL(isauthorized, 0) = 0 ")
                    dsList = objPaymentTran.GetList(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, mdtPayPeriodStartDate, mdtPayPeriodEndDate, ConfigParameter._Object._UserAccessModeSetting, True, ConfigParameter._Object._IsIncludeInactiveEmp, "List", clsPayment_tran.enPaymentRefId.PAYSLIP, 0, clsPayment_tran.enPayTypeId.PAYMENT, False, "prpayment_tran.periodunkid = " & CInt(cboPayPeriodClose.SelectedValue) & " AND ISNULL(isauthorized, 0) = 0 ")
                    'Sohail (21 Aug 2015) -- End
                    If dsList.Tables("List").Rows.Count > 0 Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 25, "Sorry, Some payslip payment(s) are not Authorized yet. Please Authorize payslip payment."), enMsgBoxStyle.Information)
                        cboPayPeriodClose.Focus()
                        Exit Try
                    End If
                    'Sohail (02 Jul 2012) -- End

                    If lvEmployee.Items.Count > 0 Then
                        'strMsg = "Payroll Process is not done yet for some of the employees in this period. If you close this period, you will not be able to process payroll for this period in future. " & _
                        '"It is recommended that first process payroll for the listed employee before closing this period." & vbCrLf & vbCrLf & _
                        '"Do you stiil want to close this period?"
                        strMsg = Language.getMessage(mstrModuleName, 8, "Sorry, Payroll Process is not done yet for the date ") & mdtPayPeriodEndDate.ToShortDateString & Language.getMessage(mstrModuleName, 9, " for some of the employees in this period.") & vbCrLf & _
                        Language.getMessage(mstrModuleName, 10, "It is recommended that first process payroll for the listed employee for the date ") & mdtPayPeriodEndDate.ToShortDateString & Language.getMessage(mstrModuleName, 11, " before closing this period.") 'Sohail (04 Mar 2011)

                        'If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, strMsg), CType(enMsgBoxStyle.Question + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.Yes Then
                        'objPeriod._Periodunkid = CInt(cboPayPeriodClose.SelectedValue)
                        'objPeriod._Statusid = enStatusType.Close
                        'objPeriod.Update()

                        'Call FillCombo()
                        'End If
                        eZeeMsgBox.Show(strMsg, enMsgBoxStyle.Information)
                        Exit Try
                    End If


                    lvEmployee.Items.Clear()

                    'Sohail (14 Apr 2011) -- Start
                    'Sohail (21 Aug 2015) -- Start
                    'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                    'dsList = objTnALeave.Get_Balance_List("Balance", , , CInt(cboPayPeriodClose.SelectedValue), "")
                    dsList = objTnALeave.Get_Balance_List(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, mdtPayPeriodStartDate, mdtPayPeriodEndDate, ConfigParameter._Object._UserAccessModeSetting, True, ConfigParameter._Object._IsIncludeInactiveEmp, False, "", "Balance", , CInt(cboPayPeriodClose.SelectedValue), "")
                    'Sohail (21 Aug 2015) -- End
                    dtTable = New DataView(dsList.Tables("Balance"), "balanceamount > 0", "", DataViewRowState.CurrentRows).ToTable
                    If dtTable.Rows.Count > 0 Then
                        'Sohail (21 Mar 2013) -- Start
                        'TRA - ENHANCEMENT
                        'If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 20, "Some payslip's are still unpaid in this period.") & vbCrLf & vbCrLf & Language.getMessage(mstrModuleName, 12, "Do you want to proceed?"), CType(enMsgBoxStyle.YesNo + enMsgBoxStyle.Question, enMsgBoxStyle)) = Windows.Forms.DialogResult.No Then
                        '    Exit Try
                        'End If
                        If ConfigParameter._Object._SetPaySlipPaymentMandatory = True Then
                            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 27, "Sorry, Some payslip's are still unpaid in this period. Please make payment for unpaid payslip."), enMsgBoxStyle.Information)
                            Exit Try
                        Else
                            If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 20, "Some payslip's are still unpaid in this period.") & vbCrLf & vbCrLf & Language.getMessage(mstrModuleName, 12, "Do you want to proceed?"), CType(enMsgBoxStyle.YesNo + enMsgBoxStyle.Question, enMsgBoxStyle)) = Windows.Forms.DialogResult.No Then
                                Exit Try
                            End If
                        End If
                        'Sohail (21 Mar 2013) -- End
                    End If
                    'Sohail (14 Apr 2011) -- End
                End If

            End If

            'Sohail (22 Sep 2016) -- Start
            'Enhancement - 63.1 - Show pending auto salary increment employee list on close period.
            If ConfigParameter._Object._IsAllowToClosePeriod = False Then
                dsList = objSalary.GetPendingEmployeeList(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, mdtPayPeriodStartDate, mdtPayPeriodEndDate, ConfigParameter._Object._UserAccessModeSetting, True, True, "AutoSalary", mdtPayPeriodEndDate, False, "")


                If dsList.Tables("AutoSalary").Rows.Count > 0 Then

                    lvEmployee.Items.Clear()
                    Dim lvItem As ListViewItem

                    For Each dsRow As DataRow In dsList.Tables("AutoSalary").Rows
                        lvItem = New ListViewItem
                        lvItem.Text = dsRow.Item("employeecode").ToString
                        lvItem.Tag = dsRow.Item("employeeunkid").ToString

                        lvItem.SubItems.Add(dsRow.Item("employeename").ToString)

                        lvEmployee.Items.Add(lvItem)
                    Next

                    lnEmployeeList.Text = Language.getMessage(mstrModuleName, 37, "Auto Salary Increment not done") & " (" & lvEmployee.Items.Count.ToString & ") "

                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 35, "Sorry, Auto Salary Increment is not done for some of the employees for this period.") & vbCrLf & vbCrLf & _
                                    Language.getMessage(mstrModuleName, 36, "Please do Auto Salary Increment for the listed employees whose Salary Increment is not done for this period."), enMsgBoxStyle.Information)
                    Exit Try
                End If
            End If
            'Sohail (22 Sep 2016) -- End

            'Sohail (23 Nov 2020) -- Start
            'NMB Enhancement # : Allow to do close period if Absent process is not done for NMB PLC only.
            Dim objCGroup As New clsGroup_Master
            objCGroup._Groupunkid = 1
            'Sohail (23 Nov 2020) -- End

            '<TO DO> need to set this as per new license policy when payroll and Tna license is not taken.
            If ConfigParameter._Object._IsArutiDemo = True Then
                'Sohail (07 Jan 2014) -- Start
                'Enhancement - Separate TnA Periods from Payroll Periods
                'If objLoginTran.Is_Absent_Processed(mdtPayPeriodStartDate, mdtPayPeriodEndDate) = False Then
                'Sohail (23 Nov 2020) -- Start
                'NMB Enhancement # : Allow to do close period if Absent process is not done for NMB PLC only.
                'If objLoginTran.Is_Absent_Processed(mdtTnAStartDate, mdtTnAEndDate) = False Then
                If objLoginTran.Is_Absent_Processed(mdtTnAStartDate, mdtTnAEndDate) = False AndAlso objCGroup._Groupname.ToUpper <> "NMB PLC" Then
                    'Sohail (23 Nov 2020) -- End
                    'Sohail (07 Jan 2014) -- End
                    'strMsg = "Absent Process is not done yet for this period. If you close this period, you will not be able to do Absent process and payroll process for this period in future. " & _
                    '"It is recommended that first do Absent process followed by payroll process for this period before closing this period." & vbCrLf & vbCrLf & _
                    '"Do you stiil want to close this period?"
                    strMsg = Language.getMessage(mstrModuleName, 7, "Sorry, Absent Process is not done yet for this period.It is recommended that first do Absent process from Time and Attendance Module followed by payroll process for this period before closing this period.")

                    'If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, strMsg), CType(enMsgBoxStyle.Question + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.No Then
                    '    Exit Try
                    'End If
                    eZeeMsgBox.Show(strMsg, enMsgBoxStyle.Information)
                    Exit Try
                End If
            Else
                If ConfigParameter._Object._IsArutiDemo = True OrElse ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Payroll_Management) OrElse ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Time_and_Attendance_Management) Then ''Anjan (09 Mar 2012)-Start
                    '*** Checking for Absent Process by checking absent count in the period ***
                    'Sohail (07 Jan 2014) -- Start
                    'Enhancement - Separate TnA Periods from Payroll Periods
                    'If objLoginTran.Is_Absent_Processed(mdtPayPeriodStartDate, mdtPayPeriodEndDate) = False Then
                    'Sohail (23 Nov 2020) -- Start
                    'NMB Enhancement # : Allow to do close period if Absent process is not done for NMB PLC only.
                    'If objLoginTran.Is_Absent_Processed(mdtTnAStartDate, mdtTnAEndDate) = False Then
                    If objLoginTran.Is_Absent_Processed(mdtTnAStartDate, mdtTnAEndDate) = False AndAlso objCGroup._Groupname.ToUpper <> "NMB PLC" Then
                        'Sohail (23 Nov 2020) -- End
                        'Sohail (07 Jan 2014) -- End
                        'strMsg = "Absent Process is not done yet for this period. If you close this period, you will not be able to do Absent process and payroll process for this period in future. " & _
                        '"It is recommended that first do Absent process followed by payroll process for this period before closing this period." & vbCrLf & vbCrLf & _
                        '"Do you stiil want to close this period?"
                        strMsg = Language.getMessage(mstrModuleName, 7, "Sorry, Absent Process is not done yet for this period.It is recommended that first do Absent process from Time and Attendance Module followed by payroll process for this period before closing this period.")

                        'If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, strMsg), CType(enMsgBoxStyle.Question + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.No Then
                        '    Exit Try
                        'End If
                        eZeeMsgBox.Show(strMsg, enMsgBoxStyle.Information)
                        Exit Try
                    End If

                End If
            End If


            'Anjan (15 Dec 2012)-End 

            'Sohail (04 Mar 2011) -- Start
            '*** Get_UnProcessed_Employee on pay period selected_indexchage
            ''*** Get List of Unprocessed employee ***
            'dsList = objPayroll.Get_UnProcessed_Employee("Employee", CInt(cboPayPeriodClose.SelectedValue)) 'Sohail (03 Nov 2010
            'For Each dsRow As DataRow In dsList.Tables("Employee").Rows
            '    lvItem = New ListViewItem
            '    lvItem.Text = dsRow.Item("employeecode").ToString
            '    lvItem.Tag = dsRow.Item("employeeunkid").ToString

            '    lvItem.SubItems.Add(dsRow.Item("employeename").ToString)

            '    lvEmployee.Items.Add(lvItem)
            'Next
            'Sohail (04 Mar 2011) -- End




            'Sohail (04 Mar 2011) -- Start
            'Dim objTnALeave As New clsTnALeaveTran
            'dsList = objTnALeave.Get_Balance_List("NegativeBal", , , CInt(cboPayPeriodClose.SelectedValue), )
            'dtTable = New DataView(dsList.Tables("NegativeBal"), "total_amount <= 0", "", DataViewRowState.CurrentRows).ToTable
            'For Each dtRow As DataRow In dtTable.Rows
            '    lvItem = New ListViewItem
            '    lvItem.Text = dtRow.Item("employeecode").ToString
            '    lvItem.Tag = dtRow.Item("employeeunkid").ToString

            '    lvItem.SubItems.Add(dtRow.Item("employeename").ToString)

            '    lvEmployee.Items.Add(lvItem)
            'Next
            'objTnALeave = Nothing

            'If lvEmployee.Items.Count > 0 Then
            '    strMsg = "Sorry, Payment is not done yet for the Negative Balance for some of the employees." & vbCrLf & _
            '                "It is recommended that first make payment for the listed employees before closing this period."

            '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 11, strMsg), enMsgBoxStyle.Information)
            '    Exit Try
            'End If
            'Sohail (04 Mar 2011) -- End



            'Anjan (15 Dec 2012)-Start
            'ENHANCEMENT : TRA COMMENTS on Rutta's Request
            '<TO DO> need to set this as per new license policy when Loan and Savings license is taken.
            If ConfigParameter._Object._IsArutiDemo = True Then


                'Pinkal (15-Sep-2013) -- Start
                'Enhancement : TRA Changes
                If ConfigParameter._Object._ClosePayrollPeriodIfLeaveIssue Then
                    Dim objLeaveForm As New clsleaveform
                    dsList = objLeaveForm.GetPendingLeaveFormForPeriod(mdtPayPeriodStartDate, mdtPayPeriodEndDate, False)
                    If dsList.Tables(0).Rows.Count > 0 Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 30, "Some Employee(s) leave form Start Date falls in this Period and their leave form(s) are in pending or approved status.Please issue or reject or reschedule leave form."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                        Exit Try
                    End If
                End If

                'Pinkal (15-Sep-2013) -- End

                'Sohail (23 Jan 2012) -- Start
                'TRA - ENHANCEMENT
                Dim objSaving As New clsSaving_Tran

                'Shani(24-Aug-2015) -- Start
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                'Change Saving GetList method for get active Employee saving
                'dsList = objSaving.GetList("Saving", , , , , mdtPayPeriodStartDate, mdtPayPeriodEndDate)
                dsList = objSaving.GetList(FinancialYear._Object._DatabaseName, _
                                           User._Object._Userunkid, _
                                           FinancialYear._Object._YearUnkid, _
                                           Company._Object._Companyunkid, _
                                           mdtPayPeriodStartDate, mdtPayPeriodEndDate, _
                                           ConfigParameter._Object._UserAccessModeSetting, _
                                           ConfigParameter._Object._IsIncludeInactiveEmp, "Saving", , , , , , , )
                'Shani(24-Aug-2015) -- End

                dtTable = New DataView(dsList.Tables("Saving"), "stopdate >= '" & eZeeDate.convertDate(mdtPayPeriodStartDate) & "' AND stopdate <= '" & eZeeDate.convertDate(mdtPayPeriodEndDate) & "' AND savingstatus <> 4 ", "", DataViewRowState.CurrentRows).ToTable
                If dtTable.Rows.Count > 0 Then
                    If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 21, "Some Employee(s) Saving Stop Date falls in this Period and their Saving will set to completed status.") & vbCrLf & vbCrLf & Language.getMessage(mstrModuleName, 28, "Do you want to continue?"), CType(enMsgBoxStyle.YesNo + enMsgBoxStyle.Question, enMsgBoxStyle)) = Windows.Forms.DialogResult.No Then
                        Exit Try
                    End If
                End If
                'Sohail (23 Jan 2012) -- End

            Else
                'Pinkal (15-Sep-2013) -- Start
                'Enhancement : TRA Changes

                If ConfigParameter._Object._IsArutiDemo = True OrElse ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Leave_Management) Then
                    If ConfigParameter._Object._ClosePayrollPeriodIfLeaveIssue Then
                        Dim objLeaveForm As New clsleaveform
                        dsList = objLeaveForm.GetPendingLeaveFormForPeriod(mdtPayPeriodStartDate, mdtPayPeriodEndDate, False)
                        If dsList.Tables(0).Rows.Count > 0 Then
                            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 30, "Some Employee(s) leave form Start Date falls in this Period and their leave form(s) are in pending or approved status.Please issue or reject or reschedule leave form."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                            Exit Try
                        End If
                    End If

                    'Pinkal (15-Sep-2013) -- End

                ElseIf ConfigParameter._Object._IsArutiDemo = True OrElse ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Payroll_Management) Then 'Anjan (09 Mar 2012)-Start
                    'Sohail (23 Jan 2012) -- Start
                    'TRA - ENHANCEMENT
                    Dim objSaving As New clsSaving_Tran
                    'Shani(24-Aug-2015) -- Start
                    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                    'Change Saving GetList method for get active Employee saving
                    'dsList = objSaving.GetList("Saving", , , , , mdtPayPeriodStartDate, mdtPayPeriodEndDate)
                    dsList = objSaving.GetList(FinancialYear._Object._DatabaseName, _
                                               User._Object._Userunkid, _
                                               FinancialYear._Object._YearUnkid, _
                                               Company._Object._Companyunkid, _
                                               mdtPayPeriodStartDate, mdtPayPeriodEndDate, _
                                               ConfigParameter._Object._UserAccessModeSetting, _
                                               ConfigParameter._Object._IsIncludeInactiveEmp, "Saving", , , , , , , )
                    'Shani(24-Aug-2015) -- End

                    dtTable = New DataView(dsList.Tables("Saving"), "stopdate >= '" & eZeeDate.convertDate(mdtPayPeriodStartDate) & "' AND stopdate <= '" & eZeeDate.convertDate(mdtPayPeriodEndDate) & "' AND savingstatus <> 4 ", "", DataViewRowState.CurrentRows).ToTable
                    If dtTable.Rows.Count > 0 Then
                        If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 21, "Some Employee(s) Saving Stop Date falls in this Period and their Saving will set to completed status.") & vbCrLf & vbCrLf & Language.getMessage(mstrModuleName, 28, "Do you want to continue?"), CType(enMsgBoxStyle.YesNo + enMsgBoxStyle.Question, enMsgBoxStyle)) = Windows.Forms.DialogResult.No Then
                            Exit Try
                        End If
                    End If
                    'Sohail (23 Jan 2012) -- End

                End If


            End If
            'Anjan (15 Dec 2012)-End 

            'Sohail (17 Sep 2014) -- Start
            'Enhancement - System generated Base, OT and PH heads for PPA Activity.
            If ConfigParameter._Object._IsArutiDemo = True Then
                Dim objPPA As New clsPayActivity_Tran
                dsList = objPPA.GetList("PPA", , CInt(cboPayPeriodClose.SelectedValue), , , , False, False)
                If dsList.Tables("PPA").Rows.Count > 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 33, "Sorry, Some PAY PER ACTIVITY transactions are in Pending status (not Posted to Payroll) for Selected Period. To perform payroll process, please Post those transactions."), enMsgBoxStyle.Information)
                    Exit Try
                End If
                objPPA = Nothing
            Else
                If ConfigParameter._Object._IsArutiDemo = True OrElse ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Payroll_Management) Then
                    Dim objPPA As New clsPayActivity_Tran
                    dsList = objPPA.GetList("PPA", , CInt(cboPayPeriodClose.SelectedValue), , , , False, False)
                    If dsList.Tables("PPA").Rows.Count > 0 Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 33, "Sorry, Some PAY PER ACTIVITY transactions are in Pending status (not Posted to Payroll) for Selected Period. To perform payroll process, please Post those transactions."), enMsgBoxStyle.Information)
                        Exit Try
                    End If
                    objPPA = Nothing
                End If
            End If
            'Sohail (17 Sep 2014) -- End

            'Sohail (05 Oct 2016) -- Start
            'Enhancement - 63.1 - Common Function to check if Aruti is running on other machines.
            Dim strHostNames As String = ""
            Dim strSPIDs As String = ""
            If IsArutiRunningOnOtherMachines(FinancialYear._Object._DatabaseName, strHostNames, strSPIDs) = True Then
                If (eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 38, "Sorry, You cannot close this Period now.") & vbCrLf & Language.getMessage(mstrModuleName, 39, "Reason : The Period to be closed is in use on ") & strHostNames & " " & Language.getMessage(mstrModuleName, 40, "Machine(s).") & vbCrLf & vbCrLf & Language.getMessage(mstrModuleName, 41, "Do you want system to close aruti forcefully on that machine(s)?"), CType(enMsgBoxStyle.Question + enMsgBoxStyle.YesNo, enMsgBoxStyle))) = Windows.Forms.DialogResult.No Then
                    Exit Try
                Else
                    If Kill_SPIDs(strSPIDs) = False Then
                        Exit Try
                    End If
                End If
            End If
            'Sohail (05 Oct 2016) -- End


            'Pinkal (13 Jun 2017) -- Start
            'ISSUE : AFK HAVING ISSUE THAT THEY DON'T ABLE TO CLOSE PAYROLL PERIOD.
            If ConfigParameter._Object._IsArutiDemo = True OrElse ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Payroll_Management) Then

                'Pinkal (16-May-2018) -- Start
                'Enhancement - Display Pending Budget timesheet Approval Employeee List Suggestion given by Suzan.

                Dim objBudgetTimesheet As New clsBudgetEmp_timesheet
                Dim dtEmpPending As DataTable = Nothing

                If CBool(ConfigParameter._Object._NotAllowClosePeriodIncompleteSubmitApproval) = True Then
                    dtEmpPending = objBudgetTimesheet.GetEmployeeFromPendingBudgetTimesheet(CInt(cboPayPeriodClose.SelectedValue), False)
                    If dtEmpPending.Rows.Count > 0 Then
                        lvEmployee.Items.Clear()
                        Dim lvItem As ListViewItem
                        For Each dsRow As DataRow In dtEmpPending.Rows
                            lvItem = New ListViewItem
                            lvItem.Text = dsRow.Item("employeecode").ToString
                            lvItem.Tag = dsRow.Item("employeeunkid").ToString
                            lvItem.SubItems.Add(dsRow.Item("employeename").ToString)
                            lvEmployee.Items.Add(lvItem)
                        Next
                        lnEmployeeList.Text = Language.getMessage(mstrModuleName, 44, "Budget timesheet submit for approval is not done") & " (" & lvEmployee.Items.Count.ToString & ") "

                        If objBudgetTimesheet.IsAllTimesheetSubmittedForApproval(CInt(cboPayPeriodClose.SelectedValue)) = False Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 42, "Some of the Budget Employee Timesheets are not submitted for approval yet."), enMsgBoxStyle.Information)
                        Exit Sub
                    End If
                End If
                End If

                If objBudgetTimesheet.IsPendingApprovalBudgetTimesheet(CInt(cboPayPeriodClose.SelectedValue)) = False Then
                    dtEmpPending = objBudgetTimesheet.GetEmployeeFromPendingBudgetTimesheet(CInt(cboPayPeriodClose.SelectedValue), True)
                    If dtEmpPending.Rows.Count > 0 Then
                        lvEmployee.Items.Clear()
                        Dim lvItem As ListViewItem
                        For Each dsRow As DataRow In dtEmpPending.Rows
                            lvItem = New ListViewItem
                            lvItem.Text = dsRow.Item("employeecode").ToString
                            lvItem.Tag = dsRow.Item("employeeunkid").ToString
                            lvItem.SubItems.Add(dsRow.Item("employeename").ToString)
                            lvEmployee.Items.Add(lvItem)
                        Next
                        lnEmployeeList.Text = Language.getMessage(mstrModuleName, 45, "Budget timesheet final approval is not done") & " (" & lvEmployee.Items.Count.ToString & ") "
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 43, "Some of the Budget Employee Timesheets are not final approved yet."), enMsgBoxStyle.Information)
                    Exit Sub
                End If
            End If
                'Pinkal (16-May-2018) -- End
            End If
            'Pinkal (13 Jun 2017) -- End

            'Sohail (18 Jan 2012) -- Start
            'TRA - ENHANCEMENT
            If chkCopyPreviousEDSlab.Checked = True Then
                If (eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 22, "Copy Previous ED Slab is ticked. This will copy Previous Period Slab.") & vbCrLf & vbCrLf & Language.getMessage(mstrModuleName, 28, "Do you want to continue?"), CType(enMsgBoxStyle.Question + enMsgBoxStyle.YesNo, enMsgBoxStyle))) = Windows.Forms.DialogResult.No Then
                    Exit Try
                End If
            End If
            'Sohail (18 Jan 2012) -- End

            'Sohail (28 Dec 2010) -- Start
            If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 13, "It is Recommended to take backup of database before closing period. ") & vbCrLf & vbCrLf & Language.getMessage(mstrModuleName, 29, " Are you sure want to Close this Period?"), CType(enMsgBoxStyle.YesNo + enMsgBoxStyle.Question, enMsgBoxStyle)) = Windows.Forms.DialogResult.No Then
                Exit Try
            End If
            'Sohail (28 Dec 2010) -- End

            ''Nilay (27 Feb 2017) -- Start
            ''ISSUE #23: Enhancements: Budget Employee TimeSheet Changes
            'If ConfigParameter._Object._IsArutiDemo = True OrElse ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Payroll_Management) Then
            '    Dim objBusgetTimesheet As New clsBudgetEmp_timesheet
            '    If CBool(ConfigParameter._Object._NotAllowClosePeriodIncompleteSubmitApproval) = True Then
            '        If objBusgetTimesheet.IsAllTimesheetSubmittedForApproval(CInt(cboPayPeriodClose.SelectedValue)) = False Then
            '            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 42, "Some of the Budget Employee Timesheets are not submitted for approval yet."), enMsgBoxStyle.Information)
            '            Exit Sub
            '        End If
            '    End If
            '    If objBusgetTimesheet.IsPendingApprovalBudgetTimesheet(CInt(cboPayPeriodClose.SelectedValue)) = False Then
            '        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 43, "Some of the Budget Employee Timesheets are not final approved yet."), enMsgBoxStyle.Information)
            '        Exit Sub
            '    End If
            'End If
            ''Nilay (27 Feb 2017) -- End

            'Sohail (20 Feb 2012) -- Start
            'TRA - ENHANCEMENT
            'Dim objConfig = New clsConfigOptions
            'objConfig._Companyunkid = Company._Object._Companyunkid
            'If objConfig._DatabaseExportPath = "" Then
            '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 21, "Database does " & vbCrLf & _
            '                             "not have set the backup path to take backup. " & vbCrLf & _
            '                             "Backup of that database(s) cannot be taken." & vbCrLf & _
            '                             "You can set the Database backup path from " & vbCrLf & _
            '                             "Aruti Configuration -> Option -> Path."))

            '    Exit Sub
            'End If
            'Sohail (20 Feb 2012) -- End

            'Sohail (04 Mar 2011) -- Start
            Cursor.Current = Cursors.WaitCursor

            'Sohail (20 Feb 2012) -- Start
            'TRA - ENHANCEMENT
            'Dim objAppSettings As New clsApplicationSettings
            'Dim strFinalPath As String = String.Empty
            'Dim StrFilePath As String = objAppSettings._ApplicationPath & "Data\Close Period Backup"
            'objAppSettings = Nothing

            'If Not System.IO.Directory.Exists(StrFilePath) Then
            '    System.IO.Directory.CreateDirectory(StrFilePath)
            'End If

            'For i As Integer = 0 To 1
            '    If i = 0 Then
            '        eZeeDatabase.change_database(FinancialYear._Object._ConfigDatabaseName)
            '        System.IO.Directory.CreateDirectory(StrFilePath & "\" & FinancialYear._Object._ConfigDatabaseName)
            '        strFinalPath = StrFilePath & "\" & FinancialYear._Object._ConfigDatabaseName
            '    ElseIf i = 1 Then
            '        eZeeDatabase.change_database(FinancialYear._Object._DatabaseName)
            '        System.IO.Directory.CreateDirectory(StrFilePath & "\" & FinancialYear._Object._DatabaseName)
            '        strFinalPath = StrFilePath & "\" & FinancialYear._Object._DatabaseName
            '    End If

            '    strFinalPath = objBackupDatabase.Backup(strFinalPath)

            '    Dim objBackup As New clsBackup
            '    If System.IO.File.Exists(strFinalPath) Then

            '        objBackup._Backup_Date = ConfigParameter._Object._CurrentDateAndTime
            '        objBackup._Backup_Path = strFinalPath
            '        If i = 0 Then
            '            objBackup._Companyunkid = -1
            '            objBackup._Yearunkid = -1
            '            objBackup._Isconfiguration = True
            '        ElseIf i = 1 Then
            '            objBackup._Companyunkid = Company._Object._Companyunkid
            '            objBackup._Yearunkid = FinancialYear._Object._YearUnkid
            '            objBackup._Isconfiguration = False
            '        End If
            '        objBackup._Userunkid = User._Object._Userunkid

            '        Call objBackup.Insert()
            '    End If
            'Next
            'Sohail (20 Feb 2012) -- End

            'Sohail (12 Dec 2015) -- Start
            'Enhancement - Showing Progress of closing period.
            ''Sohail (02 May 2013) -- Start
            ''Taking backup on server from client machine on close period - ENHANCEMENT
            'Dim objAppSettings As New clsApplicationSettings
            'Dim strFinalPath As String = String.Empty
            'Dim StrFilePath As String = objAppSettings._ApplicationPath & "Data\Close Period Backup"

            'If objAppSettings._IsClient = 0 Then
            '    If Not System.IO.Directory.Exists(StrFilePath) Then
            '        System.IO.Directory.CreateDirectory(StrFilePath)
            '    End If
            '    objAppSettings = Nothing

            '    'Dim objGSettings As New clsGeneralSettings
            '    'objGSettings._Section = enArutiApplicatinType.Aruti_Payroll.ToString
            '    'objBackupDatabase.ServerName = objGSettings._ServerName
            '    'objGSettings = Nothing

            '    For i As Integer = 0 To 2
            '        If i = 0 Then
            '            eZeeDatabase.change_database(FinancialYear._Object._ConfigDatabaseName)
            '            If Not System.IO.Directory.Exists(StrFilePath & "\" & FinancialYear._Object._ConfigDatabaseName) Then
            '                System.IO.Directory.CreateDirectory(StrFilePath & "\" & FinancialYear._Object._ConfigDatabaseName)
            '            End If
            '            strFinalPath = StrFilePath & "\" & FinancialYear._Object._ConfigDatabaseName

            '        ElseIf i = 2 Then 'this database has to be always to be taken at last and i number has to be higher in order
            '            eZeeDatabase.change_database(FinancialYear._Object._DatabaseName)
            '            If Not System.IO.Directory.Exists(StrFilePath & "\" & FinancialYear._Object._DatabaseName) Then
            '                System.IO.Directory.CreateDirectory(StrFilePath & "\" & FinancialYear._Object._DatabaseName)
            '            End If
            '            strFinalPath = StrFilePath & "\" & FinancialYear._Object._DatabaseName

            '        ElseIf i = 1 Then
            '            Dim objBkUp As New clsBackup
            '            If objBkUp.Is_DatabasePresent("arutiimages") Then
            '                eZeeDatabase.change_database("arutiimages")
            '                If Not System.IO.Directory.Exists(StrFilePath & "\" & "arutiimages") Then
            '                    System.IO.Directory.CreateDirectory(StrFilePath & "\" & "arutiimages")
            '                End If
            '                strFinalPath = StrFilePath & "\" & "arutiimages"
            '            Else
            '                strFinalPath = ""
            '            End If
            '            objBkUp = Nothing
            '        End If

            '        If strFinalPath <> "" Then

            '            'S.SANDEEP [16 FEB 2015] -- START
            '            'strFinalPath = objBackupDatabase.Backup(strFinalPath)
            '            strFinalPath = objBackupDatabase.Backup(strFinalPath, General_Settings._Object._ServerName)
            '            'S.SANDEEP [16 FEB 2015] -- END

            '            Dim objBackup As New clsBackup
            '            If System.IO.File.Exists(strFinalPath) Then

            '                objBackup._Backup_Date = ConfigParameter._Object._CurrentDateAndTime
            '                objBackup._Backup_Path = strFinalPath
            '                If i = 0 Then
            '                    objBackup._Companyunkid = -1
            '                    objBackup._Yearunkid = -1
            '                    objBackup._Isconfiguration = True
            '                ElseIf i = 1 Then
            '                    objBackup._Companyunkid = Company._Object._Companyunkid
            '                    objBackup._Yearunkid = FinancialYear._Object._YearUnkid
            '                    objBackup._Isconfiguration = False
            '                End If
            '                objBackup._Userunkid = User._Object._Userunkid

            '                Call objBackup.Insert()
            '            End If

            '        End If
            '    Next
            'Else
            '    objAppSettings = Nothing
            'End If
            ''Sohail (02 May 2013) -- End

            'Dim objClosePeriod As New clsClosePeriod
            'objClosePeriod._PeriodUnkId = CInt(cboPayPeriodClose.SelectedValue)
            'objClosePeriod._NexPeriodUnkId = CInt(cboPayPeriodOpen.SelectedValue)
            'objClosePeriod._PeriodStartDate = mdtPayPeriodStartDate
            'objClosePeriod._PeriodEndDate = mdtPayPeriodEndDate
            ''Sohail (07 Jan 2014) -- Start
            ''Enhancement - Separate TnA Periods from Payroll Periods
            'objClosePeriod._TnAStartDate = mdtTnAStartDate
            'objClosePeriod._TnAEndDate = mdtTnAEndDate
            ''Sohail (07 Jan 2014) -- End

            ''Sohail (21 Jul 2012) -- Start
            ''TRA - ENHANCEMENT
            'objClosePeriod._NextPeriodStartDate = mdtNextPayPeriodStartDate
            'objClosePeriod._NextPeriodEndDate = mdtNextPayPeriodEndDate
            'objClosePeriod._CopyClosingPeriodEDSlab = chkCopyPreviousEDSlab.Checked
            ''Sohail (21 Jul 2012) -- End
            ''Sohail (21 Aug 2015) -- Start
            ''Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            ''If objClosePeriod.Close_Period() = False Then
            'If objClosePeriod.Close_Period(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, mdtPayPeriodStartDate, mdtPayPeriodEndDate, ConfigParameter._Object._UserAccessModeSetting, True, ConfigParameter._Object._IsIncludeInactiveEmp, User._Object.Privilege._AllowToApproveEarningDeduction, ConfigParameter._Object._CurrentDateAndTime, FinancialYear._Object._Database_Start_Date, FinancialYear._Object._Database_End_Date, False, "") = False Then
            '    'Sohail (21 Aug 2015) -- End
            '    objClosePeriod = Nothing
            '    Exit Try
            'Else
            '    objClosePeriod = Nothing
            'End If
            ''Sohail (04 Mar 2011) -- End

            ''Sohail (21 Aug 2015) -- Start
            ''Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            ''objPeriod._Periodunkid = CInt(cboPayPeriodClose.SelectedValue)
            'objPeriod._Periodunkid(FinancialYear._Object._DatabaseName) = CInt(cboPayPeriodClose.SelectedValue)
            ''Sohail (21 Aug 2015) -- End
            'objPeriod._Statusid = enStatusType.Close
            'objPeriod._Closeuserunkid = User._Object._Userunkid 'Sohail (19 Nov 2010)
            'objPeriod.Update()

            'Call FillCombo()

            'Cursor.Current = Cursors.Default 'Sohail (04 Mar 2011)
            'eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 14, "Pay Period Closed successfully."), enMsgBoxStyle.Information) 'Sohail (28 Dec 2010)

            objbgwClosePayroll.RunWorkerAsync()
            'Sohail (12 Dec 2015) -- End
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnClosePeriod_Click", mstrModuleName)
        Finally
            'objPayroll = Nothing 'Sohail (04 Mar 2011)
            objPeriod = Nothing
            objLoginTran = Nothing
            objMaster = Nothing
            dtTable = Nothing
            dsList = Nothing
            'Sohail (24 Sep 2012) -- Start
            'TRA - ENHANCEMENT
            objED = Nothing
            objSalary = Nothing
            'Sohail (24 Sep 2012) -- End
            If eZeeDatabase.current_database() <> FinancialYear._Object._DatabaseName Then eZeeDatabase.change_database(FinancialYear._Object._DatabaseName) 'Sohail (12 Dec 2015) 
            Cursor.Current = Cursors.Default 'Sohail (04 Mar 2011)
        End Try
    End Sub

    Private Sub btnCloseYear_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCloseYear.Click
        Dim objPeriod As New clscommom_period_Tran
        Dim objMaster As New clsMasterData
        Dim dsList As DataSet
        Dim dtTable As DataTable
        Dim strMsg As String = ""
        Try
            lvEmployee.Items.Clear()

            If IsValid() = False Then Exit Try

            'Compare current date and year end date
            If DateTime.Today < mdtPayYearEndDate Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 15, "Sorry, You can not close this year now. Reason : The ending date of this year is ") & mdtPayYearEndDate.ToShortDateString & ".", enMsgBoxStyle.Information)
                Exit Try
            End If

            '*** Checking All Period Status ***
            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'dsList = objPeriod.GetList("Period", enModuleReference.Payroll, True, enStatusType.Open)
            dsList = objPeriod.GetList("Period", enModuleReference.Payroll, FinancialYear._Object._YearUnkid, FinancialYear._Object._Database_Start_Date, True, enStatusType.Open)
            'Sohail (21 Aug 2015) -- End
            dtTable = New DataView(dsList.Tables("Period"), "end_date <= '" & eZeeDate.convertDate(mdtPayYearEndDate) & "'", "end_date", DataViewRowState.CurrentRows).ToTable

            If dtTable.Rows.Count > 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 16, "Sorry, the Period [") & dtTable.Rows(0).Item("period_name").ToString & _
                                Language.getMessage(mstrModuleName, 17, "] is still Open. Please Close this Period first before closing Year."), enMsgBoxStyle.Information)
                cboPayPeriodClose.Focus()
                Exit Try
            End If

            'Sohail (09 Nov 2013) -- Start
            'TRA - ENHANCEMENT
            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'dsList = objPeriod.GetList("Period", enModuleReference.Payroll, True, enStatusType.Close)
            dsList = objPeriod.GetList("Period", enModuleReference.Payroll, FinancialYear._Object._YearUnkid, FinancialYear._Object._Database_Start_Date, True, enStatusType.Close)
            'Sohail (21 Aug 2015) -- End
            dtTable = New DataView(dsList.Tables("Period"), "end_date = '" & eZeeDate.convertDate(FinancialYear._Object._Database_End_Date) & "'", "", DataViewRowState.CurrentRows).ToTable
            If dtTable.Rows.Count <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 31, "You cannot close this finanacial year. Reason : There has to be one period for finanical year end date."), enMsgBoxStyle.Information)
                cboPayPeriodClose.Focus()
                Exit Try
            End If
            'Sohail (09 Nov 2013) -- End

            'Sohail (11 Sep 2010) -- Start
            '***Checking for previous year status (open or close)
            dsList = objMaster.Get_Database_Year_List("Year", False, Company._Object._Companyunkid)
            dtTable = New DataView(dsList.Tables("Year"), "end_date < '" & eZeeDate.convertDate(FinancialYear._Object._Database_End_Date) & "'", "", DataViewRowState.CurrentRows).ToTable
            If dtTable.Rows.Count > 0 Then
                strMsg = Language.getMessage(mstrModuleName, 18, "Sorry, You cannot do this process at this time. Reason : Previous year [") & dtTable.Rows(0).Item("financialyear_name").ToString & Language.getMessage(mstrModuleName, 19, "] is still open. Please close previous year first.")
                eZeeMsgBox.Show(strMsg, enMsgBoxStyle.Information)
                Exit Try
            End If
            'Sohail (11 Sep 2010) -- End

            'S.SANDEEP [26 SEP 2016] -- START
            'ENHANCEMENT : NEW CLOSE YEAR CHANGES
            'Dim objfrm As New frmGenerateYear_Wizard
            Dim objfrm As New frmNewYear_Wizard
            'S.SANDEEP [26 SEP 2016] -- END
            If objfrm.displayDialog(CInt(cboPayYearClose.SelectedValue), mdtPayYearStartDate, mdtPayYearEndDate) = True Then
                Call FillCombo()
                Me.Close()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnCloseYear_Click", mstrModuleName)
        Finally
            objPeriod = Nothing
            objMaster = Nothing
            dsList = Nothing
            dtTable = Nothing
        End Try
    End Sub
#End Region

    'Sohail (12 Dec 2015) -- Start
    'Enhancement - Showing Progress of closing period.
#Region " Background Worker Events "

    Private Sub objbgwClosePayroll_DoWork(ByVal sender As System.Object, ByVal e As System.ComponentModel.DoWorkEventArgs) Handles objbgwClosePayroll.DoWork
        Dim objPeriod As New clscommom_period_Tran
        Try
            mblnProcessFailed = False
            Me.ControlBox = False
            Me.Enabled = False

            RemoveHandler gfrmMDI.tmrReminder.Tick, AddressOf gfrmMDI.tmrReminder_Tick
            clsApplicationIdleTimer.LastIdealTime.Stop()


            Dim objAppSettings As New clsApplicationSettings
            Dim strFinalPath As String = String.Empty
            Dim StrFilePath As String = objAppSettings._ApplicationPath & "Data\Close Period Backup"

            If objAppSettings._IsClient = 0 Then
                If Not System.IO.Directory.Exists(StrFilePath) Then
                    System.IO.Directory.CreateDirectory(StrFilePath)
                End If
                objAppSettings = Nothing

                objlblProgress.Text = "Taking Database backup..."

                For i As Integer = 0 To 2
                    If i = 0 Then
                        eZeeDatabase.change_database(FinancialYear._Object._ConfigDatabaseName)
                        If Not System.IO.Directory.Exists(StrFilePath & "\" & FinancialYear._Object._ConfigDatabaseName) Then
                            System.IO.Directory.CreateDirectory(StrFilePath & "\" & FinancialYear._Object._ConfigDatabaseName)
                        End If
                        strFinalPath = StrFilePath & "\" & FinancialYear._Object._ConfigDatabaseName
                        objlblProgress.Text = "Taking Database backup [" & FinancialYear._Object._ConfigDatabaseName & "]..."

                    ElseIf i = 2 Then 'this database has to be always to be taken at last and i number has to be higher in order
                        eZeeDatabase.change_database(FinancialYear._Object._DatabaseName)
                        If Not System.IO.Directory.Exists(StrFilePath & "\" & FinancialYear._Object._DatabaseName) Then
                            System.IO.Directory.CreateDirectory(StrFilePath & "\" & FinancialYear._Object._DatabaseName)
                        End If
                        strFinalPath = StrFilePath & "\" & FinancialYear._Object._DatabaseName
                        objlblProgress.Text = "Taking Database backup [" & FinancialYear._Object._DatabaseName & "]..."

                    ElseIf i = 1 Then
                        Dim objBkUp As New clsBackup
                        If objBkUp.Is_DatabasePresent("arutiimages") Then
                            eZeeDatabase.change_database("arutiimages")
                            If Not System.IO.Directory.Exists(StrFilePath & "\" & "arutiimages") Then
                                System.IO.Directory.CreateDirectory(StrFilePath & "\" & "arutiimages")
                            End If
                            strFinalPath = StrFilePath & "\" & "arutiimages"
                            objlblProgress.Text = "Taking Database backup [arutiimages]..."
                        Else
                            strFinalPath = ""
                        End If
                        objBkUp = Nothing
                    End If
                    Me.Refresh()

                    If strFinalPath <> "" Then

                        strFinalPath = objBackupDatabase.Backup(strFinalPath, General_Settings._Object._ServerName)

                        Dim objBackup As New clsBackup
                        If System.IO.File.Exists(strFinalPath) Then

                            objBackup._Backup_Date = ConfigParameter._Object._CurrentDateAndTime
                            objBackup._Backup_Path = strFinalPath
                            If i = 0 Then
                                objBackup._Companyunkid = -1
                                objBackup._Yearunkid = -1
                                objBackup._Isconfiguration = True
                            ElseIf i = 1 Then
                                objBackup._Companyunkid = Company._Object._Companyunkid
                                objBackup._Yearunkid = FinancialYear._Object._YearUnkid
                                objBackup._Isconfiguration = False
                            End If
                            objBackup._Userunkid = User._Object._Userunkid

                            Call objBackup.Insert()
                        End If

                    End If
                Next
            Else
                objAppSettings = Nothing
            End If

            Dim objClosePeriod As New clsClosePeriod
            objClosePeriod._PeriodUnkId = CInt(cboPayPeriodClose.SelectedValue)
            objClosePeriod._NexPeriodUnkId = CInt(cboPayPeriodOpen.SelectedValue)
            objClosePeriod._PeriodStartDate = mdtPayPeriodStartDate
            objClosePeriod._PeriodEndDate = mdtPayPeriodEndDate
            objClosePeriod._TnAStartDate = mdtTnAStartDate
            objClosePeriod._TnAEndDate = mdtTnAEndDate

            objClosePeriod._NextPeriodStartDate = mdtNextPayPeriodStartDate
            objClosePeriod._NextPeriodEndDate = mdtNextPayPeriodEndDate
            objClosePeriod._CopyClosingPeriodEDSlab = chkCopyPreviousEDSlab.Checked

            With objClosePeriod
                ._FormName = mstrModuleName
                ._LoginEmployeeunkid = 0
                ._ClientIP = getIP()
                ._HostName = getHostName()
                ._FromWeb = False
                ._AuditUserId = User._Object._Userunkid
._CompanyUnkid = Company._Object._Companyunkid
                ._AuditDate = ConfigParameter._Object._CurrentDateAndTime
            End With

            If objClosePeriod.Close_Period(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, mdtPayPeriodStartDate, mdtPayPeriodEndDate, ConfigParameter._Object._UserAccessModeSetting, True, ConfigParameter._Object._IsIncludeInactiveEmp, User._Object.Privilege._AllowToApproveEarningDeduction, ConfigParameter._Object._CurrentDateAndTime, FinancialYear._Object._Database_Start_Date, FinancialYear._Object._Database_End_Date, False, "", objbgwClosePayroll) = False Then
                mblnProcessFailed = True 'Sohail (03 Feb 2016)
                If objClosePeriod._Message <> "" Then
                    'Throw New Exception(objClosePeriod._Message)
                    eZeeMsgBox.Show(objClosePeriod._Message, enMsgBoxStyle.Information)
                    Exit Try
                Else
                    DisplayError.Show("-1", "Close period process failed.", "objbgwClosePayroll_DoWork", mstrModuleName)
                End If
                objClosePeriod = Nothing
                Exit Try
            Else
                objClosePeriod = Nothing
            End If

            objPeriod._Periodunkid(FinancialYear._Object._DatabaseName) = CInt(cboPayPeriodClose.SelectedValue)
            objPeriod._Statusid = enStatusType.Close
            objPeriod._Closeuserunkid = User._Object._Userunkid

            With objPeriod
                ._FormName = mstrModuleName
                ._LoginEmployeeunkid = 0
                ._ClientIP = getIP()
                ._HostName = getHostName()
                ._FromWeb = False
                ._AuditUserId = User._Object._Userunkid
._CompanyUnkid = Company._Object._Companyunkid
                ._AuditDate = ConfigParameter._Object._CurrentDateAndTime
            End With

            objPeriod.Update()

            Call FillCombo()

            Cursor.Current = Cursors.Default


        Catch ex As Exception
            mblnProcessFailed = True
            DisplayError.Show("-1", ex.Message, "objbgwClosePayroll_DoWork", mstrModuleName)
            RemoveHandler gfrmMDI.tmrReminder.Tick, AddressOf gfrmMDI.tmrReminder_Tick
            AddHandler gfrmMDI.tmrReminder.Tick, AddressOf gfrmMDI.tmrReminder_Tick
            clsApplicationIdleTimer.LastIdealTime.Start()
        Finally
            RemoveHandler gfrmMDI.tmrReminder.Tick, AddressOf gfrmMDI.tmrReminder_Tick
            AddHandler gfrmMDI.tmrReminder.Tick, AddressOf gfrmMDI.tmrReminder_Tick
            clsApplicationIdleTimer.LastIdealTime.Start()
        End Try
    End Sub

    Private Sub objbgwClosePayroll_ProgressChanged(ByVal sender As System.Object, ByVal e As System.ComponentModel.ProgressChangedEventArgs) Handles objbgwClosePayroll.ProgressChanged
        Try
            objlblProgress.Text = "[ " & e.ProgressPercentage.ToString & " / 9 ]"
            If e.ProgressPercentage = 0 Then
                objlblProgress.Text &= vbCrLf & "Updating Non Recurrent Head On Earning Deduction..."

            ElseIf e.ProgressPercentage = 1 Then
                objlblProgress.Text &= vbCrLf & "Generating Loan Advance Balance..."
                If clsClosePeriod._ProgressTotalCount > 0 Then
                    objlblProgress.Text &= "Total Transactions [" & clsClosePeriod._ProgressTotalCount & "]"
                End If

            ElseIf e.ProgressPercentage = 2 Then
                objlblProgress.Text &= vbCrLf & "Generating Saving Balance..."

            ElseIf e.ProgressPercentage = 3 Then
                objlblProgress.Text &= vbCrLf & "Updating Saving Balance..."
                If clsClosePeriod._ProgressTotalCount > 0 Then
                    objlblProgress.Text &= "Total Transactions [" & clsClosePeriod._ProgressTotalCount & "]"
                End If

            ElseIf e.ProgressPercentage = 4 Then
                objlblProgress.Text &= vbCrLf & "Updating Issued Leave..."

            ElseIf e.ProgressPercentage = 5 Then
                objlblProgress.Text &= vbCrLf & "Updating Posted Claim Request..."

            ElseIf e.ProgressPercentage = 6 Then
                objlblProgress.Text &= vbCrLf & "Carry Forwarding opening balance to new period..."

            ElseIf e.ProgressPercentage = 7 Then
                objlblProgress.Text &= vbCrLf & "Copying Closing Period Earning Deduction Slab..."
                If clsClosePeriod._ProgressTotalCount > 0 Then
                    objlblProgress.Text &= " [" & clsClosePeriod._ProgressCurrCount & " / " & clsClosePeriod._ProgressTotalCount & "] "
                End If

            ElseIf e.ProgressPercentage = 8 Then
                objlblProgress.Text &= vbCrLf & "Updating Loan Balance..."
                If clsClosePeriod._ProgressTotalCount > 0 Then
                    'Sohail (21 Sep 2019) -- Start
                    'NMB Enhancement # : Show update loan balance count on close period.
                    'objlblProgress.Text &= "Total Transactions [" & clsClosePeriod._ProgressTotalCount & "]"
                    objlblProgress.Text &= " [" & clsClosePeriod._ProgressCurrCount & " / " & clsClosePeriod._ProgressTotalCount & "] "
                    'Sohail (21 Sep 2019) -- End
                End If

            End If


        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbgwClosePayroll_ProgressChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub objbgwClosePayroll_RunWorkerCompleted(ByVal sender As System.Object, ByVal e As System.ComponentModel.RunWorkerCompletedEventArgs) Handles objbgwClosePayroll.RunWorkerCompleted
        Try

            RemoveHandler gfrmMDI.tmrReminder.Tick, AddressOf gfrmMDI.tmrReminder_Tick
            AddHandler gfrmMDI.tmrReminder.Tick, AddressOf gfrmMDI.tmrReminder_Tick
            clsApplicationIdleTimer.LastIdealTime.Start()

            If e.Cancelled = True Then

            ElseIf e.Error IsNot Nothing Then
                eZeeMsgBox.Show(e.Error.ToString, enMsgBoxStyle.Information)
            ElseIf mblnProcessFailed = True Then

            Else
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 14, "Pay Period Closed successfully."), enMsgBoxStyle.Information)
                cboPayPeriodClose.SelectedValue = 0
                objlblProgress.Text = ""
            End If

            Me.ControlBox = True
            Me.Enabled = True

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbgwClosePayroll_RunWorkerCompleted", mstrModuleName)
        End Try
    End Sub

#End Region
    'Sohail (12 Dec 2015) -- End

#Region " Message List "
    '1, "Please select Pay Year. Pay Year is mandatory information."
    '2, "Please select Pay Period to close. Pay Period is mandatory information."
    '3, "Sorry, Previous Period (" & dtTable.Rows(0).Item("period_name").ToString & ") is still Open. Please Close Previous Period first."
    '4, "Sorry, Absent Process is not done yet for this period. " & _
    '       "It is recommended that first do Absent process from Time and Attendance Module followed by payroll process for this period before closing this period."
    '5, "Sorry, Payroll Process is not done yet for the date " & mdtPayPeriodEndDate.ToShortDateString & " for some of the employees in this period." & vbCrLf & _
    '       "It is recommended that first process payroll for the listed employee for the date " & mdtPayPeriodEndDate.ToShortDateString & " before closing this period."
    '6, "Sorry, You can not close this year now. Reason : The ending date of this year is " & mdtPayYearEndDate.ToShortDateString & "."
    '7, "Sorry, the Period (" & dtTable.Rows(0).Item("period_name").ToString & ") is still Open. Please Close this Period first before closing Year."
    '8, "Pay Period Closed successfully."
    '9, "Sorry, You can not do this process at this time. Reason : Previous year (" & dtTable.Rows(0).Item("financialyear_name").ToString & ") is still open. Please close previous year first."
    '10, "Are you sure want to Close this Period?"
    '11, "Sorry, Payment is not done yet for the Negative Balance for some of the employees." & vbCrLf & _
    '       "It is recommended that first make payment for the listed employees before closing this period."
    '12, "Please select Pay Period to be open. Pay Period is mandatory information."
    '13, "Some payslip's payment are still unpaid in this period." & vbCrLf & vbCrLf & "Do you want to proceed?"
#End Region



    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Private Sub OtherSettings()
        Try
            Me.SuspendLayout()
           
            Call SetLanguage()

            Me.gbClosePeriodInfo.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbClosePeriodInfo.ForeColor = GUI._eZeeContainerHeaderForeColor


            Me.btnClosePeriod.GradientBackColor = GUI._ButttonBackColor
            Me.btnClosePeriod.GradientForeColor = GUI._ButttonFontColor

            Me.btnClose.GradientBackColor = GUI._ButttonBackColor
            Me.btnClose.GradientForeColor = GUI._ButttonFontColor

            Me.btnCloseYear.GradientBackColor = GUI._ButttonBackColor
            Me.btnCloseYear.GradientForeColor = GUI._ButttonFontColor


            Me.ResumeLayout()
        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "OtherSettings", mstrModuleName)
        End Try
    End Sub


    Private Sub SetLanguage()
        Try
            Me.Text = Language._Object.getCaption(Me.Name, Me.Text)

            Me.btnClosePeriod.Text = Language._Object.getCaption(Me.btnClosePeriod.Name, Me.btnClosePeriod.Text)
            Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
            Me.btnCloseYear.Text = Language._Object.getCaption(Me.btnCloseYear.Name, Me.btnCloseYear.Text)
            Me.gbClosePeriodInfo.Text = Language._Object.getCaption(Me.gbClosePeriodInfo.Name, Me.gbClosePeriodInfo.Text)
            Me.lblPayPeriodOpen.Text = Language._Object.getCaption(Me.lblPayPeriodOpen.Name, Me.lblPayPeriodOpen.Text)
            Me.lblPayYearClose.Text = Language._Object.getCaption(Me.lblPayYearClose.Name, Me.lblPayYearClose.Text)
            Me.lblPayPeriodClose.Text = Language._Object.getCaption(Me.lblPayPeriodClose.Name, Me.lblPayPeriodClose.Text)
            Me.lnEmployeeList.Text = Language._Object.getCaption(Me.lnEmployeeList.Name, Me.lnEmployeeList.Text)
            Me.colhCode.Text = Language._Object.getCaption(CStr(Me.colhCode.Tag), Me.colhCode.Text)
            Me.colhName.Text = Language._Object.getCaption(CStr(Me.colhName.Tag), Me.colhName.Text)
            Me.chkCopyPreviousEDSlab.Text = Language._Object.getCaption(Me.chkCopyPreviousEDSlab.Name, Me.chkCopyPreviousEDSlab.Text)
            Me.lblNote.Text = Language._Object.getCaption(Me.lblNote.Name, Me.lblNote.Text)
            Me.lblNote1.Text = Language._Object.getCaption(Me.lblNote1.Name, Me.lblNote1.Text)
            Me.lblNoteNo1.Text = Language._Object.getCaption(Me.lblNoteNo1.Name, Me.lblNoteNo1.Text)
            Me.lblNoteNo2.Text = Language._Object.getCaption(Me.lblNoteNo2.Name, Me.lblNoteNo2.Text)
            Me.lblNote2.Text = Language._Object.getCaption(Me.lblNote2.Name, Me.lblNote2.Text)

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
        End Try
    End Sub


    Private Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Please select Pay Year. Pay Year is mandatory information.")
            Language.setMessage(mstrModuleName, 2, "Please select Pay Period to close. Pay Period is mandatory information.")
            Language.setMessage(mstrModuleName, 3, "Please select Pay Period to be open. Pay Period is mandatory information.")
            Language.setMessage(mstrModuleName, 4, "] is still open. Please close previous year first.")
            Language.setMessage(mstrModuleName, 5, "Sorry, Previous Period [")
            Language.setMessage(mstrModuleName, 6, "] is still Open. Please Close Previous Period first.")
            Language.setMessage(mstrModuleName, 7, "Sorry, Absent Process is not done yet for this period.It is recommended that first do Absent process from Time and Attendance Module followed by payroll process for this period before closing this period.")
            Language.setMessage(mstrModuleName, 8, "Sorry, Payroll Process is not done yet for the date")
            Language.setMessage(mstrModuleName, 9, " for some of the employees in this period.")
            Language.setMessage(mstrModuleName, 10, "It is recommended that first process payroll for the listed employee for the date")
            Language.setMessage(mstrModuleName, 11, " before closing this period.")
            Language.setMessage(mstrModuleName, 12, "Do you want to proceed?")
            Language.setMessage(mstrModuleName, 13, "It is Recommended to take backup of database before closing period.")
            Language.setMessage(mstrModuleName, 14, "Pay Period Closed successfully.")
            Language.setMessage(mstrModuleName, 15, "Sorry, You can not close this year now. Reason : The ending date of this year is")
            Language.setMessage(mstrModuleName, 16, "Sorry, the Period [")
            Language.setMessage(mstrModuleName, 17, "] is still Open. Please Close this Period first before closing Year.")
            Language.setMessage(mstrModuleName, 18, "Sorry, You cannot do this process at this time. Reason : Previous year [")
            Language.setMessage(mstrModuleName, 19, "] is still open. Please close previous year first.")
            Language.setMessage(mstrModuleName, 20, "Some payslip's are still unpaid in this period.")
            Language.setMessage(mstrModuleName, 21, "Some Employee(s) Saving Stop Date falls in this Period and their Saving will set to completed status.")
            Language.setMessage(mstrModuleName, 22, "Copy Previous ED Slab is ticked. This will copy Previous Period Slab.")
            Language.setMessage(mstrModuleName, 23, "Sorry, Some Salary Changes are in Pending status (not Approved) for Selected Period. To perform payroll process, please Approve those Salary Changes.")
            Language.setMessage(mstrModuleName, 24, "Sorry, Some Transaction Heads are in Pending status (not Posted to ED) on Batch Posting for Selected Period. To perform payroll process, please Post those heads.")
            Language.setMessage(mstrModuleName, 25, "Sorry, Some payslip payment(s) are not Authorized yet. Please Authorize payslip payment.")
            Language.setMessage(mstrModuleName, 26, "Sorry, Some Transaction Heads are in Pending status (not Approved) on Earning Deduction for Selected Period. To perform payroll process, please Approve those heads.")
            Language.setMessage(mstrModuleName, 27, "Sorry, Some payslip's are still unpaid in this period. Please make payment for unpaid payslip.")
            Language.setMessage(mstrModuleName, 28, "Do you want to continue?")
            Language.setMessage(mstrModuleName, 29, " Are you sure want to Close this Period?")
            Language.setMessage(mstrModuleName, 30, "Some Employee(s) leave form Start Date falls in this Period and their leave form(s) are in pending or approved status.Please issue or reject or reschedule leave form.")
            Language.setMessage(mstrModuleName, 31, "You cannot close this finanacial year. Reason : There has to be one period for finanical year end date.")
            Language.setMessage(mstrModuleName, 32, "Unprocessed Employee List")
            Language.setMessage(mstrModuleName, 33, "Sorry, Some PAY PER ACTIVITY transactions are in Pending status (not Posted to Payroll) for Selected Period. To perform payroll process, please Post those transactions.")
            Language.setMessage(mstrModuleName, 34, "Sorry. Some of Operation(s) either Interest Rate/EMI Tenure/Topup Amount are still pending for current period Approval process. In order to close period, please either Approve or Reject pending operations for selected period.")
            Language.setMessage(mstrModuleName, 35, "Sorry, Auto Salary Increment is not done for some of the employees for this period.")
            Language.setMessage(mstrModuleName, 36, "Please do Auto Salary Increment for the listed employees whose Salary Increment is not done for this period.")
            Language.setMessage(mstrModuleName, 37, "Auto Salary Increment not done")
            Language.setMessage(mstrModuleName, 38, "Sorry, You cannot close this Period now.")
            Language.setMessage(mstrModuleName, 39, "Reason : The Period to be closed is in use on")
            Language.setMessage(mstrModuleName, 40, "Machine(s).")
            Language.setMessage(mstrModuleName, 41, "Do you want system to close aruti forcefully on that machine(s)?")
            Language.setMessage(mstrModuleName, 42, "Some of the Budget Employee Timesheets are not submitted for approval yet.")
            Language.setMessage(mstrModuleName, 43, "Some of the Budget Employee Timesheets are not final approved yet.")
			Language.setMessage(mstrModuleName, 44, "Budget timesheet submit for approval is not done")
	    Language.setMessage(mstrModuleName, 45, "Budget timesheet final approval is not done")
			Language.setMessage(mstrModuleName, 46, "Do you want to process Payroll?")
			Language.setMessage(mstrModuleName, 47, "Sorry, You cannot close this period now. Reason: As per the company policy, you will able to close period on or after")
			Language.setMessage(mstrModuleName, 48, "Period end date is not reached yet, Are you sure you want to continue?")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class