﻿Option Strict On

#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data
Imports System.IO

#End Region

Public Class frmImportUpdateFlatRateHeads

#Region " Private Variables "

    Private ReadOnly mstrModuleName As String = "frmImportUpdateFlatRateHeads"
    Private mblnCancel As Boolean = True
    Private dsList As New DataSet
    Private mdtTable As New DataTable
    'S.SANDEEP [12-Jan-2018] -- START
    'ISSUE/ENHANCEMENT : REF-ID # 0001843
    'Private objIExcel As ExcelData
    'S.SANDEEP [12-Jan-2018] -- END
    'Sohail (18 Jan 2012) -- Start
    'TRA - ENHANCEMENT
    Private mdtFilteredTable As DataTable
    Private m_Dataview As DataView
    'Sohail (18 Jan 2012) -- End
#End Region

#Region " Display Dialog "

    Public Function displayDialog() As Boolean
        Try
            Me.ShowDialog()
            Return Not mblnCancel
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "displayDialog", mstrModuleName)
        End Try
    End Function

#End Region

#Region " Private Methods "

    'Sohail (26 May 2011) -- Start
    Private Sub SetColor()
        Try
            txtFilePath.BackColor = GUI.ColorComp
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetColor", mstrModuleName)
        End Try
    End Sub
    'Sohail (26 May 2011) -- End

    Private Sub FillCombo()


    End Sub

    'Sohail (18 Jan 2012) -- Start
    'TRA - ENHANCEMENT
    'Private Sub FillList()
    '    Try
    '        Dim lvItem As ListViewItem
    '        lvEmployeeList.Items.Clear()

    '        Cursor.Current = Cursors.WaitCursor
    '        lvEmployeeList.BeginUpdate()
    '        For Each dtRow As DataRow In mdtTable.Rows
    '            lvItem = New ListViewItem

    '            lvItem.Text = ""
    '            lvItem.Tag = CInt(dtRow.Item("edunkid").ToString) 'Sohail (26 May 2011)

    '            lvItem.SubItems.Add(dtRow.Item("employeecode").ToString)

    '            lvItem.SubItems.Add(dtRow.Item("employeename").ToString)
    '            lvItem.SubItems(colhEmpName.Index).Tag = CInt(dtRow.Item("employeeunkid").ToString)

    '            lvItem.SubItems.Add(dtRow.Item("tranheadname").ToString)
    '            lvItem.SubItems(colhTranHead.Index).Tag = CInt(dtRow.Item("tranheadunkid").ToString)

    '            'Sohail (26 May 2011) -- Start
    '            lvItem.SubItems.Add(dtRow.Item("costcentername").ToString)
    '            lvItem.SubItems(colhCostCenter.Index).Tag = CInt(dtRow.Item("costcenterunkid").ToString)
    '            'Sohail (26 May 2011) -- End

    '            lvItem.SubItems.Add(Format(CDec(dtRow.Item("amount")), GUI.fmtCurrency))
    '            lvItem.SubItems(colhAmount.Index).Tag = CDec(dtRow.Item("amount"))

    '            'Sohail (26 May 2011) -- Start
    '            If CInt(dtRow.Item("rowtypeid").ToString) = 0 Then 'Proper entry

    '            ElseIf CInt(dtRow.Item("rowtypeid").ToString) = 1 Then 'Unassigned heads on Ed
    '                lvItem.BackColor = System.Drawing.SystemColors.ControlDark
    '                lvItem.ForeColor = System.Drawing.SystemColors.ControlText

    '                lblUnAssignHead.Visible = True
    '                lblUnAssignHeadColor.Visible = True
    '            ElseIf CInt(dtRow.Item("rowtypeid").ToString) = 2 Then 'employee code not found
    '                lvItem.BackColor = Color.Red
    '                lvItem.ForeColor = Color.Yellow

    '                lblUnMatchedEmpColor.Visible = True
    '                lblUnMatchedEmp.Visible = True
    '            End If
    '            'Sohail (26 May 2011) -- End

    '            lvEmployeeList.Items.Add(lvItem)
    '        Next

    '        If lvEmployeeList.Items.Count > 18 Then
    '            colhEmpName.Width = 180 - 18 'Sohail (26 May 2011)
    '        Else
    '            colhEmpName.Width = 180 'Sohail (26 May 2011)
    '        End If
    '        lvEmployeeList.EndUpdate()
    '        Cursor.Current = Cursors.Default
    '    Catch ex As Exception
    '        DisplayError.Show("-1", ex.Message, "FillGirdView", mstrModuleName)
    '    End Try
    'End Sub
    Private Sub FillGirdView()
        Try
            dgvImportInfo.AutoGenerateColumns = False

            objdgcolhCheck.DataPropertyName = "IsChecked"
            dgcolhEmpCode.DataPropertyName = "employeecode"
            dgcolhEmpName.DataPropertyName = "employeename"
            dgcolhTranHead.DataPropertyName = "TranHeadName"
            dgcolhAmount.DataPropertyName = "amount"
            dgcolhCostCenter.DataPropertyName = "costcentername"
            dgcolhMedicalRefCode.DataPropertyName = "medicalrefno"

            dgvImportInfo.DataSource = m_Dataview
            dgvImportInfo.Refresh()

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillGirdView", mstrModuleName)
        End Try
    End Sub
    'Sohail (18 Jan 2012) -- End

    Private Sub CheckAll(ByVal blnCheckAll As Boolean)
        Try
            'Sohail (18 Jan 2012) -- Start
            'TRA - ENHANCEMENT
            'For Each lvItem As ListViewItem In lvEmployeeList.Items
            '    'Sohail (26 May 2011) -- Start
            '    If lvItem.BackColor = Color.Red AndAlso lblUnMatchedEmpColor.Visible = True Then Continue For
            '    If lvItem.BackColor = System.Drawing.SystemColors.ControlDark AndAlso lblUnAssignHeadColor.Visible = True Then Continue For
            '    RemoveHandler lvEmployeeList.ItemCheck, AddressOf lvEmployeeList_ItemCheck
            '    'Sohail (26 May 2011) -- End
            '    RemoveHandler lvEmployeeList.ItemChecked, AddressOf lvEmployeeList_ItemChecked
            '    lvItem.Checked = blnCheckAll
            '    AddHandler lvEmployeeList.ItemChecked, AddressOf lvEmployeeList_ItemChecked
            '    AddHandler lvEmployeeList.ItemCheck, AddressOf lvEmployeeList_ItemCheck 'Sohail (26 May 2011)
            'Next

            If m_Dataview Is Nothing Then Exit Sub

            For Each dtRow As DataRowView In m_Dataview
                dtRow.Item("IsChecked") = chkSelectAll.Checked
            Next
            dgvImportInfo.DataSource = m_Dataview
            dgvImportInfo.Refresh()
            'Sohail (18 Jan 2012) -- End

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "CheckAll", mstrModuleName)
        End Try
    End Sub
#End Region

#Region " Form's Events "

    Private Sub frmImportUpdateFlatRateHeads_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        'S.SANDEEP [12-Jan-2018] -- START
        'ISSUE/ENHANCEMENT : REF-ID # 0001843
        'objIExcel = New ExcelData
        'S.SANDEEP [12-Jan-2018] -- END
        Try
            Call Set_Logo(Me, gApplicationType)

            'Anjan (02 Sep 2011)-Start
            'Issue : Including Language Settings.
            Language.setLanguage(Me.Name)
            Call OtherSettings()
            'Anjan (02 Sep 2011)-End 

            'Sohail (26 May 2011) -- Start
            lblUnAssignHead.Visible = False
            lblUnAssignHeadColor.Visible = False
            lblUnMatchedEmpColor.Visible = False
            lblUnMatchedEmp.Visible = False

            lblUnAssignHead.Text = Language.getMessage(mstrModuleName, 5, "UnAssigned Transaction Head")
            lblUnMatchedEmp.Text = Language.getMessage(mstrModuleName, 6, "UnMatched Employee Code")
            'Sohail (26 May 2011) -- End
            Call FillCombo()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmImportUpdateFlatRateHeads_Load", mstrModuleName)
        End Try
    End Sub
    'Anjan (02 Sep 2011)-Start
    'Issue : Including Language Settings.
    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub
    'Anjan (02 Sep 2011)-End 
#End Region

#Region " Buttons Events "

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            mblnCancel = True
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnClose_Click", mstrModuleName)
        End Try

    End Sub

    Private Sub btnImport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnImport.Click
        'Sohail (18 Jan 2012) -- Start
        'TRA - ENHANCEMENT
        'If lvEmployeeList.CheckedItems.Count <= 0 Then
        '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please Check atleast one transaction to import."), enMsgBoxStyle.Information)
        '    Exit Sub
        'End If
        If mdtTable.Rows.Count <= 0 Then
            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 8, "Sorry, you cannot import this file. Reason :Assigned heads are not in this import file."), enMsgBoxStyle.Information)
            Exit Sub
        End If

        m_Dataview.RowFilter = "rowtypeid = 0 "
        mdtFilteredTable = m_Dataview.ToTable

        If mdtFilteredTable.Rows.Count <= 0 Then
            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 7, "Sorry, you cannot import this file. Reason :Some transaction Codes or Employee Codes are not in the system."), enMsgBoxStyle.Information)
            Exit Sub
        End If

        mdtFilteredTable = New DataView(m_Dataview.ToTable, "rowtypeid = 0 AND  IsChecked = 1 ", "", DataViewRowState.CurrentRows).ToTable
        If mdtFilteredTable.Rows.Count <= 0 Then
            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please Tick atleast one transaction from list to Import."), enMsgBoxStyle.Information)
            Exit Sub
        End If
        'Sohail (18 Jan 2012) -- End

        'Sohail (02 Sep 2019) -- Start
        'Mainspring Resourcing Ghana Ltd Enhancement - 76.1 - # 0004098: Approval Flow for Flat Rate Heads Amount Uploaded.
        '1. Set ED in pending status if user has not priviledge of ED approval
        '2. Perform Global approver with option to filter for allocation and approved any posted (new /edited) E & D values in MSS
        '3. Send notifications to ED approvers for all employees ED in one email
        If gobjEmailList.Count > 0 Then
            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 9, "Sending Email(s) process is in progress from other module. Please wait."), enMsgBoxStyle.Information)
            Exit Sub
        End If
        'Sohail (02 Sep 2019) -- End

        Dim objED As clsEarningDeduction
        'Sohail (26 May 2011) -- Start
        Dim objEmpCostCenter As clsemployee_costcenter_Tran
        Dim dsList As DataSet
        'Sohail (26 May 2011) -- End
        Dim blnResult As Boolean = False
        'Sohail (21 Aug 2015) -- Start
        'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
        Dim objPeriod As clscommom_period_Tran
        'Sohail (21 Aug 2015) -- End

        Try
            If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Are you sure, you want to Update Flat Rate Head to selected Employee(s)?"), CType(enMsgBoxStyle.Question + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.None Then
                Exit Try
            End If

            'Sohail (02 Sep 2019) -- Start
            'Mainspring Resourcing Ghana Ltd Enhancement - 76.1 - # 0004098: Approval Flow for Flat Rate Heads Amount Uploaded.
            '1. Set ED in pending status if user has not priviledge of ED approval
            '2. Perform Global approver with option to filter for allocation and approved any posted (new /edited) E & D values in MSS
            '3. Send notifications to ED approvers for all employees ED in one email
            Dim intPeriodID As Integer = 0
            'Sohail (02 Sep 2019) -- End

            'Sohail (26 May 2011) -- Start
            'For Each dtRow As DataRow In mdtTable.Rows
            '    objED = New clsEarningDeduction

            '    objED._Edunkid = CInt(dtRow.Item("edunkid"))
            '    If objED._Calctype_Id = enCalcType.FlatRate_Others Then
            '        objED._Amount = cdec(dtRow.Item("amount"))
            '    End If
            '    objED._Userunkid = CInt(dtRow.Item("userunkid"))

            '    blnResult = objED.Update(False)
            '    If blnResult = False AndAlso objED._Message <> "" Then
            '        eZeeMsgBox.Show(objED._Message, enMsgBoxStyle.Information)
            '        Exit Try
            '    End If
            'Next
            Cursor.Current = Cursors.WaitCursor
            'Sohail (18 Jan 2012) -- Start
            'TRA - ENHANCEMENT
            For Each dtRow As DataRow In mdtFilteredTable.Rows
                'For Each lvItem As ListViewItem In lvEmployeeList.CheckedItems
                'Sohail (18 Jan 2012) -- End
                objED = New clsEarningDeduction

                'Sohail (18 Jan 2012) -- Start
                'TRA - ENHANCEMENT
                'objED._Edunkid = CInt(lvItem.Tag)
                'Sohail (21 Aug 2015) -- Start
                'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                'objED._Edunkid = CInt(dtRow.Item("edunkid"))
                objED._Edunkid(Nothing, FinancialYear._Object._DatabaseName) = CInt(dtRow.Item("edunkid"))
                'Sohail (21 Aug 2015) -- End
                'Sohail (18 Jan 2012) -- End
                'Hemant (11 Dec 2018) -- Start
                'Issue : To Exclude Importing Salary from Salary Column in Excel To Prevent  Salary changes from ED
                If objED._Typeof_Id = enTypeOf.Salary Then
                    Continue For
                    'If objED._Calctype_Id = enCalcType.FlatRate_Others Then
                ElseIf objED._Calctype_Id = enCalcType.FlatRate_Others Then
                    'Hemant (11 Dec 2018) -- End
                    'Sohail (18 Jan 2012) -- Start
                    'TRA - ENHANCEMENT
                    'objED._Amount = CDec(lvItem.SubItems(colhAmount.Index).Text)
                    objED._Amount = CDec(dtRow.Item("amount"))
                    'Sohail (18 Jan 2012) -- End
                End If
                objED._Userunkid = User._Object._Userunkid
                'Sohail (18 Jan 2012) -- Start
                'TRA - ENHANCEMENT
                objED._MedicalRefNo = dtRow.Item("medicalrefno").ToString
                'Sohail (18 Jan 2012) -- End

                'Sohail (02 Sep 2019) -- Start
                'Mainspring Resourcing Ghana Ltd Enhancement - 76.1 - # 0004098: Approval Flow for Flat Rate Heads Amount Uploaded.
                '1. Set ED in pending status if user has not priviledge of ED approval
                '2. Perform Global approver with option to filter for allocation and approved any posted (new /edited) E & D values in MSS
                '3. Send notifications to ED approvers for all employees ED in one email
                If User._Object.Privilege._AllowToApproveEarningDeduction = True Then
                    objED._Isapproved = True
                    objED._Approveruserunkid = User._Object._Userunkid
                Else
                    objED._Isapproved = False
                    objED._Approveruserunkid = -1
                End If
                'Sohail (02 Sep 2019) -- End

                'Sohail (02 Sep 2019) -- Start
                'Mainspring Resourcing Ghana Ltd Enhancement - 76.1 - # 0004098: Approval Flow for Flat Rate Heads Amount Uploaded.
                '1. Set ED in pending status if user has not priviledge of ED approval
                '2. Perform Global approver with option to filter for allocation and approved any posted (new /edited) E & D values in MSS
                '3. Send notifications to ED approvers for all employees ED in one email
                intPeriodID = CInt(dtRow.Item("periodunkid"))
                'Sohail (02 Sep 2019) -- End

                'Sohail (21 Aug 2015) -- Start
                'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                objPeriod = New clscommom_period_Tran
                'Sohail (21 Aug 2015) -- Start
                'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                'objPeriod._Periodunkid = objED._Periodunkid
                objPeriod._Periodunkid(FinancialYear._Object._DatabaseName) = objED._Periodunkid
                'Sohail (21 Aug 2015) -- End
                'Sohail (21 Aug 2015) -- End

                'S.SANDEEP [28-May-2018] -- START
                'ISSUE/ENHANCEMENT : {Audit Trails} 
                objED._FormName = mstrModuleName
                objED._LoginEmployeeUnkid = 0
                objED._ClientIP = getIP()
                objED._HostName = getHostName()
                objED._FromWeb = False
                objED._AuditUserId = User._Object._Userunkid
objED._CompanyUnkid = Company._Object._Companyunkid
                objED._AuditDate = ConfigParameter._Object._CurrentDateAndTime
                'S.SANDEEP [28-May-2018] -- END

                'Sohail (21 Aug 2015) -- Start
                'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                'blnResult = objED.Update(False)
                blnResult = objED.Update(FinancialYear._Object._DatabaseName, False, Nothing, ConfigParameter._Object._CurrentDateAndTime)
                'Sohail (21 Aug 2015) -- End
                If blnResult = False AndAlso objED._Message <> "" Then
                    eZeeMsgBox.Show(objED._Message, enMsgBoxStyle.Information)
                    Exit Try
                End If

                Dim blnExist As Boolean = False
                'Sohail (18 Jan 2012) -- Start
                'TRA - ENHANCEMENT
                If CInt(dtRow.Item("costcenterunkid")) > 0 Then
                    'If CInt(lvItem.SubItems(colhCostCenter.Index).Tag) > 0 Then
                    'Sohail (18 Jan 2012) -- End
                    blnExist = False
                    objEmpCostCenter = New clsemployee_costcenter_Tran
                    'Sohail (18 Jan 2012) -- Start
                    'TRA - ENHANCEMENT
                    'dsList = objEmpCostCenter.GetList("CC", True, CInt(lvItem.SubItems(colhEmpName.Index).Tag), CInt(lvItem.SubItems(colhTranHead.Index).Tag))
                    'Sohail (21 Aug 2015) -- Start
                    'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                    'dsList = objEmpCostCenter.GetList("CC", True, CInt(dtRow.Item("employeeunkid")), CInt(dtRow.Item("tranheadunkid")))
                    'Sohail (29 Mar 2017) -- Start
                    'CCBRT Enhancement - 66.1 - Employee cost center distribution for non-salary heads, Effective period option on Employee Cost Center.
                    'dsList = objEmpCostCenter.GetList(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, objPeriod._Start_Date, objPeriod._End_Date, ConfigParameter._Object._UserAccessModeSetting, True, ConfigParameter._Object._IsIncludeInactiveEmp, "CC", True, CInt(dtRow.Item("employeeunkid")), CInt(dtRow.Item("tranheadunkid")))
                    dsList = objEmpCostCenter.GetList(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, objPeriod._Start_Date, objPeriod._End_Date, ConfigParameter._Object._UserAccessModeSetting, True, ConfigParameter._Object._IsIncludeInactiveEmp, "CC", True, CInt(dtRow.Item("employeeunkid")), CInt(dtRow.Item("tranheadunkid")), , objPeriod._End_Date)
                    'Sohail (29 Mar 2017) -- End
                    'Sohail (21 Aug 2015) -- End
                    'Sohail (18 Jan 2012) -- End

                    If dsList.Tables("CC").Rows.Count > 0 Then
                        'Sohail (21 Aug 2015) -- Start
                        'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                        'objEmpCostCenter._Costcentertranunkid = CInt(dsList.Tables("CC").Rows(0).Item("costcentertranunkid").ToString)
                        objEmpCostCenter._Costcentertranunkid(FinancialYear._Object._DatabaseName) = CInt(dsList.Tables("CC").Rows(0).Item("costcentertranunkid").ToString)
                        'Sohail (21 Aug 2015) -- End
                        blnExist = True
                    End If
                    'Sohail (18 Jan 2012) -- Start
                    'TRA - ENHANCEMENT
                    'objEmpCostCenter._Employeeunkid = CInt(lvItem.SubItems(colhEmpName.Index).Tag)
                    'objEmpCostCenter._Tranheadunkid = CInt(lvItem.SubItems(colhTranHead.Index).Tag)
                    'objEmpCostCenter._Costcenterunkid = CInt(lvItem.SubItems(colhCostCenter.Index).Tag)
                    objEmpCostCenter._Employeeunkid = CInt(dtRow.Item("employeeunkid"))
                    objEmpCostCenter._Tranheadunkid = CInt(dtRow.Item("tranheadunkid"))
                    objEmpCostCenter._Costcenterunkid = CInt(dtRow.Item("costcenterunkid"))
                    'Sohail (18 Jan 2012) -- End
                    'Sohail (29 Mar 2017) -- Start
                    'CCBRT Enhancement - 66.1 - Employee cost center distribution for non-salary heads, Effective period option on Employee Cost Center.
                    objEmpCostCenter._Periodunkid = objPeriod._Periodunkid(FinancialYear._Object._DatabaseName)
                    'Sohail (29 Mar 2017) -- End
                    objEmpCostCenter._Userunkid = User._Object._Userunkid
                    With objEmpCostCenter
                        ._FormName = mstrModuleName
                        ._LoginEmployeeUnkid = 0
                        ._ClientIP = getIP()
                        ._HostName = getHostName()
                        ._FromWeb = False
                        ._AuditUserId = User._Object._Userunkid
._CompanyUnkid = Company._Object._Companyunkid
                        ._AuditDate = ConfigParameter._Object._CurrentDateAndTime
                    End With
                    If blnExist = True Then
                        'Sohail (21 Aug 2015) -- Start
                        'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                        'blnResult = objEmpCostCenter.Update()
                        blnResult = objEmpCostCenter.Update(FinancialYear._Object._DatabaseName, Nothing, ConfigParameter._Object._CurrentDateAndTime)
                        'Sohail (21 Aug 2015) -- End
                    Else
                        'Sohail (07 Feb 2019) -- Start
                        'NMB Enhancement - 76.1 - Employee cost centre should be distributed on all allocations instead of only on cost centre.
                        objEmpCostCenter._AllocationbyId = enAllocation.COST_CENTER
                        'Sohail (07 Feb 2019) -- End
                        'Sohail (21 Aug 2015) -- Start
                        'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                        'blnResult = objEmpCostCenter.Insert()
                        blnResult = objEmpCostCenter.Insert(FinancialYear._Object._DatabaseName, Nothing, ConfigParameter._Object._CurrentDateAndTime)
                        'Sohail (21 Aug 2015) -- End
                    End If
                    If blnResult = False AndAlso objEmpCostCenter._Message <> "" Then
                        eZeeMsgBox.Show(objED._Message, enMsgBoxStyle.Information)
                        Exit Try
                    End If
                End If
            Next
            Cursor.Current = Cursors.Default
            'Sohail (26 May 2011) -- End

            'Sohail (02 Sep 2019) -- Start
            'Mainspring Resourcing Ghana Ltd Enhancement - 76.1 - # 0004098: Approval Flow for Flat Rate Heads Amount Uploaded.
            '1. Set ED in pending status if user has not priviledge of ED approval
            '2. Perform Global approver with option to filter for allocation and approved any posted (new /edited) E & D values in MSS
            '3. Send notifications to ED approvers for all employees ED in one email
            If User._Object.Privilege._AllowToApproveEarningDeduction = False Then
                objPeriod = New clscommom_period_Tran
                objPeriod._Periodunkid(FinancialYear._Object._DatabaseName) = intPeriodID

                objED = New clsEarningDeduction
                Call objED.SendMailToApprover(FinancialYear._Object._DatabaseName, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, User._Object._Userunkid, objPeriod._Start_Date, objPeriod._End_Date, ConfigParameter._Object._UserAccessModeSetting, True, False, User._Object.Privilege._AllowToApproveEarningDeduction, mdtFilteredTable, intPeriodID, GUI.fmtCurrency, enLogin_Mode.DESKTOP, mstrModuleName, 0)

            End If
            'Sohail (02 Sep 2019) -- End

            mblnCancel = False
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnImport_Click", mstrModuleName)
        Finally
            objED = Nothing
            objEmpCostCenter = Nothing 'Sohail (26 May 2011)
        End Try

    End Sub

    Private Sub btnGetFileFomat_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnGetFileFomat.Click
        If ConfigParameter._Object._ExportDataPath = "" Then
            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Please set the Export Data Path From Aruti Configuration -> Option -> Paths."), enMsgBoxStyle.Information)
            Exit Sub
        End If

        Dim dlgSaveFile As New SaveFileDialog
        Dim strFilePath As String = String.Empty
        Dim objEd As New clsEarningDeduction
        Dim dsList As DataSet
        Try
            With dlgSaveFile
                .InitialDirectory = ConfigParameter._Object._ExportDataPath
                .Filter = "Excel files(*.xlsx)|*.xlsx"
                .FilterIndex = 0
                .OverwritePrompt = True

                If .ShowDialog = Windows.Forms.DialogResult.OK Then
                    'Sohail (18 Jan 2012) -- Start
                    'TRA - ENHANCEMENT
                    Dim objPeriod As New clscommom_period_Tran
                    'Sohail (21 Aug 2015) -- Start
                    'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                    'objPeriod._Periodunkid = (New clsMasterData).getCurrentPeriodID(enModuleReference.Payroll, eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate))

                    'S.SANDEEP [04 JUN 2015] -- START
                    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                    'objPeriod._Periodunkid(FinancialYear._Object._DatabaseName) = (New clsMasterData).getCurrentPeriodID(enModuleReference.Payroll, eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate))
                    objPeriod._Periodunkid(FinancialYear._Object._DatabaseName) = (New clsMasterData).getCurrentPeriodID(enModuleReference.Payroll, eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), FinancialYear._Object._YearUnkid)
                    'S.SANDEEP [04 JUN 2015] -- END

                    'Sohail (21 Aug 2015) -- End
                    'Sohail (18 Jan 2012) -- End
                    strFilePath = .FileName
                    'Sohail (18 Jan 2012) -- Start
                    'TRA - ENHANCEMENT
                    'dsList = objEd.GetEmpFlatRateHeadList("ED")
                    'Sohail (21 Aug 2015) -- Start
                    'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                    'dsList = objEd.GetEmpFlatRateHeadList("ED", , oboPeriod._End_Date)
                    dsList = objEd.GetEmpFlatRateHeadList(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, objPeriod._Start_Date, objPeriod._End_Date, ConfigParameter._Object._UserAccessModeSetting, True, ConfigParameter._Object._IsIncludeInactiveEmp, "ED", , objPeriod._End_Date, True, "")
                    'Sohail (21 Aug 2015) -- End
                    'Sohail (18 Jan 2012) -- End
                    'S.SANDEEP [12-Jan-2018] -- START
                    'ISSUE/ENHANCEMENT : REF-ID # 0001843
                    'objIExcel.Export(strFilePath, dsList)
                    OpenXML_Export(strFilePath, dsList)
                    'S.SANDEEP [12-Jan-2018] -- END

                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "File Exported Successfully."), enMsgBoxStyle.Information)
                End If
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnGetFileFomat_Click", mstrModuleName)
        End Try
    End Sub

    'Sohail (18 Jan 2012) -- Start
    'TRA - ENHANCEMENT
    Private Sub btnExport_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnExport.Click
        Try
            If m_Dataview Is Nothing Then Exit Sub

            Dim savDialog As New SaveFileDialog
            savDialog.Filter = "Execl files(*.xlsx)|*.xlsx"
            If savDialog.ShowDialog = Windows.Forms.DialogResult.OK Then
                If modGlobal.Export_ErrorList(savDialog.FileName, m_Dataview.ToTable, "Import Employee Wizard") = True Then
                    Process.Start(savDialog.FileName)
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnExport_Click", mstrModuleName)
        End Try
    End Sub
    'Sohail (18 Jan 2012) -- End

#End Region

#Region " Listview's Events "

    'Sohail (18 Jan 2012) -- Start
    'TRA - ENHANCEMENT
    ''Sohail (26 May 2011) -- Start
    'Private Sub lvEmployeeList_ItemCheck(ByVal sender As Object, ByVal e As System.Windows.Forms.ItemCheckEventArgs)
    '    Try
    '        If lvEmployeeList.Items(e.Index).BackColor = Color.Red AndAlso lblUnMatchedEmpColor.Visible = True Then
    '            e.NewValue = CheckState.Unchecked
    '        ElseIf lvEmployeeList.Items(e.Index).BackColor = System.Drawing.SystemColors.ControlDark AndAlso lblUnAssignHeadColor.Visible = True Then
    '            e.NewValue = CheckState.Unchecked
    '        End If
    '    Catch ex As Exception
    '        DisplayError.Show("-1", ex.Message, "lvEmployeeList_ItemCheck", mstrModuleName)
    '    End Try
    'End Sub
    ''Sohail (26 May 2011) -- End

    'Private Sub lvEmployeeList_ItemChecked(ByVal sender As Object, ByVal e As System.Windows.Forms.ItemCheckedEventArgs)

    '    Try
    '        If lvEmployeeList.CheckedItems.Count <= 0 Then
    '            RemoveHandler chkSelectAll.CheckedChanged, AddressOf chkSelectAll_CheckedChanged
    '            chkSelectAll.CheckState = CheckState.Unchecked
    '            AddHandler chkSelectAll.CheckedChanged, AddressOf chkSelectAll_CheckedChanged
    '        ElseIf lvEmployeeList.CheckedItems.Count < lvEmployeeList.Items.Count Then
    '            RemoveHandler chkSelectAll.CheckedChanged, AddressOf chkSelectAll_CheckedChanged
    '            chkSelectAll.CheckState = CheckState.Indeterminate
    '            AddHandler chkSelectAll.CheckedChanged, AddressOf chkSelectAll_CheckedChanged
    '        ElseIf lvEmployeeList.CheckedItems.Count = lvEmployeeList.Items.Count Then
    '            RemoveHandler chkSelectAll.CheckedChanged, AddressOf chkSelectAll_CheckedChanged
    '            chkSelectAll.CheckState = CheckState.Checked
    '            AddHandler chkSelectAll.CheckedChanged, AddressOf chkSelectAll_CheckedChanged
    '        End If
    '    Catch ex As Exception
    '        DisplayError.Show("-1", ex.Message, "lvEmployeeList_ItemChecked", mstrModuleName)
    '    End Try
    'End Sub
    'Sohail (18 Jan 2012) -- End
#End Region

#Region " CheckBox's Events "
    Private Sub chkSelectAll_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkSelectAll.CheckedChanged
        Try
            Call CheckAll(chkSelectAll.Checked)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "chkSelectAll_CheckedChanged", mstrModuleName)
        End Try
    End Sub
#End Region

#Region " Other control's Events "
    Private Sub objbtnOpenFile_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnOpenFile.Click
        Dim ofdlgOpen As New OpenFileDialog
        Dim ObjFile As FileInfo
        Try
            ofdlgOpen.InitialDirectory = ConfigParameter._Object._ExportDataPath
            ofdlgOpen.Filter = "Excel files (*.xlsx)|*.xlsx|XML files (*.xml)|*.xml"
            ofdlgOpen.FilterIndex = 0

            If ofdlgOpen.ShowDialog = Windows.Forms.DialogResult.OK Then
                ObjFile = New FileInfo(ofdlgOpen.FileName)
                txtFilePath.Text = ofdlgOpen.FileName

                Select Case ofdlgOpen.FilterIndex
                    Case 1, 2
                        'S.SANDEEP [12-Jan-2018] -- START
                        'ISSUE/ENHANCEMENT : REF-ID # 0001843
                        'dsList = objIExcel.Import(txtFilePath.Text)
                        dsList = OpenXML_Import(txtFilePath.Text)
                        'S.SANDEEP [12-Jan-2018] -- END
                End Select
                Dim frm As New frmTransactionHeadMapping
                If frm.displayDialog(dsList) = False Then
                    mblnCancel = True 'Sohail (26 May 2011)
                    Exit Sub
                End If
                'Sohail (26 May 2011) -- Start
                'mdtTable = frm._DataTable
                'Sohail (18 Jan 2012) -- Start
                'TRA - ENHANCEMENT
                'mdtTable = New DataView(frm._DataTable, "", "rowtypeid", DataViewRowState.CurrentRows).ToTable
                mdtTable = frm._DataTable
                m_Dataview = New DataView(mdtTable)
                m_Dataview.RowFilter = "rowtypeid = 0 "
                'Sohail (18 Jan 2012) -- End
                'Sohail (26 May 2011) -- End

                'Sohail (18 Jan 2012) -- Start
                'TRA - ENHANCEMENT
                'Call FillList()
                Call FillGirdView()
                'Sohail (18 Jan 2012) -- End
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnOpenFile_Click", mstrModuleName)
        End Try
    End Sub

    'Sohail (18 Jan 2012) -- Start
    'TRA - ENHANCEMENT
    Private Sub mnuShowSuccessful_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuShowSuccessful.Click
        Try
            btnImport.Enabled = True
            If m_Dataview Is Nothing Then Exit Sub

            m_Dataview.RowFilter = "rowtypeid = 0 "

            For Each dtRow As DataRowView In m_Dataview
                dtRow.Item("IsChecked") = False
            Next

            Call FillGirdView()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "mnuShowSuccessful_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub mnuEmployees_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuEmployees.Click
        Try
            btnImport.Enabled = False
            If m_Dataview Is Nothing Then Exit Sub

            m_Dataview.RowFilter = "rowtypeid = 2 "

            For Each dtRow As DataRowView In m_Dataview
                dtRow.Item("IsChecked") = False
            Next

            Call FillGirdView()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "mnuEmployees_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub ShowUnassignedEDHeadsToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ShowUnassignedEDHeadsToolStripMenuItem.Click
        Try
            btnImport.Enabled = False
            If m_Dataview Is Nothing Then Exit Sub

            m_Dataview.RowFilter = "rowtypeid = 1 "

            For Each dtRow As DataRowView In m_Dataview
                dtRow.Item("IsChecked") = False
            Next

            Call FillGirdView()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "ShowUnassignedEDHeadsToolStripMenuItem_Click", mstrModuleName)
        End Try
    End Sub
    'Sohail (18 Jan 2012) -- End

    'Sohail (16 Oct 2012) -- Start
    'TRA - ENHANCEMENT
    Private Sub mnuPayrollProcessed_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuPayrollProcessed.Click
        Try
            btnImport.Enabled = False
            If m_Dataview Is Nothing Then Exit Sub

            m_Dataview.RowFilter = "rowtypeid = 3 "

            For Each dtRow As DataRowView In m_Dataview
                dtRow.Item("IsChecked") = False
            Next

            Call FillGirdView()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "mnuPayrollProcessed_Click", mstrModuleName)
        End Try
    End Sub
    'Sohail (16 Oct 2012) -- End
#End Region

#Region " Messages "
    '1, "Please Check atleast one transaction to import."
    '2, "Are you sure ,you want to Update Flate Rate Head to selected Employee(s)?"
    '3, "Please set the Export Data Path From Aruti Configuration -> Option -> Paths."
    '4, "File Exported Successfully."
#End Region

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Private Sub OtherSettings()
        Try
            Me.SuspendLayout()
           
            Call SetLanguage()

            Me.gbFileInfo.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbFileInfo.ForeColor = GUI._eZeeContainerHeaderForeColor


            Me.btnClose.GradientBackColor = GUI._ButttonBackColor
            Me.btnClose.GradientForeColor = GUI._ButttonFontColor

            Me.btnImport.GradientBackColor = GUI._ButttonBackColor
            Me.btnImport.GradientForeColor = GUI._ButttonFontColor

            Me.btnGetFileFomat.GradientBackColor = GUI._ButttonBackColor
            Me.btnGetFileFomat.GradientForeColor = GUI._ButttonFontColor

            Me.btnHeadOperations.GradientBackColor = GUI._ButttonBackColor
            Me.btnHeadOperations.GradientForeColor = GUI._ButttonFontColor

            Me.btnExport.GradientBackColor = GUI._ButttonBackColor
            Me.btnExport.GradientForeColor = GUI._ButttonFontColor


            Me.ResumeLayout()
        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "OtherSettings", mstrModuleName)
        End Try
    End Sub


    Private Sub SetLanguage()
        Try
            Me.Text = Language._Object.getCaption(Me.Name, Me.Text)

            Me.gbFileInfo.Text = Language._Object.getCaption(Me.gbFileInfo.Name, Me.gbFileInfo.Text)
            Me.lblFileName.Text = Language._Object.getCaption(Me.lblFileName.Name, Me.lblFileName.Text)
            Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
            Me.btnImport.Text = Language._Object.getCaption(Me.btnImport.Name, Me.btnImport.Text)
            Me.DataGridViewTextBoxColumn1.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn1.Name, Me.DataGridViewTextBoxColumn1.HeaderText)
            Me.DataGridViewTextBoxColumn2.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn2.Name, Me.DataGridViewTextBoxColumn2.HeaderText)
            Me.DataGridViewTextBoxColumn3.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn3.Name, Me.DataGridViewTextBoxColumn3.HeaderText)
            Me.DataGridViewTextBoxColumn4.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn4.Name, Me.DataGridViewTextBoxColumn4.HeaderText)
            Me.DataGridViewTextBoxColumn5.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn5.Name, Me.DataGridViewTextBoxColumn5.HeaderText)
            Me.DataGridViewTextBoxColumn6.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn6.Name, Me.DataGridViewTextBoxColumn6.HeaderText)
            Me.btnGetFileFomat.Text = Language._Object.getCaption(Me.btnGetFileFomat.Name, Me.btnGetFileFomat.Text)
            Me.lblUnAssignHead.Text = Language._Object.getCaption(Me.lblUnAssignHead.Name, Me.lblUnAssignHead.Text)
            Me.lblUnAssignHeadColor.Text = Language._Object.getCaption(Me.lblUnAssignHeadColor.Name, Me.lblUnAssignHeadColor.Text)
            Me.lblUnMatchedEmp.Text = Language._Object.getCaption(Me.lblUnMatchedEmp.Name, Me.lblUnMatchedEmp.Text)
            Me.lblUnMatchedEmpColor.Text = Language._Object.getCaption(Me.lblUnMatchedEmpColor.Name, Me.lblUnMatchedEmpColor.Text)
            Me.chkSelectAll.Text = Language._Object.getCaption(Me.chkSelectAll.Name, Me.chkSelectAll.Text)
            Me.btnHeadOperations.Text = Language._Object.getCaption(Me.btnHeadOperations.Name, Me.btnHeadOperations.Text)
            Me.mnuShowSuccessful.Text = Language._Object.getCaption(Me.mnuShowSuccessful.Name, Me.mnuShowSuccessful.Text)
            Me.mnuEmployees.Text = Language._Object.getCaption(Me.mnuEmployees.Name, Me.mnuEmployees.Text)
            Me.ShowUnassignedEDHeadsToolStripMenuItem.Text = Language._Object.getCaption(Me.ShowUnassignedEDHeadsToolStripMenuItem.Name, Me.ShowUnassignedEDHeadsToolStripMenuItem.Text)
            Me.DataGridViewTextBoxColumn7.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn7.Name, Me.DataGridViewTextBoxColumn7.HeaderText)
            Me.btnExport.Text = Language._Object.getCaption(Me.btnExport.Name, Me.btnExport.Text)
            Me.dgcolhEmpCode.HeaderText = Language._Object.getCaption(Me.dgcolhEmpCode.Name, Me.dgcolhEmpCode.HeaderText)
            Me.dgcolhEmpName.HeaderText = Language._Object.getCaption(Me.dgcolhEmpName.Name, Me.dgcolhEmpName.HeaderText)
            Me.dgcolhTranHead.HeaderText = Language._Object.getCaption(Me.dgcolhTranHead.Name, Me.dgcolhTranHead.HeaderText)
            Me.dgcolhAmount.HeaderText = Language._Object.getCaption(Me.dgcolhAmount.Name, Me.dgcolhAmount.HeaderText)
            Me.dgcolhCostCenter.HeaderText = Language._Object.getCaption(Me.dgcolhCostCenter.Name, Me.dgcolhCostCenter.HeaderText)
            Me.dgcolhMedicalRefCode.HeaderText = Language._Object.getCaption(Me.dgcolhMedicalRefCode.Name, Me.dgcolhMedicalRefCode.HeaderText)
            Me.mnuPayrollProcessed.Text = Language._Object.getCaption(Me.mnuPayrollProcessed.Name, Me.mnuPayrollProcessed.Text)

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
        End Try
    End Sub


    Private Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Please Tick atleast one transaction from list to Import.")
            Language.setMessage(mstrModuleName, 2, "Are you sure, you want to Update Flat Rate Head to selected Employee(s)?")
            Language.setMessage(mstrModuleName, 3, "Please set the Export Data Path From Aruti Configuration -> Option -> Paths.")
            Language.setMessage(mstrModuleName, 4, "File Exported Successfully.")
            Language.setMessage(mstrModuleName, 5, "UnAssigned Transaction Head")
            Language.setMessage(mstrModuleName, 6, "UnMatched Employee Code")
            Language.setMessage(mstrModuleName, 7, "Sorry, you cannot import this file. Reason :Some transaction Codes or Employee Codes are not in the system.")
            Language.setMessage(mstrModuleName, 8, "Sorry, you cannot import this file. Reason :Assigned heads are not in this import file.")
			Language.setMessage(mstrModuleName, 9, "Sending Email(s) process is in progress from other module. Please wait.")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class