﻿Option Strict On

#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data

#End Region

Public Class frmTransactionHeadMapping

#Region " Private Variables "
    Private ReadOnly mstrModuleName As String = "frmEDTranHeadMapping"
    Private dsData As New DataSet
    Private mblnCancel As Boolean = True
    Private dtTable As DataTable
    Private dsDataList As New DataSet
    Dim objEmp As clsEmployee_Master
    Dim objTranHead As clsTransactionHead

    'Sohail (16 Oct 2012) -- Start
    'TRA - ENHANCEMENT
    Private mdtPeriodStartDate As DateTime
    Private mdtPeriodEndDate As DateTime
    'Sohail (16 Oct 2012) -- End
#End Region

#Region " Properties "

    Public ReadOnly Property _DataTable() As DataTable
        Get
            Return dtTable
        End Get
    End Property

#End Region

#Region " Display Dialog "

    Public Function displayDialog(ByVal dsList As DataSet) As Boolean
        Try
            dsData = dsList
            Me.ShowDialog()
            Return Not mblnCancel
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "displayDialog", mstrModuleName)
        End Try
    End Function

#End Region

#Region " Private Methods "

    Private Sub SetColor()
        Try
            cboEmployeeCode.BackColor = GUI.ColorComp 'Sohail (26 May 2011)
            cboPeriod.BackColor = GUI.ColorComp 'Sohail (18 Jan 2012) 
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetColor", mstrModuleName)
        End Try
    End Sub
    Private Sub FilCombo()
        'Sohail (26 May 2011) -- Start
        'Dim objMaster As New clsMasterData
        'Dim dsCombo As DataSet 
        'Sohail (26 May 2011) -- End
        'Sohail (18 Jan 2012) -- Start
        'TRA - ENHANCEMENT
        Dim dsCombos As New DataSet
        Dim objPeriod As New clscommom_period_Tran
        'Sohail (18 Jan 2012) -- End
        Try
            'Sohail (26 May 2011) -- Start
            'dsCombo = objMaster.getComboListForHeadType("HeadType")
            'With cboTranHeadType
            '    .ValueMember = "Id"
            '    .DisplayMember = "Name"
            '    .DataSource = dsCombo.Tables("HeadType")
            '    If .Items.Count > 0 Then .SelectedValue = 0
            'End With

            'dsCombo = objTranHead.getComboList("TranHead", True, , enCalcType.FlatRate_Others, -1)
            'With cboTransactionHead
            '    .ValueMember = "tranheadunkid"
            '    .DisplayMember = "Name"
            '    .DataSource = dsCombo.Tables("TranHead")
            '    If .Items.Count > 0 Then .SelectedValue = 0
            'End With
            'Sohail (26 May 2011) -- End

            'Sohail (18 Jan 2012) -- Start
            'TRA - ENHANCEMENT
            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'dsCombos = objPeriod.getListForCombo(enModuleReference.Payroll, FinancialYear._Object._YearUnkid, "Period", True, enStatusType.Open)
            dsCombos = objPeriod.getListForCombo(enModuleReference.Payroll, FinancialYear._Object._YearUnkid, FinancialYear._Object._DatabaseName, FinancialYear._Object._Database_Start_Date, "Period", True, enStatusType.Open)
            'Sohail (21 Aug 2015) -- End
            With cboPeriod
                .BeginUpdate()
                .ValueMember = "periodunkid"
                .DisplayMember = "name"
                .DataSource = dsCombos.Tables("Period")
                .SelectedValue = 0
                .EndUpdate()
            End With
            'Sohail (18 Jan 2012) -- End

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FiilCombo", mstrModuleName)
            'Sohail (18 Jan 2012) -- Start
            'TRA - ENHANCEMENT
        Finally
            objPeriod = Nothing
            'Sohail (18 Jan 2012) -- End
        End Try
    End Sub

    Private Sub SetData()
        Try
            dtTable = New DataTable("ED")
            For Each dtColumns As DataColumn In dsData.Tables(0).Columns
                'cboAmount.Items.Add(dtColumns.ColumnName)
                cboEmployeeCode.Items.Add(dtColumns.ColumnName)
                'cboTranHead.Items.Add(dtColumns.ColumnName) 'Sohail (26 May 2011)
            Next

            dtTable.Columns.Add("edunkid", System.Type.GetType("System.Int32")).DefaultValue = -1
            dtTable.Columns.Add("employeeunkid", System.Type.GetType("System.Int32")).DefaultValue = -1
            dtTable.Columns.Add("employeecode", System.Type.GetType("System.String")).DefaultValue = ""
            dtTable.Columns.Add("tranheadunkid", System.Type.GetType("System.Int32")).DefaultValue = -1
            dtTable.Columns.Add("batchtransactionunkid", System.Type.GetType("System.Int32")).DefaultValue = -1
            dtTable.Columns.Add("amount", System.Type.GetType("System.Decimal")).DefaultValue = 0
            dtTable.Columns.Add("isdeduct", System.Type.GetType("System.Boolean")).DefaultValue = 0
            dtTable.Columns.Add("trnheadtype_id", System.Type.GetType("System.Int32")).DefaultValue = -1
            dtTable.Columns.Add("typeof_id", System.Type.GetType("System.Int32")).DefaultValue = -1
            dtTable.Columns.Add("calctype_id", System.Type.GetType("System.Int32")).DefaultValue = enCalcType.FlatRate_Others
            dtTable.Columns.Add("computeon_id", System.Type.GetType("System.Int32")).DefaultValue = -1
            dtTable.Columns.Add("formula", System.Type.GetType("System.String")).DefaultValue = ""
            dtTable.Columns.Add("formulaid", System.Type.GetType("System.String")).DefaultValue = ""
            dtTable.Columns.Add("currencyunkid", System.Type.GetType("System.Int32")).DefaultValue = 1
            dtTable.Columns.Add("vendorunkid", System.Type.GetType("System.Int32")).DefaultValue = -1
            dtTable.Columns.Add("userunkid", System.Type.GetType("System.Int32")).DefaultValue = User._Object._Userunkid
            dtTable.Columns.Add("isvoid", System.Type.GetType("System.Boolean")).DefaultValue = False
            dtTable.Columns.Add("voiddatetime", System.Type.GetType("System.DateTime"))
            dtTable.Columns.Add("voidreason", System.Type.GetType("System.String")).DefaultValue = ""
            dtTable.Columns.Add("membership_categoryunkid", System.Type.GetType("System.Int32")).DefaultValue = -1
            dtTable.Columns.Add("employeename", System.Type.GetType("System.String")).DefaultValue = ""
            dtTable.Columns.Add("TranHeadName", System.Type.GetType("System.String")).DefaultValue = ""
            dtTable.Columns.Add("HeadType", System.Type.GetType("System.String")).DefaultValue = ""
            dtTable.Columns.Add("TypeOf", System.Type.GetType("System.String")).DefaultValue = ""
            'Sohail (26 May 2011) -- Start
            dtTable.Columns.Add("costcenterunkid", System.Type.GetType("System.Int32")).DefaultValue = -1
            dtTable.Columns.Add("costcentername", System.Type.GetType("System.String")).DefaultValue = ""
            dtTable.Columns.Add("rowtypeid", System.Type.GetType("System.Int32")).DefaultValue = -1
            'Sohail (26 May 2011) -- End
            dtTable.Columns.Add("IsChecked", System.Type.GetType("System.Boolean")).DefaultValue = False
            'Sohail (18 Jan 2012) -- Start
            'TRA - ENHANCEMENT
            dtTable.Columns.Add("periodunkid", System.Type.GetType("System.Int32")).DefaultValue = -1
            dtTable.Columns.Add("medicalrefno", System.Type.GetType("System.String")).DefaultValue = ""
            'Sohail (18 Jan 2012) -- End
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetData", mstrModuleName)
        End Try
    End Sub

    'Sohail (26 May 2011) -- Start
    Private Sub SetupGrid()
        Dim objTranHead As New clsTransactionHead
        Dim objCC As New clscostcenter_master
        Dim ds As DataSet
        Try
            'Sohail (29 Oct 2012) -- Start
            'TRA - ENHANCEMENT
            'ds = objTranHead.getComboList("TranHead", True, , enCalcType.FlatRate_Others)
            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'ds = objTranHead.getComboList("TranHead", True, , enCalcType.FlatRate_Others, , , True)
            'Sohail (21 Dec 2016) -- Start
            'CCK Enhancement - 64.1 - Allow to import updating Flat Rate membership heads on Update Flat Rate ED heads screen.
            'ds = objTranHead.getComboList(FinancialYear._Object._DatabaseName, "TranHead", True, , enCalcType.FlatRate_Others, , , True)
            ds = objTranHead.getComboList(FinancialYear._Object._DatabaseName, "TranHead", True, , enCalcType.FlatRate_Others, , , False)
            'Sohail (21 Dec 2016) -- End
            'Sohail (21 Aug 2015) -- End
            'Sohail (29 Oct 2012) -- End
            dgvMapping.AutoGenerateColumns = False

            Dim cboHeads As DataGridViewComboBoxColumn
            cboHeads = CType(dgvMapping.Columns(colhDBHeads.Index), DataGridViewComboBoxColumn)
            With cboHeads
                .DisplayStyle = DataGridViewComboBoxDisplayStyle.Nothing
                .DisplayMember = "name"
                .ValueMember = "tranheadunkid"
                .DataSource = ds.Tables("TranHead")
            End With

            Dim cboCostCenter As DataGridViewComboBoxColumn
            ds = objCC.getComboList("CostCenter", True)
            cboCostCenter = CType(dgvMapping.Columns(colhCostCenter.Index), DataGridViewComboBoxColumn)
            With cboCostCenter
                .DisplayStyle = DataGridViewComboBoxDisplayStyle.Nothing
                .DisplayMember = "costcentercode"
                .ValueMember = "costcenterunkid"
                .DataSource = ds.Tables("CostCenter")
            End With

            Dim dRow As DataGridViewRow
            dgvMapping.Rows.Clear()
            For Each dCol As DataColumn In dsData.Tables(0).Columns
                If dCol.ColumnName <> cboEmployeeCode.Text Then
                    dRow = New DataGridViewRow

                    dRow.CreateCells(dgvMapping)
                    dRow.Cells(0).Value = dCol.ColumnName
                    dgvMapping.Rows.Add(dRow)
                End If
            Next

            colhFileFields.DataPropertyName = ""
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetupGrid", mstrModuleName)
        End Try
    End Sub
    'Sohail (26 May 2011) -- End

#End Region

#Region " Form's Events "

    Private Sub frmTransactionHeadMapping_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        Try
            objEmp = Nothing
            objTranHead = Nothing
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmTransactionHeadMapping_FormClosed", mstrModuleName)
        End Try
    End Sub
    Private Sub frmTransactionHeadMapping_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        objEmp = New clsEmployee_Master
        objTranHead = New clsTransactionHead
        Try
            Call Set_Logo(Me, gApplicationType)

            'Anjan (02 Sep 2011)-Start
            'Issue : Including Language Settings.
            Language.setLanguage(Me.Name)
            Call OtherSettings()
            'Anjan (02 Sep 2011)-End 


            cboEmployeeCode.Items.Clear()
            'cboTranHead.Items.Clear()'Sohail (26 May 2011)

            Call SetColor() 'Sohail (26 May 2011)
            Call FilCombo()
            Call SetData()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmTransactionHeadMapping_Load", mstrModuleName)
        End Try
    End Sub

    'Anjan (02 Sep 2011)-Start
    'Issue : Including Language Settings.
    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub
    'Anjan (02 Sep 2011)-End 


#End Region

#Region " ComboBox's Events "

    'Sohail (26 May 2011) -- Start
    'Private Sub cboTranHeadType_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
    '    Dim objMaster As New clsMasterData
    '    Dim dsCombo As DataSet
    '    Dim dtTbl As DataTable
    '    Try
    '        dsCombo = objMaster.getComboListTypeOf("TypeOf", CInt(cboTranHeadType.SelectedValue))
    '        dtTbl = New DataView(dsCombo.Tables("TypeOf"), "Id <>  " & enTypeOf.Salary & "", "", DataViewRowState.CurrentRows).ToTable 'Except Salary Heads

    '        With cboTypeOf
    '            .ValueMember = "Id"
    '            .DisplayMember = "Name"
    '            .DataSource = dtTbl
    '            If .Items.Count > 0 Then .SelectedValue = 0
    '        End With

    '    Catch ex As Exception
    '        DisplayError.Show("-1", ex.Message, "cboTranHeadType_SelectedIndexChanged", mstrModuleName)
    '    Finally
    '        objMaster = Nothing
    '    End Try
    'End Sub

    'Private Sub cboTypeOf_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
    '    Dim dsCombo As DataSet
    '    Try
    '        dsCombo = objTranHead.getComboList("TranHead", True, CInt(cboTranHeadType.SelectedValue), enCalcType.FlatRate_Others, CInt(IIf(CInt(cboTypeOf.SelectedValue) > 0, CInt(cboTypeOf.SelectedValue), -1)))
    '        With cboTransactionHead
    '            .ValueMember = "tranheadunkid"
    '            .DisplayMember = "Name"
    '            .DataSource = dsCombo.Tables("TranHead")
    '            If .Items.Count > 0 Then .SelectedValue = 0
    '        End With
    '    Catch ex As Exception
    '        DisplayError.Show("-1", ex.Message, "cboTypeOf_SelectedIndexChanged", mstrModuleName)
    '    End Try
    'End Sub

    Private Sub cboEmployeeCode_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboEmployeeCode.SelectedIndexChanged
        Try
            If CInt(cboEmployeeCode.SelectedIndex) < 0 Then
                dgvMapping.Rows.Clear()
                Exit Sub
            End If
            Call SetupGrid()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboEmployeeCode_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub
    'Sohail (26 May 2011) -- End

    'Sohail (16 Oct 2012) -- Start
    'TRA - ENHANCEMENT
    Private Sub cboPeriod_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboPeriod.SelectedIndexChanged
        Dim objPeriod As New clscommom_period_Tran
        Dim dsList As DataSet = Nothing
        Try
            If CInt(cboPeriod.SelectedValue) > 0 Then
                'Sohail (21 Aug 2015) -- Start
                'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                'objPeriod._Periodunkid = CInt(cboPeriod.SelectedValue)
                objPeriod._Periodunkid(FinancialYear._Object._DatabaseName) = CInt(cboPeriod.SelectedValue)
                'Sohail (21 Aug 2015) -- End
                mdtPeriodStartDate = objPeriod._Start_Date
                mdtPeriodEndDate = objPeriod._End_Date
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboPeriod_SelectedIndexChanged", mstrModuleName)
        Finally
            objPeriod = Nothing
            dsList = Nothing
        End Try
    End Sub
    'Sohail (16 Oct 2012) -- End
#End Region

#Region " Buttons Events "

    Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Try
            mblnCancel = True
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnCancel_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnOk_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnOk.Click
        Dim objED As New clsEarningDeduction
        Dim dsEmployee As DataSet
        Dim dtEmployee As DataTable
        Dim intEDunkId As Integer
        Dim strAmount As String = ""
        Dim strColumn As String = "" 'Sohail (26 May 2011)
        Dim objTnA As New clsTnALeaveTran 'Sohail (16 Oct 2012)

        Try
            'Sohail (18 Jan 2012) -- Start
            'TRA - ENHANCEMENT
            If CInt(cboPeriod.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 9, "Please select Period. Period is mandatory information."), enMsgBoxStyle.Information)
                cboPeriod.Focus()
                Exit Try
            End If
            'Sohail (18 Jan 2012) -- End

            If CInt(cboEmployeeCode.SelectedIndex) < 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please select employee code column."), enMsgBoxStyle.Information)
                cboEmployeeCode.Focus()
                Exit Try
                'Sohail (26 May 2011) -- Start
                'ElseIf CInt(cboTranHead.SelectedIndex) < 0 Then
                '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Please select Transaction Head column."), enMsgBoxStyle.Information)
                '    cboTranHead.Focus()
                '    Exit Try
                'ElseIf CInt(cboTransactionHead.SelectedValue) <= 0 Then
                '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Please select Transaction Head to map."), enMsgBoxStyle.Information)
                '    cboTransactionHead.Focus()
                '    Exit Try
                'Sohail (26 May 2011) -- End
            End If

            'Sohail (26 May 2011) -- Start
            Dim blnMapFound As Boolean = False
            For Each dgRow As DataGridViewRow In dgvMapping.Rows
                If Convert.ToInt32(dgRow.Cells(colhDBHeads.Index).Value) > 0 Then
                    blnMapFound = True
                    Exit For
                End If
            Next
            If blnMapFound = False Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 8, "Please Map atleast one Database field with File field."), enMsgBoxStyle.Information)
                dgvMapping.Focus()
                Exit Try
            End If
            'Sohail (26 May 2011) -- End

            mblnCancel = True
            'Sohail (23 Nov 2012) -- Start
            'TRA - ENHANCEMENT
            'dsEmployee = objEmp.GetEmployeeList("Employee", False, True)
            'Anjan [10 June 2015] -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS

            'dsEmployee = objEmp.GetEmployeeList("Employee", False, True, , , , , , , , , , , , mdtPeriodStartDate, mdtPeriodEndDate, , , , , , True)
            dsEmployee = objEmp.GetEmployeeList(FinancialYear._Object._DatabaseName, _
                                           User._Object._Userunkid, _
                                           FinancialYear._Object._YearUnkid, _
                                           Company._Object._Companyunkid, _
                                           mdtPeriodStartDate, _
                                           mdtPeriodEndDate, _
                                           ConfigParameter._Object._UserAccessModeSetting, _
                                           True, False, "Employee", False)

            'Sohail (23 Nov 2012) -- End
            'Anjan [10 June 2015] -- End

            'objTranHead._Tranheadunkid = CInt(cboTransactionHead.SelectedValue) 'Sohail (26 May 2011)

            'Sohail (26 May 2011) -- Start
            For Each dgRow As DataGridViewRow In dgvMapping.Rows

                If Convert.ToInt32(dgRow.Cells(colhDBHeads.Index).Value) > 0 Then


                    objTranHead = New clsTransactionHead
                    'Sohail (21 Aug 2015) -- Start
                    'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                    'objTranHead._Tranheadunkid = Convert.ToInt32(dgRow.Cells(colhDBHeads.Index).Value)
                    objTranHead._Tranheadunkid(FinancialYear._Object._DatabaseName) = Convert.ToInt32(dgRow.Cells(colhDBHeads.Index).Value)
                    'Sohail (21 Aug 2015) -- End
                    'Sohail (26 May 2011) -- End

            For Each dsRow As DataRow In dsData.Tables(0).Rows
                Dim dRow As DataRow

                        'Sohail (26 May 2011) -- Start
                        'strAmount = dsRow.Item(cboTranHead.Text).ToString
                        strColumn = dgRow.Cells(colhFileFields.Index).FormattedValue.ToString
                        strAmount = dsRow.Item(strColumn).ToString
                        'Sohail (26 May 2011) -- End
                        If strAmount.Trim = "" Then Continue For

                dtEmployee = New DataView(dsEmployee.Tables("Employee"), "employeecode = '" & dsRow.Item(cboEmployeeCode.Text).ToString.Trim & "'", "", DataViewRowState.CurrentRows).ToTable

                        dRow = dtTable.NewRow

                If dtEmployee.Rows.Count > 0 Then

                            'Sohail (16 Oct 2012) -- Start
                            'TRA - ENHANCEMENT
                            If objTnA.IsPayrollProcessDone(CInt(cboPeriod.SelectedValue), dtEmployee.Rows(0).Item("employeeunkid").ToString, mdtPeriodEndDate) = True Then
                                dRow.Item("edunkid") = 0
                                dRow.Item("employeeunkid") = CInt(dtEmployee.Rows(0).Item("employeeunkid"))
                                dRow.Item("employeecode") = dtEmployee.Rows(0).Item("employeecode").ToString
                                dRow.Item("employeename") = dtEmployee.Rows(0).Item("employeename").ToString
                                dRow.Item("TranHeadName") = objTranHead._Trnheadname
                                dRow.Item("costcentername") = ""
                                dRow.Item("amount") = CDec(strAmount)
                                dRow.Item("rowtypeid") = 3 'Process Payroll is already done for last date of selected Period for current the employee.

                                dtTable.Rows.Add(dRow)
                            Else
                                'Sohail (16 Oct 2012) -- End

                            'Sohail (26 May 2011) -- Start
                            'intEDunkId = objED.GetEDUnkID(CInt(dtEmployee.Rows(0).Item("employeeunkid")), CInt(cboTransactionHead.SelectedValue))
                            'Sohail (18 Jan 2012) -- Start
                            'TRA - ENHANCEMENT
                            'intEDunkId = objED.GetEDUnkID(CInt(dtEmployee.Rows(0).Item("employeeunkid")), Convert.ToInt32(dgRow.Cells(colhDBHeads.Index).Value))
                                'Sohail (21 Aug 2015) -- Start
                                'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                                'intEDunkId = objED.GetEDUnkID(CInt(dtEmployee.Rows(0).Item("employeeunkid")), Convert.ToInt32(dgRow.Cells(colhDBHeads.Index).Value), CInt(cboPeriod.SelectedValue))
                                intEDunkId = objED.GetEDUnkID(FinancialYear._Object._DatabaseName, CInt(dtEmployee.Rows(0).Item("employeeunkid")), Convert.ToInt32(dgRow.Cells(colhDBHeads.Index).Value), CInt(cboPeriod.SelectedValue))
                                'Sohail (21 Aug 2015) -- End
                            'Sohail (18 Jan 2012) -- End
                            'Sohail (26 May 2011) -- End

                    If intEDunkId > 0 Then

                        dRow.Item("edunkid") = intEDunkId
                        dRow.Item("employeeunkid") = CInt(dtEmployee.Rows(0).Item("employeeunkid"))
                        dRow.Item("employeecode") = dtEmployee.Rows(0).Item("employeecode").ToString
                        dRow.Item("employeename") = dtEmployee.Rows(0).Item("employeename").ToString
                                'Sohail (26 May 2011) -- Start
                                'dRow.Item("tranheadunkid") = CInt(cboTransactionHead.SelectedValue)
                                dRow.Item("tranheadunkid") = Convert.ToInt32(dgRow.Cells(colhDBHeads.Index).Value)
                                'Sohail (26 May 2011) -- End
                                dRow.Item("amount") = CDec(strAmount)
                                dRow.Item("TranHeadName") = objTranHead._Trnheadname
                                dRow.Item("userunkid") = User._Object._Userunkid
                                'Sohail (26 May 2011) -- Start
                                If Convert.ToInt32(dgRow.Cells(colhCostCenter.Index).Value) > 0 Then
                                    dRow.Item("costcenterunkid") = Convert.ToInt32(dgRow.Cells(colhCostCenter.Index).Value)
                                    dRow.Item("costcentername") = dgRow.Cells(colhCostCenter.Index).FormattedValue.ToString
                                Else
                                    dRow.Item("costcenterunkid") = -1
                                    dRow.Item("costcentername") = ""
                                End If
                                dRow.Item("rowtypeid") = 0 'proper entry
                                'Sohail (26 May 2011) -- End

                                'Sohail (18 Jan 2012) -- Start
                                'TRA - ENHANCEMENT
                                dRow.Item("periodunkid") = CInt(cboPeriod.SelectedValue)
                                dRow.Item("medicalrefno") = ""
                                'Sohail (18 Jan 2012) -- End

                                dtTable.Rows.Add(dRow)
                            Else 'intEDunkId <= 0 (not found in ED)
                                dRow.Item("edunkid") = 0
                                dRow.Item("employeeunkid") = CInt(dtEmployee.Rows(0).Item("employeeunkid"))
                                dRow.Item("employeecode") = dtEmployee.Rows(0).Item("employeecode").ToString
                                dRow.Item("employeename") = dtEmployee.Rows(0).Item("employeename").ToString
                                dRow.Item("TranHeadName") = objTranHead._Trnheadname
                                dRow.Item("costcentername") = ""
                                dRow.Item("amount") = CDec(strAmount)
                                dRow.Item("rowtypeid") = 1 'Unassigned heads on Ed

                                dtTable.Rows.Add(dRow)
                            End If
                            End If
                        Else 'dtEmployee.Rows.Count <= 0 (not found in employee master
                            dRow.Item("edunkid") = 0
                            dRow.Item("employeeunkid") = 0
                            dRow.Item("employeecode") = dsRow.Item(cboEmployeeCode.Text).ToString.Trim
                            dRow.Item("employeename") = ""
                            dRow.Item("TranHeadName") = objTranHead._Trnheadname
                            'Sohail (31 Oct 2019) -- Start
                            'Mainspring Resourcing Ghana : Support Issue Id # 0004199 - Email notification not sent to approvers when there is uploaded, edited e & d.
                            dRow.Item("calctype_id") = objTranHead._Calctype_Id
                            'Sohail (31 Oct 2019) -- End
                            dRow.Item("costcentername") = ""
                            dRow.Item("amount") = CDec(strAmount)
                            dRow.Item("rowtypeid") = 2 'employee code not found

                            dtTable.Rows.Add(dRow)
                End If
            Next
                End If 'Sohail (26 May 2011)
            Next 'Sohail (26 May 2011)

            If dtTable.Rows.Count > 0 Then
                mblnCancel = False
                Me.Close()
            Else
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 7, "Sorry, Employee Code and Transaction Head are not matching from file!"), enMsgBoxStyle.Exclamation)
            End If

        Catch ex As Exception
            'Sohail (10 Dec 2019) -- Start
            'MAIN SPRING RESOURCES Support Issue Id #  : Object reference error instead of actual error when there is no inner exception thrown.
            'If ex.InnerException.Message.ToString = "Input string was not in a correct format." Then
            If ex.InnerException IsNot Nothing AndAlso ex.InnerException.Message.ToString = "Input string was not in a correct format." Then
                'Sohail (10 Dec 2019) -- End
                'Sohail (26 May 2011) -- Start
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Invalid Data [Non-Numeric amount ") & strAmount & Language.getMessage(mstrModuleName, 3, "] found in File for column of  ") & strColumn & ".", enMsgBoxStyle.Information)
                'Sohail (26 May 2011) -- End
            Else
                DisplayError.Show("-1", ex.Message, "btnOk_Click", mstrModuleName)
            End If
        Finally
            objED = Nothing
            dsEmployee = Nothing
        End Try
    End Sub
#End Region

#Region " Other Control's Events "
    'Sohail (26 May 2011) -- Start
    'Private Sub objbtnSearchTranHead_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)

    '    Dim objfrm As New frmCommonSearch
    '    Dim objEmployee As New clsTransactionHead
    '    Dim dsList As DataSet
    '    Try
    '        'If User._Object._RightToLeft = True Then
    '        '    objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
    '        '    objfrm.RightToLeftLayout = True
    '        '    Call Language.ctlRightToLeftlayOut(objfrm)
    '        'End If
    '        dsList = objEmployee.getComboList("TranHead", False, CInt(cboTranHeadType.SelectedValue), enCalcType.FlatRate_Others, CInt(IIf(CInt(cboTypeOf.SelectedValue) > 0, CInt(cboTypeOf.SelectedValue), -1)))

    '        With cboTransactionHead
    '            objfrm.DataSource = dsList.Tables("TranHead")
    '            objfrm.ValueMember = .ValueMember
    '            objfrm.DisplayMember = .DisplayMember
    '            objfrm.CodeMember = "code"
    '            If objfrm.DisplayDialog Then
    '                .SelectedValue = objfrm.SelectedValue
    '            End If
    '            .Focus()
    '        End With
    '    Catch ex As Exception
    '        DisplayError.Show("-1", ex.Message, "objbtnSearchTranHead_Click", mstrModuleName)
    '    Finally
    '        objfrm = Nothing
    '        objEmployee = Nothing
    '    End Try
    'End Sub
    'Sohail (26 May 2011) -- End

    'Sohail (23 Dec 2014) -- Start
    'Enhancement - Provide Auto Map option on head mapping on Update Falt rate heads on ED.
    Private Sub lnkAutoMap_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lnkAutoMap.LinkClicked
        Try
            Dim dTable As DataTable = CType(CType(dgvMapping.Columns(colhDBHeads.Index), DataGridViewComboBoxColumn).DataSource, DataTable)
            For Each dRow As DataGridViewRow In dgvMapping.Rows
                Dim strName As String = dRow.Cells(0).Value.ToString
                Dim dgvrow As DataGridViewRow = dRow

                Dim row As DataRow = (From p In dTable Where (p.Item("name").ToString.ToUpper Like "*" & strName.ToUpper & "*") Select (p)).FirstOrDefault
                If row IsNot Nothing Then
                    Dim intCount As Integer = (From p In dgvMapping.Rows.Cast(Of DataGridViewRow)() Where (CInt(p.Cells(colhDBHeads.Index).Value) = CInt(row.Item("tranheadunkid")) AndAlso p.Cells(colhFileFields.Index).Value IsNot Nothing AndAlso p.Cells(colhFileFields.Index).Value.ToString <> dgvrow.Cells(colhFileFields.Index).Value.ToString) Select (p)).ToList.Count
                    If intCount > 0 Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Sorry! This Fields is already Mapped with ") & dRow.Cells(colhFileFields.Index).FormattedValue.ToString & Language.getMessage(mstrModuleName, 5, " Fields From File.") & vbCrLf & Language.getMessage(mstrModuleName, 6, "Please select different Field."), enMsgBoxStyle.Information)
                        dRow.Cells(colhDBHeads.Index).Value = Nothing
                    Else
                        dRow.Cells(colhDBHeads.Index).Value = CInt(row.Item("tranheadunkid"))
                    End If
                End If
            Next
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lnkAutoMap_LinkClicked", mstrModuleName)
        End Try
    End Sub
    'Sohail (23 Dec 2014) -- End

#End Region

    'Sohail (26 May 2011) -- Start
#Region " Datagridview's Events "

    Private Sub dgvMapping_CellClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvMapping.CellClick
        Try
            If e.ColumnIndex = objcolhSearch.Index Then
                If e.RowIndex < 0 Then Exit Sub

        Dim objfrm As New frmCommonSearch
        Dim objEmployee As New clsTransactionHead
        Dim dsList As DataSet
            'If User._Object._RightToLeft = True Then
            '    objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
            '    objfrm.RightToLeftLayout = True
            '    Call Language.ctlRightToLeftlayOut(objfrm)
            'End If
                'Sohail (29 Oct 2012) -- Start
                'TRA - ENHANCEMENT
                'dsList = objEmployee.getComboList("TranHead", False, , enCalcType.FlatRate_Others)
                'Sohail (21 Aug 2015) -- Start
                'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                'dsList = objEmployee.getComboList("TranHead", False, , enCalcType.FlatRate_Others, , , True)
                'Sohail (21 Dec 2016) -- Start
                'CCK Enhancement - 64.1 - Allow to import updating Flat Rate membership heads on Update Flat Rate ED heads screen.
                'dsList = objEmployee.getComboList(FinancialYear._Object._DatabaseName, "TranHead", False, , enCalcType.FlatRate_Others, , , True)
                dsList = objEmployee.getComboList(FinancialYear._Object._DatabaseName, "TranHead", False, , enCalcType.FlatRate_Others, , , False)
                'Sohail (21 Dec 2016) -- End
                'Sohail (21 Aug 2015) -- End
                'Sohail (29 Oct 2012) -- End

                objfrm.DataSource = dsList.Tables("TranHead")
                objfrm.ValueMember = "tranheadunkid"
                objfrm.DisplayMember = "name"
                objfrm.CodeMember = "code"
                If objfrm.DisplayDialog Then
                    Dim cb As DataGridViewComboBoxCell = TryCast(dgvMapping.CurrentRow.Cells(colhDBHeads.Index), DataGridViewComboBoxCell)
                    If cb IsNot Nothing Then
                        cb.Value = Convert.ToInt32(objfrm.SelectedValue)
                        SendKeys.Send("{LEFT}")
                    End If
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgvMapping_CellClick", mstrModuleName)
        End Try
    End Sub

    Private Sub dgvMapping_CellEnter(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvMapping.CellEnter
        Try
            If e.ColumnIndex = colhDBHeads.Index OrElse e.ColumnIndex = colhCostCenter.Index Then
                SendKeys.Send("{F2}")
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgvMapping_CellEnter", mstrModuleName)
        End Try
    End Sub

    Private Sub dgvMapping_CellMouseMove(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellMouseEventArgs) Handles dgvMapping.CellMouseMove
        Try
            If e.RowIndex < 0 Then Exit Sub
            If e.ColumnIndex = objcolhSearch.Index Then
                Cursor.Current = Cursors.Hand
            Else
                Cursor.Current = Cursors.Default
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgvMapping_CellMouseMove", mstrModuleName)
        End Try
    End Sub

    Private Sub dgvMapping_DataError(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewDataErrorEventArgs) Handles dgvMapping.DataError
        Try
            eZeeMsgBox.Show(e.Exception.Message, enMsgBoxStyle.Critical)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgvMapping_DataError", mstrModuleName)
        End Try
    End Sub

    Private Sub dgvMapping_EditingControlShowing(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewEditingControlShowingEventArgs) Handles dgvMapping.EditingControlShowing
        Try

            If dgvMapping.CurrentCell.ColumnIndex = colhDBHeads.Index OrElse dgvMapping.CurrentCell.ColumnIndex = colhCostCenter.Index Then
                Dim cb As ComboBox = TryCast(e.Control, ComboBox)
                If cb IsNot Nothing Then
                    RemoveHandler cb.Validating, AddressOf TranHead_Validating

                    Select Case dgvMapping.CurrentCell.ColumnIndex
                        Case colhDBHeads.Index
                            AddHandler cb.Validating, AddressOf TranHead_Validating
                    End Select
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgvMapping_EditingControlShowing", mstrModuleName)
        End Try
    End Sub

    Private Sub TranHead_Validating(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs)
        Try
            Dim cb As ComboBox = TryCast(sender, ComboBox)
            If cb IsNot Nothing Then
                If cb.SelectedIndex <> 0 Then
                    For Each dRow As DataGridViewRow In dgvMapping.Rows
                        If dRow.Index <> dgvMapping.CurrentRow.Index AndAlso Convert.ToInt32(dRow.Cells(colhDBHeads.Index).Value) = Convert.ToInt32(cb.SelectedValue) Then
                            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Sorry! This Fields is already Mapped with ") & dRow.Cells(colhFileFields.Index).FormattedValue.ToString & Language.getMessage(mstrModuleName, 5, " Fields From File.") & vbCrLf & Language.getMessage(mstrModuleName, 6, "Please select different Field."), enMsgBoxStyle.Information)
                            dgvMapping.CurrentCell.Value = 0
                            cb.SelectedIndex = 0
                            e.Cancel = True
                            Exit For
                        End If
                    Next
                End If
                End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "TranHead_Validating", mstrModuleName)
        End Try
    End Sub

#End Region
    'Sohail (26 May 2011) -- End

#Region " Messages "
    '1, "Please select employee code column."
    '2, "Please select Transaction Head column."
    '3, "Please select Transaction Head to map."
    '4, "Invalid Data (Non-Numeric amount) found in File for column of  '" & strColumn & "'."
    '5, "Sorry, Employee Code and Transaction Head are not matching from file!"
    '6, "Sorry! This Fields is already Mapped with '" & dRow.Cells(colhFileFields.Index).FormattedValue.ToString & "' Fields From File." & vbCrLf & "Please select different Field."
    '7, "Please Map atleast one Database field with File field."
#End Region


	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Private Sub OtherSettings()
		Try
			Me.SuspendLayout()
			
			Call SetLanguage()
			
			Me.gbFieldMapping.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbFieldMapping.ForeColor = GUI._eZeeContainerHeaderForeColor 


			Me.btnCancel.GradientBackColor = GUI._ButttonBackColor 
			Me.btnCancel.GradientForeColor = GUI._ButttonFontColor

			Me.btnOk.GradientBackColor = GUI._ButttonBackColor 
			Me.btnOk.GradientForeColor = GUI._ButttonFontColor


			Me.ResumeLayout()
		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "OtherSettings", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetLanguage()
		Try
			Me.Text = Language._Object.getCaption(Me.Name, Me.Text)
			
			Me.gbFieldMapping.Text = Language._Object.getCaption(Me.gbFieldMapping.Name, Me.gbFieldMapping.Text)
			Me.lblEmployeeCode.Text = Language._Object.getCaption(Me.lblEmployeeCode.Name, Me.lblEmployeeCode.Text)
			Me.btnCancel.Text = Language._Object.getCaption(Me.btnCancel.Name, Me.btnCancel.Text)
			Me.btnOk.Text = Language._Object.getCaption(Me.btnOk.Name, Me.btnOk.Text)
			Me.DataGridViewTextBoxColumn1.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn1.Name, Me.DataGridViewTextBoxColumn1.HeaderText)
			Me.DataGridViewTextBoxColumn2.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn2.Name, Me.DataGridViewTextBoxColumn2.HeaderText)
			Me.DataGridViewTextBoxColumn3.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn3.Name, Me.DataGridViewTextBoxColumn3.HeaderText)
			Me.lblPeriod.Text = Language._Object.getCaption(Me.lblPeriod.Name, Me.lblPeriod.Text)
			Me.colhFileFields.HeaderText = Language._Object.getCaption(Me.colhFileFields.Name, Me.colhFileFields.HeaderText)
			Me.colhDBHeads.HeaderText = Language._Object.getCaption(Me.colhDBHeads.Name, Me.colhDBHeads.HeaderText)
			Me.colhCostCenter.HeaderText = Language._Object.getCaption(Me.colhCostCenter.Name, Me.colhCostCenter.HeaderText)
			Me.lnkAutoMap.Text = Language._Object.getCaption(Me.lnkAutoMap.Name, Me.lnkAutoMap.Text)

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetLanguage", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "Please select employee code column.")
			Language.setMessage(mstrModuleName, 2, "Invalid Data [Non-Numeric amount")
			Language.setMessage(mstrModuleName, 3, "] found in File for column of")
			Language.setMessage(mstrModuleName, 4, "Sorry! This Fields is already Mapped with")
			Language.setMessage(mstrModuleName, 5, " Fields From File.")
			Language.setMessage(mstrModuleName, 6, "Please select different Field.")
			Language.setMessage(mstrModuleName, 7, "Sorry, Employee Code and Transaction Head are not matching from file!")
			Language.setMessage(mstrModuleName, 8, "Please Map atleast one Database field with File field.")
			Language.setMessage(mstrModuleName, 9, "Please select Period. Period is mandatory information.")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class