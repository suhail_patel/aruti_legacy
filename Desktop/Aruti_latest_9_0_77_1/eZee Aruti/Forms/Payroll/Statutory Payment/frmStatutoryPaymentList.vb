﻿Option Strict On

Imports eZeeCommonLib
Imports Aruti.Data

Public Class frmStatutoryPaymentList

#Region " Private Varaibles "
    Private objStatPaymentMaster As New clsStatutorypayment_master
    Private ReadOnly mstrModuleName As String = "frmStatutoryPaymentList"
    Private mblnCancel As Boolean = True

    Private mintCheckedEmployee As Integer = 0
    Private mblnProcessFailed As Boolean = False
    Private mdtTable As DataTable = Nothing

    Private mstrVoidReason As String = String.Empty
    Private strVoidIds As String = ""
    Private mstrAdvanceFilter As String = ""
#End Region

#Region " Display Dialog "
    Public Function displayDialog() As Boolean
        Try

            Me.ShowDialog()

            Return Not mblnCancel

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "displayDialog", mstrModuleName)
        End Try
    End Function
#End Region

#Region " Private Method & Function "

    Private Sub FillCombo()
        Dim objPeriod As New clscommom_period_Tran
        Dim objMember As New clsmembership_master
        Dim dsCombos As DataSet
        Try

            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'dsCombos = objPeriod.getListForCombo(enModuleReference.Payroll, FinancialYear._Object._YearUnkid, "Period", True, enStatusType.Close)
            dsCombos = objPeriod.getListForCombo(enModuleReference.Payroll, FinancialYear._Object._YearUnkid, FinancialYear._Object._DatabaseName, FinancialYear._Object._Database_Start_Date, "Period", True, enStatusType.Close)
            'Sohail (21 Aug 2015) -- End
            With cboPayPeriod
                .BeginUpdate()
                .ValueMember = "periodunkid"
                .DisplayMember = "name"
                .DataSource = dsCombos.Tables("Period")
                .SelectedValue = 0
                .EndUpdate()
            End With

            dsCombos = objMember.getListForCombo("Membership", True, , 1)
            With cboMembership
                .BeginUpdate()
                .ValueMember = "membershipunkid"
                .DisplayMember = "name"
                .DataSource = dsCombos.Tables("Membership")
                .SelectedValue = 0
                .EndUpdate()
            End With

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        Finally
            objMember = Nothing
            objPeriod = Nothing
        End Try
    End Sub

    Private Sub FillReceiptNo()
        Dim dsCombo As DataSet
        Try
            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'dsCombo = objStatPaymentMaster.Get_DIST_ReceiptNo("Receipt", True, CInt(cboPayPeriod.SelectedValue), CInt(cboEmployee.SelectedValue), CInt(cboMembership.SelectedValue))
            dsCombo = objStatPaymentMaster.Get_DIST_ReceiptNo(FinancialYear._Object._DatabaseName, "Receipt", True, CInt(cboPayPeriod.SelectedValue), CInt(cboEmployee.SelectedValue), CInt(cboMembership.SelectedValue))
            'Sohail (21 Aug 2015) -- End
            With cboReceiptNo
                .ValueMember = "receiptno"
                .DisplayMember = "receiptno"
                .DataSource = dsCombo.Tables("Receipt")
                .SelectedIndex = 0
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillReceiptNo", mstrModuleName)
        End Try
    End Sub

    Private Sub FillList()
        Dim dsPayment As New DataSet
        Dim StrSearching As String = String.Empty
        Dim lvItem As ListViewItem

        Try
            lvPayment.Items.Clear()
            If User._Object.Privilege._AllowToViewStatutoryPaymentList = False Then Exit Try


            If txtReceiptAmountFrom.Decimal <> 0 AndAlso txtReceiptAmountTo.Decimal <> 0 Then
                StrSearching &= "AND total_receiptamount >= " & txtReceiptAmountFrom.Decimal & " AND total_receiptamount <= " & txtReceiptAmountTo.Decimal & " "
            End If

            If CInt(cboReceiptNo.SelectedIndex) > 0 Then
                StrSearching &= "AND receiptno = '" & cboReceiptNo.Text & "'" & " "
            End If

            If dtpReceiptDate.Checked = True Then
                StrSearching &= "AND CONVERT(CHAR(8), receiptdate, 112) = '" & eZeeDate.convertDate(dtpReceiptDate.Value) & "'" & "  "
            End If

            If StrSearching.Length > 0 Then
                If mstrAdvanceFilter.Trim <> "" Then
                    StrSearching = mstrAdvanceFilter & " " & StrSearching
                Else
                    StrSearching = StrSearching.Substring(3)
                End If
            Else
                StrSearching = mstrAdvanceFilter
            End If

            dsPayment = objStatPaymentMaster.GetList("Payment", CInt(cboMembership.SelectedValue), , , StrSearching)

            For Each dtRow As DataRow In dsPayment.Tables("Payment").Rows
                lvItem = New ListViewItem

                lvItem.Text = ""
                lvItem.Tag = dtRow.Item("statutorypaymentmasterunkid")

                lvItem.SubItems.Add(dtRow.Item("FromPeriodName").ToString)
                lvItem.SubItems(colhFromPeriod.Index).Tag = CInt(dtRow.Item("fromperiodunkid"))

                lvItem.SubItems.Add(dtRow.Item("ToPeriodName").ToString)
                lvItem.SubItems(colhToPeriod.Index).Tag = CInt(dtRow.Item("toperiodunkid"))

                lvItem.SubItems.Add(dtRow.Item("receiptno").ToString)
                lvItem.SubItems.Add(eZeeDate.convertDate(dtRow.Item("receiptdate").ToString).ToShortDateString)
                lvItem.SubItems.Add(dtRow.Item("membershipname").ToString)

                lvItem.SubItems.Add(Format(CDec(dtRow.Item("total_payableamount").ToString), GUI.fmtCurrency))

                lvItem.SubItems.Add(Format(CDec(dtRow.Item("total_receiptamount").ToString), GUI.fmtCurrency))

                lvItem.SubItems.Add(dtRow.Item("remark").ToString)

                RemoveHandler lvPayment.ItemChecked, AddressOf lvPayment_ItemChecked
                lvPayment.Items.Add(lvItem)
                AddHandler lvPayment.ItemChecked, AddressOf lvPayment_ItemChecked
                lvItem = Nothing
            Next


            If lvPayment.Items.Count > 11 Then
                colhRemark.Width = 80 - 18
            Else
                colhRemark.Width = 80
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillList", mstrModuleName)
        Finally
            Call objbtnSearch.ShowResult(CStr(lvPayment.Items.Count))
        End Try
    End Sub

    Private Sub CheckAllPayment(ByVal blnCheckAll As Boolean)
        Try
            For Each lvItem As ListViewItem In lvPayment.Items
                RemoveHandler lvPayment.ItemChecked, AddressOf lvPayment_ItemChecked
                lvItem.Checked = blnCheckAll
                AddHandler lvPayment.ItemChecked, AddressOf lvPayment_ItemChecked
            Next
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "CheckAllEmployee", mstrModuleName)
        End Try
    End Sub

    Private Sub SetVisibility()

        Try

            btnNew.Enabled = User._Object.Privilege._AllowToAddStatutoryPayment
            btnEdit.Enabled = User._Object.Privilege._AllowToEditStatutoryPayment
            btnVoid.Enabled = User._Object.Privilege._AllowToDeleteStatutoryPayment

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetVisibility", mstrModuleName)
        End Try

    End Sub
#End Region

#Region " Form's Events "

    Private Sub frmStatutoryPaymentList_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        Try
            objStatPaymentMaster = Nothing
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmStatutoryPaymentList_FormClosed", mstrModuleName)
        End Try
    End Sub

    Private Sub frmStatutoryPaymentList_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown
        Try
            If e.KeyCode = Keys.Escape Then
                Me.Close()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmStatutoryPaymentList_KeyDown", mstrModuleName)
        End Try
    End Sub

    Private Sub frmStatutoryPaymentList_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        objStatPaymentMaster = New clsStatutorypayment_master
        Try
            Call Set_Logo(Me, gApplicationType)

            Call Language.setLanguage(Me.Name)
            Call OtherSettings()

            Call FillCombo()
            'Call FillList()

            If lvPayment.Items.Count > 0 Then lvPayment.Items(0).Selected = True
            lvPayment.Select()
            Call SetVisibility()

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmStatutoryPaymentList_Load", mstrModuleName)
        End Try
    End Sub

    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsStatutoryPayment_Tran.SetMessages()
            objfrm._Other_ModuleNames = "clsStatutoryPayment_Tran"
            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub

#End Region

#Region " Button's Events "
    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnClose_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnNew_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNew.Click
        Dim objFrm As New frmStatutoryPayment
        Try
            If User._Object._Isrighttoleft = True Then
                objFrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objFrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objFrm)
            End If

            If objFrm.DisplayDialog(-1, enAction.ADD_CONTINUE) = True Then
                Call FillReceiptNo()
                Call FillList()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnNew_Click", mstrModuleName)
        Finally
            objFrm = Nothing
        End Try
    End Sub

    Private Sub btnEdit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEdit.Click, lvPayment.DoubleClick
        If lvPayment.SelectedItems.Count <= 0 Then
            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Please select any Statutory Payment to perform further operation."), enMsgBoxStyle.Information)
            lvPayment.Focus()
            Exit Sub
        End If

        Dim objFrm As New frmStatutoryPayment

        If User._Object._Isrighttoleft = True Then
            objFrm.RightToLeft = Windows.Forms.RightToLeft.Yes
            objFrm.RightToLeftLayout = True
            Call Language.ctlRightToLeftlayOut(objFrm)
        End If

        Try
            If objFrm.DisplayDialog(CInt(lvPayment.SelectedItems(0).Tag), enAction.EDIT_ONE) = True Then
                Call FillReceiptNo()
                Call FillList()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnEdit_Click", mstrModuleName)
        Finally
            objFrm = Nothing
        End Try
    End Sub

    Private Sub btnVoid_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnVoid.Click
        Dim strFilter As String = ""
        Try
            If lvPayment.CheckedItems.Count <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please select Payment from the list to perform further operation."), enMsgBoxStyle.Information) '?1
                lvPayment.Select()
                Exit Sub
            End If

            strVoidIds = lvPayment.CheckedItemIdsString

            If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Are you sure you want to void selected Payment(s)?"), CType(enMsgBoxStyle.Question + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.No Then
                Exit Try
            End If

            Dim intSelectedIndex As Integer
            intSelectedIndex = lvPayment.SelectedItems(0).Index

            Dim frm As New frmReasonSelection
            mstrVoidReason = ""
            frm.displayDialog(enVoidCategoryType.PAYROLL, mstrVoidReason)
            If mstrVoidReason.Length <= 0 Then
                Exit Sub
            End If

            mintCheckedEmployee = lvPayment.CheckedItems.Count
            RemoveHandler gfrmMDI.tmrReminder.Tick, AddressOf gfrmMDI.tmrReminder_Tick
            objbgwVoidPayment.RunWorkerAsync()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnVoid_Click", mstrModuleName)
            RemoveHandler gfrmMDI.tmrReminder.Tick, AddressOf gfrmMDI.tmrReminder_Tick
            AddHandler gfrmMDI.tmrReminder.Tick, AddressOf gfrmMDI.tmrReminder_Tick
        End Try
    End Sub
#End Region

#Region " Combobox Events "
    Private Sub cboPayPeriod_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboPayPeriod.SelectedIndexChanged
        Dim objEmployee As New clsEmployee_Master
        Dim dsCombo As DataSet
        Try
            'Anjan [10 June 2015] -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'If CInt(cboPayPeriod.SelectedValue) > 0 Then
            '    dsCombo = objEmployee.GetEmployeeList("Employee", True, , , , , , , , , , , , , eZeeDate.convertDate(CType(cboPayPeriod.SelectedItem, DataRowView).Item("start_date").ToString), eZeeDate.convertDate(CType(cboPayPeriod.SelectedItem, DataRowView).Item("end_date").ToString))
            'Else
            '    If ConfigParameter._Object._IsIncludeInactiveEmp = False Then
            '        dsCombo = objEmployee.GetEmployeeList("Employee", True, , , , , , , , , , , , , ConfigParameter._Object._CurrentDateAndTime.Date, ConfigParameter._Object._CurrentDateAndTime.Date)
            '    Else
            '        dsCombo = objEmployee.GetEmployeeList("Employee", True)
            '    End If
            'End If


            Dim EmpDate1 As Date = Nothing
            Dim EmpDate2 As Date = Nothing
            If CInt(cboPayPeriod.SelectedValue) > 0 Then
                EmpDate1 = eZeeDate.convertDate(CType(cboPayPeriod.SelectedItem, DataRowView).Item("start_date").ToString)
                EmpDate2 = eZeeDate.convertDate(CType(cboPayPeriod.SelectedItem, DataRowView).Item("end_date").ToString)
            Else
                EmpDate1 = eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate)
                EmpDate2 = eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate)
            End If

            dsCombo = objEmployee.GetEmployeeList(FinancialYear._Object._DatabaseName, _
                                          User._Object._Userunkid, _
                                          FinancialYear._Object._YearUnkid, _
                                          Company._Object._Companyunkid, _
                                          EmpDate1, _
                                          EmpDate2, _
                                          ConfigParameter._Object._UserAccessModeSetting, _
                                          True, ConfigParameter._Object._IsIncludeInactiveEmp, "Employee", True)

            'Anjan [10 June 2015] -- End


            With cboEmployee
                .BeginUpdate()
                .ValueMember = "employeeunkid"
                .DisplayMember = "employeename"
                .DataSource = dsCombo.Tables("Employee")
                .SelectedValue = 0
                .EndUpdate()
            End With

            Call FillReceiptNo()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboPayPeriod_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub cboEmployee_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboEmployee.SelectedIndexChanged
        Try
            Call FillReceiptNo()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboEmployee_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub cboMembership_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboMembership.SelectedIndexChanged
        Try
            Call FillReceiptNo()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboMembership_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub
#End Region

#Region " Listview's Events "
    Private Sub lvPayment_ItemChecked(ByVal sender As Object, ByVal e As System.Windows.Forms.ItemCheckedEventArgs) Handles lvPayment.ItemChecked
        Try
            If lvPayment.CheckedItems.Count <= 0 Then
                RemoveHandler objchkSelectAll.CheckedChanged, AddressOf chkSelectAll_CheckedChanged
                objchkSelectAll.CheckState = CheckState.Unchecked
                AddHandler objchkSelectAll.CheckedChanged, AddressOf chkSelectAll_CheckedChanged
            ElseIf lvPayment.CheckedItems.Count < lvPayment.Items.Count Then
                RemoveHandler objchkSelectAll.CheckedChanged, AddressOf chkSelectAll_CheckedChanged
                objchkSelectAll.CheckState = CheckState.Indeterminate
                AddHandler objchkSelectAll.CheckedChanged, AddressOf chkSelectAll_CheckedChanged
            ElseIf lvPayment.CheckedItems.Count = lvPayment.Items.Count Then
                RemoveHandler objchkSelectAll.CheckedChanged, AddressOf chkSelectAll_CheckedChanged
                objchkSelectAll.CheckState = CheckState.Checked
                AddHandler objchkSelectAll.CheckedChanged, AddressOf chkSelectAll_CheckedChanged
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lvPayment_ItemChecked", mstrModuleName)
        End Try
    End Sub
#End Region

#Region " CheckBox's Events "
    Private Sub chkSelectAll_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objchkSelectAll.CheckedChanged
        Dim decTotAmt As Decimal = 0
        Try
            Call CheckAllPayment(objchkSelectAll.Checked)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "chkSelectAll_CheckedChanged", mstrModuleName)
        End Try
    End Sub
#End Region

#Region " Other Control's Events "
    Private Sub objbtnReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnReset.Click
        Try
            cboPayPeriod.SelectedValue = 0
            cboEmployee.SelectedValue = 0
            cboMembership.SelectedValue = 0
            cboReceiptNo.SelectedIndex = 0
            txtReceiptAmountFrom.Text = ""
            txtReceiptAmountTo.Text = ""
            dtpReceiptDate.Checked = False
            mstrAdvanceFilter = ""
            Call FillList()

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnReset_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnSearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearch.Click
        Try
            Call FillList()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearch_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnSearchEmployee_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchEmployee.Click
        Dim objfrm As New frmCommonSearch
        Try
            With objfrm
                .ValueMember = cboEmployee.ValueMember
                .DisplayMember = cboEmployee.DisplayMember
                .DataSource = CType(cboEmployee.DataSource, DataTable)
                .CodeMember = "employeecode"
            End With

            If objfrm.DisplayDialog Then
                cboEmployee.SelectedValue = objfrm.SelectedValue
                cboEmployee.Focus()
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchEmployee_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbgwVoidPayment_DoWork(ByVal sender As System.Object, ByVal e As System.ComponentModel.DoWorkEventArgs) Handles objbgwVoidPayment.DoWork
        Try
            mblnProcessFailed = False
            Me.ControlBox = False
            Me.Enabled = False

            'S.SANDEEP [28-May-2018] -- START
            'ISSUE/ENHANCEMENT : {Audit Trails} 
            objStatPaymentMaster._FormName = mstrModuleName
            objStatPaymentMaster._LoginEmployeeUnkid = 0
            objStatPaymentMaster._ClientIP = getIP()
            objStatPaymentMaster._HostName = getHostName()
            objStatPaymentMaster._FromWeb = False
            objStatPaymentMaster._AuditUserId = User._Object._Userunkid
objStatPaymentMaster._CompanyUnkid = Company._Object._Companyunkid
            objStatPaymentMaster._AuditDate = ConfigParameter._Object._CurrentDateAndTime
            'S.SANDEEP [28-May-2018] -- END

            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'If objStatPaymentMaster.VoidAll(strVoidIds, User._Object._Userunkid, ConfigParameter._Object._CurrentDateAndTime, mstrVoidReason, objbgwVoidPayment) = True Then
            If objStatPaymentMaster.VoidAll(strVoidIds, User._Object._Userunkid, ConfigParameter._Object._CurrentDateAndTime, mstrVoidReason, ConfigParameter._Object._CurrentDateAndTime, objbgwVoidPayment) = True Then
                'Sohail (21 Aug 2015) -- End

                Call FillList()

                mblnCancel = False
            End If
        Catch ex As Exception
            mblnProcessFailed = True
            DisplayError.Show("-1", ex.Message, "objbgwVoidPayment_DoWork", mstrModuleName)
        End Try
    End Sub

    Private Sub objbgwVoidPayment_ProgressChanged(ByVal sender As Object, ByVal e As System.ComponentModel.ProgressChangedEventArgs) Handles objbgwVoidPayment.ProgressChanged
        Try
            objlblProgress.Text = "[ " & e.ProgressPercentage.ToString & " / " & mintCheckedEmployee & " ]"
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbgwVoidPayment_ProgressChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub objbgwVoidPayment_RunWorkerCompleted(ByVal sender As Object, ByVal e As System.ComponentModel.RunWorkerCompletedEventArgs) Handles objbgwVoidPayment.RunWorkerCompleted
        Try

            RemoveHandler gfrmMDI.tmrReminder.Tick, AddressOf gfrmMDI.tmrReminder_Tick
            AddHandler gfrmMDI.tmrReminder.Tick, AddressOf gfrmMDI.tmrReminder_Tick

            If e.Cancelled = True Then

            ElseIf e.Error IsNot Nothing Then
                eZeeMsgBox.Show(e.Error.ToString, enMsgBoxStyle.Information)
            ElseIf mblnProcessFailed = True Then

            Else
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Void Payment Process completed successfully."), enMsgBoxStyle.Information)
                objchkSelectAll.Checked = False
                cboPayPeriod.SelectedValue = 0
                objlblProgress.Text = ""
            End If

            Me.ControlBox = True
            Me.Enabled = True

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbgwVoidPayment_RunWorkerCompleted", mstrModuleName)
        End Try
    End Sub

    Private Sub lnkAdvanceFilter_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lnkAdvanceFilter.LinkClicked
        Try
            Dim frm As New frmAdvanceSearch
            frm.ShowDialog()
            mstrAdvanceFilter = frm._GetFilterString
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lnkAdvanceFilter_LinkClicked", mstrModuleName)
        End Try
    End Sub

#End Region


    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Private Sub OtherSettings()
        Try
            Me.SuspendLayout()
           
            Call SetLanguage()

            Me.gbFilterCriteria.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbFilterCriteria.ForeColor = GUI._eZeeContainerHeaderForeColor

            Me.EZeeHeader.GradientColor1 = GUI._HeaderBackColor1
            Me.EZeeHeader.GradientColor2 = GUI._HeaderBackColor2
            Me.EZeeHeader.BorderColor = GUI._HeaderBorderColor
            Me.EZeeHeader.HeaderTextForeColor = GUI._HeaderTitleFontColor
            Me.EZeeHeader.DescriptionForeColor = GUI._HeaderMessageFontColor


            Me.btnVoid.GradientBackColor = GUI._ButttonBackColor
            Me.btnVoid.GradientForeColor = GUI._ButttonFontColor

            Me.btnClose.GradientBackColor = GUI._ButttonBackColor
            Me.btnClose.GradientForeColor = GUI._ButttonFontColor

            Me.btnNew.GradientBackColor = GUI._ButttonBackColor
            Me.btnNew.GradientForeColor = GUI._ButttonFontColor

            Me.btnEdit.GradientBackColor = GUI._ButttonBackColor
            Me.btnEdit.GradientForeColor = GUI._ButttonFontColor


            Me.ResumeLayout()
        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "OtherSettings", mstrModuleName)
        End Try
    End Sub


    Private Sub SetLanguage()
        Try
            Me.Text = Language._Object.getCaption(Me.Name, Me.Text)

            Me.colhDate.Text = Language._Object.getCaption(CStr(Me.colhDate.Tag), Me.colhDate.Text)
            Me.colhReceipt.Text = Language._Object.getCaption(CStr(Me.colhReceipt.Tag), Me.colhReceipt.Text)
            Me.colhReceiptAmount.Text = Language._Object.getCaption(CStr(Me.colhReceiptAmount.Tag), Me.colhReceiptAmount.Text)
            Me.EZeeHeader.Title = Language._Object.getCaption(Me.EZeeHeader.Name & "_Title", Me.EZeeHeader.Title)
            Me.EZeeHeader.Message = Language._Object.getCaption(Me.EZeeHeader.Name & "_Message", Me.EZeeHeader.Message)
            Me.btnVoid.Text = Language._Object.getCaption(Me.btnVoid.Name, Me.btnVoid.Text)
            Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
            Me.gbFilterCriteria.Text = Language._Object.getCaption(Me.gbFilterCriteria.Name, Me.gbFilterCriteria.Text)
            Me.lnkAdvanceFilter.Text = Language._Object.getCaption(Me.lnkAdvanceFilter.Name, Me.lnkAdvanceFilter.Text)
            Me.lblTo.Text = Language._Object.getCaption(Me.lblTo.Name, Me.lblTo.Text)
            Me.lblReceiptAmount.Text = Language._Object.getCaption(Me.lblReceiptAmount.Name, Me.lblReceiptAmount.Text)
            Me.lblReceiptDate.Text = Language._Object.getCaption(Me.lblReceiptDate.Name, Me.lblReceiptDate.Text)
            Me.lblReceiptNo.Text = Language._Object.getCaption(Me.lblReceiptNo.Name, Me.lblReceiptNo.Text)
            Me.lblPayPeriod.Text = Language._Object.getCaption(Me.lblPayPeriod.Name, Me.lblPayPeriod.Text)
            Me.lblEmployee.Text = Language._Object.getCaption(Me.lblEmployee.Name, Me.lblEmployee.Text)
            Me.colhMembership.Text = Language._Object.getCaption(CStr(Me.colhMembership.Tag), Me.colhMembership.Text)
            Me.lblMembership.Text = Language._Object.getCaption(Me.lblMembership.Name, Me.lblMembership.Text)
            Me.colhTotalPayableAmount.Text = Language._Object.getCaption(CStr(Me.colhTotalPayableAmount.Tag), Me.colhTotalPayableAmount.Text)
            Me.btnNew.Text = Language._Object.getCaption(Me.btnNew.Name, Me.btnNew.Text)
            Me.colhRemark.Text = Language._Object.getCaption(CStr(Me.colhRemark.Tag), Me.colhRemark.Text)
            Me.btnEdit.Text = Language._Object.getCaption(Me.btnEdit.Name, Me.btnEdit.Text)
            Me.colhFromPeriod.Text = Language._Object.getCaption(CStr(Me.colhFromPeriod.Tag), Me.colhFromPeriod.Text)
            Me.colhToPeriod.Text = Language._Object.getCaption(CStr(Me.colhToPeriod.Tag), Me.colhToPeriod.Text)

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
        End Try
    End Sub


    Private Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Please select Payment from the list to perform further operation.")
            Language.setMessage(mstrModuleName, 2, "Are you sure you want to void selected Payment(s)?")
            Language.setMessage(mstrModuleName, 3, "Void Payment Process completed successfully.")
            Language.setMessage(mstrModuleName, 4, "Please select any Statutory Payment to perform further operation.")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class