﻿
Option Strict On

Imports eZeeCommonLib
Imports Aruti.Data

'Last Message Index = 5

Public Class frmEmployee_Exemption_AddEdit

#Region " Private Variables "

    Private ReadOnly mstrModuleName As String = "frmEmployee_Exemption_AddEdit"
    Private mblnCancel As Boolean = True
    Private menAction As enAction = enAction.ADD_ONE
    Private objEmpExemption As clsemployee_exemption_Tran
    Private mintExemptionUnkid As Integer = -1
    Private mintYearID As Integer = 0
    Private mintPeriodID As Integer = 0
    Private mintEmployeeID As Integer = 0
    Private mintTranHeadID As Integer = 0

    'Vimal (01 Nov 2010) -- Start 
    Private mblnExemptFlag As Boolean
    Private mstrExemptPeriodIds As String = String.Empty
    Private mstrEmployeeIds As String = String.Empty
    Dim empArray() As String

    'Sohail (06 Mar 2012) -- Start
    'TRA - ENHANCEMENT
    Private mdtPayPeriodStartDate As DateTime
    Private mdtPayPeriodEndDate As DateTime
    'Sohail (06 Mar 2012) -- End

    'Sohail (17 Sep 2019) -- Start
    'NMB Enhancement # TC006: system should be able to bind earning/deduction (benefit) with specific allocation and automatically assign to employee as per his/her allocation.
    Private mstrAdvanceFilter As String = ""
    Private dvEmployee As DataView
    Private mintTotalEmployee As Integer = 0
    Private mintCount As Integer = 0
    Private mstrSearchEmpText As String = ""
    'Sohail (17 Sep 2019) -- End

    Public ReadOnly Property _ExemptPeriodIds() As String
        Get
            Return mstrExemptPeriodIds
        End Get
    End Property

    Public ReadOnly Property _EmployeeIds() As String
        Get
            Return mstrEmployeeIds
        End Get
    End Property
    'Vimal (01 Nov 2010) -- End

#End Region

#Region " Display Dialog "

    'Vimal (01 Nov 2010) -- Start 
    'Public Function displayDialog(ByRef intUnkId As Integer, ByVal eAction As enAction, Optional ByVal intYearID As Integer = 0, Optional ByVal intPeriodID As Integer = 0, Optional ByVal intEmpID As Integer = 0, Optional ByVal intTranHeadID As Integer = 0) As Boolean
    '    Try
    '        mintExemptionUnkid = intUnkId
    '        menAction = eAction

    '        mintYearID = intYearID
    '        mintPeriodID = intPeriodID
    '        mintEmployeeID = intEmpID
    '        mintTranHeadID = intTranHeadID

    '        Me.ShowDialog()

    '        intUnkId = mintExemptionUnkid

    '        Return Not mblnCancel
    '    Catch ex As Exception
    '        DisplayError.Show("-1", ex.Message, "displayDialog", mstrModuleName)
    '    End Try
    'End Function

    Public Function displayDialog(ByRef intUnkId As Integer, ByVal eAction As enAction, Optional ByVal intYearID As Integer = 0, Optional ByVal intPeriodID As Integer = 0, Optional ByVal intEmpID As Integer = 0, Optional ByVal intTranHeadID As Integer = 0, Optional ByVal blnExemptFlag As Boolean = False, Optional ByVal strEmpIds As String = "") As Boolean
        Try
            mintExemptionUnkid = intUnkId
            menAction = eAction

            mintYearID = intYearID
            mintPeriodID = intPeriodID
            mintEmployeeID = intEmpID
            mintTranHeadID = intTranHeadID

            mblnExemptFlag = blnExemptFlag
            mstrEmployeeIds = strEmpIds

            empArray = strEmpIds.Split(",".ToCharArray)
           
            Me.ShowDialog()

            intUnkId = mintExemptionUnkid

            Return Not mblnCancel
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "displayDialog", mstrModuleName)
        End Try
    End Function
    'Vimal (01 Nov 2010) -- End
#End Region

#Region " Private Methods "

    Private Sub SetColor()
        Try
            cboYear.BackColor = GUI.ColorComp
            'cboPeriod.BackColor = GUI.ColorComp 'Sohail (11 Sep 2010)
            'cboEmployee.BackColor = GUI.ColorComp 'Sohail (09 Oct 2010)
            cboTransactionhead.BackColor = GUI.ColorComp
            'Sohail (09 Oct 2010) -- Start
            cboTrnHeadType.BackColor = GUI.ColorOptional
            cboTypeOf.BackColor = GUI.ColorOptional
            cboEmployee.BackColor = GUI.ColorOptional
            'Sohail (17 Sep 2019) -- Start
            'NMB Enhancement # TC006: system should be able to bind earning/deduction (benefit) with specific allocation and automatically assign to employee as per his/her allocation.
            'cboDepartment.BackColor = GUI.ColorOptional
            'cboGrade.BackColor = GUI.ColorOptional
            'cboSections.BackColor = GUI.ColorOptional
            'cboClass.BackColor = GUI.ColorOptional
            'cboCostCenter.BackColor = GUI.ColorOptional
            'cboJob.BackColor = GUI.ColorOptional
            'cboPayPoint.BackColor = GUI.ColorOptional
            'cboUnit.BackColor = GUI.ColorOptional
            'Sohail (17 Sep 2019) -- End
            'Sohail (09 Oct 2010) -- End

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetColor", mstrModuleName)
        End Try
    End Sub

    Private Sub GetValue()
        Try
            cboYear.SelectedValue = objEmpExemption._Yearunkid
            'cboPeriod.SelectedValue = objEmpExemption._Periodunkid 'Sohail (11 Sep 2010)
            cboEmployee.SelectedValue = objEmpExemption._Employeeunkid
            cboTransactionhead.SelectedValue = objEmpExemption._Tranheadunkid
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetValue", mstrModuleName)
        End Try
    End Sub

    Private Sub SetValue()
        Try
            objEmpExemption._Yearunkid = CInt(cboYear.SelectedValue)
            'objEmpExemption._Periodunkid = CInt(cboPeriod.SelectedValue) 'Sohail (11 Sep 2010)
            objEmpExemption._Employeeunkid = CInt(cboEmployee.SelectedValue)
            objEmpExemption._Tranheadunkid = CInt(cboTransactionhead.SelectedValue)
            objEmpExemption._Userunkid = User._Object._Userunkid 'Sohail (14 Oct 2010)
            'Sohail (26 Nov 2011) -- Start
            If menAction <> enAction.EDIT_ONE Then
                If User._Object.Privilege._AllowToApproveEmpExemtion = True Then
                    objEmpExemption._Isapproved = True
                    objEmpExemption._Approveruserunkid = User._Object._Userunkid
                Else
                    objEmpExemption._Isapproved = False
                End If
            End If
            'Sohail (26 Nov 2011) -- End
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetValue", mstrModuleName)
        End Try
    End Sub

    Private Sub FillCombo()
        Dim dsfill As DataSet = Nothing
        Dim objMaster As New clsMasterData
        Dim objTranhead As New clsTransactionHead
        Dim objEmployee As New clsEmployee_Master
        'Sohail (17 Sep 2019) -- Start
        'NMB Enhancement # TC006: system should be able to bind earning/deduction (benefit) with specific allocation and automatically assign to employee as per his/her allocation.
        'Dim objDept As New clsDepartment
        'Dim objGrade As New clsGrade
        'Dim objSection As New clsSections
        'Dim objClass As New clsClass
        'Dim objCostCenter As New clscostcenter_master
        'Dim objJob As New clsJobs
        'Dim objPayPoint As New clspaypoint_master
        'Dim objUnit As New clsUnits
        'Sohail (17 Sep 2019) -- End
        Dim dsList As DataSet
        Dim dtTable As DataTable
        Try
            'FOR YEAR
            Dim objyear As New clsMasterData

            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'dsfill = objyear.getComboListPAYYEAR("year", True)
            dsfill = objyear.getComboListPAYYEAR(FinancialYear._Object._YearUnkid, FinancialYear._Object._FinancialYear_Name, Company._Object._Companyunkid, "year", True)
            'S.SANDEEP [04 JUN 2015] -- END

            cboYear.ValueMember = "Id"
            cboYear.DisplayMember = "name"
            cboYear.DataSource = dsfill.Tables("year")

            'Sohail (09 Oct 2010) -- Start
            'FOR EMPLOYEE
            'Dim objEmployee As New clsEmployee_Master
            'dsfill = objEmployee.GetEmployeeList("Employee", True, True)
            'cboEmployee.ValueMember = "employeeunkid"
            'cboEmployee.DisplayMember = "employeename"
            'cboEmployee.DataSource = dsfill.Tables("Employee")

            'FOR TRANSACTION HEAD
            'dsfill = Nothing
            'Dim objTranhead As New clsTransactionHead
            'dsfill = objTranhead.getComboList("TranHead", True)
            'cboTransactionhead.ValueMember = "tranheadunkid"
            'cboTransactionhead.DisplayMember = "name"
            'cboTransactionhead.DataSource = dsfill.Tables("TranHead")
            dsList = objMaster.getComboListForHeadType("HeadType")
            With cboTrnHeadType
                .ValueMember = "Id"
                .DisplayMember = "Name"
                .DataSource = dsList.Tables("HeadType")
                If .Items.Count > 0 Then .SelectedValue = 0
            End With
            With objTranHeadType
                .ValueMember = "Id"
                .DisplayMember = "Name"
                .DataSource = cboTrnHeadType.DataSource
            End With

            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'dsList = objTranhead.getComboList("TranHead", True, 0, 0, -1)

            'S.SANDEEP [12-JUL-2018] -- START
            'ISSUE/ENHANCEMENT : {Ref#223|ARUTI-239}
            'dsList = objTranhead.getComboList(FinancialYear._Object._DatabaseName, "TranHead", True, 0, 0, -1)
            'Sohail (05 Dec 2019) -- Start
            'NMB UAT Enhancement # TC010 : Allow to exempt inactive heads on employee exemption screen.
            'dsList = objTranhead.getComboList(FinancialYear._Object._DatabaseName, "TranHead", True, 0, 0, -1, , , , , , , 0)
            dsList = objTranhead.getComboList(FinancialYear._Object._DatabaseName, "TranHead", True, 0, 0, -1, , , , , , , 0, True)
            'Sohail (05 Dec 2019) -- End
            'S.SANDEEP [12-JUL-2018] -- END

            'Sohail (21 Aug 2015) -- End
            dtTable = New DataView(dsList.Tables("TranHead"), "typeof_id <> " & enTypeOf.Salary & "", "", DataViewRowState.CurrentRows).ToTable
            With cboTransactionhead
                .ValueMember = "tranheadunkid"
                .DisplayMember = "name"
                .DataSource = dtTable
                .SelectedValue = 0
            End With

            'Sohail (06 Mar 2012) -- Start
            'TRA - ENHANCEMENT
            ''Sohail (06 Jan 2012) -- Start
            ''TRA - ENHANCEMENT
            ''dsList = objEmployee.GetEmployeeList("EmployeeList", True)
            'If menAction = enAction.EDIT_ONE Then
            '    dsList = objEmployee.GetEmployeeList("EmployeeList", True, , , , , , , , , , , , , eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate))
            'Else
            '    dsList = objEmployee.GetEmployeeList("EmployeeList", True, , , , , , , , , , , , , ConfigParameter._Object._CurrentDateAndTime.Date, ConfigParameter._Object._CurrentDateAndTime.Date)
            'End If
            ''Sohail (06 Jan 2012) -- End
            'With cboEmployee
            '    .BeginUpdate()
            '    .ValueMember = "employeeunkid"
            '    .DisplayMember = "employeename"
            '    .DataSource = dsList.Tables("EmployeeList")
            '    If .Items.Count > 0 Then .SelectedValue = 0
            '    .EndUpdate()
            'End With
            Call FillCombo_Employee()
            'Sohail (06 Mar 2012) -- End

            'Sohail (17 Sep 2019) -- Start
            'NMB Enhancement # TC006: system should be able to bind earning/deduction (benefit) with specific allocation and automatically assign to employee as per his/her allocation.
            'dsList = objDept.getComboList("DepartmentList", True)
            'With cboDepartment
            '    .BeginUpdate()
            '    .ValueMember = "departmentunkid"
            '    .DisplayMember = "name"
            '    .DataSource = dsList.Tables("DepartmentList")
            '    If .Items.Count > 0 Then .SelectedValue = 0
            '    .EndUpdate()
            'End With

            'dsList = objGrade.getComboList("GradeList", True)
            'With cboGrade
            '    .BeginUpdate()
            '    .ValueMember = "gradeunkid"
            '    .DisplayMember = "name"
            '    .DataSource = dsList.Tables("GradeList")
            '    If .Items.Count > 0 Then .SelectedValue = 0
            '    .EndUpdate()
            'End With

            'dsList = objSection.getComboList("SectionList", True)
            'With cboSections
            '    .BeginUpdate()
            '    .ValueMember = "sectionunkid"
            '    .DisplayMember = "name"
            '    .DataSource = dsList.Tables("SectionList")
            '    If .Items.Count > 0 Then .SelectedValue = 0
            '    .EndUpdate()
            'End With

            'dsList = objClass.getComboList("ClassList", True)
            'With cboClass
            '    .BeginUpdate()
            '    .ValueMember = "classesunkid"
            '    .DisplayMember = "name"
            '    .DataSource = dsList.Tables("ClassList")
            '    If .Items.Count > 0 Then .SelectedValue = 0
            '    .EndUpdate()
            'End With

            'dsList = objCostCenter.getComboList("CostCenterList", True)
            'With cboCostCenter
            '    .BeginUpdate()
            '    .ValueMember = "CostCenterunkid"
            '    .DisplayMember = "CostCentername"
            '    .DataSource = dsList.Tables("CostCenterList")
            '    If .Items.Count > 0 Then .SelectedValue = 0
            '    .EndUpdate()
            'End With

            'dsList = objJob.getComboList("JobList", True)
            'With cboJob
            '    .BeginUpdate()
            '    .ValueMember = "jobunkid"
            '    .DisplayMember = "name"
            '    .DataSource = dsList.Tables("JobList")
            '    If .Items.Count > 0 Then .SelectedValue = 0
            '    .EndUpdate()
            'End With

            'dsList = objPayPoint.getListForCombo("PayPointList", True)
            'With cboPayPoint
            '    .BeginUpdate()
            '    .ValueMember = "paypointunkid"
            '    .DisplayMember = "name"
            '    .DataSource = dsList.Tables("PayPointList")
            '    If .Items.Count > 0 Then .SelectedValue = 0
            '    .EndUpdate()
            'End With

            'dsList = objUnit.getComboList("UnitList", True)
            'With cboUnit
            '    .BeginUpdate()
            '    .ValueMember = "unitunkid"
            '    .DisplayMember = "name"
            '    .DataSource = dsList.Tables("UnitList")
            '    If .Items.Count > 0 Then .SelectedValue = 0
            '    .EndUpdate()
            'End With
            'Sohail (17 Sep 2019) -- End
            'Sohail (09 Oct 2010) -- End


        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        Finally
            objTranhead = Nothing
            objEmployee = Nothing
            'Sohail (17 Sep 2019) -- Start
            'NMB Enhancement # TC006: system should be able to bind earning/deduction (benefit) with specific allocation and automatically assign to employee as per his/her allocation.
            'objDept = Nothing
            'objGrade = Nothing
            'objSection = Nothing
            'objClass = Nothing
            'objCostCenter = Nothing
            'objJob = Nothing
            'objPayPoint = Nothing
            'objUnit = Nothing
            'Sohail (17 Sep 2019) -- End
        End Try
    End Sub

    'Sohail (06 Mar 2012) -- Start
    'TRA - ENHANCEMENT
    Private Sub FillCombo_Employee()
        Dim objEmployee As New clsEmployee_Master
        Dim dsList As DataSet
        Try
            'Anjan [10 June 2015] -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            Dim EmpDate1 As Date = Nothing
            Dim EmpDate2 As Date = Nothing

            If menAction = enAction.EDIT_ONE Then
                'dsList = objEmployee.GetEmployeeList("EmployeeList",  True, , , , , , , , , , , , , eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate))
                EmpDate1 = eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate)
                EmpDate2 = eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate)

            Else
                'dsList = objEmployee.GetEmployeeList("EmployeeList", True, , , , , , , , , , , , , mdtPayPeriodStartDate, mdtPayPeriodEndDate)
                EmpDate1 = mdtPayPeriodStartDate
                EmpDate2 = mdtPayPeriodEndDate
            End If

            dsList = objEmployee.GetEmployeeList(FinancialYear._Object._DatabaseName, _
                                          User._Object._Userunkid, _
                                          FinancialYear._Object._YearUnkid, _
                                          Company._Object._Companyunkid, _
                                          EmpDate1, _
                                          EmpDate2, _
                                          ConfigParameter._Object._UserAccessModeSetting, _
                                          True, False, "EmployeeList", True, strAdvanceFilterQuery:=mstrAdvanceFilter)
            'Sohail (17 Sep 2019) - [mstrAdvanceFilter]
            'Anjan [10 June 2015] -- End

            With cboEmployee
                .BeginUpdate()
                .ValueMember = "employeeunkid"
                .DisplayMember = "employeename"
                .DataSource = dsList.Tables("EmployeeList")
                .SelectedIndex = 0
                .EndUpdate()
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillComboEmployee", mstrModuleName)
        Finally
            objEmployee = Nothing
        End Try
    End Sub

    Private Sub FillList_Period()
        Dim objPeriod As New clscommom_period_Tran
        Dim dsFill As DataSet
        Try
            lvPeriod.Items.Clear()
            If CInt(cboYear.SelectedValue) > 0 Then
                'Sohail (21 Aug 2015) -- Start
                'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                'dsFill = objPeriod.GetList("Period", enModuleReference.Payroll, True, enStatusType.Open)
                dsFill = objPeriod.GetList("Period", enModuleReference.Payroll, FinancialYear._Object._YearUnkid, FinancialYear._Object._Database_Start_Date, True, enStatusType.Open)
                'Sohail (21 Aug 2015) -- End
                Dim lvItem As ListViewItem
                For Each dsRow As DataRow In dsFill.Tables("Period").Rows
                    lvItem = New ListViewItem

                    lvItem.Tag = dsRow.Item("periodunkid").ToString

                    lvItem.SubItems.Add(dsRow.Item("period_name").ToString)
                    lvItem.SubItems(colhPeriod.Index).Tag = CInt(dsRow.Item("periodunkid").ToString)

                    lvItem.SubItems.Add(eZeeDate.convertDate(dsRow.Item("start_date").ToString).ToShortDateString)

                    lvItem.SubItems.Add(eZeeDate.convertDate(dsRow.Item("end_date").ToString).ToShortDateString)

                    If menAction = enAction.EDIT_ONE AndAlso objEmpExemption._Periodunkid = CInt(dsRow.Item("periodunkid").ToString) Then
                        lvItem.Checked = True
                    ElseIf mintPeriodID > 0 AndAlso CInt(dsRow.Item("periodunkid").ToString) = mintPeriodID Then
                        lvItem.Checked = True
                    End If
                    RemoveHandler lvPeriod.ItemChecked, AddressOf lvPeriod_ItemChecked
                    lvPeriod.Items.Add(lvItem)
                    AddHandler lvPeriod.ItemChecked, AddressOf lvPeriod_ItemChecked
                Next
            End If
            If lvPeriod.Items.Count > 12 Then
                colhPeriod.Width = 140 - 18
            Else
                colhPeriod.Width = 140
            End If
            'Sohail (17 Sep 2019) -- Start
            'NMB Enhancement # TC006: system should be able to bind earning/deduction (benefit) with specific allocation and automatically assign to employee as per his/her allocation.
            If lvPeriod.CheckedItems.Count > 0 Then
                Call FillList()
            End If
            'Sohail (17 Sep 2019) -- End
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillList_Period", mstrModuleName)
        Finally
            objPeriod = Nothing
        End Try
    End Sub
    'Sohail (06 Mar 2012) -- End

    'Sohail (11 Sep 2010) -- Start
    Private Sub CheckAllPeriod(ByVal blnCheckAll As Boolean)
        Try
            For Each lvItem As ListViewItem In lvPeriod.Items
                RemoveHandler lvPeriod.ItemChecked, AddressOf lvPeriod_ItemChecked 'Sohail (03 Nov 2010)
                lvItem.Checked = blnCheckAll
                AddHandler lvPeriod.ItemChecked, AddressOf lvPeriod_ItemChecked 'Sohail (03 Nov 2010)

                'Sohail (06 Mar 2012) -- Start
                'TRA - ENHANCEMENT
                If blnCheckAll = True Then
                    If mdtPayPeriodStartDate = Nothing OrElse mdtPayPeriodStartDate > CDate(lvItem.SubItems(colhStartDate.Index).Text) Then
                        mdtPayPeriodStartDate = CDate(lvItem.SubItems(colhStartDate.Index).Text)
                    End If
                    If mdtPayPeriodEndDate = Nothing OrElse mdtPayPeriodEndDate < CDate(lvItem.SubItems(colhEndDate.Index).Text) Then
                        mdtPayPeriodEndDate = CDate(lvItem.SubItems(colhEndDate.Index).Text)
                    End If
                Else
                    mdtPayPeriodStartDate = Nothing
                    mdtPayPeriodEndDate = Nothing
                End If
                'Sohail (06 Mar 2012) -- End
            Next
            'Sohail (06 Mar 2012) -- Start
            'TRA - ENHANCEMENT
            Call FillCombo_Employee()
            Call FillList()
            'Sohail (06 Mar 2012) -- End
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "CheckAllPeriod", mstrModuleName)
        End Try
    End Sub
    'Sohail (11 Sep 2010) -- End

    'Sohail (09 Oct 2010) -- Start
    Private Sub FillList()
        Dim objEmployee As New clsEmployee_Master
        Dim dsEmployee As New DataSet

        Try
            'Sohail (17 Sep 2019) -- Start
            'NMB Enhancement # TC006: system should be able to bind earning/deduction (benefit) with specific allocation and automatically assign to employee as per his/her allocation.
            objlblEmpCount.Text = "( 0 / 0 )"
            mintCount = 0
            dgEmployee.DataSource = Nothing
            RemoveHandler txtSearchEmp.TextChanged, AddressOf txtSearchEmp_TextChanged
            Call SetDefaultSearchEmpText()
            AddHandler txtSearchEmp.TextChanged, AddressOf txtSearchEmp_TextChanged
            'Sohail (17 Sep 2019) -- End

            'Sohail (06 Jan 2012) -- Start
            'TRA - ENHANCEMENT
            'dsEmployee = objEmployee.GetEmployeeList("Employee", False, True _
            '                                          , CInt(cboEmployee.SelectedValue) _
            '                                          , CInt(cboDepartment.SelectedValue) _
            '                                          , CInt(cboSections.SelectedValue) _
            '                                          , CInt(cboUnit.SelectedValue) _
            '                                          , CInt(cboGrade.SelectedValue) _
            '                                          , 0 _
            '                                          , CInt(cboClass.SelectedValue) _
            '                                          , CInt(cboCostCenter.SelectedValue) _
            '                                          , 0 _
            '                                          , CInt(cboJob.SelectedValue) _
            '                                          , CInt(cboPayPoint.SelectedValue) _
            '                                        )
            'Sohail (06 Mar 2012) -- Start
            'TRA - ENHANCEMENT
            'dsEmployee = objEmployee.GetEmployeeList("Employee", False, True _
            '                                          , CInt(cboEmployee.SelectedValue) _
            '                                          , CInt(cboDepartment.SelectedValue) _
            '                                          , CInt(cboSections.SelectedValue) _
            '                                          , CInt(cboUnit.SelectedValue) _
            '                                          , CInt(cboGrade.SelectedValue) _
            '                                          , 0 _
            '                                          , CInt(cboClass.SelectedValue) _
            '                                          , CInt(cboCostCenter.SelectedValue) _
            '                                          , 0 _
            '                                          , CInt(cboJob.SelectedValue) _
            '                                          , CInt(cboPayPoint.SelectedValue) _
            '                                          , ConfigParameter._Object._CurrentDateAndTime.Date, ConfigParameter._Object._CurrentDateAndTime.Date)
            Cursor.Current = Cursors.WaitCursor
            'Anjan [10 June 2015] -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'dsEmployee = objEmployee.GetEmployeeList("Employee", False, True _
            '                                          , CInt(cboEmployee.SelectedValue) _
            '                                          , CInt(cboDepartment.SelectedValue) _
            '                                          , CInt(cboSections.SelectedValue) _
            '                                          , CInt(cboUnit.SelectedValue) _
            '                                          , CInt(cboGrade.SelectedValue) _
            '                                          , 0 _
            '                                          , CInt(cboClass.SelectedValue) _
            '                                          , CInt(cboCostCenter.SelectedValue) _
            '                                          , 0 _
            '                                          , CInt(cboJob.SelectedValue) _
            '                                          , CInt(cboPayPoint.SelectedValue) _
            '                                          , mdtPayPeriodStartDate, mdtPayPeriodEndDate)

            'Sohail (17 Sep 2019) -- Start
            'NMB Enhancement # TC006: system should be able to bind earning/deduction (benefit) with specific allocation and automatically assign to employee as per his/her allocation.
            'dsEmployee = objEmployee.GetEmployeeList(FinancialYear._Object._DatabaseName, _
            '                              User._Object._Userunkid, _
            '                              FinancialYear._Object._YearUnkid, _
            '                              Company._Object._Companyunkid, _
            '                              mdtPayPeriodStartDate, _
            '                              mdtPayPeriodEndDate, _
            '                              ConfigParameter._Object._UserAccessModeSetting, _
            '                               True, False, "Employee", False, CInt(cboEmployee.SelectedValue), CInt(cboDepartment.SelectedValue) _
            '                                          , CInt(cboSections.SelectedValue) _
            '                                          , CInt(cboUnit.SelectedValue) _
            '                                          , CInt(cboGrade.SelectedValue) _
            '                                          , 0 _
            '                                          , CInt(cboClass.SelectedValue) _
            '                                          , CInt(cboCostCenter.SelectedValue) _
            '                                          , 0 _
            '                                          , CInt(cboJob.SelectedValue) _
            '                                          , CInt(cboPayPoint.SelectedValue))
            dsEmployee = objEmployee.GetEmployeeList(FinancialYear._Object._DatabaseName, _
                                          User._Object._Userunkid, _
                                          FinancialYear._Object._YearUnkid, _
                                          Company._Object._Companyunkid, _
                                          mdtPayPeriodStartDate, _
                                          mdtPayPeriodEndDate, _
                                          ConfigParameter._Object._UserAccessModeSetting, _
                                          True, False, "Employee", False, CInt(cboEmployee.SelectedValue) _
                                                     , strAdvanceFilterQuery:=mstrAdvanceFilter)
            'Sohail (17 Sep 2019) -- End

            'Sohail (06 Mar 2012) -- End
            'Sohail (06 Jan 2012) -- End
            'Anjan [10 June 2015] -- End

            'Sohail (17 Sep 2019) -- Start
            'NMB Enhancement # TC006: system should be able to bind earning/deduction (benefit) with specific allocation and automatically assign to employee as per his/her allocation.
            'Dim lvItem As ListViewItem
            'Dim lvArray As New List(Of ListViewItem) 'Sohail (03 Nov 2010)
            'lvEmployeeList.Items.Clear()
            'lvEmployeeList.BeginUpdate()

            'For Each dtRow As DataRow In dsEmployee.Tables("Employee").Rows
            '    lvItem = New ListViewItem
            '    With lvItem
            '        .Text = ""
            '        .Tag = dtRow.Item("employeeunkid").ToString
            '        .SubItems.Add(dtRow.Item("employeecode").ToString)
            '        .SubItems.Add(dtRow.Item("employeename").ToString)
            '        'Sohail (15 Oct 2010) -- Start
            '        If mintEmployeeID > 0 AndAlso CInt(.Tag) = mintEmployeeID Then
            '            RemoveHandler lvEmployeeList.ItemChecked, AddressOf lvEmployeeList_ItemChecked
            '            .Checked = True
            '            AddHandler lvEmployeeList.ItemChecked, AddressOf lvEmployeeList_ItemChecked
            '        End If

            '        'Vimal (01 Nov 2010) -- Start 
            '        If mblnExemptFlag = True Then
            '            For Each s As String In empArray
            '                If s = .Tag.ToString Then
            '                    RemoveHandler lvEmployeeList.ItemChecked, AddressOf lvEmployeeList_ItemChecked 'Sohail (03 Nov 2010)
            '                    .Checked = True
            '                    AddHandler lvEmployeeList.ItemChecked, AddressOf lvEmployeeList_ItemChecked 'Sohail (03 Nov 2010)
            '                    'Sohail (03 Nov 2010) -- Start
            '                    lvArray.Add(lvItem)
            '                    'lvEmployeeList.Items.Add(lvItem)
            '                    'Sohail (03 Nov 2010) -- End
            '                End If
            '            Next
            '        Else
            '            'Sohail (03 Nov 2010) -- Start
            '            lvArray.Add(lvItem)
            '            'RemoveHandler lvEmployeeList.ItemChecked, AddressOf lvEmployeeList_ItemChecked
            '            'lvEmployeeList.Items.Add(lvItem)
            '            'AddHandler lvEmployeeList.ItemChecked, AddressOf lvEmployeeList_ItemChecked
            '            'Sohail (03 Nov 2010) -- End
            '        End If
            '        'Vimal (01 Nov 2010) -- End

            '        'Sohail (15 Oct 2010) -- End

            '        'Vimal (01 Nov 2010) -- Start 
            '        'lvArray.Add(lvItem)
            '        'Vimal (01 Nov 2010) -- End
            '    End With
            'Next
                        'RemoveHandler lvEmployeeList.ItemChecked, AddressOf lvEmployeeList_ItemChecked
            'lvEmployeeList.Items.AddRange(lvArray.ToArray) 'Sohail (03 Nov 2010)
                        'AddHandler lvEmployeeList.ItemChecked, AddressOf lvEmployeeList_ItemChecked
            'lvArray = Nothing
            'If lvEmployeeList.Items.Count > 15 Then
            '    colhName.Width = 193 - 18
            'Else
            '    colhName.Width = colhName.Width
            'End If
            'lvEmployeeList.EndUpdate()
            mintTotalEmployee = dsEmployee.Tables("Employee").Rows.Count
            objlblEmpCount.Text = "( 0 / " & mintTotalEmployee.ToString & " )"

            If dsEmployee.Tables(0).Columns.Contains("IsChecked") = False Then
                Dim dtCol As DataColumn
                dtCol = New DataColumn("IsChecked", System.Type.GetType("System.Boolean"))
                dtCol.DefaultValue = False
                dtCol.AllowDBNull = False
                dsEmployee.Tables(0).Columns.Add(dtCol)
                    End If

            Dim dtTable As DataTable = dsEmployee.Tables("Employee")

            Dim strFilter As String = ""
            If mintEmployeeID > 0 Then
                strFilter &= " AND employeeunkid = " & mintEmployeeID & " "
            End If
            If mblnExemptFlag = True AndAlso mstrEmployeeIds.Trim.Length > 0 Then
                strFilter &= " AND employeeunkid IN (" & mstrEmployeeIds & ") "
            End If
            If strFilter.Trim.Length > 0 Then
                Dim dr() As DataRow = dtTable.Select(strFilter.Substring(4))
                If dr.Length > 0 Then
                    For Each dtRow As DataRow In dr
                        dtRow.Item("IsChecked") = True
            Next
                    dtTable.AcceptChanges()
                End If
            End If
            dvEmployee = dtTable.DefaultView
            dgEmployee.AutoGenerateColumns = False
            objdgcolhCheck.DataPropertyName = "IsChecked"
            objdgcolhEmployeeunkid.DataPropertyName = "employeeunkid"
            dgColhEmpCode.DataPropertyName = "employeecode"
            dgColhEmployee.DataPropertyName = "employeename"

            dgEmployee.DataSource = dvEmployee
            dvEmployee.Sort = "IsChecked DESC, employeename "
            'Sohail (17 Sep 2019) -- End
            Cursor.Current = Cursors.Default 'Sohail (06 Mar 2012)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillList", mstrModuleName)
        Finally
            objEmployee = Nothing
            dsEmployee = Nothing
        End Try
    End Sub

    Private Sub CheckAllEmployee(ByVal blnCheckAll As Boolean)
        Try
            'Sohail (17 Sep 2019) -- Start
            'NMB Enhancement # TC006: system should be able to bind earning/deduction (benefit) with specific allocation and automatically assign to employee as per his/her allocation.
            'For Each lvItem As ListViewItem In lvEmployeeList.Items
            '    RemoveHandler lvEmployeeList.ItemChecked, AddressOf lvEmployeeList_ItemChecked 'Sohail (03 Nov 2010)
            '    lvItem.Checked = blnCheckAll
            '    AddHandler lvEmployeeList.ItemChecked, AddressOf lvEmployeeList_ItemChecked 'Sohail (03 Nov 2010)
            'Next
            If dvEmployee IsNot Nothing Then
                For Each dr As DataRowView In dvEmployee
                    dr.Item("IsChecked") = blnCheckAll
                    dr.EndEdit()
            Next
                dvEmployee.ToTable.AcceptChanges()

                Dim drRow As DataRow() = dvEmployee.Table.Select("IsChecked = 1")
                mintCount = drRow.Length
                objlblEmpCount.Text = "( " & mintCount.ToString & " / " & mintTotalEmployee.ToString & " )"
            End If
            'Sohail (17 Sep 2019) -- End
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "CheckAllEmployee", mstrModuleName)
        End Try
    End Sub
    'Sohail (09 Oct 2010) -- End

    'Sohail (17 Sep 2019) -- Start
    'NMB Enhancement # TC006: system should be able to bind earning/deduction (benefit) with specific allocation and automatically assign to employee as per his/her allocation.
    Private Sub SetCheckBoxValue()
        Try

            RemoveHandler objchkSelectAllEmployee.CheckedChanged, AddressOf chkSelectAllEmployee_CheckedChanged

            Dim blnIsChecked As Boolean = Convert.ToBoolean(dgEmployee.CurrentRow.Cells(objdgcolhCheck.Index).Value)

            If blnIsChecked = True Then
                mintCount += 1
            Else
                mintCount -= 1
            End If

            objlblEmpCount.Text = "( " & mintCount.ToString & " / " & mintTotalEmployee.ToString & " )"

            If mintCount <= 0 Then
                objchkSelectAllEmployee.CheckState = CheckState.Unchecked
            ElseIf mintCount < dgEmployee.Rows.Count Then
                objchkSelectAllEmployee.CheckState = CheckState.Indeterminate
            ElseIf mintCount = dgEmployee.Rows.Count Then
                objchkSelectAllEmployee.CheckState = CheckState.Checked
            End If

            AddHandler objchkSelectAllEmployee.CheckedChanged, AddressOf chkSelectAllEmployee_CheckedChanged
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetCheckBoxValue", mstrModuleName)
        End Try

    End Sub

    Private Sub SetDefaultSearchEmpText()
        Try
            mstrSearchEmpText = lblSearchEmp.Text
            With txtSearchEmp
                .ForeColor = Color.Gray
                .Text = mstrSearchEmpText
                .Font = New Font(Me.Font.FontFamily, Me.Font.Size, FontStyle.Italic)
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "", mstrModuleName)
        End Try
    End Sub
    'Sohail (17 Sep 2019) -- End

#End Region

#Region " Form's Events "

    Private Sub frmEmployee_Exemption_AddEdit_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        objEmpExemption = New clsemployee_exemption_Tran
        Try
            Call Set_Logo(Me, gApplicationType)
            'Anjan (02 Sep 2011)-Start
            'Issue : Including Language Settings.
            Language.setLanguage(Me.Name)
            Call OtherSettings()
            'Anjan (02 Sep 2011)-End 

            SetColor()
            FillCombo()
            'Call FillList() 'Sohail (09 Oct 2010)

            'Sohail (17 Sep 2019) -- Start
            'NMB Enhancement # TC006: system should be able to bind earning/deduction (benefit) with specific allocation and automatically assign to employee as per his/her allocation.
            Call SetDefaultSearchEmpText()
            'Sohail (17 Sep 2019) -- End

            If menAction = enAction.EDIT_ONE Then
                objEmpExemption._Exemptionunkid = mintExemptionUnkid
                cboYear.Enabled = False
                'Sohail (11 Sep 2010) -- Start
                'cboPeriod.Enabled = False 
                lvPeriod.Enabled = False
                'Sohail (11 Sep 2010) -- End
                cboEmployee.Enabled = False
                'Sohail (30 Aug 2010) -- Start
                objSearchEmployee.Enabled = False
                'Sohail (30 Aug 2010) -- End
                GetValue()
            Else
                GetValue()
                cboYear.SelectedValue = mintYearID
                'cboPeriod.SelectedValue = mintPeriodID 'Sohail (11 Sep 2010)
                'cboEmployee.SelectedValue = mintEmployeeID 'Sohail (15 Oct 2010)
                cboTransactionhead.SelectedValue = mintTranHeadID

                'Vimal (01 Nov 2010) -- Start 
                If mblnExemptFlag = True Then
                    Dim objTranHead As New clsTransactionHead
                    'Sohail (21 Aug 2015) -- Start
                    'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                    'objTranHead._Tranheadunkid = mintTranHeadID
                    objTranHead._Tranheadunkid(FinancialYear._Object._DatabaseName) = mintTranHeadID
                    'Sohail (21 Aug 2015) -- End
                    cboTrnHeadType.SelectedValue = objTranHead._Trnheadtype_Id
                    cboTypeOf.SelectedValue = objTranHead._Typeof_id
                    cboTransactionhead.SelectedValue = mintTranHeadID

                    cboTransactionhead.Enabled = False
                    cboTrnHeadType.Enabled = False
                    cboTypeOf.Enabled = False
                    objSearchTransactionhead.Enabled = False
                End If
                'Vimal (01 Nov 2010) -- End

            End If
            'FillCombo()
            'GetValue()

            cboYear.Select()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmEmployee_Exemption_AddEdit_Load", mstrModuleName)
        End Try
    End Sub

    Private Sub frmEmployee_Exemption_AddEdit_KeyDown(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles MyBase.KeyDown
        Try
            If e.Control = True And e.KeyCode = Keys.S Then
                btnSave_Click(sender, e)
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmEmployee_Exemption_AddEdit_KeyDown", mstrModuleName)
        End Try
    End Sub

    Private Sub frmEmployee_Exemption_AddEdit_KeyPress(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles MyBase.KeyPress
        Try
            If Asc(e.KeyChar) = 13 Then
                SendKeys.Send("{TAB}")
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmEmployee_Exemption_AddEdit_KeyPress", mstrModuleName)
        End Try
    End Sub

    Private Sub frmEmployee_Exemption_AddEdit_FormClosed(ByVal sender As System.Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles MyBase.FormClosed

        objEmpExemption = Nothing

    End Sub
    'Anjan (02 Sep 2011)-Start
    'Issue : Including Language Settings.
    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsemployee_exemption_Tran.SetMessages()
            objfrm._Other_ModuleNames = "clsemployee_exemption_Tran"
            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub
    'Anjan (02 Sep 2011)-End 
#End Region

#Region " Button's Events "

    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Dim blnFlag As Boolean = False
        Dim strPeriodList As String = ""
        Dim strEmpList As String = ""
        Dim objTnA As New clsTnALeaveTran 'Sohail (18 Oct 2012)

        Try
            If CInt(cboYear.SelectedValue) = 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Year is compulsory information.Please Select Year."), enMsgBoxStyle.Information)
                cboYear.Focus()
                Exit Sub
                'Sohail (11 Sep 2010) -- Start
                'ElseIf CInt(cboPeriod.SelectedValue) = 0 Then
            ElseIf lvPeriod.CheckedItems.Count = 0 Then
                'Sohail (11 Sep 2010) -- End
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Period is compulsory information.Please Select Period."), enMsgBoxStyle.Information)
                'cboPeriod.Focus() 'Sohail (11 Sep 2010)
                Exit Sub
                'Sohail (09 Oct 2010) -- Start
                'ElseIf CInt(cboEmployee.SelectedValue) = 0 Then
                '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Employee is compulsory information.Please Select Employee."), enMsgBoxStyle.Information)
                '    cboEmployee.Focus()
                '    Exit Sub

                'Sohail (17 Sep 2019) -- Start
                'NMB Enhancement # TC006: system should be able to bind earning/deduction (benefit) with specific allocation and automatically assign to employee as per his/her allocation.
                'ElseIf lvEmployeeList.CheckedItems.Count = 0 Then
                '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "Please select atleast one Employee from list."), enMsgBoxStyle.Information)
                '    lvEmployeeList.Focus()
                '    Exit Sub
            ElseIf dvEmployee.Table.Select("IsChecked = 1 ").Length <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "Please select atleast one Employee from list."), enMsgBoxStyle.Information)
                dgEmployee.Focus()
                Exit Sub
                'Sohail (17 Sep 2019) -- End
                'Sohail (09 Oct 2010) -- End
            ElseIf CInt(cboTransactionhead.SelectedValue) = 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Transaction Head is compulsory information.Please Select Transaction Head."), enMsgBoxStyle.Information)
                cboTransactionhead.Focus()
                Exit Sub
            End If

            'Sohail (18 Oct 2012) -- Start
            'TRA - ENHANCEMENT
            'Sohail (17 Sep 2019) -- Start
            'NMB Enhancement # TC006: system should be able to bind earning/deduction (benefit) with specific allocation and automatically assign to employee as per his/her allocation.
            'For Each lvItem As ListViewItem In lvEmployeeList.CheckedItems
            '    If strEmpList = "" Then
            '        strEmpList = lvItem.Tag.ToString
            '    Else
            '        strEmpList &= "," & lvItem.Tag.ToString
            '    End If
            'Next
            'Sohail (17 Sep 2019) -- Start
            'NMB Enhancement # TC006: system should be able to bind earning/deduction (benefit) with specific allocation and automatically assign to employee as per his/her allocation.
            'strEmpList = String.Join(",", (From lv In lvEmployeeList.CheckedItems.Cast(Of ListViewItem)() Select (lv.Tag.ToString)).ToArray)
            strEmpList = String.Join(",", (From p In dvEmployee.Table Where (CBool(p.Item("IsChecked")) = True) Select (p.Item("employeeunkid").ToString)).ToArray)
            'Sohail (17 Sep 2019) -- End
            strPeriodList = String.Join(",", (From lv In lvPeriod.CheckedItems.Cast(Of ListViewItem)() Select (lv.Tag.ToString)).ToArray)
            'Sohail (17 Sep 2019) -- End

            If lvPeriod.Items(0).Checked = True Then
                Dim objPeriod As New clscommom_period_Tran
                'Sohail (21 Aug 2015) -- Start
                'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                'objPeriod._Periodunkid = CInt(lvPeriod.Items(0).Tag)
                objPeriod._Periodunkid(FinancialYear._Object._DatabaseName) = CInt(lvPeriod.Items(0).Tag)
                'Sohail (21 Aug 2015) -- End
                If objTnA.IsPayrollProcessDone(CInt(lvPeriod.Items(0).Tag), strEmpList, objPeriod._End_Date) = True Then
                    'Sohail (19 Apr 2019) -- Start
                    'Enhancement - 76.1 - Option to open process payroll screen if process payroll is already done message come.
                    'eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 7, "Sorry, Process Payroll is already done for last date of one of the selected Period. Please Void Process Payroll first for selected employee to exempt head."), enMsgBoxStyle.Information)
                    If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 7, "Sorry, Process Payroll is already done for last date of one of the selected Period. Please Void Process Payroll first for selected employee to exempt head.") & vbCrLf & vbCrLf & Language.getMessage(mstrModuleName, 8, "Do you want to void Payroll?"), CType(enMsgBoxStyle.Information + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.Yes Then
                        Dim objFrm As New frmProcessPayroll
                        objFrm.ShowDialog()
                    End If
                    'Sohail (19 Apr 2019) -- End
                    Exit Try
                End If
            End If
            'Sohail (18 Oct 2012) -- End

            'Sohail (28 Jan 2012) -- Start
            'TRA - ENHANCEMENT
            Dim objTranHead As New clsTransactionHead
            'Sohail (27 Sep 2019) -- Start
            'NMB Enhancement # : System should not allow user to map allocation with benefit while payroll is already processed for employees in the same allocation.
            'Dim ds As DataSet = objTranHead.IsTranHeadUsedInAnyFormula(CInt(cboTransactionhead.SelectedValue), "TranHead")
            Dim ds As DataSet = objTranHead.IsTranHeadUsedInAnyFormula(CInt(cboTransactionhead.SelectedValue), mdtPayPeriodEndDate, "TranHead")
            'Sohail (27 Sep 2019) -- End
            If ds.Tables("TranHead").Rows.Count > 0 Then
                'Sohail (17 Sep 2019) -- Start
                'NMB Enhancement # TC006: system should be able to bind earning/deduction (benefit) with specific allocation and automatically assign to employee as per his/her allocation.
                'eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 6, "Sorry! You can not Exempt this Transaction Head. This Transaction Head is used in the formula of") & " '" & ds.Tables("TranHead").Rows(0).Item("trnheadname").ToString & "'.", enMsgBoxStyle.Information)
                'cboTransactionhead.Focus()
                'Exit Sub
                For Each dsRow As DataRow In ds.Tables(0).Rows
                    For Each dtRow As DataRow In dvEmployee.Table.Select("IsChecked = 1")
                        Dim dsList As DataSet = objEmpExemption.GetList(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, mdtPayPeriodStartDate, mdtPayPeriodEndDate, ConfigParameter._Object._UserAccessModeSetting, True, False, "List", True, True, CInt(dtRow.Item("employeeunkid")), CInt(dsRow.Item("tranheadunkid")), 0, , , , strPeriodList)
                        If dsList.Tables(0).Rows.Count <= 0 Then
                            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 6, "Sorry! You can not Exempt this Transaction Head. This Transaction Head is used in the formula of") & " '" & dsRow.Item("trnheadname").ToString & "'" & " " & Language.getMessage(mstrModuleName, 9, "for employee :") & " " & dtRow.Item("employeename").ToString & ".", enMsgBoxStyle.Information)
                cboTransactionhead.Focus()
                Exit Sub
            End If
                    Next
                Next
                'Sohail (17 Sep 2019) -- End                
            End If
            'Sohail (28 Jan 2012) -- End

            Call SetValue()

            'Sohail (11 Sep 2010) -- Start
            If lvPeriod.CheckedItems.Count > 1 Then
                If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Are you sure you want to Exempt this Transaction Head for all selected Period?"), CType(enMsgBoxStyle.Question + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.No Then
                    Exit Sub
                End If
            End If
            'Sohail (17 Sep 2019) -- Start
            'NMB Enhancement # TC006: system should be able to bind earning/deduction (benefit) with specific allocation and automatically assign to employee as per his/her allocation.
            'For Each lvItem As ListViewItem In lvPeriod.CheckedItems
            '    If strPeriodList = "" Then
            '        strPeriodList = lvItem.SubItems(colhPeriod.Index).Tag.ToString
            '    Else
            '        strPeriodList &= "," & lvItem.SubItems(colhPeriod.Index).Tag.ToString
            '    End If
            'Next
            'Sohail (17 Sep 2019) -- End
            'Sohail (11 Sep 2010) -- End

            'Sohail (09 Oct 2010) -- Start
            'Sohail (18 Oct 2012) -- Start
            'TRA - ENHANCEMENT
            'For Each lvItem As ListViewItem In lvEmployeeList.CheckedItems
            '    If strEmpList = "" Then
            '        strEmpList = lvItem.Tag.ToString
            '    Else
            '        strEmpList &= "," & lvItem.Tag.ToString
            '    End If
            'Next
            'Sohail (18 Oct 2012) -- End
            'Sohail (09 Oct 2010) -- End

            'Vimal (01 Nov 2010) -- Start 
            If mblnExemptFlag = True Then
                mstrExemptPeriodIds = strPeriodList
                Me.Close()
                Exit Sub
            End If
            'Vimal (01 Nov 2010) -- End

            'S.SANDEEP [28-May-2018] -- START
            'ISSUE/ENHANCEMENT : {Audit Trails} 
            objEmpExemption._FormName = mstrModuleName
            objEmpExemption._LoginEmployeeUnkid = 0
            objEmpExemption._ClientIP = getIP()
            objEmpExemption._HostName = getHostName()
            objEmpExemption._FromWeb = False
            objEmpExemption._AuditUserId = User._Object._Userunkid
objEmpExemption._CompanyUnkid = Company._Object._Companyunkid
            objEmpExemption._AuditDate = ConfigParameter._Object._CurrentDateAndTime
            'S.SANDEEP [28-May-2018] -- END

            If menAction = enAction.EDIT_ONE Then
                objEmpExemption._Periodunkid = CInt(lvPeriod.CheckedItems(0).SubItems(colhPeriod.Index).Tag) 'Sohail (11 Sep 2010)
                'S.SANDEEP [04 JUN 2015] -- START
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                'blnFlag = objEmpExemption.Update()
                blnFlag = objEmpExemption.Update(ConfigParameter._Object._CurrentDateAndTime)
                'S.SANDEEP [04 JUN 2015] -- END
            Else
                'Sohail (11 Sep 2010) -- Start
                'blnFlag = objEmpExemption.Insert()
                'Sohail (09 Oct 2010) -- Start
                'blnFlag = objEmpExemption.InsertByPeriod(strPeriodList)

                'S.SANDEEP [04 JUN 2015] -- START
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                'blnFlag = objEmpExemption.InsertByPeriod(strEmpList, strPeriodList)
                blnFlag = objEmpExemption.InsertByPeriod(strEmpList, strPeriodList, ConfigParameter._Object._CurrentDateAndTime)
                'S.SANDEEP [04 JUN 2015] -- END

                'Sohail (09 Oct 2010) -- End
                'Sohail (11 Sep 2010) -- End
            End If

            If blnFlag = False And objEmpExemption._Message <> "" Then
                eZeeMsgBox.Show(objEmpExemption._Message, enMsgBoxStyle.Information)
            End If

            If blnFlag Then
                mblnCancel = False
                If menAction = enAction.ADD_CONTINUE Then
                    objEmpExemption = Nothing
                    objEmpExemption = New clsemployee_exemption_Tran
                    Call GetValue()
                    cboYear.Select()
                Else
                    mintExemptionUnkid = objEmpExemption._Exemptionunkid
                    Me.Close()
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnSave_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objSearchEmployee_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objSearchEmployee.Click
        Dim objfrmCommonsearch As New frmCommonSearch
        Try
            objfrmCommonsearch.DataSource = CType(cboEmployee.DataSource, DataTable)
            objfrmCommonsearch.ValueMember = cboEmployee.ValueMember
            objfrmCommonsearch.DisplayMember = cboEmployee.DisplayMember
            objfrmCommonsearch.CodeMember = "employeecode"
            If objfrmCommonsearch.DisplayDialog Then
                cboEmployee.SelectedValue = objfrmCommonsearch.SelectedValue
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objSearchEmployee_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objSearchTransactionhead_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objSearchTransactionhead.Click
        Dim objfrmCommonsearch As New frmCommonSearch
        Dim objTranHead As New clsTransactionHead
        Dim dtTable As DataTable
        Try
            'Sohail (09 Oct 2010) -- Start
            dtTable = New DataView(CType(cboTransactionhead.DataSource, DataTable), "tranheadunkid <> 0", "", DataViewRowState.CurrentRows).ToTable
            'objfrmCommonsearch.DataSource = CType(cboTransactionhead.DataSource, DataTable)
            objfrmCommonsearch.DataSource = dtTable
            'Sohail (09 Oct 2010) -- End
            objfrmCommonsearch.ValueMember = cboTransactionhead.ValueMember
            objfrmCommonsearch.DisplayMember = cboTransactionhead.DisplayMember
            objfrmCommonsearch.CodeMember = "code"
            If objfrmCommonsearch.DisplayDialog Then
                cboTransactionhead.SelectedValue = objfrmCommonsearch.SelectedValue
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objSearchTransactionhead_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Me.Close()
    End Sub

#End Region

#Region "Dropdown's Event"

    Private Sub cboYear_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboYear.SelectedIndexChanged 'Sohail (03 Nov 2010)
        'Dim dsFill As DataSet = Nothing 'Sohail (06 Mar 2012)
        Try
            'Sohail (06 Mar 2012) -- Start
            'TRA - ENHANCEMENT
            'Dim objPeriod As New clscommom_period_Tran
            ''Sohail (11 Sep 2010) -- Start
            ''dsFill = objPeriod.getListForCombo(enModuleRefenence.Payroll, CInt(cboYear.SelectedValue), "Period", True, enStatusType.Open)
            ''cboPeriod.ValueMember = "periodunkid"
            ''cboPeriod.DisplayMember = "name"
            ''cboPeriod.DataSource = dsFill.Tables("Period")
            'lvPeriod.Items.Clear()
            'If CInt(cboYear.SelectedValue) > 0 Then
            '    dsFill = objPeriod.GetList("Period", enModuleReference.Payroll, True, enStatusType.Open)
            '    Dim lvItem As ListViewItem
            '    For Each dsRow As DataRow In dsFill.Tables("Period").Rows
            '        lvItem = New ListViewItem

            '        lvItem.Tag = dsRow.Item("periodunkid").ToString

            '        lvItem.SubItems.Add(dsRow.Item("period_name").ToString)
            '        lvItem.SubItems(colhPeriod.Index).Tag = CInt(dsRow.Item("periodunkid").ToString)

            '        lvItem.SubItems.Add(eZeeDate.convertDate(dsRow.Item("start_date").ToString).ToShortDateString)

            '        lvItem.SubItems.Add(eZeeDate.convertDate(dsRow.Item("end_date").ToString).ToShortDateString)

            '        If menAction = enAction.EDIT_ONE AndAlso objEmpExemption._Periodunkid = CInt(dsRow.Item("periodunkid").ToString) Then
            '            lvItem.Checked = True
            '            'Sohail (15 Oct 2010) -- Start
            '        ElseIf mintPeriodID > 0 AndAlso CInt(dsRow.Item("periodunkid").ToString) = mintPeriodID Then
            '            lvItem.Checked = True
            '            'Sohail (15 Oct 2010) -- End
            '        End If
            '        RemoveHandler lvPeriod.ItemChecked, AddressOf lvPeriod_ItemChecked
            '        lvPeriod.Items.Add(lvItem)
            '        AddHandler lvPeriod.ItemChecked, AddressOf lvPeriod_ItemChecked
            '    Next
            'End If
            'If lvPeriod.Items.Count > 12 Then
            '    colhPeriod.Width = 140 - 18
            'Else
            '    colhPeriod.Width = 140
            'End If
            'objPeriod = Nothing
            ''Sohail (11 Sep 2010) -- End
            Call FillList_Period()
            'Sohail (06 Mar 2012) -- End
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboYear_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

    'Sohail (11 Sep 2010) -- Start
    'Private Sub cboPeriod_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
    '    Try
    '        objlblPeriod.Text = ""
    '        If CInt(cboPeriod.SelectedValue) > 0 Then
    '            Dim objPeriod As New clscommom_period_Tran
    '            objPeriod._Periodunkid = CInt(cboPeriod.SelectedValue)
    '            objlblPeriod.Text = objPeriod._Start_Date.ToShortDateString & " To " & objPeriod._End_Date.ToShortDateString
    '        End If
    '    Catch ex As Exception
    '        DisplayError.Show("-1", ex.Message, "cboPeriod_SelectedIndexChanged", mstrModuleName)
    '    End Try
    'End Sub
    'Sohail (11 Sep 2010) -- End

    'Sohail (09 Oct 2010) -- Start
    'Private Sub cboEmployee_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboEmployee.SelectedIndexChanged
    '    Dim objTranHead As New clsTransactionHead
    '    Dim objED As New clsEarningDeduction
    '    Dim dsCombo As DataSet = Nothing
    '    Dim ds As DataSet
    '    Dim dt As DataTable
    '    Dim dtTable As New DataTable
    '    Dim dtRow As DataRow
    '    Try


    '        dtTable.Columns.Add(New DataColumn("Id", GetType(System.Int32)))
    '        dtTable.Columns.Add(New DataColumn("Name", GetType(System.String)))

    '        ds = objTranHead.getComboList("Heads", True, enTranHeadType.EmployersStatutoryContributions, 1, 1)
    '        dt = New DataView(ds.Tables("Heads"), "tranheadunkid = 0", "", DataViewRowState.CurrentRows).ToTable

    '        'Adding First Item As 'Select'
    '        dtRow = dtTable.NewRow
    '        dtRow.Item("Id") = CInt(dt.Rows(0).Item("tranheadunkid").ToString)
    '        dtRow.Item("Name") = dt.Rows(0).Item("name").ToString
    '        dtTable.Rows.Add(dtRow)

    '        dsCombo = objED.GetList("ED", CInt(cboEmployee.SelectedValue))

    '        For Each dsRow As DataRow In dsCombo.Tables("ED").Rows
    '            If CInt(dsRow.Item("typeof_id").ToString) <> enTypeOf.Salary Then
    '                dtRow = dtTable.NewRow
    '                dtRow.Item("Id") = CInt(dsRow.Item("tranheadunkid").ToString)
    '                dtRow.Item("Name") = dsRow.Item("trnheadname").ToString
    '                dtTable.Rows.Add(dtRow)
    '            End If
    '        Next
    '        cboTransactionhead.ValueMember = "Id"
    '        cboTransactionhead.DisplayMember = "Name"
    '        cboTransactionhead.DataSource = dtTable

    '    Catch ex As Exception
    '        DisplayError.Show("-1", ex.Message, "cboEmployee_SelectedIndexChanged", mstrModuleName)
    '    Finally
    '        objED = Nothing
    '    End Try
    'End Sub

    Private Sub cboTrnHeadType_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboTrnHeadType.SelectedIndexChanged 'Sohail (03 Nov 2010)
        Dim objMaster As New clsMasterData
        Dim dsList As DataSet
        Dim dtTable As DataTable
        Try
            dsList = objMaster.getComboListTypeOf("TypeOf", CInt(cboTrnHeadType.SelectedValue))
            dtTable = New DataView(dsList.Tables("TypeOf"), "Id <>  " & enTypeOf.Salary & "", "", DataViewRowState.CurrentRows).ToTable 'Except Salary Heads

            With cboTypeOf
                .ValueMember = "Id"
                .DisplayMember = "Name"
                .DataSource = dtTable
                If .Items.Count > 0 Then .SelectedValue = 0
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboTrnHeadType_SelectedIndexChanged", mstrModuleName)
        Finally
            objMaster = Nothing
            dsList = Nothing
            dtTable = Nothing
        End Try
    End Sub

    Private Sub cboTypeOf_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboTypeOf.SelectedIndexChanged 'Sohail (03 Nov 2010)
        Dim objTranHead As New clsTransactionHead
        Dim dsList As DataSet
        Dim dtTable As DataTable
        Try

            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'dsList = objTranHead.getComboList("TranHead", True, CInt(cboTrnHeadType.SelectedValue), , CInt(IIf(CInt(cboTypeOf.SelectedValue) > 0, CInt(cboTypeOf.SelectedValue), -1)))
            dsList = objTranHead.getComboList(FinancialYear._Object._DatabaseName, "TranHead", True, CInt(cboTrnHeadType.SelectedValue), , CInt(IIf(CInt(cboTypeOf.SelectedValue) > 0, CInt(cboTypeOf.SelectedValue), -1)))
            'Sohail (21 Aug 2015) -- End
            dtTable = New DataView(dsList.Tables("TranHead"), "typeof_id <> " & enTypeOf.Salary & "", "", DataViewRowState.CurrentRows).ToTable 'Except Salary Heads
            With cboTransactionhead
                .ValueMember = "tranheadunkid"
                .DisplayMember = "Name"
                .DataSource = dtTable
                If .Items.Count > 0 Then .SelectedValue = 0
            End With

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboTypeOf_SelectedIndexChanged", mstrModuleName)
        Finally
            objTranHead = Nothing
            dsList = Nothing
        End Try
    End Sub
    'Sohail (09 Oct 2010) -- End
#End Region

    'Sohail (11 Sep 2010) -- Start
#Region " Listview's Events "
    Private Sub lvPeriod_ItemChecked(ByVal sender As Object, ByVal e As System.Windows.Forms.ItemCheckedEventArgs) Handles lvPeriod.ItemChecked
        Try
            'Sohail (03 Nov 2010) -- Start
            'If lvPeriod.CheckedItems.Count = lvPeriod.Items.Count Then
            '    chkSelectAll.Checked = True
            'ElseIf lvPeriod.CheckedItems.Count = 0 Then
            '    chkSelectAll.Checked = False
            'End If
            If lvPeriod.CheckedItems.Count <= 0 Then
                RemoveHandler objchkSelectAll.CheckedChanged, AddressOf chkSelectAll_CheckedChanged
                objchkSelectAll.CheckState = CheckState.Unchecked
                AddHandler objchkSelectAll.CheckedChanged, AddressOf chkSelectAll_CheckedChanged
            ElseIf lvPeriod.CheckedItems.Count < lvPeriod.Items.Count Then
                RemoveHandler objchkSelectAll.CheckedChanged, AddressOf chkSelectAll_CheckedChanged
                objchkSelectAll.CheckState = CheckState.Indeterminate
                AddHandler objchkSelectAll.CheckedChanged, AddressOf chkSelectAll_CheckedChanged
            ElseIf lvPeriod.CheckedItems.Count = lvPeriod.Items.Count Then
                RemoveHandler objchkSelectAll.CheckedChanged, AddressOf chkSelectAll_CheckedChanged
                objchkSelectAll.CheckState = CheckState.Checked
                AddHandler objchkSelectAll.CheckedChanged, AddressOf chkSelectAll_CheckedChanged
            End If
            'Sohail (03 Nov 2010) -- End

            'Sohail (06 Mar 2012) -- Start
            'TRA - ENHANCEMENT
            If e.Item.Checked = True Then
                If mdtPayPeriodStartDate = Nothing OrElse mdtPayPeriodStartDate > CDate(e.Item.SubItems(colhStartDate.Index).Text) Then
                    mdtPayPeriodStartDate = CDate(e.Item.SubItems(colhStartDate.Index).Text)
                End If
                If mdtPayPeriodEndDate = Nothing OrElse mdtPayPeriodEndDate < CDate(e.Item.SubItems(colhEndDate.Index).Text) Then
                    mdtPayPeriodEndDate = CDate(e.Item.SubItems(colhEndDate.Index).Text)
                End If
            ElseIf e.Item.Checked = False Then
                If mdtPayPeriodStartDate = CDate(e.Item.SubItems(colhStartDate.Index).Text) Then
                    mdtPayPeriodStartDate = Nothing
                    For Each lvItem As ListViewItem In lvPeriod.CheckedItems
                        If mdtPayPeriodStartDate = Nothing OrElse mdtPayPeriodStartDate > CDate(lvItem.SubItems(colhStartDate.Index).Text) Then
                            mdtPayPeriodStartDate = CDate(lvItem.SubItems(colhStartDate.Index).Text)
                        End If
                    Next
                End If
                If mdtPayPeriodEndDate = CDate(e.Item.SubItems(colhEndDate.Index).Text) Then
                    mdtPayPeriodEndDate = Nothing
                    For Each lvItem As ListViewItem In lvPeriod.CheckedItems
                        If mdtPayPeriodEndDate = Nothing OrElse mdtPayPeriodEndDate < CDate(lvItem.SubItems(colhEndDate.Index).Text) Then
                            mdtPayPeriodEndDate = CDate(lvItem.SubItems(colhEndDate.Index).Text)
                        End If
                    Next
                End If
            End If
            Call FillCombo_Employee()
            Call FillList()
            'Sohail (06 Mar 2012) -- End
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lvPeriod_ItemChecked", mstrModuleName)
        End Try
    End Sub

    'Sohail (09 Oct 2010) -- Start
    'Sohail (17 Sep 2019) -- Start
    'NMB Enhancement # TC006: system should be able to bind earning/deduction (benefit) with specific allocation and automatically assign to employee as per his/her allocation.
    'Private Sub lvEmployeeList_ItemChecked(ByVal sender As Object, ByVal e As System.Windows.Forms.ItemCheckedEventArgs)

    '    Try
    '        'Sohail (03 Nov 2010) -- Start
    '        'If lvEmployeeList.CheckedItems.Count = lvEmployeeList.Items.Count Then
    '        '    chkSelectAllEmployee.Checked = True
    '        'ElseIf lvEmployeeList.CheckedItems.Count = 0 Then
    '        '    chkSelectAllEmployee.Checked = False
    '        'End If
    '        If lvEmployeeList.CheckedItems.Count <= 0 Then
    '            RemoveHandler objchkSelectAllEmployee.CheckedChanged, AddressOf chkSelectAllEmployee_CheckedChanged
    '            objchkSelectAllEmployee.CheckState = CheckState.Unchecked
    '            AddHandler objchkSelectAllEmployee.CheckedChanged, AddressOf chkSelectAllEmployee_CheckedChanged
    '        ElseIf lvEmployeeList.CheckedItems.Count < lvEmployeeList.Items.Count Then
    '            RemoveHandler objchkSelectAllEmployee.CheckedChanged, AddressOf chkSelectAllEmployee_CheckedChanged
    '            objchkSelectAllEmployee.CheckState = CheckState.Indeterminate
    '            AddHandler objchkSelectAllEmployee.CheckedChanged, AddressOf chkSelectAllEmployee_CheckedChanged
    '        ElseIf lvEmployeeList.CheckedItems.Count = lvEmployeeList.Items.Count Then
    '            RemoveHandler objchkSelectAllEmployee.CheckedChanged, AddressOf chkSelectAllEmployee_CheckedChanged
    '            objchkSelectAllEmployee.CheckState = CheckState.Checked
    '            AddHandler objchkSelectAllEmployee.CheckedChanged, AddressOf chkSelectAllEmployee_CheckedChanged
    '        End If
    '        'Sohail (03 Nov 2010) -- End
    '    Catch ex As Exception
    '        DisplayError.Show("-1", ex.Message, "lvEmployeeList_ItemChecked", mstrModuleName)
    '    End Try
    'End Sub
    'Sohail (17 Sep 2019) -- End
    'Sohail (09 Oct 2010) -- End

#End Region

    'Sohail (17 Sep 2019) -- Start
    'NMB Enhancement # TC006: system should be able to bind earning/deduction (benefit) with specific allocation and automatically assign to employee as per his/her allocation.
#Region " GridView Events "

    Private Sub dgEmployee_CurrentCellDirtyStateChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles dgEmployee.CurrentCellDirtyStateChanged
        Try
            If dgEmployee.IsCurrentCellDirty Then
                dgEmployee.CommitEdit(DataGridViewDataErrorContexts.Commit)
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgEmployee_CurrentCellDirtyStateChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub dgEmployee_CellValueChanged(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgEmployee.CellValueChanged
        Try
            If e.RowIndex < 0 Then Exit Sub

            If e.ColumnIndex = objdgcolhCheck.Index Then
                SetCheckBoxValue()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgEmployee_CellValueChanged", mstrModuleName)
        End Try
    End Sub

#End Region
    'Sohail (17 Sep 2019) -- End

#Region " CheckBox's Events "
    Private Sub chkSelectAll_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objchkSelectAll.CheckedChanged
        Try
            Call CheckAllPeriod(objchkSelectAll.Checked)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "chkSelectAll_CheckedChanged", mstrModuleName)
        End Try
    End Sub

    'Sohail (09 Oct 2010) -- Start
    Private Sub chkSelectAllEmployee_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objchkSelectAllEmployee.CheckedChanged
        Try
            Call CheckAllEmployee(objchkSelectAllEmployee.Checked)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "chkSelectAllEmployee_CheckedChanged", mstrModuleName)
        End Try
    End Sub
    'Sohail (09 Oct 2010) -- End
#End Region
    'Sohail (11 Sep 2010) -- End

    'Sohail (09 Oct 2010) -- Start
#Region " Other Control's Events "
    Private Sub objbtnSearchEmployee_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchEmployee.Click
        Dim objfrm As New frmCommonSearch
        'Sohail (06 Mar 2012) -- Start
        'TRA - ENHANCEMENT
        'Dim objEmployee As New clsEmployee_Master
        'Dim dsList As DataSet
        'Sohail (06 Mar 2012) -- End
        Try
            ''Anjan (02 Sep 2011)-Start
            'Issue : Including Language Settings.
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If
            'Anjan (02 Sep 2011)-End 

            'Sohail (06 Jan 2012) -- Start
            'TRA - ENHANCEMENT
            'dsList = objEmployee.GetEmployeeList("EmployeeList")
            'dsList = objEmployee.GetEmployeeList("EmployeeList", True, , , , , , , , , , , , , ConfigParameter._Object._CurrentDateAndTime.Date, ConfigParameter._Object._CurrentDateAndTime.Date) 'Sohail (06 Mar 2012)
            'Sohail (06 Jan 2012) -- End
            With cboEmployee
                'Sohail (06 Mar 2012) -- Start
                'TRA - ENHANCEMENT
                'objfrm.DataSource = dsList.Tables("EmployeeList")
                objfrm.DataSource = CType(cboEmployee.DataSource, DataTable)
                'Sohail (06 Mar 2012) -- End
                objfrm.ValueMember = .ValueMember
                objfrm.DisplayMember = .DisplayMember
                objfrm.CodeMember = "employeecode"
                If objfrm.DisplayDialog Then
                    .SelectedValue = objfrm.SelectedValue
                End If
                .Focus()
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchEmployee_Click", mstrModuleName)
        Finally
            objfrm = Nothing
            'objEmployee = Nothing 'Sohail (06 Mar 2012)
        End Try
    End Sub

    Private Sub objbtnSearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearch.Click
        Try
            If objchkSelectAllEmployee.Checked = True Then objchkSelectAllEmployee.Checked = False 'Sohail (09 Oct 2010)
            Call FillList()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearch_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnReset.Click
        Try
            If cboEmployee.Items.Count > 0 Then cboEmployee.SelectedValue = 0
            'Sohail (17 Sep 2019) -- Start
            'NMB Enhancement # TC006: system should be able to bind earning/deduction (benefit) with specific allocation and automatically assign to employee as per his/her allocation.
            'If cboDepartment.Items.Count > 0 Then cboDepartment.SelectedValue = 0
            'If cboGrade.Items.Count > 0 Then cboGrade.SelectedValue = 0
            'If cboSections.Items.Count > 0 Then cboSections.SelectedValue = 0
            'If cboClass.Items.Count > 0 Then cboClass.SelectedValue = 0
            'If cboCostCenter.Items.Count > 0 Then cboCostCenter.SelectedValue = 0
            'If cboJob.Items.Count > 0 Then cboJob.SelectedValue = 0
            'If cboPayPoint.Items.Count > 0 Then cboPayPoint.SelectedValue = 0
            'If cboUnit.Items.Count > 0 Then cboUnit.SelectedValue = 0

            'lvEmployeeList.Items.Clear()
            'If objchkSelectAllEmployee.Checked = True Then objchkSelectAllEmployee.Checked = False 'Sohail (09 Oct 2010)
            mstrAdvanceFilter = ""
            dgEmployee.DataSource = Nothing
            If objchkSelectAllEmployee.Checked = True Then objchkSelectAllEmployee.Checked = False
            'Sohail (17 Sep 2019) -- End
            Call FillList()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnReset_Click", mstrModuleName)
        End Try
    End Sub

    'Sohail (25 May 2012) -- Start
    'TRA - ENHANCEMENT
    'Sohail (17 Sep 2019) -- Start
    'NMB Enhancement # TC006: system should be able to bind earning/deduction (benefit) with specific allocation and automatically assign to employee as per his/her allocation.
    'Private Sub txtSearchEmp_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
    '    Try
    '        If lvEmployeeList.Items.Count <= 0 Then Exit Sub
    '        lvEmployeeList.SelectedIndices.Clear()
    '        Dim lvFoundItem As ListViewItem = lvEmployeeList.FindItemWithText(txtSearchEmp.Text, True, 0, True)
    '        If lvFoundItem IsNot Nothing Then
    '            lvEmployeeList.TopItem = lvFoundItem
    '            lvFoundItem.Selected = True
    '        End If
    '    Catch ex As Exception
    '        DisplayError.Show("-1", ex.Message, "txtSearchEmp_TextChanged", mstrModuleName)
    '    End Try
    'End Sub
    Private Sub txtSearchEmp_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtSearchEmp.GotFocus
        Try
            With txtSearchEmp
                .ForeColor = Color.Black
                .Font = New Font(Me.Font.FontFamily, Me.Font.Size, FontStyle.Regular)

                If .Text = mstrSearchEmpText Then
                    .Clear()
                End If
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "txtSearchEmp_GotFocus", mstrModuleName)
        End Try
    End Sub

    Private Sub txtSearchEmp_Leave(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtSearchEmp.Leave
        Try
            If txtSearchEmp.Text.Trim = "" Then
                Call SetDefaultSearchEmpText()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "txtSearchEmp_Leave", mstrModuleName)
        End Try
    End Sub

    Private Sub txtSearchEmp_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtSearchEmp.TextChanged
        Try
            If txtSearchEmp.Text.Trim = mstrSearchEmpText Then Exit Sub
            If dvEmployee IsNot Nothing Then
                dvEmployee.RowFilter = "employeecode LIKE '%" & txtSearchEmp.Text.Replace("'", "''") & "%'  OR employeename LIKE '%" & txtSearchEmp.Text.Replace("'", "''") & "%'"
                dgEmployee.Refresh()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "txtSearchEmp_TextChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub lnkAllocation_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lnkAllocation.LinkClicked
        Try
            Dim frm As New frmAdvanceSearch
            frm.ShowDialog()
            mstrAdvanceFilter = frm._GetFilterString
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lnkAllocation_LinkClicked", mstrModuleName)
        End Try
    End Sub
    'Sohail (17 Sep 2019) -- End
    'Sohail (25 May 2012) -- End
#End Region

    'Sohail (09 Oct 2010) -- End


	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Private Sub OtherSettings()
		Try
			Me.SuspendLayout()
			
			Call SetLanguage()
			
			Me.gbEmpExemptionInfo.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbEmpExemptionInfo.ForeColor = GUI._eZeeContainerHeaderForeColor 

			Me.gbFilterCriteria.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbFilterCriteria.ForeColor = GUI._eZeeContainerHeaderForeColor 

			Me.gbEmployeeList.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbEmployeeList.ForeColor = GUI._eZeeContainerHeaderForeColor 


			
			Me.EZeeHeader1.GradientColor1 = GUI._HeaderBackColor1 
			Me.EZeeHeader1.GradientColor2 = GUI._HeaderBackColor2 
			Me.EZeeHeader1.BorderColor = GUI._HeaderBorderColor 
			Me.EZeeHeader1.HeaderTextForeColor = GUI._HeaderTitleFontColor 
			Me.EZeeHeader1.DescriptionForeColor = GUI._HeaderMessageFontColor 


			Me.btnClose.GradientBackColor = GUI._ButttonBackColor 
			Me.btnClose.GradientForeColor = GUI._ButttonFontColor

			Me.btnSave.GradientBackColor = GUI._ButttonBackColor 
			Me.btnSave.GradientForeColor = GUI._ButttonFontColor


			Me.ResumeLayout()
		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "OtherSettings", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetLanguage()
		Try
			Me.Text = Language._Object.getCaption(Me.Name, Me.Text)
			
			Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
			Me.btnSave.Text = Language._Object.getCaption(Me.btnSave.Name, Me.btnSave.Text)
			Me.gbEmpExemptionInfo.Text = Language._Object.getCaption(Me.gbEmpExemptionInfo.Name, Me.gbEmpExemptionInfo.Text)
			Me.lblYear.Text = Language._Object.getCaption(Me.lblYear.Name, Me.lblYear.Text)
			Me.lblTransactionhead.Text = Language._Object.getCaption(Me.lblTransactionhead.Name, Me.lblTransactionhead.Text)
			Me.lblEmployee.Text = Language._Object.getCaption(Me.lblEmployee.Name, Me.lblEmployee.Text)
			Me.EZeeHeader1.Title = Language._Object.getCaption(Me.EZeeHeader1.Name & "_Title" , Me.EZeeHeader1.Title)
			Me.EZeeHeader1.Message = Language._Object.getCaption(Me.EZeeHeader1.Name & "_Message" , Me.EZeeHeader1.Message)
			Me.colhCheck.Text = Language._Object.getCaption(CStr(Me.colhCheck.Tag), Me.colhCheck.Text)
			Me.colhPeriod.Text = Language._Object.getCaption(CStr(Me.colhPeriod.Tag), Me.colhPeriod.Text)
			Me.colhStartDate.Text = Language._Object.getCaption(CStr(Me.colhStartDate.Tag), Me.colhStartDate.Text)
			Me.colhEndDate.Text = Language._Object.getCaption(CStr(Me.colhEndDate.Tag), Me.colhEndDate.Text)
			Me.gbFilterCriteria.Text = Language._Object.getCaption(Me.gbFilterCriteria.Name, Me.gbFilterCriteria.Text)
			Me.Label1.Text = Language._Object.getCaption(Me.Label1.Name, Me.Label1.Text)
			Me.lblTypeOf.Text = Language._Object.getCaption(Me.lblTypeOf.Name, Me.lblTypeOf.Text)
			Me.lblTrnHeadType.Text = Language._Object.getCaption(Me.lblTrnHeadType.Name, Me.lblTrnHeadType.Text)
			Me.lblSearchEmp.Text = Language._Object.getCaption(Me.lblSearchEmp.Name, Me.lblSearchEmp.Text)
			Me.gbEmployeeList.Text = Language._Object.getCaption(Me.gbEmployeeList.Name, Me.gbEmployeeList.Text)
			Me.dgColhEmpCode.HeaderText = Language._Object.getCaption(Me.dgColhEmpCode.Name, Me.dgColhEmpCode.HeaderText)
			Me.dgColhEmployee.HeaderText = Language._Object.getCaption(Me.dgColhEmployee.Name, Me.dgColhEmployee.HeaderText)
			Me.lnkAllocation.Text = Language._Object.getCaption(Me.lnkAllocation.Name, Me.lnkAllocation.Text)

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetLanguage", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "Year is compulsory information.Please Select Year.")
			Language.setMessage(mstrModuleName, 2, "Period is compulsory information.Please Select Period.")
			Language.setMessage(mstrModuleName, 3, "Transaction Head is compulsory information.Please Select Transaction Head.")
			Language.setMessage(mstrModuleName, 4, "Are you sure you want to Exempt this Transaction Head for all selected Period?")
			Language.setMessage(mstrModuleName, 5, "Please select atleast one Employee from list.")
			Language.setMessage(mstrModuleName, 6, "Sorry! You can not Exempt this Transaction Head. This Transaction Head is used in the formula of")
                        Language.setMessage(mstrModuleName, 7, "Sorry, Process Payroll is already done for last date of one of the selected Period. Please Void Process Payroll first for selected employee to exempt head.")
			Language.setMessage(mstrModuleName, 8, "Do you want to void Payroll?")
			Language.setMessage(mstrModuleName, 9, "for employee :")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class
