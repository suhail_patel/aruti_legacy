﻿Option Strict On

#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data

#End Region

Public Class frmTranHeadMapping
    Private ReadOnly mstrModuleName As String = "frmTranHeadMapping"
    Private dsData As New DataSet
    Private mblnCancel As Boolean = True
    Private dtTable As DataTable
    Private dsDataList As New DataSet
    Dim objTranHead As clsTransactionHead

#Region " Properties "

    Public ReadOnly Property _DataTable() As DataTable
        Get
            Return dtTable
        End Get
    End Property

#End Region

#Region " Display Dialog "

    Public Function displayDialog(ByVal dsList As DataSet) As Boolean
        Try
            dsData = dsList
            Me.ShowDialog()
            Return Not mblnCancel
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "", mstrModuleName)
        End Try
    End Function

#End Region

#Region " Form's Events "

    Private Sub frmTranHeadMapping_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        objTranHead = New clsTransactionHead
        Try
            Call Set_Logo(Me, gApplicationType)

            'Anjan (02 Sep 2011)-Start
            'Issue : Including Language Settings.
            Language.setLanguage(Me.Name)
            Call OtherSettings()
            'Anjan (02 Sep 2011)-End 

            Call SetColor() 'Sohail (26 May 2011)
            cboTranHeadCode.Items.Clear()
            cboTranHeadName.Items.Clear()
            Call SetData()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmTranHeadMapping_Load", mstrModuleName)
        End Try
    End Sub
    'Anjan (02 Sep 2011)-Start
    'Issue : Including Language Settings.
    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub
    'Anjan (02 Sep 2011)-End 
#End Region

#Region " Private Methods "

    'Sohail (26 May 2011) -- Start
    Private Sub SetColor()
        Try
            cboTranHeadCode.BackColor = GUI.ColorComp
            cboTranHeadName.BackColor = GUI.ColorComp
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetColor", mstrModuleName)
        End Try
    End Sub
    'Sohail (26 May 2011) -- End

    Private Sub SetData()
        Try
            dtTable = New DataTable("TranHead")
            For Each dtColumns As DataColumn In dsData.Tables(0).Columns
                cboTranHeadCode.Items.Add(dtColumns.ColumnName)
                cboTranHeadName.Items.Add(dtColumns.ColumnName)
            Next
            dtTable.Columns.Add("tranheadunkid", System.Type.GetType("System.Int32")).DefaultValue = -1
            dtTable.Columns.Add("trnheadcode", System.Type.GetType("System.String")).DefaultValue = ""
            dtTable.Columns.Add("trnheadname", System.Type.GetType("System.String")).DefaultValue = ""

            dtTable.Columns.Add("trnheadtype_id", System.Type.GetType("System.Int32")).DefaultValue = -1
            dtTable.Columns.Add("trnheadtype_name", System.Type.GetType("System.String")).DefaultValue = ""

            dtTable.Columns.Add("typeof_id", System.Type.GetType("System.Int32")).DefaultValue = -1
            dtTable.Columns.Add("typeof_name", System.Type.GetType("System.String")).DefaultValue = ""

            dtTable.Columns.Add("calctype_id", System.Type.GetType("System.Int32")).DefaultValue = -1
            dtTable.Columns.Add("calctype_name", System.Type.GetType("System.String")).DefaultValue = ""

            dtTable.Columns.Add("computeon_id", System.Type.GetType("System.Int32")).DefaultValue = -1
            dtTable.Columns.Add("formula", System.Type.GetType("System.String")).DefaultValue = ""
            dtTable.Columns.Add("formulaid", System.Type.GetType("System.String")).DefaultValue = ""
            dtTable.Columns.Add("isappearonpayslip", System.Type.GetType("System.Boolean")).DefaultValue = False
            dtTable.Columns.Add("isrecurrent", System.Type.GetType("System.Boolean")).DefaultValue = False
            dtTable.Columns.Add("istaxable", System.Type.GetType("System.Boolean")).DefaultValue = False
            dtTable.Columns.Add("istaxrelief", System.Type.GetType("System.Boolean")).DefaultValue = False
            dtTable.Columns.Add("userunkid", System.Type.GetType("System.Int32")).DefaultValue = User._Object._Userunkid
            dtTable.Columns.Add("isvoid", System.Type.GetType("System.Boolean")).DefaultValue = False
            dtTable.Columns.Add("voiddatetime", System.Type.GetType("System.DateTime"))
            dtTable.Columns.Add("voidreason", System.Type.GetType("System.String")).DefaultValue = ""
            dtTable.Columns.Add("trnheadname1", System.Type.GetType("System.String")).DefaultValue = ""
            dtTable.Columns.Add("trnheadname2", System.Type.GetType("System.String")).DefaultValue = ""
            dtTable.Columns.Add("IsChecked", System.Type.GetType("System.Boolean")).DefaultValue = False
            dtTable.Columns.Add("IsChange", System.Type.GetType("System.Boolean")).DefaultValue = False

        Catch ex As Exception
            mblnCancel = False
            DisplayError.Show("-1", ex.Message, "SetData", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Buttons Events "

    Private Sub btnOk_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnOk.Click
        Try
            'Sohail (26 May 2011) -- Start
            If CInt(cboTranHeadCode.SelectedIndex) < 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please select field for Transaction Head Code."), enMsgBoxStyle.Information)
                cboTranHeadCode.Focus()
                Exit Try
            ElseIf CInt(cboTranHeadName.SelectedIndex) < 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Please select field for Transaction Head Name."), enMsgBoxStyle.Information)
                cboTranHeadName.Focus()
                Exit Try
            End If
            'Sohail (26 May 2011) -- End

            For i As Integer = 0 To dsData.Tables(0).Rows.Count - 1
                Dim dRow As DataRow
                dRow = dtTable.NewRow

                dRow.Item("trnheadcode") = dsData.Tables(0).Rows(i)(cboTranHeadCode.Text)
                dRow.Item("trnheadname") = dsData.Tables(0).Rows(i)(cboTranHeadName.Text)
                dRow.Item("userunkid") = User._Object._Userunkid
                dtTable.Rows.Add(dRow)
            Next
            mblnCancel = False
            objTranHead = Nothing
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnOk_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnCancel_Click", mstrModuleName)
        End Try
    End Sub

#End Region
    
    'Sohail (26 May 2011) -- Start
#Region " Message "
    '1, "Please select field for Transaction Head Code."
    '2, "Please select field for Transaction Head Name."
#End Region
    'Sohail (26 May 2011) -- End

	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Private Sub OtherSettings()
		Try
			Me.SuspendLayout()
			
			Call SetLanguage()
			
			Me.gbFieldMapping.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbFieldMapping.ForeColor = GUI._eZeeContainerHeaderForeColor 


			Me.btnCancel.GradientBackColor = GUI._ButttonBackColor 
			Me.btnCancel.GradientForeColor = GUI._ButttonFontColor

			Me.btnOk.GradientBackColor = GUI._ButttonBackColor 
			Me.btnOk.GradientForeColor = GUI._ButttonFontColor


			Me.ResumeLayout()
		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "OtherSettings", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetLanguage()
		Try
			Me.Text = Language._Object.getCaption(Me.Name, Me.Text)
			
			Me.gbFieldMapping.Text = Language._Object.getCaption(Me.gbFieldMapping.Name, Me.gbFieldMapping.Text)
			Me.lblTranHeadName.Text = Language._Object.getCaption(Me.lblTranHeadName.Name, Me.lblTranHeadName.Text)
			Me.lblTranCode.Text = Language._Object.getCaption(Me.lblTranCode.Name, Me.lblTranCode.Text)
			Me.btnCancel.Text = Language._Object.getCaption(Me.btnCancel.Name, Me.btnCancel.Text)
			Me.btnOk.Text = Language._Object.getCaption(Me.btnOk.Name, Me.btnOk.Text)

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetLanguage", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "Please select field for Transaction Head Code.")
			Language.setMessage(mstrModuleName, 2, "Please select field for Transaction Head Name.")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class