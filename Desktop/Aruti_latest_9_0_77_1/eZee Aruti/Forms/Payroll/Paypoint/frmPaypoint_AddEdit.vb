﻿Option Strict On

Imports eZeeCommonLib
Imports Aruti.Data

'Last Message Index = 2


Public Class frmPaypoint_AddEdit


#Region "Private Variable"

    Private ReadOnly mstrModuleName As String = "frmPaypoint_AddEdit"
    Private mblnCancel As Boolean = True
    Private objPaypointMaster As clspaypoint_master
    Private menAction As enAction = enAction.ADD_ONE
    Private mintPaypointMasterUnkid As Integer = -1

#End Region

#Region " Display Dialog "

    Public Function displayDialog(ByRef intUnkId As Integer, ByVal eAction As enAction) As Boolean
        Try
            mintPaypointMasterUnkid = intUnkId
            menAction = eAction

            Me.ShowDialog()

            intUnkId = mintPaypointMasterUnkid

            Return Not mblnCancel
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "displayDialog", mstrModuleName)
        End Try
    End Function

#End Region

#Region "Form's Event "

    Private Sub frmPaypoint_AddEdit_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        objPaypointMaster = New clspaypoint_master
        Try
            Call Set_Logo(Me, gApplicationType)

            'Anjan (02 Sep 2011)-Start
            'Issue : Including Language Settings.
            Language.setLanguage(Me.Name)
            Call OtherSettings()
            'Anjan (02 Sep 2011)-End

            setColor()
            If menAction = enAction.EDIT_ONE Then
                objPaypointMaster._Paypointunkid = mintPaypointMasterUnkid
            End If
            GetValue()
            txtCode.Select()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmPaypoint_AddEdit_Load", mstrModuleName)
        End Try
    End Sub

    Private Sub frmPaypoint_AddEdit_KeyPress(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles MyBase.KeyPress
        Try
            If Asc(e.KeyChar) = 13 Then
                SendKeys.Send("{TAB}")
                e.Handled = True
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmPaypoint_AddEdit_KeyPress", mstrModuleName)
        End Try
    End Sub

    Private Sub frmPaypoint_AddEdit_KeyDown(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles MyBase.KeyDown
        Try
            If e.Control = True And e.KeyCode = Keys.S Then
                btnSave_Click(sender, e)
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmPaypoint_AddEdit_KeyDown", mstrModuleName)
        End Try
    End Sub

    Private Sub frmPaypoint_AddEdit_FormClosed(ByVal sender As System.Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles MyBase.FormClosed
        objPaypointMaster = Nothing
    End Sub
    'Anjan (02 Sep 2011)-Start
    'Issue : Including Language Settings.
    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clspaypoint_master.SetMessages()
            objfrm._Other_ModuleNames = "clspaypoint_master"
            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub
    'Anjan (02 Sep 2011)-End 

#End Region

#Region "Button's Event"

    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Dim blnFlag As Boolean = False
        Try
            If Trim(txtCode.Text) = "" Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Pay Point Code cannot be blank. Pay Point Code is required information."), enMsgBoxStyle.Information)
                txtCode.Focus()
                Exit Sub
            ElseIf Trim(txtName.Text) = "" Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Pay Point Name cannot be blank. Pay Point Name is required information."), enMsgBoxStyle.Information)
                txtName.Focus()
                Exit Sub
            End If

            Call SetValue()

            'S.SANDEEP [28-May-2018] -- START
            'ISSUE/ENHANCEMENT : {Audit Trails} 
            objPaypointMaster._FormName = mstrModuleName
            objPaypointMaster._LoginEmployeeunkid = 0
            objPaypointMaster._ClientIP = getIP()
            objPaypointMaster._HostName = getHostName()
            objPaypointMaster._FromWeb = False
            objPaypointMaster._AuditUserId = User._Object._Userunkid
objPaypointMaster._CompanyUnkid = Company._Object._Companyunkid
            objPaypointMaster._AuditDate = ConfigParameter._Object._CurrentDateAndTime
            'S.SANDEEP [28-May-2018] -- END

            If menAction = enAction.EDIT_ONE Then
                blnFlag = objPaypointMaster.Update()
            Else
                blnFlag = objPaypointMaster.Insert()
            End If

            If blnFlag = False And objPaypointMaster._Message <> "" Then
                eZeeMsgBox.Show(objPaypointMaster._Message, enMsgBoxStyle.Information)
            End If

            If blnFlag Then
                mblnCancel = False
                If menAction = enAction.ADD_CONTINUE Then
                    objPaypointMaster = Nothing
                    objPaypointMaster = New clspaypoint_master
                    Call GetValue()
                    txtCode.Select()
                Else
                    mintPaypointMasterUnkid = objPaypointMaster._Paypointunkid
                    Me.Close()
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnSave_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnOtherLanguage_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnOtherLanguage.Click
        Try
            Dim objFrmLangPopup As New NameLanguagePopup_Form

            'Anjan (02 Sep 2011)-Start
            'Issue : Including Language Settings.
            If User._Object._Isrighttoleft = True Then
                objFrmLangPopup.RightToLeft = Windows.Forms.RightToLeft.Yes
                objFrmLangPopup.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objFrmLangPopup)
            End If
            'Anjan (02 Sep 2011)-End 

            objFrmLangPopup.displayDialog(txtName.Text, objPaypointMaster._Paypointname1, objPaypointMaster._Paypointname2)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnOtherLanguage_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Me.Close()
    End Sub


#End Region

#Region "Private Methods"

    Private Sub setColor()
        Try
            txtAlias.BackColor = GUI.ColorOptional
            txtCode.BackColor = GUI.ColorOptional
            txtName.BackColor = GUI.ColorComp
            txtDescription.BackColor = GUI.ColorOptional
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "setColor", mstrModuleName)
        End Try
    End Sub

    Private Sub GetValue()
        Try
            txtAlias.Text = objPaypointMaster._Paypointalias
            txtCode.Text = objPaypointMaster._Paypointcode
            txtName.Text = objPaypointMaster._Paypointname
            txtDescription.Text = objPaypointMaster._Description
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetValue", mstrModuleName)
        End Try
    End Sub

    Private Sub SetValue()
        Try
            objPaypointMaster._Paypointalias = txtAlias.Text.Trim
            objPaypointMaster._Paypointcode = txtCode.Text.Trim
            objPaypointMaster._Paypointname = txtName.Text.Trim
            objPaypointMaster._Description = txtDescription.Text.Trim
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetValue", mstrModuleName)
        End Try
    End Sub

#End Region

   
	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Private Sub OtherSettings()
		Try
			Me.SuspendLayout()
			
			Call SetLanguage()
			
			Me.gbbankAccountTypeInfo.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbbankAccountTypeInfo.ForeColor = GUI._eZeeContainerHeaderForeColor 


			Me.btnClose.GradientBackColor = GUI._ButttonBackColor 
			Me.btnClose.GradientForeColor = GUI._ButttonFontColor

			Me.btnSave.GradientBackColor = GUI._ButttonBackColor 
			Me.btnSave.GradientForeColor = GUI._ButttonFontColor


			Me.ResumeLayout()
		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "OtherSettings", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetLanguage()
		Try
			Me.Text = Language._Object.getCaption(Me.Name, Me.Text)
			
			Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
			Me.btnSave.Text = Language._Object.getCaption(Me.btnSave.Name, Me.btnSave.Text)
			Me.gbbankAccountTypeInfo.Text = Language._Object.getCaption(Me.gbbankAccountTypeInfo.Name, Me.gbbankAccountTypeInfo.Text)
			Me.lblDescription.Text = Language._Object.getCaption(Me.lblDescription.Name, Me.lblDescription.Text)
			Me.lblName.Text = Language._Object.getCaption(Me.lblName.Name, Me.lblName.Text)
			Me.lblAlias.Text = Language._Object.getCaption(Me.lblAlias.Name, Me.lblAlias.Text)
			Me.lblCode.Text = Language._Object.getCaption(Me.lblCode.Name, Me.lblCode.Text)

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetLanguage", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "Pay Point Code cannot be blank. Pay Point Code is required information.")
			Language.setMessage(mstrModuleName, 2, "Pay Point Name cannot be blank. Pay Point Name is required information.")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class