﻿Option Strict On

Imports eZeeCommonLib
Imports Aruti.Data

'Last Message Index = 3

Public Class frmPaypointList


#Region "Private Variable"

    Private objPayPointmaster As clspaypoint_master
    Private ReadOnly mstrModuleName As String = "frmPaypointList"

#End Region

#Region "Form's Event"

    Private Sub frmPaypointList_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        objPayPointmaster = New clspaypoint_master
        Try
            Call Set_Logo(Me, gApplicationType)
            Language.setLanguage(Me.Name)

            'Anjan (02 Sep 2011)-Start
            'Issue : Including Language Settings.
            Call OtherSettings()
            'Anjan (02 Sep 2011)-End 

            Call SetVisibility()
            fillList()
            If lvPaypointcodes.Items.Count > 0 Then lvPaypointcodes.Items(0).Selected = True
            lvPaypointcodes.Select()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmPaypointList_Load", mstrModuleName)
        End Try
    End Sub

    Private Sub frmPaypointList_KeyUp(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles MyBase.KeyUp
        Try
            If e.KeyCode = Keys.Delete And lvPaypointcodes.Focused = True Then
                'Sohail (24 Jun 2011) -- Start
                'Issue : Delete event fired even if there is no delete privilege
                'btnDelete_Click(sender, e)
                Call btnDelete.PerformClick()
                'Sohail (24 Jun 2011) -- End
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmPaypointList_KeyUp", mstrModuleName)
        End Try
    End Sub

    Private Sub frmPaypointList_FormClosed(ByVal sender As System.Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles MyBase.FormClosed
        objPayPointmaster = Nothing
    End Sub
    'Anjan (02 Sep 2011)-Start
    'Issue : Including Language Settings.
    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clspaypoint_master.SetMessages()
            objfrm._Other_ModuleNames = "clspaypoint_master"
            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub
    'Anjan (02 Sep 2011)-End 
#End Region

#Region "Button's Event"

    Private Sub btnNew_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNew.Click
        Try
            Dim objfrmpaypoint_AddEdit As New frmPaypoint_AddEdit
            'Anjan (02 Sep 2011)-Start
            'Issue : Including Language Settings.
            If User._Object._Isrighttoleft = True Then
                objfrmpaypoint_AddEdit.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrmpaypoint_AddEdit.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrmpaypoint_AddEdit)
            End If
            'Anjan (02 Sep 2011)-End 

            If objfrmpaypoint_AddEdit.displayDialog(-1, enAction.ADD_CONTINUE) Then
                fillList()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnNew_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnEdit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEdit.Click, lvPaypointcodes.DoubleClick 'Sohail (19 Nov 2010)
        'Sohail (24 Jun 2011) -- Start
        If User._Object.Privilege._EditPayPoint = False Then Exit Sub
        'Sohail (24 Jun 2011) -- End
        Try
            If lvPaypointcodes.SelectedItems.Count < 1 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please select Pay Point from the list to perform further operation."), enMsgBoxStyle.Information) '?1
                lvPaypointcodes.Select()
                Exit Sub
            End If
            Dim objfrmPaypoint_AddEdit As New frmPaypoint_AddEdit

            'Anjan (02 Sep 2011)-Start
            'Issue : Including Language Settings.
            If User._Object._Isrighttoleft = True Then
                objfrmPaypoint_AddEdit.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrmPaypoint_AddEdit.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrmPaypoint_AddEdit)
            End If
            'Anjan (02 Sep 2011)-End
            Try
                Dim intSelectedIndex As Integer
                intSelectedIndex = lvPaypointcodes.SelectedItems(0).Index
                If objfrmPaypoint_AddEdit.displayDialog(CInt(lvPaypointcodes.SelectedItems(0).Tag), enAction.EDIT_ONE) Then
                    Call fillList()
                End If
                objfrmPaypoint_AddEdit = Nothing

                lvPaypointcodes.Items(intSelectedIndex).Selected = True
                lvPaypointcodes.EnsureVisible(intSelectedIndex)
                lvPaypointcodes.Select()
            Catch ex As Exception
                Call DisplayError.Show("-1", ex.Message, "btnEdit_Click", mstrModuleName)
            Finally
                If objfrmPaypoint_AddEdit IsNot Nothing Then objfrmPaypoint_AddEdit.Dispose()
            End Try
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnEdit_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDelete.Click
        If lvPaypointcodes.SelectedItems.Count < 1 Then
            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please select Pay Point from the list to perform further operation."), enMsgBoxStyle.Information) '?1
            lvPaypointcodes.Select()
            Exit Sub
        End If
        'If objPayPointmaster.isUsed(CInt(lvState.SelectedItems(0).Tag)) Then
        '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Sorry, You cannot delete this Pay Point. Reason: This Pay Point is in use."), enMsgBoxStyle.Information) '?2
        '    lvState.Select()
        '    Exit Sub
        'End If
        'Sohail (19 Nov 2010) -- Start
        If objPayPointmaster.isUsed(CInt(lvPaypointcodes.SelectedItems(0).Tag)) Then
            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Sorry, You cannot delete this Pay Point. Reason: This Pay Point is in use."), enMsgBoxStyle.Information) '?2
            lvPaypointcodes.Select()
            Exit Sub
        End If
        'Sohail (19 Nov 2010) -- End
        Try
            Dim intSelectedIndex As Integer
            intSelectedIndex = lvPaypointcodes.SelectedItems(0).Index

            If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Are you sure you want to delete this Pay Point?"), CType(enMsgBoxStyle.Question + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.Yes Then

                'S.SANDEEP [28-May-2018] -- START
                'ISSUE/ENHANCEMENT : {Audit Trails} 
                objPayPointmaster._FormName = mstrModuleName
                objPayPointmaster._LoginEmployeeunkid = 0
                objPayPointmaster._ClientIP = getIP()
                objPayPointmaster._HostName = getHostName()
                objPayPointmaster._FromWeb = False
                objPayPointmaster._AuditUserId = User._Object._Userunkid
objPayPointmaster._CompanyUnkid = Company._Object._Companyunkid
                objPayPointmaster._AuditDate = ConfigParameter._Object._CurrentDateAndTime
                'S.SANDEEP [28-May-2018] -- END

                objPayPointmaster.Delete(CInt(lvPaypointcodes.SelectedItems(0).Tag))
                lvPaypointcodes.SelectedItems(0).Remove()

                If lvPaypointcodes.Items.Count <= 0 Then
                    Exit Try
                End If

                If lvPaypointcodes.Items.Count = intSelectedIndex Then
                    intSelectedIndex = lvPaypointcodes.Items.Count - 1
                    lvPaypointcodes.Items(intSelectedIndex).Selected = True
                    lvPaypointcodes.EnsureVisible(intSelectedIndex)
                ElseIf lvPaypointcodes.Items.Count <> 0 Then
                    lvPaypointcodes.Items(intSelectedIndex).Selected = True
                    lvPaypointcodes.EnsureVisible(intSelectedIndex)
                End If
            End If
            lvPaypointcodes.Select()
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "btnDelete_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Me.Close()
    End Sub

#End Region

#Region " Private Methods "

    Private Sub fillList()
        Dim dsPayPointList As New DataSet
        Try

            If User._Object.Privilege._AllowToViewPayPointList = True Then   'Pinkal (09-Jul-2012) -- Start


                'S.SANDEEP [28-May-2018] -- START
                'ISSUE/ENHANCEMENT : {Audit Trails} 
                objPayPointmaster._FormName = mstrModuleName
                objPayPointmaster._LoginEmployeeunkid = 0
                objPayPointmaster._ClientIP = getIP()
                objPayPointmaster._HostName = getHostName()
                objPayPointmaster._FromWeb = False
                objPayPointmaster._AuditUserId = User._Object._Userunkid
objPayPointmaster._CompanyUnkid = Company._Object._Companyunkid
                objPayPointmaster._AuditDate = ConfigParameter._Object._CurrentDateAndTime
                'S.SANDEEP [28-May-2018] -- END

                dsPayPointList = objPayPointmaster.GetList("List")

                Dim lvItem As ListViewItem

                lvPaypointcodes.Items.Clear()
                For Each drRow As DataRow In dsPayPointList.Tables(0).Rows
                    lvItem = New ListViewItem
                    lvItem.Text = drRow("paypointcode").ToString
                    lvItem.Tag = drRow("paypointunkid")
                    lvItem.SubItems.Add(drRow("paypointname").ToString)
                    lvItem.SubItems.Add(drRow("description").ToString)
                    lvPaypointcodes.Items.Add(lvItem)
                Next

                If lvPaypointcodes.Items.Count > 16 Then
                    colhdescription.Width = 400 - 18
                Else
                    colhdescription.Width = 400
                End If

            End If

        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "fillList", mstrModuleName)
        Finally
            dsPayPointList.Dispose()
        End Try
    End Sub

    Private Sub SetVisibility()

        Try
            btnNew.Enabled = User._Object.Privilege._AddPayPoint
            btnEdit.Enabled = User._Object.Privilege._EditPayPoint
            btnDelete.Enabled = User._Object.Privilege._DeletePayPoint

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetVisibility", mstrModuleName)
        End Try

    End Sub

#End Region


    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Private Sub OtherSettings()
        Try
            Me.SuspendLayout()
           
            Call SetLanguage()

          
            Me.eZeeHeader.GradientColor1 = GUI._HeaderBackColor1
            Me.eZeeHeader.GradientColor2 = GUI._HeaderBackColor2
            Me.eZeeHeader.BorderColor = GUI._HeaderBorderColor
            Me.eZeeHeader.HeaderTextForeColor = GUI._HeaderTitleFontColor
            Me.eZeeHeader.DescriptionForeColor = GUI._HeaderMessageFontColor


            Me.btnDelete.GradientBackColor = GUI._ButttonBackColor
            Me.btnDelete.GradientForeColor = GUI._ButttonFontColor

            Me.btnEdit.GradientBackColor = GUI._ButttonBackColor
            Me.btnEdit.GradientForeColor = GUI._ButttonFontColor

            Me.btnNew.GradientBackColor = GUI._ButttonBackColor
            Me.btnNew.GradientForeColor = GUI._ButttonFontColor

            Me.btnClose.GradientBackColor = GUI._ButttonBackColor
            Me.btnClose.GradientForeColor = GUI._ButttonFontColor


            Me.ResumeLayout()
        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "OtherSettings", mstrModuleName)
        End Try
    End Sub


    Private Sub SetLanguage()
        Try
            Me.Text = Language._Object.getCaption(Me.Name, Me.Text)

            Me.colhPaypointcode.Text = Language._Object.getCaption(CStr(Me.colhPaypointcode.Tag), Me.colhPaypointcode.Text)
            Me.colhPaypointname.Text = Language._Object.getCaption(CStr(Me.colhPaypointname.Tag), Me.colhPaypointname.Text)
            Me.colhdescription.Text = Language._Object.getCaption(CStr(Me.colhdescription.Tag), Me.colhdescription.Text)
            Me.btnDelete.Text = Language._Object.getCaption(Me.btnDelete.Name, Me.btnDelete.Text)
            Me.btnEdit.Text = Language._Object.getCaption(Me.btnEdit.Name, Me.btnEdit.Text)
            Me.btnNew.Text = Language._Object.getCaption(Me.btnNew.Name, Me.btnNew.Text)
            Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
            Me.eZeeHeader.Title = Language._Object.getCaption(Me.eZeeHeader.Name & "_Title", Me.eZeeHeader.Title)
            Me.eZeeHeader.Message = Language._Object.getCaption(Me.eZeeHeader.Name & "_Message", Me.eZeeHeader.Message)

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
        End Try
    End Sub


    Private Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Please select Pay Point from the list to perform further operation.")
            Language.setMessage(mstrModuleName, 2, "Sorry, You cannot delete this Pay Point. Reason: This Pay Point is in use.")
            Language.setMessage(mstrModuleName, 3, "Are you sure you want to delete this Pay Point?")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class