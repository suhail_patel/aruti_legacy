﻿Option Strict On

Imports eZeeCommonLib
Imports Aruti.Data
'Last Message Index = 3

Public Class frmVendorList

#Region "Private Variable"

    Private objVendormaster As clsvendor_master
    Private ReadOnly mstrModuleName As String = "frmVendorList"

#End Region

#Region "Form's Event"

    Private Sub frmVendorList_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        objVendormaster = New clsvendor_master
        Try
            Call Set_Logo(Me, gApplicationType)
            Language.setLanguage(Me.Name)
            fillList()
            If lvVendorList.Items.Count > 0 Then lvVendorList.Items(0).Selected = True
            lvVendorList.Select()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmVendorList_Load", mstrModuleName)
        End Try
    End Sub

    Private Sub frmVendorList_KeyUp(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles MyBase.KeyUp
        Try
            If e.KeyCode = Keys.Delete And lvVendorList.Focused = True Then
                btnDelete_Click(sender, e)
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmVendorList_KeyUp", mstrModuleName)
        End Try
    End Sub

    Private Sub frmVendorList_FormClosed(ByVal sender As System.Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles MyBase.FormClosed
        'Sohail (11 Sep 2010) -- Start
        Try
        objVendormaster = Nothing
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmVendorList_FormClosed", mstrModuleName)
        End Try
        'Sohail (11 Sep 2010) -- End
    End Sub

#End Region

#Region "Button's Event"

    Private Sub btnNew_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNew.Click
        Try
            Dim objfrmVendor_AddEdit As New frmVendor_AddEdit
            If objfrmVendor_AddEdit.displayDialog(-1, enAction.ADD_CONTINUE) Then
                fillList()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnNew_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnEdit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEdit.Click, lvVendorList.DoubleClick 'Sohail (11 Sep 2010)
        Try
            If lvVendorList.SelectedItems.Count < 1 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please select Vendor from the list to perform further operation on it."), enMsgBoxStyle.Information) '?1
                lvVendorList.Select()
                Exit Sub
            End If
            Dim objfrmVendor_AddEdit As New frmVendor_AddEdit
            Try
                Dim intSelectedIndex As Integer
                intSelectedIndex = lvVendorList.SelectedItems(0).Index
                If objfrmVendor_AddEdit.displayDialog(CInt(lvVendorList.SelectedItems(0).Tag), enAction.EDIT_ONE) Then
                    fillList()
                End If
                objfrmVendor_AddEdit = Nothing

                lvVendorList.Items(intSelectedIndex).Selected = True
                lvVendorList.EnsureVisible(intSelectedIndex)
                lvVendorList.Select()
            Catch ex As Exception
                Call DisplayError.Show("-1", ex.Message, "btnEdit_Click", mstrModuleName)
            Finally
                If objfrmVendor_AddEdit IsNot Nothing Then objfrmVendor_AddEdit.Dispose()
            End Try
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnEdit_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDelete.Click
        If lvVendorList.SelectedItems.Count < 1 Then
            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please select Vendor from the list to perform further operation on it."), enMsgBoxStyle.Information) '?1
            lvVendorList.Select()
            Exit Sub
        End If
        'If objVendormaster.isUsed(CInt(lvAdvertise.SelectedItems(0).Tag)) Then
        '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Sorry, You cannot delete this Vendor. Reason: This Vendor is in use."), enMsgBoxStyle.Information) '?2
        '    lvAdvertise.Select()
        '    Exit Sub
        'End If
        Try
            Dim intSelectedIndex As Integer
            intSelectedIndex = lvVendorList.SelectedItems(0).Index

            If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Are you sure you want to delete this Vendor?"), CType(enMsgBoxStyle.Question + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.Yes Then

'S.SANDEEP [28-May-2018] -- START
'ISSUE/ENHANCEMENT : {Audit Trails} 
objVendormaster._FormName = mstrModuleName 
objVendormaster._LoginEmployeeUnkid = 0 
objVendormaster._ClientIP = getIP() 
objVendormaster._HostName = getHostName() 
objVendormaster._FromWeb = False
objVendormaster._AuditUserId = mintAuditUserId
objVendormaster._AuditDate = mdtAuditDate
'S.SANDEEP [28-May-2018] -- END

                objVendormaster.Delete(CInt(lvVendorList.SelectedItems(0).Tag))
                lvVendorList.SelectedItems(0).Remove()

                If lvVendorList.Items.Count <= 0 Then
                    Exit Try
                End If

                If lvVendorList.Items.Count = intSelectedIndex Then
                    intSelectedIndex = lvVendorList.Items.Count - 1
                    lvVendorList.Items(intSelectedIndex).Selected = True
                    lvVendorList.EnsureVisible(intSelectedIndex)
                ElseIf lvVendorList.Items.Count <> 0 Then
                    lvVendorList.Items(intSelectedIndex).Selected = True
                    lvVendorList.EnsureVisible(intSelectedIndex)
                End If
            End If
            lvVendorList.Select()
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "btnDelete_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        'Sohail (11 Sep 2010) -- Start
        Try
        Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnClose_Click", mstrModuleName)
        End Try
        'Sohail (11 Sep 2010) -- End
    End Sub

#End Region

#Region " Private Methods "

    Private Sub fillList()
        Dim dsVendor As New DataSet
        Try

'S.SANDEEP [28-May-2018] -- START
'ISSUE/ENHANCEMENT : {Audit Trails} 
objVendormaster._FormName = mstrModuleName 
objVendormaster._LoginEmployeeUnkid = 0 
objVendormaster._ClientIP = getIP() 
objVendormaster._HostName = getHostName() 
objVendormaster._FromWeb = False
objVendormaster._AuditUserId = mintAuditUserId
objVendormaster._AuditDate = mdtAuditDate
'S.SANDEEP [28-May-2018] -- END

            dsVendor = objVendormaster.GetList("List")

            Dim lvItem As ListViewItem

            lvVendorList.Items.Clear()
            For Each drRow As DataRow In dsVendor.Tables(0).Rows
                lvItem = New ListViewItem
                lvItem.Text = drRow("vendorgroup").ToString
                lvItem.Tag = drRow("vendorunkid")
                lvItem.SubItems.Add(drRow("vendorcode").ToString)
                lvItem.SubItems.Add(drRow("companyname").ToString)
                lvItem.SubItems.Add(drRow("address").ToString)
                lvItem.SubItems.Add(drRow("state").ToString)
                lvItem.SubItems.Add(drRow("pincode").ToString)
                lvVendorList.Items.Add(lvItem)
            Next

            If lvVendorList.Items.Count > 16 Then
                colhVendorPinCode.Width = 100 - 18
            Else
                colhVendorPinCode.Width = 100
            End If

        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "fillList", mstrModuleName)
        Finally
            dsVendor.Dispose()
        End Try
    End Sub

#End Region


	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Private Sub OtherSettings()
		Try
			Me.SuspendLayout()
            Call SetLanguage()
			
		
			Me.eZeeHeader.GradientColor1 = GUI._HeaderBackColor1 
			Me.eZeeHeader.GradientColor2 = GUI._HeaderBackColor2 
			Me.eZeeHeader.BorderColor = GUI._HeaderBorderColor 
			Me.eZeeHeader.HeaderTextForeColor = GUI._HeaderTitleFontColor 
			Me.eZeeHeader.DescriptionForeColor = GUI._HeaderMessageFontColor 


			Me.btnDelete.GradientBackColor = GUI._ButttonBackColor 
			Me.btnDelete.GradientForeColor = GUI._ButttonFontColor

			Me.btnEdit.GradientBackColor = GUI._ButttonBackColor 
			Me.btnEdit.GradientForeColor = GUI._ButttonFontColor

			Me.btnNew.GradientBackColor = GUI._ButttonBackColor 
			Me.btnNew.GradientForeColor = GUI._ButttonFontColor

			Me.btnClose.GradientBackColor = GUI._ButttonBackColor 
			Me.btnClose.GradientForeColor = GUI._ButttonFontColor


			Me.ResumeLayout()
		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "OtherSettings", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetLanguage()
		Try
			Me.Text = Language._Object.getCaption(Me.Name, Me.Text)
			
			Me.colhVendorgroup.Text = Language._Object.getCaption(CStr(Me.colhVendorgroup.Tag), Me.colhVendorgroup.Text)
			Me.colhVendorcode.Text = Language._Object.getCaption(CStr(Me.colhVendorcode.Tag), Me.colhVendorcode.Text)
			Me.colhVendorname.Text = Language._Object.getCaption(CStr(Me.colhVendorname.Tag), Me.colhVendorname.Text)
			Me.colhVendorAddress.Text = Language._Object.getCaption(CStr(Me.colhVendorAddress.Tag), Me.colhVendorAddress.Text)
			Me.eZeeHeader.Title = Language._Object.getCaption(Me.eZeeHeader.Name & "_Title" , Me.eZeeHeader.Title)
			Me.eZeeHeader.Message = Language._Object.getCaption(Me.eZeeHeader.Name & "_Message" , Me.eZeeHeader.Message)
			Me.btnDelete.Text = Language._Object.getCaption(Me.btnDelete.Name, Me.btnDelete.Text)
			Me.btnEdit.Text = Language._Object.getCaption(Me.btnEdit.Name, Me.btnEdit.Text)
			Me.btnNew.Text = Language._Object.getCaption(Me.btnNew.Name, Me.btnNew.Text)
			Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
			Me.colhVendorState.Text = Language._Object.getCaption(CStr(Me.colhVendorState.Tag), Me.colhVendorState.Text)
			Me.colhVendorPinCode.Text = Language._Object.getCaption(CStr(Me.colhVendorPinCode.Tag), Me.colhVendorPinCode.Text)

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetLanguage", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "Please select Vendor from the list to perform further operation on it.")
			Language.setMessage(mstrModuleName, 2, "Are you sure you want to delete this Vendor?")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class