﻿Option Strict On


Imports eZeeCommonLib
Imports Aruti.Data

Public Class frmExchangeRateList

    Public strcurrency_Name As String
    Dim objExchangeRate As clsExchangeRate
    'Sandeep | 04 JAN 2010 | -- Start
    Private mintOldBaseCurrId As Integer = -1
    Private mintNewBaseCurrId As Integer = -1
    'Sandeep | 04 JAN 2010 | -- END 
    Private ReadOnly mstrModuleName As String = "frmExchangeRateList"

#Region " Private Methods "

    Private Sub fillList()
        Dim dsExchangeRate As DataSet = Nothing
        Dim dsCountryList As DataSet = Nothing
        Try

            If User._Object.Privilege._AllowToViewCurrencyList = True Then

                dsExchangeRate = objExchangeRate.GetList("List")
                Dim objMasterData As New clsMasterData
                Dim objDataOperation As New clsDataOperation
                Dim strQ As String
                'Sohail (19 Nov 2010) -- Start
                'strQ = "SELECT * FROM cfcountry_master WHERE countryunkid=@countryunkid"
                strQ = "SELECT * FROM hrmsConfiguration..cfcountry_master WHERE countryunkid=@countryunkid"
                'Sohail (19 Nov 2010) -- End
                Dim lvItem As ListViewItem

                lvExchangeRate.Items.Clear()

                'Anjan (29 Nov 2010)-Start
                If dsExchangeRate.Tables(0).Rows.Count <= 0 Then Exit Sub
                'Anjan (29 Nov 2010)-End

                For Each drRow As DataRow In dsExchangeRate.Tables(0).Rows
                    lvItem = New ListViewItem
                    objDataOperation.ClearParameters()

                    objDataOperation.AddParameter("@countryunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, drRow("countryunkid").ToString)
                    dsCountryList = objDataOperation.ExecQuery(strQ, "CountryList")

                    lvItem.Text = dsCountryList.Tables("CountryList").Rows(0)("country_name").ToString
                    lvItem.Tag = drRow("exchangerateunkid")
                    'Sandeep | 04 JAN 2010 | -- Start
                    'If CInt(drRow("exchangerateunkid")) = 1 Then
                    '    lvItem.ForeColor = Color.Red
                    'End If
                    'Sandeep | 04 JAN 2010 | -- END 
                    lvItem.SubItems.Add(drRow("currency_name").ToString)
                    lvItem.SubItems.Add(drRow("currency_sign").ToString)
                    lvItem.SubItems.Add(drRow("exchange_rate").ToString)

                    'Sandeep | 04 JAN 2010 | -- Start
                    If CBool(drRow.Item("isbasecurrency")) = True Then
                        mintOldBaseCurrId = CInt(drRow("exchangerateunkid"))
                        lvItem.ForeColor = Color.Red
                    End If
                    'Sandeep | 04 JAN 2010 | -- END 

                    'S.SANDEEP [ 07 NOV 2011 ] -- START
                    'ENHANCEMENT : TRA CHANGES
                    If IsDBNull(drRow("exchange_date")) = False Then
                        lvItem.SubItems.Add(eZeeDate.convertDate(drRow("exchange_date").ToString).ToShortDateString)
                    Else
                        lvItem.SubItems.Add("")
                    End If
                    'S.SANDEEP [ 07 NOV 2011 ] -- END

                    lvExchangeRate.Items.Add(lvItem)
                Next



                If lvExchangeRate.Items.Count > 16 Then
                    colhCurrency.Width = 130 - 18
                Else
                    colhCurrency.Width = 130
                End If

                objMasterData = Nothing
                objDataOperation = Nothing

            End If

        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "fillList", mstrModuleName)
        Finally
            If dsExchangeRate IsNot Nothing Then dsExchangeRate.Dispose()
            If dsCountryList IsNot Nothing Then dsCountryList.Dispose()
        End Try
    End Sub

    Private Sub AssignContextMenuItemText()
        Try
            objtsmiNew.Text = btnNew.Text
            objtsmiEdit.Text = btnEdit.Text
            objtsmiDelete.Text = btnDelete.Text
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "AssignContextMenuItemText", mstrModuleName)
        End Try
    End Sub

    Private Sub SetVisibility()

        Try
            btnNew.Enabled = User._Object.Privilege._AddCurrency
            btnEdit.Enabled = User._Object.Privilege._EditCurrency
            btnDelete.Enabled = User._Object.Privilege._DeleteCurrency
            btnMakeBaseCurrency.Visible = False 'Sohail (03 Sep 2012) - [Now base currency is defined from company localization and do not allow user to make base currency which has multiple rates on differenct date]
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetVisibility", mstrModuleName)
        End Try


    End Sub
#End Region

#Region " Form's Events "

    Private Sub frmExchangeRateList_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        objExchangeRate = Nothing
    End Sub

    Private Sub frmExchangeRateList_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles Me.KeyPress
        If Asc(e.KeyChar) = 27 Then
            Me.Close()
        End If
    End Sub

    Private Sub frmExchangeRateList_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            Call Set_Logo(Me, gApplicationType)
            objExchangeRate = New clsExchangeRate
            Call Language.setLanguage(Me.Name)

            'Anjan (02 Sep 2011)-Start
            'Issue : Including Language Settings.
            Call OtherSettings()
            'Anjan (02 Sep 2011)-End 


            Call SetVisibility()
            ' Call OtherSettings()
            ''Call ListViewSettings()

            Call fillList()
            If lvExchangeRate.Items.Count > 0 Then lvExchangeRate.Items(0).Selected = True
            lvExchangeRate.Select()
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "frmExchangeRateList_Load", mstrModuleName)
        End Try
    End Sub

    'Anjan (02 Sep 2011)-Start
    'Issue : Including Language Settings.
    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsExchangeRate.SetMessages()
            objfrm._Other_ModuleNames = "clsExchangeRate"
            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub
    'Anjan (02 Sep 2011)-End 

#End Region

#Region " Button's Events "

    Private Sub btnNew_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNew.Click, objtsmiNew.Click
        Dim frm As New frmExchangeRate_AddEdit
        Try
            'Anjan (02 Sep 2011)-Start
            'Issue : Including Language Settings.
            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If
            'Anjan (02 Sep 2011)-End 

            Call frm.displayDialog(-1, enAction.ADD_CONTINUE)
            Call fillList()
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "btnNew_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub btnEdit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEdit.Click, objtsmiEdit.Click, lvExchangeRate.DoubleClick
        'Sohail (24 Jun 2011) -- Start
        If User._Object.Privilege._EditCurrency = False Then Exit Sub
        'Sohail (24 Jun 2011) -- End
        If lvExchangeRate.SelectedItems.Count < 1 Then
            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please select Exchange Rate from the list to perform operation."), enMsgBoxStyle.Information) '?1eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please select Exchange Rate from the list to perform operation."), enMsgBoxStyle.Information) '?1
            lvExchangeRate.Select()
            Exit Sub
        End If

        'S.SANDEEP [ 05 FEB 2013 ] -- START
        'ENHANCEMENT : TRA CHANGES

        'Varsha (13 Dec 2017) -- Start
        'Enhancement: Allow to change Base currency Decimal Place
        'If objExchangeRate.isUsed(CInt(lvExchangeRate.SelectedItems(0).Tag)) = True Then
        objExchangeRate._ExchangeRateunkid = CInt(lvExchangeRate.SelectedItems(0).Tag)
        If objExchangeRate.isUsed(CInt(lvExchangeRate.SelectedItems(0).Tag)) = True AndAlso objExchangeRate._Isbasecurrency = False Then
            'Varsha (13 Dec 2017) -- End
            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 6, "Sorry, you can not Edit this Currency. This Currency is in Use."), enMsgBoxStyle.Information)
            lvExchangeRate.Select()
            Exit Sub
        End If
        'S.SANDEEP [ 05 FEB 2013 ] -- END

        Dim frm As New frmExchangeRate_AddEdit
        Try
            Dim intSelectedIndex As Integer
            intSelectedIndex = lvExchangeRate.SelectedItems(0).Index

            'Anjan (02 Sep 2011)-Start
            'Issue : Including Language Settings.
            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If
            'Anjan (02 Sep 2011)-End 


            'Sandeep | 04 JAN 2010 | -- Start
            'Call frm.displayDialog(CInt(lvExchangeRate.SelectedItems(0).Tag), enAction.EDIT_ONE)
            If lvExchangeRate.SelectedItems(0).ForeColor = Color.Red Then
                Call frm.displayDialog(CInt(lvExchangeRate.SelectedItems(0).Tag), enAction.EDIT_ONE, True)
            Else
                Call frm.displayDialog(CInt(lvExchangeRate.SelectedItems(0).Tag), enAction.EDIT_ONE)
            End If
            'Sandeep | 04 JAN 2010 | -- END 
            frm = Nothing
            Call fillList()

            lvExchangeRate.Items(intSelectedIndex).Selected = True
            lvExchangeRate.EnsureVisible(intSelectedIndex)
            lvExchangeRate.Select()
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "btnEdit_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub btnDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDelete.Click, objtsmiDelete.Click
        If lvExchangeRate.SelectedItems.Count < 1 Then
            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please select Exchange Rate from the list to perform operation."), enMsgBoxStyle.Information) '?1eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please select Exchange Rate from the list to perform operation."), enMsgBoxStyle.Information) '?1
            lvExchangeRate.Select()
            Exit Sub
        End If
        Try
            Dim intSelectedIndex As Integer
            intSelectedIndex = lvExchangeRate.SelectedItems(0).Index


            'Sandeep | 04 JAN 2010 | -- Start
            'If CInt(lvExchangeRate.SelectedItems(0).Tag) = 1 Then
            If lvExchangeRate.SelectedItems(0).ForeColor = Color.Red Then
                'Sandeep | 04 JAN 2010 | -- END 
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Sorry, you can not delete Base Currency."), enMsgBoxStyle.Information) '?2
                lvExchangeRate.Select()
                Exit Sub
            End If

            'If objExchangeRate.isUsed(CInt(lvExchangeRate.SelectedItems(0).Tag)) = True And objExchangeRate._Message <> "" Then
            '    eZeeMsgBox.Show(objExchangeRate._Message, enMsgBoxStyle.Information)
            'lvExchangeRate.Select()
            'Exit Sub
            'End If
            'Sohail (19 Nov 2010) -- Start
            If objExchangeRate.isUsed(CInt(lvExchangeRate.SelectedItems(0).Tag)) Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Sorry, you can not delete this Currency. This Currency is in Use."), enMsgBoxStyle.Information)
                lvExchangeRate.Select()
                Exit Sub
            End If
            'Sohail (19 Nov 2010) -- End

            If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Are you sure you want to delete selected Exchange Rate?"), CType(enMsgBoxStyle.Question + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.Yes Then '?4

                'S.SANDEEP [28-May-2018] -- START
                'ISSUE/ENHANCEMENT : {Audit Trails} 
                objExchangeRate._FormName = mstrModuleName
                objExchangeRate._LoginEmployeeunkid = 0
                objExchangeRate._ClientIP = getIP()
                objExchangeRate._HostName = getHostName()
                objExchangeRate._FromWeb = False
                objExchangeRate._AuditUserId = User._Object._Userunkid
objExchangeRate._CompanyUnkid = Company._Object._Companyunkid
                objExchangeRate._AuditDate = ConfigParameter._Object._CurrentDateAndTime
                'S.SANDEEP [28-May-2018] -- END

                objExchangeRate.Delete(CInt(lvExchangeRate.SelectedItems(0).Tag))
                lvExchangeRate.SelectedItems(0).Remove()

                If lvExchangeRate.Items.Count <= 0 Then
                    Exit Try
                End If

                If lvExchangeRate.Items.Count = intSelectedIndex Then
                    intSelectedIndex = lvExchangeRate.Items.Count - 1
                    lvExchangeRate.Items(intSelectedIndex).Selected = True
                    lvExchangeRate.EnsureVisible(intSelectedIndex)
                ElseIf lvExchangeRate.Items.Count <> 0 Then
                    lvExchangeRate.Items(intSelectedIndex).Selected = True
                    lvExchangeRate.EnsureVisible(intSelectedIndex)
                End If
            End If
            lvExchangeRate.Select()
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "btnDelete_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Me.Close()
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "btnCancel_Click", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Other Control's Events "

    Private Sub cpOperation_Opening(ByVal sender As System.Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles cpOperation.Opening
        If lvExchangeRate.SelectedItems.Count < 1 Then
            objtsmiEdit.Enabled = False
            objtsmiDelete.Enabled = False
        Else
            objtsmiEdit.Enabled = True
            objtsmiDelete.Enabled = True
        End If
    End Sub

    Private Sub lvExchangeRate_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles lvExchangeRate.KeyPress
        If Asc(e.KeyChar) = 13 Then
            If lvExchangeRate.SelectedItems.Count > 0 Then
                Call btnEdit.PerformClick()
            Else
                Call btnNew.PerformClick()
            End If
        End If
    End Sub

#End Region

    'Sandeep | 04 JAN 2010 | -- Start
    Private Sub btnMakeBaseCurrency_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnMakeBaseCurrency.Click
        If lvExchangeRate.SelectedItems.Count <= 0 Then
            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please select Exchange Rate from the list to perform operation."), enMsgBoxStyle.Information) '?1eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please select Exchange Rate from the list to perform operation."), enMsgBoxStyle.Information) '?1
            lvExchangeRate.Select()
            Exit Sub
        End If
        Dim blnFlag As Boolean = False
        Try
            If lvExchangeRate.SelectedItems(0).ForeColor = Color.Red Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "Sorry, you can not make this currency as Base Currency. Reason : This currency is already a Base Currency"), enMsgBoxStyle.Information) '?2
                lvExchangeRate.Select()
                Exit Sub
            End If

            mintNewBaseCurrId = CInt(lvExchangeRate.SelectedItems(0).Tag)


            'S.SANDEEP [28-May-2018] -- START
            'ISSUE/ENHANCEMENT : {Audit Trails} 
            objExchangeRate._FormName = mstrModuleName
            objExchangeRate._LoginEmployeeunkid = 0
            objExchangeRate._ClientIP = getIP()
            objExchangeRate._HostName = getHostName()
            objExchangeRate._FromWeb = False
            objExchangeRate._AuditUserId = User._Object._Userunkid
objExchangeRate._CompanyUnkid = Company._Object._Companyunkid
            objExchangeRate._AuditDate = ConfigParameter._Object._CurrentDateAndTime
            'S.SANDEEP [28-May-2018] -- END

            blnFlag = objExchangeRate.SetBaseCurrency(mintOldBaseCurrId, mintNewBaseCurrId)

            If blnFlag = True Then
                'Sandeep [ 01 MARCH 2011 ] -- Start
                Dim objExRate As New clsExchangeRate
                objExRate._ExchangeRateunkid = mintNewBaseCurrId
                ConfigParameter._Object._Base_CurrencyId = mintNewBaseCurrId
                ConfigParameter._Object._CurrencyFormat = "#,###,###,###,###,###,###,###,###,##0." & New String(CChar("0"), objExRate._Digits_After_Decimal)
                ConfigParameter._Object._CountryUnkid = objExRate._Countryunkid
                ConfigParameter._Object._Currency = objExRate._Currency_Sign
                ConfigParameter._Object.updateParam()
                ConfigParameter._Object.Refresh()
                objExRate = Nothing
                GUI.fmtCurrency = ConfigParameter._Object._CurrencyFormat
                Company._Object._Localization_Country = ConfigParameter._Object._CountryUnkid
                Company._Object._Localization_Currency = ConfigParameter._Object._Currency
                'Sandeep [ 01 MARCH 2011 ] -- End 
                Call fillList()
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnMakeBaseCurrency_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub lvExchangeRate_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lvExchangeRate.Click, lvExchangeRate.SelectedIndexChanged
        Try
            If lvExchangeRate.SelectedItems.Count > 0 Then
                If lvExchangeRate.SelectedItems(0).ForeColor = Color.Red Then
                    btnMakeBaseCurrency.Enabled = False
                Else
                    btnMakeBaseCurrency.Enabled = True
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lvExchangeRate_Click", mstrModuleName)
        End Try
    End Sub
    'Sandeep | 04 JAN 2010 | -- END 

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Private Sub OtherSettings()
        Try
            Me.SuspendLayout()
           
            Call SetLanguage()

          
            Me.eZeeHeader.GradientColor1 = GUI._HeaderBackColor1
            Me.eZeeHeader.GradientColor2 = GUI._HeaderBackColor2
            Me.eZeeHeader.BorderColor = GUI._HeaderBorderColor
            Me.eZeeHeader.HeaderTextForeColor = GUI._HeaderTitleFontColor
            Me.eZeeHeader.DescriptionForeColor = GUI._HeaderMessageFontColor


            Me.btnDelete.GradientBackColor = GUI._ButttonBackColor
            Me.btnDelete.GradientForeColor = GUI._ButttonFontColor

            Me.btnEdit.GradientBackColor = GUI._ButttonBackColor
            Me.btnEdit.GradientForeColor = GUI._ButttonFontColor

            Me.btnNew.GradientBackColor = GUI._ButttonBackColor
            Me.btnNew.GradientForeColor = GUI._ButttonFontColor

            Me.btnClose.GradientBackColor = GUI._ButttonBackColor
            Me.btnClose.GradientForeColor = GUI._ButttonFontColor

            Me.btnMakeBaseCurrency.GradientBackColor = GUI._ButttonBackColor
            Me.btnMakeBaseCurrency.GradientForeColor = GUI._ButttonFontColor


            Me.ResumeLayout()
        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "OtherSettings", mstrModuleName)
        End Try
    End Sub


    Private Sub SetLanguage()
        Try
            Me.Text = Language._Object.getCaption(Me.Name, Me.Text)

            Me.colhCurrency.Text = Language._Object.getCaption(CStr(Me.colhCurrency.Tag), Me.colhCurrency.Text)
            Me.colhCurrencySign.Text = Language._Object.getCaption(CStr(Me.colhCurrencySign.Tag), Me.colhCurrencySign.Text)
            Me.colhCurrencyCountry.Text = Language._Object.getCaption(CStr(Me.colhCurrencyCountry.Tag), Me.colhCurrencyCountry.Text)
            Me.eZeeHeader.Title = Language._Object.getCaption(Me.eZeeHeader.Name & "_Title", Me.eZeeHeader.Title)
            Me.eZeeHeader.Message = Language._Object.getCaption(Me.eZeeHeader.Name & "_Message", Me.eZeeHeader.Message)
            Me.btnDelete.Text = Language._Object.getCaption(Me.btnDelete.Name, Me.btnDelete.Text)
            Me.btnEdit.Text = Language._Object.getCaption(Me.btnEdit.Name, Me.btnEdit.Text)
            Me.btnNew.Text = Language._Object.getCaption(Me.btnNew.Name, Me.btnNew.Text)
            Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
            Me.colhExchangeRate.Text = Language._Object.getCaption(CStr(Me.colhExchangeRate.Tag), Me.colhExchangeRate.Text)
            Me.btnMakeBaseCurrency.Text = Language._Object.getCaption(Me.btnMakeBaseCurrency.Name, Me.btnMakeBaseCurrency.Text)
            Me.colhExchangeDate.Text = Language._Object.getCaption(CStr(Me.colhExchangeDate.Tag), Me.colhExchangeDate.Text)


            Call AssignContextMenuItemText()
        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
        End Try
    End Sub


    Private Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Please select Exchange Rate from the list to perform operation.")
            Language.setMessage(mstrModuleName, 2, "Sorry, you can not delete Base Currency.")
            Language.setMessage(mstrModuleName, 3, "Sorry, you can not delete this Currency. This Currency is in Use.")
            Language.setMessage(mstrModuleName, 4, "Are you sure you want to delete selected Exchange Rate?")
            Language.setMessage(mstrModuleName, 5, "Sorry, you can not make this currency as Base Currency. Reason : This currency is already a Base Currency")
            Language.setMessage(mstrModuleName, 6, "Sorry, you can not Edit this Currency. This Currency is in Use.")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class