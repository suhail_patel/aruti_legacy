﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmAutoGeneratePeriods
    Inherits eZee.Common.eZeeForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmAutoGeneratePeriods))
        Me.pnlPeriod = New System.Windows.Forms.Panel
        Me.objFooter = New eZee.Common.eZeeFooter
        Me.btnGenerate = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnClose = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnSave = New eZee.Common.eZeeLightButton(Me.components)
        Me.gbPeriod = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.Panel1 = New System.Windows.Forms.Panel
        Me.lvPeriodList = New eZee.Common.eZeeListView(Me.components)
        Me.colhPeriodcode = New System.Windows.Forms.ColumnHeader
        Me.colhPeriodname = New System.Windows.Forms.ColumnHeader
        Me.colhStartperiod = New System.Windows.Forms.ColumnHeader
        Me.colhEndperiod = New System.Windows.Forms.ColumnHeader
        Me.colhTnAEndDate = New System.Windows.Forms.ColumnHeader
        Me.cboNameFormat = New System.Windows.Forms.ComboBox
        Me.Label1 = New System.Windows.Forms.Label
        Me.cboCodeFormat = New System.Windows.Forms.ComboBox
        Me.lblYear = New System.Windows.Forms.Label
        Me.dtpEnddate = New System.Windows.Forms.DateTimePicker
        Me.dtpStartdate = New System.Windows.Forms.DateTimePicker
        Me.lblStartdate = New System.Windows.Forms.Label
        Me.lblEnddate = New System.Windows.Forms.Label
        Me.EZeeHeader1 = New eZee.Common.eZeeHeader
        Me.colhSunjvPeriodCode = New System.Windows.Forms.ColumnHeader
        Me.pnlPeriod.SuspendLayout()
        Me.objFooter.SuspendLayout()
        Me.gbPeriod.SuspendLayout()
        Me.Panel1.SuspendLayout()
        Me.SuspendLayout()
        '
        'pnlPeriod
        '
        Me.pnlPeriod.Controls.Add(Me.objFooter)
        Me.pnlPeriod.Controls.Add(Me.gbPeriod)
        Me.pnlPeriod.Controls.Add(Me.EZeeHeader1)
        Me.pnlPeriod.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pnlPeriod.Location = New System.Drawing.Point(0, 0)
        Me.pnlPeriod.Name = "pnlPeriod"
        Me.pnlPeriod.Size = New System.Drawing.Size(749, 364)
        Me.pnlPeriod.TabIndex = 0
        '
        'objFooter
        '
        Me.objFooter.BorderColor = System.Drawing.Color.Silver
        Me.objFooter.Controls.Add(Me.btnGenerate)
        Me.objFooter.Controls.Add(Me.btnClose)
        Me.objFooter.Controls.Add(Me.btnSave)
        Me.objFooter.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.objFooter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objFooter.GradiantStyle = eZee.Common.eZeeFooter.GradientStyle.Up
        Me.objFooter.GradientColor1 = System.Drawing.SystemColors.Control
        Me.objFooter.GradientColor2 = System.Drawing.SystemColors.Control
        Me.objFooter.Location = New System.Drawing.Point(0, 309)
        Me.objFooter.Name = "objFooter"
        Me.objFooter.Size = New System.Drawing.Size(749, 55)
        Me.objFooter.TabIndex = 1
        '
        'btnGenerate
        '
        Me.btnGenerate.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnGenerate.BackColor = System.Drawing.Color.White
        Me.btnGenerate.BackgroundImage = CType(resources.GetObject("btnGenerate.BackgroundImage"), System.Drawing.Image)
        Me.btnGenerate.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnGenerate.BorderColor = System.Drawing.Color.Empty
        Me.btnGenerate.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnGenerate.FlatAppearance.BorderSize = 0
        Me.btnGenerate.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnGenerate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnGenerate.ForeColor = System.Drawing.Color.Black
        Me.btnGenerate.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnGenerate.GradientForeColor = System.Drawing.Color.Black
        Me.btnGenerate.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnGenerate.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnGenerate.Location = New System.Drawing.Point(456, 13)
        Me.btnGenerate.Name = "btnGenerate"
        Me.btnGenerate.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnGenerate.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnGenerate.Size = New System.Drawing.Size(90, 30)
        Me.btnGenerate.TabIndex = 0
        Me.btnGenerate.Text = "&Generate"
        Me.btnGenerate.UseVisualStyleBackColor = True
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnClose.BackColor = System.Drawing.Color.White
        Me.btnClose.BackgroundImage = CType(resources.GetObject("btnClose.BackgroundImage"), System.Drawing.Image)
        Me.btnClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnClose.BorderColor = System.Drawing.Color.Empty
        Me.btnClose.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnClose.FlatAppearance.BorderSize = 0
        Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClose.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.ForeColor = System.Drawing.Color.Black
        Me.btnClose.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnClose.GradientForeColor = System.Drawing.Color.Black
        Me.btnClose.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Location = New System.Drawing.Point(648, 13)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Size = New System.Drawing.Size(90, 30)
        Me.btnClose.TabIndex = 2
        Me.btnClose.Text = "&Close"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'btnSave
        '
        Me.btnSave.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnSave.BackColor = System.Drawing.Color.White
        Me.btnSave.BackgroundImage = CType(resources.GetObject("btnSave.BackgroundImage"), System.Drawing.Image)
        Me.btnSave.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnSave.BorderColor = System.Drawing.Color.Empty
        Me.btnSave.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnSave.FlatAppearance.BorderSize = 0
        Me.btnSave.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnSave.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSave.ForeColor = System.Drawing.Color.Black
        Me.btnSave.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnSave.GradientForeColor = System.Drawing.Color.Black
        Me.btnSave.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSave.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnSave.Location = New System.Drawing.Point(552, 13)
        Me.btnSave.Name = "btnSave"
        Me.btnSave.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSave.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnSave.Size = New System.Drawing.Size(90, 30)
        Me.btnSave.TabIndex = 1
        Me.btnSave.Text = "&Save"
        Me.btnSave.UseVisualStyleBackColor = True
        '
        'gbPeriod
        '
        Me.gbPeriod.BorderColor = System.Drawing.Color.Black
        Me.gbPeriod.Checked = False
        Me.gbPeriod.CollapseAllExceptThis = False
        Me.gbPeriod.CollapsedHoverImage = Nothing
        Me.gbPeriod.CollapsedNormalImage = Nothing
        Me.gbPeriod.CollapsedPressedImage = Nothing
        Me.gbPeriod.CollapseOnLoad = False
        Me.gbPeriod.Controls.Add(Me.Panel1)
        Me.gbPeriod.Controls.Add(Me.cboNameFormat)
        Me.gbPeriod.Controls.Add(Me.Label1)
        Me.gbPeriod.Controls.Add(Me.cboCodeFormat)
        Me.gbPeriod.Controls.Add(Me.lblYear)
        Me.gbPeriod.Controls.Add(Me.dtpEnddate)
        Me.gbPeriod.Controls.Add(Me.dtpStartdate)
        Me.gbPeriod.Controls.Add(Me.lblStartdate)
        Me.gbPeriod.Controls.Add(Me.lblEnddate)
        Me.gbPeriod.ExpandedHoverImage = Nothing
        Me.gbPeriod.ExpandedNormalImage = Nothing
        Me.gbPeriod.ExpandedPressedImage = Nothing
        Me.gbPeriod.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbPeriod.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbPeriod.HeaderHeight = 25
        Me.gbPeriod.HeaderMessage = ""
        Me.gbPeriod.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbPeriod.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbPeriod.HeightOnCollapse = 0
        Me.gbPeriod.LeftTextSpace = 0
        Me.gbPeriod.Location = New System.Drawing.Point(11, 67)
        Me.gbPeriod.Name = "gbPeriod"
        Me.gbPeriod.OpenHeight = 300
        Me.gbPeriod.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbPeriod.ShowBorder = True
        Me.gbPeriod.ShowCheckBox = False
        Me.gbPeriod.ShowCollapseButton = False
        Me.gbPeriod.ShowDefaultBorderColor = True
        Me.gbPeriod.ShowDownButton = False
        Me.gbPeriod.ShowHeader = True
        Me.gbPeriod.Size = New System.Drawing.Size(727, 238)
        Me.gbPeriod.TabIndex = 0
        Me.gbPeriod.Temp = 0
        Me.gbPeriod.Text = "Period"
        Me.gbPeriod.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(Me.lvPeriodList)
        Me.Panel1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Panel1.Location = New System.Drawing.Point(231, 36)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(486, 194)
        Me.Panel1.TabIndex = 112
        '
        'lvPeriodList
        '
        Me.lvPeriodList.BackColorOnChecked = True
        Me.lvPeriodList.ColumnHeaders = Nothing
        Me.lvPeriodList.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.colhPeriodcode, Me.colhPeriodname, Me.colhStartperiod, Me.colhEndperiod, Me.colhTnAEndDate, Me.colhSunjvPeriodCode})
        Me.lvPeriodList.CompulsoryColumns = ""
        Me.lvPeriodList.Dock = System.Windows.Forms.DockStyle.Fill
        Me.lvPeriodList.FullRowSelect = True
        Me.lvPeriodList.GridLines = True
        Me.lvPeriodList.GroupingColumn = Nothing
        Me.lvPeriodList.HideSelection = False
        Me.lvPeriodList.Location = New System.Drawing.Point(0, 0)
        Me.lvPeriodList.MinColumnWidth = 50
        Me.lvPeriodList.MultiSelect = False
        Me.lvPeriodList.Name = "lvPeriodList"
        Me.lvPeriodList.OptionalColumns = ""
        Me.lvPeriodList.ShowMoreItem = False
        Me.lvPeriodList.ShowSaveItem = False
        Me.lvPeriodList.ShowSelectAll = True
        Me.lvPeriodList.ShowSizeAllColumnsToFit = True
        Me.lvPeriodList.Size = New System.Drawing.Size(486, 194)
        Me.lvPeriodList.Sortable = True
        Me.lvPeriodList.TabIndex = 0
        Me.lvPeriodList.UseCompatibleStateImageBehavior = False
        Me.lvPeriodList.View = System.Windows.Forms.View.Details
        '
        'colhPeriodcode
        '
        Me.colhPeriodcode.Tag = "colhPeriodcode"
        Me.colhPeriodcode.Text = "Code"
        Me.colhPeriodcode.Width = 80
        '
        'colhPeriodname
        '
        Me.colhPeriodname.Tag = "colhPeriodname"
        Me.colhPeriodname.Text = "Period Name"
        Me.colhPeriodname.Width = 120
        '
        'colhStartperiod
        '
        Me.colhStartperiod.Tag = "colhStartperiod"
        Me.colhStartperiod.Text = "Start Period"
        Me.colhStartperiod.Width = 96
        '
        'colhEndperiod
        '
        Me.colhEndperiod.Tag = "colhEndperiod"
        Me.colhEndperiod.Text = "End Period"
        Me.colhEndperiod.Width = 91
        '
        'colhTnAEndDate
        '
        Me.colhTnAEndDate.Tag = "colhTnAEndDate"
        Me.colhTnAEndDate.Text = "TnA End Period"
        Me.colhTnAEndDate.Width = 90
        '
        'cboNameFormat
        '
        Me.cboNameFormat.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboNameFormat.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboNameFormat.FormattingEnabled = True
        Me.cboNameFormat.Location = New System.Drawing.Point(107, 61)
        Me.cboNameFormat.Name = "cboNameFormat"
        Me.cboNameFormat.Size = New System.Drawing.Size(111, 21)
        Me.cboNameFormat.TabIndex = 1
        '
        'Label1
        '
        Me.Label1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(8, 63)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(93, 16)
        Me.Label1.TabIndex = 109
        Me.Label1.Text = "Name Format"
        Me.Label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboCodeFormat
        '
        Me.cboCodeFormat.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboCodeFormat.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboCodeFormat.FormattingEnabled = True
        Me.cboCodeFormat.Location = New System.Drawing.Point(107, 34)
        Me.cboCodeFormat.Name = "cboCodeFormat"
        Me.cboCodeFormat.Size = New System.Drawing.Size(111, 21)
        Me.cboCodeFormat.TabIndex = 0
        '
        'lblYear
        '
        Me.lblYear.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblYear.Location = New System.Drawing.Point(8, 36)
        Me.lblYear.Name = "lblYear"
        Me.lblYear.Size = New System.Drawing.Size(93, 16)
        Me.lblYear.TabIndex = 106
        Me.lblYear.Text = "Code Format"
        Me.lblYear.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'dtpEnddate
        '
        Me.dtpEnddate.Checked = False
        Me.dtpEnddate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpEnddate.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpEnddate.Location = New System.Drawing.Point(107, 115)
        Me.dtpEnddate.Name = "dtpEnddate"
        Me.dtpEnddate.Size = New System.Drawing.Size(111, 21)
        Me.dtpEnddate.TabIndex = 3
        '
        'dtpStartdate
        '
        Me.dtpStartdate.Checked = False
        Me.dtpStartdate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpStartdate.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpStartdate.Location = New System.Drawing.Point(107, 88)
        Me.dtpStartdate.Name = "dtpStartdate"
        Me.dtpStartdate.Size = New System.Drawing.Size(111, 21)
        Me.dtpStartdate.TabIndex = 2
        '
        'lblStartdate
        '
        Me.lblStartdate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblStartdate.Location = New System.Drawing.Point(8, 90)
        Me.lblStartdate.Name = "lblStartdate"
        Me.lblStartdate.Size = New System.Drawing.Size(93, 16)
        Me.lblStartdate.TabIndex = 97
        Me.lblStartdate.Text = "Starting From"
        Me.lblStartdate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblEnddate
        '
        Me.lblEnddate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEnddate.Location = New System.Drawing.Point(8, 117)
        Me.lblEnddate.Name = "lblEnddate"
        Me.lblEnddate.Size = New System.Drawing.Size(93, 16)
        Me.lblEnddate.TabIndex = 98
        Me.lblEnddate.Text = "End To"
        Me.lblEnddate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'EZeeHeader1
        '
        Me.EZeeHeader1.BackColor = System.Drawing.SystemColors.Control
        Me.EZeeHeader1.BorderColor = System.Drawing.SystemColors.ControlDark
        Me.EZeeHeader1.DescriptionForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.EZeeHeader1.Dock = System.Windows.Forms.DockStyle.Top
        Me.EZeeHeader1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.EZeeHeader1.GradiantStyle = eZee.Common.eZeeHeader.GradientStyle.Central
        Me.EZeeHeader1.GradientColor1 = System.Drawing.SystemColors.Window
        Me.EZeeHeader1.GradientColor2 = System.Drawing.SystemColors.Control
        Me.EZeeHeader1.HeaderTextForeColor = System.Drawing.SystemColors.ControlText
        Me.EZeeHeader1.Icon = Nothing
        Me.EZeeHeader1.Location = New System.Drawing.Point(0, 0)
        Me.EZeeHeader1.Message = ""
        Me.EZeeHeader1.Name = "EZeeHeader1"
        Me.EZeeHeader1.Size = New System.Drawing.Size(749, 60)
        Me.EZeeHeader1.TabIndex = 21
        Me.EZeeHeader1.Title = "Period Information"
        '
        'colhSunjvPeriodCode
        '
        Me.colhSunjvPeriodCode.Text = "colhSunjvPeriodCode"
        Me.colhSunjvPeriodCode.Width = 0
        '
        'frmAutoGeneratePeriods
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(749, 364)
        Me.Controls.Add(Me.pnlPeriod)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmAutoGeneratePeriods"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Auto Generate Periods"
        Me.pnlPeriod.ResumeLayout(False)
        Me.objFooter.ResumeLayout(False)
        Me.gbPeriod.ResumeLayout(False)
        Me.Panel1.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents pnlPeriod As System.Windows.Forms.Panel
    Friend WithEvents objFooter As eZee.Common.eZeeFooter
    Friend WithEvents btnClose As eZee.Common.eZeeLightButton
    Friend WithEvents btnSave As eZee.Common.eZeeLightButton
    Friend WithEvents gbPeriod As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents cboCodeFormat As System.Windows.Forms.ComboBox
    Friend WithEvents lblYear As System.Windows.Forms.Label
    Friend WithEvents dtpEnddate As System.Windows.Forms.DateTimePicker
    Friend WithEvents dtpStartdate As System.Windows.Forms.DateTimePicker
    Friend WithEvents lblStartdate As System.Windows.Forms.Label
    Friend WithEvents lblEnddate As System.Windows.Forms.Label
    Friend WithEvents EZeeHeader1 As eZee.Common.eZeeHeader
    Friend WithEvents cboNameFormat As System.Windows.Forms.ComboBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents btnGenerate As eZee.Common.eZeeLightButton
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents lvPeriodList As eZee.Common.eZeeListView
    Friend WithEvents colhPeriodcode As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhPeriodname As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhStartperiod As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhEndperiod As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhTnAEndDate As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhSunjvPeriodCode As System.Windows.Forms.ColumnHeader
End Class
