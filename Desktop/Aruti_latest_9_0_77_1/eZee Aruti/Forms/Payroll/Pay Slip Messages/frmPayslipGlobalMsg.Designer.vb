﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmPayslipGlobalMsg
    Inherits eZee.Common.eZeeForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmPayslipGlobalMsg))
        Me.pnlPayslipGlobalmsg = New System.Windows.Forms.Panel
        Me.eZeeHeader = New eZee.Common.eZeeHeader
        Me.objFooter = New eZee.Common.eZeeFooter
        Me.btnSave = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnClose = New eZee.Common.eZeeLightButton(Me.components)
        Me.gbPayslipMsg = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.txtMsg4 = New eZee.TextBox.AlphanumericTextBox
        Me.txtMsg3 = New eZee.TextBox.AlphanumericTextBox
        Me.txtMsg1 = New eZee.TextBox.AlphanumericTextBox
        Me.txtMsg2 = New eZee.TextBox.AlphanumericTextBox
        Me.lblMsg2 = New System.Windows.Forms.Label
        Me.lblMsg4 = New System.Windows.Forms.Label
        Me.lblMsg3 = New System.Windows.Forms.Label
        Me.lblMsg1 = New System.Windows.Forms.Label
        Me.objbtnSearchPeriod = New eZee.Common.eZeeGradientButton
        Me.cboPeriod = New System.Windows.Forms.ComboBox
        Me.lbleffectivePeriod = New System.Windows.Forms.Label
        Me.pnlPayslipGlobalmsg.SuspendLayout()
        Me.objFooter.SuspendLayout()
        Me.gbPayslipMsg.SuspendLayout()
        Me.SuspendLayout()
        '
        'pnlPayslipGlobalmsg
        '
        Me.pnlPayslipGlobalmsg.Controls.Add(Me.eZeeHeader)
        Me.pnlPayslipGlobalmsg.Controls.Add(Me.objFooter)
        Me.pnlPayslipGlobalmsg.Controls.Add(Me.gbPayslipMsg)
        Me.pnlPayslipGlobalmsg.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pnlPayslipGlobalmsg.Location = New System.Drawing.Point(0, 0)
        Me.pnlPayslipGlobalmsg.Name = "pnlPayslipGlobalmsg"
        Me.pnlPayslipGlobalmsg.Size = New System.Drawing.Size(487, 305)
        Me.pnlPayslipGlobalmsg.TabIndex = 0
        '
        'eZeeHeader
        '
        Me.eZeeHeader.BackColor = System.Drawing.SystemColors.Control
        Me.eZeeHeader.BorderColor = System.Drawing.SystemColors.ControlDark
        Me.eZeeHeader.DescriptionForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.eZeeHeader.Dock = System.Windows.Forms.DockStyle.Top
        Me.eZeeHeader.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.eZeeHeader.GradiantStyle = eZee.Common.eZeeHeader.GradientStyle.Central
        Me.eZeeHeader.GradientColor1 = System.Drawing.SystemColors.Window
        Me.eZeeHeader.GradientColor2 = System.Drawing.SystemColors.Control
        Me.eZeeHeader.HeaderTextForeColor = System.Drawing.SystemColors.ControlText
        Me.eZeeHeader.Icon = Nothing
        Me.eZeeHeader.Location = New System.Drawing.Point(0, 0)
        Me.eZeeHeader.Message = ""
        Me.eZeeHeader.Name = "eZeeHeader"
        Me.eZeeHeader.Size = New System.Drawing.Size(487, 60)
        Me.eZeeHeader.TabIndex = 2
        Me.eZeeHeader.Title = "ADD/EDIT Payslip Global Message"
        '
        'objFooter
        '
        Me.objFooter.BorderColor = System.Drawing.Color.Silver
        Me.objFooter.Controls.Add(Me.btnSave)
        Me.objFooter.Controls.Add(Me.btnClose)
        Me.objFooter.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.objFooter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objFooter.GradiantStyle = eZee.Common.eZeeFooter.GradientStyle.Up
        Me.objFooter.GradientColor1 = System.Drawing.SystemColors.Control
        Me.objFooter.GradientColor2 = System.Drawing.SystemColors.Control
        Me.objFooter.Location = New System.Drawing.Point(0, 250)
        Me.objFooter.Name = "objFooter"
        Me.objFooter.Size = New System.Drawing.Size(487, 55)
        Me.objFooter.TabIndex = 1
        '
        'btnSave
        '
        Me.btnSave.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnSave.BackColor = System.Drawing.Color.White
        Me.btnSave.BackgroundImage = CType(resources.GetObject("btnSave.BackgroundImage"), System.Drawing.Image)
        Me.btnSave.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnSave.BorderColor = System.Drawing.Color.Empty
        Me.btnSave.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnSave.FlatAppearance.BorderSize = 0
        Me.btnSave.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnSave.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSave.ForeColor = System.Drawing.Color.Black
        Me.btnSave.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnSave.GradientForeColor = System.Drawing.Color.Black
        Me.btnSave.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSave.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnSave.Location = New System.Drawing.Point(275, 13)
        Me.btnSave.Name = "btnSave"
        Me.btnSave.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSave.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnSave.Size = New System.Drawing.Size(97, 30)
        Me.btnSave.TabIndex = 0
        Me.btnSave.Text = "&Save"
        Me.btnSave.UseVisualStyleBackColor = True
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnClose.BackColor = System.Drawing.Color.White
        Me.btnClose.BackgroundImage = CType(resources.GetObject("btnClose.BackgroundImage"), System.Drawing.Image)
        Me.btnClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnClose.BorderColor = System.Drawing.Color.Empty
        Me.btnClose.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnClose.FlatAppearance.BorderSize = 0
        Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClose.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.ForeColor = System.Drawing.Color.Black
        Me.btnClose.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnClose.GradientForeColor = System.Drawing.Color.Black
        Me.btnClose.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Location = New System.Drawing.Point(378, 13)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Size = New System.Drawing.Size(97, 30)
        Me.btnClose.TabIndex = 1
        Me.btnClose.Text = "&Close"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'gbPayslipMsg
        '
        Me.gbPayslipMsg.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.gbPayslipMsg.BorderColor = System.Drawing.Color.Black
        Me.gbPayslipMsg.Checked = False
        Me.gbPayslipMsg.CollapseAllExceptThis = False
        Me.gbPayslipMsg.CollapsedHoverImage = Nothing
        Me.gbPayslipMsg.CollapsedNormalImage = Nothing
        Me.gbPayslipMsg.CollapsedPressedImage = Nothing
        Me.gbPayslipMsg.CollapseOnLoad = False
        Me.gbPayslipMsg.Controls.Add(Me.objbtnSearchPeriod)
        Me.gbPayslipMsg.Controls.Add(Me.cboPeriod)
        Me.gbPayslipMsg.Controls.Add(Me.lbleffectivePeriod)
        Me.gbPayslipMsg.Controls.Add(Me.txtMsg4)
        Me.gbPayslipMsg.Controls.Add(Me.txtMsg3)
        Me.gbPayslipMsg.Controls.Add(Me.txtMsg1)
        Me.gbPayslipMsg.Controls.Add(Me.txtMsg2)
        Me.gbPayslipMsg.Controls.Add(Me.lblMsg2)
        Me.gbPayslipMsg.Controls.Add(Me.lblMsg4)
        Me.gbPayslipMsg.Controls.Add(Me.lblMsg3)
        Me.gbPayslipMsg.Controls.Add(Me.lblMsg1)
        Me.gbPayslipMsg.ExpandedHoverImage = Nothing
        Me.gbPayslipMsg.ExpandedNormalImage = Nothing
        Me.gbPayslipMsg.ExpandedPressedImage = Nothing
        Me.gbPayslipMsg.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbPayslipMsg.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbPayslipMsg.HeaderHeight = 25
        Me.gbPayslipMsg.HeaderMessage = ""
        Me.gbPayslipMsg.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbPayslipMsg.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbPayslipMsg.HeightOnCollapse = 0
        Me.gbPayslipMsg.LeftTextSpace = 0
        Me.gbPayslipMsg.Location = New System.Drawing.Point(11, 66)
        Me.gbPayslipMsg.Name = "gbPayslipMsg"
        Me.gbPayslipMsg.OpenHeight = 300
        Me.gbPayslipMsg.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbPayslipMsg.ShowBorder = True
        Me.gbPayslipMsg.ShowCheckBox = False
        Me.gbPayslipMsg.ShowCollapseButton = False
        Me.gbPayslipMsg.ShowDefaultBorderColor = True
        Me.gbPayslipMsg.ShowDownButton = False
        Me.gbPayslipMsg.ShowHeader = True
        Me.gbPayslipMsg.Size = New System.Drawing.Size(465, 178)
        Me.gbPayslipMsg.TabIndex = 0
        Me.gbPayslipMsg.Temp = 0
        Me.gbPayslipMsg.Text = "Payslip Global Message"
        Me.gbPayslipMsg.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtMsg4
        '
        Me.txtMsg4.Flags = 0
        Me.txtMsg4.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtMsg4.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtMsg4.Location = New System.Drawing.Point(130, 147)
        Me.txtMsg4.Name = "txtMsg4"
        Me.txtMsg4.Size = New System.Drawing.Size(322, 21)
        Me.txtMsg4.TabIndex = 3
        '
        'txtMsg3
        '
        Me.txtMsg3.Flags = 0
        Me.txtMsg3.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtMsg3.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtMsg3.Location = New System.Drawing.Point(130, 119)
        Me.txtMsg3.Name = "txtMsg3"
        Me.txtMsg3.Size = New System.Drawing.Size(322, 21)
        Me.txtMsg3.TabIndex = 2
        '
        'txtMsg1
        '
        Me.txtMsg1.Flags = 0
        Me.txtMsg1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtMsg1.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtMsg1.Location = New System.Drawing.Point(130, 63)
        Me.txtMsg1.Name = "txtMsg1"
        Me.txtMsg1.Size = New System.Drawing.Size(322, 21)
        Me.txtMsg1.TabIndex = 0
        '
        'txtMsg2
        '
        Me.txtMsg2.Flags = 0
        Me.txtMsg2.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtMsg2.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtMsg2.Location = New System.Drawing.Point(130, 91)
        Me.txtMsg2.Name = "txtMsg2"
        Me.txtMsg2.Size = New System.Drawing.Size(322, 21)
        Me.txtMsg2.TabIndex = 1
        '
        'lblMsg2
        '
        Me.lblMsg2.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblMsg2.Location = New System.Drawing.Point(8, 94)
        Me.lblMsg2.Name = "lblMsg2"
        Me.lblMsg2.Size = New System.Drawing.Size(110, 15)
        Me.lblMsg2.TabIndex = 269
        Me.lblMsg2.Text = "Global Message 2:"
        '
        'lblMsg4
        '
        Me.lblMsg4.BackColor = System.Drawing.Color.Transparent
        Me.lblMsg4.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblMsg4.Location = New System.Drawing.Point(8, 150)
        Me.lblMsg4.Name = "lblMsg4"
        Me.lblMsg4.Size = New System.Drawing.Size(110, 15)
        Me.lblMsg4.TabIndex = 265
        Me.lblMsg4.Text = "Global Message 4:"
        '
        'lblMsg3
        '
        Me.lblMsg3.BackColor = System.Drawing.Color.Transparent
        Me.lblMsg3.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblMsg3.Location = New System.Drawing.Point(8, 122)
        Me.lblMsg3.Name = "lblMsg3"
        Me.lblMsg3.Size = New System.Drawing.Size(110, 15)
        Me.lblMsg3.TabIndex = 263
        Me.lblMsg3.Text = "Global Message 3:"
        '
        'lblMsg1
        '
        Me.lblMsg1.BackColor = System.Drawing.Color.Transparent
        Me.lblMsg1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblMsg1.Location = New System.Drawing.Point(8, 66)
        Me.lblMsg1.Name = "lblMsg1"
        Me.lblMsg1.Size = New System.Drawing.Size(110, 15)
        Me.lblMsg1.TabIndex = 230
        Me.lblMsg1.Text = "Global Message 1:"
        '
        'objbtnSearchPeriod
        '
        Me.objbtnSearchPeriod.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchPeriod.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchPeriod.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchPeriod.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchPeriod.BorderSelected = False
        Me.objbtnSearchPeriod.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchPeriod.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objbtnSearchPeriod.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchPeriod.Location = New System.Drawing.Point(431, 34)
        Me.objbtnSearchPeriod.Name = "objbtnSearchPeriod"
        Me.objbtnSearchPeriod.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchPeriod.TabIndex = 272
        '
        'cboPeriod
        '
        Me.cboPeriod.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboPeriod.DropDownWidth = 250
        Me.cboPeriod.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboPeriod.FormattingEnabled = True
        Me.cboPeriod.Location = New System.Drawing.Point(130, 34)
        Me.cboPeriod.Name = "cboPeriod"
        Me.cboPeriod.Size = New System.Drawing.Size(295, 21)
        Me.cboPeriod.TabIndex = 271
        '
        'lbleffectivePeriod
        '
        Me.lbleffectivePeriod.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbleffectivePeriod.Location = New System.Drawing.Point(8, 37)
        Me.lbleffectivePeriod.Name = "lbleffectivePeriod"
        Me.lbleffectivePeriod.Size = New System.Drawing.Size(110, 15)
        Me.lbleffectivePeriod.TabIndex = 273
        Me.lbleffectivePeriod.Text = "Effective Period"
        '
        'frmPayslipGlobalMsg
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(487, 305)
        Me.Controls.Add(Me.pnlPayslipGlobalmsg)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmPayslipGlobalMsg"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "ADD/EDIT Payslip Global Message"
        Me.pnlPayslipGlobalmsg.ResumeLayout(False)
        Me.objFooter.ResumeLayout(False)
        Me.gbPayslipMsg.ResumeLayout(False)
        Me.gbPayslipMsg.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents pnlPayslipGlobalmsg As System.Windows.Forms.Panel
    Friend WithEvents eZeeHeader As eZee.Common.eZeeHeader
    Friend WithEvents objFooter As eZee.Common.eZeeFooter
    Friend WithEvents btnSave As eZee.Common.eZeeLightButton
    Friend WithEvents btnClose As eZee.Common.eZeeLightButton
    Friend WithEvents gbPayslipMsg As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents txtMsg4 As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents txtMsg3 As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents txtMsg1 As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents txtMsg2 As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblMsg2 As System.Windows.Forms.Label
    Friend WithEvents lblMsg4 As System.Windows.Forms.Label
    Friend WithEvents lblMsg3 As System.Windows.Forms.Label
    Friend WithEvents lblMsg1 As System.Windows.Forms.Label
    Friend WithEvents objbtnSearchPeriod As eZee.Common.eZeeGradientButton
    Friend WithEvents cboPeriod As System.Windows.Forms.ComboBox
    Friend WithEvents lbleffectivePeriod As System.Windows.Forms.Label
End Class
