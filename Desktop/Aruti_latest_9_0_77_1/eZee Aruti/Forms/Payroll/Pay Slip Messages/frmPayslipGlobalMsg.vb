﻿Option Strict On

Imports eZeeCommonLib
Imports Aruti.Data

Public Class frmPayslipGlobalMsg

#Region " Private Variables "
    Private ReadOnly mstrModuleName As String = "frmPayslipGlobalMsg"
    Private menAction As enAction = enAction.ADD_ONE
    Private mintMessageUnkID As Integer = -1
    Private objPayslipMsg As clsPayslipMessages_master
    'Nilay (28-Apr-2016) -- Start
    'Enhancement : 60.1 - #77 (KBC & Kenya Project Comments List.xls) - For payslip messages, include payroll period so that If you print payslip for last month and you had a new payslip message this month, It does not pick up message of current month but last month.
    Private mblnCancel As Boolean = True
    'Nilay (28-Apr-2016) -- End

#End Region

    'Nilay (28-Apr-2016) -- Start
    'Enhancement : 60.1 - #77 (KBC & Kenya Project Comments List.xls) - For payslip messages, include payroll period so that If you print payslip for last month and you had a new payslip message this month, It does not pick up message of current month but last month.
#Region "Display Dialog"
    Public Function displayDialog(ByVal intMessageunkid As Integer, ByVal eAction As enAction) As Boolean
        Try
            mintMessageUnkID = intMessageunkid
            menAction = eAction
            Me.ShowDialog()

            Return Not mblnCancel
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "displayDialog", mstrModuleName)
        End Try
    End Function
#End Region
    'Nilay (28-Apr-2016) -- End

#Region " Private Methods "

    'Nilay (28-Apr-2016) -- Start
    'Enhancement : 60.1 - #77 (KBC & Kenya Project Comments List.xls) - For payslip messages, include payroll period so that If you print payslip for last month and you had a new payslip message this month, It does not pick up message of current month but last month.
    Private Sub FillCombo()
        Try
            Dim dsCombo As DataSet = Nothing
            Dim objPeriod As New clscommom_period_Tran

            dsCombo = objPeriod.getListForCombo(enModuleReference.Payroll, FinancialYear._Object._YearUnkid, FinancialYear._Object._DatabaseName, _
                                                FinancialYear._Object._Database_Start_Date, "List", True, enStatusType.Open)
            With cboPeriod
                .ValueMember = "periodunkid"
                .DisplayMember = "name"
                .DataSource = dsCombo.Tables("List")
                .SelectedValue = 0
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        End Try
    End Sub
    'Nilay (28-Apr-2016) -- End

    Private Sub SetColor()
        Try
            txtMsg1.BackColor = GUI.ColorOptional
            txtMsg2.BackColor = GUI.ColorOptional
            txtMsg3.BackColor = GUI.ColorOptional
            txtMsg4.BackColor = GUI.ColorOptional
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetColor", mstrModuleName)
        End Try
    End Sub

    Private Sub GetValue()
        Try
            'Nilay (28-Apr-2016) -- Start
            'Enhancement : 60.1 - #77 (KBC & Kenya Project Comments List.xls) - For payslip messages, include payroll period so that If you print payslip for last month and you had a new payslip message this month, It does not pick up message of current month but last month.
            'With objPayslipMsg
            '    txtMsg1.Text = ._Msg1
            '    txtMsg2.Text = ._Msg2
            '    txtMsg3.Text = ._Msg3
            '    txtMsg4.Text = ._Msg4
            'End With
            If menAction = enAction.EDIT_ONE Then
            With objPayslipMsg
                txtMsg1.Text = ._Msg1
                txtMsg2.Text = ._Msg2
                txtMsg3.Text = ._Msg3
                txtMsg4.Text = ._Msg4
                    cboPeriod.SelectedValue = ._Periodunkid
            End With
            End If
            'Nilay (28-Apr-2016) -- End

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetValue", mstrModuleName)
        End Try
    End Sub

    Private Sub SetValue()
        Try
            With objPayslipMsg
                'Nilay (28-Apr-2016) -- Start
                'Enhancement : 60.1 - #77 (KBC & Kenya Project Comments List.xls) - For payslip messages, include payroll period so that If you print payslip for last month and you had a new payslip message this month, It does not pick up message of current month but last month.
                If menAction = enAction.EDIT_ONE Then
                    ._Messageunkid = mintMessageUnkID
                End If
                ._Periodunkid = CInt(cboPeriod.SelectedValue)
                'Nilay (28-Apr-2016) -- End
                ._Msg1 = txtMsg1.Text.Trim
                ._Msg2 = txtMsg2.Text.Trim
                ._Msg3 = txtMsg3.Text.Trim
                ._Msg4 = txtMsg4.Text.Trim
                ._Isglobalmessage = True
                ._Userunkid = User._Object._Userunkid 'Sohail (14 Oct 2010)
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetValue", mstrModuleName)
        End Try
    End Sub

    Private Sub Get_List()
        Dim dsList As DataSet
        Try
            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'dsList = objPayslipMsg.GetList("Msg", True, 0, 0, 0, 0, 0, 0, 0, 0, 0, True)
            dsList = objPayslipMsg.GetList(FinancialYear._Object._DatabaseName, _
                                           User._Object._Userunkid, _
                                           FinancialYear._Object._YearUnkid, _
                                           Company._Object._Companyunkid, _
                                           eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                           eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                           ConfigParameter._Object._UserAccessModeSetting, True, _
                                           ConfigParameter._Object._IsIncludeInactiveEmp, "Msg", True, 0, 0, 0, 0, 0, 0, 0, 0, 0, True)
            'S.SANDEEP [04 JUN 2015] -- END

            If dsList.Tables("Msg").Rows.Count > 0 Then
                mintMessageUnkID = CInt(dsList.Tables("Msg").Rows(0).Item("messageunkid").ToString)
                menAction = enAction.EDIT_ONE
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Get_List", mstrModuleName)
        End Try
    End Sub

    'Nilay (28-Apr-2016) -- Start
    'Enhancement : 60.1 - #77 (KBC & Kenya Project Comments List.xls) - For payslip messages, include payroll period so that If you print payslip for last month and you had a new payslip message this month, It does not pick up message of current month but last month.
    Private Function IsValidate() As Boolean
        Try
            If CInt(cboPeriod.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Period is mandatory information. Please select effective period."), enMsgBoxStyle.Information)
                cboPeriod.Focus()
                Return False
            ElseIf txtMsg1.Text.Trim.Length <= 0 AndAlso (txtMsg2.Text.Trim.Length > 0 OrElse txtMsg3.Text.Trim.Length > 0 OrElse txtMsg4.Text.Trim.Length > 0) Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Please enter Global Message 1. Global Message 1 is mandatory information."), enMsgBoxStyle.Information)
                txtMsg1.Focus()
                Exit Function
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "IsValidate", mstrModuleName)
        End Try
        Return True
    End Function
    'Nilay (28-Apr-2016) -- End

#End Region

#Region " Form's Events "

    Private Sub frmPayslipGlobalMsg_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        Try
            objPayslipMsg = Nothing
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmPayslipGlobalMsg_FormClosed", mstrModuleName)
        End Try
    End Sub

    Private Sub frmPayslipGlobalMsg_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown
        Try
            Select Case e.KeyCode
                Case Keys.Return
                    Windows.Forms.SendKeys.Send("{Tab}")
                    e.Handled = True
                Case Keys.S
                    If e.Control = True Then
                        Call btnSave.PerformClick()
                    End If
            End Select
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmPayslipGlobalMsg_KeyDown", mstrModuleName)
        End Try
    End Sub

    Private Sub frmPayslipGlobalMsg_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        objPayslipMsg = New clsPayslipMessages_master
        Try
            Call Set_Logo(Me, gApplicationType)
            Language.setLanguage(Me.Name)

            'Anjan (02 Sep 2011)-Start
            'Issue : Including Language Settings.
            Call OtherSettings()
            'Anjan (02 Sep 2011)-End 

            Call SetColor()
            'Nilay (28-Apr-2016) -- Start
            'Enhancement : 60.1 - #77 (KBC & Kenya Project Comments List.xls) - For payslip messages, include payroll period so that If you print payslip for last month and you had a new payslip message this month, It does not pick up message of current month but last month.
            'Call Get_List()
            Call FillCombo()
            'Nilay (28-Apr-2016) -- End

            If menAction = enAction.EDIT_ONE Then
                objPayslipMsg._Messageunkid = mintMessageUnkID
                'Nilay (28-Apr-2016) -- Start
                'Enhancement : 60.1 - #77 (KBC & Kenya Project Comments List.xls) - For payslip messages, include payroll period so that If you print payslip for last month and you had a new payslip message this month, It does not pick up message of current month but last month.
                cboPeriod.Enabled = False
                objbtnSearchPeriod.Enabled = False
                'Nilay (28-Apr-2016) -- End
            End If
            Call GetValue()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmPayslipGlobalMsg_Load", mstrModuleName)
        End Try
    End Sub
    'Anjan (02 Sep 2011)-Start
    'Issue : Including Language Settings.
    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsPayslipMessages_master.SetMessages()
            objfrm._Other_ModuleNames = "clsPayslipMessages_master"
            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub
    'Anjan (02 Sep 2011)-End 

#End Region

#Region " Button's Events "
    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnClose_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Dim blnFlag As Boolean
        Try
            'Nilay (28-Apr-2016) -- Start
            'Enhancement : 60.1 - #77 (KBC & Kenya Project Comments List.xls) - For payslip messages, include payroll period so that If you print payslip for last month and you had a new payslip message this month, It does not pick up message of current month but last month.
            If IsValidate() = False Then Exit Sub
            'Nilay (28-Apr-2016) -- End
            
            Call SetValue()

            'S.SANDEEP [28-May-2018] -- START
            'ISSUE/ENHANCEMENT : {Audit Trails} 
            objPayslipMsg._FormName = mstrModuleName
            objPayslipMsg._LoginEmployeeunkid = 0
            objPayslipMsg._ClientIP = getIP()
            objPayslipMsg._HostName = getHostName()
            objPayslipMsg._FromWeb = False
            objPayslipMsg._AuditUserId = User._Object._Userunkid
objPayslipMsg._CompanyUnkid = Company._Object._Companyunkid
            objPayslipMsg._AuditDate = ConfigParameter._Object._CurrentDateAndTime
            'S.SANDEEP [28-May-2018] -- END

            If menAction = enAction.EDIT_ONE Then
                blnFlag = objPayslipMsg.Update()
            Else
                blnFlag = objPayslipMsg.Insert()
            End If

            'Nilay (28-Apr-2016) -- Start
            'Enhancement : 60.1 - #77 (KBC & Kenya Project Comments List.xls) - For payslip messages, include payroll period so that If you print payslip for last month and you had a new payslip message this month, It does not pick up message of current month but last month.
            If blnFlag = False AndAlso objPayslipMsg._Message <> "" Then
                eZeeMsgBox.Show(objPayslipMsg._Message, enMsgBoxStyle.Information)
                Exit Sub
            End If
            'Nilay (28-Apr-2016) -- End

            If blnFlag Then
                'Nilay (28-Apr-2016) -- Start
                'Enhancement : 60.1 - #77 (KBC & Kenya Project Comments List.xls) - For payslip messages, include payroll period so that If you print payslip for last month and you had a new payslip message this month, It does not pick up message of current month but last month.
                mblnCancel = False
                'Nilay (28-Apr-2016) -- End

                If menAction = enAction.ADD_CONTINUE Then
                    objPayslipMsg = Nothing
                    objPayslipMsg = New clsPayslipMessages_master
                    Call GetValue()
                    txtMsg1.Focus()
                Else
                    mintMessageUnkID = objPayslipMsg._Messageunkid
                    Me.Close()
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnSave_Click", mstrModuleName)
        End Try
    End Sub

    'Nilay (28-Apr-2016) -- Start
    'Enhancement : 60.1 - #77 (KBC & Kenya Project Comments List.xls) - For payslip messages, include payroll period so that If you print payslip for last month and you had a new payslip message this month, It does not pick up message of current month but last month.
    Private Sub objbtnSearchPeriod_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchPeriod.Click
        Dim frm As New frmCommonSearch
        Try
            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If

            With frm
                .ValueMember = cboPeriod.ValueMember
                .DisplayMember = cboPeriod.DisplayMember
                .CodeMember = "code"
                .DataSource = CType(cboPeriod.DataSource, DataTable)
                If .DisplayDialog Then
                    cboPeriod.SelectedValue = .SelectedValue
                    cboPeriod.Focus()
                End If
            End With
        Catch ex As Exception

        End Try
    End Sub
    'Nilay (28-Apr-2016) -- End

#End Region

#Region " Message List "

#End Region
   
    

	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Private Sub OtherSettings()
		Try
			Me.SuspendLayout()
			
			Call SetLanguage()
			
			Me.gbPayslipMsg.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbPayslipMsg.ForeColor = GUI._eZeeContainerHeaderForeColor 


            Me.eZeeHeader.GradientColor1 = GUI._HeaderBackColor1
			Me.eZeeHeader.GradientColor2 = GUI._HeaderBackColor2 
			Me.eZeeHeader.BorderColor = GUI._HeaderBorderColor 
			Me.eZeeHeader.HeaderTextForeColor = GUI._HeaderTitleFontColor 
			Me.eZeeHeader.DescriptionForeColor = GUI._HeaderMessageFontColor 


			Me.btnSave.GradientBackColor = GUI._ButttonBackColor 
			Me.btnSave.GradientForeColor = GUI._ButttonFontColor

			Me.btnClose.GradientBackColor = GUI._ButttonBackColor 
			Me.btnClose.GradientForeColor = GUI._ButttonFontColor


			Me.ResumeLayout()
		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "OtherSettings", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetLanguage()
		Try
			Me.Text = Language._Object.getCaption(Me.Name, Me.Text)
			
			Me.eZeeHeader.Title = Language._Object.getCaption(Me.eZeeHeader.Name & "_Title" , Me.eZeeHeader.Title)
			Me.eZeeHeader.Message = Language._Object.getCaption(Me.eZeeHeader.Name & "_Message" , Me.eZeeHeader.Message)
			Me.btnSave.Text = Language._Object.getCaption(Me.btnSave.Name, Me.btnSave.Text)
			Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
			Me.gbPayslipMsg.Text = Language._Object.getCaption(Me.gbPayslipMsg.Name, Me.gbPayslipMsg.Text)
			Me.lblMsg2.Text = Language._Object.getCaption(Me.lblMsg2.Name, Me.lblMsg2.Text)
			Me.lblMsg4.Text = Language._Object.getCaption(Me.lblMsg4.Name, Me.lblMsg4.Text)
			Me.lblMsg3.Text = Language._Object.getCaption(Me.lblMsg3.Name, Me.lblMsg3.Text)
			Me.lblMsg1.Text = Language._Object.getCaption(Me.lblMsg1.Name, Me.lblMsg1.Text)
            Me.lbleffectivePeriod.Text = Language._Object.getCaption(Me.lbleffectivePeriod.Name, Me.lbleffectivePeriod.Text)

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetLanguage", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetMessages()
		Try
            Language.setMessage(mstrModuleName, 1, "Period is mandatory information. Please select effective period.")
            Language.setMessage(mstrModuleName, 2, "Please enter Global Message 1. Global Message 1 is mandatory information.")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class