﻿Option Strict On

Imports eZeeCommonLib
Imports Aruti.Data

Public Class frmPayslipGlobalMsgList

#Region "Private Variables"
    Private ReadOnly mstrModuleName As String = "frmPayslipGlobalMsgList"
    Private menAction As enAction = enAction.ADD_ONE
    Private objPayslipMsg As clsPayslipMessages_master
#End Region

#Region "Form's Events"
    Private Sub frmPayslipGlobalMsgList_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        Try
            objPayslipMsg = Nothing
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmPayslipGlobalMsgList_FormClosed", mstrModuleName)
        End Try
    End Sub

    Private Sub frmPayslipGlobalMsgList_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown
        Try
            Select Case e.KeyCode
                Case Keys.Escape
                    Me.Close()
                Case Keys.Delete
                    btnDelete.PerformClick()
                Case Keys.Return
                    SendKeys.Send("{TAB}")
            End Select
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmPayslipGlobalMsgList_KeyDown", mstrModuleName)
        End Try
    End Sub

    Private Sub frmPayslipGlobalMsgList_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        objPayslipMsg = New clsPayslipMessages_master
        Try
            Call Set_Logo(Me, gApplicationType)
            Language.setLanguage(Me.Name)

            'Varsha Rana (17-Oct-2017) -- Start
            'Enhancement - Give user privileges in payroll module.
            Call SetVisibility()
            'Varsha Rana (17-Oct-2017) -- End


            Call OtherSettings()
            Call FillCombo()
            Call FillList()

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmPayslipGlobalMsgList_Load", mstrModuleName)
        End Try
    End Sub

    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsPayslipMessages_master.SetMessages()
            objfrm._Other_ModuleNames = "clsPayslipMessages_master"
            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub
#End Region

#Region "Private Methods"

    Private Sub FillCombo()
        Try
            Dim dsCombo As DataSet = Nothing
            Dim objPeriod As New clscommom_period_Tran

            dsCombo = objPeriod.getListForCombo(enModuleReference.Payroll, FinancialYear._Object._YearUnkid, FinancialYear._Object._DatabaseName, _
                                                FinancialYear._Object._Database_Start_Date, "List", True)
            With cboPeriod
                .ValueMember = "periodunkid"
                .DisplayMember = "name"
                .DataSource = dsCombo.Tables("List")
                .SelectedValue = 0
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        End Try
    End Sub

    Private Sub FillList()
        Dim dsList As DataSet = Nothing
        Try
            If User._Object.Privilege._AllowViewGlobalPayslipMessage = False Then Exit Sub

            dsList = objPayslipMsg.GetList(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, _
                                           eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                           eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                           ConfigParameter._Object._UserAccessModeSetting, True, _
                                           ConfigParameter._Object._IsIncludeInactiveEmp, "List", True, , , , , , , , , , , , _
                                           CInt(cboPeriod.SelectedValue))

            dgvGlobalMsgList.AutoGenerateColumns = False
            dgcolhEffectivePeriod.DataPropertyName = "period_name"
            dgcolhGlobalMsg1.DataPropertyName = "msg1"
            dgcolhGlobalMsg2.DataPropertyName = "msg2"
            dgcolhGlobalMsg3.DataPropertyName = "msg3"
            dgcolhGlobalMsg4.DataPropertyName = "msg4"
            objdgcolhperiodunkid.DataPropertyName = "periodunkid"
            objdgcolhmessageunkid.DataPropertyName = "messageunkid"
            objdgcolhstatusid.DataPropertyName = "statusid"

            dgvGlobalMsgList.DataSource = dsList.Tables("List")

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillList", mstrModuleName)
        Finally
            Call objbtnSearch.ShowResult(CStr(dgvGlobalMsgList.RowCount.ToString))
        End Try
    End Sub


    'Varsha Rana (17-Oct-2017) -- Start
    'Enhancement - Give user privileges in payroll module.
    Private Sub SetVisibility()

        Try
            btnNew.Enabled = User._Object.Privilege._AllowAddGlobalPayslipMessage
            btnEdit.Enabled = User._Object.Privilege._AllowEditGlobalPayslipMessage
            btnDelete.Enabled = User._Object.Privilege._AllowDeleteGlobalPayslipMessage

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetVisibility", mstrModuleName)
        End Try

    End Sub
    'Varsha Rana (17-Oct-2017) -- End


#End Region

#Region "Button's Events"

    Private Sub objbtnSearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearch.Click
        Try
            Call FillList()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearch_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnReset.Click
        Try
            If CInt(cboPeriod.SelectedValue) > 0 Then
                cboPeriod.SelectedValue = 0
                Call FillList()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnReset_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnSearchPeriod_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchPeriod.Click
        Dim frm As New frmCommonSearch
        Try
            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If

            With frm
                .ValueMember = cboPeriod.ValueMember
                .DisplayMember = cboPeriod.DisplayMember
                .CodeMember = "code"
                .DataSource = CType(cboPeriod.DataSource, DataTable)
                If .DisplayDialog Then
                    cboPeriod.SelectedValue = .SelectedValue
                    cboPeriod.Focus()
                End If
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchPeriod_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnClose_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnNew_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnNew.Click
        Dim objFrm As New frmPayslipGlobalMsg
        Try
            If User._Object.Privilege._AllowAddGlobalPayslipMessage = False Then Exit Sub

            If User._Object._Isrighttoleft = True Then
                objFrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objFrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objFrm)
            End If

            If objFrm.displayDialog(-1, enAction.ADD_ONE) Then
                cboPeriod.SelectedValue = 0
                Call FillList()
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnNew_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnEdit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnEdit.Click
        Dim objFrm As New frmPayslipGlobalMsg
        Try
            If User._Object.Privilege._AllowEditGlobalPayslipMessage = False Then Exit Sub

            If User._Object._Isrighttoleft = True Then
                objFrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objFrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objFrm)
            End If

            If dgvGlobalMsgList.RowCount <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please select Payslip Global Message from the list to perform further operation."), enMsgBoxStyle.Information)
                dgvGlobalMsgList.Focus()
                Exit Sub
            End If

            If CInt(dgvGlobalMsgList.SelectedRows(0).Cells(objdgcolhstatusid.Index).Value) = enStatusType.Close Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Sorry, you cannot Edit/Delete this Payslip Global Message. Reason: Period of selected message is closed."), enMsgBoxStyle.Information)
                dgvGlobalMsgList.Focus()
                Exit Sub
            End If

            If objFrm.displayDialog(CInt(dgvGlobalMsgList.SelectedRows(0).Cells(objdgcolhmessageunkid.Index).Value), enAction.EDIT_ONE) Then
                Call FillList()
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnEdit_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDelete.Click
        Dim blnFlag As Boolean = True
        Try
            If User._Object.Privilege._AllowDeleteGlobalPayslipMessage = False Then Exit Sub

            If dgvGlobalMsgList.RowCount <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please select Payslip Global Message from the list to perform further operation."), enMsgBoxStyle.Information)
                dgvGlobalMsgList.Focus()
                Exit Sub
            End If

            If CInt(dgvGlobalMsgList.SelectedRows(0).Cells(objdgcolhstatusid.Index).Value) = 2 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Sorry, you cannot Edit/Delete this Payslip Global Message. Reason: Period of selected message is closed."), enMsgBoxStyle.Information)
                dgvGlobalMsgList.Focus()
                Exit Sub
            End If

            If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Are you sure you want to Delete this Payslip Global Message?"), _
                               CType(enMsgBoxStyle.Question + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.Yes Then


                'S.SANDEEP [28-May-2018] -- START
                'ISSUE/ENHANCEMENT : {Audit Trails} 
                objPayslipMsg._FormName = mstrModuleName
                objPayslipMsg._LoginEmployeeunkid = 0
                objPayslipMsg._ClientIP = getIP()
                objPayslipMsg._HostName = getHostName()
                objPayslipMsg._FromWeb = False
                objPayslipMsg._AuditUserId = User._Object._Userunkid
objPayslipMsg._CompanyUnkid = Company._Object._Companyunkid
                objPayslipMsg._AuditDate = ConfigParameter._Object._CurrentDateAndTime
                'S.SANDEEP [28-May-2018] -- END

                blnFlag = objPayslipMsg.Delete(CInt(dgvGlobalMsgList.SelectedRows(0).Cells(objdgcolhmessageunkid.Index).Value))
                If blnFlag = False AndAlso objPayslipMsg._Message <> "" Then
                    eZeeMsgBox.Show(objPayslipMsg._Message, enMsgBoxStyle.Information)
                    Exit Sub
                End If
                Call FillList()
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnDelete_Click", mstrModuleName)
        End Try
    End Sub

#End Region

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Private Sub OtherSettings()
        Try
            Me.SuspendLayout()
            
            Call SetLanguage()

            Me.gbFilterCriteria.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbFilterCriteria.ForeColor = GUI._eZeeContainerHeaderForeColor


            Me.eZeeHeader.GradientColor1 = GUI._HeaderBackColor1
            Me.eZeeHeader.GradientColor2 = GUI._HeaderBackColor2
            Me.eZeeHeader.BorderColor = GUI._HeaderBorderColor
            Me.eZeeHeader.HeaderTextForeColor = GUI._HeaderTitleFontColor
            Me.eZeeHeader.DescriptionForeColor = GUI._HeaderMessageFontColor


            Me.btnDelete.GradientBackColor = GUI._ButttonBackColor
            Me.btnDelete.GradientForeColor = GUI._ButttonFontColor

            Me.btnEdit.GradientBackColor = GUI._ButttonBackColor
            Me.btnEdit.GradientForeColor = GUI._ButttonFontColor

            Me.btnNew.GradientBackColor = GUI._ButttonBackColor
            Me.btnNew.GradientForeColor = GUI._ButttonFontColor

            Me.btnClose.GradientBackColor = GUI._ButttonBackColor
            Me.btnClose.GradientForeColor = GUI._ButttonFontColor


            Me.ResumeLayout()
        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "OtherSettings", mstrModuleName)
        End Try
    End Sub


    Private Sub SetLanguage()
        Try
            Me.Text = Language._Object.getCaption(Me.Name, Me.Text)

            Me.eZeeHeader.Title = Language._Object.getCaption(Me.eZeeHeader.Name & "_Title", Me.eZeeHeader.Title)
            Me.eZeeHeader.Message = Language._Object.getCaption(Me.eZeeHeader.Name & "_Message", Me.eZeeHeader.Message)
            Me.gbFilterCriteria.Text = Language._Object.getCaption(Me.gbFilterCriteria.Name, Me.gbFilterCriteria.Text)
            Me.lblAsOnPeriod.Text = Language._Object.getCaption(Me.lblAsOnPeriod.Name, Me.lblAsOnPeriod.Text)
            Me.btnDelete.Text = Language._Object.getCaption(Me.btnDelete.Name, Me.btnDelete.Text)
            Me.btnEdit.Text = Language._Object.getCaption(Me.btnEdit.Name, Me.btnEdit.Text)
            Me.btnNew.Text = Language._Object.getCaption(Me.btnNew.Name, Me.btnNew.Text)
            Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
            Me.DataGridViewTextBoxColumn1.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn1.Name, Me.DataGridViewTextBoxColumn1.HeaderText)
            Me.DataGridViewTextBoxColumn2.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn2.Name, Me.DataGridViewTextBoxColumn2.HeaderText)
            Me.DataGridViewTextBoxColumn3.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn3.Name, Me.DataGridViewTextBoxColumn3.HeaderText)
            Me.DataGridViewTextBoxColumn4.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn4.Name, Me.DataGridViewTextBoxColumn4.HeaderText)
            Me.DataGridViewTextBoxColumn5.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn5.Name, Me.DataGridViewTextBoxColumn5.HeaderText)
            Me.DataGridViewTextBoxColumn6.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn6.Name, Me.DataGridViewTextBoxColumn6.HeaderText)
            Me.DataGridViewTextBoxColumn7.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn7.Name, Me.DataGridViewTextBoxColumn7.HeaderText)
            Me.dgcolhEffectivePeriod.HeaderText = Language._Object.getCaption(Me.dgcolhEffectivePeriod.Name, Me.dgcolhEffectivePeriod.HeaderText)
            Me.dgcolhGlobalMsg1.HeaderText = Language._Object.getCaption(Me.dgcolhGlobalMsg1.Name, Me.dgcolhGlobalMsg1.HeaderText)
            Me.dgcolhGlobalMsg2.HeaderText = Language._Object.getCaption(Me.dgcolhGlobalMsg2.Name, Me.dgcolhGlobalMsg2.HeaderText)
            Me.dgcolhGlobalMsg3.HeaderText = Language._Object.getCaption(Me.dgcolhGlobalMsg3.Name, Me.dgcolhGlobalMsg3.HeaderText)
            Me.dgcolhGlobalMsg4.HeaderText = Language._Object.getCaption(Me.dgcolhGlobalMsg4.Name, Me.dgcolhGlobalMsg4.HeaderText)
            Me.DataGridViewTextBoxColumn8.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn8.Name, Me.DataGridViewTextBoxColumn8.HeaderText)

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
        End Try
    End Sub


    Private Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Please select Payslip Global Message from the list to perform further operation.")
            Language.setMessage(mstrModuleName, 2, "Are you sure you want to Delete this Payslip Global Message?")
            Language.setMessage(mstrModuleName, 3, "Sorry, you cannot Edit/Delete this Payslip Global Message. Reason: Period of selected message is closed.")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class