﻿Option Strict On

Imports eZeeCommonLib
Imports Aruti.Data

Public Class frmEmpDistributedCostCenterList

#Region " Private Varaibles "
    Private ReadOnly mstrModuleName As String = "frmEmpDistributedCostCenterList"
    Private objEmpCostCenter As clsemployee_costcenter_Tran
    'Private mstrSalaryHeadIDs As String 'Sohail (29 Mar 2017)

    'Sohail (29 Mar 2017) -- Start
    'CCBRT Enhancement - 66.1 - Employee cost center distribution for non-salary heads, Effective period option on Employee Cost Center.
    Private mdtPeriod_startdate As DateTime
    Private mdtPeriod_enddate As DateTime
    'Sohail (29 Mar 2017) -- End
    'Sohail (13 Jan 2022) -- Start
    'NMB Enhancement # OLD-101 : - Give Checkbox selection option on Employee Cost Center Screen.
    Private mdtView As DataView
    'Sohail (13 Jan 2022) -- End

#End Region

#Region " Private Functions "
    Private Sub SetColor()
        Try
            cboEmployee.BackColor = GUI.ColorOptional
            cboTranHead.BackColor = GUI.ColorOptional
            cboCostCenter.BackColor = GUI.ColorOptional
            txtPercFrom.BackColor = GUI.ColorOptional
            txtPercTo.BackColor = GUI.ColorOptional
            cboPeriod.BackColor = GUI.ColorComp 'Sohail (29 Mar 2017)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetColor", mstrModuleName)
        End Try
    End Sub

    Private Sub FillList()
        Dim dsList As New DataSet
        Dim StrFilter As String = String.Empty
        Dim dtTable As New DataTable
        'Hemant (11 Mar 2018) -- Start
        'ISSUE : performance issue on emp cost centre list screen, taking 30 to 45 mins to open
        Cursor.Current = Cursors.WaitCursor
        'Hemant (11 Mar 2018) -- End
        Try

            If User._Object.Privilege._AllowToViewEmpDistributedCostCenterList = True Then                'Pinkal (02-Jul-2012) -- Start

                'Sohail (21 Aug 2015) -- Start
                'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                'dsList = objEmpBankTran.GetList("EmpCC")
                'Sohail (21 Aug 2015) -- End

                'Sohail (11 Sep 2015) -- Start
                'Enhancement - Changes in iTax Form B Report for Kenya as per comment of Rutta in email Sub.:Aga Khan Foundation - Mandatory iTax Filing on 08-Sep-2015.
                'StrFilter = "tranheadunkid IN (" & mstrSalaryHeadIDs & ") "
                'Sohail (29 Mar 2017) -- Start
                'CCBRT Enhancement - 66.1 - Employee cost center distribution for non-salary heads, Effective period option on Employee Cost Center.
                'StrFilter = " premployee_costcenter_tran.tranheadunkid IN (" & mstrSalaryHeadIDs & ") "
                'Hemant (11 Mar 2018) -- Start
                'ISSUE : payable heads not coming on emp cost centre list after save
                'StrFilter = " prtranhead_master.trnheadtype_id <> " & enTranHeadType.Informational & " "
                StrFilter = " 1 = 1 "
                'Hemant (11 Mar 2018) -- End
                'Sohail (29 Mar 2017) -- End
                'Sohail (11 Sep 2015) -- End
                If CInt(cboEmployee.SelectedValue) > 0 Then
                    'Sohail (21 Aug 2015) -- Start
                    'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                    'StrFilter &= "AND employeeunkid = " & CInt(cboEmployee.SelectedValue) & " "
                    StrFilter &= "AND premployee_costcenter_tran.employeeunkid = " & CInt(cboEmployee.SelectedValue) & " "
                    'Sohail (21 Aug 2015) -- End
                End If

                If CInt(cboTranHead.SelectedValue) > 0 Then
                    'Sohail (21 Aug 2015) -- Start
                    'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                    'StrFilter &= "AND tranheadunkid = " & CInt(cboTranHead.SelectedValue) & " "
                    StrFilter &= "AND premployee_costcenter_tran.tranheadunkid = " & CInt(cboTranHead.SelectedValue) & " "
                    'Sohail (21 Aug 2015) -- End
                End If

                'Sohail (07 Feb 2019) -- Start
                'NMB Enhancement - 76.1 - Employee cost centre should be distributed on all allocations instead of only on cost centre.
                If CInt(cboAllocation.SelectedValue) > 0 Then
                    StrFilter &= "AND premployee_costcenter_tran.allocationbyid = " & CInt(cboAllocation.SelectedValue) & " "
                End If
                'Sohail (07 Feb 2019) -- End

                If CInt(cboCostCenter.SelectedValue) > 0 Then
                    'Sohail (24 Apr 2018) -- Start
                    'Internal Issue : Ambiguise column name costcenterunkid in 71.1
                    'StrFilter &= "AND costcenterunkid = " & CInt(cboCostCenter.SelectedValue) & " "
                    StrFilter &= "AND premployee_costcenter_tran.costcenterunkid = " & CInt(cboCostCenter.SelectedValue) & " "
                    'Sohail (24 Apr 2018) -- End
                End If

                If txtPercFrom.Decimal > 0 Then
                    StrFilter &= "AND percentage >= " & txtPercFrom.Decimal & " "
                End If

                If txtPercTo.Decimal > 0 Then
                    StrFilter &= "AND percentage <= " & txtPercTo.Decimal & " "
                End If

                'Sohail (21 Aug 2015) -- Start
                'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                'dtTable = New DataView(dsList.Tables(0), StrFilter, "employeeunkid", DataViewRowState.CurrentRows).ToTable
                'Sohail (29 Mar 2017) -- Start
                'CCBRT Enhancement - 66.1 - Employee cost center distribution for non-salary heads, Effective period option on Employee Cost Center.
                'dsList = objEmpBankTran.GetList(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), ConfigParameter._Object._UserAccessModeSetting, True, ConfigParameter._Object._IsIncludeInactiveEmp, "EmpCC", True, , , StrFilter)
                Dim xPeriodStart As Date
                Dim xPeriodEnd As Date
                If CInt(cboPeriod.SelectedValue) > 0 Then
                    xPeriodStart = mdtPeriod_startdate
                    xPeriodEnd = mdtPeriod_enddate
                Else
                    xPeriodStart = eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate)
                    xPeriodEnd = eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate)
                End If
                If chkGroupByEmpName.Checked = False Then
                    'dsList = objEmpBankTran.GetList(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, xPeriodStart, xPeriodEnd, ConfigParameter._Object._UserAccessModeSetting, True, ConfigParameter._Object._IsIncludeInactiveEmp, "EmpCC", True, , , StrFilter, mdtPeriod_enddate, "costcentername, employeename, trnheadname, end_date DESC")
                    'Sohail (07 Feb 2019) -- Start
                    'NMB Enhancement - 76.1 - Employee cost centre should be distributed on all allocations instead of only on cost centre.
                    'dsList = objEmpBankTran.GetList(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, xPeriodStart, xPeriodEnd, ConfigParameter._Object._UserAccessModeSetting, True, ConfigParameter._Object._IsIncludeInactiveEmp, "EmpCC", True, , , StrFilter, mdtPeriod_enddate, "costcentername")
                    'Hemant (11 Mar 2018) -- Start
                    'ISSUE : performance issue on emp cost centre list screen, taking 30 to 45 mins to open
                    'dsList = objEmpBankTran.GetList(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, xPeriodStart, xPeriodEnd, ConfigParameter._Object._UserAccessModeSetting, True, ConfigParameter._Object._IsIncludeInactiveEmp, "EmpCC", True, , , StrFilter, mdtPeriod_enddate, "AllocationByName, AllocationName")


                    'Gajanan [24-Aug-2020] -- Start
                    'NMB Enhancement : Allow to set account configuration mapping 
                    'inactive for closed period to allow to map head on other account 
                    'configuration screen from new period
                    'dsList = objEmpBankTran.GetList(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, xPeriodStart, xPeriodEnd, ConfigParameter._Object._UserAccessModeSetting, True, ConfigParameter._Object._IsIncludeInactiveEmp, "EmpCC", True, , , StrFilter, mdtPeriod_enddate, "AllocationByName, AllocationName", "", True)
                    dsList = objEmpCostCenter.GetList(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, xPeriodStart, xPeriodEnd, ConfigParameter._Object._UserAccessModeSetting, True, ConfigParameter._Object._IsIncludeInactiveEmp, "EmpCC", True, , , StrFilter, mdtPeriod_enddate, "AllocationByName, AllocationName", "", True, CInt(cboActiveInactive.SelectedValue))
                    'Gajanan [24-Aug-2020] -- End

                    'Hemant (11 Mar 2018) -- End
                    'Sohail (07 Feb 2019) -- End
                Else
                    'dsList = objEmpBankTran.GetList(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, xPeriodStart, xPeriodEnd, ConfigParameter._Object._UserAccessModeSetting, True, ConfigParameter._Object._IsIncludeInactiveEmp, "EmpCC", True, , , StrFilter, mdtPeriod_enddate, "employeename, end_date DESC, costcentername, trnheadname")
                    'Hemant (11 Mar 2018) -- Start
                    'ISSUE : performance issue on emp cost centre list screen, taking 30 to 45 mins to open
                    'dsList = objEmpBankTran.GetList(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, xPeriodStart, xPeriodEnd, ConfigParameter._Object._UserAccessModeSetting, True, ConfigParameter._Object._IsIncludeInactiveEmp, "EmpCC", True, , , StrFilter, mdtPeriod_enddate, "")
                    'dsList = objEmpBankTran.GetList(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, xPeriodStart, xPeriodEnd, ConfigParameter._Object._UserAccessModeSetting, True, ConfigParameter._Object._IsIncludeInactiveEmp, "EmpCC", True, , , StrFilter, mdtPeriod_enddate, "", "", True)


                    'Gajanan [24-Aug-2020] -- Start
                    'NMB Enhancement : Allow to set account configuration mapping 
                    'inactive for closed period to allow to map head on other account 
                    'configuration screen from new period
                    dsList = objEmpCostCenter.GetList(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, xPeriodStart, xPeriodEnd, ConfigParameter._Object._UserAccessModeSetting, True, ConfigParameter._Object._IsIncludeInactiveEmp, "EmpCC", True, , , StrFilter, mdtPeriod_enddate, "", "", True, CInt(cboActiveInactive.SelectedValue))
                    'Gajanan [24-Aug-2020] -- End

                    'Hemant (11 Mar 2018) -- End
                End If
                'Sohail (29 Mar 2017) -- End
                dtTable = New DataView(dsList.Tables(0)).ToTable
                'Sohail (21 Aug 2015) -- End

                'Sohail (13 Jan 2022) -- Start
                'NMB Enhancement # OLD-101 : - Give Checkbox selection option on Employee Cost Center Screen.
                mdtView = dsList.Tables(0).DefaultView
                'Sohail (13 Jan 2022) -- End

                'Hemant (11 Mar 2018) -- Start
                'ISSUE : performance issue on emp cost centre list screen, taking 30 to 45 mins to open
                'Dim lvItem As ListViewItem

                'lvEmpBankList.Items.Clear()
                'Dim lvArray As New List(Of ListViewItem)
                'lvEmpBankList.BeginUpdate()

                'For Each dtRow As DataRow In dtTable.Rows
                '    lvItem = New ListViewItem

                '    lvItem.Text = dtRow.Item("costcentertranunkid").ToString
                '    lvItem.Tag = dtRow.Item("costcentertranunkid")

                '    'Sohail (28 Jan 2012) -- Start
                '    'TRA - ENHANCEMENT
                '    lvItem.SubItems.Add(dtRow.Item("employeecode").ToString)
                '    'Sohail (28 Jan 2012) -- End

                '    lvItem.SubItems.Add(dtRow.Item("employeename").ToString)
                '    lvItem.SubItems(colhEmpName.Index).Tag = CInt(dtRow.Item("employeeunkid"))

                '    lvItem.SubItems.Add(dtRow.Item("trnheadname").ToString)
                '    lvItem.SubItems(colhTranHead.Index).Tag = CInt(dtRow.Item("tranheadunkid"))

                '    'Sohail (07 Feb 2019) -- Start
                '    'NMB Enhancement - 76.1 - Employee cost centre should be distributed on all allocations instead of only on cost centre.
                '    'lvItem.SubItems.Add(dtRow.Item("costcenterName").ToString)
                '    lvItem.SubItems.Add(dtRow.Item("AllocationByName").ToString & " : " & dtRow.Item("AllocationName").ToString)
                '    'Sohail (07 Feb 2019) -- End
                '    lvItem.SubItems(colhCostCenter.Index).Tag = CInt(dtRow.Item("costcenterunkid"))

                '    'Sohail (29 Mar 2017) -- Start
                '    'CCBRT Enhancement - 66.1 - Employee cost center distribution for non-salary heads, Effective period option on Employee Cost Center.
                '    lvItem.SubItems.Add(dtRow.Item("period_name").ToString)
                '    lvItem.SubItems(colhPeriod.Index).Tag = CInt(dtRow.Item("periodunkid"))
                '    'Sohail (29 Mar 2017) -- End

                '    lvItem.SubItems.Add(Format(dtRow.Item("percentage"), GUI.fmtCurrency))

                '    'Sohail (29 Mar 2017) -- Start
                '    'CCBRT Enhancement - 66.1 - Employee cost center distribution for non-salary heads, Effective period option on Employee Cost Center.
                '    lvItem.SubItems.Add(dtRow.Item("statusid").ToString)
                '    'Sohail (29 Mar 2017) -- End

                '    lvArray.Add(lvItem)
                'Next
                'lvEmpBankList.Items.AddRange(lvArray.ToArray)
                'lvEmpBankList.EndUpdate()

                ''Sohail (29 Mar 2017) -- Start
                ''CCBRT Enhancement - 66.1 - Employee cost center distribution for non-salary heads, Effective period option on Employee Cost Center.
                ''lvEmpBankList.GroupingColumn = colhEmpName
                'If chkGroupByEmpName.Checked = False Then
                '    lvEmpBankList.GroupingColumn = colhCostCenter
                'Else
                'lvEmpBankList.GroupingColumn = colhEmpName
                'End If
                ''Sohail (29 Mar 2017) -- End
                'lvEmpBankList.DisplayGroups(True)

                'lvEmpBankList.GridLines = False

                'If lvEmpBankList.Items.Count > 6 Then
                '    colhPercentage.Width = 150 - 15
                'Else
                '    colhPercentage.Width = 150
                'End If
                ''Sohail (29 Mar 2017) -- Start
                ''CCBRT Enhancement - 66.1 - Employee cost center distribution for non-salary heads, Effective period option on Employee Cost Center.
                'If chkGroupByEmpName.Checked = False Then
                '    colhEmpName.Width = 270
                '    colhCostCenter.Width = 0
                'Else
                '    colhEmpName.Width = 0
                '    colhCostCenter.Width = 270
                'End If
                ''Sohail (07 Feb 2019) - [210] = [270]
                ''Sohail (29 Mar 2017) -- End
                dgvEmpBankList.AutoGenerateColumns = False
                objdgColhTranUnkid.DataPropertyName = "costcentertranunkid"
                dgcolhEmpCode.DataPropertyName = "employeecode"
                dgcolhEmpName.DataPropertyName = "employeename"
                dgcolhTranHead.DataPropertyName = "trnheadname"
                dgcolhCostCenter.DataPropertyName = "CostCenterColumn"
                dgcolhAllocationByName.DataPropertyName = "AllocationByName"
                dgcolhPercentage.DataPropertyName = "percentage"
                dgcolhPeriod.DataPropertyName = "period_name"
                objdgcolhIsGrp.DataPropertyName = "IsGrp"
                objdgColhPeriodStatusId.DataPropertyName = "statusid"
                objdgcolhIsinactive.DataPropertyName = "isinactive"
                'Sohail (25 Jul 2020) -- Start
                'NMB Issue # : JV is not picking employee bank account when net pay head is mapped as default for all non mapped employees.
                objdgcolhEmployeeunkid.DataPropertyName = "employeeunkid"
                objdgcolhTranheadunkid.DataPropertyName = "tranheadunkid"
                objdgcolhAllocationByid.DataPropertyName = "allocationbyid"
                objdgcolhCostcenterUnkid.DataPropertyName = "costcenterunkid"
                objdgcolhPeriodunkid.DataPropertyName = "periodunkid"
                'Sohail (25 Jul 2020) -- End
                objdgcolhCheck.DataPropertyName = "IsChecked" 'Sohail (13 Jan 2022)

                If chkGroupByEmpName.Checked = False Then
                    dgcolhCostCenter.Visible = False
                    dgcolhEmpName.Visible = True
                    dgcolhEmpCode.Visible = True
                    dgcolhEmpName.AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill
                Else
                    dgcolhEmpName.Visible = False
                    dgcolhEmpCode.Visible = False
                    dgcolhCostCenter.Visible = True
                    dgcolhCostCenter.AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill
                End If
                'Sohail (13 Jan 2022) -- Start
                'NMB Enhancement # OLD-101 : - Give Checkbox selection option on Employee Cost Center Screen.
                'dgvEmpBankList.DataSource = dtTable
                dgvEmpBankList.DataSource = mdtView
                'Sohail (13 Jan 2022) -- End

                SetGridColor()
                'Hemant (11 Mar 2018) -- End
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillList", mstrModuleName)
            'Sohail (29 Mar 2017) -- Start
            'CCBRT Enhancement - 66.1 - Employee cost center distribution for non-salary heads, Effective period option on Employee Cost Center.
        Finally
            'Hemant (11 Mar 2018) -- Start
            'ISSUE : performance issue on emp cost centre list screen, taking 30 to 45 mins to open
            'objbtnSearch.ShowResult(lvEmpBankList1.Items.Count.ToString)
            'Sohail (18 Jun 2019) -- Start
            'Eko Supreme Nigeria Ltd - Support issue # 0003911 - 74.1 : User able to see Payslip l Eko Supreme Nigeria Ltd. (Error : Cannot find column 'IsGrp')
            'objbtnSearch.ShowResult(dtTable.Select("IsGrp = 0").Length.ToString)
            If dtTable IsNot Nothing AndAlso dtTable.Rows.Count > 0 Then
            objbtnSearch.ShowResult(dtTable.Select("IsGrp = 0").Length.ToString)
            Else
                objbtnSearch.ShowResult("0")
            End If
            'Sohail (18 Jun 2019) -- End
            Cursor.Current = Cursors.Default
            'Hemant (11 Mar 2018) -- End
            'Sohail (29 Mar 2017) -- End
        End Try
    End Sub
    'Hemant (11 Mar 2018) -- Start
    'ISSUE : performance issue on emp cost centre list screen, taking 30 to 45 mins to open
    Private Sub SetGridColor()
        Try
            Dim dr = From p As DataGridViewRow In dgvEmpBankList.Rows.Cast(Of DataGridViewRow)() Where CBool(p.Cells(objdgcolhIsGrp.Index).Value) = True Select p
            dr.ToList.ForEach(Function(x) SetRowStyle(x, True))

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetGridColor", mstrModuleName)
        Finally
        End Try
    End Sub

    Public Function SetRowStyle(ByVal xRow As DataGridViewRow, ByVal isHeader As Boolean) As Boolean
        Try
            If isHeader Then
                Dim dgvcsHeader As New DataGridViewCellStyle
                dgvcsHeader.ForeColor = Color.White
                dgvcsHeader.SelectionBackColor = Color.Gray
                dgvcsHeader.SelectionForeColor = Color.White
                dgvcsHeader.BackColor = Color.Gray
                xRow.DefaultCellStyle = dgvcsHeader
                'xRow.Cells(dgcolhParticular.Index).Value = xRow.Cells(dgcolhParticular.Index).Value.ToString & Space(10) & "[ " & Language.getMessage(mstrModuleName, 10, "Shift Hours:") & " " & xRow.Cells(objdgcolhShiftHours.Index).Value.ToString & " ]"
            Else
                xRow.DefaultCellStyle.ForeColor = Color.Red
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetRowStyle", mstrModuleName)
        End Try
        Return True
    End Function
    'Hemant (11 Mar 2018) -- End

    Private Sub FillCombo()
        Dim dsCombos As New DataSet
        'Dim objEmployee As New clsEmployee_Master 'Sohail (29 Mar 2017)
        Dim objTranHead As New clsTransactionHead
        Dim objCostCenter As New clscostcenter_master
        'Sohail (29 Mar 2017) -- Start
        'CCBRT Enhancement - 66.1 - Employee cost center distribution for non-salary heads, Effective period option on Employee Cost Center.
        'Dim objAccType As New clsBankAccType
        Dim objMaster As New clsMasterData
        Dim objPeriod As New clscommom_period_Tran
        Dim mintFirstPeriodID As Integer = 0
        'Sohail (29 Mar 2017) -- End
        Try
            'Anjan [10 June 2015] -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS

            'Sohail (06 Jan 2012) -- Start
            'TRA - ENHANCEMENT
            'dsCombos = objEmployee.GetEmployeeList("Employee", True, True)
            'If ConfigParameter._Object._IsIncludeInactiveEmp = False Then
            '    dsCombos = objEmployee.GetEmployeeList("Employee", True, , , , , , , , , , , , , eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate))
            'Else
            '    dsCombos = objEmployee.GetEmployeeList("Employee", True)
            'End If

            'Sohail (29 Mar 2017) -- Start
            'CCBRT Enhancement - 66.1 - Employee cost center distribution for non-salary heads, Effective period option on Employee Cost Center.
            'dsCombos = objEmployee.GetEmployeeList(FinancialYear._Object._DatabaseName, _
            '                               User._Object._Userunkid, _
            '                               FinancialYear._Object._YearUnkid, _
            '                               Company._Object._Companyunkid, _
            '                               eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
            '                               eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
            '                               ConfigParameter._Object._UserAccessModeSetting, _
            '                               True, ConfigParameter._Object._IsIncludeInactiveEmp, "Employee", True)
            ''Anjan [10 June 2015] -- End

            ''Sohail (06 Jan 2012) -- End
            'With cboEmployee
            '    .ValueMember = "employeeunkid"
            '    .DisplayMember = "employeename"
            '    .DataSource = dsCombos.Tables("Employee")
            '    .SelectedValue = 0
            'End With
            'Sohail (29 Mar 2017) -- End
            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'dsCombos = objTranHead.getComboList("TranHead", True, , , enTypeOf.Salary)
            'Sohail (24 Apr 2018) -- Start
            'Voltamp Enhancement : Each head shoud be rounded before using in addition in formula in 71.1.
            'dsCombos = objTranHead.getComboList(FinancialYear._Object._DatabaseName, "TranHead", True, , , enTypeOf.Salary)
            'Sohail (07 Feb 2019) -- Start
            'NMB Enhancement - 76.1 - Include all heads including Payable heads on employee cost centre screen.
            'dsCombos = objTranHead.getComboList(FinancialYear._Object._DatabaseName, "TranHead", True)
            dsCombos = objTranHead.getComboList(FinancialYear._Object._DatabaseName, "TranHead", True, , , , True, False, "", True, False, True)
            'Sohail (07 Feb 2019) -- End
            'Sohail (24 Apr 2018) -- End
            'Sohail (21 Aug 2015) -- End
            With cboTranHead
                .ValueMember = "tranheadunkid"
                .DisplayMember = "name"
                .DataSource = dsCombos.Tables("TranHead")
                .SelectedValue = 0
            End With
            'Sohail (07 Feb 2019) -- Start
            'NMB Enhancement - 76.1 - Employee cost centre should be distributed on all allocations instead of only on cost centre.
            'dsCombos = objCostCenter.getComboList("CostCenter", True)
            'With cboCostCenter
            '    .ValueMember = "costcenterunkid"
            '    .DisplayMember = "costcentername"
            '    .DataSource = dsCombos.Tables("CostCenter")
            '    .SelectedValue = 0
            'End With
            dsCombos = objMaster.GetEAllocation_Notification("List", "", False, True)
            With cboAllocation
                .ValueMember = "id"
                .DisplayMember = "name"
                .DataSource = dsCombos.Tables(0)
                .SelectedValue = 0
            End With
            'Sohail (07 Feb 2019) -- End

            'Sohail (29 Mar 2017) -- Start
            'CCBRT Enhancement - 66.1 - Employee cost center distribution for non-salary heads, Effective period option on Employee Cost Center.
            mintFirstPeriodID = objMaster.getFirstPeriodID(enModuleReference.Payroll, FinancialYear._Object._YearUnkid, enStatusType.Open, False, True)
            dsCombos = objPeriod.getListForCombo(enModuleReference.Payroll, FinancialYear._Object._YearUnkid, FinancialYear._Object._DatabaseName, FinancialYear._Object._Database_Start_Date, "Period", True, 0)
            With cboPeriod
                .ValueMember = "periodunkid"
                .DisplayMember = "Name"
                .DataSource = dsCombos.Tables("Period")
                .SelectedValue = mintFirstPeriodID
            End With
            'Sohail (29 Mar 2017) -- End

            'Gajanan [24-Aug-2020] -- Start
            'NMB Enhancement : Allow to set account configuration mapping 
            'inactive for closed period to allow to map head on other account 
            'configuration screen from new period
            dsCombos = objMaster.getComboListTranHeadActiveInActive("ActiveInactive", False)
            With cboActiveInactive
                .DisplayMember = "Name"
                .ValueMember = "Id"
                .DataSource = dsCombos.Tables("ActiveInactive")
                .SelectedIndex = 0
            End With
            'Gajanan [24-Aug-2020] -- End

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        Finally
            'objEmployee = Nothing 'Sohail (29 Mar 2017)
            objTranHead = Nothing
            objCostCenter = Nothing
            'Sohail (29 Mar 2017) -- Start
            'CCBRT Enhancement - 66.1 - Employee cost center distribution for non-salary heads, Effective period option on Employee Cost Center.
            'objAccType = Nothing
            objMaster = Nothing
            objPeriod = Nothing
            'Sohail (29 Mar 2017) -- End
            dsCombos = Nothing

        End Try
    End Sub

    'Sohail (29 Mar 2017) -- Start
    'CCBRT Enhancement - 66.1 - Employee cost center distribution for non-salary heads, Effective period option on Employee Cost Center.
    Private Sub FillCombo_Employee()
        Dim objEmployee As New clsEmployee_Master
        Dim dsList As DataSet
        Try

            dsList = objEmployee.GetEmployeeList(FinancialYear._Object._DatabaseName, _
                                           User._Object._Userunkid, _
                                           FinancialYear._Object._YearUnkid, _
                                           Company._Object._Companyunkid, _
                                           mdtPeriod_startdate, _
                                           mdtPeriod_enddate, _
                                           ConfigParameter._Object._UserAccessModeSetting, _
                                           True, False, "EmployeeList", True)

            With cboEmployee
                .BeginUpdate()
                .ValueMember = "employeeunkid"
                .DisplayMember = "employeename"
                .DataSource = dsList.Tables("EmployeeList")
                .SelectedValue = 0
                .EndUpdate()
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo_Employee", mstrModuleName)
        Finally
            objEmployee = Nothing
        End Try
    End Sub
    'Sohail (29 Mar 2017) -- End

    Private Sub SetVisibility()
        Try
            btnNew.Enabled = User._Object.Privilege._AddEmployeeBanks
            btnEdit.Enabled = User._Object.Privilege._EditEmployeeBanks
            btnInactive.Enabled = User._Object.Privilege._AllowToInactiveEmployeeCostCenter 'Sohail (25 Jul 2020)

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetVisibility", mstrModuleName)
        End Try
    End Sub
#End Region

#Region " From's Events "

    Private Sub frmEmpDistributedCostCenterList_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        Try
            objEmpCostCenter = Nothing
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmEmpDistributedCostCenterList_FormClosed", mstrModuleName)
        End Try
    End Sub

    Private Sub frmEmpDistributedCostCenterList_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown
        Try
            If e.KeyCode = Keys.Escape Then
                Me.Close()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "", mstrModuleName)
        End Try
    End Sub

    Private Sub frmEmpDistributedCostCenterList_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        objEmpCostCenter = New clsemployee_costcenter_Tran
        Try
            Call Set_Logo(Me, gApplicationType)
            Call Language.setLanguage(Me.Name)
            Call OtherSettings() 'Sohail (10 Jul 2014)

            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'mstrSalaryHeadIDs = (New clsTransactionHead).GetSalaryHeadsIDs(True)
            'Sohail (29 Mar 2017) -- Start
            'CCBRT Enhancement - 66.1 - Employee cost center distribution for non-salary heads, Effective period option on Employee Cost Center.
            'mstrSalaryHeadIDs = (New clsTransactionHead).GetSalaryHeadsIDs(FinancialYear._Object._DatabaseName, True)
            'mstrSalaryHeadIDs = (New clsTransactionHead).GetSalaryHeadsIDs(FinancialYear._Object._DatabaseName, True)
            'Sohail (29 Mar 2017) -- End
            'Sohail (21 Aug 2015) -- End

            Call SetVisibility()

            Call SetColor()
            Call FillCombo()
            Call FillList()

            'Hemant (11 Mar 2018) -- Start
            'ISSUE : performance issue on emp cost centre list screen, taking 30 to 45 mins to open
            'If lvEmpBankList1.Items.Count > 0 Then lvEmpBankList1.Items(0).Selected = True
            'lvEmpBankList1.Select()
            If dgvEmpBankList.Rows.Count > 0 Then dgvEmpBankList.Rows(0).Selected = True
            dgvEmpBankList.Select()
            'Hemant (11 Mar 2018) -- End


        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmEmpDistributedCostCenterList_Load", mstrModuleName)
        End Try
    End Sub

    'Sohail (10 Jul 2014) -- Start
    'Enhancement - Custom Language.
    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsemployee_costcenter_Tran.SetMessages()
            objfrm._Other_ModuleNames = "clsemployee_costcenter_Tran"
            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub
    'Sohail (10 Jul 2014) -- End

#End Region


    'Sohail (29 Mar 2017) -- Start
    'CCBRT Enhancement - 66.1 - Employee cost center distribution for non-salary heads, Effective period option on Employee Cost Center.
#Region " Combobox Events "
    Private Sub cboPeriod_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboPeriod.SelectedIndexChanged
        Dim objPeriod As New clscommom_period_Tran
        Try
            If CInt(cboPeriod.SelectedValue) > 0 Then
                objPeriod._Periodunkid(FinancialYear._Object._DatabaseName) = CInt(cboPeriod.SelectedValue)
                mdtPeriod_startdate = objPeriod._Start_Date
                mdtPeriod_enddate = objPeriod._End_Date
            End If
            Call FillCombo_Employee()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboPeriod_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

    'Sohail (07 Feb 2019) -- Start
    'NMB Enhancement - 76.1 - Employee cost centre should be distributed on all allocations instead of only on cost centre.
    Private Sub cboAllocation_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboAllocation.SelectedIndexChanged
        Dim objStation As New clsStation
        Dim objDeptGrp As New clsDepartmentGroup
        Dim objDepartment As New clsDepartment
        Dim objSection As New clsSections
        Dim objUnit As New clsUnits
        Dim objJobGrp As New clsJobGroup
        Dim objJob As New clsJobs
        Dim objClassGrp As New clsClassGroup
        Dim objClass As New clsClass
        Dim objSectionGrp As New clsSectionGroup
        Dim objUnitGroup As New clsUnitGroup
        Dim objTeam As New clsTeams
        Dim objCCentre As New clscostcenter_master
        Dim dsCombos As DataSet = Nothing

        Try

            Select Case CInt(cboAllocation.SelectedValue)

                Case enAllocation.BRANCH
                    dsCombos = objStation.getComboList("Station", True)
                    dsCombos.Tables(0).Columns(0).ColumnName = "Id"
                Case enAllocation.DEPARTMENT_GROUP
                    dsCombos = objDeptGrp.getComboList("DeptGrp", True)
                    dsCombos.Tables(0).Columns(0).ColumnName = "Id"
                Case enAllocation.DEPARTMENT
                    dsCombos = objDepartment.getComboList("Department", True)
                    dsCombos.Tables(0).Columns(0).ColumnName = "Id"
                Case enAllocation.SECTION_GROUP
                    dsCombos = objSectionGrp.getComboList("List", True)
                    dsCombos.Tables(0).Columns(0).ColumnName = "Id"
                Case enAllocation.SECTION
                    dsCombos = objSection.getComboList("Section", True)
                    dsCombos.Tables(0).Columns(0).ColumnName = "Id"
                Case enAllocation.UNIT_GROUP
                    dsCombos = objUnitGroup.getComboList("List", True)
                    dsCombos.Tables(0).Columns(0).ColumnName = "Id"
                Case enAllocation.UNIT
                    dsCombos = objUnit.getComboList("Unit", True)
                    dsCombos.Tables(0).Columns(0).ColumnName = "Id"
                Case enAllocation.TEAM
                    dsCombos = objTeam.getComboList("List", True)
                    dsCombos.Tables(0).Columns(0).ColumnName = "Id"
                Case enAllocation.JOB_GROUP
                    dsCombos = objJobGrp.getComboList("JobGrp", True)
                    dsCombos.Tables(0).Columns(0).ColumnName = "Id"
                Case enAllocation.JOBS
                    dsCombos = objJob.getComboList("Job", True)
                    dsCombos.Tables(0).Columns(0).ColumnName = "Id"
                Case enAllocation.CLASS_GROUP
                    dsCombos = objClassGrp.getComboList("ClassGrp", True)
                    dsCombos.Tables(0).Columns(0).ColumnName = "Id"
                Case enAllocation.CLASSES
                    dsCombos = objClass.getComboList("Class", True)
                    dsCombos.Tables(0).Columns(0).ColumnName = "Id"
                Case enAllocation.COST_CENTER
                    dsCombos = objCCentre.getComboList("CCentre", True)
                    dsCombos.Tables(0).Columns(0).ColumnName = "Id"
                    dsCombos.Tables(0).Columns(2).ColumnName = "Name"

                Case Else
                    dsCombos = Nothing

            End Select

            lblCostCenter.Text = cboAllocation.Text
            If dsCombos IsNot Nothing Then

                With cboCostCenter
                    .ValueMember = "Id"
                    .DisplayMember = "Name"
                    .DataSource = dsCombos.Tables(0)
                    .SelectedValue = 0
                End With
            Else
                cboCostCenter.DataSource = Nothing
                cboCostCenter.Items.Clear()
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboAllocation_SelectedIndexChanged", mstrModuleName)
        Finally
            objStation = Nothing
            objDeptGrp = Nothing
            objDepartment = Nothing
            objSection = Nothing
            objUnit = Nothing
            objJobGrp = Nothing
            objJob = Nothing
            objClassGrp = Nothing
            objClass = Nothing
            objSectionGrp = Nothing
            objUnitGroup = Nothing
            objTeam = Nothing
            objCCentre = Nothing
            dsCombos = Nothing
        End Try
    End Sub
    'Sohail (07 Feb 2019) -- End

    'Gajanan [24-Aug-2020] -- Start
    'NMB Enhancement : Allow to set account configuration mapping 
    'inactive for closed period to allow to map head on other account 
    'configuration screen from new period
    Private Sub cboActiveInactive_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboActiveInactive.SelectedIndexChanged
        Try
            If CInt(cboActiveInactive.SelectedValue) = enTranHeadActiveInActive.ACTIVE Then
                btnInactive.Visible = True
            Else
                btnInactive.Visible = False
            End If

            FillList()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboActiveInactive_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub
    'Gajanan [24-Aug-2020] -- End
#End Region
    'Sohail (29 Mar 2017) -- End

#Region " Button's Events "

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnClose_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnNew_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNew.Click
        Dim frm As New frmEmpDistributedCostCenter_AddEdit
        Try
            'If User._Object._RightToLeft = True Then
            '    frm.RightToLeft = Windows.Forms.RightToLeft.Yes
            '    frm.RightToLeftLayout = True
            '    Call Language.ctlRightToLeftlayOut(frm)
            'End If
            If frm.displayDialog(-1, enAction.ADD_CONTINUE) Then
                Call FillList()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnNew_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnEdit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEdit.Click, dgvEmpBankList.DoubleClick
        If User._Object.Privilege._EditEmployeeBanks = False OrElse User._Object.Privilege._DeleteEmployeeBanks = False Then Exit Sub
        Dim frm As New frmEmpDistributedCostCenter_AddEdit
        Try
            'Hemant (11 Mar 2018) -- Start
            'ISSUE : performance issue on emp cost centre list screen, taking 30 to 45 mins to open
            'If lvEmpBankList.SelectedItems.Count <= 0 Then
            If dgvEmpBankList.SelectedRows.Count <= 0 Then
                'Hemant (11 Mar 2018) -- End
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please select Employee from the list to perform further operation."), enMsgBoxStyle.Information)
                'Hemant (11 Mar 2018) -- Start
                'ISSUE : performance issue on emp cost centre list screen, taking 30 to 45 mins to open
                'lvEmpBankList.Select()
                dgvEmpBankList.Select()
                'Hemant (11 Mar 2018) -- End
                Exit Sub
            End If


            Dim intSelectedIndex As Integer
            'Hemant (11 Mar 2018) -- Start
            'ISSUE : performance issue on emp cost centre list screen, taking 30 to 45 mins to open
            'intSelectedIndex = lvEmpBankList.SelectedItems(0).Index
            intSelectedIndex = dgvEmpBankList.CurrentRow.Index
            'Hemant (11 Mar 2018) -- End

            'Hemant (11 Mar 2018) -- Start
            'ISSUE : performance issue on emp cost centre list screen, taking 30 to 45 mins to open            
            If CBool(dgvEmpBankList.CurrentRow.Cells(objdgcolhIsGrp.Index).Value) = True Then Exit Try
            'Hemant (11 Mar 2018) -- End


            'Sohail (02 Jan 2017) -- Start
            'TRA Enhancement - 64.1 - Performance enhancement in process payroll, and global void payment.
            'Hemant (11 Mar 2018) -- Start
            'ISSUE : performance issue on emp cost centre list screen, taking 30 to 45 mins to open
            'If CInt(lvEmpBankList.Items(intSelectedIndex).SubItems(objcolhPeriodStatusId.Index).Text) = enStatusType.Close Then
            If CInt(dgvEmpBankList.Rows(intSelectedIndex).Cells(objdgColhPeriodStatusId.Index).Value) = enStatusType.Close Then
                'Hemant (11 Mar 2018) -- End
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Sorry, You cannot edit this employee cost center. Reason : Period is already closed"), enMsgBoxStyle.Information)
                'Hemant (11 Mar 2018) -- Start
                'ISSUE : performance issue on emp cost centre list screen, taking 30 to 45 mins to open
                'lvEmpBankList.Select()
                dgvEmpBankList.Select()
                'Hemant (11 Mar 2018) -- End
                Exit Sub
            End If
            'Sohail (02 Jan 2017) -- End

            'If User._Object._RightToLeft = True Then
            '    frm.RightToLeft = Windows.Forms.RightToLeft.Yes
            '    frm.RightToLeftLayout = True
            '    Call Language.ctlRightToLeftlayOut(frm)
            'End If

            'Hemant (11 Mar 2018) -- Start
            'ISSUE : performance issue on emp cost centre list screen, taking 30 to 45 mins to open
            'If frm.displayDialog(CInt(lvEmpBankList.SelectedItems(0).Tag), enAction.EDIT_ONE) Then
            'Sohail (13 Jan 2022) -- Start
            'NMB Enhancement # OLD-101 : - Give Checkbox selection option on Employee Cost Center Screen. 
            'If frm.displayDialog(CInt(dgvEmpBankList.CurrentRow.Cells(0).Value), enAction.EDIT_ONE) Then
            If frm.displayDialog(CInt(dgvEmpBankList.CurrentRow.Cells(objdgColhTranUnkid.Index).Value), enAction.EDIT_ONE) Then
                'Sohail (13 Jan 2022) -- End
                'Hemant (11 Mar 2018) -- End
                Call FillList()
            End If

            frm = Nothing

            'Hemant (11 Mar 2018) -- Start
            'ISSUE : performance issue on emp cost centre list screen, taking 30 to 45 mins to open
            'lvEmpBankList.Select()
            dgvEmpBankList.Select()
            'Hemant (11 Mar 2018) -- End

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnEdit_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub btnInactive_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnInactive.Click
        If User._Object.Privilege._EditEmployeeBanks = False OrElse User._Object.Privilege._DeleteEmployeeBanks = False Then Exit Sub
        Dim dsList As DataSet = Nothing
        Dim frm As New frmEmpDistributedCostCenter_AddEdit
        Try
            'Sohail (13 Jan 2022) -- Start
            'NMB Enhancement # OLD-101 : - Give Checkbox selection option on Employee Cost Center Screen.
            'If dgvEmpBankList.SelectedRows.Count <= 0 Then
            If mdtView Is Nothing Then Exit Sub
            If mdtView.ToTable.Select("IsGrp = 0 AND IsChecked = 1").Length <= 0 Then
                'Sohail (13 Jan 2022) -- End
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please select Employee from the list to perform further operation."), enMsgBoxStyle.Information)
                dgvEmpBankList.Select()
                Exit Sub
            End If


            'Sohail (13 Jan 2022) -- Start
            'NMB Enhancement # OLD-101 : - Give Checkbox selection option on Employee Cost Center Screen.
            'Dim intSelectedIndex As Integer
            'intSelectedIndex = dgvEmpBankList.CurrentRow.Index
            'If CBool(dgvEmpBankList.CurrentRow.Cells(objdgcolhIsGrp.Index).Value) = True Then Exit Try
            'Sohail (13 Jan 2022) -- End

            Dim objPeriod As New clscommom_period_Tran
            objPeriod._Periodunkid(FinancialYear._Object._DatabaseName) = CInt(cboPeriod.SelectedValue)
            If objPeriod._Statusid = enStatusType.Close Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Sorry, selected period is already closed."), enMsgBoxStyle.Information)
                cboPeriod.Focus()
                Exit Sub
            End If
            objPeriod = Nothing

            'Sohail (13 Jan 2022) -- Start
            'NMB Enhancement # OLD-101 : - Give Checkbox selection option on Employee Cost Center Screen.
            'If CInt(dgvEmpBankList.Rows(intSelectedIndex).Cells(objdgColhPeriodStatusId.Index).Value) = enStatusType.Open Then
            '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Sorry, You cannot inactive this employee cost center. Reason : Period is already opened"), enMsgBoxStyle.Information)
            '    dgvEmpBankList.Select()
            '    Exit Sub
            'End If
            Dim arrDistPeriod() As String = (From p In mdtView.ToTable Where (CBool(p.Item("IsGrp")) = False AndAlso CBool(p.Item("IsChecked")) = True) Select (p.Item("periodunkid").ToString)).Distinct().ToArray
            For Each strPeriodID As String In arrDistPeriod
                objPeriod = New clscommom_period_Tran
                objPeriod._Periodunkid(FinancialYear._Object._DatabaseName) = CInt(strPeriodID)
                If objPeriod._Statusid = enStatusType.Open Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Sorry, You cannot inactive this employee cost center. Reason : Period is already opened."), enMsgBoxStyle.Information)
                dgvEmpBankList.Select()
                Exit Sub
            End If
                objPeriod = Nothing
            Next
            'Sohail (13 Jan 2022) -- End


            'Sohail (13 Jan 2022) -- Start
            'NMB Enhancement # OLD-101 : - Give Checkbox selection option on Employee Cost Center Screen.
            'If objEmpBankTran.isExist(FinancialYear._Object._DatabaseName, CInt(dgvEmpBankList.Rows(intSelectedIndex).Cells(objdgcolhEmployeeunkid.Index).Value), CInt(dgvEmpBankList.Rows(intSelectedIndex).Cells(objdgcolhTranheadunkid.Index).Value), CInt(dgvEmpBankList.Rows(intSelectedIndex).Cells(objdgcolhAllocationByid.Index).Value), CInt(dgvEmpBankList.Rows(intSelectedIndex).Cells(objdgcolhCostcenterUnkid.Index).Value), CInt(cboPeriod.SelectedValue)) = True Then
            '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "Sorry, #headname# is already mapped in selected period #periodname#.").Replace("#headname#", dgvEmpBankList.Rows(intSelectedIndex).Cells(dgcolhAllocationByName.Index).Value.ToString & " : " & dgvEmpBankList.Rows(intSelectedIndex).Cells(dgcolhTranHead.Index).Value.ToString).Replace("#periodname#", cboPeriod.Text), enMsgBoxStyle.Information)
            '    dgvEmpBankList.Select()
            '    Exit Sub
            'End If
            Dim arrRow As List(Of DataRow) = (From p In mdtView.ToTable Where (CBool(p.Item("IsGrp")) = False AndAlso CBool(p.Item("IsChecked")) = True) Select (p)).ToList
            For Each dr As DataRow In arrRow
                'If objEmpBankTran.isExist(FinancialYear._Object._DatabaseName, CInt(dgvEmpBankList.Rows(intSelectedIndex).Cells(objdgcolhEmployeeunkid.Index).Value), CInt(dgvEmpBankList.Rows(intSelectedIndex).Cells(objdgcolhTranheadunkid.Index).Value), CInt(dgvEmpBankList.Rows(intSelectedIndex).Cells(objdgcolhAllocationByid.Index).Value), CInt(dgvEmpBankList.Rows(intSelectedIndex).Cells(objdgcolhCostcenterUnkid.Index).Value), CInt(cboPeriod.SelectedValue)) = True Then
                If objEmpCostCenter.isExist(FinancialYear._Object._DatabaseName, CInt(dr.Item("employeeunkid")), CInt(dr.Item("tranheadunkid")), CInt(dr.Item("allocationbyid")), CInt(dr.Item("costcenterunkid")), CInt(cboPeriod.SelectedValue)) = True Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "Sorry, #headname# is already mapped in selected period #periodname#.").Replace("#headname#", dr.Item("AllocationByName").ToString & " : " & dr.Item("trnheadname").ToString).Replace("#periodname#", cboPeriod.Text), enMsgBoxStyle.Information)
                dgvEmpBankList.Select()
                Exit Sub
            End If
            Next
            'Sohail (13 Jan 2022) -- End

            If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 6, "Are you sure you want to inactive ticked employee cost centers from #periodname#?").Replace("#periodname#", cboPeriod.Text), CType(enMsgBoxStyle.Question + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.Yes Then
                'Sohail (13 Jan 2022) -- Start
                'NMB Enhancement # OLD-101 : - Give Checkbox selection option on Employee Cost Center Screen.
                'Dim strIDs As String = dgvEmpBankList.Rows(intSelectedIndex).Cells(objdgColhTranUnkid.Index).Value.ToString
                'If CDec(dgvEmpBankList.Rows(intSelectedIndex).Cells(objdgColhPeriodStatusId.Index).Value) > 0 AndAlso CDec(dgvEmpBankList.Rows(intSelectedIndex).Cells(objdgColhPeriodStatusId.Index).Value) <> 100 Then
                '    dsList = objEmpBankTran.GetList(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, mdtPeriod_startdate, mdtPeriod_enddate, ConfigParameter._Object._UserAccessModeSetting, True, ConfigParameter._Object._IsIncludeInactiveEmp, "EmpCC", True, CInt(dgvEmpBankList.Rows(intSelectedIndex).Cells(objdgcolhEmployeeunkid.Index).Value), CInt(dgvEmpBankList.Rows(intSelectedIndex).Cells(objdgcolhTranheadunkid.Index).Value), "premployee_costcenter_tran.periodunkid = " & CInt(dgvEmpBankList.Rows(intSelectedIndex).Cells(objdgcolhPeriodunkid.Index).Value) & " AND premployee_costcenter_tran.costcentertranunkid <> " & CInt(dgvEmpBankList.Rows(intSelectedIndex).Cells(objdgColhTranUnkid.Index).Value) & " ", mdtPeriod_enddate, "AllocationByName, AllocationName", "", False, CInt(cboActiveInactive.SelectedValue))
                '    Dim sIDs As String = String.Join(",", (From p In dsList.Tables(0) Select (p.Item("costcentertranunkid").ToString)).ToArray)
                '    If sIDs.Trim <> "" Then strIDs = strIDs & "," & sIDs
                'End If
                Dim strIDs As String = String.Join(",", (From p In arrRow Select (p.Item("costcentertranunkid").ToString)).ToArray)
                arrRow = (From p In arrRow Where (CDec(p.Item("percentage")) > 0 AndAlso CDec(p.Item("percentage")) <> 100) Select (p)).ToList
                If arrRow.Count > 0 Then
                    Dim strDistEmp As String = String.Join(",", (From p In arrRow Select (p.Item("employeeunkid").ToString)).Distinct().ToArray)
                    Dim strDistHead As String = String.Join(",", (From p In arrRow Select (p.Item("tranheadunkid").ToString)).Distinct().ToArray)
                    Dim strDistPeriod As String = String.Join(",", (From p In arrRow Select (p.Item("periodunkid").ToString)).Distinct().ToArray)

                    dsList = objEmpCostCenter.GetList(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, mdtPeriod_startdate, mdtPeriod_enddate, ConfigParameter._Object._UserAccessModeSetting, True, ConfigParameter._Object._IsIncludeInactiveEmp, "EmpCC", True, 0, 0, "premployee_costcenter_tran.periodunkid IN (" & strDistPeriod & ") AND premployee_costcenter_tran.employeeunkid IN (" & strDistEmp & ") AND premployee_costcenter_tran.tranheadunkid IN (" & strDistHead & ") ", mdtPeriod_enddate, "AllocationByName, AllocationName", "", False, CInt(cboActiveInactive.SelectedValue))
                    For Each dsRow As DataRow In arrRow
                        Dim iPeriod As Integer = CInt(dsRow.Item("periodunkid"))
                        Dim iEmp As Integer = CInt(dsRow.Item("employeeunkid"))
                        Dim iHead As Integer = CInt(dsRow.Item("tranheadunkid"))
                        Dim iCCenterTran As Integer = CInt(dsRow.Item("costcentertranunkid"))
                        Dim sIDs As String = String.Join(",", (From p In dsList.Tables(0) Where (CInt(p.Item("periodunkid")) = iPeriod AndAlso CInt(p.Item("employeeunkid")) = iEmp AndAlso CInt(p.Item("tranheadunkid")) = iHead AndAlso CInt(p.Item("costcentertranunkid")) <> iCCenterTran) Select (p.Item("costcentertranunkid").ToString)).ToArray)
                    If sIDs.Trim <> "" Then strIDs = strIDs & "," & sIDs
                    Next
                End If
                'Sohail (13 Jan 2022) -- End
                If objEmpCostCenter.InactiveAll(FinancialYear._Object._DatabaseName, _
                                         strIDs, _
                                         ConfigParameter._Object._CurrentDateAndTime, _
                                         Nothing, CInt(cboPeriod.SelectedValue)) Then
                    Call FillList()
                End If
            End If


        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnInactive_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try


    End Sub

    'Sohail (24 Jan 2022) -- Start
    'Enhancement :  : Allow to void multiple employee cost centers at a time.
    Private Sub btnVoid_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnVoid.Click
        If User._Object.Privilege._DeleteEmployeeBanks = False Then Exit Sub
        Dim objPeriod As New clscommom_period_Tran
        Dim objTnALeave As New clsTnALeaveTran
        Try
            If mdtView Is Nothing Then Exit Sub

            If mdtView.ToTable.Select("IsGrp = 0 AND IsChecked = 1").Length <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please select Employee from the list to perform further operation."), enMsgBoxStyle.Information)
                dgvEmpBankList.Select()
                Exit Sub
            End If

            Dim arrDistPeriod() As String = (From p In mdtView.ToTable Where (CBool(p.Item("IsGrp")) = False AndAlso CBool(p.Item("IsChecked")) = True) Order By (p.Item("end_date").ToString) Select (p.Item("periodunkid").ToString)).Distinct().ToArray
            For Each strPeriodID As String In arrDistPeriod
                Dim intPeriodD As Integer = CInt(strPeriodID)
                objPeriod = New clscommom_period_Tran
                objPeriod._Periodunkid(FinancialYear._Object._DatabaseName) = intPeriodD
                If objPeriod._Statusid = enStatusType.Close Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 7, "Sorry, You cannot void some employee cost centers. Reason : Period is already closed for some employee cost centers."), enMsgBoxStyle.Information)
                    dgvEmpBankList.Select()
                    Exit Sub
                End If

                Dim dtEmpList As DataTable = Nothing
                Dim strIDs = String.Join(",", (From p In mdtView.ToTable Where (CBool(p.Item("IsGrp")) = False AndAlso CBool(p.Item("IsChecked")) = True AndAlso CInt(p.Item("periodunkid")) = intPeriodD) Select (p.Item("employeeunkid").ToString)).Distinct().ToArray)
                If objTnALeave.IsPayrollProcessDone(intPeriodD, strIDs, objPeriod._End_Date, dtEmpList:=dtEmpList) = True Then
                    Dim objYesNp As New frmCommonValidationList
                    If objYesNp.displayDialog(True, Language.getMessage(mstrModuleName, 8, "Sorry, Payroll Process is already done for some of the employees.") & vbCrLf & "  " & Language.getMessage(mstrModuleName, 9, "Do you want to void Payroll?"), dtEmpList) = False Then
                        Exit Try
                    Else
                        Dim objFrm As New frmProcessPayroll
                        objFrm.ShowDialog()
                    End If
                    Exit Try
                End If
            Next

            Dim dec = (From p In mdtView.ToTable Where (CBool(p.Item("IsGrp")) = False AndAlso CBool(p.Item("IsChecked")) = True) Group p By G = New With {Key .enddate = p.Item("end_date").ToString, Key .employeeunkid = CInt(p.Item("employeeunkid").ToString), Key .tranheadunkid = CInt(p.Item("tranheadunkid").ToString), Key .employeename = p.Item("employeename").ToString} _
                   Into grp = Group Select New With {Key .end_date = grp(0).Item("end_date").ToString, Key .employeeunkid = grp(0).Item("employeeunkid").ToString, Key .tranheadunkid = grp(0).Item("tranheadunkid").ToString, Key .employeename = grp(0).Item("employeename").ToString, Key .amount = grp.Sum(Function(x) CDec(x.Item("percentage")))})

            For Each itm In dec
                If itm.amount <> 100 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 10, "Sorry, Total Percentage must be 100 for employee : ") & itm.employeename, enMsgBoxStyle.Information)
                    Exit Try
                End If
            Next

            Dim sIDs As String = String.Join(",", (From p In mdtView.ToTable Where (CBool(p.Item("IsGrp")) = False AndAlso CBool(p.Item("IsChecked")) = True) Select (p.Item("costcentertranunkid").ToString)).ToArray)
            For Each sID In sIDs.Split(CChar(","))
                If objEmpCostCenter.isUsed(CInt(sID)) = True Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 11, "Sorry, You can not delete this Employee Cost Center. Some Employee Cost Center is in used."), enMsgBoxStyle.Information)
                    Exit Sub
                End If
            Next

            If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 12, "Are you sure you want to delete ticked employee cost centers?"), CType(enMsgBoxStyle.Question + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.Yes Then
                Dim frm As New frmReasonSelection
                If User._Object._Isrighttoleft = True Then
                    frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                    frm.RightToLeftLayout = True
                    Call Language.ctlRightToLeftlayOut(frm)
                End If

                Dim mstrVoidReason As String = String.Empty
                frm.displayDialog(enVoidCategoryType.PAYROLL, mstrVoidReason)
                If mstrVoidReason.Length <= 0 Then
                    Exit Sub
                End If

                If objEmpCostCenter.VoideAll(FinancialYear._Object._DatabaseName _
                                         , Nothing _
                                         , sIDs _
                                         , User._Object._Userunkid _
                                         , mstrVoidReason _
                                         , ConfigParameter._Object._CurrentDateAndTime _
                                         , getIP() _
                                         , getHostName() _
                                         , mstrModuleName _
                                         , False _
                                         ) Then
                    Call FillList()
                End If

            End If


        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnVoid_Click", mstrModuleName)
        Finally
            objPeriod = Nothing
            objTnALeave = Nothing
        End Try
    End Sub
    'Sohail (24 Jan 2022) -- End

#End Region

#Region "Gridview Events"

    'Sohail (13 Jan 2022) -- Start
    'NMB Enhancement # OLD-101 : - Give Checkbox selection option on Employee Cost Center Screen.
    Private Sub dgvEmpBankList_CellContentClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvEmpBankList.CellContentClick
        Try
            RemoveHandler objchkSelectAll.CheckedChanged, AddressOf chkSelectAll_CheckedChanged

            If e.ColumnIndex = objdgcolhCheck.Index Then
                If Me.dgvEmpBankList.IsCurrentCellDirty Then
                    Me.dgvEmpBankList.CommitEdit(DataGridViewDataErrorContexts.Commit)
                End If
                Dim drRow As DataRow() = Nothing
                If CBool(dgvEmpBankList.Rows(e.RowIndex).Cells(objdgcolhIsGrp.Index).Value) = True Then
                    If chkGroupByEmpName.Checked = False Then
                        drRow = mdtView.Table.Select(dgcolhCostCenter.DataPropertyName & " = '" & dgvEmpBankList.Rows(e.RowIndex).Cells(dgcolhCostCenter.Index).Value.ToString & "' ", "")
                    Else
                        drRow = mdtView.Table.Select(objdgcolhEmployeeunkid.DataPropertyName & " = " & CInt(dgvEmpBankList.Rows(e.RowIndex).Cells(objdgcolhEmployeeunkid.Index).Value) & " ", "")
                    End If
                    If drRow.Length > 0 Then
                        For index As Integer = 0 To drRow.Length - 1
                            drRow(index)("isChecked") = CBool(dgvEmpBankList.Rows(e.RowIndex).Cells(objdgcolhCheck.Index).Value)
                        Next
                    End If
                End If

                drRow = mdtView.ToTable.Select("isChecked = true", "")

                If drRow.Length > 0 Then
                    If mdtView.ToTable.Rows.Count = drRow.Length Then
                        objchkSelectAll.CheckState = CheckState.Checked
                    Else
                        objchkSelectAll.CheckState = CheckState.Indeterminate
                    End If
                Else
                    objchkSelectAll.CheckState = CheckState.Unchecked
                End If

            End If

            AddHandler objchkSelectAll.CheckedChanged, AddressOf chkSelectAll_CheckedChanged

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgvEmpBankList_CellContentClick", mstrModuleName)
        End Try
    End Sub
    'Sohail (13 Jan 2022) -- End

    'Hemant (11 Mar 2018) -- Start
    'ISSUE : performance issue on emp cost centre list screen, taking 30 to 45 mins to open
    Private Sub dgvEmpBankList_CellPainting(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellPaintingEventArgs) Handles dgvEmpBankList.CellPainting
        Try
            If e.RowIndex >= 0 AndAlso e.RowIndex < dgvEmpBankList.RowCount - 1 AndAlso CBool(dgvEmpBankList.Rows(e.RowIndex).Cells(objdgcolhIsGrp.Index).Value) = True AndAlso e.ColumnIndex > 0 Then
                If (e.ColumnIndex = objdgColhBlank.Index) Then
                    Dim backColorBrush As Brush = New SolidBrush(Color.Gray)
                    Dim totWidth As Integer = 0
                    For i As Integer = 1 To dgvEmpBankList.Columns.Count - 1
                        totWidth += dgvEmpBankList.Columns(i).Width
                    Next
                    Dim r As New RectangleF(e.CellBounds.Left, e.CellBounds.Top, totWidth, e.CellBounds.Height)
                    e.Graphics.FillRectangle(backColorBrush, r)
                    If chkGroupByEmpName.Checked = False Then
                        e.Graphics.DrawString(CType(dgvEmpBankList.Rows(e.RowIndex).Cells(dgcolhCostCenter.Index).Value.ToString, String), e.CellStyle.Font, Brushes.White, e.CellBounds.X, e.CellBounds.Y + 5)
                    Else
                        e.Graphics.DrawString(CType(dgvEmpBankList.Rows(e.RowIndex).Cells(dgcolhEmpName.Index).Value.ToString & " : [" & dgvEmpBankList.Rows(e.RowIndex).Cells(dgcolhEmpCode.Index).Value.ToString & "]", String), e.CellStyle.Font, Brushes.White, e.CellBounds.X, e.CellBounds.Y + 5)
                    End If
                End If

                e.Handled = True
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgvEmpBankList_CellPainting", mstrModuleName)
        End Try
    End Sub
    'Hemant (11 Mar 2018) -- End
#End Region

    'Sohail (13 Jan 2022) -- Start
    'NMB Enhancement # OLD-101 : - Give Checkbox selection option on Employee Cost Center Screen.
#Region " CheckBox's Events "
    Private Sub chkSelectAll_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objchkSelectAll.CheckedChanged
        Try
            If mdtView Is Nothing Then Exit Sub
            RemoveHandler dgvEmpBankList.CellContentClick, AddressOf dgvEmpBankList_CellContentClick
            For Each dr As DataRowView In mdtView
                dr.Item(objdgcolhCheck.DataPropertyName) = CBool(objchkSelectAll.CheckState)
            Next
            dgvEmpBankList.Refresh()
            
            AddHandler dgvEmpBankList.CellContentClick, AddressOf dgvEmpBankList_CellContentClick

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "chkSelectAll_CheckedChanged", mstrModuleName)
        End Try
    End Sub
#End Region
    'Sohail (13 Jan 2022) -- End

#Region " Other Control's Events "
    Private Sub objbtnSearchEmployee_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchEmployee.Click
        Dim objFrm As New frmCommonSearch
        Try
            With objFrm
                .DataSource = CType(cboEmployee.DataSource, DataTable)
                .ValueMember = cboEmployee.ValueMember
                .DisplayMember = cboEmployee.DisplayMember
                .CodeMember = "employeecode"
            End With

            If objFrm.DisplayDialog Then
                cboEmployee.SelectedValue = objFrm.SelectedValue
                cboEmployee.Focus()
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchEmployee_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objSearchED_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objSearchED.Click
        Dim objfrmCommonsearch As New frmCommonSearch
        Try
            objfrmCommonsearch.DataSource = CType(cboTranHead.DataSource, DataTable)
            objfrmCommonsearch.ValueMember = cboTranHead.ValueMember
            objfrmCommonsearch.DisplayMember = cboTranHead.DisplayMember
            objfrmCommonsearch.CodeMember = "code"
            If objfrmCommonsearch.DisplayDialog() Then
                cboTranHead.SelectedValue = objfrmCommonsearch.SelectedValue
            End If
            cboTranHead.Select()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objSearchED_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objSearchCostCenter_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objSearchCostCenter.Click
        'Sohail (07 Feb 2019) -- Start
        'NMB Enhancement - 76.1 - Employee cost centre should be distributed on all allocations instead of only on cost centre.
        If CType(cboCostCenter.DataSource, DataTable) Is Nothing Then Exit Sub
        'Sohail (07 Feb 2019) -- End
        Dim objfrmCommonsearch As New frmCommonSearch
        Try
            objfrmCommonsearch.DataSource = CType(cboCostCenter.DataSource, DataTable)
            objfrmCommonsearch.ValueMember = cboCostCenter.ValueMember
            objfrmCommonsearch.DisplayMember = cboCostCenter.DisplayMember
            objfrmCommonsearch.CodeMember = "costcentercode"
            If objfrmCommonsearch.DisplayDialog() Then
                cboCostCenter.SelectedValue = objfrmCommonsearch.SelectedValue
            End If
            cboCostCenter.Select()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objSearchCostCenter_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnSearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearch.Click
        Try
            Call FillList()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearch_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnReset.Click
        Try
            cboEmployee.SelectedValue = 0
            cboTranHead.SelectedValue = 0
            'Sohail (07 Feb 2019) -- Start
            'NMB Enhancement - 76.1 - Employee cost centre should be distributed on all allocations instead of only on cost centre.
            cboAllocation.SelectedValue = 0
            'Sohail (07 Feb 2019) -- End
            cboCostCenter.SelectedValue = 0
            txtPercFrom.Decimal = 0
            txtPercTo.Decimal = 0
            Call FillList()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnReset_Click", mstrModuleName)
        End Try
    End Sub

    'Sohail (29 Mar 2017) -- Start
    'CCBRT Enhancement - 66.1 - Employee cost center distribution for non-salary heads, Effective period option on Employee Cost Center.
    Private Sub chkGroupByEmpName_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkGroupByEmpName.CheckedChanged
        Try
            Call FillList()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "chkGroupByEmpName_CheckedChanged", mstrModuleName)
        End Try
    End Sub
    'Sohail (29 Mar 2017) -- End

#End Region



   
    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Private Sub OtherSettings()
        Try
            Me.SuspendLayout()
            
            Call SetLanguage()
			
			Me.gbFilterCriteria.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbFilterCriteria.ForeColor = GUI._eZeeContainerHeaderForeColor 


			
			Me.eZeeHeader.GradientColor1 = GUI._HeaderBackColor1 
			Me.eZeeHeader.GradientColor2 = GUI._HeaderBackColor2 
			Me.eZeeHeader.BorderColor = GUI._HeaderBorderColor 
			Me.eZeeHeader.HeaderTextForeColor = GUI._HeaderTitleFontColor 
			Me.eZeeHeader.DescriptionForeColor = GUI._HeaderMessageFontColor 


			Me.btnClose.GradientBackColor = GUI._ButttonBackColor 
            Me.btnClose.GradientForeColor = GUI._ButttonFontColor

			Me.btnEdit.GradientBackColor = GUI._ButttonBackColor 
            Me.btnEdit.GradientForeColor = GUI._ButttonFontColor

			Me.btnNew.GradientBackColor = GUI._ButttonBackColor 
            Me.btnNew.GradientForeColor = GUI._ButttonFontColor

			Me.btnInactive.GradientBackColor = GUI._ButttonBackColor 
			Me.btnInactive.GradientForeColor = GUI._ButttonFontColor

			Me.btnVoid.GradientBackColor = GUI._ButttonBackColor 
			Me.btnVoid.GradientForeColor = GUI._ButttonFontColor


            Me.ResumeLayout()
        Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "OtherSettings", mstrModuleName)
        End Try
    End Sub
			
			
    Private Sub SetLanguage()
        Try
            Me.Text = Language._Object.getCaption(Me.Name, Me.Text)
			
			Me.eZeeHeader.Title = Language._Object.getCaption(Me.eZeeHeader.Name & "_Title" , Me.eZeeHeader.Title)
			Me.eZeeHeader.Message = Language._Object.getCaption(Me.eZeeHeader.Name & "_Message" , Me.eZeeHeader.Message)
            Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
            Me.btnEdit.Text = Language._Object.getCaption(Me.btnEdit.Name, Me.btnEdit.Text)
            Me.btnNew.Text = Language._Object.getCaption(Me.btnNew.Name, Me.btnNew.Text)
            Me.gbFilterCriteria.Text = Language._Object.getCaption(Me.gbFilterCriteria.Name, Me.gbFilterCriteria.Text)
            Me.lblPercTo.Text = Language._Object.getCaption(Me.lblPercTo.Name, Me.lblPercTo.Text)
            Me.lblCostCenter.Text = Language._Object.getCaption(Me.lblCostCenter.Name, Me.lblCostCenter.Text)
            Me.lblPercFrom.Text = Language._Object.getCaption(Me.lblPercFrom.Name, Me.lblPercFrom.Text)
            Me.lblTranHead.Text = Language._Object.getCaption(Me.lblTranHead.Name, Me.lblTranHead.Text)
            Me.lblEmployee.Text = Language._Object.getCaption(Me.lblEmployee.Name, Me.lblEmployee.Text)
			Me.chkGroupByEmpName.Text = Language._Object.getCaption(Me.chkGroupByEmpName.Name, Me.chkGroupByEmpName.Text)
			Me.lblPeriod.Text = Language._Object.getCaption(Me.lblPeriod.Name, Me.lblPeriod.Text)
			Me.lblAllocation.Text = Language._Object.getCaption(Me.lblAllocation.Name, Me.lblAllocation.Text)
			Me.btnInactive.Text = Language._Object.getCaption(Me.btnInactive.Name, Me.btnInactive.Text)
			Me.lblactiveinactive.Text = Language._Object.getCaption(Me.lblactiveinactive.Name, Me.lblactiveinactive.Text)
            Me.dgcolhEmpCode.HeaderText = Language._Object.getCaption(Me.dgcolhEmpCode.Name, Me.dgcolhEmpCode.HeaderText)
            Me.dgcolhEmpName.HeaderText = Language._Object.getCaption(Me.dgcolhEmpName.Name, Me.dgcolhEmpName.HeaderText)
            Me.dgcolhCostCenter.HeaderText = Language._Object.getCaption(Me.dgcolhCostCenter.Name, Me.dgcolhCostCenter.HeaderText)
            Me.dgcolhTranHead.HeaderText = Language._Object.getCaption(Me.dgcolhTranHead.Name, Me.dgcolhTranHead.HeaderText)
            Me.dgcolhAllocationByName.HeaderText = Language._Object.getCaption(Me.dgcolhAllocationByName.Name, Me.dgcolhAllocationByName.HeaderText)
            Me.dgcolhPeriod.HeaderText = Language._Object.getCaption(Me.dgcolhPeriod.Name, Me.dgcolhPeriod.HeaderText)
            Me.dgcolhPercentage.HeaderText = Language._Object.getCaption(Me.dgcolhPercentage.Name, Me.dgcolhPercentage.HeaderText)
			Me.btnVoid.Text = Language._Object.getCaption(Me.btnVoid.Name, Me.btnVoid.Text)

        Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetLanguage", mstrModuleName)
        End Try
    End Sub
			
			
    Private Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Please select Employee from the list to perform further operation.")
	    Language.setMessage(mstrModuleName, 2, "Sorry, You cannot edit this employee cost center. Reason : Period is already closed")
			Language.setMessage(mstrModuleName, 3, "Sorry, You cannot inactive this employee cost center. Reason : Period is already opened.")
			Language.setMessage(mstrModuleName, 4, "Sorry, selected period is already closed.")
			Language.setMessage(mstrModuleName, 5, "Sorry, #headname# is already mapped in selected period #periodname#.")
			Language.setMessage(mstrModuleName, 6, "Are you sure you want to inactive ticked employee cost centers from #periodname#?")
			Language.setMessage(mstrModuleName, 7, "Sorry, You cannot void some employee cost centers. Reason : Period is already closed for some employee cost centers.")
			Language.setMessage(mstrModuleName, 8, "Sorry, Payroll Process is already done for some of the employees.")
			Language.setMessage(mstrModuleName, 9, "Do you want to void Payroll?")
			Language.setMessage(mstrModuleName, 10, "Sorry, Total Percentage must be 100 for employee :")
			Language.setMessage(mstrModuleName, 11, "Sorry, You can not delete this Employee Cost Center. Some Employee Cost Center is in used.")
			Language.setMessage(mstrModuleName, 12, "Are you sure you want to delete ticked employee cost centers?")

        Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class