﻿Option Strict On

Imports eZeeCommonLib
Imports Aruti.Data

'Last Message Index = 3

Public Class frmCostcenter_AddEdit

#Region "Private Variable"

    Private ReadOnly mstrModuleName As String = "frmCostcenter_AddEdit"
    Private mblnCancel As Boolean = True
    Private objCostcentermaster As clscostcenter_master
    Private menAction As enAction = enAction.ADD_ONE
    Private mintCostcenterMasterUnkid As Integer = -1

#End Region

#Region " Display Dialog "

    Public Function displayDialog(ByRef intUnkId As Integer, ByVal eAction As enAction) As Boolean
        Try
            mintCostcenterMasterUnkid = intUnkId
            menAction = eAction

            Me.ShowDialog()

            intUnkId = mintCostcenterMasterUnkid

            Return Not mblnCancel
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "displayDialog", mstrModuleName)
        End Try
    End Function

#End Region

#Region "Form's Event"

    Private Sub frmCostcenter_AddEdit_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        objCostcentermaster = New clscostcenter_master
        Try
            Call Set_Logo(Me, gApplicationType)

            'Anjan (02 Sep 2011)-Start
            'Issue : Including Language Settings.
            Language.setLanguage(Me.Name)
            Call OtherSettings()
            'Anjan (02 Sep 2011)-End 

            setColor()
            FillCombo()
            If menAction = enAction.EDIT_ONE Then
                objCostcentermaster._Costcenterunkid = mintCostcenterMasterUnkid
                'Anjan (04 Apr 2011)-Start
                'Issue : on import of employee group of new cost center goes 0 by default, so group of cost center has been put open on edit mode.
                'cboGroup.Enabled = False
                'Anjan (04 Apr 2011)-End
                'Sohail (11 Sep 2010) -- Start
                objbtnAddGroup.Enabled = False
                'Sohail (11 Sep 2010) -- End
            End If
            'FillCombo()
            GetValue()
            cboGroup.Select()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmCostcenter_AddEdit_Load", mstrModuleName)
        End Try
    End Sub

    Private Sub frmCostcenter_AddEdit_KeyPress(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles MyBase.KeyPress
        Try
            If Asc(e.KeyChar) = 13 Then
                SendKeys.Send("{TAB}")
                e.Handled = True
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmCostcenter_AddEdit_KeyPress", mstrModuleName)
        End Try
    End Sub

    Private Sub frmCostcenter_AddEdit_KeyDown(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles MyBase.KeyDown
        Try
            If e.Control = True And e.KeyCode = Keys.S Then
                btnSave_Click(sender, e)
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmCostcenter_AddEdit_KeyDown", mstrModuleName)
        End Try
    End Sub

    Private Sub frmCostcenter_AddEdit_FormClosed(ByVal sender As System.Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles MyBase.FormClosed
        objCostcentermaster = Nothing
    End Sub
    'Anjan (02 Sep 2011)-Start
    'Issue : Including Language Settings.
    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clscostcenter_master.SetMessages()
            objfrm._Other_ModuleNames = "clscostcenter_master"
            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub
    'Anjan (02 Sep 2011)-End 
#End Region

#Region "Button's Event"

    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Dim blnFlag As Boolean = False
        Try
            If CInt(cboGroup.SelectedValue) = 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "CostCenter Group is compulsory information.Please Select CostCenter Group."), enMsgBoxStyle.Information)
                cboGroup.Focus()
                Exit Sub
            ElseIf Trim(txtCode.Text) = "" Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "CostCenter Code cannot be blank. CostCenter Code is required information."), enMsgBoxStyle.Information)
                txtCode.Focus()
                Exit Sub
            ElseIf Trim(txtName.Text) = "" Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "CostCenter Name cannot be blank. CostCenter Name is required information."), enMsgBoxStyle.Information)
                txtName.Focus()
                Exit Sub
            End If

            Call SetValue()

            'S.SANDEEP [28-May-2018] -- START
            'ISSUE/ENHANCEMENT : {Audit Trails} 
            objCostcentermaster._FormName = mstrModuleName
            objCostcentermaster._LoginEmployeeunkid = 0
            objCostcentermaster._ClientIP = getIP()
            objCostcentermaster._HostName = getHostName()
            objCostcentermaster._FromWeb = False
            objCostcentermaster._AuditUserId = User._Object._Userunkid
objCostcentermaster._CompanyUnkid = Company._Object._Companyunkid
            objCostcentermaster._AuditDate = ConfigParameter._Object._CurrentDateAndTime
            'S.SANDEEP [28-May-2018] -- END

            If menAction = enAction.EDIT_ONE Then
                blnFlag = objCostcentermaster.Update()
            Else
                blnFlag = objCostcentermaster.Insert()
            End If

            If blnFlag = False And objCostcentermaster._Message <> "" Then
                eZeeMsgBox.Show(objCostcentermaster._Message, enMsgBoxStyle.Information)
            End If

            If blnFlag Then
                mblnCancel = False
                If menAction = enAction.ADD_CONTINUE Then
                    objCostcentermaster = Nothing
                    objCostcentermaster = New clscostcenter_master
                    Call GetValue()
                    cboGroup.Select()
                Else
                    mintCostcenterMasterUnkid = objCostcentermaster._Costcenterunkid
                    Me.Close()
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnSave_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnOtherLanguage_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnOtherLanguage.Click
        Dim objFrmLangPopup As New NameLanguagePopup_Form
        Try
            'Anjan (02 Sep 2011)-Start
            'Issue : Including Language Settings.
            If User._Object._Isrighttoleft = True Then
                objFrmLangPopup.RightToLeft = Windows.Forms.RightToLeft.Yes
                objFrmLangPopup.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objFrmLangPopup)
            End If
            'Anjan (02 Sep 2011)-End 
            Call objFrmLangPopup.displayDialog(txtName.Text, objCostcentermaster._Costcentername1, objCostcentermaster._Costcentername2)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnOtherLanguage_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnAddGroup_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnAddGroup.Click
        Try
            
            Dim ObjPayrollGroups_AddEdit As New frmPayrollGroups_AddEdit

            'Anjan (02 Sep 2011)-Start
            'Issue : Including Language Settings.
            If User._Object._Isrighttoleft = True Then
                ObjPayrollGroups_AddEdit.RightToLeft = Windows.Forms.RightToLeft.Yes
                ObjPayrollGroups_AddEdit.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(ObjPayrollGroups_AddEdit)
            End If
            'Anjan (02 Sep 2011)-End 

            ObjPayrollGroups_AddEdit.lblwebsite.Visible = False
            ObjPayrollGroups_AddEdit.txtwebsite.Visible = False

            ObjPayrollGroups_AddEdit.lblDescription.Location = ObjPayrollGroups_AddEdit.lblSwiftcode.Location
            ObjPayrollGroups_AddEdit.txtDescription.Location = ObjPayrollGroups_AddEdit.txtSwiftcode.Location

            ObjPayrollGroups_AddEdit.lblSwiftcode.Visible = False
            ObjPayrollGroups_AddEdit.txtSwiftcode.Visible = False

            'ObjPayrollGroups_AddEdit.gbGroupInfo.Size = New Size(374, 180)
            'ObjPayrollGroups_AddEdit.Size = New Size(403, 279)

            ObjPayrollGroups_AddEdit.gbGroupInfo.Size = New Size(402, 155)
            ObjPayrollGroups_AddEdit.Size = New Size(431, 255)

            If ObjPayrollGroups_AddEdit.displayDialog(-1, enAction.ADD_CONTINUE, PayrollGroupType.CostCenter) Then
                FillCombo()
                cboGroup.Select()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnAddGroup_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        'Sohail (11 Sep 2010) -- Start
        Try
        Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnClose_Click", mstrModuleName)
        End Try
        'Sohail (11 Sep 2010) -- End
    End Sub

#End Region

#Region "Private Methods"

    Private Sub setColor()
        Try
            cboGroup.BackColor = GUI.ColorComp
            txtCode.BackColor = GUI.ColorComp
            txtName.BackColor = GUI.ColorComp
            txtDescription.BackColor = GUI.ColorOptional
            'S.SANDEEP [09-AUG-2018] -- START
            'ISSUE/ENHANCEMENT : {Ref#292}
            txtCustomCode.BackColor = GUI.ColorOptional
            'S.SANDEEP [09-AUG-2018] -- END
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "setColor", mstrModuleName)
        End Try
    End Sub

    Private Sub GetValue()
        Try
            txtCode.Text = objCostcentermaster._Costcentercode
            txtName.Text = objCostcentermaster._Costcentername
            txtDescription.Text = objCostcentermaster._Description
            cboGroup.SelectedValue = CInt(objCostcentermaster._Costcentergroupmasterunkid)
            'S.SANDEEP [09-AUG-2018] -- START
            'ISSUE/ENHANCEMENT : {Ref#292}
            txtCustomCode.Text = objCostcentermaster._Customcode
            'S.SANDEEP [09-AUG-2018] -- END
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetValue", mstrModuleName)
        End Try
    End Sub

    Private Sub SetValue()
        Try
            objCostcentermaster._Costcentercode = txtCode.Text.Trim
            objCostcentermaster._Costcentername = txtName.Text.Trim
            objCostcentermaster._Description = txtDescription.Text.Trim
            objCostcentermaster._Costcentergroupmasterunkid = CInt(cboGroup.SelectedValue)
            'S.SANDEEP [09-AUG-2018] -- START
            'ISSUE/ENHANCEMENT : {Ref#292}
            objCostcentermaster._Customcode = txtCustomCode.Text
            'S.SANDEEP [09-AUG-2018] -- END
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetValue", mstrModuleName)
        End Try
    End Sub

    Private Sub FillCombo()
        Try
            Dim objGroupMaster As New clspayrollgroup_master
            Dim dsGroup As DataSet = objGroupMaster.getListForCombo(CInt(PayrollGroupType.CostCenter), "CostCenter Group", True)
            cboGroup.ValueMember = "groupmasterunkid"
            cboGroup.DisplayMember = "name"
            cboGroup.DataSource = dsGroup.Tables("CostCenter Group")
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        End Try
    End Sub

#End Region

    'S.SANDEEP [09-AUG-2018] -- START
    'ISSUE/ENHANCEMENT : {Ref#292}
#Region " Textbox Event(s) "

    Private Sub txtCode_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtCode.LostFocus
        Try
            If txtCustomCode.Text.Trim.Length <= 0 Then
                txtCustomCode.Text = txtCode.Text
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "txtCode_LostFocus", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region
    'S.SANDEEP [09-AUG-2018] -- END

 
	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Private Sub OtherSettings()
		Try
			Me.SuspendLayout()
			
			Call SetLanguage()
			
			Me.gbCostcenterInformation.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbCostcenterInformation.ForeColor = GUI._eZeeContainerHeaderForeColor 

			Me.btnClose.GradientBackColor = GUI._ButttonBackColor 
			Me.btnClose.GradientForeColor = GUI._ButttonFontColor

			Me.btnSave.GradientBackColor = GUI._ButttonBackColor 
			Me.btnSave.GradientForeColor = GUI._ButttonFontColor


			Me.ResumeLayout()
		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "OtherSettings", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetLanguage()
		Try
			Me.Text = Language._Object.getCaption(Me.Name, Me.Text)
			
			Me.gbCostcenterInformation.Text = Language._Object.getCaption(Me.gbCostcenterInformation.Name, Me.gbCostcenterInformation.Text)
			Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
			Me.btnSave.Text = Language._Object.getCaption(Me.btnSave.Name, Me.btnSave.Text)
			Me.lblGroup.Text = Language._Object.getCaption(Me.lblGroup.Name, Me.lblGroup.Text)
			Me.lblCode.Text = Language._Object.getCaption(Me.lblCode.Name, Me.lblCode.Text)
			Me.lblDescription.Text = Language._Object.getCaption(Me.lblDescription.Name, Me.lblDescription.Text)
			Me.lblName.Text = Language._Object.getCaption(Me.lblName.Name, Me.lblName.Text)

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetLanguage", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "CostCenter Group is compulsory information.Please Select CostCenter Group.")
			Language.setMessage(mstrModuleName, 2, "CostCenter Code cannot be blank. CostCenter Code is required information.")
			Language.setMessage(mstrModuleName, 3, "CostCenter Name cannot be blank. CostCenter Name is required information.")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>

    
End Class