﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmEmpDistributedCostCenterList
    Inherits eZee.Common.eZeeForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmEmpDistributedCostCenterList))
        Me.pnlMainInfo = New System.Windows.Forms.Panel
        Me.objchkSelectAll = New System.Windows.Forms.CheckBox
        Me.dgvEmpBankList = New System.Windows.Forms.DataGridView
        Me.objdgcolhCheck = New System.Windows.Forms.DataGridViewCheckBoxColumn
        Me.objdgColhBlank = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhEmpCode = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhEmpName = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhCostCenter = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhTranHead = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhAllocationByName = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhPeriod = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhPercentage = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgColhPeriodStatusId = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhIsGrp = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhIsinactive = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgColhTranUnkid = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhEmployeeunkid = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhTranheadunkid = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhAllocationByid = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhCostcenterUnkid = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhPeriodunkid = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.eZeeHeader = New eZee.Common.eZeeHeader
        Me.objFooter = New eZee.Common.eZeeFooter
        Me.btnInactive = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnClose = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnEdit = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnNew = New eZee.Common.eZeeLightButton(Me.components)
        Me.gbFilterCriteria = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.EZeeStraightLine1 = New eZee.Common.eZeeStraightLine
        Me.cboActiveInactive = New System.Windows.Forms.ComboBox
        Me.lblactiveinactive = New System.Windows.Forms.Label
        Me.lblAllocation = New System.Windows.Forms.Label
        Me.cboAllocation = New System.Windows.Forms.ComboBox
        Me.lblPeriod = New System.Windows.Forms.Label
        Me.cboPeriod = New System.Windows.Forms.ComboBox
        Me.chkGroupByEmpName = New System.Windows.Forms.CheckBox
        Me.objSearchCostCenter = New eZee.Common.eZeeGradientButton
        Me.objSearchED = New eZee.Common.eZeeGradientButton
        Me.txtPercTo = New eZee.TextBox.NumericTextBox
        Me.txtPercFrom = New eZee.TextBox.NumericTextBox
        Me.lblPercTo = New System.Windows.Forms.Label
        Me.lblCostCenter = New System.Windows.Forms.Label
        Me.lblPercFrom = New System.Windows.Forms.Label
        Me.cboCostCenter = New System.Windows.Forms.ComboBox
        Me.cboTranHead = New System.Windows.Forms.ComboBox
        Me.lblTranHead = New System.Windows.Forms.Label
        Me.objLine1 = New eZee.Common.eZeeStraightLine
        Me.objbtnReset = New eZee.Common.eZeeSearchResetButton(Me.components)
        Me.objbtnSearchEmployee = New eZee.Common.eZeeGradientButton
        Me.objbtnSearch = New eZee.Common.eZeeSearchResetButton(Me.components)
        Me.cboEmployee = New System.Windows.Forms.ComboBox
        Me.lblEmployee = New System.Windows.Forms.Label
        Me.btnVoid = New eZee.Common.eZeeLightButton(Me.components)
        Me.pnlMainInfo.SuspendLayout()
        CType(Me.dgvEmpBankList, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.objFooter.SuspendLayout()
        Me.gbFilterCriteria.SuspendLayout()
        CType(Me.objbtnReset, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.objbtnSearch, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'pnlMainInfo
        '
        Me.pnlMainInfo.Controls.Add(Me.objchkSelectAll)
        Me.pnlMainInfo.Controls.Add(Me.dgvEmpBankList)
        Me.pnlMainInfo.Controls.Add(Me.eZeeHeader)
        Me.pnlMainInfo.Controls.Add(Me.objFooter)
        Me.pnlMainInfo.Controls.Add(Me.gbFilterCriteria)
        Me.pnlMainInfo.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pnlMainInfo.Location = New System.Drawing.Point(0, 0)
        Me.pnlMainInfo.Name = "pnlMainInfo"
        Me.pnlMainInfo.Size = New System.Drawing.Size(874, 612)
        Me.pnlMainInfo.TabIndex = 0
        '
        'objchkSelectAll
        '
        Me.objchkSelectAll.AutoSize = True
        Me.objchkSelectAll.Location = New System.Drawing.Point(21, 198)
        Me.objchkSelectAll.Name = "objchkSelectAll"
        Me.objchkSelectAll.Size = New System.Drawing.Size(15, 14)
        Me.objchkSelectAll.TabIndex = 19
        Me.objchkSelectAll.UseVisualStyleBackColor = True
        '
        'dgvEmpBankList
        '
        Me.dgvEmpBankList.AllowUserToAddRows = False
        Me.dgvEmpBankList.AllowUserToDeleteRows = False
        Me.dgvEmpBankList.AllowUserToResizeRows = False
        Me.dgvEmpBankList.BackgroundColor = System.Drawing.Color.WhiteSmoke
        Me.dgvEmpBankList.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing
        Me.dgvEmpBankList.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.objdgcolhCheck, Me.objdgColhBlank, Me.dgcolhEmpCode, Me.dgcolhEmpName, Me.dgcolhCostCenter, Me.dgcolhTranHead, Me.dgcolhAllocationByName, Me.dgcolhPeriod, Me.dgcolhPercentage, Me.objdgColhPeriodStatusId, Me.objdgcolhIsGrp, Me.objdgcolhIsinactive, Me.objdgColhTranUnkid, Me.objdgcolhEmployeeunkid, Me.objdgcolhTranheadunkid, Me.objdgcolhAllocationByid, Me.objdgcolhCostcenterUnkid, Me.objdgcolhPeriodunkid})
        Me.dgvEmpBankList.Location = New System.Drawing.Point(12, 193)
        Me.dgvEmpBankList.Name = "dgvEmpBankList"
        Me.dgvEmpBankList.RowHeadersVisible = False
        Me.dgvEmpBankList.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing
        Me.dgvEmpBankList.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvEmpBankList.Size = New System.Drawing.Size(850, 358)
        Me.dgvEmpBankList.TabIndex = 18
        Me.dgvEmpBankList.TabStop = False
        '
        'objdgcolhCheck
        '
        Me.objdgcolhCheck.HeaderText = ""
        Me.objdgcolhCheck.Name = "objdgcolhCheck"
        Me.objdgcolhCheck.Width = 30
        '
        'objdgColhBlank
        '
        Me.objdgColhBlank.HeaderText = ""
        Me.objdgColhBlank.Name = "objdgColhBlank"
        Me.objdgColhBlank.ReadOnly = True
        Me.objdgColhBlank.Width = 30
        '
        'dgcolhEmpCode
        '
        Me.dgcolhEmpCode.HeaderText = "Emp. Code"
        Me.dgcolhEmpCode.Name = "dgcolhEmpCode"
        Me.dgcolhEmpCode.ReadOnly = True
        Me.dgcolhEmpCode.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.dgcolhEmpCode.Width = 150
        '
        'dgcolhEmpName
        '
        Me.dgcolhEmpName.HeaderText = "Employee Name"
        Me.dgcolhEmpName.Name = "dgcolhEmpName"
        Me.dgcolhEmpName.ReadOnly = True
        Me.dgcolhEmpName.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.dgcolhEmpName.Width = 225
        '
        'dgcolhCostCenter
        '
        Me.dgcolhCostCenter.HeaderText = "Cost Center"
        Me.dgcolhCostCenter.Name = "dgcolhCostCenter"
        Me.dgcolhCostCenter.ReadOnly = True
        Me.dgcolhCostCenter.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.dgcolhCostCenter.Width = 225
        '
        'dgcolhTranHead
        '
        Me.dgcolhTranHead.HeaderText = "Transaction Head"
        Me.dgcolhTranHead.Name = "dgcolhTranHead"
        Me.dgcolhTranHead.ReadOnly = True
        Me.dgcolhTranHead.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.dgcolhTranHead.Width = 170
        '
        'dgcolhAllocationByName
        '
        Me.dgcolhAllocationByName.HeaderText = "AllocationBy Name"
        Me.dgcolhAllocationByName.Name = "dgcolhAllocationByName"
        Me.dgcolhAllocationByName.ReadOnly = True
        Me.dgcolhAllocationByName.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.dgcolhAllocationByName.Visible = False
        '
        'dgcolhPeriod
        '
        Me.dgcolhPeriod.HeaderText = "Period"
        Me.dgcolhPeriod.Name = "dgcolhPeriod"
        Me.dgcolhPeriod.ReadOnly = True
        Me.dgcolhPeriod.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'dgcolhPercentage
        '
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle1.Format = "N2"
        DataGridViewCellStyle1.NullValue = Nothing
        Me.dgcolhPercentage.DefaultCellStyle = DataGridViewCellStyle1
        Me.dgcolhPercentage.HeaderText = "Percentage(%)"
        Me.dgcolhPercentage.Name = "dgcolhPercentage"
        Me.dgcolhPercentage.ReadOnly = True
        Me.dgcolhPercentage.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.dgcolhPercentage.Width = 75
        '
        'objdgColhPeriodStatusId
        '
        Me.objdgColhPeriodStatusId.HeaderText = "objcolhPeriodStatusId"
        Me.objdgColhPeriodStatusId.Name = "objdgColhPeriodStatusId"
        Me.objdgColhPeriodStatusId.ReadOnly = True
        Me.objdgColhPeriodStatusId.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.objdgColhPeriodStatusId.Visible = False
        Me.objdgColhPeriodStatusId.Width = 5
        '
        'objdgcolhIsGrp
        '
        Me.objdgcolhIsGrp.HeaderText = "IsGrp"
        Me.objdgcolhIsGrp.Name = "objdgcolhIsGrp"
        Me.objdgcolhIsGrp.ReadOnly = True
        Me.objdgcolhIsGrp.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.objdgcolhIsGrp.Visible = False
        '
        'objdgcolhIsinactive
        '
        Me.objdgcolhIsinactive.HeaderText = "Inactive Status"
        Me.objdgcolhIsinactive.Name = "objdgcolhIsinactive"
        Me.objdgcolhIsinactive.ReadOnly = True
        '
        'objdgColhTranUnkid
        '
        Me.objdgColhTranUnkid.HeaderText = "tranunkid"
        Me.objdgColhTranUnkid.Name = "objdgColhTranUnkid"
        Me.objdgColhTranUnkid.ReadOnly = True
        Me.objdgColhTranUnkid.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.objdgColhTranUnkid.Visible = False
        Me.objdgColhTranUnkid.Width = 5
        '
        'objdgcolhEmployeeunkid
        '
        Me.objdgcolhEmployeeunkid.HeaderText = "objdgcolhEmployeeunkid"
        Me.objdgcolhEmployeeunkid.Name = "objdgcolhEmployeeunkid"
        Me.objdgcolhEmployeeunkid.ReadOnly = True
        Me.objdgcolhEmployeeunkid.Visible = False
        '
        'objdgcolhTranheadunkid
        '
        Me.objdgcolhTranheadunkid.HeaderText = "objdgcolhTranheadunkid"
        Me.objdgcolhTranheadunkid.Name = "objdgcolhTranheadunkid"
        Me.objdgcolhTranheadunkid.ReadOnly = True
        Me.objdgcolhTranheadunkid.Visible = False
        '
        'objdgcolhAllocationByid
        '
        Me.objdgcolhAllocationByid.HeaderText = "objdgcolhAllocationByid"
        Me.objdgcolhAllocationByid.Name = "objdgcolhAllocationByid"
        Me.objdgcolhAllocationByid.ReadOnly = True
        Me.objdgcolhAllocationByid.Visible = False
        '
        'objdgcolhCostcenterUnkid
        '
        Me.objdgcolhCostcenterUnkid.HeaderText = "objdgcolhCostcenterUnkid"
        Me.objdgcolhCostcenterUnkid.Name = "objdgcolhCostcenterUnkid"
        Me.objdgcolhCostcenterUnkid.ReadOnly = True
        Me.objdgcolhCostcenterUnkid.Visible = False
        '
        'objdgcolhPeriodunkid
        '
        Me.objdgcolhPeriodunkid.HeaderText = "objdgcolhPeriodunkid"
        Me.objdgcolhPeriodunkid.Name = "objdgcolhPeriodunkid"
        Me.objdgcolhPeriodunkid.ReadOnly = True
        Me.objdgcolhPeriodunkid.Visible = False
        '
        'eZeeHeader
        '
        Me.eZeeHeader.BackColor = System.Drawing.SystemColors.Control
        Me.eZeeHeader.BorderColor = System.Drawing.SystemColors.ControlDark
        Me.eZeeHeader.DescriptionForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.eZeeHeader.Dock = System.Windows.Forms.DockStyle.Top
        Me.eZeeHeader.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.eZeeHeader.GradiantStyle = eZee.Common.eZeeHeader.GradientStyle.Central
        Me.eZeeHeader.GradientColor1 = System.Drawing.SystemColors.Window
        Me.eZeeHeader.GradientColor2 = System.Drawing.SystemColors.Control
        Me.eZeeHeader.HeaderTextForeColor = System.Drawing.SystemColors.ControlText
        Me.eZeeHeader.Icon = Nothing
        Me.eZeeHeader.Location = New System.Drawing.Point(0, 0)
        Me.eZeeHeader.Message = ""
        Me.eZeeHeader.Name = "eZeeHeader"
        Me.eZeeHeader.Size = New System.Drawing.Size(874, 60)
        Me.eZeeHeader.TabIndex = 17
        Me.eZeeHeader.Title = "Employee Distributed Cost Center List"
        '
        'objFooter
        '
        Me.objFooter.BorderColor = System.Drawing.Color.Silver
        Me.objFooter.Controls.Add(Me.btnVoid)
        Me.objFooter.Controls.Add(Me.btnInactive)
        Me.objFooter.Controls.Add(Me.btnClose)
        Me.objFooter.Controls.Add(Me.btnEdit)
        Me.objFooter.Controls.Add(Me.btnNew)
        Me.objFooter.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.objFooter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objFooter.GradiantStyle = eZee.Common.eZeeFooter.GradientStyle.Up
        Me.objFooter.GradientColor1 = System.Drawing.SystemColors.Control
        Me.objFooter.GradientColor2 = System.Drawing.SystemColors.Control
        Me.objFooter.Location = New System.Drawing.Point(0, 557)
        Me.objFooter.Name = "objFooter"
        Me.objFooter.Size = New System.Drawing.Size(874, 55)
        Me.objFooter.TabIndex = 2
        '
        'btnInactive
        '
        Me.btnInactive.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnInactive.BackColor = System.Drawing.Color.White
        Me.btnInactive.BackgroundImage = CType(resources.GetObject("btnInactive.BackgroundImage"), System.Drawing.Image)
        Me.btnInactive.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnInactive.BorderColor = System.Drawing.Color.Empty
        Me.btnInactive.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnInactive.FlatAppearance.BorderSize = 0
        Me.btnInactive.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnInactive.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnInactive.ForeColor = System.Drawing.Color.Black
        Me.btnInactive.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnInactive.GradientForeColor = System.Drawing.Color.Black
        Me.btnInactive.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnInactive.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnInactive.Location = New System.Drawing.Point(12, 13)
        Me.btnInactive.Name = "btnInactive"
        Me.btnInactive.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnInactive.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnInactive.Size = New System.Drawing.Size(104, 30)
        Me.btnInactive.TabIndex = 3
        Me.btnInactive.Text = "&Inactive"
        Me.btnInactive.UseVisualStyleBackColor = True
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnClose.BackColor = System.Drawing.Color.White
        Me.btnClose.BackgroundImage = CType(resources.GetObject("btnClose.BackgroundImage"), System.Drawing.Image)
        Me.btnClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnClose.BorderColor = System.Drawing.Color.Empty
        Me.btnClose.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnClose.FlatAppearance.BorderSize = 0
        Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClose.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.ForeColor = System.Drawing.Color.Black
        Me.btnClose.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnClose.GradientForeColor = System.Drawing.Color.Black
        Me.btnClose.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Location = New System.Drawing.Point(769, 13)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Size = New System.Drawing.Size(93, 30)
        Me.btnClose.TabIndex = 2
        Me.btnClose.Text = "&Close"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'btnEdit
        '
        Me.btnEdit.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnEdit.BackColor = System.Drawing.Color.White
        Me.btnEdit.BackgroundImage = CType(resources.GetObject("btnEdit.BackgroundImage"), System.Drawing.Image)
        Me.btnEdit.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnEdit.BorderColor = System.Drawing.Color.Empty
        Me.btnEdit.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnEdit.FlatAppearance.BorderSize = 0
        Me.btnEdit.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnEdit.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnEdit.ForeColor = System.Drawing.Color.Black
        Me.btnEdit.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnEdit.GradientForeColor = System.Drawing.Color.Black
        Me.btnEdit.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnEdit.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnEdit.Location = New System.Drawing.Point(563, 13)
        Me.btnEdit.Name = "btnEdit"
        Me.btnEdit.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnEdit.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnEdit.Size = New System.Drawing.Size(104, 30)
        Me.btnEdit.TabIndex = 1
        Me.btnEdit.Text = "&Edit / Delete"
        Me.btnEdit.UseVisualStyleBackColor = True
        '
        'btnNew
        '
        Me.btnNew.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnNew.BackColor = System.Drawing.Color.White
        Me.btnNew.BackgroundImage = CType(resources.GetObject("btnNew.BackgroundImage"), System.Drawing.Image)
        Me.btnNew.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnNew.BorderColor = System.Drawing.Color.Empty
        Me.btnNew.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnNew.FlatAppearance.BorderSize = 0
        Me.btnNew.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnNew.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnNew.ForeColor = System.Drawing.Color.Black
        Me.btnNew.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnNew.GradientForeColor = System.Drawing.Color.Black
        Me.btnNew.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnNew.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnNew.Location = New System.Drawing.Point(467, 13)
        Me.btnNew.Name = "btnNew"
        Me.btnNew.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnNew.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnNew.Size = New System.Drawing.Size(90, 30)
        Me.btnNew.TabIndex = 0
        Me.btnNew.Text = "&New"
        Me.btnNew.UseVisualStyleBackColor = True
        '
        'gbFilterCriteria
        '
        Me.gbFilterCriteria.BorderColor = System.Drawing.Color.Black
        Me.gbFilterCriteria.Checked = False
        Me.gbFilterCriteria.CollapseAllExceptThis = False
        Me.gbFilterCriteria.CollapsedHoverImage = Nothing
        Me.gbFilterCriteria.CollapsedNormalImage = Nothing
        Me.gbFilterCriteria.CollapsedPressedImage = Nothing
        Me.gbFilterCriteria.CollapseOnLoad = False
        Me.gbFilterCriteria.Controls.Add(Me.EZeeStraightLine1)
        Me.gbFilterCriteria.Controls.Add(Me.cboActiveInactive)
        Me.gbFilterCriteria.Controls.Add(Me.lblactiveinactive)
        Me.gbFilterCriteria.Controls.Add(Me.lblAllocation)
        Me.gbFilterCriteria.Controls.Add(Me.cboAllocation)
        Me.gbFilterCriteria.Controls.Add(Me.lblPeriod)
        Me.gbFilterCriteria.Controls.Add(Me.cboPeriod)
        Me.gbFilterCriteria.Controls.Add(Me.chkGroupByEmpName)
        Me.gbFilterCriteria.Controls.Add(Me.objSearchCostCenter)
        Me.gbFilterCriteria.Controls.Add(Me.objSearchED)
        Me.gbFilterCriteria.Controls.Add(Me.txtPercTo)
        Me.gbFilterCriteria.Controls.Add(Me.txtPercFrom)
        Me.gbFilterCriteria.Controls.Add(Me.lblPercTo)
        Me.gbFilterCriteria.Controls.Add(Me.lblCostCenter)
        Me.gbFilterCriteria.Controls.Add(Me.lblPercFrom)
        Me.gbFilterCriteria.Controls.Add(Me.cboCostCenter)
        Me.gbFilterCriteria.Controls.Add(Me.cboTranHead)
        Me.gbFilterCriteria.Controls.Add(Me.lblTranHead)
        Me.gbFilterCriteria.Controls.Add(Me.objLine1)
        Me.gbFilterCriteria.Controls.Add(Me.objbtnReset)
        Me.gbFilterCriteria.Controls.Add(Me.objbtnSearchEmployee)
        Me.gbFilterCriteria.Controls.Add(Me.objbtnSearch)
        Me.gbFilterCriteria.Controls.Add(Me.cboEmployee)
        Me.gbFilterCriteria.Controls.Add(Me.lblEmployee)
        Me.gbFilterCriteria.ExpandedHoverImage = Nothing
        Me.gbFilterCriteria.ExpandedNormalImage = Nothing
        Me.gbFilterCriteria.ExpandedPressedImage = Nothing
        Me.gbFilterCriteria.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbFilterCriteria.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbFilterCriteria.HeaderHeight = 25
        Me.gbFilterCriteria.HeaderMessage = ""
        Me.gbFilterCriteria.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbFilterCriteria.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbFilterCriteria.HeightOnCollapse = 0
        Me.gbFilterCriteria.LeftTextSpace = 0
        Me.gbFilterCriteria.Location = New System.Drawing.Point(12, 66)
        Me.gbFilterCriteria.Name = "gbFilterCriteria"
        Me.gbFilterCriteria.OpenHeight = 91
        Me.gbFilterCriteria.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbFilterCriteria.ShowBorder = True
        Me.gbFilterCriteria.ShowCheckBox = False
        Me.gbFilterCriteria.ShowCollapseButton = False
        Me.gbFilterCriteria.ShowDefaultBorderColor = True
        Me.gbFilterCriteria.ShowDownButton = False
        Me.gbFilterCriteria.ShowHeader = True
        Me.gbFilterCriteria.Size = New System.Drawing.Size(850, 121)
        Me.gbFilterCriteria.TabIndex = 0
        Me.gbFilterCriteria.Temp = 0
        Me.gbFilterCriteria.Text = "Filter Criteria"
        Me.gbFilterCriteria.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'EZeeStraightLine1
        '
        Me.EZeeStraightLine1.BackColor = System.Drawing.Color.Transparent
        Me.EZeeStraightLine1.ForeColor = System.Drawing.SystemColors.ButtonShadow
        Me.EZeeStraightLine1.LineType = eZee.Common.StraightLineTypes.Vertical
        Me.EZeeStraightLine1.Location = New System.Drawing.Point(625, 33)
        Me.EZeeStraightLine1.Name = "EZeeStraightLine1"
        Me.EZeeStraightLine1.Size = New System.Drawing.Size(9, 78)
        Me.EZeeStraightLine1.TabIndex = 263
        Me.EZeeStraightLine1.Text = "EZeeStraightLine2"
        '
        'cboActiveInactive
        '
        Me.cboActiveInactive.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboActiveInactive.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboActiveInactive.FormattingEnabled = True
        Me.cboActiveInactive.Location = New System.Drawing.Point(395, 87)
        Me.cboActiveInactive.Name = "cboActiveInactive"
        Me.cboActiveInactive.Size = New System.Drawing.Size(202, 21)
        Me.cboActiveInactive.TabIndex = 261
        '
        'lblactiveinactive
        '
        Me.lblactiveinactive.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblactiveinactive.Location = New System.Drawing.Point(293, 90)
        Me.lblactiveinactive.Name = "lblactiveinactive"
        Me.lblactiveinactive.Size = New System.Drawing.Size(91, 15)
        Me.lblactiveinactive.TabIndex = 262
        Me.lblactiveinactive.Text = "Inactive Status"
        '
        'lblAllocation
        '
        Me.lblAllocation.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblAllocation.Location = New System.Drawing.Point(293, 36)
        Me.lblAllocation.Name = "lblAllocation"
        Me.lblAllocation.Size = New System.Drawing.Size(96, 15)
        Me.lblAllocation.TabIndex = 259
        Me.lblAllocation.Text = "Allocation"
        '
        'cboAllocation
        '
        Me.cboAllocation.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboAllocation.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboAllocation.FormattingEnabled = True
        Me.cboAllocation.Location = New System.Drawing.Point(395, 33)
        Me.cboAllocation.Name = "cboAllocation"
        Me.cboAllocation.Size = New System.Drawing.Size(202, 21)
        Me.cboAllocation.TabIndex = 3
        '
        'lblPeriod
        '
        Me.lblPeriod.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPeriod.Location = New System.Drawing.Point(8, 36)
        Me.lblPeriod.Name = "lblPeriod"
        Me.lblPeriod.Size = New System.Drawing.Size(74, 15)
        Me.lblPeriod.TabIndex = 256
        Me.lblPeriod.Text = "As On Period"
        '
        'cboPeriod
        '
        Me.cboPeriod.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboPeriod.DropDownWidth = 160
        Me.cboPeriod.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboPeriod.FormattingEnabled = True
        Me.cboPeriod.Location = New System.Drawing.Point(88, 33)
        Me.cboPeriod.Name = "cboPeriod"
        Me.cboPeriod.Size = New System.Drawing.Size(165, 21)
        Me.cboPeriod.TabIndex = 0
        '
        'chkGroupByEmpName
        '
        Me.chkGroupByEmpName.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkGroupByEmpName.Location = New System.Drawing.Point(638, 91)
        Me.chkGroupByEmpName.Name = "chkGroupByEmpName"
        Me.chkGroupByEmpName.Size = New System.Drawing.Size(186, 17)
        Me.chkGroupByEmpName.TabIndex = 5
        Me.chkGroupByEmpName.Text = "Group by Employee Name"
        Me.chkGroupByEmpName.UseVisualStyleBackColor = True
        '
        'objSearchCostCenter
        '
        Me.objSearchCostCenter.BackColor = System.Drawing.Color.Transparent
        Me.objSearchCostCenter.BackColor1 = System.Drawing.Color.Transparent
        Me.objSearchCostCenter.BackColor2 = System.Drawing.Color.Transparent
        Me.objSearchCostCenter.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objSearchCostCenter.BorderSelected = False
        Me.objSearchCostCenter.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objSearchCostCenter.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objSearchCostCenter.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objSearchCostCenter.Location = New System.Drawing.Point(603, 60)
        Me.objSearchCostCenter.Name = "objSearchCostCenter"
        Me.objSearchCostCenter.Size = New System.Drawing.Size(21, 21)
        Me.objSearchCostCenter.TabIndex = 249
        '
        'objSearchED
        '
        Me.objSearchED.BackColor = System.Drawing.Color.Transparent
        Me.objSearchED.BackColor1 = System.Drawing.Color.Transparent
        Me.objSearchED.BackColor2 = System.Drawing.Color.Transparent
        Me.objSearchED.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objSearchED.BorderSelected = False
        Me.objSearchED.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objSearchED.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objSearchED.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objSearchED.Location = New System.Drawing.Point(259, 87)
        Me.objSearchED.Name = "objSearchED"
        Me.objSearchED.Size = New System.Drawing.Size(21, 21)
        Me.objSearchED.TabIndex = 248
        '
        'txtPercTo
        '
        Me.txtPercTo.AllowNegative = False
        Me.txtPercTo.Decimal = New Decimal(New Integer() {0, 0, 0, 0})
        Me.txtPercTo.DigitsInGroup = 0
        Me.txtPercTo.Flags = 65536
        Me.txtPercTo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtPercTo.Location = New System.Drawing.Point(739, 60)
        Me.txtPercTo.MaxDecimalPlaces = 6
        Me.txtPercTo.MaxWholeDigits = 21
        Me.txtPercTo.Name = "txtPercTo"
        Me.txtPercTo.Prefix = ""
        Me.txtPercTo.RangeMax = 1.7976931348623157E+308
        Me.txtPercTo.RangeMin = -1.7976931348623157E+308
        Me.txtPercTo.Size = New System.Drawing.Size(85, 21)
        Me.txtPercTo.TabIndex = 7
        Me.txtPercTo.Text = "0"
        Me.txtPercTo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtPercFrom
        '
        Me.txtPercFrom.AllowNegative = False
        Me.txtPercFrom.Decimal = New Decimal(New Integer() {0, 0, 0, 0})
        Me.txtPercFrom.DigitsInGroup = 0
        Me.txtPercFrom.Flags = 65536
        Me.txtPercFrom.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtPercFrom.Location = New System.Drawing.Point(739, 33)
        Me.txtPercFrom.MaxDecimalPlaces = 6
        Me.txtPercFrom.MaxWholeDigits = 21
        Me.txtPercFrom.Name = "txtPercFrom"
        Me.txtPercFrom.Prefix = ""
        Me.txtPercFrom.RangeMax = 1.7976931348623157E+308
        Me.txtPercFrom.RangeMin = -1.7976931348623157E+308
        Me.txtPercFrom.Size = New System.Drawing.Size(85, 21)
        Me.txtPercFrom.TabIndex = 6
        Me.txtPercFrom.Text = "0"
        Me.txtPercFrom.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'lblPercTo
        '
        Me.lblPercTo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPercTo.Location = New System.Drawing.Point(637, 63)
        Me.lblPercTo.Name = "lblPercTo"
        Me.lblPercTo.Size = New System.Drawing.Size(96, 15)
        Me.lblPercTo.TabIndex = 228
        Me.lblPercTo.Text = "Percentage To"
        '
        'lblCostCenter
        '
        Me.lblCostCenter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCostCenter.Location = New System.Drawing.Point(293, 63)
        Me.lblCostCenter.Name = "lblCostCenter"
        Me.lblCostCenter.Size = New System.Drawing.Size(96, 15)
        Me.lblCostCenter.TabIndex = 227
        Me.lblCostCenter.Text = "Cost Center"
        '
        'lblPercFrom
        '
        Me.lblPercFrom.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPercFrom.Location = New System.Drawing.Point(637, 36)
        Me.lblPercFrom.Name = "lblPercFrom"
        Me.lblPercFrom.Size = New System.Drawing.Size(98, 15)
        Me.lblPercFrom.TabIndex = 226
        Me.lblPercFrom.Text = "Percentage From"
        '
        'cboCostCenter
        '
        Me.cboCostCenter.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboCostCenter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboCostCenter.FormattingEnabled = True
        Me.cboCostCenter.Location = New System.Drawing.Point(395, 60)
        Me.cboCostCenter.Name = "cboCostCenter"
        Me.cboCostCenter.Size = New System.Drawing.Size(202, 21)
        Me.cboCostCenter.TabIndex = 4
        '
        'cboTranHead
        '
        Me.cboTranHead.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboTranHead.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboTranHead.FormattingEnabled = True
        Me.cboTranHead.Location = New System.Drawing.Point(88, 87)
        Me.cboTranHead.Name = "cboTranHead"
        Me.cboTranHead.Size = New System.Drawing.Size(165, 21)
        Me.cboTranHead.TabIndex = 2
        '
        'lblTranHead
        '
        Me.lblTranHead.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTranHead.Location = New System.Drawing.Point(8, 90)
        Me.lblTranHead.Name = "lblTranHead"
        Me.lblTranHead.Size = New System.Drawing.Size(74, 15)
        Me.lblTranHead.TabIndex = 220
        Me.lblTranHead.Text = "Tran. Head"
        '
        'objLine1
        '
        Me.objLine1.BackColor = System.Drawing.Color.Transparent
        Me.objLine1.ForeColor = System.Drawing.SystemColors.ButtonShadow
        Me.objLine1.LineType = eZee.Common.StraightLineTypes.Vertical
        Me.objLine1.Location = New System.Drawing.Point(281, 35)
        Me.objLine1.Name = "objLine1"
        Me.objLine1.Size = New System.Drawing.Size(9, 76)
        Me.objLine1.TabIndex = 88
        Me.objLine1.Text = "EZeeStraightLine2"
        '
        'objbtnReset
        '
        Me.objbtnReset.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.objbtnReset.BackColor = System.Drawing.Color.Transparent
        Me.objbtnReset.ButtonType = eZee.Common.eZeeSearchResetButton.EnumButtonType.Reset
        Me.objbtnReset.Image = CType(resources.GetObject("objbtnReset.Image"), System.Drawing.Image)
        Me.objbtnReset.Location = New System.Drawing.Point(823, 0)
        Me.objbtnReset.Name = "objbtnReset"
        Me.objbtnReset.ResultMessage = ""
        Me.objbtnReset.SearchMessage = ""
        Me.objbtnReset.Size = New System.Drawing.Size(24, 24)
        Me.objbtnReset.TabIndex = 67
        Me.objbtnReset.TabStop = False
        '
        'objbtnSearchEmployee
        '
        Me.objbtnSearchEmployee.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchEmployee.BorderSelected = False
        Me.objbtnSearchEmployee.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchEmployee.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objbtnSearchEmployee.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchEmployee.Location = New System.Drawing.Point(259, 60)
        Me.objbtnSearchEmployee.Name = "objbtnSearchEmployee"
        Me.objbtnSearchEmployee.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchEmployee.TabIndex = 86
        '
        'objbtnSearch
        '
        Me.objbtnSearch.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.objbtnSearch.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearch.ButtonType = eZee.Common.eZeeSearchResetButton.EnumButtonType.Search
        Me.objbtnSearch.Image = CType(resources.GetObject("objbtnSearch.Image"), System.Drawing.Image)
        Me.objbtnSearch.Location = New System.Drawing.Point(800, 0)
        Me.objbtnSearch.Name = "objbtnSearch"
        Me.objbtnSearch.ResultMessage = ""
        Me.objbtnSearch.SearchMessage = ""
        Me.objbtnSearch.Size = New System.Drawing.Size(24, 24)
        Me.objbtnSearch.TabIndex = 66
        Me.objbtnSearch.TabStop = False
        '
        'cboEmployee
        '
        Me.cboEmployee.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboEmployee.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboEmployee.FormattingEnabled = True
        Me.cboEmployee.Location = New System.Drawing.Point(88, 60)
        Me.cboEmployee.Name = "cboEmployee"
        Me.cboEmployee.Size = New System.Drawing.Size(165, 21)
        Me.cboEmployee.TabIndex = 1
        '
        'lblEmployee
        '
        Me.lblEmployee.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEmployee.Location = New System.Drawing.Point(8, 63)
        Me.lblEmployee.Name = "lblEmployee"
        Me.lblEmployee.Size = New System.Drawing.Size(74, 15)
        Me.lblEmployee.TabIndex = 85
        Me.lblEmployee.Text = "Employee"
        '
        'btnVoid
        '
        Me.btnVoid.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnVoid.BackColor = System.Drawing.Color.White
        Me.btnVoid.BackgroundImage = CType(resources.GetObject("btnVoid.BackgroundImage"), System.Drawing.Image)
        Me.btnVoid.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnVoid.BorderColor = System.Drawing.Color.Empty
        Me.btnVoid.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnVoid.FlatAppearance.BorderSize = 0
        Me.btnVoid.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnVoid.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnVoid.ForeColor = System.Drawing.Color.Black
        Me.btnVoid.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnVoid.GradientForeColor = System.Drawing.Color.Black
        Me.btnVoid.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnVoid.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnVoid.Location = New System.Drawing.Point(673, 13)
        Me.btnVoid.Name = "btnVoid"
        Me.btnVoid.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnVoid.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnVoid.Size = New System.Drawing.Size(90, 30)
        Me.btnVoid.TabIndex = 4
        Me.btnVoid.Text = "&Void"
        Me.btnVoid.UseVisualStyleBackColor = True
        '
        'frmEmpDistributedCostCenterList
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(874, 612)
        Me.Controls.Add(Me.pnlMainInfo)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmEmpDistributedCostCenterList"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Employee Distributed Cost Center List"
        Me.pnlMainInfo.ResumeLayout(False)
        Me.pnlMainInfo.PerformLayout()
        CType(Me.dgvEmpBankList, System.ComponentModel.ISupportInitialize).EndInit()
        Me.objFooter.ResumeLayout(False)
        Me.gbFilterCriteria.ResumeLayout(False)
        Me.gbFilterCriteria.PerformLayout()
        CType(Me.objbtnReset, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.objbtnSearch, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents pnlMainInfo As System.Windows.Forms.Panel
    Friend WithEvents eZeeHeader As eZee.Common.eZeeHeader
    Friend WithEvents objFooter As eZee.Common.eZeeFooter
    Friend WithEvents btnClose As eZee.Common.eZeeLightButton
    Friend WithEvents btnEdit As eZee.Common.eZeeLightButton
    Friend WithEvents btnNew As eZee.Common.eZeeLightButton
    Friend WithEvents gbFilterCriteria As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents lblPercTo As System.Windows.Forms.Label
    Friend WithEvents lblCostCenter As System.Windows.Forms.Label
    Friend WithEvents lblPercFrom As System.Windows.Forms.Label
    Friend WithEvents cboCostCenter As System.Windows.Forms.ComboBox
    Friend WithEvents cboTranHead As System.Windows.Forms.ComboBox
    Friend WithEvents lblTranHead As System.Windows.Forms.Label
    Friend WithEvents objLine1 As eZee.Common.eZeeStraightLine
    Friend WithEvents objbtnReset As eZee.Common.eZeeSearchResetButton
    Friend WithEvents objbtnSearchEmployee As eZee.Common.eZeeGradientButton
    Friend WithEvents objbtnSearch As eZee.Common.eZeeSearchResetButton
    Friend WithEvents cboEmployee As System.Windows.Forms.ComboBox
    Friend WithEvents lblEmployee As System.Windows.Forms.Label
    Friend WithEvents txtPercTo As eZee.TextBox.NumericTextBox
    Friend WithEvents txtPercFrom As eZee.TextBox.NumericTextBox
    Friend WithEvents objSearchCostCenter As eZee.Common.eZeeGradientButton
    Friend WithEvents objSearchED As eZee.Common.eZeeGradientButton
    Friend WithEvents chkGroupByEmpName As System.Windows.Forms.CheckBox
    Friend WithEvents lblPeriod As System.Windows.Forms.Label
    Friend WithEvents cboPeriod As System.Windows.Forms.ComboBox
    Friend WithEvents lblAllocation As System.Windows.Forms.Label
    Friend WithEvents cboAllocation As System.Windows.Forms.ComboBox
    Friend WithEvents dgvEmpBankList As System.Windows.Forms.DataGridView
    Friend WithEvents btnInactive As eZee.Common.eZeeLightButton
    Friend WithEvents cboActiveInactive As System.Windows.Forms.ComboBox
    Friend WithEvents lblactiveinactive As System.Windows.Forms.Label
    Friend WithEvents EZeeStraightLine1 As eZee.Common.eZeeStraightLine
    Friend WithEvents objchkSelectAll As System.Windows.Forms.CheckBox
    Friend WithEvents objdgcolhCheck As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents objdgColhBlank As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhEmpCode As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhEmpName As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhCostCenter As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhTranHead As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhAllocationByName As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhPeriod As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhPercentage As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgColhPeriodStatusId As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhIsGrp As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhIsinactive As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgColhTranUnkid As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhEmployeeunkid As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhTranheadunkid As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhAllocationByid As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhCostcenterUnkid As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhPeriodunkid As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents btnVoid As eZee.Common.eZeeLightButton
End Class
