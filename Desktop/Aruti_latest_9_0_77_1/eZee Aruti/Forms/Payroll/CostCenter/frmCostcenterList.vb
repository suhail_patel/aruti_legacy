﻿Option Strict On

Imports eZeeCommonLib
Imports Aruti.Data
Imports System.Xml
Imports System.Xml.Linq
Imports System.Net
Imports System.IO
Imports System.Text
Imports System.Text.RegularExpressions

'Last Message Index = 3

Public Class frmCostcenterList

#Region "Private Variable"

    Private objCostcenterMaster As clscostcenter_master
    Private ReadOnly mstrModuleName As String = "frmCostcenterList"
    'Sohail (03 Jan 2020) -- Start
    'NMB Enhancement # : Search functionality on cost center list screen.
    Private mstrSearchText As String = ""
    'Sohail (03 Jan 2020) -- End

#End Region

#Region "Form's Event"

    Private Sub frmCostcenterList_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        objCostcenterMaster = New clscostcenter_master
        Try
            Call Set_Logo(Me, gApplicationType)

            Language.setLanguage(Me.Name)


            'Anjan (02 Sep 2011)-Start
            'Issue : Including Language Settings.
            Call OtherSettings()
            'Anjan (02 Sep 2011)-End 

            Call SetVisibility()
            'Sohail (03 Jan 2020) -- Start
            'NMB Enhancement # : Search functionality on cost center list screen.
            Call FillCombo()
            'Sohail (03 Jan 2020) -- End
            fillList()
            'Sohail (03 Jan 2020) -- Start
            'NMB Enhancement # : Search functionality on cost center list screen.
            'If lvCostcenterCodes.Items.Count > 0 Then lvCostcenterCodes.Items(0).Selected = True
            'lvCostcenterCodes.Select()
            dgvCostcenterCodes.Focus()
            'Sohail (03 Jan 2020) -- End
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmCostcenterList_Load", mstrModuleName)
        End Try
    End Sub

    Private Sub frmCostcenterList_KeyUp(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles MyBase.KeyUp
        Try
            'Sohail (03 Jan 2020) -- Start
            'NMB Enhancement # : Search functionality on cost center list screen.
            'If e.KeyCode = Keys.Delete And lvCostcenterCodes.Focused = True Then
            If e.KeyCode = Keys.Delete And dgvCostcenterCodes.Focused = True Then
                'Sohail (03 Jan 2020) -- End
                'Sohail (24 Jun 2011) -- Start
                'Issue : Delete event fired even if there is no delete privilege
                'btnDelete_Click(sender, e)
                Call btnDelete.PerformClick()
                'Sohail (24 Jun 2011) -- End
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmCostcenterList_KeyUp", mstrModuleName)
        End Try
    End Sub

    Private Sub frmCostcenterList_FormClosed(ByVal sender As System.Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles MyBase.FormClosed
        objCostcenterMaster = Nothing
    End Sub
    'Anjan (02 Sep 2011)-Start
    'Issue : Including Language Settings.
    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clscostcenter_master.SetMessages()
            objfrm._Other_ModuleNames = "clscostcenter_master"
            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub
    'Anjan (02 Sep 2011)-End 
#End Region

#Region "Button's Event"

    Private Sub btnNew_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNew.Click
        Try
            Dim objCostcenter_AddEdit As New frmCostcenter_AddEdit
            'Anjan (02 Sep 2011)-Start
            'Issue : Including Language Settings.
            If User._Object._Isrighttoleft = True Then
                objCostcenter_AddEdit.RightToLeft = Windows.Forms.RightToLeft.Yes
                objCostcenter_AddEdit.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objCostcenter_AddEdit)
            End If
            'Anjan (02 Sep 2011)-End 

            If objCostcenter_AddEdit.displayDialog(-1, enAction.ADD_CONTINUE) Then
                fillList()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnNew_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnEdit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEdit.Click 'Sohail (11 Sep 2010)
        'Sohail (24 Jun 2011) -- Start
        If User._Object.Privilege._EditCostCenter = False Then Exit Sub
        'Sohail (24 Jun 2011) -- End
        Try
            'Sohail (03 Jan 2020) -- Start
            'NMB Enhancement # : Search functionality on cost center list screen.
            'If lvCostcenterCodes.SelectedItems.Count < 1 Then
            If dgvCostcenterCodes.SelectedRows.Count <= 0 Then
                'Sohail (03 Jan 2020) -- End
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please select CostCenter from the list to perform further operation."), enMsgBoxStyle.Information) '?1
                'Sohail (03 Jan 2020) -- Start
                'NMB Enhancement # : Search functionality on cost center list screen.
                'lvCostcenterCodes.Select()
                dgvCostcenterCodes.Select()
                'Sohail (03 Jan 2020) -- End
                Exit Sub
            End If
            Dim objfrmCostcenter_AddEdit As New frmCostcenter_AddEdit
            'Anjan (02 Sep 2011)-Start
            'Issue : Including Language Settings.
            If User._Object._Isrighttoleft = True Then
                objfrmCostcenter_AddEdit.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrmCostcenter_AddEdit.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrmCostcenter_AddEdit)
            End If
            'Anjan (02 Sep 2011)-End 
            Try
                Dim intSelectedIndex As Integer
                'Sohail (03 Jan 2020) -- Start
                'NMB Enhancement # : Search functionality on cost center list screen.
                'intSelectedIndex = lvCostcenterCodes.SelectedItems(0).Index
                'If objfrmCostcenter_AddEdit.displayDialog(CInt(lvCostcenterCodes.SelectedItems(0).Tag), enAction.EDIT_ONE) Then
                If objfrmCostcenter_AddEdit.displayDialog(CInt(dgvCostcenterCodes.SelectedRows(0).Cells(objdgcolhCostcenterunkid.Index).Value), enAction.EDIT_ONE) Then
                    intSelectedIndex = dgvCostcenterCodes.SelectedRows(0).Index
                    'Sohail (03 Jan 2020) -- End
                    fillList()
                End If
                objfrmCostcenter_AddEdit = Nothing

                'Sohail (03 Jan 2020) -- Start
                'NMB Enhancement # : Search functionality on cost center list screen.
                'lvCostcenterCodes.Items(intSelectedIndex).Selected = True
                'lvCostcenterCodes.EnsureVisible(intSelectedIndex)
                'lvCostcenterCodes.Select()
                'Sohail (03 Jan 2020) -- End
            Catch ex As Exception
                Call DisplayError.Show("-1", ex.Message, "btnEdit_Click", mstrModuleName)
            Finally
                If objfrmCostcenter_AddEdit IsNot Nothing Then objfrmCostcenter_AddEdit.Dispose()
            End Try
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnEdit_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDelete.Click
        'Sohail (03 Jan 2020) -- Start
        'NMB Enhancement # : Search functionality on cost center list screen.
        'If lvCostcenterCodes.SelectedItems.Count < 1 Then
        If dgvCostcenterCodes.SelectedRows.Count <= 0 Then
            'Sohail (03 Jan 2020) -- End
            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please select CostCenter from the list to perform further operation."), enMsgBoxStyle.Information) '?1
            'Sohail (03 Jan 2020) -- Start
            'NMB Enhancement # : Search functionality on cost center list screen.
            'lvCostcenterCodes.Select()
            dgvCostcenterCodes.Select()
            'Sohail (03 Jan 2020) -- End
            Exit Sub
        End If
        'Sohail (19 Nov 2010) -- Start
        'If objCostcenterMaster.isUsed(CInt(lvCostcenterCodes.SelectedItems(0).Tag)) Then
        '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Sorry, You cannot delete this CostCenter. Reason: This CostCenter is in use."), enMsgBoxStyle.Information) '?2
        '    lvCostcenterCodes.Select()
        '    Exit Sub
        'End If
        'Sohail (18 Mar 2016) -- Start
        'Enhancement - Allocation changes in 58.1.
        'If objCostcenterMaster.isUsed(CInt(lvCostcenterCodes.SelectedItems(0).Tag)) Then
        'Sohail (03 Jan 2020) -- Start
        'NMB Enhancement # : Search functionality on cost center list screen.
        'If objCostcenterMaster.isUsed(CInt(lvCostcenterCodes.SelectedItems(0).Tag), eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate)) Then
        If objCostcenterMaster.isUsed(CInt(dgvCostcenterCodes.SelectedRows(0).Cells(objdgcolhCostcenterunkid.Index).Value), eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate)) Then
            'Sohail (03 Jan 2020) -- End
            'Sohail (18 Mar 2016) -- End
            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Sorry, You cannot delete this CostCenter. Reason: This CostCenter is in use."), enMsgBoxStyle.Information) '?2
            'Sohail (03 Jan 2020) -- Start
            'NMB Enhancement # : Search functionality on cost center list screen.
            'lvCostcenterCodes.Select()
            dgvCostcenterCodes.Select()
            'Sohail (03 Jan 2020) -- End
            Exit Sub
        End If
        'Sohail (19 Nov 2010) -- End
        Try
            Dim intSelectedIndex As Integer
            'Sohail (03 Jan 2020) -- Start
            'NMB Enhancement # : Search functionality on cost center list screen.
            'intSelectedIndex = lvCostcenterCodes.SelectedItems(0).Index
            intSelectedIndex = dgvCostcenterCodes.SelectedRows(0).Index
            'Sohail (03 Jan 2020) -- End

            If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Are you sure you want to delete this CostCenter?"), CType(enMsgBoxStyle.Question + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.Yes Then

                'S.SANDEEP [28-May-2018] -- START
                'ISSUE/ENHANCEMENT : {Audit Trails} 
                objCostcenterMaster._FormName = mstrModuleName
                objCostcenterMaster._LoginEmployeeunkid = 0
                objCostcenterMaster._ClientIP = getIP()
                objCostcenterMaster._HostName = getHostName()
                objCostcenterMaster._FromWeb = False
                objCostcenterMaster._AuditUserId = User._Object._Userunkid
objCostcenterMaster._CompanyUnkid = Company._Object._Companyunkid
                objCostcenterMaster._AuditDate = ConfigParameter._Object._CurrentDateAndTime
                'S.SANDEEP [28-May-2018] -- END
'Sohail (03 Jan 2020) -- Start
                'NMB Enhancement # : Search functionality on cost center list screen.
                'objCostcenterMaster.Delete(CInt(lvCostcenterCodes.SelectedItems(0).Tag))
                'lvCostcenterCodes.SelectedItems(0).Remove()

                'If lvCostcenterCodes.Items.Count <= 0 Then
                '    Exit Try
                'End If

                'If lvCostcenterCodes.Items.Count = intSelectedIndex Then
                '    intSelectedIndex = lvCostcenterCodes.Items.Count - 1
                '    lvCostcenterCodes.Items(intSelectedIndex).Selected = True
                '    lvCostcenterCodes.EnsureVisible(intSelectedIndex)
                'ElseIf lvCostcenterCodes.Items.Count <> 0 Then
                '    lvCostcenterCodes.Items(intSelectedIndex).Selected = True
                '    lvCostcenterCodes.EnsureVisible(intSelectedIndex)
                'End If
                If objCostcenterMaster.Delete(CInt(dgvCostcenterCodes.SelectedRows(0).Cells(objdgcolhCostcenterunkid.Index).Value)) = True Then
                    Call fillList()
                End If
                'Sohail (03 Jan 2020) -- End
            End If
            'lvCostcenterCodes.Select() 'Sohail (03 Jan 2020)
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "btnDelete_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        'Sohail (11 Sep 2010) -- Start
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnClose_Click", mstrModuleName)
        End Try
        'Sohail (11 Sep 2010) -- End
    End Sub

    'Pinkal (20-Nov-2018) -- Start
    'Enhancement - Working on P2P Integration for NMB.

    Private Sub btnImportCCFromP2P_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnImportCCFromP2P.Click
        Try
            Dim objFrm As New frmImporCostCenterFromtP2P
            objFrm.ShowDialog()
            fillList()
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "btnImportCCFromP2P_Click", mstrModuleName)
        End Try
    End Sub

    'Pinkal (20-Nov-2018) -- End

    'Sohail (03 Jan 2020) -- Start
    'NMB Enhancement # : Search functionality on cost center list screen.
    Private Sub objbtnSearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearch.Click
        Try
            Call fillList()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearch_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnReset.Click
        Try
            If CInt(cboCostCenterGroup.SelectedValue) > 0 Then cboCostCenterGroup.SelectedValue = 0
            If CInt(cboCostCenter.SelectedValue) > 0 Then cboCostCenter.SelectedValue = 0
            
            Call FillList()

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnReset_Click", mstrModuleName)
        End Try
    End Sub
    'Sohail (03 Jan 2020) -- End

#End Region

#Region " Private Methods "

    'Sohail (03 Jan 2020) -- Start
    'NMB Enhancement # : Search functionality on cost center list screen.
    Private Sub FillCombo()
        Dim objGroupMaster As New clspayrollgroup_master
        Dim dsCombo As DataSet
        Try
            dsCombo = objGroupMaster.getListForCombo(CInt(PayrollGroupType.CostCenter), "List", True)
            With cboCostCenterGroup
                .ValueMember = "groupmasterunkid"
                .DisplayMember = "name"
                .DataSource = dsCombo.Tables(0)
                .SelectedValue = 0
                Call SetDefaultSearchText(cboCostCenterGroup)
            End With

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        Finally
            objGroupMaster=Nothing
        End Try
    End Sub
    'Sohail (03 Jan 2020) -- End

    Private Sub fillList()
        Dim dsCostCenter As New DataSet
        Try

            If User._Object.Privilege._AllowToViewCostCenterList = True Then    'Pinkal (09-Jul-2012) -- Start

                'Sohail (03 Jan 2020) -- Start
                'NMB Enhancement # : Search functionality on cost center list screen.
                'dsCostCenter = objCostcenterMaster.GetList("List")
                Dim strFilter As String = ""
                If CInt(cboCostCenterGroup.SelectedValue) > 0 Then
                    strFilter &= " AND prcostcenter_master.costcentergroupmasterunkid = " & CInt(cboCostCenterGroup.SelectedValue) & " "
                End If
                If CInt(cboCostCenter.SelectedValue) > 0 Then
                    strFilter &= " AND prcostcenter_master.costcenterunkid = " & CInt(cboCostCenter.SelectedValue) & " "
                End If
                dsCostCenter = objCostcenterMaster.GetList("List", , strFilter)
                'Sohail (03 Jan 2020) -- End

                'Sohail (03 Jan 2020) -- Start
                'NMB Enhancement # : Option for Cost centre code column on custom payroll report.
                'Dim lvItem As ListViewItem

                'lvCostcenterCodes.Items.Clear()
                'For Each drRow As DataRow In dsCostCenter.Tables(0).Rows
                '    lvItem = New ListViewItem
                '    lvItem.Text = drRow("costcentergroup").ToString
                '    lvItem.Tag = drRow("costcenterunkid")
                '    lvItem.SubItems.Add(drRow("costcentercode").ToString)
                '    lvItem.SubItems.Add(drRow("costcentername").ToString)
                '    lvItem.SubItems.Add(drRow("description").ToString)
                '    'S.SANDEEP [09-AUG-2018] -- START
                '    'ISSUE/ENHANCEMENT : {Ref#292}
                '    lvItem.SubItems.Add(drRow("customcode").ToString)
                '    'S.SANDEEP [09-AUG-2018] -- END
                '    lvCostcenterCodes.Items.Add(lvItem)
                'Next
                colhCostcentergroup.DataPropertyName = "costcentergroup"
                colhCostcentercode.DataPropertyName = "costcentercode"
                colhCostcentername.DataPropertyName = "costcentername"
                colhCostcenterdesc.DataPropertyName = "description"
                colhCustomCode.DataPropertyName = "customcode"
                objdgcolhCostcenterunkid.DataPropertyName = "costcenterunkid"

                With dgvCostcenterCodes
                    .AutoGenerateColumns = False

                    .DataSource = dsCostCenter.Tables(0)

                End With
                'Sohail (03 Jan 2020) -- End
                '

                'S.SANDEEP [09-AUG-2018] -- START
                'ISSUE/ENHANCEMENT : {Ref#292}
                'If lvCostcenterCodes.Items.Count > 16 Then
                '    colhCostcenterdesc.Width = 325 - 18
                'Else
                '    colhCostcenterdesc.Width = 325
                'End If

                'Sohail (03 Jan 2020) -- Start
                'NMB Enhancement # : Option for Cost centre code column on custom payroll report.
                'If lvCostcenterCodes.Items.Count > 16 Then
                '    colhCostcenterdesc.Width = 225 - 18
                'Else
                '    colhCostcenterdesc.Width = 225
                'End If
                'Sohail (03 Jan 2020) -- End
                'S.SANDEEP [09-AUG-2018] -- END

            End If

        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "fillList", mstrModuleName)
        Finally
            dsCostCenter.Dispose()
            'Sohail (03 Jan 2020) -- Start
            'NMB Enhancement # : Search functionality on cost center list screen.
            Call objbtnSearch.ShowResult(dgvCostcenterCodes.Rows.Count.ToString)
            'Sohail (03 Jan 2020) -- End
        End Try
    End Sub

    Private Sub SetVisibility()

        Try
            btnNew.Enabled = User._Object.Privilege._AddCostCenter
            btnEdit.Enabled = User._Object.Privilege._EditCostCenter
            btnDelete.Enabled = User._Object.Privilege._DeleteCostCenter

            'Pinkal (20-Nov-2018) -- Start
            'Enhancement - Working on P2P Integration for NMB.
            If (User._Object.Privilege._AddCostCenter OrElse User._Object.Privilege._EditCostCenter) AndAlso ConfigParameter._Object._ImportCostCenterP2PServiceURL.Trim.Length > 0 Then
                btnImportCCFromP2P.Visible = True
            Else
                btnImportCCFromP2P.Visible = False
            End If
            'Pinkal (20-Nov-2018) -- End


        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetVisibility", mstrModuleName)
        End Try

    End Sub

    'Sohail (03 Jan 2020) -- Start
    'NMB Enhancement # : Search functionality on cost center list screen.
    Private Sub SetDefaultSearchText(ByVal cbo As ComboBox)
        Try
            mstrSearchText = Language.getMessage(mstrModuleName, 4, "Type to Search")
            With cbo
                .ForeColor = Color.Gray
                .Text = mstrSearchText
                .Font = New Font(Me.Font.FontFamily, Me.Font.Size, FontStyle.Italic)
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetDefaultSearchText", mstrModuleName)
        End Try
    End Sub
    'Sohail (03 Jan 2020) -- End

#End Region

    'Sohail (03 Jan 2020) -- Start
    'NMB Enhancement # : Search functionality on cost center list screen.
#Region " Combobox's Events "

    Private Sub cboCostCenterGroup_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboCostCenterGroup.SelectedIndexChanged, cboCostCenter.SelectedIndexChanged
        Dim cbo As ComboBox = CType(sender, ComboBox)
        Dim dsCombo As DataSet
        Dim strFilter As String = ""
        Try
            If cbo.Name = cboCostCenterGroup.Name Then
                If CInt(cboCostCenterGroup.SelectedValue) > 0 Then
                    strFilter &= " AND prcostcenter_master.costcentergroupmasterunkid = " & CInt(cboCostCenterGroup.SelectedValue) & " "
                End If
                dsCombo = objCostcenterMaster.getComboList("List", True, , strFilter)
                With cboCostCenter
                    .ValueMember = "costcenterunkid"
                    .DisplayMember = "costcentername"
                    .DataSource = dsCombo.Tables(0)
                    .SelectedValue = 0
                    Call SetDefaultSearchText(cboCostCenter)
                End With
            End If

            If CInt(cbo.SelectedValue) <= 0 Then Call SetDefaultSearchText(cbo)

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboCostCenterGroup_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub cboCostCenterGroup_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles cboCostCenterGroup.KeyPress, cboCostCenter.KeyPress
        Dim cbo As ComboBox = CType(sender, ComboBox)
        Try
            If (CInt(AscW(e.KeyChar)) >= 65 AndAlso CInt(AscW(e.KeyChar)) <= 90) Or (CInt(AscW(e.KeyChar)) >= 97 AndAlso CInt(AscW(e.KeyChar)) <= 122) Or (CInt(AscW(e.KeyChar)) >= 47 AndAlso CInt(AscW(e.KeyChar)) <= 57) Then
                Dim frm As New frmCommonSearch
                With frm
                    .ValueMember = cbo.ValueMember
                    .DisplayMember = cbo.DisplayMember
                    .DataSource = CType(cbo.DataSource, DataTable)
                    If cbo.Name = cboCostCenter.Name Then
                        .CodeMember = "costcentercode"
                    Else
                        .CodeMember = "code"
                    End If
                End With
                Dim c As Char = Convert.ToChar(e.KeyChar)
                frm.TypedText = c.ToString
                If frm.DisplayDialog Then
                    cbo.SelectedValue = frm.SelectedValue
                    e.KeyChar = ChrW(Keys.ShiftKey)
                Else
                    cbo.Text = ""
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, cbo.Name & "_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub cboCostCenterGroup_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboCostCenterGroup.GotFocus, cboCostCenter.GotFocus
        Dim cbo As ComboBox = CType(sender, ComboBox)
        Try
            
            With cbo
                .ForeColor = Color.Black
                .Font = New Font(Me.Font.FontFamily, Me.Font.Size, FontStyle.Regular)

                If .Text = mstrSearchText Then
                    .Text = ""
                End If
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, cbo.Name & "_GotFocus", mstrModuleName)
        End Try
    End Sub

    Private Sub cboCostCenterGroup_Leave(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboCostCenterGroup.Leave, cboCostCenter.Leave
        Dim cbo As ComboBox = CType(sender, ComboBox)
        Try
            If CInt(cbo.SelectedValue) <= 0 Then
                Call SetDefaultSearchText(cbo)
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, cbo.Name & "_Leave", mstrModuleName)
        End Try
    End Sub
#End Region
    'Sohail (03 Jan 2020) -- End


    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Private Sub OtherSettings()
        Try
            Me.SuspendLayout()
			 
            Call SetLanguage()

			Me.gbFilterCriteria.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbFilterCriteria.ForeColor = GUI._eZeeContainerHeaderForeColor 
            
            Me.eZeeHeader.GradientColor1 = GUI._HeaderBackColor1
            Me.eZeeHeader.GradientColor2 = GUI._HeaderBackColor2
            Me.eZeeHeader.BorderColor = GUI._HeaderBorderColor
            Me.eZeeHeader.HeaderTextForeColor = GUI._HeaderTitleFontColor
            Me.eZeeHeader.DescriptionForeColor = GUI._HeaderMessageFontColor


            Me.btnDelete.GradientBackColor = GUI._ButttonBackColor
            Me.btnDelete.GradientForeColor = GUI._ButttonFontColor

            Me.btnEdit.GradientBackColor = GUI._ButttonBackColor
            Me.btnEdit.GradientForeColor = GUI._ButttonFontColor

            Me.btnNew.GradientBackColor = GUI._ButttonBackColor
            Me.btnNew.GradientForeColor = GUI._ButttonFontColor

            Me.btnClose.GradientBackColor = GUI._ButttonBackColor
            Me.btnClose.GradientForeColor = GUI._ButttonFontColor

			Me.btnImportCCFromP2P.GradientBackColor = GUI._ButttonBackColor 
			Me.btnImportCCFromP2P.GradientForeColor = GUI._ButttonFontColor


            Me.ResumeLayout()
        Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "OtherSettings", mstrModuleName)
        End Try
    End Sub


    Private Sub SetLanguage()
        Try
            Me.Text = Language._Object.getCaption(Me.Name, Me.Text)

			Me.eZeeHeader.Title = Language._Object.getCaption(Me.eZeeHeader.Name & "_Title" , Me.eZeeHeader.Title)
			Me.eZeeHeader.Message = Language._Object.getCaption(Me.eZeeHeader.Name & "_Message" , Me.eZeeHeader.Message)
            Me.btnDelete.Text = Language._Object.getCaption(Me.btnDelete.Name, Me.btnDelete.Text)
            Me.btnEdit.Text = Language._Object.getCaption(Me.btnEdit.Name, Me.btnEdit.Text)
            Me.btnNew.Text = Language._Object.getCaption(Me.btnNew.Name, Me.btnNew.Text)
            Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
			Me.btnImportCCFromP2P.Text = Language._Object.getCaption(Me.btnImportCCFromP2P.Name, Me.btnImportCCFromP2P.Text)
			Me.gbFilterCriteria.Text = Language._Object.getCaption(Me.gbFilterCriteria.Name, Me.gbFilterCriteria.Text)
			Me.lblCostCenter.Text = Language._Object.getCaption(Me.lblCostCenter.Name, Me.lblCostCenter.Text)
			Me.lblCostCenterGroup.Text = Language._Object.getCaption(Me.lblCostCenterGroup.Name, Me.lblCostCenterGroup.Text)
			Me.colhCostcentergroup.HeaderText = Language._Object.getCaption(Me.colhCostcentergroup.Name, Me.colhCostcentergroup.HeaderText)
			Me.colhCostcentercode.HeaderText = Language._Object.getCaption(Me.colhCostcentercode.Name, Me.colhCostcentercode.HeaderText)
			Me.colhCostcentername.HeaderText = Language._Object.getCaption(Me.colhCostcentername.Name, Me.colhCostcentername.HeaderText)
			Me.colhCustomCode.HeaderText = Language._Object.getCaption(Me.colhCustomCode.Name, Me.colhCustomCode.HeaderText)
			Me.colhCostcenterdesc.HeaderText = Language._Object.getCaption(Me.colhCostcenterdesc.Name, Me.colhCostcenterdesc.HeaderText)

        Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetLanguage", mstrModuleName)
        End Try
    End Sub


    Private Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Please select CostCenter from the list to perform further operation.")
            Language.setMessage(mstrModuleName, 2, "Sorry, You cannot delete this CostCenter. Reason: This CostCenter is in use.")
            Language.setMessage(mstrModuleName, 3, "Are you sure you want to delete this CostCenter?")
			Language.setMessage(mstrModuleName, 4, "Type to Search")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class