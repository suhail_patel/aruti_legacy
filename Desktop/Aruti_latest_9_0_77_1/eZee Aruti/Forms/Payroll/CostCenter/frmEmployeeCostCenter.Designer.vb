﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmEmployeeCostCenter
    Inherits eZee.Common.eZeeForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmEmployeeCostCenter))
        Me.pnlEmpCostCenter = New System.Windows.Forms.Panel
        Me.gbCostCenterInfo = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.lblDefCC = New System.Windows.Forms.Label
        Me.btnSave = New eZee.Common.eZeeLightButton(Me.components)
        Me.objSearchCostCenter = New eZee.Common.eZeeGradientButton
        Me.objSearchED = New eZee.Common.eZeeGradientButton
        Me.lblCostCenter = New System.Windows.Forms.Label
        Me.cboCostcenter = New System.Windows.Forms.ComboBox
        Me.cboED = New System.Windows.Forms.ComboBox
        Me.lblTrnHead = New System.Windows.Forms.Label
        Me.objSearchEmployee = New eZee.Common.eZeeGradientButton
        Me.cboEmployee = New System.Windows.Forms.ComboBox
        Me.lblEmployee = New System.Windows.Forms.Label
        Me.eZeeHeader = New eZee.Common.eZeeHeader
        Me.lvEmpCostCenter = New eZee.Common.eZeeListView(Me.components)
        Me.colhEmpCode = New System.Windows.Forms.ColumnHeader
        Me.colhEmpName = New System.Windows.Forms.ColumnHeader
        Me.colhTrnHead = New System.Windows.Forms.ColumnHeader
        Me.colhCcName = New System.Windows.Forms.ColumnHeader
        Me.colhEmployeeunkid = New System.Windows.Forms.ColumnHeader
        Me.colhTranheadunkid = New System.Windows.Forms.ColumnHeader
        Me.colhCostcenterunkid = New System.Windows.Forms.ColumnHeader
        Me.objFooter = New eZee.Common.eZeeFooter
        Me.btnSalaryCC = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnDelete = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnClose = New eZee.Common.eZeeLightButton(Me.components)
        Me.pnlEmpCostCenter.SuspendLayout()
        Me.gbCostCenterInfo.SuspendLayout()
        Me.objFooter.SuspendLayout()
        Me.SuspendLayout()
        '
        'pnlEmpCostCenter
        '
        Me.pnlEmpCostCenter.Controls.Add(Me.gbCostCenterInfo)
        Me.pnlEmpCostCenter.Controls.Add(Me.eZeeHeader)
        Me.pnlEmpCostCenter.Controls.Add(Me.lvEmpCostCenter)
        Me.pnlEmpCostCenter.Controls.Add(Me.objFooter)
        Me.pnlEmpCostCenter.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pnlEmpCostCenter.Location = New System.Drawing.Point(0, 0)
        Me.pnlEmpCostCenter.Name = "pnlEmpCostCenter"
        Me.pnlEmpCostCenter.Size = New System.Drawing.Size(660, 398)
        Me.pnlEmpCostCenter.TabIndex = 0
        '
        'gbCostCenterInfo
        '
        Me.gbCostCenterInfo.BorderColor = System.Drawing.Color.Black
        Me.gbCostCenterInfo.Checked = False
        Me.gbCostCenterInfo.CollapseAllExceptThis = False
        Me.gbCostCenterInfo.CollapsedHoverImage = Nothing
        Me.gbCostCenterInfo.CollapsedNormalImage = Nothing
        Me.gbCostCenterInfo.CollapsedPressedImage = Nothing
        Me.gbCostCenterInfo.CollapseOnLoad = False
        Me.gbCostCenterInfo.Controls.Add(Me.lblDefCC)
        Me.gbCostCenterInfo.Controls.Add(Me.btnSave)
        Me.gbCostCenterInfo.Controls.Add(Me.objSearchCostCenter)
        Me.gbCostCenterInfo.Controls.Add(Me.objSearchED)
        Me.gbCostCenterInfo.Controls.Add(Me.lblCostCenter)
        Me.gbCostCenterInfo.Controls.Add(Me.cboCostcenter)
        Me.gbCostCenterInfo.Controls.Add(Me.cboED)
        Me.gbCostCenterInfo.Controls.Add(Me.lblTrnHead)
        Me.gbCostCenterInfo.Controls.Add(Me.objSearchEmployee)
        Me.gbCostCenterInfo.Controls.Add(Me.cboEmployee)
        Me.gbCostCenterInfo.Controls.Add(Me.lblEmployee)
        Me.gbCostCenterInfo.ExpandedHoverImage = Nothing
        Me.gbCostCenterInfo.ExpandedNormalImage = Nothing
        Me.gbCostCenterInfo.ExpandedPressedImage = Nothing
        Me.gbCostCenterInfo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbCostCenterInfo.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbCostCenterInfo.HeaderHeight = 25
        Me.gbCostCenterInfo.HeaderMessage = ""
        Me.gbCostCenterInfo.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbCostCenterInfo.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbCostCenterInfo.HeightOnCollapse = 0
        Me.gbCostCenterInfo.LeftTextSpace = 0
        Me.gbCostCenterInfo.Location = New System.Drawing.Point(12, 66)
        Me.gbCostCenterInfo.Name = "gbCostCenterInfo"
        Me.gbCostCenterInfo.OpenHeight = 91
        Me.gbCostCenterInfo.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbCostCenterInfo.ShowBorder = True
        Me.gbCostCenterInfo.ShowCheckBox = False
        Me.gbCostCenterInfo.ShowCollapseButton = False
        Me.gbCostCenterInfo.ShowDefaultBorderColor = True
        Me.gbCostCenterInfo.ShowDownButton = False
        Me.gbCostCenterInfo.ShowHeader = True
        Me.gbCostCenterInfo.Size = New System.Drawing.Size(636, 116)
        Me.gbCostCenterInfo.TabIndex = 0
        Me.gbCostCenterInfo.Temp = 0
        Me.gbCostCenterInfo.Text = "Employee Cost Center Info"
        Me.gbCostCenterInfo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblDefCC
        '
        Me.lblDefCC.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDefCC.Location = New System.Drawing.Point(368, 36)
        Me.lblDefCC.Name = "lblDefCC"
        Me.lblDefCC.Size = New System.Drawing.Size(264, 39)
        Me.lblDefCC.TabIndex = 249
        Me.lblDefCC.Text = "Default Cost Center :"
        '
        'btnSave
        '
        Me.btnSave.BackColor = System.Drawing.Color.White
        Me.btnSave.BackgroundImage = CType(resources.GetObject("btnSave.BackgroundImage"), System.Drawing.Image)
        Me.btnSave.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnSave.BorderColor = System.Drawing.Color.Empty
        Me.btnSave.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnSave.FlatAppearance.BorderSize = 0
        Me.btnSave.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnSave.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSave.ForeColor = System.Drawing.Color.Black
        Me.btnSave.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnSave.GradientForeColor = System.Drawing.Color.Black
        Me.btnSave.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSave.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnSave.Location = New System.Drawing.Point(368, 78)
        Me.btnSave.Name = "btnSave"
        Me.btnSave.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSave.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnSave.Size = New System.Drawing.Size(93, 30)
        Me.btnSave.TabIndex = 7
        Me.btnSave.Text = "&Add"
        Me.btnSave.UseVisualStyleBackColor = True
        '
        'objSearchCostCenter
        '
        Me.objSearchCostCenter.BackColor = System.Drawing.Color.Transparent
        Me.objSearchCostCenter.BackColor1 = System.Drawing.Color.Transparent
        Me.objSearchCostCenter.BackColor2 = System.Drawing.Color.Transparent
        Me.objSearchCostCenter.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objSearchCostCenter.BorderSelected = False
        Me.objSearchCostCenter.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objSearchCostCenter.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objSearchCostCenter.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objSearchCostCenter.Location = New System.Drawing.Point(331, 87)
        Me.objSearchCostCenter.Name = "objSearchCostCenter"
        Me.objSearchCostCenter.Size = New System.Drawing.Size(21, 21)
        Me.objSearchCostCenter.TabIndex = 6
        '
        'objSearchED
        '
        Me.objSearchED.BackColor = System.Drawing.Color.Transparent
        Me.objSearchED.BackColor1 = System.Drawing.Color.Transparent
        Me.objSearchED.BackColor2 = System.Drawing.Color.Transparent
        Me.objSearchED.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objSearchED.BorderSelected = False
        Me.objSearchED.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objSearchED.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objSearchED.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objSearchED.Location = New System.Drawing.Point(331, 60)
        Me.objSearchED.Name = "objSearchED"
        Me.objSearchED.Size = New System.Drawing.Size(21, 21)
        Me.objSearchED.TabIndex = 4
        '
        'lblCostCenter
        '
        Me.lblCostCenter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCostCenter.Location = New System.Drawing.Point(8, 90)
        Me.lblCostCenter.Name = "lblCostCenter"
        Me.lblCostCenter.Size = New System.Drawing.Size(110, 15)
        Me.lblCostCenter.TabIndex = 227
        Me.lblCostCenter.Text = "Cost Center"
        '
        'cboCostcenter
        '
        Me.cboCostcenter.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboCostcenter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboCostcenter.FormattingEnabled = True
        Me.cboCostcenter.Location = New System.Drawing.Point(129, 87)
        Me.cboCostcenter.Name = "cboCostcenter"
        Me.cboCostcenter.Size = New System.Drawing.Size(196, 21)
        Me.cboCostcenter.TabIndex = 5
        '
        'cboED
        '
        Me.cboED.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboED.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboED.FormattingEnabled = True
        Me.cboED.Location = New System.Drawing.Point(129, 60)
        Me.cboED.Name = "cboED"
        Me.cboED.Size = New System.Drawing.Size(196, 21)
        Me.cboED.TabIndex = 3
        '
        'lblTrnHead
        '
        Me.lblTrnHead.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTrnHead.Location = New System.Drawing.Point(8, 63)
        Me.lblTrnHead.Name = "lblTrnHead"
        Me.lblTrnHead.Size = New System.Drawing.Size(110, 15)
        Me.lblTrnHead.TabIndex = 220
        Me.lblTrnHead.Text = "Transaction Head"
        '
        'objSearchEmployee
        '
        Me.objSearchEmployee.BackColor = System.Drawing.Color.Transparent
        Me.objSearchEmployee.BackColor1 = System.Drawing.Color.Transparent
        Me.objSearchEmployee.BackColor2 = System.Drawing.Color.Transparent
        Me.objSearchEmployee.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objSearchEmployee.BorderSelected = False
        Me.objSearchEmployee.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objSearchEmployee.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objSearchEmployee.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objSearchEmployee.Location = New System.Drawing.Point(331, 33)
        Me.objSearchEmployee.Name = "objSearchEmployee"
        Me.objSearchEmployee.Size = New System.Drawing.Size(21, 21)
        Me.objSearchEmployee.TabIndex = 2
        '
        'cboEmployee
        '
        Me.cboEmployee.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboEmployee.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboEmployee.FormattingEnabled = True
        Me.cboEmployee.Location = New System.Drawing.Point(129, 33)
        Me.cboEmployee.Name = "cboEmployee"
        Me.cboEmployee.Size = New System.Drawing.Size(196, 21)
        Me.cboEmployee.TabIndex = 1
        '
        'lblEmployee
        '
        Me.lblEmployee.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEmployee.Location = New System.Drawing.Point(8, 36)
        Me.lblEmployee.Name = "lblEmployee"
        Me.lblEmployee.Size = New System.Drawing.Size(110, 15)
        Me.lblEmployee.TabIndex = 85
        Me.lblEmployee.Text = "Employee"
        '
        'eZeeHeader
        '
        Me.eZeeHeader.BackColor = System.Drawing.SystemColors.Control
        Me.eZeeHeader.BorderColor = System.Drawing.SystemColors.ControlDark
        Me.eZeeHeader.DescriptionForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.eZeeHeader.Dock = System.Windows.Forms.DockStyle.Top
        Me.eZeeHeader.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.eZeeHeader.GradiantStyle = eZee.Common.eZeeHeader.GradientStyle.Central
        Me.eZeeHeader.GradientColor1 = System.Drawing.SystemColors.Window
        Me.eZeeHeader.GradientColor2 = System.Drawing.SystemColors.Control
        Me.eZeeHeader.HeaderTextForeColor = System.Drawing.SystemColors.ControlText
        Me.eZeeHeader.Icon = Nothing
        Me.eZeeHeader.Location = New System.Drawing.Point(0, 0)
        Me.eZeeHeader.Message = ""
        Me.eZeeHeader.Name = "eZeeHeader"
        Me.eZeeHeader.Size = New System.Drawing.Size(660, 60)
        Me.eZeeHeader.TabIndex = 4
        Me.eZeeHeader.Title = "Employee Cost Center"
        '
        'lvEmpCostCenter
        '
        Me.lvEmpCostCenter.BackColorOnChecked = True
        Me.lvEmpCostCenter.ColumnHeaders = Nothing
        Me.lvEmpCostCenter.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.colhEmpCode, Me.colhEmpName, Me.colhTrnHead, Me.colhCcName, Me.colhEmployeeunkid, Me.colhTranheadunkid, Me.colhCostcenterunkid})
        Me.lvEmpCostCenter.CompulsoryColumns = ""
        Me.lvEmpCostCenter.FullRowSelect = True
        Me.lvEmpCostCenter.GridLines = True
        Me.lvEmpCostCenter.GroupingColumn = Nothing
        Me.lvEmpCostCenter.HideSelection = False
        Me.lvEmpCostCenter.Location = New System.Drawing.Point(12, 188)
        Me.lvEmpCostCenter.MinColumnWidth = 50
        Me.lvEmpCostCenter.MultiSelect = False
        Me.lvEmpCostCenter.Name = "lvEmpCostCenter"
        Me.lvEmpCostCenter.OptionalColumns = ""
        Me.lvEmpCostCenter.ShowMoreItem = False
        Me.lvEmpCostCenter.ShowSaveItem = False
        Me.lvEmpCostCenter.ShowSelectAll = True
        Me.lvEmpCostCenter.ShowSizeAllColumnsToFit = True
        Me.lvEmpCostCenter.Size = New System.Drawing.Size(636, 149)
        Me.lvEmpCostCenter.Sortable = True
        Me.lvEmpCostCenter.TabIndex = 8
        Me.lvEmpCostCenter.UseCompatibleStateImageBehavior = False
        Me.lvEmpCostCenter.View = System.Windows.Forms.View.Details
        '
        'colhEmpCode
        '
        Me.colhEmpCode.Tag = "colhEmpCode"
        Me.colhEmpCode.Text = "Emp. Code"
        Me.colhEmpCode.Width = 90
        '
        'colhEmpName
        '
        Me.colhEmpName.Tag = "colhEmpName"
        Me.colhEmpName.Text = "Employee Name"
        Me.colhEmpName.Width = 170
        '
        'colhTrnHead
        '
        Me.colhTrnHead.Tag = "colhTrnHead"
        Me.colhTrnHead.Text = "Transaction Head"
        Me.colhTrnHead.Width = 160
        '
        'colhCcName
        '
        Me.colhCcName.Tag = "colhCcName"
        Me.colhCcName.Text = "Cost Center"
        Me.colhCcName.Width = 210
        '
        'colhEmployeeunkid
        '
        Me.colhEmployeeunkid.Tag = "colhEmployeeunkid"
        Me.colhEmployeeunkid.Text = "Employeeunkid"
        Me.colhEmployeeunkid.Width = 0
        '
        'colhTranheadunkid
        '
        Me.colhTranheadunkid.Tag = "colhTranheadunkid"
        Me.colhTranheadunkid.Text = "Tranheadunkid"
        Me.colhTranheadunkid.Width = 0
        '
        'colhCostcenterunkid
        '
        Me.colhCostcenterunkid.Tag = "colhCostcenterunkid"
        Me.colhCostcenterunkid.Text = "Costcenterunkid"
        Me.colhCostcenterunkid.Width = 0
        '
        'objFooter
        '
        Me.objFooter.BorderColor = System.Drawing.Color.Silver
        Me.objFooter.Controls.Add(Me.btnSalaryCC)
        Me.objFooter.Controls.Add(Me.btnDelete)
        Me.objFooter.Controls.Add(Me.btnClose)
        Me.objFooter.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.objFooter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objFooter.GradiantStyle = eZee.Common.eZeeFooter.GradientStyle.Up
        Me.objFooter.GradientColor1 = System.Drawing.SystemColors.Control
        Me.objFooter.GradientColor2 = System.Drawing.SystemColors.Control
        Me.objFooter.Location = New System.Drawing.Point(0, 343)
        Me.objFooter.Name = "objFooter"
        Me.objFooter.Size = New System.Drawing.Size(660, 55)
        Me.objFooter.TabIndex = 15
        '
        'btnSalaryCC
        '
        Me.btnSalaryCC.BackColor = System.Drawing.Color.White
        Me.btnSalaryCC.BackgroundImage = CType(resources.GetObject("btnSalaryCC.BackgroundImage"), System.Drawing.Image)
        Me.btnSalaryCC.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnSalaryCC.BorderColor = System.Drawing.Color.Empty
        Me.btnSalaryCC.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnSalaryCC.FlatAppearance.BorderSize = 0
        Me.btnSalaryCC.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnSalaryCC.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSalaryCC.ForeColor = System.Drawing.Color.Black
        Me.btnSalaryCC.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnSalaryCC.GradientForeColor = System.Drawing.Color.Black
        Me.btnSalaryCC.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSalaryCC.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnSalaryCC.Location = New System.Drawing.Point(12, 13)
        Me.btnSalaryCC.Name = "btnSalaryCC"
        Me.btnSalaryCC.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSalaryCC.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnSalaryCC.Size = New System.Drawing.Size(139, 30)
        Me.btnSalaryCC.TabIndex = 11
        Me.btnSalaryCC.Text = "&Salary Cost Center"
        Me.btnSalaryCC.UseVisualStyleBackColor = True
        '
        'btnDelete
        '
        Me.btnDelete.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnDelete.BackColor = System.Drawing.Color.White
        Me.btnDelete.BackgroundImage = CType(resources.GetObject("btnDelete.BackgroundImage"), System.Drawing.Image)
        Me.btnDelete.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnDelete.BorderColor = System.Drawing.Color.Empty
        Me.btnDelete.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnDelete.FlatAppearance.BorderSize = 0
        Me.btnDelete.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnDelete.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnDelete.ForeColor = System.Drawing.Color.Black
        Me.btnDelete.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnDelete.GradientForeColor = System.Drawing.Color.Black
        Me.btnDelete.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnDelete.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnDelete.Location = New System.Drawing.Point(456, 13)
        Me.btnDelete.Name = "btnDelete"
        Me.btnDelete.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnDelete.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnDelete.Size = New System.Drawing.Size(93, 30)
        Me.btnDelete.TabIndex = 9
        Me.btnDelete.Text = "&Delete"
        Me.btnDelete.UseVisualStyleBackColor = True
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnClose.BackColor = System.Drawing.Color.White
        Me.btnClose.BackgroundImage = CType(resources.GetObject("btnClose.BackgroundImage"), System.Drawing.Image)
        Me.btnClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnClose.BorderColor = System.Drawing.Color.Empty
        Me.btnClose.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnClose.FlatAppearance.BorderSize = 0
        Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClose.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.ForeColor = System.Drawing.Color.Black
        Me.btnClose.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnClose.GradientForeColor = System.Drawing.Color.Black
        Me.btnClose.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Location = New System.Drawing.Point(555, 13)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Size = New System.Drawing.Size(93, 30)
        Me.btnClose.TabIndex = 10
        Me.btnClose.Text = "&Close"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'frmEmployeeCostCenter
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(660, 398)
        Me.Controls.Add(Me.pnlEmpCostCenter)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmEmployeeCostCenter"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Employee Cost Center"
        Me.pnlEmpCostCenter.ResumeLayout(False)
        Me.gbCostCenterInfo.ResumeLayout(False)
        Me.objFooter.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents pnlEmpCostCenter As System.Windows.Forms.Panel
    Friend WithEvents gbCostCenterInfo As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents btnSave As eZee.Common.eZeeLightButton
    Friend WithEvents objSearchCostCenter As eZee.Common.eZeeGradientButton
    Friend WithEvents objSearchED As eZee.Common.eZeeGradientButton
    Friend WithEvents lblCostCenter As System.Windows.Forms.Label
    Friend WithEvents cboCostcenter As System.Windows.Forms.ComboBox
    Friend WithEvents cboED As System.Windows.Forms.ComboBox
    Friend WithEvents lblTrnHead As System.Windows.Forms.Label
    Friend WithEvents objSearchEmployee As eZee.Common.eZeeGradientButton
    Friend WithEvents cboEmployee As System.Windows.Forms.ComboBox
    Friend WithEvents lblEmployee As System.Windows.Forms.Label
    Friend WithEvents eZeeHeader As eZee.Common.eZeeHeader
    Friend WithEvents lvEmpCostCenter As eZee.Common.eZeeListView
    Friend WithEvents colhEmpName As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhTrnHead As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhCcName As System.Windows.Forms.ColumnHeader
    Friend WithEvents objFooter As eZee.Common.eZeeFooter
    Friend WithEvents btnClose As eZee.Common.eZeeLightButton
    Friend WithEvents btnDelete As eZee.Common.eZeeLightButton
    Friend WithEvents colhEmployeeunkid As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhTranheadunkid As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhCostcenterunkid As System.Windows.Forms.ColumnHeader
    Friend WithEvents btnSalaryCC As eZee.Common.eZeeLightButton
    Friend WithEvents colhEmpCode As System.Windows.Forms.ColumnHeader
    Friend WithEvents lblDefCC As System.Windows.Forms.Label
End Class
