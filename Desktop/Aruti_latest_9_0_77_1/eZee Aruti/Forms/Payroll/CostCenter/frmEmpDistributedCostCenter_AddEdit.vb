﻿
Option Strict On

Imports eZeeCommonLib
Imports Aruti.Data

Public Class frmEmpDistributedCostCenter_AddEdit

#Region " Private Variables "

    Private ReadOnly mstrModuleName As String = "frmEmpDistributedCostCenter_AddEdit"
    Private mblnCancel As Boolean = True
    Private menAction As enAction = enAction.ADD_ONE
    Private mintEmpcostcenterUnkid As Integer = -1
    Private objEmployeeCostcenter As clsemployee_costcenter_Tran
    Private mintItemIndex As Integer = -1
    Private mdblTotPerc As Double = 0
    Private mdtTran As DataTable
    'Sohail (29 Mar 2017) -- Start
    'CCBRT Enhancement - 66.1 - Employee cost center distribution for non-salary heads, Effective period option on Employee Cost Center.
    'Private mstrSalaryHeadIDs As String
    Private mdtPeriod_startdate As DateTime
    Private mdtPeriod_enddate As DateTime
    Private mintPeriod_Status As Integer
    Private mintEmployeeID As Integer = 0
    Private mintTranHeadID As Integer = 0
    Private mintCostCenterID As Integer = 0
    'Sohail (29 Mar 2017) -- End
    'Hemant (26 Dec 2018) -- Start
    'Enhancement - On employee cost centre, provide a global option to assign all employees, on a single transaction head to one cost centre. 
    'Also on this screen, provide an analysis by option where user can filter out employees in 76.1.
    Private dvEmployee As DataView
    Private mintCount As Integer = 0
    Private mintTotalEmployee As Integer = 0
    Private mstrSearchEmpText As String = ""
    Private mstrAdvanceFilter As String = ""
    'Hemant (26 Dec 2018) -- End

    'Hemant (11 Mar 2018) -- Start
    'ISSUE : performance issue on emp cost centre list screen, taking 30 to 45 mins to open
     Private mstrSearchText As String = ""
     'Hemant (11 Mar 2018) -- End
    'Sohail (23 Mar 2020) -- Start
    'Internal Enhancement # : Showing progress counts on employee cost center screen.
    Private mblnProcessFailed As Boolean = False
    Private mblnStopPayrollProcess As Boolean = False
    'Sohail (23 Mar 2020) -- End

#End Region

#Region " Display Dialog "

    Public Function displayDialog(ByRef intUnkId As Integer, ByVal eAction As enAction, Optional ByVal intEmpID As Integer = 0, Optional ByVal intTranHeadID As Integer = 0, Optional ByVal intCostCenterID As Integer = 0) As Boolean
        Try
            mintEmpcostcenterUnkid = intUnkId
            menAction = eAction

            'Sohail (11 Sep 2010) -- Start
            'Sohail (29 Mar 2017) -- Start
            'CCBRT Enhancement - 66.1 - Employee cost center distribution for non-salary heads, Effective period option on Employee Cost Center.
            'mintEmployeeID = intEmpID
            'mintTranHeadID = intTranHeadID
            'mintCostCenterID = intCostCenterID
            mintEmployeeID = intEmpID
            mintTranHeadID = intTranHeadID
            mintCostCenterID = intCostCenterID
            'Sohail (29 Mar 2017) -- End
            'Sohail (11 Sep 2010) -- End

            Me.ShowDialog()

            intUnkId = mintEmpcostcenterUnkid

            Return Not mblnCancel
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "displayDialog", mstrModuleName)
        End Try
    End Function

#End Region

#Region " Private Methods "

    Private Sub SetColor()
        Try
            cboEmployee.BackColor = GUI.ColorComp
            cboTranHead.BackColor = GUI.ColorComp
            cboCostcenter.BackColor = GUI.ColorComp
            txtPercentage.BackColor = GUI.ColorComp
            cboPeriod.BackColor = GUI.ColorComp 'Sohail (29 Mar 2017)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetColor", mstrModuleName)
        End Try
    End Sub

    Private Sub GetValue()
        Try
            'Hemant (26 Dec 2018) -- Start
            'Enhancement - On employee cost centre, provide a global option to assign all employees, on a single transaction head to one cost centre. 
            'Also on this screen, provide an analysis by option where user can filter out employees in 76.1.
            'cboEmployee.SelectedValue = objEmployeeCostcenter._Employeeunkid
            If menAction = enAction.EDIT_ONE Then
                cboPeriod.SelectedValue = objEmployeeCostcenter._Periodunkid
            End If
            'Hemant (26 Dec 2018) -- End
            cboTranHead.SelectedValue = objEmployeeCostcenter._Tranheadunkid
            cboCostcenter.SelectedValue = objEmployeeCostcenter._Costcenterunkid
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetValue", mstrModuleName)
        End Try
    End Sub

    Private Sub ResetValue()
        Try
            cboCostcenter.SelectedValue = 0
            txtPercentage.Text = "100.00"
            btnAdd.Enabled = True
            cboTranHead.Enabled = True 'Sohail (29 Mar 2017)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "ResetValue", mstrModuleName)
        End Try
    End Sub

    Private Sub SetValue()
        Try
            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'objEmployeeCostcenter._Costcentertranunkid = mintEmpcostcenterUnkid
            objEmployeeCostcenter._Costcentertranunkid(FinancialYear._Object._DatabaseName) = mintEmpcostcenterUnkid
            'Sohail (21 Aug 2015) -- End
            objEmployeeCostcenter._Employeeunkid = CInt(cboEmployee.SelectedValue)
            objEmployeeCostcenter._Tranheadunkid = CInt(cboTranHead.SelectedValue)
            objEmployeeCostcenter._Costcenterunkid = CInt(cboCostcenter.SelectedValue)
            objEmployeeCostcenter._Userunkid = User._Object._Userunkid
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetValue", mstrModuleName)
        End Try
    End Sub

    Private Sub FillCombo()
        'Sohail (29 Mar 2017) -- Start
        'CCBRT Enhancement - 66.1 - Employee cost center distribution for non-salary heads, Effective period option on Employee Cost Center.
        Dim objPeriod As New clscommom_period_Tran
        Dim objMaster As New clsMasterData
        Dim intCurrPeriodID As Integer
        'Sohail (29 Mar 2017) -- End
        Dim dsFill As DataSet = Nothing
        'Hemant (26 Dec 2018) -- Start
        'Enhancement - On employee cost centre, provide a global option to assign all employees, on a single transaction head to one cost centre. 
        'Also on this screen, provide an analysis by option where user can filter out employees in 76.1.
        'Sohail (07 Feb 2019) -- Start
        'NMB Enhancement - 76.1 - Include all heads including Payable heads on employee cost centre screen.
        'Dim objED As New clsEarningDeduction
        Dim objHead As New clsTransactionHead
        'Sohail (07 Feb 2019) -- End
        Dim dsList As DataSet
        Dim dtTable As DataTable
        'Hemant (26 Dec 2018) -- End

        Try

            'Dim objEmployee As New clsEmployee_Master 'Sohail (29 Mar 2017)


            'Anjan [10 June 2015] -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'Sohail (06 Jan 2012) -- Start
            'TRA - ENHANCEMENT
            'dsFill = objEmployee.GetEmployeeList("Employee", True, True)
            If menAction = enAction.EDIT_ONE Then
                'dsFill = objEmployee.GetEmployeeList("Employee",  True, , , , , , , , , , , , , eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate))
            Else
                'dsFill = objEmployee.GetEmployeeList("Employee", True, , , , , , , , , , , , , ConfigParameter._Object._CurrentDateAndTime.Date, ConfigParameter._Object._CurrentDateAndTime.Date)
            End If

            'Anjan [10 June 2015] -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'Sohail (29 Mar 2017) -- Start
            'CCBRT Enhancement - 66.1 - Employee cost center distribution for non-salary heads, Effective period option on Employee Cost Center.
            'dsFill = objEmployee.GetEmployeeList(FinancialYear._Object._DatabaseName, _
            '                               User._Object._Userunkid, _
            '                               FinancialYear._Object._YearUnkid, _
            '                               Company._Object._Companyunkid, _
            '                               eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
            '                               eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
            '                               ConfigParameter._Object._UserAccessModeSetting, _
            '                               True, False, "Employee", True)
            'Sohail (29 Mar 2017) -- End
            'Anjan [10 June 2015] -- End


            'Sohail (29 Mar 2017) -- Start
            'CCBRT Enhancement - 66.1 - Employee cost center distribution for non-salary heads, Effective period option on Employee Cost Center.
            'Sohail (06 Jan 2012) -- End
            'cboEmployee.ValueMember = "employeeunkid"
            'cboEmployee.DisplayMember = "employeename"
            'cboEmployee.DataSource = dsFill.Tables("Employee")
            'Sohail (29 Mar 2017) -- End

            'Sohail (07 Feb 2019) -- Start
            'NMB Enhancement - 76.1 - Employee cost centre should be distributed on all allocations instead of only on cost centre.
            'dsFill = Nothing
            'Dim objCostcenter As New clscostcenter_master
            'dsFill = objCostcenter.getComboList("CostCenter", True)
            'cboCostcenter.ValueMember = "costcenterunkid"
            'cboCostcenter.DisplayMember = "costcentername"
            'cboCostcenter.DataSource = dsFill.Tables("CostCenter")
            dsFill = objMaster.GetEAllocation_Notification("List")
            With cboAllocation
                .ValueMember = "id"
                .DisplayMember = "name"
                .DataSource = dsFill.Tables(0)
                .SelectedValue = enAllocation.COST_CENTER
            End With
            'Sohail (07 Feb 2019) -- End

            'Sohail (29 Mar 2017) -- Start
            'CCBRT Enhancement - 66.1 - Employee cost center distribution for non-salary heads, Effective period option on Employee Cost Center.
            intCurrPeriodID = objMaster.getFirstPeriodID(enModuleReference.Payroll, FinancialYear._Object._YearUnkid, enStatusType.Open)
            'Hemant (26 Dec 2018) -- Start
            'Enhancement - On employee cost centre, provide a global option to assign all employees, on a single transaction head to one cost centre. 
            'Also on this screen, provide an analysis by option where user can filter out employees in 76.1.           
            'dsFill = objPeriod.getListForCombo(enModuleReference.Payroll, FinancialYear._Object._YearUnkid, FinancialYear._Object._DatabaseName, FinancialYear._Object._Database_Start_Date, "Period", True, 0)
            'Hemant (06 Feb 2019) -- Start
            'Issue : InvalidArgument=Value of '5' is not valid for 'index'.
            'dsFill = objPeriod.getListForCombo(enModuleReference.Payroll, FinancialYear._Object._YearUnkid, FinancialYear._Object._DatabaseName, FinancialYear._Object._Database_Start_Date, "Period", True, enStatusType.Open)
            dsFill = objPeriod.getListForCombo(enModuleReference.Payroll, FinancialYear._Object._YearUnkid, FinancialYear._Object._DatabaseName, FinancialYear._Object._Database_Start_Date, "Period", True, 0)
            'Hemant (06 Feb 2019) -- End
            'Hemant (26 Dec 2018) -- End
            With cboPeriod
                .ValueMember = "periodunkid"
                .DisplayMember = "Name"
                .DataSource = dsFill.Tables("Period")
                .SelectedValue = intCurrPeriodID
            End With
            'Sohail (29 Mar 2017) -- End

            'Hemant (26 Dec 2018) -- Start
            'Enhancement - On employee cost centre, provide a global option to assign all employees, on a single transaction head to one cost centre. 
            'Also on this screen, provide an analysis by option where user can filter out employees in 76.1.
            'Sohail (07 Feb 2019) -- Start
            'NMB Enhancement - 76.1 - Include all heads including Payable heads on employee cost centre screen.
            'dsList = objED.getListForCombo_TransactionHead("TranHead", 0, mdtPeriod_enddate)
            dsList = objHead.getComboList(FinancialYear._Object._DatabaseName, "TranHead", True, , , , True, False, "", True, False, True)
            'Sohail (07 Feb 2019) -- End
            dtTable = New DataView(dsList.Tables("TranHead"), "", "", DataViewRowState.CurrentRows).ToTable
            With cboTranHead
                .ValueMember = "tranheadunkid"
                'Sohail (07 Feb 2019) -- Start
                'NMB Enhancement - 76.1 - Include all heads including Payable heads on employee cost centre screen.
                '.DisplayMember = "trnheadname"
                .DisplayMember = "name"
                'Sohail (07 Feb 2019) -- End
                .DataSource = dtTable
                .SelectedValue = 0

                'Hemant (11 Mar 2018) -- Start
                'ISSUE : performance issue on emp cost centre list screen, taking 30 to 45 mins to open
                Call SetDefaultSearchText(cboTranHead)
                'Hemant (11 Mar 2018) -- End

            End With
            'Hemant (26 Dec 2018) -- End


        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
            'Sohail (29 Mar 2017) -- Start
            'CCBRT Enhancement - 66.1 - Employee cost center distribution for non-salary heads, Effective period option on Employee Cost Center.
        Finally
            objPeriod = Nothing
            objMaster = Nothing
            'Sohail (29 Mar 2017) -- End
        End Try
    End Sub

    'Sohail (29 Mar 2017) -- Start
    'CCBRT Enhancement - 66.1 - Employee cost center distribution for non-salary heads, Effective period option on Employee Cost Center.
    Private Sub FillCombo_Employee()
        Dim objEmployee As New clsEmployee_Master
        Dim dsList As DataSet
        'Hemant (11 Mar 2018) -- Start
        'ISSUE/ENHANCEMENT : need filter to show only newly hired, rehired employees and those not assigned, same as on global assign
        Dim strTempTableSelectQry As String = ""
        Dim strTempTableJoinQry As String = ""
        Dim strTempTableDropQry As String = ""
        'Hemant (11 Mar 2018) -- End
        Try

            'Hemant (26 Dec 2018) -- Start
            'Enhancement - On employee cost centre, provide a global option to assign all employees, on a single transaction head to one cost centre. 
            'Also on this screen, provide an analysis by option where user can filter out employees in 76.1.
            'dsList = objEmployee.GetEmployeeList(FinancialYear._Object._DatabaseName, _
            '                                           User._Object._Userunkid, _
            '                                           FinancialYear._Object._YearUnkid, _
            '                                           Company._Object._Companyunkid, _
            '                                           mdtPeriod_startdate, _
            '                                           mdtPeriod_enddate, _
            '                                           ConfigParameter._Object._UserAccessModeSetting, _
            '                                           True, False, "EmployeeList", True)
            'With cboEmployee
            '    .BeginUpdate()
            '    .ValueMember = "employeeunkid"
            '    .DisplayMember = "employeename"
            '    .DataSource = dsList.Tables("EmployeeList")
            '    .SelectedValue = 0
            '    .EndUpdate()
            'End With
            Dim strFilter As String = ""
            'Hemant (11 Mar 2018) -- Start
            'ISSUE/ENHANCEMENT : need filter to show only newly hired, rehired employees and those not assigned, same as on global assign
            Cursor.Current = Cursors.WaitCursor
            If chkShowNewlyHiredEmployees.Checked = True Then strFilter = " CONVERT(CHAR(8), appointeddate, 112) >= @startdate "

            If chkNotAssignedHead.Checked = True AndAlso CInt(cboPeriod.SelectedValue) > 0 AndAlso CInt(cboTranHead.SelectedValue) > 0 Then
                strTempTableSelectQry &= "SELECT     employeeunkid " & _
                                         "INTO #tblCostCenter " & _
                                         "FROM premployee_costcenter_tran " & _
                                         "WHERE isvoid = 0 " & _
                                             "AND periodunkid = " & CInt(cboPeriod.SelectedValue) & " " & _
                                             "AND tranheadunkid = " & CInt(cboTranHead.SelectedValue) & " "

                strTempTableJoinQry &= "LEFT JOIN #tblCostCenter ON #tblCostCenter.employeeunkid = hremployee_master.employeeunkid "

                strTempTableDropQry &= "AND #tblCostCenter.employeeunkid IS NULL; DROP TABLE #tblCostCenter "
            End If

            If chkShowReinstatedEmployees.Checked = True Then

                strTempTableSelectQry &= "SELECT DISTINCT hremployee_rehire_tran.employeeunkid " & _
                                         "INTO #tblRehire " & _
                                         "FROM   hremployee_rehire_tran " & _
                                        " WHERE isvoid = 0 " & _
                                                 "AND reinstatment_date IS NOT NULL " & _
                                                 "AND CONVERT(CHAR(8), effectivedate, 112) <= @enddate " & _
                                                 "AND CONVERT(CHAR(8), reinstatment_date, 112) BETWEEN @startdate AND @enddate "

                strTempTableJoinQry &= "JOIN #tblRehire ON #tblRehire.employeeunkid = hremployee_master.employeeunkid "

                strTempTableDropQry &= "DROP TABLE #tblRehire "

            End If
            'Hemant (11 Mar 2018) -- End

            If mstrAdvanceFilter <> "" Then
                If strFilter <> "" Then
                    strFilter &= " AND " & mstrAdvanceFilter
                Else
                    strFilter = mstrAdvanceFilter
                End If
            End If

            dsList = objEmployee.GetEmployeeList(FinancialYear._Object._DatabaseName, _
                                           User._Object._Userunkid, _
                                           FinancialYear._Object._YearUnkid, _
                                           Company._Object._Companyunkid, _
                                           mdtPeriod_startdate, _
                                           mdtPeriod_enddate, _
                                           ConfigParameter._Object._UserAccessModeSetting, _
                                                       True, False, "EmployeeList", False, objEmployeeCostcenter._Employeeunkid, , , , , , , , , , , , , , strFilter _
                                                        , strTempTableSelectQry:=strTempTableSelectQry _
                                                        , strTempTableJoinQry:=strTempTableJoinQry _
                                                        , strTempTableDropQry:=strTempTableDropQry _
                                                        )
            'Hemant (11 Mar 2018) -- [strTempTableSelectQry,strTempTableJoinQry,strTempTableDropQry]

            mintTotalEmployee = dsList.Tables("EmployeeList").Rows.Count
            objlblEmpCount.Text = "( 0 / " & mintTotalEmployee.ToString & " )"

            If dsList.Tables(0).Columns.Contains("IsChecked") = False Then
                Dim dtCol As DataColumn
                dtCol = New DataColumn("IsChecked", System.Type.GetType("System.Boolean"))
                dtCol.DefaultValue = False
                dtCol.AllowDBNull = False
                dsList.Tables(0).Columns.Add(dtCol)
            End If
            dvEmployee = dsList.Tables("EmployeeList").DefaultView
            dgEmployee.AutoGenerateColumns = False
            objdgcolhCheck.DataPropertyName = "IsChecked"
            objdgcolhEmployeeunkid.DataPropertyName = "employeeunkid"
            dgColhEmpCode.DataPropertyName = "employeecode"
            dgColhEmployee.DataPropertyName = "employeename"

            dgEmployee.DataSource = dvEmployee
            dvEmployee.Sort = "IsChecked DESC, employeename "
            'Hemant (26 Dec 2018) -- End
            'Hemant (06 Feb 2019) -- Start
            'Error : InvalidArgument=Value of '5' is not valid for 'index'.
            'With cboEmployee
            '    .BeginUpdate()
            '    .ValueMember = "employeeunkid"
            '    .DisplayMember = "employeename"
            '    .DataSource = dsList.Tables("EmployeeList")
            '    .SelectedValue = 0
            '    .EndUpdate()
            'End With
            'Hemant (06 Feb 2019) -- End
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo_Employee", mstrModuleName)
        Finally
            objEmployee = Nothing
            'Hemant (11 Mar 2018) -- Start
            'ISSUE/ENHANCEMENT : need filter to show only newly hired, rehired employees and those not assigned, same as on global assign
            Cursor.Current = Cursors.Default
            'Hemant (11 Mar 2018) --
        End Try
    End Sub
    'Sohail (29 Mar 2017) -- End

    Private Sub FillList()
        Dim dsList As New DataSet
        'Dim lvItem As ListViewItem 'Sohail (23 Mar 2020)
        Try

            'Hemant (11 Mar 2018) -- Start
            'ISSUE : performance issue on emp cost centre list screen, taking 30 to 45 mins to open
            'lvEmpCostCenter.Items.Clear()
            'Hemant (11 Mar 2018) -- End

            mdblTotPerc = 0

            'Sohail (29 Mar 2017) -- Start
            'CCBRT Enhancement - 66.1 - Employee cost center distribution for non-salary heads, Effective period option on Employee Cost Center.
            If cboPeriod.SelectedValue Is Nothing OrElse CInt(cboPeriod.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 12, "Please select Period."), enMsgBoxStyle.Information)
                cboPeriod.Focus()
                Exit Sub
            End If
            Dim intPrevPeriodID As Integer = 0
            'Sohail (29 Mar 2017) -- End

            'Dim lvArray As New List(Of ListViewItem) 'Sohail (23 Mar 2020)
            'Hemant (26 Dec 2018) -- Start
            'Enhancement - On employee cost centre, provide a global option to assign all employees, on a single transaction head to one cost centre. 
            'Also on this screen, provide an analysis by option where user can filter out employees in 76.1.
            'If CInt(cboEmployee.SelectedValue) > 0 Then
            dvEmployee.Table.AcceptChanges()
            'Hemant (11 Mar 2018) -- Start
            'ISSUE : performance issue on emp cost centre list screen, taking 30 to 45 mins to open

            'If dvEmployee.Table.Select("IsChecked = 1").Length > 0 Or menAction = enAction.EDIT_ONE Then
            '    'Hemant (26 Dec 2018) -- End


            '    lvEmpCostCenter.BeginUpdate()
            '    For Each dtRow As DataRow In mdtTran.Rows

            '        If CStr(IIf(IsDBNull(dtRow.Item("AUD")), "A", dtRow.Item("AUD"))) <> "D" Then
            '            lvItem = New ListViewItem

            '            lvItem.Text = dtRow("costcentertranunkid").ToString
            '            lvItem.Tag = CInt(dtRow("costcentertranunkid").ToString)

            '            lvItem.SubItems.Add(dtRow.Item("employeename").ToString)
            '            lvItem.SubItems(colhEmployee.Index).Tag = CInt(dtRow.Item("employeeunkid").ToString)

            '            lvItem.SubItems.Add(dtRow("trnheadname").ToString())
            '            lvItem.SubItems(colhTranHead.Index).Tag = CInt(dtRow.Item("tranheadunkid").ToString)

            '            'Sohail (07 Feb 2019) -- Start
            '            'NMB Enhancement - 76.1 - Employee cost centre should be distributed on all allocations instead of only on cost centre.
            '            lvItem.SubItems.Add(dtRow("allocationbyname").ToString())
            '            lvItem.SubItems(colhAllocation.Index).Tag = CInt(dtRow.Item("allocationbyid").ToString)
            '            'Sohail (07 Feb 2019) -- End

            '            lvItem.SubItems.Add(dtRow("costcentername").ToString())
            '            lvItem.SubItems(colhCostCenter.Index).Tag = CInt(dtRow.Item("costcenterunkid").ToString)

            '            lvItem.SubItems.Add(Format(CDec(dtRow.Item("percentage")), "00.00"))
            '            mdblTotPerc += CDec(dtRow.Item("percentage").ToString)
            '            lvItem.SubItems.Add(dtRow.Item("GUID").ToString)

            '            'Sohail (29 Mar 2017) -- Start
            '            'CCBRT Enhancement - 66.1 - Employee cost center distribution for non-salary heads, Effective period option on Employee Cost Center.
            '            lvItem.SubItems.Add(dtRow.Item("period_name").ToString)
            '            lvItem.SubItems(colhPeriod.Index).Tag = CInt(dtRow.Item("periodunkid"))
            '            'Sohail (29 Mar 2017) -- End

            '            lvArray.Add(lvItem)

            '            lvItem = Nothing
            '        End If
            '    Next
            '    lvEmpCostCenter.Items.AddRange(lvArray.ToArray)
            '    lvEmpCostCenter.EndUpdate()

            '    If lvEmpCostCenter.Items.Count > 17 Then
            '        colhPerc.Width = 80 - 18
            '    Else
            '        colhPerc.Width = 80
            '    End If
            '    'Sohail (07 Feb 2019) - [8, 120] to [17, 80]

            '    If lvEmpCostCenter.SelectedItems.Count > 0 Then
            '        lvEmpCostCenter.SelectedItems(0).Selected = True
            '    End If
            'Else
            '    lvEmpCostCenter.Items.Clear()
            'End If
            If dvEmployee.Table.Select("IsChecked = 1").Length > 0 Or menAction = enAction.EDIT_ONE Then
                dgvEmpCostCenter.AutoGenerateColumns = False
                objdgcolhID.DataPropertyName = "costcentertranunkid"
                dgcolhEmployee1.DataPropertyName = "employeename"
                dgcolhTranHead.DataPropertyName = "trnheadname"
                dgcolhCostCenter.DataPropertyName = "costcentername"
                dgcolhAllocation.DataPropertyName = "allocationbyname"
                dgcolhPerc.DataPropertyName = "percentage"
                dgcolhPeriod.DataPropertyName = "period_name"
                objdgColhGUID.DataPropertyName = "GUID"
                objdgcolhPeriodID.DataPropertyName = "periodunkid"
                objcolhTranHeadID.DataPropertyName = "tranheadunkid"
                objcolhAllocationID.DataPropertyName = "allocationbyid"
                objcolhCostCenterID.DataPropertyName = "costcenterunkid"
                dgcolhEmployee1.AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill

                Dim dt As DataTable = New DataView(mdtTran, "AUD <> 'D'", "", DataViewRowState.CurrentRows).ToTable
                dgvEmpCostCenter.DataSource = dt
                dgvEmpCostCenter.ClearSelection()

            End If
            'Hemant (11 Mar 2018) -- End
            Call ResetValue()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillList", mstrModuleName)
        End Try
    End Sub

    Private Function IsValid() As Boolean
        Try
            'Sohail (29 Mar 2017) -- Start
            'CCBRT Enhancement - 66.1 - Employee cost center distribution for non-salary heads, Effective period option on Employee Cost Center.
            If cboPeriod.SelectedValue Is Nothing Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 11, "Sorry, This Period is closed."), enMsgBoxStyle.Information)
                Return False
            ElseIf CInt(cboPeriod.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 12, "Please select Period."), enMsgBoxStyle.Information)
                cboPeriod.Focus()
                Return False
            ElseIf mintPeriod_Status = enStatusType.Close Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 11, "Sorry, This Period is closed."), enMsgBoxStyle.Information)
                cboPeriod.Focus()
                Return False
            End If
            'Sohail (29 Mar 2017) -- End

            'Hemant (26 Dec 2018) -- Start
            'Enhancement - On employee cost centre, provide a global option to assign all employees, on a single transaction head to one cost centre. 
            'Also on this screen, provide an analysis by option where user can filter out employees in 76.1.
            'If CInt(cboEmployee.SelectedValue) <= 0 Then
            '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Employee is compulosry information. Please select Employee to continue."), enMsgBoxStyle.Information)
            '    cboEmployee.Focus()
            '    Return False
            If dvEmployee.Table.Select("IsChecked = 1 ").Length <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 14, "Please select atleast one Employee from list."), enMsgBoxStyle.Information)
                dgEmployee.Focus()
                Return False
                'Hemant (26 Dec 2018) -- End
            ElseIf CInt(cboTranHead.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Transaction Head is compulosry information. Please select Transaction Head to continue."), enMsgBoxStyle.Information)
                cboTranHead.Focus()
                Return False
            ElseIf CInt(cboCostcenter.SelectedValue) <= 0 Then
                'Sohail (07 Feb 2019) -- Start
                'NMB Enhancement - 76.1 - Employee cost centre should be distributed on all allocations instead of only on cost centre.
                'eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Cost center is compulosry information. Please select Cost center to continue."), enMsgBoxStyle.Information)
                eZeeMsgBox.Show(lblCostCenter.Text & " " & Language.getMessage(mstrModuleName, 17, "is compulosry information. Please select") & " " & lblCostCenter.Text, enMsgBoxStyle.Information)
                'Sohail (07 Feb 2019) -- End
                cboCostcenter.Focus()
                Return False
            ElseIf txtPercentage.Decimal <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Sorry, Percentage should be greater than Zero."), enMsgBoxStyle.Information)
                txtPercentage.Focus()
                Return False
            ElseIf txtPercentage.Decimal > 100 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "Sorry, Percentage should not be greater than 100."), enMsgBoxStyle.Information)
                txtPercentage.Focus()
                Return False
                'Sohail (29 Mar 2017) -- Start
                'CCBRT Enhancement - 66.1 - Employee cost center distribution for non-salary heads, Effective period option on Employee Cost Center.
            Else
                Dim objTnALeave As New clsTnALeaveTran
                'Hemant (26 Dec 2018) -- Start
                'Enhancement - On employee cost centre, provide a global option to assign all employees, on a single transaction head to one cost centre. 
                'Also on this screen, provide an analysis by option where user can filter out employees in 76.1.
                'If objTnALeave.IsPayrollProcessDone(CInt(cboPeriod.SelectedValue), cboEmployee.SelectedValue.ToString, mdtPeriod_enddate) = True Then
                '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 13, "Sorry, Payroll Process is already done."), enMsgBoxStyle.Information)
                '    Return False
                'End If
                'Sohail (12 Mar 2020) -- Start
                'NMB Enhancement # : Performance enhancement on employee cost center screen.
                'For Each dtRow As DataRow In dvEmployee.Table.Select("IsChecked = 1")
                '    If objTnALeave.IsPayrollProcessDone(CInt(cboPeriod.SelectedValue), dtRow.Item("employeeunkid").ToString, mdtPeriod_enddate) = True Then
                '        'Sohail (19 Apr 2019) -- Start
                '        'Enhancement - 76.1 - Option to open process payroll screen if process payroll is already done message come.
                '        'eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 16, "Sorry, Payroll Process is already done for employee : ") & dtRow.Item("employeename").ToString, enMsgBoxStyle.Information)
                '        If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 16, "Sorry, Payroll Process is already done for employee : ") & dtRow.Item("employeename").ToString & vbCrLf & vbCrLf & Language.getMessage(mstrModuleName, 19, "Do you want to void Payroll?"), CType(enMsgBoxStyle.Information + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.Yes Then
                '            Dim objFrm As New frmProcessPayroll
                '            objFrm.ShowDialog()
                '        End If
                '        'Sohail (19 Apr 2019) -- End
                '        Return False
                '    End If
                'Next
                Dim dtEmpList As DataTable = Nothing
                Dim strIDs As String = String.Join(",", (From p In dvEmployee.Table.AsEnumerable() Where (CBool(p.Item("IsChecked")) = True) Select (p.Item("employeeunkid").ToString)).ToArray)
                If objTnALeave.IsPayrollProcessDone(CInt(cboPeriod.SelectedValue), strIDs, mdtPeriod_enddate, dtEmpList:=dtEmpList) = True Then
                    Dim objYesNp As New frmCommonValidationList
                    If objYesNp.displayDialog(True, Language.getMessage(mstrModuleName, 16, "Sorry, Payroll Process is already done for some of the employees.") & vbCrLf & "  " & Language.getMessage(mstrModuleName, 19, "Do you want to void Payroll?"), dtEmpList) = False Then
                        Return False
                        Exit Try
                    Else
                            Dim objFrm As New frmProcessPayroll
                            objFrm.ShowDialog()
                        End If
                    Return False
                End If
                'Sohail (12 Mar 2020) -- End
                'Hemant (26 Dec 2018) -- End            
                'Sohail (29 Mar 2017) -- End
            End If

            Return True
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "IsValid", mstrModuleName)
        End Try
    End Function
    Private Sub SetVisibility()

        Try
            btnSave.Enabled = User._Object.Privilege._AddEmployeeCostCenter
            btnDelete.Enabled = User._Object.Privilege._DeleteEmployeeCostCenter

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetVisibility", mstrModuleName)
        End Try

    End Sub

    'Hemant (26 Dec 2018) -- Start
    'Enhancement - On employee cost centre, provide a global option to assign all employees, on a single transaction head to one cost centre. 
    'Also on this screen, provide an analysis by option where user can filter out employees in 76.1.
    Private Sub SetCheckBoxValue()
        Try

            RemoveHandler objchkSelectAll.CheckedChanged, AddressOf chkSelectAll_CheckedChanged

            Dim blnIsChecked As Boolean = Convert.ToBoolean(dgEmployee.CurrentRow.Cells(objdgcolhCheck.Index).Value)

            If blnIsChecked = True Then
                mintCount += 1
            Else
                mintCount -= 1
            End If

            objlblEmpCount.Text = "( " & mintCount.ToString & " / " & mintTotalEmployee.ToString & " )"

            If mintCount <= 0 Then
                objchkSelectAll.CheckState = CheckState.Unchecked
            ElseIf mintCount < dgEmployee.Rows.Count Then
                objchkSelectAll.CheckState = CheckState.Indeterminate
            ElseIf mintCount = dgEmployee.Rows.Count Then
                objchkSelectAll.CheckState = CheckState.Checked
            End If

            AddHandler objchkSelectAll.CheckedChanged, AddressOf chkSelectAll_CheckedChanged
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetCheckBoxValue", mstrModuleName)
        End Try

    End Sub

    Private Sub SetDefaultSearchEmpText()
        Try
            mstrSearchEmpText = lblSearchEmp.Text
            With txtSearchEmp
                .ForeColor = Color.Gray
                .Text = mstrSearchEmpText
                .Font = New Font(Me.Font.FontFamily, Me.Font.Size, FontStyle.Italic)
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "", mstrModuleName)
        End Try
    End Sub

    Private Sub CheckAllEmployee(ByVal blnCheckAll As Boolean)
        Try
            If dvEmployee IsNot Nothing Then
                For Each dr As DataRowView In dvEmployee
                    dr.Item("IsChecked") = blnCheckAll
                    dr.EndEdit()
                Next
                dvEmployee.ToTable.AcceptChanges()

                Dim drRow As DataRow() = dvEmployee.Table.Select("IsChecked = 1")
                mintCount = drRow.Length
                objlblEmpCount.Text = "( " & mintCount.ToString & " / " & mintTotalEmployee.ToString & " )"
            End If
            'Sohail (18 Jul 2017) -- End
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "CheckAllEmployee", mstrModuleName)
        End Try
    End Sub
    'Hemant (26 Dec 2018) -- End

    'Hemant (11 Mar 2018) -- Start
    'ISSUE : performance issue on emp cost centre list screen, taking 30 to 45 mins to open
    Private Sub SetDefaultSearchText(ByVal cbo As ComboBox)
        Try
            mstrSearchText = Language.getMessage(mstrModuleName, 18, "Type to Search")
            With cbo
                .ForeColor = Color.Gray
                .Text = mstrSearchText
                .Font = New Font(Me.Font.FontFamily, Me.Font.Size, FontStyle.Italic)
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetDefaultSearchText", mstrModuleName)
        End Try
    End Sub

    Private Sub SetRegularFont(ByVal cbo As ComboBox)
        Try
            With cbo
                .ForeColor = Color.Black
                .Font = New Font(Me.Font.FontFamily, Me.Font.Size, FontStyle.Regular)
                .SelectionLength = 0
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetDefauSetRegularFontltSearchText", mstrModuleName)
        End Try
    End Sub
    'Hemant (11 Mar 2018) -- End

    'Sohail (23 Mar 2020) -- Start
    'Internal Enhancement # : Showing progress counts on employee cost center screen.
    Private Sub EnableControls(ByVal blnEnable As Boolean)
        Try
            Dim lst As IEnumerable(Of Control) = pnlMainInfo.Controls.OfType(Of Control)().Where(Function(t) t.Name <> objFooter.Name)
            For Each ctrl In lst
                ctrl.Enabled = blnEnable
            Next
            lst = objFooter.Controls.OfType(Of Control).Where(Function(x) x.Name <> btnStop.Name)
            For Each ctrl In lst
                ctrl.Enabled = blnEnable
            Next
            If blnEnable = True Then
                btnStop.Visible = False
            Else
                btnStop.Visible = True
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "EnableControls", mstrModuleName)
        End Try
    End Sub
    'Sohail (23 Mar 2020) -- End

#End Region

#Region " Form's Events "

    Private Sub frmEmpDistributedCostCenter_AddEdit_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        Try
            objEmployeeCostcenter = Nothing
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmEmpDistributedCostCenter_AddEdit_FormClosed", mstrModuleName)
        End Try
    End Sub

    Private Sub frmEmpDistributedCostCenter_AddEdit_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown
        Try
            If e.KeyCode = Keys.Return Then
                SendKeys.Send("{TAB}")
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmEmpDistributedCostCenter_AddEdit_KeyDown", mstrModuleName)
        End Try
    End Sub

    Private Sub frmEmpDistributedCostCenter_AddEdit_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        objEmployeeCostcenter = New clsemployee_costcenter_Tran
        Try
            Call Set_Logo(Me, gApplicationType)
            'Sohail (10 Jul 2014) -- Start
            'Enhancement - Custom Language.
            Language.setLanguage(Me.Name)
            Call OtherSettings()
            'Sohail (10 Jul 2014) -- End

            Call SetVisibility()
            SetColor()
            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'mstrSalaryHeadIDs = (New clsTransactionHead).GetSalaryHeadsIDs(True)
            'Sohail (29 Mar 2017) -- Start
            'CCBRT Enhancement - 66.1 - Employee cost center distribution for non-salary heads, Effective period option on Employee Cost Center.
            'mstrSalaryHeadIDs = (New clsTransactionHead).GetSalaryHeadsIDs(FinancialYear._Object._DatabaseName, True)
            'Sohail (29 Mar 2017) -- End
            'Sohail (21 Aug 2015) -- End
            'Hemant (26 Dec 2018) -- Start
            'Enhancement - On employee cost centre, provide a global option to assign all employees, on a single transaction head to one cost centre. 
            'Also on this screen, provide an analysis by option where user can filter out employees in 76.1.
            'Call FillCombo()
            'Hemant (26 Dec 2018) -- End

            If menAction = enAction.EDIT_ONE Then
                'Sohail (21 Aug 2015) -- Start
                'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                'objEmployeeCostcenter._Costcentertranunkid = mintEmpcostcenterUnkid
                objEmployeeCostcenter._Costcentertranunkid(FinancialYear._Object._DatabaseName) = mintEmpcostcenterUnkid
                'Sohail (21 Aug 2015) -- End
                'Hemant (11 Mar 2018) -- Start
                'ISSUE/ENHANCEMENT : need filter to show only newly hired, rehired employees and those not assigned, same as on global assign
                chkShowReinstatedEmployees.Enabled = False
                chkShowNewlyHiredEmployees.Enabled = False
                chkNotAssignedHead.Enabled = False
                'Hemant (11 Mar 2018) -- End
            End If
            'Hemant (26 Dec 2018) -- Start
            'Enhancement - On employee cost centre, provide a global option to assign all employees, on a single transaction head to one cost centre. 
            'Also on this screen, provide an analysis by option where user can filter out employees in 76.1.
            Call FillCombo()
            'Hemant (26 Dec 2018) -- End
            Call GetValue()

            If menAction = enAction.EDIT_ONE Then
                cboEmployee.Enabled = False
                cboTranHead.Enabled = False
                objbtnSearchEmployee.Enabled = False
                objSearchED.Enabled = False
                cboPeriod.Enabled = False 'Sohail (29 Mar 2017)
            End If

            'Sohail (29 Mar 2017) -- Start
            'CCBRT Enhancement - 66.1 - Employee cost center distribution for non-salary heads, Effective period option on Employee Cost Center.
            If mintEmployeeID > 0 Then cboEmployee.SelectedValue = mintEmployeeID
            If mintTranHeadID > 0 Then cboTranHead.SelectedValue = mintTranHeadID
            If mintCostCenterID > 0 Then cboCostcenter.SelectedValue = mintCostCenterID
            'Sohail (29 Mar 2017) -- End

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmEmpDistributedCostCenter_AddEdit_Load", mstrModuleName)
        End Try
    End Sub

    'Sohail (10 Jul 2014) -- Start
    'Enhancement - Custom Language.
    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsemployee_costcenter_Tran.SetMessages()
            objfrm._Other_ModuleNames = "clsemployee_costcenter_Tran"
            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub
    'Sohail (10 Jul 2014) -- End

#End Region

#Region " Combo Box's Events "
    Private Sub cboEmployee_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboEmployee.SelectedIndexChanged
        Dim objEmployee As New clsEmployee_Master
        Dim objCC As New clscostcenter_master
        Dim objED As New clsEarningDeduction
        Dim dsList As DataSet
        Dim dtTable As DataTable
        Try

            lblDefCC.Text = ""
            If CInt(cboEmployee.SelectedValue) > 0 Then

                'S.SANDEEP [04 JUN 2015] -- START
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                'objEmployee._Employeeunkid = CInt(cboEmployee.SelectedValue)
                objEmployee._Employeeunkid(eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate)) = CInt(cboEmployee.SelectedValue)
                'S.SANDEEP [04 JUN 2015] -- END

                objCC._Costcenterunkid = objEmployee._Costcenterunkid
                lblDefCC.Text = Language.getMessage(mstrModuleName, 10, "Default Cost Center :") & " " & objCC._Costcentername
            End If
            'Sohail (29 Mar 2017) -- Start
            'CCBRT Enhancement - 66.1 - Employee cost center distribution for non-salary heads, Effective period option on Employee Cost Center.
            'dsList = objED.getListForCombo_TransactionHead("TranHead", CInt(cboEmployee.SelectedValue))
            'dtTable = New DataView(dsList.Tables("TranHead"), "tranheadunkid IN (" & mstrSalaryHeadIDs & ")", "", DataViewRowState.CurrentRows).ToTable
            dsList = objED.getListForCombo_TransactionHead("TranHead", CInt(cboEmployee.SelectedValue), mdtPeriod_enddate)
            dtTable = New DataView(dsList.Tables("TranHead"), "", "", DataViewRowState.CurrentRows).ToTable
            'Sohail (29 Mar 2017) -- End
            With cboTranHead
                .ValueMember = "tranheadunkid"
                .DisplayMember = "trnheadname"
                .DataSource = dtTable
                .SelectedValue = 0
            End With


            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'objEmployeeCostcenter._Employeeunkid = CInt(cboEmployee.SelectedValue)
            'Sohail (29 Mar 2017) -- Start
            'CCBRT Enhancement - 66.1 - Employee cost center distribution for non-salary heads, Effective period option on Employee Cost Center.
            'objEmployeeCostcenter.GetEmployeeCostCenter_Tran(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), ConfigParameter._Object._UserAccessModeSetting, True, ConfigParameter._Object._IsIncludeInactiveEmp, CInt(cboEmployee.SelectedValue), "List", True, "")
            'mdtTran = New DataView(objEmployeeCostcenter._DataTable, "tranheadunkid IN (" & mstrSalaryHeadIDs & ")", "", DataViewRowState.CurrentRows).ToTable
            'Call FillList()
            'Sohail (21 Aug 2015) -- End
            'Sohail (29 Mar 2017) -- End

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboEmployee_SelectedIndexChanged", mstrModuleName)
        Finally
            objEmployee = Nothing
            objCC = Nothing
            objED = Nothing
        End Try
    End Sub

    'Sohail (29 Mar 2017) -- Start
    'CCBRT Enhancement - 66.1 - Employee cost center distribution for non-salary heads, Effective period option on Employee Cost Center.
    Private Sub cboPeriod_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboPeriod.SelectedIndexChanged
        Dim objPeriod As New clscommom_period_Tran
        Try
            If CInt(cboPeriod.SelectedValue) > 0 Then
                objPeriod._Periodunkid(FinancialYear._Object._DatabaseName) = CInt(cboPeriod.SelectedValue)
                'Sohail (21 Aug 2015) -- End
                mdtPeriod_startdate = objPeriod._Start_Date
                mdtPeriod_enddate = objPeriod._End_Date
                mintPeriod_Status = objPeriod._Statusid
                Call FillCombo_Employee()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboPeriod_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub cboTranHead_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboTranHead.SelectedIndexChanged
        Try
            'Hemant (26 Dec 2018) -- Start
            'Enhancement - On employee cost centre, provide a global option to assign all employees, on a single transaction head to one cost centre. 
            'Also on this screen, provide an analysis by option where user can filter out employees in 76.1.
            'objEmployeeCostcenter.GetEmployeeCostCenter_Tran(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, mdtPeriod_startdate, mdtPeriod_enddate, ConfigParameter._Object._UserAccessModeSetting, True, ConfigParameter._Object._IsIncludeInactiveEmp, CInt(cboEmployee.SelectedValue), CInt(cboTranHead.SelectedValue), "List", True, "")
            If CInt(cboTranHead.SelectedValue) > 0 Then
                Dim strEmpUnkIdList = String.Join(",", (From p In dvEmployee.Table Where (CBool(p.Item("IsChecked")) = True) Select (p.Item("Employeeunkid").ToString)).ToArray)
                objEmployeeCostcenter.GetEmployeeCostCenter_Tran(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, mdtPeriod_startdate, mdtPeriod_enddate, ConfigParameter._Object._UserAccessModeSetting, True, ConfigParameter._Object._IsIncludeInactiveEmp, objEmployeeCostcenter._Employeeunkid, CInt(cboTranHead.SelectedValue), "List", True, "", strEmpUnkIdList)
            End If
            'Hemant (26 Dec 2018) -- End
            mdtTran = New DataView(objEmployeeCostcenter._DataTable, "", "", DataViewRowState.CurrentRows).ToTable
            Call FillList()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboTranHead_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub
    'Sohail (29 Mar 2017) -- End

    'Sohail (07 Feb 2019) -- Start
    'NMB Enhancement - 76.1 - Employee cost centre should be distributed on all allocations instead of only on cost centre.
    Private Sub cboAllocation_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboAllocation.SelectedIndexChanged
        Dim objStation As New clsStation
        Dim objDeptGrp As New clsDepartmentGroup
        Dim objDepartment As New clsDepartment
        Dim objSection As New clsSections
        Dim objUnit As New clsUnits
        Dim objJobGrp As New clsJobGroup
        Dim objJob As New clsJobs
        Dim objClassGrp As New clsClassGroup
        Dim objClass As New clsClass
        Dim objSectionGrp As New clsSectionGroup
        Dim objUnitGroup As New clsUnitGroup
        Dim objTeam As New clsTeams
        Dim objCCentre As New clscostcenter_master
        Dim dsCombos As DataSet = Nothing

        Try

            Select Case CInt(cboAllocation.SelectedValue)

                Case enAllocation.BRANCH
                    dsCombos = objStation.getComboList("Station", True)
                    dsCombos.Tables(0).Columns(0).ColumnName = "Id"
                Case enAllocation.DEPARTMENT_GROUP
                    dsCombos = objDeptGrp.getComboList("DeptGrp", True)
                    dsCombos.Tables(0).Columns(0).ColumnName = "Id"
                Case enAllocation.DEPARTMENT
                    dsCombos = objDepartment.getComboList("Department", True)
                    dsCombos.Tables(0).Columns(0).ColumnName = "Id"
                Case enAllocation.SECTION_GROUP
                    dsCombos = objSectionGrp.getComboList("List", True)
                    dsCombos.Tables(0).Columns(0).ColumnName = "Id"
                Case enAllocation.SECTION
                    dsCombos = objSection.getComboList("Section", True)
                    dsCombos.Tables(0).Columns(0).ColumnName = "Id"
                Case enAllocation.UNIT_GROUP
                    dsCombos = objUnitGroup.getComboList("List", True)
                    dsCombos.Tables(0).Columns(0).ColumnName = "Id"
                Case enAllocation.UNIT
                    dsCombos = objUnit.getComboList("Unit", True)
                    dsCombos.Tables(0).Columns(0).ColumnName = "Id"
                Case enAllocation.TEAM
                    dsCombos = objTeam.getComboList("List", True)
                    dsCombos.Tables(0).Columns(0).ColumnName = "Id"
                Case enAllocation.JOB_GROUP
                    dsCombos = objJobGrp.getComboList("JobGrp", True)
                    dsCombos.Tables(0).Columns(0).ColumnName = "Id"
                Case enAllocation.JOBS
                    dsCombos = objJob.getComboList("Job", True)
                    dsCombos.Tables(0).Columns(0).ColumnName = "Id"
                Case enAllocation.CLASS_GROUP
                    dsCombos = objClassGrp.getComboList("ClassGrp", True)
                    dsCombos.Tables(0).Columns(0).ColumnName = "Id"
                Case enAllocation.CLASSES
                    dsCombos = objClass.getComboList("Class", True)
                    dsCombos.Tables(0).Columns(0).ColumnName = "Id"
                Case enAllocation.COST_CENTER
                    dsCombos = objCCentre.getComboList("CCentre", True)
                    dsCombos.Tables(0).Columns(0).ColumnName = "Id"
                    dsCombos.Tables(0).Columns(2).ColumnName = "Name"

            End Select

            lblCostCenter.Text = cboAllocation.Text
            If dsCombos IsNot Nothing Then

                With cboCostcenter
                    .ValueMember = "Id"
                    .DisplayMember = "Name"
                    .DataSource = dsCombos.Tables(0)
                    .SelectedValue = 0

                    'Hemant (11 Mar 2018) -- Start
                    'ISSUE : performance issue on emp cost centre list screen, taking 30 to 45 mins to open
                    Call SetDefaultSearchText(cboCostcenter)
                    'Hemant (11 Mar 2018) -- End
                End With
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboAllocation_SelectedIndexChanged", mstrModuleName)
        Finally
            objStation = Nothing
            objDeptGrp = Nothing
            objDepartment = Nothing
            objSection = Nothing
            objUnit = Nothing
            objJobGrp = Nothing
            objJob = Nothing
            objClassGrp = Nothing
            objClass = Nothing
            objSectionGrp = Nothing
            objUnitGroup = Nothing
            objTeam = Nothing
            objCCentre = Nothing
            dsCombos = Nothing
        End Try
    End Sub
    'Sohail (07 Feb 2019) -- End

    'Hemant (11 Mar 2018) -- Start
    'ISSUE : performance issue on emp cost centre list screen, taking 30 to 45 mins to open

    Private Sub cboCostcenter_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles cboCostcenter.KeyPress, cboTranHead.KeyPress
        Dim cbo As ComboBox = CType(sender, ComboBox)

        Try

            If (CInt(AscW(e.KeyChar)) >= 65 AndAlso CInt(AscW(e.KeyChar)) <= 90) Or (CInt(AscW(e.KeyChar)) >= 97 AndAlso CInt(AscW(e.KeyChar)) <= 122) Or (CInt(AscW(e.KeyChar)) >= 47 AndAlso CInt(AscW(e.KeyChar)) <= 57) Then
                Dim frm As New frmCommonSearch
                With frm
                    .ValueMember = cbo.ValueMember
                    .DisplayMember = cbo.DisplayMember
                    .DataSource = CType(cbo.DataSource, DataTable)
                    If CInt(cboAllocation.SelectedValue) = enAllocation.COST_CENTER AndAlso cbo.Name = cboCostcenter.Name Then
                        .CodeMember = "costcentercode"
                    ElseIf (CInt(cboAllocation.SelectedValue) = enAllocation.CLASS_GROUP OrElse CInt(cboAllocation.SelectedValue) = enAllocation.CLASSES) AndAlso cbo.Name = cboCostcenter.Name Then
                        .CodeMember = "code"
                    ElseIf cbo.Name = cboTranHead.Name Then
                        .CodeMember = "code"
                    Else

                    End If

                End With
                Dim c As Char = Convert.ToChar(e.KeyChar)
                frm.TypedText = c.ToString
                If frm.DisplayDialog Then
                    cbo.SelectedValue = frm.SelectedValue
                    e.KeyChar = ChrW(Keys.ShiftKey)
                Else
                    cbo.Text = ""
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, cbo.Name & "_KeyPress", mstrModuleName)
        End Try
    End Sub

    Private Sub cboCostcenter_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboCostcenter.SelectedIndexChanged, cboTranHead.SelectedIndexChanged
        Dim cbo As ComboBox = CType(sender, ComboBox)

        Try

            If CInt(cbo.SelectedValue) < 0 Then
                Call SetDefaultSearchText(cbo)
            Else
                Call SetRegularFont(cbo)
            End If


        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, cbo.Name & "_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub cboCostcenter_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboCostcenter.GotFocus, cboTranHead.GotFocus
        Dim cbo As ComboBox = CType(sender, ComboBox)

        Try

            With cbo
                .ForeColor = Color.Black
                .Font = New Font(Me.Font.FontFamily, Me.Font.Size, FontStyle.Regular)

                If .Text = mstrSearchText Then
                    .Text = ""
                End If
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, cbo.Name & "_GotFocus", mstrModuleName)
        End Try
    End Sub

    Private Sub cboCostcenter_Leave(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboCostcenter.Leave, cboTranHead.Leave
        Dim cbo As ComboBox = CType(sender, ComboBox)

        Try

            If CInt(cbo.SelectedValue) <= 0 Then
                Call SetDefaultSearchText(cbo)
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, cbo.Name & "_Leave", mstrModuleName)
        End Try
    End Sub
    'Hemant (11 Mar 2018) -- End
#End Region

#Region " Button's Events "
    Private Sub btnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnClose_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAdd.Click
        Try
            If IsValid() = False Then Exit Sub

            'Hemant (26 Dec 2018) -- Start
            'Enhancement - On employee cost centre, provide a global option to assign all employees, on a single transaction head to one cost centre. 
            'Also on this screen, provide an analysis by option where user can filter out employees in 76.1.           
            ''Sohail (29 Mar 2017) -- Start
            ''CCBRT Enhancement - 66.1 - Employee cost center distribution for non-salary heads, Effective period option on Employee Cost Center.
            ''Dim dtRow As DataRow() = mdtTran.Select("costcenterunkid = '" & CInt(cboCostcenter.SelectedValue) & "' AND (AUD <> 'D' OR AUD IS NULL) ")
            'Dim dtRow As DataRow() = mdtTran.Select("costcenterunkid = '" & CInt(cboCostcenter.SelectedValue) & "' AND periodunkid = " & CInt(cboPeriod.SelectedValue) & " AND (AUD <> 'D' OR AUD IS NULL) ")
            ''Sohail (29 Mar 2017) -- End

            'If dtRow.Length > 0 Then
            '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 6, "Selected Cost Center is already added to the list."), enMsgBoxStyle.Information)
            '    Exit Sub
            'End If
            
            For Each dsRow As DataRow In dvEmployee.Table.Select("IsChecked = 1 ")
                'If chkIsExist.Checked = True Then
                '    For Each drUpdateRow As DataRow In mdtTran.Select("employeeunkid = '" & CInt(dsRow.Item("employeeunkid")) & "' AND tranheadunkid = '" & CInt(cboTranHead.SelectedValue) & "' AND periodunkid = " & CInt(cboPeriod.SelectedValue) & " AND  AUD = '' ")
                '        drUpdateRow.Item("AUD") = "D"
                '        mdtTran.AcceptChanges()
                '    Next

                'End If

                'Dim drCCRow As DataRow() = mdtTran.Select("employeeunkid = '" & CInt(dsRow.Item("employeeunkid")) & "' AND tranheadunkid = '" & CInt(cboTranHead.SelectedValue) & "' AND periodunkid = " & CInt(cboPeriod.SelectedValue) & " AND  AUD <> 'U' AND AUD <> 'A'  ")
                Dim drHeadRow As DataRow() = mdtTran.Select("employeeunkid = '" & CInt(dsRow.Item("employeeunkid")) & "' AND tranheadunkid = '" & CInt(cboTranHead.SelectedValue) & "' AND periodunkid = " & CInt(cboPeriod.SelectedValue) & " AND (AUD <> 'D' OR AUD IS NULL) ")

                'Sohail (07 Feb 2019) -- Start
                'NMB Enhancement - 76.1 - Employee cost centre should be distributed on all allocations instead of only on cost centre.
                'If drHeadRow.Length > 0 Then
                Dim intEmpId As Integer = CInt(dsRow.Item("employeeunkid"))
                Dim decTotPerc As Decimal = (From p In mdtTran Where (CInt(p.Item("employeeunkid")) = intEmpId AndAlso CInt(p.Item("tranheadunkid")) = CInt(cboTranHead.SelectedValue) AndAlso CInt(p.Item("periodunkid")) = CInt(cboPeriod.SelectedValue) AndAlso (IsDBNull(p.Item("AUD")) = True OrElse p.Item("AUD").ToString <> "D")) Select (CDec(p.Item("percentage")))).Sum

                If decTotPerc >= 100 Then
                    'Sohail (07 Feb 2019) -- End

                    If chkIsExist.Checked = True Then
                        'Sohail (07 Feb 2019) -- Start
                        'NMB Enhancement - 76.1 - Employee cost centre should be distributed on all allocations instead of only on cost centre.
                        'Dim drCCRow As DataRow() = mdtTran.Select("employeeunkid = '" & CInt(dsRow.Item("employeeunkid")) & "' AND tranheadunkid = '" & CInt(cboTranHead.SelectedValue) & "' AND periodunkid = " & CInt(cboPeriod.SelectedValue) & " AND costcenterunkid = " & CInt(cboCostcenter.SelectedValue) & " AND (AUD <> 'D' OR AUD IS NULL) ")
                        Dim drCCRow As DataRow() = mdtTran.Select("employeeunkid = '" & CInt(dsRow.Item("employeeunkid")) & "' AND tranheadunkid = '" & CInt(cboTranHead.SelectedValue) & "' AND periodunkid = " & CInt(cboPeriod.SelectedValue) & " AND costcenterunkid = " & CInt(cboCostcenter.SelectedValue) & " AND allocationbyid = " & CInt(cboAllocation.SelectedValue) & " AND (AUD <> 'D' OR AUD IS NULL) ")
                        'Sohail (07 Feb 2019) -- End

                        If drCCRow.Length > 0 Then
                            For Each dr As DataRow In drHeadRow
                                If CInt(dr.Item("costcenterunkid")) = CInt(cboCostcenter.SelectedValue) Then
                                    dr.Item("percentage") = Format(txtPercentage.Decimal, "00.00")
                                    If dr.Item("AUD").ToString <> "A" Then drCCRow(0).Item("AUD") = "U"
                                ElseIf dr.Item("AUD").ToString = "" Then
                                    dr.Item("AUD") = "D"
                                End If
                                mdtTran.AcceptChanges()
                            Next
                        Else
                            For Each dr As DataRow In drHeadRow
                                If dr.Item("AUD").ToString = "" Then
                                    dr.Item("AUD") = "D"
                                    mdtTran.AcceptChanges()
            End If
                            Next

                            Dim dtEBRow As DataRow
                            dtEBRow = mdtTran.NewRow

                            dtEBRow.Item("costcentertranunkid") = -1
                            dtEBRow.Item("employeeunkid") = CInt(dsRow.Item("employeeunkid"))
                            dtEBRow.Item("employeename") = dsRow.Item("employeename")
                            dtEBRow.Item("tranheadunkid") = CInt(cboTranHead.SelectedValue)
                            dtEBRow.Item("trnheadname") = cboTranHead.Text
                            'Sohail (07 Feb 2019) -- Start
                            'NMB Enhancement - 76.1 - Employee cost centre should be distributed on all allocations instead of only on cost centre.
                            dtEBRow.Item("allocationbyid") = CInt(cboAllocation.SelectedValue)
                            dtEBRow.Item("allocationbyname") = cboAllocation.Text
                            'Sohail (07 Feb 2019) -- End
                            dtEBRow.Item("costcenterunkid") = CInt(cboCostcenter.SelectedValue)
                            dtEBRow.Item("costcentername") = cboCostcenter.Text
                            dtEBRow.Item("percentage") = Format(txtPercentage.Decimal, "00.00")
                            dtEBRow.Item("periodunkid") = CInt(cboPeriod.SelectedValue)
                            dtEBRow.Item("period_name") = cboPeriod.Text
                            dtEBRow.Item("end_date") = eZeeDate.convertDate(mdtPeriod_enddate)

                            dtEBRow.Item("AUD") = "A"
                            dtEBRow.Item("GUID") = Guid.NewGuid().ToString

                            mdtTran.Rows.Add(dtEBRow)
                        End If
                    End If
                Else
                    'Hemant (26 Dec 2018) -- End

                    'Sohail (07 Feb 2019) -- Start
                    'NMB Enhancement - 76.1 - Employee cost centre should be distributed on all allocations instead of only on cost centre.
                    Dim drCCRow As DataRow() = mdtTran.Select("employeeunkid = '" & CInt(dsRow.Item("employeeunkid")) & "' AND tranheadunkid = '" & CInt(cboTranHead.SelectedValue) & "' AND periodunkid = " & CInt(cboPeriod.SelectedValue) & " AND costcenterunkid = " & CInt(cboCostcenter.SelectedValue) & " AND allocationbyid = " & CInt(cboAllocation.SelectedValue) & " AND (AUD <> 'D' OR AUD IS NULL) ")

                    If drCCRow.Length > 0 Then
                        If chkIsExist.Checked = True Then
                            With drCCRow(0)
                                .Item("percentage") = Format(txtPercentage.Decimal, "00.00")
                                If .Item("AUD").ToString <> "A" Then drCCRow(0).Item("AUD") = "U"
                            End With
                            mdtTran.AcceptChanges()
                        End If
                    Else
                        'Sohail (07 Feb 2019) -- End

            Dim dtEBRow As DataRow
            dtEBRow = mdtTran.NewRow

            dtEBRow.Item("costcentertranunkid") = -1
                    'Hemant (26 Dec 2018) -- Start
                    'Enhancement - On employee cost centre, provide a global option to assign all employees, on a single transaction head to one cost centre. 
                    ''Also on this screen, provide an analysis by option where user can filter out employees in 76.1.
                    'dtEBRow.Item("employeeunkid") = CInt(cboEmployee.SelectedValue)
                    'dtEBRow.Item("employeename") = cboEmployee.Text
                    dtEBRow.Item("employeeunkid") = CInt(dsRow.Item("employeeunkid"))
                    dtEBRow.Item("employeename") = dsRow.Item("employeename")
                    'Hemant (26 Dec 2018) -- End
            dtEBRow.Item("tranheadunkid") = CInt(cboTranHead.SelectedValue)
            dtEBRow.Item("trnheadname") = cboTranHead.Text
                        'Sohail (07 Feb 2019) -- Start
                        'NMB Enhancement - 76.1 - Employee cost centre should be distributed on all allocations instead of only on cost centre.
                        dtEBRow.Item("allocationbyid") = CInt(cboAllocation.SelectedValue)
                        dtEBRow.Item("allocationbyname") = cboAllocation.Text
                        'Sohail (07 Feb 2019) -- End
            dtEBRow.Item("costcenterunkid") = CInt(cboCostcenter.SelectedValue)
            dtEBRow.Item("costcentername") = cboCostcenter.Text
            dtEBRow.Item("percentage") = Format(txtPercentage.Decimal, "00.00")

            'Sohail (29 Mar 2017) -- Start
            'CCBRT Enhancement - 66.1 - Employee cost center distribution for non-salary heads, Effective period option on Employee Cost Center.
            dtEBRow.Item("periodunkid") = CInt(cboPeriod.SelectedValue)
            dtEBRow.Item("period_name") = cboPeriod.Text
            dtEBRow.Item("end_date") = eZeeDate.convertDate(mdtPeriod_enddate)
            'Sohail (29 Mar 2017) -- End

            dtEBRow.Item("AUD") = "A"
            dtEBRow.Item("GUID") = Guid.NewGuid().ToString

            mdtTran.Rows.Add(dtEBRow)
                    End If 'Sohail (07 Feb 2019)
                    'Hemant (26 Dec 2018) -- Start
                    'Enhancement - On employee cost centre, provide a global option to assign all employees, on a single transaction head to one cost centre. 
                    'Also on this screen, provide an analysis by option where user can filter out employees in 76.1.        
                End If
            Next
            'Hemant (26 Dec 2018) -- End

            mdtTran = New DataView(mdtTran, "", "end_date DESC, employeename", DataViewRowState.CurrentRows).ToTable 'Sohail (29 Mar 2017)

            Call FillList()

            If mdtTran.Rows.Count > 0 Then
                cboEmployee.Enabled = False
                cboTranHead.Enabled = False
                objbtnSearchEmployee.Enabled = False
                objSearchED.Enabled = False
                cboPeriod.Enabled = False 'Sohail (29 Mar 2017)
            Else
                cboEmployee.Enabled = True
                objbtnSearchEmployee.Enabled = True
                cboTranHead.Enabled = True
                objSearchED.Enabled = True
                cboPeriod.Enabled = True 'Sohail (29 Mar 2017)
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnAdd_Click", mstrModuleName)
        End Try

    End Sub

    Private Sub btnEdit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnEdit.Click
        Try
            'Hemant (11 Mar 2018) -- Start
            'ISSUE : performance issue on emp cost centre list screen, taking 30 to 45 mins to open
            'If lvEmpCostCenter.SelectedItems.Count > 0 Then
            If dgvEmpCostCenter.SelectedRows.Count > 0 Then
                'Hemant (11 Mar 2018) -- End
                If mintItemIndex > -1 Then
                    If IsValid() = False Then Exit Sub 'Sohail (29 Mar 2017) 

                    Dim drTemp As DataRow()
                    'Hemant (11 Mar 2018) -- Start
                    'ISSUE : performance issue on emp cost centre list screen, taking 30 to 45 mins to open
                    'If CInt(lvEmpCostCenter.Items(mintItemIndex).Tag) = -1 Then
                    'drTemp = mdtTran.Select("GUID = '" & lvEmpCostCenter.Items(mintItemIndex).SubItems(colhGUID.Index).Text & "'")  
                    If CInt(dgvEmpCostCenter.Rows(mintItemIndex).Cells(0).Value) = -1 Then
                        drTemp = mdtTran.Select("GUID = '" & dgvEmpCostCenter.Rows(mintItemIndex).Cells(objdgColhGUID.Index).Value.ToString & "'")
                        'Hemant (11 Mar 2018) -- End
                    Else
                        'Sohail (29 Mar 2017) -- Start
                        'CCBRT Enhancement - 66.1 - Employee cost center distribution for non-salary heads, Effective period option on Employee Cost Center.
                        'drTemp = mdtTran.Select("costcentertranunkid = " & CInt(lvEmpCostCenter.Items(mintItemIndex).Tag))
                        'Hemant (11 Mar 2018) -- Start
                        'ISSUE : performance issue on emp cost centre list screen, taking 30 to 45 mins to open
                        'drTemp = mdtTran.Select("costcentertranunkid = " & CInt(lvEmpCostCenter.Items(mintItemIndex).Tag) & " AND periodunkid = " & CInt(cboPeriod.SelectedValue) & " ")
                        drTemp = mdtTran.Select("costcentertranunkid = " & CInt(dgvEmpCostCenter.Rows(mintItemIndex).Cells(0).Value) & " AND periodunkid = " & CInt(cboPeriod.SelectedValue) & " ")
                        'Hemant (11 Mar 2018) -- End
                        'Sohail (29 Mar 2017) -- End
                    End If
                    If drTemp.Length > 0 Then
                        'If IsValid() = False Then Exit Sub 'Sohail (29 Mar 2017)
                        With drTemp(0)
                            'Hemant (11 Mar 2018) -- Start
                            'ISSUE : performance issue on emp cost centre list screen, taking 30 to 45 mins to open
                            '.Item("costcentertranunkid") = CInt(lvEmpCostCenter.Items(mintItemIndex).Tag)
                            .Item("costcentertranunkid") = CInt(dgvEmpCostCenter.Rows(mintItemIndex).Cells(0).Value)
                            'Hemant (11 Mar 2018) -- End
                            'Hemant (26 Dec 2018) -- Start
                            'Enhancement - On employee cost centre, provide a global option to assign all employees, on a single transaction head to one cost centre. 
                            'Also on this screen, provide an analysis by option where user can filter out employees in 76.1.
                            '.Item("employeeunkid") = CInt(cboEmployee.SelectedValue)
                            '.Item("employeename") = cboEmployee.Text
                            'Hemant (26 Dec 2018) -- End
                            .Item("tranheadunkid") = CInt(cboTranHead.SelectedValue)
                            .Item("trnheadname") = cboTranHead.Text
                            'Sohail (07 Feb 2019) -- Start
                            'NMB Enhancement - 76.1 - Employee cost centre should be distributed on all allocations instead of only on cost centre.
                            .Item("allocationbyid") = CInt(cboAllocation.SelectedValue)
                            .Item("allocationbyname") = cboAllocation.Text
                            'Sohail (07 Feb 2019) -- End
                            .Item("costcenterunkid") = CInt(cboCostcenter.SelectedValue)
                            .Item("costcentername") = cboCostcenter.Text
                            .Item("percentage") = Format(txtPercentage.Decimal, "00.00")
                            'Sohail (29 Mar 2017) -- Start
                            'CCBRT Enhancement - 66.1 - Employee cost center distribution for non-salary heads, Effective period option on Employee Cost Center.
                            .Item("periodunkid") = CInt(cboPeriod.SelectedValue)
                            .Item("period_name") = cboPeriod.Text
                            .Item("end_date") = eZeeDate.convertDate(mdtPeriod_enddate)
                            'Sohail (29 Mar 2017) -- End

                            If IsDBNull(.Item("AUD")) Or CStr(.Item("AUD")).ToString.Trim = "" Then
                                .Item("AUD") = "U"
                            End If
                            .Item("GUID") = Guid.NewGuid().ToString
                            .AcceptChanges()
                        End With
                        Call FillList()
                    End If
                End If
                If mdtTran.Rows.Count > 0 Then
                    cboEmployee.Enabled = False
                    cboTranHead.Enabled = False
                    objbtnSearchEmployee.Enabled = False
                    objSearchED.Enabled = False
                    cboPeriod.Enabled = False 'Sohail (29 Mar 2017)
                Else
                    cboEmployee.Enabled = True
                    objbtnSearchEmployee.Enabled = True
                    cboTranHead.Enabled = True
                    objSearchED.Enabled = True
                    cboPeriod.Enabled = True 'Sohail (29 Mar 2017)
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnEdit_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDelete.Click
        Try
            'Hemant (11 Mar 2018) -- Start
            'ISSUE : performance issue on emp cost centre list screen, taking 30 to 45 mins to open
            'If lvEmpCostCenter.SelectedItems.Count > 0 Then
            If dgvEmpCostCenter.SelectedRows.Count > 0 Then
                'Hemant (11 Mar 2018) -- End
                If mintItemIndex > -1 Then
                    If IsValid() = False Then Exit Sub 'Sohail (29 Mar 2017)

                    Dim drTemp As DataRow()
                    'Hemant (11 Mar 2018) -- Start
                    'ISSUE : performance issue on emp cost centre list screen, taking 30 to 45 mins to open
                    'If CInt(lvEmpCostCenter.Items(mintItemIndex).Tag) = -1 Then
                    'drTemp = mdtTran.Select("GUID = '" & lvEmpCostCenter.Items(mintItemIndex).SubItems(colhGUID.Index).Text & "'")
                    If CInt(dgvEmpCostCenter.Rows(mintItemIndex).Cells(0).Value) = -1 Then
                        drTemp = mdtTran.Select("GUID = '" & dgvEmpCostCenter.Rows(mintItemIndex).Cells(objdgColhGUID.Index).Value.ToString & "'")
                        'Hemant (11 Mar 2018) -- End
                    Else
                        'Hemant (11 Mar 2018) -- Start
                        'ISSUE : performance issue on emp cost centre list screen, taking 30 to 45 mins to open
                        'If objEmployeeCostcenter.isUsed(CInt(lvEmpCostCenter.Items(mintItemIndex).Tag)) = True Then
                        If objEmployeeCostcenter.isUsed(CInt(dgvEmpCostCenter.Rows(mintItemIndex).Cells(0).Value)) = True Then
                            'Hemant (11 Mar 2018) -- End
                            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 7, "Sorry, You can not delete this Employee Cost Center. This Employee Cost Center is in used."), enMsgBoxStyle.Information)
                            Exit Sub
                        End If
                        'Sohail (29 Mar 2017) -- Start
                        'CCBRT Enhancement - 66.1 - Employee cost center distribution for non-salary heads, Effective period option on Employee Cost Center.
                        'drTemp = mdtTran.Select("costcentertranunkid = '" & CInt(lvEmpCostCenter.Items(mintItemIndex).Tag) & "'")
                        'Hemant (11 Mar 2018) -- Start
                        'ISSUE : performance issue on emp cost centre list screen, taking 30 to 45 mins to open
                        'drTemp = mdtTran.Select("costcentertranunkid = " & CInt(lvEmpCostCenter.Items(mintItemIndex).Tag) & " AND periodunkid = " & CInt(cboPeriod.SelectedValue) & " ")
                        drTemp = mdtTran.Select("costcentertranunkid = " & CInt(dgvEmpCostCenter.Rows(mintItemIndex).Cells(0).Value) & " AND periodunkid = " & CInt(cboPeriod.SelectedValue) & " ")
                        'Hemant (11 Mar 2018) -- End
                        'Sohail (29 Mar 2017) -- End
                    End If
                    If drTemp.Length > 0 Then
                        drTemp(0).Item("AUD") = "D"
                        mdblTotPerc -= CDec(drTemp(0).Item("percentage"))
                        Call FillList()
                    End If

                    If mdtTran.Rows.Count > 0 Then
                        cboEmployee.Enabled = False
                        cboTranHead.Enabled = False
                        objbtnSearchEmployee.Enabled = False
                        objSearchED.Enabled = False
                        cboPeriod.Enabled = False 'Sohail (29 Mar 2017)
                    Else
                        cboEmployee.Enabled = True
                        objbtnSearchEmployee.Enabled = True
                        cboTranHead.Enabled = True
                        objSearchED.Enabled = True
                        cboPeriod.Enabled = True 'Sohail (29 Mar 2017)
                    End If
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnDelete_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Dim blnFlag As Boolean = False
        Try

            'Sohail (29 Mar 2017) -- Start
            'CCBRT Enhancement - 66.1 - Employee cost center distribution for non-salary heads, Effective period option on Employee Cost Center.
            If cboPeriod.SelectedValue Is Nothing Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 11, "Sorry, This Period is closed."), enMsgBoxStyle.Information)
                Exit Sub
            End If
            'Sohail (29 Mar 2017) -- End

            If mdtTran.Rows.Count <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 8, "Please add atleast one employee Cost Center."), enMsgBoxStyle.Information)
                Exit Sub
                'Sohail (29 Mar 2017) -- Start
                'CCBRT Enhancement - 66.1 - Employee cost center distribution for non-salary heads, Effective period option on Employee Cost Center.
                'ElseIf lvEmpCostCenter.Items.Count > 0 AndAlso mdblTotPerc <> 100 Then
                '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 9, "Sorry, Total Percentage must be 100."), enMsgBoxStyle.Information)
                '    Exit Sub
                'Hemant (11 Mar 2018) -- Start
                'ISSUE : performance issue on emp cost centre list screen, taking 30 to 45 mins to open
                'ElseIf lvEmpCostCenter.Items.Count > 0 Then
            ElseIf dgvEmpCostCenter.Rows.Count > 0 Then
                'Hemant (11 Mar 2018) -- End
                'Hemant (26 Dec 2018) -- Start
                'Enhancement - On employee cost centre, provide a global option to assign all employees, on a single transaction head to one cost centre. 
                'Also on this screen, provide an analysis by option where user can filter out employees in 76.1.
                'Dim dec = (From p In mdtTran.AsEnumerable Where p.Item("AUD").ToString <> "D" Group p By G = New With {Key .enddate = p.Item("end_date").ToString, Key .employeeunkid = CInt(p.Item("employeeunkid").ToString), Key .tranheadunkid = CInt(p.Item("tranheadunkid").ToString)} _
                '                   Into grp = Group Select New With {Key .end_date = grp(0).Item("end_date").ToString, Key .employeeunkid = grp(0).Item("employeeunkid").ToString, Key .tranheadunkid = grp(0).Item("tranheadunkid").ToString, Key .amount = grp.Sum(Function(x) CDec(x.Item("percentage")))})
                Dim dec = (From p In mdtTran.AsEnumerable Where p.Item("AUD").ToString <> "D" Group p By G = New With {Key .enddate = p.Item("end_date").ToString, Key .employeeunkid = CInt(p.Item("employeeunkid").ToString), Key .tranheadunkid = CInt(p.Item("tranheadunkid").ToString), Key .employeename = p.Item("employeename").ToString} _
                   Into grp = Group Select New With {Key .end_date = grp(0).Item("end_date").ToString, Key .employeeunkid = grp(0).Item("employeeunkid").ToString, Key .tranheadunkid = grp(0).Item("tranheadunkid").ToString, Key .employeename = grp(0).Item("employeename").ToString, Key .amount = grp.Sum(Function(x) CDec(x.Item("percentage")))})
                'Hemant (26 Dec 2018) -- End

                For Each itm In dec
                    If itm.amount <> 100 Then

                        'Hemant (26 Dec 2018) -- Start
                        'Enhancement - On employee cost centre, provide a global option to assign all employees, on a single transaction head to one cost centre. 
                        'Also on this screen, provide an analysis by option where user can filter out employees in 76.1.
                        'eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 9, "Sorry, Total Percentage must be 100."), enMsgBoxStyle.Information)
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 15, "Sorry, Total Percentage must be 100 for employee : ") & itm.employeename, enMsgBoxStyle.Information)
                        'Hemant (26 Dec 2018) -- End
                Exit Sub
            End If
                Next
                'Sohail (29 Mar 2017) -- End
            End If

            'Sohail (23 Mar 2020) -- Start
            'Internal Enhancement # : Showing progress counts on employee cost center screen.
            'objEmployeeCostcenter._DataTable = mdtTran
            'objEmployeeCostcenter._Userunkid = User._Object._Userunkid
            ''Sohail (21 Aug 2015) -- Start
            ''Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            ''blnFlag = objEmployeeCostcenter.InsertUpdateDelete_EmpCostCenter()

            ''Hemant (26 Dec 2018) -- Start
            ''Enhancement - On employee cost centre, provide a global option to assign all employees, on a single transaction head to one cost centre. 
            ''Also on this screen, provide an analysis by option where user can filter out employees in 76.1.
            ''blnFlag = objEmployeeCostcenter.InsertUpdateDelete_EmpCostCenter(FinancialYear._Object._DatabaseName, ConfigParameter._Object._CurrentDateAndTime)
            'blnFlag = objEmployeeCostcenter.InsertUpdateDelete_EmpCostCenter(FinancialYear._Object._DatabaseName, ConfigParameter._Object._CurrentDateAndTime, Nothing)
            ''Hemant (26 Dec 2018) -- End
            ''Sohail (21 Aug 2015) -- End

            'If blnFlag Then
            '    mblnCancel = False
            '    Me.Close()
            '    'Sohail (29 Mar 2017) -- Start
            '    'CCBRT Enhancement - 66.1 - Employee cost center distribution for non-salary heads, Effective period option on Employee Cost Center.
            'Else
            '    If objEmployeeCostcenter._Message <> "" Then
            '        eZeeMsgBox.Show(objEmployeeCostcenter._Message, enMsgBoxStyle.Information)
            '    End If
            '    'Sohail (29 Mar 2017) -- End
            'End If
            RemoveHandler gfrmMDI.tmrReminder.Tick, AddressOf gfrmMDI.tmrReminder_Tick
            objbgWorker.RunWorkerAsync()
            'Sohail (23 Mar 2020) -- End
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnSave_Click", mstrModuleName)
            RemoveHandler gfrmMDI.tmrReminder.Tick, AddressOf gfrmMDI.tmrReminder_Tick
            AddHandler gfrmMDI.tmrReminder.Tick, AddressOf gfrmMDI.tmrReminder_Tick
        End Try
    End Sub

    'Sohail (23 Mar 2020) -- Start
    'Internal Enhancement # : Showing progress counts on employee cost center screen.
    Private Sub btnStop_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnStop.Click
        Try
            If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 20, "Are you sure you want to Stop the Process?"), CType(enMsgBoxStyle.Information + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.No Then
                Exit Sub
            End If

            mblnStopPayrollProcess = True
            objbgWorker.CancelAsync()

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnStop_Click", mstrModuleName)
        End Try
    End Sub
    'Sohail (23 Mar 2020) -- End

#End Region

#Region " Listview Events "
    'Hemant (11 Mar 2018) -- Start
    'ISSUE : performance issue on emp cost centre list screen, taking 30 to 45 mins to open
    'Private Sub lvEmpCostCenter_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles lvEmpCostCenter.SelectedIndexChanged
    '    Try
    '        If lvEmpCostCenter.SelectedItems.Count > 0 Then
    '            mintItemIndex = CInt(lvEmpCostCenter.SelectedItems(0).Index)
    '            'Sohail (29 Mar 2017) -- Start
    '            'CCBRT Enhancement - 66.1 - Employee cost center distribution for non-salary heads, Effective period option on Employee Cost Center.
    '            'cboTranHead.SelectedValue = 0
    '            'cboCostcenter.SelectedValue = 0

    '            'cboEmployee.SelectedValue = CInt(lvEmpCostCenter.SelectedItems(0).SubItems(colhEmployee.Index).Tag)
    '            'cboTranHead.SelectedValue = CInt(lvEmpCostCenter.SelectedItems(0).SubItems(colhTranHead.Index).Tag)
    '            'cboCostcenter.SelectedValue = CInt(lvEmpCostCenter.SelectedItems(0).SubItems(colhCostCenter.Index).Tag)
    '            'txtPercentage.Text = lvEmpCostCenter.SelectedItems(0).SubItems(colhPerc.Index).Text
    '            RemoveHandler cboPeriod.SelectedIndexChanged, AddressOf cboPeriod_SelectedIndexChanged
    '            cboPeriod.SelectedValue = CInt(lvEmpCostCenter.Items(mintItemIndex).SubItems(colhPeriod.Index).Tag)
    '            AddHandler cboPeriod.SelectedIndexChanged, AddressOf cboPeriod_SelectedIndexChanged
    '            'Hemant (06 Feb 2019) -- Start
    '            'Issue : InvalidArgument=Value of '5' is not valid for 'index'.
    '            'cboEmployee.SelectedValue = CInt(lvEmpCostCenter.Items(mintItemIndex).SubItems(colhEmployee.Index).Tag)
    '            'Hemant (06 Feb 2019) -- End
    '            RemoveHandler cboTranHead.SelectedIndexChanged, AddressOf cboTranHead_SelectedIndexChanged
    '            cboTranHead.SelectedValue = CInt(lvEmpCostCenter.Items(mintItemIndex).SubItems(colhTranHead.Index).Tag)
    '            AddHandler cboTranHead.SelectedIndexChanged, AddressOf cboTranHead_SelectedIndexChanged
    '            'Sohail (07 Feb 2019) -- Start
    '            'NMB Enhancement - 76.1 - Employee cost centre should be distributed on all allocations instead of only on cost centre.
    '            cboAllocation.SelectedValue = CInt(lvEmpCostCenter.Items(mintItemIndex).SubItems(colhAllocation.Index).Tag)
    '            'Sohail (07 Feb 2019) -- End
    '            cboCostcenter.SelectedValue = CInt(lvEmpCostCenter.Items(mintItemIndex).SubItems(colhCostCenter.Index).Tag)
    '            txtPercentage.Text = lvEmpCostCenter.Items(mintItemIndex).SubItems(colhPerc.Index).Text

    '            cboTranHead.Enabled = False
    '            'Sohail (29 Mar 2017) -- End

    '            'Hemant (06 Feb 2019) -- Start
    '            'Issue : InvalidArgument=Value of '5' is not valid for 'index'.
    '            'btnAdd.Enabled = False
    '            'Hemant (06 Feb 2019) -- End

    '        Else
    '            Call ResetValue()
    '        End If
    '    Catch ex As Exception
    '        DisplayError.Show("-1", ex.Message, "lvEmpCostCenter_SelectedIndexChanged", mstrModuleName)
    '    End Try
    'End Sub
    'Hemant (11 Mar 2018) -- End
#End Region

#Region " Other Control's Events "
    Private Sub objbtnSearchEmployee_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchEmployee.Click
        Dim objFrm As New frmCommonSearch
        Try
            With objFrm
                .DataSource = CType(cboEmployee.DataSource, DataTable)
                .ValueMember = cboEmployee.ValueMember
                .DisplayMember = cboEmployee.DisplayMember
                .CodeMember = "employeecode"
            End With

            If objFrm.DisplayDialog Then
                cboEmployee.SelectedValue = objFrm.SelectedValue
                cboEmployee.Focus()
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchEmployee_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objSearchED_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objSearchED.Click
        Dim objfrmCommonsearch As New frmCommonSearch
        Try
            objfrmCommonsearch.DataSource = CType(cboTranHead.DataSource, DataTable)
            objfrmCommonsearch.ValueMember = cboTranHead.ValueMember
            objfrmCommonsearch.DisplayMember = cboTranHead.DisplayMember
            objfrmCommonsearch.CodeMember = "code"
            If objfrmCommonsearch.DisplayDialog() Then
                cboTranHead.SelectedValue = objfrmCommonsearch.SelectedValue
            End If
            cboTranHead.Select()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objSearchED_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objSearchCostCenter_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objSearchCostCenter.Click
        Dim objfrmCommonsearch As New frmCommonSearch
        Try
            objfrmCommonsearch.DataSource = CType(cboCostcenter.DataSource, DataTable)
            objfrmCommonsearch.ValueMember = cboCostcenter.ValueMember
            objfrmCommonsearch.DisplayMember = cboCostcenter.DisplayMember
            'Sohail (07 Feb 2019) -- Start
            'NMB Enhancement - 76.1 - Employee cost centre should be distributed on all allocations instead of only on cost centre.
            'objfrmCommonsearch.CodeMember = "costcentercode"
            If CInt(cboAllocation.SelectedValue) = enAllocation.COST_CENTER Then
            objfrmCommonsearch.CodeMember = "costcentercode"
            Else
                objfrmCommonsearch.CodeMember = "code"
            End If
            'Sohail (07 Feb 2019) -- End
            If objfrmCommonsearch.DisplayDialog() Then
                cboCostcenter.SelectedValue = objfrmCommonsearch.SelectedValue
            End If
            cboCostcenter.Select()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objSearchCostCenter_Click", mstrModuleName)
        End Try
    End Sub

    'Hemant (26 Dec 2018) -- Start
    'Enhancement - On employee cost centre, provide a global option to assign all employees, on a single transaction head to one cost centre. 
    'Also on this screen, provide an analysis by option where user can filter out employees in 76.1.
    Private Sub txtSearchEmp_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtSearchEmp.TextChanged
        Try
            If txtSearchEmp.Text.Trim = mstrSearchEmpText Then Exit Sub
            If dvEmployee IsNot Nothing Then
                dvEmployee.RowFilter = "employeecode LIKE '%" & txtSearchEmp.Text.Replace("'", "''") & "%'  OR employeename LIKE '%" & txtSearchEmp.Text.Replace("'", "''") & "%'"
                dgEmployee.Refresh()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "txtSearchEmp_TextChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub lnkAllocation_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lnkAllocation.LinkClicked
        Try
            Dim frm As New frmAdvanceSearch
            frm.ShowDialog()
            mstrAdvanceFilter = frm._GetFilterString
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lnkAllocation_LinkClicked", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnSearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearch.Click
        Try
            If objchkSelectAll.Checked = True Then objchkSelectAll.Checked = False
            Call FillCombo_Employee()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearch_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnReset.Click
        Try
            If cboEmployee.Items.Count > 0 Then cboEmployee.SelectedValue = 0
            mstrAdvanceFilter = ""
            dgEmployee.DataSource = Nothing
            If objchkSelectAll.Checked = True Then objchkSelectAll.Checked = False
            Call FillCombo_Employee()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnReset_Click", mstrModuleName)
        End Try
    End Sub
    'Hemant (26 Dec 2018) -- End

    'Sohail (23 Mar 2020) -- Start
    'Internal Enhancement # : Showing progress counts on employee cost center screen.
    Private Sub objbgWorker_DoWork(ByVal sender As Object, ByVal e As System.ComponentModel.DoWorkEventArgs) Handles objbgWorker.DoWork
        Try
            GC.Collect()

            mblnProcessFailed = False
            Me.ControlBox = False

            Call EnableControls(False)

            Dim dtFinal As DataTable = New DataView(mdtTran, "AUD IS NOT NULL AND AUD <> '' ", "", DataViewRowState.CurrentRows).ToTable
            objEmployeeCostcenter._DataTable = dtFinal
            objEmployeeCostcenter._Userunkid = User._Object._Userunkid

            objEmployeeCostcenter.InsertUpdateDelete_EmpCostCenter(FinancialYear._Object._DatabaseName, ConfigParameter._Object._CurrentDateAndTime, Nothing, objbgWorker)

        Catch ex As Exception
            mblnProcessFailed = True
            RemoveHandler gfrmMDI.tmrReminder.Tick, AddressOf gfrmMDI.tmrReminder_Tick
            AddHandler gfrmMDI.tmrReminder.Tick, AddressOf gfrmMDI.tmrReminder_Tick
            clsApplicationIdleTimer.LastIdealTime.Start()
            DisplayError.Show("-1", ex.Message, "objbgWorker_DoWork", mstrModuleName)
        Finally
            RemoveHandler gfrmMDI.tmrReminder.Tick, AddressOf gfrmMDI.tmrReminder_Tick
            AddHandler gfrmMDI.tmrReminder.Tick, AddressOf gfrmMDI.tmrReminder_Tick
            clsApplicationIdleTimer.LastIdealTime.Start()
        End Try
    End Sub

    Private Sub objbgWorker_ProgressChanged(ByVal sender As Object, ByVal e As System.ComponentModel.ProgressChangedEventArgs) Handles objbgWorker.ProgressChanged
        Try
            objlblProgress.Text = "[ " & e.ProgressPercentage.ToString & " / " & clsemployee_costcenter_Tran._ProgressTotalCount & " ]"
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbgWorker_ProgressChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub objbgWorker_RunWorkerCompleted(ByVal sender As Object, ByVal e As System.ComponentModel.RunWorkerCompletedEventArgs) Handles objbgWorker.RunWorkerCompleted
        Try

            RemoveHandler gfrmMDI.tmrReminder.Tick, AddressOf gfrmMDI.tmrReminder_Tick
            AddHandler gfrmMDI.tmrReminder.Tick, AddressOf gfrmMDI.tmrReminder_Tick
            clsApplicationIdleTimer.LastIdealTime.Start()

            If e.Cancelled = True Then

            ElseIf e.Error IsNot Nothing Then
                eZeeMsgBox.Show(e.Error.ToString, enMsgBoxStyle.Information)
            ElseIf mblnProcessFailed = True Then

            Else
                mblnCancel = False
                Me.Close()
            End If


        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbgWorker_RunWorkerCompleted", mstrModuleName)
        Finally
            Me.ControlBox = True
            Call EnableControls(True)
            mblnStopPayrollProcess = False
        End Try
    End Sub
    'Sohail (23 Mar 2020) -- End

#End Region

#Region " GridView Events "

    'Hemant (26 Dec 2018) -- Start
    'Enhancement - On employee cost centre, provide a global option to assign all employees, on a single transaction head to one cost centre. 
    'Also on this screen, provide an analysis by option where user can filter out employees in 76.1.
    Private Sub dgEmployee_CurrentCellDirtyStateChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles dgEmployee.CurrentCellDirtyStateChanged
        Try
            If dgEmployee.IsCurrentCellDirty Then
                dgEmployee.CommitEdit(DataGridViewDataErrorContexts.Commit)
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgEmployee_CurrentCellDirtyStateChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub dgEmployee_CellValueChanged(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgEmployee.CellValueChanged
        Try
            If e.RowIndex < 0 Then Exit Sub

            If e.ColumnIndex = objdgcolhCheck.Index Then
                SetCheckBoxValue()
                cboTranHead_SelectedIndexChanged(cboTranHead, e)
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgEmployee_CellValueChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub dgEmployee_CellContentClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgEmployee.CellContentClick
        Dim objEmployee As New clsEmployee_Master
        Dim objCC As New clscostcenter_master
        Try
            If e.RowIndex < 0 Then Exit Sub
            objEmployee._Employeeunkid(eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate)) = CInt(dvEmployee.Table.Rows(e.RowIndex)("employeeunkid"))

            objCC._Costcenterunkid = objEmployee._Costcenterunkid
            lblDefCC.Text = Language.getMessage(mstrModuleName, 10, "Default Cost Center :") & " " & objCC._Costcentername
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgEmployee_CellContentClick", mstrModuleName)
        End Try
    End Sub

    Private Sub dgvEmpCostCenter_SelectionChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles dgvEmpCostCenter.SelectionChanged
        Try
            If dgvEmpCostCenter.SelectedRows.Count > 0 Then
                If dgvEmpCostCenter.Focused = True Then

                    mintItemIndex = CInt(dgvEmpCostCenter.CurrentRow.Index)

                    RemoveHandler cboPeriod.SelectedIndexChanged, AddressOf cboPeriod_SelectedIndexChanged
                    cboPeriod.SelectedValue = CInt(dgvEmpCostCenter.Rows(mintItemIndex).Cells(objdgcolhPeriodID.Index).Value)
                    AddHandler cboPeriod.SelectedIndexChanged, AddressOf cboPeriod_SelectedIndexChanged

                    RemoveHandler cboTranHead.SelectedIndexChanged, AddressOf cboTranHead_SelectedIndexChanged
                    cboTranHead.SelectedValue = CInt(dgvEmpCostCenter.Rows(mintItemIndex).Cells(objcolhTranHeadID.Index).Value)
                    AddHandler cboTranHead.SelectedIndexChanged, AddressOf cboTranHead_SelectedIndexChanged

                    cboAllocation.SelectedValue = CInt(dgvEmpCostCenter.Rows(mintItemIndex).Cells(objcolhAllocationID.Index).Value)

                    cboCostcenter.SelectedValue = CInt(dgvEmpCostCenter.Rows(mintItemIndex).Cells(objcolhCostCenterID.Index).Value)
                    txtPercentage.Text = Format(CDec(dgvEmpCostCenter.Rows(mintItemIndex).Cells(dgcolhPerc.Index).Value.ToString), "0.00")

                    cboTranHead.Enabled = False
                End If

            Else
                Call ResetValue()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgvEmpCostCenter_SelectionChanged", mstrModuleName)
        End Try
    End Sub
    'Hemant (26 Dec 2018) -- End
#End Region

#Region "Checkbox Events"
    'Hemant (26 Dec 2018) -- Start
    'Enhancement - On employee cost centre, provide a global option to assign all employees, on a single transaction head to one cost centre. 
    'Also on this screen, provide an analysis by option where user can filter out employees in 76.1.
    Private Sub chkSelectAll_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objchkSelectAll.CheckedChanged
        Try
            Call CheckAllEmployee(objchkSelectAll.Checked)
            cboTranHead_SelectedIndexChanged(cboTranHead, e)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "chkSelectAll_CheckedChanged", mstrModuleName)
        End Try
    End Sub
    'Hemant (26 Dec 2018) -- End

    'Hemant (11 Mar 2018) -- Start
    'ISSUE/ENHANCEMENT : need filter to show only newly hired, rehired employees and those not assigned, same as on global assign
    Private Sub chkShowReinstatedEmployees_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkShowReinstatedEmployees.CheckedChanged
        Try
            Call FillCombo_Employee()
            Call FillList()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "chkShowReinstatedEmployees_CheckedChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub chkShowNewlyHiredEmployees_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkShowNewlyHiredEmployees.CheckedChanged
        Try
            Call FillCombo_Employee()
            Call FillList()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "chkShowNewlyHiredEmployees_CheckedChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub chkNotAssignedHead_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkNotAssignedHead.CheckedChanged
        Try
            Call FillCombo_Employee()
            Call FillList()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "chkNotAssignedHead_CheckedChanged", mstrModuleName)
        End Try
    End Sub
    'Hemant (11 Mar 2018) -- End

#End Region

#Region " Messages "
    '1, "Employee is compulosry information. Please select Employee to continue.")
    '2, "Transaction Head is compulosry information. Please select Transaction Head to continue."
    '3, "Cost center is compulosry information. Please select Cost center to continue."
    '4, "Sorry, Percentage should be greater than Zero."
    '5, "Sorry, Percentage should not be greater than 100."
    '6, "Selected Cost Center is already added to the list.
    '7, "Sorry, You can not delete this Employee Cost Center. This Employee Cost Center is in used."
    '8, "Please add atleast one employee Cost Center."
    '9, "Sorry, Total Percentage must be 100."
#End Region

	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Private Sub OtherSettings()
		Try
			Me.SuspendLayout()
			
            Call SetLanguage()
			
			Me.gbCostCenterInfo.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbCostCenterInfo.ForeColor = GUI._eZeeContainerHeaderForeColor 

			Me.gbEmployeeList.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbEmployeeList.ForeColor = GUI._eZeeContainerHeaderForeColor 


			
			Me.eZeeHeader.GradientColor1 = GUI._HeaderBackColor1 
			Me.eZeeHeader.GradientColor2 = GUI._HeaderBackColor2 
			Me.eZeeHeader.BorderColor = GUI._HeaderBorderColor 
			Me.eZeeHeader.HeaderTextForeColor = GUI._HeaderTitleFontColor 
			Me.eZeeHeader.DescriptionForeColor = GUI._HeaderMessageFontColor 


			Me.btnSave.GradientBackColor = GUI._ButttonBackColor 
			Me.btnSave.GradientForeColor = GUI._ButttonFontColor

			Me.btnClose.GradientBackColor = GUI._ButttonBackColor 
			Me.btnClose.GradientForeColor = GUI._ButttonFontColor

			Me.btnAdd.GradientBackColor = GUI._ButttonBackColor 
			Me.btnAdd.GradientForeColor = GUI._ButttonFontColor

			Me.btnDelete.GradientBackColor = GUI._ButttonBackColor 
			Me.btnDelete.GradientForeColor = GUI._ButttonFontColor

			Me.btnEdit.GradientBackColor = GUI._ButttonBackColor 
			Me.btnEdit.GradientForeColor = GUI._ButttonFontColor

			Me.btnStop.GradientBackColor = GUI._ButttonBackColor 
			Me.btnStop.GradientForeColor = GUI._ButttonFontColor


			Me.ResumeLayout()
		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "OtherSettings", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetLanguage()
		Try
			Me.Text = Language._Object.getCaption(Me.Name, Me.Text)
			
			Me.eZeeHeader.Title = Language._Object.getCaption(Me.eZeeHeader.Name & "_Title" , Me.eZeeHeader.Title)
			Me.eZeeHeader.Message = Language._Object.getCaption(Me.eZeeHeader.Name & "_Message" , Me.eZeeHeader.Message)
			Me.btnSave.Text = Language._Object.getCaption(Me.btnSave.Name, Me.btnSave.Text)
			Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
			Me.gbCostCenterInfo.Text = Language._Object.getCaption(Me.gbCostCenterInfo.Name, Me.gbCostCenterInfo.Text)
			Me.lblPerc.Text = Language._Object.getCaption(Me.lblPerc.Name, Me.lblPerc.Text)
			Me.lblSalaryDistrib.Text = Language._Object.getCaption(Me.lblSalaryDistrib.Name, Me.lblSalaryDistrib.Text)
			Me.lblDefCC.Text = Language._Object.getCaption(Me.lblDefCC.Name, Me.lblDefCC.Text)
			Me.lblCostCenter.Text = Language._Object.getCaption(Me.lblCostCenter.Name, Me.lblCostCenter.Text)
			Me.lblTrnHead.Text = Language._Object.getCaption(Me.lblTrnHead.Name, Me.lblTrnHead.Text)
			Me.lblEmployee.Text = Language._Object.getCaption(Me.lblEmployee.Name, Me.lblEmployee.Text)
			Me.lblPeriod.Text = Language._Object.getCaption(Me.lblPeriod.Name, Me.lblPeriod.Text)
			Me.gbEmployeeList.Text = Language._Object.getCaption(Me.gbEmployeeList.Name, Me.gbEmployeeList.Text)
			Me.dgColhEmpCode.HeaderText = Language._Object.getCaption(Me.dgColhEmpCode.Name, Me.dgColhEmpCode.HeaderText)
			Me.dgColhEmployee.HeaderText = Language._Object.getCaption(Me.dgColhEmployee.Name, Me.dgColhEmployee.HeaderText)
			Me.btnAdd.Text = Language._Object.getCaption(Me.btnAdd.Name, Me.btnAdd.Text)
			Me.btnDelete.Text = Language._Object.getCaption(Me.btnDelete.Name, Me.btnDelete.Text)
			Me.btnEdit.Text = Language._Object.getCaption(Me.btnEdit.Name, Me.btnEdit.Text)
			Me.lblSearchEmp.Text = Language._Object.getCaption(Me.lblSearchEmp.Name, Me.lblSearchEmp.Text)
			Me.lnkAllocation.Text = Language._Object.getCaption(Me.lnkAllocation.Name, Me.lnkAllocation.Text)
			Me.chkIsExist.Text = Language._Object.getCaption(Me.chkIsExist.Name, Me.chkIsExist.Text)
			Me.lblAllocation.Text = Language._Object.getCaption(Me.lblAllocation.Name, Me.lblAllocation.Text)
			Me.dgcolhEmployee1.HeaderText = Language._Object.getCaption(Me.dgcolhEmployee1.Name, Me.dgcolhEmployee1.HeaderText)
			Me.dgcolhTranHead.HeaderText = Language._Object.getCaption(Me.dgcolhTranHead.Name, Me.dgcolhTranHead.HeaderText)
			Me.dgcolhAllocation.HeaderText = Language._Object.getCaption(Me.dgcolhAllocation.Name, Me.dgcolhAllocation.HeaderText)
			Me.dgcolhCostCenter.HeaderText = Language._Object.getCaption(Me.dgcolhCostCenter.Name, Me.dgcolhCostCenter.HeaderText)
			Me.dgcolhPerc.HeaderText = Language._Object.getCaption(Me.dgcolhPerc.Name, Me.dgcolhPerc.HeaderText)
			Me.dgcolhPeriod.HeaderText = Language._Object.getCaption(Me.dgcolhPeriod.Name, Me.dgcolhPeriod.HeaderText)
			Me.chkShowReinstatedEmployees.Text = Language._Object.getCaption(Me.chkShowReinstatedEmployees.Name, Me.chkShowReinstatedEmployees.Text)
			Me.chkShowNewlyHiredEmployees.Text = Language._Object.getCaption(Me.chkShowNewlyHiredEmployees.Name, Me.chkShowNewlyHiredEmployees.Text)
			Me.chkNotAssignedHead.Text = Language._Object.getCaption(Me.chkNotAssignedHead.Name, Me.chkNotAssignedHead.Text)
			Me.btnStop.Text = Language._Object.getCaption(Me.btnStop.Name, Me.btnStop.Text)

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetLanguage", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 2, "Transaction Head is compulosry information. Please select Transaction Head to continue.")
			Language.setMessage(mstrModuleName, 4, "Sorry, Percentage should be greater than Zero.")
			Language.setMessage(mstrModuleName, 5, "Sorry, Percentage should not be greater than 100.")
			Language.setMessage(mstrModuleName, 7, "Sorry, You can not delete this Employee Cost Center. This Employee Cost Center is in used.")
			Language.setMessage(mstrModuleName, 8, "Please add atleast one employee Cost Center.")
			Language.setMessage(mstrModuleName, 10, "Default Cost Center :")
			Language.setMessage(mstrModuleName, 11, "Sorry, This Period is closed.")
			Language.setMessage(mstrModuleName, 12, "Please select Period.")
			Language.setMessage(mstrModuleName, 14, "Please select atleast one Employee from list.")
			Language.setMessage(mstrModuleName, 15, "Sorry, Total Percentage must be 100 for employee :")
            Language.setMessage(mstrModuleName, 16, "Sorry, Payroll Process is already done for some of the employees.")
			Language.setMessage(mstrModuleName, 17, "is compulosry information. Please select")
			Language.setMessage(mstrModuleName, 18, "Type to Search")
			Language.setMessage(mstrModuleName, 19, "Do you want to void Payroll?")
			Language.setMessage(mstrModuleName, 20, "Are you sure you want to Stop the Process?")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class