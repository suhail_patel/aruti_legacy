﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmImportWagesTable
    Inherits eZee.Common.eZeeForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmImportWagesTable))
        Dim DataGridViewCellStyle5 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle6 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle7 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle8 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle4 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Me.gbFileInfo = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.txtFilePath = New System.Windows.Forms.TextBox
        Me.btnOpenFile = New eZee.Common.eZeeLightButton(Me.components)
        Me.lblFileName = New System.Windows.Forms.Label
        Me.objefFormFooter = New eZee.Common.eZeeFooter
        Me.btnHeadOperations = New eZee.Common.eZeeSplitButton
        Me.cmnuOperations = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.mnuShowSuccessful = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuShowUnSuccessful = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuShowAll = New System.Windows.Forms.ToolStripMenuItem
        Me.btnGetFileFomat = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnExport = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnClose = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnImport = New eZee.Common.eZeeLightButton(Me.components)
        Me.dgvWagesList = New System.Windows.Forms.DataGridView
        Me.chkCheckAll = New System.Windows.Forms.CheckBox
        Me.DataGridViewTextBoxColumn1 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn2 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn3 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn4 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn5 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn6 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn7 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn8 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn9 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhCheck = New System.Windows.Forms.DataGridViewCheckBoxColumn
        Me.dgcolhPeriodName = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhGradeGroupCode = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhGradeCode = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhGradeLevelCode = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhMinimum = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhMidpoint = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhMaximum = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhIncrement = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhRemark = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.gbFileInfo.SuspendLayout()
        Me.objefFormFooter.SuspendLayout()
        Me.cmnuOperations.SuspendLayout()
        CType(Me.dgvWagesList, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'gbFileInfo
        '
        Me.gbFileInfo.BorderColor = System.Drawing.Color.Black
        Me.gbFileInfo.Checked = False
        Me.gbFileInfo.CollapseAllExceptThis = False
        Me.gbFileInfo.CollapsedHoverImage = Nothing
        Me.gbFileInfo.CollapsedNormalImage = Nothing
        Me.gbFileInfo.CollapsedPressedImage = Nothing
        Me.gbFileInfo.CollapseOnLoad = False
        Me.gbFileInfo.Controls.Add(Me.txtFilePath)
        Me.gbFileInfo.Controls.Add(Me.btnOpenFile)
        Me.gbFileInfo.Controls.Add(Me.lblFileName)
        Me.gbFileInfo.ExpandedHoverImage = Nothing
        Me.gbFileInfo.ExpandedNormalImage = Nothing
        Me.gbFileInfo.ExpandedPressedImage = Nothing
        Me.gbFileInfo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbFileInfo.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbFileInfo.HeaderHeight = 25
        Me.gbFileInfo.HeaderMessage = ""
        Me.gbFileInfo.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbFileInfo.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbFileInfo.HeightOnCollapse = 0
        Me.gbFileInfo.LeftTextSpace = 0
        Me.gbFileInfo.Location = New System.Drawing.Point(12, 12)
        Me.gbFileInfo.Name = "gbFileInfo"
        Me.gbFileInfo.OpenHeight = 300
        Me.gbFileInfo.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbFileInfo.ShowBorder = True
        Me.gbFileInfo.ShowCheckBox = False
        Me.gbFileInfo.ShowCollapseButton = False
        Me.gbFileInfo.ShowDefaultBorderColor = True
        Me.gbFileInfo.ShowDownButton = False
        Me.gbFileInfo.ShowHeader = True
        Me.gbFileInfo.Size = New System.Drawing.Size(795, 66)
        Me.gbFileInfo.TabIndex = 3
        Me.gbFileInfo.Temp = 0
        Me.gbFileInfo.Text = "Import File"
        Me.gbFileInfo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtFilePath
        '
        Me.txtFilePath.BackColor = System.Drawing.Color.White
        Me.txtFilePath.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtFilePath.Location = New System.Drawing.Point(115, 34)
        Me.txtFilePath.Name = "txtFilePath"
        Me.txtFilePath.ReadOnly = True
        Me.txtFilePath.Size = New System.Drawing.Size(638, 21)
        Me.txtFilePath.TabIndex = 3
        '
        'btnOpenFile
        '
        Me.btnOpenFile.BackColor = System.Drawing.Color.White
        Me.btnOpenFile.BackgroundImage = CType(resources.GetObject("btnOpenFile.BackgroundImage"), System.Drawing.Image)
        Me.btnOpenFile.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnOpenFile.BorderColor = System.Drawing.Color.Empty
        Me.btnOpenFile.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnOpenFile.FlatAppearance.BorderSize = 0
        Me.btnOpenFile.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnOpenFile.ForeColor = System.Drawing.Color.Black
        Me.btnOpenFile.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnOpenFile.GradientForeColor = System.Drawing.Color.Black
        Me.btnOpenFile.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnOpenFile.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnOpenFile.Location = New System.Drawing.Point(759, 34)
        Me.btnOpenFile.Name = "btnOpenFile"
        Me.btnOpenFile.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnOpenFile.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnOpenFile.Size = New System.Drawing.Size(23, 20)
        Me.btnOpenFile.TabIndex = 17
        Me.btnOpenFile.Text = "..."
        Me.btnOpenFile.UseVisualStyleBackColor = False
        '
        'lblFileName
        '
        Me.lblFileName.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblFileName.Location = New System.Drawing.Point(15, 35)
        Me.lblFileName.Name = "lblFileName"
        Me.lblFileName.Size = New System.Drawing.Size(94, 17)
        Me.lblFileName.TabIndex = 3
        Me.lblFileName.Text = "Select File"
        Me.lblFileName.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objefFormFooter
        '
        Me.objefFormFooter.BorderColor = System.Drawing.Color.Silver
        Me.objefFormFooter.Controls.Add(Me.btnHeadOperations)
        Me.objefFormFooter.Controls.Add(Me.btnGetFileFomat)
        Me.objefFormFooter.Controls.Add(Me.btnExport)
        Me.objefFormFooter.Controls.Add(Me.btnClose)
        Me.objefFormFooter.Controls.Add(Me.btnImport)
        Me.objefFormFooter.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.objefFormFooter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objefFormFooter.GradiantStyle = eZee.Common.eZeeFooter.GradientStyle.Up
        Me.objefFormFooter.GradientColor1 = System.Drawing.Color.Transparent
        Me.objefFormFooter.GradientColor2 = System.Drawing.Color.Transparent
        Me.objefFormFooter.Location = New System.Drawing.Point(0, 407)
        Me.objefFormFooter.Name = "objefFormFooter"
        Me.objefFormFooter.Size = New System.Drawing.Size(819, 55)
        Me.objefFormFooter.TabIndex = 4
        '
        'btnHeadOperations
        '
        Me.btnHeadOperations.BorderColor = System.Drawing.Color.Black
        Me.btnHeadOperations.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnHeadOperations.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnHeadOperations.GradientForeColor = System.Drawing.SystemColors.WindowText
        Me.btnHeadOperations.Location = New System.Drawing.Point(12, 13)
        Me.btnHeadOperations.Name = "btnHeadOperations"
        Me.btnHeadOperations.ShowDefaultBorderColor = True
        Me.btnHeadOperations.Size = New System.Drawing.Size(110, 30)
        Me.btnHeadOperations.SplitButtonMenu = Me.cmnuOperations
        Me.btnHeadOperations.TabIndex = 100
        Me.btnHeadOperations.Text = "Operations"
        '
        'cmnuOperations
        '
        Me.cmnuOperations.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mnuShowSuccessful, Me.mnuShowUnSuccessful, Me.mnuShowAll})
        Me.cmnuOperations.Name = "cmnuOperations"
        Me.cmnuOperations.Size = New System.Drawing.Size(203, 70)
        '
        'mnuShowSuccessful
        '
        Me.mnuShowSuccessful.Name = "mnuShowSuccessful"
        Me.mnuShowSuccessful.Size = New System.Drawing.Size(202, 22)
        Me.mnuShowSuccessful.Text = "Show Successful Data"
        '
        'mnuShowUnSuccessful
        '
        Me.mnuShowUnSuccessful.Name = "mnuShowUnSuccessful"
        Me.mnuShowUnSuccessful.Size = New System.Drawing.Size(202, 22)
        Me.mnuShowUnSuccessful.Text = "Show Unsuccessful Data"
        '
        'mnuShowAll
        '
        Me.mnuShowAll.Name = "mnuShowAll"
        Me.mnuShowAll.Size = New System.Drawing.Size(202, 22)
        Me.mnuShowAll.Text = "Show All Data"
        '
        'btnGetFileFomat
        '
        Me.btnGetFileFomat.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnGetFileFomat.BackColor = System.Drawing.Color.White
        Me.btnGetFileFomat.BackgroundImage = CType(resources.GetObject("btnGetFileFomat.BackgroundImage"), System.Drawing.Image)
        Me.btnGetFileFomat.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnGetFileFomat.BorderColor = System.Drawing.Color.Empty
        Me.btnGetFileFomat.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnGetFileFomat.FlatAppearance.BorderSize = 0
        Me.btnGetFileFomat.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnGetFileFomat.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnGetFileFomat.ForeColor = System.Drawing.Color.Black
        Me.btnGetFileFomat.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnGetFileFomat.GradientForeColor = System.Drawing.Color.Black
        Me.btnGetFileFomat.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnGetFileFomat.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnGetFileFomat.Location = New System.Drawing.Point(231, 13)
        Me.btnGetFileFomat.Name = "btnGetFileFomat"
        Me.btnGetFileFomat.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnGetFileFomat.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnGetFileFomat.Size = New System.Drawing.Size(116, 30)
        Me.btnGetFileFomat.TabIndex = 2
        Me.btnGetFileFomat.Text = "&Get File Format"
        Me.btnGetFileFomat.UseVisualStyleBackColor = False
        '
        'btnExport
        '
        Me.btnExport.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnExport.BackColor = System.Drawing.Color.White
        Me.btnExport.BackgroundImage = CType(resources.GetObject("btnExport.BackgroundImage"), System.Drawing.Image)
        Me.btnExport.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnExport.BorderColor = System.Drawing.Color.Empty
        Me.btnExport.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnExport.FlatAppearance.BorderSize = 0
        Me.btnExport.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnExport.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnExport.ForeColor = System.Drawing.Color.Black
        Me.btnExport.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnExport.GradientForeColor = System.Drawing.Color.Black
        Me.btnExport.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnExport.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnExport.Location = New System.Drawing.Point(128, 13)
        Me.btnExport.Name = "btnExport"
        Me.btnExport.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnExport.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnExport.Size = New System.Drawing.Size(97, 30)
        Me.btnExport.TabIndex = 101
        Me.btnExport.Text = "&Export"
        Me.btnExport.UseVisualStyleBackColor = False
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnClose.BackColor = System.Drawing.Color.White
        Me.btnClose.BackgroundImage = CType(resources.GetObject("btnClose.BackgroundImage"), System.Drawing.Image)
        Me.btnClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnClose.BorderColor = System.Drawing.Color.Empty
        Me.btnClose.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnClose.FlatAppearance.BorderSize = 0
        Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClose.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.ForeColor = System.Drawing.Color.Black
        Me.btnClose.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnClose.GradientForeColor = System.Drawing.Color.Black
        Me.btnClose.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Location = New System.Drawing.Point(710, 13)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Size = New System.Drawing.Size(97, 30)
        Me.btnClose.TabIndex = 1
        Me.btnClose.Text = "&Close"
        Me.btnClose.UseVisualStyleBackColor = False
        '
        'btnImport
        '
        Me.btnImport.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnImport.BackColor = System.Drawing.Color.White
        Me.btnImport.BackgroundImage = CType(resources.GetObject("btnImport.BackgroundImage"), System.Drawing.Image)
        Me.btnImport.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnImport.BorderColor = System.Drawing.Color.Empty
        Me.btnImport.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnImport.FlatAppearance.BorderSize = 0
        Me.btnImport.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnImport.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnImport.ForeColor = System.Drawing.Color.Black
        Me.btnImport.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnImport.GradientForeColor = System.Drawing.Color.Black
        Me.btnImport.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnImport.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnImport.Location = New System.Drawing.Point(607, 13)
        Me.btnImport.Name = "btnImport"
        Me.btnImport.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnImport.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnImport.Size = New System.Drawing.Size(97, 30)
        Me.btnImport.TabIndex = 0
        Me.btnImport.Text = "&Import"
        Me.btnImport.UseVisualStyleBackColor = False
        '
        'dgvWagesList
        '
        Me.dgvWagesList.AllowUserToAddRows = False
        Me.dgvWagesList.AllowUserToDeleteRows = False
        Me.dgvWagesList.AllowUserToResizeRows = False
        Me.dgvWagesList.BackgroundColor = System.Drawing.Color.White
        Me.dgvWagesList.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing
        Me.dgvWagesList.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.objdgcolhCheck, Me.dgcolhPeriodName, Me.dgcolhGradeGroupCode, Me.dgcolhGradeCode, Me.dgcolhGradeLevelCode, Me.dgcolhMinimum, Me.dgcolhMidpoint, Me.dgcolhMaximum, Me.dgcolhIncrement, Me.dgcolhRemark})
        Me.dgvWagesList.Location = New System.Drawing.Point(12, 84)
        Me.dgvWagesList.Name = "dgvWagesList"
        Me.dgvWagesList.RowHeadersWidth = 4
        Me.dgvWagesList.Size = New System.Drawing.Size(795, 317)
        Me.dgvWagesList.TabIndex = 5
        '
        'chkCheckAll
        '
        Me.chkCheckAll.AutoSize = True
        Me.chkCheckAll.BackColor = System.Drawing.Color.White
        Me.chkCheckAll.Location = New System.Drawing.Point(23, 89)
        Me.chkCheckAll.Name = "chkCheckAll"
        Me.chkCheckAll.Size = New System.Drawing.Size(15, 14)
        Me.chkCheckAll.TabIndex = 6
        Me.chkCheckAll.UseVisualStyleBackColor = False
        '
        'DataGridViewTextBoxColumn1
        '
        Me.DataGridViewTextBoxColumn1.HeaderText = "Effective Period"
        Me.DataGridViewTextBoxColumn1.Name = "DataGridViewTextBoxColumn1"
        Me.DataGridViewTextBoxColumn1.ReadOnly = True
        Me.DataGridViewTextBoxColumn1.Width = 90
        '
        'DataGridViewTextBoxColumn2
        '
        Me.DataGridViewTextBoxColumn2.HeaderText = "Grade Group Code"
        Me.DataGridViewTextBoxColumn2.Name = "DataGridViewTextBoxColumn2"
        Me.DataGridViewTextBoxColumn2.ReadOnly = True
        Me.DataGridViewTextBoxColumn2.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.DataGridViewTextBoxColumn2.Width = 120
        '
        'DataGridViewTextBoxColumn3
        '
        Me.DataGridViewTextBoxColumn3.HeaderText = "Grade Code"
        Me.DataGridViewTextBoxColumn3.Name = "DataGridViewTextBoxColumn3"
        Me.DataGridViewTextBoxColumn3.ReadOnly = True
        Me.DataGridViewTextBoxColumn3.Width = 120
        '
        'DataGridViewTextBoxColumn4
        '
        Me.DataGridViewTextBoxColumn4.HeaderText = "Grade Level Code"
        Me.DataGridViewTextBoxColumn4.Name = "DataGridViewTextBoxColumn4"
        Me.DataGridViewTextBoxColumn4.ReadOnly = True
        Me.DataGridViewTextBoxColumn4.Width = 120
        '
        'DataGridViewTextBoxColumn5
        '
        DataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle5.Format = "F2"
        Me.DataGridViewTextBoxColumn5.DefaultCellStyle = DataGridViewCellStyle5
        Me.DataGridViewTextBoxColumn5.HeaderText = "Minimum"
        Me.DataGridViewTextBoxColumn5.Name = "DataGridViewTextBoxColumn5"
        Me.DataGridViewTextBoxColumn5.ReadOnly = True
        Me.DataGridViewTextBoxColumn5.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        '
        'DataGridViewTextBoxColumn6
        '
        DataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle6.Format = "F2"
        DataGridViewCellStyle6.NullValue = Nothing
        Me.DataGridViewTextBoxColumn6.DefaultCellStyle = DataGridViewCellStyle6
        Me.DataGridViewTextBoxColumn6.HeaderText = "Mid point"
        Me.DataGridViewTextBoxColumn6.Name = "DataGridViewTextBoxColumn6"
        Me.DataGridViewTextBoxColumn6.ReadOnly = True
        Me.DataGridViewTextBoxColumn6.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        '
        'DataGridViewTextBoxColumn7
        '
        DataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle7.Format = "F2"
        Me.DataGridViewTextBoxColumn7.DefaultCellStyle = DataGridViewCellStyle7
        Me.DataGridViewTextBoxColumn7.HeaderText = "Maximum"
        Me.DataGridViewTextBoxColumn7.Name = "DataGridViewTextBoxColumn7"
        Me.DataGridViewTextBoxColumn7.ReadOnly = True
        Me.DataGridViewTextBoxColumn7.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        '
        'DataGridViewTextBoxColumn8
        '
        DataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle8.Format = "F2"
        Me.DataGridViewTextBoxColumn8.DefaultCellStyle = DataGridViewCellStyle8
        Me.DataGridViewTextBoxColumn8.HeaderText = "Increment"
        Me.DataGridViewTextBoxColumn8.Name = "DataGridViewTextBoxColumn8"
        Me.DataGridViewTextBoxColumn8.ReadOnly = True
        Me.DataGridViewTextBoxColumn8.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        '
        'DataGridViewTextBoxColumn9
        '
        Me.DataGridViewTextBoxColumn9.HeaderText = "Remark"
        Me.DataGridViewTextBoxColumn9.Name = "DataGridViewTextBoxColumn9"
        Me.DataGridViewTextBoxColumn9.ReadOnly = True
        Me.DataGridViewTextBoxColumn9.Width = 200
        '
        'objdgcolhCheck
        '
        Me.objdgcolhCheck.Frozen = True
        Me.objdgcolhCheck.HeaderText = ""
        Me.objdgcolhCheck.Name = "objdgcolhCheck"
        Me.objdgcolhCheck.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.objdgcolhCheck.Width = 25
        '
        'dgcolhPeriodName
        '
        Me.dgcolhPeriodName.HeaderText = "Effective Period"
        Me.dgcolhPeriodName.Name = "dgcolhPeriodName"
        Me.dgcolhPeriodName.ReadOnly = True
        Me.dgcolhPeriodName.Width = 90
        '
        'dgcolhGradeGroupCode
        '
        Me.dgcolhGradeGroupCode.HeaderText = "Grade Group Code"
        Me.dgcolhGradeGroupCode.Name = "dgcolhGradeGroupCode"
        Me.dgcolhGradeGroupCode.ReadOnly = True
        Me.dgcolhGradeGroupCode.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgcolhGradeGroupCode.Width = 120
        '
        'dgcolhGradeCode
        '
        Me.dgcolhGradeCode.HeaderText = "Grade Code"
        Me.dgcolhGradeCode.Name = "dgcolhGradeCode"
        Me.dgcolhGradeCode.ReadOnly = True
        Me.dgcolhGradeCode.Width = 120
        '
        'dgcolhGradeLevelCode
        '
        Me.dgcolhGradeLevelCode.HeaderText = "Grade Level Code"
        Me.dgcolhGradeLevelCode.Name = "dgcolhGradeLevelCode"
        Me.dgcolhGradeLevelCode.ReadOnly = True
        Me.dgcolhGradeLevelCode.Width = 120
        '
        'dgcolhMinimum
        '
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle1.Format = "F2"
        Me.dgcolhMinimum.DefaultCellStyle = DataGridViewCellStyle1
        Me.dgcolhMinimum.HeaderText = "Minimum"
        Me.dgcolhMinimum.Name = "dgcolhMinimum"
        Me.dgcolhMinimum.ReadOnly = True
        Me.dgcolhMinimum.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        '
        'dgcolhMidpoint
        '
        DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle2.Format = "F2"
        DataGridViewCellStyle2.NullValue = Nothing
        Me.dgcolhMidpoint.DefaultCellStyle = DataGridViewCellStyle2
        Me.dgcolhMidpoint.HeaderText = "Mid point"
        Me.dgcolhMidpoint.Name = "dgcolhMidpoint"
        Me.dgcolhMidpoint.ReadOnly = True
        Me.dgcolhMidpoint.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        '
        'dgcolhMaximum
        '
        DataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle3.Format = "F2"
        Me.dgcolhMaximum.DefaultCellStyle = DataGridViewCellStyle3
        Me.dgcolhMaximum.HeaderText = "Maximum"
        Me.dgcolhMaximum.Name = "dgcolhMaximum"
        Me.dgcolhMaximum.ReadOnly = True
        Me.dgcolhMaximum.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        '
        'dgcolhIncrement
        '
        DataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle4.Format = "F2"
        Me.dgcolhIncrement.DefaultCellStyle = DataGridViewCellStyle4
        Me.dgcolhIncrement.HeaderText = "Increment"
        Me.dgcolhIncrement.Name = "dgcolhIncrement"
        Me.dgcolhIncrement.ReadOnly = True
        Me.dgcolhIncrement.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        '
        'dgcolhRemark
        '
        Me.dgcolhRemark.HeaderText = "Remark"
        Me.dgcolhRemark.Name = "dgcolhRemark"
        Me.dgcolhRemark.ReadOnly = True
        Me.dgcolhRemark.Width = 200
        '
        'frmImportWagesTable
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(819, 462)
        Me.Controls.Add(Me.chkCheckAll)
        Me.Controls.Add(Me.dgvWagesList)
        Me.Controls.Add(Me.objefFormFooter)
        Me.Controls.Add(Me.gbFileInfo)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmImportWagesTable"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Import Wages Table"
        Me.gbFileInfo.ResumeLayout(False)
        Me.gbFileInfo.PerformLayout()
        Me.objefFormFooter.ResumeLayout(False)
        Me.cmnuOperations.ResumeLayout(False)
        CType(Me.dgvWagesList, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents gbFileInfo As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents txtFilePath As System.Windows.Forms.TextBox
    Friend WithEvents lblFileName As System.Windows.Forms.Label
    Friend WithEvents btnOpenFile As eZee.Common.eZeeLightButton
    Friend WithEvents objefFormFooter As eZee.Common.eZeeFooter
    Friend WithEvents btnGetFileFomat As eZee.Common.eZeeLightButton
    Friend WithEvents btnExport As eZee.Common.eZeeLightButton
    Friend WithEvents btnClose As eZee.Common.eZeeLightButton
    Friend WithEvents btnImport As eZee.Common.eZeeLightButton
    Friend WithEvents btnHeadOperations As eZee.Common.eZeeSplitButton
    Friend WithEvents dgvWagesList As System.Windows.Forms.DataGridView
    Friend WithEvents chkCheckAll As System.Windows.Forms.CheckBox
    Friend WithEvents cmnuOperations As System.Windows.Forms.ContextMenuStrip
    Friend WithEvents mnuShowSuccessful As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuShowUnSuccessful As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuShowAll As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents DataGridViewTextBoxColumn1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn2 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn3 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn4 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn5 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn6 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn7 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn8 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn9 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhCheck As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents dgcolhPeriodName As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhGradeGroupCode As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhGradeCode As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhGradeLevelCode As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhMinimum As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhMidpoint As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhMaximum As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhIncrement As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhRemark As System.Windows.Forms.DataGridViewTextBoxColumn
End Class
