﻿Option Strict On

#Region " Imports "
Imports eZeeCommonLib
Imports Aruti.Data
#End Region

Public Class frmWagesTableMapping
#Region " Private Variables "
    Private ReadOnly mstrModuleName As String = "frmWagesTableMapping"
    Private mblnCancel As Boolean = True
    Private dsList As New DataSet
    Private mintPeriodunkid As Integer = 0
    Private dtTable As DataTable
#End Region

#Region " Properties "

    Public ReadOnly Property _DataTable() As DataTable
        Get
            Return dtTable
        End Get
    End Property

    Public ReadOnly Property _Periodunkid() As Integer
        Get
            Return mintPeriodunkid
        End Get
    End Property
#End Region

#Region " Display Dialog "
    Public Function displayDialog(ByVal dsData As DataSet) As Boolean
        Try
            dsList = dsData
            Me.ShowDialog()
            Return Not mblnCancel

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "displayDialog", mstrModuleName)
        End Try
    End Function
#End Region

#Region " Form's Events "

    Private Sub frmWagesTableMapping_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        Me.Close()
    End Sub

    Private Sub frmWagesTableMapping_LanguageClick(ByVal sender As Object, ByVal eventArgs As System.EventArgs) Handles Me.LanguageClick
        Try
            Dim objfrm As New frmLanguage

            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsWagesTran.SetMessages()
            objfrm._Other_ModuleNames = "clsWagesTran"
            objfrm.displayDialog(Me)

            Call SetLanguage()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmWagesTableMapping_LanguageClick", mstrModuleName)
        End Try
    End Sub

    Private Sub frmWagesTableMapping_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Call Set_Logo(Me, gApplicationType)
            Call SetColor()
            Call Language.setLanguage(Me.Name)
            Call OtherSettings()

            cboGradeGroupCode.Items.Clear()
            cboGradeCode.Items.Clear()
            cboGradeLevelCode.Items.Clear()
            cboMinimum.Items.Clear()
            cboMidPoint.Items.Clear()
            cboMaximum.Items.Clear()
            cboIncrement.Items.Clear()

            Call FillCombo()
            Call SetData()

            lblMessage.Text = "*   indicates mandatory fields."

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmWagesTableMapping_Load", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Private Methods "

    Private Sub SetColor()
        Try
            cboPeriod.BackColor = GUI.ColorComp
            cboGradeGroupCode.BackColor = GUI.ColorComp
            cboGradeCode.BackColor = GUI.ColorComp
            cboGradeLevelCode.BackColor = GUI.ColorComp
            cboMinimum.BackColor = GUI.ColorComp
            cboMidPoint.BackColor = GUI.ColorOptional
            cboMaximum.BackColor = GUI.ColorComp
            cboIncrement.BackColor = GUI.ColorComp

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetColor", mstrModuleName)
        End Try
    End Sub

    Private Sub FillCombo()
        Try
            Dim dsList As New DataSet
            Dim objPeriod As New clscommom_period_Tran

            dsList = objPeriod.getListForCombo(enModuleReference.Payroll, FinancialYear._Object._YearUnkid, FinancialYear._Object._DatabaseName, _
                                               FinancialYear._Object._Database_Start_Date, "List", True, enStatusType.Open)
            With cboPeriod
                .DisplayMember = "name"
                .ValueMember = "periodunkid"
                .DataSource = dsList.Tables("List")
                .SelectedValue = 0
            End With
            objPeriod = Nothing

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        End Try
    End Sub

    Private Sub SetData()
        Try
            dtTable = New DataTable("Wages")

            For Each dtCol As DataColumn In dsList.Tables(0).Columns
                cboGradeGroupCode.Items.Add(dtCol.ColumnName)
                cboGradeCode.Items.Add(dtCol.ColumnName)
                cboGradeLevelCode.Items.Add(dtCol.ColumnName)
                cboMinimum.Items.Add(dtCol.ColumnName)
                cboMidPoint.Items.Add(dtCol.ColumnName)
                cboMaximum.Items.Add(dtCol.ColumnName)
                cboIncrement.Items.Add(dtCol.ColumnName)
            Next
            'System.DateTime
            dtTable.Columns.Add("periodunkid", System.Type.GetType("System.Int32")).DefaultValue = -1
            dtTable.Columns.Add("periodname", System.Type.GetType("System.String")).DefaultValue = ""
            dtTable.Columns.Add("wagesunkid", System.Type.GetType("System.Int32")).DefaultValue = -1
            dtTable.Columns.Add("gradegroupcode", System.Type.GetType("System.String")).DefaultValue = ""
            dtTable.Columns.Add("gradegroupunkid", System.Type.GetType("System.Int32")).DefaultValue = -1
            dtTable.Columns.Add("gradecode", System.Type.GetType("System.String")).DefaultValue = ""
            dtTable.Columns.Add("gradeunkid", System.Type.GetType("System.Int32")).DefaultValue = -1
            dtTable.Columns.Add("gradelevelcode", System.Type.GetType("System.String")).DefaultValue = ""
            dtTable.Columns.Add("gradelevelunkid", System.Type.GetType("System.Int32")).DefaultValue = -1
            dtTable.Columns.Add("minimum", System.Type.GetType("System.Decimal")).DefaultValue = 0
            dtTable.Columns.Add("midpoint", System.Type.GetType("System.Decimal")).DefaultValue = 0
            dtTable.Columns.Add("maximum", System.Type.GetType("System.Decimal")).DefaultValue = 0
            dtTable.Columns.Add("increment", System.Type.GetType("System.Decimal")).DefaultValue = 0
            dtTable.Columns.Add("isvoid", System.Type.GetType("System.Boolean")).DefaultValue = False
            dtTable.Columns.Add("voiduserunkid", System.Type.GetType("System.Int32")).DefaultValue = -1
            dtTable.Columns.Add("voiddatetime", System.Type.GetType("System.DateTime")).DefaultValue = Nothing
            dtTable.Columns.Add("voidreason", System.Type.GetType("System.String")).DefaultValue = ""
            dtTable.Columns.Add("ischecked", System.Type.GetType("System.Boolean")).DefaultValue = False
            dtTable.Columns.Add("rowtypeid", System.Type.GetType("System.Int32")).DefaultValue = -1
            dtTable.Columns.Add("issuccess", System.Type.GetType("System.Boolean")).DefaultValue = False
            dtTable.Columns.Add("remark", System.Type.GetType("System.String")).DefaultValue = ""

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetData", mstrModuleName)
        End Try
    End Sub

    Private Function IsValidate() As Boolean
        Try
            If CInt(cboPeriod.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "Please select Period. Period is mandatory information."), enMsgBoxStyle.Information)
                cboPeriod.Focus()
                Return False
            ElseIf CInt(cboGradeGroupCode.SelectedIndex) < 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 6, "Please select Grade Group Code. Grade Group Code is mandatory information."), enMsgBoxStyle.Information)
                cboPeriod.Focus()
                Return False
            ElseIf CInt(cboGradeCode.SelectedIndex) < 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 7, "Please select Grade Code. Grade Code is mandatory information."), enMsgBoxStyle.Information)
                cboPeriod.Focus()
                Return False
            ElseIf CInt(cboGradeLevelCode.SelectedIndex) < 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 8, "Please select Grade Level Code. Grade Level Code is mandatory information."), enMsgBoxStyle.Information)
                cboPeriod.Focus()
                Return False
            ElseIf CInt(cboMinimum.SelectedIndex) < 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 9, "Please select Minimum. Minimum is mandatory information."), enMsgBoxStyle.Information)
                cboPeriod.Focus()
                Return False
            ElseIf CInt(cboMaximum.SelectedIndex) < 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 11, "Please select Maximum. Maximum is mandatory information."), enMsgBoxStyle.Information)
                cboPeriod.Focus()
                Return False
            ElseIf CInt(cboIncrement.SelectedIndex) < 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 12, "Please select Increment. Increment is mandatory information."), enMsgBoxStyle.Information)
                cboPeriod.Focus()
                Return False
            End If


        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "IsValidate", mstrModuleName)
        End Try
        Return True
    End Function

#End Region

#Region " Button's Events "

    Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnCancel_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnOk_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnOk.Click
        Try
            If IsValidate() = False Then Exit Sub

            Dim dtData As DataTable = New DataView(dsList.Tables(0), "", "", DataViewRowState.CurrentRows).ToTable

            For Each dtRow As DataRow In dtData.Rows
                Dim dRow As DataRow
                dRow = dtTable.NewRow

                dRow.Item("periodunkid") = CInt(cboPeriod.SelectedValue)
                dRow.Item("periodname") = cboPeriod.Text.ToString

                '========================== To check blank data START ===============================================

                If IsDBNull(dtRow.Item(cboGradeGroupCode.Text)) = True OrElse dtRow.Item(cboGradeGroupCode.Text).ToString = "" Then
                    dRow.Item("gradegroupcode") = ""
                    dRow.Item("gradecode") = If(IsDBNull(dtRow.Item(cboGradeCode.Text)), "", dtRow.Item(cboGradeCode.Text))
                    dRow.Item("gradelevelcode") = If(IsDBNull(dtRow.Item(cboGradeLevelCode.Text)), "", dtRow.Item(cboGradeLevelCode.Text))
                    dRow.Item("minimum") = If(IsDBNull(dtRow.Item(cboMinimum.Text)), 0, CDec(dtRow.Item(cboMinimum.Text)))
                    dRow.Item("midpoint") = If(IsDBNull(dtRow.Item(cboMidPoint.Text)), 0, CDec(dtRow.Item(cboMidPoint.Text)))
                    dRow.Item("maximum") = If(IsDBNull(dtRow.Item(cboMaximum.Text)), 0, CDec(dtRow.Item(cboMaximum.Text)))
                    dRow.Item("increment") = If(IsDBNull(dtRow.Item(cboIncrement.Text)), 0, CDec(dtRow.Item(cboIncrement.Text)))
                    dRow.Item("rowtypeid") = 1
                    dRow.Item("issuccess") = False
                    dRow.Item("remark") = Language.getMessage(mstrModuleName, 13, "Grade Group Code is not given.")

                    dtTable.Rows.Add(dRow)
                    Continue For
                End If

                If IsDBNull(dtRow.Item(cboGradeCode.Text)) = True OrElse dtRow.Item(cboGradeCode.Text).ToString = "" Then
                    dRow.Item("gradegroupcode") = dtRow.Item(cboGradeGroupCode.Text)
                    dRow.Item("gradecode") = ""
                    dRow.Item("gradelevelcode") = If(IsDBNull(dtRow.Item(cboGradeLevelCode.Text)), "", dtRow.Item(cboGradeLevelCode.Text))
                    dRow.Item("minimum") = If(IsDBNull(dtRow.Item(cboMinimum.Text)), 0, CDec(dtRow.Item(cboMinimum.Text)))
                    dRow.Item("midpoint") = If(IsDBNull(dtRow.Item(cboMidPoint.Text)), 0, CDec(dtRow.Item(cboMidPoint.Text)))
                    dRow.Item("maximum") = If(IsDBNull(dtRow.Item(cboMaximum.Text)), 0, CDec(dtRow.Item(cboMaximum.Text)))
                    dRow.Item("increment") = If(IsDBNull(dtRow.Item(cboIncrement.Text)), 0, CDec(dtRow.Item(cboIncrement.Text)))
                    dRow.Item("rowtypeid") = 1
                    dRow.Item("issuccess") = False
                    dRow.Item("remark") = Language.getMessage(mstrModuleName, 14, "Grade Code is not given.")

                    dtTable.Rows.Add(dRow)
                    Continue For
                End If

                If IsDBNull(dtRow.Item(cboGradeLevelCode.Text)) = True OrElse dtRow.Item(cboGradeLevelCode.Text).ToString = "" Then
                    dRow.Item("gradegroupcode") = dtRow.Item(cboGradeGroupCode.Text)
                    dRow.Item("gradecode") = dtRow.Item(cboGradeCode.Text)
                    dRow.Item("gradelevelcode") = ""
                    dRow.Item("minimum") = If(IsDBNull(dtRow.Item(cboMinimum.Text)), 0, CDec(dtRow.Item(cboMinimum.Text)))
                    dRow.Item("midpoint") = If(IsDBNull(dtRow.Item(cboMidPoint.Text)), 0, CDec(dtRow.Item(cboMidPoint.Text)))
                    dRow.Item("maximum") = If(IsDBNull(dtRow.Item(cboMaximum.Text)), 0, CDec(dtRow.Item(cboMaximum.Text)))
                    dRow.Item("increment") = If(IsDBNull(dtRow.Item(cboIncrement.Text)), 0, CDec(dtRow.Item(cboIncrement.Text)))
                    dRow.Item("rowtypeid") = 1
                    dRow.Item("issuccess") = False
                    dRow.Item("remark") = Language.getMessage(mstrModuleName, 15, "Grade Code is not given.")

                    dtTable.Rows.Add(dRow)
                    Continue For
                End If

                If IsDBNull(dtRow.Item(cboMinimum.Text)) = True Then
                    dRow.Item("gradegroupcode") = dtRow.Item(cboGradeGroupCode.Text)
                    dRow.Item("gradecode") = dtRow.Item(cboGradeCode.Text)
                    dRow.Item("gradelevelcode") = dtRow.Item(cboGradeLevelCode.Text)
                    dRow.Item("minimum") = 0
                    dRow.Item("midpoint") = If(IsDBNull(dtRow.Item(cboMidPoint.Text)), 0, CDec(dtRow.Item(cboMidPoint.Text)))
                    dRow.Item("maximum") = If(IsDBNull(dtRow.Item(cboMaximum.Text)), 0, CDec(dtRow.Item(cboMaximum.Text)))
                    dRow.Item("increment") = If(IsDBNull(dtRow.Item(cboIncrement.Text)), 0, CDec(dtRow.Item(cboIncrement.Text)))
                    dRow.Item("rowtypeid") = 1
                    dRow.Item("issuccess") = False
                    dRow.Item("remark") = Language.getMessage(mstrModuleName, 16, "Minimum amount is not given.")

                    dtTable.Rows.Add(dRow)
                    Continue For

                ElseIf IsNumeric(dtRow.Item(cboMinimum.Text)) = False OrElse CDec(dtRow.Item(cboMinimum.Text)) < 0 Then
                    dRow.Item("gradegroupcode") = dtRow.Item(cboGradeGroupCode.Text)
                    dRow.Item("gradecode") = dtRow.Item(cboGradeCode.Text)
                    dRow.Item("gradelevelcode") = dtRow.Item(cboGradeLevelCode.Text)
                    dRow.Item("minimum") = CDec(dtRow.Item(cboMinimum.Text))
                    dRow.Item("midpoint") = If(IsDBNull(dtRow.Item(cboMidPoint.Text)), 0, CDec(dtRow.Item(cboMidPoint.Text)))
                    dRow.Item("maximum") = CDec(dtRow.Item(cboMaximum.Text))
                    dRow.Item("increment") = CDec(dtRow.Item(cboIncrement.Text))
                    dRow.Item("rowtypeid") = 1
                    dRow.Item("issuccess") = False
                    dRow.Item("remark") = Language.getMessage(mstrModuleName, 17, "Minimum amount must be a numeric value and cannot be negative.")

                    dtTable.Rows.Add(dRow)
                    Continue For
                End If

                If IsDBNull(dtRow.Item(cboMaximum.Text)) = True Then
                    dRow.Item("gradegroupcode") = dtRow.Item(cboGradeGroupCode.Text)
                    dRow.Item("gradecode") = dtRow.Item(cboGradeCode.Text)
                    dRow.Item("gradelevelcode") = dtRow.Item(cboGradeLevelCode.Text)
                    dRow.Item("minimum") = CDec(dtRow.Item(cboMinimum.Text))
                    dRow.Item("midpoint") = If(IsDBNull(dtRow.Item(cboMidPoint.Text)), 0, CDec(dtRow.Item(cboMidPoint.Text)))
                    dRow.Item("maximum") = 0
                    dRow.Item("increment") = If(IsDBNull(dtRow.Item(cboIncrement.Text)), 0, CDec(dtRow.Item(cboIncrement.Text)))
                    dRow.Item("rowtypeid") = 1
                    dRow.Item("issuccess") = False
                    dRow.Item("remark") = Language.getMessage(mstrModuleName, 18, "Maximum amount is not given.")

                    dtTable.Rows.Add(dRow)
                    Continue For

                ElseIf IsNumeric(dtRow.Item(cboMaximum.Text)) = False OrElse CDec(dtRow.Item(cboMaximum.Text)) < 0 Then
                    dRow.Item("gradegroupcode") = dtRow.Item(cboGradeGroupCode.Text)
                    dRow.Item("gradecode") = dtRow.Item(cboGradeCode.Text)
                    dRow.Item("gradelevelcode") = dtRow.Item(cboGradeLevelCode.Text)
                    dRow.Item("minimum") = CDec(dtRow.Item(cboMinimum.Text))
                    dRow.Item("midpoint") = If(IsDBNull(dtRow.Item(cboMidPoint.Text)), 0, CDec(dtRow.Item(cboMidPoint.Text)))
                    dRow.Item("maximum") = CDec(dtRow.Item(cboMaximum.Text))
                    dRow.Item("increment") = CDec(dtRow.Item(cboIncrement.Text))
                    dRow.Item("rowtypeid") = 1
                    dRow.Item("issuccess") = False
                    dRow.Item("remark") = Language.getMessage(mstrModuleName, 19, "Maximum amount must be a numeric value and cannot be negative.")

                    dtTable.Rows.Add(dRow)
                    Continue For

                ElseIf CDec(dtRow.Item(cboMaximum.Text)) <= 0 Then
                    dRow.Item("gradegroupcode") = dtRow.Item(cboGradeGroupCode.Text)
                    dRow.Item("gradecode") = dtRow.Item(cboGradeCode.Text)
                    dRow.Item("gradelevelcode") = dtRow.Item(cboGradeLevelCode.Text)
                    dRow.Item("minimum") = CDec(dtRow.Item(cboMinimum.Text))
                    dRow.Item("midpoint") = If(IsDBNull(dtRow.Item(cboMidPoint.Text)), 0, CDec(dtRow.Item(cboMidPoint.Text)))
                    dRow.Item("maximum") = CDec(dtRow.Item(cboMaximum.Text))
                    dRow.Item("increment") = CDec(dtRow.Item(cboIncrement.Text))
                    dRow.Item("rowtypeid") = 1
                    dRow.Item("issuccess") = False
                    dRow.Item("remark") = Language.getMessage(mstrModuleName, 20, "Maximum amount is mandatory information.")

                    dtTable.Rows.Add(dRow)
                    Continue For

                ElseIf CDec(dtRow.Item(cboMaximum.Text)) <= CDec(dtRow.Item(cboMinimum.Text)) Then
                    dRow.Item("gradegroupcode") = dtRow.Item(cboGradeGroupCode.Text)
                    dRow.Item("gradecode") = dtRow.Item(cboGradeCode.Text)
                    dRow.Item("gradelevelcode") = dtRow.Item(cboGradeLevelCode.Text)
                    dRow.Item("minimum") = CDec(dtRow.Item(cboMinimum.Text))
                    dRow.Item("midpoint") = If(IsDBNull(dtRow.Item(cboMidPoint.Text)), 0, CDec(dtRow.Item(cboMidPoint.Text)))
                    dRow.Item("maximum") = CDec(dtRow.Item(cboMaximum.Text))
                    dRow.Item("increment") = CDec(dtRow.Item(cboIncrement.Text))
                    dRow.Item("rowtypeid") = 1
                    dRow.Item("issuccess") = False
                    dRow.Item("remark") = Language.getMessage(mstrModuleName, 21, "Maximum amount should be greater than Minimum Amount.")

                    dtTable.Rows.Add(dRow)
                    Continue For
                End If

                If IsDBNull(dtRow.Item(cboIncrement.Text)) = True Then
                    dRow.Item("gradegroupcode") = dtRow.Item(cboGradeGroupCode.Text)
                    dRow.Item("gradecode") = dtRow.Item(cboGradeCode.Text)
                    dRow.Item("gradelevelcode") = dtRow.Item(cboGradeLevelCode.Text)
                    dRow.Item("minimum") = CDec(dtRow.Item(cboMinimum.Text))
                    dRow.Item("midpoint") = If(IsDBNull(dtRow.Item(cboMidPoint.Text)), 0, CDec(dtRow.Item(cboMidPoint.Text)))
                    dRow.Item("maximum") = CDec(dtRow.Item(cboMaximum.Text))
                    dRow.Item("increment") = 0
                    dRow.Item("rowtypeid") = 1
                    dRow.Item("issuccess") = False
                    dRow.Item("remark") = Language.getMessage(mstrModuleName, 22, "Increment amount is not given.")

                    dtTable.Rows.Add(dRow)
                    Continue For

                ElseIf IsNumeric(dtRow.Item(cboIncrement.Text)) = False OrElse CDec(dtRow.Item(cboIncrement.Text)) < 0 Then
                    dRow.Item("gradegroupcode") = dtRow.Item(cboGradeGroupCode.Text)
                    dRow.Item("gradecode") = dtRow.Item(cboGradeCode.Text)
                    dRow.Item("gradelevelcode") = dtRow.Item(cboGradeLevelCode.Text)
                    dRow.Item("minimum") = CDec(dtRow.Item(cboMinimum.Text))
                    dRow.Item("midpoint") = CDec(dtRow.Item(cboMidPoint.Text))
                    dRow.Item("maximum") = CDec(dtRow.Item(cboMaximum.Text))
                    dRow.Item("increment") = CDec(dtRow.Item(cboIncrement.Text))
                    dRow.Item("rowtypeid") = 1
                    dRow.Item("issuccess") = False
                    dRow.Item("remark") = Language.getMessage(mstrModuleName, 23, "Increment amount must be a numeric value and cannot be negative.")

                    dtTable.Rows.Add(dRow)
                    Continue For
                End If

                If IsDBNull(dtRow.Item(cboMidPoint.Text)) = True Then
                    dRow.Item("gradegroupcode") = dtRow.Item(cboGradeGroupCode.Text)
                    dRow.Item("gradecode") = dtRow.Item(cboGradeCode.Text)
                    dRow.Item("gradelevelcode") = dtRow.Item(cboGradeLevelCode.Text)
                    dRow.Item("minimum") = CDec(dtRow.Item(cboMinimum.Text))
                    dRow.Item("midpoint") = 0
                    dRow.Item("maximum") = CDec(dtRow.Item(cboMaximum.Text))
                    dRow.Item("increment") = CDec(dtRow.Item(cboIncrement.Text))
                    dRow.Item("rowtypeid") = 1
                    dRow.Item("issuccess") = False
                    dRow.Item("remark") = Language.getMessage(mstrModuleName, 24, "Mid Point amount cannot be blank.")

                    dtTable.Rows.Add(dRow)
                    Continue For

                ElseIf IsNumeric(dtRow.Item(cboMidPoint.Text)) = False OrElse CDec(dtRow.Item(cboMidPoint.Text)) < 0 Then
                    dRow.Item("gradegroupcode") = dtRow.Item(cboGradeGroupCode.Text)
                    dRow.Item("gradecode") = dtRow.Item(cboGradeCode.Text)
                    dRow.Item("gradelevelcode") = dtRow.Item(cboGradeLevelCode.Text)
                    dRow.Item("minimum") = CDec(dtRow.Item(cboMinimum.Text))
                    dRow.Item("midpoint") = CDec(dtRow.Item(cboMidPoint.Text))
                    dRow.Item("maximum") = CDec(dtRow.Item(cboMaximum.Text))
                    dRow.Item("increment") = CDec(dtRow.Item(cboIncrement.Text))
                    dRow.Item("rowtypeid") = 1
                    dRow.Item("issuccess") = False
                    dRow.Item("remark") = Language.getMessage(mstrModuleName, 25, "Mid Point amount must be a numeric value and cannot be negative.")

                    dtTable.Rows.Add(dRow)
                    Continue For
                End If
                '=============================== To check blank data END ===================================================

                '=============================== To check Calculation START ===================================================

                If CDec(dtRow.Item(cboIncrement.Text)) > CDec(dtRow.Item(cboMaximum.Text)) Then
                    dRow.Item("gradegroupcode") = dtRow.Item(cboGradeGroupCode.Text)
                    dRow.Item("gradecode") = dtRow.Item(cboGradeCode.Text)
                    dRow.Item("gradelevelcode") = dtRow.Item(cboGradeLevelCode.Text)
                    dRow.Item("minimum") = CDec(dtRow.Item(cboMinimum.Text))
                    dRow.Item("midpoint") = CDec(dtRow.Item(cboMidPoint.Text))
                    dRow.Item("maximum") = CDec(dtRow.Item(cboMaximum.Text))
                    dRow.Item("increment") = CDec(dtRow.Item(cboIncrement.Text))
                    dRow.Item("rowtypeid") = 1
                    dRow.Item("issuccess") = False
                    dRow.Item("remark") = Language.getMessage(mstrModuleName, 26, "Increment amount must not be greater than Maximum.")

                    dtTable.Rows.Add(dRow)
                    Continue For
                End If

                If CDec(CDec(dtRow.Item(cboIncrement.Text)) + CDec(dtRow.Item(cboMinimum.Text))) > CDec(dtRow.Item(cboMaximum.Text)) Then
                    dRow.Item("gradegroupcode") = dtRow.Item(cboGradeGroupCode.Text)
                    dRow.Item("gradecode") = dtRow.Item(cboGradeCode.Text)
                    dRow.Item("gradelevelcode") = dtRow.Item(cboGradeLevelCode.Text)
                    dRow.Item("minimum") = CDec(dtRow.Item(cboMinimum.Text))
                    dRow.Item("midpoint") = CDec(dtRow.Item(cboMidPoint.Text))
                    dRow.Item("maximum") = CDec(dtRow.Item(cboMaximum.Text))
                    dRow.Item("increment") = CDec(dtRow.Item(cboIncrement.Text))
                    dRow.Item("rowtypeid") = 1
                    dRow.Item("issuccess") = False
                    dRow.Item("remark") = Language.getMessage(mstrModuleName, 27, "Incremented amount should not exceed maximum limit.")

                    dtTable.Rows.Add(dRow)
                    Continue For
                End If

                If CDec(dtRow.Item(cboMidPoint.Text)) < CDec(dtRow.Item(cboMinimum.Text)) AndAlso CDec(dtRow.Item(cboMidPoint.Text)) > CDec(dtRow.Item(cboMaximum.Text)) Then
                    dRow.Item("gradegroupcode") = dtRow.Item(cboGradeGroupCode.Text)
                    dRow.Item("gradecode") = dtRow.Item(cboGradeCode.Text)
                    dRow.Item("gradelevelcode") = dtRow.Item(cboGradeLevelCode.Text)
                    dRow.Item("minimum") = CDec(dtRow.Item(cboMinimum.Text))
                    dRow.Item("midpoint") = CDec(dtRow.Item(cboMidPoint.Text))
                    dRow.Item("maximum") = CDec(dtRow.Item(cboMaximum.Text))
                    dRow.Item("increment") = CDec(dtRow.Item(cboIncrement.Text))
                    dRow.Item("rowtypeid") = 1
                    dRow.Item("issuccess") = False
                    dRow.Item("remark") = Language.getMessage(mstrModuleName, 28, "Mid Point amount must not be less than Minimum amount and greater then Maximum amount.")

                    dtTable.Rows.Add(dRow)
                    Continue For
                End If
                '=============================== To check Calculation END ===================================================

                '================================== To check code exists START =============================================================

                Dim objGradeGroup As New clsGradeGroup
                Dim objGrade As New clsGrade
                Dim objGradeLevel As New clsGradeLevel

                Dim intGradeGroupunkid As Integer = 0
                Dim intGradeunkid As Integer = 0
                Dim intGradeLevelunkid As Integer = 0
                Dim strGradeLevelName As String = ""

                If objGradeGroup.isExist(dtRow.Item(cboGradeGroupCode.Text).ToString, , , intGradeGroupunkid) = False Then
                    dRow.Item("gradegroupcode") = dtRow.Item(cboGradeGroupCode.Text)
                    dRow.Item("gradecode") = dtRow.Item(cboGradeCode.Text)
                    dRow.Item("gradelevelcode") = dtRow.Item(cboGradeLevelCode.Text)
                    dRow.Item("minimum") = CDec(dtRow.Item(cboMinimum.Text))
                    dRow.Item("midpoint") = CDec(dtRow.Item(cboMidPoint.Text))
                    dRow.Item("maximum") = CDec(dtRow.Item(cboMaximum.Text))
                    dRow.Item("increment") = CDec(dtRow.Item(cboIncrement.Text))
                    dRow.Item("rowtypeid") = 1
                    dRow.Item("issuccess") = False
                    dRow.Item("remark") = Language.getMessage(mstrModuleName, 29, "Grade Group does not exist.")

                    dtTable.Rows.Add(dRow)
                    Continue For
                End If

                If objGrade.isExist(dtRow.Item(cboGradeCode.Text).ToString, , , intGradeunkid) = False Then
                    dRow.Item("gradegroupcode") = dtRow.Item(cboGradeGroupCode.Text)
                    dRow.Item("gradecode") = dtRow.Item(cboGradeCode.Text)
                    dRow.Item("gradelevelcode") = dtRow.Item(cboGradeLevelCode.Text)
                    dRow.Item("minimum") = CDec(dtRow.Item(cboMinimum.Text))
                    dRow.Item("midpoint") = CDec(dtRow.Item(cboMidPoint.Text))
                    dRow.Item("maximum") = CDec(dtRow.Item(cboMaximum.Text))
                    dRow.Item("increment") = CDec(dtRow.Item(cboIncrement.Text))
                    dRow.Item("rowtypeid") = 1
                    dRow.Item("issuccess") = False
                    dRow.Item("remark") = Language.getMessage(mstrModuleName, 30, "Grade Code does not exist.")

                    dtTable.Rows.Add(dRow)
                    Continue For
                End If

                If objGradeLevel.isCodeExits(dtRow.Item(cboGradeLevelCode.Text).ToString, , strGradeLevelName, intGradeLevelunkid) = False Then
                    dRow.Item("gradegroupcode") = dtRow.Item(cboGradeGroupCode.Text)
                    dRow.Item("gradecode") = dtRow.Item(cboGradeCode.Text)
                    dRow.Item("gradelevelcode") = dtRow.Item(cboGradeLevelCode.Text)
                    dRow.Item("minimum") = CDec(dtRow.Item(cboMinimum.Text))
                    dRow.Item("midpoint") = CDec(dtRow.Item(cboMidPoint.Text))
                    dRow.Item("maximum") = CDec(dtRow.Item(cboMaximum.Text))
                    dRow.Item("increment") = CDec(dtRow.Item(cboIncrement.Text))
                    dRow.Item("rowtypeid") = 1
                    dRow.Item("issuccess") = False
                    dRow.Item("remark") = Language.getMessage(mstrModuleName, 31, "Grade Level Code does not exist.")

                    dtTable.Rows.Add(dRow)
                    Continue For
                End If

                '================================== To check code exists END =============================================================

                '================================== To check Valid Entry START =============================================================

                Dim ds As New DataSet
                Dim objWages As New clsWages
                Dim objWagesTran As New clsWagesTran

                ds = objGrade.GetList("List", , "hrgrade_master.gradeunkid = " & intGradeunkid & " AND hrgrade_master.gradegroupunkid = " & intGradeGroupunkid & " ")

                If ds.Tables("List").Rows.Count <= 0 Then
                    dRow.Item("gradegroupcode") = dtRow.Item(cboGradeGroupCode.Text)
                    dRow.Item("gradecode") = dtRow.Item(cboGradeCode.Text)
                    dRow.Item("gradelevelcode") = dtRow.Item(cboGradeLevelCode.Text)
                    dRow.Item("minimum") = CDec(dtRow.Item(cboMinimum.Text))
                    dRow.Item("midpoint") = CDec(dtRow.Item(cboMidPoint.Text))
                    dRow.Item("maximum") = CDec(dtRow.Item(cboMaximum.Text))
                    dRow.Item("increment") = CDec(dtRow.Item(cboIncrement.Text))
                    dRow.Item("rowtypeid") = 1
                    dRow.Item("issuccess") = False
                    dRow.Item("remark") = Language.getMessage(mstrModuleName, 32, "Grade is not linked with given Grade Group.")
                End If

                If objGradeLevel.isExist(intGradeunkid, strGradeLevelName, intGradeGroupunkid, , intGradeLevelunkid) = False Then
                    dRow.Item("gradegroupcode") = dtRow.Item(cboGradeGroupCode.Text)
                    dRow.Item("gradecode") = dtRow.Item(cboGradeCode.Text)
                    dRow.Item("gradelevelcode") = dtRow.Item(cboGradeLevelCode.Text)
                    dRow.Item("minimum") = CDec(dtRow.Item(cboMinimum.Text))
                    dRow.Item("midpoint") = CDec(dtRow.Item(cboMidPoint.Text))
                    dRow.Item("maximum") = CDec(dtRow.Item(cboMaximum.Text))
                    dRow.Item("increment") = CDec(dtRow.Item(cboIncrement.Text))
                    dRow.Item("rowtypeid") = 1
                    dRow.Item("issuccess") = False
                    dRow.Item("remark") = Language.getMessage(mstrModuleName, 33, "Grade Level is not linked with given Grade Group and Grade.")

                    dtTable.Rows.Add(dRow)
                    Continue For
                End If

                If objWagesTran.isExistByPeriodByLevel(mintPeriodunkid, intGradeLevelunkid) = True Then
                    dRow.Item("gradegroupcode") = dtRow.Item(cboGradeGroupCode.Text)
                    dRow.Item("gradecode") = dtRow.Item(cboGradeCode.Text)
                    dRow.Item("gradelevelcode") = dtRow.Item(cboGradeLevelCode.Text)
                    dRow.Item("minimum") = CDec(dtRow.Item(cboMinimum.Text))
                    dRow.Item("midpoint") = CDec(dtRow.Item(cboMidPoint.Text))
                    dRow.Item("maximum") = CDec(dtRow.Item(cboMaximum.Text))
                    dRow.Item("increment") = CDec(dtRow.Item(cboIncrement.Text))
                    dRow.Item("rowtypeid") = 1
                    dRow.Item("issuccess") = False
                    dRow.Item("remark") = Language.getMessage(mstrModuleName, 34, "This Wages is already present for the given Grade Level and selected Period.")

                    dtTable.Rows.Add(dRow)
                    Continue For
                End If
                '================================== To check Valid Entry END =============================================================

                dRow.Item("gradegroupcode") = dtRow.Item(cboGradeGroupCode.Text)
                dRow.Item("gradegroupunkid") = intGradeGroupunkid
                dRow.Item("gradecode") = dtRow.Item(cboGradeCode.Text)
                dRow.Item("gradeunkid") = intGradeunkid
                dRow.Item("gradelevelcode") = dtRow.Item(cboGradeLevelCode.Text)
                dRow.Item("gradelevelunkid") = intGradeLevelunkid
                dRow.Item("minimum") = CDec(dtRow.Item(cboMinimum.Text))
                dRow.Item("midpoint") = CDec(dtRow.Item(cboMidPoint.Text))
                dRow.Item("maximum") = CDec(dtRow.Item(cboMaximum.Text))
                dRow.Item("increment") = CDec(dtRow.Item(cboIncrement.Text))
                'dRow.Item("isvoid") = False
                'dRow.Item("voiduserunkid") = -1
                'dRow.Item("voiddatetime") = Nothing
                'dRow.Item("voidreason") = ""
                dRow.Item("rowtypeid") = 0
                dRow.Item("issuccess") = True
                dRow.Item("remark") = Language.getMessage(mstrModuleName, 35, "Success.")

                dtTable.Rows.Add(dRow)

            Next

            If dtTable.Rows.Count > 0 Then
                mintPeriodunkid = CInt(cboPeriod.SelectedValue)
                mblnCancel = False
                Me.Close()
            Else
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 36, "Sorry, You cannot import this file. Reason : There is no transaction to import in this file."), enMsgBoxStyle.Exclamation)
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnOk_Click", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " ComboBox's Events "

    Private Sub cboGradeGroupCode_Validating(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles cboGradeGroupCode.Validating, _
                                                                                                                               cboGradeCode.Validating, _
                                                                                                                               cboGradeLevelCode.Validating, _
                                                                                                                               cboMinimum.Validating, _
                                                                                                                               cboMidPoint.Validating, _
                                                                                                                               cboMaximum.Validating, _
                                                                                                                               cboIncrement.Validating
        Try
            Dim cbo As ComboBox = CType(sender, ComboBox)

            Dim list As IEnumerable(Of ComboBox) = gbFieldMapping.Controls.OfType(Of ComboBox)().Where(Function(p) p.Name <> cbo.Name AndAlso p.Name <> cboPeriod.Name _
                                                                                                       AndAlso CInt(p.SelectedIndex) = CInt(cbo.SelectedIndex))
            If list.Count > 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 10, "Sorry, This column is already mapped."))
                cbo.SelectedIndex = -1
                Exit Sub
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboGradeGroupCode_Validating", mstrModuleName)
        End Try
    End Sub

    Private Sub cboPeriod_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboPeriod.SelectedIndexChanged
        Try
            Dim objPeriod As New clscommom_period_Tran
            If CInt(cboPeriod.SelectedValue) > 0 Then
                mintPeriodunkid = CInt(cboPeriod.SelectedValue)
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboPeriod_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Link button's Events "
    Private Sub lnkAutoMap_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkAutoMap.Click
        Try
            Dim lstCtrl As List(Of Control) = (From cb In gbFieldMapping.Controls.Cast(Of Control)() Where (TypeOf cb Is ComboBox AndAlso CType(cb, ComboBox).DataSource Is Nothing) Select (cb)).ToList

            For Each ctrl In lstCtrl
                Dim strName As String = CType(ctrl, ComboBox).Name.ToUpper.Substring(3)

                Dim dCol As DataColumn = (From p In dsList.Tables(0).Columns.Cast(Of DataColumn)() Where (p.ColumnName.ToString.Replace(" ", "").ToUpper Like "*" & strName & "*") Select (p)).FirstOrDefault

                If dCol IsNot Nothing Then
                    Dim strCtrlName As String = ctrl.Name
                    Dim combos As List(Of ComboBox) = (From cb In gbFieldMapping.Controls.Cast(Of Control)() Where (TypeOf cb Is ComboBox AndAlso CType(cb, ComboBox).DataSource Is Nothing AndAlso CType(cb, ComboBox).Name <> strCtrlName AndAlso CType(cb, ComboBox).SelectedIndex = dCol.Ordinal) Select (CType(cb, ComboBox))).ToList
                    If combos.Count > 0 Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Sorry! This Column") & "  " & dCol.ColumnName & " " & _
                                        Language.getMessage(mstrModuleName, 2, "is already mapped with") & " " & combos(0).Name.Substring(3) & _
                                        Language.getMessage(mstrModuleName, 3, " Fields From File.") & vbCrLf & _
                                        Language.getMessage(mstrModuleName, 4, "Please select different Field."), enMsgBoxStyle.Information)
                        CType(ctrl, ComboBox).SelectedIndex = -1
                    Else
                        CType(ctrl, ComboBox).SelectedIndex = dCol.Ordinal
                    End If
                End If
            Next

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lnkAutoMap_Click", mstrModuleName)
        End Try
    End Sub
#End Region

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Private Sub OtherSettings()
        Try
            Me.SuspendLayout()
           
            Call SetLanguage()

            Me.gbFieldMapping.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbFieldMapping.ForeColor = GUI._eZeeContainerHeaderForeColor


            Me.btnCancel.GradientBackColor = GUI._ButttonBackColor
            Me.btnCancel.GradientForeColor = GUI._ButttonFontColor

            Me.btnOk.GradientBackColor = GUI._ButttonBackColor
            Me.btnOk.GradientForeColor = GUI._ButttonFontColor


            Me.ResumeLayout()
        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "OtherSettings", mstrModuleName)
        End Try
    End Sub


    Private Sub SetLanguage()
        Try
            Me.Text = Language._Object.getCaption(Me.Name, Me.Text)

            Me.gbFieldMapping.Text = Language._Object.getCaption(Me.gbFieldMapping.Name, Me.gbFieldMapping.Text)
            Me.lblPeriod.Text = Language._Object.getCaption(Me.lblPeriod.Name, Me.lblPeriod.Text)
            Me.lblMaximum.Text = Language._Object.getCaption(Me.lblMaximum.Name, Me.lblMaximum.Text)
            Me.lblMidpoint.Text = Language._Object.getCaption(Me.lblMidpoint.Name, Me.lblMidpoint.Text)
            Me.lblMinimum.Text = Language._Object.getCaption(Me.lblMinimum.Name, Me.lblMinimum.Text)
            Me.lblGradeLevelCode.Text = Language._Object.getCaption(Me.lblGradeLevelCode.Name, Me.lblGradeLevelCode.Text)
            Me.lblGradeCode.Text = Language._Object.getCaption(Me.lblGradeCode.Name, Me.lblGradeCode.Text)
            Me.lblGradeGroupCode.Text = Language._Object.getCaption(Me.lblGradeGroupCode.Name, Me.lblGradeGroupCode.Text)
            Me.btnCancel.Text = Language._Object.getCaption(Me.btnCancel.Name, Me.btnCancel.Text)
            Me.btnOk.Text = Language._Object.getCaption(Me.btnOk.Name, Me.btnOk.Text)
            Me.lblIncrement.Text = Language._Object.getCaption(Me.lblIncrement.Name, Me.lblIncrement.Text)
            Me.lblMessage.Text = Language._Object.getCaption(Me.lblMessage.Name, Me.lblMessage.Text)
            Me.lnkAutoMap.Text = Language._Object.getCaption(Me.lnkAutoMap.Name, Me.lnkAutoMap.Text)

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
        End Try
    End Sub


    Private Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Sorry! This Column")
            Language.setMessage(mstrModuleName, 2, "is already mapped with")
            Language.setMessage(mstrModuleName, 3, " Fields From File.")
            Language.setMessage(mstrModuleName, 4, "Please select different Field.")
            Language.setMessage(mstrModuleName, 5, "Please select Period. Period is mandatory information.")
            Language.setMessage(mstrModuleName, 6, "Please select Grade Group Code. Grade Group Code is mandatory information.")
            Language.setMessage(mstrModuleName, 7, "Please select Grade Code. Grade Code is mandatory information.")
            Language.setMessage(mstrModuleName, 8, "Please select Grade Level Code. Grade Level Code is mandatory information.")
            Language.setMessage(mstrModuleName, 9, "Please select Minimum. Minimum is mandatory information.")
            Language.setMessage(mstrModuleName, 10, "Sorry, This column is already mapped.")
            Language.setMessage(mstrModuleName, 11, "Please select Maximum. Maximum is mandatory information.")
            Language.setMessage(mstrModuleName, 12, "Please select Increment. Increment is mandatory information.")
            Language.setMessage(mstrModuleName, 13, "Grade Group Code is not given.")
            Language.setMessage(mstrModuleName, 14, "Grade Code is not given.")
            Language.setMessage(mstrModuleName, 15, "Grade Code is not given.")
            Language.setMessage(mstrModuleName, 16, "Minimum amount is not given.")
            Language.setMessage(mstrModuleName, 17, "Minimum amount must be a numeric value and cannot be negative.")
            Language.setMessage(mstrModuleName, 18, "Maximum amount is not given.")
            Language.setMessage(mstrModuleName, 19, "Maximum amount must be a numeric value and cannot be negative.")
            Language.setMessage(mstrModuleName, 20, "Maximum amount is mandatory information.")
            Language.setMessage(mstrModuleName, 21, "Maximum amount should be greater than Minimum Amount.")
            Language.setMessage(mstrModuleName, 22, "Increment amount is not given.")
            Language.setMessage(mstrModuleName, 23, "Increment amount must be a numeric value and cannot be negative.")
            Language.setMessage(mstrModuleName, 24, "Mid Point amount cannot be blank.")
            Language.setMessage(mstrModuleName, 25, "Mid Point amount must be a numeric value and cannot be negative.")
            Language.setMessage(mstrModuleName, 26, "Increment amount must not be greater than Maximum.")
            Language.setMessage(mstrModuleName, 27, "Incremented amount should not exceed maximum limit.")
            Language.setMessage(mstrModuleName, 28, "Mid Point amount must not be less than Minimum amount and greater then Maximum amount.")
            Language.setMessage(mstrModuleName, 29, "Grade Group does not exist.")
            Language.setMessage(mstrModuleName, 30, "Grade Code does not exist.")
            Language.setMessage(mstrModuleName, 31, "Grade Level Code does not exist.")
            Language.setMessage(mstrModuleName, 32, "Grade is not linked with given Grade Group.")
            Language.setMessage(mstrModuleName, 33, "Grade Level is not linked with given Grade Group and Grade.")
            Language.setMessage(mstrModuleName, 34, "This Wages is already present for the given Grade Level and selected Period.")
            Language.setMessage(mstrModuleName, 35, "Success.")
            Language.setMessage(mstrModuleName, 36, "Sorry, You cannot import this file. Reason : There is no transaction to import in this file.")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class