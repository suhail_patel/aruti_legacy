﻿Option Strict On

Imports eZeeCommonLib
Imports Aruti.Data
'Last Message Index = 3

Public Class frmTimesheetApproversList

#Region "Private Variable"

    Private objApprover As clstsapprover_master
    Private ReadOnly mstrModuleName As String = "frmTimesheetApproversList"
    Private mintUserMappunkid As Integer = -1

#End Region

#Region "Form's Event"

    Private Sub frmTimesheetApproversList_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        objApprover = New clstsapprover_master
        Try

            Call Set_Logo(Me, gApplicationType)
            Language.setLanguage(Me.Name)
            Call OtherSettings()
            FillCombo()
            Call SetVisibility()
            If lvApproverList.Items.Count > 0 Then lvApproverList.Items(0).Selected = True
            lvApproverList.Select()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmTimesheetApproversList_Load", mstrModuleName)
        End Try
    End Sub

    Private Sub frmTimesheetApproversList_KeyDown(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles MyBase.KeyDown
        Try
            If e.KeyCode = Keys.Delete Then
                Call btnDelete.PerformClick()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmTimesheetApproversList_KeyDown", mstrModuleName)
        End Try
    End Sub

    Private Sub frmTimesheetApproversList_FormClosed(ByVal sender As System.Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles MyBase.FormClosed
        objApprover = Nothing
    End Sub

    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clstsapprover_master.SetMessages()
            clstsapprover_Tran.SetMessages()
            objfrm._Other_ModuleNames = "clstsapprover_master,clstsapprover_Tran"
            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub

#End Region

#Region "Button's Event"

    Private Sub btnNew_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNew.Click
        Try
            Dim ObjFrm As New frmTimesheetApprover_AddEdit
            If ObjFrm.displayDialog(enAction.ADD_CONTINUE, -1) Then
                fillList()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnNew_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnEdit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEdit.Click
        If lvApproverList.SelectedItems.Count < 1 Then
            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please select Approver from the list to perform further operation on it."), enMsgBoxStyle.Information) '?1
            lvApproverList.Select()
            Exit Sub
        End If
        Dim objfrmLeaveApprover_AddEdit As New frmTimesheetApprover_AddEdit
        Try
            Dim intSelectedIndex As Integer
            intSelectedIndex = lvApproverList.SelectedItems(0).Index
            If objfrmLeaveApprover_AddEdit.displayDialog(enAction.EDIT_ONE, CInt(lvApproverList.SelectedItems(0).Tag)) Then
                Call fillList()
            End If
            objfrmLeaveApprover_AddEdit = Nothing

            lvApproverList.Items(intSelectedIndex).Selected = True
            lvApproverList.EnsureVisible(intSelectedIndex)
            lvApproverList.Select()
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "btnEdit_Click", mstrModuleName)
        Finally
            If objfrmLeaveApprover_AddEdit IsNot Nothing Then objfrmLeaveApprover_AddEdit.Dispose()
        End Try
    End Sub

    Private Sub btnDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDelete.Click
        If lvApproverList.SelectedItems.Count < 1 Then
            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please select Approver from the list to perform further operation on it."), enMsgBoxStyle.Information) '?1
            lvApproverList.Select()
            Exit Sub
        End If
        If objApprover.isUsed(CInt(lvApproverList.SelectedItems(0).Tag)) Then
            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Sorry, You cannot delete this Approver. Reason: This Approver is in use."), enMsgBoxStyle.Information) '?2
            lvApproverList.Select()
            Exit Sub
        End If
        Try
            Dim intSelectedIndex As Integer
            intSelectedIndex = lvApproverList.SelectedItems(0).Index

            If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Are you sure you want to delete this Approver?"), CType(enMsgBoxStyle.Question + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.Yes Then
                Dim frm As New frmReasonSelection
                Dim mstrVoidReason As String = String.Empty
                frm.displayDialog(enVoidCategoryType.PAYROLL, mstrVoidReason)
                If mstrVoidReason.Length <= 0 Then
                    Exit Sub
                Else
                    objApprover._Voidreason = mstrVoidReason
                End If
                frm = Nothing
                objApprover._Voiduserunkid = User._Object._Userunkid

                'S.SANDEEP [28-May-2018] -- START
                'ISSUE/ENHANCEMENT : {Audit Trails} 
                objApprover._FormName = mstrModuleName
                objApprover._LoginEmployeeunkid = 0
                objApprover._ClientIP = getIP()
                objApprover._HostName = getHostName()
                objApprover._FromWeb = False
                objApprover._AuditUserId = User._Object._Userunkid
objApprover._CompanyUnkid = Company._Object._Companyunkid
                objApprover._AuditDate = ConfigParameter._Object._CurrentDateAndTime
                'S.SANDEEP [28-May-2018] -- END

                objApprover.Delete(CInt(lvApproverList.SelectedItems(0).Tag), eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate))
                lvApproverList.SelectedItems(0).Remove()

                If lvApproverList.Items.Count <= 0 Then
                    Exit Try
                End If

                If lvApproverList.Items.Count = intSelectedIndex Then
                    intSelectedIndex = lvApproverList.Items.Count - 1
                    lvApproverList.Items(intSelectedIndex).Selected = True
                    lvApproverList.EnsureVisible(intSelectedIndex)
                ElseIf lvApproverList.Items.Count <> 0 Then
                    lvApproverList.Items(intSelectedIndex).Selected = True
                    lvApproverList.EnsureVisible(intSelectedIndex)
                End If
            End If
            lvApproverList.Select()
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "btnDelete_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Me.Close()
    End Sub

    Private Sub objbtnSearchApprover_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchApprover.Click
        Dim objfrm As New frmCommonSearch
        Dim dtList As DataTable
        Try
            dtList = CType(cboApprover.DataSource, DataTable)
            With cboApprover
                objfrm.DataSource = dtList
                objfrm.ValueMember = .ValueMember
                objfrm.DisplayMember = .DisplayMember
                If objfrm.DisplayDialog Then
                    .SelectedValue = objfrm.SelectedValue
                End If
                .Focus()
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchApprover_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnSearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearch.Click
        Try
            fillList()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearch_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnReset.Click
        Try
            cboLevel.SelectedIndex = 0
            cboApprover.SelectedIndex = 0
            cboStatus.SelectedValue = 1
            fillList()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnReset_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnSearchLevel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchLevel.Click
        Dim objfrm As New frmCommonSearch
        Dim dtList As DataTable
        Try
            dtList = CType(cboLevel.DataSource, DataTable)
            With cboLevel
                objfrm.DataSource = dtList
                objfrm.ValueMember = .ValueMember
                objfrm.DisplayMember = .DisplayMember
                If objfrm.DisplayDialog Then
                    .SelectedValue = objfrm.SelectedValue
                End If
                .Focus()
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchLevel_Click", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Private Methods "

    Private Sub fillList()
        Dim dsApproverList As New DataSet
        Dim strSearching As String = ""
        Try

            'Pinkal (03-May-2017) -- Start
            'Enhancement - Working On Implementing privileges in Budget Timesheet Module.
            If User._Object.Privilege._AllowToViewBudgetTimesheetApproverList = False Then Exit Sub
            'Pinkal (03-May-2017) -- End

            If CInt(cboLevel.SelectedValue) > 0 Then
                strSearching &= "AND tsapprover_master.tslevelunkid = " & CInt(cboLevel.SelectedValue)
            End If

            If CInt(cboApprover.SelectedValue) > 0 Then
                strSearching &= "AND tsapprover_master.employeeapproverunkid = " & CInt(cboApprover.SelectedValue)
            End If



            'S.SANDEEP [28-May-2018] -- START
            'ISSUE/ENHANCEMENT : {Audit Trails} 
            objApprover._FormName = mstrModuleName
            objApprover._LoginEmployeeunkid = 0
            objApprover._ClientIP = getIP()
            objApprover._HostName = getHostName()
            objApprover._FromWeb = False
            objApprover._AuditUserId = User._Object._Userunkid
objApprover._CompanyUnkid = Company._Object._Companyunkid
            objApprover._AuditDate = ConfigParameter._Object._CurrentDateAndTime
            'S.SANDEEP [28-May-2018] -- END

            dsApproverList = objApprover.GetList("List", FinancialYear._Object._DatabaseName, User._Object._Userunkid _
                                                                          , FinancialYear._Object._YearUnkid, Company._Object._Companyunkid _
                                                                          , ConfigParameter._Object._EmployeeAsOnDate, ConfigParameter._Object._UserAccessModeSetting _
                                                                          , True, True, CBool(IIf(CInt(cboStatus.SelectedValue) = 1, True, False)), False, -1, Nothing _
                                                                          , strSearching, "")
            Dim lvItem As ListViewItem

            lvApproverList.Items.Clear()
            For Each drRow As DataRow In dsApproverList.Tables(0).Rows
                lvItem = New ListViewItem
                lvItem.Text = drRow("name").ToString
                lvItem.Tag = drRow("tsapproverunkid")
                lvItem.SubItems.Add(drRow("levelname").ToString)
                lvItem.SubItems.Add(drRow("departmentname").ToString())
                lvItem.SubItems.Add(drRow("jobname").ToString())
                lvItem.SubItems.Add(drRow("ExAppr").ToString())
                lvItem.SubItems.Add(drRow("usermapped").ToString())
                lvApproverList.Items.Add(lvItem)
            Next

            lvApproverList.GroupingColumn = colhLevel
            lvApproverList.SortBy(colhLevel.Index, SortOrder.Ascending)
            lvApproverList.DisplayGroups(True)

            If lvApproverList.Items.Count > 16 Then
                colhJob.Width = 160 - 20
            Else
                colhJob.Width = 160
            End If

      
            If CInt(cboStatus.SelectedValue) = 1 Then
                Call SetVisibility()
            Else
                btnEdit.Enabled = False
                btnDelete.Enabled = False
            End If

        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "fillList", mstrModuleName)
        Finally
            dsApproverList.Dispose()
        End Try

    End Sub

    Private Sub SetVisibility()
        Try
            'Pinkal (03-May-2017) -- Start
            'Enhancement - Working On Implementing privileges in Budget Timesheet Module.
            btnNew.Enabled = User._Object.Privilege._AllowToAddBudgetTimesheetApprover
            btnEdit.Enabled = User._Object.Privilege._AllowToEditBudgetTimesheetApprover
            btnDelete.Enabled = User._Object.Privilege._AllowToDeleteBudgetTimesheetApprover
            mnuActiveApprover.Enabled = User._Object.Privilege._AllowToSetBudgetTimesheetApproverAsActive
            mnuInActiveApprover.Enabled = User._Object.Privilege._AllowToSetBudgetTimesheetApproverAsInActive
            'Pinkal (03-May-2017) -- End
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetVisibility", mstrModuleName)
        End Try

    End Sub

    Private Sub FillCombo()
        Try

            Dim objLevel As New clstsapproverlevel_master
            Dim dsList As DataSet = objLevel.getListForCombo("Level", True)
            cboLevel.DisplayMember = "name"
            cboLevel.ValueMember = "tslevelunkid"
            cboLevel.DataSource = dsList.Tables(0)

            Dim objMaster As New clsMasterData
            Dim dsCombo As DataSet = objMaster.getComboListTranHeadActiveInActive("List", False)
            With cboStatus
                .ValueMember = "Id"
                .DisplayMember = "Name"
                .DataSource = dsCombo.Tables("List")
                .SelectedValue = 1
            End With

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        End Try
    End Sub

#End Region

#Region "ListView Event"

    Private Sub lvApproverList_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lvApproverList.SelectedIndexChanged
        Dim blnflag As Boolean
        Try
            If lvApproverList.SelectedItems.Count = 0 Then Exit Sub
            Dim objUserMapping As New clsapprover_Usermapping
            blnflag = objUserMapping.isExist(enUserType.Approver, CInt(lvApproverList.SelectedItems(0).Tag))

            If blnflag Then

                Dim dsFill As DataSet = objUserMapping.GetList("List")
                Dim dtFill As DataTable = New DataView(dsFill.Tables("List"), "approverunkid=" & CInt(lvApproverList.SelectedItems(0).Tag) & " AND usertypeid =  " & enUserType.Approver, "", DataViewRowState.CurrentRows).ToTable
                If dtFill.Rows.Count > 0 Then
                    mintUserMappunkid = CInt(dtFill.Rows(0)("mappingunkid"))
                Else
                    mintUserMappunkid = 0
                End If
            Else
                mintUserMappunkid = 0
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lvApproverList_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

#End Region

#Region "Menu Event"

    Private Sub mnuInActiveApprover_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuInActiveApprover.Click
        Try
            If lvApproverList.SelectedItems.Count = 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please select Approver from the list to perform further operation on it."), enMsgBoxStyle.Information) '?1
                lvApproverList.Select()
                Exit Sub
            End If

            'Dim objLeavForm As New clsleaveform
            'If objLeavForm.GetApproverPendingLeaveFormCount(CInt(lvApproverList.SelectedItems(0).Tag), "") > 0 Then
            '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "You cannot inactive this approver.Reason :This Approver has Pending Leave Application Form(s)."), enMsgBoxStyle.Information)
            '    Exit Sub
            'End If
            'objLeavForm = Nothing


            'S.SANDEEP [28-May-2018] -- START
            'ISSUE/ENHANCEMENT : {Audit Trails} 
            objApprover._FormName = mstrModuleName
            objApprover._LoginEmployeeunkid = 0
            objApprover._ClientIP = getIP()
            objApprover._HostName = getHostName()
            objApprover._FromWeb = False
            objApprover._AuditUserId = User._Object._Userunkid
objApprover._CompanyUnkid = Company._Object._Companyunkid
            objApprover._AuditDate = ConfigParameter._Object._CurrentDateAndTime
            'S.SANDEEP [28-May-2018] -- END

            If objApprover.InActiveApprover(CInt(lvApproverList.SelectedItems(0).Tag), False) = False Then
                eZeeMsgBox.Show(objApprover._Message, enMsgBoxStyle.Information)
                Exit Sub
            Else
                fillList()
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "mnuInActiveApprover_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub mnuActiveApprover_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles mnuActiveApprover.Click
        Try
            If lvApproverList.SelectedItems.Count = 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please select Approver from the list to perform further operation on it."), enMsgBoxStyle.Information) '?1
                lvApproverList.Select()
                Exit Sub
            End If


            'S.SANDEEP [28-May-2018] -- START
            'ISSUE/ENHANCEMENT : {Audit Trails} 
            objApprover._FormName = mstrModuleName
            objApprover._LoginEmployeeunkid = 0
            objApprover._ClientIP = getIP()
            objApprover._HostName = getHostName()
            objApprover._FromWeb = False
            objApprover._AuditUserId = User._Object._Userunkid
objApprover._CompanyUnkid = Company._Object._Companyunkid
            objApprover._AuditDate = ConfigParameter._Object._CurrentDateAndTime
            'S.SANDEEP [28-May-2018] -- END

            If objApprover.InActiveApprover(CInt(lvApproverList.SelectedItems(0).Tag), True) = False Then
                eZeeMsgBox.Show(objApprover._Message, enMsgBoxStyle.Information)
                Exit Sub
            Else
                fillList()
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "mnuActiveApprover_Click", mstrModuleName)
        End Try
    End Sub


    'Pinkal (25-Nov-2020) -- Start
    'Enhancement IHI - Working on Import Timesheet Approver.

    Private Sub mnuImportTimesheetApprover_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuImportTimesheetApprover.Click
        Dim frm As New frmTimesheetApprImportWizard
        Try
            frm.ShowDialog()
            Call fillList()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "mnuImportTimesheetApprover_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub mnuGetFileFormat_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuGetFileFormat.Click
        Try
            Dim objSave As New SaveFileDialog
            objSave.Filter = "Excel files(*.xlsx)|*.xlsx"
            If objSave.ShowDialog = Windows.Forms.DialogResult.OK Then
                Dim dsList As DataSet = objApprover.GetImportFileStructure()
                OpenXML_Export(objSave.FileName, dsList)
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "mnuGetFileFormat_Click", mstrModuleName)
        End Try
    End Sub

    'Pinkal (25-Nov-2020) -- End


#End Region

#Region "Dropdown Event"

    Private Sub cboLevel_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboLevel.SelectedIndexChanged
        Try

            Dim objApprover As New clstsapprover_master

            'S.SANDEEP [28-May-2018] -- START
            'ISSUE/ENHANCEMENT : {Audit Trails} 
            objApprover._FormName = mstrModuleName
            objApprover._LoginEmployeeunkid = 0
            objApprover._ClientIP = getIP()
            objApprover._HostName = getHostName()
            objApprover._FromWeb = False
            objApprover._AuditUserId = User._Object._Userunkid
objApprover._CompanyUnkid = Company._Object._Companyunkid
            objApprover._AuditDate = ConfigParameter._Object._CurrentDateAndTime
            'S.SANDEEP [28-May-2018] -- END

            Dim dsList As DataSet = objApprover.GetList("List", FinancialYear._Object._DatabaseName, User._Object._Userunkid _
                                                                              , FinancialYear._Object._YearUnkid, Company._Object._Companyunkid _
                                                                              , ConfigParameter._Object._EmployeeAsOnDate, ConfigParameter._Object._UserAccessModeSetting _
                                                                              , True, ConfigParameter._Object._IsIncludeInactiveEmp, True, False, -1 _
                                                                              , Nothing, "", "")


            Dim dtTable As DataTable = Nothing
            If CInt(cboLevel.SelectedValue) > 0 Then
                dtTable = New DataView(dsList.Tables(0), "tslevelunkid = " & CInt(cboLevel.SelectedValue), "", DataViewRowState.CurrentRows).ToTable
            Else
                dtTable = dsList.Tables(0).DefaultView.ToTable(True, "employeeapproverunkid", "name", "isexternalapprover")
            End If

            Dim dr As DataRow = dtTable.NewRow
            dr("employeeapproverunkid") = 0
            dr("name") = Language.getMessage(mstrModuleName, 4, "Select")
            dr("isexternalapprover") = False
            dtTable.Rows.InsertAt(dr, 0)

            cboApprover.DisplayMember = "name"
            cboApprover.ValueMember = "employeeapproverunkid"
            cboApprover.DataSource = dtTable
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboLevel_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub cboStatus_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboStatus.SelectedIndexChanged
        Try
            If CInt(cboStatus.SelectedValue) = 1 Then
                mnuInActiveApprover.Enabled = True
                mnuActiveApprover.Enabled = False
            ElseIf CInt(cboStatus.SelectedValue) = 2 Then
                mnuInActiveApprover.Enabled = False
                mnuActiveApprover.Enabled = True
            Else
                mnuInActiveApprover.Enabled = False
                mnuActiveApprover.Enabled = False
            End If
            fillList()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboStatus_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

#End Region




   
  
    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Private Sub OtherSettings()
        Try
            Me.SuspendLayout()
            Call SetLanguage()

            Me.gbFilterCriteria.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbFilterCriteria.ForeColor = GUI._eZeeContainerHeaderForeColor


            Me.btnDelete.GradientBackColor = GUI._ButttonBackColor
            Me.btnDelete.GradientForeColor = GUI._ButttonFontColor

            Me.btnEdit.GradientBackColor = GUI._ButttonBackColor
            Me.btnEdit.GradientForeColor = GUI._ButttonFontColor

            Me.btnNew.GradientBackColor = GUI._ButttonBackColor
            Me.btnNew.GradientForeColor = GUI._ButttonFontColor

            Me.btnClose.GradientBackColor = GUI._ButttonBackColor
            Me.btnClose.GradientForeColor = GUI._ButttonFontColor

            Me.btnOpearation.GradientBackColor = GUI._ButttonBackColor
            Me.btnOpearation.GradientForeColor = GUI._ButttonFontColor


            Me.ResumeLayout()
        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "OtherSettings", mstrModuleName)
        End Try
    End Sub


    Private Sub SetLanguage()
        Try
            Me.Text = Language._Object.getCaption(Me.Name, Me.Text)

            Me.btnDelete.Text = Language._Object.getCaption(Me.btnDelete.Name, Me.btnDelete.Text)
            Me.btnEdit.Text = Language._Object.getCaption(Me.btnEdit.Name, Me.btnEdit.Text)
            Me.btnNew.Text = Language._Object.getCaption(Me.btnNew.Name, Me.btnNew.Text)
            Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
            Me.colhApproverName.Text = Language._Object.getCaption(CStr(Me.colhApproverName.Tag), Me.colhApproverName.Text)
            Me.colhLevel.Text = Language._Object.getCaption(CStr(Me.colhLevel.Tag), Me.colhLevel.Text)
            Me.colhDepartment.Text = Language._Object.getCaption(CStr(Me.colhDepartment.Tag), Me.colhDepartment.Text)
            Me.colhJob.Text = Language._Object.getCaption(CStr(Me.colhJob.Tag), Me.colhJob.Text)
            Me.gbFilterCriteria.Text = Language._Object.getCaption(Me.gbFilterCriteria.Name, Me.gbFilterCriteria.Text)
            Me.lblApprover.Text = Language._Object.getCaption(Me.lblApprover.Name, Me.lblApprover.Text)
            Me.btnOpearation.Text = Language._Object.getCaption(Me.btnOpearation.Name, Me.btnOpearation.Text)
            Me.lblLevel.Text = Language._Object.getCaption(Me.lblLevel.Name, Me.lblLevel.Text)
            Me.mnuInActiveApprover.Text = Language._Object.getCaption(Me.mnuInActiveApprover.Name, Me.mnuInActiveApprover.Text)
            Me.lblStatus.Text = Language._Object.getCaption(Me.lblStatus.Name, Me.lblStatus.Text)
            Me.mnuActiveApprover.Text = Language._Object.getCaption(Me.mnuActiveApprover.Name, Me.mnuActiveApprover.Text)
            Me.colhIsExternalApprover.Text = Language._Object.getCaption(CStr(Me.colhIsExternalApprover.Tag), Me.colhIsExternalApprover.Text)
			Me.colhMappedUser.Text = Language._Object.getCaption(CStr(Me.colhMappedUser.Tag), Me.colhMappedUser.Text)
			Me.mnuImportTimesheetApprover.Text = Language._Object.getCaption(Me.mnuImportTimesheetApprover.Name, Me.mnuImportTimesheetApprover.Text)
			Me.mnuGetFileFormat.Text = Language._Object.getCaption(Me.mnuGetFileFormat.Name, Me.mnuGetFileFormat.Text)

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
        End Try
    End Sub


    Private Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Please select Approver from the list to perform further operation on it.")
            Language.setMessage(mstrModuleName, 2, "Sorry, You cannot delete this Approver. Reason: This Approver is in use.")
            Language.setMessage(mstrModuleName, 3, "Are you sure you want to delete this Approver?")
            Language.setMessage(mstrModuleName, 4, "Select")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class