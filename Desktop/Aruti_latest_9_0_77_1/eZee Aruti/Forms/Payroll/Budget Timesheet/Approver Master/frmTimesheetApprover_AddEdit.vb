﻿Option Strict On

#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data

#End Region

Public Class frmTimesheetApprover_AddEdit

#Region " Private Variables "

    Private mstrModuleName As String = "frmTimesheetApprover_AddEdit"
    Private menAction As enAction = enAction.ADD_ONE
    Private mstrAdvanceFilter As String = ""
    Private mstrEmployeeIDs As String = ""
    Private mdtEmployee As DataTable
    Private mblnCancel As Boolean = True
    Private mintApproverMasterId As Integer = -1
    Private objApproverMaster As clstsapprover_master
    Private objApproverTran As clstsapprover_Tran
    Private mdtAssigedEmp As DataTable
    Private dtAseView As DataView
    Private dtEmpView As DataView
    Private mdtMapTran As DataTable

#End Region

#Region " Display Dialog "

    Public Function displayDialog(ByVal eAction As enAction, ByVal iTsApproverId As Integer) As Boolean
        Try
            mintApproverMasterId = iTsApproverId
            menAction = eAction
            Me.ShowDialog()

            Return Not mblnCancel
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "displayDialog", mstrModuleName)
        Finally
        End Try
    End Function

#End Region

#Region " Private Methods "

    Private Sub Fill_Combo()
        Dim dsCombo As New DataSet
        Dim objUsr As New clsUserAddEdit
        Dim objPswd As New clsPassowdOptions
        Dim objEmp As New clsEmployee_Master
        Try


            'FOR APPROVER LEVEL
            Dim objApproverLevel As New clstsapproverlevel_master
            dsCombo = objApproverLevel.getListForCombo("Level", True)
            cboApproveLevel.ValueMember = "tslevelunkid"
            cboApproveLevel.DisplayMember = "name"
            cboApproveLevel.DataSource = dsCombo.Tables("Level")

            dsCombo = objUsr.getNewComboList("User", , True, Company._Object._Companyunkid, CStr(CInt(enUserPriviledge.AllowToMapBudgetTimesheetApprover)), FinancialYear._Object._YearUnkid, True)
            With cboUser
                .ValueMember = "userunkid"
                .DisplayMember = "name"
                .DataSource = dsCombo.Tables("User")
                .SelectedValue = 0
            End With

            Dim blnInActiveEmp As Boolean = False
            If menAction <> enAction.EDIT_ONE Then
                blnInActiveEmp = False
            Else
                blnInActiveEmp = True
            End If


            dsCombo = objEmp.GetList(FinancialYear._Object._DatabaseName, _
                                     User._Object._Userunkid, _
                                     FinancialYear._Object._YearUnkid, _
                                     Company._Object._Companyunkid, _
                                     eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                     eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                     ConfigParameter._Object._UserAccessModeSetting, True, blnInActiveEmp, _
                                     "List", ConfigParameter._Object._ShowFirstAppointmentDate, , , , , False)


            mdtEmployee = dsCombo.Tables("List")
            mdtEmployee.Columns.Add("ischeck", System.Type.GetType("System.Boolean"))


        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Fill_Combo", mstrModuleName)
        Finally
            objPswd = Nothing : objUsr = Nothing : objEmp = Nothing
        End Try
    End Sub

    Private Sub Fill_Employee()
        Try
            If mdtEmployee.Rows.Count > 0 Then
                Dim dEmp As DataTable = Nothing
                Dim sFilter As String = String.Empty
                Dim mstrIds As String = String.Empty
                If mstrAdvanceFilter.Trim.Length > 0 Then
                    sFilter &= "AND " & mstrAdvanceFilter
                End If


                If chkExternalApprover.Checked = False Then
                    If Not txtName.Tag Is Nothing And CInt(txtName.Tag) > 0 Then
                        sFilter &= "AND employeeunkid <> " & CInt(txtName.Tag) & " "
                    End If
                End If


                If mstrEmployeeIDs.Trim.Length > 0 Then
                    sFilter &= "AND employeeunkid not in ( " & mstrEmployeeIDs.Trim & " )"
                End If

                If sFilter.Trim.Length > 0 Then
                    sFilter = sFilter.Substring(3)
                    dEmp = New DataView(mdtEmployee, sFilter, "", DataViewRowState.CurrentRows).ToTable
                Else
                    dEmp = New DataView(mdtEmployee, "", "", DataViewRowState.CurrentRows).ToTable
                End If
                dtEmpView = dEmp.DefaultView

                dgvAEmployee.AutoGenerateColumns = False
                objdgcolhECheck.DataPropertyName = "ischeck"
                dgcolhEcode.DataPropertyName = "employeecode"
                dgcolhEName.DataPropertyName = "name"
                objdgcolhEmpId.DataPropertyName = "employeeunkid"
                dgvAEmployee.DataSource = dtEmpView
                If txtName.Tag IsNot Nothing Then
                    If mdtAssigedEmp IsNot Nothing Then
                        mdtAssigedEmp.Rows.Clear()
                    End If
                    objApproverTran._EmployeeAsonDate = eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate)
                    objApproverTran._Tsapproverunkid = mintApproverMasterId
                    objApproverTran.GetData()
                    mdtAssigedEmp = objApproverTran._DataList
                    Call Fill_Assigned_Employee()
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Fill_Employee", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub Fill_Assigned_Employee()
        Try
            dtAseView = mdtAssigedEmp.DefaultView
            dtAseView.RowFilter = " AUD <> 'D' "
            dgvEmployee.AutoGenerateColumns = False
            objdgcolhaCheck.DataPropertyName = "ischeck"
            dgcolhaCode.DataPropertyName = "employeecode"
            dgcolhaEmp.DataPropertyName = "name"
            dgcolhaDepartment.DataPropertyName = "departmentname"
            dgcolhaJob.DataPropertyName = "jobname"
            objdgcolhaEmpId.DataPropertyName = "employeeunkid"
            objdgcolhAMasterId.DataPropertyName = "tsapproverunkid"
            objdgcolhATranId.DataPropertyName = "tsapprovertranunkid"
            dgvEmployee.DataSource = dtAseView
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Fill_Assigned_Employee", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Function Is_Valid_Data() As Boolean
        Try
            If txtName.Text.Trim.Length <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Timesheet Approver is mandatory information. Please provide Timesheet Approver to continue."), enMsgBoxStyle.Information)
                Return False
            End If

            If CInt(cboApproveLevel.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Timesheet Approver Level is mandatory information. Please provide Timesheet Approver Level to continue"), enMsgBoxStyle.Information)
                cboApproveLevel.Focus()
                Return False
            End If

            If CInt(cboUser.SelectedValue) <= 0 Then
                If chkExternalApprover.Checked = False Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "User is mandatory information. Please provide user to continue"), enMsgBoxStyle.Information)
                    cboUser.Focus()
                    Return False
                End If
            End If

            Dim dtmp() As DataRow = Nothing
            mdtAssigedEmp.AcceptChanges()
            dtmp = mdtAssigedEmp.Select("AUD <> 'D'")
            If dtmp.Length <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "Timesheet Approver access is mandatory. Please provide Timesheet Approver access to save."), enMsgBoxStyle.Information)
                Return False
            End If

            Return True
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Is_Valid_Data", mstrModuleName)
        Finally
        End Try
    End Function

    Private Sub SetValue()
        Try
            objApproverMaster._Employeeapproverunkid = CInt(txtName.Tag)
            objApproverMaster._Tslevelunkid = CInt(cboApproveLevel.SelectedValue)

            If chkExternalApprover.Checked = True Then
                objApproverMaster._MapUserId = CInt(txtName.Tag)
            Else
                objApproverMaster._MapUserId = CInt(cboUser.SelectedValue)
            End If

            objApproverMaster._Userunkid = User._Object._Userunkid

            If menAction <> enAction.EDIT_ONE Then
                objApproverMaster._Isvoid = False
                objApproverMaster._Voiddatetime = Nothing
                objApproverMaster._Voidreason = ""
                objApproverMaster._Voiduserunkid = -1
                objApproverMaster._Isactive = True
            End If

            objApproverMaster._Isexternalapprover = chkExternalApprover.Checked
            objApproverMaster._EmployeeTran = mdtAssigedEmp

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetValue", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub GetValue()
        Try
            txtName.Tag = objApproverMaster._Employeeapproverunkid
            cboUser.SelectedValue = objApproverMaster._MapUserId
            txtName.Text = objApproverMaster._EmployeeName
            cboApproveLevel.SelectedValue = objApproverMaster._Tslevelunkid
            chkExternalApprover.Checked = objApproverMaster._Isexternalapprover
            If menAction = enAction.EDIT_ONE Then
                mstrEmployeeIDs = objApproverMaster.GetTimesheetApproverEmployeeId(CInt(txtName.Tag), objApproverMaster._Isexternalapprover)
            End If
            If txtName.Text.Trim.Length > 0 Then Call Fill_Employee()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetValue", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub Add_DataRow(ByVal dRow As DataRow)
        Try
            Dim mdtRow As DataRow = Nothing
            Dim dtmp() As DataRow = Nothing
            dtmp = mdtAssigedEmp.Select("employeeunkid = '" & CInt(dRow.Item("employeeunkid")) & "' AND AUD <> 'D' ")
            If dtmp.Length <= 0 Then
                mdtRow = mdtAssigedEmp.NewRow
                mdtRow.Item("tsapprovertranunkid") = -1
                mdtRow.Item("tsapproverunkid") = mintApproverMasterId
                mdtRow.Item("employeeunkid") = CInt(dRow.Item("employeeunkid"))
                mdtRow.Item("employeecode") = dRow.Item("employeecode").ToString()
                mdtRow.Item("name") = dRow.Item("name").ToString()
                mdtRow.Item("departmentname") = dRow.Item("DeptName")
                mdtRow.Item("jobname") = dRow.Item("job_name")
                mdtRow.Item("AUD") = "A"
                mdtRow.Item("GUID") = Guid.NewGuid.ToString
              
                mdtAssigedEmp.Rows.Add(mdtRow)
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Add_DataRow", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

#Region " Form's Events "

    Private Sub frmExApproverAddEdit_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        objApproverMaster = New clstsapprover_master
        objApproverTran = New clstsapprover_Tran
        Try
            Call Set_Logo(Me, gApplicationType)
            Language.setLanguage(Me.Name)
            Call OtherSettings()
            Call Fill_Combo()
            If menAction = enAction.EDIT_ONE Then
                objApproverMaster._Tsapproverunkid = mintApproverMasterId
                objbtnSearchEmployee.Enabled = False
                chkExternalApprover.Enabled = False
            End If
            Call GetValue()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmExApproverAddEdit_Load", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsExpenseApprover_Master.SetMessages()
            objfrm._Other_ModuleNames = "clsExpenseApprover_Master"
            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub

#End Region

#Region " Button's Events "

    Private Sub objbtnSearchEmployee_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchEmployee.Click
        Dim frm As New frmCommonSearch
        Try
            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If

            If chkExternalApprover.Checked = False Then

                With frm
                    .ValueMember = "employeeunkid"
                    .DisplayMember = "name"
                    .CodeMember = "employeecode"
                    .DataSource = mdtEmployee
                End With

                If frm.DisplayDialog Then
                    txtName.Text = frm.SelectedAlias & " - " & frm.SelectedText
                    txtName.Tag = frm.SelectedValue
                    Dim objOption As New clsPassowdOptions
                    If objOption._IsEmployeeAsUser Then
                        Dim objUser As New clsUserAddEdit
                        Dim mintUserID As Integer = objUser.Return_UserId(CInt(frm.SelectedValue), Company._Object._Companyunkid)
                        Dim drRow() As DataRow = CType(cboUser.DataSource, DataTable).Select("userunkid = " & mintUserID)
                        If drRow.Length > 0 Then
                            cboUser.SelectedValue = mintUserID
                        Else
                            cboUser.SelectedValue = 0
                        End If
                    End If
                End If

            Else

                Dim objUser As New clsUserAddEdit

                Dim dsList As DataSet = objUser.GetExternalApproverList("List", _
                                                         Company._Object._Companyunkid, _
                                                         FinancialYear._Object._YearUnkid, _
                                                         eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                         eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), CInt(enUserPriviledge.AllowToMapBudgetTimesheetApprover).ToString())
                objUser = Nothing

                frm.DataSource = dsList.Tables("List")
                frm.ValueMember = "userunkid"
                frm.DisplayMember = "Name"
                frm.CodeMember = "username"

                If frm.DisplayDialog Then
                    txtName.Text = frm.SelectedAlias & " - " & frm.SelectedText
                    txtName.Tag = frm.SelectedValue
                End If

            End If

            mstrEmployeeIDs = objApproverMaster.GetTimesheetApproverEmployeeId(CInt(txtName.Tag), chkExternalApprover.Checked)

            If txtName.Text.Trim.Length > 0 Then Call Fill_Employee()

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchEmployee_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub objAlloacationReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objAlloacationReset.Click
        Try
            mstrAdvanceFilter = ""
            Call Fill_Employee()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objAlloacationReset_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub objbtnSearchUser_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchUser.Click
        Dim frm As New frmCommonSearch
        Try
            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If
            With frm
                .ValueMember = cboUser.ValueMember
                .CodeMember = cboUser.DisplayMember
                .DisplayMember = "Display"
                .DataSource = CType(cboUser.DataSource, DataTable)
            End With
            If frm.DisplayDialog Then
                cboUser.SelectedValue = frm.SelectedValue
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchUser_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub objbtnSearchLevel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchLevel.Click
        Dim frm As New frmCommonSearch
        Try
            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If
            If cboApproveLevel.DataSource IsNot Nothing Then
                With frm
                    .ValueMember = cboApproveLevel.ValueMember
                    .DisplayMember = cboApproveLevel.DisplayMember
                    .CodeMember = "Code"
                    .DataSource = CType(cboApproveLevel.DataSource, DataTable)
                End With
                If frm.DisplayDialog Then
                    cboUser.SelectedValue = frm.SelectedValue
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchLevel_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub btnAdd_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAdd.Click
        Try
            If dtEmpView IsNot Nothing Then
                Dim drCheck() As DataRow = Nothing
                drCheck = dtEmpView.ToTable.Select("ischeck=true")
                Dim iblFlag As Boolean = False
                If drCheck.Length > 0 Then
                    For i As Integer = 0 To drCheck.Length - 1
                        Call Add_DataRow(drCheck(i))
                    Next
                End If
                Call Fill_Assigned_Employee()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnAdd_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub btnDeleteA_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDeleteA.Click
        Try
            If dtAseView Is Nothing Then Exit Sub
            Dim drTemp As DataRow() = Nothing
            Dim dtmp() As DataRow = dtAseView.Table.Select("ischeck=True AND AUD <> 'D' ")
            If dtmp.Length <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 6, "Please check atleast one of the employee to unassigned."), enMsgBoxStyle.Information)
                Exit Sub
            End If
            Me.SuspendLayout()
            Dim blnFlag As Boolean = False
            If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 9, "Are you sure you want to delete Selected Employee?"), CType(enMsgBoxStyle.Question + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.Yes Then
                Me.Cursor = Cursors.WaitCursor
                'Dim objExpenseForm As New clsclaim_request_master

                For i As Integer = 0 To dtmp.Length - 1

                    'If objExpenseForm.GetApproverPendingExpenseFormCount(CInt(dtmp(i)("crapproverunkid")), CInt(dtmp(i)("employeeunkid")).ToString()) <= 0 Then
                    drTemp = dtAseView.Table.Select("employeeunkid = " & CInt(dtmp(i)("employeeunkid")))
                    'Else
                    '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 10, "This Employee has Pending Expense Application Form.You cannot delete this employee."), enMsgBoxStyle.Information)
                    '    Exit For
                    'End If

                    If drTemp IsNot Nothing AndAlso drTemp.Length > 0 Then
                        drTemp(0).Item("AUD") = "D"
                        dtAseView.Table.AcceptChanges()
                    End If

                Next

                txtaSearch.Text = ""
                objchkAssessor.Checked = False
                Fill_Assigned_Employee()
            End If
            Me.ResumeLayout()
        Catch ex As Exception
            Me.Cursor = Cursors.Default
            DisplayError.Show("-1", ex.Message, "btnDeleteA_Click", mstrModuleName)
        Finally
            Me.Cursor = Cursors.Default
        End Try
    End Sub

    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Try
            If Is_Valid_Data() = False Then Exit Sub
            Call SetValue()

            'S.SANDEEP [28-May-2018] -- START
            'ISSUE/ENHANCEMENT : {Audit Trails} 
            objApproverMaster._FormName = mstrModuleName
            objApproverMaster._LoginEmployeeunkid = 0
            objApproverMaster._ClientIP = getIP()
            objApproverMaster._HostName = getHostName()
            objApproverMaster._FromWeb = False
            objApproverMaster._AuditUserId = User._Object._Userunkid
objApproverMaster._CompanyUnkid = Company._Object._Companyunkid
            objApproverMaster._AuditDate = ConfigParameter._Object._CurrentDateAndTime
            'S.SANDEEP [28-May-2018] -- END

            If menAction <> enAction.EDIT_ONE Then
                If objApproverMaster.Insert() Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 7, "Timesheet Approver defined successfully, with all access and usermapping."), enMsgBoxStyle.Information)
                    mblnCancel = False
                    Me.Close()
                Else
                    eZeeMsgBox.Show(objApproverMaster._Message, enMsgBoxStyle.Information)
                    Exit Sub
                End If
            Else
                If objApproverMaster.Update() = True Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 8, "Timesheet Approver updated successfully, with all access and usermapping."), enMsgBoxStyle.Information)
                    mblnCancel = False
                    Me.Close()
                Else
                    eZeeMsgBox.Show(objApproverMaster._Message, enMsgBoxStyle.Information)
                    Exit Sub
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnSave_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            mblnCancel = False
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnClose_Click", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

#Region "LinkButton Event"

    Private Sub lnkAllocation_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lnkAllocation.LinkClicked
        Dim frm As New frmAdvanceSearch
        Try
            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If

            If frm.ShowDialog = Windows.Forms.DialogResult.OK Then
                mstrAdvanceFilter = frm._GetFilterString.Replace("ADF.", "")
                Call Fill_Employee()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lnkAllocation_LinkClicked", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

#End Region

#Region "Textbox Event"

    Private Sub txtSearchEmp_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtSearchEmp.KeyDown
        Try
            Select Case e.KeyCode
                Case Windows.Forms.Keys.Down
                    If dgvAEmployee.Rows.Count > 0 Then
                        If dgvAEmployee.SelectedRows(0).Index = dgvAEmployee.Rows(dgvAEmployee.RowCount - 1).Index Then Exit Sub
                        dgvAEmployee.Rows(dgvAEmployee.SelectedRows(0).Index + 1).Selected = True
                    End If
                Case Windows.Forms.Keys.Up
                    If dgvAEmployee.Rows.Count > 0 Then
                        If dgvAEmployee.SelectedRows(0).Index = 0 Then Exit Sub
                        dgvAEmployee.Rows(dgvAEmployee.SelectedRows(0).Index - 1).Selected = True
                    End If
            End Select
        Catch ex As Exception
            DisplayError.Show(CStr(-1), ex.Message, "txtSearchEmp_KeyDown", mstrModuleName)
        End Try
    End Sub

    Private Sub txtSearchEmp_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtSearchEmp.TextChanged
        Try
            Dim strSearch As String = ""
            If txtSearchEmp.Text.Trim.Length > 0 Then
                strSearch = dgcolhEcode.DataPropertyName & " LIKE '%" & txtSearchEmp.Text & "%' OR " & _
                            dgcolhEName.DataPropertyName & " LIKE '%" & txtSearchEmp.Text & "%'"
            End If
            dtEmpView.RowFilter = strSearch
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub txtaSearch_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtaSearch.KeyDown
        Try
            Select Case e.KeyCode
                Case Windows.Forms.Keys.Down
                    If dgvEmployee.Rows.Count > 0 Then
                        If dgvEmployee.SelectedRows(0).Index = dgvEmployee.Rows(dgvEmployee.RowCount - 1).Index Then Exit Sub
                        dgvEmployee.Rows(dgvEmployee.SelectedRows(0).Index + 1).Selected = True
                    End If
                Case Windows.Forms.Keys.Up
                    If dgvEmployee.Rows.Count > 0 Then
                        If dgvEmployee.SelectedRows(0).Index = 0 Then Exit Sub
                        dgvEmployee.Rows(dgvEmployee.SelectedRows(0).Index - 1).Selected = True
                    End If
            End Select
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "txtaSearch_KeyDown", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub txtaSearch_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtaSearch.TextChanged
        Try
            Dim strSearch As String = ""
            If txtaSearch.Text.Trim.Length > 0 Then
                strSearch = dgcolhaCode.DataPropertyName & " LIKE '%" & txtaSearch.Text & "%' OR " & _
                            dgcolhaEmp.DataPropertyName & " LIKE '%" & txtaSearch.Text & "%' OR " & _
                            dgcolhaDepartment.DataPropertyName & " LIKE '%" & txtaSearch.Text & "%' OR " & _
                            dgcolhaJob.DataPropertyName & " LIKE '%" & txtaSearch.Text & "%'  AND AUD <> 'D' "
                dtAseView.RowFilter = strSearch
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "txtaSearch_TextChanged", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

#Region " Checkbox Events "

    Private Sub objchkEmployee_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objchkEmployee.CheckedChanged
        Try
            RemoveHandler dgvAEmployee.CellContentClick, AddressOf dgvAEmployee_CellContentClick
            For Each dr As DataRowView In dtEmpView
                dr.Item("ischeck") = CBool(objchkEmployee.CheckState)
            Next
            dgvAEmployee.Refresh()
            AddHandler dgvAEmployee.CellContentClick, AddressOf dgvAEmployee_CellContentClick
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objchkEmployee_CheckedChanged", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub objchkAssessor_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objchkAssessor.CheckedChanged
        Try
            RemoveHandler dgvEmployee.CellContentClick, AddressOf dgvEmployee_CellContentClick
            For Each dr As DataRowView In dtAseView
                dr.Item("ischeck") = CBool(objchkAssessor.CheckState)
            Next
            dtAseView.Table.AcceptChanges()
            dgvEmployee.Refresh()
            AddHandler dgvEmployee.CellContentClick, AddressOf dgvEmployee_CellContentClick
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objchkAssessor_CheckedChanged", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub chkExternalApprover_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkExternalApprover.CheckedChanged
        Try
            LblUser.Visible = Not chkExternalApprover.Checked
            cboUser.Visible = Not chkExternalApprover.Checked
            objbtnSearchUser.Visible = Not chkExternalApprover.Checked
            If chkExternalApprover.Checked = False Then
                cboUser.SelectedValue = 0
            End If
            If menAction <> enAction.EDIT_ONE Then
                txtName.Text = ""
                txtName.Tag = Nothing
                mstrEmployeeIDs = ""
                dgvAEmployee.DataSource = Nothing
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "chkExternalApprover_CheckedChanged", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

#Region "Datagrid Event"

    Private Sub dgvAEmployee_CellContentClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvAEmployee.CellContentClick, dgvAEmployee.CellContentDoubleClick
        Try
            RemoveHandler objchkEmployee.CheckedChanged, AddressOf objchkEmployee_CheckedChanged

            If e.ColumnIndex = objdgcolhECheck.Index Then

                If Me.dgvAEmployee.IsCurrentCellDirty Then
                    Me.dgvAEmployee.CommitEdit(DataGridViewDataErrorContexts.Commit)
                End If

                Dim drRow As DataRow() = dtEmpView.ToTable.Select("ischeck = true", "")
                If drRow.Length > 0 Then
                    If dtEmpView.ToTable.Rows.Count = drRow.Length Then
                        objchkEmployee.CheckState = CheckState.Checked
                    Else
                        objchkEmployee.CheckState = CheckState.Indeterminate
                    End If
                Else
                    objchkEmployee.CheckState = CheckState.Unchecked
                End If
            End If

            AddHandler objchkEmployee.CheckedChanged, AddressOf objchkEmployee_CheckedChanged

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgvAEmployee_CellContentClick", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub dgvEmployee_CellContentClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvEmployee.CellContentClick, dgvEmployee.CellContentDoubleClick
        Try
            RemoveHandler objchkAssessor.CheckedChanged, AddressOf objchkAssessor_CheckedChanged

            If e.ColumnIndex = objdgcolhECheck.Index Then

                If Me.dgvEmployee.IsCurrentCellDirty Then
                    Me.dgvEmployee.CommitEdit(DataGridViewDataErrorContexts.Commit)
                End If

                Dim drRow As DataRow() = dtAseView.ToTable.Select("ischeck = true", "")
                If drRow.Length > 0 Then
                    If dtAseView.ToTable.Rows.Count = drRow.Length Then
                        objchkAssessor.CheckState = CheckState.Checked
                    Else
                        objchkAssessor.CheckState = CheckState.Indeterminate
                    End If
                Else
                    objchkAssessor.CheckState = CheckState.Unchecked
                End If
            End If

            AddHandler objchkAssessor.CheckedChanged, AddressOf objchkAssessor_CheckedChanged

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgvEmployee_CellContentClick", mstrModuleName)
        Finally
        End Try
    End Sub


#End Region

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Private Sub OtherSettings()
        Try
            Me.SuspendLayout()
            Call SetLanguage()

            Me.gbInfo.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbInfo.ForeColor = GUI._eZeeContainerHeaderForeColor

            Me.gbAssignedEmployee.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbAssignedEmployee.ForeColor = GUI._eZeeContainerHeaderForeColor


            Me.btnSave.GradientBackColor = GUI._ButttonBackColor
            Me.btnSave.GradientForeColor = GUI._ButttonFontColor

            Me.btnClose.GradientBackColor = GUI._ButttonBackColor
            Me.btnClose.GradientForeColor = GUI._ButttonFontColor

            Me.btnAdd.GradientBackColor = GUI._ButttonBackColor
            Me.btnAdd.GradientForeColor = GUI._ButttonFontColor

            Me.btnDeleteA.GradientBackColor = GUI._ButttonBackColor
            Me.btnDeleteA.GradientForeColor = GUI._ButttonFontColor


            Me.ResumeLayout()
        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "OtherSettings", mstrModuleName)
        End Try
    End Sub


    Private Sub SetLanguage()
        Try
            Me.Text = Language._Object.getCaption(Me.Name, Me.Text)

            Me.btnSave.Text = Language._Object.getCaption(Me.btnSave.Name, Me.btnSave.Text)
            Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
            Me.gbInfo.Text = Language._Object.getCaption(Me.gbInfo.Name, Me.gbInfo.Text)
            Me.lnkAllocation.Text = Language._Object.getCaption(Me.lnkAllocation.Name, Me.lnkAllocation.Text)
            Me.btnAdd.Text = Language._Object.getCaption(Me.btnAdd.Name, Me.btnAdd.Text)
            Me.dgcolhEcode.HeaderText = Language._Object.getCaption(Me.dgcolhEcode.Name, Me.dgcolhEcode.HeaderText)
            Me.dgcolhEName.HeaderText = Language._Object.getCaption(Me.dgcolhEName.Name, Me.dgcolhEName.HeaderText)
            Me.lblApproverName.Text = Language._Object.getCaption(Me.lblApproverName.Name, Me.lblApproverName.Text)
            Me.LblUser.Text = Language._Object.getCaption(Me.LblUser.Name, Me.LblUser.Text)
            Me.EZeeLine1.Text = Language._Object.getCaption(Me.EZeeLine1.Name, Me.EZeeLine1.Text)
            Me.lblApproveLevel.Text = Language._Object.getCaption(Me.lblApproveLevel.Name, Me.lblApproveLevel.Text)
            Me.gbAssignedEmployee.Text = Language._Object.getCaption(Me.gbAssignedEmployee.Name, Me.gbAssignedEmployee.Text)
            Me.btnDeleteA.Text = Language._Object.getCaption(Me.btnDeleteA.Name, Me.btnDeleteA.Text)
            Me.dgcolhaCode.HeaderText = Language._Object.getCaption(Me.dgcolhaCode.Name, Me.dgcolhaCode.HeaderText)
            Me.dgcolhaEmp.HeaderText = Language._Object.getCaption(Me.dgcolhaEmp.Name, Me.dgcolhaEmp.HeaderText)
            Me.dgcolhaDepartment.HeaderText = Language._Object.getCaption(Me.dgcolhaDepartment.Name, Me.dgcolhaDepartment.HeaderText)
            Me.dgcolhaJob.HeaderText = Language._Object.getCaption(Me.dgcolhaJob.Name, Me.dgcolhaJob.HeaderText)
            Me.chkExternalApprover.Text = Language._Object.getCaption(Me.chkExternalApprover.Name, Me.chkExternalApprover.Text)

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
        End Try
    End Sub


    Private Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Expense Approver is mandatory information. Please provide Expense Approver to continue.")
            Language.setMessage(mstrModuleName, 3, "Expense Approver Level is mandatory information. Please provide Expense Approver Level to continue")
            Language.setMessage(mstrModuleName, 4, "User is mandatory information. Please provide user to continue")
            Language.setMessage(mstrModuleName, 5, "Expense Approver access is mandatory. Please provide Expense Approver access to save.")
            Language.setMessage(mstrModuleName, 6, "Please check atleast one of the employee to unassigned.")
            Language.setMessage(mstrModuleName, 7, "Expense Approver defined successfully, with all access and usermapping.")
            Language.setMessage(mstrModuleName, 8, "Expense Approver updated successfully, with all access and usermapping.")
            Language.setMessage(mstrModuleName, 9, "Are you sure you want to delete Selected Employee?")
            Language.setMessage(mstrModuleName, 10, "This Employee has Pending Expense Application Form.You cannot delete this employee.")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class