﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmTimesheetApproval
    Inherits eZee.Common.eZeeForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmTimesheetApproval))
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Me.pnlMain = New System.Windows.Forms.Panel
        Me.pnlGrid = New System.Windows.Forms.Panel
        Me.dgvEmpTimesheet = New System.Windows.Forms.DataGridView
        Me.txtRemarks = New eZee.TextBox.AlphanumericTextBox
        Me.LblRemarks = New System.Windows.Forms.Label
        Me.LblPeriod = New System.Windows.Forms.Label
        Me.cboPeriod = New System.Windows.Forms.ComboBox
        Me.objFooter = New eZee.Common.eZeeFooter
        Me.btnReject = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnClose = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnApprove = New eZee.Common.eZeeLightButton(Me.components)
        Me.objdgcolhShowDetails = New System.Windows.Forms.DataGridViewImageColumn
        Me.dgcolhParticular = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhActivityDate = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhActivity = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhApprover = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhDonor = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhProject = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhEmpDescription = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhSubmissionRemark = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhTotalActivityHours = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhHours = New eZee.Common.DataGridViewMaskTextBoxColumn
        Me.objdgcolhShiftHoursInSec = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhTotalActivityHoursInMin = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhActivityHrsInMins = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhActivityPercentage = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhPeriodID = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhEmployeeID = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhFundSourceID = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhProjectID = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhActivityID = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhIsGrp = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhMappedUserId = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhStatusID = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhEmployeeName = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhApproverEmployeeID = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhActivityDate = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhPriority = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhEmpTimesheetID = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhTimesheetApprovalID = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhIsExternalApprover = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhIsHoliday = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.pnlMain.SuspendLayout()
        Me.pnlGrid.SuspendLayout()
        CType(Me.dgvEmpTimesheet, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.objFooter.SuspendLayout()
        Me.SuspendLayout()
        '
        'pnlMain
        '
        Me.pnlMain.Controls.Add(Me.pnlGrid)
        Me.pnlMain.Controls.Add(Me.txtRemarks)
        Me.pnlMain.Controls.Add(Me.LblRemarks)
        Me.pnlMain.Controls.Add(Me.LblPeriod)
        Me.pnlMain.Controls.Add(Me.cboPeriod)
        Me.pnlMain.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pnlMain.Location = New System.Drawing.Point(0, 0)
        Me.pnlMain.Name = "pnlMain"
        Me.pnlMain.Size = New System.Drawing.Size(846, 465)
        Me.pnlMain.TabIndex = 0
        '
        'pnlGrid
        '
        Me.pnlGrid.Controls.Add(Me.dgvEmpTimesheet)
        Me.pnlGrid.Location = New System.Drawing.Point(13, 52)
        Me.pnlGrid.Name = "pnlGrid"
        Me.pnlGrid.Size = New System.Drawing.Size(821, 352)
        Me.pnlGrid.TabIndex = 258
        Me.pnlGrid.TabStop = True
        '
        'dgvEmpTimesheet
        '
        Me.dgvEmpTimesheet.AllowUserToAddRows = False
        Me.dgvEmpTimesheet.AllowUserToDeleteRows = False
        Me.dgvEmpTimesheet.AllowUserToResizeColumns = False
        Me.dgvEmpTimesheet.AllowUserToResizeRows = False
        Me.dgvEmpTimesheet.BackgroundColor = System.Drawing.Color.WhiteSmoke
        Me.dgvEmpTimesheet.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing
        Me.dgvEmpTimesheet.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.objdgcolhShowDetails, Me.dgcolhParticular, Me.dgcolhActivityDate, Me.dgcolhActivity, Me.dgcolhApprover, Me.dgcolhDonor, Me.dgcolhProject, Me.dgcolhEmpDescription, Me.dgcolhSubmissionRemark, Me.dgcolhTotalActivityHours, Me.dgcolhHours, Me.objdgcolhShiftHoursInSec, Me.objdgcolhTotalActivityHoursInMin, Me.objdgcolhActivityHrsInMins, Me.objdgcolhActivityPercentage, Me.objdgcolhPeriodID, Me.objdgcolhEmployeeID, Me.objdgcolhFundSourceID, Me.objdgcolhProjectID, Me.objdgcolhActivityID, Me.objdgcolhIsGrp, Me.objdgcolhMappedUserId, Me.objdgcolhStatusID, Me.objdgcolhEmployeeName, Me.objdgcolhApproverEmployeeID, Me.objdgcolhActivityDate, Me.objdgcolhPriority, Me.objdgcolhEmpTimesheetID, Me.objdgcolhTimesheetApprovalID, Me.objdgcolhIsExternalApprover, Me.objdgcolhIsHoliday})
        Me.dgvEmpTimesheet.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dgvEmpTimesheet.Location = New System.Drawing.Point(0, 0)
        Me.dgvEmpTimesheet.Name = "dgvEmpTimesheet"
        Me.dgvEmpTimesheet.RowHeadersVisible = False
        Me.dgvEmpTimesheet.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing
        Me.dgvEmpTimesheet.Size = New System.Drawing.Size(821, 352)
        Me.dgvEmpTimesheet.TabIndex = 0
        '
        'txtRemarks
        '
        Me.txtRemarks.Flags = 0
        Me.txtRemarks.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtRemarks.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtRemarks.Location = New System.Drawing.Point(468, 11)
        Me.txtRemarks.Multiline = True
        Me.txtRemarks.Name = "txtRemarks"
        Me.txtRemarks.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.txtRemarks.Size = New System.Drawing.Size(366, 35)
        Me.txtRemarks.TabIndex = 5
        '
        'LblRemarks
        '
        Me.LblRemarks.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblRemarks.Location = New System.Drawing.Point(391, 13)
        Me.LblRemarks.Name = "LblRemarks"
        Me.LblRemarks.Size = New System.Drawing.Size(71, 16)
        Me.LblRemarks.TabIndex = 4
        Me.LblRemarks.Text = "Remarks"
        '
        'LblPeriod
        '
        Me.LblPeriod.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblPeriod.Location = New System.Drawing.Point(13, 13)
        Me.LblPeriod.Name = "LblPeriod"
        Me.LblPeriod.Size = New System.Drawing.Size(75, 16)
        Me.LblPeriod.TabIndex = 0
        Me.LblPeriod.Text = "Period"
        '
        'cboPeriod
        '
        Me.cboPeriod.BackColor = System.Drawing.Color.White
        Me.cboPeriod.DropDownStyle = System.Windows.Forms.ComboBoxStyle.Simple
        Me.cboPeriod.DropDownWidth = 250
        Me.cboPeriod.Enabled = False
        Me.cboPeriod.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboPeriod.FormattingEnabled = True
        Me.cboPeriod.Location = New System.Drawing.Point(94, 11)
        Me.cboPeriod.Name = "cboPeriod"
        Me.cboPeriod.Size = New System.Drawing.Size(260, 21)
        Me.cboPeriod.TabIndex = 1
        '
        'objFooter
        '
        Me.objFooter.BorderColor = System.Drawing.Color.Silver
        Me.objFooter.Controls.Add(Me.btnReject)
        Me.objFooter.Controls.Add(Me.btnClose)
        Me.objFooter.Controls.Add(Me.btnApprove)
        Me.objFooter.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.objFooter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objFooter.GradiantStyle = eZee.Common.eZeeFooter.GradientStyle.Up
        Me.objFooter.GradientColor1 = System.Drawing.SystemColors.Control
        Me.objFooter.GradientColor2 = System.Drawing.SystemColors.Control
        Me.objFooter.Location = New System.Drawing.Point(0, 410)
        Me.objFooter.Name = "objFooter"
        Me.objFooter.Size = New System.Drawing.Size(846, 55)
        Me.objFooter.TabIndex = 1
        '
        'btnReject
        '
        Me.btnReject.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnReject.BackColor = System.Drawing.Color.White
        Me.btnReject.BackgroundImage = CType(resources.GetObject("btnReject.BackgroundImage"), System.Drawing.Image)
        Me.btnReject.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnReject.BorderColor = System.Drawing.Color.Empty
        Me.btnReject.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnReject.FlatAppearance.BorderSize = 0
        Me.btnReject.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnReject.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnReject.ForeColor = System.Drawing.Color.Black
        Me.btnReject.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnReject.GradientForeColor = System.Drawing.Color.Black
        Me.btnReject.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnReject.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnReject.Location = New System.Drawing.Point(642, 13)
        Me.btnReject.Name = "btnReject"
        Me.btnReject.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnReject.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnReject.Size = New System.Drawing.Size(93, 30)
        Me.btnReject.TabIndex = 1
        Me.btnReject.Text = "&Reject"
        Me.btnReject.UseVisualStyleBackColor = True
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnClose.BackColor = System.Drawing.Color.White
        Me.btnClose.BackgroundImage = CType(resources.GetObject("btnClose.BackgroundImage"), System.Drawing.Image)
        Me.btnClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnClose.BorderColor = System.Drawing.Color.Empty
        Me.btnClose.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnClose.FlatAppearance.BorderSize = 0
        Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClose.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.ForeColor = System.Drawing.Color.Black
        Me.btnClose.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnClose.GradientForeColor = System.Drawing.Color.Black
        Me.btnClose.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Location = New System.Drawing.Point(741, 13)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Size = New System.Drawing.Size(93, 30)
        Me.btnClose.TabIndex = 2
        Me.btnClose.Text = "&Close"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'btnApprove
        '
        Me.btnApprove.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnApprove.BackColor = System.Drawing.Color.White
        Me.btnApprove.BackgroundImage = CType(resources.GetObject("btnApprove.BackgroundImage"), System.Drawing.Image)
        Me.btnApprove.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnApprove.BorderColor = System.Drawing.Color.Empty
        Me.btnApprove.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnApprove.FlatAppearance.BorderSize = 0
        Me.btnApprove.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnApprove.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnApprove.ForeColor = System.Drawing.Color.Black
        Me.btnApprove.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnApprove.GradientForeColor = System.Drawing.Color.Black
        Me.btnApprove.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnApprove.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnApprove.Location = New System.Drawing.Point(543, 13)
        Me.btnApprove.Name = "btnApprove"
        Me.btnApprove.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnApprove.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnApprove.Size = New System.Drawing.Size(93, 30)
        Me.btnApprove.TabIndex = 0
        Me.btnApprove.Text = "&Approve"
        Me.btnApprove.UseVisualStyleBackColor = True
        '
        'objdgcolhShowDetails
        '
        Me.objdgcolhShowDetails.Frozen = True
        Me.objdgcolhShowDetails.HeaderText = ""
        Me.objdgcolhShowDetails.Image = Global.Aruti.Main.My.Resources.Resources.Info_icons
        Me.objdgcolhShowDetails.Name = "objdgcolhShowDetails"
        Me.objdgcolhShowDetails.ReadOnly = True
        Me.objdgcolhShowDetails.ToolTipText = "Show Project Hour Details"
        Me.objdgcolhShowDetails.Width = 25
        '
        'dgcolhParticular
        '
        Me.dgcolhParticular.Frozen = True
        Me.dgcolhParticular.HeaderText = "Particular"
        Me.dgcolhParticular.Name = "dgcolhParticular"
        Me.dgcolhParticular.ReadOnly = True
        Me.dgcolhParticular.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.dgcolhParticular.Width = 150
        '
        'dgcolhActivityDate
        '
        Me.dgcolhActivityDate.HeaderText = "Activity Date"
        Me.dgcolhActivityDate.Name = "dgcolhActivityDate"
        Me.dgcolhActivityDate.ReadOnly = True
        Me.dgcolhActivityDate.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.dgcolhActivityDate.Width = 125
        '
        'dgcolhActivity
        '
        Me.dgcolhActivity.HeaderText = "Activity"
        Me.dgcolhActivity.Name = "dgcolhActivity"
        Me.dgcolhActivity.ReadOnly = True
        Me.dgcolhActivity.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.dgcolhActivity.Width = 150
        '
        'dgcolhApprover
        '
        Me.dgcolhApprover.HeaderText = "Approver"
        Me.dgcolhApprover.Name = "dgcolhApprover"
        Me.dgcolhApprover.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.dgcolhApprover.Width = 225
        '
        'dgcolhDonor
        '
        Me.dgcolhDonor.HeaderText = "Donor/Grant"
        Me.dgcolhDonor.Name = "dgcolhDonor"
        Me.dgcolhDonor.ReadOnly = True
        Me.dgcolhDonor.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.dgcolhDonor.Width = 150
        '
        'dgcolhProject
        '
        Me.dgcolhProject.HeaderText = "Project Code"
        Me.dgcolhProject.Name = "dgcolhProject"
        Me.dgcolhProject.ReadOnly = True
        Me.dgcolhProject.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.dgcolhProject.Width = 150
        '
        'dgcolhEmpDescription
        '
        Me.dgcolhEmpDescription.HeaderText = "Description"
        Me.dgcolhEmpDescription.Name = "dgcolhEmpDescription"
        Me.dgcolhEmpDescription.ReadOnly = True
        Me.dgcolhEmpDescription.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.dgcolhEmpDescription.Width = 175
        '
        'dgcolhSubmissionRemark
        '
        Me.dgcolhSubmissionRemark.HeaderText = "Submission Remark"
        Me.dgcolhSubmissionRemark.Name = "dgcolhSubmissionRemark"
        Me.dgcolhSubmissionRemark.ReadOnly = True
        Me.dgcolhSubmissionRemark.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.dgcolhSubmissionRemark.Width = 200
        '
        'dgcolhTotalActivityHours
        '
        Me.dgcolhTotalActivityHours.HeaderText = "Total Activity Hours"
        Me.dgcolhTotalActivityHours.Name = "dgcolhTotalActivityHours"
        Me.dgcolhTotalActivityHours.ReadOnly = True
        Me.dgcolhTotalActivityHours.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.dgcolhTotalActivityHours.Width = 125
        '
        'dgcolhHours
        '
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle1.Format = "00:00"
        Me.dgcolhHours.DefaultCellStyle = DataGridViewCellStyle1
        Me.dgcolhHours.HeaderText = "Activity Hours"
        Me.dgcolhHours.Mask = "00:00"
        Me.dgcolhHours.Name = "dgcolhHours"
        Me.dgcolhHours.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgcolhHours.Width = 110
        '
        'objdgcolhShiftHoursInSec
        '
        Me.objdgcolhShiftHoursInSec.HeaderText = "Shift Hours In Sec"
        Me.objdgcolhShiftHoursInSec.Name = "objdgcolhShiftHoursInSec"
        Me.objdgcolhShiftHoursInSec.ReadOnly = True
        Me.objdgcolhShiftHoursInSec.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.objdgcolhShiftHoursInSec.Visible = False
        '
        'objdgcolhTotalActivityHoursInMin
        '
        Me.objdgcolhTotalActivityHoursInMin.HeaderText = "Total Activity Hours In Min"
        Me.objdgcolhTotalActivityHoursInMin.Name = "objdgcolhTotalActivityHoursInMin"
        Me.objdgcolhTotalActivityHoursInMin.ReadOnly = True
        Me.objdgcolhTotalActivityHoursInMin.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.objdgcolhTotalActivityHoursInMin.Visible = False
        '
        'objdgcolhActivityHrsInMins
        '
        Me.objdgcolhActivityHrsInMins.HeaderText = "ActivityHrsInMins"
        Me.objdgcolhActivityHrsInMins.Name = "objdgcolhActivityHrsInMins"
        Me.objdgcolhActivityHrsInMins.ReadOnly = True
        Me.objdgcolhActivityHrsInMins.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.objdgcolhActivityHrsInMins.Visible = False
        '
        'objdgcolhActivityPercentage
        '
        Me.objdgcolhActivityPercentage.HeaderText = "Percentage"
        Me.objdgcolhActivityPercentage.Name = "objdgcolhActivityPercentage"
        Me.objdgcolhActivityPercentage.ReadOnly = True
        Me.objdgcolhActivityPercentage.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.objdgcolhActivityPercentage.Visible = False
        '
        'objdgcolhPeriodID
        '
        Me.objdgcolhPeriodID.HeaderText = "PeriodID"
        Me.objdgcolhPeriodID.Name = "objdgcolhPeriodID"
        Me.objdgcolhPeriodID.ReadOnly = True
        Me.objdgcolhPeriodID.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.objdgcolhPeriodID.Visible = False
        '
        'objdgcolhEmployeeID
        '
        Me.objdgcolhEmployeeID.HeaderText = "EmployeeID"
        Me.objdgcolhEmployeeID.Name = "objdgcolhEmployeeID"
        Me.objdgcolhEmployeeID.ReadOnly = True
        Me.objdgcolhEmployeeID.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.objdgcolhEmployeeID.Visible = False
        '
        'objdgcolhFundSourceID
        '
        Me.objdgcolhFundSourceID.HeaderText = "FundSourceID"
        Me.objdgcolhFundSourceID.Name = "objdgcolhFundSourceID"
        Me.objdgcolhFundSourceID.ReadOnly = True
        Me.objdgcolhFundSourceID.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.objdgcolhFundSourceID.Visible = False
        '
        'objdgcolhProjectID
        '
        Me.objdgcolhProjectID.HeaderText = "ProjectID"
        Me.objdgcolhProjectID.Name = "objdgcolhProjectID"
        Me.objdgcolhProjectID.ReadOnly = True
        Me.objdgcolhProjectID.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.objdgcolhProjectID.Visible = False
        '
        'objdgcolhActivityID
        '
        Me.objdgcolhActivityID.HeaderText = "ActivityID"
        Me.objdgcolhActivityID.Name = "objdgcolhActivityID"
        Me.objdgcolhActivityID.ReadOnly = True
        Me.objdgcolhActivityID.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.objdgcolhActivityID.Visible = False
        '
        'objdgcolhIsGrp
        '
        Me.objdgcolhIsGrp.HeaderText = "IsGrp"
        Me.objdgcolhIsGrp.Name = "objdgcolhIsGrp"
        Me.objdgcolhIsGrp.ReadOnly = True
        Me.objdgcolhIsGrp.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.objdgcolhIsGrp.Visible = False
        '
        'objdgcolhMappedUserId
        '
        Me.objdgcolhMappedUserId.HeaderText = "MappedUserId"
        Me.objdgcolhMappedUserId.Name = "objdgcolhMappedUserId"
        Me.objdgcolhMappedUserId.ReadOnly = True
        Me.objdgcolhMappedUserId.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.objdgcolhMappedUserId.Visible = False
        '
        'objdgcolhStatusID
        '
        Me.objdgcolhStatusID.HeaderText = "StatusID"
        Me.objdgcolhStatusID.Name = "objdgcolhStatusID"
        Me.objdgcolhStatusID.ReadOnly = True
        Me.objdgcolhStatusID.Visible = False
        '
        'objdgcolhEmployeeName
        '
        Me.objdgcolhEmployeeName.HeaderText = "Employee Name"
        Me.objdgcolhEmployeeName.Name = "objdgcolhEmployeeName"
        Me.objdgcolhEmployeeName.ReadOnly = True
        Me.objdgcolhEmployeeName.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.objdgcolhEmployeeName.Visible = False
        '
        'objdgcolhApproverEmployeeID
        '
        Me.objdgcolhApproverEmployeeID.HeaderText = "ApproverEmployeeID"
        Me.objdgcolhApproverEmployeeID.Name = "objdgcolhApproverEmployeeID"
        Me.objdgcolhApproverEmployeeID.ReadOnly = True
        Me.objdgcolhApproverEmployeeID.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.objdgcolhApproverEmployeeID.Visible = False
        '
        'objdgcolhActivityDate
        '
        Me.objdgcolhActivityDate.HeaderText = "Activity Date"
        Me.objdgcolhActivityDate.Name = "objdgcolhActivityDate"
        Me.objdgcolhActivityDate.ReadOnly = True
        Me.objdgcolhActivityDate.Visible = False
        '
        'objdgcolhPriority
        '
        Me.objdgcolhPriority.HeaderText = "Priority"
        Me.objdgcolhPriority.Name = "objdgcolhPriority"
        Me.objdgcolhPriority.ReadOnly = True
        Me.objdgcolhPriority.Visible = False
        '
        'objdgcolhEmpTimesheetID
        '
        Me.objdgcolhEmpTimesheetID.HeaderText = "EmpTimesheetID"
        Me.objdgcolhEmpTimesheetID.Name = "objdgcolhEmpTimesheetID"
        Me.objdgcolhEmpTimesheetID.ReadOnly = True
        Me.objdgcolhEmpTimesheetID.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.objdgcolhEmpTimesheetID.Visible = False
        '
        'objdgcolhTimesheetApprovalID
        '
        Me.objdgcolhTimesheetApprovalID.HeaderText = "TimesheetApprovalID"
        Me.objdgcolhTimesheetApprovalID.Name = "objdgcolhTimesheetApprovalID"
        Me.objdgcolhTimesheetApprovalID.ReadOnly = True
        Me.objdgcolhTimesheetApprovalID.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.objdgcolhTimesheetApprovalID.Visible = False
        '
        'objdgcolhIsExternalApprover
        '
        Me.objdgcolhIsExternalApprover.HeaderText = "IsExternalApprover"
        Me.objdgcolhIsExternalApprover.Name = "objdgcolhIsExternalApprover"
        Me.objdgcolhIsExternalApprover.ReadOnly = True
        Me.objdgcolhIsExternalApprover.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.objdgcolhIsExternalApprover.Visible = False
        '
        'objdgcolhIsHoliday
        '
        Me.objdgcolhIsHoliday.HeaderText = "IsHoliday"
        Me.objdgcolhIsHoliday.Name = "objdgcolhIsHoliday"
        Me.objdgcolhIsHoliday.ReadOnly = True
        Me.objdgcolhIsHoliday.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.objdgcolhIsHoliday.Visible = False
        '
        'frmTimesheetApproval
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(846, 465)
        Me.Controls.Add(Me.objFooter)
        Me.Controls.Add(Me.pnlMain)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmTimesheetApproval"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Timesheet Approval"
        Me.pnlMain.ResumeLayout(False)
        Me.pnlMain.PerformLayout()
        Me.pnlGrid.ResumeLayout(False)
        CType(Me.dgvEmpTimesheet, System.ComponentModel.ISupportInitialize).EndInit()
        Me.objFooter.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents pnlMain As System.Windows.Forms.Panel
    Friend WithEvents objFooter As eZee.Common.eZeeFooter
    Friend WithEvents btnReject As eZee.Common.eZeeLightButton
    Friend WithEvents btnClose As eZee.Common.eZeeLightButton
    Friend WithEvents btnApprove As eZee.Common.eZeeLightButton
    Friend WithEvents LblPeriod As System.Windows.Forms.Label
    Friend WithEvents cboPeriod As System.Windows.Forms.ComboBox
    Friend WithEvents txtRemarks As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents LblRemarks As System.Windows.Forms.Label
    Friend WithEvents pnlGrid As System.Windows.Forms.Panel
    Friend WithEvents dgvEmpTimesheet As System.Windows.Forms.DataGridView
    Friend WithEvents objdgcolhShowDetails As System.Windows.Forms.DataGridViewImageColumn
    Friend WithEvents dgcolhParticular As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhActivityDate As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhActivity As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhApprover As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhDonor As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhProject As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhEmpDescription As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhSubmissionRemark As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhTotalActivityHours As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhHours As eZee.Common.DataGridViewMaskTextBoxColumn
    Friend WithEvents objdgcolhShiftHoursInSec As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhTotalActivityHoursInMin As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhActivityHrsInMins As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhActivityPercentage As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhPeriodID As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhEmployeeID As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhFundSourceID As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhProjectID As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhActivityID As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhIsGrp As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhMappedUserId As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhStatusID As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhEmployeeName As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhApproverEmployeeID As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhActivityDate As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhPriority As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhEmpTimesheetID As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhTimesheetApprovalID As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhIsExternalApprover As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhIsHoliday As System.Windows.Forms.DataGridViewTextBoxColumn
End Class
