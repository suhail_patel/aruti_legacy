﻿Option Strict On

Imports Aruti.Data
Imports eZeeCommonLib
Imports System.IO

Public Class frmTimesheetApprovalList

#Region " Private Varaibles "

    Private ReadOnly mstrModuleName As String = "frmTimesheetApprovalList"
    Private mblnCancel As Boolean = True
    Private menAction As enAction = enAction.ADD_ONE
    Private mstrSearchText As String = ""
    Private mstrAdvanceFilter As String = ""
    Dim objTimesheetApproval As clstsemptimesheet_approval
    Dim objApprover As clstsapprover_master
    Dim dsApproveList As DataSet = Nothing
    Dim mstrEmployeeIDs As String = ""

#End Region

#Region " Display Dialog "

    Public Function displayDialog(ByRef intUnkId As Integer, ByVal eAction As enAction) As Boolean
        Try
            menAction = eAction
            Me.ShowDialog()
            Return Not mblnCancel
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "displayDialog", mstrModuleName)
        End Try
    End Function

#End Region

#Region " Form's Events "

    Private Sub frmTimesheetApprovalList_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            objTimesheetApproval = New clstsemptimesheet_approval
            objApprover = New clstsapprover_master
            Call Set_Logo(Me, gApplicationType)
            Language.setLanguage(Me.Name)
            Call OtherSettings()
            FillCombo()


            'Pinkal (03-May-2017) -- Start
            'Enhancement - Working On Implementing privileges in Budget Timesheet Module.
            SetVisibility()
            'Pinkal (03-May-2017) -- End


            Dim objMapping As New clsapprover_Usermapping
            objMapping.GetData(enUserType.Timesheet_Approver, , User._Object._Userunkid, )


            'Nilay (10 Jan 2017) -- Start
            'ISSUE #23: Enhancements: Budget Employee TimeSheet Email Notification
            'objApprover._Tsapproverunkid = objMapping._Approverunkid
            'Dim dsAppr As New DataSet
            'dsAppr = objApprover.GetList("List", FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, ConfigParameter._Object._EmployeeAsOnDate, ConfigParameter._Object._UserAccessModeSetting, True, ConfigParameter._Object._IsIncludeInactiveEmp, , , , , , , objApprover._Tsapproverunkid)
            'If dsAppr.Tables("List").Rows.Count > 0 Then
            '    txtApprover.Text = CStr(dsAppr.Tables("List").Rows(0)("name"))
            'End If
            'dsAppr.Dispose()
            'objMapping = Nothing
            'Nilay (10 Jan 2017) -- End

            'Nilay (07 Feb 2017) -- Start
            'ISSUE #23: Enhancements: Budget Employee Timesheet 65.1 - ignore activity percentage restriction for the shift hrs
            'RemoveHandler chkMyApprovals.CheckedChanged, AddressOf chkMyApprovals_CheckedChanged
            'chkMyApprovals.Checked = True
            'AddHandler chkMyApprovals.CheckedChanged, AddressOf chkMyApprovals_CheckedChanged
            'Nilay (07 Feb 2017) -- End

            'Call SetDefaultSearchText(cboPeriod)
            'Call SetDefaultSearchText(cboEmployee)
            'Call SetDefaultSearchText(cboDonor)
            'Call SetDefaultSearchText(cboProject)
            'Call SetDefaultSearchText(cboActivity)
            dtpDate.Focus()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmTimesheetApprovalList_Load", mstrModuleName)
        End Try
    End Sub

    Private Sub frmTimesheetApprovalList_KeyPress(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles MyBase.KeyPress
        Try
            If Asc(e.KeyChar) = 13 Then
                SendKeys.Send("{TAB}")
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmTimesheetApprovalList_KeyPress", mstrModuleName)
        End Try
    End Sub

    Private Sub frmTimesheetApprovalList_LanguageClick(ByVal sender As Object, ByVal eventArgs As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clstsemptimesheet_approval.SetMessages()
            objfrm._Other_ModuleNames = "clstsemptimesheet_approval"
            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "frmTimesheetApprovalList_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub

#End Region

#Region " Private Methods "

    Private Sub FillCombo()
        Dim dsList As DataSet = Nothing
        Try
            'Nilay (21 Mar 2017) -- Start
            'ISSUE #23: Enhancements: Budget Employee TimeSheet Changes
            Dim objMaster As New clsMasterData
            'Nilay (21 Mar 2017) -- End

            Dim objPeriod As New clscommom_period_Tran
            dsList = objPeriod.getListForCombo(enModuleReference.Payroll, FinancialYear._Object._YearUnkid, FinancialYear._Object._DatabaseName _
                                                            , FinancialYear._Object._Database_Start_Date.Date, "List", True)

            cboPeriod.ValueMember = "periodunkid"
            cboPeriod.DisplayMember = "name"
            cboPeriod.DataSource = dsList.Tables(0)
            'Nilay (21 Mar 2017) -- Start
            'cboPeriod.SelectedValue = 0
            cboPeriod.SelectedValue = objMaster.getFirstPeriodID(enModuleReference.Payroll, FinancialYear._Object._YearUnkid, enStatusType.Open, False, True, Nothing)
            'Nilay (21 Mar 2017) -- End

            objPeriod = Nothing
            dsList = Nothing

            'Nilay (10 Jan 2017) -- Start
            'ISSUE #23: Enhancements: Budget Employee TimeSheet Email Notification
            Dim objtsApprMaster As New clstsapprover_master
            dsList = objtsApprMaster.GetLevelFromUserLogin(User._Object._Userunkid)
            With cboApprover
                .ValueMember = "tsapproverunkid"
                .DisplayMember = "Approver"
                .DataSource = dsList.Tables(0)
            End With
            objtsApprMaster = Nothing
            dsList = Nothing
            'Nilay (10 Jan 2017) -- End

            Dim objFundCode As New clsFundSource_Master
            dsList = objFundCode.GetComboList("List", True)
            cboDonor.DisplayMember = "fundname"
            cboDonor.ValueMember = "fundsourceunkid"
            cboDonor.DataSource = dsList.Tables(0)
            cboDonor.SelectedValue = 0
            objFundCode = Nothing
            dsList = Nothing


            'Pinkal (03-Jan-2020) -- Start
            'Enhancement - ATLAS COPCO TANZANIA LTD [0003673] - Remove or rename “Rescheduled” status, when approver is changing leave form status.
            'Dim dtList As DataTable = objMaster.getLeaveStatusList("Status", False, False).Tables(0).Select("statusunkid IN (0,1,2)").CopyToDataTable
            Dim dtList As DataTable = objMaster.getLeaveStatusList("Status", "", False, False).Tables(0).Select("statusunkid IN (0,1,2)").CopyToDataTable
            'Pinkal (03-Jan-2020) -- End

            cboStatus.ValueMember = "statusunkid"
            cboStatus.DisplayMember = "name"
            cboStatus.DataSource = dtList
            cboStatus.SelectedValue = 2

            cboApproverStatus.ValueMember = "statusunkid"
            cboApproverStatus.DisplayMember = "name"
            cboApproverStatus.DataSource = dtList.Copy()
            cboApproverStatus.SelectedValue = 2

            dtpDate_ValueChanged(New Object(), New EventArgs())
            cboDonor_SelectedIndexChanged(New Object(), New EventArgs())
            cboProject_SelectedIndexChanged(New Object(), New EventArgs())

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        End Try
    End Sub

    Private Sub SetDefaultSearchText(ByVal cbo As ComboBox)
        Try
            'mstrSearchText = Language.getMessage(mstrModuleName, 1, "Type to Search")

            With cbo
                .ForeColor = Color.Gray
                .Text = mstrSearchText
                .Font = New Font(Me.Font.FontFamily, Me.Font.Size, FontStyle.Italic)
            End With

            'With cboEmployee
            '    .ForeColor = Color.Gray
            '    .Text = mstrSearchText
            '    .Font = New Font(Me.Font.FontFamily, Me.Font.Size, FontStyle.Italic)
            'End With

            'With cboDonor
            '    .ForeColor = Color.Gray
            '    .Text = mstrSearchText
            '    .Font = New Font(Me.Font.FontFamily, Me.Font.Size, FontStyle.Italic)
            'End With

            'With cboProject
            '    .ForeColor = Color.Gray
            '    .Text = mstrSearchText
            '    .Font = New Font(Me.Font.FontFamily, Me.Font.Size, FontStyle.Italic)
            'End With

            'With cboActivity
            '    .ForeColor = Color.Gray
            '    .Text = mstrSearchText
            '    .Font = New Font(Me.Font.FontFamily, Me.Font.Size, FontStyle.Italic)
            'End With

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetDefaultSearchText", mstrModuleName)
        End Try
    End Sub

    Private Sub FillList()
        Dim strSearching As String = ""
        Try

            'Pinkal (03-May-2017) -- Start
            'Enhancement - Working On Implementing privileges in Budget Timesheet Module.
            If User._Object.Privilege._AllowToViewEmployeeBudgetTimesheetApprovalList = False Then Exit Sub
            'Pinkal (03-May-2017) -- End

            If CInt(cboPeriod.SelectedValue) > 0 Then
                strSearching &= "AND tsemptimesheet_approval.periodunkid =" & CInt(cboPeriod.SelectedValue) & " "
            End If

            If CInt(cboEmployee.SelectedValue) > 0 Then
                'Pinkal (23-Nov-2017) -- Start
                'Enhancement - Issue related to Budget timesheet Insertion issue.
                'strSearching = "AND tsemptimesheet_approval.employeeunkid =" & CInt(cboEmployee.SelectedValue) & " "
                strSearching &= "AND tsemptimesheet_approval.employeeunkid =" & CInt(cboEmployee.SelectedValue) & " "
                'Pinkal (23-Nov-2017) -- End
            End If

            If dtpDate.Checked Then
                strSearching &= "AND CONVERT(CHAR(8),ltbemployee_timesheet.activitydate,112) = '" & eZeeDate.convertDate(dtpDate.Value.Date) & "'"
            End If

            If CInt(cboDonor.SelectedValue) > 0 Then
                strSearching &= "AND tsemptimesheet_approval.fundsourceunkid = " & CInt(cboDonor.SelectedValue) & " "
            End If

            If CInt(cboProject.SelectedValue) > 0 Then
                strSearching &= "AND tsemptimesheet_approval.projectcodeunkid = " & CInt(cboProject.SelectedValue) & " "
            End If

            If CInt(cboActivity.SelectedValue) > 0 Then
                strSearching &= "AND tsemptimesheet_approval.activityunkid = " & CInt(cboActivity.SelectedValue) & " "
            End If

            If CInt(cboStatus.SelectedValue) > 0 Then
                strSearching &= "AND ltbemployee_timesheet.statusunkid = " & CInt(cboStatus.SelectedValue) & " "
            End If


            If mstrAdvanceFilter.Trim.Length > 0 Then
                strSearching &= "AND " & mstrAdvanceFilter & " "
            End If

            strSearching &= "AND tsemptimesheet_approval.visibleunkid <> -1 " & " "

            If strSearching.Trim.Length > 0 Then
                strSearching = strSearching.Substring(3)
            End If

            'Pinkal (13-Aug-2018) -- Start
            'Enhancement - Changes For PACT [Ref #249,252]
            'dsApproveList = objTimesheetApproval.GetList("List", FinancialYear._Object._DatabaseName, eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate) _
            '                                          , ConfigParameter._Object._CompanyDateFormat, User._Object._Userunkid, True _
            '                                          , Nothing, True, -1, True, strSearching)

            dsApproveList = objTimesheetApproval.GetList("List", FinancialYear._Object._DatabaseName, eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate) _
                                                      , ConfigParameter._Object._CompanyDateFormat, User._Object._Userunkid, True, ConfigParameter._Object._AllowOverTimeToEmpTimesheet _
                                                       , Nothing, True, -1, True, strSearching)

            'Pinkal (13-Aug-2018) -- End
           

            Dim xTimesheetApprovalunkid As Integer = 0
            Dim dList As DataTable = Nothing
            Dim mstrStaus As String = ""

            If dsApproveList.Tables(0) IsNot Nothing AndAlso dsApproveList.Tables(0).Rows.Count > 0 Then

                For Each drRow As DataRow In dsApproveList.Tables(0).Rows

                    If CInt(drRow("timesheetapprovalunkid")) <= 0 Then Continue For
                    mstrStaus = ""

                    If xTimesheetApprovalunkid <> CInt(drRow("timesheetapprovalunkid")) Then
                        dList = New DataView(dsApproveList.Tables(0), "employeeunkid = " & CInt(drRow("employeeunkid")) & " AND emptimesheetunkid = " & CInt(drRow("emptimesheetunkid").ToString()), "", DataViewRowState.CurrentRows).ToTable
                        xTimesheetApprovalunkid = CInt(drRow("timesheetapprovalunkid"))
                    End If

                    If dList IsNot Nothing AndAlso dList.Rows.Count > 0 Then
                        Dim dr As DataRow() = dList.Select("priority >= " & CInt(drRow("priority")))

                        If dr.Length > 0 Then

                            For i As Integer = 0 To dr.Length - 1

                                If CInt(drRow("ApprovalStatusId")) = 1 Then
                                    mstrStaus = Language.getMessage(mstrModuleName, 7, "Approved By :-  ") & drRow("ApproverName").ToString()
                                    Exit For

                                ElseIf CInt(drRow("ApprovalStatusId")) = 2 Then

                                    If CInt(dr(i)("ApprovalStatusId")) = 1 Then
                                        mstrStaus = Language.getMessage(mstrModuleName, 7, "Approved By :-  ") & dr(i)("ApproverName").ToString()
                                        Exit For

                                    ElseIf CInt(dr(i)("ApprovalStatusId")) = 3 Then
                                        mstrStaus = Language.getMessage(mstrModuleName, 8, "Rejected By :-  ") & dr(i)("ApproverName").ToString()
                                        Exit For

                                    End If

                                ElseIf CInt(drRow("ApprovalStatusId")) = 3 Then
                                    mstrStaus = Language.getMessage(mstrModuleName, 8, "Rejected By :-  ") & drRow("ApproverName").ToString()
                                    Exit For

                                End If

                            Next

                        End If

                    End If

                    If mstrStaus <> "" Then
                        drRow("status") = mstrStaus.Trim
                    End If

                Next

            End If

            Dim dtTable As New DataTable

            If chkMyApprovals.Checked Then

                'Pinkal (16-Feb-2017) -- Start
                'Enhancement - Working on Employee Budget Timesheet Report for Aga Khan.
                'dtTable = New DataView(dsApproveList.Tables("List"), "Mapuserunkid = " & User._Object._Userunkid & " OR Mapuserunkid <= 0", "", DataViewRowState.CurrentRows).ToTable
                If CInt(cboApproverStatus.SelectedValue) > 0 Then
                    dtTable = New DataView(dsApproveList.Tables("List"), "tsapproverunkid = " & CInt(cboApprover.SelectedValue) & " AND visibleunkid = " & CInt(cboApproverStatus.SelectedValue) & "  AND  Mapuserunkid = " & User._Object._Userunkid & " OR Mapuserunkid <= 0", "", DataViewRowState.CurrentRows).ToTable
                Else
                    dtTable = New DataView(dsApproveList.Tables("List"), "tsapproverunkid = " & CInt(cboApprover.SelectedValue) & " AND  Mapuserunkid = " & User._Object._Userunkid & " OR Mapuserunkid <= 0", "", DataViewRowState.CurrentRows).ToTable
                End If

                Dim dtTemp As DataTable = New DataView(dtTable, "", "", DataViewRowState.CurrentRows).ToTable(True, "ADate", "employeeunkid")


                'Pinkal (02-Mar-2017) -- Start
                'Enhancement - Working on AKFTZ Employee Budget Timesheet Report Enhancement.

                'Dim dr = From drTable In dtTable Group Join drTemp In dtTemp On drTable.Field(Of String)("ADate") Equals drTemp.Field(Of String)("ADate") And _
                '                drTable.Field(Of Integer)("employeeunkid") Equals drTemp.Field(Of Integer)("employeeunkid") Into Grp = Group Select drTable.Field(Of String)("ADate")

                Dim dr = From drTable In dtTable Group Join drTemp In dtTemp On drTable.Field(Of String)("ADate") Equals drTemp.Field(Of String)("ADate") And _
                             drTable.Field(Of Integer)("employeeunkid") Equals drTemp.Field(Of Integer)("employeeunkid") Into Grp = Group Select drTable

                'Pinkal (02-Mar-2017) -- End


                If dr.Count > 0 Then
                    dr.ToList.ForEach(Function(x) DeleteRow(x, dtTable))
                End If

                'Pinkal (16-Feb-2017) -- End

            Else
                If mstrEmployeeIDs.Trim.Length > 0 Then
                    dtTable = New DataView(dsApproveList.Tables("List"), "employeeunkid  in (" & mstrEmployeeIDs & ") OR Mapuserunkid <= 0", "", DataViewRowState.CurrentRows).ToTable
                End If
            End If

            dgvEmpTimesheet.AutoGenerateColumns = False

            'Pinkal (13-Oct-2017) -- Start
            'Enhancement - Ref Id 62 Working on Global Budget Timesheet Change.
            'objdgcolhIsCheck.DataPropertyName = "ischeck"
            objdgcolhIsCheck.DataPropertyName = "ischecked"
            'Pinkal (13-Oct-2017) -- End

            dgcolhParticular.DataPropertyName = "Particulars"
            dgcolhApprover.DataPropertyName = "Approver"
            dgcolhApprovalDate.DataPropertyName = "approvaldate"
            dgcolhDonor.DataPropertyName = "fundname"
            dgcolhProject.DataPropertyName = "fundprojectname"
            dgcolhActivity.DataPropertyName = "activity_name"
            dgcolhHours.DataPropertyName = "activity_hrs"
            objdgcolhShiftHours.DataPropertyName = "ShiftHours"
            objdgcolhShiftHoursInSec.DataPropertyName = "ShiftHoursInSec"
            dgcolhTotalActivityHours.DataPropertyName = "TotalActivityHours"
            objdgcolhTotalActivityHoursInMin.DataPropertyName = "TotalActivityHoursInMin"
            dgcolhStatus.DataPropertyName = "status"
            objdgcolhActivityHrsInMins.DataPropertyName = "activity_hrsinMins"
            objdgcolhActivityPercentage.DataPropertyName = "percentage"
            objdgcolhPeriodID.DataPropertyName = "periodunkid"
            objdgcolhEmployeeID.DataPropertyName = "employeeunkid"
            objdgcolhFundSourceID.DataPropertyName = "fundsourceunkid"
            objdgcolhProjectID.DataPropertyName = "projectcodeunkid"
            objdgcolhActivityID.DataPropertyName = "activityunkid"
            objdgcolhIsGrp.DataPropertyName = "IsGrp"
            objdgcolhMappedUserId.DataPropertyName = "mapuserunkid"
            objdgcolhStatusID.DataPropertyName = "ApprovalStatusId"
            objdgcolhEmployeeName.DataPropertyName = "Employee"
            objdgcolhActivityDate.DataPropertyName = "activitydate"
            objdgcolhPriority.DataPropertyName = "priority"
            objdgcolhApproverunkid.DataPropertyName = "tsapproverunkid"
            objdgcolhApproverEmployeeID.DataPropertyName = "approveremployeeunkid"
            objdgcolhEmpTimesheetID.DataPropertyName = "emptimesheetunkid"
            objdgcolhTimesheetApprovalID.DataPropertyName = "timesheetapprovalunkid"
            objdgcolhIsExternalApprover.DataPropertyName = "isexternalapprover"


            'Pinkal (28-Mar-2018) -- Start
            'Enhancement - (RefNo: 199)  Working on Additional column to show the description filled by the employee, on timesheet submission screen, view completed submit for approval and timesheet approval].
            dgcolhEmpDescription.DataPropertyName = "description"
            'Pinkal (28-Mar-2018) -- End

            'Pinkal (28-Mar-2018) -- Start
            'Enhancement - (RefNo: 198)  Working on Project Targeted/Acutal Hours Details.
            objdgcolhpercentage.DataPropertyName = "percentage"
            'Pinkal (28-Mar-2018) -- End


            'Pinkal (28-Jul-2018) -- Start
            'Enhancement - Doing for PACT Budget Timesheet Requirment [Ref #249,250,251]
            dgcolhSubmissionRemark.DataPropertyName = "submission_remark"
            'Pinkal (28-Jul-2018) -- End

            'Pinkal (13-Aug-2018) -- Start
            'Enhancement - Changes For PACT [Ref #249,252]
            objdgcolhIsHoliday.DataPropertyName = "isholiday"
            'Pinkal (13-Aug-2018) -- End

            dgvEmpTimesheet.DataSource = dtTable
            SetGridColor()

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillList", mstrModuleName)
        End Try
    End Sub

    Private Sub ResetValue()
        Try
            cboPeriod.SelectedValue = 0
            dtpDate.Value = ConfigParameter._Object._CurrentDateAndTime.Date
            cboEmployee.SelectedValue = 0
            cboDonor.SelectedValue = 0
            cboProject.SelectedValue = 0
            cboActivity.SelectedValue = 0
            'txtHours.Text = ""
            'txtDescription.Text = ""
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "ResetValue", mstrModuleName)
        End Try
    End Sub

    Private Sub SetGridColor()
        Try
            'Pinkal (13-Aug-2018) -- Start
            'Enhancement - Changes For PACT [Ref #249,252]

            Dim dr = From p As DataGridViewRow In dgvEmpTimesheet.Rows.Cast(Of DataGridViewRow)() Where CBool(p.Cells(objdgcolhIsGrp.Index).Value) = True Select p
            dr.ToList.ForEach(Function(x) SetRowStyle(x, True))

            Dim drDays = From p As DataGridViewRow In dgvEmpTimesheet.Rows.Cast(Of DataGridViewRow)() _
                             Where CBool(p.Cells(objdgcolhIsHoliday.Index).Value) = True Select p
            drDays.ToList.ForEach(Function(x) SetRowStyle(x, False))

            'Pinkal (13-Aug-2018) -- End

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetGridColor", mstrModuleName)
        Finally
        End Try
    End Sub

    'Pinkal (13-Aug-2018) -- Start
    'Enhancement - Changes For PACT [Ref #249,252]

    Public Function SetRowStyle(ByVal xRow As DataGridViewRow, ByVal isHeader As Boolean) As Boolean
        Try
            If isHeader Then
            Dim dgvcsHeader As New DataGridViewCellStyle
            dgvcsHeader.ForeColor = Color.White
            dgvcsHeader.SelectionBackColor = Color.Gray
            dgvcsHeader.SelectionForeColor = Color.White
            dgvcsHeader.BackColor = Color.Gray
            xRow.DefaultCellStyle = dgvcsHeader
            xRow.Cells(dgcolhParticular.Index).Value = xRow.Cells(dgcolhParticular.Index).Value.ToString & Space(10) & "[ " & Language.getMessage(mstrModuleName, 10, "Shift Hours:") & " " & xRow.Cells(objdgcolhShiftHours.Index).Value.ToString & " ]"
            Else
                xRow.DefaultCellStyle.ForeColor = Color.Red
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetRowStyle", mstrModuleName)
        End Try
        Return True
    End Function

    'Pinkal (13-Aug-2018) -- End

    Private Sub TextBoxTimeValidation(ByVal txt As MaskedTextBox)
        Try
            If txt.Text.Trim = ":" Then
                txt.Text = "00:00"
            ElseIf CDec(txt.Text.Trim.Replace(":", ".")) > 24 Then
Isvalid:
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 6, "Invalid working hrs.Please enter valid working hrs."), enMsgBoxStyle.Information)
                txt.Focus()
                Exit Sub

            ElseIf txt.Text.Trim.Contains(":") Then
                If txt.Text.Trim.Contains(":60") Then

calFromHr:
                    If CDec(CDec(txt.Text.Trim.Replace(":", ".")) + 0.4) <= 24 Then
                        txt.Text = CDec(CDec(txt.Text.Trim.Replace(":", ".")) + 0.4).ToString("#00.00").Replace(".", ":")
                    Else
                        GoTo Isvalid
                    End If
                ElseIf txt.Text <> "" And txt.Text <> ":" Then
                    If txt.Text.Trim.IndexOf(":") = 0 And txt.Text.Trim.Contains(":60") Then
                        GoTo calFromHr
                    ElseIf txt.Text.Substring(txt.Text.Trim.IndexOf(":"), txt.Text.Trim.Length - txt.Text.Trim.IndexOf(":")) = ":" Then
                        txt.Text = txt.Text & "00"
                    ElseIf CDec(IIf(txt.Text.Substring(txt.Text.Trim.IndexOf(":") + 1, txt.Text.Trim.Length - txt.Text.Trim.IndexOf(":") - 1) = "", "0", txt.Text.Substring(txt.Text.Trim.IndexOf(":") + 1, txt.Text.Trim.Length - txt.Text.Trim.IndexOf(":") - 1))) > 59 Then
                        GoTo calFromHr
                    End If
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "TextBoxTimeValidation", mstrModuleName)
        End Try
    End Sub

    Public Shadows Sub DoubleBuffered(ByVal dgv As DataGridView, ByVal setting As Boolean)
        Dim dgvType As Type = dgv.GetType()
        Dim pi As System.Reflection.PropertyInfo = dgvType.GetProperty("DoubleBuffered", Reflection.BindingFlags.Instance Or Reflection.BindingFlags.NonPublic)
        pi.SetValue(dgv, setting, Nothing)
    End Sub

    Public Function UpdateRow(ByVal dr As DataRow) As Boolean
        Try
            Dim drParentRow() As DataRow = CType(dgvEmpTimesheet.DataSource, DataTable).Select("isgrp = 1 AND emptimesheetunkid = " & CInt(dr("emptimesheetunkid")))
            If drParentRow.Length > 0 Then
                For Each drParent In drParentRow

                    'Pinkal (13-Oct-2017) -- Start
                    'Enhancement - Ref Id 62 Working on Global Budget Timesheet Change.
                    'drParent("ischeck") = chkSelectAll.Checked
                    drParent("ischecked") = chkSelectAll.Checked
                    'Pinkal (13-Oct-2017) -- End


                    drParent.AcceptChanges()
                Next
            End If

            'Pinkal (13-Oct-2017) -- Start
            'Enhancement - Ref Id 62 Working on Global Budget Timesheet Change.
            'dr("ischeck") = chkSelectAll.Checked
            dr("ischecked") = chkSelectAll.Checked
            'Pinkal (13-Oct-2017) -- End
            dr.AcceptChanges()

            'Dim mintIndex As Integer = CType(dgvEmpTimesheet.DataSource, DataTable).Rows.IndexOf(dr)
            'dgvEmpTimesheet.CurrentCell = dgvEmpTimesheet.Rows(mintIndex).Cells(objdgcolhIsCheck.Index)

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "UpdateRow", mstrModuleName)
        Finally
        End Try
        Return True
    End Function

    Private Sub SetCheckBoxValue()
        Try

            'Pinkal (13-Oct-2017) -- Start
            'Enhancement - Ref Id 62 Working on Global Budget Timesheet Change.
            'Dim drcheckRow As DataRow() = CType(dgvEmpTimesheet.DataSource, DataTable).Select("ischeck = True AND isgrp = 0")
            Dim drcheckRow As DataRow() = CType(dgvEmpTimesheet.DataSource, DataTable).Select("ischecked = True AND isgrp = 0")
            'Pinkal (13-Oct-2017) -- End
            Dim druncheckRowRow As DataRow() = CType(dgvEmpTimesheet.DataSource, DataTable).Select("isgrp = 0")

            RemoveHandler chkSelectAll.CheckedChanged, AddressOf chkSelectAll_CheckedChanged

            If drcheckRow.Length <= 0 Then
                chkSelectAll.CheckState = CheckState.Unchecked
            ElseIf drcheckRow.Length < druncheckRowRow.Length Then
                chkSelectAll.CheckState = CheckState.Indeterminate
            ElseIf drcheckRow.Length = druncheckRowRow.Length Then
                chkSelectAll.CheckState = CheckState.Checked
            End If

            AddHandler chkSelectAll.CheckedChanged, AddressOf chkSelectAll_CheckedChanged

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetCheckBoxValue", mstrModuleName)
        End Try

    End Sub


    'Pinkal (02-Mar-2017) -- Start
    'Enhancement - Working on AKFTZ Employee Budget Timesheet Report Enhancement.

    Private Function DeleteRow(ByVal dr As DataRow, ByVal dtTable As DataTable) As Boolean
        Try
            If dtTable IsNot Nothing AndAlso dtTable.Rows.Count > 0 Then
                Dim drRow() As DataRow = dtTable.Select("Adate = '" & dr("ADate").ToString() & "' AND employeeunkid = " & CInt(dr("employeeunkid")))
                If drRow.Length <= 1 Then
                    dtTable.Rows.Remove(drRow(0))
                    dtTable.AcceptChanges()
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "DeleteRow", mstrModuleName)
        End Try
        Return True
    End Function

    'Pinkal (02-Mar-2017) -- End


    'Pinkal (03-May-2017) -- Start
    'Enhancement - Working On Implementing privileges in Budget Timesheet Module.
    Private Sub SetVisibility()
        Try
            btnEdit.Enabled = User._Object.Privilege._AllowToChangeEmployeeBudgetTimesheetStatus


            'Pinkal (28-Jul-2018) -- Start
            'Enhancement - Doing for PACT Budget Timesheet Requirment [Ref #249,250,251]
            dgcolhActivity.Visible = ConfigParameter._Object._ShowBgTimesheetActivity
            dgcolhProject.Visible = ConfigParameter._Object._ShowBgTimesheetActivityProject
            objdgcolhShowDetails.Visible = ConfigParameter._Object._ShowBgTimesheetActivityHrsDetail
            'Pinkal (28-Jul-2018) -- End

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetVisibility", mstrModuleName)
        End Try
    End Sub
    'Pinkal (03-May-2017) -- End



#End Region

#Region " Combobox's Events "

    Private Sub cboPeriod_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboPeriod.SelectedIndexChanged
        Try
            'Pinkal (31-Oct-2017) -- Start
            'Enhancement - Solve Budget Timesheet Issue As Per Suzan's Comment.
            RemoveHandler dtpDate.ValueChanged, AddressOf dtpDate_ValueChanged
            dtpDate.MinDate = CDate(eZeeDate.convertDate("17530101")).Date
            dtpDate.MaxDate = CDate(eZeeDate.convertDate("99981231")).Date
            AddHandler dtpDate.ValueChanged, AddressOf dtpDate_ValueChanged
            'Pinkal (31-Oct-2017) -- End

            If CInt(cboPeriod.SelectedValue) > 0 Then

                'Pinkal (05-Jun-2017) -- Start
                'Enhancement - Implemented TnA Period for PSI Malawi AS Per Suzan's Request .


                'Dim dRowView As DataRowView = CType(cboPeriod.SelectedItem, DataRowView)
                'If dRowView IsNot Nothing Then
                '    dtpDate.MinDate = eZeeDate.convertDate(dRowView("start_date").ToString()).Date
                '    dtpDate.MaxDate = eZeeDate.convertDate(dRowView("end_date").ToString()).Date
                '    dtpDate.Value = dtpDate.MinDate
                'Else
                '    GoTo DateRange
                'End If

                Dim objPeriod As New clscommom_period_Tran
                objPeriod._Periodunkid(FinancialYear._Object._DatabaseName) = CInt(cboPeriod.SelectedValue)
                dtpDate.MinDate = objPeriod._TnA_StartDate.Date
                dtpDate.MaxDate = objPeriod._TnA_EndDate.Date
                    dtpDate.Value = dtpDate.MinDate
                objPeriod = Nothing

            'Pinkal (05-Jun-2017) -- End

            Else
DateRange:
                dtpDate.Value = ConfigParameter._Object._CurrentDateAndTime.Date
            End If
            dtpDate.Checked = False
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboPeriod_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub cboEmployee_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboEmployee.SelectedIndexChanged
        Try
            If CInt(cboEmployee.SelectedValue) > 0 Then
                Dim objBudgetMst As New clsBudget_MasterNew
                Dim dsList As DataSet = objBudgetMst.GetComboList("List", False, True, "allocationbyid = 0")
                dsList = Nothing
                objBudgetMst = Nothing
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboEmployee_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub cboDonor_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboDonor.SelectedIndexChanged
        Try
            Dim objProjectCode As New clsFundProjectCode
            Dim dsList As DataSet = objProjectCode.GetComboList("List", True, CInt(cboDonor.SelectedValue))
            cboProject.DisplayMember = "fundprojectname"
            cboProject.ValueMember = "fundprojectcodeunkid"
            cboProject.DataSource = dsList.Tables(0)
            cboProject.SelectedValue = 0
            dsList = Nothing
            objProjectCode = Nothing
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboDonor_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub cboProject_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboProject.SelectedIndexChanged
        Try
            Dim objActivity As New clsfundactivity_Tran
            Dim dsList As DataSet = objActivity.GetComboList("List", True, CInt(cboProject.SelectedValue))
            cboActivity.DisplayMember = "activityname"
            cboActivity.ValueMember = "fundactivityunkid"
            cboActivity.DataSource = dsList.Tables(0)
            cboActivity.SelectedValue = 0
            dsList = Nothing
            objActivity = Nothing
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboProject_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub


    'Private Sub cboPeriod_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboPeriod.GotFocus, cboEmployee.GotFocus, cboDonor.GotFocus, cboProject.GotFocus, cboActivity.GotFocus
    '    Try
    '        With CType(sender, ComboBox)
    '            .ForeColor = Color.Black
    '            .Font = New Font(Me.Font.FontFamily, Me.Font.Size, FontStyle.Regular)
    '            If .Text = mstrSearchText Then
    '                .Text = ""
    '            End If
    '        End With
    '    Catch ex As Exception
    '        DisplayError.Show("-1", ex.Message, "cboPeriod_GotFocus", mstrModuleName)
    '    End Try
    'End Sub

    'Private Sub cboPeriod_Leave(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboPeriod.Leave, cboEmployee.Leave, cboDonor.Leave, cboProject.Leave, cboActivity.Leave
    '    Try
    '        If CInt(CType(sender, ComboBox).SelectedValue) <= 0 Then
    '            Call SetDefaultSearchText(CType(sender, ComboBox))
    '        End If
    '    Catch ex As Exception
    '        DisplayError.Show("-1", ex.Message, "cboPeriod_Leave", mstrModuleName)
    '    End Try
    'End Sub

    'Private Sub cboPeriod_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles cboPeriod.KeyPress, cboEmployee.KeyPress, cboDonor.KeyPress, cboProject.KeyPress, cboActivity.KeyPress
    '    Try
    '        If (CInt(AscW(e.KeyChar)) >= 65 AndAlso CInt(AscW(e.KeyChar)) <= 90) Or (CInt(AscW(e.KeyChar)) >= 97 AndAlso CInt(AscW(e.KeyChar)) <= 122) Or (CInt(AscW(e.KeyChar)) >= 47 AndAlso CInt(AscW(e.KeyChar)) <= 57) Then
    '            Dim frm As New frmCommonSearch
    '            With frm
    '                .ValueMember = CType(sender, ComboBox).ValueMember
    '                .DisplayMember = CType(sender, ComboBox).DisplayMember
    '                .DataSource = CType(CType(sender, ComboBox).DataSource, DataTable)
    '                If CType(sender, ComboBox).Name = cboPeriod.Name Then
    '                    .CodeMember = "code"
    '                ElseIf CType(sender, ComboBox).Name = cboEmployee.Name Then
    '                    .CodeMember = "employeecode"
    '                ElseIf CType(sender, ComboBox).Name = cboDonor.Name Then
    '                    .CodeMember = "fundcode"
    '                ElseIf CType(sender, ComboBox).Name = cboProject.Name Then
    '                    .CodeMember = "fundprojectcode"
    '                ElseIf CType(sender, ComboBox).Name = cboActivity.Name Then
    '                    .CodeMember = "activitycode"
    '                End If



    '            End With

    '            Dim c As Char = Convert.ToChar(e.KeyChar)
    '            frm.TypedText = c.ToString

    '            If frm.DisplayDialog Then
    '                CType(sender, ComboBox).SelectedValue = frm.SelectedValue
    '                CType(sender, ComboBox).Tag = frm.SelectedAlias
    '                e.KeyChar = ChrW(Keys.ShiftKey)
    '            Else
    '                CType(sender, ComboBox).Text = ""
    '                CType(sender, ComboBox).Tag = ""
    '            End If
    '        End If
    '    Catch ex As Exception
    '        DisplayError.Show("-1", ex.Message, "cboPeriod_KeyPress", mstrModuleName)
    '    End Try
    'End Sub

#End Region

#Region "DatePicker Events"

    Private Sub dtpDate_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles dtpDate.ValueChanged
        Try
            Dim objEmployee As New clsEmployee_Master

            'Pinkal (31-Oct-2017) -- Start
            'Enhancement - Solve Budget Timesheet Issue As Per Suzan's Comment.

            'Dim dsList As DataSet = objEmployee.GetEmployeeList(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid _
            '                                                      , Company._Object._Companyunkid, dtpDate.Value.Date _
            '                                                      , dtpDate.Value.Date, ConfigParameter._Object._UserAccessModeSetting _
            '                                                      , True, ConfigParameter._Object._IsIncludeInactiveEmp, "List", True, , , , , , , , , , , , , , , , , , )

            Dim dsList As DataSet = objEmployee.GetEmployeeList(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid _
                                                                 , Company._Object._Companyunkid, dtpDate.MinDate.Date _
                                                                 , dtpDate.MaxDate.Date, ConfigParameter._Object._UserAccessModeSetting _
                                                                    , True, ConfigParameter._Object._IsIncludeInactiveEmp, "List", True, , , , , , , , , , , , , , , , , , )

            'Pinkal (31-Oct-2017) -- End
          

            cboEmployee.DisplayMember = "employeename"
            cboEmployee.ValueMember = "employeeunkid"
            cboEmployee.DataSource = dsList.Tables(0)
            cboEmployee.SelectedValue = 0
            objEmployee = Nothing

            Dim lstIDs As List(Of String) = (From p In dsList.Tables(0) Where (CInt(p.Item("employeeunkid")) > 0) Select (p.Item("employeeunkid").ToString)).Distinct.ToList
            mstrEmployeeIDs = String.Join(",", CType(lstIDs.ToArray(), String()))

            dsList = Nothing

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dtpDate_ValueChanged", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Button's Events "

    Private Sub objbtnSearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearch.Click
        Try
            'Nilay (15-Nov-2016) -- Start
            'Enhancement : Working on Budget Employee Timesheet as Per Mr.Andrew Requirement
            If CInt(cboPeriod.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 9, "Period is compulsory information.Please Select Period."), enMsgBoxStyle.Information)
                cboPeriod.Select()
                Exit Sub
            End If
            'Nilay (15-Nov-2016) -- End


            'Pinkal (05-Dec-2016) -- Start
            'Enhancement - Working on Budget Timesheet Module As per Suzan Requirement.

            'If CInt(cboEmployee.SelectedValue) <= 0 Then
            '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 9, "Employee is compulsory information.Please Select Employee."), enMsgBoxStyle.Information)
            '    cboEmployee.Select()
            '    Exit Sub
            'End If

            'Pinkal (05-Dec-2016) -- End

            'Nilay (10 Jan 2017) -- Start
            'ISSUE #23: Enhancements: Budget Employee TimeSheet Email Notification
            chkSelectAll.Checked = False
            'Nilay (10 Jan 2017) -- End

            FillList()

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearch_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnSearchEmployee_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchEmployee.Click, objbtnSearchPeriod.Click _
                                                                                                                                                                           , objbtnSearchApproverStatus.Click
        Dim objfrm As New frmCommonSearch
        Dim dtList As DataTable
        Try
            Dim cboCombo As ComboBox = Nothing
            If CType(sender, eZee.Common.eZeeGradientButton).Name.ToUpper() = objbtnSearchPeriod.Name.ToUpper Then
                cboCombo = cboPeriod
            ElseIf CType(sender, eZee.Common.eZeeGradientButton).Name.ToUpper() = objbtnSearchEmployee.Name.ToUpper Then
                cboCombo = cboEmployee
            ElseIf CType(sender, eZee.Common.eZeeGradientButton).Name.ToUpper() = objbtnSearchDonor.Name.ToUpper Then
                cboCombo = cboDonor
            ElseIf CType(sender, eZee.Common.eZeeGradientButton).Name.ToUpper() = objbtnSearchProject.Name.ToUpper Then
                cboCombo = cboProject
            ElseIf CType(sender, eZee.Common.eZeeGradientButton).Name.ToUpper() = objbtnSearchActivity.Name.ToUpper Then
                cboCombo = cboActivity
            ElseIf CType(sender, eZee.Common.eZeeGradientButton).Name.ToUpper() = objbtnSearchStatus.Name.ToUpper Then
                cboCombo = cboStatus
            ElseIf CType(sender, eZee.Common.eZeeGradientButton).Name.ToUpper() = objbtnSearchApproverStatus.Name.ToUpper Then
                cboCombo = cboApproverStatus
            End If

            dtList = CType(cboCombo.DataSource, DataTable)
            With cboCombo
                objfrm.DataSource = dtList
                objfrm.ValueMember = .ValueMember
                objfrm.DisplayMember = .DisplayMember
                If cboCombo.Name.ToUpper = cboEmployee.Name.ToUpper Then objfrm.CodeMember = "employeecode"
                If objfrm.DisplayDialog Then
                    .SelectedValue = objfrm.SelectedValue
                End If
                .Focus()
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchEmployee_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnReset.Click
        Try
            cboPeriod.SelectedIndex = 0
            dtpDate.Value = ConfigParameter._Object._CurrentDateAndTime.Date
            'chkMyApprovals.Checked = False
            cboEmployee.SelectedIndex = 0
            cboDonor.SelectedIndex = 0
            cboProject.SelectedIndex = 0
            cboActivity.SelectedIndex = 0
            cboStatus.SelectedValue = 2
            cboApproverStatus.SelectedValue = 2
            chkSelectAll.Checked = False
            mstrAdvanceFilter = ""
            'Nilay (15-Nov-2016) -- Start
            'Enhancement : Working on Budget Employee Timesheet as Per Mr.Andrew Requirement
            'FillList()
            dgvEmpTimesheet.DataSource = Nothing
            'Nilay (15-Nov-2016) -- End

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnReset_Click", mstrModuleName)
        End Try
    End Sub

    'Nilay (02-Jan-2017) -- Start
    'Issue #33: Enhancement - Implementing Budget Employee Time Sheet
    'Private Sub btnEdit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEdit.Click
    '    Try
    '        Dim dtTable As DataTable = CType(dgvEmpTimesheet.DataSource, DataTable)
    '        If dtTable IsNot Nothing AndAlso dtTable.Rows.Count > 0 Then
    '            Dim drRow() As DataRow = dtTable.Select("ischeck = True")
    '            If drRow.Length <= 0 Then
    '                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please check atleast one transaction from the list to perform further operation on it."), enMsgBoxStyle.Information) '?1
    '                dgvEmpTimesheet.Select()
    '                Exit Sub
    '            Else
    '                dtTable = New DataView(dtTable, "ischeck = True", "", DataViewRowState.CurrentRows).ToTable
    '            End If
    '        End If

    '        'If CBool(dgvEmpTimesheet.SelectedRows(0).Cells(objdgcolhIsGrp.Index).Value) = True Then
    '        '    btnEdit.Enabled = False
    '        'Else
    '        '    btnEdit.Enabled = True
    '        'End If

    '        'If User._Object._Userunkid <> CInt(dgvEmpTimesheet.SelectedRows(0).Cells(objdgcolhMappedUserId.Index).Value) Then
    '        '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "You can't Edit this transaction detail. Reason: You are logged in into another user login."), enMsgBoxStyle.Information) '?1
    '        '    dgvEmpTimesheet.Select()
    '        '    Exit Sub
    '        'End If

    '        If CInt(dgvEmpTimesheet.SelectedRows(0).Cells(objdgcolhStatusID.Index).Value) = 1 Then
    '            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "You can't Edit checked transaction(s) detail. Reason: Some of the transaction(s) are already approved."), enMsgBoxStyle.Information) '?1
    '            dgvEmpTimesheet.Select()
    '            Exit Sub

    '        ElseIf CInt(dgvEmpTimesheet.SelectedRows(0).Cells(objdgcolhStatusID.Index).Value) = 3 Then
    '            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "You can't Edit checked transaction(s) detail. Reason: Some of the transaction(s) are already rejected."), enMsgBoxStyle.Information) '?1
    '            dgvEmpTimesheet.Select()
    '            Exit Sub
    '        End If


    '        'START FOR CHECK WHETHER LOWER LEVEL APPROVE OR NOT

    '        Dim dtList As DataTable = New DataView(dsApproveList.Tables(0), "employeeunkid = " & CInt(dgvEmpTimesheet.SelectedRows(0).Cells(objdgcolhEmployeeID.Index).Value) _
    '                                              & " AND ADate = '" & eZeeDate.convertDate(CDate(dgvEmpTimesheet.SelectedRows(0).Cells(objdgcolhActivityDate.Index).Value)).ToString() & "'" _
    '                                              & " AND activityunkid = " & CInt(dgvEmpTimesheet.SelectedRows(0).Cells(objdgcolhActivityID.Index).Value) _
    '                                              & " AND approveremployeeunkid <> " & CInt(dgvEmpTimesheet.SelectedRows(0).Cells(objdgcolhApproverEmployeeID.Index).Value), "", DataViewRowState.CurrentRows).ToTable


    '        If dtList.Rows.Count > 0 Then
    '            Dim objLevel As New clstsapproverlevel_master
    '            objLevel._Tslevelunkid = objApprover._Tslevelunkid
    '            For i As Integer = 0 To dtList.Rows.Count - 1


    '                If objLevel._Priority > CInt(dtList.Rows(i)("Priority")) Then

    '                    'START FOR CHECK CASE WHETHER ONE USER HAS MULTIPLE APPROVER WITH SAME LEVEL AND STATUS IS NOT PENDING

    '                    Dim dList As DataTable = New DataView(dtList, "tslevelunkid = " & CInt(dtList.Rows(i)("tslevelunkid")) & " AND (Approvalstatusid = 1 or Approvalstatusid = 7)", "", DataViewRowState.CurrentRows).ToTable

    '                    If dList.Rows.Count > 0 Then Continue For

    '                    'END FOR CHECK CASE WHETHER ONE USER HAS MULTIPLE APPROVER WITH SAME LEVEL AND STATUS IS NOT PENDING

    '                    If CInt(dtList.Rows(i)("Approvalstatusid")) = 2 Then
    '                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "You can't Edit checked transaction(s) detail. Reason: Some of the transaction(s) approval are still pending."), enMsgBoxStyle.Information)
    '                        Exit Sub

    '                    ElseIf CInt(dtList.Rows(i)("Approvalstatusid")) = 3 Then
    '                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "You can't Edit checked transaction(s) detail. Reason: Some of the transaction(s) are already rejected."), enMsgBoxStyle.Information)
    '                        Exit Sub

    '                    End If

    '                ElseIf objLevel._Priority <= CInt(dtList.Rows(i)("Priority")) Then

    '                    If CInt(dtList.Rows(i)("Approvalstatusid")) = 1 Then
    '                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "You can't Edit checked transaction(s) detail. Reason: Some of the transaction(s) are already approved."), enMsgBoxStyle.Information)
    '                        Exit Sub

    '                    ElseIf CInt(dtList.Rows(i)("Approvalstatusid")) = 3 Then
    '                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "You can't Edit checked transaction(s) detail. Reason: Some of the transaction(s) are already rejected."), enMsgBoxStyle.Information)
    '                        Exit Sub

    '                    End If

    '                End If

    '            Next

    '        End If


    '        'END FOR CHECK WHETHER LOWER LEVEL APPROVE OR NOT

    '        Dim objFrm As New frmTimesheetApproval
    '        'If objFrm.displayDialog(CInt(cboPeriod.SelectedValue), CInt(dgvEmpTimesheet.SelectedRows(0).Cells(objdgcolhApproverunkid.Index).Value), CInt(dgvEmpTimesheet.SelectedRows(0).Cells(objdgcolhApproverEmployeeID.Index).Value) _
    '        '                                , dtTable, dgvEmpTimesheet.SelectedRows(0).Cells(dgcolhApprover.Index).Value.ToString(), CInt(dgvEmpTimesheet.SelectedRows(0).Cells(objdgcolhPriority.Index).Value) _
    '        '                                 , CBool(dgvEmpTimesheet.SelectedRows(0).Cells(objdgcolhIsExternalApprover.Index).Value), enAction.ADD_ONE) Then
    '        '    chkSelectAll.Checked = False
    '        '    FillList()
    '        'End If
    '        If objFrm.displayDialog(CInt(cboPeriod.SelectedValue), CInt(dgvEmpTimesheet.SelectedRows(0).Cells(objdgcolhApproverunkid.Index).Value), CInt(dgvEmpTimesheet.SelectedRows(0).Cells(objdgcolhApproverEmployeeID.Index).Value) _
    '                                        , dtTable, dgvEmpTimesheet.SelectedRows(0).Cells(dgcolhApprover.Index).Value.ToString(), CBool(dgvEmpTimesheet.SelectedRows(0).Cells(objdgcolhIsExternalApprover.Index).Value), enAction.ADD_ONE) Then
    '            chkSelectAll.Checked = False
    '            FillList()
    '        End If


    '    Catch ex As Exception
    '        DisplayError.Show("-1", ex.Message, "btnEdit_Click", mstrModuleName)
    '    End Try
    'End Sub

    Private Sub btnEdit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEdit.Click
        Try
            'Nilay (27 Feb 2017) -- Start
            'ISSUE #23: Enhancements: Budget Employee TimeSheet Changes
            If gobjEmailList.Count > 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 11, "Sending Email(s) process is in progress. Please wait for sometime."), enMsgBoxStyle.Information)
                Exit Sub
            End If
            'Nilay (27 Feb 2017) -- End

            Dim dRow As DataRow()
            Dim dtTable As DataTable = CType(dgvEmpTimesheet.DataSource, DataTable)
            If dtTable IsNot Nothing AndAlso dtTable.Rows.Count > 0 Then
                'Pinkal (13-Oct-2017) -- Start
                'Enhancement - Ref Id 62 Working on Global Budget Timesheet Change.
                'dRow = dtTable.Select("ischeck = True")
                dRow = dtTable.Select("ischecked = True")
                'Pinkal (13-Oct-2017) -- End

                If dRow.Length <= 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please check atleast one transaction from the list to perform further operation on it."), enMsgBoxStyle.Information) '?1
                    dgvEmpTimesheet.Select()
                    Exit Sub
                Else

                    'Pinkal (13-Oct-2017) -- Start
                    'Enhancement - Ref Id 62 Working on Global Budget Timesheet Change.
                    'dtTable = New DataView(dtTable, "ischeck = True", "", DataViewRowState.CurrentRows).ToTable
                    dtTable = New DataView(dtTable, "ischecked = True", "", DataViewRowState.CurrentRows).ToTable
                    'Pinkal (13-Oct-2017) -- End
                End If
            Else
                Exit Sub
            End If

            For Each dR In dRow

                If CBool(dR.Item("IsGrp")) = True Then Continue For

                If CInt(dR.Item("ApprovalStatusId")) = 1 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "You can't Edit checked transaction(s) detail. Reason: Some of the transaction(s) are already approved."), enMsgBoxStyle.Information) '?1
                    dgvEmpTimesheet.Select()
                    Exit Sub
                ElseIf CInt(dR.Item("ApprovalStatusId")) = 3 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "You can't Edit checked transaction(s) detail. Reason: Some of the transaction(s) are already rejected."), enMsgBoxStyle.Information) '?1
                    dgvEmpTimesheet.Select()
                    Exit Sub
                End If

                'START FOR CHECK WHETHER LOWER LEVEL APPROVE OR NOT
                'Dim dtList As DataTable = New DataView(dsApproveList.Tables(0), "employeeunkid = " & CInt(dR.Item("employeeunkid")) _
                '                                       & " AND ADate = '" & eZeeDate.convertDate(CDate(dR.Item("activitydate"))).ToString() & "'" _
                '                                       & " AND activityunkid = " & CInt(dR.Item("activityunkid")) _
                '                                       & " AND approveremployeeunkid <> " & CInt(dR.Item("approveremployeeunkid")), _
                '                                       "", DataViewRowState.CurrentRows).ToTable


                'Pinkal (16-Feb-2017) -- Start
                'Enhancement - Working on Employee Budget Timesheet Report for Aga Khan.

                Dim dtList As DataTable = New DataView(dsApproveList.Tables(0), "employeeunkid = " & CInt(dR.Item("employeeunkid")) _
                                                       & " AND ADate = '" & eZeeDate.convertDate(CDate(dR.Item("activitydate"))).ToString() & "'" _
                                                       & " AND activityunkid = " & CInt(dR.Item("activityunkid")) _
                                                     & " AND approveremployeeunkid = " & CInt(dR.Item("approveremployeeunkid")), _
                                                       "", DataViewRowState.CurrentRows).ToTable


                'Pinkal (16-Feb-2017) -- End



                If dtList.Rows.Count > 0 Then
                    Dim objLevel As New clstsapproverlevel_master
                    objLevel._Tslevelunkid = objApprover._Tslevelunkid

                    For i As Integer = 0 To dtList.Rows.Count - 1

                        If objLevel._Priority > CInt(dtList.Rows(i)("Priority")) Then

                            'START FOR CHECK CASE WHETHER ONE USER HAS MULTIPLE APPROVER WITH SAME LEVEL AND STATUS IS NOT PENDING
                            Dim dList As DataTable = New DataView(dtList, "tslevelunkid = " & CInt(dtList.Rows(i)("tslevelunkid")) & " AND (Approvalstatusid = 1 or Approvalstatusid = 7)", "", DataViewRowState.CurrentRows).ToTable
                            If dList.Rows.Count > 0 Then Continue For
                            'END FOR CHECK CASE WHETHER ONE USER HAS MULTIPLE APPROVER WITH SAME LEVEL AND STATUS IS NOT PENDING
                            If CInt(dtList.Rows(i)("Approvalstatusid")) = 2 Then
                                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "You can't Edit checked transaction(s) detail. Reason: Some of the transaction(s) approval are still pending."), enMsgBoxStyle.Information)
                                Exit Sub
                            ElseIf CInt(dtList.Rows(i)("Approvalstatusid")) = 3 Then
                                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "You can't Edit checked transaction(s) detail. Reason: Some of the transaction(s) are already rejected."), enMsgBoxStyle.Information)
                                Exit Sub
                            End If

                        ElseIf objLevel._Priority <= CInt(dtList.Rows(i)("Priority")) Then

                            If CInt(dtList.Rows(i)("Approvalstatusid")) = 1 Then
                                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "You can't Edit checked transaction(s) detail. Reason: Some of the transaction(s) are already approved."), enMsgBoxStyle.Information)
                                Exit Sub
                            ElseIf CInt(dtList.Rows(i)("Approvalstatusid")) = 3 Then
                                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "You can't Edit checked transaction(s) detail. Reason: Some of the transaction(s) are already rejected."), enMsgBoxStyle.Information)
                                Exit Sub
                            End If
                        End If
                    Next

                End If
                'END FOR CHECK WHETHER LOWER LEVEL APPROVE OR NOT
            Next

            'Nilay (10 Jan 2017) -- Start
            'ISSUE #23: Enhancements: Budget Employee TimeSheet Email Notification
            objApprover = New clstsapprover_master
            Dim objtsLevel As New clstsapproverlevel_master
            objApprover._Tsapproverunkid = CInt(cboApprover.SelectedValue)
            objtsLevel._Tslevelunkid = objApprover._Tslevelunkid
            'Nilay (10 Jan 2017) -- End

            Dim objFrm As New frmTimesheetApproval
            If objFrm.displayDialog(CInt(cboPeriod.SelectedValue), CInt(objtsLevel._Priority), dtTable) Then 'Nilay (10 Jan 2017) -- [CInt(objtsLevel._Priority)]
                chkSelectAll.Checked = False
                FillList()
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnEdit_Click", mstrModuleName)
        End Try
    End Sub
    'Nilay (02-Jan-2017) -- End

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnClose_Click", mstrModuleName)
        End Try
    End Sub


#End Region

#Region "Datagrid Events"

    Private Sub dgvEmpTimesheet_ColumnWidthChanged(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewColumnEventArgs) Handles dgvEmpTimesheet.ColumnWidthChanged
        Try
            Dim rIsgrp As Rectangle = dgvEmpTimesheet.DisplayRectangle
            rIsgrp.Height = CInt(dgvEmpTimesheet.ColumnHeadersHeight / 2)
            dgvEmpTimesheet.Invalidate(rIsgrp)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgvEmpTimesheet_ColumnWidthChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub dgvEmpTimesheet_Scroll(ByVal sender As Object, ByVal e As System.Windows.Forms.ScrollEventArgs) Handles dgvEmpTimesheet.Scroll
        Try
            DoubleBuffered(dgvEmpTimesheet, True)
            dgvEmpTimesheet.PerformLayout()
            dgvEmpTimesheet.Refresh()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgvEmpTimesheet_Scroll", mstrModuleName)
        End Try
    End Sub

    Private Sub dgvEmpTimesheet_Paint(ByVal sender As Object, ByVal e As System.Windows.Forms.PaintEventArgs) Handles dgvEmpTimesheet.Paint
        Try
            For i As Integer = 0 To dgvEmpTimesheet.Rows.Count - 1
                If CBool(dgvEmpTimesheet.Rows(i).Cells(objdgcolhIsGrp.Index).Value) = True Then
                    Dim r1 As Rectangle = Nothing
                    Dim w2 As Integer = 0
                    Dim format As New StringFormat()
                    format.Alignment = StringAlignment.Near
                    format.LineAlignment = StringAlignment.Center
                    For j As Integer = 0 To dgvEmpTimesheet.ColumnCount - 7
                        If j = 0 Then r1 = dgvEmpTimesheet.GetCellDisplayRectangle(j, i, True)
                        w2 = dgvEmpTimesheet.GetCellDisplayRectangle(j + 1, i, True).Width
                        r1.Width = r1.Width + w2 - 1
                        e.Graphics.FillRectangle(New SolidBrush(Color.Gray), r1)
                    Next
                    e.Graphics.DrawString(dgvEmpTimesheet.Rows(i).Cells(dgcolhParticular.Index).Value.ToString(), New Font(dgvEmpTimesheet.DefaultCellStyle.Font, FontStyle.Bold), New SolidBrush(Color.White), r1, format)
                End If
            Next
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgvEmpTimesheet_Paint", mstrModuleName)
        End Try
    End Sub

    Private Sub dgvEmpTimesheet_CellPainting(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellPaintingEventArgs) Handles dgvEmpTimesheet.CellPainting
        Try
            If e.RowIndex < 0 Then Exit Sub
            If CBool(dgvEmpTimesheet.Rows(e.RowIndex).Cells(objdgcolhIsGrp.Index).Value) Then
                Dim r2 As Rectangle = e.CellBounds
                r2.Y = CInt(r2.Y + e.CellBounds.Height / 2)
                r2.Height = CInt(e.CellBounds.Height / 2)
                e.PaintBackground(r2, True)
                e.PaintContent(r2)
                e.Handled = True
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgvEmpTimesheet_CellPainting", mstrModuleName)
        End Try
    End Sub

    Private Sub dgvEmpTimesheet_CellContentClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvEmpTimesheet.CellContentClick, dgvEmpTimesheet.CellContentDoubleClick
        Try
            If e.RowIndex < 0 Then Exit Sub

            'Nilay (21 Mar 2017) -- Start
            'ISSUE #23: Enhancements: Budget Employee TimeSheet Changes
            If CBool(dgvEmpTimesheet.Rows(e.RowIndex).Cells(objdgcolhIsGrp.Index).Value) = True Then Exit Sub
            'Nilay (21 Mar 2017) -- End

            If e.ColumnIndex = objdgcolhIsCheck.Index Then

                If dgvEmpTimesheet.IsCurrentCellDirty Then
                    dgvEmpTimesheet.CommitEdit(DataGridViewDataErrorContexts.Commit)
                End If
                'Nilay (07 Feb 2017) -- Start
                'ISSUE #23: Enhancements: Budget Employee Timesheet 65.1 - ignore activity percentage restriction for the shift hrs
                'Dim drRow() As DataRow = CType(dgvEmpTimesheet.DataSource, DataTable).Select("isgrp = 1 AND emptimesheetunkid = " & CInt(dgvEmpTimesheet.Rows(e.RowIndex).Cells(objdgcolhEmpTimesheetID.Index).Value))
                'If drRow.Length > 0 Then
                '    For Each dr In drRow
                '        dr("ischeck") = CBool(CInt(dgvEmpTimesheet.Rows(e.RowIndex).Cells(objdgcolhIsCheck.Index).Value))
                '        dr.AcceptChanges()
                '    Next
                'End If
                'CType(dgvEmpTimesheet.DataSource, DataTable).AcceptChanges()

                Dim drRow = From dr In CType(dgvEmpTimesheet.DataSource, DataTable) Where CBool(dr("IsGrp")) = False _
                            AndAlso CInt(dr("employeeunkid")) = CInt(dgvEmpTimesheet.Rows(e.RowIndex).Cells(objdgcolhEmployeeID.Index).Value) _
                            AndAlso CStr(dr("ADate")) = eZeeDate.convertDate(CDate(dgvEmpTimesheet.Rows(e.RowIndex).Cells(objdgcolhActivityDate.Index).Value).Date).ToString() _
                            Select dr


                'Pinkal (13-Oct-2017) -- Start
                'Enhancement - Ref Id 62 Working on Global Budget Timesheet Change.
                'Dim dRow = From dr In CType(dgvEmpTimesheet.DataSource, DataTable) Where CBool(dr("IsGrp")) = False AndAlso CBool(dr("ischeck")) = False _
                '           AndAlso CInt(dr("employeeunkid")) = CInt(dgvEmpTimesheet.Rows(e.RowIndex).Cells(objdgcolhEmployeeID.Index).Value) _
                '           AndAlso CStr(dr("ADate")) = eZeeDate.convertDate(CDate(dgvEmpTimesheet.Rows(e.RowIndex).Cells(objdgcolhActivityDate.Index).Value).Date).ToString() _
                '           Select dr
                Dim dRow = From dr In CType(dgvEmpTimesheet.DataSource, DataTable) Where CBool(dr("IsGrp")) = False AndAlso CBool(dr("ischecked")) = False _
                           AndAlso CInt(dr("employeeunkid")) = CInt(dgvEmpTimesheet.Rows(e.RowIndex).Cells(objdgcolhEmployeeID.Index).Value) _
                           AndAlso CStr(dr("ADate")) = eZeeDate.convertDate(CDate(dgvEmpTimesheet.Rows(e.RowIndex).Cells(objdgcolhActivityDate.Index).Value).Date).ToString() _
                           Select dr
                'Pinkal (13-Oct-2017) -- End

                


                Dim gRow = From dr In CType(dgvEmpTimesheet.DataSource, DataTable) Where CBool(dr("IsGrp")) = True _
                           AndAlso CInt(dr("employeeunkid")) = CInt(dgvEmpTimesheet.Rows(e.RowIndex).Cells(objdgcolhEmployeeID.Index).Value) _
                           AndAlso CStr(dr("ADate")) = eZeeDate.convertDate(CDate(dgvEmpTimesheet.Rows(e.RowIndex).Cells(objdgcolhActivityDate.Index).Value).Date).ToString() _
                           Select dr


                'Dim drRow() As DataRow = dtTable.Select("IsGrp=0 AND employeeunkid=" & CInt(dgvEmpTimesheet.Rows(e.RowIndex).Cells(objdgcolhEmployeeID.Index).Value) & " AND ADate = '" & eZeeDate.convertDate(CDate(dgvEmpTimesheet.Rows(e.RowIndex).Cells(objdgcolhActivityDate.Index).Value).Date) & "'")
                'Dim dRow As DataRow() = dtTable.Select("IsGrp=0 AND ischeck=0 AND employeeunkid=" & CInt(dgvEmpTimesheet.Rows(e.RowIndex).Cells(objdgcolhEmployeeID.Index).Value) & " AND ADate = '" & eZeeDate.convertDate(CDate(dgvEmpTimesheet.Rows(e.RowIndex).Cells(objdgcolhActivityDate.Index).Value).Date) & "'")
                'Dim gRow As DataRow() = dtTable.Select("IsGrp=1 AND employeeunkid=" & CInt(dgvEmpTimesheet.Rows(e.RowIndex).Cells(objdgcolhEmployeeID.Index).Value) & " AND ADate = " & eZeeDate.convertDate(CDate(dgvEmpTimesheet.Rows(e.RowIndex).Cells(objdgcolhActivityDate.Index).Value).Date))


                'Pinkal (13-Oct-2017) -- Start
                'Enhancement - Ref Id 62 Working on Global Budget Timesheet Change.
                If drRow.Count = dRow.Count Then
                    If gRow.Count > 0 Then
                        'gRow(0).Item("ischeck") = False
                        gRow(0).Item("ischecked") = False
                    Else
                        'gRow(0).Item("ischeck") = True
                        gRow(0).Item("ischecked") = True
                    End If
                Else
                    'gRow(0).Item("ischeck") = True
                    gRow(0).Item("ischecked") = True
                End If
                'Pinkal (13-Oct-2017) -- End

                gRow(0).AcceptChanges()
                CType(dgvEmpTimesheet.DataSource, DataTable).AcceptChanges()

                'Pinkal (28-Mar-2018) -- Start
                'Enhancement - (RefNo: 198)  Working on Project Targeted/Acutal Hours Details.

            ElseIf e.ColumnIndex = objdgcolhShowDetails.Index Then

                Dim mintShiftID As Integer = 0
                Dim mintShiftWorkingHrs As Integer = 0
                Dim objEmpshift As New clsEmployee_Shift_Tran
                mintShiftID = objEmpshift.GetEmployee_Current_ShiftId(dtpDate.MinDate.Date, CInt(dgvEmpTimesheet.Rows(e.RowIndex).Cells(objdgcolhEmployeeID.Index).Value))
                objEmpshift = Nothing

                Dim objShiftTran As New clsshift_tran
                objShiftTran.GetShiftTran(mintShiftID)
                Dim drRow() As DataRow = objShiftTran._dtShiftday.Select("dayid = " & Weekday(dtpDate.MinDate.Date, FirstDayOfWeek.Sunday) - 1)
                If drRow.Length > 0 Then
                    mintShiftWorkingHrs = CInt(drRow(0)("workinghrsinsec"))
                End If
                objShiftTran = Nothing


                'Pinkal (16-May-2018) -- Start
                'Enhancement - Display Pending Budget timesheet Approval Employeee List Suggestion given by Suzan.

                Dim drPeriodRow As DataRowView = CType(cboPeriod.SelectedItem, DataRowView)
                Dim mdtPStartDate As Date = Nothing
                Dim mdtPEndDate As Date = Nothing
                If drRow IsNot Nothing Then
                    mdtPStartDate = eZeeDate.convertDate(drPeriodRow("start_date").ToString())
                    mdtPEndDate = eZeeDate.convertDate(drPeriodRow("end_date").ToString())
                End If

                Dim objFrm As New frmProjectHoursDetails
                'objFrm.displayDialog(CInt(dgvEmpTimesheet.Rows(e.RowIndex).Cells(objdgcolhEmployeeID.Index).Value), dgvEmpTimesheet.Rows(e.RowIndex).Cells(objdgcolhEmployeeName.Index).Value.ToString(), cboPeriod.Text _
                '                                    , dtpDate.MinDate.Date, dtpDate.MaxDate.Date, dgvEmpTimesheet.Rows(e.RowIndex).Cells(dgcolhProject.Index).Value.ToString() _
                '                                    , dgvEmpTimesheet.Rows(e.RowIndex).Cells(dgcolhDonor.Index).Value.ToString(), CInt(dgvEmpTimesheet.Rows(e.RowIndex).Cells(objdgcolhActivityID.Index).Value) _
                '                                    , dgvEmpTimesheet.Rows(e.RowIndex).Cells(dgcolhActivity.Index).Value.ToString() _
                '                                    , Convert.ToDouble(dgvEmpTimesheet.Rows(e.RowIndex).Cells(objdgcolhpercentage.Index).Value), CType(dgvEmpTimesheet.DataSource, DataTable), True)

                objFrm.displayDialog(CInt(dgvEmpTimesheet.Rows(e.RowIndex).Cells(objdgcolhEmployeeID.Index).Value), dgvEmpTimesheet.Rows(e.RowIndex).Cells(objdgcolhEmployeeName.Index).Value.ToString(), cboPeriod.Text _
                                                    , dtpDate.MinDate.Date, dtpDate.MaxDate.Date, dgvEmpTimesheet.Rows(e.RowIndex).Cells(dgcolhProject.Index).Value.ToString() _
                                                    , dgvEmpTimesheet.Rows(e.RowIndex).Cells(dgcolhDonor.Index).Value.ToString(), CInt(dgvEmpTimesheet.Rows(e.RowIndex).Cells(objdgcolhActivityID.Index).Value) _
                                                    , dgvEmpTimesheet.Rows(e.RowIndex).Cells(dgcolhActivity.Index).Value.ToString() _
                                                   , Convert.ToDouble(dgvEmpTimesheet.Rows(e.RowIndex).Cells(objdgcolhpercentage.Index).Value), CType(dgvEmpTimesheet.DataSource, DataTable), True _
                                                   , mdtPStartDate, mdtPEndDate)

                'Pinkal (16-May-2018) -- End


                'Pinkal (28-Mar-2018) -- End

            End If
            'Nilay (07 Feb 2017) -- End
            SetCheckBoxValue()


        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgvEmpTimesheet_CellContentClick", mstrModuleName)
        End Try
    End Sub


    'Pinkal (28-Mar-2018) -- Start
    'Enhancement - (RefNo: 198)  Working on Project Targeted/Acutal Hours Details.

    'Private Sub dgvTimesheet_CellContentClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvTimesheet.CellContentClick, dgvTimesheet.CellContentDoubleClick
    '    Try
    '        If e.ColumnIndex = objdgcolhShowDetails.Index Then

    '            Dim mstrEmployeeCode As String = ""
    '            Dim drEmpDataRow As DataRowView = CType(cboEmployee.SelectedItem, DataRowView)
    '            If drEmpDataRow IsNot Nothing Then
    '                mstrEmployeeCode = drEmpDataRow("employeecode").ToString()
    '            End If


    '            Dim objFrm As New frmProjectHoursDetails
    '            objFrm.displayDialog(CInt(cboEmployee.SelectedValue), mstrEmployeeCode & " - " & cboEmployee.Text, cboPeriod.Text _
    '                                                , mdtPeriodStartDate.Date, mdtPeriodEndDate.Date, dgvTimesheet.Rows(e.RowIndex).Cells(dgcolhEmpProject.Index).Value.ToString() _
    '                                                , dgvTimesheet.Rows(e.RowIndex).Cells(dgcolhEmpDonor.Index).Value.ToString(), CInt(dgvTimesheet.Rows(e.RowIndex).Cells(objdgcolhEmpActivityunkid.Index).Value) _
    '                                                , dgvTimesheet.Rows(e.RowIndex).Cells(dgcolhEmpActivity.Index).Value.ToString() _
    '                                                , Convert.ToDouble(dgvTimesheet.Rows(e.RowIndex).Cells(dgcolhEmpPercentage.Index).Value), CType(dgvEmpTimesheetList.DataSource, DataTable))
    '        End If
    '    Catch ex As Exception
    '        DisplayError.Show("-1", ex.Message, "dgvTimesheet_CellContentClick", mstrModuleName)
    '    End Try
    'End Sub

    'Pinkal (28-Mar-2018) -- End

#End Region

#Region "CheckBox Event"

    Private Sub chkMyApprovals_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkMyApprovals.CheckedChanged
        Try
            Me.Cursor = Cursors.WaitCursor
            'Nilay (07 Feb 2017) -- Start
            'ISSUE #23: Enhancements: Budget Employee Timesheet 65.1 - ignore activity percentage restriction for the shift hrs
            'FillList()
            'Nilay (07 Feb 2017) -- End
            Me.Cursor = Cursors.Default
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "chkMyApprovals_CheckedChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub chkSelectAll_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkSelectAll.CheckedChanged
        Try
            If dgvEmpTimesheet.Rows.Count > 0 Then
                Dim dtTable As DataTable = CType(dgvEmpTimesheet.DataSource, DataTable)
                If dtTable IsNot Nothing AndAlso dtTable.Rows.Count > 0 Then
                    'Nilay (07 Feb 2017) -- Start
                    'ISSUE #23: Enhancements: Budget Employee Timesheet 65.1 - ignore activity percentage restriction for the shift hrs
                    'Dim drRow = From dr As DataRow In dtTable Where CBool(dr("isgrp")) = False Select dr
                    Dim drRow = From dr As DataRow In dtTable Select dr
                    'Nilay (07 Feb 2017) -- End
                    dgvEmpTimesheet.SuspendLayout()
                    drRow.ToList().ForEach(Function(x) UpdateRow(x))
                    dgvEmpTimesheet.ResumeLayout()
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "chkSelectAll_CheckedChanged", mstrModuleName)
        End Try
    End Sub

#End Region

#Region "Linkbutton Event"

    Private Sub lnkAllocation_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lnkAllocation.LinkClicked
        Try
            Dim frm As New frmAdvanceSearch
            frm.ShowDialog()
            'Nilay (15-Nov-2016) -- Start
            'Enhancement : Working on Budget Employee Timesheet as Per Mr.Andrew Requirement
            'mstrAdvanceFilter = frm._GetFilterString
            mstrAdvanceFilter = frm._GetFilterString.Replace("ADF", "hremployee_master")
            'Nilay (15-Nov-2016) -- End

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lnkAllocation_LinkClicked", mstrModuleName)
        End Try
    End Sub

#End Region


    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Private Sub OtherSettings()
        Try
            Me.SuspendLayout()
            Call SetLanguage()

            Me.gbEmployeeTimesheet.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbEmployeeTimesheet.ForeColor = GUI._eZeeContainerHeaderForeColor


            Me.btnClose.GradientBackColor = GUI._ButttonBackColor
            Me.btnClose.GradientForeColor = GUI._ButttonFontColor

            Me.btnEdit.GradientBackColor = GUI._ButttonBackColor
            Me.btnEdit.GradientForeColor = GUI._ButttonFontColor


            Me.ResumeLayout()
        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "OtherSettings", mstrModuleName)
        End Try
    End Sub


    Private Sub SetLanguage()
        Try
            Me.Text = Language._Object.getCaption(Me.Name, Me.Text)

            Me.gbEmployeeTimesheet.Text = Language._Object.getCaption(Me.gbEmployeeTimesheet.Name, Me.gbEmployeeTimesheet.Text)
            Me.LblActivity.Text = Language._Object.getCaption(Me.LblActivity.Name, Me.LblActivity.Text)
            Me.LblProject.Text = Language._Object.getCaption(Me.LblProject.Name, Me.LblProject.Text)
            Me.LblDonor.Text = Language._Object.getCaption(Me.LblDonor.Name, Me.LblDonor.Text)
            Me.LblDate.Text = Language._Object.getCaption(Me.LblDate.Name, Me.LblDate.Text)
            Me.LblEmployee.Text = Language._Object.getCaption(Me.LblEmployee.Name, Me.LblEmployee.Text)
            Me.LblPeriod.Text = Language._Object.getCaption(Me.LblPeriod.Name, Me.LblPeriod.Text)
            Me.LblApprover.Text = Language._Object.getCaption(Me.LblApprover.Name, Me.LblApprover.Text)
            Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
            Me.btnEdit.Text = Language._Object.getCaption(Me.btnEdit.Name, Me.btnEdit.Text)
            Me.lnkAllocation.Text = Language._Object.getCaption(Me.lnkAllocation.Name, Me.lnkAllocation.Text)
            Me.LblStatus.Text = Language._Object.getCaption(Me.LblStatus.Name, Me.LblStatus.Text)
            Me.chkMyApprovals.Text = Language._Object.getCaption(Me.chkMyApprovals.Name, Me.chkMyApprovals.Text)
            Me.chkSelectAll.Text = Language._Object.getCaption(Me.chkSelectAll.Name, Me.chkSelectAll.Text)
            Me.LblApproverStatus.Text = Language._Object.getCaption(Me.LblApproverStatus.Name, Me.LblApproverStatus.Text)
            Me.dgcolhParticular.HeaderText = Language._Object.getCaption(Me.dgcolhParticular.Name, Me.dgcolhParticular.HeaderText)
            Me.dgcolhApprover.HeaderText = Language._Object.getCaption(Me.dgcolhApprover.Name, Me.dgcolhApprover.HeaderText)
            Me.dgcolhApprovalDate.HeaderText = Language._Object.getCaption(Me.dgcolhApprovalDate.Name, Me.dgcolhApprovalDate.HeaderText)
            Me.dgcolhActivity.HeaderText = Language._Object.getCaption(Me.dgcolhActivity.Name, Me.dgcolhActivity.HeaderText)
            Me.dgcolhDonor.HeaderText = Language._Object.getCaption(Me.dgcolhDonor.Name, Me.dgcolhDonor.HeaderText)
            Me.dgcolhProject.HeaderText = Language._Object.getCaption(Me.dgcolhProject.Name, Me.dgcolhProject.HeaderText)
            Me.dgcolhHours.HeaderText = Language._Object.getCaption(Me.dgcolhHours.Name, Me.dgcolhHours.HeaderText)
			Me.dgcolhEmpDescription.HeaderText = Language._Object.getCaption(Me.dgcolhEmpDescription.Name, Me.dgcolhEmpDescription.HeaderText)
			Me.dgcolhSubmissionRemark.HeaderText = Language._Object.getCaption(Me.dgcolhSubmissionRemark.Name, Me.dgcolhSubmissionRemark.HeaderText)
            Me.dgcolhTotalActivityHours.HeaderText = Language._Object.getCaption(Me.dgcolhTotalActivityHours.Name, Me.dgcolhTotalActivityHours.HeaderText)
            Me.dgcolhStatus.HeaderText = Language._Object.getCaption(Me.dgcolhStatus.Name, Me.dgcolhStatus.HeaderText)

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
        End Try
    End Sub


    Private Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Please check atleast one transaction from the list to perform further operation on it.")
            Language.setMessage(mstrModuleName, 2, "You can't Edit checked transaction(s) detail. Reason: Some of the transaction(s) approval are still pending.")
            Language.setMessage(mstrModuleName, 3, "You can't Edit checked transaction(s) detail. Reason: Some of the transaction(s) are already approved.")
            Language.setMessage(mstrModuleName, 4, "You can't Edit checked transaction(s) detail. Reason: Some of the transaction(s) are already rejected.")
            Language.setMessage(mstrModuleName, 6, "Invalid working hrs.Please enter valid working hrs.")
            Language.setMessage(mstrModuleName, 7, "Approved By :-")
            Language.setMessage(mstrModuleName, 8, "Rejected By :-")
            Language.setMessage(mstrModuleName, 9, "Period is compulsory information.Please Select Period.")
            Language.setMessage(mstrModuleName, 10, "Shift Hours:")
            Language.setMessage(mstrModuleName, 11, "Sending Email(s) process is in progress. Please wait for sometime.")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class