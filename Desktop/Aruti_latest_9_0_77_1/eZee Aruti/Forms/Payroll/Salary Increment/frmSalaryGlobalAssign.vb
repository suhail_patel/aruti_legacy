﻿Option Strict On
Imports eZeeCommonLib
Imports Aruti.Data
Imports System.Threading
Imports System.Web

Public Class frmSalaryGlobalAssign

#Region " Private Variables "
    Private ReadOnly mstrModuleName As String = "frmSalaryGlobalAssign"

    Private mblnCancel As Boolean = True

    Private objSalary As clsSalaryIncrement

    Private mdtPayPeriodStartDate As DateTime
    Private mdtPayPeriodEndDate As DateTime
    Private mdtTran As DataTable 'Sohail (12 Oct 2011)
    'Sohail (10 Feb 2012) -- Start
    'TRA - ENHANCEMENT
    Private mstrAppraisalEmpIdList As String = ""
    Private mintAppraisalPeriodUnkId As Integer = 0
    'Sohail (10 Feb 2012) -- End
    Private mdicAppraisal As Dictionary(Of String, ArrayList)  'Sohail (29 Dec 2012)

    'S.SANDEEP [ 18 SEP 2012 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Dim StrApprlMessage As String = String.Empty
    Dim dicApprlNotification As New Dictionary(Of String, String)
    Private trd As Thread
    'S.SANDEEP [ 18 SEP 2012 ] -- END
    'Sohail (12 Apr 2016) -- Start
    'Enhancement - 58.1 - Remove existing allocation filters and give allocation link on global salary change screen.
    Private mstrAdvanceFilter As String = ""
    'Sohail (12 Apr 2016) -- End

#End Region

#Region " Display Dialogue "
    Public Function DisplayDialog(Optional ByRef strAppraisalEmpIdList As String = "", Optional ByRef intAppraisalPeriodId As Integer = 0, Optional ByRef dicAppraisal As Dictionary(Of String, ArrayList) = Nothing) As Boolean 'Sohail (10 Feb 2012)
        'Sohail (29 Dec 2012) - [dicAppraisal]

        Try
            'Sohail (10 Feb 2012) -- Start
            'TRA - ENHANCEMENT
            mstrAppraisalEmpIdList = strAppraisalEmpIdList
            mintAppraisalPeriodUnkId = intAppraisalPeriodId
            'Sohail (10 Feb 2012) -- End
            mdicAppraisal = dicAppraisal 'Sohail (29 Dec 2012)

            Me.ShowDialog()

            'Sohail (10 Feb 2012) -- Start
            'TRA - ENHANCEMENT
            strAppraisalEmpIdList = mstrAppraisalEmpIdList
            intAppraisalPeriodId = mintAppraisalPeriodUnkId
            'Sohail (10 Feb 2012) -- End

            dicAppraisal = mdicAppraisal 'Sohail (29 Dec 2012) 

            Return Not mblnCancel
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "displayDialog", mstrModuleName)
        End Try
    End Function

#End Region

#Region " Private Methods "
    Private Sub SetColor()
        Try
            cboPayYear.BackColor = GUI.ColorComp
            cboPayPeriod.BackColor = GUI.ColorComp

            cboEmployee.BackColor = GUI.ColorOptional
            'Sohail (12 Apr 2016) -- Start
            'Enhancement - 58.1 - Remove existing allocation filters and give allocation link on global salary change screen.
            'cboDepartment.BackColor = GUI.ColorOptional
            'cboGrade.BackColor = GUI.ColorOptional
            'cboSections.BackColor = GUI.ColorOptional
            'cboClass.BackColor = GUI.ColorOptional
            'cboCostCenter.BackColor = GUI.ColorOptional
            'cboJob.BackColor = GUI.ColorOptional
            'cboPayPoint.BackColor = GUI.ColorOptional
            'cboUnit.BackColor = GUI.ColorOptional
            'Sohail (12 Apr 2016) -- End
            'Sohail (12 Oct 2011) -- Start
            txtPercentage.BackColor = GUI.ColorOptional
            'Sohail (08 Nov 2011) -- Start
            cboIncrementBy.BackColor = GUI.ColorComp
            'txtRemark.BackColor = GUI.ColorOptional
            cboReason.BackColor = GUI.ColorComp 'Anjan (01 Aug 2012)
            'Sohail (08 Nov 2011) -- End
            'Sohail (12 Oct 2011) -- End
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetColor", mstrModuleName)
        End Try
    End Sub

    Private Sub SetValue()
        Try
            With objSalary
                ._Periodunkid = CInt(cboPayPeriod.SelectedValue)
                ._Incrementdate = dtpIncrementdate.Value
                ._Userunkid = User._Object._Userunkid 'Sohail (14 Oct 2010)
                'Sohail (12 Oct 2011) -- Start
                ._Isgradechange = False
                ._Isfromemployee = False
                'Sohail (08 Nov 2011) -- Start
                '._Remark = txtRemark.Text.Trim
                ._Reason_Id = CInt(cboReason.SelectedValue)
                'Sohail (08 Nov 2011) -- End
                'Sohail (12 Oct 2011) -- End

                'Sohail (31 Mar 2012) -- Start
                'TRA - ENHANCEMENT
                ._Increment_Mode = CInt(cboIncrementBy.SelectedValue)
                If CInt(cboIncrementBy.SelectedValue) = enSalaryIncrementBy.Percentage Then
                    ._Percentage = txtPercentage.Decimal
                Else
                    ._Percentage = 0
                End If
                'Sohail (31 Mar 2012) -- End

                'Sohail (24 Sep 2012) -- Start
                'TRA - ENHANCEMENT
                If User._Object.Privilege._AllowToApproveSalaryChange = True Then
                    ._Isapproved = True
                    ._Approveruserunkid = User._Object._Userunkid
                Else
                    ._Isapproved = False
                End If
                'Sohail (24 Sep 2012) -- End

                'Sohail (21 Jan 2020) -- Start
                'NMB Enhancement # : Actual increment date on salary change screen for informational purpose.
                ._ActualDate = dtpActualDate.Value
                ._Arrears_Amount = 0
                ._EMI_Amount = 0
                ._NoOfInstallment = 1
                ._Arrears_Countryid = Company._Object._Localization_Country
                ._Approval_StatusUnkid = enApprovalStatus.PENDING
                ._FinalApproverUnkid = -1
                ._Arrears_StatusUnkid = 0 'enLoanStatus.IN_PROGRESS
                'Sohail (21 Jan 2020) -- End

                'S.SANDEEP [ 04 SEP 2012 ] -- START
                'ENHANCEMENT : TRA CHANGES
                ._IsCopyPrevoiusSLAB = chkCopyPreviousEDSlab.Checked
                ._IsOverwritePrevoiusSLAB = chkCopyPreviousEDSlab.Checked
                'S.SANDEEP [ 04 SEP 2012 ] -- END

                'Sohail (21 Jan 2020) -- Start
                'NMB Enhancement # : Actual increment date on salary change screen for informational purpose.
                ._ActualDate = dtpActualDate.Value
                'Sohail (21 Jan 2020) -- End

            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "", mstrModuleName)
        End Try
    End Sub

    Private Sub FillCombo()
        Dim objMaster As New clsMasterData
        'Sohail (12 Apr 2016) -- Start
        'Enhancement - 58.1 - Remove existing allocation filters and give allocation link on global salary change screen.
        'Dim objDept As New clsDepartment
        'Dim objGrade As New clsGrade
        'Dim objSection As New clsSections
        'Dim objAccess As New clsAccess
        'Dim objClass As New clsClass
        'Dim objCostCenter As New clscostcenter_master
        'Dim objService As New clsServices
        'Dim objJob As New clsJobs
        'Dim objPayPoint As New clspaypoint_master
        'Dim objUnit As New clsUnits
        'Sohail (12 Apr 2016) -- End
        Dim dsCombo As DataSet
        'Dim objEmployee As New clsEmployee_Master 'Sohail (06 Jan 2012)
        Dim objCommon As New clsCommon_Master 'Sohail (08 Nov 2011)

        Try

            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'dsCombo = objMaster.getComboListPAYYEAR("PayYear", True)
            dsCombo = objMaster.getComboListPAYYEAR(FinancialYear._Object._YearUnkid, FinancialYear._Object._FinancialYear_Name, Company._Object._Companyunkid, "PayYear", True)
            'S.SANDEEP [04 JUN 2015] -- END

            With cboPayYear
                .BeginUpdate()
                .ValueMember = "Id"
                .DisplayMember = "Name"
                .DataSource = dsCombo.Tables("PayYear")
                If .Items.Count > 0 Then .SelectedValue = 0
                .EndUpdate()
            End With

            'Sohail (06 Jan 2012) -- Start
            'TRA - ENHANCEMENT
            'dsCombo = objEmployee.GetEmployeeList("EmployeeList", True)
            'With cboEmployee
            '    .BeginUpdate()
            '    .ValueMember = "employeeunkid"
            '    .DisplayMember = "employeename"
            '    .DataSource = dsCombo.Tables("EmployeeList")
            '    If .Items.Count > 0 Then .SelectedIndex = 0
            '    .EndUpdate()
            'End With
            'Sohail (06 Jan 2012) -- End

            'Sohail (12 Apr 2016) -- Start
            'Enhancement - 58.1 - Remove existing allocation filters and give allocation link on global salary change screen.
            'dsCombo = objDept.getComboList("DepartmentList", True)
            'With cboDepartment
            '    .BeginUpdate()
            '    .ValueMember = "departmentunkid"
            '    .DisplayMember = "name"
            '    .DataSource = dsCombo.Tables("DepartmentList")
            '    If .Items.Count > 0 Then .SelectedIndex = 0
            '    .EndUpdate()
            'End With

            'dsCombo = objGrade.getComboList("GradeList", True)
            'With cboGrade
            '    .BeginUpdate()
            '    .ValueMember = "gradeunkid"
            '    .DisplayMember = "name"
            '    .DataSource = dsCombo.Tables("GradeList")
            '    If .Items.Count > 0 Then .SelectedIndex = 0
            '    .EndUpdate()
            'End With

            'dsCombo = objSection.getComboList("SectionList", True)
            'With cboSections
            '    .BeginUpdate()
            '    .ValueMember = "sectionunkid"
            '    .DisplayMember = "name"
            '    .DataSource = dsCombo.Tables("SectionList")
            '    If .Items.Count > 0 Then .SelectedIndex = 0
            '    .EndUpdate()
            'End With

            'dsCombo = objClass.getComboList("ClassList", True)
            'With cboClass
            '    .BeginUpdate()
            '    .ValueMember = "classesunkid"
            '    .DisplayMember = "name"
            '    .DataSource = dsCombo.Tables("ClassList")
            '    If .Items.Count > 0 Then .SelectedIndex = 0
            '    .EndUpdate()
            'End With

            'dsCombo = objCostCenter.getComboList("CostCenterList", True)
            'With cboCostCenter
            '    .BeginUpdate()
            '    .ValueMember = "CostCenterunkid"
            '    .DisplayMember = "CostCentername"
            '    .DataSource = dsCombo.Tables("CostCenterList")
            '    If .Items.Count > 0 Then .SelectedIndex = 0
            '    .EndUpdate()
            'End With

            'dsCombo = objJob.getComboList("JobList", True)
            'With cboJob
            '    .BeginUpdate()
            '    .ValueMember = "jobunkid"
            '    .DisplayMember = "name"
            '    .DataSource = dsCombo.Tables("JobList")
            '    If .Items.Count > 0 Then .SelectedIndex = 0
            '    .EndUpdate()
            'End With

            'dsCombo = objPayPoint.getListForCombo("PayPointList", True)
            'With cboPayPoint
            '    .BeginUpdate()
            '    .ValueMember = "paypointunkid"
            '    .DisplayMember = "name"
            '    .DataSource = dsCombo.Tables("PayPointList")
            '    If .Items.Count > 0 Then .SelectedIndex = 0
            '    .EndUpdate()
            'End With

            'dsCombo = objUnit.getComboList("UnitList", True)
            'With cboUnit
            '    .BeginUpdate()
            '    .ValueMember = "unitunkid"
            '    .DisplayMember = "name"
            '    .DataSource = dsCombo.Tables("UnitList")
            '    If .Items.Count > 0 Then .SelectedIndex = 0
            '    .EndUpdate()
            'End With
            'Sohail (12 Apr 2016) -- End

            'Sohail (08 Nov 2011) -- Start
            'Sohail (27 Apr 2016) -- Start
            'Enhancement - 61.1 - Priority Order on Grade Group, Grade and Grade Level; Auto Salary Increment to next Grade Level as per Priority Order; Grade filter on Wages Table; Export and Import for Wages Table for KBC.
            '#23 (KBC & Kenya Project Comments List.xls) - Auto Salary incrment Per employee Anniversary,Employee Anniversay,All KBC Employees have Annivesary date for Salary increment,date is sent  when employee is confimed ,the salary is done automatically by the system when annivesary is due from current level to next level and stops at the Last level (notch),unless changed to higher Grade
            'dsCombo = objMaster.getComboListSalaryIncrementBy(False, "Incr")
            dsCombo = objMaster.getComboListSalaryIncrementBy(False, "Incr", True)
            'Sohail (27 Apr 2016) -- End
            With cboIncrementBy
                .BeginUpdate()
                .ValueMember = "Id"
                .DisplayMember = "Name"
                .DataSource = dsCombo.Tables("Incr")
                If .Items.Count > 0 Then .SelectedIndex = 0
                .EndUpdate()
            End With

            dsCombo = objCommon.getComboList(clsCommon_Master.enCommonMaster.SALINC_REASON, True, "SalReason")
            With cboReason
                .BeginUpdate()
                .ValueMember = "masterunkid"
                .DisplayMember = "Name"
                .DataSource = dsCombo.Tables("SalReason")
                .SelectedValue = 0
                .EndUpdate()
            End With
            'Sohail (08 Nov 2011) -- End

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        Finally
            objMaster = Nothing
            'objEmployee = Nothing 'Sohail (06 Jan 2012)
            'Sohail (12 Apr 2016) -- Start
            'Enhancement - 58.1 - Remove existing allocation filters and give allocation link on global salary change screen.
            'objDept = Nothing
            'objGrade = Nothing
            'objSection = Nothing
            'objAccess = Nothing
            'objClass = Nothing
            'objCostCenter = Nothing
            'objService = Nothing
            'objJob = Nothing
            'objPayPoint = Nothing
            'objUnit = Nothing
            'Sohail (12 Apr 2016) -- End
            objCommon = Nothing 'Sohail (08 Nov 2011)
        End Try
    End Sub

    Private Sub FillList()
        Dim objEmployee As New clsEmployee_Master
        Dim dsEmployee As New DataSet
        Dim dsList As DataSet
        Dim dtTable As DataTable
        Dim intCount As Integer = 0
        Dim strEmployeeList As String = ""
        Dim mintGradeunkid As Integer
        Dim mintGradelevelunkid As Integer
        'Sohail (27 Apr 2016) -- Start
        'Enhancement - 61.1 - Priority Order on Grade Group, Grade and Grade Level; Auto Salary Increment to next Grade Level as per Priority Order; Grade filter on Wages Table; Export and Import for Wages Table for KBC.
        '#23 (KBC & Kenya Project Comments List.xls) - Auto Salary incrment Per employee Anniversary,Employee Anniversay,All KBC Employees have Annivesary date for Salary increment,date is sent  when employee is confimed ,the salary is done automatically by the system when annivesary is due from current level to next level and stops at the Last level (notch),unless changed to higher Grade
        Dim mintGradeGroupunkid As Integer
        Dim mintGradeGroupPriority As Integer
        Dim mintGradePriority As Integer
        Dim mintGradeLevelPriority As Integer
        'Sohail (27 Apr 2016) -- End
        'Sohail (12 Oct 2011) -- Start
        Dim decCurScale As Decimal
        Dim decIncrAmt As Decimal
        Dim decMaxScale As Decimal
        Dim blnScaleInfoFound As Boolean = False
        Dim blnMaxLevelReached As Boolean = False
        'Sohail (12 Oct 2011) -- End
        Dim strFilter As String = "" 'Sohail (20 Jun 2012)
        Try
            'Sohail (03 Nov 2010) -- Start
            'dsEmployee = objEmployee.GetEmployeeList("Employee", False, True _
            '                                                      , CInt(cboEmployee.SelectedValue) _
            '                                                      , CInt(cboDepartment.SelectedValue) _
            '                                                      , CInt(cboSections.SelectedValue) _
            '                                                      , CInt(cboUnit.SelectedValue) _
            '                                                      , CInt(cboGrade.SelectedValue) _
            '                                                      , 0 _
            '                                                      , CInt(cboClass.SelectedValue) _
            '                                                      , CInt(cboCostCenter.SelectedValue) _
            '                                                      , 0 _
            '                                                      , CInt(cboJob.SelectedValue) _
            '                                                      , CInt(cboPayPoint.SelectedValue) _
            '                                                    )
            'Sohail (03 Nov 2010) -- End

            Dim lvItem As ListViewItem
            Dim lvArray As New List(Of ListViewItem) 'Sohail (03 Nov 2010)
            lvEmployeeList.Items.Clear()
            'lvEmployeeList.BeginUpdate() 'Sohail (08 Nov 2011)

            'Sohail (12 Oct 2011) -- Start
            objlblMaxLevelReachedColor.Visible = False
            lblMaxLevelReachedMsg.Visible = False
            'Sohail (12 Oct 2011) -- End
            'Sohail (27 Apr 2016) -- Start
            'Enhancement - 61.1 - Priority Order on Grade Group, Grade and Grade Level; Auto Salary Increment to next Grade Level as per Priority Order; Grade filter on Wages Table; Export and Import for Wages Table for KBC.
            '#23 (KBC & Kenya Project Comments List.xls) - Auto Salary incrment Per employee Anniversary,Employee Anniversay,All KBC Employees have Annivesary date for Salary increment,date is sent  when employee is confimed ,the salary is done automatically by the system when annivesary is due from current level to next level and stops at the Last level (notch),unless changed to higher Grade
            objlblGradeLevelChangedColor.Visible = False
            lblGradeLevelChangedMsg.Visible = False
            If objchkSelectAll.CheckState <> CheckState.Unchecked Then objchkSelectAll.CheckState = CheckState.Unchecked
            'Sohail (27 Apr 2016) -- End

            'Sohail (08 Nov 2011) -- Start
            If CInt(cboPayPeriod.SelectedValue) <= 0 OrElse CInt(cboIncrementBy.SelectedValue) <= 0 Then Exit Try
            If CInt(cboIncrementBy.SelectedValue) = enSalaryIncrementBy.Amount OrElse CInt(cboIncrementBy.SelectedValue) = enSalaryIncrementBy.Percentage Then
                If txtPercentage.Decimal = 0 Then Exit Try
            End If
            Cursor.Current = Cursors.WaitCursor 'Sohail (10 Feb 2012)
            lvEmployeeList.BeginUpdate()
            'Sohail (08 Nov 2011) -- End

            'Sohail (29 Dec 2012) -- Start
            'TRA - ENHANCEMENT - Code Commented to allow to give increment more than one time in one period (Issue# 131)
            'If CInt(cboPayPeriod.SelectedValue) > 0 Then
            '    Dim ds As DataSet = objSalary.GetEmployeeList("Salary", mdtPayPeriodStartDate)
            '    For Each dsRow As DataRow In ds.Tables("Salary").Rows
            '        With dsRow
            '            If strEmployeeList.Length <= 0 Then
            '                strEmployeeList = .Item("employeeunkid").ToString
            '            Else
            '                strEmployeeList &= "," & .Item("employeeunkid").ToString
            '            End If
            '        End With
            '    Next
            'End If
            If CInt(cboPayPeriod.SelectedValue) > 0 Then
                'Sohail (21 Aug 2015) -- Start
                'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                'Dim ds As DataSet = objSalary.GetEmployeeList("Salary", dtpIncrementdate.Value.Date)
                'Sohail (27 Apr 2016) -- Start
                'Enhancement - 61.1 - Priority Order on Grade Group, Grade and Grade Level; Auto Salary Increment to next Grade Level as per Priority Order; Grade filter on Wages Table; Export and Import for Wages Table for KBC.
                '#23 (KBC & Kenya Project Comments List.xls) - Auto Salary incrment Per employee Anniversary,Employee Anniversay,All KBC Employees have Annivesary date for Salary increment,date is sent  when employee is confimed ,the salary is done automatically by the system when annivesary is due from current level to next level and stops at the Last level (notch),unless changed to higher Grade
                'Dim ds As DataSet = objSalary.GetEmployeeList(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, mdtPayPeriodStartDate, mdtPayPeriodEndDate, ConfigParameter._Object._UserAccessModeSetting, True, ConfigParameter._Object._IsIncludeInactiveEmp, "Salary", dtpIncrementdate.Value.Date, True, "")
                Dim ds As DataSet = objSalary.GetEmployeeList(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, mdtPayPeriodStartDate, mdtPayPeriodEndDate, ConfigParameter._Object._UserAccessModeSetting, True, ConfigParameter._Object._IsIncludeInactiveEmp, "Salary", dtpIncrementdate.Value.Date, True, "", , CInt(cboIncrementBy.SelectedValue))
                'Sohail (27 Apr 2016) -- End
                'Sohail (21 Aug 2015) -- End
                'Sohail (27 Apr 2016) -- Start
                'Enhancement - 61.1 - Priority Order on Grade Group, Grade and Grade Level; Auto Salary Increment to next Grade Level as per Priority Order; Grade filter on Wages Table; Export and Import for Wages Table for KBC.
                '#23 (KBC & Kenya Project Comments List.xls) - Auto Salary incrment Per employee Anniversary,Employee Anniversay,All KBC Employees have Annivesary date for Salary increment,date is sent  when employee is confimed ,the salary is done automatically by the system when annivesary is due from current level to next level and stops at the Last level (notch),unless changed to higher Grade
                'For Each dsRow As DataRow In ds.Tables("Salary").Rows
                '    With dsRow
                '        If strEmployeeList.Length <= 0 Then
                '            strEmployeeList = .Item("employeeunkid").ToString
                '        Else
                '            strEmployeeList &= "," & .Item("employeeunkid").ToString
                '        End If
                '    End With
                'Next
                strEmployeeList = String.Join(",", (From p In ds.Tables("Salary") Select (p.Item("employeeunkid").ToString)).ToArray)
                'Sohail (27 Apr 2016) -- End
            End If
            'Sohail (29 Dec 2012) -- End

            'Sohail (20 Jun 2012) -- Start
            'TRA - ENHANCEMENT
            If strEmployeeList.Length > 0 Then
                'Sohail (21 Aug 2015) -- Start
                'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                'strFilter = " employeeunkid NOT IN (" & strEmployeeList & ") "
                strFilter = " hremployee_master.employeeunkid NOT IN (" & strEmployeeList & ") "
                'Sohail (21 Aug 2015) -- End
            End If
            If mstrAppraisalEmpIdList.Trim.Length > 0 Then
                If strFilter = "" Then
                    'Sohail (21 Aug 2015) -- Start
                    'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                    'strFilter = " employeeunkid IN (" & mstrAppraisalEmpIdList & ") "
                    strFilter = " hremployee_master.employeeunkid IN (" & mstrAppraisalEmpIdList & ") "
                    'Sohail (21 Aug 2015) -- End
                Else
                    'Sohail (21 Aug 2015) -- Start
                    'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                    'strFilter &= " AND employeeunkid IN (" & mstrAppraisalEmpIdList & ") "
                    strFilter &= " AND hremployee_master.employeeunkid IN (" & mstrAppraisalEmpIdList & ") "
                    'Sohail (21 Aug 2015) -- End
                End If
            End If
            'Sohail (20 Jun 2012) -- End

            'Sohail (27 Apr 2016) -- Start
            'Enhancement - 61.1 - Priority Order on Grade Group, Grade and Grade Level; Auto Salary Increment to next Grade Level as per Priority Order; Grade filter on Wages Table; Export and Import for Wages Table for KBC.
            '#23 (KBC & Kenya Project Comments List.xls) - Auto Salary incrment Per employee Anniversary,Employee Anniversay,All KBC Employees have Annivesary date for Salary increment,date is sent  when employee is confimed ,the salary is done automatically by the system when annivesary is due from current level to next level and stops at the Last level (notch),unless changed to higher Grade
            Dim strCurrPeriodEmployees As String = ""
            If CInt(cboIncrementBy.SelectedValue) = enSalaryIncrementBy.Auto_Increment Then
                Dim objSalAnniv As New clsEmployee_salary_anniversary_tran
                Dim ds As DataSet = objSalAnniv.GetList(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, mdtPayPeriodStartDate, mdtPayPeriodEndDate, ConfigParameter._Object._UserAccessModeSetting, True, False, "List", True, , " A.anniversarymonth = " & Month(mdtPayPeriodEndDate) & " ", , )
                strCurrPeriodEmployees = String.Join(",", (From p In ds.Tables("List") Select (p.Item("employeeunkid").ToString)).ToArray)
                If strCurrPeriodEmployees.Trim = "" Then strCurrPeriodEmployees = "-1234"

                If strFilter = "" Then
                    strFilter = " hremployee_master.employeeunkid IN (" & strCurrPeriodEmployees & ") "
                Else
                    strFilter &= " AND hremployee_master.employeeunkid IN (" & strCurrPeriodEmployees & ") "
                End If
            End If
            'Sohail (27 Apr 2016) -- End

            'Sohail (03 Nov 2010) -- Start
            'Sohail (20 Jun 2012) -- Start
            'TRA - ENHANCEMENT
            'dsEmployee = objEmployee.GetEmployeeList("Employee", False, True _
            '                                              , CInt(cboEmployee.SelectedValue) _
            '                                              , CInt(cboDepartment.SelectedValue) _
            '                                              , CInt(cboSections.SelectedValue) _
            '                                              , CInt(cboUnit.SelectedValue) _
            '                                              , CInt(cboGrade.SelectedValue) _
            '                                              , 0 _
            '                                              , CInt(cboClass.SelectedValue) _
            '                                              , CInt(cboCostCenter.SelectedValue) _
            '                                              , 0 _
            '                                              , CInt(cboJob.SelectedValue) _
            '                                              , CInt(cboPayPoint.SelectedValue) _
            '                                              , mdtPayPeriodStartDate _
            '                                              , mdtPayPeriodEndDate _
            '                                            )
            'Anjan [10 June 2015] -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS

            'dtTable = objEmployee.GetEmployeeList("Employee", False, True _
            '                                              , CInt(cboEmployee.SelectedValue) _
            '                                              , CInt(cboDepartment.SelectedValue) _
            '                                              , CInt(cboSections.SelectedValue) _
            '                                              , CInt(cboUnit.SelectedValue) _
            '                                              , CInt(cboGrade.SelectedValue) _
            '                                              , 0 _
            '                                              , CInt(cboClass.SelectedValue) _
            '                                              , CInt(cboCostCenter.SelectedValue) _
            '                                              , 0 _
            '                                              , CInt(cboJob.SelectedValue) _
            '                                              , CInt(cboPayPoint.SelectedValue) _
            '                                              , mdtPayPeriodStartDate _
            '                                              , mdtPayPeriodEndDate _
            '                                             , , , , , _
            '                                             strFilter _
            '                                           ).Tables(0) 'Sohail (20 Jun 2012) - [strFilter]
            'Sohail (12 Apr 2016) -- Start
            'Enhancement - 58.1 - Remove existing allocation filters and give allocation link on global salary change screen.
            'dtTable = objEmployee.GetEmployeeList(FinancialYear._Object._DatabaseName, _
            '                               User._Object._Userunkid, _
            '                               FinancialYear._Object._YearUnkid, _
            '                               Company._Object._Companyunkid, _
            '                               mdtPayPeriodStartDate, _
            '                               mdtPayPeriodEndDate, _
            '                               ConfigParameter._Object._UserAccessModeSetting, _
            '                               True, False, "Employee", False, CInt(cboEmployee.SelectedValue), CInt(cboDepartment.SelectedValue), _
            '                               CInt(cboSections.SelectedValue), CInt(cboUnit.SelectedValue), CInt(cboGrade.SelectedValue), , CInt(cboClass.SelectedValue), CInt(cboCostCenter.SelectedValue), _
            '                               , CInt(cboJob.SelectedValue), CInt(cboPayPoint.SelectedValue), , , , strFilter).Tables(0)
            If mstrAdvanceFilter <> "" Then
                If strFilter.Trim <> "" Then
                    strFilter &= " AND " & mstrAdvanceFilter
                Else
                    strFilter = mstrAdvanceFilter
                End If
            End If

            dtTable = objEmployee.GetEmployeeList(FinancialYear._Object._DatabaseName, _
                                           User._Object._Userunkid, _
                                           FinancialYear._Object._YearUnkid, _
                                           Company._Object._Companyunkid, _
                                           mdtPayPeriodStartDate, _
                                           mdtPayPeriodEndDate, _
                                           ConfigParameter._Object._UserAccessModeSetting, _
                                           True, False, "Employee", False, CInt(cboEmployee.SelectedValue), , _
                                           , , , , , , _
                                           , , , , , , strFilter).Tables(0)
            'Sohail (12 Apr 2016) -- End
            'Anjan [10 June 2015] -- End

            'Sohail (20 Jun 2012) -- End
            'Sohail (03 Nov 2010) -- End

            'Sohail (20 Jun 2012) -- Start
            'TRA - ENHANCEMENT
            'If strEmployeeList.Length > 0 Then
            '    dtTable = New DataView(dsEmployee.Tables("Employee"), "employeeunkid NOT IN (" & strEmployeeList & ")", "", DataViewRowState.CurrentRows).ToTable
            'Else
            '    dtTable = New DataView(dsEmployee.Tables("Employee")).ToTable
            'End If
            'Sohail (20 Jun 2012) -- End

            'Sohail (10 Feb 2012) -- Start
            'TRA - ENHANCEMENT
            'Sohail (20 Jun 2012) -- Start
            'TRA - ENHANCEMENT
            'If mstrAppraisalEmpIdList.Trim.Length > 0 Then
            '    dtTable = New DataView(dtTable, "employeeunkid IN (" & mstrAppraisalEmpIdList & ")", "", DataViewRowState.CurrentRows).ToTable
            'End If
            'Sohail (20 Jun 2012) -- End
            'Sohail (10 Feb 2012) -- End

            'Sohail (20 Jun 2012) -- Start
            'TRA - ENHANCEMENT
            Dim objWages As New clsWagesTran
            Dim dtRow As DataRow
            Dim intRowCount As Integer = dtTable.Rows.Count
            'Sohail (20 Jun 2012) -- End

            'Sohail (20 Jun 2012) -- Start
            'TRA - ENHANCEMENT
            For i As Integer = 0 To intRowCount - 1
                dtRow = dtTable.Rows(i)
                'For Each dtRow As DataRow In dtTable.Rows
                'Sohail (20 Jun 2012) -- End
                lvItem = New ListViewItem
                With lvItem
                    .Text = ""
                    .Tag = dtRow.Item("employeeunkid").ToString
                    .SubItems.Add(dtRow.Item("employeecode").ToString)
                    .SubItems.Add(dtRow.Item("employeename").ToString)

                    'Sohail (16 Oct 2010) -- Start
                    mintGradeunkid = 0
                    mintGradelevelunkid = 0
                    'Sohail (27 Apr 2016) -- Start
                    'Enhancement - 61.1 - Priority Order on Grade Group, Grade and Grade Level; Auto Salary Increment to next Grade Level as per Priority Order; Grade filter on Wages Table; Export and Import for Wages Table for KBC.
                    '#23 (KBC & Kenya Project Comments List.xls) - Auto Salary incrment Per employee Anniversary,Employee Anniversay,All KBC Employees have Annivesary date for Salary increment,date is sent  when employee is confimed ,the salary is done automatically by the system when annivesary is due from current level to next level and stops at the Last level (notch),unless changed to higher Grade
                    mintGradeGroupunkid = 0
                    mintGradeGroupPriority = 0
                    mintGradePriority = 0
                    mintGradeLevelPriority = 0
                    'Sohail (27 Apr 2016) -- End
                    decCurScale = 0 'Sohail (12 Oct 2011)
                    'Sohail (07 Sep 2013) -- Start
                    'TRA - ENHANCEMENT
                    'dsList = objSalary.getLastIncrement("LastIncr", CInt(dtRow.Item("employeeunkid").ToString))
                    dsList = objSalary.getLastIncrement("LastIncr", CInt(dtRow.Item("employeeunkid").ToString), True)
                    'Sohail (07 Sep 2013) -- End
                    If dsList.Tables("LastIncr").Rows.Count > 0 Then

                        'Sohail (07 Sep 2013) -- Start
                        'TRA - ENHANCEMENT
                        If CBool(dsList.Tables("LastIncr").Rows(0).Item("isapproved")) = False Then Continue For
                        'Sohail (07 Sep 2013) -- End 

                        'Sohail (21 Jan 2020) -- Start
                        'NMB Enhancement # : Actual increment date on salary change screen for informational purpose.
                        If eZeeDate.convertDate(dtpActualDate.Value.Date) < dsList.Tables("LastIncr").Rows(0).Item("incrementdate").ToString Then Continue For
                        'Sohail (21 Jan 2020) -- End

                        'Sohail (20 Jun 2012) -- Start
                        'TRA - ENHANCEMENT
                        'Dim objGradeGroup As New clsGradeGroup
                        'objGradeGroup._Gradegroupunkid = CInt(dsList.Tables("LastIncr").Rows(0).Item("gradegroupunkid").ToString)
                        '.SubItems.Add(objGradeGroup._Name) 'Grade Group
                        'objGradeGroup = Nothing

                        'Dim objGrade As New clsGrade
                        'mintGradeunkid = CInt(dsList.Tables("LastIncr").Rows(0).Item("gradeunkid").ToString)
                        'objGrade._Gradeunkid = mintGradeunkid
                        '.SubItems.Add(objGrade._Name) 'Grade
                        'objGrade = Nothing

                        'Dim objGradeLevel As New clsGradeLevel
                        'mintGradelevelunkid = CInt(dsList.Tables("LastIncr").Rows(0).Item("gradelevelunkid").ToString)
                        'objGradeLevel._Gradelevelunkid = mintGradelevelunkid
                        '.SubItems.Add(objGradeLevel._Name) 'Grade Level
                        'objGradeLevel = Nothing
                        .SubItems.Add(dsList.Tables("LastIncr").Rows(0).Item("GradeGroupName").ToString) 'Grade Group
                        .SubItems.Add(dsList.Tables("LastIncr").Rows(0).Item("GradeName").ToString) 'Grade
                        .SubItems.Add(dsList.Tables("LastIncr").Rows(0).Item("GradeLevelName").ToString) 'Grade Level
                        .SubItems.Add(eZeeDate.convertDate(dsList.Tables("LastIncr").Rows(0).Item("incrementdate").ToString).ToShortDateString) 'Last Increment Date
                        'Sohail (20 Jun 2012) -- End

                        'Sohail (12 Oct 2011) -- Start
                        mintGradeunkid = CInt(dsList.Tables("LastIncr").Rows(0).Item("gradeunkid").ToString)
                        mintGradelevelunkid = CInt(dsList.Tables("LastIncr").Rows(0).Item("gradelevelunkid").ToString)
                        'Sohail (27 Apr 2016) -- Start
                        'Enhancement - 61.1 - Priority Order on Grade Group, Grade and Grade Level; Auto Salary Increment to next Grade Level as per Priority Order; Grade filter on Wages Table; Export and Import for Wages Table for KBC.
                        '#23 (KBC & Kenya Project Comments List.xls) - Auto Salary incrment Per employee Anniversary,Employee Anniversay,All KBC Employees have Annivesary date for Salary increment,date is sent  when employee is confimed ,the salary is done automatically by the system when annivesary is due from current level to next level and stops at the Last level (notch),unless changed to higher Grade
                        mintGradeGroupunkid = CInt(dsList.Tables("LastIncr").Rows(0).Item("gradegroupunkid").ToString)
                        mintGradeGroupPriority = CInt(dsList.Tables("LastIncr").Rows(0).Item("GradeGroupPriority").ToString)
                        mintGradePriority = CInt(dsList.Tables("LastIncr").Rows(0).Item("GradePriority").ToString)
                        mintGradeLevelPriority = CInt(dsList.Tables("LastIncr").Rows(0).Item("GradeLevelPriority").ToString)
                        'Sohail (27 Apr 2016) -- End
                        .SubItems(colhGradeGroup.Index).Tag = CInt(dsList.Tables("LastIncr").Rows(0).Item("gradegroupunkid").ToString)
                        .SubItems(colhGrade.Index).Tag = CInt(dsList.Tables("LastIncr").Rows(0).Item("gradeunkid").ToString)
                        .SubItems(colhLevel.Index).Tag = CInt(dsList.Tables("LastIncr").Rows(0).Item("gradelevelunkid").ToString)

                        decCurScale = CDec(dsList.Tables("LastIncr").Rows(0).Item("newscale").ToString)
                        '.SubItems.Add(Format(CDec(dsList.Tables("LastIncr").Rows(0).Item("newscale").ToString), GUI.fmtCurrency)) 'Current Scale 'Sohail (01 Dec 2010), 'Sohail (11 May 2011)
                        .SubItems.Add(Format(decCurScale, GUI.fmtCurrency)) 'Current Scale 'Sohail (01 Dec 2010), 'Sohail (11 May 2011)
                        'Sohail (12 Oct 2011) -- End


                    Else
                        'Sohail (21 Jan 2020) -- Start
                        'NMB Enhancement # : Actual increment date on salary change screen for informational purpose.
                        If dtpActualDate.Value.Date > mdtPayPeriodEndDate OrElse dtpActualDate.Value.Date < mdtPayPeriodStartDate Then Continue For
                        'Sohail (21 Jan 2020) -- End
                        .SubItems.Add("") 'Grade Group
                        .SubItems.Add("") 'Grade
                        .SubItems.Add("") 'Grade Level
                        .SubItems.Add("") 'Last Increment Date
                        .SubItems.Add("") 'Current Scale
                    End If
                    .SubItems(colhCurrScale.Index).Tag = decCurScale 'Sohail (12 Oct 2011)

                    'Sohail (12 Oct 2011) -- Start
                    decIncrAmt = 0
                    decMaxScale = 0
                    'Sohail (12 Oct 2011) -- End
                    'Dim objWages As New clsWagesTran 'Sohail (20 Jun 2012)
                    'Sohail (27 Apr 2016) -- Start
                    'Enhancement - 59.1 - Priority Order on Grade Group, Grade and Grade Level; Auto Salary Increment to next Grade Level as per Priority Order; Grade filter on Wages Table; Export and Import for Wages Table for KBC.
                    '#23 (KBC & Kenya Project Comments List.xls) - Auto Salary incrment Per employee Anniversary,Employee Anniversay,All KBC Employees have Annivesary date for Salary increment,date is sent  when employee is confimed ,the salary is done automatically by the system when annivesary is due from current level to next level and stops at the Last level (notch),unless changed to higher Grade
                    'dsList = objWages.getScaleInfo(mintGradeunkid, mintGradelevelunkid, "Scale")
                    dsList = objWages.getScaleInfo(mintGradeunkid, mintGradelevelunkid, mdtPayPeriodEndDate, "Scale")
                    blnScaleInfoFound = False
                    blnMaxLevelReached = False
                    Dim intNewGradeGroupId As Integer = 0
                    Dim strNewGradeGroupName As String = ""
                    Dim intNewGradeId As Integer = 0
                    Dim strNewGradeName As String = ""
                    Dim intNewGradeLevelId As Integer = 0
                    Dim strNewGradeLevelName As String = ""
                    'Sohail (27 Apr 2016) -- End
                    If dsList.Tables("Scale").Rows.Count > 0 Then
                        blnScaleInfoFound = True 'Sohail (27 Apr 2016)

                        'Sohail (12 Oct 2011) -- Start
                        '.SubItems.Add(Format(CDec(dsList.Tables("Scale").Rows(0).Item("increment").ToString), GUI.fmtCurrency)) 'Increment Amount 'Sohail (01 Dec 2010), 'Sohail (11 May 2011)
                        '.SubItems.Add(Format(CDec(dsList.Tables("Scale").Rows(0).Item("maximum").ToString), GUI.fmtCurrency)) 'Maximum Scale 'Sohail (01 Dec 2010), 'Sohail (11 May 2011)
                        If CInt(cboIncrementBy.SelectedValue) = enSalaryIncrementBy.Percentage Then 'Sohail (08 Nov 2011)
                            decIncrAmt = decCurScale * txtPercentage.Decimal / 100
                            'Sohail (08 Nov 2011) -- Start
                        ElseIf CInt(cboIncrementBy.SelectedValue) = enSalaryIncrementBy.Amount Then
                            decIncrAmt = txtPercentage.Decimal
                            'Sohail (08 Nov 2011) -- End
                        Else
                            decIncrAmt = CDec(dsList.Tables("Scale").Rows(0).Item("increment").ToString)
                        End If
                        decMaxScale = CDec(dsList.Tables("Scale").Rows(0).Item("maximum").ToString)
                        'Sohail (27 Apr 2016) -- Start
                        'Enhancement - 61.1 - Priority Order on Grade Group, Grade and Grade Level; Auto Salary Increment to next Grade Level as per Priority Order; Grade filter on Wages Table; Export and Import for Wages Table for KBC.
                        '#23 (KBC & Kenya Project Comments List.xls) - Auto Salary incrment Per employee Anniversary,Employee Anniversay,All KBC Employees have Annivesary date for Salary increment,date is sent  when employee is confimed ,the salary is done automatically by the system when annivesary is due from current level to next level and stops at the Last level (notch),unless changed to higher Grade
                        'If decMaxScale < (decCurScale + decIncrAmt) Then
                        If Math.Round(decMaxScale, 0) <= Math.Round((decCurScale + decIncrAmt), 0) Then
                            'Sohail (27 Apr 2016) -- End
                            decIncrAmt = decMaxScale - decCurScale

                            'Sohail (27 Apr 2016) -- Start
                            'Enhancement - 61.1 - Priority Order on Grade Group, Grade and Grade Level; Auto Salary Increment to next Grade Level as per Priority Order; Grade filter on Wages Table; Export and Import for Wages Table for KBC.
                            '#23 (KBC & Kenya Project Comments List.xls) - Auto Salary incrment Per employee Anniversary,Employee Anniversay,All KBC Employees have Annivesary date for Salary increment,date is sent  when employee is confimed ,the salary is done automatically by the system when annivesary is due from current level to next level and stops at the Last level (notch),unless changed to higher Grade
                            '.BackColor = System.Drawing.SystemColors.ControlDark
                            '.ForeColor = System.Drawing.SystemColors.ControlText

                            'objlblMaxLevelReachedColor.Visible = True
                            'lblMaxLevelReachedMsg.Visible = True
                            If Math.Round(decIncrAmt, 0) = 0 AndAlso CInt(cboIncrementBy.SelectedValue) = enSalaryIncrementBy.Auto_Increment Then
                                Dim ds As DataSet
                                objWages.GetNextGradeLevelPriority(mintGradeunkid, mintGradeLevelPriority, True, intNewGradeLevelId, strNewGradeLevelName)
                                If intNewGradeLevelId > 0 Then
                                    ds = objWages.getScaleInfo(mintGradeunkid, intNewGradeLevelId, mdtPayPeriodEndDate, "Scale")
                                    If ds.Tables("Scale").Rows.Count > 0 Then
                                        'decIncrAmt = CDec(ds.Tables("Scale").Rows(0).Item("increment").ToString)
                                        decMaxScale = CDec(ds.Tables("Scale").Rows(0).Item("maximum").ToString)

                                        If CDec(ds.Tables("Scale").Rows(0).Item("salary").ToString) > decCurScale Then
                                            decIncrAmt = CDec(ds.Tables("Scale").Rows(0).Item("salary").ToString) - decCurScale
                                        Else
                                            decIncrAmt = (CDec(ds.Tables("Scale").Rows(0).Item("salary").ToString) + CDec(ds.Tables("Scale").Rows(0).Item("increment").ToString)) - decCurScale
                                        End If

                                        If decMaxScale < (decCurScale + decIncrAmt) Then
                                            decIncrAmt = decMaxScale - decCurScale

                                            'If decIncrAmt = 0 Then
                                            '    Dim intNewGradeId As Integer = 0
                                            '    objWages.GetNextGradePriority(mintGradeGroupunkid, mintGradePriority, intNewGradeId)
                                            '    If intNewGradeId > 0 Then
                                            '        ds = objWages.getScaleInfo(intNewGradeId, intNewGradeLevelId, mdtPayPeriodEndDate, "Scale")
                                            '        If ds.Tables("Scale").Rows.Count > 0 Then
                                            '            decIncrAmt = CDec(ds.Tables("Scale").Rows(0).Item("increment").ToString)
                                            '            decMaxScale = CDec(ds.Tables("Scale").Rows(0).Item("maximum").ToString)

                                            '            If decMaxScale < (decCurScale + decIncrAmt) Then
                                            '                decIncrAmt = decMaxScale - decCurScale

                                            '                If decIncrAmt = 0 Then
                                            '                    Dim intNewGradeGroupId As Integer = 0
                                            '                    objWages.GetNextGradeGroupPriority(mintGradeGroupPriority, intNewGradeGroupId)
                                            '                    If intNewGradeGroupId > 0 Then
                                            '                        'ds = objWages.getScaleInfo(intNewGradeId, intNewGradeLevelId, mdtPayPeriodEndDate, "Scale")
                                            '                    End If
                                            '                End If
                                            '            End If
                                            '        End If
                                            '    End If
                                            'End If
                                            blnMaxLevelReached = True
                                        End If
                                    End If
                                Else
                                    objWages.GetNextGradePriority(mintGradeGroupunkid, mintGradePriority, True, intNewGradeId, strNewGradeName)
                                    If intNewGradeId > 0 Then
                                        objWages.GetNextGradeLevelPriority(intNewGradeId, 0, False, intNewGradeLevelId, strNewGradeLevelName)
                                        ds = objWages.getScaleInfo(intNewGradeId, intNewGradeLevelId, mdtPayPeriodEndDate, "Scale")
                                        If ds.Tables("Scale").Rows.Count > 0 Then
                                            'decIncrAmt = CDec(ds.Tables("Scale").Rows(0).Item("increment").ToString)
                                            decMaxScale = CDec(ds.Tables("Scale").Rows(0).Item("maximum").ToString)

                                            If CDec(ds.Tables("Scale").Rows(0).Item("salary").ToString) > decCurScale Then
                                                decIncrAmt = CDec(ds.Tables("Scale").Rows(0).Item("salary").ToString) - decCurScale
                                            Else
                                                decIncrAmt = (CDec(ds.Tables("Scale").Rows(0).Item("salary").ToString) + CDec(ds.Tables("Scale").Rows(0).Item("increment").ToString)) - decCurScale
                                            End If

                                            If decMaxScale < (decCurScale + decIncrAmt) Then
                                                decIncrAmt = decMaxScale - decCurScale

                                                'If decIncrAmt = 0 Then
                                                '    Dim intNewGradeGroupId As Integer = 0
                                                '    objWages.GetNextGradeGroupPriority(mintGradeGroupPriority, intNewGradeGroupId)
                                                '    If intNewGradeGroupId > 0 Then
                                                '        'ds = objWages.getScaleInfo(intNewGradeId, intNewGradeLevelId, mdtPayPeriodEndDate, "Scale")
                                                '    End If
                                                'End If
                                            End If
                                        End If
                                    Else
                                        objWages.GetNextGradeGroupPriority(mintGradeGroupPriority, intNewGradeGroupId, strNewGradeGroupName)
                                        If intNewGradeGroupId > 0 Then
                                            objWages.GetNextGradePriority(intNewGradeGroupId, 0, False, intNewGradeId, strNewGradeName)
                                            If intNewGradeId > 0 Then
                                                objWages.GetNextGradeLevelPriority(intNewGradeId, 0, False, intNewGradeLevelId, strNewGradeLevelName)
                                                ds = objWages.getScaleInfo(intNewGradeId, intNewGradeLevelId, mdtPayPeriodEndDate, "Scale")
                                                If ds.Tables("Scale").Rows.Count > 0 Then
                                                    'decIncrAmt = CDec(ds.Tables("Scale").Rows(0).Item("increment").ToString)
                                                    decMaxScale = CDec(ds.Tables("Scale").Rows(0).Item("maximum").ToString)

                                                    If CDec(ds.Tables("Scale").Rows(0).Item("salary").ToString) > decCurScale Then
                                                        decIncrAmt = CDec(ds.Tables("Scale").Rows(0).Item("salary").ToString) - decCurScale
                                                    Else
                                                        decIncrAmt = (CDec(ds.Tables("Scale").Rows(0).Item("salary").ToString) + CDec(ds.Tables("Scale").Rows(0).Item("increment").ToString)) - decCurScale
                                                    End If

                                                    If decMaxScale < (decCurScale + decIncrAmt) Then
                                                        decIncrAmt = decMaxScale - decCurScale

                                                        'If decIncrAmt = 0 Then
                                                        '    Dim intNewGradeGroupId As Integer = 0
                                                        '    objWages.GetNextGradeGroupPriority(mintGradeGroupPriority, intNewGradeGroupId)
                                                        '    If intNewGradeGroupId > 0 Then
                                                        '        'ds = objWages.getScaleInfo(intNewGradeId, intNewGradeLevelId, mdtPayPeriodEndDate, "Scale")
                                                        '    End If
                                                        'End If
                                                    End If
                                                End If
                                            Else
                                                blnMaxLevelReached = True
                                            End If
                                        Else
                                            blnMaxLevelReached = True
                                        End If
                                    End If
                                End If
                            Else
                                blnMaxLevelReached = True
                            End If
                            'Sohail (27 Apr 2016) -- End
                        End If
                        'Sohail (27 Apr 2016) -- Start
                        'Enhancement - 61.1 - Priority Order on Grade Group, Grade and Grade Level; Auto Salary Increment to next Grade Level as per Priority Order; Grade filter on Wages Table; Export and Import for Wages Table for KBC.
                        '#23 (KBC & Kenya Project Comments List.xls) - Auto Salary incrment Per employee Anniversary,Employee Anniversay,All KBC Employees have Annivesary date for Salary increment,date is sent  when employee is confimed ,the salary is done automatically by the system when annivesary is due from current level to next level and stops at the Last level (notch),unless changed to higher Grade
                        '.SubItems.Add(Format(decIncrAmt, GUI.fmtCurrency)) 'Increment Amount   
                        '.SubItems.Add(Format(decCurScale + decIncrAmt, GUI.fmtCurrency)) 'Expected New Scale
                        '.SubItems.Add(Format(decMaxScale, GUI.fmtCurrency)) 'Maximum Scale 
                        'Sohail (27 Apr 2016) -- End
                        'Sohail (12 Oct 2011) -- End
                    Else
                        'Sohail (27 Apr 2016) -- Start
                        'Enhancement - 61.1 - Priority Order on Grade Group, Grade and Grade Level; Auto Salary Increment to next Grade Level as per Priority Order; Grade filter on Wages Table; Export and Import for Wages Table for KBC.
                        '#23 (KBC & Kenya Project Comments List.xls) - Auto Salary incrment Per employee Anniversary,Employee Anniversay,All KBC Employees have Annivesary date for Salary increment,date is sent  when employee is confimed ,the salary is done automatically by the system when annivesary is due from current level to next level and stops at the Last level (notch),unless changed to higher Grade
                        '.SubItems.Add("") 'Increment Amount
                        '.SubItems.Add("") 'Expected New Scale 'Sohail (12 Oct 2011)
                        '.SubItems.Add("") 'Maximum Scale
                        'Sohail (27 Apr 2016) -- End
                    End If
                    'Sohail (12 Oct 2011) -- Start

                    'Sohail (27 Apr 2016) -- Start
                    'Enhancement - 61.1 - Priority Order on Grade Group, Grade and Grade Level; Auto Salary Increment to next Grade Level as per Priority Order; Grade filter on Wages Table; Export and Import for Wages Table for KBC.
                    '#23 (KBC & Kenya Project Comments List.xls) - Auto Salary incrment Per employee Anniversary,Employee Anniversay,All KBC Employees have Annivesary date for Salary increment,date is sent  when employee is confimed ,the salary is done automatically by the system when annivesary is due from current level to next level and stops at the Last level (notch),unless changed to higher Grade
                    If blnMaxLevelReached = True Then
                        .BackColor = System.Drawing.SystemColors.ControlDark
                        .ForeColor = System.Drawing.SystemColors.ControlText

                        objlblMaxLevelReachedColor.Visible = True
                        lblMaxLevelReachedMsg.Visible = True
                    End If

                    If blnScaleInfoFound = True Then
                        .SubItems.Add(Format(decIncrAmt, GUI.fmtCurrency)) 'Increment Amount   
                        .SubItems.Add(Format(decCurScale + decIncrAmt, GUI.fmtCurrency)) 'Expected New Scale
                        .SubItems.Add(Format(decMaxScale, GUI.fmtCurrency)) 'Maximum Scale 
                    Else
                        .SubItems.Add("") 'Increment Amount
                        .SubItems.Add("") 'Expected New Scale 
                        .SubItems.Add("") 'Maximum Scale
                    End If
                    'Sohail (27 Apr 2016) -- End

                    .SubItems(colhIncrAmt.Index).Tag = decIncrAmt
                    .SubItems(colhNewScale.Index).Tag = decCurScale + decIncrAmt
                    .SubItems(colhMaxScale.Index).Tag = decMaxScale
                    .SubItems.Add(CInt(cboIncrementBy.SelectedValue).ToString)
                    'Hemant (07 Jan 2019) -- Start
                    'ISSUE/ENHANCEMENT : {Audit Trails} in 76.1.
                    '.SubItems.Add(txtPercentage.Text)
                    If CInt(cboIncrementBy.SelectedValue) = enSalaryIncrementBy.Percentage Then
                    .SubItems.Add(txtPercentage.Text)
                    Else
                        .SubItems.Add("0.00")
                    End If
                    'Hemant (07 Jan 2019) -- End

                    'Sohail (27 Apr 2016) -- Start
                    'Enhancement - 61.1 - Priority Order on Grade Group, Grade and Grade Level; Auto Salary Increment to next Grade Level as per Priority Order; Grade filter on Wages Table; Export and Import for Wages Table for KBC.
                    '#23 (KBC & Kenya Project Comments List.xls) - Auto Salary incrment Per employee Anniversary,Employee Anniversay,All KBC Employees have Annivesary date for Salary increment,date is sent  when employee is confimed ,the salary is done automatically by the system when annivesary is due from current level to next level and stops at the Last level (notch),unless changed to higher Grade
                    If intNewGradeGroupId > 0 Then
                        .SubItems.Add(strNewGradeGroupName) 'New Grade Group
                        .SubItems(colhNewGradeGroup.Index).Tag = intNewGradeGroupId
                    Else
                        .SubItems.Add(.SubItems(colhGradeGroup.Index).Text) 'New Grade Group
                        .SubItems(colhNewGradeGroup.Index).Tag = CInt(.SubItems(colhGradeGroup.Index).Tag)
                    End If
                    If intNewGradeId > 0 Then
                        .SubItems.Add(strNewGradeName) 'New Grade
                        .SubItems(colhNewGrade.Index).Tag = intNewGradeId
                    Else
                        .SubItems.Add(.SubItems(colhGrade.Index).Text) 'New Grade
                        .SubItems(colhNewGrade.Index).Tag = CInt(.SubItems(colhGrade.Index).Tag)
                    End If
                    If intNewGradeLevelId > 0 Then
                        .SubItems.Add(strNewGradeLevelName) 'New Grade Level
                        .ForeColor = Color.Blue

                        .SubItems(colhNewGradeLevel.Index).Tag = intNewGradeLevelId
                        objlblGradeLevelChangedColor.Visible = True
                        lblGradeLevelChangedMsg.Visible = True
                    Else
                        .SubItems.Add(.SubItems(colhLevel.Index).Text) 'New Grade Level
                        .SubItems(colhNewGradeLevel.Index).Tag = CInt(.SubItems(colhLevel.Index).Tag)
                    End If
                    'Sohail (27 Apr 2016) -- End

                    'Sohail (12 Oct 2011) -- End
                    'objWages = Nothing 'Sohail (20 Jun 2012)
                    'Sohail (16 Oct 2010) -- End

                    'Sohail (03 Nov 2010) -- Start
                    'lvArray.Add(lvItem)
                    'Sohail (12 Oct 2011) -- Start
                    If decIncrAmt <> 0 Then
                        'RemoveHandler lvEmployeeList.ItemChecked, AddressOf lvEmployeeList_ItemChecked
                        'Sohail (12 Oct 2011) -- End
                        'lvEmployeeList.Items.Add(lvItem)
                        lvArray.Add(lvItem)
                        'Sohail (12 Oct 2011) -- Start
                        'AddHandler lvEmployeeList.ItemChecked, AddressOf lvEmployeeList_ItemChecked
                    End If
                    'Sohail (12 Oct 2011) -- End
                    'Sohail (03 Nov 2010) -- End
                End With
            Next
            RemoveHandler lvEmployeeList.ItemChecked, AddressOf lvEmployeeList_ItemChecked
            lvEmployeeList.Items.AddRange(lvArray.ToArray) 'Sohail (03 Nov 2010)
            AddHandler lvEmployeeList.ItemChecked, AddressOf lvEmployeeList_ItemChecked
            lvArray = Nothing

            If lvEmployeeList.Items.Count > 13 Then 'Sohail (12 Oct 2011)
                colhName.Width = 150 - 18
            Else
                colhName.Width = 150
            End If
            lvEmployeeList.EndUpdate()
            Cursor.Current = Cursors.Default 'Sohail (10 Feb 2012)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillList", mstrModuleName)
        Finally
            objEmployee = Nothing
            dsEmployee = Nothing
            dtTable = Nothing
            'Sohail (12 Apr 2016) -- Start
            'Enhancement - 58.1 - Remove existing allocation filters and give allocation link on global salary change screen.
            objbtnSearch.ShowResult(lvEmployeeList.Items.Count.ToString)
            'Sohail (12 Apr 2016) -- End
        End Try
    End Sub

    Private Sub CheckAllEmployee(ByVal blnCheckAll As Boolean)
        'Sohail (31 Mar 2012) -- Start
        'TRA - ENHANCEMENT
        Dim decTotCurScale As Decimal = 0
        Dim decTotIncrAmt As Decimal = 0
        Dim decTotNewScale As Decimal = 0
        'Sohail (31 Mar 2012) -- End
        Try
            For Each lvItem As ListViewItem In lvEmployeeList.Items
                RemoveHandler lvEmployeeList.ItemChecked, AddressOf lvEmployeeList_ItemChecked 'Sohail (03 Nov 2010)
                lvItem.Checked = blnCheckAll
                AddHandler lvEmployeeList.ItemChecked, AddressOf lvEmployeeList_ItemChecked 'Sohail (03 Nov 2010)
            Next

            'Sohail (31 Mar 2012) -- Start
            'TRA - ENHANCEMENT
            If blnCheckAll = True Then
                For Each lvItem As ListViewItem In lvEmployeeList.CheckedItems
                    If lvItem.Checked = True Then
                        decTotCurScale += CDec(lvItem.SubItems(colhCurrScale.Index).Tag)
                        decTotIncrAmt += CDec(lvItem.SubItems(colhIncrAmt.Index).Tag)
                        decTotNewScale += CDec(lvItem.SubItems(colhNewScale.Index).Tag)
                    End If
                Next
            End If
            txtTotCurrScale.Text = Format(decTotCurScale, GUI.fmtCurrency)
            txtTotIncrAmt.Text = Format(decTotIncrAmt, GUI.fmtCurrency)
            txtTotNewScale.Text = Format(decTotNewScale, GUI.fmtCurrency)
            'Sohail (31 Mar 2012) -- End
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "CheckAllEmployee", mstrModuleName)
        End Try
    End Sub

    'Sohail (06 Jan 2012) -- Start
    'TRA - ENHANCEMENT
    Private Sub FillEmployeeCombo()
        Dim objEmployee As New clsEmployee_Master
        Dim dsCombo As DataSet
        Try
            'Anjan [10 June 2015] -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'dsCombo = objEmployee.GetEmployeeList("Employee", True, , , , , , , , , , , , , mdtPayPeriodStartDate, mdtPayPeriodEndDate)
            'Sohail (12 Apr 2016) -- Start
            'Enhancement - 58.1 - Remove existing allocation filters and give allocation link on global salary change screen.
            'dsCombo = objEmployee.GetEmployeeList(FinancialYear._Object._DatabaseName, _
            '                              User._Object._Userunkid, _
            '                              FinancialYear._Object._YearUnkid, _
            '                              Company._Object._Companyunkid, _
            '                              mdtPayPeriodStartDate, _
            '                              mdtPayPeriodEndDate, _
            '                              ConfigParameter._Object._UserAccessModeSetting, _
            '                              True, False, "Employee", True)
            dsCombo = objEmployee.GetEmployeeList(FinancialYear._Object._DatabaseName, _
                                          User._Object._Userunkid, _
                                          FinancialYear._Object._YearUnkid, _
                                          Company._Object._Companyunkid, _
                                          mdtPayPeriodStartDate, _
                                          mdtPayPeriodEndDate, _
                                          ConfigParameter._Object._UserAccessModeSetting, _
                                          True, False, "Employee", True, , , , , , , , , , , , , , , mstrAdvanceFilter)
            'Sohail (12 Apr 2016) -- End
            'Anjan [10 June 2015] -- End

            With cboEmployee
                .BeginUpdate()
                .ValueMember = "employeeunkid"
                .DisplayMember = "employeename"
                .DataSource = dsCombo.Tables("Employee")
                .SelectedValue = 0
                .EndUpdate()
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillEmployeeCombo", mstrModuleName)
        Finally
            objEmployee = Nothing
        End Try

    End Sub
    'Sohail (06 Jan 2012) -- End

    'S.SANDEEP [ 18 SEP 2012 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Private Function Set_Notification_Approvals(ByVal StrUserName As String, ByVal intUserID As Integer, ByVal intSalaryincrementtranunkid As Integer) As String
        'Sohail (30 Dec 2013) - [intUserID, intSalaryincrementtranunkid]
        Dim StrMessage As New System.Text.StringBuilder
        Try
            StrMessage.Append("<HTML><BODY>")
            StrMessage.Append(vbCrLf)
            StrMessage.Append("<p><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#333399; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>")


            'Gajanan [27-Mar-2019] -- Start
            'Enhancement - Change Email Language
            'StrMessage.Append("Dear <b>" & StrUserName & "</b></span></p>")
            StrMessage.Append(Language.getMessage(mstrModuleName, 22, "Dear") & " " & "<b>" & getTitleCase(StrUserName) & "</b></span></p>")
            'Gajanan [27-Mar-2019] -- End


            StrMessage.Append(vbCrLf)
            StrMessage.Append("<p><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#333399; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>")

            'Gajanan [27-Mar-2019] -- Start
            'Enhancement - Change Email Language
            'StrMessage.Append("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; This is to inform you that following changes have been made in salary for the following employee(s).</span></p>")
            StrMessage.Append(Language.getMessage(mstrModuleName, 23, "This is to inform you that following changes have been made in salary for the following employee(s).") & "</span></p>")
            'Gajanan [27-Mar-2019] -- End

            StrMessage.Append("<p><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#333399; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>")

            'Gajanan [27-Mar-2019] -- Start
            'Enhancement - Change Email Language
            'StrMessage.Append("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; By user : <b>" & User._Object._Firstname & " " & User._Object._Lastname & "</b></span></p>")
            StrMessage.Append(" " & Language.getMessage(mstrModuleName, 24, "By user") & " <b>" & getTitleCase(User._Object._Firstname & " " & User._Object._Lastname) & "</b></span></p>")
            'Gajanan [27-Mar-2019] -- End
            StrMessage.Append("<p><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#333399; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>")

            'Gajanan [27-Mar-2019] -- Start
            'Enhancement - Change Email Language

            'StrMessage.Append("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; from Machine : <b>" & getHostName.ToString & "</b> and IPAddress : <b>" & getIP.ToString & "</b></span></p>")
            StrMessage.Append(" " & Language.getMessage(mstrModuleName, 25, "from Machine") & " " & "<b>" & getHostName.ToString & "</b>" & " " & Language.getMessage(mstrModuleName, 26, "and IPAddress") & " " & "<b>" & getIP.ToString & "</b></span></p>")
            'Gajanan [27-Mar-2019] -- End

            StrMessage.Append(vbCrLf)
            StrMessage.Append("<p><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#333399; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>")
            'Sohail (30 Dec 2013) -- Start
            'Enhancement - Send link in salary change notification
            'StrMessage.Append("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Please login to Main Aruti HR system to Approve/Reject this change.</span></p>")
            'Sohail (30 Dec 2013) -- End
            StrMessage.Append(StrApprlMessage & vbCrLf)
            StrMessage.Append("<br>")
            StrMessage.Append(vbCrLf)
            'Sohail (30 Dec 2013) -- Start
            'Enhancement - Send link in salary change notification
            Dim strLink As String
            strLink = ConfigParameter._Object._ArutiSelfServiceURL & "/HR/wPg_SalaryIncrementList.aspx?" & HttpUtility.UrlEncode(clsCrypto.Encrypt(Company._Object._Companyunkid.ToString & "|" & intUserID & "|0|" & CInt(cboPayPeriod.SelectedValue).ToString & "|0"))


            'Gajanan [27-Mar-2019] -- Start
            'Enhancement - Change Email Language

            'StrMessage.Append("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Please click on the following link to Approve/Reject these changes.</span></p>")
            'StrMessage.Append("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href='" & strLink & "'>" & strLink & "</a>")

            StrMessage.Append(Language.getMessage(mstrModuleName, 27, "Please click on the following link to Approve/Reject these changes.") & "</span></p>")
            StrMessage.Append("<a href='" & strLink & "'>" & strLink & "</a>")
            'Gajanan [27-Mar-2019] -- End

            StrMessage.Append("<br><br>")
            StrMessage.Append(vbCrLf)

            'Gajanan [27-Mar-2019] -- Start
            'Enhancement - Change Email Language
            'StrMessage.Append("<p><span style='padding-top:25px;font-size:10.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#333399;margin-left:0px;margin-right:0px;margin-top:0px;margin-bottom:10px;'><b>&nbsp;&nbsp;&nbsp;&nbsp;&quot;POWERED BY ARUTI HR & PAYROLL MANAGEMENT SOFTWARE.&quot;</b>")
            StrMessage.Append("<p><center style='padding-top:25px;font-size:10.0pt;font-family:&quot;Verdana&quot;,&quot;Sans-Serif&quot;;color:#333399;margin-left:0px;margin-right:0px;margin-top:0px;margin-bottom:10px'><b>&quot;POWERED BY ARUTI HR &amp; PAYROLL MANAGEMENT SOFTWARE.&quot;</b></center></p>")
            'Gajanan [27-Mar-2019] -- End

            StrMessage.Append("</span></p>")
            'Sohail (30 Dec 2013) -- End
            StrMessage.Append("</BODY></HTML>")

            Return StrMessage.ToString

        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "Set_Notification_Approvals", mstrModuleName)
            Return ""
        Finally

        End Try
    End Function

    Private Sub Send_Notification()
        Try
            If User._Object.Privilege._AllowToApproveSalaryChange = False Then
                If dicApprlNotification.Keys.Count > 0 Then
                    Dim objSendMail As New clsSendMail
                    For Each sKey As String In dicApprlNotification.Keys
                        objSendMail._ToEmail = sKey
                        objSendMail._Subject = Language.getMessage(mstrModuleName, 13, "Notifications to Approve Employee Salary Change")
                        objSendMail._Message = dicApprlNotification(sKey)
                        With objSendMail
                            ._FormName = mstrModuleName
                            ._LoginEmployeeunkid = 0
                            ._ClientIP = getIP()
                            ._HostName = getHostName()
                            ._FromWeb = False
                            ._AuditUserId = User._Object._Userunkid
._CompanyUnkid = Company._Object._Companyunkid
                            ._AuditDate = ConfigParameter._Object._CurrentDateAndTime
                        End With
                        'Sohail (30 Nov 2017) -- Start
                        'SUMATRA Enhancement – SUMATRA – issue # 0001669: Additional feature for Exchange Server configuration setting in 70.1.
                        'objSendMail.SendMail()
                        objSendMail.SendMail(Company._Object._Companyunkid)
                        'Sohail (30 Nov 2017) -- End
                    Next
                End If
            End If
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "Send_Notification", mstrModuleName)
        End Try
    End Sub
    'S.SANDEEP [ 18 SEP 2012 ] -- END

#End Region

#Region " Form's Events "

    Private Sub frmSalaryGlobalAssign_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        Try
            objSalary = Nothing
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmSalaryGlobalAssign_FormClosed", mstrModuleName)
        End Try
    End Sub

    Private Sub frmSalaryGlobalAssign_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown
        Try
            Select Case e.KeyCode
                Case Keys.Return
                    SendKeys.Send("{TAB}")
                    e.Handled = True
            End Select
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmSalaryGlobalAssign_KeyDown", mstrModuleName)
        End Try
    End Sub

    Private Sub frmSalaryGlobalAssign_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        objSalary = New clsSalaryIncrement(True)
        Try
            Call Set_Logo(Me, gApplicationType)

            'Anjan (02 Sep 2011)-Start
            'Issue : Including Language Settings.
            Language.setLanguage(Me.Name)
            Call OtherSettings()
            'Anjan (02 Sep 2011)-End 

            'Sohail (12 Oct 2011) -- Start
            txtPercentage.Enabled = False
            lblMaxLevelReachedMsg.Text = Language.getMessage(mstrModuleName, 7, "Expected New Scale reached the level of Maximum Scale.")
            'Sohail (12 Oct 2011) -- End
            Call SetColor()
            Call FillCombo()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmSalaryGlobalAssign_Load", mstrModuleName)
        End Try
    End Sub

    'Anjan (02 Sep 2011)-Start
    'Issue : Including Language Settings.
    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub
    'Anjan (02 Sep 2011)-End 


#End Region

#Region " ComboBox's Events "
    Private Sub cboPayYear_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboPayYear.SelectedIndexChanged
        Dim objPeriod As New clscommom_period_Tran
        Dim dsCombo As DataSet = Nothing

        Try
            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'dsCombo = objPeriod.getListForCombo(enModuleReference.Payroll, CInt(cboPayYear.SelectedValue), "PayPeriod", True, enStatusType.Open)
            dsCombo = objPeriod.getListForCombo(enModuleReference.Payroll, CInt(cboPayYear.SelectedValue), FinancialYear._Object._DatabaseName, FinancialYear._Object._Database_Start_Date, "PayPeriod", True, enStatusType.Open)
            'Sohail (21 Aug 2015) -- End
            With cboPayPeriod
                .ValueMember = "periodunkid"
                .DisplayMember = "Name"
                .DataSource = dsCombo.Tables("PayPeriod")
                If .Items.Count > 0 Then .SelectedValue = 0
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboPayYear_SelectedIndexChanged", mstrModuleName)
        Finally
            objPeriod = Nothing
            dsCombo = Nothing
        End Try
    End Sub

    Private Sub cboPayPeriod_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboPayPeriod.SelectedIndexChanged
        Dim objPeriod As New clscommom_period_Tran
        Dim dsList As DataSet = Nothing
        Try
            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'objPeriod._Periodunkid = CInt(cboPayPeriod.SelectedValue)
            objPeriod._Periodunkid(FinancialYear._Object._DatabaseName) = CInt(cboPayPeriod.SelectedValue)
            'Sohail (21 Aug 2015) -- End
            mdtPayPeriodStartDate = objPeriod._Start_Date
            mdtPayPeriodEndDate = objPeriod._End_Date

            'Sohail (06 Feb 2019) -- Start
            'NMB Enhancement - 76.1 - Set Increment date selection mandatory and Notify user if Salary will be prorated.
            If CInt(cboPayPeriod.SelectedValue) > 0 Then
                dtpIncrementdate.Value = mdtPayPeriodStartDate.AddMinutes(DateDiff(DateInterval.Minute, DateAndTime.Now.Date, Now))
                dtpIncrementdate.Checked = False
                'Sohail (21 Jan 2020) -- Start
                'NMB Enhancement # : Actual increment date on salary change screen for informational purpose.
                dtpActualDate.Checked = dtpIncrementdate.Checked
                'Sohail (21 Jan 2020) -- End
            End If
            'Sohail (06 Feb 2019) -- End

            If objchkSelectAll.Checked = True Then objchkSelectAll.Checked = False
            'Sohail (06 Jan 2012) -- Start
            'TRA - ENHANCEMENT
            Call FillEmployeeCombo()
            'Sohail (06 Jan 2012) -- End
            Call FillList()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboPayPeriod_SelectedIndexChanged", mstrModuleName)
        Finally
            objPeriod = Nothing
            dsList = Nothing
        End Try
    End Sub

    'Sohail (08 Nov 2011) -- Start
    Private Sub cboIncrementBy_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboIncrementBy.SelectedIndexChanged
        Try
            txtPercentage.Text = "0"
            Select Case CInt(cboIncrementBy.SelectedValue)
                Case enSalaryIncrementBy.Percentage
                    txtPercentage.Enabled = True
                    lblPercentage.Text = "Percentage (%)"
                Case enSalaryIncrementBy.Amount
                    txtPercentage.Enabled = True
                    lblPercentage.Text = "Amount"
                Case Else
                    lblPercentage.Text = ""
                    txtPercentage.Enabled = False
            End Select
            Call FillList()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboIncrementBy_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub
    'Sohail (08 Nov 2011) -- End

#End Region

#Region " Button's Events "
    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnClose_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnProcess_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnProcess.Click
        Dim strEmpList As String = ""
        Dim blnFlag As Boolean = False
        'Sohail (24 Sep 2012) -- Start
        'TRA - ENHANCEMENT
        Dim objTnA As New clsTnALeaveTran
        Dim objPayment As New clsPayment_tran
        Dim dsList As DataSet
        'Sohail (24 Sep 2012) -- End
        Try
            If lvEmployeeList.CheckedItems.Count = 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please select atleast one employee to process Salary Increment."), enMsgBoxStyle.Information)
                Exit Sub
                'Sohail (12 Oct 2011) -- Start
                'ElseIf CInt(cboPayYear.SelectedValue) <= 0 Then
                '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Please select Pay Year. Pay Year is mandatory information."), enMsgBoxStyle.Information)
                '    cboPayYear.Focus()
                '    Exit Sub
                'Sohail (12 Oct 2011) -- End
            ElseIf CInt(cboPayPeriod.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Please select Pay Period. Pay Period is mandatory information."), enMsgBoxStyle.Information)
                cboPayPeriod.Focus()
                Exit Sub
                'Sohail (06 Feb 2019) -- Start
                'NMB Enhancement - 76.1 - Set Increment date selection mandatory and Notify user if Salary will be prorated.
            ElseIf dtpIncrementdate.Checked = False Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 19, "Sorry, Increment date is mandatory information. Please provide Increment date to continue."), enMsgBoxStyle.Information)
                dtpIncrementdate.Focus()
                Exit Try
                'Sohail (06 Feb 2019) -- End
            ElseIf dtpIncrementdate.Value.Date > mdtPayPeriodEndDate Or dtpIncrementdate.Value.Date < mdtPayPeriodStartDate Then 'Sohail (16 Oct 2010)
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Increment Date should be in between ") & mdtPayPeriodStartDate & Language.getMessage(mstrModuleName, 6, " And ") & mdtPayPeriodEndDate & ".", enMsgBoxStyle.Information)
                dtpIncrementdate.Focus()
                Exit Try
                'Sohail (21 Jan 2020) -- Start
                'NMB Enhancement # : Actual increment date on salary change screen for informational purpose.
            ElseIf dtpActualDate.Checked = False Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 30, "Sorry, Actual date is mandatory information. Please provide Actual date to continue."), enMsgBoxStyle.Information)
                dtpActualDate.Focus()
                Exit Try
            ElseIf dtpActualDate.Value.Date > mdtPayPeriodEndDate Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 31, "Actual Date should not greater than selected period end date."), enMsgBoxStyle.Information)
                dtpActualDate.Focus()
                Exit Try
            ElseIf eZeeDate.convertDate(dtpActualDate.Value.Date) > eZeeDate.convertDate(dtpIncrementdate.Value.Date) Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 32, "Actual Date should not be greater than Increment Date."), enMsgBoxStyle.Information)
                dtpActualDate.Focus()
                Exit Try
                'Sohail (21 Jan 2020) -- End
            End If

            If CInt(cboReason.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 16, "Please select Reason. Reason is mandatory information. "), enMsgBoxStyle.Information)
                cboReason.Focus()
                Exit Try
            End If

            'Sohail (06 Feb 2019) -- Start
            'NMB Enhancement - 76.1 - Set Increment date selection mandatory and Notify user if Salary will be prorated.
            If dtpIncrementdate.Value.Date <> mdtPayPeriodStartDate Then
                If (eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 20, "Increment date is not set as selected period start date, So the salary will be prorated as per increment date.") & vbCrLf & vbCrLf & Language.getMessage(mstrModuleName, 21, "Are you sure you want to continue?"), CType(enMsgBoxStyle.Information + enMsgBoxStyle.YesNo, enMsgBoxStyle))) = Windows.Forms.DialogResult.No Then
                    dtpIncrementdate.Focus()
                    Exit Try
                End If
            End If
            'Sohail (06 Feb 2019) -- End

            'Sohail (21 Jan 2020) -- Start
            'NMB Enhancement # : Actual increment date on salary change screen for informational purpose.
            If dtpActualDate.Value.Date < mdtPayPeriodStartDate Then
                If (eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 33, "Actual date is set in past period, Which may cause salary arrears.") & vbCrLf & vbCrLf & Language.getMessage(mstrModuleName, 29, "Are you sure you want to continue?"), CType(enMsgBoxStyle.Information + enMsgBoxStyle.YesNo, enMsgBoxStyle))) = Windows.Forms.DialogResult.No Then
                    dtpActualDate.Focus()
                    Exit Try
                End If
            End If
            'Sohail (21 Jan 2020) -- End

            'S.SANDEEP [ 04 SEP 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            'PURPOSE : INCLUSION OF SALARY HEAD IN INFORMATIONAL HEAD AND GIVE SELECTION TO USER
            If chkCopyPreviousEDSlab.Checked = True Then
                If (eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 17, "You have ticked Copy Previous ED Slab. This will copy Previous Period Slab.") & vbCrLf & vbCrLf & Language.getMessage(mstrModuleName, 18, "Do you want to continue?"), CType(enMsgBoxStyle.Question + enMsgBoxStyle.YesNo, enMsgBoxStyle))) = Windows.Forms.DialogResult.No Then
                    Exit Try
                End If
            End If
            'S.SANDEEP [ 04 SEP 2012 ] -- END

            Call SetValue()


            'Sohail (12 Oct 2011) -- Start

            'S.SANDEEP [ 10 JULY 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            'mdtTran = objSalary._DataTable
            mdtTran = objSalary._DataTable.Copy
            'S.SANDEEP [ 10 JULY 2012 ] -- END


            Dim dtRow As DataRow
            'Sohail (12 Oct 2011) -- End

            'S.SANDEEP [ 18 SEP 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            If User._Object.Privilege._AllowToApproveSalaryChange = False Then
                dicApprlNotification = New Dictionary(Of String, String)
                StrApprlMessage = "<TABLE border = '1' WIDTH = '80%'> " & vbCrLf & _
                                  "<TR WIDTH = '80%' bgcolor= 'SteelBlue'> " & vbCrLf & _
                                  "<TD align = 'LEFT' STYLE='WIDTH:16%'><b><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#FFFFFF; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>" & Language.getMessage(mstrModuleName, 8, "Period") & "</span></b></TD>" & vbCrLf & _
                                  "<TD align = 'LEFT' STYLE='WIDTH:16%'><b><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#FFFFFF; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>" & Language.getMessage(mstrModuleName, 2, "Employee") & "</span></b></TD>" & vbCrLf & _
                                  "<TD align = 'LEFT' STYLE='WIDTH:16%'><b><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#FFFFFF; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>" & Language.getMessage(mstrModuleName, 9, "Date") & "</span></b></TD>" & vbCrLf & _
                                  "<TD align = 'LEFT' STYLE='WIDTH:16%'><b><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#FFFFFF; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>" & Language.getMessage(mstrModuleName, 10, "Old Scale") & "</span></b></TD>" & vbCrLf & _
                                  "<TD align = 'LEFT' STYLE='WIDTH:16%'><b><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#FFFFFF; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>" & Language.getMessage(mstrModuleName, 11, "New Scale") & "</span></b></TD>" & vbCrLf & _
                                  "<TD align = 'LEFT' STYLE='WIDTH:16%'><b><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#FFFFFF; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>" & Language.getMessage(mstrModuleName, 12, "Reason") & "</span></b></TD>" & vbCrLf & _
                                  "</TR>"

            End If
            'S.SANDEEP [ 18 SEP 2012 ] -- END

            For Each lvItem As ListViewItem In lvEmployeeList.CheckedItems

                'Sohail (21 Jan 2020) -- Start
                'NMB Enhancement # : Actual increment date on salary change screen for informational purpose.
                dsList = objSalary.getLastIncrement("LastIncr", CInt(lvItem.Tag), True)
                If dsList.Tables("LastIncr").Rows.Count > 0 Then
                    If eZeeDate.convertDate(dtpIncrementdate.Value) < dsList.Tables("LastIncr").Rows(0).Item("incrementdate").ToString Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 34, "Sorry, Some salary changes are already done from another machine. Please refresh list to do further operation."), enMsgBoxStyle.Information)
                        Exit Try
                    End If
                    If eZeeDate.convertDate(dtpActualDate.Value) < dsList.Tables("LastIncr").Rows(0).Item("incrementdate").ToString Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 34, "Sorry, Some salary changes are already done from another machine. Please refresh list to do further operation."), enMsgBoxStyle.Information)
                        Exit Try
                    End If
                End If
                'Sohail (21 Jan 2020) -- End

                'Sohail (12 Oct 2011) -- Start
                'For i As Integer = 0 To lvEmployeeList.CheckedItems.Count - 1
                'If strEmpList.Length <= 0 Then
                '    strEmpList &= CInt(lvEmployeeList.CheckedItems(i).Tag)
                'Else
                '    strEmpList &= "," & CInt(lvEmployeeList.CheckedItems(i).Tag)
                'End If
                'Sohail (10 Feb 2012) -- Start
                'TRA - ENHANCEMENT
                If strEmpList.Length <= 0 Then
                    strEmpList &= CInt(lvItem.Tag)
                Else
                    strEmpList &= "," & CInt(lvItem.Tag)
                End If
                'Sohail (10 Feb 2012) -- End

                dtRow = mdtTran.NewRow

                dtRow.Item("employeeunkid") = CInt(lvItem.Tag)
                dtRow.Item("currentscale") = CDec(lvItem.SubItems(colhCurrScale.Index).Tag)
                dtRow.Item("increment") = CDec(lvItem.SubItems(colhIncrAmt.Index).Tag)
                dtRow.Item("newscale") = CDec(lvItem.SubItems(colhNewScale.Index).Tag)
                'Sohail (27 Apr 2016) -- Start
                'Enhancement - 61.1 - Priority Order on Grade Group, Grade and Grade Level; Auto Salary Increment to next Grade Level as per Priority Order; Grade filter on Wages Table; Export and Import for Wages Table for KBC.
                '#23 (KBC & Kenya Project Comments List.xls) - Auto Salary incrment Per employee Anniversary,Employee Anniversay,All KBC Employees have Annivesary date for Salary increment,date is sent  when employee is confimed ,the salary is done automatically by the system when annivesary is due from current level to next level and stops at the Last level (notch),unless changed to higher Grade
                'dtRow.Item("gradegroupunkid") = CInt(lvItem.SubItems(colhGradeGroup.Index).Tag)
                'dtRow.Item("gradeunkid") = CInt(lvItem.SubItems(colhGrade.Index).Tag)
                'dtRow.Item("gradelevelunkid") = CInt(lvItem.SubItems(colhLevel.Index).Tag)
                dtRow.Item("gradegroupunkid") = CInt(lvItem.SubItems(colhNewGradeGroup.Index).Tag)
                dtRow.Item("gradeunkid") = CInt(lvItem.SubItems(colhNewGrade.Index).Tag)
                dtRow.Item("gradelevelunkid") = CInt(lvItem.SubItems(colhNewGradeLevel.Index).Tag)
                If CInt(lvItem.SubItems(colhNewGradeLevel.Index).Tag) = CInt(lvItem.SubItems(colhLevel.Index).Tag) Then
                    dtRow.Item("isgradechange") = False
                Else
                    dtRow.Item("isgradechange") = True
                End If
                'Sohail (27 Apr 2016) -- End
                dtRow.Item("increment_mode") = CInt(lvItem.SubItems(objcolhIncMode.Index).Text)
                dtRow.Item("percentage") = CDec(lvItem.SubItems(objcolhPercentage.Index).Text)
                'Sohail (21 Jan 2020) -- Start
                'NMB Enhancement # : Actual increment date on salary change screen for informational purpose.
                dtRow.Item("actualdate") = dtpActualDate.Value
                dtRow.Item("arrears_amount") = 0
                dtRow.Item("emi_amount") = 0
                dtRow.Item("noofinstallment") = 1
                dtRow.Item("arrears_countryid") = Company._Object._Localization_Country
                dtRow.Item("approval_statusunkid") = CInt(enApprovalStatus.PENDING)
                dtRow.Item("finalapproverunkid") = -1
                dtRow.Item("arrears_statusunkid") = 0 'CInt(enLoanStatus.IN_PROGRESS)

                Dim objMaster As New clsMasterData
                Dim intActualDatePeriod As Integer = objMaster.getCurrentPeriodID(enModuleReference.Payroll, dtpActualDate.Value, 0, 0, True, False, Nothing, True, True)
                If intActualDatePeriod <> CInt(cboPayPeriod.SelectedValue) Then
                    Dim decDiff As Decimal = 0
                    'If chkChangeGrade.Checked = True Then
                    '    decDiff = CDec(txtNewScaleGrade.Decimal) - CDec(txtLatestScale.Decimal)
                    'Else
                    decDiff = CDec(lvItem.SubItems(colhNewScale.Index).Tag) - CDec(lvItem.SubItems(colhCurrScale.Index).Tag)
                    'End If
                    Dim intMonths As Integer = 0
                    Dim objPeriod As New clscommom_period_Tran
                    Dim dsPeriod As DataSet = objPeriod.getListForCombo(enModuleReference.Payroll, 0, FinancialYear._Object._DatabaseName, FinancialYear._Object._Database_Start_Date, "List", False, 0, False)
                    Dim dtTable As DataTable = New DataView(dsPeriod.Tables(0), "start_date >= " & eZeeDate.convertDate(dtpActualDate.Value.Date) & " AND start_date < " & eZeeDate.convertDate(mdtPayPeriodStartDate) & " ", "", DataViewRowState.CurrentRows).ToTable
                    intMonths = dtTable.Rows.Count
                    Dim decArrears As Decimal = decDiff * intMonths

                    dtRow.Item("arrears_amount") = decDiff * intMonths
                    dtRow.Item("emi_amount") = decDiff * intMonths
                    dtRow.Item("noofinstallment") = 1
                    dtRow.Item("arrears_countryid") = Company._Object._Localization_Country
                    dtRow.Item("approval_statusunkid") = CInt(enApprovalStatus.PENDING)
                    dtRow.Item("finalapproverunkid") = -1
                    dtRow.Item("arrears_statusunkid") = 0 'CInt(enLoanStatus.IN_PROGRESS)
                End If
                'Sohail (21 Jan 2020) -- End
                mdtTran.Rows.Add(dtRow)
                'Sohail (12 Oct 2011) -- End

                'S.SANDEEP [ 18 SEP 2012 ] -- START
                'ENHANCEMENT : TRA CHANGES
                If User._Object.Privilege._AllowToApproveSalaryChange = False Then
                    StrApprlMessage &= "<TR WIDTH = '80%'>" & vbCrLf & _
                                       "<TD align = 'LEFT' WIDTH = '16%'><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#000000; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>" & cboPayPeriod.Text & "</span></TD>" & vbCrLf & _
                                       "<TD align = 'LEFT' WIDTH = '16%'><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#000000; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>" & lvItem.SubItems(colhName.Index).Text & "</span></TD>" & vbCrLf & _
                                       "<TD align = 'LEFT' WIDTH = '16%'><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#000000; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>" & dtpIncrementdate.Value.Date & "</span></TD>" & vbCrLf & _
                                       "<TD align = 'LEFT' WIDTH = '16%'><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#000000; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>" & Format(CDec(lvItem.SubItems(colhCurrScale.Index).Text), GUI.fmtCurrency) & "</span></TD>" & vbCrLf & _
                                       "<TD align = 'LEFT' WIDTH = '16%'><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#000000; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>" & Format(CDec(lvItem.SubItems(colhNewScale.Index).Text), GUI.fmtCurrency) & "</span></TD>" & vbCrLf & _
                                       "<TD align = 'LEFT' WIDTH = '16%'><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#000000; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>" & cboReason.Text & "</span></TD>" & vbCrLf & _
                                       "</TR>" & vbCrLf
                End If
                'S.SANDEEP [ 18 SEP 2012 ] -- END
            Next

            If User._Object.Privilege._AllowToApproveSalaryChange = False Then

                StrApprlMessage &= "</TABLE>" & vbCrLf
                'Sohail (30 Dec 2013) -- Start
                'Enhancement - Send link in salary change notification
                '"<p><span style='padding-top:25px;font-size:10.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#333399;margin-left:0px;margin-right:0px;margin-top:0px;margin-bottom:10px;'><b>&nbsp;&nbsp;&nbsp;&nbsp;&quot;POWERED BY ARUTI HR & PAYROLL MANAGEMENT SOFTWARE.&quot;</b>" & vbCrLf & _
                '"</span></p>"
                'Sohail (30 Dec 2013) -- End

                'Sohail (08 Mar 2016) -- Start
                'Issue - User access on On Salary change, All approvers are receiving Notifications even if employee with salary change not in his/her user access. #51 (KBC & Kenya Project Comments List.xls)
                Dim objEmployee As New clsEmployee_Master
                Dim dsEmp As DataSet = objEmployee.GetList(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, mdtPayPeriodStartDate, mdtPayPeriodEndDate, ConfigParameter._Object._UserAccessModeSetting, True, False, "List", , , , "hremployee_master.employeeunkid IN (" & strEmpList & ")", , )
                Dim strDeptIDs As String = String.Join(",", (From p In New DataView(dsEmp.Tables(0)).ToTable(True, "departmentunkid") Select (If(IsDBNull(p.Item("departmentunkid")), "-99", p.Item("departmentunkid")).ToString)).ToArray)
                Dim strJobIDs As String = String.Join(",", (From p In New DataView(dsEmp.Tables(0)).ToTable(True, "jobunkid") Select (If(IsDBNull(p.Item("jobunkid")), "-99", p.Item("jobunkid")).ToString)).ToArray)
                Dim strClassGrpIDs As String = String.Join(",", (From p In New DataView(dsEmp.Tables(0)).ToTable(True, "classgroupunkid") Select (If(IsDBNull(p.Item("classgroupunkid")), "-99", p.Item("classgroupunkid")).ToString)).ToArray)
                Dim strClassIDs As String = String.Join(",", (From p In New DataView(dsEmp.Tables(0)).ToTable(True, "classunkid") Select (If(IsDBNull(p.Item("classunkid")), "-99", p.Item("classunkid")).ToString)).ToArray)
                'Sohail (08 Mar 2016) -- End

                Dim objUsr As New clsUserAddEdit : Dim StrMessage As String = String.Empty
                Dim dUList As New DataSet
                dUList = objUsr.Get_UserBy_PrivilegeId(671)
                If dUList.Tables(0).Rows.Count > 0 Then
                    For Each dRow As DataRow In dUList.Tables(0).Rows
                        'Sohail (30 Dec 2013) -- Start
                        'Enhancement - Send link in salary change notification
                        'StrMessage = Set_Notification_Approvals(CStr(dRow.Item("UName")))
                        'Sohail (08 Mar 2016) -- Start
                        'Issue - User access on On Salary change, All approvers are receiving Notifications even if employee with salary change not in his/her user access. #51 (KBC & Kenya Project Comments List.xls)
                        'StrMessage = Set_Notification_Approvals(CStr(dRow.Item("UName")), CInt(dRow.Item("UId")), 0)
                        ''Sohail (30 Dec 2013) -- End
                        'If dicApprlNotification.ContainsKey(CStr(dRow.Item("UEmail"))) = False Then
                        '    dicApprlNotification.Add(CStr(dRow.Item("UEmail")), StrMessage)
                        'End If
                        Dim dtUserAccess As DataTable = objUsr.GetUserAccessFromUser(CInt(dRow.Item("UId")), Company._Object._Companyunkid, FinancialYear._Object._YearUnkid, ConfigParameter._Object._UserAccessModeSetting)
                        If dtUserAccess IsNot Nothing AndAlso dtUserAccess.Rows.Count > 0 Then
                            Dim mblnFlag As Boolean = False
                            For Each AID As String In ConfigParameter._Object._UserAccessModeSetting.Trim.Split(CChar(","))
                                Dim drRow() As DataRow = Nothing
                                Select Case CInt(AID)

                                    Case enAllocation.DEPARTMENT
                                        drRow = dtUserAccess.Select("allocationunkid IN (" & strDeptIDs & ") AND referenceunkid = " & enAllocation.DEPARTMENT)
                                        If drRow.Length > 0 Then
                                            mblnFlag = True
                                        Else
                                            mblnFlag = False
                                            Exit For
                                        End If

                                    Case enAllocation.JOBS
                                        drRow = dtUserAccess.Select("allocationunkid IN (" & strJobIDs & ") AND referenceunkid = " & enAllocation.JOBS)
                                        If drRow.Length > 0 Then
                                            mblnFlag = True
                                        Else
                                            mblnFlag = False
                                            Exit For
                                        End If

                                    Case enAllocation.CLASS_GROUP
                                        drRow = dtUserAccess.Select("allocationunkid IN (" & strClassGrpIDs & ") AND referenceunkid = " & enAllocation.CLASS_GROUP)
                                        If drRow.Length > 0 Then
                                            mblnFlag = True
                                        Else
                                            mblnFlag = False
                                            Exit For
                                        End If

                                    Case enAllocation.CLASSES
                                        drRow = dtUserAccess.Select("allocationunkid IN (" & strClassIDs & ") AND referenceunkid = " & enAllocation.CLASSES)
                                        If drRow.Length > 0 Then
                                            mblnFlag = True
                                        Else
                                            mblnFlag = False
                                            Exit For
                                        End If

                                    Case Else
                                        mblnFlag = False
                                End Select
                            Next

                            If mblnFlag Then
                                StrMessage = Set_Notification_Approvals(CStr(dRow.Item("UName")), CInt(dRow.Item("UId")), 0)
                                If dicApprlNotification.ContainsKey(CStr(dRow.Item("UEmail"))) = False Then
                                    dicApprlNotification.Add(CStr(dRow.Item("UEmail")), StrMessage)
                                End If
                            End If
                        End If
                        'Sohail (08 Mar 2016) -- End
                    Next
                End If
            End If

            'Sohail (24 Sep 2012) -- Start
            'TRA - ENHANCEMENT
            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'dsList = objPayment.GetListByPeriod("Payment", clsPayment_tran.enPaymentRefId.PAYSLIP, CInt(cboPayPeriod.SelectedValue), clsPayment_tran.enPayTypeId.PAYMENT, strEmpList)
            dsList = objPayment.GetListByPeriod(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, mdtPayPeriodStartDate, mdtPayPeriodEndDate, ConfigParameter._Object._UserAccessModeSetting, True, ConfigParameter._Object._IsIncludeInactiveEmp, "Payment", clsPayment_tran.enPaymentRefId.PAYSLIP, CInt(cboPayPeriod.SelectedValue), clsPayment_tran.enPayTypeId.PAYMENT, strEmpList, False)
            'Sohail (21 Aug 2015) -- End
            If dsList.Tables("Payment").Rows.Count > 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 15, "Sorry, Payment of Salary for some of the selected employees is already done for this period. Please Void Payment for selected employee to give salary change."), enMsgBoxStyle.Information)
                Exit Try
            End If

            If objTnA.IsPayrollProcessDone(CInt(cboPayPeriod.SelectedValue), strEmpList, mdtPayPeriodEndDate) = True Then
                'Sohail (19 Apr 2019) -- Start
                'Enhancement - 76.1 - Option to open process payroll screen if process payroll is already done message come.
                'eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 14, "Sorry, Process Payroll is already done for last date of selected Period for some of the employees. Please Void Process Payroll first for selected employee to give salary change."), enMsgBoxStyle.Information)
                If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 14, "Sorry, Process Payroll is already done for last date of selected Period for some of the employees. Please Void Process Payroll first for selected employee to give salary change.") & vbCrLf & vbCrLf & Language.getMessage(mstrModuleName, 28, "Do you want to void Payroll?"), CType(enMsgBoxStyle.Information + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.Yes Then
                    Dim objFrm As New frmProcessPayroll
                    objFrm.ShowDialog()
                End If
                'Sohail (19 Apr 2019) -- End
                Exit Try
            End If
            'Sohail (24 Sep 2012) -- End

            'S.SANDEEP [ 04 SEP 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            'PURPOSE : INCLUSION OF SALARY HEAD IN INFORMATIONAL HEAD AND GIVE SELECTION TO USER
            If chkCopyPreviousEDSlab.Checked = True Then
                If (eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 17, "You have ticked Copy Previous ED Slab. This will copy Previous Period Slab.") & vbCrLf & vbCrLf & Language.getMessage(mstrModuleName, 18, "Do you want to continue?"), CType(enMsgBoxStyle.Question + enMsgBoxStyle.YesNo, enMsgBoxStyle))) = Windows.Forms.DialogResult.No Then
                    Exit Try
                End If
            End If
            'S.SANDEEP [ 04 SEP 2012 ] -- END

            'S.SANDEEP [28-May-2018] -- START
            'ISSUE/ENHANCEMENT : {Audit Trails} 
            objSalary._FormName = mstrModuleName
            objSalary._LoginEmployeeUnkid = 0
            objSalary._ClientIP = getIP()
            objSalary._HostName = getHostName()
            objSalary._FromWeb = False
            objSalary._AuditUserId = User._Object._Userunkid
objSalary._CompanyUnkid = Company._Object._Companyunkid
            objSalary._AuditDate = ConfigParameter._Object._CurrentDateAndTime
            'S.SANDEEP [28-May-2018] -- END

            'Sohail (12 Oct 2011) -- Start
            'blnFlag = objSalary.InsertAll(strEmpList, mdtPayPeriodStartDate)
            'Sohail (29 Dec 2012) -- Start
            'TRA - ENHANCEMENT
            'blnFlag = objSalary.InsertAll(mdtTran)
            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'blnFlag = objSalary.InsertAll(mdtTran, mdicAppraisal)
            'Sohail (24 Feb 2022) -- Start
            'Issue :  : TWC - Not able to approve Pending Salary heads on Earning and Deduction screen.
            'blnFlag = objSalary.InsertAll(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, mdtPayPeriodStartDate, mdtPayPeriodEndDate, ConfigParameter._Object._UserAccessModeSetting, True, ConfigParameter._Object._IsIncludeInactiveEmp, mdtTran, User._Object.Privilege._AllowToApproveEarningDeduction, ConfigParameter._Object._CurrentDateAndTime, mdicAppraisal, True, "")
            blnFlag = objSalary.InsertAll(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, mdtPayPeriodStartDate, mdtPayPeriodEndDate, ConfigParameter._Object._UserAccessModeSetting, True, ConfigParameter._Object._IsIncludeInactiveEmp, mdtTran, True, ConfigParameter._Object._CurrentDateAndTime, mdicAppraisal, True, "")
            'Sohail ((24 Feb 2022) -- End
            'Sohail (21 Aug 2015) -- End

            'Sohail (29 Dec 2012) -- End
            'Sohail (12 Oct 2011) -- End

            If blnFlag = True Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "Salary Increment Process completed successfully."), enMsgBoxStyle.Information)
                objchkSelectAll.Checked = False
                'Sohail (10 Feb 2012) -- Start
                'TRA - ENHANCEMENT
                If mstrAppraisalEmpIdList.Trim <> "" Then
                    mintAppraisalPeriodUnkId = CInt(cboPayPeriod.SelectedValue)
                    mstrAppraisalEmpIdList = strEmpList
                End If
                'Sohail (10 Feb 2012) -- End
                'Sohail (12 Oct 2011) -- Start
                'cboPayYear.SelectedValue = 0
                cboPayPeriod.SelectedValue = 0
                'Sohail (08 Nov 2011) -- Start
                'chkByPercentage.Checked = False
                'txtRemark.Text = ""
                If cboIncrementBy.Items.Count > 0 Then cboIncrementBy.SelectedIndex = 0
                cboReason.SelectedValue = 0
                'Sohail (08 Nov 2011) -- End
                Call FillList()
                'Sohail (12 Oct 2011) -- End
                mblnCancel = False
                If mstrAppraisalEmpIdList.Trim <> "" Then Me.Close() 'Sohail (10 Feb 2012)
            End If

            'S.SANDEEP [ 18 SEP 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            If blnFlag = True Then
                trd = New Thread(AddressOf Send_Notification)
                trd.IsBackground = True
                trd.Start()
            End If
            'S.SANDEEP [ 18 SEP 2012 ] -- END

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnProcess_Click", mstrModuleName)
        End Try
    End Sub
#End Region

#Region " Listview's Events "
    Private Sub lvEmployeeList_ItemChecked(ByVal sender As Object, ByVal e As System.Windows.Forms.ItemCheckedEventArgs) Handles lvEmployeeList.ItemChecked
        'Sohail (31 Mar 2012) -- Start
        'TRA - ENHANCEMENT
        Dim decTotCurScale As Decimal = 0
        Dim decTotIncrAmt As Decimal = 0
        Dim decTotNewScale As Decimal = 0
        'Sohail (31 Mar 2012) -- End
        Try
            'Sohail (03 Nov 2010) -- Start
            'If lvEmployeeList.CheckedItems.Count = lvEmployeeList.Items.Count Then
            '    chkSelectAll.Checked = True
            'ElseIf lvEmployeeList.CheckedItems.Count = 0 Then
            '    chkSelectAll.Checked = False
            'End If
            If lvEmployeeList.CheckedItems.Count <= 0 Then
                RemoveHandler objchkSelectAll.CheckedChanged, AddressOf chkSelectAll_CheckedChanged
                objchkSelectAll.CheckState = CheckState.Unchecked
                AddHandler objchkSelectAll.CheckedChanged, AddressOf chkSelectAll_CheckedChanged
            ElseIf lvEmployeeList.CheckedItems.Count < lvEmployeeList.Items.Count Then
                RemoveHandler objchkSelectAll.CheckedChanged, AddressOf chkSelectAll_CheckedChanged
                objchkSelectAll.CheckState = CheckState.Indeterminate
                AddHandler objchkSelectAll.CheckedChanged, AddressOf chkSelectAll_CheckedChanged

                'Sohail (31 Mar 2012) -- Start
                'TRA - ENHANCEMENT
                For Each lvItem As ListViewItem In lvEmployeeList.CheckedItems
                    If lvItem.Checked = True Then
                        decTotCurScale += CDec(lvItem.SubItems(colhCurrScale.Index).Tag)
                        decTotIncrAmt += CDec(lvItem.SubItems(colhIncrAmt.Index).Tag)
                        decTotNewScale += CDec(lvItem.SubItems(colhNewScale.Index).Tag)
                    End If
                Next
                'Sohail (31 Mar 2012) -- End
            ElseIf lvEmployeeList.CheckedItems.Count = lvEmployeeList.Items.Count Then
                RemoveHandler objchkSelectAll.CheckedChanged, AddressOf chkSelectAll_CheckedChanged
                objchkSelectAll.CheckState = CheckState.Checked
                AddHandler objchkSelectAll.CheckedChanged, AddressOf chkSelectAll_CheckedChanged

                'Sohail (31 Mar 2012) -- Start
                'TRA - ENHANCEMENT
                For Each lvItem As ListViewItem In lvEmployeeList.CheckedItems
                    If lvItem.Checked = True Then
                        decTotCurScale += CDec(lvItem.SubItems(colhCurrScale.Index).Tag)
                        decTotIncrAmt += CDec(lvItem.SubItems(colhIncrAmt.Index).Tag)
                        decTotNewScale += CDec(lvItem.SubItems(colhNewScale.Index).Tag)
                    End If
                Next
                'Sohail (31 Mar 2012) -- End
            End If
            'Sohail (03 Nov 2010) -- End

            'Sohail (31 Mar 2012) -- Start
            'TRA - ENHANCEMENT
            txtTotCurrScale.Text = Format(decTotCurScale, GUI.fmtCurrency)
            txtTotIncrAmt.Text = Format(decTotIncrAmt, GUI.fmtCurrency)
            txtTotNewScale.Text = Format(decTotNewScale, GUI.fmtCurrency)
            'Sohail (31 Mar 2012) -- End
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lvEmployeeList_ItemChecked", mstrModuleName)
        End Try
    End Sub
#End Region

#Region " CheckBox's Events "
    Private Sub chkSelectAll_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objchkSelectAll.CheckedChanged
        Try
            Call CheckAllEmployee(objchkSelectAll.Checked)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "chkSelectAll_CheckedChanged", mstrModuleName)
        End Try
    End Sub

    'Sohail (12 Oct 2011) -- Start
    'Private Sub chkByPercentage_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
    '    Try
    '        If chkByPercentage.Checked = True Then
    '            txtPercentage.Enabled = True
    '        Else
    '            txtPercentage.Text = "0"
    '            txtPercentage.Enabled = False
    '            Call FillList()
    '        End If
    '    Catch ex As Exception
    '        DisplayError.Show("-1", ex.Message, "chkByPercentage_CheckedChanged", mstrModuleName)
    '    End Try
    'End Sub
    'Sohail (12 Oct 2011) -- End


    'S.SANDEEP [ 04 SEP 2012 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Private Sub chkCopyPreviousEDSlab_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkCopyPreviousEDSlab.CheckedChanged
        Try
            If chkCopyPreviousEDSlab.Checked = False Then chkOverwritePrevEDSlabHeads.Checked = False
            chkOverwritePrevEDSlabHeads.Enabled = chkCopyPreviousEDSlab.Checked
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "chkCopyPreviousEDSlab_CheckedChanged", mstrModuleName)
        End Try
    End Sub
    'S.SANDEEP [ 04 SEP 2012 ] -- END


#End Region

    'Sohail (12 Oct 2011) -- Start
#Region " Textbox's Events "
    Private Sub txtPercentage_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtPercentage.LostFocus
        Try
            Call FillList()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "txtPercentage_LostFocus", mstrModuleName)
        End Try
    End Sub

    'Sohail (20 Jun 2012) -- Start
    'TRA - ENHANCEMENT
    Private Sub txtSearchEmp_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtSearchEmp.TextChanged
        Try
            If lvEmployeeList.Items.Count <= 0 Then Exit Sub
            lvEmployeeList.SelectedIndices.Clear()
            Dim lvFoundItem As ListViewItem = lvEmployeeList.FindItemWithText(txtSearchEmp.Text, True, 0, True)
            If lvFoundItem IsNot Nothing Then
                lvEmployeeList.TopItem = lvFoundItem
                lvFoundItem.Selected = True
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "txtSearchEmp_TextChanged", mstrModuleName)
        End Try
    End Sub
    'Sohail (20 Jun 2012) -- End
#End Region
    'Sohail (12 Oct 2011) -- End

#Region " Other Control's Events "
    Private Sub objbtnSearchEmployee_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchEmployee.Click
        Dim objfrm As New frmCommonSearch
        'Sohail (06 Jan 2012) -- Start
        'TRA - ENHANCEMENT
        'Dim objEmployee As New clsEmployee_Master
        'Dim dsList As DataSet
        'Sohail (06 Jan 2012) -- End
        Try
            'Anjan (02 Sep 2011)-Start
            'Issue : Including Language Settings.
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If
            'Anjan (02 Sep 2011)-End 

            'Sohail (06 Jan 2012) -- Start
            'TRA - ENHANCEMENT
            'dsList = objEmployee.GetEmployeeList("EmployeeList")            
            'Sohail (06 Jan 2012) -- End
            With cboEmployee
                'Sohail (06 Jan 2012) -- Start
                'TRA - ENHANCEMENT
                'objfrm.DataSource = dsList.Tables("EmployeeList")
                objfrm.DataSource = CType(cboEmployee.DataSource, DataTable)
                'Sohail (06 Jan 2012) -- End
                objfrm.ValueMember = .ValueMember
                objfrm.DisplayMember = .DisplayMember
                objfrm.CodeMember = "employeecode"
                If objfrm.DisplayDialog Then
                    .SelectedValue = objfrm.SelectedValue
                End If
                .Focus()
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchEmployee_Click", mstrModuleName)
        Finally
            objfrm = Nothing
            'objEmployee = Nothing 'Sohail (06 Jan 2012)
        End Try
    End Sub

    Private Sub objbtnSearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearch.Click
        Try
            If objchkSelectAll.Checked = True Then objchkSelectAll.Checked = False
            Call FillList()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearch_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnReset.Click
        Try
            If cboEmployee.Items.Count > 0 Then cboEmployee.SelectedValue = 0
            'Sohail (12 Apr 2016) -- Start
            'Enhancement - 58.1 - Remove existing allocation filters and give allocation link on global salary change screen.
            'If cboDepartment.Items.Count > 0 Then cboDepartment.SelectedValue = 0
            'If cboGrade.Items.Count > 0 Then cboGrade.SelectedValue = 0
            'If cboSections.Items.Count > 0 Then cboSections.SelectedValue = 0
            'If cboClass.Items.Count > 0 Then cboClass.SelectedValue = 0
            'If cboCostCenter.Items.Count > 0 Then cboCostCenter.SelectedValue = 0
            'If cboJob.Items.Count > 0 Then cboJob.SelectedValue = 0
            'If cboPayPoint.Items.Count > 0 Then cboPayPoint.SelectedValue = 0
            'If cboUnit.Items.Count > 0 Then cboUnit.SelectedValue = 0
            mstrAdvanceFilter = ""
            Call FillEmployeeCombo()
            'Sohail (12 Apr 2016) -- End

            lvEmployeeList.Items.Clear()
            If objchkSelectAll.Checked = True Then objchkSelectAll.Checked = False
            Call FillList()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnReset_Click", mstrModuleName)
        End Try
    End Sub

    'Sohail (08 Nov 2011) -- Start
    Private Sub objbtnAddReason_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnAddReason.Click
        Dim frm As New frmCommonMaster
        Dim dsList As New DataSet
        Dim intGroupId As Integer = -1
        Dim objGroup As New clsCommon_Master
        Try
            frm.displayDialog(intGroupId, clsCommon_Master.enCommonMaster.SALINC_REASON, enAction.ADD_ONE)
            If intGroupId > -1 Then
                dsList = objGroup.getComboList(clsCommon_Master.enCommonMaster.SALINC_REASON, True, "Group")
                With cboReason
                    .ValueMember = "masterunkid"
                    .DisplayMember = "name"
                    .DataSource = dsList.Tables("Group")
                    .SelectedValue = intGroupId
                End With
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnAddReason_Click", mstrModuleName)
        Finally
            frm = Nothing
            dsList = Nothing
            objGroup = Nothing
        End Try
    End Sub
    'Sohail (08 Nov 2011) -- End

    'Sohail (29 Dec 2012) -- Start
    'TRA - ENHANCEMENT
    Private Sub dtpIncrementdate_Leave(ByVal sender As Object, ByVal e As System.EventArgs) Handles dtpIncrementdate.Leave
        Try
            Call FillList()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dtpIncrementdate_Leave", mstrModuleName)
        End Try
    End Sub
    'Sohail (29 Dec 2012) -- End

    'Sohail (21 Jan 2020) -- Start
    'NMB Enhancement # : Actual increment date on salary change screen for informational purpose.
    Private Sub dtpIncrementdate_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles dtpIncrementdate.ValueChanged
        Try
            dtpActualDate.Checked = dtpIncrementdate.Checked
            dtpActualDate.Value = dtpIncrementdate.Value
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dtpIncrementdate_ValueChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub dtpActualDate_Leave(ByVal sender As Object, ByVal e As System.EventArgs) Handles dtpActualDate.Leave
        Try
            Call FillList()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dtpActualDate_Leave", mstrModuleName)
        End Try
    End Sub
    'Sohail (21 Jan 2020) -- End

    'Sohail (12 Apr 2016) -- Start
    'Enhancement - 58.1 - Remove existing allocation filters and give allocation link on global salary change screen.
    Private Sub lnkAllocation_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lnkAllocation.LinkClicked
        Try
            Dim frm As New frmAdvanceSearch
            frm.ShowDialog()
            mstrAdvanceFilter = frm._GetFilterString
            Call FillEmployeeCombo()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lnkAllocation_LinkClicked", mstrModuleName)
        End Try
    End Sub
    'Sohail (12 Apr 2016) -- End

#End Region

#Region " Message List "
    '1, "Please select atleast one employee to process Salary Increment."
    '2, "Please select Pay Year. Pay Year is mandatory information."
    '3, "Please select Pay Period. Pay Period is mandatory information."
    '4, "Increment Date should be in between " & mdtPayPeriodStartDate & " And " & mdtPayPeriodEndDate & "."
    '5, "Salary Increment Process completed successfully."
    '7, "Expected New Scale will reached the level of Maximum Scale."
#End Region

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Private Sub OtherSettings()
        Try
            Me.SuspendLayout()
            
            Call SetLanguage()
			
			Me.gbSalaryIncrement.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbSalaryIncrement.ForeColor = GUI._eZeeContainerHeaderForeColor 

			Me.gbEmployeeList.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbEmployeeList.ForeColor = GUI._eZeeContainerHeaderForeColor 


			
			Me.eZeeHeader.GradientColor1 = GUI._HeaderBackColor1 
			Me.eZeeHeader.GradientColor2 = GUI._HeaderBackColor2 
			Me.eZeeHeader.BorderColor = GUI._HeaderBorderColor 
			Me.eZeeHeader.HeaderTextForeColor = GUI._HeaderTitleFontColor 
			Me.eZeeHeader.DescriptionForeColor = GUI._HeaderMessageFontColor 


			Me.btnProcess.GradientBackColor = GUI._ButttonBackColor 
            Me.btnProcess.GradientForeColor = GUI._ButttonFontColor

			Me.btnClose.GradientBackColor = GUI._ButttonBackColor 
            Me.btnClose.GradientForeColor = GUI._ButttonFontColor


            Me.ResumeLayout()
        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "OtherSettings", mstrModuleName)
        End Try
    End Sub
			
			
    Private Sub SetLanguage()
        Try
            Me.Text = Language._Object.getCaption(Me.Name, Me.Text)
			
            Me.eZeeHeader.Title = Language._Object.getCaption(Me.eZeeHeader.Name & "_Title", Me.eZeeHeader.Title)
            Me.eZeeHeader.Message = Language._Object.getCaption(Me.eZeeHeader.Name & "_Message", Me.eZeeHeader.Message)
            Me.btnProcess.Text = Language._Object.getCaption(Me.btnProcess.Name, Me.btnProcess.Text)
            Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
            Me.gbSalaryIncrement.Text = Language._Object.getCaption(Me.gbSalaryIncrement.Name, Me.gbSalaryIncrement.Text)
            Me.gbEmployeeList.Text = Language._Object.getCaption(Me.gbEmployeeList.Name, Me.gbEmployeeList.Text)
            Me.colhCheck.Text = Language._Object.getCaption(CStr(Me.colhCheck.Tag), Me.colhCheck.Text)
            Me.colhCode.Text = Language._Object.getCaption(CStr(Me.colhCode.Tag), Me.colhCode.Text)
            Me.colhName.Text = Language._Object.getCaption(CStr(Me.colhName.Tag), Me.colhName.Text)
            Me.lblPayYear.Text = Language._Object.getCaption(Me.lblPayYear.Name, Me.lblPayYear.Text)
            Me.lblPayPeriod.Text = Language._Object.getCaption(Me.lblPayPeriod.Name, Me.lblPayPeriod.Text)
            Me.lblIncrementDate.Text = Language._Object.getCaption(Me.lblIncrementDate.Name, Me.lblIncrementDate.Text)
            Me.lblEmployee.Text = Language._Object.getCaption(Me.lblEmployee.Name, Me.lblEmployee.Text)
            Me.colhGradeGroup.Text = Language._Object.getCaption(CStr(Me.colhGradeGroup.Tag), Me.colhGradeGroup.Text)
            Me.colhGrade.Text = Language._Object.getCaption(CStr(Me.colhGrade.Tag), Me.colhGrade.Text)
            Me.colhLevel.Text = Language._Object.getCaption(CStr(Me.colhLevel.Tag), Me.colhLevel.Text)
            Me.colhCurrScale.Text = Language._Object.getCaption(CStr(Me.colhCurrScale.Tag), Me.colhCurrScale.Text)
            Me.colhLastDate.Text = Language._Object.getCaption(CStr(Me.colhLastDate.Tag), Me.colhLastDate.Text)
            Me.colhIncrAmt.Text = Language._Object.getCaption(CStr(Me.colhIncrAmt.Tag), Me.colhIncrAmt.Text)
            Me.colhMaxScale.Text = Language._Object.getCaption(CStr(Me.colhMaxScale.Tag), Me.colhMaxScale.Text)
            Me.lblPercentage.Text = Language._Object.getCaption(Me.lblPercentage.Name, Me.lblPercentage.Text)
            Me.colhNewScale.Text = Language._Object.getCaption(CStr(Me.colhNewScale.Tag), Me.colhNewScale.Text)
            Me.lblMaxLevelReachedMsg.Text = Language._Object.getCaption(Me.lblMaxLevelReachedMsg.Name, Me.lblMaxLevelReachedMsg.Text)
            Me.lblIncrementBy.Text = Language._Object.getCaption(Me.lblIncrementBy.Name, Me.lblIncrementBy.Text)
            Me.lblReason.Text = Language._Object.getCaption(Me.lblReason.Name, Me.lblReason.Text)
            Me.lblTotNewScale.Text = Language._Object.getCaption(Me.lblTotNewScale.Name, Me.lblTotNewScale.Text)
            Me.lblTotCurrScale.Text = Language._Object.getCaption(Me.lblTotCurrScale.Name, Me.lblTotCurrScale.Text)
            Me.lblTotIncrAmt.Text = Language._Object.getCaption(Me.lblTotIncrAmt.Name, Me.lblTotIncrAmt.Text)
            Me.lblSearchEmp.Text = Language._Object.getCaption(Me.lblSearchEmp.Name, Me.lblSearchEmp.Text)
            Me.chkOverwritePrevEDSlabHeads.Text = Language._Object.getCaption(Me.chkOverwritePrevEDSlabHeads.Name, Me.chkOverwritePrevEDSlabHeads.Text)
            Me.chkCopyPreviousEDSlab.Text = Language._Object.getCaption(Me.chkCopyPreviousEDSlab.Name, Me.chkCopyPreviousEDSlab.Text)
            Me.lnkAllocation.Text = Language._Object.getCaption(Me.lnkAllocation.Name, Me.lnkAllocation.Text)
            Me.colhNewGradeGroup.Text = Language._Object.getCaption(CStr(Me.colhNewGradeGroup.Tag), Me.colhNewGradeGroup.Text)
            Me.colhNewGrade.Text = Language._Object.getCaption(CStr(Me.colhNewGrade.Tag), Me.colhNewGrade.Text)
            Me.colhNewGradeLevel.Text = Language._Object.getCaption(CStr(Me.colhNewGradeLevel.Tag), Me.colhNewGradeLevel.Text)
            Me.lblGradeLevelChangedMsg.Text = Language._Object.getCaption(Me.lblGradeLevelChangedMsg.Name, Me.lblGradeLevelChangedMsg.Text)
			Me.lblActualDate.Text = Language._Object.getCaption(Me.lblActualDate.Name, Me.lblActualDate.Text)

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
        End Try
    End Sub
			
			
    Private Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Please select atleast one employee to process Salary Increment.")
            Language.setMessage(mstrModuleName, 2, "Employee")
            Language.setMessage(mstrModuleName, 3, "Please select Pay Period. Pay Period is mandatory information.")
            Language.setMessage(mstrModuleName, 4, "Increment Date should be in between")
            Language.setMessage(mstrModuleName, 5, "Salary Increment Process completed successfully.")
            Language.setMessage(mstrModuleName, 6, " And")
            Language.setMessage(mstrModuleName, 7, "Expected New Scale reached the level of Maximum Scale.")
            Language.setMessage(mstrModuleName, 8, "Period")
            Language.setMessage(mstrModuleName, 9, "Date")
            Language.setMessage(mstrModuleName, 10, "Old Scale")
            Language.setMessage(mstrModuleName, 11, "New Scale")
            Language.setMessage(mstrModuleName, 12, "Reason")
            Language.setMessage(mstrModuleName, 13, "Notifications to Approve Employee Salary Change")
            Language.setMessage(mstrModuleName, 14, "Sorry, Process Payroll is already done for last date of selected Period for some of the employees. Please Void Process Payroll first for selected employee to give salary change.")
            Language.setMessage(mstrModuleName, 15, "Sorry, Payment of Salary for some of the selected employees is already done for this period. Please Void Payment for selected employee to give salary change.")
            Language.setMessage(mstrModuleName, 16, "Please select Reason. Reason is mandatory information.")
            Language.setMessage(mstrModuleName, 17, "You have ticked Copy Previous ED Slab. This will copy Previous Period Slab.")
            Language.setMessage(mstrModuleName, 18, "Do you want to continue?")
			Language.setMessage(mstrModuleName, 19, "Sorry, Increment date is mandatory information. Please provide Increment date to continue.")
			Language.setMessage(mstrModuleName, 20, "Increment date is not set as selected period start date, So the salary will be prorated as per increment date.")
			Language.setMessage(mstrModuleName, 21, "Are you sure you want to continue?")
			Language.setMessage(mstrModuleName, 22, "Dear")
			Language.setMessage(mstrModuleName, 23, "This is to inform you that following changes have been made in salary for the following employee(s).")
			Language.setMessage(mstrModuleName, 24, "By user")
			Language.setMessage(mstrModuleName, 25, "from Machine")
			Language.setMessage(mstrModuleName, 26, "and IPAddress")
			Language.setMessage(mstrModuleName, 27, "Please click on the following link to Approve/Reject these changes.")
			Language.setMessage(mstrModuleName, 28, "Do you want to void Payroll?")
			Language.setMessage(mstrModuleName, 29, "Are you sure you want to continue?")
			Language.setMessage(mstrModuleName, 30, "Sorry, Actual date is mandatory information. Please provide Actual date to continue.")
			Language.setMessage(mstrModuleName, 31, "Actual Date should not greater than selected period end date.")
			Language.setMessage(mstrModuleName, 32, "Actual Date should not be greater than Increment Date.")
			Language.setMessage(mstrModuleName, 33, "Actual date is set in past period, Which may cause salary arrears.")
			Language.setMessage(mstrModuleName, 34, "Sorry, Some salary changes are already done from another machine. Please refresh list to do further operation.")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class