﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmSalaryChangeMapping
    Inherits eZee.Common.eZeeForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmSalaryChangeMapping))
        Me.pnlMainInfo = New System.Windows.Forms.Panel
        Me.gbFieldMapping = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.lblPromotionDate = New System.Windows.Forms.Label
        Me.cboPromotionDate = New System.Windows.Forms.ComboBox
        Me.lblChangeType = New System.Windows.Forms.Label
        Me.cboChangeType = New System.Windows.Forms.ComboBox
        Me.cboGradeLevel = New System.Windows.Forms.ComboBox
        Me.cboGrade = New System.Windows.Forms.ComboBox
        Me.cboGradeGroup = New System.Windows.Forms.ComboBox
        Me.lblGradeGroup = New System.Windows.Forms.Label
        Me.lblGradeLevel = New System.Windows.Forms.Label
        Me.lblGrade = New System.Windows.Forms.Label
        Me.cboIncrementBy = New System.Windows.Forms.ComboBox
        Me.lblIncrementBy = New System.Windows.Forms.Label
        Me.objbtnAddReason = New eZee.Common.eZeeGradientButton
        Me.cboReason = New System.Windows.Forms.ComboBox
        Me.lblReason = New System.Windows.Forms.Label
        Me.dtpIncrementdate = New System.Windows.Forms.DateTimePicker
        Me.lblIncrementDate = New System.Windows.Forms.Label
        Me.lblPeriod = New System.Windows.Forms.Label
        Me.cboPeriod = New System.Windows.Forms.ComboBox
        Me.cboAmount = New System.Windows.Forms.ComboBox
        Me.cboEmployeeCode = New System.Windows.Forms.ComboBox
        Me.lblAmount = New System.Windows.Forms.Label
        Me.lblEmployeecode = New System.Windows.Forms.Label
        Me.objefFormFooter = New eZee.Common.eZeeFooter
        Me.chkChangeGrade = New System.Windows.Forms.CheckBox
        Me.btnCancel = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnOk = New eZee.Common.eZeeLightButton(Me.components)
        Me.dtpActualDate = New System.Windows.Forms.DateTimePicker
        Me.lblActualDate = New System.Windows.Forms.Label
        Me.pnlMainInfo.SuspendLayout()
        Me.gbFieldMapping.SuspendLayout()
        Me.objefFormFooter.SuspendLayout()
        Me.SuspendLayout()
        '
        'pnlMainInfo
        '
        Me.pnlMainInfo.Controls.Add(Me.gbFieldMapping)
        Me.pnlMainInfo.Controls.Add(Me.objefFormFooter)
        Me.pnlMainInfo.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pnlMainInfo.Location = New System.Drawing.Point(0, 0)
        Me.pnlMainInfo.Name = "pnlMainInfo"
        Me.pnlMainInfo.Size = New System.Drawing.Size(374, 417)
        Me.pnlMainInfo.TabIndex = 0
        '
        'gbFieldMapping
        '
        Me.gbFieldMapping.BorderColor = System.Drawing.Color.Black
        Me.gbFieldMapping.Checked = False
        Me.gbFieldMapping.CollapseAllExceptThis = False
        Me.gbFieldMapping.CollapsedHoverImage = Nothing
        Me.gbFieldMapping.CollapsedNormalImage = Nothing
        Me.gbFieldMapping.CollapsedPressedImage = Nothing
        Me.gbFieldMapping.CollapseOnLoad = False
        Me.gbFieldMapping.Controls.Add(Me.dtpActualDate)
        Me.gbFieldMapping.Controls.Add(Me.lblActualDate)
        Me.gbFieldMapping.Controls.Add(Me.lblPromotionDate)
        Me.gbFieldMapping.Controls.Add(Me.cboPromotionDate)
        Me.gbFieldMapping.Controls.Add(Me.lblChangeType)
        Me.gbFieldMapping.Controls.Add(Me.cboChangeType)
        Me.gbFieldMapping.Controls.Add(Me.cboGradeLevel)
        Me.gbFieldMapping.Controls.Add(Me.cboGrade)
        Me.gbFieldMapping.Controls.Add(Me.cboGradeGroup)
        Me.gbFieldMapping.Controls.Add(Me.lblGradeGroup)
        Me.gbFieldMapping.Controls.Add(Me.lblGradeLevel)
        Me.gbFieldMapping.Controls.Add(Me.lblGrade)
        Me.gbFieldMapping.Controls.Add(Me.cboIncrementBy)
        Me.gbFieldMapping.Controls.Add(Me.lblIncrementBy)
        Me.gbFieldMapping.Controls.Add(Me.objbtnAddReason)
        Me.gbFieldMapping.Controls.Add(Me.cboReason)
        Me.gbFieldMapping.Controls.Add(Me.lblReason)
        Me.gbFieldMapping.Controls.Add(Me.dtpIncrementdate)
        Me.gbFieldMapping.Controls.Add(Me.lblIncrementDate)
        Me.gbFieldMapping.Controls.Add(Me.lblPeriod)
        Me.gbFieldMapping.Controls.Add(Me.cboPeriod)
        Me.gbFieldMapping.Controls.Add(Me.cboAmount)
        Me.gbFieldMapping.Controls.Add(Me.cboEmployeeCode)
        Me.gbFieldMapping.Controls.Add(Me.lblAmount)
        Me.gbFieldMapping.Controls.Add(Me.lblEmployeecode)
        Me.gbFieldMapping.Dock = System.Windows.Forms.DockStyle.Fill
        Me.gbFieldMapping.ExpandedHoverImage = Nothing
        Me.gbFieldMapping.ExpandedNormalImage = Nothing
        Me.gbFieldMapping.ExpandedPressedImage = Nothing
        Me.gbFieldMapping.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbFieldMapping.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbFieldMapping.HeaderHeight = 25
        Me.gbFieldMapping.HeaderMessage = ""
        Me.gbFieldMapping.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbFieldMapping.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbFieldMapping.HeightOnCollapse = 0
        Me.gbFieldMapping.LeftTextSpace = 0
        Me.gbFieldMapping.Location = New System.Drawing.Point(0, 0)
        Me.gbFieldMapping.Name = "gbFieldMapping"
        Me.gbFieldMapping.OpenHeight = 300
        Me.gbFieldMapping.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbFieldMapping.ShowBorder = True
        Me.gbFieldMapping.ShowCheckBox = False
        Me.gbFieldMapping.ShowCollapseButton = False
        Me.gbFieldMapping.ShowDefaultBorderColor = True
        Me.gbFieldMapping.ShowDownButton = False
        Me.gbFieldMapping.ShowHeader = True
        Me.gbFieldMapping.Size = New System.Drawing.Size(374, 362)
        Me.gbFieldMapping.TabIndex = 0
        Me.gbFieldMapping.Temp = 0
        Me.gbFieldMapping.Text = "Field Mapping"
        Me.gbFieldMapping.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblPromotionDate
        '
        Me.lblPromotionDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPromotionDate.Location = New System.Drawing.Point(12, 255)
        Me.lblPromotionDate.Name = "lblPromotionDate"
        Me.lblPromotionDate.Size = New System.Drawing.Size(113, 14)
        Me.lblPromotionDate.TabIndex = 270
        Me.lblPromotionDate.Text = "Promotion Date"
        '
        'cboPromotionDate
        '
        Me.cboPromotionDate.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboPromotionDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboPromotionDate.FormattingEnabled = True
        Me.cboPromotionDate.Location = New System.Drawing.Point(131, 252)
        Me.cboPromotionDate.Name = "cboPromotionDate"
        Me.cboPromotionDate.Size = New System.Drawing.Size(203, 21)
        Me.cboPromotionDate.TabIndex = 269
        '
        'lblChangeType
        '
        Me.lblChangeType.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblChangeType.Location = New System.Drawing.Point(12, 147)
        Me.lblChangeType.Name = "lblChangeType"
        Me.lblChangeType.Size = New System.Drawing.Size(113, 14)
        Me.lblChangeType.TabIndex = 267
        Me.lblChangeType.Text = "Change Type"
        '
        'cboChangeType
        '
        Me.cboChangeType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboChangeType.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboChangeType.FormattingEnabled = True
        Me.cboChangeType.Location = New System.Drawing.Point(131, 144)
        Me.cboChangeType.Name = "cboChangeType"
        Me.cboChangeType.Size = New System.Drawing.Size(203, 21)
        Me.cboChangeType.TabIndex = 266
        '
        'cboGradeLevel
        '
        Me.cboGradeLevel.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboGradeLevel.Enabled = False
        Me.cboGradeLevel.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboGradeLevel.FormattingEnabled = True
        Me.cboGradeLevel.Location = New System.Drawing.Point(132, 333)
        Me.cboGradeLevel.Name = "cboGradeLevel"
        Me.cboGradeLevel.Size = New System.Drawing.Size(202, 21)
        Me.cboGradeLevel.TabIndex = 8
        '
        'cboGrade
        '
        Me.cboGrade.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboGrade.Enabled = False
        Me.cboGrade.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboGrade.FormattingEnabled = True
        Me.cboGrade.Location = New System.Drawing.Point(132, 306)
        Me.cboGrade.Name = "cboGrade"
        Me.cboGrade.Size = New System.Drawing.Size(202, 21)
        Me.cboGrade.TabIndex = 7
        '
        'cboGradeGroup
        '
        Me.cboGradeGroup.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboGradeGroup.Enabled = False
        Me.cboGradeGroup.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboGradeGroup.FormattingEnabled = True
        Me.cboGradeGroup.Location = New System.Drawing.Point(132, 279)
        Me.cboGradeGroup.Name = "cboGradeGroup"
        Me.cboGradeGroup.Size = New System.Drawing.Size(202, 21)
        Me.cboGradeGroup.TabIndex = 6
        '
        'lblGradeGroup
        '
        Me.lblGradeGroup.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblGradeGroup.Location = New System.Drawing.Point(12, 282)
        Me.lblGradeGroup.Name = "lblGradeGroup"
        Me.lblGradeGroup.Size = New System.Drawing.Size(113, 14)
        Me.lblGradeGroup.TabIndex = 264
        Me.lblGradeGroup.Text = "Grade Group Code"
        '
        'lblGradeLevel
        '
        Me.lblGradeLevel.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblGradeLevel.Location = New System.Drawing.Point(12, 336)
        Me.lblGradeLevel.Name = "lblGradeLevel"
        Me.lblGradeLevel.Size = New System.Drawing.Size(113, 14)
        Me.lblGradeLevel.TabIndex = 263
        Me.lblGradeLevel.Text = "Grade Level Code"
        '
        'lblGrade
        '
        Me.lblGrade.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblGrade.Location = New System.Drawing.Point(12, 309)
        Me.lblGrade.Name = "lblGrade"
        Me.lblGrade.Size = New System.Drawing.Size(113, 14)
        Me.lblGrade.TabIndex = 262
        Me.lblGrade.Text = "Grade Code"
        '
        'cboIncrementBy
        '
        Me.cboIncrementBy.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend
        Me.cboIncrementBy.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.cboIncrementBy.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboIncrementBy.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboIncrementBy.FormattingEnabled = True
        Me.cboIncrementBy.Location = New System.Drawing.Point(131, 117)
        Me.cboIncrementBy.Name = "cboIncrementBy"
        Me.cboIncrementBy.Size = New System.Drawing.Size(203, 21)
        Me.cboIncrementBy.TabIndex = 2
        '
        'lblIncrementBy
        '
        Me.lblIncrementBy.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblIncrementBy.Location = New System.Drawing.Point(12, 120)
        Me.lblIncrementBy.Name = "lblIncrementBy"
        Me.lblIncrementBy.Size = New System.Drawing.Size(113, 14)
        Me.lblIncrementBy.TabIndex = 257
        Me.lblIncrementBy.Text = "Change By"
        '
        'objbtnAddReason
        '
        Me.objbtnAddReason.BackColor = System.Drawing.Color.Transparent
        Me.objbtnAddReason.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnAddReason.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnAddReason.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnAddReason.BorderSelected = False
        Me.objbtnAddReason.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnAddReason.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Add
        Me.objbtnAddReason.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnAddReason.Location = New System.Drawing.Point(340, 171)
        Me.objbtnAddReason.Name = "objbtnAddReason"
        Me.objbtnAddReason.Size = New System.Drawing.Size(21, 21)
        Me.objbtnAddReason.TabIndex = 254
        '
        'cboReason
        '
        Me.cboReason.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboReason.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboReason.FormattingEnabled = True
        Me.cboReason.Location = New System.Drawing.Point(131, 171)
        Me.cboReason.Name = "cboReason"
        Me.cboReason.Size = New System.Drawing.Size(203, 21)
        Me.cboReason.TabIndex = 3
        '
        'lblReason
        '
        Me.lblReason.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblReason.Location = New System.Drawing.Point(12, 174)
        Me.lblReason.Name = "lblReason"
        Me.lblReason.Size = New System.Drawing.Size(113, 14)
        Me.lblReason.TabIndex = 253
        Me.lblReason.Text = "Reason"
        '
        'dtpIncrementdate
        '
        Me.dtpIncrementdate.Checked = False
        Me.dtpIncrementdate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpIncrementdate.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpIncrementdate.Location = New System.Drawing.Point(131, 63)
        Me.dtpIncrementdate.Name = "dtpIncrementdate"
        Me.dtpIncrementdate.ShowCheckBox = True
        Me.dtpIncrementdate.Size = New System.Drawing.Size(105, 21)
        Me.dtpIncrementdate.TabIndex = 1
        '
        'lblIncrementDate
        '
        Me.lblIncrementDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblIncrementDate.Location = New System.Drawing.Point(12, 66)
        Me.lblIncrementDate.Name = "lblIncrementDate"
        Me.lblIncrementDate.Size = New System.Drawing.Size(113, 14)
        Me.lblIncrementDate.TabIndex = 234
        Me.lblIncrementDate.Text = "Change Date"
        '
        'lblPeriod
        '
        Me.lblPeriod.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPeriod.Location = New System.Drawing.Point(12, 39)
        Me.lblPeriod.Name = "lblPeriod"
        Me.lblPeriod.Size = New System.Drawing.Size(113, 14)
        Me.lblPeriod.TabIndex = 231
        Me.lblPeriod.Text = "Effective Period"
        Me.lblPeriod.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboPeriod
        '
        Me.cboPeriod.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboPeriod.DropDownWidth = 215
        Me.cboPeriod.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboPeriod.FormattingEnabled = True
        Me.cboPeriod.Location = New System.Drawing.Point(131, 36)
        Me.cboPeriod.Name = "cboPeriod"
        Me.cboPeriod.Size = New System.Drawing.Size(203, 21)
        Me.cboPeriod.TabIndex = 0
        '
        'cboAmount
        '
        Me.cboAmount.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboAmount.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboAmount.FormattingEnabled = True
        Me.cboAmount.Location = New System.Drawing.Point(132, 225)
        Me.cboAmount.Name = "cboAmount"
        Me.cboAmount.Size = New System.Drawing.Size(202, 21)
        Me.cboAmount.TabIndex = 5
        '
        'cboEmployeeCode
        '
        Me.cboEmployeeCode.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboEmployeeCode.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboEmployeeCode.FormattingEnabled = True
        Me.cboEmployeeCode.Location = New System.Drawing.Point(131, 198)
        Me.cboEmployeeCode.Name = "cboEmployeeCode"
        Me.cboEmployeeCode.Size = New System.Drawing.Size(203, 21)
        Me.cboEmployeeCode.TabIndex = 4
        '
        'lblAmount
        '
        Me.lblAmount.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblAmount.Location = New System.Drawing.Point(12, 228)
        Me.lblAmount.Name = "lblAmount"
        Me.lblAmount.Size = New System.Drawing.Size(113, 14)
        Me.lblAmount.TabIndex = 3
        Me.lblAmount.Text = "Increment Amt/Perc."
        Me.lblAmount.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblEmployeecode
        '
        Me.lblEmployeecode.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEmployeecode.Location = New System.Drawing.Point(12, 201)
        Me.lblEmployeecode.Name = "lblEmployeecode"
        Me.lblEmployeecode.Size = New System.Drawing.Size(113, 14)
        Me.lblEmployeecode.TabIndex = 1
        Me.lblEmployeecode.Text = "Employee Code"
        Me.lblEmployeecode.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objefFormFooter
        '
        Me.objefFormFooter.BorderColor = System.Drawing.Color.Silver
        Me.objefFormFooter.Controls.Add(Me.chkChangeGrade)
        Me.objefFormFooter.Controls.Add(Me.btnCancel)
        Me.objefFormFooter.Controls.Add(Me.btnOk)
        Me.objefFormFooter.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.objefFormFooter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objefFormFooter.GradiantStyle = eZee.Common.eZeeFooter.GradientStyle.Up
        Me.objefFormFooter.GradientColor1 = System.Drawing.Color.Transparent
        Me.objefFormFooter.GradientColor2 = System.Drawing.Color.Transparent
        Me.objefFormFooter.Location = New System.Drawing.Point(0, 362)
        Me.objefFormFooter.Name = "objefFormFooter"
        Me.objefFormFooter.Size = New System.Drawing.Size(374, 55)
        Me.objefFormFooter.TabIndex = 1
        '
        'chkChangeGrade
        '
        Me.chkChangeGrade.AutoSize = True
        Me.chkChangeGrade.Location = New System.Drawing.Point(12, 13)
        Me.chkChangeGrade.Name = "chkChangeGrade"
        Me.chkChangeGrade.Size = New System.Drawing.Size(95, 17)
        Me.chkChangeGrade.TabIndex = 2
        Me.chkChangeGrade.Text = "Change &Grade"
        Me.chkChangeGrade.UseVisualStyleBackColor = True
        '
        'btnCancel
        '
        Me.btnCancel.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnCancel.BackColor = System.Drawing.Color.White
        Me.btnCancel.BackgroundImage = CType(resources.GetObject("btnCancel.BackgroundImage"), System.Drawing.Image)
        Me.btnCancel.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnCancel.BorderColor = System.Drawing.Color.Empty
        Me.btnCancel.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnCancel.FlatAppearance.BorderSize = 0
        Me.btnCancel.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnCancel.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnCancel.ForeColor = System.Drawing.Color.Black
        Me.btnCancel.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnCancel.GradientForeColor = System.Drawing.Color.Black
        Me.btnCancel.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnCancel.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnCancel.Location = New System.Drawing.Point(265, 13)
        Me.btnCancel.Name = "btnCancel"
        Me.btnCancel.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnCancel.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnCancel.Size = New System.Drawing.Size(97, 30)
        Me.btnCancel.TabIndex = 1
        Me.btnCancel.Text = "&Cancel"
        Me.btnCancel.UseVisualStyleBackColor = False
        '
        'btnOk
        '
        Me.btnOk.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnOk.BackColor = System.Drawing.Color.White
        Me.btnOk.BackgroundImage = CType(resources.GetObject("btnOk.BackgroundImage"), System.Drawing.Image)
        Me.btnOk.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnOk.BorderColor = System.Drawing.Color.Empty
        Me.btnOk.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnOk.FlatAppearance.BorderSize = 0
        Me.btnOk.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnOk.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnOk.ForeColor = System.Drawing.Color.Black
        Me.btnOk.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnOk.GradientForeColor = System.Drawing.Color.Black
        Me.btnOk.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnOk.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnOk.Location = New System.Drawing.Point(162, 13)
        Me.btnOk.Name = "btnOk"
        Me.btnOk.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnOk.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnOk.Size = New System.Drawing.Size(97, 30)
        Me.btnOk.TabIndex = 0
        Me.btnOk.Text = "&Ok"
        Me.btnOk.UseVisualStyleBackColor = False
        '
        'dtpActualDate
        '
        Me.dtpActualDate.Checked = False
        Me.dtpActualDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpActualDate.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpActualDate.Location = New System.Drawing.Point(131, 90)
        Me.dtpActualDate.Name = "dtpActualDate"
        Me.dtpActualDate.ShowCheckBox = True
        Me.dtpActualDate.Size = New System.Drawing.Size(105, 21)
        Me.dtpActualDate.TabIndex = 272
        '
        'lblActualDate
        '
        Me.lblActualDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblActualDate.Location = New System.Drawing.Point(12, 93)
        Me.lblActualDate.Name = "lblActualDate"
        Me.lblActualDate.Size = New System.Drawing.Size(113, 14)
        Me.lblActualDate.TabIndex = 273
        Me.lblActualDate.Text = "Actual Date"
        '
        'frmSalaryChangeMapping
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(374, 417)
        Me.Controls.Add(Me.pnlMainInfo)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmSalaryChangeMapping"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Salary Change Mapping"
        Me.pnlMainInfo.ResumeLayout(False)
        Me.gbFieldMapping.ResumeLayout(False)
        Me.objefFormFooter.ResumeLayout(False)
        Me.objefFormFooter.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents pnlMainInfo As System.Windows.Forms.Panel
    Friend WithEvents gbFieldMapping As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents lblPeriod As System.Windows.Forms.Label
    Friend WithEvents cboPeriod As System.Windows.Forms.ComboBox
    Friend WithEvents cboAmount As System.Windows.Forms.ComboBox
    Friend WithEvents cboEmployeeCode As System.Windows.Forms.ComboBox
    Friend WithEvents lblAmount As System.Windows.Forms.Label
    Friend WithEvents lblEmployeecode As System.Windows.Forms.Label
    Friend WithEvents objefFormFooter As eZee.Common.eZeeFooter
    Friend WithEvents btnCancel As eZee.Common.eZeeLightButton
    Friend WithEvents btnOk As eZee.Common.eZeeLightButton
    Friend WithEvents dtpIncrementdate As System.Windows.Forms.DateTimePicker
    Friend WithEvents lblIncrementDate As System.Windows.Forms.Label
    Friend WithEvents objbtnAddReason As eZee.Common.eZeeGradientButton
    Friend WithEvents cboReason As System.Windows.Forms.ComboBox
    Friend WithEvents lblReason As System.Windows.Forms.Label
    Friend WithEvents cboIncrementBy As System.Windows.Forms.ComboBox
    Friend WithEvents lblIncrementBy As System.Windows.Forms.Label
    Friend WithEvents chkChangeGrade As System.Windows.Forms.CheckBox
    Friend WithEvents cboGradeLevel As System.Windows.Forms.ComboBox
    Friend WithEvents cboGrade As System.Windows.Forms.ComboBox
    Friend WithEvents cboGradeGroup As System.Windows.Forms.ComboBox
    Friend WithEvents lblGradeGroup As System.Windows.Forms.Label
    Friend WithEvents lblGradeLevel As System.Windows.Forms.Label
    Friend WithEvents lblGrade As System.Windows.Forms.Label
    Friend WithEvents lblChangeType As System.Windows.Forms.Label
    Friend WithEvents cboChangeType As System.Windows.Forms.ComboBox
    Friend WithEvents lblPromotionDate As System.Windows.Forms.Label
    Friend WithEvents cboPromotionDate As System.Windows.Forms.ComboBox
    Friend WithEvents dtpActualDate As System.Windows.Forms.DateTimePicker
    Friend WithEvents lblActualDate As System.Windows.Forms.Label
End Class
