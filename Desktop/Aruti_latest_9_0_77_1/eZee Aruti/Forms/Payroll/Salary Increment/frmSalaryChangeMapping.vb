﻿Option Strict On

#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data

#End Region

Public Class frmSalaryChangeMapping

#Region " Private variables "

    Private ReadOnly mstrModuleName As String = "frmSalaryChangeMapping"
    Private dsData As New DataSet
    Private mblnCancel As Boolean = True
    Private dtTable As DataTable
    Private mdtPayPeriodStartDate As Date = Nothing
    Private mdtPayPeriodEndDate As Date = Nothing
    Private mintPeriodUnkId As Integer = -1
    Private mdtIncrementDate As DateTime = Nothing
    Private mintReasonUnkId As Integer = -1
    Private mintChangeTypeId As Integer = 0
    'Sohail (21 Jan 2020) -- Start
    'NMB Enhancement # : Actual increment date on salary change screen for informational purpose.
    Private mdtActualDate As DateTime = Nothing
    'Sohail (21 Jan 2020) -- End

#End Region

#Region " Properties "

    Public ReadOnly Property _DataTable() As DataTable
        Get
            Return dtTable
        End Get
    End Property

    Public ReadOnly Property _PeriodId() As Integer
        Get
            Return mintPeriodUnkId
        End Get
    End Property

    Public ReadOnly Property _IncrementDate() As DateTime
        Get
            Return mdtIncrementDate
        End Get
    End Property

    Public ReadOnly Property _ReasonId() As Integer
        Get
            Return mintReasonUnkId
        End Get
    End Property

    Public ReadOnly Property _ChangeTypeId() As Integer
        Get
            Return mintChangeTypeId
        End Get
    End Property

    'Sohail (21 Jan 2020) -- Start
    'NMB Enhancement # : Actual increment date on salary change screen for informational purpose.
    Public ReadOnly Property _ActualDate() As DateTime
        Get
            Return mdtActualDate
        End Get
    End Property
    'Sohail (21 Jan 2020) -- End

#End Region

#Region " Display Dialog "

    Public Function displayDialog(ByVal dsList As DataSet) As Boolean
        Try
            dsData = dsList
            Me.ShowDialog()
            Return Not mblnCancel
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "", mstrModuleName)
        End Try
    End Function

#End Region

#Region " Private Methods "
    Private Sub SetColor()
        Try
            cboEmployeeCode.BackColor = GUI.ColorComp
            cboAmount.BackColor = GUI.ColorComp
            cboPeriod.BackColor = GUI.ColorComp
            cboIncrementBy.BackColor = GUI.ColorComp
            cboReason.BackColor = GUI.ColorComp
            cboGrade.BackColor = GUI.ColorComp
            cboGradeGroup.BackColor = GUI.ColorComp
            cboGradeLevel.BackColor = GUI.ColorComp
            cboChangeType.BackColor = GUI.ColorComp
            cboPromotionDate.BackColor = GUI.ColorOptional
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetColor", mstrModuleName)
        End Try
    End Sub

    Private Sub FillCombo()
        Dim dsCombos As New DataSet
        Dim objPeriod As New clscommom_period_Tran
        Dim objMaster As New clsMasterData
        Dim objCommon As New clsCommon_Master
        Try

            dsCombos = objPeriod.getListForCombo(enModuleReference.Payroll, FinancialYear._Object._YearUnkid, FinancialYear._Object._DatabaseName, FinancialYear._Object._Database_Start_Date, "Period", True, enStatusType.Open)
            With cboPeriod
                .BeginUpdate()
                .ValueMember = "periodunkid"
                .DisplayMember = "name"
                .DataSource = dsCombos.Tables("Period")
                .SelectedValue = 0
                .EndUpdate()
            End With

            dsCombos = objMaster.getComboListSalaryIncrementBy(True, "IncrMode")
            With cboIncrementBy
                .ValueMember = "Id"
                .DisplayMember = "Name"
                .DataSource = New DataView(dsCombos.Tables("IncrMode"), "Id <> " & enSalaryIncrementBy.Grade & " ", "", DataViewRowState.CurrentRows).ToTable
                .SelectedValue = 0
            End With

            dsCombos = objCommon.getComboList(clsCommon_Master.enCommonMaster.SALINC_REASON, True, "SalReason")
            With cboReason
                .BeginUpdate()
                .ValueMember = "masterunkid"
                .DisplayMember = "Name"
                .DataSource = dsCombos.Tables("SalReason")
                .SelectedValue = 0
                .EndUpdate()
            End With

            dsCombos = objMaster.getComboListForSalaryChangeType("List", True)
            With cboChangeType
                .ValueMember = "Id"
                .DisplayMember = "Name"
                .DataSource = dsCombos.Tables("List")
                .SelectedValue = 0
            End With

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        Finally
            objPeriod = Nothing
            objCommon = Nothing
        End Try
    End Sub

    Private Sub SetData()
        Try

            dtTable = New DataTable("Salary")
            For Each dtColumns As DataColumn In dsData.Tables(0).Columns
                cboAmount.Items.Add(dtColumns.ColumnName)
                cboEmployeeCode.Items.Add(dtColumns.ColumnName)
                cboGradeGroup.Items.Add(dtColumns.ColumnName)
                cboGrade.Items.Add(dtColumns.ColumnName)
                cboGradeLevel.Items.Add(dtColumns.ColumnName)
                cboPromotionDate.Items.Add(dtColumns.ColumnName)
            Next

            dtTable.Columns.Add("employeeunkid", System.Type.GetType("System.Int32")).DefaultValue = -1
            dtTable.Columns.Add("EmpCode", System.Type.GetType("System.String")).DefaultValue = ""
            dtTable.Columns.Add("EmpName", System.Type.GetType("System.String")).DefaultValue = ""
            dtTable.Columns.Add("currentscale", System.Type.GetType("System.Decimal")).DefaultValue = 0
            dtTable.Columns.Add("increment", System.Type.GetType("System.Decimal")).DefaultValue = 0
            dtTable.Columns.Add("newscale", System.Type.GetType("System.Decimal")).DefaultValue = 0
            dtTable.Columns.Add("gradegroupunkid", System.Type.GetType("System.Int32")).DefaultValue = -1
            dtTable.Columns.Add("gradeunkid", System.Type.GetType("System.Int32")).DefaultValue = -1
            dtTable.Columns.Add("gradelevelunkid", System.Type.GetType("System.Int32")).DefaultValue = -1
            dtTable.Columns.Add("increment_mode", System.Type.GetType("System.Int32")).DefaultValue = 0
            dtTable.Columns.Add("percentage", System.Type.GetType("System.Double")).DefaultValue = 0
            dtTable.Columns.Add("reasonid", System.Type.GetType("System.Int32")).DefaultValue = -1
            dtTable.Columns.Add("reason", System.Type.GetType("System.String")).DefaultValue = ""
            dtTable.Columns.Add("IsChecked", System.Type.GetType("System.Boolean")).DefaultValue = False
            dtTable.Columns.Add("rowtypeid", System.Type.GetType("System.Int32")).DefaultValue = -1
            dtTable.Columns.Add("isgradechange", System.Type.GetType("System.Boolean")).DefaultValue = False
            dtTable.Columns.Add("remark", System.Type.GetType("System.String")).DefaultValue = ""
            dtTable.Columns.Add("promotion_date", GetType(DateTime)).DefaultValue = Nothing
            'Sohail (21 Jan 2020) -- Start
            'NMB Enhancement # : Actual increment date on salary change screen for informational purpose.
            dtTable.Columns.Add("actualdate", System.Type.GetType("System.DateTime")).DefaultValue = Nothing
            dtTable.Columns.Add("arrears_amount", System.Type.GetType("System.Decimal")).DefaultValue = 0
            dtTable.Columns.Add("arrears_countryid", System.Type.GetType("System.Int32")).DefaultValue = 0
            dtTable.Columns.Add("noofinstallment", System.Type.GetType("System.Int32")).DefaultValue = 0
            dtTable.Columns.Add("emi_amount", System.Type.GetType("System.Decimal")).DefaultValue = 0
            dtTable.Columns.Add("approval_statusunkid", System.Type.GetType("System.Int32")).DefaultValue = 0
            dtTable.Columns.Add("finalapproverunkid", System.Type.GetType("System.Int32")).DefaultValue = 0
            dtTable.Columns.Add("arrears_statusunkid", System.Type.GetType("System.Int32")).DefaultValue = 0
            'Sohail (21 Jan 2020) -- End

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetData", mstrModuleName)
        End Try
    End Sub

    Private Function IsValidate() As Boolean
        Try
            If CInt(cboPeriod.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please select Period. Period is mandatory information."), enMsgBoxStyle.Information)
                cboPeriod.Focus()
                Exit Try
                'Sohail (06 Feb 2019) -- Start
                'NMB Enhancement - 76.1 - Set Increment date selection mandatory and Notify user if Salary will be prorated.
            ElseIf dtpIncrementdate.Checked = False Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 36, "Sorry, Increment date is mandatory information. Please provide Increment date to continue."), enMsgBoxStyle.Information)
                dtpIncrementdate.Focus()
                Return False
                'Sohail (06 Feb 2019) -- End
            ElseIf dtpIncrementdate.Value.Date > mdtPayPeriodEndDate Or dtpIncrementdate.Value.Date < mdtPayPeriodStartDate Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Increment Date should be in between ") & mdtPayPeriodStartDate & Language.getMessage(mstrModuleName, 4, " And ") & mdtPayPeriodEndDate & ".", enMsgBoxStyle.Information)
                dtpIncrementdate.Focus()
                Exit Try
                'Sohail (21 Jan 2020) -- Start
                'NMB Enhancement # : Actual increment date on salary change screen for informational purpose.
            ElseIf dtpActualDate.Checked = False Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 39, "Sorry, Actual date is mandatory information. Please provide Actual date to continue."), enMsgBoxStyle.Information)
                dtpActualDate.Focus()
                Return False
            ElseIf dtpActualDate.Value.Date > mdtPayPeriodEndDate Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 40, "Actual Date should not greater than selected period end date."), enMsgBoxStyle.Information)
                dtpActualDate.Focus()
                Return False
            ElseIf eZeeDate.convertDate(dtpActualDate.Value.Date) > eZeeDate.convertDate(dtpIncrementdate.Value.Date) Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 41, "Actual Date should not be greater than Increment Date."), enMsgBoxStyle.Information)
                dtpActualDate.Focus()
                Return False
                'Sohail (21 Jan 2020) -- End
            ElseIf CInt(cboEmployeeCode.SelectedIndex) < 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Please select field for Employee Code."), enMsgBoxStyle.Information)
                cboEmployeeCode.Focus()
                Exit Try
            ElseIf CInt(cboAmount.SelectedIndex) < 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "Please select field for Increment Amount."), enMsgBoxStyle.Information)
                cboAmount.Focus()
                Exit Try
            ElseIf chkChangeGrade.Checked = False AndAlso CInt(cboIncrementBy.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 6, "Please select Increment By. Increment By is mandatory information."), enMsgBoxStyle.Information)
                cboIncrementBy.Focus()
                Exit Try
            ElseIf CInt(cboReason.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 7, "Please select Reason. Reason is mandatory information. "), enMsgBoxStyle.Information)
                cboReason.Focus()
                Exit Try
            ElseIf chkChangeGrade.Checked = True Then
                If CInt(cboGradeGroup.SelectedIndex) < 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 8, "Please select field for Grade Group."), enMsgBoxStyle.Information)
                    cboGradeGroup.Focus()
                    Exit Try
                ElseIf CInt(cboGrade.SelectedIndex) < 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 9, "Please select field for Grade."), enMsgBoxStyle.Information)
                    cboGrade.Focus()
                    Exit Try
                ElseIf CInt(cboGradeLevel.SelectedIndex) < 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 10, "Please select field for Grade Level."), enMsgBoxStyle.Information)
                    cboGradeLevel.Focus()
                    Exit Try
                End If
            ElseIf CInt(cboChangeType.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 32, "Please select change type. Change type is mandatory information."), enMsgBoxStyle.Information)
                cboChangeType.Focus()
                Exit Try
            ElseIf CInt(cboChangeType.SelectedValue) = enSalaryChangeType.SALARY_CHANGE_WITH_PROMOTION AndAlso cboPromotionDate.Text.Trim.Length <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 33, "Please select field for promotion date."), enMsgBoxStyle.Information)
                cboPromotionDate.Focus()
                Exit Try
            End If

            'Sohail (06 Feb 2019) -- Start
            'NMB Enhancement - 76.1 - Set Increment date selection mandatory and Notify user if Salary will be prorated.
            If dtpIncrementdate.Value.Date <> mdtPayPeriodStartDate Then
                If (eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 37, "Increment date is not set as selected period start date, So the salary will be prorated as per increment date.") & vbCrLf & vbCrLf & Language.getMessage(mstrModuleName, 38, "Are you sure you want to continue?"), CType(enMsgBoxStyle.Information + enMsgBoxStyle.YesNo, enMsgBoxStyle))) = Windows.Forms.DialogResult.No Then
                    dtpIncrementdate.Focus()
                    Return False
                End If
            End If
            'Sohail (06 Feb 2019) -- End

            'Sohail (21 Jan 2020) -- Start
            'NMB Enhancement # : Actual increment date on salary change screen for informational purpose.
            If dtpActualDate.Value.Date < mdtPayPeriodStartDate Then
                If (eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 42, "Actual date is set in past period, Which may cause salary arrears.") & vbCrLf & vbCrLf & Language.getMessage(mstrModuleName, 38, "Are you sure you want to continue?"), CType(enMsgBoxStyle.Information + enMsgBoxStyle.YesNo, enMsgBoxStyle))) = Windows.Forms.DialogResult.No Then
                    dtpActualDate.Focus()
                    Exit Try
                End If
            End If
            'Sohail (21 Jan 2020) -- End

            Return True
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "IsValidate", mstrModuleName)
        End Try
    End Function

#End Region

#Region " Form's Events "

    Private Sub frmSalaryChangeMapping_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed

    End Sub

    Private Sub frmSalaryChangeMapping_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Call Set_Logo(Me, gApplicationType)
            Call SetColor()
            Call Language.setLanguage(Me.Name)
            Call OtherSettings()
            cboAmount.Items.Clear()
            cboEmployeeCode.Items.Clear()
            cboGradeGroup.Items.Clear()
            cboGrade.Items.Clear()
            cboGradeLevel.Items.Clear()
            cboPromotionDate.Items.Clear()
            Call FillCombo()
            Call SetData()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmSalaryChangeMapping_Load", mstrModuleName)
        End Try
    End Sub

    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsSalaryIncrement.SetMessages()
            objfrm._Other_ModuleNames = "clsSalaryIncrement"
            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub

#End Region

#Region " Combobox's Events "

    Private Sub cboPeriod_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboPeriod.SelectedIndexChanged
        Dim objPeriod As New clscommom_period_Tran
        Try
            objPeriod._Periodunkid(FinancialYear._Object._DatabaseName) = CInt(cboPeriod.SelectedValue)
            mdtPayPeriodStartDate = objPeriod._Start_Date
            mdtPayPeriodEndDate = objPeriod._End_Date

            'Sohail (06 Feb 2019) -- Start
            'NMB Enhancement - 76.1 - Set Increment date selection mandatory and Notify user if Salary will be prorated.
            If CInt(cboPeriod.SelectedValue) > 0 Then
                dtpIncrementdate.Value = mdtPayPeriodStartDate.AddMinutes(DateDiff(DateInterval.Minute, DateAndTime.Now.Date, Now))
                dtpIncrementdate.Checked = False
                'Sohail (21 Jan 2020) -- Start
                'NMB Enhancement # : Actual increment date on salary change screen for informational purpose.
                dtpActualDate.Checked = dtpIncrementdate.Checked
                'Sohail (21 Jan 2020) -- End
            End If
            'Sohail (06 Feb 2019) -- End

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboPeriod_SelectedIndexChanged", mstrModuleName)
        Finally
            objPeriod = Nothing
        End Try
    End Sub

    Private Sub cboEmployeeCode_Validating(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles cboEmployeeCode.Validating, _
                                                                                                                             cboAmount.Validating, _
                                                                                                                             cboGradeGroup.Validating, _
                                                                                                                             cboGrade.Validating, _
                                                                                                                             cboGradeLevel.Validating, _
                                                                                                                             cboPromotionDate.Validating
        Try
            Dim cbo As ComboBox = CType(sender, ComboBox)

            If CInt(cbo.SelectedIndex) >= 0 Then
                Dim lst As IEnumerable(Of ComboBox) = gbFieldMapping.Controls.OfType(Of ComboBox)().Where(Function(t) t.Name <> cbo.Name AndAlso _
                                                                                                              t.Name <> cboPeriod.Name AndAlso _
                                                                                                              t.Name <> cboIncrementBy.Name AndAlso _
                                                                                                              t.Name <> cboReason.Name AndAlso _
                                                                                                              t.Name <> cboChangeType.Name AndAlso _
                                                                                                              CInt(t.SelectedIndex) = CInt(cbo.SelectedIndex))
                If lst.Count > 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 11, "Sorry, This column is already mapped."))
                    cbo.SelectedIndex = -1
                    e.Cancel = True
                End If
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboEmployeeCode_Validating", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Buttons Events "

    Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnCancel_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnOk_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnOk.Click

        Try
            If IsValidate() = False Then Exit Sub

            Dim dtData As DataTable = New DataView(dsData.Tables(0), " [" & cboEmployeeCode.Text & "] IS NOT NULL ", "", DataViewRowState.CurrentRows).ToTable

            Dim objEmp As New clsEmployee_Master
            Dim objSalary As New clsSalaryIncrement
            Dim objMaster As New clsMasterData
            Dim objWagesTran As New clsWagesTran
            Dim dsEmployee As DataSet
            Dim dtEmployee As DataTable
            Dim ds As DataSet

            dsEmployee = objEmp.GetEmployeeList(FinancialYear._Object._DatabaseName, _
                                           User._Object._Userunkid, _
                                           FinancialYear._Object._YearUnkid, _
                                           Company._Object._Companyunkid, _
                                           mdtPayPeriodStartDate, _
                                           mdtPayPeriodEndDate, _
                                           ConfigParameter._Object._UserAccessModeSetting, _
                                           True, False, "Employee", False)

            For Each dsRow As DataRow In dtData.Rows

                Dim dRow As DataRow

                dtEmployee = New DataView(dsEmployee.Tables("Employee"), "EmployeeCode = '" & CStr(dsRow.Item(cboEmployeeCode.Text).ToString.Trim) & "' ", "", DataViewRowState.CurrentRows).ToTable

                dRow = dtTable.NewRow
                If dtEmployee.Rows.Count > 0 Then

                    'Sohail (21 Jan 2020) -- Start
                    'NMB Enhancement # : Actual increment date on salary change screen for informational purpose.
                    dRow.Item("actualdate") = dtpActualDate.Value
                    'Sohail (21 Jan 2020) -- End

                    ds = objSalary.getLastIncrement("Incr", CInt(dtEmployee.Rows(0).Item("employeeunkid")), True)

                    If ds.Tables("Incr").Rows.Count > 0 Then
                        If eZeeDate.convertDate(dtpIncrementdate.Value.Date) <= ds.Tables("Incr").Rows(0).Item("incrementdate").ToString Then
                            dRow.Item("employeeunkid") = CInt(dtEmployee.Rows(0).Item("employeeunkid"))
                            dRow.Item("EmpCode") = dsRow.Item(cboEmployeeCode.Text).ToString.Trim
                            dRow.Item("EmpName") = dtEmployee.Rows(0).Item("employeename").ToString

                            ds = objMaster.Get_Current_Scale("CurrSalary", CInt(dtEmployee.Rows(0).Item("employeeunkid")), mdtPayPeriodEndDate)
                            If ds.Tables("CurrSalary").Rows.Count > 0 Then
                                dRow.Item("currentscale") = CDec(ds.Tables("CurrSalary").Rows(0).Item("newscale"))
                                dRow.Item("gradegroupunkid") = CInt(ds.Tables("CurrSalary").Rows(0).Item("gradegroupunkid"))
                                dRow.Item("gradeunkid") = CInt(ds.Tables("CurrSalary").Rows(0).Item("gradeunkid"))
                                dRow.Item("gradelevelunkid") = CInt(ds.Tables("CurrSalary").Rows(0).Item("gradelevelunkid"))
                            Else
                                dRow.Item("currentscale") = 0
                                dRow.Item("gradegroupunkid") = 0
                                dRow.Item("gradeunkid") = 0
                                dRow.Item("gradelevelunkid") = 0
                            End If

                            If CInt(cboIncrementBy.SelectedValue) = enSalaryIncrementBy.Percentage Then
                                If IsDBNull(dsRow.Item(cboAmount.Text)) = True Then
                                    dRow.Item("increment") = 0
                                Else
                                    dRow.Item("increment") = CDec(dRow.Item("currentscale")) * CDec(dsRow.Item(cboAmount.Text)) / 100
                                End If
                                dRow.Item("percentage") = CDec(dsRow.Item(cboAmount.Text))
                            Else
                                If IsDBNull(dsRow.Item(cboAmount.Text)) = True Then
                                    dRow.Item("increment") = 0
                                Else
                                    dRow.Item("increment") = CDec(dsRow.Item(cboAmount.Text))
                                End If
                                dRow.Item("percentage") = 0
                            End If

                            dRow.Item("newscale") = CDec(dRow.Item("currentscale")) + CDec(dRow.Item("increment"))


                            dRow.Item("increment_mode") = CInt(cboIncrementBy.SelectedValue)
                            dRow.Item("percentage") = 0

                            dRow.Item("rowtypeid") = 2 'Increment date should be greater than last increment date.
                            dRow.Item("remark") = Language.getMessage(mstrModuleName, 12, "Increment date should be greater than last increment date.")

                            dtTable.Rows.Add(dRow)

                            Continue For

                            'Sohail (21 Jan 2020) -- Start
                            'NMB Enhancement # : Actual increment date on salary change screen for informational purpose.
                        ElseIf eZeeDate.convertDate(dtpActualDate.Value.Date) <= ds.Tables("Incr").Rows(0).Item("incrementdate").ToString Then
                            dRow.Item("employeeunkid") = CInt(dtEmployee.Rows(0).Item("employeeunkid"))
                            dRow.Item("EmpCode") = dsRow.Item(cboEmployeeCode.Text).ToString.Trim
                            dRow.Item("EmpName") = dtEmployee.Rows(0).Item("employeename").ToString

                            ds = objMaster.Get_Current_Scale("CurrSalary", CInt(dtEmployee.Rows(0).Item("employeeunkid")), mdtPayPeriodEndDate)
                            If ds.Tables("CurrSalary").Rows.Count > 0 Then
                                dRow.Item("currentscale") = CDec(ds.Tables("CurrSalary").Rows(0).Item("newscale"))
                                dRow.Item("gradegroupunkid") = CInt(ds.Tables("CurrSalary").Rows(0).Item("gradegroupunkid"))
                                dRow.Item("gradeunkid") = CInt(ds.Tables("CurrSalary").Rows(0).Item("gradeunkid"))
                                dRow.Item("gradelevelunkid") = CInt(ds.Tables("CurrSalary").Rows(0).Item("gradelevelunkid"))
                            Else
                                dRow.Item("currentscale") = 0
                                dRow.Item("gradegroupunkid") = 0
                                dRow.Item("gradeunkid") = 0
                                dRow.Item("gradelevelunkid") = 0
                            End If

                            If CInt(cboIncrementBy.SelectedValue) = enSalaryIncrementBy.Percentage Then
                                If IsDBNull(dsRow.Item(cboAmount.Text)) = True Then
                                    dRow.Item("increment") = 0
                                Else
                                    dRow.Item("increment") = CDec(dRow.Item("currentscale")) * CDec(dsRow.Item(cboAmount.Text)) / 100
                                End If
                                dRow.Item("percentage") = CDec(dsRow.Item(cboAmount.Text))
                            Else
                                If IsDBNull(dsRow.Item(cboAmount.Text)) = True Then
                                    dRow.Item("increment") = 0
                                Else
                                    dRow.Item("increment") = CDec(dsRow.Item(cboAmount.Text))
                                End If
                                dRow.Item("percentage") = 0
                            End If

                            dRow.Item("newscale") = CDec(dRow.Item("currentscale")) + CDec(dRow.Item("increment"))


                            dRow.Item("increment_mode") = CInt(cboIncrementBy.SelectedValue)
                            dRow.Item("percentage") = 0

                            dRow.Item("rowtypeid") = 19
                            dRow.Item("remark") = Language.getMessage(mstrModuleName, 43, "Actual date should be greater than last increment date.")

                            dtTable.Rows.Add(dRow)

                            Continue For
                            'Sohail (21 Jan 2020) -- End

                            'Sohail (07 Sep 2013) -- Start
                            'TRA - ENHANCEMENT
                        ElseIf CBool(ds.Tables("Incr").Rows(0).Item("isapproved")) = False Then

                            dRow.Item("employeeunkid") = CInt(dtEmployee.Rows(0).Item("employeeunkid"))
                            dRow.Item("EmpCode") = dsRow.Item(cboEmployeeCode.Text).ToString.Trim
                            dRow.Item("EmpName") = dtEmployee.Rows(0).Item("employeename").ToString

                            ds = objMaster.Get_Current_Scale("CurrSalary", CInt(dtEmployee.Rows(0).Item("employeeunkid")), mdtPayPeriodEndDate)
                            If ds.Tables("CurrSalary").Rows.Count > 0 Then
                                dRow.Item("currentscale") = CDec(ds.Tables("CurrSalary").Rows(0).Item("newscale"))
                                dRow.Item("gradegroupunkid") = CInt(ds.Tables("CurrSalary").Rows(0).Item("gradegroupunkid"))
                                dRow.Item("gradeunkid") = CInt(ds.Tables("CurrSalary").Rows(0).Item("gradeunkid"))
                                dRow.Item("gradelevelunkid") = CInt(ds.Tables("CurrSalary").Rows(0).Item("gradelevelunkid"))
                            Else
                                dRow.Item("currentscale") = 0
                                dRow.Item("gradegroupunkid") = 0
                                dRow.Item("gradeunkid") = 0
                                dRow.Item("gradelevelunkid") = 0
                            End If

                            If CInt(cboIncrementBy.SelectedValue) = enSalaryIncrementBy.Percentage Then
                                If IsDBNull(dsRow.Item(cboAmount.Text)) = True Then
                                    dRow.Item("increment") = 0
                                Else
                                    dRow.Item("increment") = CDec(dRow.Item("currentscale")) * CDec(dsRow.Item(cboAmount.Text)) / 100
                                End If
                                dRow.Item("percentage") = CDec(dsRow.Item(cboAmount.Text))
                            Else
                                If IsDBNull(dsRow.Item(cboAmount.Text)) = True Then
                                    dRow.Item("increment") = 0
                                Else
                                    dRow.Item("increment") = CDec(dsRow.Item(cboAmount.Text))
                                End If
                                dRow.Item("percentage") = 0
                            End If

                            dRow.Item("newscale") = CDec(dRow.Item("currentscale")) + CDec(dRow.Item("increment"))


                            dRow.Item("increment_mode") = CInt(cboIncrementBy.SelectedValue)
                            dRow.Item("percentage") = 0

                            dRow.Item("rowtypeid") = 6 'Last Increment Not Approved.
                            dRow.Item("remark") = Language.getMessage(mstrModuleName, 13, "Last Salary Change is not Approved.")

                            dtTable.Rows.Add(dRow)

                            Continue For

                            'Sohail (07 Sep 2013) -- End

                            'Sohail (29 Dec 2012) -- Start
                            'TRA - ENHANCEMENT - Code Commented to allow to give increment more than one time in one period (Issue# 131)
                            'ElseIf objSalary.isExist(CInt(cboPeriod.SelectedValue), CInt(dtEmployee.Rows(0).Item("employeeunkid"))) = True Then
                        ElseIf objSalary.isExistOnSameDate(dtpIncrementdate.Value.Date, CInt(dtEmployee.Rows(0).Item("employeeunkid"))) = True Then
                            'Sohail (29 Dec 2012) -- End
                            dRow.Item("employeeunkid") = CInt(dtEmployee.Rows(0).Item("employeeunkid"))
                            dRow.Item("EmpCode") = dsRow.Item(cboEmployeeCode.Text).ToString.Trim
                            dRow.Item("EmpName") = dtEmployee.Rows(0).Item("employeename").ToString

                            ds = objMaster.Get_Current_Scale("CurrSalary", CInt(dtEmployee.Rows(0).Item("employeeunkid")), mdtPayPeriodEndDate)
                            If ds.Tables("CurrSalary").Rows.Count > 0 Then
                                dRow.Item("currentscale") = CDec(ds.Tables("CurrSalary").Rows(0).Item("newscale"))
                                dRow.Item("gradegroupunkid") = CInt(ds.Tables("CurrSalary").Rows(0).Item("gradegroupunkid"))
                                dRow.Item("gradeunkid") = CInt(ds.Tables("CurrSalary").Rows(0).Item("gradeunkid"))
                                dRow.Item("gradelevelunkid") = CInt(ds.Tables("CurrSalary").Rows(0).Item("gradelevelunkid"))
                            Else
                                dRow.Item("currentscale") = 0
                                dRow.Item("gradegroupunkid") = 0
                                dRow.Item("gradeunkid") = 0
                                dRow.Item("gradelevelunkid") = 0
                            End If

                            If IsDBNull(dsRow.Item(cboAmount.Text)) = True Then
                                dRow.Item("increment") = 0
                            Else
                                'dRow.Item("increment") = CDec(dsRow.Item(cboAmount.Text))
                                dRow.Item("increment") = 0
                            End If

                            dRow.Item("newscale") = CDec(dRow.Item("currentscale")) + CDec(dRow.Item("increment"))


                            dRow.Item("increment_mode") = CInt(cboIncrementBy.SelectedValue)
                            dRow.Item("percentage") = 0

                            dRow.Item("rowtypeid") = 3 'Increment is already done in this period.
                            dRow.Item("remark") = Language.getMessage(mstrModuleName, 14, "Increment is already done on selected Increment Date.")

                            dtTable.Rows.Add(dRow)

                            Continue For

                            'Sohail (21 Jan 2020) -- Start
                            'NMB Enhancement # : Actual increment date on salary change screen for informational purpose.
                        ElseIf objSalary.isExistOnSameDate(dtpActualDate.Value.Date, CInt(dtEmployee.Rows(0).Item("employeeunkid"))) = True Then
                            dRow.Item("employeeunkid") = CInt(dtEmployee.Rows(0).Item("employeeunkid"))
                            dRow.Item("EmpCode") = dsRow.Item(cboEmployeeCode.Text).ToString.Trim
                            dRow.Item("EmpName") = dtEmployee.Rows(0).Item("employeename").ToString

                            ds = objMaster.Get_Current_Scale("CurrSalary", CInt(dtEmployee.Rows(0).Item("employeeunkid")), mdtPayPeriodEndDate)
                            If ds.Tables("CurrSalary").Rows.Count > 0 Then
                                dRow.Item("currentscale") = CDec(ds.Tables("CurrSalary").Rows(0).Item("newscale"))
                                dRow.Item("gradegroupunkid") = CInt(ds.Tables("CurrSalary").Rows(0).Item("gradegroupunkid"))
                                dRow.Item("gradeunkid") = CInt(ds.Tables("CurrSalary").Rows(0).Item("gradeunkid"))
                                dRow.Item("gradelevelunkid") = CInt(ds.Tables("CurrSalary").Rows(0).Item("gradelevelunkid"))
                            Else
                                dRow.Item("currentscale") = 0
                                dRow.Item("gradegroupunkid") = 0
                                dRow.Item("gradeunkid") = 0
                                dRow.Item("gradelevelunkid") = 0
                            End If

                            If IsDBNull(dsRow.Item(cboAmount.Text)) = True Then
                                dRow.Item("increment") = 0
                            Else
                                'dRow.Item("increment") = CDec(dsRow.Item(cboAmount.Text))
                                dRow.Item("increment") = 0
                            End If

                            dRow.Item("newscale") = CDec(dRow.Item("currentscale")) + CDec(dRow.Item("increment"))


                            dRow.Item("increment_mode") = CInt(cboIncrementBy.SelectedValue)
                            dRow.Item("percentage") = 0

                            dRow.Item("rowtypeid") = 20 
                            dRow.Item("remark") = Language.getMessage(mstrModuleName, 44, "Increment is already done on selected Actual Date.")

                            dtTable.Rows.Add(dRow)

                            Continue For
                            'Sohail (21 Jan 2020) -- End

                        End If
                    End If

                    If IsDBNull(dsRow.Item(cboAmount.Text)) = True OrElse CDec(dsRow.Item(cboAmount.Text)) = 0 Then

                        dRow.Item("employeeunkid") = CInt(dtEmployee.Rows(0).Item("employeeunkid"))
                        dRow.Item("EmpCode") = dsRow.Item(cboEmployeeCode.Text).ToString.Trim
                        dRow.Item("EmpName") = dtEmployee.Rows(0).Item("employeename").ToString

                        dRow.Item("rowtypeid") = 4 'Increment amount is not given.
                        dRow.Item("remark") = Language.getMessage(mstrModuleName, 15, "Increment amount / Percentage / New Scale is not given.")

                        dtTable.Rows.Add(dRow)

                        Continue For

                    End If

                    'Sohail (10 Nov 2014) -- Start
                    'Enhancement - Change Grade option on Import Salary Change.
                    Dim intGradeGroupID As Integer = 0
                    Dim intGradeID As Integer = 0
                    Dim intGradeLevelID As Integer = 0
                    Dim strGradeLevelName As String = ""
                    If chkChangeGrade.Checked = True Then
                        'If IsDBNull(dsRow.Item(cboGradeGroup.Text)) = True OrElse IsDBNull(dsRow.Item(cboGrade.Text)) = True OrElse IsDBNull(dsRow.Item(cboGradeLevel.Text)) = True Then

                        'End If
                        If IsDBNull(dsRow.Item(cboGradeGroup.Text)) = True Then
                            dRow.Item("employeeunkid") = CInt(dtEmployee.Rows(0).Item("employeeunkid"))
                            dRow.Item("EmpCode") = dsRow.Item(cboEmployeeCode.Text).ToString.Trim
                            dRow.Item("EmpName") = dtEmployee.Rows(0).Item("employeename").ToString

                            dRow.Item("rowtypeid") = 7 'Grade Group is not given.
                            dRow.Item("remark") = Language.getMessage(mstrModuleName, 16, "Grade Group is not given.")

                            dtTable.Rows.Add(dRow)

                            Continue For

                        ElseIf IsDBNull(dsRow.Item(cboGrade.Text)) = True Then
                            dRow.Item("employeeunkid") = CInt(dtEmployee.Rows(0).Item("employeeunkid"))
                            dRow.Item("EmpCode") = dsRow.Item(cboEmployeeCode.Text).ToString.Trim
                            dRow.Item("EmpName") = dtEmployee.Rows(0).Item("employeename").ToString

                            dRow.Item("rowtypeid") = 8 'Grade is not given.
                            dRow.Item("remark") = Language.getMessage(mstrModuleName, 17, "Grade is not given.")

                            dtTable.Rows.Add(dRow)

                            Continue For

                        ElseIf IsDBNull(dsRow.Item(cboGradeLevel.Text)) = True Then
                            dRow.Item("employeeunkid") = CInt(dtEmployee.Rows(0).Item("employeeunkid"))
                            dRow.Item("EmpCode") = dsRow.Item(cboEmployeeCode.Text).ToString.Trim
                            dRow.Item("EmpName") = dtEmployee.Rows(0).Item("employeename").ToString

                            dRow.Item("rowtypeid") = 9 'Grade Level is not given.
                            dRow.Item("remark") = Language.getMessage(mstrModuleName, 18, "Grade Level is not given.")

                            dtTable.Rows.Add(dRow)

                            Continue For

                        End If


                        Dim objGradeGroup As New clsGradeGroup
                        If objGradeGroup.isExist(dsRow.Item(cboGradeGroup.Text).ToString, , , intGradeGroupID) = False Then
                            dRow.Item("employeeunkid") = CInt(dtEmployee.Rows(0).Item("employeeunkid"))
                            dRow.Item("EmpCode") = dsRow.Item(cboEmployeeCode.Text).ToString.Trim
                            dRow.Item("EmpName") = dtEmployee.Rows(0).Item("employeename").ToString

                            dRow.Item("rowtypeid") = 10 'Grade Group does not exist.
                            dRow.Item("remark") = Language.getMessage(mstrModuleName, 19, "Grade Group does not exist.")

                            dtTable.Rows.Add(dRow)

                            Continue For
                        End If

                        Dim objGrade As New clsGrade
                        If objGrade.isExist(dsRow.Item(cboGrade.Text).ToString, , , intGradeID) = False Then
                            dRow.Item("employeeunkid") = CInt(dtEmployee.Rows(0).Item("employeeunkid"))
                            dRow.Item("EmpCode") = dsRow.Item(cboEmployeeCode.Text).ToString.Trim
                            dRow.Item("EmpName") = dtEmployee.Rows(0).Item("employeename").ToString

                            dRow.Item("rowtypeid") = 11 'Grade does not exist.
                            dRow.Item("remark") = Language.getMessage(mstrModuleName, 20, "Grade does not exist.")

                            dtTable.Rows.Add(dRow)

                            Continue For
                        End If

                        Dim objGradeLevel As New clsGradeLevel
                        If objGradeLevel.isCodeExits(dsRow.Item(cboGradeLevel.Text).ToString, , strGradeLevelName) = False Then
                            dRow.Item("employeeunkid") = CInt(dtEmployee.Rows(0).Item("employeeunkid"))
                            dRow.Item("EmpCode") = dsRow.Item(cboEmployeeCode.Text).ToString.Trim
                            dRow.Item("EmpName") = dtEmployee.Rows(0).Item("employeename").ToString

                            dRow.Item("rowtypeid") = 12 'Grade Level does not exist.
                            dRow.Item("remark") = Language.getMessage(mstrModuleName, 21, "Grade Level does not exist.")

                            dtTable.Rows.Add(dRow)

                            Continue For
                        End If

                        If objGradeLevel.isExist(intGradeID, strGradeLevelName, intGradeGroupID, , intGradeLevelID) = False Then
                            dRow.Item("employeeunkid") = CInt(dtEmployee.Rows(0).Item("employeeunkid"))
                            dRow.Item("EmpCode") = dsRow.Item(cboEmployeeCode.Text).ToString.Trim
                            dRow.Item("EmpName") = dtEmployee.Rows(0).Item("employeename").ToString

                            dRow.Item("rowtypeid") = 13 'Grade Level is not linked with given Grade Group and Grade.
                            dRow.Item("remark") = Language.getMessage(mstrModuleName, 22, "Grade Level is not linked with given Grade Group and Grade.")

                            dtTable.Rows.Add(dRow)

                            Continue For
                        End If

                    End If
                    'Sohail (10 Nov 2014) -- End

                    If CInt(cboChangeType.SelectedValue) > 0 AndAlso CInt(cboChangeType.SelectedValue) = enSalaryChangeType.SALARY_CHANGE_WITH_PROMOTION Then
                        If IsDate(dsRow.Item(cboPromotionDate.Text).ToString.Trim) = False Then
                            dRow.Item("employeeunkid") = CInt(dtEmployee.Rows(0).Item("employeeunkid"))
                            dRow.Item("EmpCode") = dsRow.Item(cboEmployeeCode.Text).ToString.Trim
                            dRow.Item("EmpName") = dtEmployee.Rows(0).Item("employeename").ToString

                            dRow.Item("rowtypeid") = 16 'Promotion Date is not Proper.
                            dRow.Item("remark") = Language.getMessage(mstrModuleName, 34, "Promotion date is not in valid date format.")

                            dtTable.Rows.Add(dRow)

                            Continue For
                        Else

                            If objSalary.isExistOnSameDate(CDate(dsRow.Item(cboPromotionDate.Text)), CInt(dtEmployee.Rows(0).Item("employeeunkid")), , True) Then
                                dRow.Item("employeeunkid") = CInt(dtEmployee.Rows(0).Item("employeeunkid"))
                                dRow.Item("EmpCode") = dsRow.Item(cboEmployeeCode.Text).ToString.Trim
                                dRow.Item("EmpName") = dtEmployee.Rows(0).Item("employeename").ToString

                                dRow.Item("rowtypeid") = 17 'Promotion is already given for the selected date.
                                dRow.Item("remark") = Language.getMessage("clsSalaryIncrement", 3, "Sorry! Promotion for the employee on the selected date is already done.")

                                dtTable.Rows.Add(dRow)

                                Continue For
                            End If

                            objEmp._Employeeunkid(eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate)) = CInt(dtEmployee.Rows(0).Item("employeeunkid"))
                            If CDate(dsRow.Item(cboPromotionDate.Text)).Date <= objEmp._Appointeddate.Date Then
                                dRow.Item("employeeunkid") = CInt(dtEmployee.Rows(0).Item("employeeunkid"))
                                dRow.Item("EmpCode") = dsRow.Item(cboEmployeeCode.Text).ToString.Trim
                                dRow.Item("EmpName") = dtEmployee.Rows(0).Item("employeename").ToString

                                dRow.Item("rowtypeid") = 18 'Date of Promotion cannot be less than or equal to employee's Appointment Date.
                                dRow.Item("remark") = Language.getMessage(mstrModuleName, 35, "Sorry! Date of Promotion cannot be less than or equal to employee's Appointment Date.")

                                dtTable.Rows.Add(dRow)

                                Continue For
                            End If

                        End If
                    End If



                    '*******   PROPER ENTRY    **************************

                    dRow.Item("employeeunkid") = CInt(dtEmployee.Rows(0).Item("employeeunkid"))
                    dRow.Item("EmpCode") = dsRow.Item(cboEmployeeCode.Text).ToString.Trim
                    dRow.Item("EmpName") = dtEmployee.Rows(0).Item("employeename").ToString

                    If CInt(cboChangeType.SelectedValue) > 0 AndAlso CInt(cboChangeType.SelectedValue) = enSalaryChangeType.SALARY_CHANGE_WITH_PROMOTION Then
                        dRow.Item("promotion_date") = CDate(dsRow.Item(cboPromotionDate.Text))
                    End If


                    'Sohail (10 Nov 2014) -- Start
                    'Enhancement - Change Grade option on Import Salary Change.
                    'ds = objMaster.Get_Current_Scale("CurrSalary", CInt(dtEmployee.Rows(0).Item("employeeunkid")), mdtPayPeriodEndDate)
                    ds = objMaster.Get_Current_Scale("CurrSalary", CInt(dtEmployee.Rows(0).Item("employeeunkid")), dtpIncrementdate.Value.Date)
                    'Sohail (10 Nov 2014) -- End
                    If ds.Tables("CurrSalary").Rows.Count > 0 Then
                        dRow.Item("currentscale") = CDec(ds.Tables("CurrSalary").Rows(0).Item("newscale"))
                        dRow.Item("gradegroupunkid") = CInt(ds.Tables("CurrSalary").Rows(0).Item("gradegroupunkid"))
                        dRow.Item("gradeunkid") = CInt(ds.Tables("CurrSalary").Rows(0).Item("gradeunkid"))
                        dRow.Item("gradelevelunkid") = CInt(ds.Tables("CurrSalary").Rows(0).Item("gradelevelunkid"))
                    Else
                        dRow.Item("currentscale") = 0
                        dRow.Item("gradegroupunkid") = 0
                        dRow.Item("gradeunkid") = 0
                        dRow.Item("gradelevelunkid") = 0
                    End If

                    'Sohail (10 Nov 2014) -- Start
                    'Enhancement - Change Grade option on Import Salary Change.
                    dRow.Item("isgradechange") = chkChangeGrade.Checked
                    'Sohail (10 Nov 2014) -- End

                    If CBool(dRow.Item("isgradechange")) = False Then 'Sohail (10 Nov 2014)
                        If CInt(cboIncrementBy.SelectedValue) = enSalaryIncrementBy.Percentage Then
                            If IsDBNull(dsRow.Item(cboAmount.Text)) = True Then
                                dRow.Item("increment") = 0
                            Else
                                dRow.Item("increment") = CDec(dRow.Item("currentscale")) * CDec(dsRow.Item(cboAmount.Text)) / 100
                            End If
                            dRow.Item("percentage") = CDec(dsRow.Item(cboAmount.Text))
                        Else
                            If IsDBNull(dsRow.Item(cboAmount.Text)) = True Then
                                dRow.Item("increment") = 0
                            Else
                                dRow.Item("increment") = CDec(dsRow.Item(cboAmount.Text))
                            End If
                            dRow.Item("percentage") = 0
                        End If

                        dRow.Item("newscale") = CDec(dRow.Item("currentscale")) + CDec(dRow.Item("increment"))

                        'Sohail (10 Nov 2014) -- Start
                        'Enhancement - Change Grade option on Import Salary Change.
                    Else

                        If CInt(dRow.Item("gradelevelunkid")) = intGradeLevelID Then
                            dRow.Item("employeeunkid") = CInt(dtEmployee.Rows(0).Item("employeeunkid"))
                            dRow.Item("EmpCode") = dsRow.Item(cboEmployeeCode.Text).ToString.Trim
                            dRow.Item("EmpName") = dtEmployee.Rows(0).Item("employeename").ToString

                            dRow.Item("rowtypeid") = 14 'Same Grade Level is already exist.
                            dRow.Item("remark") = Language.getMessage(mstrModuleName, 23, "Same Grade Level already exist. Please give different Grade Level.")

                            dtTable.Rows.Add(dRow)

                            Continue For
                        End If

                        If IsDBNull(dsRow.Item(cboAmount.Text)) = True Then
                            dRow.Item("newscale") = 0
                        Else
                            dRow.Item("newscale") = CDec(dsRow.Item(cboAmount.Text))
                        End If
                        dRow.Item("increment") = CDec(dRow.Item("newscale")) - CDec(dRow.Item("currentscale"))
                        dRow.Item("percentage") = 0

                        dRow.Item("gradegroupunkid") = intGradeGroupID
                        dRow.Item("gradeunkid") = intGradeID
                        dRow.Item("gradelevelunkid") = intGradeLevelID
                    End If
                    'Sohail (10 Nov 2014) -- End

                    dRow.Item("increment_mode") = CInt(cboIncrementBy.SelectedValue)

                    dRow.Item("reasonid") = CInt(cboReason.SelectedValue)
                    dRow.Item("reason") = cboReason.Text

                    'Sohail (27 Apr 2016) -- Start
                    'Enhancement - 59.1 - Priority Order on Grade Group, Grade and Grade Level; Auto Salary Increment to next Grade Level as per Priority Order; Grade filter on Wages Table; Export and Import for Wages Table for KBC.
                    '#23 (KBC & Kenya Project Comments List.xls) - Auto Salary incrment Per employee Anniversary,Employee Anniversay,All KBC Employees have Annivesary date for Salary increment,date is sent  when employee is confimed ,the salary is done automatically by the system when annivesary is due from current level to next level and stops at the Last level (notch),unless changed to higher Grade
                    'ds = objWagesTran.getScaleInfo(CInt(dRow.Item("gradeunkid")), CInt(dRow.Item("gradelevelunkid")), "Scale")
                    ds = objWagesTran.getScaleInfo(CInt(dRow.Item("gradeunkid")), CInt(dRow.Item("gradelevelunkid")), mdtPayPeriodEndDate, "Scale")
                    'Sohail (27 Apr 2016) -- End
                    If ds.Tables("Scale").Rows.Count > 0 Then
                        If CDec(dRow.Item("newscale")) > CDec(ds.Tables("Scale").Rows(0).Item("maximum")) Then

                            dRow.Item("rowtypeid") = 5 'New Scale Exceeds the Maximum Scale
                            dRow.Item("remark") = Language.getMessage(mstrModuleName, 24, "New Scale Exceeds the Maximum Scale.")

                            dtTable.Rows.Add(dRow)

                            Continue For

                        End If

                        'Sohail (10 Nov 2014) -- Start
                        'Enhancement - Change Grade option on Import Salary Change.
                        If CDec(dRow.Item("newscale")) < CDec(ds.Tables("Scale").Rows(0).Item("salary")) Then

                            dRow.Item("rowtypeid") = 15 'New Scale Exceeds the Maximum Scale
                            dRow.Item("remark") = Language.getMessage(mstrModuleName, 25, "New Scale is below to the Minimum Scale.")

                            dtTable.Rows.Add(dRow)

                            Continue For

                        End If
                        'Sohail (10 Nov 2014) -- End

                    End If


                    dRow.Item("rowtypeid") = 0 'proper entry
                    dRow.Item("remark") = Language.getMessage(mstrModuleName, 26, "Success.")

                    'Sohail (21 Jan 2020) -- Start
                    'NMB Enhancement # : Actual increment date on salary change screen for informational purpose.
                    dRow.Item("actualdate") = dtpActualDate.Value
                    dRow.Item("arrears_amount") = 0
                    dRow.Item("emi_amount") = 0
                    dRow.Item("noofinstallment") = 1
                    dRow.Item("arrears_countryid") = Company._Object._Localization_Country
                    dRow.Item("approval_statusunkid") = CInt(enApprovalStatus.PENDING)
                    dRow.Item("finalapproverunkid") = -1
                    dRow.Item("arrears_statusunkid") = 0 'CInt(enLoanStatus.IN_PROGRESS)

                    'Dim objMaster As New clsMasterData
                    Dim intActualDatePeriod As Integer = objMaster.getCurrentPeriodID(enModuleReference.Payroll, dtpActualDate.Value, 0, 0, True, False, Nothing, True, True)
                    If intActualDatePeriod <> CInt(cboPeriod.SelectedValue) Then
                        Dim decDiff As Decimal = 0
                        'If chkChangeGrade.Checked = True Then
                        '    decDiff = CDec(txtNewScaleGrade.Decimal) - CDec(txtLatestScale.Decimal)
                        'Else
                        decDiff = CDec(dRow.Item("newscale")) - CDec(dRow.Item("currentscale"))
                        'End If
                        Dim intMonths As Integer = 0
                        Dim objPeriod As New clscommom_period_Tran
                        Dim dsPeriod As DataSet = objPeriod.getListForCombo(enModuleReference.Payroll, 0, FinancialYear._Object._DatabaseName, FinancialYear._Object._Database_Start_Date, "List", False, 0, False)
                        Dim dtTable As DataTable = New DataView(dsPeriod.Tables(0), "start_date >= " & eZeeDate.convertDate(dtpActualDate.Value.Date) & " AND start_date < " & eZeeDate.convertDate(mdtPayPeriodStartDate) & " ", "", DataViewRowState.CurrentRows).ToTable
                        intMonths = dtTable.Rows.Count
                        Dim decArrears As Decimal = decDiff * intMonths

                        dRow.Item("arrears_amount") = decDiff * intMonths
                        dRow.Item("emi_amount") = decDiff * intMonths
                        dRow.Item("noofinstallment") = 1
                        dRow.Item("arrears_countryid") = Company._Object._Localization_Country
                        dRow.Item("approval_statusunkid") = CInt(enApprovalStatus.PENDING)
                        dRow.Item("finalapproverunkid") = -1
                        dRow.Item("arrears_statusunkid") = 0 'CInt(enLoanStatus.IN_PROGRESS)
                    End If
                    'Sohail (21 Jan 2020) -- End

                    dtTable.Rows.Add(dRow)


                Else '***employee not found
                    dRow.Item("employeeunkid") = 0
                    dRow.Item("EmpCode") = dsRow.Item(cboEmployeeCode.Text).ToString.Trim
                    dRow.Item("EmpName") = ""

                    If IsDBNull(dsRow.Item(cboAmount.Text)) = True Then
                        dRow.Item("increment") = 0
                    Else
                        dRow.Item("increment") = CDec(dsRow.Item(cboAmount.Text))
                    End If
                    dRow.Item("rowtypeid") = 1 'employee code not found
                    dRow.Item("remark") = Language.getMessage(mstrModuleName, 27, "Employee code does not exist.")

                    dtTable.Rows.Add(dRow)
                End If
            Next

            'Sohail (10 Nov 2014) -- Start
            'Enhancement - Change Grade option on Import Salary Change.
            Dim duplicates = (From p In dtTable.AsEnumerable() Where (CInt(p.Item("rowtypeid")) = 0) Group p By emp = p.Item("employeeunkid") Into groups = Group Where groups.Count() > 1 Select (groups))

            Dim intPrevID As Integer = 0
            For Each dup In duplicates

                For Each rw In dup
                    If intPrevID = CInt(rw.Item("employeeunkid")) Then
                        rw.Item("rowtypeid") = 16 'Sorry, you can import only one salary change for each employee at a time.
                        rw.Item("remark") = Language.getMessage(mstrModuleName, 28, "Sorry, you can import only one salary change for each employee at a time.")
                        rw.AcceptChanges()
                    End If
                    intPrevID = CInt(rw.Item("employeeunkid"))
                Next
            Next
            'Sohail (10 Nov 2014) -- End

            If dtTable.Rows.Count > 0 Then
                mintPeriodUnkId = CInt(cboPeriod.SelectedValue)
                mdtIncrementDate = dtpIncrementdate.Value
                mintReasonUnkId = CInt(cboReason.SelectedValue)
                mintChangeTypeId = CInt(cboChangeType.SelectedValue)
                'Sohail (21 Jan 2020) -- Start
                'NMB Enhancement # : Actual increment date on salary change screen for informational purpose.
                mdtActualDate = dtpActualDate.Value
                'Sohail (21 Jan 2020) -- End
                mblnCancel = False
                Me.Close()
            Else
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 29, "Sorry, You cannot import this file. Reason : There is no transaction to import in this file."), enMsgBoxStyle.Exclamation)
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnOk_Click", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Check box's Events "
    Private Sub chkChangeGrade_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkChangeGrade.CheckedChanged
        Try
            If chkChangeGrade.Checked = True Then
                cboIncrementBy.SelectedValue = 0
                cboIncrementBy.Enabled = False
                cboGradeGroup.Enabled = True
                cboGrade.Enabled = True
                cboGradeLevel.Enabled = True
                lblAmount.Text = Language.getMessage(mstrModuleName, 30, "New Scale")
            Else
                cboIncrementBy.Enabled = True
                cboGradeGroup.Enabled = False
                cboGrade.Enabled = False
                cboGradeLevel.Enabled = False
                lblAmount.Text = Language.getMessage(mstrModuleName, 31, "Increment Amt/Perc.")
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "chkChangeGrade_CheckedChanged", mstrModuleName)
        End Try
    End Sub
#End Region

    'Sohail (21 Jan 2020) -- Start
    'NMB Enhancement # : Actual increment date on salary change screen for informational purpose.
#Region " Date Control Events "

    Private Sub dtpIncrementdate_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles dtpIncrementdate.ValueChanged
        Try
            dtpActualDate.Checked = dtpIncrementdate.Checked
            dtpActualDate.Value = dtpIncrementdate.Value
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dtpIncrementdate_ValueChanged", mstrModuleName)
        End Try
    End Sub

#End Region
    'Sohail (21 Jan 2020) -- End

#Region " Other Control's Events "
    Private Sub objbtnAddReason_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnAddReason.Click
        Dim frm As New frmCommonMaster
        Dim dsList As New DataSet
        Dim intGroupId As Integer = -1
        Dim objGroup As New clsCommon_Master
        Try
            frm.displayDialog(intGroupId, clsCommon_Master.enCommonMaster.SALINC_REASON, enAction.ADD_ONE)
            If intGroupId > -1 Then
                dsList = objGroup.getComboList(clsCommon_Master.enCommonMaster.SALINC_REASON, True, "Group")
                With cboReason
                    .ValueMember = "masterunkid"
                    .DisplayMember = "name"
                    .DataSource = dsList.Tables("Group")
                    .SelectedValue = intGroupId
                End With
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnAddReason_Click", mstrModuleName)
        Finally
            frm = Nothing
            dsList = Nothing
            objGroup = Nothing
        End Try
    End Sub
#End Region

#Region " S.SANDEEP [09 MAY 2016] - TRANSACTION SCREEN FOR PROMOTION "
'*************************************** S.SANDEEP [09 MAY 2016] - TRANSACTION SCREEN FOR PROMOTION ************************************|
'
'
'
'
'
'Public Class frmSalaryChangeMapping

'#Region " Private variables "
'    Private ReadOnly mstrModuleName As String = "frmSalaryChangeMapping"
'    Private dsData As New DataSet
'    Private mblnCancel As Boolean = True
'    Private dtTable As DataTable
'    Private mdtPayPeriodStartDate As Date = Nothing
'    Private mdtPayPeriodEndDate As Date = Nothing
'    Private mintPeriodUnkId As Integer = -1
'    Private mdtIncrementDate As DateTime = Nothing
'    Private mintReasonUnkId As Integer = -1
'    'Private dsDataList As New DataSet
'    'Dim objEmp As clsEmployee_Master
'    'Dim objTranHead As clsTransactionHead
'#End Region

'#Region " Properties "
'    Public ReadOnly Property _DataTable() As DataTable
'        Get
'            Return dtTable
'        End Get
'    End Property

'    Public ReadOnly Property _PeriodId() As Integer
'        Get
'            Return mintPeriodUnkId
'        End Get
'    End Property

'    Public ReadOnly Property _IncrementDate() As DateTime
'        Get
'            Return mdtIncrementDate
'        End Get
'    End Property

'    Public ReadOnly Property _ReasonId() As Integer
'        Get
'            Return mintReasonUnkId
'        End Get
'    End Property
'#End Region

'#Region " Display Dialog "

'    Public Function displayDialog(ByVal dsList As DataSet) As Boolean
'        Try
'            dsData = dsList
'            Me.ShowDialog()
'            Return Not mblnCancel
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "", mstrModuleName)
'        End Try
'    End Function

'#End Region

'#Region " Private Methods "
'    Private Sub SetColor()
'        Try
'            cboEmployeeCode.BackColor = GUI.ColorComp
'            cboAmount.BackColor = GUI.ColorComp
'            cboPeriod.BackColor = GUI.ColorComp
'            cboIncrementBy.BackColor = GUI.ColorComp
'            cboReason.BackColor = GUI.ColorComp
'            cboGrade.BackColor = GUI.ColorComp
'            cboGradeGroup.BackColor = GUI.ColorComp
'            cboGradeLevel.BackColor = GUI.ColorComp
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "SetColor", mstrModuleName)
'        End Try
'    End Sub

'    Private Sub FillCombo()
'        Dim dsCombos As New DataSet
'        Dim objPeriod As New clscommom_period_Tran
'        Dim objMaster As New clsMasterData
'        Dim objCommon As New clsCommon_Master
'        Try

'            'Sohail (21 Aug 2015) -- Start
'            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
'            'dsCombos = objPeriod.getListForCombo(enModuleReference.Payroll, FinancialYear._Object._YearUnkid, "Period", True, enStatusType.Open)
'            dsCombos = objPeriod.getListForCombo(enModuleReference.Payroll, FinancialYear._Object._YearUnkid, FinancialYear._Object._DatabaseName, FinancialYear._Object._Database_Start_Date, "Period", True, enStatusType.Open)
'            'Sohail (21 Aug 2015) -- End
'            With cboPeriod
'                .BeginUpdate()
'                .ValueMember = "periodunkid"
'                .DisplayMember = "name"
'                .DataSource = dsCombos.Tables("Period")
'                .SelectedValue = 0
'                .EndUpdate()
'            End With

'            dsCombos = objMaster.getComboListSalaryIncrementBy(True, "IncrMode")
'            With cboIncrementBy
'                .ValueMember = "Id"
'                .DisplayMember = "Name"
'                .DataSource = New DataView(dsCombos.Tables("IncrMode"), "Id <> " & enSalaryIncrementBy.Grade & " ", "", DataViewRowState.CurrentRows).ToTable
'                .SelectedValue = 0
'            End With

'            dsCombos = objCommon.getComboList(clsCommon_Master.enCommonMaster.SALINC_REASON, True, "SalReason")
'            With cboReason
'                .BeginUpdate()
'                .ValueMember = "masterunkid"
'                .DisplayMember = "Name"
'                .DataSource = dsCombos.Tables("SalReason")
'                .SelectedValue = 0
'                .EndUpdate()
'            End With
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
'        Finally
'            objPeriod = Nothing
'            objCommon = Nothing
'        End Try
'    End Sub

'    Private Sub SetData()
'        Try

'            dtTable = New DataTable("Salary")
'            For Each dtColumns As DataColumn In dsData.Tables(0).Columns
'                cboAmount.Items.Add(dtColumns.ColumnName)
'                cboEmployeeCode.Items.Add(dtColumns.ColumnName)
'                'Sohail (10 Nov 2014) -- Start
'                'Enhancement - Change Grade option on Import Salary Change.
'                cboGradeGroup.Items.Add(dtColumns.ColumnName)
'                cboGrade.Items.Add(dtColumns.ColumnName)
'                cboGradeLevel.Items.Add(dtColumns.ColumnName)
'                'Sohail (10 Nov 2014) -- End
'            Next

'            dtTable.Columns.Add("employeeunkid", System.Type.GetType("System.Int32")).DefaultValue = -1
'            dtTable.Columns.Add("EmpCode", System.Type.GetType("System.String")).DefaultValue = ""
'            dtTable.Columns.Add("EmpName", System.Type.GetType("System.String")).DefaultValue = ""
'            dtTable.Columns.Add("currentscale", System.Type.GetType("System.Decimal")).DefaultValue = 0
'            dtTable.Columns.Add("increment", System.Type.GetType("System.Decimal")).DefaultValue = 0
'            dtTable.Columns.Add("newscale", System.Type.GetType("System.Decimal")).DefaultValue = 0
'            dtTable.Columns.Add("gradegroupunkid", System.Type.GetType("System.Int32")).DefaultValue = -1
'            dtTable.Columns.Add("gradeunkid", System.Type.GetType("System.Int32")).DefaultValue = -1
'            dtTable.Columns.Add("gradelevelunkid", System.Type.GetType("System.Int32")).DefaultValue = -1
'            dtTable.Columns.Add("increment_mode", System.Type.GetType("System.Int32")).DefaultValue = 0
'            dtTable.Columns.Add("percentage", System.Type.GetType("System.Double")).DefaultValue = 0
'            dtTable.Columns.Add("reasonid", System.Type.GetType("System.Int32")).DefaultValue = -1
'            dtTable.Columns.Add("reason", System.Type.GetType("System.String")).DefaultValue = ""
'            dtTable.Columns.Add("IsChecked", System.Type.GetType("System.Boolean")).DefaultValue = False
'            dtTable.Columns.Add("rowtypeid", System.Type.GetType("System.Int32")).DefaultValue = -1
'            'Sohail (10 Nov 2014) -- Start
'            'Enhancement - Change Grade option on Import Salary Change.
'            dtTable.Columns.Add("isgradechange", System.Type.GetType("System.Boolean")).DefaultValue = False
'            dtTable.Columns.Add("remark", System.Type.GetType("System.String")).DefaultValue = ""
'            'Sohail (10 Nov 2014) -- End

'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "SetData", mstrModuleName)
'        End Try
'    End Sub

'    Private Function IsValidate() As Boolean
'        Try
'            If CInt(cboPeriod.SelectedValue) <= 0 Then
'                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please select Period. Period is mandatory information."), enMsgBoxStyle.Information)
'                cboPeriod.Focus()
'                Exit Try
'            ElseIf dtpIncrementdate.Value.Date > mdtPayPeriodEndDate Or dtpIncrementdate.Value.Date < mdtPayPeriodStartDate Then
'                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Increment Date should be in between ") & mdtPayPeriodStartDate & Language.getMessage(mstrModuleName, 4, " And ") & mdtPayPeriodEndDate & ".", enMsgBoxStyle.Information)
'                dtpIncrementdate.Focus()
'                Exit Try
'            ElseIf CInt(cboEmployeeCode.SelectedIndex) < 0 Then
'                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Please select field for Employee Code."), enMsgBoxStyle.Information)
'                cboEmployeeCode.Focus()
'                Exit Try
'                'ElseIf CInt(cboTranHeadCode.SelectedIndex) < 0 Then
'                '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Please select field for Transaction Head ID."), enMsgBoxStyle.Information)
'                '    cboTranHeadCode.Focus()
'                '    Exit Try
'            ElseIf CInt(cboAmount.SelectedIndex) < 0 Then
'                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "Please select field for Increment Amount."), enMsgBoxStyle.Information)
'                cboAmount.Focus()
'                Exit Try
'                'Sohail (10 Nov 2014) -- Start
'                'Enhancement - Change Grade option on Import Salary Change.
'            ElseIf chkChangeGrade.Checked = False AndAlso CInt(cboIncrementBy.SelectedValue) <= 0 Then
'                'ElseIf CInt(cboIncrementBy.SelectedValue) <= 0 Then
'                'Sohail (10 Nov 2014) -- End
'                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 6, "Please select Increment By. Increment By is mandatory information."), enMsgBoxStyle.Information)
'                cboIncrementBy.Focus()
'                Exit Try
'            ElseIf CInt(cboReason.SelectedValue) <= 0 Then
'                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 7, "Please select Reason. Reason is mandatory information. "), enMsgBoxStyle.Information)
'                cboReason.Focus()
'                Exit Try
'                'Sohail (10 Nov 2014) -- Start
'                'Enhancement - Change Grade option on Import Salary Change.
'            ElseIf chkChangeGrade.Checked = True Then
'                If CInt(cboGradeGroup.SelectedIndex) < 0 Then
'                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 8, "Please select field for Grade Group."), enMsgBoxStyle.Information)
'                    cboGradeGroup.Focus()
'                    Exit Try
'                ElseIf CInt(cboGrade.SelectedIndex) < 0 Then
'                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 9, "Please select field for Grade."), enMsgBoxStyle.Information)
'                    cboGrade.Focus()
'                    Exit Try
'                ElseIf CInt(cboGradeLevel.SelectedIndex) < 0 Then
'                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 10, "Please select field for Grade Level."), enMsgBoxStyle.Information)
'                    cboGradeLevel.Focus()
'                    Exit Try
'                End If
'                'Sohail (10 Nov 2014) -- End
'            End If

'            Return True
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "IsValidate", mstrModuleName)
'        End Try
'    End Function
'#End Region

'#Region " Form's Events "

'    Private Sub frmSalaryChangeMapping_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed

'    End Sub

'    Private Sub frmSalaryChangeMapping_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
'        Try
'            Call Set_Logo(Me, gApplicationType)
'            Call SetColor()

'            'Sohail (10 Jul 2014) -- Start
'            'Enhancement - Custom Language.
'            Call Language.setLanguage(Me.Name)
'            Call OtherSettings()
'            'Sohail (10 Jul 2014) -- End

'            cboAmount.Items.Clear()
'            cboEmployeeCode.Items.Clear()
'            'Sohail (10 Nov 2014) -- Start
'            'Enhancement - Change Grade option on Import Salary Change.
'            cboGradeGroup.Items.Clear()
'            cboGrade.Items.Clear()
'            cboGradeLevel.Items.Clear()
'            'Sohail (10 Nov 2014) -- End

'            Call FillCombo()
'            Call SetData()
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "", mstrModuleName)
'        End Try
'    End Sub

'    'Sohail (10 Jul 2014) -- Start
'    'Enhancement - Custom Language.
'    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
'        Dim objfrm As New frmLanguage
'        Try
'            If User._Object._Isrighttoleft = True Then
'                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
'                objfrm.RightToLeftLayout = True
'                Call Language.ctlRightToLeftlayOut(objfrm)
'            End If

'            Call SetMessages()

'            clsSalaryIncrement.SetMessages()
'            objfrm._Other_ModuleNames = "clsSalaryIncrement"
'            objfrm.displayDialog(Me)

'            Call SetLanguage()

'        Catch ex As System.Exception
'            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
'        Finally
'            objfrm.Dispose()
'            objfrm = Nothing
'        End Try
'    End Sub
'    'Sohail (10 Jul 2014) -- End

'#End Region

'#Region " Combobox's Events "
'    Private Sub cboPeriod_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboPeriod.SelectedIndexChanged
'        Dim objPeriod As New clscommom_period_Tran
'        Try
'            'Sohail (21 Aug 2015) -- Start
'            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
'            'objPeriod._Periodunkid = CInt(cboPeriod.SelectedValue)
'            objPeriod._Periodunkid(FinancialYear._Object._DatabaseName) = CInt(cboPeriod.SelectedValue)
'            'Sohail (21 Aug 2015) -- End
'            mdtPayPeriodStartDate = objPeriod._Start_Date
'            mdtPayPeriodEndDate = objPeriod._End_Date

'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "cboPeriod_SelectedIndexChanged", mstrModuleName)
'        Finally
'            objPeriod = Nothing
'        End Try
'    End Sub

'    'Sohail (10 Nov 2014) -- Start
'    'Enhancement - Change Grade option on Import Salary Change.
'    Private Sub cboEmployeeCode_Validating(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles cboEmployeeCode.Validating, _
'                                                                                                                            cboAmount.Validating, _
'                                                                                                                            cboGradeGroup.Validating, _
'                                                                                                                            cboGrade.Validating, _
'                                                                                                                            cboGradeLevel.Validating
'        Try
'            Dim cbo As ComboBox = CType(sender, ComboBox)

'            If CInt(cbo.SelectedIndex) >= 0 Then
'                Dim lst As IEnumerable(Of ComboBox) = gbFieldMapping.Controls.OfType(Of ComboBox)().Where(Function(t) t.Name <> cbo.Name AndAlso t.Name <> cboPeriod.Name AndAlso t.Name <> cboIncrementBy.Name AndAlso t.Name <> cboReason.Name AndAlso CInt(t.SelectedIndex) = CInt(cbo.SelectedIndex))
'                If lst.Count > 0 Then
'                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 11, "Sorry, This column is already mapped."))
'                    cbo.SelectedIndex = -1
'                    e.Cancel = True
'                End If
'            End If

'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "cboEmployeeCode_Validating", mstrModuleName)
'        End Try
'    End Sub
'    'Sohail (10 Nov 2014) -- End

'#End Region

'#Region " Buttons Events "
'    Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click
'        Try
'            Me.Close()
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "btnCancel_Click", mstrModuleName)
'        End Try
'    End Sub

'    Private Sub btnOk_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnOk.Click

'        Try
'            If IsValidate() = False Then Exit Sub

'            Dim dtData As DataTable = New DataView(dsData.Tables(0), " [" & cboEmployeeCode.Text & "] IS NOT NULL ", "", DataViewRowState.CurrentRows).ToTable

'            Dim objEmp As New clsEmployee_Master
'            Dim objSalary As New clsSalaryIncrement
'            Dim objMaster As New clsMasterData
'            Dim objWagesTran As New clsWagesTran
'            Dim dsEmployee As DataSet
'            Dim dtEmployee As DataTable
'            Dim ds As DataSet

'            'Sohail (23 Nov 2012) -- Start
'            'TRA - ENHANCEMENT
'            'dsEmployee = objEmp.GetEmployeeList("Employee", False, True)
'            'dsEmployee = objEmp.GetEmployeeList("Employee", False, True, , , , , , , , , , , , mdtPayPeriodStartDate, mdtPayPeriodEndDate, , , , , , True)
'            'Sohail (23 Nov 2012) -- End

'            'Anjan [10 June 2015] -- Start
'            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
'            'dsEmployee = objEmp.GetEmployeeList("Employee", False, True, , , , , , , , , , , , mdtPayPeriodStartDate, mdtPayPeriodEndDate, , , , , , True)

'            dsEmployee = objEmp.GetEmployeeList(FinancialYear._Object._DatabaseName, _
'                                           User._Object._Userunkid, _
'                                           FinancialYear._Object._YearUnkid, _
'                                           Company._Object._Companyunkid, _
'                                           mdtPayPeriodStartDate, _
'                                           mdtPayPeriodEndDate, _
'                                           ConfigParameter._Object._UserAccessModeSetting, _
'                                           True, False, "Employee", False)
'            'Anjan [10 June 2015] -- End

'            For Each dsRow As DataRow In dtData.Rows

'                Dim dRow As DataRow

'                dtEmployee = New DataView(dsEmployee.Tables("Employee"), "EmployeeCode = '" & CStr(dsRow.Item(cboEmployeeCode.Text).ToString.Trim) & "' ", "", DataViewRowState.CurrentRows).ToTable

'                dRow = dtTable.NewRow
'                If dtEmployee.Rows.Count > 0 Then

'                    'Sohail (07 Sep 2013) -- Start
'                    'TRA - ENHANCEMENT
'                    'ds = objSalary.getLastIncrement("Incr", CInt(dtEmployee.Rows(0).Item("employeeunkid")))
'                    ds = objSalary.getLastIncrement("Incr", CInt(dtEmployee.Rows(0).Item("employeeunkid")), True)
'                    'Sohail (07 Sep 2013) -- End

'                    If ds.Tables("Incr").Rows.Count > 0 Then
'                        If eZeeDate.convertDate(dtpIncrementdate.Value.Date) <= ds.Tables("Incr").Rows(0).Item("incrementdate").ToString Then
'                            dRow.Item("employeeunkid") = CInt(dtEmployee.Rows(0).Item("employeeunkid"))
'                            dRow.Item("EmpCode") = dsRow.Item(cboEmployeeCode.Text).ToString.Trim
'                            dRow.Item("EmpName") = dtEmployee.Rows(0).Item("employeename").ToString

'                            ds = objMaster.Get_Current_Scale("CurrSalary", CInt(dtEmployee.Rows(0).Item("employeeunkid")), mdtPayPeriodEndDate)
'                            If ds.Tables("CurrSalary").Rows.Count > 0 Then
'                                dRow.Item("currentscale") = CDec(ds.Tables("CurrSalary").Rows(0).Item("newscale"))
'                                dRow.Item("gradegroupunkid") = CInt(ds.Tables("CurrSalary").Rows(0).Item("gradegroupunkid"))
'                                dRow.Item("gradeunkid") = CInt(ds.Tables("CurrSalary").Rows(0).Item("gradeunkid"))
'                                dRow.Item("gradelevelunkid") = CInt(ds.Tables("CurrSalary").Rows(0).Item("gradelevelunkid"))
'                            Else
'                                dRow.Item("currentscale") = 0
'                                dRow.Item("gradegroupunkid") = 0
'                                dRow.Item("gradeunkid") = 0
'                                dRow.Item("gradelevelunkid") = 0
'                            End If

'                            If CInt(cboIncrementBy.SelectedValue) = enSalaryIncrementBy.Percentage Then
'                                If IsDBNull(dsRow.Item(cboAmount.Text)) = True Then
'                                    dRow.Item("increment") = 0
'                                Else
'                                    dRow.Item("increment") = CDec(dRow.Item("currentscale")) * CDec(dsRow.Item(cboAmount.Text)) / 100
'                                End If
'                                dRow.Item("percentage") = CDec(dsRow.Item(cboAmount.Text))
'                            Else
'                                If IsDBNull(dsRow.Item(cboAmount.Text)) = True Then
'                                    dRow.Item("increment") = 0
'                                Else
'                                    dRow.Item("increment") = CDec(dsRow.Item(cboAmount.Text))
'                                End If
'                                dRow.Item("percentage") = 0
'                            End If

'                            dRow.Item("newscale") = CDec(dRow.Item("currentscale")) + CDec(dRow.Item("increment"))


'                            dRow.Item("increment_mode") = CInt(cboIncrementBy.SelectedValue)
'                            dRow.Item("percentage") = 0

'                            dRow.Item("rowtypeid") = 2 'Increment date should be greater than last increment date.
'                            dRow.Item("remark") = Language.getMessage(mstrModuleName, 12, "Increment date should be greater than last increment date.")

'                            dtTable.Rows.Add(dRow)

'                            Continue For

'                            'Sohail (07 Sep 2013) -- Start
'                            'TRA - ENHANCEMENT
'                        ElseIf CBool(ds.Tables("Incr").Rows(0).Item("isapproved")) = False Then

'                            dRow.Item("employeeunkid") = CInt(dtEmployee.Rows(0).Item("employeeunkid"))
'                            dRow.Item("EmpCode") = dsRow.Item(cboEmployeeCode.Text).ToString.Trim
'                            dRow.Item("EmpName") = dtEmployee.Rows(0).Item("employeename").ToString

'                            ds = objMaster.Get_Current_Scale("CurrSalary", CInt(dtEmployee.Rows(0).Item("employeeunkid")), mdtPayPeriodEndDate)
'                            If ds.Tables("CurrSalary").Rows.Count > 0 Then
'                                dRow.Item("currentscale") = CDec(ds.Tables("CurrSalary").Rows(0).Item("newscale"))
'                                dRow.Item("gradegroupunkid") = CInt(ds.Tables("CurrSalary").Rows(0).Item("gradegroupunkid"))
'                                dRow.Item("gradeunkid") = CInt(ds.Tables("CurrSalary").Rows(0).Item("gradeunkid"))
'                                dRow.Item("gradelevelunkid") = CInt(ds.Tables("CurrSalary").Rows(0).Item("gradelevelunkid"))
'                            Else
'                                dRow.Item("currentscale") = 0
'                                dRow.Item("gradegroupunkid") = 0
'                                dRow.Item("gradeunkid") = 0
'                                dRow.Item("gradelevelunkid") = 0
'                            End If

'                            If CInt(cboIncrementBy.SelectedValue) = enSalaryIncrementBy.Percentage Then
'                                If IsDBNull(dsRow.Item(cboAmount.Text)) = True Then
'                                    dRow.Item("increment") = 0
'                                Else
'                                    dRow.Item("increment") = CDec(dRow.Item("currentscale")) * CDec(dsRow.Item(cboAmount.Text)) / 100
'                                End If
'                                dRow.Item("percentage") = CDec(dsRow.Item(cboAmount.Text))
'                            Else
'                                If IsDBNull(dsRow.Item(cboAmount.Text)) = True Then
'                                    dRow.Item("increment") = 0
'                                Else
'                                    dRow.Item("increment") = CDec(dsRow.Item(cboAmount.Text))
'                                End If
'                                dRow.Item("percentage") = 0
'                            End If

'                            dRow.Item("newscale") = CDec(dRow.Item("currentscale")) + CDec(dRow.Item("increment"))


'                            dRow.Item("increment_mode") = CInt(cboIncrementBy.SelectedValue)
'                            dRow.Item("percentage") = 0

'                            dRow.Item("rowtypeid") = 6 'Last Increment Not Approved.
'                            dRow.Item("remark") = Language.getMessage(mstrModuleName, 13, "Last Salary Change is not Approved.")

'                            dtTable.Rows.Add(dRow)

'                            Continue For

'                            'Sohail (07 Sep 2013) -- End

'                            'Sohail (29 Dec 2012) -- Start
'                            'TRA - ENHANCEMENT - Code Commented to allow to give increment more than one time in one period (Issue# 131)
'                            'ElseIf objSalary.isExist(CInt(cboPeriod.SelectedValue), CInt(dtEmployee.Rows(0).Item("employeeunkid"))) = True Then
'                        ElseIf objSalary.isExistOnSameDate(dtpIncrementdate.Value.Date, CInt(dtEmployee.Rows(0).Item("employeeunkid"))) = True Then
'                            'Sohail (29 Dec 2012) -- End
'                            dRow.Item("employeeunkid") = CInt(dtEmployee.Rows(0).Item("employeeunkid"))
'                            dRow.Item("EmpCode") = dsRow.Item(cboEmployeeCode.Text).ToString.Trim
'                            dRow.Item("EmpName") = dtEmployee.Rows(0).Item("employeename").ToString

'                            ds = objMaster.Get_Current_Scale("CurrSalary", CInt(dtEmployee.Rows(0).Item("employeeunkid")), mdtPayPeriodEndDate)
'                            If ds.Tables("CurrSalary").Rows.Count > 0 Then
'                                dRow.Item("currentscale") = CDec(ds.Tables("CurrSalary").Rows(0).Item("newscale"))
'                                dRow.Item("gradegroupunkid") = CInt(ds.Tables("CurrSalary").Rows(0).Item("gradegroupunkid"))
'                                dRow.Item("gradeunkid") = CInt(ds.Tables("CurrSalary").Rows(0).Item("gradeunkid"))
'                                dRow.Item("gradelevelunkid") = CInt(ds.Tables("CurrSalary").Rows(0).Item("gradelevelunkid"))
'                            Else
'                                dRow.Item("currentscale") = 0
'                                dRow.Item("gradegroupunkid") = 0
'                                dRow.Item("gradeunkid") = 0
'                                dRow.Item("gradelevelunkid") = 0
'                            End If

'                            If IsDBNull(dsRow.Item(cboAmount.Text)) = True Then
'                                dRow.Item("increment") = 0
'                            Else
'                                'dRow.Item("increment") = CDec(dsRow.Item(cboAmount.Text))
'                                dRow.Item("increment") = 0
'                            End If

'                            dRow.Item("newscale") = CDec(dRow.Item("currentscale")) + CDec(dRow.Item("increment"))


'                            dRow.Item("increment_mode") = CInt(cboIncrementBy.SelectedValue)
'                            dRow.Item("percentage") = 0

'                            dRow.Item("rowtypeid") = 3 'Increment is already done in this period.
'                            dRow.Item("remark") = Language.getMessage(mstrModuleName, 14, "Increment is already done on selected Increment Date.")

'                            dtTable.Rows.Add(dRow)

'                            Continue For

'                        End If
'                    End If

'                    If IsDBNull(dsRow.Item(cboAmount.Text)) = True OrElse CDec(dsRow.Item(cboAmount.Text)) = 0 Then

'                        dRow.Item("employeeunkid") = CInt(dtEmployee.Rows(0).Item("employeeunkid"))
'                        dRow.Item("EmpCode") = dsRow.Item(cboEmployeeCode.Text).ToString.Trim
'                        dRow.Item("EmpName") = dtEmployee.Rows(0).Item("employeename").ToString

'                        dRow.Item("rowtypeid") = 4 'Increment amount is not given.
'                        dRow.Item("remark") = Language.getMessage(mstrModuleName, 15, "Increment amount / Percentage / New Scale is not given.")

'                        dtTable.Rows.Add(dRow)

'                        Continue For

'                    End If

'                    'Sohail (10 Nov 2014) -- Start
'                    'Enhancement - Change Grade option on Import Salary Change.
'                    Dim intGradeGroupID As Integer = 0
'                    Dim intGradeID As Integer = 0
'                    Dim intGradeLevelID As Integer = 0
'                    Dim strGradeLevelName As String = ""
'                    If chkChangeGrade.Checked = True Then
'                        'If IsDBNull(dsRow.Item(cboGradeGroup.Text)) = True OrElse IsDBNull(dsRow.Item(cboGrade.Text)) = True OrElse IsDBNull(dsRow.Item(cboGradeLevel.Text)) = True Then

'                        'End If
'                        If IsDBNull(dsRow.Item(cboGradeGroup.Text)) = True Then
'                            dRow.Item("employeeunkid") = CInt(dtEmployee.Rows(0).Item("employeeunkid"))
'                            dRow.Item("EmpCode") = dsRow.Item(cboEmployeeCode.Text).ToString.Trim
'                            dRow.Item("EmpName") = dtEmployee.Rows(0).Item("employeename").ToString

'                            dRow.Item("rowtypeid") = 7 'Grade Group is not given.
'                            dRow.Item("remark") = Language.getMessage(mstrModuleName, 16, "Grade Group is not given.")

'                            dtTable.Rows.Add(dRow)

'                            Continue For

'                        ElseIf IsDBNull(dsRow.Item(cboGrade.Text)) = True Then
'                            dRow.Item("employeeunkid") = CInt(dtEmployee.Rows(0).Item("employeeunkid"))
'                            dRow.Item("EmpCode") = dsRow.Item(cboEmployeeCode.Text).ToString.Trim
'                            dRow.Item("EmpName") = dtEmployee.Rows(0).Item("employeename").ToString

'                            dRow.Item("rowtypeid") = 8 'Grade is not given.
'                            dRow.Item("remark") = Language.getMessage(mstrModuleName, 17, "Grade is not given.")

'                            dtTable.Rows.Add(dRow)

'                            Continue For

'                        ElseIf IsDBNull(dsRow.Item(cboGradeLevel.Text)) = True Then
'                            dRow.Item("employeeunkid") = CInt(dtEmployee.Rows(0).Item("employeeunkid"))
'                            dRow.Item("EmpCode") = dsRow.Item(cboEmployeeCode.Text).ToString.Trim
'                            dRow.Item("EmpName") = dtEmployee.Rows(0).Item("employeename").ToString

'                            dRow.Item("rowtypeid") = 9 'Grade Level is not given.
'                            dRow.Item("remark") = Language.getMessage(mstrModuleName, 18, "Grade Level is not given.")

'                            dtTable.Rows.Add(dRow)

'                            Continue For

'                        End If


'                        Dim objGradeGroup As New clsGradeGroup
'                        If objGradeGroup.isExist(dsRow.Item(cboGradeGroup.Text).ToString, , , intGradeGroupID) = False Then
'                            dRow.Item("employeeunkid") = CInt(dtEmployee.Rows(0).Item("employeeunkid"))
'                            dRow.Item("EmpCode") = dsRow.Item(cboEmployeeCode.Text).ToString.Trim
'                            dRow.Item("EmpName") = dtEmployee.Rows(0).Item("employeename").ToString

'                            dRow.Item("rowtypeid") = 10 'Grade Group does not exist.
'                            dRow.Item("remark") = Language.getMessage(mstrModuleName, 19, "Grade Group does not exist.")

'                            dtTable.Rows.Add(dRow)

'                            Continue For
'                        End If

'                        Dim objGrade As New clsGrade
'                        If objGrade.isExist(dsRow.Item(cboGrade.Text).ToString, , , intGradeID) = False Then
'                            dRow.Item("employeeunkid") = CInt(dtEmployee.Rows(0).Item("employeeunkid"))
'                            dRow.Item("EmpCode") = dsRow.Item(cboEmployeeCode.Text).ToString.Trim
'                            dRow.Item("EmpName") = dtEmployee.Rows(0).Item("employeename").ToString

'                            dRow.Item("rowtypeid") = 11 'Grade does not exist.
'                            dRow.Item("remark") = Language.getMessage(mstrModuleName, 20, "Grade does not exist.")

'                            dtTable.Rows.Add(dRow)

'                            Continue For
'                        End If

'                        Dim objGradeLevel As New clsGradeLevel
'                        If objGradeLevel.isCodeExits(dsRow.Item(cboGradeLevel.Text).ToString, , strGradeLevelName) = False Then
'                            dRow.Item("employeeunkid") = CInt(dtEmployee.Rows(0).Item("employeeunkid"))
'                            dRow.Item("EmpCode") = dsRow.Item(cboEmployeeCode.Text).ToString.Trim
'                            dRow.Item("EmpName") = dtEmployee.Rows(0).Item("employeename").ToString

'                            dRow.Item("rowtypeid") = 12 'Grade Level does not exist.
'                            dRow.Item("remark") = Language.getMessage(mstrModuleName, 21, "Grade Level does not exist.")

'                            dtTable.Rows.Add(dRow)

'                            Continue For
'                        End If

'                        If objGradeLevel.isExist(intGradeID, strGradeLevelName, intGradeGroupID, , intGradeLevelID) = False Then
'                            dRow.Item("employeeunkid") = CInt(dtEmployee.Rows(0).Item("employeeunkid"))
'                            dRow.Item("EmpCode") = dsRow.Item(cboEmployeeCode.Text).ToString.Trim
'                            dRow.Item("EmpName") = dtEmployee.Rows(0).Item("employeename").ToString

'                            dRow.Item("rowtypeid") = 13 'Grade Level is not linked with given Grade Group and Grade.
'                            dRow.Item("remark") = Language.getMessage(mstrModuleName, 22, "Grade Level is not linked with given Grade Group and Grade.")

'                            dtTable.Rows.Add(dRow)

'                            Continue For
'                        End If

'                    End If
'                    'Sohail (10 Nov 2014) -- End




'                    '*******   PROPER ENTRY    **************************

'                    dRow.Item("employeeunkid") = CInt(dtEmployee.Rows(0).Item("employeeunkid"))
'                    dRow.Item("EmpCode") = dsRow.Item(cboEmployeeCode.Text).ToString.Trim
'                    dRow.Item("EmpName") = dtEmployee.Rows(0).Item("employeename").ToString

'                    'Sohail (10 Nov 2014) -- Start
'                    'Enhancement - Change Grade option on Import Salary Change.
'                    'ds = objMaster.Get_Current_Scale("CurrSalary", CInt(dtEmployee.Rows(0).Item("employeeunkid")), mdtPayPeriodEndDate)
'                    ds = objMaster.Get_Current_Scale("CurrSalary", CInt(dtEmployee.Rows(0).Item("employeeunkid")), dtpIncrementdate.Value.Date)
'                    'Sohail (10 Nov 2014) -- End
'                    If ds.Tables("CurrSalary").Rows.Count > 0 Then
'                        dRow.Item("currentscale") = CDec(ds.Tables("CurrSalary").Rows(0).Item("newscale"))
'                        dRow.Item("gradegroupunkid") = CInt(ds.Tables("CurrSalary").Rows(0).Item("gradegroupunkid"))
'                        dRow.Item("gradeunkid") = CInt(ds.Tables("CurrSalary").Rows(0).Item("gradeunkid"))
'                        dRow.Item("gradelevelunkid") = CInt(ds.Tables("CurrSalary").Rows(0).Item("gradelevelunkid"))
'                    Else
'                        dRow.Item("currentscale") = 0
'                        dRow.Item("gradegroupunkid") = 0
'                        dRow.Item("gradeunkid") = 0
'                        dRow.Item("gradelevelunkid") = 0
'                    End If

'                    'Sohail (10 Nov 2014) -- Start
'                    'Enhancement - Change Grade option on Import Salary Change.
'                    dRow.Item("isgradechange") = chkChangeGrade.Checked
'                    'Sohail (10 Nov 2014) -- End

'                    If CBool(dRow.Item("isgradechange")) = False Then 'Sohail (10 Nov 2014)
'                        If CInt(cboIncrementBy.SelectedValue) = enSalaryIncrementBy.Percentage Then
'                            If IsDBNull(dsRow.Item(cboAmount.Text)) = True Then
'                                dRow.Item("increment") = 0
'                            Else
'                                dRow.Item("increment") = CDec(dRow.Item("currentscale")) * CDec(dsRow.Item(cboAmount.Text)) / 100
'                            End If
'                            dRow.Item("percentage") = CDec(dsRow.Item(cboAmount.Text))
'                        Else
'                            If IsDBNull(dsRow.Item(cboAmount.Text)) = True Then
'                                dRow.Item("increment") = 0
'                            Else
'                                dRow.Item("increment") = CDec(dsRow.Item(cboAmount.Text))
'                            End If
'                            dRow.Item("percentage") = 0
'                        End If

'                        dRow.Item("newscale") = CDec(dRow.Item("currentscale")) + CDec(dRow.Item("increment"))

'                        'Sohail (10 Nov 2014) -- Start
'                        'Enhancement - Change Grade option on Import Salary Change.
'                    Else

'                        If CInt(dRow.Item("gradelevelunkid")) = intGradeLevelID Then
'                            dRow.Item("employeeunkid") = CInt(dtEmployee.Rows(0).Item("employeeunkid"))
'                            dRow.Item("EmpCode") = dsRow.Item(cboEmployeeCode.Text).ToString.Trim
'                            dRow.Item("EmpName") = dtEmployee.Rows(0).Item("employeename").ToString

'                            dRow.Item("rowtypeid") = 14 'Same Grade Level is already exist.
'                            dRow.Item("remark") = Language.getMessage(mstrModuleName, 23, "Same Grade Level already exist. Please give different Grade Level.")

'                            dtTable.Rows.Add(dRow)

'                            Continue For
'                        End If

'                        If IsDBNull(dsRow.Item(cboAmount.Text)) = True Then
'                            dRow.Item("newscale") = 0
'                        Else
'                            dRow.Item("newscale") = CDec(dsRow.Item(cboAmount.Text))
'                        End If
'                        dRow.Item("increment") = CDec(dRow.Item("newscale")) - CDec(dRow.Item("currentscale"))
'                        dRow.Item("percentage") = 0

'                        dRow.Item("gradegroupunkid") = intGradeGroupID
'                        dRow.Item("gradeunkid") = intGradeID
'                        dRow.Item("gradelevelunkid") = intGradeLevelID
'                    End If
'                    'Sohail (10 Nov 2014) -- End

'                    dRow.Item("increment_mode") = CInt(cboIncrementBy.SelectedValue)

'                    dRow.Item("reasonid") = CInt(cboReason.SelectedValue)
'                    dRow.Item("reason") = cboReason.Text


'                    ds = objWagesTran.getScaleInfo(CInt(dRow.Item("gradeunkid")), CInt(dRow.Item("gradelevelunkid")), "Scale")
'                    If ds.Tables("Scale").Rows.Count > 0 Then
'                        If CDec(dRow.Item("newscale")) > CDec(ds.Tables("Scale").Rows(0).Item("maximum")) Then

'                            dRow.Item("rowtypeid") = 5 'New Scale Exceeds the Maximum Scale
'                            dRow.Item("remark") = Language.getMessage(mstrModuleName, 24, "New Scale Exceeds the Maximum Scale.")

'                            dtTable.Rows.Add(dRow)

'                            Continue For

'                        End If

'                        'Sohail (10 Nov 2014) -- Start
'                        'Enhancement - Change Grade option on Import Salary Change.
'                        If CDec(dRow.Item("newscale")) < CDec(ds.Tables("Scale").Rows(0).Item("salary")) Then

'                            dRow.Item("rowtypeid") = 15 'New Scale Exceeds the Maximum Scale
'                            dRow.Item("remark") = Language.getMessage(mstrModuleName, 25, "New Scale is below to the Minimum Scale.")

'                            dtTable.Rows.Add(dRow)

'                            Continue For

'                        End If
'                        'Sohail (10 Nov 2014) -- End

'                    End If


'                    dRow.Item("rowtypeid") = 0 'proper entry
'                    dRow.Item("remark") = Language.getMessage(mstrModuleName, 26, "Success.")

'                    dtTable.Rows.Add(dRow)


'                Else '***employee not found
'                    dRow.Item("employeeunkid") = 0
'                    dRow.Item("EmpCode") = dsRow.Item(cboEmployeeCode.Text).ToString.Trim
'                    dRow.Item("EmpName") = ""

'                    If IsDBNull(dsRow.Item(cboAmount.Text)) = True Then
'                        dRow.Item("increment") = 0
'                    Else
'                        dRow.Item("increment") = CDec(dsRow.Item(cboAmount.Text))
'                    End If
'                    dRow.Item("rowtypeid") = 1 'employee code not found
'                    dRow.Item("remark") = Language.getMessage(mstrModuleName, 27, "Employee code does not exist.")

'                    dtTable.Rows.Add(dRow)
'                End If
'            Next

'            'Sohail (10 Nov 2014) -- Start
'            'Enhancement - Change Grade option on Import Salary Change.
'            Dim duplicates = (From p In dtTable.AsEnumerable() Where (CInt(p.Item("rowtypeid")) = 0) Group p By emp = p.Item("employeeunkid") Into groups = Group Where groups.Count() > 1 Select (groups))

'            Dim intPrevID As Integer = 0
'            For Each dup In duplicates

'                For Each rw In dup
'                    If intPrevID = CInt(rw.Item("employeeunkid")) Then
'                        rw.Item("rowtypeid") = 16 'Sorry, you can import only one salary change for each employee at a time.
'                        rw.Item("remark") = Language.getMessage(mstrModuleName, 28, "Sorry, you can import only one salary change for each employee at a time.")
'                        rw.AcceptChanges()
'                    End If
'                    intPrevID = CInt(rw.Item("employeeunkid"))
'                Next
'            Next
'            'Sohail (10 Nov 2014) -- End

'            If dtTable.Rows.Count > 0 Then
'                mintPeriodUnkId = CInt(cboPeriod.SelectedValue)
'                mdtIncrementDate = dtpIncrementdate.Value
'                mintReasonUnkId = CInt(cboReason.SelectedValue)
'                mblnCancel = False
'                Me.Close()
'            Else
'                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 29, "Sorry, You cannot import this file. Reason : There is no transaction to import in this file."), enMsgBoxStyle.Exclamation)
'            End If

'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "btnOk_Click", mstrModuleName)
'        End Try
'    End Sub
'#End Region


'    'Sohail (10 Nov 2014) -- Start
'    'Enhancement - Change Grade option on Import Salary Change.
'#Region " Check box's Events "
'    Private Sub chkChangeGrade_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkChangeGrade.CheckedChanged
'        Try
'            If chkChangeGrade.Checked = True Then
'                cboIncrementBy.SelectedValue = 0
'                cboIncrementBy.Enabled = False
'                cboGradeGroup.Enabled = True
'                cboGrade.Enabled = True
'                cboGradeLevel.Enabled = True
'                lblAmount.Text = Language.getMessage(mstrModuleName, 30, "New Scale")
'            Else
'                cboIncrementBy.Enabled = True
'                cboGradeGroup.Enabled = False
'                cboGrade.Enabled = False
'                cboGradeLevel.Enabled = False
'                lblAmount.Text = Language.getMessage(mstrModuleName, 31, "Increment Amt/Perc.")
'            End If
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "chkChangeGrade_CheckedChanged", mstrModuleName)
'        End Try
'    End Sub
'#End Region
'    'Sohail (10 Nov 2014) -- End

'#Region " Other Control's Events "
'    Private Sub objbtnAddReason_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnAddReason.Click
'        Dim frm As New frmCommonMaster
'        Dim dsList As New DataSet
'        Dim intGroupId As Integer = -1
'        Dim objGroup As New clsCommon_Master
'        Try
'            frm.displayDialog(intGroupId, clsCommon_Master.enCommonMaster.SALINC_REASON, enAction.ADD_ONE)
'            If intGroupId > -1 Then
'                dsList = objGroup.getComboList(clsCommon_Master.enCommonMaster.SALINC_REASON, True, "Group")
'                With cboReason
'                    .ValueMember = "masterunkid"
'                    .DisplayMember = "name"
'                    .DataSource = dsList.Tables("Group")
'                    .SelectedValue = intGroupId
'                End With
'            End If
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "objbtnAddReason_Click", mstrModuleName)
'        Finally
'            frm = Nothing
'            dsList = Nothing
'            objGroup = Nothing
'        End Try
'    End Sub
'#End Region
#End Region



	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Private Sub OtherSettings()
		Try
			Me.SuspendLayout()
			
			Call SetLanguage()
			
			Me.gbFieldMapping.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbFieldMapping.ForeColor = GUI._eZeeContainerHeaderForeColor 


			Me.btnCancel.GradientBackColor = GUI._ButttonBackColor 
			Me.btnCancel.GradientForeColor = GUI._ButttonFontColor

			Me.btnOk.GradientBackColor = GUI._ButttonBackColor 
			Me.btnOk.GradientForeColor = GUI._ButttonFontColor


			Me.ResumeLayout()
		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "OtherSettings", mstrModuleName)
		End Try
	End Sub


	Private Sub SetLanguage()
		Try
			Me.Text = Language._Object.getCaption(Me.Name, Me.Text)

			Me.gbFieldMapping.Text = Language._Object.getCaption(Me.gbFieldMapping.Name, Me.gbFieldMapping.Text)
			Me.lblPeriod.Text = Language._Object.getCaption(Me.lblPeriod.Name, Me.lblPeriod.Text)
			Me.lblAmount.Text = Language._Object.getCaption(Me.lblAmount.Name, Me.lblAmount.Text)
			Me.lblEmployeecode.Text = Language._Object.getCaption(Me.lblEmployeecode.Name, Me.lblEmployeecode.Text)
			Me.btnCancel.Text = Language._Object.getCaption(Me.btnCancel.Name, Me.btnCancel.Text)
			Me.btnOk.Text = Language._Object.getCaption(Me.btnOk.Name, Me.btnOk.Text)
			Me.lblIncrementDate.Text = Language._Object.getCaption(Me.lblIncrementDate.Name, Me.lblIncrementDate.Text)
			Me.lblReason.Text = Language._Object.getCaption(Me.lblReason.Name, Me.lblReason.Text)
			Me.lblIncrementBy.Text = Language._Object.getCaption(Me.lblIncrementBy.Name, Me.lblIncrementBy.Text)
			Me.chkChangeGrade.Text = Language._Object.getCaption(Me.chkChangeGrade.Name, Me.chkChangeGrade.Text)
			Me.lblGradeGroup.Text = Language._Object.getCaption(Me.lblGradeGroup.Name, Me.lblGradeGroup.Text)
			Me.lblGradeLevel.Text = Language._Object.getCaption(Me.lblGradeLevel.Name, Me.lblGradeLevel.Text)
			Me.lblGrade.Text = Language._Object.getCaption(Me.lblGrade.Name, Me.lblGrade.Text)
			Me.lblChangeType.Text = Language._Object.getCaption(Me.lblChangeType.Name, Me.lblChangeType.Text)
			Me.lblPromotionDate.Text = Language._Object.getCaption(Me.lblPromotionDate.Name, Me.lblPromotionDate.Text)
			Me.lblActualDate.Text = Language._Object.getCaption(Me.lblActualDate.Name, Me.lblActualDate.Text)

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetLanguage", mstrModuleName)
		End Try
	End Sub


	Private Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "Please select Period. Period is mandatory information.")
			Language.setMessage(mstrModuleName, 2, "Increment Date should be in between")
			Language.setMessage(mstrModuleName, 3, "Please select field for Employee Code.")
			Language.setMessage(mstrModuleName, 4, " And")
			Language.setMessage(mstrModuleName, 5, "Please select field for Increment Amount.")
			Language.setMessage(mstrModuleName, 6, "Please select Increment By. Increment By is mandatory information.")
			Language.setMessage(mstrModuleName, 7, "Please select Reason. Reason is mandatory information.")
			Language.setMessage(mstrModuleName, 8, "Please select field for Grade Group.")
			Language.setMessage(mstrModuleName, 9, "Please select field for Grade.")
			Language.setMessage(mstrModuleName, 10, "Please select field for Grade Level.")
			Language.setMessage(mstrModuleName, 11, "Sorry, This column is already mapped.")
			Language.setMessage(mstrModuleName, 12, "Increment date should be greater than last increment date.")
			Language.setMessage(mstrModuleName, 13, "Last Salary Change is not Approved.")
			Language.setMessage(mstrModuleName, 14, "Increment is already done on selected Increment Date.")
			Language.setMessage(mstrModuleName, 15, "Increment amount / Percentage / New Scale is not given.")
			Language.setMessage(mstrModuleName, 16, "Grade Group is not given.")
			Language.setMessage(mstrModuleName, 17, "Grade is not given.")
			Language.setMessage(mstrModuleName, 18, "Grade Level is not given.")
			Language.setMessage(mstrModuleName, 19, "Grade Group does not exist.")
			Language.setMessage(mstrModuleName, 20, "Grade does not exist.")
			Language.setMessage(mstrModuleName, 21, "Grade Level does not exist.")
			Language.setMessage(mstrModuleName, 22, "Grade Level is not linked with given Grade Group and Grade.")
			Language.setMessage(mstrModuleName, 23, "Same Grade Level already exist. Please give different Grade Level.")
			Language.setMessage(mstrModuleName, 24, "New Scale Exceeds the Maximum Scale.")
			Language.setMessage(mstrModuleName, 25, "New Scale is below to the Minimum Scale.")
			Language.setMessage(mstrModuleName, 26, "Success.")
			Language.setMessage(mstrModuleName, 27, "Employee code does not exist.")
			Language.setMessage(mstrModuleName, 28, "Sorry, you can import only one salary change for each employee at a time.")
			Language.setMessage(mstrModuleName, 29, "Sorry, You cannot import this file. Reason : There is no transaction to import in this file.")
			Language.setMessage(mstrModuleName, 30, "New Scale")
			Language.setMessage(mstrModuleName, 31, "Increment Amt/Perc.")
			Language.setMessage(mstrModuleName, 32, "Please select change type. Change type is mandatory information.")
			Language.setMessage(mstrModuleName, 33, "Please select field for promotion date.")
			Language.setMessage(mstrModuleName, 34, "Promotion date is not in valid date format.")
			Language.setMessage(mstrModuleName, 35, "Sorry! Date of Promotion cannot be less than or equal to employee's Appointment Date.")
			Language.setMessage(mstrModuleName, 36, "Sorry, Increment date is mandatory information. Please provide Increment date to continue.")
			Language.setMessage(mstrModuleName, 37, "Increment date is not set as selected period start date, So the salary will be prorated as per increment date.")
			Language.setMessage(mstrModuleName, 38, "Are you sure you want to continue?")
			Language.setMessage(mstrModuleName, 39, "Sorry, Actual date is mandatory information. Please provide Actual date to continue.")
			Language.setMessage(mstrModuleName, 40, "Actual Date should not greater than selected period end date.")
			Language.setMessage(mstrModuleName, 41, "Actual Date should not be greater than Increment Date.")
			Language.setMessage(mstrModuleName, 42, "Actual date is set in past period, Which may cause salary arrears.")
			Language.setMessage(mstrModuleName, 43, "Actual date should be greater than last increment date.")
			Language.setMessage(mstrModuleName, 44, "Increment is already done on selected Actual Date.")
			Language.setMessage("clsSalaryIncrement", 3, "Sorry! Promotion for the employee on the selected date is already done.")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class





