﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmEarningDeductionList
    Inherits eZee.Common.eZeeForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim DataGridViewCellStyle9 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle10 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmEarningDeductionList))
        Me.pnlMainInfo = New System.Windows.Forms.Panel
        Me.gbEDList = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.lblPeriod = New System.Windows.Forms.Label
        Me.cboPeriod = New System.Windows.Forms.ComboBox
        Me.chkIncludeInactiveEmployee = New System.Windows.Forms.CheckBox
        Me.pnlEdList = New System.Windows.Forms.Panel
        Me.objchkSelectAll = New System.Windows.Forms.CheckBox
        Me.dgvED = New System.Windows.Forms.DataGridView
        Me.objdgcolhCheck = New System.Windows.Forms.DataGridViewCheckBoxColumn
        Me.objdgColhBlank = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhEmployee = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhEmpCode = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhTrnHeadCode = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhTrnHead = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhTranHeadType = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhTypeOf = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhCalcType = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhPeriod = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhAmount = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhApprovePending = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhIsGroup = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhEDUnkid = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhEmpid = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhTranheadUnkId = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhPeriodUnkid = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhTranHeadTypeId = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhTypeOfId = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhCalcTypeId = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhMembership = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhMemTranUnkid = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objFooter = New eZee.Common.eZeeFooter
        Me.btnApprove = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnDisApprove = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnOperations = New eZee.Common.eZeeSplitButton
        Me.mnuOperations = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.mnuBatchTransaction = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuGlobalAssign = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuExemption = New System.Windows.Forms.ToolStripMenuItem
        Me.objSep1 = New System.Windows.Forms.ToolStripSeparator
        Me.mnuExportED = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuApprovedEDHeads = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuPendingEDHeads = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuAllEDHeads = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuImportED = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuImporUpdatetFlatRateTran = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuUpdatetT24ACBLoanData = New System.Windows.Forms.ToolStripMenuItem
        Me.ToolStripMenuItem1 = New System.Windows.Forms.ToolStripSeparator
        Me.mnuAssignMembershipHeads = New System.Windows.Forms.ToolStripMenuItem
        Me.btnDelete = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnEdit = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnNew = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnClose = New eZee.Common.eZeeLightButton(Me.components)
        Me.gbFilterCriteria = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.lblCalcType = New System.Windows.Forms.Label
        Me.cboCalcType = New System.Windows.Forms.ComboBox
        Me.lnkAllocation = New System.Windows.Forms.LinkLabel
        Me.objbtnSearchTranHead = New eZee.Common.eZeeGradientButton
        Me.objTranHeadType = New System.Windows.Forms.ComboBox
        Me.lblTranHead = New System.Windows.Forms.Label
        Me.cboTranHead = New System.Windows.Forms.ComboBox
        Me.lblApprovalStatus = New System.Windows.Forms.Label
        Me.objTypeOf = New System.Windows.Forms.ComboBox
        Me.objCalcType = New System.Windows.Forms.ComboBox
        Me.cboEDApprovalStatus = New System.Windows.Forms.ComboBox
        Me.objbtnSearchEmployee = New eZee.Common.eZeeGradientButton
        Me.cboEmployee = New System.Windows.Forms.ComboBox
        Me.lblEmployee = New System.Windows.Forms.Label
        Me.objbtnReset = New eZee.Common.eZeeSearchResetButton(Me.components)
        Me.objbtnSearch = New eZee.Common.eZeeSearchResetButton(Me.components)
        Me.eZeeHeader = New eZee.Common.eZeeHeader
        Me.dlgSaveFile = New System.Windows.Forms.SaveFileDialog
        Me.lblTotalAmt = New System.Windows.Forms.Label
        Me.objlblTotalAmt = New System.Windows.Forms.Label
        Me.pnlMainInfo.SuspendLayout()
        Me.gbEDList.SuspendLayout()
        Me.pnlEdList.SuspendLayout()
        CType(Me.dgvED, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.objFooter.SuspendLayout()
        Me.mnuOperations.SuspendLayout()
        Me.gbFilterCriteria.SuspendLayout()
        CType(Me.objbtnReset, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.objbtnSearch, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'pnlMainInfo
        '
        Me.pnlMainInfo.Controls.Add(Me.gbEDList)
        Me.pnlMainInfo.Controls.Add(Me.objFooter)
        Me.pnlMainInfo.Controls.Add(Me.gbFilterCriteria)
        Me.pnlMainInfo.Controls.Add(Me.eZeeHeader)
        Me.pnlMainInfo.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pnlMainInfo.Location = New System.Drawing.Point(0, 0)
        Me.pnlMainInfo.Name = "pnlMainInfo"
        Me.pnlMainInfo.Size = New System.Drawing.Size(954, 571)
        Me.pnlMainInfo.TabIndex = 0
        '
        'gbEDList
        '
        Me.gbEDList.BorderColor = System.Drawing.Color.Black
        Me.gbEDList.Checked = False
        Me.gbEDList.CollapseAllExceptThis = False
        Me.gbEDList.CollapsedHoverImage = Nothing
        Me.gbEDList.CollapsedNormalImage = Nothing
        Me.gbEDList.CollapsedPressedImage = Nothing
        Me.gbEDList.CollapseOnLoad = False
        Me.gbEDList.Controls.Add(Me.objlblTotalAmt)
        Me.gbEDList.Controls.Add(Me.lblTotalAmt)
        Me.gbEDList.Controls.Add(Me.lblPeriod)
        Me.gbEDList.Controls.Add(Me.cboPeriod)
        Me.gbEDList.Controls.Add(Me.chkIncludeInactiveEmployee)
        Me.gbEDList.Controls.Add(Me.pnlEdList)
        Me.gbEDList.ExpandedHoverImage = Nothing
        Me.gbEDList.ExpandedNormalImage = Nothing
        Me.gbEDList.ExpandedPressedImage = Nothing
        Me.gbEDList.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbEDList.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbEDList.HeaderHeight = 25
        Me.gbEDList.HeaderMessage = ""
        Me.gbEDList.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbEDList.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbEDList.HeightOnCollapse = 0
        Me.gbEDList.LeftTextSpace = 0
        Me.gbEDList.Location = New System.Drawing.Point(12, 138)
        Me.gbEDList.Name = "gbEDList"
        Me.gbEDList.OpenHeight = 300
        Me.gbEDList.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbEDList.ShowBorder = True
        Me.gbEDList.ShowCheckBox = False
        Me.gbEDList.ShowCollapseButton = False
        Me.gbEDList.ShowDefaultBorderColor = True
        Me.gbEDList.ShowDownButton = False
        Me.gbEDList.ShowHeader = True
        Me.gbEDList.Size = New System.Drawing.Size(930, 372)
        Me.gbEDList.TabIndex = 9
        Me.gbEDList.Temp = 0
        Me.gbEDList.Text = "Earning Deduction List"
        Me.gbEDList.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblPeriod
        '
        Me.lblPeriod.BackColor = System.Drawing.Color.Transparent
        Me.lblPeriod.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPeriod.Location = New System.Drawing.Point(711, 6)
        Me.lblPeriod.Name = "lblPeriod"
        Me.lblPeriod.Size = New System.Drawing.Size(76, 14)
        Me.lblPeriod.TabIndex = 219
        Me.lblPeriod.Text = "As On Period"
        Me.lblPeriod.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboPeriod
        '
        Me.cboPeriod.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboPeriod.DropDownWidth = 215
        Me.cboPeriod.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboPeriod.FormattingEnabled = True
        Me.cboPeriod.Location = New System.Drawing.Point(795, 4)
        Me.cboPeriod.Name = "cboPeriod"
        Me.cboPeriod.Size = New System.Drawing.Size(121, 21)
        Me.cboPeriod.TabIndex = 218
        '
        'chkIncludeInactiveEmployee
        '
        Me.chkIncludeInactiveEmployee.AutoSize = True
        Me.chkIncludeInactiveEmployee.BackColor = System.Drawing.Color.Transparent
        Me.chkIncludeInactiveEmployee.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkIncludeInactiveEmployee.Location = New System.Drawing.Point(160, 6)
        Me.chkIncludeInactiveEmployee.Name = "chkIncludeInactiveEmployee"
        Me.chkIncludeInactiveEmployee.Size = New System.Drawing.Size(157, 17)
        Me.chkIncludeInactiveEmployee.TabIndex = 75
        Me.chkIncludeInactiveEmployee.Text = "Include Inactive Employees"
        Me.chkIncludeInactiveEmployee.UseVisualStyleBackColor = False
        '
        'pnlEdList
        '
        Me.pnlEdList.Controls.Add(Me.objchkSelectAll)
        Me.pnlEdList.Controls.Add(Me.dgvED)
        Me.pnlEdList.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.pnlEdList.Location = New System.Drawing.Point(0, 27)
        Me.pnlEdList.Name = "pnlEdList"
        Me.pnlEdList.Size = New System.Drawing.Size(930, 344)
        Me.pnlEdList.TabIndex = 8
        '
        'objchkSelectAll
        '
        Me.objchkSelectAll.AutoSize = True
        Me.objchkSelectAll.Location = New System.Drawing.Point(8, 6)
        Me.objchkSelectAll.Name = "objchkSelectAll"
        Me.objchkSelectAll.Size = New System.Drawing.Size(15, 14)
        Me.objchkSelectAll.TabIndex = 18
        Me.objchkSelectAll.UseVisualStyleBackColor = True
        '
        'dgvED
        '
        Me.dgvED.AllowUserToAddRows = False
        Me.dgvED.AllowUserToDeleteRows = False
        Me.dgvED.AllowUserToResizeRows = False
        Me.dgvED.BackgroundColor = System.Drawing.Color.White
        Me.dgvED.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.dgvED.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.objdgcolhCheck, Me.objdgColhBlank, Me.dgcolhEmployee, Me.objdgcolhEmpCode, Me.dgcolhTrnHeadCode, Me.dgcolhTrnHead, Me.dgcolhTranHeadType, Me.dgcolhTypeOf, Me.dgcolhCalcType, Me.dgcolhPeriod, Me.dgcolhAmount, Me.dgcolhApprovePending, Me.objdgcolhIsGroup, Me.objdgcolhEDUnkid, Me.objdgcolhEmpid, Me.objdgcolhTranheadUnkId, Me.objdgcolhPeriodUnkid, Me.objdgcolhTranHeadTypeId, Me.objdgcolhTypeOfId, Me.objdgcolhCalcTypeId, Me.objdgcolhMembership, Me.objdgcolhMemTranUnkid})
        Me.dgvED.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dgvED.Location = New System.Drawing.Point(0, 0)
        Me.dgvED.Name = "dgvED"
        Me.dgvED.RowHeadersVisible = False
        Me.dgvED.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvED.Size = New System.Drawing.Size(930, 344)
        Me.dgvED.TabIndex = 124
        '
        'objdgcolhCheck
        '
        Me.objdgcolhCheck.HeaderText = ""
        Me.objdgcolhCheck.Name = "objdgcolhCheck"
        Me.objdgcolhCheck.Width = 30
        '
        'objdgColhBlank
        '
        Me.objdgColhBlank.HeaderText = ""
        Me.objdgColhBlank.Name = "objdgColhBlank"
        Me.objdgColhBlank.ReadOnly = True
        Me.objdgColhBlank.Width = 30
        '
        'dgcolhEmployee
        '
        Me.dgcolhEmployee.HeaderText = "Employee"
        Me.dgcolhEmployee.Name = "dgcolhEmployee"
        Me.dgcolhEmployee.ReadOnly = True
        Me.dgcolhEmployee.Width = 5
        '
        'objdgcolhEmpCode
        '
        Me.objdgcolhEmpCode.HeaderText = "objdgcolhEmpCode"
        Me.objdgcolhEmpCode.Name = "objdgcolhEmpCode"
        Me.objdgcolhEmpCode.ReadOnly = True
        Me.objdgcolhEmpCode.Width = 5
        '
        'dgcolhTrnHeadCode
        '
        Me.dgcolhTrnHeadCode.HeaderText = "Trans. Code"
        Me.dgcolhTrnHeadCode.Name = "dgcolhTrnHeadCode"
        Me.dgcolhTrnHeadCode.ReadOnly = True
        Me.dgcolhTrnHeadCode.Width = 75
        '
        'dgcolhTrnHead
        '
        Me.dgcolhTrnHead.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.dgcolhTrnHead.HeaderText = "Transaction Head"
        Me.dgcolhTrnHead.Name = "dgcolhTrnHead"
        Me.dgcolhTrnHead.ReadOnly = True
        '
        'dgcolhTranHeadType
        '
        Me.dgcolhTranHeadType.HeaderText = "Transaction Head Type"
        Me.dgcolhTranHeadType.Name = "dgcolhTranHeadType"
        Me.dgcolhTranHeadType.ReadOnly = True
        Me.dgcolhTranHeadType.Width = 130
        '
        'dgcolhTypeOf
        '
        Me.dgcolhTypeOf.HeaderText = "Type Of"
        Me.dgcolhTypeOf.Name = "dgcolhTypeOf"
        Me.dgcolhTypeOf.ReadOnly = True
        Me.dgcolhTypeOf.Width = 80
        '
        'dgcolhCalcType
        '
        Me.dgcolhCalcType.HeaderText = "Calculation Type"
        Me.dgcolhCalcType.Name = "dgcolhCalcType"
        Me.dgcolhCalcType.ReadOnly = True
        Me.dgcolhCalcType.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.dgcolhCalcType.Width = 110
        '
        'dgcolhPeriod
        '
        Me.dgcolhPeriod.HeaderText = "Period"
        Me.dgcolhPeriod.Name = "dgcolhPeriod"
        Me.dgcolhPeriod.ReadOnly = True
        Me.dgcolhPeriod.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.dgcolhPeriod.Width = 110
        '
        'dgcolhAmount
        '
        DataGridViewCellStyle9.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle9.Format = "N2"
        DataGridViewCellStyle9.NullValue = Nothing
        Me.dgcolhAmount.DefaultCellStyle = DataGridViewCellStyle9
        Me.dgcolhAmount.HeaderText = "Amount"
        Me.dgcolhAmount.Name = "dgcolhAmount"
        Me.dgcolhAmount.ReadOnly = True
        Me.dgcolhAmount.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.dgcolhAmount.Width = 140
        '
        'dgcolhApprovePending
        '
        DataGridViewCellStyle10.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        Me.dgcolhApprovePending.DefaultCellStyle = DataGridViewCellStyle10
        Me.dgcolhApprovePending.HeaderText = "Status"
        Me.dgcolhApprovePending.Name = "dgcolhApprovePending"
        Me.dgcolhApprovePending.ReadOnly = True
        Me.dgcolhApprovePending.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgcolhApprovePending.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.dgcolhApprovePending.Width = 50
        '
        'objdgcolhIsGroup
        '
        Me.objdgcolhIsGroup.HeaderText = "IsGroup"
        Me.objdgcolhIsGroup.Name = "objdgcolhIsGroup"
        Me.objdgcolhIsGroup.ReadOnly = True
        Me.objdgcolhIsGroup.Visible = False
        '
        'objdgcolhEDUnkid
        '
        Me.objdgcolhEDUnkid.HeaderText = "objdgcolhEDUnkid"
        Me.objdgcolhEDUnkid.Name = "objdgcolhEDUnkid"
        Me.objdgcolhEDUnkid.ReadOnly = True
        Me.objdgcolhEDUnkid.Visible = False
        '
        'objdgcolhEmpid
        '
        Me.objdgcolhEmpid.HeaderText = "objdgcolhEmpid"
        Me.objdgcolhEmpid.Name = "objdgcolhEmpid"
        Me.objdgcolhEmpid.ReadOnly = True
        Me.objdgcolhEmpid.Visible = False
        '
        'objdgcolhTranheadUnkId
        '
        Me.objdgcolhTranheadUnkId.HeaderText = "objdgcolhTranheadUnkId"
        Me.objdgcolhTranheadUnkId.Name = "objdgcolhTranheadUnkId"
        Me.objdgcolhTranheadUnkId.ReadOnly = True
        Me.objdgcolhTranheadUnkId.Visible = False
        '
        'objdgcolhPeriodUnkid
        '
        Me.objdgcolhPeriodUnkid.HeaderText = "objdgcolhPeriodUnkid"
        Me.objdgcolhPeriodUnkid.Name = "objdgcolhPeriodUnkid"
        Me.objdgcolhPeriodUnkid.ReadOnly = True
        Me.objdgcolhPeriodUnkid.Visible = False
        '
        'objdgcolhTranHeadTypeId
        '
        Me.objdgcolhTranHeadTypeId.HeaderText = "objdgcolhTranHeadTypeId"
        Me.objdgcolhTranHeadTypeId.Name = "objdgcolhTranHeadTypeId"
        Me.objdgcolhTranHeadTypeId.ReadOnly = True
        Me.objdgcolhTranHeadTypeId.Visible = False
        '
        'objdgcolhTypeOfId
        '
        Me.objdgcolhTypeOfId.HeaderText = "objdgcolhTypeOfId"
        Me.objdgcolhTypeOfId.Name = "objdgcolhTypeOfId"
        Me.objdgcolhTypeOfId.ReadOnly = True
        Me.objdgcolhTypeOfId.Visible = False
        '
        'objdgcolhCalcTypeId
        '
        Me.objdgcolhCalcTypeId.HeaderText = "objdgcolhCalcTypeId"
        Me.objdgcolhCalcTypeId.Name = "objdgcolhCalcTypeId"
        Me.objdgcolhCalcTypeId.ReadOnly = True
        Me.objdgcolhCalcTypeId.Visible = False
        '
        'objdgcolhMembership
        '
        Me.objdgcolhMembership.HeaderText = "objdgcolhMembership"
        Me.objdgcolhMembership.Name = "objdgcolhMembership"
        Me.objdgcolhMembership.ReadOnly = True
        Me.objdgcolhMembership.Visible = False
        '
        'objdgcolhMemTranUnkid
        '
        Me.objdgcolhMemTranUnkid.HeaderText = "objdgcolhMemTranUnkid"
        Me.objdgcolhMemTranUnkid.Name = "objdgcolhMemTranUnkid"
        Me.objdgcolhMemTranUnkid.ReadOnly = True
        Me.objdgcolhMemTranUnkid.Visible = False
        '
        'objFooter
        '
        Me.objFooter.BorderColor = System.Drawing.Color.Silver
        Me.objFooter.Controls.Add(Me.btnApprove)
        Me.objFooter.Controls.Add(Me.btnDisApprove)
        Me.objFooter.Controls.Add(Me.btnOperations)
        Me.objFooter.Controls.Add(Me.btnDelete)
        Me.objFooter.Controls.Add(Me.btnEdit)
        Me.objFooter.Controls.Add(Me.btnNew)
        Me.objFooter.Controls.Add(Me.btnClose)
        Me.objFooter.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.objFooter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objFooter.GradiantStyle = eZee.Common.eZeeFooter.GradientStyle.Up
        Me.objFooter.GradientColor1 = System.Drawing.SystemColors.Control
        Me.objFooter.GradientColor2 = System.Drawing.SystemColors.Control
        Me.objFooter.Location = New System.Drawing.Point(0, 516)
        Me.objFooter.Name = "objFooter"
        Me.objFooter.Size = New System.Drawing.Size(954, 55)
        Me.objFooter.TabIndex = 1
        '
        'btnApprove
        '
        Me.btnApprove.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnApprove.BackColor = System.Drawing.Color.White
        Me.btnApprove.BackgroundImage = CType(resources.GetObject("btnApprove.BackgroundImage"), System.Drawing.Image)
        Me.btnApprove.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnApprove.BorderColor = System.Drawing.Color.Empty
        Me.btnApprove.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnApprove.FlatAppearance.BorderSize = 0
        Me.btnApprove.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnApprove.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnApprove.ForeColor = System.Drawing.Color.Black
        Me.btnApprove.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnApprove.GradientForeColor = System.Drawing.Color.Black
        Me.btnApprove.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnApprove.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnApprove.Location = New System.Drawing.Point(126, 13)
        Me.btnApprove.Name = "btnApprove"
        Me.btnApprove.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnApprove.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnApprove.Size = New System.Drawing.Size(97, 30)
        Me.btnApprove.TabIndex = 9
        Me.btnApprove.Text = "&Approve"
        Me.btnApprove.UseVisualStyleBackColor = True
        Me.btnApprove.Visible = False
        '
        'btnDisApprove
        '
        Me.btnDisApprove.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnDisApprove.BackColor = System.Drawing.Color.White
        Me.btnDisApprove.BackgroundImage = CType(resources.GetObject("btnDisApprove.BackgroundImage"), System.Drawing.Image)
        Me.btnDisApprove.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnDisApprove.BorderColor = System.Drawing.Color.Empty
        Me.btnDisApprove.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnDisApprove.FlatAppearance.BorderSize = 0
        Me.btnDisApprove.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnDisApprove.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnDisApprove.ForeColor = System.Drawing.Color.Black
        Me.btnDisApprove.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnDisApprove.GradientForeColor = System.Drawing.Color.Black
        Me.btnDisApprove.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnDisApprove.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnDisApprove.Location = New System.Drawing.Point(229, 13)
        Me.btnDisApprove.Name = "btnDisApprove"
        Me.btnDisApprove.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnDisApprove.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnDisApprove.Size = New System.Drawing.Size(97, 30)
        Me.btnDisApprove.TabIndex = 8
        Me.btnDisApprove.Text = "&Reject"
        Me.btnDisApprove.UseVisualStyleBackColor = True
        Me.btnDisApprove.Visible = False
        '
        'btnOperations
        '
        Me.btnOperations.BorderColor = System.Drawing.Color.Black
        Me.btnOperations.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnOperations.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnOperations.GradientForeColor = System.Drawing.SystemColors.WindowText
        Me.btnOperations.Location = New System.Drawing.Point(12, 13)
        Me.btnOperations.Name = "btnOperations"
        Me.btnOperations.ShowDefaultBorderColor = True
        Me.btnOperations.Size = New System.Drawing.Size(108, 30)
        Me.btnOperations.SplitButtonMenu = Me.mnuOperations
        Me.btnOperations.TabIndex = 7
        Me.btnOperations.Text = "Operations"
        '
        'mnuOperations
        '
        Me.mnuOperations.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mnuBatchTransaction, Me.mnuGlobalAssign, Me.mnuExemption, Me.objSep1, Me.mnuExportED, Me.mnuImportED, Me.mnuImporUpdatetFlatRateTran, Me.mnuUpdatetT24ACBLoanData, Me.ToolStripMenuItem1, Me.mnuAssignMembershipHeads})
        Me.mnuOperations.Name = "mnuOperations"
        Me.mnuOperations.Size = New System.Drawing.Size(216, 192)
        '
        'mnuBatchTransaction
        '
        Me.mnuBatchTransaction.Name = "mnuBatchTransaction"
        Me.mnuBatchTransaction.Size = New System.Drawing.Size(215, 22)
        Me.mnuBatchTransaction.Text = "&Add Batch Transaction"
        '
        'mnuGlobalAssign
        '
        Me.mnuGlobalAssign.Name = "mnuGlobalAssign"
        Me.mnuGlobalAssign.Size = New System.Drawing.Size(215, 22)
        Me.mnuGlobalAssign.Text = "&Global Assign"
        '
        'mnuExemption
        '
        Me.mnuExemption.Name = "mnuExemption"
        Me.mnuExemption.Size = New System.Drawing.Size(215, 22)
        Me.mnuExemption.Text = "Employee Exemption"
        '
        'objSep1
        '
        Me.objSep1.Name = "objSep1"
        Me.objSep1.Size = New System.Drawing.Size(212, 6)
        '
        'mnuExportED
        '
        Me.mnuExportED.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mnuApprovedEDHeads, Me.mnuPendingEDHeads, Me.mnuAllEDHeads})
        Me.mnuExportED.Name = "mnuExportED"
        Me.mnuExportED.Size = New System.Drawing.Size(215, 22)
        Me.mnuExportED.Text = "E&xport Earning Deduction"
        '
        'mnuApprovedEDHeads
        '
        Me.mnuApprovedEDHeads.Name = "mnuApprovedEDHeads"
        Me.mnuApprovedEDHeads.Size = New System.Drawing.Size(126, 22)
        Me.mnuApprovedEDHeads.Text = "Approved"
        '
        'mnuPendingEDHeads
        '
        Me.mnuPendingEDHeads.Name = "mnuPendingEDHeads"
        Me.mnuPendingEDHeads.Size = New System.Drawing.Size(126, 22)
        Me.mnuPendingEDHeads.Text = "Pending"
        '
        'mnuAllEDHeads
        '
        Me.mnuAllEDHeads.Name = "mnuAllEDHeads"
        Me.mnuAllEDHeads.Size = New System.Drawing.Size(126, 22)
        Me.mnuAllEDHeads.Text = "All"
        '
        'mnuImportED
        '
        Me.mnuImportED.Name = "mnuImportED"
        Me.mnuImportED.Size = New System.Drawing.Size(215, 22)
        Me.mnuImportED.Text = "&Import Earning Deduction"
        '
        'mnuImporUpdatetFlatRateTran
        '
        Me.mnuImporUpdatetFlatRateTran.Name = "mnuImporUpdatetFlatRateTran"
        Me.mnuImporUpdatetFlatRateTran.Size = New System.Drawing.Size(215, 22)
        Me.mnuImporUpdatetFlatRateTran.Text = "Update Flat Rate  Heads"
        '
        'mnuUpdatetT24ACBLoanData
        '
        Me.mnuUpdatetT24ACBLoanData.Name = "mnuUpdatetT24ACBLoanData"
        Me.mnuUpdatetT24ACBLoanData.Size = New System.Drawing.Size(215, 22)
        Me.mnuUpdatetT24ACBLoanData.Text = "Update T24 ACB Loan Data"
        '
        'ToolStripMenuItem1
        '
        Me.ToolStripMenuItem1.Name = "ToolStripMenuItem1"
        Me.ToolStripMenuItem1.Size = New System.Drawing.Size(212, 6)
        '
        'mnuAssignMembershipHeads
        '
        Me.mnuAssignMembershipHeads.Name = "mnuAssignMembershipHeads"
        Me.mnuAssignMembershipHeads.Size = New System.Drawing.Size(215, 22)
        Me.mnuAssignMembershipHeads.Text = "Assign &Membership Heads"
        '
        'btnDelete
        '
        Me.btnDelete.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnDelete.BackColor = System.Drawing.Color.White
        Me.btnDelete.BackgroundImage = CType(resources.GetObject("btnDelete.BackgroundImage"), System.Drawing.Image)
        Me.btnDelete.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnDelete.BorderColor = System.Drawing.Color.Empty
        Me.btnDelete.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnDelete.FlatAppearance.BorderSize = 0
        Me.btnDelete.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnDelete.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnDelete.ForeColor = System.Drawing.Color.Black
        Me.btnDelete.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnDelete.GradientForeColor = System.Drawing.Color.Black
        Me.btnDelete.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnDelete.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnDelete.Location = New System.Drawing.Point(742, 13)
        Me.btnDelete.Name = "btnDelete"
        Me.btnDelete.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnDelete.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnDelete.Size = New System.Drawing.Size(97, 30)
        Me.btnDelete.TabIndex = 2
        Me.btnDelete.Text = "&Delete"
        Me.btnDelete.UseVisualStyleBackColor = True
        '
        'btnEdit
        '
        Me.btnEdit.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnEdit.BackColor = System.Drawing.Color.White
        Me.btnEdit.BackgroundImage = CType(resources.GetObject("btnEdit.BackgroundImage"), System.Drawing.Image)
        Me.btnEdit.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnEdit.BorderColor = System.Drawing.Color.Empty
        Me.btnEdit.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnEdit.FlatAppearance.BorderSize = 0
        Me.btnEdit.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnEdit.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnEdit.ForeColor = System.Drawing.Color.Black
        Me.btnEdit.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnEdit.GradientForeColor = System.Drawing.Color.Black
        Me.btnEdit.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnEdit.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnEdit.Location = New System.Drawing.Point(639, 13)
        Me.btnEdit.Name = "btnEdit"
        Me.btnEdit.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnEdit.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnEdit.Size = New System.Drawing.Size(97, 30)
        Me.btnEdit.TabIndex = 1
        Me.btnEdit.Text = "&Edit"
        Me.btnEdit.UseVisualStyleBackColor = True
        '
        'btnNew
        '
        Me.btnNew.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnNew.BackColor = System.Drawing.Color.White
        Me.btnNew.BackgroundImage = CType(resources.GetObject("btnNew.BackgroundImage"), System.Drawing.Image)
        Me.btnNew.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnNew.BorderColor = System.Drawing.Color.Empty
        Me.btnNew.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnNew.FlatAppearance.BorderSize = 0
        Me.btnNew.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnNew.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnNew.ForeColor = System.Drawing.Color.Black
        Me.btnNew.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnNew.GradientForeColor = System.Drawing.Color.Black
        Me.btnNew.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnNew.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnNew.Location = New System.Drawing.Point(536, 13)
        Me.btnNew.Name = "btnNew"
        Me.btnNew.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnNew.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnNew.Size = New System.Drawing.Size(97, 30)
        Me.btnNew.TabIndex = 0
        Me.btnNew.Text = "&New"
        Me.btnNew.UseVisualStyleBackColor = True
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnClose.BackColor = System.Drawing.Color.White
        Me.btnClose.BackgroundImage = CType(resources.GetObject("btnClose.BackgroundImage"), System.Drawing.Image)
        Me.btnClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnClose.BorderColor = System.Drawing.Color.Empty
        Me.btnClose.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnClose.FlatAppearance.BorderSize = 0
        Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClose.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.ForeColor = System.Drawing.Color.Black
        Me.btnClose.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnClose.GradientForeColor = System.Drawing.Color.Black
        Me.btnClose.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Location = New System.Drawing.Point(845, 13)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Size = New System.Drawing.Size(97, 30)
        Me.btnClose.TabIndex = 3
        Me.btnClose.Text = "&Close"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'gbFilterCriteria
        '
        Me.gbFilterCriteria.BorderColor = System.Drawing.Color.Black
        Me.gbFilterCriteria.Checked = False
        Me.gbFilterCriteria.CollapseAllExceptThis = False
        Me.gbFilterCriteria.CollapsedHoverImage = Nothing
        Me.gbFilterCriteria.CollapsedNormalImage = Nothing
        Me.gbFilterCriteria.CollapsedPressedImage = Nothing
        Me.gbFilterCriteria.CollapseOnLoad = False
        Me.gbFilterCriteria.Controls.Add(Me.lblCalcType)
        Me.gbFilterCriteria.Controls.Add(Me.cboCalcType)
        Me.gbFilterCriteria.Controls.Add(Me.lnkAllocation)
        Me.gbFilterCriteria.Controls.Add(Me.objbtnSearchTranHead)
        Me.gbFilterCriteria.Controls.Add(Me.objTranHeadType)
        Me.gbFilterCriteria.Controls.Add(Me.lblTranHead)
        Me.gbFilterCriteria.Controls.Add(Me.cboTranHead)
        Me.gbFilterCriteria.Controls.Add(Me.lblApprovalStatus)
        Me.gbFilterCriteria.Controls.Add(Me.objTypeOf)
        Me.gbFilterCriteria.Controls.Add(Me.objCalcType)
        Me.gbFilterCriteria.Controls.Add(Me.cboEDApprovalStatus)
        Me.gbFilterCriteria.Controls.Add(Me.objbtnSearchEmployee)
        Me.gbFilterCriteria.Controls.Add(Me.cboEmployee)
        Me.gbFilterCriteria.Controls.Add(Me.lblEmployee)
        Me.gbFilterCriteria.Controls.Add(Me.objbtnReset)
        Me.gbFilterCriteria.Controls.Add(Me.objbtnSearch)
        Me.gbFilterCriteria.ExpandedHoverImage = Nothing
        Me.gbFilterCriteria.ExpandedNormalImage = Nothing
        Me.gbFilterCriteria.ExpandedPressedImage = Nothing
        Me.gbFilterCriteria.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbFilterCriteria.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbFilterCriteria.HeaderHeight = 25
        Me.gbFilterCriteria.HeaderMessage = ""
        Me.gbFilterCriteria.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbFilterCriteria.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbFilterCriteria.HeightOnCollapse = 0
        Me.gbFilterCriteria.LeftTextSpace = 0
        Me.gbFilterCriteria.Location = New System.Drawing.Point(12, 66)
        Me.gbFilterCriteria.Name = "gbFilterCriteria"
        Me.gbFilterCriteria.OpenHeight = 300
        Me.gbFilterCriteria.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbFilterCriteria.ShowBorder = True
        Me.gbFilterCriteria.ShowCheckBox = False
        Me.gbFilterCriteria.ShowCollapseButton = False
        Me.gbFilterCriteria.ShowDefaultBorderColor = True
        Me.gbFilterCriteria.ShowDownButton = False
        Me.gbFilterCriteria.ShowHeader = True
        Me.gbFilterCriteria.Size = New System.Drawing.Size(930, 66)
        Me.gbFilterCriteria.TabIndex = 0
        Me.gbFilterCriteria.Temp = 0
        Me.gbFilterCriteria.Text = "Filter Criteria"
        Me.gbFilterCriteria.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblCalcType
        '
        Me.lblCalcType.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCalcType.Location = New System.Drawing.Point(227, 35)
        Me.lblCalcType.Name = "lblCalcType"
        Me.lblCalcType.Size = New System.Drawing.Size(100, 17)
        Me.lblCalcType.TabIndex = 310
        Me.lblCalcType.Text = "Calc. Type"
        Me.lblCalcType.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboCalcType
        '
        Me.cboCalcType.DropDownWidth = 300
        Me.cboCalcType.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboCalcType.FormattingEnabled = True
        Me.cboCalcType.Location = New System.Drawing.Point(328, 33)
        Me.cboCalcType.Name = "cboCalcType"
        Me.cboCalcType.Size = New System.Drawing.Size(134, 21)
        Me.cboCalcType.TabIndex = 309
        '
        'lnkAllocation
        '
        Me.lnkAllocation.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lnkAllocation.BackColor = System.Drawing.Color.Transparent
        Me.lnkAllocation.Location = New System.Drawing.Point(791, 6)
        Me.lnkAllocation.Name = "lnkAllocation"
        Me.lnkAllocation.Size = New System.Drawing.Size(81, 13)
        Me.lnkAllocation.TabIndex = 307
        Me.lnkAllocation.TabStop = True
        Me.lnkAllocation.Text = "Allocations"
        '
        'objbtnSearchTranHead
        '
        Me.objbtnSearchTranHead.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchTranHead.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchTranHead.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchTranHead.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchTranHead.BorderSelected = False
        Me.objbtnSearchTranHead.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchTranHead.Image = CType(resources.GetObject("objbtnSearchTranHead.Image"), System.Drawing.Image)
        Me.objbtnSearchTranHead.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchTranHead.Location = New System.Drawing.Point(650, 34)
        Me.objbtnSearchTranHead.Name = "objbtnSearchTranHead"
        Me.objbtnSearchTranHead.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchTranHead.TabIndex = 305
        Me.objbtnSearchTranHead.Visible = False
        '
        'objTranHeadType
        '
        Me.objTranHeadType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.objTranHeadType.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objTranHeadType.FormattingEnabled = True
        Me.objTranHeadType.Location = New System.Drawing.Point(609, 34)
        Me.objTranHeadType.Name = "objTranHeadType"
        Me.objTranHeadType.Size = New System.Drawing.Size(43, 21)
        Me.objTranHeadType.TabIndex = 294
        Me.objTranHeadType.Visible = False
        '
        'lblTranHead
        '
        Me.lblTranHead.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTranHead.Location = New System.Drawing.Point(485, 35)
        Me.lblTranHead.Name = "lblTranHead"
        Me.lblTranHead.Size = New System.Drawing.Size(100, 17)
        Me.lblTranHead.TabIndex = 303
        Me.lblTranHead.Text = "Transaction Head"
        Me.lblTranHead.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboTranHead
        '
        Me.cboTranHead.DropDownWidth = 300
        Me.cboTranHead.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboTranHead.FormattingEnabled = True
        Me.cboTranHead.Location = New System.Drawing.Point(586, 33)
        Me.cboTranHead.Name = "cboTranHead"
        Me.cboTranHead.Size = New System.Drawing.Size(134, 21)
        Me.cboTranHead.TabIndex = 7
        '
        'lblApprovalStatus
        '
        Me.lblApprovalStatus.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblApprovalStatus.Location = New System.Drawing.Point(733, 37)
        Me.lblApprovalStatus.Name = "lblApprovalStatus"
        Me.lblApprovalStatus.Size = New System.Drawing.Size(95, 15)
        Me.lblApprovalStatus.TabIndex = 300
        Me.lblApprovalStatus.Text = "Approval Status"
        '
        'objTypeOf
        '
        Me.objTypeOf.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.objTypeOf.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objTypeOf.FormattingEnabled = True
        Me.objTypeOf.Location = New System.Drawing.Point(169, 33)
        Me.objTypeOf.Name = "objTypeOf"
        Me.objTypeOf.Size = New System.Drawing.Size(43, 21)
        Me.objTypeOf.TabIndex = 297
        Me.objTypeOf.Visible = False
        '
        'objCalcType
        '
        Me.objCalcType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.objCalcType.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objCalcType.FormattingEnabled = True
        Me.objCalcType.Location = New System.Drawing.Point(787, 34)
        Me.objCalcType.Name = "objCalcType"
        Me.objCalcType.Size = New System.Drawing.Size(43, 21)
        Me.objCalcType.TabIndex = 295
        Me.objCalcType.Visible = False
        '
        'cboEDApprovalStatus
        '
        Me.cboEDApprovalStatus.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboEDApprovalStatus.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboEDApprovalStatus.FormattingEnabled = True
        Me.cboEDApprovalStatus.Location = New System.Drawing.Point(833, 34)
        Me.cboEDApprovalStatus.Name = "cboEDApprovalStatus"
        Me.cboEDApprovalStatus.Size = New System.Drawing.Size(83, 21)
        Me.cboEDApprovalStatus.TabIndex = 8
        '
        'objbtnSearchEmployee
        '
        Me.objbtnSearchEmployee.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchEmployee.BorderSelected = False
        Me.objbtnSearchEmployee.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchEmployee.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objbtnSearchEmployee.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchEmployee.Location = New System.Drawing.Point(179, 35)
        Me.objbtnSearchEmployee.Name = "objbtnSearchEmployee"
        Me.objbtnSearchEmployee.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchEmployee.TabIndex = 88
        Me.objbtnSearchEmployee.Visible = False
        '
        'cboEmployee
        '
        Me.cboEmployee.DropDownWidth = 300
        Me.cboEmployee.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboEmployee.FormattingEnabled = True
        Me.cboEmployee.Location = New System.Drawing.Point(82, 33)
        Me.cboEmployee.Name = "cboEmployee"
        Me.cboEmployee.Size = New System.Drawing.Size(130, 21)
        Me.cboEmployee.TabIndex = 0
        '
        'lblEmployee
        '
        Me.lblEmployee.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEmployee.Location = New System.Drawing.Point(8, 36)
        Me.lblEmployee.Name = "lblEmployee"
        Me.lblEmployee.Size = New System.Drawing.Size(68, 15)
        Me.lblEmployee.TabIndex = 85
        Me.lblEmployee.Text = "Employee"
        '
        'objbtnReset
        '
        Me.objbtnReset.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.objbtnReset.BackColor = System.Drawing.Color.Transparent
        Me.objbtnReset.ButtonType = eZee.Common.eZeeSearchResetButton.EnumButtonType.Reset
        Me.objbtnReset.Image = CType(resources.GetObject("objbtnReset.Image"), System.Drawing.Image)
        Me.objbtnReset.Location = New System.Drawing.Point(903, 0)
        Me.objbtnReset.Name = "objbtnReset"
        Me.objbtnReset.ResultMessage = ""
        Me.objbtnReset.SearchMessage = ""
        Me.objbtnReset.Size = New System.Drawing.Size(24, 24)
        Me.objbtnReset.TabIndex = 69
        Me.objbtnReset.TabStop = False
        '
        'objbtnSearch
        '
        Me.objbtnSearch.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.objbtnSearch.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearch.ButtonType = eZee.Common.eZeeSearchResetButton.EnumButtonType.Search
        Me.objbtnSearch.Image = CType(resources.GetObject("objbtnSearch.Image"), System.Drawing.Image)
        Me.objbtnSearch.Location = New System.Drawing.Point(880, 0)
        Me.objbtnSearch.Name = "objbtnSearch"
        Me.objbtnSearch.ResultMessage = ""
        Me.objbtnSearch.SearchMessage = ""
        Me.objbtnSearch.Size = New System.Drawing.Size(24, 24)
        Me.objbtnSearch.TabIndex = 68
        Me.objbtnSearch.TabStop = False
        '
        'eZeeHeader
        '
        Me.eZeeHeader.BackColor = System.Drawing.SystemColors.Control
        Me.eZeeHeader.BorderColor = System.Drawing.SystemColors.ControlDark
        Me.eZeeHeader.DescriptionForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.eZeeHeader.Dock = System.Windows.Forms.DockStyle.Top
        Me.eZeeHeader.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.eZeeHeader.GradiantStyle = eZee.Common.eZeeHeader.GradientStyle.Central
        Me.eZeeHeader.GradientColor1 = System.Drawing.SystemColors.Window
        Me.eZeeHeader.GradientColor2 = System.Drawing.SystemColors.Control
        Me.eZeeHeader.HeaderTextForeColor = System.Drawing.SystemColors.ControlText
        Me.eZeeHeader.Icon = Nothing
        Me.eZeeHeader.Location = New System.Drawing.Point(0, 0)
        Me.eZeeHeader.Message = ""
        Me.eZeeHeader.Name = "eZeeHeader"
        Me.eZeeHeader.Size = New System.Drawing.Size(954, 60)
        Me.eZeeHeader.TabIndex = 3
        Me.eZeeHeader.Title = "Employee Earning and  Deduction List"
        '
        'lblTotalAmt
        '
        Me.lblTotalAmt.BackColor = System.Drawing.Color.Transparent
        Me.lblTotalAmt.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTotalAmt.Location = New System.Drawing.Point(370, 6)
        Me.lblTotalAmt.Name = "lblTotalAmt"
        Me.lblTotalAmt.Size = New System.Drawing.Size(122, 14)
        Me.lblTotalAmt.TabIndex = 221
        Me.lblTotalAmt.Text = "Total Flat Rate Amount"
        Me.lblTotalAmt.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objlblTotalAmt
        '
        Me.objlblTotalAmt.BackColor = System.Drawing.Color.Transparent
        Me.objlblTotalAmt.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objlblTotalAmt.Location = New System.Drawing.Point(498, 6)
        Me.objlblTotalAmt.Name = "objlblTotalAmt"
        Me.objlblTotalAmt.Size = New System.Drawing.Size(96, 14)
        Me.objlblTotalAmt.TabIndex = 222
        Me.objlblTotalAmt.Text = "0.00"
        Me.objlblTotalAmt.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'frmEarningDeductionList
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(954, 571)
        Me.Controls.Add(Me.pnlMainInfo)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmEarningDeductionList"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Employee Earning and  Deduction List"
        Me.pnlMainInfo.ResumeLayout(False)
        Me.gbEDList.ResumeLayout(False)
        Me.gbEDList.PerformLayout()
        Me.pnlEdList.ResumeLayout(False)
        Me.pnlEdList.PerformLayout()
        CType(Me.dgvED, System.ComponentModel.ISupportInitialize).EndInit()
        Me.objFooter.ResumeLayout(False)
        Me.mnuOperations.ResumeLayout(False)
        Me.gbFilterCriteria.ResumeLayout(False)
        CType(Me.objbtnReset, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.objbtnSearch, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents pnlMainInfo As System.Windows.Forms.Panel
    Friend WithEvents eZeeHeader As eZee.Common.eZeeHeader
    Friend WithEvents gbFilterCriteria As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents objbtnSearchEmployee As eZee.Common.eZeeGradientButton
    Friend WithEvents cboEmployee As System.Windows.Forms.ComboBox
    Friend WithEvents lblEmployee As System.Windows.Forms.Label
    Friend WithEvents objbtnReset As eZee.Common.eZeeSearchResetButton
    Friend WithEvents objbtnSearch As eZee.Common.eZeeSearchResetButton
    Friend WithEvents objFooter As eZee.Common.eZeeFooter
    Friend WithEvents btnDelete As eZee.Common.eZeeLightButton
    Friend WithEvents btnEdit As eZee.Common.eZeeLightButton
    Friend WithEvents btnNew As eZee.Common.eZeeLightButton
    Friend WithEvents btnClose As eZee.Common.eZeeLightButton
    Friend WithEvents btnOperations As eZee.Common.eZeeSplitButton
    Friend WithEvents mnuOperations As System.Windows.Forms.ContextMenuStrip
    Friend WithEvents mnuBatchTransaction As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuGlobalAssign As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuExemption As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents objSep1 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents mnuExportED As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuImportED As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents dlgSaveFile As System.Windows.Forms.SaveFileDialog
    Friend WithEvents mnuImporUpdatetFlatRateTran As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents cboEDApprovalStatus As System.Windows.Forms.ComboBox
    Friend WithEvents objTypeOf As System.Windows.Forms.ComboBox
    Friend WithEvents objCalcType As System.Windows.Forms.ComboBox
    Friend WithEvents objTranHeadType As System.Windows.Forms.ComboBox
    Friend WithEvents lblApprovalStatus As System.Windows.Forms.Label
    Friend WithEvents pnlEdList As System.Windows.Forms.Panel
    Friend WithEvents btnDisApprove As eZee.Common.eZeeLightButton
    Friend WithEvents btnApprove As eZee.Common.eZeeLightButton
    Friend WithEvents cboTranHead As System.Windows.Forms.ComboBox
    Friend WithEvents lblTranHead As System.Windows.Forms.Label
    Friend WithEvents objbtnSearchTranHead As eZee.Common.eZeeGradientButton
    Friend WithEvents mnuApprovedEDHeads As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuPendingEDHeads As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuAllEDHeads As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents gbEDList As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents chkIncludeInactiveEmployee As System.Windows.Forms.CheckBox
    Friend WithEvents objchkSelectAll As System.Windows.Forms.CheckBox
    Friend WithEvents lnkAllocation As System.Windows.Forms.LinkLabel
    Friend WithEvents lblPeriod As System.Windows.Forms.Label
    Friend WithEvents cboPeriod As System.Windows.Forms.ComboBox
    Friend WithEvents ToolStripMenuItem1 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents mnuAssignMembershipHeads As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuUpdatetT24ACBLoanData As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents dgvED As System.Windows.Forms.DataGridView
    Friend WithEvents objdgcolhCheck As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents objdgColhBlank As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhEmployee As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhEmpCode As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhTrnHeadCode As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhTrnHead As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhTranHeadType As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhTypeOf As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhCalcType As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhPeriod As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhAmount As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhApprovePending As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhIsGroup As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhEDUnkid As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhEmpid As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhTranheadUnkId As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhPeriodUnkid As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhTranHeadTypeId As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhTypeOfId As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhCalcTypeId As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhMembership As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhMemTranUnkid As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents lblCalcType As System.Windows.Forms.Label
    Friend WithEvents cboCalcType As System.Windows.Forms.ComboBox
    Friend WithEvents objlblTotalAmt As System.Windows.Forms.Label
    Friend WithEvents lblTotalAmt As System.Windows.Forms.Label
End Class
