﻿Option Strict On
Imports eZeeCommonLib
Imports Aruti.Data

Public Class frmGlobalAssignEarningDeduction

#Region " Private Variables "
    Private ReadOnly mstrModuleName As String = "frmGlobalAssignEarningDeduction"

    Private mblnCancel As Boolean = True

    Private objED As clsEarningDeduction

    Private mintEdunkid As Integer = -1
    Private mstrCurrencyFormat As String = "#0.00"
    Private menAction As enAction = enAction.ADD_ONE 'Sohail (03 Nov 2010)

    Private mdtPayPeriodStartDate As DateTime
    Private mdtPayPeriodEndDate As DateTime

    Private mstrAdvanceFilter As String = "" 'Sohail (31 Aug 2012)

    'Sohail (18 Jul 2017) -- Start
    'Enhancement - 69.1 - Day wise total days worked and hours worked functions.
    Private dvEmployee As DataView
    Private mintTotalEmployee As Integer = 0
    Private mintCount As Integer = 0
    Private mstrSearchEmpText As String = ""
    'Sohail (18 Jul 2017) -- End
    Private mblnProcessFailed As Boolean = False 'Hemant (20 July 2018)



#End Region

#Region " Display Dialogue "
    Public Function DisplayDialog(ByVal eAction As enAction) As Boolean 'Sohail (03 Nov 2010)
        'Public Function DisplayDialog() As Boolean
        Try
            menAction = eAction 'Sohail (03 Nov 2010)
            Me.ShowDialog()

            Return Not mblnCancel
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "displayDialog", mstrModuleName)
        End Try
    End Function

#End Region

#Region " Private Methods "

    Private Sub SetColor()
        Try
            cboTrnHeadType.BackColor = GUI.ColorOptional 'Sohail (15 Oct 2010)
            cboTypeOf.BackColor = GUI.ColorOptional 'Sohail (15 Oct 2010)
            cboTrnHead.BackColor = GUI.ColorComp
            'cboVendor.BackColor = GUI.ColorOptional 'Sohail (11 Sep 2010)
            'cboCurrencyCountry.BackColor = GUI.ColorComp
            txtAmount.BackColor = GUI.ColorComp

            cboEmployee.BackColor = GUI.ColorOptional
            'Sohail (09 Nov 2013) -- Start
            'TRA - ENHANCEMENT
            'cboDepartment.BackColor = GUI.ColorOptional
            'cboGrade.BackColor = GUI.ColorOptional
            'cboSections.BackColor = GUI.ColorOptional
            'cboClass.BackColor = GUI.ColorOptional
            'cboCostCenter.BackColor = GUI.ColorOptional
            'cboJob.BackColor = GUI.ColorOptional
            'cboPayPoint.BackColor = GUI.ColorOptional
            'cboUnit.BackColor = GUI.ColorOptional
            'cboGender.BackColor = GUI.ColorOptional 'Sohail (31 Aug 2012)
            'Sohail (09 Nov 2013) -- End

            cboPeriod.BackColor = GUI.ColorComp 'Sohail (13 Jan 2012)
            txtMedicalRefNo.BackColor = GUI.ColorOptional 'Sohail (18 Jan 2012)

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetColor", mstrModuleName)
        End Try
    End Sub

    'Private Sub SetValue(ByVal intEmpUnkId As Integer)
    '    Dim objTranHead As New clsTransactionHead
    '    'Dim dsList As DataSet

    '    Try
    '        With objED
    '            ._Employeeunkid = CInt(lvEmployeeList.Items(intEmpUnkId).Tag)
    '            ._Tranheadunkid = CInt(cboTrnHead.SelectedValue)
    '            ._Batchtransactionunkid = Nothing
    '            If txtAmount.Text.Length > 0 Then
    '                ._Amount = cdec(txtAmount.Text)
    '            Else
    '                ._Amount = Nothing
    '            End If
    '            If CInt(cboTrnHeadType.SelectedValue) = 1 Then 'Earning for Employee
    '                ._Isdeduct = False
    '            ElseIf CInt(cboTrnHeadType.SelectedValue) = 2 Or CInt(cboTrnHeadType.SelectedValue) = 3 Then 'Deduction, Statutory Deduction
    '                ._Isdeduct = True
    '            Else
    '                ._Isdeduct = Nothing
    '            End If

    '            objTranHead._Tranheadunkid = CInt(cboTrnHead.SelectedValue)
    '            ._Trnheadtype_Id = objTranHead._Trnheadtype_Id
    '            ._Typeof_Id = objTranHead._Typeof_id
    '            ._Calctype_Id = objTranHead._Calctype_Id
    '            ._Computeon_Id = objTranHead._Computeon_Id
    '            ._Formula = objTranHead._Formula

    '            'dsList = objTranHead.GetTranHeadByUnkId(CInt(cboTrnHead.SelectedValue), "TranHead")
    '            'If dsList.Tables("TranHead").Rows.Count > 0 Then
    '            '    For Each dtRow As DataRow In dsList.Tables("TranHead").Rows
    '            '        ._Trnheadtype_Id = CInt(dtRow("trnheadtype_id").ToString)
    '            '        ._Typeof_Id = CInt(dtRow("typeof_id").ToString)
    '            '        ._Calctype_Id = CInt(dtRow("calctype_id").ToString)
    '            '        '._Calcperiod_Id = CInt(dtRow("calcperiod_id").ToString)
    '            '        ._Computeon_Id = CInt(dtRow("computeon_id").ToString)
    '            '        ._Formula = dtRow("formula").ToString
    '            '    Next
    '            'Else
    '            '    ._Trnheadtype_Id = Nothing
    '            '    ._Typeof_Id = Nothing
    '            '    ._Calctype_Id = Nothing
    '            '    '._Calcperiod_Id = Nothing
    '            '    ._Computeon_Id = Nothing
    '            '    ._Formula = Nothing
    '            'End If

    '            ._Currencyunkid = CInt(cboCurrencyCountry.SelectedValue)
    '            ._Vendorunkid = CInt(cboVendor.SelectedValue)
    '            ._Userunkid = Nothing
    '            ._Isvoid = Nothing
    '            ._Voiduserunkid = Nothing
    '            ._Voiddatetime = Nothing
    '        End With
    '    Catch ex As Exception
    '        DisplayError.Show("-1", ex.Message, "SetValue", mstrModuleName)
    '    Finally
    '        objTranHead = Nothing
    '    End Try
    'End Sub

    Private Sub FillList()
        Dim objEmployee As New clsEmployee_Master
        Dim dsEmployee As New DataSet
        Dim strFilter As String = "" 'Sohail (25 May 2012)
        'Sohail (14 Feb 2019) -- Start
        'NMB Enhancement - 76.1 - Option to filter employees who has not been assigned selected head on Global Assign ED screen.
        Dim strTempTableSelectQry As String = ""
        Dim strTempTableJoinQry As String = ""
        Dim strTempTableDropQry As String = ""
        'Sohail (14 Feb 2019) -- End

        Try
            'Sohail (25 May 2012) -- Start
            'TRA - ENHANCEMENT
            'Sohail (18 Jul 2017) -- Start
            'Enhancement - 69.1 - Day wise total days worked and hours worked functions.
            'lvEmployeeList.Items.Clear()
            objlblEmpCount.Text = "( 0 / 0 )"
            mintCount = 0
            dgEmployee.DataSource = Nothing
            RemoveHandler txtSearchEmp.TextChanged, AddressOf txtSearchEmp_TextChanged
            Call SetDefaultSearchEmpText()
            AddHandler txtSearchEmp.TextChanged, AddressOf txtSearchEmp_TextChanged
            'Sohail (18 Jul 2017) -- End
            If CInt(cboPeriod.SelectedValue) <= 0 Then Exit Sub
            If chkShowNewlyHiredEmployees.Checked = True Then strFilter = " CONVERT(CHAR(8), appointeddate, 112) >= @startdate "
            'Sohail (25 May 2012) -- End

            'Sohail (14 Feb 2019) -- Start
            'NMB Enhancement - 76.1 - Option to filter employees who has not been assigned selected head on Global Assign ED screen.
            If chkNotAssignedHead.Checked = True AndAlso CInt(cboPeriod.SelectedValue) > 0 AndAlso CInt(cboTrnHead.SelectedValue) > 0 Then
                strTempTableSelectQry &= "SELECT     employeeunkid " & _
                                         "INTO #tblED " & _
                                         "FROM prearningdeduction_master " & _
                                         "WHERE isvoid = 0 " & _
                                             "AND periodunkid = " & CInt(cboPeriod.SelectedValue) & " " & _
                                             "AND tranheadunkid = " & CInt(cboTrnHead.SelectedValue) & " "

                strTempTableJoinQry &= "LEFT JOIN #tblED ON #tblED.employeeunkid = hremployee_master.employeeunkid "

                strTempTableDropQry &= "AND #tblED.employeeunkid IS NULL; DROP TABLE #tblED "
            End If
            'Sohail (14 Feb 2019) -- End

            'Sohail (30 Nov 2013) -- Start
            'Enhancement - Oman
            If chkShowReinstatedEmployees.Checked = True Then
                'Sohail (14 Feb 2019) -- Start
                'NMB Enhancement - 76.1 - Option to filter employees who has not been assigned selected head on Global Assign ED screen.
                'If strFilter.Trim <> "" Then
                '    strFilter &= " AND CONVERT(CHAR(8), reinstatement_date, 112) >= @startdate "
                'Else
                '    strFilter = " CONVERT(CHAR(8), reinstatement_date, 112) >= @startdate "
                'End If
                strTempTableSelectQry &= "SELECT DISTINCT hremployee_rehire_tran.employeeunkid " & _
                                         "INTO #tblRehire " & _
                                         "FROM   hremployee_rehire_tran " & _
                                        " WHERE isvoid = 0 " & _
                                                 "AND reinstatment_date IS NOT NULL " & _
                                                 "AND CONVERT(CHAR(8), effectivedate, 112) <= @enddate " & _
                                                 "AND CONVERT(CHAR(8), reinstatment_date, 112) BETWEEN @startdate AND @enddate "

                strTempTableJoinQry &= "JOIN #tblRehire ON #tblRehire.employeeunkid = hremployee_master.employeeunkid "

                strTempTableDropQry &= "DROP TABLE #tblRehire "
                'Sohail (14 Feb 2019) -- End
            End If
            'Sohail (30 Nov 2013) -- End

            'Sohail (31 Aug 2012) -- Start
            'TRA - ENHANCEMENT
            If mstrAdvanceFilter <> "" Then
                If strFilter <> "" Then
                    strFilter &= " AND " & mstrAdvanceFilter
                Else
                    strFilter = mstrAdvanceFilter
                End If
            End If
            'Sohail (31 Aug 2012) -- End

            Cursor.Current = Cursors.WaitCursor 'Sohail (01 Dec 2010)
            'Sohail (06 Jan 2012) -- Start
            'TRA - ENHANCEMENT
            'dsEmployee = objEmployee.GetEmployeeList("Employee", False, True _
            '                                          , CInt(cboEmployee.SelectedValue) _
            '                                          , CInt(cboDepartment.SelectedValue) _
            '                                          , CInt(cboSections.SelectedValue) _
            '                                          , CInt(cboUnit.SelectedValue) _
            '                                          , CInt(cboGrade.SelectedValue) _
            '                                          , 0 _
            '                                          , CInt(cboClass.SelectedValue) _
            '                                          , CInt(cboCostCenter.SelectedValue) _
            '                                          , 0 _
            '                                          , CInt(cboJob.SelectedValue) _
            '                                          , CInt(cboPayPoint.SelectedValue) _
            '                                        )
            'Sohail (09 Nov 2013) -- Start
            'TRA - ENHANCEMENT
            'dsEmployee = objEmployee.GetEmployeeList("Employee", False, True _
            '                                          , CInt(cboEmployee.SelectedValue) _
            '                                          , CInt(cboDepartment.SelectedValue) _
            '                                          , CInt(cboSections.SelectedValue) _
            '                                          , CInt(cboUnit.SelectedValue) _
            '                                          , CInt(cboGrade.SelectedValue) _
            '                                          , 0 _
            '                                          , CInt(cboClass.SelectedValue) _
            '                                          , CInt(cboCostCenter.SelectedValue) _
            '                                          , 0 _
            '                                          , CInt(cboJob.SelectedValue) _
            '                                          , CInt(cboPayPoint.SelectedValue) _
            '                                          , mdtPayPeriodStartDate, mdtPayPeriodEndDate, , CInt(cboGender.SelectedValue), , _
            '                                          , strFilter) 'Sohail (25 May 2012) - [strFilter]
            ''                                         'Sohail (31 Aug 2012) - [CInt(cboGender.SelectedValue)]
            'Anjan [10 June 2015] -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'dsEmployee = objEmployee.GetEmployeeList("Employee", False, True _
            '                                          , CInt(cboEmployee.SelectedValue) _
            '                                          , 0 _
            '                                          , 0 _
            '                                          , 0 _
            '                                          , 0 _
            '                                          , 0 _
            '                                          , 0 _
            '                                          , 0 _
            '                                          , 0 _
            '                                          , 0 _
            '                                          , 0 _
            '                                          , mdtPayPeriodStartDate, mdtPayPeriodEndDate, , 0, , _
            '                                          , strFilter)

            dsEmployee = objEmployee.GetEmployeeList(FinancialYear._Object._DatabaseName, _
                                           User._Object._Userunkid, _
                                           FinancialYear._Object._YearUnkid, _
                                           Company._Object._Companyunkid, _
                                           mdtPayPeriodStartDate, _
                                           mdtPayPeriodEndDate, _
                                           ConfigParameter._Object._UserAccessModeSetting, _
                                            True, False, "Employee", False, CInt(cboEmployee.SelectedValue), , , , , , , , , , , , , , strFilter _
                                            , strTempTableSelectQry:=strTempTableSelectQry _
                                            , strTempTableJoinQry:=strTempTableJoinQry _
                                            , strTempTableDropQry:=strTempTableDropQry _
                                            )
            'Sohail (14 Feb 2019) - [strTempTableSelectQry, strTempTableJoinQry, strTempTableDropQry]
            'Anjan [10 June 2015] -- End

            '                                         'Sohail (25 May 2012) - [strFilter]
            '                                         'Sohail (31 Aug 2012) - [CInt(cboGender.SelectedValue)]
            'Sohail (09 Nov 2013) -- End
            'Sohail (06 Jan 2012) -- End

            'Sohail (18 Jul 2017) -- Start
            'Enhancement - 69.1 - Day wise total days worked and hours worked functions.
            'Dim lvItem As ListViewItem
            'Dim lvArray As New List(Of ListViewItem) 'Sohail (03 Nov 2010)
            'lvEmployeeList.Items.Clear()
            'lvEmployeeList.BeginUpdate() 'Sohail (11 Sep 2010)
            mintTotalEmployee = dsEmployee.Tables("Employee").Rows.Count
            objlblEmpCount.Text = "( 0 / " & mintTotalEmployee.ToString & " )"

            If dsEmployee.Tables(0).Columns.Contains("IsChecked") = False Then
                Dim dtCol As DataColumn
                dtCol = New DataColumn("IsChecked", System.Type.GetType("System.Boolean"))
                dtCol.DefaultValue = False
                dtCol.AllowDBNull = False
                dsEmployee.Tables(0).Columns.Add(dtCol)
            End If
            'Sohail (18 Jul 2017) -- End

            'Sohail (18 Jul 2017) -- Start
            'Enhancement - 69.1 - Day wise total days worked and hours worked functions.
            'For Each dtRow As DataRow In dsEmployee.Tables("Employee").Rows
            '    lvItem = New ListViewItem
            '    With lvItem
            '        .Text = ""
            '        .Tag = dtRow.Item("employeeunkid").ToString
            '        .SubItems.Add(dtRow.Item("employeecode").ToString)
            '        .SubItems.Add(dtRow.Item("employeename").ToString)
            '        'Sohail (11 Sep 2010) -- Start
            '        'RemoveHandler lvEmployeeList.ItemChecked, AddressOf lvEmployeeList_ItemChecked
            '        'lvEmployeeList.Items.Add(lvItem) 'Sohail (03 Nov 2010)
            '        'AddHandler lvEmployeeList.ItemChecked, AddressOf lvEmployeeList_ItemChecked
            '        lvArray.Add(lvItem) 'Sohail (03 Nov 2010)
            '        'Sohail (11 Sep 2010) -- End
            '    End With
            'Next
                    'RemoveHandler lvEmployeeList.ItemChecked, AddressOf lvEmployeeList_ItemChecked
            'lvEmployeeList.Items.AddRange(lvArray.ToArray) 'Sohail (03 Nov 2010)
                    'AddHandler lvEmployeeList.ItemChecked, AddressOf lvEmployeeList_ItemChecked
            'If lvEmployeeList.Items.Count > 15 Then
            '    colhName.Width = 190 - 18
            'Else
            '    colhName.Width = 190
            'End If
            'Call objbtnSearch.ShowResult(CStr(lvEmployeeList.Items.Count))
            dvEmployee = dsEmployee.Tables("Employee").DefaultView
            dgEmployee.AutoGenerateColumns = False
            objdgcolhCheck.DataPropertyName = "IsChecked"
            objdgcolhEmployeeunkid.DataPropertyName = "employeeunkid"
            dgColhEmpCode.DataPropertyName = "employeecode"
            dgColhEmployee.DataPropertyName = "employeename"

            dgEmployee.DataSource = dvEmployee
            dvEmployee.Sort = "IsChecked DESC, employeename "
            'Sohail (18 Jul 2017) -- End
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillList", mstrModuleName)
        Finally
            objEmployee = Nothing
            dsEmployee = Nothing
            'lvEmployeeList.EndUpdate() 'Sohail (11 Sep 2010) 'Sohail (18 Jul 2017)
            Cursor.Current = Cursors.Default 'Sohail (01 Dec 2010)
        End Try
    End Sub

    Private Sub FillCombo()
        Dim objCommonMaster As New clsMasterData
        'Dim objVendor As New clsvendor_master
        'Sohail (09 Nov 2013) -- Start
        'TRA - ENHANCEMENT
        'Dim objDept As New clsDepartment
        'Dim objGrade As New clsGrade
        'Dim objSection As New clsSections
        'Dim objClass As New clsClass
        'Dim objCostCenter As New clscostcenter_master
        'Dim objJob As New clsJobs
        'Dim objPayPoint As New clspaypoint_master
        'Dim objUnit As New clsUnits
        'Sohail (09 Nov 2013) -- End
        Dim dsList As DataSet
        'Dim objExRate As New clsExchangeRate
        'Dim objEmployee As New clsEmployee_Master 'Sohail (03 Mar 2012)
        Dim objTranHead As New clsTransactionHead
        Dim dtTable As DataTable
        Dim objPeriod As New clscommom_period_Tran 'Sohail (13 Jan 2012)

        Try
            dsList = objCommonMaster.getComboListForHeadType("HeadType")
            With cboTrnHeadType
                .BeginUpdate() 'Sohail (11 Sep 2010)
                .ValueMember = "Id"
                .DisplayMember = "Name"
                .DataSource = dsList.Tables("HeadType")
                If .Items.Count > 0 Then .SelectedIndex = 0
                .EndUpdate() 'Sohail (11 Sep 2010)
            End With
            With objTranHeadType
                .BeginUpdate() 'Sohail (11 Sep 2010)
                .ValueMember = "Id"
                .DisplayMember = "Name"
                .DataSource = cboTrnHeadType.DataSource
                .EndUpdate() 'Sohail (11 Sep 2010)
            End With

            dsList = objCommonMaster.getComboListComputedOn("ComputeOn")
            With objComputeOn
                .BeginUpdate() 'Sohail (11 Sep 2010)
                .ValueMember = "Id"
                .DisplayMember = "Name"
                .DataSource = dsList.Tables("ComputeOn")
                If .Items.Count > 0 Then .SelectedIndex = 0
                .EndUpdate() 'Sohail (11 Sep 2010)
            End With

            'Sohail (11 Sep 2010) -- Start
            'dsList = objVendor.getComborList("Vendor", True)
            'With cboVendor
            '    .ValueMember = "vendorunkid"
            '    .DisplayMember = "companyname"
            '    .DataSource = dsList.Tables("Vendor")
            '    If .Items.Count > 0 Then .SelectedIndex = 0
            'End With
            'Sohail (11 Sep 2010) -- End


            'dsList = objExRate.getComboList("Currency", True)
            'Dim dtRow As DataRow() = dsList.Tables("Currency").Select("isbasecurrency =" & True)
            'With cboCurrencyCountry
            '    .ValueMember = "exchangerateunkid"
            '    .DisplayMember = "currency_name"
            '    .DataSource = dsList.Tables("Currency")
            '    If .Items.Count > 0 Then
            '        If dtRow.Length > 0 Then
            '            .SelectedValue = CInt(dtRow(0).Item("exchangerateunkid"))
            '        End If
            '    End If
            'End With



            'Sohail (03 Mar 2012) -- Start
            'TRA - ENHANCEMENT
            'Sohail (06 Jan 2012) -- Start
            'TRA - ENHANCEMENT
            'dsList = objEmployee.GetEmployeeList("EmployeeList", True)
            'dsList = objEmployee.GetEmployeeList("EmployeeList", True, , , , , , , , , , , , , ConfigParameter._Object._CurrentDateAndTime.Date, ConfigParameter._Object._CurrentDateAndTime.Date)
            ''Sohail (06 Jan 2012) -- End
            'With cboEmployee
            '    .BeginUpdate() 'Sohail (11 Sep 2010)
            '    .ValueMember = "employeeunkid"
            '    .DisplayMember = "employeename"
            '    .DataSource = dsList.Tables("EmployeeList")
            '    If .Items.Count > 0 Then .SelectedIndex = 0
            '    .EndUpdate() 'Sohail (11 Sep 2010)
            'End With
            Call FillCombo_Employee()
            'Sohail (03 Mar 2012) -- End

            'Sohail (09 Nov 2013) -- Start
            'TRA - ENHANCEMENT
            'dsList = objDept.getComboList("DepartmentList", True)
            'With cboDepartment
            '    .BeginUpdate() 'Sohail (11 Sep 2010)
            '    .ValueMember = "departmentunkid"
            '    .DisplayMember = "name"
            '    .DataSource = dsList.Tables("DepartmentList")
            '    If .Items.Count > 0 Then .SelectedIndex = 0
            '    .EndUpdate() 'Sohail (11 Sep 2010)
            'End With

            'dsList = objGrade.getComboList("GradeList", True)
            'With cboGrade
            '    .BeginUpdate() 'Sohail (11 Sep 2010)
            '    .ValueMember = "gradeunkid"
            '    .DisplayMember = "name"
            '    .DataSource = dsList.Tables("GradeList")
            '    If .Items.Count > 0 Then .SelectedIndex = 0
            '    .EndUpdate() 'Sohail (11 Sep 2010)
            'End With

            'dsList = objSection.getComboList("SectionList", True)
            'With cboSections
            '    .BeginUpdate() 'Sohail (11 Sep 2010)
            '    .ValueMember = "sectionunkid"
            '    .DisplayMember = "name"
            '    .DataSource = dsList.Tables("SectionList")
            '    If .Items.Count > 0 Then .SelectedIndex = 0
            '    .EndUpdate() 'Sohail (11 Sep 2010)
            'End With

            'dsList = objClass.getComboList("ClassList", True)
            'With cboClass
            '    .BeginUpdate() 'Sohail (11 Sep 2010)
            '    .ValueMember = "classesunkid"
            '    .DisplayMember = "name"
            '    .DataSource = dsList.Tables("ClassList")
            '    If .Items.Count > 0 Then .SelectedIndex = 0
            '    .EndUpdate() 'Sohail (11 Sep 2010)
            'End With

            'dsList = objCostCenter.getComboList("CostCenterList", True)
            'With cboCostCenter
            '    .BeginUpdate() 'Sohail (11 Sep 2010)
            '    .ValueMember = "CostCenterunkid"
            '    .DisplayMember = "CostCentername"
            '    .DataSource = dsList.Tables("CostCenterList")
            '    If .Items.Count > 0 Then .SelectedIndex = 0
            '    .EndUpdate() 'Sohail (11 Sep 2010)
            'End With

            'dsList = objJob.getComboList("JobList", True)
            'With cboJob
            '    .BeginUpdate() 'Sohail (11 Sep 2010)
            '    .ValueMember = "jobunkid"
            '    .DisplayMember = "name"
            '    .DataSource = dsList.Tables("JobList")
            '    If .Items.Count > 0 Then .SelectedIndex = 0
            '    .EndUpdate() 'Sohail (11 Sep 2010)
            'End With

            'dsList = objPayPoint.getListForCombo("PayPointList", True)
            'With cboPayPoint
            '    .BeginUpdate() 'Sohail (11 Sep 2010)
            '    .ValueMember = "paypointunkid"
            '    .DisplayMember = "name"
            '    .DataSource = dsList.Tables("PayPointList")
            '    If .Items.Count > 0 Then .SelectedIndex = 0
            '    .EndUpdate() 'Sohail (11 Sep 2010)
            'End With

            'dsList = objUnit.getComboList("UnitList", True)
            'With cboUnit
            '    .BeginUpdate() 'Sohail (11 Sep 2010)
            '    .ValueMember = "unitunkid"
            '    .DisplayMember = "name"
            '    .DataSource = dsList.Tables("UnitList")
            '    If .Items.Count > 0 Then .SelectedIndex = 0
            '    .EndUpdate() 'Sohail (11 Sep 2010)
            'End With

            ''Sohail (31 Aug 2012) -- Start
            ''TRA - ENHANCEMENT
            'dsList = objCommonMaster.getGenderList("Gender", True)
            'With cboGender
            '    .ValueMember = "id"
            '    .DisplayMember = "Name"
            '    .DataSource = dsList.Tables("Gender")
            'End With
            ''Sohail (31 Aug 2012) -- End
            'Sohail (09 Nov 2013) -- End

            'Sohail (15 Oct 2010) -- Start
            'Sohail (14 Apr 2011) -- Start
            'dsList = objTranHead.getComboList("TranHead", True, 0, enCalcType.FlatRate_Others, -1)
            'Sohail (29 Oct 2012) -- Start
            'TRA - ENHANCEMENT
            'dsList = objTranHead.getComboList("TranHead", True, 0, 0, -1)
            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'dsList = objTranHead.getComboList("TranHead", True, 0, 0, -1, , True)
            dsList = objTranHead.getComboList(FinancialYear._Object._DatabaseName, "TranHead", True, 0, 0, -1, , True)
            'Sohail (21 Aug 2015) -- End
            'Sohail (29 Oct 2012) -- End
            'Sohail (14 Apr 2011) -- End
            dtTable = New DataView(dsList.Tables("TranHead"), "typeof_id <> " & enTypeOf.Salary & "", "", DataViewRowState.CurrentRows).ToTable 'Except Salary Heads
            With cboTrnHead
                .ValueMember = "tranheadunkid"
                .DisplayMember = "Name"
                .DataSource = dtTable
                If .Items.Count > 0 Then .SelectedValue = 0
            End With
            'Sohail (15 Oct 2010) -- End

            'Sohail (13 Jan 2012) -- Start
            'TRA - ENHANCEMENT
            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'dsList = objPeriod.getListForCombo(enModuleReference.Payroll, FinancialYear._Object._YearUnkid, "Period", True, enStatusType.Open)
            dsList = objPeriod.getListForCombo(enModuleReference.Payroll, FinancialYear._Object._YearUnkid, FinancialYear._Object._DatabaseName, FinancialYear._Object._Database_Start_Date, "Period", True, enStatusType.Open)
            'Sohail (21 Aug 2015) -- End
            With cboPeriod
                .BeginUpdate()
                .ValueMember = "periodunkid"
                .DisplayMember = "name"
                .DataSource = dsList.Tables("Period")
                .EndUpdate()
            End With
            'Sohail (13 Jan 2012) -- End

            'Sohail (15 Dec 2018) -- Start
            'HJFMRI Enhancement - Copy Selected Period ED Slab option on Global Assign ED screen in 75.1.
            dsList = objPeriod.getListForCombo(enModuleReference.Payroll, FinancialYear._Object._YearUnkid, FinancialYear._Object._DatabaseName, FinancialYear._Object._Database_Start_Date, "Period", True, enStatusType.Close)
            With cboCopySelectedPeriodEDSlab
                .BeginUpdate()
                .ValueMember = "periodunkid"
                .DisplayMember = "name"
                .DataSource = dsList.Tables("Period")
                .EndUpdate()
            End With
            'Sohail (15 Dec 2018) -- End

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        Finally
            objCommonMaster = Nothing
            'objEmployee = Nothing 'Sohail (03 Mar 2012)
            'Sohail (09 Nov 2013) -- Start
            'TRA - ENHANCEMENT
            'objDept = Nothing
            'objGrade = Nothing
            'objSection = Nothing
            'objClass = Nothing
            'objCostCenter = Nothing
            'objJob = Nothing
            'objPayPoint = Nothing
            'objUnit = Nothing
            'Sohail (09 Nov 2013) -- End
            'objVendor = Nothing
            'objExRate = Nothing
        End Try
    End Sub


    'Sohail (03 Mar 2012) -- Start
    'TRA - ENHANCEMENT
    Private Sub FillCombo_Employee()
        Dim objEmployee As New clsEmployee_Master
        Dim dsList As DataSet
        Dim strFilter As String = "" 'Sohail (25 May 2012)
        Try
            'Sohail (25 May 2012) -- Start
            'TRA - ENHANCEMENT
            'dsList = objEmployee.GetEmployeeList("EmployeeList", True, , , , , , , , , , , , , mdtPayPeriodStartDate, mdtPayPeriodEndDate)
            If chkShowNewlyHiredEmployees.Checked = True AndAlso CInt(cboPeriod.SelectedValue) > 0 Then strFilter = " CONVERT(CHAR(8), appointeddate, 112) >= @startdate "
            'Sohail (31 Aug 2012) -- Start
            'TRA - ENHANCEMENT
            If mstrAdvanceFilter <> "" Then
                If strFilter <> "" Then
                    strFilter &= " AND " & mstrAdvanceFilter
                Else
                    strFilter = mstrAdvanceFilter
                End If
            End If
            'Sohail (31 Aug 2012) -- End
            'Sohail (09 Nov 2013) -- Start
            'TRA - ENHANCEMENT
            'dsList = objEmployee.GetEmployeeList("EmployeeList", True, , , , , , , , , , , , , mdtPayPeriodStartDate, mdtPayPeriodEndDate, , CInt(cboGender.SelectedValue), , , strFilter) 'Sohail (31 Aug 2012) - [CInt(cboGender.SelectedValue)]
            'Anjan [10 June 2015] -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'dsList = objEmployee.GetEmployeeList("EmployeeList", True, , , , , , , , , , , , , mdtPayPeriodStartDate, mdtPayPeriodEndDate, , 0, , , strFilter) 'Sohail (31 Aug 2012) - [CInt(cboGender.SelectedValue)]

            dsList = objEmployee.GetEmployeeList(FinancialYear._Object._DatabaseName, _
                                           User._Object._Userunkid, _
                                           FinancialYear._Object._YearUnkid, _
                                           Company._Object._Companyunkid, _
                                           mdtPayPeriodStartDate, _
                                           mdtPayPeriodEndDate, _
                                           ConfigParameter._Object._UserAccessModeSetting, _
                                           True, False, "EmployeeList", True, , , , , , , , , , , , , , , strFilter)
            'Anjan [10 June 2015] -- End

            'Sohail (09 Nov 2013) -- End
            'Sohail (25 May 2012) -- End



            With cboEmployee
                .BeginUpdate()
                .ValueMember = "employeeunkid"
                .DisplayMember = "employeename"
                .DataSource = dsList.Tables("EmployeeList")
                If .Items.Count > 0 Then .SelectedIndex = 0
                .EndUpdate()
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillComboEmployee", mstrModuleName)
        Finally
            objEmployee = Nothing
        End Try
    End Sub

    'Sohail (03 Mar 2012) -- End

    'Private Sub FillItemList()

    '    Try
    '        With lvEarningDeduction
    '            Dim lvItems As ListViewItem

    '            .Items.Clear()

    '            For Each dtRow As DataRow In mdtTran.Rows
    '                If CStr(IIf(IsDBNull(dtRow.Item("AUD")), "A", dtRow.Item("AUD"))) <> "D" Then
    '                    lvItems = New ListViewItem
    '                    With lvItems
    '                        .Text = dtRow.Item("edunkid").ToString
    '                        .Tag = dtRow.Item("edunkid").ToString
    '                        .SubItems.Add(dtRow.Item("batchtranunkid").ToString)
    '                        .SubItems.Add(dtRow.Item("tranheadunkid").ToString)
    '                        .SubItems.Add(dtRow.Item("tranhead").ToString)
    '                        .SubItems.Add(dtRow.Item("trnheadtypeID").ToString)
    '                        .SubItems.Add(dtRow.Item("trnheadtype").ToString)
    '                        .SubItems.Add(dtRow.Item("typeofId").ToString)
    '                        .SubItems.Add(dtRow.Item("calctypeId").ToString)
    '                        .SubItems.Add(dtRow.Item("calctype").ToString)
    '                        .SubItems.Add(dtRow.Item("computeonid").ToString)
    '                        .SubItems.Add(dtRow.Item("computeon").ToString)
    '                        .SubItems.Add(dtRow.Item("amount").ToString)
    '                        .SubItems.Add(dtRow.Item("currencyunkId").ToString)
    '                        .SubItems.Add(dtRow.Item("vendorunkId").ToString)
    '                        .SubItems.Add(dtRow.Item("userunkId").ToString)
    '                        .SubItems.Add(dtRow.Item("isvoid").ToString)
    '                        .SubItems.Add(dtRow.Item("voiduserunkId").ToString)
    '                        .SubItems.Add(dtRow.Item("voiddatetime").ToString)
    '                        .SubItems.Add(dtRow.Item("formula").ToString)
    '                        '.SubItems.Add(dtRow.Item("AUD").ToString)
    '                        .SubItems.Add(dtRow.Item("GUID").ToString)
    '                        .SubItems.Add(dtRow.Item("isdeduct").ToString)
    '                    End With
    '                    lvEarningDeduction.Items.Add(lvItems)
    '                End If
    '            Next

    '            If lvEarningDeduction.Items.Count > 11 Then
    '                lvEarningDeduction.Columns(3).Width = 150 - 18
    '            Else
    '                lvEarningDeduction.Columns(3).Width = 150
    '            End If
    '        End With
    '    Catch ex As Exception
    '        DisplayError.Show("-1", ex.Message, "FillItemList", mstrModuleName)
    '    End Try
    'End Sub

    'Private Sub MergeDataTable(ByVal newTable As DataTable)
    '    Dim drTemp As DataRow() = Nothing
    '    Dim objtranHead As New clsTransactionHead
    '    Dim objCommonMaster As New clsMasterData
    '    Dim dsCalcType As DataSet = Nothing

    '    Try
    '        For i As Integer = 0 To newTable.Rows.Count - 1
    '            With newTable.Rows(i)
    '                drTemp = mdtTran.Select("tranheadunkid = " & CInt(.Item("Id").ToString) & " AND (AUD <> 'D' OR AUD IS NULL) ")

    '                objtranHead._Tranheadunkid = CInt(.Item("Id").ToString)

    '                Dim drRow As DataRow
    '                drRow = mdtTran.NewRow

    '                If drTemp.Length > 0 Then
    '                    drRow.Item("edunkid") = CInt(drTemp(0)("edunkid"))
    '                Else
    '                    drRow.Item("edunkid") = -1
    '                End If
    '                drRow.Item("batchtranunkid") = CInt(.Item("batchtransactionunkid").ToString)
    '                drRow.Item("tranheadunkid") = objtranHead._Tranheadunkid
    '                drRow.Item("tranhead") = objtranHead._Trnheadname

    '                drRow.Item("trnheadtypeID") = objtranHead._Trnheadtype_Id
    '                objTranHeadType.SelectedValue = objtranHead._Trnheadtype_Id
    '                drRow.Item("trnheadtype") = objTranHeadType.Text
    '                objTranHeadType.SelectedValue = 0

    '                drRow.Item("typeofId") = objtranHead._Typeof_id
    '                drRow.Item("calctypeId") = objtranHead._Calctype_Id
    '                Select Case objtranHead._Typeof_id
    '                    Case Is > 1
    '                        dsCalcType = objCommonMaster.getComboListCalcType("CalcType", 2)
    '                    Case Else
    '                        dsCalcType = objCommonMaster.getComboListCalcType("CalcType", objtranHead._Typeof_id)
    '                End Select
    '                With objCalcType
    '                    .ValueMember = "Id"
    '                    .DisplayMember = "Name"
    '                    .DataSource = dsCalcType.Tables("CalcType")
    '                    If .Items.Count > 0 Then .SelectedIndex = 0
    '                End With
    '                objCalcType.SelectedValue = objtranHead._Calctype_Id
    '                drRow.Item("calctype") = objCalcType.Text
    '                objCalcType.SelectedValue = 0

    '                If objtranHead._Computeon_Id > 0 Then
    '                    drRow.Item("computeonid") = objtranHead._Computeon_Id
    '                    objComputeOn.SelectedValue = objtranHead._Computeon_Id
    '                    drRow.Item("computeon") = objComputeOn.Text
    '                    objComputeOn.SelectedValue = 0
    '                End If
    '                drRow.Item("formula") = objtranHead._Formula

    '                If objtranHead._Trnheadtype_Id = enTranHeadType.EarningForEmployees Then
    '                    drRow.Item("isdeduct") = False
    '                ElseIf objtranHead._Trnheadtype_Id = enTranHeadType.EmployersStatutoryContributions Then
    '                    drRow.Item("isdeduct") = Nothing
    '                Else
    '                    drRow.Item("isdeduct") = True
    '                End If

    '                If cdec(.Item("Amount").ToString) > 0 Then
    '                    drRow.Item("amount") = cdec(.Item("Amount").ToString)
    '                Else
    '                    drRow.Item("amount") = DBNull.Value
    '                End If

    '                drRow.Item("currencyunkId") = 0
    '                drRow.Item("vendorunkId") = 0
    '                drRow.Item("userunkId") = Nothing
    '                drRow.Item("isvoid") = Nothing
    '                drRow.Item("voiduserunkId") = Nothing
    '                drRow.Item("voiddatetime") = Nothing
    '                If drTemp.Length > 0 Then
    '                    If IsDBNull(drTemp(0)("AUD")) Then
    '                        drRow.Item("AUD") = "U"
    '                    Else
    '                        drRow.Item("AUD") = "A"
    '                    End If
    '                Else
    '                    drRow.Item("AUD") = "A"
    '                End If

    '                drRow.Item("GUID") = Guid.NewGuid.ToString

    '                If drTemp.Length > 0 Then
    '                    mdtTran.Rows.Remove(drTemp(0))
    '                End If
    '                mdtTran.Rows.Add(drRow)
    '                .AcceptChanges()
    '            End With
    '        Next

    '        'Call FillItemList()
    '        Call ResetItemValue()

    '    Catch ex As Exception
    '        DisplayError.Show("-1", ex.Message, "MergeDataTable", mstrModuleName)
    '    End Try
    'End Sub

    Private Sub ResetItemValue()
        cboTrnHead.Enabled = True
        cboTrnHeadType.Enabled = True
        cboTrnHeadType.SelectedValue = 0
        cboTrnHead.SelectedValue = 0
        txtAmount.Text = Format(0, GUI.fmtCurrency) 'Sohail (01 Dec 2010) -- End
        'Sohail (18 Jan 2012) -- Start
        'TRA - ENHANCEMENT
        cboPeriod.SelectedValue = 0
        txtMedicalRefNo.Text = ""
        'Sohail (18 Jan 2012) -- End
    End Sub

    Private Sub CheckAllEmployee(ByVal blnCheckAll As Boolean)
        Try
            'Sohail (18 Jul 2017) -- Start
            'Enhancement - 69.1 - Day wise total days worked and hours worked functions.
            'For Each lvItem As ListViewItem In lvEmployeeList.Items
            '    RemoveHandler lvEmployeeList.ItemChecked, AddressOf lvEmployeeList_ItemChecked 'Sohail (03 Nov 2010)
            '    lvItem.Checked = blnCheckAll
            '    AddHandler lvEmployeeList.ItemChecked, AddressOf lvEmployeeList_ItemChecked 'Sohail (03 Nov 2010)
            'Next
            If dvEmployee IsNot Nothing Then
                For Each dr As DataRowView In dvEmployee
                    dr.Item("IsChecked") = blnCheckAll
                    dr.EndEdit()
            Next
                dvEmployee.ToTable.AcceptChanges()

                Dim drRow As DataRow() = dvEmployee.Table.Select("IsChecked = 1")
                mintCount = drRow.Length
                objlblEmpCount.Text = "( " & mintCount.ToString & " / " & mintTotalEmployee.ToString & " )"
            End If
            'Sohail (18 Jul 2017) -- End
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "CheckAllEmployee", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Private Functions "
    Private Function IsValidate() As Boolean
        Dim strMsg As String = ""
        'Dim dblAmount As Double = 0
        'Sohail (14 Apr 2011) -- Start
        Dim dsList As DataSet
        Dim objTrnFormula As New clsTranheadFormulaTran
        Dim objTranHead As New clsTransactionHead
        Dim strHeadName As String = ""
        'Sohail (14 Apr 2011) -- End

        Try
            'Sohail (03 Sep 2010) -- Start
            'If txtAmount.Text.Length > 0 Then
            '    dblAmount = cdec(txtAmount.Text)
            'End If
            'Sohail (03 Sep 2010) -- End
            'Sohail (03 Nov 2010)-Start
            'Issue : Mr. Rutta wanted this to be optional to validation has been removed.
            'If CInt(cboTrnHeadType.SelectedValue) <= 0 Then
            '    strMsg = Language.getMessage(mstrModuleName, 1, "Please Select Transaction Head Type.")
            '    eZeeMsgBox.Show(strMsg, enMsgBoxStyle.Information)
            '    cboTrnHeadType.Focus()
            '    Exit Function
            'ElseIf CInt(cboTypeOf.SelectedValue) <= 0 Then
            '    strMsg = Language.getMessage(mstrModuleName, 2, "Please Select Transaction Head Type Of.")
            '    eZeeMsgBox.Show(strMsg, enMsgBoxStyle.Information)
            '    cboTypeOf.Focus()
            '    Exit Function
            'Else
            'Sohail (03 Nov 2010)-End
            'Sohail (13 Jan 2012) -- Start
            'TRA - ENHANCEMENT
            If CInt(cboPeriod.SelectedValue) <= 0 Then
                strMsg = Language.getMessage(mstrModuleName, 10, "Please select Period. Period is mandatory information.")
                eZeeMsgBox.Show(strMsg, enMsgBoxStyle.Information)
                cboPeriod.Focus()
                Exit Function
            End If
            'Sohail (13 Jan 2012) -- End

            If CInt(cboTrnHead.SelectedValue) <= 0 Then
                strMsg = Language.getMessage(mstrModuleName, 1, "Please Select Transaction Head.")
                eZeeMsgBox.Show(strMsg, enMsgBoxStyle.Information)
                cboTrnHead.Focus()
                Exit Function
                'Anjan (06 Jun 2011)-Start
                'Issue : Rutta wanted on allow -ve ,0 amount on ED for flat rate. 
                'ElseIf txtAmount.Enabled = True And txtAmount.Decimal <= 0 Then 'Sohail (03 Sep 2010)
                'strMsg = Language.getMessage(mstrModuleName, 4, "Sorry, Amount can not be Zero.")
                'eZeeMsgBox.Show(strMsg, enMsgBoxStyle.Information)
                'txtAmount.Focus()
                'Exit Function
                'Anjan (06 Jun 2011)-End
                'ElseIf CInt(cboCurrencyCountry.SelectedValue) <= 0 Then
                '    strMsg = Language.getMessage(mstrModuleName, 5, "Please Select Currency.")
                '    eZeeMsgBox.Show(strMsg, enMsgBoxStyle.Information)
                '    cboCurrencyCountry.Focus()
                '    Exit Function
                'Sohail (18 Jul 2017) -- Start
                'Enhancement - 69.1 - Day wise total days worked and hours worked functions.
                'ElseIf lvEmployeeList.CheckedItems.Count = 0 Then
            ElseIf dvEmployee.Table.Select("IsChecked = 1 ").Length <= 0 Then
                'Sohail (18 Jul 2017) -- End
                strMsg = Language.getMessage(mstrModuleName, 9, "Please select atleast one Employee from list.")
                eZeeMsgBox.Show(strMsg, enMsgBoxStyle.Information)
                'Sohail (18 Jul 2017) -- Start
                'Enhancement - 69.1 - Day wise total days worked and hours worked functions.
                'lvEmployeeList.Focus()
                dgEmployee.Focus()
                'Sohail (18 Jul 2017) -- End
                Exit Function
                'ElseIf mdtTran.Rows.Count = 0 Then
                '    strMsg = Language.getMessage(mstrModuleName, 7, "Formula is not set on this Transaction Head. Please set formula from Transaction Head Form.")
                '    eZeeMsgBox.Show(strMsg, enMsgBoxStyle.Information)
                '    cboTrnHead.Focus()
                '    Exit Sub
            End If

            'Sohail (09 Nov 2013) -- Start
            'TRA - ENHANCEMENT
            If dtpCumulativeStartDate.Checked = True AndAlso dtpCumulativeStartDate.Value.Date > mdtPayPeriodEndDate.Date Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 14, "Sorry, Cumulative Start Date should not be greater than.") & " " & mdtPayPeriodEndDate.ToShortDateString, enMsgBoxStyle.Information)
                Exit Function
            End If
            If dtpStopDate.Checked = True AndAlso dtpStopDate.Value.Date < mdtPayPeriodStartDate.Date Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 15, "Sorry, Stop Date should not be less than.") & " " & mdtPayPeriodStartDate.ToShortDateString, enMsgBoxStyle.Information)
                Exit Function
            End If
            'Sohail (09 Nov 2013) -- End
            'Sohail (24 Aug 2019) -- Start
            'NMB Payroll UAT # TC016 - 76.1 - System should be able to accept future amendments (Future period) and prorate. It should be possible to set start date of the specific transaction and prorate based on the start date. It should also be possible to set start date of the future period.
            If dtpEDStartDate.Checked = True AndAlso (dtpEDStartDate.Value.Date < mdtPayPeriodStartDate.Date OrElse dtpEDStartDate.Value.Date > mdtPayPeriodEndDate.Date) Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 17, "Sorry, E. D. Start Date should be in between selected period start date and end date."), enMsgBoxStyle.Information)
                Exit Try
            End If
            If dtpStopDate.Checked = True AndAlso dtpEDStartDate.Checked = True AndAlso eZeeDate.convertDate(dtpStopDate.Value.Date) < eZeeDate.convertDate(dtpEDStartDate.Value.Date) Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 18, "Sorry, E. D. Start Date should be greater than stop date."), enMsgBoxStyle.Information)
                Exit Try
            End If
            'Sohail (24 Aug 2019) -- End

            'Sohail (14 Apr 2011) -- Start
            dsList = objTrnFormula.getFlateRateHeadFromFormula(CInt(cboTrnHead.SelectedValue), "TranHead")
            For Each dsRow As DataRow In dsList.Tables("TranHead").Rows
                With dsRow

                    'Sohail (16 May 2012) -- Start
                    'TRA - ENHANCEMENT
                    'Do not prompt message if default value has been set on transaction head formula (other tham Computed Value)
                    If CInt(dsRow.Item("defaultvalue_id")) <> enTranHeadFormula_DefaultValue.COMPUTED_VALUE Then Continue For
                    'Sohail (16 May 2012) -- End

                    'Sohail (17 Jan 2012) -- Start
                    'TRA - ENHANCEMENT
                    'Sohail (21 Aug 2015) -- Start
                    'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                    'If objED.isExist(CInt(cboEmployee.SelectedValue), CInt(.Item("tranheadunkid").ToString), , CInt(cboPeriod.SelectedValue)) = False Then
                    If objED.isExist(FinancialYear._Object._DatabaseName, CInt(cboEmployee.SelectedValue), CInt(.Item("tranheadunkid").ToString), , CInt(cboPeriod.SelectedValue)) = False Then
                        'Sohail (21 Aug 2015) -- End
                        'If objED.isExist(CInt(cboEmployee.SelectedValue), CInt(.Item("tranheadunkid").ToString)) = False Then
                        'Sohail (17 Jan 2012) -- End
                        'Sohail (21 Aug 2015) -- Start
                        'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                        'objTranHead._Tranheadunkid = CInt(.Item("tranheadunkid").ToString)
                        objTranHead._Tranheadunkid(FinancialYear._Object._DatabaseName) = CInt(.Item("tranheadunkid").ToString)
                        'Sohail (21 Aug 2015) -- End
                        strHeadName = objTranHead._Trnheadname

                        strMsg = Language.getMessage(mstrModuleName, 2, "The Formula of this Transaction Head contains ") & strHeadName & Language.getMessage(mstrModuleName, 3, " Tranasaction Head (type of Flat Rate).") & vbCrLf & Language.getMessage(mstrModuleName, 4, "If you do not assign ") & strHeadName & Language.getMessage(mstrModuleName, 5, " Transaction Head to selected employee, then it's value will be considered as Zero.") & vbCrLf & vbCrLf & Language.getMessage(mstrModuleName, 6, "Do you want to proceed?")
                        If eZeeMsgBox.Show(strMsg, CType(enMsgBoxStyle.Question + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.No Then
                            Exit Function
                        End If
                    End If
                End With
            Next
            'Sohail (14 Apr 2011) -- End

            'Sohail (26 May 2011) -- Start
            objTranHead = New clsTransactionHead
            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'objTranHead._Tranheadunkid = CInt(cboTrnHead.SelectedValue)
            objTranHead._Tranheadunkid(FinancialYear._Object._DatabaseName) = CInt(cboTrnHead.SelectedValue)
            'Sohail (21 Aug 2015) -- End
            If objTranHead._Formulaid.Trim = "" AndAlso (objTranHead._Calctype_Id = enCalcType.AsComputedValue OrElse objTranHead._Calctype_Id = enCalcType.AsComputedOnWithINEXCESSOFTaxSlab) Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 7, "Formula is not set on this Transaction Head. Please set formula from Transaction Head Form."), enMsgBoxStyle.Information)
                Return False
            End If
            'Sohail (26 May 2011) -- End

            'Sohail (18 Jan 2012) -- Start
            'TRA - ENHANCEMENT
            If chkCopyPreviousEDSlab.Checked = True Then
                If (eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 11, "You have ticked Copy Previous ED Slab. This will copy Previous Period Slab.") & vbCrLf & vbCrLf & Language.getMessage(mstrModuleName, 13, "Do you want to continue?"), CType(enMsgBoxStyle.Question + enMsgBoxStyle.YesNo, enMsgBoxStyle))) = Windows.Forms.DialogResult.No Then
                    Return False
                End If
            End If
            'Sohail (18 Jan 2012) -- End

            Return True
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "IsValidate", mstrModuleName)
            'Sohail (14 Apr 2011) -- Start
        Finally
            objTrnFormula = Nothing
            objTranHead = Nothing
            dsList = Nothing
            'Sohail (14 Apr 2011) -- End
        End Try
    End Function

    'Sohail (18 Jul 2017) -- Start
    'Enhancement - 69.1 - Day wise total days worked and hours worked functions.
    Private Sub SetCheckBoxValue()
        Try

            RemoveHandler objchkSelectAll.CheckedChanged, AddressOf chkSelectAll_CheckedChanged

            Dim blnIsChecked As Boolean = Convert.ToBoolean(dgEmployee.CurrentRow.Cells(objdgcolhCheck.Index).Value)

            If blnIsChecked = True Then
                mintCount += 1
            Else
                mintCount -= 1
            End If

            objlblEmpCount.Text = "( " & mintCount.ToString & " / " & mintTotalEmployee.ToString & " )"

            If mintCount <= 0 Then
                objchkSelectAll.CheckState = CheckState.Unchecked
            ElseIf mintCount < dgEmployee.Rows.Count Then
                objchkSelectAll.CheckState = CheckState.Indeterminate
            ElseIf mintCount = dgEmployee.Rows.Count Then
                objchkSelectAll.CheckState = CheckState.Checked
            End If

            AddHandler objchkSelectAll.CheckedChanged, AddressOf chkSelectAll_CheckedChanged
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetCheckBoxValue", mstrModuleName)
        End Try

    End Sub

    Private Sub SetDefaultSearchEmpText()
        Try
            mstrSearchEmpText = lblSearchEmp.Text
            With txtSearchEmp
                .ForeColor = Color.Gray
                .Text = mstrSearchEmpText
                .Font = New Font(Me.Font.FontFamily, Me.Font.Size, FontStyle.Italic)
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "", mstrModuleName)
        End Try
    End Sub
    'Sohail (18 Jul 2017) -- End

#End Region

#Region " Form's Events "

    Private Sub frmEmployeeEarningDeduction_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        Try
            objED = Nothing
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmEmployeeEarningDeduction_FormClosed", mstrModuleName)
        End Try
    End Sub

    Private Sub frmGlobalAssignEarningDeduction_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown
        Try
            Select Case e.KeyCode
                Case Keys.Return
                    SendKeys.Send("{TAB}")
                Case Keys.S
                    If e.Control = True Then
                        btnSave.PerformClick()
                    End If
            End Select
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmGlobalAssignEarningDeduction_KeyDown", mstrModuleName)
        End Try
    End Sub

    Private Sub frmEmployeeEarningDeduction_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        objED = New clsEarningDeduction
        'Dim objExRate As New clsExchangeRate 'Sohail (01 Dec 2010)
        Try
            Call Set_Logo(Me, gApplicationType)

            'Anjan (02 Sep 2011)-Start
            'Issue : Including Language Settings.
            Language.setLanguage(Me.Name)
            Call OtherSettings()
            'Anjan (02 Sep 2011)-End 

            Call SetColor()
            Call FillCombo()
            '*** Currency Format ***
            'Sohail (01 Dec 2010) -- Start
            'If objExRate.getComboList.Tables(0).Rows.Count > 0 Then
            '    objExRate._ExchangeRateunkid = 1
            '    mstrCurrencyFormat = objExRate._fmtCurrency
            'End If
            mstrCurrencyFormat = GUI.fmtCurrency
            'Sohail (01 Dec 2010) -- End
            If txtAmount.Text.Length = 0 Then txtAmount.Text = "0"
            txtAmount.Text = Format(txtAmount.Decimal, mstrCurrencyFormat) 'Sohail (01 Dec 2010), 'Sohail (11 May 2011)

            'Sohail (18 Jul 2017) -- Start
            'Enhancement - 69.1 - Day wise total days worked and hours worked functions.
            Call SetDefaultSearchEmpText()
            'Sohail (18 Jul 2017) -- End

            'Call FillList() 'Sohail (11 Sep 2010)
            Call ResetItemValue()

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmEmployeeEarningDeduction_Load", mstrModuleName)
        Finally
            'objExRate = Nothing 'Sohail (01 Dec 2010)
        End Try
    End Sub
    'Anjan (02 Sep 2011)-Start
    'Issue : Including Language Settings.
    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsEarningDeduction.SetMessages()
            objfrm._Other_ModuleNames = "clsEarningDeduction"
            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub
    'Anjan (02 Sep 2011)-End 

#End Region

#Region " ComboBox's Events "
    Private Sub cboTrnHeadType_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboTrnHeadType.SelectedIndexChanged
        'Dim objTranHead As New clsTransactionHead
        Dim objMaster As New clsMasterData
        Dim dsList As DataSet
        Dim dtTable As DataTable

        Try
            dsList = objMaster.getComboListTypeOf("TypeOf", CInt(cboTrnHeadType.SelectedValue))
            dtTable = New DataView(dsList.Tables("TypeOf"), "Id <>  " & enTypeOf.Salary & "", "", DataViewRowState.CurrentRows).ToTable 'Except Salary Heads 'Sohail (15 Oct 2010)

            With cboTypeOf
                .ValueMember = "Id"
                .DisplayMember = "Name"
                .DataSource = dtTable
                If .Items.Count > 0 Then .SelectedIndex = 0
            End With
            'dsList = objTranHead.getComboList("TranHead", True, CInt(cboTrnHeadType.SelectedValue), enCalcType.FlatRate_Others)
            'With cboTrnHead
            '    .ValueMember = "tranheadunkid"
            '    .DisplayMember = "Name"
            '    .DataSource = dsList.Tables("TranHead")
            '    If .Items.Count > 0 Then .SelectedValue = 0
            'End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboTrnHeadType_SelectedIndexChanged", mstrModuleName)
        Finally
            'objTranHead = Nothing
            objMaster = Nothing
            dsList = Nothing
            dtTable = Nothing
        End Try
    End Sub

    Private Sub cboTypeOf_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboTypeOf.SelectedIndexChanged
        Dim objTranHead As New clsTransactionHead
        Dim dsList As DataSet
        Try
            'Sohail (15 Oct 2010) -- Start
            'dsList = objTranHead.getComboList("TranHead", True, 0, enCalcType.FlatRate_Others, CInt(cboTypeOf.SelectedValue))
            'Sohail (14 Apr 2011) -- Start
            'dsList = objTranHead.getComboList("TranHead", True, CInt(cboTrnHeadType.SelectedValue), enCalcType.FlatRate_Others, CInt(IIf(CInt(cboTypeOf.SelectedValue) > 0, CInt(cboTypeOf.SelectedValue), -1)))
            'Sohail (29 Oct 2012) -- Start
            'TRA - ENHANCEMENT
            'dsList = objTranHead.getComboList("TranHead", True, CInt(cboTrnHeadType.SelectedValue), 0, CInt(IIf(CInt(cboTypeOf.SelectedValue) > 0, CInt(cboTypeOf.SelectedValue), -1)))
            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'dsList = objTranHead.getComboList("TranHead", True, CInt(cboTrnHeadType.SelectedValue), 0, CInt(IIf(CInt(cboTypeOf.SelectedValue) > 0, CInt(cboTypeOf.SelectedValue), -1)), , True)
            'Sohail (22 Mar 2016) -- Start
            'Issue - Salary heads were coming in head list in 58.1.
            'dsList = objTranHead.getComboList(FinancialYear._Object._DatabaseName, "TranHead", True, CInt(cboTrnHeadType.SelectedValue), 0, CInt(IIf(CInt(cboTypeOf.SelectedValue) > 0, CInt(cboTypeOf.SelectedValue), -1)), , True)
            dsList = objTranHead.getComboList(FinancialYear._Object._DatabaseName, "TranHead", True, CInt(cboTrnHeadType.SelectedValue), 0, CInt(IIf(CInt(cboTypeOf.SelectedValue) > 0, CInt(cboTypeOf.SelectedValue), -1)), , True, "Typeof_id <> " & enTypeOf.Salary & "")
            'Sohail (22 Mar 2016) -- End
            'Sohail (21 Aug 2015) -- End
            'Sohail (29 Oct 2012) -- End
            'Sohail (14 Apr 2011) -- End
            'Sohail (15 Oct 2010) -- End


            With cboTrnHead
                .ValueMember = "tranheadunkid"
                .DisplayMember = "Name"
                .DataSource = dsList.Tables("TranHead")
                If .Items.Count > 0 Then .SelectedValue = 0
            End With

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboTypeOf_SelectedIndexChanged", mstrModuleName)
        Finally
            objTranHead = Nothing
            dsList = Nothing
        End Try
    End Sub

    Private Sub cboTrnHead_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboTrnHead.SelectedIndexChanged
        Dim objTranHead As New clsTransactionHead
        Try
            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'objTranHead._Tranheadunkid = CInt(cboTrnHead.SelectedValue)
            objTranHead._Tranheadunkid(FinancialYear._Object._DatabaseName) = CInt(cboTrnHead.SelectedValue)
            'Sohail (21 Aug 2015) -- End
            'Sohail (15 Oct 2010) -- Start
            'If objTranHead._Calctype_Id = enCalcType.AsComputedValue Or _
            '    objTranHead._Calctype_Id = enCalcType.AsComputedOnWithINEXCESSOFTaxSlab Or _
            '    objTranHead._Calctype_Id = enCalcType.AsUserDefinedValue Or _
            '    objTranHead._Calctype_Id = enCalcType.DEFINED_SALARY Or _
            '    objTranHead._Calctype_Id = enCalcType.OnAttendance Or _
            '    objTranHead._Calctype_Id = enCalcType.OnHourWorked Then

            '    txtAmount.Enabled = False
            '    txtAmount.BackColor = GUI.ColorOptional
            '    txtAmount.Text = "0.0"
            'Else
            '    txtAmount.Enabled = True
            '    txtAmount.BackColor = GUI.ColorComp
            'End If
            If objTranHead._Calctype_Id = enCalcType.FlatRate_Others Then
                txtAmount.Enabled = True
                txtAmount.BackColor = GUI.ColorComp
            Else
                txtAmount.Enabled = False
                txtAmount.BackColor = GUI.ColorOptional
                txtAmount.Text = "0.0"
            End If
            'Sohail (15 Oct 2010) -- End
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboTrnHead_SelectedIndexChanged", mstrModuleName)
        Finally
            objTranHead = Nothing
        End Try
    End Sub

    Private Sub cboPeriod_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboPeriod.SelectedIndexChanged
        Dim objPeriod As New clscommom_period_Tran
        Dim dsList As DataSet = Nothing
        Try
            If CInt(cboPeriod.SelectedValue) > 0 Then
                'Sohail (21 Aug 2015) -- Start
                'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                'objPeriod._Periodunkid = CInt(cboPeriod.SelectedValue)
                objPeriod._Periodunkid(FinancialYear._Object._DatabaseName) = CInt(cboPeriod.SelectedValue)
                'Sohail (21 Aug 2015) -- End
                mdtPayPeriodStartDate = objPeriod._Start_Date
                mdtPayPeriodEndDate = objPeriod._End_Date
                Call FillCombo_Employee() 'Sohail (03 Mar 2012)
                Call FillList()
                'Sohail (21 Aug 2015) -- Start
                'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            Else
                mdtPayPeriodStartDate = eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate)
                mdtPayPeriodEndDate = eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate)
                'Sohail (21 Aug 2015) -- End
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboPeriod_SelectedIndexChanged", mstrModuleName)
        Finally
            objPeriod = Nothing
            dsList = Nothing
        End Try
    End Sub

    'Private Sub cboCurrencyCountry_SelectedValueChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboCurrencyCountry.SelectedValueChanged
    '    Dim objExRate As New clsExchangeRate
    '    Try
    '        objExRate._ExchangeRateunkid = CInt(cboCurrencyCountry.SelectedValue)
    '        objlblCurrencyName.Text = objExRate._Currency_Sign
    '        mstrCurrencyFormat = objExRate._fmtCurrency
    '        If txtAmount.Text.Length = 0 Then txtAmount.Text = "0"
    '        txtAmount.Text = Format(cdec(txtAmount.Text), mstrCurrencyFormat)
    '    Catch ex As Exception
    '        DisplayError.Show("-1", ex.Message, "cboCurrencyCountry_SelectedValueChanged", mstrModuleName)
    '    Finally
    '        objExRate = Nothing
    '    End Try
    'End Sub
#End Region

#Region " Button's Events "
    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Me.Close()
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "btnClose_Click", mstrModuleName)
        End Try
    End Sub

    'Private Sub btnAdd_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
    '    Dim objTranHead As New clsTransactionHead
    '    Dim objCommonMaster As New clsMasterData
    '    Dim dsCalcType As DataSet
    '    Dim strMsg As String = ""

    '    Try

    '        If IsValidate() = False Then Exit Sub

    '        Dim drTemp As DataRow()
    '        drTemp = mdtTran.Select("tranheadunkid = " & CInt(cboTrnHead.SelectedValue) & " AND (AUD <> 'D' OR AUD IS NULL)")
    '        If drTemp.Length > 0 Then
    '            strMsg = Language.getMessage(mstrModuleName, 8, "Sorry, This Transaction Head is already exist.")
    '            eZeeMsgBox.Show(strMsg, enMsgBoxStyle.Information)
    '            cboTrnHead.Focus()
    '            Exit Sub
    '        End If

    '        Dim dtRow As DataRow
    '        dtRow = mdtTran.NewRow()
    '        With dtRow
    '            .Item("edunkid") = -1
    '            .Item("batchtranunkid") = -1
    '            .Item("tranheadunkid") = CInt(cboTrnHead.SelectedValue)

    '            objTranHead._Tranheadunkid = CInt(cboTrnHead.SelectedValue)

    '            .Item("tranhead") = objTranHead._Trnheadname

    '            .Item("trnheadtypeID") = objTranHead._Trnheadtype_Id
    '            objTranHeadType.SelectedValue = objTranHead._Trnheadtype_Id
    '            .Item("trnheadtype") = objTranHeadType.Text
    '            objTranHeadType.SelectedValue = 0

    '            .Item("typeofId") = objTranHead._Typeof_id

    '            .Item("calctypeId") = objTranHead._Calctype_Id
    '            Select Case objTranHead._Typeof_id
    '                Case Is > 1
    '                    dsCalcType = objCommonMaster.getComboListCalcType("CalcType", 2)
    '                Case Else
    '                    dsCalcType = objCommonMaster.getComboListCalcType("CalcType", objTranHead._Typeof_id)
    '            End Select

    '            With objCalcType
    '                .ValueMember = "Id"
    '                .DisplayMember = "Name"
    '                .DataSource = dsCalcType.Tables("CalcType")
    '                If .Items.Count > 0 Then .SelectedIndex = 0
    '            End With
    '            objCalcType.SelectedValue = objTranHead._Calctype_Id
    '            .Item("calctype") = objCalcType.Text
    '            objCalcType.SelectedValue = 0

    '            If objTranHead._Computeon_Id > 0 Then
    '                .Item("computeonid") = objTranHead._Computeon_Id
    '                objComputeOn.SelectedValue = objTranHead._Computeon_Id
    '                .Item("computeon") = objComputeOn.Text
    '                objComputeOn.SelectedValue = 0
    '            End If

    '            .Item("formula") = objTranHead._Formula
    '            If objTranHead._Trnheadtype_Id = enTranHeadType.EarningForEmployees Then
    '                .Item("isdeduct") = False
    '            ElseIf objTranHead._Trnheadtype_Id = enTranHeadType.EmployersStatutoryContributions Then
    '                .Item("isdeduct") = Nothing
    '            Else
    '                .Item("isdeduct") = True
    '            End If
    '            If txtAmount.Text.Length > 0 Then
    '                .Item("amount") = cdec(txtAmount.Text)
    '            Else
    '                .Item("amount") = Nothing
    '            End If
    '            .Item("currencyunkId") = CInt(cboCurrencyCountry.SelectedValue)
    '            .Item("vendorunkId") = CInt(cboVendor.SelectedValue)
    '            .Item("userunkId") = Nothing
    '            .Item("isvoid") = False
    '            .Item("voiduserunkId") = -1
    '            .Item("voiddatetime") = Nothing
    '            .Item("AUD") = "A"
    '            .Item("GUID") = Guid.NewGuid.ToString
    '        End With
    '        mdtTran.Rows.Add(dtRow)

    '        'Call FillItemList()
    '        Call ResetItemValue()

    '    Catch ex As Exception
    '        DisplayError.Show("-1", ex.Message, "btnAdd_Click", mstrModuleName)
    '    Finally
    '        objTranHead = Nothing
    '        objCommonMaster = Nothing
    '    End Try
    'End Sub

    'Private Sub btnEdit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
    '    Dim objTranHead As New clsTransactionHead
    '    Dim objCommonMaster As New clsMasterData
    '    Dim dsCalcType As DataSet

    '    Try
    '        If lvEarningDeduction.SelectedItems.Count <= 0 Then Exit Sub

    '        If IsValidate() = False Then Exit Sub

    '        If mintItemIndex > -1 Then
    '            Dim drTemp As DataRow()
    '            If CInt(lvEarningDeduction.Items(mintItemIndex).Tag) = -1 Then
    '                drTemp = mdtTran.Select("GUID = '" & lvEarningDeduction.Items(mintItemIndex).SubItems(colhGUID.Index).Text & "'")
    '            Else
    '                drTemp = mdtTran.Select("edunkid = " & CInt(lvEarningDeduction.Items(mintItemIndex).Tag) & " AND (AUD <> 'D' OR AUD IS NULL)")
    '            End If
    '            If drTemp.Length > 0 Then

    '                With drTemp(0)
    '                    '.Item("edunkid") = CInt(lvEarningDeduction.Items(mintItemIndex).Tag)
    '                    '.Item("batchtranunkid") = -1
    '                    objTranHead._Tranheadunkid = CInt(cboTrnHead.SelectedValue)
    '                    .Item("tranheadunkid") = objTranHead._Tranheadunkid
    '                    .Item("tranhead") = objTranHead._Trnheadname
    '                    .Item("trnheadtypeID") = objTranHead._Trnheadtype_Id
    '                    objTranHeadType.SelectedValue = objTranHead._Trnheadtype_Id
    '                    .Item("trnheadtype") = objTranHeadType.Text
    '                    objTranHeadType.SelectedValue = 0

    '                    .Item("typeofId") = objTranHead._Typeof_id
    '                    .Item("calctypeId") = objTranHead._Calctype_Id
    '                    Select Case objTranHead._Typeof_id
    '                        Case Is > 1
    '                            dsCalcType = objCommonMaster.getComboListCalcType("CalcType", 2)
    '                        Case Else
    '                            dsCalcType = objCommonMaster.getComboListCalcType("CalcType", objTranHead._Typeof_id)
    '                    End Select
    '                    With objCalcType
    '                        .ValueMember = "Id"
    '                        .DisplayMember = "Name"
    '                        .DataSource = dsCalcType.Tables("CalcType")
    '                        If .Items.Count > 0 Then .SelectedIndex = 0
    '                    End With
    '                    objCalcType.SelectedValue = objTranHead._Calctype_Id
    '                    .Item("calctype") = objCalcType.Text
    '                    objCalcType.SelectedValue = 0

    '                    If objTranHead._Computeon_Id > 0 Then
    '                        .Item("computeonid") = objTranHead._Computeon_Id
    '                        objComputeOn.SelectedValue = objTranHead._Computeon_Id
    '                        .Item("computeon") = objComputeOn.Text
    '                        objComputeOn.SelectedValue = 0
    '                    End If

    '                    .Item("formula") = objTranHead._Formula
    '                    If objTranHead._Trnheadtype_Id = enTranHeadType.EarningForEmployees Then
    '                        .Item("isdeduct") = False
    '                    ElseIf objTranHead._Trnheadtype_Id = enTranHeadType.EmployersStatutoryContributions Then
    '                        .Item("isdeduct") = Nothing
    '                    Else
    '                        .Item("isdeduct") = True
    '                    End If
    '                    .Item("amount") = Nothing
    '                    If txtAmount.Text.Length > 0 Then
    '                        .Item("amount") = Format(cdec(txtAmount.Text), "#0.00")
    '                    End If

    '                    .Item("currencyunkId") = CInt(cboCurrencyCountry.SelectedValue)
    '                    .Item("vendorunkId") = CInt(cboVendor.SelectedValue)
    '                    .Item("userunkId") = Nothing
    '                    .Item("isvoid") = False
    '                    .Item("voiduserunkId") = -1
    '                    .Item("voiddatetime") = Nothing
    '                    .Item("GUID") = Guid.NewGuid.ToString
    '                    If IsDBNull(.Item("AUD")) Or .Item("AUD").ToString.Trim = "" Then
    '                        .Item("AUD") = "U"
    '                    End If
    '                    .AcceptChanges()
    '                End With
    '                Call FillItemList()
    '            End If
    '        End If
    '        Call ResetItemValue()
    '    Catch ex As Exception
    '        DisplayError.Show("-1", ex.Message, "btnEdit_Click", mstrModuleName)
    '    Finally
    '        objTranHead = Nothing
    '    End Try
    'End Sub

    'Private Sub btnDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
    '    Try
    '        If lvEarningDeduction.SelectedItems.Count > 0 And _
    '             mintItemIndex > -1 Then

    '            Dim drTemp As DataRow()
    '            If CInt(lvEarningDeduction.Items(mintItemIndex).Tag) = -1 Then
    '                drTemp = mdtTran.Select("guid = '" & lvEarningDeduction.Items(mintItemIndex).SubItems(colhGUID.Index).Text & "'")
    '            Else
    '                drTemp = mdtTran.Select("edunkid = " & CInt(lvEarningDeduction.Items(mintItemIndex).Tag) & " AND (AUD <> 'D' OR AUD IS NULL)")
    '            End If

    '            If drTemp.Length > 0 Then
    '                drTemp(0).Item("AUD") = "D"
    '                Call FillItemList()
    '                Call ResetItemValue()
    '            End If
    '        End If
    '    Catch ex As Exception
    '        DisplayError.Show("-1", ex.Message, "btnEdit_Click", mstrModuleName)
    '    End Try
    'End Sub

    Private Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Dim strList As String = ""
        Dim strMsg As String = ""

        'Hemant (20 July 2018) -- Start
        'Support Issue # : 0002390: FINCA UGANDA :Earning and deduction not Comming on July period due to july was wrongly configured during closure of june period
        'Dim decAmount As Decimal = 0 'Sohail (11 May 2011)
        'Hemant (20 July 2018) -- End
        Dim objTnA As New clsTnALeaveTran 'Sohail (16 Oct 2012)

        Try

            If IsValidate() = False Then Exit Sub
            Cursor.Current = Cursors.WaitCursor 'Sohail (01 Dec 2010)

            'Sohail (11 Sep 2010) -- Start
            'If txtAmount.Text.Length > 0 Then
            '    decAmount = CDec(txtAmount.Text)
            'End If
            'Hemant (20 July 2018) -- Start
            'Support Issue # : 0002390: FINCA UGANDA :Earning and deduction not Comming on July period due to july was wrongly configured during closure of june period
            'decAmount = txtAmount.Decimal 'Sohail (11 May 2011)
            'Hemant (20 July 2018) -- End

            'Sohail (11 Sep 2010) -- End


            'Sohail (09 Nov 2013) -- Start
            'TRA - ENHANCEMENT
            'For i As Integer = 0 To lvEmployeeList.CheckedItems.Count - 1
            '    If strList.Length <= 0 Then
            '        strList &= CInt(lvEmployeeList.CheckedItems(i).Tag)
            '    Else
            '        strList &= "," & CInt(lvEmployeeList.CheckedItems(i).Tag)
            '    End If
            'Next
            'Sohail (18 Jul 2017) -- Start
            'Enhancement - 69.1 - Day wise total days worked and hours worked functions.
            'Dim allEmp As List(Of String) = (From lv In lvEmployeeList.CheckedItems.Cast(Of ListViewItem)() Select (lv.Tag.ToString)).ToList
            Dim allEmp As List(Of String) = (From p In dvEmployee.Table Where (CBool(p.Item("IsChecked")) = True) Select (p.Item("employeeunkid").ToString)).ToList
            'Sohail (18 Jul 2017) -- End
            strList = String.Join(",", allEmp.ToArray)
            'Sohail (09 Nov 2013) -- End

            'Sohail (16 Oct 2012) -- Start
            'TRA - ENHANCEMENT
            If objTnA.IsPayrollProcessDone(CInt(cboPeriod.SelectedValue), strList, mdtPayPeriodEndDate) = True Then
                Cursor.Current = Cursors.Default
                'Sohail (19 Apr 2019) -- Start
                'Enhancement - 76.1 - Option to open process payroll screen if process payroll is already done message come.
                'eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 12, "Sorry, Process Payroll is already done for last date of selected Period for some of the employees. Please Void Process Payroll first for selected employees to assign earning deduction."), enMsgBoxStyle.Information)
                If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 12, "Sorry, Process Payroll is already done for last date of selected Period for some of the employees. Please Void Process Payroll first for selected employees to assign earning deduction.") & vbCrLf & vbCrLf & Language.getMessage(mstrModuleName, 16, "Do you want to void Payroll?"), CType(enMsgBoxStyle.Information + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.Yes Then
                    Dim objFrm As New frmProcessPayroll
                    objFrm.ShowDialog()
                End If
                'Sohail (19 Apr 2019) -- End
                Exit Try
            End If
            'Sohail (16 Oct 2012) -- End

            'blnFlag = objED.InsertAllByEmployeeList(strList, CInt(cboTrnHead.SelectedValue), CInt(cboCurrencyCountry.SelectedValue), CInt(cboVendor.SelectedValue), _
            '                          -1, dblAmount, radOverwrite.Checked, radAdd.Checked)
            'Sohail (13 Jan 2012) -- Start
            'TRA - ENHANCEMENT
            'blnFlag = objED.InsertAllByEmployeeList(strList, CInt(cboTrnHead.SelectedValue), 0, CInt(cboVendor.SelectedValue), _
            '                                                 -1, decAmount, radOverwrite.Checked, radAdd.Checked)
            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'blnFlag = objED.InsertAllByEmployeeList(strList, CInt(cboTrnHead.SelectedValue), 0, CInt(cboVendor.SelectedValue), _
            '                                     -1, decAmount, radOverwrite.Checked, radAdd.Checked, CInt(cboPeriod.SelectedValue), txtMedicalRefNo.Text.Trim, CDate(IIf(dtpCumulativeStartDate.Checked = True, dtpCumulativeStartDate.Value.Date, Nothing)), CDate(IIf(dtpStopDate.Checked = True, dtpStopDate.Value.Date, Nothing)), True _
            '                                     , chkCopyPreviousEDSlab.Checked _
            '                                     , chkOverwritePrevEDSlabHeads.Checked _
            '                                     )
            'Hemant (20 July 2018) -- Start
            'blnFlag = objED.InsertAllByEmployeeList(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, mdtPayPeriodStartDate, mdtPayPeriodEndDate, ConfigParameter._Object._UserAccessModeSetting, True, ConfigParameter._Object._IsIncludeInactiveEmp, strList, CInt(cboTrnHead.SelectedValue), 0, CInt(cboVendor.SelectedValue), _
            '                                     -1, decAmount, radOverwrite.Checked, radAdd.Checked, CInt(cboPeriod.SelectedValue), txtMedicalRefNo.Text.Trim, CDate(IIf(dtpCumulativeStartDate.Checked = True, dtpCumulativeStartDate.Value.Date, Nothing)), CDate(IIf(dtpStopDate.Checked = True, dtpStopDate.Value.Date, Nothing)), User._Object.Privilege._AllowToApproveEarningDeduction, ConfigParameter._Object._CurrentDateAndTime, True _
            '                                     , chkCopyPreviousEDSlab.Checked _
            '                                     , chkOverwritePrevEDSlabHeads.Checked _
            '                                     , True, "")
            'Sohail (21 Aug 2015) -- End
            '                                    'Sohail (09 Nov 2013) - [dtpCumulativeStartDate.Value.Date, dtpStopDate.Value.Date]
            '                                    'Sohail (07 Mar 2012) - [chkCopyPreviousEDSlab.Checked]
            '                                    'Sohail (26 Jul 2012) - [chkOverwritePrevEDSlabHeads.Checked]
            'Sohail (13 Jan 2012) -- End
            RemoveHandler gfrmMDI.tmrReminder.Tick, AddressOf gfrmMDI.tmrReminder_Tick
            objbgwGlobalAssignED.RunWorkerAsync()
            'Hemant (20 July 2018) -- End

            'Hemant (20 July 2018) -- Start
            'Support Issue # : 0002390: FINCA UGANDA :Earning and deduction not Comming on July period due to july was wrongly configured during closure of june period
            'If blnFlag = False And objED._Message <> "" Then
            '    eZeeMsgBox.Show(objED._Message, enMsgBoxStyle.Information)
            '    Exit Sub
            'End If

            'Cursor.Current = Cursors.Default 'Sohail (01 Dec 2010)
            'Hemant (20 July 2018) -- End
            'Sohail (03 Nov 2010) -- Start
            'Call ResetItemValue()
            'Call CheckAllEmployee(False)

            'mblnCancel = False
            'btnClose.PerformClick()
            'Hemant (20 July 2018) -- Start
            'Support Issue # : 0002390: FINCA UGANDA :Earning and deduction not Comming on July period due to july was wrongly configured during closure of june period
            'If blnFlag Then
            '    mblnCancel = False
            '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 8, "Process completed successfully."), enMsgBoxStyle.Information)
            '    If menAction = enAction.ADD_CONTINUE Then
            '        Call ResetItemValue()
            '        Call CheckAllEmployee(False)
            '        cboTrnHead.Focus()
            '    Else
            '        Me.Close()
            '    End If
            'End If
            'Hemant (20 July 2018) -- End
            'Sohail (03 Nov 2010) -- End            

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnSave_Click", mstrModuleName)
			'Hemant (20 July 2018) -- Start
			'Support Issue # : 0002390: FINCA UGANDA :Earning and deduction not Comming on July period due to july was wrongly configured during closure of june period
			RemoveHandler gfrmMDI.tmrReminder.Tick, AddressOf gfrmMDI.tmrReminder_Tick
            AddHandler gfrmMDI.tmrReminder.Tick, AddressOf gfrmMDI.tmrReminder_Tick
			'Hemant (20 July 2018) -- End
            
        End Try
    End Sub

    'Private Sub btnBatchTransaction_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
    '    Dim objBatchEntry As New frmBatchEntry
    '    Try
    '        'If objBatchEntry.DisplayDialog = True Then
    '        '    Call MergeDataTable(objBatchEntry.DataSource)
    '        'End If
    '    Catch ex As Exception
    '        DisplayError.Show("-1", ex.Message, "btnBatchTransaction_Click", mstrModuleName)
    '    Finally
    '        objBatchEntry = Nothing
    '    End Try
    'End Sub

#End Region

#Region " Listview's Events "
    'Private Sub lvEarningDeduction_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
    '    Try
    '        If lvEarningDeduction.SelectedItems.Count > 0 Then
    '            mintItemIndex = lvEarningDeduction.SelectedItems(0).Index

    '            cboTrnHeadType.SelectedValue = CInt(lvEarningDeduction.Items(mintItemIndex).SubItems(colhHeadTypeID.Index).Text)
    '            cboTrnHead.SelectedValue = CInt(lvEarningDeduction.Items(mintItemIndex).SubItems(colhTrnHeadunkid.Index).Text)
    '            txtAmount.Text = lvEarningDeduction.Items(mintItemIndex).SubItems(colhAmount.Index).Text
    '            cboVendor.SelectedValue = CInt(lvEarningDeduction.Items(mintItemIndex).SubItems(colhVendorunkid.Index).Text)
    '            cboCurrencyCountry.SelectedValue = CInt(lvEarningDeduction.Items(mintItemIndex).SubItems(colhCurrencyunkid.Index).Text)
    '            cboTrnHead.Enabled = False
    '            cboTrnHeadType.Enabled = False
    '        End If
    '    Catch ex As Exception
    '        DisplayError.Show("-1", ex.Message, "lvEarningDeduction_SelectedIndexChanged", mstrModuleName)
    '    End Try
    'End Sub

    'Private Sub lvEmployeeList_ItemCheck(ByVal sender As Object, ByVal e As System.Windows.Forms.ItemCheckEventArgs) Handles lvEmployeeList.ItemCheck
    '    Try
    '        If lvEmployeeList.SelectedItems.Count = 0 Then Exit Sub

    '        If e.Index = lvEmployeeList.SelectedItems(0).Index Then
    '            e.NewValue = CheckState.Checked
    '        End If
    '    Catch ex As Exception
    '        DisplayError.Show("-1", ex.Message, "lvEmployeeList_ItemCheck", mstrModuleName)
    '    End Try
    'End Sub

    'Sohail (07 Nov 2014) -- Start
    'Enhancement - Sorting on System List View.
    'Sohail (18 Jul 2017) -- Start
    'Enhancement - 69.1 - Day wise total days worked and hours worked functions.
    'Private Sub lvEmployeeList_ColumnClick(ByVal sender As Object, ByVal e As System.Windows.Forms.ColumnClickEventArgs)
    '    Try
    '        Dim new_sorting_column As ColumnHeader = lvEmployeeList.Columns(e.Column)

    '        Dim sort_order As System.Windows.Forms.SortOrder

    '        '*** To store each column sorting order (issue is some spaces are coming before column header text for image)
    '        'If new_sorting_column.ImageKey <> "" Then
    '        '    If new_sorting_column.ImageKey.ToString = ">" Then
    '        '        sort_order = SortOrder.Descending
    '        '    Else
    '        '        sort_order = SortOrder.Ascending
    '        '    End If
    '        'Else
    '        '    sort_order = SortOrder.Ascending
    '        'End If

    '        'If sort_order = SortOrder.Ascending Then
    '        '    new_sorting_column.ImageKey = ">"
    '        'Else
    '        '    new_sorting_column.ImageKey = "<"
    '        'End If
    '        If lvEmployeeList.Sorting = SortOrder.Descending Then
    '            sort_order = SortOrder.Ascending
    '        Else
            '        sort_order = SortOrder.Descending
            '    End If
    '        lvEmployeeList.Sorting = sort_order

    '        lvEmployeeList.ListViewItemSorter = New ListViewComparer(e.Column, sort_order)

    '        lvEmployeeList.Sort()

    '    Catch ex As Exception
    '        DisplayError.Show("-1", ex.Message, "lvEmployeeList_ColumnClick", mstrModuleName)
    '    End Try
    'End Sub
    'Sohail (18 Jul 2017) -- End
    'Sohail (07 Nov 2014) -- End
    'Sohail (18 Jul 2017) -- Start
    'Enhancement - 69.1 - Day wise total days worked and hours worked functions.
    'Private Sub lvEmployeeList_ItemChecked(ByVal sender As Object, ByVal e As System.Windows.Forms.ItemCheckedEventArgs)

    '    Try
    '        'Sohail (03 Nov 2010) -- Start
    '        'If lvEmployeeList.CheckedItems.Count = lvEmployeeList.Items.Count Then
    '        '    chkSelectAll.Checked = True
    '        'ElseIf lvEmployeeList.CheckedItems.Count = 0 Then
    '        '    chkSelectAll.Checked = False
    '        'End If
    '        If lvEmployeeList.CheckedItems.Count <= 0 Then
    '            RemoveHandler objchkSelectAll.CheckedChanged, AddressOf chkSelectAll_CheckedChanged
    '            objchkSelectAll.CheckState = CheckState.Unchecked
    '            AddHandler objchkSelectAll.CheckedChanged, AddressOf chkSelectAll_CheckedChanged
    '        ElseIf lvEmployeeList.CheckedItems.Count < lvEmployeeList.Items.Count Then
    '            RemoveHandler objchkSelectAll.CheckedChanged, AddressOf chkSelectAll_CheckedChanged
    '            objchkSelectAll.CheckState = CheckState.Indeterminate
    '            AddHandler objchkSelectAll.CheckedChanged, AddressOf chkSelectAll_CheckedChanged
    '        ElseIf lvEmployeeList.CheckedItems.Count = lvEmployeeList.Items.Count Then
    '            RemoveHandler objchkSelectAll.CheckedChanged, AddressOf chkSelectAll_CheckedChanged
    '            objchkSelectAll.CheckState = CheckState.Checked
    '            AddHandler objchkSelectAll.CheckedChanged, AddressOf chkSelectAll_CheckedChanged
    '        End If
    '        'Sohail (03 Nov 2010) -- End
    '    Catch ex As Exception
    '        DisplayError.Show("-1", ex.Message, "lvEmployeeList_ItemChecked", mstrModuleName)
    '    End Try
    'End Sub
    'Sohail (18 Jul 2017) -- End
    'Private Sub lvEmployeeList_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lvEmployeeList.SelectedIndexChanged
    '    Dim objTranHead As New clsTransactionHead
    '    Dim objCommonMaster As New clsMasterData
    '    Dim dsEmpHeads As DataSet = Nothing
    '    Dim dsCalcType As DataSet

    '    Try
    '        If lvEmployeeList.SelectedItems.Count = 0 Then Exit Sub

    '        dsEmpHeads = objED.GetList("EmpEDHeads", CInt(lvEmployeeList.Items(lvEmployeeList.SelectedItems(0).Index).Tag))
    '        mdtTran.Rows.Clear()
    '        cboTrnHead.Enabled = True
    '        cboTrnHeadType.Enabled = True

    '        Call CheckAllEmployee(False)
    '        lvEmployeeList.SelectedItems(0).Checked = True

    '        Dim dtRow As DataRow

    '        For Each dRow As DataRow In dsEmpHeads.Tables("EmpEDHeads").Rows
    '            dtRow = mdtTran.NewRow
    '            With dtRow
    '                .Item("edunkid") = CInt(dRow.Item("edunkid").ToString)
    '                .Item("batchtranunkid") = CInt(dRow.Item("batchtransactionunkid").ToString)
    '                .Item("tranheadunkid") = CInt(dRow.Item("tranheadunkid").ToString)

    '                objTranHead._Tranheadunkid = CInt(dRow.Item("tranheadunkid").ToString)
    '                .Item("tranhead") = objTranHead._Trnheadname
    '                .Item("trnheadtypeID") = objTranHead._Trnheadtype_Id
    '                objTranHeadType.SelectedValue = objTranHead._Trnheadtype_Id
    '                .Item("trnheadtype") = objTranHeadType.Text
    '                objTranHeadType.SelectedValue = 0

    '                .Item("typeofId") = objTranHead._Typeof_id

    '                .Item("calctypeId") = objTranHead._Calctype_Id
    '                Select Case objTranHead._Typeof_id
    '                    Case Is > 1
    '                        dsCalcType = objCommonMaster.getComboListCalcType("CalcType", 2)
    '                    Case Else
    '                        dsCalcType = objCommonMaster.getComboListCalcType("CalcType", objTranHead._Typeof_id)
    '                End Select

    '                With objCalcType
    '                    .ValueMember = "Id"
    '                    .DisplayMember = "Name"
    '                    .DataSource = dsCalcType.Tables("CalcType")
    '                    If .Items.Count > 0 Then .SelectedIndex = 0
    '                End With
    '                objCalcType.SelectedValue = objTranHead._Calctype_Id
    '                .Item("calctype") = objCalcType.Text
    '                objCalcType.SelectedValue = 0

    '                If objTranHead._Computeon_Id > 0 Then
    '                    .Item("computeonid") = objTranHead._Computeon_Id
    '                    objComputeOn.SelectedValue = objTranHead._Computeon_Id
    '                    .Item("computeon") = objComputeOn.Text
    '                    objComputeOn.SelectedValue = 0
    '                End If
    '                .Item("formula") = objTranHead._Formula

    '                If objTranHead._Trnheadtype_Id = enTranHeadType.EarningForEmployees Then
    '                    .Item("isdeduct") = False
    '                ElseIf objTranHead._Trnheadtype_Id = enTranHeadType.EmployersStatutoryContributions Then
    '                    .Item("isdeduct") = Nothing
    '                Else
    '                    .Item("isdeduct") = True
    '                End If

    '                If objTranHead._Calctype_Id = enCalcType.AsComputedValue Or _
    '                    objTranHead._Calctype_Id = enCalcType.AsComputedOnWithINEXCESSOFTaxSlab Or _
    '                    objTranHead._Calctype_Id = enCalcType.AsUserDefinedValue Or _
    '                    dRow.Item("amount").ToString = Nothing Then

    '                    .Item("amount") = Nothing
    '                Else
    '                    .Item("amount") = Format(dRow.Item("amount"), "#0.00")
    '                End If

    '                .Item("currencyunkId") = dRow.Item("currencyunkId").ToString
    '                .Item("vendorunkId") = dRow.Item("vendorunkId").ToString
    '                .Item("userunkId") = dRow.Item("userunkId").ToString
    '                .Item("isvoid") = dRow.Item("userunkId").ToString
    '                .Item("voiduserunkId") = dRow.Item("userunkId").ToString
    '                .Item("voiddatetime") = dRow.Item("userunkId").ToString
    '                '.Item("AUD") = "I"
    '                '.Item("GUID") = "" 'Guid.NewGuid.ToString
    '            End With
    '            mdtTran.Rows.Add(dtRow)
    '        Next
    '        'Call FillItemList()
    '    Catch ex As Exception
    '        DisplayError.Show("-1", ex.Message, "lvEmployeeList_SelectedIndexChanged", mstrModuleName)
    '    Finally
    '        objTranHead = Nothing
    '        objCommonMaster = Nothing
    '    End Try
    'End Sub
#End Region

#Region " GridView Events "

    'Sohail (18 Jul 2017) -- Start
    'Enhancement - 69.1 - Day wise total days worked and hours worked functions.
    Private Sub dgEmployee_CurrentCellDirtyStateChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles dgEmployee.CurrentCellDirtyStateChanged
        Try
            If dgEmployee.IsCurrentCellDirty Then
                dgEmployee.CommitEdit(DataGridViewDataErrorContexts.Commit)
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgEmployee_CurrentCellDirtyStateChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub dgEmployee_CellValueChanged(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgEmployee.CellValueChanged
        Try
            If e.RowIndex < 0 Then Exit Sub

            If e.ColumnIndex = objdgcolhCheck.Index Then
                SetCheckBoxValue()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgEmployee_CellValueChanged", mstrModuleName)
        End Try
    End Sub
    'Sohail (18 Jul 2017) -- End

#End Region

#Region " CheckBox's Events "
    Private Sub chkSelectAll_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objchkSelectAll.CheckedChanged
        Try
            Call CheckAllEmployee(objchkSelectAll.Checked)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "chkSelectAll_CheckedChanged", mstrModuleName)
        End Try
    End Sub

    'Sohail (25 May 2012) -- Start
    'TRA - ENHANCEMENT
    Private Sub chkShowNewlyHiredEmployees_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkShowNewlyHiredEmployees.CheckedChanged
        Try
            Call FillCombo_Employee()
            Call FillList()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "chkShowNewlyHiredEmployees_CheckedChanged", mstrModuleName)
        End Try
    End Sub
    'Sohail (25 May 2012) -- End

    'Sohail (30 Nov 2013) -- Start
    'Enhancement - Oman
    Private Sub chkShowReinstatedEmployees_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkShowReinstatedEmployees.CheckedChanged
        Try
            Call FillCombo_Employee()
            Call FillList()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "chkShowReinstatedEmployees_CheckedChanged", mstrModuleName)
        End Try
    End Sub
    'Sohail (30 Nov 2013) -- End

    'Sohail (26 Jul 2012) -- Start
    'TRA - ENHANCEMENT
    Private Sub chkCopyPreviousEDSlab_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkCopyPreviousEDSlab.CheckedChanged
        Try
            'Sohail (15 Dec 2018) -- Start
            'HJFMRI Enhancement - Copy Selected Period ED Slab option on Global Assign ED screen in 75.1.
            'If chkCopyPreviousEDSlab.Checked = False Then chkOverwritePrevEDSlabHeads.Checked = False
            'chkOverwritePrevEDSlabHeads.Enabled = chkCopyPreviousEDSlab.Checked
            RemoveHandler chkCopySelectedPeriodEDSlab.CheckedChanged, AddressOf chkCopySelectedPeriodEDSlab_CheckedChanged
            If chkCopySelectedPeriodEDSlab.Checked = True Then chkCopySelectedPeriodEDSlab.Checked = False
            AddHandler chkCopySelectedPeriodEDSlab.CheckedChanged, AddressOf chkCopySelectedPeriodEDSlab_CheckedChanged

            If chkCopyPreviousEDSlab.Checked = False AndAlso chkCopySelectedPeriodEDSlab.Checked = False Then chkOverwritePrevEDSlabHeads.Checked = False
            cboCopySelectedPeriodEDSlab.Enabled = chkCopySelectedPeriodEDSlab.Checked
            If chkCopySelectedPeriodEDSlab.Checked = False Then
                If CInt(cboCopySelectedPeriodEDSlab.SelectedValue) > 0 Then cboCopySelectedPeriodEDSlab.SelectedValue = 0
            End If
            If chkCopyPreviousEDSlab.Checked = True OrElse chkCopySelectedPeriodEDSlab.Checked = True Then
                chkOverwritePrevEDSlabHeads.Enabled = True
            Else
                chkOverwritePrevEDSlabHeads.Enabled = False
            End If
            'Sohail (15 Dec 2018) -- End
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "chkCopyPreviousEDSlab_CheckedChanged", mstrModuleName)
        End Try
    End Sub
    'Sohail (26 Jul 2012) -- End

    'Sohail (15 Dec 2018) -- Start
    'HJFMRI Enhancement - Copy Selected Period ED Slab option on Global Assign ED screen in 75.1.
    Private Sub chkCopySelectedPeriodEDSlab_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkCopySelectedPeriodEDSlab.CheckedChanged
        Try
            cboCopySelectedPeriodEDSlab.Enabled = chkCopySelectedPeriodEDSlab.Checked
            If chkCopySelectedPeriodEDSlab.Checked = False Then
                If CInt(cboCopySelectedPeriodEDSlab.SelectedValue) > 0 Then cboCopySelectedPeriodEDSlab.SelectedValue = 0
            End If
            RemoveHandler chkCopyPreviousEDSlab.CheckedChanged, AddressOf chkCopyPreviousEDSlab_CheckedChanged
            If chkCopyPreviousEDSlab.Checked = True Then chkCopyPreviousEDSlab.Checked = False
            AddHandler chkCopyPreviousEDSlab.CheckedChanged, AddressOf chkCopyPreviousEDSlab_CheckedChanged

            If chkCopyPreviousEDSlab.Checked = False AndAlso chkCopySelectedPeriodEDSlab.Checked = False Then chkOverwritePrevEDSlabHeads.Checked = False
            If chkCopyPreviousEDSlab.Checked = True OrElse chkCopySelectedPeriodEDSlab.Checked = True Then
                chkOverwritePrevEDSlabHeads.Enabled = True
            Else
                chkOverwritePrevEDSlabHeads.Enabled = False
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "chkCopySelectedPeriodEDSlab_CheckedChanged", mstrModuleName)
        End Try
    End Sub
    'Sohail (15 Dec 2018) -- End

    'Sohail (14 Feb 2019) -- Start
    'NMB Enhancement - 76.1 - Option to filter employees who has not been assigned selected head on Global Assign ED screen.
    Private Sub chkNotAssignedHead_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkNotAssignedHead.CheckedChanged
        Try
            Call FillCombo_Employee()
            Call FillList()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "chkNotAssignedHead_CheckedChanged", mstrModuleName)
        End Try
    End Sub
    'Sohail (14 Feb 2019) -- End

#End Region

#Region " Textbox's Events "
    'Private Sub txtAmount_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtAmount.LostFocus
    '    'Sohail (03 Sep 2010) -- Start
    '    'If txtAmount.Text.Length = 0 Then txtAmount.Text = "0"
    '    'txtAmount.Text = Format(cdec(txtAmount.Text), mstrCurrencyFormat)
    '    Try
    '        txtAmount.Text = Format(txtAmount.Decimal, mstrCurrencyFormat)
    '    Catch ex As Exception
    '        DisplayError.Show("-1", ex.Message, "", mstrModuleName)
    '    End Try
    '    'Sohail (03 Sep 2010) -- Start
    'End Sub

    'Sohail (11 May 2011) -- Start
    Private Sub txtAmount_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtAmount.TextChanged
        Try
            If txtAmount.SelectionLength <= 0 Then Exit Sub
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "txtAmount_TextChanged", mstrModuleName)
        End Try
    End Sub
    'Sohail (11 May 2011) -- End

    'Sohail (25 May 2012) -- Start
    'TRA - ENHANCEMENT
    'Sohail (18 Jul 2017) -- Start
    'Enhancement - 69.1 - Day wise total days worked and hours worked functions.
    'Private Sub txtSearchEmp_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
    '    Try
    '        If lvEmployeeList.Items.Count <= 0 Then Exit Sub
    '        lvEmployeeList.SelectedIndices.Clear()
    '        Dim lvFoundItem As ListViewItem = lvEmployeeList.FindItemWithText(txtSearchEmp.Text, True, 0, True)
    '        If lvFoundItem IsNot Nothing Then
    '            lvEmployeeList.TopItem = lvFoundItem
    '            lvFoundItem.Selected = True
    '        End If
    '    Catch ex As Exception
    '        DisplayError.Show("-1", ex.Message, "txtSearchEmp_TextChanged", mstrModuleName)
    '    End Try
    'End Sub
    Private Sub txtSearchEmp_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtSearchEmp.GotFocus
        Try
            With txtSearchEmp
                .ForeColor = Color.Black
                .Font = New Font(Me.Font.FontFamily, Me.Font.Size, FontStyle.Regular)

                If .Text = mstrSearchEmpText Then
                    .Clear()
                End If
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "txtSearchEmp_GotFocus", mstrModuleName)
        End Try
    End Sub

    Private Sub txtSearchEmp_Leave(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtSearchEmp.Leave
        Try
            If txtSearchEmp.Text.Trim = "" Then
                Call SetDefaultSearchEmpText()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "txtSearchEmp_Leave", mstrModuleName)
        End Try
    End Sub

    Private Sub txtSearchEmp_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtSearchEmp.TextChanged
        Try
            If txtSearchEmp.Text.Trim = mstrSearchEmpText Then Exit Sub
            If dvEmployee IsNot Nothing Then
                dvEmployee.RowFilter = "employeecode LIKE '%" & txtSearchEmp.Text.Replace("'", "''") & "%'  OR employeename LIKE '%" & txtSearchEmp.Text.Replace("'", "''") & "%'"
                dgEmployee.Refresh()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "txtSearchEmp_TextChanged", mstrModuleName)
        End Try
    End Sub
    'Sohail (18 Jul 2017) -- End
    'Sohail (25 May 2012) -- End
#End Region

#Region " Other Control's Events "
    Private Sub objbtnSearchEmployee_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchEmployee.Click
        Dim objfrm As New frmCommonSearch
        'Dim objEmployee As New clsEmployee_Master 'Sohail (03 Mar 2012)
        'Dim dsList As DataSet 'Sohail (03 Mar 2012)
        Try
            'If User._Object._RightToLeft = True Then
            '    objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
            '    objfrm.RightToLeftLayout = True
            '    Call Language.ctlRightToLeftlayOut(objfrm)
            'End If
            'Sohail (06 Jan 2012) -- Start
            'TRA - ENHANCEMENT
            'dsList = objEmployee.GetEmployeeList("EmployeeList")

            'dsList = objEmployee.GetEmployeeList("EmployeeList", True, , , , , , , , , , , , , ConfigParameter._Object._CurrentDateAndTime.Date, ConfigParameter._Object._CurrentDateAndTime.Date)'Sohail (03 Mar 2012)
            'Sohail (06 Jan 2012) -- End
            With cboEmployee
                'Sohail (03 Mar 2012) -- Start
                'TRA - ENHANCEMENT
                'objfrm.DataSource = dsList.Tables("EmployeeList")
                objfrm.DataSource = CType(cboEmployee.DataSource, DataTable)
                'Sohail (03 Mar 2012) -- End
                objfrm.ValueMember = .ValueMember
                objfrm.DisplayMember = .DisplayMember
                objfrm.CodeMember = "employeecode"
                If objfrm.DisplayDialog Then
                    .SelectedValue = objfrm.SelectedValue
                End If
                .Focus()
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchEmployee_Click", mstrModuleName)
        Finally
            objfrm = Nothing
            'objEmployee = Nothing 'Sohail (03 Mar 2012)
        End Try
    End Sub

    Private Sub objbtnSearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearch.Click
        Try
            If objchkSelectAll.Checked = True Then objchkSelectAll.Checked = False 'Sohail (09 Oct 2010)
            Call FillList()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearch_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnReset.Click
        Try
            If cboEmployee.Items.Count > 0 Then cboEmployee.SelectedValue = 0
            'Sohail (09 Nov 2013) -- Start
            'TRA - ENHANCEMENT
            'If cboDepartment.Items.Count > 0 Then cboDepartment.SelectedValue = 0
            'If cboGrade.Items.Count > 0 Then cboGrade.SelectedValue = 0
            'If cboSections.Items.Count > 0 Then cboSections.SelectedValue = 0
            'If cboClass.Items.Count > 0 Then cboClass.SelectedValue = 0
            'If cboCostCenter.Items.Count > 0 Then cboCostCenter.SelectedValue = 0
            'If cboJob.Items.Count > 0 Then cboJob.SelectedValue = 0
            'If cboPayPoint.Items.Count > 0 Then cboPayPoint.SelectedValue = 0
            'If cboUnit.Items.Count > 0 Then cboUnit.SelectedValue = 0
            'Sohail (09 Nov 2013) -- End
            mstrAdvanceFilter = "" 'Sohail (31 Aug 2012)

            'Sohail (18 Jul 2017) -- Start
            'Enhancement - 69.1 - Day wise total days worked and hours worked functions.
            'lvEmployeeList.Items.Clear()
            dgEmployee.DataSource = Nothing
            'Sohail (18 Jul 2017) -- End
            If objchkSelectAll.Checked = True Then objchkSelectAll.Checked = False 'Sohail (09 Oct 2010)
            Call FillList() 'Sohail (11 Sep 2010)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnReset_Click", mstrModuleName)
        End Try
    End Sub

    'Sohail (15 Oct 2010) -- Start
    Private Sub objbtnSearchTranHead_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles objbtnSearchTranHead.Click
        Dim objfrm As New frmCommonSearch
        Dim objEmployee As New clsTransactionHead
        Dim dsList As DataSet
        Dim dtTable As DataTable
        Try
            'If User._Object._RightToLeft = True Then
            '    objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
            '    objfrm.RightToLeftLayout = True
            '    Call Language.ctlRightToLeftlayOut(objfrm)
            'End If
            'Sohail (14 Apr 2011) -- Start
            'dsList = objEmployee.getComboList("TranHead", False, CInt(cboTrnHeadType.SelectedValue), enCalcType.FlatRate_Others, CInt(IIf(CInt(cboTypeOf.SelectedValue) > 0, CInt(cboTypeOf.SelectedValue), -1)))
            'Sohail (29 Oct 2012) -- Start
            'TRA - ENHANCEMENT
            'dsList = objEmployee.getComboList("TranHead", False, CInt(cboTrnHeadType.SelectedValue), 0, CInt(IIf(CInt(cboTypeOf.SelectedValue) > 0, CInt(cboTypeOf.SelectedValue), -1)))
            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'dsList = objEmployee.getComboList("TranHead", False, CInt(cboTrnHeadType.SelectedValue), 0, CInt(IIf(CInt(cboTypeOf.SelectedValue) > 0, CInt(cboTypeOf.SelectedValue), -1)), , True)
            dsList = objEmployee.getComboList(FinancialYear._Object._DatabaseName, "TranHead", False, CInt(cboTrnHeadType.SelectedValue), 0, CInt(IIf(CInt(cboTypeOf.SelectedValue) > 0, CInt(cboTypeOf.SelectedValue), -1)), , True)
            'Sohail (21 Aug 2015) -- End
            'Sohail (29 Oct 2012) -- End
            'Sohail (14 Apr 2011) -- End
            dtTable = New DataView(dsList.Tables("TranHead"), "Typeof_id <> " & enTypeOf.Salary & "", "", DataViewRowState.CurrentRows).ToTable
            With cboTrnHead
                objfrm.DataSource = dtTable
                objfrm.ValueMember = .ValueMember
                objfrm.DisplayMember = .DisplayMember
                objfrm.CodeMember = "code"
                If objfrm.DisplayDialog Then
                    .SelectedValue = objfrm.SelectedValue
                End If
                .Focus()
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchEmployee_Click", mstrModuleName)
        Finally
            objfrm = Nothing
            objEmployee = Nothing
        End Try
    End Sub
    'Sohail (15 Oct 2010) -- End

    'Sohail (31 Aug 2012) -- Start
    'TRA - ENHANCEMENT
    Private Sub lnkAllocation_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lnkAllocation.LinkClicked
        Try
            Dim frm As New frmAdvanceSearch
            frm.ShowDialog()
            mstrAdvanceFilter = frm._GetFilterString
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lnkAllocation_LinkClicked", mstrModuleName)
        End Try
    End Sub
    'Sohail (31 Aug 2012) -- End

    'Hemant (20 July 2018) -- Start
    Private Sub objbgwGlobalAssignED_DoWork(ByVal sender As System.Object, ByVal e As System.ComponentModel.DoWorkEventArgs) Handles objbgwGlobalAssignED.DoWork
        Dim decAmount As Decimal = 0
        mblnProcessFailed = False
        Dim strList As String = ""
        Dim blnFlag As Boolean
        Try
            decAmount = txtAmount.Decimal
            Dim allEmp As List(Of String) = (From p In dvEmployee.Table Where (CBool(p.Item("IsChecked")) = True) Select (p.Item("employeeunkid").ToString)).ToList
            strList = String.Join(",", allEmp.ToArray)
            Me.ControlBox = False
            EnableControls(False)

            'S.SANDEEP [28-May-2018] -- START
            'ISSUE/ENHANCEMENT : {Audit Trails} 
            objED._FormName = mstrModuleName
            objED._LoginEmployeeUnkid = 0
            objED._ClientIP = getIP()
            objED._HostName = getHostName()
            objED._FromWeb = False
            objED._AuditUserId = User._Object._Userunkid
objED._CompanyUnkid = Company._Object._Companyunkid
            objED._AuditDate = ConfigParameter._Object._CurrentDateAndTime
            'S.SANDEEP [28-May-2018] -- END

            'Sohail (15 Dec 2018) -- Start
            'HJFMRI Enhancement - Copy Selected Period ED Slab option on Global Assign ED screen in 75.1.
            'blnFlag = objED.InsertAllByEmployeeList(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, mdtPayPeriodStartDate, mdtPayPeriodEndDate, ConfigParameter._Object._UserAccessModeSetting, True, ConfigParameter._Object._IsIncludeInactiveEmp, strList, CInt(cboTrnHead.SelectedValue), 0, CInt(cboVendor.SelectedValue), _
            '                                     -1, decAmount, radOverwrite.Checked, radAdd.Checked, CInt(cboPeriod.SelectedValue), txtMedicalRefNo.Text.Trim, CDate(IIf(dtpCumulativeStartDate.Checked = True, dtpCumulativeStartDate.Value.Date, Nothing)), CDate(IIf(dtpStopDate.Checked = True, dtpStopDate.Value.Date, Nothing)), User._Object.Privilege._AllowToApproveEarningDeduction, ConfigParameter._Object._CurrentDateAndTime, True _
            '                                     , chkCopyPreviousEDSlab.Checked _
            '                                     , chkOverwritePrevEDSlabHeads.Checked _
            '                                     , True, "", objbgwGlobalAssignED)
            'Sohail (25 Jun 2020) -- Start
            'NMB Enhancement # : Performance enhancement on Import earning deduction heads.
            'blnFlag = objED.InsertAllByEmployeeList(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, mdtPayPeriodStartDate, mdtPayPeriodEndDate, ConfigParameter._Object._UserAccessModeSetting, True, ConfigParameter._Object._IsIncludeInactiveEmp, strList, CInt(cboTrnHead.SelectedValue), 0, CInt(cboVendor.SelectedValue), _
            '                                     -1, decAmount, radOverwrite.Checked, radAdd.Checked, CInt(cboPeriod.SelectedValue), txtMedicalRefNo.Text.Trim, CDate(IIf(dtpCumulativeStartDate.Checked = True, dtpCumulativeStartDate.Value.Date, Nothing)), CDate(IIf(dtpStopDate.Checked = True, dtpStopDate.Value.Date, Nothing)), User._Object.Privilege._AllowToApproveEarningDeduction, Nothing, ConfigParameter._Object._CurrentDateAndTime, True _
            '                                     , chkCopyPreviousEDSlab.Checked _
            '                                     , chkOverwritePrevEDSlabHeads.Checked _
            '                                     , True, "", objbgwGlobalAssignED, True, CInt(cboCopySelectedPeriodEDSlab.SelectedValue), CDate(IIf(dtpEDStartDate.Checked = True, dtpEDStartDate.Value.Date, Nothing)))
            blnFlag = objED.InsertAllByEmployeeList(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, mdtPayPeriodStartDate, mdtPayPeriodEndDate, ConfigParameter._Object._UserAccessModeSetting, True, ConfigParameter._Object._IsIncludeInactiveEmp, strList, CInt(cboTrnHead.SelectedValue), 0, CInt(cboVendor.SelectedValue), _
                                                 -1, decAmount, radOverwrite.Checked, radAdd.Checked, CInt(cboPeriod.SelectedValue), txtMedicalRefNo.Text.Trim, CDate(IIf(dtpCumulativeStartDate.Checked = True, dtpCumulativeStartDate.Value.Date, Nothing)), CDate(IIf(dtpStopDate.Checked = True, dtpStopDate.Value.Date, Nothing)), User._Object.Privilege._AllowToApproveEarningDeduction, Nothing, ConfigParameter._Object._CurrentDateAndTime, True _
                                                 , chkCopyPreviousEDSlab.Checked _
                                                 , chkOverwritePrevEDSlabHeads.Checked _
                                                 , True, "", objbgwGlobalAssignED, True, CInt(cboCopySelectedPeriodEDSlab.SelectedValue), CDate(IIf(dtpEDStartDate.Checked = True, dtpEDStartDate.Value.Date, Nothing)), True)
            'Sohail (25 Jun 2020) -- End
            'Sohail (24 Aug 2019) - [edstart_date]
            'Sohail (15 Dec 2018) -- End
            If blnFlag = False AndAlso objED._Message <> "" Then
                Throw New Exception(objED._Message)

            End If
            'Sohail (31 Oct 2019) -- Start
            'Mainspring Resourcing Ghana : Support Issue Id # 0004199 - Email notification not sent to approvers when there is uploaded, edited e & d.
            If blnFlag = True Then
                If User._Object.Privilege._AllowToApproveEarningDeduction = False AndAlso radOverwrite.Checked = True Then
                    Dim objHead As New clsTransactionHead
                    objHead._Tranheadunkid(FinancialYear._Object._DatabaseName) = CInt(cboTrnHead.SelectedValue)
                    If objHead._Calctype_Id = enCalcType.FlatRate_Others Then
                        Dim dtTable As New DataTable
                        dtTable.Columns.Add("employeeunkid", System.Type.GetType("System.Int32")).DefaultValue = 0
                        dtTable.Columns.Add("employeecode", System.Type.GetType("System.String")).DefaultValue = ""
                        dtTable.Columns.Add("employeename", System.Type.GetType("System.String")).DefaultValue = ""
                        dtTable.Columns.Add("tranheadname", System.Type.GetType("System.String")).DefaultValue = cboTrnHead.Text
                        dtTable.Columns.Add("calctype_id", System.Type.GetType("System.Int32")).DefaultValue = objHead._Calctype_Id
                        dtTable.Columns.Add("amount", System.Type.GetType("System.Decimal")).DefaultValue = decAmount

                        Dim d_r() As DataRow = dvEmployee.Table.Select("IsChecked = 1 ")

                        For Each r As DataRow In d_r
                            Dim dr As DataRow = dtTable.NewRow

                            dr.Item("employeeunkid") = r.Item("employeeunkid")
                            dr.Item("employeecode") = r.Item("employeecode")
                            dr.Item("employeename") = r.Item("employeename")

                            dtTable.Rows.Add(dr)
                        Next

                        Call objED.SendMailToApprover(FinancialYear._Object._DatabaseName, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, User._Object._Userunkid, mdtPayPeriodStartDate, mdtPayPeriodEndDate, ConfigParameter._Object._UserAccessModeSetting, True, False, User._Object.Privilege._AllowToApproveEarningDeduction, dtTable, CInt(cboPeriod.SelectedValue), GUI.fmtCurrency, enLogin_Mode.DESKTOP, mstrModuleName, 0)
                    End If
                End If
            End If
            'Sohail (31 Oct 2019) -- End

        Catch ex As Exception
            mblnProcessFailed = True
            RemoveHandler gfrmMDI.tmrReminder.Tick, AddressOf gfrmMDI.tmrReminder_Tick
            AddHandler gfrmMDI.tmrReminder.Tick, AddressOf gfrmMDI.tmrReminder_Tick
            clsApplicationIdleTimer.LastIdealTime.Start()
            DisplayError.Show("-1", ex.Message, "objbgwGlobalAssignED_DoWork", mstrModuleName)
        Finally
            RemoveHandler gfrmMDI.tmrReminder.Tick, AddressOf gfrmMDI.tmrReminder_Tick
            AddHandler gfrmMDI.tmrReminder.Tick, AddressOf gfrmMDI.tmrReminder_Tick
            clsApplicationIdleTimer.LastIdealTime.Start()
        End Try
    End Sub
    'Hemant (20 July 2018) -- End
    'Hemant (20 July 2018) -- Start
    Private Sub objbgwGlobalAssignED_ProgressChanged(ByVal sender As System.Object, ByVal e As System.ComponentModel.ProgressChangedEventArgs) Handles objbgwGlobalAssignED.ProgressChanged
        Try
            objlblProgress.Text = " [" & clsClosePeriod._ProgressCurrCount & " / " & clsClosePeriod._ProgressTotalCount & "] "
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbgwGlobalAssignED_ProgressChanged", mstrModuleName)
        End Try
    End Sub
    'Hemant (20 July 2018) -- End
    'Hemant (20 July 2018) -- Start
    Private Sub objbgwGlobalAssignED_RunWorkerCompleted(ByVal sender As System.Object, ByVal e As System.ComponentModel.RunWorkerCompletedEventArgs) Handles objbgwGlobalAssignED.RunWorkerCompleted
        Try
            RemoveHandler gfrmMDI.tmrReminder.Tick, AddressOf gfrmMDI.tmrReminder_Tick
            AddHandler gfrmMDI.tmrReminder.Tick, AddressOf gfrmMDI.tmrReminder_Tick
            clsApplicationIdleTimer.LastIdealTime.Start()

            If e.Cancelled = True Then

            ElseIf e.Error IsNot Nothing Then
                eZeeMsgBox.Show(e.Error.ToString, enMsgBoxStyle.Information)
            ElseIf mblnProcessFailed = True Then
            Else
                objlblProgress.Text = ""
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 8, "Process completed successfully."), enMsgBoxStyle.Information)
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbgwGlobalAssignED_RunWorkerCompleted", mstrModuleName)
            
        Finally

            Call EnableControls(True)
            Me.ControlBox = True
        End Try

    End Sub
    'Hemant (20 July 2018) -- End
    'Hemant (20 July 2018) -- Start
    Private Sub EnableControls(ByVal blnEnable As Boolean)
        Try
            Dim lst As IEnumerable(Of Control) = pnlMainInfo.Controls.OfType(Of Control)().Where(Function(t) t.Name <> objFooter.Name)
            For Each ctrl In lst
                ctrl.Enabled = blnEnable
            Next
            lst = objFooter.Controls.OfType(Of Control)() '.Where(Function(x) x.Name <> btnStop.Name)
            For Each ctrl In lst
                ctrl.Enabled = blnEnable
            Next
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "EnableControls", mstrModuleName)
        End Try
    End Sub
    'Hemant (20 July 2018) -- End

#End Region

#Region " Message List "
    '1, "Please Select Transaction Head Type."
    '2, "Please Select Transaction Head Type Of."
    '3, "Please Select Transaction Head.
    '4, "Sorry, Amount can not be Zero.
    '5, "Please Select Currency.
    '6, "Please select atleast one Employee from list.
    '7, "Please Add atleast one Transaction Head for the selected Employee.
    '8, "Sorry, This Transaction Head is already exist."
    '9, "Process completed successfully."
    '10, "The Formula of this Transaction Head contains " & strHeadName & " Tranasaction Head (type of Flate Rate)." & vbCrLf & "If you do not assign " & strHeadName & " Transaction Head to selected employee, its value will be cosidered as Zero." & vbCrLf & vbCrLf & "Do you want to proceed?"
    '11, "Formula is not set on this Trnasction Head. Please set formula from Transaction Head Form."
#End Region

    'Private Sub objbtnMoveUp_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
    '    Dim selIdx As Integer
    '    Dim strPrev As String

    '    With lvEarningDeduction
    '        If .SelectedItems.Count = 0 Then Exit Sub

    '        selIdx = .SelectedItems(0).Index
    '        If selIdx = 0 Then Exit Sub

    '        For i As Integer = 0 To .Items(selIdx).SubItems.Count - 1
    '            strPrev = .Items(selIdx - 1).SubItems(i).Text
    '            .Items(selIdx - 1).SubItems(i).Text = .Items(selIdx).SubItems(i).Text
    '            .Items(selIdx).SubItems(i).Text = strPrev
    '        Next
    '        .Items(selIdx - 1).Selected = True
    '    End With
    'End Sub

    'Private Sub objbtnMoveDown_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
    '    Dim selIdx As Integer
    '    Dim strNext As String

    '    With lvEarningDeduction
    '        If .SelectedItems.Count = 0 Then Exit Sub

    '        selIdx = .SelectedItems(0).Index
    '        If selIdx = .Items.Count - 1 Then Exit Sub

    '        For i As Integer = 0 To .Items(selIdx).SubItems.Count - 1
    '            strNext = .Items(selIdx + 1).SubItems(i).Text
    '            .Items(selIdx + 1).SubItems(i).Text = .Items(selIdx).SubItems(i).Text
    '            .Items(selIdx).SubItems(i).Text = strNext
    '        Next
    '        .Items(selIdx + 1).Selected = True
    '    End With
    'End Sub


    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Private Sub OtherSettings()
        Try
            Me.SuspendLayout()

            Call SetLanguage()

            Me.gbFilterCriteria.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbFilterCriteria.ForeColor = GUI._eZeeContainerHeaderForeColor

			Me.gbEarningDeduction.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbEarningDeduction.ForeColor = GUI._eZeeContainerHeaderForeColor 

            Me.gbEmployeeList.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbEmployeeList.ForeColor = GUI._eZeeContainerHeaderForeColor



            Me.eZeeHeader.GradientColor1 = GUI._HeaderBackColor1
            Me.eZeeHeader.GradientColor2 = GUI._HeaderBackColor2
            Me.eZeeHeader.BorderColor = GUI._HeaderBorderColor
            Me.eZeeHeader.HeaderTextForeColor = GUI._HeaderTitleFontColor
            Me.eZeeHeader.DescriptionForeColor = GUI._HeaderMessageFontColor


            Me.btnSave.GradientBackColor = GUI._ButttonBackColor
            Me.btnSave.GradientForeColor = GUI._ButttonFontColor

            Me.btnClose.GradientBackColor = GUI._ButttonBackColor
            Me.btnClose.GradientForeColor = GUI._ButttonFontColor


            Me.ResumeLayout()
        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "OtherSettings", mstrModuleName)
        End Try
    End Sub


    Private Sub SetLanguage()
        Try
            Me.Text = Language._Object.getCaption(Me.Name, Me.Text)

            Me.gbFilterCriteria.Text = Language._Object.getCaption(Me.gbFilterCriteria.Name, Me.gbFilterCriteria.Text)
            Me.lblEmployee.Text = Language._Object.getCaption(Me.lblEmployee.Name, Me.lblEmployee.Text)
            Me.gbEarningDeduction.Text = Language._Object.getCaption(Me.gbEarningDeduction.Name, Me.gbEarningDeduction.Text)
            Me.lblVendor.Text = Language._Object.getCaption(Me.lblVendor.Name, Me.lblVendor.Text)
            Me.lblCurrencyCountry.Text = Language._Object.getCaption(Me.lblCurrencyCountry.Name, Me.lblCurrencyCountry.Text)
            Me.lblAmount.Text = Language._Object.getCaption(Me.lblAmount.Name, Me.lblAmount.Text)
            Me.lblTrnHead.Text = Language._Object.getCaption(Me.lblTrnHead.Name, Me.lblTrnHead.Text)
            Me.lblEffectiveFrom.Text = Language._Object.getCaption(Me.lblEffectiveFrom.Name, Me.lblEffectiveFrom.Text)
			Me.eZeeHeader.Title = Language._Object.getCaption(Me.eZeeHeader.Name & "_Title" , Me.eZeeHeader.Title)
			Me.eZeeHeader.Message = Language._Object.getCaption(Me.eZeeHeader.Name & "_Message" , Me.eZeeHeader.Message)
            Me.btnSave.Text = Language._Object.getCaption(Me.btnSave.Name, Me.btnSave.Text)
            Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
            Me.radOverwrite.Text = Language._Object.getCaption(Me.radOverwrite.Name, Me.radOverwrite.Text)
            Me.radAdd.Text = Language._Object.getCaption(Me.radAdd.Name, Me.radAdd.Text)
            Me.radDontOverWrite.Text = Language._Object.getCaption(Me.radDontOverWrite.Name, Me.radDontOverWrite.Text)
            Me.lblTypeOf.Text = Language._Object.getCaption(Me.lblTypeOf.Name, Me.lblTypeOf.Text)
            Me.lblPeriod.Text = Language._Object.getCaption(Me.lblPeriod.Name, Me.lblPeriod.Text)
            Me.lblMedicalRefNo.Text = Language._Object.getCaption(Me.lblMedicalRefNo.Name, Me.lblMedicalRefNo.Text)
            Me.chkCopyPreviousEDSlab.Text = Language._Object.getCaption(Me.chkCopyPreviousEDSlab.Name, Me.chkCopyPreviousEDSlab.Text)
            Me.chkShowNewlyHiredEmployees.Text = Language._Object.getCaption(Me.chkShowNewlyHiredEmployees.Name, Me.chkShowNewlyHiredEmployees.Text)
            Me.lblSearchEmp.Text = Language._Object.getCaption(Me.lblSearchEmp.Name, Me.lblSearchEmp.Text)
            Me.chkOverwritePrevEDSlabHeads.Text = Language._Object.getCaption(Me.chkOverwritePrevEDSlabHeads.Name, Me.chkOverwritePrevEDSlabHeads.Text)
            Me.lnkAllocation.Text = Language._Object.getCaption(Me.lnkAllocation.Name, Me.lnkAllocation.Text)
            Me.lblCumulativeStartDate.Text = Language._Object.getCaption(Me.lblCumulativeStartDate.Name, Me.lblCumulativeStartDate.Text)
            Me.lblStopDate.Text = Language._Object.getCaption(Me.lblStopDate.Name, Me.lblStopDate.Text)
            Me.chkShowReinstatedEmployees.Text = Language._Object.getCaption(Me.chkShowReinstatedEmployees.Name, Me.chkShowReinstatedEmployees.Text)
			Me.gbEmployeeList.Text = Language._Object.getCaption(Me.gbEmployeeList.Name, Me.gbEmployeeList.Text)
			Me.dgColhEmpCode.HeaderText = Language._Object.getCaption(Me.dgColhEmpCode.Name, Me.dgColhEmpCode.HeaderText)
			Me.dgColhEmployee.HeaderText = Language._Object.getCaption(Me.dgColhEmployee.Name, Me.dgColhEmployee.HeaderText)
			Me.chkCopySelectedPeriodEDSlab.Text = Language._Object.getCaption(Me.chkCopySelectedPeriodEDSlab.Name, Me.chkCopySelectedPeriodEDSlab.Text)
			Me.chkNotAssignedHead.Text = Language._Object.getCaption(Me.chkNotAssignedHead.Name, Me.chkNotAssignedHead.Text)
			Me.lblEDStartDate.Text = Language._Object.getCaption(Me.lblEDStartDate.Name, Me.lblEDStartDate.Text)

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
        End Try
    End Sub


    Private Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Please Select Transaction Head.")
            Language.setMessage(mstrModuleName, 2, "The Formula of this Transaction Head contains")
            Language.setMessage(mstrModuleName, 3, " Tranasaction Head (type of Flat Rate).")
            Language.setMessage(mstrModuleName, 4, "If you do not assign")
            Language.setMessage(mstrModuleName, 5, " Transaction Head to selected employee, then it's value will be considered as Zero.")
            Language.setMessage(mstrModuleName, 6, "Do you want to proceed?")
            Language.setMessage(mstrModuleName, 7, "Formula is not set on this Transaction Head. Please set formula from Transaction Head Form.")
            Language.setMessage(mstrModuleName, 8, "Process completed successfully.")
            Language.setMessage(mstrModuleName, 9, "Please select atleast one Employee from list.")
            Language.setMessage(mstrModuleName, 10, "Please select Period. Period is mandatory information.")
            Language.setMessage(mstrModuleName, 11, "You have ticked Copy Previous ED Slab. This will copy Previous Period Slab.")
            Language.setMessage(mstrModuleName, 12, "Sorry, Process Payroll is already done for last date of selected Period for some of the employees. Please Void Process Payroll first for selected employees to assign earning deduction.")
            Language.setMessage(mstrModuleName, 13, "Do you want to continue?")
            Language.setMessage(mstrModuleName, 14, "Sorry, Cumulative Start Date should not be greater than.")
            Language.setMessage(mstrModuleName, 15, "Sorry, Stop Date should not be less than.")
			Language.setMessage(mstrModuleName, 16, "Do you want to void Payroll?")
			Language.setMessage(mstrModuleName, 17, "Sorry, E. D. Start Date should be in between selected period start date and end date.")
			Language.setMessage(mstrModuleName, 18, "Sorry, E. D. Start Date should be greater than stop date.")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class