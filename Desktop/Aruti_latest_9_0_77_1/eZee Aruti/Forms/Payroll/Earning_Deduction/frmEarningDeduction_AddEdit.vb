﻿Option Strict On

Imports Aruti.Data
Imports eZeeCommonLib

Public Class frmEarningDeduction_AddEdit

#Region " Private Variables "
    Private ReadOnly mstrModuleName As String = "frmEarningDeduction_AddEdit"
    Private mblnCancel As Boolean = True
    Private menAction As enAction = enAction.ADD_ONE
    Private objED As clsEarningDeduction
    Private mintEDUnkID As Integer = -1
    Private mstrCurrencyFormat As String = "#0.00"

    'Sohail (13 Jan 2012) -- Start
    'TRA - ENHANCEMENT
    Private mdtPeriod_startdate As DateTime
    Private mdtPeriod_enddate As DateTime
    Private mdtCopyPeriod_startdate As DateTime
    Private mdtCopyPeriod_enddate As DateTime
    Private mstrCopyPeriodName As String

    Private mdtTable As DataTable
    'Sohail (13 Jan 2012) -- End

    Private mdtSingleED As DataTable 'Sohail (18 Jan 2012)
#End Region

#Region " Display Dialog "
    Public Function displayDialog(ByRef intUnkId As Integer, ByVal eAction As enAction) As Boolean
        Try
            mintEDUnkID = intUnkId
            menAction = eAction

            Me.ShowDialog()

            intUnkId = mintEDUnkID

            Return Not mblnCancel
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "displayDialog", mstrModuleName)
        End Try
    End Function
#End Region

#Region " Private Methods "
    Private Sub SetColor()
        Try
            cboEmployee.BackColor = GUI.ColorComp
            cboTrnHeadType.BackColor = GUI.ColorOptional 'Sohail (15 Oct 2010)
            cboTypeOf.BackColor = GUI.ColorOptional 'Sohail (15 Oct 2010)
            cboTrnHead.BackColor = GUI.ColorComp
            txtAmount.BackColor = GUI.ColorComp
            'cboCurrencyCountry.BackColor = GUI.ColorComp
            'Sohail (11 Sep 2010) -- Start
            'cboVendor.BackColor = GUI.ColorOptional
            cboMemCategory.BackColor = GUI.ColorOptional
            cboMembershipType.BackColor = GUI.ColorOptional
            'Sohail (11 Sep 2010) -- End
            cboCostCenter.BackColor = GUI.ColorOptional 'Sohail (08 Nov 2011)
            'Sohail (13 Jan 2012) -- Start
            'TRA - ENHANCEMENT
            cboPeriod.BackColor = GUI.ColorComp
            cboCopyEDPeriod.BackColor = GUI.ColorComp
            'Sohail (13 Jan 2012) -- End
            txtMedicalRefNo.BackColor = GUI.ColorOptional 'Sohail (18 Jan 2012)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetColor", mstrModuleName)
        End Try
    End Sub

    Private Sub GetValue()
        'Dim objExRate As New clsExchangeRate
        'Sohail (08 Nov 2011) -- Start
        Dim objCC As New clsemployee_costcenter_Tran
        Dim dsList As DataSet
        'Sohail (08 Nov 2011) -- End
        Try
            With objED
                cboPeriod.SelectedValue = ._Periodunkid
                'Sohail (21 Aug 2015) -- Start
                'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                'cboEmployee.SelectedValue = ._Employeeunkid
                cboEmployee.SelectedValue = ._Employeeunkid(FinancialYear._Object._DatabaseName)
                'Sohail (21 Aug 2015) -- End
                cboTrnHeadType.SelectedValue = ._Trnheadtype_Id
                cboTypeOf.SelectedValue = ._Typeof_Id
                cboTrnHead.SelectedValue = ._Tranheadunkid
                'cboCurrencyCountry.SelectedValue = ._Currencyunkid
                'objExRate._ExchangeRateunkid = ._Currencyunkid
                'mstrCurrencyFormat = objExRate._fmtCurrency
                txtAmount.Text = Format(._Amount, mstrCurrencyFormat)
                'Sohail (11 Sep 2010) -- Start
                'cboVendor.SelectedValue = ._Vendorunkid
                cboMemCategory.SelectedValue = ._Membership_Categoryunkid
                cboMembershipType.SelectedValue = ._Vendorunkid
                'Sohail (11 Sep 2010) -- End
                'Sohail (09 Nov 2013) -- Start
                'TRA - ENHANCEMENT
                If ._Cumulative_Startdate <> Nothing Then
                    dtpCumulativeStartDate.Value = ._Cumulative_Startdate
                    dtpCumulativeStartDate.Checked = True
                Else
                    dtpCumulativeStartDate.Checked = False
                End If
                If ._Stop_Date <> Nothing Then
                    dtpStopDate.Value = ._Stop_Date
                    dtpStopDate.Checked = True
                Else
                    dtpStopDate.Checked = False
                End If
                'Sohail (09 Nov 2013) -- End
                'Sohail (24 Aug 2019) -- Start
                'NMB Payroll UAT # TC016 - 76.1 - System should be able to accept future amendments (Future period) and prorate. It should be possible to set start date of the specific transaction and prorate based on the start date. It should also be possible to set start date of the future period.
                If ._EDStart_Date <> Nothing Then
                    dtpEDStartDate.Value = ._EDStart_Date
                    dtpEDStartDate.Checked = True
                Else
                    dtpEDStartDate.Checked = False
                End If
                'Sohail (24 Aug 2019) -- End

                'Sohail (08 Nov 2011) -- Start
                If menAction = enAction.EDIT_ONE Then
                    'Sohail (21 Aug 2015) -- Start
                    'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                    'dsList = objCC.GetList("EmpCC", True, CInt(cboEmployee.SelectedValue), CInt(cboTrnHead.SelectedValue))
                    'Sohail (29 Mar 2017) -- Start
                    'CCBRT Enhancement - 66.1 - Employee cost center distribution for non-salary heads, Effective period option on Employee Cost Center.
                    'dsList = objCC.GetList(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, mdtPeriod_startdate, mdtPeriod_enddate, ConfigParameter._Object._UserAccessModeSetting, True, True, "EmpCC", True, CInt(cboEmployee.SelectedValue), CInt(cboTrnHead.SelectedValue))
                    dsList = objCC.GetList(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, mdtPeriod_startdate, mdtPeriod_enddate, ConfigParameter._Object._UserAccessModeSetting, True, True, "EmpCC", True, CInt(cboEmployee.SelectedValue), CInt(cboTrnHead.SelectedValue), , mdtPeriod_enddate)
                    'Sohail (29 Mar 2017) -- End
                    'Sohail (21 Aug 2015) -- End
                    'Sohail (19 Dec 2018) -- Start
                    'Internal Issue - 76.1 - Employee Cost center get removed ob Editing Earning Deduction when distributed (multiple) cost center has been set for any head.
                    'If dsList.Tables("EmpCC").Rows.Count > 0 Then
                    '    cboCostCenter.SelectedValue = CInt(dsList.Tables("EmpCC").Rows(0).Item("costcenterunkid"))
                    'End If
                    cboCostCenter.Enabled = True
                    If dsList.Tables("EmpCC").Rows.Count > 1 Then 'More than one cost center
                        cboCostCenter.SelectedValue = 0
                        cboCostCenter.Enabled = False
                    ElseIf dsList.Tables("EmpCC").Rows.Count = 1 Then
                        cboCostCenter.SelectedValue = CInt(dsList.Tables("EmpCC").Rows(0).Item("costcenterunkid"))
                    Else
                        cboCostCenter.SelectedValue = 0
                    End If
                    'Sohail (19 Dec 2018) -- End

                Else
                    cboCostCenter.SelectedValue = 0
                End If
                'Sohail (08 Nov 2011) -- End

                'Sohail (13 Jan 2012) -- Start
                'TRA - ENHANCEMENT

                If menAction = enAction.EDIT_ONE Then
                    Dim dRow As DataRow() = mdtTable.Select("AUD <> 'D' AND employeeunkid = " & CInt(cboEmployee.SelectedValue) & " AND tranheadunkid = " & CInt(cboTrnHead.SelectedValue) & " AND end_date > '" & eZeeDate.convertDate(mdtPeriod_startdate) & "' ")
                    If dRow.Length > 0 Then
                        dRow(0).Item("AUD") = "U"
                        mdtTable.AcceptChanges()
                    End If
                End If
                'Sohail (13 Jan 2012) -- End
                txtMedicalRefNo.Text = objED._MedicalRefNo 'Sohail (18 Jan 2012)
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetValue", mstrModuleName)
        Finally
            'objExRate = Nothing
            objCC = Nothing 'Sohail (08 Nov 2011)
        End Try
    End Sub

    Private Sub SetValue()
        Dim objTranHead As New clsTransactionHead
        Try
            With objED
                'Sohail (21 Aug 2015) -- Start
                'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                '._Employeeunkid = CInt(cboEmployee.SelectedValue)
                ._Employeeunkid(FinancialYear._Object._DatabaseName) = CInt(cboEmployee.SelectedValue)
                'Sohail (21 Aug 2015) -- End
                'Sohail (11 Sep 2010) -- Start
                '._Trnheadtype_Id = CInt(cboTrnHeadType.SelectedValue)
                '._Typeof_Id = CInt(cboTypeOf.SelectedValue)
                'Sohail (21 Aug 2015) -- Start
                'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                'objTranHead._Tranheadunkid = CInt(cboTrnHead.SelectedValue)
                objTranHead._Tranheadunkid(FinancialYear._Object._DatabaseName) = CInt(cboTrnHead.SelectedValue)
                'Sohail (21 Aug 2015) -- End
                'Sohail (11 Sep 2010) -- End
                ._Tranheadunkid = CInt(cboTrnHead.SelectedValue)

                If txtAmount.Text.Length > 0 Then
                    ._Amount = txtAmount.Decimal 'Sohail (03 Sep 2010)
                Else
                    ._Amount = Nothing
                End If
                '._Currencyunkid = CInt(cboCurrencyCountry.SelectedValue)
                ._Currencyunkid = 0 'Currency is removed (05 Aug 2010)
                'Sohail (11 Sep 2010) -- Start
                '._Vendorunkid = CInt(cboVendor.SelectedValue)
                ._Vendorunkid = CInt(cboMembershipType.SelectedValue)
                If CInt(cboMembershipType.SelectedValue) > 0 Then
                    ._Membership_Categoryunkid = CInt(cboMemCategory.SelectedValue)
                Else
                    ._Membership_Categoryunkid = 0
                End If
                'Sohail (11 Sep 2010) -- End

                'Sohail (11 Sep 2010) -- Start
                'If CInt(cboTrnHeadType.SelectedValue) = enTranHeadType.EarningForEmployees Then
                '    ._Isdeduct = False
                'ElseIf CInt(cboTrnHeadType.SelectedValue) = enTranHeadType.DeductionForEmployee Or CInt(cboTrnHeadType.SelectedValue) = enTranHeadType.EmployeesStatutoryDeductions Then
                '    ._Isdeduct = True
                'Else
                '    ._Isdeduct = Nothing
                'End If
                If objTranHead._Trnheadtype_Id = enTranHeadType.EarningForEmployees Then
                    ._Isdeduct = False
                ElseIf objTranHead._Trnheadtype_Id = enTranHeadType.DeductionForEmployee Or objTranHead._Trnheadtype_Id = enTranHeadType.EmployeesStatutoryDeductions Then
                    ._Isdeduct = True
                Else
                    ._Isdeduct = Nothing
                End If

                'objTranHead._Tranheadunkid = CInt(cboTrnHead.SelectedValue)
                'Sohail (11 Sep 2010) -- End
                ._Trnheadtype_Id = objTranHead._Trnheadtype_Id
                'Sohail (11 Sep 2010) -- Start
                ._Typeof_Id = objTranHead._Typeof_id
                'Sohail (11 Sep 2010) -- End

                'Sohail (07 Aug 2010) -- Start
                'Sohail (11 Sep 2010) -- Start
                ''._Calctype_Id = objTranHead._Calctype_Id
                'If objTranHead._Calctype_Id = enCalcType.DEFINED_SALARY Or _
                '    objTranHead._Calctype_Id = enCalcType.OnHourWorked Or _
                '    objTranHead._Calctype_Id = enCalcType.OnAttendance Then

                '    ._Calctype_Id = enCalcType.OnAttendance
                'Else
                '._Calctype_Id = objTranHead._Calctype_Id
                'End If
                ._Calctype_Id = objTranHead._Calctype_Id
                'Sohail (11 Sep 2010) -- End
                'Sohail (07 Aug 2010) -- End
                ._Computeon_Id = objTranHead._Computeon_Id
                ._Formula = objTranHead._Formula
                ._FormulaId = objTranHead._Formulaid
                ._Userunkid = User._Object._Userunkid 'Sohail (14 Oct 2010)
                ._CostCenterUnkID = CInt(cboCostCenter.SelectedValue) 'Sohail (08 Nov 2011)
                ._Periodunkid = CInt(cboPeriod.SelectedValue) 'Sohail (13 Jan 2012)
                ._MedicalRefNo = txtMedicalRefNo.Text.Trim 'Sohail (18 Jan 2012)
                'Sohail (09 Nov 2013) -- Start
                'TRA - ENHANCEMENT
                If dtpCumulativeStartDate.Checked = True Then
                    ._Cumulative_Startdate = dtpCumulativeStartDate.Value.Date
                Else
                    ._Cumulative_Startdate = Nothing
                End If
                If dtpStopDate.Checked = True Then
                    ._Stop_Date = dtpStopDate.Value.Date
                Else
                    ._Stop_Date = Nothing
                End If
                'Sohail (09 Nov 2013) -- End
                'Sohail (24 Aug 2019) -- Start
                'NMB Payroll UAT # TC016 - 76.1 - System should be able to accept future amendments (Future period) and prorate. It should be possible to set start date of the specific transaction and prorate based on the start date. It should also be possible to set start date of the future period.
                If dtpEDStartDate.Checked = True Then
                    ._EDStart_Date = dtpEDStartDate.Value.Date
                Else
                    ._EDStart_Date = Nothing
                End If
                'Sohail (24 Aug 2019) -- End

                'Sohail (05 Jul 2011) -- Start
                'If menAction <> enAction.EDIT_ONE Then 'Sohail (02 Sep 2019)
                    If User._Object.Privilege._AllowToApproveEarningDeduction = True Then
                        ._Isapproved = True
                        ._Approveruserunkid = User._Object._Userunkid
                    Else
                        ._Isapproved = False
                    End If
                'End If 'Sohail (02 Sep 2019)
                'Sohail (05 Jul 2011) -- End

            End With

            'Sohail (18 Jan 2012) -- Start
            'TRA - ENHANCEMENT [Now when assigning single head, copy all previous slab heads]
            Dim drED As DataRow

            mdtSingleED = New DataView(mdtTable, "1 = 2 ", "", DataViewRowState.CurrentRows).ToTable

            drED = mdtSingleED.NewRow
            drED.Item("edunkid") = -1
            drED.Item("employeeunkid") = CInt(cboEmployee.SelectedValue)
            'Sohail (31 Oct 2019) -- Start
            'Mainspring Resourcing Ghana : Support Issue Id # 0004199 - Email notification not sent to approvers when there is uploaded, edited e & d.
            drED.Item("employeecode") = CType(cboEmployee.SelectedItem, DataRowView).Item("employeecode").ToString
            drED.Item("employeename") = cboEmployee.Text
            'Sohail (31 Oct 2019) -- End
            drED.Item("tranheadunkid") = CInt(cboTrnHead.SelectedValue)
            'Sohail (31 Oct 2019) -- Start
            'Mainspring Resourcing Ghana : Support Issue Id # 0004199 - Email notification not sent to approvers when there is uploaded, edited e & d.
            'drED.Item("trnheadname") = ""
            drED.Item("trnheadname") = cboTrnHead.Text
            drED.Item("calctype_id") = objTranHead._Calctype_Id
            'Sohail (31 Oct 2019) -- End
            drED.Item("batchtransactionunkid") = -1
            If txtAmount.Text.Length > 0 Then
                drED.Item("amount") = txtAmount.Decimal
            Else
                drED.Item("amount") = 0
            End If
            drED.Item("currencyid") = 0
            drED.Item("vendorid") = CInt(cboMembershipType.SelectedValue)
            drED.Item("userunkid") = User._Object._Userunkid
            drED.Item("isvoid") = False
            drED.Item("voiduserunkid") = -1
            drED.Item("voiddatetime") = DBNull.Value
            drED.Item("voidreason") = ""
            If CInt(cboMembershipType.SelectedValue) > 0 Then
                drED.Item("membership_categoryunkid") = CInt(cboMemCategory.SelectedValue)
            Else
                drED.Item("membership_categoryunkid") = 0
            End If
            If User._Object.Privilege._AllowToApproveEarningDeduction = True Then
                drED.Item("isapproved") = True
                drED.Item("approveruserunkid") = User._Object._Userunkid
            Else
                drED.Item("isapproved") = False
                drED.Item("approveruserunkid") = -1
            End If
            drED.Item("periodunkid") = CInt(cboPeriod.SelectedValue)
            drED.Item("start_date") = eZeeDate.convertDate(mdtPeriod_startdate)
            drED.Item("end_date") = eZeeDate.convertDate(mdtPeriod_enddate)
            drED.Item("costcenterunkid") = CInt(cboCostCenter.SelectedValue)
            drED.Item("medicalrefno") = txtMedicalRefNo.Text.Trim() 'Sohail (18 Jan 2012)
            'Sohail (09 Nov 2013) -- Start
            'TRA - ENHANCEMENT
            'Sohail (21 Dec 2016) -- Start
            'CCK Issue - 64.1 - Membershiptranunkid got reset to 0 when editing membership head.
            'drED.Item("membershiptranunkid") = 0
            If menAction = enAction.EDIT_ONE Then
                Dim objED As New clsEarningDeduction
                objED._Edunkid(Nothing, FinancialYear._Object._DatabaseName) = mintEDUnkID
                drED.Item("membershiptranunkid") = objED._MembershipTranUnkid
            Else
                drED.Item("membershiptranunkid") = 0
            End If
            'Sohail (21 Dec 2016) -- End
            drED.Item("disciplinefileunkid") = 0
            If dtpCumulativeStartDate.Checked = True Then
                drED.Item("cumulative_startdate") = dtpCumulativeStartDate.Value.Date
            Else
                drED.Item("cumulative_startdate") = DBNull.Value
            End If
            If dtpStopDate.Checked = True Then
                drED.Item("stop_date") = dtpStopDate.Value.Date
            Else
                drED.Item("stop_date") = DBNull.Value
            End If
            'Sohail (09 Nov 2013) -- End
            'Sohail (24 Aug 2019) -- Start
            'NMB Payroll UAT # TC016 - 76.1 - System should be able to accept future amendments (Future period) and prorate. It should be possible to set start date of the specific transaction and prorate based on the start date. It should also be possible to set start date of the future period.
            If dtpEDStartDate.Checked = True Then
                drED.Item("edstart_date") = dtpEDStartDate.Value.Date
            Else
                drED.Item("edstart_date") = DBNull.Value
            End If
            'Sohail (24 Aug 2019) -- End

            drED.Item("AUD") = "A"

            mdtSingleED.Rows.Add(drED)

            'Sohail (07 Mar 2012) -- Start
            'TRA - ENHANCEMENT
            'If chkCopyPreviousEDSlab.Checked = True Then
            '    Dim strPrevEndDate As String = ""
            '    Dim drRow As DataRow()
            '    Dim dtRow As DataRow()
            '    Dim strFilter As String

            '    drRow = mdtTable.Select("AUD = '' AND end_date <= '" & eZeeDate.convertDate(mdtPeriod_startdate) & "' AND isrecurrent = 1 ", "end_date DESC")

            '    If drRow.Length > 0 Then
            '        For Each dRow As DataRow In drRow
            '            If strPrevEndDate <> "" AndAlso strPrevEndDate <> dRow.Item("end_date").ToString Then
            '                Exit For
            '            End If

            '            objTranHead._Tranheadunkid = CInt(dRow.Item("tranheadunkid"))
            '            If objTranHead._Typeof_id = enTypeOf.Salary Then
            '                Continue For
            '            End If

            '            strFilter = "AUD <> 'D' AND employeeunkid = " & CInt(dRow.Item("employeeunkid")) & _
            '                        " AND tranheadunkid = " & CInt(dRow.Item("tranheadunkid")) & _
            '                        " AND periodunkid = " & CInt(cboPeriod.SelectedValue) & _
            '                        " AND end_date = '" & eZeeDate.convertDate(mdtPeriod_enddate) & "' "

            '            dtRow = mdtSingleED.Select(strFilter)
            '            If dtRow.Length > 0 Then
            '                Continue For    '*** Already Exist
            '            End If

            '            drED = mdtSingleED.NewRow()

            '            drED.Item("edunkid") = -1
            '            drED.Item("employeeunkid") = CInt(dRow.Item("employeeunkid"))
            '            drED.Item("tranheadunkid") = CInt(dRow.Item("tranheadunkid"))
            '            drED.Item("trnheadname") = dRow.Item("trnheadname").ToString
            '            drED.Item("batchtransactionunkid") = CInt(dRow.Item("batchtransactionunkid"))
            '            drED.Item("amount") = CDec(dRow.Item("amount"))
            '            drED.Item("isdeduct") = CBool(dRow.Item("isdeduct"))
            '            drED.Item("trnheadtype_id") = CInt(dRow.Item("trnheadtype_id"))
            '            drED.Item("typeof_id") = CInt(dRow.Item("typeof_id"))
            '            drED.Item("calctype_id") = CInt(dRow.Item("calctype_id"))
            '            drED.Item("computeon_id") = CInt(dRow.Item("computeon_id"))
            '            drED.Item("formula") = dRow.Item("formula").ToString
            '            drED.Item("formulaid") = dRow.Item("formulaid").ToString
            '            drED.Item("currencyid") = CInt(dRow.Item("currencyid"))
            '            drED.Item("vendorid") = CInt(dRow.Item("vendorid"))
            '            drED.Item("userunkid") = CInt(dRow.Item("userunkid"))
            '            drED.Item("isvoid") = CBool(dRow.Item("isvoid"))
            '            drED.Item("voiduserunkid") = CInt(dRow.Item("voiduserunkid"))
            '            drED.Item("voiddatetime") = dRow.Item("voiddatetime")
            '            drED.Item("voidreason") = dRow.Item("voidreason").ToString
            '            If IsDBNull(dRow.Item("membership_categoryunkid")) Then
            '                drED.Item("membership_categoryunkid") = 0
            '            Else
            '                drED.Item("membership_categoryunkid") = CInt(dRow.Item("membership_categoryunkid"))
            '            End If
            '            drED.Item("isapproved") = CBool(dRow.Item("isapproved"))
            '            drED.Item("approveruserunkid") = CInt(dRow.Item("approveruserunkid"))
            '            drED.Item("periodunkid") = CInt(cboPeriod.SelectedValue)
            '            drED.Item("period_name") = cboPeriod.Text
            '            drED.Item("start_date") = eZeeDate.convertDate(mdtPeriod_startdate)
            '            drED.Item("end_date") = eZeeDate.convertDate(mdtPeriod_enddate)
            '            drED.Item("costcenterunkid") = 0
            '            drED.Item("medicalrefno") = dRow.Item("medicalrefno").ToString 'Sohail (18 Jan 2012)

            '            drED.Item("AUD") = "A"

            '            mdtSingleED.Rows.Add(drED)

            '            strPrevEndDate = dRow.Item("end_date").ToString
            '        Next
            '    End If
            'End If
            'Sohail (07 Mar 2012) -- End
            'Sohail (18 Jan 2012) -- End
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetValue", mstrModuleName)
        Finally
            objTranHead = Nothing
        End Try
    End Sub

    Private Sub FillCombo()
        Dim objEmployee As New clsEmployee_Master
        Dim objMaster As New clsMasterData
        Dim objTranHead As New clsTransactionHead
        'Dim objVendor As New clsvendor_master
        Dim objExRate As New clsExchangeRate
        Dim objCostCenter As New clscostcenter_master 'Sohail (08 Nov 2011)
        Dim objPeriod As New clscommom_period_Tran 'Sohail (13 Jan 2012)
        Dim dsCombo As DataSet
        Dim dtTable As DataTable
        Try

            'Sohail (03 Mar 2012) -- Start
            'TRA - ENHANCEMENT
            ''Sohail (06 Jan 2012) -- Start
            ''TRA - ENHANCEMENT
            ''dsCombo = objEmployee.GetEmployeeList("EmployeeList", True)
            'If menAction = enAction.EDIT_ONE Then
            '    dsCombo = objEmployee.GetEmployeeList("EmployeeList", True, , , , , , , , , , , , , eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate))
            'Else
            '    dsCombo = objEmployee.GetEmployeeList("EmployeeList", True, , , , , , , , , , , , , ConfigParameter._Object._CurrentDateAndTime.Date, ConfigParameter._Object._CurrentDateAndTime.Date)
            'End If
            ''Sohail (06 Jan 2012) -- End           
            'With cboEmployee
            '    .ValueMember = "employeeunkid"
            '    .DisplayMember = "employeename"
            '    .DataSource = dsCombo.Tables("EmployeeList")
            '    If .Items.Count > 0 Then .SelectedIndex = 0
            'End With
            Call FillCombo_Employee()
            'Sohail (03 Mar 2012) -- End

            dsCombo = objMaster.getComboListForHeadType("HeadType")
            With cboTrnHeadType
                .ValueMember = "Id"
                .DisplayMember = "Name"
                .DataSource = dsCombo.Tables("HeadType")
                If .Items.Count > 0 Then .SelectedIndex = 0
            End With
            With objTranHeadType
                .ValueMember = "Id"
                .DisplayMember = "Name"
                .DataSource = cboTrnHeadType.DataSource
            End With

            dsCombo = objMaster.getComboListComputedOn("ComputeOn")
            With objComputeOn
                .ValueMember = "Id"
                .DisplayMember = "Name"
                .DataSource = dsCombo.Tables("ComputeOn")
                If .Items.Count > 0 Then .SelectedIndex = 0
            End With

            'Sohail (11 Sep 2010) -- Start
            'dsCombo = objVendor.getComborList("Vendor", True)
            'With cboVendor
            '    .ValueMember = "vendorunkid"
            '    .DisplayMember = "companyname"
            '    .DataSource = dsCombo.Tables("Vendor")
            '    If .Items.Count > 0 Then .SelectedIndex = 0
            'End With
            'Sohail (11 Sep 2010) -- End

            'dsCombo = objExRate.getComboList("Currency", True)
            'Dim dtRow As DataRow() = dsCombo.Tables("Currency").Select("isbasecurrency =" & True)
            'With cboCurrencyCountry
            '    .ValueMember = "exchangerateunkid"
            '    .DisplayMember = "currency_name"
            '    .DataSource = dsCombo.Tables("Currency")
            '    If .Items.Count > 0 Then
            '        If dtRow.Length > 0 Then
            '            .SelectedValue = CInt(dtRow(0).Item("exchangerateunkid"))
            '        End If
            '    End If
            'End With

            'Sohail (11 Sep 2010) -- Start
            'Sohail (29 Oct 2012) -- Start
            'TRA - ENHANCEMENT
            'dsCombo = objTranHead.getComboList("TranHead", True, 0, 0, -1)
            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'dsCombo = objTranHead.getComboList("TranHead", True, 0, 0, -1, , True)

            'S.SANDEEP [12-JUL-2018] -- START
            'ISSUE/ENHANCEMENT : {Ref#223|ARUTI-239}
            'dsCombo = objTranHead.getComboList(FinancialYear._Object._DatabaseName, "TranHead", True, 0, 0, -1, , True)
            Dim intPerformanceHeadSetting As Integer = 2
            If menAction = enAction.EDIT_ONE Then
                intPerformanceHeadSetting = 0
            End If
            dsCombo = objTranHead.getComboList(FinancialYear._Object._DatabaseName, "TranHead", True, 0, 0, -1, , True, , , , , intPerformanceHeadSetting)
            'S.SANDEEP [12-JUL-2018] -- END


            'Sohail (21 Aug 2015) -- End
            'Sohail (29 Oct 2012) -- End
            dtTable = New DataView(dsCombo.Tables("TranHead"), "typeof_id <> " & enTypeOf.Salary & "", "", DataViewRowState.CurrentRows).ToTable 'Except Salary Heads
            With cboTrnHead
                .ValueMember = "tranheadunkid"
                .DisplayMember = "Name"
                .DataSource = dtTable
                If .Items.Count > 0 Then .SelectedValue = 0
            End With
            'Sohail (11 Sep 2010) -- End

            'Sohail (08 Nov 2011) -- Start
            dsCombo = objCostCenter.getComboList("CostCenter", True)
            With cboCostCenter
                .ValueMember = "costcenterunkid"
                .DisplayMember = "costcentername"
                .DataSource = dsCombo.Tables("CostCenter")
                .SelectedValue = 0
            End With
            'Sohail (08 Nov 2011) -- End

            'Sohail (13 Jan 2012) -- Start
            'TRA - ENHANCEMENT
            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'dsCombo = objPeriod.getListForCombo(enModuleReference.Payroll, FinancialYear._Object._YearUnkid, "Period", True, enStatusType.Open)
            dsCombo = objPeriod.getListForCombo(enModuleReference.Payroll, FinancialYear._Object._YearUnkid, FinancialYear._Object._DatabaseName, FinancialYear._Object._Database_Start_Date, "Period", True, enStatusType.Open)
            'Sohail (21 Aug 2015) -- End
            With cboPeriod
                .BeginUpdate()
                .ValueMember = "periodunkid"
                .DisplayMember = "name"
                .DataSource = dsCombo.Tables("Period")
                .EndUpdate()
            End With
            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'dsCombo = objPeriod.getListForCombo(enModuleReference.Payroll, FinancialYear._Object._YearUnkid, "Period", True, enStatusType.Open)
            dsCombo = objPeriod.getListForCombo(enModuleReference.Payroll, FinancialYear._Object._YearUnkid, FinancialYear._Object._DatabaseName, FinancialYear._Object._Database_Start_Date, "Period", True, enStatusType.Open)
            'Sohail (21 Aug 2015) -- End
            With cboCopyEDPeriod
                .BeginUpdate()
                .ValueMember = "periodunkid"
                .DisplayMember = "name"
                .DataSource = dsCombo.Tables("Period")
                .EndUpdate()
            End With
            'Sohail (13 Jan 2012) -- End
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        Finally
            objEmployee = Nothing
            objMaster = Nothing
            'objVendor = Nothing
            objExRate = Nothing
            objCostCenter = Nothing 'Sohail (08 Nov 2011)
            objPeriod = Nothing 'Sohail (13 Jan 2012)
        End Try
    End Sub

    'Sohail (03 Mar 2012) -- Start
    'TRA - ENHANCEMENT
    Private Sub FillCombo_Employee()
        Dim objEmployee As New clsEmployee_Master
        Dim dsList As DataSet
        Try


            'Anjan [10 June 2015] -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'dsList = objEmployee.GetEmployeeList("EmployeeList", True, , , , , , , , , , , , , mdtPeriod_startdate, mdtPeriod_enddate)

            dsList = objEmployee.GetEmployeeList(FinancialYear._Object._DatabaseName, _
                                           User._Object._Userunkid, _
                                           FinancialYear._Object._YearUnkid, _
                                           Company._Object._Companyunkid, _
                                           mdtPeriod_startdate, _
                                           mdtPeriod_enddate, _
                                           ConfigParameter._Object._UserAccessModeSetting, _
                                           True, False, "EmployeeList", True)
            'Anjan [10 June 2015] -- End

            With cboEmployee
                .BeginUpdate()
                .ValueMember = "employeeunkid"
                .DisplayMember = "employeename"
                .DataSource = dsList.Tables("EmployeeList")
                If .Items.Count > 0 Then .SelectedIndex = 0
                .EndUpdate()
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillComboEmployee", mstrModuleName)
        Finally
            objEmployee = Nothing
        End Try
    End Sub

    Private Sub SetVisibility()

        Try
            objbtnAddMembership.Enabled = User._Object.Privilege._AddMembership

            'Sohail (13 Jan 2012) -- Start
            'TRA - ENHANCEMENT
            TabED.SelectedTab = tabpgED
            btnSave.Visible = True
            btnSaveEDHistory.Visible = False
            'Sohail (13 Jan 2012) -- End
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetVisibility", mstrModuleName)
        End Try

    End Sub


    'Sohail (13 Jan 2012) -- Start
    'TRA - ENHANCEMENT
    Private Sub FillList_ShowHistory()
        Dim objTranHead As New clsTransactionHead
        Dim dsTranHead As DataSet
        Dim objDic As New Dictionary(Of Integer, ArrayList)
        Dim arrList As ArrayList
        'Dim dsList As DataSet
        Try
            lvEDHistory.Items.Clear()
            If mdtTable Is Nothing OrElse mdtTable.Rows.Count <= 0 Then Exit Sub

            'dsList = objED.GetList("EDHistory", CInt(cboEmployee.SelectedValue), , , , , , , , , , , "employeename, employeeunkid, cfcommon_period_tran.end_date DESC, trnheadtype_id, typeof_id, calctype_id DESC")
            mdtTable = New DataView(mdtTable, "", "end_date DESC, trnheadtype_id, typeof_id, calctype_id DESC", DataViewRowState.CurrentRows).ToTable

            Dim lvItem As ListViewItem
            For Each dsRow As DataRow In mdtTable.Rows
                lvItem = New ListViewItem

                lvItem.Text = dsRow.Item("period_name").ToString
                lvItem.Tag = CInt(dsRow.Item("periodunkid"))

                lvItem.SubItems.Add(dsRow.Item("trnheadname").ToString)
                lvItem.SubItems(colhTranHead.Index).Tag = CInt(dsRow.Item("tranheadunkid"))

                If objDic.ContainsKey(CInt(dsRow.Item("tranheadunkid"))) = False Then
                    dsTranHead = objTranHead.GetList("TranHead", CInt(dsRow.Item("tranheadunkid")), , True)
                    If dsTranHead.Tables("TranHead").Rows.Count > 0 Then
                        arrList = New ArrayList
                        arrList.Add(dsTranHead.Tables("TranHead").Rows(0).Item("HeadType").ToString)
                        arrList.Add(dsTranHead.Tables("TranHead").Rows(0).Item("calctype").ToString)

                        objDic.Add(CInt(dsRow.Item("tranheadunkid")), arrList)

                        lvItem.SubItems.Add(dsTranHead.Tables("TranHead").Rows(0).Item("HeadType").ToString)
                        lvItem.SubItems(colhTranheadType.Index).Tag = CInt(dsTranHead.Tables("TranHead").Rows(0).Item("trnheadtype_id"))

                        lvItem.SubItems.Add(dsTranHead.Tables("TranHead").Rows(0).Item("calctype").ToString)
                        lvItem.SubItems(colhCalcType.Index).Tag = CInt(dsTranHead.Tables("TranHead").Rows(0).Item("calctype_id"))

                    Else
                        lvItem.SubItems.Add("")
                        lvItem.SubItems.Add("")
                    End If
                Else
                    arrList = objDic.Item(CInt(dsRow.Item("tranheadunkid")))
                    lvItem.SubItems.Add(arrList.Item(0).ToString)
                    lvItem.SubItems.Add(arrList.Item(1).ToString)
                End If

                If CDec(dsRow.Item("amount").ToString) = 0 Then
                    lvItem.SubItems.Add("")
                Else
                    lvItem.SubItems.Add(Format(dsRow.Item("amount"), mstrCurrencyFormat))
                End If

                lvEDHistory.Items.Add(lvItem)
            Next

            lvEDHistory.GroupingColumn = colhPeriod
            lvEDHistory.DisplayGroups(True)

            If lvEDHistory.Items.Count > 3 Then
                colhCalcType.Width = 100 - 18
            Else
                colhCalcType.Width = 100
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillList_ShowHistory", mstrModuleName)
        End Try
    End Sub
    'Sohail (13 Jan 2012) -- End

#End Region

#Region " Private Functions "
    Private Function IsValidate() As Boolean
        Dim strMsg As String = ""
        Dim strHeadName As String = ""
        'Dim dblAmount As Double = 0
        Dim dsList As DataSet = Nothing
        Dim objTrnFormula As New clsTranheadFormulaTran
        Dim objED As New clsEarningDeduction
        Dim objTranHead As New clsTransactionHead
        Dim objTnA As New clsTnALeaveTran 'Sohail (16 Oct 2012)

        Try
            'Sohail (03 Sep 2010) -- Start
            'If txtAmount.Text.Length > 0 Then
            '    dblAmount = cdec(txtAmount.Text)
            'End If
            'Sohail (03 Sep 2010) -- End

            If CInt(cboEmployee.SelectedValue) <= 0 Then
                strMsg = Language.getMessage(mstrModuleName, 1, "Please Select Employee. Employee is mandatory information.")
                eZeeMsgBox.Show(strMsg, enMsgBoxStyle.Information)
                cboEmployee.Focus()
                Exit Function
                'Sohail (11 Sep 2010) -- Start
                'ElseIf CInt(cboTrnHeadType.SelectedValue) <= 0 Then
                '    strMsg = Language.getMessage(mstrModuleName, 2, "Please Select Transaction Head Type. Transaction Head Type is mandatory information.")
                '    eZeeMsgBox.Show(strMsg, enMsgBoxStyle.Information)
                '    cboTrnHeadType.Focus()
                '    Exit Function
                'ElseIf CInt(cboTypeOf.SelectedValue) <= 0 Then
                '    strMsg = Language.getMessage(mstrModuleName, 3, "Please Select Transaction Type Of. Transaction Type Of is mandatory information.")
                '    eZeeMsgBox.Show(strMsg, enMsgBoxStyle.Information)
                '    cboTypeOf.Focus()
                '    Exit Function
                'Sohail (11 Sep 2010) -- End
                'Sohail (13 Jan 2012) -- Start
                'TRA - ENHANCEMENT
            ElseIf CInt(cboPeriod.SelectedValue) <= 0 Then
                strMsg = Language.getMessage(mstrModuleName, 9, "Please Select Period. Period is mandatory information.")
                eZeeMsgBox.Show(strMsg, enMsgBoxStyle.Information)
                cboPeriod.Focus()
                Exit Function
                'Sohail (13 Jan 2012) -- End
            ElseIf CInt(cboTrnHead.SelectedValue) <= 0 Then
                strMsg = Language.getMessage(mstrModuleName, 2, "Please Select Transaction Head. Transaction Head is mandatory information.")
                eZeeMsgBox.Show(strMsg, enMsgBoxStyle.Information)
                cboTrnHead.Focus()
                Exit Function
                'Anjan (06 Jun 2011)-Start
                'Issue : Rutta wanted on allow -ve ,0 amount on ED for flat rate. 
                'ElseIf txtAmount.Enabled = True And txtAmount.Decimal <= 0 Then 'Sohail (03 Sep 2010)
                'strMsg = Language.getMessage(mstrModuleName, 5, "Sorry, Amount can not be Zero.")
                'eZeeMsgBox.Show(strMsg, enMsgBoxStyle.Information)
                'txtAmount.Focus()
                'Exit Function
                'Anjan (06 Jun 2011)-End
                'ElseIf CInt(cboCurrencyCountry.SelectedValue) <= 0 Then
                '    strMsg = Language.getMessage(mstrModuleName, 6, "Please Select Currency. Currency is mandatory information.")
                '    eZeeMsgBox.Show(strMsg, enMsgBoxStyle.Information)
                '    cboCurrencyCountry.Focus()
                '    Exit Function
            End If

            'Sohail (09 Nov 2013) -- Start
            'TRA - ENHANCEMENT
            If dtpCumulativeStartDate.Checked = True AndAlso dtpCumulativeStartDate.Value.Date > mdtPeriod_enddate.Date Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 15, "Sorry, Cumulative Start Date should not be greater than.") & " " & mdtPeriod_enddate.ToShortDateString, enMsgBoxStyle.Information)
                Exit Try
            End If
            If dtpStopDate.Checked = True AndAlso dtpStopDate.Value.Date < mdtPeriod_startdate.Date Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 16, "Sorry, Stop Date should not be less than.") & " " & mdtPeriod_startdate.ToShortDateString, enMsgBoxStyle.Information)
                Exit Try
            End If
            'Sohail (09 Nov 2013) -- End
            'Sohail (24 Aug 2019) -- Start
            'NMB Payroll UAT # TC016 - 76.1 - System should be able to accept future amendments (Future period) and prorate. It should be possible to set start date of the specific transaction and prorate based on the start date. It should also be possible to set start date of the future period.
            If dtpEDStartDate.Checked = True AndAlso (dtpEDStartDate.Value.Date < mdtPeriod_startdate.Date OrElse dtpEDStartDate.Value.Date > mdtPeriod_enddate.Date) Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 19, "Sorry, E. D. Start Date should be in between selected period start date and end date."), enMsgBoxStyle.Information)
                Exit Try
            End If
            If dtpStopDate.Checked = True AndAlso dtpEDStartDate.Checked = True AndAlso eZeeDate.convertDate(dtpStopDate.Value.Date) < eZeeDate.convertDate(dtpEDStartDate.Value.Date) Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 20, "Sorry, E. D. Start Date should be greater than stop date."), enMsgBoxStyle.Information)
                Exit Try
            End If
            'Sohail (24 Aug 2019) -- End

            'Sohail (16 Oct 2012) -- Start
            'TRA - ENHANCEMENT
            If objTnA.IsPayrollProcessDone(CInt(cboPeriod.SelectedValue), cboEmployee.SelectedValue.ToString, mdtPeriod_enddate) = True Then
                'Sohail (19 Apr 2019) -- Start
                'Enhancement - 76.1 - Option to open process payroll screen if process payroll is already done message come.
                'eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 14, "Sorry, Process Payroll is already done for last date of selected Period. Please Void Process Payroll first for selected employee to assign earning deduction."), enMsgBoxStyle.Information)
                If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 14, "Sorry, Process Payroll is already done for last date of selected Period. Please Void Process Payroll first for selected employee to assign earning deduction.") & vbCrLf & vbCrLf & Language.getMessage(mstrModuleName, 18, "Do you want to void Payroll?"), CType(enMsgBoxStyle.Information + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.Yes Then
                    Dim objFrm As New frmProcessPayroll
                    objFrm.ShowDialog()
                End If
                'Sohail (19 Apr 2019) -- End
                Exit Try
            End If
            'Sohail (16 Oct 2012) -- End

            'Sohail (22 Jul 2010) -- Start
            dsList = objTrnFormula.getFlateRateHeadFromFormula(CInt(cboTrnHead.SelectedValue), "TranHead")
            For Each dsRow As DataRow In dsList.Tables("TranHead").Rows
                With dsRow

                    'Sohail (16 May 2012) -- Start
                    'TRA - ENHANCEMENT
                    'Do not prompt message if default value has been set on transaction head formula (other tham Computed Value)
                    If CInt(dsRow.Item("defaultvalue_id")) <> enTranHeadFormula_DefaultValue.COMPUTED_VALUE Then Continue For
                    'Sohail (16 May 2012) -- End

                    'Sohail (17 Jan 2012) -- Start
                    'TRA - ENHANCEMENT
                    'Sohail (21 Aug 2015) -- Start
                    'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                    'If objED.isExist(CInt(cboEmployee.SelectedValue), CInt(.Item("tranheadunkid").ToString), , CInt(cboPeriod.SelectedValue)) = False Then
                    If objED.isExist(FinancialYear._Object._DatabaseName, CInt(cboEmployee.SelectedValue), CInt(.Item("tranheadunkid").ToString), , CInt(cboPeriod.SelectedValue)) = False Then
                        'Sohail (21 Aug 2015) -- End
                        'If objED.isExist(CInt(cboEmployee.SelectedValue), CInt(.Item("tranheadunkid").ToString)) = False Then
                        'Sohail (17 Jan 2012) -- End
                        'Sohail (21 Aug 2015) -- Start
                        'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                        'objTranHead._Tranheadunkid = CInt(.Item("tranheadunkid").ToString)
                        objTranHead._Tranheadunkid(FinancialYear._Object._DatabaseName) = CInt(.Item("tranheadunkid").ToString)
                        'Sohail (21 Aug 2015) -- End
                        strHeadName = objTranHead._Trnheadname

                        'Sohail (14 Apr 2011) -- Start
                        'strMsg = Language.getMessage(mstrModuleName, 7, "The Formula of this Transaction Head contains " & strHeadName & " Tranasaction Head (type of Flate Rate)." & vbCrLf & "So please add " & strHeadName & " Transaction Head First.")
                        'eZeeMsgBox.Show(strMsg, enMsgBoxStyle.Information)
                        'Exit Function
                        strMsg = Language.getMessage(mstrModuleName, 3, "The Formula of this Transaction Head contains ") & strHeadName & Language.getMessage(mstrModuleName, 4, " Tranasaction Head (type of Flate Rate).") & vbCrLf & Language.getMessage(mstrModuleName, 5, "If you do not assign ") & strHeadName & Language.getMessage(mstrModuleName, 6, "   Transaction Head, its value will be considered as Zero.") & vbCrLf & vbCrLf & Language.getMessage(mstrModuleName, 7, "Do you want to proceed?")
                        If eZeeMsgBox.Show(strMsg, CType(enMsgBoxStyle.Question + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.No Then
                            Exit Function
                        End If
                        'Sohail (14 Apr 2011) -- End
                    End If
                End With
            Next
            'Sohail (22 Jul 2010) -- End

            'Sohail (26 May 2011) -- Start
            objTranHead = New clsTransactionHead
            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'objTranHead._Tranheadunkid = CInt(cboTrnHead.SelectedValue)
            objTranHead._Tranheadunkid(FinancialYear._Object._DatabaseName) = CInt(cboTrnHead.SelectedValue)
            'Sohail (21 Aug 2015) -- End
            If objTranHead._Formulaid.Trim = "" AndAlso (objTranHead._Calctype_Id = enCalcType.AsComputedValue OrElse objTranHead._Calctype_Id = enCalcType.AsComputedOnWithINEXCESSOFTaxSlab) Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 8, "Formula is not set on this Transaction Head. Please set formula from Transaction Head Form."), enMsgBoxStyle.Information)
                Return False
            End If
            'Sohail (26 May 2011) -- End

            'Sohail (13 Jan 2012) -- Start
            'TRA - ENHANCEMENT
            Dim dRow As DataRow()
            'Sohail (17 Jan 2012) -- Start
            'TRA - ENHANCEMENT
            '*** Allow to assign heads in any open period when future period ed already assigned
            'dRow = mdtTable.Select("AUD <> 'D' AND end_date > '" & eZeeDate.convertDate(mdtPeriod_enddate) & "' ")
            'If dRow.Length > 0 Then
            '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 10, "Sorry! Slab for this Period is already defined.."), enMsgBoxStyle.Information)
            '    Return False
            'End If
            'Sohail (17 Jan 2012) -- End

            If menAction = enAction.EDIT_ONE Then
                dRow = mdtTable.Select("edunkid <> " & mintEDUnkID & " AND AUD <> 'D' AND employeeunkid = " & CInt(cboEmployee.SelectedValue) & " AND tranheadunkid = " & CInt(cboTrnHead.SelectedValue) & " AND end_date = '" & eZeeDate.convertDate(mdtPeriod_enddate) & "' ")
            Else
                dRow = mdtTable.Select("AUD <> 'D' AND employeeunkid = " & CInt(cboEmployee.SelectedValue) & " AND tranheadunkid = " & CInt(cboTrnHead.SelectedValue) & " AND end_date = '" & eZeeDate.convertDate(mdtPeriod_enddate) & "' ")
            End If
            If dRow.Length > 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 10, "Sorry! This Transaction Head for this Period is already defined.."), enMsgBoxStyle.Information)
                Return False
            End If
            'Sohail (13 Jan 2012) -- End

            'Sohail (18 Jan 2012) -- Start
            'TRA - ENHANCEMENT
            If menAction <> enAction.EDIT_ONE AndAlso chkCopyPreviousEDSlab.Checked = True Then
                If (eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 12, "You have ticked Copy Previous ED Slab. This will copy Previous Period Slab." & vbCrLf & vbCrLf & "Do you want to continue?"), CType(enMsgBoxStyle.Question + enMsgBoxStyle.YesNo, enMsgBoxStyle))) = Windows.Forms.DialogResult.No Then
                    Return False
                End If
            End If
            'Sohail (18 Jan 2012) -- End

            Return True
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "IsValidate", mstrModuleName)
        Finally
            objTrnFormula = Nothing
            objED = Nothing
            objTranHead = Nothing
            dsList = Nothing
        End Try
    End Function
#End Region

#Region " Form's Events "

    Private Sub frmEarningDeduction_AddEdit_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        Try
            objED = Nothing
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmEarningDeduction_AddEdit_FormClosed", mstrModuleName)
        End Try
    End Sub

    Private Sub frmEarningDeduction_AddEdit_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown
        Try
            Select Case e.KeyCode
                Case Keys.Return
                    Windows.Forms.SendKeys.Send("{Tab}")
                Case Keys.S
                    If e.Control = True Then
                        Call btnSave.PerformClick()
                    End If
            End Select
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmEarningDeduction_AddEdit_KeyDown", mstrModuleName)
        End Try
    End Sub

    Private Sub frmEarningDeduction_AddEdit_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        objED = New clsEarningDeduction
        'Dim objExRate As New clsExchangeRate 'Sohail (01 Dec 2010)
        Try
            Call Set_Logo(Me, gApplicationType)

            'Anjan (02 Sep 2011)-Start
            'Issue : Including Language Settings.
            Language.setLanguage(Me.Name)
            Call OtherSettings()
            'Anjan (02 Sep 2011)-End 

            Call SetVisibility()
            Call SetColor()
            Call FillCombo()

            '*** Currency Format ***
            'Sohail (01 Dec 2010) -- Start
            'If objExRate.getComboList.Tables(0).Rows.Count > 0 Then
            '    objExRate._ExchangeRateunkid = 1
            '    mstrCurrencyFormat = objExRate._fmtCurrency
            'End If
            mstrCurrencyFormat = GUI.fmtCurrency
            'Sohail (01 Dec 2010) -- End
            If txtAmount.Text.Length = 0 Then txtAmount.Text = "0"
            txtAmount.Text = Format(txtAmount.Decimal, mstrCurrencyFormat) 'Sohail (01 Dec 2010), 'Sohail (11 May 2011)

            If menAction = enAction.EDIT_ONE Then
                'Sohail (21 Aug 2015) -- Start
                'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                'objED._Edunkid = mintEDUnkID
                objED._Edunkid(Nothing, FinancialYear._Object._DatabaseName) = mintEDUnkID
                'Sohail (21 Aug 2015) -- End
                cboEmployee.Enabled = False
                cboTrnHeadType.Enabled = False
                cboTypeOf.Enabled = False
                cboTrnHead.Enabled = False
                objbtnSearchEmployee.Enabled = False
                objbtnSearchTranHead.Enabled = False
                cboPeriod.Enabled = False 'Sohail (13 Jan 2012)
                'Sohail (18 Jan 2012) -- Start
                'TRA - ENHANCEMENT
                chkCopyPreviousEDSlab.Checked = False
                chkCopyPreviousEDSlab.Visible = False
                'Sohail (18 Jan 2012) -- End
            End If

            Call GetValue()

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmEarningDeduction_AddEdit_Load", mstrModuleName)
        Finally
            'objExRate = Nothing 'Sohail (01 Dec 2010) 
        End Try
    End Sub
    'Anjan (02 Sep 2011)-Start
    'Issue : Including Language Settings.
    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsEarningDeduction.SetMessages()
            objfrm._Other_ModuleNames = "clsEarningDeduction"
            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub
    'Anjan (02 Sep 2011)-End 
#End Region

#Region " ComboBox's Events "
    Private Sub cboTrnHeadType_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboTrnHeadType.SelectedIndexChanged
        'Dim objTranHead As New clsTransactionHead
        Dim objMaster As New clsMasterData
        Dim dsList As DataSet
        Dim dtTable As DataTable
        Try
            dsList = objMaster.getComboListTypeOf("TypeOf", CInt(cboTrnHeadType.SelectedValue))
            dtTable = New DataView(dsList.Tables("TypeOf"), "Id <>  " & enTypeOf.Salary & "", "", DataViewRowState.CurrentRows).ToTable 'Except Salary Heads

            With cboTypeOf
                .ValueMember = "Id"
                .DisplayMember = "Name"
                .DataSource = dtTable
                If .Items.Count > 0 Then .SelectedValue = 0
            End With
            'dsList = objTranHead.getComboList("TranHead", True, CInt(cboTrnHeadType.SelectedValue))
            'With cboTrnHead
            '    .ValueMember = "tranheadunkid"
            '    .DisplayMember = "Name"
            '    .DataSource = dsList.Tables("TranHead")
            '    If .Items.Count > 0 Then .SelectedValue = 0
            'End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboTrnHeadType_SelectedIndexChanged", mstrModuleName)
        Finally
            'objTranHead = Nothing
            objMaster = Nothing
            dsList = Nothing
            dtTable = Nothing
        End Try
    End Sub

    Private Sub cboTypeOf_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboTypeOf.SelectedIndexChanged
        Dim objTranHead As New clsTransactionHead
        Dim dsList As DataSet
        Dim dtTable As DataTable
        Try

            'Sohail (11 Sep 2010) -- Start
            'dsList = objTranHead.getComboList("TranHead", True, 0, , CInt(cboTypeOf.SelectedValue))
            'Sohail (29 Oct 2012) -- Start
            'TRA - ENHANCEMENT
            'dsList = objTranHead.getComboList("TranHead", True, CInt(cboTrnHeadType.SelectedValue), , CInt(IIf(CInt(cboTypeOf.SelectedValue) > 0, CInt(cboTypeOf.SelectedValue), -1)))
            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'dsList = objTranHead.getComboList("TranHead", True, CInt(cboTrnHeadType.SelectedValue), , CInt(IIf(CInt(cboTypeOf.SelectedValue) > 0, CInt(cboTypeOf.SelectedValue), -1)), , True)
            'Sohail (19 Dec 2016) -- Start
            'Issue - 64.1 - Could not edit membership heads on ED.
            'dsList = objTranHead.getComboList(FinancialYear._Object._DatabaseName, "TranHead", True, CInt(cboTrnHeadType.SelectedValue), , CInt(IIf(CInt(cboTypeOf.SelectedValue) > 0, CInt(cboTypeOf.SelectedValue), -1)), , True)
            If menAction = enAction.EDIT_ONE Then
                'S.SANDEEP [12-JUL-2018] -- START
                'ISSUE/ENHANCEMENT : {Ref#223|ARUTI-239}
                'dsList = objTranHead.getComboList(FinancialYear._Object._DatabaseName, "TranHead", True, CInt(cboTrnHeadType.SelectedValue), , CInt(IIf(CInt(cboTypeOf.SelectedValue) > 0, CInt(cboTypeOf.SelectedValue), -1)), , False)
                dsList = objTranHead.getComboList(FinancialYear._Object._DatabaseName, "TranHead", True, CInt(cboTrnHeadType.SelectedValue), , CInt(IIf(CInt(cboTypeOf.SelectedValue) > 0, CInt(cboTypeOf.SelectedValue), -1)), , False, , , , , 0)
                'S.SANDEEP [12-JUL-2018] -- END
            Else
                dsList = objTranHead.getComboList(FinancialYear._Object._DatabaseName, "TranHead", True, CInt(cboTrnHeadType.SelectedValue), , CInt(IIf(CInt(cboTypeOf.SelectedValue) > 0, CInt(cboTypeOf.SelectedValue), -1)), , True)
            End If
            'Sohail (19 Dec 2016) -- End
            'Sohail (21 Aug 2015) -- End
            'Sohail (29 Oct 2012) -- End
            dtTable = New DataView(dsList.Tables("TranHead"), "typeof_id <> " & enTypeOf.Salary & "", "", DataViewRowState.CurrentRows).ToTable 'Except Salary Heads
            'Sohail (11 Sep 2010) -- End
            With cboTrnHead
                .ValueMember = "tranheadunkid"
                .DisplayMember = "Name"
                '.DataSource = dsList.Tables("TranHead")
                .DataSource = dtTable
                If .Items.Count > 0 Then .SelectedValue = 0
            End With

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboTypeOf_SelectedIndexChanged", mstrModuleName)
        Finally
            objTranHead = Nothing
            dsList = Nothing
        End Try
    End Sub

    Private Sub cboTrnHead_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboTrnHead.SelectedIndexChanged
        Dim objTranHead As New clsTransactionHead
        Try
            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'objTranHead._Tranheadunkid = CInt(cboTrnHead.SelectedValue)
            objTranHead._Tranheadunkid(FinancialYear._Object._DatabaseName) = CInt(cboTrnHead.SelectedValue)
            'Sohail (21 Aug 2015) -- End
            'Sohail (09 Aug 2010) -- Start
            'If objTranHead._Calctype_Id = enCalcType.AsComputedValue Or _
            '    objTranHead._Calctype_Id = enCalcType.AsComputedOnWithINEXCESSOFTaxSlab Or _
            '    objTranHead._Calctype_Id = enCalcType.AsUserDefinedValue Or _
            '    objTranHead._Calctype_Id = enCalcType.FlatRate_Salary Or _
            '    objTranHead._Calctype_Id = enCalcType.OnAttendance Or _
            '    objTranHead._Calctype_Id = enCalcType.OnHourWorked Then

            '    txtAmount.Enabled = False
            '    txtAmount.BackColor = GUI.ColorOptional
            '    txtAmount.Text = "0.0"
            'Else
            '    txtAmount.Enabled = True
            '    txtAmount.BackColor = GUI.ColorComp
            'End If
            If objTranHead._Calctype_Id = enCalcType.FlatRate_Others Then
                txtAmount.Enabled = True
                txtAmount.BackColor = GUI.ColorComp
            Else
                txtAmount.Enabled = False
                txtAmount.BackColor = GUI.ColorOptional
                txtAmount.Text = "0.0"
            End If
            'Sohail (09 Aug 2010) -- Start
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboTrnHead_SelectedIndexChanged", mstrModuleName)
        Finally
            objTranHead = Nothing
        End Try
    End Sub

    'Private Sub cboCurrencyCountry_SelectedValueChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboCurrencyCountry.SelectedValueChanged
    '    Dim objExRate As New clsExchangeRate
    '    Try
    '        objExRate._ExchangeRateunkid = CInt(cboCurrencyCountry.SelectedValue)
    '        mstrCurrencyFormat = objExRate._fmtCurrency
    '        objlblCurrencyName.Text = objExRate._Currency_Sign

    '        If txtAmount.Text.Length = 0 Then txtAmount.Text = "0"
    '        txtAmount.Text = Format(cdec(txtAmount.Text), mstrCurrencyFormat)
    '    Catch ex As Exception
    '        DisplayError.Show("-1", ex.Message, "cboCurrencyCountry_SelectedValueChanged", mstrModuleName)
    '    Finally
    '        objExRate = Nothing
    '    End Try
    'End Sub

    'Sohail (11 Sep 2010) -- Start
    Private Sub cboEmployee_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboEmployee.SelectedIndexChanged
        Dim objEmployee As New clsEmployee_Master
        Dim objCC As New clscostcenter_master 'Sohail (08 Nov 2011)
        Dim dsCombo As DataSet
        Try
            If CInt(cboEmployee.SelectedValue) > 0 Then
                objbtnAddMembership.Enabled = True
            Else
                objbtnAddMembership.Enabled = False
            End If

            'Sohail (08 Nov 2011) -- Start
            lblDefCC.Text = ""
            If CInt(cboEmployee.SelectedValue) > 0 Then
                'S.SANDEEP [04 JUN 2015] -- START
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                'objEmployee._Employeeunkid = CInt(cboEmployee.SelectedValue)
                objEmployee._Employeeunkid(eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate)) = CInt(cboEmployee.SelectedValue)
                'S.SANDEEP [04 JUN 2015] -- END
                objCC._Costcenterunkid = objEmployee._Costcenterunkid
                lblDefCC.Text = Language.getMessage(mstrModuleName, 17, "Default Cost Center :") & " " & objCC._Costcentername

                'Sohail (13 Jan 2012) -- Start
                'TRA - ENHANCEMENT
                'Sohail (21 Aug 2015) -- Start
                'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                'objED._Employeeunkid = CInt(cboEmployee.SelectedValue)
                objED._Employeeunkid(FinancialYear._Object._DatabaseName) = CInt(cboEmployee.SelectedValue)
                'Sohail (21 Aug 2015) -- End
                mdtTable = objED._DataSource
                'Sohail (13 Jan 2012) -- End
            End If
            'Sohail (08 Nov 2011) -- End

            dsCombo = objEmployee.GetEmployee_MembershipCategory(CInt(cboEmployee.SelectedValue), True, "MemCategory") 'Sohail (13 Dec 2010)
            With cboMemCategory
                .ValueMember = "Mem_CatId"
                .DisplayMember = "Mem_CatName"
                .DataSource = dsCombo.Tables("MemCategory")
                .SelectedValue = 0
            End With

            'Sohail (13 Jan 2012) -- Start
            'TRA - ENHANCEMENT
            Call FillList_ShowHistory()
            'Sohail (13 Jan 2012) -- End

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboEmployee_SelectedIndexChanged", mstrModuleName)
        Finally
            objEmployee = Nothing
            objCC = Nothing 'Sohail (08 Nov 2011)
        End Try
    End Sub

    Private Sub cboMemCategory_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboMemCategory.SelectedIndexChanged
        Dim objEmployee As New clsEmployee_Master
        Dim dsCombo As DataSet
        Dim dtTable As DataTable
        Try
            dsCombo = clsEmployee_Master.GetEmployee_Membership(CInt(cboEmployee.SelectedValue), True, "Membership")
            dtTable = New DataView(dsCombo.Tables("Membership"), "Mem_CatId IN (0, " & CInt(cboMemCategory.SelectedValue) & ")", "", DataViewRowState.CurrentRows).ToTable
            With cboMembershipType
                .ValueMember = "Mem_Id"
                .DisplayMember = "Mem_Name"
                .DataSource = dtTable
                .SelectedValue = 0
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboMemCategory_SelectedIndexChanged", mstrModuleName)
        Finally
            objEmployee = Nothing
        End Try
    End Sub
    'Sohail (11 Sep 2010) -- End

    'Sohail (13 Jan 2012) -- Start
    'TRA - ENHANCEMENT
    Private Sub cboPeriod_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboPeriod.SelectedIndexChanged
        Dim objPeriod As New clscommom_period_Tran
        Try
            If CInt(cboPeriod.SelectedValue) > 0 Then
                'Sohail (21 Aug 2015) -- Start
                'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                'objPeriod._Periodunkid = CInt(cboPeriod.SelectedValue)
                objPeriod._Periodunkid(FinancialYear._Object._DatabaseName) = CInt(cboPeriod.SelectedValue)
                'Sohail (21 Aug 2015) -- End
                mdtPeriod_startdate = objPeriod._Start_Date
                mdtPeriod_enddate = objPeriod._End_Date
                Call FillCombo_Employee() 'Sohail (03 Mar 2012)
                'Sohail (21 Aug 2015) -- Start
                'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            Else
                mdtPeriod_startdate = eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate)
                mdtPeriod_enddate = eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate)
                'Sohail (21 Aug 2015) -- End
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboPeriod_SelectedIndexChanged", mstrModuleName)
        Finally
            objPeriod = Nothing
        End Try
    End Sub

    Private Sub cboCopyEDPeriod_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboCopyEDPeriod.SelectedIndexChanged
        Dim objPeriod As New clscommom_period_Tran
        Try
            If CInt(cboCopyEDPeriod.SelectedValue) > 0 Then
                'Sohail (21 Aug 2015) -- Start
                'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                'objPeriod._Periodunkid = CInt(cboCopyEDPeriod.SelectedValue)
                objPeriod._Periodunkid(FinancialYear._Object._DatabaseName) = CInt(cboCopyEDPeriod.SelectedValue)
                'Sohail (21 Aug 2015) -- End
                mdtCopyPeriod_startdate = objPeriod._Start_Date
                mdtCopyPeriod_enddate = objPeriod._End_Date
                mstrCopyPeriodName = objPeriod._Period_Name
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboPeriod_SelectedIndexChanged", mstrModuleName)
        Finally
            objPeriod = Nothing
        End Try
    End Sub
    'Sohail (13 Jan 2012) -- End

#End Region

#Region " Button's Events "
    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnClose_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Dim blnFlag As Boolean
        Try
            If IsValidate() = False Then Exit Sub

            Call SetValue()

            'S.SANDEEP [28-May-2018] -- START
            'ISSUE/ENHANCEMENT : {Audit Trails} 
            objED._FormName = mstrModuleName
            objED._LoginEmployeeUnkid = 0
            objED._ClientIP = getIP()
            objED._HostName = getHostName()
            objED._FromWeb = False
            objED._AuditUserId = User._Object._Userunkid
objED._CompanyUnkid = Company._Object._Companyunkid
            objED._AuditDate = ConfigParameter._Object._CurrentDateAndTime
            'S.SANDEEP [28-May-2018] -- END

            If menAction = enAction.EDIT_ONE Then
                'Sohail (11 Nov 2010) -- Start
                'blnFlag = objED.Update()
                'Sohail (18 Jan 2012) -- Start
                'TRA - ENHANCEMENT
                'blnFlag = objED.Update(False)
                'Sohail (07 Mar 2012) -- Start
                'TRA - ENHANCEMENT
                'blnFlag = objED.InsertAllByDataTable(cboEmployee.SelectedValue.ToString, mdtSingleED, True, False)
                'Sohail (26 Jul 2012) -- Start
                'TRA - ENHANCEMENT
                'blnFlag = objED.InsertAllByDataTable(cboEmployee.SelectedValue.ToString, mdtSingleED, True, False, chkCopyPreviousEDSlab.Checked)
                'Sohail (21 Aug 2015) -- Start
                'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                'blnFlag = objED.InsertAllByDataTable(cboEmployee.SelectedValue.ToString, mdtSingleED, True, False, chkCopyPreviousEDSlab.Checked, , , chkOverwritePrevEDSlabHeads.Checked)
                'Sohail (15 Dec 2018) -- Start
                'HJFMRI Enhancement - Copy Selected Period ED Slab option on Global Assign ED screen in 75.1.
                'blnFlag = objED.InsertAllByDataTable(FinancialYear._Object._DatabaseName, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, mdtPeriod_startdate, mdtPeriod_enddate, ConfigParameter._Object._UserAccessModeSetting, True, ConfigParameter._Object._IsIncludeInactiveEmp, cboEmployee.SelectedValue.ToString, mdtSingleED, True, User._Object._Userunkid, User._Object.Privilege._AllowToApproveEarningDeduction, ConfigParameter._Object._CurrentDateAndTime, False, chkCopyPreviousEDSlab.Checked, chkOverwritePrevEDSlabHeads.Checked, True, "")
                blnFlag = objED.InsertAllByDataTable(FinancialYear._Object._DatabaseName, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, mdtPeriod_startdate, mdtPeriod_enddate, ConfigParameter._Object._UserAccessModeSetting, True, ConfigParameter._Object._IsIncludeInactiveEmp, Nothing, cboEmployee.SelectedValue.ToString, mdtSingleED, True, User._Object._Userunkid, User._Object.Privilege._AllowToApproveEarningDeduction, ConfigParameter._Object._CurrentDateAndTime, False, chkCopyPreviousEDSlab.Checked, chkOverwritePrevEDSlabHeads.Checked, True, "")
                'Sohail (15 Dec 2018) -- End
                'Sohail (21 Aug 2015) -- End
                'Sohail (26 Jul 2012) -- End
                'Sohail (07 Mar 2012) -- End
                'Sohail (18 Jan 2012) -- End
                'Sohail (11 Nov 2010) -- End
                'Sohail (31 Oct 2019) -- Start
                'Mainspring Resourcing Ghana : Support Issue Id # 0004199 - Email notification not sent to approvers when there is uploaded, edited e & d.
                If blnFlag = True Then
                    If User._Object.Privilege._AllowToApproveEarningDeduction = False AndAlso mdtSingleED.Select("calctype_id = " & enCalcType.FlatRate_Others & " ").Length > 0 Then
                        If mdtSingleED.Columns.Contains("trnheadname") = True Then
                            mdtSingleED.Columns("trnheadname").ColumnName = "tranheadname"
                        End If
                        Call objED.SendMailToApprover(FinancialYear._Object._DatabaseName, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, User._Object._Userunkid, mdtPeriod_startdate, mdtPeriod_enddate, ConfigParameter._Object._UserAccessModeSetting, True, False, User._Object.Privilege._AllowToApproveEarningDeduction, mdtSingleED, CInt(cboPeriod.SelectedValue), GUI.fmtCurrency, enLogin_Mode.DESKTOP, mstrModuleName, 0)
                    End If
                End If
                'Sohail (31 Oct 2019) -- End
            Else
                'Sohail (11 Nov 2010) -- Start
                'blnFlag = objED.Insert()
                'Sohail (13 Jan 2012) -- Start
                'TRA - ENHANCEMENT
                'blnFlag = objED.Insert(False)
                'Sohail (18 Jan 2012) -- Start
                'TRA - ENHANCEMENT [****  Now when assigning single head, copy all previous slab heads]
                'blnFlag = objED.Insert(False, True)
                'Sohail (07 Mar 2012) -- Start
                'TRA - ENHANCEMENT
                'blnFlag = objED.InsertAllByDataTable(cboEmployee.SelectedValue.ToString, mdtSingleED, False, True)
                'Sohail (26 Jul 2012) -- Start
                'TRA - ENHANCEMENT
                'blnFlag = objED.InsertAllByDataTable(cboEmployee.SelectedValue.ToString, mdtSingleED, False, True, chkCopyPreviousEDSlab.Checked)
                'Sohail (21 Aug 2015) -- Start
                'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                'blnFlag = objED.InsertAllByDataTable(cboEmployee.SelectedValue.ToString, mdtSingleED, False, True, chkCopyPreviousEDSlab.Checked, , , chkOverwritePrevEDSlabHeads.Checked)
                'Sohail (15 Dec 2018) -- Start
                'HJFMRI Enhancement - Copy Selected Period ED Slab option on Global Assign ED screen in 75.1.
                'blnFlag = objED.InsertAllByDataTable(FinancialYear._Object._DatabaseName, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, mdtPeriod_startdate, mdtPeriod_enddate, ConfigParameter._Object._UserAccessModeSetting, True, ConfigParameter._Object._IsIncludeInactiveEmp, cboEmployee.SelectedValue.ToString, mdtSingleED, False, User._Object._Userunkid, User._Object.Privilege._AllowToApproveEarningDeduction, ConfigParameter._Object._CurrentDateAndTime, True, chkCopyPreviousEDSlab.Checked, chkOverwritePrevEDSlabHeads.Checked, True, "")
                blnFlag = objED.InsertAllByDataTable(FinancialYear._Object._DatabaseName, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, mdtPeriod_startdate, mdtPeriod_enddate, ConfigParameter._Object._UserAccessModeSetting, True, ConfigParameter._Object._IsIncludeInactiveEmp, Nothing, cboEmployee.SelectedValue.ToString, mdtSingleED, False, User._Object._Userunkid, User._Object.Privilege._AllowToApproveEarningDeduction, ConfigParameter._Object._CurrentDateAndTime, True, chkCopyPreviousEDSlab.Checked, chkOverwritePrevEDSlabHeads.Checked, True, "")
                'Sohail (15 Dec 2018) -- End
                'Sohail (21 Aug 2015) -- End
                'Sohail (26 Jul 2012) -- End
                'Sohail (07 Mar 2012) -- End
                'Sohail (18 Jan 2012) -- End
                'Sohail (13 Jan 2012) -- End
                'Sohail (11 Nov 2010) -- End
                'Sohail (31 Oct 2019) -- Start
                'Mainspring Resourcing Ghana : Support Issue Id # 0004199 - Email notification not sent to approvers when there is uploaded, edited e & d.
                If blnFlag = True Then
                    If User._Object.Privilege._AllowToApproveEarningDeduction = False AndAlso mdtSingleED.Select("calctype_id = " & enCalcType.FlatRate_Others & " ").Length > 0 Then
                        If mdtSingleED.Columns.Contains("trnheadname") = True Then
                            mdtSingleED.Columns("trnheadname").ColumnName = "tranheadname"
                        End If
                        Call objED.SendMailToApprover(FinancialYear._Object._DatabaseName, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, User._Object._Userunkid, mdtPeriod_startdate, mdtPeriod_enddate, ConfigParameter._Object._UserAccessModeSetting, True, False, User._Object.Privilege._AllowToApproveEarningDeduction, mdtSingleED, CInt(cboPeriod.SelectedValue), GUI.fmtCurrency, enLogin_Mode.DESKTOP, mstrModuleName, 0)
                    End If
                End If
                'Sohail (31 Oct 2019) -- End
            End If

            If blnFlag = False And objED._Message <> "" Then
                eZeeMsgBox.Show(objED._Message, enMsgBoxStyle.Information)
            End If

            If blnFlag Then
                mblnCancel = False
                If menAction = enAction.ADD_CONTINUE Then
                    objED = Nothing
                    objED = New clsEarningDeduction
                    mdtTable.Rows.Clear()

                    'Pinkal (25-APR-2012) -- Start
                    'Enhancement : TRA Changes
                    cboPeriod.Tag = CInt(cboPeriod.SelectedValue)
                    cboEmployee.Tag = CInt(cboEmployee.SelectedValue)
                    'Pinkal (25-APR-2012) -- End

                    Call GetValue()

                    'Pinkal (25-APR-2012) -- Start
                    'Enhancement : TRA Changes
                    cboPeriod.SelectedValue = CInt(cboPeriod.Tag)
                    cboEmployee.SelectedValue = CInt(cboEmployee.Tag)
                    'Pinkal (25-APR-2012) -- End

                    cboEmployee.Focus()
                Else
                    'Sohail (21 Aug 2015) -- Start
                    'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                    'mintEDUnkID = objED._Edunkid
                    'Sohail (10 Dec 2019) -- Start
                    'MAIN SPRING RESOURCES Support Issue Id # 0004311 : Add employee code, cost centre and class group columns in email notification.
                    'mintEDUnkID = objED._Edunkid(Nothing, FinancialYear._Object._DatabaseName)
                    'Sohail (10 Dec 2019) -- End
                    'Sohail (21 Aug 2015) -- End
                    Me.Close()
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnSave_Click", mstrModuleName)
        End Try
    End Sub

    'Sohail (13 Jan 2012) -- Start
    'TRA - ENHANCEMENT
    Private Sub btnSaveEDHistory_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSaveEDHistory.Click
        Dim objTranHead As New clsTransactionHead
        Dim blnFlag As Boolean
        Dim strEmpList As String = ""
        Try
            If CInt(cboEmployee.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please Select Employee. Employee is mandatory information."), enMsgBoxStyle.Information)
                If TabED.SelectedTab IsNot tabpgED Then TabED.SelectedTab = tabpgED
                cboEmployee.Focus()
                Exit Sub
            End If

            Dim dt As DataTable = New DataView(mdtTable, "AUD = 'A'", "", DataViewRowState.CurrentRows).ToTable
            If dt.Rows.Count <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 11, "Sorry! First Copy ED Slab to Save the Earning Deduction"), enMsgBoxStyle.Information)
                cboCopyEDPeriod.Focus()
                Exit Sub
            End If

          'Sohail (19 Apr 2019) -- Start
            'Enhancement - 76.1 - Option to open process payroll screen if process payroll is already done message come.
            Dim objTnA As New clsTnALeaveTran
            If objTnA.IsPayrollProcessDone(CInt(cboCopyEDPeriod.SelectedValue), cboEmployee.SelectedValue.ToString, mdtCopyPeriod_enddate) = True Then
                If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 14, "Sorry, Process Payroll is already done for last date of selected Period. Please Void Process Payroll first for selected employee to assign earning deduction.") & vbCrLf & vbCrLf & Language.getMessage(mstrModuleName, 18, "Do you want to void Payroll?"), CType(enMsgBoxStyle.Information + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.Yes Then
                    Dim objFrm As New frmProcessPayroll
                    objFrm.ShowDialog()
                End If
                Exit Try
            End If
            'Sohail (19 Apr 2019) -- End

            'S.SANDEEP [28-May-2018] -- START
            'ISSUE/ENHANCEMENT : {Audit Trails} 
            objED._FormName = mstrModuleName
            objED._LoginEmployeeUnkid = 0
            objED._ClientIP = getIP()
            objED._HostName = getHostName()
            objED._FromWeb = False
            objED._AuditUserId = User._Object._Userunkid
objED._CompanyUnkid = Company._Object._Companyunkid
            objED._AuditDate = ConfigParameter._Object._CurrentDateAndTime
            'S.SANDEEP [28-May-2018] -- END

            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'blnFlag = objED.InsertAllByDataTable(cboEmployee.SelectedValue.ToString, dt, chkOverwrite.Checked, False)
            'Sohail (15 Dec 2018) -- Start
            'HJFMRI Enhancement - Copy Selected Period ED Slab option on Global Assign ED screen in 75.1.
            'blnFlag = objED.InsertAllByDataTable(FinancialYear._Object._DatabaseName, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, mdtPeriod_startdate, mdtPeriod_enddate, ConfigParameter._Object._UserAccessModeSetting, True, ConfigParameter._Object._IsIncludeInactiveEmp, cboEmployee.SelectedValue.ToString, dt, chkOverwrite.Checked, User._Object._Userunkid, User._Object.Privilege._AllowToApproveEarningDeduction, ConfigParameter._Object._CurrentDateAndTime, False, , , True, "")
            blnFlag = objED.InsertAllByDataTable(FinancialYear._Object._DatabaseName, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, mdtPeriod_startdate, mdtPeriod_enddate, ConfigParameter._Object._UserAccessModeSetting, True, ConfigParameter._Object._IsIncludeInactiveEmp, Nothing, cboEmployee.SelectedValue.ToString, dt, chkOverwrite.Checked, User._Object._Userunkid, User._Object.Privilege._AllowToApproveEarningDeduction, ConfigParameter._Object._CurrentDateAndTime, False, , , True, "")
            'Sohail (15 Dec 2018) -- End
            'Sohail (21 Aug 2015) -- End

            If blnFlag = False AndAlso objED._Message <> "" Then
                eZeeMsgBox.Show(objED._Message, enMsgBoxStyle.Information)
            End If

            If blnFlag = True Then
                mblnCancel = False 'Sohail (17 Jan 2012)
                Me.Close()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnSaveEDHistory_Click", mstrModuleName)
        End Try
    End Sub
    'Sohail (13 Jan 2012) -- End

#End Region

#Region " Textbox's Events "
    'Private Sub txtAmount_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtAmount.LostFocus
    '   Try
    'Sohail (03 Sep 2010) -- Start
    'If txtAmount.Text.Length = 0 Then txtAmount.Text = "0"
    'txtAmount.Text = Format(cdec(txtAmount.Text), mstrCurrencyFormat)
    'txtAmount.Text = Format(CDec(txtAmount.Text), mstrCurrencyFormat)
    'Sohail (03 Sep 2010) -- End
    '  Catch ex As Exception
    '     DisplayError.Show("-1", ex.Message, "txtAmount_LostFocus", mstrModuleName)
    'End Try

    'End Sub

    Private Sub txtAmount_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtAmount.TextChanged

        Try
            If txtAmount.SelectionLength <= 0 Then
                Exit Sub
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "txtAmount_TextChanged", mstrModuleName)
        End Try

    End Sub
#End Region

    'Sohail (26 Jul 2012) -- Start
    'TRA - ENHANCEMENT
#Region " Checkbox's Events "
    Private Sub chkCopyPreviousEDSlab_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkCopyPreviousEDSlab.CheckedChanged
        Try
            If chkCopyPreviousEDSlab.Checked = False Then chkOverwritePrevEDSlabHeads.Checked = False
            chkOverwritePrevEDSlabHeads.Enabled = chkCopyPreviousEDSlab.Checked
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "chkCopyPreviousEDSlab_CheckedChanged", mstrModuleName)
        End Try
    End Sub
#End Region
    'Sohail (26 Jul 2012) -- End

#Region " Other Control's Events "
    Private Sub objbtnSearchEmployee_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchEmployee.Click
        Dim objfrm As New frmCommonSearch
        'Dim objEmployee As New clsEmployee_Master 'Sohail (03 Mar 2012)
        'Dim dsList As DataSet 'Sohail (03 Mar 2012)
        Try
            'Anjan (02 Sep 2011)-Start
            'Issue : Including Language Settings.
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If
            'Anjan (02 Sep 2011)-End 

            'Sohail (06 Jan 2012) -- Start
            'TRA - ENHANCEMENT
            'dsList = objEmployee.GetEmployeeList("EmployeeList")
            'Sohail (03 Mar 2012) -- Start
            'TRA - ENHANCEMENT
            'If menAction = enAction.EDIT_ONE Then
            '    dsList = objEmployee.GetEmployeeList("EmployeeList", True, , , , , , , , , , , , , eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate))
            'Else
            '    dsList = objEmployee.GetEmployeeList("EmployeeList", True, , , , , , , , , , , , , ConfigParameter._Object._CurrentDateAndTime.Date, ConfigParameter._Object._CurrentDateAndTime.Date)
            'End If
            'Sohail (03 Mar 2012) -- End
            'Sohail (06 Jan 2012) -- End
            With cboEmployee
                'Sohail (03 Mar 2012) -- Start
                'TRA - ENHANCEMENT
                'objfrm.DataSource = dsList.Tables("EmployeeList")
                objfrm.DataSource = CType(cboEmployee.DataSource, DataTable)
                'Sohail (03 Mar 2012) -- End
                objfrm.ValueMember = .ValueMember
                objfrm.DisplayMember = .DisplayMember
                objfrm.CodeMember = "employeecode"
                If objfrm.DisplayDialog Then
                    .SelectedValue = objfrm.SelectedValue
                End If
                .Focus()
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchEmployee_Click", mstrModuleName)
        Finally
            objfrm = Nothing
            'objEmployee = Nothing 'Sohail (03 Mar 2012)
        End Try
    End Sub

    'Sohail (11 Sep 2010) -- Start
    Private Sub objbtnAddMembership_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnAddMembership.Click
        Dim objFrm As New frmEmployeeMaster
        Dim intRefId As Integer = CInt(cboEmployee.SelectedValue)
        Try
            'Anjan (02 Sep 2011)-Start
            'Issue : Including Language Settings.
            If User._Object._Isrighttoleft = True Then
                objFrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objFrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objFrm)
            End If
            'Anjan (02 Sep 2011)-End 

            objFrm.tabcEmployeeDetails.SelectedTab = objFrm.tabpMembership
            objFrm.cboMemCategory.Focus()
            If objFrm.displayDialog(intRefId, enAction.EDIT_ONE) = True Then
                Call cboEmployee_SelectedIndexChanged(sender, e)
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnAddMembership_Click", mstrModuleName)
        End Try
    End Sub
    'Sohail (11 Sep 2010) -- End

    'Sohail (09 Oct 2010) -- Start
    Private Sub objbtnSearchTranHead_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles objbtnSearchTranHead.Click
        Dim objfrm As New frmCommonSearch
        Dim objTranHead As New clsTransactionHead
        Dim dsList As DataSet
        Dim dtTable As DataTable
        Try
            'Anjan (02 Sep 2011)-Start
            'Issue : Including Language Settings.
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If
            'Anjan (02 Sep 2011)-End 

            'Sohail (29 Oct 2012) -- Start
            'TRA - ENHANCEMENT
            'dsList = objTranHead.getComboList("TranHead", False, CInt(cboTrnHeadType.SelectedValue), 0, CInt(IIf(CInt(cboTypeOf.SelectedValue) > 0, CInt(cboTypeOf.SelectedValue), -1)))
            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'dsList = objTranHead.getComboList("TranHead", False, CInt(cboTrnHeadType.SelectedValue), 0, CInt(IIf(CInt(cboTypeOf.SelectedValue) > 0, CInt(cboTypeOf.SelectedValue), -1)), , True)
            dsList = objTranHead.getComboList(FinancialYear._Object._DatabaseName, "TranHead", False, CInt(cboTrnHeadType.SelectedValue), 0, CInt(IIf(CInt(cboTypeOf.SelectedValue) > 0, CInt(cboTypeOf.SelectedValue), -1)), , True)
            'Sohail (21 Aug 2015) -- End
            'Sohail (29 Oct 2012) -- End
            dtTable = New DataView(dsList.Tables("TranHead"), "Typeof_id <> " & enTypeOf.Salary & "", "", DataViewRowState.CurrentRows).ToTable
            With cboTrnHead
                objfrm.DataSource = dtTable
                objfrm.ValueMember = .ValueMember
                objfrm.DisplayMember = .DisplayMember
                objfrm.CodeMember = "code"
                If objfrm.DisplayDialog Then
                    .SelectedValue = objfrm.SelectedValue
                End If
                .Focus()
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchEmployee_Click", mstrModuleName)
        Finally
            objfrm = Nothing
            objTranHead = Nothing
        End Try
    End Sub
    'Sohail (09 Oct 2010) -- End

    'Sohail (13 Jan 2012) -- Start
    'TRA - ENHANCEMENT
    Private Sub TabED_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TabED.SelectedIndexChanged
        Try
            If TabED.SelectedTab Is tabpgED Then
                btnSave.Visible = True
                btnSaveEDHistory.Visible = False
            Else
                btnSave.Visible = False
                btnSaveEDHistory.Visible = True
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "TabED_SelectedIndexChanged", mstrModuleName)
        End Try

    End Sub

    Private Sub lnkCopyEDSlab_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lnkCopyEDSlab.LinkClicked
        'Sohail (03 Mar 2012) -- Start
        'TRA - ENHANCEMENT
        Dim objEmployee As New clsEmployee_Master
        Dim dsList As DataSet
        'Sohail (03 Mar 2012) -- End
        Dim drED As DataRow
        Dim strPrevEndDate As String = ""
        Try
            If CInt(cboEmployee.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please Select Employee. Employee is mandatory information."), enMsgBoxStyle.Information)
                If TabED.SelectedTab IsNot tabpgED Then TabED.SelectedTab = tabpgED
                cboEmployee.Focus()
                Exit Try
            ElseIf CInt(cboCopyEDPeriod.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 9, "Please Select Period. Period is mandatory information."), enMsgBoxStyle.Information)
                cboCopyEDPeriod.Focus()
                Exit Try
            End If

            'Sohail (03 Mar 2012) -- Start
            'TRA - ENHANCEMENT

            'Anjan [10 June 2015] -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'dsList = objEmployee.GetEmployeeList("EmployeeList",  False, , CInt(cboEmployee.SelectedValue), , , , , , , , , , , mdtCopyPeriod_startdate, mdtCopyPeriod_enddate)

            dsList = objEmployee.GetEmployeeList(FinancialYear._Object._DatabaseName, _
                                           User._Object._Userunkid, _
                                           FinancialYear._Object._YearUnkid, _
                                           Company._Object._Companyunkid, _
                                           mdtCopyPeriod_startdate, _
                                           mdtCopyPeriod_enddate, _
                                           ConfigParameter._Object._UserAccessModeSetting, _
                                           True, False, "EmployeeList", False, CInt(cboEmployee.SelectedValue))
            'Anjan [10 June 2015] -- End

            If dsList.Tables("EmployeeList").Rows.Count <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 13, "Sorry! You can not assign the Earning Deduction Slab to this Employee for this Period. Reason:This Employee is getting terminated in this period."), enMsgBoxStyle.Information)
                cboCopyEDPeriod.Focus()
                Exit Try
            End If
            'Sohail (03 Mar 2012) -- End

            Dim drRow As DataRow()
            'Sohail (17 Jan 2012) -- Start
            'TRA - ENHANCEMENT
            '*** Allow to copy period for any open period when future period is already assigned.
            'drRow = mdtTable.Select("AUD <> 'D' AND end_date >= '" & eZeeDate.convertDate(mdtCopyPeriod_enddate) & "' ")
            'If drRow.Length > 0 Then
            '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 10, "Sorry! Slab for this Period is already defined.."), enMsgBoxStyle.Information)
            '    cboCopyEDPeriod.Focus()
            '    Exit Sub
            'End If
            Dim dtRow As DataRow()
            Dim blnIsExist As Boolean
            'Sohail (17 Jan 2012) -- End

            'Sohail (17 Jan 2012) -- Start
            'TRA - ENHANCEMENT
            'drRow = mdtTable.Select("AUD <> 'D' AND end_date <= '" & eZeeDate.convertDate(mdtCopyPeriod_enddate) & "' ", "end_date DESC")
            'Sohail (18 Jan 2012) -- Start
            'TRA - ENHANCEMENT
            'drRow = mdtTable.Select("AUD <> 'D' AND end_date <= '" & eZeeDate.convertDate(mdtCopyPeriod_startdate) & "' ", "end_date DESC")
            If chkAddNonRecurrentHeads.Checked = False Then     '*** Do not include non recurrent heads
                drRow = mdtTable.Select("AUD <> 'D' AND end_date <= '" & eZeeDate.convertDate(mdtCopyPeriod_startdate) & "' AND isrecurrent = 1 ", "end_date DESC")
            Else                                                '*** include non recurrent heads (All heads = no filter)
                drRow = mdtTable.Select("AUD <> 'D' AND end_date <= '" & eZeeDate.convertDate(mdtCopyPeriod_startdate) & "' ", "end_date DESC")
            End If
            'Sohail (18 Jan 2012) -- End
            'Sohail (17 Jan 2012) -- End
            If drRow.Length > 0 Then
                For Each dRow As DataRow In drRow
                    If strPrevEndDate <> "" AndAlso strPrevEndDate <> dRow.Item("end_date").ToString Then
                        Exit For
                    End If

                    drED = mdtTable.NewRow()

                    blnIsExist = False 'Sohail (17 Jan 2012)

                    drED.Item("edunkid") = -1 'CInt(dRow.Item("edunkid")) 'Sohail (17 Jan 2012)
                    drED.Item("employeeunkid") = CInt(dRow.Item("employeeunkid"))
                    drED.Item("tranheadunkid") = CInt(dRow.Item("tranheadunkid"))
                    drED.Item("trnheadname") = dRow.Item("trnheadname").ToString
                    drED.Item("batchtransactionunkid") = CInt(dRow.Item("batchtransactionunkid"))
                    'Sohail (17 Jan 2012) -- Start
                    'TRA - ENHANCEMENT
                    'drED.Item("amount") = CDec(dRow.Item("amount"))
                    dtRow = mdtTable.Select("AUD <> 'D' AND employeeunkid = " & CInt(dRow.Item("employeeunkid")) & " AND tranheadunkid = " & CInt(dRow.Item("tranheadunkid")) & " AND periodunkid = " & CInt(cboCopyEDPeriod.SelectedValue) & " AND end_date = '" & eZeeDate.convertDate(mdtCopyPeriod_enddate) & "' ")
                    If dtRow.Length > 0 Then
                        blnIsExist = True
                    End If
                    If blnIsExist = True AndAlso chkOverwrite.Checked = True Then
                        dtRow(0).Item("amount") = CDec(dRow.Item("amount"))
                        dtRow(0).Item("AUD") = "A"
                        mdtTable.AcceptChanges()
                        Continue For
                    ElseIf blnIsExist = True AndAlso chkOverwrite.Checked = False Then
                        Continue For
                    Else
                        drED.Item("amount") = CDec(dRow.Item("amount"))
                    End If
                    'Sohail (17 Jan 2012) -- End
                    drED.Item("isdeduct") = CBool(dRow.Item("isdeduct"))
                    drED.Item("trnheadtype_id") = CInt(dRow.Item("trnheadtype_id"))
                    drED.Item("typeof_id") = CInt(dRow.Item("typeof_id"))
                    drED.Item("calctype_id") = CInt(dRow.Item("calctype_id"))
                    drED.Item("computeon_id") = CInt(dRow.Item("computeon_id"))
                    drED.Item("formula") = dRow.Item("formula").ToString
                    drED.Item("formulaid") = dRow.Item("formulaid").ToString
                    drED.Item("currencyid") = CInt(dRow.Item("currencyid"))
                    drED.Item("vendorid") = CInt(dRow.Item("vendorid"))
                    drED.Item("userunkid") = CInt(dRow.Item("userunkid"))
                    drED.Item("isvoid") = CBool(dRow.Item("isvoid"))
                    drED.Item("voiduserunkid") = CInt(dRow.Item("voiduserunkid"))
                    drED.Item("voiddatetime") = dRow.Item("voiddatetime")
                    drED.Item("voidreason") = dRow.Item("voidreason").ToString
                    If IsDBNull(dRow.Item("membership_categoryunkid")) Then
                        drED.Item("membership_categoryunkid") = 0
                    Else
                        drED.Item("membership_categoryunkid") = CInt(dRow.Item("membership_categoryunkid"))
                    End If
                    drED.Item("isapproved") = CBool(dRow.Item("isapproved"))
                    drED.Item("approveruserunkid") = CInt(dRow.Item("approveruserunkid"))
                    drED.Item("periodunkid") = CInt(cboCopyEDPeriod.SelectedValue)
                    drED.Item("period_name") = mstrCopyPeriodName
                    drED.Item("start_date") = eZeeDate.convertDate(mdtCopyPeriod_startdate)
                    drED.Item("end_date") = eZeeDate.convertDate(mdtCopyPeriod_enddate)
                    'Sohail (18 Jan 2012) -- Start
                    'TRA - ENHANCEMENT
                    drED.Item("costcenterunkid") = 0
                    drED.Item("medicalrefno") = dRow.Item("medicalrefno").ToString
                    'Sohail (18 Jan 2012) -- End
                    'Sohail (09 Nov 2013) -- Start
                    'TRA - ENHANCEMENT
                    drED.Item("membershiptranunkid") = dRow.Item("membershiptranunkid")
                    drED.Item("disciplinefileunkid") = dRow.Item("disciplinefileunkid")
                    drED.Item("cumulative_startdate") = dRow.Item("cumulative_startdate")
                    drED.Item("stop_date") = dRow.Item("stop_date")
                    'Sohail (09 Nov 2013) -- End
                    'Sohail (24 Aug 2019) -- Start
                    'NMB Payroll UAT # TC016 - 76.1 - System should be able to accept future amendments (Future period) and prorate. It should be possible to set start date of the specific transaction and prorate based on the start date. It should also be possible to set start date of the future period.
                    drED.Item("edstart_date") = dRow.Item("edstart_date")
                    'Sohail (24 Aug 2019) -- End

                    drED.Item("AUD") = "A"

                    mdtTable.Rows.Add(drED)

                    strPrevEndDate = dRow.Item("end_date").ToString
                Next

                FillList_ShowHistory()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lnkCopyEDSlab_LinkClicked", mstrModuleName)
        End Try
    End Sub
    'Sohail (13 Jan 2012) -- End

#End Region

#Region " Message List "
    '1, "Please Select Employee. Employee is mandatory information."
    '2, "Please Select Transaction Head Type. Transaction Head Type is mandatory information."
    '3, "Please Select Transaction Type Of. Transaction Type Of is mandatory information."
    '4, "Please Select Transaction Head. Transaction Head is mandatory information."
    '5, "Sorry, Amount can not be Zero."
    '4, "Please Select Currency. Currency is mandatory information."
    '6, "The Formula of this Transaction Head contains " & strHeadName & " Tranasaction Head (type of Flate Rate)." & vbCrLf & "So please add " & strHeadName & " Transaction Head First."
    '7, "The Formula of this Transaction Head contains " & strHeadName & " Tranasaction Head (type of Flate Rate)." & vbCrLf & "If you do not assign " & strHeadName & " Transaction Head, its value will be cosidered as Zero." & vbCrLf & vbCrLf & "Do you want to proceed?"
    '8, "Formula is not set on this Trnasction Head. Please set formula from Transaction Head Form."
#End Region

    ' Private Sub txtAmount_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtAmount.TextChanged
    '
    '     Try
    '         If txtAmount.SelectionLength <= 0 Then
    '             Exit Sub
    '         End If
    '     Catch ex As Exception
    '         DisplayError.Show("-1", ex.Message, "txtAmount_TextChanged", mstrModuleName)
    '     End Try
    '
    ' End Sub
    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Private Sub OtherSettings()
        Try
            Me.SuspendLayout()
           
            Call SetLanguage()

            Me.gbEarningDeduction.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbEarningDeduction.ForeColor = GUI._eZeeContainerHeaderForeColor


            Me.eZeeHeader.GradientColor1 = GUI._HeaderBackColor1
            Me.eZeeHeader.GradientColor2 = GUI._HeaderBackColor2
            Me.eZeeHeader.BorderColor = GUI._HeaderBorderColor
            Me.eZeeHeader.HeaderTextForeColor = GUI._HeaderTitleFontColor
            Me.eZeeHeader.DescriptionForeColor = GUI._HeaderMessageFontColor


            Me.btnSave.GradientBackColor = GUI._ButttonBackColor
            Me.btnSave.GradientForeColor = GUI._ButttonFontColor

            Me.btnClose.GradientBackColor = GUI._ButttonBackColor
            Me.btnClose.GradientForeColor = GUI._ButttonFontColor

            Me.btnSaveEDHistory.GradientBackColor = GUI._ButttonBackColor
            Me.btnSaveEDHistory.GradientForeColor = GUI._ButttonFontColor


            Me.ResumeLayout()
        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "OtherSettings", mstrModuleName)
        End Try
    End Sub


    Private Sub SetLanguage()
        Try
            Me.Text = Language._Object.getCaption(Me.Name, Me.Text)

            Me.eZeeHeader.Title = Language._Object.getCaption(Me.eZeeHeader.Name & "_Title", Me.eZeeHeader.Title)
            Me.eZeeHeader.Message = Language._Object.getCaption(Me.eZeeHeader.Name & "_Message", Me.eZeeHeader.Message)
            Me.btnSave.Text = Language._Object.getCaption(Me.btnSave.Name, Me.btnSave.Text)
            Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
            Me.gbEarningDeduction.Text = Language._Object.getCaption(Me.gbEarningDeduction.Name, Me.gbEarningDeduction.Text)
            Me.lblVendor.Text = Language._Object.getCaption(Me.lblVendor.Name, Me.lblVendor.Text)
            Me.lblCurrencyCountry.Text = Language._Object.getCaption(Me.lblCurrencyCountry.Name, Me.lblCurrencyCountry.Text)
            Me.lblAmount.Text = Language._Object.getCaption(Me.lblAmount.Name, Me.lblAmount.Text)
            Me.lblTrnHead.Text = Language._Object.getCaption(Me.lblTrnHead.Name, Me.lblTrnHead.Text)
            Me.lblTrnHeadType.Text = Language._Object.getCaption(Me.lblTrnHeadType.Name, Me.lblTrnHeadType.Text)
            Me.lblEmployee.Text = Language._Object.getCaption(Me.lblEmployee.Name, Me.lblEmployee.Text)
            Me.lblTypeOf.Text = Language._Object.getCaption(Me.lblTypeOf.Name, Me.lblTypeOf.Text)
            Me.lblMembershipName.Text = Language._Object.getCaption(Me.lblMembershipName.Name, Me.lblMembershipName.Text)
            Me.lblMembershipType.Text = Language._Object.getCaption(Me.lblMembershipType.Name, Me.lblMembershipType.Text)
            Me.lblCostCenter.Text = Language._Object.getCaption(Me.lblCostCenter.Name, Me.lblCostCenter.Text)
            Me.lblDefCC.Text = Language._Object.getCaption(Me.lblDefCC.Name, Me.lblDefCC.Text)
            Me.lblPeriod.Text = Language._Object.getCaption(Me.lblPeriod.Name, Me.lblPeriod.Text)
            Me.tabpgED.Text = Language._Object.getCaption(Me.tabpgED.Name, Me.tabpgED.Text)
            Me.tabpgHistory.Text = Language._Object.getCaption(Me.tabpgHistory.Name, Me.tabpgHistory.Text)
            Me.colhPeriod.Text = Language._Object.getCaption(CStr(Me.colhPeriod.Tag), Me.colhPeriod.Text)
            Me.colhTranHead.Text = Language._Object.getCaption(CStr(Me.colhTranHead.Tag), Me.colhTranHead.Text)
            Me.colhTranheadType.Text = Language._Object.getCaption(CStr(Me.colhTranheadType.Tag), Me.colhTranheadType.Text)
            Me.colhCalcType.Text = Language._Object.getCaption(CStr(Me.colhCalcType.Tag), Me.colhCalcType.Text)
            Me.colhAmount.Text = Language._Object.getCaption(CStr(Me.colhAmount.Tag), Me.colhAmount.Text)
            Me.lblCopyEDPeriod.Text = Language._Object.getCaption(Me.lblCopyEDPeriod.Name, Me.lblCopyEDPeriod.Text)
            Me.lnkCopyEDSlab.Text = Language._Object.getCaption(Me.lnkCopyEDSlab.Name, Me.lnkCopyEDSlab.Text)
            Me.btnSaveEDHistory.Text = Language._Object.getCaption(Me.btnSaveEDHistory.Name, Me.btnSaveEDHistory.Text)
            Me.chkOverwrite.Text = Language._Object.getCaption(Me.chkOverwrite.Name, Me.chkOverwrite.Text)
            Me.chkCopyPreviousEDSlab.Text = Language._Object.getCaption(Me.chkCopyPreviousEDSlab.Name, Me.chkCopyPreviousEDSlab.Text)
            Me.chkAddNonRecurrentHeads.Text = Language._Object.getCaption(Me.chkAddNonRecurrentHeads.Name, Me.chkAddNonRecurrentHeads.Text)
            Me.lblMedicalRefNo.Text = Language._Object.getCaption(Me.lblMedicalRefNo.Name, Me.lblMedicalRefNo.Text)
            Me.chkOverwritePrevEDSlabHeads.Text = Language._Object.getCaption(Me.chkOverwritePrevEDSlabHeads.Name, Me.chkOverwritePrevEDSlabHeads.Text)
            Me.lblCumulativeStartDate.Text = Language._Object.getCaption(Me.lblCumulativeStartDate.Name, Me.lblCumulativeStartDate.Text)
            Me.lblStopDate.Text = Language._Object.getCaption(Me.lblStopDate.Name, Me.lblStopDate.Text)
			Me.lblEDStartDate.Text = Language._Object.getCaption(Me.lblEDStartDate.Name, Me.lblEDStartDate.Text)

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
        End Try
    End Sub


    Private Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Please Select Employee. Employee is mandatory information.")
            Language.setMessage(mstrModuleName, 2, "Please Select Transaction Head. Transaction Head is mandatory information.")
            Language.setMessage(mstrModuleName, 3, "The Formula of this Transaction Head contains")
            Language.setMessage(mstrModuleName, 4, " Tranasaction Head (type of Flate Rate).")
            Language.setMessage(mstrModuleName, 5, "If you do not assign")
            Language.setMessage(mstrModuleName, 6, "   Transaction Head, its value will be considered as Zero.")
            Language.setMessage(mstrModuleName, 7, "Do you want to proceed?")
            Language.setMessage(mstrModuleName, 8, "Formula is not set on this Transaction Head. Please set formula from Transaction Head Form.")
            Language.setMessage(mstrModuleName, 9, "Please Select Period. Period is mandatory information.")
            Language.setMessage(mstrModuleName, 10, "Sorry! This Transaction Head for this Period is already defined..")
            Language.setMessage(mstrModuleName, 11, "Sorry! First Copy ED Slab to Save the Earning Deduction")
            Language.setMessage(mstrModuleName, 12, "You have ticked Copy Previous ED Slab. This will copy Previous Period Slab." & vbCrLf & vbCrLf & "Do you want to continue?")
            Language.setMessage(mstrModuleName, 13, "Sorry! You can not assign the Earning Deduction Slab to this Employee for this Period. Reason:This Employee is getting terminated in this period.")
            Language.setMessage(mstrModuleName, 14, "Sorry, Process Payroll is already done for last date of selected Period. Please Void Process Payroll first for selected employee to assign earning deduction.")
            Language.setMessage(mstrModuleName, 15, "Sorry, Cumulative Start Date should not be greater than.")
            Language.setMessage(mstrModuleName, 16, "Sorry, Stop Date should not be less than.")
            Language.setMessage(mstrModuleName, 17, "Default Cost Center :")
			Language.setMessage(mstrModuleName, 18, "Do you want to void Payroll?")
			Language.setMessage(mstrModuleName, 19, "Sorry, E. D. Start Date should be in between selected period start date and end date.")
			Language.setMessage(mstrModuleName, 20, "Sorry, E. D. Start Date should be greater than stop date.")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class