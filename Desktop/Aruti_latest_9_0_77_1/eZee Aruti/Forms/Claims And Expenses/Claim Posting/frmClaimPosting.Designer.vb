﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmClaimPosting
    Inherits eZee.Common.eZeeForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmClaimPosting))
        Dim DataGridViewCellStyle6 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle7 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle8 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle4 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle5 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Me.objFooter = New eZee.Common.eZeeFooter
        Me.btnSave = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnClose = New eZee.Common.eZeeLightButton(Me.components)
        Me.gbFilterCriteria = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.rdAmtTranHeadMapping = New System.Windows.Forms.RadioButton
        Me.rdQtyTrnheadMapping = New System.Windows.Forms.RadioButton
        Me.objbtnReset = New eZee.Common.eZeeSearchResetButton(Me.components)
        Me.objbtnSearch = New eZee.Common.eZeeSearchResetButton(Me.components)
        Me.cboUOM = New System.Windows.Forms.ComboBox
        Me.LblUOM = New System.Windows.Forms.Label
        Me.cboViewBy = New System.Windows.Forms.ComboBox
        Me.lblViewBy = New System.Windows.Forms.Label
        Me.btnPosting = New eZee.Common.eZeeLightButton(Me.components)
        Me.objbtnSearchTranHead = New eZee.Common.eZeeGradientButton
        Me.objbtnSearchPeriod = New eZee.Common.eZeeGradientButton
        Me.objbtnSearchExpense = New eZee.Common.eZeeGradientButton
        Me.objbtnSearchExpCategory = New eZee.Common.eZeeGradientButton
        Me.LblTranHead = New System.Windows.Forms.Label
        Me.cboTranhead = New System.Windows.Forms.ComboBox
        Me.cboExpense = New System.Windows.Forms.ComboBox
        Me.LblExpense = New System.Windows.Forms.Label
        Me.lblPeriod = New System.Windows.Forms.Label
        Me.cboPeriod = New System.Windows.Forms.ComboBox
        Me.cboExpCategory = New System.Windows.Forms.ComboBox
        Me.lblExpCategory = New System.Windows.Forms.Label
        Me.objbtnSearchEmployee = New eZee.Common.eZeeGradientButton
        Me.cboEmployee = New System.Windows.Forms.ComboBox
        Me.lblEmployee = New System.Windows.Forms.Label
        Me.lblClaimNo = New System.Windows.Forms.Label
        Me.txtClaimNo = New eZee.TextBox.AlphanumericTextBox
        Me.btnUnposting = New eZee.Common.eZeeLightButton(Me.components)
        Me.dgvClaimPosting = New System.Windows.Forms.DataGridView
        Me.chkSelectAll = New System.Windows.Forms.CheckBox
        Me.DataGridViewTextBoxColumn1 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn2 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn3 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn4 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn5 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn6 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn7 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhCollaps = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objcolhIschecked = New System.Windows.Forms.DataGridViewCheckBoxColumn
        Me.dgcolhParticulars = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhExpense = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhSector = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhUnitprice = New eZee.Common.DataGridViewNumericTextBoxColumn
        Me.dgcolhQuantity = New eZee.Common.DataGridViewNumericTextBoxColumn
        Me.dgcolhAmount = New eZee.Common.DataGridViewNumericTextBoxColumn
        Me.dgcolhCurrency = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhQtytranhead = New System.Windows.Forms.DataGridViewComboBoxColumn
        Me.dgcolhAmttranhead = New System.Windows.Forms.DataGridViewComboBoxColumn
        Me.objdgcolhIsGroup = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhClaimFomMstID = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objFooter.SuspendLayout()
        Me.gbFilterCriteria.SuspendLayout()
        CType(Me.objbtnReset, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.objbtnSearch, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dgvClaimPosting, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'objFooter
        '
        Me.objFooter.BorderColor = System.Drawing.Color.Silver
        Me.objFooter.Controls.Add(Me.btnSave)
        Me.objFooter.Controls.Add(Me.btnClose)
        Me.objFooter.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.objFooter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objFooter.GradiantStyle = eZee.Common.eZeeFooter.GradientStyle.Up
        Me.objFooter.GradientColor1 = System.Drawing.SystemColors.Control
        Me.objFooter.GradientColor2 = System.Drawing.SystemColors.Control
        Me.objFooter.Location = New System.Drawing.Point(0, 428)
        Me.objFooter.Name = "objFooter"
        Me.objFooter.Size = New System.Drawing.Size(851, 55)
        Me.objFooter.TabIndex = 16
        '
        'btnSave
        '
        Me.btnSave.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnSave.BackColor = System.Drawing.Color.White
        Me.btnSave.BackgroundImage = CType(resources.GetObject("btnSave.BackgroundImage"), System.Drawing.Image)
        Me.btnSave.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnSave.BorderColor = System.Drawing.Color.Empty
        Me.btnSave.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnSave.FlatAppearance.BorderSize = 0
        Me.btnSave.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnSave.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSave.ForeColor = System.Drawing.Color.Black
        Me.btnSave.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnSave.GradientForeColor = System.Drawing.Color.Black
        Me.btnSave.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSave.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnSave.Location = New System.Drawing.Point(639, 13)
        Me.btnSave.Name = "btnSave"
        Me.btnSave.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSave.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnSave.Size = New System.Drawing.Size(97, 30)
        Me.btnSave.TabIndex = 122
        Me.btnSave.Text = "Save"
        Me.btnSave.UseVisualStyleBackColor = True
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnClose.BackColor = System.Drawing.Color.White
        Me.btnClose.BackgroundImage = CType(resources.GetObject("btnClose.BackgroundImage"), System.Drawing.Image)
        Me.btnClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnClose.BorderColor = System.Drawing.Color.Empty
        Me.btnClose.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnClose.FlatAppearance.BorderSize = 0
        Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClose.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.ForeColor = System.Drawing.Color.Black
        Me.btnClose.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnClose.GradientForeColor = System.Drawing.Color.Black
        Me.btnClose.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Location = New System.Drawing.Point(742, 13)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Size = New System.Drawing.Size(97, 30)
        Me.btnClose.TabIndex = 119
        Me.btnClose.Text = "&Close"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'gbFilterCriteria
        '
        Me.gbFilterCriteria.BorderColor = System.Drawing.Color.Black
        Me.gbFilterCriteria.Checked = False
        Me.gbFilterCriteria.CollapseAllExceptThis = False
        Me.gbFilterCriteria.CollapsedHoverImage = Nothing
        Me.gbFilterCriteria.CollapsedNormalImage = Nothing
        Me.gbFilterCriteria.CollapsedPressedImage = Nothing
        Me.gbFilterCriteria.CollapseOnLoad = False
        Me.gbFilterCriteria.Controls.Add(Me.rdAmtTranHeadMapping)
        Me.gbFilterCriteria.Controls.Add(Me.rdQtyTrnheadMapping)
        Me.gbFilterCriteria.Controls.Add(Me.objbtnReset)
        Me.gbFilterCriteria.Controls.Add(Me.objbtnSearch)
        Me.gbFilterCriteria.Controls.Add(Me.cboUOM)
        Me.gbFilterCriteria.Controls.Add(Me.LblUOM)
        Me.gbFilterCriteria.Controls.Add(Me.cboViewBy)
        Me.gbFilterCriteria.Controls.Add(Me.lblViewBy)
        Me.gbFilterCriteria.Controls.Add(Me.btnPosting)
        Me.gbFilterCriteria.Controls.Add(Me.objbtnSearchTranHead)
        Me.gbFilterCriteria.Controls.Add(Me.objbtnSearchPeriod)
        Me.gbFilterCriteria.Controls.Add(Me.objbtnSearchExpense)
        Me.gbFilterCriteria.Controls.Add(Me.objbtnSearchExpCategory)
        Me.gbFilterCriteria.Controls.Add(Me.LblTranHead)
        Me.gbFilterCriteria.Controls.Add(Me.cboTranhead)
        Me.gbFilterCriteria.Controls.Add(Me.cboExpense)
        Me.gbFilterCriteria.Controls.Add(Me.LblExpense)
        Me.gbFilterCriteria.Controls.Add(Me.lblPeriod)
        Me.gbFilterCriteria.Controls.Add(Me.cboPeriod)
        Me.gbFilterCriteria.Controls.Add(Me.cboExpCategory)
        Me.gbFilterCriteria.Controls.Add(Me.lblExpCategory)
        Me.gbFilterCriteria.Controls.Add(Me.objbtnSearchEmployee)
        Me.gbFilterCriteria.Controls.Add(Me.cboEmployee)
        Me.gbFilterCriteria.Controls.Add(Me.lblEmployee)
        Me.gbFilterCriteria.Controls.Add(Me.lblClaimNo)
        Me.gbFilterCriteria.Controls.Add(Me.txtClaimNo)
        Me.gbFilterCriteria.Controls.Add(Me.btnUnposting)
        Me.gbFilterCriteria.ExpandedHoverImage = Nothing
        Me.gbFilterCriteria.ExpandedNormalImage = Nothing
        Me.gbFilterCriteria.ExpandedPressedImage = Nothing
        Me.gbFilterCriteria.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbFilterCriteria.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbFilterCriteria.HeaderHeight = 25
        Me.gbFilterCriteria.HeaderMessage = ""
        Me.gbFilterCriteria.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbFilterCriteria.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbFilterCriteria.HeightOnCollapse = 0
        Me.gbFilterCriteria.LeftTextSpace = 0
        Me.gbFilterCriteria.Location = New System.Drawing.Point(7, 6)
        Me.gbFilterCriteria.Name = "gbFilterCriteria"
        Me.gbFilterCriteria.OpenHeight = 91
        Me.gbFilterCriteria.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbFilterCriteria.ShowBorder = True
        Me.gbFilterCriteria.ShowCheckBox = False
        Me.gbFilterCriteria.ShowCollapseButton = False
        Me.gbFilterCriteria.ShowDefaultBorderColor = True
        Me.gbFilterCriteria.ShowDownButton = False
        Me.gbFilterCriteria.ShowHeader = True
        Me.gbFilterCriteria.Size = New System.Drawing.Size(834, 114)
        Me.gbFilterCriteria.TabIndex = 18
        Me.gbFilterCriteria.Temp = 0
        Me.gbFilterCriteria.Text = "Claim Posting"
        Me.gbFilterCriteria.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'rdAmtTranHeadMapping
        '
        Me.rdAmtTranHeadMapping.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.rdAmtTranHeadMapping.Location = New System.Drawing.Point(636, 85)
        Me.rdAmtTranHeadMapping.Name = "rdAmtTranHeadMapping"
        Me.rdAmtTranHeadMapping.Size = New System.Drawing.Size(117, 17)
        Me.rdAmtTranHeadMapping.TabIndex = 333
        Me.rdAmtTranHeadMapping.TabStop = True
        Me.rdAmtTranHeadMapping.Text = "Amount Mapping"
        Me.rdAmtTranHeadMapping.UseVisualStyleBackColor = True
        '
        'rdQtyTrnheadMapping
        '
        Me.rdQtyTrnheadMapping.Checked = True
        Me.rdQtyTrnheadMapping.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.rdQtyTrnheadMapping.Location = New System.Drawing.Point(505, 85)
        Me.rdQtyTrnheadMapping.Name = "rdQtyTrnheadMapping"
        Me.rdQtyTrnheadMapping.Size = New System.Drawing.Size(117, 17)
        Me.rdQtyTrnheadMapping.TabIndex = 332
        Me.rdQtyTrnheadMapping.TabStop = True
        Me.rdQtyTrnheadMapping.Text = "Qty Mapping"
        Me.rdQtyTrnheadMapping.UseVisualStyleBackColor = True
        '
        'objbtnReset
        '
        Me.objbtnReset.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.objbtnReset.BackColor = System.Drawing.Color.Transparent
        Me.objbtnReset.ButtonType = eZee.Common.eZeeSearchResetButton.EnumButtonType.Reset
        Me.objbtnReset.Image = CType(resources.GetObject("objbtnReset.Image"), System.Drawing.Image)
        Me.objbtnReset.Location = New System.Drawing.Point(807, 0)
        Me.objbtnReset.Name = "objbtnReset"
        Me.objbtnReset.ResultMessage = ""
        Me.objbtnReset.SearchMessage = ""
        Me.objbtnReset.Size = New System.Drawing.Size(24, 24)
        Me.objbtnReset.TabIndex = 67
        Me.objbtnReset.TabStop = False
        '
        'objbtnSearch
        '
        Me.objbtnSearch.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.objbtnSearch.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearch.ButtonType = eZee.Common.eZeeSearchResetButton.EnumButtonType.Search
        Me.objbtnSearch.Image = CType(resources.GetObject("objbtnSearch.Image"), System.Drawing.Image)
        Me.objbtnSearch.Location = New System.Drawing.Point(784, 0)
        Me.objbtnSearch.Name = "objbtnSearch"
        Me.objbtnSearch.ResultMessage = ""
        Me.objbtnSearch.SearchMessage = ""
        Me.objbtnSearch.Size = New System.Drawing.Size(24, 24)
        Me.objbtnSearch.TabIndex = 66
        Me.objbtnSearch.TabStop = False
        '
        'cboUOM
        '
        Me.cboUOM.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboUOM.DropDownWidth = 350
        Me.cboUOM.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboUOM.FormattingEnabled = True
        Me.cboUOM.Location = New System.Drawing.Point(347, 83)
        Me.cboUOM.Name = "cboUOM"
        Me.cboUOM.Size = New System.Drawing.Size(127, 21)
        Me.cboUOM.TabIndex = 328
        '
        'LblUOM
        '
        Me.LblUOM.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblUOM.Location = New System.Drawing.Point(272, 85)
        Me.LblUOM.Name = "LblUOM"
        Me.LblUOM.Size = New System.Drawing.Size(68, 16)
        Me.LblUOM.TabIndex = 327
        Me.LblUOM.Text = "UOM"
        Me.LblUOM.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboViewBy
        '
        Me.cboViewBy.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboViewBy.DropDownWidth = 350
        Me.cboViewBy.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboViewBy.FormattingEnabled = True
        Me.cboViewBy.Location = New System.Drawing.Point(85, 31)
        Me.cboViewBy.Name = "cboViewBy"
        Me.cboViewBy.Size = New System.Drawing.Size(156, 21)
        Me.cboViewBy.TabIndex = 20
        '
        'lblViewBy
        '
        Me.lblViewBy.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblViewBy.Location = New System.Drawing.Point(8, 34)
        Me.lblViewBy.Name = "lblViewBy"
        Me.lblViewBy.Size = New System.Drawing.Size(71, 15)
        Me.lblViewBy.TabIndex = 19
        Me.lblViewBy.Text = "View Type"
        '
        'btnPosting
        '
        Me.btnPosting.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnPosting.BackColor = System.Drawing.Color.White
        Me.btnPosting.BackgroundImage = CType(resources.GetObject("btnPosting.BackgroundImage"), System.Drawing.Image)
        Me.btnPosting.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnPosting.BorderColor = System.Drawing.Color.Empty
        Me.btnPosting.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnPosting.FlatAppearance.BorderSize = 0
        Me.btnPosting.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnPosting.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnPosting.ForeColor = System.Drawing.Color.Black
        Me.btnPosting.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnPosting.GradientForeColor = System.Drawing.Color.Black
        Me.btnPosting.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnPosting.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnPosting.Location = New System.Drawing.Point(767, 42)
        Me.btnPosting.Name = "btnPosting"
        Me.btnPosting.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnPosting.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnPosting.Size = New System.Drawing.Size(58, 58)
        Me.btnPosting.TabIndex = 323
        Me.btnPosting.Text = "&Post"
        Me.btnPosting.UseVisualStyleBackColor = True
        '
        'objbtnSearchTranHead
        '
        Me.objbtnSearchTranHead.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchTranHead.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchTranHead.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchTranHead.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchTranHead.BorderSelected = False
        Me.objbtnSearchTranHead.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchTranHead.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objbtnSearchTranHead.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchTranHead.Location = New System.Drawing.Point(740, 57)
        Me.objbtnSearchTranHead.Name = "objbtnSearchTranHead"
        Me.objbtnSearchTranHead.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchTranHead.TabIndex = 321
        '
        'objbtnSearchPeriod
        '
        Me.objbtnSearchPeriod.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchPeriod.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchPeriod.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchPeriod.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchPeriod.BorderSelected = False
        Me.objbtnSearchPeriod.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchPeriod.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objbtnSearchPeriod.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchPeriod.Location = New System.Drawing.Point(740, 31)
        Me.objbtnSearchPeriod.Name = "objbtnSearchPeriod"
        Me.objbtnSearchPeriod.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchPeriod.TabIndex = 320
        '
        'objbtnSearchExpense
        '
        Me.objbtnSearchExpense.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchExpense.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchExpense.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchExpense.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchExpense.BorderSelected = False
        Me.objbtnSearchExpense.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchExpense.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objbtnSearchExpense.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchExpense.Location = New System.Drawing.Point(477, 57)
        Me.objbtnSearchExpense.Name = "objbtnSearchExpense"
        Me.objbtnSearchExpense.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchExpense.TabIndex = 319
        '
        'objbtnSearchExpCategory
        '
        Me.objbtnSearchExpCategory.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchExpCategory.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchExpCategory.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchExpCategory.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchExpCategory.BorderSelected = False
        Me.objbtnSearchExpCategory.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchExpCategory.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objbtnSearchExpCategory.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchExpCategory.Location = New System.Drawing.Point(477, 31)
        Me.objbtnSearchExpCategory.Name = "objbtnSearchExpCategory"
        Me.objbtnSearchExpCategory.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchExpCategory.TabIndex = 318
        '
        'LblTranHead
        '
        Me.LblTranHead.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblTranHead.Location = New System.Drawing.Point(500, 59)
        Me.LblTranHead.Name = "LblTranHead"
        Me.LblTranHead.Size = New System.Drawing.Size(73, 16)
        Me.LblTranHead.TabIndex = 316
        Me.LblTranHead.Text = "Tran. Head"
        Me.LblTranHead.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboTranhead
        '
        Me.cboTranhead.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboTranhead.DropDownWidth = 350
        Me.cboTranhead.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboTranhead.FormattingEnabled = True
        Me.cboTranhead.Location = New System.Drawing.Point(583, 57)
        Me.cboTranhead.Name = "cboTranhead"
        Me.cboTranhead.Size = New System.Drawing.Size(151, 21)
        Me.cboTranhead.TabIndex = 317
        '
        'cboExpense
        '
        Me.cboExpense.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboExpense.DropDownWidth = 350
        Me.cboExpense.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboExpense.FormattingEnabled = True
        Me.cboExpense.Location = New System.Drawing.Point(347, 57)
        Me.cboExpense.Name = "cboExpense"
        Me.cboExpense.Size = New System.Drawing.Size(127, 21)
        Me.cboExpense.TabIndex = 315
        '
        'LblExpense
        '
        Me.LblExpense.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblExpense.Location = New System.Drawing.Point(272, 59)
        Me.LblExpense.Name = "LblExpense"
        Me.LblExpense.Size = New System.Drawing.Size(68, 16)
        Me.LblExpense.TabIndex = 314
        Me.LblExpense.Text = "Expense"
        Me.LblExpense.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblPeriod
        '
        Me.lblPeriod.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPeriod.Location = New System.Drawing.Point(500, 33)
        Me.lblPeriod.Name = "lblPeriod"
        Me.lblPeriod.Size = New System.Drawing.Size(73, 16)
        Me.lblPeriod.TabIndex = 312
        Me.lblPeriod.Text = "Period"
        Me.lblPeriod.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboPeriod
        '
        Me.cboPeriod.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboPeriod.DropDownWidth = 350
        Me.cboPeriod.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboPeriod.FormattingEnabled = True
        Me.cboPeriod.Location = New System.Drawing.Point(583, 31)
        Me.cboPeriod.Name = "cboPeriod"
        Me.cboPeriod.Size = New System.Drawing.Size(151, 21)
        Me.cboPeriod.TabIndex = 313
        '
        'cboExpCategory
        '
        Me.cboExpCategory.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboExpCategory.DropDownWidth = 350
        Me.cboExpCategory.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboExpCategory.FormattingEnabled = True
        Me.cboExpCategory.Location = New System.Drawing.Point(347, 31)
        Me.cboExpCategory.Name = "cboExpCategory"
        Me.cboExpCategory.Size = New System.Drawing.Size(127, 21)
        Me.cboExpCategory.TabIndex = 311
        '
        'lblExpCategory
        '
        Me.lblExpCategory.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblExpCategory.Location = New System.Drawing.Point(272, 33)
        Me.lblExpCategory.Name = "lblExpCategory"
        Me.lblExpCategory.Size = New System.Drawing.Size(68, 16)
        Me.lblExpCategory.TabIndex = 310
        Me.lblExpCategory.Text = "Exp. Cat."
        Me.lblExpCategory.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objbtnSearchEmployee
        '
        Me.objbtnSearchEmployee.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchEmployee.BorderSelected = False
        Me.objbtnSearchEmployee.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchEmployee.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objbtnSearchEmployee.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchEmployee.Location = New System.Drawing.Point(247, 57)
        Me.objbtnSearchEmployee.Name = "objbtnSearchEmployee"
        Me.objbtnSearchEmployee.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchEmployee.TabIndex = 302
        '
        'cboEmployee
        '
        Me.cboEmployee.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboEmployee.DropDownWidth = 350
        Me.cboEmployee.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboEmployee.FormattingEnabled = True
        Me.cboEmployee.Location = New System.Drawing.Point(85, 57)
        Me.cboEmployee.Name = "cboEmployee"
        Me.cboEmployee.Size = New System.Drawing.Size(156, 21)
        Me.cboEmployee.TabIndex = 22
        '
        'lblEmployee
        '
        Me.lblEmployee.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEmployee.Location = New System.Drawing.Point(8, 59)
        Me.lblEmployee.Name = "lblEmployee"
        Me.lblEmployee.Size = New System.Drawing.Size(60, 16)
        Me.lblEmployee.TabIndex = 21
        Me.lblEmployee.Text = "Employee"
        Me.lblEmployee.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblClaimNo
        '
        Me.lblClaimNo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblClaimNo.Location = New System.Drawing.Point(8, 85)
        Me.lblClaimNo.Name = "lblClaimNo"
        Me.lblClaimNo.Size = New System.Drawing.Size(60, 16)
        Me.lblClaimNo.TabIndex = 23
        Me.lblClaimNo.Text = "Claim No."
        Me.lblClaimNo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtClaimNo
        '
        Me.txtClaimNo.Flags = 0
        Me.txtClaimNo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtClaimNo.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtClaimNo.Location = New System.Drawing.Point(85, 83)
        Me.txtClaimNo.Name = "txtClaimNo"
        Me.txtClaimNo.Size = New System.Drawing.Size(156, 21)
        Me.txtClaimNo.TabIndex = 24
        '
        'btnUnposting
        '
        Me.btnUnposting.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnUnposting.BackColor = System.Drawing.Color.White
        Me.btnUnposting.BackgroundImage = CType(resources.GetObject("btnUnposting.BackgroundImage"), System.Drawing.Image)
        Me.btnUnposting.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnUnposting.BorderColor = System.Drawing.Color.Empty
        Me.btnUnposting.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnUnposting.FlatAppearance.BorderSize = 0
        Me.btnUnposting.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnUnposting.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnUnposting.ForeColor = System.Drawing.Color.Black
        Me.btnUnposting.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnUnposting.GradientForeColor = System.Drawing.Color.Black
        Me.btnUnposting.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnUnposting.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnUnposting.Location = New System.Drawing.Point(767, 42)
        Me.btnUnposting.Name = "btnUnposting"
        Me.btnUnposting.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnUnposting.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnUnposting.Size = New System.Drawing.Size(58, 58)
        Me.btnUnposting.TabIndex = 330
        Me.btnUnposting.Text = "&UnPost"
        Me.btnUnposting.UseVisualStyleBackColor = True
        '
        'dgvClaimPosting
        '
        Me.dgvClaimPosting.AllowUserToAddRows = False
        Me.dgvClaimPosting.AllowUserToDeleteRows = False
        Me.dgvClaimPosting.AllowUserToResizeColumns = False
        Me.dgvClaimPosting.AllowUserToResizeRows = False
        Me.dgvClaimPosting.BackgroundColor = System.Drawing.Color.WhiteSmoke
        Me.dgvClaimPosting.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing
        Me.dgvClaimPosting.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.objdgcolhCollaps, Me.objcolhIschecked, Me.dgcolhParticulars, Me.dgcolhExpense, Me.dgcolhSector, Me.dgcolhUnitprice, Me.dgcolhQuantity, Me.dgcolhAmount, Me.dgcolhCurrency, Me.dgcolhQtytranhead, Me.dgcolhAmttranhead, Me.objdgcolhIsGroup, Me.objdgcolhClaimFomMstID})
        DataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle6.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle6.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle6.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle6.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle6.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle6.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgvClaimPosting.DefaultCellStyle = DataGridViewCellStyle6
        Me.dgvClaimPosting.Location = New System.Drawing.Point(7, 126)
        Me.dgvClaimPosting.MultiSelect = False
        Me.dgvClaimPosting.Name = "dgvClaimPosting"
        Me.dgvClaimPosting.RowHeadersVisible = False
        Me.dgvClaimPosting.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.CellSelect
        Me.dgvClaimPosting.Size = New System.Drawing.Size(834, 296)
        Me.dgvClaimPosting.TabIndex = 325
        '
        'chkSelectAll
        '
        Me.chkSelectAll.Location = New System.Drawing.Point(40, 133)
        Me.chkSelectAll.Name = "chkSelectAll"
        Me.chkSelectAll.Size = New System.Drawing.Size(13, 15)
        Me.chkSelectAll.TabIndex = 326
        Me.chkSelectAll.UseVisualStyleBackColor = True
        '
        'DataGridViewTextBoxColumn1
        '
        DataGridViewCellStyle7.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.DataGridViewTextBoxColumn1.DefaultCellStyle = DataGridViewCellStyle7
        Me.DataGridViewTextBoxColumn1.Frozen = True
        Me.DataGridViewTextBoxColumn1.HeaderText = ""
        Me.DataGridViewTextBoxColumn1.Name = "DataGridViewTextBoxColumn1"
        Me.DataGridViewTextBoxColumn1.ReadOnly = True
        Me.DataGridViewTextBoxColumn1.Width = 25
        '
        'DataGridViewTextBoxColumn2
        '
        DataGridViewCellStyle8.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.DataGridViewTextBoxColumn2.DefaultCellStyle = DataGridViewCellStyle8
        Me.DataGridViewTextBoxColumn2.Frozen = True
        Me.DataGridViewTextBoxColumn2.HeaderText = "Particulars"
        Me.DataGridViewTextBoxColumn2.Name = "DataGridViewTextBoxColumn2"
        Me.DataGridViewTextBoxColumn2.ReadOnly = True
        Me.DataGridViewTextBoxColumn2.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn2.Width = 250
        '
        'DataGridViewTextBoxColumn3
        '
        Me.DataGridViewTextBoxColumn3.HeaderText = "Expense"
        Me.DataGridViewTextBoxColumn3.Name = "DataGridViewTextBoxColumn3"
        Me.DataGridViewTextBoxColumn3.ReadOnly = True
        Me.DataGridViewTextBoxColumn3.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'DataGridViewTextBoxColumn4
        '
        Me.DataGridViewTextBoxColumn4.HeaderText = "Sector"
        Me.DataGridViewTextBoxColumn4.Name = "DataGridViewTextBoxColumn4"
        Me.DataGridViewTextBoxColumn4.ReadOnly = True
        Me.DataGridViewTextBoxColumn4.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'DataGridViewTextBoxColumn5
        '
        Me.DataGridViewTextBoxColumn5.HeaderText = "IsGroup"
        Me.DataGridViewTextBoxColumn5.Name = "DataGridViewTextBoxColumn5"
        Me.DataGridViewTextBoxColumn5.ReadOnly = True
        Me.DataGridViewTextBoxColumn5.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn5.Visible = False
        '
        'DataGridViewTextBoxColumn6
        '
        Me.DataGridViewTextBoxColumn6.HeaderText = "objdgcolhClaimFomMstID"
        Me.DataGridViewTextBoxColumn6.Name = "DataGridViewTextBoxColumn6"
        Me.DataGridViewTextBoxColumn6.ReadOnly = True
        Me.DataGridViewTextBoxColumn6.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn6.Visible = False
        '
        'DataGridViewTextBoxColumn7
        '
        Me.DataGridViewTextBoxColumn7.HeaderText = "objdgcolhClaimFomMstID"
        Me.DataGridViewTextBoxColumn7.Name = "DataGridViewTextBoxColumn7"
        Me.DataGridViewTextBoxColumn7.ReadOnly = True
        Me.DataGridViewTextBoxColumn7.Visible = False
        '
        'objdgcolhCollaps
        '
        DataGridViewCellStyle1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objdgcolhCollaps.DefaultCellStyle = DataGridViewCellStyle1
        Me.objdgcolhCollaps.Frozen = True
        Me.objdgcolhCollaps.HeaderText = ""
        Me.objdgcolhCollaps.Name = "objdgcolhCollaps"
        Me.objdgcolhCollaps.ReadOnly = True
        Me.objdgcolhCollaps.Width = 25
        '
        'objcolhIschecked
        '
        Me.objcolhIschecked.Frozen = True
        Me.objcolhIschecked.HeaderText = ""
        Me.objcolhIschecked.Name = "objcolhIschecked"
        Me.objcolhIschecked.Width = 25
        '
        'dgcolhParticulars
        '
        DataGridViewCellStyle2.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dgcolhParticulars.DefaultCellStyle = DataGridViewCellStyle2
        Me.dgcolhParticulars.Frozen = True
        Me.dgcolhParticulars.HeaderText = "Particulars"
        Me.dgcolhParticulars.Name = "dgcolhParticulars"
        Me.dgcolhParticulars.ReadOnly = True
        Me.dgcolhParticulars.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.dgcolhParticulars.Width = 250
        '
        'dgcolhExpense
        '
        Me.dgcolhExpense.HeaderText = "Expense"
        Me.dgcolhExpense.Name = "dgcolhExpense"
        Me.dgcolhExpense.ReadOnly = True
        Me.dgcolhExpense.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'dgcolhSector
        '
        Me.dgcolhSector.HeaderText = "Sector"
        Me.dgcolhSector.Name = "dgcolhSector"
        Me.dgcolhSector.ReadOnly = True
        Me.dgcolhSector.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'dgcolhUnitprice
        '
        Me.dgcolhUnitprice.AllowNegative = False
        Me.dgcolhUnitprice.DecimalLength = 2
        DataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle3.Format = "F2"
        Me.dgcolhUnitprice.DefaultCellStyle = DataGridViewCellStyle3
        Me.dgcolhUnitprice.HeaderText = "Unit Price"
        Me.dgcolhUnitprice.Name = "dgcolhUnitprice"
        Me.dgcolhUnitprice.ReadOnly = True
        Me.dgcolhUnitprice.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'dgcolhQuantity
        '
        Me.dgcolhQuantity.AllowNegative = False
        Me.dgcolhQuantity.DecimalLength = 2
        DataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle4.Format = "F2"
        Me.dgcolhQuantity.DefaultCellStyle = DataGridViewCellStyle4
        Me.dgcolhQuantity.HeaderText = "Quantity"
        Me.dgcolhQuantity.Name = "dgcolhQuantity"
        Me.dgcolhQuantity.ReadOnly = True
        Me.dgcolhQuantity.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'dgcolhAmount
        '
        Me.dgcolhAmount.AllowNegative = False
        Me.dgcolhAmount.DecimalLength = 2
        DataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle5.Format = "F2"
        Me.dgcolhAmount.DefaultCellStyle = DataGridViewCellStyle5
        Me.dgcolhAmount.HeaderText = "Amount"
        Me.dgcolhAmount.Name = "dgcolhAmount"
        Me.dgcolhAmount.ReadOnly = True
        Me.dgcolhAmount.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'dgcolhCurrency
        '
        Me.dgcolhCurrency.HeaderText = "Currency"
        Me.dgcolhCurrency.Name = "dgcolhCurrency"
        Me.dgcolhCurrency.ReadOnly = True
        Me.dgcolhCurrency.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.dgcolhCurrency.Width = 90
        '
        'dgcolhQtytranhead
        '
        Me.dgcolhQtytranhead.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.[Nothing]
        Me.dgcolhQtytranhead.HeaderText = "Qty Transaction Head"
        Me.dgcolhQtytranhead.Name = "dgcolhQtytranhead"
        Me.dgcolhQtytranhead.ReadOnly = True
        Me.dgcolhQtytranhead.Width = 160
        '
        'dgcolhAmttranhead
        '
        Me.dgcolhAmttranhead.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.[Nothing]
        Me.dgcolhAmttranhead.HeaderText = "Amt Transaction Head"
        Me.dgcolhAmttranhead.Name = "dgcolhAmttranhead"
        Me.dgcolhAmttranhead.ReadOnly = True
        Me.dgcolhAmttranhead.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgcolhAmttranhead.Width = 160
        '
        'objdgcolhIsGroup
        '
        Me.objdgcolhIsGroup.HeaderText = "IsGroup"
        Me.objdgcolhIsGroup.Name = "objdgcolhIsGroup"
        Me.objdgcolhIsGroup.ReadOnly = True
        Me.objdgcolhIsGroup.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.objdgcolhIsGroup.Visible = False
        '
        'objdgcolhClaimFomMstID
        '
        Me.objdgcolhClaimFomMstID.HeaderText = "objdgcolhClaimFomMstID"
        Me.objdgcolhClaimFomMstID.Name = "objdgcolhClaimFomMstID"
        Me.objdgcolhClaimFomMstID.ReadOnly = True
        Me.objdgcolhClaimFomMstID.Visible = False
        '
        'frmClaimPosting
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(851, 483)
        Me.Controls.Add(Me.chkSelectAll)
        Me.Controls.Add(Me.gbFilterCriteria)
        Me.Controls.Add(Me.objFooter)
        Me.Controls.Add(Me.dgvClaimPosting)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmClaimPosting"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Post To Payroll"
        Me.objFooter.ResumeLayout(False)
        Me.gbFilterCriteria.ResumeLayout(False)
        Me.gbFilterCriteria.PerformLayout()
        CType(Me.objbtnReset, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.objbtnSearch, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dgvClaimPosting, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents objFooter As eZee.Common.eZeeFooter
    Friend WithEvents btnSave As eZee.Common.eZeeLightButton
    Friend WithEvents btnClose As eZee.Common.eZeeLightButton
    Friend WithEvents gbFilterCriteria As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents objbtnReset As eZee.Common.eZeeSearchResetButton
    Friend WithEvents objbtnSearch As eZee.Common.eZeeSearchResetButton
    Friend WithEvents objbtnSearchEmployee As eZee.Common.eZeeGradientButton
    Friend WithEvents cboEmployee As System.Windows.Forms.ComboBox
    Friend WithEvents lblEmployee As System.Windows.Forms.Label
    Friend WithEvents lblClaimNo As System.Windows.Forms.Label
    Friend WithEvents txtClaimNo As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents cboExpCategory As System.Windows.Forms.ComboBox
    Friend WithEvents lblExpCategory As System.Windows.Forms.Label
    Friend WithEvents cboPeriod As System.Windows.Forms.ComboBox
    Friend WithEvents lblPeriod As System.Windows.Forms.Label
    Friend WithEvents cboExpense As System.Windows.Forms.ComboBox
    Friend WithEvents LblExpense As System.Windows.Forms.Label
    Friend WithEvents LblTranHead As System.Windows.Forms.Label
    Friend WithEvents cboTranhead As System.Windows.Forms.ComboBox
    Friend WithEvents objbtnSearchTranHead As eZee.Common.eZeeGradientButton
    Friend WithEvents objbtnSearchPeriod As eZee.Common.eZeeGradientButton
    Friend WithEvents objbtnSearchExpense As eZee.Common.eZeeGradientButton
    Friend WithEvents objbtnSearchExpCategory As eZee.Common.eZeeGradientButton
    Friend WithEvents btnPosting As eZee.Common.eZeeLightButton
    Friend WithEvents dgvClaimPosting As System.Windows.Forms.DataGridView
    Friend WithEvents cboViewBy As System.Windows.Forms.ComboBox
    Friend WithEvents lblViewBy As System.Windows.Forms.Label
    Friend WithEvents cboUOM As System.Windows.Forms.ComboBox
    Friend WithEvents LblUOM As System.Windows.Forms.Label
    Friend WithEvents chkSelectAll As System.Windows.Forms.CheckBox
    Friend WithEvents DataGridViewTextBoxColumn1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn2 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn3 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn4 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn5 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents btnUnposting As eZee.Common.eZeeLightButton
    Friend WithEvents DataGridViewTextBoxColumn6 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents rdAmtTranHeadMapping As System.Windows.Forms.RadioButton
    Friend WithEvents rdQtyTrnheadMapping As System.Windows.Forms.RadioButton
    Friend WithEvents objdgcolhCollaps As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objcolhIschecked As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents dgcolhParticulars As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhExpense As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhSector As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhUnitprice As eZee.Common.DataGridViewNumericTextBoxColumn
    Friend WithEvents dgcolhQuantity As eZee.Common.DataGridViewNumericTextBoxColumn
    Friend WithEvents dgcolhAmount As eZee.Common.DataGridViewNumericTextBoxColumn
    Friend WithEvents dgcolhCurrency As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhQtytranhead As System.Windows.Forms.DataGridViewComboBoxColumn
    Friend WithEvents dgcolhAmttranhead As System.Windows.Forms.DataGridViewComboBoxColumn
    Friend WithEvents objdgcolhIsGroup As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhClaimFomMstID As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn7 As System.Windows.Forms.DataGridViewTextBoxColumn
End Class
