﻿Option Strict On

#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data

#End Region

Public Class frmEmployeeSectorList

#Region " Private Variables "

    'Pinkal (07-Jan-2019) -- Start
    'AT Testing- Working on AT Testing for All Modules.
    'Private ReadOnly mstrModuleName As String = "frmEmployeeExpensesList"
    Private ReadOnly mstrModuleName As String = "frmEmployeeSectorList"
    'Pinkal (07-Jan-2019) -- End

    Private objEmpSector As clsassignemp_sector
    Private mblnCancel As Boolean = True
    Private dtTab As DataTable
    Private imgPlusIcon As Image = My.Resources.plus_blue
    Private imgMinusIcon As Image = My.Resources.minus_blue
    Private imgBlankIcon As Image = My.Resources.blankImage

#End Region

#Region " Private Methods "

    Private Sub FillCombo()
        Dim dsCombo As New DataSet

        Try
            Dim objEmp As New clsEmployee_Master
            dsCombo = objEmp.GetEmployeeList(FinancialYear._Object._DatabaseName, _
                                             User._Object._Userunkid, _
                                             FinancialYear._Object._YearUnkid, _
                                             Company._Object._Companyunkid, _
                                             eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                             eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                             ConfigParameter._Object._UserAccessModeSetting, _
                                             True, False, "List", True)
            With cboEmployee
                .ValueMember = "employeeunkid"
                .DisplayMember = "employeename"
                .DataSource = dsCombo.Tables(0)
                .SelectedValue = 0
            End With
            objEmp = Nothing

            Dim objCMaster As New clsCommon_Master
            dsCombo = objCMaster.getComboList(clsCommon_Master.enCommonMaster.SECTOR_ROUTE, True, "List")
            With cboSector
                .ValueMember = "masterunkid"
                .DisplayMember = "name"
                .DataSource = dsCombo.Tables(0)
                .SelectedValue = 0
            End With
            objCMaster = Nothing


        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub FillGrid()
        Dim dsList As New DataSet
        Try
            Me.Cursor = Cursors.WaitCursor
            dsList = objEmpSector.GetList("List", CInt(cboEmployee.SelectedValue), CInt(cboSector.SelectedValue))
            'Pinkal (26-Dec-2018) -- Start
            'Enhancement - Working on Leave/Claim Enhancement Phase 2 for NMB.
            'dtTab = New DataView(dsList.Tables(0), "", "", DataViewRowState.CurrentRows).ToTable
            dtTab = dsList.Tables(0)
            'Pinkal (26-Dec-2018) -- End

            Call SetDataSource()
            If dgEmployee.RowCount <= 0 Then
                chkSelectAll.Checked = False
                chkSelectAll.Enabled = False
            Else
                chkSelectAll.Enabled = True
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillGrid", mstrModuleName)
        Finally
            Me.Cursor = Cursors.Default
        End Try
    End Sub

    Private Sub SetGridColor()
        Try

            'Pinkal (26-Dec-2018) -- Start
            'Enhancement - Working on Leave/Claim Enhancement Phase 2 for NMB.
            'Dim dgvcsHeader As New DataGridViewCellStyle
            'dgvcsHeader.ForeColor = Color.White
            'dgvcsHeader.SelectionBackColor = Color.Gray
            'dgvcsHeader.SelectionForeColor = Color.White
            'dgvcsHeader.BackColor = Color.Gray

            'For i As Integer = 0 To dgEmployee.RowCount - 1
            '    If CBool(dgEmployee.Rows(i).Cells(objdgcolhIsGrp.Index).Value) = True Then
            '        dgEmployee.Rows(i).DefaultCellStyle = dgvcsHeader
            '    End If
            'Next

            Dim dr = From dgdr As DataGridViewRow In dgEmployee.Rows.Cast(Of DataGridViewRow)() Where CBool(dgdr.Cells(objdgcolhIsGrp.Index).Value) = True Select dgdr
            dr.ToList.ForEach(Function(x) SetRowStyle(x, True))

            'dr = Nothing
            'dr = From dgdr As DataGridViewRow In dgEmployee.Rows.Cast(Of DataGridViewRow)() Where CBool(dgdr.Cells(objdgcolhIsGrp.Index).Value) = False Select dgdr
            'dr.ToList.ForEach(Function(x) SetRowStyle(x, False))
            'Pinkal (26-Dec-2018) -- End

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetGridColor", mstrModuleName)
        Finally
        End Try
    End Sub

    'Pinkal (26-Dec-2018) -- Start
    'Enhancement - Working on Leave/Claim Enhancement Phase 2 for NMB.

    'Private Sub SetCollapseValue()
    '    Try
    '        Me.dgEmployee.SuspendLayout()
    '        Dim objdgvsCollapseHeader As New DataGridViewCellStyle
    '        Dim objCMManager As CurrencyManager = CType(BindingContext(dgEmployee.DataSource), CurrencyManager)
    '        objdgvsCollapseHeader.Font = New Font(dgEmployee.Font.FontFamily, 13, FontStyle.Bold)
    '        objdgvsCollapseHeader.ForeColor = Color.White
    '        objdgvsCollapseHeader.BackColor = Color.Gray
    '        objdgvsCollapseHeader.SelectionBackColor = Color.Gray
    '        objdgvsCollapseHeader.Alignment = DataGridViewContentAlignment.MiddleCenter

    '        For i As Integer = 0 To dgEmployee.RowCount - 1
    '            If CBool(dgEmployee.Rows(i).Cells(objdgcolhIsGrp.Index).Value) = True Then
    '                If dgEmployee.Rows(i).Cells(objdgcolhCollapse.Index).Value Is Nothing Then
    '                    dgEmployee.Rows(i).Cells(objdgcolhCollapse.Index).Value = imgPlusIcon
    '                End If
    '                dgEmployee.Rows(i).Cells(objdgcolhCollapse.Index).Style = objdgvsCollapseHeader
    '            Else
    '                dgEmployee.Rows(i).Cells(objdgcolhCollapse.Index).Value = imgBlankIcon
    '                objCMManager.SuspendBinding()
    '                dgEmployee.Rows(i).Visible = False
    '                objCMManager.ResumeBinding()
    '            End If
    '        Next
    '        Me.dgEmployee.ResumeLayout()
    '    Catch ex As Exception
    '        DisplayError.Show("-1", ex.Message, "SetCollapseValue", mstrModuleName)
    '    Finally
    '    End Try
    'End Sub

    'Pinkal (26-Dec-2018) -- End

    Private Sub SetDataSource()
        Try
            dgEmployee.AutoGenerateColumns = False
            objdgcolhEmpSectorunkid.DataPropertyName = "crempsecrouteunkid"
            objdgcolhGrpId.DataPropertyName = "grpid"
            objdgcolhIsGrp.DataPropertyName = "isgrp"
            objdgSelect.DataPropertyName = "ischeck"
            dgcolhSectorRoute.DataPropertyName = "Sector"
            objdgcolhEmployeeId.DataPropertyName = "employeeunkid"
            objdgcolhSectorID.DataPropertyName = "sectorId"
            dgEmployee.DataSource = dtTab


            'Pinkal (26-Dec-2018) -- Start
            'Enhancement - Working on Leave/Claim Enhancement Phase 2 for NMB.
            Call SetGridColor()
            'Call SetCollapseValue()
            'Pinkal (26-Dec-2018) -- End

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetDataSource", mstrModuleName)
        Finally
        End Try
    End Sub



    'Pinkal (26-Dec-2018) -- Start
    'Enhancement - Working on Leave/Claim Enhancement Phase 2 for NMB.

    Private Function SetRowStyle(ByVal xRow As DataGridViewRow, ByVal mblnIsGroup As Boolean) As Boolean
        Try
            If mblnIsGroup Then
                Dim dgvcsHeader As New DataGridViewCellStyle
                dgvcsHeader.ForeColor = Color.White
                dgvcsHeader.SelectionBackColor = Color.Gray
                dgvcsHeader.SelectionForeColor = Color.White
                dgvcsHeader.BackColor = Color.Gray
                dgvcsHeader.Font = New Font(dgEmployee.Font.FontFamily, dgEmployee.Font.Size, FontStyle.Bold)
                xRow.DefaultCellStyle = dgvcsHeader

                If xRow.Cells(objdgcolhCollapse.Index).Value Is Nothing Then
                    xRow.Cells(objdgcolhCollapse.Index).Value = imgPlusIcon
                End If
                'Else
                'xRow.Cells(objdgcolhCollapse.Index).Value = imgBlankIcon
                'xRow.Visible = False
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetRowStyle", mstrModuleName)
        End Try
    End Function

    Private Function UpdateAll(ByVal dr As DataRow, ByVal mblnisCheck As Boolean) As Boolean
        Try
            If dr Is Nothing Then Return False
            dr("ischeck") = mblnisCheck
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "UpdateAll", mstrModuleName)
            Return False
        End Try
        Return True
    End Function

    'Pinkal (26-Dec-2018) -- End

#End Region

#Region " Form's Events "

    Private Sub frmEmployeeSectorList_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        objEmpSector = New clsassignemp_sector
        Try
            Call Set_Logo(Me, gApplicationType)
            Language.setLanguage(Me.Name)
            Call OtherSettings()
            Call FillCombo()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmEmployeeSectorList_Load", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()
            clsEmployeeExpenseBalance.SetMessages()
            objfrm._Other_ModuleNames = "clsEmployeeExpenseBalance"
            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub

#End Region

#Region " Button's Event's "

    Private Sub btnNew_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNew.Click
        Dim frm As New frmEmployeeSectors
        Try
            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If

            If frm.displayDialog(enAction.ADD_CONTINUE) Then
                Call FillGrid()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnNew_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub btnDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDelete.Click
        Try
            If dtTab IsNot Nothing Then
                Dim dtmp() As DataRow = dtTab.Select("ischeck=true and isgrp=false")
                If dtmp.Length <= 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Sorry, Please tick atleast one sector/route assigned to employee."), enMsgBoxStyle.Information)
                    Exit Sub
                End If
                Dim blnUsed As Boolean = False
                Dim blnShown As Boolean = False
                Dim mstrVoidReason As String = String.Empty
                For i As Integer = 0 To dtmp.Length - 1
                    If blnShown = False Then
                        If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Are you sure you want to delete checked transaction." & vbCrLf & _
                                                               "Do you wish to continue?"), CType(enMsgBoxStyle.Information + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.No Then
                            Exit Sub
                        End If
                        Dim frm As New frmReasonSelection
                        If User._Object._Isrighttoleft = True Then
                            frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                            frm.RightToLeftLayout = True
                            Call Language.ctlRightToLeftlayOut(frm)
                        End If
                        frm.displayDialog(enVoidCategoryType.OTHERS, mstrVoidReason)
                        If mstrVoidReason.Length <= 0 Then
                            Exit Sub
                        End If
                        blnShown = True
                    End If

                    'S.SANDEEP [28-May-2018] -- START
                    'ISSUE/ENHANCEMENT : {Audit Trails} 
                    objEmpSector._FormName = mstrModuleName
                    objEmpSector._LoginEmployeeunkid = 0
                    objEmpSector._ClientIP = getIP()
                    objEmpSector._HostName = getHostName()
                    objEmpSector._FromWeb = False
                    objEmpSector._AuditUserId = User._Object._Userunkid
objEmpSector._CompanyUnkid = Company._Object._Companyunkid
                    objEmpSector._AuditDate = ConfigParameter._Object._CurrentDateAndTime
                    'S.SANDEEP [28-May-2018] -- END

                    If objEmpSector.isUsed(CInt(dtmp(i).Item("employeeunkid")), CInt(dtmp(i).Item("sectorId"))) = True Then
                        dgEmployee.Rows(dtTab.Rows.IndexOf(dtmp(i))).DefaultCellStyle.ForeColor = Color.Red
                        dgEmployee.Rows(dtTab.Rows.IndexOf(dtmp(i))).DefaultCellStyle.SelectionForeColor = Color.Red
                        blnUsed = True
                    Else
                        Me.Cursor = Cursors.WaitCursor
                        objEmpSector._Crempsecrouteunkid = CInt(dtmp(i).Item("crempsecrouteunkid"))

                        objEmpSector._Isvoid = True
                        objEmpSector._Voiddatetime = ConfigParameter._Object._CurrentDateAndTime
                        objEmpSector._Voidreason = mstrVoidReason
                        objEmpSector._Voiduserunkid = User._Object._Userunkid


                        'S.SANDEEP [28-May-2018] -- START
                        'ISSUE/ENHANCEMENT : {Audit Trails} 
                        objEmpSector._FormName = mstrModuleName
                        objEmpSector._LoginEmployeeunkid = 0
                        objEmpSector._ClientIP = getIP()
                        objEmpSector._HostName = getHostName()
                        objEmpSector._FromWeb = False
                        objEmpSector._AuditUserId = User._Object._Userunkid
objEmpSector._CompanyUnkid = Company._Object._Companyunkid
                        objEmpSector._AuditDate = ConfigParameter._Object._CurrentDateAndTime
                        'S.SANDEEP [28-May-2018] -- END

                        If objEmpSector.Delete(CInt(dtmp(i).Item("crempsecrouteunkid"))) = False Then
                            eZeeMsgBox.Show(objEmpSector._Message, enMsgBoxStyle.Information)
                            Exit Sub
                        End If
                    End If
                Next
                If blnUsed = True Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Sorry, some of the ticked sector/route are already in used,therefor cannot be deleted and highlighted in red."), enMsgBoxStyle.Information)
                    Exit Sub
                End If
            End If
            Call FillGrid()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnDelete_Click", mstrModuleName)
        Finally
            Me.Cursor = Cursors.Default
        End Try
    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnClose_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub objbtnSearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearch.Click
        Try
            Call FillGrid()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearch_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub objbtnReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnReset.Click
        Try
            cboEmployee.SelectedValue = 0
            cboSector.SelectedValue = 0
            If dtTab IsNot Nothing Then dtTab.Rows.Clear()
            dgEmployee.DataSource = Nothing
            If dgEmployee.RowCount <= 0 Then chkSelectAll.Enabled = False
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnReset_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub objbtnSearchEmployee_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchEmployee.Click, objbtnSearchSector.Click
        Dim frm As New frmCommonSearch
        Try
            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If
            Dim cbo As ComboBox = Nothing
            Select Case CType(sender, eZee.Common.eZeeGradientButton).Name.ToUpper
                Case objbtnSearchEmployee.Name.ToUpper
                    cbo = cboEmployee
                Case objbtnSearchSector.Name.ToUpper
                    cbo = cboSector
            End Select
            If cbo.DataSource Is Nothing Then Exit Sub
            With frm
                .ValueMember = cbo.ValueMember
                .DisplayMember = cbo.DisplayMember
                .DataSource = CType(cbo.DataSource, DataTable)
                Select Case CType(sender, eZee.Common.eZeeGradientButton).Name.ToUpper
                    Case objbtnSearchEmployee.Name.ToUpper
                        .CodeMember = "employeecode"
                End Select
                If .DisplayDialog = True Then
                    cbo.SelectedValue = .SelectedValue
                End If
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchEmployee_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

#End Region

#Region " DataGrid Event's "

    Private Sub dgEmployee_CellContentClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgEmployee.CellContentClick, dgEmployee.CellContentDoubleClick
        Try
            RemoveHandler chkSelectAll.CheckedChanged, AddressOf chkSelectAll_CheckedChanged
            Dim i As Integer
            If e.RowIndex = -1 Then Exit Sub

            If Me.dgEmployee.IsCurrentCellDirty Then
                Me.dgEmployee.CommitEdit(DataGridViewDataErrorContexts.Commit)
            End If

            If CBool(dgEmployee.Rows(e.RowIndex).Cells(objdgcolhIsGrp.Index).Value) = True Then
                Select Case CInt(e.ColumnIndex)
                    Case objdgSelect.Index
                        For i = e.RowIndex + 1 To dgEmployee.RowCount - 1
                            Dim blnFlg As Boolean = False
                            blnFlg = CBool(dgEmployee.Rows(e.RowIndex).Cells(objdgSelect.Index).Value)
                            If CInt(dgEmployee.Rows(e.RowIndex).Cells(objdgcolhGrpId.Index).Value) = CInt(dgEmployee.Rows(i).Cells(objdgcolhGrpId.Index).Value) Then
                                dgEmployee.Rows(i).Cells(objdgSelect.Index).Value = blnFlg
                            End If
                        Next
                    Case objdgcolhCollapse.Index
                        If dgEmployee.Rows(e.RowIndex).Cells(objdgcolhCollapse.Index).Value Is imgMinusIcon Then
                            dgEmployee.Rows(e.RowIndex).Cells(objdgcolhCollapse.Index).Value = imgPlusIcon
                        Else
                            dgEmployee.Rows(e.RowIndex).Cells(objdgcolhCollapse.Index).Value = imgMinusIcon
                        End If

                        For i = e.RowIndex + 1 To dgEmployee.RowCount - 1
                            If CInt(dgEmployee.Rows(e.RowIndex).Cells(objdgcolhGrpId.Index).Value) = CInt(dgEmployee.Rows(i).Cells(objdgcolhGrpId.Index).Value) Then
                                If dgEmployee.Rows(i).Visible = False Then
                                    dgEmployee.Rows(i).Visible = True
                                Else
                                    dgEmployee.Rows(i).Visible = False
                                End If
                            Else
                                Exit For
                            End If
                        Next
                End Select
            End If
            AddHandler chkSelectAll.CheckedChanged, AddressOf chkSelectAll_CheckedChanged
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgEmployee_CellContentClick", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

#Region " Checkbox Event's "

    Private Sub chkSelectAll_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkSelectAll.CheckedChanged
        Try
            RemoveHandler dgEmployee.CellContentClick, AddressOf dgEmployee_CellContentClick
            If dtTab.Rows.Count > 0 Then

                'Pinkal (26-Dec-2018) -- Start
                'Enhancement - Working on Leave/Claim Enhancement Phase 2 for NMB.
                'For Each dr As DataRow In dtTab.Rows
                '    dr("ischeck") = chkSelectAll.Checked
                '    dr.EndEdit()
                'Next
                'dtTab.AcceptChanges()
                Me.Cursor = Cursors.WaitCursor
                Dim dr = dtTab.AsEnumerable()
                dr.ToList().ForEach(Function(x) UpdateAll(x, chkSelectAll.Checked))
            End If
            'Call SetDataSource()
            'Pinkal (26-Dec-2018) -- End
            AddHandler dgEmployee.CellContentClick, AddressOf dgEmployee_CellContentClick
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "chkSelectAll_CheckedChanged", mstrModuleName)
        Finally
            Me.Cursor = Cursors.Default
        End Try
    End Sub

#End Region

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Private Sub OtherSettings()
        Try
            Me.SuspendLayout()
            Call SetLanguage()

            Me.gbFilterCriteria.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbFilterCriteria.ForeColor = GUI._eZeeContainerHeaderForeColor

            Me.btnDelete.GradientBackColor = GUI._ButttonBackColor
            Me.btnDelete.GradientForeColor = GUI._ButttonFontColor

            Me.btnNew.GradientBackColor = GUI._ButttonBackColor
            Me.btnNew.GradientForeColor = GUI._ButttonFontColor

            Me.btnClose.GradientBackColor = GUI._ButttonBackColor
            Me.btnClose.GradientForeColor = GUI._ButttonFontColor


            Me.ResumeLayout()
        Catch Ex As Exception
            DisplayError.Show("-1", ex.Message, "OtherSettings", mstrModuleName)
        End Try
    End Sub


    Private Sub SetLanguage()
        Try
            Me.Text = Language._Object.getCaption(Me.Name, Me.Text)

            Me.gbFilterCriteria.Text = Language._Object.getCaption(Me.gbFilterCriteria.Name, Me.gbFilterCriteria.Text)
            Me.lblSectorRoute.Text = Language._Object.getCaption(Me.lblSectorRoute.Name, Me.lblSectorRoute.Text)
            Me.lblEmployee.Text = Language._Object.getCaption(Me.lblEmployee.Name, Me.lblEmployee.Text)
            Me.btnDelete.Text = Language._Object.getCaption(Me.btnDelete.Name, Me.btnDelete.Text)
            Me.btnNew.Text = Language._Object.getCaption(Me.btnNew.Name, Me.btnNew.Text)
            Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
            Me.chkSelectAll.Text = Language._Object.getCaption(Me.chkSelectAll.Name, Me.chkSelectAll.Text)
           

        Catch Ex As Exception
            DisplayError.Show("-1", ex.Message, "SetLanguage", mstrModuleName)
        End Try
    End Sub


    Private Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Sorry, Please tick atleast one expense assigned to employee.")
            Language.setMessage(mstrModuleName, 2, "Are you sure you want to delete checked transaction." & vbCrLf & _
                                                                        "Do you wish to continue?")
            Language.setMessage(mstrModuleName, 3, "Sorry, some of the ticked expenses are already issued,therefor cannot be deleted and highlighted in red.")

        Catch Ex As Exception
            DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class