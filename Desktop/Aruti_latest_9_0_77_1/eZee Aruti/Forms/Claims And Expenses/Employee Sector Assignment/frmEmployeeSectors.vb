﻿Option Strict On

#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data

#End Region

Public Class frmEmployeeSectors

#Region " Private Variable "

    Private ReadOnly mstrModuleName As String = "frmEmployeeSectors"
    Private mblnCancel As Boolean = True
    Private menAction As enAction = enAction.ADD_ONE
    Private mintYearunkid As Integer = -1
    Private mdtEmployee As DataTable = Nothing
    Private dvEmployee As DataView
    Private mstrAdvanceFilter As String = String.Empty
    Private mdtDate As Date = ConfigParameter._Object._CurrentDateAndTime.Date
    Private mdtToDate As Date = ConfigParameter._Object._CurrentDateAndTime.Date
    Private dsEmployee As DataSet
    Private objAssignEmpSector As New clsassignemp_sector
    Private mblnIsLeaveEncashment As Boolean = False

    'Pinkal (20-Nov-2018) -- Start
    'Enhancement - Working on P2P Integration for NMB.
    Private mdtSectorRoute As DataTable = Nothing
    Private dvSectorRoute As DataView = Nothing
    Private mdtExportExistData As DataTable = Nothing
    'Pinkal (20-Nov-2018) -- End


#End Region

#Region " Display Dialog "

    Public Function displayDialog(ByVal eAction As enAction) As Boolean
        Try
            menAction = eAction
            Me.ShowDialog()
            Return Not mblnCancel
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "displayDialog", mstrModuleName)
        Finally
        End Try
    End Function

#End Region

#Region " Form's Event "

    Private Sub frmEmployeeSectors_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Call Set_Logo(Me, gApplicationType)
            Language.setLanguage(Me.Name)
            Call OtherSettings()
            Call FillCombo()
            'Pinkal (26-Dec-2018) -- Start
            'Enhancement - Working on Leave/Claim Enhancement Phase 2 for NMB.
            'radAppointedDate.Checked = True
            'Pinkal (26-Dec-2018) -- End
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmEmployeeSectors_Load", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub frmEmployeeSectors_KeyPress(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles MyBase.KeyPress
        Try
            If Asc(e.KeyChar) = 13 Then
                SendKeys.Send("{TAB}")
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmEmployeeSectors_KeyPress", mstrModuleName)
        End Try
    End Sub

    Private Sub frmEmployeeSectors_KeyDown(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles MyBase.KeyDown
        Try
            If e.Control = True And e.KeyCode = Keys.S Then
                btnSave_Click(sender, e)
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmEmployeeSectors_KeyDown", mstrModuleName)
        End Try
    End Sub

    Private Sub frmEmployeeSectors_FormClosed(ByVal sender As System.Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles MyBase.FormClosed
        objAssignEmpSector = Nothing
    End Sub

    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()
            clsassignemp_sector.SetMessages()
            objfrm._Other_ModuleNames = "clsassignemp_sector"
            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub


#End Region

#Region " Private Methods "

    Private Sub FillCombo()
        Dim dsCombo As New DataSet
        Dim objMaster As New clsMasterData
        Dim objCMaster As New clsCommon_Master
        Try
           
            dsCombo = objMaster.GetCondition(True, True, False, False, False)
            With cboFromcondition
                .DisplayMember = "Name"
                .ValueMember = "id"
                .DataSource = dsCombo.Tables(0).Copy
                .SelectedValue = 0
            End With

            With cboTocondition
                .DisplayMember = "Name"
                .ValueMember = "id"
                .DataSource = dsCombo.Tables(0).Copy
                .SelectedValue = 0
            End With


            'Pinkal (26-Dec-2018) -- Start
            'Enhancement - Working on Leave/Claim Enhancement Phase 2 for NMB.
            'dsCombo = objCMaster.getComboList(clsCommon_Master.enCommonMaster.SECTOR_ROUTE, True, "List")
            'With cboSector
            '    .ValueMember = "masterunkid"
            '    .DisplayMember = "name"
            '    .DataSource = dsCombo.Tables("List")
            '    .SelectedValue = 0
            'End With
            'Pinkal (26-Dec-2018) -- End

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        Finally
            dsCombo.Dispose() : objMaster = Nothing
        End Try
    End Sub

    Private Sub SetDataSource()
        Try
            dvEmployee = mdtEmployee.DefaultView
            dgEmployee.AutoGenerateColumns = False
            dgEmployee.DataSource = dvEmployee
            objEmpSelect.DataPropertyName = "ischecked"

            'Pinkal (26-Dec-2018) -- Start
            'Enhancement - Working on Leave/Claim Enhancement Phase 2 for NMB.
            'dgColhEmpCode.DataPropertyName = "employeecode"
            'dgColhEmployee.DataPropertyName = "name"
            'dgColhAppointdate.DataPropertyName = "appointeddate"
            'dgcolhJobTitle.DataPropertyName = "job_name"
            dgColhEmpCode.DataPropertyName = Language.getMessage("clsEmployee_Master", 42, "Code")
            dgColhEmployee.DataPropertyName = Language.getMessage("clsEmployee_Master", 46, "Employee Name")
            dgColhAppointdate.DataPropertyName = Language.getMessage("clsEmployee_Master", 48, "Appointed Date")
            dgcolhJobTitle.DataPropertyName = Language.getMessage("clsEmployee_Master", 118, "Job")
            'Pinkal (26-Dec-2018) -- End

            If dgEmployee.Rows.Count > 0 Then
                chkEmpSelectAll.Enabled = True
            Else
                chkEmpSelectAll.Enabled = False
                chkEmpSelectAll.Checked = False
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetDataSource", mstrModuleName)
        Finally
            objbtnSearch.ShowResult(dgEmployee.RowCount.ToString())
        End Try
    End Sub

    Private Sub Get_Employee()
        Try
            Dim strSearching As String = ""
            Dim objEmployee As New clsEmployee_Master

            Dim blnIncludeReinstatemetDate As Boolean = False
            Dim dtStartDate, dtEndDate As Date

            dtStartDate = eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate)
            dtEndDate = eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate)

            If radAppointedDate.Checked = True Then
                dtStartDate = dtpDate1.Value.Date
                dtEndDate = dtpDate2.Value.Date
            Else
                blnIncludeReinstatemetDate = True
            End If


            'Pinkal (26-Dec-2018) -- Start
            'Enhancement - Working on Leave/Claim Enhancement Phase 2 for NMB.
            'Dim mstrEmployeId As String = objAssignEmpSector.GetEmployeeForSector(FinancialYear._Object._DatabaseName, ConfigParameter._Object._EmployeeAsOnDate, CInt(cboSector.SelectedValue))

            'If mstrEmployeId.Trim.Length > 0 Then
            '    strSearching = "AND hremployee_master.employeeunkid in (" & mstrEmployeId & ") "
            'Else
            '    strSearching = "AND hremployee_master.employeeunkid = 0"
            'End If
            'Pinkal (26-Dec-2018) -- End


            If radExpYear.Checked = True Then
                If nudFromYear.Value > 0 Or nudFromMonth.Value > 0 Then
                    strSearching &= "AND hremployee_master.appointeddate " & cboFromcondition.Text & " '" & eZeeDate.convertDate(mdtToDate) & "' "
                End If

                If nudToYear.Value > 0 Or nudToMonth.Value > 0 Then
                    strSearching &= "AND hremployee_master.appointeddate " & cboTocondition.Text & " '" & eZeeDate.convertDate(mdtDate) & "' "
                End If
            End If



            'Pinkal (22-Oct-2018) -- Start
            'Bug - appointment date filter not working on leave Global Assign accrue and Caim & Request expense assignment screen[Support Issue Id # 0002823]
            If radAppointedDate.Checked = True Then
                strSearching &= "AND Convert(CHAR(8),hremployee_master.appointeddate,112) BETWEEN '" & eZeeDate.convertDate(dtStartDate) & "' AND '" & eZeeDate.convertDate(dtEndDate) & "'"
            End If
            'Pinkal (22-Oct-2018) -- End

            If mstrAdvanceFilter.Trim.Length > 0 Then
                strSearching &= "AND " & mstrAdvanceFilter
            End If

            If strSearching.Trim.Length > 0 Then
                strSearching = strSearching.Trim.Substring(3)
            End If


            'Pinkal (26-Dec-2018) -- Start
            'Enhancement - Working on Leave/Claim Enhancement Phase 2 for NMB.

            'dsEmployee = objEmployee.GetList(FinancialYear._Object._DatabaseName, _
            '                                  User._Object._Userunkid, _
            '                                  FinancialYear._Object._YearUnkid, _
            '                                  Company._Object._Companyunkid, _
            '                                  dtStartDate, _
            '                                  dtEndDate, _
            '                                  ConfigParameter._Object._UserAccessModeSetting, True, False, _
            '                                  "Employee", ConfigParameter._Object._ShowFirstAppointmentDate, , , strSearching, blnIncludeReinstatemetDate)
            Dim StrCheck_Fields As String = String.Empty
            StrCheck_Fields = clsEmployee_Master.EmpColEnum.Col_Code & "," & clsEmployee_Master.EmpColEnum.Col_Employee_Name & "," & clsEmployee_Master.EmpColEnum.Col_Department & "," & clsEmployee_Master.EmpColEnum.Col_Job & "," & clsEmployee_Master.EmpColEnum.Col_Appointed_Date & "," & clsEmployee_Master.EmpColEnum.Col_Email

            dsEmployee = objEmployee.GetListForDynamicField(StrCheck_Fields, FinancialYear._Object._DatabaseName _
                                                                                                                        , User._Object._Userunkid, FinancialYear._Object._YearUnkid _
                                                                                                                        , Company._Object._Companyunkid, dtStartDate, dtEndDate _
                                                                                                                        , ConfigParameter._Object._UserAccessModeSetting, True, False _
                                                                                                                        , "Employee", -1, False, strSearching, ConfigParameter._Object._ShowFirstAppointmentDate _
                                                                                                                        , False, False, True, Nothing)

            'Pinkal (26-Dec-2018) -- End

         
            mdtEmployee = dsEmployee.Tables(0)


            'Pinkal (20-Nov-2018) -- Start
            'Enhancement - Working on P2P Integration for NMB.

            'For Each dr As DataRow In mdtEmployee.Rows
            '    dr("appointeddate") = eZeeDate.convertDate(dr("appointeddate").ToString()).ToShortDateString
            'Next


            'If mdtEmployee.Columns.Contains("ischecked") = False Then
            '    mdtEmployee.Columns.Add("ischecked", Type.GetType("System.Boolean"))
            '    mdtEmployee.Columns("ischecked").DefaultValue = False
            'End If

            If mdtEmployee.Columns.Contains("ischecked") = False Then
                Dim dcColumn As New DataColumn("ischecked", Type.GetType("System.Boolean"))
                dcColumn.DefaultValue = False
                mdtEmployee.Columns.Add(dcColumn)
            End If

            If mdtEmployee IsNot Nothing Then
                mdtEmployee.AsEnumerable().ToList().ForEach(Function(x) UpdateRow(x))
            End If

            'Pinkal (20-Nov-2018) -- End


            If radAppointedDate.Checked = True Then
                LblValue.Text = dtpDate1.Value.Date & " - " & dtpDate2.Value.Date
            Else
                LblValue.Text = mdtToDate.Date & " - " & mdtDate.Date
            End If

            Call SetDataSource()

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Get_Employee", mstrModuleName)
        Finally
        End Try
    End Sub

    'Pinkal (26-Dec-2018) -- Start
    'Enhancement - Working on Leave/Claim Enhancement Phase 2 for NMB.

    Private Sub GetSectorRouteList()
        Try
            Dim objCMaster As New clsCommon_Master
            Dim dsSecRoute As DataSet = objCMaster.getComboList(clsCommon_Master.enCommonMaster.SECTOR_ROUTE, False, "List")
            objCMaster = Nothing

            If dsSecRoute IsNot Nothing AndAlso dsSecRoute.Tables(0).Columns.Contains("isSecRouteCheckAll") = False Then
                Dim dcColumn As New DataColumn("isSecRouteCheckAll", Type.GetType("System.Boolean"))
                dcColumn.DefaultValue = False
                dsSecRoute.Tables(0).Columns.Add(dcColumn)
            End If

            mdtSectorRoute = dsSecRoute.Tables(0).Copy()

            dsSecRoute.Clear()
            dsSecRoute = Nothing

            dgSecRoute.AutoGenerateColumns = False
            objSecRouteSelect.DataPropertyName = "isSecRouteCheckAll"
            dgcolhSecRoute.DataPropertyName = "name"
            objdgcolhSecRouteId.DataPropertyName = "masterunkid"
            dvSectorRoute = mdtSectorRoute.DefaultView
            dgSecRoute.DataSource = dvSectorRoute
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetSectorRouteList", mstrModuleName)
        End Try
    End Sub

    Private Function UpdateRow(ByVal dr As DataRow) As Boolean
        Try
            dr(Language.getMessage("clsEmployee_Master", 48, "Appointed Date")) = eZeeDate.convertDate(dr(Language.getMessage("clsEmployee_Master", 48, "Appointed Date")).ToString()).ToShortDateString()
            dr.AcceptChanges()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "UpdateRow", mstrModuleName)
            Return False
        End Try
        Return True
    End Function

    Private Sub SetValue(ByVal iEmployeeId As Integer, ByVal iSectorRouteId As Integer)
        Try
            objAssignEmpSector._Employeeunkid = iEmployeeId
            'Pinkal (26-Dec-2018) -- Start
            'Enhancement - Working on Leave/Claim Enhancement Phase 2 for NMB.
            'objAssignEmpSector._Secrouteunkid = CInt(cboSector.SelectedValue)
            objAssignEmpSector._Secrouteunkid = iSectorRouteId
            'Pinkal (26-Dec-2018) -- End
            objAssignEmpSector._Userunkid = User._Object._Userunkid
            objAssignEmpSector._Isvoid = False
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetValue", mstrModuleName)
        Finally
        End Try
    End Sub

    'Pinkal (26-Dec-2018) -- End

#End Region

#Region " Button's Event "

    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Dim blnFlag As Boolean = False
        Try
            Dim dr() As DataRow = Nothing
            'Pinkal (26-Dec-2018) -- Start
            'Enhancement - Working on Leave/Claim Enhancement Phase 2 for NMB.
            Dim drRow() As DataRow = Nothing

            If dgEmployee.Rows.Count <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "There is no employee to Assign Sector/Route."), enMsgBoxStyle.Information)
                dgEmployee.Select()
                Exit Sub

            ElseIf dgSecRoute.Rows.Count <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 11, "There is no Sector/Route Assign to employee."), enMsgBoxStyle.Information)
                dgEmployee.Select()
                Exit Sub
            End If

            If mdtEmployee IsNot Nothing Then
                dr = mdtEmployee.Select("ischecked=True")
                If dr.Length <= 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 6, "Employee is compulsory information.Please check atleast one employee."), enMsgBoxStyle.Information)
                    dgEmployee.Select()
                    Exit Sub
                End If

            End If

            If mdtSectorRoute IsNot Nothing Then
                drRow = mdtSectorRoute.Select("isSecRouteCheckAll=True")
                If drRow.Length <= 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 10, "Sector/Route is compulsory information.Please check atleast one Sector/Route."), enMsgBoxStyle.Information)
                    dgSecRoute.Select()
                    Exit Sub
                End If
            End If

            Dim blnExists As Boolean = False
            Dim blnShown As Boolean = False

            mdtExportExistData = New DataTable()
            mdtExportExistData.Columns.Add(Language.getMessage("clsEmployee_Master", 42, "Code"), Type.GetType("System.String"))
            mdtExportExistData.Columns.Add(Language.getMessage("clsEmployee_Master", 46, "Employee Name"), Type.GetType("System.String"))
            mdtExportExistData.Columns.Add(Me.dgcolhSecRoute.HeaderText, Type.GetType("System.String"))
           
            Me.ControlBox = False
            Me.Enabled = False

            GC.Collect()
            Dim dtrdSectorRoute As DataTableReader = drRow.CopyToDataTable().CreateDataReader()

            Dim xCountSecRoute As Integer = 0
            Dim xCountEmp As Integer = 0

            While dtrdSectorRoute.Read

                If xCountSecRoute <= 0 Then Application.DoEvents() : lnkSecRouteProcess.Text = Language.getMessage(mstrModuleName, 14, "Sector/Route Processed : ") & xCountSecRoute & "/" & drRow.Length

                Dim dtrdEmployee As DataTableReader = dr.CopyToDataTable().CreateDataReader()
                While dtrdEmployee.Read
                    Application.DoEvents() : lnkEmpProcess.Text = Language.getMessage(mstrModuleName, 15, "Employee Processed : ") & xCountEmp + 1 & "/" & dr.Length

                    blnExists = objAssignEmpSector.isExist(CInt(dtrdEmployee("employeeunkid")), CInt(dtrdSectorRoute("masterunkid")))
                    If blnExists = True Then
                        If mdtExportExistData IsNot Nothing Then
                            Dim drExistRow As DataRow = mdtExportExistData.NewRow()
                            drExistRow(Language.getMessage("clsEmployee_Master", 42, "Code")) = dtrdEmployee(Language.getMessage("clsEmployee_Master", 42, "Code"))
                            drExistRow(Language.getMessage("clsEmployee_Master", 46, "Employee Name")) = dtrdEmployee(Language.getMessage("clsEmployee_Master", 46, "Employee Name"))
                            drExistRow(Me.dgcolhSecRoute.HeaderText) = dtrdSectorRoute("name").ToString()
                            mdtExportExistData.Rows.Add(drExistRow)
                        End If
                        xCountEmp += 1
                        Continue While
                    End If

                    Call SetValue(CInt(dtrdEmployee("employeeunkid")), CInt(dtrdSectorRoute("masterunkid")))

                    With objAssignEmpSector
                        ._FormName = mstrModuleName
                        ._LoginEmployeeunkid = 0
                        ._ClientIP = getIP()
                        ._HostName = getHostName()
                        ._FromWeb = False
                        ._AuditUserId = User._Object._Userunkid
._CompanyUnkid = Company._Object._Companyunkid
                        ._AuditDate = ConfigParameter._Object._CurrentDateAndTime
                    End With

                    blnFlag = objAssignEmpSector.Insert()
                    If blnFlag = False AndAlso objAssignEmpSector._Message <> "" Then
                        eZeeMsgBox.Show(objAssignEmpSector._Message & " - [ " & dtrdEmployee("name").ToString & " ] ", enMsgBoxStyle.Information)
                        Exit While
                    End If
                    xCountEmp += 1
                End While
                xCountSecRoute += 1
                Application.DoEvents() : lnkSecRouteProcess.Text = Language.getMessage(mstrModuleName, 14, "Sector/Route Processed : ") & xCountSecRoute & "/" & drRow.Length
                xCountEmp = 0
            End While

            If blnExists Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 12, "Sorry, Some of the checked employee(s) are already assigned the checked Sector/Route(s).") & vbCrLf & _
                                                    Language.getMessage(mstrModuleName, 13, "Due to this some employee(s) will be discarded from the Sector/Route(s) assignment process."), enMsgBoxStyle.Information)
                blnExists = False
                btnExportExistAssignment.Enabled = True
                Call objbtnReset_Click(sender, e)
                Exit Sub
                    End If

            'Pinkal (26-Dec-2018) -- End

            If blnFlag = True Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 9, "Employee Sector/Route successfully assigned."), enMsgBoxStyle.Information)
                Call objbtnReset_Click(sender, e)
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnSave_Click", mstrModuleName)
        Finally
            Me.ControlBox = True
            Me.Enabled = True
        End Try
    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Me.Close()
    End Sub

    Private Sub objbtnReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnReset.Click
        Try
            nudFromYear.Value = 0
            nudFromMonth.Value = 0
            nudToYear.Value = 0
            nudToMonth.Value = 0
            cboFromcondition.SelectedValue = 0
            cboTocondition.SelectedValue = 0
            mstrAdvanceFilter = ""
            chkEmpSelectAll.Checked = False
            If mdtEmployee IsNot Nothing Then mdtEmployee.Rows.Clear()
            dgEmployee.DataSource = Nothing
            cboSector.SelectedValue = 0
            txtEmpSearch.Text = ""
            LblFrom.Text = ""
            LblValue.Text = ""
            lnkSecRouteProcess.Text = ""

            'Pinkal (26-Dec-2018) -- Start
            'Enhancement - Working on Leave/Claim Enhancement Phase 2 for NMB.
            dvEmployee = Nothing
            chkSecRouteSelectAll.Checked = False
            If mdtSectorRoute IsNot Nothing Then mdtSectorRoute.Rows.Clear()
            dgSecRoute.DataSource = Nothing
            lnkEmpProcess.Text = ""
            'Pinkal (26-Dec-2018) -- End

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnReset_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnSearchExp_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Dim frm As New frmCommonSearch
        Try
            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If
            With frm
                .ValueMember = cboSector.ValueMember
                .DisplayMember = cboSector.DisplayMember
                .DataSource = CType(cboSector.DataSource, DataTable)
                If .DisplayDialog = True Then
                    cboSector.SelectedValue = .SelectedValue
                End If
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchExp_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub objbtnSearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearch.Click
        Try

            'Pinkal (26-Dec-2018) -- Start
            'Enhancement - Working on Leave/Claim Enhancement Phase 2 for NMB.
            'If CInt(cboSector.SelectedValue) <= 0 Then
            '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Sorry, Sector/Route is mandatory information. Please select Sector/Route to continue."), enMsgBoxStyle.Information)
            '    cboSector.Focus()
            '    Exit Sub
            'End If
            'Pinkal (26-Dec-2018) -- End

            If radAppointedDate.Checked = True Then
                If dtpDate2.Value.Date < dtpDate1.Value.Date Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Sorry, To date cannot ne less than From date."), enMsgBoxStyle.Information)
                    Exit Sub
                End If
            ElseIf radExpYear.Checked = True Then
                If (nudFromYear.Value > 0 Or nudFromMonth.Value > 0) And CInt(cboFromcondition.SelectedValue) <= 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "From Condition is compulsory information.Please Select From Condition."), enMsgBoxStyle.Information)
                    cboFromcondition.Select()
                    Exit Sub
                ElseIf (nudToYear.Value > 0 Or nudToMonth.Value > 0) And CInt(cboTocondition.SelectedValue) <= 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "To Condition is compulsory information.Please Select To Condition."), enMsgBoxStyle.Information)
                    cboTocondition.Select()
                    Exit Sub
                End If
            End If
            'Pinkal (26-Dec-2018) -- Start
            'Enhancement - Working on Leave/Claim Enhancement Phase 2 for NMB.
            Me.Cursor = Cursors.WaitCursor
            GetSectorRouteList()
            'Pinkal (26-Dec-2018) -- End
            Call Get_Employee()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearch_Click", mstrModuleName)
        Finally
            Me.Cursor = Cursors.Default
        End Try
    End Sub

#End Region

#Region " Combox Event's "

#End Region

#Region " CheckBox's Event "

    'Pinkal (26-Dec-2018) -- Start
    'Enhancement - Working on Leave/Claim Enhancement Phase 2 for NMB.

    Private Sub chkEmpSelectAll_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkEmpSelectAll.CheckedChanged
        Try

            If mdtEmployee.Rows.Count > 0 Then
                For Each dr As DataRowView In dvEmployee
                    RemoveHandler dgEmployee.CellContentClick, AddressOf dgEmployee_CellContentClick
                    dr("ischecked") = chkEmpSelectAll.Checked
                    AddHandler dgEmployee.CellContentClick, AddressOf dgEmployee_CellContentClick
                    dr.EndEdit()
                Next
                dvEmployee.ToTable.AcceptChanges()
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "chkEmpSelectAll_CheckedChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub chkSecRouteSelectAll_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkSecRouteSelectAll.CheckedChanged
        Try
            If dvSectorRoute IsNot Nothing Then
                For Each dr As DataRowView In dvSectorRoute
                    RemoveHandler dgEmployee.CellContentClick, AddressOf dgEmployee_CellContentClick
                    dr("isSecRouteCheckAll") = chkSecRouteSelectAll.Checked
                    AddHandler dgEmployee.CellContentClick, AddressOf dgEmployee_CellContentClick
                    dr.EndEdit()
                Next
                dvSectorRoute.ToTable.AcceptChanges()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "chkSecRouteSelectAll_CheckedChanged", mstrModuleName)
        End Try
    End Sub

    'Pinkal (26-Dec-2018) -- End

#End Region

#Region " LinkButton Events "

    Private Sub lnkAllocation_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lnkAllocation.LinkClicked
        Try
            mstrAdvanceFilter = ""
            txtEmpSearch.Text = ""
            Dim frm As New frmAdvanceSearch
            If frm.ShowDialog() = Windows.Forms.DialogResult.OK Then
                mstrAdvanceFilter = frm._GetFilterString
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lnkAllocation_LinkClicked", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " NumericUpdown Event "

    Private Sub nudFromYear_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles nudFromYear.ValueChanged, nudFromMonth.Validated, nudFromMonth.ValueChanged, nudFromMonth.Validated
        Try
            mdtDate = ConfigParameter._Object._CurrentDateAndTime.Date.AddYears(CInt(nudFromYear.Value) * -1).AddMonths(CInt(nudFromMonth.Value) * -1)
            nudToYear.Minimum = nudFromYear.Value
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "nudFromYear_ValueChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub nudToYear_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles nudToYear.ValueChanged, nudToYear.Validated, nudToMonth.ValueChanged, nudToMonth.Validated
        Try
            mdtToDate = ConfigParameter._Object._CurrentDateAndTime.Date.AddYears(CInt(nudToYear.Value) * -1).AddMonths(CInt(nudToMonth.Value) * -1)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "nudToYear_ValueChanged", mstrModuleName)
        End Try
    End Sub


#End Region

#Region " RadioButton Event "

    Private Sub radAppointedDate_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles radAppointedDate.CheckedChanged, radExpYear.CheckedChanged
        Try
            Select Case CType(sender, RadioButton).Name.ToUpper
                Case radAppointedDate.Name.ToUpper
                    If radAppointedDate.Checked = True Then
                        pnlAppDate.Enabled = True : pnlYear.Enabled = False
                    End If
                Case radExpYear.Name.ToUpper
                    If radExpYear.Checked = True Then
                        pnlAppDate.Enabled = False : pnlYear.Enabled = True
                    End If
            End Select
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "radAppointedDate_CheckedChanged", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub chkNoAction_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Try
           
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "chkNoAction_CheckedChanged", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Textbox Event "

    'Pinkal (26-Dec-2018) -- Start
    'Enhancement - Working on Leave/Claim Enhancement Phase 2 for NMB.

    Private Sub txtEmpSearch_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtEmpSearch.TextChanged
        Try
            If dvEmployee.Table.Rows.Count > 0 Then
                dvEmployee.RowFilter = "[" & dgColhEmpCode.DataPropertyName & "] like '%" & txtEmpSearch.Text.Trim & "%'  OR [" & dgColhEmployee.DataPropertyName & "] like '%" & txtEmpSearch.Text.Trim & "%'"
                dgEmployee.Refresh()
            End If

            If dgEmployee.Rows.Count > 0 Then
                chkEmpSelectAll.Enabled = True
            Else
                chkEmpSelectAll.Enabled = False
                chkEmpSelectAll.Checked = False
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "txtEmpSearch_TextChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub txtSecRouteSearch_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtSecRouteSearch.TextChanged
        Try
            If dvSectorRoute.Table.Rows.Count > 0 Then
                dvSectorRoute.RowFilter = "[" & dgcolhSecRoute.DataPropertyName & "] like '%" & txtSecRouteSearch.Text.Trim & "%'"
                dgSecRoute.Refresh()
            End If

            If dgSecRoute.Rows.Count > 0 Then
                chkSecRouteSelectAll.Enabled = True
            Else
                chkSecRouteSelectAll.Enabled = False
                chkSecRouteSelectAll.Checked = False
            End If
            dgSecRoute_CellContentClick(dgSecRoute, New DataGridViewCellEventArgs(objSecRouteSelect.Index, 0))
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "txtSecRouteSearch_TextChanged", mstrModuleName)
        End Try
    End Sub

    'Pinkal (26-Dec-2018) -- End


#End Region

#Region " DataGrid Event "

    Private Sub dgEmployee_CellContentClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgEmployee.CellContentClick, dgEmployee.CellContentDoubleClick
        Try
            If e.RowIndex < 0 Then Exit Sub

            'Pinkal (26-Dec-2018) -- Start
            'Enhancement - Working on Leave/Claim Enhancement Phase 2 for NMB.
            'RemoveHandler chkEmpSelectAll.CheckedChanged, AddressOf chkSelectAll_CheckedChanged
            RemoveHandler chkEmpSelectAll.CheckedChanged, AddressOf chkEmpSelectAll_CheckedChanged
            'Pinkal (26-Dec-2018) -- End

            If e.ColumnIndex = objEmpSelect.Index Then
                If dgEmployee.IsCurrentCellDirty Then
                    dgEmployee.CommitEdit(DataGridViewDataErrorContexts.Commit)
                    mdtEmployee.AcceptChanges()
                End If
                Dim drRow As DataRow() = dvEmployee.ToTable.Select("ischecked = true")
                If drRow.Length <= 0 Then
                    chkEmpSelectAll.CheckState = CheckState.Unchecked
                ElseIf drRow.Length < dgEmployee.Rows.Count Then
                    chkEmpSelectAll.CheckState = CheckState.Indeterminate
                ElseIf drRow.Length = dgEmployee.Rows.Count Then
                    chkEmpSelectAll.CheckState = CheckState.Checked
                End If
            End If

            'Pinkal (26-Dec-2018) -- Start
            'Enhancement - Working on Leave/Claim Enhancement Phase 2 for NMB.
            'AddHandler chkEmpSelectAll.CheckedChanged, AddressOf chkSelectAll_CheckedChanged
            AddHandler chkEmpSelectAll.CheckedChanged, AddressOf chkEmpSelectAll_CheckedChanged
            'Pinkal (26-Dec-2018) -- End

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgEmployee_CellContentClick", mstrModuleName)
        End Try
    End Sub

    'Pinkal (26-Dec-2018) -- Start
    'Enhancement - Working on Leave/Claim Enhancement Phase 2 for NMB.

    Private Sub dgSecRoute_CellContentClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgSecRoute.CellContentClick, dgSecRoute.CellContentDoubleClick
        Try
            If e.RowIndex < 0 Then Exit Sub

            RemoveHandler chkSecRouteSelectAll.CheckedChanged, AddressOf chkSecRouteSelectAll_CheckedChanged

            If e.ColumnIndex = objSecRouteSelect.Index Then
                If dgSecRoute.IsCurrentCellDirty Then
                    dgSecRoute.CommitEdit(DataGridViewDataErrorContexts.Commit)
                    mdtSectorRoute.AcceptChanges()
                End If
                Dim drRow As DataRow() = dvSectorRoute.ToTable.Select("isSecRouteCheckAll = true")
                If drRow.Length <= 0 Then
                    chkSecRouteSelectAll.CheckState = CheckState.Unchecked
                ElseIf drRow.Length < dgEmployee.Rows.Count Then
                    chkSecRouteSelectAll.CheckState = CheckState.Indeterminate
                ElseIf drRow.Length = dgEmployee.Rows.Count Then
                    chkSecRouteSelectAll.CheckState = CheckState.Checked
                End If
            End If

            AddHandler chkSecRouteSelectAll.CheckedChanged, AddressOf chkSecRouteSelectAll_CheckedChanged
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgSecRoute_CellContentClick", mstrModuleName)
        End Try
    End Sub

    'Pinkal (26-Dec-2018) -- End


#End Region

    'Pinkal (26-Dec-2018) -- Start
    'Enhancement - Working on Leave/Claim Enhancement Phase 2 for NMB.
#Region "Context Menu Events"

    Private Sub btnExportExistAssignment_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnExportExistAssignment.Click
        Try
            If mdtExportExistData Is Nothing OrElse mdtExportExistData.Rows.Count <= 0 Then

            End If

            mdtExportExistData = New DataView(mdtExportExistData, "", "[" & Language.getMessage("clsEmployee_Master", 46, "Employee Name") & "],[" & Me.dgcolhSecRoute.HeaderText & "]", DataViewRowState.CurrentRows).ToTable()
            Dim savDialog As New SaveFileDialog
            savDialog.Filter = "Execl files(*.xlsx)|*.xlsx"
            If savDialog.ShowDialog = Windows.Forms.DialogResult.OK Then
                If Export_ErrorList(savDialog.FileName, mdtExportExistData, "Already assigned Sector/Route to Employee") = True Then
                    Process.Start(savDialog.FileName)
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnExportExistAssignment_Click", mstrModuleName)
        End Try
    End Sub

#End Region

    'Pinkal (26-Dec-2018) -- End




	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Private Sub OtherSettings()
		Try
			Me.SuspendLayout()
            Call SetLanguage()
			
			Me.gbFilterCriteria.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbFilterCriteria.ForeColor = GUI._eZeeContainerHeaderForeColor 

			Me.gbLeaveInfo.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbLeaveInfo.ForeColor = GUI._eZeeContainerHeaderForeColor 


			Me.btnSave.GradientBackColor = GUI._ButttonBackColor 
			Me.btnSave.GradientForeColor = GUI._ButttonFontColor

			Me.btnClose.GradientBackColor = GUI._ButttonBackColor 
			Me.btnClose.GradientForeColor = GUI._ButttonFontColor


			Me.ResumeLayout()
		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "OtherSettings", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetLanguage()
		Try
			Me.Text = Language._Object.getCaption(Me.Name, Me.Text)
			
			Me.btnSave.Text = Language._Object.getCaption(Me.btnSave.Name, Me.btnSave.Text)
			Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
			Me.gbFilterCriteria.Text = Language._Object.getCaption(Me.gbFilterCriteria.Name, Me.gbFilterCriteria.Text)
			Me.lblToDate.Text = Language._Object.getCaption(Me.lblToDate.Name, Me.lblToDate.Text)
			Me.lblFromDate.Text = Language._Object.getCaption(Me.lblFromDate.Name, Me.lblFromDate.Text)
			Me.lblFromYear.Text = Language._Object.getCaption(Me.lblFromYear.Name, Me.lblFromYear.Text)
			Me.LblToYear.Text = Language._Object.getCaption(Me.LblToYear.Name, Me.LblToYear.Text)
			Me.LblToMonth.Text = Language._Object.getCaption(Me.LblToMonth.Name, Me.LblToMonth.Text)
			Me.LblMonth.Text = Language._Object.getCaption(Me.LblMonth.Name, Me.LblMonth.Text)
			Me.radExpYear.Text = Language._Object.getCaption(Me.radExpYear.Name, Me.radExpYear.Text)
			Me.radAppointedDate.Text = Language._Object.getCaption(Me.radAppointedDate.Name, Me.radAppointedDate.Text)
			Me.lnkAllocation.Text = Language._Object.getCaption(Me.lnkAllocation.Name, Me.lnkAllocation.Text)
			Me.LblValue.Text = Language._Object.getCaption(Me.LblValue.Name, Me.LblValue.Text)
			Me.LblFrom.Text = Language._Object.getCaption(Me.LblFrom.Name, Me.LblFrom.Text)
            Me.chkEmpSelectAll.Text = Language._Object.getCaption(Me.chkEmpSelectAll.Name, Me.chkEmpSelectAll.Text)
            Me.lnkSecRouteProcess.Text = Language._Object.getCaption(Me.lnkSecRouteProcess.Name, Me.lnkSecRouteProcess.Text)
			Me.DataGridViewTextBoxColumn1.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn1.Name, Me.DataGridViewTextBoxColumn1.HeaderText)
			Me.DataGridViewTextBoxColumn2.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn2.Name, Me.DataGridViewTextBoxColumn2.HeaderText)
			Me.DataGridViewTextBoxColumn3.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn3.Name, Me.DataGridViewTextBoxColumn3.HeaderText)
			Me.DataGridViewTextBoxColumn4.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn4.Name, Me.DataGridViewTextBoxColumn4.HeaderText)
			Me.gbLeaveInfo.Text = Language._Object.getCaption(Me.gbLeaveInfo.Name, Me.gbLeaveInfo.Text)
			Me.lblSector.Text = Language._Object.getCaption(Me.lblSector.Name, Me.lblSector.Text)
            Me.chkSecRouteSelectAll.Text = Language._Object.getCaption(Me.chkSecRouteSelectAll.Name, Me.chkSecRouteSelectAll.Text)
            Me.dgcolhSecRoute.HeaderText = Language._Object.getCaption(Me.dgcolhSecRoute.Name, Me.dgcolhSecRoute.HeaderText)
            Me.DataGridViewTextBoxColumn5.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn5.Name, Me.DataGridViewTextBoxColumn5.HeaderText)
            Me.DataGridViewTextBoxColumn6.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn6.Name, Me.DataGridViewTextBoxColumn6.HeaderText)
            Me.dgColhEmpCode.HeaderText = Language._Object.getCaption(Me.dgColhEmpCode.Name, Me.dgColhEmpCode.HeaderText)
            Me.dgColhEmployee.HeaderText = Language._Object.getCaption(Me.dgColhEmployee.Name, Me.dgColhEmployee.HeaderText)
            Me.dgColhAppointdate.HeaderText = Language._Object.getCaption(Me.dgColhAppointdate.Name, Me.dgColhAppointdate.HeaderText)
            Me.dgcolhJobTitle.HeaderText = Language._Object.getCaption(Me.dgcolhJobTitle.Name, Me.dgcolhJobTitle.HeaderText)

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetLanguage", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 2, "Sorry, To date cannot ne less than From date.")
			Language.setMessage(mstrModuleName, 3, "From Condition is compulsory information.Please Select From Condition.")
			Language.setMessage(mstrModuleName, 4, "To Condition is compulsory information.Please Select To Condition.")
			Language.setMessage(mstrModuleName, 5, "There is no employee to Assign Sector/Route.")
			Language.setMessage(mstrModuleName, 6, "Employee is compulsory information.Please check atleast one employee.")
			Language.setMessage(mstrModuleName, 7, "Processed :")
			Language.setMessage(mstrModuleName, 9, "Employee Sector/Route successfully assigned.")
            Language.setMessage(mstrModuleName, 10, "Sector/Route is compulsory information.Please check atleast one Sector/Route.")
            Language.setMessage(mstrModuleName, 11, "There is no Sector/Route Assign to employee.")
            Language.setMessage(mstrModuleName, 12, "Sorry, Some of the checked employee(s) are already assigned the checked Sector/Route(s).")
            Language.setMessage(mstrModuleName, 13, "Due to this some employee(s) will be discarded from the Sector/Route(s) assignment process.")
            Language.setMessage("clsEmployee_Master", 42, "Code")
            Language.setMessage("clsEmployee_Master", 46, "Employee Name")
            Language.setMessage("clsEmployee_Master", 48, "Appointed Date")
            Language.setMessage("clsEmployee_Master", 118, "Job")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class