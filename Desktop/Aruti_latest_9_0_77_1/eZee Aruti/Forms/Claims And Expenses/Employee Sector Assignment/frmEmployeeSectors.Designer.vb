﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmEmployeeSectors
    Inherits eZee.Common.eZeeForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmEmployeeSectors))
        Me.objFooter = New eZee.Common.eZeeFooter
        Me.objbtnImport = New eZee.Common.eZeeSplitButton
        Me.lnkEmpProcess = New System.Windows.Forms.LinkLabel
        Me.lnkSecRouteProcess = New System.Windows.Forms.LinkLabel
        Me.btnSave = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnClose = New eZee.Common.eZeeLightButton(Me.components)
        Me.gbFilterCriteria = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.lnkAllocation = New System.Windows.Forms.LinkLabel
        Me.objbtnReset = New eZee.Common.eZeeSearchResetButton(Me.components)
        Me.objbtnSearch = New eZee.Common.eZeeSearchResetButton(Me.components)
        Me.pnlAppDate = New System.Windows.Forms.Panel
        Me.lblToDate = New System.Windows.Forms.Label
        Me.dtpDate1 = New System.Windows.Forms.DateTimePicker
        Me.lblFromDate = New System.Windows.Forms.Label
        Me.dtpDate2 = New System.Windows.Forms.DateTimePicker
        Me.pnlYear = New System.Windows.Forms.Panel
        Me.lblFromYear = New System.Windows.Forms.Label
        Me.nudToYear = New System.Windows.Forms.NumericUpDown
        Me.LblToYear = New System.Windows.Forms.Label
        Me.nudFromMonth = New System.Windows.Forms.NumericUpDown
        Me.LblToMonth = New System.Windows.Forms.Label
        Me.LblMonth = New System.Windows.Forms.Label
        Me.nudToMonth = New System.Windows.Forms.NumericUpDown
        Me.cboTocondition = New System.Windows.Forms.ComboBox
        Me.nudFromYear = New System.Windows.Forms.NumericUpDown
        Me.cboFromcondition = New System.Windows.Forms.ComboBox
        Me.pnlFliter = New System.Windows.Forms.Panel
        Me.radExpYear = New System.Windows.Forms.RadioButton
        Me.radAppointedDate = New System.Windows.Forms.RadioButton
        Me.LblValue = New System.Windows.Forms.Label
        Me.LblFrom = New System.Windows.Forms.Label
        Me.txtEmpSearch = New eZee.TextBox.AlphanumericTextBox
        Me.chkEmpSelectAll = New System.Windows.Forms.CheckBox
        Me.dgEmployee = New System.Windows.Forms.DataGridView
        Me.objEmpSelect = New System.Windows.Forms.DataGridViewCheckBoxColumn
        Me.dgColhEmpCode = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgColhEmployee = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgColhAppointdate = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhJobTitle = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.gbLeaveInfo = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.SplitContainer1 = New System.Windows.Forms.SplitContainer
        Me.chkSecRouteSelectAll = New System.Windows.Forms.CheckBox
        Me.dgSecRoute = New System.Windows.Forms.DataGridView
        Me.objSecRouteSelect = New System.Windows.Forms.DataGridViewCheckBoxColumn
        Me.dgcolhSecRoute = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhSecRouteId = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.txtSecRouteSearch = New eZee.TextBox.AlphanumericTextBox
        Me.cboSector = New System.Windows.Forms.ComboBox
        Me.lblSector = New System.Windows.Forms.Label
        Me.objbtnSearchSector = New eZee.Common.eZeeGradientButton
        Me.DataGridViewTextBoxColumn1 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn2 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn3 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn4 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn5 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn6 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.cmnuSecRouteEmpData = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.btnExportExistAssignment = New System.Windows.Forms.ToolStripMenuItem
        Me.objFooter.SuspendLayout()
        Me.gbFilterCriteria.SuspendLayout()
        CType(Me.objbtnReset, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.objbtnSearch, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.pnlAppDate.SuspendLayout()
        Me.pnlYear.SuspendLayout()
        CType(Me.nudToYear, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.nudFromMonth, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.nudToMonth, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.nudFromYear, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.pnlFliter.SuspendLayout()
        CType(Me.dgEmployee, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.gbLeaveInfo.SuspendLayout()
        Me.SplitContainer1.Panel1.SuspendLayout()
        Me.SplitContainer1.Panel2.SuspendLayout()
        Me.SplitContainer1.SuspendLayout()
        CType(Me.dgSecRoute, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.cmnuSecRouteEmpData.SuspendLayout()
        Me.SuspendLayout()
        '
        'objFooter
        '
        Me.objFooter.BorderColor = System.Drawing.Color.Silver
        Me.objFooter.Controls.Add(Me.objbtnImport)
        Me.objFooter.Controls.Add(Me.lnkEmpProcess)
        Me.objFooter.Controls.Add(Me.lnkSecRouteProcess)
        Me.objFooter.Controls.Add(Me.btnSave)
        Me.objFooter.Controls.Add(Me.btnClose)
        Me.objFooter.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.objFooter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objFooter.GradiantStyle = eZee.Common.eZeeFooter.GradientStyle.Up
        Me.objFooter.GradientColor1 = System.Drawing.SystemColors.Control
        Me.objFooter.GradientColor2 = System.Drawing.SystemColors.Control
        Me.objFooter.Location = New System.Drawing.Point(0, 441)
        Me.objFooter.Name = "objFooter"
        Me.objFooter.Size = New System.Drawing.Size(725, 55)
        Me.objFooter.TabIndex = 4
        '
        'objbtnImport
        '
        Me.objbtnImport.BorderColor = System.Drawing.Color.Black
        Me.objbtnImport.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objbtnImport.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.objbtnImport.GradientForeColor = System.Drawing.SystemColors.WindowText
        Me.objbtnImport.Location = New System.Drawing.Point(9, 13)
        Me.objbtnImport.Name = "objbtnImport"
        Me.objbtnImport.ShowDefaultBorderColor = True
        Me.objbtnImport.Size = New System.Drawing.Size(110, 30)
        Me.objbtnImport.SplitButtonMenu = Me.cmnuSecRouteEmpData
        Me.objbtnImport.TabIndex = 296
        Me.objbtnImport.Text = "Opearation"
        '
        'lnkEmpProcess
        '
        Me.lnkEmpProcess.AutoSize = True
        Me.lnkEmpProcess.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lnkEmpProcess.LinkBehavior = System.Windows.Forms.LinkBehavior.NeverUnderline
        Me.lnkEmpProcess.Location = New System.Drawing.Point(238, 33)
        Me.lnkEmpProcess.Name = "lnkEmpProcess"
        Me.lnkEmpProcess.Size = New System.Drawing.Size(0, 13)
        Me.lnkEmpProcess.TabIndex = 3
        '
        'lnkSecRouteProcess
        '
        Me.lnkSecRouteProcess.AutoSize = True
        Me.lnkSecRouteProcess.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lnkSecRouteProcess.LinkBehavior = System.Windows.Forms.LinkBehavior.NeverUnderline
        Me.lnkSecRouteProcess.Location = New System.Drawing.Point(238, 10)
        Me.lnkSecRouteProcess.Name = "lnkSecRouteProcess"
        Me.lnkSecRouteProcess.Size = New System.Drawing.Size(0, 13)
        Me.lnkSecRouteProcess.TabIndex = 2
        Me.lnkSecRouteProcess.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'btnSave
        '
        Me.btnSave.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnSave.BackColor = System.Drawing.Color.White
        Me.btnSave.BackgroundImage = CType(resources.GetObject("btnSave.BackgroundImage"), System.Drawing.Image)
        Me.btnSave.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnSave.BorderColor = System.Drawing.Color.Empty
        Me.btnSave.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnSave.FlatAppearance.BorderSize = 0
        Me.btnSave.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnSave.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSave.ForeColor = System.Drawing.Color.Black
        Me.btnSave.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnSave.GradientForeColor = System.Drawing.Color.Black
        Me.btnSave.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSave.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnSave.Location = New System.Drawing.Point(513, 13)
        Me.btnSave.Name = "btnSave"
        Me.btnSave.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSave.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnSave.Size = New System.Drawing.Size(97, 30)
        Me.btnSave.TabIndex = 0
        Me.btnSave.Text = "&Save"
        Me.btnSave.UseVisualStyleBackColor = True
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnClose.BackColor = System.Drawing.Color.White
        Me.btnClose.BackgroundImage = CType(resources.GetObject("btnClose.BackgroundImage"), System.Drawing.Image)
        Me.btnClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnClose.BorderColor = System.Drawing.Color.Empty
        Me.btnClose.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnClose.FlatAppearance.BorderSize = 0
        Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClose.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.ForeColor = System.Drawing.Color.Black
        Me.btnClose.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnClose.GradientForeColor = System.Drawing.Color.Black
        Me.btnClose.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Location = New System.Drawing.Point(616, 13)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Size = New System.Drawing.Size(97, 30)
        Me.btnClose.TabIndex = 1
        Me.btnClose.Text = "&Close"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'gbFilterCriteria
        '
        Me.gbFilterCriteria.BorderColor = System.Drawing.Color.Black
        Me.gbFilterCriteria.Checked = False
        Me.gbFilterCriteria.CollapseAllExceptThis = False
        Me.gbFilterCriteria.CollapsedHoverImage = Nothing
        Me.gbFilterCriteria.CollapsedNormalImage = Nothing
        Me.gbFilterCriteria.CollapsedPressedImage = Nothing
        Me.gbFilterCriteria.CollapseOnLoad = False
        Me.gbFilterCriteria.Controls.Add(Me.lnkAllocation)
        Me.gbFilterCriteria.Controls.Add(Me.objbtnReset)
        Me.gbFilterCriteria.Controls.Add(Me.objbtnSearch)
        Me.gbFilterCriteria.Controls.Add(Me.pnlAppDate)
        Me.gbFilterCriteria.Controls.Add(Me.pnlYear)
        Me.gbFilterCriteria.Controls.Add(Me.pnlFliter)
        Me.gbFilterCriteria.Dock = System.Windows.Forms.DockStyle.Top
        Me.gbFilterCriteria.ExpandedHoverImage = Nothing
        Me.gbFilterCriteria.ExpandedNormalImage = Nothing
        Me.gbFilterCriteria.ExpandedPressedImage = Nothing
        Me.gbFilterCriteria.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbFilterCriteria.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbFilterCriteria.HeaderHeight = 25
        Me.gbFilterCriteria.HeaderMessage = ""
        Me.gbFilterCriteria.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbFilterCriteria.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbFilterCriteria.HeightOnCollapse = 0
        Me.gbFilterCriteria.LeftTextSpace = 0
        Me.gbFilterCriteria.Location = New System.Drawing.Point(0, 0)
        Me.gbFilterCriteria.Name = "gbFilterCriteria"
        Me.gbFilterCriteria.OpenHeight = 91
        Me.gbFilterCriteria.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbFilterCriteria.ShowBorder = True
        Me.gbFilterCriteria.ShowCheckBox = False
        Me.gbFilterCriteria.ShowCollapseButton = False
        Me.gbFilterCriteria.ShowDefaultBorderColor = True
        Me.gbFilterCriteria.ShowDownButton = False
        Me.gbFilterCriteria.ShowHeader = True
        Me.gbFilterCriteria.Size = New System.Drawing.Size(725, 84)
        Me.gbFilterCriteria.TabIndex = 15
        Me.gbFilterCriteria.TabStop = True
        Me.gbFilterCriteria.Temp = 0
        Me.gbFilterCriteria.Text = "Filter Criteria"
        Me.gbFilterCriteria.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lnkAllocation
        '
        Me.lnkAllocation.BackColor = System.Drawing.Color.Transparent
        Me.lnkAllocation.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lnkAllocation.LinkBehavior = System.Windows.Forms.LinkBehavior.NeverUnderline
        Me.lnkAllocation.Location = New System.Drawing.Point(572, 5)
        Me.lnkAllocation.Name = "lnkAllocation"
        Me.lnkAllocation.Size = New System.Drawing.Size(96, 16)
        Me.lnkAllocation.TabIndex = 284
        Me.lnkAllocation.TabStop = True
        Me.lnkAllocation.Text = "Allocations"
        Me.lnkAllocation.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'objbtnReset
        '
        Me.objbtnReset.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.objbtnReset.BackColor = System.Drawing.Color.Transparent
        Me.objbtnReset.ButtonType = eZee.Common.eZeeSearchResetButton.EnumButtonType.Reset
        Me.objbtnReset.Image = CType(resources.GetObject("objbtnReset.Image"), System.Drawing.Image)
        Me.objbtnReset.Location = New System.Drawing.Point(698, 0)
        Me.objbtnReset.Name = "objbtnReset"
        Me.objbtnReset.ResultMessage = ""
        Me.objbtnReset.SearchMessage = ""
        Me.objbtnReset.Size = New System.Drawing.Size(24, 24)
        Me.objbtnReset.TabIndex = 67
        Me.objbtnReset.TabStop = False
        '
        'objbtnSearch
        '
        Me.objbtnSearch.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.objbtnSearch.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearch.ButtonType = eZee.Common.eZeeSearchResetButton.EnumButtonType.Search
        Me.objbtnSearch.Image = CType(resources.GetObject("objbtnSearch.Image"), System.Drawing.Image)
        Me.objbtnSearch.Location = New System.Drawing.Point(675, 0)
        Me.objbtnSearch.Name = "objbtnSearch"
        Me.objbtnSearch.ResultMessage = ""
        Me.objbtnSearch.SearchMessage = ""
        Me.objbtnSearch.Size = New System.Drawing.Size(24, 24)
        Me.objbtnSearch.TabIndex = 66
        Me.objbtnSearch.TabStop = False
        '
        'pnlAppDate
        '
        Me.pnlAppDate.Controls.Add(Me.lblToDate)
        Me.pnlAppDate.Controls.Add(Me.dtpDate1)
        Me.pnlAppDate.Controls.Add(Me.lblFromDate)
        Me.pnlAppDate.Controls.Add(Me.dtpDate2)
        Me.pnlAppDate.Location = New System.Drawing.Point(529, 28)
        Me.pnlAppDate.Name = "pnlAppDate"
        Me.pnlAppDate.Size = New System.Drawing.Size(190, 52)
        Me.pnlAppDate.TabIndex = 298
        '
        'lblToDate
        '
        Me.lblToDate.BackColor = System.Drawing.Color.Transparent
        Me.lblToDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblToDate.Location = New System.Drawing.Point(6, 32)
        Me.lblToDate.Name = "lblToDate"
        Me.lblToDate.Size = New System.Drawing.Size(69, 15)
        Me.lblToDate.TabIndex = 305
        Me.lblToDate.Text = "To Date"
        Me.lblToDate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'dtpDate1
        '
        Me.dtpDate1.CalendarFont = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpDate1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpDate1.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpDate1.Location = New System.Drawing.Point(81, 2)
        Me.dtpDate1.Name = "dtpDate1"
        Me.dtpDate1.Size = New System.Drawing.Size(104, 21)
        Me.dtpDate1.TabIndex = 1
        '
        'lblFromDate
        '
        Me.lblFromDate.BackColor = System.Drawing.Color.Transparent
        Me.lblFromDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblFromDate.Location = New System.Drawing.Point(6, 5)
        Me.lblFromDate.Name = "lblFromDate"
        Me.lblFromDate.Size = New System.Drawing.Size(69, 15)
        Me.lblFromDate.TabIndex = 304
        Me.lblFromDate.Text = "From Date"
        Me.lblFromDate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'dtpDate2
        '
        Me.dtpDate2.CalendarFont = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpDate2.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpDate2.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpDate2.Location = New System.Drawing.Point(81, 29)
        Me.dtpDate2.Name = "dtpDate2"
        Me.dtpDate2.Size = New System.Drawing.Size(104, 21)
        Me.dtpDate2.TabIndex = 304
        '
        'pnlYear
        '
        Me.pnlYear.Controls.Add(Me.lblFromYear)
        Me.pnlYear.Controls.Add(Me.nudToYear)
        Me.pnlYear.Controls.Add(Me.LblToYear)
        Me.pnlYear.Controls.Add(Me.nudFromMonth)
        Me.pnlYear.Controls.Add(Me.LblToMonth)
        Me.pnlYear.Controls.Add(Me.LblMonth)
        Me.pnlYear.Controls.Add(Me.nudToMonth)
        Me.pnlYear.Controls.Add(Me.cboTocondition)
        Me.pnlYear.Controls.Add(Me.nudFromYear)
        Me.pnlYear.Controls.Add(Me.cboFromcondition)
        Me.pnlYear.Location = New System.Drawing.Point(162, 28)
        Me.pnlYear.Name = "pnlYear"
        Me.pnlYear.Size = New System.Drawing.Size(363, 52)
        Me.pnlYear.TabIndex = 303
        '
        'lblFromYear
        '
        Me.lblFromYear.BackColor = System.Drawing.Color.Transparent
        Me.lblFromYear.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblFromYear.Location = New System.Drawing.Point(3, 5)
        Me.lblFromYear.Name = "lblFromYear"
        Me.lblFromYear.Size = New System.Drawing.Size(57, 15)
        Me.lblFromYear.TabIndex = 279
        Me.lblFromYear.Text = "From Year"
        Me.lblFromYear.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'nudToYear
        '
        Me.nudToYear.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.nudToYear.Location = New System.Drawing.Point(67, 29)
        Me.nudToYear.Name = "nudToYear"
        Me.nudToYear.Size = New System.Drawing.Size(55, 21)
        Me.nudToYear.TabIndex = 292
        Me.nudToYear.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'LblToYear
        '
        Me.LblToYear.BackColor = System.Drawing.Color.Transparent
        Me.LblToYear.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblToYear.Location = New System.Drawing.Point(3, 32)
        Me.LblToYear.Name = "LblToYear"
        Me.LblToYear.Size = New System.Drawing.Size(57, 15)
        Me.LblToYear.TabIndex = 291
        Me.LblToYear.Text = "To Year"
        Me.LblToYear.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'nudFromMonth
        '
        Me.nudFromMonth.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.nudFromMonth.Location = New System.Drawing.Point(202, 2)
        Me.nudFromMonth.Maximum = New Decimal(New Integer() {11, 0, 0, 0})
        Me.nudFromMonth.Name = "nudFromMonth"
        Me.nudFromMonth.Size = New System.Drawing.Size(55, 21)
        Me.nudFromMonth.TabIndex = 289
        Me.nudFromMonth.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'LblToMonth
        '
        Me.LblToMonth.BackColor = System.Drawing.Color.Transparent
        Me.LblToMonth.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblToMonth.Location = New System.Drawing.Point(128, 32)
        Me.LblToMonth.Name = "LblToMonth"
        Me.LblToMonth.Size = New System.Drawing.Size(68, 15)
        Me.LblToMonth.TabIndex = 293
        Me.LblToMonth.Text = "To Month"
        Me.LblToMonth.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'LblMonth
        '
        Me.LblMonth.BackColor = System.Drawing.Color.Transparent
        Me.LblMonth.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblMonth.Location = New System.Drawing.Point(128, 5)
        Me.LblMonth.Name = "LblMonth"
        Me.LblMonth.Size = New System.Drawing.Size(68, 15)
        Me.LblMonth.TabIndex = 288
        Me.LblMonth.Text = "From Month"
        Me.LblMonth.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'nudToMonth
        '
        Me.nudToMonth.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.nudToMonth.Location = New System.Drawing.Point(202, 29)
        Me.nudToMonth.Maximum = New Decimal(New Integer() {11, 0, 0, 0})
        Me.nudToMonth.Name = "nudToMonth"
        Me.nudToMonth.Size = New System.Drawing.Size(55, 21)
        Me.nudToMonth.TabIndex = 294
        Me.nudToMonth.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'cboTocondition
        '
        Me.cboTocondition.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboTocondition.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboTocondition.FormattingEnabled = True
        Me.cboTocondition.Location = New System.Drawing.Point(263, 29)
        Me.cboTocondition.Name = "cboTocondition"
        Me.cboTocondition.Size = New System.Drawing.Size(97, 21)
        Me.cboTocondition.TabIndex = 297
        '
        'nudFromYear
        '
        Me.nudFromYear.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.nudFromYear.Location = New System.Drawing.Point(67, 2)
        Me.nudFromYear.Name = "nudFromYear"
        Me.nudFromYear.Size = New System.Drawing.Size(55, 21)
        Me.nudFromYear.TabIndex = 281
        Me.nudFromYear.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'cboFromcondition
        '
        Me.cboFromcondition.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboFromcondition.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboFromcondition.FormattingEnabled = True
        Me.cboFromcondition.Location = New System.Drawing.Point(263, 2)
        Me.cboFromcondition.Name = "cboFromcondition"
        Me.cboFromcondition.Size = New System.Drawing.Size(97, 21)
        Me.cboFromcondition.TabIndex = 295
        '
        'pnlFliter
        '
        Me.pnlFliter.Controls.Add(Me.radExpYear)
        Me.pnlFliter.Controls.Add(Me.radAppointedDate)
        Me.pnlFliter.Location = New System.Drawing.Point(9, 28)
        Me.pnlFliter.Name = "pnlFliter"
        Me.pnlFliter.Size = New System.Drawing.Size(152, 52)
        Me.pnlFliter.TabIndex = 1
        '
        'radExpYear
        '
        Me.radExpYear.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.radExpYear.Location = New System.Drawing.Point(9, 3)
        Me.radExpYear.Name = "radExpYear"
        Me.radExpYear.Size = New System.Drawing.Size(139, 16)
        Me.radExpYear.TabIndex = 303
        Me.radExpYear.TabStop = True
        Me.radExpYear.Text = "Year of Experience"
        Me.radExpYear.UseVisualStyleBackColor = True
        '
        'radAppointedDate
        '
        Me.radAppointedDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.radAppointedDate.Location = New System.Drawing.Point(9, 30)
        Me.radAppointedDate.Name = "radAppointedDate"
        Me.radAppointedDate.Size = New System.Drawing.Size(139, 16)
        Me.radAppointedDate.TabIndex = 1
        Me.radAppointedDate.TabStop = True
        Me.radAppointedDate.Text = "Appointment Date"
        Me.radAppointedDate.UseVisualStyleBackColor = True
        '
        'LblValue
        '
        Me.LblValue.BackColor = System.Drawing.Color.Transparent
        Me.LblValue.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblValue.Location = New System.Drawing.Point(380, 5)
        Me.LblValue.Name = "LblValue"
        Me.LblValue.Size = New System.Drawing.Size(338, 16)
        Me.LblValue.TabIndex = 292
        Me.LblValue.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'LblFrom
        '
        Me.LblFrom.BackColor = System.Drawing.Color.Transparent
        Me.LblFrom.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblFrom.Location = New System.Drawing.Point(312, 5)
        Me.LblFrom.Name = "LblFrom"
        Me.LblFrom.Size = New System.Drawing.Size(59, 16)
        Me.LblFrom.TabIndex = 291
        Me.LblFrom.Text = "From"
        Me.LblFrom.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtEmpSearch
        '
        Me.txtEmpSearch.Dock = System.Windows.Forms.DockStyle.Top
        Me.txtEmpSearch.Flags = 0
        Me.txtEmpSearch.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtEmpSearch.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtEmpSearch.Location = New System.Drawing.Point(0, 0)
        Me.txtEmpSearch.Name = "txtEmpSearch"
        Me.txtEmpSearch.Size = New System.Drawing.Size(474, 21)
        Me.txtEmpSearch.TabIndex = 290
        '
        'chkEmpSelectAll
        '
        Me.chkEmpSelectAll.AutoSize = True
        Me.chkEmpSelectAll.Location = New System.Drawing.Point(6, 27)
        Me.chkEmpSelectAll.Name = "chkEmpSelectAll"
        Me.chkEmpSelectAll.Size = New System.Drawing.Size(15, 14)
        Me.chkEmpSelectAll.TabIndex = 293
        Me.chkEmpSelectAll.UseVisualStyleBackColor = True
        '
        'dgEmployee
        '
        Me.dgEmployee.AllowUserToAddRows = False
        Me.dgEmployee.AllowUserToDeleteRows = False
        Me.dgEmployee.AllowUserToResizeRows = False
        Me.dgEmployee.BackgroundColor = System.Drawing.Color.WhiteSmoke
        Me.dgEmployee.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.dgEmployee.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing
        Me.dgEmployee.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.objEmpSelect, Me.dgColhEmpCode, Me.dgColhEmployee, Me.dgColhAppointdate, Me.dgcolhJobTitle})
        Me.dgEmployee.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dgEmployee.Location = New System.Drawing.Point(0, 21)
        Me.dgEmployee.Name = "dgEmployee"
        Me.dgEmployee.RowHeadersVisible = False
        Me.dgEmployee.RowHeadersWidth = 5
        Me.dgEmployee.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing
        Me.dgEmployee.Size = New System.Drawing.Size(474, 297)
        Me.dgEmployee.TabIndex = 294
        '
        'objEmpSelect
        '
        Me.objEmpSelect.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None
        Me.objEmpSelect.Frozen = True
        Me.objEmpSelect.HeaderText = ""
        Me.objEmpSelect.Name = "objEmpSelect"
        Me.objEmpSelect.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.objEmpSelect.Width = 25
        '
        'dgColhEmpCode
        '
        Me.dgColhEmpCode.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None
        Me.dgColhEmpCode.HeaderText = "Code"
        Me.dgColhEmpCode.Name = "dgColhEmpCode"
        Me.dgColhEmpCode.ReadOnly = True
        Me.dgColhEmpCode.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgColhEmpCode.Width = 125
        '
        'dgColhEmployee
        '
        Me.dgColhEmployee.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None
        Me.dgColhEmployee.HeaderText = "Employee"
        Me.dgColhEmployee.Name = "dgColhEmployee"
        Me.dgColhEmployee.ReadOnly = True
        Me.dgColhEmployee.Width = 200
        '
        'dgColhAppointdate
        '
        Me.dgColhAppointdate.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None
        Me.dgColhAppointdate.HeaderText = "Appointment Date"
        Me.dgColhAppointdate.Name = "dgColhAppointdate"
        Me.dgColhAppointdate.ReadOnly = True
        Me.dgColhAppointdate.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        '
        'dgcolhJobTitle
        '
        Me.dgcolhJobTitle.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None
        Me.dgcolhJobTitle.HeaderText = "Job Title"
        Me.dgcolhJobTitle.Name = "dgcolhJobTitle"
        Me.dgcolhJobTitle.ReadOnly = True
        Me.dgcolhJobTitle.Width = 230
        '
        'gbLeaveInfo
        '
        Me.gbLeaveInfo.BorderColor = System.Drawing.Color.Black
        Me.gbLeaveInfo.Checked = False
        Me.gbLeaveInfo.CollapseAllExceptThis = False
        Me.gbLeaveInfo.CollapsedHoverImage = Nothing
        Me.gbLeaveInfo.CollapsedNormalImage = Nothing
        Me.gbLeaveInfo.CollapsedPressedImage = Nothing
        Me.gbLeaveInfo.CollapseOnLoad = False
        Me.gbLeaveInfo.Controls.Add(Me.SplitContainer1)
        Me.gbLeaveInfo.Controls.Add(Me.LblValue)
        Me.gbLeaveInfo.Controls.Add(Me.LblFrom)
        Me.gbLeaveInfo.ExpandedHoverImage = Nothing
        Me.gbLeaveInfo.ExpandedNormalImage = Nothing
        Me.gbLeaveInfo.ExpandedPressedImage = Nothing
        Me.gbLeaveInfo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbLeaveInfo.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbLeaveInfo.HeaderHeight = 25
        Me.gbLeaveInfo.HeaderMessage = ""
        Me.gbLeaveInfo.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbLeaveInfo.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbLeaveInfo.HeightOnCollapse = 0
        Me.gbLeaveInfo.LeftTextSpace = 0
        Me.gbLeaveInfo.Location = New System.Drawing.Point(0, 88)
        Me.gbLeaveInfo.Name = "gbLeaveInfo"
        Me.gbLeaveInfo.OpenHeight = 300
        Me.gbLeaveInfo.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbLeaveInfo.ShowBorder = True
        Me.gbLeaveInfo.ShowCheckBox = False
        Me.gbLeaveInfo.ShowCollapseButton = False
        Me.gbLeaveInfo.ShowDefaultBorderColor = True
        Me.gbLeaveInfo.ShowDownButton = False
        Me.gbLeaveInfo.ShowHeader = True
        Me.gbLeaveInfo.Size = New System.Drawing.Size(724, 347)
        Me.gbLeaveInfo.TabIndex = 295
        Me.gbLeaveInfo.Temp = 0
        Me.gbLeaveInfo.Text = "Assignment Info"
        Me.gbLeaveInfo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'SplitContainer1
        '
        Me.SplitContainer1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.SplitContainer1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.SplitContainer1.Location = New System.Drawing.Point(3, 26)
        Me.SplitContainer1.Name = "SplitContainer1"
        '
        'SplitContainer1.Panel1
        '
        Me.SplitContainer1.Panel1.Controls.Add(Me.chkSecRouteSelectAll)
        Me.SplitContainer1.Panel1.Controls.Add(Me.dgSecRoute)
        Me.SplitContainer1.Panel1.Controls.Add(Me.txtSecRouteSearch)
        '
        'SplitContainer1.Panel2
        '
        Me.SplitContainer1.Panel2.Controls.Add(Me.chkEmpSelectAll)
        Me.SplitContainer1.Panel2.Controls.Add(Me.dgEmployee)
        Me.SplitContainer1.Panel2.Controls.Add(Me.txtEmpSearch)
        Me.SplitContainer1.Panel2.Controls.Add(Me.cboSector)
        Me.SplitContainer1.Panel2.Controls.Add(Me.lblSector)
        Me.SplitContainer1.Panel2.Controls.Add(Me.objbtnSearchSector)
        Me.SplitContainer1.Size = New System.Drawing.Size(719, 320)
        Me.SplitContainer1.SplitterDistance = 239
        Me.SplitContainer1.TabIndex = 297
        '
        'chkSecRouteSelectAll
        '
        Me.chkSecRouteSelectAll.AutoSize = True
        Me.chkSecRouteSelectAll.Location = New System.Drawing.Point(7, 27)
        Me.chkSecRouteSelectAll.Name = "chkSecRouteSelectAll"
        Me.chkSecRouteSelectAll.Size = New System.Drawing.Size(15, 14)
        Me.chkSecRouteSelectAll.TabIndex = 295
        Me.chkSecRouteSelectAll.UseVisualStyleBackColor = True
        '
        'dgSecRoute
        '
        Me.dgSecRoute.AllowUserToAddRows = False
        Me.dgSecRoute.AllowUserToDeleteRows = False
        Me.dgSecRoute.BackgroundColor = System.Drawing.Color.WhiteSmoke
        Me.dgSecRoute.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.dgSecRoute.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing
        Me.dgSecRoute.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.objSecRouteSelect, Me.dgcolhSecRoute, Me.objdgcolhSecRouteId})
        Me.dgSecRoute.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dgSecRoute.Location = New System.Drawing.Point(0, 21)
        Me.dgSecRoute.Name = "dgSecRoute"
        Me.dgSecRoute.RowHeadersVisible = False
        Me.dgSecRoute.Size = New System.Drawing.Size(237, 297)
        Me.dgSecRoute.TabIndex = 292
        '
        'objSecRouteSelect
        '
        Me.objSecRouteSelect.Frozen = True
        Me.objSecRouteSelect.HeaderText = ""
        Me.objSecRouteSelect.Name = "objSecRouteSelect"
        Me.objSecRouteSelect.Width = 25
        '
        'dgcolhSecRoute
        '
        Me.dgcolhSecRoute.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.dgcolhSecRoute.HeaderText = "Sector/Route"
        Me.dgcolhSecRoute.Name = "dgcolhSecRoute"
        Me.dgcolhSecRoute.ReadOnly = True
        Me.dgcolhSecRoute.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        '
        'objdgcolhSecRouteId
        '
        Me.objdgcolhSecRouteId.HeaderText = "SecRouteId"
        Me.objdgcolhSecRouteId.Name = "objdgcolhSecRouteId"
        Me.objdgcolhSecRouteId.ReadOnly = True
        Me.objdgcolhSecRouteId.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.objdgcolhSecRouteId.Visible = False
        '
        'txtSecRouteSearch
        '
        Me.txtSecRouteSearch.Dock = System.Windows.Forms.DockStyle.Top
        Me.txtSecRouteSearch.Flags = 0
        Me.txtSecRouteSearch.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtSecRouteSearch.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtSecRouteSearch.Location = New System.Drawing.Point(0, 0)
        Me.txtSecRouteSearch.Name = "txtSecRouteSearch"
        Me.txtSecRouteSearch.Size = New System.Drawing.Size(237, 21)
        Me.txtSecRouteSearch.TabIndex = 291
        '
        'cboSector
        '
        Me.cboSector.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboSector.DropDownWidth = 400
        Me.cboSector.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboSector.FormattingEnabled = True
        Me.cboSector.Location = New System.Drawing.Point(-96, 294)
        Me.cboSector.Name = "cboSector"
        Me.cboSector.Size = New System.Drawing.Size(207, 21)
        Me.cboSector.TabIndex = 296
        Me.cboSector.Visible = False
        '
        'lblSector
        '
        Me.lblSector.BackColor = System.Drawing.Color.Transparent
        Me.lblSector.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblSector.Location = New System.Drawing.Point(135, 294)
        Me.lblSector.Name = "lblSector"
        Me.lblSector.Size = New System.Drawing.Size(100, 17)
        Me.lblSector.TabIndex = 297
        Me.lblSector.Text = "Sector/Route"
        Me.lblSector.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.lblSector.Visible = False
        '
        'objbtnSearchSector
        '
        Me.objbtnSearchSector.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchSector.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchSector.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchSector.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchSector.BorderSelected = False
        Me.objbtnSearchSector.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchSector.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objbtnSearchSector.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchSector.Location = New System.Drawing.Point(117, 296)
        Me.objbtnSearchSector.Name = "objbtnSearchSector"
        Me.objbtnSearchSector.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchSector.TabIndex = 298
        Me.objbtnSearchSector.Visible = False
        '
        'DataGridViewTextBoxColumn1
        '
        Me.DataGridViewTextBoxColumn1.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None
        Me.DataGridViewTextBoxColumn1.HeaderText = "Code"
        Me.DataGridViewTextBoxColumn1.Name = "DataGridViewTextBoxColumn1"
        Me.DataGridViewTextBoxColumn1.ReadOnly = True
        Me.DataGridViewTextBoxColumn1.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.DataGridViewTextBoxColumn1.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn1.Visible = False
        Me.DataGridViewTextBoxColumn1.Width = 90
        '
        'DataGridViewTextBoxColumn2
        '
        Me.DataGridViewTextBoxColumn2.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.DataGridViewTextBoxColumn2.HeaderText = "Employee"
        Me.DataGridViewTextBoxColumn2.Name = "DataGridViewTextBoxColumn2"
        Me.DataGridViewTextBoxColumn2.ReadOnly = True
        Me.DataGridViewTextBoxColumn2.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.DataGridViewTextBoxColumn2.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn2.Visible = False
        '
        'DataGridViewTextBoxColumn3
        '
        Me.DataGridViewTextBoxColumn3.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None
        Me.DataGridViewTextBoxColumn3.HeaderText = "Appointment Date"
        Me.DataGridViewTextBoxColumn3.Name = "DataGridViewTextBoxColumn3"
        Me.DataGridViewTextBoxColumn3.ReadOnly = True
        Me.DataGridViewTextBoxColumn3.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.DataGridViewTextBoxColumn3.Width = 200
        '
        'DataGridViewTextBoxColumn4
        '
        Me.DataGridViewTextBoxColumn4.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None
        Me.DataGridViewTextBoxColumn4.HeaderText = "Job Title"
        Me.DataGridViewTextBoxColumn4.Name = "DataGridViewTextBoxColumn4"
        Me.DataGridViewTextBoxColumn4.ReadOnly = True
        Me.DataGridViewTextBoxColumn4.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.DataGridViewTextBoxColumn4.Width = 230
        '
        'DataGridViewTextBoxColumn5
        '
        Me.DataGridViewTextBoxColumn5.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None
        Me.DataGridViewTextBoxColumn5.HeaderText = "Job Title"
        Me.DataGridViewTextBoxColumn5.Name = "DataGridViewTextBoxColumn5"
        Me.DataGridViewTextBoxColumn5.ReadOnly = True
        Me.DataGridViewTextBoxColumn5.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.DataGridViewTextBoxColumn5.Width = 230
        '
        'DataGridViewTextBoxColumn6
        '
        Me.DataGridViewTextBoxColumn6.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None
        Me.DataGridViewTextBoxColumn6.HeaderText = "Job Title"
        Me.DataGridViewTextBoxColumn6.Name = "DataGridViewTextBoxColumn6"
        Me.DataGridViewTextBoxColumn6.ReadOnly = True
        Me.DataGridViewTextBoxColumn6.Width = 230
        '
        'cmnuSecRouteEmpData
        '
        Me.cmnuSecRouteEmpData.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.btnExportExistAssignment})
        Me.cmnuSecRouteEmpData.Name = "cmImportAccrueLeave"
        Me.cmnuSecRouteEmpData.Size = New System.Drawing.Size(243, 48)
        '
        'btnExportExistAssignment
        '
        Me.btnExportExistAssignment.Enabled = False
        Me.btnExportExistAssignment.Name = "btnExportExistAssignment"
        Me.btnExportExistAssignment.Size = New System.Drawing.Size(242, 22)
        Me.btnExportExistAssignment.Text = "Export Already Exist Assignment"
        '
        'frmEmployeeSectors
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(725, 496)
        Me.Controls.Add(Me.gbLeaveInfo)
        Me.Controls.Add(Me.gbFilterCriteria)
        Me.Controls.Add(Me.objFooter)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmEmployeeSectors"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Employee Sector Assignment"
        Me.objFooter.ResumeLayout(False)
        Me.objFooter.PerformLayout()
        Me.gbFilterCriteria.ResumeLayout(False)
        CType(Me.objbtnReset, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.objbtnSearch, System.ComponentModel.ISupportInitialize).EndInit()
        Me.pnlAppDate.ResumeLayout(False)
        Me.pnlYear.ResumeLayout(False)
        CType(Me.nudToYear, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.nudFromMonth, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.nudToMonth, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.nudFromYear, System.ComponentModel.ISupportInitialize).EndInit()
        Me.pnlFliter.ResumeLayout(False)
        CType(Me.dgEmployee, System.ComponentModel.ISupportInitialize).EndInit()
        Me.gbLeaveInfo.ResumeLayout(False)
        Me.SplitContainer1.Panel1.ResumeLayout(False)
        Me.SplitContainer1.Panel1.PerformLayout()
        Me.SplitContainer1.Panel2.ResumeLayout(False)
        Me.SplitContainer1.Panel2.PerformLayout()
        Me.SplitContainer1.ResumeLayout(False)
        CType(Me.dgSecRoute, System.ComponentModel.ISupportInitialize).EndInit()
        Me.cmnuSecRouteEmpData.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents objFooter As eZee.Common.eZeeFooter
    Friend WithEvents btnSave As eZee.Common.eZeeLightButton
    Friend WithEvents btnClose As eZee.Common.eZeeLightButton
    Friend WithEvents gbFilterCriteria As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents pnlAppDate As System.Windows.Forms.Panel
    Friend WithEvents lblToDate As System.Windows.Forms.Label
    Friend WithEvents dtpDate1 As System.Windows.Forms.DateTimePicker
    Friend WithEvents lblFromDate As System.Windows.Forms.Label
    Friend WithEvents dtpDate2 As System.Windows.Forms.DateTimePicker
    Friend WithEvents pnlYear As System.Windows.Forms.Panel
    Friend WithEvents lblFromYear As System.Windows.Forms.Label
    Friend WithEvents nudToYear As System.Windows.Forms.NumericUpDown
    Friend WithEvents LblToYear As System.Windows.Forms.Label
    Friend WithEvents nudFromMonth As System.Windows.Forms.NumericUpDown
    Friend WithEvents LblToMonth As System.Windows.Forms.Label
    Friend WithEvents LblMonth As System.Windows.Forms.Label
    Friend WithEvents nudToMonth As System.Windows.Forms.NumericUpDown
    Friend WithEvents cboTocondition As System.Windows.Forms.ComboBox
    Friend WithEvents nudFromYear As System.Windows.Forms.NumericUpDown
    Friend WithEvents cboFromcondition As System.Windows.Forms.ComboBox
    Friend WithEvents pnlFliter As System.Windows.Forms.Panel
    Friend WithEvents radExpYear As System.Windows.Forms.RadioButton
    Friend WithEvents radAppointedDate As System.Windows.Forms.RadioButton
    Friend WithEvents lnkAllocation As System.Windows.Forms.LinkLabel
    Friend WithEvents objbtnReset As eZee.Common.eZeeSearchResetButton
    Friend WithEvents objbtnSearch As eZee.Common.eZeeSearchResetButton
    Friend WithEvents LblValue As System.Windows.Forms.Label
    Friend WithEvents LblFrom As System.Windows.Forms.Label
    Friend WithEvents txtEmpSearch As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents chkEmpSelectAll As System.Windows.Forms.CheckBox
    Friend WithEvents dgEmployee As System.Windows.Forms.DataGridView
    Friend WithEvents lnkSecRouteProcess As System.Windows.Forms.LinkLabel
    Friend WithEvents DataGridViewTextBoxColumn1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn2 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn3 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn4 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents gbLeaveInfo As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents SplitContainer1 As System.Windows.Forms.SplitContainer
    Friend WithEvents txtSecRouteSearch As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents dgSecRoute As System.Windows.Forms.DataGridView
    Friend WithEvents chkSecRouteSelectAll As System.Windows.Forms.CheckBox
    Friend WithEvents objSecRouteSelect As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents dgcolhSecRoute As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhSecRouteId As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn5 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn6 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objEmpSelect As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents dgColhEmpCode As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgColhEmployee As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgColhAppointdate As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhJobTitle As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents cboSector As System.Windows.Forms.ComboBox
    Friend WithEvents lblSector As System.Windows.Forms.Label
    Friend WithEvents objbtnSearchSector As eZee.Common.eZeeGradientButton
    Friend WithEvents lnkEmpProcess As System.Windows.Forms.LinkLabel
    Friend WithEvents objbtnImport As eZee.Common.eZeeSplitButton
    Friend WithEvents cmnuSecRouteEmpData As System.Windows.Forms.ContextMenuStrip
    Friend WithEvents btnExportExistAssignment As System.Windows.Forms.ToolStripMenuItem
End Class
