﻿Option Strict On

#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data

#End Region

Public Class frmEmployeeExpensesList

#Region " Private Variables "

    Private ReadOnly mstrModuleName As String = "frmEmployeeExpensesList"
    Private objEmpExpBal As clsEmployeeExpenseBalance
    Private mblnCancel As Boolean = True
    Private dtTab As DataTable

    Private imgPlusIcon As Image = My.Resources.plus_blue
    Private imgMinusIcon As Image = My.Resources.minus_blue
    Private imgBlankIcon As Image = My.Resources.blankImage

#End Region

#Region " Private Methods "

    Private Sub FillCombo()
        Dim dsCombo As New DataSet
        Dim objEmp As New clsEmployee_Master
        Try
            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'dsCombo = objEmp.GetEmployeeList("List", True, True, , , , , , , , , , , , eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate))
            dsCombo = objEmp.GetEmployeeList(FinancialYear._Object._DatabaseName, _
                                             User._Object._Userunkid, _
                                             FinancialYear._Object._YearUnkid, _
                                             Company._Object._Companyunkid, _
                                             eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                             eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                             ConfigParameter._Object._UserAccessModeSetting, _
                                             True, False, "List", True)
            'S.SANDEEP [04 JUN 2015] -- END
            With cboEmployee
                .ValueMember = "employeeunkid"
                .DisplayMember = "employeename"
                .DataSource = dsCombo.Tables(0)
                .SelectedValue = 0
            End With


            'Pinkal (11-Sep-2019) -- Start
            'Enhancement NMB - Working On Claim Retirement for NMB.
            'dsCombo = clsExpCommonMethods.Get_ExpenseTypes(True, True, True, "List", True)
            dsCombo = clsExpCommonMethods.Get_ExpenseTypes(True, True, True, "List", True, True)
            'Pinkal (11-Sep-2019) -- End

            With cboExCategory
                .ValueMember = "Id"
                .DisplayMember = "Name"
                .DataSource = dsCombo.Tables(0)
                .SelectedValue = 0
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub FillGrid()
        Dim dsList As New DataSet
        Try

            'Pinkal (26-Dec-2018) -- Start
            'Enhancement - Working on Leave/Claim Enhancement Phase 2 for NMB.
            Me.Cursor = Cursors.WaitCursor
            'Pinkal (26-Dec-2018) -- End

            If User._Object.Privilege._AllowtoViewEmpExpesneAssignment = False Then Exit Sub
            dsList = objEmpExpBal.Display_List("List", , CInt(cboEmployee.SelectedValue), CInt(cboExCategory.SelectedValue), CInt(cboExpense.SelectedValue))
            'Pinkal (26-Dec-2018) -- Start
            'Enhancement - Working on Leave/Claim Enhancement Phase 2 for NMB.
            'dtTab = New DataView(dsList.Tables(0), "", "", DataViewRowState.CurrentRows).ToTable
            dtTab = dsList.Tables(0)
            'Pinkal (26-Dec-2018) -- End
            Call SetDataSource()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillGrid", mstrModuleName)
        Finally
            'Pinkal (26-Dec-2018) -- Start
            'Enhancement - Working on Leave/Claim Enhancement Phase 2 for NMB.
            Me.Cursor = Cursors.Default
            'Pinkal (26-Dec-2018) -- End
        End Try
    End Sub

    Private Sub SetGridColor()
        Try

            'Pinkal (26-Dec-2018) -- Start
            'Enhancement - Working on Leave/Claim Enhancement Phase 2 for NMB.

            'Dim dgvcsHeader As New DataGridViewCellStyle
            'dgvcsHeader.ForeColor = Color.White
            'dgvcsHeader.SelectionBackColor = Color.Gray
            'dgvcsHeader.SelectionForeColor = Color.White
            'dgvcsHeader.BackColor = Color.Gray

            'For i As Integer = 0 To dgEmployee.RowCount - 1
            '    If CBool(dgEmployee.Rows(i).Cells(objdgcolhIsGrp.Index).Value) = True Then
            '        dgEmployee.Rows(i).DefaultCellStyle = dgvcsHeader
            '    End If
            'Next

            Dim dr = From dgdr As DataGridViewRow In dgEmployee.Rows.Cast(Of DataGridViewRow)() Where CBool(dgdr.Cells(objdgcolhIsGrp.Index).Value) = True Select dgdr
            dr.ToList.ForEach(Function(x) SetRowStyle(x, True))

            'dr = Nothing
            'dr = From dgdr As DataGridViewRow In dgEmployee.Rows.Cast(Of DataGridViewRow)() Where CBool(dgdr.Cells(objdgcolhIsGrp.Index).Value) = False Select dgdr
            'dr.ToList.ForEach(Function(x) SetRowStyle(x, False))

            'Pinkal (26-Dec-2018) -- End

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetGridColor", mstrModuleName)
        Finally
        End Try
    End Sub

    'Pinkal (26-Dec-2018) -- Start
    'Enhancement - Working on Leave/Claim Enhancement Phase 2 for NMB.

    'Private Sub SetCollapseValue()
    '    Try
    '        Dim objdgvsCollapseHeader As New DataGridViewCellStyle
    '        Dim objCMManager As CurrencyManager = CType(BindingContext(dgEmployee.DataSource), CurrencyManager)
    '        objdgvsCollapseHeader.Font = New Font(dgEmployee.Font.FontFamily, 13, FontStyle.Bold)
    '        objdgvsCollapseHeader.ForeColor = Color.White
    '        objdgvsCollapseHeader.BackColor = Color.Gray
    '        objdgvsCollapseHeader.SelectionBackColor = Color.Gray
    '        objdgvsCollapseHeader.Alignment = DataGridViewContentAlignment.MiddleCenter

    '        For i As Integer = 0 To dgEmployee.RowCount - 1
    '            If CBool(dgEmployee.Rows(i).Cells(objdgcolhIsGrp.Index).Value) = True Then
    '                If dgEmployee.Rows(i).Cells(objdgcolhCollapse.Index).Value Is Nothing Then
    '                    dgEmployee.Rows(i).Cells(objdgcolhCollapse.Index).Value = imgPlusIcon
    '                End If
    '                dgEmployee.Rows(i).Cells(objdgcolhCollapse.Index).Style = objdgvsCollapseHeader
    '            Else
    '                dgEmployee.Rows(i).Cells(objdgcolhCollapse.Index).Value = imgBlankIcon
    '                objCMManager.SuspendBinding()
    '                'dgEmployee.Rows(i).Visible = False
    '                objCMManager.ResumeBinding()
    '            End If
    '        Next

    '    Catch ex As Exception
    '        DisplayError.Show("-1", ex.Message, "SetCollapseValue", mstrModuleName)
    '    Finally
    '    End Try
    'End Sub

    'Pinkal (26-Dec-2018) -- End

    Private Sub SetDataSource()
        Try
            dgEmployee.AutoGenerateColumns = False
            objdgcolhExpBalaceUnkid.DataPropertyName = "crunkid"
            objdgcolhGrpId.DataPropertyName = "grpid"
            objdgcolhIsGrp.DataPropertyName = "isgrp"
            objdgSelect.DataPropertyName = "ischeck"
            dgcolhAccrue.DataPropertyName = "accrue"
            dgcolhEDate.DataPropertyName = "edate"
            dgcolhExCategory.DataPropertyName = "exptype"
            dgcolhExpenses.DataPropertyName = "expname"
            dgcolhSDate.DataPropertyName = "sdate"
            dgEmployee.DataSource = dtTab

            dgcolhAccrue.HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleRight

            Call SetGridColor()

            'Pinkal (26-Dec-2018) -- Start
            'Enhancement - Working on Leave/Claim Enhancement Phase 2 for NMB.
            'Call SetCollapseValue()
            'Pinkal (26-Dec-2018) -- End
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetDataSource", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub SetVisibility()
        Try
            btnNew.Enabled = User._Object.Privilege._AllowtoAddEmpExpesneAssignment
            btnDelete.Enabled = User._Object.Privilege._AllowtoDeleteEmpExpesneAssignment

            'Pinkal (03-Mar-2021)-- Start
            'KADCO Enhancement  -  Working on  KADCO - Option to Adjust Expense Accrue Balance.
            If Company._Object._Name.ToUpper() = "KADCO" Then
                btnExpenseAdjustment.Visible = User._Object.Privilege._AllowToAdjustExpenseBalance
            Else
                btnExpenseAdjustment.Visible = False
            End If
            'Pinkal (03-Mar-2021) -- End

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetVisibility", mstrModuleName)
        End Try
    End Sub


    'Pinkal (26-Dec-2018) -- Start
    'Enhancement - Working on Leave/Claim Enhancement Phase 2 for NMB.

    Private Function SetRowStyle(ByVal xRow As DataGridViewRow, ByVal mblnIsGroup As Boolean) As Boolean
        Try
            If mblnIsGroup Then
                Dim dgvcsHeader As New DataGridViewCellStyle
                dgvcsHeader.ForeColor = Color.White
                dgvcsHeader.SelectionBackColor = Color.Gray
                dgvcsHeader.SelectionForeColor = Color.White
                dgvcsHeader.BackColor = Color.Gray
                dgvcsHeader.Font = New Font(dgEmployee.Font.FontFamily, dgEmployee.Font.Size, FontStyle.Bold)
                xRow.DefaultCellStyle = dgvcsHeader

                If xRow.Cells(objdgcolhCollapse.Index).Value Is Nothing Then
                    xRow.Cells(objdgcolhCollapse.Index).Value = imgPlusIcon
                End If
                'Else
                '    xRow.Cells(objdgcolhCollapse.Index).Value = imgBlankIcon
                'xRow.Visible = False
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetRowStyle", mstrModuleName)
        End Try
    End Function

    Private Function UpdateAll(ByVal dr As DataRow, ByVal mblnisCheck As Boolean) As Boolean
        Try
            If dr Is Nothing Then Return False
            dr("ischeck") = mblnisCheck
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "UpdateAll", mstrModuleName)
            Return False
        End Try
        Return True
    End Function

    'Pinkal (26-Dec-2018) -- End

#End Region

#Region " Form's Events "

    Private Sub frmEmployeeExpensesList_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        objEmpExpBal = New clsEmployeeExpenseBalance
        Try
            Call Set_Logo(Me, gApplicationType)
            Language.setLanguage(Me.Name)
            Call OtherSettings()
            Call FillCombo()

            'Pinkal (13-Jul-2015) -- Start
            'Enhancement - WORKING ON C & R ACCESS PRIVILEGE.
            Call SetVisibility()
            'Pinkal (13-Jul-2015) -- End
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmEmployeeExpensesList_Load", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()
            clsEmployeeExpenseBalance.SetMessages()
            objfrm._Other_ModuleNames = "clsEmployeeExpenseBalance"
            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub

#End Region

#Region " Button's Event's "

    Private Sub btnNew_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNew.Click
        Dim frm As New frmEmployeeExpenses
        Try
            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If

            If frm.displayDialog(enAction.ADD_CONTINUE) Then
                Call FillGrid()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnNew_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub btnDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDelete.Click
        Try
            If dtTab IsNot Nothing Then
                Dim dtmp() As DataRow = dtTab.Select("ischeck=true and isgrp=false")
                If dtmp.Length <= 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Sorry, Please tick atleast one expense assigned to employee."), enMsgBoxStyle.Information)
                    Exit Sub
                End If
                Dim blnUsed As Boolean = False
                Dim blnShown As Boolean = False
                Dim mstrVoidReason As String = String.Empty
                For i As Integer = 0 To dtmp.Length - 1
                    If blnShown = False Then
                        If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Are you sure you want to delete checked transaction." & vbCrLf & _
                                                               "Do you wish to continue?"), CType(enMsgBoxStyle.Information + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.No Then
                            Exit Sub
                        End If
                        Dim frm As New frmReasonSelection
                        If User._Object._Isrighttoleft = True Then
                            frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                            frm.RightToLeftLayout = True
                            Call Language.ctlRightToLeftlayOut(frm)
                        End If
                        frm.displayDialog(enVoidCategoryType.OTHERS, mstrVoidReason)
                        If mstrVoidReason.Length <= 0 Then
                            Exit Sub
                        End If
                        blnShown = True
                    End If

                    'Pinkal (26-Feb-2019) -- Start
                    'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.
                    'If objEmpExpBal.isUsed(CInt(dtmp(i).Item("crunkid"))) = True Then
                    If objEmpExpBal.isUsed(CInt(dtmp(i).Item("expid")), CInt(dtmp(i).Item("grpid"))) = True Then
                        'Pinkal (26-Feb-2019) -- End
                        dgEmployee.Rows(dtTab.Rows.IndexOf(dtmp(i))).DefaultCellStyle.ForeColor = Color.Red
                        dgEmployee.Rows(dtTab.Rows.IndexOf(dtmp(i))).DefaultCellStyle.SelectionForeColor = Color.Red
                        blnUsed = True
                    Else
                        objEmpExpBal._Isvoid = True
                        objEmpExpBal._Voiddatetime = ConfigParameter._Object._CurrentDateAndTime
                        objEmpExpBal._Voidreason = mstrVoidReason
                        objEmpExpBal._Voiduserunkid = User._Object._Userunkid


                        'S.SANDEEP [28-May-2018] -- START
                        'ISSUE/ENHANCEMENT : {Audit Trails} 
                        objEmpExpBal._FormName = mstrModuleName
                        objEmpExpBal._LoginEmployeeunkid = 0
                        objEmpExpBal._ClientIP = getIP()
                        objEmpExpBal._HostName = getHostName()
                        objEmpExpBal._FromWeb = False
                        objEmpExpBal._AuditUserId = User._Object._Userunkid
objEmpExpBal._CompanyUnkid = Company._Object._Companyunkid
                        objEmpExpBal._AuditDate = ConfigParameter._Object._CurrentDateAndTime
                        'S.SANDEEP [28-May-2018] -- END

                        If objEmpExpBal.Delete(CInt(dtmp(i).Item("crunkid"))) = False Then
                            eZeeMsgBox.Show(objEmpExpBal._Message, enMsgBoxStyle.Information)
                            Exit Sub
                        End If
                    End If
                Next
                If blnUsed = True Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Sorry, some of the ticked expenses are already issued, therefore cannot be deleted and is highlighted in red."), enMsgBoxStyle.Information)
                    Exit Sub
                End If
            End If
            Call FillGrid()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnDelete_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnClose_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub objbtnSearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearch.Click
        Try
            Call FillGrid()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearch_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub objbtnReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnReset.Click
        Try
            cboEmployee.SelectedValue = 0
            cboExCategory.SelectedValue = 0
            cboExpense.SelectedValue = 0
            dgEmployee.DataSource = Nothing
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnReset_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub objbtnSearchEmployee_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchEmployee.Click, objbtnSearchExpense.Click
        Dim frm As New frmCommonSearch
        Try
            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If
            Dim cbo As ComboBox = Nothing
            Select Case CType(sender, eZee.Common.eZeeGradientButton).Name.ToUpper
                Case objbtnSearchEmployee.Name.ToUpper
                    cbo = cboEmployee
                Case objbtnSearchExpense.Name.ToUpper
                    cbo = cboExpense
            End Select
            If cbo.DataSource Is Nothing Then Exit Sub
            With frm
                .ValueMember = cbo.ValueMember
                .DisplayMember = cbo.DisplayMember
                .DataSource = CType(cbo.DataSource, DataTable)
                Select Case CType(sender, eZee.Common.eZeeGradientButton).Name.ToUpper
                    Case objbtnSearchEmployee.Name.ToUpper
                        .CodeMember = "employeecode"
                End Select
                If .DisplayDialog = True Then
                    cbo.SelectedValue = .SelectedValue
                End If
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchEmployee_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub


    'Pinkal (03-Mar-2021)-- Start
    'KADCO Enhancement  -  Working on  KADCO - Option to Adjust Expense Accrue Balance.

    Private Sub btnExpenseAdjustment_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnExpenseAdjustment.Click
        Dim objfrmAdjusment As New frmExpenseAdjustment
        Try
            objfrmAdjusment.displayDialog(enAction.ADD_ONE)
            FillGrid()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnExpenseAdjustment_Click", mstrModuleName)
        Finally
            objfrmAdjusment = Nothing
        End Try
    End Sub

    'Pinkal (03-Mar-2021) -- End


#End Region

#Region " Combobox Event's "

    Private Sub cboExCategory_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboExCategory.SelectedIndexChanged
        Try
            Dim dsCombo As New DataSet
            Dim objExpense As New clsExpense_Master
            dsCombo = objExpense.getComboList(CInt(cboExCategory.SelectedValue), True, "List")
            With cboExpense
                .ValueMember = "Id"
                .DisplayMember = "Name"
                .DataSource = dsCombo.Tables(0)
                .SelectedValue = 0
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboExCategory_SelectedIndexChanged", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

#Region " DataGrid Event's "

    Private Sub dgEmployee_CellContentClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgEmployee.CellContentClick, dgEmployee.CellContentDoubleClick
        Try
            RemoveHandler chkSelectAll.CheckedChanged, AddressOf chkSelectAll_CheckedChanged
            Dim i As Integer
            If e.RowIndex = -1 Then Exit Sub

            If Me.dgEmployee.IsCurrentCellDirty Then
                Me.dgEmployee.CommitEdit(DataGridViewDataErrorContexts.Commit)
            End If

            If CBool(dgEmployee.Rows(e.RowIndex).Cells(objdgcolhIsGrp.Index).Value) = True Then
                Select Case CInt(e.ColumnIndex)
                    Case objdgSelect.Index
                        For i = e.RowIndex + 1 To dgEmployee.RowCount - 1
                            Dim blnFlg As Boolean = False
                            blnFlg = CBool(dgEmployee.Rows(e.RowIndex).Cells(objdgSelect.Index).Value)
                            If CInt(dgEmployee.Rows(e.RowIndex).Cells(objdgcolhGrpId.Index).Value) = CInt(dgEmployee.Rows(i).Cells(objdgcolhGrpId.Index).Value) Then
                                dgEmployee.Rows(i).Cells(objdgSelect.Index).Value = blnFlg
                            End If
                        Next
                    Case objdgcolhCollapse.Index
                        If dgEmployee.Rows(e.RowIndex).Cells(objdgcolhCollapse.Index).Value Is imgMinusIcon Then
                            dgEmployee.Rows(e.RowIndex).Cells(objdgcolhCollapse.Index).Value = imgPlusIcon
                        Else
                            dgEmployee.Rows(e.RowIndex).Cells(objdgcolhCollapse.Index).Value = imgMinusIcon
                        End If

                        For i = e.RowIndex + 1 To dgEmployee.RowCount - 1
                            If CInt(dgEmployee.Rows(e.RowIndex).Cells(objdgcolhGrpId.Index).Value) = CInt(dgEmployee.Rows(i).Cells(objdgcolhGrpId.Index).Value) Then
                                If dgEmployee.Rows(i).Visible = False Then
                                    dgEmployee.Rows(i).Visible = True
                                Else
                                    dgEmployee.Rows(i).Visible = False
                                End If
                            Else
                                Exit For
                            End If
                        Next
                End Select
            End If
            AddHandler chkSelectAll.CheckedChanged, AddressOf chkSelectAll_CheckedChanged
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgEmployee_CellContentClick", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

#Region " Checkbox Event's "

    Private Sub chkSelectAll_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkSelectAll.CheckedChanged
        Try
            RemoveHandler dgEmployee.CellContentClick, AddressOf dgEmployee_CellContentClick
            If dtTab.Rows.Count > 0 Then
                'Pinkal (26-Dec-2018) -- Start
                'Enhancement - Working on Leave/Claim Enhancement Phase 2 for NMB.
                'For Each dr As DataRow In dtTab.Rows
                '    dr("ischeck") = chkSelectAll.Checked
                '    dr.EndEdit()
                'Next
                'dtTab.AcceptChanges()
                Dim dr = dtTab.AsEnumerable()
                dr.ToList().ForEach(Function(x) UpdateAll(x, chkSelectAll.Checked))
            End If
            'Call SetDataSource()
            'Pinkal (26-Dec-2018) -- End
            AddHandler dgEmployee.CellContentClick, AddressOf dgEmployee_CellContentClick
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "chkSelectAll_CheckedChanged", mstrModuleName)
        End Try
    End Sub

#End Region

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Private Sub OtherSettings()
        Try
            Me.SuspendLayout()
            
            Call SetLanguage()

            Me.gbFilterCriteria.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbFilterCriteria.ForeColor = GUI._eZeeContainerHeaderForeColor

            Me.eZeeHeader.GradientColor1 = GUI._HeaderBackColor1
            Me.eZeeHeader.GradientColor2 = GUI._HeaderBackColor2
            Me.eZeeHeader.BorderColor = GUI._HeaderBorderColor
            Me.eZeeHeader.HeaderTextForeColor = GUI._HeaderTitleFontColor
            Me.eZeeHeader.DescriptionForeColor = GUI._HeaderMessageFontColor


            Me.btnDelete.GradientBackColor = GUI._ButttonBackColor
            Me.btnDelete.GradientForeColor = GUI._ButttonFontColor

            Me.btnNew.GradientBackColor = GUI._ButttonBackColor
            Me.btnNew.GradientForeColor = GUI._ButttonFontColor

            Me.btnClose.GradientBackColor = GUI._ButttonBackColor
            Me.btnClose.GradientForeColor = GUI._ButttonFontColor


            Me.ResumeLayout()
        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "OtherSettings", mstrModuleName)
        End Try
    End Sub


    Private Sub SetLanguage()
        Try
            Me.Text = Language._Object.getCaption(Me.Name, Me.Text)

            Me.eZeeHeader.Title = Language._Object.getCaption(Me.eZeeHeader.Name & "_Title", Me.eZeeHeader.Title)
            Me.eZeeHeader.Message = Language._Object.getCaption(Me.eZeeHeader.Name & "_Message", Me.eZeeHeader.Message)
            Me.gbFilterCriteria.Text = Language._Object.getCaption(Me.gbFilterCriteria.Name, Me.gbFilterCriteria.Text)
            Me.lblExpenses.Text = Language._Object.getCaption(Me.lblExpenses.Name, Me.lblExpenses.Text)
            Me.lblExpenseCat.Text = Language._Object.getCaption(Me.lblExpenseCat.Name, Me.lblExpenseCat.Text)
            Me.lblEmployee.Text = Language._Object.getCaption(Me.lblEmployee.Name, Me.lblEmployee.Text)
            Me.btnDelete.Text = Language._Object.getCaption(Me.btnDelete.Name, Me.btnDelete.Text)
            Me.btnNew.Text = Language._Object.getCaption(Me.btnNew.Name, Me.btnNew.Text)
            Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
            Me.chkSelectAll.Text = Language._Object.getCaption(Me.chkSelectAll.Name, Me.chkSelectAll.Text)
            Me.DataGridViewTextBoxColumn1.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn1.Name, Me.DataGridViewTextBoxColumn1.HeaderText)
            Me.DataGridViewTextBoxColumn2.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn2.Name, Me.DataGridViewTextBoxColumn2.HeaderText)
            Me.DataGridViewTextBoxColumn3.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn3.Name, Me.DataGridViewTextBoxColumn3.HeaderText)
            Me.DataGridViewTextBoxColumn4.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn4.Name, Me.DataGridViewTextBoxColumn4.HeaderText)
            Me.DataGridViewTextBoxColumn5.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn5.Name, Me.DataGridViewTextBoxColumn5.HeaderText)
            Me.DataGridViewTextBoxColumn6.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn6.Name, Me.DataGridViewTextBoxColumn6.HeaderText)
            Me.DataGridViewTextBoxColumn7.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn7.Name, Me.DataGridViewTextBoxColumn7.HeaderText)
            Me.DataGridViewTextBoxColumn8.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn8.Name, Me.DataGridViewTextBoxColumn8.HeaderText)
            Me.DataGridViewTextBoxColumn9.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn9.Name, Me.DataGridViewTextBoxColumn9.HeaderText)
            Me.dgcolhExpenses.HeaderText = Language._Object.getCaption(Me.dgcolhExpenses.Name, Me.dgcolhExpenses.HeaderText)
            Me.dgcolhExCategory.HeaderText = Language._Object.getCaption(Me.dgcolhExCategory.Name, Me.dgcolhExCategory.HeaderText)
            Me.dgcolhAccrue.HeaderText = Language._Object.getCaption(Me.dgcolhAccrue.Name, Me.dgcolhAccrue.HeaderText)
            Me.dgcolhSDate.HeaderText = Language._Object.getCaption(Me.dgcolhSDate.Name, Me.dgcolhSDate.HeaderText)
            Me.dgcolhEDate.HeaderText = Language._Object.getCaption(Me.dgcolhEDate.Name, Me.dgcolhEDate.HeaderText)

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
        End Try
    End Sub


    Private Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Sorry, Please tick atleast one expense assigned to employee.")
            Language.setMessage(mstrModuleName, 2, "Are you sure you want to delete checked transaction." & vbCrLf & _
                                                                        "Do you wish to continue?")
            Language.setMessage(mstrModuleName, 3, "Sorry, some of the ticked expenses are already issued, therefore cannot be deleted and is highlighted in red.")
            Language.setMessage(mstrModuleName, 4, "Sorry, Please select employee to fill expense list.")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>

    
End Class