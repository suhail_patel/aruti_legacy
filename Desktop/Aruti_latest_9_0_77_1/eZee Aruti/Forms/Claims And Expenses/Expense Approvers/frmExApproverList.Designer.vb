﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmExApproverList
    Inherits eZee.Common.eZeeForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmExApproverList))
        Me.eZeeHeader = New eZee.Common.eZeeHeader
        Me.gbFilterCriteria = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.lblLevel = New System.Windows.Forms.Label
        Me.lblStatus = New System.Windows.Forms.Label
        Me.cboExCategory = New System.Windows.Forms.ComboBox
        Me.cboStatus = New System.Windows.Forms.ComboBox
        Me.objbtnSearchLevel = New eZee.Common.eZeeGradientButton
        Me.lblExpenseCat = New System.Windows.Forms.Label
        Me.cboApprover = New System.Windows.Forms.ComboBox
        Me.cboLevel = New System.Windows.Forms.ComboBox
        Me.objbtnReset = New eZee.Common.eZeeSearchResetButton(Me.components)
        Me.objbtnSearchApprover = New eZee.Common.eZeeGradientButton
        Me.lblApprover = New System.Windows.Forms.Label
        Me.objbtnSearch = New eZee.Common.eZeeSearchResetButton(Me.components)
        Me.objFooter = New eZee.Common.eZeeFooter
        Me.btnOpearation = New eZee.Common.eZeeSplitButton
        Me.cmnuOperation = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.mnuInActiveApprover = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuActiveApprover = New System.Windows.Forms.ToolStripMenuItem
        Me.btnDelete = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnEdit = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnNew = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnClose = New eZee.Common.eZeeLightButton(Me.components)
        Me.lvApproverList = New eZee.Common.eZeeListView(Me.components)
        Me.colhApprover = New System.Windows.Forms.ColumnHeader
        Me.colhLevel = New System.Windows.Forms.ColumnHeader
        Me.colhDepartment = New System.Windows.Forms.ColumnHeader
        Me.colhJob = New System.Windows.Forms.ColumnHeader
        Me.colhMappedUser = New System.Windows.Forms.ColumnHeader
        Me.colhExternalApprover = New System.Windows.Forms.ColumnHeader
        Me.objcolhExCat = New System.Windows.Forms.ColumnHeader
        Me.gbFilterCriteria.SuspendLayout()
        CType(Me.objbtnReset, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.objbtnSearch, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.objFooter.SuspendLayout()
        Me.cmnuOperation.SuspendLayout()
        Me.SuspendLayout()
        '
        'eZeeHeader
        '
        Me.eZeeHeader.BackColor = System.Drawing.SystemColors.Control
        Me.eZeeHeader.BorderColor = System.Drawing.SystemColors.ControlDark
        Me.eZeeHeader.DescriptionForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.eZeeHeader.Dock = System.Windows.Forms.DockStyle.Top
        Me.eZeeHeader.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.eZeeHeader.GradiantStyle = eZee.Common.eZeeHeader.GradientStyle.Central
        Me.eZeeHeader.GradientColor1 = System.Drawing.SystemColors.Window
        Me.eZeeHeader.GradientColor2 = System.Drawing.SystemColors.Control
        Me.eZeeHeader.HeaderTextForeColor = System.Drawing.SystemColors.ControlText
        Me.eZeeHeader.Icon = Nothing
        Me.eZeeHeader.Location = New System.Drawing.Point(0, 0)
        Me.eZeeHeader.Message = ""
        Me.eZeeHeader.Name = "eZeeHeader"
        Me.eZeeHeader.Size = New System.Drawing.Size(718, 58)
        Me.eZeeHeader.TabIndex = 10
        Me.eZeeHeader.Title = "Expense Approvers List"
        '
        'gbFilterCriteria
        '
        Me.gbFilterCriteria.BorderColor = System.Drawing.Color.Black
        Me.gbFilterCriteria.Checked = False
        Me.gbFilterCriteria.CollapseAllExceptThis = False
        Me.gbFilterCriteria.CollapsedHoverImage = Nothing
        Me.gbFilterCriteria.CollapsedNormalImage = Nothing
        Me.gbFilterCriteria.CollapsedPressedImage = Nothing
        Me.gbFilterCriteria.CollapseOnLoad = False
        Me.gbFilterCriteria.Controls.Add(Me.lblLevel)
        Me.gbFilterCriteria.Controls.Add(Me.lblStatus)
        Me.gbFilterCriteria.Controls.Add(Me.cboExCategory)
        Me.gbFilterCriteria.Controls.Add(Me.cboStatus)
        Me.gbFilterCriteria.Controls.Add(Me.objbtnSearchLevel)
        Me.gbFilterCriteria.Controls.Add(Me.lblExpenseCat)
        Me.gbFilterCriteria.Controls.Add(Me.cboApprover)
        Me.gbFilterCriteria.Controls.Add(Me.cboLevel)
        Me.gbFilterCriteria.Controls.Add(Me.objbtnReset)
        Me.gbFilterCriteria.Controls.Add(Me.objbtnSearchApprover)
        Me.gbFilterCriteria.Controls.Add(Me.lblApprover)
        Me.gbFilterCriteria.Controls.Add(Me.objbtnSearch)
        Me.gbFilterCriteria.ExpandedHoverImage = Nothing
        Me.gbFilterCriteria.ExpandedNormalImage = Nothing
        Me.gbFilterCriteria.ExpandedPressedImage = Nothing
        Me.gbFilterCriteria.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbFilterCriteria.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbFilterCriteria.HeaderHeight = 25
        Me.gbFilterCriteria.HeaderMessage = ""
        Me.gbFilterCriteria.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbFilterCriteria.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbFilterCriteria.HeightOnCollapse = 0
        Me.gbFilterCriteria.LeftTextSpace = 0
        Me.gbFilterCriteria.Location = New System.Drawing.Point(12, 64)
        Me.gbFilterCriteria.Name = "gbFilterCriteria"
        Me.gbFilterCriteria.OpenHeight = 91
        Me.gbFilterCriteria.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbFilterCriteria.ShowBorder = True
        Me.gbFilterCriteria.ShowCheckBox = False
        Me.gbFilterCriteria.ShowCollapseButton = False
        Me.gbFilterCriteria.ShowDefaultBorderColor = True
        Me.gbFilterCriteria.ShowDownButton = False
        Me.gbFilterCriteria.ShowHeader = True
        Me.gbFilterCriteria.Size = New System.Drawing.Size(694, 89)
        Me.gbFilterCriteria.TabIndex = 16
        Me.gbFilterCriteria.Temp = 0
        Me.gbFilterCriteria.Text = "Filter Criteria"
        Me.gbFilterCriteria.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblLevel
        '
        Me.lblLevel.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblLevel.Location = New System.Drawing.Point(10, 63)
        Me.lblLevel.Name = "lblLevel"
        Me.lblLevel.Size = New System.Drawing.Size(101, 16)
        Me.lblLevel.TabIndex = 238
        Me.lblLevel.Text = "Level"
        '
        'lblStatus
        '
        Me.lblStatus.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblStatus.Location = New System.Drawing.Point(350, 36)
        Me.lblStatus.Name = "lblStatus"
        Me.lblStatus.Size = New System.Drawing.Size(72, 16)
        Me.lblStatus.TabIndex = 315
        Me.lblStatus.Text = "Status"
        '
        'cboExCategory
        '
        Me.cboExCategory.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboExCategory.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboExCategory.FormattingEnabled = True
        Me.cboExCategory.Location = New System.Drawing.Point(129, 33)
        Me.cboExCategory.Name = "cboExCategory"
        Me.cboExCategory.Size = New System.Drawing.Size(188, 21)
        Me.cboExCategory.TabIndex = 310
        '
        'cboStatus
        '
        Me.cboStatus.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboStatus.DropDownWidth = 250
        Me.cboStatus.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboStatus.FormattingEnabled = True
        Me.cboStatus.Location = New System.Drawing.Point(428, 33)
        Me.cboStatus.Name = "cboStatus"
        Me.cboStatus.Size = New System.Drawing.Size(188, 21)
        Me.cboStatus.TabIndex = 314
        '
        'objbtnSearchLevel
        '
        Me.objbtnSearchLevel.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchLevel.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchLevel.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchLevel.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchLevel.BorderSelected = False
        Me.objbtnSearchLevel.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchLevel.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objbtnSearchLevel.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchLevel.Location = New System.Drawing.Point(323, 60)
        Me.objbtnSearchLevel.Name = "objbtnSearchLevel"
        Me.objbtnSearchLevel.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchLevel.TabIndex = 236
        '
        'lblExpenseCat
        '
        Me.lblExpenseCat.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblExpenseCat.Location = New System.Drawing.Point(10, 35)
        Me.lblExpenseCat.Name = "lblExpenseCat"
        Me.lblExpenseCat.Size = New System.Drawing.Size(101, 16)
        Me.lblExpenseCat.TabIndex = 311
        Me.lblExpenseCat.Text = "Expense Category"
        '
        'cboApprover
        '
        Me.cboApprover.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboApprover.DropDownWidth = 250
        Me.cboApprover.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboApprover.FormattingEnabled = True
        Me.cboApprover.Location = New System.Drawing.Point(428, 60)
        Me.cboApprover.Name = "cboApprover"
        Me.cboApprover.Size = New System.Drawing.Size(188, 21)
        Me.cboApprover.TabIndex = 87
        '
        'cboLevel
        '
        Me.cboLevel.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboLevel.DropDownWidth = 250
        Me.cboLevel.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboLevel.FormattingEnabled = True
        Me.cboLevel.Location = New System.Drawing.Point(129, 60)
        Me.cboLevel.Name = "cboLevel"
        Me.cboLevel.Size = New System.Drawing.Size(188, 21)
        Me.cboLevel.TabIndex = 237
        '
        'objbtnReset
        '
        Me.objbtnReset.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.objbtnReset.BackColor = System.Drawing.Color.Transparent
        Me.objbtnReset.ButtonType = eZee.Common.eZeeSearchResetButton.EnumButtonType.Reset
        Me.objbtnReset.Image = CType(resources.GetObject("objbtnReset.Image"), System.Drawing.Image)
        Me.objbtnReset.Location = New System.Drawing.Point(668, 0)
        Me.objbtnReset.Name = "objbtnReset"
        Me.objbtnReset.ResultMessage = ""
        Me.objbtnReset.SearchMessage = ""
        Me.objbtnReset.Size = New System.Drawing.Size(24, 24)
        Me.objbtnReset.TabIndex = 67
        Me.objbtnReset.TabStop = False
        '
        'objbtnSearchApprover
        '
        Me.objbtnSearchApprover.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchApprover.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchApprover.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchApprover.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchApprover.BorderSelected = False
        Me.objbtnSearchApprover.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchApprover.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objbtnSearchApprover.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchApprover.Location = New System.Drawing.Point(622, 60)
        Me.objbtnSearchApprover.Name = "objbtnSearchApprover"
        Me.objbtnSearchApprover.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchApprover.TabIndex = 86
        '
        'lblApprover
        '
        Me.lblApprover.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblApprover.Location = New System.Drawing.Point(350, 63)
        Me.lblApprover.Name = "lblApprover"
        Me.lblApprover.Size = New System.Drawing.Size(72, 16)
        Me.lblApprover.TabIndex = 234
        Me.lblApprover.Text = "Approver"
        '
        'objbtnSearch
        '
        Me.objbtnSearch.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.objbtnSearch.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearch.ButtonType = eZee.Common.eZeeSearchResetButton.EnumButtonType.Search
        Me.objbtnSearch.Image = CType(resources.GetObject("objbtnSearch.Image"), System.Drawing.Image)
        Me.objbtnSearch.Location = New System.Drawing.Point(644, 0)
        Me.objbtnSearch.Name = "objbtnSearch"
        Me.objbtnSearch.ResultMessage = ""
        Me.objbtnSearch.SearchMessage = ""
        Me.objbtnSearch.Size = New System.Drawing.Size(24, 24)
        Me.objbtnSearch.TabIndex = 66
        Me.objbtnSearch.TabStop = False
        '
        'objFooter
        '
        Me.objFooter.BorderColor = System.Drawing.Color.Silver
        Me.objFooter.Controls.Add(Me.btnOpearation)
        Me.objFooter.Controls.Add(Me.btnDelete)
        Me.objFooter.Controls.Add(Me.btnEdit)
        Me.objFooter.Controls.Add(Me.btnNew)
        Me.objFooter.Controls.Add(Me.btnClose)
        Me.objFooter.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.objFooter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objFooter.GradiantStyle = eZee.Common.eZeeFooter.GradientStyle.Up
        Me.objFooter.GradientColor1 = System.Drawing.SystemColors.Control
        Me.objFooter.GradientColor2 = System.Drawing.SystemColors.Control
        Me.objFooter.Location = New System.Drawing.Point(0, 421)
        Me.objFooter.Name = "objFooter"
        Me.objFooter.Size = New System.Drawing.Size(718, 55)
        Me.objFooter.TabIndex = 17
        '
        'btnOpearation
        '
        Me.btnOpearation.BorderColor = System.Drawing.Color.Black
        Me.btnOpearation.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnOpearation.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnOpearation.GradientForeColor = System.Drawing.SystemColors.WindowText
        Me.btnOpearation.Location = New System.Drawing.Point(12, 13)
        Me.btnOpearation.Name = "btnOpearation"
        Me.btnOpearation.ShowDefaultBorderColor = True
        Me.btnOpearation.Size = New System.Drawing.Size(100, 30)
        Me.btnOpearation.SplitButtonMenu = Me.cmnuOperation
        Me.btnOpearation.TabIndex = 316
        Me.btnOpearation.Text = "Operation"
        '
        'cmnuOperation
        '
        Me.cmnuOperation.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mnuInActiveApprover, Me.mnuActiveApprover})
        Me.cmnuOperation.Name = "cmnuOperation"
        Me.cmnuOperation.Size = New System.Drawing.Size(190, 48)
        '
        'mnuInActiveApprover
        '
        Me.mnuInActiveApprover.Name = "mnuInActiveApprover"
        Me.mnuInActiveApprover.Size = New System.Drawing.Size(189, 22)
        Me.mnuInActiveApprover.Text = "Make Approver Inactive"
        '
        'mnuActiveApprover
        '
        Me.mnuActiveApprover.Name = "mnuActiveApprover"
        Me.mnuActiveApprover.Size = New System.Drawing.Size(189, 22)
        Me.mnuActiveApprover.Text = "Make Approver Active"
        '
        'btnDelete
        '
        Me.btnDelete.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnDelete.BackColor = System.Drawing.Color.White
        Me.btnDelete.BackgroundImage = CType(resources.GetObject("btnDelete.BackgroundImage"), System.Drawing.Image)
        Me.btnDelete.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnDelete.BorderColor = System.Drawing.Color.Empty
        Me.btnDelete.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnDelete.FlatAppearance.BorderSize = 0
        Me.btnDelete.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnDelete.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnDelete.ForeColor = System.Drawing.Color.Black
        Me.btnDelete.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnDelete.GradientForeColor = System.Drawing.Color.Black
        Me.btnDelete.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnDelete.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnDelete.Location = New System.Drawing.Point(506, 13)
        Me.btnDelete.Name = "btnDelete"
        Me.btnDelete.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnDelete.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnDelete.Size = New System.Drawing.Size(97, 30)
        Me.btnDelete.TabIndex = 122
        Me.btnDelete.Text = "&Delete"
        Me.btnDelete.UseVisualStyleBackColor = True
        '
        'btnEdit
        '
        Me.btnEdit.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnEdit.BackColor = System.Drawing.Color.White
        Me.btnEdit.BackgroundImage = CType(resources.GetObject("btnEdit.BackgroundImage"), System.Drawing.Image)
        Me.btnEdit.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnEdit.BorderColor = System.Drawing.Color.Empty
        Me.btnEdit.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnEdit.FlatAppearance.BorderSize = 0
        Me.btnEdit.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnEdit.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnEdit.ForeColor = System.Drawing.Color.Black
        Me.btnEdit.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnEdit.GradientForeColor = System.Drawing.Color.Black
        Me.btnEdit.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnEdit.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnEdit.Location = New System.Drawing.Point(403, 13)
        Me.btnEdit.Name = "btnEdit"
        Me.btnEdit.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnEdit.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnEdit.Size = New System.Drawing.Size(97, 30)
        Me.btnEdit.TabIndex = 121
        Me.btnEdit.Text = "&Edit"
        Me.btnEdit.UseVisualStyleBackColor = True
        '
        'btnNew
        '
        Me.btnNew.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnNew.BackColor = System.Drawing.Color.White
        Me.btnNew.BackgroundImage = CType(resources.GetObject("btnNew.BackgroundImage"), System.Drawing.Image)
        Me.btnNew.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnNew.BorderColor = System.Drawing.Color.Empty
        Me.btnNew.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnNew.FlatAppearance.BorderSize = 0
        Me.btnNew.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnNew.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnNew.ForeColor = System.Drawing.Color.Black
        Me.btnNew.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnNew.GradientForeColor = System.Drawing.Color.Black
        Me.btnNew.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnNew.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnNew.Location = New System.Drawing.Point(300, 13)
        Me.btnNew.Name = "btnNew"
        Me.btnNew.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnNew.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnNew.Size = New System.Drawing.Size(97, 30)
        Me.btnNew.TabIndex = 120
        Me.btnNew.Text = "&New"
        Me.btnNew.UseVisualStyleBackColor = True
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnClose.BackColor = System.Drawing.Color.White
        Me.btnClose.BackgroundImage = CType(resources.GetObject("btnClose.BackgroundImage"), System.Drawing.Image)
        Me.btnClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnClose.BorderColor = System.Drawing.Color.Empty
        Me.btnClose.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnClose.FlatAppearance.BorderSize = 0
        Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClose.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.ForeColor = System.Drawing.Color.Black
        Me.btnClose.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnClose.GradientForeColor = System.Drawing.Color.Black
        Me.btnClose.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Location = New System.Drawing.Point(609, 13)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Size = New System.Drawing.Size(97, 30)
        Me.btnClose.TabIndex = 119
        Me.btnClose.Text = "&Close"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'lvApproverList
        '
        Me.lvApproverList.BackColorOnChecked = True
        Me.lvApproverList.ColumnHeaders = Nothing
        Me.lvApproverList.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.colhApprover, Me.colhLevel, Me.colhDepartment, Me.colhJob, Me.colhMappedUser, Me.colhExternalApprover, Me.objcolhExCat})
        Me.lvApproverList.CompulsoryColumns = ""
        Me.lvApproverList.FullRowSelect = True
        Me.lvApproverList.GridLines = True
        Me.lvApproverList.GroupingColumn = Nothing
        Me.lvApproverList.HideSelection = False
        Me.lvApproverList.Location = New System.Drawing.Point(12, 159)
        Me.lvApproverList.MinColumnWidth = 50
        Me.lvApproverList.MultiSelect = False
        Me.lvApproverList.Name = "lvApproverList"
        Me.lvApproverList.OptionalColumns = ""
        Me.lvApproverList.ShowMoreItem = False
        Me.lvApproverList.ShowSaveItem = False
        Me.lvApproverList.ShowSelectAll = True
        Me.lvApproverList.ShowSizeAllColumnsToFit = True
        Me.lvApproverList.Size = New System.Drawing.Size(694, 249)
        Me.lvApproverList.Sortable = True
        Me.lvApproverList.TabIndex = 18
        Me.lvApproverList.UseCompatibleStateImageBehavior = False
        Me.lvApproverList.View = System.Windows.Forms.View.Details
        '
        'colhApprover
        '
        Me.colhApprover.Tag = "colhApprover"
        Me.colhApprover.Text = "Expense Approver"
        Me.colhApprover.Width = 190
        '
        'colhLevel
        '
        Me.colhLevel.Tag = "colhLevel"
        Me.colhLevel.Text = "Level"
        Me.colhLevel.Width = 110
        '
        'colhDepartment
        '
        Me.colhDepartment.Tag = "colhDepartment"
        Me.colhDepartment.Text = "Department"
        Me.colhDepartment.Width = 120
        '
        'colhJob
        '
        Me.colhJob.Tag = "colhJob"
        Me.colhJob.Text = "Job"
        Me.colhJob.Width = 150
        '
        'colhMappedUser
        '
        Me.colhMappedUser.Tag = "colhMappedUser"
        Me.colhMappedUser.Text = "Mapped User"
        Me.colhMappedUser.Width = 120
        '
        'colhExternalApprover
        '
        Me.colhExternalApprover.DisplayIndex = 6
        Me.colhExternalApprover.Tag = "colhExternalApprover"
        Me.colhExternalApprover.Text = "External Approver"
        Me.colhExternalApprover.Width = 125
        '
        'objcolhExCat
        '
        Me.objcolhExCat.DisplayIndex = 5
        Me.objcolhExCat.Tag = "objcolhExCat"
        Me.objcolhExCat.Text = ""
        Me.objcolhExCat.Width = 0
        '
        'frmExApproverList
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(718, 476)
        Me.Controls.Add(Me.lvApproverList)
        Me.Controls.Add(Me.objFooter)
        Me.Controls.Add(Me.gbFilterCriteria)
        Me.Controls.Add(Me.eZeeHeader)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmExApproverList"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Expense Approvers List"
        Me.gbFilterCriteria.ResumeLayout(False)
        CType(Me.objbtnReset, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.objbtnSearch, System.ComponentModel.ISupportInitialize).EndInit()
        Me.objFooter.ResumeLayout(False)
        Me.cmnuOperation.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents eZeeHeader As eZee.Common.eZeeHeader
    Friend WithEvents gbFilterCriteria As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents lblLevel As System.Windows.Forms.Label
    Friend WithEvents objbtnSearchLevel As eZee.Common.eZeeGradientButton
    Friend WithEvents cboLevel As System.Windows.Forms.ComboBox
    Friend WithEvents objbtnReset As eZee.Common.eZeeSearchResetButton
    Friend WithEvents objbtnSearch As eZee.Common.eZeeSearchResetButton
    Friend WithEvents lblApprover As System.Windows.Forms.Label
    Friend WithEvents objbtnSearchApprover As eZee.Common.eZeeGradientButton
    Friend WithEvents cboApprover As System.Windows.Forms.ComboBox
    Friend WithEvents cboExCategory As System.Windows.Forms.ComboBox
    Friend WithEvents lblExpenseCat As System.Windows.Forms.Label
    Friend WithEvents objFooter As eZee.Common.eZeeFooter
    Friend WithEvents btnDelete As eZee.Common.eZeeLightButton
    Friend WithEvents btnEdit As eZee.Common.eZeeLightButton
    Friend WithEvents btnNew As eZee.Common.eZeeLightButton
    Friend WithEvents btnClose As eZee.Common.eZeeLightButton
    Friend WithEvents lvApproverList As eZee.Common.eZeeListView
    Friend WithEvents colhApprover As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhLevel As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhDepartment As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhJob As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhMappedUser As System.Windows.Forms.ColumnHeader
    Friend WithEvents objcolhExCat As System.Windows.Forms.ColumnHeader
    Friend WithEvents lblStatus As System.Windows.Forms.Label
    Friend WithEvents cboStatus As System.Windows.Forms.ComboBox
    Friend WithEvents btnOpearation As eZee.Common.eZeeSplitButton
    Friend WithEvents cmnuOperation As System.Windows.Forms.ContextMenuStrip
    Friend WithEvents mnuInActiveApprover As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuActiveApprover As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents colhExternalApprover As System.Windows.Forms.ColumnHeader
End Class
