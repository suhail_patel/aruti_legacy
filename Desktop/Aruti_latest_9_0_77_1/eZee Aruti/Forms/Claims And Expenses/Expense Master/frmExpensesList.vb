﻿Option Strict On

#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data

#End Region

Public Class frmExpensesList

#Region " Private Varaibles "

    Private objExpense As clsExpense_Master
    Private ReadOnly mstrModuleName As String = "frmExpensesList"

#End Region

#Region " Private Function "


    'Pinkal (13-Jul-2015) -- Start
    'Enhancement - WORKING ON C & R ACCESS PRIVILEGE.

    Private Sub SetVisibility()
        Try
            btnNew.Enabled = User._Object.Privilege._AllowtoAddClaimExpenses
            btnEdit.Enabled = User._Object.Privilege._AllowtoEditClaimExpenses
            btnDelete.Enabled = User._Object.Privilege._AllowtoDeleteClaimExpenses
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetVisibility", mstrModuleName)
        Finally
        End Try
    End Sub

    'Pinkal (13-Jul-2015) -- End

    Private Sub FillCombo()
        Dim dsList As New DataSet
        Try

            'Pinkal (11-Sep-2019) -- Start
            'Enhancement NMB - Working On Claim Retirement for NMB.
            'dsList = clsExpCommonMethods.Get_ExpenseTypes(True, True, True, "List", True)
            dsList = clsExpCommonMethods.Get_ExpenseTypes(True, True, True, "List", True, True)
            'Pinkal (11-Sep-2019) -- End

            With cboExCategory
                .ValueMember = "Id"
                .DisplayMember = "Name"
                .DataSource = dsList.Tables("List")
                .SelectedValue = 0
            End With

            dsList = clsExpCommonMethods.Get_UoM(True, "List")
            With cboUoM
                .ValueMember = "Id"
                .DisplayMember = "Name"
                .DataSource = dsList.Tables("List")
                .SelectedValue = 0
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub Fill_List()
        Dim dsList As New DataSet
        Dim dtTable As DataTable
        Dim strSearch As String = ""
        Try


            'Pinkal (13-Jul-2015) -- Start
            'Enhancement - WORKING ON C & R ACCESS PRIVILEGE.
            If User._Object.Privilege._AllowtoViewClaimExpenseList = False Then Exit Sub
            'Pinkal (13-Jul-2015) -- End1


            dsList = objExpense.GetList("List")

            If CInt(cboExCategory.SelectedValue) > 0 Then
                strSearch &= "AND expensetypeid = '" & CInt(cboExCategory.SelectedValue) & "' "
            End If

            If CInt(cboUoM.SelectedValue) > 0 Then
                strSearch &= "AND uomunkid = '" & CInt(cboUoM.SelectedValue) & "' "
            End If

            If strSearch.Length > 0 Then
                strSearch = strSearch.Substring(3)
                dtTable = New DataView(dsList.Tables("List"), strSearch, "", DataViewRowState.CurrentRows).ToTable
            Else
                dtTable = dsList.Tables("List")
            End If

            lvExpenseList.Items.Clear()

            For Each dRow As DataRow In dtTable.Rows
                Dim lvItem As New ListViewItem

                lvItem.Text = dRow("code").ToString
                lvItem.SubItems.Add(dRow("name").ToString)
                lvItem.SubItems.Add(dRow("uom").ToString)
                lvItem.SubItems.Add(dRow("description").ToString)

                'Pinkal (20-Nov-2018) -- Start
                'Enhancement - Working on P2P Integration for NMB.
                lvItem.SubItems.Add(dRow("glcode").ToString)
                lvItem.SubItems.Add(dRow("glcodename").ToString)
                'Pinkal (20-Nov-2018) -- End

                lvItem.SubItems.Add(dRow("expensetype").ToString)
                lvItem.Tag = dRow("expenseunkid")
                lvExpenseList.Items.Add(lvItem)
            Next

            lvExpenseList.GroupingColumn = objcolhExCategory
            lvExpenseList.DisplayGroups(True)


            'Pinkal (20-Nov-2018) -- Start
            'Enhancement - Working on P2P Integration for NMB.
            If lvExpenseList.Items.Count > 4 Then
                colhGLName.Width = 200 - 20
            Else
                colhGLName.Width = 200
            End If
            'Pinkal (20-Nov-2018) -- End

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Fill_List", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

#Region " Form's Events "

    Private Sub frmExpensesList_KeyUp(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyUp
        Try
            If e.KeyCode = Keys.Delete And lvExpenseList.Focused = True Then
                Call btnDelete.PerformClick()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmExpensesList_KeyUp", mstrModuleName)
        End Try

    End Sub

    Private Sub frmExpensesList_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles Me.KeyPress
        Try
            If Asc(e.KeyChar) = 27 Then
                Me.Close()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmExpensesList_KeyPress", mstrModuleName)
        End Try
    End Sub

    Private Sub frmExpensesList_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        objExpense = New clsExpense_Master
        Try
            Call Set_Logo(Me, gApplicationType)
            Language.setLanguage(Me.Name)
            Call OtherSettings()

            Call SetVisibility()

            Call FillCombo()

            If lvExpenseList.Items.Count > 0 Then lvExpenseList.Items(0).Selected = True
            lvExpenseList.Select()

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmExpensesList_Load", mstrModuleName)
        End Try
    End Sub

    Private Sub frmExpensesList_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        objExpense = Nothing
    End Sub

    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsExpense_Master.SetMessages()
            clsExpCommonMethods.SetMessages()
            objfrm._Other_ModuleNames = "clsExpense_Master,clsExpCommonMethods"
            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub


#End Region

#Region " Buttons "

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Me.Close()
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "btnClose_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnNew_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNew.Click
        Dim frm As New frmExpensesAddEdit
        Try
            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If

            If frm.displayDialog(-1, enAction.ADD_CONTINUE) Then
                Call Fill_List()
            End If
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "btnNew_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub btnEdit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEdit.Click
        Dim mblnEditExpenseCategory As Boolean = True
        Dim mblnEditExpenseType As Boolean = True
        If lvExpenseList.SelectedItems.Count < 1 Then
            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please select Expense from the list to perform further operation."), enMsgBoxStyle.Information) '?1
            lvExpenseList.Select()
            Exit Sub
        End If


        'Pinkal (22-Jun-2015) -- Start
        'Enhancement - CHANGES IN CLAIM & REQUEST AS PER MR.ANDREW REQUIREMENT.

        'If objExpense.isUsed(CInt(lvExpenseList.SelectedItems(0).Tag), "cmclaim_request_tran") Then
        '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Sorry, you cannot Edit this Expense. Reason : This Expense is already linked with some transaction."), enMsgBoxStyle.Information)
        '    Exit Sub
        'Else

        'Pinkal (22-Jun-2015) -- End

        If objExpense.isUsed(CInt(lvExpenseList.SelectedItems(0).Tag), "cmexpbalance_tran") Then
            mblnEditExpenseType = False
            mblnEditExpenseCategory = False
        ElseIf objExpense.isUsed(CInt(lvExpenseList.SelectedItems(0).Tag)) Then
            mblnEditExpenseType = True
            mblnEditExpenseCategory = True
        End If
        Dim frm As New frmExpensesAddEdit
        Try
            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If

            If frm.displayDialog(CInt(lvExpenseList.SelectedItems(0).Tag), enAction.EDIT_ONE, mblnEditExpenseCategory, mblnEditExpenseType) Then
                Call Fill_List()
            End If
            frm = Nothing
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "btnEdit_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub btnDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDelete.Click
        If lvExpenseList.SelectedItems.Count < 1 Then
            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please select Expense from the list to perform further operation."), enMsgBoxStyle.Information) '?1
            lvExpenseList.Select()
            Exit Sub
        End If
        Try
            If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Are you sure you want to delete this Expense?"), CType(enMsgBoxStyle.Question + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.Yes Then

                'S.SANDEEP [28-May-2018] -- START
                'ISSUE/ENHANCEMENT : {Audit Trails} 
                objExpense._FormName = mstrModuleName
                objExpense._LoginEmployeeunkid = 0
                objExpense._ClientIP = getIP()
                objExpense._HostName = getHostName()
                objExpense._FromWeb = False
                objExpense._AuditUserId = User._Object._Userunkid
objExpense._CompanyUnkid = Company._Object._Companyunkid
                objExpense._AuditDate = ConfigParameter._Object._CurrentDateAndTime
                'S.SANDEEP [28-May-2018] -- END

                objExpense.Delete(CInt(lvExpenseList.SelectedItems(0).Tag))
                If objExpense._Message <> "" Then
                    eZeeMsgBox.Show(objExpense._Message, enMsgBoxStyle.Information)
                Else
                    lvExpenseList.SelectedItems(0).Remove()
                End If
            End If
            lvExpenseList.Select()
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "btnDelete_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnSearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearch.Click
        Try
            Call Fill_List()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearch_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnReset.Click
        Try
            cboExCategory.SelectedValue = 0
            cboUoM.SelectedValue = 0
            Call Fill_List()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnReset_Click", mstrModuleName)
        End Try
    End Sub


#End Region

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Private Sub OtherSettings()
        Try
            Me.SuspendLayout()
            
            Call SetLanguage()

            Me.gbFilterCriteria.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbFilterCriteria.ForeColor = GUI._eZeeContainerHeaderForeColor

            Me.btnDelete.GradientBackColor = GUI._ButttonBackColor
            Me.btnDelete.GradientForeColor = GUI._ButttonFontColor

            Me.btnEdit.GradientBackColor = GUI._ButttonBackColor
            Me.btnEdit.GradientForeColor = GUI._ButttonFontColor

            Me.btnNew.GradientBackColor = GUI._ButttonBackColor
            Me.btnNew.GradientForeColor = GUI._ButttonFontColor

            Me.btnClose.GradientBackColor = GUI._ButttonBackColor
            Me.btnClose.GradientForeColor = GUI._ButttonFontColor


            Me.ResumeLayout()
        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "OtherSettings", mstrModuleName)
        End Try
    End Sub


    Private Sub SetLanguage()
        Try
            Me.Text = Language._Object.getCaption(Me.Name, Me.Text)

            Me.gbFilterCriteria.Text = Language._Object.getCaption(Me.gbFilterCriteria.Name, Me.gbFilterCriteria.Text)
            Me.lblExpenseCat.Text = Language._Object.getCaption(Me.lblExpenseCat.Name, Me.lblExpenseCat.Text)
            Me.btnDelete.Text = Language._Object.getCaption(Me.btnDelete.Name, Me.btnDelete.Text)
            Me.btnEdit.Text = Language._Object.getCaption(Me.btnEdit.Name, Me.btnEdit.Text)
            Me.btnNew.Text = Language._Object.getCaption(Me.btnNew.Name, Me.btnNew.Text)
            Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
            Me.colhCode.Text = Language._Object.getCaption(CStr(Me.colhCode.Tag), Me.colhCode.Text)
            Me.colhName.Text = Language._Object.getCaption(CStr(Me.colhName.Tag), Me.colhName.Text)
            Me.colhDescription.Text = Language._Object.getCaption(CStr(Me.colhDescription.Tag), Me.colhDescription.Text)
            Me.lblUoM.Text = Language._Object.getCaption(Me.lblUoM.Name, Me.lblUoM.Text)
            Me.colhUOM.Text = Language._Object.getCaption(CStr(Me.colhUOM.Tag), Me.colhUOM.Text)

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
        End Try
    End Sub


    Private Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Please select Expense from the list to perform further operation.")
            Language.setMessage(mstrModuleName, 2, "Are you sure you want to delete this Expense?")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class