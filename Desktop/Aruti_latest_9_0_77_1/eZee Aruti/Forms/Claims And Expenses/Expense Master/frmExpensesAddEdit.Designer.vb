﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmExpensesAddEdit
    Inherits eZee.Common.eZeeForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmExpensesAddEdit))
        Me.objFooter = New eZee.Common.eZeeFooter
        Me.btnSave = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnClose = New eZee.Common.eZeeLightButton(Me.components)
        Me.gbExpenseInformation = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.cboDefaultCostCenter = New System.Windows.Forms.ComboBox
        Me.lblDefaultCostCenter = New System.Windows.Forms.Label
        Me.objbtnAddGLCode = New eZee.Common.eZeeGradientButton
        Me.EZeeStraightLine1 = New eZee.Common.eZeeStraightLine
        Me.pnlBalanceSetting = New System.Windows.Forms.Panel
        Me.rdIssueQtyBalAsOnDate = New System.Windows.Forms.RadioButton
        Me.rdIssueQtyTotalBal = New System.Windows.Forms.RadioButton
        Me.grpExpenseSettings = New System.Windows.Forms.GroupBox
        Me.nudMaxExpenseQty = New System.Windows.Forms.NumericUpDown
        Me.LblMaximumExpenseQty = New System.Windows.Forms.Label
        Me.chkIsImprest = New System.Windows.Forms.CheckBox
        Me.chkMakeUnitPriceEditable = New System.Windows.Forms.CheckBox
        Me.chkShowOnESS = New System.Windows.Forms.CheckBox
        Me.gbP2PExpenseSetting = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.Label1 = New System.Windows.Forms.Label
        Me.rdCapex = New System.Windows.Forms.RadioButton
        Me.chkHRExpense = New System.Windows.Forms.CheckBox
        Me.chkBudgetMandatory = New System.Windows.Forms.CheckBox
        Me.rdOpex = New System.Windows.Forms.RadioButton
        Me.chkConsiderDependants = New System.Windows.Forms.CheckBox
        Me.chkIsAccrued = New System.Windows.Forms.CheckBox
        Me.chkConsiderPayroll = New System.Windows.Forms.CheckBox
        Me.chkLeaveEncashment = New System.Windows.Forms.CheckBox
        Me.ChkAttachDocumentMandatory = New System.Windows.Forms.CheckBox
        Me.chkSecRouteMandatory = New System.Windows.Forms.CheckBox
        Me.chkDoNotShowExpInCR = New System.Windows.Forms.CheckBox
        Me.LblBalanceSettings = New System.Windows.Forms.Label
        Me.cboHeadType = New System.Windows.Forms.ComboBox
        Me.objbtnSearchGLCode = New eZee.Common.eZeeGradientButton
        Me.lblTransactionHead = New System.Windows.Forms.Label
        Me.cboGLCode = New System.Windows.Forms.ComboBox
        Me.LblGLCode = New System.Windows.Forms.Label
        Me.objbtnSearchLeaveType = New eZee.Common.eZeeGradientButton
        Me.cboExCategory = New System.Windows.Forms.ComboBox
        Me.LblLeaveType = New System.Windows.Forms.Label
        Me.cboLeaveType = New System.Windows.Forms.ComboBox
        Me.lblUoM = New System.Windows.Forms.Label
        Me.cboUoM = New System.Windows.Forms.ComboBox
        Me.lblDescription = New System.Windows.Forms.Label
        Me.txtDescription = New eZee.TextBox.AlphanumericTextBox
        Me.lblExpenseCat = New System.Windows.Forms.Label
        Me.txtCode = New eZee.TextBox.AlphanumericTextBox
        Me.objbtnOtherLanguage = New eZee.Common.eZeeGradientButton
        Me.txtName = New eZee.TextBox.AlphanumericTextBox
        Me.lblCode = New System.Windows.Forms.Label
        Me.lblName = New System.Windows.Forms.Label
        Me.objSearchImprestName = New eZee.Common.eZeeGradientButton
        Me.objSearchImprestCode = New eZee.Common.eZeeGradientButton
        Me.cboImprestName = New System.Windows.Forms.ComboBox
        Me.cboImprestCode = New System.Windows.Forms.ComboBox
        Me.chkNotAllowBackDate = New System.Windows.Forms.CheckBox
        Me.objFooter.SuspendLayout()
        Me.gbExpenseInformation.SuspendLayout()
        Me.pnlBalanceSetting.SuspendLayout()
        Me.grpExpenseSettings.SuspendLayout()
        CType(Me.nudMaxExpenseQty, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.gbP2PExpenseSetting.SuspendLayout()
        Me.SuspendLayout()
        '
        'objFooter
        '
        Me.objFooter.BorderColor = System.Drawing.Color.Silver
        Me.objFooter.Controls.Add(Me.btnSave)
        Me.objFooter.Controls.Add(Me.btnClose)
        Me.objFooter.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.objFooter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objFooter.GradiantStyle = eZee.Common.eZeeFooter.GradientStyle.Up
        Me.objFooter.GradientColor1 = System.Drawing.SystemColors.Control
        Me.objFooter.GradientColor2 = System.Drawing.SystemColors.Control
        Me.objFooter.Location = New System.Drawing.Point(0, 416)
        Me.objFooter.Name = "objFooter"
        Me.objFooter.Size = New System.Drawing.Size(706, 55)
        Me.objFooter.TabIndex = 1
        '
        'btnSave
        '
        Me.btnSave.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnSave.BackColor = System.Drawing.Color.White
        Me.btnSave.BackgroundImage = CType(resources.GetObject("btnSave.BackgroundImage"), System.Drawing.Image)
        Me.btnSave.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnSave.BorderColor = System.Drawing.Color.Empty
        Me.btnSave.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnSave.FlatAppearance.BorderSize = 0
        Me.btnSave.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnSave.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSave.ForeColor = System.Drawing.Color.Black
        Me.btnSave.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnSave.GradientForeColor = System.Drawing.Color.Black
        Me.btnSave.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSave.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnSave.Location = New System.Drawing.Point(494, 13)
        Me.btnSave.Name = "btnSave"
        Me.btnSave.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSave.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnSave.Size = New System.Drawing.Size(97, 30)
        Me.btnSave.TabIndex = 0
        Me.btnSave.Text = "&Save"
        Me.btnSave.UseVisualStyleBackColor = True
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnClose.BackColor = System.Drawing.Color.White
        Me.btnClose.BackgroundImage = CType(resources.GetObject("btnClose.BackgroundImage"), System.Drawing.Image)
        Me.btnClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnClose.BorderColor = System.Drawing.Color.Empty
        Me.btnClose.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnClose.FlatAppearance.BorderSize = 0
        Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClose.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.ForeColor = System.Drawing.Color.Black
        Me.btnClose.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnClose.GradientForeColor = System.Drawing.Color.Black
        Me.btnClose.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Location = New System.Drawing.Point(597, 13)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Size = New System.Drawing.Size(97, 30)
        Me.btnClose.TabIndex = 1
        Me.btnClose.Text = "&Close"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'gbExpenseInformation
        '
        Me.gbExpenseInformation.BorderColor = System.Drawing.Color.Black
        Me.gbExpenseInformation.Checked = False
        Me.gbExpenseInformation.CollapseAllExceptThis = False
        Me.gbExpenseInformation.CollapsedHoverImage = Nothing
        Me.gbExpenseInformation.CollapsedNormalImage = Nothing
        Me.gbExpenseInformation.CollapsedPressedImage = Nothing
        Me.gbExpenseInformation.CollapseOnLoad = False
        Me.gbExpenseInformation.Controls.Add(Me.cboDefaultCostCenter)
        Me.gbExpenseInformation.Controls.Add(Me.lblDefaultCostCenter)
        Me.gbExpenseInformation.Controls.Add(Me.objbtnAddGLCode)
        Me.gbExpenseInformation.Controls.Add(Me.EZeeStraightLine1)
        Me.gbExpenseInformation.Controls.Add(Me.pnlBalanceSetting)
        Me.gbExpenseInformation.Controls.Add(Me.grpExpenseSettings)
        Me.gbExpenseInformation.Controls.Add(Me.LblBalanceSettings)
        Me.gbExpenseInformation.Controls.Add(Me.cboHeadType)
        Me.gbExpenseInformation.Controls.Add(Me.objbtnSearchGLCode)
        Me.gbExpenseInformation.Controls.Add(Me.lblTransactionHead)
        Me.gbExpenseInformation.Controls.Add(Me.cboGLCode)
        Me.gbExpenseInformation.Controls.Add(Me.LblGLCode)
        Me.gbExpenseInformation.Controls.Add(Me.objbtnSearchLeaveType)
        Me.gbExpenseInformation.Controls.Add(Me.cboExCategory)
        Me.gbExpenseInformation.Controls.Add(Me.LblLeaveType)
        Me.gbExpenseInformation.Controls.Add(Me.cboLeaveType)
        Me.gbExpenseInformation.Controls.Add(Me.lblUoM)
        Me.gbExpenseInformation.Controls.Add(Me.cboUoM)
        Me.gbExpenseInformation.Controls.Add(Me.lblDescription)
        Me.gbExpenseInformation.Controls.Add(Me.txtDescription)
        Me.gbExpenseInformation.Controls.Add(Me.lblExpenseCat)
        Me.gbExpenseInformation.Controls.Add(Me.txtCode)
        Me.gbExpenseInformation.Controls.Add(Me.objbtnOtherLanguage)
        Me.gbExpenseInformation.Controls.Add(Me.txtName)
        Me.gbExpenseInformation.Controls.Add(Me.lblCode)
        Me.gbExpenseInformation.Controls.Add(Me.lblName)
        Me.gbExpenseInformation.Controls.Add(Me.objSearchImprestName)
        Me.gbExpenseInformation.Controls.Add(Me.objSearchImprestCode)
        Me.gbExpenseInformation.Controls.Add(Me.cboImprestName)
        Me.gbExpenseInformation.Controls.Add(Me.cboImprestCode)
        Me.gbExpenseInformation.Dock = System.Windows.Forms.DockStyle.Fill
        Me.gbExpenseInformation.ExpandedHoverImage = Nothing
        Me.gbExpenseInformation.ExpandedNormalImage = Nothing
        Me.gbExpenseInformation.ExpandedPressedImage = Nothing
        Me.gbExpenseInformation.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbExpenseInformation.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbExpenseInformation.HeaderHeight = 25
        Me.gbExpenseInformation.HeaderMessage = ""
        Me.gbExpenseInformation.HeaderMessageFont = New System.Drawing.Font("Microsoft Sans Serif", 8.25!)
        Me.gbExpenseInformation.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbExpenseInformation.HeightOnCollapse = 0
        Me.gbExpenseInformation.LeftTextSpace = 0
        Me.gbExpenseInformation.Location = New System.Drawing.Point(0, 0)
        Me.gbExpenseInformation.Name = "gbExpenseInformation"
        Me.gbExpenseInformation.OpenHeight = 300
        Me.gbExpenseInformation.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbExpenseInformation.ShowBorder = True
        Me.gbExpenseInformation.ShowCheckBox = False
        Me.gbExpenseInformation.ShowCollapseButton = False
        Me.gbExpenseInformation.ShowDefaultBorderColor = True
        Me.gbExpenseInformation.ShowDownButton = False
        Me.gbExpenseInformation.ShowHeader = True
        Me.gbExpenseInformation.Size = New System.Drawing.Size(706, 416)
        Me.gbExpenseInformation.TabIndex = 0
        Me.gbExpenseInformation.Temp = 0
        Me.gbExpenseInformation.Text = "Expense Information"
        Me.gbExpenseInformation.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboDefaultCostCenter
        '
        Me.cboDefaultCostCenter.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboDefaultCostCenter.DropDownWidth = 215
        Me.cboDefaultCostCenter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboDefaultCostCenter.FormattingEnabled = True
        Me.cboDefaultCostCenter.Location = New System.Drawing.Point(118, 386)
        Me.cboDefaultCostCenter.Name = "cboDefaultCostCenter"
        Me.cboDefaultCostCenter.Size = New System.Drawing.Size(239, 21)
        Me.cboDefaultCostCenter.TabIndex = 294
        '
        'lblDefaultCostCenter
        '
        Me.lblDefaultCostCenter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDefaultCostCenter.Location = New System.Drawing.Point(11, 389)
        Me.lblDefaultCostCenter.Name = "lblDefaultCostCenter"
        Me.lblDefaultCostCenter.Size = New System.Drawing.Size(108, 15)
        Me.lblDefaultCostCenter.TabIndex = 295
        Me.lblDefaultCostCenter.Text = "Default Cost Center"
        Me.lblDefaultCostCenter.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objbtnAddGLCode
        '
        Me.objbtnAddGLCode.BackColor = System.Drawing.Color.Transparent
        Me.objbtnAddGLCode.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnAddGLCode.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnAddGLCode.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnAddGLCode.BorderSelected = False
        Me.objbtnAddGLCode.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnAddGLCode.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Add
        Me.objbtnAddGLCode.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnAddGLCode.Location = New System.Drawing.Point(363, 248)
        Me.objbtnAddGLCode.Name = "objbtnAddGLCode"
        Me.objbtnAddGLCode.Size = New System.Drawing.Size(21, 21)
        Me.objbtnAddGLCode.TabIndex = 39
        '
        'EZeeStraightLine1
        '
        Me.EZeeStraightLine1.BackColor = System.Drawing.Color.Transparent
        Me.EZeeStraightLine1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.EZeeStraightLine1.ForeColor = System.Drawing.SystemColors.AppWorkspace
        Me.EZeeStraightLine1.LineType = eZee.Common.StraightLineTypes.Vertical
        Me.EZeeStraightLine1.Location = New System.Drawing.Point(390, 24)
        Me.EZeeStraightLine1.Name = "EZeeStraightLine1"
        Me.EZeeStraightLine1.Size = New System.Drawing.Size(3, 388)
        Me.EZeeStraightLine1.TabIndex = 263
        Me.EZeeStraightLine1.Text = "EZeeStraightLine2"
        '
        'pnlBalanceSetting
        '
        Me.pnlBalanceSetting.Controls.Add(Me.rdIssueQtyBalAsOnDate)
        Me.pnlBalanceSetting.Controls.Add(Me.rdIssueQtyTotalBal)
        Me.pnlBalanceSetting.Location = New System.Drawing.Point(119, 169)
        Me.pnlBalanceSetting.Name = "pnlBalanceSetting"
        Me.pnlBalanceSetting.Size = New System.Drawing.Size(238, 46)
        Me.pnlBalanceSetting.TabIndex = 25
        '
        'rdIssueQtyBalAsOnDate
        '
        Me.rdIssueQtyBalAsOnDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.rdIssueQtyBalAsOnDate.Location = New System.Drawing.Point(3, 26)
        Me.rdIssueQtyBalAsOnDate.Name = "rdIssueQtyBalAsOnDate"
        Me.rdIssueQtyBalAsOnDate.Size = New System.Drawing.Size(232, 17)
        Me.rdIssueQtyBalAsOnDate.TabIndex = 25
        Me.rdIssueQtyBalAsOnDate.Text = "Issue Qty As Per Balance as on Date"
        Me.rdIssueQtyBalAsOnDate.UseVisualStyleBackColor = True
        '
        'rdIssueQtyTotalBal
        '
        Me.rdIssueQtyTotalBal.Checked = True
        Me.rdIssueQtyTotalBal.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.rdIssueQtyTotalBal.Location = New System.Drawing.Point(3, 3)
        Me.rdIssueQtyTotalBal.Name = "rdIssueQtyTotalBal"
        Me.rdIssueQtyTotalBal.Size = New System.Drawing.Size(228, 17)
        Me.rdIssueQtyTotalBal.TabIndex = 26
        Me.rdIssueQtyTotalBal.TabStop = True
        Me.rdIssueQtyTotalBal.Text = "Issue Qty As Per Total Balance"
        Me.rdIssueQtyTotalBal.UseVisualStyleBackColor = True
        '
        'grpExpenseSettings
        '
        Me.grpExpenseSettings.Controls.Add(Me.chkNotAllowBackDate)
        Me.grpExpenseSettings.Controls.Add(Me.nudMaxExpenseQty)
        Me.grpExpenseSettings.Controls.Add(Me.LblMaximumExpenseQty)
        Me.grpExpenseSettings.Controls.Add(Me.chkIsImprest)
        Me.grpExpenseSettings.Controls.Add(Me.chkMakeUnitPriceEditable)
        Me.grpExpenseSettings.Controls.Add(Me.chkShowOnESS)
        Me.grpExpenseSettings.Controls.Add(Me.gbP2PExpenseSetting)
        Me.grpExpenseSettings.Controls.Add(Me.chkConsiderDependants)
        Me.grpExpenseSettings.Controls.Add(Me.chkIsAccrued)
        Me.grpExpenseSettings.Controls.Add(Me.chkConsiderPayroll)
        Me.grpExpenseSettings.Controls.Add(Me.chkLeaveEncashment)
        Me.grpExpenseSettings.Controls.Add(Me.ChkAttachDocumentMandatory)
        Me.grpExpenseSettings.Controls.Add(Me.chkSecRouteMandatory)
        Me.grpExpenseSettings.Controls.Add(Me.chkDoNotShowExpInCR)
        Me.grpExpenseSettings.Location = New System.Drawing.Point(397, 29)
        Me.grpExpenseSettings.Name = "grpExpenseSettings"
        Me.grpExpenseSettings.Size = New System.Drawing.Size(303, 383)
        Me.grpExpenseSettings.TabIndex = 264
        Me.grpExpenseSettings.TabStop = False
        Me.grpExpenseSettings.Text = "Expense Settings"
        '
        'nudMaxExpenseQty
        '
        Me.nudMaxExpenseQty.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.nudMaxExpenseQty.Location = New System.Drawing.Point(178, 233)
        Me.nudMaxExpenseQty.Maximum = New Decimal(New Integer() {999, 0, 0, 0})
        Me.nudMaxExpenseQty.Name = "nudMaxExpenseQty"
        Me.nudMaxExpenseQty.Size = New System.Drawing.Size(50, 21)
        Me.nudMaxExpenseQty.TabIndex = 29
        Me.nudMaxExpenseQty.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'LblMaximumExpenseQty
        '
        Me.LblMaximumExpenseQty.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblMaximumExpenseQty.Location = New System.Drawing.Point(7, 236)
        Me.LblMaximumExpenseQty.Name = "LblMaximumExpenseQty"
        Me.LblMaximumExpenseQty.Size = New System.Drawing.Size(165, 15)
        Me.LblMaximumExpenseQty.TabIndex = 28
        Me.LblMaximumExpenseQty.Text = "Maximum Expense Quantity"
        '
        'chkIsImprest
        '
        Me.chkIsImprest.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkIsImprest.Location = New System.Drawing.Point(162, 45)
        Me.chkIsImprest.Name = "chkIsImprest"
        Me.chkIsImprest.Size = New System.Drawing.Size(135, 17)
        Me.chkIsImprest.TabIndex = 27
        Me.chkIsImprest.Text = "Is Imprest"
        Me.chkIsImprest.UseVisualStyleBackColor = True
        '
        'chkMakeUnitPriceEditable
        '
        Me.chkMakeUnitPriceEditable.Checked = True
        Me.chkMakeUnitPriceEditable.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chkMakeUnitPriceEditable.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkMakeUnitPriceEditable.Location = New System.Drawing.Point(9, 92)
        Me.chkMakeUnitPriceEditable.Name = "chkMakeUnitPriceEditable"
        Me.chkMakeUnitPriceEditable.Size = New System.Drawing.Size(281, 17)
        Me.chkMakeUnitPriceEditable.TabIndex = 26
        Me.chkMakeUnitPriceEditable.Text = "Make Unit Price Editable On Claim Application"
        Me.chkMakeUnitPriceEditable.UseVisualStyleBackColor = True
        '
        'chkShowOnESS
        '
        Me.chkShowOnESS.Checked = True
        Me.chkShowOnESS.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chkShowOnESS.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkShowOnESS.Location = New System.Drawing.Point(9, 183)
        Me.chkShowOnESS.Name = "chkShowOnESS"
        Me.chkShowOnESS.Size = New System.Drawing.Size(281, 17)
        Me.chkShowOnESS.TabIndex = 25
        Me.chkShowOnESS.Text = "Show On ESS"
        Me.chkShowOnESS.UseVisualStyleBackColor = True
        '
        'gbP2PExpenseSetting
        '
        Me.gbP2PExpenseSetting.BorderColor = System.Drawing.Color.Black
        Me.gbP2PExpenseSetting.Checked = False
        Me.gbP2PExpenseSetting.CollapseAllExceptThis = False
        Me.gbP2PExpenseSetting.CollapsedHoverImage = Nothing
        Me.gbP2PExpenseSetting.CollapsedNormalImage = Nothing
        Me.gbP2PExpenseSetting.CollapsedPressedImage = Nothing
        Me.gbP2PExpenseSetting.CollapseOnLoad = False
        Me.gbP2PExpenseSetting.Controls.Add(Me.Label1)
        Me.gbP2PExpenseSetting.Controls.Add(Me.rdCapex)
        Me.gbP2PExpenseSetting.Controls.Add(Me.chkHRExpense)
        Me.gbP2PExpenseSetting.Controls.Add(Me.chkBudgetMandatory)
        Me.gbP2PExpenseSetting.Controls.Add(Me.rdOpex)
        Me.gbP2PExpenseSetting.ExpandedHoverImage = Nothing
        Me.gbP2PExpenseSetting.ExpandedNormalImage = Nothing
        Me.gbP2PExpenseSetting.ExpandedPressedImage = Nothing
        Me.gbP2PExpenseSetting.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbP2PExpenseSetting.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbP2PExpenseSetting.HeaderHeight = 25
        Me.gbP2PExpenseSetting.HeaderMessage = ""
        Me.gbP2PExpenseSetting.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbP2PExpenseSetting.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbP2PExpenseSetting.HeightOnCollapse = 0
        Me.gbP2PExpenseSetting.LeftTextSpace = 0
        Me.gbP2PExpenseSetting.Location = New System.Drawing.Point(7, 267)
        Me.gbP2PExpenseSetting.Name = "gbP2PExpenseSetting"
        Me.gbP2PExpenseSetting.OpenHeight = 91
        Me.gbP2PExpenseSetting.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbP2PExpenseSetting.ShowBorder = True
        Me.gbP2PExpenseSetting.ShowCheckBox = False
        Me.gbP2PExpenseSetting.ShowCollapseButton = False
        Me.gbP2PExpenseSetting.ShowDefaultBorderColor = True
        Me.gbP2PExpenseSetting.ShowDownButton = False
        Me.gbP2PExpenseSetting.ShowHeader = True
        Me.gbP2PExpenseSetting.Size = New System.Drawing.Size(294, 105)
        Me.gbP2PExpenseSetting.TabIndex = 24
        Me.gbP2PExpenseSetting.Temp = 0
        Me.gbP2PExpenseSetting.Text = "P2P Integration Expense Settings"
        Me.gbP2PExpenseSetting.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label1
        '
        Me.Label1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(6, 82)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(96, 17)
        Me.Label1.TabIndex = 304
        Me.Label1.Text = "Expenditure Type"
        '
        'rdCapex
        '
        Me.rdCapex.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.rdCapex.Location = New System.Drawing.Point(187, 82)
        Me.rdCapex.Name = "rdCapex"
        Me.rdCapex.Size = New System.Drawing.Size(73, 17)
        Me.rdCapex.TabIndex = 27
        Me.rdCapex.Text = "Capex"
        Me.rdCapex.UseVisualStyleBackColor = True
        '
        'chkHRExpense
        '
        Me.chkHRExpense.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkHRExpense.Location = New System.Drawing.Point(6, 55)
        Me.chkHRExpense.Name = "chkHRExpense"
        Me.chkHRExpense.Size = New System.Drawing.Size(174, 21)
        Me.chkHRExpense.TabIndex = 302
        Me.chkHRExpense.Text = "HR Expense"
        Me.chkHRExpense.UseVisualStyleBackColor = True
        '
        'chkBudgetMandatory
        '
        Me.chkBudgetMandatory.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkBudgetMandatory.Location = New System.Drawing.Point(6, 30)
        Me.chkBudgetMandatory.Name = "chkBudgetMandatory"
        Me.chkBudgetMandatory.Size = New System.Drawing.Size(174, 21)
        Me.chkBudgetMandatory.TabIndex = 25
        Me.chkBudgetMandatory.Text = "Budget Mandatory"
        Me.chkBudgetMandatory.UseVisualStyleBackColor = True
        '
        'rdOpex
        '
        Me.rdOpex.Checked = True
        Me.rdOpex.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.rdOpex.Location = New System.Drawing.Point(108, 82)
        Me.rdOpex.Name = "rdOpex"
        Me.rdOpex.Size = New System.Drawing.Size(62, 17)
        Me.rdOpex.TabIndex = 27
        Me.rdOpex.TabStop = True
        Me.rdOpex.Text = "Opex"
        Me.rdOpex.UseVisualStyleBackColor = True
        '
        'chkConsiderDependants
        '
        Me.chkConsiderDependants.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkConsiderDependants.Location = New System.Drawing.Point(9, 160)
        Me.chkConsiderDependants.Name = "chkConsiderDependants"
        Me.chkConsiderDependants.Size = New System.Drawing.Size(281, 17)
        Me.chkConsiderDependants.TabIndex = 23
        Me.chkConsiderDependants.Text = "Consider Dependants"
        Me.chkConsiderDependants.UseVisualStyleBackColor = True
        '
        'chkIsAccrued
        '
        Me.chkIsAccrued.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkIsAccrued.Location = New System.Drawing.Point(162, 22)
        Me.chkIsAccrued.Name = "chkIsAccrued"
        Me.chkIsAccrued.Size = New System.Drawing.Size(135, 17)
        Me.chkIsAccrued.TabIndex = 3
        Me.chkIsAccrued.Text = "Is Accrued"
        Me.chkIsAccrued.UseVisualStyleBackColor = True
        '
        'chkConsiderPayroll
        '
        Me.chkConsiderPayroll.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkConsiderPayroll.Location = New System.Drawing.Point(9, 22)
        Me.chkConsiderPayroll.Name = "chkConsiderPayroll"
        Me.chkConsiderPayroll.Size = New System.Drawing.Size(147, 17)
        Me.chkConsiderPayroll.TabIndex = 2
        Me.chkConsiderPayroll.Text = "Map to Payroll"
        Me.chkConsiderPayroll.UseVisualStyleBackColor = True
        '
        'chkLeaveEncashment
        '
        Me.chkLeaveEncashment.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkLeaveEncashment.Location = New System.Drawing.Point(9, 45)
        Me.chkLeaveEncashment.Name = "chkLeaveEncashment"
        Me.chkLeaveEncashment.Size = New System.Drawing.Size(147, 17)
        Me.chkLeaveEncashment.TabIndex = 4
        Me.chkLeaveEncashment.Text = "Leave Encashment"
        Me.chkLeaveEncashment.UseVisualStyleBackColor = True
        '
        'ChkAttachDocumentMandatory
        '
        Me.ChkAttachDocumentMandatory.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ChkAttachDocumentMandatory.Location = New System.Drawing.Point(9, 137)
        Me.ChkAttachDocumentMandatory.Name = "ChkAttachDocumentMandatory"
        Me.ChkAttachDocumentMandatory.Size = New System.Drawing.Size(281, 17)
        Me.ChkAttachDocumentMandatory.TabIndex = 22
        Me.ChkAttachDocumentMandatory.Text = "Attach Document Mandatory"
        Me.ChkAttachDocumentMandatory.UseVisualStyleBackColor = True
        '
        'chkSecRouteMandatory
        '
        Me.chkSecRouteMandatory.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkSecRouteMandatory.Location = New System.Drawing.Point(9, 68)
        Me.chkSecRouteMandatory.Name = "chkSecRouteMandatory"
        Me.chkSecRouteMandatory.Size = New System.Drawing.Size(281, 17)
        Me.chkSecRouteMandatory.TabIndex = 5
        Me.chkSecRouteMandatory.Text = "Make Sector/Route Mandatory"
        Me.chkSecRouteMandatory.UseVisualStyleBackColor = True
        '
        'chkDoNotShowExpInCR
        '
        Me.chkDoNotShowExpInCR.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkDoNotShowExpInCR.Location = New System.Drawing.Point(9, 114)
        Me.chkDoNotShowExpInCR.Name = "chkDoNotShowExpInCR"
        Me.chkDoNotShowExpInCR.Size = New System.Drawing.Size(281, 17)
        Me.chkDoNotShowExpInCR.TabIndex = 6
        Me.chkDoNotShowExpInCR.Text = "Do not show this expense in Claims and Expenses"
        Me.chkDoNotShowExpInCR.UseVisualStyleBackColor = True
        '
        'LblBalanceSettings
        '
        Me.LblBalanceSettings.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblBalanceSettings.Location = New System.Drawing.Point(12, 173)
        Me.LblBalanceSettings.Name = "LblBalanceSettings"
        Me.LblBalanceSettings.Size = New System.Drawing.Size(100, 17)
        Me.LblBalanceSettings.TabIndex = 24
        Me.LblBalanceSettings.Text = "Balance Setting"
        '
        'cboHeadType
        '
        Me.cboHeadType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboHeadType.DropDownWidth = 350
        Me.cboHeadType.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboHeadType.FormattingEnabled = True
        Me.cboHeadType.Location = New System.Drawing.Point(118, 221)
        Me.cboHeadType.Name = "cboHeadType"
        Me.cboHeadType.Size = New System.Drawing.Size(239, 21)
        Me.cboHeadType.TabIndex = 18
        '
        'objbtnSearchGLCode
        '
        Me.objbtnSearchGLCode.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchGLCode.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchGLCode.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchGLCode.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchGLCode.BorderSelected = False
        Me.objbtnSearchGLCode.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchGLCode.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(177, Byte))
        Me.objbtnSearchGLCode.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objbtnSearchGLCode.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchGLCode.Location = New System.Drawing.Point(337, 248)
        Me.objbtnSearchGLCode.Name = "objbtnSearchGLCode"
        Me.objbtnSearchGLCode.Size = New System.Drawing.Size(20, 21)
        Me.objbtnSearchGLCode.TabIndex = 266
        '
        'lblTransactionHead
        '
        Me.lblTransactionHead.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTransactionHead.Location = New System.Drawing.Point(12, 224)
        Me.lblTransactionHead.Name = "lblTransactionHead"
        Me.lblTransactionHead.Size = New System.Drawing.Size(100, 17)
        Me.lblTransactionHead.TabIndex = 17
        Me.lblTransactionHead.Text = "Head Type"
        Me.lblTransactionHead.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboGLCode
        '
        Me.cboGLCode.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboGLCode.DropDownWidth = 300
        Me.cboGLCode.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboGLCode.FormattingEnabled = True
        Me.cboGLCode.Location = New System.Drawing.Point(118, 248)
        Me.cboGLCode.Name = "cboGLCode"
        Me.cboGLCode.Size = New System.Drawing.Size(213, 21)
        Me.cboGLCode.TabIndex = 266
        '
        'LblGLCode
        '
        Me.LblGLCode.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblGLCode.Location = New System.Drawing.Point(12, 250)
        Me.LblGLCode.Name = "LblGLCode"
        Me.LblGLCode.Size = New System.Drawing.Size(100, 17)
        Me.LblGLCode.TabIndex = 266
        Me.LblGLCode.Text = "GL Code"
        '
        'objbtnSearchLeaveType
        '
        Me.objbtnSearchLeaveType.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchLeaveType.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchLeaveType.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchLeaveType.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchLeaveType.BorderSelected = False
        Me.objbtnSearchLeaveType.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchLeaveType.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(177, Byte))
        Me.objbtnSearchLeaveType.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objbtnSearchLeaveType.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchLeaveType.Location = New System.Drawing.Point(363, 115)
        Me.objbtnSearchLeaveType.Name = "objbtnSearchLeaveType"
        Me.objbtnSearchLeaveType.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchLeaveType.TabIndex = 14
        '
        'cboExCategory
        '
        Me.cboExCategory.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboExCategory.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboExCategory.FormattingEnabled = True
        Me.cboExCategory.Location = New System.Drawing.Point(118, 34)
        Me.cboExCategory.Name = "cboExCategory"
        Me.cboExCategory.Size = New System.Drawing.Size(239, 21)
        Me.cboExCategory.TabIndex = 1
        '
        'LblLeaveType
        '
        Me.LblLeaveType.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblLeaveType.Location = New System.Drawing.Point(12, 117)
        Me.LblLeaveType.Name = "LblLeaveType"
        Me.LblLeaveType.Size = New System.Drawing.Size(100, 17)
        Me.LblLeaveType.TabIndex = 12
        Me.LblLeaveType.Text = "Leave Type"
        '
        'cboLeaveType
        '
        Me.cboLeaveType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboLeaveType.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboLeaveType.FormattingEnabled = True
        Me.cboLeaveType.Location = New System.Drawing.Point(118, 115)
        Me.cboLeaveType.Name = "cboLeaveType"
        Me.cboLeaveType.Size = New System.Drawing.Size(239, 21)
        Me.cboLeaveType.TabIndex = 13
        '
        'lblUoM
        '
        Me.lblUoM.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblUoM.Location = New System.Drawing.Point(12, 144)
        Me.lblUoM.Name = "lblUoM"
        Me.lblUoM.Size = New System.Drawing.Size(100, 17)
        Me.lblUoM.TabIndex = 15
        Me.lblUoM.Text = "UoM"
        '
        'cboUoM
        '
        Me.cboUoM.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboUoM.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboUoM.FormattingEnabled = True
        Me.cboUoM.Location = New System.Drawing.Point(118, 142)
        Me.cboUoM.Name = "cboUoM"
        Me.cboUoM.Size = New System.Drawing.Size(239, 21)
        Me.cboUoM.TabIndex = 16
        '
        'lblDescription
        '
        Me.lblDescription.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDescription.Location = New System.Drawing.Point(12, 277)
        Me.lblDescription.Name = "lblDescription"
        Me.lblDescription.Size = New System.Drawing.Size(100, 17)
        Me.lblDescription.TabIndex = 19
        Me.lblDescription.Text = "Description"
        '
        'txtDescription
        '
        Me.txtDescription.Flags = 0
        Me.txtDescription.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtDescription.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtDescription.Location = New System.Drawing.Point(118, 275)
        Me.txtDescription.MaxLength = 255
        Me.txtDescription.Multiline = True
        Me.txtDescription.Name = "txtDescription"
        Me.txtDescription.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.txtDescription.Size = New System.Drawing.Size(239, 105)
        Me.txtDescription.TabIndex = 20
        '
        'lblExpenseCat
        '
        Me.lblExpenseCat.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblExpenseCat.Location = New System.Drawing.Point(12, 36)
        Me.lblExpenseCat.Name = "lblExpenseCat"
        Me.lblExpenseCat.Size = New System.Drawing.Size(100, 17)
        Me.lblExpenseCat.TabIndex = 0
        Me.lblExpenseCat.Text = "Expense Category"
        '
        'txtCode
        '
        Me.txtCode.Flags = 0
        Me.txtCode.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtCode.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtCode.Location = New System.Drawing.Point(118, 61)
        Me.txtCode.MaxLength = 255
        Me.txtCode.Name = "txtCode"
        Me.txtCode.Size = New System.Drawing.Size(239, 21)
        Me.txtCode.TabIndex = 8
        '
        'objbtnOtherLanguage
        '
        Me.objbtnOtherLanguage.BackColor = System.Drawing.Color.Transparent
        Me.objbtnOtherLanguage.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnOtherLanguage.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnOtherLanguage.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnOtherLanguage.BorderSelected = False
        Me.objbtnOtherLanguage.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnOtherLanguage.Image = Global.Aruti.Main.My.Resources.Resources.OtherLanguage_16
        Me.objbtnOtherLanguage.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnOtherLanguage.Location = New System.Drawing.Point(363, 88)
        Me.objbtnOtherLanguage.Name = "objbtnOtherLanguage"
        Me.objbtnOtherLanguage.Size = New System.Drawing.Size(21, 21)
        Me.objbtnOtherLanguage.TabIndex = 11
        '
        'txtName
        '
        Me.txtName.Flags = 0
        Me.txtName.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtName.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtName.Location = New System.Drawing.Point(118, 88)
        Me.txtName.MaxLength = 255
        Me.txtName.Name = "txtName"
        Me.txtName.Size = New System.Drawing.Size(239, 21)
        Me.txtName.TabIndex = 10
        '
        'lblCode
        '
        Me.lblCode.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCode.Location = New System.Drawing.Point(12, 63)
        Me.lblCode.Name = "lblCode"
        Me.lblCode.Size = New System.Drawing.Size(100, 17)
        Me.lblCode.TabIndex = 7
        Me.lblCode.Text = "Code"
        '
        'lblName
        '
        Me.lblName.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblName.Location = New System.Drawing.Point(12, 90)
        Me.lblName.Name = "lblName"
        Me.lblName.Size = New System.Drawing.Size(100, 17)
        Me.lblName.TabIndex = 9
        Me.lblName.Text = "Name"
        '
        'objSearchImprestName
        '
        Me.objSearchImprestName.BackColor = System.Drawing.Color.Transparent
        Me.objSearchImprestName.BackColor1 = System.Drawing.Color.Transparent
        Me.objSearchImprestName.BackColor2 = System.Drawing.Color.Transparent
        Me.objSearchImprestName.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objSearchImprestName.BorderSelected = False
        Me.objSearchImprestName.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objSearchImprestName.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(177, Byte))
        Me.objSearchImprestName.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objSearchImprestName.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objSearchImprestName.Location = New System.Drawing.Point(363, 88)
        Me.objSearchImprestName.Name = "objSearchImprestName"
        Me.objSearchImprestName.Size = New System.Drawing.Size(21, 21)
        Me.objSearchImprestName.TabIndex = 271
        '
        'objSearchImprestCode
        '
        Me.objSearchImprestCode.BackColor = System.Drawing.Color.Transparent
        Me.objSearchImprestCode.BackColor1 = System.Drawing.Color.Transparent
        Me.objSearchImprestCode.BackColor2 = System.Drawing.Color.Transparent
        Me.objSearchImprestCode.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objSearchImprestCode.BorderSelected = False
        Me.objSearchImprestCode.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objSearchImprestCode.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(177, Byte))
        Me.objSearchImprestCode.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objSearchImprestCode.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objSearchImprestCode.Location = New System.Drawing.Point(363, 61)
        Me.objSearchImprestCode.Name = "objSearchImprestCode"
        Me.objSearchImprestCode.Size = New System.Drawing.Size(21, 21)
        Me.objSearchImprestCode.TabIndex = 270
        '
        'cboImprestName
        '
        Me.cboImprestName.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboImprestName.DropDownWidth = 350
        Me.cboImprestName.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboImprestName.FormattingEnabled = True
        Me.cboImprestName.Location = New System.Drawing.Point(118, 88)
        Me.cboImprestName.Name = "cboImprestName"
        Me.cboImprestName.Size = New System.Drawing.Size(239, 21)
        Me.cboImprestName.TabIndex = 269
        '
        'cboImprestCode
        '
        Me.cboImprestCode.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboImprestCode.DropDownWidth = 350
        Me.cboImprestCode.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboImprestCode.FormattingEnabled = True
        Me.cboImprestCode.Location = New System.Drawing.Point(118, 61)
        Me.cboImprestCode.Name = "cboImprestCode"
        Me.cboImprestCode.Size = New System.Drawing.Size(239, 21)
        Me.cboImprestCode.TabIndex = 268
        '
        'chkNotAllowBackDate
        '
        Me.chkNotAllowBackDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkNotAllowBackDate.Location = New System.Drawing.Point(9, 207)
        Me.chkNotAllowBackDate.Name = "chkNotAllowBackDate"
        Me.chkNotAllowBackDate.Size = New System.Drawing.Size(281, 17)
        Me.chkNotAllowBackDate.TabIndex = 30
        Me.chkNotAllowBackDate.Text = "Do Not Allow To Apply For Back Date"
        Me.chkNotAllowBackDate.UseVisualStyleBackColor = True
        '
        'frmExpensesAddEdit
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(706, 471)
        Me.Controls.Add(Me.gbExpenseInformation)
        Me.Controls.Add(Me.objFooter)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmExpensesAddEdit"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Add/Edit Expenses"
        Me.objFooter.ResumeLayout(False)
        Me.gbExpenseInformation.ResumeLayout(False)
        Me.gbExpenseInformation.PerformLayout()
        Me.pnlBalanceSetting.ResumeLayout(False)
        Me.grpExpenseSettings.ResumeLayout(False)
        CType(Me.nudMaxExpenseQty, System.ComponentModel.ISupportInitialize).EndInit()
        Me.gbP2PExpenseSetting.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents objFooter As eZee.Common.eZeeFooter
    Friend WithEvents btnSave As eZee.Common.eZeeLightButton
    Friend WithEvents btnClose As eZee.Common.eZeeLightButton
    Friend WithEvents gbExpenseInformation As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents objbtnOtherLanguage As eZee.Common.eZeeGradientButton
    Friend WithEvents txtName As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents txtCode As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblName As System.Windows.Forms.Label
    Friend WithEvents lblCode As System.Windows.Forms.Label
    Friend WithEvents cboExCategory As System.Windows.Forms.ComboBox
    Friend WithEvents lblExpenseCat As System.Windows.Forms.Label
    Friend WithEvents lblDescription As System.Windows.Forms.Label
    Friend WithEvents txtDescription As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblUoM As System.Windows.Forms.Label
    Friend WithEvents cboUoM As System.Windows.Forms.ComboBox
    Friend WithEvents LblLeaveType As System.Windows.Forms.Label
    Friend WithEvents cboLeaveType As System.Windows.Forms.ComboBox
    Friend WithEvents objbtnSearchLeaveType As eZee.Common.eZeeGradientButton
    Friend WithEvents chkConsiderPayroll As System.Windows.Forms.CheckBox
    Friend WithEvents cboHeadType As System.Windows.Forms.ComboBox
    Friend WithEvents lblTransactionHead As System.Windows.Forms.Label
    Friend WithEvents chkLeaveEncashment As System.Windows.Forms.CheckBox
    Friend WithEvents chkIsAccrued As System.Windows.Forms.CheckBox
    Friend WithEvents chkSecRouteMandatory As System.Windows.Forms.CheckBox
    Friend WithEvents chkDoNotShowExpInCR As System.Windows.Forms.CheckBox
    Friend WithEvents ChkAttachDocumentMandatory As System.Windows.Forms.CheckBox
    Friend WithEvents LblBalanceSettings As System.Windows.Forms.Label
    Friend WithEvents rdIssueQtyTotalBal As System.Windows.Forms.RadioButton
    Friend WithEvents rdIssueQtyBalAsOnDate As System.Windows.Forms.RadioButton
    Friend WithEvents pnlBalanceSetting As System.Windows.Forms.Panel
    Friend WithEvents EZeeStraightLine1 As eZee.Common.eZeeStraightLine
    Friend WithEvents grpExpenseSettings As System.Windows.Forms.GroupBox
    Friend WithEvents chkConsiderDependants As System.Windows.Forms.CheckBox
    Friend WithEvents gbP2PExpenseSetting As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents chkHRExpense As System.Windows.Forms.CheckBox
    Friend WithEvents chkBudgetMandatory As System.Windows.Forms.CheckBox
    Friend WithEvents objbtnSearchGLCode As eZee.Common.eZeeGradientButton
    Friend WithEvents cboGLCode As System.Windows.Forms.ComboBox
    Friend WithEvents LblGLCode As System.Windows.Forms.Label
    Friend WithEvents rdCapex As System.Windows.Forms.RadioButton
    Friend WithEvents rdOpex As System.Windows.Forms.RadioButton
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents objbtnAddGLCode As eZee.Common.eZeeGradientButton
    Friend WithEvents chkShowOnESS As System.Windows.Forms.CheckBox
    Friend WithEvents chkMakeUnitPriceEditable As System.Windows.Forms.CheckBox
    Friend WithEvents chkIsImprest As System.Windows.Forms.CheckBox
    Friend WithEvents cboImprestName As System.Windows.Forms.ComboBox
    Friend WithEvents cboImprestCode As System.Windows.Forms.ComboBox
    Friend WithEvents objSearchImprestName As eZee.Common.eZeeGradientButton
    Friend WithEvents objSearchImprestCode As eZee.Common.eZeeGradientButton
    Friend WithEvents nudMaxExpenseQty As System.Windows.Forms.NumericUpDown
    Friend WithEvents LblMaximumExpenseQty As System.Windows.Forms.Label
    Friend WithEvents cboDefaultCostCenter As System.Windows.Forms.ComboBox
    Friend WithEvents lblDefaultCostCenter As System.Windows.Forms.Label
    Friend WithEvents chkNotAllowBackDate As System.Windows.Forms.CheckBox
End Class
