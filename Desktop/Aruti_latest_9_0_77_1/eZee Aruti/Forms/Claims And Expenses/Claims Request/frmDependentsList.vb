﻿Option Strict On

#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data
Imports System.Globalization

#End Region


Public Class frmDependentsList

#Region " Private Variables "
    Private ReadOnly mstrModuleName As String = "frmDependentsList"
    Private mblnCancel As Boolean = True
    Private mintEmployeeId As Integer = -1
    Private mblnIsMedical As Boolean = False
    Private mblnIsLeave As Boolean = False
    Private mdtDate As Date = Nothing
#End Region

#Region " Display Dialog "

    Public Function displayDialog(ByRef iEmployeeId As Integer, ByVal blnIsMedical As Boolean, ByVal blnIsLeave As Boolean, Optional ByVal dtDate As Date = Nothing) As Boolean
        Try
            mintEmployeeId = iEmployeeId
            mblnIsMedical = blnIsMedical
            mblnIsLeave = blnIsLeave
            If dtDate = Nothing Then
                mdtDate = ConfigParameter._Object._CurrentDateAndTime.Date
            Else
                mdtDate = dtDate
            End If
            Me.ShowDialog()
            Return Not mblnCancel
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "displayDialog", mstrModuleName)
        Finally
        End Try
    End Function

#End Region

#Region " Form's Events "

    Private Sub frmDependentsList_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try

            Call Set_Logo(Me, gApplicationType)
            Language.setLanguage(Me.Name)
            Call OtherSettings()

            Dim objDependant As New clsDependants_Beneficiary_tran
            'Sohail (18 May 2019) -- Start
            'NMB Enhancement - REF # - 76.1 - On dependants menu, provide a way for user to set status of dependant as deceased etc. Allow user to deactivate a dependant with a reason instead of deleting.
            'Dim dsList As DataSet = objDependant.GetQualifiedDepedant(mintEmployeeId, mblnIsMedical, mblnIsLeave, mdtDate)
            Dim dsList As DataSet = objDependant.GetQualifiedDepedant(mintEmployeeId, mblnIsMedical, mblnIsLeave, mdtDate, dtAsOnDate:=mdtDate)
            'Sohail (18 May 2019) -- End
            dgDepedent.AutoGenerateColumns = False
            dgDepedent.DataSource = dsList.Tables(0)
            dgcolhName.DataPropertyName = "dependants"
            dgcolhAge.DataPropertyName = "age"
            dgcolhGender.DataPropertyName = "gender"
            dgcolhRelation.DataPropertyName = "relation"
            dgcolhMonth.DataPropertyName = "Months"
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmDependentsList_Load", mstrModuleName)
        End Try
    End Sub

    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()
            objfrm.displayDialog(Me)
            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub

#End Region

#Region "Button's Event"

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            mblnCancel = False
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnClose_Click", mstrModuleName)
        End Try
    End Sub

#End Region


    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Private Sub OtherSettings()
        Try
            Me.SuspendLayout()
            Call SetLanguage()

            Me.btnClose.GradientBackColor = GUI._ButttonBackColor
            Me.btnClose.GradientForeColor = GUI._ButttonFontColor


            Me.ResumeLayout()
        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "OtherSettings", mstrModuleName)
        End Try
    End Sub


    Private Sub SetLanguage()
        Try
            Me.Text = Language._Object.getCaption(Me.Name, Me.Text)

            Me.dgcolhName.HeaderText = Language._Object.getCaption(Me.dgcolhName.Name, Me.dgcolhName.HeaderText)
            Me.dgcolhGender.HeaderText = Language._Object.getCaption(Me.dgcolhGender.Name, Me.dgcolhGender.HeaderText)
            Me.dgcolhAge.HeaderText = Language._Object.getCaption(Me.dgcolhAge.Name, Me.dgcolhAge.HeaderText)
            Me.dgcolhMonth.HeaderText = Language._Object.getCaption(Me.dgcolhMonth.Name, Me.dgcolhMonth.HeaderText)
            Me.dgcolhRelation.HeaderText = Language._Object.getCaption(Me.dgcolhRelation.Name, Me.dgcolhRelation.HeaderText)
            Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
        End Try
    End Sub


    Private Sub SetMessages()
        Try

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>


End Class