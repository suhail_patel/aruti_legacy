﻿'Option Strict On

#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data
Imports System.IO
Imports System.Web

#End Region

Public Class frmClaims_RequestList

#Region " Private Variables "

    Private ReadOnly mstrModuleName As String = "frmClaims_RequestList"
    Private objClaimReqestMaster As clsclaim_request_master
    Private objClaimReqestTran As clsclaim_request_tran
    Private mstrAdvanceFilter As String = ""

#End Region

#Region " Private Functions "

    Private Sub FillCombo()
        Dim objPrd As New clscommom_period_Tran
        Dim objEMaster As New clsEmployee_Master
        Dim objMasterData As New clsMasterData
        Dim dsCombo As New DataSet
        Try
        
            dsCombo = objPrd.getListForCombo(enModuleReference.Payroll, FinancialYear._Object._YearUnkid, FinancialYear._Object._DatabaseName, FinancialYear._Object._Database_Start_Date, "List", True, enStatusType.Open)
            With cboPeriod
                .ValueMember = "periodunkid"
                .DisplayMember = "name"
                .DataSource = dsCombo.Tables(0)
                .SelectedValue = 0
            End With


            'Pinkal (11-Sep-2019) -- Start
            'Enhancement NMB - Working On Claim Retirement for NMB.
            'dsCombo = clsExpCommonMethods.Get_ExpenseTypes(True, False, True, "List", True)
            dsCombo = clsExpCommonMethods.Get_ExpenseTypes(True, False, True, "List", True, True)
            'Pinkal (11-Sep-2019) -- End

            With cboExpCategory
                .ValueMember = "id"
                .DisplayMember = "name"
                .DataSource = dsCombo.Tables(0)
                .SelectedValue = 0
            End With

      
            dsCombo = objEMaster.GetEmployeeList(FinancialYear._Object._DatabaseName, _
                                                 User._Object._Userunkid, _
                                                 FinancialYear._Object._YearUnkid, _
                                                 Company._Object._Companyunkid, _
                                                 eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                 eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                 ConfigParameter._Object._UserAccessModeSetting, _
                                                 True, False, "List", True)

            With cboEmployee
                .ValueMember = "employeeunkid"
                .DisplayMember = "employeename"
                .DataSource = dsCombo.Tables(0)
                .SelectedValue = 0
            End With


            'Pinkal (07-Mar-2019) -- Start
            'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.
            'dsCombo = objMasterData.getLeaveStatusList("List")

            'Pinkal (03-Jan-2020) -- Start
            'Enhancement - ATLAS COPCO TANZANIA LTD [0003673] - Remove or rename “Rescheduled” status, when approver is changing leave form status.
            'dsCombo = objMasterData.getLeaveStatusList("List", False, False, True)
            dsCombo = objMasterData.getLeaveStatusList("List", "", False, False, True)
            'Pinkal (03-Jan-2020) -- End

            Dim dtab As DataTable = Nothing
            'dtab = New DataView(dsCombo.Tables(0), "statusunkid IN (0,1,2,3,6)", "statusunkid", DataViewRowState.CurrentRows).ToTable
            dtab = New DataView(dsCombo.Tables(0), "statusunkid IN (0,1,2,3,6,8)", "statusunkid", DataViewRowState.CurrentRows).ToTable()
            'Pinkal (07-Mar-2019) -- End
            With cboStatus
                .ValueMember = "statusunkid"
                .DisplayMember = "Name"
                .DataSource = dtab
                .SelectedValue = 0
            End With

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub SetVisibility()
        Try
            btnNew.Enabled = User._Object.Privilege._AllowtoAddClaimExpenseForm
            btnEdit.Enabled = User._Object.Privilege._AllowtoEditClaimExpenseForm
            btnDelete.Enabled = User._Object.Privilege._AllowtoDeleteClaimExpenseForm
            mnuCancelExpenseForm.Enabled = User._Object.Privilege._AllowtoCancelClaimExpenseForm

            'Varsha Rana (17-Oct-2017) -- Start
            'Enhancement - Give user privileges.
            mnuGlobalApproveExpense.Enabled = User._Object.Privilege._AllowtoProcessClaimExpenseForm
            'Varsha Rana (17-Oct-2017) -- End

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetVisibility", mstrModuleName)
        End Try
    End Sub

    Private Sub Fill_List()
        Dim dsList As New DataSet
        'Dim dTable As DataTable
        Dim StrSearch As String = String.Empty
        Try

            'Pinkal (13-Jul-2015) -- Start
            'Enhancement - WORKING ON C & R ACCESS PRIVILEGE.
            If User._Object.Privilege._AllowtoViewClaimExpenseFormList = False Then Exit Sub
            'Pinkal (13-Jul-2015) -- End


            'Pinkal (22-Jun-2015) -- Start
            'Enhancement - CHANGES IN CLAIM & REQUEST AS PER MR.ANDREW REQUIREMENT.
            'dsList = objClaimReqestMaster.GetList("List")
            'Pinkal (22-Jun-2015) -- End

            'Pinkal (24-Aug-2015) -- Start
            'Enhancement - WORKING ON ACTIVE EMPLOYEE CONDITION.
            'dsList = objClaimReqestMaster.GetList("List", ConfigParameter._Object._PaymentApprovalwithLeaveApproval)


            If CInt(cboEmployee.SelectedValue) > 0 Then
                StrSearch &= "AND cmclaim_request_master.employeeunkid = '" & CInt(cboEmployee.SelectedValue) & "' "
            End If

            If CInt(cboExpCategory.SelectedValue) > 0 Then
                StrSearch &= "AND cmclaim_request_master.expensetypeid = '" & CInt(cboExpCategory.SelectedValue) & "' "
            End If

            If CInt(cboPeriod.SelectedValue) > 0 Then
                StrSearch &= "AND cmclaim_request_master.periodunkid = '" & CInt(cboPeriod.SelectedValue) & "' "
            End If

            If CInt(cboStatus.SelectedValue) > 0 Then
                StrSearch &= "AND cmclaim_request_master.statusunkid = '" & CInt(cboStatus.SelectedValue) & "' "
            End If

            If txtClaimNo.Text.Trim.Length > 0 Then
                StrSearch &= "AND cmclaim_request_master.claimrequestno LIKE '%" & txtClaimNo.Text & "%' "
            End If

            If dtpFDate.Checked = True AndAlso dtpTDate.Checked = True Then
                StrSearch &= "AND cmclaim_request_master.transactiondate >= '" & eZeeDate.convertDate(dtpFDate.Value) & "' AND cmclaim_request_master.transactiondate <= '" & eZeeDate.convertDate(dtpTDate.Value) & "' "
            End If

            If mstrAdvanceFilter.Length > 0 Then
                StrSearch &= "AND " & mstrAdvanceFilter
            End If

            'If StrSearch.Trim.Length > 0 Then
            '    StrSearch = StrSearch.Substring(3)
            '    dTable = New DataView(dsList.Tables("List"), StrSearch, "", DataViewRowState.CurrentRows).ToTable
            'Else
            '    dTable = New DataView(dsList.Tables("List"), "", "", DataViewRowState.CurrentRows).ToTable
            'End If

            If StrSearch.Trim.Length > 0 Then
                StrSearch = StrSearch.Substring(3)
            End If

            dsList = objClaimReqestMaster.GetList("List", ConfigParameter._Object._PaymentApprovalwithLeaveApproval, FinancialYear._Object._DatabaseName, User._Object._Userunkid _
                                                                   , FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, ConfigParameter._Object._EmployeeAsOnDate _
                                                                   , ConfigParameter._Object._UserAccessModeSetting, True, True, StrSearch)



            lvClaimRequestList.Items.Clear()


            'Pinkal (10-Jun-2020) -- Start
            'Enhancement Claim NMB - Implementing Maximum Quantity setting expense wise on Claim module.
            Dim lvArray As New List(Of ListViewItem)
            lvClaimRequestList.BeginUpdate()
            'Pinkal (10-Jun-2020) -- End


            For Each dRow As DataRow In dsList.Tables(0).Rows
                Dim lvitem As New ListViewItem
                lvitem.Tag = dRow.Item("crmasterunkid").ToString
                lvitem.Text = dRow.Item("claimrequestno").ToString
                lvitem.SubItems.Add(dRow.Item("expensetype").ToString)
                lvitem.SubItems.Add(dRow.Item("ename").ToString)
                lvitem.SubItems.Add(eZeeDate.convertDate(dRow.Item("tdate").ToString).ToShortDateString)

                'Pinkal (28-Apr-2020) -- Start
                'Optimization  - Working on Process Optimization and performance for require module.	
                Dim mdcAppliedAmount As Decimal = 0
                If ConfigParameter._Object._PaymentApprovalwithLeaveApproval = False Then
                    mdcAppliedAmount = CDec(dRow.Item("ReqAmount"))
                Else
                    mdcAppliedAmount = objClaimReqestTran.GetAppliedExpenseAmount(CInt(dRow.Item("crmasterunkid")))
                End If

                lvitem.SubItems.Add(Format(mdcAppliedAmount, GUI.fmtCurrency))
                'Pinkal (28-Apr-2020) -- End


               

                'Pinkal (13-Mar-2019) -- Start
                'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.
                If dRow.Item("p2prequisitionid").ToString().Length > 0 AndAlso CInt(dRow.Item("statusunkid")) = 1 Then
                    lvitem.SubItems.Add(Format(mdcAppliedAmount, GUI.fmtCurrency))
                Else
                    'Pinkal (28-Apr-2020) -- Start
                    'Optimization  - Working on Process Optimization and performance for require module.	
                    If ConfigParameter._Object._PaymentApprovalwithLeaveApproval = False Then
                        lvitem.SubItems.Add(Format(CDec(dRow("ApprovedAmt")), GUI.fmtCurrency))
                    Else
                Dim mdcApprovedAmount As Decimal = objClaimReqestMaster.GetFinalApproverApprovedAmount(CInt(dRow.Item("employeeunkid")), CInt(dRow.Item("expensetypeid")), CInt(dRow.Item("crmasterunkid")))
                lvitem.SubItems.Add(Format(mdcApprovedAmount, GUI.fmtCurrency))
                End If
                    'Pinkal (28-Apr-2020) -- End

                End If
                'Pinkal (13-Mar-2019) -- End


                lvitem.SubItems.Add(dRow.Item("status").ToString)
                lvitem.SubItems.Add(dRow.Item("statusunkid").ToString)
                lvitem.SubItems.Add(dRow.Item("expensetypeid").ToString)
                lvitem.SubItems.Add(dRow.Item("employeeunkid").ToString)

                'Pinkal (13-Mar-2019) -- Start
                'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.
                lvitem.SubItems.Add(dRow.Item("p2prequisitionid").ToString)
                'Pinkal (13-Mar-2019) -- End


                'Pinkal (10-Jun-2020) -- Start
                'Enhancement Claim NMB - Implementing Maximum Quantity setting expense wise on Claim module.
                'lvClaimRequestList.Items.Add(lvitem)
                lvArray.Add(lvitem)
                'Pinkal (10-Jun-2020) -- End

            Next


            'Pinkal (10-Jun-2020) -- Start
            'Enhancement Claim NMB - Implementing Maximum Quantity setting expense wise on Claim module.
            lvClaimRequestList.Items.AddRange(lvArray.ToArray)
            'Pinkal (10-Jun-2020) -- End

            If lvClaimRequestList.Items.Count > 16 Then
                colhStatus.Width = colhStatus.Width - 18
            Else
                colhStatus.Width = 120
            End If

            'Pinkal (24-Aug-2015) -- End

            'Pinkal (10-Jun-2020) -- Start
            'Enhancement Claim NMB - Implementing Maximum Quantity setting expense wise on Claim module.
            lvArray.Clear()
            lvArray = Nothing
            lvClaimRequestList.EndUpdate()
            'Pinkal (10-Jun-2020) -- End


        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Fill_List", mstrModuleName)
        Finally
        End Try
    End Sub

    'Pinkal (22-Oct-2021)-- Start
    'Claim Request Email Notification Issue due to NMB Global Approval email sending changes.
    Private Sub Send_Notification(ByVal intCompanyID As Object)
        Try
            If gobjEmailList.Count > 0 Then
                Dim objSendMail As New clsSendMail
SendEmail:
                For Each objEmail In gobjEmailList
                    objSendMail._ToEmail = objEmail._EmailTo
                    objSendMail._Subject = objEmail._Subject
                    objSendMail._Message = objEmail._Message
                    objSendMail._Form_Name = objEmail._FormName
                    objSendMail._LogEmployeeUnkid = objEmail._LogEmployeeUnkid
                    objSendMail._OperationModeId = objEmail._OperationModeId
                    objSendMail._UserUnkid = objEmail._UserUnkid
                    objSendMail._SenderAddress = objEmail._SenderAddress
                    objSendMail._ModuleRefId = objEmail._ModuleRefId

                    If objEmail._FileName.ToString.Trim.Length > 0 Then
                        objSendMail._AttachedFiles = objEmail._FileName
                    End If

                    Dim intCUnkId As Integer = 0
                    If TypeOf intCompanyID Is Integer Then
                        intCUnkId = CInt(intCompanyID)
                    Else
                        intCUnkId = CInt(intCompanyID(0))
                    End If
                    If objSendMail.SendMail(intCUnkId, CBool(IIf(objEmail._FileName.ToString.Trim.Length > 0, True, False)), _
                                                objEmail._ExportReportPath).ToString.Length > 0 Then
                        gobjEmailList.Remove(objEmail)
                        GoTo SendEmail
                    End If
                Next
                gobjEmailList.Clear()
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Send_Notification; Module Name: " & mstrModuleName)
        End Try
    End Sub
    'Pinkal (22-Oct-2021)-- End

#End Region

#Region " Form's Events "

    Private Sub frmClaims_RequestList_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        objClaimReqestMaster = New clsclaim_request_master
        objClaimReqestTran = New clsclaim_request_tran
        Try
            Call Set_Logo(Me, gApplicationType)
            Language.setLanguage(Me.Name)
            Call OtherSettings()
            Call SetVisibility()
            Call FillCombo()
            objClaimReqestMaster._IsPaymentApprovalwithLeaveApproval = ConfigParameter._Object._PaymentApprovalwithLeaveApproval
            objClaimReqestMaster._LeaveApproverForLeaveType = ConfigParameter._Object._IsLeaveApprover_ForLeaveType
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmClaims_RequestList_Load", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsclaim_request_master.SetMessages()
            objfrm._Other_ModuleNames = "clsclaim_request_master"
            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub

#End Region

#Region " Button's Events "

    Private Sub btnNew_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNew.Click
        Dim frm As New frmClaims_RequestAddEdit
        Try
            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If
            If frm.displayDialog(-1, enAction.ADD_ONE) = True Then
                Call Fill_List()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnNew_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub btnEdit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEdit.Click
        Dim frm As New frmClaims_RequestAddEdit
        Try
            If lvClaimRequestList.SelectedItems.Count = 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please select Claim Request from the list to perform further operation."), enMsgBoxStyle.Information)
                lvClaimRequestList.Select()
                Exit Sub
            End If

            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If

            If frm.displayDialog(CInt(lvClaimRequestList.SelectedItems(0).Tag), enAction.EDIT_ONE) Then
                Call Fill_List()
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnEdit_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub btnDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDelete.Click
        'Shani (20-Aug-2016) -- Start
        'Enhancement - Add Scan Attchment in Claim Request Module given by Matthew
        Dim dtExpense As DataTable = Nothing
        Dim mdtAttachement As DataTable = Nothing
        'Shani (20-Aug-2016) -- End
        Try
            If lvClaimRequestList.SelectedItems.Count = 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please select Claim Request from the list to perform further operation."), enMsgBoxStyle.Information)
                lvClaimRequestList.Select()
                Exit Sub
            End If

            If objClaimReqestMaster.isUsed(CInt(lvClaimRequestList.SelectedItems(0).Tag)) = True Then
                eZeeMsgBox.Show(objClaimReqestMaster._Message, enMsgBoxStyle.Information)
                Exit Sub
            End If

            If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Are you sure you want to delete this Claim Request?"), CType(enMsgBoxStyle.Question + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.Yes Then
                Dim sVoidReason As String = String.Empty
                Dim frm As New frmReasonSelection
                If User._Object._Isrighttoleft = True Then
                    frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                    frm.RightToLeftLayout = True
                    Call Language.ctlRightToLeftlayOut(frm)
                End If
                frm.displayDialog(enVoidCategoryType.OTHERS, sVoidReason)
                If sVoidReason.Trim.Length <= 0 Then Exit Sub

                'Pinkal (13-Jul-2015) -- Start
                'Enhancement - WORKING ON C & R ACCESS PRIVILEGE.
                objClaimReqestMaster._Crmasterunkid = CInt(lvClaimRequestList.SelectedItems(0).Tag)
                objClaimReqestMaster._IsPaymentApprovalwithLeaveApproval = ConfigParameter._Object._PaymentApprovalwithLeaveApproval
                objClaimReqestMaster._CompanyID = Company._Object._Companyunkid
                objClaimReqestMaster._ArutiSelfServiceURL = ConfigParameter._Object._ArutiSelfServiceURL
                objClaimReqestMaster._LoginMode = enLogin_Mode.DESKTOP
                objClaimReqestMaster._Userunkid = User._Object._Userunkid
                With objClaimReqestMaster
                    ._FormName = mstrModuleName
                    ._Loginemployeeunkid = 0
                    ._ClientIP = getIP()
                    ._HostName = getHostName()
                    ._FromWeb = False
                    ._AuditUserId = User._Object._Userunkid
                    ._CompanyUnkid = Company._Object._Companyunkid
                    ._AuditDate = ConfigParameter._Object._CurrentDateAndTime
                End With
                'Pinkal (13-Jul-2015) -- End

                objClaimReqestMaster._Isvoid = True
                objClaimReqestMaster._Voiddatetime = ConfigParameter._Object._CurrentDateAndTime
                objClaimReqestMaster._Voidreason = sVoidReason
                objClaimReqestMaster._Voiduserunkid = User._Object._Userunkid
                objClaimReqestMaster._VoidLoginEmployeeID = -1

                'Shani (20-Aug-2016) -- Start
                'Enhancement - Add Scan Attchment in Claim Request Module given by Matthew
                Dim objClaimTran As New clsclaim_request_tran
                objClaimTran._ClaimRequestMasterId = CInt(lvClaimRequestList.SelectedItems(0).Tag)
                dtExpense = objClaimTran._DataTable
                objClaimTran = Nothing
                'Shani (20-Aug-2016) -- End

                'Shani (20-Aug-2016) -- Start
                'Enhancement - Add Scan Attchment in Claim Request Module given by Matthew
                Dim objAttchement As New clsScan_Attach_Documents

                'S.SANDEEP |04-SEP-2021| -- START
                'Call objAttchement.GetList(ConfigParameter._Object._Document_Path, "List", "", CInt(lvClaimRequestList.SelectedItems(0).SubItems(objcolhEmployeeId.Index).Text))
                Dim blnOnlyStrut As Boolean = False
                If CInt(lvClaimRequestList.SelectedItems(0).SubItems(objcolhEmployeeId.Index).Text) <= 0 Then
                    blnOnlyStrut = True
                End If
                Call objAttchement.GetList(ConfigParameter._Object._Document_Path, "List", "", CInt(lvClaimRequestList.SelectedItems(0).SubItems(objcolhEmployeeId.Index).Text), , , , , blnOnlyStrut)
                'S.SANDEEP |04-SEP-2021| -- END
                Dim strTranIds As String = String.Join(",", dtExpense.AsEnumerable().Select(Function(x) x.Field(Of Integer)("crtranunkid").ToString).ToArray)
                If strTranIds.Trim.Length <= 0 Then strTranIds = "0"
                mdtAttachement = New DataView(objAttchement._Datatable, "scanattachrefid = " & enScanAttactRefId.CLAIM_REQUEST & " AND transactionunkid IN (" & strTranIds & ")", "", DataViewRowState.CurrentRows).ToTable
                objAttchement = Nothing
                For Each dRow As DataRow In mdtAttachement.Rows
                    dRow.Item("AUD") = "D"
                Next
                mdtAttachement.AcceptChanges()
                'Shani (20-Aug-2016) -- End




                'S.SANDEEP [28-May-2018] -- START
                'ISSUE/ENHANCEMENT : {Audit Trails} 
                objClaimReqestMaster._FormName = mstrModuleName
                objClaimReqestMaster._Loginemployeeunkid = 0
                objClaimReqestMaster._ClientIP = getIP()
                objClaimReqestMaster._HostName = getHostName()
                objClaimReqestMaster._FromWeb = False
                objClaimReqestMaster._AuditUserId = User._Object._Userunkid
objClaimReqestMaster._CompanyUnkid = Company._Object._Companyunkid
                objClaimReqestMaster._AuditDate = ConfigParameter._Object._CurrentDateAndTime
                'S.SANDEEP [28-May-2018] -- END

                objClaimReqestMaster.Delete(CInt(lvClaimRequestList.SelectedItems(0).Tag), _
                                            User._Object._Userunkid, _
                                            FinancialYear._Object._DatabaseName, _
                                            FinancialYear._Object._YearUnkid, _
                                            Company._Object._Companyunkid, _
                                            ConfigParameter._Object._EmployeeAsOnDate, _
                                            ConfigParameter._Object._UserAccessModeSetting, _
                                            True, mdtAttachement, True, Nothing)

                If objClaimReqestMaster._Message <> "" Then
                    eZeeMsgBox.Show(objClaimReqestMaster._Message, enMsgBoxStyle.Information)
                    Exit Sub
                Else
                    Dim blnIsIISInstalled As Boolean = IsSelfServiceExist()
                    Dim mdsDoc As DataSet = (New clsScan_Attach_Documents).GetDocFolderName("Docs")
                    Dim mstrFolderName As String = (From p In mdsDoc.Tables("Docs") Where (CInt(p.Item("Id").ToString) = enScanAttactRefId.CLAIM_REQUEST) Select (p.Item("Name").ToString)).FirstOrDefault

                    For Each dRow As DataRow In mdtAttachement.Rows
                        Dim strError As String = ""
                        If dRow("AUD").ToString = "D" AndAlso dRow("fileuniquename").ToString <> "" Then
                            Dim strFileName As String = dRow("fileuniquename").ToString

                            If blnIsIISInstalled Then
                                'Gajanan [8-April-2019] -- Start
                                'If clsFileUploadDownload.DeleteFile(dRow("filepath").ToString, strFileName, mstrFolderName, strError, ConfigParameter._Object._ArutiSelfServiceURL) = False Then
                                If clsFileUploadDownload.DeleteFile(dRow("filepath").ToString, strFileName, mstrFolderName, strError, ConfigParameter._Object._ArutiSelfServiceURL, ConfigParameter._Object._Companyunkid) = False Then
                                    'Gajanan [8-April-2019] -- End
                                    eZeeMsgBox.Show(strError, enMsgBoxStyle.Information)
                                    Exit Sub
                                End If
                            Else
                                If Directory.Exists(ConfigParameter._Object._Document_Path) Then
                                    Dim strDocLocalPath As String = ConfigParameter._Object._Document_Path & "\" & mstrFolderName & "\" & strFileName
                                    If File.Exists(strDocLocalPath) Then
                                        File.Delete(strDocLocalPath)
                                    End If
                                Else
                                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 6, "Configuration Path does not Exist."), enMsgBoxStyle.Information)
                                    Exit Sub
                                End If
                            End If
                        End If
                    Next


                    'Pinkal (22-Oct-2021)-- Start
                    'Claim Request Email Notification Issue due to NMB Global Approval email sending changes.
                    If gobjEmailList.Count > 0 Then
                        Dim objThread As Threading.Thread
                        If HttpContext.Current Is Nothing Then
                            objThread = New Threading.Thread(AddressOf Send_Notification)
                            objThread.IsBackground = True
                            Dim arr(1) As Object
                            arr(0) = Company._Object._Companyunkid
                            objThread.Start(arr)
                        Else
                            Call Send_Notification(Company._Object._Companyunkid)
                        End If
                    End If
                    'Pinkal (22-Oct-2021)-- End
               

                    lvClaimRequestList.SelectedItems(0).Remove()
                    Call Fill_List()
                    If lvClaimRequestList.Items.Count <= 0 Then
                        Exit Try
                    End If
                End If
                'Shani (20-Aug-2016) -- End

                'Pinkal (24-Aug-2015) -- End






            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnDelete_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Try
            If lvClaimRequestList.SelectedItems.Count = 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please select Claim Request from the list to perform further operation."), enMsgBoxStyle.Information)
                lvClaimRequestList.Select()
                Exit Sub
            End If

            If objClaimReqestMaster.isUsed(CInt(lvClaimRequestList.SelectedItems(0).Tag)) = True Then
                eZeeMsgBox.Show(objClaimReqestMaster._Message, enMsgBoxStyle.Information)
                Exit Sub
            End If

            If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Are you sure you want to cancel this Claim Request?"), CType(enMsgBoxStyle.Question + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.Yes Then
                Dim sVoidReason As String = String.Empty
                Dim frm As New frmReasonSelection
                If User._Object._Isrighttoleft = True Then
                    frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                    frm.RightToLeftLayout = True
                    Call Language.ctlRightToLeftlayOut(frm)
                End If
                frm.displayDialog(enVoidCategoryType.OTHERS, sVoidReason)
                If sVoidReason.Trim.Length <= 0 Then Exit Sub
                objClaimReqestMaster._Iscancel = True
                objClaimReqestMaster._Cancel_Datetime = ConfigParameter._Object._CurrentDateAndTime
                objClaimReqestMaster._Cancel_Remark = sVoidReason
                objClaimReqestMaster._Canceluserunkid = User._Object._Userunkid

                objClaimReqestMaster.Cancel(CInt(lvClaimRequestList.SelectedItems(0).Tag), User._Object._Userunkid)

                'S.SANDEEP [28-May-2018] -- START
                'ISSUE/ENHANCEMENT : {Audit Trails} 
                objClaimReqestMaster._FormName = mstrModuleName
                objClaimReqestMaster._Loginemployeeunkid = 0
                objClaimReqestMaster._ClientIP = getIP()
                objClaimReqestMaster._HostName = getHostName()
                objClaimReqestMaster._FromWeb = False
                objClaimReqestMaster._AuditUserId = User._Object._Userunkid
objClaimReqestMaster._CompanyUnkid = Company._Object._Companyunkid
                objClaimReqestMaster._AuditDate = ConfigParameter._Object._CurrentDateAndTime
                'S.SANDEEP [28-May-2018] -- END


                'Pinkal (22-Oct-2021)-- Start
                'Claim Request Email Notification Issue due to NMB Global Approval email sending changes.
                If gobjEmailList.Count > 0 Then
                    Dim objThread As Threading.Thread
                    If HttpContext.Current Is Nothing Then
                        objThread = New Threading.Thread(AddressOf Send_Notification)
                        objThread.IsBackground = True
                        Dim arr(1) As Object
                        arr(0) = Company._Object._Companyunkid
                        objThread.Start(arr)
                    Else
                        Call Send_Notification(Company._Object._Companyunkid)
                    End If
                End If
                'Pinkal (22-Oct-2021)-- End

                lvClaimRequestList.SelectedItems(0).Remove()
                Call Fill_List()
                If lvClaimRequestList.Items.Count <= 0 Then
                    Exit Try
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnCancel_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnClose_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub objbtnReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnReset.Click
        Try
            cboEmployee.SelectedValue = 0
            cboExpCategory.SelectedValue = 0
            cboPeriod.SelectedValue = 0
            dtpFDate.Checked = False
            dtpTDate.Checked = False
            cboStatus.SelectedValue = 0
            lvClaimRequestList.Items.Clear()
            mstrAdvanceFilter = ""
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnReset_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub objbtnSearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearch.Click
        Try
            Call Fill_List()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearch_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub objbtnSearchEmployee_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchEmployee.Click
        Dim frm As New frmCommonSearch
        Try

            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If

            With frm
                .ValueMember = cboEmployee.ValueMember
                .DisplayMember = cboEmployee.DisplayMember
                .CodeMember = "employeecode"
                .DataSource = CType(cboEmployee.DataSource, DataTable)
                If .DisplayDialog Then
                    cboEmployee.SelectedValue = .SelectedValue
                    cboEmployee.Focus()
                End If
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchEmployee_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

#End Region

#Region " Listview Event(s) "

    Private Sub lvClaimRequestList_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles lvClaimRequestList.SelectedIndexChanged
        Try
            If lvClaimRequestList.SelectedItems.Count > 0 Then


                'Pinkal (07-Mar-2019) -- Start
                'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.
                'If CInt(lvClaimRequestList.SelectedItems(0).SubItems(objColhStatusunkid.Index).Text) = 1 Then
                If CInt(lvClaimRequestList.SelectedItems(0).SubItems(objColhStatusunkid.Index).Text) = 1 OrElse CInt(lvClaimRequestList.SelectedItems(0).SubItems(objColhStatusunkid.Index).Text) = 8 _
                  OrElse CInt(lvClaimRequestList.SelectedItems(0).SubItems(objColhStatusunkid.Index).Text) = 3 OrElse CInt(lvClaimRequestList.SelectedItems(0).SubItems(objColhStatusunkid.Index).Text) = 6 Then   'Approved Or Reject Or Cancel Or Return
                    btnEdit.Enabled = False
                    btnDelete.Enabled = False

                ElseIf CInt(lvClaimRequestList.SelectedItems(0).SubItems(objColhStatusunkid.Index).Text) = 2 Then
                    btnEdit.Enabled = True
                    btnDelete.Enabled = True

                    'ElseIf CInt(lvClaimRequestList.SelectedItems(0).SubItems(objColhStatusunkid.Index).Text) = 3 OrElse CInt(lvClaimRequestList.SelectedItems(0).SubItems(objColhStatusunkid.Index).Text) = 6 Then
                    '    btnEdit.Enabled = False
                    '    btnDelete.Enabled = False

                End If
                'Pinkal (07-Mar-2019) -- End


                'Pinkal (07-Mar-2019) -- Start
                'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.

                'If CInt(lvClaimRequestList.SelectedItems(0).SubItems(objColhStatusunkid.Index).Text) = 1 Then
                '    cmnuExpenseFormOperations.Enabled = True
                'Else
                '    cmnuExpenseFormOperations.Enabled = False
                'End If

                If CInt(lvClaimRequestList.SelectedItems(0).SubItems(objColhStatusunkid.Index).Text) = 1 Then
                    mnuCancelExpenseForm.Enabled = True
                Else
                    mnuCancelExpenseForm.Enabled = False
                End If

                'Pinkal (07-Mar-2019) -- End


                'Pinkal (30-Mar-2020) -- Start
                'Enhancement - Changes Related to Retain Leave Expense when leave is cancel.
                If ConfigParameter._Object._AllowToCancelLeaveButRetainExpense AndAlso CInt(lvClaimRequestList.SelectedItems(0).SubItems(objcolhExpenseTypeId.Index).Text) = enExpenseType.EXP_LEAVE AndAlso CInt(lvClaimRequestList.SelectedItems(0).SubItems(objColhStatusunkid.Index).Text) = 1 Then
                    mnuCancelExpenseForm.Enabled = False
                End If
                'Pinkal (30-Mar-2020) -- End



                'Pinkal (20-Nov-2018) -- Start
                'Enhancement - Working on P2P Integration for NMB.
                If CInt(lvClaimRequestList.SelectedItems(0).SubItems(objColhStatusunkid.Index).Text) = 2 Then
                    Dim objClaimApproval As New clsclaim_request_approval_tran
                    Dim dsPendingList As DataSet = objClaimApproval.GetApproverExpesneList("Approval", False, ConfigParameter._Object._PaymentApprovalwithLeaveApproval _
                                                                                                                                        , FinancialYear._Object._DatabaseName, User._Object._Userunkid, ConfigParameter._Object._EmployeeAsOnDate _
                                                                                                                                        , CInt(lvClaimRequestList.SelectedItems(0).SubItems(objcolhExpenseTypeId.Index).Text), False, True, -1 _
                                                                                                                                        , "", CInt(lvClaimRequestList.SelectedItems(0).Tag))
                    objClaimApproval = Nothing

                    If dsPendingList IsNot Nothing AndAlso dsPendingList.Tables(0).Rows.Count > 0 Then
                        Dim xCount As Integer = dsPendingList.Tables(0).AsEnumerable().Where(Function(x) x.Field(Of Integer)("statusunkid") <> 2).Count
                        If xCount > 0 Then
                            btnEdit.Enabled = False
                            btnDelete.Enabled = False
                        End If
                    Else
                        btnEdit.Enabled = True
                        btnDelete.Enabled = True
                    End If
                End If

                '/* START FOR CHECK CLAIM FOR HAVING IS BUDGET MANDATORY OPTION IF YES THEN DISABLED EDIT,DELETE AND CANCEL EXPENSE OPTION AS PER RUTTA COMMENT.
                If ConfigParameter._Object._NewRequisitionRequestP2PServiceURL.Trim().Length > 0 Then
                    Dim objClaim As New clsclaim_request_tran
                    objClaim._ClaimRequestMasterId = CInt(lvClaimRequestList.SelectedItems(0).Tag)
                    Dim dtTable As DataTable = objClaim._DataTable
                    objClaim = Nothing
                    If dtTable IsNot Nothing AndAlso dtTable.Rows.Count > 0 Then
                        Dim xBudgetMandatoryCount As Integer = dtTable.AsEnumerable().Where(Function(x) x.Field(Of Boolean)("isbudgetmandatory") = True).Count
                        'Pinkal (04-Feb-2019) -- Start
                        'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.
                        Dim xNonHRExpenseCount As Integer = dtTable.AsEnumerable().Where(Function(x) x.Field(Of Boolean)("ishrexpense") = False).Count
                        If xBudgetMandatoryCount > 0 AndAlso xNonHRExpenseCount > 0 Then
                            btnEdit.Enabled = False
                            btnDelete.Enabled = False
                            cmnuExpenseFormOperations.Enabled = False
                        End If
                        'Pinkal (04-Feb-2019) -- End
                    End If
                End If
                '/* END FOR CHECK CLAIM FOR HAVING IS BUDGET MANDATORY OPTION IF YES THEN DISABLED EDIT,DELETE AND CANCEL EXPENSE OPTION AS PER RUTTA COMMENT.

                'Pinkal (20-Nov-2018) -- End

            Else
                btnEdit.Enabled = True
                btnDelete.Enabled = True
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lvClaimRequestList_SelectedIndexChanged", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

#Region "LinkButton Event"

    Private Sub lnkAllocation_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lnkAllocation.LinkClicked
        Try
            Dim frm As New frmAdvanceSearch
            frm.ShowDialog()
            mstrAdvanceFilter = frm._GetFilterString
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lnkAllocation_LinkClicked", mstrModuleName)
        End Try
    End Sub

#End Region

#Region "MenuItem Event"

    Private Sub mnuCancelExpenseForm_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuCancelExpenseForm.Click
        Try
            If lvClaimRequestList.SelectedItems.Count < 1 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Please select Expense Form from the list to perform further operation.."), enMsgBoxStyle.Information) '
                lvClaimRequestList.Select()
                Exit Sub
            End If

            Dim dtApprover As DataTable = Nothing
            Dim objApproverTran As New clsclaim_request_approval_tran
            dtApprover = objApproverTran.GetMaxApproverForClaimForm(CInt(lvClaimRequestList.SelectedItems(0).SubItems(objcolhEmployeeId.Index).Text) _
                                                                                                                               , CInt(lvClaimRequestList.SelectedItems(0).SubItems(objcolhExpenseTypeId.Index).Text) _
                                                                                                                               , CInt(lvClaimRequestList.SelectedItems(0).Tag) _
                                                                                                                               , ConfigParameter._Object._PaymentApprovalwithLeaveApproval _
                                                                                                                               , CInt(lvClaimRequestList.SelectedItems(0).SubItems(objColhStatusunkid.Index).Text))


            If dtApprover IsNot Nothing AndAlso dtApprover.Rows.Count > 0 Then
                Dim objCancelExpense As New frmCancelExpenseForm
                If objCancelExpense.displayDialog(CInt(lvClaimRequestList.SelectedItems(0).Tag), CInt(dtApprover.Rows(0)("crapproverunkid")) _
                                                                    , CInt(dtApprover.Rows(0)("approveremployeeunkid")), CInt(dtApprover.Rows(0)("crpriority"))) Then
                    Fill_List()
                End If
            Else
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "There is no Expense Data to Cancel the Expense Form."), enMsgBoxStyle.Information) '?1
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "mnuCancelExpenseForm_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub mnuGlobalApproveExpense_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuGlobalApproveExpense.Click
        Try
            Dim objFrm As New frmGlobalApproveExpense
            objFrm.displayDialog(enAction.ADD_ONE)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "mnuGlobalApproveExpense_Click", mstrModuleName)
        End Try
    End Sub

#End Region



    'Pinkal (27-Aug-2020) -- Start
    'Bug NMB:  Working on IIS Freezing and Dump Issue for NMB.
    Private Sub WebLanguage()
        Language.getMessage(mstrModuleName, 7, "Sorry, you cannot cancel selected data. Reason : It is not in approved status.")
        Language.getMessage(mstrModuleName, 8, "Sorry, you cannot edit selected data. Reason:It is already approved.")
        Language.getMessage(mstrModuleName, 9, "Sorry, you cannot edit selected data. Reason:It is already approved by one or more approver(s).")
        Language.getMessage(mstrModuleName, 10, "Sorry, you cannot edit selected data. Reason:It is already rejected.")
        Language.getMessage(mstrModuleName, 11, "Sorry, you cannot edit selected data. Reason: It is already cancelled.")
        Language.getMessage(mstrModuleName, 12, "Sorry, you cannot delete selected data. Reason : It is already approved.")
        Language.getMessage(mstrModuleName, 13, "Sorry, you cannot delete selected data. Reason:It is already approved by one or more approver(s).")
        Language.getMessage(mstrModuleName, 14, "Sorry, you cannot delete selected data. Reason : It is already rejected.")
        Language.getMessage(mstrModuleName, 15, "Sorry, you cannot delete selected data. Reason : It is already cancelled.")
        Language.getMessage(mstrModuleName, 16, "Sorry, you cannot edit selected data. Reason:It is already approved or pending for approval in P2P system.")
        Language.getMessage(mstrModuleName, 17, "Sorry, you cannot delete selected data. Reason:It is already approved or pending for approval in P2P system.")
        Language.getMessage(mstrModuleName, 18, "Sorry, you cannot cancel selected data. Reason:It is already approved in P2P system.")
        Language.getMessage(mstrModuleName, 19, "Expense Category is compulsory information.Please Select Expense Category.")
        Language.getMessage(mstrModuleName, 20, "Status is compulsory information.Please Select Status.")
    End Sub
    'Pinkal (27-Aug-2020) -- End



    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Private Sub OtherSettings()
        Try
            Me.SuspendLayout()
            Call SetLanguage()

            Me.gbFilterCriteria.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbFilterCriteria.ForeColor = GUI._eZeeContainerHeaderForeColor


	
            Me.eZeeHeader.GradientColor1 = GUI._HeaderBackColor1
            Me.eZeeHeader.GradientColor2 = GUI._HeaderBackColor2
            Me.eZeeHeader.BorderColor = GUI._HeaderBorderColor
            Me.eZeeHeader.HeaderTextForeColor = GUI._HeaderTitleFontColor
            Me.eZeeHeader.DescriptionForeColor = GUI._HeaderMessageFontColor


            Me.btnDelete.GradientBackColor = GUI._ButttonBackColor
            Me.btnDelete.GradientForeColor = GUI._ButttonFontColor

            Me.btnEdit.GradientBackColor = GUI._ButttonBackColor
            Me.btnEdit.GradientForeColor = GUI._ButttonFontColor

            Me.btnNew.GradientBackColor = GUI._ButttonBackColor
            Me.btnNew.GradientForeColor = GUI._ButttonFontColor

            Me.btnClose.GradientBackColor = GUI._ButttonBackColor
            Me.btnClose.GradientForeColor = GUI._ButttonFontColor

            Me.btnCancel.GradientBackColor = GUI._ButttonBackColor
            Me.btnCancel.GradientForeColor = GUI._ButttonFontColor

            Me.btnOperation.GradientBackColor = GUI._ButttonBackColor
            Me.btnOperation.GradientForeColor = GUI._ButttonFontColor


            Me.ResumeLayout()
        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "OtherSettings", mstrModuleName)
        End Try
    End Sub


    Private Sub SetLanguage()
        Try
            Me.Text = Language._Object.getCaption(Me.Name, Me.Text)

            Me.btnDelete.Text = Language._Object.getCaption(Me.btnDelete.Name, Me.btnDelete.Text)
            Me.btnEdit.Text = Language._Object.getCaption(Me.btnEdit.Name, Me.btnEdit.Text)
            Me.btnNew.Text = Language._Object.getCaption(Me.btnNew.Name, Me.btnNew.Text)
            Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
            Me.btnCancel.Text = Language._Object.getCaption(Me.btnCancel.Name, Me.btnCancel.Text)
			Me.eZeeHeader.Title = Language._Object.getCaption(Me.eZeeHeader.Name & "_Title" , Me.eZeeHeader.Title)
			Me.eZeeHeader.Message = Language._Object.getCaption(Me.eZeeHeader.Name & "_Message" , Me.eZeeHeader.Message)
            Me.gbFilterCriteria.Text = Language._Object.getCaption(Me.gbFilterCriteria.Name, Me.gbFilterCriteria.Text)
            Me.lblExpCategory.Text = Language._Object.getCaption(Me.lblExpCategory.Name, Me.lblExpCategory.Text)
            Me.lblClaimNo.Text = Language._Object.getCaption(Me.lblClaimNo.Name, Me.lblClaimNo.Text)
            Me.lblEmployee.Text = Language._Object.getCaption(Me.lblEmployee.Name, Me.lblEmployee.Text)
            Me.lblPeriod.Text = Language._Object.getCaption(Me.lblPeriod.Name, Me.lblPeriod.Text)
            Me.lblDate.Text = Language._Object.getCaption(Me.lblDate.Name, Me.lblDate.Text)
            Me.lblTo.Text = Language._Object.getCaption(Me.lblTo.Name, Me.lblTo.Text)
            Me.colhExpType.Text = Language._Object.getCaption(CStr(Me.colhExpType.Tag), Me.colhExpType.Text)
            Me.colhDate.Text = Language._Object.getCaption(CStr(Me.colhDate.Tag), Me.colhDate.Text)
            Me.colhClaimNo.Text = Language._Object.getCaption(CStr(Me.colhClaimNo.Tag), Me.colhClaimNo.Text)
            Me.colhEmployee.Text = Language._Object.getCaption(CStr(Me.colhEmployee.Tag), Me.colhEmployee.Text)
            Me.colhStatus.Text = Language._Object.getCaption(CStr(Me.colhStatus.Tag), Me.colhStatus.Text)
            Me.lblStatus.Text = Language._Object.getCaption(Me.lblStatus.Name, Me.lblStatus.Text)
            Me.colhAmount.Text = Language._Object.getCaption(CStr(Me.colhAmount.Tag), Me.colhAmount.Text)
            Me.colhApprovedAmount.Text = Language._Object.getCaption(CStr(Me.colhApprovedAmount.Tag), Me.colhApprovedAmount.Text)
            Me.lnkAllocation.Text = Language._Object.getCaption(Me.lnkAllocation.Name, Me.lnkAllocation.Text)
            Me.btnOperation.Text = Language._Object.getCaption(Me.btnOperation.Name, Me.btnOperation.Text)
            Me.mnuCancelExpenseForm.Text = Language._Object.getCaption(Me.mnuCancelExpenseForm.Name, Me.mnuCancelExpenseForm.Text)
            Me.mnuGlobalApproveExpense.Text = Language._Object.getCaption(Me.mnuGlobalApproveExpense.Name, Me.mnuGlobalApproveExpense.Text)

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
        End Try
    End Sub


    Private Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Please select Claim Request from the list to perform further operation.")
            Language.setMessage(mstrModuleName, 2, "Are you sure you want to delete this Claim Request?")
            Language.setMessage(mstrModuleName, 3, "Are you sure you want to cancel this Claim Request?")
            Language.setMessage(mstrModuleName, 4, "Please select Expense Form from the list to perform further operation..")
            Language.setMessage(mstrModuleName, 5, "There is no Expense Data to Cancel the Expense Form.")
            Language.setMessage(mstrModuleName, 6, "Configuration Path does not Exist.")
			Language.setMessage(mstrModuleName, 7, "Sorry, you cannot cancel selected data. Reason : It is not in approved status.")
			Language.setMessage(mstrModuleName, 8, "Sorry, you cannot edit selected data. Reason:It is already approved.")
			Language.setMessage(mstrModuleName, 9, "Sorry, you cannot edit selected data. Reason:It is already approved by one or more approver(s).")
			Language.setMessage(mstrModuleName, 10, "Sorry, you cannot edit selected data. Reason:It is already rejected.")
			Language.setMessage(mstrModuleName, 11, "Sorry, you cannot edit selected data. Reason: It is already cancelled.")
			Language.setMessage(mstrModuleName, 12, "Sorry, you cannot delete selected data. Reason : It is already approved.")
			Language.setMessage(mstrModuleName, 13, "Sorry, you cannot delete selected data. Reason:It is already approved by one or more approver(s).")
			Language.setMessage(mstrModuleName, 14, "Sorry, you cannot delete selected data. Reason : It is already rejected.")
			Language.setMessage(mstrModuleName, 15, "Sorry, you cannot delete selected data. Reason : It is already cancelled.")
			Language.setMessage(mstrModuleName, 16, "Sorry, you cannot edit selected data. Reason:It is already approved or pending for approval in P2P system.")
			Language.setMessage(mstrModuleName, 17, "Sorry, you cannot delete selected data. Reason:It is already approved or pending for approval in P2P system.")
			Language.setMessage(mstrModuleName, 18, "Sorry, you cannot cancel selected data. Reason:It is already approved in P2P system.")
			Language.setMessage(mstrModuleName, 19, "Expense Category is compulsory information.Please Select Expense Category.")
			Language.setMessage(mstrModuleName, 20, "Status is compulsory information.Please Select Status.")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class