<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmPrint_Export_Letters
    Inherits eZee.Common.eZeeForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmPrint_Export_Letters))
        Me.pnlMailCofirmation = New System.Windows.Forms.Panel
        Me.rtbDocument = New Aruti.Data.RichTextBoxPrintCtrl
        Me.tvLetterType = New System.Windows.Forms.TreeView
        Me.btnTo = New eZee.Common.eZeeSplitButton
        Me.cmnuMailTo = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.mnuApplicant = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuEmployee = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuLeaveForm = New System.Windows.Forms.ToolStripMenuItem
        Me.objefFormFooter = New eZee.Common.eZeeFooter
        Me.BtnExport = New eZee.Common.eZeeSplitButton
        Me.cmnuExport = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.tlmnuWord = New System.Windows.Forms.ToolStripMenuItem
        Me.tlmnuHtml = New System.Windows.Forms.ToolStripMenuItem
        Me.tlmnuRTF = New System.Windows.Forms.ToolStripMenuItem
        Me.tlmnuMergedWord = New System.Windows.Forms.ToolStripMenuItem
        Me.objbtnPrevious = New eZee.Common.eZeeLightButton(Me.components)
        Me.objbtnNext = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnCancel = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnPrint = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnViewMerge = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnFinish = New eZee.Common.eZeeLightButton(Me.components)
        Me.txtToAddress = New System.Windows.Forms.TextBox
        Me.eZeeHeader = New eZee.Common.eZeeHeader
        Me.mnuOperations = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.mnuCut = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuCopy = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuPaste = New System.Windows.Forms.ToolStripMenuItem
        Me.PrintDocument1 = New System.Drawing.Printing.PrintDocument
        Me.pnlMailCofirmation.SuspendLayout()
        Me.cmnuMailTo.SuspendLayout()
        Me.objefFormFooter.SuspendLayout()
        Me.cmnuExport.SuspendLayout()
        Me.mnuOperations.SuspendLayout()
        Me.SuspendLayout()
        '
        'pnlMailCofirmation
        '
        Me.pnlMailCofirmation.BackColor = System.Drawing.Color.Transparent
        Me.pnlMailCofirmation.Controls.Add(Me.rtbDocument)
        Me.pnlMailCofirmation.Controls.Add(Me.tvLetterType)
        Me.pnlMailCofirmation.Controls.Add(Me.btnTo)
        Me.pnlMailCofirmation.Controls.Add(Me.objefFormFooter)
        Me.pnlMailCofirmation.Controls.Add(Me.txtToAddress)
        Me.pnlMailCofirmation.Controls.Add(Me.eZeeHeader)
        Me.pnlMailCofirmation.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pnlMailCofirmation.Location = New System.Drawing.Point(0, 0)
        Me.pnlMailCofirmation.Name = "pnlMailCofirmation"
        Me.pnlMailCofirmation.Size = New System.Drawing.Size(830, 477)
        Me.pnlMailCofirmation.TabIndex = 0
        '
        'rtbDocument
        '
        Me.rtbDocument.Location = New System.Drawing.Point(223, 122)
        Me.rtbDocument.Name = "rtbDocument"
        Me.rtbDocument.Size = New System.Drawing.Size(597, 294)
        Me.rtbDocument.TabIndex = 12
        Me.rtbDocument.Text = ""
        '
        'tvLetterType
        '
        Me.tvLetterType.Location = New System.Drawing.Point(6, 62)
        Me.tvLetterType.Name = "tvLetterType"
        Me.tvLetterType.Size = New System.Drawing.Size(211, 354)
        Me.tvLetterType.TabIndex = 11
        '
        'btnTo
        '
        Me.btnTo.BorderColor = System.Drawing.Color.Black
        Me.btnTo.ContextMenuStrip = Me.cmnuMailTo
        Me.btnTo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnTo.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnTo.GradientForeColor = System.Drawing.SystemColors.WindowText
        Me.btnTo.Location = New System.Drawing.Point(223, 65)
        Me.btnTo.Name = "btnTo"
        Me.btnTo.ShowDefaultBorderColor = True
        Me.btnTo.Size = New System.Drawing.Size(71, 33)
        Me.btnTo.SplitButtonMenu = Me.cmnuMailTo
        Me.btnTo.TabIndex = 10
        Me.btnTo.Text = "To"
        '
        'cmnuMailTo
        '
        Me.cmnuMailTo.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mnuApplicant, Me.mnuEmployee, Me.mnuLeaveForm})
        Me.cmnuMailTo.Name = "cmnuMailTo"
        Me.cmnuMailTo.Size = New System.Drawing.Size(191, 70)
        '
        'mnuApplicant
        '
        Me.mnuApplicant.Name = "mnuApplicant"
        Me.mnuApplicant.Size = New System.Drawing.Size(190, 22)
        Me.mnuApplicant.Tag = "mnuApplicant"
        Me.mnuApplicant.Text = "Applicant"
        '
        'mnuEmployee
        '
        Me.mnuEmployee.Name = "mnuEmployee"
        Me.mnuEmployee.Size = New System.Drawing.Size(190, 22)
        Me.mnuEmployee.Tag = "mnuEmployee"
        Me.mnuEmployee.Text = "Employee"
        '
        'mnuLeaveForm
        '
        Me.mnuLeaveForm.Name = "mnuLeaveForm"
        Me.mnuLeaveForm.Size = New System.Drawing.Size(190, 22)
        Me.mnuLeaveForm.Text = "Employee Leave Form"
        '
        'objefFormFooter
        '
        Me.objefFormFooter.BorderColor = System.Drawing.Color.Silver
        Me.objefFormFooter.Controls.Add(Me.BtnExport)
        Me.objefFormFooter.Controls.Add(Me.objbtnPrevious)
        Me.objefFormFooter.Controls.Add(Me.objbtnNext)
        Me.objefFormFooter.Controls.Add(Me.btnCancel)
        Me.objefFormFooter.Controls.Add(Me.btnPrint)
        Me.objefFormFooter.Controls.Add(Me.btnViewMerge)
        Me.objefFormFooter.Controls.Add(Me.btnFinish)
        Me.objefFormFooter.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.objefFormFooter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objefFormFooter.GradiantStyle = eZee.Common.eZeeFooter.GradientStyle.Up
        Me.objefFormFooter.GradientColor1 = System.Drawing.Color.Transparent
        Me.objefFormFooter.GradientColor2 = System.Drawing.Color.Transparent
        Me.objefFormFooter.Location = New System.Drawing.Point(0, 422)
        Me.objefFormFooter.Name = "objefFormFooter"
        Me.objefFormFooter.Size = New System.Drawing.Size(830, 55)
        Me.objefFormFooter.TabIndex = 2
        '
        'BtnExport
        '
        Me.BtnExport.BorderColor = System.Drawing.Color.Black
        Me.BtnExport.ContextMenuStrip = Me.cmnuExport
        Me.BtnExport.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BtnExport.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.BtnExport.GradientForeColor = System.Drawing.SystemColors.WindowText
        Me.BtnExport.Location = New System.Drawing.Point(631, 13)
        Me.BtnExport.Name = "BtnExport"
        Me.BtnExport.ShowDefaultBorderColor = True
        Me.BtnExport.Size = New System.Drawing.Size(90, 30)
        Me.BtnExport.SplitButtonMenu = Me.cmnuExport
        Me.BtnExport.TabIndex = 13
        Me.BtnExport.Text = "Export"
        '
        'cmnuExport
        '
        Me.cmnuExport.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.tlmnuWord, Me.tlmnuHtml, Me.tlmnuRTF, Me.tlmnuMergedWord})
        Me.cmnuExport.Name = "cmnuMailTo"
        Me.cmnuExport.Size = New System.Drawing.Size(148, 92)
        '
        'tlmnuWord
        '
        Me.tlmnuWord.Name = "tlmnuWord"
        Me.tlmnuWord.Size = New System.Drawing.Size(147, 22)
        Me.tlmnuWord.Tag = "mnuWord"
        Me.tlmnuWord.Text = "Word"
        '
        'tlmnuHtml
        '
        Me.tlmnuHtml.Name = "tlmnuHtml"
        Me.tlmnuHtml.Size = New System.Drawing.Size(147, 22)
        Me.tlmnuHtml.Tag = "mnuHtml"
        Me.tlmnuHtml.Text = "Html"
        '
        'tlmnuRTF
        '
        Me.tlmnuRTF.Name = "tlmnuRTF"
        Me.tlmnuRTF.Size = New System.Drawing.Size(147, 22)
        Me.tlmnuRTF.Text = "RTF"
        '
        'tlmnuMergedWord
        '
        Me.tlmnuMergedWord.Name = "tlmnuMergedWord"
        Me.tlmnuMergedWord.Size = New System.Drawing.Size(147, 22)
        Me.tlmnuMergedWord.Text = "Merged Word"
        '
        'objbtnPrevious
        '
        Me.objbtnPrevious.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.objbtnPrevious.BackColor = System.Drawing.Color.White
        Me.objbtnPrevious.BackgroundImage = CType(resources.GetObject("objbtnPrevious.BackgroundImage"), System.Drawing.Image)
        Me.objbtnPrevious.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.objbtnPrevious.BorderColor = System.Drawing.Color.Empty
        Me.objbtnPrevious.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.objbtnPrevious.FlatAppearance.BorderSize = 0
        Me.objbtnPrevious.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.objbtnPrevious.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objbtnPrevious.ForeColor = System.Drawing.Color.Black
        Me.objbtnPrevious.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.objbtnPrevious.GradientForeColor = System.Drawing.Color.Black
        Me.objbtnPrevious.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.objbtnPrevious.HoverGradientForeColor = System.Drawing.Color.Black
        Me.objbtnPrevious.Image = Global.Aruti.Main.My.Resources.Resources.Previous_24x24
        Me.objbtnPrevious.Location = New System.Drawing.Point(9, 12)
        Me.objbtnPrevious.Name = "objbtnPrevious"
        Me.objbtnPrevious.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.objbtnPrevious.PressedGradientForeColor = System.Drawing.Color.Black
        Me.objbtnPrevious.Size = New System.Drawing.Size(30, 30)
        Me.objbtnPrevious.TabIndex = 0
        Me.objbtnPrevious.UseVisualStyleBackColor = False
        '
        'objbtnNext
        '
        Me.objbtnNext.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.objbtnNext.BackColor = System.Drawing.Color.White
        Me.objbtnNext.BackgroundImage = CType(resources.GetObject("objbtnNext.BackgroundImage"), System.Drawing.Image)
        Me.objbtnNext.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.objbtnNext.BorderColor = System.Drawing.Color.Empty
        Me.objbtnNext.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.objbtnNext.FlatAppearance.BorderSize = 0
        Me.objbtnNext.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.objbtnNext.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objbtnNext.ForeColor = System.Drawing.Color.Black
        Me.objbtnNext.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.objbtnNext.GradientForeColor = System.Drawing.Color.Black
        Me.objbtnNext.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.objbtnNext.HoverGradientForeColor = System.Drawing.Color.Black
        Me.objbtnNext.Image = Global.Aruti.Main.My.Resources.Resources.Next_24x24
        Me.objbtnNext.Location = New System.Drawing.Point(45, 12)
        Me.objbtnNext.Name = "objbtnNext"
        Me.objbtnNext.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.objbtnNext.PressedGradientForeColor = System.Drawing.Color.Black
        Me.objbtnNext.Size = New System.Drawing.Size(30, 30)
        Me.objbtnNext.TabIndex = 1
        Me.objbtnNext.UseVisualStyleBackColor = False
        '
        'btnCancel
        '
        Me.btnCancel.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnCancel.BackColor = System.Drawing.Color.White
        Me.btnCancel.BackgroundImage = CType(resources.GetObject("btnCancel.BackgroundImage"), System.Drawing.Image)
        Me.btnCancel.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnCancel.BorderColor = System.Drawing.Color.Empty
        Me.btnCancel.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnCancel.FlatAppearance.BorderSize = 0
        Me.btnCancel.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnCancel.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnCancel.ForeColor = System.Drawing.Color.Black
        Me.btnCancel.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnCancel.GradientForeColor = System.Drawing.Color.Black
        Me.btnCancel.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnCancel.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnCancel.Location = New System.Drawing.Point(728, 13)
        Me.btnCancel.Name = "btnCancel"
        Me.btnCancel.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnCancel.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnCancel.Size = New System.Drawing.Size(90, 30)
        Me.btnCancel.TabIndex = 4
        Me.btnCancel.Text = "&Close"
        Me.btnCancel.UseVisualStyleBackColor = False
        '
        'btnPrint
        '
        Me.btnPrint.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnPrint.BackColor = System.Drawing.Color.White
        Me.btnPrint.BackgroundImage = CType(resources.GetObject("btnPrint.BackgroundImage"), System.Drawing.Image)
        Me.btnPrint.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnPrint.BorderColor = System.Drawing.Color.Empty
        Me.btnPrint.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnPrint.FlatAppearance.BorderSize = 0
        Me.btnPrint.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnPrint.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnPrint.ForeColor = System.Drawing.Color.Black
        Me.btnPrint.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnPrint.GradientForeColor = System.Drawing.Color.Black
        Me.btnPrint.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnPrint.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnPrint.Location = New System.Drawing.Point(631, 13)
        Me.btnPrint.Name = "btnPrint"
        Me.btnPrint.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnPrint.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnPrint.Size = New System.Drawing.Size(90, 30)
        Me.btnPrint.TabIndex = 3
        Me.btnPrint.Text = "&Print"
        Me.btnPrint.UseVisualStyleBackColor = False
        '
        'btnViewMerge
        '
        Me.btnViewMerge.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnViewMerge.BackColor = System.Drawing.Color.White
        Me.btnViewMerge.BackgroundImage = CType(resources.GetObject("btnViewMerge.BackgroundImage"), System.Drawing.Image)
        Me.btnViewMerge.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnViewMerge.BorderColor = System.Drawing.Color.Empty
        Me.btnViewMerge.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnViewMerge.FlatAppearance.BorderSize = 0
        Me.btnViewMerge.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnViewMerge.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnViewMerge.ForeColor = System.Drawing.Color.Black
        Me.btnViewMerge.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnViewMerge.GradientForeColor = System.Drawing.Color.Black
        Me.btnViewMerge.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnViewMerge.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnViewMerge.Location = New System.Drawing.Point(535, 13)
        Me.btnViewMerge.Name = "btnViewMerge"
        Me.btnViewMerge.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnViewMerge.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnViewMerge.Size = New System.Drawing.Size(90, 30)
        Me.btnViewMerge.TabIndex = 2
        Me.btnViewMerge.Text = "&View Merge"
        Me.btnViewMerge.UseVisualStyleBackColor = False
        '
        'btnFinish
        '
        Me.btnFinish.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnFinish.BackColor = System.Drawing.Color.White
        Me.btnFinish.BackgroundImage = CType(resources.GetObject("btnFinish.BackgroundImage"), System.Drawing.Image)
        Me.btnFinish.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnFinish.BorderColor = System.Drawing.Color.Empty
        Me.btnFinish.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnFinish.FlatAppearance.BorderSize = 0
        Me.btnFinish.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnFinish.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnFinish.ForeColor = System.Drawing.Color.Black
        Me.btnFinish.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnFinish.GradientForeColor = System.Drawing.Color.Black
        Me.btnFinish.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnFinish.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnFinish.Location = New System.Drawing.Point(535, 13)
        Me.btnFinish.Name = "btnFinish"
        Me.btnFinish.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnFinish.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnFinish.Size = New System.Drawing.Size(90, 30)
        Me.btnFinish.TabIndex = 9
        Me.btnFinish.Text = "&Finish"
        Me.btnFinish.UseVisualStyleBackColor = False
        '
        'txtToAddress
        '
        Me.txtToAddress.BackColor = System.Drawing.Color.White
        Me.txtToAddress.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtToAddress.Location = New System.Drawing.Point(300, 64)
        Me.txtToAddress.Multiline = True
        Me.txtToAddress.Name = "txtToAddress"
        Me.txtToAddress.ReadOnly = True
        Me.txtToAddress.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.txtToAddress.Size = New System.Drawing.Size(520, 52)
        Me.txtToAddress.TabIndex = 1
        '
        'eZeeHeader
        '
        Me.eZeeHeader.BackColor = System.Drawing.Color.White
        Me.eZeeHeader.BorderColor = System.Drawing.SystemColors.ControlDark
        Me.eZeeHeader.DescriptionForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.eZeeHeader.Dock = System.Windows.Forms.DockStyle.Top
        Me.eZeeHeader.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.eZeeHeader.GradiantStyle = eZee.Common.eZeeHeader.GradientStyle.Central
        Me.eZeeHeader.GradientColor1 = System.Drawing.SystemColors.Window
        Me.eZeeHeader.GradientColor2 = System.Drawing.SystemColors.Control
        Me.eZeeHeader.HeaderTextForeColor = System.Drawing.SystemColors.ControlText
        Me.eZeeHeader.Icon = Nothing
        Me.eZeeHeader.Location = New System.Drawing.Point(0, 0)
        Me.eZeeHeader.Message = ""
        Me.eZeeHeader.Name = "eZeeHeader"
        Me.eZeeHeader.Size = New System.Drawing.Size(830, 58)
        Me.eZeeHeader.TabIndex = 0
        Me.eZeeHeader.Title = "Send Mail"
        '
        'mnuOperations
        '
        Me.mnuOperations.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mnuCut, Me.mnuCopy, Me.mnuPaste})
        Me.mnuOperations.Name = "mnuOperations"
        Me.mnuOperations.Size = New System.Drawing.Size(103, 70)
        '
        'mnuCut
        '
        Me.mnuCut.Name = "mnuCut"
        Me.mnuCut.Size = New System.Drawing.Size(102, 22)
        Me.mnuCut.Tag = "mnuCut"
        Me.mnuCut.Text = "Cut"
        Me.mnuCut.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage
        '
        'mnuCopy
        '
        Me.mnuCopy.Name = "mnuCopy"
        Me.mnuCopy.Size = New System.Drawing.Size(102, 22)
        Me.mnuCopy.Tag = "mnuCopy"
        Me.mnuCopy.Text = "Copy"
        '
        'mnuPaste
        '
        Me.mnuPaste.Name = "mnuPaste"
        Me.mnuPaste.Size = New System.Drawing.Size(102, 22)
        Me.mnuPaste.Tag = "mnuPaste"
        Me.mnuPaste.Text = "Paste"
        '
        'PrintDocument1
        '
        '
        'frmPrint_Export_Letters
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(830, 477)
        Me.Controls.Add(Me.pnlMailCofirmation)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmPrint_Export_Letters"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Send Mail"
        Me.pnlMailCofirmation.ResumeLayout(False)
        Me.pnlMailCofirmation.PerformLayout()
        Me.cmnuMailTo.ResumeLayout(False)
        Me.objefFormFooter.ResumeLayout(False)
        Me.cmnuExport.ResumeLayout(False)
        Me.mnuOperations.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents pnlMailCofirmation As System.Windows.Forms.Panel
    Friend WithEvents mnuOperations As System.Windows.Forms.ContextMenuStrip
    Friend WithEvents mnuCut As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuCopy As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuPaste As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents eZeeHeader As eZee.Common.eZeeHeader
    Friend WithEvents objefFormFooter As eZee.Common.eZeeFooter
    Friend WithEvents btnFinish As eZee.Common.eZeeLightButton
    Friend WithEvents btnViewMerge As eZee.Common.eZeeLightButton
    Friend WithEvents btnCancel As eZee.Common.eZeeLightButton
    Friend WithEvents btnPrint As eZee.Common.eZeeLightButton
    Friend WithEvents objbtnPrevious As eZee.Common.eZeeLightButton
    Friend WithEvents objbtnNext As eZee.Common.eZeeLightButton
    Friend WithEvents txtToAddress As System.Windows.Forms.TextBox
    Friend WithEvents cmnuMailTo As System.Windows.Forms.ContextMenuStrip
    Friend WithEvents mnuApplicant As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuEmployee As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents btnTo As eZee.Common.eZeeSplitButton
    Friend WithEvents tvLetterType As System.Windows.Forms.TreeView
    Friend WithEvents PrintDocument1 As System.Drawing.Printing.PrintDocument
    Friend WithEvents rtbDocument As Aruti.Data.RichTextBoxPrintCtrl
    Friend WithEvents BtnExport As eZee.Common.eZeeSplitButton
    Friend WithEvents cmnuExport As System.Windows.Forms.ContextMenuStrip
    Friend WithEvents tlmnuWord As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents tlmnuHtml As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents tlmnuRTF As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuLeaveForm As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents tlmnuMergedWord As System.Windows.Forms.ToolStripMenuItem
End Class
