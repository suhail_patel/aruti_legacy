Option Strict On

#Region " Imports "

Imports System
Imports System.Data
Imports Aruti.Data
Imports eZeeCommonLib
Imports System.Text.RegularExpressions
Imports System.Text
Imports Aruti.Data.Language
Imports System.Threading
Imports System.Runtime.InteropServices
Imports System.Collections.Specialized
Imports System.IO

#End Region

Public Class frmSendMail

    'S.SANDEEP [05-Apr-2018] -- START
    'ISSUE/ENHANCEMENT : {Ref#216|#ARUTI-102}
    <DllImport("gdiplus.dll")> _
 Private Shared Function GdipEmfToWmfBits(ByVal _hEmf As IntPtr, ByVal _bufferSize As UInteger, ByVal _buffer() As Byte, ByVal _mappingMode As Integer, ByVal _flags As EmfToWmfBitsFlags) As UInteger
    End Function

    Private Enum EmfToWmfBitsFlags
        EmfToWmfBitsFlagsDefault = &H0
        EmfToWmfBitsFlagsEmbedEmf = &H1
        EmfToWmfBitsFlagsIncludePlaceable = &H2
        EmfToWmfBitsFlagsNoXORClip = &H4
    End Enum
    'S.SANDEEP [05-Apr-2018] -- END

#Region " Private Variables "

    Private ReadOnly mstrModuleName As String = "frmSendMail"
    Private mGlobalFontSize As Integer = 0
    Private objEmail_Master As clsEmail_Master
    Private mFontStyle As New FontStyle
    Private fonts() As FontStyle = New FontStyle() {FontStyle.Bold, FontStyle.Italic, FontStyle.Underline}
    Private rtbTemp As New RichTextBox
    Private strData As String = ""
    Private mintLetterUnkid As Integer = 0
    Private mintModulRefId As Integer = -1
    Private mblnIsFromDiary As Boolean = False
    Private dsList As New DataSet
    Private strMailAddess As String()
    Private intCount As Integer = 0
    Private dtRow As DataRow() = Nothing
    Private dicMailStatus As New Dictionary(Of String, Integer)
    Private strTextData As String = ""
    Private strUnsentAddress As String = ""
    Private strSendAddress As String = ""
    Private blnShowed As Boolean = False
    Private mstrReceipentId As String = ""
    Private blnIsFirstSent As Boolean = False
    Private strOtherLetter As String = ""
    Private _rtfSource As New System.Windows.Forms.RichTextBox
    Private objMfrm As Form
    Private mstrPeriodIds As String = String.Empty
    Private mdicAttachmentInfo As New Dictionary(Of Integer, String)
    Private mblnIsFromPaySlip As Boolean = False
    Private mdtPeriodStart As DateTime = Nothing
    Private mdtPeriodEnd As DateTime = Nothing
    Private mintCallingRefId As Integer = -1
    Private mblnIsFromSickSheet As Boolean = False
    Private mstrSickSheeIDs As String
    Private Const CP_NOCLOSE_BUTTON As Integer = &H200
    Private mdicDiscpline As New Dictionary(Of String, Integer)
    Private blnIsMailSending As Boolean = False
    Private mstrLeaveFormIds As String = String.Empty
    Private mblnIsFromReport As Boolean = False
    Private mdtReportFromDate As Date = Nothing
    Private mdtReportToDate As Date = Nothing
    'Sohail (17 Feb 2021) -- Start
    'NMB Enhancement : OLD-322 : Addition of Loan Parameters Field on Letter Templates.
    Private mstrLoanAdvanceTranIds As String = String.Empty 'Sohail (17 Feb 2021)
    Private mblnSendEachTranByUnkId As Boolean = False
    'Sohail (17 Feb 2021) -- End

#End Region

#Region " Properties "

    Public Property _ModulRefId() As Integer
        Get
            Return mintModulRefId
        End Get
        Set(ByVal value As Integer)
            mintModulRefId = value
        End Set
    End Property

    Public Property _IsFromDiary() As Boolean
        Get
            Return mblnIsFromDiary
        End Get
        Set(ByVal value As Boolean)
            mblnIsFromDiary = value
        End Set
    End Property

    Public WriteOnly Property _MForm() As Form
        Set(ByVal value As Form)
            objMfrm = value
        End Set
    End Property

    '<FOR DISABLING CLOSE BUTTON OF FORM PLEASE DO NOT DELETE THIS>
    Protected Overrides ReadOnly Property CreateParams() As CreateParams
        Get
            Dim myCp As CreateParams = MyBase.CreateParams
            myCp.ClassStyle = myCp.ClassStyle Or CP_NOCLOSE_BUTTON
            Return myCp
        End Get
    End Property
    '<FOR DISABLING CLOSE BUTTON OF FORM PLEASE DO NOT DELETE THIS>
#End Region

#Region " Display Dialog "
    Public Function displayDialog(ByVal intRefId As Integer, ByVal strEmailAddress As String, ByVal dsData As DataSet) As Boolean
        Try
            mintModulRefId = intRefId
            txtToAddress.Text = strEmailAddress
            dsList = dsData
            btnTo.Enabled = False

            Me.ShowDialog()

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "", mstrModuleName)
        End Try
    End Function
#End Region

#Region " Private Methods "
    Public Shared ReadOnly FontSizes As List(Of Single) = New List(Of Single)()

    Private Shared Sub InitializeFontSizes()
        FontSizes.AddRange(New Single() {8, 9, 10, 10.5F, 11, 12, 14, 16, 18, 20, 22, 24, 26, 28, 36, 48, 72})
    End Sub

    Private Sub FillCombo()
        Try
            objcboFont.Items.Clear()
            For Each obj As FontFamily In FontFamily.Families()
                objcboFont.Items.Add(obj.Name)
            Next
            objcboFontSize.Items.Clear()
            For i As Integer = 0 To FontSizes.Count - 1
                objcboFontSize.Items.Add(FontSizes.Item(i).ToString)
            Next
            RemoveHandler objcboFont.SelectedIndexChanged, AddressOf cboFont_SelectedIndexChanged
            objcboFont.SelectedIndex = objcboFont.FindStringExact("Arial")
            objcboFontSize.SelectedIndex = objcboFontSize.FindStringExact("8")
            AddHandler objcboFont.SelectedIndexChanged, AddressOf cboFont_SelectedIndexChanged
        Catch ex As Exception
            DisplayError.Show(CStr(-1), ex.Message, "FillCombo", mstrModuleName)
        End Try
    End Sub

    Private Sub ApplyStyle(ByVal mStyle As FontStyle, ByVal mAddStyle As Boolean)
        Try
            Dim intStart As Integer = rtbDocument.SelectionStart
            Dim intEnd As Integer = rtbDocument.SelectionLength
            Dim rtbTempStart As Integer = 0

            If intEnd <= 1 And rtbDocument.SelectionFont IsNot Nothing Then
                If mAddStyle Then
                    rtbDocument.SelectionFont = New Font(rtbDocument.SelectionFont, rtbDocument.SelectionFont.Style Or mStyle)
                Else
                    rtbDocument.SelectionFont = New Font(rtbDocument.SelectionFont, rtbDocument.SelectionFont.Style And (Not mStyle))
                End If
                Return
            End If

            rtbTemp.Rtf = rtbDocument.SelectedRtf

            For i As Integer = 0 To intEnd
                rtbTemp.Select(rtbTempStart + i, 1)
                If mAddStyle Then
                    rtbTemp.SelectionFont = New Font(rtbTemp.SelectionFont, rtbTemp.SelectionFont.Style Or mStyle)
                Else
                    rtbTemp.SelectionFont = New Font(rtbTemp.SelectionFont, rtbTemp.SelectionFont.Style And (Not mStyle))
                End If
            Next

            rtbTemp.Select(rtbTempStart, intEnd)
            rtbDocument.SelectedRtf = rtbTemp.SelectedRtf
            rtbDocument.Select(intStart, intEnd)
            Return
        Catch ex As Exception
            'DisplayError.Show(CStr(-1), ex.Message, "ApplyStyle", mstrModuleName)
        End Try
    End Sub

    Private Sub ChangeFont(ByVal mFontName As String)
        Try
            Dim intStart As Integer = rtbDocument.SelectionStart
            Dim intEnd As Integer = rtbDocument.SelectionLength
            Dim rtbTempStart As Integer = 0
            Dim mStyle As New FontStyle
            mStyle = GetDefaultStyle(mFontName)

            If intEnd <= 1 And rtbDocument.SelectionFont IsNot Nothing Then
                rtbDocument.SelectionFont = New Font(CStr(mFontName), CInt(IIf(rtbDocument.SelectionFont.Size <> Nothing, rtbDocument.SelectionFont.Size, 8)), mStyle)
                Return
            End If

            rtbTemp.Rtf = rtbDocument.SelectedRtf
            For i As Integer = 0 To intEnd - 1
                rtbTemp.Select(rtbTempStart + i, 1)
                If IsNothing(rtbTemp.SelectionFont) Then
                    rtbTemp.SelectionFont = New Font(mFontName, CInt(objcboFontSize.Text), mStyle)
                Else
                    rtbTemp.SelectionFont = New Font(CStr(mFontName), CInt(IIf(rtbTemp.SelectionFont.Size <> Nothing, rtbTemp.SelectionFont.Size, 8)), mStyle)
                End If
            Next
            rtbTemp.Select(rtbTempStart, intEnd)
            rtbDocument.SelectedRtf = rtbTemp.SelectedRtf
            rtbDocument.Select(intStart, intEnd)
            Return
        Catch ex As Exception
            'DisplayError.Show(CStr(-1), ex.Message, "ChangeFont", mstrModuleName)
        End Try
    End Sub

    Private Sub ChangeFontSize(ByVal mFSize As Double)
        Try
            If mFSize <= 0 Then
                Exit Sub
            End If

            Dim intStart As Integer = rtbDocument.SelectionStart
            Dim intEnd As Integer = rtbDocument.SelectionLength
            Dim rtbTempStart As Integer = 0

            If intEnd <= 1 And rtbDocument.SelectionFont IsNot Nothing Then
                rtbDocument.SelectionFont = New Font(rtbDocument.SelectionFont.FontFamily, CInt(mFSize), rtbDocument.SelectionFont.Style)
                Return
            End If

            rtbTemp.Rtf = rtbDocument.SelectedRtf
            For i As Integer = 0 To intEnd
                rtbTemp.Select(rtbTempStart + i, 1)
                Dim mStyle As New FontStyle
                If rtbDocument.SelectionLength > 0 Then
                    mStyle = GetDefaultStyle(rtbTemp.SelectionFont.FontFamily.Name)
                Else
                    mStyle = GetDefaultStyle(objcboFont.Text)
                End If
                If IsNothing(rtbTemp.SelectionFont) Then
                    rtbTemp.SelectionFont = New Font(objcboFont.Text, CInt(mFSize), mStyle)
                Else
                    rtbTemp.SelectionFont = New Font(rtbTemp.SelectionFont.FontFamily, CInt(mFSize), mStyle)
                End If
            Next

            rtbTemp.Select(rtbTempStart, intEnd)
            rtbDocument.SelectedRtf = rtbTemp.SelectedRtf
            rtbDocument.Select(intStart, intEnd)

            Return
        Catch ex As Exception
            'DisplayError.Show(CStr(-1), ex.Message, "ChangeFontSize", mstrModuleName)
        End Try
    End Sub

    Private Function GetDefaultStyle(ByVal mFontName As String) As FontStyle
        For Each fntName As FontFamily In FontFamily.Families
            If fntName.Name.Equals(mFontName) Then
                If fntName.IsStyleAvailable(FontStyle.Regular) Then
                    Return FontStyle.Regular
                ElseIf fntName.IsStyleAvailable(FontStyle.Bold) Then
                    Return FontStyle.Bold
                ElseIf fntName.IsStyleAvailable(FontStyle.Italic) Then
                    Return FontStyle.Italic
                ElseIf fntName.IsStyleAvailable(FontStyle.Underline) Then
                    Return FontStyle.Underline
                End If
            End If
        Next
    End Function

    Private Sub RegainOldStyle()
        Try
            If objtlbbtnBold.Checked = True Then
                Call ApplyStyle(FontStyle.Bold, True)
            Else
                Call ApplyStyle(FontStyle.Bold, False)
            End If
            If objtlbbtnItalic.Checked = True Then
                Call ApplyStyle(FontStyle.Italic, True)
            Else
                Call ApplyStyle(FontStyle.Italic, False)
            End If
            If objtlbbtnUnderline.Checked = True Then
                Call ApplyStyle(FontStyle.Underline, True)
            Else
                Call ApplyStyle(FontStyle.Underline, False)
            End If
        Catch ex As Exception
            'DisplayError.Show("-1", ex.Message, "RegainOldStyle", mstrModuleName)
        End Try
    End Sub

    Private Sub SetColor()
        Try
            txtToAddress.BackColor = GUI.ColorComp
            'txtCC.BackColor = GUI.ColorOptional
            txtSubject.BackColor = GUI.ColorOptional
            'lstAttachments.BackColor = GUI.ColorOptional
            objcboFont.BackColor = GUI.ColorOptional
            objcboFontSize.BackColor = GUI.ColorOptional
        Catch ex As Exception
            DisplayError.Show(CStr(-1), ex.Message, "SetColor", mstrModuleName)
        End Try
    End Sub

    Private Sub SetAddress()
        strMailAddess = txtToAddress.Text.Split(CChar(","))
        Try
            For i As Integer = 0 To strMailAddess.Length - 1
                Dim strFinalAddess As String = ""
                Dim k As Integer = strMailAddess(i).IndexOf("<")
                Dim j As Integer = strMailAddess(i).IndexOf(">")
                If k > 0 Then
                    strFinalAddess = strMailAddess(i).Substring(k)
                    strFinalAddess = strFinalAddess.Replace("<", "")
                    strFinalAddess = strFinalAddess.Replace(">", "")
                    strMailAddess.SetValue(Trim(strFinalAddess), i)
                End If
            Next
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetAddress", mstrModuleName)
        End Try
    End Sub

    Private Sub ViewMergeData(ByVal intNumber As Integer)
        Try
            RemoveHandler rtbDocument.TextChanged, AddressOf rtbDocument_TextChanged
            Dim strDataName As String = ""
            For i As Integer = intNumber To intNumber
                Dim StrCol As String = ""


                'Pinkal (18-Sep-2012) -- Start
                'Enhancement : TRA Changes

                If mstrLeaveFormIds.Trim.Length <= 0 Then

                    'S.SANDEEP [07-MAR-2017] -- START
                    'ISSUE/ENHANCEMENT : INDEX OUT OF BOUND ERRROR
                    'For j As Integer = 0 To dtRow(0).Table.Columns.Count - 1
                    '    StrCol = dtRow(0).Table.Columns(j).ColumnName
                    '    If rtbDocument.Rtf.Contains("#" & StrCol & "#") Then
                    '        rtbDocument.Focus()

                    '        'S.SANDEEP [ 07 MAY 2012 ] -- START
                    '        'ENHANCEMENT : TRA CHANGES
                    '        If StrCol.Trim.ToUpper = "PASSWORD" Then
                    '            If blnIsMailSending = False Then
                    '                strDataName = rtbDocument.Rtf.Replace("#" & StrCol & "#", dtRow(0).Item(StrCol).ToString)
                    '            ElseIf blnIsMailSending = True Then
                    '                strDataName = rtbDocument.Rtf.Replace("#" & StrCol & "#", dtRow(0).Item("Original_Password").ToString)
                    '            End If
                    '        Else
                    '            strDataName = rtbDocument.Rtf.Replace("#" & StrCol & "#", dtRow(0).Item(StrCol).ToString)
                    '        End If
                    '        'S.SANDEEP [ 07 MAY 2012 ] -- END
                    '        rtbDocument.Rtf = strDataName
                    '    End If
                    'Next
                    If dtRow.Length > 0 Then
                        For j As Integer = 0 To dtRow(0).Table.Columns.Count - 1
                            StrCol = dtRow(0).Table.Columns(j).ColumnName
                            If rtbDocument.Rtf.ToLower.Contains("#" & StrCol.ToLower & "#") Then
                                'Sohail (17 Feb 2021) - [.ToLower]
                                rtbDocument.Focus()
                                If StrCol.Trim.ToUpper = "PASSWORD" Then
                                    If blnIsMailSending = False Then
                                        'Sohail (17 Feb 2021) -- Start
                                        'NMB Enhancement : OLD-322 : Addition of Loan Parameters Field on Letter Templates.
                                        'strDataName = rtbDocument.Rtf.Replace("#" & StrCol & "#", dtRow(0).Item(StrCol).ToString)
                                        strDataName = Microsoft.VisualBasic.Strings.Replace(rtbDocument.Rtf, "#" & StrCol & "#", dtRow(0).Item(StrCol).ToString, , , CompareMethod.Text)
                                        'Sohail (17 Feb 2021) -- End
                                    ElseIf blnIsMailSending = True Then
                                        'Sohail (17 Feb 2021) -- Start
                                        'NMB Enhancement : OLD-322 : Addition of Loan Parameters Field on Letter Templates.
                                        'strDataName = rtbDocument.Rtf.Replace("#" & StrCol & "#", dtRow(0).Item("Original_Password").ToString)
                                        strDataName = Microsoft.VisualBasic.Strings.Replace(rtbDocument.Rtf, "#" & StrCol & "#", dtRow(0).Item("Original_Password").ToString, , , CompareMethod.Text)
                                        'Sohail (17 Feb 2021) -- End
                                    End If
                                    'S.SANDEEP [05-Apr-2018] -- START
                                    'ISSUE/ENHANCEMENT : {Ref#216|#ARUTI-102}
                                ElseIf StrCol.Trim.ToUpper = "COMPANYLOGO" Then
                                    'S.SANDEEP |08-JAN-2019| -- START
                                    'Dim data As Byte() = CType(dtRow(0).Item(StrCol), Byte())
                                    'Dim ms As MemoryStream = New MemoryStream(data)
                                    'Dim img As Image = Image.FromStream(ms)
                                    'Dim strValue As String = embedImage(img)
                                    'strDataName = rtbDocument.Rtf.Replace("#" & StrCol & "#", strValue)
                                    If IsDBNull(dtRow(0).Item(StrCol)) = False Then
                                    Dim data As Byte() = CType(dtRow(0).Item(StrCol), Byte())
                                    Dim ms As MemoryStream = New MemoryStream(data)
                                    Dim img As Image = Image.FromStream(ms)
                                    Dim strValue As String = embedImage(img)
                                        'Sohail (17 Feb 2021) -- Start
                                        'NMB Enhancement : OLD-322 : Addition of Loan Parameters Field on Letter Templates.
                                        'strDataName = rtbDocument.Rtf.Replace("#" & StrCol & "#", strValue)
                                        strDataName = Microsoft.VisualBasic.Strings.Replace(rtbDocument.Rtf, "#" & StrCol & "#", strValue, , , CompareMethod.Text)
                                        'Sohail (17 Feb 2021) -- End
                                    Else
                                        'Sohail (17 Feb 2021) -- Start
                                        'NMB Enhancement : OLD-322 : Addition of Loan Parameters Field on Letter Templates.
                                        'strDataName = rtbDocument.Rtf.Replace("#" & StrCol & "#", "")
                                        strDataName = Microsoft.VisualBasic.Strings.Replace(rtbDocument.Rtf, "#" & StrCol & "#", "", , , CompareMethod.Text)
                                        'Sohail (17 Feb 2021) -- End
                                    End If
                                    'S.SANDEEP |08-JAN-2019| -- END
                                ElseIf StrCol.Trim.ToUpper = "KEY DUTIES AND RESPONSIBILITIES" Then
                                    'Sohail (17 Feb 2021) -- Start
                                    'NMB Enhancement : OLD-322 : Addition of Loan Parameters Field on Letter Templates.
                                    'strDataName = rtbDocument.Rtf.Replace("#" & StrCol & "#", dtRow(0).Item(StrCol).ToString.Replace("\r\n", "\line"))
                                    strDataName = Microsoft.VisualBasic.Strings.Replace(rtbDocument.Rtf, "#" & StrCol & "#", dtRow(0).Item(StrCol).ToString.Replace("\r\n", "\line"), , , CompareMethod.Text)
                                    'Sohail (17 Feb 2021) -- End
                                Else
                                    'Sohail (17 Feb 2021) -- Start
                                    'NMB Enhancement : OLD-322 : Addition of Loan Parameters Field on Letter Templates.
                                    'strDataName = rtbDocument.Rtf.Replace("#" & StrCol & "#", dtRow(0).Item(StrCol).ToString)
                                    If TypeOf dtRow(0).Item(StrCol) Is Decimal Then
                                        strDataName = Microsoft.VisualBasic.Strings.Replace(rtbDocument.Rtf, "#" & StrCol & "#", Format(dtRow(0).Item(StrCol), GUI.fmtCurrency), , , CompareMethod.Text)
                                Else
                                        strDataName = Microsoft.VisualBasic.Strings.Replace(rtbDocument.Rtf, "#" & StrCol & "#", dtRow(0).Item(StrCol).ToString, , , CompareMethod.Text)
                                    End If
                                    'Sohail (17 Feb 2021) -- End
                                End If
                                'S.SANDEEP [05-Apr-2018] -- END

                                rtbDocument.Rtf = strDataName
                            End If
                        Next
                    End If
                    'S.SANDEEP [07-MAR-2017] -- END




                Else

                    'S.SANDEEP [06-MAR-2017] -- START
                    'ISSUE/ENHANCEMENT : Training Module Notification
                    'For j As Integer = 0 To dtRow(intNumber).Table.Columns.Count - 1
                    '    StrCol = dtRow(intNumber).Table.Columns(j).ColumnName
                    '    If rtbDocument.Rtf.Contains("#" & StrCol & "#") Then
                    '        rtbDocument.Focus()
                    '        If StrCol.Trim.ToUpper = "PASSWORD" Then
                    '            If blnIsMailSending = False Then
                    '                strDataName = rtbDocument.Rtf.Replace("#" & StrCol & "#", dtRow(intNumber).Item(StrCol).ToString)
                    '            ElseIf blnIsMailSending = True Then
                    '                strDataName = rtbDocument.Rtf.Replace("#" & StrCol & "#", dtRow(intNumber).Item("Original_Password").ToString)
                    '            End If
                    '        Else
                    '            strDataName = rtbDocument.Rtf.Replace("#" & StrCol & "#", dtRow(intNumber).Item(StrCol).ToString)
                    '        End If
                    '        rtbDocument.Rtf = strDataName
                    '    End If
                    'Next

                    If dtRow.Length > 0 Then
                        For j As Integer = 0 To dtRow(intNumber).Table.Columns.Count - 1
                            StrCol = dtRow(intNumber).Table.Columns(j).ColumnName
                            If rtbDocument.Rtf.ToLower.Contains("#" & StrCol.ToLower & "#") Then
                                'Sohail (17 Feb 2021) - [.ToLower]
                                rtbDocument.Focus()
                                If StrCol.Trim.ToUpper = "PASSWORD" Then
                                    If blnIsMailSending = False Then
                                        'Sohail (17 Feb 2021) -- Start
                                        'NMB Enhancement : OLD-322 : Addition of Loan Parameters Field on Letter Templates.
                                        'strDataName = rtbDocument.Rtf.Replace("#" & StrCol & "#", dtRow(intNumber).Item(StrCol).ToString)
                                        strDataName = Microsoft.VisualBasic.Strings.Replace(rtbDocument.Rtf, "#" & StrCol & "#", dtRow(intNumber).Item(StrCol).ToString, , , CompareMethod.Text)
                                        'Sohail (17 Feb 2021) -- End
                                    ElseIf blnIsMailSending = True Then
                                        'Sohail (17 Feb 2021) -- Start
                                        'NMB Enhancement : OLD-322 : Addition of Loan Parameters Field on Letter Templates.
                                        'strDataName = rtbDocument.Rtf.Replace("#" & StrCol & "#", dtRow(intNumber).Item("Original_Password").ToString)
                                        strDataName = Microsoft.VisualBasic.Strings.Replace(rtbDocument.Rtf, "#" & StrCol & "#", dtRow(intNumber).Item("Original_Password").ToString, , , CompareMethod.Text)
                                        'Sohail (17 Feb 2021) -- End
                                    End If
                                    'S.SANDEEP [05-Apr-2018] -- START
                                    'ISSUE/ENHANCEMENT : {Ref#216|#ARUTI-102}
                                ElseIf StrCol.Trim.ToUpper = "COMPANYLOGO" Then
                                    Dim data As Byte() = CType(dtRow(0).Item(StrCol), Byte())
                                    Dim ms As MemoryStream = New MemoryStream(data)
                                    Dim img As Image = Image.FromStream(ms)
                                    Dim strValue As String = embedImage(img)
                                    'Sohail (17 Feb 2021) -- Start
                                    'NMB Enhancement : OLD-322 : Addition of Loan Parameters Field on Letter Templates.
                                    'strDataName = rtbDocument.Rtf.Replace("#" & StrCol & "#", strValue)
                                    strDataName = Microsoft.VisualBasic.Strings.Replace(rtbDocument.Rtf, "#" & StrCol & "#", strValue, , , CompareMethod.Text)
                                    'Sohail (17 Feb 2021) -- End
                                ElseIf StrCol.Trim.ToUpper = "KEY DUTIES AND RESPONSIBILITIES" Then
                                    'Sohail (17 Feb 2021) -- Start
                                    'NMB Enhancement : OLD-322 : Addition of Loan Parameters Field on Letter Templates.
                                    'strDataName = rtbDocument.Rtf.Replace("#" & StrCol & "#", dtRow(intNumber).Item(StrCol).ToString.Replace("\r\n", "\line"))
                                    strDataName = Microsoft.VisualBasic.Strings.Replace(rtbDocument.Rtf, "#" & StrCol & "#", dtRow(intNumber).Item(StrCol).ToString.Replace("\r\n", "\line"), , , CompareMethod.Text)
                                    'Sohail (17 Feb 2021) -- End
                                Else
                                    'Sohail (17 Feb 2021) -- Start
                                    'NMB Enhancement : OLD-322 : Addition of Loan Parameters Field on Letter Templates.
                                    'strDataName = rtbDocument.Rtf.Replace("#" & StrCol & "#", dtRow(intNumber).Item(StrCol).ToString)
                                    If TypeOf dtRow(0).Item(StrCol) Is Decimal Then
                                        strDataName = Microsoft.VisualBasic.Strings.Replace(rtbDocument.Rtf, "#" & StrCol & "#", Format(dtRow(intNumber).Item(StrCol), GUI.fmtCurrency), , , CompareMethod.Text)
                                    Else
                                        strDataName = Microsoft.VisualBasic.Strings.Replace(rtbDocument.Rtf, "#" & StrCol & "#", dtRow(intNumber).Item(StrCol).ToString, , , CompareMethod.Text)
                                    End If
                                    'Sohail (17 Feb 2021) -- End
                                End If
                                'S.SANDEEP [05-Apr-2018] -- END

                                rtbDocument.Rtf = strDataName
                            End If
                        Next
                    End If
                    'S.SANDEEP [06-MAR-2017] -- END

                End If

                'Pinkal (18-Sep-2012) -- End

                'Sohail (20 Jan 2021) -- Start
                'NMB Enhancement : - Allow calculation in letter template.
                Dim arr As MatchCollection = Regex.Matches(rtbDocument.Rtf, "<calc[^>]*>(?<TEXT>[^<]*)<\/calc>", RegexOptions.IgnoreCase)
                For Each s As RegularExpressions.Match In arr
                    Dim dt As New DataTable
                    Dim ans As Object = Nothing
                    Try
                        ans = dt.Compute(s.Groups("TEXT").ToString.Replace(",", ""), "")
                    Catch ex As Exception

                    End Try
                    If ans Is Nothing Then
                        ans = 0
                End If
                    Dim sreplace As String = Microsoft.VisualBasic.Strings.Replace(rtbDocument.Rtf, "<calc>" & s.Groups("TEXT").ToString & "</calc>", Format(ans, "###,###,###,##0.00"), , , CompareMethod.Text)
                    rtbDocument.Rtf = sreplace
                Next
                'Sohail (20 Jan 2021) -- End

                'S.SANDEEP |13-FEB-2021| -- START
                'ISSUE/ENHANCEMENT : BONUS LETTER
                arr = Nothing
                arr = Regex.Matches(rtbDocument.Rtf, "<hcode[^>]*>(?<TEXT>[^<]*)<\/hcode>", RegexOptions.IgnoreCase)
                If arr IsNot Nothing AndAlso arr.Count > 0 Then
                    Dim objPR As New clsPayrollProcessTran
                    Dim objMD As New clsMasterData
                    Dim objPD As New clscommom_period_Tran
                    Dim objCS As New clsComputeScore_master
                    Dim intPeriodId As Integer = -1
                    For Each s As RegularExpressions.Match In arr
                        intPeriodId = objMD.getFirstPeriodID(enModuleReference.Payroll, FinancialYear._Object._YearUnkid, 1, False, False)
                        '*********************************** PAYROLL AMOUNT START
                        Dim amt As Decimal = 0
                        If intPeriodId > 0 Then
                            objPD._Periodunkid(FinancialYear._Object._DatabaseName) = intPeriodId
                            Dim dsAmt As New DataSet
                            dsAmt = objPR.GetList(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, objPD._Start_Date, objPD._End_Date, ConfigParameter._Object._UserAccessModeSetting, True, False, "List", CInt(dtRow(0)("EmpId")), intPeriodId, 0, False, "prtranhead_master.trnheadcode = '" & s.Groups("TEXT").ToString() & "' ", "", False, "")
                            If dsAmt IsNot Nothing AndAlso dsAmt.Tables.Count > 0 Then
                                If dsAmt.Tables(0).Rows.Count > 0 Then
                                    amt = CDec(dsAmt.Tables(0).Rows(0)("amount"))
                                End If
                            End If                            
                        End If
                        Dim sreplace As String = Microsoft.VisualBasic.Strings.Replace(rtbDocument.Rtf, "<hcode>" & s.Groups("TEXT").ToString & "</hcode>", Format(amt, "###,###,###,##0.00"), , , CompareMethod.Text)
                        rtbDocument.Rtf = sreplace
                        '*********************************** PAYROLL AMOUNT END

                        '*********************************** PERFORMANCE RATING START
                        intPeriodId = -1
                        Dim strRating As String = ""
                        intPeriodId = objMD.getFirstPeriodID(enModuleReference.Assessment, FinancialYear._Object._YearUnkid, 1, True, False)
                        If intPeriodId > 0 Then                            
                            strRating = objCS.GetRating(CInt(dtRow(0)("EmpId")), intPeriodId, ConfigParameter._Object._IsCalibrationSettingActive)
                        End If
                        sreplace = Microsoft.VisualBasic.Strings.Replace(rtbDocument.Rtf, "<rating>", strRating, , , CompareMethod.Text)
                        rtbDocument.Rtf = sreplace
                        '*********************************** PERFORMANCE RATING END
                    Next
                    objPR = Nothing
                    objMD = Nothing
                    objPD = Nothing
                    objCS = Nothing
                End If
                'S.SANDEEP |13-FEB-2021| -- END

            Next
            AddHandler rtbDocument.TextChanged, AddressOf rtbDocument_TextChanged
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "ViewMergeData", mstrModuleName)
        End Try
    End Sub

    Private Sub CallFrom(ByVal intModuleRefId As Integer)
        'Sohail (17 Feb 2021) -- Start
        'NMB Enhancement : OLD-322 : Addition of Loan Parameters Field on Letter Templates.
        mblnSendEachTranByUnkId = False
        mstrLoanAdvanceTranIds = ""
        'Sohail (17 Feb 2021) -- End
        Try
            If dsList Is Nothing Then dsList = New DataSet
            Select Case intModuleRefId
                Case enImg_Email_RefId.Employee_Module
                    If mblnIsFromDiary = False Then
                        Dim objFrm As New frmEmployeeList
                        If User._Object._Isrighttoleft = True Then
                            objFrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                            objFrm.RightToLeftLayout = True
                            Call Language.ctlRightToLeftlayOut(objFrm)
                        End If
                        objFrm._IsFromMail = True
                        If dsList.Tables.Count > 0 Then
                            objFrm._DataView = dsList
                        End If
                        objFrm.objFooter.Visible = False
                        objFrm.ShowDialog()
                        dsList = objFrm._DataView
                        txtToAddress.Text = ""
                        If objFrm.EmailList IsNot Nothing Then
                            For Each str1 As String In objFrm.EmailList
                                If str1 Is Nothing Then
                                    Exit For
                                End If
                                If txtToAddress.Text.Contains(str1) Then
                                    Continue For
                                Else
                                    txtToAddress.Text &= str1 & " , "
                                End If
                            Next
                        End If
                    End If
                    mblnIsFromPaySlip = False
                    mblnIsFromSickSheet = False
                    If txtToAddress.Text.Trim.Length > 0 Then
                        btnEmployeeDocument.Visible = True
                    End If

                Case enImg_Email_RefId.Applicant_Module
                    Dim objFrm As New frmApplicantList
                    If User._Object._Isrighttoleft = True Then
                        objFrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                        objFrm.RightToLeftLayout = True
                        Call Language.ctlRightToLeftlayOut(objFrm)
                    End If
                    If dsList.Tables.Count > 0 Then
                        objFrm._DataView = dsList
                    End If

                    objFrm._IsFromMail = True

                    objFrm.objFooter.Visible = False
                    objFrm.ShowDialog()
                    dsList = objFrm._DataView
                    txtToAddress.Text = ""
                    If objFrm.EmailList IsNot Nothing Then
                        For Each str1 As String In objFrm.EmailList
                            If str1 Is Nothing Then
                                Exit For
                            End If
                            If txtToAddress.Text.Contains(str1) Then
                                Continue For
                            Else
                                txtToAddress.Text &= str1 & " , "
                            End If
                        Next
                    End If
                    mblnIsFromPaySlip = False
                    mblnIsFromSickSheet = False
                    btnEmployeeDocument.Visible = False
                    If objFrm.EmailList IsNot Nothing Then
                        For Each lvItem As ListViewItem In lvAttachedFile.Items
                            If lvItem.SubItems(objcolhIsEmpDoc.Index).Text = "True" Then lvItem.Remove()
                        Next
                    End If

                Case enImg_Email_RefId.Leave_Module
                    Dim frm As New frmLeaveFormList
                    If User._Object._Isrighttoleft = True Then
                        frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                        frm.RightToLeftLayout = True
                        Call Language.ctlRightToLeftlayOut(frm)
                    End If
                    frm.objFooter.Visible = False
                    frm.objEFooter.Visible = True
                    frm._IsFromMail = True
                    frm.ShowDialog()
                    dsList = frm._Email_Data
                    mstrLeaveFormIds = frm.LeaveFormIDs
                    txtToAddress.Text = ""
                    If frm._Email_Data IsNot Nothing Then
                        For Each dtRow As DataRow In dsList.Tables(0).Rows
                            If txtToAddress.Text.Contains(dtRow.Item("Email").ToString.Trim) Then
                                Continue For
                            Else
                                txtToAddress.Text &= dtRow.Item("Email").ToString.Trim & ","
                            End If
                        Next
                    End If
                    mblnIsFromPaySlip = False
                    mblnIsFromSickSheet = False

                Case enImg_Email_RefId.Payroll_Module
                    Dim frm As New frmPayslipList
                    If User._Object._Isrighttoleft = True Then
                        frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                        frm.RightToLeftLayout = True
                        Call Language.ctlRightToLeftlayOut(frm)
                    End If
                    frm.objFooter.Visible = False
                    frm.objEFooter.Visible = True
                    frm.ShowDialog()
                    dsList = frm._Email_Data
                    mstrPeriodIds = frm._PeriodUnkids
                    mdtPeriodStart = frm._PeriodStart_EPayslip
                    mdtPeriodEnd = frm._PeriodEnd_EPayslip
                    txtToAddress.Text = ""
                    If dsList.Tables.Count > 0 Then
                        For Each dtRow As DataRow In dsList.Tables(0).Rows
                            If txtToAddress.Text.Contains(dtRow.Item("Email").ToString.Trim) Then
                                Continue For
                            Else
                                txtToAddress.Text &= dtRow.Item("Email").ToString.Trim & ","
                            End If
                        Next
                    End If
                    mblnIsFromPaySlip = True
                    mblnIsFromSickSheet = False

                Case enImg_Email_RefId.Medical_Module
                    Dim frm As New frmSickSheetList
                    If User._Object._Isrighttoleft = True Then
                        frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                        frm.RightToLeftLayout = True
                        Call Language.ctlRightToLeftlayOut(frm)
                    End If
                    frm.objFooter.Visible = False
                    frm.objEFooter.Visible = True
                    frm._IsFromMail = True
                    frm.ShowDialog()
                    dsList = frm._Email_Data
                    mstrSickSheeIDs = frm.SickSheetIDs
                    txtToAddress.Text = ""
                    If dsList.Tables.Count > 0 Then
                        For Each dtRow As DataRow In dsList.Tables(0).Rows
                            If txtToAddress.Text.Contains(dtRow.Item("Email").ToString.Trim) Then
                                Continue For
                            Else
                                txtToAddress.Text &= dtRow.Item("Email").ToString.Trim & ","
                            End If
                        Next
                    End If
                    mblnIsFromPaySlip = False
                    mblnIsFromSickSheet = True

                Case enImg_Email_RefId.Appraisal_Module
                    Dim frm As New frmAppraisals
                    If User._Object._Isrighttoleft = True Then
                        frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                        frm.RightToLeftLayout = True
                        Call Language.ctlRightToLeftlayOut(frm)
                    End If
                    frm._IsFromMail = True
                    frm.ShowDialog()
                    dsList = frm._EmailData
                    txtToAddress.Text = ""
                    If frm.EmailList IsNot Nothing Then
                        For Each str1 As String In frm.EmailList
                            If str1 Is Nothing Then
                                Exit For
                            End If
                            If txtToAddress.Text.Contains(str1) Then
                                Continue For
                            Else
                                txtToAddress.Text &= str1 & " , "
                            End If
                        Next
                    End If
                    mblnIsFromPaySlip = False
                    btnEmployeeDocument.Visible = True
                Case enImg_Email_RefId.Discipline_Module
                    Dim frm As New frmDisciplineFilingList
                    If User._Object._Isrighttoleft = True Then
                        frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                        frm.RightToLeftLayout = True
                        Call Language.ctlRightToLeftlayOut(frm)
                    End If
                    frm._FromMail = True
                    frm.ShowDialog()
                    dsList = frm._EmailData
                    txtToAddress.Text = ""
                    If frm._Emails IsNot Nothing Then
                        For Each str1 As String In frm._Emails
                            If str1 Is Nothing Then
                                Exit For
                            End If
                            txtToAddress.Text &= str1 & " , "
                        Next
                    End If
                    mdicDiscpline = frm._dicDisciplineFile
                    frm = Nothing

                Case enImg_Email_RefId.TimeAndAttendance
                    Dim frm As New frmEmailDailyTimesheet
                    If User._Object._Isrighttoleft = True Then
                        frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                        frm.RightToLeftLayout = True
                        Call Language.ctlRightToLeftlayOut(frm)
                    End If
                    Dim strEmailAdress As String = String.Empty
                    If frm.displayDialog(strEmailAdress, mdtReportFromDate, mdtReportToDate, dsList) Then
                        mblnIsFromReport = True
                        txtToAddress.Text = strEmailAdress
                    End If
                    mblnIsFromPaySlip = False
                    mblnIsFromSickSheet = False

                    'Sohail (17 Feb 2021) -- Start
                    'NMB Enhancement : OLD-322 : Addition of Loan Parameters Field on Letter Templates.
                Case enImg_Email_RefId.Loan_Balance
                    Dim frm As New frmNewLoanAdvanceList
                    If User._Object._Isrighttoleft = True Then
                        frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                        frm.RightToLeftLayout = True
                        Call Language.ctlRightToLeftlayOut(frm)
                    End If
                    frm.objFooter.Visible = False
                    frm.objEFooter.Visible = True
                    frm._IsFromMail = True
                    frm.ShowDialog()
                    dsList = frm._Email_Data
                    mstrLoanAdvanceTranIds = frm._LoanAdvanceTranUnkids
                    txtToAddress.Text = ""
                    If dsList.Tables.Count > 0 Then
                        For Each dtRow As DataRow In dsList.Tables(0).Rows
                            If txtToAddress.Text.Contains(dtRow.Item("Email").ToString.Trim) Then
                                Continue For
                            Else
                                txtToAddress.Text &= dtRow.Item("Email").ToString.Trim & ","
                            End If
                        Next
                    End If
                    mblnIsFromPaySlip = False
                    mblnIsFromSickSheet = False
                    'Sohail (17 Feb 2021) -- End

            End Select
            mintCallingRefId = intModuleRefId
            'Sohail (17 Feb 2021) -- Start
            'NMB Enhancement : OLD-322 : Addition of Loan Parameters Field on Letter Templates.
            If mstrLoanAdvanceTranIds.Trim <> "" Then
                mblnSendEachTranByUnkId = True
            End If
            'Sohail (17 Feb 2021) -- End
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "CallFrom", mstrModuleName)
        End Try

    End Sub

    Private Sub Export_EPayslip()
        Dim objPaySlpReport As New ArutiReports.clsPaySlip(User._Object._Languageunkid, Company._Object._Companyunkid)
        Try
            Dim strReportName As String = String.Empty
            Dim strOriginalReportName As String = String.Empty
            Cursor.Current = Cursors.WaitCursor

            objPaySlpReport._AnalysisRefId = enAnalysisRef.Employee
            objPaySlpReport._PeriodIds = mstrPeriodIds
            objPaySlpReport._PeriodStart = mdtPeriodStart
            objPaySlpReport._PeriodEnd = mdtPeriodEnd

            'Sohail (21 May 2016) -- Start
            'Enhancement - 60.1 - Show Period wise allocations on Payslip Report.
            Dim dicPeriod As New Dictionary(Of Integer, String)
            For Each strId As String In mstrPeriodIds.Split(CChar(","))
                Dim objPeriod As New clscommom_period_Tran
                objPeriod._Periodunkid(FinancialYear._Object._DatabaseName) = CInt(strId)
                If dicPeriod.ContainsKey(CInt(strId)) = False Then
                    dicPeriod.Add(CInt(strId), eZeeDate.convertDate(objPeriod._End_Date))
                End If
            Next
            objPaySlpReport._Dic_Period = dicPeriod
            objPaySlpReport._IgnoreZeroValueHead = ConfigParameter._Object._IgnoreZeroValueHeadsOnPayslip
            objPaySlpReport._ShowLoanBalance = ConfigParameter._Object._ShowLoanBalanceOnPayslip
            objPaySlpReport._ShowSavingBalance = ConfigParameter._Object._ShowSavingBalanceOnPayslip
            objPaySlpReport._ShowEmployerContribution = ConfigParameter._Object._ShowEmployerContributionOnPayslip
            objPaySlpReport._ShowAllHeads = ConfigParameter._Object._ShowAllHeadsOnPayslip
            objPaySlpReport._ShowSalaryOnHold = ConfigParameter._Object._ShowSalaryOnHoldOnPayslip
            objPaySlpReport._LeaveBalanceSetting = ConfigParameter._Object._LeaveBalanceSetting
            objPaySlpReport._ShowBirthDate = ConfigParameter._Object._ShowBirthDateOnPayslip
            objPaySlpReport._LeaveTypeId = ConfigParameter._Object._LeaveTypeOnPayslip
            objPaySlpReport._ShowAge = ConfigParameter._Object._ShowAgeOnPayslip
            objPaySlpReport._ShowCategory = ConfigParameter._Object._ShowCategoryOnPayslip
            objPaySlpReport._ShowPaymentDetails = ConfigParameter._Object._ShowPaymentDetailsOnPayslip
            objPaySlpReport._ShowNo_of_Dependents = ConfigParameter._Object._ShowNoOfDependantsOnPayslip
            objPaySlpReport._ShowCompanyName = ConfigParameter._Object._ShowCompanyNameOnPayslip
            objPaySlpReport._ShowCumulativeAccrual = ConfigParameter._Object._ShowCumulativeAccrualOnPayslip
            objPaySlpReport._ShowInformationalHeads = ConfigParameter._Object._ShowInformationalHeadsOnPayslip
            objPaySlpReport._ShowMembership = ConfigParameter._Object._ShowMembershipOnPayslip
            objPaySlpReport._ShowMonthlySalary = ConfigParameter._Object._ShowMonthlySalaryOnPayslip
            'Pinkal (18-Nov-2016) -- Start
            'Issue :  issue # 0002322
            'Enhancement - Working on IBA Leave Requirement as per Mr.Rutta's Guidance.
            objPaySlpReport._LeaveAccrueTenureSetting = ConfigParameter._Object._LeaveAccrueTenureSetting
            objPaySlpReport._LeaveAccrueDaysAfterEachMonth = ConfigParameter._Object._LeaveAccrueDaysAfterEachMonth
            'Pinkal (18-Nov-2016) -- End

            'Nilay (23 Jan 2017) -- Start
            'CCBRT Enhancement - 65.1 - Show Statutory Message on ESS Payslip.
            objPaySlpReport._InternalUseMessage = ConfigParameter._Object._StatutoryMessageOnPayslipOnESS
            'Nilay (23 Jan 2017) -- End

            'Anjan [01 September 2016] -- Start
            'ENHANCEMENT : Including Payslip settings on configuration.
            objPaySlpReport._ShowBankAccountNo = ConfigParameter._Object._ShowBankAccountNoOnPayslip
            'Anjan [01 Sepetember 2016] -- End


            Select Case ConfigParameter._Object._PayslipTemplate
                Case 2 'TRA Format
                    objPaySlpReport._IsLogoCompanyInfo = False
                    objPaySlpReport._IsOnlyCompanyInfo = False
                    objPaySlpReport._IsOnlyLogo = True
                Case Else
                    Select Case ConfigParameter._Object._LogoOnPayslip
                        Case enLogoOnPayslip.LOGO_AND_COMPANY_DETAILS
                            objPaySlpReport._IsLogoCompanyInfo = True
                            objPaySlpReport._IsOnlyCompanyInfo = False
                            objPaySlpReport._IsOnlyLogo = False
                        Case enLogoOnPayslip.COMPANY_DETAILS
                            objPaySlpReport._IsLogoCompanyInfo = False
                            objPaySlpReport._IsOnlyCompanyInfo = True
                            objPaySlpReport._IsOnlyLogo = False
                        Case enLogoOnPayslip.LOGO
                            objPaySlpReport._IsLogoCompanyInfo = False
                            objPaySlpReport._IsOnlyCompanyInfo = False
                            objPaySlpReport._IsOnlyLogo = True
                    End Select
            End Select

            strReportName = objPaySlpReport._ReportName
            strOriginalReportName = strReportName

            If System.IO.Directory.Exists(mstrExportEPaySlipPath) = False Then
                System.IO.Directory.CreateDirectory(mstrExportEPaySlipPath)
            End If

            For Each dtRow As DataRow In dsList.Tables(0).Rows
                objPaySlpReport._EmployeeIds = dtRow.Item("EmpId").ToString
                strReportName = dtRow.Item("EmpId").ToString & "_" & objPaySlpReport._ReportName.Replace(" ", "_")
                strReportName = strReportName & ConfigParameter._Object._CurrentDateAndTime.ToString("yyymmddhhmmss")
                objPaySlpReport.setDefaultOrderBy(0)
                'Sohail (21 Aug 2015) -- Start
                'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                'objPaySlpReport.Send_EPayslip(strReportName, mstrExportEPaySlipPath)
                'Sohail (26 Feb 2016) -- Start
                'Enhancement - Email Payslip to have password protection (request from KBC and CCK) (41# KBC & Kenya Project Comments List.xls)
                'objPaySlpReport.Send_EPayslip(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, mdtPeriodStart, mdtPeriodEnd, ConfigParameter._Object._UserAccessModeSetting, True, True, True, strReportName, mstrExportEPaySlipPath, ConfigParameter._Object._CurrencyFormat, ConfigParameter._Object._PayslipTemplate, FinancialYear._Object._Database_Start_Date, FinancialYear._Object._Database_End_Date, ConfigParameter._Object._HourAmtMapping, ConfigParameter._Object._ShowBranchOnPayslip, ConfigParameter._Object._ShowDepartmentGroupOnPayslip, ConfigParameter._Object._ShowDepartmentOnPayslip, ConfigParameter._Object._ShowSectionGroupOnPayslip, ConfigParameter._Object._ShowSectionOnPayslip, ConfigParameter._Object._ShowUnitGroupOnPayslip, ConfigParameter._Object._ShowUnitOnPayslip, ConfigParameter._Object._ShowTeamOnPayslip, ConfigParameter._Object._ShowClassGroupOnPayslip, ConfigParameter._Object._ShowClassOnPayslip, ConfigParameter._Object._ActualHeadId, ConfigParameter._Object._AmtToBePaidHeadId, ConfigParameter._Object._EmplCostHeadId, ConfigParameter._Object._WorkingDaysHeadId, ConfigParameter._Object._TaxableAmountHeadId, ConfigParameter._Object._IPRCalculationHeadId, ConfigParameter._Object._13thPayHeadId, ConfigParameter._Object._NoOfDependantHeadId, ConfigParameter._Object._NoOfChildHeadId, ConfigParameter._Object._RoundOff_Type, Company._Object._Stamp, Company._Object._Name, User._Object._Username, ConfigParameter._Object._ShowCompanyStampOnPayslip, ConfigParameter._Object._ShowJobOnPayslip, ConfigParameter._Object._ShowGradeOnPayslip, ConfigParameter._Object._ShowGradeGroupOnPayslip, ConfigParameter._Object._ShowGradeLevelOnPayslip, ConfigParameter._Object._ShowAppointmentDateOnPayslip, ConfigParameter._Object._ShowRetirementDateOnPayslip, ConfigParameter._Object._ShowEOCDateOnPayslip, ConfigParameter._Object._ShowLeavingDateOnPayslip, ConfigParameter._Object._ShowCompanyNameOnPayslip, ConfigParameter._Object._ShowEmployeeCodeOnPayslip, ConfigParameter._Object._ShowMembershipOnPayslip, ConfigParameter._Object._ShowPaymentDetailsOnPayslip, Company._Object._Company_Reg_No, Company._Object._Tinno, Company._Object._Nssfno, Company._Object._Registerdno, Company._Object._Address1, Company._Object._Address2, Company._Object._State_Name, Company._Object._City_Name, Company._Object._Country_Name, Company._Object._Phone1, Company._Object._Email, Company._Object._Website, ConfigParameter._Object._ShowExchangeRateOnPayslip)
                'Sohail (04 Aug 2016) -- Start
                'Enhancement - 63.1 - Option on configuration to Send Password protected E-Payslip.
                'objPaySlpReport.Send_EPayslip(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, mdtPeriodStart, mdtPeriodEnd, ConfigParameter._Object._UserAccessModeSetting, True, True, True, strReportName, mstrExportEPaySlipPath, ConfigParameter._Object._CurrencyFormat, ConfigParameter._Object._PayslipTemplate, FinancialYear._Object._Database_Start_Date, FinancialYear._Object._Database_End_Date, ConfigParameter._Object._HourAmtMapping, ConfigParameter._Object._ShowBranchOnPayslip, ConfigParameter._Object._ShowDepartmentGroupOnPayslip, ConfigParameter._Object._ShowDepartmentOnPayslip, ConfigParameter._Object._ShowSectionGroupOnPayslip, ConfigParameter._Object._ShowSectionOnPayslip, ConfigParameter._Object._ShowUnitGroupOnPayslip, ConfigParameter._Object._ShowUnitOnPayslip, ConfigParameter._Object._ShowTeamOnPayslip, ConfigParameter._Object._ShowClassGroupOnPayslip, ConfigParameter._Object._ShowClassOnPayslip, ConfigParameter._Object._ActualHeadId, ConfigParameter._Object._AmtToBePaidHeadId, ConfigParameter._Object._EmplCostHeadId, ConfigParameter._Object._WorkingDaysHeadId, ConfigParameter._Object._TaxableAmountHeadId, ConfigParameter._Object._IPRCalculationHeadId, ConfigParameter._Object._13thPayHeadId, ConfigParameter._Object._NoOfDependantHeadId, ConfigParameter._Object._NoOfChildHeadId, ConfigParameter._Object._RoundOff_Type, Company._Object._Stamp, Company._Object._Name, User._Object._Username, ConfigParameter._Object._ShowCompanyStampOnPayslip, ConfigParameter._Object._ShowJobOnPayslip, ConfigParameter._Object._ShowGradeOnPayslip, ConfigParameter._Object._ShowGradeGroupOnPayslip, ConfigParameter._Object._ShowGradeLevelOnPayslip, ConfigParameter._Object._ShowAppointmentDateOnPayslip, ConfigParameter._Object._ShowRetirementDateOnPayslip, ConfigParameter._Object._ShowEOCDateOnPayslip, ConfigParameter._Object._ShowLeavingDateOnPayslip, ConfigParameter._Object._ShowCompanyNameOnPayslip, ConfigParameter._Object._ShowEmployeeCodeOnPayslip, ConfigParameter._Object._ShowMembershipOnPayslip, ConfigParameter._Object._ShowPaymentDetailsOnPayslip, Company._Object._Company_Reg_No, Company._Object._Tinno, Company._Object._Nssfno, Company._Object._Registerdno, Company._Object._Address1, Company._Object._Address2, Company._Object._State_Name, Company._Object._City_Name, Company._Object._Country_Name, Company._Object._Phone1, Company._Object._Email, Company._Object._Website, ConfigParameter._Object._ShowExchangeRateOnPayslip, dtRow.Item("Password").ToString)
                'Sohail (20 Sep 2016) -- Start
                'Enhancement - 63.1 - New Payslip Template 15 for TIMAJA.
                'objPaySlpReport.Send_EPayslip(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, mdtPeriodStart, mdtPeriodEnd, ConfigParameter._Object._UserAccessModeSetting, True, True, True, strReportName, mstrExportEPaySlipPath, ConfigParameter._Object._CurrencyFormat, ConfigParameter._Object._PayslipTemplate, FinancialYear._Object._Database_Start_Date, FinancialYear._Object._Database_End_Date, ConfigParameter._Object._HourAmtMapping, ConfigParameter._Object._ShowBranchOnPayslip, ConfigParameter._Object._ShowDepartmentGroupOnPayslip, ConfigParameter._Object._ShowDepartmentOnPayslip, ConfigParameter._Object._ShowSectionGroupOnPayslip, ConfigParameter._Object._ShowSectionOnPayslip, ConfigParameter._Object._ShowUnitGroupOnPayslip, ConfigParameter._Object._ShowUnitOnPayslip, ConfigParameter._Object._ShowTeamOnPayslip, ConfigParameter._Object._ShowClassGroupOnPayslip, ConfigParameter._Object._ShowClassOnPayslip, ConfigParameter._Object._ActualHeadId, ConfigParameter._Object._AmtToBePaidHeadId, ConfigParameter._Object._EmplCostHeadId, ConfigParameter._Object._WorkingDaysHeadId, ConfigParameter._Object._TaxableAmountHeadId, ConfigParameter._Object._IPRCalculationHeadId, ConfigParameter._Object._13thPayHeadId, ConfigParameter._Object._NoOfDependantHeadId, ConfigParameter._Object._NoOfChildHeadId, ConfigParameter._Object._RoundOff_Type, Company._Object._Stamp, Company._Object._Name, User._Object._Username, ConfigParameter._Object._ShowCompanyStampOnPayslip, ConfigParameter._Object._ShowJobOnPayslip, ConfigParameter._Object._ShowGradeOnPayslip, ConfigParameter._Object._ShowGradeGroupOnPayslip, ConfigParameter._Object._ShowGradeLevelOnPayslip, ConfigParameter._Object._ShowAppointmentDateOnPayslip, ConfigParameter._Object._ShowRetirementDateOnPayslip, ConfigParameter._Object._ShowEOCDateOnPayslip, ConfigParameter._Object._ShowLeavingDateOnPayslip, ConfigParameter._Object._ShowCompanyNameOnPayslip, ConfigParameter._Object._ShowEmployeeCodeOnPayslip, ConfigParameter._Object._ShowMembershipOnPayslip, ConfigParameter._Object._ShowPaymentDetailsOnPayslip, Company._Object._Company_Reg_No, Company._Object._Tinno, Company._Object._Nssfno, Company._Object._Registerdno, Company._Object._Address1, Company._Object._Address2, Company._Object._State_Name, Company._Object._City_Name, Company._Object._Country_Name, Company._Object._Phone1, Company._Object._Email, Company._Object._Website, ConfigParameter._Object._ShowExchangeRateOnPayslip, dtRow.Item("Password").ToString, ConfigParameter._Object._SendPasswordProtectedEPayslip)
                objPaySlpReport.Send_EPayslip(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid, _
                                              Company._Object._Companyunkid, mdtPeriodStart, mdtPeriodEnd, ConfigParameter._Object._UserAccessModeSetting, _
                                              True, True, True, strReportName, mstrExportEPaySlipPath, ConfigParameter._Object._CurrencyFormat, _
                                              ConfigParameter._Object._PayslipTemplate, FinancialYear._Object._Database_Start_Date, FinancialYear._Object._Database_End_Date, _
                                              ConfigParameter._Object._HourAmtMapping, ConfigParameter._Object._ShowBranchOnPayslip, _
                                              ConfigParameter._Object._ShowDepartmentGroupOnPayslip, ConfigParameter._Object._ShowDepartmentOnPayslip, _
                                              ConfigParameter._Object._ShowSectionGroupOnPayslip, ConfigParameter._Object._ShowSectionOnPayslip, _
                                              ConfigParameter._Object._ShowUnitGroupOnPayslip, ConfigParameter._Object._ShowUnitOnPayslip, _
                                              ConfigParameter._Object._ShowTeamOnPayslip, ConfigParameter._Object._ShowClassGroupOnPayslip, _
                                              ConfigParameter._Object._ShowClassOnPayslip, ConfigParameter._Object._ActualHeadId, ConfigParameter._Object._AmtToBePaidHeadId, _
                                              ConfigParameter._Object._EmplCostHeadId, ConfigParameter._Object._WorkingDaysHeadId, ConfigParameter._Object._TaxableAmountHeadId, _
                                              ConfigParameter._Object._IPRCalculationHeadId, ConfigParameter._Object._13thPayHeadId, ConfigParameter._Object._NoOfDependantHeadId, _
                                              ConfigParameter._Object._NoOfChildHeadId, ConfigParameter._Object._RoundOff_Type, Company._Object._Stamp, _
                                              Company._Object._Name, User._Object._Username, ConfigParameter._Object._ShowCompanyStampOnPayslip, _
                                              ConfigParameter._Object._ShowJobOnPayslip, ConfigParameter._Object._ShowGradeOnPayslip, ConfigParameter._Object._ShowGradeGroupOnPayslip, _
                                              ConfigParameter._Object._ShowGradeLevelOnPayslip, ConfigParameter._Object._ShowAppointmentDateOnPayslip, _
                                              ConfigParameter._Object._ShowRetirementDateOnPayslip, ConfigParameter._Object._ShowEOCDateOnPayslip, _
                                              ConfigParameter._Object._ShowLeavingDateOnPayslip, ConfigParameter._Object._ShowCompanyNameOnPayslip, _
                                              ConfigParameter._Object._ShowEmployeeCodeOnPayslip, ConfigParameter._Object._ShowMembershipOnPayslip, _
                                              ConfigParameter._Object._ShowPaymentDetailsOnPayslip, Company._Object._Company_Reg_No, Company._Object._Tinno, _
                                              Company._Object._Nssfno, Company._Object._Registerdno, Company._Object._Address1, Company._Object._Address2, _
                                              Company._Object._State_Name, Company._Object._City_Name, Company._Object._Country_Name, Company._Object._Phone1, _
                                              Company._Object._Email, Company._Object._Website, ConfigParameter._Object._ShowExchangeRateOnPayslip, _
                                              dtRow.Item("Password").ToString, ConfigParameter._Object._SendPasswordProtectedEPayslip, _
                                              ConfigParameter._Object._CumulativeTaxHeadId, ConfigParameter._Object._DailyBasicSalaryHeadId, ConfigParameter._Object._AccruedLeavePayHeadId, _
                                              ConfigParameter._Object._LeaveTakenHeadID, ConfigParameter._Object._LeaveAccruedDaysHeadID, _
                                              ConfigParameter._Object._TaxFreeIncomeHeadID, ConfigParameter._Object._AssessableIncomeHeadID, _
                                              ConfigParameter._Object._NAPSATaxHeadID)
                'Nilay (24-Oct-2016) -- [ConfigParameter._Object._NAPSATaxHeadID]
                'Nilay (03-Oct-2016) -- [ConfigParameter._Object._LeaveTakenHeadID, ConfigParameter._Object._LeaveAccruedDaysHeadID, ConfigParameter._Object._TaxFreeIncomeHeadID, ConfigParameter._Object._AssessableIncomeHeadID]
                'Sohail (20 Sep 2016) -- End
                'Sohail (04 Aug 2016) -- End
                'Sohail (26 Feb 2016) -- End
                'Sohail (21 Aug 2015) -- End
                'Sohail (16 May 2012) -- End

                If mdicAttachmentInfo.ContainsKey(CInt(dtRow.Item("EmpId"))) Then
                    mdicAttachmentInfo(CInt(dtRow.Item("EmpId"))) = mdicAttachmentInfo(CInt(dtRow.Item("EmpId"))) & "," & strReportName & ".pdf"
                    'mdicAttachmentInfo(CInt(dtRow.Item("EmpId"))) = mdicAttachmentInfo(CInt(dtRow.Item("EmpId"))) & "," & Regex.Replace(strReportName, "[^0-9a-zA-Z]+", "") & ".pdf"                    
                Else
                    mdicAttachmentInfo.Add(CInt(dtRow.Item("EmpId")), strReportName & ".pdf")
                    'mdicAttachmentInfo.Add(CInt(dtRow.Item("EmpId")), Regex.Replace(strReportName, "[^0-9a-zA-Z]+", "") & ".pdf")
                End If
                objPaySlpReport._ReportName = strOriginalReportName
            Next

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Export_EPayslip", mstrModuleName)
        Finally
            objPaySlpReport = Nothing
            Cursor.Current = Cursors.Default
        End Try
    End Sub

    Private Sub Export_ESickSheet()
        Dim objSickSheetReport As New ArutiReports.clsSickSheetReport
        Try
            Dim strReportName As String = String.Empty
            Dim strOriginalReportName As String = String.Empty

            strReportName = "Sick Sheet"
            strOriginalReportName = strReportName

            If System.IO.Directory.Exists(mstrExportEPaySlipPath) = False Then
                System.IO.Directory.CreateDirectory(mstrExportEPaySlipPath)
            End If

            For Each dtRow As DataRow In dsList.Tables(0).Rows
                objSickSheetReport._SickSheetunkid = dtRow.Item("sicksheetunkid").ToString
                objSickSheetReport._Employeeunkid = dtRow.Item("EmpId").ToString
                strReportName = dtRow.Item("EmpCode").ToString & "_" & strReportName.Replace(" ", "_")
                strReportName &= eZeeDate.convertDate(CDate(dtRow.Item("sicksheetdate").ToString))

                'Pinkal (16-Apr-2016) -- Start
                'Enhancement - Implementing Date Format (DD-MMM-YYYY) as Per Mr.Andrew's Comment.
                'objSickSheetReport.Send_ESickSheet(strReportName, mstrExportEPaySlipPath)
                objSickSheetReport.Send_ESickSheet(eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), strReportName, mstrExportEPaySlipPath)
                'Pinkal (16-Apr-2016) -- End

                If mdicAttachmentInfo.ContainsKey(CInt(dtRow.Item("EmpId"))) Then
                    mdicAttachmentInfo(CInt(dtRow.Item("EmpId"))) = mdicAttachmentInfo(CInt(dtRow.Item("EmpId"))) & "," & strReportName & ".pdf"
                Else
                    mdicAttachmentInfo.Add(CInt(dtRow.Item("EmpId")), strReportName & ".pdf")
                End If
                strReportName = strOriginalReportName
            Next

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Export_ESickSheet", mstrModuleName)
        Finally
            objSickSheetReport = Nothing
        End Try
    End Sub

    Public Function Export_ELeaveForm(ByVal xDatabaseName As String, ByVal xUserUnkid As Integer, ByVal xYearUnkid As Integer _
                                                                , ByVal xCompanyUnkid As Integer, ByVal mdtEmployeeAsonDate As Date, ByVal xOnlyApproved As Boolean _
                                                                , ByVal xUserModeSetting As String, ByVal mstrFormNo As String, ByVal mintEmployeeID As Integer, ByVal mintFormId As Integer _
                                                                , ByVal intLeaveBalanceSetting As Integer, ByVal intYearId As Integer, ByVal mdtdbStartDate As Date _
                                                                , ByVal mdtdbEndDate As Date, ByVal mblnShowLeaveExpense As Boolean) As String

        Dim objLeaveFormReport As New ArutiReports.clsEmployeeLeaveForm(User._Object._Languageunkid, Company._Object._Companyunkid)
        Dim StrPath As String = ""
        Try
            Dim strReportName As String = String.Empty
            Dim strOriginalReportName As String = String.Empty

            strReportName = "Employee Leave Form"
            strOriginalReportName = strReportName

            If System.IO.Directory.Exists(mstrExportEPaySlipPath) = False Then
                System.IO.Directory.CreateDirectory(mstrExportEPaySlipPath)
            End If

            strReportName = mstrFormNo & "_" & strReportName.Replace(" ", "_") & ConfigParameter._Object._CurrentDateAndTime.ToString("yyymmddhhmmss")
            objLeaveFormReport._LeaveFormNo = mstrFormNo
            objLeaveFormReport._LeaveFormId = mintFormId
            objLeaveFormReport._LeaveBalanceSetting = intLeaveBalanceSetting
            objLeaveFormReport._YearId = intYearId
            objLeaveFormReport._Fin_StartDate = mdtdbStartDate
            objLeaveFormReport._Fin_Enddate = mdtdbEndDate
            objLeaveFormReport._ShowLeaveExpense = mblnShowLeaveExpense
            objLeaveFormReport._EmployeeId = mintEmployeeID
            objLeaveFormReport.Send_ELeaveForm(xDatabaseName, xUserUnkid, xYearUnkid _
                                                                    , xCompanyUnkid, mdtEmployeeAsonDate, xOnlyApproved, xUserModeSetting _
                                                                    , strReportName, mstrExportEPaySlipPath)

            If mdicAttachmentInfo.ContainsKey(mintEmployeeID) Then
                mdicAttachmentInfo(mintEmployeeID) = mdicAttachmentInfo(mintEmployeeID) & "," & strReportName & ".pdf"
            Else
                mdicAttachmentInfo.Add(mintEmployeeID, strReportName & ".pdf")
            End If

            StrPath = strReportName.Replace(" ", "_") & ".pdf"
            strReportName = strOriginalReportName

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Export_ELeaveForm", mstrModuleName)
        Finally
            objLeaveFormReport = Nothing
        End Try
        Return StrPath
    End Function

    Private Sub Export_ETimeSheetReport()
        Dim objEmployeeTimeSheet As New ArutiReports.clsEmployeeTimesheetReport(User._Object._Languageunkid, Company._Object._Companyunkid)
        Try
            Dim strReportName As String = String.Empty
            Dim strOriginalReportName As String = String.Empty
            Cursor.Current = Cursors.WaitCursor
            objEmployeeTimeSheet.setDefaultOrderBy(0)

            objEmployeeTimeSheet._FromDate = mdtReportFromDate
            objEmployeeTimeSheet._ToDate = mdtReportToDate
            objEmployeeTimeSheet._DBstartDate = FinancialYear._Object._Database_Start_Date.Date
            objEmployeeTimeSheet._DBEndDate = FinancialYear._Object._Database_End_Date.Date
            objEmployeeTimeSheet._YearId = FinancialYear._Object._YearUnkid
            objEmployeeTimeSheet._IsPolicyManagementTNA = ConfigParameter._Object._PolicyManagementTNA

            If ConfigParameter._Object._EmpTimesheetSetting.Trim.Length > 0 Then
                Dim strSettings() As String = ConfigParameter._Object._EmpTimesheetSetting.Trim.Split(CChar("|"))
                If strSettings.Length > 0 Then
                    objEmployeeTimeSheet._ShowEachEmployeeOnNewPage = CBool(IIf(strSettings(3) = Nothing, False, strSettings(3)))
                    If ConfigParameter._Object._PolicyManagementTNA = False Then
                        objEmployeeTimeSheet._ShowTiming24Hrs = CBool(IIf(strSettings(0) = Nothing, False, strSettings(0)))
                        objEmployeeTimeSheet._ShowHoliday = CBool(IIf(strSettings(1) = Nothing, False, strSettings(1)))
                        objEmployeeTimeSheet._ShowLeaveType = CBool(IIf(strSettings(2) = Nothing, False, strSettings(2)))
                        objEmployeeTimeSheet._ShowBreakTime = CBool(IIf(strSettings(4) = Nothing, False, strSettings(4)))
                        objEmployeeTimeSheet._ShowShift = CBool(IIf(strSettings(5) = Nothing, False, strSettings(5)))
                        objEmployeeTimeSheet._ShowBaseHrs = CBool(IIf(strSettings(6) = Nothing, False, strSettings(6)))
                        objEmployeeTimeSheet._ShowTotalAbsentDays = CBool(IIf(strSettings(7) = Nothing, False, strSettings(7)))
                    End If
                End If
            End If
            strReportName = objEmployeeTimeSheet._ReportName
            strOriginalReportName = strReportName

            If System.IO.Directory.Exists(mstrExportEPaySlipPath) = False Then
                System.IO.Directory.CreateDirectory(mstrExportEPaySlipPath)
            End If

            Dim objEmp As New clsEmployee_Master
            For Each dtRow As DataRow In dsList.Tables(0).Rows
                objEmployeeTimeSheet._EmpId = CInt(dtRow.Item("EmpId"))
                objEmployeeTimeSheet._EmpName = dtRow.Item("Firstname").ToString & " " & dtRow.Item("Othername").ToString & " " & dtRow.Item("Surname").ToString
                strReportName = dtRow.Item("EmpCode").ToString & "_" & objEmployeeTimeSheet._ReportName.Replace(" ", "_")
                strReportName = strReportName & ConfigParameter._Object._CurrentDateAndTime.ToString("yyymmddhhmmss")
                objEmp._Employeeunkid(eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate)) = CInt(dtRow.Item("EmpId"))
                'objEmployeeTimeSheet._Shiftunkid = objEmp._Shiftunkid
                objEmployeeTimeSheet._EmpCode = objEmp._Employeecode
                objEmployeeTimeSheet._DepartmentId = objEmp._Departmentunkid
                Dim objDept As New clsDepartment
                objDept._Departmentunkid = objEmp._Departmentunkid
                objEmployeeTimeSheet._Department = objDept._Name
                objDept = Nothing
                objEmployeeTimeSheet.Send_ETimeSheet(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), ConfigParameter._Object._UserAccessModeSetting, True, strReportName, mstrExportEPaySlipPath)
                If mdicAttachmentInfo.ContainsKey(CInt(dtRow.Item("EmpId"))) Then
                    mdicAttachmentInfo(CInt(dtRow.Item("EmpId"))) = mdicAttachmentInfo(CInt(dtRow.Item("EmpId"))) & "," & strReportName & ".pdf"
                Else
                    mdicAttachmentInfo.Add(CInt(dtRow.Item("EmpId")), strReportName & ".pdf")
                End If
                strReportName = strOriginalReportName
            Next
            objEmp = Nothing
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Export_ETimeSheetReport", mstrModuleName)
        Finally
        End Try
    End Sub



    'Pinkal (27-Oct-2020) -- Start
    'Enhancement St.Jude -   Working on Automatic Attendance download and send Report to Manager.
    Public Function Export_ETnAEarlyLateReport(ByVal xDatabaseName As String, ByVal xUserUnkid As Integer, ByVal xYearUnkid As Integer _
                                                               , ByVal xCompanyUnkid As Integer, ByVal mdtStartDate As Date, ByVal mdtEndDate As Date, ByVal xOnlyApproved As Boolean _
                                                               , ByVal xUserModeSetting As String) As String

        Dim objEmpEarlyLateReport As New ArutiReports.clsLateArrialReport(User._Object._Languageunkid, xCompanyUnkid)
        Dim StrPath As String = ""
        Try
            Dim strReportName As String = String.Empty
            Dim strOriginalReportName As String = String.Empty

            strReportName = "Late Arrival Early Departure Report"
            strOriginalReportName = strReportName

            If System.IO.Directory.Exists(mstrExportEPaySlipPath) = False Then
                System.IO.Directory.CreateDirectory(mstrExportEPaySlipPath)
            End If

            strReportName = strReportName.Replace(" ", "_") & "_" & ConfigParameter._Object._CurrentDateAndTime.ToString("yyymmddhhmmss")
            objEmpEarlyLateReport._YearId = xYearUnkid
            objEmpEarlyLateReport._CompanyUnkId = xCompanyUnkid
            objEmpEarlyLateReport._UserUnkId = xUserUnkid
            objEmpEarlyLateReport._EmployeeID = 0
            objEmpEarlyLateReport._GenerateReportForNotification = True
            objEmpEarlyLateReport.Send_ELateEarlyTiming(xDatabaseName, xUserUnkid, xYearUnkid _
                                                                    , xCompanyUnkid, mdtStartDate, mdtEndDate, xOnlyApproved, xUserModeSetting _
                                                                    , strReportName, mstrExportEPaySlipPath)

            If mdicAttachmentInfo.ContainsKey(CInt(ConfigParameter._Object._CurrentDateAndTime.ToString("yyymmdd"))) Then
                mdicAttachmentInfo(CInt(ConfigParameter._Object._CurrentDateAndTime.ToString("yyymmdd"))) = mdicAttachmentInfo(CInt(ConfigParameter._Object._CurrentDateAndTime.ToString("yyymmdd"))) & "," & strReportName & ".pdf"
            Else
                mdicAttachmentInfo.Add(CInt(ConfigParameter._Object._CurrentDateAndTime.ToString("yyymmdd")), strReportName & ".pdf")
            End If

            StrPath = strReportName.Replace(" ", "_") & ".pdf"
            strReportName = strOriginalReportName

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Export_ETnAEarlyLateReport", mstrModuleName)
        Finally
            objEmpEarlyLateReport = Nothing
        End Try
        Return StrPath
    End Function
    'Pinkal (27-Oct-2020) -- End



    Private Sub SetControlVisibility()
        If mblnIsFromPaySlip = True Then
            Call Export_EPayslip()
        End If
        If mblnIsFromSickSheet Then
            Call Export_ESickSheet()
        End If
        If mblnIsFromReport Then
            Call Export_ETimeSheetReport()
        End If
        pbProgress.Visible = True
        btnSend.Enabled = False : btnViewMerge.Enabled = False
        btnCancel.Enabled = False
        mblnIsSendingMail = True
        bgwSendMail.RunWorkerAsync()
    End Sub

    'NOTE : if you change anything in the procedure then please see the feasiliblity that this change is applicable in LEAVE SEND procedure which is below this procedure.  
    Private Sub Send()
        rtbDocument.Enabled = False
        Dim mstrReceipentUnkid As String = ""
        Dim strMessage As String = ""
        Dim filename As String = ""
        Dim mblnResult As Boolean = False
        Dim blnSend As Boolean = False
        Dim strFailGusetName As String = ""
        Dim objEmailData As New clsEmail_Master
        Try
            mblnIsSendingPayslip = True
            Control.CheckForIllegalCrossThreadCalls = False
            Call SetAddress()
            If lvAttachedFile.Items.Count > 0 Then
                filename = ""
                For Each Item As ListViewItem In lvAttachedFile.Items
                    If IO.File.Exists(Item.Text) Then
                        filename &= "," & Item.Text.ToString
                    End If
                Next
                If filename.Trim.Length > 0 Then filename = Mid(filename, 2)
            End If
            Dim objSendMail As New clsSendMail
            strTextData = rtbDocument.Rtf
            blnIsMailSending = True
            objlblCaption.Text = Language.getMessage(mstrModuleName, 10, "Sending Mail(s).")
            objlblCaption.Visible = True
            'Sohail (17 Feb 2021) -- Start
            'NMB Enhancement : OLD-322 : Addition of Loan Parameters Field on Letter Templates.
            'For i As Integer = 0 To strMailAddess.Length - 1
            'If Trim(strMailAddess(i)) = "" Then Continue For
            'If Not Regex.IsMatch(Trim(strMailAddess(i)), iEmailRegxExpression) Then
            Dim intCount As Integer = 0
            Dim strEmail As String = ""
            If mblnSendEachTranByUnkId = False Then
                intCount = strMailAddess.Length
            Else
                intCount = dsList.Tables(0).Rows.Count
            End If
            If intCount <= 0 Then Exit Try
            For i As Integer = 0 To intCount - 1
                If mblnSendEachTranByUnkId = False Then
                    strEmail = Trim(strMailAddess(i))
                Else
                    strEmail = dsList.Tables(0).Rows(i).Item("email").ToString.Trim
                End If
                If strEmail = "" Then Continue For
                If Not Regex.IsMatch(strEmail, iEmailRegxExpression) Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "The following email Address is invalid :") & """" & strEmail & """" & Language.getMessage(mstrModuleName, 5, "Please enter valid email."), enMsgBoxStyle.Information)
                    Exit Sub
                End If
                'Sohail (17 Feb 2021) -- End
                If dsList.Tables.Count > 0 Then
                    'Sohail (17 Feb 2021) -- Start
                    'NMB Enhancement : OLD-322 : Addition of Loan Parameters Field on Letter Templates.
                    'dtRow = dsList.Tables(0).Select("email = " & "'" & strMailAddess(i).Replace("'", "''") & "'")
                    If mblnSendEachTranByUnkId = False Then
                        dtRow = dsList.Tables(0).Select("email = " & "'" & strEmail.Replace("'", "''") & "'")
                    Else
                        dtRow = Nothing
                        ReDim dtRow(0)
                        dtRow(0) = dsList.Tables(0).Rows(i)
                    End If
                    'Sohail (17 Feb 2021) -- End
                    If dtRow.Length > 0 Then
                        If mdicDiscpline.Keys.Count <= 0 Then
                            'Sohail (17 Feb 2021) -- Start
                            'NMB Enhancement : OLD-322 : Addition of Loan Parameters Field on Letter Templates.
                            'If Not dicMailStatus.ContainsKey(strMailAddess(i)) Then
                            'dicMailStatus.Add(strMailAddess(i), 1)
                            If Not dicMailStatus.ContainsKey(strEmail) Then
                                dicMailStatus.Add(strEmail, 1)
                                'Sohail (17 Feb 2021) -- End
                            Else
                                'Sohail (17 Feb 2021) -- Start
                                'NMB Enhancement : OLD-322 : Addition of Loan Parameters Field on Letter Templates.
                                'Continue For
                                If mblnSendEachTranByUnkId = False Then
                                Continue For
                            End If
                                'Sohail (17 Feb 2021) -- End
                                End If
                        End If
                    End If
                    Call ViewMergeData(i)
                    'Sohail (17 Feb 2021) -- Start
                    'NMB Enhancement : OLD-322 : Addition of Loan Parameters Field on Letter Templates.
                    'objSendMail._ToEmail = strMailAddess(i)
                    objSendMail._ToEmail = strEmail
                    'Sohail (17 Feb 2021) -- End
                    objSendMail._Subject = txtSubject.Text

                    'S.SANDEEP [05-Apr-2018] -- START
                    'ISSUE/ENHANCEMENT : {Ref#216|#ARUTI-102}
                    'Dim strConvertedData As String = objEmailData.rtf2html3(rtbDocument.Rtf, "+H+G+T+CR+I")
                    'objSendMail._Message = strConvertedData

                    Dim htmlOutput = "Document.html"
                    Dim contentUriPrefix = Path.GetFileNameWithoutExtension(htmlOutput)
                    Dim htmlResult = RtfToHtmlConverter.RtfToHtml(rtbDocument.Rtf, contentUriPrefix)
                    objSendMail._ImageContents = htmlResult._Content
                    htmlResult.WriteToFile(htmlOutput)
                    'Sohail (20 Jan 2021) -- Start
                    'NMB Enhancement : - Allow calculation in letter template.
                    'Dim oHtml As String = htmlResult._HTML
                    Dim oHtml As String = htmlResult._HTML.Replace("  ", "&nbsp;&nbsp;").Replace(vbTab, "&#9;")
                    'Sohail (20 Jan 2021) -- End
                    objSendMail._Message = oHtml
                    'S.SANDEEP [05-Apr-2018] -- END



                    'Sohail (30 Nov 2017) -- Start
                    'SUMATRA Enhancement – SUMATRA – issue # 0001669: Additional feature for Exchange Server configuration setting in 70.1.
                    objSendMail._ModuleRefId = clsSendMail.enAT_VIEW_TYPE.PAYROLL_MGT
                    'Sohail (30 Nov 2017) -- End
                    If mblnIsFromPaySlip = False And mblnIsFromSickSheet = False And mblnIsFromReport = False Then
                        If filename = "" Then
                            objSendMail._AttachedFiles = ""
                        Else
                            objSendMail._AttachedFiles = filename
                        End If
                    Else
                        If mdicAttachmentInfo.Keys.Count > 0 Then
                            If mdicAttachmentInfo.ContainsKey(CInt(dtRow(0)("EmpId"))) Then
                                objSendMail._AttachedFiles = mdicAttachmentInfo(CInt(dtRow(0)("EmpId")))
                            Else
                                objSendMail._AttachedFiles = ""
                            End If
                        End If
                    End If
                    If mblnIsFromPaySlip = False And mblnIsFromSickSheet = False And mblnIsFromReport = False Then
                        'Sohail (30 Nov 2017) -- Start
                        'SUMATRA Enhancement – SUMATRA – issue # 0001669: Additional feature for Exchange Server configuration setting in 70.1.
                        'strMessage = objSendMail.SendMail()
                        strMessage = objSendMail.SendMail(Company._Object._Companyunkid)
                        'Sohail (30 Nov 2017) -- End
                    Else
                        'Sohail (30 Nov 2017) -- Start
                        'SUMATRA Enhancement – SUMATRA – issue # 0001669: Additional feature for Exchange Server configuration setting in 70.1.
                        'strMessage = objSendMail.SendMail(True, mstrExportEPaySlipPath)
                        strMessage = objSendMail.SendMail(Company._Object._Companyunkid, True, mstrExportEPaySlipPath)
                        'Sohail (30 Nov 2017) -- End
                    End If
                    If strMessage <> "" Then
                        strFailGusetName &= ", " & dsList.Tables(0).Rows(i)("Email").ToString
                    Else
                        blnSend = True
                    End If
                    If mstrReceipentUnkid = "" Then
                        blnSend = True
                    Else
                        mstrReceipentUnkid &= " , " & dsList.Tables(0).Rows(i)("FirstName").ToString & " " & dsList.Tables(0).Rows(i)("SurName").ToString & "<" & dsList.Tables(0).Rows(i)("Email").ToString & ">" & vbCrLf
                    End If
                    If mstrReceipentId = "" Then
                        mstrReceipentId = CStr(dtRow(0).Item("EmpId"))
                    Else
                        mstrReceipentId &= " , " & CStr(dtRow(0).Item("EmpId"))
                    End If
                    rtbDocument.Rtf = strTextData
                End If
                bgwSendMail.ReportProgress(i)
            Next
            objEmailData._dicDisciplineFiles = mdicDiscpline
            objEmailData._EmailDateTime = ConfigParameter._Object._CurrentDateAndTime
            objEmailData._Istype_of = CChar("O")
            objEmailData._LettertypeUnkId = mintLetterUnkid
            If mintLetterUnkid > 0 Then
                Dim objLetterType As New clsLetterType
                objLetterType._LettertypeUnkId = mintLetterUnkid
                objEmailData._Message = objLetterType._Lettercontent
                objLetterType = Nothing
            Else
                objEmailData._Message = rtbDocument.Rtf
            End If
            objEmailData._Subject = txtSubject.Text
            objEmailData._UserUnkId = User._Object._Userunkid
            Select Case mintCallingRefId
                Case enImg_Email_RefId.Applicant_Module
                    objEmailData._Isapplicant = True
                Case Else
                    objEmailData._Isapplicant = False
            End Select
            If mstrReceipentId.Length > 0 Then
                With objEmailData
                    ._FormName = mstrModuleName
                    ._LoginEmployeeunkid = 0
                    ._ClientIP = getIP()
                    ._HostName = getHostName()
                    ._FromWeb = False
                    ._AuditUserId = User._Object._Userunkid
._CompanyUnkid = Company._Object._Companyunkid
                    ._AuditDate = ConfigParameter._Object._CurrentDateAndTime
                End With
                mblnResult = objEmailData.Insert(mstrReceipentId)
            End If
            If blnSend And strFailGusetName.Length = 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Mail(s) sent successfully!"), enMsgBoxStyle.Information)
            ElseIf blnSend And strFailGusetName.Length > 2 Then
                strFailGusetName = strFailGusetName.Substring(2)
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Failure sending some mails!") & vbCrLf & """" & strFailGusetName & """", enMsgBoxStyle.Information)
            Else
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Failure sending mails!"), enMsgBoxStyle.Information)
            End If
        Catch ex As Exception
            If ex.Message.Contains("Fail") Then
                eZeeMsgBox.Show(ex.Message & " " & Language.getMessage(mstrModuleName, 6, "Please check your Internet settings."), enMsgBoxStyle.Information)
            ElseIf ex.Message.Contains("secure") Then
                eZeeMsgBox.Show(ex.Message & " " & Language.getMessage(mstrModuleName, 7, "Please Uncheck Login using SSL setting."), enMsgBoxStyle.Information)
            Else
                DisplayError.Show("-1", ex.Message, "btnSend_Click", mstrModuleName)
            End If
        Finally
            blnIsMailSending = False
        End Try
    End Sub

    Private Sub LeaveFormSend()
        rtbDocument.Enabled = False
        Dim mstrReceipentUnkid As String = ""
        Dim strMessage As String = ""
        Dim filename As String = ""
        Dim mblnResult As Boolean = False
        Dim blnSend As Boolean = False
        Dim strFailGusetName As String = ""
        Dim objEmailData As New clsEmail_Master
        Try
            Control.CheckForIllegalCrossThreadCalls = False
            Call SetAddress()
            If lvAttachedFile.Items.Count > 0 Then
                filename = ""
                For Each Item As ListViewItem In lvAttachedFile.Items
                    If IO.File.Exists(Item.Text) Then
                        filename &= "," & Item.Text.ToString
                    End If
                Next
                If filename.Trim.Length > 0 Then filename = Mid(filename, 2)
            End If
            Dim objSendMail As New clsSendMail
            strTextData = rtbDocument.Rtf

            blnIsMailSending = True
            objlblCaption.Text = Language.getMessage(mstrModuleName, 10, "Sending Mail(s).")
            objlblCaption.Visible = True


            For i As Integer = 0 To strMailAddess.Length - 1
                If Trim(strMailAddess(i)) = "" Then Continue For
                If Not Regex.IsMatch(Trim(strMailAddess(i)), iEmailRegxExpression) Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "The following email Address is invalid :") & """" & strMailAddess(i) & """" & Language.getMessage(mstrModuleName, 5, "Please enter valid email."), enMsgBoxStyle.Information)
                    Exit Sub
                End If
                If dsList.Tables.Count > 0 Then
                    dtRow = dsList.Tables(0).Select("email = " & "'" & strMailAddess(i).Replace("'", "''") & "'")
                    If dtRow.Length > 0 Then
                        If mdicDiscpline.Keys.Count <= 0 Then
                            If Not dicMailStatus.ContainsKey(strMailAddess(i)) Then
                                dicMailStatus.Add(strMailAddess(i), 1)
                            Else
                                Continue For
                            End If
                        End If
                    End If
                    Dim drRow() As DataRow = dsList.Tables(0).Select("EmpId = " & "'" & dtRow(0)("EmpId").ToString() & "'")
                    If drRow.Length <= 0 Then Continue For
                    For k As Integer = 0 To drRow.Length - 1
                        Call ViewMergeData(k)
                        objSendMail._ToEmail = strMailAddess(i)
                        objSendMail._Subject = txtSubject.Text
                        'S.SANDEEP [05-Apr-2018] -- START
                        'ISSUE/ENHANCEMENT : {Ref#216|#ARUTI-102}
                        'Dim strConvertedData As String = objEmailData.rtf2html3(rtbDocument.Rtf, "+H+G+T+CR+I")
                        'objSendMail._Message = strConvertedData

                        Dim htmlOutput = "Document.html"
                        Dim contentUriPrefix = Path.GetFileNameWithoutExtension(htmlOutput)
                        Dim htmlResult = RtfToHtmlConverter.RtfToHtml(rtbDocument.Rtf, contentUriPrefix)
                        objSendMail._ImageContents = htmlResult._Content
                        htmlResult.WriteToFile(htmlOutput)
                        Dim oHtml As String = htmlResult._HTML
                        objSendMail._Message = oHtml
                        'S.SANDEEP [05-Apr-2018] -- END

                        If mblnIsFromPaySlip = False And mblnIsFromSickSheet = False Then
                            If filename = "" Then
                                objSendMail._AttachedFiles = ""
                            Else
                                objSendMail._AttachedFiles = filename
                            End If
                        Else
                            If mdicAttachmentInfo.Keys.Count > 0 Then
                                If mdicAttachmentInfo.ContainsKey(CInt(dtRow(0)("EmpId"))) Then
                                    objSendMail._AttachedFiles = mdicAttachmentInfo(CInt(dtRow(0)("EmpId")))
                                Else
                                    objSendMail._AttachedFiles = ""
                                End If
                            End If
                        End If
                        If mblnIsFromPaySlip = False And mblnIsFromSickSheet = False Then
                            'Sohail (30 Nov 2017) -- Start
                            'SUMATRA Enhancement – SUMATRA – issue # 0001669: Additional feature for Exchange Server configuration setting in 70.1.
                            'strMessage = objSendMail.SendMail()
                            strMessage = objSendMail.SendMail(Company._Object._Companyunkid)
                            'Sohail (30 Nov 2017) -- End
                        Else
                            'Sohail (30 Nov 2017) -- Start
                            'SUMATRA Enhancement – SUMATRA – issue # 0001669: Additional feature for Exchange Server configuration setting in 70.1.
                            'strMessage = objSendMail.SendMail(True, mstrExportEPaySlipPath)
                            strMessage = objSendMail.SendMail(Company._Object._Companyunkid, True, mstrExportEPaySlipPath)
                            'Sohail (30 Nov 2017) -- End
                        End If

                        If strMessage <> "" Then
                            strFailGusetName &= ", " & dsList.Tables(0).Rows(i)("Email").ToString
                        Else
                            blnSend = True
                        End If
                        If mstrReceipentUnkid = "" Then
                            blnSend = True
                        Else
                            mstrReceipentUnkid &= " , " & dsList.Tables(0).Rows(i)("FirstName").ToString & " " & dsList.Tables(0).Rows(i)("SurName").ToString & "<" & dsList.Tables(0).Rows(i)("Email").ToString & ">" & vbCrLf
                        End If

                        If mstrReceipentId = "" Then
                            mstrReceipentId = CStr(dtRow(0).Item("EmpId"))
                        Else
                            mstrReceipentId &= " , " & CStr(dtRow(0).Item("EmpId"))
                        End If
                        rtbDocument.Rtf = strTextData
                    Next
                End If
                bgwSendMail.ReportProgress(i)
            Next
            objEmailData._dicDisciplineFiles = mdicDiscpline
            objEmailData._EmailDateTime = ConfigParameter._Object._CurrentDateAndTime
            objEmailData._Istype_of = CChar("O")
            objEmailData._LettertypeUnkId = mintLetterUnkid
            If mintLetterUnkid > 0 Then
                Dim objLetterType As New clsLetterType
                objLetterType._LettertypeUnkId = mintLetterUnkid
                objEmailData._Message = objLetterType._Lettercontent
                objLetterType = Nothing
            Else
                objEmailData._Message = rtbDocument.Rtf
            End If
            objEmailData._Subject = txtSubject.Text
            objEmailData._UserUnkId = User._Object._Userunkid
            Select Case mintCallingRefId
                Case enImg_Email_RefId.Applicant_Module
                    objEmailData._Isapplicant = True
                Case Else
                    objEmailData._Isapplicant = False
            End Select
            If mstrReceipentId.Length > 0 Then
                With objEmailData
                    ._FormName = mstrModuleName
                    ._LoginEmployeeunkid = 0
                    ._ClientIP = getIP()
                    ._HostName = getHostName()
                    ._FromWeb = False
                    ._AuditUserId = User._Object._Userunkid
._CompanyUnkid = Company._Object._Companyunkid
                    ._AuditDate = ConfigParameter._Object._CurrentDateAndTime
                End With
                mblnResult = objEmailData.Insert(mstrReceipentId)
            End If
            If blnSend And strFailGusetName.Length = 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Mail(s) sent successfully!"), enMsgBoxStyle.Information)
            ElseIf blnSend And strFailGusetName.Length > 2 Then
                strFailGusetName = strFailGusetName.Substring(2)
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Failure sending some mails!") & vbCrLf & """" & strFailGusetName & """", enMsgBoxStyle.Information)
            Else
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Failure sending mails!"), enMsgBoxStyle.Information)
            End If
        Catch ex As Exception
            If ex.Message.Contains("Fail") Then
                eZeeMsgBox.Show(ex.Message & " " & Language.getMessage(mstrModuleName, 6, "Please check your Internet settings."), enMsgBoxStyle.Information)
            ElseIf ex.Message.Contains("secure") Then
                eZeeMsgBox.Show(ex.Message & " " & Language.getMessage(mstrModuleName, 7, "Please Uncheck Login using SSL setting."), enMsgBoxStyle.Information)
            Else
                DisplayError.Show("-1", ex.Message, "btnSend_Click", mstrModuleName)
            End If
        Finally
            blnIsMailSending = False
        End Try
    End Sub


    'S.SANDEEP [05-Apr-2018] -- START
    'ISSUE/ENHANCEMENT : {Ref#216|#ARUTI-102}
    Private Function embedImage(ByVal img As Image) As String
        Dim rtf As New StringBuilder()
        Try
            rtf.Append("{\rtf1\ansi\ansicpg1252\deff0\deflang1033")
            rtf.Append(GetFontTable(Me.Font))
            rtf.Append(GetImagePrefix(img))
            rtf.Append(getRtfImage(img))
            rtf.Append("}")
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "embedImage", mstrModuleName)
        Finally
        End Try
        Return rtf.ToString()
    End Function

    Private Function GetFontTable(ByVal font As Font) As String
        Dim fontTable = New StringBuilder()
        ' Append table control string
        fontTable.Append("{\fonttbl{\f0")
        fontTable.Append("\")
        Dim rtfFontFamily = New HybridDictionary()
        rtfFontFamily.Add(FontFamily.GenericMonospace.Name, "\fmodern")
        rtfFontFamily.Add(FontFamily.GenericSansSerif, "\fswiss")
        rtfFontFamily.Add(FontFamily.GenericSerif, "\froman")
        rtfFontFamily.Add("UNKNOWN", "\fnil")

        ' If the font's family corresponds to an RTF family, append the
        ' RTF family name, else, append the RTF for unknown font family.
        fontTable.Append(If(rtfFontFamily.Contains(font.FontFamily.Name), rtfFontFamily(font.FontFamily.Name), rtfFontFamily("UNKNOWN")))
        ' \fcharset specifies the character set of a font in the font table.
        ' 0 is for ANSI.
        fontTable.Append("\fcharset0 ")
        ' Append the name of the font
        fontTable.Append(font.Name)
        ' Close control string
        fontTable.Append(";}}")
        Return fontTable.ToString()
    End Function

    Private Function GetImagePrefix(ByVal _image As Image) As String
        Dim xDpi, yDpi As Single
        Dim rtf = New StringBuilder()
        Using graphics As Graphics = CreateGraphics()
            xDpi = graphics.DpiX
            yDpi = graphics.DpiY
        End Using
        ' Calculate the current width of the image in (0.01)mm
        Dim picw = CInt(Math.Truncate(Math.Round((_image.Width / xDpi) * 2540)))
        ' Calculate the current height of the image in (0.01)mm
        Dim pich = CInt(Math.Truncate(Math.Round((_image.Height / yDpi) * 2540)))
        ' Calculate the target width of the image in twips
        Dim picwgoal = CInt(Math.Truncate(Math.Round((_image.Width / xDpi) * 1440)))
        ' Calculate the target height of the image in twips
        Dim pichgoal = CInt(Math.Truncate(Math.Round((_image.Height / yDpi) * 1440)))
        ' Append values to RTF string
        rtf.Append("{\pict\wmetafile8")
        rtf.Append("\picw")
        rtf.Append(picw)
        rtf.Append("\pich")
        rtf.Append(pich)
        rtf.Append("\picwgoal")
        rtf.Append(picwgoal)
        rtf.Append("\pichgoal")
        rtf.Append(pichgoal)
        rtf.Append(" ")

        Return rtf.ToString()
    End Function

    Private Function getRtfImage(ByVal image As Image) As String
        ' Used to store the enhanced metafile
        Dim stream As MemoryStream = Nothing
        ' Used to create the metafile and draw the image
        Dim graphics As Graphics = Nothing
        ' The enhanced metafile
        Dim metaFile As Imaging.Metafile = Nothing
        Try
            Dim rtf = New StringBuilder()
            stream = New MemoryStream()
            ' Get a graphics context from the RichTextBox
            graphics = CreateGraphics()
            Using graphics
                ' Get the device context from the graphics context
                Dim hdc As IntPtr = graphics.GetHdc()
                ' Create a new Enhanced Metafile from the device context
                metaFile = New Imaging.Metafile(stream, hdc)
                ' Release the device context
                graphics.ReleaseHdc(hdc)
            End Using

            ' Get a graphics context from the Enhanced Metafile
            graphics = graphics.FromImage(metaFile)
            Using graphics
                ' Draw the image on the Enhanced Metafile
                graphics.DrawImage(image, New Rectangle(0, 0, image.Width, image.Height))
            End Using

            ' Get the handle of the Enhanced Metafile
            Dim hEmf As IntPtr = metaFile.GetHenhmetafile()
            ' A call to EmfToWmfBits with a null buffer return the size of the
            ' buffer need to store the WMF bits.  Use this to get the buffer
            ' size.
            Dim bufferSize As UInteger = GdipEmfToWmfBits(hEmf, 0, Nothing, 8, EmfToWmfBitsFlags.EmfToWmfBitsFlagsDefault)
            ' Create an array to hold the bits
            Dim buffer = New Byte(CInt(bufferSize - 1)) {}
            ' A call to EmfToWmfBits with a valid buffer copies the bits into the
            ' buffer an returns the number of bits in the WMF.  
            Dim _convertedSize As UInteger = GdipEmfToWmfBits(hEmf, bufferSize, buffer, 8, EmfToWmfBitsFlags.EmfToWmfBitsFlagsDefault)
            ' Append the bits to the RTF string
            For Each t As Byte In buffer
                rtf.Append(String.Format("{0:X2}", t))
            Next t
            Return rtf.ToString()
        Finally
            If graphics IsNot Nothing Then
                graphics.Dispose()
            End If
            If metaFile IsNot Nothing Then
                metaFile.Dispose()
            End If
            If stream IsNot Nothing Then
                stream.Close()
            End If
        End Try
    End Function
    'S.SANDEEP [05-Apr-2018] -- END

#End Region

#Region " Form's Events "

    Private Sub frmSendMail_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        mblnIsSendDisposed = True
    End Sub

    Private Sub frmSendMail_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Call Set_Logo(Me, gApplicationType)
            Call Language.setLanguage(Me.Name)
            'Hemant (30 Nov 2018) -- Start
            'Enhancement : Including Language Settings For Scan/Attachment Button
            Call OtherSettings()
            'Hemant (30 Nov 2018) -- End
            pnlColor.Hide()
            If FontSizes.Count <= 0 Then
                InitializeFontSizes()
            End If
            Call FillCombo()
            If rtbDocument.SelectedText.Length = 0 Then
                mnuCut.Enabled = False
                mnuCopy.Enabled = False
                mnuPaste.Enabled = False
            End If
            If txtToAddress.Text = "" Then
                btnViewMerge.Enabled = False
                btnFinish.Enabled = False
            End If
            objbtnNext.Visible = False
            objbtnPrevious.Visible = False
            btnEmployeeDocument.Visible = False
            objlblCaption.Visible = False
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmSendMail_Load", mstrModuleName)
        End Try
    End Sub

    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()
            objfrm.displayDialog(Me)
            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub

#End Region

#Region " ToolStrip Events "

#Region " Font And Size "

    Private Sub cboFontSize_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles objcboFontSize.KeyPress
        Try
            If Not (Asc(e.KeyChar) > 47 And Asc(e.KeyChar) < 58) Then
                If (Asc(e.KeyChar) = Asc(GUI.DecimalSeparator) And InStr(Trim(objcboFontSize.Text), GUI.DecimalSeparator) = 0) Or Asc(e.KeyChar) = 8 Then Exit Sub
                e.KeyChar = CChar("")
            End If
        Catch ex As Exception
            DisplayError.Show(CStr(-1), ex.Message, "cboFontSize_KeyPress", mstrModuleName)
        End Try
    End Sub

    Private Sub cboFontSize_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles objcboFontSize.TextChanged
        Try
            If objcboFontSize.Text = "" Or objcboFontSize.Text = "." Then
                Exit Sub
            Else
                Call ChangeFontSize(CDec(objcboFontSize.Text))
            End If
        Catch ex As Exception
            DisplayError.Show(CStr(-1), ex.Message, "cboFontSize_TextChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub cboFont_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles objcboFont.SelectedIndexChanged
        Try
            Call ChangeFont(objcboFont.Text)
        Catch ex As Exception
            DisplayError.Show(CStr(-1), ex.Message, "cboFont_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Alignments "
    Private Sub tlbbtnLeftAlign_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objtlbbtnLeftAlign.Click
        Try
            Dim strFormat As New StringFormat(StringFormatFlags.NoClip)
            objtlbbtnCenterAlign.Checked = False
            objtlbbtnRightAlign.Checked = False
            objtlbbtnLeftAlign.Checked = True
            If rtbDocument.SelectionAlignment <> HorizontalAlignment.Left Then
                rtbDocument.SelectionAlignment = HorizontalAlignment.Left
                Call RegainOldStyle()
            End If
        Catch ex As Exception
            DisplayError.Show(CStr(-1), ex.Message, "tlbbtnLeftAlign_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub tlbbtnCenterAlign_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objtlbbtnCenterAlign.Click
        Try
            objtlbbtnCenterAlign.Checked = True
            objtlbbtnRightAlign.Checked = False
            objtlbbtnLeftAlign.Checked = False
            If rtbDocument.SelectionAlignment <> HorizontalAlignment.Center Then
                rtbDocument.SelectionAlignment = HorizontalAlignment.Center
                Call RegainOldStyle()
            End If
        Catch ex As Exception
            DisplayError.Show(CStr(-1), ex.Message, "tlbbtnCenterAlign_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub tlbbtnRightAlign_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objtlbbtnRightAlign.Click
        Try
            objtlbbtnCenterAlign.Checked = False
            objtlbbtnRightAlign.Checked = True
            objtlbbtnLeftAlign.Checked = False
            If rtbDocument.SelectionAlignment <> HorizontalAlignment.Right Then
                rtbDocument.SelectionAlignment = HorizontalAlignment.Right
                Call RegainOldStyle()
            End If
        Catch ex As Exception
            DisplayError.Show(CStr(-1), ex.Message, "tlbbtnRightAlign_Click", mstrModuleName)
        End Try
    End Sub
#End Region

#Region " Styles "

    Private Sub objtlbbtnBold_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objtlbbtnBold.Click
        Try
            If objtlbbtnBold.Checked = True Then
                Call ApplyStyle(FontStyle.Bold, True)
            Else
                Call ApplyStyle(FontStyle.Bold, False)
            End If
        Catch ex As Exception
            DisplayError.Show(CStr(-1), ex.Message, "objtlbbtnBold_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objtlbbtnItalic_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objtlbbtnItalic.Click
        Try
            If objtlbbtnItalic.Checked = True Then
                Call ApplyStyle(FontStyle.Italic, True)
            Else
                Call ApplyStyle(FontStyle.Italic, False)
            End If
        Catch ex As Exception
            DisplayError.Show(CStr(-1), ex.Message, "objtlbbtnItalic_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objtlbbtnUnderline_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objtlbbtnUnderline.Click
        Try
            If objtlbbtnUnderline.Checked = True Then
                Call ApplyStyle(FontStyle.Underline, True)
            Else
                Call ApplyStyle(FontStyle.Underline, False)
            End If
        Catch ex As Exception
            DisplayError.Show(CStr(-1), ex.Message, "objtlbbtnUnderline_Click", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Operations "

    Private Sub objtlbbtnUndo_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objtlbbtnUndo.Click
        Try
            rtbDocument.Undo()
        Catch ex As Exception
            DisplayError.Show(CStr(-1), ex.Message, "objtlbbtnUndo_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objtlbbtnRedo_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objtlbbtnRedo.Click
        Try
            rtbDocument.Redo()
        Catch ex As Exception
            DisplayError.Show(CStr(-1), ex.Message, "objtlbbtnRedo_Click", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Buttons "
    Private Sub objtlbbtnOpenTemplate_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objtlbbtnOpenTemplate.Click
        Dim frm As New frmLetterTemplate
        Try
            'If User._Object._RightToLeft = True Then
            '    frm.RightToLeft = System.Windows.Forms.RightToLeft.Yes
            '    frm.RightToLeftLayout = True
            '    Call Language.ctlRightToLeftlayOut(frm)
            'End If

            'S.SANDEEP [ 02 MAY 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            'frm.displayDialog(True)
            If frm.displayDialog(True) = True Then
                strData = frm._Letter_Content
                mintLetterUnkid = frm._Letter_Unkid
                rtbDocument.Rtf = strData
            End If
            'S.SANDEEP [ 02 MAY 2012 ] -- END
        Catch ex As Exception
            DisplayError.Show(CStr(-1), ex.Message, "objtlbbtnOpenTemplate_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objtlbbtnBulletSimple_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objtlbbtnBulletSimple.Click
        Try
            If objtlbbtnBulletSimple.Checked = True Then
                rtbDocument.SelectionBullet = True
            Else
                rtbDocument.SelectionBullet = False
            End If
            Call RegainOldStyle()
        Catch ex As Exception
            DisplayError.Show(CStr(-1), ex.Message, "objtlbbtnBulletSimple_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objtlbbtnLeftIndent_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objtlbbtnLeftIndent.Click
        Try
            If rtbDocument.SelectionIndent = 0 Then
                rtbDocument.SelectionIndent = 5
            Else
                rtbDocument.SelectionIndent += 10
            End If
            Call RegainOldStyle()
        Catch ex As Exception
            DisplayError.Show(CStr(-1), ex.Message, "objtlbbtnLeftIndent_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objtlbbtnRightIndent_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objtlbbtnRightIndent.Click
        Try
            If rtbDocument.SelectionRightIndent = 0 Then
                rtbDocument.SelectionRightIndent = 5
            Else
                rtbDocument.SelectionRightIndent += 10
            End If
            Call RegainOldStyle()
        Catch ex As Exception
            DisplayError.Show(CStr(-1), ex.Message, "objtlbbtnRightIndent_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objtlbbtnCut_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objtlbbtnCut.Click
        Try
            If rtbDocument.SelectedText.Length > 0 Then
                My.Computer.Clipboard.SetText(rtbDocument.SelectedText)
                rtbDocument.SelectedText = ""
            End If
        Catch ex As Exception
            DisplayError.Show(CStr(-1), ex.Message, "objtlbbtnCut_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objtlbbtnCopy_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objtlbbtnCopy.Click
        Try
            If rtbDocument.SelectedText.Length > 0 Then
                My.Computer.Clipboard.SetText(rtbDocument.SelectedText)
            End If

        Catch ex As Exception
            DisplayError.Show(CStr(-1), ex.Message, "objtlbbtnCopy_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objtlbbtnPaste_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objtlbbtnPaste.Click
        Try
            If My.Computer.Clipboard.ContainsText Then
                rtbDocument.AppendText(My.Computer.Clipboard.GetText())
            End If
        Catch ex As Exception
            DisplayError.Show(CStr(-1), ex.Message, "objtlbbtnPaste_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objtlbbtnColor_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objtlbbtnColor.Click
        If pnlColor.Visible = True Then

            pnlColor.Hide()
            pnlColor.SendToBack()
        End If
        pnlColor.BringToFront()

        pnlColor.Show()
    End Sub

    Private Sub SetColor(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles objbtn1.MouseClick, _
                                objbtn2.MouseClick, objbtn3.MouseClick, objbtn4.MouseClick, objbtn5.MouseClick, objbtn6.MouseClick, objbtn7.MouseClick, _
                                objbtn8.MouseClick, objbtn9.MouseClick, objbtn10.MouseClick, objbtn11.MouseClick, objbtn12.MouseClick, objbtn13.MouseClick, objbtn14.MouseClick, _
                                 objbtn15.MouseClick, objbtn16.MouseClick, objbtn17.MouseClick, objbtn18.MouseClick, objbtn19.MouseClick, _
                                objbtn20.MouseClick, objbtn21.MouseClick, objbtn22.MouseClick, objbtn23.MouseClick, objbtn24.MouseClick, objbtn25.MouseClick, _
                                objbtn26.MouseClick, objbtn27.MouseClick, objbtn28.MouseClick, objbtn29.MouseClick, objbtn30.MouseClick, objbtn31.MouseClick, _
                                objbtn32.MouseClick, objbtn33.MouseClick, objbtn34.MouseClick, objbtn35.MouseClick, objbtn36.MouseClick, objbtn37.MouseClick, _
                                objbtn38.MouseClick, objbtn39.MouseClick, objbtn40.MouseClick, objbtn41.MouseClick, objbtn42.MouseClick, objbtn43.MouseClick, _
                                 objbtn44.MouseClick, objbtn45.MouseClick, objbtn46.MouseClick, objbtn47.MouseClick, objbtn48.MouseClick
        rtbDocument.SelectionColor = CType(sender, Button).BackColor
        Call RegainOldStyle()
        pnlColor.Hide()
    End Sub

    Private Sub btnMoreColor_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnMoreColor.Click
        ColorDialog1.ShowDialog()
        rtbDocument.SelectionColor = ColorDialog1.Color
        pnlColor.Hide()
        Call RegainOldStyle()
    End Sub
#End Region

#End Region

#Region " Context Menu Events "

    Private Sub mnuCut_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuCut.Click
        Try
            If rtbDocument.SelectedText.Length > 0 Then
                mnuCut.Enabled = True
                My.Computer.Clipboard.SetText(rtbDocument.SelectedText)
                rtbDocument.SelectedText = ""
            Else
                mnuCut.Enabled = False
            End If
        Catch ex As Exception
            DisplayError.Show(CStr(-1), ex.Message, "mnuCut_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub mnuCopy_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuCopy.Click
        Try
            If rtbDocument.SelectedText.Length > 0 Then
                mnuCopy.Enabled = True
                My.Computer.Clipboard.SetText(rtbDocument.SelectedText)
            Else
                mnuCopy.Enabled = False
            End If
        Catch ex As Exception
            DisplayError.Show(CStr(-1), ex.Message, "mnuCopy_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub mnuPaste_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuPaste.Click
        Try
            Dim strString As String = ""
            Dim mFontSize As Integer = CInt(objcboFontSize.Text)
            mGlobalFontSize = CInt(objcboFontSize.Text)
            Dim mFontFamily As New FontFamily(objcboFont.Text)

            If My.Computer.Clipboard.ContainsText Then
                mnuPaste.Enabled = True
                strString = My.Computer.Clipboard.GetText()

                rtbDocument.AppendText(strString)
                rtbDocument.SelectionFont = New Font(mFontFamily, mFontSize, FontStyle.Regular)
            Else
                mnuPaste.Enabled = False
            End If
        Catch ex As Exception
            DisplayError.Show(CStr(-1), ex.Message, "mnuPaste_Click", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Other Control's Events "

    Private Sub rtbDocument_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles rtbDocument.TextChanged
        Try
            mnuCut.Enabled = True
            mnuCopy.Enabled = True
            mnuPaste.Enabled = True
            Call ChangeFont(objcboFont.Text)
            Call ChangeFontSize(CDec(objcboFontSize.Text))
        Catch ex As Exception
            DisplayError.Show(CStr(-1), ex.Message, "rtbDocument_TextChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub txtToAddress_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtToAddress.TextChanged
        Try
            If txtToAddress.Text.Length > 0 Then
                btnViewMerge.Enabled = True
                btnFinish.Enabled = True
                btnSend.Enabled = True
            Else
                btnViewMerge.Enabled = False
                btnFinish.Enabled = False
                btnSend.Enabled = False
            End If
        Catch ex As Exception
            DisplayError.Show(CStr(-1), ex.Message, "txtToAddress_TextChanged", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Button's Events "

    Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show(CStr(-1), ex.Message, "btnCancel_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnViewMerge_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnViewMerge.Click
        Try
            rtbDocument.ReadOnly = True
            rtbDocument.BackColor = GUI.ColorComp
            btnViewMerge.Hide()
            btnFinish.Show()
            objbtnNext.Visible = True
            objbtnPrevious.Visible = True
            btnSend.Enabled = False
            If rtbDocument.Rtf <> strData Then
                strData = rtbDocument.Rtf
            End If
        Catch ex As Exception
            DisplayError.Show(CStr(-1), ex.Message, "btnViewMerge_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnFinish_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnFinish.Click
        Try
            rtbDocument.ReadOnly = False
            rtbDocument.BackColor = GUI.ColorOptional
            btnViewMerge.Show()
            btnFinish.Hide()
            objbtnNext.Visible = False
            objbtnPrevious.Visible = False
            rtbDocument.Rtf = strData
            btnSend.Enabled = True
            intCount = 0
        Catch ex As Exception
            DisplayError.Show(CStr(-1), ex.Message, "btnViewMerge_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub mnuEmployee_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuEmployee.Click
        Try
            If tabcSendMail.TabPages.Contains(tabpAttachment) = False Then
                tabcSendMail.TabPages.Add(tabpAttachment)
            End If
            Call CallFrom(enImg_Email_RefId.Employee_Module)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "mnuEmployee_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnNext_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnNext.Click
        Try
            If mblnSendEachTranByUnkId = False Then 'Sohail (17 Feb 2021)
            Call SetAddress()
            If dsList.Tables(0).Rows.Count = 1 Then
                If rtbDocument.Rtf <> strData Then
                    rtbDocument.Rtf = strData
                End If

                dtRow = dsList.Tables(0).Select("email = " & "'" & strMailAddess(intCount).Replace("'", "''") & "'")

                If mstrLeaveFormIds.Trim.Length > 0 Then
                    For i As Integer = 0 To dtRow.Length - 1
                        Call ViewMergeData(i)
                    Next
                Else
                    Call ViewMergeData(intCount)
                End If
            Else
                If dsList Is Nothing Then
                    Exit Sub
                ElseIf intCount = dsList.Tables(0).Rows.Count Then
                    Exit Sub
                ElseIf dsList.Tables(0).Rows.Count - intCount = 1 Then
                    Exit Sub
                Else
                    If rtbDocument.Rtf <> strData Then
                        intCount += 1
                    End If
                    rtbDocument.Rtf = strData
                    dtRow = dsList.Tables(0).Select("email = " & "'" & strMailAddess(intCount).Replace("'", "''") & "'")
                    If dtRow.Length > 0 Then
                        If mstrLeaveFormIds.Trim.Length > 0 Then
                            For i As Integer = 0 To dtRow.Length - 1
                                Call ViewMergeData(i)
                            Next
                        Else
                            Call ViewMergeData(intCount)
                        End If
                    End If
                End If
            End If
                'Sohail (17 Feb 2021) -- Start
                'NMB Enhancement : OLD-322 : Addition of Loan Parameters Field on Letter Templates.
            Else
                If dsList Is Nothing Then
                    Exit Sub
                ElseIf dsList.Tables(0).Rows.Count <= 0 Then
                    Exit Sub
                Else
                    If rtbDocument.Rtf <> strData Then
                        intCount += 1
                    End If
                    If intCount = dsList.Tables(0).Rows.Count Then
                        intCount -= 1
                        Exit Sub
                    End If
                    rtbDocument.Rtf = strData

                    Dim row As DataRow = dsList.Tables(0).Rows(intCount)
                    dtRow = Nothing
                    ReDim dtRow(0)
                    dtRow(0) = row

                    If dtRow.Length > 0 Then
                        Call ViewMergeData(intCount)
                    End If
                End If
            End If
            'Sohail (17 Feb 2021) -- End
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnNext_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnPrevious_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnPrevious.Click
        Try
            If mblnSendEachTranByUnkId = False Then 'Sohail (17 Feb 2021)
            If strMailAddess IsNot Nothing Then
                If CInt(strMailAddess.Length) <= 0 Then
                    Call SetAddress()
                End If
            End If
            If intCount <= 0 Then
                Exit Sub
            ElseIf intCount <> dsList.Tables(0).Rows.Count Then
                intCount -= 1
                rtbDocument.Rtf = strData
                dtRow = dsList.Tables(0).Select("email = " & "'" & strMailAddess(intCount).Replace("'", "''") & "'")
                If dtRow.Length > 0 Then
                    If mstrLeaveFormIds.Trim.Length > 0 Then
                        For i As Integer = 0 To dtRow.Length - 1
                            Call ViewMergeData(i)
                        Next
                    Else
                        Call ViewMergeData(intCount)
                    End If
                End If
            End If
                'Sohail (17 Feb 2021) -- Start
                'NMB Enhancement : OLD-322 : Addition of Loan Parameters Field on Letter Templates.
            Else
                If dsList Is Nothing Then
                    Exit Sub
                ElseIf dsList.Tables(0).Rows.Count <= 0 Then
                    Exit Sub
                Else
                    If rtbDocument.Rtf <> strData Then
                        intCount -= 1
                    End If
                    If intCount < 0 Then
                        intCount += 1
                        Exit Sub
                    End If
                    rtbDocument.Rtf = strData

                    Dim row As DataRow = dsList.Tables(0).Rows(intCount)
                    dtRow = Nothing
                    ReDim dtRow(0)
                    dtRow(0) = row

                    If dtRow.Length > 0 Then
                        Call ViewMergeData(intCount)
                    End If
                End If
            End If
            'Sohail (17 Feb 2021) -- End
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "", mstrModuleName)
        End Try
    End Sub

    Private Sub btnAttach_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAttach.Click
        Try
            ofdMailAttachment.Filter = "Word Files (*.doc)|*.rtf|Image Files (*.bmp)|*.bmp|JPG Files (*.jpg)|*.jpg|All Files (*.*)|*.*"
            If (ofdMailAttachment.ShowDialog(Me) = System.Windows.Forms.DialogResult.OK) Then


                'Pinkal (27-Nov-2020) -- Start
                'Enhancement Outlook  -  Working on  Don't Allow Users to attach .Exe file types in email.
                Dim mstrExtension As String = Path.GetExtension(ofdMailAttachment.FileName)
                If mstrExtension = ".exe" Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 11, "You can not attach .exe(Executable File) for the security reason."))
                    Exit Sub
                End If
                'Pinkal (27-Nov-2020) -- End

                Dim lvItem As New ListViewItem
                lvItem.Text = ofdMailAttachment.FileName
                lvItem.SubItems.Add("False")
                lvAttachedFile.Items.Add(lvItem)

            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnAttach_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnRemove_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnRemove.Click
        Try
            If lvAttachedFile.SelectedItems.Count > 0 Then
                lvAttachedFile.Items.Remove(lvAttachedFile.SelectedItems(0))
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnRemove_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnSend_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSend.Click
        Try
            If txtSubject.Text.Trim.Length <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 8, "Subject is mandatory information. Please write Subject to continue."), enMsgBoxStyle.Information)
                Exit Sub
            ElseIf txtToAddress.Text.Trim.Length <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 9, "Email Address is mandatory information. Please Set Email Address to continue."), enMsgBoxStyle.Information)
                Exit Sub
            End If
            Call SetControlVisibility()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnSend_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnEmployeeDocument_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEmployeeDocument.Click
        Dim frm As New frmScanAttachmentList
        Try
            frm._IsFromMail = True
            frm.objFooter.Visible = False
            frm.objeFooter.Visible = True
            frm.ShowDialog()
            If frm._Attached_Files.Trim.Length > 0 Then
                For Each fl As String In frm._Attached_Files.Split(CChar("|"))
                    Dim lvItem As New ListViewItem
                    lvItem.Text = fl.ToString
                    lvItem.SubItems.Add("True")
                    lvAttachedFile.Items.Add(lvItem)
                Next
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

#Region " Menu Event(s) "

    Private Sub mnuApplicant_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles mnuApplicant.Click
        Try
            If tabcSendMail.TabPages.Contains(tabpAttachment) = False Then
                tabcSendMail.TabPages.Add(tabpAttachment)
            End If
            Call CallFrom(enImg_Email_RefId.Applicant_Module)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "mnuApplicant_Click", mstrModuleName)
        End Try

    End Sub

    Private Sub mnuSendPayslip_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuSendPayslip.Click
        Try
            If tabcSendMail.TabPages.Contains(tabpAttachment) Then
                tabcSendMail.TabPages.Remove(tabpAttachment)
            End If
            Call CallFrom(enImg_Email_RefId.Payroll_Module)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "mnuSendPayslip_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub mnuAppraisalLetters_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuAppraisalLetters.Click
        Try
            If tabcSendMail.TabPages.Contains(tabpAttachment) = False Then
                tabcSendMail.TabPages.Add(tabpAttachment)
            End If
            Call CallFrom(enImg_Email_RefId.Appraisal_Module)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "mnuAppraisalLetters_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub mnuSendSickSheet_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuSendSickSheet.Click
        Try
            If tabcSendMail.TabPages.Contains(tabpAttachment) Then
                tabcSendMail.TabPages.Remove(tabpAttachment)
            End If
            Call CallFrom(enImg_Email_RefId.Medical_Module)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "mnuSendSickSheet_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub mnuDiscipline_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuDiscipline.Click
        Try
            If tabcSendMail.TabPages.Contains(tabpAttachment) = False Then
                tabcSendMail.TabPages.Add(tabpAttachment)
            End If
            Call CallFrom(enImg_Email_RefId.Discipline_Module)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "mnuDiscipline_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub mnuSendLeaveForm_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuSendLeaveForm.Click
        If tabcSendMail.TabPages.Contains(tabpAttachment) Then
            tabcSendMail.TabPages.Remove(tabpAttachment)
        End If
        Call CallFrom(enImg_Email_RefId.Leave_Module)
    End Sub

    Private Sub mnuEmailReports_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuEmailReports.Click
        If tabcSendMail.TabPages.Contains(tabpAttachment) Then
            tabcSendMail.TabPages.Remove(tabpAttachment)
        End If
        Call CallFrom(enImg_Email_RefId.TimeAndAttendance)
    End Sub

    'Sohail (17 Feb 2021) -- Start
    'NMB Enhancement : OLD-322 : Addition of Loan Parameters Field on Letter Templates.
    Private Sub mnuLoanBalance_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuLoanBalance.Click
        Try
            If tabcSendMail.TabPages.Contains(tabpAttachment) Then
                tabcSendMail.TabPages.Remove(tabpAttachment)
            End If
            Call CallFrom(enImg_Email_RefId.Loan_Balance)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "mnuLoanBalance_Click", mstrModuleName)
        End Try
    End Sub

    'Sohail (17 Feb 2021) -- End

#End Region

#Region " Background Worker Event(s) "

    Private Sub bgwSendMail_DoWork(ByVal sender As System.Object, ByVal e As System.ComponentModel.DoWorkEventArgs) Handles bgwSendMail.DoWork
        Try
            RemoveHandler gfrmMDI.tmrReminder.Tick, AddressOf gfrmMDI.tmrReminder_Tick
            If mstrLeaveFormIds.Trim.Length > 0 Then
                Call LeaveFormSend()
            Else
                Call Send()
            End If
            'AddHandler gfrmMDI.tmrReminder.Tick, AddressOf gfrmMDI.tmrReminder_Tick
        Catch ex As Exception
            RemoveHandler gfrmMDI.tmrReminder.Tick, AddressOf gfrmMDI.tmrReminder_Tick
            AddHandler gfrmMDI.tmrReminder.Tick, AddressOf gfrmMDI.tmrReminder_Tick
            'DisplayError.Show("-1", ex.Message, "bgwSendMail_DoWork", mstrModuleName)
        Finally
            mblnIsSendingMail = False
        End Try
    End Sub

    Private Sub bgwSendMail_ProgressChanged(ByVal sender As Object, ByVal e As System.ComponentModel.ProgressChangedEventArgs) Handles bgwSendMail.ProgressChanged
        Application.DoEvents()
        Dim canvas As Graphics = Me.pbProgress.CreateGraphics
        If e.ProgressPercentage = 0 Then
            pbProgress.Value = CInt(((1 * 100) / dsList.Tables(0).Rows.Count))
        Else
            pbProgress.Value = CInt(((e.ProgressPercentage * 100) / dsList.Tables(0).Rows.Count))
        End If
        canvas.DrawString(pbProgress.Value.ToString & "%", New Font("Verdana", 9, FontStyle.Bold), New SolidBrush(Color.Black), 180, 5)
        canvas.Dispose()
    End Sub

    Private Sub bgwSendMail_RunWorkerCompleted(ByVal sender As Object, ByVal e As System.ComponentModel.RunWorkerCompletedEventArgs) Handles bgwSendMail.RunWorkerCompleted
        Try
            mblnIsSendingMail = False
            mblnIsSendDisposed = True
            Me.Close()
            RemoveHandler gfrmMDI.tmrReminder.Tick, AddressOf gfrmMDI.tmrReminder_Tick
            AddHandler gfrmMDI.tmrReminder.Tick, AddressOf gfrmMDI.tmrReminder_Tick
        Catch ex As IO.IOException
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "bgwSendMail_RunWorkerCompleted", mstrModuleName)
        Finally
            mblnIsSendingPayslip = False
            mblnIsSendingMail = False
        End Try
    End Sub

#End Region


    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Private Sub OtherSettings()
        Try
            Me.SuspendLayout()

            Call SetLanguage()

			
            Me.eZeeHeader.GradientColor1 = GUI._HeaderBackColor1
            Me.eZeeHeader.GradientColor2 = GUI._HeaderBackColor2
            Me.eZeeHeader.BorderColor = GUI._HeaderBorderColor
            Me.eZeeHeader.HeaderTextForeColor = GUI._HeaderTitleFontColor
            Me.eZeeHeader.DescriptionForeColor = GUI._HeaderMessageFontColor


            Me.btnFinish.GradientBackColor = GUI._ButttonBackColor
            Me.btnFinish.GradientForeColor = GUI._ButttonFontColor

            Me.btnViewMerge.GradientBackColor = GUI._ButttonBackColor
            Me.btnViewMerge.GradientForeColor = GUI._ButttonFontColor

            Me.btnCancel.GradientBackColor = GUI._ButttonBackColor
            Me.btnCancel.GradientForeColor = GUI._ButttonFontColor

            Me.btnSend.GradientBackColor = GUI._ButttonBackColor
            Me.btnSend.GradientForeColor = GUI._ButttonFontColor

            Me.objbtnPrevious.GradientBackColor = GUI._ButttonBackColor
            Me.objbtnPrevious.GradientForeColor = GUI._ButttonFontColor

            Me.objbtnNext.GradientBackColor = GUI._ButttonBackColor
            Me.objbtnNext.GradientForeColor = GUI._ButttonFontColor

            Me.btnRemove.GradientBackColor = GUI._ButttonBackColor
            Me.btnRemove.GradientForeColor = GUI._ButttonFontColor

            Me.btnTo.GradientBackColor = GUI._ButttonBackColor
            Me.btnTo.GradientForeColor = GUI._ButttonFontColor

            Me.btnAttach.GradientBackColor = GUI._ButttonBackColor
            Me.btnAttach.GradientForeColor = GUI._ButttonFontColor

            Me.btnEmployeeDocument.GradientBackColor = GUI._ButttonBackColor
            Me.btnEmployeeDocument.GradientForeColor = GUI._ButttonFontColor


            Me.ResumeLayout()
        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "OtherSettings", mstrModuleName)
        End Try
    End Sub


    Private Sub SetLanguage()
        Try
            Me.Text = Language._Object.getCaption(Me.Name, Me.Text)

            Me.mnuCut.Text = Language._Object.getCaption(Me.mnuCut.Name, Me.mnuCut.Text)
            Me.mnuCopy.Text = Language._Object.getCaption(Me.mnuCopy.Name, Me.mnuCopy.Text)
            Me.mnuPaste.Text = Language._Object.getCaption(Me.mnuPaste.Name, Me.mnuPaste.Text)
			Me.eZeeHeader.Title = Language._Object.getCaption(Me.eZeeHeader.Name & "_Title" , Me.eZeeHeader.Title)
			Me.eZeeHeader.Message = Language._Object.getCaption(Me.eZeeHeader.Name & "_Message" , Me.eZeeHeader.Message)
            Me.btnFinish.Text = Language._Object.getCaption(Me.btnFinish.Name, Me.btnFinish.Text)
            Me.btnViewMerge.Text = Language._Object.getCaption(Me.btnViewMerge.Name, Me.btnViewMerge.Text)
            Me.btnCancel.Text = Language._Object.getCaption(Me.btnCancel.Name, Me.btnCancel.Text)
            Me.btnSend.Text = Language._Object.getCaption(Me.btnSend.Name, Me.btnSend.Text)
            Me.lblSubject.Text = Language._Object.getCaption(Me.lblSubject.Name, Me.lblSubject.Text)
            Me.btnRemove.Text = Language._Object.getCaption(Me.btnRemove.Name, Me.btnRemove.Text)
            Me.btnMoreColor.Text = Language._Object.getCaption(Me.btnMoreColor.Name, Me.btnMoreColor.Text)
            Me.mnuApplicant.Text = Language._Object.getCaption(Me.mnuApplicant.Name, Me.mnuApplicant.Text)
            Me.mnuEmployee.Text = Language._Object.getCaption(Me.mnuEmployee.Name, Me.mnuEmployee.Text)
            Me.btnTo.Text = Language._Object.getCaption(Me.btnTo.Name, Me.btnTo.Text)
            Me.mnuSendPayslip.Text = Language._Object.getCaption(Me.mnuSendPayslip.Name, Me.mnuSendPayslip.Text)
            Me.mnuSendSickSheet.Text = Language._Object.getCaption(Me.mnuSendSickSheet.Name, Me.mnuSendSickSheet.Text)
            Me.mnuAppraisalLetters.Text = Language._Object.getCaption(Me.mnuAppraisalLetters.Name, Me.mnuAppraisalLetters.Text)
            Me.btnAttach.Text = Language._Object.getCaption(Me.btnAttach.Name, Me.btnAttach.Text)
            Me.btnEmployeeDocument.Text = Language._Object.getCaption(Me.btnEmployeeDocument.Name, Me.btnEmployeeDocument.Text)
            Me.tabpEmailInfo.Text = Language._Object.getCaption(Me.tabpEmailInfo.Name, Me.tabpEmailInfo.Text)
            Me.tabpAttachment.Text = Language._Object.getCaption(Me.tabpAttachment.Name, Me.tabpAttachment.Text)
            Me.pbProgress.Text = Language._Object.getCaption(Me.pbProgress.Name, Me.pbProgress.Text)
            Me.mnuDiscipline.Text = Language._Object.getCaption(Me.mnuDiscipline.Name, Me.mnuDiscipline.Text)
            Me.mnuSendLeaveForm.Text = Language._Object.getCaption(Me.mnuSendLeaveForm.Name, Me.mnuSendLeaveForm.Text)
            Me.mnuEmailReports.Text = Language._Object.getCaption(Me.mnuEmailReports.Name, Me.mnuEmailReports.Text)
			Me.mnuLoanBalance.Text = Language._Object.getCaption(Me.mnuLoanBalance.Name, Me.mnuLoanBalance.Text)

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
        End Try
    End Sub


    Private Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Mail(s) sent successfully!")
            Language.setMessage(mstrModuleName, 2, "Failure sending some mails!")
            Language.setMessage(mstrModuleName, 3, "Failure sending mails!")
            Language.setMessage(mstrModuleName, 4, "The following email Address is invalid :")
            Language.setMessage(mstrModuleName, 5, "Please enter valid email.")
            Language.setMessage(mstrModuleName, 6, "Please check your Internet settings.")
            Language.setMessage(mstrModuleName, 7, "Please Uncheck Login using SSL setting.")
            Language.setMessage(mstrModuleName, 8, "Subject is mandatory information. Please write Subject to continue.")
            Language.setMessage(mstrModuleName, 9, "Email Address is mandatory information. Please Set Email Address to continue.")
            Language.setMessage(mstrModuleName, 10, "Sending Mail(s).")
			Language.setMessage(mstrModuleName, 11, "You can not attach .exe(Executable File) for the security reason.")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class

'S.SANDEEP [30 DEC 2016] -- START {'ENHANCEMENT : VOLTAMP EMAILING REPORT} -- END
'Public Class frmSendMail

'#Region " Private Variables "

'    Private ReadOnly mstrModuleName As String = "frmSendMail"
'    Private mGlobalFontSize As Integer = 0
'    Private objEmail_Master As clsEmail_Master
'    Private mFontStyle As New FontStyle
'    Private fonts() As FontStyle = New FontStyle() {FontStyle.Bold, FontStyle.Italic, FontStyle.Underline}
'    Private rtbTemp As New RichTextBox
'    Private strData As String = ""
'    Private mintLetterUnkid As Integer = 0
'    Private mintModulRefId As Integer = -1
'    Private mblnIsFromDiary As Boolean = False
'    Private dsList As New DataSet
'    Private strMailAddess As String()
'    Private intCount As Integer = 0
'    Private dtRow As DataRow() = Nothing
'    Private dicMailStatus As New Dictionary(Of String, Integer)
'    Private strTextData As String = ""
'    Private strUnsentAddress As String = ""
'    Private strSendAddress As String = ""
'    Private blnShowed As Boolean = False
'    Private mstrReceipentId As String = ""
'    Private blnIsFirstSent As Boolean = False
'    Private strOtherLetter As String = ""
'    Private _rtfSource As New System.Windows.Forms.RichTextBox
'    Private objMfrm As Form
'    'Sandeep [ 07 APRIL 2011 ] -- Start
'    Private mstrPeriodIds As String = String.Empty
'    Private mdicAttachmentInfo As New Dictionary(Of Integer, String)
'    Private mblnIsFromPaySlip As Boolean = False
'    'Sohail (23 Oct 2012) -- Start
'    'TRA - ENHANCEMENT
'    Private mdtPeriodStart As DateTime = Nothing
'    Private mdtPeriodEnd As DateTime = Nothing
'    'Sohail (23 Oct 2012) -- End

'    'Delegate Sub Set_Rich_Delegate(ByVal [rtfDoc] As RichTextBox, ByVal [rtfText] As String)
'    'Sandeep [ 07 APRIL 2011 ] -- End 

'    'S.SANDEEP [ 07 NOV 2011 ] -- START
'    'ENHANCEMENT : TRA CHANGES
'    Private mintCallingRefId As Integer = -1
'    'S.SANDEEP [ 07 NOV 2011 ] -- END


'    'Pinkal (20-Jan-2012) -- Start
'    'Enhancement : TRA Changes
'    Private mblnIsFromSickSheet As Boolean = False
'    Dim mstrSickSheeIDs As String
'    'Pinkal (20-Jan-2012) -- End

'    '<FOR DISABLING CLOSE BUTTON OF FORM PLEASE DO NOT DELETE THIS>
'    Private Const CP_NOCLOSE_BUTTON As Integer = &H200
'    '<FOR DISABLING CLOSE BUTTON OF FORM PLEASE DO NOT DELETE THIS>

'    'S.SANDEEP [ 20 APRIL 2012 ] -- START
'    'ENHANCEMENT : TRA CHANGES
'    Private mdicDiscpline As New Dictionary(Of String, Integer)
'    'S.SANDEEP [ 20 APRIL 2012 ] -- END

'    'S.SANDEEP [ 07 MAY 2012 ] -- START
'    'ENHANCEMENT : TRA CHANGES
'    Private blnIsMailSending As Boolean = False
'    'S.SANDEEP [ 07 MAY 2012 ] -- END



'    'Pinkal (18-Sep-2012) -- Start
'    'Enhancement : TRA Changes

'    'S.SANDEEP [ 09 NOV 2012 ] -- START
'    'ENHANCEMENT : TRA CHANGES
'    'Dim mstrLeaveFormIds As String
'    Dim mstrLeaveFormIds As String = String.Empty
'    'S.SANDEEP [ 09 NOV 2012 ] -- END


'    'Pinkal (18-Sep-2012) -- End


'#End Region

'#Region " Properties "
'    Public Property _ModulRefId() As Integer
'        Get
'            Return mintModulRefId
'        End Get
'        Set(ByVal value As Integer)
'            mintModulRefId = value
'        End Set
'    End Property

'    Public Property _IsFromDiary() As Boolean
'        Get
'            Return mblnIsFromDiary
'        End Get
'        Set(ByVal value As Boolean)
'            mblnIsFromDiary = value
'        End Set
'    End Property

'    Public WriteOnly Property _MForm() As Form
'        Set(ByVal value As Form)
'            objMfrm = value
'        End Set
'    End Property

'    '<FOR DISABLING CLOSE BUTTON OF FORM PLEASE DO NOT DELETE THIS>
'    Protected Overrides ReadOnly Property CreateParams() As CreateParams
'        Get
'            Dim myCp As CreateParams = MyBase.CreateParams
'            myCp.ClassStyle = myCp.ClassStyle Or CP_NOCLOSE_BUTTON
'            Return myCp
'        End Get
'    End Property
'    '<FOR DISABLING CLOSE BUTTON OF FORM PLEASE DO NOT DELETE THIS>
'#End Region

'#Region " Display Dialog "
'    Public Function displayDialog(ByVal intRefId As Integer, ByVal strEmailAddress As String, ByVal dsData As DataSet) As Boolean
'        Try
'            mintModulRefId = intRefId
'            txtToAddress.Text = strEmailAddress
'            dsList = dsData
'            btnTo.Enabled = False

'            Me.ShowDialog()

'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "", mstrModuleName)
'        End Try
'    End Function
'#End Region

'#Region " Private Methods "
'    Public Shared ReadOnly FontSizes As List(Of Single) = New List(Of Single)()

'    Private Shared Sub InitializeFontSizes()
'        FontSizes.AddRange(New Single() {8, 9, 10, 10.5F, 11, 12, 14, 16, 18, 20, 22, 24, 26, 28, 36, 48, 72})
'    End Sub

'    Private Sub FillCombo()
'        Try
'            objcboFont.Items.Clear()
'            For Each obj As FontFamily In FontFamily.Families()
'                objcboFont.Items.Add(obj.Name)
'            Next
'            objcboFontSize.Items.Clear()
'            For i As Integer = 0 To FontSizes.Count - 1
'                objcboFontSize.Items.Add(FontSizes.Item(i).ToString)
'            Next
'            RemoveHandler objcboFont.SelectedIndexChanged, AddressOf cboFont_SelectedIndexChanged
'            objcboFont.SelectedIndex = objcboFont.FindStringExact("Arial")
'            objcboFontSize.SelectedIndex = objcboFontSize.FindStringExact("8")
'            AddHandler objcboFont.SelectedIndexChanged, AddressOf cboFont_SelectedIndexChanged
'        Catch ex As Exception
'            DisplayError.Show(CStr(-1), ex.Message, "FillCombo", mstrModuleName)
'        End Try
'    End Sub

'    Private Sub ApplyStyle(ByVal mStyle As FontStyle, ByVal mAddStyle As Boolean)
'        Try
'            Dim intStart As Integer = rtbDocument.SelectionStart
'            Dim intEnd As Integer = rtbDocument.SelectionLength
'            Dim rtbTempStart As Integer = 0

'            If intEnd <= 1 And rtbDocument.SelectionFont IsNot Nothing Then
'                If mAddStyle Then
'                    rtbDocument.SelectionFont = New Font(rtbDocument.SelectionFont, rtbDocument.SelectionFont.Style Or mStyle)
'                Else
'                    rtbDocument.SelectionFont = New Font(rtbDocument.SelectionFont, rtbDocument.SelectionFont.Style And (Not mStyle))
'                End If
'                Return
'            End If

'            rtbTemp.Rtf = rtbDocument.SelectedRtf

'            For i As Integer = 0 To intEnd
'                rtbTemp.Select(rtbTempStart + i, 1)
'                If mAddStyle Then
'                    rtbTemp.SelectionFont = New Font(rtbTemp.SelectionFont, rtbTemp.SelectionFont.Style Or mStyle)
'                Else
'                    rtbTemp.SelectionFont = New Font(rtbTemp.SelectionFont, rtbTemp.SelectionFont.Style And (Not mStyle))
'                End If
'            Next

'            rtbTemp.Select(rtbTempStart, intEnd)
'            rtbDocument.SelectedRtf = rtbTemp.SelectedRtf
'            rtbDocument.Select(intStart, intEnd)
'            Return
'        Catch ex As Exception
'            'DisplayError.Show(CStr(-1), ex.Message, "ApplyStyle", mstrModuleName)
'        End Try
'    End Sub

'    Private Sub ChangeFont(ByVal mFontName As String)
'        Try
'            Dim intStart As Integer = rtbDocument.SelectionStart
'            Dim intEnd As Integer = rtbDocument.SelectionLength
'            Dim rtbTempStart As Integer = 0
'            Dim mStyle As New FontStyle
'            mStyle = GetDefaultStyle(mFontName)

'            If intEnd <= 1 And rtbDocument.SelectionFont IsNot Nothing Then
'                rtbDocument.SelectionFont = New Font(CStr(mFontName), CInt(IIf(rtbDocument.SelectionFont.Size <> Nothing, rtbDocument.SelectionFont.Size, 8)), mStyle)
'                Return
'            End If

'            rtbTemp.Rtf = rtbDocument.SelectedRtf
'            For i As Integer = 0 To intEnd - 1
'                rtbTemp.Select(rtbTempStart + i, 1)
'                If IsNothing(rtbTemp.SelectionFont) Then
'                    rtbTemp.SelectionFont = New Font(mFontName, CInt(objcboFontSize.Text), mStyle)
'                Else
'                    rtbTemp.SelectionFont = New Font(CStr(mFontName), CInt(IIf(rtbTemp.SelectionFont.Size <> Nothing, rtbTemp.SelectionFont.Size, 8)), mStyle)
'                End If
'            Next
'            rtbTemp.Select(rtbTempStart, intEnd)
'            rtbDocument.SelectedRtf = rtbTemp.SelectedRtf
'            rtbDocument.Select(intStart, intEnd)
'            Return
'        Catch ex As Exception
'            'DisplayError.Show(CStr(-1), ex.Message, "ChangeFont", mstrModuleName)
'        End Try
'    End Sub

'    Private Sub ChangeFontSize(ByVal mFSize As Double)
'        Try
'            If mFSize <= 0 Then
'                Exit Sub
'            End If

'            Dim intStart As Integer = rtbDocument.SelectionStart
'            Dim intEnd As Integer = rtbDocument.SelectionLength
'            Dim rtbTempStart As Integer = 0

'            If intEnd <= 1 And rtbDocument.SelectionFont IsNot Nothing Then
'                rtbDocument.SelectionFont = New Font(rtbDocument.SelectionFont.FontFamily, CInt(mFSize), rtbDocument.SelectionFont.Style)
'                Return
'            End If

'            rtbTemp.Rtf = rtbDocument.SelectedRtf
'            For i As Integer = 0 To intEnd
'                rtbTemp.Select(rtbTempStart + i, 1)
'                Dim mStyle As New FontStyle
'                If rtbDocument.SelectionLength > 0 Then
'                    mStyle = GetDefaultStyle(rtbTemp.SelectionFont.FontFamily.Name)
'                Else
'                    mStyle = GetDefaultStyle(objcboFont.Text)
'                End If
'                If IsNothing(rtbTemp.SelectionFont) Then
'                    rtbTemp.SelectionFont = New Font(objcboFont.Text, CInt(mFSize), mStyle)
'                Else
'                    rtbTemp.SelectionFont = New Font(rtbTemp.SelectionFont.FontFamily, CInt(mFSize), mStyle)
'                End If
'            Next

'            rtbTemp.Select(rtbTempStart, intEnd)
'            rtbDocument.SelectedRtf = rtbTemp.SelectedRtf
'            rtbDocument.Select(intStart, intEnd)

'            Return
'        Catch ex As Exception
'            'DisplayError.Show(CStr(-1), ex.Message, "ChangeFontSize", mstrModuleName)
'        End Try
'    End Sub

'    Private Function GetDefaultStyle(ByVal mFontName As String) As FontStyle
'        For Each fntName As FontFamily In FontFamily.Families
'            If fntName.Name.Equals(mFontName) Then
'                If fntName.IsStyleAvailable(FontStyle.Regular) Then
'                    Return FontStyle.Regular
'                ElseIf fntName.IsStyleAvailable(FontStyle.Bold) Then
'                    Return FontStyle.Bold
'                ElseIf fntName.IsStyleAvailable(FontStyle.Italic) Then
'                    Return FontStyle.Italic
'                ElseIf fntName.IsStyleAvailable(FontStyle.Underline) Then
'                    Return FontStyle.Underline
'                End If
'            End If
'        Next
'    End Function

'    Private Sub RegainOldStyle()
'        Try
'            If objtlbbtnBold.Checked = True Then
'                Call ApplyStyle(FontStyle.Bold, True)
'            Else
'                Call ApplyStyle(FontStyle.Bold, False)
'            End If
'            If objtlbbtnItalic.Checked = True Then
'                Call ApplyStyle(FontStyle.Italic, True)
'            Else
'                Call ApplyStyle(FontStyle.Italic, False)
'            End If
'            If objtlbbtnUnderline.Checked = True Then
'                Call ApplyStyle(FontStyle.Underline, True)
'            Else
'                Call ApplyStyle(FontStyle.Underline, False)
'            End If
'        Catch ex As Exception
'            'DisplayError.Show("-1", ex.Message, "RegainOldStyle", mstrModuleName)
'        End Try
'    End Sub

'    Private Sub SetColor()
'        Try
'            txtToAddress.BackColor = GUI.ColorComp
'            'txtCC.BackColor = GUI.ColorOptional
'            txtSubject.BackColor = GUI.ColorOptional
'            'lstAttachments.BackColor = GUI.ColorOptional
'            objcboFont.BackColor = GUI.ColorOptional
'            objcboFontSize.BackColor = GUI.ColorOptional
'        Catch ex As Exception
'            DisplayError.Show(CStr(-1), ex.Message, "SetColor", mstrModuleName)
'        End Try
'    End Sub

'    Private Sub SetAddress()
'        strMailAddess = txtToAddress.Text.Split(CChar(","))
'        Try
'            For i As Integer = 0 To strMailAddess.Length - 1
'                Dim strFinalAddess As String = ""
'                Dim k As Integer = strMailAddess(i).IndexOf("<")
'                Dim j As Integer = strMailAddess(i).IndexOf(">")
'                If k > 0 Then
'                    strFinalAddess = strMailAddess(i).Substring(k)
'                    strFinalAddess = strFinalAddess.Replace("<", "")
'                    strFinalAddess = strFinalAddess.Replace(">", "")
'                    strMailAddess.SetValue(Trim(strFinalAddess), i)
'                End If
'            Next
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "SetAddress", mstrModuleName)
'        End Try
'    End Sub

'    Private Sub ViewMergeData(ByVal intNumber As Integer)
'        Try
'            RemoveHandler rtbDocument.TextChanged, AddressOf rtbDocument_TextChanged
'            Dim strDataName As String = ""
'            For i As Integer = intNumber To intNumber
'                Dim StrCol As String = ""


'                'Pinkal (18-Sep-2012) -- Start
'                'Enhancement : TRA Changes

'                If mstrLeaveFormIds.Trim.Length <= 0 Then
'                    For j As Integer = 0 To dtRow(0).Table.Columns.Count - 1
'                        StrCol = dtRow(0).Table.Columns(j).ColumnName
'                        If rtbDocument.Rtf.Contains("#" & StrCol & "#") Then
'                            rtbDocument.Focus()

'                            'S.SANDEEP [ 07 MAY 2012 ] -- START
'                            'ENHANCEMENT : TRA CHANGES
'                            If StrCol.Trim.ToUpper = "PASSWORD" Then
'                                If blnIsMailSending = False Then
'                                    strDataName = rtbDocument.Rtf.Replace("#" & StrCol & "#", dtRow(0).Item(StrCol).ToString)
'                                ElseIf blnIsMailSending = True Then
'                                    strDataName = rtbDocument.Rtf.Replace("#" & StrCol & "#", dtRow(0).Item("Original_Password").ToString)
'                                End If
'                            Else
'                                strDataName = rtbDocument.Rtf.Replace("#" & StrCol & "#", dtRow(0).Item(StrCol).ToString)
'                            End If
'                            'S.SANDEEP [ 07 MAY 2012 ] -- END
'                            rtbDocument.Rtf = strDataName
'                        End If
'                    Next

'                Else

'                    For j As Integer = 0 To dtRow(intNumber).Table.Columns.Count - 1
'                        StrCol = dtRow(intNumber).Table.Columns(j).ColumnName
'                        If rtbDocument.Rtf.Contains("#" & StrCol & "#") Then
'                            rtbDocument.Focus()
'                            If StrCol.Trim.ToUpper = "PASSWORD" Then
'                                If blnIsMailSending = False Then
'                                    strDataName = rtbDocument.Rtf.Replace("#" & StrCol & "#", dtRow(intNumber).Item(StrCol).ToString)
'                                ElseIf blnIsMailSending = True Then
'                                    strDataName = rtbDocument.Rtf.Replace("#" & StrCol & "#", dtRow(intNumber).Item("Original_Password").ToString)
'                                End If
'                            Else
'                                strDataName = rtbDocument.Rtf.Replace("#" & StrCol & "#", dtRow(intNumber).Item(StrCol).ToString)
'                            End If
'                            rtbDocument.Rtf = strDataName
'                        End If
'                    Next
'                End If

'                'Pinkal (18-Sep-2012) -- End

'            Next
'            AddHandler rtbDocument.TextChanged, AddressOf rtbDocument_TextChanged
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "ViewMergeData", mstrModuleName)
'        End Try
'    End Sub

'    Private Sub CallFrom(ByVal intModuleRefId As Integer)

'        Try
'            'S.SANDEEP [ 18 JUL 2014 ] -- START
'            If dsList Is Nothing Then dsList = New DataSet
'            'S.SANDEEP [ 18 JUL 2014 ] -- END
'            Select Case intModuleRefId
'                Case enImg_Email_RefId.Employee_Module
'                    If mblnIsFromDiary = False Then
'                        Dim objFrm As New frmEmployeeList
'                        'S.SANDEEP [ 12 OCT 2011 ] -- START
'                        objFrm._IsFromMail = True
'                        'S.SANDEEP [ 12 OCT 2011 ] -- END 
'                        If dsList.Tables.Count > 0 Then
'                            objFrm._DataView = dsList
'                        End If
'                        objFrm.objFooter.Visible = False
'                        objFrm.ShowDialog()
'                        dsList = objFrm._DataView
'                        txtToAddress.Text = ""
'                        If objFrm.EmailList IsNot Nothing Then
'                            For Each str1 As String In objFrm.EmailList
'                                If str1 Is Nothing Then
'                                    Exit For
'                                End If
'                                If txtToAddress.Text.Contains(str1) Then
'                                    Continue For
'                                Else
'                                    txtToAddress.Text &= str1 & " , "
'                                End If
'                            Next
'                        End If
'                    End If
'                    'Sandeep [ 07 APRIL 2011 ] -- Start
'                    mblnIsFromPaySlip = False
'                    'Sandeep [ 07 APRIL 2011 ] -- End 

'                    'Pinkal (20-Jan-2012) -- Start
'                    'Enhancement : TRA Changes
'                    mblnIsFromSickSheet = False
'                    'Pinkal (20-Jan-2012) -- End

'                    'S.SANDEEP [ 18 FEB 2012 ] -- START
'                    'ENHANCEMENT : TRA CHANGES
'                    If txtToAddress.Text.Trim.Length > 0 Then
'                        btnEmployeeDocument.Visible = True
'                    End If
'                    'S.SANDEEP [ 18 FEB 2012 ] -- END

'                Case enImg_Email_RefId.Applicant_Module
'                    Dim objFrm As New frmApplicantList
'                    If dsList.Tables.Count > 0 Then
'                        objFrm._DataView = dsList
'                    End If

'                    'Pinkal (12 Oct 2011)-Start
'                    'ENHANCEMENT : TRA changes
'                    objFrm._IsFromMail = True
'                    'Pinkal (12 Oct 2011)-End 


'                    objFrm.objFooter.Visible = False
'                    objFrm.ShowDialog()
'                    dsList = objFrm._DataView
'                    txtToAddress.Text = ""
'                    If objFrm.EmailList IsNot Nothing Then
'                        For Each str1 As String In objFrm.EmailList
'                            If str1 Is Nothing Then
'                                Exit For
'                            End If
'                            If txtToAddress.Text.Contains(str1) Then
'                                Continue For
'                            Else
'                                txtToAddress.Text &= str1 & " , "
'                            End If
'                        Next
'                    End If
'                    'Sandeep [ 07 APRIL 2011 ] -- Start
'                    mblnIsFromPaySlip = False
'                    'Sandeep [ 07 APRIL 2011 ] -- End 


'                    'Pinkal (20-Jan-2012) -- Start
'                    'Enhancement : TRA Changes
'                    mblnIsFromSickSheet = False
'                    'Pinkal (20-Jan-2012) -- End

'                    'S.SANDEEP [ 18 FEB 2012 ] -- START
'                    'ENHANCEMENT : TRA CHANGES
'                    btnEmployeeDocument.Visible = False
'                    If objFrm.EmailList IsNot Nothing Then
'                        For Each lvItem As ListViewItem In lvAttachedFile.Items
'                            If lvItem.SubItems(objcolhIsEmpDoc.Index).Text = "True" Then lvItem.Remove()
'                        Next
'                    End If
'                    'S.SANDEEP [ 18 FEB 2012 ] -- END

'                Case enImg_Email_RefId.Leave_Module


'                    'Pinkal (18-Sep-2012) -- Start
'                    'Enhancement : TRA Changes

'                    Dim frm As New frmLeaveFormList
'                    frm.objFooter.Visible = False
'                    frm.objEFooter.Visible = True
'                    frm._IsFromMail = True
'                    frm.ShowDialog()
'                    dsList = frm._Email_Data
'                    mstrLeaveFormIds = frm.LeaveFormIDs
'                    txtToAddress.Text = ""
'                    If frm._Email_Data IsNot Nothing Then
'                        For Each dtRow As DataRow In dsList.Tables(0).Rows
'                            If txtToAddress.Text.Contains(dtRow.Item("Email").ToString.Trim) Then
'                                Continue For
'                            Else
'                                txtToAddress.Text &= dtRow.Item("Email").ToString.Trim & ","
'                            End If
'                        Next
'                    End If
'                    mblnIsFromPaySlip = False
'                    mblnIsFromSickSheet = False

'                    'Pinkal (18-Sep-2012) -- End

'                Case enImg_Email_RefId.Payroll_Module
'                    'Sandeep [ 07 APRIL 2011 ] -- Start
'                    Dim frm As New frmPayslipList
'                    frm.objFooter.Visible = False
'                    frm.objEFooter.Visible = True
'                    frm.ShowDialog()
'                    dsList = frm._Email_Data
'                    mstrPeriodIds = frm._PeriodUnkids
'                    'Sohail (23 Oct 2012) -- Start
'                    'TRA - ENHANCEMENT
'                    mdtPeriodStart = frm._PeriodStart_EPayslip
'                    mdtPeriodEnd = frm._PeriodEnd_EPayslip
'                    'Sohail (23 Oct 2012) -- End
'                    txtToAddress.Text = ""
'                    If dsList.Tables.Count > 0 Then
'                        For Each dtRow As DataRow In dsList.Tables(0).Rows
'                            If txtToAddress.Text.Contains(dtRow.Item("Email").ToString.Trim) Then
'                                Continue For
'                            Else
'                                txtToAddress.Text &= dtRow.Item("Email").ToString.Trim & ","
'                            End If
'                        Next
'                    End If
'                    mblnIsFromPaySlip = True
'                    'Sandeep [ 07 APRIL 2011 ] -- End 

'                    'Pinkal (20-Jan-2012) -- Start
'                    'Enhancement : TRA Changes
'                    mblnIsFromSickSheet = False
'                    'Pinkal (20-Jan-2012) -- End


'                    'Pinkal (20-Jan-2012) -- Start
'                    'Enhancement : TRA Changes
'                Case enImg_Email_RefId.Medical_Module

'                    Dim frm As New frmSickSheetList
'                    frm.objFooter.Visible = False
'                    frm.objEFooter.Visible = True
'                    frm._IsFromMail = True
'                    frm.ShowDialog()
'                    dsList = frm._Email_Data
'                    mstrSickSheeIDs = frm.SickSheetIDs
'                    txtToAddress.Text = ""
'                    If dsList.Tables.Count > 0 Then
'                        For Each dtRow As DataRow In dsList.Tables(0).Rows
'                            If txtToAddress.Text.Contains(dtRow.Item("Email").ToString.Trim) Then
'                                Continue For
'                            Else
'                                txtToAddress.Text &= dtRow.Item("Email").ToString.Trim & ","
'                            End If
'                        Next
'                    End If
'                    mblnIsFromPaySlip = False
'                    mblnIsFromSickSheet = True
'                    'Pinkal (20-Jan-2012) -- End
'                    'Case enImg_Email_RefId.Discipline_Module
'                    '     Dim frm As New frmDisciplineResolutionList
'                    '     frm._IsFromMail = True
'                    '     frm.ShowDialog()
'                Case enImg_Email_RefId.Appraisal_Module
'                    Dim frm As New frmAppraisals
'                    frm._IsFromMail = True
'                    frm.ShowDialog()
'                    dsList = frm._EmailData
'                    txtToAddress.Text = ""
'                    If frm.EmailList IsNot Nothing Then
'                        For Each str1 As String In frm.EmailList
'                            If str1 Is Nothing Then
'                                Exit For
'                            End If
'                            If txtToAddress.Text.Contains(str1) Then
'                                Continue For
'                            Else
'                                txtToAddress.Text &= str1 & " , "
'                            End If
'                        Next
'                    End If
'                    mblnIsFromPaySlip = False

'                    'S.SANDEEP [ 18 FEB 2012 ] -- START
'                    'ENHANCEMENT : TRA CHANGES
'                    btnEmployeeDocument.Visible = True
'                    'S.SANDEEP [ 18 FEB 2012 ] -- END

'                Case enImg_Email_RefId.Discipline_Module 'S.SANDEEP [ 20 APRIL 2012 ]
'                    Dim frm As New frmDisciplineFilingList
'                    frm._FromMail = True
'                    frm.ShowDialog()
'                    dsList = frm._EmailData
'                    txtToAddress.Text = ""
'                    If frm._Emails IsNot Nothing Then
'                        For Each str1 As String In frm._Emails
'                            If str1 Is Nothing Then
'                                Exit For
'                            End If
'                            txtToAddress.Text &= str1 & " , "
'                        Next
'                    End If
'                    mdicDiscpline = frm._dicDisciplineFile
'                    frm = Nothing
'            End Select

'            'S.SANDEEP [ 07 NOV 2011 ] -- START
'            'ENHANCEMENT : TRA CHANGES
'            mintCallingRefId = intModuleRefId
'            'S.SANDEEP [ 07 NOV 2011 ] -- END

'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "CallFrom", mstrModuleName)
'        End Try

'    End Sub

'    'Sandeep [ 07 APRIL 2011 ] -- Start
'    Private Sub Export_EPayslip()
'        Dim objPaySlpReport As New ArutiReports.clsPaySlip
'        Try
'            Dim strReportName As String = String.Empty
'            Dim strOriginalReportName As String = String.Empty

'            Cursor.Current = Cursors.WaitCursor 'Sohail (23 Oct 2012)

'            objPaySlpReport._AnalysisRefId = enAnalysisRef.Employee
'            objPaySlpReport._PeriodIds = mstrPeriodIds
'            'Sohail (23 Oct 2012) -- Start
'            'TRA - ENHANCEMENT
'            objPaySlpReport._PeriodStart = mdtPeriodStart
'            objPaySlpReport._PeriodEnd = mdtPeriodEnd
'            'Sohail (23 Oct 2012) -- End

'            'Sohail (21 May 2016) -- Start
'            'Enhancement - 60.1 - Show Period wise allocations on Payslip Report.
'            Dim dicPeriod As New Dictionary(Of Integer, String)
'            For Each strId As String In mstrPeriodIds.Split(CChar(","))
'                Dim objPeriod As New clscommom_period_Tran
'                objPeriod._Periodunkid(FinancialYear._Object._DatabaseName) = CInt(strId)
'                If dicPeriod.ContainsKey(CInt(strId)) = False Then
'                    dicPeriod.Add(CInt(strId), eZeeDate.convertDate(objPeriod._End_Date))
'                End If
'            Next
'            objPaySlpReport._Dic_Period = dicPeriod
'            'Sohail (21 May 2016) -- End

'            'Sohail (16 May 2012) -- Start
'            'TRA - ENHANCEMENT
'            objPaySlpReport._IgnoreZeroValueHead = ConfigParameter._Object._IgnoreZeroValueHeadsOnPayslip
'            objPaySlpReport._ShowLoanBalance = ConfigParameter._Object._ShowLoanBalanceOnPayslip
'            objPaySlpReport._ShowSavingBalance = ConfigParameter._Object._ShowSavingBalanceOnPayslip
'            objPaySlpReport._ShowEmployerContribution = ConfigParameter._Object._ShowEmployerContributionOnPayslip
'            objPaySlpReport._ShowAllHeads = ConfigParameter._Object._ShowAllHeadsOnPayslip
'            objPaySlpReport._ShowSalaryOnHold = ConfigParameter._Object._ShowSalaryOnHoldOnPayslip
'            objPaySlpReport._LeaveBalanceSetting = ConfigParameter._Object._LeaveBalanceSetting
'            'Sohail (14 May 2015) -- Start
'            'Enhancement - Default setting of Payslip on Configuration.
'            objPaySlpReport._ShowBirthDate = ConfigParameter._Object._ShowBirthDateOnPayslip
'            objPaySlpReport._LeaveTypeId = ConfigParameter._Object._LeaveTypeOnPayslip
'            objPaySlpReport._ShowAge = ConfigParameter._Object._ShowAgeOnPayslip
'            objPaySlpReport._ShowCategory = ConfigParameter._Object._ShowCategoryOnPayslip
'            objPaySlpReport._ShowPaymentDetails = ConfigParameter._Object._ShowPaymentDetailsOnPayslip
'            objPaySlpReport._ShowNo_of_Dependents = ConfigParameter._Object._ShowNoOfDependantsOnPayslip
'            objPaySlpReport._ShowCompanyName = ConfigParameter._Object._ShowCompanyNameOnPayslip
'            objPaySlpReport._ShowCumulativeAccrual = ConfigParameter._Object._ShowCumulativeAccrualOnPayslip
'            objPaySlpReport._ShowInformationalHeads = ConfigParameter._Object._ShowInformationalHeadsOnPayslip
'            objPaySlpReport._ShowMembership = ConfigParameter._Object._ShowMembershipOnPayslip
'            objPaySlpReport._ShowMonthlySalary = ConfigParameter._Object._ShowMonthlySalaryOnPayslip
'            'Sohail (14 May 2015) -- End


'            'Anjan [01 September 2016] -- Start
'            'ENHANCEMENT : Including Payslip settings on configuration.
'            objPaySlpReport._ShowBankAccountNo = ConfigParameter._Object._ShowBankAccountNoOnPayslip
'            'Anjan [01 Sepetember 2016] -- End


'            Select Case ConfigParameter._Object._PayslipTemplate
'                Case 2 'TRA Format
'                    objPaySlpReport._IsLogoCompanyInfo = False
'                    objPaySlpReport._IsOnlyCompanyInfo = False
'                    objPaySlpReport._IsOnlyLogo = True
'                Case Else
'                    Select Case ConfigParameter._Object._LogoOnPayslip
'                        Case enLogoOnPayslip.LOGO_AND_COMPANY_DETAILS
'                            objPaySlpReport._IsLogoCompanyInfo = True
'                            objPaySlpReport._IsOnlyCompanyInfo = False
'                            objPaySlpReport._IsOnlyLogo = False
'                        Case enLogoOnPayslip.COMPANY_DETAILS
'                            objPaySlpReport._IsLogoCompanyInfo = False
'                            objPaySlpReport._IsOnlyCompanyInfo = True
'                            objPaySlpReport._IsOnlyLogo = False
'                        Case enLogoOnPayslip.LOGO
'                            objPaySlpReport._IsLogoCompanyInfo = False
'                            objPaySlpReport._IsOnlyCompanyInfo = False
'                            objPaySlpReport._IsOnlyLogo = True
'                    End Select
'            End Select


'            'Sohail (16 May 2012) -- End
'            strReportName = objPaySlpReport._ReportName
'            strOriginalReportName = strReportName

'            If System.IO.Directory.Exists(mstrExportEPaySlipPath) = False Then
'                System.IO.Directory.CreateDirectory(mstrExportEPaySlipPath)
'            End If

'            For Each dtRow As DataRow In dsList.Tables(0).Rows
'                objPaySlpReport._EmployeeIds = dtRow.Item("EmpId").ToString
'                strReportName = dtRow.Item("EmpId").ToString & "_" & objPaySlpReport._ReportName.Replace(" ", "_")
'                'Sohail (16 May 2012) -- Start
'                'TRA - ENHANCEMENT
'                'objPaySlpReport._ReportName = strReportName & ConfigParameter._Object._CurrentDateAndTime.ToString("yyymmddhhmmss")
'                'objPaySlpReport.Send_EPayslip(objPaySlpReport._ReportName.ToString, mstrExportEPaySlipPath)
'                strReportName = strReportName & ConfigParameter._Object._CurrentDateAndTime.ToString("yyymmddhhmmss")
'                objPaySlpReport.setDefaultOrderBy(0)
'                'Sohail (21 Aug 2015) -- Start
'                'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
'                'objPaySlpReport.Send_EPayslip(strReportName, mstrExportEPaySlipPath)
'                'Sohail (26 Feb 2016) -- Start
'                'Enhancement - Email Payslip to have password protection (request from KBC and CCK) (41# KBC & Kenya Project Comments List.xls)
'                'objPaySlpReport.Send_EPayslip(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, mdtPeriodStart, mdtPeriodEnd, ConfigParameter._Object._UserAccessModeSetting, True, True, True, strReportName, mstrExportEPaySlipPath, ConfigParameter._Object._CurrencyFormat, ConfigParameter._Object._PayslipTemplate, FinancialYear._Object._Database_Start_Date, FinancialYear._Object._Database_End_Date, ConfigParameter._Object._HourAmtMapping, ConfigParameter._Object._ShowBranchOnPayslip, ConfigParameter._Object._ShowDepartmentGroupOnPayslip, ConfigParameter._Object._ShowDepartmentOnPayslip, ConfigParameter._Object._ShowSectionGroupOnPayslip, ConfigParameter._Object._ShowSectionOnPayslip, ConfigParameter._Object._ShowUnitGroupOnPayslip, ConfigParameter._Object._ShowUnitOnPayslip, ConfigParameter._Object._ShowTeamOnPayslip, ConfigParameter._Object._ShowClassGroupOnPayslip, ConfigParameter._Object._ShowClassOnPayslip, ConfigParameter._Object._ActualHeadId, ConfigParameter._Object._AmtToBePaidHeadId, ConfigParameter._Object._EmplCostHeadId, ConfigParameter._Object._WorkingDaysHeadId, ConfigParameter._Object._TaxableAmountHeadId, ConfigParameter._Object._IPRCalculationHeadId, ConfigParameter._Object._13thPayHeadId, ConfigParameter._Object._NoOfDependantHeadId, ConfigParameter._Object._NoOfChildHeadId, ConfigParameter._Object._RoundOff_Type, Company._Object._Stamp, Company._Object._Name, User._Object._Username, ConfigParameter._Object._ShowCompanyStampOnPayslip, ConfigParameter._Object._ShowJobOnPayslip, ConfigParameter._Object._ShowGradeOnPayslip, ConfigParameter._Object._ShowGradeGroupOnPayslip, ConfigParameter._Object._ShowGradeLevelOnPayslip, ConfigParameter._Object._ShowAppointmentDateOnPayslip, ConfigParameter._Object._ShowRetirementDateOnPayslip, ConfigParameter._Object._ShowEOCDateOnPayslip, ConfigParameter._Object._ShowLeavingDateOnPayslip, ConfigParameter._Object._ShowCompanyNameOnPayslip, ConfigParameter._Object._ShowEmployeeCodeOnPayslip, ConfigParameter._Object._ShowMembershipOnPayslip, ConfigParameter._Object._ShowPaymentDetailsOnPayslip, Company._Object._Company_Reg_No, Company._Object._Tinno, Company._Object._Nssfno, Company._Object._Registerdno, Company._Object._Address1, Company._Object._Address2, Company._Object._State_Name, Company._Object._City_Name, Company._Object._Country_Name, Company._Object._Phone1, Company._Object._Email, Company._Object._Website, ConfigParameter._Object._ShowExchangeRateOnPayslip)
'                'Sohail (04 Aug 2016) -- Start
'                'Enhancement - 63.1 - Option on configuration to Send Password protected E-Payslip.
'                'objPaySlpReport.Send_EPayslip(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, mdtPeriodStart, mdtPeriodEnd, ConfigParameter._Object._UserAccessModeSetting, True, True, True, strReportName, mstrExportEPaySlipPath, ConfigParameter._Object._CurrencyFormat, ConfigParameter._Object._PayslipTemplate, FinancialYear._Object._Database_Start_Date, FinancialYear._Object._Database_End_Date, ConfigParameter._Object._HourAmtMapping, ConfigParameter._Object._ShowBranchOnPayslip, ConfigParameter._Object._ShowDepartmentGroupOnPayslip, ConfigParameter._Object._ShowDepartmentOnPayslip, ConfigParameter._Object._ShowSectionGroupOnPayslip, ConfigParameter._Object._ShowSectionOnPayslip, ConfigParameter._Object._ShowUnitGroupOnPayslip, ConfigParameter._Object._ShowUnitOnPayslip, ConfigParameter._Object._ShowTeamOnPayslip, ConfigParameter._Object._ShowClassGroupOnPayslip, ConfigParameter._Object._ShowClassOnPayslip, ConfigParameter._Object._ActualHeadId, ConfigParameter._Object._AmtToBePaidHeadId, ConfigParameter._Object._EmplCostHeadId, ConfigParameter._Object._WorkingDaysHeadId, ConfigParameter._Object._TaxableAmountHeadId, ConfigParameter._Object._IPRCalculationHeadId, ConfigParameter._Object._13thPayHeadId, ConfigParameter._Object._NoOfDependantHeadId, ConfigParameter._Object._NoOfChildHeadId, ConfigParameter._Object._RoundOff_Type, Company._Object._Stamp, Company._Object._Name, User._Object._Username, ConfigParameter._Object._ShowCompanyStampOnPayslip, ConfigParameter._Object._ShowJobOnPayslip, ConfigParameter._Object._ShowGradeOnPayslip, ConfigParameter._Object._ShowGradeGroupOnPayslip, ConfigParameter._Object._ShowGradeLevelOnPayslip, ConfigParameter._Object._ShowAppointmentDateOnPayslip, ConfigParameter._Object._ShowRetirementDateOnPayslip, ConfigParameter._Object._ShowEOCDateOnPayslip, ConfigParameter._Object._ShowLeavingDateOnPayslip, ConfigParameter._Object._ShowCompanyNameOnPayslip, ConfigParameter._Object._ShowEmployeeCodeOnPayslip, ConfigParameter._Object._ShowMembershipOnPayslip, ConfigParameter._Object._ShowPaymentDetailsOnPayslip, Company._Object._Company_Reg_No, Company._Object._Tinno, Company._Object._Nssfno, Company._Object._Registerdno, Company._Object._Address1, Company._Object._Address2, Company._Object._State_Name, Company._Object._City_Name, Company._Object._Country_Name, Company._Object._Phone1, Company._Object._Email, Company._Object._Website, ConfigParameter._Object._ShowExchangeRateOnPayslip, dtRow.Item("Password").ToString)
'                'Sohail (20 Sep 2016) -- Start
'                'Enhancement - 63.1 - New Payslip Template 15 for TIMAJA.
'                'objPaySlpReport.Send_EPayslip(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, mdtPeriodStart, mdtPeriodEnd, ConfigParameter._Object._UserAccessModeSetting, True, True, True, strReportName, mstrExportEPaySlipPath, ConfigParameter._Object._CurrencyFormat, ConfigParameter._Object._PayslipTemplate, FinancialYear._Object._Database_Start_Date, FinancialYear._Object._Database_End_Date, ConfigParameter._Object._HourAmtMapping, ConfigParameter._Object._ShowBranchOnPayslip, ConfigParameter._Object._ShowDepartmentGroupOnPayslip, ConfigParameter._Object._ShowDepartmentOnPayslip, ConfigParameter._Object._ShowSectionGroupOnPayslip, ConfigParameter._Object._ShowSectionOnPayslip, ConfigParameter._Object._ShowUnitGroupOnPayslip, ConfigParameter._Object._ShowUnitOnPayslip, ConfigParameter._Object._ShowTeamOnPayslip, ConfigParameter._Object._ShowClassGroupOnPayslip, ConfigParameter._Object._ShowClassOnPayslip, ConfigParameter._Object._ActualHeadId, ConfigParameter._Object._AmtToBePaidHeadId, ConfigParameter._Object._EmplCostHeadId, ConfigParameter._Object._WorkingDaysHeadId, ConfigParameter._Object._TaxableAmountHeadId, ConfigParameter._Object._IPRCalculationHeadId, ConfigParameter._Object._13thPayHeadId, ConfigParameter._Object._NoOfDependantHeadId, ConfigParameter._Object._NoOfChildHeadId, ConfigParameter._Object._RoundOff_Type, Company._Object._Stamp, Company._Object._Name, User._Object._Username, ConfigParameter._Object._ShowCompanyStampOnPayslip, ConfigParameter._Object._ShowJobOnPayslip, ConfigParameter._Object._ShowGradeOnPayslip, ConfigParameter._Object._ShowGradeGroupOnPayslip, ConfigParameter._Object._ShowGradeLevelOnPayslip, ConfigParameter._Object._ShowAppointmentDateOnPayslip, ConfigParameter._Object._ShowRetirementDateOnPayslip, ConfigParameter._Object._ShowEOCDateOnPayslip, ConfigParameter._Object._ShowLeavingDateOnPayslip, ConfigParameter._Object._ShowCompanyNameOnPayslip, ConfigParameter._Object._ShowEmployeeCodeOnPayslip, ConfigParameter._Object._ShowMembershipOnPayslip, ConfigParameter._Object._ShowPaymentDetailsOnPayslip, Company._Object._Company_Reg_No, Company._Object._Tinno, Company._Object._Nssfno, Company._Object._Registerdno, Company._Object._Address1, Company._Object._Address2, Company._Object._State_Name, Company._Object._City_Name, Company._Object._Country_Name, Company._Object._Phone1, Company._Object._Email, Company._Object._Website, ConfigParameter._Object._ShowExchangeRateOnPayslip, dtRow.Item("Password").ToString, ConfigParameter._Object._SendPasswordProtectedEPayslip)
'                objPaySlpReport.Send_EPayslip(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid, _
'                                              Company._Object._Companyunkid, mdtPeriodStart, mdtPeriodEnd, ConfigParameter._Object._UserAccessModeSetting, _
'                                              True, True, True, strReportName, mstrExportEPaySlipPath, ConfigParameter._Object._CurrencyFormat, _
'                                              ConfigParameter._Object._PayslipTemplate, FinancialYear._Object._Database_Start_Date, FinancialYear._Object._Database_End_Date, _
'                                              ConfigParameter._Object._HourAmtMapping, ConfigParameter._Object._ShowBranchOnPayslip, _
'                                              ConfigParameter._Object._ShowDepartmentGroupOnPayslip, ConfigParameter._Object._ShowDepartmentOnPayslip, _
'                                              ConfigParameter._Object._ShowSectionGroupOnPayslip, ConfigParameter._Object._ShowSectionOnPayslip, _
'                                              ConfigParameter._Object._ShowUnitGroupOnPayslip, ConfigParameter._Object._ShowUnitOnPayslip, _
'                                              ConfigParameter._Object._ShowTeamOnPayslip, ConfigParameter._Object._ShowClassGroupOnPayslip, _
'                                              ConfigParameter._Object._ShowClassOnPayslip, ConfigParameter._Object._ActualHeadId, ConfigParameter._Object._AmtToBePaidHeadId, _
'                                              ConfigParameter._Object._EmplCostHeadId, ConfigParameter._Object._WorkingDaysHeadId, ConfigParameter._Object._TaxableAmountHeadId, _
'                                              ConfigParameter._Object._IPRCalculationHeadId, ConfigParameter._Object._13thPayHeadId, ConfigParameter._Object._NoOfDependantHeadId, _
'                                              ConfigParameter._Object._NoOfChildHeadId, ConfigParameter._Object._RoundOff_Type, Company._Object._Stamp, _
'                                              Company._Object._Name, User._Object._Username, ConfigParameter._Object._ShowCompanyStampOnPayslip, _
'                                              ConfigParameter._Object._ShowJobOnPayslip, ConfigParameter._Object._ShowGradeOnPayslip, ConfigParameter._Object._ShowGradeGroupOnPayslip, _
'                                              ConfigParameter._Object._ShowGradeLevelOnPayslip, ConfigParameter._Object._ShowAppointmentDateOnPayslip, _
'                                              ConfigParameter._Object._ShowRetirementDateOnPayslip, ConfigParameter._Object._ShowEOCDateOnPayslip, _
'                                              ConfigParameter._Object._ShowLeavingDateOnPayslip, ConfigParameter._Object._ShowCompanyNameOnPayslip, _
'                                              ConfigParameter._Object._ShowEmployeeCodeOnPayslip, ConfigParameter._Object._ShowMembershipOnPayslip, _
'                                              ConfigParameter._Object._ShowPaymentDetailsOnPayslip, Company._Object._Company_Reg_No, Company._Object._Tinno, _
'                                              Company._Object._Nssfno, Company._Object._Registerdno, Company._Object._Address1, Company._Object._Address2, _
'                                              Company._Object._State_Name, Company._Object._City_Name, Company._Object._Country_Name, Company._Object._Phone1, _
'                                              Company._Object._Email, Company._Object._Website, ConfigParameter._Object._ShowExchangeRateOnPayslip, _
'                                              dtRow.Item("Password").ToString, ConfigParameter._Object._SendPasswordProtectedEPayslip, _
'                                              ConfigParameter._Object._CumulativeTaxHeadId, ConfigParameter._Object._DailyBasicSalaryHeadId, ConfigParameter._Object._AccruedLeavePayHeadId, _
'                                              ConfigParameter._Object._LeaveTakenHeadID, ConfigParameter._Object._LeaveAccruedDaysHeadID, _
'                                              ConfigParameter._Object._TaxFreeIncomeHeadID, ConfigParameter._Object._AssessableIncomeHeadID, _
'                                              ConfigParameter._Object._NAPSATaxHeadID)
'                'Nilay (24-Oct-2016) -- [ConfigParameter._Object._NAPSATaxHeadID]
'                'Nilay (03-Oct-2016) -- [ConfigParameter._Object._LeaveTakenHeadID, ConfigParameter._Object._LeaveAccruedDaysHeadID, ConfigParameter._Object._TaxFreeIncomeHeadID, ConfigParameter._Object._AssessableIncomeHeadID]
'                'Sohail (20 Sep 2016) -- End
'                'Sohail (04 Aug 2016) -- End
'                'Sohail (26 Feb 2016) -- End
'                'Sohail (21 Aug 2015) -- End
'                'Sohail (16 May 2012) -- End
'                If mdicAttachmentInfo.ContainsKey(CInt(dtRow.Item("EmpId"))) Then
'                    'Sohail (16 May 2012) -- Start
'                    'TRA - ENHANCEMENT
'                    'mdicAttachmentInfo(CInt(dtRow.Item("EmpId"))) = mdicAttachmentInfo(CInt(dtRow.Item("EmpId"))) & "," & objPaySlpReport._ReportName & ".pdf"
'                    mdicAttachmentInfo(CInt(dtRow.Item("EmpId"))) = mdicAttachmentInfo(CInt(dtRow.Item("EmpId"))) & "," & strReportName & ".pdf"
'                    'Sohail (16 May 2012) -- End
'                Else
'                    'Sohail (16 May 2012) -- Start
'                    'TRA - ENHANCEMENT
'                    'mdicAttachmentInfo.Add(CInt(dtRow.Item("EmpId")), objPaySlpReport._ReportName & ".pdf")
'                    mdicAttachmentInfo.Add(CInt(dtRow.Item("EmpId")), strReportName & ".pdf")
'                    'Sohail (16 May 2012) -- End
'                End If
'                objPaySlpReport._ReportName = strOriginalReportName
'            Next

'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "Export_EPayslip", mstrModuleName)
'        Finally
'            objPaySlpReport = Nothing
'            Cursor.Current = Cursors.Default 'Sohail (23 Oct 2012)
'        End Try
'    End Sub

'    'Private Sub Set_Rich_Rtf(ByVal [rtfDoc] As RichTextBox, ByVal [rtfText] As String)
'    '    If rtfDoc.InvokeRequired Then
'    '        Dim MyDelegate As New Set_Rich_Delegate(AddressOf Set_Rich_Rtf)
'    '        Me.Invoke(MyDelegate, New Object() {[rtfDoc], [rtfText]})
'    '    Else
'    '        [rtfDoc].Rtf = [rtfText]
'    '    End If
'    'End Sub
'    'Sandeep [ 07 APRIL 2011 ] -- End 



'    'Pinkal (20-Jan-2012) -- Start
'    'Enhancement : TRA Changes
'    Private Sub Export_ESickSheet()
'        Dim objSickSheetReport As New ArutiReports.clsSickSheetReport
'        Try
'            Dim strReportName As String = String.Empty
'            Dim strOriginalReportName As String = String.Empty

'            strReportName = "Sick Sheet"
'            strOriginalReportName = strReportName

'            If System.IO.Directory.Exists(mstrExportEPaySlipPath) = False Then
'                System.IO.Directory.CreateDirectory(mstrExportEPaySlipPath)
'            End If

'            For Each dtRow As DataRow In dsList.Tables(0).Rows
'                objSickSheetReport._SickSheetunkid = dtRow.Item("sicksheetunkid").ToString
'                objSickSheetReport._Employeeunkid = dtRow.Item("EmpId").ToString
'                strReportName = dtRow.Item("EmpCode").ToString & "_" & strReportName.Replace(" ", "_")
'                strReportName &= eZeeDate.convertDate(CDate(dtRow.Item("sicksheetdate").ToString))

'                'Pinkal (16-Apr-2016) -- Start
'                'Enhancement - Implementing Date Format (DD-MMM-YYYY) as Per Mr.Andrew's Comment.
'                'objSickSheetReport.Send_ESickSheet(strReportName, mstrExportEPaySlipPath)
'                objSickSheetReport.Send_ESickSheet(eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), strReportName, mstrExportEPaySlipPath)
'                'Pinkal (16-Apr-2016) -- End

'                If mdicAttachmentInfo.ContainsKey(CInt(dtRow.Item("EmpId"))) Then
'                    mdicAttachmentInfo(CInt(dtRow.Item("EmpId"))) = mdicAttachmentInfo(CInt(dtRow.Item("EmpId"))) & "," & strReportName & ".pdf"
'                Else
'                    mdicAttachmentInfo.Add(CInt(dtRow.Item("EmpId")), strReportName & ".pdf")
'                End If
'                strReportName = strOriginalReportName
'            Next

'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "Export_ESickSheet", mstrModuleName)
'        Finally
'            objSickSheetReport = Nothing
'        End Try
'    End Sub
'    'Pinkal (20-Jan-2012) -- End

'    'Pinkal (06-Mar-2014) -- Start
'    'Enhancement : TRA Changes  Leave form in ESS is not user friendly to employees. They system is asking for leave form number which employee does not know. 


'    'Pinkal (24-Aug-2015) -- Start
'    'Enhancement - WORKING ON ACTIVE EMPLOYEE CONDITION.

'    'Public Function Export_ELeaveForm(ByVal mstrFormNo As String, ByVal mintEmployeeID As Integer, ByVal mintFormId As Integer, _
'    '                                  ByVal intLeaveBalanceSetting As Integer, ByVal intYearId As Integer, ByVal mdtdbStartDate As Date, _
'    '                                  ByVal mdtdbEndDate As Date, ByVal mblnShowLeaveExpense As Boolean) As String

'    '    'Pinkal (13-Jul-2015) -- WORKING ON C & R ACCESS PRIVILEGE. [ByVal mblnShowLeaveExpense As Boolean]

'    '    Dim objLeaveFormReport As New ArutiReports.clsEmployeeLeaveForm
'    '    Dim StrPath As String = ""
'    '    Try
'    '        Dim strReportName As String = String.Empty
'    '        Dim strOriginalReportName As String = String.Empty

'    '        strReportName = "Employee Leave Form"
'    '        strOriginalReportName = strReportName

'    '        If System.IO.Directory.Exists(mstrExportEPaySlipPath) = False Then
'    '            System.IO.Directory.CreateDirectory(mstrExportEPaySlipPath)
'    '        End If

'    '        strReportName = mstrFormNo & "_" & strReportName.Replace(" ", "_") & ConfigParameter._Object._CurrentDateAndTime.ToString("yyymmddhhmmss")
'    '        objLeaveFormReport._LeaveFormNo = mstrFormNo
'    '        objLeaveFormReport._LeaveFormId = mintFormId

'    '        'Pinkal (11-Jun-2014) -- Start
'    '        'Enhancement : TRA Changes [Problem In Leave Form Attachment]
'    '        objLeaveFormReport._LeaveBalanceSetting = intLeaveBalanceSetting
'    '        objLeaveFormReport._YearId = intYearId
'    '        objLeaveFormReport._Fin_StartDate = mdtdbStartDate
'    '        objLeaveFormReport._Fin_Enddate = mdtdbEndDate
'    '        'Pinkal (11-Jun-2014) -- End


'    '        'Pinkal (13-Jul-2015) -- Start
'    '        'Enhancement - WORKING ON C & R ACCESS PRIVILEGE.
'    '        objLeaveFormReport._ShowLeaveExpense = mblnShowLeaveExpense
'    '        'Pinkal (13-Jul-2015) -- End

'    '        objLeaveFormReport.Send_ELeaveForm(strReportName, mstrExportEPaySlipPath)
'    '        If mdicAttachmentInfo.ContainsKey(mintEmployeeID) Then
'    '            mdicAttachmentInfo(mintEmployeeID) = mdicAttachmentInfo(mintEmployeeID) & "," & strReportName & ".pdf"
'    '        Else
'    '            mdicAttachmentInfo.Add(mintEmployeeID, strReportName & ".pdf")
'    '        End If

'    '        StrPath = strReportName.Replace(" ", "_") & ".pdf"
'    '        strReportName = strOriginalReportName

'    '    Catch ex As Exception
'    '        DisplayError.Show("-1", ex.Message, "Export_ELeaveForm", mstrModuleName)
'    '    Finally
'    '        objLeaveFormReport = Nothing
'    '    End Try
'    '    Return StrPath
'    'End Function

'    Public Function Export_ELeaveForm(ByVal xDatabaseName As String, ByVal xUserUnkid As Integer, ByVal xYearUnkid As Integer _
'                                                                , ByVal xCompanyUnkid As Integer, ByVal mdtEmployeeAsonDate As Date, ByVal xOnlyApproved As Boolean _
'                                                                , ByVal xUserModeSetting As String, ByVal mstrFormNo As String, ByVal mintEmployeeID As Integer, ByVal mintFormId As Integer _
'                                                                , ByVal intLeaveBalanceSetting As Integer, ByVal intYearId As Integer, ByVal mdtdbStartDate As Date _
'                                                                , ByVal mdtdbEndDate As Date, ByVal mblnShowLeaveExpense As Boolean) As String

'        'Pinkal (13-Jul-2015) -- WORKING ON C & R ACCESS PRIVILEGE. [ByVal mblnShowLeaveExpense As Boolean]

'        Dim objLeaveFormReport As New ArutiReports.clsEmployeeLeaveForm
'        Dim StrPath As String = ""
'        Try
'            Dim strReportName As String = String.Empty
'            Dim strOriginalReportName As String = String.Empty

'            strReportName = "Employee Leave Form"
'            strOriginalReportName = strReportName

'            If System.IO.Directory.Exists(mstrExportEPaySlipPath) = False Then
'                System.IO.Directory.CreateDirectory(mstrExportEPaySlipPath)
'            End If

'            strReportName = mstrFormNo & "_" & strReportName.Replace(" ", "_") & ConfigParameter._Object._CurrentDateAndTime.ToString("yyymmddhhmmss")
'            objLeaveFormReport._LeaveFormNo = mstrFormNo
'            objLeaveFormReport._LeaveFormId = mintFormId

'            'Pinkal (11-Jun-2014) -- Start
'            'Enhancement : TRA Changes [Problem In Leave Form Attachment]
'            objLeaveFormReport._LeaveBalanceSetting = intLeaveBalanceSetting
'            objLeaveFormReport._YearId = intYearId
'            objLeaveFormReport._Fin_StartDate = mdtdbStartDate
'            objLeaveFormReport._Fin_Enddate = mdtdbEndDate
'            'Pinkal (11-Jun-2014) -- End


'            'Pinkal (13-Jul-2015) -- Start
'            'Enhancement - WORKING ON C & R ACCESS PRIVILEGE.
'            objLeaveFormReport._ShowLeaveExpense = mblnShowLeaveExpense
'            'Pinkal (13-Jul-2015) -- End


'            'Pinkal (24-Aug-2015) -- Start
'            'Enhancement - WORKING ON ACTIVE EMPLOYEE CONDITION.
'            'objLeaveFormReport.Send_ELeaveForm(strReportName, mstrExportEPaySlipPath)
'            objLeaveFormReport._EmployeeId = mintEmployeeID
'            objLeaveFormReport.Send_ELeaveForm(xDatabaseName, xUserUnkid, xYearUnkid _
'                                                                    , xCompanyUnkid, mdtEmployeeAsonDate, xOnlyApproved, xUserModeSetting _
'                                                                    , strReportName, mstrExportEPaySlipPath)
'            'Pinkal (24-Aug-2015) -- End


'            If mdicAttachmentInfo.ContainsKey(mintEmployeeID) Then
'                mdicAttachmentInfo(mintEmployeeID) = mdicAttachmentInfo(mintEmployeeID) & "," & strReportName & ".pdf"
'            Else
'                mdicAttachmentInfo.Add(mintEmployeeID, strReportName & ".pdf")
'            End If

'            StrPath = strReportName.Replace(" ", "_") & ".pdf"
'            strReportName = strOriginalReportName

'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "Export_ELeaveForm", mstrModuleName)
'        Finally
'            objLeaveFormReport = Nothing
'        End Try
'        Return StrPath
'    End Function

'    'Pinkal (24-Aug-2015) -- End

'    'Pinkal (06-Mar-2014) -- End



'#End Region

'#Region " Form's Events "

'    Private Sub frmSendMail_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
'        mblnIsSendDisposed = True
'    End Sub

'    Private Sub frmSendMail_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
'        Try
'            Call Set_Logo(Me, gApplicationType)
'            Call Language.setLanguage(Me.Name)
'            pnlColor.Hide()
'            If FontSizes.Count <= 0 Then
'                InitializeFontSizes()
'            End If
'            Call FillCombo()
'            If rtbDocument.SelectedText.Length = 0 Then
'                mnuCut.Enabled = False
'                mnuCopy.Enabled = False
'                mnuPaste.Enabled = False
'            End If
'            If txtToAddress.Text = "" Then
'                btnViewMerge.Enabled = False
'                btnFinish.Enabled = False
'            End If
'            objbtnNext.Visible = False
'            objbtnPrevious.Visible = False

'            'S.SANDEEP [ 18 FEB 2012 ] -- START
'            'ENHANCEMENT : TRA CHANGES
'            btnEmployeeDocument.Visible = False
'            'S.SANDEEP [ 18 FEB 2012 ] -- END

'            'S.SANDEEP [ 07 MAY 2012 ] -- START
'            'ENHANCEMENT : TRA CHANGES
'            objlblCaption.Visible = False
'            'S.SANDEEP [ 07 MAY 2012 ] -- END

'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "frmSendMail_Load", mstrModuleName)
'        End Try
'    End Sub

'#End Region

'#Region " ToolStrip Events "

'#Region " Font And Size "

'    Private Sub cboFontSize_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles objcboFontSize.KeyPress
'        Try
'            If Not (Asc(e.KeyChar) > 47 And Asc(e.KeyChar) < 58) Then
'                If (Asc(e.KeyChar) = Asc(GUI.DecimalSeparator) And InStr(Trim(objcboFontSize.Text), GUI.DecimalSeparator) = 0) Or Asc(e.KeyChar) = 8 Then Exit Sub
'                e.KeyChar = CChar("")
'            End If
'        Catch ex As Exception
'            DisplayError.Show(CStr(-1), ex.Message, "cboFontSize_KeyPress", mstrModuleName)
'        End Try
'    End Sub

'    Private Sub cboFontSize_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles objcboFontSize.TextChanged
'        Try
'            If objcboFontSize.Text = "" Or objcboFontSize.Text = "." Then
'                Exit Sub
'            Else
'                Call ChangeFontSize(CDec(objcboFontSize.Text))
'            End If
'        Catch ex As Exception
'            DisplayError.Show(CStr(-1), ex.Message, "cboFontSize_TextChanged", mstrModuleName)
'        End Try
'    End Sub

'    Private Sub cboFont_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles objcboFont.SelectedIndexChanged
'        Try
'            Call ChangeFont(objcboFont.Text)
'        Catch ex As Exception
'            DisplayError.Show(CStr(-1), ex.Message, "cboFont_SelectedIndexChanged", mstrModuleName)
'        End Try
'    End Sub

'#End Region

'#Region " Alignments "
'    Private Sub tlbbtnLeftAlign_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objtlbbtnLeftAlign.Click
'        Try
'            Dim strFormat As New StringFormat(StringFormatFlags.NoClip)
'            objtlbbtnCenterAlign.Checked = False
'            objtlbbtnRightAlign.Checked = False
'            objtlbbtnLeftAlign.Checked = True
'            If rtbDocument.SelectionAlignment <> HorizontalAlignment.Left Then
'                rtbDocument.SelectionAlignment = HorizontalAlignment.Left
'                Call RegainOldStyle()
'            End If
'        Catch ex As Exception
'            DisplayError.Show(CStr(-1), ex.Message, "tlbbtnLeftAlign_Click", mstrModuleName)
'        End Try
'    End Sub

'    Private Sub tlbbtnCenterAlign_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objtlbbtnCenterAlign.Click
'        Try
'            objtlbbtnCenterAlign.Checked = True
'            objtlbbtnRightAlign.Checked = False
'            objtlbbtnLeftAlign.Checked = False
'            If rtbDocument.SelectionAlignment <> HorizontalAlignment.Center Then
'                rtbDocument.SelectionAlignment = HorizontalAlignment.Center
'                Call RegainOldStyle()
'            End If
'        Catch ex As Exception
'            DisplayError.Show(CStr(-1), ex.Message, "tlbbtnCenterAlign_Click", mstrModuleName)
'        End Try
'    End Sub

'    Private Sub tlbbtnRightAlign_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objtlbbtnRightAlign.Click
'        Try
'            objtlbbtnCenterAlign.Checked = False
'            objtlbbtnRightAlign.Checked = True
'            objtlbbtnLeftAlign.Checked = False
'            If rtbDocument.SelectionAlignment <> HorizontalAlignment.Right Then
'                rtbDocument.SelectionAlignment = HorizontalAlignment.Right
'                Call RegainOldStyle()
'            End If
'        Catch ex As Exception
'            DisplayError.Show(CStr(-1), ex.Message, "tlbbtnRightAlign_Click", mstrModuleName)
'        End Try
'    End Sub
'#End Region

'#Region " Styles "

'    Private Sub objtlbbtnBold_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objtlbbtnBold.Click
'        Try
'            If objtlbbtnBold.Checked = True Then
'                Call ApplyStyle(FontStyle.Bold, True)
'            Else
'                Call ApplyStyle(FontStyle.Bold, False)
'            End If
'        Catch ex As Exception
'            DisplayError.Show(CStr(-1), ex.Message, "objtlbbtnBold_Click", mstrModuleName)
'        End Try
'    End Sub

'    Private Sub objtlbbtnItalic_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objtlbbtnItalic.Click
'        Try
'            If objtlbbtnItalic.Checked = True Then
'                Call ApplyStyle(FontStyle.Italic, True)
'            Else
'                Call ApplyStyle(FontStyle.Italic, False)
'            End If
'        Catch ex As Exception
'            DisplayError.Show(CStr(-1), ex.Message, "objtlbbtnItalic_Click", mstrModuleName)
'        End Try
'    End Sub

'    Private Sub objtlbbtnUnderline_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objtlbbtnUnderline.Click
'        Try
'            If objtlbbtnUnderline.Checked = True Then
'                Call ApplyStyle(FontStyle.Underline, True)
'            Else
'                Call ApplyStyle(FontStyle.Underline, False)
'            End If
'        Catch ex As Exception
'            DisplayError.Show(CStr(-1), ex.Message, "objtlbbtnUnderline_Click", mstrModuleName)
'        End Try
'    End Sub

'#End Region

'#Region " Operations "

'    Private Sub objtlbbtnUndo_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objtlbbtnUndo.Click
'        Try
'            rtbDocument.Undo()
'        Catch ex As Exception
'            DisplayError.Show(CStr(-1), ex.Message, "objtlbbtnUndo_Click", mstrModuleName)
'        End Try
'    End Sub

'    Private Sub objtlbbtnRedo_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objtlbbtnRedo.Click
'        Try
'            rtbDocument.Redo()
'        Catch ex As Exception
'            DisplayError.Show(CStr(-1), ex.Message, "objtlbbtnRedo_Click", mstrModuleName)
'        End Try
'    End Sub

'#End Region

'#Region " Buttons "
'    Private Sub objtlbbtnOpenTemplate_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objtlbbtnOpenTemplate.Click
'        Dim frm As New frmLetterTemplate
'        Try
'            'If User._Object._RightToLeft = True Then
'            '    frm.RightToLeft = System.Windows.Forms.RightToLeft.Yes
'            '    frm.RightToLeftLayout = True
'            '    Call Language.ctlRightToLeftlayOut(frm)
'            'End If

'            'S.SANDEEP [ 02 MAY 2012 ] -- START
'            'ENHANCEMENT : TRA CHANGES
'            'frm.displayDialog(True)
'            If frm.displayDialog(True) = True Then
'                strData = frm._Letter_Content
'                mintLetterUnkid = frm._Letter_Unkid
'                rtbDocument.Rtf = strData
'            End If
'            'S.SANDEEP [ 02 MAY 2012 ] -- END
'        Catch ex As Exception
'            DisplayError.Show(CStr(-1), ex.Message, "objtlbbtnOpenTemplate_Click", mstrModuleName)
'        End Try
'    End Sub

'    Private Sub objtlbbtnBulletSimple_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objtlbbtnBulletSimple.Click
'        Try
'            If objtlbbtnBulletSimple.Checked = True Then
'                rtbDocument.SelectionBullet = True
'            Else
'                rtbDocument.SelectionBullet = False
'            End If
'            Call RegainOldStyle()
'        Catch ex As Exception
'            DisplayError.Show(CStr(-1), ex.Message, "objtlbbtnBulletSimple_Click", mstrModuleName)
'        End Try
'    End Sub

'    Private Sub objtlbbtnLeftIndent_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objtlbbtnLeftIndent.Click
'        Try
'            If rtbDocument.SelectionIndent = 0 Then
'                rtbDocument.SelectionIndent = 5
'            Else
'                rtbDocument.SelectionIndent += 10
'            End If
'            Call RegainOldStyle()
'        Catch ex As Exception
'            DisplayError.Show(CStr(-1), ex.Message, "objtlbbtnLeftIndent_Click", mstrModuleName)
'        End Try
'    End Sub

'    Private Sub objtlbbtnRightIndent_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objtlbbtnRightIndent.Click
'        Try
'            If rtbDocument.SelectionRightIndent = 0 Then
'                rtbDocument.SelectionRightIndent = 5
'            Else
'                rtbDocument.SelectionRightIndent += 10
'            End If
'            Call RegainOldStyle()
'        Catch ex As Exception
'            DisplayError.Show(CStr(-1), ex.Message, "objtlbbtnRightIndent_Click", mstrModuleName)
'        End Try
'    End Sub

'    Private Sub objtlbbtnCut_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objtlbbtnCut.Click
'        Try
'            If rtbDocument.SelectedText.Length > 0 Then
'                My.Computer.Clipboard.SetText(rtbDocument.SelectedText)
'                rtbDocument.SelectedText = ""
'            End If
'        Catch ex As Exception
'            DisplayError.Show(CStr(-1), ex.Message, "objtlbbtnCut_Click", mstrModuleName)
'        End Try
'    End Sub

'    Private Sub objtlbbtnCopy_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objtlbbtnCopy.Click
'        Try
'            If rtbDocument.SelectedText.Length > 0 Then
'                My.Computer.Clipboard.SetText(rtbDocument.SelectedText)
'            End If

'        Catch ex As Exception
'            DisplayError.Show(CStr(-1), ex.Message, "objtlbbtnCopy_Click", mstrModuleName)
'        End Try
'    End Sub

'    Private Sub objtlbbtnPaste_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objtlbbtnPaste.Click
'        Try
'            If My.Computer.Clipboard.ContainsText Then
'                rtbDocument.AppendText(My.Computer.Clipboard.GetText())
'            End If
'        Catch ex As Exception
'            DisplayError.Show(CStr(-1), ex.Message, "objtlbbtnPaste_Click", mstrModuleName)
'        End Try
'    End Sub

'    Private Sub objtlbbtnColor_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objtlbbtnColor.Click
'        If pnlColor.Visible = True Then

'            pnlColor.Hide()
'            pnlColor.SendToBack()
'        End If
'        pnlColor.BringToFront()

'        pnlColor.Show()
'    End Sub

'    Private Sub SetColor(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles objbtn1.MouseClick, _
'                                objbtn2.MouseClick, objbtn3.MouseClick, objbtn4.MouseClick, objbtn5.MouseClick, objbtn6.MouseClick, objbtn7.MouseClick, _
'                                objbtn8.MouseClick, objbtn9.MouseClick, objbtn10.MouseClick, objbtn11.MouseClick, objbtn12.MouseClick, objbtn13.MouseClick, objbtn14.MouseClick, _
'                                 objbtn15.MouseClick, objbtn16.MouseClick, objbtn17.MouseClick, objbtn18.MouseClick, objbtn19.MouseClick, _
'                                objbtn20.MouseClick, objbtn21.MouseClick, objbtn22.MouseClick, objbtn23.MouseClick, objbtn24.MouseClick, objbtn25.MouseClick, _
'                                objbtn26.MouseClick, objbtn27.MouseClick, objbtn28.MouseClick, objbtn29.MouseClick, objbtn30.MouseClick, objbtn31.MouseClick, _
'                                objbtn32.MouseClick, objbtn33.MouseClick, objbtn34.MouseClick, objbtn35.MouseClick, objbtn36.MouseClick, objbtn37.MouseClick, _
'                                objbtn38.MouseClick, objbtn39.MouseClick, objbtn40.MouseClick, objbtn41.MouseClick, objbtn42.MouseClick, objbtn43.MouseClick, _
'                                 objbtn44.MouseClick, objbtn45.MouseClick, objbtn46.MouseClick, objbtn47.MouseClick, objbtn48.MouseClick
'        rtbDocument.SelectionColor = CType(sender, Button).BackColor
'        Call RegainOldStyle()
'        pnlColor.Hide()
'    End Sub

'    Private Sub btnMoreColor_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnMoreColor.Click
'        ColorDialog1.ShowDialog()
'        rtbDocument.SelectionColor = ColorDialog1.Color
'        pnlColor.Hide()
'        Call RegainOldStyle()
'    End Sub
'#End Region

'#End Region

'#Region " Context Menu Events "

'    Private Sub mnuCut_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuCut.Click
'        Try
'            If rtbDocument.SelectedText.Length > 0 Then
'                mnuCut.Enabled = True
'                My.Computer.Clipboard.SetText(rtbDocument.SelectedText)
'                rtbDocument.SelectedText = ""
'            Else
'                mnuCut.Enabled = False
'            End If
'        Catch ex As Exception
'            DisplayError.Show(CStr(-1), ex.Message, "mnuCut_Click", mstrModuleName)
'        End Try
'    End Sub

'    Private Sub mnuCopy_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuCopy.Click
'        Try
'            If rtbDocument.SelectedText.Length > 0 Then
'                mnuCopy.Enabled = True
'                My.Computer.Clipboard.SetText(rtbDocument.SelectedText)
'            Else
'                mnuCopy.Enabled = False
'            End If
'        Catch ex As Exception
'            DisplayError.Show(CStr(-1), ex.Message, "mnuCopy_Click", mstrModuleName)
'        End Try
'    End Sub

'    Private Sub mnuPaste_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuPaste.Click
'        Try
'            Dim strString As String = ""
'            Dim mFontSize As Integer = CInt(objcboFontSize.Text)
'            mGlobalFontSize = CInt(objcboFontSize.Text)
'            Dim mFontFamily As New FontFamily(objcboFont.Text)

'            If My.Computer.Clipboard.ContainsText Then
'                mnuPaste.Enabled = True
'                strString = My.Computer.Clipboard.GetText()

'                rtbDocument.AppendText(strString)
'                rtbDocument.SelectionFont = New Font(mFontFamily, mFontSize, FontStyle.Regular)
'            Else
'                mnuPaste.Enabled = False
'            End If
'        Catch ex As Exception
'            DisplayError.Show(CStr(-1), ex.Message, "mnuPaste_Click", mstrModuleName)
'        End Try
'    End Sub

'#End Region

'#Region " Other Control's Events "

'    Private Sub rtbDocument_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles rtbDocument.TextChanged
'        Try
'            mnuCut.Enabled = True
'            mnuCopy.Enabled = True
'            mnuPaste.Enabled = True
'            Call ChangeFont(objcboFont.Text)
'            Call ChangeFontSize(CDec(objcboFontSize.Text))
'        Catch ex As Exception
'            DisplayError.Show(CStr(-1), ex.Message, "rtbDocument_TextChanged", mstrModuleName)
'        End Try
'    End Sub

'    Private Sub txtToAddress_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtToAddress.TextChanged
'        Try
'            If txtToAddress.Text.Length > 0 Then
'                btnViewMerge.Enabled = True
'                btnFinish.Enabled = True
'                btnSend.Enabled = True
'            Else
'                btnViewMerge.Enabled = False
'                btnFinish.Enabled = False
'                btnSend.Enabled = False
'            End If
'        Catch ex As Exception
'            DisplayError.Show(CStr(-1), ex.Message, "txtToAddress_TextChanged", mstrModuleName)
'        End Try
'    End Sub

'#End Region

'#Region " Button's Events "

'    Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click
'        Try
'            Me.Close()
'        Catch ex As Exception
'            DisplayError.Show(CStr(-1), ex.Message, "btnCancel_Click", mstrModuleName)
'        End Try
'    End Sub

'    Private Sub btnViewMerge_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnViewMerge.Click
'        Try
'            rtbDocument.ReadOnly = True
'            rtbDocument.BackColor = GUI.ColorComp
'            btnViewMerge.Hide()
'            btnFinish.Show()
'            objbtnNext.Visible = True
'            objbtnPrevious.Visible = True
'            'S.SANDEEP [ 02 APRIL 2012 ] -- START
'            'ENHANCEMENT : TRA CHANGES
'            btnSend.Enabled = False
'            'S.SANDEEP [ 28 APRIL 2012 ] -- END

'            If rtbDocument.Rtf <> strData Then
'                strData = rtbDocument.Rtf
'            End If

'        Catch ex As Exception
'            DisplayError.Show(CStr(-1), ex.Message, "btnViewMerge_Click", mstrModuleName)
'        End Try
'    End Sub

'    Private Sub btnFinish_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnFinish.Click
'        Try
'            rtbDocument.ReadOnly = False
'            rtbDocument.BackColor = GUI.ColorOptional
'            btnViewMerge.Show()
'            btnFinish.Hide()
'            objbtnNext.Visible = False
'            objbtnPrevious.Visible = False
'            rtbDocument.Rtf = strData
'            'S.SANDEEP [ 02 APRIL 2012 ] -- START
'            'ENHANCEMENT : TRA CHANGES
'            btnSend.Enabled = True
'            'S.SANDEEP [ 28 APRIL 2012 ] -- END

'            'S.SANDEEP [ 09 NOV 2012 ] -- START
'            'ENHANCEMENT : TRA CHANGES
'            intCount = 0
'            'S.SANDEEP [ 09 NOV 2012 ] -- END

'        Catch ex As Exception
'            DisplayError.Show(CStr(-1), ex.Message, "btnViewMerge_Click", mstrModuleName)
'        End Try
'    End Sub


'    'S.SANDEEP [ 18 FEB 2012 ] -- START
'    'ENHANCEMENT : TRA CHANGES
'    'Private Sub btnTo_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuEmployee.Click
'    '    Try

'    '        'Sandeep [ 07 APRIL 2011 ] -- Start
'    '        If tabcSendMail.TabPages.Contains(tabpAttachment) = False Then
'    '            tabcSendMail.TabPages.Add(tabpAttachment)
'    '        End If
'    '        'Sandeep [ 07 APRIL 2011 ] -- End 
'    '        Call CallFrom(enImg_Email_RefId.Employee_Module)
'    '    Catch ex As Exception
'    '        DisplayError.Show("-1", ex.Message, "btnTo_Click", mstrModuleName)
'    '    End Try
'    'End Sub
'    Private Sub mnuEmployee_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuEmployee.Click
'        Try

'            'Sandeep [ 07 APRIL 2011 ] -- Start
'            If tabcSendMail.TabPages.Contains(tabpAttachment) = False Then
'                tabcSendMail.TabPages.Add(tabpAttachment)
'            End If
'            'Sandeep [ 07 APRIL 2011 ] -- End 
'            Call CallFrom(enImg_Email_RefId.Employee_Module)
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "mnuEmployee_Click", mstrModuleName)
'        End Try
'    End Sub
'    'S.SANDEEP [ 18 FEB 2012 ] -- END


'    Private Sub objbtnNext_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnNext.Click
'        Try
'            Call SetAddress()
'            If dsList.Tables(0).Rows.Count = 1 Then
'                If rtbDocument.Rtf <> strData Then
'                    rtbDocument.Rtf = strData
'                End If

'                'S.SANDEEP [ 26 SEPT 2013 ] -- START
'                'ENHANCEMENT : TRA CHANGES
'                'dtRow = dsList.Tables(0).Select("email = " & "'" & strMailAddess(intCount) & "'")
'                dtRow = dsList.Tables(0).Select("email = " & "'" & strMailAddess(intCount).Replace("'", "''") & "'")
'                'S.SANDEEP [ 26 SEPT 2013 ] -- END


'                'Pinkal (18-Sep-2012) -- Start
'                'Enhancement : TRA Changes
'                If mstrLeaveFormIds.Trim.Length > 0 Then
'                    For i As Integer = 0 To dtRow.Length - 1
'                        Call ViewMergeData(i)
'                    Next
'                Else
'                    Call ViewMergeData(intCount)
'                End If

'                'Pinkal (18-Sep-2012) -- End


'            Else
'                If dsList Is Nothing Then
'                    Exit Sub
'                ElseIf intCount = dsList.Tables(0).Rows.Count Then
'                    Exit Sub
'                ElseIf dsList.Tables(0).Rows.Count - intCount = 1 Then
'                    Exit Sub
'                Else
'                    If rtbDocument.Rtf <> strData Then
'                        intCount += 1
'                    End If
'                    rtbDocument.Rtf = strData

'                    'S.SANDEEP [ 26 SEPT 2013 ] -- START
'                    'ENHANCEMENT : TRA CHANGES
'                    'dtRow = dsList.Tables(0).Select("email = " & "'" & strMailAddess(intCount) & "'")
'                    dtRow = dsList.Tables(0).Select("email = " & "'" & strMailAddess(intCount).Replace("'", "''") & "'")
'                    'S.SANDEEP [ 26 SEPT 2013 ] -- END

'                    ''S.SANDEEP [ 04 MAY 2012 ] -- START
'                    ''ENHANCEMENT : TRA CHANGES
'                    'If dtRow.Length > 0 Then
'                    '    Call ViewMergeData(intCount)
'                    'End If

'                    'Pinkal (18-Sep-2012) -- Start
'                    'Enhancement : TRA Changes

'                    If dtRow.Length > 0 Then
'                        If mstrLeaveFormIds.Trim.Length > 0 Then
'                            For i As Integer = 0 To dtRow.Length - 1
'                                Call ViewMergeData(i)
'                            Next
'                        Else
'                            Call ViewMergeData(intCount)
'                        End If
'                    End If

'                    'Pinkal (18-Sep-2012) -- End
'                End If
'                'S.SANDEEP [ 04 MAY 2012 ] -- END
'            End If
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "objbtnNext_Click", mstrModuleName)
'        End Try
'    End Sub

'    Private Sub objbtnPrevious_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnPrevious.Click
'        Try
'            If strMailAddess IsNot Nothing Then
'                If CInt(strMailAddess.Length) <= 0 Then
'                    Call SetAddress()
'                End If
'            End If
'            If intCount <= 0 Then
'    Exit Sub
'            ElseIf intCount <> dsList.Tables(0).Rows.Count Then
'                intCount -= 1
'                rtbDocument.Rtf = strData

'                'S.SANDEEP [ 26 SEPT 2013 ] -- START
'                'ENHANCEMENT : TRA CHANGES
'                'dtRow = dsList.Tables(0).Select("email = " & "'" & strMailAddess(intCount) & "'")
'                dtRow = dsList.Tables(0).Select("email = " & "'" & strMailAddess(intCount).Replace("'", "''") & "'")
'                'S.SANDEEP [ 26 SEPT 2013 ] -- END
'                '    'S.SANDEEP [ 04 MAY 2012 ] -- START
'                '    'ENHANCEMENT : TRA CHANGES
'                '    If dtRow.Length > 0 Then
'                '        Call ViewMergeData(intCount)
'                'End If
'                '    'S.SANDEEP [ 04 MAY 2012 ] -- END

'                If dtRow.Length > 0 Then
'                    If mstrLeaveFormIds.Trim.Length > 0 Then
'                        For i As Integer = 0 To dtRow.Length - 1
'                            Call ViewMergeData(i)
'                        Next
'                    Else
'                        Call ViewMergeData(intCount)
'                    End If
'End If

'            End If
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "", mstrModuleName)
'        End Try
'    End Sub

'    Private Sub btnAttach_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAttach.Click
'        Try
'            'S.SANDEEP [ 18 FEB 2012 ] -- START
'            'ENHANCEMENT : TRA CHANGES
'            ''Sandeep [ 12 MARCH 2011 ] -- Start
'            ''ofdMailAttachment.InitialDirectory = My.Computer.FileSystem.SpecialDirectories.MyDocuments
'            'If ConfigParameter._Object._ExportReportPath.Trim.Length <= 0 Then
'            '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 10, "Invalid Export Report Path. Please set valid export Path to Attach file(s)."), enMsgBoxStyle.Information)
'            '    Exit Sub
'            'End If

'            'If System.IO.Directory.Exists(ConfigParameter._Object._ExportReportPath) Then
'            '    ofdMailAttachment.InitialDirectory = ConfigParameter._Object._ExportReportPath
'            'Else
'            '    Dim objAppSettings As New clsApplicationSettings
'            '    ofdMailAttachment.InitialDirectory = objAppSettings._ApplicationPath & "Data\Images"
'            '    objAppSettings = Nothing
'            '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 10, "Invalid Export Report Path. Please set valid export Path to Attach file(s)."), enMsgBoxStyle.Information)
'            '    Exit Sub
'            'End If
'            ''Sandeep [ 12 MARCH 2011 ] -- End 
'            'S.SANDEEP [ 18 FEB 2012 ] -- END


'            ofdMailAttachment.Filter = "Word Files (*.doc)|*.rtf|Image Files (*.bmp)|*.bmp|JPG Files (*.jpg)|*.jpg|All Files (*.*)|*.*"
'            If (ofdMailAttachment.ShowDialog(Me) = System.Windows.Forms.DialogResult.OK) Then

'                Dim lvItem As New ListViewItem
'                lvItem.Text = ofdMailAttachment.FileName
'                lvItem.SubItems.Add("False")
'                lvAttachedFile.Items.Add(lvItem)

'                'S.SANDEEP [ 18 FEB 2012 ] -- START
'                'ENHANCEMENT : TRA CHANGES
'                'lvAttachedFile.Items.Add(ofdMailAttachment.FileName)
'                'Dim objFname As System.IO.FileInfo = New System.IO.FileInfo(ofdMailAttachment.FileName)
'                'If IO.File.Exists(ConfigParameter._Object._ExportReportPath & "\" & objFname.Name) Then
'                '    'Sandeep [ 23 Oct 2010 ] -- Start
'                '    'Issue : when same path is selected for export and attach at that time file is not found due to file is removed before attachment.
'                '    'IO.File.Delete(ConfigParameter._Object._ExportReportPath & "\" & objFname.Name)
'                '    'IO.File.Copy(ofdMailAttachment.FileName, ConfigParameter._Object._ExportReportPath & "\" & objFname.Name)
'                '    'Sandeep [ 23 Oct 2010 ] -- End
'                'Else
'                'IO.File.Copy(ofdMailAttachment.FileName, ConfigParameter._Object._ExportReportPath & "\" & objFname.Name)
'                'End If
'                'S.SANDEEP [ 18 FEB 2012 ] -- END

'            End If
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "btnAttach_Click", mstrModuleName)
'        End Try
'    End Sub

'    Private Sub btnRemove_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnRemove.Click
'        Try
'            If lvAttachedFile.SelectedItems.Count > 0 Then
'                lvAttachedFile.Items.Remove(lvAttachedFile.SelectedItems(0))
'            End If
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "btnRemove_Click", mstrModuleName)
'        End Try
'    End Sub

'    Private Sub btnSend_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSend.Click
'        Try
'            'S.SANDEEP [ 27 FEB 2012 ] -- START
'            'ENHANCEMENT : TRA CHANGES
'            If txtSubject.Text.Trim.Length <= 0 Then
'                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 8, "Subject is mandatory information. Please write Subject to continue."), enMsgBoxStyle.Information)
'                Exit Sub
'            ElseIf txtToAddress.Text.Trim.Length <= 0 Then
'                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 9, "Email Address is mandatory information. Please Set Email Address to continue."), enMsgBoxStyle.Information)
'                Exit Sub
'            End If
'            'S.SANDEEP [ 27 FEB 2012 ] -- END
'            Call SetControlVisibility()
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "btnSend_Click", mstrModuleName)
'        End Try
'    End Sub

'#End Region

'    Private Sub mnuApplicant_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles mnuApplicant.Click
'        Try
'            'Sandeep [ 07 APRIL 2011 ] -- Start
'            If tabcSendMail.TabPages.Contains(tabpAttachment) = False Then
'                tabcSendMail.TabPages.Add(tabpAttachment)
'            End If
'            'Sandeep [ 07 APRIL 2011 ] -- End 
'            Call CallFrom(enImg_Email_RefId.Applicant_Module)
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "mnuApplicant_Click", mstrModuleName)
'        End Try

'    End Sub

'    Private Sub SetControlVisibility()
'        'Sandeep [ 07 APRIL 2011 ] -- Start
'        If mblnIsFromPaySlip = True Then
'            Call Export_EPayslip()
'        End If
'        'Sandeep [ 07 APRIL 2011 ] -- End 


'        'Pinkal (20-Jan-2012) -- Start
'        'Enhancement : TRA Changes
'        If mblnIsFromSickSheet Then
'            Call Export_ESickSheet()
'        End If
'        'Pinkal (20-Jan-2012) -- End
'        pbProgress.Visible = True
'        btnSend.Enabled = False : btnViewMerge.Enabled = False
'        btnCancel.Enabled = False
'        mblnIsSendingMail = True
'        bgwSendMail.RunWorkerAsync()
'    End Sub

'    'NOTE : if you change anything in the procedure then please see the feasiliblity that this change is applicable in LEAVE SEND procedure which is below this procedure.  
'Private Sub Send()
'        'S.SANDEEP [ 02 APRIL 2012 ] -- START
'        'ENHANCEMENT : TRA CHANGES
'        rtbDocument.Enabled = False
'        'S.SANDEEP [ 28 APRIL 2012 ] -- END
'    Dim mstrReceipentUnkid As String = ""
'    Dim strMessage As String = ""
'    Dim filename As String = ""
'    Dim mblnResult As Boolean = False
'    Dim blnSend As Boolean = False
'    Dim strFailGusetName As String = ""
'    Dim objEmailData As New clsEmail_Master
'    Try
'        mblnIsSendingPayslip = True
'        Control.CheckForIllegalCrossThreadCalls = False
'        Call SetAddress()
'        If lvAttachedFile.Items.Count > 0 Then
'            filename = ""
'            For Each Item As ListViewItem In lvAttachedFile.Items
'                If IO.File.Exists(Item.Text) Then
'                    filename &= "," & Item.Text.ToString
'                End If
'            Next
'            If filename.Trim.Length > 0 Then filename = Mid(filename, 2)
'        End If
'        Dim objSendMail As New clsSendMail
'        strTextData = rtbDocument.Rtf


'            'S.SANDEEP [ 07 MAY 2012 ] -- START
'            'ENHANCEMENT : TRA CHANGES
'            blnIsMailSending = True
'            objlblCaption.Text = Language.getMessage(mstrModuleName, 10, "Sending Mail(s).")
'            objlblCaption.Visible = True
'            'S.SANDEEP [ 07 MAY 2012 ] -- END




'        For i As Integer = 0 To strMailAddess.Length - 1
'            If Trim(strMailAddess(i)) = "" Then Continue For

'                'Anjan [14 Mar 2014] -- Start
'                'ENHANCEMENT : Email Character "’"
'                'If Not Regex.IsMatch(Trim(strMailAddess(i)), _
'                '                           "^[a-zA-Z][\w\.-]*[a-zA-Z0-9]@[a-zA-Z0-9][\w\.-]*[a-zA-Z0-9]\.[a-zA-Z][a-zA-Z\.]*[a-zA-Z]$") Then
'                '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "The following email Address isinvalid :") & """" & strMailAddess(i) & """" & Language.getMessage(mstrModuleName, 5, "Please enter valid email."), enMsgBoxStyle.Information)
'                '    Exit Sub
'                'End If

'                If Not Regex.IsMatch(Trim(strMailAddess(i)), iEmailRegxExpression) Then
'                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "The following email Address isinvalid :") & """" & strMailAddess(i) & """" & Language.getMessage(mstrModuleName, 5, "Please enter valid email."), enMsgBoxStyle.Information)
'                Exit Sub
'            End If
'                'Anjan [14 Mar 2014 ] -- End

'            If dsList.Tables.Count > 0 Then
'                    'S.SANDEEP [ 26 SEPT 2013 ] -- START
'                    'ENHANCEMENT : TRA CHANGES
'                    'dtRow = dsList.Tables(0).Select("email = " & "'" & strMailAddess(i) & "'")
'                    dtRow = dsList.Tables(0).Select("email = " & "'" & strMailAddess(i).Replace("'", "''") & "'")
'                    'S.SANDEEP [ 26 SEPT 2013 ] -- END
'                If dtRow.Length > 0 Then
'                        If mdicDiscpline.Keys.Count <= 0 Then
'                            If Not dicMailStatus.ContainsKey(strMailAddess(i)) Then
'                                dicMailStatus.Add(strMailAddess(i), 1)
'                            Else
'                                Continue For
'                            End If
'                        End If
'                    End If
'                    Call ViewMergeData(i)
'                    objSendMail._ToEmail = strMailAddess(i)
'                    objSendMail._Subject = txtSubject.Text
'                    Dim strConvertedData As String = objEmailData.rtf2html3(rtbDocument.Rtf, "+H+G+T+CR+I")
'                    objSendMail._Message = strConvertedData

'                    If mblnIsFromPaySlip = False And mblnIsFromSickSheet = False Then
'                        If filename = "" Then
'                            objSendMail._AttachedFiles = ""
'                        Else
'                            objSendMail._AttachedFiles = filename
'                        End If
'                    Else
'                        If mdicAttachmentInfo.Keys.Count > 0 Then
'                            If mdicAttachmentInfo.ContainsKey(CInt(dtRow(0)("EmpId"))) Then
'                                objSendMail._AttachedFiles = mdicAttachmentInfo(CInt(dtRow(0)("EmpId")))
'                            Else
'                                objSendMail._AttachedFiles = ""
'                            End If
'                        End If
'                    End If

'                    If mblnIsFromPaySlip = False And mblnIsFromSickSheet = False Then

'                        strMessage = objSendMail.SendMail()
'                    Else
'                        strMessage = objSendMail.SendMail(True, mstrExportEPaySlipPath)
'                    End If

'                    If strMessage <> "" Then
'                        strFailGusetName &= ", " & dsList.Tables(0).Rows(i)("Email").ToString
'                    Else
'                        blnSend = True
'                    End If
'                    If mstrReceipentUnkid = "" Then
'                        blnSend = True
'                    Else
'                        mstrReceipentUnkid &= " , " & dsList.Tables(0).Rows(i)("FirstName").ToString & " " & dsList.Tables(0).Rows(i)("SurName").ToString & "<" & dsList.Tables(0).Rows(i)("Email").ToString & ">" & vbCrLf
'                    End If

'                    If mstrReceipentId = "" Then
'                        mstrReceipentId = CStr(dtRow(0).Item("EmpId"))
'                    Else
'                        mstrReceipentId &= " , " & CStr(dtRow(0).Item("EmpId"))
'                    End If
'                    rtbDocument.Rtf = strTextData
'                        End If
'                bgwSendMail.ReportProgress(i)
'            Next
'            'S.SANDEEP [ 20 APRIL 2012 ] -- START
'            'ENHANCEMENT : TRA CHANGES
'            objEmailData._dicDisciplineFiles = mdicDiscpline
'            'S.SANDEEP [ 20 APRIL 2012 ] -- END
'            objEmailData._EmailDateTime = ConfigParameter._Object._CurrentDateAndTime
'            objEmailData._Istype_of = CChar("O")
'            objEmailData._LettertypeUnkId = mintLetterUnkid
'            If mintLetterUnkid > 0 Then
'                Dim objLetterType As New clsLetterType
'                objLetterType._LettertypeUnkId = mintLetterUnkid
'                objEmailData._Message = objLetterType._Lettercontent
'                objLetterType = Nothing
'            Else
'                objEmailData._Message = rtbDocument.Rtf
'            End If
'            objEmailData._Subject = txtSubject.Text
'            objEmailData._UserUnkId = User._Object._Userunkid
'            Select Case mintCallingRefId
'                Case enImg_Email_RefId.Applicant_Module
'                    objEmailData._Isapplicant = True
'                Case Else
'                    objEmailData._Isapplicant = False
'            End Select
'            If mstrReceipentId.Length > 0 Then
'                mblnResult = objEmailData.Insert(mstrReceipentId)
'                    End If
'            If blnSend And strFailGusetName.Length = 0 Then
'                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Mail(s) sent successfully!"), enMsgBoxStyle.Information)
'            ElseIf blnSend And strFailGusetName.Length > 2 Then
'                strFailGusetName = strFailGusetName.Substring(2)
'                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Some mails sent fail!" & vbCrLf) & """" & strFailGusetName & """", enMsgBoxStyle.Information)
'                Else
'                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Mail(s) sent fail!"), enMsgBoxStyle.Information)
'                        End If
'        Catch ex As Exception
'            If ex.Message.Contains("Fail") Then
'                eZeeMsgBox.Show(ex.Message & " " & Language.getMessage(mstrModuleName, 6, "Please check your Internet settings."), enMsgBoxStyle.Information)
'            ElseIf ex.Message.Contains("secure") Then
'                eZeeMsgBox.Show(ex.Message & " " & Language.getMessage(mstrModuleName, 7, "Please Uncheck Login using SSL setting."), enMsgBoxStyle.Information)
'                    Else
'                DisplayError.Show("-1", ex.Message, "btnSend_Click", mstrModuleName)
'                    End If
'        Finally
'            'S.SANDEEP [ 07 MAY 2012 ] -- START
'            'ENHANCEMENT : TRA CHANGES
'            blnIsMailSending = False
'            'S.SANDEEP [ 07 MAY 2012 ] -- END
'        End Try
'    End Sub



'    'Pinkal (18-Sep-2012) -- Start
'    'Enhancement : TRA Changes
'    'NOTE : if you change anything in the procedure then please see the feasiliblity that this change is applicable in SEND procedure which is above this procedure.  
'    Private Sub LeaveFormSend()
'        rtbDocument.Enabled = False
'        Dim mstrReceipentUnkid As String = ""
'        Dim strMessage As String = ""
'        Dim filename As String = ""
'        Dim mblnResult As Boolean = False
'        Dim blnSend As Boolean = False
'        Dim strFailGusetName As String = ""
'        Dim objEmailData As New clsEmail_Master
'        Try
'            Control.CheckForIllegalCrossThreadCalls = False
'            Call SetAddress()
'            If lvAttachedFile.Items.Count > 0 Then
'                filename = ""
'                For Each Item As ListViewItem In lvAttachedFile.Items
'                    If IO.File.Exists(Item.Text) Then
'                        filename &= "," & Item.Text.ToString
'                End If
'                Next
'                If filename.Trim.Length > 0 Then filename = Mid(filename, 2)
'            End If
'            Dim objSendMail As New clsSendMail
'            strTextData = rtbDocument.Rtf

'            blnIsMailSending = True
'            objlblCaption.Text = Language.getMessage(mstrModuleName, 10, "Sending Mail(s).")
'            objlblCaption.Visible = True


'            For i As Integer = 0 To strMailAddess.Length - 1
'                If Trim(strMailAddess(i)) = "" Then Continue For

'                'Anjan [14 Mar 2014] -- Start
'                'ENHANCEMENT : Email Character "’"
'                'If Not Regex.IsMatch(Trim(strMailAddess(i)), _
'                '                           "^[a-zA-Z][\w\.-]*[a-zA-Z0-9]@[a-zA-Z0-9][\w\.-]*[a-zA-Z0-9]\.[a-zA-Z][a-zA-Z\.]*[a-zA-Z]$") Then
'                '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "The following email Address isinvalid :") & """" & strMailAddess(i) & """" & Language.getMessage(mstrModuleName, 5, "Please enter valid email."), enMsgBoxStyle.Information)
'                '    Exit Sub
'                'End If

'                If Not Regex.IsMatch(Trim(strMailAddess(i)), iEmailRegxExpression) Then
'                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "The following email Address isinvalid :") & """" & strMailAddess(i) & """" & Language.getMessage(mstrModuleName, 5, "Please enter valid email."), enMsgBoxStyle.Information)
'                Exit Sub
'            End If
'                'Anjan [14 Mar 2014 ] -- End

'                If dsList.Tables.Count > 0 Then
'                    'S.SANDEEP [ 26 SEPT 2013 ] -- START
'                    'ENHANCEMENT : TRA CHANGES
'                    'dtRow = dsList.Tables(0).Select("email = " & "'" & strMailAddess(i) & "'")
'                    dtRow = dsList.Tables(0).Select("email = " & "'" & strMailAddess(i).Replace("'", "''") & "'")
'                    'S.SANDEEP [ 26 SEPT 2013 ] -- END
'                    If dtRow.Length > 0 Then
'                        If mdicDiscpline.Keys.Count <= 0 Then
'                            If Not dicMailStatus.ContainsKey(strMailAddess(i)) Then
'                                dicMailStatus.Add(strMailAddess(i), 1)
'                            Else
'                                Continue For
'                    End If
'                End If
'            End If

'                    Dim drRow() As DataRow = dsList.Tables(0).Select("EmpId = " & "'" & dtRow(0)("EmpId").ToString() & "'")

'                    If drRow.Length <= 0 Then Continue For

'                    For k As Integer = 0 To drRow.Length - 1

'                        Call ViewMergeData(k)
'                        objSendMail._ToEmail = strMailAddess(i)
'                objSendMail._Subject = txtSubject.Text
'                        Dim strConvertedData As String = objEmailData.rtf2html3(rtbDocument.Rtf, "+H+G+T+CR+I")
'                objSendMail._Message = strConvertedData

'                        If mblnIsFromPaySlip = False And mblnIsFromSickSheet = False Then
'                If filename = "" Then
'                    objSendMail._AttachedFiles = ""
'                Else
'                    objSendMail._AttachedFiles = filename
'                            End If
'                        Else
'                            If mdicAttachmentInfo.Keys.Count > 0 Then
'                                If mdicAttachmentInfo.ContainsKey(CInt(dtRow(0)("EmpId"))) Then
'                                    objSendMail._AttachedFiles = mdicAttachmentInfo(CInt(dtRow(0)("EmpId")))
'                                Else
'                                    objSendMail._AttachedFiles = ""
'                                End If
'                            End If
'                End If

'                        If mblnIsFromPaySlip = False And mblnIsFromSickSheet = False Then
'                strMessage = objSendMail.SendMail()
'                        Else
'                            strMessage = objSendMail.SendMail(True, mstrExportEPaySlipPath)
'                        End If

'                If strMessage <> "" Then
'                            strFailGusetName &= ", " & dsList.Tables(0).Rows(i)("Email").ToString
'                Else
'                    blnSend = True
'                End If
'                        If mstrReceipentUnkid = "" Then
'                            blnSend = True
'                        Else
'                            mstrReceipentUnkid &= " , " & dsList.Tables(0).Rows(i)("FirstName").ToString & " " & dsList.Tables(0).Rows(i)("SurName").ToString & "<" & dsList.Tables(0).Rows(i)("Email").ToString & ">" & vbCrLf
'                        End If

'                If mstrReceipentId = "" Then
'                            mstrReceipentId = CStr(dtRow(0).Item("EmpId"))
'                Else
'                            mstrReceipentId &= " , " & CStr(dtRow(0).Item("EmpId"))
'                End If
'                rtbDocument.Rtf = strTextData

'                    Next

'            End If
'                bgwSendMail.ReportProgress(i)
'        Next
'            'S.SANDEEP [ 20 APRIL 2012 ] -- START
'            'ENHANCEMENT : TRA CHANGES
'            objEmailData._dicDisciplineFiles = mdicDiscpline
'            'S.SANDEEP [ 20 APRIL 2012 ] -- END
'        objEmailData._EmailDateTime = ConfigParameter._Object._CurrentDateAndTime
'        objEmailData._Istype_of = CChar("O")
'        objEmailData._LettertypeUnkId = mintLetterUnkid
'        If mintLetterUnkid > 0 Then
'            Dim objLetterType As New clsLetterType
'            objLetterType._LettertypeUnkId = mintLetterUnkid
'            objEmailData._Message = objLetterType._Lettercontent
'            objLetterType = Nothing
'        Else
'            objEmailData._Message = rtbDocument.Rtf
'        End If
'        objEmailData._Subject = txtSubject.Text
'        objEmailData._UserUnkId = User._Object._Userunkid
'        Select Case mintCallingRefId
'            Case enImg_Email_RefId.Applicant_Module
'                objEmailData._Isapplicant = True
'            Case Else
'                objEmailData._Isapplicant = False
'        End Select
'        If mstrReceipentId.Length > 0 Then
'            mblnResult = objEmailData.Insert(mstrReceipentId)
'        End If
'        If blnSend And strFailGusetName.Length = 0 Then
'            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Mail(s) sent successfully!"), enMsgBoxStyle.Information)
'        ElseIf blnSend And strFailGusetName.Length > 2 Then
'            strFailGusetName = strFailGusetName.Substring(2)
'            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Some mails sent fail!" & vbCrLf) & """" & strFailGusetName & """", enMsgBoxStyle.Information)
'        Else
'            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Mail(s) sent fail!"), enMsgBoxStyle.Information)
'        End If
'    Catch ex As Exception
'        If ex.Message.Contains("Fail") Then
'                eZeeMsgBox.Show(ex.Message & " " & Language.getMessage(mstrModuleName, 6, "Please check your Internet settings."), enMsgBoxStyle.Information)
'        ElseIf ex.Message.Contains("secure") Then
'                eZeeMsgBox.Show(ex.Message & " " & Language.getMessage(mstrModuleName, 7, "Please Uncheck Login using SSL setting."), enMsgBoxStyle.Information)
'        Else
'            DisplayError.Show("-1", ex.Message, "btnSend_Click", mstrModuleName)
'        End If
'        Finally
'            'S.SANDEEP [ 07 MAY 2012 ] -- START
'            'ENHANCEMENT : TRA CHANGES
'            blnIsMailSending = False
'            'S.SANDEEP [ 07 MAY 2012 ] -- END
'    End Try
'End Sub
'    'Pinkal (18-Sep-2012) -- End




'    'Private Sub Send()
'    '    Dim mstrReceipentUnkid As String = ""
'    '    Dim strMessage As String = ""
'    '    Dim filename As String = ""
'    '    Dim mblnResult As Boolean = False
'    '    Dim blnSend As Boolean = False
'    '    Dim strFailGusetName As String = ""
'    '    Dim objEmailData As New clsEmail_Master
'    '    Try

'    '        mblnIsSendingPayslip = True
'    '        Control.CheckForIllegalCrossThreadCalls = False

'    '        Call SetAddress()

'    '        If lvAttachedFile.Items.Count > 0 Then
'    '            filename = ""
'    '            'S.SANDEEP [ 18 FEB 2012 ] -- START
'    '            'ENHANCEMENT : TRA CHANGES
'    '            'For Each Item As ListViewItem In lvAttachedFile.Items
'    '            '    Dim objFname As System.IO.FileInfo = New System.IO.FileInfo(Item.Text)
'    '            '    If objFname.Exists Then
'    '            '        If filename = "" Then
'    '            '            filename = objFname.Name
'    '            '        Else
'    '            '            filename &= "," & objFname.Name
'    '            '        End If
'    '            '    End If
'    '            'Next
'    '            For Each Item As ListViewItem In lvAttachedFile.Items
'    '                If IO.File.Exists(Item.Text) Then
'    '                    filename &= "," & Item.Text.ToString
'    '                End If
'    '            Next
'    '            If filename.Trim.Length > 0 Then filename = Mid(filename, 2)
'    '            'S.SANDEEP [ 18 FEB 2012 ] -- END
'    '        End If
'    '        Dim objSendMail As New clsSendMail
'    '        strTextData = rtbDocument.Rtf

'    '        For i As Integer = 0 To strMailAddess.Length - 1
'    '            If Trim(strMailAddess(i)) = "" Then Continue For
'    '            If Not Regex.IsMatch(Trim(strMailAddess(i)), _
'    '                       "^[a-zA-Z][\w\.-]*[a-zA-Z0-9]@[a-zA-Z0-9][\w\.-]*[a-zA-Z0-9]\.[a-zA-Z][a-zA-Z\.]*[a-zA-Z]$") Then
'    '                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "The following email Address isinvalid :") & """" & strMailAddess(i) & """" & Language.getMessage(mstrModuleName, 6, "Please enter valid email."), enMsgBoxStyle.Information)
'    '                Exit Sub
'    '            End If
'    '            If dsList.Tables.Count > 0 Then
'    '                dtRow = dsList.Tables(0).Select("email = " & "'" & strMailAddess(i) & "'")
'    '                If dtRow.Length > 0 Then
'    '                    'If Not dicMailStatus.ContainsKey(strMailAddess(i)) Then
'    '                    '    dicMailStatus.Add(strMailAddess(i), 1)
'    '                    'Else
'    '                    '    Continue For
'    '                    'End If
'    '                    Call ViewMergeData(i)
'    '                    objSendMail._ToEmail = strMailAddess(i)
'    '                    objSendMail._Subject = txtSubject.Text
'    '                    Dim strConvertedData As String = objEmailData.rtf2html3(rtbDocument.Rtf, "+H+G+T+CR+I")
'    '                    objSendMail._Message = strConvertedData


'    '                    'Pinkal (20-Jan-2012) -- Start
'    '                    'Enhancement : TRA Changes


'    '                    'If mblnIsFromPaySlip = False Then
'    '                    'If filename = "" Then
'    '                    '    objSendMail._AttachedFiles = ""
'    '                    'Else
'    '                    '    objSendMail._AttachedFiles = filename
'    '                    'End If
'    '                    'Else
'    '                    '    If mdicAttachmentInfo.Keys.Count > 0 Then
'    '                    '        If mdicAttachmentInfo.ContainsKey(CInt(dtRow(0)("EmpId"))) Then
'    '                    '            objSendMail._AttachedFiles = mdicAttachmentInfo(CInt(dtRow(0)("EmpId")))
'    '                    '        Else
'    '                    '            objSendMail._AttachedFiles = ""
'    '                    '        End If
'    '                    '    End If
'    '                    'End If


'    '                    If mblnIsFromPaySlip = False And mblnIsFromSickSheet = False Then
'    '                        If filename = "" Then
'    '                            objSendMail._AttachedFiles = ""
'    '                        Else
'    '                            objSendMail._AttachedFiles = filename
'    '                        End If
'    '                    Else
'    '                        If mdicAttachmentInfo.Keys.Count > 0 Then
'    '                            If mdicAttachmentInfo.ContainsKey(CInt(dtRow(0)("EmpId"))) Then
'    '                                objSendMail._AttachedFiles = mdicAttachmentInfo(CInt(dtRow(0)("EmpId")))
'    '                            Else
'    '                                objSendMail._AttachedFiles = ""
'    '                            End If
'    '                        End If
'    '                    End If




'    '                    'If mblnIsFromPaySlip = False Then
'    '                    'strMessage = objSendMail.SendMail()
'    '                    'Else
'    '                    '    strMessage = objSendMail.SendMail(True, mstrExportEPaySlipPath)
'    '                    'End If
'    '                    If mblnIsFromPaySlip = False And mblnIsFromSickSheet = False Then
'    '                        strMessage = objSendMail.SendMail()
'    '                    Else
'    '                        strMessage = objSendMail.SendMail(True, mstrExportEPaySlipPath)
'    '                    End If

'    '                    'Pinkal (20-Jan-2012) -- End



'    '                    If strMessage <> "" Then
'    '                        strFailGusetName &= ", " & dsList.Tables(0).Rows(i)("Email").ToString
'    '                    Else
'    '                        blnSend = True
'    '                    End If
'    '                    If mstrReceipentUnkid = "" Then
'    '                        blnSend = True
'    '                    Else
'    '                        mstrReceipentUnkid &= " , " & dsList.Tables(0).Rows(i)("FirstName").ToString & " " & dsList.Tables(0).Rows(i)("SurName").ToString & "<" & dsList.Tables(0).Rows(i)("Email").ToString & ">" & vbCrLf
'    '                    End If

'    '                    If mstrReceipentId = "" Then
'    '                        mstrReceipentId = CStr(dtRow(0).Item("EmpId"))
'    '                    Else
'    '                        mstrReceipentId &= " , " & CStr(dtRow(0).Item("EmpId"))
'    '                    End If


'    '                    'Sandeep [ 25 APRIL 2011 ] -- Start
'    '                    'Issue : MESSAGE BOX STYLE
'    '                    'Sandeep [ 07 APRIL 2011 ] -- Start
'    '                    'rtbDocument.Rtf = strTextData
'    '                    'Set_Rich_Rtf(rtbDocument, strTextData)
'    '                    'Sandeep [ 07 APRIL 2011 ] -- End 
'    '                    rtbDocument.Rtf = strTextData
'    '                    'Sandeep [ 25 APRIL 2011 ] -- End 

'    '                    If strUnsentAddress.Length > 0 And mstrReceipentId = "" Then
'    '                        If strUnsentAddress.Contains(strMailAddess(i)) = False Then
'    '                            strUnsentAddress &= " , " & strMailAddess(i)
'    '                        End If
'    '                    End If
'    '                Else
'    '                    If strUnsentAddress.Length > 0 Then
'    '                        If strUnsentAddress.Contains(strMailAddess(i)) = False Then
'    '                            strUnsentAddress &= " , " & strMailAddess(i)
'    '                        End If
'    '                    Else
'    '                        strUnsentAddress &= " , " & strMailAddess(i)
'    '                    End If
'    '                End If
'    '            End If

'    '            bgwSendMail.ReportProgress(i)

'    '        Next

'    '        If strUnsentAddress.Length > 0 Then
'    '            strMailAddess = strUnsentAddress.Split(CChar(","))
'    '        End If

'    '        For j As Integer = 0 To strMailAddess.Length - 1
'    '            If dicMailStatus.ContainsKey(strMailAddess(j)) = True Then Continue For
'    '            If Trim(strMailAddess(j)) = "" Then Continue For
'    '            If Not Regex.IsMatch(Trim(strMailAddess(j)), _
'    '                                       "^[a-zA-Z][\w\.-]*[a-zA-Z0-9]@[a-zA-Z0-9][\w\.-]*[a-zA-Z0-9]\.[a-zA-Z][a-zA-Z\.]*[a-zA-Z]$") Then
'    '                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "The following email Address isinvalid :") & """" & strMailAddess(j) & """" & Language.getMessage(mstrModuleName, 6, "Please enter valid email."), enMsgBoxStyle.Information)
'    '                Exit Sub
'    '            End If
'    '            If strMailAddess.Length > 1 And mintLetterUnkid > 0 Then
'    '                If mintLetterUnkid <> 0 Then
'    '                    If blnShowed = False Then
'    '                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 7, "You have manually change the information." & vbCrLf & "Reason: Information not present for following guest(s).  ") & """" & strMailAddess(j) & """", enMsgBoxStyle.Information)
'    '                        blnShowed = True
'    '                        Exit Sub
'    '                    End If
'    '                End If
'    '                blnShowed = False
'    '            End If

'    '            If mintLetterUnkid > 0 Then
'    '                If blnIsFirstSent = True Then
'    '                    btnSend.Selected = True
'    '                Else
'    '                    btnSend.Selected = False
'    '                End If
'    '            Else
'    '                btnSend.Selected = True
'    '            End If

'    '            If btnSend.Selected = True Then
'    '                If Not dicMailStatus.ContainsKey(strMailAddess(j)) Then
'    '                    dicMailStatus.Add(strMailAddess(j), 1)
'    '                Else
'    '                    Continue For
'    '                End If
'    '                objSendMail._ToEmail = strMailAddess(j)
'    '                objSendMail._Subject = txtSubject.Text
'    '                Dim strConvertedData As String = objEmailData.rtf2html3(rtbDocument.Rtf, "+H+G+T+CR+I+F-FF")
'    '                objSendMail._Message = strConvertedData
'    '                If filename = "" Then
'    '                    objSendMail._AttachedFiles = ""
'    '                Else
'    '                    objSendMail._AttachedFiles = filename

'    '                End If

'    '                strMessage = objSendMail.SendMail()

'    '                If strMessage <> "" Then
'    '                    strFailGusetName &= ", " & strMailAddess(j)
'    '                Else
'    '                    blnSend = True
'    '                End If
'    '                If mstrReceipentId = "" Then
'    '                    mstrReceipentId = CStr(-1)
'    '                Else
'    '                    mstrReceipentId &= " , " & CStr(-1)
'    '                End If
'    '                rtbDocument.Rtf = strTextData
'    '                strSendAddress &= " , " & strMailAddess(j)
'    '                blnIsFirstSent = False
'    '            Else
'    '                blnIsFirstSent = False
'    '                Exit Sub
'    '            End If
'    '        Next

'    '        'Sandeep [ 04 NOV 2010 ] -- Start
'    '        'objEmailData._EmailDateTime = CDate(CDate(Now.Date) & " " & (Now).ToLongTimeString)
'    '        objEmailData._EmailDateTime = ConfigParameter._Object._CurrentDateAndTime
'    '        'Sandeep [ 04 NOV 2010 ] -- End
'    '        objEmailData._Istype_of = CChar("O")
'    '        objEmailData._LettertypeUnkId = mintLetterUnkid
'    '        'S.SANDEEP [ 07 NOV 2011 ] -- START
'    '        'ENHANCEMENT : TRA CHANGES
'    '        'If mintLetterUnkid = 0 Then
'    '        '    objEmailData._Message = rtbDocument.Rtf
'    '        'Else
'    '        '    objEmailData._Message = strTextData
'    '        'End If

'    '        If mintLetterUnkid > 0 Then
'    '            Dim objLetterType As New clsLetterType
'    '            objLetterType._LettertypeUnkId = mintLetterUnkid
'    '            objEmailData._Message = objLetterType._Lettercontent
'    '            objLetterType = Nothing
'    '        Else
'    '            objEmailData._Message = rtbDocument.Rtf
'    '        End If
'    '        'S.SANDEEP [ 07 NOV 2011 ] -- END


'    '        objEmailData._Subject = txtSubject.Text
'    '        objEmailData._UserUnkId = User._Object._Userunkid
'    '        'S.SANDEEP [ 07 NOV 2011 ] -- START
'    '        'ENHANCEMENT : TRA CHANGES
'    '        Select Case mintCallingRefId
'    '            Case enImg_Email_RefId.Applicant_Module
'    '                objEmailData._Isapplicant = True
'    '            Case Else
'    '                objEmailData._Isapplicant = False
'    '        End Select
'    '        'S.SANDEEP [ 07 NOV 2011 ] -- END




'    '        'Sandeep [ 25 APRIL 2011 ] -- Start
'    '        'mblnResult = objEmailData.Insert(mstrReceipentId)
'    '        If mstrReceipentId.Length > 0 Then
'    '            mblnResult = objEmailData.Insert(mstrReceipentId)
'    '        End If
'    '        'Sandeep [ 25 APRIL 2011 ] -- End 

'    '        If blnSend And strFailGusetName.Length = 0 Then
'    '            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Mail(s) sent successfully!"), enMsgBoxStyle.Information)
'    '        ElseIf blnSend And strFailGusetName.Length > 2 Then
'    '            strFailGusetName = strFailGusetName.Substring(2)
'    '            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Some mails sent fail!" & vbCrLf) & """" & strFailGusetName & """", enMsgBoxStyle.Information)
'    '        Else
'    '            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Mail(s) sent fail!"), enMsgBoxStyle.Information)
'    '        End If
'    '    Catch ex As Exception
'    '        If ex.Message.Contains("Fail") Then
'    '            eZeeMsgBox.Show(ex.Message & " " & Language.getMessage(mstrModuleName, 11, "Please check your Internet settings."), enMsgBoxStyle.Information)
'    '        ElseIf ex.Message.Contains("secure") Then
'    '            eZeeMsgBox.Show(ex.Message & " " & Language.getMessage(mstrModuleName, 12, "Please Uncheck Login using SSL setting."), enMsgBoxStyle.Information)
'    '        Else
'    '            DisplayError.Show("-1", ex.Message, "btnSend_Click", mstrModuleName)
'    '        End If

'    '    End Try
'    'End Sub

'    Private Sub bgwSendMail_DoWork(ByVal sender As System.Object, ByVal e As System.ComponentModel.DoWorkEventArgs) Handles bgwSendMail.DoWork
'        Try
'            RemoveHandler gfrmMDI.tmrReminder.Tick, AddressOf gfrmMDI.tmrReminder_Tick


'            'Pinkal (18-Sep-2012) -- Start
'            'Enhancement : TRA Changes
'            If mstrLeaveFormIds.Trim.Length > 0 Then
'                Call LeaveFormSend()
'            Else
'                Call Send()
'            End If
'            'Pinkal (18-Sep-2012) -- End

'            AddHandler gfrmMDI.tmrReminder.Tick, AddressOf gfrmMDI.tmrReminder_Tick

'        Catch ex As Exception
'            'DisplayError.Show("-1", ex.Message, "bgwSendMail_DoWork", mstrModuleName)
'        Finally
'            mblnIsSendingMail = False
'        End Try
'    End Sub

'    'Sandeep [ 07 APRIL 2011 ] -- Start
'    Private Sub mnuSendPayslip_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuSendPayslip.Click
'        Try
'            If tabcSendMail.TabPages.Contains(tabpAttachment) Then
'                tabcSendMail.TabPages.Remove(tabpAttachment)
'            End If
'            Call CallFrom(enImg_Email_RefId.Payroll_Module)
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "mnuSendPayslip_Click", mstrModuleName)
'        End Try
'    End Sub

'    'S.SANDEEP [ 04 FEB 2012 ] -- START
'    'ENHANCEMENT : TRA CHANGES
'    Private Sub mnuAppraisalLetters_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuAppraisalLetters.Click
'    Try
'        If tabcSendMail.TabPages.Contains(tabpAttachment) = False Then
'            tabcSendMail.TabPages.Add(tabpAttachment)
'        End If
'            Call CallFrom(enImg_Email_RefId.Appraisal_Module)
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "mnuAppraisalLetters_Click", mstrModuleName)
'        Finally
'        End Try
'    End Sub

'    Private Sub bgwSendMail_ProgressChanged(ByVal sender As Object, ByVal e As System.ComponentModel.ProgressChangedEventArgs) Handles bgwSendMail.ProgressChanged
'        Application.DoEvents()
'        Dim canvas As Graphics = Me.pbProgress.CreateGraphics
'        If e.ProgressPercentage = 0 Then
'            pbProgress.Value = CInt(((1 * 100) / dsList.Tables(0).Rows.Count))
'        Else
'            pbProgress.Value = CInt(((e.ProgressPercentage * 100) / dsList.Tables(0).Rows.Count))
'        End If
'        canvas.DrawString(pbProgress.Value.ToString & "%", New Font("Verdana", 9, FontStyle.Bold), New SolidBrush(Color.Black), 180, 5)
'        canvas.Dispose()
'    End Sub

'    'S.SANDEEP [ 04 FEB 2012 ] -- END



'    ''S.SANDEEP [ 23 JAN 2012 ] -- START
'    ''ENHANCEMENT : TRA CHANGES : AUTOMATIC ASSIGNMENT OF BENEFITS        
'    'Private Sub mnuDiscipline_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles mnuDiscipline.Click
'    '    Try
'    '        If tabcSendMail.TabPages.Contains(tabpAttachment) = False Then
'    '            tabcSendMail.TabPages.Add(tabpAttachment)
'    '        End If
'    '        Call CallFrom(enImg_Email_RefId.Discipline_Module)
'    '    Catch ex As Exception
'    '        DisplayError.Show("-1", ex.Message, "mnuDiscipline_Click", mstrModuleName)
'    '    Finally
'    '    End Try
'    'End Sub
'    ''S.SANDEEP [ 23 JAN 2012 ] -- END

'    Private Sub bgwSendMail_RunWorkerCompleted(ByVal sender As Object, ByVal e As System.ComponentModel.RunWorkerCompletedEventArgs) Handles bgwSendMail.RunWorkerCompleted
'        Try
'            mblnIsSendingMail = False
'            mblnIsSendDisposed = True
'            Me.Close()
'            AddHandler gfrmMDI.tmrReminder.Tick, AddressOf gfrmMDI.tmrReminder_Tick
'        Catch ex As IO.IOException
'    Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "bgwSendMail_RunWorkerCompleted", mstrModuleName)
'    Finally
'            mblnIsSendingPayslip = False
'            mblnIsSendingMail = False
'    End Try
'End Sub
'    'Sandeep [ 07 APRIL 2011 ] -- End 


'    'Pinkal (20-Jan-2012) -- Start
'    'Enhancement : TRA Changes

'    Private Sub mnuSendSickSheet_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuSendSickSheet.Click
'        Try
'            If tabcSendMail.TabPages.Contains(tabpAttachment) Then
'                tabcSendMail.TabPages.Remove(tabpAttachment)
'            End If
'            Call CallFrom(enImg_Email_RefId.Medical_Module)
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "mnuSendSickSheet_Click", mstrModuleName)
'        End Try
'    End Sub

'    'Pinkal (20-Jan-2012) -- End



'    'S.SANDEEP [ 18 FEB 2012 ] -- START
'    'ENHANCEMENT : TRA CHANGES
'    Private Sub btnEmployeeDocument_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEmployeeDocument.Click
'        Dim frm As New frmScanAttachmentList
'        Try
'            frm._IsFromMail = True
'            frm.objFooter.Visible = False
'            frm.objeFooter.Visible = True
'            frm.ShowDialog()
'            If frm._Attached_Files.Trim.Length > 0 Then
'                For Each fl As String In frm._Attached_Files.Split(CChar("|"))
'                    Dim lvItem As New ListViewItem
'                    lvItem.Text = fl.ToString
'                    lvItem.SubItems.Add("True")
'                    lvAttachedFile.Items.Add(lvItem)
'                Next
'            End If
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "", mstrModuleName)
'        Finally
'        End Try
'    End Sub
'    'S.SANDEEP [ 18 FEB 2012 ] -- END

'    'S.SANDEEP [ 20 APRIL 2012 ] -- START
'    'ENHANCEMENT : TRA CHANGES
'    Private Sub mnuDiscipline_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuDiscipline.Click
'        Try
'            If tabcSendMail.TabPages.Contains(tabpAttachment) = False Then
'                tabcSendMail.TabPages.Add(tabpAttachment)
'            End If
'            Call CallFrom(enImg_Email_RefId.Discipline_Module)
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "mnuDiscipline_Click", mstrModuleName)
'        Finally
'        End Try
'    End Sub
'    'S.SANDEEP [ 20 APRIL 2012 ] -- END



'    'Pinkal (18-Sep-2012) -- Start
'    'Enhancement : TRA Changes

'    Private Sub mnuSendLeaveForm_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuSendLeaveForm.Click
'        If tabcSendMail.TabPages.Contains(tabpAttachment) Then
'            tabcSendMail.TabPages.Remove(tabpAttachment)
'        End If
'        Call CallFrom(enImg_Email_RefId.Leave_Module)
'    End Sub

'    'Pinkal (18-Sep-2012) -- End

'    'Anjan [04 June 2014] -- Start
'    'ENHANCEMENT : Implementing Language,requested by Andrew
'    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
'        Dim objfrm As New frmLanguage
'        Try
'            If User._Object._Isrighttoleft = True Then
'                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
'                objfrm.RightToLeftLayout = True
'                Call Language.ctlRightToLeftlayOut(objfrm)
'            End If

'            Call SetMessages()
'            objfrm.displayDialog(Me)
'            Call SetLanguage()

'        Catch ex As System.Exception
'            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
'        Finally
'            objfrm.Dispose()
'            objfrm = Nothing
'        End Try
'    End Sub
'    'Anjan [04 June 2014] -- End



'End Class

