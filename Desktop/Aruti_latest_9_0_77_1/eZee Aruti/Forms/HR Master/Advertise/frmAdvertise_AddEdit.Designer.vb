﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmAdvertise_AddEdit
    Inherits eZee.Common.eZeeForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmAdvertise_AddEdit))
        Me.pnlAdvertiser = New System.Windows.Forms.Panel
        Me.gbSkill = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.objbtnAddCategory = New eZee.Common.eZeeGradientButton
        Me.cboZipcode = New System.Windows.Forms.ComboBox
        Me.cboCity = New System.Windows.Forms.ComboBox
        Me.cboState = New System.Windows.Forms.ComboBox
        Me.txtEmail = New eZee.TextBox.AlphanumericTextBox
        Me.lblEmail = New System.Windows.Forms.Label
        Me.txtFax = New eZee.TextBox.AlphanumericTextBox
        Me.lblFax = New System.Windows.Forms.Label
        Me.txtPhone = New eZee.TextBox.AlphanumericTextBox
        Me.lblPhone = New System.Windows.Forms.Label
        Me.cboCountry = New System.Windows.Forms.ComboBox
        Me.lblCountry = New System.Windows.Forms.Label
        Me.lblCity = New System.Windows.Forms.Label
        Me.lblAddress2 = New System.Windows.Forms.Label
        Me.txtCompany = New eZee.TextBox.AlphanumericTextBox
        Me.lblCompany = New System.Windows.Forms.Label
        Me.txtwebsite = New eZee.TextBox.AlphanumericTextBox
        Me.lblwebsite = New System.Windows.Forms.Label
        Me.txtcontactperson = New eZee.TextBox.AlphanumericTextBox
        Me.lblcontactperson = New System.Windows.Forms.Label
        Me.txtcontactno = New eZee.TextBox.AlphanumericTextBox
        Me.lblContactNo = New System.Windows.Forms.Label
        Me.lblPincode = New System.Windows.Forms.Label
        Me.lblstate = New System.Windows.Forms.Label
        Me.txtaddress2 = New eZee.TextBox.AlphanumericTextBox
        Me.txtaddress1 = New eZee.TextBox.AlphanumericTextBox
        Me.lblAddress = New System.Windows.Forms.Label
        Me.cboCategory = New System.Windows.Forms.ComboBox
        Me.lblCategory = New System.Windows.Forms.Label
        Me.objFooter = New eZee.Common.eZeeFooter
        Me.btnClose = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnSave = New eZee.Common.eZeeLightButton(Me.components)
        Me.EZeeHeader1 = New eZee.Common.eZeeHeader
        Me.pnlAdvertiser.SuspendLayout()
        Me.gbSkill.SuspendLayout()
        Me.objFooter.SuspendLayout()
        Me.SuspendLayout()
        '
        'pnlAdvertiser
        '
        Me.pnlAdvertiser.Controls.Add(Me.gbSkill)
        Me.pnlAdvertiser.Controls.Add(Me.objFooter)
        Me.pnlAdvertiser.Controls.Add(Me.EZeeHeader1)
        Me.pnlAdvertiser.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pnlAdvertiser.Location = New System.Drawing.Point(0, 0)
        Me.pnlAdvertiser.Name = "pnlAdvertiser"
        Me.pnlAdvertiser.Size = New System.Drawing.Size(478, 434)
        Me.pnlAdvertiser.TabIndex = 1
        '
        'gbSkill
        '
        Me.gbSkill.BorderColor = System.Drawing.Color.Black
        Me.gbSkill.Checked = False
        Me.gbSkill.CollapseAllExceptThis = False
        Me.gbSkill.CollapsedHoverImage = Nothing
        Me.gbSkill.CollapsedNormalImage = Nothing
        Me.gbSkill.CollapsedPressedImage = Nothing
        Me.gbSkill.CollapseOnLoad = False
        Me.gbSkill.Controls.Add(Me.objbtnAddCategory)
        Me.gbSkill.Controls.Add(Me.cboZipcode)
        Me.gbSkill.Controls.Add(Me.cboCity)
        Me.gbSkill.Controls.Add(Me.cboState)
        Me.gbSkill.Controls.Add(Me.txtEmail)
        Me.gbSkill.Controls.Add(Me.lblEmail)
        Me.gbSkill.Controls.Add(Me.txtFax)
        Me.gbSkill.Controls.Add(Me.lblFax)
        Me.gbSkill.Controls.Add(Me.txtPhone)
        Me.gbSkill.Controls.Add(Me.lblPhone)
        Me.gbSkill.Controls.Add(Me.cboCountry)
        Me.gbSkill.Controls.Add(Me.lblCountry)
        Me.gbSkill.Controls.Add(Me.lblCity)
        Me.gbSkill.Controls.Add(Me.lblAddress2)
        Me.gbSkill.Controls.Add(Me.txtCompany)
        Me.gbSkill.Controls.Add(Me.lblCompany)
        Me.gbSkill.Controls.Add(Me.txtwebsite)
        Me.gbSkill.Controls.Add(Me.lblwebsite)
        Me.gbSkill.Controls.Add(Me.txtcontactperson)
        Me.gbSkill.Controls.Add(Me.lblcontactperson)
        Me.gbSkill.Controls.Add(Me.txtcontactno)
        Me.gbSkill.Controls.Add(Me.lblContactNo)
        Me.gbSkill.Controls.Add(Me.lblPincode)
        Me.gbSkill.Controls.Add(Me.lblstate)
        Me.gbSkill.Controls.Add(Me.txtaddress2)
        Me.gbSkill.Controls.Add(Me.txtaddress1)
        Me.gbSkill.Controls.Add(Me.lblAddress)
        Me.gbSkill.Controls.Add(Me.cboCategory)
        Me.gbSkill.Controls.Add(Me.lblCategory)
        Me.gbSkill.ExpandedHoverImage = Nothing
        Me.gbSkill.ExpandedNormalImage = Nothing
        Me.gbSkill.ExpandedPressedImage = Nothing
        Me.gbSkill.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbSkill.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbSkill.HeaderHeight = 25
        Me.gbSkill.HeaderMessage = ""
        Me.gbSkill.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbSkill.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbSkill.HeightOnCollapse = 0
        Me.gbSkill.LeftTextSpace = 0
        Me.gbSkill.Location = New System.Drawing.Point(12, 67)
        Me.gbSkill.Name = "gbSkill"
        Me.gbSkill.OpenHeight = 300
        Me.gbSkill.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbSkill.ShowBorder = True
        Me.gbSkill.ShowCheckBox = False
        Me.gbSkill.ShowCollapseButton = False
        Me.gbSkill.ShowDefaultBorderColor = True
        Me.gbSkill.ShowDownButton = False
        Me.gbSkill.ShowHeader = True
        Me.gbSkill.Size = New System.Drawing.Size(454, 304)
        Me.gbSkill.TabIndex = 2
        Me.gbSkill.Temp = 0
        Me.gbSkill.Text = "Agency"
        Me.gbSkill.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objbtnAddCategory
        '
        Me.objbtnAddCategory.BackColor = System.Drawing.Color.Transparent
        Me.objbtnAddCategory.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnAddCategory.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnAddCategory.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnAddCategory.BorderSelected = False
        Me.objbtnAddCategory.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnAddCategory.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Add
        Me.objbtnAddCategory.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnAddCategory.Location = New System.Drawing.Point(260, 31)
        Me.objbtnAddCategory.Name = "objbtnAddCategory"
        Me.objbtnAddCategory.Size = New System.Drawing.Size(21, 21)
        Me.objbtnAddCategory.TabIndex = 213
        '
        'cboZipcode
        '
        Me.cboZipcode.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboZipcode.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboZipcode.FormattingEnabled = True
        Me.cboZipcode.Location = New System.Drawing.Point(319, 166)
        Me.cboZipcode.Name = "cboZipcode"
        Me.cboZipcode.Size = New System.Drawing.Size(124, 21)
        Me.cboZipcode.TabIndex = 8
        '
        'cboCity
        '
        Me.cboCity.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboCity.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboCity.FormattingEnabled = True
        Me.cboCity.Location = New System.Drawing.Point(101, 166)
        Me.cboCity.Name = "cboCity"
        Me.cboCity.Size = New System.Drawing.Size(124, 21)
        Me.cboCity.TabIndex = 7
        '
        'cboState
        '
        Me.cboState.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboState.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboState.FormattingEnabled = True
        Me.cboState.Location = New System.Drawing.Point(319, 139)
        Me.cboState.Name = "cboState"
        Me.cboState.Size = New System.Drawing.Size(123, 21)
        Me.cboState.TabIndex = 6
        '
        'txtEmail
        '
        Me.txtEmail.Flags = 0
        Me.txtEmail.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtEmail.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtEmail.Location = New System.Drawing.Point(101, 220)
        Me.txtEmail.Name = "txtEmail"
        Me.txtEmail.Size = New System.Drawing.Size(342, 21)
        Me.txtEmail.TabIndex = 11
        '
        'lblEmail
        '
        Me.lblEmail.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEmail.Location = New System.Drawing.Point(8, 222)
        Me.lblEmail.Name = "lblEmail"
        Me.lblEmail.Size = New System.Drawing.Size(81, 16)
        Me.lblEmail.TabIndex = 136
        Me.lblEmail.Text = "Email"
        Me.lblEmail.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtFax
        '
        Me.txtFax.Flags = 0
        Me.txtFax.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtFax.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtFax.Location = New System.Drawing.Point(319, 193)
        Me.txtFax.Name = "txtFax"
        Me.txtFax.Size = New System.Drawing.Size(124, 21)
        Me.txtFax.TabIndex = 10
        '
        'lblFax
        '
        Me.lblFax.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblFax.Location = New System.Drawing.Point(231, 195)
        Me.lblFax.Name = "lblFax"
        Me.lblFax.Size = New System.Drawing.Size(81, 16)
        Me.lblFax.TabIndex = 133
        Me.lblFax.Text = "Fax"
        Me.lblFax.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtPhone
        '
        Me.txtPhone.Flags = 0
        Me.txtPhone.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtPhone.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtPhone.Location = New System.Drawing.Point(101, 193)
        Me.txtPhone.Name = "txtPhone"
        Me.txtPhone.Size = New System.Drawing.Size(124, 21)
        Me.txtPhone.TabIndex = 9
        '
        'lblPhone
        '
        Me.lblPhone.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPhone.Location = New System.Drawing.Point(8, 195)
        Me.lblPhone.Name = "lblPhone"
        Me.lblPhone.Size = New System.Drawing.Size(81, 16)
        Me.lblPhone.TabIndex = 131
        Me.lblPhone.Text = "Phone"
        Me.lblPhone.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboCountry
        '
        Me.cboCountry.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboCountry.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboCountry.FormattingEnabled = True
        Me.cboCountry.Location = New System.Drawing.Point(101, 139)
        Me.cboCountry.Name = "cboCountry"
        Me.cboCountry.Size = New System.Drawing.Size(124, 21)
        Me.cboCountry.TabIndex = 5
        '
        'lblCountry
        '
        Me.lblCountry.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCountry.Location = New System.Drawing.Point(8, 141)
        Me.lblCountry.Name = "lblCountry"
        Me.lblCountry.Size = New System.Drawing.Size(81, 16)
        Me.lblCountry.TabIndex = 129
        Me.lblCountry.Text = "Country"
        Me.lblCountry.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblCity
        '
        Me.lblCity.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCity.Location = New System.Drawing.Point(8, 168)
        Me.lblCity.Name = "lblCity"
        Me.lblCity.Size = New System.Drawing.Size(81, 16)
        Me.lblCity.TabIndex = 127
        Me.lblCity.Text = "City"
        Me.lblCity.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblAddress2
        '
        Me.lblAddress2.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblAddress2.Location = New System.Drawing.Point(8, 114)
        Me.lblAddress2.Name = "lblAddress2"
        Me.lblAddress2.Size = New System.Drawing.Size(81, 16)
        Me.lblAddress2.TabIndex = 126
        Me.lblAddress2.Text = "Address2"
        Me.lblAddress2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtCompany
        '
        Me.txtCompany.Flags = 0
        Me.txtCompany.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtCompany.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtCompany.Location = New System.Drawing.Point(101, 58)
        Me.txtCompany.Name = "txtCompany"
        Me.txtCompany.Size = New System.Drawing.Size(342, 21)
        Me.txtCompany.TabIndex = 2
        '
        'lblCompany
        '
        Me.lblCompany.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCompany.Location = New System.Drawing.Point(8, 61)
        Me.lblCompany.Name = "lblCompany"
        Me.lblCompany.Size = New System.Drawing.Size(81, 16)
        Me.lblCompany.TabIndex = 124
        Me.lblCompany.Text = "Company"
        Me.lblCompany.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtwebsite
        '
        Me.txtwebsite.Flags = 0
        Me.txtwebsite.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtwebsite.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtwebsite.Location = New System.Drawing.Point(101, 247)
        Me.txtwebsite.Name = "txtwebsite"
        Me.txtwebsite.Size = New System.Drawing.Size(342, 21)
        Me.txtwebsite.TabIndex = 12
        '
        'lblwebsite
        '
        Me.lblwebsite.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblwebsite.Location = New System.Drawing.Point(8, 249)
        Me.lblwebsite.Name = "lblwebsite"
        Me.lblwebsite.Size = New System.Drawing.Size(81, 16)
        Me.lblwebsite.TabIndex = 120
        Me.lblwebsite.Text = "Website"
        Me.lblwebsite.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtcontactperson
        '
        Me.txtcontactperson.Flags = 0
        Me.txtcontactperson.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtcontactperson.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtcontactperson.Location = New System.Drawing.Point(101, 274)
        Me.txtcontactperson.Name = "txtcontactperson"
        Me.txtcontactperson.Size = New System.Drawing.Size(124, 21)
        Me.txtcontactperson.TabIndex = 13
        '
        'lblcontactperson
        '
        Me.lblcontactperson.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblcontactperson.Location = New System.Drawing.Point(8, 276)
        Me.lblcontactperson.Name = "lblcontactperson"
        Me.lblcontactperson.Size = New System.Drawing.Size(81, 16)
        Me.lblcontactperson.TabIndex = 118
        Me.lblcontactperson.Text = "Contact Person"
        Me.lblcontactperson.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtcontactno
        '
        Me.txtcontactno.Flags = 0
        Me.txtcontactno.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtcontactno.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtcontactno.Location = New System.Drawing.Point(319, 274)
        Me.txtcontactno.Name = "txtcontactno"
        Me.txtcontactno.Size = New System.Drawing.Size(124, 21)
        Me.txtcontactno.TabIndex = 14
        '
        'lblContactNo
        '
        Me.lblContactNo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblContactNo.Location = New System.Drawing.Point(231, 276)
        Me.lblContactNo.Name = "lblContactNo"
        Me.lblContactNo.Size = New System.Drawing.Size(81, 16)
        Me.lblContactNo.TabIndex = 116
        Me.lblContactNo.Text = "Contact No"
        Me.lblContactNo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblPincode
        '
        Me.lblPincode.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPincode.Location = New System.Drawing.Point(232, 167)
        Me.lblPincode.Name = "lblPincode"
        Me.lblPincode.Size = New System.Drawing.Size(81, 16)
        Me.lblPincode.TabIndex = 114
        Me.lblPincode.Text = "Pincode"
        Me.lblPincode.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblstate
        '
        Me.lblstate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblstate.Location = New System.Drawing.Point(231, 141)
        Me.lblstate.Name = "lblstate"
        Me.lblstate.Size = New System.Drawing.Size(81, 16)
        Me.lblstate.TabIndex = 112
        Me.lblstate.Text = "State"
        Me.lblstate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtaddress2
        '
        Me.txtaddress2.Flags = 0
        Me.txtaddress2.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtaddress2.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtaddress2.Location = New System.Drawing.Point(101, 112)
        Me.txtaddress2.Name = "txtaddress2"
        Me.txtaddress2.Size = New System.Drawing.Size(342, 21)
        Me.txtaddress2.TabIndex = 4
        '
        'txtaddress1
        '
        Me.txtaddress1.Flags = 0
        Me.txtaddress1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtaddress1.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtaddress1.Location = New System.Drawing.Point(101, 85)
        Me.txtaddress1.Name = "txtaddress1"
        Me.txtaddress1.Size = New System.Drawing.Size(342, 21)
        Me.txtaddress1.TabIndex = 3
        '
        'lblAddress
        '
        Me.lblAddress.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblAddress.Location = New System.Drawing.Point(8, 87)
        Me.lblAddress.Name = "lblAddress"
        Me.lblAddress.Size = New System.Drawing.Size(81, 16)
        Me.lblAddress.TabIndex = 109
        Me.lblAddress.Text = "Address1"
        Me.lblAddress.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboCategory
        '
        Me.cboCategory.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboCategory.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboCategory.FormattingEnabled = True
        Me.cboCategory.Location = New System.Drawing.Point(101, 31)
        Me.cboCategory.Name = "cboCategory"
        Me.cboCategory.Size = New System.Drawing.Size(152, 21)
        Me.cboCategory.TabIndex = 1
        '
        'lblCategory
        '
        Me.lblCategory.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCategory.Location = New System.Drawing.Point(8, 33)
        Me.lblCategory.Name = "lblCategory"
        Me.lblCategory.Size = New System.Drawing.Size(81, 16)
        Me.lblCategory.TabIndex = 103
        Me.lblCategory.Text = "Category"
        Me.lblCategory.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objFooter
        '
        Me.objFooter.BorderColor = System.Drawing.Color.Silver
        Me.objFooter.Controls.Add(Me.btnClose)
        Me.objFooter.Controls.Add(Me.btnSave)
        Me.objFooter.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.objFooter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objFooter.GradiantStyle = eZee.Common.eZeeFooter.GradientStyle.Up
        Me.objFooter.GradientColor1 = System.Drawing.SystemColors.Control
        Me.objFooter.GradientColor2 = System.Drawing.SystemColors.Control
        Me.objFooter.Location = New System.Drawing.Point(0, 379)
        Me.objFooter.Name = "objFooter"
        Me.objFooter.Size = New System.Drawing.Size(478, 55)
        Me.objFooter.TabIndex = 20
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnClose.BackColor = System.Drawing.Color.White
        Me.btnClose.BackgroundImage = CType(resources.GetObject("btnClose.BackgroundImage"), System.Drawing.Image)
        Me.btnClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnClose.BorderColor = System.Drawing.Color.Empty
        Me.btnClose.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnClose.FlatAppearance.BorderSize = 0
        Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClose.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.ForeColor = System.Drawing.Color.Black
        Me.btnClose.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnClose.GradientForeColor = System.Drawing.Color.Black
        Me.btnClose.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Location = New System.Drawing.Point(372, 13)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Size = New System.Drawing.Size(94, 30)
        Me.btnClose.TabIndex = 16
        Me.btnClose.Text = "&Close"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'btnSave
        '
        Me.btnSave.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnSave.BackColor = System.Drawing.Color.White
        Me.btnSave.BackgroundImage = CType(resources.GetObject("btnSave.BackgroundImage"), System.Drawing.Image)
        Me.btnSave.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnSave.BorderColor = System.Drawing.Color.Empty
        Me.btnSave.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnSave.FlatAppearance.BorderSize = 0
        Me.btnSave.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnSave.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSave.ForeColor = System.Drawing.Color.Black
        Me.btnSave.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnSave.GradientForeColor = System.Drawing.Color.Black
        Me.btnSave.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSave.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnSave.Location = New System.Drawing.Point(272, 13)
        Me.btnSave.Name = "btnSave"
        Me.btnSave.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSave.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnSave.Size = New System.Drawing.Size(94, 30)
        Me.btnSave.TabIndex = 15
        Me.btnSave.Text = "&Save"
        Me.btnSave.UseVisualStyleBackColor = True
        '
        'EZeeHeader1
        '
        Me.EZeeHeader1.BackColor = System.Drawing.SystemColors.Control
        Me.EZeeHeader1.BorderColor = System.Drawing.SystemColors.ControlDark
        Me.EZeeHeader1.DescriptionForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.EZeeHeader1.Dock = System.Windows.Forms.DockStyle.Top
        Me.EZeeHeader1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.EZeeHeader1.GradiantStyle = eZee.Common.eZeeHeader.GradientStyle.Central
        Me.EZeeHeader1.GradientColor1 = System.Drawing.SystemColors.Window
        Me.EZeeHeader1.GradientColor2 = System.Drawing.SystemColors.Control
        Me.EZeeHeader1.HeaderTextForeColor = System.Drawing.SystemColors.ControlText
        Me.EZeeHeader1.Icon = Nothing
        Me.EZeeHeader1.Location = New System.Drawing.Point(0, 0)
        Me.EZeeHeader1.Message = ""
        Me.EZeeHeader1.Name = "EZeeHeader1"
        Me.EZeeHeader1.Size = New System.Drawing.Size(478, 60)
        Me.EZeeHeader1.TabIndex = 0
        Me.EZeeHeader1.Title = "Agency Information"
        '
        'frmAdvertise_AddEdit
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(478, 434)
        Me.Controls.Add(Me.pnlAdvertiser)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmAdvertise_AddEdit"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Add/Edit Agency"
        Me.pnlAdvertiser.ResumeLayout(False)
        Me.gbSkill.ResumeLayout(False)
        Me.gbSkill.PerformLayout()
        Me.objFooter.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents pnlAdvertiser As System.Windows.Forms.Panel
    Friend WithEvents gbSkill As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents objFooter As eZee.Common.eZeeFooter
    Friend WithEvents btnClose As eZee.Common.eZeeLightButton
    Friend WithEvents btnSave As eZee.Common.eZeeLightButton
    Friend WithEvents EZeeHeader1 As eZee.Common.eZeeHeader
    Friend WithEvents lblCity As System.Windows.Forms.Label
    Friend WithEvents lblAddress2 As System.Windows.Forms.Label
    Friend WithEvents txtCompany As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblCompany As System.Windows.Forms.Label
    Friend WithEvents txtwebsite As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblwebsite As System.Windows.Forms.Label
    Friend WithEvents txtcontactperson As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblcontactperson As System.Windows.Forms.Label
    Friend WithEvents txtcontactno As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblContactNo As System.Windows.Forms.Label
    Friend WithEvents lblPincode As System.Windows.Forms.Label
    Friend WithEvents lblstate As System.Windows.Forms.Label
    Friend WithEvents txtaddress2 As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents txtaddress1 As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblAddress As System.Windows.Forms.Label
    Friend WithEvents cboCategory As System.Windows.Forms.ComboBox
    Friend WithEvents lblCategory As System.Windows.Forms.Label
    Friend WithEvents txtFax As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblFax As System.Windows.Forms.Label
    Friend WithEvents txtPhone As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblPhone As System.Windows.Forms.Label
    Friend WithEvents cboCountry As System.Windows.Forms.ComboBox
    Friend WithEvents lblCountry As System.Windows.Forms.Label
    Friend WithEvents txtEmail As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblEmail As System.Windows.Forms.Label
    Friend WithEvents cboZipcode As System.Windows.Forms.ComboBox
    Friend WithEvents cboCity As System.Windows.Forms.ComboBox
    Friend WithEvents cboState As System.Windows.Forms.ComboBox
    Friend WithEvents objbtnAddCategory As eZee.Common.eZeeGradientButton
End Class
