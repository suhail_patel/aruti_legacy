﻿Option Strict On

Imports eZeeCommonLib
Imports Aruti.Data
'Last Message Index = 3

Public Class frmAdvertiseList

#Region "Private Variable"

    Private objAdvertisemaster As clsAdvertise_master
    Private ReadOnly mstrModuleName As String = "frmAdvertiseList"

#End Region

#Region "Form's Event"

    Private Sub frmAdvertiseList_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        objAdvertisemaster = New clsAdvertise_master
        Try
            Call Set_Logo(Me, gApplicationType)
            'S.SANDEEP [ 20 AUG 2011 ] -- START
            'ENHANCEMENT : LANGUAGES IMPLEMENTATION
            Language.setLanguage(Me.Name)
            Call OtherSettings()
            'S.SANDEEP [ 20 AUG 2011 ] -- END
            Call SetVisibility()
            fillList()
            If lvAdvertise.Items.Count > 0 Then lvAdvertise.Items(0).Selected = True
            lvAdvertise.Select()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmAdvertiseList_Load", mstrModuleName)
        End Try
    End Sub

    Private Sub frmAdvertiseList_KeyUp(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles MyBase.KeyUp
        Try
            If e.KeyCode = Keys.Delete And lvAdvertise.Focused = True Then
                'Sohail (28 Jun 2011) -- Start
                'Issue : Delete event fired even if there is no delete privilege
                'btnDelete_Click(sender, e)
                Call btnDelete.PerformClick()
                'Sohail (28 Jun 2011) -- End
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmAdvertiseList_KeyUp", mstrModuleName)
        End Try
    End Sub

    Private Sub frmAdvertiseList_FormClosed(ByVal sender As System.Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles MyBase.FormClosed
        objAdvertisemaster = Nothing
    End Sub

    'S.SANDEEP [ 20 AUG 2011 ] -- START
    'ENHANCEMENT : LANGUAGES IMPLEMENTATION
    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsAdvertise_master.SetMessages()
            objfrm._Other_ModuleNames = "clsAdvertise_master"
            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub
    'S.SANDEEP [ 20 AUG 2011 ] -- END

#End Region

#Region "Button's Event"

    Private Sub btnNew_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNew.Click
        Try
            Dim objFrmAdvertise_AddEdit As New frmAdvertise_AddEdit

            'S.SANDEEP [ 20 AUG 2011 ] -- START
            'ENHANCEMENT : LANGUAGES IMPLEMENTATION
            If User._Object._Isrighttoleft = True Then
                objFrmAdvertise_AddEdit.RightToLeft = Windows.Forms.RightToLeft.Yes
                objFrmAdvertise_AddEdit.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objFrmAdvertise_AddEdit)
            End If
            'S.SANDEEP [ 20 AUG 2011 ] -- END

            If objFrmAdvertise_AddEdit.displayDialog(-1, enAction.ADD_CONTINUE) Then
                fillList()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnNew_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnEdit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEdit.Click
        Try
            If lvAdvertise.SelectedItems.Count < 1 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please select Advertise from the list to perform further operation."), enMsgBoxStyle.Information) '?1
                lvAdvertise.Select()
                Exit Sub
            End If

            If objAdvertisemaster.isUsed(CInt(lvAdvertise.SelectedItems(0).Tag)) Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Sorry, You cannot edit this Advertise. Reason: This Advertise is in use."), enMsgBoxStyle.Information) '?2
                lvAdvertise.Select()
                Exit Sub
            End If


            Dim objfrmAdvertise_AddEdit As New frmAdvertise_AddEdit



            Try
                Dim intSelectedIndex As Integer
                intSelectedIndex = lvAdvertise.SelectedItems(0).Index

                'S.SANDEEP [ 20 AUG 2011 ] -- START
                'ENHANCEMENT : LANGUAGES IMPLEMENTATION
                If User._Object._Isrighttoleft = True Then
                    objfrmAdvertise_AddEdit.RightToLeft = Windows.Forms.RightToLeft.Yes
                    objfrmAdvertise_AddEdit.RightToLeftLayout = True
                    Call Language.ctlRightToLeftlayOut(objfrmAdvertise_AddEdit)
                End If
                'S.SANDEEP [ 20 AUG 2011 ] -- END

                If objfrmAdvertise_AddEdit.displayDialog(CInt(lvAdvertise.SelectedItems(0).Tag), enAction.EDIT_ONE) Then
                    fillList()
                End If
                objfrmAdvertise_AddEdit = Nothing

                lvAdvertise.Items(intSelectedIndex).Selected = True
                lvAdvertise.EnsureVisible(intSelectedIndex)
                lvAdvertise.Select()
            Catch ex As Exception
                Call DisplayError.Show("-1", ex.Message, "btnEdit_Click", mstrModuleName)
            Finally
                If objfrmAdvertise_AddEdit IsNot Nothing Then objfrmAdvertise_AddEdit.Dispose()
            End Try
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnEdit_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDelete.Click
        If lvAdvertise.SelectedItems.Count < 1 Then
            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please select Advertise from the list to perform further operation."), enMsgBoxStyle.Information) '?1
            lvAdvertise.Select()
            Exit Sub
        End If
        If objAdvertisemaster.isUsed(CInt(lvAdvertise.SelectedItems(0).Tag)) Then
            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Sorry, You cannot delete this Advertise. Reason: This Advertise is in use."), enMsgBoxStyle.Information) '?2
            lvAdvertise.Select()
            Exit Sub
        End If
        Try
            Dim intSelectedIndex As Integer
            intSelectedIndex = lvAdvertise.SelectedItems(0).Index

            If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Are you sure you want to delete this Advertise?"), CType(enMsgBoxStyle.Question + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.Yes Then

                'S.SANDEEP [28-May-2018] -- START
                'ISSUE/ENHANCEMENT : {Audit Trails} 
                objAdvertisemaster._FormName = mstrModuleName
                objAdvertisemaster._LoginEmployeeunkid = 0
                objAdvertisemaster._ClientIP = getIP()
                objAdvertisemaster._HostName = getHostName()
                objAdvertisemaster._FromWeb = False
                objAdvertisemaster._AuditUserId = User._Object._Userunkid
objAdvertisemaster._CompanyUnkid = Company._Object._Companyunkid
                objAdvertisemaster._AuditDate = ConfigParameter._Object._CurrentDateAndTime
                'S.SANDEEP [28-May-2018] -- END

                objAdvertisemaster.Delete(CInt(lvAdvertise.SelectedItems(0).Tag))
                lvAdvertise.SelectedItems(0).Remove()

                If lvAdvertise.Items.Count <= 0 Then
                    Exit Try
                End If

                If lvAdvertise.Items.Count = intSelectedIndex Then
                    intSelectedIndex = lvAdvertise.Items.Count - 1
                    lvAdvertise.Items(intSelectedIndex).Selected = True
                    lvAdvertise.EnsureVisible(intSelectedIndex)
                ElseIf lvAdvertise.Items.Count <> 0 Then
                    lvAdvertise.Items(intSelectedIndex).Selected = True
                    lvAdvertise.EnsureVisible(intSelectedIndex)
                End If
            End If
            lvAdvertise.Select()
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "btnDelete_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Me.Close()
    End Sub

#End Region

#Region " Private Methods "

    Private Sub fillList()
        Dim dsAdverise As New DataSet
        Try

            If User._Object.Privilege._AllowToViewAgencyMasterList = True Then   'Pinkal (09-Jul-2012) -- Start

                dsAdverise = objAdvertisemaster.GetList("List")

                Dim lvItem As ListViewItem

                lvAdvertise.Items.Clear()
                For Each drRow As DataRow In dsAdverise.Tables(0).Rows
                    lvItem = New ListViewItem
                    lvItem.Text = drRow("name").ToString
                    lvItem.Tag = drRow("advertiseunkid")
                    lvItem.SubItems.Add(drRow("company").ToString)
                    lvItem.SubItems.Add(drRow("email").ToString)
                    lvItem.SubItems.Add(drRow("contactperson").ToString)
                    lvItem.SubItems.Add(drRow("contactno").ToString)
                    lvAdvertise.Items.Add(lvItem)
                Next

                If lvAdvertise.Items.Count > 16 Then
                    colhcontactno.Width = 100 - 18
                Else
                    colhcontactno.Width = 100
                End If

            End If

        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "fillList", mstrModuleName)
        Finally
            dsAdverise.Dispose()
        End Try
    End Sub

    Private Sub SetVisibility()

        Try

            btnNew.Enabled = User._Object.Privilege._AddAdvertise
            btnEdit.Enabled = User._Object.Privilege._EditAdvertise
            btnDelete.Enabled = User._Object.Privilege._DeleteAdvertise

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetVisibility", mstrModuleName)
        End Try

    End Sub

#End Region


    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Private Sub OtherSettings()
        Try
            Me.SuspendLayout()
           
            Call SetLanguage()

           
            Me.EZeeHeader1.GradientColor1 = GUI._HeaderBackColor1
            Me.EZeeHeader1.GradientColor2 = GUI._HeaderBackColor2
            Me.EZeeHeader1.BorderColor = GUI._HeaderBorderColor
            Me.EZeeHeader1.HeaderTextForeColor = GUI._HeaderTitleFontColor
            Me.EZeeHeader1.DescriptionForeColor = GUI._HeaderMessageFontColor


            Me.btnClose.GradientBackColor = GUI._ButttonBackColor
            Me.btnClose.GradientForeColor = GUI._ButttonFontColor

            Me.btnDelete.GradientBackColor = GUI._ButttonBackColor
            Me.btnDelete.GradientForeColor = GUI._ButttonFontColor

            Me.btnEdit.GradientBackColor = GUI._ButttonBackColor
            Me.btnEdit.GradientForeColor = GUI._ButttonFontColor

            Me.btnNew.GradientBackColor = GUI._ButttonBackColor
            Me.btnNew.GradientForeColor = GUI._ButttonFontColor


            Me.ResumeLayout()
        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "OtherSettings", mstrModuleName)
        End Try
    End Sub


    Private Sub SetLanguage()
        Try
            Me.Text = Language._Object.getCaption(Me.Name, Me.Text)

            Me.EZeeHeader1.Title = Language._Object.getCaption(Me.EZeeHeader1.Name & "_Title", Me.EZeeHeader1.Title)
            Me.EZeeHeader1.Message = Language._Object.getCaption(Me.EZeeHeader1.Name & "_Message", Me.EZeeHeader1.Message)
            Me.colhAdvertiseCategory.Text = Language._Object.getCaption(CStr(Me.colhAdvertiseCategory.Tag), Me.colhAdvertiseCategory.Text)
            Me.colhCompany.Text = Language._Object.getCaption(CStr(Me.colhCompany.Tag), Me.colhCompany.Text)
            Me.colhContactperson.Text = Language._Object.getCaption(CStr(Me.colhContactperson.Tag), Me.colhContactperson.Text)
            Me.colhcontactno.Text = Language._Object.getCaption(CStr(Me.colhcontactno.Tag), Me.colhcontactno.Text)
            Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
            Me.btnDelete.Text = Language._Object.getCaption(Me.btnDelete.Name, Me.btnDelete.Text)
            Me.btnEdit.Text = Language._Object.getCaption(Me.btnEdit.Name, Me.btnEdit.Text)
            Me.btnNew.Text = Language._Object.getCaption(Me.btnNew.Name, Me.btnNew.Text)
            Me.colhEmail.Text = Language._Object.getCaption(CStr(Me.colhEmail.Tag), Me.colhEmail.Text)

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
        End Try
    End Sub


    Private Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Please select Advertise from the list to perform further operation.")
            Language.setMessage(mstrModuleName, 2, "Sorry, You cannot edit this Advertise. Reason: This Advertise is in use.")
            Language.setMessage(mstrModuleName, 3, "Are you sure you want to delete this Advertise?")
            Language.setMessage(mstrModuleName, 4, "Sorry, You cannot delete this Advertise. Reason: This Advertise is in use.")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class