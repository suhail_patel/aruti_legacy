﻿Option Strict On

Imports eZeeCommonLib
Imports Aruti.Data

'Last Message Index = 3

Public Class frmBenefitplan_AddEdit

#Region "Private Variable"

    Private ReadOnly mstrModuleName As String = "frmBenefitplan_AddEdit"
    Private mblnCancel As Boolean = True
    Private objBenefitplanMaster As clsbenefitplan_master
    Private menAction As enAction = enAction.ADD_ONE
    Private mintBenefitplanMasterUnkid As Integer = -1

#End Region

#Region " Display Dialog "

    Public Function displayDialog(ByRef intUnkId As Integer, ByVal eAction As enAction) As Boolean
        Try
            mintBenefitplanMasterUnkid = intUnkId
            menAction = eAction

            Me.ShowDialog()

            intUnkId = mintBenefitplanMasterUnkid

            Return Not mblnCancel
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "displayDialog", mstrModuleName)
        End Try
    End Function

#End Region

#Region "Form's Event"

    Private Sub frmBenefitplan_AddEdit_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        objBenefitplanMaster = New clsbenefitplan_master
        Try
            Call Set_Logo(Me, gApplicationType)

            'S.SANDEEP [ 20 AUG 2011 ] -- START
            'ENHANCEMENT : LANGUAGES IMPLEMENTATION
            Language.setLanguage(Me.Name)
            Call OtherSettings()
            'S.SANDEEP [ 20 AUG 2011 ] -- END

            Call SetVisibility()
            setColor()
            If menAction = enAction.EDIT_ONE Then
                objBenefitplanMaster._Benefitplanunkid = mintBenefitplanMasterUnkid
            End If
            FillCombo()
            GetValue()
            cboBenefitGroup.Select()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmBenefitplan_AddEdit_Load", mstrModuleName)
        End Try
    End Sub

    Private Sub frmBenefitplan_AddEdit_KeyPress(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles MyBase.KeyPress
        Try
            If Asc(e.KeyChar) = 13 Then
                SendKeys.Send("{TAB}")
                e.Handled = True
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmBenefitplan_AddEdit_KeyPress", mstrModuleName)
        End Try
    End Sub

    Private Sub frmBenefitplan_AddEdit_KeyDown(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles MyBase.KeyDown
        Try
            If e.Control = True And e.KeyCode = Keys.S Then
                btnSave_Click(sender, e)
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmBenefitplan_AddEdit_KeyDown", mstrModuleName)
        End Try
    End Sub

    Private Sub frmBenefitplan_AddEdit_FormClosed(ByVal sender As System.Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles MyBase.FormClosed
        objBenefitplanMaster = Nothing
    End Sub

    'S.SANDEEP [ 20 AUG 2011 ] -- START
    'ENHANCEMENT : LANGUAGES IMPLEMENTATION
    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsbenefitplan_master.SetMessages()
            objfrm._Other_ModuleNames = "clsbenefitplan_master"
            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub
    'S.SANDEEP [ 20 AUG 2011 ] -- END

#End Region

#Region "Button's Event"

    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Dim blnFlag As Boolean = False
        Try
            If CInt(cboBenefitGroup.SelectedValue) = 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Benefit Group is compulsory information.Please Select Benefit Group."), enMsgBoxStyle.Information)
                cboBenefitGroup.Focus()
                Exit Sub
            ElseIf Trim(txtBenefitplancode.Text) = "" Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Benefit Plan Code cannot be blank. Benefit Plan Code is required information."), enMsgBoxStyle.Information)
                txtBenefitplancode.Focus()
                Exit Sub
            ElseIf Trim(txtBenefitplanName.Text) = "" Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Benefit Plan Name cannot be blank. Benefit Plan Name is required information."), enMsgBoxStyle.Information)
                txtBenefitplanName.Focus()
                Exit Sub
            End If

            Call SetValue()

            'S.SANDEEP [28-May-2018] -- START
            'ISSUE/ENHANCEMENT : {Audit Trails} 
            objBenefitplanMaster._FormName = mstrModuleName
            objBenefitplanMaster._LoginEmployeeunkid = 0
            objBenefitplanMaster._ClientIP = getIP()
            objBenefitplanMaster._HostName = getHostName()
            objBenefitplanMaster._FromWeb = False
            objBenefitplanMaster._AuditUserId = User._Object._Userunkid
objBenefitplanMaster._CompanyUnkid = Company._Object._Companyunkid
            objBenefitplanMaster._AuditDate = ConfigParameter._Object._CurrentDateAndTime
            'S.SANDEEP [28-May-2018] -- END
            If menAction = enAction.EDIT_ONE Then
                blnFlag = objBenefitplanMaster.Update()
            Else
                blnFlag = objBenefitplanMaster.Insert()
            End If

            If blnFlag = False And objBenefitplanMaster._Message <> "" Then
                eZeeMsgBox.Show(objBenefitplanMaster._Message, enMsgBoxStyle.Information)
            End If

            If blnFlag Then
                mblnCancel = False
                If menAction = enAction.ADD_CONTINUE Then
                    objBenefitplanMaster = Nothing
                    objBenefitplanMaster = New clsbenefitplan_master
                    Call GetValue()
                    cboBenefitGroup.Select()
                Else
                    mintBenefitplanMasterUnkid = objBenefitplanMaster._Benefitplanunkid
                    Me.Close()
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnSave_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnOtherLanguage_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnOtherLanguage.Click
        Dim objFrmLangPopup As New NameLanguagePopup_Form
        Try
            'S.SANDEEP [ 20 AUG 2011 ] -- START
            'ENHANCEMENT : LANGUAGES IMPLEMENTATION
            If User._Object._Isrighttoleft = True Then
                objFrmLangPopup.RightToLeft = Windows.Forms.RightToLeft.Yes
                objFrmLangPopup.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objFrmLangPopup)
            End If
            'S.SANDEEP [ 20 AUG 2011 ] -- END
            Call objFrmLangPopup.displayDialog(txtBenefitplanName.Text, objBenefitplanMaster._Benefitplanname1, objBenefitplanMaster._Benefitplanname2)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnOtherLanguage_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnAddGroup_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnAddGroup.Click
        Try
            Dim objFrmCommonmaster As New frmCommonMaster

            'S.SANDEEP [ 20 AUG 2011 ] -- START
            'ENHANCEMENT : LANGUAGES IMPLEMENTATION
            If User._Object._Isrighttoleft = True Then
                objFrmCommonmaster.RightToLeft = Windows.Forms.RightToLeft.Yes
                objFrmCommonmaster.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objFrmCommonmaster)
            End If
            'S.SANDEEP [ 20 AUG 2011 ] -- END

            objFrmCommonmaster.displayDialog(-1, clsCommon_Master.enCommonMaster.BENEFIT_GROUP, enAction.ADD_ONE)
            FillCombo()
            cboBenefitGroup.Select()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnAddGroup_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Me.Close()
    End Sub

#End Region

#Region "Private Methods"

    Private Sub setColor()
        Try
            txtBenefitplancode.BackColor = GUI.ColorOptional
            txtBenefitplanName.BackColor = GUI.ColorComp
            txtDescription.BackColor = GUI.ColorOptional
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "setColor", mstrModuleName)
        End Try
    End Sub

    Private Sub GetValue()
        Try
            txtBenefitplancode.Text = objBenefitplanMaster._Benefitplancode
            txtBenefitplanName.Text = objBenefitplanMaster._Benefitplanname
            cboBenefitGroup.SelectedValue = CInt(objBenefitplanMaster._Benefitgroupunkid)
            txtDescription.Text = objBenefitplanMaster._Description
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetValue", mstrModuleName)
        End Try
    End Sub

    Private Sub SetValue()
        Try
            objBenefitplanMaster._Benefitplancode = txtBenefitplancode.Text.Trim
            objBenefitplanMaster._Benefitplanname = txtBenefitplanName.Text.Trim
            objBenefitplanMaster._Benefitgroupunkid = CInt(cboBenefitGroup.SelectedValue)
            objBenefitplanMaster._Description = txtDescription.Text.Trim
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetValue", mstrModuleName)
        End Try
    End Sub

    Private Sub FillCombo()
        Try
            Dim ObjCommonMaster As New clsCommon_Master
            Dim dsBenefitGroup As DataSet = ObjCommonMaster.getComboList(clsCommon_Master.enCommonMaster.BENEFIT_GROUP, True, "Benefit Group")
            cboBenefitGroup.ValueMember = "masterunkid"
            cboBenefitGroup.DisplayMember = "name"
            cboBenefitGroup.DataSource = dsBenefitGroup.Tables(0)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        End Try
    End Sub

    Private Sub SetVisibility()

        Try
            objbtnAddGroup.Enabled = User._Object.Privilege._AddCommonMasters

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetVisibility", mstrModuleName)
        End Try

    End Sub

#End Region


	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Private Sub OtherSettings()
		Try
			Me.SuspendLayout()
			

			Call SetLanguage()
			
			Me.gbBenefitPlan.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbBenefitPlan.ForeColor = GUI._eZeeContainerHeaderForeColor 


			Me.btnSave.GradientBackColor = GUI._ButttonBackColor 
			Me.btnSave.GradientForeColor = GUI._ButttonFontColor

			Me.btnClose.GradientBackColor = GUI._ButttonBackColor 
			Me.btnClose.GradientForeColor = GUI._ButttonFontColor


			Me.ResumeLayout()
		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "OtherSettings", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetLanguage()
		Try
			Me.Text = Language._Object.getCaption(Me.Name, Me.Text)
			
			Me.gbBenefitPlan.Text = Language._Object.getCaption(Me.gbBenefitPlan.Name, Me.gbBenefitPlan.Text)
			Me.lblBenefitplanName.Text = Language._Object.getCaption(Me.lblBenefitplanName.Name, Me.lblBenefitplanName.Text)
			Me.lblBenefitplancode.Text = Language._Object.getCaption(Me.lblBenefitplancode.Name, Me.lblBenefitplancode.Text)
			Me.lblBenefitGroup.Text = Language._Object.getCaption(Me.lblBenefitGroup.Name, Me.lblBenefitGroup.Text)
			Me.btnSave.Text = Language._Object.getCaption(Me.btnSave.Name, Me.btnSave.Text)
			Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
			Me.lblDescription.Text = Language._Object.getCaption(Me.lblDescription.Name, Me.lblDescription.Text)

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetLanguage", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "Benefit Group is compulsory information.Please Select Benefit Group.")
			Language.setMessage(mstrModuleName, 2, "Benefit Plan Code cannot be blank. Benefit Plan Code is required information.")
			Language.setMessage(mstrModuleName, 3, "Benefit Plan Name cannot be blank. Benefit Plan Name is required information.")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class