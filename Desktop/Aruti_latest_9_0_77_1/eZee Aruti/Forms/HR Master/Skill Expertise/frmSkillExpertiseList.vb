﻿Option Strict On

Imports eZeeCommonLib
Imports Aruti.Data

Public Class frmSkillExpertiseList
    Private ReadOnly mstrModuleName As String = "frmSkillExpertiseList"
    Private objSkillExpertise As clsSkillExpertise_master

#Region " Form's Events "

    Private Sub frmSkillExpertiseList_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Call Set_Logo(Me, gApplicationType)

            Call OtherSettings()

            Call SetVisibility()

            Call FillList()

            Call FillCombo()

            If lvSkillExpertise.Items.Count <= 0 Then
                objSkillExpertise = New clsSkillExpertise_master

                'S.SANDEEP [28-May-2018] -- START
                'ISSUE/ENHANCEMENT : {Audit Trails} 
                objSkillExpertise._FormName = mstrModuleName
                objSkillExpertise._LoginEmployeeunkid = 0
                objSkillExpertise._ClientIP = getIP()
                objSkillExpertise._HostName = getHostName()
                objSkillExpertise._FromWeb = False
                objSkillExpertise._AuditUserId = User._Object._Userunkid
objSkillExpertise._CompanyUnkid = Company._Object._Companyunkid
                objSkillExpertise._AuditDate = ConfigParameter._Object._CurrentDateAndTime
                'S.SANDEEP [28-May-2018] -- END

                If objSkillExpertise.InsertDefaultSkillExpertise() = True Then
                    Call FillList()
                    Call FillCombo()
                End If
            End If

            If lvSkillExpertise.Items.Count > 0 Then lvSkillExpertise.Items(0).Selected = True
            lvSkillExpertise.Select()

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmSkillExpertiseList_Load", mstrModuleName)
        End Try
    End Sub

    Private Sub frmSkillExpertiseList_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        objSkillExpertise = Nothing
    End Sub

    Private Sub frmSkillExpertiseList_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles Me.KeyPress
        If Asc(e.KeyChar) = 27 Then
            Me.Close()
        End If
    End Sub

    Private Sub frmSkillExpertiseList_KeyUp(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyUp
        If e.KeyCode = Keys.Delete And lvSkillExpertise.Focused = True Then
            Call btnDelete.PerformClick()
        End If
    End Sub

    Private Sub frmSkillExpertiseList_LanguageClick(ByVal sender As Object, ByVal eventArgs As System.EventArgs) Handles Me.LanguageClick
        Try
            Dim objfrm As New frmLanguage

            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsSkillExpertise_master.SetMessages()
            objfrm._Other_ModuleNames = "clsSkillExpertise_master"
            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmSkillExpertiseList_LanguageClick", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Private Methods "

    Private Sub FillList()

        Dim strSearching As String = ""
        Dim dsList As New DataSet
        Dim dtTable As DataTable

        Try
            If User._Object.Privilege._AllowToViewSkillMasterList = True Then

                objSkillExpertise = New clsSkillExpertise_master

                dsList = objSkillExpertise.GetList("List")

                If CInt(cboSkillExpertise.SelectedValue) > 0 Then
                    strSearching &= "AND skillexpertiseunkid =" & CInt(cboSkillExpertise.SelectedValue) & " "
                End If

                If strSearching.Length > 0 Then
                    strSearching = strSearching.Substring(3)
                    dtTable = New DataView(dsList.Tables("List"), strSearching, "", DataViewRowState.CurrentRows).ToTable
                Else
                    dtTable = dsList.Tables(0)
                End If

                Dim lvItem As ListViewItem

                lvSkillExpertise.Items.Clear()

                For Each drRow As DataRow In dtTable.Rows
                    lvItem = New ListViewItem
                    lvItem.Text = drRow("code").ToString
                    lvItem.SubItems.Add(drRow("name").ToString)
                    lvItem.Tag = drRow("skillexpertiseunkid")

                    lvSkillExpertise.Items.Add(lvItem)
                Next

            End If

        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "fillList", mstrModuleName)
        Finally
            dsList.Dispose()
        End Try
    End Sub

    Private Sub FillCombo()
        Try
            Dim dsList As New DataSet
            objSkillExpertise = New clsSkillExpertise_master

            dsList = objSkillExpertise.getListForCombo("List", True)
            With cboSkillExpertise
                .ValueMember = "skillexpertiseunkid"
                .DisplayMember = "name"
                .DataSource = dsList.Tables("List")

            End With

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        Finally

        End Try
    End Sub

    Private Sub SetVisibility()
        Try
            btnNew.Enabled = User._Object.Privilege._AllowToAddSkillExpertise
            btnEdit.Enabled = User._Object.Privilege._AllowToEditSkillExpertise
            btnDelete.Enabled = User._Object.Privilege._AllowToDeleteSkillExpertise

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetVisibility", mstrModuleName)
        End Try
    End Sub
#End Region

#Region " Button's Events "

    Private Sub btnNew_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNew.Click
        Try
            Dim frm As New frmSkillExpertiseAddEdit

            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If

            If frm.displayDialog(-1, enAction.ADD_CONTINUE) Then
                Call FillList()
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnNew_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnEdit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEdit.Click
        Dim frm As New frmSkillExpertiseAddEdit
        Try
            If User._Object.Privilege._AllowToEditSkillExpertise = False Then Exit Sub

            If lvSkillExpertise.SelectedItems.Count < 1 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please select skill expertise from the list to perform further operation."), enMsgBoxStyle.Information)
                lvSkillExpertise.Select()
                Exit Sub
            End If

            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If

            Dim intSelectedIndex As Integer
            intSelectedIndex = lvSkillExpertise.SelectedItems(0).Index

            If frm.displayDialog(CInt(lvSkillExpertise.SelectedItems(0).Tag), enAction.EDIT_ONE) Then
                Call FillList()
            End If
            frm = Nothing

            lvSkillExpertise.Items(intSelectedIndex).Selected = True
            lvSkillExpertise.EnsureVisible(intSelectedIndex)
            lvSkillExpertise.Select()

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnEdit_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub btnDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDelete.Click
        objSkillExpertise = New clsSkillExpertise_master
        Try
            If lvSkillExpertise.SelectedItems.Count < 1 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please select skill expertise from the list to perform further operation."), enMsgBoxStyle.Information) '?1
                lvSkillExpertise.Select()
                Exit Sub
            End If
            If objSkillExpertise.isUsed(CInt(lvSkillExpertise.SelectedItems(0).Tag)) Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Sorry, You cannot delete this skill expertise. Reason: This skill expertise is in use."), enMsgBoxStyle.Information) '?2
                lvSkillExpertise.Select()
                Exit Sub
            End If

            If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Are you sure you want to delete this Skill Expertise?"), CType(enMsgBoxStyle.Question + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.Yes Then

                objSkillExpertise._Skillexpertiseunkid = CInt(lvSkillExpertise.SelectedItems(0).Tag)
                objSkillExpertise._Userunkid = User._Object._Userunkid


                'S.SANDEEP [28-May-2018] -- START
                'ISSUE/ENHANCEMENT : {Audit Trails} 
                objSkillExpertise._FormName = mstrModuleName
                objSkillExpertise._LoginEmployeeunkid = 0
                objSkillExpertise._ClientIP = getIP()
                objSkillExpertise._HostName = getHostName()
                objSkillExpertise._FromWeb = False
                objSkillExpertise._AuditUserId = User._Object._Userunkid
objSkillExpertise._CompanyUnkid = Company._Object._Companyunkid
                objSkillExpertise._AuditDate = ConfigParameter._Object._CurrentDateAndTime
                'S.SANDEEP [28-May-2018] -- END

                objSkillExpertise.Delete(CInt(lvSkillExpertise.SelectedItems(0).Tag))

                lvSkillExpertise.SelectedItems(0).Remove()
                If lvSkillExpertise.Items.Count <= 0 Then
                    Exit Try
                End If
            End If
            lvSkillExpertise.Select()

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnDelete_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnClose_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnSearchSkillExpertise_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchSkillExpertise.Click
        Dim objfrm As New frmCommonSearch
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            With objfrm
                .ValueMember = cboSkillExpertise.ValueMember
                .DisplayMember = cboSkillExpertise.DisplayMember
                .DataSource = CType(cboSkillExpertise.DataSource, DataTable)
                .CodeMember = "code"
            End With

            If objfrm.DisplayDialog Then
                cboSkillExpertise.SelectedValue = objfrm.SelectedValue
                cboSkillExpertise.Focus()
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchSkillExpertise_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnSearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearch.Click
        Try
            Call FillList()

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearch_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnReset.Click
        Try
            cboSkillExpertise.SelectedValue = 0
            Call FillList()

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnReset_Click", mstrModuleName)
        End Try
    End Sub

#End Region

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Private Sub OtherSettings()
        Try
            Me.SuspendLayout()
           
            Call SetLanguage()

            Me.gbFilterCriteria.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbFilterCriteria.ForeColor = GUI._eZeeContainerHeaderForeColor

            Me.eZeeHeader.GradientColor1 = GUI._HeaderBackColor1
            Me.eZeeHeader.GradientColor2 = GUI._HeaderBackColor2
            Me.eZeeHeader.BorderColor = GUI._HeaderBorderColor
            Me.eZeeHeader.HeaderTextForeColor = GUI._HeaderTitleFontColor
            Me.eZeeHeader.DescriptionForeColor = GUI._HeaderMessageFontColor


            Me.btnDelete.GradientBackColor = GUI._ButttonBackColor
            Me.btnDelete.GradientForeColor = GUI._ButttonFontColor

            Me.btnEdit.GradientBackColor = GUI._ButttonBackColor
            Me.btnEdit.GradientForeColor = GUI._ButttonFontColor

            Me.btnNew.GradientBackColor = GUI._ButttonBackColor
            Me.btnNew.GradientForeColor = GUI._ButttonFontColor

            Me.btnClose.GradientBackColor = GUI._ButttonBackColor
            Me.btnClose.GradientForeColor = GUI._ButttonFontColor


            Me.ResumeLayout()
        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "OtherSettings", mstrModuleName)
        End Try
    End Sub


    Private Sub SetLanguage()
        Try
            Me.Text = Language._Object.getCaption(Me.Name, Me.Text)

            Me.eZeeHeader.Title = Language._Object.getCaption(Me.eZeeHeader.Name & "_Title", Me.eZeeHeader.Title)
            Me.eZeeHeader.Message = Language._Object.getCaption(Me.eZeeHeader.Name & "_Message", Me.eZeeHeader.Message)
            Me.btnDelete.Text = Language._Object.getCaption(Me.btnDelete.Name, Me.btnDelete.Text)
            Me.btnEdit.Text = Language._Object.getCaption(Me.btnEdit.Name, Me.btnEdit.Text)
            Me.btnNew.Text = Language._Object.getCaption(Me.btnNew.Name, Me.btnNew.Text)
            Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
            Me.gbFilterCriteria.Text = Language._Object.getCaption(Me.gbFilterCriteria.Name, Me.gbFilterCriteria.Text)
            Me.lblSkillExpertise.Text = Language._Object.getCaption(Me.lblSkillExpertise.Name, Me.lblSkillExpertise.Text)
            Me.colhCode.Text = Language._Object.getCaption(CStr(Me.colhCode.Tag), Me.colhCode.Text)
            Me.colhSkillExpertiseName.Text = Language._Object.getCaption(CStr(Me.colhSkillExpertiseName.Tag), Me.colhSkillExpertiseName.Text)

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
        End Try
    End Sub


    Private Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Please select skill expertise from the list to perform further operation.")
            Language.setMessage(mstrModuleName, 2, "Sorry, You cannot delete this skill expertise. Reason: This skill expertise is in use.")
            Language.setMessage(mstrModuleName, 3, "Are you sure you want to delete this Skill Expertise?")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class