﻿Imports eZeeCommonLib


Module modGlobal
    Public gfrmMDI As frmNewMDI
    'Sandeep [ 07 APRIL 2011 ] -- Start
    Public mblnIsSendingPayslip As Boolean = False
    Public mstrExportEPaySlipPath As String = ""
    Public mblnIsSendingMail As Boolean = False
    'S.SANDEEP [ 28 MARCH 2012 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Public mblnIsSendDisposed As Boolean = False
    'S.SANDEEP [ 28 MARCH 2012 ] -- END

    'Sandeep [ 07 APRIL 2011 ] -- End 


    Public Enum enGroupType
        CostCenter = 1
        Vendor = 2
        BankGroup = 3
    End Enum

    'Sohail (25 Jan 2011) -- Start
    'Public Enum enAccountGroup
    '    AccountsPayable = 1
    '    AccountsReceivable = 2
    '    Bank = 3
    '    CostOfGoodsSold = 4
    '    CreditCard = 5
    '    Equity = 6
    '    Expense = 7
    '    FixedAsset = 8
    '    Income = 9
    '    LongTermLiability = 10
    '    NonPosting = 11
    '    OtherAsset = 12
    '    OtherCurrentAsset = 13
    '    OtherCurrentLiability = 14
    '    OtherExpense = 15
    '    OtherIncome = 16
    'End Enum
    'Sohail (25 Jan 2011) -- End

    Public Enum enSelectionMode
        Select_Mode = 1
        NA_Mode = 2
    End Enum



    Public Enum enStatusType
        Open = 1
        Close = 2
    End Enum

    'Public Enum enPaymentRefId
    '    LOAN = 1
    '    ADVANCE = 2
    '    PAYSLIP = 3
    '    SAVINGS = 4
    'End Enum

    'Public Enum enPayTypeId
    '    PAYMENT = 1
    '    RECEIVED = 2
    '    WITHDRAWAL = 3
    '    REDEMPTION = 4
    '    REPAYMENT = 5
    'End Enum


    'Pinkal (12-Oct-2011) -- Start
    'Enhancement : TRA Changes

    'Public Enum enMedicalMasterType
    '    'Category = 1
    '    Medical_Category = 1
    '    Cover = 2
    '    Treatment = 3
    '    Service = 4
    'End Enum

    'Pinkal (12-Oct-2011) -- End


    ''FOR GET TIME
    'Public Function CalculateTime(ByVal IsHours As Boolean, ByVal intSecond As Integer) As Double
    '    Dim calctime As Double = 0
    '    Dim calcMinute As Double = 0
    '    Dim calMinute As Double = 0
    '    Try
    '        If IsHours Then
    '            calcMinute = intSecond / 60
    '            calMinute = calcMinute Mod 60
    '            Dim calHour As Double = calcMinute / 60
    '            Return CDec(CDec(Int(calHour) + (calMinute / 100)))
    '        Else
    '            calctime = CDec(intSecond / 60)
    '            Return Math.Round(calctime)
    '        End If
    '    Catch ex As Exception
    '        DisplayError.Show("-1", ex.Message, "CalculateTime", "modglobal")
    '    End Try
    '    Return calctime
    'End Function

    'Sohail (25 Jan 2010) -- Start
    'Public Enum enDebitCredit
    '    DEBIT = 1
    '    CREDIT = 2
    'End Enum
    'Sohail (25 Jan 2010) -- End

    'Sohail (25 Jan 2010) -- Start
    'Public Enum enJVTransactionType
    '    TRANSACTION_HEAD = 1
    '    LOAN = 2
    '    ADVANCE = 3
    '    SAVINGS = 4
    'End Enum
    'Sohail (25 Jan 2010) -- End
    'Sohail (25 Jan 2010) -- Start
    'Public Enum enFormula_LeaveType
    '    PAID_ACCRUE_LEAVE = 1
    '    PAID_ISSUE_LEAVE = 2
    '    UNPAID_ISSUE_LEAVE = 3
    'End Enum
    'Sohail (25 Jan 2010) -- End
    'Sohail (16 Oct 2010) -- Start
    'Sohail (15 Dec 2010) -- Start
    'Public Enum enPaymentMode
    '    CASH = 1
    '    CHEQUE = 2
    '    TRANSFER = 3
    'End Enum
    'Sohail (15 Dec 2010) -- End
    'Sohail (16 Oct 2010) -- End

    'Sohail (11 May 2011) -- Start
    '*** Key Press event for Datagridview Textbox column to allow only Numeric
    Public Sub tb_keypress(ByVal sender As Object, ByVal e As KeyPressEventArgs)
        Dim isBackSpace As Boolean = False
        Try

            Dim tb As TextBox
            tb = CType(sender, TextBox)

            Select Case Asc(e.KeyChar)
                Case 48 To 59 'These are 0 to 9.
                    Dim d As Decimal
                    'Sohail (12 Oct 2011) -- Start
                    'd = Convert.ToDecimal(IIf(tb.Text = "", 0, tb.Text))
                    Decimal.TryParse(IIf(tb.Text = "", 0, tb.Text), d)
                    'Sohail (12 Oct 2011) -- End

                    If tb.SelectionLength = tb.Text.Length Then 'If 0 to 9 pressed on selected text, 
                        'Do nothing

                    ElseIf InStr(tb.Text, ".") = 0 AndAlso d.ToString.Length = 20 Then 'Allow only 20 digit before entering "."
                        e.Handled = True
                        Exit Sub
                    ElseIf InStr(tb.Text.Trim, ".") > 0 AndAlso tb.Text.Substring(InStr(tb.Text.Trim, ".") - 1).Trim.Length > 6 Then 'Allow only 6 Decimal
                        e.Handled = True
                        Exit Sub
                    ElseIf InStr(tb.Text.Trim, ".") > 0 AndAlso tb.Text.Substring(0, InStr(tb.Text.Trim, ".") - 1).Trim.Length >= 20 AndAlso tb.SelectionStart <= 20 Then 'Allow only 20 digit before Decimal after entering "."
                        e.Handled = True
                        Exit Sub
                    ElseIf InStr(tb.Text, "-") <> 0 AndAlso tb.SelectionStart = 0 Then 'Do not allow digit before - sign  e.g.  4-1234
                        e.Handled = True
                        Exit Sub
                    End If
                Case Keys.Back 'BACKSPACE
                    'This is fine. Do nothing

                Case Keys.Insert ' -  sign to allow negative
                    If tb.SelectionLength = tb.Text.Length Then 'If - pressed on selected text, 
                        'Do nothing

                    ElseIf InStr(tb.Text, "-") <> 0 Then 'Number can only have 1 minus sign
                        'Sohail (12 Oct 2011) -- Start
                        Dim i As Integer = tb.SelectionStart
                        tb.Text = tb.Text.Replace("-", "")
                        tb.SelectionStart = i
                        'Sohail (12 Oct 2011) -- End
                        e.Handled = True
                        Exit Sub
                    End If

                    'if the insertion point is not sitting at zero, put minus at zero position
                    If tb.SelectionStart <> 0 Then
                        Dim i As Integer = tb.SelectionStart
                        tb.Text = e.KeyChar & tb.Text
                        tb.SelectionStart = i + 1
                        e.Handled = True
                        Exit Sub
                    End If
                Case Keys.Delete 'Period/Decimal "."
                    If tb.SelectionLength = tb.Text.Length Then 'If . pressed on selected text, 
                        'Do nothing

                    ElseIf InStr(tb.Text, ".") <> 0 Then 'number can have only 1 period
                        e.Handled = True
                        Exit Sub
                    End If

                Case Else
                    e.Handled = True
                    Exit Sub
            End Select

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "tb_keypress", "modglobal")
        End Try
    End Sub
    'Sohail (11 May 2011) -- End

    'S.SANDEEP [ 07 NOV 2011 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Public Function Export_ErrorList(ByVal StrPath As String, ByVal objDataReader As DataTable, ByVal strSourceName As String) As Boolean
        Dim strBuilder As New System.Text.StringBuilder
        Dim blnFlag As Boolean = False
        Try
            'S.SANDEEP [16-May-2018] -- START
            'ISSUE/ENHANCEMENT : {#0002255|#ARUTI-167}
            Dim dsError As New DataSet()
            dsError.Tables.Add(objDataReader.Copy())
            If Aruti.Data.modGlobal.OpenXML_Export(strFilePath:=StrPath, ds:=dsError) = 0 Then
                blnFlag = True
            End If

            'strBuilder.Append(" <HTML> " & vbCrLf)
            'strBuilder.Append(" <TITLE>" & strSourceName & "</TITLE> " & vbCrLf)
            'strBuilder.Append(" <BODY><FONT FACE =VERDANA FONT SIZE=2> " & vbCrLf)
            'strBuilder.Append(" <TABLE BORDER=1 BORDERCOLOR =BLACK CELLSPACING =0 CELLPADDING =3 WIDTH=100%> " & vbCrLf)
            'strBuilder.Append(" <TR ALIGN = CENTER VALIGN=TOP> " & vbCrLf)

            'For j As Integer = 0 To objDataReader.Columns.Count - 1

            '    'S.SANDEEP [ 13 FEB 2013 ] -- START
            '    'ENHANCEMENT : TRA CHANGES
            '    'strBuilder.Append("<TD BORDER=1 WIDTH='60%' ALIGN='MIDDLE'><FONT SIZE=2><B>" & objDataReader.Columns(j).ColumnName & "</B></FONT></TD>" & vbCrLf)
            '    Dim StrColName As String = ""
            '    StrColName = objDataReader.Columns(j).ColumnName
            '    If StrColName.Contains("_") = True Then StrColName = StrColName.Replace("_", "  ")
            '    StrColName = StrConv(StrColName, VbStrConv.ProperCase)
            '    strBuilder.Append("<TD BORDER=1 WIDTH='60%' ALIGN='MIDDLE'><FONT SIZE=2><B>" & StrColName & "</B></FONT></TD>" & vbCrLf)
            '    'S.SANDEEP [ 13 FEB 2013 ] -- END
            'Next
            'strBuilder.Append(" </TR> " & vbCrLf)

            'For i As Integer = 0 To objDataReader.Rows.Count - 1
            '    strBuilder.Append(" <TR> " & vbCrLf)
            '    For k As Integer = 0 To objDataReader.Columns.Count - 1
            '        strBuilder.Append("<TD BORDER=1 BORDERCOLOR=BLACK ALIGN='LEFT'><FONT SIZE=2>&nbsp;" & objDataReader.Rows(i)(k) & "</FONT></TD>" & vbCrLf)
            '    Next
            '    strBuilder.Append(" </TR> " & vbCrLf)
            'Next
            'strBuilder.Append(" </TR>  " & vbCrLf)
            'strBuilder.Append(" </TABLE> " & vbCrLf)
            'strBuilder.Append(" </BODY> " & vbCrLf)
            'strBuilder.Append(" </HTML> " & vbCrLf)


            'If Savefile(StrPath, strBuilder) Then
            '    blnFlag = True
            'Else
            '    blnFlag = False
            'End If
            'S.SANDEEP [16-May-2018] -- END

            Return blnFlag

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Export_ErrorList", "modGlobal")
        Finally
        End Try
    End Function

    Private Function Savefile(ByVal fpath As String, ByVal sb As System.Text.StringBuilder) As Boolean
        Dim fsFile As New IO.FileStream(fpath, IO.FileMode.Create, IO.FileAccess.Write)
        Dim strWriter As New IO.StreamWriter(fsFile)
        Try
            With strWriter
                .BaseStream.Seek(0, IO.SeekOrigin.End)
                .WriteLine(sb)
                .Close()
            End With
            Return True
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SaveExcelfile", "modGlobal")
            Return False
        Finally
            sb = Nothing
            strWriter = Nothing
            fsFile = Nothing
        End Try
    End Function

    'S.SANDEEP [ 07 NOV 2011 ] -- END
    
End Module

Public Enum enCardReaderType
    Serial = 1
    Keyboard = 2
    ScanShell900 = 3
End Enum

'Gajanan [09-SEP-2019] -- Start      
'Enhancements: ENHANCEMENTS FOR NMB
'Move this block to applogic/module/modglobal.vb
'Reason:Create Delete Reason Control So Need This Enum
'Sandeep [ 16 Oct 2010 ] -- Start
'Public Enum enVoidCategoryType
'    EMPLOYEE = 1
'    DISCIPLINE = 2
'    ASSESSMENT = 3
'    TRAINING = 4
'    PAYROLL = 5
'    MEDICAL = 6
'    LEAVE = 7
'    TNA = 8
'    LOAN = 9
'    SAVINGS = 10
'    APPLICANT = 11
'    INTERVIEW = 12
'    VACANCY = 13
'    OTHERS = 14
'    PAYMENT_DISAPPROVAL = 15 'Sohail (13 Feb 2013)
'    STAFF_REQUISITION = 16 'Sohail (28 May 2014)
'End Enum
'Sandeep [ 16 Oct 2010 ] -- End 
'Gajanan [09-SEP-2019] -- End 