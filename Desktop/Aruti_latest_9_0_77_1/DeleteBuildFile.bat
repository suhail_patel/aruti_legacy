@echo off
setlocal

set servers=acore32,appLogix,ArutiAutoBackup,ArutiAutoRecruit,ArutiReport.Desiner,ArutiReports,ArutiTimeSheet,eZeeArutiConfiguration,eZeeReport.Engine.Library,""eZee Aruti""

call :parse "%servers%"
pause

goto :eos

:parse
set list=%1
set list=%list:"=%

FOR /f "tokens=1* delims=," %%a IN ("%list%") DO (
  if not "%%a" == "" ( call :DEL_FOLDER "%~dp0%%a\obj"
						call :DEL_FOLDER "%~dp0%%a\bin" )
  if not "%%b" == "" call :parse "%%b"
)

goto :eos

:DEL_FOLDER

IF EXIST %1 ( 
RD /S /Q %1
echo %1 -- DONE 
) ELSE ( 
echo %1 -- NOT FOUND
)
	

goto :eos

:eos
endlocal
