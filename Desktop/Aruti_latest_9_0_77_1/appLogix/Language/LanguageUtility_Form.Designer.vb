<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmLanguageUtility
    Inherits eZee.Common.eZeeForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmLanguageUtility))
        Me.tabcLanguageUtility = New System.Windows.Forms.TabControl
        Me.tabpExport = New System.Windows.Forms.TabPage
        Me.gbExport = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.objbtnOpenDirectory = New eZee.Common.eZeeLightButton(Me.components)
        Me.lblExportFileName = New System.Windows.Forms.Label
        Me.txtExportFile = New eZee.TextBox.AlphanumericTextBox
        Me.lblExportPath = New System.Windows.Forms.Label
        Me.txtExportPath = New eZee.TextBox.AlphanumericTextBox
        Me.tabpImport = New System.Windows.Forms.TabPage
        Me.gbImport = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.objbtnOpenFile = New eZee.Common.eZeeLightButton(Me.components)
        Me.txtImportFileName = New eZee.TextBox.AlphanumericTextBox
        Me.lblImportFilePath = New System.Windows.Forms.Label
        Me.tabpImportLookUp = New System.Windows.Forms.TabPage
        Me.gbImportLookup = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.btnLookup = New eZee.Common.eZeeLightButton(Me.components)
        Me.txtLookupFile = New eZee.TextBox.AlphanumericTextBox
        Me.lblLookupFile = New System.Windows.Forms.Label
        Me.fdbPath = New System.Windows.Forms.FolderBrowserDialog
        Me.ofdFile = New System.Windows.Forms.OpenFileDialog
        Me.pnlMain = New System.Windows.Forms.Panel
        Me.eZeeHeader = New eZee.Common.eZeeHeader
        Me.objefFormFooter = New eZee.Common.eZeeFooter
        Me.btnClose = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnImport = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnExport = New eZee.Common.eZeeLightButton(Me.components)
        Me.tabcLanguageUtility.SuspendLayout()
        Me.tabpExport.SuspendLayout()
        Me.gbExport.SuspendLayout()
        Me.tabpImport.SuspendLayout()
        Me.gbImport.SuspendLayout()
        Me.tabpImportLookUp.SuspendLayout()
        Me.gbImportLookup.SuspendLayout()
        Me.pnlMain.SuspendLayout()
        Me.objefFormFooter.SuspendLayout()
        Me.SuspendLayout()
        '
        'tabcLanguageUtility
        '
        Me.tabcLanguageUtility.Controls.Add(Me.tabpExport)
        Me.tabcLanguageUtility.Controls.Add(Me.tabpImport)
        Me.tabcLanguageUtility.Controls.Add(Me.tabpImportLookUp)
        Me.tabcLanguageUtility.Location = New System.Drawing.Point(8, 66)
        Me.tabcLanguageUtility.Name = "tabcLanguageUtility"
        Me.tabcLanguageUtility.SelectedIndex = 0
        Me.tabcLanguageUtility.Size = New System.Drawing.Size(524, 147)
        Me.tabcLanguageUtility.TabIndex = 1
        '
        'tabpExport
        '
        Me.tabpExport.Controls.Add(Me.gbExport)
        Me.tabpExport.Location = New System.Drawing.Point(4, 22)
        Me.tabpExport.Name = "tabpExport"
        Me.tabpExport.Padding = New System.Windows.Forms.Padding(3)
        Me.tabpExport.Size = New System.Drawing.Size(516, 121)
        Me.tabpExport.TabIndex = 0
        Me.tabpExport.Text = "Export"
        Me.tabpExport.UseVisualStyleBackColor = True
        '
        'gbExport
        '
        Me.gbExport.BorderColor = System.Drawing.Color.Black
        Me.gbExport.Checked = False
        Me.gbExport.CollapseAllExceptThis = False
        Me.gbExport.CollapsedHoverImage = Nothing
        Me.gbExport.CollapsedNormalImage = Nothing
        Me.gbExport.CollapsedPressedImage = Nothing
        Me.gbExport.CollapseOnLoad = False
        Me.gbExport.Controls.Add(Me.objbtnOpenDirectory)
        Me.gbExport.Controls.Add(Me.lblExportFileName)
        Me.gbExport.Controls.Add(Me.txtExportFile)
        Me.gbExport.Controls.Add(Me.lblExportPath)
        Me.gbExport.Controls.Add(Me.txtExportPath)
        Me.gbExport.ExpandedHoverImage = Nothing
        Me.gbExport.ExpandedNormalImage = Nothing
        Me.gbExport.ExpandedPressedImage = Nothing
        Me.gbExport.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbExport.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbExport.HeaderHeight = 25
        Me.gbExport.HeightOnCollapse = 0
        Me.gbExport.LeftTextSpace = 0
        Me.gbExport.Location = New System.Drawing.Point(3, 6)
        Me.gbExport.Name = "gbExport"
        Me.gbExport.OpenHeight = 110
        Me.gbExport.Padding = New System.Windows.Forms.Padding(3)
        Me.gbExport.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbExport.ShowBorder = True
        Me.gbExport.ShowCheckBox = False
        Me.gbExport.ShowCollapseButton = False
        Me.gbExport.ShowDefaultBorderColor = True
        Me.gbExport.ShowDownButton = False
        Me.gbExport.ShowHeader = True
        Me.gbExport.Size = New System.Drawing.Size(507, 110)
        Me.gbExport.TabIndex = 0
        Me.gbExport.Temp = 0
        Me.gbExport.Text = "Select file to export language"
        Me.gbExport.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objbtnOpenDirectory
        '
        Me.objbtnOpenDirectory.BackColor = System.Drawing.Color.White
        Me.objbtnOpenDirectory.BackgroundImage = CType(resources.GetObject("objbtnOpenDirectory.BackgroundImage"), System.Drawing.Image)
        Me.objbtnOpenDirectory.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.objbtnOpenDirectory.BorderColor = System.Drawing.Color.Empty
        Me.objbtnOpenDirectory.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.objbtnOpenDirectory.FlatAppearance.BorderSize = 0
        Me.objbtnOpenDirectory.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.objbtnOpenDirectory.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objbtnOpenDirectory.ForeColor = System.Drawing.Color.Black
        Me.objbtnOpenDirectory.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.objbtnOpenDirectory.GradientForeColor = System.Drawing.Color.Black
        Me.objbtnOpenDirectory.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.objbtnOpenDirectory.HoverGradientForeColor = System.Drawing.Color.Black
        Me.objbtnOpenDirectory.Location = New System.Drawing.Point(474, 43)
        Me.objbtnOpenDirectory.Name = "objbtnOpenDirectory"
        Me.objbtnOpenDirectory.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.objbtnOpenDirectory.PressedGradientForeColor = System.Drawing.Color.Black
        Me.objbtnOpenDirectory.Selected = False
        Me.objbtnOpenDirectory.ShowDefaultBorderColor = True
        Me.objbtnOpenDirectory.Size = New System.Drawing.Size(24, 21)
        Me.objbtnOpenDirectory.TabIndex = 2
        Me.objbtnOpenDirectory.Text = "..."
        Me.objbtnOpenDirectory.UseVisualStyleBackColor = False
        '
        'lblExportFileName
        '
        Me.lblExportFileName.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblExportFileName.Location = New System.Drawing.Point(8, 74)
        Me.lblExportFileName.Name = "lblExportFileName"
        Me.lblExportFileName.Size = New System.Drawing.Size(57, 13)
        Me.lblExportFileName.TabIndex = 3
        Me.lblExportFileName.Text = "File Name"
        Me.lblExportFileName.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtExportFile
        '
        Me.txtExportFile.BackColor = System.Drawing.Color.White
        Me.txtExportFile.Flags = 0
        Me.txtExportFile.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtExportFile.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(46)}
        Me.txtExportFile.Location = New System.Drawing.Point(71, 70)
        Me.txtExportFile.Name = "txtExportFile"
        Me.txtExportFile.Size = New System.Drawing.Size(397, 21)
        Me.txtExportFile.TabIndex = 4
        '
        'lblExportPath
        '
        Me.lblExportPath.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblExportPath.Location = New System.Drawing.Point(8, 47)
        Me.lblExportPath.Name = "lblExportPath"
        Me.lblExportPath.Size = New System.Drawing.Size(57, 13)
        Me.lblExportPath.TabIndex = 0
        Me.lblExportPath.Text = "File Path"
        Me.lblExportPath.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtExportPath
        '
        Me.txtExportPath.BackColor = System.Drawing.Color.White
        Me.txtExportPath.Flags = 0
        Me.txtExportPath.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtExportPath.InvalidChars = New Char(-1) {}
        Me.txtExportPath.Location = New System.Drawing.Point(71, 43)
        Me.txtExportPath.Name = "txtExportPath"
        Me.txtExportPath.Size = New System.Drawing.Size(397, 21)
        Me.txtExportPath.TabIndex = 1
        '
        'tabpImport
        '
        Me.tabpImport.Controls.Add(Me.gbImport)
        Me.tabpImport.Location = New System.Drawing.Point(4, 22)
        Me.tabpImport.Name = "tabpImport"
        Me.tabpImport.Padding = New System.Windows.Forms.Padding(3)
        Me.tabpImport.Size = New System.Drawing.Size(516, 121)
        Me.tabpImport.TabIndex = 1
        Me.tabpImport.Text = "Import"
        Me.tabpImport.UseVisualStyleBackColor = True
        '
        'gbImport
        '
        Me.gbImport.BorderColor = System.Drawing.Color.Black
        Me.gbImport.Checked = False
        Me.gbImport.CollapseAllExceptThis = False
        Me.gbImport.CollapsedHoverImage = Nothing
        Me.gbImport.CollapsedNormalImage = Nothing
        Me.gbImport.CollapsedPressedImage = Nothing
        Me.gbImport.CollapseOnLoad = False
        Me.gbImport.Controls.Add(Me.objbtnOpenFile)
        Me.gbImport.Controls.Add(Me.txtImportFileName)
        Me.gbImport.Controls.Add(Me.lblImportFilePath)
        Me.gbImport.ExpandedHoverImage = Nothing
        Me.gbImport.ExpandedNormalImage = Nothing
        Me.gbImport.ExpandedPressedImage = Nothing
        Me.gbImport.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbImport.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbImport.HeaderHeight = 25
        Me.gbImport.HeightOnCollapse = 0
        Me.gbImport.LeftTextSpace = 0
        Me.gbImport.Location = New System.Drawing.Point(3, 6)
        Me.gbImport.Name = "gbImport"
        Me.gbImport.OpenHeight = 100
        Me.gbImport.Padding = New System.Windows.Forms.Padding(3)
        Me.gbImport.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbImport.ShowBorder = True
        Me.gbImport.ShowCheckBox = False
        Me.gbImport.ShowCollapseButton = False
        Me.gbImport.ShowDefaultBorderColor = True
        Me.gbImport.ShowDownButton = False
        Me.gbImport.ShowHeader = True
        Me.gbImport.Size = New System.Drawing.Size(507, 110)
        Me.gbImport.TabIndex = 0
        Me.gbImport.Temp = 0
        Me.gbImport.Text = "Select file to import language"
        Me.gbImport.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objbtnOpenFile
        '
        Me.objbtnOpenFile.BackColor = System.Drawing.Color.White
        Me.objbtnOpenFile.BackgroundImage = CType(resources.GetObject("objbtnOpenFile.BackgroundImage"), System.Drawing.Image)
        Me.objbtnOpenFile.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.objbtnOpenFile.BorderColor = System.Drawing.Color.Empty
        Me.objbtnOpenFile.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.objbtnOpenFile.FlatAppearance.BorderSize = 0
        Me.objbtnOpenFile.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.objbtnOpenFile.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objbtnOpenFile.ForeColor = System.Drawing.Color.Black
        Me.objbtnOpenFile.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.objbtnOpenFile.GradientForeColor = System.Drawing.Color.Black
        Me.objbtnOpenFile.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.objbtnOpenFile.HoverGradientForeColor = System.Drawing.Color.Black
        Me.objbtnOpenFile.Location = New System.Drawing.Point(474, 43)
        Me.objbtnOpenFile.Name = "objbtnOpenFile"
        Me.objbtnOpenFile.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.objbtnOpenFile.PressedGradientForeColor = System.Drawing.Color.Black
        Me.objbtnOpenFile.Selected = False
        Me.objbtnOpenFile.ShowDefaultBorderColor = True
        Me.objbtnOpenFile.Size = New System.Drawing.Size(24, 21)
        Me.objbtnOpenFile.TabIndex = 2
        Me.objbtnOpenFile.Text = "..."
        Me.objbtnOpenFile.UseVisualStyleBackColor = False
        '
        'txtImportFileName
        '
        Me.txtImportFileName.BackColor = System.Drawing.Color.White
        Me.txtImportFileName.Flags = 0
        Me.txtImportFileName.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtImportFileName.InvalidChars = New Char(-1) {}
        Me.txtImportFileName.Location = New System.Drawing.Point(71, 43)
        Me.txtImportFileName.Name = "txtImportFileName"
        Me.txtImportFileName.Size = New System.Drawing.Size(397, 21)
        Me.txtImportFileName.TabIndex = 1
        '
        'lblImportFilePath
        '
        Me.lblImportFilePath.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblImportFilePath.Location = New System.Drawing.Point(8, 47)
        Me.lblImportFilePath.Name = "lblImportFilePath"
        Me.lblImportFilePath.Size = New System.Drawing.Size(57, 13)
        Me.lblImportFilePath.TabIndex = 0
        Me.lblImportFilePath.Text = "File Path"
        '
        'tabpImportLookUp
        '
        Me.tabpImportLookUp.Controls.Add(Me.gbImportLookup)
        Me.tabpImportLookUp.Location = New System.Drawing.Point(4, 22)
        Me.tabpImportLookUp.Name = "tabpImportLookUp"
        Me.tabpImportLookUp.Padding = New System.Windows.Forms.Padding(3)
        Me.tabpImportLookUp.Size = New System.Drawing.Size(516, 121)
        Me.tabpImportLookUp.TabIndex = 2
        Me.tabpImportLookUp.Text = "Import LookUp"
        Me.tabpImportLookUp.UseVisualStyleBackColor = True
        '
        'gbImportLookup
        '
        Me.gbImportLookup.BorderColor = System.Drawing.Color.Black
        Me.gbImportLookup.Checked = False
        Me.gbImportLookup.CollapseAllExceptThis = False
        Me.gbImportLookup.CollapsedHoverImage = Nothing
        Me.gbImportLookup.CollapsedNormalImage = Nothing
        Me.gbImportLookup.CollapsedPressedImage = Nothing
        Me.gbImportLookup.CollapseOnLoad = False
        Me.gbImportLookup.Controls.Add(Me.btnLookup)
        Me.gbImportLookup.Controls.Add(Me.txtLookupFile)
        Me.gbImportLookup.Controls.Add(Me.lblLookupFile)
        Me.gbImportLookup.ExpandedHoverImage = Nothing
        Me.gbImportLookup.ExpandedNormalImage = Nothing
        Me.gbImportLookup.ExpandedPressedImage = Nothing
        Me.gbImportLookup.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbImportLookup.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbImportLookup.HeaderHeight = 25
        Me.gbImportLookup.HeightOnCollapse = 0
        Me.gbImportLookup.LeftTextSpace = 0
        Me.gbImportLookup.Location = New System.Drawing.Point(3, 6)
        Me.gbImportLookup.Name = "gbImportLookup"
        Me.gbImportLookup.OpenHeight = 110
        Me.gbImportLookup.Padding = New System.Windows.Forms.Padding(3)
        Me.gbImportLookup.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbImportLookup.ShowBorder = True
        Me.gbImportLookup.ShowCheckBox = False
        Me.gbImportLookup.ShowCollapseButton = False
        Me.gbImportLookup.ShowDefaultBorderColor = True
        Me.gbImportLookup.ShowDownButton = False
        Me.gbImportLookup.ShowHeader = True
        Me.gbImportLookup.Size = New System.Drawing.Size(507, 110)
        Me.gbImportLookup.TabIndex = 1
        Me.gbImportLookup.Temp = 0
        Me.gbImportLookup.Text = "Select file to import Lookup"
        Me.gbImportLookup.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'btnLookup
        '
        Me.btnLookup.BackColor = System.Drawing.Color.White
        Me.btnLookup.BackgroundImage = CType(resources.GetObject("btnLookup.BackgroundImage"), System.Drawing.Image)
        Me.btnLookup.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnLookup.BorderColor = System.Drawing.Color.Empty
        Me.btnLookup.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnLookup.FlatAppearance.BorderSize = 0
        Me.btnLookup.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnLookup.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnLookup.ForeColor = System.Drawing.Color.Black
        Me.btnLookup.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnLookup.GradientForeColor = System.Drawing.Color.Black
        Me.btnLookup.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnLookup.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnLookup.Location = New System.Drawing.Point(474, 43)
        Me.btnLookup.Name = "btnLookup"
        Me.btnLookup.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnLookup.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnLookup.Selected = False
        Me.btnLookup.ShowDefaultBorderColor = True
        Me.btnLookup.Size = New System.Drawing.Size(24, 21)
        Me.btnLookup.TabIndex = 2
        Me.btnLookup.Text = "..."
        Me.btnLookup.UseVisualStyleBackColor = False
        '
        'txtLookupFile
        '
        Me.txtLookupFile.BackColor = System.Drawing.Color.White
        Me.txtLookupFile.Flags = 0
        Me.txtLookupFile.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtLookupFile.InvalidChars = New Char(-1) {}
        Me.txtLookupFile.Location = New System.Drawing.Point(71, 43)
        Me.txtLookupFile.Name = "txtLookupFile"
        Me.txtLookupFile.Size = New System.Drawing.Size(397, 21)
        Me.txtLookupFile.TabIndex = 1
        '
        'lblLookupFile
        '
        Me.lblLookupFile.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblLookupFile.Location = New System.Drawing.Point(8, 47)
        Me.lblLookupFile.Name = "lblLookupFile"
        Me.lblLookupFile.Size = New System.Drawing.Size(57, 13)
        Me.lblLookupFile.TabIndex = 0
        Me.lblLookupFile.Text = "File Path"
        '
        'pnlMain
        '
        Me.pnlMain.Controls.Add(Me.eZeeHeader)
        Me.pnlMain.Controls.Add(Me.tabcLanguageUtility)
        Me.pnlMain.Controls.Add(Me.objefFormFooter)
        Me.pnlMain.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pnlMain.Location = New System.Drawing.Point(0, 0)
        Me.pnlMain.Name = "pnlMain"
        Me.pnlMain.Size = New System.Drawing.Size(538, 272)
        Me.pnlMain.TabIndex = 0
        '
        'eZeeHeader
        '
        Me.eZeeHeader.BackColor = System.Drawing.Color.White
        Me.eZeeHeader.BorderColor = System.Drawing.SystemColors.ControlDark
        Me.eZeeHeader.DescriptionForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.eZeeHeader.Dock = System.Windows.Forms.DockStyle.Top
        Me.eZeeHeader.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.eZeeHeader.GradiantStyle = eZee.Common.eZeeHeader.GradientStyle.Central
        Me.eZeeHeader.GradientColor1 = System.Drawing.SystemColors.Window
        Me.eZeeHeader.GradientColor2 = System.Drawing.SystemColors.Control
        Me.eZeeHeader.HeaderTextForeColor = System.Drawing.SystemColors.ControlText
        Me.eZeeHeader.Icon = Nothing
        Me.eZeeHeader.Location = New System.Drawing.Point(0, 0)
        Me.eZeeHeader.Message = "User "
        Me.eZeeHeader.Name = "eZeeHeader"
        Me.eZeeHeader.Size = New System.Drawing.Size(538, 60)
        Me.eZeeHeader.TabIndex = 0
        Me.eZeeHeader.Title = "Import/Export Utility"
        '
        'objefFormFooter
        '
        Me.objefFormFooter.BorderColor = System.Drawing.Color.Silver
        Me.objefFormFooter.Controls.Add(Me.btnClose)
        Me.objefFormFooter.Controls.Add(Me.btnImport)
        Me.objefFormFooter.Controls.Add(Me.btnExport)
        Me.objefFormFooter.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.objefFormFooter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objefFormFooter.GradiantStyle = eZee.Common.eZeeFooter.GradientStyle.Up
        Me.objefFormFooter.GradientColor1 = System.Drawing.Color.Transparent
        Me.objefFormFooter.GradientColor2 = System.Drawing.Color.Transparent
        Me.objefFormFooter.Location = New System.Drawing.Point(0, 217)
        Me.objefFormFooter.Name = "objefFormFooter"
        Me.objefFormFooter.Size = New System.Drawing.Size(538, 55)
        Me.objefFormFooter.TabIndex = 2
        '
        'btnClose
        '
        Me.btnClose.BackColor = System.Drawing.Color.White
        Me.btnClose.BackgroundImage = CType(resources.GetObject("btnClose.BackgroundImage"), System.Drawing.Image)
        Me.btnClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnClose.BorderColor = System.Drawing.Color.Empty
        Me.btnClose.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnClose.FlatAppearance.BorderSize = 0
        Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClose.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.ForeColor = System.Drawing.Color.Black
        Me.btnClose.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnClose.GradientForeColor = System.Drawing.Color.Black
        Me.btnClose.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Location = New System.Drawing.Point(439, 12)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Selected = False
        Me.btnClose.ShowDefaultBorderColor = True
        Me.btnClose.Size = New System.Drawing.Size(90, 30)
        Me.btnClose.TabIndex = 1
        Me.btnClose.Text = "Close"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'btnImport
        '
        Me.btnImport.BackColor = System.Drawing.Color.White
        Me.btnImport.BackgroundImage = CType(resources.GetObject("btnImport.BackgroundImage"), System.Drawing.Image)
        Me.btnImport.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnImport.BorderColor = System.Drawing.Color.Empty
        Me.btnImport.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnImport.FlatAppearance.BorderSize = 0
        Me.btnImport.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnImport.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnImport.ForeColor = System.Drawing.Color.Black
        Me.btnImport.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnImport.GradientForeColor = System.Drawing.Color.Black
        Me.btnImport.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnImport.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnImport.Location = New System.Drawing.Point(343, 12)
        Me.btnImport.Name = "btnImport"
        Me.btnImport.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnImport.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnImport.Selected = False
        Me.btnImport.ShowDefaultBorderColor = True
        Me.btnImport.Size = New System.Drawing.Size(90, 30)
        Me.btnImport.TabIndex = 0
        Me.btnImport.Text = "&Import"
        Me.btnImport.UseVisualStyleBackColor = False
        '
        'btnExport
        '
        Me.btnExport.BackColor = System.Drawing.Color.White
        Me.btnExport.BackgroundImage = CType(resources.GetObject("btnExport.BackgroundImage"), System.Drawing.Image)
        Me.btnExport.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnExport.BorderColor = System.Drawing.Color.Empty
        Me.btnExport.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnExport.FlatAppearance.BorderSize = 0
        Me.btnExport.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnExport.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnExport.ForeColor = System.Drawing.Color.Black
        Me.btnExport.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnExport.GradientForeColor = System.Drawing.Color.Black
        Me.btnExport.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnExport.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnExport.Location = New System.Drawing.Point(343, 12)
        Me.btnExport.Name = "btnExport"
        Me.btnExport.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnExport.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnExport.Selected = False
        Me.btnExport.ShowDefaultBorderColor = True
        Me.btnExport.Size = New System.Drawing.Size(90, 30)
        Me.btnExport.TabIndex = 0
        Me.btnExport.Text = "&Export"
        Me.btnExport.UseVisualStyleBackColor = False
        '
        'frmLanguageUtility
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(538, 272)
        Me.Controls.Add(Me.pnlMain)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmLanguageUtility"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Language Utility"
        Me.tabcLanguageUtility.ResumeLayout(False)
        Me.tabpExport.ResumeLayout(False)
        Me.gbExport.ResumeLayout(False)
        Me.gbExport.PerformLayout()
        Me.tabpImport.ResumeLayout(False)
        Me.gbImport.ResumeLayout(False)
        Me.gbImport.PerformLayout()
        Me.tabpImportLookUp.ResumeLayout(False)
        Me.gbImportLookup.ResumeLayout(False)
        Me.gbImportLookup.PerformLayout()
        Me.pnlMain.ResumeLayout(False)
        Me.objefFormFooter.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents tabcLanguageUtility As System.Windows.Forms.TabControl
    Friend WithEvents tabpExport As System.Windows.Forms.TabPage
    Friend WithEvents tabpImport As System.Windows.Forms.TabPage
    Friend WithEvents lblExportFileName As System.Windows.Forms.Label
    Friend WithEvents txtExportFile As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents txtImportFileName As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblExportPath As System.Windows.Forms.Label
    Friend WithEvents lblImportFilePath As System.Windows.Forms.Label
    Friend WithEvents fdbPath As System.Windows.Forms.FolderBrowserDialog
    Friend WithEvents ofdFile As System.Windows.Forms.OpenFileDialog
    Friend WithEvents pnlMain As System.Windows.Forms.Panel
    Friend WithEvents eZeeHeader As eZee.Common.eZeeHeader
    Friend WithEvents objbtnOpenDirectory As eZee.Common.eZeeLightButton
    Friend WithEvents objefFormFooter As eZee.Common.eZeeFooter
    Friend WithEvents btnImport As eZee.Common.eZeeLightButton
    Friend WithEvents btnExport As eZee.Common.eZeeLightButton
    Friend WithEvents btnClose As eZee.Common.eZeeLightButton
    Friend WithEvents objbtnOpenFile As eZee.Common.eZeeLightButton
    Friend WithEvents gbImport As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents gbExport As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents txtExportPath As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents tabpImportLookUp As System.Windows.Forms.TabPage
    Friend WithEvents gbImportLookup As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents btnLookup As eZee.Common.eZeeLightButton
    Friend WithEvents txtLookupFile As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblLookupFile As System.Windows.Forms.Label
End Class
