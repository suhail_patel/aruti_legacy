
''' <summary>
''' ApplicationIdleTimer provides a convenient way of
''' processing events only during application dormancy.
''' Why use this instead of the Application.Idle event?
''' That event gets fired EVERY TIME the message stack
''' is exhausted, which basically means it fires very
''' frequently.  With this, you only get events when 
''' the application is actually idle.
''' </summary>
Public Class clsApplicationIdleTimer

#Region "Static Members and Events"

    ' private singleton
    Private Shared instance As clsApplicationIdleTimer = Nothing

    ' Notes:
    ' Could have utilized the System.Timers.ElapsedEventArgs, but that 
    ' only provides the time an event happend (even though it's called
    ' "Elapsed" not "Timed" EventArgs).  I figgure most listeners care
    ' less *WHEN* the app went idle, but rather *HOW LONG* it has been 
    ' idle.

    ''' <summary>
    ''' EventArgs for an ApplicationIdle event.
    ''' </summary>
    Public Class ApplicationIdleEventArgs
        Inherits EventArgs
        ' time of last idle
        Private _idleSince As Date
        ' duration of "idleness"
        Private _idleTime As TimeSpan

        ''' <summary>
        ''' Internal constructor
        ''' </summary>
        ''' <param name="idleSince">Time app was declared idle</param>
        Friend Sub New(ByVal idleSince As Date)
            MyBase.New()
            _idleSince = idleSince
            _idleTime = New TimeSpan(Date.Now.Ticks - idleSince.Ticks)
        End Sub

        ''' <summary>
        ''' Timestamp of the last time the application was "active".
        ''' </summary>
        Public ReadOnly Property IdleSince() As Date
            Get
                Return _idleSince
            End Get
        End Property

        ''' <summary>
        ''' Duration of time the application has been idle.
        ''' </summary>
        Public ReadOnly Property IdleDuration() As TimeSpan
            Get
                Return _idleTime
            End Get
        End Property
    End Class
    ''' <summary>
    ''' ApplicationIdle event handler.
    ''' </summary>
    Public Delegate Sub ApplicationIdleEventHandler(ByVal e As ApplicationIdleEventArgs)

    ''' <summary>
    ''' Hook into the ApplicationIdle event to monitor inactivity.
    ''' It will fire AT MOST once per second.
    ''' </summary>
    Public Shared Event ApplicationIdle As ApplicationIdleEventHandler
#End Region

#Region "Private Members"
    ' Timer used to guarentee perodic updates.
    Private _timer As System.Timers.Timer

    ' Tracks idle state
    'INSTANT VB NOTE: The variable isIdle was renamed since Visual Basic does not allow class members with the same name:
    Private isIdle_Renamed As Boolean
    ' Last time application was declared "idle"
    Private lastAppIdleTime As Date
    ' Last time we checked for GUI activity
    Private lastIdleCheckpoint As Long
    ' Running count of Application.Idle events recorded since a checkpoint.
    ' Expressed as a long (instead of int) for math.
    Private idlesSinceCheckpoint As Long
    ' Number of ticks used by application process at last checkpoint
    Private cpuTime As Long
    ' Last time we checked for cpu activity
    Private lastCpuCheckpoint As Long

    ' These values can be adjusted through the static properties:
    ' Maximum "activity" (Application.Idle events per second) that will be considered "idle"
    ' Here it is expressed as minimum ticks between idles.
    Private guiThreshold As Long = TimeSpan.TicksPerMillisecond * 115L
    ' Maximum CPU use (percentage) that is considered "idle"
    Private cpuThreshold As Double = 0.01
#End Region

#Region "Constructors"
    ''' <summary>
    ''' Private constructor.  One instance is plenty.
    ''' </summary> 
    Private Sub New()
        ' Initialize counters
        isIdle_Renamed = False
        lastAppIdleTime = Date.Now
        lastCpuCheckpoint = Date.UtcNow.Ticks
        lastIdleCheckpoint = lastCpuCheckpoint
        cpuTime = 0
        idlesSinceCheckpoint = cpuTime

        ' Set up the timer and the counters
        _timer = New System.Timers.Timer(500) ' every half-second.
        _timer.Enabled = True
        _timer.Start()

        ' Hook into the events
        AddHandler _timer.Elapsed, AddressOf Heartbeat
        AddHandler Application.Idle, AddressOf Application_Idle
    End Sub
    ''' <summary>
    ''' Static initialization.  Called once per AppDomain.
    ''' </summary>
    Shared Sub New()
        ' Create the singleton.
        If instance Is Nothing Then
            instance = New clsApplicationIdleTimer()
        End If
    End Sub
#End Region

#Region "Private Methods"

    Private Sub Heartbeat(ByVal sender As Object, ByVal e As System.Timers.ElapsedEventArgs)
        ' First we need to do here is compensate for the
        ' "heartbeat", since it will result in an 'extra'
        ' Idle firing .. just don't cause a divide by zero!
        If idlesSinceCheckpoint > 1 Then
            idlesSinceCheckpoint -= 1
        End If

        Dim newIdle As Boolean = isIdle_Renamed
        Dim delta As Long = Date.UtcNow.Ticks - lastIdleCheckpoint

        ' Determine average idle events per second.  Done manually here
        ' instead of using the ComputeGUIActivity() method to avoid the 
        ' unnecessary numeric conversion and use of a TimeSpan object.
        If delta >= TimeSpan.TicksPerSecond Then
            ' It's been over a second since last checkpoint, 
            ' so determine how "busy" the app has been over that timeframe.
            If idlesSinceCheckpoint = 0 OrElse delta \ idlesSinceCheckpoint >= guiThreshold Then
                ' Minimal gui activity.  Check recent CPU activity.
                If cpuThreshold < 1.0 Then
                    newIdle = (ComputeCPUUsage(True) < cpuThreshold)
                Else
                    newIdle = True
                End If
            Else
                newIdle = False
            End If

            ' Update counters if state changed.
            If newIdle <> isIdle_Renamed Then
                isIdle_Renamed = newIdle
                If newIdle Then
                    lastAppIdleTime = Date.Now.AddTicks(-1L * delta)
                End If
            End If

            ' Reset checkpoint.
            lastIdleCheckpoint = Date.UtcNow.Ticks
            idlesSinceCheckpoint = 0

            ' Last but not least, if idle, raise the event.
            If newIdle Then
                OnApplicationIdle()
            End If
        End If
    End Sub

    Private Sub Application_Idle(ByVal sender As Object, ByVal e As EventArgs)
        ' Increment idle counter.
        idlesSinceCheckpoint += 1
    End Sub

    Friend Function ComputeCPUUsage(ByVal resetCounters As Boolean) As Double
        Dim delta As Long = Date.UtcNow.Ticks - lastCpuCheckpoint
        Dim pctUse As Double = 0.0
        Try
            ' Get total time this process has used the cpu.
            Dim cpu As Long = Process.GetCurrentProcess().TotalProcessorTime.Ticks

            ' Compute usage.
            If delta > 0 Then
                pctUse = (CDbl(cpu - cpuTime)) / CDbl(delta)
            Else
                pctUse = (If((cpu - cpuTime) = 0, 0.0, 1.0))
            End If

            ' Update counter and checkpoint if told OR if delta is at least a quarter second.
            ' This is to prevent inaccurate readings due to frequent OR infrequent calls.
            If resetCounters OrElse 4L * delta >= TimeSpan.TicksPerSecond Then
                lastCpuCheckpoint = Date.UtcNow.Ticks
                cpuTime = cpu

                ' Update idle status if above threshold.
                If (Not resetCounters) AndAlso isIdle_Renamed AndAlso pctUse > cpuThreshold Then
                    isIdle_Renamed = False
                End If
            End If
        Catch e1 As Exception
            ' Probably a security thing.  Just ignore.
            pctUse = Double.NaN
        End Try
        Return pctUse
    End Function

    Friend Function ComputeGUIActivity() As Double
        If idlesSinceCheckpoint <= 0 Then
            Return 0.0
        End If

        Dim delta As New TimeSpan(Date.UtcNow.Ticks - lastIdleCheckpoint)
        If delta.Ticks = 0 Then
            ' Clock hasn't updated yet.  Return a "real" value
            ' based on counter (either 0 or twice the threshold).
            Return (If(idlesSinceCheckpoint = 0, 0.0, (CDbl(TimeSpan.TicksPerSecond) / CDbl(instance.guiThreshold)) * 2.0))
        End If

        ' Expressed as activity (number of idles) per second.
        Return (CDbl(idlesSinceCheckpoint) / delta.TotalSeconds)

        ' Note that this method, unlike his CPU brother, does not reset any counters.
        ' The gui activity counters are reset once a second by the Heartbeat.
    End Function

    Private Sub OnApplicationIdle()
        ' Check to see if anyone cares.
        If ApplicationIdleEvent Is Nothing Then
            Return
        End If

        ' Build the message
        Dim e As New ApplicationIdleEventArgs(Me.lastAppIdleTime)

        ' Iterate over all listeners
        For Each multicast As MulticastDelegate In ApplicationIdleEvent.GetInvocationList()
            ' Raise the event
            multicast.DynamicInvoke(New Object() {e})
        Next multicast
    End Sub

#End Region

#Region "Static Properties"

    ''' <summary>
    ''' Returns the percent CPU use for the current process (0.0-1.0).
    ''' Will return double.NaN if indeterminate.
    ''' </summary>
    Public Shared ReadOnly Property CurrentCPUUsage() As Double
        Get
            Return instance.ComputeCPUUsage(False)
        End Get
    End Property

    ''' <summary>
    ''' Returns an "indication" of the gui activity, expressed as 
    ''' activity per second.  0 indicates no activity.
    ''' GUI activity includes user interactions (typing, 
    ''' moving mouse) as well as events, paint operations, etc.
    ''' </summary>
    Public Shared ReadOnly Property CurrentGUIActivity() As Double
        Get
            Return instance.ComputeGUIActivity()
        End Get
    End Property

    ''' <summary>
    ''' Returns the *last determined* idle state.  Idle state is 
    ''' recomputed once per second.  Both the gui and the cpu must
    ''' be idle for this property to be true.
    ''' </summary>
    Public Shared ReadOnly Property IsIdle() As Boolean
        Get
            Return instance.isIdle_Renamed
        End Get
    End Property

    Public Shared Property LastIdealTime() As System.Timers.Timer
        Get
            Return instance._timer
        End Get
        Set(ByVal value As System.Timers.Timer)
            instance._timer = value
        End Set
    End Property

    ''' <summary>
    ''' The threshold (gui activity) for determining idleness.
    ''' GUI activity below this level is considered "idle".
    ''' </summary>
    Public Shared Property GUIActivityThreshold() As Double
        Get
            Return (CDbl(TimeSpan.TicksPerSecond) / CDbl(instance.guiThreshold))
        End Get
        Set(ByVal value As Double)
            ' validate value
            If value <= 0.0 Then
                Throw New ArgumentOutOfRangeException("GUIActivityThreshold", value, "GUIActivityThreshold must be greater than zero.")
            End If

            instance.guiThreshold = CLng(Fix(CDbl(TimeSpan.TicksPerSecond) / value))
        End Set
    End Property

    ''' <summary>
    ''' The threshold (cpu usage) for determining idleness.
    ''' CPU usage below this level is considered "idle".
    ''' A value >= 1.0 will disable CPU idle checking.
    ''' </summary>
    Public Shared Property CPUUsageThreshold() As Double
        Get
            Return instance.cpuThreshold
        End Get
        Set(ByVal value As Double)
            If value = instance.cpuThreshold Then
                Return
            End If

            ' validate value
            If value < 0.0 Then
                Throw New ArgumentOutOfRangeException("CPUUsageThreshold", value, "Negative values are not allowed.")
            End If

            instance.cpuThreshold = value
        End Set
    End Property
#End Region

End Class

