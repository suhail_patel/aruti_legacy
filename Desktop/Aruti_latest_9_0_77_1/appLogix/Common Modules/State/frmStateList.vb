﻿Option Strict On

Imports eZeeCommonLib
Imports Aruti.Data

'Last Message Index = 3

Public Class frmstateList

#Region "Private Variable"

    Private objStatemaster As clsstate_master
    Private ReadOnly mstrModuleName As String = "frmstateList"

#End Region

#Region "Form's Event"

    Private Sub frmstateList_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        objStatemaster = New clsstate_master
        Try
            Call Set_Logo(Me, gApplicationType)
            Language.setLanguage(Me.Name)

            'Anjan (02 Sep 2011)-Start
            'Issue : Including Language Settings.
            Call OtherSettings()
            'Anjan (02 Sep 2011)-End 

            Call SetVisibility()
            FillCombo()
            fillList()
            If lvState.Items.Count > 0 Then lvState.Items(0).Selected = True
            lvState.Select()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmstateList_Load", mstrModuleName)
        End Try
    End Sub

    Private Sub frmstateList_KeyUp(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles MyBase.KeyUp
        Try
            If e.KeyCode = Keys.Delete And lvState.Focused = True Then
                'Sohail (28 Jun 2011) -- Start
                'Issue : Delete event fired even if there is no delete privilege
                'btnDelete_Click(sender, e)
                Call btnDelete.PerformClick()
                'Sohail (28 Jun 2011) -- End
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmstateList_KeyUp", mstrModuleName)
        End Try
    End Sub

    Private Sub frmstateList_FormClosed(ByVal sender As System.Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles MyBase.FormClosed
        objStatemaster = Nothing
    End Sub
    'Anjan (02 Sep 2011)-Start
    'Issue : Including Language Settings.
    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsstate_master.SetMessages()
            objfrm._Other_ModuleNames = "clsstate_master"
            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub
    'Anjan (02 Sep 2011)-End 

#End Region

#Region "Button's Event"

    Private Sub btnNew_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNew.Click
        Try
            Dim objfrmState_AddEdit As New frmState_AddEdit
            If objfrmState_AddEdit.displayDialog(-1, enAction.ADD_CONTINUE) Then
                fillList()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnNew_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnEdit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEdit.Click
        Try
            If lvState.SelectedItems.Count < 1 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please select State from the list to perform further operation."), enMsgBoxStyle.Information) '?1
                lvState.Select()
                Exit Sub
            End If
            Dim objfrmState_AddEdit As New frmState_AddEdit
            Try
                Dim intSelectedIndex As Integer
                intSelectedIndex = lvState.SelectedItems(0).Index
                If objfrmState_AddEdit.displayDialog(CInt(lvState.SelectedItems(0).Tag), enAction.EDIT_ONE) Then
                    Call fillList()
                End If
                objfrmState_AddEdit = Nothing

                lvState.Items(intSelectedIndex).Selected = True
                lvState.EnsureVisible(intSelectedIndex)
                lvState.Select()
            Catch ex As Exception
                Call DisplayError.Show("-1", ex.Message, "btnEdit_Click", mstrModuleName)
            Finally
                If objfrmState_AddEdit IsNot Nothing Then objfrmState_AddEdit.Dispose()
            End Try
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnEdit_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDelete.Click
        If lvState.SelectedItems.Count < 1 Then
            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please select State from the list to perform further operation."), enMsgBoxStyle.Information) '?1
            lvState.Select()
            Exit Sub
        End If
        If objStatemaster.isUsed(CInt(lvState.SelectedItems(0).Tag)) Then
            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Sorry, You cannot delete this State. Reason: This State is in use."), enMsgBoxStyle.Information) '?2
            lvState.Select()
            Exit Sub
        End If
        Try
            Dim intSelectedIndex As Integer
            intSelectedIndex = lvState.SelectedItems(0).Index

            If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Are you sure you want to delete this State?"), CType(enMsgBoxStyle.Question + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.Yes Then

                'S.SANDEEP [28-May-2018] -- START
                'ISSUE/ENHANCEMENT : {Audit Trails} 
                objStatemaster._FormName = mstrModuleName
                objStatemaster._LoginEmployeeunkid = 0
                objStatemaster._ClientIP = getIP()
                objStatemaster._HostName = getHostName()
                objStatemaster._FromWeb = False
                objStatemaster._AuditUserId = User._Object._Userunkid
objStatemaster._CompanyUnkid = Company._Object._Companyunkid
                objStatemaster._AuditDate = ConfigParameter._Object._CurrentDateAndTime
                'S.SANDEEP [28-May-2018] -- END

                objStatemaster.Delete(CInt(lvState.SelectedItems(0).Tag))
                lvState.SelectedItems(0).Remove()

                If lvState.Items.Count <= 0 Then
                    Exit Try
                End If

                If lvState.Items.Count = intSelectedIndex Then
                    intSelectedIndex = lvState.Items.Count - 1
                    lvState.Items(intSelectedIndex).Selected = True
                    lvState.EnsureVisible(intSelectedIndex)
                ElseIf lvState.Items.Count <> 0 Then
                    lvState.Items(intSelectedIndex).Selected = True
                    lvState.EnsureVisible(intSelectedIndex)
                End If
            End If
            lvState.Select()
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "btnDelete_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnSearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearch.Click
        Try
            fillList()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearch_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnReset.Click
        Try
            cboCountry.SelectedIndex = 0
            fillList()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnReset_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Me.Close()
    End Sub

#End Region

#Region " Private Methods "

    Private Sub fillList()
        Dim strSearching As String = String.Empty
        Dim dsStateList As New DataSet
        Try
            'Anjan (25 Oct 2012)-Start
            'ENHANCEMENT : TRA COMMENTS on Andrew's Request
            If User._Object.Privilege._AllowToViewStateList = False Then Exit Sub
            'Anjan (25 Oct 2012)-End 


            If CInt(cboCountry.SelectedValue) = 0 Then
                dsStateList = objStatemaster.GetList("State", True, False)
            Else
                dsStateList = objStatemaster.GetList("State", True, False, CInt(cboCountry.SelectedValue))
            End If


            Dim lvItem As ListViewItem

            lvState.Items.Clear()
            For Each drRow As DataRow In dsStateList.Tables(0).Rows
                lvItem = New ListViewItem
                lvItem.Text = drRow("country").ToString
                lvItem.Tag = drRow("stateunkid")
                lvItem.SubItems.Add(drRow("code").ToString)
                lvItem.SubItems.Add(drRow("name").ToString)
                lvState.Items.Add(lvItem)
            Next

            If lvState.Items.Count > 16 Then
                colhStataName.Width = 322 - 18
            Else
                colhStataName.Width = 322
            End If

        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "fillList", mstrModuleName)
        Finally
            dsStateList.Dispose()
        End Try
    End Sub

    Private Sub FillCombo()
        Try
            Dim objMasterdata As New clsMasterData
            Dim dsCountry As DataSet = objMasterdata.getCountryList("Country", True)
            cboCountry.ValueMember = "countryunkid"
            cboCountry.DisplayMember = "country_name"
            cboCountry.DataSource = dsCountry.Tables(0)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        End Try
    End Sub

    Private Sub SetVisibility()

        Try
            btnNew.Enabled = User._Object.Privilege._AddState
            btnEdit.Enabled = User._Object.Privilege._EditState
            btnDelete.Enabled = User._Object.Privilege._DeleteState
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetVisibility", mstrModuleName)
        End Try

    End Sub


#End Region

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Private Sub OtherSettings()
        Try
            Me.SuspendLayout()
            
            Call SetLanguage()

            Me.gbState.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbState.ForeColor = GUI._eZeeContainerHeaderForeColor

            Me.eZeeHeader.GradientColor1 = GUI._HeaderBackColor1
            Me.eZeeHeader.GradientColor2 = GUI._HeaderBackColor2
            Me.eZeeHeader.BorderColor = GUI._HeaderBorderColor
            Me.eZeeHeader.HeaderTextForeColor = GUI._HeaderTitleFontColor
            Me.eZeeHeader.DescriptionForeColor = GUI._HeaderMessageFontColor


            Me.btnDelete.GradientBackColor = GUI._ButttonBackColor
            Me.btnDelete.GradientForeColor = GUI._ButttonFontColor

            Me.btnEdit.GradientBackColor = GUI._ButttonBackColor
            Me.btnEdit.GradientForeColor = GUI._ButttonFontColor

            Me.btnNew.GradientBackColor = GUI._ButttonBackColor
            Me.btnNew.GradientForeColor = GUI._ButttonFontColor

            Me.btnClose.GradientBackColor = GUI._ButttonBackColor
            Me.btnClose.GradientForeColor = GUI._ButttonFontColor


            Me.ResumeLayout()
        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "OtherSettings", mstrModuleName)
        End Try
    End Sub


    Private Sub SetLanguage()
        Try
            Me.Text = Language._Object.getCaption(Me.Name, Me.Text)

            Me.eZeeHeader.Title = Language._Object.getCaption(Me.eZeeHeader.Name & "_Title", Me.eZeeHeader.Title)
            Me.eZeeHeader.Message = Language._Object.getCaption(Me.eZeeHeader.Name & "_Message", Me.eZeeHeader.Message)
            Me.gbState.Text = Language._Object.getCaption(Me.gbState.Name, Me.gbState.Text)
            Me.lblCountry.Text = Language._Object.getCaption(Me.lblCountry.Name, Me.lblCountry.Text)
            Me.colhCountry.Text = Language._Object.getCaption(CStr(Me.colhCountry.Tag), Me.colhCountry.Text)
            Me.colhStateCode.Text = Language._Object.getCaption(CStr(Me.colhStateCode.Tag), Me.colhStateCode.Text)
            Me.colhStataName.Text = Language._Object.getCaption(CStr(Me.colhStataName.Tag), Me.colhStataName.Text)
            Me.btnDelete.Text = Language._Object.getCaption(Me.btnDelete.Name, Me.btnDelete.Text)
            Me.btnEdit.Text = Language._Object.getCaption(Me.btnEdit.Name, Me.btnEdit.Text)
            Me.btnNew.Text = Language._Object.getCaption(Me.btnNew.Name, Me.btnNew.Text)
            Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
        End Try
    End Sub


    Private Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Please select State from the list to perform further operation.")
            Language.setMessage(mstrModuleName, 2, "Sorry, You cannot delete this State. Reason: This State is in use.")
            Language.setMessage(mstrModuleName, 3, "Are you sure you want to delete this State?")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class