<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmDemoNotification
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmDemoNotification))
        Me.Panel1 = New System.Windows.Forms.Panel
        Me.Panel3 = New System.Windows.Forms.Panel
        Me.lblDemoDescription = New System.Windows.Forms.Label
        Me.lblDaysLeft = New System.Windows.Forms.Label
        Me.lblOEMInfo = New System.Windows.Forms.Label
        Me.lblNoofRooms = New System.Windows.Forms.Label
        Me.Panel2 = New System.Windows.Forms.Panel
        Me.btnUnlock = New eZee.Common.eZeeLightButton(Me.components)
        Me.PictureBox1 = New System.Windows.Forms.PictureBox
        Me.btnCancel = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnFeedback = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnContinue = New eZee.Common.eZeeLightButton(Me.components)
        Me.Panel1.SuspendLayout()
        Me.Panel3.SuspendLayout()
        Me.Panel2.SuspendLayout()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(Me.Panel3)
        Me.Panel1.Controls.Add(Me.Panel2)
        Me.Panel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel1.Location = New System.Drawing.Point(0, 0)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(489, 315)
        Me.Panel1.TabIndex = 15
        '
        'Panel3
        '
        Me.Panel3.BackColor = System.Drawing.Color.SeaGreen
        Me.Panel3.BackgroundImage = CType(resources.GetObject("Panel3.BackgroundImage"), System.Drawing.Image)
        Me.Panel3.Controls.Add(Me.lblDemoDescription)
        Me.Panel3.Controls.Add(Me.lblDaysLeft)
        Me.Panel3.Controls.Add(Me.lblOEMInfo)
        Me.Panel3.Controls.Add(Me.lblNoofRooms)
        Me.Panel3.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel3.Location = New System.Drawing.Point(0, 0)
        Me.Panel3.Name = "Panel3"
        Me.Panel3.Size = New System.Drawing.Size(489, 261)
        Me.Panel3.TabIndex = 0
        '
        'lblDemoDescription
        '
        Me.lblDemoDescription.BackColor = System.Drawing.Color.Transparent
        Me.lblDemoDescription.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDemoDescription.ForeColor = System.Drawing.Color.LightSlateGray
        Me.lblDemoDescription.Location = New System.Drawing.Point(12, 75)
        Me.lblDemoDescription.Name = "lblDemoDescription"
        Me.lblDemoDescription.Size = New System.Drawing.Size(465, 53)
        Me.lblDemoDescription.TabIndex = 0
        Me.lblDemoDescription.Text = "At the end of the evaluation  period  the  software will  stop working.  You  can" & _
            " however continue using the software after the evaluation time is over by purcha" & _
            "sing the license."
        '
        'lblDaysLeft
        '
        Me.lblDaysLeft.BackColor = System.Drawing.Color.Transparent
        Me.lblDaysLeft.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDaysLeft.ForeColor = System.Drawing.Color.DarkRed
        Me.lblDaysLeft.Location = New System.Drawing.Point(12, 153)
        Me.lblDaysLeft.Name = "lblDaysLeft"
        Me.lblDaysLeft.Size = New System.Drawing.Size(465, 18)
        Me.lblDaysLeft.TabIndex = 1
        Me.lblDaysLeft.Text = "Days Left"
        '
        'lblOEMInfo
        '
        Me.lblOEMInfo.BackColor = System.Drawing.Color.Transparent
        Me.lblOEMInfo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblOEMInfo.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.lblOEMInfo.Location = New System.Drawing.Point(12, 192)
        Me.lblOEMInfo.Name = "lblOEMInfo"
        Me.lblOEMInfo.Size = New System.Drawing.Size(465, 53)
        Me.lblOEMInfo.TabIndex = 3
        Me.lblOEMInfo.Text = "To obtain a commercial license contact us at aruti@aruti.com today." & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "For more inf" & _
            "ormation visit us at www.aruti.com."
        '
        'lblNoofRooms
        '
        Me.lblNoofRooms.BackColor = System.Drawing.Color.Transparent
        Me.lblNoofRooms.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblNoofRooms.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.lblNoofRooms.Location = New System.Drawing.Point(12, 171)
        Me.lblNoofRooms.Name = "lblNoofRooms"
        Me.lblNoofRooms.Size = New System.Drawing.Size(465, 18)
        Me.lblNoofRooms.TabIndex = 2
        Me.lblNoofRooms.Text = "Thank you for evaluating Aruti"
        '
        'Panel2
        '
        Me.Panel2.BackColor = System.Drawing.Color.White
        Me.Panel2.Controls.Add(Me.btnUnlock)
        Me.Panel2.Controls.Add(Me.PictureBox1)
        Me.Panel2.Controls.Add(Me.btnCancel)
        Me.Panel2.Controls.Add(Me.btnFeedback)
        Me.Panel2.Controls.Add(Me.btnContinue)
        Me.Panel2.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.Panel2.Location = New System.Drawing.Point(0, 261)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(489, 54)
        Me.Panel2.TabIndex = 1
        '
        'btnUnlock
        '
        Me.btnUnlock.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnUnlock.BackColor = System.Drawing.Color.White
        Me.btnUnlock.BackgroundImage = CType(resources.GetObject("btnUnlock.BackgroundImage"), System.Drawing.Image)
        Me.btnUnlock.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnUnlock.BorderColor = System.Drawing.Color.Empty
        Me.btnUnlock.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnUnlock.FlatAppearance.BorderSize = 0
        Me.btnUnlock.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnUnlock.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnUnlock.ForeColor = System.Drawing.Color.Black
        Me.btnUnlock.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnUnlock.GradientForeColor = System.Drawing.Color.Black
        Me.btnUnlock.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnUnlock.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnUnlock.Location = New System.Drawing.Point(191, 12)
        Me.btnUnlock.Name = "btnUnlock"
        Me.btnUnlock.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnUnlock.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnUnlock.Size = New System.Drawing.Size(92, 30)
        Me.btnUnlock.TabIndex = 0
        Me.btnUnlock.Text = "&Register"
        Me.btnUnlock.UseVisualStyleBackColor = False
        '
        'PictureBox1
        '
        Me.PictureBox1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.PictureBox1.Location = New System.Drawing.Point(7, 3)
        Me.PictureBox1.Name = "PictureBox1"
        Me.PictureBox1.Size = New System.Drawing.Size(48, 48)
        Me.PictureBox1.TabIndex = 14
        Me.PictureBox1.TabStop = False
        '
        'btnCancel
        '
        Me.btnCancel.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnCancel.BackColor = System.Drawing.Color.White
        Me.btnCancel.BackgroundImage = CType(resources.GetObject("btnCancel.BackgroundImage"), System.Drawing.Image)
        Me.btnCancel.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnCancel.BorderColor = System.Drawing.Color.Empty
        Me.btnCancel.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.btnCancel.FlatAppearance.BorderSize = 0
        Me.btnCancel.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnCancel.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnCancel.ForeColor = System.Drawing.Color.Black
        Me.btnCancel.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnCancel.GradientForeColor = System.Drawing.Color.Black
        Me.btnCancel.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnCancel.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnCancel.Location = New System.Drawing.Point(387, 12)
        Me.btnCancel.Name = "btnCancel"
        Me.btnCancel.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnCancel.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnCancel.Size = New System.Drawing.Size(90, 30)
        Me.btnCancel.TabIndex = 2
        Me.btnCancel.Text = "&Cancel"
        Me.btnCancel.UseVisualStyleBackColor = False
        '
        'btnFeedback
        '
        Me.btnFeedback.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnFeedback.BackColor = System.Drawing.Color.White
        Me.btnFeedback.BackgroundImage = CType(resources.GetObject("btnFeedback.BackgroundImage"), System.Drawing.Image)
        Me.btnFeedback.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnFeedback.BorderColor = System.Drawing.Color.Empty
        Me.btnFeedback.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnFeedback.FlatAppearance.BorderSize = 0
        Me.btnFeedback.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnFeedback.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnFeedback.ForeColor = System.Drawing.Color.Black
        Me.btnFeedback.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnFeedback.GradientForeColor = System.Drawing.Color.Black
        Me.btnFeedback.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnFeedback.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnFeedback.Location = New System.Drawing.Point(289, 12)
        Me.btnFeedback.Name = "btnFeedback"
        Me.btnFeedback.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnFeedback.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnFeedback.Size = New System.Drawing.Size(92, 30)
        Me.btnFeedback.TabIndex = 15
        Me.btnFeedback.Text = "&Feedback"
        Me.btnFeedback.UseVisualStyleBackColor = False
        Me.btnFeedback.Visible = False
        '
        'btnContinue
        '
        Me.btnContinue.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnContinue.BackColor = System.Drawing.Color.White
        Me.btnContinue.BackgroundImage = CType(resources.GetObject("btnContinue.BackgroundImage"), System.Drawing.Image)
        Me.btnContinue.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnContinue.BorderColor = System.Drawing.Color.Empty
        Me.btnContinue.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnContinue.DialogResult = System.Windows.Forms.DialogResult.OK
        Me.btnContinue.FlatAppearance.BorderSize = 0
        Me.btnContinue.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnContinue.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnContinue.ForeColor = System.Drawing.Color.Black
        Me.btnContinue.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnContinue.GradientForeColor = System.Drawing.Color.Black
        Me.btnContinue.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnContinue.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnContinue.Location = New System.Drawing.Point(289, 12)
        Me.btnContinue.Name = "btnContinue"
        Me.btnContinue.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnContinue.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnContinue.Size = New System.Drawing.Size(92, 30)
        Me.btnContinue.TabIndex = 1
        Me.btnContinue.Text = "&Continue"
        Me.btnContinue.UseVisualStyleBackColor = False
        '
        'frmDemoNotification
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(489, 315)
        Me.Controls.Add(Me.Panel1)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmDemoNotification"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Demo Notification"
        Me.Panel1.ResumeLayout(False)
        Me.Panel3.ResumeLayout(False)
        Me.Panel2.ResumeLayout(False)
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents Panel2 As System.Windows.Forms.Panel
    Friend WithEvents Panel3 As System.Windows.Forms.Panel
    Friend WithEvents lblNoofRooms As System.Windows.Forms.Label
    Friend WithEvents lblOEMInfo As System.Windows.Forms.Label
    Friend WithEvents lblDaysLeft As System.Windows.Forms.Label
    Friend WithEvents PictureBox1 As System.Windows.Forms.PictureBox
    Friend WithEvents lblDemoDescription As System.Windows.Forms.Label
    Friend WithEvents btnUnlock As eZee.Common.eZeeLightButton
    Friend WithEvents btnContinue As eZee.Common.eZeeLightButton
    Friend WithEvents btnCancel As eZee.Common.eZeeLightButton
    Friend WithEvents btnFeedback As eZee.Common.eZeeLightButton
End Class
