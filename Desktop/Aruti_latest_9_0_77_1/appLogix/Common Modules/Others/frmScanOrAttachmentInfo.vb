﻿#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data
Imports System.IO
Imports System.IO.Packaging

#End Region

Public Class frmScanOrAttachmentInfo

#Region " Private Variables "

    Private ReadOnly mstrModuleName As String = "frmScanOrAttachmentInfo"
    Private mstrLableCaption As String = ""
    Private objDocument As clsScan_Attach_Documents
    Private mdtTran As DataTable
    Private mintRefModuleId As Integer = -1
    Private mAction As enAction
    Private mstrToEditIds As String


    'Pinkal (03-Jan-2014) -- Start
    'Enhancement : Oman Changes


    'Nilay (03-Dec-2015) -- Start
    'ENHANCEMENT for CCBRT & SL - Requested by Andrew Muga.
    'Private mstrEmployeeIds As String = ""
    Private mintEmployeeId As Integer = -1
    'Nilay (03-Dec-2015) -- End


    Private mintScanRefId As Integer = -1
    'Pinkal (03-Jan-2014) -- End


    'Pinkal (07-May-2015) -- Start
    'Leave Enhancement - CHANGES IN LEAVE FORM FOR SCAN ATTACH DOCUMENTS.
    Private mblnIsOnAddTransaction As Boolean = False
    Private mblnCancel As Boolean = True
    'Pinkal (07-May-2015) -- End

    'SHANI (16 JUL 2015) -- Start
    'Enhancement : Save all attachments from deskto and SS to IIS Aruti Self Service 
    'Upload Image folder to access those attachemts from Server and 
    'all client machines if ArutiSelfService is installed otherwise save then on Document path
    Private mdsDoc As DataSet
    Private mstrFolderName As String = ""
    'SHANI (16 JUL 2015) -- End 

    'Nilay (03-Dec-2015) -- Start
    'ENHANCEMENT for CCBRT & SL - Requested by Andrew Muga.
    Private mblnIsEmpForm As Boolean = False
    Private mstrEmpName As String = ""
    Private mblnIsApproved As Boolean = True
    Private mstrMissingDoc As String = String.Empty
    'Nilay (03-Dec-2015) -- End


    'Pinkal (06-Jan-2016) -- Start
    'Enhancement - Working on Changes in SS for Leave Module.
    Private mintTransactionID As Integer = -1
    'Pinkal (06-Jan-2016) -- End

    'S.SANDEEP [26 JUL 2016] -- START
    'ENHANCEMENT : DISCIPLINE MAKE-OVER {BY ANDREW}
    Private mstrTransactionScreenName As String = String.Empty
    Private mstrTransactionGuidString As String = String.Empty
    'S.SANDEEP [26 JUL 2016] -- END

    'Shani (20-Aug-2016) -- Start
    'Enhancement - Add Scan Attchment in Claim Request Module given by Matthew
    Private mintTransactionMstUnkId As Integer = -1
    'Shani (20-Aug-2016) -- End

    'S.SANDEEP |26-APR-2019| -- START
    Private mdtSource As DataTable = Nothing
    Private mstrValueMember As String = String.Empty
    Private mstrDisplayMember As String = String.Empty
    Private mblnAddEmployeeDataApprovalCondition As Boolean = False
    Private mblnIsEmpDataApprovalIsOn As Boolean = False
    'S.SANDEEP |26-APR-2019| -- END

#End Region

#Region " Display Dialog "


    'Nilay (03-Dec-2015) -- Start
    'ENHANCEMENT for CCBRT & SL - Requested by Andrew Muga.
    'Public Function displayDialog(ByVal strLabelCaption As String, ByVal intRefModuleId As Integer, ByVal eAction As enAction, ByVal StrEditIds As String _
    '                                             , Optional ByVal EmployeeIds As String = "", Optional ByVal intScanRefModuleId As Integer = -1, Optional ByVal blnIsOnAddTransaction As Boolean = False) As Boolean

    'S.SANDEEP [26 JUL 2016] -- START
    'ENHANCEMENT : DISCIPLINE MAKE-OVER {BY ANDREW}
    Public Function displayDialog(ByVal strLabelCaption As String, _
                                  ByVal intRefModuleId As Integer, _
                                  ByVal eAction As enAction, _
                                  ByVal StrEditIds As String, _
                                  ByVal strTransactionScreenName As String, _
                                  ByVal blnAddEmployeeDataApprovalCondition As Boolean, _
                                  Optional ByVal EmployeeIds As Integer = -1, _
                                  Optional ByVal intScanRefModuleId As Integer = -1, _
                                  Optional ByVal blnIsOnAddTransaction As Boolean = False, _
                                  Optional ByVal blnIsEmpForm As Boolean = False, _
                                  Optional ByVal strEmployeeName As String = "", _
                                  Optional ByVal blnIsApproved As Boolean = True, _
                                  Optional ByVal dtSource As DataTable = Nothing, _
                                  Optional ByVal strValueMember As String = "", _
                                  Optional ByVal strDisplayMember As String = "", _
                                  Optional ByVal blnIsEmpDataApprovalIsOn As Boolean = False) As Boolean 'S.SANDEEP |26-APR-2019| -- START {mListSource,strValueMember,strDisplayMember} -- END

        'Public Function displayDialog(ByVal strLabelCaption As String, _
        '                          ByVal intRefModuleId As Integer, _
        '                          ByVal eAction As enAction, _
        '                          ByVal StrEditIds As String, _
        '                          Optional ByVal EmployeeIds As Integer = -1, _
        '                          Optional ByVal intScanRefModuleId As Integer = -1, _
        '                          Optional ByVal blnIsOnAddTransaction As Boolean = False, _
        '                          Optional ByVal blnIsEmpForm As Boolean = False, _
        '                          Optional ByVal strEmployeeName As String = "", _
        '                          Optional ByVal blnIsApproved As Boolean = True) As Boolean
        'S.SANDEEP [26 JUL 2016] -- END


        'Nilay (03-Dec-2015) -- End
        Try
            mstrLableCaption = strLabelCaption
            mintRefModuleId = intRefModuleId


            'Pinkal (03-Jan-2014) -- Start
            'Enhancement : Oman Changes

            'Nilay (03-Dec-2015) -- Start
            'ENHANCEMENT for CCBRT & SL - Requested by Andrew Muga.
            'mstrEmployeeIds = EmployeeIds
            mintEmployeeId = EmployeeIds
            mblnIsEmpForm = blnIsEmpForm
            mstrEmpName = strEmployeeName
            mblnIsApproved = blnIsApproved
            'Nilay (03-Dec-2015) -- End

            mintScanRefId = intScanRefModuleId
            'Pinkal (03-Jan-2014) -- End


            'Pinkal (07-May-2015) -- Start
            'Leave Enhancement - CHANGES IN LEAVE FORM FOR SCAN ATTACH DOCUMENTS.
            mblnIsOnAddTransaction = blnIsOnAddTransaction
            'Pinkal (07-May-2015) -- End


            mAction = eAction
            mstrToEditIds = StrEditIds


            'S.SANDEEP [26 JUL 2016] -- START
            'ENHANCEMENT : DISCIPLINE MAKE-OVER {BY ANDREW}
            mstrTransactionScreenName = strTransactionScreenName
            'S.SANDEEP [26 JUL 2016] -- END

            'S.SANDEEP |26-APR-2019| -- START
            mdtSource = dtSource
            mstrValueMember = strValueMember
            mstrDisplayMember = strDisplayMember
            If intScanRefModuleId > 0 Then
                cboDocumentType.Enabled = False
            End If
            mblnAddEmployeeDataApprovalCondition = blnAddEmployeeDataApprovalCondition
            mblnIsEmpDataApprovalIsOn = blnIsEmpDataApprovalIsOn
            'S.SANDEEP |26-APR-2019| -- END

            Me.ShowDialog()


            'Pinkal (07-May-2015) -- Start
            'Leave Enhancement - CHANGES IN LEAVE FORM FOR SCAN ATTACH DOCUMENTS.
            Return Not mblnCancel
            'Pinkal (07-May-2015) -- End

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "displayDialog", mstrModuleName)
        Finally
        End Try
    End Function
#End Region

#Region "Public Property"

    'Pinkal (07-May-2015) -- Start
    'Leave Enhancement - CHANGES IN LEAVE FORM FOR SCAN ATTACH DOCUMENTS.

    Public Property _dtAttachment() As DataTable
        Get
            Return mdtTran
        End Get
        Set(ByVal value As DataTable)
            mdtTran = value
        End Set
    End Property

    'Pinkal (07-May-2015) -- End

    'Pinkal (06-Jan-2016) -- Start
    'Enhancement - Working on Changes in SS for Leave Module.

    Public Property _TransactionID() As Integer
        Get
            Return mintTransactionID
        End Get
        Set(ByVal value As Integer)
            mintTransactionID = value
        End Set
    End Property

    'Pinkal (06-Jan-2016) -- End

    'S.SANDEEP [25 JUL 2016] -- START
    'ENHANCEMENT : DISCIPLINE MAKE-OVER {BY ANDREW}
    Public Property _TransactionGuidString() As String
        Get
            Return mstrTransactionGuidString
        End Get
        Set(ByVal value As String)
            mstrTransactionGuidString = value
        End Set
    End Property
    'S.SANDEEP [25 JUL 2016] -- START

    'Shani (20-Aug-2016) -- Start
    'Enhancement - Add Scan Attchment in Claim Request Module given by Matthew
    Public Property _TransactionMstUnkId() As Integer
        Get
            Return mintTransactionMstUnkId
        End Get
        Set(ByVal value As Integer)
            mintTransactionMstUnkId = value
        End Set
    End Property
    'Shani (20-Aug-2016) -- End


#End Region

#Region " Form's Events "

    Private Sub frmScanOrAttachmentInfo_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        objDocument = New clsScan_Attach_Documents
        Try
            Call Set_Logo(Me, gApplicationType)
            SetColor()
            objlblCaption.Text = mstrLableCaption
            lvScanAttachment.GridLines = False

            'Sohail (10 Jul 2014) -- Start
            'Enhancement - Custom Language.
            Language.setLanguage(Me.Name)
            Call OtherSettings()
            'Sohail (10 Jul 2014) -- End

            Call FillCombo()
            ofdAttachment.Filter = "All Image Formats (*.bmp;*.jpg;*.jpeg;*.gif;*.tif;*.png)|*.bmp;*.jpg;*.jpeg;*.gif;*.tif;*.png|Bitmaps (*.bmp)|*.bmp|GIFs (*.gif)|*.gif|JPEGs (*.jpg)|*.jpg;*.jpeg|TIFs(*.tif)|*.tif|PNGs(*.png)|*.png|All Files (*.*)|*.*"

            'Nilay (03-Dec-2015) -- Start
            'ENHANCEMENT for CCBRT & SL - Requested by Andrew Muga.
            If mAction = enAction.EDIT_ONE AndAlso mdtTran Is Nothing Then
                'Nilay (03-Dec-2015) -- End

                'Shani(24-Aug-2015) -- Start
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                'Call objDocument.GetList("List", mstrToEditIds)

                'S.SANDEEP |04-SEP-2021| -- START
                'Call objDocument.GetList(ConfigParameter._Object._Document_Path, "List", mstrToEditIds)
                Call objDocument.GetList(ConfigParameter._Object._Document_Path, "List", mstrToEditIds, , , , , , CBool(IIf(mstrToEditIds.Trim.Length <= 0, True, False)))
                'S.SANDEEP |04-SEP-2021| -- END

                'Shani(24-Aug-2015) -- End

                btnEdit.Visible = True
                btnEdit.BringToFront()
                btnScan.Enabled = False
                'Nilay (03-Dec-2015) -- Start
                'ENHANCEMENT for CCBRT & SL - Requested by Andrew Muga.
                'btnAdd.Enabled = False
                btnAdd.Enabled = True
                'Nilay (03-Dec-2015) -- End
                cboEmployee.Enabled = False
                objbtnSearch.Enabled = False
                AddHandler lvScanAttachment.SelectedIndexChanged, AddressOf lvScanAttachment_SelectedIndexChanged
                'Nilay (03-Dec-2015) -- Start
                'ENHANCEMENT for CCBRT & SL - Requested by Andrew Muga.
            ElseIf mAction = enAction.EDIT_ONE AndAlso mblnIsEmpForm = True Then
                cboEmployee.Enabled = False
                objbtnSearch.Enabled = False
                'S.SANDEEP |16-MAY-2019| -- START
                'ISSUE/ENHANCEMENT : {Attachment Preview} Leave UAT
                'btnPreview.Visible = True
                'S.SANDEEP |16-MAY-2019| -- END

            ElseIf mAction = enAction.ADD_ONE AndAlso mblnIsEmpForm = True Then
                cboEmployee.Enabled = False
                objbtnSearch.Enabled = False

                'S.SANDEEP |04-SEP-2021| -- START
                'Call objDocument.GetList(ConfigParameter._Object._Document_Path, "List", "", mintEmployeeId)
                Call objDocument.GetList(ConfigParameter._Object._Document_Path, "List", "", mintEmployeeId, , , , , CBool(IIf(mintEmployeeId <= 0, True, False)))
                'S.SANDEEP |04-SEP-2021| -- END

                'Nilay (03-Dec-2015) -- End
            ElseIf mAction = enAction.EDIT_ONE AndAlso mblnIsEmpForm = False AndAlso mblnIsOnAddTransaction = True Then
                If mintScanRefId = enScanAttactRefId.DISCIPLINES Then
                    cboEmployee.Enabled = False
                    objbtnSearch.Enabled = False
                    cboDocumentType.Enabled = False
                    cboAssocatedValue.Enabled = False
                    btnScan.Enabled = False
                    btnSave.Enabled = False
                    btnAdd.Enabled = False
                    btnEdit.Enabled = False
                    btnRemove.Enabled = False
                End If
            End If

            'Nilay (03-Dec-2015) -- Start
            'ENHANCEMENT for CCBRT & SL - Requested by Andrew Muga.
            'mdtTran = objDocument._Datatable.Copy
            If mdtTran Is Nothing Then
                mdtTran = objDocument._Datatable.Copy
            End If

            'If mAction = enAction.EDIT_ONE Then
            '    Call FillList()
            'End If

            'S.SANDEEP [24 MAY 2016] -- START
            'ENHANCEMENT : DISCIPLINE MAKE-OVER {BY ANDREW}
            'If mAction = enAction.EDIT_ONE OrElse mAction = enAction.ADD_ONE AndAlso mblnIsEmpForm = True Then
            '    Call FillList()
            'End If
            If mdtTran IsNot Nothing Then
                Call FillList()
            End If
            'S.SANDEEP [24 MAY 2016] -- END


            'Nilay (03-Dec-2015) -- End


            'SHANI (16 JUL 2015) -- Start
            'Enhancement : Save all attachments from deskto and SS to IIS Aruti Self Service 
            'Upload Image folder to access those attachemts from Server and 
            'all client machines if ArutiSelfService is installed otherwise save then on Document path
            mdsDoc = (New clsScan_Attach_Documents).GetDocFolderName("Docs")
            'SHANI (16 JUL 2015) -- End 

            'S.SANDEEP [24 MAY 2016] -- START
            'ENHANCEMENT : DISCIPLINE MAKE-OVER {BY ANDREW}
            If mintScanRefId = enScanAttactRefId.DISCIPLINES Then
                'S.SANDEEP |16-MAY-2019| -- START
                'ISSUE/ENHANCEMENT : {Attachment Preview} Leave UAT
                'btnPreview.Visible = True
                'S.SANDEEP |16-MAY-2019| -- END
                'cboDocument.SelectedValue = 0
            End If
            'S.SANDEEP [24 MAY 2016] -- END


            'Pinkal (03-May-2019) -- Start
            'Enhancement - Working on Leave UAT Changes for NMB.
            If mintScanRefId = enScanAttactRefId.LEAVEFORMS Then
                lblDocType.Visible = False
                cboDocumentType.Visible = False
                lblSelection.Visible = False
                cboAssocatedValue.Visible = False
                lblUploadDate.Visible = False
                dtpUploadDate.Visible = False
            End If
            'Pinkal (03-May-2019) -- End


        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmScanOrAttachmentInfo_Load", mstrModuleName)
        Finally
        End Try
    End Sub

    'Sohail (10 Jul 2014) -- Start
    'Enhancement - Custom Language.
    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsScan_Attach_Documents.SetMessages()
            objfrm._Other_ModuleNames = "clsScan_Attach_Documents"
            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub
    'Sohail (10 Jul 2014) -- End

#End Region

#Region " Button's Events "

    Private Sub btnRemove_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnRemove.Click
        Try
            If lvScanAttachment.SelectedItems.Count > 0 Then
                Dim drTemp() As DataRow = Nothing

                If lvScanAttachment.SelectedItems(0).Tag > 0 Then
                    drTemp = mdtTran.Select("scanattachtranunkid = '" & CInt(lvScanAttachment.SelectedItems(0).Tag) & "'")
                Else
                    drTemp = mdtTran.Select("GUID = '" & lvScanAttachment.SelectedItems(0).SubItems(objcolhGuid.Index).Text.ToString & "'")
                End If

                If drTemp.Length > 0 Then
                    drTemp(0)("AUD") = "D"
                End If
                Call FillList()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnRemove_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub btnAdd_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAdd.Click
        Try
            If IsDataValid() = False Then
                Exit Sub
            End If

            If (ofdAttachment.ShowDialog(Me) = System.Windows.Forms.DialogResult.OK) Then

                Dim f As New System.IO.FileInfo(ofdAttachment.FileName)


                'Pinkal (27-Nov-2020) -- Start
                'Enhancement Outlook  -  Working on  Don't Allow Users to attach .Exe file types in email.
                Dim mstrExtension As String = Path.GetExtension(ofdAttachment.FileName).ToLower
                If mstrExtension = ".exe" Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 14, "You can not attach .exe(Executable File) for the security reason."))
                    Exit Sub
                End If
                'Pinkal (27-Nov-2020) -- End

                'Sohail (14 Feb 2022) -- Start
                'Enhancement :  : ZRA -Allowing only doc and pdf attachments on recruitment portal for ZRA.
                Dim objGroupMaster As New clsGroup_Master
                objGroupMaster._Groupunkid = 1
                If objGroupMaster._Groupname.ToString().ToUpper() = "ZAMBIA REVENUE AUTHORITY" AndAlso mintRefModuleId = enImg_Email_RefId.Applicant_Job_Vacancy Then
                    If Not (mstrExtension = ".doc" OrElse mstrExtension = ".docx" OrElse mstrExtension = ".pdf") Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 15, "Please select only Document or PDF file."))
                        Exit Sub
                    End If
                End If
                'Sohail (14 Feb 2022) -- End

                'Nilay (03-Dec-2015) -- Start
                'ENHANCEMENT for CCBRT & SL - Requested by Andrew Muga.
                'If objDocument.IsExist(mintRefModuleId, CInt(cboDocumentType.SelectedValue), f.Name, CInt(cboEmployee.SelectedValue)) = False Then
                '    Call AddRow(f, ofdAttachment.FileName.ToString)
                'Else
                '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 6, "Selected information is already present for particular employee."), enMsgBoxStyle.Information)
                '    Exit Sub
                'End If
                If mblnIsEmpForm = True AndAlso mintEmployeeId <= 0 Then
                    Dim xRow() As DataRow = mdtTran.Select("scanattachrefid = '" & CInt(cboDocumentType.SelectedValue) & "' AND modulerefid = '" & mintRefModuleId & "' AND filename = '" & f.Name & "' AND AUD <> 'D' ")
                    If xRow.Length <= 0 Then
                        Call AddRow(f, ofdAttachment.FileName.ToString)
                    Else
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 6, "Selected information is already present for particular employee."), enMsgBoxStyle.Information)
                        Exit Sub
                    End If

                ElseIf mblnIsEmpForm = True Then
                    Dim xRow() As DataRow = mdtTran.Select("scanattachrefid = '" & CInt(cboDocumentType.SelectedValue) & "' AND modulerefid = '" & mintRefModuleId & "' AND filename = '" & f.Name & "' AND AUD <> 'D' ")
                    If xRow.Length <= 0 Then
                        Call AddRow(f, ofdAttachment.FileName.ToString)
                    Else
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 6, "Selected information is already present for particular employee."), enMsgBoxStyle.Information)
                        Exit Sub
                    End If

                ElseIf mblnIsEmpForm = False Then

                    'Gajanan [02-SEP-2019] -- Start      
                    'Enhancements: ENHANCEMENTS IN ONLINE RECRUITMENT FOR NMB
                    'If objDocument.IsExist(mintRefModuleId, CInt(cboDocumentType.SelectedValue), f.Name, CInt(cboEmployee.SelectedValue)) = False Then
                    If objDocument.IsExist(mintRefModuleId, CInt(cboDocumentType.SelectedValue), f.Name, CInt(cboEmployee.SelectedValue), -1, IIf(mintRefModuleId = CInt(enImg_Email_RefId.Staff_Requisition), mintTransactionID, -1)) = False Then
                        'Gajanan [02-SEP-2019] -- End    

                        Dim xRow() As DataRow = mdtTran.Select("scanattachrefid = '" & CInt(cboDocumentType.SelectedValue) & "' AND modulerefid = '" & mintRefModuleId & "' AND filename = '" & f.Name & "' AND AUD <> 'D' ")
                        If xRow.Length <= 0 Then
                            Call AddRow(f, ofdAttachment.FileName.ToString)
                        Else
                            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 6, "Selected information is already present for particular employee."), enMsgBoxStyle.Information)
                            Exit Sub
                        End If
                    Else
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 6, "Selected information is already present for particular employee."), enMsgBoxStyle.Information)
                        Exit Sub
                    End If
                End If
                'Nilay (03-Dec-2015) -- End
                f = Nothing
            End If

            Call FillList()

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnAdd_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub btnScan_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnScan.Click
        Try
            If IsDataValid() = False Then
                Exit Sub
            End If

            Dim frm As New frmCommonImageScanner
            If frm.DisplayDialog() <> Windows.Forms.DialogResult.Cancel Then
                Dim img As Image
                img = frm._Image
                If img IsNot Nothing Then
                    Dim strFileName As String = Guid.NewGuid().ToString & ".png"

                    'Pinkal (09-Dec-2020) -- Start
                    'Bug  -  Worked on Login and Forgot Password Issue in Oryx.
                    'System.IO.File.Create(System.IO.Path.GetTempPath & "\" & strFileName)
                    Dim imgCon As New ImageConverter()
                    Dim imgbyte As Byte() = imgCon.ConvertTo(img, GetType(Byte()))
                    Dim ms As New MemoryStream(imgbyte)
                    Dim fs As New FileStream(System.IO.Path.GetTempPath & "\" & strFileName, FileMode.Create)
                    ms.WriteTo(fs)
                    ms.Close()
                    fs.Close()
                    fs.Dispose()
                    imgCon = Nothing
                    Dim f As New System.IO.FileInfo(System.IO.Path.GetTempPath & "\" & strFileName)
                    'Pinkal (09-Dec-2020) -- End

                    Call AddRow(f, System.IO.Path.GetTempPath & "\" & strFileName.ToString)
                    Call FillList()
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnScan_Click", mstrModuleName)
        Finally
        End Try


    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            ''Nilay (03-Dec-2015) -- Start
            ''ENHANCEMENT for CCBRT & SL - Requested by Andrew Muga.
            'Dim blnFlag As Boolean = IsValid_MandatoryDoc()
            'If blnFlag = False Then
            '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 13, "Sorry, Some of the Scan/Attached documents which are set as mandatory. Please click operation button to attach missing document(s) - " & mstrMissingDoc), enMsgBoxStyle.Information)
            '    Exit Sub
            'End If
            ''Nilay (03-Dec-2015) -- End
            Dim dtTemp() As DataRow = Nothing
            dtTemp = mdtTran.Select("AUD IN ('A','U')")
            If dtTemp.Length > 0 Then
                If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "There are some unsaved information(s) on the list." & vbCrLf & " Are you sure, you don't want to save the information(s)?"), CType(enMsgBoxStyle.Information + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.Yes Then
                    Me.Close()
                End If
            Else
                Me.Close()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnClose_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub objbtnSearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearch.Click
        Dim frm As New frmCommonSearch
        Try
            With frm
                .DisplayMember = cboEmployee.DisplayMember
                .ValueMember = cboEmployee.ValueMember
                Select Case mintRefModuleId
                    Case enImg_Email_RefId.Employee_Module, _
                         enImg_Email_RefId.Dependants_Beneficiaries, _
                         enImg_Email_RefId.Leave_Module, _
                         enImg_Email_RefId.Payroll_Module, _
                         enImg_Email_RefId.Training_Module
                        .CodeMember = "employeecode"
                    Case enImg_Email_RefId.Applicant_Module
                        .CodeMember = "applicant_code"
                End Select
                .DataSource = CType(cboEmployee.DataSource, DataTable)
            End With

            If frm.DisplayDialog Then
                cboEmployee.SelectedValue = frm.SelectedValue
                cboEmployee.Focus()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearch_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Try
            'Sohail (29 Jan 2013) -- Start
            'TRA - ENHANCEMENT
            If ConfigParameter._Object._Document_Path.Trim = "" Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 10, "Please set Documents Path from Aruti Configuration -> Path."), enMsgBoxStyle.Information)
                Exit Try
            ElseIf mdtTran.Rows.Count <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 11, "Please add atleast one document."), enMsgBoxStyle.Information)
                Exit Try
            End If
            'Sohail (29 Jan 2013) -- End

            'Nilay (03-Dec-2015) -- Start
            'ENHANCEMENT for CCBRT & SL - Requested by Andrew Muga.
            If mblnIsEmpForm = True Then
                Dim blnFlag As Boolean = IsValid_MandatoryDoc()
                If blnFlag = False Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 13, "Sorry, Some of the Scan/Attached documents are set as mandatory. Please click ADD button to attach missing document(s) - ") & mstrMissingDoc, enMsgBoxStyle.Information)
                    Exit Sub
                End If
            End If
            'Nilay (03-Dec-2015) -- End

            'Pinkal (07-May-2015) -- Start
            'Leave Enhancement - CHANGES IN LEAVE FORM FOR SCAN ATTACH DOCUMENTS.
            If mblnIsOnAddTransaction = False Then

                'SHANI (16 JUL 2015) -- Start
                'Enhancement : Save all attachments from deskto and SS to IIS Aruti Self Service 
                'Upload Image folder to access those attachemts from Server and 
                'all client machines if ArutiSelfService is installed otherwise save then on Document path
                Dim blnIsIISInstalled As Boolean = IsSelfServiceExist()
                Dim strError As String = ""

                For Each dRow As DataRow In mdtTran.Rows
                    If dRow("AUD").ToString = "A" AndAlso dRow("orgfilepath").ToString <> "" Then
                        Dim strFileName As String = Company._Object._Code & "_" & Guid.NewGuid.ToString & Path.GetExtension(CStr(dRow("orgfilepath")))
                        Dim intScanAttachRefId As Integer = CInt(dRow("scanattachrefid"))
                        mstrFolderName = (From p In mdsDoc.Tables("Docs") Where (CInt(p.Item("Id").ToString) = intScanAttachRefId) Select (p.Item("Name").ToString)).FirstOrDefault
                        If blnIsIISInstalled Then

                            'Shani(24-Aug-2015) -- Start
                            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                            'If clsFileUploadDownload.UploadFile(CStr(dRow("orgfilepath")), mstrFolderName, strFileName, strError) = False Then
                            If clsFileUploadDownload.UploadFile(CStr(dRow("orgfilepath")), mstrFolderName, strFileName, strError, ConfigParameter._Object._ArutiSelfServiceURL) = False Then
                                'Shani(24-Aug-2015) -- End

                                eZeeMsgBox.Show(strError, enMsgBoxStyle.Information)
                                Exit Sub
                            Else
                                Dim strPath As String = ConfigParameter._Object._ArutiSelfServiceURL
                                dRow("fileuniquename") = strFileName
                                If Strings.Right(strPath, 1) <> "/" AndAlso Strings.Right(strPath, 1) <> "\" Then
                                    strPath += "/"
                                End If
                                strPath += "uploadimage/" & mstrFolderName & "/" + strFileName
                                dRow("filepath") = strPath
                                dRow.AcceptChanges()
                            End If
                        Else
                            If Directory.Exists(ConfigParameter._Object._Document_Path) Then
                                Dim strDocLocalPath As String = ConfigParameter._Object._Document_Path & "\" & mstrFolderName & "\" & strFileName
                                If Directory.Exists(ConfigParameter._Object._Document_Path & "\" & mstrFolderName) = False Then
                                    Directory.CreateDirectory(ConfigParameter._Object._Document_Path & "\" & mstrFolderName)
                                End If
                                File.Copy(CStr(dRow("orgfilepath")), strDocLocalPath, True)
                                dRow("fileuniquename") = strFileName
                                dRow("filepath") = strDocLocalPath
                                dRow.AcceptChanges()
                            Else
                                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 12, "Configuration Path does not Exist."), enMsgBoxStyle.Information)
                                Exit Sub
                            End If
                        End If
                    ElseIf dRow("AUD").ToString = "U" AndAlso dRow("fileuniquename").ToString <> "" Then
                        Dim strFileName As String = dRow("fileuniquename").ToString
                        Dim intScanattachrefid As Integer = CInt(dRow("scanattachrefid"))
                        mstrFolderName = (From p In mdsDoc.Tables("Docs") Where (CInt(p.Item("Id").ToString) = intScanattachrefid) Select (p.Item("Name").ToString)).FirstOrDefault

                        If blnIsIISInstalled Then
                            Dim strPath As String = ConfigParameter._Object._ArutiSelfServiceURL
                            If Strings.Right(strPath, 1) <> "/" AndAlso Strings.Right(strPath, 1) <> "\" Then strPath += "/"
                            strPath += "uploadimage/" & mstrFolderName & "/" + strFileName


                            'Shani(24-Aug-2015) -- Start
                            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                            'If clsFileUploadDownload.UpdateFile(CStr(dRow("filepath")), strPath, mstrFolderName, strError) = False Then
                            If clsFileUploadDownload.UpdateFile(CStr(dRow("filepath")), strPath, mstrFolderName, strError, ConfigParameter._Object._ArutiSelfServiceURL) = False Then
                                'Shani(24-Aug-2015) -- End

                                eZeeMsgBox.Show(strError, enMsgBoxStyle.Information)
                                Exit Sub
                            Else
                                dRow("filepath") = strPath
                                dRow.AcceptChanges()
                            End If
                        Else
                            If Directory.Exists(ConfigParameter._Object._Document_Path) Then
                                Dim strDocLocalPath As String = ConfigParameter._Object._Document_Path & "\" & mstrFolderName & "\" & strFileName
                                If Directory.Exists(ConfigParameter._Object._Document_Path & "\" & mstrFolderName) = False Then
                                    Directory.CreateDirectory(ConfigParameter._Object._Document_Path & "\" & mstrFolderName)
                                End If
                                File.Move(CStr(dRow("filepath")), strDocLocalPath)

                                dRow("filepath") = strDocLocalPath
                                dRow.AcceptChanges()
                            Else
                                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 12, "Configuration Path does not Exist."), enMsgBoxStyle.Information)
                                Exit Sub
                            End If
                        End If
                    ElseIf dRow("AUD").ToString = "D" AndAlso dRow("fileuniquename").ToString <> "" Then
                        Dim strFileName As String = dRow("fileuniquename").ToString
                        If blnIsIISInstalled Then

                            'Shani(24-Aug-2015) -- Start
                            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                            'If clsFileUploadDownload.DeleteFile(dRow("filepath").ToString, strFileName, mstrFolderName, strError) = False Then
                            'Hemant [8-April-2019] -- Start
                            'If clsFileUploadDownload.DeleteFile(dRow("filepath").ToString, strFileName, mstrFolderName, strError, ConfigParameter._Object._ArutiSelfServiceURL) = False Then
                            If clsFileUploadDownload.DeleteFile(dRow("filepath").ToString, strFileName, mstrFolderName, strError, ConfigParameter._Object._ArutiSelfServiceURL, ConfigParameter._Object._Companyunkid) = False Then
                                'Hemant [8-April-2019] -- End
                                'Shani(24-Aug-2015) -- End

                                eZeeMsgBox.Show(strError, enMsgBoxStyle.Information)
                                Exit Sub
                            End If
                        Else
                            If Directory.Exists(ConfigParameter._Object._Document_Path) Then
                                Dim strDocLocalPath As String = ConfigParameter._Object._Document_Path & "\" & mstrFolderName & "\" & strFileName
                                If File.Exists(strDocLocalPath) Then
                                    File.Delete(strDocLocalPath)
                                End If
                            Else
                                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 12, "Configuration Path does not Exist."), enMsgBoxStyle.Information)
                                Exit Sub
                            End If
                        End If
                    End If
                Next
                'SHANI (16 JUL 2015) -- End 

                objDocument._Datatable = mdtTran

                'S.SANDEEP [28-May-2018] -- START
                'ISSUE/ENHANCEMENT : {Audit Trails} 
                objDocument._FormName = mstrModuleName
                objDocument._LoginEmployeeunkid = 0
                objDocument._ClientIP = getIP()
                objDocument._HostName = getHostName()
                objDocument._FromWeb = False
                objDocument._AuditUserId = User._Object._Userunkid
objDocument._CompanyUnkid = Company._Object._Companyunkid
                objDocument._AuditDate = ConfigParameter._Object._CurrentDateAndTime
                'S.SANDEEP [28-May-2018] -- END

                objDocument.InsertUpdateDelete_Documents()


                'Anjan [ 05 Feb 2013 ] -- Start
                'ENHANCEMENT : TRA CHANGES
                If objDocument._Message <> "" Then
                    eZeeMsgBox.Show(objDocument._Message, enMsgBoxStyle.Information)
                    Exit Try
                End If
                'Anjan [ 05 Feb 2013 ] -- End

            End If

            'Pinkal (07-May-2015) -- End

            'Pinkal (07-May-2015) -- Start
            'Leave Enhancement - CHANGES IN LEAVE FORM FOR SCAN ATTACH DOCUMENTS.
            mblnCancel = False
            'Pinkal (07-May-2015) -- End

            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnSave_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub btnEdit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnEdit.Click
        Try
            If lvScanAttachment.SelectedItems.Count <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 7, "Please select atleast one information in order to edit."), enMsgBoxStyle.Information)
                Exit Sub
            End If

            'Nilay (03-Dec-2015) -- Start
            'ENHANCEMENT for CCBRT & SL - Requested by Andrew Muga.
            'If objDocument.IsExist(mintRefModuleId, CInt(cboDocumentType.SelectedValue), lvScanAttachment.SelectedItems(0).SubItems(colhFileName.Index).Text, CInt(cboEmployee.SelectedValue), lvScanAttachment.SelectedItems(0).Tag) = False Then
            '    'Dim f As New System.IO.FileInfo(lvScanAttachment.SelectedItems(0).SubItems(objcolhFullPath.Index).Text)
            '    'Call AddRow(f, lvScanAttachment.SelectedItems(0).SubItems(objcolhFullPath.Index).Text)
            '    Dim dtTemp() As DataRow = mdtTran.Select("scanattachtranunkid = '" & lvScanAttachment.SelectedItems(0).Tag & "'")
            '    If dtTemp.Length > 0 Then
            '        Call EditRow(dtTemp)
            '    End If
            '    Call FillList()
            'Else
            '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 6, "Selected information is already present for particular employee."), enMsgBoxStyle.Information)
            '    Exit Sub
            'End If
            If mblnIsEmpForm = True AndAlso mintEmployeeId <= 0 Then
                Dim xRow() As DataRow = mdtTran.Select("scanattachrefid = " & CInt(cboDocumentType.SelectedValue) & " AND modulerefid = " & mintRefModuleId & " AND filename = " & lvScanAttachment.SelectedItems(0).SubItems(colhFileName.Index).Text & " ")
                If xRow.Length <= 0 Then
                    Dim dtTemp() As DataRow = mdtTran.Select("scanattachtranunkid = '" & lvScanAttachment.SelectedItems(0).Tag & "'")
                    If dtTemp.Length > 0 Then
                        Call EditRow(dtTemp)
                    End If
                    Call FillList()
                Else
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 6, "Selected information is already present for particular employee."), enMsgBoxStyle.Information)
                    Exit Sub
                End If
            Else
                If objDocument.IsExist(mintRefModuleId, CInt(cboDocumentType.SelectedValue), lvScanAttachment.SelectedItems(0).SubItems(colhFileName.Index).Text, CInt(cboEmployee.SelectedValue), lvScanAttachment.SelectedItems(0).Tag) = False Then
                    Dim dtTemp() As DataRow = mdtTran.Select("scanattachtranunkid = '" & lvScanAttachment.SelectedItems(0).Tag & "'")
                    If dtTemp.Length > 0 Then
                        Call EditRow(dtTemp)
                    End If
                    Call FillList()
                Else
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 6, "Selected information is already present for particular employee."), enMsgBoxStyle.Information)
                    Exit Sub
                End If
            End If
            'Nilay (03-Dec-2015) -- End



        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnEdit_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    'Nilay (03-Dec-2015) -- Start
    'ENHANCEMENT for CCBRT & SL - Requested by Andrew Muga.
    Private Sub btnPreview_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnPreview.Click
        Try

            'S.SANDEEP [24 MAY 2016] -- START
            'ENHANCEMENT : DISCIPLINE MAKE-OVER {BY ANDREW}
            'Dim frm As New frmScanAttachmentList
            'Dim objEmp As New clsEmployee_Master

            ''Nilay (25-Feb-2016) -- Start
            ''ISSUE - Scan Attachement
            'objEmp._Employeeunkid(eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate).Date) = mintEmployeeId
            ''Nilay (25-Feb-2016) -- End

            'frm.displayDialog(enImg_Email_RefId.Employee_Module, mintEmployeeId, , True, objEmp._Isapproved)


            'S.SANDEEP |16-MAY-2019| -- START
            'ISSUE/ENHANCEMENT : {Attachment Preview} Leave UAT
            'If mintScanRefId = enScanAttactRefId.DISCIPLINES Then
            '    If mdtTran IsNot Nothing AndAlso mdtTran.Rows.Count > 0 Then
            '        Dim frm As New frmPreviewDocuments
            '        frm._dtTable = mdtTran
            '        frm.displayDialog("", True)
            '    End If
            'Else
            '    Dim frm As New frmScanAttachmentList
            '    Dim objEmp As New clsEmployee_Master
            '    objEmp._Employeeunkid(eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate).Date) = mintEmployeeId
            '    frm.displayDialog(enImg_Email_RefId.Employee_Module, mintEmployeeId, , True, objEmp._Isapproved)
            'End If
            If lvScanAttachment.Items.Count <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 11, "Please add atleast one document."), enMsgBoxStyle.Information)
                Exit Sub
            End If
                If mdtTran IsNot Nothing AndAlso mdtTran.Rows.Count > 0 Then
                Dim flList As New List(Of String)
                If mdtTran.AsEnumerable().Where(Function(x) x.Field(Of String)("AUD") <> "D").Count <= 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 11, "Please add atleast one document."), enMsgBoxStyle.Information)
                    Exit Sub
                End If
                Dim mblnIsSelfServiceInstall As Boolean = IsSelfServiceExist()
                Dim dtData As DataTable = mdtTran.Select("AUD <> 'D'").CopyToDataTable()
                If dtData.Columns.Contains("error") = False Then dtData.Columns.Add("error", GetType(System.String))
                If dtData.Columns.Contains("temppath") = False Then dtData.Columns.Add("temppath", Type.GetType("System.String"))
                Dim xRow() As DataRow = Nothing
                xRow = dtData.Select("filepath = ''")
                If xRow IsNot Nothing AndAlso xRow.Length > 0 Then
                    For idx As Integer = 0 To xRow.Length - 1
                        If IO.File.Exists(xRow(idx)("orgfilepath")) Then
                            flList.Add(xRow(idx)("orgfilepath"))
                        Else
                            xRow(idx)("error") = Language.getMessage("frmScanAttachmentList", 14, "File not found.")
                        End If
                    Next
                End If
                xRow = dtData.Select("filepath <> ''")
                Dim strLocalPath As String = "" : Dim strError As String = ""
                For idx As Integer = 0 To xRow.Length - 1
                    If IsDBNull(xRow(idx)("file_data")) = False Then
                        strLocalPath = Path.GetTempPath() & DateTime.Now.ToString("MMyyyyddsshhmm") & "_" & xRow(idx)("fileuniquename").ToString
                        Dim ms As New MemoryStream(CType(xRow(idx)("file_data"), Byte()))
                        Dim fs As New FileStream(strLocalPath, FileMode.Create)
                        ms.WriteTo(fs)
                        ms.Close()
                        fs.Close()
                        fs.Dispose()
                        If strLocalPath <> "" Then
                            flList.Add(strLocalPath)
                        End If
                    ElseIf IsDBNull(xRow(idx)("file_data")) = True Then
                        If mblnIsSelfServiceInstall Then
                            If xRow(idx)("filepath").ToString().StartsWith("http://") Or xRow(idx)("filepath").ToString().StartsWith("https://") Then
                                If xRow(idx)("temppath").ToString.Trim <> "" Then
                                    If System.IO.File.Exists(xRow(idx)("temppath").ToString.Trim) Then
                                        strLocalPath = xRow(idx)("temppath").ToString.Trim
                                    End If
                                Else
                                    strLocalPath = CreateImage(xRow(idx), strError)
                                    xRow(idx)("error") = strError
                                End If
                            Else
                                strLocalPath = xRow(idx)("filepath").ToString()
                                If IO.File.Exists(strLocalPath) = False Then
                                    xRow(idx)("error") = Language.getMessage("frmScanAttachmentList", 14, "File not found.")
                                End If
                End If
            Else
                            strLocalPath = xRow(idx)("filepath").ToString()
                            If IO.File.Exists(strLocalPath) = False Then
                                xRow(idx)("error") = Language.getMessage("frmScanAttachmentList", 14, "File not found.")
                            End If
                        End If
                        If strLocalPath <> "" Then
                            flList.Add(strLocalPath)
                        End If
                    End If
                Next

                'dtData.AcceptChanges()
                'Dim objValid As New frmCommonValidationList
                'If dtData.AsEnumerable().Where(Function(x) x.Field(Of String)("error") <> "").Count() > 0 Then
                '    Dim eRData As DataTable = dtData.DefaultView.ToTable(True, "error", "filename")
                '    objValid.displayDialog(False, Language.getMessage("frmScanAttachmentList", 15, "Download Issue(s)."), eRData)
                '    Exit Try
                'End If

                If flList.Count > 0 Then
                    Dim sfd As New SaveFileDialog() : sfd.Filter = "Zip File (.zip)|*.zip"
                    If sfd.ShowDialog() = DialogResult.OK Then
                        Dim fl As New System.IO.FileInfo(sfd.FileName)
                        AddFileToZip(fl.FullName, flList, CompressionOption.Normal)
                        eZeeMsgBox.Show(Language.getMessage("frmScanAttachmentList", 13, "File(s) downloaded successfully to the selected location."), enMsgBoxStyle.Information)
                    End If
            End If
            End If
            'S.SANDEEP |16-MAY-2019| -- END


            'S.SANDEEP [24 MAY 2016] -- END

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnPreview_Click", mstrModuleName)
        End Try
    End Sub
    'Nilay (03-Dec-2015) -- End


#End Region

#Region " Private Functions "

    'S.SANDEEP |26-APR-2019| -- START
    Private Sub FillList(Optional ByVal strFilterString As String = "")
        'Private Sub FillList()
        'S.SANDEEP |26-APR-2019| -- END

        'S.SANDEEP [24 MAY 2016] -- Start       
        Dim intAsscatedValue As Integer = cboAssocatedValue.SelectedValue
        Dim intDocumnetType As Integer = cboDocumentType.SelectedValue
        'S.SANDEEP [24 MAY 2016] -- End
        Try
            RemoveHandler cboDocumentType.SelectedIndexChanged, AddressOf cboDocumentType_SelectedIndexChanged
            lvScanAttachment.Items.Clear()
            Dim dTemp() As DataRow = Nothing
            Dim lvItem As ListViewItem

            'S.SANDEEP |26-APR-2019| -- START
            'For Each dtRow As DataRow In mdtTran.Rows
            Dim dtFTable As DataTable = Nothing
            If strFilterString.Trim.Length > 0 Then
                dtFTable = New DataView(mdtTran, strFilterString, "", DataViewRowState.CurrentRows).ToTable()
            Else
                If mdtSource IsNot Nothing Then
                    Dim strFilter As String = "pguid = '" & DirectCast(cboAssocatedValue.SelectedItem, DataRowView).Item("guid").ToString() & "' "
                    dtFTable = New DataView(mdtTran, strFilter, "", DataViewRowState.CurrentRows).ToTable()
                Else
                    dtFTable = mdtTran
                End If
            End If
            For Each dtRow As DataRow In dtFTable.Rows
                'S.SANDEEP |26-APR-2019| -- END
                If CStr(IIf(IsDBNull(dtRow.Item("AUD")), "A", dtRow.Item("AUD"))) <> "D" Then
                    lvItem = New ListViewItem

                    'Nilay (03-Dec-2015) -- Start
                    'ENHANCEMENT for CCBRT & SL - Requested by Andrew Muga.
                    'If CInt(dtRow.Item("employeeunkid")) > 0 Then
                    '    Select Case mintRefModuleId
                    '        Case enImg_Email_RefId.Employee_Module, enImg_Email_RefId.Discipline_Module, enImg_Email_RefId.Leave_Module, enImg_Email_RefId.Medical_Module, enImg_Email_RefId.Training_Module
                    '            dTemp = CType(cboEmployee.DataSource, DataTable).Select("employeeunkid = '" & CInt(dtRow.Item("employeeunkid")) & "'")
                    '        Case enImg_Email_RefId.Applicant_Module
                    '            dTemp = CType(cboEmployee.DataSource, DataTable).Select("applicantunkid = '" & CInt(dtRow.Item("employeeunkid")) & "'")
                    '    End Select
                    'Else
                    '    Select Case mintRefModuleId
                    '        Case enImg_Email_RefId.Employee_Module
                    '            dTemp = CType(cboEmployee.DataSource, DataTable).Select("employeeunkid = '" & CInt(cboEmployee.SelectedValue) & "'")
                    '        Case enImg_Email_RefId.Applicant_Module
                    '            dTemp = CType(cboEmployee.DataSource, DataTable).Select("applicantunkid = '" & CInt(cboEmployee.SelectedValue) & "'")
                    '    End Select
                    'End If

                    'If dTemp.Length > 0 Then
                    '    Select Case mintRefModuleId
                    '        Case enImg_Email_RefId.Employee_Module, enImg_Email_RefId.Discipline_Module, enImg_Email_RefId.Leave_Module, enImg_Email_RefId.Medical_Module, enImg_Email_RefId.Training_Module
                    '            lvItem.Text = dTemp(0)("employeecode").ToString '0
                    '            lvItem.SubItems.Add(dTemp(0)("employeename").ToString) '1
                    '        Case enImg_Email_RefId.Applicant_Module
                    '            lvItem.Text = dTemp(0)("applicant_code").ToString '0
                    '            lvItem.SubItems.Add(dTemp(0)("applicantname").ToString) '1
                    '    End Select
                    'End If


                    If mblnIsEmpForm = True AndAlso mintEmployeeId <= 0 Then
                        lvItem.Text = "" '0
                        lvItem.SubItems.Add(mstrEmpName) '1
                        GoTo employeelistgoto
                    End If
                    If CInt(dtRow.Item("employeeunkid")) > 0 Then
                        Select Case mintRefModuleId
                            Case enImg_Email_RefId.Employee_Module, enImg_Email_RefId.Discipline_Module, enImg_Email_RefId.Leave_Module, enImg_Email_RefId.Medical_Module, enImg_Email_RefId.Training_Module, enImg_Email_RefId.Claim_Request, enImg_Email_RefId.Appraisal_Module 'S.SANDEEP [29-NOV-2017] -- START {REF-ID # 136 [enImg_Email_RefId.Appraisal_Module]} -- END
                                dTemp = CType(cboEmployee.DataSource, DataTable).Select("employeeunkid = '" & CInt(dtRow.Item("employeeunkid")) & "'")
                            Case enImg_Email_RefId.Applicant_Module
                                dTemp = CType(cboEmployee.DataSource, DataTable).Select("applicantunkid = '" & CInt(dtRow.Item("employeeunkid")) & "'")
                        End Select
                    Else
                        Select Case mintRefModuleId
                            Case enImg_Email_RefId.Employee_Module
                                dTemp = CType(cboEmployee.DataSource, DataTable).Select("employeeunkid = '" & CInt(cboEmployee.SelectedValue) & "'")
                            Case enImg_Email_RefId.Applicant_Module
                                dTemp = CType(cboEmployee.DataSource, DataTable).Select("applicantunkid = '" & CInt(cboEmployee.SelectedValue) & "'")
                        End Select
                    End If

                    'Gajanan [02-SEP-2019] -- Start      
                    'Enhancements: ENHANCEMENTS IN ONLINE RECRUITMENT FOR NMB
                    'If dTemp.Length > 0 Then
                    If IsNothing(dTemp) = False AndAlso dTemp.Length > 0 Then
                        'Gajanan [02-SEP-2019] -- End
                        Select Case mintRefModuleId
                            Case enImg_Email_RefId.Employee_Module, enImg_Email_RefId.Discipline_Module, enImg_Email_RefId.Leave_Module, enImg_Email_RefId.Medical_Module, enImg_Email_RefId.Training_Module, enImg_Email_RefId.Claim_Request, enImg_Email_RefId.Appraisal_Module 'S.SANDEEP [29-NOV-2017] -- START {REF-ID # 136 [enImg_Email_RefId.Appraisal_Module]} -- END
                                lvItem.Text = dTemp(0)("employeecode").ToString '0
                                lvItem.SubItems.Add(dTemp(0)("employeename").ToString) '1
                            Case enImg_Email_RefId.Applicant_Module
                                lvItem.Text = dTemp(0)("applicant_code").ToString '0
                                lvItem.SubItems.Add(dTemp(0)("applicantname").ToString) '1
                        End Select
                    End If
employeelistgoto:
                    'Nilay (03-Dec-2015) -- End

                    If CInt(dtRow.Item("documentunkid")) > 0 Then
                        cboDocument.SelectedValue = CInt(dtRow.Item("documentunkid"))
                        lvItem.SubItems.Add(cboDocument.Text.ToString) '2
                    Else
                        lvItem.SubItems.Add("") '2
                    End If

                    lvItem.SubItems.Add(dtRow.Item("filename").ToString) '3
                    'S.SANDEEP [24 MAY 2016] -- START
                    'ENHANCEMENT : DISCIPLINE MAKE-OVER {BY ANDREW}
                    'lvItem.SubItems.Add(dtRow.Item("valuename").ToString) '4
                    If dtRow.Item("valuename").ToString.Trim.Trim.Length > 0 Then
                        lvItem.SubItems.Add(dtRow.Item("valuename").ToString) '4
                    Else
                        cboAssocatedValue.SelectedValue = dtRow.Item("transactionunkid").ToString
                        lvItem.SubItems.Add(cboAssocatedValue.Text) '4
                    End If
                    'S.SANDEEP [24 MAY 2016] -- END


                    If CInt(dtRow.Item("scanattachrefid")) > 0 Then
                        cboDocumentType.SelectedValue = CInt(dtRow.Item("scanattachrefid"))
                    End If
                    lvItem.SubItems.Add(cboDocumentType.Text.ToString) '5

                    'Select Case mintRefModuleId
                    '    Case enImg_Email_RefId.Employee_Module
                    '        lvItem.SubItems.Add(dtRow.Item("employeeunkid").ToString) '6
                    '        lvItem.SubItems.Add("-1") '7
                    '    Case enImg_Email_RefId.Applicant_Module
                    '        lvItem.SubItems.Add("-1") '6
                    '        lvItem.SubItems.Add(dtRow.Item("employeeunkid").ToString) '7
                    'End Select
                    'lvItem.SubItems.Add(dtRow.Item("orgfilepath").ToString) '8
                    'lvItem.SubItems.Add(dtRow.Item("GUID").ToString) '9
                    lvItem.SubItems.Add(dtRow.Item("GUID").ToString) '6
                    Select Case mintRefModuleId
                        Case enImg_Email_RefId.Employee_Module
                            lvItem.SubItems.Add(dtRow.Item("employeeunkid").ToString) '7
                            lvItem.SubItems.Add("-1") '8
                        Case enImg_Email_RefId.Applicant_Module
                            lvItem.SubItems.Add("-1") '7
                            lvItem.SubItems.Add(dtRow.Item("employeeunkid").ToString) '8
                    End Select

                    lvItem.SubItems.Add(dtRow.Item("orgfilepath").ToString) '9
                    lvItem.SubItems.Add(dtRow.Item("transactionunkid").ToString) '10
                    lvItem.SubItems.Add(dtRow.Item("documentunkid").ToString) '11
                    lvItem.Tag = dtRow.Item("scanattachtranunkid")
                    lvScanAttachment.Items.Add(lvItem)
                End If
            Next

            'Pinkal (06-Jan-2016) -- Start
            'Enhancement - Working on Changes in SS for Leave Module.
            If mAction <> enAction.EDIT_ONE Then
                Call ResetValue()
            End If
            'Pinkal (06-Jan-2016) -- End



            lvScanAttachment.GridLines = False
            lvScanAttachment.GroupingColumn = objcolhDocType
            lvScanAttachment.DisplayGroups(True)

            If lvScanAttachment.Items.Count > 0 Then
                colhDocument.Width = 160 - 20
            Else
                colhDocument.Width = 160
            End If
            'S.SANDEEP [24 MAY 2016] -- START
            'ENHANCEMENT : DISCIPLINE MAKE-OVER {BY ANDREW}
            cboDocumentType.SelectedValue = intDocumnetType
            cboAssocatedValue.SelectedValue = intAsscatedValue
            'S.SANDEEP [24 MAY 2016] -- END

            AddHandler cboDocumentType.SelectedIndexChanged, AddressOf cboDocumentType_SelectedIndexChanged

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillList", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub ResetValue()
        Try
            'cboEmployee.SelectedValue = 0
            cboDocument.SelectedValue = 0
            cboDocumentType.SelectedValue = 0
            cboAssocatedValue.SelectedValue = 0
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "ResetValue", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub FillCombo()
        Dim dsCombo As New DataSet
        Dim objCMaster As New clsCommon_Master
        Try
            Select Case mintRefModuleId
                Case enImg_Email_RefId.Employee_Module, _
                     enImg_Email_RefId.Dependants_Beneficiaries, _
                     enImg_Email_RefId.Leave_Module, _
                     enImg_Email_RefId.Payroll_Module, _
                     enImg_Email_RefId.Training_Module, enImg_Email_RefId.Discipline_Module, enImg_Email_RefId.Claim_Request, _
                     enImg_Email_RefId.Appraisal_Module
                    'S.SANDEEP [29-NOV-2017] -- START {REF-ID # 136 [enImg_Email_RefId.Appraisal_Module]} -- END

                    'Shani (20-Aug-2016) --Add-->[Claim_Request]-->'Enhancement - Add Scan Attchment in Claim Request Module given by Matthew

                    'Nilay (03-Dec-2015) -- Start
                    'ENHANCEMENT for CCBRT & SL - Requested by Andrew Muga.
                    If mblnIsEmpForm = True AndAlso mintEmployeeId <= 0 Then
                        cboEmployee.DropDownStyle = ComboBoxStyle.Simple
                        cboEmployee.Text = mstrEmpName
                        GoTo Employeegoto
                    End If
                    'Nilay (03-Dec-2015) -- End

                    Dim objEMaster As New clsEmployee_Master
                    'Anjan (17 Apr 2012)-Start
                    'ENHANCEMENT : TRA COMMENTS on Andrew sir's Request
                    'dsCombo = objEMaster.GetEmployeeList("List", True, Not ConfigParameter._Object._IsIncludeInactiveEmp)


                    'Pinkal (03-Jan-2014) -- Start
                    'Enhancement : Oman Changes

                    'Shani(24-Aug-2015) -- Start
                    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS

                    'If mstrEmployeeIds.Trim.Length > 0 Then
                    '    If ConfigParameter._Object._IsIncludeInactiveEmp = False Then
                    '        dsCombo = objEMaster.GetEmployeeList("List", False, , , , , , , , , , , , , eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), , , , , "employeeunkid IN (" & mstrEmployeeIds & ")")
                    '    Else
                    '        dsCombo = objEMaster.GetEmployeeList("List", False, , , , , , , , , , , , , ConfigParameter._Object._CurrentDateAndTime, ConfigParameter._Object._CurrentDateAndTime, , , , , "employeeunkid IN (" & mstrEmployeeIds & ")")
                    '    End If
                    'Else
                    '    If ConfigParameter._Object._IsIncludeInactiveEmp = False Then
                    '        dsCombo = objEMaster.GetEmployeeList("List", True, , , , , , , , , , , , , eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate))
                    '    Else
                    '        dsCombo = objEMaster.GetEmployeeList("List", True, , , , , , , , , , , , , ConfigParameter._Object._CurrentDateAndTime, ConfigParameter._Object._CurrentDateAndTime)
                    '    End If
                    'End If
                    Dim strEmployeeIds As String = ""

                    'Nilay (03-Dec-2015) -- Start
                    'ENHANCEMENT for CCBRT & SL - Requested by Andrew Muga.
                    'If mstrEmployeeIds.Trim.Length > 0 Then
                    '    strEmployeeIds = " hremployee_master.employeeunkid IN (" & mstrEmployeeIds & ") "
                    'End If
                    Dim blnselect As Boolean = True
                    If mintEmployeeId > 0 Then
                        blnselect = False
                        strEmployeeIds = " hremployee_master.employeeunkid IN (" & mintEmployeeId & ") "
                    End If

                    'dsCombo = objEMaster.GetEmployeeList(FinancialYear._Object._DatabaseName, _
                    '                                     User._Object._Userunkid, _
                    '                                     FinancialYear._Object._YearUnkid, _
                    '                                     Company._Object._Companyunkid, _
                    '                                     FinancialYear._Object._Database_Start_Date.Date, _
                    '                                     FinancialYear._Object._Database_End_Date.Date, _
                    '                                     ConfigParameter._Object._UserAccessModeSetting, _
                    '                                     True, ConfigParameter._Object._IsIncludeInactiveEmp, "List", blnselect, _
                    '                                     , , , , , , , , , , , , , , strEmployeeIds)

                    'S.SANDEEP [20-JUN-2018] -- Start
                    'Enhancement - Implementing Employee Approver Flow For NMB .
                    Dim mblnAddApprovalCondition As Boolean = True
                    If ConfigParameter._Object._PendingEmployeeScreenIDs.Trim.Length > 0 Then
                        If ConfigParameter._Object._PendingEmployeeScreenIDs.ToString.Split(CChar(",")).Contains(CStr(CInt(enScreenName.frmLeaveFrequencyList))) Then
                            mblnIsApproved = False
                            mblnAddApprovalCondition = False
                        End If
                    End If


                    'dsCombo = objEMaster.GetEmployeeList(FinancialYear._Object._DatabaseName, _
                    '                                     User._Object._Userunkid, _
                    '                                     FinancialYear._Object._YearUnkid, _
                    '                                     Company._Object._Companyunkid, _
                    '                          FinancialYear._Object._Database_Start_Date.Date, _
                    '                          FinancialYear._Object._Database_End_Date.Date, _
                    '                                     ConfigParameter._Object._UserAccessModeSetting, _
                    '                                     mblnIsApproved, ConfigParameter._Object._IsIncludeInactiveEmp, "List", blnselect, _
                    '                                     , , , , , , , , , , , , , , strEmployeeIds)


                    dsCombo = objEMaster.GetEmployeeList(FinancialYear._Object._DatabaseName, _
                                                         User._Object._Userunkid, _
                                                         FinancialYear._Object._YearUnkid, _
                                                         Company._Object._Companyunkid, _
                                              FinancialYear._Object._Database_Start_Date.Date, _
                                              FinancialYear._Object._Database_End_Date.Date, _
                                                         ConfigParameter._Object._UserAccessModeSetting, _
                                                         mblnIsApproved, ConfigParameter._Object._IsIncludeInactiveEmp, "List", blnselect, _
                                                                                   , , , , , , , , , , , , , , strEmployeeIds, False, True, mblnAddApprovalCondition)

                    'S.SANDEEP [20-JUN-2018] -- End


                    'Nilay (03-Dec-2015) -- End

                    'Shani(24-Aug-2015) -- End

                    'Pinkal (03-Jan-2014) -- End

                    'Anjan (17 Apr 2012)-End 

                    With cboEmployee
                        .ValueMember = "employeeunkid"
                        .DisplayMember = "employeename"
                        .DataSource = dsCombo.Tables("List")
                    End With

                Case enImg_Email_RefId.Applicant_Module

                    Dim objAMaster As New clsApplicant_master

                    'Nilay (03-Dec-2015) -- Start
                    'ENHANCEMENT for CCBRT & SL - Requested by Andrew Muga.
                    If mAction = enAction.EDIT_ONE Then

                        dsCombo = objAMaster.GetApplicantList("List", False, mintEmployeeId)

                        With cboEmployee
                            .ValueMember = "applicantunkid"
                            .DisplayMember = "applicantname"
                            .DataSource = dsCombo.Tables("List")
                        End With
                        cboEmployee.Enabled = False
                        objbtnSearch.Enabled = False
                        'Nilay (03-Dec-2015) -- End
                    Else
                        dsCombo = objAMaster.GetApplicantList("List", True)

                        With cboEmployee
                            .ValueMember = "applicantunkid"
                            .DisplayMember = "applicantname"
                            .DataSource = dsCombo.Tables("List")
                        End With
                    End If

            End Select
Employeegoto:
            dsCombo = objCMaster.getComboList(clsCommon_Master.enCommonMaster.ATTACHMENT_TYPES, True, "List")
            With cboDocument
                .ValueMember = "masterunkid"
                .DisplayMember = "name"
                .DataSource = dsCombo.Tables("List")
            End With

            'S.SANDEEP |26-APR-2019| -- START
            'dsCombo = objDocument.GetDocType()
            dsCombo = objDocument.GetDocType(Company._Object._Companyunkid, mblnAddEmployeeDataApprovalCondition)
            'S.SANDEEP |26-APR-2019| -- END


            'Pinkal (03-Jan-2014) -- Start
            'Enhancement : Oman Changes
            Dim dtRefID As DataTable = Nothing

            If mintScanRefId > 0 Then
                dtRefID = New DataView(dsCombo.Tables(0), "Id = " & mintScanRefId, "", DataViewRowState.CurrentRows).ToTable()
            Else
                'Shani (20-Aug-2016) -- Start
                'Enhancement - Add Scan Attchment in Claim Request Module given by Matthew
                'dtRefID = dsCombo.Tables(0)
                Dim strFilter As String = enScanAttactRefId.DISCIPLINES & "," & enScanAttactRefId.CLAIM_REQUEST
                dtRefID = New DataView(dsCombo.Tables(0), "Id NOT IN (" & strFilter & ")", "", DataViewRowState.CurrentRows).ToTable()
                'Shani (20-Aug-2016) -- End
            End If

            With cboDocumentType
                .ValueMember = "Id"
                .DisplayMember = "Name"
                .DataSource = dtRefID
                .SelectedValue = mintScanRefId 'S.SANDEEP [24 MAY 2016] -- START -- END
            End With

            'Pinkal (03-Jan-2014) -- End

            'Sohail (04 Jul 2019) -- Start
            'PACT Enhancement - Support Issue Id # 3955 - 76.1 - Check box option "Mandatory Attachment for Recruitment Portal" in common master for attachment types (Setting on aruti configuration to set Curriculam Vitae and Cover Letter attachment mandatory).
            If mintScanRefId > 0 Then Call cboDocumentType_SelectedIndexChanged(cboDocumentType, New System.EventArgs)
            'Sohail (04 Jul 2019) -- End

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        Finally
            dsCombo.Dispose() : objCMaster = Nothing
        End Try
    End Sub

    Private Sub SetColor()
        Try
            cboDocument.BackColor = GUI.ColorComp
            cboDocumentType.BackColor = GUI.ColorComp
            cboEmployee.BackColor = GUI.ColorComp
            cboAssocatedValue.BackColor = GUI.ColorComp
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetColor", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Function IsDataValid() As Boolean
        Try

            'Nilay (03-Dec-2015) -- Start
            'ENHANCEMENT for CCBRT & SL - Requested by Andrew Muga.
            'If CInt(cboEmployee.SelectedValue) <= 0 Then
            '    Select Case mintRefModuleId
            '        Case enImg_Email_RefId.Employee_Module
            '            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Employee is mandatory information. Please select Employee to continue."), enMsgBoxStyle.Information)
            '            cboEmployee.Focus()
            '        Case enImg_Email_RefId.Applicant_Module
            '            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Applicant is mandatory information. Please select Applicant to continue."), enMsgBoxStyle.Information)
            '            cboEmployee.Focus()
            '    End Select
            '    Return False
            'End If
            If mintEmployeeId > 0 Then
                If CInt(cboEmployee.SelectedValue) <= 0 Then
                    Select Case mintRefModuleId
                        Case enImg_Email_RefId.Employee_Module
                            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Employee is mandatory information. Please select Employee to continue."), enMsgBoxStyle.Information)
                            cboEmployee.Focus()
                        Case enImg_Email_RefId.Applicant_Module
                            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Applicant is mandatory information. Please select Applicant to continue."), enMsgBoxStyle.Information)
                            cboEmployee.Focus()
                    End Select
                    Return False
                End If
            End If
            'Nilay (03-Dec-2015) -- End




            If CInt(cboDocument.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Document is mandatory information. Please select Document to continue."), enMsgBoxStyle.Information)
                cboDocument.Focus()
                Return False
            End If

            If CInt(cboDocumentType.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Document Type is mandatory information. Please select Document Type to continue."), enMsgBoxStyle.Information)
                cboDocumentType.Focus()
                Return False
            End If



            'Pinkal (07-May-2015) -- Start
            'Leave Enhancement - CHANGES IN LEAVE FORM FOR SCAN ATTACH DOCUMENTS.
            If mblnIsOnAddTransaction = False Then
                If cboAssocatedValue.Visible = True Then
                    'Sohail (22 Apr 2015) -- Start
                    'Enhancement - Providing qualification attachment, certificate no., achivement, email confirmation in online recruitmenmt.
                    '*** Make Qualification selection optional as per discussion with Andrew on Skype on 02-May-2015.
                    'If CInt(cboAssocatedValue.SelectedValue) <= 0 Then
                    If CInt(cboAssocatedValue.SelectedValue) <= 0 AndAlso CInt(cboDocumentType.SelectedValue) <> enScanAttactRefId.QUALIFICATIONS Then
                        'Sohail (22 Apr 2015) -- End
                        eZeeMsgBox.Show(cboDocumentType.Text & Language.getMessage(mstrModuleName, 8, " is mandatory information. Please select ") & cboDocumentType.Text & _
                                        Language.getMessage(mstrModuleName, 9, " in order to continue."), enMsgBoxStyle.Information)
                        cboAssocatedValue.Focus()
                        Return False
                    End If
                End If
            End If
            'Pinkal (07-May-2015) -- End

            Return True

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "IsDataValid", mstrModuleName)
        Finally
        End Try
    End Function

    Private Sub AddRow(ByVal f As System.IO.FileInfo, ByVal strfullpath As String)
        Try
            Dim dRow As DataRow = mdtTran.NewRow
            dRow.Item("scanattachtranunkid") = -1
            dRow.Item("documentunkid") = cboDocument.SelectedValue

            'Nilay (03-Dec-2015) -- Start
            'ENHANCEMENT for CCBRT & SL - Requested by Andrew Muga.
            'dRow.Item("employeeunkid") = cboEmployee.SelectedValue
            If mblnIsEmpForm = True Then
                dRow.Item("employeeunkid") = mintEmployeeId
            Else

                'Gajanan [02-SEP-2019] -- Start      
                'Enhancements: ENHANCEMENTS IN ONLINE RECRUITMENT FOR NMB
                'dRow.Item("employeeunkid") = cboEmployee.SelectedValue
                If IsNothing(cboEmployee.DataSource) = False Then
                dRow.Item("employeeunkid") = cboEmployee.SelectedValue
                Else
                    dRow.Item("employeeunkid") = mintEmployeeId
                End If
                'Gajanan [02-SEP-2019] -- End
            End If
            'Nilay (03-Dec-2015) -- End


            dRow.Item("filename") = f.Name
            dRow.Item("scanattachrefid") = cboDocumentType.SelectedValue
            dRow.Item("modulerefid") = mintRefModuleId
            dRow.Item("userunkid") = User._Object._Userunkid
            dRow.Item("AUD") = "A"

            'S.SANDEEP [25 JUL 2016] -- START
            'ENHANCEMENT : DISCIPLINE MAKE-OVER {BY ANDREW}
            'dRow.Item("GUID") = Guid.NewGuid.ToString
            'Anjan (28 Aug 2017) --  Start
            'Issue : for some module forms GUID was not getting changed as that was done for discipline, CR module etc.
            'If mstrTransactionGuidString.Trim.Length <= 0 Then mstrTransactionGuidString = Guid.NewGuid.ToString
            'dRow.Item("GUID") = mstrTransactionGuidString
            Select Case mstrTransactionScreenName.ToUpper
                Case "FRMCLAIMS_REQUESTADDEDIT", "FRMADDEDITCOUNTS", "FRMCHARGECOUNTRESPONSE", "FRMDISCIPLINEFILING"
                    If mstrTransactionGuidString.Trim.Length <= 0 Then mstrTransactionGuidString = Guid.NewGuid.ToString
                    dRow.Item("GUID") = mstrTransactionGuidString
                Case Else
                    dRow.Item("GUID") = Guid.NewGuid.ToString
            End Select
            'Anjan (28 Aug 2017)-- End
            
            'S.SANDEEP [25 JUL 2016] -- START

            dRow.Item("orgfilepath") = strfullpath
            dRow.Item("valuename") = cboAssocatedValue.Text
            Select Case CInt(cboDocumentType.SelectedValue)
                Case enScanAttactRefId.CURRICULAM_VITAE, enScanAttactRefId.OTHERS
                    dRow.Item("transactionunkid") = -1
                Case Else
                    'Sohail (22 Apr 2015) -- Start
                    'Enhancement - Providing qualification attachment, certificate no., achivement, email confirmation in online recruitmenmt.
                    'dRow.Item("transactionunkid") = cboAssocatedValue.SelectedValue
                    If cboAssocatedValue.SelectedValue Is Nothing Then
                        dRow.Item("transactionunkid") = -1
                        dRow.Item("valuename") = ""
                    Else
                        'S.SANDEEP |26-APR-2019| -- START
                        'dRow.Item("transactionunkid") = cboAssocatedValue.SelectedValue
                        If mdtSource Is Nothing Then
                        dRow.Item("transactionunkid") = cboAssocatedValue.SelectedValue
                        Else
                            dRow.Item("transactionunkid") = -1
                        End If
                        'S.SANDEEP |26-APR-2019| -- END
                        If CInt(cboAssocatedValue.SelectedValue) <= 0 Then
                            dRow.Item("valuename") = ""
                        End If
                    End If
                    'Sohail (22 Apr 2015) -- End
            End Select
            dRow.Item("attached_date") = dtpUploadDate.Value
            dRow.Item("doctype") = cboDocumentType.Text
            If ConfigParameter._Object._Document_Path.LastIndexOf("\") = ConfigParameter._Object._Document_Path.Length - 1 Then
                dRow.Item("destfilepath") = ConfigParameter._Object._Document_Path & cboDocumentType.Text & "\" & f.Name
            Else
                dRow.Item("destfilepath") = ConfigParameter._Object._Document_Path & "\" & cboDocumentType.Text & "\" & f.Name
            End If

            'SHANI (16 JUL 2015) -- Start
            'Enhancement : Save all attachments from deskto and SS to IIS Aruti Self Service 
            'Upload Image folder to access those attachemts from Server and 
            'all client machines if ArutiSelfService is installed otherwise save then on Document path
            dRow.Item("filesize") = f.Length / 1024
            'SHANI (16 JUL 2015) -- End 


            'S.SANDEEP [24 MAY 2016] -- START
            'ENHANCEMENT : DISCIPLINE MAKE-OVER {BY ANDREW}
            'dTemp = CType(cboEmployee.DataSource, DataTable).Select("employeeunkid = '" & CInt(dtRow.Item("employeeunkid")) & "'")
            If mintScanRefId = enScanAttactRefId.DISCIPLINES Then
                Dim dTemp = CType(cboEmployee.DataSource, DataTable).AsEnumerable().Where(Function(x) x.Field(Of Integer)("employeeunkid") = CInt(cboEmployee.SelectedValue))
                dRow.Item("code") = dTemp(0)("employeecode").ToString
                dRow.Item("names") = dTemp(0)("employeename").ToString
                dRow.Item("document") = cboDocument.Text.ToString
            End If
            'S.SANDEEP [24 MAY 2016] -- END

            'S.SANDEEP [26 JUL 2016] -- START
            'ENHANCEMENT : DISCIPLINE MAKE-OVER {BY ANDREW}
            dRow.Item("form_name") = mstrTransactionScreenName
            'S.SANDEEP [26 JUL 2016] -- END

            'Pinkal (20-Nov-2018) -- Start
            'Enhancement - Working on P2P Integration for NMB.
            dRow("Documentype") = GetMimeType(strfullpath)
            Dim xDocumentData As Byte() = File.ReadAllBytes(strfullpath)
            dRow("DocBase64Value") = Convert.ToBase64String(xDocumentData)
            Dim objFile As New FileInfo(strfullpath)
            dRow("fileextension") = objFile.Extension.ToString().Replace(".", "")
            objFile = Nothing
            'Pinkal (20-Nov-2018) -- End

            'S.SANDEEP |25-JAN-2019| -- START
            'ISSUE/ENHANCEMENT : {Ref#2540|ARUTI-}
            dRow("file_data") = xDocumentData
            'S.SANDEEP |25-JAN-2019| -- END

            'S.SANDEEP |26-APR-2019| -- START
            If mdtSource IsNot Nothing Then
                dRow("pguid") = DirectCast(cboAssocatedValue.SelectedItem, DataRowView).Item("guid")
            End If
            dRow("isinapproval") = mblnIsEmpDataApprovalIsOn
            'S.SANDEEP |26-APR-2019| -- END

            mdtTran.Rows.Add(dRow)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "AddRow", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub EditRow(ByVal dRow() As DataRow)
        Try
            dRow(0).Item("scanattachtranunkid") = dRow(0).Item("scanattachtranunkid")
            dRow(0).Item("documentunkid") = cboDocument.SelectedValue


            'Nilay (03-Dec-2015) -- Start
            'ENHANCEMENT for CCBRT & SL - Requested by Andrew Muga.
            'dRow(0).Item("employeeunkid") = dRow(0).Item("employeeunkid")
            If mblnIsEmpForm = True Then
                dRow(0).Item("employeeunkid") = mintEmployeeId
            Else
                dRow(0).Item("employeeunkid") = dRow(0).Item("employeeunkid")
            End If
            'Nilay (03-Dec-2015) -- End



            dRow(0).Item("filename") = dRow(0).Item("filename")
            dRow(0).Item("scanattachrefid") = cboDocumentType.SelectedValue
            dRow(0).Item("modulerefid") = dRow(0).Item("modulerefid")
            dRow(0).Item("userunkid") = User._Object._Userunkid
            If dRow(0).Item("AUD").ToString = "" Then
                dRow(0).Item("AUD") = "U"
            End If

            'S.SANDEEP [25 JUL 2016] -- START
            'ENHANCEMENT : DISCIPLINE MAKE-OVER {BY ANDREW}
            'dRow.Item("GUID") = Guid.NewGuid.ToString
            'Anjan (28 Aug 2017) --  Start
            'Issue : for some module forms GUID was not getting changed as that was done for discipline, CR module etc.
            'If mstrTransactionGuidString.Trim.Length <= 0 Then mstrTransactionGuidString = Guid.NewGuid.ToString
            'dRow(0).Item("GUID") = mstrTransactionGuidString
            Select Case mstrTransactionScreenName.ToUpper
                Case "FRMCLAIMS_REQUESTADDEDIT", "FRMADDEDITCOUNTS", "FRMCHARGECOUNTRESPONSE", "FRMDISCIPLINEFILING"
                    If mstrTransactionGuidString.Trim.Length <= 0 Then mstrTransactionGuidString = Guid.NewGuid.ToString
                    dRow(0).Item("GUID") = mstrTransactionGuidString
                Case Else
                    dRow(0).Item("GUID") = Guid.NewGuid.ToString
            End Select
            'Anjan (28 Aug 2017)-- End

           

            'S.SANDEEP [25 JUL 2016] -- START
            dRow(0).Item("orgfilepath") = dRow(0).Item("orgfilepath")
            dRow(0).Item("valuename") = cboAssocatedValue.Text
            Select Case CInt(cboDocumentType.SelectedValue)
                Case enScanAttactRefId.CURRICULAM_VITAE, enScanAttactRefId.OTHERS
                    dRow(0).Item("transactionunkid") = -1
                Case Else
                    'Sohail (22 Apr 2015) -- Start
                    'Enhancement - Providing qualification attachment, certificate no., achivement, email confirmation in online recruitmenmt.
                    'dRow(0).Item("transactionunkid") = cboAssocatedValue.SelectedValue
                    If cboAssocatedValue.SelectedValue Is Nothing Then
                        dRow(0).Item("transactionunkid") = -1
                    Else
                        dRow(0).Item("transactionunkid") = cboAssocatedValue.SelectedValue
                    End If
                    'Sohail (22 Apr 2015) -- End
            End Select
            dRow(0).Item("attached_date") = dtpUploadDate.Value
            dRow(0).Item("doctype") = cboDocumentType.Text
            If ConfigParameter._Object._Document_Path.LastIndexOf("\") = ConfigParameter._Object._Document_Path.Length - 1 Then
                dRow(0).Item("destfilepath") = ConfigParameter._Object._Document_Path & cboDocumentType.Text & "\" & dRow(0).Item("filename")
            Else
                dRow(0).Item("destfilepath") = ConfigParameter._Object._Document_Path & "\" & cboDocumentType.Text & "\" & dRow(0).Item("filename")
            End If

            'S.SANDEEP [26 JUL 2016] -- START
            'ENHANCEMENT : DISCIPLINE MAKE-OVER {BY ANDREW}
            If dRow(0).Item("form_name").ToString.Trim.Length <= 0 Then
                dRow(0).Item("form_name") = mstrTransactionScreenName
            Else
                dRow(0).Item("form_name") = dRow(0).Item("form_name")
            End If
            'S.SANDEEP [26 JUL 2016] -- END

            dRow(0).AcceptChanges()

            mdtTran.AcceptChanges()

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "", mstrModuleName)
        Finally
        End Try
    End Sub


    'Nilay (03-Dec-2015) -- Start
    'ENHANCEMENT for CCBRT & SL - Requested by Andrew Muga.
    Private Function IsValid_MandatoryDoc() As Boolean
        Try
            Dim mdtMandatoryList As DataTable = Nothing
            Dim objcfCommon As New clsCommon_Master

            mdtMandatoryList = objcfCommon.GetList(clsCommon_Master.enCommonMaster.ATTACHMENT_TYPES, "List").Tables("List")

            If mdtMandatoryList IsNot Nothing AndAlso mdtTran IsNot Nothing Then
                Dim arrEmpDoc() As String = (From p In mdtTran Where (CStr(p.Item("AUD")) <> "D") Select (p.Item("documentunkid").ToString)).ToArray

                mstrMissingDoc = String.Join(", ", (From p In mdtMandatoryList Where (arrEmpDoc.Contains(p.Item("masterunkid").ToString) = False _
                                                    AndAlso CBool(p.Item("ismandatoryfornewemp")) = True) Select (p.Item("name").ToString)).ToArray)
                If mstrMissingDoc.Trim.Length > 0 Then
                    Return False
                End If
            Else
                Return False
            End If

            Return True
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "", mstrModuleName)
        End Try
    End Function
    'Nilay (03-Dec-2015) -- End


    'S.SANDEEP |16-MAY-2019| -- START
    'ISSUE/ENHANCEMENT : {Attachment Preview} Leave UAT
    Private Function CreateImage(ByVal xrow As DataRow, Optional ByRef strErr As String = "") As String
        Dim strError As String = ""
        Dim strLocalpath As String = ""
        Try
            Dim mstrFolderName As String = (From p In mdsDoc.Tables("Docs") Where (CInt(p.Item("Id").ToString) = CInt(xrow("scanattachrefid").ToString)) Select (p.Item("Name").ToString)).FirstOrDefault
            Dim imagebyte() As Byte = clsFileUploadDownload.DownloadFile(xrow("filepath").ToString, xrow("fileuniquename").ToString, mstrFolderName, strError, ConfigParameter._Object._ArutiSelfServiceURL)
            If imagebyte IsNot Nothing Then
                strLocalpath = Path.GetTempPath() & DateTime.Now.ToString("MMyyyyddsshhmm") & "_" & xrow("fileuniquename").ToString
                Dim ms As New MemoryStream(imagebyte)
                Dim fs As New FileStream(strLocalpath, FileMode.Create)
                ms.WriteTo(fs)
                ms.Close()
                fs.Close()
                fs.Dispose()
            End If
            strErr = strError
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "CreateImage :", mstrModuleName)
        End Try
        Return strLocalpath
    End Function

    Private Sub AddFileToZip(ByVal zipFilename As String, ByVal fileToAdd As List(Of String), Optional ByVal compression As CompressionOption = CompressionOption.Normal)
        Try
            Using zip As Package = System.IO.Packaging.Package.Open(zipFilename, FileMode.OpenOrCreate)
                For Each item As String In fileToAdd
                    Dim destFilename As String = ".\" & Path.GetFileName(item)
                    Dim uri As Uri = PackUriHelper.CreatePartUri(New Uri(destFilename, UriKind.Relative))
                    If zip.PartExists(uri) Then
                        zip.DeletePart(uri)
                    End If
                    Dim part As PackagePart = zip.CreatePart(uri, "", compression)
                    Using fileStream As New FileStream(item, FileMode.Open, FileAccess.Read)
                        Using dest As Stream = part.GetStream()
                            CopyStream(CType(fileStream, Stream), part.GetStream())
                        End Using
                    End Using
                Next
            End Using
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "AddFileToZip", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub CopyStream(ByVal source As Stream, ByVal target As Stream)
        Const bufSize As Integer = (5859 * 1024)
        Dim buf(bufSize - 1) As Byte
        Dim bytesRead As Integer = 0

        bytesRead = source.Read(buf, 0, bufSize)
        Do While bytesRead > 0
            target.Write(buf, 0, bytesRead)
            bytesRead = source.Read(buf, 0, bufSize)
        Loop
    End Sub
    'S.SANDEEP |16-MAY-2019| -- END

#End Region

#Region " Controls "

    'Shani (12-Jan-2017) -- Start
    'Issue - 
    'Private Sub cboDocumentType_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboDocumentType.SelectedIndexChanged
    '    Try

    '        'Shani (26-Sep-2016) -- Start
    '        'Enhancement -Changes PA Flow And Add New screen Given By (Andrew)
    '        Me.Cursor = Cursors.WaitCursor
    '        'Shani (26-Sep-2016) -- End


    '        If CInt(cboDocumentType.SelectedValue) > 0 Then
    '            Dim blnFlag As Boolean = False

    '            'S.SANDEEP [24 MAY 2016] -- START
    '            'ENHANCEMENT : DISCIPLINE MAKE-OVER {BY ANDREW}
    '            'lblSelection.Text = "Select " & cboDocumentType.Text
    '            If CInt(cboDocumentType.SelectedValue) = enScanAttactRefId.DISCIPLINES Then
    '                lblSelection.Text = "Reference Number"
    '            Else
    '                lblSelection.Text = "Select " & cboDocumentType.Text
    '            End If
    '            'S.SANDEEP [24 MAY 2016] -- END

    '            cboAssocatedValue.Visible = True
    '            'SHANI (20 JUN 2015) -- Start
    '            'Enhancement - Allow to add attachments for Dependants and provide attachment option on Employee Qualification and Employee Dependant screen
    '            If CInt(cboDocumentType.SelectedValue) = enScanAttactRefId.DEPENDANTS Then
    '                If CInt(cboEmployee.SelectedValue) <= 0 Then Exit Sub
    '            End If
    '            'SHANI (20 JUN 2015) -- End

    '            'S.SANDEEP [04 JUN 2015] -- START
    '            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
    '            'Dim ds As DataSet = objDocument.GetAssociatedValue(CInt(cboDocumentType.SelectedValue), CInt(cboEmployee.SelectedValue), mintRefModuleId, True)
    '            Dim ds As DataSet = objDocument.GetAssociatedValue(FinancialYear._Object._DatabaseName, _
    '                                                               User._Object._Userunkid, _
    '                                                               FinancialYear._Object._YearUnkid, _
    '                                                               Company._Object._Companyunkid, _
    '                                                               eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
    '                                                               eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
    '                                                               ConfigParameter._Object._UserAccessModeSetting, True, _
    '                                                               ConfigParameter._Object._IsIncludeInactiveEmp, _
    '                                                               CInt(cboDocumentType.SelectedValue), CInt(cboEmployee.SelectedValue), _
    '                                                               mintRefModuleId, FinancialYear._Object._FinancialYear_Name, _
    '                                                               FinancialYear._Object._Financial_Start_Date, FinancialYear._Object._Financial_End_Date, mstrTransactionScreenName, True, mintTransactionID)
    '            'Shani (20-Aug-2016) -- [ConfigParameter._Object._PaymentApprovalwithLeaveApproval]

    '            'S.SANDEEP [26 JUL 2016] -- START {xTransactionScreenName,intTransactionId} -- END
    '            'S.SANDEEP [04 JUN 2015] -- END

    '            If ds IsNot Nothing AndAlso ds.Tables.Count > 0 Then
    '                With cboAssocatedValue
    '                    Select Case CInt(cboDocumentType.SelectedValue)
    '                        Case enScanAttactRefId.IDENTITYS, enScanAttactRefId.QUALIFICATIONS, enScanAttactRefId.LEAVEFORMS, enScanAttactRefId.TRAININGS, enScanAttactRefId.JOBHISTORYS
    '                            .DataSource = Nothing
    '                            .ValueMember = "Id"
    '                            .DisplayMember = "Name"
    '                            blnFlag = False
    '                        Case enScanAttactRefId.MEMBERSHIPS
    '                            .DataSource = Nothing
    '                            .ValueMember = "Mem_Id"
    '                            .DisplayMember = "Mem_Name"
    '                            blnFlag = False
    '                        Case enScanAttactRefId.CURRICULAM_VITAE, enScanAttactRefId.OTHERS
    '                            blnFlag = True
    '                        Case enScanAttactRefId.DISCIPLINES

    '                            'S.SANDEEP [24 MAY 2016] -- START
    '                            'ENHANCEMENT : DISCIPLINE MAKE-OVER {BY ANDREW}
    '                            '.DataSource = Nothing
    '                            'Dim dtRow As DataRow = ds.Tables(0).NewRow
    '                            'dtRow.Item("disciplinefileunkid") = 0
    '                            'dtRow.Item("incident") = "Select"
    '                            'ds.Tables(0).Rows.Add(dtRow)
    '                            'ds.Tables(0).DefaultView.Sort = "disciplinefileunkid"
    '                            '.ValueMember = "disciplinefileunkid"
    '                            '.DisplayMember = "incident"
    '                            'blnFlag = False

    '                            .DataSource = Nothing
    '                            .ValueMember = "disciplinefileunkid"
    '                            .DisplayMember = "reference_no"
    '                            blnFlag = False

    '                            'S.SANDEEP [24 MAY 2016] -- END

    '                            'Sohail (07 Mar 2012) -- Start
    '                            'TRA - ENHANCEMENT
    '                        Case enScanAttactRefId.ASSET_DECLARATION
    '                            .DataSource = Nothing
    '                            .ValueMember = "assetdeclarationunkid"
    '                            .DisplayMember = "employeename"
    '                            blnFlag = False
    '                            'Sohail (07 Mar 2012) -- End

    '                            'SHANI (20 JUN 2015) -- Start
    '                            'Enhancement - Allow to add attachments for Dependants and provide attachment option on Employee Qualification and Employee Dependant screen
    '                        Case enScanAttactRefId.DEPENDANTS
    '                            .DataSource = Nothing
    '                            .ValueMember = "DpndtTranId"
    '                            .DisplayMember = "DpndtBefName"
    '                            blnFlag = False
    '                            'SHANI (20 JUN 2015) -- End 

    '                            'Shani (20-Aug-2016) -- Start
    '                            'Enhancement - Add Scan Attchment in Claim Request Module given by Matthew
    '                        Case enScanAttactRefId.CLAIM_REQUEST
    '                            .DataSource = Nothing
    '                            .ValueMember = "crtranunkid"
    '                            .DisplayMember = "claimrequestno"
    '                            blnFlag = False
    '                            'Shani (20-Aug-2016) -- End

    '                    End Select

    '                    If blnFlag = False Then
    '                        .DataSource = ds.Tables(0)
    '                        'S.SANDEEP [26 JUL 2016] -- START
    '                        'ENHANCEMENT : DISCIPLINE MAKE-OVER {BY ANDREW}
    '                        If ds.Tables(0).Rows.Count > 1 Then
    '                            If mintScanRefId = enScanAttactRefId.DISCIPLINES Then
    '                                If mintTransactionID > 0 Then .SelectedValue = mintTransactionID
    '                            Else
    '                                .SelectedValue = 0
    '                            End If
    '                        End If
    '                        'S.SANDEEP [26 JUL 2016] -- END

    '                        'S.SANDEEP [24 MAY 2016] -- START

    '                        'Shani (26-Sep-2016) -- Start
    '                        'Enhancement -Changes PA Flow And Add New screen Given By (Andrew)
    '                        'If CInt(cboDocumentType.SelectedValue) = enScanAttactRefId.DEPENDANTS Then
    '                        If ds.Tables(0).Rows.Count > 0 AndAlso CInt(cboDocumentType.SelectedValue) = enScanAttactRefId.DEPENDANTS Then
    '                            'Shani (26-Sep-2016) -- End
    '                            .SelectedIndex = 0
    '                        End If
    '                        'S.SANDEEP [24 MAY 2016] -- END
    '                    Else
    '                        lblSelection.Text = "" : cboAssocatedValue.Visible = False
    '                    End If
    '                End With
    '            Else
    '                lblSelection.Text = "" : cboAssocatedValue.Visible = False
    '            End If


    '            'Pinkal (07-May-2015) -- Start
    '            'Leave Enhancement - CHANGES IN LEAVE FORM FOR SCAN ATTACH DOCUMENTS.
    '            If mblnIsOnAddTransaction AndAlso mintScanRefId = enScanAttactRefId.LEAVEFORMS Then
    '                If ds.Tables(0).Rows.Count > 0 Then

    '                    'Pinkal (06-Jan-2016) -- Start
    '                    'Enhancement - Working on Changes in SS for Leave Module.
    '                    Dim drRow() As DataRow
    '                    If mintTransactionID > 0 Then
    '                        drRow = ds.Tables(0).Select("Id <> " & mintTransactionID)
    '                    Else
    '                        drRow = ds.Tables(0).Select("Id <> 0")
    '                    End If
    '                    'Pinkal (06-Jan-2016) -- End


    '                    If drRow.Length > 0 Then
    '                        For i As Integer = 0 To drRow.Length - 1
    '                            drRow(i).Delete()
    '                        Next
    '                    End If
    '                End If
    '            End If
    '            'Pinkal (07-May-2015) -- End


    '            'S.SANDEEP [24 MAY 2016] -- START
    '            'ENHANCEMENT : DISCIPLINE MAKE-OVER {BY ANDREW}

    '            'Shani (15-Nov-2016) -- Start
    '            'Enhancement - 
    '            'If ds.Tables(0).Rows.Count > 0 Then
    '            If ds IsNot Nothing AndAlso ds.Tables(0).Rows.Count > 0 Then
    '                'Shani (15-Nov-2016) -- End
    '                If mblnIsOnAddTransaction Then
    '                    Dim drRow() As DataRow = Nothing
    '                    Select Case mintScanRefId
    '                        Case enScanAttactRefId.DISCIPLINES
    '                            If mintTransactionID > 0 Then
    '                                drRow = ds.Tables(0).Select("disciplinefileunkid <> " & mintTransactionID)
    '                            ElseIf mintTransactionMstUnkId > 0 Then
    '                                drRow = ds.Tables(0).Select("disciplinefileunkid <> " & mintTransactionMstUnkId)
    '                            Else
    '                                drRow = ds.Tables(0).Select("disciplinefileunkid <> 0")
    '                            End If
    '                        Case enScanAttactRefId.CLAIM_REQUEST
    '                            If mintTransactionID > 0 Then
    '                                drRow = ds.Tables(0).Select("crtranunkid <> " & mintTransactionID)
    '                            ElseIf mintTransactionMstUnkId > 0 Then
    '                                drRow = ds.Tables(0).Select("crtranunkid <> " & mintTransactionMstUnkId)
    '                            Else
    '                                drRow = ds.Tables(0).Select("crtranunkid <> 0")
    '                            End If
    '                    End Select
    '                    If drRow IsNot Nothing AndAlso drRow.Length > 0 Then
    '                        For i As Integer = 0 To drRow.Length - 1
    '                            drRow(i).Delete()
    '                        Next
    '                    End If
    '                End If
    '            End If
    '            'S.SANDEEP [24 MAY 2016] -- END


    '        Else
    '            lblSelection.Text = "" : cboAssocatedValue.Visible = False
    '        End If



    '    Catch ex As Exception
    '        DisplayError.Show("-1", ex.Message, "cboDocumentType_SelectedIndexChanged", mstrModuleName)
    '    Finally
    '        'Shani (26-Sep-2016) -- Start
    '        'Enhancement -Changes PA Flow And Add New screen Given By (Andrew)
    '        Me.Cursor = Cursors.Default
    '        'Shani (26-Sep-2016) -- End
    '    End Try
    'End Sub
    Private Sub cboDocumentType_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboDocumentType.SelectedIndexChanged
        Try
            Me.Cursor = Cursors.WaitCursor

            If CInt(cboDocumentType.SelectedValue) > 0 Then
                Dim blnFlag As Boolean = False
                If CInt(cboDocumentType.SelectedValue) = enScanAttactRefId.DISCIPLINES Then
                    lblSelection.Text = "Reference Number"
                    'Gajanan [02-SEP-2019] -- Start      
                    'Enhancements: ENHANCEMENTS IN ONLINE RECRUITMENT FOR NMB
                    pnlEmployee.Enabled = True
                ElseIf CInt(cboDocumentType.SelectedValue) = enScanAttactRefId.STAFF_REQUISITION Then
                    lblSelection.Text = "Select " & cboDocumentType.Text
                    cboEmployee.SelectedValue = 0
                    pnlEmployee.Enabled = False
                    'Gajanan [02-SEP-2019] -- End
                Else
                    lblSelection.Text = "Select " & cboDocumentType.Text
                    'Gajanan [02-SEP-2019] -- Start      
                    'Enhancements: ENHANCEMENTS IN ONLINE RECRUITMENT FOR NMB
                    pnlEmployee.Enabled = True
                    'Gajanan [02-SEP-2019] -- End
                End If

                'Pinkal (03-May-2019) -- Start
                'Enhancement - Working on Leave UAT Changes for NMB.
                If mintScanRefId = enScanAttactRefId.LEAVEFORMS Then
                    cboAssocatedValue.Visible = False
                Else
                cboAssocatedValue.Visible = True
                End If
                'Pinkal (03-May-2019) -- End


                If CInt(cboDocumentType.SelectedValue) = enScanAttactRefId.DEPENDANTS Then
                    If CInt(cboEmployee.SelectedValue) <= 0 Then Exit Sub
                End If

                'S.SANDEEP |26-APR-2019| -- START
                'Dim ds As DataSet = objDocument.GetAssociatedValue(FinancialYear._Object._DatabaseName, _
                '                                                   User._Object._Userunkid, _
                '                                                   FinancialYear._Object._YearUnkid, _
                '                                                   Company._Object._Companyunkid, _
                '                                                   eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                '                                                   eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                '                                                   ConfigParameter._Object._UserAccessModeSetting, True, _
                '                                                   ConfigParameter._Object._IsIncludeInactiveEmp, _
                '                                                   CInt(cboDocumentType.SelectedValue), CInt(cboEmployee.SelectedValue), _
                '                                                   mintRefModuleId, FinancialYear._Object._FinancialYear_Name, _
                '                                                   FinancialYear._Object._Financial_Start_Date, FinancialYear._Object._Financial_End_Date, mstrTransactionScreenName, True, mintTransactionID)

                'If ds IsNot Nothing AndAlso ds.Tables.Count > 0 Then
                '    With cboAssocatedValue
                '        Select Case CInt(cboDocumentType.SelectedValue)
                '            Case enScanAttactRefId.IDENTITYS, enScanAttactRefId.QUALIFICATIONS, enScanAttactRefId.LEAVEFORMS, enScanAttactRefId.TRAININGS, enScanAttactRefId.JOBHISTORYS
                '                .DataSource = Nothing
                '                .ValueMember = "Id"
                '                .DisplayMember = "Name"
                '                blnFlag = False
                '            Case enScanAttactRefId.MEMBERSHIPS
                '                .DataSource = Nothing
                '                .ValueMember = "Mem_Id"
                '                .DisplayMember = "Mem_Name"
                '                blnFlag = False
                '            Case enScanAttactRefId.CURRICULAM_VITAE, enScanAttactRefId.OTHERS
                '                blnFlag = True
                '            Case enScanAttactRefId.DISCIPLINES
                '                .DataSource = Nothing
                '                .ValueMember = "disciplinefileunkid"
                '                .DisplayMember = "reference_no"
                '                blnFlag = False
                '            Case enScanAttactRefId.ASSET_DECLARATION
                '                .DataSource = Nothing
                '                .ValueMember = "assetdeclarationunkid"
                '                .DisplayMember = "employeename"
                '                blnFlag = False
                '            Case enScanAttactRefId.DEPENDANTS
                '                .DataSource = Nothing
                '                .ValueMember = "DpndtTranId"
                '                .DisplayMember = "DpndtBefName"
                '                blnFlag = False
                '            Case enScanAttactRefId.CLAIM_REQUEST
                '                .DataSource = Nothing
                '                .ValueMember = "crtranunkid"
                '                .DisplayMember = "claimrequestno"
                '                blnFlag = False
                '                'S.SANDEEP [29-NOV-2017] -- START
                '                'ISSUE/ENHANCEMENT : REF-ID # 136
                '            Case enScanAttactRefId.ASSESSMENT
                '                Select Case mstrTransactionScreenName.ToUpper
                '                    Case "FRMUPDATEFIELDVALUE"
                '                        .DataSource = Nothing
                '                        .ValueMember = "avalueid"
                '                        .DisplayMember = "avalue"
                '                        blnFlag = False
                '                End Select
                '                'S.SANDEEP [29-NOV-2017] -- END
                '        End Select

                '        If blnFlag = False Then
                '            .DataSource = ds.Tables(0)
                '            If ds.Tables(0).Rows.Count > 1 Then
                '                If mintScanRefId = enScanAttactRefId.DISCIPLINES Then
                '                    If mintTransactionID > 0 Then .SelectedValue = mintTransactionID
                '                Else
                '                    .SelectedValue = 0
                '                End If
                '            End If
                '            If ds.Tables(0).Rows.Count > 0 AndAlso CInt(cboDocumentType.SelectedValue) = enScanAttactRefId.DEPENDANTS Then
                '                .SelectedIndex = 0
                '            End If
                '        Else
                '            lblSelection.Text = "" : cboAssocatedValue.Visible = False
                '        End If
                '    End With
                'Else
                '    lblSelection.Text = "" : cboAssocatedValue.Visible = False
                'End If

                'If mblnIsOnAddTransaction AndAlso mintScanRefId = enScanAttactRefId.LEAVEFORMS Then
                '    If ds.Tables(0).Rows.Count > 0 Then
                '        Dim drRow() As DataRow
                '        If mintTransactionID > 0 Then
                '            drRow = ds.Tables(0).Select("Id = " & mintTransactionID)

                '            'Pinkal (28-Nov-2017) -- Start
                '            'Enhancement - Cancel form document attachment  - (RefNo: 132) On leave cancel process there should be a place to attach document for a reference and this feature (cancel leave form) should be included in Aruti self service .
                '        ElseIf mintTransactionMstUnkId > 0 Then
                '            drRow = ds.Tables(0).Select("Id = " & mintTransactionMstUnkId)
                '            'Pinkal (28-Nov-2017) -- End

                '        Else
                '            drRow = ds.Tables(0).Select("Id = 0")
                '        End If

                '        'Pinkal (28-Nov-2017) -- Start
                '        'Enhancement - Cancel form document attachment  - (RefNo: 132) On leave cancel process there should be a place to attach document for a reference and this feature (cancel leave form) should be included in Aruti self service .
                '        'ds.Tables(0).Rows.Clear()
                '        'Pinkal (28-Nov-2017) -- End

                '        If drRow.Length > 0 Then
                '            Dim dt As DataTable = drRow.CopyToDataTable
                '            'Pinkal (28-Nov-2017) -- Start
                '            'Enhancement - Cancel form document attachment  - (RefNo: 132) On leave cancel process there should be a place to attach document for a reference and this feature (cancel leave form) should be included in Aruti self service .
                '            ds.Tables(0).Rows.Clear()
                '            'Pinkal (28-Nov-2017) -- End
                '            ds.Tables(0).Merge(dt)
                '            'For i As Integer = 0 To drRow.Length - 1
                '            '    drRow(i).Delete()
                '            'Next
                '        End If
                '    End If
                'End If

                'If ds IsNot Nothing AndAlso ds.Tables(0).Rows.Count > 0 Then
                '    If mblnIsOnAddTransaction Then
                '        Dim drRow() As DataRow = Nothing
                '        Select Case mintScanRefId
                '            Case enScanAttactRefId.DISCIPLINES
                '                If mintTransactionID > 0 Then
                '                    drRow = ds.Tables(0).Select("disciplinefileunkid <> " & mintTransactionID)
                '                ElseIf mintTransactionMstUnkId > 0 Then
                '                    drRow = ds.Tables(0).Select("disciplinefileunkid <> " & mintTransactionMstUnkId)
                '                Else
                '                    drRow = ds.Tables(0).Select("disciplinefileunkid <> 0")
                '                End If
                '                If drRow IsNot Nothing AndAlso drRow.Length > 0 Then
                '                    For i As Integer = 0 To drRow.Length - 1
                '                        drRow(i).Delete()
                '                    Next
                '                End If
                '            Case enScanAttactRefId.CLAIM_REQUEST
                '                If mintTransactionID > 0 Then
                '                    drRow = ds.Tables(0).Select("crtranunkid = " & mintTransactionID)
                '                ElseIf mintTransactionMstUnkId > 0 Then
                '                    drRow = ds.Tables(0).Select("crtranunkid = " & mintTransactionMstUnkId)
                '                Else
                '                    'Pinkal (28-Nov-2017) -- Start
                '                    'Enhancement - Cancel form document attachment  - (RefNo: 132) On leave cancel process there should be a place to attach document for a reference and this feature (cancel leave form) should be included in Aruti self service .
                '                    'drRow = ds.Tables(0).Select("crtranunkid = 0")
                '                    drRow = ds.Tables(0).Select("crtranunkid <> 0")
                '                    'Pinkal (28-Nov-2017) -- Start
                '                End If

                '                'Pinkal (28-Nov-2017) -- Start
                '                'Enhancement - Cancel form document attachment  - (RefNo: 132) On leave cancel process there should be a place to attach document for a reference and this feature (cancel leave form) should be included in Aruti self service .
                '                'ds.Tables(0).Rows.Clear()
                '                'Pinkal (28-Nov-2017) -- End
                '                If drRow IsNot Nothing AndAlso drRow.Length > 0 Then
                '                    Dim dt As DataTable = drRow.CopyToDataTable
                '                    'Pinkal (28-Nov-2017) -- Start
                '                    'Enhancement - Cancel form document attachment  - (RefNo: 132) On leave cancel process there should be a place to attach document for a reference and this feature (cancel leave form) should be included in Aruti self service .
                '                    ds.Tables(0).Rows.Clear()
                '                    'Pinkal (28-Nov-2017) -- End
                '                    ds.Tables(0).Merge(dt)
                '                End If
                '        End Select

                '    End If
                'End If

                If mdtSource Is Nothing Then
                Dim ds As DataSet = objDocument.GetAssociatedValue(FinancialYear._Object._DatabaseName, _
                                                                   User._Object._Userunkid, _
                                                                   FinancialYear._Object._YearUnkid, _
                                                                   Company._Object._Companyunkid, _
                                                                   eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                                   eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                                   ConfigParameter._Object._UserAccessModeSetting, True, _
                                                                   ConfigParameter._Object._IsIncludeInactiveEmp, _
                                                                   CInt(cboDocumentType.SelectedValue), CInt(cboEmployee.SelectedValue), _
                                                                   mintRefModuleId, FinancialYear._Object._FinancialYear_Name, _
                                                                   FinancialYear._Object._Financial_Start_Date, FinancialYear._Object._Financial_End_Date, mstrTransactionScreenName, True, mintTransactionID)
                
                If ds IsNot Nothing AndAlso ds.Tables.Count > 0 Then
                    With cboAssocatedValue
                        Select Case CInt(cboDocumentType.SelectedValue)
                            Case enScanAttactRefId.IDENTITYS, enScanAttactRefId.QUALIFICATIONS, enScanAttactRefId.LEAVEFORMS, enScanAttactRefId.TRAININGS, enScanAttactRefId.JOBHISTORYS
                                .DataSource = Nothing
                                .ValueMember = "Id"
                                .DisplayMember = "Name"
                                blnFlag = False
                            Case enScanAttactRefId.MEMBERSHIPS
                                .DataSource = Nothing
                                .ValueMember = "Mem_Id"
                                .DisplayMember = "Mem_Name"
                                blnFlag = False
                            Case enScanAttactRefId.CURRICULAM_VITAE, enScanAttactRefId.OTHERS
                                blnFlag = True
                            Case enScanAttactRefId.DISCIPLINES
                                .DataSource = Nothing
                                .ValueMember = "disciplinefileunkid"
                                .DisplayMember = "reference_no"
                                blnFlag = False
                            Case enScanAttactRefId.ASSET_DECLARATION
                                .DataSource = Nothing
                                .ValueMember = "assetdeclarationunkid"
                                .DisplayMember = "employeename"
                                blnFlag = False
                            Case enScanAttactRefId.DEPENDANTS
                                .DataSource = Nothing
                                .ValueMember = "DpndtTranId"
                                .DisplayMember = "DpndtBefName"
                                blnFlag = False
                            Case enScanAttactRefId.CLAIM_REQUEST
                                .DataSource = Nothing
                                .ValueMember = "crtranunkid"
                                .DisplayMember = "claimrequestno"
                                blnFlag = False
                                'S.SANDEEP [29-NOV-2017] -- START
                                'ISSUE/ENHANCEMENT : REF-ID # 136
                            Case enScanAttactRefId.ASSESSMENT
                                Select Case mstrTransactionScreenName.ToUpper
                                    Case "FRMUPDATEFIELDVALUE"
                                        .DataSource = Nothing
                                        .ValueMember = "avalueid"
                                        .DisplayMember = "avalue"
                                        blnFlag = False
                                End Select
                                'S.SANDEEP [29-NOV-2017] -- END

                                    'Gajanan [02-SEP-2019] -- Start      
                                    'Enhancements: ENHANCEMENTS IN ONLINE RECRUITMENT FOR NMB
                                Case enScanAttactRefId.STAFF_REQUISITION
                                    .DataSource = Nothing
                                    .ValueMember = "staffrequisitiontranunkid"
                                    .DisplayMember = "formno"
                                    blnFlag = False
                                    'Gajanan [02-SEP-2019] -- End

                        End Select

                        If blnFlag = False Then
                            .DataSource = ds.Tables(0)
                            If ds.Tables(0).Rows.Count > 1 Then
                                If mintScanRefId = enScanAttactRefId.DISCIPLINES Then
                                    If mintTransactionID > 0 Then .SelectedValue = mintTransactionID
                                Else
                                    .SelectedValue = 0
                                End If
                            End If
                            If ds.Tables(0).Rows.Count > 0 AndAlso CInt(cboDocumentType.SelectedValue) = enScanAttactRefId.DEPENDANTS Then
                                .SelectedIndex = 0
                            End If
                        Else
                            lblSelection.Text = "" : cboAssocatedValue.Visible = False
                        End If
                    End With
                Else
                    lblSelection.Text = "" : cboAssocatedValue.Visible = False
                End If

                If mblnIsOnAddTransaction AndAlso mintScanRefId = enScanAttactRefId.LEAVEFORMS Then
                    If ds.Tables(0).Rows.Count > 0 Then
                        Dim drRow() As DataRow
                        If mintTransactionID > 0 Then
                            drRow = ds.Tables(0).Select("Id = " & mintTransactionID)

                            'Pinkal (28-Nov-2017) -- Start
                            'Enhancement - Cancel form document attachment  - (RefNo: 132) On leave cancel process there should be a place to attach document for a reference and this feature (cancel leave form) should be included in Aruti self service .
                        ElseIf mintTransactionMstUnkId > 0 Then
                            drRow = ds.Tables(0).Select("Id = " & mintTransactionMstUnkId)
                            'Pinkal (28-Nov-2017) -- End

                        Else
                            drRow = ds.Tables(0).Select("Id = 0")
                        End If

                        'Pinkal (28-Nov-2017) -- Start
                        'Enhancement - Cancel form document attachment  - (RefNo: 132) On leave cancel process there should be a place to attach document for a reference and this feature (cancel leave form) should be included in Aruti self service .
                        'ds.Tables(0).Rows.Clear()
                        'Pinkal (28-Nov-2017) -- End

                        If drRow.Length > 0 Then
                            Dim dt As DataTable = drRow.CopyToDataTable
                            'Pinkal (28-Nov-2017) -- Start
                            'Enhancement - Cancel form document attachment  - (RefNo: 132) On leave cancel process there should be a place to attach document for a reference and this feature (cancel leave form) should be included in Aruti self service .
                            ds.Tables(0).Rows.Clear()
                            'Pinkal (28-Nov-2017) -- End
                            ds.Tables(0).Merge(dt)
                            'For i As Integer = 0 To drRow.Length - 1
                            '    drRow(i).Delete()
                            'Next
                        End If
                    End If
                End If

                If ds IsNot Nothing AndAlso ds.Tables(0).Rows.Count > 0 Then
                    If mblnIsOnAddTransaction Then
                        Dim drRow() As DataRow = Nothing
                        Select Case mintScanRefId
                            Case enScanAttactRefId.DISCIPLINES
                                If mintTransactionID > 0 Then
                                        drRow = ds.Tables(0).Select("disciplinefileunkid IN (" & mintTransactionID & ")")
                                ElseIf mintTransactionMstUnkId > 0 Then
                                        drRow = ds.Tables(0).Select("disciplinefileunkid IN (" & mintTransactionMstUnkId & ")")
                                Else
                                        drRow = ds.Tables(0).Select("disciplinefileunkid IN (0) ")
                                End If
                                    'If drRow IsNot Nothing AndAlso drRow.Length > 0 Then
                                    '    For i As Integer = 0 To drRow.Length - 1
                                    '        drRow(i).Delete()
                                    '    Next
                                    'End If
                                If drRow IsNot Nothing AndAlso drRow.Length > 0 Then
                                        'For i As Integer = 0 To drRow.Length - 1
                                        '    drRow(i).Delete()
                                        'Next
                                        Dim dt As DataTable = drRow.CopyToDataTable
                                        ds.Tables(0).Rows.Clear()
                                        ds.Tables(0).Merge(dt)
                                End If
                            Case enScanAttactRefId.CLAIM_REQUEST
                                If mintTransactionID > 0 Then
                                    drRow = ds.Tables(0).Select("crtranunkid = " & mintTransactionID)
                                ElseIf mintTransactionMstUnkId > 0 Then
                                    drRow = ds.Tables(0).Select("crtranunkid = " & mintTransactionMstUnkId)
                                Else
                                    'Pinkal (28-Nov-2017) -- Start
                                    'Enhancement - Cancel form document attachment  - (RefNo: 132) On leave cancel process there should be a place to attach document for a reference and this feature (cancel leave form) should be included in Aruti self service .
                                    'drRow = ds.Tables(0).Select("crtranunkid = 0")
                                    drRow = ds.Tables(0).Select("crtranunkid <> 0")
                                    'Pinkal (28-Nov-2017) -- Start
                                End If

                                'Pinkal (28-Nov-2017) -- Start
                                'Enhancement - Cancel form document attachment  - (RefNo: 132) On leave cancel process there should be a place to attach document for a reference and this feature (cancel leave form) should be included in Aruti self service .
                                'ds.Tables(0).Rows.Clear()
                                'Pinkal (28-Nov-2017) -- End
                        If drRow IsNot Nothing AndAlso drRow.Length > 0 Then
                                    Dim dt As DataTable = drRow.CopyToDataTable
                                    'Pinkal (28-Nov-2017) -- Start
                                    'Enhancement - Cancel form document attachment  - (RefNo: 132) On leave cancel process there should be a place to attach document for a reference and this feature (cancel leave form) should be included in Aruti self service .
                                    ds.Tables(0).Rows.Clear()
                                    'Pinkal (28-Nov-2017) -- End
                                    ds.Tables(0).Merge(dt)
                        End If
                        End Select
                        
                    End If
                End If
            Else
                    RemoveHandler cboAssocatedValue.SelectedIndexChanged, AddressOf cboAssocatedValue_SelectedIndexChanged
                    With cboAssocatedValue
                        .DataSource = mdtSource
                        .ValueMember = mstrValueMember
                        .DisplayMember = mstrDisplayMember
                        .SelectedIndex = 0                        
                    End With
                    AddHandler cboAssocatedValue.SelectedIndexChanged, AddressOf cboAssocatedValue_SelectedIndexChanged
                End If
                'S.SANDEEP |26-APR-2019| -- END
            Else
                lblSelection.Text = "" : cboAssocatedValue.Visible = False
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboDocumentType_SelectedIndexChanged", mstrModuleName)
        Finally
            Me.Cursor = Cursors.Default
        End Try
    End Sub
    'Shani (12-Jan-2017) -- End

    Private Sub cboEmployee_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboEmployee.SelectedIndexChanged
        Try
            'SHANI (20 JUN 2015) -- Start
            'Enhancement - Allow to add attachments for Dependants and provide attachment option on Employee Qualification and Employee Dependant screen
            If CInt(cboDocument.SelectedValue) = enScanAttactRefId.DEPENDANTS Then
                cboDocument.SelectedValue = 0
            End If
            'SHANI (20 JUN 2015) -- End
            cboDocumentType.SelectedValue = 0

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub lvScanAttachment_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Try
            If lvScanAttachment.SelectedItems.Count > 0 Then

                'Nilay (03-Dec-2015) -- Start
                'ENHANCEMENT for CCBRT & SL - Requested by Andrew Muga.
                'Select Case mintRefModuleId
                '    Case enImg_Email_RefId.Applicant_Module
                '        cboEmployee.SelectedValue = CInt(lvScanAttachment.SelectedItems(0).SubItems(objcolhAppId.Index).Text)
                '    Case enImg_Email_RefId.Employee_Module
                '        cboEmployee.SelectedValue = CInt(lvScanAttachment.SelectedItems(0).SubItems(objcolhEmpId.Index).Text)
                'End Select
                If mblnIsEmpForm = False OrElse mintEmployeeId > 0 Then
                    Select Case mintRefModuleId
                        Case enImg_Email_RefId.Applicant_Module
                            cboEmployee.SelectedValue = CInt(lvScanAttachment.SelectedItems(0).SubItems(objcolhAppId.Index).Text)
                        Case enImg_Email_RefId.Employee_Module
                            cboEmployee.SelectedValue = CInt(lvScanAttachment.SelectedItems(0).SubItems(objcolhEmpId.Index).Text)
                    End Select
                End If
                'Nilay (03-Dec-2015) -- End

                cboDocument.SelectedValue = CInt(lvScanAttachment.SelectedItems(0).SubItems(objcolhDocumentId.Index).Text)
                cboDocumentType.Text = CStr(lvScanAttachment.SelectedItems(0).SubItems(objcolhDocType.Index).Text)
                cboAssocatedValue.SelectedValue = CInt(lvScanAttachment.SelectedItems(0).SubItems(objcolhTransId.Index).Text)
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lvScanAttachment_SelectedIndexChanged", mstrModuleName)
        Finally
        End Try
    End Sub

    'S.SANDEEP |26-APR-2019| -- START
    Private Sub cboAssocatedValue_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboAssocatedValue.SelectedIndexChanged
        Try
            If mdtSource IsNot Nothing Then
                Dim strFilter As String = "pguid = '" & DirectCast(cboAssocatedValue.SelectedItem, DataRowView).Item("guid").ToString() & "' "
                FillList(strFilter)
            End If
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: cboAssocatedValue_SelectedIndexChanged; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Sub
    'S.SANDEEP |26-APR-2019| -- END

#End Region

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Private Sub OtherSettings()
        Try
            Me.SuspendLayout()
           
            Call SetLanguage()

            Me.eZeeHeader.GradientColor1 = GUI._HeaderBackColor1
            Me.eZeeHeader.GradientColor2 = GUI._HeaderBackColor2
            Me.eZeeHeader.BorderColor = GUI._HeaderBorderColor
            Me.eZeeHeader.HeaderTextForeColor = GUI._HeaderTitleFontColor
            Me.eZeeHeader.DescriptionForeColor = GUI._HeaderMessageFontColor


            Me.btnClose.GradientBackColor = GUI._ButttonBackColor
            Me.btnClose.GradientForeColor = GUI._ButttonFontColor

            Me.btnScan.GradientBackColor = GUI._ButttonBackColor
            Me.btnScan.GradientForeColor = GUI._ButttonFontColor

            Me.btnSave.GradientBackColor = GUI._ButttonBackColor
            Me.btnSave.GradientForeColor = GUI._ButttonFontColor

            Me.btnRemove.GradientBackColor = GUI._ButttonBackColor
            Me.btnRemove.GradientForeColor = GUI._ButttonFontColor

            Me.btnAdd.GradientBackColor = GUI._ButttonBackColor
            Me.btnAdd.GradientForeColor = GUI._ButttonFontColor

            Me.btnEdit.GradientBackColor = GUI._ButttonBackColor
            Me.btnEdit.GradientForeColor = GUI._ButttonFontColor

            Me.btnPreview.GradientBackColor = GUI._ButttonBackColor
            Me.btnPreview.GradientForeColor = GUI._ButttonFontColor


            Me.ResumeLayout()
        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "OtherSettings", mstrModuleName)
        End Try
    End Sub


    Private Sub SetLanguage()
        Try
            Me.Text = Language._Object.getCaption(Me.Name, Me.Text)

            Me.eZeeHeader.Title = Language._Object.getCaption(Me.eZeeHeader.Name & "_Title", Me.eZeeHeader.Title)
            Me.eZeeHeader.Message = Language._Object.getCaption(Me.eZeeHeader.Name & "_Message", Me.eZeeHeader.Message)
            Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
            Me.btnScan.Text = Language._Object.getCaption(Me.btnScan.Name, Me.btnScan.Text)
            Me.btnSave.Text = Language._Object.getCaption(Me.btnSave.Name, Me.btnSave.Text)
            Me.lblDocType.Text = Language._Object.getCaption(Me.lblDocType.Name, Me.lblDocType.Text)
            Me.lblDocument.Text = Language._Object.getCaption(Me.lblDocument.Name, Me.lblDocument.Text)
            Me.btnRemove.Text = Language._Object.getCaption(Me.btnRemove.Name, Me.btnRemove.Text)
            Me.btnAdd.Text = Language._Object.getCaption(Me.btnAdd.Name, Me.btnAdd.Text)
            Me.colhCode.Text = Language._Object.getCaption(CStr(Me.colhCode.Tag), Me.colhCode.Text)
            Me.colhName.Text = Language._Object.getCaption(CStr(Me.colhName.Tag), Me.colhName.Text)
            Me.colhDocument.Text = Language._Object.getCaption(CStr(Me.colhDocument.Tag), Me.colhDocument.Text)
            Me.colhFileName.Text = Language._Object.getCaption(CStr(Me.colhFileName.Tag), Me.colhFileName.Text)
            Me.lblUploadDate.Text = Language._Object.getCaption(Me.lblUploadDate.Name, Me.lblUploadDate.Text)
            Me.lblSelection.Text = Language._Object.getCaption(Me.lblSelection.Name, Me.lblSelection.Text)
            Me.colhValue.Text = Language._Object.getCaption(CStr(Me.colhValue.Tag), Me.colhValue.Text)
            Me.btnEdit.Text = Language._Object.getCaption(Me.btnEdit.Name, Me.btnEdit.Text)
            Me.btnPreview.Text = Language._Object.getCaption(Me.btnPreview.Name, Me.btnPreview.Text)

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
        End Try
    End Sub


    Private Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Employee is mandatory information. Please select Employee to continue.")
            Language.setMessage(mstrModuleName, 2, "Document is mandatory information. Please select Document to continue.")
            Language.setMessage(mstrModuleName, 3, "Document Type is mandatory information. Please select Document Type to continue.")
            Language.setMessage(mstrModuleName, 4, "Applicant is mandatory information. Please select Applicant to continue.")
            Language.setMessage(mstrModuleName, 5, "There are some unsaved information(s) on the list." & vbCrLf & " Are you sure, you don't want to save the information(s)?")
            Language.setMessage(mstrModuleName, 6, "Selected information is already present for particular employee.")
            Language.setMessage(mstrModuleName, 7, "Please select atleast one information in order to edit.")
            Language.setMessage(mstrModuleName, 8, " is mandatory information. Please select")
            Language.setMessage(mstrModuleName, 9, " in order to continue.")
            Language.setMessage(mstrModuleName, 10, "Please set Documents Path from Aruti Configuration -> Path.")
            Language.setMessage(mstrModuleName, 11, "Please add atleast one document.")
            Language.setMessage(mstrModuleName, 12, "Configuration Path does not Exist.")
            Language.setMessage(mstrModuleName, 13, "Sorry, Some of the Scan/Attached documents are set as mandatory. Please click ADD button to attach missing document(s) -")
			Language.setMessage(mstrModuleName, 14, "You can not attach .exe(Executable File) for the security reason.")
			Language.setMessage(mstrModuleName, 15, "Please select only Document or PDF file.")
			Language.setMessage("frmScanAttachmentList", 13, "File(s) downloaded successfully to the selected location.")
			Language.setMessage("frmScanAttachmentList", 14, "File not found.")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class

'#Region " Private Variables "
'    Private ReadOnly mstrModuleName As String = "frmScanOrAttachmentInfo"
'    Private mstrLableCaption As String = ""
'    Private dsList As New DataSet
'    Private mintRefId As Integer = 0
'    Private mintEmployeeunkid As Integer = 0
'    Private mintApplicantUnkid As Integer = 0
'    Private mblnIsComboVisible As Boolean = False
'    Private mstrTextValue As String = ""
'    Private mintActionTag As Integer = 0
'    Private mstrEmployeeName As String = ""
'    Private mstrApplicantName As String = ""
'    Private objDocument As clsScan_Attach_Documents
'#End Region

'#Region " Properties "
'    Public WriteOnly Property _LableCaption() As String
'        Set(ByVal value As String)
'            mstrLableCaption = value
'        End Set
'    End Property

'    Public WriteOnly Property _Dataset() As DataSet
'        Set(ByVal value As DataSet)
'            dsList = value
'        End Set
'    End Property

'    Public WriteOnly Property _RefId() As Integer
'        Set(ByVal value As Integer)
'            mintRefId = value
'        End Set
'    End Property

'    Public WriteOnly Property _EmployeeUnkid() As Integer
'        Set(ByVal value As Integer)
'            mintEmployeeunkid = value
'        End Set
'    End Property

'    Public WriteOnly Property _ApplicantUnkid() As Integer
'        Set(ByVal value As Integer)
'            mintApplicantUnkid = value
'        End Set
'    End Property

'    Public WriteOnly Property _IsComboVisible() As Boolean
'        Set(ByVal value As Boolean)
'            mblnIsComboVisible = value
'        End Set
'    End Property

'    Public WriteOnly Property _TextValue() As String
'        Set(ByVal value As String)
'            mstrTextValue = value
'        End Set
'    End Property

'    Public WriteOnly Property _ActionTag() As Integer
'        Set(ByVal value As Integer)
'            mintActionTag = value
'        End Set
'    End Property

'    Public WriteOnly Property _EmployeeName() As String
'        Set(ByVal value As String)
'            mstrEmployeeName = value
'        End Set
'    End Property

'    Public WriteOnly Property _ApplicantName() As String
'        Set(ByVal value As String)
'            mstrApplicantName = value
'        End Set
'    End Property
'#End Region

'#Region " Form's "
'    Private Sub frmScanOrAttachmentInfo_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
'        Try
'            Call Set_Logo(Me, gApplicationType)
'            objDocument = New clsScan_Attach_Documents
'            objlblSelection.Text = mstrLableCaption
'            Call SetProcessVisibility()
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "frmScanOrAttachmentInfo_Load", mstrModuleName)
'        End Try
'    End Sub
'#End Region

'#Region " Private Functions "
'    Private Sub FillCombo()
'        Try
'            With cboSelection
'                Select Case mintRefId
'                    Case enScanAttactRefId.EMPLOYEE_IDENTITY                    'Will Stored in Identity Path
'                        .ValueMember = "Id"
'                        .DisplayMember = "Name"
'                        .DataSource = dsList.Tables(0)
'                    Case enScanAttactRefId.EMPLOYEE_MEMBERSHIP              'Will Stored in Identity Path
'                        .ValueMember = "Mem_Id"
'                        .DisplayMember = "Mem_Name"
'                        .DataSource = dsList.Tables(0)
'                    Case enScanAttactRefId.EMPLOYEE_QUALIFICATION          'Will Stored in Certificate Path
'                        .ValueMember = "QualifyId"
'                        .DisplayMember = "Qualify"
'                        .DataSource = dsList.Tables(0)
'                    Case enScanAttactRefId.EMPLOYEE_JOBHISTORY               'Will Stored in Certificate Path
'                        .ValueMember = "ExpId"
'                        .DisplayMember = "Company"
'                        .DataSource = dsList.Tables(0)
'                        'Case enScanAttactRefId.EMPLOYEE_LEAVEFORM                'Will Stored in Other Path
'                        'Case enScanAttactRefId.EMPLOYEE_DISCIPLINE                'Will Stored in Other Path
'                        'Case enScanAttactRefId.APPLICANT_CV                              'Will Stored in Other Path
'                    Case enScanAttactRefId.APPLICANT_QUALIFICATION, _
'                             enScanAttactRefId.APPLICANT_JOBHISTORY                   'Will Stored in Certificate Path
'                        .ValueMember = "Id"
'                        .DisplayMember = "Name"
'                        .DataSource = dsList.Tables(0)
'                        'S.SANDEEP [ 15 SEP 2011 ] -- START
'                        'ENHANCEMENT : CODE OPTIMIZATION
'                    Case enScanAttactRefId.EMPLOYEE_LEAVEFORM
'                        .ValueMember = "formunkid"
'                        .DisplayMember = "formno"
'                        .DataSource = dsList.Tables(0)
'                    Case enScanAttactRefId.EMPLOYEE_TRAINING
'                        .ValueMember = "trainingschedulingunkid"
'                        .DisplayMember = "Title"
'                        .DataSource = dsList.Tables(0)
'                        'S.SANDEEP [ 15 SEP 2011 ] -- END 

'                End Select
'            End With
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
'        End Try
'    End Sub

'    Private Sub FillList(ByVal intRefId As Integer, ByVal intEmpId As Integer, Optional ByVal intDocumentId As Integer = 0)
'        Dim dsData As New DataSet
'        Try
'            dsData = objDocument.GetList("Document", intRefId, intEmpId, intDocumentId)
'            Dim lvItem As ListViewItem

'            lvAttachment.Items.Clear()

'            For Each dtRow As DataRow In dsData.Tables(0).Rows
'                lvItem = New ListViewItem

'                Select Case CInt(dtRow.Item("scanattachrefid"))
'                    Case enScanAttactRefId.EMPLOYEE_IDENTITY, _
'                             enScanAttactRefId.EMPLOYEE_MEMBERSHIP
'                        lvItem.Text = ConfigParameter._Object._IdentityPath & "\" & dtRow.Item("filename").ToString

'                        'S.SANDEEP [ 15 SEP 2011 ] -- START
'                        'ENHANCEMENT : CODE OPTIMIZATION
'                        'Case enScanAttactRefId.EMPLOYEE_QUALIFICATION, _
'                        '     enScanAttactRefId.EMPLOYEE_JOBHISTORY, _
'                        '     enScanAttactRefId.APPLICANT_QUALIFICATION, _
'                        '     enScanAttactRefId.APPLICANT_JOBHISTORY
'                    Case enScanAttactRefId.EMPLOYEE_QUALIFICATION, _
'                             enScanAttactRefId.EMPLOYEE_JOBHISTORY, _
'                             enScanAttactRefId.APPLICANT_QUALIFICATION, _
'                         enScanAttactRefId.APPLICANT_JOBHISTORY, _
'                         enScanAttactRefId.EMPLOYEE_TRAINING
'                        'S.SANDEEP [ 15 SEP 2011 ] -- END 

'                        lvItem.Text = ConfigParameter._Object._CertificatePath & "\" & dtRow.Item("filename").ToString
'                        'Sandeep [ 21 APRIL 2011 ] -- Start
'                        'Issue : SCAN & ATTACHMENT ENHANCEMENT
'                        'Case enScanAttactRefId.EMPLOYEE_LEAVEFORM, _
'                        '                             enScanAttactRefId.EMPLOYEE_DISCIPLINE, _
'                        '                             enScanAttactRefId.APPLICANT_CV
'                    Case enScanAttactRefId.EMPLOYEE_LEAVEFORM, _
'                             enScanAttactRefId.EMPLOYEE_DISCIPLINE, _
'                             enScanAttactRefId.APPLICANT_CV, _
'                             enScanAttactRefId.OTHER_DOCUMENTS
'                        'Sandeep [ 21 APRIL 2011 ] -- End 
'                        lvItem.Text = ConfigParameter._Object._OtherPath & "\" & dtRow.Item("filename").ToString
'                End Select
'                lvItem.Tag = dtRow.Item("scanattachtranunkid")

'                lvAttachment.Items.Add(lvItem)
'            Next

'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "FillList", mstrModuleName)
'        End Try
'    End Sub

'    Private Function IsValid() As Boolean
'        Try

'            Select Case mintRefId
'                Case enScanAttactRefId.EMPLOYEE_IDENTITY, enScanAttactRefId.EMPLOYEE_MEMBERSHIP
'                    If ConfigParameter._Object._IdentityPath = "" Then
'                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Identity path is not set. Please set identity path from configuration."), enMsgBoxStyle.Information)
'                        Return False
'                    End If
'                Case enScanAttactRefId.EMPLOYEE_QUALIFICATION, enScanAttactRefId.EMPLOYEE_JOBHISTORY, enScanAttactRefId.APPLICANT_QUALIFICATION
'                    If ConfigParameter._Object._CertificatePath = "" Then
'                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Certificate path is not set. Please set certificate path from configuration."), enMsgBoxStyle.Information)
'                        Return False
'                    End If
'                Case enScanAttactRefId.EMPLOYEE_LEAVEFORM, enScanAttactRefId.EMPLOYEE_DISCIPLINE, enScanAttactRefId.APPLICANT_CV
'                    If ConfigParameter._Object._OtherPath = "" Then
'                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Other path is not set. Please set other path from configuration."), enMsgBoxStyle.Information)
'                        Return False
'                    End If
'            End Select

'            Return True
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "IsValid", mstrModuleName)
'        End Try
'    End Function

'    Private Sub AttachDocument(ByVal intRefUnkid As Integer, ByVal strFile As String) 
'        Try
'            Select Case intRefUnkid
'                Case enScanAttactRefId.EMPLOYEE_IDENTITY, _
'                         enScanAttactRefId.EMPLOYEE_MEMBERSHIP

'                    If Not System.IO.Directory.Exists(ConfigParameter._Object._IdentityPath) Then
'                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Cannot find the path : """) & ConfigParameter._Object._IdentityPath & Language.getMessage(mstrModuleName, 5, """ for saving file. Please set valid destination  path from configuration."), enMsgBoxStyle.Information)
'                        Exit Sub
'                    Else
'                        If IO.File.Exists(ConfigParameter._Object._IdentityPath & "\" & strFile) Then
'                            IO.File.Delete(ConfigParameter._Object._IdentityPath & "\" & strFile)
'                        End If
'                        IO.File.Copy(ofdAttachment.FileName, ConfigParameter._Object._IdentityPath & "\" & strFile)
'                        objDocument._Employeeunkid = mintEmployeeunkid
'                        objDocument._Filename = strFile
'                        objDocument._Scanattachrefid = mintRefId
'                        objDocument._Documentunkid = CInt(cboSelection.SelectedValue)
'                        If objDocument.Insert() Then
'                            Call FillList(mintRefId, mintEmployeeunkid, CInt(cboSelection.SelectedValue))
'                        Else
'                            If objDocument._Message <> "" Then
'                                eZeeMsgBox.Show(objDocument._Message, enMsgBoxStyle.Information)
'                            End If
'                        End If
'                    End If

'                    'S.SANDEEP [ 15 SEP 2011 ] -- START
'                    'ENHANCEMENT : CODE OPTIMIZATION
'                    'Case enScanAttactRefId.EMPLOYEE_QUALIFICATION, _
'                    '                         enScanAttactRefId.EMPLOYEE_JOBHISTORY, _
'                    '                         enScanAttactRefId.APPLICANT_QUALIFICATION, _
'                    '                         enScanAttactRefId.APPLICANT_JOBHISTORY
'                Case enScanAttactRefId.EMPLOYEE_QUALIFICATION, _
'                         enScanAttactRefId.EMPLOYEE_JOBHISTORY, _
'                         enScanAttactRefId.APPLICANT_QUALIFICATION, _
'                     enScanAttactRefId.APPLICANT_JOBHISTORY, _
'                     enScanAttactRefId.EMPLOYEE_TRAINING
'                    'S.SANDEEP [ 15 SEP 2011 ] -- END 

'                    If Not System.IO.Directory.Exists(ConfigParameter._Object._CertificatePath) Then
'                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Cannot find the path : """) & ConfigParameter._Object._CertificatePath & Language.getMessage(mstrModuleName, 5, """ for saving file. Please set valid destination  path from configuration."), enMsgBoxStyle.Information)
'                        Exit Sub
'                    Else
'                        If IO.File.Exists(ConfigParameter._Object._CertificatePath & "\" & strFile) Then
'                            IO.File.Delete(ConfigParameter._Object._CertificatePath & "\" & strFile)
'                        End If
'                        IO.File.Copy(ofdAttachment.FileName, ConfigParameter._Object._CertificatePath & "\" & strFile)
'                        objDocument._Employeeunkid = CInt(IIf(mintEmployeeunkid = 0, mintApplicantUnkid, mintEmployeeunkid))
'                        objDocument._Filename = strFile
'                        objDocument._Scanattachrefid = mintRefId
'                        objDocument._Documentunkid = CInt(cboSelection.SelectedValue)
'                        If objDocument.Insert() Then
'                            Call FillList(mintRefId, CInt(IIf(mintEmployeeunkid = 0, mintApplicantUnkid, mintEmployeeunkid)), CInt(cboSelection.SelectedValue))
'                        Else
'                            If objDocument._Message <> "" Then
'                                eZeeMsgBox.Show(objDocument._Message, enMsgBoxStyle.Information)
'                            End If
'                        End If
'                    End If
'                    'Sandeep [ 21 APRIL 2011 ] -- Start
'                    'Issue : SCAN & ATTACHMENT ENHANCEMENT
'                    'Case enScanAttactRefId.EMPLOYEE_LEAVEFORM, enScanAttactRefId.EMPLOYEE_DISCIPLINE, enScanAttactRefId.APPLICANT_CV
'                Case enScanAttactRefId.EMPLOYEE_LEAVEFORM, enScanAttactRefId.EMPLOYEE_DISCIPLINE, enScanAttactRefId.APPLICANT_CV, enScanAttactRefId.OTHER_DOCUMENTS
'                    'Sandeep [ 21 APRIL 2011 ] -- End 
'                    If Not System.IO.Directory.Exists(ConfigParameter._Object._OtherPath) Then
'                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Cannot find the path : """) & ConfigParameter._Object._OtherPath & Language.getMessage(mstrModuleName, 5, """ for saving file. Please set valid destination  path from configuration."), enMsgBoxStyle.Information)
'                        Exit Sub
'                    Else
'                        If IO.File.Exists(ConfigParameter._Object._OtherPath & "\" & strFile) Then
'                            IO.File.Delete(ConfigParameter._Object._OtherPath & "\" & strFile)
'                        End If
'                        IO.File.Copy(ofdAttachment.FileName, ConfigParameter._Object._OtherPath & "\" & strFile)
'                        objDocument._Employeeunkid = CInt(IIf(mintEmployeeunkid = 0, mintApplicantUnkid, mintEmployeeunkid))
'                        objDocument._Filename = strFile
'                        objDocument._Scanattachrefid = mintRefId
'                        'S.SANDEEP [ 15 SEP 2011 ] -- START
'                        'ENHANCEMENT : CODE OPTIMIZATION
'                        'objDocument._Documentunkid = CInt(txtTextValue.Tag)
'                        If cboSelection.Visible = True And CInt(cboSelection.SelectedValue) > 0 Then
'                            objDocument._Documentunkid = CInt(cboSelection.SelectedValue)
'                        Else
'                        objDocument._Documentunkid = CInt(txtTextValue.Tag)
'                        End If
'                        'S.SANDEEP [ 15 SEP 2011 ] -- END 
'                        If objDocument.Insert() Then
'                            Select Case intRefUnkid
'                                Case enScanAttactRefId.EMPLOYEE_LEAVEFORM
'                                    Call FillList(mintRefId, CInt(IIf(mintEmployeeunkid = 0, mintApplicantUnkid, mintEmployeeunkid)), CInt(cboSelection.SelectedValue))
'                                Case Else
'                            Call FillList(mintRefId, CInt(IIf(mintEmployeeunkid = 0, mintApplicantUnkid, mintEmployeeunkid)), CInt(txtTextValue.Tag))
'                            End Select
'                        Else
'                            If objDocument._Message <> "" Then
'                                eZeeMsgBox.Show(objDocument._Message, enMsgBoxStyle.Information)
'                            End If
'                        End If
'                    End If
'            End Select
'        Catch ex As Exception
'            'Sandeep [ 21 APRIL 2011 ] -- Start
'            'Issue : SCAN & ATTACHMENT ENHANCEMENT
'            'DisplayError.Show("-1", ex.Message, "", mstrModuleName)
'            DisplayError.Show("-1", ex.Message, "AttachDocument", mstrModuleName)
'            'Sandeep [ 21 APRIL 2011 ] -- End 
'        End Try
'    End Sub

'    Private Sub SetProcessVisibility()
'        Try
'            If mblnIsComboVisible = True Then
'                Call FillCombo()
'                Call FillList(mintRefId, CInt(IIf(mintEmployeeunkid = 0, mintApplicantUnkid, mintEmployeeunkid)), CInt(cboSelection.SelectedValue))
'                txtTextValue.Visible = False
'                cboSelection.Visible = True
'            Else
'                txtTextValue.Visible = True
'                cboSelection.Visible = False
'                txtTextValue.Text = mstrTextValue
'                txtTextValue.Tag = mintActionTag
'                Call FillList(mintRefId, CInt(IIf(mintEmployeeunkid = 0, mintApplicantUnkid, mintEmployeeunkid)), CInt(txtTextValue.Tag))
'            End If

'            Select Case mintRefId
'                Case enScanAttactRefId.APPLICANT_CV, enScanAttactRefId.APPLICANT_QUALIFICATION, enScanAttactRefId.APPLICANT_JOBHISTORY
'                    lblEmployeeName.Text = Language.getMessage(mstrModuleName, 7, "Applicant")
'                    txtEmployeeName.Text = mstrApplicantName
'                    'Sandeep [ 21 APRIL 2011 ] -- Start
'                    'Issue : SCAN & ATTACHMENT ENHANCEMENT
'                Case enScanAttactRefId.OTHER_DOCUMENTS
'                    If mintApplicantUnkid > 0 Then
'                        lblEmployeeName.Text = Language.getMessage(mstrModuleName, 7, "Applicant")
'                        txtEmployeeName.Text = mstrApplicantName
'                    ElseIf mintEmployeeunkid > 0 Then
'                        lblEmployeeName.Text = Language.getMessage(mstrModuleName, 8, "Employee")
'                        txtEmployeeName.Text = mstrEmployeeName
'                    End If
'                    'Sandeep [ 21 APRIL 2011 ] -- End 
'                Case Else
'                    lblEmployeeName.Text = Language.getMessage(mstrModuleName, 8, "Employee")
'                    txtEmployeeName.Text = mstrEmployeeName
'            End Select
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "SetProcessVisibility", mstrModuleName)
'        End Try
'    End Sub
'#End Region

'#Region " Button's "
'    Private Sub btnScan_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnScan.Click
'        Dim strFilePath As String = ""
'        Select Case mintRefId
'            Case enScanAttactRefId.EMPLOYEE_IDENTITY, enScanAttactRefId.EMPLOYEE_MEMBERSHIP
'                If ConfigParameter._Object._IdentityPath = "" Then
'                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Identity path is not set. Please set identity path from configuration."), enMsgBoxStyle.Information)
'                    Exit Sub
'                End If
'            Case enScanAttactRefId.EMPLOYEE_QUALIFICATION, enScanAttactRefId.EMPLOYEE_JOBHISTORY, enScanAttactRefId.APPLICANT_QUALIFICATION
'                If ConfigParameter._Object._CertificatePath = "" Then
'                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Certificate path is not set. Please set certificate path from configuration."), enMsgBoxStyle.Information)
'                    Exit Sub
'                End If
'                'Sandeep [ 21 APRIL 2011 ] -- Start
'                'Issue : SCAN & ATTACHMENT ENHANCEMENT
'                'Case enScanAttactRefId.EMPLOYEE_LEAVEFORM, enScanAttactRefId.EMPLOYEE_DISCIPLINE, enScanAttactRefId.APPLICANT_CV
'            Case enScanAttactRefId.EMPLOYEE_LEAVEFORM, enScanAttactRefId.EMPLOYEE_DISCIPLINE, enScanAttactRefId.APPLICANT_CV, enScanAttactRefId.OTHER_DOCUMENTS
'                'Sandeep [ 21 APRIL 2011 ] -- End 
'                If ConfigParameter._Object._OtherPath = "" Then
'                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Other path is not set. Please set other path from configuration."), enMsgBoxStyle.Information)
'                    Exit Sub
'                End If
'        End Select
'        Try
'            Dim frm As New frmCommonImageScanner
'            If frm.DisplayDialog() <> Windows.Forms.DialogResult.Cancel Then
'                Dim img As Image
'                img = frm._Image
'                Dim strFileName As String = Guid.NewGuid().ToString & ".png"
'                strFilePath = ConfigParameter._Object._IdentityPath & "/" & strFileName
'                If img Is Nothing Then Exit Sub
'                img.Save(strFilePath)
'            End If
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "btnScan_Click", mstrModuleName)
'        End Try
'    End Sub

'    Private Sub btnAdd_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAdd.Click
'        Dim strFilePath As String = ""

'        If IsValid() = False Then Exit Sub

'        Try
'            ofdAttachment.Filter = "All Image Formats (*.bmp;*.jpg;*.jpeg;*.gif;*.tif;*.png)|*.bmp;*.jpg;*.jpeg;*.gif;*.tif;*.png|Bitmaps (*.bmp)|*.bmp|GIFs (*.gif)|*.gif|JPEGs (*.jpg)|*.jpg;*.jpeg|TIFs(*.tif)|*.tif|PNGs(*.png)|*.png|All Files (*.*)|*.*"
'            If (ofdAttachment.ShowDialog(Me) = System.Windows.Forms.DialogResult.OK) Then
'                strFilePath = ofdAttachment.FileName
'                Dim strFileName As String = System.IO.Path.GetFileName(strFilePath).ToString
'                Call AttachDocument(mintRefId, strFileName)
'            End If
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "btnAdd_Click", mstrModuleName)
'        End Try
'    End Sub

'    Private Sub btnDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDelete.Click
'        'Sandeep [ 09 Oct 2010 ] -- Start
'        'Issues Reported by Vimal
'        'If lvAttachment.SelectedItems.Count <= 0 Then
'        '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "Please select information to perform operation on it"), enMsgBoxStyle.Information)
'        'End If

'        If lvAttachment.SelectedItems.Count <= 0 Then
'            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "Please select information to perform operation on it"), enMsgBoxStyle.Information)
'            Exit Sub
'        End If
'        'Sandeep [ 09 Oct 2010 ] -- End 
'        Try
'            If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 6, "Are you sure you want to delete."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.Yes Then
'                objDocument._Scanattachtranunkid = CInt(lvAttachment.SelectedItems(0).Tag)
'                objDocument.Delete(CInt(lvAttachment.SelectedItems(0).Tag))
'                Call FillList(mintRefId, CInt(IIf(mintEmployeeunkid = 0, mintApplicantUnkid, mintEmployeeunkid)), CInt(cboSelection.SelectedValue))
'                picDetails.Image = Nothing
'                Select Case mintRefId
'                    Case enScanAttactRefId.EMPLOYEE_IDENTITY, enScanAttactRefId.EMPLOYEE_MEMBERSHIP
'                        If IO.File.Exists(ConfigParameter._Object._IdentityPath & "\" & objDocument._Filename) Then
'                            IO.File.Delete(ConfigParameter._Object._IdentityPath & "\" & objDocument._Filename)
'                        End If
'                    Case enScanAttactRefId.EMPLOYEE_QUALIFICATION, enScanAttactRefId.EMPLOYEE_JOBHISTORY, enScanAttactRefId.APPLICANT_QUALIFICATION
'                        If IO.File.Exists(ConfigParameter._Object._CertificatePath & "\" & objDocument._Filename) Then
'                            IO.File.Delete(ConfigParameter._Object._CertificatePath & "\" & objDocument._Filename)
'                        End If
'                        'Sandeep [ 21 APRIL 2011 ] -- Start
'                        'Issue : SCAN & ATTACHMENT ENHANCEMENT
'                        'Case enScanAttactRefId.EMPLOYEE_LEAVEFORM, enScanAttactRefId.EMPLOYEE_DISCIPLINE, enScanAttactRefId.APPLICANT_CV
'                    Case enScanAttactRefId.EMPLOYEE_LEAVEFORM, enScanAttactRefId.EMPLOYEE_DISCIPLINE, enScanAttactRefId.APPLICANT_CV, enScanAttactRefId.OTHER_DOCUMENTS
'                        'Sandeep [ 21 APRIL 2011 ] -- End 
'                        If IO.File.Exists(ConfigParameter._Object._OtherPath & "\" & objDocument._Filename) Then
'                            IO.File.Delete(ConfigParameter._Object._OtherPath & "\" & objDocument._Filename)
'                        End If
'                End Select
'            End If
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "btnDelete_Click", mstrModuleName)
'        End Try
'    End Sub
'#End Region

'#Region " Controls "
'    Private Sub cboSelection_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboSelection.SelectedIndexChanged
'        Try
'            If CInt(cboSelection.SelectedValue) > 0 Then
'                Call FillList(mintRefId, CInt(IIf(mintEmployeeunkid = 0, mintApplicantUnkid, mintEmployeeunkid)), CInt(cboSelection.SelectedValue))
'            End If
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "cboSelection_SelectedIndexChanged", mstrModuleName)
'        End Try
'    End Sub

'    Private Sub lvAttachment_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles lvAttachment.SelectedIndexChanged
'        Try
'            If lvAttachment.SelectedItems.Count > 0 Then
'                picDetails.ImageLocation = lvAttachment.SelectedItems(0).SubItems(colhAttachment.Index).Text
'            End If
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "lvAttachment_SelectedIndexChanged", mstrModuleName)
'        End Try
'    End Sub

'    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
'        Try
'            Me.Close()
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "btnClose_Click", mstrModuleName)
'        End Try
'    End Sub
'#End Region