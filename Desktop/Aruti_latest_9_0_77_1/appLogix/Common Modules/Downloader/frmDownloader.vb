Imports eZeeCommonLib
Imports Aruti.Data

Public Class frmDownloader

    Dim mstrModuleName As String = "frmDownloader"
    Dim webClient As New System.Net.WebClient()

    Dim mlngDataDownloaded As Long = 0
    Dim mlngTotalData As Long = 0

    Private mstrFileName As String = ""
    Public Property _URL() As String
        Get
            Return mstrFileName
        End Get
        Set(ByVal value As String)
            mstrFileName = value
        End Set
    End Property

    Private mblnDownloaded As Boolean = False
    Public Property _Downloaded() As Boolean
        Get
            Return mblnDownloaded
        End Get
        Set(ByVal value As Boolean)
            mblnDownloaded = value
        End Set
    End Property


    Private mstrDownloadFilePath As String = ""
    Public Property _DownloadFilePath() As String
        Get
            Return mstrDownloadFilePath
        End Get
        Set(ByVal value As String)
            mstrDownloadFilePath = value
        End Set
    End Property

    Private Sub frmDownloader_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        mblnDownloaded = (mlngDataDownloaded = mlngTotalData)
    End Sub

    Private Sub frmDownloader_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            Call Set_Logo(Me, gApplicationType)
            Call DownloadFile()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmDownloader_Load", mstrModuleName)
        End Try
    End Sub

    Public Sub DownloadFile()
        Try
            AddHandler webClient.DownloadFileCompleted, AddressOf DownCompleted
            AddHandler webClient.DownloadProgressChanged, AddressOf ProgressChanged
            webClient.DownloadFileAsync(New Uri(mstrFileName), System.IO.Path.GetTempPath() & System.IO.Path.GetFileName(mstrFileName))
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "DownloadFile", mstrModuleName)
        End Try
    End Sub

    Private Sub ProgressChanged(ByVal sender As Object, ByVal e As System.Net.DownloadProgressChangedEventArgs)
        pb.BeginInvoke(New Progress_del(AddressOf Progress), New Object() {e.ProgressPercentage, e.TotalBytesToReceive, e.BytesReceived})
    End Sub

    Private Sub DownCompleted(ByVal sender As Object, ByVal e As System.ComponentModel.AsyncCompletedEventArgs)
        Me.Close()
        If mlngTotalData = mlngDataDownloaded Then
            Dim strFile1 As String = System.IO.Path.Combine(System.IO.Path.GetTempPath(), System.IO.Path.GetFileName(mstrFileName))
            If Not System.IO.Directory.Exists(System.IO.Path.Combine(AppSettings._Object._ApplicationPath, "Updates")) Then
                System.IO.Directory.CreateDirectory(System.IO.Path.Combine(AppSettings._Object._ApplicationPath, "Updates"))
                Dim strCommand As String = "net share Updates=""" & System.IO.Path.Combine(AppSettings._Object._ApplicationPath, "Updates") & """"
                Shell(strCommand, , True)
            End If
            Dim strFile2 As String = System.IO.Path.Combine(System.IO.Path.Combine(AppSettings._Object._ApplicationPath, "Updates"), System.IO.Path.GetFileName(mstrFileName))
            If System.IO.File.Exists(strFile2) Then System.IO.File.Delete(strFile2)
            System.IO.File.Copy(strFile1, strFile2)
            mstrDownloadFilePath = strFile2
        End If
    End Sub

    'Using Delegates
    Public Delegate Sub Progress_del(ByVal value As Integer, ByVal lngTotalBytes As Long, ByVal lngByteRecieved As Long)
    Public Sub Progress(ByVal value As Integer, ByVal lngTotalBytes As Long, ByVal lngByteRecieved As Long)
        pb.Value = value
        Me.Text = value & " % " & "Downloaded"
        lblData.Text = System.IO.Path.GetFileName(mstrFileName) & " Downloaded " & Format(lngByteRecieved / 1048576, "##0.00") & " MB " & " of " & Format(lngTotalBytes / 1048576, "##0.00") & " MB. " & vbCrLf & value & " % " & "Completed..."
        mlngTotalData = lngTotalBytes
        mlngDataDownloaded = lngByteRecieved
    End Sub

    Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        webClient.CancelAsync()
        Me.Close()
    End Sub

	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Private Sub OtherSettings()
		Try
			Me.SuspendLayout()
		
			Call SetLanguage()
			
			Me.btnCancel.GradientBackColor = GUI._ButttonBackColor 
			Me.btnCancel.GradientForeColor = GUI._ButttonFontColor


			Me.ResumeLayout()
		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "OtherSettings", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetLanguage()
		Try
			Me.Text = Language._Object.getCaption(Me.Name, Me.Text)
			
			Me.pb.Text = Language._Object.getCaption(Me.pb.Name, Me.pb.Text)
			Me.lblData.Text = Language._Object.getCaption(Me.lblData.Name, Me.lblData.Text)
			Me.btnCancel.Text = Language._Object.getCaption(Me.btnCancel.Name, Me.btnCancel.Text)

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetLanguage", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetMessages()
		Try

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class