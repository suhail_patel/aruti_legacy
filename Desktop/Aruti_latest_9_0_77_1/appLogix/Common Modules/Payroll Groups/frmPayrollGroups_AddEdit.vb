﻿Option Strict On

Imports eZeeCommonLib
Imports Aruti.Data
Imports System.Threading

'Last Message Index = 9

Public Class frmPayrollGroups_AddEdit


#Region "Private Variable"

    Private ReadOnly mstrModuleName As String = "frmPayrollGroups_AddEdit"
    Private mblnCancel As Boolean = True
    Private objPayrollGroupMaster As clspayrollgroup_master
    Private menAction As enAction = enAction.ADD_ONE
    Private mintPayrollGroupMasterUnkid As Integer = -1
    Private mintPayrollGroupTypeId As Integer = -1

    'S.SANDEEP [ 11 MAR 2014 ] -- START
    'Dim strExpression As String = "^([a-zA-Z0-9_\-\.']+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$"
    'S.SANDEEP [ 11 MAR 2014 ] -- END

    'S.SANDEEP [ 03 OCT 2012 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Dim dicNotification As New Dictionary(Of String, String)
    Private trd As Thread
    'S.SANDEEP [ 03 OCT 2012 ] -- END

#End Region

#Region " Display Dialog "

    Public Function displayDialog(ByRef intUnkId As Integer, ByVal eAction As enAction, ByVal intGroupTypeId As Integer) As Boolean
        Try
            mintPayrollGroupMasterUnkid = intUnkId
            mintPayrollGroupTypeId = intGroupTypeId
            menAction = eAction
            Me.ShowDialog()
            intUnkId = mintPayrollGroupMasterUnkid
            Return Not mblnCancel
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "displayDialog", mstrModuleName)
        End Try
    End Function

#End Region

#Region "Form's Event"

    Private Sub frmPayrollGroups_AddEdit_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        objPayrollGroupMaster = New clspayrollgroup_master
        Try
            Call Set_Logo(Me, gApplicationType)
            'Anjan (02 Sep 2011)-Start
            'Issue : Including Language Settings.
            Language.setLanguage(Me.Name)
            Call OtherSettings()
            'Anjan (02 Sep 2011)-End 
 'Pinkal (12-Oct-2011) -- Start
            Select Case mintPayrollGroupTypeId
                Case CInt(PayrollGroupType.CostCenter)
                    Me.Text = Language.getMessage(mstrModuleName, 4, "Add/Edit Cost Center Group")
                    Me.gbGroupInfo.Text = Language.getMessage(mstrModuleName, 5, "Cost Center Group Info")
                Case CInt(PayrollGroupType.Vendor)
                    Me.Text = Language.getMessage(mstrModuleName, 6, "Add/Edit Vendor Group")
                    Me.gbGroupInfo.Text = Language.getMessage(mstrModuleName, 7, "Vendor Group Info")
                Case CInt(PayrollGroupType.Bank)
                    Me.Text = Language.getMessage(mstrModuleName, 8, "Add/Edit Bank Group")
                    Me.gbGroupInfo.Text = Language.getMessage(mstrModuleName, 9, "Bank Group Info")
            End Select

            'Pinkal (12-Oct-2011) -- End

            setColor()
            If menAction = enAction.EDIT_ONE Then
                objPayrollGroupMaster._Groupmasterunkid = mintPayrollGroupMasterUnkid
            End If
            GetValue()
            txtCode.Select()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmPayrollGroups_AddEdit_Load", mstrModuleName)
        End Try
    End Sub

    Private Sub frmPayrollGroups_AddEdit_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown
        If e.Control = True And e.KeyCode = Windows.Forms.Keys.S Then
            Call btnSave.PerformClick()
        End If
    End Sub


    Private Sub frmPayrollGroups_AddEdit_KeyPress(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles MyBase.KeyPress
        Try
            If Asc(e.KeyChar) = 13 Then
                SendKeys.Send("{TAB}")
                e.Handled = True
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmPayrollGroups_AddEdit_KeyPress", mstrModuleName)
        End Try
    End Sub

    Private Sub frmPayrollGroups_AddEdit_FormClosed(ByVal sender As System.Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles MyBase.FormClosed
        objPayrollGroupMaster = Nothing
    End Sub
    'Anjan (02 Sep 2011)-Start
    'Issue : Including Language Settings.
    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clspayrollgroup_master.SetMessages()
            objfrm._Other_ModuleNames = "clspayrollgroup_master"
            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub
    'Anjan (02 Sep 2011)-End 

#End Region

#Region "Button's Event"

    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Dim blnFlag As Boolean = False

        'S.SANDEEP [ 11 MAR 2014 ] -- START
        'Dim Expression As New System.Text.RegularExpressions.Regex(strExpression)
        Dim Expression As New System.Text.RegularExpressions.Regex(iEmailRegxExpression)
        'S.SANDEEP [ 11 MAR 2014 ] -- END


        Dim strName As String = String.Empty
        Try

            If mintPayrollGroupTypeId = CInt(PayrollGroupType.CostCenter) Then
                strName = PayrollGroupType.CostCenter.ToString()
            ElseIf mintPayrollGroupTypeId = CInt(PayrollGroupType.Vendor) Then
                strName = PayrollGroupType.Vendor.ToString()
            ElseIf mintPayrollGroupTypeId = CInt(PayrollGroupType.Bank) Then
                strName = PayrollGroupType.Bank.ToString()
            End If
            'Sohail (30 Aug 2010) -- Start
            If txtCode.Text.Trim = "" Then
                eZeeMsgBox.Show(strName & Language.getMessage(mstrModuleName, 1, " Group Code cannot be blank.") & strName & Language.getMessage(mstrModuleName, 2, " Group Code is required information."), enMsgBoxStyle.Information)
                txtCode.Focus()
                Exit Sub
            End If
            'Sohail (30 Aug 2010) -- End
            If Trim(txtName.Text) = "" Then
                eZeeMsgBox.Show(strName & Language.getMessage(mstrModuleName, 3, " Group Name cannot be blank.") & strName & Language.getMessage(mstrModuleName, 14, " Group Name is required information."), enMsgBoxStyle.Information)
                txtName.Focus()
                Exit Sub
                'ElseIf txtwebsite.Text.Trim.Length > 0 Then
                '    If Expression.IsMatch(txtwebsite.Text.Trim) = False Then
                '        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Invalid Website.Please Enter Valid Website"), enMsgBoxStyle.Information)
                '        txtwebsite.Focus()
                '        Exit Sub
                '    End If
            End If


            'S.SANDEEP [ 03 OCT 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            If mintPayrollGroupTypeId = CInt(PayrollGroupType.Bank) Then
                If ConfigParameter._Object._Notify_Bank_Users.Trim.Length > 0 Then
                    Dim objUsr As New clsUserAddEdit : Dim StrMessage As String = String.Empty
                    For Each sId As String In ConfigParameter._Object._Notify_Bank_Users.Split(CChar("||"))(2).Split(CChar(","))
                        objUsr._Userunkid = CInt(sId)
                        StrMessage = Set_Notification(objUsr._Firstname & " " & objUsr._Lastname)
                        If dicNotification.ContainsKey(objUsr._Email) = False Then
                            dicNotification.Add(objUsr._Email, StrMessage)
                        End If

                    Next
                    objUsr = Nothing
                End If
            End If
            'S.SANDEEP [ 03 OCT 2012 ] -- END

            Call SetValue()
            'S.SANDEEP [28-May-2018] -- START
            'ISSUE/ENHANCEMENT : {Audit Trails} 
            objPayrollGroupMaster._FormName = mstrModuleName
            objPayrollGroupMaster._LoginEmployeeunkid = 0
            objPayrollGroupMaster._ClientIP = getIP()
            objPayrollGroupMaster._HostName = getHostName()
            objPayrollGroupMaster._FromWeb = False
            objPayrollGroupMaster._AuditUserId = User._Object._Userunkid
objPayrollGroupMaster._CompanyUnkid = Company._Object._Companyunkid
            objPayrollGroupMaster._AuditDate = ConfigParameter._Object._CurrentDateAndTime
            'S.SANDEEP [28-May-2018] -- END
            If menAction = enAction.EDIT_ONE Then
                blnFlag = objPayrollGroupMaster.Update()
            Else
                blnFlag = objPayrollGroupMaster.Insert()
            End If

            If blnFlag = False And objPayrollGroupMaster._Message <> "" Then
                eZeeMsgBox.Show(objPayrollGroupMaster._Message, enMsgBoxStyle.Information)
            End If

            If blnFlag Then
                mblnCancel = False
                If menAction = enAction.ADD_CONTINUE Then
                    objPayrollGroupMaster = Nothing
                    objPayrollGroupMaster = New clspayrollgroup_master
                    Call GetValue()
                    txtCode.Select()
                Else
                    mintPayrollGroupMasterUnkid = objPayrollGroupMaster._Groupmasterunkid
                    Me.Close()
                End If
            End If

            'S.SANDEEP [ 03 OCT 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            If blnFlag = True Then
                trd = New Thread(AddressOf Send_Notification)
                trd.IsBackground = True
                trd.Start()
            End If
            'S.SANDEEP [ 03 OCT 2012 ] -- END

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnSave_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnOtherLanguage_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnOtherLanguage.Click
        Try
            Dim objFrmLangPopup As New NameLanguagePopup_Form
            Try
                Call objFrmLangPopup.displayDialog(txtName.Text, objPayrollGroupMaster._Groupname1, objPayrollGroupMaster._Groupname2)
            Catch ex As Exception
                DisplayError.Show("-1", ex.Message, "objbtnOtherLanguage_Click", mstrModuleName)
            End Try
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnOtherLanguage_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Me.Close()
    End Sub

#End Region

#Region "Private Methods"

    Private Sub setColor()
        Try
            txtAlias.BackColor = GUI.ColorOptional
            txtCode.BackColor = GUI.ColorOptional
            txtName.BackColor = GUI.ColorComp
            txtDescription.BackColor = GUI.ColorOptional
            txtwebsite.BackColor = GUI.ColorOptional


            'Pinkal (12-Oct-2011) -- Start
            If mintPayrollGroupTypeId = CInt(PayrollGroupType.Bank) Then
                txtSwiftcode.BackColor = GUI.ColorComp
                txtReferenceNo.BackColor = GUI.ColorComp 'Sohail (31 Mar 2014)
            End If
            'Pinkal (12-Oct-2011) -- End

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "setColor", mstrModuleName)
        End Try
    End Sub

    Private Sub GetValue()
        Try
            txtAlias.Text = objPayrollGroupMaster._Groupalias
            txtCode.Text = objPayrollGroupMaster._Groupcode
            txtName.Text = objPayrollGroupMaster._Groupname
            txtDescription.Text = objPayrollGroupMaster._Description
            txtwebsite.Text = objPayrollGroupMaster._Website


            'Pinkal (12-Oct-2011) -- Start

            If mintPayrollGroupTypeId = CInt(PayrollGroupType.Bank) Then
                txtSwiftcode.Text = objPayrollGroupMaster._Swiftcode
                txtReferenceNo.Text = objPayrollGroupMaster._ReferenceNo  'Sohail (31 Mar 2014)
            End If

            'Pinkal (12-Oct-2011) -- End


        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetValue", mstrModuleName)
        End Try
    End Sub

    Private Sub SetValue()
        Try
            objPayrollGroupMaster._Groupalias = txtAlias.Text.Trim
            objPayrollGroupMaster._Groupcode = txtCode.Text.Trim
            objPayrollGroupMaster._Groupname = txtName.Text.Trim
            objPayrollGroupMaster._Description = txtDescription.Text.Trim
            objPayrollGroupMaster._Website = txtwebsite.Text.Trim
            objPayrollGroupMaster._Grouptype_Id = mintPayrollGroupTypeId


        'Pinkal (12-Oct-2011) -- Start

            If mintPayrollGroupTypeId = CInt(PayrollGroupType.Bank) Then
                objPayrollGroupMaster._Swiftcode = txtSwiftcode.Text.Trim
                objPayrollGroupMaster._ReferenceNo = txtReferenceNo.Text.Trim     'Sohail (31 Mar 2014)
            End If

            'Pinkal (12-Oct-2011) -- End


Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetValue", mstrModuleName)
        End Try
    End Sub

    'S.SANDEEP [ 03 OCT 2012 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Private Function Set_Notification(ByVal StrUserName As String) As String
        Dim StrMessage As New System.Text.StringBuilder
        Dim blnFlag As Boolean = False
        Try
            StrMessage.Append("<HTML><BODY>")
            StrMessage.Append(vbCrLf)
            If ConfigParameter._Object._Notify_Bank_Users.Trim.Length > 0 Then
                StrMessage.Append("<p><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#333399; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>")

                'Gajanan [27-Mar-2019] -- Start
                'Enhancement - Change Email Language

                'StrMessage.Append("Dear <b>" & StrUserName & "</b></span></p>")
                StrMessage.Append(Language.getMessage(mstrModuleName, 14, "Dear") & " " & "<b>" & getTitleCase(StrUserName) & "</b></span></p>")
                'Gajanan [27-Mar-2019] -- End

                StrMessage.Append(vbCrLf)
                StrMessage.Append("<p><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#333399; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>")

                'Gajanan [27-Mar-2019] -- Start
                'Enhancement - Change Email Language
                'StrMessage.Append("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; This is to inform you that changes have been made for bank. Following information has been changed by user : <b>" & User._Object._Firstname & " " & User._Object._Lastname & "</b></span></p>")
                StrMessage.Append(Language.getMessage(mstrModuleName, 15, "This is to inform you that changes have been made for bank. Following information has been changed by user" & " <b>" & getTitleCase(User._Object._Firstname & " " & User._Object._Lastname) & "</b></span></p>"))

                'Gajanan [27-Mar-2019] -- End



                StrMessage.Append("<p><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#333399; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>")
                StrMessage.Append("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; from Machine : <b>" & getHostName.ToString & "</b> and IPAddress : <b>" & getIP.ToString & "</b></span></p>")
                StrMessage.Append(vbCrLf)
                StrMessage.Append("<TABLE border = '1' WIDTH = '30%'>")
                StrMessage.Append(vbCrLf)
                StrMessage.Append("<TR WIDTH = '30%' bgcolor= 'SteelBlue'>")
                StrMessage.Append(vbCrLf)
                StrMessage.Append("<TD align = 'LEFT' STYLE='WIDTH:10%'><b><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#FFFFFF; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>" & Language.getMessage(mstrModuleName, 10, "Field") & "</span></b></TD>")
                StrMessage.Append("<TD align = 'LEFT' STYLE='WIDTH:10%'><b><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#FFFFFF; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>" & Language.getMessage(mstrModuleName, 11, "Old Value") & "</span></b></TD>")
                StrMessage.Append("<TD align = 'LEFT' STYLE='WIDTH:10%'><b><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#FFFFFF; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>" & Language.getMessage(mstrModuleName, 12, "New Value") & "</span></b></TD>")
                StrMessage.Append(vbCrLf)
                StrMessage.Append("</TR>")
                For Each sId As String In ConfigParameter._Object._Notify_Bank_Users.Split(CChar("||"))(0).Split(CChar(","))
                    Select Case CInt(sId)
                        Case enNotificationBank.BANK_GROUP
                            If objPayrollGroupMaster._Groupcode <> txtCode.Text Then
                                StrMessage.Append("<TR WIDTH = '30%'>" & vbCrLf)
                                StrMessage.Append(vbCrLf)
                                StrMessage.Append("<TD align = 'LEFT' WIDTH = '10%'><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#000000; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>" & lblCode.Text & "</span></TD>")
                                StrMessage.Append("<TD align = 'LEFT' WIDTH = '10%'><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#000000; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>" & objPayrollGroupMaster._Groupcode & "</span></TD>")
                                StrMessage.Append("<TD align = 'LEFT' WIDTH = '10%'><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#000000; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>" & txtCode.Text.Trim & "</span></TD>")
                                StrMessage.Append("</TR>" & vbCrLf)
                                blnFlag = True
                            End If

                            If objPayrollGroupMaster._Groupname <> txtName.Text Then
                                StrMessage.Append("<TR WIDTH = '30%'>" & vbCrLf)
                                StrMessage.Append(vbCrLf)
                                StrMessage.Append("<TD align = 'LEFT' WIDTH = '10%'><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#000000; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>" & lblName.Text & "</span></TD>")
                                StrMessage.Append("<TD align = 'LEFT' WIDTH = '10%'><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#000000; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>" & objPayrollGroupMaster._Groupname & "</span></TD>")
                                StrMessage.Append("<TD align = 'LEFT' WIDTH = '10%'><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#000000; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>" & txtName.Text.Trim & "</span></TD>")
                                StrMessage.Append("</TR>" & vbCrLf)
                                blnFlag = True
                            End If

                            If objPayrollGroupMaster._Swiftcode <> txtSwiftcode.Text Then
                                StrMessage.Append("<TR WIDTH = '30%'>" & vbCrLf)
                                StrMessage.Append(vbCrLf)
                                StrMessage.Append("<TD align = 'LEFT' WIDTH = '10%'><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#000000; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>" & lblSwiftcode.Text & "</span></TD>")
                                StrMessage.Append("<TD align = 'LEFT' WIDTH = '10%'><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#000000; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>" & objPayrollGroupMaster._Swiftcode & "</span></TD>")
                                StrMessage.Append("<TD align = 'LEFT' WIDTH = '10%'><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#000000; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>" & txtSwiftcode.Text.Trim & "</span></TD>")
                                StrMessage.Append("</TR>" & vbCrLf)
                                blnFlag = True
                            End If

                            'Sohail (31 Mar 2014) -- Start
                            'ENHANCEMENT - Show Bank Reference No on Bank Payment List.
                            If objPayrollGroupMaster._ReferenceNo <> txtReferenceNo.Text Then
                                StrMessage.Append("<TR WIDTH = '30%'>" & vbCrLf)
                                StrMessage.Append(vbCrLf)
                                StrMessage.Append("<TD align = 'LEFT' WIDTH = '10%'><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#000000; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>" & lblReferenceNo.Text & "</span></TD>")
                                StrMessage.Append("<TD align = 'LEFT' WIDTH = '10%'><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#000000; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>" & objPayrollGroupMaster._ReferenceNo & "</span></TD>")
                                StrMessage.Append("<TD align = 'LEFT' WIDTH = '10%'><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#000000; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>" & txtReferenceNo.Text.Trim & "</span></TD>")
                                StrMessage.Append("</TR>" & vbCrLf)
                                blnFlag = True
                            End If
                            'Sohail (31 Mar 2014) -- End

                    End Select
                Next
            End If

            StrMessage.Append("</TABLE>")

            'Gajanan [27-Mar-2019] -- Start
            'Enhancement - Change Email Language
            'StrMessage.Append("<p><span style='padding-top:25px;font-size:10.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#333399;margin-left:0px;margin-right:0px;margin-top:0px;margin-bottom:10px;'><b>&nbsp;&nbsp;&nbsp;&nbsp;&quot;POWERED BY ARUTI HR & PAYROLL MANAGEMENT SOFTWARE.&quot;</b>")
            StrMessage.Append("<p><span style='padding-top:25px;font-size:10.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#333399;margin-left:0px;margin-right:0px;margin-top:0px;margin-bottom:10px;'><b>&quot;POWERED BY ARUTI HR & PAYROLL MANAGEMENT SOFTWARE.&quot;</b>")
            'Gajanan [27-Mar-2019] -- End




            StrMessage.Append("</span></p>")
            StrMessage.Append("</BODY></HTML>")

            If blnFlag = False Then
                StrMessage = StrMessage.Remove(0, StrMessage.Length)
            End If

            Return StrMessage.ToString
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "Set_Notification", mstrModuleName)
            Return ""
        End Try
    End Function

    Private Sub Send_Notification()
        Try
            If menAction = enAction.EDIT_ONE Then
                If dicNotification.Keys.Count > 0 Then
                    Dim objSendMail As New clsSendMail
                    For Each sKey As String In dicNotification.Keys
                        If dicNotification(sKey).Trim.Length > 0 Then
                            objSendMail._ToEmail = sKey
                            objSendMail._Subject = Language.getMessage(mstrModuleName, 13, "Notification of Changes in Bank Group.")
                            objSendMail._Message = dicNotification(sKey)
                            'objSendMail._FormName = "" 'Please pass Form Name for WEB
                            'objSendMail._LoginEmployeeunkid = -1
                            With objSendMail
                                ._FormName = mstrModuleName
                                ._LoginEmployeeunkid = 0
                                ._ClientIP = getIP()
                                ._HostName = getHostName()
                                ._FromWeb = False
                                ._AuditUserId = User._Object._Userunkid
._CompanyUnkid = Company._Object._Companyunkid
                                ._AuditDate = ConfigParameter._Object._CurrentDateAndTime
                            End With
                            objSendMail._OperationModeId = enLogin_Mode.DESKTOP
                            objSendMail._UserUnkid = User._Object._Userunkid
                            objSendMail._SenderAddress = User._Object._Email
                            objSendMail._ModuleRefId = clsSendMail.enAT_VIEW_TYPE.PAYROLL_MGT
                   
                            'Sohail (30 Nov 2017) -- Start
                            'SUMATRA Enhancement – SUMATRA – issue # 0001669: Additional feature for Exchange Server configuration setting in 70.1.
                            'objSendMail.SendMail()
                            objSendMail.SendMail(Company._Object._Companyunkid)
                            'Sohail (30 Nov 2017) -- End
                        End If
                    Next
                End If
            End If
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "Send_Notification", mstrModuleName)
        End Try
    End Sub
    'S.SANDEEP [ 03 OCT 2012 ] -- END

#End Region

	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Private Sub OtherSettings()
		Try
			Me.SuspendLayout()
			
			Call SetLanguage()
			
			Me.gbGroupInfo.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbGroupInfo.ForeColor = GUI._eZeeContainerHeaderForeColor 


			Me.btnClose.GradientBackColor = GUI._ButttonBackColor 
			Me.btnClose.GradientForeColor = GUI._ButttonFontColor

			Me.btnSave.GradientBackColor = GUI._ButttonBackColor 
			Me.btnSave.GradientForeColor = GUI._ButttonFontColor


			Me.ResumeLayout()
		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "OtherSettings", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetLanguage()
		Try
			Me.Text = Language._Object.getCaption(Me.Name, Me.Text)
			
			Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
			Me.btnSave.Text = Language._Object.getCaption(Me.btnSave.Name, Me.btnSave.Text)
			Me.lblDescription.Text = Language._Object.getCaption(Me.lblDescription.Name, Me.lblDescription.Text)
			Me.lblName.Text = Language._Object.getCaption(Me.lblName.Name, Me.lblName.Text)
			Me.lblAlias.Text = Language._Object.getCaption(Me.lblAlias.Name, Me.lblAlias.Text)
			Me.lblCode.Text = Language._Object.getCaption(Me.lblCode.Name, Me.lblCode.Text)
			Me.lblwebsite.Text = Language._Object.getCaption(Me.lblwebsite.Name, Me.lblwebsite.Text)
			Me.gbGroupInfo.Text = Language._Object.getCaption(Me.gbGroupInfo.Name, Me.gbGroupInfo.Text)
			Me.lblSwiftcode.Text = Language._Object.getCaption(Me.lblSwiftcode.Name, Me.lblSwiftcode.Text)
			Me.lblReferenceNo.Text = Language._Object.getCaption(Me.lblReferenceNo.Name, Me.lblReferenceNo.Text)

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetLanguage", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, " Group Code cannot be blank.")
			Language.setMessage(mstrModuleName, 2, " Group Code is required information.")
			Language.setMessage(mstrModuleName, 3, " Group Name cannot be blank.")
			Language.setMessage(mstrModuleName, 4, "Add/Edit Cost Center Group")
			Language.setMessage(mstrModuleName, 5, "Cost Center Group Info")
			Language.setMessage(mstrModuleName, 6, "Add/Edit Vendor Group")
			Language.setMessage(mstrModuleName, 7, "Vendor Group Info")
			Language.setMessage(mstrModuleName, 8, "Add/Edit Bank Group")
			Language.setMessage(mstrModuleName, 9, "Bank Group Info")
			Language.setMessage(mstrModuleName, 10, "Field")
			Language.setMessage(mstrModuleName, 11, "Old Value")
			Language.setMessage(mstrModuleName, 12, "New Value")
			Language.setMessage(mstrModuleName, 13, "Notification of Changes in Bank Group.")
			Language.setMessage(mstrModuleName, 14, " Group Name is required information.")
			Language.setMessage(mstrModuleName, 15, "This is to inform you that changes have been made for bank. Following information has been changed by user" & " <b>" & getTitleCase(User._Object._Firstname & " " & User._Object._Lastname) & "</b></span></p>")
			Language.setMessage(mstrModuleName, 16, "from Machine")
			Language.setMessage(mstrModuleName, 17, "and IPAddress")
			Language.setMessage(mstrModuleName, 14, "Dear")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class