﻿Option Strict On

Imports eZeeCommonLib
Imports Aruti.Data

'Last Message Index = 3

Public Class frmCityList

#Region "Private Variable"

    Private objCityMaster As clscity_master
    Private ReadOnly mstrModuleName As String = "frmCityList"

#End Region

#Region "Form's Event"

    Private Sub frmCityList_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        objCityMaster = New clscity_master
        Try
            Call Set_Logo(Me, gApplicationType)
            Language.setLanguage(Me.Name)

            'Anjan (02 Sep 2011)-Start
            'Issue : Including Language Settings.
            Call OtherSettings()
            'Anjan (02 Sep 2011)-End 

            Call SetVisibility()
            FillCombo()
            'FillState()
            fillList()
            If lvCity.Items.Count > 0 Then lvCity.Items(0).Selected = True
            lvCity.Select()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmCityList_Load", mstrModuleName)
        End Try
    End Sub

    Private Sub frmCityList_KeyUp(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles MyBase.KeyUp
        Try
            If e.KeyCode = Keys.Delete And lvCity.Focused = True Then
                'Sohail (28 Jun 2011) -- Start
                'Issue : Delete event fired even if there is no delete privilege
                'btnDelete_Click(sender, e)
                Call btnDelete.PerformClick()
                'Sohail (28 Jun 2011) -- End
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmCityList_KeyUp", mstrModuleName)
        End Try
    End Sub

    Private Sub frmCityList_FormClosed(ByVal sender As System.Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles MyBase.FormClosed
        objCityMaster = Nothing
    End Sub
    'Anjan (02 Sep 2011)-Start
    'Issue : Including Language Settings.
    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clscity_master.SetMessages()
            objfrm._Other_ModuleNames = "clscity_master"
            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub
    'Anjan (02 Sep 2011)-End 

#End Region

#Region "dropdown's Event"

    Private Sub cboCountry_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboCountry.SelectedIndexChanged
        Try
            FillState()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboCountry_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

#End Region

#Region "Button's Event"

    Private Sub btnNew_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNew.Click
        Try
            Dim objfrmCity_AddEdit As New frmCity_AddEdit
            If objfrmCity_AddEdit.displayDialog(-1, enAction.ADD_CONTINUE) Then
                fillList()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnNew_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnEdit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEdit.Click
        Try
            If lvCity.SelectedItems.Count < 1 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please select City from the list to perform further operation."), enMsgBoxStyle.Information) '?1
                lvCity.Select()
                Exit Sub
            End If
            Dim objfrmCity_AddEdit As New frmCity_AddEdit
            Try
                Dim intSelectedIndex As Integer
                intSelectedIndex = lvCity.SelectedItems(0).Index
                If objfrmCity_AddEdit.displayDialog(CInt(lvCity.SelectedItems(0).Tag), enAction.EDIT_ONE) Then
                    Call fillList()
                End If
                objfrmCity_AddEdit = Nothing

                lvCity.Items(intSelectedIndex).Selected = True
                lvCity.EnsureVisible(intSelectedIndex)
                lvCity.Select()
            Catch ex As Exception
                Call DisplayError.Show("-1", ex.Message, "btnEdit_Click", mstrModuleName)
            Finally
                If objfrmCity_AddEdit IsNot Nothing Then objfrmCity_AddEdit.Dispose()
            End Try
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnEdit_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDelete.Click
        If lvCity.SelectedItems.Count < 1 Then
            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please select City from the list to perform further operation."), enMsgBoxStyle.Information) '?1
            lvCity.Select()
            Exit Sub
        End If

        'S.SANDEEP [28-May-2018] -- START
        'ISSUE/ENHANCEMENT : {Audit Trails} 
        objCityMaster._FormName = mstrModuleName
        objCityMaster._LoginEmployeeunkid = 0
        objCityMaster._ClientIP = getIP()
        objCityMaster._HostName = getHostName()
        objCityMaster._FromWeb = False
        objCityMaster._AuditUserId = User._Object._Userunkid
objCityMaster._CompanyUnkid = Company._Object._Companyunkid
        objCityMaster._AuditDate = ConfigParameter._Object._CurrentDateAndTime
        'S.SANDEEP [28-May-2018] -- END

        If objCityMaster.isUsed(CInt(lvCity.SelectedItems(0).Tag)) Then
            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Sorry, You cannot delete this City. Reason: This City is in use."), enMsgBoxStyle.Information) '?2
            lvCity.Select()
            Exit Sub
        End If
        Try
            Dim intSelectedIndex As Integer
            intSelectedIndex = lvCity.SelectedItems(0).Index

            If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Are you sure you want to delete this City?"), CType(enMsgBoxStyle.Question + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.Yes Then

                'S.SANDEEP [28-May-2018] -- START
                'ISSUE/ENHANCEMENT : {Audit Trails} 
                objCityMaster._FormName = mstrModuleName
                objCityMaster._LoginEmployeeunkid = 0
                objCityMaster._ClientIP = getIP()
                objCityMaster._HostName = getHostName()
                objCityMaster._FromWeb = False
                objCityMaster._AuditUserId = User._Object._Userunkid
objCityMaster._CompanyUnkid = Company._Object._Companyunkid
                objCityMaster._AuditDate = ConfigParameter._Object._CurrentDateAndTime
                'S.SANDEEP [28-May-2018] -- END

                objCityMaster.Delete(CInt(lvCity.SelectedItems(0).Tag))
                lvCity.SelectedItems(0).Remove()

                If lvCity.Items.Count <= 0 Then
                    Exit Try
                End If

                If lvCity.Items.Count = intSelectedIndex Then
                    intSelectedIndex = lvCity.Items.Count - 1
                    lvCity.Items(intSelectedIndex).Selected = True
                    lvCity.EnsureVisible(intSelectedIndex)
                ElseIf lvCity.Items.Count <> 0 Then
                    lvCity.Items(intSelectedIndex).Selected = True
                    lvCity.EnsureVisible(intSelectedIndex)
                End If
            End If
            lvCity.Select()
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "btnDelete_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnSearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearch.Click
        Try
            fillList()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearch_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnReset.Click
        Try
            cboCountry.SelectedIndex = 0
            cboState.SelectedIndex = 0
            fillList()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnReset_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Me.Close()
    End Sub

#End Region

#Region " Private Methods "

    Private Sub fillList()
        Dim strSearching As String = String.Empty
        Dim dsCityList As New DataSet
        Dim dtState As DataTable
        Try
            'Anjan (25 Oct 2012)-Start
            'ENHANCEMENT : TRA COMMENTS on Andrew's Request
            If User._Object.Privilege._AllowToViewCityList = False Then Exit Sub
            'Anjan (25 Oct 2012)-End 



            'S.SANDEEP [28-May-2018] -- START
            'ISSUE/ENHANCEMENT : {Audit Trails} 
            objCityMaster._FormName = mstrModuleName
            objCityMaster._LoginEmployeeunkid = 0
            objCityMaster._ClientIP = getIP()
            objCityMaster._HostName = getHostName()
            objCityMaster._FromWeb = False
            objCityMaster._AuditUserId = User._Object._Userunkid
objCityMaster._CompanyUnkid = Company._Object._Companyunkid
            objCityMaster._AuditDate = ConfigParameter._Object._CurrentDateAndTime
            'S.SANDEEP [28-May-2018] -- END

            dsCityList = objCityMaster.GetList("City")

            If CInt(cboCountry.SelectedValue) > 0 Then
                strSearching = "AND countryunkid=" & CInt(cboCountry.SelectedValue) & " AND stateunkid=" & CInt(cboState.SelectedValue)
            End If

            If strSearching.Length > 0 Then
                strSearching = strSearching.Substring(3)
                dtState = New DataView(dsCityList.Tables("City"), strSearching, "", DataViewRowState.CurrentRows).ToTable
            Else
                dtState = dsCityList.Tables("City")
            End If

            Dim lvItem As ListViewItem

            lvCity.Items.Clear()
            For Each drRow As DataRow In dtState.Rows
                lvItem = New ListViewItem
                lvItem.Text = drRow("country").ToString
                lvItem.Tag = drRow("cityunkid")
                lvItem.SubItems.Add(drRow("state").ToString)
                lvItem.SubItems.Add(drRow("code").ToString)
                lvItem.SubItems.Add(drRow("name").ToString)
                lvCity.Items.Add(lvItem)
            Next

            If lvCity.Items.Count > 16 Then
                colhCityName.Width = 220 - 18
            Else
                colhCityName.Width = 220
            End If

        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "fillList", mstrModuleName)
        Finally
            dsCityList.Dispose()
        End Try
    End Sub

    Private Sub FillCombo()
        Try
            Dim objMasterdata As New clsMasterData
            Dim dsCountry As DataSet = objMasterdata.getCountryList("Country", True)
            cboCountry.ValueMember = "countryunkid"
            cboCountry.DisplayMember = "country_name"
            cboCountry.DataSource = dsCountry.Tables(0)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        End Try
    End Sub

    Private Sub FillState()
        Try
            Dim objStateMaster As New clsstate_master
            Dim dsState As DataSet = Nothing
            If CInt(cboCountry.SelectedValue) > 0 Then
                dsState = objStateMaster.GetList("State", True, False, CInt(cboCountry.SelectedValue))
            Else
                dsState = objStateMaster.GetList("State", True, True)
            End If

            cboState.DisplayMember = "name"
            cboState.ValueMember = "stateunkid"
            cboState.DataSource = dsState.Tables(0)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillState", mstrModuleName)
        End Try
    End Sub


    Private Sub SetVisibility()

        Try
            btnNew.Enabled = User._Object.Privilege._AddCity
            btnEdit.Enabled = User._Object.Privilege._EditCity
            btnDelete.Enabled = User._Object.Privilege._DeleteCity
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetVisibility", mstrModuleName)
        End Try

    End Sub


#End Region


    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Private Sub OtherSettings()
        Try
            Me.SuspendLayout()
           
            Call SetLanguage()

            Me.gbCity.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbCity.ForeColor = GUI._eZeeContainerHeaderForeColor


            Me.eZeeHeader.GradientColor1 = GUI._HeaderBackColor1
            Me.eZeeHeader.GradientColor2 = GUI._HeaderBackColor2
            Me.eZeeHeader.BorderColor = GUI._HeaderBorderColor
            Me.eZeeHeader.HeaderTextForeColor = GUI._HeaderTitleFontColor
            Me.eZeeHeader.DescriptionForeColor = GUI._HeaderMessageFontColor


            Me.btnDelete.GradientBackColor = GUI._ButttonBackColor
            Me.btnDelete.GradientForeColor = GUI._ButttonFontColor

            Me.btnEdit.GradientBackColor = GUI._ButttonBackColor
            Me.btnEdit.GradientForeColor = GUI._ButttonFontColor

            Me.btnNew.GradientBackColor = GUI._ButttonBackColor
            Me.btnNew.GradientForeColor = GUI._ButttonFontColor

            Me.btnClose.GradientBackColor = GUI._ButttonBackColor
            Me.btnClose.GradientForeColor = GUI._ButttonFontColor


            Me.ResumeLayout()
        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "OtherSettings", mstrModuleName)
        End Try
    End Sub


    Private Sub SetLanguage()
        Try
            Me.Text = Language._Object.getCaption(Me.Name, Me.Text)

            Me.colhCityCode.Text = Language._Object.getCaption(CStr(Me.colhCityCode.Tag), Me.colhCityCode.Text)
            Me.colhCityName.Text = Language._Object.getCaption(CStr(Me.colhCityName.Tag), Me.colhCityName.Text)
            Me.colhCountry.Text = Language._Object.getCaption(CStr(Me.colhCountry.Tag), Me.colhCountry.Text)
            Me.btnDelete.Text = Language._Object.getCaption(Me.btnDelete.Name, Me.btnDelete.Text)
            Me.btnEdit.Text = Language._Object.getCaption(Me.btnEdit.Name, Me.btnEdit.Text)
            Me.btnNew.Text = Language._Object.getCaption(Me.btnNew.Name, Me.btnNew.Text)
            Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
            Me.lblCountry.Text = Language._Object.getCaption(Me.lblCountry.Name, Me.lblCountry.Text)
            Me.gbCity.Text = Language._Object.getCaption(Me.gbCity.Name, Me.gbCity.Text)
            Me.lblState.Text = Language._Object.getCaption(Me.lblState.Name, Me.lblState.Text)
            Me.eZeeHeader.Title = Language._Object.getCaption(Me.eZeeHeader.Name & "_Title", Me.eZeeHeader.Title)
            Me.eZeeHeader.Message = Language._Object.getCaption(Me.eZeeHeader.Name & "_Message", Me.eZeeHeader.Message)
            Me.colhState.Text = Language._Object.getCaption(CStr(Me.colhState.Tag), Me.colhState.Text)

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
        End Try
    End Sub


    Private Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Please select City from the list to perform further operation.")
            Language.setMessage(mstrModuleName, 2, "Sorry, You cannot delete this City. Reason: This City is in use.")
            Language.setMessage(mstrModuleName, 3, "Are you sure you want to delete this City?")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class