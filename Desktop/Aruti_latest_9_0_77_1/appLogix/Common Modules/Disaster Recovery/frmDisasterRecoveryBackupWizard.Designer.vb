﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmDisasterRecoveryBackupWizard
    Inherits eZee.Common.eZeeForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Me.pnlMain = New System.Windows.Forms.Panel
        Me.ewWizard = New eZee.Common.eZeeWizard
        Me.ewpPage3 = New eZee.Common.eZeeWizardPage(Me.components)
        Me.gbTaskInfo = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.txtEmail3 = New eZee.TextBox.AlphanumericTextBox
        Me.txtEmail2 = New eZee.TextBox.AlphanumericTextBox
        Me.txtEmail1 = New eZee.TextBox.AlphanumericTextBox
        Me.Label1 = New System.Windows.Forms.Label
        Me.lblNotes = New System.Windows.Forms.Label
        Me.cboCompany = New System.Windows.Forms.ComboBox
        Me.lblCompany = New System.Windows.Forms.Label
        Me.lblTaskName = New System.Windows.Forms.Label
        Me.Label12 = New System.Windows.Forms.Label
        Me.ewpPage2 = New eZee.Common.eZeeWizardPage(Me.components)
        Me.lblMsg2 = New System.Windows.Forms.Label
        Me.txtCPWD = New System.Windows.Forms.TextBox
        Me.txtPWD = New System.Windows.Forms.TextBox
        Me.txtUserName = New System.Windows.Forms.TextBox
        Me.lblConfirmPassword = New System.Windows.Forms.Label
        Me.lblPassword = New System.Windows.Forms.Label
        Me.lblUserName = New System.Windows.Forms.Label
        Me.lblTitle2 = New System.Windows.Forms.Label
        Me.lblMinuts = New System.Windows.Forms.Label
        Me.nudMinutes = New System.Windows.Forms.NumericUpDown
        Me.lblEvery = New System.Windows.Forms.Label
        Me.dtpTime = New System.Windows.Forms.DateTimePicker
        Me.objOpenFile = New eZee.Common.eZeeGradientButton
        Me.txtBackupFolder = New eZee.TextBox.AlphanumericTextBox
        Me.lblBackupFolder = New System.Windows.Forms.Label
        Me.lblTime = New System.Windows.Forms.Label
        Me.rbtnMonthly = New System.Windows.Forms.RadioButton
        Me.rbtnWeekly = New System.Windows.Forms.RadioButton
        Me.rbtnDaily = New System.Windows.Forms.RadioButton
        Me.lblTaskType = New System.Windows.Forms.Label
        Me.lblWeekDay = New System.Windows.Forms.Label
        Me.cboDay = New System.Windows.Forms.ComboBox
        Me.lblDay = New System.Windows.Forms.Label
        Me.cboWeekDay = New System.Windows.Forms.ComboBox
        Me.ewpPage4 = New eZee.Common.eZeeWizardPage(Me.components)
        Me.chkRunNow = New System.Windows.Forms.CheckBox
        Me.lblTitle3 = New System.Windows.Forms.Label
        Me.lblMsg3 = New System.Windows.Forms.Label
        Me.ewpPage1 = New eZee.Common.eZeeWizardPage(Me.components)
        Me.lblMsg1 = New System.Windows.Forms.Label
        Me.lblTitle1 = New System.Windows.Forms.Label
        Me.txtTaskName = New System.Windows.Forms.TextBox
        Me.pnlMain.SuspendLayout()
        Me.ewWizard.SuspendLayout()
        Me.ewpPage3.SuspendLayout()
        Me.gbTaskInfo.SuspendLayout()
        Me.ewpPage2.SuspendLayout()
        CType(Me.nudMinutes, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.ewpPage4.SuspendLayout()
        Me.ewpPage1.SuspendLayout()
        Me.SuspendLayout()
        '
        'pnlMain
        '
        Me.pnlMain.Controls.Add(Me.ewWizard)
        Me.pnlMain.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pnlMain.Location = New System.Drawing.Point(0, 0)
        Me.pnlMain.Name = "pnlMain"
        Me.pnlMain.Size = New System.Drawing.Size(548, 381)
        Me.pnlMain.TabIndex = 0
        '
        'ewWizard
        '
        Me.ewWizard.Controls.Add(Me.ewpPage3)
        Me.ewWizard.Controls.Add(Me.ewpPage2)
        Me.ewWizard.Controls.Add(Me.ewpPage1)
        Me.ewWizard.Controls.Add(Me.ewpPage4)
        Me.ewWizard.Dock = System.Windows.Forms.DockStyle.Fill
        Me.ewWizard.HeaderFont = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ewWizard.HeaderImage = Global.Aruti.Data.My.Resources.Resources.Aruti_Wiz
        Me.ewWizard.HeaderTitleFont = New System.Drawing.Font("Tahoma", 10.25!, System.Drawing.FontStyle.Bold)
        Me.ewWizard.Location = New System.Drawing.Point(0, 0)
        Me.ewWizard.Name = "ewWizard"
        Me.ewWizard.Pages.AddRange(New eZee.Common.eZeeWizardPage() {Me.ewpPage1, Me.ewpPage2, Me.ewpPage3, Me.ewpPage4})
        Me.ewWizard.Size = New System.Drawing.Size(548, 381)
        Me.ewWizard.TabIndex = 2
        Me.ewWizard.WelcomeFont = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ewWizard.WelcomeImage = Global.Aruti.Data.My.Resources.Resources.Aruti_Wiz
        Me.ewWizard.WelcomeTitleFont = New System.Drawing.Font("Tahoma", 18.25!, System.Drawing.FontStyle.Bold)
        '
        'ewpPage3
        '
        Me.ewpPage3.Controls.Add(Me.gbTaskInfo)
        Me.ewpPage3.Controls.Add(Me.Label12)
        Me.ewpPage3.Location = New System.Drawing.Point(0, 0)
        Me.ewpPage3.Name = "ewpPage3"
        Me.ewpPage3.Size = New System.Drawing.Size(548, 333)
        Me.ewpPage3.Style = eZee.Common.eZeeWizardPageStyle.eZeeStyle
        Me.ewpPage3.TabIndex = 1
        '
        'gbTaskInfo
        '
        Me.gbTaskInfo.BorderColor = System.Drawing.Color.Black
        Me.gbTaskInfo.Checked = False
        Me.gbTaskInfo.CollapseAllExceptThis = False
        Me.gbTaskInfo.CollapsedHoverImage = Nothing
        Me.gbTaskInfo.CollapsedNormalImage = Nothing
        Me.gbTaskInfo.CollapsedPressedImage = Nothing
        Me.gbTaskInfo.CollapseOnLoad = False
        Me.gbTaskInfo.Controls.Add(Me.txtTaskName)
        Me.gbTaskInfo.Controls.Add(Me.txtEmail3)
        Me.gbTaskInfo.Controls.Add(Me.txtEmail2)
        Me.gbTaskInfo.Controls.Add(Me.txtEmail1)
        Me.gbTaskInfo.Controls.Add(Me.Label1)
        Me.gbTaskInfo.Controls.Add(Me.lblNotes)
        Me.gbTaskInfo.Controls.Add(Me.cboCompany)
        Me.gbTaskInfo.Controls.Add(Me.lblCompany)
        Me.gbTaskInfo.Controls.Add(Me.lblTaskName)
        Me.gbTaskInfo.ExpandedHoverImage = Nothing
        Me.gbTaskInfo.ExpandedNormalImage = Nothing
        Me.gbTaskInfo.ExpandedPressedImage = Nothing
        Me.gbTaskInfo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbTaskInfo.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbTaskInfo.HeaderHeight = 25
        Me.gbTaskInfo.HeaderMessage = ""
        Me.gbTaskInfo.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbTaskInfo.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbTaskInfo.HeightOnCollapse = 0
        Me.gbTaskInfo.LeftTextSpace = 0
        Me.gbTaskInfo.Location = New System.Drawing.Point(171, 12)
        Me.gbTaskInfo.Name = "gbTaskInfo"
        Me.gbTaskInfo.OpenHeight = 300
        Me.gbTaskInfo.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbTaskInfo.ShowBorder = True
        Me.gbTaskInfo.ShowCheckBox = False
        Me.gbTaskInfo.ShowCollapseButton = False
        Me.gbTaskInfo.ShowDefaultBorderColor = True
        Me.gbTaskInfo.ShowDownButton = False
        Me.gbTaskInfo.ShowHeader = True
        Me.gbTaskInfo.Size = New System.Drawing.Size(367, 306)
        Me.gbTaskInfo.TabIndex = 23
        Me.gbTaskInfo.Temp = 0
        Me.gbTaskInfo.Text = "Task Information"
        Me.gbTaskInfo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtEmail3
        '
        Me.txtEmail3.BackColor = System.Drawing.Color.White
        Me.txtEmail3.Flags = 0
        Me.txtEmail3.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtEmail3.InvalidChars = New Char(-1) {}
        Me.txtEmail3.Location = New System.Drawing.Point(11, 176)
        Me.txtEmail3.Name = "txtEmail3"
        Me.txtEmail3.Size = New System.Drawing.Size(343, 21)
        Me.txtEmail3.TabIndex = 44
        '
        'txtEmail2
        '
        Me.txtEmail2.BackColor = System.Drawing.Color.White
        Me.txtEmail2.Flags = 0
        Me.txtEmail2.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtEmail2.InvalidChars = New Char(-1) {}
        Me.txtEmail2.Location = New System.Drawing.Point(11, 149)
        Me.txtEmail2.Name = "txtEmail2"
        Me.txtEmail2.Size = New System.Drawing.Size(343, 21)
        Me.txtEmail2.TabIndex = 43
        '
        'txtEmail1
        '
        Me.txtEmail1.BackColor = System.Drawing.Color.White
        Me.txtEmail1.Flags = 0
        Me.txtEmail1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtEmail1.InvalidChars = New Char(-1) {}
        Me.txtEmail1.Location = New System.Drawing.Point(11, 122)
        Me.txtEmail1.Name = "txtEmail1"
        Me.txtEmail1.Size = New System.Drawing.Size(343, 21)
        Me.txtEmail1.TabIndex = 42
        '
        'Label1
        '
        Me.Label1.BackColor = System.Drawing.Color.Transparent
        Me.Label1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(8, 103)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(345, 16)
        Me.Label1.TabIndex = 41
        Me.Label1.Tag = ""
        Me.Label1.Text = "In case of failure of Backup/Recovery, Please notify by email to :"
        '
        'lblNotes
        '
        Me.lblNotes.BackColor = System.Drawing.Color.Transparent
        Me.lblNotes.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblNotes.Location = New System.Drawing.Point(8, 263)
        Me.lblNotes.Name = "lblNotes"
        Me.lblNotes.Size = New System.Drawing.Size(345, 34)
        Me.lblNotes.TabIndex = 40
        Me.lblNotes.Tag = ""
        Me.lblNotes.Text = "* Select the company from which you want to use the email settings to send email " & _
            "notification in case of Backup/Recovery."
        '
        'cboCompany
        '
        Me.cboCompany.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboCompany.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboCompany.FormattingEnabled = True
        Me.cboCompany.Location = New System.Drawing.Point(95, 61)
        Me.cboCompany.Name = "cboCompany"
        Me.cboCompany.Size = New System.Drawing.Size(243, 21)
        Me.cboCompany.TabIndex = 39
        '
        'lblCompany
        '
        Me.lblCompany.BackColor = System.Drawing.Color.Transparent
        Me.lblCompany.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCompany.Location = New System.Drawing.Point(8, 63)
        Me.lblCompany.Name = "lblCompany"
        Me.lblCompany.Size = New System.Drawing.Size(81, 16)
        Me.lblCompany.TabIndex = 24
        Me.lblCompany.Tag = ""
        Me.lblCompany.Text = "* Company"
        '
        'lblTaskName
        '
        Me.lblTaskName.BackColor = System.Drawing.Color.Transparent
        Me.lblTaskName.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTaskName.Location = New System.Drawing.Point(8, 36)
        Me.lblTaskName.Name = "lblTaskName"
        Me.lblTaskName.Size = New System.Drawing.Size(81, 16)
        Me.lblTaskName.TabIndex = 23
        Me.lblTaskName.Tag = ""
        Me.lblTaskName.Text = "Task Name"
        '
        'Label12
        '
        Me.Label12.BackColor = System.Drawing.Color.Transparent
        Me.Label12.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label12.Location = New System.Drawing.Point(248, 627)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(370, 16)
        Me.Label12.TabIndex = 18
        Me.Label12.Text = "Select time when you want this task to start"
        '
        'ewpPage2
        '
        Me.ewpPage2.BackColor = System.Drawing.Color.White
        Me.ewpPage2.Controls.Add(Me.lblMsg2)
        Me.ewpPage2.Controls.Add(Me.txtCPWD)
        Me.ewpPage2.Controls.Add(Me.txtPWD)
        Me.ewpPage2.Controls.Add(Me.txtUserName)
        Me.ewpPage2.Controls.Add(Me.lblConfirmPassword)
        Me.ewpPage2.Controls.Add(Me.lblPassword)
        Me.ewpPage2.Controls.Add(Me.lblUserName)
        Me.ewpPage2.Controls.Add(Me.lblTitle2)
        Me.ewpPage2.Controls.Add(Me.lblMinuts)
        Me.ewpPage2.Controls.Add(Me.nudMinutes)
        Me.ewpPage2.Controls.Add(Me.lblEvery)
        Me.ewpPage2.Controls.Add(Me.dtpTime)
        Me.ewpPage2.Controls.Add(Me.objOpenFile)
        Me.ewpPage2.Controls.Add(Me.txtBackupFolder)
        Me.ewpPage2.Controls.Add(Me.lblBackupFolder)
        Me.ewpPage2.Controls.Add(Me.lblTime)
        Me.ewpPage2.Controls.Add(Me.rbtnMonthly)
        Me.ewpPage2.Controls.Add(Me.rbtnWeekly)
        Me.ewpPage2.Controls.Add(Me.rbtnDaily)
        Me.ewpPage2.Controls.Add(Me.lblTaskType)
        Me.ewpPage2.Controls.Add(Me.lblWeekDay)
        Me.ewpPage2.Controls.Add(Me.cboDay)
        Me.ewpPage2.Controls.Add(Me.lblDay)
        Me.ewpPage2.Controls.Add(Me.cboWeekDay)
        Me.ewpPage2.Location = New System.Drawing.Point(0, 0)
        Me.ewpPage2.Name = "ewpPage2"
        Me.ewpPage2.Size = New System.Drawing.Size(548, 333)
        Me.ewpPage2.Style = eZee.Common.eZeeWizardPageStyle.eZeeStyle
        Me.ewpPage2.TabIndex = 2
        '
        'lblMsg2
        '
        Me.lblMsg2.BackColor = System.Drawing.Color.Transparent
        Me.lblMsg2.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblMsg2.Location = New System.Drawing.Point(172, 312)
        Me.lblMsg2.Name = "lblMsg2"
        Me.lblMsg2.Size = New System.Drawing.Size(370, 16)
        Me.lblMsg2.TabIndex = 36
        Me.lblMsg2.Text = "If a password is not entered, scheduled tasks might not run."
        '
        'txtCPWD
        '
        Me.txtCPWD.Location = New System.Drawing.Point(362, 256)
        Me.txtCPWD.Name = "txtCPWD"
        Me.txtCPWD.PasswordChar = Global.Microsoft.VisualBasic.ChrW(42)
        Me.txtCPWD.Size = New System.Drawing.Size(179, 21)
        Me.txtCPWD.TabIndex = 35
        '
        'txtPWD
        '
        Me.txtPWD.Location = New System.Drawing.Point(362, 230)
        Me.txtPWD.Name = "txtPWD"
        Me.txtPWD.PasswordChar = Global.Microsoft.VisualBasic.ChrW(42)
        Me.txtPWD.Size = New System.Drawing.Size(179, 21)
        Me.txtPWD.TabIndex = 34
        '
        'txtUserName
        '
        Me.txtUserName.Location = New System.Drawing.Point(362, 204)
        Me.txtUserName.Name = "txtUserName"
        Me.txtUserName.Size = New System.Drawing.Size(179, 21)
        Me.txtUserName.TabIndex = 33
        '
        'lblConfirmPassword
        '
        Me.lblConfirmPassword.BackColor = System.Drawing.Color.Transparent
        Me.lblConfirmPassword.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblConfirmPassword.Location = New System.Drawing.Point(171, 258)
        Me.lblConfirmPassword.Name = "lblConfirmPassword"
        Me.lblConfirmPassword.Size = New System.Drawing.Size(180, 16)
        Me.lblConfirmPassword.TabIndex = 32
        Me.lblConfirmPassword.Text = "Confirm password:"
        '
        'lblPassword
        '
        Me.lblPassword.BackColor = System.Drawing.Color.Transparent
        Me.lblPassword.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPassword.Location = New System.Drawing.Point(171, 232)
        Me.lblPassword.Name = "lblPassword"
        Me.lblPassword.Size = New System.Drawing.Size(180, 16)
        Me.lblPassword.TabIndex = 31
        Me.lblPassword.Text = "Enter the password:"
        '
        'lblUserName
        '
        Me.lblUserName.BackColor = System.Drawing.Color.Transparent
        Me.lblUserName.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblUserName.Location = New System.Drawing.Point(171, 206)
        Me.lblUserName.Name = "lblUserName"
        Me.lblUserName.Size = New System.Drawing.Size(180, 16)
        Me.lblUserName.TabIndex = 30
        Me.lblUserName.Text = "Enter the user name:"
        '
        'lblTitle2
        '
        Me.lblTitle2.BackColor = System.Drawing.Color.Transparent
        Me.lblTitle2.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTitle2.Location = New System.Drawing.Point(171, 176)
        Me.lblTitle2.Name = "lblTitle2"
        Me.lblTitle2.Size = New System.Drawing.Size(370, 25)
        Me.lblTitle2.TabIndex = 29
        Me.lblTitle2.Text = "Enter the name and password of a windows user. "
        '
        'lblMinuts
        '
        Me.lblMinuts.BackColor = System.Drawing.Color.Transparent
        Me.lblMinuts.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblMinuts.Location = New System.Drawing.Point(307, 137)
        Me.lblMinuts.Name = "lblMinuts"
        Me.lblMinuts.Size = New System.Drawing.Size(78, 16)
        Me.lblMinuts.TabIndex = 24
        Me.lblMinuts.Tag = ""
        Me.lblMinuts.Text = "minute(s)"
        '
        'nudMinutes
        '
        Me.nudMinutes.Location = New System.Drawing.Point(240, 135)
        Me.nudMinutes.Maximum = New Decimal(New Integer() {1400, 0, 0, 0})
        Me.nudMinutes.Minimum = New Decimal(New Integer() {2, 0, 0, 0})
        Me.nudMinutes.Name = "nudMinutes"
        Me.nudMinutes.Size = New System.Drawing.Size(61, 21)
        Me.nudMinutes.TabIndex = 23
        Me.nudMinutes.Value = New Decimal(New Integer() {2, 0, 0, 0})
        '
        'lblEvery
        '
        Me.lblEvery.BackColor = System.Drawing.Color.Transparent
        Me.lblEvery.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEvery.Location = New System.Drawing.Point(183, 137)
        Me.lblEvery.Name = "lblEvery"
        Me.lblEvery.Size = New System.Drawing.Size(51, 16)
        Me.lblEvery.TabIndex = 22
        Me.lblEvery.Tag = ""
        Me.lblEvery.Text = "Every"
        '
        'dtpTime
        '
        Me.dtpTime.Format = System.Windows.Forms.DateTimePickerFormat.Time
        Me.dtpTime.Location = New System.Drawing.Point(171, 79)
        Me.dtpTime.Name = "dtpTime"
        Me.dtpTime.Size = New System.Drawing.Size(95, 21)
        Me.dtpTime.TabIndex = 17
        '
        'objOpenFile
        '
        Me.objOpenFile.BackColor = System.Drawing.Color.Transparent
        Me.objOpenFile.BackColor1 = System.Drawing.Color.Transparent
        Me.objOpenFile.BackColor2 = System.Drawing.Color.Transparent
        Me.objOpenFile.BorderNormalColor = System.Drawing.Color.Black
        Me.objOpenFile.BorderSelectedColor = System.Drawing.Color.Black
        Me.objOpenFile.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objOpenFile.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objOpenFile.Image = Global.Aruti.Data.My.Resources.Resources.Mini_Folder
        Me.objOpenFile.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objOpenFile.Location = New System.Drawing.Point(517, 28)
        Me.objOpenFile.Name = "objOpenFile"
        Me.objOpenFile.Size = New System.Drawing.Size(21, 21)
        Me.objOpenFile.TabIndex = 16
        '
        'txtBackupFolder
        '
        Me.txtBackupFolder.BackColor = System.Drawing.Color.White
        Me.txtBackupFolder.Flags = 0
        Me.txtBackupFolder.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtBackupFolder.InvalidChars = New Char(-1) {}
        Me.txtBackupFolder.Location = New System.Drawing.Point(171, 28)
        Me.txtBackupFolder.Name = "txtBackupFolder"
        Me.txtBackupFolder.ReadOnly = True
        Me.txtBackupFolder.Size = New System.Drawing.Size(343, 21)
        Me.txtBackupFolder.TabIndex = 15
        '
        'lblBackupFolder
        '
        Me.lblBackupFolder.BackColor = System.Drawing.Color.Transparent
        Me.lblBackupFolder.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblBackupFolder.Location = New System.Drawing.Point(168, 9)
        Me.lblBackupFolder.Name = "lblBackupFolder"
        Me.lblBackupFolder.Size = New System.Drawing.Size(370, 16)
        Me.lblBackupFolder.TabIndex = 14
        Me.lblBackupFolder.Text = "Select directory where you want to store backup file." & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        '
        'lblTime
        '
        Me.lblTime.BackColor = System.Drawing.Color.Transparent
        Me.lblTime.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTime.Location = New System.Drawing.Point(168, 60)
        Me.lblTime.Name = "lblTime"
        Me.lblTime.Size = New System.Drawing.Size(370, 16)
        Me.lblTime.TabIndex = 13
        Me.lblTime.Text = "Select time when you want this task to start"
        '
        'rbtnMonthly
        '
        Me.rbtnMonthly.AutoSize = True
        Me.rbtnMonthly.Location = New System.Drawing.Point(324, 294)
        Me.rbtnMonthly.Name = "rbtnMonthly"
        Me.rbtnMonthly.Size = New System.Drawing.Size(63, 17)
        Me.rbtnMonthly.TabIndex = 12
        Me.rbtnMonthly.Text = "Monthly"
        Me.rbtnMonthly.UseVisualStyleBackColor = True
        Me.rbtnMonthly.Visible = False
        '
        'rbtnWeekly
        '
        Me.rbtnWeekly.AutoSize = True
        Me.rbtnWeekly.Location = New System.Drawing.Point(240, 294)
        Me.rbtnWeekly.Name = "rbtnWeekly"
        Me.rbtnWeekly.Size = New System.Drawing.Size(60, 17)
        Me.rbtnWeekly.TabIndex = 11
        Me.rbtnWeekly.Text = "Weekly"
        Me.rbtnWeekly.UseVisualStyleBackColor = True
        Me.rbtnWeekly.Visible = False
        '
        'rbtnDaily
        '
        Me.rbtnDaily.AutoSize = True
        Me.rbtnDaily.Checked = True
        Me.rbtnDaily.Location = New System.Drawing.Point(175, 294)
        Me.rbtnDaily.Name = "rbtnDaily"
        Me.rbtnDaily.Size = New System.Drawing.Size(48, 17)
        Me.rbtnDaily.TabIndex = 10
        Me.rbtnDaily.TabStop = True
        Me.rbtnDaily.Text = "Daily"
        Me.rbtnDaily.UseVisualStyleBackColor = True
        Me.rbtnDaily.Visible = False
        '
        'lblTaskType
        '
        Me.lblTaskType.BackColor = System.Drawing.Color.Transparent
        Me.lblTaskType.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTaskType.Location = New System.Drawing.Point(168, 116)
        Me.lblTaskType.Name = "lblTaskType"
        Me.lblTaskType.Size = New System.Drawing.Size(370, 16)
        Me.lblTaskType.TabIndex = 9
        Me.lblTaskType.Tag = ""
        Me.lblTaskType.Text = "Perform this task: "
        '
        'lblWeekDay
        '
        Me.lblWeekDay.BackColor = System.Drawing.Color.Transparent
        Me.lblWeekDay.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblWeekDay.Location = New System.Drawing.Point(253, 299)
        Me.lblWeekDay.Name = "lblWeekDay"
        Me.lblWeekDay.Size = New System.Drawing.Size(181, 16)
        Me.lblWeekDay.TabIndex = 18
        Me.lblWeekDay.Tag = ""
        Me.lblWeekDay.Text = "Select the day of the week:"
        Me.lblWeekDay.Visible = False
        '
        'cboDay
        '
        Me.cboDay.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboDay.FormattingEnabled = True
        Me.cboDay.Location = New System.Drawing.Point(393, 293)
        Me.cboDay.Name = "cboDay"
        Me.cboDay.Size = New System.Drawing.Size(121, 21)
        Me.cboDay.TabIndex = 21
        Me.cboDay.Visible = False
        '
        'lblDay
        '
        Me.lblDay.BackColor = System.Drawing.Color.Transparent
        Me.lblDay.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDay.Location = New System.Drawing.Point(243, 312)
        Me.lblDay.Name = "lblDay"
        Me.lblDay.Size = New System.Drawing.Size(181, 16)
        Me.lblDay.TabIndex = 19
        Me.lblDay.Tag = ""
        Me.lblDay.Text = "Select the day:"
        Me.lblDay.Visible = False
        '
        'cboWeekDay
        '
        Me.cboWeekDay.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboWeekDay.FormattingEnabled = True
        Me.cboWeekDay.Location = New System.Drawing.Point(393, 294)
        Me.cboWeekDay.Name = "cboWeekDay"
        Me.cboWeekDay.Size = New System.Drawing.Size(121, 21)
        Me.cboWeekDay.TabIndex = 20
        Me.cboWeekDay.Visible = False
        '
        'ewpPage4
        '
        Me.ewpPage4.Controls.Add(Me.chkRunNow)
        Me.ewpPage4.Controls.Add(Me.lblTitle3)
        Me.ewpPage4.Controls.Add(Me.lblMsg3)
        Me.ewpPage4.Location = New System.Drawing.Point(0, 0)
        Me.ewpPage4.Name = "ewpPage4"
        Me.ewpPage4.Size = New System.Drawing.Size(548, 333)
        Me.ewpPage4.Style = eZee.Common.eZeeWizardPageStyle.Finish
        Me.ewpPage4.TabIndex = 7
        '
        'chkRunNow
        '
        Me.chkRunNow.BackColor = System.Drawing.Color.Transparent
        Me.chkRunNow.Location = New System.Drawing.Point(171, 285)
        Me.chkRunNow.Name = "chkRunNow"
        Me.chkRunNow.Size = New System.Drawing.Size(372, 16)
        Me.chkRunNow.TabIndex = 17
        Me.chkRunNow.Text = "Run schedule task after create it."
        Me.chkRunNow.UseVisualStyleBackColor = False
        '
        'lblTitle3
        '
        Me.lblTitle3.BackColor = System.Drawing.Color.Transparent
        Me.lblTitle3.Font = New System.Drawing.Font("Tahoma", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTitle3.Location = New System.Drawing.Point(168, 9)
        Me.lblTitle3.Name = "lblTitle3"
        Me.lblTitle3.Size = New System.Drawing.Size(370, 57)
        Me.lblTitle3.TabIndex = 16
        Me.lblTitle3.Text = "You have successfully scheduled the Auto Backup Task."
        '
        'lblMsg3
        '
        Me.lblMsg3.BackColor = System.Drawing.Color.Transparent
        Me.lblMsg3.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblMsg3.Location = New System.Drawing.Point(168, 305)
        Me.lblMsg3.Name = "lblMsg3"
        Me.lblMsg3.Size = New System.Drawing.Size(370, 16)
        Me.lblMsg3.TabIndex = 15
        Me.lblMsg3.Text = "Click Finish to add this task to your Windows schedule."
        '
        'ewpPage1
        '
        Me.ewpPage1.BackColor = System.Drawing.Color.White
        Me.ewpPage1.Controls.Add(Me.lblMsg1)
        Me.ewpPage1.Controls.Add(Me.lblTitle1)
        Me.ewpPage1.Location = New System.Drawing.Point(0, 0)
        Me.ewpPage1.Name = "ewpPage1"
        Me.ewpPage1.Size = New System.Drawing.Size(548, 333)
        Me.ewpPage1.Style = eZee.Common.eZeeWizardPageStyle.Welcome
        Me.ewpPage1.TabIndex = 0
        '
        'lblMsg1
        '
        Me.lblMsg1.BackColor = System.Drawing.Color.Transparent
        Me.lblMsg1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblMsg1.Location = New System.Drawing.Point(168, 77)
        Me.lblMsg1.Name = "lblMsg1"
        Me.lblMsg1.Size = New System.Drawing.Size(370, 34)
        Me.lblMsg1.TabIndex = 3
        Me.lblMsg1.Text = "This wizard will help you to create a windows scheduled task, which will take dat" & _
            "abase backup automatically."
        '
        'lblTitle1
        '
        Me.lblTitle1.BackColor = System.Drawing.Color.Transparent
        Me.lblTitle1.Font = New System.Drawing.Font("Tahoma", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTitle1.Location = New System.Drawing.Point(168, 9)
        Me.lblTitle1.Name = "lblTitle1"
        Me.lblTitle1.Size = New System.Drawing.Size(370, 60)
        Me.lblTitle1.TabIndex = 2
        Me.lblTitle1.Text = "Auto Backup Schedule Task for Disaster Recovery"
        '
        'txtTaskName
        '
        Me.txtTaskName.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtTaskName.Location = New System.Drawing.Point(95, 34)
        Me.txtTaskName.Name = "txtTaskName"
        Me.txtTaskName.Size = New System.Drawing.Size(243, 21)
        Me.txtTaskName.TabIndex = 47
        '
        'frmDisasterRecoveryBackupWizard
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(548, 381)
        Me.Controls.Add(Me.pnlMain)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmDisasterRecoveryBackupWizard"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Auto Backup Schedule Task for Disaster Recovery"
        Me.pnlMain.ResumeLayout(False)
        Me.ewWizard.ResumeLayout(False)
        Me.ewpPage3.ResumeLayout(False)
        Me.gbTaskInfo.ResumeLayout(False)
        Me.gbTaskInfo.PerformLayout()
        Me.ewpPage2.ResumeLayout(False)
        Me.ewpPage2.PerformLayout()
        CType(Me.nudMinutes, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ewpPage4.ResumeLayout(False)
        Me.ewpPage1.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents pnlMain As System.Windows.Forms.Panel
    Friend WithEvents ewWizard As eZee.Common.eZeeWizard
    Friend WithEvents ewpPage1 As eZee.Common.eZeeWizardPage
    Friend WithEvents lblMsg1 As System.Windows.Forms.Label
    Friend WithEvents lblTitle1 As System.Windows.Forms.Label
    Friend WithEvents ewpPage4 As eZee.Common.eZeeWizardPage
    Friend WithEvents chkRunNow As System.Windows.Forms.CheckBox
    Friend WithEvents lblTitle3 As System.Windows.Forms.Label
    Friend WithEvents lblMsg3 As System.Windows.Forms.Label
    Friend WithEvents ewpPage3 As eZee.Common.eZeeWizardPage
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents ewpPage2 As eZee.Common.eZeeWizardPage
    Friend WithEvents dtpTime As System.Windows.Forms.DateTimePicker
    Friend WithEvents objOpenFile As eZee.Common.eZeeGradientButton
    Friend WithEvents txtBackupFolder As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblBackupFolder As System.Windows.Forms.Label
    Friend WithEvents lblTime As System.Windows.Forms.Label
    Friend WithEvents rbtnMonthly As System.Windows.Forms.RadioButton
    Friend WithEvents rbtnWeekly As System.Windows.Forms.RadioButton
    Friend WithEvents rbtnDaily As System.Windows.Forms.RadioButton
    Friend WithEvents lblTaskType As System.Windows.Forms.Label
    Friend WithEvents lblWeekDay As System.Windows.Forms.Label
    Friend WithEvents cboDay As System.Windows.Forms.ComboBox
    Friend WithEvents lblDay As System.Windows.Forms.Label
    Friend WithEvents cboWeekDay As System.Windows.Forms.ComboBox
    Friend WithEvents lblEvery As System.Windows.Forms.Label
    Friend WithEvents lblMinuts As System.Windows.Forms.Label
    Friend WithEvents nudMinutes As System.Windows.Forms.NumericUpDown
    Friend WithEvents gbTaskInfo As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents lblCompany As System.Windows.Forms.Label
    Friend WithEvents lblTaskName As System.Windows.Forms.Label
    Friend WithEvents cboCompany As System.Windows.Forms.ComboBox
    Friend WithEvents lblNotes As System.Windows.Forms.Label
    Friend WithEvents txtCPWD As System.Windows.Forms.TextBox
    Friend WithEvents txtPWD As System.Windows.Forms.TextBox
    Friend WithEvents txtUserName As System.Windows.Forms.TextBox
    Friend WithEvents lblConfirmPassword As System.Windows.Forms.Label
    Friend WithEvents lblPassword As System.Windows.Forms.Label
    Friend WithEvents lblUserName As System.Windows.Forms.Label
    Friend WithEvents lblTitle2 As System.Windows.Forms.Label
    Friend WithEvents txtEmail3 As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents txtEmail2 As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents txtEmail1 As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents lblMsg2 As System.Windows.Forms.Label
    Friend WithEvents txtTaskName As System.Windows.Forms.TextBox
End Class
