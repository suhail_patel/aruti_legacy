﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmReasonSelection
    Inherits eZee.Common.eZeeForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmReasonSelection))
        Me.pnlMainInfo = New System.Windows.Forms.Panel
        Me.dgvReasonSelection = New System.Windows.Forms.DataGridView
        Me.colAlias = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.colReason = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objcolReasonUnkid = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.eZeeHeader = New eZee.Common.eZeeHeader
        Me.objFooter = New eZee.Common.eZeeFooter
        Me.btnContinue = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnClose = New eZee.Common.eZeeLightButton(Me.components)
        Me.pnlMainInfo.SuspendLayout()
        CType(Me.dgvReasonSelection, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.objFooter.SuspendLayout()
        Me.SuspendLayout()
        '
        'pnlMainInfo
        '
        Me.pnlMainInfo.Controls.Add(Me.dgvReasonSelection)
        Me.pnlMainInfo.Controls.Add(Me.eZeeHeader)
        Me.pnlMainInfo.Controls.Add(Me.objFooter)
        Me.pnlMainInfo.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pnlMainInfo.Location = New System.Drawing.Point(0, 0)
        Me.pnlMainInfo.Name = "pnlMainInfo"
        Me.pnlMainInfo.Size = New System.Drawing.Size(536, 318)
        Me.pnlMainInfo.TabIndex = 14
        '
        'dgvReasonSelection
        '
        Me.dgvReasonSelection.AllowUserToAddRows = False
        Me.dgvReasonSelection.AllowUserToDeleteRows = False
        Me.dgvReasonSelection.AllowUserToResizeColumns = False
        Me.dgvReasonSelection.AllowUserToResizeRows = False
        DataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvReasonSelection.AlternatingRowsDefaultCellStyle = DataGridViewCellStyle1
        Me.dgvReasonSelection.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCellsExceptHeaders
        Me.dgvReasonSelection.BackgroundColor = System.Drawing.Color.WhiteSmoke
        Me.dgvReasonSelection.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing
        Me.dgvReasonSelection.ColumnHeadersVisible = False
        Me.dgvReasonSelection.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.colAlias, Me.colReason, Me.objcolReasonUnkid})
        Me.dgvReasonSelection.Location = New System.Drawing.Point(9, 64)
        Me.dgvReasonSelection.Name = "dgvReasonSelection"
        Me.dgvReasonSelection.ReadOnly = True
        Me.dgvReasonSelection.RowHeadersVisible = False
        Me.dgvReasonSelection.RowTemplate.DefaultCellStyle.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvReasonSelection.RowTemplate.Height = 80
        Me.dgvReasonSelection.RowTemplate.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgvReasonSelection.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvReasonSelection.Size = New System.Drawing.Size(518, 193)
        Me.dgvReasonSelection.TabIndex = 17
        '
        'colAlias
        '
        Me.colAlias.HeaderText = "Alias"
        Me.colAlias.Name = "colAlias"
        Me.colAlias.ReadOnly = True
        Me.colAlias.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.colAlias.Width = 75
        '
        'colReason
        '
        Me.colReason.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.colReason.HeaderText = "colReason"
        Me.colReason.Name = "colReason"
        Me.colReason.ReadOnly = True
        Me.colReason.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'objcolReasonUnkid
        '
        Me.objcolReasonUnkid.HeaderText = "objcolReasonUnkid"
        Me.objcolReasonUnkid.Name = "objcolReasonUnkid"
        Me.objcolReasonUnkid.ReadOnly = True
        Me.objcolReasonUnkid.Visible = False
        '
        'eZeeHeader
        '
        Me.eZeeHeader.BackColor = System.Drawing.SystemColors.Control
        Me.eZeeHeader.BorderColor = System.Drawing.SystemColors.ControlDark
        Me.eZeeHeader.DescriptionForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.eZeeHeader.Dock = System.Windows.Forms.DockStyle.Top
        Me.eZeeHeader.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.eZeeHeader.GradiantStyle = eZee.Common.eZeeHeader.GradientStyle.Central
        Me.eZeeHeader.GradientColor1 = System.Drawing.SystemColors.Window
        Me.eZeeHeader.GradientColor2 = System.Drawing.SystemColors.Control
        Me.eZeeHeader.HeaderTextForeColor = System.Drawing.SystemColors.ControlText
        Me.eZeeHeader.Icon = Nothing
        Me.eZeeHeader.Location = New System.Drawing.Point(0, 0)
        Me.eZeeHeader.Message = ""
        Me.eZeeHeader.Name = "eZeeHeader"
        Me.eZeeHeader.Size = New System.Drawing.Size(536, 58)
        Me.eZeeHeader.TabIndex = 16
        Me.eZeeHeader.Title = "Reason Selection"
        '
        'objFooter
        '
        Me.objFooter.BorderColor = System.Drawing.Color.Silver
        Me.objFooter.Controls.Add(Me.btnContinue)
        Me.objFooter.Controls.Add(Me.btnClose)
        Me.objFooter.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.objFooter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objFooter.GradiantStyle = eZee.Common.eZeeFooter.GradientStyle.Up
        Me.objFooter.GradientColor1 = System.Drawing.SystemColors.Control
        Me.objFooter.GradientColor2 = System.Drawing.SystemColors.Control
        Me.objFooter.Location = New System.Drawing.Point(0, 263)
        Me.objFooter.Name = "objFooter"
        Me.objFooter.Size = New System.Drawing.Size(536, 55)
        Me.objFooter.TabIndex = 14
        '
        'btnContinue
        '
        Me.btnContinue.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnContinue.BackColor = System.Drawing.Color.White
        Me.btnContinue.BackgroundImage = CType(resources.GetObject("btnContinue.BackgroundImage"), System.Drawing.Image)
        Me.btnContinue.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnContinue.BorderColor = System.Drawing.Color.Empty
        Me.btnContinue.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnContinue.FlatAppearance.BorderSize = 0
        Me.btnContinue.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnContinue.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnContinue.ForeColor = System.Drawing.Color.Black
        Me.btnContinue.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnContinue.GradientForeColor = System.Drawing.Color.Black
        Me.btnContinue.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnContinue.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnContinue.Location = New System.Drawing.Point(338, 13)
        Me.btnContinue.Name = "btnContinue"
        Me.btnContinue.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnContinue.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnContinue.Size = New System.Drawing.Size(90, 30)
        Me.btnContinue.TabIndex = 5
        Me.btnContinue.Text = "&Ok"
        Me.btnContinue.UseVisualStyleBackColor = True
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnClose.BackColor = System.Drawing.Color.White
        Me.btnClose.BackgroundImage = CType(resources.GetObject("btnClose.BackgroundImage"), System.Drawing.Image)
        Me.btnClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnClose.BorderColor = System.Drawing.Color.Empty
        Me.btnClose.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnClose.FlatAppearance.BorderSize = 0
        Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClose.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.ForeColor = System.Drawing.Color.Black
        Me.btnClose.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnClose.GradientForeColor = System.Drawing.Color.Black
        Me.btnClose.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Location = New System.Drawing.Point(434, 13)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Size = New System.Drawing.Size(93, 30)
        Me.btnClose.TabIndex = 6
        Me.btnClose.Text = "&Close"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'frmReasonSelection
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(536, 318)
        Me.Controls.Add(Me.pnlMainInfo)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmReasonSelection"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Reason"
        Me.pnlMainInfo.ResumeLayout(False)
        CType(Me.dgvReasonSelection, System.ComponentModel.ISupportInitialize).EndInit()
        Me.objFooter.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents pnlMainInfo As System.Windows.Forms.Panel
    Friend WithEvents objFooter As eZee.Common.eZeeFooter
    Friend WithEvents btnContinue As eZee.Common.eZeeLightButton
    Friend WithEvents btnClose As eZee.Common.eZeeLightButton
    Friend WithEvents eZeeHeader As eZee.Common.eZeeHeader
    Friend WithEvents dgvReasonSelection As System.Windows.Forms.DataGridView
    Friend WithEvents colAlias As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colReason As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objcolReasonUnkid As System.Windows.Forms.DataGridViewTextBoxColumn
End Class
