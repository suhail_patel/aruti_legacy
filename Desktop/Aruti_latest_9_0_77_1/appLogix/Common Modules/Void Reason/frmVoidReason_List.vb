﻿Option Strict On

#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data
Imports System

#End Region

Public Class frmVoidReason_List

#Region " Private Variables "

    Private ReadOnly mstrModuleName As String = "frmVoidReason_List"
    Private objVoidReason As clsVoidReason

#End Region

#Region " Private Methods "

    Private Sub FillCombo()
        Dim dsList As New DataSet
        Dim objMaster As New clsMasterData
        Try
            dsList = objMaster.GetVoidCategoryList("List")
            With cboCategory
                .ValueMember = "Id"
                .DisplayMember = "NAME"
                .DataSource = dsList.Tables("List")
                .SelectedValue = 0
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        End Try
    End Sub

    Private Sub FillList()
        Dim dsReason As New DataSet
        Dim lvItem As ListViewItem
        Dim dtTable As DataTable
        Dim StrString As String = String.Empty
        Try
            dsReason = objVoidReason.GetList("Reason")

            If CInt(cboCategory.SelectedValue) > 0 Then
                StrString = "AND categoryunkid = " & CInt(cboCategory.SelectedValue)
            End If

            If StrString.Length > 0 Then
                StrString = StrString.Substring(3)
                dtTable = New DataView(dsReason.Tables("Reason"), StrString, "", DataViewRowState.CurrentRows).ToTable
            Else
                dtTable = New DataView(dsReason.Tables("Reason"), "", "", DataViewRowState.CurrentRows).ToTable
            End If

            lvVoidReason.Items.Clear()

            For Each dtRow As DataRow In dtTable.Rows
                lvItem = New ListViewItem

                lvItem.Text = dtRow.Item("alias").ToString
                lvItem.SubItems.Add(dtRow.Item("CatName").ToString)
                lvItem.SubItems.Add(dtRow.Item("reason").ToString)
                lvItem.Tag = dtRow.Item("reasonunkid")

                lvVoidReason.Items.Add(lvItem)
            Next

            If lvVoidReason.Items.Count > 12 Then
                colhReason.Width = colhReason.Width - 15
            Else
                colhReason.Width = colhReason.Width
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillList", mstrModuleName)
        End Try
    End Sub

    'S.SANDEEP [ 29 OCT 2012 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Private Sub SetVisibility()
        Try
            btnNew.Enabled = User._Object.Privilege._AllowtoAddVoidReason
            btnEdit.Enabled = User._Object.Privilege._AllowtoEditVoidReason
            btnDelete.Enabled = User._Object.Privilege._AllowtoDeleteVoidReason
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "SetVisibility", mstrModuleName)
        End Try
    End Sub
    'S.SANDEEP [ 29 OCT 2012 ] -- END

#End Region

#Region " Form's Events "

    Private Sub frmVoidReason_List_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        objVoidReason = New clsVoidReason
        Try
            Call Set_Logo(Me, gApplicationType)
            Language.setLanguage(Me.Name)


            'Pinkal (10-Jul-2014) -- Start
            'Enhancement - Language Settings
            OtherSettings()
            'Pinkal (10-Jul-2014) -- End

            FillCombo()
            FillList()
            If lvVoidReason.Items.Count > 0 Then lvVoidReason.Items(0).Selected = True
            lvVoidReason.Select()

            'S.SANDEEP [ 29 OCT 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            Call SetVisibility()
            'S.SANDEEP [ 29 OCT 2012 ] -- END

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmVoidReason_List_Load", mstrModuleName)
        End Try
    End Sub

    Private Sub frmVoidReason_List_KeyUp(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles MyBase.KeyUp
        Try
            If e.KeyCode = Keys.Delete And lvVoidReason.Focused = True Then
                btnDelete_Click(sender, e)
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmVoidReason_List_KeyUp", mstrModuleName)
        End Try
    End Sub

    Private Sub frmVoidReason_List_FormClosed(ByVal sender As System.Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles MyBase.FormClosed
        objVoidReason = Nothing
    End Sub



    'Pinkal (10-Jul-2014) -- Start
    'Enhancement - Language Settings

    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsVoidReason.SetMessages()
            objfrm._Other_ModuleNames = "clsVoidReason"
            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub

    'Pinkal (10-Jul-2014) -- End


#End Region

#Region " Button's Events "

    Private Sub btnNew_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNew.Click
        Try
            Dim frm As New frmVoidReason_AddEdit
            If frm.displayDialog(-1, enAction.ADD_CONTINUE) Then
                Call FillList()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnNew_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnEdit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEdit.Click
        If lvVoidReason.SelectedItems.Count <= 0 Then
            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please select atleast one reason to perform further operation."), enMsgBoxStyle.Information)
            Exit Sub
        End If
        Try
            Dim frm As New frmVoidReason_AddEdit

            If frm.displayDialog(CInt(lvVoidReason.SelectedItems(0).Tag), enAction.EDIT_ONE) Then
                Call FillList()
            End If

            lvVoidReason.Select()

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnEdit_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDelete.Click
        If lvVoidReason.SelectedItems.Count <= 0 Then
            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please select atleast one reason to perform further operation."), enMsgBoxStyle.Information)
            Exit Sub
        End If
        Try
            Dim intSelectedIndex As Integer
            intSelectedIndex = lvVoidReason.SelectedItems(0).Index

            If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Are you sure you want to delete this Reason?"), CType(enMsgBoxStyle.Question + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.Yes Then

                'S.SANDEEP [28-May-2018] -- START
                'ISSUE/ENHANCEMENT : {Audit Trails} 
                objVoidReason._FormName = mstrModuleName
                objVoidReason._LoginEmployeeunkid = 0
                objVoidReason._ClientIP = getIP()
                objVoidReason._HostName = getHostName()
                objVoidReason._FromWeb = False
                objVoidReason._AuditUserId = User._Object._Userunkid
objVoidReason._CompanyUnkid = Company._Object._Companyunkid
                objVoidReason._AuditDate = ConfigParameter._Object._CurrentDateAndTime
                'S.SANDEEP [28-May-2018] -- END

                objVoidReason.Delete(CInt(lvVoidReason.SelectedItems(0).Tag))
                lvVoidReason.SelectedItems(0).Remove()
            End If
            lvVoidReason.Select()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnDelete_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnClose_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnSearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearch.Click
        Try
            Call FillList()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearch_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnReset.Click
        Try
            cboCategory.SelectedValue = 0
            Call FillList()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnReset_Click", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Controls "

    Private Sub cboCategory_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboCategory.SelectedIndexChanged
        Try
            Call FillList()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboCategory_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

#End Region

	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Private Sub OtherSettings()
		Try
			Me.SuspendLayout()
			
			Call SetLanguage()
			
			Me.gbState.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbState.ForeColor = GUI._eZeeContainerHeaderForeColor 


			
			Me.eZeeHeader.GradientColor1 = GUI._HeaderBackColor1 
			Me.eZeeHeader.GradientColor2 = GUI._HeaderBackColor2 
			Me.eZeeHeader.BorderColor = GUI._HeaderBorderColor 
			Me.eZeeHeader.HeaderTextForeColor = GUI._HeaderTitleFontColor 
			Me.eZeeHeader.DescriptionForeColor = GUI._HeaderMessageFontColor 


			Me.btnDelete.GradientBackColor = GUI._ButttonBackColor 
			Me.btnDelete.GradientForeColor = GUI._ButttonFontColor

			Me.btnEdit.GradientBackColor = GUI._ButttonBackColor 
			Me.btnEdit.GradientForeColor = GUI._ButttonFontColor

			Me.btnNew.GradientBackColor = GUI._ButttonBackColor 
			Me.btnNew.GradientForeColor = GUI._ButttonFontColor

			Me.btnClose.GradientBackColor = GUI._ButttonBackColor 
			Me.btnClose.GradientForeColor = GUI._ButttonFontColor


			Me.ResumeLayout()
		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "OtherSettings", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetLanguage()
		Try
			Me.Text = Language._Object.getCaption(Me.Name, Me.Text)
			
			Me.btnDelete.Text = Language._Object.getCaption(Me.btnDelete.Name, Me.btnDelete.Text)
			Me.btnEdit.Text = Language._Object.getCaption(Me.btnEdit.Name, Me.btnEdit.Text)
			Me.btnNew.Text = Language._Object.getCaption(Me.btnNew.Name, Me.btnNew.Text)
			Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
			Me.eZeeHeader.Title = Language._Object.getCaption(Me.eZeeHeader.Name & "_Title" , Me.eZeeHeader.Title)
			Me.eZeeHeader.Message = Language._Object.getCaption(Me.eZeeHeader.Name & "_Message" , Me.eZeeHeader.Message)
			Me.gbState.Text = Language._Object.getCaption(Me.gbState.Name, Me.gbState.Text)
			Me.lblCategory.Text = Language._Object.getCaption(Me.lblCategory.Name, Me.lblCategory.Text)
			Me.colhAlias.Text = Language._Object.getCaption(CStr(Me.colhAlias.Tag), Me.colhAlias.Text)
			Me.colhCategory.Text = Language._Object.getCaption(CStr(Me.colhCategory.Tag), Me.colhCategory.Text)
			Me.colhReason.Text = Language._Object.getCaption(CStr(Me.colhReason.Tag), Me.colhReason.Text)

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetLanguage", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "Please select atleast one reason to perform further operation.")
			Language.setMessage(mstrModuleName, 2, "Are you sure you want to delete this Reason?")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class