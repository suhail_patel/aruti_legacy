﻿Option Strict On

#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data

#End Region

Public Class frmReasonSelection

#Region " Private Variables "
    Private ReadOnly mstrModuleName As String = "frmReasonSelection"
    Private mstrVoidReason As String = String.Empty
    Private mintCategoryId As Integer = -1
    Private objVoidReason As clsVoidReason
    Private mblnCancel As Boolean = True


    'Gajanan [24-OCT-2019] -- Start   
    'Enhancement:Worked On NMB Grievance Reporting To Approval Flow Change   


    Public Enum endisplayType
        ReasonMaster = 1
        ReportingTolist = 2
    End Enum
    Public menDisplayType As endisplayType
    Private mdtReportingTo As DataTable

    'Gajanan [24-OCT-2019] -- End

#End Region

#Region " Diplay Dialog "
    Public Sub displayDialog(ByVal intCategoryId As Integer, ByRef strReason As String, Optional ByVal displayType As endisplayType = endisplayType.ReasonMaster, Optional ByVal mdtreportingTolist As DataTable = Nothing)
        mintCategoryId = intCategoryId
        mstrVoidReason = strReason

        'Gajanan [24-OCT-2019] -- Start   
        'Enhancement:Worked On NMB Grievance Reporting To Approval Flow Change   
        menDisplayType = displayType
        mdtReportingTo = mdtreportingTolist
        'Gajanan [24-OCT-2019] -- End

        Me.ShowDialog()

        strReason = mstrVoidReason
    End Sub
#End Region

#Region " Private Methods "
    Private Sub FillList()
        Dim dsList As New DataSet
        Try

            'Gajanan [24-OCT-2019] -- Start   
            'Enhancement:Worked On NMB Grievance Reporting To Approval Flow Change   

            'dsList = objVoidReason.getComboList(mintCategoryId, True, "List")
            'dgvReasonSelection.AutoGenerateColumns = False
            'objcolReasonUnkid.DataPropertyName = "Id"
            'colAlias.DataPropertyName = "alias"
            'colReason.DataPropertyName = "NAME"
            'dgvReasonSelection.DataSource = dsList.Tables("List")

            If menDisplayType = CInt(endisplayType.ReportingTolist) Then
                dgvReasonSelection.AutoGenerateColumns = False
                objcolReasonUnkid.DataPropertyName = "rempid"
                colAlias.Visible = False
                colReason.DataPropertyName = "employee"
                dgvReasonSelection.DataSource = mdtReportingTo
            Else
                dsList = objVoidReason.getComboList(mintCategoryId, True, "List")
            dgvReasonSelection.AutoGenerateColumns = False
            objcolReasonUnkid.DataPropertyName = "Id"
            colAlias.DataPropertyName = "alias"
            colReason.DataPropertyName = "NAME"
            dgvReasonSelection.DataSource = dsList.Tables("List")
            End If
            'Gajanan [24-OCT-2019] -- End



        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillList", mstrModuleName)
        End Try
    End Sub
#End Region

#Region " Form's Events "

    Private Sub frmReasonSelection_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        Try
            objVoidReason = Nothing
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmReasonSelection_FormClosing", mstrModuleName)
        End Try
    End Sub

    Private Sub frmReasonSelection_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown
        Try
            If e.KeyCode = Windows.Forms.Keys.Enter Then
                If btnContinue.Enabled = True Then
                    Call btnContinue.PerformClick()
                End If
            ElseIf e.Control = True And e.KeyCode = Windows.Forms.Keys.O Then
                Call btnContinue.PerformClick()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmReasonSelection_KeyDown", mstrModuleName)
        End Try
    End Sub

    Private Sub frmReasonSelection_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        objVoidReason = New clsVoidReason
        Try
            Call Set_Logo(Me, gApplicationType)


            'Gajanan [24-OCT-2019] -- Start   
            'Enhancement:Worked On NMB Grievance Reporting To Approval Flow Change   
            If menDisplayType = CInt(endisplayType.ReportingTolist) Then
                btnContinue.Visible = False
                eZeeHeader.Title = Language.getMessage(mstrModuleName, 1, "Reporting To List")
                Me.Text = Language.getMessage(mstrModuleName, 1, "Reporting To List")
            End If
            'Gajanan [24-OCT-2019] -- End

            Call FillList()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmReasonSelection_Load", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Buttons "

    Private Sub btnContinue_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnContinue.Click
        Try
            'If lvReason.SelectedItems.Count > 0 Then
            '    mstrVoidReason = lvReason.SelectedItems(0).Text
            '    Call btnClose_Click(sender, e)
            'Else
            '    mstrVoidReason = ""
            'End If

            If dgvReasonSelection.SelectedRows.Count > 0 Then
                mstrVoidReason = dgvReasonSelection.SelectedRows(0).Cells(colReason.Index).Value.ToString
                Call btnClose_Click(sender, e)
            Else
                mstrVoidReason = ""
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnContinue_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnClose_Click", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Controls "

    Private Sub dgvReasonSelection_SelectionChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles dgvReasonSelection.SelectionChanged
        Try
            If dgvReasonSelection.SelectedRows.Count > 0 Then
                If dgvReasonSelection.SelectedRows(0).Cells(objcolReasonUnkid.Index).Value.ToString = "0" Then
                    btnContinue.Enabled = False
                Else
                    btnContinue.Enabled = True
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgvReasonSelection_SelectionChanged", mstrModuleName)
        End Try
    End Sub

#End Region

	
	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Private Sub OtherSettings()
		Try
			Me.SuspendLayout()
			
			Call SetLanguage()
			
			Me.eZeeHeader.GradientColor1 = GUI._HeaderBackColor1 
			Me.eZeeHeader.GradientColor2 = GUI._HeaderBackColor2 
			Me.eZeeHeader.BorderColor = GUI._HeaderBorderColor 
			Me.eZeeHeader.HeaderTextForeColor = GUI._HeaderTitleFontColor 
			Me.eZeeHeader.DescriptionForeColor = GUI._HeaderMessageFontColor 


			Me.btnContinue.GradientBackColor = GUI._ButttonBackColor 
			Me.btnContinue.GradientForeColor = GUI._ButttonFontColor

			Me.btnClose.GradientBackColor = GUI._ButttonBackColor 
			Me.btnClose.GradientForeColor = GUI._ButttonFontColor


			Me.ResumeLayout()
		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "OtherSettings", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetLanguage()
		Try
			Me.Text = Language._Object.getCaption(Me.Name, Me.Text)
			
			Me.btnContinue.Text = Language._Object.getCaption(Me.btnContinue.Name, Me.btnContinue.Text)
			Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
			Me.eZeeHeader.Title = Language._Object.getCaption(Me.eZeeHeader.Name & "_Title" , Me.eZeeHeader.Title)
			Me.eZeeHeader.Message = Language._Object.getCaption(Me.eZeeHeader.Name & "_Message" , Me.eZeeHeader.Message)
			Me.colAlias.HeaderText = Language._Object.getCaption(Me.colAlias.Name, Me.colAlias.HeaderText)
			Me.colReason.HeaderText = Language._Object.getCaption(Me.colReason.Name, Me.colReason.HeaderText)

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetLanguage", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetMessages()
		Try

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class