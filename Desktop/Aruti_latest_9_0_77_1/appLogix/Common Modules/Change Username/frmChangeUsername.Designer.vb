﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmChangeUsername
    Inherits eZee.Common.eZeeForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmChangeUsername))
        Me.EZeeFooter1 = New eZee.Common.eZeeFooter
        Me.btnCancel = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnSave = New eZee.Common.eZeeLightButton(Me.components)
        Me.gbChangeUsername = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.txtPassword = New eZee.TextBox.AlphanumericTextBox
        Me.lblPwd = New System.Windows.Forms.Label
        Me.txtConfUName = New eZee.TextBox.AlphanumericTextBox
        Me.lblConfUName = New System.Windows.Forms.Label
        Me.txtNewUName = New eZee.TextBox.AlphanumericTextBox
        Me.lblNewUsername = New System.Windows.Forms.Label
        Me.txtOldUName = New eZee.TextBox.AlphanumericTextBox
        Me.lblOldUname = New System.Windows.Forms.Label
        Me.EZeeFooter1.SuspendLayout()
        Me.gbChangeUsername.SuspendLayout()
        Me.SuspendLayout()
        '
        'EZeeFooter1
        '
        Me.EZeeFooter1.BorderColor = System.Drawing.Color.Silver
        Me.EZeeFooter1.Controls.Add(Me.btnCancel)
        Me.EZeeFooter1.Controls.Add(Me.btnSave)
        Me.EZeeFooter1.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.EZeeFooter1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.EZeeFooter1.GradiantStyle = eZee.Common.eZeeFooter.GradientStyle.Up
        Me.EZeeFooter1.GradientColor1 = System.Drawing.SystemColors.Control
        Me.EZeeFooter1.GradientColor2 = System.Drawing.SystemColors.Control
        Me.EZeeFooter1.Location = New System.Drawing.Point(0, 146)
        Me.EZeeFooter1.Name = "EZeeFooter1"
        Me.EZeeFooter1.Size = New System.Drawing.Size(353, 55)
        Me.EZeeFooter1.TabIndex = 1
        '
        'btnCancel
        '
        Me.btnCancel.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnCancel.BackColor = System.Drawing.Color.White
        Me.btnCancel.BackgroundImage = CType(resources.GetObject("btnCancel.BackgroundImage"), System.Drawing.Image)
        Me.btnCancel.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnCancel.BorderColor = System.Drawing.Color.Empty
        Me.btnCancel.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnCancel.FlatAppearance.BorderSize = 0
        Me.btnCancel.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnCancel.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnCancel.ForeColor = System.Drawing.Color.Black
        Me.btnCancel.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnCancel.GradientForeColor = System.Drawing.Color.Black
        Me.btnCancel.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnCancel.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnCancel.Location = New System.Drawing.Point(251, 13)
        Me.btnCancel.Name = "btnCancel"
        Me.btnCancel.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnCancel.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnCancel.Size = New System.Drawing.Size(90, 30)
        Me.btnCancel.TabIndex = 5
        Me.btnCancel.Text = "&Cancel"
        Me.btnCancel.UseVisualStyleBackColor = True
        '
        'btnSave
        '
        Me.btnSave.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnSave.BackColor = System.Drawing.Color.White
        Me.btnSave.BackgroundImage = CType(resources.GetObject("btnSave.BackgroundImage"), System.Drawing.Image)
        Me.btnSave.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnSave.BorderColor = System.Drawing.Color.Empty
        Me.btnSave.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnSave.FlatAppearance.BorderSize = 0
        Me.btnSave.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnSave.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSave.ForeColor = System.Drawing.Color.Black
        Me.btnSave.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnSave.GradientForeColor = System.Drawing.Color.Black
        Me.btnSave.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSave.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnSave.Location = New System.Drawing.Point(155, 13)
        Me.btnSave.Name = "btnSave"
        Me.btnSave.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSave.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnSave.Size = New System.Drawing.Size(90, 30)
        Me.btnSave.TabIndex = 4
        Me.btnSave.Text = "&Save"
        Me.btnSave.UseVisualStyleBackColor = True
        '
        'gbChangeUsername
        '
        Me.gbChangeUsername.BorderColor = System.Drawing.Color.Black
        Me.gbChangeUsername.Checked = False
        Me.gbChangeUsername.CollapseAllExceptThis = False
        Me.gbChangeUsername.CollapsedHoverImage = Nothing
        Me.gbChangeUsername.CollapsedNormalImage = Nothing
        Me.gbChangeUsername.CollapsedPressedImage = Nothing
        Me.gbChangeUsername.CollapseOnLoad = False
        Me.gbChangeUsername.Controls.Add(Me.txtPassword)
        Me.gbChangeUsername.Controls.Add(Me.lblPwd)
        Me.gbChangeUsername.Controls.Add(Me.txtConfUName)
        Me.gbChangeUsername.Controls.Add(Me.lblConfUName)
        Me.gbChangeUsername.Controls.Add(Me.txtNewUName)
        Me.gbChangeUsername.Controls.Add(Me.lblNewUsername)
        Me.gbChangeUsername.Controls.Add(Me.txtOldUName)
        Me.gbChangeUsername.Controls.Add(Me.lblOldUname)
        Me.gbChangeUsername.Dock = System.Windows.Forms.DockStyle.Fill
        Me.gbChangeUsername.ExpandedHoverImage = Nothing
        Me.gbChangeUsername.ExpandedNormalImage = Nothing
        Me.gbChangeUsername.ExpandedPressedImage = Nothing
        Me.gbChangeUsername.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbChangeUsername.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbChangeUsername.HeaderHeight = 25
        Me.gbChangeUsername.HeaderMessage = ""
        Me.gbChangeUsername.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbChangeUsername.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbChangeUsername.HeightOnCollapse = 0
        Me.gbChangeUsername.LeftTextSpace = 0
        Me.gbChangeUsername.Location = New System.Drawing.Point(0, 0)
        Me.gbChangeUsername.Name = "gbChangeUsername"
        Me.gbChangeUsername.OpenHeight = 300
        Me.gbChangeUsername.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbChangeUsername.ShowBorder = True
        Me.gbChangeUsername.ShowCheckBox = False
        Me.gbChangeUsername.ShowCollapseButton = False
        Me.gbChangeUsername.ShowDefaultBorderColor = True
        Me.gbChangeUsername.ShowDownButton = False
        Me.gbChangeUsername.ShowHeader = True
        Me.gbChangeUsername.Size = New System.Drawing.Size(353, 146)
        Me.gbChangeUsername.TabIndex = 2
        Me.gbChangeUsername.Temp = 0
        Me.gbChangeUsername.Text = "Change Username"
        Me.gbChangeUsername.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtPassword
        '
        Me.txtPassword.Flags = 0
        Me.txtPassword.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtPassword.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(92), Global.Microsoft.VisualBasic.ChrW(93), Global.Microsoft.VisualBasic.ChrW(91)}
        Me.txtPassword.Location = New System.Drawing.Point(113, 115)
        Me.txtPassword.Name = "txtPassword"
        Me.txtPassword.PasswordChar = Global.Microsoft.VisualBasic.ChrW(42)
        Me.txtPassword.Size = New System.Drawing.Size(228, 21)
        Me.txtPassword.TabIndex = 9
        '
        'lblPwd
        '
        Me.lblPwd.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPwd.Location = New System.Drawing.Point(8, 117)
        Me.lblPwd.Name = "lblPwd"
        Me.lblPwd.Size = New System.Drawing.Size(99, 17)
        Me.lblPwd.TabIndex = 10
        Me.lblPwd.Text = "Password"
        Me.lblPwd.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtConfUName
        '
        Me.txtConfUName.Flags = 0
        Me.txtConfUName.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtConfUName.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtConfUName.Location = New System.Drawing.Point(113, 88)
        Me.txtConfUName.Name = "txtConfUName"
        Me.txtConfUName.Size = New System.Drawing.Size(228, 21)
        Me.txtConfUName.TabIndex = 8
        '
        'lblConfUName
        '
        Me.lblConfUName.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblConfUName.Location = New System.Drawing.Point(8, 90)
        Me.lblConfUName.Name = "lblConfUName"
        Me.lblConfUName.Size = New System.Drawing.Size(99, 17)
        Me.lblConfUName.TabIndex = 7
        Me.lblConfUName.Text = "Confirm Username"
        Me.lblConfUName.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtNewUName
        '
        Me.txtNewUName.Flags = 0
        Me.txtNewUName.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtNewUName.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtNewUName.Location = New System.Drawing.Point(113, 61)
        Me.txtNewUName.Name = "txtNewUName"
        Me.txtNewUName.Size = New System.Drawing.Size(228, 21)
        Me.txtNewUName.TabIndex = 6
        '
        'lblNewUsername
        '
        Me.lblNewUsername.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblNewUsername.Location = New System.Drawing.Point(8, 63)
        Me.lblNewUsername.Name = "lblNewUsername"
        Me.lblNewUsername.Size = New System.Drawing.Size(99, 17)
        Me.lblNewUsername.TabIndex = 5
        Me.lblNewUsername.Text = "New Username"
        Me.lblNewUsername.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtOldUName
        '
        Me.txtOldUName.Flags = 0
        Me.txtOldUName.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtOldUName.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtOldUName.Location = New System.Drawing.Point(113, 34)
        Me.txtOldUName.Name = "txtOldUName"
        Me.txtOldUName.Size = New System.Drawing.Size(228, 21)
        Me.txtOldUName.TabIndex = 4
        '
        'lblOldUname
        '
        Me.lblOldUname.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblOldUname.Location = New System.Drawing.Point(8, 36)
        Me.lblOldUname.Name = "lblOldUname"
        Me.lblOldUname.Size = New System.Drawing.Size(99, 17)
        Me.lblOldUname.TabIndex = 3
        Me.lblOldUname.Text = "Old Username"
        Me.lblOldUname.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'frmChangeUsername
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(353, 201)
        Me.Controls.Add(Me.gbChangeUsername)
        Me.Controls.Add(Me.EZeeFooter1)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmChangeUsername"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Change Username"
        Me.EZeeFooter1.ResumeLayout(False)
        Me.gbChangeUsername.ResumeLayout(False)
        Me.gbChangeUsername.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents EZeeFooter1 As eZee.Common.eZeeFooter
    Friend WithEvents btnCancel As eZee.Common.eZeeLightButton
    Friend WithEvents btnSave As eZee.Common.eZeeLightButton
    Friend WithEvents gbChangeUsername As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents lblOldUname As System.Windows.Forms.Label
    Friend WithEvents txtConfUName As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblConfUName As System.Windows.Forms.Label
    Friend WithEvents txtNewUName As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblNewUsername As System.Windows.Forms.Label
    Friend WithEvents txtOldUName As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents txtPassword As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblPwd As System.Windows.Forms.Label
End Class
