﻿Option Strict On

#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data
Imports System.IO
Imports System.Text

#End Region

Public Class frmOrbitDictionaryMapping

#Region " Private Variables "

    Private ReadOnly mstrModuleName As String = "frmOrbitDictionaryMapping"
    Private objMAOP As clsMapArutiOrbitParams
    Private mdtMappingTran As DataTable
    Private mdtMappedData As DataTable
    Private mVMappedView As DataView
    Private mdvParameter As DataView
    Private mintEditedAllocationId As Integer = 0
    Private mstrEditedOrbitKeyId As String = String.Empty

#End Region

#Region " Form's Events "

    Private Sub frmOrbitDictionaryMapping_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            Call Set_Logo(Me, enArutiApplicatinType.Aruti_Payroll)
            objMAOP = New clsMapArutiOrbitParams
            Call FillParameters()
            mdtMappingTran = objMAOP._Datatable
            Call FillMappedData()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmOrbitDictionaryMapping_Load", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub frmRemark_LanguageClick(ByVal sender As Object, ByVal eventArgs As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            objfrm.displayDialog(Me)

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "frmRemark_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub

#End Region

#Region " Private Methods "

    Private Sub FillParameters()
        Try
            cboArutiParams.DataSource = [Enum].GetValues(GetType(clsMapArutiOrbitParams.enArutiParameter))
            cboOrbitParams.DataSource = [Enum].GetValues(GetType(clsMapArutiOrbitParams.enOrbitParameter))
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillParameters", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub FillArutiData(ByVal eType As clsMapArutiOrbitParams.enArutiParameter, Optional ByVal intAllocationId As Integer = 0)
        Try
            Dim dsList As New DataSet
            dgvData.AutoGenerateColumns = False
            dgvData.DataSource = Nothing
            Select Case eType
                Case clsMapArutiOrbitParams.enArutiParameter.BRANCH
                    Dim objStation As New clsStation
                    dsList = objStation.getComboList("List", False)
                    dgcolhAParam.DataPropertyName = "name"
                    objdgcolhAPraramId.DataPropertyName = "stationunkid"
                    objStation = Nothing
                Case clsMapArutiOrbitParams.enArutiParameter.GENDER
                    Dim objMst As New clsMasterData
                    dsList = objMst.getGenderList("list", False)
                    dgcolhAParam.DataPropertyName = "name"
                    objdgcolhAPraramId.DataPropertyName = "id"
                    objMst = Nothing
                Case clsMapArutiOrbitParams.enArutiParameter.COUNTRY, _
                     clsMapArutiOrbitParams.enArutiParameter.NATIONALITY
                    Dim objMst As New clsMasterData
                    dsList = objMst.getCountryList("list", False)
                    dgcolhAParam.DataPropertyName = "country_name"
                    objdgcolhAPraramId.DataPropertyName = "countryunkid"
                    objMst = Nothing
                Case clsMapArutiOrbitParams.enArutiParameter.STATE
                    Dim objState As New clsstate_master
                    dsList = objState.GetList("list", True, False)
                    dgcolhAParam.DataPropertyName = "name"
                    objdgcolhAPraramId.DataPropertyName = "stateunkid"
                    objState = Nothing
                Case clsMapArutiOrbitParams.enArutiParameter.CITY
                    Dim objCity As New clscity_master
                    dsList = objCity.GetList("list", True, False)
                    dgcolhAParam.DataPropertyName = "name"
                    objdgcolhAPraramId.DataPropertyName = "cityunkid"
                    objCity = Nothing
                Case clsMapArutiOrbitParams.enArutiParameter.JOB
                    Dim objJob As New clsJobs
                    dsList = objJob.GetList("list", True)
                    dgcolhAParam.DataPropertyName = "JobName"
                    objdgcolhAPraramId.DataPropertyName = "jobunkid"
                    objJob = Nothing
                Case clsMapArutiOrbitParams.enArutiParameter.TITLE, _
                     clsMapArutiOrbitParams.enArutiParameter.IDTYPE, _
                     clsMapArutiOrbitParams.enArutiParameter.MARITALSTATUS
                    Dim objCMst As New clsCommon_Master
                    dsList = objCMst.getComboList(CType(CInt(eType), clsCommon_Master.enCommonMaster), False)
                    dgcolhAParam.DataPropertyName = "name"
                    objdgcolhAPraramId.DataPropertyName = "masterunkid"
                    objCMst = Nothing
            End Select

            If dsList.Tables.Count > 0 Then
                Dim strFilter As String = String.Empty
                strFilter = objMAOP.GetMappedAllocationIds(eType)
                If mintEditedAllocationId > 0 Then
                    mdvParameter = New DataView(dsList.Tables(0), objdgcolhAPraramId.DataPropertyName & " = " & mintEditedAllocationId, "", DataViewRowState.CurrentRows)
                Else
                    If strFilter.Trim.Length > 0 Then
                        mdvParameter = New DataView(dsList.Tables(0), objdgcolhAPraramId.DataPropertyName & " NOT IN (" & strFilter & ")", "", DataViewRowState.CurrentRows)
                    Else
                        mdvParameter = dsList.Tables(0).DefaultView
                    End If
                End If
                dgvData.DataSource = mdvParameter
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillArutiData", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub FillMappedData()
        Try
            mdtMappedData = objMAOP.GetList("list", "")
            dgvMapping.AutoGenerateColumns = False

            mVMappedView = mdtMappedData.DefaultView

            dgcolhDAParam.DataPropertyName = "GrpName"
            objdgcolhMappingTranId.DataPropertyName = "mappingtranid"
            objdgcolhIsGrp.DataPropertyName = "IsGrp"
            objdgcolhIsGrpId.DataPropertyName = "GrpId"

            objdgcolhallocationrefid.DataPropertyName = "allocationrefid"
            objdgcolharutiorbitrefid.DataPropertyName = "arutiorbitrefid"

            objdgcolhallocationunkid.DataPropertyName = "allocationunkid"
            objdgcolhorbitfldkey.DataPropertyName = "orbitfldkey"

            dgvMapping.DataSource = mVMappedView

            Call SetGridStyle()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillMappedData", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Function EscapeLikeValue(ByVal value As String) As String
        Dim sb As New StringBuilder(value.Length)
        For i As Integer = 0 To value.Length - 1
            Dim c As Char = value(i)
            Select Case c
                Case "]"c, "["c, "%"c, "*"c
                    sb.Append("[").Append(c).Append("]")
                    Exit Select
                Case "'"c
                    sb.Append("''")
                    Exit Select
                Case Else
                    sb.Append(c)
                    Exit Select
            End Select
        Next
        Return sb.ToString()
    End Function

    Private Sub SetGridStyle()
        Try
            Dim oGrpRow As IEnumerable(Of DataGridViewRow) = dgvMapping.Rows.Cast(Of DataGridViewRow).AsEnumerable().Where(Function(x) CBool(x.Cells(objdgcolhIsGrp.Index).Value) = True)
            For Each gRow As DataGridViewRow In oGrpRow
                gRow.DefaultCellStyle.BackColor = Color.Gray
                gRow.DefaultCellStyle.ForeColor = Color.White
                gRow.DefaultCellStyle.Font = New Font(Me.Font, FontStyle.Bold)

                gRow.Cells(objdgcolhDelete.Index).Style.BackColor = Color.White
                gRow.Cells(objdgcolhDelete.Index).Style.SelectionBackColor = Color.White
                gRow.Cells(objdgcolhDelete.Index).Style.SelectionForeColor = Color.White

                Dim currencyManager1 As CurrencyManager = CType(BindingContext(dgvMapping.DataSource), CurrencyManager)
                currencyManager1.SuspendBinding()
                If dgvMapping.Rows.Cast(Of DataGridViewRow).AsEnumerable().Where(Function(x) CBool(x.Cells(objdgcolhIsGrp.Index).Value) = False And _
                                                                                     x.Cells(objdgcolhIsGrpId.Index).Value.ToString() = gRow.Cells(objdgcolhIsGrpId.Index).Value.ToString()).Count <= 0 Then
                    gRow.Visible = False
                Else
                    gRow.Visible = True
                End If
                currencyManager1.ResumeBinding()
            Next

            oGrpRow = dgvMapping.Rows.Cast(Of DataGridViewRow).AsEnumerable().Where(Function(x) CBool(x.Cells(objdgcolhIsGrp.Index).Value) = False)
            For Each gRow As DataGridViewRow In oGrpRow
                gRow.Cells(objdgcolhDelete.Index).Style.BackColor = Color.White
                gRow.Cells(objdgcolhDelete.Index).Style.SelectionBackColor = Color.White
                gRow.Cells(objdgcolhDelete.Index).Style.SelectionForeColor = Color.White
            Next

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetGridStyle", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

#Region " Button's Events "

    Private Sub btnSaveMapping_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSaveMapping.Click
        Try
            If dgvData.RowCount > 0 Then
                'S.SANDEEP |27-JUL-2020| -- START
                'ISSUE/ENHANCEMENT : ORBIT MAPPING
                Dim gRows As IEnumerable(Of DataGridViewRow) = dgvData.Rows.Cast(Of DataGridViewRow).AsEnumerable().Where(Function(x) x.Cells(dgcolhOParam.Index).Value IsNot Nothing AndAlso x.Cells(dgcolhOParam.Index).Value.ToString.Trim <> "")
                For Each gRow As DataGridViewRow In gRows
                    'For Each gRow As DataGridViewRow In dgvData.Rows
                    'S.SANDEEP |27-JUL-2020| -- END
                    If gRow.Cells(dgcolhOParam.Index).Value.ToString() <> "" Then
                        Dim iRow As DataRow = mdtMappingTran.NewRow()
                        iRow("mappingtranid") = -1
                        iRow("allocationrefid") = CInt(cboArutiParams.SelectedValue)
                        iRow("allocationunkid") = gRow.Cells(objdgcolhAPraramId.Index).Value
                        iRow("arutiorbitrefid") = CInt(cboOrbitParams.SelectedValue)
                        iRow("orbitfldname") = cboOrbitParams.Text
                        iRow("orbitfldkey") = gRow.Cells(dgcolhOParam.Index).Value.ToString()
                        iRow("orbitfldvalue") = gRow.Cells(dgcolhOParam.Index).FormattedValue.ToString()
                        iRow("isactive") = True
                        iRow("audittype") = enAuditType.ADD
                        iRow("auditdatetime") = ConfigParameter._Object._CurrentDateAndTime
                        iRow("audituserunkid") = User._Object._Userunkid
                        iRow("ip") = getIP()
                        iRow("host") = getHostName()
                        iRow("form_name") = mstrModuleName
                        iRow("isweb") = False
                        iRow("AUD") = "A"
                        mdtMappingTran.Rows.Add(iRow)
                    End If
                Next
                objMAOP._Datatable = mdtMappingTran.Copy()
                If mdtMappingTran.Rows.Count <= 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "Sorry, No Data is mapped in order to perform save operation."), enMsgBoxStyle.Information)
                    Exit Sub
                End If
                If objMAOP.SaveMapping() = True Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Mapping saved successfully."))
                    Call FillMappedData()
                    cboArutiParams.SelectedIndex = 0
                    cboOrbitParams.SelectedIndex = 0
                Else
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Problem in saving mapping."))
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnSaveMapping_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnClose_Click", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

#Region " Textbox Event(s) "

    Private Sub txtSearch_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtSearch.TextChanged
        Dim str As String = ""
        Try
            If mdvParameter Is Nothing Then Exit Sub
            If (txtSearch.Text.Length > 0) Then
                str = dgcolhAParam.DataPropertyName & " LIKE '%" & EscapeLikeValue(txtSearch.Text) & "%'"
            End If
            mdvParameter.RowFilter = str
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "txtSearch_TextChanged", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub txtSearchMapping_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtSearchMapping.TextChanged
        Dim str As String = ""
        Try
            If mVMappedView Is Nothing Then Exit Sub
            If (txtSearchMapping.Text.Length > 0) Then
                str = dgcolhDAParam.DataPropertyName & " LIKE '%" & EscapeLikeValue(txtSearchMapping.Text) & "%' Or IsGrp = 1 "
            End If
            mVMappedView.RowFilter = str
            Call SetGridStyle()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "txtSearchMapping_TextChanged", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

#Region " Combobox Events "

    Private Sub cboOrbitParams_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboOrbitParams.SelectedIndexChanged
        Try
            Dim eType As clsMapArutiOrbitParams.enOrbitParameter
            eType = CType([Enum].Parse(GetType(clsMapArutiOrbitParams.enOrbitParameter), CStr(cboOrbitParams.SelectedItem)), clsMapArutiOrbitParams.enOrbitParameter)
            Dim dtTable As DataTable = Nothing
            dtTable = objMAOP.FetchDictornaryValues(eType, Company._Object._Companyunkid)
            If dtTable IsNot Nothing Then
                dgcolhOParam.ValueMember = "Key"
                dgcolhOParam.DisplayMember = "value"
                dgcolhOParam.DataSource = dtTable.Copy()
                dgcolhOParam.DropDownWidth = 200
            Else
                If eType <> clsMapArutiOrbitParams.enOrbitParameter.NONE Then
                    If objMAOP._Message.Trim.Length > 0 Then
                        eZeeMsgBox.Show(objMAOP._Message, enMsgBoxStyle.Information)
                        cboOrbitParams.SelectedIndex = 0
                        Exit Sub
                    End If
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboOrbitParams_SelectedIndexChanged", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub cboArutiParams_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboArutiParams.SelectedIndexChanged
        Try
            Dim eType As clsMapArutiOrbitParams.enArutiParameter
            eType = CType([Enum].Parse(GetType(clsMapArutiOrbitParams.enArutiParameter), CStr(cboArutiParams.SelectedItem)), clsMapArutiOrbitParams.enArutiParameter)
            Call FillArutiData(eType)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboArutiParams_SelectedIndexChanged", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

#Region " GridView Events "

    Private Sub dgvData_DataError(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewDataErrorEventArgs) Handles dgvData.DataError

    End Sub

    Private Sub dgvData_CellEnter(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvData.CellEnter
        Try
            If e.ColumnIndex = dgcolhOParam.Index Then
                SendKeys.Send("{F2}")
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgvData_CellEnter", mstrModuleName)
        End Try
    End Sub

    Private Sub dgvMapping_CellContentClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvMapping.CellContentClick
        Try
            Select Case e.ColumnIndex
                Case objdgcolhEdit.Index
                    If CBool(dgvMapping.Rows(e.RowIndex).Cells(objdgcolhIsGrp.Index).Value) = True Then

                    Else
                        mintEditedAllocationId = CInt(dgvMapping.Rows(e.RowIndex).Cells(objdgcolhallocationunkid.Index).Value)
                        mstrEditedOrbitKeyId = CStr(dgvMapping.Rows(e.RowIndex).Cells(objdgcolhorbitfldkey.Index).Value)
                        cboArutiParams.SelectedValue = CInt(dgvMapping.Rows(e.RowIndex).Cells(objdgcolhallocationrefid.Index).Value)
                        cboOrbitParams.SelectedValue = CInt(dgvMapping.Rows(e.RowIndex).Cells(objdgcolharutiorbitrefid.Index).Value)

                    End If
                Case objdgcolhDelete.Index
                    If CBool(dgvMapping.Rows(e.RowIndex).Cells(objdgcolhIsGrp.Index).Value) = True Then
                        If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "You are about to delete mapping for entire group, Are you sure you wanted to remove this entire mapping?"), CType(enMsgBoxStyle.Information + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.No Then Exit Sub

                        Dim dr As DataRow() = mVMappedView.ToTable.Select("allocationrefid = '" & dgvMapping.Rows(e.RowIndex).Cells(objdgcolhallocationrefid.Index).Value.ToString & "' AND arutiorbitrefid = '" & dgvMapping.Rows(e.RowIndex).Cells(objdgcolharutiorbitrefid.Index).Value.ToString & "' AND IsGrp = 0 ")

                        If dr.Length > 0 Then
                            For Each iR In dr
                                iR("audittype") = enAuditType.DELETE
                                iR("auditdatetime") = ConfigParameter._Object._CurrentDateAndTime
                                iR("audituserunkid") = User._Object._Userunkid
                                iR("ip") = getIP()
                                iR("host") = getHostName()
                                iR("form_name") = mstrModuleName
                                iR("isweb") = False
                                iR("loginemployeeunkid") = 0
                            Next
                            If objMAOP.VoidAllMapping(dr) Then
                                Call FillMappedData()
                            End If
                        End If
                    Else
                        If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "You are about to delete mapping, Are you sure you wanted to remove this mapping?"), CType(enMsgBoxStyle.Information + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.No Then Exit Sub

                        Dim dr As DataRow() = mVMappedView.ToTable.Select("mappingtranid = '" & dgvMapping.Rows(e.RowIndex).Cells(objdgcolhMappingTranId.Index).Value.ToString & "'")
                        If dr.Length > 0 Then
                            dr(0)("audittype") = enAuditType.DELETE
                            dr(0)("auditdatetime") = ConfigParameter._Object._CurrentDateAndTime
                            dr(0)("audituserunkid") = User._Object._Userunkid
                            dr(0)("ip") = getIP()
                            dr(0)("host") = getHostName()
                            dr(0)("form_name") = mstrModuleName
                            dr(0)("isweb") = False
                            dr(0)("loginemployeeunkid") = 0

                            If objMAOP.VoidMapping(CInt(dgvMapping.Rows(e.RowIndex).Cells(objdgcolhMappingTranId.Index).Value), dr(0)) Then
                                dgvMapping.Rows.RemoveAt(e.RowIndex)
                            End If

                        End If
                    End If
            End Select
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgvMapping_CellContentClick", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub dgvMapping_DataError(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewDataErrorEventArgs) Handles dgvMapping.DataError

    End Sub

#End Region

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Private Sub OtherSettings()
        Try
            Me.SuspendLayout()
            
            Call SetLanguage()

            Me.gbDictionaryParmeters.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbDictionaryParmeters.ForeColor = GUI._eZeeContainerHeaderForeColor


            Me.btnClose.GradientBackColor = GUI._ButttonBackColor
            Me.btnClose.GradientForeColor = GUI._ButttonFontColor

            Me.btnSaveMapping.GradientBackColor = GUI._ButttonBackColor
            Me.btnSaveMapping.GradientForeColor = GUI._ButttonFontColor

            Me.btnDeleteChecked.GradientBackColor = GUI._ButttonBackColor
            Me.btnDeleteChecked.GradientForeColor = GUI._ButttonFontColor

            Me.btnEditChecked.GradientBackColor = GUI._ButttonBackColor
            Me.btnEditChecked.GradientForeColor = GUI._ButttonFontColor


            Me.ResumeLayout()
        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "OtherSettings", mstrModuleName)
        End Try
    End Sub


    Private Sub SetLanguage()
        Try
            Me.Text = Language._Object.getCaption(Me.Name, Me.Text)

            Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
            Me.gbDictionaryParmeters.Text = Language._Object.getCaption(Me.gbDictionaryParmeters.Name, Me.gbDictionaryParmeters.Text)
            Me.lblArutiDictionary.Text = Language._Object.getCaption(Me.lblArutiDictionary.Name, Me.lblArutiDictionary.Text)
            Me.lblOrbitDictionary.Text = Language._Object.getCaption(Me.lblOrbitDictionary.Name, Me.lblOrbitDictionary.Text)
            Me.btnSaveMapping.Text = Language._Object.getCaption(Me.btnSaveMapping.Name, Me.btnSaveMapping.Text)
            Me.btnDeleteChecked.Text = Language._Object.getCaption(Me.btnDeleteChecked.Name, Me.btnDeleteChecked.Text)
            Me.btnEditChecked.Text = Language._Object.getCaption(Me.btnEditChecked.Name, Me.btnEditChecked.Text)
            Me.dgcolhAParam.HeaderText = Language._Object.getCaption(Me.dgcolhAParam.Name, Me.dgcolhAParam.HeaderText)
            Me.dgcolhOParam.HeaderText = Language._Object.getCaption(Me.dgcolhOParam.Name, Me.dgcolhOParam.HeaderText)
            Me.DataGridViewTextBoxColumn1.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn1.Name, Me.DataGridViewTextBoxColumn1.HeaderText)
            Me.DataGridViewTextBoxColumn2.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn2.Name, Me.DataGridViewTextBoxColumn2.HeaderText)
            Me.DataGridViewTextBoxColumn3.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn3.Name, Me.DataGridViewTextBoxColumn3.HeaderText)
            Me.DataGridViewTextBoxColumn4.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn4.Name, Me.DataGridViewTextBoxColumn4.HeaderText)
            Me.DataGridViewTextBoxColumn5.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn5.Name, Me.DataGridViewTextBoxColumn5.HeaderText)
            Me.DataGridViewTextBoxColumn6.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn6.Name, Me.DataGridViewTextBoxColumn6.HeaderText)
            Me.DataGridViewTextBoxColumn7.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn7.Name, Me.DataGridViewTextBoxColumn7.HeaderText)
            Me.DataGridViewTextBoxColumn8.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn8.Name, Me.DataGridViewTextBoxColumn8.HeaderText)
            Me.DataGridViewTextBoxColumn9.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn9.Name, Me.DataGridViewTextBoxColumn9.HeaderText)
            Me.DataGridViewTextBoxColumn10.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn10.Name, Me.DataGridViewTextBoxColumn10.HeaderText)
            Me.lblNote.Text = Language._Object.getCaption(Me.lblNote.Name, Me.lblNote.Text)
            Me.dgcolhDAParam.HeaderText = Language._Object.getCaption(Me.dgcolhDAParam.Name, Me.dgcolhDAParam.HeaderText)

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
        End Try
    End Sub


    Private Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Mapping saved successfully.")
            Language.setMessage(mstrModuleName, 2, "Problem in saving mapping.")
            Language.setMessage(mstrModuleName, 3, "You are about to delete mapping, Are you sure you wanted to remove this mapping?")
            Language.setMessage(mstrModuleName, 4, "You are about to delete mapping for entire group, Are you sure you wanted to remove this entire mapping?")
            Language.setMessage(mstrModuleName, 5, "Sorry, No Data is mapped in order to perform save operation.")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class