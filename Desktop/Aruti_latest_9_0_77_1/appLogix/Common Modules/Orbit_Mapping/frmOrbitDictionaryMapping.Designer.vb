﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmOrbitDictionaryMapping
    Inherits eZee.Common.eZeeForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmOrbitDictionaryMapping))
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Me.Panel1 = New System.Windows.Forms.Panel
        Me.gbDictionaryParmeters = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.objpnlData = New System.Windows.Forms.Panel
        Me.dgvMapping = New System.Windows.Forms.DataGridView
        Me.objdgcolhCollapse = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhCheck = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhDAParam = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhEdit = New System.Windows.Forms.DataGridViewImageColumn
        Me.objdgcolhDelete = New System.Windows.Forms.DataGridViewImageColumn
        Me.objdgcolhMappingTranId = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhIsGrp = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhIsGrpId = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhallocationrefid = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolharutiorbitrefid = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhallocationunkid = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhorbitfldkey = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.txtSearchMapping = New eZee.TextBox.AlphanumericTextBox
        Me.objspc1 = New System.Windows.Forms.SplitContainer
        Me.tblInformation = New System.Windows.Forms.TableLayoutPanel
        Me.cboOrbitParams = New System.Windows.Forms.ComboBox
        Me.lblArutiDictionary = New System.Windows.Forms.Label
        Me.cboArutiParams = New System.Windows.Forms.ComboBox
        Me.lblOrbitDictionary = New System.Windows.Forms.Label
        Me.btnSaveMapping = New eZee.Common.eZeeLightButton(Me.components)
        Me.txtSearch = New eZee.TextBox.AlphanumericTextBox
        Me.dgvData = New System.Windows.Forms.DataGridView
        Me.dgcolhAParam = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhOParam = New System.Windows.Forms.DataGridViewComboBoxColumn
        Me.objdgcolhAPraramId = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhfieldName = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhKey = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhValue = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhMaptranguid = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objFooter = New eZee.Common.eZeeFooter
        Me.lblNote = New System.Windows.Forms.Label
        Me.btnClose = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnEditChecked = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnDeleteChecked = New eZee.Common.eZeeLightButton(Me.components)
        Me.DataGridViewTextBoxColumn1 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn2 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn3 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn4 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn5 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn6 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn7 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn8 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn9 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn10 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Panel1.SuspendLayout()
        Me.gbDictionaryParmeters.SuspendLayout()
        Me.objpnlData.SuspendLayout()
        CType(Me.dgvMapping, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.objspc1.Panel1.SuspendLayout()
        Me.objspc1.Panel2.SuspendLayout()
        Me.objspc1.SuspendLayout()
        Me.tblInformation.SuspendLayout()
        CType(Me.dgvData, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.objFooter.SuspendLayout()
        Me.SuspendLayout()
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(Me.gbDictionaryParmeters)
        Me.Panel1.Controls.Add(Me.objFooter)
        Me.Panel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel1.Location = New System.Drawing.Point(0, 0)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(944, 541)
        Me.Panel1.TabIndex = 0
        '
        'gbDictionaryParmeters
        '
        Me.gbDictionaryParmeters.BorderColor = System.Drawing.Color.Black
        Me.gbDictionaryParmeters.Checked = False
        Me.gbDictionaryParmeters.CollapseAllExceptThis = False
        Me.gbDictionaryParmeters.CollapsedHoverImage = Nothing
        Me.gbDictionaryParmeters.CollapsedNormalImage = Nothing
        Me.gbDictionaryParmeters.CollapsedPressedImage = Nothing
        Me.gbDictionaryParmeters.CollapseOnLoad = False
        Me.gbDictionaryParmeters.Controls.Add(Me.objpnlData)
        Me.gbDictionaryParmeters.Controls.Add(Me.txtSearchMapping)
        Me.gbDictionaryParmeters.Controls.Add(Me.objspc1)
        Me.gbDictionaryParmeters.Dock = System.Windows.Forms.DockStyle.Fill
        Me.gbDictionaryParmeters.ExpandedHoverImage = Nothing
        Me.gbDictionaryParmeters.ExpandedNormalImage = Nothing
        Me.gbDictionaryParmeters.ExpandedPressedImage = Nothing
        Me.gbDictionaryParmeters.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbDictionaryParmeters.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbDictionaryParmeters.HeaderHeight = 25
        Me.gbDictionaryParmeters.HeaderMessage = ""
        Me.gbDictionaryParmeters.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbDictionaryParmeters.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbDictionaryParmeters.HeightOnCollapse = 0
        Me.gbDictionaryParmeters.LeftTextSpace = 0
        Me.gbDictionaryParmeters.Location = New System.Drawing.Point(0, 0)
        Me.gbDictionaryParmeters.Name = "gbDictionaryParmeters"
        Me.gbDictionaryParmeters.OpenHeight = 300
        Me.gbDictionaryParmeters.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbDictionaryParmeters.ShowBorder = True
        Me.gbDictionaryParmeters.ShowCheckBox = False
        Me.gbDictionaryParmeters.ShowCollapseButton = False
        Me.gbDictionaryParmeters.ShowDefaultBorderColor = True
        Me.gbDictionaryParmeters.ShowDownButton = False
        Me.gbDictionaryParmeters.ShowHeader = True
        Me.gbDictionaryParmeters.Size = New System.Drawing.Size(944, 486)
        Me.gbDictionaryParmeters.TabIndex = 6
        Me.gbDictionaryParmeters.Temp = 0
        Me.gbDictionaryParmeters.Text = "Dictionary Parmeters"
        Me.gbDictionaryParmeters.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objpnlData
        '
        Me.objpnlData.Controls.Add(Me.dgvMapping)
        Me.objpnlData.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objpnlData.Location = New System.Drawing.Point(405, 48)
        Me.objpnlData.Name = "objpnlData"
        Me.objpnlData.Size = New System.Drawing.Size(536, 435)
        Me.objpnlData.TabIndex = 123
        '
        'dgvMapping
        '
        Me.dgvMapping.AllowUserToAddRows = False
        Me.dgvMapping.AllowUserToDeleteRows = False
        Me.dgvMapping.AllowUserToResizeRows = False
        Me.dgvMapping.BackgroundColor = System.Drawing.Color.White
        Me.dgvMapping.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvMapping.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle1
        Me.dgvMapping.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing
        Me.dgvMapping.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.objdgcolhCollapse, Me.objdgcolhCheck, Me.dgcolhDAParam, Me.objdgcolhEdit, Me.objdgcolhDelete, Me.objdgcolhMappingTranId, Me.objdgcolhIsGrp, Me.objdgcolhIsGrpId, Me.objdgcolhallocationrefid, Me.objdgcolharutiorbitrefid, Me.objdgcolhallocationunkid, Me.objdgcolhorbitfldkey})
        Me.dgvMapping.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dgvMapping.Location = New System.Drawing.Point(0, 0)
        Me.dgvMapping.Name = "dgvMapping"
        Me.dgvMapping.RowHeadersVisible = False
        Me.dgvMapping.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing
        Me.dgvMapping.Size = New System.Drawing.Size(536, 435)
        Me.dgvMapping.TabIndex = 108
        '
        'objdgcolhCollapse
        '
        Me.objdgcolhCollapse.HeaderText = ""
        Me.objdgcolhCollapse.Name = "objdgcolhCollapse"
        Me.objdgcolhCollapse.ReadOnly = True
        Me.objdgcolhCollapse.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.objdgcolhCollapse.Visible = False
        Me.objdgcolhCollapse.Width = 25
        '
        'objdgcolhCheck
        '
        Me.objdgcolhCheck.HeaderText = ""
        Me.objdgcolhCheck.Name = "objdgcolhCheck"
        Me.objdgcolhCheck.ReadOnly = True
        Me.objdgcolhCheck.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.objdgcolhCheck.Visible = False
        Me.objdgcolhCheck.Width = 25
        '
        'dgcolhDAParam
        '
        Me.dgcolhDAParam.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.dgcolhDAParam.HeaderText = "Mapped Parameter (Aruti Parameter -> Orbit Parameter)"
        Me.dgcolhDAParam.Name = "dgcolhDAParam"
        Me.dgcolhDAParam.ReadOnly = True
        Me.dgcolhDAParam.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'objdgcolhEdit
        '
        Me.objdgcolhEdit.HeaderText = ""
        Me.objdgcolhEdit.Image = Global.Aruti.Data.My.Resources.Resources.edit
        Me.objdgcolhEdit.Name = "objdgcolhEdit"
        Me.objdgcolhEdit.ReadOnly = True
        Me.objdgcolhEdit.Visible = False
        Me.objdgcolhEdit.Width = 25
        '
        'objdgcolhDelete
        '
        Me.objdgcolhDelete.HeaderText = ""
        Me.objdgcolhDelete.Image = Global.Aruti.Data.My.Resources.Resources.remove
        Me.objdgcolhDelete.Name = "objdgcolhDelete"
        Me.objdgcolhDelete.ReadOnly = True
        Me.objdgcolhDelete.Width = 25
        '
        'objdgcolhMappingTranId
        '
        Me.objdgcolhMappingTranId.HeaderText = "objdgcolhMappingTranId"
        Me.objdgcolhMappingTranId.Name = "objdgcolhMappingTranId"
        Me.objdgcolhMappingTranId.Visible = False
        '
        'objdgcolhIsGrp
        '
        Me.objdgcolhIsGrp.HeaderText = "objdgcolhIsGrp"
        Me.objdgcolhIsGrp.Name = "objdgcolhIsGrp"
        Me.objdgcolhIsGrp.Visible = False
        '
        'objdgcolhIsGrpId
        '
        Me.objdgcolhIsGrpId.HeaderText = "objdgcolhIsGrpId"
        Me.objdgcolhIsGrpId.Name = "objdgcolhIsGrpId"
        Me.objdgcolhIsGrpId.Visible = False
        '
        'objdgcolhallocationrefid
        '
        Me.objdgcolhallocationrefid.HeaderText = "objdgcolhallocationrefid"
        Me.objdgcolhallocationrefid.Name = "objdgcolhallocationrefid"
        Me.objdgcolhallocationrefid.Visible = False
        '
        'objdgcolharutiorbitrefid
        '
        Me.objdgcolharutiorbitrefid.HeaderText = "objdgcolharutiorbitrefid"
        Me.objdgcolharutiorbitrefid.Name = "objdgcolharutiorbitrefid"
        Me.objdgcolharutiorbitrefid.Visible = False
        '
        'objdgcolhallocationunkid
        '
        Me.objdgcolhallocationunkid.HeaderText = "objdgcolhallocationunkid"
        Me.objdgcolhallocationunkid.Name = "objdgcolhallocationunkid"
        Me.objdgcolhallocationunkid.Visible = False
        '
        'objdgcolhorbitfldkey
        '
        Me.objdgcolhorbitfldkey.HeaderText = "objdgcolhorbitfldkey"
        Me.objdgcolhorbitfldkey.Name = "objdgcolhorbitfldkey"
        Me.objdgcolhorbitfldkey.Visible = False
        '
        'txtSearchMapping
        '
        Me.txtSearchMapping.Flags = 0
        Me.txtSearchMapping.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtSearchMapping.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92), Global.Microsoft.VisualBasic.ChrW(91), Global.Microsoft.VisualBasic.ChrW(93)}
        Me.txtSearchMapping.Location = New System.Drawing.Point(405, 26)
        Me.txtSearchMapping.Name = "txtSearchMapping"
        Me.txtSearchMapping.Size = New System.Drawing.Size(536, 21)
        Me.txtSearchMapping.TabIndex = 109
        '
        'objspc1
        '
        Me.objspc1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objspc1.IsSplitterFixed = True
        Me.objspc1.Location = New System.Drawing.Point(3, 26)
        Me.objspc1.Name = "objspc1"
        Me.objspc1.Orientation = System.Windows.Forms.Orientation.Horizontal
        '
        'objspc1.Panel1
        '
        Me.objspc1.Panel1.Controls.Add(Me.tblInformation)
        Me.objspc1.Panel1MinSize = 20
        '
        'objspc1.Panel2
        '
        Me.objspc1.Panel2.Controls.Add(Me.btnSaveMapping)
        Me.objspc1.Panel2.Controls.Add(Me.txtSearch)
        Me.objspc1.Panel2.Controls.Add(Me.dgvData)
        Me.objspc1.Panel2MinSize = 20
        Me.objspc1.Size = New System.Drawing.Size(398, 458)
        Me.objspc1.SplitterWidth = 1
        Me.objspc1.TabIndex = 101
        '
        'tblInformation
        '
        Me.tblInformation.ColumnCount = 2
        Me.tblInformation.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.tblInformation.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.tblInformation.Controls.Add(Me.cboOrbitParams, 1, 1)
        Me.tblInformation.Controls.Add(Me.lblArutiDictionary, 0, 0)
        Me.tblInformation.Controls.Add(Me.cboArutiParams, 0, 1)
        Me.tblInformation.Controls.Add(Me.lblOrbitDictionary, 1, 0)
        Me.tblInformation.Dock = System.Windows.Forms.DockStyle.Fill
        Me.tblInformation.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tblInformation.Location = New System.Drawing.Point(0, 0)
        Me.tblInformation.Name = "tblInformation"
        Me.tblInformation.RowCount = 2
        Me.tblInformation.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 23.0!))
        Me.tblInformation.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20.0!))
        Me.tblInformation.Size = New System.Drawing.Size(398, 50)
        Me.tblInformation.TabIndex = 100
        '
        'cboOrbitParams
        '
        Me.cboOrbitParams.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest
        Me.cboOrbitParams.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.cboOrbitParams.Dock = System.Windows.Forms.DockStyle.Fill
        Me.cboOrbitParams.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboOrbitParams.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboOrbitParams.FormattingEnabled = True
        Me.cboOrbitParams.Location = New System.Drawing.Point(202, 26)
        Me.cboOrbitParams.Name = "cboOrbitParams"
        Me.cboOrbitParams.Size = New System.Drawing.Size(193, 21)
        Me.cboOrbitParams.TabIndex = 103
        '
        'lblArutiDictionary
        '
        Me.lblArutiDictionary.Dock = System.Windows.Forms.DockStyle.Fill
        Me.lblArutiDictionary.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblArutiDictionary.Location = New System.Drawing.Point(3, 0)
        Me.lblArutiDictionary.Name = "lblArutiDictionary"
        Me.lblArutiDictionary.Size = New System.Drawing.Size(193, 23)
        Me.lblArutiDictionary.TabIndex = 0
        Me.lblArutiDictionary.Text = "Aruti Dictionary"
        Me.lblArutiDictionary.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'cboArutiParams
        '
        Me.cboArutiParams.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest
        Me.cboArutiParams.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.cboArutiParams.Dock = System.Windows.Forms.DockStyle.Fill
        Me.cboArutiParams.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboArutiParams.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboArutiParams.FormattingEnabled = True
        Me.cboArutiParams.Location = New System.Drawing.Point(3, 26)
        Me.cboArutiParams.Name = "cboArutiParams"
        Me.cboArutiParams.Size = New System.Drawing.Size(193, 21)
        Me.cboArutiParams.TabIndex = 99
        '
        'lblOrbitDictionary
        '
        Me.lblOrbitDictionary.Dock = System.Windows.Forms.DockStyle.Fill
        Me.lblOrbitDictionary.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblOrbitDictionary.Location = New System.Drawing.Point(202, 0)
        Me.lblOrbitDictionary.Name = "lblOrbitDictionary"
        Me.lblOrbitDictionary.Size = New System.Drawing.Size(193, 23)
        Me.lblOrbitDictionary.TabIndex = 1
        Me.lblOrbitDictionary.Text = "Orbit Dictionary"
        Me.lblOrbitDictionary.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'btnSaveMapping
        '
        Me.btnSaveMapping.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnSaveMapping.BackColor = System.Drawing.Color.White
        Me.btnSaveMapping.BackgroundImage = CType(resources.GetObject("btnSaveMapping.BackgroundImage"), System.Drawing.Image)
        Me.btnSaveMapping.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnSaveMapping.BorderColor = System.Drawing.Color.Empty
        Me.btnSaveMapping.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnSaveMapping.FlatAppearance.BorderSize = 0
        Me.btnSaveMapping.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnSaveMapping.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSaveMapping.ForeColor = System.Drawing.Color.Black
        Me.btnSaveMapping.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnSaveMapping.GradientForeColor = System.Drawing.Color.Black
        Me.btnSaveMapping.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSaveMapping.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnSaveMapping.Location = New System.Drawing.Point(281, 373)
        Me.btnSaveMapping.Name = "btnSaveMapping"
        Me.btnSaveMapping.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSaveMapping.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnSaveMapping.Size = New System.Drawing.Size(114, 30)
        Me.btnSaveMapping.TabIndex = 120
        Me.btnSaveMapping.Text = "Save &Mapping"
        Me.btnSaveMapping.UseVisualStyleBackColor = True
        '
        'txtSearch
        '
        Me.txtSearch.Flags = 0
        Me.txtSearch.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92), Global.Microsoft.VisualBasic.ChrW(91), Global.Microsoft.VisualBasic.ChrW(93)}
        Me.txtSearch.Location = New System.Drawing.Point(3, 2)
        Me.txtSearch.Name = "txtSearch"
        Me.txtSearch.Size = New System.Drawing.Size(392, 21)
        Me.txtSearch.TabIndex = 107
        '
        'dgvData
        '
        Me.dgvData.AllowUserToAddRows = False
        Me.dgvData.AllowUserToDeleteRows = False
        Me.dgvData.AllowUserToResizeRows = False
        Me.dgvData.BackgroundColor = System.Drawing.Color.White
        Me.dgvData.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle2.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvData.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle2
        Me.dgvData.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing
        Me.dgvData.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.dgcolhAParam, Me.dgcolhOParam, Me.objdgcolhAPraramId, Me.objdgcolhfieldName, Me.objdgcolhKey, Me.objdgcolhValue, Me.objdgcolhMaptranguid})
        Me.dgvData.Location = New System.Drawing.Point(3, 24)
        Me.dgvData.Name = "dgvData"
        Me.dgvData.RowHeadersVisible = False
        Me.dgvData.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing
        Me.dgvData.Size = New System.Drawing.Size(392, 343)
        Me.dgvData.TabIndex = 0
        '
        'dgcolhAParam
        '
        Me.dgcolhAParam.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.dgcolhAParam.HeaderText = "Aruti Parameters"
        Me.dgcolhAParam.Name = "dgcolhAParam"
        Me.dgcolhAParam.ReadOnly = True
        Me.dgcolhAParam.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Programmatic
        '
        'dgcolhOParam
        '
        Me.dgcolhOParam.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.dgcolhOParam.HeaderText = "Orbit Parameters"
        Me.dgcolhOParam.Name = "dgcolhOParam"
        Me.dgcolhOParam.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgcolhOParam.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Programmatic
        '
        'objdgcolhAPraramId
        '
        Me.objdgcolhAPraramId.HeaderText = "objdgcolhAPraramId"
        Me.objdgcolhAPraramId.Name = "objdgcolhAPraramId"
        Me.objdgcolhAPraramId.ReadOnly = True
        Me.objdgcolhAPraramId.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Programmatic
        Me.objdgcolhAPraramId.Visible = False
        '
        'objdgcolhfieldName
        '
        Me.objdgcolhfieldName.HeaderText = "objdgcolhfieldName"
        Me.objdgcolhfieldName.Name = "objdgcolhfieldName"
        Me.objdgcolhfieldName.ReadOnly = True
        Me.objdgcolhfieldName.Visible = False
        '
        'objdgcolhKey
        '
        Me.objdgcolhKey.HeaderText = "objdgcolhKey"
        Me.objdgcolhKey.Name = "objdgcolhKey"
        Me.objdgcolhKey.ReadOnly = True
        Me.objdgcolhKey.Visible = False
        '
        'objdgcolhValue
        '
        Me.objdgcolhValue.HeaderText = "objdgcolhValue"
        Me.objdgcolhValue.Name = "objdgcolhValue"
        Me.objdgcolhValue.ReadOnly = True
        Me.objdgcolhValue.Visible = False
        '
        'objdgcolhMaptranguid
        '
        Me.objdgcolhMaptranguid.HeaderText = "objdgcolhMaptranguid"
        Me.objdgcolhMaptranguid.Name = "objdgcolhMaptranguid"
        Me.objdgcolhMaptranguid.ReadOnly = True
        Me.objdgcolhMaptranguid.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Programmatic
        Me.objdgcolhMaptranguid.Visible = False
        '
        'objFooter
        '
        Me.objFooter.BorderColor = System.Drawing.Color.Silver
        Me.objFooter.Controls.Add(Me.lblNote)
        Me.objFooter.Controls.Add(Me.btnClose)
        Me.objFooter.Controls.Add(Me.btnEditChecked)
        Me.objFooter.Controls.Add(Me.btnDeleteChecked)
        Me.objFooter.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.objFooter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objFooter.GradiantStyle = eZee.Common.eZeeFooter.GradientStyle.Up
        Me.objFooter.GradientColor1 = System.Drawing.SystemColors.Control
        Me.objFooter.GradientColor2 = System.Drawing.SystemColors.Control
        Me.objFooter.Location = New System.Drawing.Point(0, 486)
        Me.objFooter.Name = "objFooter"
        Me.objFooter.Size = New System.Drawing.Size(944, 55)
        Me.objFooter.TabIndex = 5
        '
        'lblNote
        '
        Me.lblNote.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblNote.ForeColor = System.Drawing.Color.Red
        Me.lblNote.Location = New System.Drawing.Point(13, 16)
        Me.lblNote.Name = "lblNote"
        Me.lblNote.Size = New System.Drawing.Size(766, 28)
        Me.lblNote.TabIndex = 125
        Me.lblNote.Text = "Only Unmapped Parameter From Aruti will be displayed and allowed to map. Those wh" & _
            "ich are already mapped they will not come into the list of mapping with Orbit pa" & _
            "rameter."
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnClose.BackColor = System.Drawing.Color.White
        Me.btnClose.BackgroundImage = CType(resources.GetObject("btnClose.BackgroundImage"), System.Drawing.Image)
        Me.btnClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnClose.BorderColor = System.Drawing.Color.Empty
        Me.btnClose.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnClose.FlatAppearance.BorderSize = 0
        Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClose.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.ForeColor = System.Drawing.Color.Black
        Me.btnClose.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnClose.GradientForeColor = System.Drawing.Color.Black
        Me.btnClose.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Location = New System.Drawing.Point(835, 13)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Size = New System.Drawing.Size(97, 30)
        Me.btnClose.TabIndex = 119
        Me.btnClose.Text = "&Close"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'btnEditChecked
        '
        Me.btnEditChecked.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnEditChecked.BackColor = System.Drawing.Color.White
        Me.btnEditChecked.BackgroundImage = CType(resources.GetObject("btnEditChecked.BackgroundImage"), System.Drawing.Image)
        Me.btnEditChecked.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnEditChecked.BorderColor = System.Drawing.Color.Empty
        Me.btnEditChecked.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnEditChecked.FlatAppearance.BorderSize = 0
        Me.btnEditChecked.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnEditChecked.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnEditChecked.ForeColor = System.Drawing.Color.Black
        Me.btnEditChecked.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnEditChecked.GradientForeColor = System.Drawing.Color.Black
        Me.btnEditChecked.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnEditChecked.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnEditChecked.Location = New System.Drawing.Point(922, 13)
        Me.btnEditChecked.Name = "btnEditChecked"
        Me.btnEditChecked.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnEditChecked.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnEditChecked.Size = New System.Drawing.Size(10, 30)
        Me.btnEditChecked.TabIndex = 124
        Me.btnEditChecked.Text = "Edit Checked"
        Me.btnEditChecked.UseVisualStyleBackColor = True
        Me.btnEditChecked.Visible = False
        '
        'btnDeleteChecked
        '
        Me.btnDeleteChecked.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnDeleteChecked.BackColor = System.Drawing.Color.White
        Me.btnDeleteChecked.BackgroundImage = CType(resources.GetObject("btnDeleteChecked.BackgroundImage"), System.Drawing.Image)
        Me.btnDeleteChecked.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnDeleteChecked.BorderColor = System.Drawing.Color.Empty
        Me.btnDeleteChecked.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnDeleteChecked.FlatAppearance.BorderSize = 0
        Me.btnDeleteChecked.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnDeleteChecked.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnDeleteChecked.ForeColor = System.Drawing.Color.Black
        Me.btnDeleteChecked.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnDeleteChecked.GradientForeColor = System.Drawing.Color.Black
        Me.btnDeleteChecked.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnDeleteChecked.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnDeleteChecked.Location = New System.Drawing.Point(922, 13)
        Me.btnDeleteChecked.Name = "btnDeleteChecked"
        Me.btnDeleteChecked.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnDeleteChecked.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnDeleteChecked.Size = New System.Drawing.Size(10, 30)
        Me.btnDeleteChecked.TabIndex = 121
        Me.btnDeleteChecked.Text = "Delete Checked"
        Me.btnDeleteChecked.UseVisualStyleBackColor = True
        Me.btnDeleteChecked.Visible = False
        '
        'DataGridViewTextBoxColumn1
        '
        Me.DataGridViewTextBoxColumn1.HeaderText = ""
        Me.DataGridViewTextBoxColumn1.Name = "DataGridViewTextBoxColumn1"
        Me.DataGridViewTextBoxColumn1.ReadOnly = True
        Me.DataGridViewTextBoxColumn1.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn1.Width = 25
        '
        'DataGridViewTextBoxColumn2
        '
        Me.DataGridViewTextBoxColumn2.HeaderText = ""
        Me.DataGridViewTextBoxColumn2.Name = "DataGridViewTextBoxColumn2"
        Me.DataGridViewTextBoxColumn2.ReadOnly = True
        Me.DataGridViewTextBoxColumn2.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn2.Visible = False
        Me.DataGridViewTextBoxColumn2.Width = 25
        '
        'DataGridViewTextBoxColumn3
        '
        Me.DataGridViewTextBoxColumn3.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.DataGridViewTextBoxColumn3.HeaderText = "Aruti Parameter"
        Me.DataGridViewTextBoxColumn3.Name = "DataGridViewTextBoxColumn3"
        Me.DataGridViewTextBoxColumn3.ReadOnly = True
        Me.DataGridViewTextBoxColumn3.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'DataGridViewTextBoxColumn4
        '
        Me.DataGridViewTextBoxColumn4.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.DataGridViewTextBoxColumn4.HeaderText = "Orbit Parameter"
        Me.DataGridViewTextBoxColumn4.Name = "DataGridViewTextBoxColumn4"
        Me.DataGridViewTextBoxColumn4.ReadOnly = True
        Me.DataGridViewTextBoxColumn4.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'DataGridViewTextBoxColumn5
        '
        Me.DataGridViewTextBoxColumn5.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.DataGridViewTextBoxColumn5.HeaderText = "Aruti Parameters"
        Me.DataGridViewTextBoxColumn5.Name = "DataGridViewTextBoxColumn5"
        Me.DataGridViewTextBoxColumn5.ReadOnly = True
        Me.DataGridViewTextBoxColumn5.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Programmatic
        '
        'DataGridViewTextBoxColumn6
        '
        Me.DataGridViewTextBoxColumn6.HeaderText = "objdgcolhAPraramId"
        Me.DataGridViewTextBoxColumn6.Name = "DataGridViewTextBoxColumn6"
        Me.DataGridViewTextBoxColumn6.ReadOnly = True
        Me.DataGridViewTextBoxColumn6.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Programmatic
        Me.DataGridViewTextBoxColumn6.Visible = False
        '
        'DataGridViewTextBoxColumn7
        '
        Me.DataGridViewTextBoxColumn7.HeaderText = "objdgcolhfieldName"
        Me.DataGridViewTextBoxColumn7.Name = "DataGridViewTextBoxColumn7"
        Me.DataGridViewTextBoxColumn7.ReadOnly = True
        Me.DataGridViewTextBoxColumn7.Visible = False
        '
        'DataGridViewTextBoxColumn8
        '
        Me.DataGridViewTextBoxColumn8.HeaderText = "objdgcolhKey"
        Me.DataGridViewTextBoxColumn8.Name = "DataGridViewTextBoxColumn8"
        Me.DataGridViewTextBoxColumn8.ReadOnly = True
        Me.DataGridViewTextBoxColumn8.Visible = False
        '
        'DataGridViewTextBoxColumn9
        '
        Me.DataGridViewTextBoxColumn9.HeaderText = "objdgcolhValue"
        Me.DataGridViewTextBoxColumn9.Name = "DataGridViewTextBoxColumn9"
        Me.DataGridViewTextBoxColumn9.ReadOnly = True
        Me.DataGridViewTextBoxColumn9.Visible = False
        '
        'DataGridViewTextBoxColumn10
        '
        Me.DataGridViewTextBoxColumn10.HeaderText = "objdgcolhMaptranguid"
        Me.DataGridViewTextBoxColumn10.Name = "DataGridViewTextBoxColumn10"
        Me.DataGridViewTextBoxColumn10.ReadOnly = True
        Me.DataGridViewTextBoxColumn10.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Programmatic
        Me.DataGridViewTextBoxColumn10.Visible = False
        '
        'frmOrbitDictionaryMapping
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(944, 541)
        Me.Controls.Add(Me.Panel1)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmOrbitDictionaryMapping"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Orbit Dictionary Parameter Mapping"
        Me.Panel1.ResumeLayout(False)
        Me.gbDictionaryParmeters.ResumeLayout(False)
        Me.gbDictionaryParmeters.PerformLayout()
        Me.objpnlData.ResumeLayout(False)
        CType(Me.dgvMapping, System.ComponentModel.ISupportInitialize).EndInit()
        Me.objspc1.Panel1.ResumeLayout(False)
        Me.objspc1.Panel2.ResumeLayout(False)
        Me.objspc1.Panel2.PerformLayout()
        Me.objspc1.ResumeLayout(False)
        Me.tblInformation.ResumeLayout(False)
        CType(Me.dgvData, System.ComponentModel.ISupportInitialize).EndInit()
        Me.objFooter.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents objFooter As eZee.Common.eZeeFooter
    Friend WithEvents btnClose As eZee.Common.eZeeLightButton
    Friend WithEvents gbDictionaryParmeters As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents objspc1 As System.Windows.Forms.SplitContainer
    Friend WithEvents tblInformation As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents cboArutiParams As System.Windows.Forms.ComboBox
    Friend WithEvents cboOrbitParams As System.Windows.Forms.ComboBox
    Friend WithEvents lblArutiDictionary As System.Windows.Forms.Label
    Friend WithEvents lblOrbitDictionary As System.Windows.Forms.Label
    Friend WithEvents dgvData As System.Windows.Forms.DataGridView
    Friend WithEvents btnSaveMapping As eZee.Common.eZeeLightButton
    Friend WithEvents txtSearch As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents txtSearchMapping As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents dgvMapping As System.Windows.Forms.DataGridView
    Friend WithEvents btnDeleteChecked As eZee.Common.eZeeLightButton
    Friend WithEvents objpnlData As System.Windows.Forms.Panel
    Friend WithEvents btnEditChecked As eZee.Common.eZeeLightButton
    Friend WithEvents dgcolhAParam As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhOParam As System.Windows.Forms.DataGridViewComboBoxColumn
    Friend WithEvents objdgcolhAPraramId As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhfieldName As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhKey As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhValue As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhMaptranguid As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn2 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn3 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn4 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn5 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn6 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn7 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn8 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn9 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn10 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents lblNote As System.Windows.Forms.Label
    Friend WithEvents objdgcolhCollapse As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhCheck As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhDAParam As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhEdit As System.Windows.Forms.DataGridViewImageColumn
    Friend WithEvents objdgcolhDelete As System.Windows.Forms.DataGridViewImageColumn
    Friend WithEvents objdgcolhMappingTranId As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhIsGrp As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhIsGrpId As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhallocationrefid As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolharutiorbitrefid As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhallocationunkid As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhorbitfldkey As System.Windows.Forms.DataGridViewTextBoxColumn
End Class
