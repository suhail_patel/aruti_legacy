#Region " Imports "
Imports eZeeCommonLib
Imports Aruti.Data
Imports Aruti.Data.Language
#End Region

Public Class frmReminderDisplay

#Region " Private Variable "
    Private ReadOnly mstrModuleName As String = "frmReminderDisplay"
    Dim objReminder As clsreminder_master
    Dim mintReminderUnkID As Integer = -1
    Dim strSnoozType As String = ""
#End Region

#Region " Display Dialog "
    Public Sub displayDialog(ByVal intReminerUnkId As Integer)
        Try
            mintReminderUnkID = intReminerUnkId
            Me.ShowDialog()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "displayDialog", mstrModuleName)
        End Try
    End Sub
#End Region

#Region " Private Methods "

    Private Sub GetData()
        Try
            Dim objuser As New clsUserAddEdit

            Select Case objReminder._Priority
                Case 2
                    objlblDueDateTime.BackColor = Drawing.Color.Thistle
                Case 1
                    objlblDueDateTime.BackColor = Drawing.Color.SteelBlue
                Case 0
                    objlblDueDateTime.BackColor = Drawing.Color.Red
            End Select
            objuser._Userunkid = objReminder._Userunkid

            objlblDueDateTime.Text = objuser._Username & " -- " & CStr(objReminder._Start_Date)
            txttitle.Text = objReminder._Title
            txtMessage.Text = objReminder._ReminderMessage
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetData", mstrModuleName)
        End Try
    End Sub

    Private Sub SetNextReminder()
        Dim blnSnoozRemainder As Boolean = True
        Dim dtNextDay As Date
        Try
            Select Case strSnoozType.ToUpper
                Case "miFiveMin".ToUpper
                    dtNextDay = DateAdd(DateInterval.Minute, 5, Now)
                Case "miTenMin".ToUpper
                    dtNextDay = DateAdd(DateInterval.Minute, 10, Now)
                Case "miTwentyMin".ToUpper
                    dtNextDay = DateAdd(DateInterval.Minute, 20, Now)
                Case "miThirtyMin".ToUpper
                    dtNextDay = DateAdd(DateInterval.Minute, 30, Now)
                Case "miOneHour".ToUpper
                    dtNextDay = DateAdd(DateInterval.Hour, 1, Now)
                Case "miTwoHour".ToUpper
                    dtNextDay = DateAdd(DateInterval.Hour, 2, Now)
                Case "miFourHours".ToUpper
                    dtNextDay = DateAdd(DateInterval.Hour, 4, Now)
                Case "miEightHours".ToUpper
                    dtNextDay = DateAdd(DateInterval.Hour, 8, Now)
                Case "miOneDay".ToUpper
                    dtNextDay = DateAdd(DateInterval.Day, 1, Now)
                Case "miTwoDay".ToUpper
                    dtNextDay = DateAdd(DateInterval.Day, 2, Now)
                Case "miThreeDays".ToUpper
                    dtNextDay = DateAdd(DateInterval.Day, 3, Now)
                Case "miOneWeek".ToUpper
                    dtNextDay = DateAdd(DateInterval.Day, 7, Now)
                Case Else
                    Select Case objReminder._Interval_Type
                        Case 0 'Once
                            blnSnoozRemainder = False
                            dtNextDay = Now
                        Case 1 'Daily
                            dtNextDay = DateAdd(DateInterval.Day, objReminder._Frequency, Now)
                        Case 2 'Weekly
                            dtNextDay = DateAdd(DateInterval.Day, objReminder._Frequency * 7, Now)
                        Case 3 'Monthly
                            dtNextDay = DateAdd(DateInterval.Month, objReminder._Frequency, Now)
                        Case 4 'yearly
                            dtNextDay = DateAdd(DateInterval.Year, objReminder._Frequency, Now)
                    End Select
                    If Not objReminder._Stopreminder = Nothing Then
                        If eZeeDate.convertDate(dtNextDay) < eZeeDate.convertDate(CDate(objReminder._Stopreminder)) Then
                            blnSnoozRemainder = (objReminder._Interval_Type <> 0)
                        End If
                    End If
            End Select

            'S.SANDEEP [28-May-2018] -- START
            'ISSUE/ENHANCEMENT : {Audit Trails} 
            objReminder._FormName = mstrModuleName
            objReminder._LoginEmployeeunkid = 0
            objReminder._ClientIP = getIP()
            objReminder._HostName = getHostName()
            objReminder._FromWeb = False
            objReminder._AuditUserId = User._Object._Userunkid
objReminder._CompanyUnkid = Company._Object._Companyunkid
            objReminder._AuditDate = ConfigParameter._Object._CurrentDateAndTime
            'S.SANDEEP [28-May-2018] -- END

            If blnSnoozRemainder Then
                objReminder.SnoozRemainder(mintReminderUnkID, User._Object._UserUnkId, dtNextDay)
            Else
                objReminder.MarkAsRead(mintReminderUnkID, User._Object._UserUnkId)
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetNextReminder", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Form's Events "
    Private Sub frmReminderDisplay_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        Try
            objReminder = Nothing
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "frmReminderDisplay_FormClosed", mstrModuleName)
        End Try
    End Sub

    Private Sub frmReminderDisplay_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        Try
            Call setNextReminder()
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "frmReminderDisplay_FormClosing", mstrModuleName)
        End Try
    End Sub

    Private Sub frmReminderDisplay_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            Call Set_Logo(Me, gApplicationType)
            objReminder = New clsreminder_master
            'Call Language.setLanguage(Me.Name)


            txttitle.BackColor = Me.BackColor
            txtMessage.BackColor = Me.BackColor

            objReminder._Reminderunkid = mintReminderUnkID

            Call GetData()

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmReminderDisplay_Load", mstrModuleName)
        End Try
    End Sub
    'Anjan (02 Sep 2011)-Start
    'Issue : Including Language Settings.
    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub
    'Anjan (02 Sep 2011)-End 

#End Region

#Region " Button's Events "
    Private Sub btnOK_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) _
            Handles btnOK.Click, _
                    miFiveMin.Click, _
                    miTenMin.Click, _
                    miTwentyMin.Click, _
                    miThirtyMin.Click, _
                    miOneHour.Click, _
                    miTwoHour.Click, _
                    miFourHours.Click, _
                    miEightHours.Click, _
                    miOneDay.Click, _
                    miTwoDay.Click, _
                    miThreeDays.Click, _
                    miOneWeek.Click

        Try

            strSnoozType = sender.Name
            Me.Close()

        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "btnOK_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnSnooze_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSnooze.Click
        cmsSoonze.Show(sender, CInt(0), CInt(0))
    End Sub
#End Region

	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Private Sub OtherSettings()
		Try
			Me.SuspendLayout()
			
			Call SetLanguage()
			
			Me.btnOK.GradientBackColor = GUI._ButttonBackColor 
			Me.btnOK.GradientForeColor = GUI._ButttonFontColor

			Me.btnSnooze.GradientBackColor = GUI._ButttonBackColor 
			Me.btnSnooze.GradientForeColor = GUI._ButttonFontColor


			Me.ResumeLayout()
		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "OtherSettings", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetLanguage()
		Try
			Me.Text = Language._Object.getCaption(Me.Name, Me.Text)
			
			Me.btnOK.Text = Language._Object.getCaption(Me.btnOK.Name, Me.btnOK.Text)
			Me.lblMessage.Text = Language._Object.getCaption(Me.lblMessage.Name, Me.lblMessage.Text)
			Me.miFiveMin.Text = Language._Object.getCaption(Me.miFiveMin.Name, Me.miFiveMin.Text)
			Me.miTenMin.Text = Language._Object.getCaption(Me.miTenMin.Name, Me.miTenMin.Text)
			Me.miTwentyMin.Text = Language._Object.getCaption(Me.miTwentyMin.Name, Me.miTwentyMin.Text)
			Me.miThirtyMin.Text = Language._Object.getCaption(Me.miThirtyMin.Name, Me.miThirtyMin.Text)
			Me.miOneHour.Text = Language._Object.getCaption(Me.miOneHour.Name, Me.miOneHour.Text)
			Me.miTwoHour.Text = Language._Object.getCaption(Me.miTwoHour.Name, Me.miTwoHour.Text)
			Me.miFourHours.Text = Language._Object.getCaption(Me.miFourHours.Name, Me.miFourHours.Text)
			Me.miEightHours.Text = Language._Object.getCaption(Me.miEightHours.Name, Me.miEightHours.Text)
			Me.miOneDay.Text = Language._Object.getCaption(Me.miOneDay.Name, Me.miOneDay.Text)
			Me.miTwoDay.Text = Language._Object.getCaption(Me.miTwoDay.Name, Me.miTwoDay.Text)
			Me.miThreeDays.Text = Language._Object.getCaption(Me.miThreeDays.Name, Me.miThreeDays.Text)
			Me.miOneWeek.Text = Language._Object.getCaption(Me.miOneWeek.Name, Me.miOneWeek.Text)
			Me.lblTitle.Text = Language._Object.getCaption(Me.lblTitle.Name, Me.lblTitle.Text)
			Me.btnSnooze.Text = Language._Object.getCaption(Me.btnSnooze.Name, Me.btnSnooze.Text)

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetLanguage", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetMessages()
		Try

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class