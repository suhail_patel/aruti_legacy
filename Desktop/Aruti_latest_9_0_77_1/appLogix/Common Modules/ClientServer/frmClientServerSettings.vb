Imports eZeeCommonLib
Public Class frmClientServerSettings

    Dim mstrModuleName As String = "frmClientServerSettings"

    Private objMachineSettings As clsGeneralSettings

    Private Sub frmClientServerSettings_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            'Call Language.setLanguage(Me.Name)
            'Me.ShowLanguageButton = User._Object.FD._AllowChangeLanguage
            'Call SetLanguage()

            'Call OtherSettings()

            objMachineSettings = New clsGeneralSettings()
            objMachineSettings._Section = gApplicationType.ToString
            cboWorkingMode.Text = objMachineSettings._WorkingMode
            txtServer.Text = objMachineSettings._ServerName
            'S.SANDEEP [21-NOV-2017] -- START
            txtSQLPort.Text = objMachineSettings._SQLPortNumber
            'S.SANDEEP [21-NOV-2017] -- END
            If CDbl(objMachineSettings._RefreshInterval) < 1 Then
                nudRefreshInterval.Value = 1
            Else
                nudRefreshInterval.Value = CInt(objMachineSettings._RefreshInterval)
            End If

        Catch ex As Exception
            'DisplayError.Show("-1", ex.Message, "frmClientServerSettings_Load", mstrModuleName)
        End Try
    End Sub

    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Try
            objMachineSettings._Section = gApplicationType.ToString
            objMachineSettings._WorkingMode = cboWorkingMode.Text
            objMachineSettings._ServerName = txtServer.Text
            objMachineSettings._RefreshInterval = CStr(nudRefreshInterval.Value)
            'S.SANDEEP [ 10 NOV 2014 ] -- START
            If txtAutoUpdateServer.Text.Trim.Length <= 0 Then
                objMachineSettings._AutoUpdate_Server = txtServer.Text
            Else
                objMachineSettings._AutoUpdate_Server = txtAutoUpdateServer.Text
            End If
            'S.SANDEEP [ 10 NOV 2014 ] -- END

            'S.SANDEEP [21-NOV-2017] -- START
            objMachineSettings._SQLPortNumber = CInt(txtSQLPort.Text)
            'S.SANDEEP [21-NOV-2017] -- END

            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnSave_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Me.Close()
    End Sub

    'S.SANDEEP [ 10 NOV 2014 ] -- START
    Private Sub txtServer_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtServer.TextChanged
        Try
            txtAutoUpdateServer.Text = txtServer.Text
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "txtServer_TextChanged", mstrModuleName)
        Finally
        End Try
    End Sub
    'S.SANDEEP [ 10 NOV 2014 ] -- END

    'S.SANDEEP [21-NOV-2017] -- START
    Private Sub objbtnInstructions_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnInstructions.Click
        Try
            Dim strMsg As String = String.Empty
            strMsg = "To Find SQL Current Port Number Follow Following Steps: " & vbCrLf
            strMsg &= "1. Go to Server Machine, And Press (Windows Key + R) to Activate Run Command Window. " & vbCrLf
            strMsg &= "2. Depending on the SQL Setup Installed, Type Following Command on Run Window " & vbCrLf
            strMsg &= "     a).SQL 2008 -> SQLServerManager10.msc " & vbCrLf
            strMsg &= "     b).SQL 2012 -> SQLServerManager11.msc " & vbCrLf
            strMsg &= "     c).SQL 2014 -> SQLServerManager12.msc " & vbCrLf
            strMsg &= "     d).SQL 2016 -> SQLServerManager13.msc " & vbCrLf
            strMsg &= "     e).SQL 2017 -> SQLServerManager14.msc " & vbCrLf
            strMsg &= "3. New Window will Open With Title as 'SQl Server Configuration Manager'. " & vbCrLf
            strMsg &= "4. In the above Mentioned Window Find 'SQL Server Network Configuration' -> Protocols for APAYROLL, and click it. " & vbCrLf
            strMsg &= "5. On the right pane, TCP/IP, Enable it, if it is disabled and double click it and go to IP Addresses Tab. " & vbCrLf
            strMsg &= "6. Scroll down to that Tab and Find Section Called 'IPALL'. " & vbCrLf
            strMsg &= "7. In the above mentioned section, there are 2 ports, " & vbCrLf
            strMsg &= "     a).TCP Port            -> This is static port default number is 1433 " & vbCrLf
            strMsg &= "     b).TCP Dynamic Ports   -> This is dynamic, It can have any number allocated by Operating system. " & vbCrLf
            strMsg &= "8. If 'TCP Port' is blank than take the value from 'TCP Dynamic Ports', and Paste on this screen and save. "

            eZeeMsgBox.Show(strMsg, enMsgBoxStyle.Information, Me.Text, False)

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnInstructions_Click", mstrModuleName)
        Finally
        End Try
    End Sub
    'S.SANDEEP [21-NOV-2017] -- END


	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Private Sub OtherSettings()
		Try
			Me.SuspendLayout()
			
			Call SetLanguage()
			
			Me.gbClientServerOption.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbClientServerOption.ForeColor = GUI._eZeeContainerHeaderForeColor 


			Me.btnSave.GradientBackColor = GUI._ButttonBackColor 
			Me.btnSave.GradientForeColor = GUI._ButttonFontColor

			Me.btnClose.GradientBackColor = GUI._ButttonBackColor 
			Me.btnClose.GradientForeColor = GUI._ButttonFontColor


			Me.ResumeLayout()
		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "OtherSettings", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetLanguage()
		Try
			Me.Text = Language._Object.getCaption(Me.Name, Me.Text)
			
			Me.gbClientServerOption.Text = Language._Object.getCaption(Me.gbClientServerOption.Name, Me.gbClientServerOption.Text)
			Me.lblWorkingMode.Text = Language._Object.getCaption(Me.lblWorkingMode.Name, Me.lblWorkingMode.Text)
			Me.lblServer.Text = Language._Object.getCaption(Me.lblServer.Name, Me.lblServer.Text)
			Me.lblMinute.Text = Language._Object.getCaption(Me.lblMinute.Name, Me.lblMinute.Text)
			Me.lblRefreshInterval.Text = Language._Object.getCaption(Me.lblRefreshInterval.Name, Me.lblRefreshInterval.Text)
			Me.btnSave.Text = Language._Object.getCaption(Me.btnSave.Name, Me.btnSave.Text)
			Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetLanguage", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetMessages()
		Try

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class