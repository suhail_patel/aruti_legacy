﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmPermanentlyDeletePPALogs
    Inherits eZee.Common.eZeeForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmPermanentlyDeletePPALogs))
        Me.pnlMain = New System.Windows.Forms.Panel
        Me.gbGroupList = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.cboRemoveFrom = New System.Windows.Forms.ComboBox
        Me.lblRemoveFrom = New System.Windows.Forms.Label
        Me.dtpFromDate = New System.Windows.Forms.DateTimePicker
        Me.lblFromDate = New System.Windows.Forms.Label
        Me.dtpTodate = New System.Windows.Forms.DateTimePicker
        Me.lblTodate = New System.Windows.Forms.Label
        Me.objFooter = New eZee.Common.eZeeFooter
        Me.btnDelete = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnClose = New eZee.Common.eZeeLightButton(Me.components)
        Me.pnlMain.SuspendLayout()
        Me.gbGroupList.SuspendLayout()
        Me.objFooter.SuspendLayout()
        Me.SuspendLayout()
        '
        'pnlMain
        '
        Me.pnlMain.Controls.Add(Me.gbGroupList)
        Me.pnlMain.Controls.Add(Me.objFooter)
        Me.pnlMain.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pnlMain.Location = New System.Drawing.Point(0, 0)
        Me.pnlMain.Name = "pnlMain"
        Me.pnlMain.Size = New System.Drawing.Size(378, 151)
        Me.pnlMain.TabIndex = 0
        '
        'gbGroupList
        '
        Me.gbGroupList.BorderColor = System.Drawing.Color.Black
        Me.gbGroupList.Checked = False
        Me.gbGroupList.CollapseAllExceptThis = False
        Me.gbGroupList.CollapsedHoverImage = Nothing
        Me.gbGroupList.CollapsedNormalImage = Nothing
        Me.gbGroupList.CollapsedPressedImage = Nothing
        Me.gbGroupList.CollapseOnLoad = False
        Me.gbGroupList.Controls.Add(Me.cboRemoveFrom)
        Me.gbGroupList.Controls.Add(Me.lblRemoveFrom)
        Me.gbGroupList.Controls.Add(Me.dtpFromDate)
        Me.gbGroupList.Controls.Add(Me.lblFromDate)
        Me.gbGroupList.Controls.Add(Me.dtpTodate)
        Me.gbGroupList.Controls.Add(Me.lblTodate)
        Me.gbGroupList.Dock = System.Windows.Forms.DockStyle.Fill
        Me.gbGroupList.ExpandedHoverImage = Nothing
        Me.gbGroupList.ExpandedNormalImage = Nothing
        Me.gbGroupList.ExpandedPressedImage = Nothing
        Me.gbGroupList.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbGroupList.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbGroupList.HeaderHeight = 25
        Me.gbGroupList.HeaderMessage = ""
        Me.gbGroupList.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbGroupList.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbGroupList.HeightOnCollapse = 0
        Me.gbGroupList.LeftTextSpace = 0
        Me.gbGroupList.Location = New System.Drawing.Point(0, 0)
        Me.gbGroupList.Name = "gbGroupList"
        Me.gbGroupList.OpenHeight = 63
        Me.gbGroupList.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbGroupList.ShowBorder = True
        Me.gbGroupList.ShowCheckBox = False
        Me.gbGroupList.ShowCollapseButton = False
        Me.gbGroupList.ShowDefaultBorderColor = True
        Me.gbGroupList.ShowDownButton = False
        Me.gbGroupList.ShowHeader = True
        Me.gbGroupList.Size = New System.Drawing.Size(378, 101)
        Me.gbGroupList.TabIndex = 16
        Me.gbGroupList.Temp = 0
        Me.gbGroupList.Text = "Remove Information"
        Me.gbGroupList.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboRemoveFrom
        '
        Me.cboRemoveFrom.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboRemoveFrom.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboRemoveFrom.FormattingEnabled = True
        Me.cboRemoveFrom.Location = New System.Drawing.Point(97, 39)
        Me.cboRemoveFrom.Name = "cboRemoveFrom"
        Me.cboRemoveFrom.Size = New System.Drawing.Size(260, 21)
        Me.cboRemoveFrom.TabIndex = 246
        '
        'lblRemoveFrom
        '
        Me.lblRemoveFrom.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblRemoveFrom.Location = New System.Drawing.Point(12, 41)
        Me.lblRemoveFrom.Name = "lblRemoveFrom"
        Me.lblRemoveFrom.Size = New System.Drawing.Size(78, 17)
        Me.lblRemoveFrom.TabIndex = 245
        Me.lblRemoveFrom.Text = "Remove From"
        Me.lblRemoveFrom.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'dtpFromDate
        '
        Me.dtpFromDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpFromDate.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpFromDate.Location = New System.Drawing.Point(97, 66)
        Me.dtpFromDate.Name = "dtpFromDate"
        Me.dtpFromDate.Size = New System.Drawing.Size(106, 21)
        Me.dtpFromDate.TabIndex = 207
        '
        'lblFromDate
        '
        Me.lblFromDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblFromDate.Location = New System.Drawing.Point(12, 68)
        Me.lblFromDate.Name = "lblFromDate"
        Me.lblFromDate.Size = New System.Drawing.Size(78, 17)
        Me.lblFromDate.TabIndex = 208
        Me.lblFromDate.Text = "From Date"
        Me.lblFromDate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'dtpTodate
        '
        Me.dtpTodate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpTodate.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpTodate.Location = New System.Drawing.Point(251, 66)
        Me.dtpTodate.Name = "dtpTodate"
        Me.dtpTodate.Size = New System.Drawing.Size(106, 21)
        Me.dtpTodate.TabIndex = 209
        '
        'lblTodate
        '
        Me.lblTodate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTodate.Location = New System.Drawing.Point(209, 68)
        Me.lblTodate.Name = "lblTodate"
        Me.lblTodate.Size = New System.Drawing.Size(36, 17)
        Me.lblTodate.TabIndex = 210
        Me.lblTodate.Text = "To"
        Me.lblTodate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objFooter
        '
        Me.objFooter.BorderColor = System.Drawing.Color.Silver
        Me.objFooter.Controls.Add(Me.btnDelete)
        Me.objFooter.Controls.Add(Me.btnClose)
        Me.objFooter.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.objFooter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objFooter.GradiantStyle = eZee.Common.eZeeFooter.GradientStyle.Up
        Me.objFooter.GradientColor1 = System.Drawing.SystemColors.Control
        Me.objFooter.GradientColor2 = System.Drawing.SystemColors.Control
        Me.objFooter.Location = New System.Drawing.Point(0, 101)
        Me.objFooter.Name = "objFooter"
        Me.objFooter.Size = New System.Drawing.Size(378, 50)
        Me.objFooter.TabIndex = 15
        '
        'btnDelete
        '
        Me.btnDelete.BackColor = System.Drawing.Color.White
        Me.btnDelete.BackgroundImage = CType(resources.GetObject("btnDelete.BackgroundImage"), System.Drawing.Image)
        Me.btnDelete.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnDelete.BorderColor = System.Drawing.Color.Empty
        Me.btnDelete.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnDelete.FlatAppearance.BorderSize = 0
        Me.btnDelete.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnDelete.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnDelete.ForeColor = System.Drawing.Color.Black
        Me.btnDelete.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnDelete.GradientForeColor = System.Drawing.Color.Black
        Me.btnDelete.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnDelete.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnDelete.Location = New System.Drawing.Point(12, 10)
        Me.btnDelete.Name = "btnDelete"
        Me.btnDelete.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnDelete.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnDelete.Size = New System.Drawing.Size(131, 30)
        Me.btnDelete.TabIndex = 120
        Me.btnDelete.Text = "&Delete permanently"
        Me.btnDelete.UseVisualStyleBackColor = True
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnClose.BackColor = System.Drawing.Color.White
        Me.btnClose.BackgroundImage = CType(resources.GetObject("btnClose.BackgroundImage"), System.Drawing.Image)
        Me.btnClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnClose.BorderColor = System.Drawing.Color.Empty
        Me.btnClose.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnClose.FlatAppearance.BorderSize = 0
        Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClose.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.ForeColor = System.Drawing.Color.Black
        Me.btnClose.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnClose.GradientForeColor = System.Drawing.Color.Black
        Me.btnClose.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Location = New System.Drawing.Point(269, 10)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Size = New System.Drawing.Size(97, 30)
        Me.btnClose.TabIndex = 119
        Me.btnClose.Text = "&Close"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'frmPermanentlyDeleteLogs
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(378, 151)
        Me.Controls.Add(Me.pnlMain)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmPermanentlyDeletePPALogs"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Permanently Delete Pay Per Activity Logs"
        Me.pnlMain.ResumeLayout(False)
        Me.gbGroupList.ResumeLayout(False)
        Me.objFooter.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents pnlMain As System.Windows.Forms.Panel
    Friend WithEvents objFooter As eZee.Common.eZeeFooter
    Friend WithEvents btnDelete As eZee.Common.eZeeLightButton
    Friend WithEvents btnClose As eZee.Common.eZeeLightButton
    Friend WithEvents gbGroupList As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents dtpFromDate As System.Windows.Forms.DateTimePicker
    Friend WithEvents lblFromDate As System.Windows.Forms.Label
    Friend WithEvents dtpTodate As System.Windows.Forms.DateTimePicker
    Friend WithEvents lblTodate As System.Windows.Forms.Label
    Friend WithEvents cboRemoveFrom As System.Windows.Forms.ComboBox
    Friend WithEvents lblRemoveFrom As System.Windows.Forms.Label
End Class
