﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmATLogView_New
    Inherits eZee.Common.eZeeForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmATLogView_New))
        Me.pnlMain = New System.Windows.Forms.Panel
        Me.objFooter = New eZee.Common.eZeeFooter
        Me.lvData = New eZee.Common.eZeeListView(Me.components)
        Me.btnExport = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnClose = New eZee.Common.eZeeLightButton(Me.components)
        Me.objSPC1 = New System.Windows.Forms.SplitContainer
        Me.gbGroupList = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.cboViewBy = New System.Windows.Forms.ComboBox
        Me.lblViewBy = New System.Windows.Forms.Label
        Me.lblModuleName = New System.Windows.Forms.Label
        Me.lblMachine = New System.Windows.Forms.Label
        Me.txtModuleName = New System.Windows.Forms.TextBox
        Me.lblIPAddress = New System.Windows.Forms.Label
        Me.txtMachine = New System.Windows.Forms.TextBox
        Me.txtIPAddress = New System.Windows.Forms.TextBox
        Me.btnReset = New eZee.Common.eZeeGradientButton
        Me.btnSearch = New eZee.Common.eZeeGradientButton
        Me.lblTodate = New System.Windows.Forms.Label
        Me.dtpTodate = New System.Windows.Forms.DateTimePicker
        Me.dtpFromDate = New System.Windows.Forms.DateTimePicker
        Me.lblFromDate = New System.Windows.Forms.Label
        Me.objbtnSearch = New eZee.Common.eZeeGradientButton
        Me.objlblCaption = New System.Windows.Forms.Label
        Me.cboFilter = New System.Windows.Forms.ComboBox
        Me.flpPanel1 = New System.Windows.Forms.FlowLayoutPanel
        Me.gbList = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.lvSummary = New eZee.Common.eZeeListView(Me.components)
        Me.gbDetails = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.cboEventType = New System.Windows.Forms.ComboBox
        Me.lblEventType = New System.Windows.Forms.Label
        Me.cboChildData = New System.Windows.Forms.ComboBox
        Me.tabcData = New System.Windows.Forms.TabControl
        Me.tabpMasterDetail = New System.Windows.Forms.TabPage
        Me.objspc2 = New System.Windows.Forms.SplitContainer
        Me.txtSearchGrid = New eZee.TextBox.AlphanumericTextBox
        Me.dgvAuditData = New System.Windows.Forms.DataGridView
        Me.tabpChildDetail = New System.Windows.Forms.TabPage
        Me.objspc3 = New System.Windows.Forms.SplitContainer
        Me.txtSearchList = New eZee.TextBox.AlphanumericTextBox
        Me.dgvChildDetails = New Aruti.Data.GroupByGrid
        Me.objdgcolhCheck = New System.Windows.Forms.DataGridViewCheckBoxColumn
        Me.dgcolhRefNo = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhChargeDate = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhPersonInvloved = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhCount = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhCountDesc = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhOffcenceCat = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhOffence = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhCountStatus = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhLastResolutionDesc = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhDisciPenalty = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhFinalStatus = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhChargeDesc = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhCategory = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhProceedingMasterId = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhDisciplineFileId = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhEmployeeId = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhProceedingTranId = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhIsSubmitForApproval = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhIsApproved = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewCheckBoxColumn1 = New System.Windows.Forms.DataGridViewCheckBoxColumn
        Me.DataGridViewTextBoxColumn1 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn2 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn3 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn4 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn5 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn6 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn7 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn8 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn9 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn10 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn11 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn12 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn13 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn14 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn15 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn16 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn17 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn18 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn19 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.pnlMain.SuspendLayout()
        Me.objFooter.SuspendLayout()
        Me.objSPC1.Panel1.SuspendLayout()
        Me.objSPC1.Panel2.SuspendLayout()
        Me.objSPC1.SuspendLayout()
        Me.gbGroupList.SuspendLayout()
        Me.flpPanel1.SuspendLayout()
        Me.gbList.SuspendLayout()
        Me.gbDetails.SuspendLayout()
        Me.tabcData.SuspendLayout()
        Me.tabpMasterDetail.SuspendLayout()
        Me.objspc2.Panel1.SuspendLayout()
        Me.objspc2.Panel2.SuspendLayout()
        Me.objspc2.SuspendLayout()
        CType(Me.dgvAuditData, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.tabpChildDetail.SuspendLayout()
        Me.objspc3.Panel1.SuspendLayout()
        Me.objspc3.Panel2.SuspendLayout()
        Me.objspc3.SuspendLayout()
        CType(Me.dgvChildDetails, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'pnlMain
        '
        Me.pnlMain.Controls.Add(Me.objFooter)
        Me.pnlMain.Controls.Add(Me.objSPC1)
        Me.pnlMain.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pnlMain.Location = New System.Drawing.Point(0, 0)
        Me.pnlMain.Name = "pnlMain"
        Me.pnlMain.Size = New System.Drawing.Size(1018, 622)
        Me.pnlMain.TabIndex = 0
        '
        'objFooter
        '
        Me.objFooter.BorderColor = System.Drawing.Color.Silver
        Me.objFooter.Controls.Add(Me.lvData)
        Me.objFooter.Controls.Add(Me.btnExport)
        Me.objFooter.Controls.Add(Me.btnClose)
        Me.objFooter.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.objFooter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objFooter.GradiantStyle = eZee.Common.eZeeFooter.GradientStyle.Up
        Me.objFooter.GradientColor1 = System.Drawing.SystemColors.Control
        Me.objFooter.GradientColor2 = System.Drawing.SystemColors.Control
        Me.objFooter.Location = New System.Drawing.Point(0, 567)
        Me.objFooter.Name = "objFooter"
        Me.objFooter.Size = New System.Drawing.Size(1018, 55)
        Me.objFooter.TabIndex = 9
        '
        'lvData
        '
        Me.lvData.BackColorOnChecked = False
        Me.lvData.ColumnHeaders = Nothing
        Me.lvData.CompulsoryColumns = ""
        Me.lvData.FullRowSelect = True
        Me.lvData.GridLines = True
        Me.lvData.GroupingColumn = Nothing
        Me.lvData.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable
        Me.lvData.HideSelection = False
        Me.lvData.Location = New System.Drawing.Point(307, 6)
        Me.lvData.MinColumnWidth = 50
        Me.lvData.MultiSelect = False
        Me.lvData.Name = "lvData"
        Me.lvData.OptionalColumns = ""
        Me.lvData.ShowMoreItem = False
        Me.lvData.ShowSaveItem = False
        Me.lvData.ShowSelectAll = True
        Me.lvData.ShowSizeAllColumnsToFit = True
        Me.lvData.Size = New System.Drawing.Size(43, 40)
        Me.lvData.Sortable = True
        Me.lvData.TabIndex = 0
        Me.lvData.UseCompatibleStateImageBehavior = False
        Me.lvData.View = System.Windows.Forms.View.Details
        Me.lvData.Visible = False
        '
        'btnExport
        '
        Me.btnExport.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnExport.BackColor = System.Drawing.Color.White
        Me.btnExport.BackgroundImage = CType(resources.GetObject("btnExport.BackgroundImage"), System.Drawing.Image)
        Me.btnExport.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnExport.BorderColor = System.Drawing.Color.Empty
        Me.btnExport.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnExport.FlatAppearance.BorderSize = 0
        Me.btnExport.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnExport.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnExport.ForeColor = System.Drawing.Color.Black
        Me.btnExport.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnExport.GradientForeColor = System.Drawing.Color.Black
        Me.btnExport.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnExport.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnExport.Location = New System.Drawing.Point(806, 13)
        Me.btnExport.Name = "btnExport"
        Me.btnExport.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnExport.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnExport.Size = New System.Drawing.Size(97, 30)
        Me.btnExport.TabIndex = 120
        Me.btnExport.Text = "&Export"
        Me.btnExport.UseVisualStyleBackColor = True
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnClose.BackColor = System.Drawing.Color.White
        Me.btnClose.BackgroundImage = CType(resources.GetObject("btnClose.BackgroundImage"), System.Drawing.Image)
        Me.btnClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnClose.BorderColor = System.Drawing.Color.Empty
        Me.btnClose.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnClose.FlatAppearance.BorderSize = 0
        Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClose.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.ForeColor = System.Drawing.Color.Black
        Me.btnClose.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnClose.GradientForeColor = System.Drawing.Color.Black
        Me.btnClose.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Location = New System.Drawing.Point(909, 13)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Size = New System.Drawing.Size(97, 30)
        Me.btnClose.TabIndex = 119
        Me.btnClose.Text = "&Close"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'objSPC1
        '
        Me.objSPC1.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.objSPC1.IsSplitterFixed = True
        Me.objSPC1.Location = New System.Drawing.Point(0, 0)
        Me.objSPC1.Margin = New System.Windows.Forms.Padding(0)
        Me.objSPC1.Name = "objSPC1"
        Me.objSPC1.Orientation = System.Windows.Forms.Orientation.Horizontal
        '
        'objSPC1.Panel1
        '
        Me.objSPC1.Panel1.Controls.Add(Me.gbGroupList)
        '
        'objSPC1.Panel2
        '
        Me.objSPC1.Panel2.Controls.Add(Me.flpPanel1)
        Me.objSPC1.Size = New System.Drawing.Size(1018, 566)
        Me.objSPC1.SplitterDistance = 89
        Me.objSPC1.SplitterWidth = 1
        Me.objSPC1.TabIndex = 0
        '
        'gbGroupList
        '
        Me.gbGroupList.BorderColor = System.Drawing.Color.Black
        Me.gbGroupList.Checked = False
        Me.gbGroupList.CollapseAllExceptThis = False
        Me.gbGroupList.CollapsedHoverImage = Nothing
        Me.gbGroupList.CollapsedNormalImage = Nothing
        Me.gbGroupList.CollapsedPressedImage = Nothing
        Me.gbGroupList.CollapseOnLoad = False
        Me.gbGroupList.Controls.Add(Me.cboViewBy)
        Me.gbGroupList.Controls.Add(Me.lblViewBy)
        Me.gbGroupList.Controls.Add(Me.lblModuleName)
        Me.gbGroupList.Controls.Add(Me.lblMachine)
        Me.gbGroupList.Controls.Add(Me.txtModuleName)
        Me.gbGroupList.Controls.Add(Me.lblIPAddress)
        Me.gbGroupList.Controls.Add(Me.txtMachine)
        Me.gbGroupList.Controls.Add(Me.txtIPAddress)
        Me.gbGroupList.Controls.Add(Me.btnReset)
        Me.gbGroupList.Controls.Add(Me.btnSearch)
        Me.gbGroupList.Controls.Add(Me.lblTodate)
        Me.gbGroupList.Controls.Add(Me.dtpTodate)
        Me.gbGroupList.Controls.Add(Me.dtpFromDate)
        Me.gbGroupList.Controls.Add(Me.lblFromDate)
        Me.gbGroupList.Controls.Add(Me.objbtnSearch)
        Me.gbGroupList.Controls.Add(Me.objlblCaption)
        Me.gbGroupList.Controls.Add(Me.cboFilter)
        Me.gbGroupList.Dock = System.Windows.Forms.DockStyle.Fill
        Me.gbGroupList.ExpandedHoverImage = Nothing
        Me.gbGroupList.ExpandedNormalImage = Nothing
        Me.gbGroupList.ExpandedPressedImage = Nothing
        Me.gbGroupList.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbGroupList.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbGroupList.HeaderHeight = 25
        Me.gbGroupList.HeaderMessage = ""
        Me.gbGroupList.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbGroupList.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbGroupList.HeightOnCollapse = 0
        Me.gbGroupList.LeftTextSpace = 0
        Me.gbGroupList.Location = New System.Drawing.Point(0, 0)
        Me.gbGroupList.Name = "gbGroupList"
        Me.gbGroupList.OpenHeight = 63
        Me.gbGroupList.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbGroupList.ShowBorder = True
        Me.gbGroupList.ShowCheckBox = False
        Me.gbGroupList.ShowCollapseButton = False
        Me.gbGroupList.ShowDefaultBorderColor = True
        Me.gbGroupList.ShowDownButton = False
        Me.gbGroupList.ShowHeader = True
        Me.gbGroupList.Size = New System.Drawing.Size(1018, 89)
        Me.gbGroupList.TabIndex = 8
        Me.gbGroupList.Temp = 0
        Me.gbGroupList.Text = "Filter Criteria"
        Me.gbGroupList.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboViewBy
        '
        Me.cboViewBy.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboViewBy.DropDownWidth = 200
        Me.cboViewBy.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboViewBy.FormattingEnabled = True
        Me.cboViewBy.Location = New System.Drawing.Point(108, 34)
        Me.cboViewBy.Name = "cboViewBy"
        Me.cboViewBy.Size = New System.Drawing.Size(193, 21)
        Me.cboViewBy.TabIndex = 237
        '
        'lblViewBy
        '
        Me.lblViewBy.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblViewBy.Location = New System.Drawing.Point(8, 36)
        Me.lblViewBy.Name = "lblViewBy"
        Me.lblViewBy.Size = New System.Drawing.Size(94, 16)
        Me.lblViewBy.TabIndex = 236
        Me.lblViewBy.Text = "Operation Mode"
        Me.lblViewBy.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblModuleName
        '
        Me.lblModuleName.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblModuleName.Location = New System.Drawing.Point(334, 64)
        Me.lblModuleName.Name = "lblModuleName"
        Me.lblModuleName.Size = New System.Drawing.Size(85, 15)
        Me.lblModuleName.TabIndex = 224
        Me.lblModuleName.Text = "Module/Screen"
        '
        'lblMachine
        '
        Me.lblMachine.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblMachine.Location = New System.Drawing.Point(682, 64)
        Me.lblMachine.Name = "lblMachine"
        Me.lblMachine.Size = New System.Drawing.Size(85, 15)
        Me.lblMachine.TabIndex = 225
        Me.lblMachine.Text = "Machine Name"
        '
        'txtModuleName
        '
        Me.txtModuleName.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtModuleName.Location = New System.Drawing.Point(425, 61)
        Me.txtModuleName.Name = "txtModuleName"
        Me.txtModuleName.Size = New System.Drawing.Size(251, 21)
        Me.txtModuleName.TabIndex = 222
        '
        'lblIPAddress
        '
        Me.lblIPAddress.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblIPAddress.Location = New System.Drawing.Point(682, 37)
        Me.lblIPAddress.Name = "lblIPAddress"
        Me.lblIPAddress.Size = New System.Drawing.Size(85, 15)
        Me.lblIPAddress.TabIndex = 223
        Me.lblIPAddress.Text = "IP Address"
        '
        'txtMachine
        '
        Me.txtMachine.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtMachine.Location = New System.Drawing.Point(773, 61)
        Me.txtMachine.Name = "txtMachine"
        Me.txtMachine.Size = New System.Drawing.Size(233, 21)
        Me.txtMachine.TabIndex = 222
        '
        'txtIPAddress
        '
        Me.txtIPAddress.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtIPAddress.Location = New System.Drawing.Point(773, 34)
        Me.txtIPAddress.Name = "txtIPAddress"
        Me.txtIPAddress.Size = New System.Drawing.Size(233, 21)
        Me.txtIPAddress.TabIndex = 222
        '
        'btnReset
        '
        Me.btnReset.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnReset.BackColor = System.Drawing.Color.Transparent
        Me.btnReset.BackColor1 = System.Drawing.Color.Transparent
        Me.btnReset.BackColor2 = System.Drawing.Color.Transparent
        Me.btnReset.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.btnReset.BorderSelected = False
        Me.btnReset.DialogResult = System.Windows.Forms.DialogResult.None
        Me.btnReset.Image = Global.Aruti.Data.My.Resources.Resources.reset_20
        Me.btnReset.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.btnReset.Location = New System.Drawing.Point(994, 2)
        Me.btnReset.Name = "btnReset"
        Me.btnReset.Size = New System.Drawing.Size(21, 21)
        Me.btnReset.TabIndex = 219
        '
        'btnSearch
        '
        Me.btnSearch.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnSearch.BackColor = System.Drawing.Color.Transparent
        Me.btnSearch.BackColor1 = System.Drawing.Color.Transparent
        Me.btnSearch.BackColor2 = System.Drawing.Color.Transparent
        Me.btnSearch.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.btnSearch.BorderSelected = False
        Me.btnSearch.DialogResult = System.Windows.Forms.DialogResult.None
        Me.btnSearch.Image = Global.Aruti.Data.My.Resources.Resources.search_20
        Me.btnSearch.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.btnSearch.Location = New System.Drawing.Point(970, 2)
        Me.btnSearch.Name = "btnSearch"
        Me.btnSearch.Size = New System.Drawing.Size(21, 21)
        Me.btnSearch.TabIndex = 220
        '
        'lblTodate
        '
        Me.lblTodate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTodate.Location = New System.Drawing.Point(535, 37)
        Me.lblTodate.Name = "lblTodate"
        Me.lblTodate.Size = New System.Drawing.Size(31, 15)
        Me.lblTodate.TabIndex = 210
        Me.lblTodate.Text = "To"
        '
        'dtpTodate
        '
        Me.dtpTodate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpTodate.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpTodate.Location = New System.Drawing.Point(572, 34)
        Me.dtpTodate.Name = "dtpTodate"
        Me.dtpTodate.Size = New System.Drawing.Size(104, 21)
        Me.dtpTodate.TabIndex = 209
        '
        'dtpFromDate
        '
        Me.dtpFromDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpFromDate.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpFromDate.Location = New System.Drawing.Point(425, 34)
        Me.dtpFromDate.Name = "dtpFromDate"
        Me.dtpFromDate.Size = New System.Drawing.Size(104, 21)
        Me.dtpFromDate.TabIndex = 207
        '
        'lblFromDate
        '
        Me.lblFromDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblFromDate.Location = New System.Drawing.Point(334, 37)
        Me.lblFromDate.Name = "lblFromDate"
        Me.lblFromDate.Size = New System.Drawing.Size(85, 15)
        Me.lblFromDate.TabIndex = 208
        Me.lblFromDate.Text = "From Date"
        '
        'objbtnSearch
        '
        Me.objbtnSearch.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearch.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearch.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearch.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearch.BorderSelected = False
        Me.objbtnSearch.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearch.Image = Global.Aruti.Data.My.Resources.Resources.Mini_Search
        Me.objbtnSearch.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearch.Location = New System.Drawing.Point(307, 61)
        Me.objbtnSearch.Name = "objbtnSearch"
        Me.objbtnSearch.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearch.TabIndex = 87
        '
        'objlblCaption
        '
        Me.objlblCaption.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objlblCaption.Location = New System.Drawing.Point(9, 63)
        Me.objlblCaption.Name = "objlblCaption"
        Me.objlblCaption.Size = New System.Drawing.Size(94, 16)
        Me.objlblCaption.TabIndex = 1
        Me.objlblCaption.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboFilter
        '
        Me.cboFilter.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboFilter.DropDownWidth = 450
        Me.cboFilter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboFilter.FormattingEnabled = True
        Me.cboFilter.Location = New System.Drawing.Point(108, 61)
        Me.cboFilter.Name = "cboFilter"
        Me.cboFilter.Size = New System.Drawing.Size(193, 21)
        Me.cboFilter.TabIndex = 2
        '
        'flpPanel1
        '
        Me.flpPanel1.AutoScroll = True
        Me.flpPanel1.Controls.Add(Me.gbList)
        Me.flpPanel1.Controls.Add(Me.gbDetails)
        Me.flpPanel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.flpPanel1.Location = New System.Drawing.Point(0, 0)
        Me.flpPanel1.Margin = New System.Windows.Forms.Padding(0)
        Me.flpPanel1.Name = "flpPanel1"
        Me.flpPanel1.Size = New System.Drawing.Size(1018, 476)
        Me.flpPanel1.TabIndex = 226
        '
        'gbList
        '
        Me.gbList.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.gbList.BorderColor = System.Drawing.Color.Black
        Me.gbList.Checked = False
        Me.gbList.CollapseAllExceptThis = False
        Me.gbList.CollapsedHoverImage = CType(resources.GetObject("gbList.CollapsedHoverImage"), System.Drawing.Image)
        Me.gbList.CollapsedNormalImage = CType(resources.GetObject("gbList.CollapsedNormalImage"), System.Drawing.Image)
        Me.gbList.CollapsedPressedImage = CType(resources.GetObject("gbList.CollapsedPressedImage"), System.Drawing.Image)
        Me.gbList.CollapseOnLoad = False
        Me.gbList.Controls.Add(Me.lvSummary)
        Me.gbList.ExpandedHoverImage = CType(resources.GetObject("gbList.ExpandedHoverImage"), System.Drawing.Image)
        Me.gbList.ExpandedNormalImage = CType(resources.GetObject("gbList.ExpandedNormalImage"), System.Drawing.Image)
        Me.gbList.ExpandedPressedImage = CType(resources.GetObject("gbList.ExpandedPressedImage"), System.Drawing.Image)
        Me.gbList.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbList.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbList.HeaderHeight = 25
        Me.gbList.HeaderMessage = ""
        Me.gbList.HeaderMessageFont = New System.Drawing.Font("Microsoft Sans Serif", 8.25!)
        Me.gbList.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbList.HeightOnCollapse = 0
        Me.gbList.LeftTextSpace = 0
        Me.gbList.Location = New System.Drawing.Point(3, 3)
        Me.gbList.Name = "gbList"
        Me.gbList.OpenHeight = 290
        Me.gbList.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbList.ShowBorder = True
        Me.gbList.ShowCheckBox = False
        Me.gbList.ShowCollapseButton = True
        Me.gbList.ShowDefaultBorderColor = True
        Me.gbList.ShowDownButton = False
        Me.gbList.ShowHeader = True
        Me.gbList.Size = New System.Drawing.Size(994, 158)
        Me.gbList.TabIndex = 226
        Me.gbList.Temp = 0
        Me.gbList.Text = "Summary List"
        Me.gbList.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lvSummary
        '
        Me.lvSummary.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lvSummary.BackColorOnChecked = True
        Me.lvSummary.ColumnHeaders = Nothing
        Me.lvSummary.CompulsoryColumns = ""
        Me.lvSummary.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lvSummary.FullRowSelect = True
        Me.lvSummary.GridLines = True
        Me.lvSummary.GroupingColumn = Nothing
        Me.lvSummary.HideSelection = False
        Me.lvSummary.Location = New System.Drawing.Point(3, 26)
        Me.lvSummary.MinColumnWidth = 50
        Me.lvSummary.MultiSelect = False
        Me.lvSummary.Name = "lvSummary"
        Me.lvSummary.OptionalColumns = ""
        Me.lvSummary.ShowMoreItem = False
        Me.lvSummary.ShowSaveItem = False
        Me.lvSummary.ShowSelectAll = True
        Me.lvSummary.ShowSizeAllColumnsToFit = True
        Me.lvSummary.Size = New System.Drawing.Size(988, 129)
        Me.lvSummary.Sortable = True
        Me.lvSummary.TabIndex = 225
        Me.lvSummary.UseCompatibleStateImageBehavior = False
        Me.lvSummary.View = System.Windows.Forms.View.Details
        '
        'gbDetails
        '
        Me.gbDetails.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.gbDetails.BorderColor = System.Drawing.Color.Black
        Me.gbDetails.Checked = False
        Me.gbDetails.CollapseAllExceptThis = False
        Me.gbDetails.CollapsedHoverImage = CType(resources.GetObject("gbDetails.CollapsedHoverImage"), System.Drawing.Image)
        Me.gbDetails.CollapsedNormalImage = CType(resources.GetObject("gbDetails.CollapsedNormalImage"), System.Drawing.Image)
        Me.gbDetails.CollapsedPressedImage = CType(resources.GetObject("gbDetails.CollapsedPressedImage"), System.Drawing.Image)
        Me.gbDetails.CollapseOnLoad = False
        Me.gbDetails.Controls.Add(Me.cboEventType)
        Me.gbDetails.Controls.Add(Me.lblEventType)
        Me.gbDetails.Controls.Add(Me.cboChildData)
        Me.gbDetails.Controls.Add(Me.tabcData)
        Me.gbDetails.ExpandedHoverImage = CType(resources.GetObject("gbDetails.ExpandedHoverImage"), System.Drawing.Image)
        Me.gbDetails.ExpandedNormalImage = CType(resources.GetObject("gbDetails.ExpandedNormalImage"), System.Drawing.Image)
        Me.gbDetails.ExpandedPressedImage = CType(resources.GetObject("gbDetails.ExpandedPressedImage"), System.Drawing.Image)
        Me.gbDetails.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbDetails.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbDetails.HeaderHeight = 25
        Me.gbDetails.HeaderMessage = ""
        Me.gbDetails.HeaderMessageFont = New System.Drawing.Font("Microsoft Sans Serif", 8.25!)
        Me.gbDetails.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbDetails.HeightOnCollapse = 0
        Me.gbDetails.LeftTextSpace = 0
        Me.gbDetails.Location = New System.Drawing.Point(3, 167)
        Me.gbDetails.Name = "gbDetails"
        Me.gbDetails.OpenHeight = 470
        Me.gbDetails.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbDetails.ShowBorder = True
        Me.gbDetails.ShowCheckBox = False
        Me.gbDetails.ShowCollapseButton = True
        Me.gbDetails.ShowDefaultBorderColor = True
        Me.gbDetails.ShowDownButton = False
        Me.gbDetails.ShowHeader = True
        Me.gbDetails.Size = New System.Drawing.Size(994, 194)
        Me.gbDetails.TabIndex = 226
        Me.gbDetails.Temp = 0
        Me.gbDetails.Text = "Detail List"
        Me.gbDetails.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboEventType
        '
        Me.cboEventType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboEventType.DropDownWidth = 200
        Me.cboEventType.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboEventType.FormattingEnabled = True
        Me.cboEventType.Location = New System.Drawing.Point(855, 28)
        Me.cboEventType.Name = "cboEventType"
        Me.cboEventType.Size = New System.Drawing.Size(130, 21)
        Me.cboEventType.TabIndex = 92
        '
        'lblEventType
        '
        Me.lblEventType.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEventType.Location = New System.Drawing.Point(783, 31)
        Me.lblEventType.Name = "lblEventType"
        Me.lblEventType.Size = New System.Drawing.Size(66, 15)
        Me.lblEventType.TabIndex = 91
        Me.lblEventType.Text = "Event Type"
        Me.lblEventType.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboChildData
        '
        Me.cboChildData.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.cboChildData.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboChildData.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboChildData.FormattingEnabled = True
        Me.cboChildData.Location = New System.Drawing.Point(542, 28)
        Me.cboChildData.Name = "cboChildData"
        Me.cboChildData.Size = New System.Drawing.Size(235, 21)
        Me.cboChildData.TabIndex = 11
        '
        'tabcData
        '
        Me.tabcData.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.tabcData.Controls.Add(Me.tabpMasterDetail)
        Me.tabcData.Controls.Add(Me.tabpChildDetail)
        Me.tabcData.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tabcData.Location = New System.Drawing.Point(3, 34)
        Me.tabcData.Margin = New System.Windows.Forms.Padding(0)
        Me.tabcData.Name = "tabcData"
        Me.tabcData.Padding = New System.Drawing.Point(0, 0)
        Me.tabcData.SelectedIndex = 0
        Me.tabcData.Size = New System.Drawing.Size(986, 155)
        Me.tabcData.TabIndex = 9
        '
        'tabpMasterDetail
        '
        Me.tabpMasterDetail.Controls.Add(Me.objspc2)
        Me.tabpMasterDetail.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tabpMasterDetail.Location = New System.Drawing.Point(4, 22)
        Me.tabpMasterDetail.Name = "tabpMasterDetail"
        Me.tabpMasterDetail.Size = New System.Drawing.Size(978, 129)
        Me.tabpMasterDetail.TabIndex = 0
        Me.tabpMasterDetail.Text = "Master Detail"
        Me.tabpMasterDetail.UseVisualStyleBackColor = True
        '
        'objspc2
        '
        Me.objspc2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.objspc2.FixedPanel = System.Windows.Forms.FixedPanel.Panel1
        Me.objspc2.IsSplitterFixed = True
        Me.objspc2.Location = New System.Drawing.Point(0, 0)
        Me.objspc2.Name = "objspc2"
        Me.objspc2.Orientation = System.Windows.Forms.Orientation.Horizontal
        '
        'objspc2.Panel1
        '
        Me.objspc2.Panel1.Controls.Add(Me.txtSearchGrid)
        '
        'objspc2.Panel2
        '
        Me.objspc2.Panel2.Controls.Add(Me.dgvAuditData)
        Me.objspc2.Size = New System.Drawing.Size(978, 129)
        Me.objspc2.SplitterDistance = 25
        Me.objspc2.SplitterWidth = 1
        Me.objspc2.TabIndex = 108
        '
        'txtSearchGrid
        '
        Me.txtSearchGrid.BackColor = System.Drawing.SystemColors.Window
        Me.txtSearchGrid.Dock = System.Windows.Forms.DockStyle.Fill
        Me.txtSearchGrid.Flags = 0
        Me.txtSearchGrid.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92), Global.Microsoft.VisualBasic.ChrW(91), Global.Microsoft.VisualBasic.ChrW(93)}
        Me.txtSearchGrid.Location = New System.Drawing.Point(0, 0)
        Me.txtSearchGrid.Name = "txtSearchGrid"
        Me.txtSearchGrid.Size = New System.Drawing.Size(978, 21)
        Me.txtSearchGrid.TabIndex = 107
        '
        'dgvAuditData
        '
        Me.dgvAuditData.AllowUserToAddRows = False
        Me.dgvAuditData.AllowUserToDeleteRows = False
        Me.dgvAuditData.AllowUserToResizeRows = False
        Me.dgvAuditData.BackgroundColor = System.Drawing.Color.White
        Me.dgvAuditData.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.dgvAuditData.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.RaisedHorizontal
        Me.dgvAuditData.ColumnHeadersHeight = 22
        Me.dgvAuditData.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing
        Me.dgvAuditData.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dgvAuditData.Location = New System.Drawing.Point(0, 0)
        Me.dgvAuditData.Name = "dgvAuditData"
        Me.dgvAuditData.ReadOnly = True
        Me.dgvAuditData.RowHeadersVisible = False
        Me.dgvAuditData.RowHeadersWidth = 5
        Me.dgvAuditData.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing
        Me.dgvAuditData.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvAuditData.Size = New System.Drawing.Size(978, 103)
        Me.dgvAuditData.TabIndex = 9
        '
        'tabpChildDetail
        '
        Me.tabpChildDetail.Controls.Add(Me.objspc3)
        Me.tabpChildDetail.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tabpChildDetail.Location = New System.Drawing.Point(4, 22)
        Me.tabpChildDetail.Margin = New System.Windows.Forms.Padding(0)
        Me.tabpChildDetail.Name = "tabpChildDetail"
        Me.tabpChildDetail.Size = New System.Drawing.Size(978, 129)
        Me.tabpChildDetail.TabIndex = 1
        Me.tabpChildDetail.UseVisualStyleBackColor = True
        '
        'objspc3
        '
        Me.objspc3.Dock = System.Windows.Forms.DockStyle.Fill
        Me.objspc3.FixedPanel = System.Windows.Forms.FixedPanel.Panel1
        Me.objspc3.Location = New System.Drawing.Point(0, 0)
        Me.objspc3.Name = "objspc3"
        Me.objspc3.Orientation = System.Windows.Forms.Orientation.Horizontal
        '
        'objspc3.Panel1
        '
        Me.objspc3.Panel1.Controls.Add(Me.txtSearchList)
        '
        'objspc3.Panel2
        '
        Me.objspc3.Panel2.Controls.Add(Me.dgvChildDetails)
        Me.objspc3.Size = New System.Drawing.Size(978, 129)
        Me.objspc3.SplitterDistance = 25
        Me.objspc3.SplitterWidth = 1
        Me.objspc3.TabIndex = 1
        '
        'txtSearchList
        '
        Me.txtSearchList.BackColor = System.Drawing.SystemColors.Window
        Me.txtSearchList.Dock = System.Windows.Forms.DockStyle.Fill
        Me.txtSearchList.Flags = 0
        Me.txtSearchList.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92), Global.Microsoft.VisualBasic.ChrW(91), Global.Microsoft.VisualBasic.ChrW(93)}
        Me.txtSearchList.Location = New System.Drawing.Point(0, 0)
        Me.txtSearchList.Name = "txtSearchList"
        Me.txtSearchList.Size = New System.Drawing.Size(978, 21)
        Me.txtSearchList.TabIndex = 108
        '
        'dgvChildDetails
        '
        Me.dgvChildDetails.AllowUserToAddRows = False
        Me.dgvChildDetails.AllowUserToDeleteRows = False
        Me.dgvChildDetails.AllowUserToResizeRows = False
        Me.dgvChildDetails.BackgroundColor = System.Drawing.Color.WhiteSmoke
        Me.dgvChildDetails.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.dgvChildDetails.ColumnHeadersHeight = 30
        Me.dgvChildDetails.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing
        Me.dgvChildDetails.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dgvChildDetails.IgnoreFirstColumn = False
        Me.dgvChildDetails.Location = New System.Drawing.Point(0, 0)
        Me.dgvChildDetails.MultiSelect = False
        Me.dgvChildDetails.Name = "dgvChildDetails"
        Me.dgvChildDetails.RowHeadersVisible = False
        Me.dgvChildDetails.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvChildDetails.Size = New System.Drawing.Size(978, 103)
        Me.dgvChildDetails.TabIndex = 90
        '
        'objdgcolhCheck
        '
        Me.objdgcolhCheck.Frozen = True
        Me.objdgcolhCheck.HeaderText = ""
        Me.objdgcolhCheck.Name = "objdgcolhCheck"
        Me.objdgcolhCheck.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.objdgcolhCheck.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic
        Me.objdgcolhCheck.Visible = False
        Me.objdgcolhCheck.Width = 25
        '
        'dgcolhRefNo
        '
        Me.dgcolhRefNo.HeaderText = "Ref No."
        Me.dgcolhRefNo.Name = "dgcolhRefNo"
        Me.dgcolhRefNo.ReadOnly = True
        Me.dgcolhRefNo.Width = 75
        '
        'dgcolhChargeDate
        '
        Me.dgcolhChargeDate.HeaderText = "Charge Date"
        Me.dgcolhChargeDate.Name = "dgcolhChargeDate"
        Me.dgcolhChargeDate.ReadOnly = True
        '
        'dgcolhPersonInvloved
        '
        Me.dgcolhPersonInvloved.HeaderText = "Person Invloved"
        Me.dgcolhPersonInvloved.Name = "dgcolhPersonInvloved"
        Me.dgcolhPersonInvloved.ReadOnly = True
        Me.dgcolhPersonInvloved.Width = 125
        '
        'dgcolhCount
        '
        Me.dgcolhCount.HeaderText = "Count"
        Me.dgcolhCount.Name = "dgcolhCount"
        Me.dgcolhCount.ReadOnly = True
        Me.dgcolhCount.Width = 75
        '
        'dgcolhCountDesc
        '
        Me.dgcolhCountDesc.HeaderText = "Incident"
        Me.dgcolhCountDesc.Name = "dgcolhCountDesc"
        Me.dgcolhCountDesc.ReadOnly = True
        Me.dgcolhCountDesc.Width = 150
        '
        'dgcolhOffcenceCat
        '
        Me.dgcolhOffcenceCat.HeaderText = "Offence Category"
        Me.dgcolhOffcenceCat.Name = "dgcolhOffcenceCat"
        Me.dgcolhOffcenceCat.ReadOnly = True
        Me.dgcolhOffcenceCat.Width = 140
        '
        'dgcolhOffence
        '
        Me.dgcolhOffence.HeaderText = "Offence"
        Me.dgcolhOffence.Name = "dgcolhOffence"
        Me.dgcolhOffence.ReadOnly = True
        Me.dgcolhOffence.Width = 130
        '
        'dgcolhCountStatus
        '
        Me.dgcolhCountStatus.HeaderText = "Count Status"
        Me.dgcolhCountStatus.Name = "dgcolhCountStatus"
        Me.dgcolhCountStatus.ReadOnly = True
        '
        'dgcolhLastResolutionDesc
        '
        Me.dgcolhLastResolutionDesc.HeaderText = "Last Resolution"
        Me.dgcolhLastResolutionDesc.Name = "dgcolhLastResolutionDesc"
        Me.dgcolhLastResolutionDesc.ReadOnly = True
        Me.dgcolhLastResolutionDesc.Width = 125
        '
        'dgcolhDisciPenalty
        '
        Me.dgcolhDisciPenalty.HeaderText = "Disciplinary Penalty"
        Me.dgcolhDisciPenalty.Name = "dgcolhDisciPenalty"
        Me.dgcolhDisciPenalty.ReadOnly = True
        Me.dgcolhDisciPenalty.Width = 125
        '
        'dgcolhFinalStatus
        '
        Me.dgcolhFinalStatus.HeaderText = "Status"
        Me.dgcolhFinalStatus.Name = "dgcolhFinalStatus"
        Me.dgcolhFinalStatus.ReadOnly = True
        Me.dgcolhFinalStatus.Width = 85
        '
        'dgcolhChargeDesc
        '
        Me.dgcolhChargeDesc.HeaderText = "Charge Description"
        Me.dgcolhChargeDesc.Name = "dgcolhChargeDesc"
        Me.dgcolhChargeDesc.Visible = False
        '
        'dgcolhCategory
        '
        Me.dgcolhCategory.HeaderText = "Category"
        Me.dgcolhCategory.Name = "dgcolhCategory"
        Me.dgcolhCategory.ReadOnly = True
        '
        'objdgcolhProceedingMasterId
        '
        Me.objdgcolhProceedingMasterId.HeaderText = "objdgcolhProceedingMasterId"
        Me.objdgcolhProceedingMasterId.Name = "objdgcolhProceedingMasterId"
        Me.objdgcolhProceedingMasterId.ReadOnly = True
        Me.objdgcolhProceedingMasterId.Visible = False
        '
        'objdgcolhDisciplineFileId
        '
        Me.objdgcolhDisciplineFileId.HeaderText = "objdgcolhDisciplineFileId"
        Me.objdgcolhDisciplineFileId.Name = "objdgcolhDisciplineFileId"
        Me.objdgcolhDisciplineFileId.ReadOnly = True
        Me.objdgcolhDisciplineFileId.Visible = False
        '
        'objdgcolhEmployeeId
        '
        Me.objdgcolhEmployeeId.HeaderText = "objdgcolhEmployeeId"
        Me.objdgcolhEmployeeId.Name = "objdgcolhEmployeeId"
        Me.objdgcolhEmployeeId.ReadOnly = True
        Me.objdgcolhEmployeeId.Visible = False
        '
        'objdgcolhProceedingTranId
        '
        Me.objdgcolhProceedingTranId.HeaderText = "objdgcolhProceedingTranId"
        Me.objdgcolhProceedingTranId.Name = "objdgcolhProceedingTranId"
        Me.objdgcolhProceedingTranId.Visible = False
        '
        'objdgcolhIsSubmitForApproval
        '
        Me.objdgcolhIsSubmitForApproval.HeaderText = "objdgcolhIsSubmitForApproval"
        Me.objdgcolhIsSubmitForApproval.Name = "objdgcolhIsSubmitForApproval"
        Me.objdgcolhIsSubmitForApproval.Visible = False
        '
        'objdgcolhIsApproved
        '
        Me.objdgcolhIsApproved.HeaderText = "objdgcolhIsApproved"
        Me.objdgcolhIsApproved.Name = "objdgcolhIsApproved"
        Me.objdgcolhIsApproved.Visible = False
        '
        'DataGridViewCheckBoxColumn1
        '
        Me.DataGridViewCheckBoxColumn1.Frozen = True
        Me.DataGridViewCheckBoxColumn1.HeaderText = ""
        Me.DataGridViewCheckBoxColumn1.Name = "DataGridViewCheckBoxColumn1"
        Me.DataGridViewCheckBoxColumn1.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.DataGridViewCheckBoxColumn1.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic
        Me.DataGridViewCheckBoxColumn1.Visible = False
        Me.DataGridViewCheckBoxColumn1.Width = 25
        '
        'DataGridViewTextBoxColumn1
        '
        Me.DataGridViewTextBoxColumn1.HeaderText = "Ref No."
        Me.DataGridViewTextBoxColumn1.Name = "DataGridViewTextBoxColumn1"
        Me.DataGridViewTextBoxColumn1.ReadOnly = True
        Me.DataGridViewTextBoxColumn1.Width = 75
        '
        'DataGridViewTextBoxColumn2
        '
        Me.DataGridViewTextBoxColumn2.HeaderText = "Charge Date"
        Me.DataGridViewTextBoxColumn2.Name = "DataGridViewTextBoxColumn2"
        Me.DataGridViewTextBoxColumn2.ReadOnly = True
        '
        'DataGridViewTextBoxColumn3
        '
        Me.DataGridViewTextBoxColumn3.HeaderText = "Person Invloved"
        Me.DataGridViewTextBoxColumn3.Name = "DataGridViewTextBoxColumn3"
        Me.DataGridViewTextBoxColumn3.ReadOnly = True
        Me.DataGridViewTextBoxColumn3.Width = 125
        '
        'DataGridViewTextBoxColumn4
        '
        Me.DataGridViewTextBoxColumn4.HeaderText = "Count"
        Me.DataGridViewTextBoxColumn4.Name = "DataGridViewTextBoxColumn4"
        Me.DataGridViewTextBoxColumn4.ReadOnly = True
        Me.DataGridViewTextBoxColumn4.Width = 75
        '
        'DataGridViewTextBoxColumn5
        '
        Me.DataGridViewTextBoxColumn5.HeaderText = "Incident"
        Me.DataGridViewTextBoxColumn5.Name = "DataGridViewTextBoxColumn5"
        Me.DataGridViewTextBoxColumn5.ReadOnly = True
        Me.DataGridViewTextBoxColumn5.Width = 150
        '
        'DataGridViewTextBoxColumn6
        '
        Me.DataGridViewTextBoxColumn6.HeaderText = "Offence Category"
        Me.DataGridViewTextBoxColumn6.Name = "DataGridViewTextBoxColumn6"
        Me.DataGridViewTextBoxColumn6.ReadOnly = True
        Me.DataGridViewTextBoxColumn6.Width = 140
        '
        'DataGridViewTextBoxColumn7
        '
        Me.DataGridViewTextBoxColumn7.HeaderText = "Offence"
        Me.DataGridViewTextBoxColumn7.Name = "DataGridViewTextBoxColumn7"
        Me.DataGridViewTextBoxColumn7.ReadOnly = True
        Me.DataGridViewTextBoxColumn7.Width = 130
        '
        'DataGridViewTextBoxColumn8
        '
        Me.DataGridViewTextBoxColumn8.HeaderText = "Count Status"
        Me.DataGridViewTextBoxColumn8.Name = "DataGridViewTextBoxColumn8"
        Me.DataGridViewTextBoxColumn8.ReadOnly = True
        '
        'DataGridViewTextBoxColumn9
        '
        Me.DataGridViewTextBoxColumn9.HeaderText = "Last Resolution"
        Me.DataGridViewTextBoxColumn9.Name = "DataGridViewTextBoxColumn9"
        Me.DataGridViewTextBoxColumn9.ReadOnly = True
        Me.DataGridViewTextBoxColumn9.Width = 125
        '
        'DataGridViewTextBoxColumn10
        '
        Me.DataGridViewTextBoxColumn10.HeaderText = "Disciplinary Penalty"
        Me.DataGridViewTextBoxColumn10.Name = "DataGridViewTextBoxColumn10"
        Me.DataGridViewTextBoxColumn10.ReadOnly = True
        Me.DataGridViewTextBoxColumn10.Width = 125
        '
        'DataGridViewTextBoxColumn11
        '
        Me.DataGridViewTextBoxColumn11.HeaderText = "Status"
        Me.DataGridViewTextBoxColumn11.Name = "DataGridViewTextBoxColumn11"
        Me.DataGridViewTextBoxColumn11.ReadOnly = True
        Me.DataGridViewTextBoxColumn11.Width = 85
        '
        'DataGridViewTextBoxColumn12
        '
        Me.DataGridViewTextBoxColumn12.HeaderText = "Charge Description"
        Me.DataGridViewTextBoxColumn12.Name = "DataGridViewTextBoxColumn12"
        Me.DataGridViewTextBoxColumn12.Visible = False
        '
        'DataGridViewTextBoxColumn13
        '
        Me.DataGridViewTextBoxColumn13.HeaderText = "Category"
        Me.DataGridViewTextBoxColumn13.Name = "DataGridViewTextBoxColumn13"
        Me.DataGridViewTextBoxColumn13.ReadOnly = True
        '
        'DataGridViewTextBoxColumn14
        '
        Me.DataGridViewTextBoxColumn14.HeaderText = "objdgcolhProceedingMasterId"
        Me.DataGridViewTextBoxColumn14.Name = "DataGridViewTextBoxColumn14"
        Me.DataGridViewTextBoxColumn14.ReadOnly = True
        Me.DataGridViewTextBoxColumn14.Visible = False
        '
        'DataGridViewTextBoxColumn15
        '
        Me.DataGridViewTextBoxColumn15.HeaderText = "objdgcolhDisciplineFileId"
        Me.DataGridViewTextBoxColumn15.Name = "DataGridViewTextBoxColumn15"
        Me.DataGridViewTextBoxColumn15.ReadOnly = True
        Me.DataGridViewTextBoxColumn15.Visible = False
        '
        'DataGridViewTextBoxColumn16
        '
        Me.DataGridViewTextBoxColumn16.HeaderText = "objdgcolhEmployeeId"
        Me.DataGridViewTextBoxColumn16.Name = "DataGridViewTextBoxColumn16"
        Me.DataGridViewTextBoxColumn16.ReadOnly = True
        Me.DataGridViewTextBoxColumn16.Visible = False
        '
        'DataGridViewTextBoxColumn17
        '
        Me.DataGridViewTextBoxColumn17.HeaderText = "objdgcolhProceedingTranId"
        Me.DataGridViewTextBoxColumn17.Name = "DataGridViewTextBoxColumn17"
        Me.DataGridViewTextBoxColumn17.Visible = False
        '
        'DataGridViewTextBoxColumn18
        '
        Me.DataGridViewTextBoxColumn18.HeaderText = "objdgcolhIsSubmitForApproval"
        Me.DataGridViewTextBoxColumn18.Name = "DataGridViewTextBoxColumn18"
        Me.DataGridViewTextBoxColumn18.Visible = False
        '
        'DataGridViewTextBoxColumn19
        '
        Me.DataGridViewTextBoxColumn19.HeaderText = "objdgcolhIsApproved"
        Me.DataGridViewTextBoxColumn19.Name = "DataGridViewTextBoxColumn19"
        Me.DataGridViewTextBoxColumn19.Visible = False
        '
        'frmATLogView_New
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1018, 622)
        Me.Controls.Add(Me.pnlMain)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmATLogView_New"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Application Events Log"
        Me.pnlMain.ResumeLayout(False)
        Me.objFooter.ResumeLayout(False)
        Me.objSPC1.Panel1.ResumeLayout(False)
        Me.objSPC1.Panel2.ResumeLayout(False)
        Me.objSPC1.ResumeLayout(False)
        Me.gbGroupList.ResumeLayout(False)
        Me.gbGroupList.PerformLayout()
        Me.flpPanel1.ResumeLayout(False)
        Me.gbList.ResumeLayout(False)
        Me.gbDetails.ResumeLayout(False)
        Me.tabcData.ResumeLayout(False)
        Me.tabpMasterDetail.ResumeLayout(False)
        Me.objspc2.Panel1.ResumeLayout(False)
        Me.objspc2.Panel1.PerformLayout()
        Me.objspc2.Panel2.ResumeLayout(False)
        Me.objspc2.ResumeLayout(False)
        CType(Me.dgvAuditData, System.ComponentModel.ISupportInitialize).EndInit()
        Me.tabpChildDetail.ResumeLayout(False)
        Me.objspc3.Panel1.ResumeLayout(False)
        Me.objspc3.Panel1.PerformLayout()
        Me.objspc3.Panel2.ResumeLayout(False)
        Me.objspc3.ResumeLayout(False)
        CType(Me.dgvChildDetails, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents pnlMain As System.Windows.Forms.Panel
    Friend WithEvents objSPC1 As System.Windows.Forms.SplitContainer
    Friend WithEvents gbGroupList As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents btnReset As eZee.Common.eZeeGradientButton
    Friend WithEvents btnSearch As eZee.Common.eZeeGradientButton
    Friend WithEvents lblTodate As System.Windows.Forms.Label
    Friend WithEvents dtpTodate As System.Windows.Forms.DateTimePicker
    Friend WithEvents lblEventType As System.Windows.Forms.Label
    Friend WithEvents cboEventType As System.Windows.Forms.ComboBox
    Friend WithEvents dtpFromDate As System.Windows.Forms.DateTimePicker
    Friend WithEvents lblFromDate As System.Windows.Forms.Label
    Friend WithEvents objbtnSearch As eZee.Common.eZeeGradientButton
    Friend WithEvents objlblCaption As System.Windows.Forms.Label
    Friend WithEvents cboFilter As System.Windows.Forms.ComboBox
    Friend WithEvents txtMachine As System.Windows.Forms.TextBox
    Friend WithEvents txtIPAddress As System.Windows.Forms.TextBox
    Friend WithEvents lblModuleName As System.Windows.Forms.Label
    Friend WithEvents lblIPAddress As System.Windows.Forms.Label
    Friend WithEvents lvSummary As eZee.Common.eZeeListView
    Friend WithEvents lblMachine As System.Windows.Forms.Label
    Friend WithEvents txtModuleName As System.Windows.Forms.TextBox
    Friend WithEvents flpPanel1 As System.Windows.Forms.FlowLayoutPanel
    Friend WithEvents gbList As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents gbDetails As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents tabcData As System.Windows.Forms.TabControl
    Friend WithEvents tabpMasterDetail As System.Windows.Forms.TabPage
    Friend WithEvents dgvAuditData As System.Windows.Forms.DataGridView
    Friend WithEvents tabpChildDetail As System.Windows.Forms.TabPage
    Friend WithEvents lvData As eZee.Common.eZeeListView
    Friend WithEvents cboChildData As System.Windows.Forms.ComboBox
    Friend WithEvents objFooter As eZee.Common.eZeeFooter
    Friend WithEvents btnExport As eZee.Common.eZeeLightButton
    Friend WithEvents btnClose As eZee.Common.eZeeLightButton
    Friend WithEvents cboViewBy As System.Windows.Forms.ComboBox
    Friend WithEvents lblViewBy As System.Windows.Forms.Label
    Friend WithEvents txtSearchGrid As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents objspc2 As System.Windows.Forms.SplitContainer
    Friend WithEvents objspc3 As System.Windows.Forms.SplitContainer
    Friend WithEvents txtSearchList As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents objdgcolhCheck As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents dgcolhRefNo As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhChargeDate As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhPersonInvloved As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhCount As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhCountDesc As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhOffcenceCat As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhOffence As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhCountStatus As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhLastResolutionDesc As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhDisciPenalty As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhFinalStatus As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhChargeDesc As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhCategory As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhProceedingMasterId As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhDisciplineFileId As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhEmployeeId As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhProceedingTranId As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhIsSubmitForApproval As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhIsApproved As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewCheckBoxColumn1 As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn2 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn3 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn4 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn5 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn6 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn7 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn8 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn9 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn10 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn11 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn12 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn13 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn14 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn15 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn16 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn17 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn18 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn19 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgvChildDetails As Aruti.Data.GroupByGrid
End Class
