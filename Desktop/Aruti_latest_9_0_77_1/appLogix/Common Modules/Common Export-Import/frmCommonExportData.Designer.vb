﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmCommonExportData
    Inherits eZee.Common.eZeeForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmCommonExportData))
        Me.btnExport = New eZee.Common.eZeeLightButton(Me.components)
        Me.cboFileType = New System.Windows.Forms.ComboBox
        Me.lblFileType = New System.Windows.Forms.Label
        Me.lvMasters = New eZee.Common.eZeeListView(Me.components)
        Me.colhSelect = New System.Windows.Forms.ColumnHeader
        Me.colhMasterName = New System.Windows.Forms.ColumnHeader
        Me.EZeeHeader1 = New eZee.Common.eZeeHeader
        Me.EZeeFooter1 = New eZee.Common.eZeeFooter
        Me.btnclose = New eZee.Common.eZeeLightButton(Me.components)
        Me.chkSelectAll = New System.Windows.Forms.CheckBox
        Me.txtFilter = New eZee.TextBox.AlphanumericTextBox
        Me.lblFilter = New System.Windows.Forms.Label
        Me.EZeeFooter1.SuspendLayout()
        Me.SuspendLayout()
        '
        'btnExport
        '
        Me.btnExport.BackColor = System.Drawing.Color.White
        Me.btnExport.BackgroundImage = CType(resources.GetObject("btnExport.BackgroundImage"), System.Drawing.Image)
        Me.btnExport.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnExport.BorderColor = System.Drawing.Color.Empty
        Me.btnExport.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnExport.FlatAppearance.BorderSize = 0
        Me.btnExport.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnExport.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnExport.ForeColor = System.Drawing.Color.Black
        Me.btnExport.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnExport.GradientForeColor = System.Drawing.Color.Black
        Me.btnExport.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnExport.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnExport.Location = New System.Drawing.Point(184, 13)
        Me.btnExport.Name = "btnExport"
        Me.btnExport.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnExport.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnExport.Size = New System.Drawing.Size(90, 30)
        Me.btnExport.TabIndex = 0
        Me.btnExport.Text = "&Export"
        Me.btnExport.UseVisualStyleBackColor = True
        '
        'cboFileType
        '
        Me.cboFileType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboFileType.FormattingEnabled = True
        Me.cboFileType.Location = New System.Drawing.Point(97, 356)
        Me.cboFileType.Name = "cboFileType"
        Me.cboFileType.Size = New System.Drawing.Size(274, 21)
        Me.cboFileType.TabIndex = 3
        '
        'lblFileType
        '
        Me.lblFileType.Location = New System.Drawing.Point(12, 359)
        Me.lblFileType.Name = "lblFileType"
        Me.lblFileType.Size = New System.Drawing.Size(71, 15)
        Me.lblFileType.TabIndex = 7
        Me.lblFileType.Text = "File Type"
        Me.lblFileType.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lvMasters
        '
        Me.lvMasters.BackColorOnChecked = False
        Me.lvMasters.CheckBoxes = True
        Me.lvMasters.ColumnHeaders = Nothing
        Me.lvMasters.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.colhSelect, Me.colhMasterName})
        Me.lvMasters.CompulsoryColumns = ""
        Me.lvMasters.FullRowSelect = True
        Me.lvMasters.GridLines = True
        Me.lvMasters.GroupingColumn = Nothing
        Me.lvMasters.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable
        Me.lvMasters.HideSelection = False
        Me.lvMasters.Location = New System.Drawing.Point(12, 66)
        Me.lvMasters.MinColumnWidth = 50
        Me.lvMasters.MultiSelect = False
        Me.lvMasters.Name = "lvMasters"
        Me.lvMasters.OptionalColumns = ""
        Me.lvMasters.ShowMoreItem = False
        Me.lvMasters.ShowSaveItem = False
        Me.lvMasters.ShowSelectAll = True
        Me.lvMasters.ShowSizeAllColumnsToFit = True
        Me.lvMasters.Size = New System.Drawing.Size(359, 257)
        Me.lvMasters.Sortable = True
        Me.lvMasters.TabIndex = 1
        Me.lvMasters.UseCompatibleStateImageBehavior = False
        Me.lvMasters.View = System.Windows.Forms.View.Details
        '
        'colhSelect
        '
        Me.colhSelect.Tag = "colhSelect"
        Me.colhSelect.Text = ""
        Me.colhSelect.Width = 30
        '
        'colhMasterName
        '
        Me.colhMasterName.Tag = "colhMasterName"
        Me.colhMasterName.Text = "Master Name"
        Me.colhMasterName.Width = 325
        '
        'EZeeHeader1
        '
        Me.EZeeHeader1.BackColor = System.Drawing.SystemColors.Control
        Me.EZeeHeader1.BorderColor = System.Drawing.SystemColors.ControlDark
        Me.EZeeHeader1.DescriptionForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.EZeeHeader1.Dock = System.Windows.Forms.DockStyle.Top
        Me.EZeeHeader1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.EZeeHeader1.GradiantStyle = eZee.Common.eZeeHeader.GradientStyle.Central
        Me.EZeeHeader1.GradientColor1 = System.Drawing.SystemColors.Window
        Me.EZeeHeader1.GradientColor2 = System.Drawing.SystemColors.Control
        Me.EZeeHeader1.HeaderTextForeColor = System.Drawing.SystemColors.ControlText
        Me.EZeeHeader1.Icon = Nothing
        Me.EZeeHeader1.Location = New System.Drawing.Point(0, 0)
        Me.EZeeHeader1.Message = ""
        Me.EZeeHeader1.Name = "EZeeHeader1"
        Me.EZeeHeader1.Size = New System.Drawing.Size(385, 60)
        Me.EZeeHeader1.TabIndex = 10
        Me.EZeeHeader1.Title = "Master List Information"
        '
        'EZeeFooter1
        '
        Me.EZeeFooter1.BorderColor = System.Drawing.Color.Silver
        Me.EZeeFooter1.Controls.Add(Me.btnclose)
        Me.EZeeFooter1.Controls.Add(Me.btnExport)
        Me.EZeeFooter1.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.EZeeFooter1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.EZeeFooter1.GradiantStyle = eZee.Common.eZeeFooter.GradientStyle.Up
        Me.EZeeFooter1.GradientColor1 = System.Drawing.SystemColors.Control
        Me.EZeeFooter1.GradientColor2 = System.Drawing.SystemColors.Control
        Me.EZeeFooter1.Location = New System.Drawing.Point(0, 385)
        Me.EZeeFooter1.Name = "EZeeFooter1"
        Me.EZeeFooter1.Size = New System.Drawing.Size(385, 55)
        Me.EZeeFooter1.TabIndex = 11
        '
        'btnclose
        '
        Me.btnclose.BackColor = System.Drawing.Color.White
        Me.btnclose.BackgroundImage = CType(resources.GetObject("btnclose.BackgroundImage"), System.Drawing.Image)
        Me.btnclose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnclose.BorderColor = System.Drawing.Color.Empty
        Me.btnclose.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnclose.FlatAppearance.BorderSize = 0
        Me.btnclose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnclose.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnclose.ForeColor = System.Drawing.Color.Black
        Me.btnclose.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnclose.GradientForeColor = System.Drawing.Color.Black
        Me.btnclose.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnclose.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnclose.Location = New System.Drawing.Point(281, 13)
        Me.btnclose.Name = "btnclose"
        Me.btnclose.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnclose.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnclose.Size = New System.Drawing.Size(90, 30)
        Me.btnclose.TabIndex = 1
        Me.btnclose.Text = "&Close"
        Me.btnclose.UseVisualStyleBackColor = True
        '
        'chkSelectAll
        '
        Me.chkSelectAll.Location = New System.Drawing.Point(20, 70)
        Me.chkSelectAll.Name = "chkSelectAll"
        Me.chkSelectAll.Size = New System.Drawing.Size(14, 15)
        Me.chkSelectAll.TabIndex = 12
        Me.chkSelectAll.UseVisualStyleBackColor = True
        '
        'txtFilter
        '
        Me.txtFilter.Flags = 0
        Me.txtFilter.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtFilter.Location = New System.Drawing.Point(97, 330)
        Me.txtFilter.Name = "txtFilter"
        Me.txtFilter.Size = New System.Drawing.Size(274, 21)
        Me.txtFilter.TabIndex = 2
        '
        'lblFilter
        '
        Me.lblFilter.Location = New System.Drawing.Point(12, 333)
        Me.lblFilter.Name = "lblFilter"
        Me.lblFilter.Size = New System.Drawing.Size(71, 15)
        Me.lblFilter.TabIndex = 14
        Me.lblFilter.Text = "Filter"
        Me.lblFilter.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'frmCommonExportData
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(385, 440)
        Me.Controls.Add(Me.lblFilter)
        Me.Controls.Add(Me.txtFilter)
        Me.Controls.Add(Me.chkSelectAll)
        Me.Controls.Add(Me.EZeeFooter1)
        Me.Controls.Add(Me.EZeeHeader1)
        Me.Controls.Add(Me.lvMasters)
        Me.Controls.Add(Me.cboFileType)
        Me.Controls.Add(Me.lblFileType)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        '  Me.HelpProvider1.SetHelpNavigator(Me, System.Windows.Forms.HelpNavigator.Topic)
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmCommonExportData"
        '    Me.HelpProvider1.SetShowHelp(Me, True)
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Common Export"
        Me.EZeeFooter1.ResumeLayout(False)
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents btnExport As eZee.Common.eZeeLightButton
    Friend WithEvents cboFileType As System.Windows.Forms.ComboBox
    Friend WithEvents lblFileType As System.Windows.Forms.Label
    Friend WithEvents lvMasters As eZee.Common.eZeeListView
    Friend WithEvents EZeeHeader1 As eZee.Common.eZeeHeader
    Friend WithEvents EZeeFooter1 As eZee.Common.eZeeFooter
    Friend WithEvents btnclose As eZee.Common.eZeeLightButton
    Friend WithEvents colhSelect As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhMasterName As System.Windows.Forms.ColumnHeader
    Friend WithEvents chkSelectAll As System.Windows.Forms.CheckBox
    Friend WithEvents txtFilter As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblFilter As System.Windows.Forms.Label
End Class
