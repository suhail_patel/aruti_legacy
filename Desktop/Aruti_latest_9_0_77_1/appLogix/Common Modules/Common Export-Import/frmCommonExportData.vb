﻿
Option Strict On

Imports eZeeCommonLib
Imports Aruti.Data

'Last Message Index = 5


Public Class frmCommonExportData

#Region " Private Variables "

    Private ReadOnly mstrModuleName As String = "frmCommonExportData"
    Private mblnCancel As Boolean = True
    Private menAction As enAction = enAction.ADD_ONE
    Dim objExportdata As clsCommonImportExport

#End Region

#Region "Form Event"

    Private Sub frmCommonExportData_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            Call Set_Logo(Me, gApplicationType)
            objExportdata = New clsCommonImportExport
            cboFileType.Items.Add(Language.getMessage(mstrModuleName, 0, "Select Export Type"))
            cboFileType.Items.Add(Language.getMessage(mstrModuleName, 1, "XML"))
            cboFileType.Items.Add(Language.getMessage(mstrModuleName, 2, "EXCEL"))
            cboFileType.Items.Add(Language.getMessage(mstrModuleName, 3, "CSV"))
            cboFileType.SelectedIndex = 0
            lvMasters.Select()
            FillMasters()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmCommonExportData_Load", mstrModuleName)
        End Try
    End Sub

#End Region

#Region "Private Methods"

    Public Sub FillMasters()
        Try

            Dim objMaster As New clsMasterData
            Dim dsList As DataSet = objMaster.GetMasterName("Masters")

            Dim lvItem As ListViewItem
            lvMasters.Items.Clear()

            For Each dr As DataRow In dsList.Tables("Masters").Rows
                lvItem = New ListViewItem
                lvItem.Tag = dr("Id")
                lvItem.Text = ""
                lvItem.SubItems.Add(dr("name").ToString)
                lvMasters.Items.Add(lvItem)
                RemoveHandler lvMasters.ItemChecked, AddressOf lvMasters_ItemChecked
            Next

            AddHandler lvMasters.ItemChecked, AddressOf lvMasters_ItemChecked

            If lvMasters.Items.Count > 18 Then
                colhMasterName.Width = 325 - 18
            Else
                colhMasterName.Width = 325
            End If

            If lvMasters.Items.Count > 0 Then lvMasters.Items(0).Selected = True

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillMasters", mstrModuleName)
        End Try
    End Sub

    Private Sub SetOperation(ByVal blnOperation As Boolean)
        Try
            For Each Item As ListViewItem In lvMasters.Items
                Item.Checked = blnOperation
            Next
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetOperation", mstrModuleName)
        End Try
    End Sub

    Public Function ExportMasterData() As Boolean
        Dim strFilePath As String = ""
        Dim path As String = ""
        Dim blnFlag As Boolean = False
        Try


            path = ConfigParameter._Object._ExportDataPath & "\" & Company._Object._Code
            Dim strFormat As String = eZeeDate.convertDate(ConfigParameter._Object._CurrentDateAndTime) & Format(ConfigParameter._Object._CurrentDateAndTime, "hhmmss")
            For i As Integer = 0 To lvMasters.CheckedItems.Count - 1

                strFilePath = ""



                Select Case CInt(lvMasters.CheckedItems(i).Tag)

                    Case 1    'COMMON MASTER

                        strFilePath = path & "_Common_Master_" & strFormat
                        blnFlag = objExportdata.ExportCommonMaster(strFilePath, cboFileType.SelectedIndex)
                        If blnFlag = False Then Return False

                    Case 2  'REASON MASTER

                        strFilePath = path & "_Reason_Master_" & strFormat
                        blnFlag = objExportdata.ExportReasonMaster(strFilePath, cboFileType.SelectedIndex)
                        If blnFlag = False Then Return False

                    Case 3 'SKILL MASTER

                        strFilePath = path & "_Skill_Master_" & strFormat
                        blnFlag = objExportdata.ExportSkillMaster(strFilePath, cboFileType.SelectedIndex)
                        If blnFlag = False Then Return False

                    Case 4 'STATE MASTER

                        strFilePath = path & "_State_Master_" & strFormat
                        blnFlag = objExportdata.ExportStateMaster(strFilePath, cboFileType.SelectedIndex)
                        If blnFlag = False Then Return False

                    Case 5 'CITY MASTER

                        strFilePath = path & "_City_Master_" & strFormat
                        blnFlag = objExportdata.ExportCityMaster(strFilePath, cboFileType.SelectedIndex)
                        If blnFlag = False Then Return False

                    Case 6 'ZIPCODE MASTER

                        strFilePath = path & "_Zipcode_Master_" & strFormat
                        blnFlag = objExportdata.ExportZipcodeMaster(strFilePath, cboFileType.SelectedIndex)
                        If blnFlag = False Then Return False

                    Case 7 'ADVERTISE MASTER

                        strFilePath = path & "_Advertise_Master_" & strFormat
                        blnFlag = objExportdata.ExportAdvertiseMaster(strFilePath, cboFileType.SelectedIndex)
                        If blnFlag = False Then Return False

                    Case 8 'QUALIFICATION / COURSE MASTER

                        strFilePath = path & "_Qualification_Master_" & strFormat
                        blnFlag = objExportdata.ExportQualificationMaster(strFilePath, cboFileType.SelectedIndex)
                        If blnFlag = False Then Return False

                    Case 9 'RESULT MASTER

                        strFilePath = path & "_Result_Master_" & strFormat
                        blnFlag = objExportdata.ExportResultMaster(strFilePath, cboFileType.SelectedIndex)
                        If blnFlag = False Then Return False

                    Case 10 'MEMBERSHIP MASTER

                        strFilePath = path & "_Member_Master_" & strFormat
                        blnFlag = objExportdata.ExportMembershipMaster(strFilePath, cboFileType.SelectedIndex)
                        If blnFlag = False Then Return False

                    Case 11 'BENEFIT PLAN MASTER

                        strFilePath = path & "_Benefit_Plan_Master_" & strFormat
                        blnFlag = objExportdata.ExportBenefit_plan_Master(strFilePath, cboFileType.SelectedIndex)
                        If blnFlag = False Then Return False

                    Case 12 'BRANCH MASTER

                        strFilePath = path & "_Branch_Master_" & strFormat
                        blnFlag = objExportdata.ExportBranchMaster(strFilePath, cboFileType.SelectedIndex)
                        If blnFlag = False Then Return False

                    Case 13 'DEPARTMENT GROUP MASTER

                        strFilePath = path & "_Depatment_Group_Master_" & strFormat
                        blnFlag = objExportdata.ExportDeptGroupMaster(strFilePath, cboFileType.SelectedIndex)
                        If blnFlag = False Then Return False

                    Case 14 'DEPARTMENT MASTER

                        strFilePath = path & "_Depatment_Master_" & strFormat
                        blnFlag = objExportdata.ExportDepartmentMaster(strFilePath, cboFileType.SelectedIndex)
                        If blnFlag = False Then Return False

                        'S.SANDEEP [ 07 NOV 2011 ] -- START
                        'ENHANCEMENT : TRA CHANGES
                        'Case 15 'SECTION MASTER

                        '    strFilePath = path & "_Section_Master_" & strFormat
                        '    blnFlag = objExportdata.ExportSectionMaster(strFilePath, cboFileType.SelectedIndex)
                        '    If blnFlag = False Then Return False

                        'Case 16 'UNIT MASTER

                        '    strFilePath = path & "_Unit_Master_" & strFormat
                        '    blnFlag = objExportdata.ExportUnitMaster(strFilePath, cboFileType.SelectedIndex)
                        '    If blnFlag = False Then Return False

                        'Case 17 'JOB GROUP MASTER

                        '    strFilePath = path & "_Job_Group_Master_" & strFormat
                        '    blnFlag = objExportdata.ExportJobGroupMaster(strFilePath, cboFileType.SelectedIndex)
                        '    If blnFlag = False Then Return False

                        'Case 18 'JOB MASTER

                        '    strFilePath = path & "_Job_Master_" & strFormat
                        '    blnFlag = objExportdata.ExportJobMaster(strFilePath, cboFileType.SelectedIndex)
                        '    If blnFlag = False Then Return False

                        'Case 19 'CLASS GROUP MASTER

                        '    strFilePath = path & "_Class_Group_Master_" & strFormat
                        '    blnFlag = objExportdata.ExportClassGroupMaster(strFilePath, cboFileType.SelectedIndex)
                        '    If blnFlag = False Then Return False

                        'Case 20 'CLASS MASTER

                        '    strFilePath = path & "_Class_Master_" & strFormat
                        '    blnFlag = objExportdata.ExportClassMaster(strFilePath, cboFileType.SelectedIndex)
                        '    If blnFlag = False Then Return False

                        'Case 21 'GRADE GROUP MASTER

                        '    strFilePath = path & "_Grade_Group_Master_" & strFormat
                        '    blnFlag = objExportdata.ExportGradeGroupMaster(strFilePath, cboFileType.SelectedIndex)
                        '    If blnFlag = False Then Return False

                        'Case 22 'GRADE MASTER

                        '    strFilePath = path & "_Grade_Master_" & strFormat
                        '    blnFlag = objExportdata.ExportGradeMaster(strFilePath, cboFileType.SelectedIndex)
                        '    If blnFlag = False Then Return False

                        'Case 23 'GRADE LEVEL MASTER

                        '    strFilePath = path & "_Grade_Level_Master_" & strFormat
                        '    blnFlag = objExportdata.ExportGradeLevelMaster(strFilePath, cboFileType.SelectedIndex)
                        '    If blnFlag = False Then Return False

                        'Case 24 'SHIFT MASTER

                        '    strFilePath = path & "_Shift_Master_" & strFormat
                        '    blnFlag = objExportdata.ExportShiftMaster(strFilePath, cboFileType.SelectedIndex)
                        '    If blnFlag = False Then Return False

                        'Case 25 'PAYROLL GROUP MASTER

                        '    strFilePath = path & "_Payroll_Group_Master_" & strFormat
                        '    blnFlag = objExportdata.ExportPayrollGroupMaster(strFilePath, cboFileType.SelectedIndex)
                        '    If blnFlag = False Then Return False

                        'Case 26 'COST CENTER MASTER

                        '    strFilePath = path & "_Cost_Center_Master_" & strFormat
                        '    blnFlag = objExportdata.ExportCostCenterMaster(strFilePath, cboFileType.SelectedIndex)
                        '    If blnFlag = False Then Return False

                        'Case 27 'PAY POINT MASTER

                        '    strFilePath = path & "_Pay_Point_Master_" & strFormat
                        '    blnFlag = objExportdata.ExportPayPointMaster(strFilePath, cboFileType.SelectedIndex)
                        '    If blnFlag = False Then Return False

                        'Case 28 'TRNSACTION HEAD MASTER

                        '    strFilePath = path & "_Transaction_Head_Master_" & strFormat
                        '    blnFlag = objExportdata.ExportTransactionHeadMaster(strFilePath, cboFileType.SelectedIndex)
                        '    If blnFlag = False Then Return False

                        '    'strFilePath = path & "_Transaction_Head_Formula_" & strFormat
                        '    'blnFlag = objExportdata.ExportTransactionHead_formula(strFilePath, cboFileType.SelectedIndex)
                        '    'If blnFlag = False Then Return False

                        '    'strFilePath = path & "_Transaction_Head_Slab_" & strFormat
                        '    'blnFlag = objExportdata.ExportTransactionHead_Slab(strFilePath, cboFileType.SelectedIndex)
                        '    'If blnFlag = False Then Return False

                        '    'strFilePath = path & "_Transaction_Head_Inexcess_" & strFormat
                        '    'blnFlag = objExportdata.ExportTransactionHead_InexcessSlab(strFilePath, cboFileType.SelectedIndex)
                        '    'If blnFlag = False Then Return False


                        'Case 29 'WAGES MASTER

                        '    strFilePath = path & "_Wages_Master_" & strFormat
                        '    blnFlag = objExportdata.ExportWagesMaster(strFilePath, cboFileType.SelectedIndex)
                        '    If blnFlag = False Then Return False

                        '    strFilePath = path & "_Wages_Tran_" & strFormat
                        '    blnFlag = objExportdata.ExportWages_Tran(strFilePath, cboFileType.SelectedIndex)
                        '    If blnFlag = False Then Return False


                        'Case 30 'EMPLOYEE MASTER

                        '    strFilePath = path & "_Employee_Master_" & strFormat
                        '    blnFlag = objExportdata.ExportEmployeeMaster(strFilePath, cboFileType.SelectedIndex)
                        '    If blnFlag = False Then Return False

                        '    'strFilePath = path & "_Employee_Allergies_tran_" & strFormat
                        '    'blnFlag = objExportdata.ExportEmployee_Allergies(strFilePath, cboFileType.SelectedIndex)
                        '    'If blnFlag = False Then Return False

                        '    'strFilePath = path & "_Employee_Disabilities_tran_" & strFormat
                        '    'blnFlag = objExportdata.ExportEmployee_Disability(strFilePath, cboFileType.SelectedIndex)
                        '    'If blnFlag = False Then Return False

                        '    'strFilePath = path & "_Employee_IdInfo_tran_" & strFormat
                        '    'blnFlag = objExportdata.ExportEmployee_Idinfo_tran(strFilePath, cboFileType.SelectedIndex)
                        '    'If blnFlag = False Then Return False

                        '    'strFilePath = path & "_Employee_MemInfo_tran_" & strFormat
                        '    'blnFlag = objExportdata.ExportEmployee_Meminfo_tran(strFilePath, cboFileType.SelectedIndex)
                        '    'If blnFlag = False Then Return False


                        'Case 31 'DEPENDANTS MASTER

                        '    strFilePath = path & "_Dependants_Master_" & strFormat
                        '    blnFlag = objExportdata.ExportDependantsMaster(strFilePath, cboFileType.SelectedIndex)
                        '    If blnFlag = False Then Return False

                        '    strFilePath = path & "_Dependants_benefit_tran_" & strFormat
                        '    blnFlag = objExportdata.ExportDependants_Benefit_Tran(strFilePath, cboFileType.SelectedIndex)
                        '    If blnFlag = False Then Return False

                        '    strFilePath = path & "_Dependants_Membership_tran_" & strFormat
                        '    blnFlag = objExportdata.ExportDependants_Membership_Tran(strFilePath, cboFileType.SelectedIndex)
                        '    If blnFlag = False Then Return False

                        'Case 32 'EMPLOYEE REFEREE MASTER

                        '    strFilePath = path & "_Employee_Referee_Master_" & strFormat
                        '    blnFlag = objExportdata.ExportEmployee_Referee_Master(strFilePath, cboFileType.SelectedIndex)
                        '    If blnFlag = False Then Return False

                        'Case 33 'EMPLOYEE ASSEST MASTER

                        '    strFilePath = path & "_Employee_Assest_Master_" & strFormat
                        '    blnFlag = objExportdata.ExportEmployee_Assest_Master(strFilePath, cboFileType.SelectedIndex)
                        '    If blnFlag = False Then Return False

                        'Case 34 'DISCIPLINARY ACTION MASTER

                        '    strFilePath = path & "_Disciplinary_Action_Master_" & strFormat
                        '    blnFlag = objExportdata.ExportDisciplinary_Action_Master(strFilePath, cboFileType.SelectedIndex)
                        '    If blnFlag = False Then Return False

                        'Case 35 'DISCIPLINE TYPE MASTER

                        '    strFilePath = path & "_Discipline_Type_Master_" & strFormat
                        '    blnFlag = objExportdata.ExportDiscipline_Type_Master(strFilePath, cboFileType.SelectedIndex)
                        '    If blnFlag = False Then Return False

                        'Case 36 'DISCIPLINE STATUS MASTER

                        '    strFilePath = path & "_Discipline_Status_Master_" & strFormat
                        '    blnFlag = objExportdata.ExportDiscipline_Status_Master(strFilePath, cboFileType.SelectedIndex)
                        '    If blnFlag = False Then Return False

                        'Case 37 'PAYROLL PERIOD MASTER

                        '    strFilePath = path & "_Period_Master_" & strFormat
                        '    blnFlag = objExportdata.ExportPayroll_Period_Master(strFilePath, cboFileType.SelectedIndex)
                        '    If blnFlag = False Then Return False

                        'Case 38 'ASSESSMENT GROUP MASTER

                        '    strFilePath = path & "_Assessment_Group_Master_" & strFormat
                        '    blnFlag = objExportdata.ExportAssessment_Group_Master(strFilePath, cboFileType.SelectedIndex)
                        '    If blnFlag = False Then Return False

                        'Case 39 'ASSESSMENT ITEM MASTER

                        '    strFilePath = path & "_Assessment_Item_Master_" & strFormat
                        '    blnFlag = objExportdata.ExportAssessment_Item_Master(strFilePath, cboFileType.SelectedIndex)
                        '    If blnFlag = False Then Return False

                        'Case 40 'BANK BRANCH MASTER

                        '    strFilePath = path & "_Bank_Branch_Master_" & strFormat
                        '    blnFlag = objExportdata.ExportBank_Branch_Master(strFilePath, cboFileType.SelectedIndex)
                        '    If blnFlag = False Then Return False

                        'Case 41 'BANK ACCOUNT TYPE MASTER

                        '    strFilePath = path & "_Bank_Account_Type_Master_" & strFormat
                        '    blnFlag = objExportdata.ExportBank_Account_Type_Master(strFilePath, cboFileType.SelectedIndex)
                        '    If blnFlag = False Then Return False

                        'Case 42 'CURRENCY MASTER

                        '    strFilePath = path & "_Currency_Master_" & strFormat
                        '    blnFlag = objExportdata.ExportCurrency_Master(strFilePath, cboFileType.SelectedIndex)
                        '    If blnFlag = False Then Return False

                        'Case 43 'DENOMINATION MASTER

                        '    strFilePath = path & "_Denomination_Master_" & strFormat
                        '    blnFlag = objExportdata.ExportDenomination_Master(strFilePath, cboFileType.SelectedIndex)
                        '    If blnFlag = False Then Return False

                        'Case 44 'MEDICAL MASTER

                        '    strFilePath = path & "_Medical_Master_" & strFormat
                        '    blnFlag = objExportdata.ExportMedical_Master(strFilePath, cboFileType.SelectedIndex)
                        '    If blnFlag = False Then Return False

                        'Case 45 'SERVICE PROVIDER MASTER

                        '    strFilePath = path & "_Service_Provider_Master_" & strFormat
                        '    blnFlag = objExportdata.ExportService_Provider_Master(strFilePath, cboFileType.SelectedIndex)
                        '    If blnFlag = False Then Return False

                        'Case 46 'ASSIGN MEDICAL CATEGORY MASTER

                        '    strFilePath = path & "_Medical_Category_Master_" & strFormat
                        '    blnFlag = objExportdata.ExportMedical_Category_Master(strFilePath, cboFileType.SelectedIndex)
                        '    If blnFlag = False Then Return False

                        'Case 47 'EMPLOYEE INJURIES MASTER

                        '    strFilePath = path & "_Employee_Injury_Master_" & strFormat
                        '    blnFlag = objExportdata.ExportEmployee_Injury_Master(strFilePath, cboFileType.SelectedIndex)
                        '    If blnFlag = False Then Return False

                        'Case 48 'LEAVE TYPE MASTER

                        '    strFilePath = path & "_Leave_Type_Master_" & strFormat
                        '    blnFlag = objExportdata.ExportLeave_Type_Master(strFilePath, cboFileType.SelectedIndex)
                        '    If blnFlag = False Then Return False

                        'Case 49 'HOLIDAY MASTER

                        '    strFilePath = path & "_Holiday_Master_" & strFormat
                        '    blnFlag = objExportdata.ExportHoliday_Master(strFilePath, cboFileType.SelectedIndex)
                        '    If blnFlag = False Then Return False

                        'Case 50 'APPROVER LEVEL MASTER

                        '    strFilePath = path & "_Approver_Level_Master_" & strFormat
                        '    blnFlag = objExportdata.ExportApprover_Level_Master(strFilePath, cboFileType.SelectedIndex)
                        '    If blnFlag = False Then Return False

                        'Case 51 'LEAVE PLANNER

                        '    strFilePath = path & "_Leave_Planner_Master_" & strFormat
                        '    blnFlag = objExportdata.ExportLeave_Planner_Master(strFilePath, cboFileType.SelectedIndex)
                        '    If blnFlag = False Then Return False

                        'Case 52 'LEAVE ACCRUE MASTER

                        '    strFilePath = path & "_Leave_Accrue_Master_" & strFormat
                        '    blnFlag = objExportdata.ExportLeave_Accrue_Master(strFilePath, cboFileType.SelectedIndex)
                        '    If blnFlag = False Then Return False

                        'Case 53 'LOAN SCHEME MASTER

                        '    strFilePath = path & "_Loan_Scheme_Master_" & strFormat
                        '    blnFlag = objExportdata.ExportLoan_Scheme_Master(strFilePath, cboFileType.SelectedIndex)
                        '    If blnFlag = False Then Return False

                        'Case 54  'SAVING SCHEME MASTER

                        '    strFilePath = path & "_Saving_Scheme_Master_" & strFormat
                        '    blnFlag = objExportdata.ExportLoan_Scheme_Master(strFilePath, cboFileType.SelectedIndex)
                        '    If blnFlag = False Then Return False

                        'Case 55  'APPLICANT MASTER

                        '    strFilePath = path & "_Applicant_Master_" & strFormat
                        '    blnFlag = objExportdata.ExportApplicant_Master(strFilePath, cboFileType.SelectedIndex)
                        '    If blnFlag = False Then Return False

                        'Case 56  'VACANCY MASTER

                        '    strFilePath = path & "_Vacancy_Master_" & strFormat
                        '    blnFlag = objExportdata.ExportVacancy_Master(strFilePath, cboFileType.SelectedIndex)
                        '    If blnFlag = False Then Return False

                        'Case 57  'TRAINING PROVIDER MASTER

                        '    strFilePath = path & "_Training_Provider_Master_" & strFormat
                        '    blnFlag = objExportdata.ExportTraining_Provider_Master(strFilePath, cboFileType.SelectedIndex)
                        '    If blnFlag = False Then Return False

                        'Case 58  'LETTER TYPE MASTER

                        '    strFilePath = path & "_Letter_Type_Master_" & strFormat
                        '    blnFlag = objExportdata.ExportLetter_Type_Master(strFilePath, cboFileType.SelectedIndex)
                        '    If blnFlag = False Then Return False

                        'Case 59  'REMINDER MASTER

                        '    strFilePath = path & "_Reminder_Master_" & strFormat
                        '    blnFlag = objExportdata.ExportReminder_Master(strFilePath, cboFileType.SelectedIndex)
                        '    If blnFlag = False Then Return False

                    Case 15 'SECTION GROUP 

                        strFilePath = path & "_Section_Grp_Master_" & strFormat
                        blnFlag = objExportdata.ExportSectionGrpMaster(strFilePath, cboFileType.SelectedIndex)
                        If blnFlag = False Then Return False

                    Case 16 'SECTION MASTER

                        strFilePath = path & "_Section_Master_" & strFormat
                        blnFlag = objExportdata.ExportSectionMaster(strFilePath, cboFileType.SelectedIndex)
                        If blnFlag = False Then Return False

                    Case 17 'UNIT GROUP

                        strFilePath = path & "_Unit_Grp_Master_" & strFormat
                        blnFlag = objExportdata.ExportUnitGrpMaster(strFilePath, cboFileType.SelectedIndex)
                        If blnFlag = False Then Return False

                    Case 18 'UNIT MASTER
                        strFilePath = path & "_Unit_Master_" & strFormat
                        blnFlag = objExportdata.ExportUnitMaster(strFilePath, cboFileType.SelectedIndex)
                        If blnFlag = False Then Return False

                    Case 19 'TEAM MASTER

                        strFilePath = path & "_Team_Master_" & strFormat
                        blnFlag = objExportdata.ExportTeamMaster(strFilePath, cboFileType.SelectedIndex)
                        If blnFlag = False Then Return False


                    Case 20 'JOB GROUP MASTER

                        strFilePath = path & "_Job_Group_Master_" & strFormat
                        blnFlag = objExportdata.ExportJobGroupMaster(strFilePath, cboFileType.SelectedIndex)
                        If blnFlag = False Then Return False

                    Case 21 'JOB MASTER

                        strFilePath = path & "_Job_Master_" & strFormat
                        blnFlag = objExportdata.ExportJobMaster(strFilePath, cboFileType.SelectedIndex)
                        If blnFlag = False Then Return False

                    Case 22 'CLASS GROUP MASTER

                        strFilePath = path & "_Class_Group_Master_" & strFormat
                        blnFlag = objExportdata.ExportClassGroupMaster(strFilePath, cboFileType.SelectedIndex)
                        If blnFlag = False Then Return False

                    Case 23 'CLASS MASTER

                        strFilePath = path & "_Class_Master_" & strFormat
                        blnFlag = objExportdata.ExportClassMaster(strFilePath, cboFileType.SelectedIndex)
                        If blnFlag = False Then Return False

                    Case 24 'GRADE GROUP MASTER

                        strFilePath = path & "_Grade_Group_Master_" & strFormat
                        blnFlag = objExportdata.ExportGradeGroupMaster(strFilePath, cboFileType.SelectedIndex)
                        If blnFlag = False Then Return False

                    Case 25 'GRADE MASTER

                        strFilePath = path & "_Grade_Master_" & strFormat
                        blnFlag = objExportdata.ExportGradeMaster(strFilePath, cboFileType.SelectedIndex)
                        If blnFlag = False Then Return False

                    Case 26 'GRADE LEVEL MASTER

                        strFilePath = path & "_Grade_Level_Master_" & strFormat
                        blnFlag = objExportdata.ExportGradeLevelMaster(strFilePath, cboFileType.SelectedIndex)
                        If blnFlag = False Then Return False

                    Case 27 'SHIFT MASTER

                        strFilePath = path & "_Shift_Master_" & strFormat
                        blnFlag = objExportdata.ExportShiftMaster(strFilePath, cboFileType.SelectedIndex)
                        If blnFlag = False Then Return False

                    Case 28 'PAYROLL GROUP MASTER

                        strFilePath = path & "_Payroll_Group_Master_" & strFormat
                        blnFlag = objExportdata.ExportPayrollGroupMaster(strFilePath, cboFileType.SelectedIndex)
                        If blnFlag = False Then Return False

                    Case 29 'COST CENTER MASTER

                        strFilePath = path & "_Cost_Center_Master_" & strFormat
                        blnFlag = objExportdata.ExportCostCenterMaster(strFilePath, cboFileType.SelectedIndex)
                        If blnFlag = False Then Return False

                    Case 30 'PAY POINT MASTER

                        strFilePath = path & "_Pay_Point_Master_" & strFormat
                        blnFlag = objExportdata.ExportPayPointMaster(strFilePath, cboFileType.SelectedIndex)
                        If blnFlag = False Then Return False

                    Case 31 'TRNSACTION HEAD MASTER

                        strFilePath = path & "_Transaction_Head_Master_" & strFormat
                        blnFlag = objExportdata.ExportTransactionHeadMaster(strFilePath, cboFileType.SelectedIndex)
                        If blnFlag = False Then Return False

                        'strFilePath = path & "_Transaction_Head_Formula_" & strFormat
                        'blnFlag = objExportdata.ExportTransactionHead_formula(strFilePath, cboFileType.SelectedIndex)
                        'If blnFlag = False Then Return False

                        'strFilePath = path & "_Transaction_Head_Slab_" & strFormat
                        'blnFlag = objExportdata.ExportTransactionHead_Slab(strFilePath, cboFileType.SelectedIndex)
                        'If blnFlag = False Then Return False

                        'strFilePath = path & "_Transaction_Head_Inexcess_" & strFormat
                        'blnFlag = objExportdata.ExportTransactionHead_InexcessSlab(strFilePath, cboFileType.SelectedIndex)
                        'If blnFlag = False Then Return False


                    Case 32 'WAGES MASTER

                        strFilePath = path & "_Wages_Master_" & strFormat
                        blnFlag = objExportdata.ExportWagesMaster(strFilePath, cboFileType.SelectedIndex)
                        If blnFlag = False Then Return False

                        strFilePath = path & "_Wages_Tran_" & strFormat
                        blnFlag = objExportdata.ExportWages_Tran(strFilePath, cboFileType.SelectedIndex)
                        If blnFlag = False Then Return False


                    Case 33 'EMPLOYEE MASTER

                        strFilePath = path & "_Employee_Master_" & strFormat
                        blnFlag = objExportdata.ExportEmployeeMaster(strFilePath, cboFileType.SelectedIndex)
                        If blnFlag = False Then Return False

                        'strFilePath = path & "_Employee_Allergies_tran_" & strFormat
                        'blnFlag = objExportdata.ExportEmployee_Allergies(strFilePath, cboFileType.SelectedIndex)
                        'If blnFlag = False Then Return False

                        'strFilePath = path & "_Employee_Disabilities_tran_" & strFormat
                        'blnFlag = objExportdata.ExportEmployee_Disability(strFilePath, cboFileType.SelectedIndex)
                        'If blnFlag = False Then Return False

                        'strFilePath = path & "_Employee_IdInfo_tran_" & strFormat
                        'blnFlag = objExportdata.ExportEmployee_Idinfo_tran(strFilePath, cboFileType.SelectedIndex)
                        'If blnFlag = False Then Return False

                        'strFilePath = path & "_Employee_MemInfo_tran_" & strFormat
                        'blnFlag = objExportdata.ExportEmployee_Meminfo_tran(strFilePath, cboFileType.SelectedIndex)
                        'If blnFlag = False Then Return False


                    Case 34 'DEPENDANTS MASTER

                        strFilePath = path & "_Dependants_Master_" & strFormat
                        'Sohail (18 May 2019) -- Start
                        'NMB Enhancement - REF # - 76.1 - On dependants menu, provide a way for user to set status of dependant as deceased etc. Allow user to deactivate a dependant with a reason instead of deleting.
                        'blnFlag = objExportdata.ExportDependantsMaster(strFilePath, cboFileType.SelectedIndex)
                        blnFlag = objExportdata.ExportDependantsMaster(strFilePath, cboFileType.SelectedIndex, FinancialYear._Object._DatabaseName, _
                                                                                                               User._Object._Userunkid, _
                                                                                                               FinancialYear._Object._YearUnkid, _
                                                                                                               Company._Object._Companyunkid, _
                                                                                                               eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                                                                               eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                                                                               ConfigParameter._Object._UserAccessModeSetting, True, True, False)
                        'Sohail (18 May 2019) -- End
                        If blnFlag = False Then Return False

                        strFilePath = path & "_Dependants_benefit_tran_" & strFormat
                        blnFlag = objExportdata.ExportDependants_Benefit_Tran(strFilePath, cboFileType.SelectedIndex)
                        If blnFlag = False Then Return False

                        strFilePath = path & "_Dependants_Membership_tran_" & strFormat
                        blnFlag = objExportdata.ExportDependants_Membership_Tran(strFilePath, cboFileType.SelectedIndex)
                        If blnFlag = False Then Return False

                    Case 35 'EMPLOYEE REFEREE MASTER

                        strFilePath = path & "_Employee_Referee_Master_" & strFormat
                        blnFlag = objExportdata.ExportEmployee_Referee_Master(strFilePath, cboFileType.SelectedIndex)
                        If blnFlag = False Then Return False

                    Case 36 'EMPLOYEE ASSEST MASTER

                        strFilePath = path & "_Employee_Assest_Master_" & strFormat
                        blnFlag = objExportdata.ExportEmployee_Assest_Master(strFilePath, cboFileType.SelectedIndex)
                        If blnFlag = False Then Return False

                    Case 37 'DISCIPLINARY ACTION MASTER

                        strFilePath = path & "_Disciplinary_Action_Master_" & strFormat
                        blnFlag = objExportdata.ExportDisciplinary_Action_Master(strFilePath, cboFileType.SelectedIndex)
                        If blnFlag = False Then Return False

                    Case 38 'DISCIPLINE TYPE MASTER

                        strFilePath = path & "_Discipline_Type_Master_" & strFormat
                        blnFlag = objExportdata.ExportDiscipline_Type_Master(strFilePath, cboFileType.SelectedIndex)
                        If blnFlag = False Then Return False

                    Case 39 'DISCIPLINE STATUS MASTER

                        strFilePath = path & "_Discipline_Status_Master_" & strFormat
                        blnFlag = objExportdata.ExportDiscipline_Status_Master(strFilePath, cboFileType.SelectedIndex)
                        If blnFlag = False Then Return False

                    Case 40 'PAYROLL PERIOD MASTER

                        strFilePath = path & "_Period_Master_" & strFormat
                        blnFlag = objExportdata.ExportPayroll_Period_Master(strFilePath, cboFileType.SelectedIndex)
                        If blnFlag = False Then Return False

                    Case 41 'ASSESSMENT GROUP MASTER

                        strFilePath = path & "_Assessment_Group_Master_" & strFormat
                        blnFlag = objExportdata.ExportAssessment_Group_Master(strFilePath, cboFileType.SelectedIndex)
                        If blnFlag = False Then Return False

                    Case 42 'ASSESSMENT ITEM MASTER

                        strFilePath = path & "_Assessment_Item_Master_" & strFormat
                        blnFlag = objExportdata.ExportAssessment_Item_Master(strFilePath, cboFileType.SelectedIndex)
                        If blnFlag = False Then Return False

                    Case 43 'BANK BRANCH MASTER

                        strFilePath = path & "_Bank_Branch_Master_" & strFormat
                        blnFlag = objExportdata.ExportBank_Branch_Master(strFilePath, cboFileType.SelectedIndex)
                        If blnFlag = False Then Return False

                    Case 44 'BANK ACCOUNT TYPE MASTER

                        strFilePath = path & "_Bank_Account_Type_Master_" & strFormat
                        blnFlag = objExportdata.ExportBank_Account_Type_Master(strFilePath, cboFileType.SelectedIndex)
                        If blnFlag = False Then Return False

                    Case 45 'CURRENCY MASTER

                        strFilePath = path & "_Currency_Master_" & strFormat
                        blnFlag = objExportdata.ExportCurrency_Master(strFilePath, cboFileType.SelectedIndex)
                        If blnFlag = False Then Return False

                    Case 46 'DENOMINATION MASTER

                        strFilePath = path & "_Denomination_Master_" & strFormat
                        blnFlag = objExportdata.ExportDenomination_Master(strFilePath, cboFileType.SelectedIndex)
                        If blnFlag = False Then Return False

                    Case 47 'MEDICAL MASTER

                        strFilePath = path & "_Medical_Master_" & strFormat
                        blnFlag = objExportdata.ExportMedical_Master(strFilePath, cboFileType.SelectedIndex)
                        If blnFlag = False Then Return False

                    Case 48 'SERVICE PROVIDER MASTER

                        strFilePath = path & "_Service_Provider_Master_" & strFormat
                        blnFlag = objExportdata.ExportService_Provider_Master(strFilePath, cboFileType.SelectedIndex)
                        If blnFlag = False Then Return False

                    Case 49 'ASSIGN MEDICAL CATEGORY MASTER

                        strFilePath = path & "_Medical_Category_Master_" & strFormat
                        blnFlag = objExportdata.ExportMedical_Category_Master(strFilePath, cboFileType.SelectedIndex)
                        If blnFlag = False Then Return False

                    Case 50 'EMPLOYEE INJURIES MASTER

                        strFilePath = path & "_Employee_Injury_Master_" & strFormat

                        'S.SANDEEP [28-May-2018] -- START
                        'ISSUE/ENHANCEMENT : {Audit Trails} 
                        objExportdata._FormName = mstrModuleName
                        objExportdata._LoginEmployeeunkid = 0
                        objExportdata._ClientIP = getIP()
                        objExportdata._HostName = getHostName()
                        objExportdata._FromWeb = False
                        objExportdata._AuditUserId = User._Object._Userunkid
objExportdata._CompanyUnkid = Company._Object._Companyunkid
                        objExportdata._AuditDate = ConfigParameter._Object._CurrentDateAndTime
                        'S.SANDEEP [28-May-2018] -- END

                        'Shani(24-Aug-2015) -- Start
                        'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                        'blnFlag = objExportdata.ExportEmployee_Injury_Master(strFilePath, cboFileType.SelectedIndex)
                        blnFlag = objExportdata.ExportEmployee_Injury_Master(FinancialYear._Object._DatabaseName, _
                                                                             User._Object._Userunkid, _
                                                                             FinancialYear._Object._YearUnkid, _
                                                                             Company._Object._Companyunkid, _
                                                                             eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                                             eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                                             ConfigParameter._Object._UserAccessModeSetting, _
                                                                             True, ConfigParameter._Object._IsIncludeInactiveEmp, _
                                                                             strFilePath, cboFileType.SelectedIndex)
                        'Shani(24-Aug-2015) -- End
                        If blnFlag = False Then Return False

                    Case 51 'LEAVE TYPE MASTER

                        strFilePath = path & "_Leave_Type_Master_" & strFormat
                        blnFlag = objExportdata.ExportLeave_Type_Master(strFilePath, cboFileType.SelectedIndex)
                        If blnFlag = False Then Return False

                    Case 52 'HOLIDAY MASTER

                        strFilePath = path & "_Holiday_Master_" & strFormat
                        blnFlag = objExportdata.ExportHoliday_Master(strFilePath, cboFileType.SelectedIndex)
                        If blnFlag = False Then Return False

                    Case 53 'APPROVER LEVEL MASTER

                        strFilePath = path & "_Approver_Level_Master_" & strFormat
                        blnFlag = objExportdata.ExportApprover_Level_Master(strFilePath, cboFileType.SelectedIndex)
                        If blnFlag = False Then Return False

                    Case 54 'LEAVE PLANNER

                        strFilePath = path & "_Leave_Planner_Master_" & strFormat

                        'Pinkal (24-Aug-2015) -- Start
                        'Enhancement - WORKING ON ACTIVE EMPLOYEE CONDITION.
                        'blnFlag = objExportdata.ExportLeave_Planner_Master(strFilePath, cboFileType.SelectedIndex)
                        blnFlag = objExportdata.ExportLeave_Planner_Master(strFilePath, cboFileType.SelectedIndex, FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid _
                                                                                    , Company._Object._Companyunkid, eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate) _
                                                                                    , ConfigParameter._Object._UserAccessModeSetting, True, ConfigParameter._Object._IsIncludeInactiveEmp, True, True)
                        'Pinkal (24-Aug-2015) -- End
                        If blnFlag = False Then Return False

                    Case 55 'LEAVE ACCRUE MASTER

                        strFilePath = path & "_Leave_Accrue_Master_" & strFormat

                        'Pinkal (24-Aug-2015) -- Start
                        'Enhancement - WORKING ON ACTIVE EMPLOYEE CONDITION.
                        'blnFlag = objExportdata.ExportLeave_Accrue_Master(strFilePath, cboFileType.SelectedIndex)
                        blnFlag = objExportdata.ExportLeave_Accrue_Master(strFilePath, cboFileType.SelectedIndex, FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid _
                                                                                    , Company._Object._Companyunkid, ConfigParameter._Object._EmployeeAsOnDate _
                                                                                    , ConfigParameter._Object._UserAccessModeSetting, True, ConfigParameter._Object._IsIncludeInactiveEmp, True, True)
                        'Pinkal (24-Aug-2015) -- End
                        If blnFlag = False Then Return False

                    Case 56 'LOAN SCHEME MASTER

                        strFilePath = path & "_Loan_Scheme_Master_" & strFormat
                        blnFlag = objExportdata.ExportLoan_Scheme_Master(strFilePath, cboFileType.SelectedIndex)
                        If blnFlag = False Then Return False

                    Case 57  'SAVING SCHEME MASTER

                        strFilePath = path & "_Saving_Scheme_Master_" & strFormat
                        blnFlag = objExportdata.ExportLoan_Scheme_Master(strFilePath, cboFileType.SelectedIndex)
                        If blnFlag = False Then Return False

                    Case 58  'APPLICANT MASTER

                        strFilePath = path & "_Applicant_Master_" & strFormat
                        blnFlag = objExportdata.ExportApplicant_Master(strFilePath, cboFileType.SelectedIndex)
                        If blnFlag = False Then Return False

                    Case 59  'VACANCY MASTER

                        strFilePath = path & "_Vacancy_Master_" & strFormat
                        blnFlag = objExportdata.ExportVacancy_Master(strFilePath, cboFileType.SelectedIndex)
                        If blnFlag = False Then Return False

                    Case 60  'TRAINING PROVIDER MASTER

                        strFilePath = path & "_Training_Provider_Master_" & strFormat
                        blnFlag = objExportdata.ExportTraining_Provider_Master(strFilePath, cboFileType.SelectedIndex)
                        If blnFlag = False Then Return False

                    Case 61  'LETTER TYPE MASTER

                        strFilePath = path & "_Letter_Type_Master_" & strFormat
                        blnFlag = objExportdata.ExportLetter_Type_Master(strFilePath, cboFileType.SelectedIndex)
                        If blnFlag = False Then Return False

                    Case 62  'REMINDER MASTER

                        strFilePath = path & "_Reminder_Master_" & strFormat
                        blnFlag = objExportdata.ExportReminder_Master(strFilePath, cboFileType.SelectedIndex)
                        If blnFlag = False Then Return False
                        'S.SANDEEP [ 07 NOV 2011 ] -- END

                End Select

            Next

            If objExportdata._Message <> "" Then
                eZeeMsgBox.Show(objExportdata._Message, enMsgBoxStyle.Information)
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "ExportMasterData", mstrModuleName)
            Return False
        End Try

        Return True
    End Function

#End Region

#Region "Button Event"

    Private Sub btnExport_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnExport.Click
        Dim blnFlag As Boolean = False
        Try

            If ConfigParameter._Object._ExportDataPath = "" Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 6, "Please set Export Data Path from Aruti configuration -> Options -> Path."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                Exit Sub

            ElseIf lvMasters.CheckedItems.Count = 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 7, "Please Check atleast one Master to Export data."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                lvMasters.Select()
                Exit Sub

            ElseIf cboFileType.SelectedIndex = 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 8, "Please select Export File Type."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                cboFileType.Select()
                Exit Sub

            End If

            Cursor = Cursors.WaitCursor

            blnFlag = ExportMasterData()

            Cursor = Cursors.Default

            If blnFlag Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Data Successfully Exported."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                chkSelectAll.Checked = False
                chkSelectAll_CheckedChanged(sender, e)
                cboFileType.SelectedIndex = 0
            ElseIf blnFlag = False And objExportdata._Message = "" Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "Problem with Exporting Data.Export Data Failed."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
            ElseIf blnFlag = False And objExportdata._Message <> "" Then
                eZeeMsgBox.Show(objExportdata._Message, enMsgBoxStyle.Information)
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnExport_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnclose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnclose.Click
        Me.Close()
    End Sub

    'Anjan [04 June 2014] -- Start
    'ENHANCEMENT : Implementing Language,requested by Andrew
    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsLetterType.SetMessages()
            objfrm._Other_ModuleNames = "clsCommonImportExport"

            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub
    'Anjan [04 June 2014] -- End

#End Region

#Region "CheckBox event"

    Private Sub chkSelectAll_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkSelectAll.CheckedChanged
        Try

            If lvMasters.Items.Count = 0 Then Exit Sub

            SetOperation(CBool(chkSelectAll.CheckState))

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "chkSelectAll_CheckedChanged", mstrModuleName)
        End Try
    End Sub

#End Region

#Region "TextBox Event"

    Private Sub txtFilter_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtFilter.TextChanged
        Try
            Dim Lvitem As ListViewItem
            Lvitem = lvMasters.FindItemWithText(txtFilter.Text.Trim, True, 0)
            If Lvitem IsNot Nothing Then
                Lvitem.EnsureVisible()
                lvMasters.Items(Lvitem.Index).Selected = True
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "txtFilter_TextChanged", mstrModuleName)
        End Try
    End Sub

#End Region

#Region "ListView Event"

    Private Sub lvMasters_ItemChecked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.ItemCheckedEventArgs) Handles lvMasters.ItemChecked
        Try

            If lvMasters.CheckedItems.Count <= 0 Then
                RemoveHandler chkSelectAll.CheckedChanged, AddressOf chkSelectAll_CheckedChanged
                chkSelectAll.CheckState = CheckState.Unchecked
                AddHandler chkSelectAll.CheckedChanged, AddressOf chkSelectAll_CheckedChanged

            ElseIf lvMasters.CheckedItems.Count < lvMasters.Items.Count Then
                RemoveHandler chkSelectAll.CheckedChanged, AddressOf chkSelectAll_CheckedChanged
                chkSelectAll.CheckState = CheckState.Indeterminate
                AddHandler chkSelectAll.CheckedChanged, AddressOf chkSelectAll_CheckedChanged

            ElseIf lvMasters.CheckedItems.Count = lvMasters.Items.Count Then
                RemoveHandler chkSelectAll.CheckedChanged, AddressOf chkSelectAll_CheckedChanged
                chkSelectAll.CheckState = CheckState.Checked
                AddHandler chkSelectAll.CheckedChanged, AddressOf chkSelectAll_CheckedChanged

            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lvMasters_ItemChecked", mstrModuleName)
        End Try
    End Sub

#End Region

	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Private Sub OtherSettings()
		Try
			Me.SuspendLayout()
		
			Call SetLanguage()
			
			
			Me.EZeeHeader1.GradientColor1 = GUI._HeaderBackColor1 
			Me.EZeeHeader1.GradientColor2 = GUI._HeaderBackColor2 
			Me.EZeeHeader1.BorderColor = GUI._HeaderBorderColor 
			Me.EZeeHeader1.HeaderTextForeColor = GUI._HeaderTitleFontColor 
			Me.EZeeHeader1.DescriptionForeColor = GUI._HeaderMessageFontColor 


			Me.btnExport.GradientBackColor = GUI._ButttonBackColor 
			Me.btnExport.GradientForeColor = GUI._ButttonFontColor

			Me.btnclose.GradientBackColor = GUI._ButttonBackColor 
			Me.btnclose.GradientForeColor = GUI._ButttonFontColor


			Me.ResumeLayout()
		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "OtherSettings", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetLanguage()
		Try
			Me.Text = Language._Object.getCaption(Me.Name, Me.Text)
			
			Me.btnExport.Text = Language._Object.getCaption(Me.btnExport.Name, Me.btnExport.Text)
			Me.lblFileType.Text = Language._Object.getCaption(Me.lblFileType.Name, Me.lblFileType.Text)
			Me.EZeeHeader1.Title = Language._Object.getCaption(Me.EZeeHeader1.Name & "_Title" , Me.EZeeHeader1.Title)
			Me.EZeeHeader1.Message = Language._Object.getCaption(Me.EZeeHeader1.Name & "_Message" , Me.EZeeHeader1.Message)
			Me.btnclose.Text = Language._Object.getCaption(Me.btnclose.Name, Me.btnclose.Text)
			Me.colhSelect.Text = Language._Object.getCaption(CStr(Me.colhSelect.Tag), Me.colhSelect.Text)
			Me.colhMasterName.Text = Language._Object.getCaption(CStr(Me.colhMasterName.Tag), Me.colhMasterName.Text)
			Me.chkSelectAll.Text = Language._Object.getCaption(Me.chkSelectAll.Name, Me.chkSelectAll.Text)
			Me.lblFilter.Text = Language._Object.getCaption(Me.lblFilter.Name, Me.lblFilter.Text)

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetLanguage", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 0, "Select Export Type")
			Language.setMessage(mstrModuleName, 1, "XML")
			Language.setMessage(mstrModuleName, 2, "EXCEL")
			Language.setMessage(mstrModuleName, 3, "CSV")
			Language.setMessage(mstrModuleName, 4, "Data Successfully Exported.")
			Language.setMessage(mstrModuleName, 5, "Problem with Exporting Data.Export Data Failed.")
			Language.setMessage(mstrModuleName, 6, "Please set Export Data Path from Aruti configuration -> Options -> Path.")
			Language.setMessage(mstrModuleName, 7, "Please Check atleast one Master to Export data.")
			Language.setMessage(mstrModuleName, 8, "Please select Export File Type.")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class