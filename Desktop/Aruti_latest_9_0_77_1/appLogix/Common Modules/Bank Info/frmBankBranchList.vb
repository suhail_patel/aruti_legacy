﻿Option Strict On

Imports eZeeCommonLib
Imports Aruti.Data
'Last Message Index = 3

Public Class frmBankBranchList

#Region "Private Variable"

    Private objBankbranchmaster As clsbankbranch_master
    Private ReadOnly mstrModuleName As String = "frmBankBranchList"

#End Region

#Region "Form's Event"

    Private Sub frmBankBranchList_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        objBankbranchmaster = New clsbankbranch_master
        Try
            Call Set_Logo(Me, gApplicationType)
            Language.setLanguage(Me.Name)
            Call OtherSettings() 'Sohail (05 Mar 2013)

            Call SetVisibility()

            'Shani(25-Sep-2015) -- Start
            'ENHANCEMENT : Add Option to Import Bank Branch Master on Common Importation(Issue No : 455)
            'fillList()
            'If lvBankBranchList.Items.Count > 0 Then lvBankBranchList.Items(0).Selected = True
            'lvBankBranchList.Select()
            Call FillBankGroup()
            'Shani(25-Sep-2015) -- End

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmBankBranchList_Load", mstrModuleName)
        End Try
    End Sub

    Private Sub frmBankBranchList_KeyUp(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles MyBase.KeyUp
        Try
            If e.KeyCode = Keys.Delete And lvBankBranchList.Focused = True Then
                'Sohail (24 Jun 2011) -- Start
                'Issue : Delete event fired even if there is no delete privilege
                'btnDelete_Click(sender, e)
                Call btnDelete.PerformClick()
                'Sohail (24 Jun 2011) -- End
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmBankBranchList_KeyUp", mstrModuleName)
        End Try
    End Sub

    Private Sub frmBankBranchList_FormClosed(ByVal sender As System.Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles MyBase.FormClosed
        'Sohail (11 Sep 2010) -- Start
        Try
            objBankbranchmaster = Nothing
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmBankBranchList_FormClosed", mstrModuleName)
        End Try
        'Sohail (11 Sep 2010) -- End
    End Sub

    'Sohail (05 Mar 2013) -- Start
    'TRA - ENHANCEMENT
    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsbankbranch_master.SetMessages()
            objfrm._Other_ModuleNames = "clsbankbranch_master"
            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub
    'Sohail (05 Mar 2013) -- End

#End Region

#Region "Button's Event"

    Private Sub btnNew_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNew.Click
        Try
            Dim objFrmbank_AddEdit As New frmBankBranch_AddEdit
            If objFrmbank_AddEdit.displayDialog(-1, enAction.ADD_CONTINUE) Then
                fillList()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnNew_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnEdit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEdit.Click, lvBankBranchList.DoubleClick 'Sohail (19 Nov 2010)
        'Sohail (24 Jun 2011) -- Start
        If User._Object.Privilege._EditBankBranch = False Then Exit Sub
        'Sohail (24 Jun 2011) -- End
        Try
            If lvBankBranchList.SelectedItems.Count < 1 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please select Bank Branch from the list to perform further operation."), enMsgBoxStyle.Information) '?1
                lvBankBranchList.Select()
                Exit Sub
            End If
            Dim objfrmBankBranch_AddEdit As New frmBankBranch_AddEdit
            Try
                Dim intSelectedIndex As Integer
                intSelectedIndex = lvBankBranchList.SelectedItems(0).Index
                If objfrmBankBranch_AddEdit.displayDialog(CInt(lvBankBranchList.SelectedItems(0).Tag), enAction.EDIT_ONE) Then
                    fillList()
                End If
                objfrmBankBranch_AddEdit = Nothing

                lvBankBranchList.Items(intSelectedIndex).Selected = True
                lvBankBranchList.EnsureVisible(intSelectedIndex)
                lvBankBranchList.Select()
            Catch ex As Exception
                Call DisplayError.Show("-1", ex.Message, "btnEdit_Click", mstrModuleName)
            Finally
                If objfrmBankBranch_AddEdit IsNot Nothing Then objfrmBankBranch_AddEdit.Dispose()
            End Try
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnEdit_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDelete.Click
        If lvBankBranchList.SelectedItems.Count < 1 Then
            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please select Bank Branch from the list to perform further operation."), enMsgBoxStyle.Information) '?1
            lvBankBranchList.Select()
            Exit Sub
        End If
        'If objBankbranchmaster.isUsed(CInt(lvBankBranchList.SelectedItems(0).Tag)) Then
        '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Sorry, You cannot delete this Bank Branch. Reason: This Bank Branch is in use."), enMsgBoxStyle.Information) '?2
        '    lvBankBranchList.Select()
        '    Exit Sub
        'End If
        'Sohail (19 Nov 2010) -- Start
        Cursor.Current = Cursors.WaitCursor 'Sohail (05 Mar 2013)
        If objBankbranchmaster.isUsed(CInt(lvBankBranchList.SelectedItems(0).Tag)) Then
            Cursor.Current = Cursors.Default 'Sohail (05 Mar 2013)
            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Sorry, You cannot delete this Bank Branch. Reason: This Bank Branch is in use."), enMsgBoxStyle.Information) '?2
            lvBankBranchList.Select()
            Exit Sub
        End If
        Cursor.Current = Cursors.Default 'Sohail (05 Mar 2013)
        'Sohail (19 Nov 2010) -- End
        Try
            Dim intSelectedIndex As Integer
            intSelectedIndex = lvBankBranchList.SelectedItems(0).Index

            If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Are you sure you want to delete this Bank Branch?"), CType(enMsgBoxStyle.Question + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.Yes Then

                'S.SANDEEP [28-May-2018] -- START
                'ISSUE/ENHANCEMENT : {Audit Trails} 
                objBankbranchmaster._FormName = mstrModuleName
                objBankbranchmaster._LoginEmployeeunkid = 0
                objBankbranchmaster._ClientIP = getIP()
                objBankbranchmaster._HostName = getHostName()
                objBankbranchmaster._FromWeb = False
                objBankbranchmaster._AuditUserId = User._Object._Userunkid
objBankbranchmaster._CompanyUnkid = Company._Object._Companyunkid
                objBankbranchmaster._AuditDate = ConfigParameter._Object._CurrentDateAndTime
                'S.SANDEEP [28-May-2018] -- END

                objBankbranchmaster.Delete(CInt(lvBankBranchList.SelectedItems(0).Tag))
                lvBankBranchList.SelectedItems(0).Remove()

                If lvBankBranchList.Items.Count <= 0 Then
                    Exit Try
                End If

                If lvBankBranchList.Items.Count = intSelectedIndex Then
                    intSelectedIndex = lvBankBranchList.Items.Count - 1
                    lvBankBranchList.Items(intSelectedIndex).Selected = True
                    lvBankBranchList.EnsureVisible(intSelectedIndex)
                ElseIf lvBankBranchList.Items.Count <> 0 Then
                    lvBankBranchList.Items(intSelectedIndex).Selected = True
                    lvBankBranchList.EnsureVisible(intSelectedIndex)
                End If
            End If
            lvBankBranchList.Select()
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "btnDelete_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        'Sohail (11 Sep 2010) -- Start

        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnClose_Click", mstrModuleName)
        End Try
        'Sohail (11 Sep 2010) -- End
    End Sub

    'Shani(25-Sep-2015) -- Start
    'ENHANCEMENT : Add search Option on Add edit bank Branch  Bank Group(Issue No : 477)
    Private Sub objbtnSearchBankGroup_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchBankGroup.Click
        Dim objfrm As New frmCommonSearch
        Try
            objfrm.DataSource = CType(cboBankGroup.DataSource, DataTable)
            objfrm.ValueMember = "groupmasterunkid"
            objfrm.DisplayMember = "name"
            objfrm.CodeMember = "Code"
            If objfrm.DisplayDialog Then
                cboBankGroup.SelectedValue = objfrm.SelectedValue
            End If
            cboBankGroup.Select()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchBankGroup_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnSearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearch.Click
        Try
            Call fillList()
            If lvBankBranchList.Items.Count > 0 Then lvBankBranchList.Items(0).Selected = True
            lvBankBranchList.Select()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearch_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnReset.Click
        Try
            cboBankGroup.SelectedValue = 0
            lvBankBranchList.Items.Clear()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnReset_Click", mstrModuleName)
        End Try
    End Sub
    'Shani(25-Sep-2015) -- End

#End Region

#Region " Private Methods "

    Private Sub fillList()
        Dim dsBankBranch As New DataSet
        Try

            If User._Object.Privilege._AllowToViewBankBranchList = True Then   'Pinkal (09-Jul-2012) -- Start

                'Shani(25-Sep-2015) -- Start
                'ENHANCEMENT : Add Option to Import Bank Branch Master on Common Importation(Issue No : 455)
                'dsBankBranch = objBankbranchmaster.GetList("List")
                dsBankBranch = objBankbranchmaster.GetList("List", True)
                Dim strSearch As String = ""
                If CInt(cboBankGroup.SelectedValue) > 0 Then
                    strSearch = "bankgroupunkid = " & CStr(cboBankGroup.SelectedValue) & " "
                End If
                Dim dtTable As DataTable = New DataView(dsBankBranch.Tables(0), strSearch, "", DataViewRowState.CurrentRows).ToTable
                'Shani(25-Sep-2015) -- End

                Dim lvItem As ListViewItem

                lvBankBranchList.Items.Clear()

                'Shani(25-Sep-2015) -- Start
                'ENHANCEMENT : Add Option to Import Bank Branch Master on Common Importation(Issue No : 455)
                'For Each drRow As DataRow In dsBankBranch.Tables(0).Rows
                For Each drRow As DataRow In dtTable.Rows
                    'Shani(25-Sep-2015) -- End
                    lvItem = New ListViewItem
                    lvItem.Text = drRow("bankgroup").ToString
                    lvItem.Tag = drRow("branchunkid")
                    lvItem.SubItems.Add(drRow("branchcode").ToString)
                    lvItem.SubItems.Add(drRow("branchname").ToString)
                    lvItem.SubItems.Add(drRow("address").ToString)
                    lvItem.SubItems.Add(drRow("state").ToString)
                    lvItem.SubItems.Add(drRow("pincode").ToString)
                    lvItem.SubItems.Add(drRow("sortcode").ToString)

                    lvBankBranchList.Items.Add(lvItem)
                Next

                If lvBankBranchList.Items.Count > 16 Then
                    colhBankPincode.Width = 90 - 10
                Else
                    colhBankPincode.Width = 90
                End If

            End If

        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "fillList", mstrModuleName)
        Finally
            dsBankBranch.Dispose()
        End Try
    End Sub

    Private Sub SetVisibility()

        Try
            btnNew.Enabled = User._Object.Privilege._AddBankBranch
            btnEdit.Enabled = User._Object.Privilege._EditBankBranch
            btnDelete.Enabled = User._Object.Privilege._DeleteBankBranch

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetVisibility", mstrModuleName)
        End Try

    End Sub

    'Shani(25-Sep-2015) -- Start
    'ENHANCEMENT : Add Option to Import Bank Branch Master on Common Importation(Issue No : 455)
    Private Sub FillBankGroup()
        Try
            Dim objGroupMaster As New clspayrollgroup_master
            Dim dsGroup As DataSet = objGroupMaster.getListForCombo(CInt(PayrollGroupType.Bank), "Bank Group", True)
            cboBankGroup.ValueMember = "groupmasterunkid"
            cboBankGroup.DisplayMember = "name"
            cboBankGroup.DataSource = dsGroup.Tables("Bank Group")
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        End Try
    End Sub
    'Shani(25-Sep-2015) -- End

#End Region



    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Private Sub OtherSettings()
        Try
            Me.SuspendLayout()
           
            Call SetLanguage()

            Me.gbFilterCriteria.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbFilterCriteria.ForeColor = GUI._eZeeContainerHeaderForeColor


            Me.eZeeHeader.GradientColor1 = GUI._HeaderBackColor1
            Me.eZeeHeader.GradientColor2 = GUI._HeaderBackColor2
            Me.eZeeHeader.BorderColor = GUI._HeaderBorderColor
            Me.eZeeHeader.HeaderTextForeColor = GUI._HeaderTitleFontColor
            Me.eZeeHeader.DescriptionForeColor = GUI._HeaderMessageFontColor


            Me.btnDelete.GradientBackColor = GUI._ButttonBackColor
            Me.btnDelete.GradientForeColor = GUI._ButttonFontColor

            Me.btnEdit.GradientBackColor = GUI._ButttonBackColor
            Me.btnEdit.GradientForeColor = GUI._ButttonFontColor

            Me.btnNew.GradientBackColor = GUI._ButttonBackColor
            Me.btnNew.GradientForeColor = GUI._ButttonFontColor

            Me.btnClose.GradientBackColor = GUI._ButttonBackColor
            Me.btnClose.GradientForeColor = GUI._ButttonFontColor


            Me.ResumeLayout()
        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "OtherSettings", mstrModuleName)
        End Try
    End Sub


    Private Sub SetLanguage()
        Try
            Me.Text = Language._Object.getCaption(Me.Name, Me.Text)

            Me.colhBankgroup.Text = Language._Object.getCaption(CStr(Me.colhBankgroup.Tag), Me.colhBankgroup.Text)
            Me.colhBankcode.Text = Language._Object.getCaption(CStr(Me.colhBankcode.Tag), Me.colhBankcode.Text)
            Me.colhBranchName.Text = Language._Object.getCaption(CStr(Me.colhBranchName.Tag), Me.colhBranchName.Text)
            Me.colhBankAddress.Text = Language._Object.getCaption(CStr(Me.colhBankAddress.Tag), Me.colhBankAddress.Text)
            Me.eZeeHeader.Title = Language._Object.getCaption(Me.eZeeHeader.Name & "_Title", Me.eZeeHeader.Title)
            Me.eZeeHeader.Message = Language._Object.getCaption(Me.eZeeHeader.Name & "_Message", Me.eZeeHeader.Message)
            Me.btnDelete.Text = Language._Object.getCaption(Me.btnDelete.Name, Me.btnDelete.Text)
            Me.btnEdit.Text = Language._Object.getCaption(Me.btnEdit.Name, Me.btnEdit.Text)
            Me.btnNew.Text = Language._Object.getCaption(Me.btnNew.Name, Me.btnNew.Text)
            Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
            Me.colhBankState.Text = Language._Object.getCaption(CStr(Me.colhBankState.Tag), Me.colhBankState.Text)
            Me.colhBankPincode.Text = Language._Object.getCaption(CStr(Me.colhBankPincode.Tag), Me.colhBankPincode.Text)
            Me.gbFilterCriteria.Text = Language._Object.getCaption(Me.gbFilterCriteria.Name, Me.gbFilterCriteria.Text)
            Me.lblBankGroup.Text = Language._Object.getCaption(Me.lblBankGroup.Name, Me.lblBankGroup.Text)
            Me.colhSortCode.Text = Language._Object.getCaption(CStr(Me.colhSortCode.Tag), Me.colhSortCode.Text)

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
        End Try
    End Sub


    Private Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Please select Bank Branch from the list to perform further operation.")
            Language.setMessage(mstrModuleName, 2, "Sorry, You cannot delete this Bank Branch. Reason: This Bank Branch is in use.")
            Language.setMessage(mstrModuleName, 3, "Are you sure you want to delete this Bank Branch?")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class