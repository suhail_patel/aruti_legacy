﻿Option Strict On

#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data
Imports Microsoft.SqlServer


#End Region

Public Class frmFullBackUp

#Region " Private Variables "

    Private ReadOnly mstrModuleName As String = "frmFullBackUp"
    'Dim objServer As Server
    Private objCompany As clsCompany_Master
    Dim dtFinalTable As DataTable
    Private WithEvents objBackupDatabase As New eZeeDatabase


    'Pinkal (04-Apr-2013) -- Start
    'Enhancement : TRA Changes
    Dim dctPath As New ArrayList
    'Pinkal (04-Apr-2013) -- End

#End Region

#Region " Form's Events "

    Private Sub frmFullBackUp_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles Me.KeyPress
        If Asc(e.KeyChar) = 13 Then
            Windows.Forms.SendKeys.Send("{Tab}")
            e.Handled = True
        ElseIf Asc(e.KeyChar) = 27 Then
            Call btnCancel.PerformClick()
        End If
    End Sub

    Private Sub frmFullBackUp_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        objCompany = New clsCompany_Master
        Try
            Call Set_Logo(Me, gApplicationType)
            'Anjan (02 Sep 2011)-Start
            'Issue : Including Language Settings.
            Language.setLanguage(Me.Name)
            Call OtherSettings()
            'Anjan (02 Sep 2011)-End 
            Call FillGird()
            Call SetColor()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmFullBackUp_Load", mstrModuleName)
        End Try
    End Sub
    'Anjan (02 Sep 2011)-Start
    'Issue : Including Language Settings.
    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub
    'Anjan (02 Sep 2011)-End 
#End Region

#Region " Private Function "

    Private Sub FillGird()
        Try
            dtFinalTable = objCompany.Get_DataBaseList("DataList")

            dgvBackup.AutoGenerateColumns = False

            dgcolhName.DataPropertyName = "Database"
            dgcolhPath.DataPropertyName = "Path"
            objdgcolhCheck.DataPropertyName = "IsCheck"
            objdgcolhGrpId.DataPropertyName = "GrpId"
            objdgcolhIsGrp.DataPropertyName = "IsGrp"

            dgvBackup.DataSource = dtFinalTable


            'Pinkal (04-Apr-2013) -- Start
            'Enhancement : TRA Changes
            Dim drRow() As DataRow = dtFinalTable.Select("Database = 'arutiimages'")
            If drRow.Length > 0 Then
                dgvBackup.Rows(0).Height = 0    'arutiimages Database
                dgvBackup.Rows(1).Height = 0    'hrmsConfiguration Database
            Else
                dgvBackup.Rows(0).Height = 0    'hrmsConfiguration Database
            End If
            'Pinkal (04-Apr-2013) -- End


        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillGird", mstrModuleName)
        End Try
    End Sub

    Private Sub SetColor()
        Try
            Dim dgvcsHeader As New DataGridViewCellStyle
            dgvcsHeader.ForeColor = Color.White
            dgvcsHeader.SelectionBackColor = Color.Gray
            dgvcsHeader.BackColor = Color.Gray

            Dim dgvcsChild As New DataGridViewCellStyle
            dgvcsChild.ForeColor = Color.Black
            dgvcsChild.SelectionForeColor = Color.Black
            dgvcsChild.SelectionBackColor = Color.White
            dgvcsChild.BackColor = Color.White

            For Each dgvRow As DataGridViewRow In dgvBackup.Rows
                If CBool(dgvRow.Cells(objdgcolhIsGrp.Index).Value) = True Then
                    dgvRow.DefaultCellStyle = dgvcsHeader
                Else
                    dgvRow.DefaultCellStyle = dgvcsChild
                End If
                If CStr(dgvRow.Cells(dgcolhName.Index).Value) = "" Then
                    dgvRow.Cells(objdgcolhCheck.Index).Value = False
                    dgvRow.Cells(objdgcolhCheck.Index).ReadOnly = True
                ElseIf CStr(dgvRow.Cells(dgcolhName.Index).Value) = "hrmsConfiguration" Then
                    dgvRow.Cells(objdgcolhCheck.Index).Value = True
                    dgvRow.Cells(objdgcolhCheck.Index).ReadOnly = True


                    'Pinkal (04-Apr-2013) -- Start
                    'Enhancement : TRA Changes
                ElseIf CStr(dgvRow.Cells(dgcolhName.Index).Value) = "arutiimages" Then
                    dgvRow.Cells(objdgcolhCheck.Index).Value = True
                    dgvRow.Cells(objdgcolhCheck.Index).ReadOnly = True
                    'Pinkal (04-Apr-2013) -- End


                End If

            Next
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetColor", mstrModuleName)
        End Try
    End Sub

    Private Sub Check_ChildRow(ByVal intGrpValue As Integer, ByVal blnOperation As Boolean)
        For Each dgvRow As DataGridViewRow In dgvBackup.Rows
            If CInt(dgvRow.Cells(objdgcolhGrpId.Index).Value) = intGrpValue Then
                dgvRow.Cells(objdgcolhCheck.Index).Value = blnOperation
            End If
        Next
    End Sub

    Private Function Is_Valid() As Boolean
        Dim blnBlankPath As Boolean = False
        Try
            Dim dtTemp() As DataRow = dtFinalTable.Select("IsCheck = " & True)
            If dtTemp.Length <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please check atleast one company or database to take backup."))
                Return False
            End If

            For Each dgvRow As DataGridViewRow In dgvBackup.Rows
                If CBool(dgvRow.Cells(objdgcolhCheck.Index).Value) = True Then
                    If CBool(dgvRow.Cells(objdgcolhIsGrp.Index).Value) = False Then
                        If CStr(dgvRow.Cells(dgcolhPath.Index).Value) = "" Then
                            dgvRow.Cells(dgcolhPath.Index).Style.BackColor = Color.Red
                            dgvRow.Cells(dgcolhName.Index).Style.BackColor = Color.Red
                            blnBlankPath = True
                        Else
                            blnBlankPath = False
                        End If
                    End If
                Else
                    If CBool(dgvRow.Cells(objdgcolhIsGrp.Index).Value) = False Then
                        dgvRow.Cells(dgcolhPath.Index).Style.BackColor = Color.White
                        dgvRow.Cells(dgcolhName.Index).Style.BackColor = Color.White
                    End If
                End If
            Next

            If blnBlankPath = True Then
                lblMessage.Text = Language.getMessage(mstrModuleName, 2, "NOTE : From the selected database(s) does " & vbCrLf & _
                                                      "not have set the backup path to take backup. " & vbCrLf & _
                                                      "Backup of that database(s) cannot be taken." & vbCrLf & _
                                                      "You can set the Database backup path from " & vbCrLf & _
                                                      "Aruti Configuration -> Option -> Path.")
                lblMessage.ForeColor = Color.Red
                Exit Function
            Else
                lblMessage.Text = ""
            End If

            If lblMessage.Text.Trim = "" Then
                blnBlankPath = True
            Else
                blnBlankPath = False
            End If

            Return blnBlankPath

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Is_Valid", mstrModuleName)
        End Try
    End Function


    'Pinkal (04-Apr-2013) -- Start
    'Enhancement : TRA Changes

    Private Function GetConfigBackUp(ByVal intCompanyId As Integer) As Boolean
        Try
            Dim strFinalPath As String = ""
            Dim objBackup As New clsBackup
            eZeeDatabase.change_database("hrmsConfiguration")

            Dim objConfig As New clsConfigOptions
            objConfig._Companyunkid = intCompanyId

            If dctPath Is Nothing Then
                dctPath = New ArrayList
                dctPath.Add(objConfig._ConfigDatabaseExportPath.Trim)
            Else
                If dctPath.Contains(objConfig._ConfigDatabaseExportPath.Trim) Then
                    Return True
                Else
                    dctPath.Add(objConfig._ConfigDatabaseExportPath.Trim)
                End If
            End If



            Dim StrFilePath As String = ""

            If objConfig._ConfigDatabaseExportPath.ToString().Trim().Length > 0 Then
                StrFilePath = objConfig._ConfigDatabaseExportPath.ToString().Trim()
            Else
                Dim objAppSettings As New clsApplicationSettings
                StrFilePath = objAppSettings._ApplicationPath & "Data\Backup"
                objAppSettings = Nothing
            End If


            If Not System.IO.Directory.Exists(StrFilePath) Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Backup path ") & _
                                StrFilePath.ToString & _
                                Language.getMessage(mstrModuleName, 4, " is invalid. Please select valid path in order to perform Database Backup."))
                Return False
            End If
            System.IO.Directory.CreateDirectory(StrFilePath & "\" & "hrmsConfiguration")
            strFinalPath = StrFilePath & "\" & "hrmsConfiguration"


            Me.Cursor = Cursors.WaitCursor

            'S.SANDEEP [16 FEB 2015] -- START
            'strFinalPath = objBackupDatabase.Backup(strFinalPath)
            objlblDatabaseName.Text = "" : pbProgress.Value = 0
            objlblDatabaseName.Text = Language.getMessage(mstrModuleName, 6, "Backing Up Database :") & " " & "hrmsConfiguration"
            strFinalPath = objBackupDatabase.Backup(strFinalPath, General_Settings._Object._ServerName)
            objlblDatabaseName.Text = "" : pbProgress.Value = 0
            'S.SANDEEP [16 FEB 2015] -- END

            If System.IO.File.Exists(strFinalPath) Then
                StrFilePath = System.IO.Path.GetFullPath(strFinalPath)

                objBackup._Backup_Date = ConfigParameter._Object._CurrentDateAndTime
                objBackup._Backup_Path = strFinalPath
                objBackup._Companyunkid = -1
                objBackup._Isconfiguration = True
                objBackup._Userunkid = User._Object._Userunkid
                objBackup._Yearunkid = -1

                Call objBackup.Insert()
            End If


            '=======================================================> IMAGE DATABASE BACKUP ==> arutiimages

            objConfig._Companyunkid = intCompanyId

            If objBackup.Is_DatabasePresent("arutiimages") Then

                If objConfig._ConfigDatabaseExportPath.ToString().Trim().Length > 0 Then
                    StrFilePath = objConfig._ConfigDatabaseExportPath.ToString().Trim()
                Else
                    Dim objAppSettings As New clsApplicationSettings
                    StrFilePath = objAppSettings._ApplicationPath & "Data\Backup"
                    objAppSettings = Nothing
                End If

                If Not System.IO.Directory.Exists(StrFilePath) Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Backup path ") & _
                                    StrFilePath.ToString & _
                                    Language.getMessage(mstrModuleName, 4, " is invalid. Please select valid path in order to perform Database Backup."))
                    Return False
                End If

                eZeeDatabase.change_database("arutiimages")

                System.IO.Directory.CreateDirectory(StrFilePath & "\" & "arutiimages")
                strFinalPath = StrFilePath & "\" & "arutiimages"

                Me.Cursor = Cursors.WaitCursor
                'S.SANDEEP [16 FEB 2015] -- START
                'strFinalPath = objBackupDatabase.Backup(strFinalPath)
                objlblDatabaseName.Text = "" : pbProgress.Value = 0
                objlblDatabaseName.Text = Language.getMessage(mstrModuleName, 6, "Backing Up Database :") & " " & "arutiimages"
                strFinalPath = objBackupDatabase.Backup(strFinalPath, General_Settings._Object._ServerName)
                objlblDatabaseName.Text = "" : pbProgress.Value = 0
                'S.SANDEEP [16 FEB 2015] -- END

                If System.IO.File.Exists(strFinalPath) Then
                    StrFilePath = System.IO.Path.GetFullPath(strFinalPath)

                    objBackup._Backup_Date = ConfigParameter._Object._CurrentDateAndTime
                    objBackup._Backup_Path = strFinalPath
                    objBackup._Companyunkid = -1
                    objBackup._Isconfiguration = True
                    objBackup._Userunkid = User._Object._Userunkid
                    objBackup._Yearunkid = -1

                    Call objBackup.Insert()
                End If
                dtFinalTable.AcceptChanges()
                strFinalPath = ""

            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetConfigBackUp", mstrModuleName)
            Return False
        End Try
        Return True
    End Function

    'Pinkal (04-Apr-2013) -- End

#End Region

#Region " DataGrid Event(s) "

    Private Sub dgvBackup_CellContentClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvBackup.CellContentClick
        Try
            If e.RowIndex = -1 Then Exit Sub
            If CStr(dgvBackup.Rows(e.RowIndex).Cells(dgcolhName.Index).Value) = "" Or _
               CStr(dgvBackup.Rows(e.RowIndex).Cells(dgcolhName.Index).Value) = "hrmsConfiguration" Then
                Exit Sub
            End If
            If e.ColumnIndex = 0 Then
                Dim blnOldValue As Boolean = CBool(dgvBackup.Rows(e.RowIndex).Cells(objdgcolhCheck.Index).Value)
                If CBool(dgvBackup.Rows(e.RowIndex).Cells(objdgcolhIsGrp.Index).Value) = True Then
                    If blnOldValue = True Then
                        blnOldValue = False
                        dgvBackup.Rows(e.RowIndex).Cells(objdgcolhCheck.Index).Value = blnOldValue
                        dgvBackup.RefreshEdit()
                        Call Check_ChildRow(CInt(dgvBackup.Rows(e.RowIndex).Cells(objdgcolhGrpId.Index).Value), blnOldValue)
                    ElseIf blnOldValue = False Then
                        blnOldValue = True
                        dgvBackup.Rows(e.RowIndex).Cells(objdgcolhCheck.Index).Value = blnOldValue
                        dgvBackup.RefreshEdit()
                        Call Check_ChildRow(CInt(dgvBackup.Rows(e.RowIndex).Cells(objdgcolhGrpId.Index).Value), blnOldValue)
                    End If
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgvBackup_CellClick", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Buttons "

    Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnCancel_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnBackup_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBackup.Click
        Dim strFinalPath As String = String.Empty
        Dim objBackup As New clsBackup
        Try
            
            If Is_Valid() = False Then Exit Sub

            'S.SANDEEP [16 FEB 2015] -- START
            Me.Enabled = False
            objlblDatabaseName.Text = "" : pbProgress.Value = 0
            'S.SANDEEP [16 FEB 2015] -- END

            Dim dtFilter As DataTable = Nothing


            'Pinkal (04-Apr-2013) -- Start
            'Enhancement : TRA Changes


            'Dim drRow() As DataRow = dtFinalTable.Select("Ischeck = True AND YearId > 0 AND GrpId > 0")

            'If drRow.Length > 0 Then

            '    Dim dctPath As New ArrayList

            '    For i As Integer = 0 To drRow.Length - 1

            '        '=======================================================> MAIN DATABASE BACKUP ==> hrmsConfiguration

            '        'Pinkal (24-Jan-2013) -- Start
            '        'Enhancement : TRA Changes

            '        ' Dim objAppSettings As New clsApplicationSettings
            '        'Dim StrFilePath As String = objAppSettings._ApplicationPath & "Data\Backup"
            '        'objAppSettings = Nothing


            '        'Pinkal (04-Apr-2013) -- Start
            '        'Enhancement : TRA Changes
            '        eZeeDatabase.change_database("hrmsConfiguration")
            '        'Pinkal (04-Apr-2013) -- End


            '        Dim objConfig As New clsConfigOptions
            '        objConfig._Companyunkid = CInt(drRow(i)("GrpId"))

            '        If dctPath.Contains(objConfig._ConfigDatabaseExportPath.Trim) Then
            '            Continue For
            '        Else
            '            dctPath.Add(objConfig._ConfigDatabaseExportPath.Trim)
            '        End If

            '        Dim StrFilePath As String = ""

            '        If objConfig._ConfigDatabaseExportPath.ToString().Trim().Length > 0 Then
            '            StrFilePath = objConfig._ConfigDatabaseExportPath.ToString().Trim()
            '        Else
            '            Dim objAppSettings As New clsApplicationSettings
            '            StrFilePath = objAppSettings._ApplicationPath & "Data\Backup"
            '            objAppSettings = Nothing
            '        End If

            '        'Pinkal (24-Jan-2013) -- End


            '        If Not System.IO.Directory.Exists(StrFilePath) Then
            '            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Backup path ") & _
            '                            StrFilePath.ToString & _
            '                            Language.getMessage(mstrModuleName, 4, " is invalid. Please select valid path in order to perform Database Backup."))
            '            Exit Sub
            '        End If
            '        System.IO.Directory.CreateDirectory(StrFilePath & "\" & "hrmsConfiguration")
            '        strFinalPath = StrFilePath & "\" & "hrmsConfiguration"


            '        If System.IO.File.Exists(strFinalPath) Then Continue For

            '        Me.Cursor = Cursors.WaitCursor
            '        strFinalPath = objBackupDatabase.Backup(strFinalPath)

            '        If System.IO.File.Exists(strFinalPath) Then
            '            StrFilePath = System.IO.Path.GetFullPath(strFinalPath)

            '            objBackup._Backup_Date = ConfigParameter._Object._CurrentDateAndTime
            '            objBackup._Backup_Path = strFinalPath
            '            objBackup._Companyunkid = -1
            '            objBackup._Isconfiguration = True
            '            objBackup._Userunkid = User._Object._Userunkid
            '            objBackup._Yearunkid = -1

            '            Call objBackup.Insert()
            '        End If
            '        dtFinalTable.AcceptChanges()
            '        strFinalPath = ""

            '        'Pinkal (04-Apr-2013) -- Start
            '        'Enhancement : TRA Changes

            '        '=======================================================> IMAGE DATABASE BACKUP ==> arutiimages

            '        objConfig._Companyunkid = CInt(drRow(i)("GrpId"))

            '        If objConfig._ConfigDatabaseExportPath.ToString().Trim().Length > 0 Then
            '            StrFilePath = objConfig._ConfigDatabaseExportPath.ToString().Trim()
            '        Else
            ' Dim objAppSettings As New clsApplicationSettings
            '            StrFilePath = objAppSettings._ApplicationPath & "Data\Backup"
            'objAppSettings = Nothing
            '        End If

            '        If Not System.IO.Directory.Exists(StrFilePath) Then
            '            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Backup path ") & _
            '                            StrFilePath.ToString & _
            '                            Language.getMessage(mstrModuleName, 4, " is invalid. Please select valid path in order to perform Database Backup."))
            '            Exit Sub
            '        End If

            '        eZeeDatabase.change_database("arutiimages")

            '        System.IO.Directory.CreateDirectory(StrFilePath & "\" & "arutiimages")
            '        strFinalPath = StrFilePath & "\" & "arutiimages"

            '        Me.Cursor = Cursors.WaitCursor

            '        strFinalPath = objBackupDatabase.Backup(strFinalPath)

            '        If System.IO.File.Exists(strFinalPath) Then
            '            StrFilePath = System.IO.Path.GetFullPath(strFinalPath)

            '            objBackup._Backup_Date = ConfigParameter._Object._CurrentDateAndTime
            '            objBackup._Backup_Path = strFinalPath
            '            objBackup._Companyunkid = -1
            '            objBackup._Isconfiguration = True
            '            objBackup._Userunkid = User._Object._Userunkid
            '            objBackup._Yearunkid = -1

            '            Call objBackup.Insert()
            '        End If
            '        dtFinalTable.AcceptChanges()
            '        strFinalPath = ""
            '    Next

            'End If



            For Each dtRow As DataRow In dtFinalTable.Rows

                If CBool(dtRow.Item("IsCheck")) = False Or CInt(dtRow.Item("GrpId")) <= 0 Then Continue For

                '=======================================================> COMPANY DATABASE BACKUP ==> BASED ON USER SELECTION (COMPANY GROUP SELECTED)
                If CBool(dtRow.Item("IsGrp")) = True And CBool(dtRow.Item("IsTaken")) = False Then
                    Dim strBaseDirPath As String = String.Empty

                    If Not System.IO.Directory.Exists(dtRow.Item("GrpPath").ToString) Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Backup path ") & _
                                        dtRow.Item("GrpPath").ToString & _
                                        Language.getMessage(mstrModuleName, 4, " is invalid. Please select valid path in order to perform Database Backup."))

                        Exit Sub
                    End If

                    System.IO.Directory.CreateDirectory(dtRow.Item("GrpPath").ToString & "\" & dtRow.Item("Database").ToString.Trim)
                    strBaseDirPath = dtRow.Item("GrpPath").ToString & "\" & dtRow.Item("Database").ToString.Trim

                    dtFilter = New DataView(dtFinalTable, "GrpId = '" & CInt(dtRow.Item("GrpId")) & "' AND IsGrp = 0", "", DataViewRowState.CurrentRows).ToTable

                    If dtFilter.Rows.Count > 0 Then


                        'Pinkal (04-Apr-2013) -- Start
                        'Enhancement : TRA Changes

                        If GetConfigBackUp(CInt(dtRow.Item("GrpId"))) = False Then Exit Sub

                        'Pinkal (04-Apr-2013) -- End



                        For Each dcRow As DataRow In dtFilter.Rows
                            Dim strOtherPath = ""
                            System.IO.Directory.CreateDirectory(strBaseDirPath.ToString & "\" & dcRow.Item("Database").ToString.Trim)
                            strOtherPath = strBaseDirPath.ToString & "\" & dcRow.Item("Database").ToString.Trim


                            eZeeDatabase.change_database(dcRow.Item("Database").ToString.Trim)

                            'S.SANDEEP [16 FEB 2015] -- START
                            'strOtherPath = objBackupDatabase.Backup(strOtherPath)
                            objlblDatabaseName.Text = "" : pbProgress.Value = 0
                            objlblDatabaseName.Text = Language.getMessage(mstrModuleName, 6, "Backing Up Database :") & " " & dcRow.Item("Database").ToString.Trim
                            strOtherPath = objBackupDatabase.Backup(strOtherPath, General_Settings._Object._ServerName)
                            objlblDatabaseName.Text = "" : pbProgress.Value = 0
                            'S.SANDEEP [16 FEB 2015] -- END



                            If System.IO.File.Exists(strOtherPath) Then
                                strOtherPath = System.IO.Path.GetFullPath(strOtherPath)

                                objBackup._Backup_Date = ConfigParameter._Object._CurrentDateAndTime
                                objBackup._Backup_Path = strOtherPath
                                objBackup._Companyunkid = CInt(dcRow.Item("GrpId"))
                                objBackup._Isconfiguration = False
                                objBackup._Userunkid = User._Object._Userunkid
                                objBackup._Yearunkid = CInt(dcRow.Item("YearId"))

                                Call objBackup.Insert()
                            End If

                            dtRow.Item("IsTaken") = True
                            Dim dtTemp() As DataRow = dtFinalTable.Select("YearId ='" & CInt(dcRow.Item("YearId")) & "'")
                            If dtTemp.Length > 0 Then
                                Dim idx As Integer = dtFinalTable.Rows.IndexOf(dtTemp(0))
                                dtFinalTable.Rows(idx).Item("IsTaken") = True
                            End If
                            dtFinalTable.AcceptChanges()

                            'S.SANDEEP [16 FEB 2015] -- START
                            Call SetColor()
                            dgvBackup.Rows(0).Height = 0
                            'S.SANDEEP [16 FEB 2015] -- END
                        Next
                    End If
                Else
                    '=======================================================> COMPANY DATABASE BACKUP ==> BASED ON USER SELECTION (COMPANY GROUP NOT SELECTED)
                    If CBool(dtRow.Item("IsGrp")) = False And CBool(dtRow.Item("IsTaken")) = False Then
                        Dim dtTemp() As DataRow = dtFinalTable.Select("GrpId = '" & CInt(dtRow.Item("GrpId")) & "' AND IsGrp = 1")
                        If dtTemp.Length > 0 Then

                            If Not System.IO.Directory.Exists(dtTemp(0).Item("GrpPath").ToString) Then
                                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Backup path ") & _
                                                dtRow.Item("GrpPath").ToString & _
                                                Language.getMessage(mstrModuleName, 4, " is invalid. Please select valid path in order to perform Database Backup."))
                                Exit Sub
                            End If


                            'Pinkal (04-Apr-2013) -- Start
                            'Enhancement : TRA Changes

                            If GetConfigBackUp(CInt(dtTemp(0).Item("GrpId"))) = False Then Exit Sub

                            'Pinkal (04-Apr-2013) -- End

                            System.IO.Directory.CreateDirectory(dtTemp(0).Item("GrpPath").ToString & "\" & dtTemp(0).Item("Database").ToString)
                            strFinalPath = dtTemp(0).Item("GrpPath").ToString & "\" & dtTemp(0).Item("Database").ToString

                            Dim strOtherPath = ""
                            System.IO.Directory.CreateDirectory(strFinalPath.ToString & "\" & dtRow.Item("Database").ToString.Trim)
                            strOtherPath = strFinalPath.ToString & "\" & dtRow.Item("Database").ToString.Trim

                            eZeeDatabase.change_database(dtRow.Item("Database").ToString.Trim)


                            'S.SANDEEP [16 FEB 2015] -- START
                            'strOtherPath = objBackupDatabase.Backup(strOtherPath)
                            objlblDatabaseName.Text = "" : pbProgress.Value = 0
                            objlblDatabaseName.Text = Language.getMessage(mstrModuleName, 6, "Backing Up Database :") & " " & dtRow.Item("Database").ToString.Trim
                            strOtherPath = objBackupDatabase.Backup(strOtherPath, General_Settings._Object._ServerName)
                            objlblDatabaseName.Text = "" : pbProgress.Value = 0
                            'S.SANDEEP [16 FEB 2015] -- END

                            If System.IO.File.Exists(strOtherPath) Then
                                strOtherPath = System.IO.Path.GetFullPath(strOtherPath)

                                objBackup._Backup_Date = ConfigParameter._Object._CurrentDateAndTime
                                objBackup._Backup_Path = strOtherPath
                                objBackup._Companyunkid = CInt(dtTemp(0).Item("GrpId"))
                                objBackup._Isconfiguration = True
                                objBackup._Userunkid = User._Object._Userunkid
                                objBackup._Yearunkid = CInt(dtRow.Item("YearId"))

                                Call objBackup.Insert()
                            End If
                            dtRow.Item("IsTaken") = True
                            dtFinalTable.AcceptChanges()

                            'S.SANDEEP [16 FEB 2015] -- START
                            Call SetColor()
                            dgvBackup.Rows(0).Height = 0
                            'S.SANDEEP [16 FEB 2015] -- END

                        End If
                    End If
                End If
            Next

            eZeeDatabase.change_database("hrmsConfiguration")
            Me.Cursor = Cursors.Default
            'Call SetColor()
            'dgvBackup.Rows(0).Height = 0

            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "Backup has been taken successfully."), enMsgBoxStyle.Information)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnBackup_Click", mstrModuleName)
        Finally

            'Pinkal (04-Apr-2013) -- Start
            'Enhancement : TRA Changes
            dctPath = Nothing
            'Pinkal (04-Apr-2013) -- End
            Me.Cursor = Cursors.Default
            'S.SANDEEP [16 FEB 2015] -- START
            Me.Enabled = True
            objlblDatabaseName.Text = "" : pbProgress.Value = 0
            'S.SANDEEP [16 FEB 2015] -- END
        End Try
    End Sub

#End Region

    'S.SANDEEP [16 FEB 2015] -- START
#Region " Controls Events "

    Private Sub objDatabase_Progress(ByVal sender As Object, ByVal e As System.Data.SqlClient.SqlInfoMessageEventArgs) Handles objBackupDatabase.Progress
        Try
            Application.DoEvents()
            For Each info As SqlClient.SqlError In e.Errors
                If IsNumeric(info.Message.Split(CChar(" "))(0)) Then
                    pbProgress.Value = CInt(info.Message.Split(CChar(" "))(0))
                End If
            Next
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objDatabase_Progress", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region
    'S.SANDEEP [16 FEB 2015] -- END


	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Private Sub OtherSettings()
		Try
			Me.SuspendLayout()
			
			Call SetLanguage()
			
			Me.btnBackup.GradientBackColor = GUI._ButttonBackColor 
			Me.btnBackup.GradientForeColor = GUI._ButttonFontColor

			Me.btnCancel.GradientBackColor = GUI._ButttonBackColor 
			Me.btnCancel.GradientForeColor = GUI._ButttonFontColor


			Me.ResumeLayout()
		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "OtherSettings", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetLanguage()
		Try
			Me.Text = Language._Object.getCaption(Me.Name, Me.Text)
			
			Me.btnBackup.Text = Language._Object.getCaption(Me.btnBackup.Name, Me.btnBackup.Text)
			Me.btnCancel.Text = Language._Object.getCaption(Me.btnCancel.Name, Me.btnCancel.Text)
			Me.dgcolhName.HeaderText = Language._Object.getCaption(Me.dgcolhName.Name, Me.dgcolhName.HeaderText)
			Me.dgcolhPath.HeaderText = Language._Object.getCaption(Me.dgcolhPath.Name, Me.dgcolhPath.HeaderText)
			Me.lblMessage.Text = Language._Object.getCaption(Me.lblMessage.Name, Me.lblMessage.Text)

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetLanguage", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "Please check atleast one company or database to take backup.")
			Language.setMessage(mstrModuleName, 2, "NOTE : From the selected database(s")
			Language.setMessage(mstrModuleName, 3, "Backup path")
			Language.setMessage(mstrModuleName, 4, " is invalid. Please select valid path in order to perform Database Backup.")
			Language.setMessage(mstrModuleName, 5, "Backup has been taken successfully.")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class