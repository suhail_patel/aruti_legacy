﻿Option Strict On

Imports eZeeCommonLib
Imports Aruti.Data


Public Class frmUserAttemptsLog

#Region " Private Variables "
    Private ReadOnly mstrModuleName As String = "frmUserAttemptsLog"
    Private mblnCancel As Boolean = True
    Private menAction As enAction = enAction.ADD_ONE
    Dim dsList As DataSet = Nothing
#End Region

#Region " Private Methods "

    Private Sub FillCombo()
        Try

            With cboAttemptType
                .Items.Clear()
                .Items.Add(Language.getMessage(mstrModuleName, 1, "Select"))
                .Items.Add(Language.getMessage(mstrModuleName, 2, "Successful"))
                .Items.Add(Language.getMessage(mstrModuleName, 3, "Unsuccessful"))
                .SelectedIndex = 0
            End With

            With cboViewBy
                .Items.Clear()
                .Items.Add(Language.getMessage(mstrModuleName, 1, "Select"))
                .Items.Add(Language.getMessage(mstrModuleName, 4, "DeskTop"))
                Select Case gApplicationType
                    Case enArutiApplicatinType.Aruti_Payroll
                        .Items.Add(Language.getMessage(mstrModuleName, 5, "Employee Self Service"))
                        .Items.Add(Language.getMessage(mstrModuleName, 6, "Manager Self Service"))
                End Select
                .SelectedIndex = 0
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        End Try
    End Sub

    Private Sub FillList()
        Dim StrSearching As String = String.Empty
        Dim dTable As DataTable = Nothing
        Try
            Dim objUserLog As New clsUser_tracking_Tran
            'dsList = objUserLog.GetUserAttempts(gApplicationType, cboAttemptType.SelectedIndex, cboViewBy.SelectedIndex, CInt(cboFilter.SelectedValue))
            dsList = objUserLog.GetUserAttempts(gApplicationType)

            If cboAttemptType.SelectedIndex > 0 Then
                StrSearching &= "AND Attempt_TypeId = " & cboAttemptType.SelectedIndex
            End If

            If cboViewBy.SelectedIndex > 0 Then
                StrSearching &= "AND login_modeid = " & cboViewBy.SelectedIndex
            End If

            If CInt(cboFilter.SelectedValue) > 0 Then
                StrSearching &= "AND userunkid = " & CInt(cboFilter.SelectedValue)
            End If

            If StrSearching.Trim.Length > 0 Then
                StrSearching = StrSearching.Substring(3)
                dTable = New DataView(dsList.Tables(0), StrSearching, "", DataViewRowState.CurrentRows).ToTable
            Else
                dTable = New DataView(dsList.Tables(0), "", "", DataViewRowState.CurrentRows).ToTable
            End If

            lvUserLog.Items.Clear()
            If dsList IsNot Nothing AndAlso dTable.Rows.Count > 0 Then
                Dim lvitem As ListViewItem = Nothing
                For Each drrow As DataRow In dTable.Rows
                    lvitem = New ListViewItem
                    lvitem.Text = drrow("User").ToString
                    lvitem.SubItems.Add(drrow("IP").ToString)
                    lvitem.SubItems.Add(drrow("Machine").ToString)
                    lvitem.SubItems.Add(drrow("Attempts").ToString)
                    lvitem.SubItems.Add(drrow.Item("Attempt_Type").ToString)
                    lvitem.SubItems.Add(drrow.Item("Mode").ToString)
                    'S.SANDEEP [ 09 MAR 2013 ] -- START
                    'ENHANCEMENT : TRA CHANGES
                    If drrow.Item("attempt_date").ToString.Trim.Length > 0 Then
                        lvitem.SubItems.Add(eZeeDate.convertDate(drrow.Item("attempt_date").ToString).ToShortDateString)
                    Else
                        lvitem.SubItems.Add("")
                    End If
                    'S.SANDEEP [ 09 MAR 2013 ] -- END
                    lvUserLog.Items.Add(lvitem)
                Next
                lvUserLog.GroupingColumn = colhUserName
                lvUserLog.DisplayGroups(True)
                lvUserLog.GridLines = False

                If lvUserLog.Items.Count > 18 Then
                    colhLoginFrom.Width = 150 - 18
                Else
                    colhLoginFrom.Width = 150
                End If

            End If

            If lvUserLog.Items.Count > 0 Then
                btnExport.Enabled = True
            Else
                btnExport.Enabled = False
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillList", mstrModuleName)
        End Try
    End Sub

    'S.SANDEEP [ 19 JULY 2012 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Private Function Store_User_Log(ByVal blnIsSearch As Boolean) As Boolean
        Dim objViewAT As New clsView_AT_Logs
        Try
            objViewAT._Atviewmodeid = clsView_AT_Logs.enAT_View_Mode.USER_ATTEMPTS_LOG
            objViewAT._Userunkid = User._Object._Userunkid
            objViewAT._Viewdatetime = ConfigParameter._Object._CurrentDateAndTime
            If blnIsSearch = True Then
                objViewAT._Viewoperationid = clsView_AT_Logs.enAT_OperationId.SEARCH
            Else
                objViewAT._Viewoperationid = clsView_AT_Logs.enAT_OperationId.EXPORT
            End If

            Dim StrXML_Value As String = ""
            StrXML_Value = "<" & Me.Name & " xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"">" & vbCrLf

            For Each ctrl As Control In gbFilterCriteria.Controls
                If TypeOf ctrl Is ComboBox Then
                    If CType(ctrl, ComboBox).SelectedIndex >= 0 Then
                        If CType(ctrl, ComboBox).DataSource IsNot Nothing Then
                            StrXML_Value &= "<" & ctrl.Name & ">" & CInt(CType(ctrl, ComboBox).SelectedValue) & "</" & ctrl.Name & ">" & vbCrLf
                        Else
                            StrXML_Value &= "<" & ctrl.Name & ">" & CType(ctrl, ComboBox).SelectedIndex & "</" & ctrl.Name & ">" & vbCrLf
                        End If
                    Else
                        StrXML_Value &= "<" & ctrl.Name & "/>" & vbCrLf
                    End If
                End If
            Next
            StrXML_Value &= "</" & Me.Name & ">"

            objViewAT._Value_View = StrXML_Value

            If objViewAT.Insert = False Then
                eZeeMsgBox.Show(objViewAT._Message, enMsgBoxStyle.Information)
                Return False
            End If

            Return True

        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "Store_User_Log", mstrModuleName)
        Finally
            objViewAT = Nothing
        End Try
    End Function
    'S.SANDEEP [ 19 JULY 2012 ] -- END


#End Region

#Region " Form's Events "

    Private Sub frmUserAttemptsLog_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            Call Set_Logo(Me, gApplicationType)

            'Pinkal (10-Jul-2014) -- Start
            'Enhancement - Language Settings
            Language.setLanguage(Me.Name)
            OtherSettings()
            'Pinkal (10-Jul-2014) -- End

            lvUserLog.GridLines = False
            btnExport.Enabled = False
            Call FillCombo()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmUserAttemptsLog_Load", mstrModuleName)
        End Try
    End Sub


    'Pinkal (10-Jul-2014) -- Start
    'Enhancement - Language Settings

    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()
            clsView_AT_Logs.SetMessages()
            objfrm._Other_ModuleNames = "clsView_AT_Logs"
            objfrm.displayDialog(Me)
            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub

    'Pinkal (10-Jul-2014) -- End


#End Region

#Region " Button's Events "

    Private Sub btnExport_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnExport.Click
        Try
            'S.SANDEEP [ 19 JULY 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            If Store_User_Log(False) = False Then Exit Sub
            'S.SANDEEP [ 19 JULY 2012 ] -- END

            Dim strBuilder As New System.Text.StringBuilder
            strBuilder.Append(" <HTML> " & vbCrLf)
            strBuilder.Append(" <BODY><FONT FACE = TAHOMA FONT SIZE=2> " & vbCrLf)
            strBuilder.Append(" <TABLE BORDER = 1 BORDERCOLOR = BLACK WIDTH='100%'> " & vbCrLf)

            strBuilder.Append(" <TR ALIGN = LEFT VALIGN=TOP WIDTH='100%'> " & vbCrLf)
            strBuilder.Append(" <TD BORDER=1 BORDERCOLOR=BLACK ALIGN='CENTER' COLSPAN='6'><FONT SIZE=2><B>" & Language.getMessage(mstrModuleName, 7, "USER LOGIN ATTEMPT LIST") & "</B></FONT></TD>" & vbCrLf)
            strBuilder.Append(" </TR> " & vbCrLf)


            If cboAttemptType.SelectedIndex > 0 Then
                strBuilder.Append(" <TR ALIGN = LEFT VALIGN=TOP WIDTH='100%'> " & vbCrLf)
                strBuilder.Append(" <TD BORDER=1 BORDERCOLOR=BLACK ALIGN='LEFT' WIDTH = '20%'><FONT SIZE=2><B>" & lblAttemptsType.Text.Trim & " : " & "</B></FONT></TD> " & vbCrLf)
                strBuilder.Append(" <TD BORDER=1 BORDERCOLOR=BLACK ALIGN='LEFT' WIDTH = '80%' COLSPAN='5'><FONT SIZE=2><B>" & cboAttemptType.Text.Trim & "</B></FONT></TD> " & vbCrLf)
                strBuilder.Append(" </TR> " & vbCrLf)
            End If

            If cboViewBy.SelectedIndex > 0 Then
                strBuilder.Append(" <TR ALIGN = LEFT VALIGN=TOP WIDTH='100%'> " & vbCrLf)
                strBuilder.Append(" <TD BORDER=1 BORDERCOLOR=BLACK ALIGN='LEFT' WIDTH = '20%'><FONT SIZE=2><B>" & lblViewBy.Text.Trim & " : " & "</B></FONT></TD> " & vbCrLf)
                strBuilder.Append(" <TD BORDER=1 BORDERCOLOR=BLACK ALIGN='LEFT' WIDTH = '80%' COLSPAN='5'><FONT SIZE=2><B>" & cboViewBy.Text.Trim & "</B></FONT></TD> " & vbCrLf)
                strBuilder.Append(" </TR> " & vbCrLf)
            End If

            If cboFilter.SelectedIndex > 0 Then
                strBuilder.Append(" <TR ALIGN = LEFT VALIGN=TOP WIDTH='100%'> " & vbCrLf)
                strBuilder.Append(" <TD BORDER=1 BORDERCOLOR=BLACK ALIGN='LEFT' WIDTH = '20%'><FONT SIZE=2><B>" & objlblCaption.Text.Trim & " : " & "</B></FONT></TD> " & vbCrLf)
                strBuilder.Append(" <TD BORDER=1 BORDERCOLOR=BLACK ALIGN='LEFT' WIDTH = '80%' COLSPAN='5'><FONT SIZE=2><B>" & cboFilter.Text.Trim & "</B></FONT></TD> " & vbCrLf)
                strBuilder.Append(" </TR> " & vbCrLf)
            End If

            strBuilder.Append(" </TABLE> " & vbCrLf)

            strBuilder.Append(" <BR/> " & vbCrLf)

            strBuilder.Append(" <TABLE BORDER = 1 BORDERCOLOR = BLACK WIDTH='100%'> " & vbCrLf)
            strBuilder.Append(" <TR ALIGN = LEFT VALIGN=TOP WIDTH='100%'> " & vbCrLf)

            For i As Integer = 0 To dsList.Tables(0).Columns.Count - 1
                If dsList.Tables(0).Columns(i).Caption.ToString() = "Attempt_Type" Then
                    dsList.Tables(0).Columns(i).Caption = dsList.Tables(0).Columns(i).Caption.Replace("_", " ")
                End If
                strBuilder.Append(" <TD BORDER=1 BORDERCOLOR=BLACK ALIGN='LEFT'><FONT SIZE=2><B>" & dsList.Tables(0).Columns(i).Caption.ToString & "</B></FONT></TD> " & vbCrLf)
            Next
            strBuilder.Append(" </TR> " & vbCrLf)

            strBuilder.Append(" <TR ALIGN = LEFT VALIGN=TOP WIDTH='100%'> " & vbCrLf)
            For j As Integer = 0 To dsList.Tables(0).Rows.Count - 1
                For i As Integer = 0 To dsList.Tables(0).Columns.Count - 1
                    strBuilder.Append(" <TD BORDER=1 BORDERCOLOR=BLACK ALIGN='LEFT'><FONT SIZE=2>" & dsList.Tables(0).Rows(j)(i).ToString & "</FONT></TD> " & vbCrLf)
                Next
                strBuilder.Append(" </TR> " & vbCrLf)
            Next

            strBuilder.Append(" </TABLE> " & vbCrLf)


            strBuilder.Append(" </BODY> " & vbCrLf)
            strBuilder.Append(" </HTML> " & vbCrLf)

            Dim svDialog As New SaveFileDialog
            svDialog.Filter = "Excel files (*.xlsx)|*.xlsx"
            If svDialog.ShowDialog = Windows.Forms.DialogResult.OK Then
                Dim fsFile As New IO.FileStream(svDialog.FileName, IO.FileMode.Create, IO.FileAccess.Write)
                Dim strWriter As New IO.StreamWriter(fsFile)
                With strWriter
                    .BaseStream.Seek(0, IO.SeekOrigin.End)
                    .WriteLine(strBuilder)
                    .Close()
                End With
                Diagnostics.Process.Start(svDialog.FileName)
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnExport_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnSearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSearch.Click
        Try
            'S.SANDEEP [ 19 JULY 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            If Store_User_Log(True) = False Then Exit Sub
            'S.SANDEEP [ 19 JULY 2012 ] -- END

            FillList()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnSearch_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnReset.Click
        Try
            cboAttemptType.SelectedIndex = 0
            cboViewBy.SelectedIndex = 0
            cboFilter.SelectedValue = 0
            lvUserLog.Items.Clear()
            dsList = Nothing
            cboAttemptType.Focus()
            btnExport.Enabled = False
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnReset_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnSearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearch.Click
        Dim frm As New frmCommonSearch
        Try
            With frm
                .ValueMember = cboFilter.ValueMember
                .DisplayMember = cboFilter.DisplayMember
                Select Case gApplicationType
                    Case enArutiApplicatinType.Aruti_Payroll
                        'S.SANDEEP [10 AUG 2015] -- START
                        'ENHANCEMENT : Aruti SaaS Changes
                        '.CodeMember = "employeecode"
                        If cboViewBy.SelectedIndex = 2 Then
                        .CodeMember = "employeecode"
                        End If
                        'S.SANDEEP [10 AUG 2015] -- END
                End Select
                .DataSource = CType(cboFilter.DataSource, DataTable)
            End With
            If frm.DisplayDialog Then
                cboFilter.SelectedValue = frm.SelectedValue
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearch_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Me.Close()
    End Sub

#End Region

#Region "Combobox Event"

    Private Sub cboViewBy_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboViewBy.SelectedIndexChanged
        Try
            Dim dsList As New DataSet
            Select Case cboViewBy.SelectedIndex
                Case 0  'NONE
                    objlblCaption.Visible = False
                    cboFilter.Visible = False
                    objbtnSearch.Visible = False
                    colhUserName.Text = Language.getMessage(mstrModuleName, 200, "User")
                    cboFilter.SelectedValue = 0
                Case 1, 3  'DESKTOP,MANAGER SELF SERVICE 
                    cboFilter.DataSource = Nothing
                    objlblCaption.Visible = True
                    cboFilter.Visible = True
                    objbtnSearch.Visible = True
                    objlblCaption.Text = Language.getMessage(mstrModuleName, 200, "User")
                    Dim objFUser As New clsUserAddEdit
                    'S.SANDEEP [10 AUG 2015] -- START
                    'ENHANCEMENT : Aruti SaaS Changes
                    'Dim objOption As New clsPassowdOptions
                    'Dim drOption As DataRow() = objOption._DataTable.Select("passwdoptionid = " & enPasswordOption.USER_LOGIN_MODE)
                    'If drOption.Length > 0 Then
                    '    If objOption._UserLogingModeId = enAuthenticationMode.BASIC_AUTHENTICATION Then
                    '        'S.SANDEEP [ 11 DEC 2012 ] -- START
                    '        'ENHANCEMENT : TRA CHANGES
                    '        'dsList = objFUser.getComboList("List", True, False)
                    '        Dim objPswd As New clsPassowdOptions
                    '        If objPswd._IsEmployeeAsUser Then
                    '            dsList = objFUser.getComboList("List", True, False, True)
                    '        Else
                    '        dsList = objFUser.getComboList("List", True, False)
                    '        End If
                    '        objPswd = Nothing
                    '        'S.SANDEEP [ 11 DEC 2012 ] -- END
                    '    ElseIf objOption._UserLogingModeId = enAuthenticationMode.AD_BASIC_AUTHENTICATION Then
                    '        dsList = objFUser.getComboList("List", True, True)
                    '    ElseIf objOption._UserLogingModeId = enAuthenticationMode.AD_SSO_AUTHENTICATION Then
                    '        dsList = objFUser.getComboList("List", True, True)
                    '    End If
                    'Else
                            'dsList = objFUser.getComboList("List", True, False)
                    'End If

                    'Nilay (01-Mar-2016) -- Start
                    'dsList = objFUser.getNewComboList("List", , True)
                    dsList = objFUser.getNewComboList("List", , True, , , , True)
                    'Nilay (01-Mar-2016) -- End

                    'S.SANDEEP [10 AUG 2015] -- END

                    objFUser = Nothing
                    With cboFilter
                        .ValueMember = "userunkid"
                        .DisplayMember = "name"
                    End With
                    cboFilter.DataSource = dsList.Tables("List")
                    cboFilter.SelectedValue = 0
                Case 2  'EMPLOYEE SELF SERVICE
                    cboFilter.DataSource = Nothing
                    objlblCaption.Visible = True
                    cboFilter.Visible = True
                    objbtnSearch.Visible = True
                    objlblCaption.Text = Language.getMessage(mstrModuleName, 201, "Employee")
                    Dim objEmp As New clsEmployee_Master

                    'S.SANDEEP [04 JUN 2015] -- START
                    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS

                    'If ConfigParameter._Object._IsIncludeInactiveEmp = False Then
                    '    dsList = objEmp.GetEmployeeList("List", True, ConfigParameter._Object._IsIncludeInactiveEmp, , , , , , , , , , , , CDate(eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate).ToShortDateString), CDate(eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate).ToShortDateString))
                    'Else
                    '    dsList = objEmp.GetEmployeeList("List", True, Not ConfigParameter._Object._IsIncludeInactiveEmp)
                    'End If

                    dsList = objEmp.GetEmployeeList(FinancialYear._Object._DatabaseName, _
                                                  User._Object._Userunkid, _
                                                  FinancialYear._Object._YearUnkid, _
                                                  Company._Object._Companyunkid, _
                                                  FinancialYear._Object._Database_Start_Date.Date, _
                                                  FinancialYear._Object._Database_End_Date.Date, _
                                                  ConfigParameter._Object._UserAccessModeSetting, _
                                                  True, ConfigParameter._Object._IsIncludeInactiveEmp, "List", True)
                    'S.SANDEEP [04 JUN 2015] -- END

                    With cboFilter
                        .ValueMember = "employeeunkid"
                        .DisplayMember = "employeename"
                    End With
                    cboFilter.DataSource = dsList.Tables("List")
                    cboFilter.SelectedValue = 0
            End Select
            'Dim dsList As New DataSet
            'Select Case cboViewBy.SelectedIndex
            '    Case 0, 1, 3  'DESKTOP,MANAGER SELF SERVICE 
            '        cboFilter.DataSource = Nothing
            '        objlblCaption.Visible = True
            '        cboFilter.Visible = True
            '        objbtnSearch.Visible = True
            '        objlblCaption.Text = Language.getMessage(mstrModuleName, 200, "User")
            '        Dim objFUser As New clsUserAddEdit
            '        Dim objOption As New clsPassowdOptions
            '        Dim drOption As DataRow() = objOption._DataTable.Select("passwdoptionid = " & enPasswordOption.USER_LOGIN_MODE)
            '        If drOption.Length > 0 Then
            '            If objOption._UserLogingModeId = enAuthenticationMode.BASIC_AUTHENTICATION Then
            '                dsList = objFUser.getComboList("List", True, False)
            '            ElseIf objOption._UserLogingModeId = enAuthenticationMode.AD_BASIC_AUTHENTICATION Then
            '                dsList = objFUser.getComboList("List", True, True)
            '            ElseIf objOption._UserLogingModeId = enAuthenticationMode.AD_SSO_AUTHENTICATION Then
            '                dsList = objFUser.getComboList("List", True, True)
            '            End If
            '        Else
            '            dsList = objFUser.getComboList("List", True, False)
            '        End If
            '        objFUser = Nothing
            '        With cboFilter
            '            .ValueMember = "userunkid"
            '            .DisplayMember = "name"
            '        End With
            '        cboFilter.DataSource = dsList.Tables("List")
            '        cboFilter.SelectedValue = 0
            '    Case 2  'EMPLOYEE SELF SERVICE
            '        cboFilter.DataSource = Nothing
            '        objlblCaption.Visible = True
            '        cboFilter.Visible = True
            '        objbtnSearch.Visible = True
            '        objlblCaption.Text = Language.getMessage(mstrModuleName, 201, "Employee")
            '        Dim objEmp As New clsEmployee_Master
            '        If ConfigParameter._Object._IsIncludeInactiveEmp = False Then
            '            dsList = objEmp.GetEmployeeList("List", True, ConfigParameter._Object._IsIncludeInactiveEmp, , , , , , , , , , , , CDate(eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate).ToShortDateString), CDate(eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate).ToShortDateString))
            '        Else
            '            dsList = objEmp.GetEmployeeList("List", True, Not ConfigParameter._Object._IsIncludeInactiveEmp)
            '        End If
            '        With cboFilter
            '            .ValueMember = "employeeunkid"
            '            .DisplayMember = "employeename"
            '        End With
            '        cboFilter.DataSource = dsList.Tables("List")
            '        cboFilter.SelectedValue = 0
            'End Select
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboViewBy_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

#End Region

	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Private Sub OtherSettings()
		Try
			Me.SuspendLayout()
			
			Call SetLanguage()
			
			Me.gbFilterCriteria.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbFilterCriteria.ForeColor = GUI._eZeeContainerHeaderForeColor 

			Me.gbUserLog.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbUserLog.ForeColor = GUI._eZeeContainerHeaderForeColor 


			Me.btnExport.GradientBackColor = GUI._ButttonBackColor 
			Me.btnExport.GradientForeColor = GUI._ButttonFontColor

			Me.btnClose.GradientBackColor = GUI._ButttonBackColor 
			Me.btnClose.GradientForeColor = GUI._ButttonFontColor


			Me.ResumeLayout()
		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "OtherSettings", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetLanguage()
		Try
			Me.Text = Language._Object.getCaption(Me.Name, Me.Text)
			
			Me.gbFilterCriteria.Text = Language._Object.getCaption(Me.gbFilterCriteria.Name, Me.gbFilterCriteria.Text)
			Me.btnReset.Text = Language._Object.getCaption(Me.btnReset.Name, Me.btnReset.Text)
			Me.lblViewBy.Text = Language._Object.getCaption(Me.lblViewBy.Name, Me.lblViewBy.Text)
			Me.btnSearch.Text = Language._Object.getCaption(Me.btnSearch.Name, Me.btnSearch.Text)
			Me.gbUserLog.Text = Language._Object.getCaption(Me.gbUserLog.Name, Me.gbUserLog.Text)
			Me.colhUserName.Text = Language._Object.getCaption(CStr(Me.colhUserName.Tag), Me.colhUserName.Text)
			Me.colhIp.Text = Language._Object.getCaption(CStr(Me.colhIp.Tag), Me.colhIp.Text)
			Me.colhMachine.Text = Language._Object.getCaption(CStr(Me.colhMachine.Tag), Me.colhMachine.Text)
			Me.colhAttempts.Text = Language._Object.getCaption(CStr(Me.colhAttempts.Tag), Me.colhAttempts.Text)
			Me.colhLoginFrom.Text = Language._Object.getCaption(CStr(Me.colhLoginFrom.Tag), Me.colhLoginFrom.Text)
			Me.btnExport.Text = Language._Object.getCaption(Me.btnExport.Name, Me.btnExport.Text)
			Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
			Me.lblAttemptsType.Text = Language._Object.getCaption(Me.lblAttemptsType.Name, Me.lblAttemptsType.Text)
			Me.colhAttemptType.Text = Language._Object.getCaption(CStr(Me.colhAttemptType.Tag), Me.colhAttemptType.Text)
			Me.colhAttempt_Date.Text = Language._Object.getCaption(CStr(Me.colhAttempt_Date.Tag), Me.colhAttempt_Date.Text)

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetLanguage", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "Select")
			Language.setMessage(mstrModuleName, 2, "Successful")
			Language.setMessage(mstrModuleName, 3, "Unsuccessful")
			Language.setMessage(mstrModuleName, 4, "DeskTop")
			Language.setMessage(mstrModuleName, 5, "Employee Self Service")
			Language.setMessage(mstrModuleName, 6, "Manager Self Service")
			Language.setMessage(mstrModuleName, 7, "USER LOGIN ATTEMPT LIST")
			Language.setMessage(mstrModuleName, 200, "User")
			Language.setMessage(mstrModuleName, 201, "Employee")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class