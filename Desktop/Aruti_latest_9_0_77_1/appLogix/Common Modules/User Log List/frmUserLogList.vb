﻿Option Strict On

Imports eZeeCommonLib
Imports Aruti.Data

Public Class frmUserLogList

#Region " Private Variables "
    Private ReadOnly mstrModuleName As String = "frmUserLogList"
    Private mblnCancel As Boolean = True
    Private menAction As enAction = enAction.ADD_ONE
    Private objUserLog As clsUser_tracking_Tran
    Private dtReader As DataTable = Nothing
#End Region

#Region " Private Methods "

    'S.SANDEEP [ 21 MAY 2012 ] -- START
    'ENHANCEMENT : TRA USER PRIVILEGE CHANGES
    Private Sub Fill_Combo()
        Try
            With cboGrouping
                .Items.Clear()
                .Items.Add(Language.getMessage(mstrModuleName, 101, "User Name"))
                .Items.Add(Language.getMessage(mstrModuleName, 103, "Login Date"))
                .Items.Add(Language.getMessage(mstrModuleName, 104, "Logout Date"))
                .Items.Add(Language.getMessage(mstrModuleName, 108, "Login From"))
                .SelectedIndex = 0
            End With

            With cboViewBy
                .Items.Clear()
                .Items.Add(Language.getMessage(mstrModuleName, 100, "Select"))
                .Items.Add(Language.getMessage(mstrModuleName, 105, "DeskTop"))
                Select Case gApplicationType
                    Case enArutiApplicatinType.Aruti_Payroll
                        .Items.Add(Language.getMessage(mstrModuleName, 106, "Employee Self Service"))
                        .Items.Add(Language.getMessage(mstrModuleName, 107, "Manager Self Service"))
                End Select
                .SelectedIndex = 0
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Fill_Combo", mstrModuleName)
        End Try
    End Sub
    'S.SANDEEP [ 21 MAY 2012 ] -- END

    Private Sub FillList()
        Dim dtTable As DataTable = Nothing
        Dim StrSearching As String = String.Empty
        Dim StrSort As String = String.Empty
        Try
            Dim dsList As DataSet = objUserLog.GetUserLog("List", User._Object._Userunkid, gApplicationType)

            If dtpLoginFromDate.Checked = True Then
                StrSearching &= "AND logindt >= '" & eZeeDate.convertDate(dtpLoginFromDate.Value) & "' "
            End If

            If dtpLoginToDate.Checked = True Then
                StrSearching &= "AND logindt <= '" & eZeeDate.convertDate(dtpLoginToDate.Value) & "' "
            End If

            If dtpLogoutFromDate.Checked = True Then
                StrSearching &= "AND logoutdt >= '" & eZeeDate.convertDate(dtpLogoutFromDate.Value) & "' "
            End If

            If dtpLogoutToDate.Checked = True Then
                StrSearching &= "AND logoutdt <= '" & eZeeDate.convertDate(dtpLogoutToDate.Value) & "' "
            End If

            Select Case CInt(cboGrouping.SelectedIndex)
                Case 0  'USERWISE
                    StrSort &= "username " & IIf(radAscending.Checked = True, "asc", "desc").ToString
                Case 1  'LOGINDATE
                    StrSort &= "logindate " & IIf(radAscending.Checked = True, "asc", "desc").ToString
                Case 2  'LOGOUTDATE
                    StrSort &= "logout_date " & IIf(radAscending.Checked = True, "asc", "desc").ToString
                Case 3 'LOGIN FROM
                    StrSort &= "LOGIN_FROM " & IIf(radAscending.Checked = True, "asc", "desc").ToString
            End Select

            Select Case CInt(cboViewBy.SelectedIndex)
                Case 0  'NONE
                Case 1  'DESKTOP
                    StrSearching &= "AND login_modeid = " & enLogin_Mode.DESKTOP
                Case 2  'EMPLOYEE SELF SERVICE
                    StrSearching &= "AND login_modeid = " & enLogin_Mode.EMP_SELF_SERVICE
                Case 3  'MANAGER SELF SERVICE
                    StrSearching &= "AND login_modeid = " & enLogin_Mode.MGR_SELF_SERVICE
            End Select
            If objlblCaption.Text.Contains("#") Then
                colhUserName.Text = Language.getMessage(mstrModuleName, 200, "User")
            Else
                colhUserName.Text = objlblCaption.Text
            End If


            If CInt(cboFilter.SelectedValue) > 0 Then
                StrSearching &= "AND userunkid = '" & CInt(cboFilter.SelectedValue) & "' "
            End If

            If StrSearching.Trim.Length > 0 Then
                StrSearching = StrSearching.Substring(3)
                dtTable = New DataView(dsList.Tables(0), StrSearching, StrSort, DataViewRowState.CurrentRows).ToTable
            Else
                dtTable = New DataView(dsList.Tables(0), "", StrSort, DataViewRowState.CurrentRows).ToTable
            End If

            dtReader = New DataTable("List")
            dtReader.Columns.Add("user", System.Type.GetType("System.String")).Caption = Language.getMessage(mstrModuleName, 201, "User")
            dtReader.Columns.Add("logindt", System.Type.GetType("System.String")).Caption = Language.getMessage(mstrModuleName, 202, "Last Login Date")
            dtReader.Columns.Add("logoutdt", System.Type.GetType("System.String")).Caption = Language.getMessage(mstrModuleName, 203, "Logout Date")
            dtReader.Columns.Add("ip", System.Type.GetType("System.String")).Caption = Language.getMessage(mstrModuleName, 204, "IP")
            dtReader.Columns.Add("machine", System.Type.GetType("System.String")).Caption = Language.getMessage(mstrModuleName, 205, "Machine")
            dtReader.Columns.Add("status", System.Type.GetType("System.String")).Caption = Language.getMessage(mstrModuleName, 206, "Status")
            dtReader.Columns.Add("logfrm", System.Type.GetType("System.String")).Caption = Language.getMessage(mstrModuleName, 207, "Login From")

            lvUserLog.Items.Clear()
            If dsList IsNot Nothing AndAlso dsList.Tables(0).Rows.Count > 0 Then
                Dim lvitem As ListViewItem
                For Each drrow As DataRow In dtTable.Rows
                    Dim dtRow As DataRow = dtReader.NewRow
                    lvitem = New ListViewItem

                    lvitem.Tag = CInt(drrow("trackingunkid"))
                    lvitem.Text = drrow("username").ToString
                    If drrow("logindate").ToString <> "" Then
                        lvitem.SubItems.Add(CDate(drrow("logindate")).ToString)
                    Else
                        lvitem.SubItems.Add("")
                    End If
                    If drrow("logout_date").ToString <> "" Then
                        lvitem.SubItems.Add(CDate(drrow("logout_date")).ToString)
                    Else
                        lvitem.SubItems.Add("N/A")
                    End If
                    lvitem.SubItems.Add(drrow("ip").ToString)
                    lvitem.SubItems.Add(drrow("machine").ToString)
                    lvitem.SubItems.Add(drrow("remark").ToString)
                    If drrow("logindt").ToString.Trim.Length > 0 Then
                        lvitem.SubItems.Add(eZeeDate.convertDate(drrow("logindt").ToString).ToShortDateString)
                    Else
                        lvitem.SubItems.Add("")
                    End If

                    If drrow("logoutdt").ToString.Trim.Length > 0 Then
                        lvitem.SubItems.Add(eZeeDate.convertDate(drrow("logoutdt").ToString).ToShortDateString)
                    Else
                        lvitem.SubItems.Add("N/A")
                    End If

                    lvitem.SubItems.Add(drrow.Item("LOGIN_FROM").ToString)

                    dtRow.Item("user") = drrow.Item("username")
                    dtRow.Item("logindt") = drrow.Item("logindate")
                    dtRow.Item("logoutdt") = drrow.Item("logout_date")
                    dtRow.Item("ip") = drrow.Item("ip")
                    dtRow.Item("machine") = drrow.Item("machine")
                    dtRow.Item("status") = drrow.Item("remark")
                    dtRow.Item("logfrm") = drrow.Item("LOGIN_FROM")

                    dtReader.Rows.Add(dtRow)

                    lvUserLog.Items.Add(lvitem)
                Next

                Select Case CInt(cboGrouping.SelectedIndex)
                    Case 0  'USERWISE
                        lvUserLog.GroupingColumn = colhUserName
                    Case 1  'LOGINDATE
                        lvUserLog.GroupingColumn = objcolhLoginDate
                    Case 2  'LOGOUTDATE
                        lvUserLog.GroupingColumn = objcolhLogoutDate
                    Case 3  'LOGIN FROM
                        lvUserLog.GroupingColumn = colhLoginFrom
                End Select

                lvUserLog.DisplayGroups(True)
                lvUserLog.GridLines = False

            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        End Try
    End Sub

    'S.SANDEEP [ 19 JULY 2012 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Private Function Store_User_Log(ByVal blnIsSearch As Boolean) As Boolean
        Dim objViewAT As New clsView_AT_Logs
        Try
            objViewAT._Atviewmodeid = clsView_AT_Logs.enAT_View_Mode.USER_AUTHENTICATION_LOG
            objViewAT._Userunkid = User._Object._Userunkid
            objViewAT._Viewdatetime = ConfigParameter._Object._CurrentDateAndTime
            If blnIsSearch = True Then
                objViewAT._Viewoperationid = clsView_AT_Logs.enAT_OperationId.SEARCH
            Else
                objViewAT._Viewoperationid = clsView_AT_Logs.enAT_OperationId.EXPORT
            End If

            Dim StrXML_Value As String = ""
            StrXML_Value = "<" & Me.Name & " xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"">" & vbCrLf

            For Each ctrl As Control In gbFilterCriteria.Controls
                If TypeOf ctrl Is DateTimePicker Then
                    If CType(ctrl, DateTimePicker).Checked = True Then
                        StrXML_Value &= "<" & ctrl.Name & ">" & eZeeDate.convertDate(CType(ctrl, DateTimePicker).Value) & "</" & ctrl.Name & ">" & vbCrLf
                    Else
                        StrXML_Value &= "<" & ctrl.Name & "/>" & vbCrLf
                    End If
                ElseIf TypeOf ctrl Is ComboBox Then
                    If CType(ctrl, ComboBox).SelectedIndex >= 0 Then
                        If CType(ctrl, ComboBox).DataSource IsNot Nothing Then
                            StrXML_Value &= "<" & ctrl.Name & ">" & CInt(CType(ctrl, ComboBox).SelectedValue) & "</" & ctrl.Name & ">" & vbCrLf
                        Else
                            StrXML_Value &= "<" & ctrl.Name & ">" & CType(ctrl, ComboBox).SelectedIndex & "</" & ctrl.Name & ">" & vbCrLf
                        End If
                    Else
                        StrXML_Value &= "<" & ctrl.Name & "/>" & vbCrLf
                    End If
                ElseIf TypeOf ctrl Is RadioButton Then
                    If CType(ctrl, RadioButton).Checked = True Then
                        StrXML_Value &= "<" & ctrl.Name & ">" & CType(ctrl, RadioButton).Checked & "</" & ctrl.Name & ">" & vbCrLf
                    Else
                        StrXML_Value &= "<" & ctrl.Name & "/>" & vbCrLf
                    End If
                End If
            Next
            StrXML_Value &= "</" & Me.Name & ">"

            objViewAT._Value_View = StrXML_Value

            If objViewAT.Insert = False Then
                eZeeMsgBox.Show(objViewAT._Message, enMsgBoxStyle.Information)
                Return False
            End If

            Return True

        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "Store_User_Log", mstrModuleName)
        Finally
            objViewAT = Nothing
        End Try
    End Function
    'S.SANDEEP [ 19 JULY 2012 ] -- END

#End Region

#Region " Form's Events "

    Private Sub frmUserLogList_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            Call Set_Logo(Me, gApplicationType)

            'Anjan (02 Sep 2011)-Start
            'Issue : Including Language Settings.
            Language.setLanguage(Me.Name)
            Call OtherSettings()
            'Anjan (02 Sep 2011)-End 

            lvUserLog.GridLines = False
            objUserLog = New clsUser_tracking_Tran
            'S.SANDEEP [ 21 MAY 2012 ] -- START
            'ENHANCEMENT : TRA USER PRIVILEGE CHANGES
            'FillList()
            Call Fill_Combo()
            btnExport.Enabled = False
            'S.SANDEEP [ 21 MAY 2012 ] -- END
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmUserLogList_Load", mstrModuleName)
        End Try
    End Sub
    'Anjan (02 Sep 2011)-Start
    'Issue : Including Language Settings.
    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsUser_tracking_Tran.SetMessages()
            objfrm._Other_ModuleNames = "clsUser_tracking_Tran"
            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub
    'Anjan (02 Sep 2011)-End 
#End Region

#Region " Button's Events "

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        mblnCancel = False
        Me.Close()
    End Sub

    'S.SANDEEP [ 21 MAY 2012 ] -- START
    'ENHANCEMENT : TRA USER PRIVILEGE CHANGES
    Private Sub objbtnHide_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnHide.Click
        Try
            spcContainer.Panel1Collapsed = True
            objbtnShow.Visible = True
            objbtnHide.Visible = False
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnHide_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnShow_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnShow.Click
        Try
            spcContainer.Panel1Collapsed = False
            objbtnShow.Visible = False
            objbtnHide.Visible = True
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnShow_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnSearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSearch.Click
        Try

            'S.SANDEEP [ 19 JULY 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            If Store_User_Log(True) = False Then Exit Sub
            'S.SANDEEP [ 19 JULY 2012 ] -- END

            Call FillList()
            If lvUserLog.Items.Count > 0 Then
                btnExport.Enabled = True
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnSearch_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnReset.Click
        Try
            dtpLoginFromDate.Checked = False
            dtpLoginToDate.Checked = False
            dtpLogoutFromDate.Checked = False
            dtpLogoutToDate.Checked = False
            cboGrouping.SelectedIndex = 0
            radAscending.Checked = True
            cboViewBy.SelectedIndex = 0
            lvUserLog.Items.Clear()
            btnExport.Enabled = False
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnReset_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnSearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearch.Click
        Dim frm As New frmCommonSearch
        Try
            With frm
                .ValueMember = cboFilter.ValueMember
                .DisplayMember = cboFilter.DisplayMember
                Select Case gApplicationType
                    Case enArutiApplicatinType.Aruti_Payroll
                        'S.SANDEEP [10 AUG 2015] -- START
                        'ENHANCEMENT : Aruti SaaS Changes
                        '.CodeMember = "employeecode"
                        If cboViewBy.SelectedIndex = 2 Then
                        .CodeMember = "employeecode"
                        End If
                        'S.SANDEEP [10 AUG 2015] -- END
                End Select
                .DataSource = CType(cboFilter.DataSource, DataTable)
            End With
            If frm.DisplayDialog Then
                cboFilter.SelectedValue = frm.SelectedValue
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearch_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub btnExport_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnExport.Click
        Try
            'S.SANDEEP [ 19 JULY 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            If Store_User_Log(False) = False Then Exit Sub
            'S.SANDEEP [ 19 JULY 2012 ] -- END

            Dim strBuilder As New System.Text.StringBuilder
            strBuilder.Append(" <HTML> " & vbCrLf)
            strBuilder.Append(" <BODY><FONT FACE = TAHOMA FONT SIZE=2> " & vbCrLf)
            strBuilder.Append(" <TABLE BORDER = 1 BORDERCOLOR = BLACK WIDTH='50%'> " & vbCrLf)

            strBuilder.Append(" <TR ALIGN = LEFT VALIGN=TOP WIDTH='50%'> " & vbCrLf)
            strBuilder.Append(" <TD BORDER=1 BORDERCOLOR=BLACK ALIGN='CENTER' COLSPAN='2'><FONT SIZE=2><B>" & Language.getMessage(mstrModuleName, 1, "USER LOG LIST") & "</B></FONT></TD>" & vbCrLf)
            strBuilder.Append(" </TR> " & vbCrLf)

            strBuilder.Append(" <TR ALIGN = LEFT VALIGN=TOP WIDTH='50%'> " & vbCrLf)
            strBuilder.Append(" <TD BORDER=1 BORDERCOLOR=BLACK ALIGN='LEFT' WIDTH = '20%'><FONT SIZE=2><B>" & Language.getMessage(mstrModuleName, 2, "Login From Date : ") & "</B></FONT></TD> " & vbCrLf)
            If dtpLoginFromDate.Checked = True Then
                strBuilder.Append(" <TD BORDER=1 BORDERCOLOR=BLACK ALIGN='LEFT' WIDTH = '20%'><FONT SIZE=2><B>" & dtpLoginFromDate.Value.Date & "</B></FONT></TD> " & vbCrLf)
            Else
                strBuilder.Append(" <TD BORDER=1 BORDERCOLOR=BLACK ALIGN='LEFT' WIDTH = '20%'><FONT SIZE=2><B>" & "&nbsp;" & "</B></FONT></TD> " & vbCrLf)
            End If
            strBuilder.Append(" </TR> " & vbCrLf)

            strBuilder.Append(" <TR ALIGN = LEFT VALIGN=TOP WIDTH='50%'> " & vbCrLf)
            strBuilder.Append(" <TD BORDER=1 BORDERCOLOR=BLACK ALIGN='LEFT' WIDTH = '20%'><FONT SIZE=2><B>" & Language.getMessage(mstrModuleName, 3, "Login To Date : ") & "</B></FONT></TD> " & vbCrLf)
            If dtpLoginToDate.Checked = True Then
                strBuilder.Append(" <TD BORDER=1 BORDERCOLOR=BLACK ALIGN='LEFT' WIDTH = '20%'><FONT SIZE=2><B>" & dtpLoginToDate.Value.Date & "</B></FONT></TD> " & vbCrLf)
            Else
                strBuilder.Append(" <TD BORDER=1 BORDERCOLOR=BLACK ALIGN='LEFT' WIDTH = '20%'><FONT SIZE=2><B>" & "&nbsp;" & "</B></FONT></TD> " & vbCrLf)
            End If
            strBuilder.Append(" </TR> " & vbCrLf)

            strBuilder.Append(" <TR ALIGN = LEFT VALIGN=TOP WIDTH='50%'> " & vbCrLf)
            strBuilder.Append(" <TD BORDER=1 BORDERCOLOR=BLACK ALIGN='LEFT' WIDTH = '20%'><FONT SIZE=2><B>" & Language.getMessage(mstrModuleName, 4, "Logout From Date : ") & "</B></FONT></TD> " & vbCrLf)
            If dtpLogoutFromDate.Checked = True Then
                strBuilder.Append(" <TD BORDER=1 BORDERCOLOR=BLACK ALIGN='LEFT' WIDTH = '20%'><FONT SIZE=2><B>" & dtpLogoutFromDate.Value.Date & "</B></FONT></TD> " & vbCrLf)
            Else
                strBuilder.Append(" <TD BORDER=1 BORDERCOLOR=BLACK ALIGN='LEFT' WIDTH = '20%'><FONT SIZE=2><B>" & "&nbsp;" & "</B></FONT></TD> " & vbCrLf)
            End If
            strBuilder.Append(" </TR> " & vbCrLf)

            strBuilder.Append(" <TR ALIGN = LEFT VALIGN=TOP WIDTH='50%'> " & vbCrLf)
            strBuilder.Append(" <TD BORDER=1 BORDERCOLOR=BLACK ALIGN='LEFT' WIDTH = '20%'><FONT SIZE=2><B>" & Language.getMessage(mstrModuleName, 5, "Logout To Date : ") & "</B></FONT></TD> " & vbCrLf)
            If dtpLogoutToDate.Checked = True Then
                strBuilder.Append(" <TD BORDER=1 BORDERCOLOR=BLACK ALIGN='LEFT' WIDTH = '20%'><FONT SIZE=2><B>" & dtpLogoutToDate.Value.Date & "</B></FONT></TD> " & vbCrLf)
            Else
                strBuilder.Append(" <TD BORDER=1 BORDERCOLOR=BLACK ALIGN='LEFT' WIDTH = '20%'><FONT SIZE=2><B>" & "&nbsp;" & "</B></FONT></TD> " & vbCrLf)
            End If
            strBuilder.Append(" </TR> " & vbCrLf)

            strBuilder.Append(" <TR ALIGN = LEFT VALIGN=TOP WIDTH='50%'> " & vbCrLf)
            strBuilder.Append(" <TD BORDER=1 BORDERCOLOR=BLACK ALIGN='LEFT' WIDTH = '20%'><FONT SIZE=2><B>" & Language.getMessage(mstrModuleName, 6, "Group By : ") & "</B></FONT></TD> " & vbCrLf)
            strBuilder.Append(" <TD BORDER=1 BORDERCOLOR=BLACK ALIGN='LEFT' WIDTH = '20%'><FONT SIZE=2><B>" & cboGrouping.Text & "</B></FONT></TD> " & vbCrLf)
            strBuilder.Append(" </TR> " & vbCrLf)

            strBuilder.Append(" <TR ALIGN = LEFT VALIGN=TOP WIDTH='50%'> " & vbCrLf)
            strBuilder.Append(" <TD BORDER=1 BORDERCOLOR=BLACK ALIGN='LEFT' WIDTH = '20%'><FONT SIZE=2><B>" & Language.getMessage(mstrModuleName, 7, "Sort Mode : ") & "</B></FONT></TD> " & vbCrLf)
            If radAscending.Checked = True Then
                strBuilder.Append(" <TD BORDER=1 BORDERCOLOR=BLACK ALIGN='LEFT' WIDTH = '20%'><FONT SIZE=2><B>" & radAscending.Text & "</B></FONT></TD> " & vbCrLf)
            ElseIf radDescending.Checked = True Then
                strBuilder.Append(" <TD BORDER=1 BORDERCOLOR=BLACK ALIGN='LEFT' WIDTH = '20%'><FONT SIZE=2><B>" & radDescending.Text & "</B></FONT></TD> " & vbCrLf)
            End If
            strBuilder.Append(" </TR> " & vbCrLf)

            strBuilder.Append(" <TR ALIGN = LEFT VALIGN=TOP WIDTH='50%'> " & vbCrLf)
            strBuilder.Append(" <TD BORDER=1 BORDERCOLOR=BLACK ALIGN='LEFT' WIDTH = '20%'><FONT SIZE=2><B>" & Language.getMessage(mstrModuleName, 8, "View By : ") & "</B></FONT></TD> " & vbCrLf)
            If cboViewBy.SelectedIndex > 0 Then
                strBuilder.Append(" <TD BORDER=1 BORDERCOLOR=BLACK ALIGN='LEFT' WIDTH = '20%'><FONT SIZE=2><B>" & cboViewBy.Text & "</B></FONT></TD> " & vbCrLf)
            Else
                strBuilder.Append(" <TD BORDER=1 BORDERCOLOR=BLACK ALIGN='LEFT' WIDTH = '20%'><FONT SIZE=2><B>" & "&nbsp;" & "</B></FONT></TD> " & vbCrLf)
            End If
            strBuilder.Append(" </TR> " & vbCrLf)

            strBuilder.Append(" <TR ALIGN = LEFT VALIGN=TOP WIDTH='50%'> " & vbCrLf)
            strBuilder.Append(" <TD BORDER=1 BORDERCOLOR=BLACK ALIGN='LEFT' WIDTH = '20%'><FONT SIZE=2><B>" & Language.getMessage(mstrModuleName, 9, "Filter By : ") & "</B></FONT></TD> " & vbCrLf)
            If cboFilter.SelectedIndex > 0 Then
                strBuilder.Append(" <TD BORDER=1 BORDERCOLOR=BLACK ALIGN='LEFT' WIDTH = '20%'><FONT SIZE=2><B>" & cboFilter.Text & "</B></FONT></TD> " & vbCrLf)
            Else
                strBuilder.Append(" <TD BORDER=1 BORDERCOLOR=BLACK ALIGN='LEFT' WIDTH = '20%'><FONT SIZE=2><B>" & "&nbsp;" & "</B></FONT></TD> " & vbCrLf)
            End If
            strBuilder.Append(" </TR> " & vbCrLf)

            strBuilder.Append(" </TABLE> " & vbCrLf)

            strBuilder.Append(" <BR/> " & vbCrLf)

            strBuilder.Append(" <TABLE BORDER = 1 BORDERCOLOR = BLACK WIDTH='100%'> " & vbCrLf)
            strBuilder.Append(" <TR ALIGN = LEFT VALIGN=TOP WIDTH='100%'> " & vbCrLf)

            For i As Integer = 0 To dtReader.Columns.Count - 1
                strBuilder.Append(" <TD BORDER=1 BORDERCOLOR=BLACK ALIGN='LEFT'><FONT SIZE=2><B>" & dtReader.Columns(i).Caption.ToString & "</B></FONT></TD> " & vbCrLf)
            Next
            strBuilder.Append(" </TR> " & vbCrLf)

            strBuilder.Append(" <TR ALIGN = LEFT VALIGN=TOP WIDTH='100%'> " & vbCrLf)
            For j As Integer = 0 To dtReader.Rows.Count - 1
                For i As Integer = 0 To dtReader.Columns.Count - 1
                    strBuilder.Append(" <TD BORDER=1 BORDERCOLOR=BLACK ALIGN='LEFT'><FONT SIZE=2>" & dtReader.Rows(j)(i).ToString & "</FONT></TD> " & vbCrLf)
                Next
                strBuilder.Append(" </TR> " & vbCrLf)
            Next

            strBuilder.Append(" </TABLE> " & vbCrLf)


            strBuilder.Append(" </BODY> " & vbCrLf)
            strBuilder.Append(" </HTML> " & vbCrLf)

            Dim svDialog As New SaveFileDialog
            svDialog.Filter = "Excel files (*.xlsx)|*.xlsx"
            If svDialog.ShowDialog = Windows.Forms.DialogResult.OK Then
                Dim fsFile As New IO.FileStream(svDialog.FileName, IO.FileMode.Create, IO.FileAccess.Write)
                Dim strWriter As New IO.StreamWriter(fsFile)
                With strWriter
                    .BaseStream.Seek(0, IO.SeekOrigin.End)
                    .WriteLine(strBuilder)
                    .Close()
                End With
                Diagnostics.Process.Start(svDialog.FileName)
            End If


        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnExport_Click", mstrModuleName)
        End Try
    End Sub
    'S.SANDEEP [ 21 MAY 2012 ] -- END

#End Region

#Region " Controls Events "

    Private Sub cboViewBy_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboViewBy.SelectedIndexChanged
        Try
            Dim dsList As New DataSet
            Select Case cboViewBy.SelectedIndex
                Case 0  'NONE
                    objlblCaption.Visible = False
                    cboFilter.Visible = False
                    objbtnSearch.Visible = False
                    colhUserName.Text = Language.getMessage(mstrModuleName, 200, "User")
                    cboFilter.SelectedValue = 0
                Case 1, 3  'DESKTOP,MANAGER SELF SERVICE 
                    cboFilter.DataSource = Nothing
                    objlblCaption.Visible = True
                    cboFilter.Visible = True
                    objbtnSearch.Visible = True
                    objlblCaption.Text = Language.getMessage(mstrModuleName, 200, "User")
                    Dim objFUser As New clsUserAddEdit
                    'S.SANDEEP [10 AUG 2015] -- START
                    'ENHANCEMENT : Aruti SaaS Changes
                    'Dim objOption As New clsPassowdOptions
                    'Dim drOption As DataRow() = objOption._DataTable.Select("passwdoptionid = " & enPasswordOption.USER_LOGIN_MODE)
                    'If drOption.Length > 0 Then
                    '    If objOption._UserLogingModeId = enAuthenticationMode.BASIC_AUTHENTICATION Then
                    '        'S.SANDEEP [ 11 DEC 2012 ] -- START
                    '        'ENHANCEMENT : TRA CHANGES
                    '        'dsList = objFUser.getComboList("List", True, False)
                    '        Dim objPswd As New clsPassowdOptions
                    '        If objPswd._IsEmployeeAsUser Then
                    '            dsList = objFUser.getComboList("List", True, False, True)
                    '        Else
                    '        dsList = objFUser.getComboList("List", True, False)
                    '        End If
                    '        objPswd = Nothing
                    '        'S.SANDEEP [ 11 DEC 2012 ] -- END
                    '    ElseIf objOption._UserLogingModeId = enAuthenticationMode.AD_BASIC_AUTHENTICATION Then
                    '        dsList = objFUser.getComboList("List", True, True)
                    '    ElseIf objOption._UserLogingModeId = enAuthenticationMode.AD_SSO_AUTHENTICATION Then
                    '        dsList = objFUser.getComboList("List", True, True)
                    '    End If
                    'Else
                            'dsList = objFUser.getComboList("List", True, False)
                    'End If

                    'Nilay (01-Mar-2016) -- Start
                    'dsList = objFUser.getNewComboList("List", , True)
                    dsList = objFUser.getNewComboList("List", , True, , , , True)
                    'Nilay (01-Mar-2016) -- End

                    'S.SANDEEP [10 AUG 2015] -- END

                    objFUser = Nothing
                    With cboFilter
                        .ValueMember = "userunkid"
                        .DisplayMember = "name"
                    End With
                    cboFilter.DataSource = dsList.Tables("List")
                    cboFilter.SelectedValue = 0
                Case 2  'EMPLOYEE SELF SERVICE
                    cboFilter.DataSource = Nothing
                    objlblCaption.Visible = True
                    cboFilter.Visible = True
                    objbtnSearch.Visible = True
                    objlblCaption.Text = Language.getMessage(mstrModuleName, 201, "Employee")
                    Dim objEmp As New clsEmployee_Master

                    'S.SANDEEP [04 JUN 2015] -- START
                    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS

                    'If ConfigParameter._Object._IsIncludeInactiveEmp = False Then
                    '    dsList = objEmp.GetEmployeeList("List", True, ConfigParameter._Object._IsIncludeInactiveEmp, , , , , , , , , , , , CDate(eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate).ToShortDateString), CDate(eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate).ToShortDateString))
                    'Else
                    '    dsList = objEmp.GetEmployeeList("List", True, Not ConfigParameter._Object._IsIncludeInactiveEmp)
                    'End If

                    dsList = objEmp.GetEmployeeList(FinancialYear._Object._DatabaseName, _
                                                  User._Object._Userunkid, _
                                                  FinancialYear._Object._YearUnkid, _
                                                  Company._Object._Companyunkid, _
                                                  FinancialYear._Object._Database_Start_Date.Date, _
                                                  FinancialYear._Object._Database_End_Date.Date, _
                                                  ConfigParameter._Object._UserAccessModeSetting, _
                                                  True, ConfigParameter._Object._IsIncludeInactiveEmp, "List", True)

                    'S.SANDEEP [04 JUN 2015] -- END

                    With cboFilter
                        .ValueMember = "employeeunkid"
                        .DisplayMember = "employeename"
                    End With
                    cboFilter.DataSource = dsList.Tables("List")
                    cboFilter.SelectedValue = 0
            End Select
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboViewBy_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

#End Region


	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Private Sub OtherSettings()
		Try
			Me.SuspendLayout()
			
			Call SetLanguage()
			
			Me.gbFilterCriteria.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbFilterCriteria.ForeColor = GUI._eZeeContainerHeaderForeColor 

			Me.gbUserLog.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbUserLog.ForeColor = GUI._eZeeContainerHeaderForeColor 


			Me.btnClose.GradientBackColor = GUI._ButttonBackColor 
			Me.btnClose.GradientForeColor = GUI._ButttonFontColor

			Me.btnExport.GradientBackColor = GUI._ButttonBackColor 
			Me.btnExport.GradientForeColor = GUI._ButttonFontColor


			Me.ResumeLayout()
		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "OtherSettings", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetLanguage()
		Try
			Me.Text = Language._Object.getCaption(Me.Name, Me.Text)
			
			Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
			Me.colhUserName.Text = Language._Object.getCaption(CStr(Me.colhUserName.Tag), Me.colhUserName.Text)
			Me.colhLogindate.Text = Language._Object.getCaption(CStr(Me.colhLogindate.Tag), Me.colhLogindate.Text)
			Me.colhIp.Text = Language._Object.getCaption(CStr(Me.colhIp.Tag), Me.colhIp.Text)
			Me.colhMachine.Text = Language._Object.getCaption(CStr(Me.colhMachine.Tag), Me.colhMachine.Text)
			Me.colhRemark.Text = Language._Object.getCaption(CStr(Me.colhRemark.Tag), Me.colhRemark.Text)
			Me.gbFilterCriteria.Text = Language._Object.getCaption(Me.gbFilterCriteria.Name, Me.gbFilterCriteria.Text)
			Me.gbUserLog.Text = Language._Object.getCaption(Me.gbUserLog.Name, Me.gbUserLog.Text)
			Me.btnSearch.Text = Language._Object.getCaption(Me.btnSearch.Name, Me.btnSearch.Text)
			Me.btnReset.Text = Language._Object.getCaption(Me.btnReset.Name, Me.btnReset.Text)
			Me.colhLogoutDate.Text = Language._Object.getCaption(CStr(Me.colhLogoutDate.Tag), Me.colhLogoutDate.Text)
			Me.lblLoginToDate.Text = Language._Object.getCaption(Me.lblLoginToDate.Name, Me.lblLoginToDate.Text)
			Me.lblLoginFromDate.Text = Language._Object.getCaption(Me.lblLoginFromDate.Name, Me.lblLoginFromDate.Text)
			Me.lblLogOutDateTo.Text = Language._Object.getCaption(Me.lblLogOutDateTo.Name, Me.lblLogOutDateTo.Text)
			Me.lblLogOutDateFrom.Text = Language._Object.getCaption(Me.lblLogOutDateFrom.Name, Me.lblLogOutDateFrom.Text)
			Me.lblGroupBy.Text = Language._Object.getCaption(Me.lblGroupBy.Name, Me.lblGroupBy.Text)
			Me.radDescending.Text = Language._Object.getCaption(Me.radDescending.Name, Me.radDescending.Text)
			Me.radAscending.Text = Language._Object.getCaption(Me.radAscending.Name, Me.radAscending.Text)
			Me.lblViewBy.Text = Language._Object.getCaption(Me.lblViewBy.Name, Me.lblViewBy.Text)
			Me.colhLoginFrom.Text = Language._Object.getCaption(CStr(Me.colhLoginFrom.Tag), Me.colhLoginFrom.Text)
			Me.btnExport.Text = Language._Object.getCaption(Me.btnExport.Name, Me.btnExport.Text)

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetLanguage", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "USER LOG LIST")
			Language.setMessage(mstrModuleName, 2, "Login From Date :")
			Language.setMessage(mstrModuleName, 3, "Login To Date :")
			Language.setMessage(mstrModuleName, 4, "Logout From Date :")
			Language.setMessage(mstrModuleName, 5, "Logout To Date :")
			Language.setMessage(mstrModuleName, 6, "Group By :")
			Language.setMessage(mstrModuleName, 7, "Sort Mode :")
			Language.setMessage(mstrModuleName, 8, "View By :")
			Language.setMessage(mstrModuleName, 9, "Filter By :")
			Language.setMessage(mstrModuleName, 100, "Select")
			Language.setMessage(mstrModuleName, 101, "User Name")
			Language.setMessage(mstrModuleName, 103, "Login Date")
			Language.setMessage(mstrModuleName, 104, "Logout Date")
			Language.setMessage(mstrModuleName, 105, "DeskTop")
			Language.setMessage(mstrModuleName, 106, "Employee Self Service")
			Language.setMessage(mstrModuleName, 107, "Manager Self Service")
			Language.setMessage(mstrModuleName, 108, "Login From")
			Language.setMessage(mstrModuleName, 200, "User")
			Language.setMessage(mstrModuleName, 201, "User")
			Language.setMessage(mstrModuleName, 202, "Last Login Date")
			Language.setMessage(mstrModuleName, 203, "Logout Date")
			Language.setMessage(mstrModuleName, 204, "IP")
			Language.setMessage(mstrModuleName, 205, "Machine")
			Language.setMessage(mstrModuleName, 206, "Status")
			Language.setMessage(mstrModuleName, 207, "Login From")
			Language.setMessage(mstrModuleName, 201, "Employee")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class


