﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmForceLogout
    Inherits eZee.Common.eZeeForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmForceLogout))
        Me.Panel1 = New System.Windows.Forms.Panel
        Me.TableLayoutPanel1 = New System.Windows.Forms.TableLayoutPanel
        Me.txtSearch = New eZee.TextBox.AlphanumericTextBox
        Me.SplitContainer1 = New System.Windows.Forms.SplitContainer
        Me.dtpTime = New System.Windows.Forms.DateTimePicker
        Me.btnReset = New eZee.Common.eZeeGradientButton
        Me.txtReason = New eZee.TextBox.AlphanumericTextBox
        Me.lblLogoutReason = New System.Windows.Forms.Label
        Me.btnSearch = New eZee.Common.eZeeGradientButton
        Me.objStLine = New eZee.Common.eZeeStraightLine
        Me.cboViewBy = New System.Windows.Forms.ComboBox
        Me.radApplyToChecked = New System.Windows.Forms.RadioButton
        Me.cboCompany = New System.Windows.Forms.ComboBox
        Me.lblViewBy = New System.Windows.Forms.Label
        Me.radApplyAll = New System.Windows.Forms.RadioButton
        Me.lblCompany = New System.Windows.Forms.Label
        Me.btnSet = New eZee.Common.eZeeLightButton(Me.components)
        Me.lblCaption = New System.Windows.Forms.Label
        Me.dtpReloginDate = New System.Windows.Forms.DateTimePicker
        Me.objchkAll = New System.Windows.Forms.CheckBox
        Me.dgvData = New System.Windows.Forms.DataGridView
        Me.dgcolhIsCheck = New System.Windows.Forms.DataGridViewCheckBoxColumn
        Me.dgcolhUser = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhLoginDate = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhLogoutDate = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhIP = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhMachine = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhReloginDate = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.EZeeFooter1 = New eZee.Common.eZeeFooter
        Me.btnLogout = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnClose = New eZee.Common.eZeeLightButton(Me.components)
        Me.Panel1.SuspendLayout()
        Me.TableLayoutPanel1.SuspendLayout()
        Me.SplitContainer1.Panel1.SuspendLayout()
        Me.SplitContainer1.Panel2.SuspendLayout()
        Me.SplitContainer1.SuspendLayout()
        CType(Me.dgvData, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.EZeeFooter1.SuspendLayout()
        Me.SuspendLayout()
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(Me.TableLayoutPanel1)
        Me.Panel1.Controls.Add(Me.EZeeFooter1)
        Me.Panel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel1.Location = New System.Drawing.Point(0, 0)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(832, 534)
        Me.Panel1.TabIndex = 0
        '
        'TableLayoutPanel1
        '
        Me.TableLayoutPanel1.ColumnCount = 1
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanel1.Controls.Add(Me.txtSearch, 0, 0)
        Me.TableLayoutPanel1.Controls.Add(Me.SplitContainer1, 0, 1)
        Me.TableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TableLayoutPanel1.Location = New System.Drawing.Point(0, 0)
        Me.TableLayoutPanel1.Name = "TableLayoutPanel1"
        Me.TableLayoutPanel1.RowCount = 2
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 26.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanel1.Size = New System.Drawing.Size(832, 479)
        Me.TableLayoutPanel1.TabIndex = 108
        '
        'txtSearch
        '
        Me.txtSearch.BackColor = System.Drawing.SystemColors.Window
        Me.txtSearch.Dock = System.Windows.Forms.DockStyle.Fill
        Me.txtSearch.Flags = 0
        Me.txtSearch.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92), Global.Microsoft.VisualBasic.ChrW(91), Global.Microsoft.VisualBasic.ChrW(93)}
        Me.txtSearch.Location = New System.Drawing.Point(3, 3)
        Me.txtSearch.Name = "txtSearch"
        Me.txtSearch.Size = New System.Drawing.Size(826, 21)
        Me.txtSearch.TabIndex = 107
        '
        'SplitContainer1
        '
        Me.SplitContainer1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SplitContainer1.IsSplitterFixed = True
        Me.SplitContainer1.Location = New System.Drawing.Point(3, 29)
        Me.SplitContainer1.Name = "SplitContainer1"
        Me.SplitContainer1.Orientation = System.Windows.Forms.Orientation.Horizontal
        '
        'SplitContainer1.Panel1
        '
        Me.SplitContainer1.Panel1.Controls.Add(Me.dtpTime)
        Me.SplitContainer1.Panel1.Controls.Add(Me.btnReset)
        Me.SplitContainer1.Panel1.Controls.Add(Me.txtReason)
        Me.SplitContainer1.Panel1.Controls.Add(Me.lblLogoutReason)
        Me.SplitContainer1.Panel1.Controls.Add(Me.btnSearch)
        Me.SplitContainer1.Panel1.Controls.Add(Me.objStLine)
        Me.SplitContainer1.Panel1.Controls.Add(Me.cboViewBy)
        Me.SplitContainer1.Panel1.Controls.Add(Me.radApplyToChecked)
        Me.SplitContainer1.Panel1.Controls.Add(Me.cboCompany)
        Me.SplitContainer1.Panel1.Controls.Add(Me.lblViewBy)
        Me.SplitContainer1.Panel1.Controls.Add(Me.radApplyAll)
        Me.SplitContainer1.Panel1.Controls.Add(Me.lblCompany)
        Me.SplitContainer1.Panel1.Controls.Add(Me.btnSet)
        Me.SplitContainer1.Panel1.Controls.Add(Me.lblCaption)
        Me.SplitContainer1.Panel1.Controls.Add(Me.dtpReloginDate)
        '
        'SplitContainer1.Panel2
        '
        Me.SplitContainer1.Panel2.Controls.Add(Me.objchkAll)
        Me.SplitContainer1.Panel2.Controls.Add(Me.dgvData)
        Me.SplitContainer1.Size = New System.Drawing.Size(826, 447)
        Me.SplitContainer1.SplitterDistance = 83
        Me.SplitContainer1.SplitterWidth = 2
        Me.SplitContainer1.TabIndex = 108
        '
        'dtpTime
        '
        Me.dtpTime.Checked = False
        Me.dtpTime.CustomFormat = "HH:mm"
        Me.dtpTime.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.dtpTime.Location = New System.Drawing.Point(222, 11)
        Me.dtpTime.Name = "dtpTime"
        Me.dtpTime.ShowCheckBox = True
        Me.dtpTime.ShowUpDown = True
        Me.dtpTime.Size = New System.Drawing.Size(82, 21)
        Me.dtpTime.TabIndex = 240
        '
        'btnReset
        '
        Me.btnReset.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnReset.BackColor = System.Drawing.Color.Transparent
        Me.btnReset.BackColor1 = System.Drawing.Color.Transparent
        Me.btnReset.BackColor2 = System.Drawing.Color.Transparent
        Me.btnReset.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.btnReset.BorderSelected = False
        Me.btnReset.DialogResult = System.Windows.Forms.DialogResult.None
        Me.btnReset.Image = Global.Aruti.Data.My.Resources.Resources.reset_20
        Me.btnReset.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.btnReset.Location = New System.Drawing.Point(798, 11)
        Me.btnReset.Name = "btnReset"
        Me.btnReset.Size = New System.Drawing.Size(21, 21)
        Me.btnReset.TabIndex = 239
        Me.btnReset.Visible = False
        '
        'txtReason
        '
        Me.txtReason.Flags = 0
        Me.txtReason.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92), Global.Microsoft.VisualBasic.ChrW(91), Global.Microsoft.VisualBasic.ChrW(93)}
        Me.txtReason.Location = New System.Drawing.Point(385, 38)
        Me.txtReason.Multiline = True
        Me.txtReason.Name = "txtReason"
        Me.txtReason.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.txtReason.Size = New System.Drawing.Size(385, 38)
        Me.txtReason.TabIndex = 108
        '
        'lblLogoutReason
        '
        Me.lblLogoutReason.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblLogoutReason.Location = New System.Drawing.Point(316, 40)
        Me.lblLogoutReason.Name = "lblLogoutReason"
        Me.lblLogoutReason.Size = New System.Drawing.Size(63, 16)
        Me.lblLogoutReason.TabIndex = 6
        Me.lblLogoutReason.Text = "Reason"
        Me.lblLogoutReason.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'btnSearch
        '
        Me.btnSearch.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnSearch.BackColor = System.Drawing.Color.Transparent
        Me.btnSearch.BackColor1 = System.Drawing.Color.Transparent
        Me.btnSearch.BackColor2 = System.Drawing.Color.Transparent
        Me.btnSearch.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.btnSearch.BorderSelected = False
        Me.btnSearch.DialogResult = System.Windows.Forms.DialogResult.None
        Me.btnSearch.Image = Global.Aruti.Data.My.Resources.Resources.search_20
        Me.btnSearch.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.btnSearch.Location = New System.Drawing.Point(776, 11)
        Me.btnSearch.Name = "btnSearch"
        Me.btnSearch.Size = New System.Drawing.Size(21, 21)
        Me.btnSearch.TabIndex = 238
        Me.btnSearch.Visible = False
        '
        'objStLine
        '
        Me.objStLine.BackColor = System.Drawing.Color.Transparent
        Me.objStLine.LineType = eZee.Common.StraightLineTypes.Vertical
        Me.objStLine.Location = New System.Drawing.Point(308, 11)
        Me.objStLine.Name = "objStLine"
        Me.objStLine.Size = New System.Drawing.Size(5, 65)
        Me.objStLine.TabIndex = 5
        '
        'cboViewBy
        '
        Me.cboViewBy.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboViewBy.DropDownWidth = 200
        Me.cboViewBy.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboViewBy.FormattingEnabled = True
        Me.cboViewBy.Location = New System.Drawing.Point(637, 11)
        Me.cboViewBy.Name = "cboViewBy"
        Me.cboViewBy.Size = New System.Drawing.Size(133, 21)
        Me.cboViewBy.TabIndex = 237
        Me.cboViewBy.Visible = False
        '
        'radApplyToChecked
        '
        Me.radApplyToChecked.Location = New System.Drawing.Point(91, 60)
        Me.radApplyToChecked.Name = "radApplyToChecked"
        Me.radApplyToChecked.Size = New System.Drawing.Size(123, 16)
        Me.radApplyToChecked.TabIndex = 3
        Me.radApplyToChecked.TabStop = True
        Me.radApplyToChecked.Text = "Apply To Checked"
        Me.radApplyToChecked.UseVisualStyleBackColor = True
        '
        'cboCompany
        '
        Me.cboCompany.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboCompany.DropDownWidth = 350
        Me.cboCompany.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboCompany.FormattingEnabled = True
        Me.cboCompany.Location = New System.Drawing.Point(385, 11)
        Me.cboCompany.Name = "cboCompany"
        Me.cboCompany.Size = New System.Drawing.Size(182, 21)
        Me.cboCompany.TabIndex = 237
        '
        'lblViewBy
        '
        Me.lblViewBy.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblViewBy.Location = New System.Drawing.Point(573, 13)
        Me.lblViewBy.Name = "lblViewBy"
        Me.lblViewBy.Size = New System.Drawing.Size(58, 16)
        Me.lblViewBy.TabIndex = 236
        Me.lblViewBy.Text = "View By"
        Me.lblViewBy.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.lblViewBy.Visible = False
        '
        'radApplyAll
        '
        Me.radApplyAll.Checked = True
        Me.radApplyAll.Location = New System.Drawing.Point(91, 38)
        Me.radApplyAll.Name = "radApplyAll"
        Me.radApplyAll.Size = New System.Drawing.Size(123, 16)
        Me.radApplyAll.TabIndex = 2
        Me.radApplyAll.TabStop = True
        Me.radApplyAll.Text = "Apply To All"
        Me.radApplyAll.UseVisualStyleBackColor = True
        '
        'lblCompany
        '
        Me.lblCompany.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCompany.Location = New System.Drawing.Point(316, 13)
        Me.lblCompany.Name = "lblCompany"
        Me.lblCompany.Size = New System.Drawing.Size(63, 16)
        Me.lblCompany.TabIndex = 236
        Me.lblCompany.Text = "Company"
        Me.lblCompany.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'btnSet
        '
        Me.btnSet.BackColor = System.Drawing.Color.White
        Me.btnSet.BackgroundImage = CType(resources.GetObject("btnSet.BackgroundImage"), System.Drawing.Image)
        Me.btnSet.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnSet.BorderColor = System.Drawing.Color.Empty
        Me.btnSet.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnSet.FlatAppearance.BorderSize = 0
        Me.btnSet.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnSet.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSet.ForeColor = System.Drawing.Color.Black
        Me.btnSet.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnSet.GradientForeColor = System.Drawing.Color.Black
        Me.btnSet.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSet.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnSet.Location = New System.Drawing.Point(222, 38)
        Me.btnSet.Name = "btnSet"
        Me.btnSet.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSet.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnSet.Size = New System.Drawing.Size(82, 38)
        Me.btnSet.TabIndex = 4
        Me.btnSet.Text = "&Set Date"
        Me.btnSet.UseVisualStyleBackColor = True
        '
        'lblCaption
        '
        Me.lblCaption.Location = New System.Drawing.Point(6, 13)
        Me.lblCaption.Name = "lblCaption"
        Me.lblCaption.Size = New System.Drawing.Size(81, 17)
        Me.lblCaption.TabIndex = 1
        Me.lblCaption.Text = "Relogin Date"
        Me.lblCaption.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'dtpReloginDate
        '
        Me.dtpReloginDate.Checked = False
        Me.dtpReloginDate.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpReloginDate.Location = New System.Drawing.Point(91, 11)
        Me.dtpReloginDate.Name = "dtpReloginDate"
        Me.dtpReloginDate.ShowCheckBox = True
        Me.dtpReloginDate.Size = New System.Drawing.Size(113, 21)
        Me.dtpReloginDate.TabIndex = 0
        '
        'objchkAll
        '
        Me.objchkAll.AutoSize = True
        Me.objchkAll.Location = New System.Drawing.Point(7, 5)
        Me.objchkAll.Name = "objchkAll"
        Me.objchkAll.Size = New System.Drawing.Size(15, 14)
        Me.objchkAll.TabIndex = 1
        Me.objchkAll.UseVisualStyleBackColor = True
        '
        'dgvData
        '
        Me.dgvData.AllowUserToAddRows = False
        Me.dgvData.AllowUserToDeleteRows = False
        Me.dgvData.AllowUserToResizeColumns = False
        Me.dgvData.AllowUserToResizeRows = False
        Me.dgvData.BackgroundColor = System.Drawing.Color.White
        Me.dgvData.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.dgvData.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.RaisedHorizontal
        Me.dgvData.ColumnHeadersHeight = 22
        Me.dgvData.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing
        Me.dgvData.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.dgcolhIsCheck, Me.dgcolhUser, Me.dgcolhLoginDate, Me.dgcolhLogoutDate, Me.dgcolhIP, Me.dgcolhMachine, Me.dgcolhReloginDate})
        Me.dgvData.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dgvData.Location = New System.Drawing.Point(0, 0)
        Me.dgvData.Name = "dgvData"
        Me.dgvData.RowHeadersVisible = False
        Me.dgvData.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing
        Me.dgvData.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvData.Size = New System.Drawing.Size(826, 362)
        Me.dgvData.TabIndex = 0
        '
        'dgcolhIsCheck
        '
        Me.dgcolhIsCheck.HeaderText = ""
        Me.dgcolhIsCheck.Name = "dgcolhIsCheck"
        Me.dgcolhIsCheck.Width = 25
        '
        'dgcolhUser
        '
        Me.dgcolhUser.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.dgcolhUser.HeaderText = "User"
        Me.dgcolhUser.Name = "dgcolhUser"
        Me.dgcolhUser.ReadOnly = True
        Me.dgcolhUser.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgcolhUser.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'dgcolhLoginDate
        '
        Me.dgcolhLoginDate.HeaderText = "Login Date"
        Me.dgcolhLoginDate.Name = "dgcolhLoginDate"
        Me.dgcolhLoginDate.ReadOnly = True
        Me.dgcolhLoginDate.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgcolhLoginDate.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'dgcolhLogoutDate
        '
        Me.dgcolhLogoutDate.HeaderText = "Logout Date"
        Me.dgcolhLogoutDate.Name = "dgcolhLogoutDate"
        Me.dgcolhLogoutDate.ReadOnly = True
        Me.dgcolhLogoutDate.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgcolhLogoutDate.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'dgcolhIP
        '
        Me.dgcolhIP.HeaderText = "IP Address"
        Me.dgcolhIP.Name = "dgcolhIP"
        Me.dgcolhIP.ReadOnly = True
        Me.dgcolhIP.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgcolhIP.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.dgcolhIP.Width = 120
        '
        'dgcolhMachine
        '
        Me.dgcolhMachine.HeaderText = "Machine"
        Me.dgcolhMachine.Name = "dgcolhMachine"
        Me.dgcolhMachine.ReadOnly = True
        Me.dgcolhMachine.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgcolhMachine.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.dgcolhMachine.Width = 120
        '
        'dgcolhReloginDate
        '
        Me.dgcolhReloginDate.HeaderText = "Relogin Date"
        Me.dgcolhReloginDate.Name = "dgcolhReloginDate"
        Me.dgcolhReloginDate.ReadOnly = True
        Me.dgcolhReloginDate.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgcolhReloginDate.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Programmatic
        '
        'EZeeFooter1
        '
        Me.EZeeFooter1.BorderColor = System.Drawing.Color.Silver
        Me.EZeeFooter1.Controls.Add(Me.btnLogout)
        Me.EZeeFooter1.Controls.Add(Me.btnClose)
        Me.EZeeFooter1.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.EZeeFooter1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.EZeeFooter1.GradiantStyle = eZee.Common.eZeeFooter.GradientStyle.Up
        Me.EZeeFooter1.GradientColor1 = System.Drawing.SystemColors.Control
        Me.EZeeFooter1.GradientColor2 = System.Drawing.SystemColors.Control
        Me.EZeeFooter1.Location = New System.Drawing.Point(0, 479)
        Me.EZeeFooter1.Name = "EZeeFooter1"
        Me.EZeeFooter1.Size = New System.Drawing.Size(832, 55)
        Me.EZeeFooter1.TabIndex = 1
        '
        'btnLogout
        '
        Me.btnLogout.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnLogout.BackColor = System.Drawing.Color.White
        Me.btnLogout.BackgroundImage = CType(resources.GetObject("btnLogout.BackgroundImage"), System.Drawing.Image)
        Me.btnLogout.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnLogout.BorderColor = System.Drawing.Color.Empty
        Me.btnLogout.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnLogout.FlatAppearance.BorderSize = 0
        Me.btnLogout.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnLogout.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnLogout.ForeColor = System.Drawing.Color.Black
        Me.btnLogout.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnLogout.GradientForeColor = System.Drawing.Color.Black
        Me.btnLogout.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnLogout.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnLogout.Location = New System.Drawing.Point(610, 13)
        Me.btnLogout.Name = "btnLogout"
        Me.btnLogout.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnLogout.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnLogout.Size = New System.Drawing.Size(102, 30)
        Me.btnLogout.TabIndex = 1
        Me.btnLogout.Text = "&Logout"
        Me.btnLogout.UseVisualStyleBackColor = True
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnClose.BackColor = System.Drawing.Color.White
        Me.btnClose.BackgroundImage = CType(resources.GetObject("btnClose.BackgroundImage"), System.Drawing.Image)
        Me.btnClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnClose.BorderColor = System.Drawing.Color.Empty
        Me.btnClose.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnClose.FlatAppearance.BorderSize = 0
        Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClose.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.ForeColor = System.Drawing.Color.Black
        Me.btnClose.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnClose.GradientForeColor = System.Drawing.Color.Black
        Me.btnClose.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Location = New System.Drawing.Point(718, 13)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Size = New System.Drawing.Size(102, 30)
        Me.btnClose.TabIndex = 1
        Me.btnClose.Text = "&Close"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'frmForceLogout
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(832, 534)
        Me.Controls.Add(Me.Panel1)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.KeyPreview = True
        Me.Margin = New System.Windows.Forms.Padding(2, 3, 2, 3)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmForceLogout"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Logout User(s) Forcefully"
        Me.Panel1.ResumeLayout(False)
        Me.TableLayoutPanel1.ResumeLayout(False)
        Me.TableLayoutPanel1.PerformLayout()
        Me.SplitContainer1.Panel1.ResumeLayout(False)
        Me.SplitContainer1.Panel1.PerformLayout()
        Me.SplitContainer1.Panel2.ResumeLayout(False)
        Me.SplitContainer1.Panel2.PerformLayout()
        Me.SplitContainer1.ResumeLayout(False)
        CType(Me.dgvData, System.ComponentModel.ISupportInitialize).EndInit()
        Me.EZeeFooter1.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents EZeeFooter1 As eZee.Common.eZeeFooter
    Friend WithEvents btnClose As eZee.Common.eZeeLightButton
    Friend WithEvents btnLogout As eZee.Common.eZeeLightButton
    Friend WithEvents TableLayoutPanel1 As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents txtSearch As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents dgvData As System.Windows.Forms.DataGridView
    Friend WithEvents SplitContainer1 As System.Windows.Forms.SplitContainer
    Friend WithEvents objchkAll As System.Windows.Forms.CheckBox
    Friend WithEvents btnSet As eZee.Common.eZeeLightButton
    Friend WithEvents radApplyToChecked As System.Windows.Forms.RadioButton
    Friend WithEvents radApplyAll As System.Windows.Forms.RadioButton
    Friend WithEvents lblCaption As System.Windows.Forms.Label
    Friend WithEvents dtpReloginDate As System.Windows.Forms.DateTimePicker
    Friend WithEvents objStLine As eZee.Common.eZeeStraightLine
    Friend WithEvents lblLogoutReason As System.Windows.Forms.Label
    Friend WithEvents txtReason As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents cboViewBy As System.Windows.Forms.ComboBox
    Friend WithEvents lblViewBy As System.Windows.Forms.Label
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents cboCompany As System.Windows.Forms.ComboBox
    Friend WithEvents lblCompany As System.Windows.Forms.Label
    Friend WithEvents dgcolhIsCheck As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents dgcolhUser As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhLoginDate As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhLogoutDate As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhIP As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhMachine As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhReloginDate As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents btnSearch As eZee.Common.eZeeGradientButton
    Friend WithEvents btnReset As eZee.Common.eZeeGradientButton
    Friend WithEvents dtpTime As System.Windows.Forms.DateTimePicker
End Class
