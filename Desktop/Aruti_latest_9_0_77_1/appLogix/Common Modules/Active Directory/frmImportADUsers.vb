﻿Option Strict On

Imports eZeeCommonLib
Imports Aruti.Data
Imports System.DirectoryServices
Imports System.IO
'Imports System.DirectoryServices.AccountManagement

Public Class frmImportADUsers

#Region " Private Variable(s) "

    Private ReadOnly mstrModuleName As String = "frmImportADUsers"
    Private mblnCancel As Boolean = True
    Private menAction As enAction = enAction.ADD_ONE
    Private dsUser As DataSet
    Private dtTable As DataTable
    Dim objUser As clsUserAddEdit
    Private imgPlusIcon As Image = My.Resources.plus_blue
    Private imgMinusIcon As Image = My.Resources.minus_blue
    Private imgBlankIcon As Image = My.Resources.blankImage


    'Pinkal (03-Apr-2017) -- Start
    'Enhancement - Working On Active directory Changes for PACRA.
    Dim dvUser As DataView = Nothing
    'Pinkal (03-Apr-2017) -- End

#End Region

#Region " Private Function(s) & Method(s) "


    'Pinkal (12-Jul-2018) -- Start
    'Enhancement - Changed for NMB for Performance Enhancement.

    'Private Function GetADUser() As DataTable
    '    Try


    '        'Pinkal (03-Apr-2017) -- Start
    '        'Enhancement - Working On Active directory Changes for PACRA.
    '        'Dim objGSettings As New clsGeneralSettings
    '        'objGSettings._Section = gApplicationType.ToString

    '        'If objGSettings._AD_ServerIP.Trim.Length <= 0 Then
    '        '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 6, "Active directory server address cannot be blank. Please set the active directroy server address."), enMsgBoxStyle.Information)
    '        '    Return Nothing
    '        'End If

    '        Dim objConfig As New clsConfigOptions
    '        Dim mstrADIPAddress As String = ""
    '        Dim mstrADDomain As String = ""
    '        Dim mstrADUserName As String = ""
    '        Dim mstrADUserPwd As String = ""

    '        objConfig.IsValue_Changed("ADIPAddress", "-999", mstrADIPAddress)
    '        objConfig.IsValue_Changed("ADDomain", "-999", mstrADDomain)
    '        objConfig.IsValue_Changed("ADDomainUser", "-999", mstrADUserName)
    '        objConfig.IsValue_Changed("ADDomainUserPwd", "-999", mstrADUserPwd)

    '        If mstrADUserPwd.Trim.Length > 0 Then
    '            mstrADUserPwd = clsSecurity.Decrypt(mstrADUserPwd, "ezee")
    '        End If

    '        'Pinkal (03-Apr-2017) -- End


    '        'Pinkal (03-Apr-2017) -- Start
    '        'Enhancement - Working On Active directory Changes for PACRA.
    '        'objSearch.SearchRoot = New DirectoryEntry("LDAP://" & objGSettings._AD_ServerIP)
    '        Dim ar() As String = Nothing
    '        If mstrADDomain.Trim.Length > 0 AndAlso mstrADDomain.Trim.Contains(".") Then
    '            ar = mstrADDomain.Trim.Split(CChar("."))
    '            mstrADDomain = ""
    '            If ar.Length > 0 Then
    '                For i As Integer = 0 To ar.Length - 1
    '                    mstrADDomain &= ",DC=" & ar(i)
    '                Next
    '            End If
    '        End If

    '        If mstrADDomain.Trim.Length > 0 Then
    '            mstrADDomain = mstrADDomain.Trim.Substring(1)
    '        End If

    '        Dim objSearch As New DirectorySearcher()
    '        objSearch.SearchRoot = New DirectoryEntry("LDAP:// " & mstrADIPAddress & "/" & mstrADDomain, mstrADUserName, mstrADUserPwd)

    '        'Pinkal (03-Apr-2017) -- End


    '        'Pinkal (28-Nov-2017) -- Start
    '        'Enhancement -  issue # 0001617: Active Directory Integration, Common Active Directory Integration for (SUMATRA,PACRA,THPS).
    '        objSearch.Filter = "(&(objectCategory=user))"
    '        'objSearch.Filter = "(objectCategory=organizationalUnit)"
    '        'Pinkal (28-Nov-2017) -- End

    '        objSearch.SearchScope = SearchScope.Subtree
    '        objSearch.ReferralChasing = ReferralChasingOption.All
    '        objSearch.Sort.Direction = SortDirection.Ascending
    '        Dim srcResult As SearchResultCollection = objSearch.FindAll


    '        If srcResult.Count > 0 Then

    '            For Each sr As SearchResult In srcResult

    '                'Dim objDirectory As DirectoryEntry = sr.GetDirectoryEntry()
    '                Dim DCName As String = sr.GetDirectoryEntry.Properties("distinguishedname").Value.ToString()
    '                'Dim strDomain As String() = sr.GetDirectoryEntry.Properties("distinguishedname").Value.ToString().Split(CChar(","))

    '                'If strDomain.Length > 0 Then
    '                '    For i As Integer = 0 To strDomain.Length - 1

    '                '        If strDomain(i).Contains("DC=") Then
    '                '            DCName = strDomain(i).Replace("DC=", "")
    '                '            Exit For
    '                '        End If
    '                '    Next
    '                'End If


    '                'If sr.GetDirectoryEntry.Properties("userAccountControl").Value IsNot DBNull.Value Then
    '                '    Dim flags As Integer = CInt(sr.GetDirectoryEntry.Properties("userAccountControl").Value)
    '                '    If Convert.ToBoolean(flags And ADS_UF_ACCOUNTDISABLE) Then
    '                '        Continue For
    '                '    End If
    '                'End If

    '                'END FOR CHECK WHETHER USER ACCOUNT IS DISABLE OR NOT

    '                'Pinkal (28-Nov-2017) -- Start
    '                'Enhancement -  issue # 0001617: Active Directory Integration, Common Active Directory Integration for (SUMATRA,PACRA,THPS).

    '                'Dim objSrch As New DirectorySearcher()
    '                'objSrch.SearchRoot = New DirectoryEntry("LDAP:// " & mstrADIPAddress & "/" & DCName, mstrADUserName, mstrADUserPwd)


    '                'objSrch.Filter = "(&(objectCategory=user))"
    '                'objSearch.Filter = "(objectCategory=organizationalUnit)"
    '                'objSrch.SearchScope = SearchScope.Subtree
    '                'objSrch.ReferralChasing = ReferralChasingOption.All
    '                'objSrch.Sort.Direction = SortDirection.Ascending
    '                'Dim objSrchResult As SearchResultCollection = objSrch.FindAll

    '                'If objSrchResult.Count > 0 Then

    '                'Pinkal (28-Nov-2017) -- End



    '                    Dim mstrGroup As String = ""
    '                    Dim mintGroupId As Integer = 0
    '                    Const ADS_UF_ACCOUNTDISABLE As Integer = &H2

    '                'Pinkal (28-Nov-2017) -- Start
    '                ''Enhancement -  issue # 0001617: Active Directory Integration, Common Active Directory Integration for (SUMATRA,PACRA,THPS).
    '                'For Each src As SearchResult In objSrchResult


    '                If sr.GetDirectoryEntry.Properties("userAccountControl").Value IsNot DBNull.Value Then
    '                    Dim flags As Integer = CInt(sr.GetDirectoryEntry.Properties("userAccountControl").Value)
    '                            If Convert.ToBoolean(flags And ADS_UF_ACCOUNTDISABLE) Then
    '                                Continue For
    '                            End If
    '                        End If


    '                        ' START FOR CHECK WHETHER IS EXIST IN TABLE IS YES THEN NOT ADD IN THIS DATATABLE
    '                        Try
    '                    If sr.GetDirectoryEntry.Properties("SamAccountName").Value IsNot DBNull.Value Then
    '                        If objUser.isExist(sr.GetDirectoryEntry.Properties("SamAccountName").Value.ToString()) Then Continue For
    '                            End If
    '                        Catch ex As Exception
    '                            Continue For
    '                        End Try
    '                ' END FOR CHECK WHETHER IS EXIST IN TABLE IS YES THEN NOT ADD IN THIS DATATABLE

    '                Dim dtRow() As DataRow = dsUser.Tables(0).Select("username= '" & sr.GetDirectoryEntry.Properties("SamAccountName").Value.ToString().Replace("'", "''") & "'")
    '                        If dtRow.Length > 0 Then Continue For

    '                Dim dRow As DataRow = dsUser.Tables(0).NewRow

    '                'If mstrGroup <> sr.GetDirectoryEntry().Parent.Name.Remove(0, 3) Then

    '                        '    Dim drRow As DataRow() = dsUser.Tables(0).Select("pname='" & src.GetDirectoryEntry().Parent.Name.Remove(0, 3).ToUpper() & "'")

    '                '    If drRow.Length = 0 Then
    '                '        mintGroupId += 1
    '                '        dRow("pname") = src.GetDirectoryEntry().Parent.Name.Remove(0, 3).ToUpper()
    '                '        dRow("GroupId") = mintGroupId
    '                '        dRow("IsGroup") = True
    '                '        dRow("ischeck") = False
    '                        '        mstrGroup = src.GetDirectoryEntry().Parent.Name.Remove(0, 3)
    '                '        dsUser.Tables(0).Rows.Add(dRow)
    '                '        Continue For
    '                '    Else
    '                '        dRow("GroupId") = drRow(0)("GroupId")
    '                '    End If
    '                'Else
    '                '    dRow("GroupId") = mintGroupId
    '                'End If


    '                Try
    '                    If sr.GetDirectoryEntry.Properties("SamAccountName").Value IsNot DBNull.Value Then
    '                        If sr.GetDirectoryEntry.Properties("SamAccountName").Value.ToString().Trim.Length <= 0 Then Continue For
    '                        dRow("username") = sr.GetDirectoryEntry.Properties("SamAccountName").Value.ToString()
    '                                'dRow("pname") = Space(5) & Convert.ToString(src.GetDirectoryEntry.Properties("SamAccountName").Value)
    '                        dRow("pname") = Convert.ToString(sr.GetDirectoryEntry.Properties("SamAccountName").Value)
    '                    End If
    '                Catch
    '                    dRow("username") = ""
    '                    dRow("pname") = ""
    '                End Try

    '                Try
    '                    If sr.GetDirectoryEntry.Properties("givenName").Value IsNot DBNull.Value Then
    '                        dRow("firstname") = sr.GetDirectoryEntry.Properties("givenName").Value.ToString()
    '                    End If
    '                Catch
    '                    dRow("firstname") = ""
    '                End Try


    '                Try
    '                    If sr.GetDirectoryEntry.Properties("sn").Value IsNot DBNull.Value Then
    '                        dRow("lastname") = sr.GetDirectoryEntry.Properties("sn").Value.ToString()
    '                    End If
    '                Catch
    '                    dRow("lastname") = ""
    '                End Try


    '                Try
    '                    If sr.GetDirectoryEntry.Properties("mail").Value IsNot DBNull.Value Then
    '                        dRow("email") = sr.GetDirectoryEntry.Properties("mail").Value.ToString()
    '                    End If
    '                Catch
    '                    dRow("email") = ""
    '                End Try

    '                Try
    '                    If sr.GetDirectoryEntry.Properties("telephoneNumber").Value IsNot DBNull.Value Then
    '                        dRow("phone") = sr.GetDirectoryEntry.Properties("telephoneNumber").Value.ToString()
    '                    End If
    '                Catch
    '                    dRow("phone") = ""
    '                End Try


    '                Try
    '                    If sr.GetDirectoryEntry.Properties("streetAddress").Value IsNot DBNull.Value Then
    '                        dRow("address1") = sr.GetDirectoryEntry.Properties("streetAddress").Value.ToString()
    '                    End If
    '                Catch
    '                    dRow("address1") = ""
    '                End Try


    '                Dim mstrAddress2 As String = ""

    '                If sr.GetDirectoryEntry.Properties("l").Value IsNot DBNull.Value Then
    '                    Try
    '                        mstrAddress2 = sr.GetDirectoryEntry.Properties("l").Value.ToString()
    '                    Catch
    '                        mstrAddress2 = ""
    '                    End Try

    '                End If

    '                If sr.GetDirectoryEntry.Properties("postalCode").Value IsNot DBNull.Value Then
    '                    Try
    '                        mstrAddress2 &= " - " & sr.GetDirectoryEntry.Properties("postalCode").Value.ToString() & " , "
    '                    Catch
    '                        mstrAddress2 &= CStr(IIf(mstrAddress2.Trim.Length > 0, " - ", ""))
    '                    End Try

    '                End If


    '                If sr.GetDirectoryEntry.Properties("st").Value IsNot DBNull.Value Then
    '                    Try
    '                        mstrAddress2 &= sr.GetDirectoryEntry.Properties("st").Value.ToString() & " , "
    '                    Catch
    '                        mstrAddress2 &= CStr(IIf(mstrAddress2.Trim.Length > 0, " - ", ""))
    '                    End Try

    '                End If

    '                If sr.GetDirectoryEntry.Properties("co").Value IsNot DBNull.Value Then
    '                    Try
    '                        mstrAddress2 &= sr.GetDirectoryEntry.Properties("co").Value.ToString()
    '                    Catch
    '                        mstrAddress2 &= ""
    '                    End Try

    '                End If

    '                dRow("address2") = mstrAddress2
    '                dRow("password") = ""
    '                dRow("roleunkid") = 0
    '                dRow("exp_days") = 0
    '                dRow("ispasswordexpire") = False
    '                dRow("userunkid") = -1
    '                dRow("ischeck") = False
    '                dRow("isrighttoleft") = False
    '                dRow("languageunkid") = 0
    '                dRow("isactive") = True
    '                dRow("IsGroup") = False
    '                dRow("isaduser") = True
    '                dRow("userdomain") = DCName
    '                        dRow("GroupId") = mintGroupId
    '                dsUser.Tables(0).Rows.Add(dRow) '
    '                'Next
    '                'End If
    '                'objSrch.SearchRoot.Close()

    '                'Pinkal (28-Nov-2017) -- End

    '            Next
    '            objSearch.SearchRoot.Close()

    '            'START FOR WHEN GROUP HAS NO USER THEN REMOVE THAT GROUP FROM DATASET

    '            If dsUser IsNot Nothing Then

    '                'Pinkal (03-Apr-2017) -- Start
    '                'Enhancement - Working On Active directory Changes for PACRA.

    '                'Dim drGrpRow As DataRow() = dsUser.Tables(0).Select("IsGroup=true")

    '                'If drGrpRow.Length > 0 Then

    '                '    For i As Integer = 0 To drGrpRow.Length - 1

    '                '        Dim dr As DataRow() = dsUser.Tables(0).Select("GroupId=" & drGrpRow(i)("GroupId").ToString() & " AND IsGroup = False")

    '                '        If dr.Length = 0 Then
    '                '            dsUser.Tables(0).Rows.Remove(drGrpRow(i))
    '                '        Else
    '                '            Continue For
    '                '        End If

    '                '    Next

    '                'End If

    '                'dtTable = New DataView(dsUser.Tables(0), "", "GroupId,username asc", DataViewRowState.CurrentRows).ToTable()
    '                dtTable = New DataView(dsUser.Tables(0), "", "username asc", DataViewRowState.CurrentRows).ToTable()

    '                'Pinkal (03-Apr-2017) -- End

    '                'END FOR WHEN GROUP HAS NO USER THEN REMOVE THAT GROUP FROM DATASET



    '            End If

    '        Else
    '            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "There is no user(s) to import data."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
    '            Return Nothing
    '        End If

    '    Catch ex As Exception
    '        Dim strMessage As String = Language.getMessage(mstrModuleName, 5, " Or does not contains the Active Directory Users. Please set correct server to use this feature.")
    '        eZeeMsgBox.Show(ex.Message & strMessage, enMsgBoxStyle.Information)
            '    Return Nothing
    '    End Try
    '    Return dtTable
    'End Function


    Private Function GetADUser() As DataTable
        Try

            Dim objConfig As New clsConfigOptions
            Dim mstrADIPAddress As String = ""
            Dim mstrADDomain As String = ""
            Dim mstrADUserName As String = ""
            Dim mstrADUserPwd As String = ""

            objConfig.IsValue_Changed("ADIPAddress", "-999", mstrADIPAddress)
            objConfig.IsValue_Changed("ADDomain", "-999", mstrADDomain)
            objConfig.IsValue_Changed("ADDomainUser", "-999", mstrADUserName)
            objConfig.IsValue_Changed("ADDomainUserPwd", "-999", mstrADUserPwd)

            If mstrADUserPwd.Trim.Length > 0 Then
                mstrADUserPwd = clsSecurity.Decrypt(mstrADUserPwd, "ezee")
            End If

            Dim ar() As String = Nothing
            If mstrADDomain.Trim.Length > 0 AndAlso mstrADDomain.Trim.Contains(".") Then
                ar = mstrADDomain.Trim.Split(CChar("."))
                mstrADDomain = ""
                If ar.Length > 0 Then
                    For i As Integer = 0 To ar.Length - 1
                        mstrADDomain &= ",DC=" & ar(i)
                    Next
                End If
            End If

            If mstrADDomain.Trim.Length > 0 Then
                mstrADDomain = mstrADDomain.Trim.Substring(1)
            End If

            Dim objSearch As New DirectorySearcher()
            objSearch.SearchRoot = New DirectoryEntry("LDAP:// " & mstrADIPAddress & "/" & mstrADDomain, mstrADUserName, mstrADUserPwd)

            objSearch.Filter = "(&(objectCategory=user))"
            objSearch.SearchScope = SearchScope.Subtree
            objSearch.ReferralChasing = ReferralChasingOption.All
            objSearch.Sort.Direction = SortDirection.Ascending
            'Pinkal (07-Mar-2019) -- Start
            'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.
            objSearch.PageSize = 1000
            'Pinkal (07-Mar-2019) -- End
            Dim srcResult As SearchResultCollection = objSearch.FindAll


            If srcResult.Count > 0 Then

                For Each sr As SearchResult In srcResult

                    'Pinkal (04-Apr-2019) -- Start
                    'Optimization - Worked on Sports Paisa.


                    'Pinkal (15-Feb-2020) -- Start
                    'Enhancement NMB AD -   Active Directory Integration Requirement For NMB [Ref No : 273].
                    'Dim objEntry As DirectoryEntry = New DirectoryEntry(sr.Path)
                    Dim objEntry As DirectoryEntry = New DirectoryEntry(sr.Path, mstrADUserName, mstrADUserPwd)
                    'Pinkal (15-Feb-2020) -- End




                    'Pinkal (29-Jul-2019) -- Start
                    'Enhancement - Working on Papaaye Shift Overriding feature On Employee Shift Assignment.
                    If objEntry.Properties.Contains("distinguishedname") = False Then
                        objEntry.Close()
                        Continue For
                    End If
                    'Pinkal (29-Jul-2019) -- End

                    Dim DCName As String = objEntry.Properties("distinguishedname").Value.ToString()
                        Dim mstrGroup As String = ""
                        Dim mintGroupId As Integer = 0
                        Const ADS_UF_ACCOUNTDISABLE As Integer = &H2

                    If objEntry.Properties("userAccountControl").Value IsNot DBNull.Value Then
                        Dim flags As Integer = CInt(objEntry.Properties("userAccountControl").Value)
                                If Convert.ToBoolean(flags And ADS_UF_ACCOUNTDISABLE) Then
                                    Continue For
                                End If
                            End If


                            ' START FOR CHECK WHETHER IS EXIST IN TABLE IS YES THEN NOT ADD IN THIS DATATABLE
                            Try
                        If objEntry.Properties("SamAccountName").Value IsNot DBNull.Value Then
                            If objUser.isExist(objEntry.Properties("SamAccountName").Value.ToString()) Then Continue For
                                End If
                            Catch ex As Exception
                                Continue For
                            End Try
                    ' END FOR CHECK WHETHER IS EXIST IN TABLE IS YES THEN NOT ADD IN THIS DATATABLE

                    Dim dtRow() As DataRow = dsUser.Tables(0).Select("username= '" & objEntry.Properties("SamAccountName").Value.ToString().Replace("'", "''") & "'")
                            If dtRow.Length > 0 Then Continue For

                    Dim dRow As DataRow = dsUser.Tables(0).NewRow
                    Try
                        If objEntry.Properties("SamAccountName").Value IsNot DBNull.Value Then
                            If objEntry.Properties("SamAccountName").Value.ToString().Trim.Length <= 0 Then Continue For
                            dRow("username") = objEntry.Properties("SamAccountName").Value.ToString()
                            dRow("pname") = Convert.ToString(objEntry.Properties("SamAccountName").Value)
                        End If
                    Catch
                        dRow("username") = ""
                        dRow("pname") = ""
                    End Try

                    Try
                        If objEntry.Properties("givenName").Value IsNot DBNull.Value Then
                            dRow("firstname") = objEntry.Properties("givenName").Value.ToString()
                        End If
                    Catch
                        dRow("firstname") = ""
                    End Try


                    Try
                        If objEntry.Properties("sn").Value IsNot DBNull.Value Then
                            dRow("lastname") = objEntry.Properties("sn").Value.ToString()
                        End If
                    Catch
                        dRow("lastname") = ""
                    End Try


                    Try
                        If objEntry.Properties("mail").Value IsNot DBNull.Value Then
                            dRow("email") = objEntry.Properties("mail").Value.ToString()
                        End If
                    Catch
                        dRow("email") = ""
                    End Try

                    Try
                        If objEntry.Properties("telephoneNumber").Value IsNot DBNull.Value Then
                            dRow("phone") = objEntry.Properties("telephoneNumber").Value.ToString()
                        End If
                    Catch
                        dRow("phone") = ""
                    End Try


                    Try
                        If objEntry.Properties("streetAddress").Value IsNot DBNull.Value Then
                            dRow("address1") = objEntry.Properties("streetAddress").Value.ToString()
                        End If
                    Catch
                        dRow("address1") = ""
                    End Try


                    Dim mstrAddress2 As String = ""

                    If objEntry.Properties("l").Value IsNot DBNull.Value Then
                        Try
                            mstrAddress2 = objEntry.Properties("l").Value.ToString()
                        Catch
                            mstrAddress2 = ""
                        End Try

                    End If

                    If objEntry.Properties("postalCode").Value IsNot DBNull.Value Then
                        Try
                            mstrAddress2 &= " - " & objEntry.Properties("postalCode").Value.ToString() & " , "
                        Catch
                            mstrAddress2 &= CStr(IIf(mstrAddress2.Trim.Length > 0, " - ", ""))
                        End Try

                    End If


                    If objEntry.Properties("st").Value IsNot DBNull.Value Then
                        Try
                            mstrAddress2 &= objEntry.Properties("st").Value.ToString() & " , "
                        Catch
                            mstrAddress2 &= CStr(IIf(mstrAddress2.Trim.Length > 0, " - ", ""))
                        End Try

                    End If

                    If objEntry.Properties("co").Value IsNot DBNull.Value Then
                        Try
                            mstrAddress2 &= objEntry.Properties("co").Value.ToString()
                        Catch
                            mstrAddress2 &= ""
                        End Try

                    End If

                    'Dim DCName As String = sr.GetDirectoryEntry().Properties("distinguishedname").Value.ToString()
                    'Dim mstrGroup As String = ""
                    'Dim mintGroupId As Integer = 0
                    'Const ADS_UF_ACCOUNTDISABLE As Integer = &H2

                    'If sr.GetDirectoryEntry().Properties("userAccountControl").Value IsNot DBNull.Value Then
                    '    Dim flags As Integer = CInt(sr.GetDirectoryEntry().Properties("userAccountControl").Value)
                    '    If Convert.ToBoolean(flags And ADS_UF_ACCOUNTDISABLE) Then
                    '        Continue For
                    '    End If
                    'End If


                    '' START FOR CHECK WHETHER IS EXIST IN TABLE IS YES THEN NOT ADD IN THIS DATATABLE
                    'Try
                    '    If sr.GetDirectoryEntry().Properties("SamAccountName").Value IsNot DBNull.Value Then
                    '        If objUser.isExist(sr.GetDirectoryEntry().Properties("SamAccountName").Value.ToString()) Then Continue For
                    '    End If
                    'Catch ex As Exception
                    '    Continue For
                    'End Try
                    '' END FOR CHECK WHETHER IS EXIST IN TABLE IS YES THEN NOT ADD IN THIS DATATABLE

                    'Dim dtRow() As DataRow = dsUser.Tables(0).Select("username= '" & sr.GetDirectoryEntry().Properties("SamAccountName").Value.ToString().Replace("'", "''") & "'")
                    'If dtRow.Length > 0 Then Continue For

                    'Dim dRow As DataRow = dsUser.Tables(0).NewRow
                    'Try
                    '    If sr.GetDirectoryEntry().Properties("SamAccountName").Value IsNot DBNull.Value Then
                    '        If sr.GetDirectoryEntry().Properties("SamAccountName").Value.ToString().Trim.Length <= 0 Then Continue For
                    '        dRow("username") = sr.GetDirectoryEntry().Properties("SamAccountName").Value.ToString()
                    '        dRow("pname") = Convert.ToString(sr.GetDirectoryEntry().Properties("SamAccountName").Value)
                    '    End If
                    'Catch
                    '    dRow("username") = ""
                    '    dRow("pname") = ""
                    'End Try

                    'Try
                    '    If sr.GetDirectoryEntry().Properties("givenName").Value IsNot DBNull.Value Then
                    '        dRow("firstname") = sr.GetDirectoryEntry().Properties("givenName").Value.ToString()
                    '    End If
                    'Catch
                    '    dRow("firstname") = ""
                    'End Try


                    'Try
                    '    If sr.GetDirectoryEntry().Properties("sn").Value IsNot DBNull.Value Then
                    '        dRow("lastname") = sr.GetDirectoryEntry().Properties("sn").Value.ToString()
                    '    End If
                    'Catch
                    '    dRow("lastname") = ""
                    'End Try


                    'Try
                    '    If sr.GetDirectoryEntry().Properties("mail").Value IsNot DBNull.Value Then
                    '        dRow("email") = sr.GetDirectoryEntry().Properties("mail").Value.ToString()
                    '    End If
                    'Catch
                    '    dRow("email") = ""
                    'End Try

                    'Try
                    '    If sr.GetDirectoryEntry().Properties("telephoneNumber").Value IsNot DBNull.Value Then
                    '        dRow("phone") = sr.GetDirectoryEntry().Properties("telephoneNumber").Value.ToString()
                    '    End If
                    'Catch
                    '    dRow("phone") = ""
                    'End Try


                    'Try
                    '    If sr.GetDirectoryEntry().Properties("streetAddress").Value IsNot DBNull.Value Then
                    '        dRow("address1") = sr.GetDirectoryEntry().Properties("streetAddress").Value.ToString()
                    '    End If
                    'Catch
                    '    dRow("address1") = ""
                    'End Try


                    'Dim mstrAddress2 As String = ""

                    'If sr.GetDirectoryEntry().Properties("l").Value IsNot DBNull.Value Then
                    '    Try
                    '        mstrAddress2 = sr.GetDirectoryEntry().Properties("l").Value.ToString()
                    '    Catch
                    '        mstrAddress2 = ""
                    '    End Try

                    'End If

                    'If sr.GetDirectoryEntry().Properties("postalCode").Value IsNot DBNull.Value Then
                    '    Try
                    '        mstrAddress2 &= " - " & sr.GetDirectoryEntry().Properties("postalCode").Value.ToString() & " , "
                    '    Catch
                    '        mstrAddress2 &= CStr(IIf(mstrAddress2.Trim.Length > 0, " - ", ""))
                    '    End Try

                    'End If


                    'If sr.GetDirectoryEntry().Properties("st").Value IsNot DBNull.Value Then
                    '    Try
                    '        mstrAddress2 &= sr.GetDirectoryEntry().Properties("st").Value.ToString() & " , "
                    '    Catch
                    '        mstrAddress2 &= CStr(IIf(mstrAddress2.Trim.Length > 0, " - ", ""))
                    '    End Try

                    'End If

                    'If sr.GetDirectoryEntry().Properties("co").Value IsNot DBNull.Value Then
                    '    Try
                    '        mstrAddress2 &= sr.GetDirectoryEntry().Properties("co").Value.ToString()
                    '    Catch
                    '        mstrAddress2 &= ""
                    '    End Try

                    'End If

                    'Pinkal (01-Apr-2019) -- End

                    dRow("address2") = mstrAddress2
                    dRow("password") = ""
                    dRow("roleunkid") = 0
                    dRow("exp_days") = 0
                    dRow("ispasswordexpire") = False
                    dRow("userunkid") = -1
                    dRow("ischeck") = False
                    dRow("isrighttoleft") = False
                    dRow("languageunkid") = 0
                    dRow("isactive") = True
                    dRow("IsGroup") = False
                    dRow("isaduser") = True
                    dRow("userdomain") = DCName
                            dRow("GroupId") = mintGroupId
                    dsUser.Tables(0).Rows.Add(dRow) '

                    'Pinkal (04-Apr-2019) -- Start
                    'Optimization - Worked on Sports Paisa.
                    'objEntry.Close()
                    sr.GetDirectoryEntry().Close()
                    'Pinkal (04-Apr-2019) -- End

                Next
                objSearch.SearchRoot.Close()

                'START FOR WHEN GROUP HAS NO USER THEN REMOVE THAT GROUP FROM DATASET

                If dsUser IsNot Nothing Then
                    dtTable = New DataView(dsUser.Tables(0), "", "username asc", DataViewRowState.CurrentRows).ToTable()
                End If
                    'END FOR WHEN GROUP HAS NO USER THEN REMOVE THAT GROUP FROM DATASET

            Else
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "There is no user(s) to import data."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                Return Nothing
            End If

        Catch ex As Exception
            Dim strMessage As String = Language.getMessage(mstrModuleName, 5, " Or does not contains the Active Directory Users. Please set correct server to use this feature.")
            eZeeMsgBox.Show(ex.Message & strMessage, enMsgBoxStyle.Information)
            Return Nothing
        End Try
        Return dtTable
    End Function

    'Pinkal (07-Jul-2018) -- End 

    Private Sub FillGrid()
        Try
            dgAdUsers.AutoGenerateColumns = False
            objdgcolhGrpId.DataPropertyName = "GroupId"
            objColhIsGroup.DataPropertyName = "IsGroup"
            objcolhUname.DataPropertyName = "username"
            objdgcolhCheck.DataPropertyName = "ischeck"
            colhUserName.DataPropertyName = "pname"


            'Pinkal (03-Apr-2017) -- Start
            'Enhancement - Working On Active directory Changes for PACRA.
            'dgAdUsers.DataSource = GetADUser()
            Dim dtTable As DataTable = GetADUser()
            If dtTable IsNot Nothing Then dvUser = dtTable.DefaultView
            dgAdUsers.DataSource = dvUser
            'Pinkal (03-Apr-2017) -- End

            'SetGridColor()
            'SetCollapseValue()

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillGrid", mstrModuleName)
        End Try
    End Sub

    Private Sub SetGridColor()
        Try
            Dim dgvcsHeader As New DataGridViewCellStyle
            dgvcsHeader.ForeColor = Color.White
            dgvcsHeader.SelectionBackColor = Color.Gray
            dgvcsHeader.SelectionForeColor = Color.White
            dgvcsHeader.BackColor = Color.Gray
            For i As Integer = 0 To dgAdUsers.RowCount - 1
                If CBool(dgAdUsers.Rows(i).Cells(objColhIsGroup.Index).Value) = True Then
                    dgAdUsers.Rows(i).DefaultCellStyle = dgvcsHeader
                    dgAdUsers.Rows(i).Cells(colhUserName.Index).Style.Font = New Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold)
                End If
            Next
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetGridColor", mstrModuleName)
        End Try
    End Sub

    Private Sub SetCollapseValue()
        Try
            For i As Integer = 0 To dgAdUsers.RowCount - 1
                If CBool(dgAdUsers.Rows(i).Cells(objColhIsGroup.Index).Value) = True Then
                    If dgAdUsers.Rows(i).Cells(objdgcolhCollapse.Index).Value Is Nothing Then
                        dgAdUsers.Rows(i).Cells(objdgcolhCollapse.Index).Value = imgMinusIcon
                    End If
                Else
                    dgAdUsers.Rows(i).Cells(objdgcolhCollapse.Index).Value = imgBlankIcon
                End If
            Next
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetCollapseValue", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Function IsValid() As Boolean
        Try
            If dgAdUsers.Rows.Count = 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "There is no data to do further operation."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                Return False
            End If

            If dsUser.Tables(0).Rows.Count > 0 Then
                Dim drRow As DataRow() = dtTable.Select("ischeck = true AND IsGroup = false")
                If drRow.Length = 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "User is compulsory information.Please select atleast one user to import."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                    Return False
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "IsValid", mstrModuleName)
            Return False
        End Try
        Return True
    End Function

#End Region

#Region " Form's Event(s) "

    Private Sub frmImportADUsers_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            objUser = New clsUserAddEdit

            Call Set_Logo(Me, gApplicationType)
            Language.setLanguage(Me.Name)
            Call OtherSettings()

            dsUser = objUser.GetList("List").Clone()
            If dsUser IsNot Nothing Then
                If dsUser.Tables(0).Columns.Contains("ischeck") = False Then
                    dsUser.Tables(0).Columns.Add("ischeck", Type.GetType("System.Boolean"))
                End If
                If dsUser.Tables(0).Columns.Contains("pname") = False Then
                    dsUser.Tables(0).Columns.Add("pname", Type.GetType("System.String"))
                    dsUser.Tables(0).Columns.Add("GroupId", Type.GetType("System.Int16"))
                    dsUser.Tables(0).Columns.Add("IsGroup", Type.GetType("System.Boolean"))

                    'Pinkal (26-Apr-2017) -- Start
                    'Enhancement - Working On Active directory Changes for PACRA.
                    dsUser.Tables(0).Columns.Add("edisplay", GetType(System.String))
                    dsUser.Tables(0).Columns.Add("efname", GetType(System.String))
                    dsUser.Tables(0).Columns.Add("elname", GetType(System.String))
                    dsUser.Tables(0).Columns.Add("ecode", GetType(System.String))
                    dsUser.Tables(0).Columns.Add("estatus", GetType(System.String))
                    dsUser.Tables(0).Columns.Add("estatusid", GetType(System.Int32))
                    'Pinkal (26-Apr-2017) -- End

                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmImportADUsers_Load", mstrModuleName)
        End Try
    End Sub

    Private Sub frmImportADUsers_LanguageClick(ByVal sender As Object, ByVal eventArgs As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try

            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()
            objfrm.displayDialog(Me)

            Language.Refresh()
            Call Language.setLanguage(Me)

        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "objLanguage_Click", mstrModuleName)
        Finally
            objfrm = Nothing
        End Try
    End Sub

#End Region

#Region " Button's Event(s) "

    Private Sub objbtnSearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearch.Click
        Try

            'Pinkal (03-Apr-2017) -- Start
            'Enhancement - Working On Active directory Changes for PACRA.
            txtSearchUser.Text = ""
            'Pinkal (03-Apr-2017) -- End

            If dsUser IsNot Nothing Then
                dsUser.Tables(0).Rows.Clear()
                Cursor = Cursors.WaitCursor
                FillGrid()
                Cursor = Cursors.Default
            End If
        Catch ex As Exception
            Cursor = Cursors.Default
            DisplayError.Show("-1", ex.Message, "", mstrModuleName)
        End Try
    End Sub


    'Pinkal (21-Jun-2012) -- Start
    'Enhancement : TRA Changes

    'Private Sub btnImport_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
    '    Try
    '        If dgAdUsers.Rows.Count = 0 Then
    '            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "There is no data to do further operation."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
    '            Exit Sub
    '        End If

    '        If dsUser.Tables(0).Rows.Count > 0 Then
    '            Dim drRow As DataRow() = dtTable.Select("ischeck = true AND IsGroup = false")
    '            If drRow.Length = 0 Then
    '                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "User is compulsory information.Please select atleast one user to import."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
    '                Exit Sub
    '            End If
    '        End If


    '        Cursor = Cursors.WaitCursor
    '        objUser._Userunkid = User._Object._Userunkid
    '        objUser._Creation_Date = ConfigParameter._Object._CurrentDateAndTime

    '        Dim dtUser As DataTable = New DataView(dtTable, "ischeck = true AND isaduser = true", "", DataViewRowState.CurrentRows).ToTable()

    '        Dim blnFlag As Boolean = objUser.ImportADUser(dtUser)

    '        If blnFlag = False And objUser._Message <> "" Then
    '            Cursor = Cursors.WaitCursor
    '            eZeeMsgBox.Show(objUser._Message, enMsgBoxStyle.Information)
    '        End If
    '        If blnFlag Then
    '            Cursor = Cursors.Default
    '            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Data successfully Imported."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
    '            Me.Close()
    '        End If
    '    Catch ex As Exception
    '        Cursor = Cursors.WaitCursor
    '        DisplayError.Show("-1", ex.Message, "btnImport_Click", mstrModuleName)
    '    End Try
    'End Sub

    'Pinkal (21-Jun-2012) -- End

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnClose_Click", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " DataGrid's Event(s) "

    Private Sub dgAdUsers_CellContentClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgAdUsers.CellContentClick, dgAdUsers.CellContentDoubleClick
        Try

            If e.RowIndex <= -1 Then Exit Sub

            If Me.dgAdUsers.IsCurrentCellDirty Then
                Me.dgAdUsers.CommitEdit(DataGridViewDataErrorContexts.Commit)
            End If


            'Pinkal (03-Apr-2017) -- Start
            'Enhancement - Working On Active directory Changes for PACRA.


            'If CBool(dgAdUsers.Rows(e.RowIndex).Cells(objColhIsGroup.Index).Value) = True Then

            '    Select Case CInt(e.ColumnIndex)
            '        Case 0
            '            If dgAdUsers.Rows(e.RowIndex).Cells(objdgcolhCollapse.Index).Value Is imgMinusIcon Then
            '                dgAdUsers.Rows(e.RowIndex).Cells(objdgcolhCollapse.Index).Value = imgPlusIcon
            '            Else
            '                dgAdUsers.Rows(e.RowIndex).Cells(objdgcolhCollapse.Index).Value = imgMinusIcon
            '            End If

            '            For i = e.RowIndex + 1 To dgAdUsers.RowCount - 1
            '                If CInt(dgAdUsers.Rows(e.RowIndex).Cells(objdgcolhGrpId.Index).Value) = CInt(dgAdUsers.Rows(i).Cells(objdgcolhGrpId.Index).Value) Then
            '                    If dgAdUsers.Rows(i).Visible = False Then
            '                        dgAdUsers.Rows(i).Visible = True
            '                    Else
            '                        dgAdUsers.Rows(i).Visible = False
            '                    End If
            '                Else
            '                    Exit For
            '                End If
            '            Next

            '        Case 1

            '            Dim blnFlg As Boolean = CBool(dgAdUsers.Rows(e.RowIndex).Cells(objdgcolhCheck.Index).Value)
            '            Dim drRow As DataRow() = dtTable.Select("GroupId = " & CInt(dgAdUsers.Rows(e.RowIndex).Cells(objdgcolhGrpId.Index).Value))

            '            If drRow.Length > 0 Then
            '                For i As Integer = 0 To drRow.Length - 1
            '                    drRow(i)("ischeck") = blnFlg
            '                Next
            '                dgAdUsers.Refresh()
            '            End If


            '    End Select

            'End If

            
            dvUser.ToTable.AcceptChanges()

            'Pinkal (26-Apr-2017) -- Start
            'Enhancement - Working On Active directory Changes for PACRA.
            RemoveHandler objChkAll.CheckedChanged, AddressOf objChkAll_CheckedChanged
            If e.ColumnIndex = objdgcolhCheck.Index Then
                Dim drRow As DataRow() = dvUser.ToTable.Select("ischeck = true")
                If drRow.Length <= 0 Then
                    objChkAll.CheckState = CheckState.Unchecked
                ElseIf drRow.Length < dgAdUsers.Rows.Count Then
                    objChkAll.CheckState = CheckState.Indeterminate
                ElseIf drRow.Length = dgAdUsers.Rows.Count Then
                    objChkAll.CheckState = CheckState.Checked
                End If
            End If
            AddHandler objChkAll.CheckedChanged, AddressOf objChkAll_CheckedChanged
            'Pinkal (26-Apr-2017) -- End

            'Pinkal (03-Apr-2017) -- End




        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgAdUsers_CellContentClick", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

#Region "ContextMenu Event"

    'Pinkal (26-Apr-2017) -- Start
    'Enhancement - Working On Active directory Changes for PACRA.

    'Private Sub mnuImportManagerUser_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuImportManagerUser.Click
    '    Try

    '        If IsValid() = False Then Exit Sub

    '        Cursor = Cursors.WaitCursor
    '        objUser._Userunkid = User._Object._Userunkid
    '        objUser._Creation_Date = ConfigParameter._Object._CurrentDateAndTime
    '        objUser._IsManager = True


    '        'Pinkal (03-Apr-2017) -- Start
    '        'Enhancement - Working On Active directory Changes for PACRA.
    '        'Dim dtUser As DataTable = New DataView(dtTable, "ischeck = true AND isaduser = true", "", DataViewRowState.CurrentRows).ToTable()
    '        Dim dtUser As DataTable = New DataView(dvUser.Table(), "ischeck = true AND isaduser = true", "", DataViewRowState.CurrentRows).ToTable()
    '        'Pinkal (03-Apr-2017) -- End


    '        Dim blnFlag As Boolean = objUser.ImportADUser(dtUser)

    '        If blnFlag = False And objUser._Message <> "" Then
    '            Cursor = Cursors.Default
    '            eZeeMsgBox.Show(objUser._Message, enMsgBoxStyle.Information)
    '        End If
    '        If blnFlag Then
    '            Cursor = Cursors.Default
    '            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Data successfully Imported."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
    '            Me.Close()
    '        End If
    '    Catch ex As Exception
    '        Cursor = Cursors.Default
    '        DisplayError.Show("-1", ex.Message, "mnuImportManagerUser_Click", mstrModuleName)
    '    End Try
    'End Sub

    'Private Sub mnuImportEmpUser_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuImportEmpUser.Click
    '    Try

    '        If IsValid() = False Then Exit Sub

    '        Cursor = Cursors.WaitCursor
    '        objUser._Userunkid = User._Object._Userunkid
    '        objUser._Creation_Date = ConfigParameter._Object._CurrentDateAndTime
    '        objUser._IsManager = False
           

    '        'Pinkal (03-Apr-2017) -- Start
    '        'Enhancement - Working On Active directory Changes for PACRA.
    '        'Dim dtUser As DataTable = New DataView(dtTable, "ischeck = true AND isaduser = true", "", DataViewRowState.CurrentRows).ToTable()
    '        Dim dtUser As DataTable = New DataView(dvUser.Table(), "ischeck = true AND isaduser = true", "", DataViewRowState.CurrentRows).ToTable()
    '        'Pinkal (03-Apr-2017) -- End

    '        Dim blnFlag As Boolean = objUser.ImportADUser(dtUser)

    '        If blnFlag = False And objUser._Message <> "" Then
    '            Cursor = Cursors.Default
    '            eZeeMsgBox.Show(objUser._Message, enMsgBoxStyle.Information)
    '        End If
    '        If blnFlag Then
    '            Cursor = Cursors.Default
    '            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Data successfully Imported."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
    '            Me.Close()
    '        End If
    '    Catch ex As Exception
    '        Cursor = Cursors.Default
    '        DisplayError.Show("-1", ex.Message, "mnuImportEmpUser_Click", mstrModuleName)
    '    End Try
    'End Sub


    Private Sub mnuImportManagerUser_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuImportManagerUser.Click
        Try

            If IsValid() = False Then Exit Sub
            Dim dtUser As DataTable = New DataView(dvUser.Table(), "ischeck = true AND isaduser = true", "", DataViewRowState.CurrentRows).ToTable()
            Dim frm As New frmADUserMapping
            frm.displayDialog(dtUser, True)
        Catch ex As Exception
            Cursor = Cursors.Default
            DisplayError.Show("-1", ex.Message, "mnuImportManagerUser_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub mnuImportEmpUser_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuImportEmpUser.Click
        Try

            If IsValid() = False Then Exit Sub
            Dim dtUser As DataTable = New DataView(dvUser.Table(), "ischeck = true AND isaduser = true", "", DataViewRowState.CurrentRows).ToTable()
            Dim frm As New frmADUserMapping
            frm.displayDialog(dtUser, False)
        Catch ex As Exception
            Cursor = Cursors.Default
            DisplayError.Show("-1", ex.Message, "mnuImportEmpUser_Click", mstrModuleName)
        End Try
    End Sub
    'Pinkal (26-Apr-2017) -- End

#End Region


    'Pinkal (03-Apr-2017) -- Start
    'Enhancement - Working On Active directory Changes for PACRA.

#Region "TextChange Event"

    Private Sub txtSearchUser_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtSearchUser.TextChanged
        Try
            If dvUser IsNot Nothing Then
                dvUser.RowFilter = "username like '%" & txtSearchUser.Text.Trim & "%'"
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "txtSearchUser_TextChanged", mstrModuleName)
        End Try
    End Sub

#End Region


    'Pinkal (03-Apr-2017) -- End

    'Pinkal (26-Apr-2017) -- Start
    'Enhancement - Working On Active directory Changes for PACRA.
    Private Sub objChkAll_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objChkAll.CheckedChanged
        Try
            If dvUser Is Nothing Then Exit Sub
            'dgAdUsers.CellContentClick, dgAdUsers.CellContentDoubleClick
            RemoveHandler dgAdUsers.CellContentClick, AddressOf dgAdUsers_CellContentClick
            RemoveHandler dgAdUsers.CellContentDoubleClick, AddressOf dgAdUsers_CellContentClick
            For Each drow As DataRowView In dvUser
                drow.Item("ischeck") = objChkAll.Checked
            Next
            dgAdUsers.Refresh()
            AddHandler dgAdUsers.CellContentClick, AddressOf dgAdUsers_CellContentClick
            AddHandler dgAdUsers.CellContentDoubleClick, AddressOf dgAdUsers_CellContentClick
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objChkAll_CheckedChanged", mstrModuleName)
        Finally
        End Try
    End Sub
    'Pinkal (26-Apr-2017) -- End



    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Private Sub OtherSettings()
        Try
            Me.SuspendLayout()

            Call SetLanguage()

            Me.EZeeCollapsibleContainer1.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.EZeeCollapsibleContainer1.ForeColor = GUI._eZeeContainerHeaderForeColor


            Me.btnClose.GradientBackColor = GUI._ButttonBackColor
            Me.btnClose.GradientForeColor = GUI._ButttonFontColor

            Me.btnOperation.GradientBackColor = GUI._ButttonBackColor
            Me.btnOperation.GradientForeColor = GUI._ButttonFontColor


            Me.ResumeLayout()
        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "OtherSettings", mstrModuleName)
        End Try
    End Sub


    Private Sub SetLanguage()
        Try
            Me.Text = Language._Object.getCaption(Me.Name, Me.Text)

            Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
            Me.EZeeCollapsibleContainer1.Text = Language._Object.getCaption(Me.EZeeCollapsibleContainer1.Name, Me.EZeeCollapsibleContainer1.Text)
            Me.colhUserName.HeaderText = Language._Object.getCaption(Me.colhUserName.Name, Me.colhUserName.HeaderText)
            Me.btnOperation.Text = Language._Object.getCaption(Me.btnOperation.Name, Me.btnOperation.Text)
            Me.mnuImportManagerUser.Text = Language._Object.getCaption(Me.mnuImportManagerUser.Name, Me.mnuImportManagerUser.Text)
            Me.mnuImportEmpUser.Text = Language._Object.getCaption(Me.mnuImportEmpUser.Name, Me.mnuImportEmpUser.Text)

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
        End Try
    End Sub


    Private Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "There is no data to do further operation.")
            Language.setMessage(mstrModuleName, 2, "User is compulsory information.Please select atleast one user to import.")
            Language.setMessage(mstrModuleName, 3, "Data successfully Imported.")
            Language.setMessage(mstrModuleName, 4, "There is no user(s) to import data.")
            Language.setMessage(mstrModuleName, 5, " Or does not contains the Active Directory Users. Please set correct server to use this feature.")
            Language.setMessage(mstrModuleName, 6, "Active directory server address cannot be blank. Please set the active directroy server address.")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>

   
End Class