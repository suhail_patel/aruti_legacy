Imports eZeeCommonLib
Imports System.Text
Imports System.Security.Cryptography

Public Class Language

    'Sandeep | 17 JAN 2011 | -- START
    'Private Shared gobjLocalization As clsLocalization
    'Public Shared Sub initLanguage()
    '    ''set Localization & CultureInfo of DataOperation
    '    gobjLocalization = New clsLocalization(0)
    '    'gobjLocalization._LangId = eZeeApplication._ApplicationSettings._LanguageId
    '    gobjLocalization._LangId = 0
    'End Sub
    'Sandeep | 17 JAN 2011 | -- END 

    Public Shared ReadOnly Property _Object() As clsLocalization
        Get
            Return gobjLocalization
        End Get
    End Property

    Public Shared Sub setLanguage(ByVal oForm As Object, Optional ByVal blnInsertNew As Boolean = True)
        gobjLocalization.setLanguage(oForm)
    End Sub

    Public Shared Sub setLanguage(ByVal oForm As String)
        gobjLocalization.setLanguage(oForm)
    End Sub


    'Sandeep | 17 JAN 2011 | -- START
    'Public Shared Function getMessage(ByVal strModuleName As String, _
    '                            ByVal MsgCode As Integer, _
    '                            ByVal strDefaultMsg As String, _
    '                            Optional ByVal intApplicationId As Integer = -1, _
    '                            Optional ByVal intLanguageId As Integer = -1) As String
    '    Try
    '        Return gobjLocalization.getMessage(strModuleName, MsgCode, strDefaultMsg, intApplicationId, intLanguageId)
    '    Catch ex As Exception
    '        Return strDefaultMsg
    '    End Try
    'End Function
    Public Shared Function getMessage(ByVal strModuleName As String, _
                                ByVal MsgCode As Integer, _
                                ByVal strDefaultMsg As String, _
                                Optional ByVal intLanguageId As Integer = -1) As String
        Try
           
            Return gobjLocalization.getMessage(strModuleName, MsgCode, strDefaultMsg, intLanguageId)
        Catch ex As Exception
            Return strDefaultMsg
        End Try
    End Function
    'Sandeep | 17 JAN 2011 | -- END 



    'Sandeep | 17 JAN 2011 | -- START
    'Public Shared Sub setMessage(ByVal strModuleName As String, _
    '                                ByVal MsgCode As Integer, _
    '                                ByVal strDefaultMsg As String, _
    '                                Optional ByVal intApplicationId As Integer = -1, _
    '                                Optional ByVal intLanguageId As Integer = -1)
    '    Try
    '        Call gobjLocalization.setMessage(strModuleName, MsgCode, strDefaultMsg, intApplicationId, intLanguageId)
    '    Catch ex As Exception

    '    End Try
    'End Sub
    Public Shared Sub setMessage(ByVal strModuleName As String, _
                                ByVal MsgCode As Integer, _
                                ByVal strDefaultMsg As String, _
                                Optional ByVal intLanguageId As Integer = -1)
        Try
            Call gobjLocalization.setMessage(strModuleName, MsgCode, strDefaultMsg, intLanguageId)
        Catch ex As Exception

        End Try
    End Sub
    'Sandeep | 17 JAN 2011 | -- END 


    Public Shared Sub Refresh()
        gobjLocalization.Refresh()
    End Sub

    Public Shared Sub ctlRightToLeftlayOut(ByVal ctlParents As Object)
        gobjLocalization.ctlRightToLeftlayOut(ctlParents)
    End Sub

End Class


Public Class ConfigParameter

    Public Shared ReadOnly Property _Object() As clsConfigOptions
        Get
            Return gobjConfigOptions
        End Get
    End Property

End Class

Public Class User
    Public Shared ReadOnly Property _Object() As clsUserAddEdit
        Get
            Return gobjUser
        End Get
    End Property

    Public Shared Sub Refresh()
        gobjUser = Nothing
        gobjUser = New clsUserAddEdit
    End Sub

End Class

Public Class Company
    Public Shared ReadOnly Property _Object() As clsCompany_Master
        Get
            Return gobjCompany
        End Get
    End Property

    Public Shared Sub Refresh()
        gobjCompany = Nothing
        gobjCompany = New clsCompany_Master
    End Sub

End Class

Public Class FinancialYear
    Public Shared ReadOnly Property _Object() As clsCompany_Master
        Get
            Return gobjFinancial
        End Get
    End Property

    Public Shared Sub Refresh()
        gobjFinancial = Nothing
        gobjFinancial = New clsCompany_Master
    End Sub

End Class

Public Class UserAccessLevel

    'S.SANDEEP [29 APR 2015] -- START
    Public Shared Property _TransferAsOnDate() As Date
        Get
            Return mdtTransferAsOnDate
        End Get
        Set(ByVal value As Date)
            mdtTransferAsOnDate = value
        End Set
    End Property

    'Public Shared ReadOnly Property _NewAccessLevelFilterString() As String
    '    Get
    '        Dim objMst As New clsMasterData
    '        Return objMst.NewUserAccessString(mdtTransferAsOnDate, , , , , mblnOnlyApprovedEmployees)
    '    End Get
    'End Property
    'S.SANDEEP [29 APR 2015] -- END

    Public Shared Property _AccessLevel() As String
        Get
            Return mstrAccessLevelIds
        End Get
        Set(ByVal value As String)
            mstrAccessLevelIds = value
        End Set
    End Property

    'Sohail (17 Apr 2012) -- Start
    'TRA - ENHANCEMENT
    Public Shared Property _AccessLevelFilterString() As String
        Get
            Return mstrAccessLevelFilterString
        End Get
        Set(ByVal value As String)
            mstrAccessLevelFilterString = value
        End Set
    End Property
    'Sohail (17 Apr 2012) -- End

    'Sohail (31 May 2012) -- Start
    'TRA - ENHANCEMENT
    Public Shared Property _AccessLevelBranchFilterString() As String
        Get
            Return mstrAccessLevelBranchFilterString
        End Get
        Set(ByVal value As String)
            mstrAccessLevelBranchFilterString = value
        End Set
    End Property

    Public Shared Property _AccessLevelDepartmentGroupFilterString() As String
        Get
            Return mstrAccessLevelDepartmentGroupFilterString
        End Get
        Set(ByVal value As String)
            mstrAccessLevelDepartmentGroupFilterString = value
        End Set
    End Property

    Public Shared Property _AccessLevelDepartmentFilterString() As String
        Get
            Return mstrAccessLevelDepartmentFilterString
        End Get
        Set(ByVal value As String)
            mstrAccessLevelDepartmentFilterString = value
        End Set
    End Property

    Public Shared Property _AccessLevelSectionGroupFilterString() As String
        Get
            Return mstrAccessLevelSectionGroupFilterString
        End Get
        Set(ByVal value As String)
            mstrAccessLevelSectionGroupFilterString = value
        End Set
    End Property

    Public Shared Property _AccessLevelSectionFilterString() As String
        Get
            Return mstrAccessLevelSectionFilterString
        End Get
        Set(ByVal value As String)
            mstrAccessLevelSectionFilterString = value
        End Set
    End Property

    Public Shared Property _AccessLevelUnitGroupFilterString() As String
        Get
            Return mstrAccessLevelUnitGroupFilterString
        End Get
        Set(ByVal value As String)
            mstrAccessLevelUnitGroupFilterString = value
        End Set
    End Property

    Public Shared Property _AccessLevelUnitFilterString() As String
        Get
            Return mstrAccessLevelUnitFilterString
        End Get
        Set(ByVal value As String)
            mstrAccessLevelUnitFilterString = value
        End Set
    End Property

    Public Shared Property _AccessLevelTeamFilterString() As String
        Get
            Return mstrAccessLevelTeamFilterString
        End Get
        Set(ByVal value As String)
            mstrAccessLevelTeamFilterString = value
        End Set
    End Property

    Public Shared Property _AccessLevelJobGroupFilterString() As String
        Get
            Return mstrAccessLevelJobGroupFilterString
        End Get
        Set(ByVal value As String)
            mstrAccessLevelJobGroupFilterString = value
        End Set
    End Property

    Public Shared Property _AccessLevelJobFilterString() As String
        Get
            Return mstrAccessLevelJobFilterString
        End Get
        Set(ByVal value As String)
            mstrAccessLevelJobFilterString = value
        End Set
    End Property

    Public Shared Property _AccessLevelClassGroupFilterString() As String
        Get
            Return mstrAccessLevelClassGroupFilterString
        End Get
        Set(ByVal value As String)
            mstrAccessLevelClassGroupFilterString = value
        End Set
    End Property

    Public Shared Property _AccessLevelClassFilterString() As String
        Get
            Return mstrAccessLevelClassFilterString
        End Get
        Set(ByVal value As String)
            mstrAccessLevelClassFilterString = value
        End Set
    End Property
    'Sohail (31 May 2012) -- End
End Class

'S.SANDEEP [ 09 AUG 2011 ] -- START
'ENHANCEMENT : AUTO BACKUP & AUTO UPDATE
Public Class AppSettings
    Public Shared ReadOnly Property _Object() As clsApplicationSettings
        Get
            Return gobjAppSettings
        End Get
    End Property

    Public Shared ReadOnly Property _AppType() As enApplication
        Get
            Return gApplicationType
        End Get
    End Property

    Public Shared Sub Refresh()
        gobjAppSettings = Nothing
        gobjAppSettings = New clsApplicationSettings
    End Sub
End Class

Public Class General_Settings
    Public Shared ReadOnly Property _Object() As clsGeneralSettings
        Get
            gobjGSetting._Section = gApplicationType.ToString
            Return gobjGSetting
        End Get
    End Property
End Class

'S.SANDEEP [ 09 AUG 2011 ] -- END 


'' '<To Do>

' ''Public Class Parameter
' ''    Public Shared ReadOnly Property _Object() As clsParameter
' ''        Get
' ''            Return gobjParam
' ''        End Get
' ''    End Property
' ''End Class

' ''Public Class User
' ''    Public Shared ReadOnly Property _Object() As clsDeskUser
' ''        Get
' ''            Return gobjUser
' ''        End Get
' ''    End Property

' ''    Public Shared Sub Refresh()
' ''        gobjUser = Nothing
' ''        gobjUser = New clsDeskUser
' ''    End Sub

' ''End Class

' ''Public Class Shift
' ''    Public Shared ReadOnly Property _Object() As clsShift
' ''        Get
' ''            Return gobjShift
' ''        End Get
' ''    End Property
' ''End Class

' ''Public Class eZeeApplication

' ''    'Naimish (14 Aug 2009) -- Start
' ''    Private Shared gobjMachineSetting As clsMachineSettings

' ''    Public Shared ReadOnly Property _MachineSetting() As clsMachineSettings
' ''        Get
' ''            If gobjMachineSetting Is Nothing Then
' ''                gobjMachineSetting = New clsMachineSettings(gApplicationType)
' ''            End If

' ''            Return gobjMachineSetting
' ''        End Get
' ''    End Property

' ''    Public Shared Sub _Refresh_MachineSetting()
' ''        gobjMachineSetting = Nothing
' ''        gobjMachineSetting = New clsMachineSettings(gApplicationType)
' ''    End Sub
' ''    'Naimish (14 Aug 2009) -- End

' ''    'Naimish (14 Aug 2009) -- Start
' ''    Public Shared objAppSetting As New clsApplicationSettings

' ''    Public Shared ReadOnly Property _ApplicationSettings() As clsApplicationSettings
' ''        Get
' ''            If objAppSetting Is Nothing Then
' ''                objAppSetting = New clsApplicationSettings
' ''            End If

' ''            Return objAppSetting
' ''        End Get
' ''    End Property

' ''    Public Shared Sub _Refresh_ApplicationSettings()
' ''        objAppSetting = Nothing
' ''        objAppSetting = New clsApplicationSettings
' ''    End Sub
' ''    'Naimish (14 Aug 2009) -- End

' ''    Public Shared ReadOnly Property _AppType() As enApplication
' ''        Get
' ''            Return gApplicationType
' ''        End Get
' ''    End Property
' ''End Class

' ''Public Class PropertyInfo
' ''    Private Shared gobjHotel As clsHotel

' ''    Public Shared ReadOnly Property _Object() As clsHotel
' ''        Get
' ''            If gobjHotel Is Nothing Then
' ''                gobjHotel = New clsHotel(True)
' ''            End If

' ''            Return gobjHotel
' ''        End Get
' ''    End Property

' ''    Public Shared Sub _Refresh()
' ''        gobjHotel = Nothing
' ''        gobjHotel = New clsHotel(True)
' ''    End Sub

' ''End Class

' ''Public Class Lic
' ''    Private Shared gobjLic As New clsLicense

' ''    Public Shared ReadOnly Property _Object() As clsLicense
' ''        Get
' ''            Return gobjLic
' ''        End Get
' ''    End Property
' ''End Class

' ''Public Class NGLic
' ''    Private Shared mobjLic As eZeeNextGenLic

' ''    Public Shared Property _Object() As eZeeNextGenLic
' ''        Get
' ''            Return mobjLic
' ''        End Get
' ''        Set(ByVal value As eZeeNextGenLic)
' ''            mobjLic = value
' ''        End Set
' ''    End Property
' ''End Class


Public Class Rounding
    'Sohail (09 jul 2011)-Start
    ' Issue : coverted from double to decimal
    Public Shared Function BRound(ByVal InValue As Decimal, _
                                Optional ByVal Factor As Double = 1, _
                                Optional ByVal enRoundType As enPaymentRoundingType = CInt(enPaymentRoundingType.AUTOMATIC)) As Decimal
        'Sohail (11 Dec 2020) - [enRoundType]

        If Factor <= 0 Then
            Return InValue
        End If

        Dim newInValue, FixTemp As Decimal
        Try

            newInValue = InValue * Factor
            FixTemp = Fix(newInValue + 0.5 * System.Math.Sign(InValue))

            'Sohail (02 Jul 2020) -- Start
            'CORAL BEACH CLUB enhancement : 0004774 : Net Pay Rounding Off not consistent for all values.
            'e.g. it returns 100 for 100.50 in -1 <-> 1 rounding settig instead of 101.
            'If newInValue - Int(newInValue) = 0.5 Then
            '    If FixTemp / 2 <> Int(FixTemp / 2) Then
            '        FixTemp = FixTemp - System.Math.Sign(InValue)
            '    End If
            'End If
            'Sohail (02 Jul 2020) -- End

            'Sohail (11 Dec 2020) -- Start
            'Netis Togo Enhancement # OLD-218 : - Give option to Round Up/Round Down/Auto on transaction head next to Rounding OFF Type.
            'Return FixTemp / Factor
            Dim decResult As Decimal = FixTemp / Factor

            If enRoundType = enPaymentRoundingType.UP Then
                If decResult < InValue Then
                    decResult = decResult + (1 / Factor)
                End If
            ElseIf enRoundType = enPaymentRoundingType.DOWN Then
                If decResult > InValue Then
                    decResult = decResult - (1 / Factor)
                End If
            End If

            Return decResult
            'Sohail (11 Dec 2020) -- End            

        Catch ex As Exception
            'DisplayError.Show(ex.Message & " " & "BRound" & " " & mstrModuleName)
            Return InValue
        End Try
    End Function
    'Sohail (09 jul 2011)-End

    
End Class

Public Class ArtLic
    Private Shared mobjLic As ArutiLic

    Public Shared Property _Object() As ArutiLic
        Get
            Return mobjLic
        End Get
        Set(ByVal value As ArutiLic)
            mobjLic = value
        End Set
    End Property
End Class

'Sohail (04 Feb 2012) -Start
'Ehnacement : This is used for online recuritment reference no decryption.
Public Class clsCrypto
    Private Shared strKey As String = "@%^"

    Public Shared Function Encrypt(ByVal strText As String) As String
        Dim Crypt As New MD5CryptoServiceProvider
        Dim b() As Byte = Encoding.UTF8.GetBytes(strText)
        Dim key() As Byte

        Try
            key = Crypt.ComputeHash(Encoding.UTF8.GetBytes(strKey))
            Crypt.Clear()

            Dim t As New TripleDESCryptoServiceProvider
            t.Key = key
            t.Mode = CipherMode.ECB
            t.Padding = PaddingMode.PKCS7

            Dim it As ICryptoTransform = t.CreateEncryptor()
            Dim rslt() As Byte = it.TransformFinalBlock(b, 0, b.Length)
            t.Clear()

            Return Convert.ToBase64String(rslt, Base64FormattingOptions.None, rslt.Length)

        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Function

    Public Shared Function Dicrypt(ByVal strEncodedText As String) As String
        Dim Crypt As New MD5CryptoServiceProvider
        Dim b() As Byte = Convert.FromBase64String(strEncodedText)
        Dim key() As Byte

        Try

            key = Crypt.ComputeHash(Encoding.UTF8.GetBytes(strKey))
            Crypt.Clear()

            Dim t As New TripleDESCryptoServiceProvider
            t.Key = key
            t.Mode = CipherMode.ECB
            t.Padding = PaddingMode.PKCS7

            Dim it As ICryptoTransform = t.CreateDecryptor()
            Dim rslt() As Byte = it.TransformFinalBlock(b, 0, b.Length)
            t.Clear()

            Return Encoding.UTF8.GetString(rslt)
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Function

    'Sohail (01 Feb 2019) -- Start
    'NMB Enhancement - 76.1 - Push amount in 3DES encrypted format in Flex Cube NMB JV.
    Private Shared str3DESKey As String = "PKC55"

    Public Shared Function Encrypt_3DES(ByVal strText As String) As String
        Try
            Dim des As TripleDES = CreateDES(str3DESKey)
            Dim ct As ICryptoTransform = des.CreateEncryptor()
            Dim input As Byte() = Encoding.Unicode.GetBytes(strText)
            Dim buffer As Byte() = ct.TransformFinalBlock(input, 0, input.Length)
            Dim b As String = Convert.ToBase64String(buffer)

            Return b

        Catch ex As Exception
            Throw New Exception(ex.Message)
            Return ""
        End Try
    End Function

    Public Shared Function CreateDES(ByVal str3DES_Key As String) As TripleDES
        Try
            Dim md5 As MD5 = New MD5CryptoServiceProvider()
            Dim des As TripleDES = New TripleDESCryptoServiceProvider()

            des.Key = md5.ComputeHash(Encoding.Unicode.GetBytes(str3DES_Key))
            des.IV = New Byte(des.BlockSize / 8 - 1) {}
            Return des

        Catch ex As Exception
            Throw New Exception(ex.Message)
            Return Nothing
        End Try
    End Function

    Public Shared Function Decrypt_3DES(ByVal strCypherText As String) As String
        Try
            Dim b As Byte() = Convert.FromBase64String(strCypherText)
            Dim des As TripleDES = CreateDES(str3DESKey)
            Dim ct As ICryptoTransform = des.CreateDecryptor()
            Dim output As Byte() = ct.TransformFinalBlock(b, 0, b.Length)
            Return Encoding.Unicode.GetString(output)
        Catch ex As Exception
            Throw New Exception(ex.Message)
            Return ""
        End Try
    End Function
    'Sohail (01 Feb 2019) -- End

End Class
'Sohail (04 Feb 2012) -Start
'S.SANDEEP [ 01 DEC 2012 ] -- START
'ENHANCEMENT : TRA CHANGES
Public Class clsRandomPassword
    Private Shared PASSWORD_CHARS_LCASE As String = "abcdefgijkmnopqrstwxyz"
    Private Shared PASSWORD_CHARS_UCASE As String = "ABCDEFGHJKLMNPQRSTWXYZ"
    Private Shared PASSWORD_CHARS_NUMERIC As String = "0123456789"
    Private Shared PASSWORD_CHARS_SPECIAL As String = "`*$-+?_&=!%{}/"

    Public Shared Function Generate(ByVal minLength As Integer, _
                                    ByVal maxLength As Integer) _
        As String

        ' Make sure that input parameters are valid.
        If (minLength <= 0 Or maxLength <= 0 Or minLength > maxLength) Then
            Generate = Nothing
        End If

        ' Create a local array containing supported password characters
        ' grouped by types. You can remove character groups from this
        ' array, but doing so will weaken the password strength.
        Dim charGroups As Char()() = New Char()() _
        { _
            PASSWORD_CHARS_LCASE.ToCharArray(), _
            PASSWORD_CHARS_UCASE.ToCharArray(), _
            PASSWORD_CHARS_NUMERIC.ToCharArray(), _
            PASSWORD_CHARS_SPECIAL.ToCharArray() _
        }

        ' Use this array to track the number of unused characters in each
        ' character group.
        Dim charsLeftInGroup As Integer() = New Integer(charGroups.Length - 1) {}

        ' Initially, all characters in each group are not used.
        Dim I As Integer
        For I = 0 To charsLeftInGroup.Length - 1
            charsLeftInGroup(I) = charGroups(I).Length
        Next

        ' Use this array to track (iterate through) unused character groups.
        Dim leftGroupsOrder As Integer() = New Integer(charGroups.Length - 1) {}

        ' Initially, all character groups are not used.
        For I = 0 To leftGroupsOrder.Length - 1
            leftGroupsOrder(I) = I
        Next

        ' Because we cannot use the default randomizer, which is based on the
        ' current time (it will produce the same "random" number within a
        ' second), we will use a random number generator to seed the
        ' randomizer.

        ' Use a 4-byte array to fill it with random bytes and convert it then
        ' to an integer value.
        Dim randomBytes As Byte() = New Byte(3) {}

        ' Generate 4 random bytes.
        Dim rng As RNGCryptoServiceProvider = New RNGCryptoServiceProvider()

        rng.GetBytes(randomBytes)

        ' Convert 4 bytes into a 32-bit integer value.
        Dim seed As Integer = ((randomBytes(0) And &H7F) << 24 Or _
                                randomBytes(1) << 16 Or _
                                randomBytes(2) << 8 Or _
                                randomBytes(3))

        ' Now, this is real randomization.
        Dim random As Random = New Random(seed)

        ' This array will hold password characters.
        Dim password As Char() = Nothing

        ' Allocate appropriate memory for the password.
        If (minLength < maxLength) Then
            password = New Char(random.Next(minLength - 1, maxLength)) {}
        Else
            password = New Char(minLength - 1) {}
        End If

        ' Index of the next character to be added to password.
        Dim nextCharIdx As Integer

        ' Index of the next character group to be processed.
        Dim nextGroupIdx As Integer

        ' Index which will be used to track not processed character groups.
        Dim nextLeftGroupsOrderIdx As Integer

        ' Index of the last non-processed character in a group.
        Dim lastCharIdx As Integer

        ' Index of the last non-processed group.
        Dim lastLeftGroupsOrderIdx As Integer = leftGroupsOrder.Length - 1

        ' Generate password characters one at a time.
        For I = 0 To password.Length - 1

            ' If only one character group remained unprocessed, process it;
            ' otherwise, pick a random character group from the unprocessed
            ' group list. To allow a special character to appear in the
            ' first position, increment the second parameter of the Next
            ' function call by one, i.e. lastLeftGroupsOrderIdx + 1.
            If (lastLeftGroupsOrderIdx = 0) Then
                nextLeftGroupsOrderIdx = 0
            Else
                nextLeftGroupsOrderIdx = random.Next(0, lastLeftGroupsOrderIdx)
            End If

            ' Get the actual index of the character group, from which we will
            ' pick the next character.
            nextGroupIdx = leftGroupsOrder(nextLeftGroupsOrderIdx)

            ' Get the index of the last unprocessed characters in this group.
            lastCharIdx = charsLeftInGroup(nextGroupIdx) - 1

            ' If only one unprocessed character is left, pick it; otherwise,
            ' get a random character from the unused character list.
            If (lastCharIdx = 0) Then
                nextCharIdx = 0
            Else
                nextCharIdx = random.Next(0, lastCharIdx + 1)
            End If

            ' Add this character to the password.
            password(I) = charGroups(nextGroupIdx)(nextCharIdx)

            ' If we processed the last character in this group, start over.
            If (lastCharIdx = 0) Then
                charsLeftInGroup(nextGroupIdx) = _
                                charGroups(nextGroupIdx).Length
                ' There are more unprocessed characters left.
            Else
                ' Swap processed character with the last unprocessed character
                ' so that we don't pick it until we process all characters in
                ' this group.
                If (lastCharIdx <> nextCharIdx) Then
                    Dim temp As Char = charGroups(nextGroupIdx)(lastCharIdx)
                    charGroups(nextGroupIdx)(lastCharIdx) = _
                                charGroups(nextGroupIdx)(nextCharIdx)
                    charGroups(nextGroupIdx)(nextCharIdx) = temp
                End If

                ' Decrement the number of unprocessed characters in
                ' this group.
                charsLeftInGroup(nextGroupIdx) = _
                           charsLeftInGroup(nextGroupIdx) - 1
            End If

            ' If we processed the last group, start all over.
            If (lastLeftGroupsOrderIdx = 0) Then
                lastLeftGroupsOrderIdx = leftGroupsOrder.Length - 1
                ' There are more unprocessed groups left.
            Else
                ' Swap processed group with the last unprocessed group
                ' so that we don't pick it until we process all groups.
                If (lastLeftGroupsOrderIdx <> nextLeftGroupsOrderIdx) Then
                    Dim temp As Integer = _
                                leftGroupsOrder(lastLeftGroupsOrderIdx)
                    leftGroupsOrder(lastLeftGroupsOrderIdx) = _
                                leftGroupsOrder(nextLeftGroupsOrderIdx)
                    leftGroupsOrder(nextLeftGroupsOrderIdx) = temp
                End If

                ' Decrement the number of unprocessed groups.
                lastLeftGroupsOrderIdx = lastLeftGroupsOrderIdx - 1
            End If
        Next

        ' Convert password characters into a string and return the result.
        Generate = New String(password)
    End Function

End Class
'S.SANDEEP [ 01 DEC 2012 ] -- END

'S.SANDEEP [04 JUN 2015] -- START
'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
Public Class clsGetDateDifference

    ''' <summary>
    ''' defining Number of days in month; index 0=> january and 11=> December
    ''' february contain either 28 or 29 days, that's why here value is -1
    ''' which wil be calculate later.
    ''' </summary>
    Private monthDay As Integer() = New Integer(11) {31, -1, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31}

    Private mdtFromDate As DateTime
    Private mdtToDate As DateTime

    Private intYear As Integer = 0
    Private intMonth As Integer = 0
    Private intDay As Integer = 0

    Public ReadOnly Property Years() As Integer
        Get
            Return intYear
        End Get
    End Property

    Public ReadOnly Property Months() As Integer
        Get
            Return intMonth
        End Get
    End Property

    Public ReadOnly Property Days() As Integer
        Get
            Return intDay
        End Get
    End Property

    Public Sub PerformDateOperation(ByVal date1 As DateTime, ByVal date2 As DateTime)
        Try
            Dim increment As Integer

            If date1 > date2 Then
                mdtFromDate = date2
                mdtToDate = date1
            Else
                mdtFromDate = date1
                mdtToDate = date2
            End If
            increment = 0
            If mdtFromDate.Day > mdtToDate.Day Then
                increment = monthDay(mdtFromDate.Month - 1)
            End If

            If increment = -1 Then  'FEB MONTH
                If DateTime.IsLeapYear(mdtFromDate.Year) Then
                    ' leap year february contain 29 days
                    increment = 29
                Else
                    increment = 28
                End If
            End If

            '''' DAY CALCULATION '''
            If increment <> 0 Then
                intDay = (mdtToDate.Day + increment) - mdtFromDate.Day
                increment = 1
            Else
                intDay = mdtToDate.Day - mdtFromDate.Day
                'If intDay = 0 Then
                '    intDay = 1
                'End If
            End If

            '''' MONTH CALCULATION '''
            If (mdtFromDate.Month + increment) > mdtToDate.Month Then
                intMonth = (mdtToDate.Month + 12) - (mdtFromDate.Month + increment)
                increment = 1
            Else
                intMonth = (mdtToDate.Month) - (mdtFromDate.Month + increment)
                increment = 0
            End If

            '''' YEAR CALCULATION '''
            intYear = mdtToDate.Year - (mdtFromDate.Year + increment)

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: PerformDateOperation; Module Name: " & "clsGetDateDifference")
        Finally
        End Try
    End Sub

    'Public Sub New(ByVal date1 As DateTime, ByVal date2 As DateTime)
    '    Try
    '        Dim increment As Integer

    '        If date1 > date2 Then
    '            mdtFromDate = date2
    '            mdtToDate = date1
    '        Else
    '            mdtFromDate = date1
    '            mdtToDate = date2
    '        End If
    '        increment = 0
    '        If mdtFromDate.Day > mdtToDate.Day Then
    '            increment = monthDay(mdtFromDate.Month - 1)
    '        End If

    '        If increment = -1 Then  'FEB MONTH
    '            If DateTime.IsLeapYear(mdtFromDate.Year) Then
    '                ' leap year february contain 29 days
    '                increment = 29
    '            Else
    '                increment = 28
    '            End If
    '        End If

    '        '''' DAY CALCULATION '''
    '        If increment <> 0 Then
    '            intDay = (mdtToDate.Day + increment) - mdtFromDate.Day
    '            increment = 1
    '        Else
    '            intDay = mdtToDate.Day - mdtFromDate.Day
    '        End If

    '        '''' MONTH CALCULATION '''
    '        If (mdtFromDate.Month + increment) > mdtToDate.Month Then
    '            intMonth = (mdtToDate.Month + 12) - (mdtFromDate.Month + increment)
    '            increment = 1
    '        Else
    '            intMonth = (mdtToDate.Month) - (mdtFromDate.Month + increment)
    '            increment = 0
    '        End If

    '        '''' YEAR CALCULATION '''
    '        intYear = mdtToDate.Year - (mdtFromDate.Year + increment)

    '    Catch ex As Exception
    '        Throw New Exception(ex.Message & "; Procedure Name: New; Module Name: " & "GetDateDifference")
    '    Finally
    '    End Try
    'End Sub

End Class
'S.SANDEEP [04 JUN 2015] -- END

Public Class SpreedsheetHelper
    '''<summary>returns an empty cell when a blank cell is encountered
    '''</summary>
    Public Shared Function GetRowCells(ByVal row As DocumentFormat.OpenXml.Spreadsheet.Row) As IEnumerable(Of DocumentFormat.OpenXml.Spreadsheet.Cell)
        Dim currentCount As Integer = 0
        Dim iCells As New List(Of DocumentFormat.OpenXml.Spreadsheet.Cell)()
        For Each cell As DocumentFormat.OpenXml.Spreadsheet.Cell In row.Descendants(Of DocumentFormat.OpenXml.Spreadsheet.Cell)()
            Dim columnName As String = GetColumnName(cell.CellReference)
            Dim currentColumnIndex As Integer = ConvertColumnNameToNumber(columnName)            
            Do While currentCount < currentColumnIndex
                iCells.Add(New DocumentFormat.OpenXml.Spreadsheet.Cell())
                ''Yield(New DocumentFormat.OpenXml.Spreadsheet.Cell())
                currentCount += 1
            Loop
            iCells.Add(cell)
            currentCount += 1
        Next cell
        Return iCells
    End Function

    ''' <summary>
    ''' Given a cell name, parses the specified cell to get the column name.
    ''' </summary>
    ''' <param name="cellReference">Address of the cell (ie. B2)</param>
    ''' <returns>Column Name (ie. B)</returns>
    Public Shared Function GetColumnName(ByVal cellReference As String) As String
        ' Match the column name portion of the cell name.
        Dim regex = New System.Text.RegularExpressions.Regex("[A-Za-z]+")
        Dim match = regex.Match(cellReference)

        Return match.Value
    End Function

    ''' <summary>
    ''' Given just the column name (no row index),
    ''' it will return the zero based column index.
    ''' </summary>
    ''' <param name="columnName">Column Name (ie. A or AB)</param>
    ''' <returns>Zero based index if the conversion was successful</returns>
    ''' <exception cref="ArgumentException">thrown if the given string
    ''' contains characters other than uppercase letters</exception>
    Public Shared Function ConvertColumnNameToNumber(ByVal columnName As String) As Integer
        Dim alpha = New System.Text.RegularExpressions.Regex("^[A-Z]+$")
        If Not alpha.IsMatch(columnName) Then
            Throw New ArgumentException()
        End If

        Dim colLetters() As Char = columnName.ToCharArray()
        Array.Reverse(colLetters)

        Dim convertedValue As Integer = 0
        For i As Integer = 0 To colLetters.Length - 1
            Dim letter As Char = colLetters(i)
            Dim current As Integer = If(i = 0, AscW(letter) - 65, AscW(letter) - 64) ' ASCII 'A' = 65
            convertedValue += current * CInt(Math.Truncate(Math.Pow(26, i)))
        Next i

        Return convertedValue
    End Function
End Class

'Sohail (10 Jan 2020) -- Start
'NMB Enhancement # : New screen for Country list and allow to edit currency sign on country list.
Public Class clsCountry

    Private mintCountryUnkid As Integer
    Public Property _CountryUnkid() As Integer
        Get
            Return mintCountryUnkid
        End Get
        Set(ByVal value As Integer)
            mintCountryUnkid = value
        End Set
    End Property

    Private mstrCurrencySign As String
    Public Property _CurrencySign() As String
        Get
            Return mstrCurrencySign
        End Get
        Set(ByVal value As String)
            mstrCurrencySign = value
        End Set
    End Property

    Public Function UpdateCountryCurrency(ByVal objCountry As clsCountry, Optional ByVal xDataOp As clsDataOperation = Nothing) As Boolean
        Dim objDataOperation As clsDataOperation
        Dim strQ As String = String.Empty
        Try
            If objCountry Is Nothing Then Return True

            If xDataOp Is Nothing Then
                objDataOperation = New clsDataOperation
            Else
                objDataOperation = xDataOp
            End If
            objDataOperation.ClearParameters()

            strQ = "UPDATE hrmsConfiguration..cfcountry_master " & _
                    "SET currency = @currency " & _
                    "WHERE countryunkid = @countryunkid "

            objDataOperation.AddParameter("@currency", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, objCountry._CurrencySign)
            objDataOperation.AddParameter("@countryunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, objCountry._CountryUnkid)

            objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                Throw New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
            End If

            Return True

        Catch ex As Exception
            Throw New Exception(ex.Message & "[UpdateCountryCurrency]")
        Finally
            If xDataOp Is Nothing Then objDataOperation = Nothing
        End Try
    End Function

End Class
'Sohail (10 Jan 2020) -- End