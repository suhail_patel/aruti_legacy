﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmUserDef_ReportMode
    Inherits eZee.Common.eZeeForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmUserDef_ReportMode))
        Me.pnlMain = New System.Windows.Forms.Panel
        Me.objchkDAll = New System.Windows.Forms.CheckBox
        Me.objchkEAll = New System.Windows.Forms.CheckBox
        Me.lvDeduction = New eZee.Common.eZeeListView(Me.components)
        Me.objDcolhcheck = New System.Windows.Forms.ColumnHeader
        Me.colhDHeadTypeName = New System.Windows.Forms.ColumnHeader
        Me.colhDTransactionHead = New System.Windows.Forms.ColumnHeader
        Me.objcolhDHeadTypeId = New System.Windows.Forms.ColumnHeader
        Me.lvEarning = New eZee.Common.eZeeListView(Me.components)
        Me.objcolhECheck = New System.Windows.Forms.ColumnHeader
        Me.colhEHeadTypeName = New System.Windows.Forms.ColumnHeader
        Me.colhETranHead = New System.Windows.Forms.ColumnHeader
        Me.objEHeadtypeId = New System.Windows.Forms.ColumnHeader
        Me.elDeduction = New eZee.Common.eZeeLine
        Me.elEarning = New eZee.Common.eZeeLine
        Me.EZeeFooter1 = New eZee.Common.eZeeFooter
        Me.btnSaveSelection = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnOk = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnClose = New eZee.Common.eZeeLightButton(Me.components)
        Me.lvNonEarningDeduction = New eZee.Common.eZeeListView(Me.components)
        Me.objcolhNCheck = New System.Windows.Forms.ColumnHeader
        Me.colhNHeadTypeName = New System.Windows.Forms.ColumnHeader
        Me.colhNTranHead = New System.Windows.Forms.ColumnHeader
        Me.objNHeadtypeId = New System.Windows.Forms.ColumnHeader
        Me.elNonEarningDeduction = New eZee.Common.eZeeLine
        Me.objchkNAll = New System.Windows.Forms.CheckBox
        Me.pnlMain.SuspendLayout()
        Me.EZeeFooter1.SuspendLayout()
        Me.SuspendLayout()
        '
        'pnlMain
        '
        Me.pnlMain.Controls.Add(Me.objchkNAll)
        Me.pnlMain.Controls.Add(Me.elNonEarningDeduction)
        Me.pnlMain.Controls.Add(Me.lvNonEarningDeduction)
        Me.pnlMain.Controls.Add(Me.objchkDAll)
        Me.pnlMain.Controls.Add(Me.objchkEAll)
        Me.pnlMain.Controls.Add(Me.lvDeduction)
        Me.pnlMain.Controls.Add(Me.lvEarning)
        Me.pnlMain.Controls.Add(Me.elDeduction)
        Me.pnlMain.Controls.Add(Me.elEarning)
        Me.pnlMain.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pnlMain.Location = New System.Drawing.Point(0, 0)
        Me.pnlMain.Name = "pnlMain"
        Me.pnlMain.Size = New System.Drawing.Size(602, 423)
        Me.pnlMain.TabIndex = 0
        '
        'objchkDAll
        '
        Me.objchkDAll.AutoSize = True
        Me.objchkDAll.Location = New System.Drawing.Point(11, 210)
        Me.objchkDAll.Name = "objchkDAll"
        Me.objchkDAll.Size = New System.Drawing.Size(15, 14)
        Me.objchkDAll.TabIndex = 38
        Me.objchkDAll.UseVisualStyleBackColor = True
        '
        'objchkEAll
        '
        Me.objchkEAll.AutoSize = True
        Me.objchkEAll.Location = New System.Drawing.Point(11, 28)
        Me.objchkEAll.Name = "objchkEAll"
        Me.objchkEAll.Size = New System.Drawing.Size(15, 14)
        Me.objchkEAll.TabIndex = 37
        Me.objchkEAll.UseVisualStyleBackColor = True
        '
        'lvDeduction
        '
        Me.lvDeduction.BackColorOnChecked = False
        Me.lvDeduction.CheckBoxes = True
        Me.lvDeduction.ColumnHeaders = Nothing
        Me.lvDeduction.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.objDcolhcheck, Me.colhDHeadTypeName, Me.colhDTransactionHead, Me.objcolhDHeadTypeId})
        Me.lvDeduction.CompulsoryColumns = ""
        Me.lvDeduction.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lvDeduction.FullRowSelect = True
        Me.lvDeduction.GridLines = True
        Me.lvDeduction.GroupingColumn = Nothing
        Me.lvDeduction.HideSelection = False
        Me.lvDeduction.Location = New System.Drawing.Point(4, 205)
        Me.lvDeduction.MinColumnWidth = 50
        Me.lvDeduction.MultiSelect = False
        Me.lvDeduction.Name = "lvDeduction"
        Me.lvDeduction.OptionalColumns = ""
        Me.lvDeduction.ShowMoreItem = False
        Me.lvDeduction.ShowSaveItem = False
        Me.lvDeduction.ShowSelectAll = True
        Me.lvDeduction.ShowSizeAllColumnsToFit = True
        Me.lvDeduction.Size = New System.Drawing.Size(283, 159)
        Me.lvDeduction.Sortable = True
        Me.lvDeduction.TabIndex = 36
        Me.lvDeduction.UseCompatibleStateImageBehavior = False
        Me.lvDeduction.View = System.Windows.Forms.View.Details
        '
        'objDcolhcheck
        '
        Me.objDcolhcheck.Tag = "objDcolhcheck"
        Me.objDcolhcheck.Text = ""
        Me.objDcolhcheck.Width = 25
        '
        'colhDHeadTypeName
        '
        Me.colhDHeadTypeName.Tag = "colhDHeadTypeName"
        Me.colhDHeadTypeName.Text = ""
        Me.colhDHeadTypeName.Width = 0
        '
        'colhDTransactionHead
        '
        Me.colhDTransactionHead.Tag = "colhDTransactionHead"
        Me.colhDTransactionHead.Text = "Transaction Head"
        Me.colhDTransactionHead.Width = 250
        '
        'objcolhDHeadTypeId
        '
        Me.objcolhDHeadTypeId.Tag = "objcolhDHeadTypeId"
        Me.objcolhDHeadTypeId.Width = 0
        '
        'lvEarning
        '
        Me.lvEarning.BackColorOnChecked = False
        Me.lvEarning.CheckBoxes = True
        Me.lvEarning.ColumnHeaders = Nothing
        Me.lvEarning.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.objcolhECheck, Me.colhEHeadTypeName, Me.colhETranHead, Me.objEHeadtypeId})
        Me.lvEarning.CompulsoryColumns = ""
        Me.lvEarning.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lvEarning.FullRowSelect = True
        Me.lvEarning.GridLines = True
        Me.lvEarning.GroupingColumn = Nothing
        Me.lvEarning.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable
        Me.lvEarning.HideSelection = False
        Me.lvEarning.Location = New System.Drawing.Point(4, 23)
        Me.lvEarning.MinColumnWidth = 50
        Me.lvEarning.MultiSelect = False
        Me.lvEarning.Name = "lvEarning"
        Me.lvEarning.OptionalColumns = ""
        Me.lvEarning.ShowMoreItem = False
        Me.lvEarning.ShowSaveItem = False
        Me.lvEarning.ShowSelectAll = True
        Me.lvEarning.ShowSizeAllColumnsToFit = True
        Me.lvEarning.Size = New System.Drawing.Size(283, 159)
        Me.lvEarning.Sortable = True
        Me.lvEarning.TabIndex = 35
        Me.lvEarning.UseCompatibleStateImageBehavior = False
        Me.lvEarning.View = System.Windows.Forms.View.Details
        '
        'objcolhECheck
        '
        Me.objcolhECheck.Tag = "objcolhECheck"
        Me.objcolhECheck.Text = ""
        Me.objcolhECheck.Width = 25
        '
        'colhEHeadTypeName
        '
        Me.colhEHeadTypeName.Tag = "colhEHeadTypeName"
        Me.colhEHeadTypeName.Text = ""
        Me.colhEHeadTypeName.Width = 0
        '
        'colhETranHead
        '
        Me.colhETranHead.Tag = "colhETranHead"
        Me.colhETranHead.Text = "Transaction Heads"
        Me.colhETranHead.Width = 250
        '
        'objEHeadtypeId
        '
        Me.objEHeadtypeId.Tag = "objEHeadtypeId"
        Me.objEHeadtypeId.Text = ""
        Me.objEHeadtypeId.Width = 0
        '
        'elDeduction
        '
        Me.elDeduction.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.elDeduction.LineOrientation = eZee.Common.eZeeLine.Orientation.Horizontal
        Me.elDeduction.Location = New System.Drawing.Point(3, 185)
        Me.elDeduction.Name = "elDeduction"
        Me.elDeduction.Size = New System.Drawing.Size(284, 17)
        Me.elDeduction.TabIndex = 3
        Me.elDeduction.Text = "Deductions"
        Me.elDeduction.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'elEarning
        '
        Me.elEarning.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.elEarning.LineOrientation = eZee.Common.eZeeLine.Orientation.Horizontal
        Me.elEarning.Location = New System.Drawing.Point(4, 3)
        Me.elEarning.Name = "elEarning"
        Me.elEarning.Size = New System.Drawing.Size(283, 17)
        Me.elEarning.TabIndex = 0
        Me.elEarning.Text = "Earnings"
        Me.elEarning.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'EZeeFooter1
        '
        Me.EZeeFooter1.BorderColor = System.Drawing.Color.Silver
        Me.EZeeFooter1.Controls.Add(Me.btnSaveSelection)
        Me.EZeeFooter1.Controls.Add(Me.btnOk)
        Me.EZeeFooter1.Controls.Add(Me.btnClose)
        Me.EZeeFooter1.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.EZeeFooter1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.EZeeFooter1.GradiantStyle = eZee.Common.eZeeFooter.GradientStyle.Up
        Me.EZeeFooter1.GradientColor1 = System.Drawing.SystemColors.Control
        Me.EZeeFooter1.GradientColor2 = System.Drawing.SystemColors.Control
        Me.EZeeFooter1.Location = New System.Drawing.Point(0, 368)
        Me.EZeeFooter1.Name = "EZeeFooter1"
        Me.EZeeFooter1.Size = New System.Drawing.Size(602, 55)
        Me.EZeeFooter1.TabIndex = 1
        '
        'btnSaveSelection
        '
        Me.btnSaveSelection.BackColor = System.Drawing.Color.White
        Me.btnSaveSelection.BackgroundImage = CType(resources.GetObject("btnSaveSelection.BackgroundImage"), System.Drawing.Image)
        Me.btnSaveSelection.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnSaveSelection.BorderColor = System.Drawing.Color.Empty
        Me.btnSaveSelection.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnSaveSelection.FlatAppearance.BorderSize = 0
        Me.btnSaveSelection.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnSaveSelection.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSaveSelection.ForeColor = System.Drawing.Color.Black
        Me.btnSaveSelection.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnSaveSelection.GradientForeColor = System.Drawing.Color.Black
        Me.btnSaveSelection.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSaveSelection.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnSaveSelection.Location = New System.Drawing.Point(7, 13)
        Me.btnSaveSelection.Name = "btnSaveSelection"
        Me.btnSaveSelection.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSaveSelection.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnSaveSelection.Size = New System.Drawing.Size(111, 30)
        Me.btnSaveSelection.TabIndex = 36
        Me.btnSaveSelection.Text = "&Save Selection"
        Me.btnSaveSelection.UseVisualStyleBackColor = True
        '
        'btnOk
        '
        Me.btnOk.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnOk.BackColor = System.Drawing.Color.White
        Me.btnOk.BackgroundImage = CType(resources.GetObject("btnOk.BackgroundImage"), System.Drawing.Image)
        Me.btnOk.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnOk.BorderColor = System.Drawing.Color.Empty
        Me.btnOk.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnOk.FlatAppearance.BorderSize = 0
        Me.btnOk.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnOk.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnOk.ForeColor = System.Drawing.Color.Black
        Me.btnOk.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnOk.GradientForeColor = System.Drawing.Color.Black
        Me.btnOk.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnOk.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnOk.Location = New System.Drawing.Point(400, 13)
        Me.btnOk.Name = "btnOk"
        Me.btnOk.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnOk.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnOk.Size = New System.Drawing.Size(90, 30)
        Me.btnOk.TabIndex = 35
        Me.btnOk.Text = "&Ok"
        Me.btnOk.UseVisualStyleBackColor = True
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnClose.BackColor = System.Drawing.Color.White
        Me.btnClose.BackgroundImage = CType(resources.GetObject("btnClose.BackgroundImage"), System.Drawing.Image)
        Me.btnClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnClose.BorderColor = System.Drawing.Color.Empty
        Me.btnClose.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnClose.FlatAppearance.BorderSize = 0
        Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClose.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.ForeColor = System.Drawing.Color.Black
        Me.btnClose.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnClose.GradientForeColor = System.Drawing.Color.Black
        Me.btnClose.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Location = New System.Drawing.Point(496, 13)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Size = New System.Drawing.Size(90, 30)
        Me.btnClose.TabIndex = 34
        Me.btnClose.Text = "&Close"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'lvNonEarningDeduction
        '
        Me.lvNonEarningDeduction.BackColorOnChecked = False
        Me.lvNonEarningDeduction.CheckBoxes = True
        Me.lvNonEarningDeduction.ColumnHeaders = Nothing
        Me.lvNonEarningDeduction.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.objcolhNCheck, Me.colhNHeadTypeName, Me.colhNTranHead, Me.objNHeadtypeId})
        Me.lvNonEarningDeduction.CompulsoryColumns = ""
        Me.lvNonEarningDeduction.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lvNonEarningDeduction.FullRowSelect = True
        Me.lvNonEarningDeduction.GridLines = True
        Me.lvNonEarningDeduction.GroupingColumn = Nothing
        Me.lvNonEarningDeduction.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable
        Me.lvNonEarningDeduction.HideSelection = False
        Me.lvNonEarningDeduction.Location = New System.Drawing.Point(303, 23)
        Me.lvNonEarningDeduction.MinColumnWidth = 50
        Me.lvNonEarningDeduction.MultiSelect = False
        Me.lvNonEarningDeduction.Name = "lvNonEarningDeduction"
        Me.lvNonEarningDeduction.OptionalColumns = ""
        Me.lvNonEarningDeduction.ShowMoreItem = False
        Me.lvNonEarningDeduction.ShowSaveItem = False
        Me.lvNonEarningDeduction.ShowSelectAll = True
        Me.lvNonEarningDeduction.ShowSizeAllColumnsToFit = True
        Me.lvNonEarningDeduction.Size = New System.Drawing.Size(283, 341)
        Me.lvNonEarningDeduction.Sortable = True
        Me.lvNonEarningDeduction.TabIndex = 39
        Me.lvNonEarningDeduction.UseCompatibleStateImageBehavior = False
        Me.lvNonEarningDeduction.View = System.Windows.Forms.View.Details
        '
        'objcolhNCheck
        '
        Me.objcolhNCheck.Tag = "objcolhNCheck"
        Me.objcolhNCheck.Text = ""
        Me.objcolhNCheck.Width = 25
        '
        'colhNHeadTypeName
        '
        Me.colhNHeadTypeName.Tag = "colhNHeadTypeName"
        Me.colhNHeadTypeName.Text = ""
        Me.colhNHeadTypeName.Width = 0
        '
        'colhNTranHead
        '
        Me.colhNTranHead.Tag = "colhNTranHead"
        Me.colhNTranHead.Text = "Transaction Heads"
        Me.colhNTranHead.Width = 250
        '
        'objNHeadtypeId
        '
        Me.objNHeadtypeId.Tag = "objNHeadtypeId"
        Me.objNHeadtypeId.Text = ""
        Me.objNHeadtypeId.Width = 0
        '
        'elNonEarningDeduction
        '
        Me.elNonEarningDeduction.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.elNonEarningDeduction.LineOrientation = eZee.Common.eZeeLine.Orientation.Horizontal
        Me.elNonEarningDeduction.Location = New System.Drawing.Point(303, 3)
        Me.elNonEarningDeduction.Name = "elNonEarningDeduction"
        Me.elNonEarningDeduction.Size = New System.Drawing.Size(283, 17)
        Me.elNonEarningDeduction.TabIndex = 40
        Me.elNonEarningDeduction.Text = "Non - Earning Deduction"
        Me.elNonEarningDeduction.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objchkNAll
        '
        Me.objchkNAll.AutoSize = True
        Me.objchkNAll.Location = New System.Drawing.Point(310, 27)
        Me.objchkNAll.Name = "objchkNAll"
        Me.objchkNAll.Size = New System.Drawing.Size(15, 14)
        Me.objchkNAll.TabIndex = 41
        Me.objchkNAll.UseVisualStyleBackColor = True
        '
        'frmUserDef_ReportMode
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(602, 423)
        Me.Controls.Add(Me.EZeeFooter1)
        Me.Controls.Add(Me.pnlMain)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmUserDef_ReportMode"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "User Define Report Mode"
        Me.pnlMain.ResumeLayout(False)
        Me.pnlMain.PerformLayout()
        Me.EZeeFooter1.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents pnlMain As System.Windows.Forms.Panel
    Friend WithEvents EZeeFooter1 As eZee.Common.eZeeFooter
    Friend WithEvents btnSaveSelection As eZee.Common.eZeeLightButton
    Friend WithEvents btnOk As eZee.Common.eZeeLightButton
    Friend WithEvents btnClose As eZee.Common.eZeeLightButton
    Friend WithEvents elEarning As eZee.Common.eZeeLine
    Friend WithEvents elDeduction As eZee.Common.eZeeLine
    Friend WithEvents lvEarning As eZee.Common.eZeeListView
    Friend WithEvents objcolhECheck As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhEHeadTypeName As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhETranHead As System.Windows.Forms.ColumnHeader
    Friend WithEvents objEHeadtypeId As System.Windows.Forms.ColumnHeader
    Friend WithEvents lvDeduction As eZee.Common.eZeeListView
    Friend WithEvents objDcolhcheck As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhDHeadTypeName As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhDTransactionHead As System.Windows.Forms.ColumnHeader
    Friend WithEvents objcolhDHeadTypeId As System.Windows.Forms.ColumnHeader
    Friend WithEvents objchkEAll As System.Windows.Forms.CheckBox
    Friend WithEvents objchkDAll As System.Windows.Forms.CheckBox
    Friend WithEvents elNonEarningDeduction As eZee.Common.eZeeLine
    Friend WithEvents lvNonEarningDeduction As eZee.Common.eZeeListView
    Friend WithEvents objcolhNCheck As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhNHeadTypeName As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhNTranHead As System.Windows.Forms.ColumnHeader
    Friend WithEvents objNHeadtypeId As System.Windows.Forms.ColumnHeader
    Friend WithEvents objchkNAll As System.Windows.Forms.CheckBox
End Class
