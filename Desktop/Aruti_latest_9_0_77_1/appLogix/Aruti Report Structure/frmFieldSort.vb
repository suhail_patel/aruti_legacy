﻿Imports eZeeCommonLib
Imports Aruti.Data


Public Class frmFieldSort
    Private ReadOnly mstrModuleName As String = "frmOrderBy"

    Private mobjOrderByList As IColumnCollection
    Private mblnCancel As Boolean = True

    Private mstrOrderByQueryName As String = ""
    Private mstrOrderByDisplayName As String = ""

    Private mblnIsHandleByKey As Boolean = False

    Private mblnOrderByDescending As Boolean = False 'Sohail (18 Feb 2012)
#Region "Form Property"
    Public Property _ArrayList() As IColumnCollection
        Get
            Return mobjOrderByList
        End Get
        Set(ByVal value As IColumnCollection)
            mobjOrderByList = value
        End Set
    End Property

    Public Property _OrderByQueryName() As String
        Get
            Return mstrOrderByQueryName
        End Get
        Set(ByVal value As String)
            mstrOrderByQueryName = value
        End Set
    End Property

    Public Property _OrderByDisplayName() As String
        Get
            Return mstrOrderByDisplayName
        End Get
        Set(ByVal value As String)
            mstrOrderByDisplayName = value
        End Set
    End Property

    'Sohail (18 Feb 2012) -- Start
    'TRA - ENHANCEMENT
    Public Property _OrderByDescending() As Boolean
        Get
            Return mblnOrderByDescending
        End Get
        Set(ByVal value As Boolean)
            mblnOrderByDescending = value
        End Set
    End Property
    'Sohail (18 Feb 2012) -- End

#End Region

#Region "Display Dialog"
    Public Function displayDialog() As Boolean
        Try
            Me.ShowDialog()
            Return Not mblnCancel
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "displayDialog", mstrModuleName)
        End Try
    End Function
#End Region

#Region "Private Function"
    Public Sub FillListBox()
        Dim strName As String = ""
        Dim strDisplay As String = ""
        'Dim intIndex As Integer 'Sohail (18 Feb 2012)

        Try
            'Sohail (18 Feb 2012) -- Start
            'TRA - ENHANCEMENT
            'chkListOrderBy.Items.Clear()
            'For Each iColItem As IColumn In mobjOrderByList.Items
            '    If iColItem.ShowInOrderBy = False Then Continue For

            '    intIndex = chkListOrderBy.Items.Add(iColItem)

            '    If InStr(mstrOrderByQueryName, iColItem.Name) > 0 And InStr(mstrOrderByDisplayName, iColItem.DisplayName) > 0 Then
            '        chkListOrderBy.SetItemCheckState(intIndex, Windows.Forms.CheckState.Checked)
            '    End If
            'Next

            'If chkListOrderBy.Items.Count > 0 Then
            '    chkListOrderBy.SelectedIndex = 0
            'End If
            Dim lvItem As ListViewItem
            lvListOrderBy.Items.Clear()
            For Each iColItem As IColumn In mobjOrderByList.Items
                If iColItem.ShowInOrderBy = False Then Continue For

                lvItem = New ListViewItem

                lvItem.Text = ""

                lvItem.SubItems.Add(iColItem.DisplayName)
                lvItem.SubItems(colhFieldName.Index).Tag = iColItem.Name

                If iColItem.OrderByDescending = True Then
                    lvItem.SubItems.Add("DESC")
                Else
                    lvItem.SubItems.Add("ASC")
                End If

                If InStr(mstrOrderByQueryName, iColItem.Name) > 0 And InStr(mstrOrderByDisplayName, iColItem.DisplayName) > 0 Then
                    lvItem.Checked = True
                End If

                lvListOrderBy.Items.Add(lvItem)
            Next
            'Sohail (18 Feb 2012) -- End

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillListBox", mstrModuleName)
        End Try
    End Sub

    Private Sub createOrderByString()
        mstrOrderByQueryName = ""
        mstrOrderByDisplayName = ""

        'Sohail (18 Feb 2012) -- Start
        'TRA - ENHANCEMENT
        'For Each iColItem As IColumn In chkListOrderBy.CheckedItems
        '    'mstrOrderByQueryName &= iColItem.Name + ","
        '    'mstrOrderByDisplayName &= iColItem.DisplayName + ","
        '    If iColItem.OrderByDescending = False Then
        '        mstrOrderByQueryName &= iColItem.Name + ","
        '        mstrOrderByDisplayName &= iColItem.DisplayName + ","
        '    Else
        '        mstrOrderByQueryName &= iColItem.Name + " DESC,"
        '        mstrOrderByDisplayName &= iColItem.DisplayName + " DESC,"
        '    End If
        'Next
        For Each lvItem As ListViewItem In lvListOrderBy.CheckedItems
            'mstrOrderByQueryName &= iColItem.Name + ","
            'mstrOrderByDisplayName &= iColItem.DisplayName + ","
            If lvItem.SubItems(colhSorting.Index).Text = "ASC" Then
                mstrOrderByQueryName &= lvItem.SubItems(colhFieldName.Index).Tag.ToString + ","
                mstrOrderByDisplayName &= lvItem.SubItems(colhFieldName.Index).Text + ","
                mobjOrderByList(lvItem.Index).OrderByDescending = False
            Else
                mstrOrderByQueryName &= lvItem.SubItems(colhFieldName.Index).Tag.ToString + " DESC,"
                mstrOrderByDisplayName &= lvItem.SubItems(colhFieldName.Index).Text + " DESC,"
                mobjOrderByList(lvItem.Index).OrderByDescending = True
            End If

        Next
        'Sohail (18 Feb 2012) -- End
        If mstrOrderByQueryName.Length > 0 Then
            mstrOrderByQueryName = mstrOrderByQueryName.Substring(0, mstrOrderByQueryName.Length - 1)
        End If

        If mstrOrderByDisplayName.Length > 0 Then
            mstrOrderByDisplayName = mstrOrderByDisplayName.Substring(0, mstrOrderByDisplayName.Length - 1)
        End If

    End Sub

#End Region

#Region "Form's Event"

    Private Sub frmFieldSort_FormClosed(ByVal sender As System.Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles MyBase.FormClosed
        Try
            If Not mobjOrderByList Is Nothing Then
                mobjOrderByList = Nothing
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmFieldSort_FormClosed", mstrModuleName)
        End Try
    End Sub

    Private Sub frmFieldSort_KeyDown(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles MyBase.KeyDown
        If e.Control And e.KeyCode = Windows.Forms.Keys.S Then
            btnOk.PerformClick()
        End If
    End Sub

    Private Sub frmFieldSort_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles Me.KeyPress
        If Asc(e.KeyChar) = 27 Then
            btnCancel.PerformClick()
        ElseIf Asc(e.KeyChar) = 13 Then
            btnOk.PerformClick()
        End If
    End Sub

    Private Sub frmFieldSort_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Call Language.setLanguage(Me.Name)
            Call Set_Logo(Me, gApplicationType)
            Call FillListBox()

            'Sohail (18 Feb 2012) -- Start
            'TRA - ENHANCEMENT
            'If chkListOrderBy.Items.Count > 0 Then chkListOrderBy.SelectedIndex = 0
            'chkListOrderBy.Select()
            lvListOrderBy.Focus()
            'Sohail (18 Feb 2012) -- End
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmFieldSort_Load", mstrModuleName)
        End Try
    End Sub

#End Region

#Region "Button's Event"

    Private Sub objbtnUp_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles objbtnUp.Click

        Dim intSelectedIndex As Integer
        Try
            'Sohail (18 Feb 2012) -- Start
            'TRA - ENHANCEMENT
            'chkListOrderBy.SuspendLayout()

            'intSelectedIndex = chkListOrderBy.SelectedIndices(0)

            'If intSelectedIndex <> 0 Then
            '    'Swapping Value of Column Collection
            '    Dim objTmp As Object
            '    objTmp = mobjOrderByList(intSelectedIndex)
            '    mobjOrderByList(intSelectedIndex) = mobjOrderByList(intSelectedIndex - 1)
            '    mobjOrderByList(intSelectedIndex - 1) = objTmp


            '    'Swapping Value of Checked ListBox
            '    objTmp = chkListOrderBy.Items(intSelectedIndex)
            '    Dim blnChecked1 As Boolean = chkListOrderBy.CheckedIndices.Contains(intSelectedIndex)
            '    Dim blnChecked2 As Boolean = chkListOrderBy.CheckedIndices.Contains(intSelectedIndex - 1)

            '    chkListOrderBy.Items(intSelectedIndex) = chkListOrderBy.Items(intSelectedIndex - 1)
            '    chkListOrderBy.Items(intSelectedIndex - 1) = objTmp

            '    If blnChecked1 Then
            '        chkListOrderBy.SetItemCheckState(intSelectedIndex - 1, Windows.Forms.CheckState.Checked)
            '    Else
            '        chkListOrderBy.SetItemCheckState(intSelectedIndex - 1, Windows.Forms.CheckState.Unchecked)
            '    End If

            '    If blnChecked2 Then
            '        chkListOrderBy.SetItemCheckState(intSelectedIndex, Windows.Forms.CheckState.Checked)
            '    Else
            '        chkListOrderBy.SetItemCheckState(intSelectedIndex, Windows.Forms.CheckState.Unchecked)
            '    End If

            '    If Not mblnIsHandleByKey Then
            '        chkListOrderBy.SelectedItem = chkListOrderBy.Items(intSelectedIndex - 1)
            '    Else
            '        chkListOrderBy.SelectedItem = chkListOrderBy.Items(intSelectedIndex)
            '    End If
            'End If
            lvListOrderBy.SuspendLayout()

            intSelectedIndex = lvListOrderBy.SelectedIndices(0)

            Dim lvItem As ListViewItem
            If intSelectedIndex <> 0 Then
                'Swapping Value of Column Collection
                Dim objTmp As Object
                objTmp = mobjOrderByList(intSelectedIndex)
                mobjOrderByList(intSelectedIndex) = mobjOrderByList(intSelectedIndex - 1)
                mobjOrderByList(intSelectedIndex - 1) = objTmp

                'Swapping Value of Listview
                Dim lvCurrItem As ListViewItem = lvListOrderBy.Items(intSelectedIndex)
                Dim lvPrevItem As ListViewItem = lvListOrderBy.Items(intSelectedIndex - 1)

                lvItem = New ListViewItem
                lvItem.Checked = lvCurrItem.Checked
                lvItem.Text = lvCurrItem.Text
                lvItem.SubItems.Add(lvCurrItem.SubItems(colhFieldName.Index).Text)
                lvItem.SubItems(colhFieldName.Index).Tag = (lvCurrItem.SubItems(colhFieldName.Index).Tag.ToString)
                lvItem.SubItems.Add(lvCurrItem.SubItems(colhSorting.Index).Text)

                lvListOrderBy.Items(intSelectedIndex).Text = lvPrevItem.Text
                lvListOrderBy.Items(intSelectedIndex).Checked = lvPrevItem.Checked
                lvListOrderBy.Items(intSelectedIndex).SubItems(colhFieldName.Index).Text = lvPrevItem.SubItems(colhFieldName.Index).Text
                lvListOrderBy.Items(intSelectedIndex).SubItems(colhFieldName.Index).Tag = lvPrevItem.SubItems(colhFieldName.Index).Tag.ToString
                lvListOrderBy.Items(intSelectedIndex).SubItems(colhSorting.Index).Text = lvPrevItem.SubItems(colhSorting.Index).Text

                lvListOrderBy.Items(intSelectedIndex - 1) = lvItem
                lvListOrderBy.Items(intSelectedIndex - 1).Checked = lvItem.Checked
                lvListOrderBy.Items(intSelectedIndex - 1).Selected = True
            End If
            lvListOrderBy.Focus()
            'Sohail (18 Feb 2012) -- End
        Catch ex As Exception
            Call DisplayError.Show(-1, ex.Message, "btnUp_Click", mstrModuleName)
        Finally
            'Sohail (18 Feb 2012) -- Start
            'TRA - ENHANCEMENT
            'chkListOrderBy.ResumeLayout()
            lvListOrderBy.ResumeLayout()
            'Sohail (18 Feb 2012) -- End
        End Try
    End Sub

    Private Sub objbtnDown_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles objbtnDown.Click
        Dim intSelectedIndex As Integer
        Try
            'Sohail (18 Feb 2012) -- Start
            'TRA - ENHANCEMENT
            'chkListOrderBy.SuspendLayout()

            'intSelectedIndex = chkListOrderBy.SelectedIndices(0)

            'If intSelectedIndex <> chkListOrderBy.Items.Count - 1 Then
            '    'Swapping Value of Column Collection
            '    Dim objTmp As Object
            '    objTmp = mobjOrderByList(intSelectedIndex)
            '    mobjOrderByList(intSelectedIndex) = mobjOrderByList(intSelectedIndex + 1)
            '    mobjOrderByList(intSelectedIndex + 1) = objTmp


            '    'Swapping Value of Checked ListBox
            '    objTmp = chkListOrderBy.Items(intSelectedIndex)
            '    Dim blnChecked1 As Boolean = chkListOrderBy.CheckedIndices.Contains(intSelectedIndex)
            '    Dim blnChecked2 As Boolean = chkListOrderBy.CheckedIndices.Contains(intSelectedIndex + 1)

            '    chkListOrderBy.Items(intSelectedIndex) = chkListOrderBy.Items(intSelectedIndex + 1)
            '    chkListOrderBy.Items(intSelectedIndex + 1) = objTmp

            '    If blnChecked1 Then
            '        chkListOrderBy.SetItemCheckState(intSelectedIndex + 1, Windows.Forms.CheckState.Checked)
            '    Else
            '        chkListOrderBy.SetItemCheckState(intSelectedIndex + 1, Windows.Forms.CheckState.Unchecked)
            '    End If

            '    If blnChecked2 Then
            '        chkListOrderBy.SetItemCheckState(intSelectedIndex, Windows.Forms.CheckState.Checked)
            '    Else
            '        chkListOrderBy.SetItemCheckState(intSelectedIndex, Windows.Forms.CheckState.Unchecked)
            '    End If

            '    If Not mblnIsHandleByKey Then
            '        chkListOrderBy.SelectedItem = chkListOrderBy.Items(intSelectedIndex + 1)
            '    Else
            '        chkListOrderBy.SelectedItem = chkListOrderBy.Items(intSelectedIndex)
            '    End If
            'End If
            lvListOrderBy.SuspendLayout()

            intSelectedIndex = lvListOrderBy.SelectedIndices(0)

            Dim lvItem As ListViewItem
            If intSelectedIndex <> lvListOrderBy.Items.Count - 1 Then
                'Swapping Value of Column Collection
                Dim objTmp As Object
                objTmp = mobjOrderByList(intSelectedIndex)
                mobjOrderByList(intSelectedIndex) = mobjOrderByList(intSelectedIndex + 1)
                mobjOrderByList(intSelectedIndex + 1) = objTmp

                'Swapping Value of Listview
                Dim lvCurrItem As ListViewItem = lvListOrderBy.Items(intSelectedIndex)
                Dim lvNextItem As ListViewItem = lvListOrderBy.Items(intSelectedIndex + 1)

                lvItem = New ListViewItem
                lvItem.Checked = lvCurrItem.Checked
                lvItem.Text = lvCurrItem.Text
                lvItem.SubItems.Add(lvCurrItem.SubItems(colhFieldName.Index).Text)
                lvItem.SubItems(colhFieldName.Index).Tag = (lvCurrItem.SubItems(colhFieldName.Index).Tag.ToString)
                lvItem.SubItems.Add(lvCurrItem.SubItems(colhSorting.Index).Text)

                lvListOrderBy.Items(intSelectedIndex).Text = lvNextItem.Text
                lvListOrderBy.Items(intSelectedIndex).Checked = lvNextItem.Checked
                lvListOrderBy.Items(intSelectedIndex).SubItems(colhFieldName.Index).Text = lvNextItem.SubItems(colhFieldName.Index).Text
                lvListOrderBy.Items(intSelectedIndex).SubItems(colhFieldName.Index).Tag = lvNextItem.SubItems(colhFieldName.Index).Tag.ToString
                lvListOrderBy.Items(intSelectedIndex).SubItems(colhSorting.Index).Text = lvNextItem.SubItems(colhSorting.Index).Text

                lvListOrderBy.Items(intSelectedIndex + 1) = lvItem
                lvListOrderBy.Items(intSelectedIndex + 1).Checked = lvItem.Checked
                lvListOrderBy.Items(intSelectedIndex + 1).Selected = True
            End If
            lvListOrderBy.Focus()
            'Sohail (18 Feb 2012) -- End
        Catch ex As Exception
            Call DisplayError.Show(-1, ex.Message, "btnDown_Click", mstrModuleName)
        Finally
            'Sohail (18 Feb 2012) -- Start
            'TRA - ENHANCEMENT
            'chkListOrderBy.ResumeLayout()
            lvListOrderBy.ResumeLayout()
            'Sohail (18 Feb 2012) -- End
        End Try
    End Sub

    Private Sub btnOk_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnOk.Click
        Try
            mblnCancel = False
            Call createOrderByString()
            Me.DialogResult = Windows.Forms.DialogResult.OK
        Catch ex As Exception
            Call DisplayError.Show(-1, ex.Message, "btnOk_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Try
            mblnCancel = True
            Me.Close()
        Catch ex As Exception
            Call DisplayError.Show(-1, ex.Message, "btnCancel_Click", mstrModuleName)
        End Try
    End Sub

   

    'Sohail (18 Feb 2012) -- Start
    'TRA - ENHANCEMENT
    Private Sub objbtnAscDesc_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles objbtnAscDesc.CheckedChanged
        Try

            If objbtnAscDesc.Selected = True Then
                objbtnAscDesc.Image = My.Resources.Ascending_24
            Else
                objbtnAscDesc.Image = My.Resources.Descending_24
            End If
            If lvListOrderBy.SelectedItems.Count <= 0 Then Exit Sub

            If objbtnAscDesc.Selected = True Then
                lvListOrderBy.SelectedItems(0).SubItems(colhSorting.Index).Text = "DESC"
            Else
                lvListOrderBy.SelectedItems(0).SubItems(colhSorting.Index).Text = "ASC"
            End If
            lvListOrderBy.Focus()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnAscDesc_CheckedChanged", mstrModuleName)
        End Try
    End Sub
    'Sohail (18 Feb 2012) -- End

#End Region

#Region "Other Control's Events"

    'Sohail (18 Feb 2012) -- Start
    'TRA - ENHANCEMENT
    'Private Sub chkListOrderBy_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs)
    '    If e.Control And e.KeyCode = Windows.Forms.Keys.Up Then
    '        mblnIsHandleByKey = True
    '        objbtnUp.PerformClick()
    '    ElseIf e.Control And e.KeyCode = Windows.Forms.Keys.Down Then
    '        mblnIsHandleByKey = True
    '        objbtnDown.PerformClick()
    '    Else
    '        mblnIsHandleByKey = False
    '    End If
    'End Sub
    Private Sub lvListOrderBy_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lvListOrderBy.SelectedIndexChanged
        Try
            If lvListOrderBy.SelectedItems.Count > 0 Then
                If lvListOrderBy.SelectedItems(0).SubItems(colhSorting.Index).Text = "ASC" Then
                    objbtnAscDesc.Selected = False
        Else
                    objbtnAscDesc.Selected = True
        End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "", mstrModuleName)
        End Try
    End Sub
    'Sohail (18 Feb 2012) -- End
#End Region

End Class