﻿Option Strict On

#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data

#End Region

Public Class frmEFTCustomColumnsExport

#Region " Private Variables "

    'Hemant (27 June 2019) -- Start
    'ENHANCEMENT :  REF # 0003777: Bank upload file format. same as EFT custom Xls,CSV now add custom txt with same  option with TAB delimiter
    'Private mstrModuleName As String = "frmEFTCustomCSVExport"
    Private mstrModuleName As String = "frmEFTCustomColumnsExport"
    'Hemant (27 June 2019) -- End
    Private mblnCancel As Boolean = True
    Private mintReportId As Integer = -1
    Private mintReportTypeId As Integer = -1
    Private mintModeId As Integer = -1
    Private mstrEFTCustomColumnsIds As String = String.Empty
    Private mblnShowColumnHeader As Boolean = False
    Private objUserDefRMode As clsUserDef_ReportMode
    Private marrEFTCustomColumnIds As New ArrayList
    'Sohail (13 Apr 2016) -- Start
    'Enhancement - 58.1 - Allow to show Employee Membership No. on EFT Custom Column XLS and CSV Report in Bank Payment List Report.
    Private mintMembershipUnkId As Integer = 0
    'Sohail (13 Apr 2016) -- End
    'Hemant (27 June 2019) -- Start
    'ENHANCEMENT :  REF # 0003777: Bank upload file format. same as EFT custom Xls,CSV now add custom txt with same  option with TAB delimiter
    Private mblnSaveAsTXT As Boolean = False
    Private mblnTabDelimiter As Boolean = False
    'Hemant (27 June 2019) -- End
#End Region

#Region " Properties "

    Public Property _EFTCustomColumnsIds() As String
        Get
            Return mstrEFTCustomColumnsIds
        End Get
        Set(ByVal value As String)
            mstrEFTCustomColumnsIds = value
        End Set
    End Property

    Public Property _EFTShowColumnHeader() As Boolean
        Get
            Return mblnShowColumnHeader
        End Get
        Set(ByVal value As Boolean)
            mblnShowColumnHeader = value
        End Set
    End Property

    'Sohail (13 Apr 2016) -- Start
    'Enhancement - 58.1 - Allow to show Employee Membership No. on EFT Custom Column XLS and CSV Report in Bank Payment List Report.
    Public Property _EFTMembershipUnkId() As Integer
        Get
            Return mintMembershipUnkId
        End Get
        Set(ByVal value As Integer)
            mintMembershipUnkId = value
        End Set
    End Property
    'Sohail (13 Apr 2016) -- End

    'Hemant (27 June 2019) -- Start
    'ENHANCEMENT :  REF # 0003777: Bank upload file format. same as EFT custom Xls,CSV now add custom txt with same  option with TAB delimiter
    Public Property _EFTSaveAsTXT() As Boolean
        Get
            Return mblnSaveAsTXT
        End Get
        Set(ByVal value As Boolean)
            mblnSaveAsTXT = value
        End Set
    End Property

    Public Property _EFTTabDelimiter() As Boolean
        Get
            Return mblnTabDelimiter
        End Get
        Set(ByVal value As Boolean)
            mblnTabDelimiter = value
        End Set
    End Property
    'Hemant (27 June 2019) -- End

    'Hemant (02 Jul 2020) -- Start
    'ENHANCEMENT(Freight Forwarders Kenya) #0004769: Add Payment Currency and Payment Date fields to EFT Custom Columns
    Private mstrDateFormat As String = String.Empty
    Public Property _EFTDateFormat() As String
        Get
            Return mstrDateFormat
        End Get
        Set(ByVal value As String)
            mstrDateFormat = value
        End Set
    End Property
    'Hemant (02 Jul 2020) -- End


#End Region

#Region " Dispaly Dialog "

    Public Function displayDialog(ByVal intReportUnkid As Integer, ByVal intModeId As Integer, Optional ByVal intReportTypeUnkid As Integer = -1) As Boolean
        Try
            mintReportId = intReportUnkid
            mintReportTypeId = intReportTypeUnkid
            mintModeId = intModeId
            Me.ShowDialog()

            Return Not mblnCancel

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure : displayDialog ; Module Name : " & mstrModuleName)
        Finally
        End Try
    End Function

#End Region

    'Sohail (13 Apr 2016) -- Start
    'Enhancement - 58.1 - Allow to show Employee Membership No. on EFT Custom Column XLS and CSV Report in Bank Payment List Report.
#Region " Private Enum "
    Private Enum enReportMode
        CUSTOM_COLUMNS = 0
        SHOW_REPORT_HEADER = 1
        MEMBERSHIP = 2
        'Hemant (27 June 2019) -- Start
        'ENHANCEMENT :  REF # 0003777: Bank upload file format. same as EFT custom Xls,CSV now add custom txt with same  option with TAB delimiter
        SAVE_AS_TXT = 3
        TAB_DELIMITER = 4
        'Hemant (27 June 2019) -- End
        'Hemant (02 Jul 2020) -- Start
        'ENHANCEMENT(Freight Forwarders Kenya) #0004769: Add Payment Currency and Payment Date fields to EFT Custom Columns
        DATE_FORMAT = 5
        'Hemant (02 Jul 2020) -- End
    End Enum
#End Region
    'Sohail (13 Apr 2016) -- End

#Region " Private Methods & Function "

    Private Sub GetValue()
        Dim dsList As DataSet
        Try
            marrEFTCustomColumnIds.Clear()
            'Sohail (25 Mar 2015) -- Start
            'B.C.Patel & Co Enhancement - New Custom CSV and XLS EFT for Advance module Payment and distribute payment as per employee banks.
            'dsList = objUserDefRMode.GetList("List", mintReportId, mintModeId)
            dsList = objUserDefRMode.GetList("List", mintReportId, mintModeId, mintReportTypeId)
            'Sohail (25 Mar 2015) -- End
            If dsList.Tables("List").Rows.Count > 0 Then
                'Hemant (27 June 2019) -- Start
                'ENHANCEMENT :  REF # 0003777: Bank upload file format. same as EFT custom Xls,CSV now add custom txt with same  option with TAB delimiter
                'For i = 0 To 2
                For i As Integer = 0 To GetEnumHighestValue(Of enReportMode)()
                    'Hemant (27 June 2019) -- End
                    If i = enReportMode.CUSTOM_COLUMNS Then 'Column order
                        marrEFTCustomColumnIds.AddRange(dsList.Tables("List").Rows(i).Item("transactionheadid").ToString.Split(CChar(",")))

                    ElseIf i = enReportMode.SHOW_REPORT_HEADER Then 'Show Column Header
                        chkShowColumnHeader.Checked = CBool(dsList.Tables("List").Rows(i).Item("transactionheadid"))

                        'Sohail (13 Apr 2016) -- Start
                        'Enhancement - 58.1 - Allow to show Employee Membership No. on EFT Custom Column XLS and CSV Report in Bank Payment List Report.
                    ElseIf i = enReportMode.MEMBERSHIP Then
                        'Sohail (26 Aug 2016) -- Start
                        'Issue - 63.1 - There is no row at position 2.
                        'cboMembership.SelectedValue = CInt(dsList.Tables("List").Rows(i).Item("transactionheadid"))
                        If dsList.Tables("List").Rows.Count > 2 Then
                        cboMembership.SelectedValue = CInt(dsList.Tables("List").Rows(i).Item("transactionheadid"))
                        Else
                            cboMembership.SelectedValue = 0
                        End If
                        'Sohail (26 Aug 2016) -- End
                        'Sohail (13 Apr 2016) -- End

                        'Hemant (27 June 2019) -- Start
                        'ENHANCEMENT :  REF # 0003777: Bank upload file format. same as EFT custom Xls,CSV now add custom txt with same  option with TAB delimiter
                    ElseIf i = enReportMode.SAVE_AS_TXT Then 'SAVE AS TXT
                        If dsList.Tables("List").Rows.Count > 3 Then
                            chkSaveAsTXT.Checked = CBool(dsList.Tables("List").Rows(i).Item("transactionheadid"))
                        End If

                    ElseIf i = enReportMode.TAB_DELIMITER Then 'TAB DELIMITER
                        If dsList.Tables("List").Rows.Count > 4 Then
                            chkTABDelimiter.Checked = CBool(dsList.Tables("List").Rows(i).Item("transactionheadid"))
                        End If
                        'Hemant (27 June 2019) -- End

                        'Hemant (02 Jul 2020) -- Start
                        'ENHANCEMENT(Freight Forwarders Kenya) #0004769: Add Payment Currency and Payment Date fields to EFT Custom Columns
                    ElseIf i = enReportMode.DATE_FORMAT Then 'Date Format
                        If dsList.Tables("List").Rows.Count > 5 Then
                            txtDateFormat.Text = CStr(dsList.Tables("List").Rows(i).Item("transactionheadid"))
                        Else
                            txtDateFormat.Text = "ddMMyyyy"
                        End If
                        'Hemant (02 Jul 2020) -- End

                    End If
                Next

            End If
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure : GetValue ; Module Name : " & mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub SetValue()
        Try
            objUserDefRMode._Reportmodeid = mintModeId
            objUserDefRMode._Reporttypeid = mintReportTypeId
            objUserDefRMode._Reportunkid = mintReportId
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure : SetValue ; Module Name : " & mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub FillList()
        Dim objMaster As New clsMasterData
        Dim dtTable As New DataTable
        Dim dsTicked As DataSet = Nothing
        Dim dsUnticked As DataSet = Nothing
        Dim lvItem As ListViewItem

        Try



            lvEFTCustomColumns.Items.Clear()
            Dim dicTicked As Dictionary(Of Integer, DataRow) = Nothing

            If marrEFTCustomColumnIds.Count > 0 Then
                dsTicked = objMaster.GetComboListForEFTCustomColumns("List", False, "ID IN (" & String.Join(",", CType(marrEFTCustomColumnIds.ToArray(Type.GetType("System.String")), String())) & ") ")
                dicTicked = (From p In dsTicked.Tables(0).AsEnumerable Select New With {.ID = CInt(p.Item("Id")), .DATAROW = p}).ToDictionary(Function(x) CInt(x.ID), Function(y) y.DATAROW)

                dsUnticked = objMaster.GetComboListForEFTCustomColumns("List", False, "ID NOT IN (" & String.Join(",", CType(marrEFTCustomColumnIds.ToArray(Type.GetType("System.String")), String())) & ") ")
            Else
                dsUnticked = objMaster.GetComboListForEFTCustomColumns("List", False)
            End If


            If dsTicked IsNot Nothing Then

                Dim dtRow As DataRow = Nothing
                For Each id As Integer In CType(marrEFTCustomColumnIds.Clone, ArrayList)
                    dtRow = dicTicked.Item(id)

                    lvItem = New ListViewItem

                    lvItem.Text = ""
                    lvItem.Tag = CInt(dtRow.Item("Id"))
                    lvItem.Checked = True

                    lvItem.SubItems.Add(dtRow.Item("NAME").ToString)

                    lvEFTCustomColumns.Items.Add(lvItem)

                    lvItem = Nothing
                Next

            End If


            If dsUnticked IsNot Nothing Then

                For Each dtRow As DataRow In dsUnticked.Tables(0).Rows
                    lvItem = New ListViewItem

                    lvItem.Text = ""
                    lvItem.Tag = CInt(dtRow.Item("Id"))
                    lvItem.Checked = False

                    lvItem.SubItems.Add(dtRow.Item("NAME").ToString)

                    lvEFTCustomColumns.Items.Add(lvItem)

                    lvItem = Nothing
                Next

            End If

            If lvEFTCustomColumns.Items.Count > 18 Then
                colhEFTCustomColumns.Width = 250 - 18
            Else
                colhEFTCustomColumns.Width = 250
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure : FillList ; Module Name : " & mstrModuleName)
        Finally
            lvItem = Nothing
        End Try
    End Sub

    Private Sub SetVisibility()
        Try
            Select Case CInt(mintModeId)
                Case enEFT_Export_Mode.CSV, enEFT_Export_Mode.XLS
                    lvEFTCustomColumns.Enabled = True
            End Select
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure : SetVisibility ; Module Name : " & mstrModuleName)
        Finally
        End Try
    End Sub

    Private Function IsValidate() As Boolean
        Try
            Select Case CInt(mintModeId)
                Case enEFT_Export_Mode.CSV, enEFT_Export_Mode.XLS
                    If lvEFTCustomColumns.CheckedItems.Count <= 0 Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please Select atleast one column to export EFT report."), enMsgBoxStyle.Information)
                        lvEFTCustomColumns.Focus()
                        Return False
                        'Sohail (13 Apr 2016) -- Start
                        'Enhancement - 58.1 - Allow to show Employee Membership No. on EFT Custom Column XLS and CSV Report in Bank Payment List Report.
                    ElseIf lvEFTCustomColumns.Items(enEFT_EFT_Custom_Columns.EMP_MEMBERSHIP_NO - 1).Checked = True AndAlso CInt(cboMembership.SelectedValue) <= 0 Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Please Select Membership."), enMsgBoxStyle.Information)
                        cboMembership.Focus()
                        Return False
                        'Sohail (13 Apr 2016) -- End
                    End If
            End Select

            mstrEFTCustomColumnsIds = String.Join(",", (From lv In lvEFTCustomColumns.CheckedItems.Cast(Of ListViewItem)() Select (lv.Tag.ToString)).ToArray)
            'Hemant (02 Jul 2020) -- Start
            'ENHANCEMENT(Freight Forwarders Kenya) #0004769: Add Payment Currency and Payment Date fields to EFT Custom Columns
            Dim strEFTCustomColumnsId() As String = Split(mstrEFTCustomColumnsIds, ",")
            If strEFTCustomColumnsId.Contains(CInt(enEFT_EFT_Custom_Columns.PAYMENT_DATE).ToString) = True AndAlso txtDateFormat.Text = "" Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Please Enter Date Format."), enMsgBoxStyle.Information)
                txtDateFormat.Focus()
                Return False
            End If
            'Hemant (02 Jul 2020) -- End
            mblnShowColumnHeader = chkShowColumnHeader.Checked
            'Sohail (13 Apr 2016) -- Start
            'Enhancement - 58.1 - Allow to show Employee Membership No. on EFT Custom Column XLS and CSV Report in Bank Payment List Report.
            mintMembershipUnkId = CInt(cboMembership.SelectedValue)
            'Sohail (13 Apr 2016) -- End

            'Hemant (27 June 2019) -- Start
            'ENHANCEMENT :  REF # 0003777: Bank upload file format. same as EFT custom Xls,CSV now add custom txt with same  option with TAB delimiter
            mblnSaveAsTXT = chkSaveAsTXT.Checked
            mblnTabDelimiter = chkTABDelimiter.Checked
            'Hemant (27 June 2019) -- End

            'Hemant (02 Jul 2020) -- Start
            'ENHANCEMENT(Freight Forwarders Kenya) #0004769: Add Payment Currency and Payment Date fields to EFT Custom Columns
            mstrDateFormat = txtDateFormat.Text
            'Hemant (02 Jul 2020) -- End

            Call SetValue()

            Return True

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "IsValidate", mstrModuleName)
        End Try
    End Function

    'Sohail (13 Apr 2016) -- Start
    'Enhancement - 58.1 - Allow to show Employee Membership No. on EFT Custom Column XLS and CSV Report in Bank Payment List Report.
    Private Sub FillCombo()
        Dim objMembership As New clsmembership_master
        Dim dsCombo As DataSet
        Try
            dsCombo = objMembership.getListForCombo("Membership", True)
            With cboMembership
                .ValueMember = "membershipunkid"
                .DisplayMember = "name"
                .DataSource = dsCombo.Tables("Membership")
                .SelectedValue = 0
            End With

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        Finally
            objMembership = Nothing
        End Try
    End Sub
    'Sohail (13 Apr 2016) -- End

#End Region

#Region " Form's Events "

    Private Sub frmEFTCustomCSVExport_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        Try
            objUserDefRMode = Nothing
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure : frmEFTCustomCSVExport_FormClosed ; Module Name : " & mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub frmEFTCustomColumnsExport_LanguageClick(ByVal sender As Object, ByVal eventArgs As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsMasterData.SetMessages()
            objfrm._Other_ModuleNames = "clsMasterData"
            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub

    Private Sub frmEFTCustomCSVExport_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        objUserDefRMode = New clsUserDef_ReportMode
        Try
            'Hemant (27 June 2019) -- Start
            'ENHANCEMENT :  REF # 0003777: Bank upload file format. same as EFT custom Xls,CSV now add custom txt with same  option with TAB delimiter
            Call Language.setLanguage(Me.Name)
            OtherSettings()
            If mintModeId = enEFT_Export_Mode.XLS Then
                chkSaveAsTXT.Visible = False
                chkSaveAsTXT.Checked = False
                chkTABDelimiter.Visible = False
                chkTABDelimiter.Checked = False
            End If
            'Hemant (27 June 2019) -- End
            objUserDefRMode._Reportmodeid = mintModeId
            objUserDefRMode._Reporttypeid = mintReportTypeId
            objUserDefRMode._Reportunkid = mintReportId
            Call SetVisibility()
            'Sohail (13 Apr 2016) -- Start
            'Enhancement - 58.1 - Allow to show Employee Membership No. on EFT Custom Column XLS and CSV Report in Bank Payment List Report.
            Call FillCombo()
            'Sohail (13 Apr 2016) -- End
            Call GetValue()

            Call FillList()
            Call GetValue() 'Sohail (13 Apr 2016)
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure : frmEFTCustomCSVExport_Load ; Module Name : " & mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

#Region " Button's Events "

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Me.Close()
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure : btnClose_Click ; Module Name : " & mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub btnOk_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnOk.Click
        Try
            If IsValidate() = False Then Exit Sub

            mblnCancel = False

            Me.Close()
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure : btnOk_Click ; Module Name : " & mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub btnSaveSelection_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSaveSelection.Click
        Try
            
            If IsValidate() = False Then Exit Sub

            'Hemant (27 June 2019) -- Start
            'ENHANCEMENT :  REF # 0003777: Bank upload file format. same as EFT custom Xls,CSV now add custom txt with same  option with TAB delimiter
            'For i As Integer = 0 To 2
            For i As Integer = 0 To GetEnumHighestValue(Of enReportMode)()
                'Hemant (27 June 2019) -- End
                objUserDefRMode = New clsUserDef_ReportMode

                Dim intUnkid As Integer = -1

                'Sohail (22 Jan 2019) -- Start
                'SLMC Enhancement - Support Issue # 2593 - 74.1 - Need option to select the required columns as in the “Bank Payment List” Report type "EFT Advance Salary CSV" to show bank details custom columns on E&D Detail with Bank Account Report.
                'objUserDefRMode._Reportunkid = enArutiReport.BankPaymentList
                objUserDefRMode._Reportunkid = mintReportId
                'Sohail (22 Jan 2019) -- End
                'Sohail (25 Mar 2015) -- Start
                'B.C.Patel & Co Enhancement - New Custom CSV and XLS EFT for Advance Payment.
                'objUserDefRMode._Reporttypeid = 0
                objUserDefRMode._Reporttypeid = mintReportTypeId
                'Sohail (25 Mar 2015) -- End
                objUserDefRMode._Reportmodeid = mintModeId

                If i = enReportMode.CUSTOM_COLUMNS Then 'Custom Columns

                   
                    objUserDefRMode._Headtypeid = i
                    objUserDefRMode._EarningTranHeadIds = mstrEFTCustomColumnsIds

                    intUnkid = objUserDefRMode.isExist(mintReportId, mintReportTypeId, mintModeId, i)

                ElseIf i = enReportMode.SHOW_REPORT_HEADER Then 'Show Column Header

                    objUserDefRMode._Headtypeid = i
                    objUserDefRMode._EarningTranHeadIds = mblnShowColumnHeader.ToString

                    intUnkid = objUserDefRMode.isExist(mintReportId, mintReportTypeId, mintModeId, i)

                    'Sohail (13 Apr 2016) -- Start
                    'Enhancement - 58.1 - Allow to show Employee Membership No. on EFT Custom Column XLS and CSV Report in Bank Payment List Report.
                ElseIf i = enReportMode.MEMBERSHIP Then

                    objUserDefRMode._Headtypeid = i
                    objUserDefRMode._EarningTranHeadIds = mintMembershipUnkId.ToString

                    intUnkid = objUserDefRMode.isExist(mintReportId, mintReportTypeId, mintModeId, i)
                    'Sohail (13 Apr 2016) -- End

                    'Hemant (27 June 2019) -- Start
                    'ENHANCEMENT :  REF # 0003777: Bank upload file format. same as EFT custom Xls,CSV now add custom txt with same  option with TAB delimiter
                ElseIf i = enReportMode.SAVE_AS_TXT Then 'SAVE AS TXT

                    objUserDefRMode._Headtypeid = i
                    objUserDefRMode._EarningTranHeadIds = mblnSaveAsTXT.ToString

                    intUnkid = objUserDefRMode.isExist(mintReportId, mintReportTypeId, mintModeId, i)

                ElseIf i = enReportMode.TAB_DELIMITER Then 'TAB DELIMITER

                    objUserDefRMode._Headtypeid = i
                    objUserDefRMode._EarningTranHeadIds = mblnTabDelimiter.ToString

                    intUnkid = objUserDefRMode.isExist(mintReportId, mintReportTypeId, mintModeId, i)
                    'Hemant (27 June 2019) -- End

                    'Hemant (02 Jul 2020) -- Start
                    'ENHANCEMENT(Freight Forwarders Kenya) #0004769: Add Payment Currency and Payment Date fields to EFT Custom Columns
                ElseIf i = enReportMode.DATE_FORMAT Then 'Date Format

                    objUserDefRMode._Headtypeid = i
                    objUserDefRMode._EarningTranHeadIds = mstrDateFormat.ToString

                    intUnkid = objUserDefRMode.isExist(mintReportId, mintReportTypeId, mintModeId, i)
                    'Hemant (02 Jul 2020) -- End


                End If
                
                objUserDefRMode._Reportmodeunkid = intUnkid


                'S.SANDEEP [28-May-2018] -- START
                'ISSUE/ENHANCEMENT : {Audit Trails} 
                objUserDefRMode._FormName = mstrModuleName
                objUserDefRMode._LoginEmployeeunkid = 0
                objUserDefRMode._ClientIP = getIP()
                objUserDefRMode._HostName = getHostName()
                objUserDefRMode._FromWeb = False
                objUserDefRMode._AuditUserId = User._Object._Userunkid
                objUserDefRMode._CompanyUnkid = Company._Object._Companyunkid
                objUserDefRMode._AuditDate = ConfigParameter._Object._CurrentDateAndTime
                'S.SANDEEP [28-May-2018] -- END
                If intUnkid <= 0 Then
                    objUserDefRMode.Insert()
                Else
                    objUserDefRMode.Update()
                End If


            Next


            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Information Successfully Saved."), enMsgBoxStyle.Information)

            mblnCancel = False

            Me.Close()

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure : btnSaveSelection_Click ; Module Name : " & mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub objbtnUp_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnUp.Click
        Try
            If lvEFTCustomColumns.SelectedItems.Count > 0 Then
                Dim SelIndex As Integer = lvEFTCustomColumns.SelectedIndices.Item(0)
                If SelIndex = 0 Then Exit Sub
                Dim lvPrevItem As ListViewItem
                lvPrevItem = lvEFTCustomColumns.Items(SelIndex - 1)
                lvEFTCustomColumns.Items(SelIndex - 1) = CType(lvEFTCustomColumns.Items(SelIndex).Clone, ListViewItem)
                lvEFTCustomColumns.Items(SelIndex) = CType(lvPrevItem.Clone, ListViewItem)
                lvEFTCustomColumns.Items(SelIndex - 1).Selected = True
                lvEFTCustomColumns.Items(SelIndex - 1).EnsureVisible()
            End If
            lvEFTCustomColumns.Focus()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnUp_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub objbtnDown_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnDown.Click
        Try
            If lvEFTCustomColumns.SelectedItems.Count > 0 Then
                Dim SelIndex As Integer = lvEFTCustomColumns.SelectedIndices.Item(0)
                If SelIndex = lvEFTCustomColumns.Items.Count - 1 Then Exit Sub
                Dim lvPrevItem As ListViewItem
                lvPrevItem = lvEFTCustomColumns.Items(SelIndex + 1)
                lvEFTCustomColumns.Items(SelIndex + 1) = CType(lvEFTCustomColumns.Items(SelIndex).Clone, ListViewItem)
                lvEFTCustomColumns.Items(SelIndex) = CType(lvPrevItem.Clone, ListViewItem)
                lvEFTCustomColumns.Items(SelIndex + 1).Selected = True
                lvEFTCustomColumns.Items(SelIndex + 1).EnsureVisible()
            End If
            lvEFTCustomColumns.Focus()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnDown_Click", mstrModuleName)
        Finally
        End Try
    End Sub
#End Region

#Region " Contols "

    Private Sub objchkNAll_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objchkNAll.CheckedChanged
        Try
            RemoveHandler lvEFTCustomColumns.ItemChecked, AddressOf lvEFTCustomColumns_ItemChecked
            For Each lvItem As ListViewItem In lvEFTCustomColumns.Items
                lvItem.Checked = CBool(objchkNAll.CheckState)
            Next

            marrEFTCustomColumnIds.Clear()
            marrEFTCustomColumnIds.AddRange((From p In lvEFTCustomColumns.CheckedItems.Cast(Of ListViewItem)() Select (p.Tag.ToString)).ToArray)

            'Sohail (13 Apr 2016) -- Start
            'Enhancement - 58.1 - Allow to show Employee Membership No. on EFT Custom Column XLS and CSV Report in Bank Payment List Report.
            If objchkNAll.Checked = True Then
                cboMembership.Enabled = True
            Else
                cboMembership.SelectedValue = 0
                cboMembership.Enabled = False
            End If
            'Sohail (13 Apr 2016) -- End

            'Hemant (02 Jul 2020) -- Start
            'ENHANCEMENT(Freight Forwarders Kenya) #0004769: Add Payment Currency and Payment Date fields to EFT Custom Columns
            If objchkNAll.Checked = True Then
                txtDateFormat.Enabled = True
            Else
                txtDateFormat.Text = "ddMMyyyy"
                txtDateFormat.Enabled = False
            End If
            'Hemant (02 Jul 2020) -- End

            AddHandler lvEFTCustomColumns.ItemChecked, AddressOf lvEFTCustomColumns_ItemChecked
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure : objchkNAll_CheckedChanged ; Module Name : " & mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub lvEFTCustomColumns_ItemChecked(ByVal sender As Object, ByVal e As System.Windows.Forms.ItemCheckedEventArgs) Handles lvEFTCustomColumns.ItemChecked
        Try
            RemoveHandler objchkNAll.CheckedChanged, AddressOf objchkNAll_CheckedChanged
            If lvEFTCustomColumns.CheckedItems.Count <= 0 Then
                objchkNAll.CheckState = CheckState.Unchecked
            ElseIf lvEFTCustomColumns.CheckedItems.Count < lvEFTCustomColumns.Items.Count Then
                objchkNAll.CheckState = CheckState.Indeterminate
            ElseIf lvEFTCustomColumns.CheckedItems.Count = lvEFTCustomColumns.Items.Count Then
                objchkNAll.CheckState = CheckState.Checked
            End If

            marrEFTCustomColumnIds.Clear()
            marrEFTCustomColumnIds.AddRange((From p In lvEFTCustomColumns.CheckedItems.Cast(Of ListViewItem)() Select (p.Tag.ToString)).ToArray)

            'Sohail (13 Apr 2016) -- Start
            'Enhancement - 58.1 - Allow to show Employee Membership No. on EFT Custom Column XLS and CSV Report in Bank Payment List Report.
            If CInt(e.Item.Tag) = enEFT_EFT_Custom_Columns.EMP_MEMBERSHIP_NO Then
                If e.Item.Checked = True Then
                    cboMembership.Enabled = True
                Else
                    cboMembership.SelectedValue = 0
                    cboMembership.Enabled = False
                End If
            End If
            'Sohail (13 Apr 2016) -- End

            'Hemant (02 Jul 2020) -- Start
            'ENHANCEMENT(Freight Forwarders Kenya) #0004769: Add Payment Currency and Payment Date fields to EFT Custom Columns
            If CInt(e.Item.Tag) = enEFT_EFT_Custom_Columns.PAYMENT_DATE Then
                If e.Item.Checked = True Then
                    txtDateFormat.Enabled = True
                Else
                    txtDateFormat.Text = "ddMMyyyy"
                    txtDateFormat.Enabled = False
                End If
            End If
            'Hemant (02 Jul 2020) -- End

            AddHandler objchkNAll.CheckedChanged, AddressOf objchkNAll_CheckedChanged
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lvEFTCustomColumns_ItemChecked", mstrModuleName)
        End Try
    End Sub
#End Region


	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Private Sub OtherSettings()
		Try
			Me.SuspendLayout()
			
			Call SetLanguage()
			
			Me.btnSaveSelection.GradientBackColor = GUI._ButttonBackColor 
			Me.btnSaveSelection.GradientForeColor = GUI._ButttonFontColor

			Me.btnOk.GradientBackColor = GUI._ButttonBackColor 
			Me.btnOk.GradientForeColor = GUI._ButttonFontColor

			Me.btnClose.GradientBackColor = GUI._ButttonBackColor 
			Me.btnClose.GradientForeColor = GUI._ButttonFontColor

			Me.objbtnUp.GradientBackColor = GUI._ButttonBackColor 
			Me.objbtnUp.GradientForeColor = GUI._ButttonFontColor

			Me.objbtnDown.GradientBackColor = GUI._ButttonBackColor 
			Me.objbtnDown.GradientForeColor = GUI._ButttonFontColor


			Me.ResumeLayout()
		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "OtherSettings", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetLanguage()
		Try
			Me.Text = Language._Object.getCaption(Me.Name, Me.Text)
			
			Me.elEFTCustomColumns.Text = Language._Object.getCaption(Me.elEFTCustomColumns.Name, Me.elEFTCustomColumns.Text)
			Me.colhEFTCustomColumns.Text = Language._Object.getCaption(CStr(Me.colhEFTCustomColumns.Tag), Me.colhEFTCustomColumns.Text)
			Me.btnSaveSelection.Text = Language._Object.getCaption(Me.btnSaveSelection.Name, Me.btnSaveSelection.Text)
			Me.btnOk.Text = Language._Object.getCaption(Me.btnOk.Name, Me.btnOk.Text)
			Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
			Me.chkShowColumnHeader.Text = Language._Object.getCaption(Me.chkShowColumnHeader.Name, Me.chkShowColumnHeader.Text)
			Me.lblMembership.Text = Language._Object.getCaption(Me.lblMembership.Name, Me.lblMembership.Text)
			Me.chkSaveAsTXT.Text = Language._Object.getCaption(Me.chkSaveAsTXT.Name, Me.chkSaveAsTXT.Text)
			Me.chkTABDelimiter.Text = Language._Object.getCaption(Me.chkTABDelimiter.Name, Me.chkTABDelimiter.Text)
			Me.lblDateFormat.Text = Language._Object.getCaption(Me.lblDateFormat.Name, Me.lblDateFormat.Text)

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetLanguage", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "Please Select atleast one column to export EFT report.")
			Language.setMessage(mstrModuleName, 2, "Information Successfully Saved.")
			Language.setMessage(mstrModuleName, 3, "Please Select Membership.")
			Language.setMessage(mstrModuleName, 4, "Please Enter Date Format.")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class