﻿Imports eZeeCommonLib

Public Class ArutiReportViewer
    Private ReadOnly mstrModuleName As String = "ArutiReportViewer"
    'Private m_strReportName As String = ecore32.core.NGTitle
    Private m_strReportName As String = "ArutiReport"

    Public Property _Heading() As String
        Get
            Return m_strReportName
        End Get
        Set(ByVal value As String)
            m_strReportName = value
        End Set
    End Property

    'Sandeep [ 07 FEB 2011 ] -- START
    Private Sub ArutiReportViewer_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        If crwArutiReportViewer.ReportSource IsNot Nothing Then
            crwArutiReportViewer.ReportSource.close()
            crwArutiReportViewer.ReportSource.dispose()
        End If
    End Sub
    'Sandeep [ 07 FEB 2011 ] -- END 


    'S.SANDEEP [ 24 JUNE 2011 ] -- START
    'ISSUE : INCLUSION OF INACTIVE EMPLOYEE
    Private Sub ArutiReportViewer_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Me.Text = m_strReportName
        Call Set_Logo(Me, gApplicationType)

    End Sub
    'S.SANDEEP [ 24 JUNE 2011 ] -- END 

    
End Class