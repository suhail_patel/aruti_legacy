﻿Option Strict On

#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data

#End Region

Public Class frmViewAnalysis

#Region " Private Variables "

    Private ReadOnly mstrModuleName As String = "frmViewAnalysis"
    Private mstrReportBy_Ids As String = String.Empty
    Private mstrReportBy_Name As String = String.Empty
    Private mblnCancel As Boolean = True
    Private dsList As New DataSet
    Private mintViewIndex As Integer
    'Sohail (26 Nov 2011) -- Start
    Private mstrAnalysis_Fields As String = String.Empty
    Private mstrAnalysis_Join As String = String.Empty
    Private mstrAnalysis_OrderBy As String = String.Empty
    Private mstrAnalysis_OrderBy_GroupName As String = String.Empty 'Sohail (24 Jan 2013)
    Private mstrHr_EmployeeTable_Alias As String = String.Empty
    Private mstrReport_GroupName As String = String.Empty
    'Sohail (26 Nov 2011) -- End
    Private mstrAnalysis_TableName As String = String.Empty 'Sohail (12 May 2012)
    'Sohail (01 Mar 2017) -- Start
    'Enhancement #24 -  65.1 - Export and Import option on Budget Codes.
    Private mstrAnalysis_CodeField As String = String.Empty
    'Sohail (01 Mar 2017) -- End

    'S.SANDEEP [ 18 FEB 2012 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Private dtTable As New DataTable
    'S.SANDEEP [ 18 FEB 2012 ] -- END

    'S.SANDEEP [06 MAY 2015] -- START
    Private mdtEffectiveDate As Date = eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate)
    'S.SANDEEP [06 MAY 2015] -- END

    'Sohail (07 Jun 2016) -- Start
    'Enhancement -  New Budget Redesign for MST in 62.1 SP.
    Private mstrAllocationUnkIDs As String = ""
    Private mblnCloseForm As Boolean = False
    Private mblnOnlyJobEnabled As Boolean = False
    'Sohail (07 Jun 2016) -- End

#End Region

#Region " Properties "

    Public Property _ReportBy_Ids() As String
        Get
            Return mstrReportBy_Ids
        End Get
        Set(ByVal value As String)
            mstrReportBy_Ids = value
        End Set
    End Property

    Public Property _ReportBy_Name() As String
        Get
            Return mstrReportBy_Name
        End Get
        Set(ByVal value As String)
            mstrReportBy_Name = value
        End Set
    End Property

    Public ReadOnly Property _ViewIndex() As Integer
        Get
            Return mintViewIndex
        End Get
    End Property

    'Sohail (26 Nov 2011) -- Start
    Public ReadOnly Property _Analysis_Fields() As String
        Get
            Return mstrAnalysis_Fields
        End Get
    End Property

    Public ReadOnly Property _Analysis_Join() As String
        Get
            Return mstrAnalysis_Join
        End Get
    End Property

    Public ReadOnly Property _Analysis_OrderBy() As String
        Get
            Return mstrAnalysis_OrderBy
        End Get
    End Property

    'Sohail (24 Jan 2013) -- Start
    'TRA - ENHANCEMENT
    Public ReadOnly Property _Analysis_OrderBy_GName() As String
        Get
            Return mstrAnalysis_OrderBy_GroupName
        End Get
    End Property
    'Sohail (24 Jan 2013) -- End

    Public ReadOnly Property _Report_GroupName() As String
        Get
            Return mstrReport_GroupName
        End Get
    End Property
    'Sohail (26 Nov 2011) -- End

    'Sohail (12 May 2012) -- Start
    'TRA - ENHANCEMENT
    Public ReadOnly Property _Analysis_TableName() As String
        Get
            Return mstrAnalysis_TableName
        End Get
    End Property
    'Sohail (12 May 2012) -- End


    'S.SANDEEP [06 MAY 2015] -- START
    Public WriteOnly Property _EffectiveDate() As Date
        Set(ByVal value As Date)
            mdtEffectiveDate = value
        End Set
    End Property
    'S.SANDEEP [06 MAY 2015] -- END

    'Sohail (01 Mar 2017) -- Start
    'Enhancement #24 -  65.1 - Export and Import option on Budget Codes.
    Public ReadOnly Property _Analysis_CodeField() As String
        Get
            Return mstrAnalysis_CodeField
        End Get
    End Property
    'Sohail (01 Mar 2017) -- End

    'Sohail (22 Jan 2021) -- Start
    'NMB Issue : - Payroll total variance report emp count not matching with payroll summary report for 2 different database periods.
    Private mstrDBName As String = ""
    Public WriteOnly Property _DatabaseName() As String
        Set(ByVal value As String)
            mstrDBName = value
        End Set
    End Property
    'Sohail (22 Jan 2021) -- End

#End Region

#Region " Dispaly Dialog "

    'Sohail (26 Nov 2011) -- Start
    'Sohail (07 Jun 2016) -- Start
    'Enhancement -  New Budget Redesign for MST in 62.1 SP.
    'Public Function displayDialog(Optional ByVal strHr_EmployeeTable_Alias As String = "hremployee_master") As Boolean
    Public Function displayDialog(Optional ByVal strHr_EmployeeTable_Alias As String = "hremployee_master" _
                                  , Optional ByVal intViewIndex As Integer = -1 _
                                  , Optional ByVal strReportByIDs As String = "" _
                                  , Optional ByVal blnCloseForm As Boolean = False _
                                  , Optional ByVal blnOnlyJobEnabled As Boolean = False _
                                  ) As Boolean
        'Sohail (07 Jun 2016) -- End
        'Public Function displayDialog() As Boolean
        'Sohail (26 Nov 2011) -- End
        Try
            mstrHr_EmployeeTable_Alias = strHr_EmployeeTable_Alias 'Sohail (26 Nov 2011)

            'Sohail (07 Jun 2016) -- Start
            'Enhancement -  New Budget Redesign for MST in 62.1 SP.
            mintViewIndex = intViewIndex
            mstrAllocationUnkIDs = strReportByIDs
            mblnCloseForm = blnCloseForm
            mblnOnlyJobEnabled = blnOnlyJobEnabled
            'Sohail (07 Jun 2016) -- End

            Me.ShowDialog()
            Return Not mblnCancel
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "displayDialog", mstrModuleName)
        End Try
    End Function

#End Region

#Region " Form Events "
    Private Sub frmViewAnalysis_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Call Set_Logo(Me, gApplicationType)
            Language.setLanguage(Me.Name)
            Call OtherSettings()
            'Shani [ 10 OCT 2014 ] -- START
            'Enhancement - Assign allocation tags for same allcation Enums to all reports (assign by Sandeepsir)
            Call Assign_Tags()
            'Shani [ 10 OCT 2014 ] -- End

            'Sohail (07 Jun 2016) -- Start
            'Enhancement -  New Budget Redesign for MST in 62.1 SP.
            If mblnOnlyJobEnabled = True Then
                Dim lst As IEnumerable(Of RadioButton) = (From r In pnlAnalysis.Controls.OfType(Of RadioButton)().Where(Function(t) t.Name <> radJob.Name))
                For Each ctrl In lst
                    ctrl.Enabled = False
                Next
            End If

            If mintViewIndex > 0 Then

                Select Case mintViewIndex
                    'Sohail (02 Sep 2016) -- Start
                    'Enhancement -  64.1 - New Budget Redesign for Marie Stopes (New JV Report Sun Account Project JV).
                    'Case enAllocation.BRANCH
                    '    radBranch.Checked = True
                    'Case enAllocation.DEPARTMENT_GROUP
                    '    radDepartmentGrp.Checked = True
                    'Case enAllocation.DEPARTMENT
                    '    radDepartment.Checked = True
                    'Case enAllocation.SECTION_GROUP
                    '    radSectionGrp.Checked = True
                    'Case enAllocation.SECTION
                    '    radSection.Checked = True
                    'Case enAllocation.UNIT_GROUP
                    '    radUnitGrp.Checked = True
                    'Case enAllocation.UNIT
                    '    radUnit.Checked = True
                    'Case enAllocation.TEAM
                    '    radTeam.Checked = True
                    'Case enAllocation.JOB_GROUP
                    '    radJobGrp.Checked = True
                    'Case enAllocation.JOBS
                    '    radJob.Checked = True
                    'Case enAllocation.CLASS_GROUP
                    '    radClassGrp.Checked = True
                    'Case enAllocation.CLASSES
                    '    radClass.Checked = True
                    'Case enAllocation.COST_CENTER
                    '    radCostCenter.Checked = True
                    Case enAnalysisReport.Branch
                        radBranch.Checked = True

                    Case enAnalysisReport.Department
                        radDepartment.Checked = True

                    Case enAnalysisReport.Section
                        radSection.Checked = True

                    Case enAnalysisReport.Unit
                        radUnit.Checked = True

                    Case enAnalysisReport.Job
                        radJob.Checked = True

                    Case enAnalysisReport.CostCenter
                        radCostCenter.Checked = True

                    Case enAnalysisReport.SectionGroup
                        radSectionGrp.Checked = True

                    Case enAnalysisReport.UnitGroup
                        radUnitGrp.Checked = True

                    Case enAnalysisReport.Team
                        radTeam.Checked = True

                    Case enAnalysisReport.DepartmentGroup
                        radDepartmentGrp.Checked = True

                    Case enAnalysisReport.JobGroup
                        radJobGrp.Checked = True

                    Case enAnalysisReport.ClassGroup
                        radClassGrp.Checked = True

                    Case enAnalysisReport.Classs
                        radClass.Checked = True

                    Case enAnalysisReport.GradeGroup
                        radGradeGrp.Checked = True

                    Case enAnalysisReport.Grade
                        radGrade.Checked = True

                    Case enAnalysisReport.GradeLevel
                        radGradeLevel.Checked = True

                        'Sohail (02 Sep 2016) -- End
                End Select

                Dim lstRow = (From p In dtTable Where (mstrAllocationUnkIDs.Split(CChar(",")).Contains(p.Item(objdgcolhReportById.DataPropertyName).ToString) = True) Select (p)).ToList

                For Each dsRow As DataRow In lstRow
                    dsRow.Item("IsCheck") = True
                Next
                dtTable.AcceptChanges()

                mstrAllocationUnkIDs = ""

                If mblnCloseForm = True Then
                    btnSet.PerformClick()
                End If
            End If
            'Sohail (07 Jun 2016) -- End

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmViewAnalysis_Load", mstrModuleName)
        End Try
    End Sub

    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub

#End Region

#Region " Radio Button's Events "

    Private Sub radBranch_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles radBranch.CheckedChanged, radCostCenter.CheckedChanged, radDepartment.CheckedChanged, _
                                                                                                                                                                      radJob.CheckedChanged, radSection.CheckedChanged, radUnit.CheckedChanged, _
                                                                                                                                                                      radSectionGrp.CheckedChanged, radUnitGrp.CheckedChanged, radTeam.CheckedChanged, _
                                                                                                             radDepartmentGrp.CheckedChanged, radJobGrp.CheckedChanged, radClassGrp.CheckedChanged, radClass.CheckedChanged, _
                                                                                                             radGradeGrp.CheckedChanged, radGrade.CheckedChanged, radGradeLevel.CheckedChanged
        Try
            If CType(sender, RadioButton).Checked = True Then
                dgvData.AutoGenerateColumns = False

                'Pinkal (12-Oct-2011) -- Start
                chkSelectallList.Checked = False
                'Pinkal (12-Oct-2011) -- End

                Select Case CType(sender, RadioButton).Name.ToUpper
                    Case "RADBRANCH"
                        Dim objBranch As New clsStation
                        dsList = objBranch.GetList("List", True)
                        dsList.Tables(0).Columns.Add("IsCheck", System.Type.GetType("System.Boolean")).DefaultValue = False

                        For i As Integer = 0 To dsList.Tables("List").Rows.Count - 1
                            dsList.Tables("List").Rows(i)("IsCheck") = False
                        Next
                        dsList.Tables("List").AcceptChanges()

                        objdgcolhCheck.DataPropertyName = "IsCheck"
                        objdgcolhReportById.DataPropertyName = "stationunkid"
                        dgcolhReportBy.DataPropertyName = "name"

                        'Sohail (31 May 2012) -- Start
                        'TRA - ENHANCEMENT
                        'Select Case ConfigParameter._Object._UserAccessModeSetting
                        '    Case enAllocation.BRANCH
                        '        If UserAccessLevel._AccessLevel.Length > 0 Then
                        '            dtTable = New DataView(dsList.Tables("List"), "stationunkid IN (" & UserAccessLevel._AccessLevel & ")", "", DataViewRowState.CurrentRows).ToTable
                        '        Else
                        '            dtTable = dsList.Tables("List")
                        '        End If
                        '    Case Else
                        '        dtTable = dsList.Tables("List")
                        'End Select 
                        If UserAccessLevel._AccessLevelBranchFilterString.Trim.Length > 0 Then
                            dtTable = New DataView(dsList.Tables("List"), UserAccessLevel._AccessLevelBranchFilterString.Replace("hremployee_master.", ""), "", DataViewRowState.CurrentRows).ToTable
                        Else
                            dtTable = dsList.Tables("List")
                        End If
                        'Sohail (31 May 2012) -- End

                        objBranch = Nothing
                    Case "RADDEPARTMENT"
                        Dim objDept As New clsDepartment
                        dsList = objDept.GetList("List", True)
                        dsList.Tables(0).Columns.Add("IsCheck", System.Type.GetType("System.Boolean")).DefaultValue = False

                        For i As Integer = 0 To dsList.Tables("List").Rows.Count - 1
                            dsList.Tables("List").Rows(i)("IsCheck") = False
                        Next
                        dsList.Tables("List").AcceptChanges()

                        objdgcolhCheck.DataPropertyName = "IsCheck"
                        objdgcolhReportById.DataPropertyName = "departmentunkid"
                        dgcolhReportBy.DataPropertyName = "name"

                        'Sohail (31 May 2012) -- Start
                        'TRA - ENHANCEMENT
                        'Select Case ConfigParameter._Object._UserAccessModeSetting
                        '    Case enAllocation.DEPARTMENT
                        '        If UserAccessLevel._AccessLevel.Length > 0 Then
                        '            dtTable = New DataView(dsList.Tables("List"), "departmentunkid IN (" & UserAccessLevel._AccessLevel & ")", "", DataViewRowState.CurrentRows).ToTable
                        '        Else
                        '            dtTable = dsList.Tables("List")
                        '        End If
                        '    Case Else
                        '        dtTable = dsList.Tables("List")
                        'End Select
                        If UserAccessLevel._AccessLevelDepartmentFilterString.Trim.Length > 0 Then
                            dtTable = New DataView(dsList.Tables("List"), UserAccessLevel._AccessLevelDepartmentFilterString.Replace("hremployee_master.", ""), "", DataViewRowState.CurrentRows).ToTable
                        Else
                            dtTable = dsList.Tables("List")
                        End If
                        'Sohail (31 May 2012) -- End

                        objDept = Nothing
                    Case "RADJOB"
                        Dim objJob As New clsJobs
                        dsList = objJob.GetList("List", True)
                        dsList.Tables(0).Columns.Add("IsCheck", System.Type.GetType("System.Boolean")).DefaultValue = False

                        For i As Integer = 0 To dsList.Tables("List").Rows.Count - 1
                            dsList.Tables("List").Rows(i)("IsCheck") = False
                        Next
                        dsList.Tables("List").AcceptChanges()

                        objdgcolhCheck.DataPropertyName = "IsCheck"
                        objdgcolhReportById.DataPropertyName = "jobunkid"
                        dgcolhReportBy.DataPropertyName = "JobName"

                        'Sohail (31 May 2012) -- Start
                        'TRA - ENHANCEMENT
                        'Select Case ConfigParameter._Object._UserAccessModeSetting
                        '    Case enAllocation.JOBS
                        '        If UserAccessLevel._AccessLevel.Length > 0 Then
                        '            dtTable = New DataView(dsList.Tables("List"), "jobunkid IN (" & UserAccessLevel._AccessLevel & ")", "", DataViewRowState.CurrentRows).ToTable
                        '        Else
                        '            dtTable = dsList.Tables("List")
                        '        End If
                        '    Case Else
                        '        dtTable = dsList.Tables("List")
                        'End Select
                        If UserAccessLevel._AccessLevelJobFilterString.Trim.Length > 0 Then
                            dtTable = New DataView(dsList.Tables("List"), UserAccessLevel._AccessLevelJobFilterString.Replace("hremployee_master.", ""), "", DataViewRowState.CurrentRows).ToTable
                        Else
                            dtTable = dsList.Tables("List")
                        End If
                        'Sohail (31 May 2012) -- End

                        objJob = Nothing
                    Case "RADSECTION"
                        Dim objSection As New clsSections
                        dsList = objSection.GetList("List", True)
                        dsList.Tables(0).Columns.Add("IsCheck", System.Type.GetType("System.Boolean")).DefaultValue = False

                        For i As Integer = 0 To dsList.Tables("List").Rows.Count - 1
                            dsList.Tables("List").Rows(i)("IsCheck") = False
                        Next
                        dsList.Tables("List").AcceptChanges()

                        objdgcolhCheck.DataPropertyName = "IsCheck"
                        objdgcolhReportById.DataPropertyName = "sectionunkid"
                        dgcolhReportBy.DataPropertyName = "name"

                        'Sohail (31 May 2012) -- Start
                        'TRA - ENHANCEMENT
                        'Select Case ConfigParameter._Object._UserAccessModeSetting
                        '    Case enAllocation.SECTION
                        '        If UserAccessLevel._AccessLevel.Length > 0 Then
                        '            dtTable = New DataView(dsList.Tables("List"), "sectionunkid IN (" & UserAccessLevel._AccessLevel & ")", "", DataViewRowState.CurrentRows).ToTable
                        '        Else
                        '            dtTable = dsList.Tables("List")
                        '        End If
                        '    Case Else
                        '        dtTable = dsList.Tables("List")
                        'End Select
                        If UserAccessLevel._AccessLevelSectionFilterString.Trim.Length > 0 Then
                            dtTable = New DataView(dsList.Tables("List"), UserAccessLevel._AccessLevelSectionFilterString.Replace("hremployee_master.", ""), "", DataViewRowState.CurrentRows).ToTable
                        Else
                            dtTable = dsList.Tables("List")
                        End If
                        'Sohail (31 May 2012) -- End

                        objSection = Nothing
                    Case "RADUNIT"
                        Dim objUnit As New clsUnits
                        dsList = objUnit.GetList("List", True)
                        dsList.Tables(0).Columns.Add("IsCheck", System.Type.GetType("System.Boolean")).DefaultValue = False

                        For i As Integer = 0 To dsList.Tables("List").Rows.Count - 1
                            dsList.Tables("List").Rows(i)("IsCheck") = False
                        Next
                        dsList.Tables("List").AcceptChanges()

                        objdgcolhCheck.DataPropertyName = "IsCheck"
                        objdgcolhReportById.DataPropertyName = "unitunkid"
                        dgcolhReportBy.DataPropertyName = "name"

                        'Sohail (31 May 2012) -- Start
                        'TRA - ENHANCEMENT
                        'Select Case ConfigParameter._Object._UserAccessModeSetting
                        '    Case enAllocation.UNIT
                        '        If UserAccessLevel._AccessLevel.Length > 0 Then
                        '            dtTable = New DataView(dsList.Tables("List"), "unitunkid IN (" & UserAccessLevel._AccessLevel & ")", "", DataViewRowState.CurrentRows).ToTable
                        '        Else
                        '            dtTable = dsList.Tables("List")
                        '        End If
                        '    Case Else
                        '        dtTable = dsList.Tables("List")
                        'End Select
                        If UserAccessLevel._AccessLevelUnitFilterString.Trim.Length > 0 Then
                            dtTable = New DataView(dsList.Tables("List"), UserAccessLevel._AccessLevelUnitFilterString.Replace("hremployee_master.", ""), "", DataViewRowState.CurrentRows).ToTable
                        Else
                            dtTable = dsList.Tables("List")
                        End If
                        'Sohail (31 May 2012) -- End

                        objUnit = Nothing
                    Case "RADCOSTCENTER"
                        Dim objCost As New clscostcenter_master
                        dsList = objCost.GetList("List", True)
                        dsList.Tables(0).Columns.Add("IsCheck", System.Type.GetType("System.Boolean")).DefaultValue = False

                        For i As Integer = 0 To dsList.Tables("List").Rows.Count - 1
                            dsList.Tables("List").Rows(i)("IsCheck") = False
                        Next
                        dsList.Tables("List").AcceptChanges()

                        objdgcolhCheck.DataPropertyName = "IsCheck"
                        objdgcolhReportById.DataPropertyName = "costcenterunkid"
                        dgcolhReportBy.DataPropertyName = "costcentername"

                        dtTable = dsList.Tables("List")

                        objCost = Nothing


                        'Pinkal (12-Oct-2011) -- Start
                        'Enhancement : TRA Changes

                    Case "RADSECTIONGRP"
                        Dim objSectionGrp As New clsSectionGroup
                        dsList = objSectionGrp.GetList("List", True)
                        dsList.Tables(0).Columns.Add("IsCheck", System.Type.GetType("System.Boolean")).DefaultValue = False

                        For i As Integer = 0 To dsList.Tables("List").Rows.Count - 1
                            dsList.Tables("List").Rows(i)("IsCheck") = False
                        Next
                        dsList.Tables("List").AcceptChanges()

                        objdgcolhCheck.DataPropertyName = "IsCheck"
                        objdgcolhReportById.DataPropertyName = "sectiongroupunkid"
                        dgcolhReportBy.DataPropertyName = "name"

                        'Sohail (31 May 2012) -- Start
                        'TRA - ENHANCEMENT
                        'Select Case ConfigParameter._Object._UserAccessModeSetting
                        '    Case enAllocation.SECTION_GROUP
                        '        If UserAccessLevel._AccessLevel.Length > 0 Then
                        '            dtTable = New DataView(dsList.Tables("List"), "sectiongroupunkid IN (" & UserAccessLevel._AccessLevel & ")", "", DataViewRowState.CurrentRows).ToTable
                        '        Else
                        '            dtTable = dsList.Tables("List")
                        '        End If
                        '    Case Else
                        '        dtTable = dsList.Tables("List")
                        'End Select
                        If UserAccessLevel._AccessLevelSectionGroupFilterString.Trim.Length > 0 Then
                            dtTable = New DataView(dsList.Tables("List"), UserAccessLevel._AccessLevelSectionGroupFilterString.Replace("hremployee_master.", ""), "", DataViewRowState.CurrentRows).ToTable
                        Else
                            dtTable = dsList.Tables("List")
                        End If
                        'Sohail (31 May 2012) -- End

                        objSectionGrp = Nothing

                    Case "RADUNITGRP"
                        Dim objUnitGrp As New clsUnitGroup
                        dsList = objUnitGrp.GetList("List", True)
                        dsList.Tables(0).Columns.Add("IsCheck", System.Type.GetType("System.Boolean")).DefaultValue = False

                        For i As Integer = 0 To dsList.Tables("List").Rows.Count - 1
                            dsList.Tables("List").Rows(i)("IsCheck") = False
                        Next
                        dsList.Tables("List").AcceptChanges()

                        objdgcolhCheck.DataPropertyName = "IsCheck"
                        objdgcolhReportById.DataPropertyName = "unitgroupunkid"
                        dgcolhReportBy.DataPropertyName = "name"

                        'Sohail (31 May 2012) -- Start
                        'TRA - ENHANCEMENT
                        'Select Case ConfigParameter._Object._UserAccessModeSetting
                        '    Case enAllocation.UNIT_GROUP
                        '        If UserAccessLevel._AccessLevel.Length > 0 Then
                        '            dtTable = New DataView(dsList.Tables("List"), "unitgroupunkid IN (" & UserAccessLevel._AccessLevel & ")", "", DataViewRowState.CurrentRows).ToTable
                        '        Else
                        '            dtTable = dsList.Tables("List")
                        '        End If
                        '    Case Else
                        '        dtTable = dsList.Tables("List")
                        'End Select
                        If UserAccessLevel._AccessLevelUnitGroupFilterString.Trim.Length > 0 Then
                            dtTable = New DataView(dsList.Tables("List"), UserAccessLevel._AccessLevelUnitGroupFilterString.Replace("hremployee_master.", ""), "", DataViewRowState.CurrentRows).ToTable
                        Else
                            dtTable = dsList.Tables("List")
                        End If
                        'Sohail (31 May 2012) -- End

                        objUnitGrp = Nothing


                    Case "RADTEAM"
                        Dim objTeam As New clsTeams
                        dsList = objTeam.GetList("List", True)
                        dsList.Tables(0).Columns.Add("IsCheck", System.Type.GetType("System.Boolean")).DefaultValue = False

                        For i As Integer = 0 To dsList.Tables("List").Rows.Count - 1
                            dsList.Tables("List").Rows(i)("IsCheck") = False
                        Next
                        dsList.Tables("List").AcceptChanges()

                        objdgcolhCheck.DataPropertyName = "IsCheck"
                        objdgcolhReportById.DataPropertyName = "teamunkid"
                        dgcolhReportBy.DataPropertyName = "name"

                        'Sohail (31 May 2012) -- Start
                        'TRA - ENHANCEMENT
                        'Select Case ConfigParameter._Object._UserAccessModeSetting
                        '    Case enAllocation.TEAM
                        '        If UserAccessLevel._AccessLevel.Length > 0 Then
                        '            dtTable = New DataView(dsList.Tables("List"), "teamunkid IN (" & UserAccessLevel._AccessLevel & ")", "", DataViewRowState.CurrentRows).ToTable
                        '        Else
                        '            dtTable = dsList.Tables("List")
                        '        End If
                        '    Case Else
                        '        dtTable = dsList.Tables("List")
                        'End Select
                        If UserAccessLevel._AccessLevelTeamFilterString.Trim.Length > 0 Then
                            dtTable = New DataView(dsList.Tables("List"), UserAccessLevel._AccessLevelTeamFilterString.Replace("hremployee_master.", ""), "", DataViewRowState.CurrentRows).ToTable
                        Else
                            dtTable = dsList.Tables("List")
                        End If
                        'Sohail (31 May 2012) -- End

                        objTeam = Nothing

                        'Pinkal (12-Oct-2011) -- End

                        'Sohail (16 May 2012) -- Start
                        'TRA - ENHANCEMENT
                    Case radDepartmentGrp.Name.ToUpper
                        Dim objDeptGrp As New clsDepartmentGroup
                        dsList = objDeptGrp.GetList("List", True)
                        dsList.Tables(0).Columns.Add("IsCheck", System.Type.GetType("System.Boolean")).DefaultValue = False

                        For i As Integer = 0 To dsList.Tables("List").Rows.Count - 1
                            dsList.Tables("List").Rows(i)("IsCheck") = False
                        Next
                        dsList.Tables("List").AcceptChanges()

                        objdgcolhCheck.DataPropertyName = "IsCheck"
                        objdgcolhReportById.DataPropertyName = "deptgroupunkid"
                        dgcolhReportBy.DataPropertyName = "name"

                        'Sohail (31 May 2012) -- Start
                        'TRA - ENHANCEMENT
                        'Select Case ConfigParameter._Object._UserAccessModeSetting
                        '    Case enAllocation.DEPARTMENT_GROUP
                        '        If UserAccessLevel._AccessLevel.Length > 0 Then
                        '            dtTable = New DataView(dsList.Tables("List"), "deptgroupunkid IN (" & UserAccessLevel._AccessLevel & ")", "", DataViewRowState.CurrentRows).ToTable
                        '        Else
                        '            dtTable = dsList.Tables("List")
                        '        End If
                        '    Case Else
                        '        dtTable = dsList.Tables("List")
                        'End Select
                        If UserAccessLevel._AccessLevelDepartmentGroupFilterString.Trim.Length > 0 Then
                            dtTable = New DataView(dsList.Tables("List"), UserAccessLevel._AccessLevelDepartmentGroupFilterString.Replace("hremployee_master.", ""), "", DataViewRowState.CurrentRows).ToTable
                        Else
                            dtTable = dsList.Tables("List")
                        End If
                        'Sohail (31 May 2012) -- End

                        objDeptGrp = Nothing

                    Case radJobGrp.Name.ToUpper
                        Dim objJobGrp As New clsJobGroup
                        dsList = objJobGrp.GetList("List", True)
                        dsList.Tables(0).Columns.Add("IsCheck", System.Type.GetType("System.Boolean")).DefaultValue = False

                        For i As Integer = 0 To dsList.Tables("List").Rows.Count - 1
                            dsList.Tables("List").Rows(i)("IsCheck") = False
                        Next
                        dsList.Tables("List").AcceptChanges()

                        objdgcolhCheck.DataPropertyName = "IsCheck"
                        objdgcolhReportById.DataPropertyName = "jobgroupunkid"
                        dgcolhReportBy.DataPropertyName = "name"

                        'Sohail (31 May 2012) -- Start
                        'TRA - ENHANCEMENT
                        'Select Case ConfigParameter._Object._UserAccessModeSetting
                        '    Case enAllocation.JOB_GROUP
                        '        If UserAccessLevel._AccessLevel.Length > 0 Then
                        '            dtTable = New DataView(dsList.Tables("List"), "jobgroupunkid IN (" & UserAccessLevel._AccessLevel & ")", "", DataViewRowState.CurrentRows).ToTable
                        '        Else
                        '            dtTable = dsList.Tables("List")
                        '        End If
                        '    Case Else
                        '        dtTable = dsList.Tables("List")
                        'End Select
                        If UserAccessLevel._AccessLevelJobGroupFilterString.Trim.Length > 0 Then
                            dtTable = New DataView(dsList.Tables("List"), UserAccessLevel._AccessLevelJobGroupFilterString.Replace("hremployee_master.", ""), "", DataViewRowState.CurrentRows).ToTable
                        Else
                            dtTable = dsList.Tables("List")
                        End If
                        'Sohail (31 May 2012) -- End

                        objJobGrp = Nothing

                    Case radClassGrp.Name.ToUpper
                        Dim objClassGrp As New clsClassGroup
                        dsList = objClassGrp.GetList("List", True)
                        dsList.Tables(0).Columns.Add("IsCheck", System.Type.GetType("System.Boolean")).DefaultValue = False

                        For i As Integer = 0 To dsList.Tables("List").Rows.Count - 1
                            dsList.Tables("List").Rows(i)("IsCheck") = False
                        Next
                        dsList.Tables("List").AcceptChanges()

                        objdgcolhCheck.DataPropertyName = "IsCheck"
                        objdgcolhReportById.DataPropertyName = "classgroupunkid"
                        dgcolhReportBy.DataPropertyName = "name"

                        'Sohail (31 May 2012) -- Start
                        'TRA - ENHANCEMENT
                        'Select Case ConfigParameter._Object._UserAccessModeSetting
                        '    'Case enAllocation.CLASS_GROUP
                        '    '    If UserAccessLevel._AccessLevel.Length > 0 Then
                        '    '        dtTable = New DataView(dsList.Tables("List"), "classgroupunkid IN (" & UserAccessLevel._AccessLevel & ")", "", DataViewRowState.CurrentRows).ToTable
                        '    '    Else
                        '    '        dtTable = dsList.Tables("List")
                        '    '    End If
                        '    Case Else
                        '        dtTable = dsList.Tables("List")
                        'End Select
                        If UserAccessLevel._AccessLevelClassGroupFilterString.Trim.Length > 0 Then
                            dtTable = New DataView(dsList.Tables("List"), UserAccessLevel._AccessLevelClassGroupFilterString.Replace("hremployee_master.", ""), "", DataViewRowState.CurrentRows).ToTable
                        Else
                            dtTable = dsList.Tables("List")
                        End If
                        'Sohail (31 May 2012) -- End

                        objClassGrp = Nothing

                    Case radClass.Name.ToUpper
                        Dim objClass As New clsClass
                        dsList = objClass.GetList("List", True)
                        dsList.Tables(0).Columns.Add("IsCheck", System.Type.GetType("System.Boolean")).DefaultValue = False

                        For i As Integer = 0 To dsList.Tables("List").Rows.Count - 1
                            dsList.Tables("List").Rows(i)("IsCheck") = False
                        Next
                        dsList.Tables("List").AcceptChanges()

                        objdgcolhCheck.DataPropertyName = "IsCheck"
                        objdgcolhReportById.DataPropertyName = "classesunkid"
                        dgcolhReportBy.DataPropertyName = "name"

                        'Sohail (31 May 2012) -- Start
                        'TRA - ENHANCEMENT
                        'Select Case ConfigParameter._Object._UserAccessModeSetting
                        '    'Case enAllocation.CLASS
                        '    '    If UserAccessLevel._AccessLevel.Length > 0 Then
                        '    '        dtTable = New DataView(dsList.Tables("List"), "classesunkid IN (" & UserAccessLevel._AccessLevel & ")", "", DataViewRowState.CurrentRows).ToTable
                        '    '    Else
                        '    '        dtTable = dsList.Tables("List")
                        '    '    End If
                        '    Case Else
                        '        dtTable = dsList.Tables("List")
                        'End Select
                        If UserAccessLevel._AccessLevelClassFilterString.Trim.Length > 0 Then
                            Dim StrFilter As String = UserAccessLevel._AccessLevelClassFilterString.Replace("hremployee_master.", "")
                            StrFilter = StrFilter.Replace("classunkid", "classesunkid")
                            dtTable = New DataView(dsList.Tables("List"), StrFilter, "", DataViewRowState.CurrentRows).ToTable
                        Else
                            dtTable = dsList.Tables("List")
                        End If
                        'Sohail (31 May 2012) -- End

                        objClass = Nothing

                    Case radGradeGrp.Name.ToUpper
                        Dim objGradeGrp As New clsGradeGroup
                        dsList = objGradeGrp.GetList("List", True)
                        dsList.Tables(0).Columns.Add("IsCheck", System.Type.GetType("System.Boolean")).DefaultValue = False

                        For i As Integer = 0 To dsList.Tables("List").Rows.Count - 1
                            dsList.Tables("List").Rows(i)("IsCheck") = False
                        Next
                        dsList.Tables("List").AcceptChanges()

                        objdgcolhCheck.DataPropertyName = "IsCheck"
                        objdgcolhReportById.DataPropertyName = "gradegroupunkid"
                        dgcolhReportBy.DataPropertyName = "name"

                        'Sohail (31 May 2012) -- Start
                        'TRA - ENHANCEMENT
                        'Select Case ConfigParameter._Object._UserAccessModeSetting
                        '    'Case enAllocation.GRADE_GROUP
                        '    '    If UserAccessLevel._AccessLevel.Length > 0 Then
                        '    '        dtTable = New DataView(dsList.Tables("List"), "gradegroupunkid IN (" & UserAccessLevel._AccessLevel & ")", "", DataViewRowState.CurrentRows).ToTable
                        '    '    Else
                        '    '        dtTable = dsList.Tables("List")
                        '    '    End If
                        '    Case Else
                        '        dtTable = dsList.Tables("List")
                        'End Select
                        dtTable = dsList.Tables("List")
                        'Sohail (31 May 2012) -- End

                        objGradeGrp = Nothing

                    Case radGrade.Name.ToUpper
                        Dim objGrade As New clsGrade
                        dsList = objGrade.GetList("List", True)
                        dsList.Tables(0).Columns.Add("IsCheck", System.Type.GetType("System.Boolean")).DefaultValue = False

                        For i As Integer = 0 To dsList.Tables("List").Rows.Count - 1
                            dsList.Tables("List").Rows(i)("IsCheck") = False
                        Next
                        dsList.Tables("List").AcceptChanges()

                        objdgcolhCheck.DataPropertyName = "IsCheck"
                        objdgcolhReportById.DataPropertyName = "gradeunkid"
                        dgcolhReportBy.DataPropertyName = "name"

                        'Sohail (31 May 2012) -- Start
                        'TRA - ENHANCEMENT
                        'Select Case ConfigParameter._Object._UserAccessModeSetting
                        '    'Case enAllocation.GRADE
                        '    '    If UserAccessLevel._AccessLevel.Length > 0 Then
                        '    '        dtTable = New DataView(dsList.Tables("List"), "gradeunkid IN (" & UserAccessLevel._AccessLevel & ")", "", DataViewRowState.CurrentRows).ToTable
                        '    '    Else
                        '    '        dtTable = dsList.Tables("List")
                        '    '    End If
                        '    Case Else
                        '        dtTable = dsList.Tables("List")
                        'End Select
                        dtTable = dsList.Tables("List")
                        'Sohail (31 May 2012) -- End

                        objGrade = Nothing

                    Case radGradeLevel.Name.ToUpper
                        Dim objGradeLevel As New clsGradeLevel
                        dsList = objGradeLevel.GetList("List", True)
                        dsList.Tables(0).Columns.Add("IsCheck", System.Type.GetType("System.Boolean")).DefaultValue = False

                        For i As Integer = 0 To dsList.Tables("List").Rows.Count - 1
                            dsList.Tables("List").Rows(i)("IsCheck") = False
                        Next
                        dsList.Tables("List").AcceptChanges()

                        objdgcolhCheck.DataPropertyName = "IsCheck"
                        objdgcolhReportById.DataPropertyName = "gradelevelunkid"
                        dgcolhReportBy.DataPropertyName = "name"

                        'Sohail (31 May 2012) -- Start
                        'TRA - ENHANCEMENT
                        'Select Case ConfigParameter._Object._UserAccessModeSetting
                        '    'Case enAllocation.GRADE_LEVEL
                        '    '    If UserAccessLevel._AccessLevel.Length > 0 Then
                        '    '        dtTable = New DataView(dsList.Tables("List"), "gradelevelunkid IN (" & UserAccessLevel._AccessLevel & ")", "", DataViewRowState.CurrentRows).ToTable
                        '    '    Else
                        '    '        dtTable = dsList.Tables("List")
                        '    '    End If
                        '    Case Else
                        '        dtTable = dsList.Tables("List")
                        'End Select
                        dtTable = dsList.Tables("List")
                        'Sohail (31 May 2012) -- End

                        objGradeLevel = Nothing
                        'Sohail (16 May 2012) -- End

                End Select

                dgvData.DataSource = dtTable
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Check Box Events "

    Private Sub chkSelectallList_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkSelectallList.CheckedChanged
        Try
            RemoveHandler dgvData.CellContentClick, AddressOf dgvData_CellContentClick

            'Pinkal (12-Oct-2011) -- Start
            'If dsList Is Nothing Or dsList.Tables.Count <= 0 Then Exit Sub
            If dtTable Is Nothing Or dtTable.Rows.Count <= 0 Then Exit Sub
            'Pinkal (12-Oct-2011) -- End

            For Each dRow As DataRow In dtTable.Rows
                dRow.Item("IsCheck") = CBool(chkSelectallList.CheckState)
            Next
            AddHandler dgvData.CellContentClick, AddressOf dgvData_CellContentClick
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "chkSelectallList_CheckedChanged", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Button's Events "

    Private Sub btnSet_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSet.Click
        Try
            If dtTable.Rows.Count > 0 Then
                Dim dtTemp() As DataRow = dtTable.Select("IsCheck=true")
                If dtTemp.Length <= 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please check atleast one report by"), enMsgBoxStyle.Information)
                    Exit Sub
                End If
            Else
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 18, "Please select any Analysis by"), enMsgBoxStyle.Information)
                Exit Sub
            End If

            'Sohail (02 Sep 2016) -- Start
            'Enhancement -  64.1 - New Budget Redesign for Marie Stopes (New JV Report Sun Account Project JV).
            'Dim strAllocationQry, strJobQry, strCCQry, strSalQry As String
            'strAllocationQry = "" : strJobQry = "" : strCCQry = "" : strSalQry = ""

            'strAllocationQry = "SELECT " & _
            '                   "     stationunkid " & _
            '                   "    ,deptgroupunkid " & _
            '                   "    ,departmentunkid " & _
            '                   "    ,sectiongroupunkid " & _
            '                   "    ,sectionunkid " & _
            '                   "    ,unitgroupunkid " & _
            '                   "    ,unitunkid " & _
            '                   "    ,teamunkid " & _
            '                   "    ,classgroupunkid " & _
            '                   "    ,classunkid " & _
            '                   "    ,employeeunkid " & _
            '                   "FROM " & _
            '                   "( " & _
            '                   "        SELECT " & _
            '                   "             stationunkid " & _
            '                   "            ,deptgroupunkid " & _
            '                   "            ,departmentunkid " & _
            '                   "            ,sectiongroupunkid " & _
            '                   "            ,sectionunkid " & _
            '                   "            ,unitgroupunkid " & _
            '                   "            ,unitunkid " & _
            '                   "            ,teamunkid " & _
            '                   "            ,classgroupunkid " & _
            '                   "            ,classunkid " & _
            '                   "            ,employeeunkid " & _
            '                   "            ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
            '                   "        FROM hremployee_transfer_tran WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(mdtEffectiveDate) & "' " & _
            '                   ") AS A WHERE A.rno = 1 "

            'strJobQry = "SELECT " & _
            '            "    jobgroupunkid " & _
            '            "   ,jobunkid " & _
            '            "   ,employeeunkid " & _
            '            "FROM " & _
            '            "( " & _
            '            "   SELECT " & _
            '            "        jobgroupunkid " & _
            '            "       ,jobunkid " & _
            '            "       ,employeeunkid " & _
            '            "       ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
            '            "   FROM hremployee_categorization_tran WHERE isvoid = 0 " & _
            '            "       AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(mdtEffectiveDate) & "' " & _
            '            ") AS A WHERE A.rno = 1 "

            'strCCQry = "SELECT " & _
            '           "     costcenterunkid " & _
            '           "    ,employeeunkid " & _
            '           "FROM " & _
            '           "( " & _
            '           "    SELECT " & _
            '           "         cctranheadvalueid AS costcenterunkid " & _
            '           "        ,employeeunkid " & _
            '           "        ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
            '           "    FROM hremployee_cctranhead_tran WHERE isvoid = 0 AND istransactionhead = 0 " & _
            '           "        AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(mdtEffectiveDate) & "' " & _
            '           ") AS A WHERE A.rno = 1 "

            'strSalQry = "SELECT " & _
            '            "    gradegroupunkid " & _
            '            "   ,gradeunkid " & _
            '            "   ,gradelevelunkid " & _
            '            "   ,employeeunkid " & _
            '            "FROM " & _
            '            "( " & _
            '            "   SELECT " & _
            '            "        gradegroupunkid " & _
            '            "       ,gradeunkid " & _
            '            "       ,gradelevelunkid " & _
            '            "       ,employeeunkid " & _
            '            "       ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY incrementdate DESC,salaryincrementtranunkid DESC) AS rno " & _
            '            "   FROM prsalaryincrement_tran WHERE isvoid = 0 AND isapproved = 1 " & _
            '            "       AND CONVERT(CHAR(8),incrementdate,112) <= '" & eZeeDate.convertDate(mdtEffectiveDate) & "' " & _
            '            ") AS A WHERE A.rno = 1 "
            'Sohail (02 Sep 2016) -- End

            For Each dgvRows As DataGridViewRow In dgvData.Rows
                If CBool(dgvRows.Cells(objdgcolhCheck.Index).Value) = True Then
                    mstrReportBy_Ids &= "," & dgvRows.Cells(objdgcolhReportById.Index).Value.ToString
                    mstrReportBy_Name &= "," & dgvRows.Cells(dgcolhReportBy.Index).Value.ToString
                End If
            Next

            If mstrReportBy_Ids.Length > 0 Then mstrReportBy_Ids = Mid(mstrReportBy_Ids, 2)
            If mstrReportBy_Name.Length > 0 Then mstrReportBy_Name = Mid(mstrReportBy_Name, 2)

            'Sohail (02 Sep 2016) -- Start
            'Enhancement -  64.1 - New Budget Redesign for Marie Stopes (New JV Report Sun Account Project JV).
            'If radBranch.Checked = True Then

            '    'Shani [ 10 OCT 2014 ] -- START
            '    'Enhancement - Assign allcation tags for same allcation Enums to all reports (assign by Sandeepsir)
            '    'mintViewIndex = enAnalysisReport.Branch
            '    mintViewIndex = CInt(IIf(radBranch.Tag IsNot Nothing, radBranch.Tag, enAnalysisReport.Branch))
            '    'Shani [ 10 OCT 2014 ] -- END

            '    'Sohail (26 Nov 2011) -- Start
            '    mstrAnalysis_Fields = ", hrstation_master.stationunkid AS Id, hrstation_master.name AS GName "

            '    'S.SANDEEP [06 MAY 2015] -- START
            '    'mstrAnalysis_Join = " JOIN hrstation_master ON hrstation_master.stationunkid = " & mstrHr_EmployeeTable_Alias & ".stationunkid AND hrstation_master.stationunkid IN ( " & mstrReportBy_Ids & " ) "
            '    mstrAnalysis_Join = "JOIN " & _
            '                        "(" & vbCrLf & _
            '                            strAllocationQry & vbCrLf & _
            '                        ") AS VB_TRF ON VB_TRF.employeeunkid = " & mstrHr_EmployeeTable_Alias & ".employeeunkid " & _
            '                        " JOIN hrstation_master ON hrstation_master.stationunkid = VB_TRF.stationunkid AND hrstation_master.stationunkid IN ( " & mstrReportBy_Ids & " ) "
            '    'S.SANDEEP [06 MAY 2015] -- END

            '    mstrAnalysis_OrderBy = " hrstation_master.stationunkid "
            '    mstrAnalysis_OrderBy_GroupName = " hrstation_master.name " 'Sohail (24 Jan 2013)
            '    mstrReport_GroupName = Language.getMessage(mstrModuleName, 2, "Branch :")
            '    'Sohail (26 Nov 2011) -- End
            '    mstrAnalysis_TableName = " hrstation_master " 'Sohail (12 May 2012)
            'ElseIf radDepartment.Checked = True Then
            '    'Shani [ 10 OCT 2014 ] -- START
            '    'Enhancement - Assign allcation tags for same allcation Enums to all reports (assign by Sandeepsir)
            '    'mintViewIndex = enAnalysisReport.Department
            '    mintViewIndex = CInt(IIf(radDepartment.Tag IsNot Nothing, radDepartment.Tag, enAnalysisReport.Department))
            '    'Shani [ 10 OCT 2014 ] -- END

            '    'Sohail (26 Nov 2011) -- Start
            '    mstrAnalysis_Fields = ", hrdepartment_master.departmentunkid AS Id, hrdepartment_master.name AS GName "

            '    'S.SANDEEP [06 MAY 2015] -- START
            '    'mstrAnalysis_Join = " JOIN hrdepartment_master ON hrdepartment_master.departmentunkid = " & mstrHr_EmployeeTable_Alias & ".departmentunkid AND hrdepartment_master.departmentunkid IN ( " & mstrReportBy_Ids & " ) "
            '    mstrAnalysis_Join = "JOIN " & _
            '                        "(" & vbCrLf & _
            '                            strAllocationQry & vbCrLf & _
            '                        ") AS VB_TRF ON VB_TRF.employeeunkid = " & mstrHr_EmployeeTable_Alias & ".employeeunkid " & _
            '                        " JOIN hrdepartment_master ON hrdepartment_master.departmentunkid = VB_TRF.departmentunkid AND hrdepartment_master.departmentunkid IN ( " & mstrReportBy_Ids & " ) "
            '    'S.SANDEEP [06 MAY 2015] -- END

            '    mstrAnalysis_OrderBy = " hrdepartment_master.departmentunkid "
            '    mstrAnalysis_OrderBy_GroupName = " hrdepartment_master.name " 'Sohail (24 Jan 2013)
            '    mstrReport_GroupName = Language.getMessage(mstrModuleName, 3, "Department :")
            '    'Sohail (26 Nov 2011) -- End
            '    mstrAnalysis_TableName = " hrdepartment_master " 'Sohail (12 May 2012)
            'ElseIf radJob.Checked = True Then
            '    'Shani [ 10 OCT 2014 ] -- START
            '    'Enhancement - Assign allcation tags for same allcation Enums to all reports (assign by Sandeepsir)
            '    'mintViewIndex = enAnalysisReport.Job
            '    mintViewIndex = CInt(IIf(radJob.Tag IsNot Nothing, radJob.Tag, enAnalysisReport.Job))
            '    'Shani [ 10 OCT 2014 ] -- END

            '    'Sohail (26 Nov 2011) -- Start
            '    mstrAnalysis_Fields = ",  hrjob_master.jobunkid AS Id, hrjob_master.job_name AS GName "

            '    'S.SANDEEP [06 MAY 2015] -- START
            '    'mstrAnalysis_Join = " JOIN hrjob_master ON hrjob_master.jobunkid = " & mstrHr_EmployeeTable_Alias & ".jobunkid AND hrjob_master.jobunkid IN ( " & mstrReportBy_Ids & " ) "

            '    mstrAnalysis_Join = "JOIN " & _
            '                        "(" & vbCrLf & _
            '                            strJobQry & vbCrLf & _
            '                        ") AS VB_RECAT ON VB_RECAT.employeeunkid = " & mstrHr_EmployeeTable_Alias & ".employeeunkid " & _
            '                        " JOIN hrjob_master ON hrjob_master.jobunkid = VB_RECAT.jobunkid AND hrjob_master.jobunkid IN ( " & mstrReportBy_Ids & " ) "
            '    'S.SANDEEP [06 MAY 2015] -- END

            '    mstrAnalysis_OrderBy = " hrjob_master.jobunkid "
            '    mstrAnalysis_OrderBy_GroupName = " hrjob_master.job_name " 'Sohail (24 Jan 2013)
            '    mstrReport_GroupName = Language.getMessage(mstrModuleName, 4, "Job :")
            '    'Sohail (26 Nov 2011) -- End
            '    mstrAnalysis_TableName = " hrjob_master " 'Sohail (12 May 2012)
            'ElseIf radSection.Checked = True Then
            '    'Shani [ 10 OCT 2014 ] -- START
            '    'Enhancement - Assign allcation tags for same allcation Enums to all reports (assign by Sandeepsir)
            '    'mintViewIndex = enAnalysisReport.Section
            '    mintViewIndex = CInt(IIf(radSection.Tag IsNot Nothing, radSection.Tag, enAnalysisReport.Section))
            '    'Shani [ 10 OCT 2014 ] -- END

            '    'Sohail (26 Nov 2011) -- Start
            '    mstrAnalysis_Fields = ",  hrsection_master.sectionunkid AS Id, hrsection_master.name AS GName "

            '    'S.SANDEEP [06 MAY 2015] -- START
            '    'mstrAnalysis_Join = " JOIN hrsection_master ON hrsection_master.sectionunkid = " & mstrHr_EmployeeTable_Alias & ".sectionunkid AND hrsection_master.sectionunkid IN ( " & mstrReportBy_Ids & " ) "

            '    mstrAnalysis_Join = "JOIN " & _
            '                        "(" & vbCrLf & _
            '                            strAllocationQry & vbCrLf & _
            '                        ") AS VB_TRF ON VB_TRF.employeeunkid = " & mstrHr_EmployeeTable_Alias & ".employeeunkid " & _
            '                        " JOIN hrsection_master ON hrsection_master.sectionunkid = VB_TRF.sectionunkid AND hrsection_master.sectionunkid IN ( " & mstrReportBy_Ids & " ) "
            '    'S.SANDEEP [06 MAY 2015] -- END

            '    mstrAnalysis_OrderBy = " hrsection_master.sectionunkid "
            '    mstrAnalysis_OrderBy_GroupName = " hrsection_master.name " 'Sohail (24 Jan 2013)
            '    mstrReport_GroupName = Language.getMessage(mstrModuleName, 5, "Section :")
            '    'Sohail (26 Nov 2011) -- End
            '    mstrAnalysis_TableName = " hrsection_master " 'Sohail (12 May 2012)
            'ElseIf radUnit.Checked = True Then
            '    'Shani [ 10 OCT 2014 ] -- START
            '    'Enhancement - Assign allcation tags for same allcation Enums to all reports (assign by Sandeepsir)
            '    'mintViewIndex = enAnalysisReport.Unit
            '    mintViewIndex = CInt(IIf(radUnit.Tag IsNot Nothing, radUnit.Tag, enAnalysisReport.Unit))
            '    'Shani [ 10 OCT 2014 ] -- END

            '    'Sohail (26 Nov 2011) -- Start
            '    mstrAnalysis_Fields = ",  hrunit_master.unitunkid AS Id, hrunit_master.name AS GName "

            '    'S.SANDEEP [06 MAY 2015] -- START
            '    'mstrAnalysis_Join = " JOIN hrunit_master ON hrunit_master.unitunkid = " & mstrHr_EmployeeTable_Alias & ".unitunkid AND hrunit_master.unitunkid IN ( " & mstrReportBy_Ids & " ) "
            '    mstrAnalysis_Join = "JOIN " & _
            '                        "(" & vbCrLf & _
            '                            strAllocationQry & vbCrLf & _
            '                        ") AS VB_TRF ON VB_TRF.employeeunkid = " & mstrHr_EmployeeTable_Alias & ".employeeunkid " & _
            '                        " JOIN hrunit_master ON hrunit_master.unitunkid = VB_TRF.unitunkid AND hrunit_master.unitunkid IN ( " & mstrReportBy_Ids & " ) "
            '    'S.SANDEEP [06 MAY 2015] -- END

            '    mstrAnalysis_OrderBy = " hrunit_master.unitunkid "
            '    mstrAnalysis_OrderBy_GroupName = " hrunit_master.name " 'Sohail (24 Jan 2013)
            '    mstrReport_GroupName = Language.getMessage(mstrModuleName, 6, "Unit :")
            '    'Sohail (26 Nov 2011) -- End
            '    mstrAnalysis_TableName = " hrunit_master " 'Sohail (12 May 2012)
            'ElseIf radCostCenter.Checked Then
            '    'Shani [ 10 OCT 2014 ] -- START
            '    'Enhancement - Assign allcation tags for same allcation Enums to all reports (assign by Sandeepsir)
            '    'mintViewIndex = enAnalysisReport.CostCenter
            '    mintViewIndex = CInt(IIf(radCostCenter.Tag IsNot Nothing, radCostCenter.Tag, enAnalysisReport.CostCenter))
            '    'Shani [ 10 OCT 2014 ] -- END

            '    'Sohail (26 Nov 2011) -- Start
            '    mstrAnalysis_Fields = ", prcostcenter_master.costcenterunkid AS Id, prcostcenter_master.costcentername AS GName "

            '    'S.SANDEEP [06 MAY 2015] -- START
            '    'mstrAnalysis_Join = " JOIN prcostcenter_master ON prcostcenter_master.costcenterunkid = " & mstrHr_EmployeeTable_Alias & ".costcenterunkid AND prcostcenter_master.costcenterunkid IN ( " & mstrReportBy_Ids & " ) "
            '    mstrAnalysis_Join = "JOIN " & _
            '                        "(" & vbCrLf & _
            '                            strCCQry & vbCrLf & _
            '                        ") AS VB_CCR ON VB_CCR.employeeunkid = " & mstrHr_EmployeeTable_Alias & ".employeeunkid " & _
            '                        " JOIN prcostcenter_master ON prcostcenter_master.costcenterunkid = VB_CCR.costcenterunkid AND prcostcenter_master.costcenterunkid IN ( " & mstrReportBy_Ids & " ) "
            '    'S.SANDEEP [06 MAY 2015] -- END
            '    mstrAnalysis_OrderBy = " prcostcenter_master.costcenterunkid "
            '    mstrAnalysis_OrderBy_GroupName = " prcostcenter_master.costcentername " 'Sohail (24 Jan 2013)
            '    mstrReport_GroupName = Language.getMessage(mstrModuleName, 7, "Cost Center :")
            '    'Sohail (26 Nov 2011) -- End
            '    mstrAnalysis_TableName = " prcostcenter_master " 'Sohail (12 May 2012)

            '    'Pinkal (12-Oct-2011) -- Start
            '    'Enhancement : TRA Changes
            'ElseIf radSectionGrp.Checked Then
            '    'Shani [ 10 OCT 2014 ] -- START
            '    'Enhancement - Assign allcation tags for same allcation Enums to all reports (assign by Sandeepsir)
            '    'mintViewIndex = enAnalysisReport.SectionGroup
            '    mintViewIndex = CInt(IIf(radSectionGrp IsNot Nothing, radSectionGrp.Tag, enAnalysisReport.SectionGroup))
            '    'Shani [ 10 OCT 2014 ] -- END

            '    'Sohail (26 Nov 2011) -- Start
            '    mstrAnalysis_Fields = ", hrsectiongroup_master.sectiongroupunkid AS Id, hrsectiongroup_master.name AS GName "

            '    'S.SANDEEP [06 MAY 2015] -- START
            '    'mstrAnalysis_Join = " JOIN hrsectiongroup_master ON hrsectiongroup_master.sectiongroupunkid = " & mstrHr_EmployeeTable_Alias & ".sectiongroupunkid AND hrsectiongroup_master.sectiongroupunkid IN ( " & mstrReportBy_Ids & " ) "
            '    mstrAnalysis_Join = "JOIN " & _
            '                        "(" & vbCrLf & _
            '                            strAllocationQry & vbCrLf & _
            '                        ") AS VB_TRF ON VB_TRF.employeeunkid = " & mstrHr_EmployeeTable_Alias & ".employeeunkid " & _
            '                        " JOIN hrsectiongroup_master ON hrsectiongroup_master.sectiongroupunkid = VB_TRF.sectiongroupunkid AND hrsectiongroup_master.sectiongroupunkid IN ( " & mstrReportBy_Ids & " ) "

            '    'S.SANDEEP [06 MAY 2015] -- END

            '    mstrAnalysis_OrderBy = " hrsectiongroup_master.sectiongroupunkid "
            '    mstrAnalysis_OrderBy_GroupName = " hrsectiongroup_master.name " 'Sohail (24 Jan 2013)
            '    mstrReport_GroupName = Language.getMessage(mstrModuleName, 8, "Section Group :")
            '    'Sohail (26 Nov 2011) -- End
            '    mstrAnalysis_TableName = " hrsectiongroup_master " 'Sohail (12 May 2012)
            'ElseIf radUnitGrp.Checked Then
            '    'Shani [ 10 OCT 2014 ] -- START
            '    'Enhancement - Assign allcation tags for same allcation Enums to all reports (assign by Sandeepsir)
            '    'mintViewIndex = enAnalysisReport.UnitGroup
            '    mintViewIndex = CInt(IIf(radUnitGrp.Tag IsNot Nothing, radUnitGrp.Tag, enAnalysisReport.UnitGroup))
            '    'Shani [ 10 OCT 2014 ] -- END

            '    'Sohail (26 Nov 2011) -- Start
            '    mstrAnalysis_Fields = ", hrunitgroup_master.unitgroupunkid AS Id, hrunitgroup_master.name AS GName "

            '    'S.SANDEEP [06 MAY 2015] -- START
            '    'mstrAnalysis_Join = " JOIN hrunitgroup_master ON hrunitgroup_master.unitgroupunkid = " & mstrHr_EmployeeTable_Alias & ".unitgroupunkid AND hrunitgroup_master.unitgroupunkid IN ( " & mstrReportBy_Ids & " ) "
            '    mstrAnalysis_Join = "JOIN " & _
            '                        "(" & vbCrLf & _
            '                            strAllocationQry & vbCrLf & _
            '                        ") AS VB_TRF ON VB_TRF.employeeunkid = " & mstrHr_EmployeeTable_Alias & ".employeeunkid " & _
            '                        " JOIN hrunitgroup_master ON hrunitgroup_master.unitgroupunkid = VB_TRF.unitgroupunkid AND hrunitgroup_master.unitgroupunkid IN ( " & mstrReportBy_Ids & " ) "
            '    'S.SANDEEP [06 MAY 2015] -- END

            '    mstrAnalysis_OrderBy = " hrunitgroup_master.unitgroupunkid "
            '    mstrAnalysis_OrderBy_GroupName = " hrunitgroup_master.name " 'Sohail (24 Jan 2013)
            '    mstrReport_GroupName = Language.getMessage(mstrModuleName, 9, "Unit Group :")
            '    'Sohail (26 Nov 2011) -- End
            '    mstrAnalysis_TableName = " hrunitgroup_master " 'Sohail (12 May 2012)
            'ElseIf radTeam.Checked Then
            '    'Shani [ 10 OCT 2014 ] -- START
            '    'Enhancement - Assign allcation tags for same allcation Enums to all reports (assign by Sandeepsir)
            '    'mintViewIndex = enAnalysisReport.Team
            '    mintViewIndex = CInt(IIf(radTeam.Tag IsNot Nothing, radTeam.Tag, enAnalysisReport.Team))
            '    'Shani [ 10 OCT 2014 ] -- END

            '    'Sohail (26 Nov 2011) -- Start
            '    mstrAnalysis_Fields = ", hrteam_master.teamunkid AS Id, hrteam_master.name AS GName "

            '    'S.SANDEEP [06 MAY 2015] -- START
            '    'mstrAnalysis_Join = " JOIN hrteam_master ON hrteam_master.teamunkid = " & mstrHr_EmployeeTable_Alias & ".teamunkid AND hrteam_master.teamunkid IN ( " & mstrReportBy_Ids & " ) "
            '    mstrAnalysis_Join = "JOIN " & _
            '                        "(" & vbCrLf & _
            '                            strAllocationQry & vbCrLf & _
            '                        ") AS VB_TRF ON VB_TRF.employeeunkid = " & mstrHr_EmployeeTable_Alias & ".employeeunkid " & _
            '                        " JOIN hrteam_master ON hrteam_master.teamunkid = VB_TRF.teamunkid AND hrteam_master.teamunkid IN ( " & mstrReportBy_Ids & " ) "
            '    'S.SANDEEP [06 MAY 2015] -- END

            '    mstrAnalysis_OrderBy = " hrteam_master.teamunkid "
            '    mstrAnalysis_OrderBy_GroupName = " hrteam_master.name " 'Sohail (24 Jan 2013)
            '    mstrReport_GroupName = Language.getMessage(mstrModuleName, 10, "Team :")
            '    mstrAnalysis_TableName = " hrteam_master " 'Sohail (12 May 2012)

            '    'Sohail (16 May 2012) -- Start
            '    'TRA - ENHANCEMENT
            'ElseIf radDepartmentGrp.Checked Then
            '    'Shani [ 10 OCT 2014 ] -- START
            '    'Enhancement - Assign allcation tags for same allcation Enums to all reports (assign by Sandeepsir)
            '    'mintViewIndex = enAnalysisReport.DepartmentGroup
            '    mintViewIndex = CInt(IIf(radDepartmentGrp.Tag IsNot Nothing, radDepartmentGrp.Tag, enAnalysisReport.DepartmentGroup))
            '    'Shani [ 10 OCT 2014 ] -- END

            '    mstrAnalysis_Fields = ", hrdepartment_group_master.deptgroupunkid AS Id, hrdepartment_group_master.name AS GName "

            '    'S.SANDEEP [06 MAY 2015] -- START
            '    'mstrAnalysis_Join = " JOIN hrdepartment_group_master ON hrdepartment_group_master.deptgroupunkid = " & mstrHr_EmployeeTable_Alias & ".deptgroupunkid AND hrdepartment_group_master.deptgroupunkid IN ( " & mstrReportBy_Ids & " ) "
            '    mstrAnalysis_Join = "JOIN " & _
            '                        "(" & vbCrLf & _
            '                            strAllocationQry & vbCrLf & _
            '                        ") AS VB_TRF ON VB_TRF.employeeunkid = " & mstrHr_EmployeeTable_Alias & ".employeeunkid " & _
            '                        " JOIN hrdepartment_group_master ON hrdepartment_group_master.deptgroupunkid = VB_TRF.deptgroupunkid AND hrdepartment_group_master.deptgroupunkid IN ( " & mstrReportBy_Ids & " ) "
            '    'S.SANDEEP [06 MAY 2015] -- END

            '    mstrAnalysis_OrderBy = " hrdepartment_group_master.deptgroupunkid "
            '    mstrAnalysis_OrderBy_GroupName = " hrdepartment_group_master.name " 'Sohail (24 Jan 2013)
            '    mstrReport_GroupName = Language.getMessage(mstrModuleName, 11, "Department Group :")
            '    mstrAnalysis_TableName = " hrdepartment_group_master "

            'ElseIf radJobGrp.Checked Then
            '    'Shani [ 10 OCT 2014 ] -- START
            '    'Enhancement - Assign allcation tags for same allcation Enums to all reports (assign by Sandeepsir)
            '    'mintViewIndex = enAnalysisReport.JobGroup
            '    mintViewIndex = CInt(IIf(radJobGrp.Tag IsNot Nothing, radJobGrp.Tag, enAnalysisReport.JobGroup))
            '    'Shani [ 10 OCT 2014 ] -- END

            '    mstrAnalysis_Fields = ", hrjobgroup_master.jobgroupunkid AS Id, hrjobgroup_master.name AS GName "

            '    'S.SANDEEP [06 MAY 2015] -- START
            '    'mstrAnalysis_Join = " JOIN hrjobgroup_master ON hrjobgroup_master.jobgroupunkid = " & mstrHr_EmployeeTable_Alias & ".jobgroupunkid AND hrjobgroup_master.jobgroupunkid IN ( " & mstrReportBy_Ids & " ) "
            '    mstrAnalysis_Join = "JOIN " & _
            '                        "(" & vbCrLf & _
            '                            strJobQry & vbCrLf & _
            '                        ") AS VB_RECAT ON VB_RECAT.employeeunkid = " & mstrHr_EmployeeTable_Alias & ".employeeunkid " & _
            '                        " JOIN hrjobgroup_master ON hrjobgroup_master.jobgroupunkid = VB_RECAT.jobgroupunkid AND hrjobgroup_master.jobgroupunkid IN ( " & mstrReportBy_Ids & " ) "
            '    'S.SANDEEP [06 MAY 2015] -- END

            '    mstrAnalysis_OrderBy = " hrjobgroup_master.jobgroupunkid "
            '    mstrAnalysis_OrderBy_GroupName = " hrjobgroup_master.name " 'Sohail (24 Jan 2013)
            '    mstrReport_GroupName = Language.getMessage(mstrModuleName, 12, "Job Group :")
            '    mstrAnalysis_TableName = " hrjobgroup_master "

            'ElseIf radClassGrp.Checked Then
            '    'Shani [ 10 OCT 2014 ] -- START
            '    'Enhancement - Assign allcation tags for same allcation Enums to all reports (assign by Sandeepsir)
            '    ''mintViewIndex = enAnalysisReport.ClassGroup
            '    mintViewIndex = CInt(IIf(radClassGrp.Tag IsNot Nothing, radClassGrp.Tag, enAnalysisReport.ClassGroup))
            '    'Shani [ 10 OCT 2014 ] -- END

            '    mstrAnalysis_Fields = ", hrclassgroup_master.classgroupunkid AS Id, hrclassgroup_master.name AS GName "

            '    'S.SANDEEP [06 MAY 2015] -- START
            '    'mstrAnalysis_Join = " JOIN hrclassgroup_master ON hrclassgroup_master.classgroupunkid = " & mstrHr_EmployeeTable_Alias & ".classgroupunkid AND hrclassgroup_master.classgroupunkid IN ( " & mstrReportBy_Ids & " ) "
            '    mstrAnalysis_Join = "JOIN " & _
            '                        "(" & vbCrLf & _
            '                            strAllocationQry & vbCrLf & _
            '                        ") AS VB_TRF ON VB_TRF.employeeunkid = " & mstrHr_EmployeeTable_Alias & ".employeeunkid " & _
            '                        " JOIN hrclassgroup_master ON hrclassgroup_master.classgroupunkid = VB_TRF.classgroupunkid AND hrclassgroup_master.classgroupunkid IN ( " & mstrReportBy_Ids & " ) "
            '    'S.SANDEEP [06 MAY 2015] -- END

            '    mstrAnalysis_OrderBy = " hrclassgroup_master.classgroupunkid "
            '    mstrAnalysis_OrderBy_GroupName = " hrclassgroup_master.name " 'Sohail (24 Jan 2013)
            '    mstrReport_GroupName = Language.getMessage(mstrModuleName, 13, "Class Group :")
            '    mstrAnalysis_TableName = " hrclassgroup_master "

            'ElseIf radClass.Checked Then
            '    'Shani [ 10 OCT 2014 ] -- START
            '    'Enhancement - Assign allcation tags for same allcation Enums to all reports (assign by Sandeepsir)
            '    ''mintViewIndex = enAnalysisReport.Classs
            '    mintViewIndex = CInt(IIf(radClass.Tag IsNot Nothing, radClass.Tag, enAnalysisReport.Classs))
            '    'Shani [ 10 OCT 2014 ] -- END

            '    mstrAnalysis_Fields = ", hrclasses_master.classesunkid AS Id, hrclasses_master.name AS GName "

            '    'S.SANDEEP [06 MAY 2015] -- START
            '    'mstrAnalysis_Join = " JOIN hrclasses_master ON hrclasses_master.classesunkid = " & mstrHr_EmployeeTable_Alias & ".classunkid AND hrclasses_master.classesunkid IN ( " & mstrReportBy_Ids & " ) "
            '    mstrAnalysis_Join = "JOIN " & _
            '                        "(" & vbCrLf & _
            '                            strAllocationQry & vbCrLf & _
            '                        ") AS VB_TRF ON VB_TRF.employeeunkid = " & mstrHr_EmployeeTable_Alias & ".employeeunkid " & _
            '                        " JOIN hrclasses_master ON hrclasses_master.classesunkid = VB_TRF.classunkid AND hrclasses_master.classesunkid IN ( " & mstrReportBy_Ids & " ) "
            '    'S.SANDEEP [06 MAY 2015] -- END

            '    mstrAnalysis_OrderBy = " hrclasses_master.classesunkid "
            '    mstrAnalysis_OrderBy_GroupName = " hrclasses_master.name " 'Sohail (24 Jan 2013)
            '    mstrReport_GroupName = Language.getMessage(mstrModuleName, 14, "Class :")
            '    mstrAnalysis_TableName = " hrclasses_master "

            'ElseIf radGradeGrp.Checked Then
            '    mintViewIndex = enAnalysisReport.GradeGroup
            '    mstrAnalysis_Fields = ", hrgradegroup_master.gradegroupunkid AS Id, hrgradegroup_master.name AS GName "

            '    'S.SANDEEP [06 MAY 2015] -- START
            '    'mstrAnalysis_Join = " JOIN hrgradegroup_master ON hrgradegroup_master.gradegroupunkid = " & mstrHr_EmployeeTable_Alias & ".gradegroupunkid AND hrgradegroup_master.gradegroupunkid IN ( " & mstrReportBy_Ids & " ) "
            '    mstrAnalysis_Join = "JOIN " & _
            '                        "(" & vbCrLf & _
            '                            strSalQry & vbCrLf & _
            '                        ") AS VB_SALI ON VB_SALI.employeeunkid = " & mstrHr_EmployeeTable_Alias & ".employeeunkid " & _
            '                        " JOIN hrgradegroup_master ON hrgradegroup_master.gradegroupunkid = VB_SALI.gradegroupunkid AND hrgradegroup_master.gradegroupunkid IN ( " & mstrReportBy_Ids & " ) "
            '    'S.SANDEEP [06 MAY 2015] -- END

            '    mstrAnalysis_OrderBy = " hrgradegroup_master.gradegroupunkid "
            '    mstrAnalysis_OrderBy_GroupName = " hrgradegroup_master.name " 'Sohail (24 Jan 2013)
            '    mstrReport_GroupName = Language.getMessage(mstrModuleName, 15, "Grade Group :")
            '    mstrAnalysis_TableName = " hrgradegroup_master "

            'ElseIf radGrade.Checked Then
            '    mintViewIndex = enAnalysisReport.Grade
            '    mstrAnalysis_Fields = ", hrgrade_master.gradeunkid AS Id, hrgrade_master.name AS GName "

            '    'S.SANDEEP [06 MAY 2015] -- START
            '    'mstrAnalysis_Join = " JOIN hrgrade_master ON hrgrade_master.gradeunkid = " & mstrHr_EmployeeTable_Alias & ".gradeunkid AND hrgrade_master.gradeunkid IN ( " & mstrReportBy_Ids & " ) "
            '    mstrAnalysis_Join = "JOIN " & _
            '                        "(" & vbCrLf & _
            '                            strSalQry & vbCrLf & _
            '                        ") AS VB_SALI ON VB_SALI.employeeunkid = " & mstrHr_EmployeeTable_Alias & ".employeeunkid " & _
            '                        " JOIN hrgrade_master ON hrgrade_master.gradeunkid = VB_SALI.gradeunkid AND hrgrade_master.gradeunkid IN ( " & mstrReportBy_Ids & " ) "
            '    'S.SANDEEP [06 MAY 2015] -- END

            '    mstrAnalysis_OrderBy = " hrgrade_master.gradeunkid "
            '    mstrAnalysis_OrderBy_GroupName = " hrgrade_master.name " 'Sohail (24 Jan 2013)
            '    mstrReport_GroupName = Language.getMessage(mstrModuleName, 16, "Grade :")
            '    mstrAnalysis_TableName = " hrgrade_master "

            'ElseIf radGradeLevel.Checked Then
            '    mintViewIndex = enAnalysisReport.GradeLevel
            '    mstrAnalysis_Fields = ", hrgradelevel_master.gradelevelunkid AS Id, hrgradelevel_master.name AS GName "

            '    'S.SANDEEP [06 MAY 2015] -- START
            '    'mstrAnalysis_Join = " JOIN hrgradelevel_master ON hrgradelevel_master.gradelevelunkid = " & mstrHr_EmployeeTable_Alias & ".gradelevelunkid AND hrgradelevel_master.gradelevelunkid IN ( " & mstrReportBy_Ids & " ) "
            '    mstrAnalysis_Join = "JOIN " & _
            '                        "(" & vbCrLf & _
            '                            strSalQry & vbCrLf & _
            '                        ") AS VB_SALI ON VB_SALI.employeeunkid = " & mstrHr_EmployeeTable_Alias & ".employeeunkid " & _
            '                        " JOIN hrgradelevel_master ON hrgradelevel_master.gradelevelunkid = VB_SALI.gradelevelunkid AND hrgradelevel_master.gradelevelunkid IN ( " & mstrReportBy_Ids & " ) "
            '    'S.SANDEEP [06 MAY 2015] -- END

            '    mstrAnalysis_OrderBy = " hrgradelevel_master.gradelevelunkid "
            '    mstrAnalysis_OrderBy_GroupName = " hrgradelevel_master.name " 'Sohail (24 Jan 2013)
            '    mstrReport_GroupName = Language.getMessage(mstrModuleName, 17, "Grade Level :")
            '    mstrAnalysis_TableName = " hrgradelevel_master "
            '    'Sohail (16 May 2012) -- End
            'Else
            '    'Sohail (16 May 2012) -- Start
            '    'TRA - ENHANCEMENT
            '    'mstrAnalysis_Fields = ""
            '    mstrAnalysis_Fields = ", 0 AS Id, '' AS GName "
            '    'Sohail (16 May 2012) -- End
            '    mstrAnalysis_Join = ""
            '    mstrAnalysis_OrderBy = ""
            '    mstrAnalysis_OrderBy_GroupName = "" 'Sohail (24 Jan 2013)
            '    mstrHr_EmployeeTable_Alias = ""
            '    mstrReport_GroupName = ""
            '    'Sohail (26 Nov 2011) -- End
            '    'Pinkal (12-Oct-2011) -- End
            '    mstrAnalysis_TableName = " " 'Sohail (12 May 2012)
            'End If
            If radBranch.Checked = True Then
                mintViewIndex = CInt(IIf(radBranch.Tag IsNot Nothing, radBranch.Tag, enAnalysisReport.Branch))

            ElseIf radDepartment.Checked = True Then
                mintViewIndex = CInt(IIf(radDepartment.Tag IsNot Nothing, radDepartment.Tag, enAnalysisReport.Department))

            ElseIf radJob.Checked = True Then
                mintViewIndex = CInt(IIf(radJob.Tag IsNot Nothing, radJob.Tag, enAnalysisReport.Job))

            ElseIf radSection.Checked = True Then
                mintViewIndex = CInt(IIf(radSection.Tag IsNot Nothing, radSection.Tag, enAnalysisReport.Section))

            ElseIf radUnit.Checked = True Then
                mintViewIndex = CInt(IIf(radUnit.Tag IsNot Nothing, radUnit.Tag, enAnalysisReport.Unit))

            ElseIf radCostCenter.Checked Then
                mintViewIndex = CInt(IIf(radCostCenter.Tag IsNot Nothing, radCostCenter.Tag, enAnalysisReport.CostCenter))

            ElseIf radSectionGrp.Checked Then
                mintViewIndex = CInt(IIf(radSectionGrp IsNot Nothing, radSectionGrp.Tag, enAnalysisReport.SectionGroup))

            ElseIf radUnitGrp.Checked Then
                mintViewIndex = CInt(IIf(radUnitGrp.Tag IsNot Nothing, radUnitGrp.Tag, enAnalysisReport.UnitGroup))

            ElseIf radTeam.Checked Then
                mintViewIndex = CInt(IIf(radTeam.Tag IsNot Nothing, radTeam.Tag, enAnalysisReport.Team))

            ElseIf radDepartmentGrp.Checked Then
                mintViewIndex = CInt(IIf(radDepartmentGrp.Tag IsNot Nothing, radDepartmentGrp.Tag, enAnalysisReport.DepartmentGroup))

            ElseIf radJobGrp.Checked Then
                mintViewIndex = CInt(IIf(radJobGrp.Tag IsNot Nothing, radJobGrp.Tag, enAnalysisReport.JobGroup))

            ElseIf radClassGrp.Checked Then
                mintViewIndex = CInt(IIf(radClassGrp.Tag IsNot Nothing, radClassGrp.Tag, enAnalysisReport.ClassGroup))

            ElseIf radClass.Checked Then
                mintViewIndex = CInt(IIf(radClass.Tag IsNot Nothing, radClass.Tag, enAnalysisReport.Classs))

            ElseIf radGradeGrp.Checked Then
                mintViewIndex = enAnalysisReport.GradeGroup

            ElseIf radGrade.Checked Then
                mintViewIndex = enAnalysisReport.Grade

            ElseIf radGradeLevel.Checked Then
                mintViewIndex = enAnalysisReport.GradeLevel

            Else
                mintViewIndex = 0
            End If

            Call GetAnalysisByDetails(mstrModuleName _
                                      , mintViewIndex _
                                      , mstrReportBy_Ids _
                                      , mdtEffectiveDate _
                                      , mstrHr_EmployeeTable_Alias _
                                      , mstrAnalysis_Fields _
                                      , mstrAnalysis_Join _
                                      , mstrAnalysis_OrderBy _
                                      , mstrAnalysis_OrderBy_GroupName _
                                      , mstrReport_GroupName _
                                      , mstrAnalysis_TableName _
                                      , mstrAnalysis_CodeField _
                                      , mstrDBName _
                                      )
            'Sohail (22 Jan 2021) - [mstrDBName]
            'Sohail (01 Mar 2017) - [mstrAnalysis_CodeField]
            'Sohail (02 Sep 2016) -- End



            mblnCancel = False

            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnSet_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnCancel_Click", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " DataGrid Events "

    Private Sub dgvData_CellContentClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvData.CellContentClick, dgvData.CellContentDoubleClick
        Try
            If e.RowIndex = -1 Then Exit Sub
            RemoveHandler chkSelectallList.CheckedChanged, AddressOf chkSelectallList_CheckedChanged
            If e.ColumnIndex = objdgcolhCheck.Index Then
                dtTable.Rows(e.RowIndex)(objdgcolhCheck.DataPropertyName) = Not CBool(dtTable.Rows(e.RowIndex)(objdgcolhCheck.DataPropertyName))
                dgvData.RefreshEdit()
                Dim drRow As DataRow() = dtTable.Select("IsCheck = true", "")
                If drRow.Length > 0 Then
                    If dtTable.Rows.Count = drRow.Length Then
                        chkSelectallList.CheckState = CheckState.Checked
                    Else
                        chkSelectallList.CheckState = CheckState.Indeterminate
                    End If
                Else
                    chkSelectallList.CheckState = CheckState.Unchecked
                End If
            End If
            AddHandler chkSelectallList.CheckedChanged, AddressOf chkSelectallList_CheckedChanged
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgvData_CellContentClick", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

    'Shani [ 10 OCT 2014 ] -- START
    'Enhancement - Assign allcation tags for same allcation Enums to all reports (assign by Sandeepsir)
#Region "Privae Methods"
    Public Sub Assign_Tags()
        Try
            For Each ctrl As Control In pnlAnalysis.Controls
                If TypeOf ctrl Is System.Windows.Forms.RadioButton Then
                    Select Case CType(ctrl, System.Windows.Forms.RadioButton).Name.ToUpper
                        'Sohail (02 Sep 2016) -- Start
                        'Enhancement -  64.1 - New Budget Redesign for Marie Stopes (New JV Report Sun Account Project JV).
                        'Case radBranch.Name.ToUpper
                        '    radBranch.Tag = enAllocation.BRANCH
                        'Case radDepartmentGrp.Name.ToUpper
                        '    radDepartmentGrp.Tag = enAllocation.DEPARTMENT_GROUP
                        'Case radDepartment.Name.ToUpper
                        '    radDepartment.Tag = enAllocation.DEPARTMENT
                        'Case radSectionGrp.Name.ToUpper
                        '    radSectionGrp.Tag = enAllocation.SECTION_GROUP
                        'Case radSection.Name.ToUpper
                        '    radSection.Tag = enAllocation.SECTION
                        'Case radUnitGrp.Name.ToUpper
                        '    radUnitGrp.Tag = enAllocation.UNIT_GROUP
                        'Case radUnit.Name.ToUpper
                        '    radUnit.Tag = enAllocation.UNIT
                        'Case radTeam.Name.ToUpper
                        '    radTeam.Tag = enAllocation.TEAM
                        'Case radJobGrp.Name.ToUpper
                        '    radJobGrp.Tag = enAllocation.JOB_GROUP
                        'Case radJob.Name.ToUpper
                        '    radJob.Tag = enAllocation.JOBS
                        'Case radClassGrp.Name.ToUpper
                        '    radClassGrp.Tag = enAllocation.CLASS_GROUP
                        'Case radClass.Name.ToUpper
                        '    radClass.Tag = enAllocation.CLASSES
                        'Case radCostCenter.Name.ToUpper
                        '    radCostCenter.Tag = enAllocation.COST_CENTER
                        Case radBranch.Name.ToUpper
                            radBranch.Tag = enAnalysisReport.Branch
                        Case radDepartmentGrp.Name.ToUpper
                            radDepartmentGrp.Tag = enAnalysisReport.DepartmentGroup
                        Case radDepartment.Name.ToUpper
                            radDepartment.Tag = enAnalysisReport.Department
                        Case radSectionGrp.Name.ToUpper
                            radSectionGrp.Tag = enAnalysisReport.SectionGroup
                        Case radSection.Name.ToUpper
                            radSection.Tag = enAnalysisReport.Section
                        Case radUnitGrp.Name.ToUpper
                            radUnitGrp.Tag = enAnalysisReport.UnitGroup
                        Case radUnit.Name.ToUpper
                            radUnit.Tag = enAnalysisReport.Unit
                        Case radTeam.Name.ToUpper
                            radTeam.Tag = enAnalysisReport.Team
                        Case radJobGrp.Name.ToUpper
                            radJobGrp.Tag = enAnalysisReport.JobGroup
                        Case radJob.Name.ToUpper
                            radJob.Tag = enAnalysisReport.Job
                        Case radClassGrp.Name.ToUpper
                            radClassGrp.Tag = enAnalysisReport.ClassGroup
                        Case radClass.Name.ToUpper
                            radClass.Tag = enAnalysisReport.Classs
                        Case radCostCenter.Name.ToUpper
                            radCostCenter.Tag = enAnalysisReport.CostCenter
                        Case radGradeGrp.Name.ToUpper
                            radGradeGrp.Tag = enAnalysisReport.GradeGroup
                        Case radGrade.Name.ToUpper
                            radGrade.Tag = enAnalysisReport.Grade
                        Case radGradeLevel.Name.ToUpper
                            radGradeLevel.Tag = enAnalysisReport.GradeLevel
                            'Sohail (02 Sep 2016) -- End

                    End Select
                End If
            Next
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Assign_Tags:", mstrModuleName)
        End Try
    End Sub

    'Sohail (02 Sep 2016) -- Start
    'Enhancement -  64.1 - New Budget Redesign for Marie Stopes (New JV Report Sun Account Project JV).
    Public Shared Function GetAnalysisByDetails(ByVal strViewAnalysisModuleName As String _
                                                , ByVal intViewIndex As Integer _
                                                , ByVal strReportBy_Ids As String _
                                                , ByVal dtEffectiveDate As Date _
                                                , Optional ByVal strHr_EmployeeTable_Alias As String = "" _
                                                , Optional ByRef stroutAnalysis_Fields As String = "" _
                                                , Optional ByRef stroutAnalysis_Join As String = "" _
                                                , Optional ByRef stroutAnalysis_OrderBy As String = "" _
                                                , Optional ByRef stroutAnalysis_OrderBy_GroupName As String = "" _
                                                , Optional ByRef stroutReport_GroupName As String = "" _
                                                , Optional ByRef stroutAnalysis_TableName As String = "" _
                                                , Optional ByRef stroutAnalysis_CodeField As String = "" _
                                                , Optional ByVal strDBName As String = "" _
                                                ) As Boolean
        'Sohail (22 Jan 2021) - [strDBName]
        'Sohail (01 Mar 2017) - [stroutAnalysis_CodeField]

        Try



            Dim strAllocationQry, strJobQry, strCCQry, strSalQry As String
            strAllocationQry = "" : strJobQry = "" : strCCQry = "" : strSalQry = ""
            'Sohail (22 Jan 2021) -- Start
            'NMB Issue : - Payroll total variance report emp count not matching with payroll summary report for 2 different database periods.
            Dim strDatabaseName As String = ""
            If strDBName.Trim.Length > 0 Then
                strDatabaseName = strDBName & ".."
            End If
            'Sohail (22 Jan 2021) -- End

            strAllocationQry = "SELECT " & _
                               "     stationunkid " & _
                               "    ,deptgroupunkid " & _
                               "    ,departmentunkid " & _
                               "    ,sectiongroupunkid " & _
                               "    ,sectionunkid " & _
                               "    ,unitgroupunkid " & _
                               "    ,unitunkid " & _
                               "    ,teamunkid " & _
                               "    ,classgroupunkid " & _
                               "    ,classunkid " & _
                               "    ,employeeunkid " & _
                               "FROM " & _
                               "( " & _
                               "        SELECT " & _
                               "             stationunkid " & _
                               "            ,deptgroupunkid " & _
                               "            ,departmentunkid " & _
                               "            ,sectiongroupunkid " & _
                               "            ,sectionunkid " & _
                               "            ,unitgroupunkid " & _
                               "            ,unitunkid " & _
                               "            ,teamunkid " & _
                               "            ,classgroupunkid " & _
                               "            ,classunkid " & _
                               "            ,employeeunkid " & _
                               "            ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
                               "        FROM " & strDatabaseName & "hremployee_transfer_tran WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(dtEffectiveDate) & "' " & _
                               ") AS A WHERE A.rno = 1 "

            strJobQry = "SELECT " & _
                        "    jobgroupunkid " & _
                        "   ,jobunkid " & _
                        "   ,employeeunkid " & _
                        "FROM " & _
                        "( " & _
                        "   SELECT " & _
                        "        jobgroupunkid " & _
                        "       ,jobunkid " & _
                        "       ,employeeunkid " & _
                        "       ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
                        "   FROM " & strDatabaseName & "hremployee_categorization_tran WHERE isvoid = 0 " & _
                        "       AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(dtEffectiveDate) & "' " & _
                        ") AS A WHERE A.rno = 1 "

            strCCQry = "SELECT " & _
                       "     costcenterunkid " & _
                       "    ,employeeunkid " & _
                       "FROM " & _
                       "( " & _
                       "    SELECT " & _
                       "         cctranheadvalueid AS costcenterunkid " & _
                       "        ,employeeunkid " & _
                       "        ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
                       "    FROM " & strDatabaseName & "hremployee_cctranhead_tran WHERE isvoid = 0 AND istransactionhead = 0 " & _
                       "        AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(dtEffectiveDate) & "' " & _
                       ") AS A WHERE A.rno = 1 "

            strSalQry = "SELECT " & _
                        "    gradegroupunkid " & _
                        "   ,gradeunkid " & _
                        "   ,gradelevelunkid " & _
                        "   ,employeeunkid " & _
                        "FROM " & _
                        "( " & _
                        "   SELECT " & _
                        "        gradegroupunkid " & _
                        "       ,gradeunkid " & _
                        "       ,gradelevelunkid " & _
                        "       ,employeeunkid " & _
                        "       ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY incrementdate DESC,salaryincrementtranunkid DESC) AS rno " & _
                        "   FROM " & strDatabaseName & "prsalaryincrement_tran WHERE isvoid = 0 AND isapproved = 1 " & _
                        "       AND CONVERT(CHAR(8),incrementdate,112) <= '" & eZeeDate.convertDate(dtEffectiveDate) & "' " & _
                        ") AS A WHERE A.rno = 1 "


            If strHr_EmployeeTable_Alias.Trim = "" Then strHr_EmployeeTable_Alias = "hremployee_master"

            Select Case intViewIndex

                Case enAnalysisReport.Branch

                    stroutAnalysis_Fields = ", hrstation_master.stationunkid AS Id, hrstation_master.name AS GName "

                    'Sohail (01 Mar 2017) -- Start
                    'Enhancement - 65.1 - Export and Import option on Budget Codes.
                    stroutAnalysis_CodeField = ", hrstation_master.code AS GCode "
                    'Sohail (01 Mar 2017) -- End

                    stroutAnalysis_Join = "JOIN " & _
                                   "(" & vbCrLf & _
                                       strAllocationQry & vbCrLf & _
                                   ") AS VB_TRF ON VB_TRF.employeeunkid = " & strHr_EmployeeTable_Alias & ".employeeunkid " & _
                                   " JOIN " & strDatabaseName & "hrstation_master ON hrstation_master.stationunkid = VB_TRF.stationunkid AND hrstation_master.stationunkid IN ( " & strReportBy_Ids & " ) "

                    stroutAnalysis_OrderBy = " hrstation_master.stationunkid "
                    stroutAnalysis_OrderBy_GroupName = " hrstation_master.name "
                    'Hemant (19 Jul 2019) -- Start
                    'ISSUE/ENHANCEMENT#3984(Eko Supreme) - Allocation Captions Not Showing on Pension.
                    'stroutReport_GroupName = Language.getMessage(strViewAnalysisModuleName, 2, "Branch :")
                    stroutReport_GroupName = Language._Object.getCaption("radBranch", "Branch") & " :"
                    'Hemant (19 Jul 2019) -- End
                    stroutAnalysis_TableName = " hrstation_master "

                Case enAnalysisReport.Department
                    stroutAnalysis_Fields = ", hrdepartment_master.departmentunkid AS Id, hrdepartment_master.name AS GName "

                    'Sohail (01 Mar 2017) -- Start
                    'Enhancement - 65.1 - Export and Import option on Budget Codes.
                    stroutAnalysis_CodeField = ", hrdepartment_master.code AS GCode "
                    'Sohail (01 Mar 2017) -- End

                    stroutAnalysis_Join = "JOIN " & _
                                        "(" & vbCrLf & _
                                            strAllocationQry & vbCrLf & _
                                        ") AS VB_TRF ON VB_TRF.employeeunkid = " & strHr_EmployeeTable_Alias & ".employeeunkid " & _
                                        " JOIN " & strDatabaseName & "hrdepartment_master ON hrdepartment_master.departmentunkid = VB_TRF.departmentunkid AND hrdepartment_master.departmentunkid IN ( " & strReportBy_Ids & " ) "

                    stroutAnalysis_OrderBy = " hrdepartment_master.departmentunkid "
                    stroutAnalysis_OrderBy_GroupName = " hrdepartment_master.name "
                    'Hemant (19 Jul 2019) -- Start
                    'ISSUE/ENHANCEMENT#3984(Eko Supreme) - Allocation Captions Not Showing on Pension.
                    'stroutReport_GroupName = Language.getMessage(strViewAnalysisModuleName, 3, "Department :")
                    stroutReport_GroupName = Language._Object.getCaption("radDepartment", "Department") & " :"
                    'Hemant (19 Jul 2019) -- End
                    stroutAnalysis_TableName = " hrdepartment_master "

                Case enAnalysisReport.Job
                    stroutAnalysis_Fields = ",  hrjob_master.jobunkid AS Id, hrjob_master.job_name AS GName "

                    'Sohail (01 Mar 2017) -- Start
                    'Enhancement - 65.1 - Export and Import option on Budget Codes.
                    stroutAnalysis_CodeField = ", hrjob_master.job_code AS GCode "
                    'Sohail (01 Mar 2017) -- End

                    stroutAnalysis_Join = "JOIN " & _
                                        "(" & vbCrLf & _
                                            strJobQry & vbCrLf & _
                                        ") AS VB_RECAT ON VB_RECAT.employeeunkid = " & strHr_EmployeeTable_Alias & ".employeeunkid " & _
                                        " JOIN " & strDatabaseName & "hrjob_master ON hrjob_master.jobunkid = VB_RECAT.jobunkid AND hrjob_master.jobunkid IN ( " & strReportBy_Ids & " ) "

                    stroutAnalysis_OrderBy = " hrjob_master.jobunkid "
                    stroutAnalysis_OrderBy_GroupName = " hrjob_master.job_name "
                    'Hemant (19 Jul 2019) -- Start
                    'ISSUE/ENHANCEMENT#3984(Eko Supreme) - Allocation Captions Not Showing on Pension.
                    'stroutReport_GroupName = Language.getMessage(strViewAnalysisModuleName, 4, "Job :")
                    stroutReport_GroupName = Language._Object.getCaption("radJob", "Job") & " :"
                    'Hemant (19 Jul 2019) -- End
                    stroutAnalysis_TableName = " hrjob_master "

                Case enAnalysisReport.Section
                    stroutAnalysis_Fields = ",  hrsection_master.sectionunkid AS Id, hrsection_master.name AS GName "

                    'Sohail (01 Mar 2017) -- Start
                    'Enhancement - 65.1 - Export and Import option on Budget Codes.
                    stroutAnalysis_CodeField = ", hrsection_master.code AS GCode "
                    'Sohail (01 Mar 2017) -- End

                    stroutAnalysis_Join = "JOIN " & _
                                        "(" & vbCrLf & _
                                            strAllocationQry & vbCrLf & _
                                        ") AS VB_TRF ON VB_TRF.employeeunkid = " & strHr_EmployeeTable_Alias & ".employeeunkid " & _
                                        " JOIN " & strDatabaseName & "hrsection_master ON hrsection_master.sectionunkid = VB_TRF.sectionunkid AND hrsection_master.sectionunkid IN ( " & strReportBy_Ids & " ) "

                    stroutAnalysis_OrderBy = " hrsection_master.sectionunkid "
                    stroutAnalysis_OrderBy_GroupName = " hrsection_master.name "
                    'Hemant (19 Jul 2019) -- Start
                    'ISSUE/ENHANCEMENT#3984(Eko Supreme) - Allocation Captions Not Showing on Pension.
                    'stroutReport_GroupName = Language.getMessage(strViewAnalysisModuleName, 5, "Section :")
                    stroutReport_GroupName = Language._Object.getCaption("radSection", "Section") & " :"
                    'Hemant (19 Jul 2019) -- End
                    stroutAnalysis_TableName = " hrsection_master "

                Case enAnalysisReport.Unit
                    stroutAnalysis_Fields = ",  hrunit_master.unitunkid AS Id, hrunit_master.name AS GName "

                    'Sohail (01 Mar 2017) -- Start
                    'Enhancement - 65.1 - Export and Import option on Budget Codes.
                    stroutAnalysis_CodeField = ", hrunit_master.code AS GCode "
                    'Sohail (01 Mar 2017) -- End

                    stroutAnalysis_Join = "JOIN " & _
                                        "(" & vbCrLf & _
                                            strAllocationQry & vbCrLf & _
                                        ") AS VB_TRF ON VB_TRF.employeeunkid = " & strHr_EmployeeTable_Alias & ".employeeunkid " & _
                                        " JOIN " & strDatabaseName & "hrunit_master ON hrunit_master.unitunkid = VB_TRF.unitunkid AND hrunit_master.unitunkid IN ( " & strReportBy_Ids & " ) "

                    stroutAnalysis_OrderBy = " hrunit_master.unitunkid "
                    stroutAnalysis_OrderBy_GroupName = " hrunit_master.name "
                    'Hemant (19 Jul 2019) -- Start
                    'ISSUE/ENHANCEMENT#3984(Eko Supreme) - Allocation Captions Not Showing on Pension.
                    'stroutReport_GroupName = Language.getMessage(strViewAnalysisModuleName, 6, "Unit :")
                    stroutReport_GroupName = Language._Object.getCaption("radUnit", "Unit") & " :"
                    'Hemant (19 Jul 2019) -- End
                    stroutAnalysis_TableName = " hrunit_master "

                Case enAnalysisReport.CostCenter
                    stroutAnalysis_Fields = ", prcostcenter_master.costcenterunkid AS Id, prcostcenter_master.costcentername AS GName "

                    'Sohail (01 Mar 2017) -- Start
                    'Enhancement - 65.1 - Export and Import option on Budget Codes.
                    stroutAnalysis_CodeField = ", prcostcenter_master.costcentercode AS GCode "
                    'Sohail (01 Mar 2017) -- End

                    stroutAnalysis_Join = "JOIN " & _
                                        "(" & vbCrLf & _
                                            strCCQry & vbCrLf & _
                                        ") AS VB_CCR ON VB_CCR.employeeunkid = " & strHr_EmployeeTable_Alias & ".employeeunkid " & _
                                        " JOIN " & strDatabaseName & "prcostcenter_master ON prcostcenter_master.costcenterunkid = VB_CCR.costcenterunkid AND prcostcenter_master.costcenterunkid IN ( " & strReportBy_Ids & " ) "

                    stroutAnalysis_OrderBy = " prcostcenter_master.costcenterunkid "
                    stroutAnalysis_OrderBy_GroupName = " prcostcenter_master.costcentername "
                    'Hemant (19 Jul 2019) -- Start
                    'ISSUE/ENHANCEMENT#3984(Eko Supreme) - Allocation Captions Not Showing on Pension.
                    'stroutReport_GroupName = Language.getMessage(strViewAnalysisModuleName, 7, "Cost Center :")
                    stroutReport_GroupName = Language._Object.getCaption("radCostCenter", "Cost Center") & " :"
                    'Hemant (19 Jul 2019) -- End
                    stroutAnalysis_TableName = " prcostcenter_master "

                Case enAnalysisReport.SectionGroup
                    stroutAnalysis_Fields = ", hrsectiongroup_master.sectiongroupunkid AS Id, hrsectiongroup_master.name AS GName "

                    'Sohail (01 Mar 2017) -- Start
                    'Enhancement - 65.1 - Export and Import option on Budget Codes.
                    stroutAnalysis_CodeField = ", hrsectiongroup_master.code AS GCode "
                    'Sohail (01 Mar 2017) -- End

                    stroutAnalysis_Join = "JOIN " & _
                                        "(" & vbCrLf & _
                                            strAllocationQry & vbCrLf & _
                                        ") AS VB_TRF ON VB_TRF.employeeunkid = " & strHr_EmployeeTable_Alias & ".employeeunkid " & _
                                        " JOIN " & strDatabaseName & "hrsectiongroup_master ON hrsectiongroup_master.sectiongroupunkid = VB_TRF.sectiongroupunkid AND hrsectiongroup_master.sectiongroupunkid IN ( " & strReportBy_Ids & " ) "

                    stroutAnalysis_OrderBy = " hrsectiongroup_master.sectiongroupunkid "
                    stroutAnalysis_OrderBy_GroupName = " hrsectiongroup_master.name "
                    'Hemant (19 Jul 2019) -- Start
                    'ISSUE/ENHANCEMENT#3984(Eko Supreme) - Allocation Captions Not Showing on Pension.
                    'stroutReport_GroupName = Language.getMessage(strViewAnalysisModuleName, 8, "Section Group :")
                    stroutReport_GroupName = Language._Object.getCaption("radSectionGrp", "Section Group") & " :"
                    'Hemant (19 Jul 2019) -- End
                    stroutAnalysis_TableName = " hrsectiongroup_master "

                Case enAnalysisReport.UnitGroup
                    stroutAnalysis_Fields = ", hrunitgroup_master.unitgroupunkid AS Id, hrunitgroup_master.name AS GName "

                    'Sohail (01 Mar 2017) -- Start
                    'Enhancement - 65.1 - Export and Import option on Budget Codes.
                    stroutAnalysis_CodeField = ", hrunitgroup_master.code AS GCode "
                    'Sohail (01 Mar 2017) -- End

                    stroutAnalysis_Join = "JOIN " & _
                                        "(" & vbCrLf & _
                                            strAllocationQry & vbCrLf & _
                                        ") AS VB_TRF ON VB_TRF.employeeunkid = " & strHr_EmployeeTable_Alias & ".employeeunkid " & _
                                        " JOIN " & strDatabaseName & "hrunitgroup_master ON hrunitgroup_master.unitgroupunkid = VB_TRF.unitgroupunkid AND hrunitgroup_master.unitgroupunkid IN ( " & strReportBy_Ids & " ) "

                    stroutAnalysis_OrderBy = " hrunitgroup_master.unitgroupunkid "
                    stroutAnalysis_OrderBy_GroupName = " hrunitgroup_master.name "
                    'Hemant (19 Jul 2019) -- Start
                    'ISSUE/ENHANCEMENT#3984(Eko Supreme) - Allocation Captions Not Showing on Pension.
                    'stroutReport_GroupName = Language.getMessage(strViewAnalysisModuleName, 9, "Unit Group :")
                    stroutReport_GroupName = Language._Object.getCaption("radUnitGrp", "Unit Group") & " :"
                    'Hemant (19 Jul 2019) -- End
                    stroutAnalysis_TableName = " hrunitgroup_master "

                Case enAnalysisReport.Team
                    stroutAnalysis_Fields = ", hrteam_master.teamunkid AS Id, hrteam_master.name AS GName "

                    'Sohail (01 Mar 2017) -- Start
                    'Enhancement - 65.1 - Export and Import option on Budget Codes.
                    stroutAnalysis_CodeField = ", hrteam_master.code AS GCode "
                    'Sohail (01 Mar 2017) -- End

                    stroutAnalysis_Join = "JOIN " & _
                                        "(" & vbCrLf & _
                                            strAllocationQry & vbCrLf & _
                                        ") AS VB_TRF ON VB_TRF.employeeunkid = " & strHr_EmployeeTable_Alias & ".employeeunkid " & _
                                        " JOIN " & strDatabaseName & "hrteam_master ON hrteam_master.teamunkid = VB_TRF.teamunkid AND hrteam_master.teamunkid IN ( " & strReportBy_Ids & " ) "

                    stroutAnalysis_OrderBy = " hrteam_master.teamunkid "
                    stroutAnalysis_OrderBy_GroupName = " hrteam_master.name "
                    'Hemant (19 Jul 2019) -- Start
                    'ISSUE/ENHANCEMENT#3984(Eko Supreme) - Allocation Captions Not Showing on Pension.
                    'stroutReport_GroupName = Language.getMessage(strViewAnalysisModuleName, 10, "Team :")
                    stroutReport_GroupName = Language._Object.getCaption("radTeam", "Team") & " :"
                    'Hemant (19 Jul 2019) -- End
                    stroutAnalysis_TableName = " hrteam_master "

                Case enAnalysisReport.DepartmentGroup
                    stroutAnalysis_Fields = ", hrdepartment_group_master.deptgroupunkid AS Id, hrdepartment_group_master.name AS GName "

                    'Sohail (01 Mar 2017) -- Start
                    'Enhancement - 65.1 - Export and Import option on Budget Codes.
                    stroutAnalysis_CodeField = ", hrdepartment_group_master.code AS GCode "
                    'Sohail (01 Mar 2017) -- End

                    stroutAnalysis_Join = "JOIN " & _
                                        "(" & vbCrLf & _
                                            strAllocationQry & vbCrLf & _
                                        ") AS VB_TRF ON VB_TRF.employeeunkid = " & strHr_EmployeeTable_Alias & ".employeeunkid " & _
                                        " JOIN " & strDatabaseName & "hrdepartment_group_master ON hrdepartment_group_master.deptgroupunkid = VB_TRF.deptgroupunkid AND hrdepartment_group_master.deptgroupunkid IN ( " & strReportBy_Ids & " ) "

                    stroutAnalysis_OrderBy = " hrdepartment_group_master.deptgroupunkid "
                    stroutAnalysis_OrderBy_GroupName = " hrdepartment_group_master.name "
                    'Hemant (19 Jul 2019) -- Start
                    'ISSUE/ENHANCEMENT#3984(Eko Supreme) - Allocation Captions Not Showing on Pension.
                    'stroutReport_GroupName = Language.getMessage(strViewAnalysisModuleName, 11, "Department Group :")
                    stroutReport_GroupName = Language._Object.getCaption("radDepartmentGrp", "Department Group") & " :"
                    'Hemant (19 Jul 2019) -- End
                    stroutAnalysis_TableName = " hrdepartment_group_master "

                Case enAnalysisReport.JobGroup
                    stroutAnalysis_Fields = ", hrjobgroup_master.jobgroupunkid AS Id, hrjobgroup_master.name AS GName "

                    'Sohail (01 Mar 2017) -- Start
                    'Enhancement - 65.1 - Export and Import option on Budget Codes.
                    stroutAnalysis_CodeField = ", hrjobgroup_master.code AS GCode "
                    'Sohail (01 Mar 2017) -- End

                    stroutAnalysis_Join = "JOIN " & _
                                        "(" & vbCrLf & _
                                            strJobQry & vbCrLf & _
                                        ") AS VB_RECAT ON VB_RECAT.employeeunkid = " & strHr_EmployeeTable_Alias & ".employeeunkid " & _
                                        " JOIN " & strDatabaseName & "hrjobgroup_master ON hrjobgroup_master.jobgroupunkid = VB_RECAT.jobgroupunkid AND hrjobgroup_master.jobgroupunkid IN ( " & strReportBy_Ids & " ) "

                    stroutAnalysis_OrderBy = " hrjobgroup_master.jobgroupunkid "
                    stroutAnalysis_OrderBy_GroupName = " hrjobgroup_master.name "
                    'Hemant (19 Jul 2019) -- Start
                    'ISSUE/ENHANCEMENT#3984(Eko Supreme) - Allocation Captions Not Showing on Pension.
                    'stroutReport_GroupName = Language.getMessage(strViewAnalysisModuleName, 12, "Job Group :")
                    stroutReport_GroupName = Language._Object.getCaption("radJobGrp", "Job Group") & " :"
                    'Hemant (19 Jul 2019) -- End
                    stroutAnalysis_TableName = " hrjobgroup_master "

                Case enAnalysisReport.ClassGroup
                    stroutAnalysis_Fields = ", hrclassgroup_master.classgroupunkid AS Id, hrclassgroup_master.name AS GName "

                    'Sohail (01 Mar 2017) -- Start
                    'Enhancement - 65.1 - Export and Import option on Budget Codes.
                    stroutAnalysis_CodeField = ", hrclassgroup_master.code AS GCode "
                    'Sohail (01 Mar 2017) -- End

                    stroutAnalysis_Join = "JOIN " & _
                                        "(" & vbCrLf & _
                                            strAllocationQry & vbCrLf & _
                                        ") AS VB_TRF ON VB_TRF.employeeunkid = " & strHr_EmployeeTable_Alias & ".employeeunkid " & _
                                        " JOIN " & strDatabaseName & "hrclassgroup_master ON hrclassgroup_master.classgroupunkid = VB_TRF.classgroupunkid AND hrclassgroup_master.classgroupunkid IN ( " & strReportBy_Ids & " ) "

                    stroutAnalysis_OrderBy = " hrclassgroup_master.classgroupunkid "
                    stroutAnalysis_OrderBy_GroupName = " hrclassgroup_master.name "
                    'Hemant (19 Jul 2019) -- Start
                    'ISSUE/ENHANCEMENT#3984(Eko Supreme) - Allocation Captions Not Showing on Pension.
                    'stroutReport_GroupName = Language.getMessage(strViewAnalysisModuleName, 13, "Class Group :")
                    stroutReport_GroupName = Language._Object.getCaption("radClassGrp", "Class Group") & " :"
                    'Hemant (19 Jul 2019) -- End
                    stroutAnalysis_TableName = " hrclassgroup_master "

                Case enAnalysisReport.Classs
                    stroutAnalysis_Fields = ", hrclasses_master.classesunkid AS Id, hrclasses_master.name AS GName "

                    'Sohail (01 Mar 2017) -- Start
                    'Enhancement - 65.1 - Export and Import option on Budget Codes.
                    stroutAnalysis_CodeField = ", hrclasses_master.code AS GCode "
                    'Sohail (01 Mar 2017) -- End

                    stroutAnalysis_Join = "JOIN " & _
                                        "(" & vbCrLf & _
                                            strAllocationQry & vbCrLf & _
                                        ") AS VB_TRF ON VB_TRF.employeeunkid = " & strHr_EmployeeTable_Alias & ".employeeunkid " & _
                                        " JOIN " & strDatabaseName & "hrclasses_master ON hrclasses_master.classesunkid = VB_TRF.classunkid AND hrclasses_master.classesunkid IN ( " & strReportBy_Ids & " ) "

                    stroutAnalysis_OrderBy = " hrclasses_master.classesunkid "
                    stroutAnalysis_OrderBy_GroupName = " hrclasses_master.name "
                    'Hemant (19 Jul 2019) -- Start
                    'ISSUE/ENHANCEMENT#3984(Eko Supreme) - Allocation Captions Not Showing on Pension.
                    'stroutReport_GroupName = Language.getMessage(strViewAnalysisModuleName, 14, "Class :")
                    stroutReport_GroupName = Language._Object.getCaption("radClass", "Class") & " :"
                    'Hemant (19 Jul 2019) -- End
                    stroutAnalysis_TableName = " hrclasses_master "

                Case enAnalysisReport.GradeGroup
                    stroutAnalysis_Fields = ", hrgradegroup_master.gradegroupunkid AS Id, hrgradegroup_master.name AS GName "

                    'Sohail (01 Mar 2017) -- Start
                    'Enhancement - 65.1 - Export and Import option on Budget Codes.
                    stroutAnalysis_CodeField = ", hrgradegroup_master.code AS GCode "
                    'Sohail (01 Mar 2017) -- End

                    stroutAnalysis_Join = "JOIN " & _
                                    "(" & vbCrLf & _
                                        strSalQry & vbCrLf & _
                                    ") AS VB_SALI ON VB_SALI.employeeunkid = " & strHr_EmployeeTable_Alias & ".employeeunkid " & _
                                    " JOIN " & strDatabaseName & "hrgradegroup_master ON hrgradegroup_master.gradegroupunkid = VB_SALI.gradegroupunkid AND hrgradegroup_master.gradegroupunkid IN ( " & strReportBy_Ids & " ) "

                    stroutAnalysis_OrderBy = " hrgradegroup_master.gradegroupunkid "
                    stroutAnalysis_OrderBy_GroupName = " hrgradegroup_master.name "
                    'Hemant (19 Jul 2019) -- Start
                    'ISSUE/ENHANCEMENT#3984(Eko Supreme) - Allocation Captions Not Showing on Pension.
                    'stroutReport_GroupName = Language.getMessage(strViewAnalysisModuleName, 15, "Grade Group :")
                    stroutReport_GroupName = Language._Object.getCaption("radGradeGrp", "Grade Group") & " :"
                    'Hemant (19 Jul 2019) -- End
                    stroutAnalysis_TableName = " hrgradegroup_master "

                Case enAnalysisReport.Grade
                    stroutAnalysis_Fields = ", hrgrade_master.gradeunkid AS Id, hrgrade_master.name AS GName "

                    'Sohail (01 Mar 2017) -- Start
                    'Enhancement - 65.1 - Export and Import option on Budget Codes.
                    stroutAnalysis_CodeField = ", hrgrade_master.code AS GCode "
                    'Sohail (01 Mar 2017) -- End

                    stroutAnalysis_Join = "JOIN " & _
                                        "(" & vbCrLf & _
                                            strSalQry & vbCrLf & _
                                        ") AS VB_SALI ON VB_SALI.employeeunkid = " & strHr_EmployeeTable_Alias & ".employeeunkid " & _
                                        " JOIN " & strDatabaseName & "hrgrade_master ON hrgrade_master.gradeunkid = VB_SALI.gradeunkid AND hrgrade_master.gradeunkid IN ( " & strReportBy_Ids & " ) "

                    stroutAnalysis_OrderBy = " hrgrade_master.gradeunkid "
                    stroutAnalysis_OrderBy_GroupName = " hrgrade_master.name "
                    'Hemant (19 Jul 2019) -- Start
                    'ISSUE/ENHANCEMENT#3984(Eko Supreme) - Allocation Captions Not Showing on Pension.
                    'stroutReport_GroupName = Language.getMessage(strViewAnalysisModuleName, 16, "Grade :")
                    stroutReport_GroupName = Language._Object.getCaption("radGrade", "Grade") & " :"
                    'Hemant (19 Jul 2019) -- End
                    stroutAnalysis_TableName = " hrgrade_master "

                Case enAnalysisReport.GradeLevel
                    stroutAnalysis_Fields = ", hrgradelevel_master.gradelevelunkid AS Id, hrgradelevel_master.name AS GName "

                    'Sohail (01 Mar 2017) -- Start
                    'Enhancement - 65.1 - Export and Import option on Budget Codes.
                    stroutAnalysis_CodeField = ", hrgradelevel_master.code AS GCode "
                    'Sohail (01 Mar 2017) -- End

                    stroutAnalysis_Join = "JOIN " & _
                                        "(" & vbCrLf & _
                                            strSalQry & vbCrLf & _
                                        ") AS VB_SALI ON VB_SALI.employeeunkid = " & strHr_EmployeeTable_Alias & ".employeeunkid " & _
                                        " JOIN " & strDatabaseName & "hrgradelevel_master ON hrgradelevel_master.gradelevelunkid = VB_SALI.gradelevelunkid AND hrgradelevel_master.gradelevelunkid IN ( " & strReportBy_Ids & " ) "

                    stroutAnalysis_OrderBy = " hrgradelevel_master.gradelevelunkid "
                    stroutAnalysis_OrderBy_GroupName = " hrgradelevel_master.name "
                    'Hemant (19 Jul 2019) -- Start
                    'ISSUE/ENHANCEMENT#3984(Eko Supreme) - Allocation Captions Not Showing on Pension.
                    'stroutReport_GroupName = Language.getMessage(strViewAnalysisModuleName, 17, "Grade Level :")
                    stroutReport_GroupName = Language._Object.getCaption("radGradeLevel", "Grade Level") & " :"
                    'Hemant (19 Jul 2019) -- End
                    stroutAnalysis_TableName = " hrgradelevel_master "

                Case Else
                    stroutAnalysis_Fields = ", 0 AS Id, '' AS GName "
                    stroutAnalysis_CodeField = "" 'Sohail (01 Mar 2017)
                    stroutAnalysis_Join = ""
                    stroutAnalysis_OrderBy = ""
                    stroutAnalysis_OrderBy_GroupName = ""
                    strHr_EmployeeTable_Alias = ""
                    stroutReport_GroupName = ""
                    stroutAnalysis_TableName = " "

            End Select

        Catch ex As Exception
            Throw New Exception("Procedure name : GetAnalysisByDetails ; ModuleName : " & strViewAnalysisModuleName & ";" & ex.Message)
        End Try
    End Function
    'Sohail (02 Sep 2016) -- End
#End Region
    'Shani [ 10 OCT 2014 ] -- END

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Private Sub OtherSettings()
        Try
            Me.SuspendLayout()
         
            Call SetLanguage()

            Me.btnCancel.GradientBackColor = GUI._ButttonBackColor
            Me.btnCancel.GradientForeColor = GUI._ButttonFontColor

            Me.btnSet.GradientBackColor = GUI._ButttonBackColor
            Me.btnSet.GradientForeColor = GUI._ButttonFontColor


            Me.ResumeLayout()
        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "OtherSettings", mstrModuleName)
        End Try
    End Sub


    Private Sub SetLanguage()
        Try
            Me.Text = Language._Object.getCaption(Me.Name, Me.Text)

            Me.btnCancel.Text = Language._Object.getCaption(Me.btnCancel.Name, Me.btnCancel.Text)
            Me.btnSet.Text = Language._Object.getCaption(Me.btnSet.Name, Me.btnSet.Text)
            Me.chkSelectallList.Text = Language._Object.getCaption(Me.chkSelectallList.Name, Me.chkSelectallList.Text)
            Me.lblAnalysisBy.Text = Language._Object.getCaption(Me.lblAnalysisBy.Name, Me.lblAnalysisBy.Text)
            Me.lblReportBy.Text = Language._Object.getCaption(Me.lblReportBy.Name, Me.lblReportBy.Text)
            Me.radCostCenter.Text = Language._Object.getCaption(Me.radCostCenter.Name, Me.radCostCenter.Text)
            Me.radJob.Text = Language._Object.getCaption(Me.radJob.Name, Me.radJob.Text)
            Me.radUnit.Text = Language._Object.getCaption(Me.radUnit.Name, Me.radUnit.Text)
            Me.radDepartment.Text = Language._Object.getCaption(Me.radDepartment.Name, Me.radDepartment.Text)
            Me.radSection.Text = Language._Object.getCaption(Me.radSection.Name, Me.radSection.Text)
            Me.radBranch.Text = Language._Object.getCaption(Me.radBranch.Name, Me.radBranch.Text)
            Me.dgcolhReportBy.HeaderText = Language._Object.getCaption(Me.dgcolhReportBy.Name, Me.dgcolhReportBy.HeaderText)
            Me.radTeam.Text = Language._Object.getCaption(Me.radTeam.Name, Me.radTeam.Text)
            Me.radUnitGrp.Text = Language._Object.getCaption(Me.radUnitGrp.Name, Me.radUnitGrp.Text)
            Me.radSectionGrp.Text = Language._Object.getCaption(Me.radSectionGrp.Name, Me.radSectionGrp.Text)
            Me.radJobGrp.Text = Language._Object.getCaption(Me.radJobGrp.Name, Me.radJobGrp.Text)
            Me.radDepartmentGrp.Text = Language._Object.getCaption(Me.radDepartmentGrp.Name, Me.radDepartmentGrp.Text)
            Me.radGradeLevel.Text = Language._Object.getCaption(Me.radGradeLevel.Name, Me.radGradeLevel.Text)
            Me.radGrade.Text = Language._Object.getCaption(Me.radGrade.Name, Me.radGrade.Text)
            Me.radGradeGrp.Text = Language._Object.getCaption(Me.radGradeGrp.Name, Me.radGradeGrp.Text)
            Me.radClass.Text = Language._Object.getCaption(Me.radClass.Name, Me.radClass.Text)
            Me.radClassGrp.Text = Language._Object.getCaption(Me.radClassGrp.Name, Me.radClassGrp.Text)

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
        End Try
    End Sub


    Private Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Please check atleast one report by")
            Language.setMessage(mstrModuleName, 18, "Please select any Analysis by")
            'Language.setMessage(strViewAnalysisModuleName, 2, "Branch :")
            'Language.setMessage(strViewAnalysisModuleName, 3, "Department :")
            'Language.setMessage(strViewAnalysisModuleName, 4, "Job :")
            'Language.setMessage(strViewAnalysisModuleName, 5, "Section :")
            'Language.setMessage(strViewAnalysisModuleName, 6, "Unit :")
            'Language.setMessage(strViewAnalysisModuleName, 7, "Cost Center :")
            'Language.setMessage(strViewAnalysisModuleName, 8, "Section Group :")
            'Language.setMessage(strViewAnalysisModuleName, 9, "Unit Group :")
            'Language.setMessage(strViewAnalysisModuleName, 10, "Team :")
            'Language.setMessage(strViewAnalysisModuleName, 11, "Department Group :")
            'Language.setMessage(strViewAnalysisModuleName, 12, "Job Group :")
            'Language.setMessage(strViewAnalysisModuleName, 13, "Class Group :")
            'Language.setMessage(strViewAnalysisModuleName, 14, "Class :")
            'Language.setMessage(strViewAnalysisModuleName, 15, "Grade Group :")
            'Language.setMessage(strViewAnalysisModuleName, 16, "Grade :")
            'Language.setMessage(strViewAnalysisModuleName, 17, "Grade Level :")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class