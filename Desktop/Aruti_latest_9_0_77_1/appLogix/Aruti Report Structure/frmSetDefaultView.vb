#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data

#End Region

Public Class frmSetDefaultView

#Region " Private Variables "

    Private ReadOnly mstrModuleName As String = "frmSetDefaultView"
    Private mblnCancel As Boolean = True
    Private mintDefaultView As Integer = -1

#End Region

#Region " Display Dialog "

    Public Function displayDialog() As Boolean
        Try
            Me.ShowDialog()
            Return Not mblnCancel

        Catch ex As System.Exception
            Call DisplayError.Show(-1, ex.Message, "displayDialog", mstrModuleName)
        Finally

        End Try
    End Function

#End Region

#Region " Private Method "

    Private Sub DefaultOpt()
        Dim objAppSetting As New clsApplicationSettings
        Try
            Select Case objAppSetting._SetDefaultPrintAction
                Case enPrintAction.Print
                    rabPrint.Checked = True
                Case enPrintAction.Preview
                    rabPreview.Checked = True
            End Select
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "DefaultOpt", mstrModuleName)
        Finally
            objAppSetting = Nothing
        End Try
    End Sub

#End Region

#Region " Form's Events "

    Private Sub frmSetDefaultView_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown
        If e.Control And e.KeyCode = Windows.Forms.Keys.S Then
            Call btnOk_Click(Nothing, Nothing)
        End If
    End Sub

    Private Sub frmSetDefaultView_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles Me.KeyPress
        If Asc(e.KeyChar) = 27 Then
            btnCancel.PerformClick()
        ElseIf Asc(e.KeyChar) = 13 Then
            Call btnOk_Click(Nothing, Nothing)
        End If
    End Sub

    Private Sub frmSetDefaultView_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Call Language.setLanguage(Me.Name)

            Call DefaultOpt()
        Catch ex As Exception
            Call DisplayError.Show(-1, ex.Message, "frmSetDefaultView_Load", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Button's Events "

    Private Sub btnOk_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnOK.Click
        Dim objAppSetting As New clsApplicationSettings
        Try
            If rabPrint.Checked = True Then
                objAppSetting._SetDefaultPrintAction = enPrintAction.Print
            ElseIf rabPreview.Checked = True Then
                objAppSetting._SetDefaultPrintAction = enPrintAction.Preview
            End If
            Me.DialogResult = Windows.Forms.DialogResult.OK
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnOk_Click", mstrModuleName)
        Finally
            objAppSetting = Nothing
        End Try
    End Sub

    'Private Sub objLanguage_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
    '    Try
    '        Dim frm As New frmLanguage
    '        If User._Object._RightToLeft = True Then
    '            frm.RightToLeft = Windows.Forms.RightToLeft.Yes
    '            frm.RightToLeftLayout = True
    '            Call Language.ctlRightToLeftlayOut(frm)
    '        End If
    '        Call SetMessages()
    '        frm.displayDialog(Me)
    '        frm = Nothing

    '        Call Language.setLanguage(Me)
    '        Call SetLanguage()
    '    Catch ex As Exception
    '        Call DisplayError.Show(-1, ex.Message, "objLanguage_Click", mstrModuleName)
    '    End Try
    'End Sub

    Private Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Try
            mblnCancel = True
            Me.Close()
        Catch ex As Exception
            Call DisplayError.Show(-1, ex.Message, "btnCancel_Click", mstrModuleName)
        End Try
    End Sub

#End Region

End Class