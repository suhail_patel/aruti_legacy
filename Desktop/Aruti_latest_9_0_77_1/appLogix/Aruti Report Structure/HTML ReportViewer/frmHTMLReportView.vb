﻿#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data

#End Region

Public Class frmHTMLReportView

#Region " Private Variables "

    Private mstrModuleName As String = "frmHTMLReportView"
    Private mstrContent As String = ""
    Private mintFormWidth As Integer = 1130
    Private originalReg As Microsoft.Win32.RegistryKey = Nothing

#End Region

#Region " Display Dialog "

    Public Function displayDialog(ByVal strContent As String, ByVal blnIsPreview As Boolean) As Boolean
        mstrContent = strContent
        If blnIsPreview = True Then
            btnPrint.Visible = False
        End If
        Me.ShowDialog()
        Return True
    End Function

#End Region

#Region " Form's Event "

    Private Sub frmHTMLReportView_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        Call ResetRegistryValue(originalReg)
    End Sub

    Private Sub frmHTMLReportView_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Call Set_Logo(Me, gApplicationType)
        brReportView.Navigate("about:blank") : brReportView.Document.OpenNew(False)
        brReportView.Document.Write(mstrContent)
        brReportView.Refresh()
    End Sub

    Private Sub frmHTMLReportView_ResizeEnd(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.ResizeEnd
        Try
            If mintFormWidth <> brReportView.Size.Width - 30 Then
                Dim intwidth As Integer = brReportView.Size.Width - 30
                Dim strbuilder As String = brReportView.DocumentText
                'strbuilder = strbuilder.Replace("<TABLE style=""WIDTH: " & mintFormWidth & "px""", "<TABLE style=""WIDTH: " & intwidth & "px""")
                strbuilder = strbuilder.Replace("style='width: " & mintFormWidth & "px;'", "style='width: " & intwidth & "px;'")
                brReportView.Document.Write(strbuilder)
                brReportView.Refresh()
                mintFormWidth = intwidth
            End If

        Catch ex As Exception

        End Try
    End Sub

#End Region

#Region " Private Methods "

    Private Sub ResetRegistryValue(ByVal oReg As Microsoft.Win32.RegistryKey)
        Try
            If oReg IsNot Nothing Then
                Dim strNames() As String = oReg.GetValueNames()
                For index As Integer = 0 To strNames.Length - 1
                    Select Case strNames(index).ToString.ToUpper
                        Case "HEADER"
                            oReg.SetValue(strNames(index), "&w&bPage &p of &P")
                        Case "FOOTER"
                            oReg.SetValue(strNames(index), "&u&b&d")
                        Case "MARGIN_BOTTOM"
                            oReg.SetValue(strNames(index), "0.201670")
                        Case "MARGIN_LEFT"
                            oReg.SetValue(strNames(index), "0.200000")
                        Case "MARGIN_RIGHT"
                            oReg.SetValue(strNames(index), "0.200000")
                        Case "MARGIN_TOP"
                            oReg.SetValue(strNames(index), "0.200000")
                        Case "PRINT_BACKGROUND"
                            oReg.SetValue(strNames(index), "no")
                        Case "SHRINK_TO_FIT"
                            oReg.SetValue(strNames(index), "no")
                        Case "FONT"
                            oReg.SetValue(strNames(index), "")
                    End Select
                Next
                oReg.Close()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "ResetRegistryValue", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

#Region " Button's Events "

    Private Sub btnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Me.Close()
    End Sub

    Private Sub btnPrint_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPrint.Click
        Try
            'Dim objReg As Microsoft.Win32.RegistryKey
            'Try
            '    objReg = Microsoft.Win32.Registry.CurrentUser.OpenSubKey("Software\Microsoft\Internet Explorer\PageSetup", True)
            '    originalReg = objReg

            '    If objReg.ValueCount > 0 Then
            '        Dim strNames() As String = objReg.GetValueNames()
            '        For index As Integer = 0 To strNames.Length - 1
            '            Select Case strNames(index).ToString.ToUpper
            '                Case "HEADER"
            '                    objReg.SetValue(strNames(index), "")
            '                Case "FOOTER"
            '                    objReg.SetValue(strNames(index), "")
            '                Case "MARGIN_BOTTOM"
            '                    objReg.SetValue(strNames(index), "0.250000")
            '                Case "MARGIN_LEFT"
            '                    objReg.SetValue(strNames(index), "0.250000")
            '                Case "MARGIN_RIGHT"
            '                    objReg.SetValue(strNames(index), "0.250000")
            '                Case "MARGIN_TOP"
            '                    objReg.SetValue(strNames(index), "0.250000")
            '                Case "PRINT_BACKGROUND"
            '                    objReg.SetValue(strNames(index), "yes")
            '                Case "SHRINK_TO_FIT"
            '                    objReg.SetValue(strNames(index), "yes")
            '                Case "FONT"
            '                    objReg.SetValue(strNames(index), "")
            '            End Select
            '        Next
            '    End If

            '    'brReportView.ShowPrintPreviewDialog()
            '    brReportView.Print()
            'Catch ex As Exception
            '    brReportView.ShowPrintPreviewDialog()
            'End Try

            Try
                brReportView.ShowPrintPreviewDialog()
            Catch ex As Exception
                brReportView.ShowPrintPreviewDialog()
            End Try
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnPrint_Click", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

End Class