﻿Imports eZeeCommonLib
Imports System.DirectoryServices

Public Class clsMain

    Private mstrModuleName As String = "clsMain"
    Private mblnLicCheck As Boolean = True

    'Anjan (12 Oct 2011)-Start
    'ENHANCEMENT : If database on client machine is not there , it will connect to server.
    Dim intIsClientCheck As Integer = 0
    Dim strMessage As String = String.Empty
    'Anjan (12 Oct 2011)-End 


    Public WriteOnly Property _LicCheck()
        Set(ByVal value)
            mblnLicCheck = value
        End Set
    End Property


    Public Function DO_Main(ByVal ApplicationType As enArutiApplicatinType) As Boolean
        'Anjan [05 may 2015] -- Start
        'ENHANCEMENT : exempting demo screen,requested by Andrew.
        'Dim arrGroupName() As String = {"AAM RESOURCES", "HUMAN RESOURCE MANAGEMENT OFFICE", "CROWN PAINTS ( K) LTD"} 'Put group name in captial letters only
        Dim arrGroupName() As String = {"AAM RESOURCES", "HUMAN RESOURCE MANAGEMENT OFFICE"} 'Put group name in captial letters only
        'Anjan [05 may 2015] -- End
        Try

            gApplicationType = ApplicationType

            'Sandeep [ 01 FEB 2011 ] -- START
            Dim objAppSettings As New clsApplicationSettings
            Dim blnIsCleared As Boolean = False
            blnIsCleared = objAppSettings._ClearCompatibility

            'Anjan (12 Oct 2011)-Start
            'ENHANCEMENT : If database on client machine is not there , it will connect to server.
            intIsClientCheck = objAppSettings._IsClient
            'Anjan (12 Oct 2011)-End 

            objAppSettings = Nothing
            'Sandeep [ 01 FEB 2011 ] -- END 


            'Anjan (12 Oct 2011)-Start
            'ENHANCEMENT : If database on client machine is not there , it will connect to server.

            'Dim blnFail As Boolean = True
            'While blnFail
            '    Try
            '        Call connect()
            '        blnFail = False

            '    Catch ex As Exception
            '        DisplayError.Show("-1", ex.Message, "DO_Main", mstrModuleName)
            '        Dim objfrmServerSettings As New frmClientServerSettings
            '        objfrmServerSettings.ShowDialog()
            '    End Try
            'End While
10:
            Dim objSettings As New clsGeneralSettings
            objSettings._Section = gApplicationType.ToString

            If intIsClientCheck = 0 Then
                If connect() = False Then
                    eZeeMsgBox.Show(strMessage, enMsgBoxStyle.Information)
                    Exit Function
                End If
            Else
                If intIsClientCheck = 1 AndAlso objSettings._ServerName <> "" Then

                    'Call connect()
                    If connect() = False Then
                        eZeeMsgBox.Show(strMessage, enMsgBoxStyle.Information)

                        'S.SANDEEP [21-NOV-2017] -- START
                        If intIsClientCheck = 1 Then
                            Dim frm As New frmClientServerSettings
                            frm.ShowDialog()
                        End If
                        'S.SANDEEP [21-NOV-2017] -- END

                        Exit Function
                    End If
                Else
                    Dim objfrmServerSettings As New frmClientServerSettings
                    objfrmServerSettings.ShowDialog()
                    GoTo 10
                End If
            End If
            'Anjan (12 Oct 2011)-End 


            'eZeeCommonLib.eZeeDatabase.change_database("PMS_NextGen")

            'Set Global Param of DataOperation
            'gobjParam = New clsParameter


            'Anjan (12 Oct 2011)-Start
            'ENHANCEMENT : If database on client machine is not there , it will connect to server.
            If intIsClientCheck = 0 Then
            If Not CheckDatabase() Then
                Return False
            End If
            End If

            'Anjan (12 Oct 2011)-End


            eZeeCommonLib.eZeeDatabase.change_database("hrmsConfiguration")
            gobjConfigOptions = New clsConfigOptions


            'Version checking and then update database
            ' If intIsClientCheck = 0 Then 
            If Not clsDataBaseUpdate.DataUpdate Then
                Return False
            End If
            'End If

            'Pinkal (03-Apr-2017) -- Start
            'Enhancement - Working On Active directory Changes for PACRA.
            'UpdateForADUser()
            'Pinkal (03-Apr-2017) -- End

            'Sandeep | 17 JAN 2011 | -- START
            gobjLocalization = New clsLocalization(gApplicationType)
            'Sandeep | 17 JAN 2011 | -- END 


            'Sandeep [ 15 DEC 2010 ] -- Start
            Dim tgs As Vintasoft.Twain.TwainGlobalSettings = New Vintasoft.Twain.TwainGlobalSettings()
            tgs.Register("Vipul Kapoor", "vipul@ezeefrontdesk.com", "W1AZdlKxOJ1qPpSTohJ4JT+LzwXOL2mtA94/xPT+slTGqcCXjOdk1qjPAWc0tcDOAgyc0JSIIVuZ+YUsp3+8vg5iOtbA6a7exCgin6HMJh9WyZNg92qzJVozESmn3A5yurbM4E1+EDgHigwLF3ZOZ45xN43aHjmSLdBtKUIeww/g")
            'Sandeep [ 15 DEC 2010 ] -- End


            ArtLic._Object = New ArutiLic(False)

            Dim objGroupMaster As New clsGroup_Master
            objGroupMaster._Groupunkid = 1

            If intIsClientCheck = 0 Then
                If mblnLicCheck = True Then
                    If ConfigParameter._Object.GetKeyValue(Nothing, "Emp") = "" Then
                        ArtLic._Object.HotelName = objGroupMaster._Groupname
                    End If

                    If ArtLic._Object.IsNewLicenseActivated Then
                        If ArtLic._Object.IsExpire Then
                            '<TODO> -- TO BE REMOVED ONCE WORK IS DONE FOR NMB -- START
                            'S.SANDEEP |24-NOV-2020| -- START
                            'ISSUE/ENHANCEMENT : TEMP WORK FOR NMB
                            'If Today.Date > eZeeDate.convertDate(acore32.core.HD) Then
                            '    eZeeMsgBox.Show("Your demo period is over. Please contact " & acore32.core.SupportTeam & ".")
                            '    Return False
                            'End If

                            'Dim frm As New frmDemoNotification
                            'If frm.ShowDialog() = Windows.Forms.DialogResult.Cancel Then
                            '    Return False
                            'End If

                            If objGroupMaster._Groupname.ToString().ToUpper() <> "NMB PLC" Then
                            If Today.Date > eZeeDate.convertDate(acore32.core.HD) Then
                                eZeeMsgBox.Show("Your demo period is over. Please contact " & acore32.core.SupportTeam & ".")
                                Return False
                            End If

                            Dim frm As New frmDemoNotification
                            If frm.ShowDialog() = Windows.Forms.DialogResult.Cancel Then
                                Return False
                            End If
                            End If
                            'S.SANDEEP |24-NOV-2020| -- END
                            '<TODO> -- TO BE REMOVED ONCE WORK IS DONE FOR NMB -- END

                            
                        Else
                            '<TODO> -- TO BE REMOVED ONCE WORK IS DONE FOR NMB -- START
                            'S.SANDEEP |24-NOV-2020| -- START
                            'ISSUE/ENHANCEMENT : TEMP WORK FOR NMB
                            'If ArtLic._Object.IsDemo Then


                            '    'Anjan [05 may 2015] -- Start
                            '    'ENHANCEMENT : exempting demo screen,requested by Andrew.
                            '    '*** checking for kenya AAM Resources
                            '    Dim objGroupMaster As New clsGroup_Master
                            '    objGroupMaster._Groupunkid = 1
                            '    ArtLic._Object.HotelName = objGroupMaster._Groupname
                            '    'Anjan [05 may 2015] -- End


                            '    If Today.Date > eZeeDate.convertDate(acore32.core.HD) Then
                            '        eZeeMsgBox.Show("Your demo period is over. ")
                            '        Return False
                            '    End If

                            '    'Pinkal (30-Jul-2013) -- Start
                            '    'Enhancement : TRA Changes
                            '    If arrGroupName.Contains(objGroupMaster._Groupname.ToUpper) = False Then 'Anjan [05 may 2015] -- Start- ENHANCEMENT : exempting demo screen,requested by Andrew.
                            '        If objSettings._Section <> enArutiApplicatinType.Aruti_TimeSheet.ToString() Then
                            '            Dim frm As New frmDemoNotification
                            '            If frm.ShowDialog() = Windows.Forms.DialogResult.Cancel Then
                            '                Return False
                            '            End If
                            '        End If
                            '    End If
                            '    'Pinkal (30-Jul-2013) -- End


                            'End If

                            If objGroupMaster._Groupname.ToString().ToUpper() <> "NMB PLC" Then
                            If ArtLic._Object.IsDemo Then


                                'Anjan [05 may 2015] -- Start
                                'ENHANCEMENT : exempting demo screen,requested by Andrew.
                                '*** checking for kenya AAM Resources
                                    'Dim objGroupMaster As New clsGroup_Master
                                    'objGroupMaster._Groupunkid = 1
                                ArtLic._Object.HotelName = objGroupMaster._Groupname
                                'Anjan [05 may 2015] -- End


                                If Today.Date > eZeeDate.convertDate(acore32.core.HD) Then
                                    eZeeMsgBox.Show("Your demo period is over. ")
                                    Return False
                                End If

                                'Pinkal (30-Jul-2013) -- Start
                                'Enhancement : TRA Changes
                                If arrGroupName.Contains(objGroupMaster._Groupname.ToUpper) = False Then 'Anjan [05 may 2015] -- Start- ENHANCEMENT : exempting demo screen,requested by Andrew.
                                    If objSettings._Section <> enArutiApplicatinType.Aruti_TimeSheet.ToString() Then
                                        Dim frm As New frmDemoNotification
                                        If frm.ShowDialog() = Windows.Forms.DialogResult.Cancel Then
                                            Return False
                                        End If
                                    End If
                                End If
                                'Pinkal (30-Jul-2013) -- End


                            End If
                            End If

                            
                            'S.SANDEEP |24-NOV-2020| -- END
                            '<TODO> -- TO BE REMOVED ONCE WORK IS DONE FOR NMB -- END

                            

                            End If
                    End If
                End If
            Else
                '<TODO> -- TO BE REMOVED ONCE WORK IS DONE FOR NMB -- START
                'S.SANDEEP |24-NOV-2020| -- START
                'ISSUE/ENHANCEMENT : TEMP WORK FOR NMB
                'Dim objGroupMaster As New clsGroup_Master  ' demo checking
                'objGroupMaster._Groupunkid = 1
                'ArtLic._Object.HotelName = objGroupMaster._Groupname

                ''Sohail (08 May 2013) -- Start
                ''License - ENHANCEMENT
                'If ConfigParameter._Object._IsArutiDemo Then 'Check if Licence is not activated (If it is Demo)
                '    If Today.Date > eZeeDate.convertDate(acore32.core.HD) Then
                '        eZeeMsgBox.Show("Your demo period is over. Please contact " & acore32.core.SupportTeam & ".")
                '        Return False
                '    End If
                '    If arrGroupName.Contains(objGroupMaster._Groupname.ToUpper) = False Then 'Anjan [05 may 2015] -- Start- ENHANCEMENT : exempting demo screen,requested by Andrew.
                '        Dim frm As New frmDemoNotification
                '        If frm.ShowDialog() = Windows.Forms.DialogResult.Cancel Then
                '            Return False
                '        End If
                '    End If

                'End If
                ''Sohail (08 May 2013) -- End
                If objGroupMaster._Groupname.ToString().ToUpper() <> "NMB PLC" Then
                    'Dim objGroupMaster As New clsGroup_Master  ' demo checking
                    'objGroupMaster._Groupunkid = 1
                ArtLic._Object.HotelName = objGroupMaster._Groupname

                'Sohail (08 May 2013) -- Start
                'License - ENHANCEMENT
                If ConfigParameter._Object._IsArutiDemo Then 'Check if Licence is not activated (If it is Demo)
                    If Today.Date > eZeeDate.convertDate(acore32.core.HD) Then
                        eZeeMsgBox.Show("Your demo period is over. Please contact " & acore32.core.SupportTeam & ".")
                        Return False
                    End If
                    If arrGroupName.Contains(objGroupMaster._Groupname.ToUpper) = False Then 'Anjan [05 may 2015] -- Start- ENHANCEMENT : exempting demo screen,requested by Andrew.
                        Dim frm As New frmDemoNotification
                        If frm.ShowDialog() = Windows.Forms.DialogResult.Cancel Then
                            Return False
                        End If
                    End If

                End If
                'Sohail (08 May 2013) -- End
                End If
                'S.SANDEEP |24-NOV-2020| -- END
                '<TODO> -- TO BE REMOVED ONCE WORK IS DONE FOR NMB -- END

                
            End If



                'S.SANDEEP [ 09 AUG 2011 ] -- START
                'ENHANCEMENT : AUTO BACKUP & AUTO UPDATE
                If acore32.core.iCore = acore32.enCore.ARUTI Then
                    If gApplicationType = enArutiApplicatinType.Aruti_Configuration Or gApplicationType = enArutiApplicatinType.Aruti_Payroll Or gApplicationType = enArutiApplicatinType.Aruti_TimeSheet Then

                        'S.SANDEEP [ 11 APRIL 2012 ] -- START
                        'ENHANCEMENT : TRA CHANGES {WEB CHANGES}
                        Dim objConfig As New clsConfigOptions
                        Dim blnValue As Boolean = False
                        blnValue = objConfig.Get_Auto_Update_Setting(-200)  '-200 IS COMPANYUNKID FOR AUTO UPDATE TRACKING
                        objConfig = Nothing
                        'S.SANDEEP [ 11 APRIL 2012 ] -- END

                        'Anjan [16 September 2014] -- Start
                        'ENHANCEMENT : Implementing autoupdate issue where Workingmode is set as 2 in aruti.ini.
                        'Reason: for issue in aruti.ini WorkingMode set as 2 then its not updating client machine so now checking is set on registry IsClient=1
                        'If blnValue = True And (General_Settings._Object._WorkingMode.ToUpper = enWorkingMode.Server.ToString.ToUpper Or General_Settings._Object._WorkingMode.ToUpper.Replace(" ", "") = enWorkingMode.StandAlone.ToString.ToUpper) Then
                        If blnValue = True And AppSettings._Object._IsClient = 0 Then 'checking for server machine only
                            'Anjan [16 September 2014] -- End



                            'If clsDataBaseUpdate.isAMCPaid Then  <TODO : OPEN THIS COMMENT WHEN AMC IS IMPLEMENTED. >
                            Dim strLink As String = clsDataBaseUpdate.getNewVersionLink
                            If strLink.Trim <> "" Then
                                If eZeeMsgBox.Show("There is new version available for download. are you ready for update application? " & vbCrLf & "If you select No then you can continue running old version and won't be able to use new feature and enhancements.", enMsgBoxStyle.Question + enMsgBoxStyle.YesNo) = Windows.Forms.DialogResult.Yes Then
                                    Dim frm As New frmDownloader
                                    frm._URL = strLink
                                    frm.ShowDialog()
                                    If frm._Downloaded Then
                                        Dim strApplicationExe As String = AppSettings._Object._ApplicationPath
                                        Select Case gApplicationType
                                            Case enArutiApplicatinType.Aruti_Configuration
                                                strApplicationExe = System.IO.Path.Combine(strApplicationExe, "ArutiConfiguration.exe")
                                            Case enArutiApplicatinType.Aruti_Payroll
                                                strApplicationExe = System.IO.Path.Combine(strApplicationExe, "Aruti.exe")
                                            Case enArutiApplicatinType.Aruti_TimeSheet
                                                strApplicationExe = System.IO.Path.Combine(strApplicationExe, "ArutiTimeSheet.exe")
                                        End Select
                                        '< TO DO for Server to be updated automatically if set in configuration.>
                                        'If System.IO.File.Exists(System.IO.Path.Combine(System.IO.Path.GetDirectoryName(frm._DownloadFilePath), "ClientSP.exe")) Then System.IO.File.Delete(System.IO.Path.Combine(System.IO.Path.GetDirectoryName(frm._DownloadFilePath), "ClientSP.exe"))
                                        'System.IO.File.Copy(frm._DownloadFilePath, System.IO.Path.Combine(System.IO.Path.GetDirectoryName(frm._DownloadFilePath), "ClientSP.exe"))
                                        Shell(frm._DownloadFilePath, AppWinStyle.NormalFocus)
                                        Return False
                                    Else
                                        frm.Dispose()
                                        Return False
                                    End If
                                End If
                            End If
                            'End If
                        End If
                    End If
                End If
                'S.SANDEEP [ 09 AUG 2011 ] -- END

                'Sohail (02 Nov 2013) -- Start
                'TRA - ENHANCEMENT
                Try
                    clsBioStar.BS_InitSDK()
                Catch ex As Exception

                End Try
                'Sohail (02 Nov 2013) -- End

                'Sohail (26 Feb 2016) -- Start
                'Enhancement - Email Payslip to have password protection (request from KBC and CCK) (41# KBC & Kenya Project Comments List.xls)
                EmbeddedAssembly.Load("Aruti.Data.PdfSharp.dll", "PdfSharp.dll")
            'Sohail (11 Dec 2017) -- Start
            EmbeddedAssembly.Load("Aruti.Data.Microsoft.Exchange.WebServices.dll", "Microsoft.Exchange.WebServices.dll")
            'Sohail (11 Dec 2017) -- End

            'S.SANDEEP [12-Jan-2018] -- START
            'ISSUE/ENHANCEMENT : REF-ID # 0001843
            EmbeddedAssembly.Load("Aruti.Data.DocumentFormat.OpenXml.dll", "DocumentFormat.OpenXml.dll")
            'S.SANDEEP [12-Jan-2018] -- END

            'S.SANDEEP |09-JAN-2020| -- START
            'ISSUE/ENHANCEMENT : ARUTI - ORBIT INTEGRATION
            EmbeddedAssembly.Load("Aruti.Data.Newtonsoft.Json.dll", "Newtonsoft.Json.dll")
            'S.SANDEEP |09-JAN-2020| -- END

            'S.SANDEEP |18-JUN-2021| -- START
            'ISSUE/ENHANCEMENT : Kenya Reports Changes (Statutory)
            EmbeddedAssembly.Load("Aruti.Data.EPPlus.dll", "EPPlus.dll")
            'S.SANDEEP |18-JUN-2021| -- END


                AddHandler AppDomain.CurrentDomain.AssemblyResolve, AddressOf CurrentDomain_AssemblyResolve
                'Sohail (26 Feb  2016) -- End

                Return True

        Catch ex As System.Exception
            Throw New Exception(ex.Message & vbCrLf & "[DO_Main," & mstrModuleName & "]")
        End Try
    End Function

    'Sohail (26 Feb 2016) -- Start
    'Enhancement - Email Payslip to have password protection (request from KBC and CCK) (41# KBC & Kenya Project Comments List.xls)
    Private Shared Function CurrentDomain_AssemblyResolve(ByVal sender As Object, ByVal args As ResolveEventArgs) As System.Reflection.Assembly
        Return EmbeddedAssembly.[Get](args.Name)
    End Function
    'Sohail (26 Feb  2016) -- End

    ''' <summary>
    ''' For create Database connection.
    ''' </summary>
    Public Function connect() As Boolean
        Dim objDatabaseConn As New eZeeCommonLib.eZeeDatabase
        Try
            Dim objGSettings As New clsGeneralSettings
            objGSettings._Section = gApplicationType.ToString
            'S.SANDEEP [21-NOV-2017] -- START
            'objDatabaseConn.ServerName = IIf(objGSettings._ServerName = "", "(Local)", objGSettings._ServerName)
            Dim strServerName As String = String.Empty
            If intIsClientCheck = 1 Then
                If objGSettings._SQLPortNumber > 0 Then
                    strServerName = IIf(objGSettings._ServerName = "", "(Local)", objGSettings._ServerName) & "," & objGSettings._SQLPortNumber.ToString()
                Else
                    strServerName = IIf(objGSettings._ServerName = "", "(Local)", objGSettings._ServerName)
                End If
            Else
                strServerName = IIf(objGSettings._ServerName = "", "(Local)", objGSettings._ServerName)
            End If
            objDatabaseConn.ServerName = strServerName
            'S.SANDEEP [21-NOV-2017] -- END
            objDatabaseConn.Connect()
            Return True
        Catch ex As Exception
            strMessage = Language.getMessage(mstrModuleName, 111, "Server Name or IP Address was not found.Please contact your system Administrator.")
            'Throw New Exception(Language.getMessage(mstrModuleName, 111, "Server Name of IP Address was not found.Please contact your system Administrator.") & vbCrLf & "[connect," & mstrModuleName & "]")
        Finally
            objDatabaseConn = Nothing
        End Try
    End Function

   'Pinkal (12-Oct-2011) -- Start
    'Enhancement : TRA Changes

    Public Sub UpdateForADUser()
        Try
            Const ADS_UF_ACCOUNTDISABLE As Integer = &H2
            Dim objOption As New clsPassowdOptions
            Dim drOption As DataRow() = objOption._DataTable.Select("passwdoptionid = " & enPasswordOption.USER_LOGIN_MODE)
            If drOption.Length > 0 Then

                If objOption._UserLogingModeId <> enAuthenticationMode.BASIC_AUTHENTICATION Then


                    'Pinkal (03-Apr-2017) -- Start
                    'Enhancement - Working On Active directory Changes for PACRA.

                    'Dim objGSettings As New clsGeneralSettings
                    'objGSettings._Section = gApplicationType.ToString

                    'If objGSettings._AD_ServerIP.Trim.Length <= 0 Then
                    '    eZeeMsgBox.Show(Language.getMessage("frmImportADUsers", 6, "Active directory server address cannot be blank. Please set the active directroy server address."), enMsgBoxStyle.Information)
                    '    Exit Sub
                    'End If
                    Dim mstrADIPAddress As String = ""
                    Dim mstrADDomainUser As String = ""
                    Dim mstrADDomainUserPwd As String = ""
                    Dim objConfig As New clsConfigOptions
                    objConfig.IsValue_Changed("ADIPAddress", "-999", mstrADIPAddress)
                    objConfig.IsValue_Changed("ADDomainUser", "-999", mstrADDomainUser)
                    objConfig.IsValue_Changed("ADDomainUserPwd", "-999", mstrADDomainUserPwd)
                    objConfig = Nothing

                    If mstrADIPAddress.Trim.Length <= 0 Then
                        eZeeMsgBox.Show(Language.getMessage("frmImportADUsers", 6, "Active directory server address cannot be blank. Please set the active directroy server address."), enMsgBoxStyle.Information)
                        Exit Sub
                    End If

                    'Pinkal (03-Apr-2017) -- End

                    Dim objSearch As New DirectorySearcher()

                    'Pinkal (03-Apr-2017) -- Start
                    'Enhancement - Working On Active directory Changes for PACRA.
                    'objSearch.SearchRoot = New DirectoryEntry("LDAP://" & objGSettings._AD_ServerIP)

                    If mstrADDomainUser.Trim.Length > 0 AndAlso mstrADDomainUserPwd.Trim.Length > 0 Then
                        objSearch.SearchRoot = New DirectoryEntry("LDAP://" & mstrADIPAddress, mstrADDomainUser, clsSecurity.Decrypt(mstrADDomainUserPwd, "ezee"))
                    Else
                        objSearch.SearchRoot = New DirectoryEntry("LDAP://" & mstrADIPAddress)
                    End If

                    'Pinkal (03-Apr-2017) -- End

                    objSearch.Filter = "(&(objectCategory=user))"
                    objSearch.SearchScope = SearchScope.Subtree
                    objSearch.ReferralChasing = ReferralChasingOption.All
                    objSearch.Sort.Direction = SortDirection.Ascending
                    Dim srcResult As SearchResultCollection = objSearch.FindAll

                    If srcResult.Count > 0 Then

                        Dim objUser As New clsUserAddEdit
                        Dim dsList As DataSet = objUser.GetList("List")



                        For Each sr As SearchResult In srcResult

                            Dim objDirectory As DirectoryEntry = sr.GetDirectoryEntry()
                            Dim drRow As DataRow()

                            If objDirectory.Properties("SamAccountName").Value IsNot Nothing Then

                                drRow = dsList.Tables(0).Select("username='" & objDirectory.Properties("SamAccountName").Value & "'")

                                If drRow.Length <= 0 Then Continue For

                                objUser._Userunkid = CInt(drRow(0)("userunkid"))


                                'START FOR SET DISABLE USER ACCOUNT IS  INACTIVE

                                If objDirectory.Properties("userAccountControl").Value IsNot Nothing Then

                                    Dim flags As Integer = Convert.ToInt32(objDirectory.Properties("userAccountControl").Value)
                                    If Convert.ToBoolean(flags And ADS_UF_ACCOUNTDISABLE) Then
                                        objUser._Isactive = False
                                    End If

                                End If

                                'END FOR SET DISABLE USER ACCOUNT IS  INACTIVE


                                If objDirectory.Properties("distinguishedname").Value IsNot Nothing Then

                                    Dim DCName As String = ""

                                    Dim strDomain As String() = objDirectory.Properties("distinguishedname").Value.ToString().Split(CChar(","))

                                    If strDomain.Length > 0 Then
                                        For i As Integer = 0 To strDomain.Length - 1

                                            If strDomain(i).Contains("DC=") Then
                                                DCName = strDomain(i).Replace("DC=", "")
                                                Exit For
                                            End If
                                        Next
                                    End If

                                    objUser._UserDomain = DCName

                                End If


                                If objDirectory.Properties("givenName").Value IsNot Nothing Then
                                    objUser._Firstname = objDirectory.Properties("givenName").Value.ToString()
                                End If

                                If objDirectory.Properties("sn").Value IsNot Nothing Then
                                    objUser._Lastname = objDirectory.Properties("sn").Value.ToString()
                                End If

                                If objDirectory.Properties("mail").Value IsNot Nothing Then
                                    objUser._Email = objDirectory.Properties("mail").Value.ToString()
                                Else
                                    objUser._Email = ""
                                End If

                                If objDirectory.Properties("telephoneNumber").Value IsNot Nothing Then
                                    objUser._Phone = objDirectory.Properties("telephoneNumber").Value.ToString()
                                Else
                                    objUser._Phone = ""
                                End If

                                If objDirectory.Properties("streetAddress").Value IsNot Nothing Then
                                    objUser._Address1 = objDirectory.Properties("streetAddress").Value.ToString()
                                Else
                                    objUser._Address1 = ""
                                End If

                                Dim mstrAddress2 As String = ""

                                If objDirectory.Properties("l").Value IsNot Nothing Then
                                    mstrAddress2 = objDirectory.Properties("l").Value.ToString()
                                Else
                                    mstrAddress2 = ""
                                End If

                                If objDirectory.Properties("postalCode").Value IsNot Nothing Then
                                    mstrAddress2 &= " - " & objDirectory.Properties("postalCode").Value.ToString() & " , "
                                Else
                                    mstrAddress2 &= ""
                                End If

                                If objDirectory.Properties("st").Value IsNot Nothing Then
                                    mstrAddress2 &= objDirectory.Properties("st").Value.ToString() & " , "
                                Else
                                    mstrAddress2 &= ""
                                End If

                                If objDirectory.Properties("co").Value IsNot Nothing Then
                                    mstrAddress2 &= objDirectory.Properties("co").Value.ToString()
                                Else
                                    mstrAddress2 &= ""
                                End If

                                objUser._Address2 = mstrAddress2

                                Dim blnFlag As Boolean = objUser.Update(True)

                                If blnFlag = False And objUser._Message <> "" Then
                                    eZeeMsgBox.Show(objUser._Message, enMsgBoxStyle.Information)
                                    Continue For
                                End If

                                dsList.Tables(0).Rows.Remove(drRow(0))

                            End If

                        Next

                        ' START FOR DISABLE THE USER WHICH DELETED IN ACTIVE DIRECTORY

                        If dsList.Tables(0).Rows.Count > 0 Then

                            Dim dRow As DataRow() = dsList.Tables(0).Select("userunkid <> 1")

                            If dRow.Length > 0 Then

                                For i As Integer = 0 To dRow.Length - 1

                                    objUser._Userunkid = CInt(dRow(i)("userunkid"))
                                    objUser._Isactive = False

                                    Dim blnFlag As Boolean = objUser.Update(True)

                                    If blnFlag = False And objUser._Message <> "" Then
                                        eZeeMsgBox.Show(objUser._Message, enMsgBoxStyle.Information)
                                        Continue For
                                    End If

                                Next

                            End If

                        End If

                        ' END FOR DISABLE THE USER WHICH DELETED IN ACTIVE DIRECTORY

                    End If

                End If

            End If
        Catch ex As Exception
            Dim strMessage As String = Language.getMessage("frmImportADUsers", 5, " Or does not contains the Active Directory Users. Please set correct server to use this feature.")
            eZeeMsgBox.Show(ex.Message & strMessage, enMsgBoxStyle.Information)
        End Try
    End Sub

    'Pinkal (12-Oct-2011) -- End


End Class
