﻿'************************************************************************************************************************************
'Class Name : clsDiscipline_file_tran.vb
'Purpose    :
'Date       :30-MAY-2011
'Written By :Sandeep J. Sharma
'Modified   :
'************************************************************************************************************************************

#Region " Imports "

Imports eZeeCommonLib
Imports System.Windows.Forms
Imports eZee.Common.eZeeForm

#End Region

''' <summary>
''' Purpose: 
''' Developer: Sandeep J. Sharma
''' </summary>

Public Class clsDiscipline_file_tran

#Region " Private Variables "

    Private Shared mstrModuleName As String = "clsDiscipline_file_tran"
    Private mintDisciplinefiletranunkid As Integer = 0
    Private mstrMessage As String = ""
    Private mintDisciplinefileunkid As Integer = 0
    Private mdtTran As DataTable
    Private mobjDataOperation As clsDataOperation

    Private mstrWebFormName As String = String.Empty
    Private mstrWebClientIP As String = String.Empty
    Private mstrWebHostName As String = String.Empty
    Private objDocument As New clsScan_Attach_Documents

#End Region

#Region " Properties "
'S.SANDEEP [28-May-2018] -- START 
'ISSUE/ENHANCEMENT : {Audit Trails} 
Private mstrFormName As String = String.Empty
Public WriteOnly Property _FormName() As String 
Set(ByVal value As String) 
mstrFormName = value 
End Set 
End Property 
 
Private mstrClientIP As String = ""
Public WriteOnly Property _ClientIP() As String 
Set(ByVal value As String) 
mstrClientIP = value 
End Set 
End Property 
 
Private mstrHostName As String = ""
Public WriteOnly Property _HostName() As String 
Set(ByVal value As String) 
mstrHostName = value 
End Set 
End Property 
 
Private mblnIsWeb As Boolean = False
Public WriteOnly Property _FromWeb() As Boolean 
Set(ByVal value As Boolean) 
mblnIsWeb = value 
End Set 
End Property 
 
    Private mintAuditUserId As Integer = 0
    Public WriteOnly Property _AuditUserId() As Integer
        Set(ByVal value As Integer)
            mintAuditUserId = value
        End Set
    End Property

    Private mintCompanyUnkid As Integer = 0
    Public WriteOnly Property _CompanyUnkid() As Integer
        Set(ByVal value As Integer)
            mintCompanyUnkid = value
        End Set
    End Property
 
Private mdtAuditDate As DateTime = Now
Public WriteOnly Property _AuditDate() As DateTime 
Set(ByVal value As DateTime) 
mdtAuditDate = value 
End Set 
End Property 
 
Private xDataOpr As clsDataOperation = Nothing
Public Property _xDataOpr() As clsDataOperation 
Get 
Return xDataOpr 
End Get 
Set(ByVal value As clsDataOperation) 
xDataOpr = value 
End Set 
End Property 
 
Private mintLoginEmployeeunkid As Integer = 0
Public Property _LoginEmployeeunkid() As Integer 
Get 
Return mintLoginEmployeeunkid 
End Get 
Set(ByVal value As Integer) 
mintLoginEmployeeunkid = value 
End Set 
End Property 
 
Private mintVoidlogingemployeeunkid As Integer=0
Public Property _Voidloginemployeeunkid() As Integer 
Get 
Return mintVoidlogingemployeeunkid 
End Get 
Set(ByVal value As Integer) 
mintVoidlogingemployeeunkid = value 
End Set 
End Property 
'S.SANDEEP [28-May-2018] -- END 
    Public WriteOnly Property _objDataOperation() As clsDataOperation
        Set(ByVal value As clsDataOperation)
            mobjDataOperation = value
        End Set
    End Property

    Public Property _Disciplinefileunkid() As Integer
        Get
            Return mintDisciplinefileunkid
        End Get
        Set(ByVal value As Integer)
            mintDisciplinefileunkid = value
            Call GetChargesCount()
        End Set
    End Property

    Public Property _ChargesTable() As DataTable
        Get
            Return mdtTran
        End Get
        Set(ByVal value As DataTable)
            mdtTran = value
        End Set
    End Property

    'Public WriteOnly Property _WebFormName() As String
    '    Set(ByVal value As String)
    '        mstrWebFormName = value
    '    End Set
    'End Property

    'Public WriteOnly Property _WebClientIP() As String
    '    Set(ByVal value As String)
    '        mstrWebClientIP = value
    '    End Set
    'End Property

    'Public WriteOnly Property _WebHostName() As String
    '    Set(ByVal value As String)
    '        mstrWebHostName = value
    '    End Set
    'End Property

#End Region

#Region " Constructor "

    Public Sub New()
        mdtTran = New DataTable("Charges")
        Dim dCol As DataColumn
        Try
            With mdtTran
                dCol = New DataColumn
                With dCol
                    .ColumnName = "charge_count"
                    .DataType = GetType(Int32)
                    .DefaultValue = 0
                End With
                .Columns.Add(dCol)

                dCol = New DataColumn
                With dCol
                    .ColumnName = "disciplinefiletranunkid"
                    .DataType = GetType(Int32)
                    .DefaultValue = 0
                End With
                .Columns.Add(dCol)

                dCol = New DataColumn
                With dCol
                    .ColumnName = "disciplinefileunkid"
                    .DataType = GetType(Int32)
                    .DefaultValue = 0
                End With
                .Columns.Add(dCol)

                dCol = New DataColumn
                With dCol
                    .ColumnName = "incident_description"
                    .DataType = GetType(String)
                    .DefaultValue = ""
                End With
                .Columns.Add(dCol)

                dCol = New DataColumn
                With dCol
                    .ColumnName = "offencecategoryunkid"
                    .DataType = GetType(Int32)
                    .DefaultValue = 0
                End With
                .Columns.Add(dCol)

                dCol = New DataColumn
                With dCol
                    .ColumnName = "offenceunkid"
                    .DataType = GetType(Int32)
                    .DefaultValue = 0
                End With
                .Columns.Add(dCol)

                dCol = New DataColumn
                With dCol
                    .ColumnName = "isvoid"
                    .DataType = GetType(Boolean)
                    .DefaultValue = False
                End With
                .Columns.Add(dCol)

                dCol = New DataColumn
                With dCol
                    .ColumnName = "voiduserunkid"
                    .DataType = GetType(Int32)
                    .DefaultValue = 0
                End With
                .Columns.Add(dCol)

                dCol = New DataColumn
                With dCol
                    .ColumnName = "voidreason"
                    .DataType = GetType(String)
                    .DefaultValue = ""
                End With
                .Columns.Add(dCol)

                dCol = New DataColumn
                With dCol
                    .ColumnName = "voiddatetime"
                    .DataType = GetType(DateTime)
                End With
                .Columns.Add(dCol)

                dCol = New DataColumn
                With dCol
                    .ColumnName = "charge_category"
                    .DataType = GetType(String)
                    .DefaultValue = ""
                End With
                .Columns.Add(dCol)

                dCol = New DataColumn
                With dCol
                    .ColumnName = "charge_descr"
                    .DataType = GetType(String)
                    .DefaultValue = ""
                End With
                .Columns.Add(dCol)

                dCol = New DataColumn
                With dCol
                    .ColumnName = "charge_severity"
                    .DataType = GetType(Int32)
                    .DefaultValue = 0
                End With
                .Columns.Add(dCol)

                dCol = New DataColumn
                With dCol
                    .ColumnName = "AUD"
                    .DataType = GetType(String)
                    .DefaultValue = ""
                End With
                .Columns.Add(dCol)

                dCol = New DataColumn
                With dCol
                    .ColumnName = "GUID"
                    .DataType = GetType(String)
                    .DefaultValue = ""
                End With
                .Columns.Add(dCol)

                dCol = New DataColumn
                With dCol
                    .ColumnName = "display_incident"
                    .DataType = GetType(System.String)
                    .DefaultValue = ""
                End With
                .Columns.Add(dCol)

                dCol = New DataColumn
                With dCol
                    .ColumnName = "count_statusid"
                    .DataType = GetType(System.Int32)
                    .DefaultValue = 0
                End With
                .Columns.Add(dCol)

                dCol = New DataColumn
                With dCol
                    .ColumnName = "disciplineproceedingmasterunkid"
                    .DataType = GetType(System.Int32)
                    .DefaultValue = 0
                End With
                .Columns.Add(dCol)

                dCol = New DataColumn
                With dCol
                    .ColumnName = "responsetypeunkid"
                    .DataType = GetType(System.Int32)
                    .DefaultValue = 0
                End With
                .Columns.Add(dCol)


                dCol = New DataColumn
                With dCol
                    .ColumnName = "response_date"
                    .DataType = GetType(System.DateTime)
                    .DefaultValue = DBNull.Value
                End With
                .Columns.Add(dCol)


                dCol = New DataColumn
                With dCol
                    .ColumnName = "response_remark"
                    .DataType = GetType(System.String)
                    .DefaultValue = ""
                End With
                .Columns.Add(dCol)

                dCol = New DataColumn
                With dCol
                    .ColumnName = "response_type"
                    .DataType = GetType(String)
                    .DefaultValue = ""
                End With
                .Columns.Add(dCol)

                'S.SANDEEP |11-NOV-2019| -- START
                'ISSUE/ENHANCEMENT : NMB DISCIPLINARY UAT COMMENTS
                dCol = New DataColumn
                With dCol
                    .ColumnName = "isfromess"
                    .DataType = GetType(Boolean)
                    .DefaultValue = False
                End With
                .Columns.Add(dCol)
                'S.SANDEEP |11-NOV-2019| -- END


                'Pinkal (19-Dec-2020) -- Start
                'Enhancement  -  Working on Discipline module for NMB.
                dCol = New DataColumn
                With dCol
                    .ColumnName = "contrarytounkid"
                    .DataType = GetType(Integer)
                    .DefaultValue = 0
                End With
                .Columns.Add(dCol)

                dCol = New DataColumn
                With dCol
                    .ColumnName = "contraryto"
                    .DataType = GetType(String)
                    .DefaultValue = ""
                End With
                .Columns.Add(dCol)
                'Pinkal (19-Dec-2020) -- End


            End With
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: New; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

#Region " Private Methods "

    Private Sub GetChargesCount()
        Dim strQ As String = ""
        Dim dsList As New DataSet
        Dim exForce As Exception
        Dim objDataOperation As clsDataOperation
        If mobjDataOperation Is Nothing Then
            objDataOperation = New clsDataOperation
        Else
            objDataOperation = mobjDataOperation
        End If
        objDataOperation.ClearParameters()
        Try
            strQ = "SELECT " & _
                   "     ROW_NUMBER()OVER(ORDER BY hrdiscipline_file_tran.disciplinefiletranunkid) AS charge_count " & _
                   "    ,hrdiscipline_file_tran.disciplinefiletranunkid " & _
                   "    ,hrdiscipline_file_tran.disciplinefileunkid " & _
                   "    ,hrdiscipline_file_tran.incident_description " & _
                   "    ,CAST(ROW_NUMBER()OVER(ORDER BY hrdiscipline_file_tran.disciplinefiletranunkid) AS nvarchar(50)) + ' - ' + ISNULL(hrdiscipline_file_tran.incident_description,'') AS display_incident " & _
                   "    ,hrdisciplinetype_master.offencecategoryunkid " & _
                   "    ,hrdisciplinetype_master.disciplinetypeunkid AS offenceunkid " & _
                   "    ,hrdiscipline_file_tran.isvoid " & _
                   "    ,hrdiscipline_file_tran.voiduserunkid " & _
                   "    ,hrdiscipline_file_tran.voidreason " & _
                   "    ,hrdiscipline_file_tran.voiddatetime " & _
                   "    ,cfcommon_master.name AS charge_category " & _
                   "    ,hrdisciplinetype_master.name AS charge_descr " & _
                   "    ,hrdisciplinetype_master.severity AS charge_severity " & _
                   "    ,A.count_statusid " & _
                   "    ,'' AS AUD " & _
                   "    ,'' AS GUID " & _
                   "    ,ISNULL(A.disciplineproceedingmasterunkid,0) AS disciplineproceedingmasterunkid " & _
                   "    ,hrdiscipline_file_tran.responsetypeunkid " & _
                   "    ,hrdiscipline_file_tran.response_date " & _
                   "    ,hrdiscipline_file_tran.response_remark " & _
                   "    ,ISNULL(RT.name,'') AS response_type " & _
                   "    ,ISNULL(hrdiscipline_file_tran.isfromess,0) AS isfromess " & _
                   "    ,ISNULL(hrdiscipline_file_tran.contrarytounkid,0) AS contrarytounkid " & _
                   "    ,ISNULL(CT.name,'') AS contraryto " & _
                   " FROM hrdiscipline_file_tran " & _
                   "    LEFT JOIN " & _
                   "    ( " & _
                   "        SELECT " & _
                   "             hrdiscipline_proceeding_master.disciplineproceedingmasterunkid " & _
                   "            ,hrdiscipline_proceeding_master.disciplinefiletranunkid " & _
                   "            ,ROW_NUMBER()OVER(PARTITION BY hrdiscipline_proceeding_master.disciplinefiletranunkid ORDER BY hrdiscipline_proceeding_master.statusdate DESC) AS rno " & _
                   "            ,hrdiscipline_proceeding_master.count_status AS count_statusid " & _
                   "        FROM hrdiscipline_proceeding_master " & _
                   "        WHERE hrdiscipline_proceeding_master.isvoid = 0 " & _
                   "    ) AS A ON A.disciplinefiletranunkid = hrdiscipline_file_tran.disciplinefiletranunkid AND A.rno = 1 " & _
                   "    JOIN hrdisciplinetype_master ON hrdiscipline_file_tran.offenceunkid = hrdisciplinetype_master.disciplinetypeunkid " & _
                   "    JOIN cfcommon_master ON cfcommon_master.masterunkid = hrdisciplinetype_master.offencecategoryunkid AND cfcommon_master.mastertype = '" & clsCommon_Master.enCommonMaster.OFFENCE_CATEGORY & "' " & _
                   "    LEFT JOIN cfcommon_master AS RT ON RT.masterunkid = hrdiscipline_file_tran.responsetypeunkid AND RT.mastertype = '" & clsCommon_Master.enCommonMaster.DISC_REPONSE_TYPE & "' " & _
                   "    LEFT JOIN cfcommon_master AS CT ON CT.masterunkid = hrdiscipline_file_tran.contrarytounkid AND CT.mastertype = '" & clsCommon_Master.enCommonMaster.DISCIPLINE_CONTRARY_TO & "' " & _
                   "   WHERE hrdiscipline_file_tran.disciplinefileunkid = @disciplinefileunkid AND hrdiscipline_file_tran.isvoid = 0 " 'S.SANDEEP |11-NOV-2019| -- START {isfromess} -- END

            'Pinkal (19-Dec-2020) -- Enhancement  -  Working on Discipline module for NMB.[ISNULL(hrdiscipline_file_tran.contrarytounkid,0) AS contrarytounkid]

            '"    LEFT JOIN " & _
            '"    ( " & _
            '"        SELECT " & _
            '"             CS.fileid " & _
            '"            ,CS.filetranid " & _
            '"            ,CS.count_statusid " & _
            '"        FROM " & _
            '"        ( " & _
            '"            SELECT " & _
            '"                 hrdiscipline_chargestatus_tran.disciplinefileunkid AS fileid " & _
            '"                ,hrdiscipline_chargestatus_tran.disciplinefiletranunkid AS filetranid " & _
            '"                ,hrdiscipline_chargestatus_tran.statusunkid AS count_statusid " & _
            '"                ,hrdiscipline_chargestatus_tran.statusdate " & _
            '"                ,ROW_NUMBER()OVER(PARTITION BY disciplinefileunkid,disciplinefiletranunkid ORDER BY statusdate DESC) AS rno " & _
            '"            FROM hrdiscipline_chargestatus_tran " & _
            '"            WHERE hrdiscipline_chargestatus_tran.isvoid = 0 " & _
            '"        ) AS CS WHERE CS.rno = 1 " & _
            '"    ) AS cstatus ON cstatus.fileid = hrdiscipline_file_tran.disciplinefileunkid " & _
            '"    AND cstatus.filetranid = hrdiscipline_file_tran.disciplinefiletranunkid " & _

            objDataOperation.AddParameter("@disciplinefileunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintDisciplinefileunkid)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            mdtTran.Rows.Clear()
            For Each drow As DataRow In dsList.Tables(0).Rows
                mdtTran.ImportRow(drow)
            Next

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetChargesCount; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Sub

    Public Function InsertUpdateDelete_DisciplineCharges(ByVal intUserId As Integer, _
                                                         ByVal dtCurrentDate As DateTime, _
                                                         ByVal dtScanAttachment As DataTable, ByVal strTransScreenName As String) As Boolean
        Dim objDataoperation As New clsDataOperation
        Dim exForce As Exception
        Try

            objDataoperation.BindTransaction()
            If InsertUpdateDelete_DisciplineCharges(objDataoperation, intUserId, dtCurrentDate, dtScanAttachment, strTransScreenName) = False Then
                exForce = New Exception(objDataoperation.ErrorNumber & ": " & objDataoperation.ErrorMessage)
                Throw exForce
            End If

            'If dtScanAttachment IsNot Nothing Then
            '    Dim objDocument As New clsScan_Attach_Documents
            '    objDocument._Datatable = dtScanAttachment
            '    objDocument.InsertUpdateDelete_Documents(objDataoperation)
            '    If objDataoperation.ErrorMessage <> "" Then
            '        exForce = New Exception(objDataoperation.ErrorNumber & ": " & objDataoperation.ErrorMessage)
            '        Throw exForce
            '    End If
            'End If
            objDataoperation.ReleaseTransaction(True)
        Catch ex As Exception
            objDataoperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: InsertUpdateDelete_DisciplineCharges; Module Name: " & mstrModuleName)
            Return False
        Finally
            objDataoperation = Nothing
        End Try
        Return True
    End Function

    Public Function InsertUpdateDelete_DisciplineCharges(ByVal objDataOperation As clsDataOperation, ByVal intUserId As Integer, _
                                                         ByVal dtCurrentDate As DateTime, ByVal dtScanAttachDocs As DataTable, ByVal strTransScreenName As String) As Boolean
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim dsList As New DataSet
        Try
            For Each drow As DataRow In mdtTran.Rows
                objDataOperation.ClearParameters()
                With drow
                    If Not IsDBNull(.Item("AUD")) Then
                        Select Case .Item("AUD")
                            Case "A"
                                strQ = "INSERT INTO hrdiscipline_file_tran ( " & _
                                           "  disciplinefileunkid " & _
                                           ", incident_description " & _
                                           ", offenceunkid " & _
                                           ", isvoid " & _
                                           ", voiduserunkid " & _
                                           ", voidreason " & _
                                           ", voiddatetime" & _
                                           ", responsetypeunkid " & _
                                           ", response_date " & _
                                           ", response_remark " & _
                                           ", isfromess " & _
                                           ", contrarytounkid " & _
                                       ") VALUES (" & _
                                           "  @disciplinefileunkid " & _
                                           ", @incident_description " & _
                                           ", @offenceunkid " & _
                                           ", @isvoid " & _
                                           ", @voiduserunkid " & _
                                           ", @voidreason " & _
                                           ", @voiddatetime" & _
                                           ", @responsetypeunkid " & _
                                           ", @response_date " & _
                                           ", @response_remark " & _
                                           ", @isfromess " & _
                                           ", @contrarytounkid " & _
                                       "); SELECT @@identity"


                                'Pinkal (19-Dec-2020) -- Enhancement  -  Working on Discipline module for NMB.[contrarytounkid]

                                'S.SANDEEP |11-NOV-2019| -- START {isfromess} -- END

                                objDataOperation.AddParameter("@disciplinefileunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintDisciplinefileunkid)
                                objDataOperation.AddParameter("@incident_description", SqlDbType.NVarChar, .Item("incident_description").ToString.Length, .Item("incident_description"))
                                objDataOperation.AddParameter("@offenceunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("offenceunkid"))
                                objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, .Item("isvoid"))
                                objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("voiduserunkid"))
                                objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, .Item("voidreason"))
                                If .Item("voiddatetime").ToString <> Nothing Then
                                    objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, .Item("voiddatetime"))
                                Else
                                    objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
                                End If
                                objDataOperation.AddParameter("@responsetypeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("responsetypeunkid"))
                                If .Item("response_date").ToString <> Nothing Then
                                    objDataOperation.AddParameter("@response_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, .Item("response_date"))
                                Else
                                    objDataOperation.AddParameter("@response_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
                                End If
                                objDataOperation.AddParameter("@response_remark", SqlDbType.NVarChar, .Item("response_remark").ToString.Length, .Item("response_remark"))

                                'S.SANDEEP |11-NOV-2019| -- START
                                'ISSUE/ENHANCEMENT : NMB DISCIPLINARY UAT COMMENTS
                                objDataOperation.AddParameter("@isfromess", SqlDbType.Bit, eZeeDataType.BIT_SIZE, .Item("isfromess"))
                                'S.SANDEEP |11-NOV-2019| -- END

                                'Pinkal (19-Dec-2020) -- Start
                                'Enhancement  -  Working on Discipline module for NMB.
                                objDataOperation.AddParameter("@contrarytounkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("contrarytounkid"))
                                'Pinkal (19-Dec-2020) -- End


                                dsList = objDataOperation.ExecQuery(strQ, "List")

                                If objDataOperation.ErrorMessage <> "" Then
                                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                    Throw exForce
                                End If

                                mintDisciplinefiletranunkid = dsList.Tables(0).Rows(0).Item(0)

                                If dtScanAttachDocs IsNot Nothing AndAlso dtScanAttachDocs.Rows.Count > 0 Then
                                    Dim dtTab As DataTable = Nothing
                                    Dim row As DataRow() = dtScanAttachDocs.Select("GUID = '" & .Item("GUID").ToString & "' AND form_name = '" & strTransScreenName & "'")
                                    If row.Length > 0 Then
                                        row.ToList.ForEach(Function(x) UpdateRowValue(x, mintDisciplinefiletranunkid, intUserId))
                                        dtTab = row.CopyToDataTable()
                                        'dtTab = New DataView(dtScanAttachDocs, "GUID = '" & .Item("GUID").ToString & "'", "", DataViewRowState.CurrentRows).ToTable
                                        objDocument._Datatable = dtTab
                                        With objDocument
                                            ._FormName = mstrFormName
                                            ._LoginEmployeeunkid = mintLoginEmployeeunkid
                                            ._ClientIP = mstrClientIP
                                            ._HostName = mstrHostName
                                            ._FromWeb = mblnIsWeb
                                            ._AuditUserId = mintAuditUserId
                                            ._CompanyUnkid = mintCompanyUnkid
                                            ._AuditDate = mdtAuditDate
                                        End With
                                        objDocument.InsertUpdateDelete_Documents(objDataOperation)
                                        If objDataOperation.ErrorMessage <> "" Then
                                            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                            Throw exForce
                                        End If


                                        'Pinkal (08-May-2021)-- Start
                                        'NMB Discipline Enhancement  -  Working on Discipline Enhancement for NMB.
                                        If row IsNot Nothing AndAlso row.Count > 0 Then
                                            For Each dr In row
                                                dtScanAttachDocs.Rows.Remove(dr)
                                            Next
                                            dtScanAttachDocs.AcceptChanges()
                                        End If
                                        'Pinkal (08-May-2021) -- End

                                    End If
                                End If


                                If Insert_DiscCharges_AuditTrail(objDataOperation, intUserId, dtCurrentDate, drow, enAuditType.ADD) = False Then
                                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                    Throw exForce
                                End If

                                'Dim objChargeStatus As New clsDiscipline_chargestatus_tran
                                'objChargeStatus._mDataOperation = objDataOperation
                                'objChargeStatus._Disciplinefiletranunkid = mintDisciplinefiletranunkid
                                'objChargeStatus._Disciplinefileunkid = mintDisciplinefileunkid
                                'objChargeStatus._Isexternal = blnIsexternal
                                'objChargeStatus._Isvoid = False
                                'objChargeStatus._Remark = ""
                                'objChargeStatus._Statusdate = dtCurrentDate
                                'objChargeStatus._Statusunkid = clsdiscipline_proceeding_master.enProceedingCountStatus.Open
                                'objChargeStatus._Userunkid = intUserId
                                'objChargeStatus._Voidatetime = Nothing
                                'objChargeStatus._Voidreason = ""
                                'objChargeStatus._Voiduserunkid = -1
                                'If objChargeStatus.Insert() = False Then
                                '    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                '    Throw exForce
                                'End If
                                'objChargeStatus = Nothing

                            Case "U"

                                strQ = "UPDATE hrdiscipline_file_tran SET " & _
                                       "  disciplinefileunkid = @disciplinefileunkid" & _
                                       ", incident_description = @incident_description" & _
                                       ", offenceunkid = @offenceunkid" & _
                                       ", isvoid = @isvoid" & _
                                       ", voiduserunkid = @voiduserunkid" & _
                                       ", voidreason = @voidreason" & _
                                       ", voiddatetime = @voiddatetime " & _
                                       ", responsetypeunkid = @responsetypeunkid" & _
                                       ", response_date = @response_date" & _
                                       ", response_remark = @response_remark " & _
                                       ", isfromess = @isfromess " & _
                                       ", contrarytounkid = @contrarytounkid " & _
                                       "WHERE disciplinefiletranunkid = @disciplinefiletranunkid "

                                'Pinkal (19-Dec-2020) -- Enhancement  -  Working on Discipline module for NMB.[contrarytounkid = @contrarytounkid]

                                'S.SANDEEP |11-NOV-2019| -- START {isfromess} -- END

                                objDataOperation.AddParameter("@disciplinefiletranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("disciplinefiletranunkid"))
                                objDataOperation.AddParameter("@disciplinefileunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("disciplinefileunkid"))
                                objDataOperation.AddParameter("@incident_description", SqlDbType.NVarChar, .Item("incident_description").ToString.Length, .Item("incident_description"))
                                objDataOperation.AddParameter("@offenceunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("offenceunkid"))
                                objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, .Item("isvoid"))
                                objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("voiduserunkid"))
                                objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, .Item("voidreason"))
                                If .Item("voiddatetime").ToString <> Nothing Then
                                    objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, .Item("voiddatetime"))
                                Else
                                    objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
                                End If
                                objDataOperation.AddParameter("@responsetypeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("responsetypeunkid"))
                                If .Item("response_date").ToString <> Nothing Then
                                    objDataOperation.AddParameter("@response_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, .Item("response_date"))
                                Else
                                    objDataOperation.AddParameter("@response_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
                                End If
                                objDataOperation.AddParameter("@response_remark", SqlDbType.NVarChar, .Item("response_remark").ToString.Length, .Item("response_remark"))

                                'S.SANDEEP |11-NOV-2019| -- START
                                'ISSUE/ENHANCEMENT : NMB DISCIPLINARY UAT COMMENTS
                                objDataOperation.AddParameter("@isfromess", SqlDbType.Bit, eZeeDataType.BIT_SIZE, .Item("isfromess"))
                                'S.SANDEEP |11-NOV-2019| -- END

                                'Pinkal (19-Dec-2020) -- Start
                                'Enhancement  -  Working on Discipline module for NMB.
                                objDataOperation.AddParameter("@contrarytounkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("contrarytounkid"))
                                'Pinkal (19-Dec-2020) -- End

                                objDataOperation.ExecNonQuery(strQ)

                                If objDataOperation.ErrorMessage <> "" Then
                                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                    Throw exForce
                                End If


                                If dtScanAttachDocs IsNot Nothing AndAlso dtScanAttachDocs.Rows.Count > 0 Then
                                    Dim dtTab As DataTable = Nothing
                                    Dim row As DataRow() = dtScanAttachDocs.Select("transactionunkid = '" & .Item("disciplinefiletranunkid").ToString & "' AND form_name = '" & strTransScreenName & "' AND AUD <> ''")
                                    If row.Length > 0 Then
                                        'row.ToList.ForEach(Function(x) UpdateRowValue(x, .Item("disciplinefiletranunkid"), intUserId))
                                        dtTab = row.CopyToDataTable()
                                        'dtTab = New DataView(dtScanAttachDocs, "GUID = '" & .Item("GUID").ToString & "'", "", DataViewRowState.CurrentRows).ToTable
                                        objDocument._Datatable = dtTab
                                        With objDocument
                                            ._FormName = mstrFormName
                                            ._LoginEmployeeunkid = mintLoginEmployeeunkid
                                            ._ClientIP = mstrClientIP
                                            ._HostName = mstrHostName
                                            ._FromWeb = mblnIsWeb
                                            ._AuditUserId = mintAuditUserId
._CompanyUnkid = mintCompanyUnkid
                                            ._AuditDate = mdtAuditDate
                                        End With
                                        objDocument.InsertUpdateDelete_Documents(objDataOperation)
                                        If objDataOperation.ErrorMessage <> "" Then
                                            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                            Throw exForce
                                        End If

                                        'Pinkal (08-May-2021)-- Start
                                        'NMB Discipline Enhancement  -  Working on Discipline Enhancement for NMB.
                                        If row IsNot Nothing AndAlso row.Count > 0 Then
                                            For Each dr In row
                                                dtScanAttachDocs.Rows.Remove(dr)
                                            Next
                                            dtScanAttachDocs.AcceptChanges()
                                        End If
                                        'Pinkal (08-May-2021)-- End

                                    End If
                                End If

                                If Insert_DiscCharges_AuditTrail(objDataOperation, intUserId, dtCurrentDate, drow, enAuditType.EDIT) = False Then
                                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                    Throw exForce
                                End If


                            Case "D"

                                strQ = "UPDATE hrdiscipline_file_tran SET " & _
                                       "  isvoid = @isvoid" & _
                                       ", voiduserunkid = @voiduserunkid" & _
                                       ", voidreason = @voidreason" & _
                                       ", voiddatetime = @voiddatetime " & _
                                       "WHERE disciplinefiletranunkid = @disciplinefiletranunkid "

                                objDataOperation.AddParameter("@disciplinefiletranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("disciplinefiletranunkid"))
                                objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, .Item("isvoid"))
                                objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("voiduserunkid"))
                                objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, .Item("voidreason"))
                                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, .Item("voiddatetime"))

                                objDataOperation.ExecNonQuery(strQ)

                                If objDataOperation.ErrorMessage <> "" Then
                                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                    Throw exForce
                                End If

                                If dtScanAttachDocs IsNot Nothing AndAlso dtScanAttachDocs.Rows.Count > 0 Then
                                    Dim dtTab As DataTable = Nothing
                                    Dim row As DataRow() = dtScanAttachDocs.Select("transactionunkid = '" & .Item("disciplinefiletranunkid").ToString & "' AND form_name = '" & strTransScreenName & "' AND AUD = 'D'")
                                    If row.Length > 0 Then
                                        'row.ToList.ForEach(Function(x) UpdateRowValue(x, x.Item("disciplinefiletranunkid"), intUserId, "D"))
                                        dtTab = row.CopyToDataTable()
                                        'dtTab = New DataView(dtScanAttachDocs, "GUID = '" & .Item("GUID").ToString & "'", "", DataViewRowState.CurrentRows).ToTable
                                        objDocument._Datatable = dtTab
                                        With objDocument
                                            ._FormName = mstrFormName
                                            ._LoginEmployeeunkid = mintLoginEmployeeunkid
                                            ._ClientIP = mstrClientIP
                                            ._HostName = mstrHostName
                                            ._FromWeb = mblnIsWeb
                                            ._AuditUserId = mintAuditUserId
._CompanyUnkid = mintCompanyUnkid
                                            ._AuditDate = mdtAuditDate
                                        End With
                                        objDocument.InsertUpdateDelete_Documents(objDataOperation)
                                        If objDataOperation.ErrorMessage <> "" Then
                                            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                            Throw exForce
                                        End If
                                    End If
                                End If

                                If Insert_DiscCharges_AuditTrail(objDataOperation, intUserId, dtCurrentDate, drow, enAuditType.DELETE) = False Then
                                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                    Throw exForce
                                End If

                                'Dim objChargeStatus As New clsDiscipline_chargestatus_tran
                                'If objChargeStatus.Delete(.Item("disciplinefileunkid"), .Item("disciplinefiletranunkid")) = False Then
                                '    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                '    Throw exForce
                                'End If
                                'objChargeStatus = Nothing

                        End Select
                    End If
                End With
            Next
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: InsertUpdateDelete_DisciplineCharges; Module Name: " & mstrModuleName)
        Finally
        End Try
        Return True
    End Function

    Private Function Insert_DiscCharges_AuditTrail(ByVal objDataOperation As clsDataOperation, _
                                                   ByVal intUserId As Integer, _
                                                   ByVal dtCurrentDate As DateTime, _
                                                   ByVal dtRow As DataRow, _
                                                   ByVal eAudit As enAuditType) As Boolean
        Dim StrQ As String = ""
        Dim exForce As Exception = Nothing
        Try
            With dtRow
                StrQ = "INSERT INTO athrdiscipline_file_tran " & _
                       "( " & _
                       "     disciplinefiletranunkid " & _
                       "    ,disciplinefileunkid " & _
                       "    ,incident_description " & _
                       "    ,offenceunkid " & _
                       "    ,audittype " & _
                       "    ,audituserunkid " & _
                       "    ,auditdatetime " & _
                       "    ,ip " & _
                       "    ,machine_name " & _
                       "    ,form_name " & _
                       "    ,isweb " & _
                       "    ,responsetypeunkid " & _
                       "    ,response_date " & _
                       "    ,response_remark " & _
                       "    ,isfromess " & _
                       "    ,contrarytounkid " & _
                       ") " & _
                       "VALUES " & _
                       "( " & _
                       "     @disciplinefiletranunkid " & _
                       "    ,@disciplinefileunkid " & _
                       "    ,@incident_description " & _
                       "    ,@offenceunkid " & _
                       "    ,@audittype " & _
                       "    ,@audituserunkid " & _
                       "    ,@auditdatetime " & _
                       "    ,@ip " & _
                       "    ,@machine_name " & _
                       "    ,@form_name " & _
                       "    ,@isweb " & _
                       "    ,@responsetypeunkid " & _
                       "    ,@response_date " & _
                       "    ,@response_remark " & _
                       "    ,@isfromess " & _
                       "    ,@contrarytounkid " & _
                       ") "

                'Pinkal (19-Dec-2020) -- Enhancement  -  Working on Discipline module for NMB.[contrarytounkid]

                'S.SANDEEP |11-NOV-2019| -- START {isfromess} -- END

                objDataOperation.ClearParameters()

                objDataOperation.AddParameter("@disciplinefiletranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, IIf(mintDisciplinefiletranunkid <= 0, .Item("disciplinefiletranunkid"), mintDisciplinefiletranunkid))
                objDataOperation.AddParameter("@disciplinefileunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, IIf(mintDisciplinefileunkid <= 0, .Item("disciplinefileunkid"), mintDisciplinefileunkid))
                objDataOperation.AddParameter("@incident_description", SqlDbType.Text, eZeeDataType.NAME_SIZE, .Item("incident_description"))
                objDataOperation.AddParameter("@offenceunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("offenceunkid"))

                objDataOperation.AddParameter("@responsetypeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("responsetypeunkid"))
                If .Item("response_date").ToString <> Nothing Then
                    objDataOperation.AddParameter("@response_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, .Item("response_date"))
                Else
                    objDataOperation.AddParameter("@response_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
                End If
                objDataOperation.AddParameter("@response_remark", SqlDbType.Text, eZeeDataType.NAME_SIZE, .Item("response_remark"))

                'S.SANDEEP |11-NOV-2019| -- START
                'ISSUE/ENHANCEMENT : NMB DISCIPLINARY UAT COMMENTS
                objDataOperation.AddParameter("@isfromess", SqlDbType.Bit, eZeeDataType.BIT_SIZE, .Item("isfromess"))
                'S.SANDEEP |11-NOV-2019| -- END

                'Pinkal (19-Dec-2020) -- Start
                'Enhancement  -  Working on Discipline module for NMB.
                objDataOperation.AddParameter("@contrarytounkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("contrarytounkid"))
                'Pinkal (19-Dec-2020) -- End

            End With

            objDataOperation.AddParameter("@audittype", SqlDbType.Int, eZeeDataType.INT_SIZE, eAudit)
            objDataOperation.AddParameter("@audituserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAuditUserId)
            objDataOperation.AddParameter("@auditdatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtAuditDate)
            objDataOperation.AddParameter("@ip", SqlDbType.NVarChar, eZeeDataType.IP_SIZE, mstrClientIP)
            objDataOperation.AddParameter("@machine_name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrHostName)
            objDataOperation.AddParameter("@form_name", SqlDbType.NVarChar, 500, mstrFormName)
            objDataOperation.AddParameter("@isweb", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsWeb)


            objDataOperation.ExecNonQuery(StrQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Insert_DiscCharges_AuditTrail; Module Name: " & mstrModuleName)
        Finally
        End Try
        Return True
    End Function

    'S.SANDEEP [25 JUL 2016] -- START
    'ENHANCEMENT : DISCIPLINE MAKE-OVER {BY ANDREW}
    Public Function GetComboListForCharge(ByVal intFileTranId As Integer, _
                                          Optional ByVal blnFlag As Boolean = False, _
                                          Optional ByVal blnShowAllCharges As Boolean = True) As DataSet 'S.SANDEEP |11-NOV-2019| -- START {blnShowAllCharges} -- END) As DataSet
        Dim StrQ As String = ""
        Dim dsList As New DataSet
        Try
            Using objDo As New clsDataOperation
                If blnFlag Then
                    StrQ = "SELECT 0 AS disciplinefileunkid, @Select AS reference_no UNION "
                End If
                StrQ &= "SELECT DISTINCT " & _
                        "    hrdiscipline_file_tran.disciplinefiletranunkid AS disciplinefileunkid " & _
                        "   ,hrdiscipline_file_master.reference_no AS reference_no " & _
                        "FROM hrdiscipline_file_tran " & _
                        "   JOIN hrdiscipline_file_master ON hrdiscipline_file_master.disciplinefileunkid = hrdiscipline_file_tran.disciplinefileunkid " & _
                        "WHERE hrdiscipline_file_tran.isvoid = 0 AND hrdiscipline_file_tran.disciplinefiletranunkid = @disciplinefiletranunkid "

                'S.SANDEEP |11-NOV-2019| -- START
                'ISSUE/ENHANCEMENT : NMB DISCIPLINARY UAT COMMENTS
                If blnShowAllCharges = False Then
                    StrQ &= " AND hrdiscipline_file_master.isvisibleoness = 1 "
                End If
                'S.SANDEEP |11-NOV-2019| -- END

                objDo.AddParameter("@disciplinefiletranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intFileTranId)
                objDo.AddParameter("@Select", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 37, "Select"))

                dsList = objDo.ExecQuery(StrQ, "List")

                If objDo.ErrorMessage <> "" Then
                    Throw New Exception(objDo.ErrorNumber & " : " & objDo.ErrorMessage)
                End If

            End Using
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetComboListForCharge; Module Name: " & mstrModuleName)
        Finally
        End Try
        Return dsList
    End Function

    Private Function UpdateRowValue(ByVal dr As DataRow, ByVal intValue As Integer, ByVal intUserId As Integer, Optional ByVal strAUDValue As String = "") As Boolean
        Try
            dr("transactionunkid") = intValue
            dr("userunkid") = intUserId
            If strAUDValue.Trim.Length > 0 Then dr("AUD") = strAUDValue
            dr.AcceptChanges()
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: UpdateRowValue; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Function

    'S.SANDEEP |01-OCT-2019| -- START
    'ISSUE/ENHANCEMENT : NMB DISCIPLINARY UAT COMMENTS
    'Public Function IsRespondedAll_Counts(ByVal intDisciplineId As Integer, Optional ByVal dtCharges As DataTable = Nothing) As String
    Public Function IsRespondedAll_Counts(ByVal intDisciplineId As Integer, Optional ByVal dtCharges As DataTable = Nothing, Optional ByVal blnIsResponseTypeMandatory As Boolean = False) As String
        'S.SANDEEP |01-OCT-2019| -- END
        Dim StrQ As String = String.Empty
        Dim StrMsg As String = String.Empty
        Dim intTotalCount, intResponseCount As Integer
        Try
            intTotalCount = 0 : intResponseCount = 0
            Using objDataOpr As New clsDataOperation

                objDataOpr.AddParameter("@disciplinefileunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intDisciplineId)

                StrQ = "SELECT @TotalCount = COUNT(hrdiscipline_file_tran.disciplinefiletranunkid) " & _
                       "FROM hrdiscipline_file_tran WHERE hrdiscipline_file_tran.isvoid = 0 AND hrdiscipline_file_tran.disciplinefileunkid = @disciplinefileunkid "

                objDataOpr.AddParameter("@TotalCount", SqlDbType.Int, eZeeDataType.INT_SIZE, intTotalCount, ParameterDirection.InputOutput)

                objDataOpr.ExecNonQuery(StrQ)

                If objDataOpr.ErrorMessage <> "" Then
                    Throw New Exception(objDataOpr.ErrorNumber & " : " & objDataOpr.ErrorMessage)
                End If

                intTotalCount = objDataOpr.GetParameterValue("@TotalCount")

                If dtCharges Is Nothing Then

                    StrQ = "SELECT " & _
                           "  @ResponseGiven = COUNT(hrdiscipline_file_tran.disciplinefiletranunkid) " & _
                           "FROM hrdiscipline_file_tran WHERE hrdiscipline_file_tran.isvoid = 0 AND hrdiscipline_file_tran.disciplinefileunkid = @disciplinefileunkid " & _
                           "AND hrdiscipline_file_tran.response_date IS NOT NULL "

                    objDataOpr.AddParameter("@ResponseGiven", SqlDbType.Int, eZeeDataType.INT_SIZE, intResponseCount, ParameterDirection.InputOutput)

                    objDataOpr.ExecNonQuery(StrQ)

                    If objDataOpr.ErrorMessage <> "" Then
                        Throw New Exception(objDataOpr.ErrorNumber & " : " & objDataOpr.ErrorMessage)
                    End If

                    intResponseCount = objDataOpr.GetParameterValue("@ResponseGiven")

                ElseIf dtCharges.Rows.Count > 0 Then

                    'S.SANDEEP |01-OCT-2019| -- START
                    'ISSUE/ENHANCEMENT : NMB DISCIPLINARY UAT COMMENTS
                    'intResponseCount = dtCharges.AsEnumerable().Where(Function(x) x.Field(Of Integer)("responsetypeunkid") > 0).Count()
                    If blnIsResponseTypeMandatory Then
                    intResponseCount = dtCharges.AsEnumerable().Where(Function(x) x.Field(Of Integer)("responsetypeunkid") > 0).Count()
                    Else
                        intResponseCount = dtCharges.Select("response_date IS NOT NULL").Count()
                        'intResponseCount = dtCharges.AsEnumerable().Where(Function(x) x.Field(Of Date)("").Equals(DBNull.Value) = False).Count()
                        'intResponseCount = dtCharges.AsEnumerable().Where(Function(x) x.Field(Of Date)("response_date").ToString().Trim.Length > 0).Count()
                    End If
                    'S.SANDEEP |01-OCT-2019| -- END

                End If
                

                If intTotalCount <> intResponseCount Then
                    StrMsg = Language.getMessage(mstrModuleName, 1, "Sorry, You cannot perform final save operation. Reason : You have not provided response for all count posted against you.")
                End If

            End Using
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: IsRespondedAll_Counts; Module Name: " & mstrModuleName)
        Finally
        End Try
        Return StrMsg
    End Function

    Public Function AllowVoidingResponse(ByVal intDisciplineFileTranId As Integer) As String
        Dim StrQ As String = String.Empty
        Dim StrMsg As String = String.Empty
        Dim iCnt As Integer = -1
        Try
            Using objDataOpr As New clsDataOperation

                StrQ = "SELECT 1 FROM hrdiscipline_proceeding_master WHERE hrdiscipline_proceeding_master.isvoid = 0 AND hrdiscipline_proceeding_master.disciplinefiletranunkid = @disciplinefiletranunkid "

                objDataOpr.AddParameter("@disciplinefiletranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intDisciplineFileTranId)

                iCnt = objDataOpr.RecordCount(StrQ)

                If objDataOpr.ErrorMessage <> "" Then
                    Throw New Exception(objDataOpr.ErrorNumber & " : " & objDataOpr.ErrorMessage)
                End If

                If iCnt > 0 Then
                    StrMsg = Language.getMessage(mstrModuleName, 2, "Sorry, You cannot void provided response. Reason : Proceeding steps for the selected count has already entered.")
                End If

            End Using
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: AllowVoidingResponse; Module Name: " & mstrModuleName)
        Finally
        End Try
        Return StrMsg
    End Function
    'S.SANDEEP [25 JUL 2016] -- START

    'S.SANDEEP |11-NOV-2019| -- START
    'ISSUE/ENHANCEMENT : NMB DISCIPLINARY UAT COMMENTS
    Public Function IsValidOperation(ByVal intDisciplineFileId As Integer) As String
        Dim StrQ As String = String.Empty
        Dim StrMsg As String = String.Empty
        Dim iCnt As Integer = -1
        Try
            Using objDataOpr As New clsDataOperation
                StrQ = "SELECT TOP 1 disciplinefileunkid FROM hrdiscipline_file_tran WHERE isvoid = 0 AND isfromess = 1 AND disciplinefileunkid = @disciplinefileunkid "

                objDataOpr.AddParameter("@disciplinefileunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intDisciplineFileId)

                iCnt = objDataOpr.RecordCount(StrQ)

                If objDataOpr.ErrorMessage <> "" Then
                    Throw New Exception(objDataOpr.ErrorNumber & " : " & objDataOpr.ErrorMessage)
                End If

                If iCnt > 0 Then
                    StrMsg = Language.getMessage(mstrModuleName, 3, "Sorry, You cannot change this setting. Reason : Response is already given by employee on this charge.")
                End If
            End Using
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: ; Module Name: " & mstrModuleName)
        Finally
        End Try
        Return StrMsg
    End Function
    'S.SANDEEP |11-NOV-2019| -- END

#End Region

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Public Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Sorry, You cannot perform final save operation. Reason : You have not provided response for all count posted against you.")
            Language.setMessage(mstrModuleName, 2, "Sorry, You cannot void provided response. Reason : Proceeding steps for the selected count has already entered.")
            Language.setMessage(mstrModuleName, 3, "Sorry, You cannot change this setting. Reason : Response is already given by employee on this charge.")
            Language.setMessage(mstrModuleName, 5, "WEB")
            Language.setMessage(mstrModuleName, 37, "Select")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class
