﻿'************************************************************************************************************************************
'Class Name : clsDiscipline_file_master.vb
'Purpose    :
'Date       :30-MAY-2011
'Written By :Sandeep J. Sharma
'Modified   :
'************************************************************************************************************************************

#Region " Imports "

Imports eZeeCommonLib
Imports System.Windows.Forms
Imports eZee.Common.eZeeForm
Imports System.Text
Imports System.Web
#End Region

''' <summary>
''' Purpose: 
''' Developer: Sandeep J. Sharma
''' </summary>

Public Class clsDiscipline_file_master
    Private Shared ReadOnly mstrModuleName As String = "clsDiscipline_file_master"
    Private mstrMessage As String = ""
    Private objDiscStatus As New clsDiscipline_StatusTran
    Private objDiscFileTran As New clsDiscipline_file_tran

#Region " Private variables "

    Private mintDisciplinefileunkid As Integer = 0
    Private mstrReference_No As String = String.Empty
    Private mdtChargedate As Date = Nothing
    Private mdtInterdictiondate As Date = Nothing
    Private mstrCharge_Description As String = String.Empty
    Private mintInvolved_Employeeunkid As Integer = 0
    Private mintDisciplinestatusunkid As Integer = 0
    Private mintEmailtranunkid As Integer = 0
    Private mdtEmaildate As Date = Nothing
    Private mintUserunkid As Integer = 0
    Private mblnIsvoid As Boolean = False
    Private mintVoiduserunkid As Integer = 0
    Private mstrVoidreason As String = String.Empty
    Private mdtVoiddatetime As Date = Nothing
    'Private mstrWebFormName As String = String.Empty
    'Private mstrWebClientIP As String = String.Empty
    'Private mstrWebHostName As String = String.Empty
    Private mobjDataOperation As clsDataOperation = Nothing
    Private mblnIsExternal As Boolean = False
    Private mintLoginemployeeId As Integer = 0
    Private mblnNotifyEmployee As Boolean = False
    Private mdtNotificationDate As DateTime = Nothing
    Private mintLoginTypeId As Integer = 0
    Private mblnIsFinal As Boolean = False
    'S.SANDEEP |11-NOV-2019| -- START
    'ISSUE/ENHANCEMENT : NMB DISCIPLINARY UAT COMMENTS
    Private mblnIsVisibleonEss As Boolean = False
    'S.SANDEEP |11-NOV-2019| -- END

    'S.SANDEEP |02-MAR-2022| -- START
    Private mintPostuserunkid As Integer = 0
    'S.SANDEEP |02-MAR-2022| -- END

#End Region

#Region " Properties "


'S.SANDEEP [28-May-2018] -- START 
'ISSUE/ENHANCEMENT : {Audit Trails} 
Private mstrFormName As String = String.Empty
Public WriteOnly Property _FormName() As String 
Set(ByVal value As String) 
mstrFormName = value 
End Set 
End Property 
 
Private mstrClientIP As String = ""
Public WriteOnly Property _ClientIP() As String 
Set(ByVal value As String) 
mstrClientIP = value 
End Set 
End Property 
 
Private mstrHostName As String = ""
Public WriteOnly Property _HostName() As String 
Set(ByVal value As String) 
mstrHostName = value 
End Set 
End Property 
 
Private mblnIsWeb As Boolean = False
Public WriteOnly Property _FromWeb() As Boolean 
Set(ByVal value As Boolean) 
mblnIsWeb = value 
End Set 
End Property 
 
    Private mintAuditUserId As Integer = 0
    Public WriteOnly Property _AuditUserId() As Integer
        Set(ByVal value As Integer)
            mintAuditUserId = value
        End Set
    End Property

    Private mintCompanyUnkid As Integer = 0
    Public WriteOnly Property _CompanyUnkid() As Integer
        Set(ByVal value As Integer)
            mintCompanyUnkid = value
        End Set
    End Property
 
Private mdtAuditDate As DateTime = Now
Public WriteOnly Property _AuditDate() As DateTime 
Set(ByVal value As DateTime) 
mdtAuditDate = value 
End Set 
End Property 
 
Private xDataOpr As clsDataOperation = Nothing
Public Property _xDataOpr() As clsDataOperation 
Get 
Return xDataOpr 
End Get 
Set(ByVal value As clsDataOperation) 
xDataOpr = value 
End Set 
End Property 
 
Private mintLoginEmployeeunkid As Integer = 0
Public Property _LoginEmployeeunkid() As Integer 
Get 
Return mintLoginEmployeeunkid 
End Get 
Set(ByVal value As Integer) 
mintLoginEmployeeunkid = value 
End Set 
End Property 
 
Private mintVoidlogingemployeeunkid As Integer=0
Public Property _Voidloginemployeeunkid() As Integer 
Get 
Return mintVoidlogingemployeeunkid 
End Get 
Set(ByVal value As Integer) 
mintVoidlogingemployeeunkid = value 
End Set 
End Property 
'S.SANDEEP [28-May-2018] -- END 

    ''' <summary>
    ''' Purpose: Get Message from Class 
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public ReadOnly Property _Message() As String
        Get
            Return mstrMessage
        End Get
    End Property

    ''' <summary>
    ''' Purpose: Get or Set disciplinefileunkid
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Disciplinefileunkid() As Integer
        Get
            Return mintDisciplinefileunkid
        End Get
        Set(ByVal value As Integer)
            mintDisciplinefileunkid = value
            Call GetData()
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set reference_no
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Reference_No() As String
        Get
            Return mstrReference_No
        End Get
        Set(ByVal value As String)
            mstrReference_No = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set chargedate
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Chargedate() As Date
        Get
            Return mdtChargedate
        End Get
        Set(ByVal value As Date)
            mdtChargedate = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set interdictiondate
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Interdictiondate() As Date
        Get
            Return mdtInterdictiondate
        End Get
        Set(ByVal value As Date)
            mdtInterdictiondate = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set charge_description
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Charge_Description() As String
        Get
            Return mstrCharge_Description
        End Get
        Set(ByVal value As String)
            mstrCharge_Description = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set involved_employeeunkid
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Involved_Employeeunkid() As Integer
        Get
            Return mintInvolved_Employeeunkid
        End Get
        Set(ByVal value As Integer)
            mintInvolved_Employeeunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set disciplinestatusunkid
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Disciplinestatusunkid() As Integer
        Get
            Return mintDisciplinestatusunkid
        End Get
        Set(ByVal value As Integer)
            mintDisciplinestatusunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set emailtranunkid
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Emailtranunkid() As Integer
        Get
            Return mintEmailtranunkid
        End Get
        Set(ByVal value As Integer)
            mintEmailtranunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set emaildate
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Emaildate() As Date
        Get
            Return mdtEmaildate
        End Get
        Set(ByVal value As Date)
            mdtEmaildate = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set userunkid
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Userunkid() As Integer
        Get
            Return mintUserunkid
        End Get
        Set(ByVal value As Integer)
            mintUserunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isvoid
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Isvoid() As Boolean
        Get
            Return mblnIsvoid
        End Get
        Set(ByVal value As Boolean)
            mblnIsvoid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiduserunkid
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Voiduserunkid() As Integer
        Get
            Return mintVoiduserunkid
        End Get
        Set(ByVal value As Integer)
            mintVoiduserunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voidreason
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Voidreason() As String
        Get
            Return mstrVoidreason
        End Get
        Set(ByVal value As String)
            mstrVoidreason = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiddatetime
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Voiddatetime() As Date
        Get
            Return mdtVoiddatetime
        End Get
        Set(ByVal value As Date)
            mdtVoiddatetime = value
        End Set
    End Property

    'Public WriteOnly Property _WebFormName() As String
    '    Set(ByVal value As String)
    '        mstrWebFormName = value
    '    End Set
    'End Property

    'Public WriteOnly Property _WebClientIP() As String
    '    Set(ByVal value As String)
    '        mstrWebClientIP = value
    '    End Set
    'End Property

    'Public WriteOnly Property _WebHostName() As String
    '    Set(ByVal value As String)
    '        mstrWebHostName = value
    '    End Set
    'End Property

    Public WriteOnly Property _objDataOperation() As clsDataOperation
        Set(ByVal value As clsDataOperation)
            mobjDataOperation = value
        End Set
    End Property

    Public WriteOnly Property _IsExternal() As Boolean
        Set(ByVal value As Boolean)
            mblnIsExternal = value
        End Set
    End Property

    Public Property _LoginemployeeId() As Integer
        Get
            Return mintLoginemployeeId
        End Get
        Set(ByVal value As Integer)
            mintLoginemployeeId = value
        End Set
    End Property

    Public Property _NotifyEmployee() As Boolean
        Get
            Return mblnNotifyEmployee
        End Get
        Set(ByVal value As Boolean)
            mblnNotifyEmployee = value
        End Set
    End Property

    Public Property _Notification_Date() As DateTime
        Get
            Return mdtNotificationDate
        End Get
        Set(ByVal value As DateTime)
            mdtNotificationDate = value
        End Set
    End Property

    'S.SANDEEP [24 MAY 2016] -- Start
    'Email Notification
    Public WriteOnly Property _LoginTypeId() As Integer
        Set(ByVal value As Integer)
            mintLoginTypeId = value
        End Set
    End Property
    'S.SANDEEP [24 MAY 2016] -- End

    'S.SANDEEP [25 JUL 2016] -- START
    'ENHANCEMENT : DISCIPLINE MAKE-OVER {BY ANDREW}
    Public Property _IsFinal() As Boolean
        Get
            Return mblnIsFinal
        End Get
        Set(ByVal value As Boolean)
            mblnIsFinal = value
        End Set
    End Property
    'S.SANDEEP [25 JUL 2016] -- START

    'S.SANDEEP |11-NOV-2019| -- START
    'ISSUE/ENHANCEMENT : NMB DISCIPLINARY UAT COMMENTS
    Public Property _IsVisibleOnEss() As Boolean
        Get
            Return mblnIsVisibleonEss
        End Get
        Set(ByVal value As Boolean)
            mblnIsVisibleonEss = value
        End Set
    End Property
    'S.SANDEEP |11-NOV-2019| -- END

    'S.SANDEEP |02-MAR-2022| -- START
    Public Property _Postuserunkid() As Integer
        Get
            Return mintPostuserunkid
        End Get
        Set(ByVal value As Integer)
            mintPostuserunkid = value
        End Set
    End Property
    'S.SANDEEP |02-MAR-2022| -- END

#End Region

    ''' <summary>
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Sub GetData()
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim objDataOperation As clsDataOperation
        If mobjDataOperation Is Nothing Then
            objDataOperation = New clsDataOperation
        Else
            objDataOperation = mobjDataOperation
        End If
        objDataOperation.ClearParameters()
        Try
            strQ = "SELECT " & _
                  "  disciplinefileunkid " & _
                  ", reference_no " & _
                  ", chargedate " & _
                  ", interdictiondate " & _
                  ", charge_description " & _
                  ", involved_employeeunkid " & _
                  ", disciplinestatusunkid " & _
                  ", emailtranunkid " & _
                  ", emaildate " & _
                  ", userunkid " & _
                  ", isvoid " & _
                  ", voiduserunkid " & _
                  ", voidreason " & _
                  ", voiddatetime " & _
                  ", loginemployeeunkid " & _
                  ", notifyemployee " & _
                  ", notification_date " & _
                  ", isfinal " & _
                  ", isvisibleoness " & _
                  ", ISNULL(postuserunkid,0) AS postuserunkid " & _
                 "FROM hrdiscipline_file_master " & _
                 "WHERE disciplinefileunkid = @disciplinefileunkid "
            'S.SANDEEP |02-MAR-2022| -- START {postuserunkid} -- END
            'S.SANDEEP |11-NOV-2019| -- START {isvisibleoness} -- END

            objDataOperation.AddParameter("@disciplinefileunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintDisciplinefileunkid.ToString)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For Each dtRow As DataRow In dsList.Tables(0).Rows
                mintDisciplinefileunkid = CInt(dtRow.Item("disciplinefileunkid"))
                mstrReference_No = dtRow.Item("reference_no").ToString
                If IsDBNull(dtRow.Item("chargedate")) = False Then
                    mdtChargedate = dtRow.Item("chargedate")
                Else
                    mdtChargedate = Nothing
                End If
                If IsDBNull(dtRow.Item("interdictiondate")) = False Then
                    mdtInterdictiondate = dtRow.Item("interdictiondate")
                Else
                    mdtInterdictiondate = Nothing
                End If
                mstrCharge_Description = dtRow.Item("charge_description").ToString
                mintInvolved_Employeeunkid = CInt(dtRow.Item("involved_employeeunkid"))
                mintDisciplinestatusunkid = CInt(dtRow.Item("disciplinestatusunkid"))
                mintEmailtranunkid = CInt(dtRow.Item("emailtranunkid"))
                If IsDBNull(dtRow.Item("emaildate")) = False Then
                    mdtEmaildate = dtRow.Item("emaildate")
                Else
                    mdtEmaildate = Nothing
                End If
                mintUserunkid = CInt(dtRow.Item("userunkid"))
                mblnIsvoid = CBool(dtRow.Item("isvoid"))
                mintVoiduserunkid = CInt(dtRow.Item("voiduserunkid"))
                mstrVoidreason = dtRow.Item("voidreason").ToString
                If IsDBNull(dtRow.Item("voiddatetime")) = False Then
                    mdtVoiddatetime = dtRow.Item("voiddatetime")
                Else
                    mdtVoiddatetime = Nothing
                End If
                mintLoginemployeeId = CInt(dtRow.Item("loginemployeeunkid"))
                mblnNotifyEmployee = CBool(dtRow.Item("notifyemployee"))
                If IsDBNull(dtRow.Item("notification_date")) = False Then
                    mdtNotificationDate = dtRow.Item("notification_date")
                Else
                    mdtNotificationDate = Nothing
                End If
                mblnIsFinal = CBool(dtRow.Item("isfinal"))
                'S.SANDEEP |11-NOV-2019| -- START
                'ISSUE/ENHANCEMENT : NMB DISCIPLINARY UAT COMMENTS
                mblnIsVisibleonEss = CBool(dtRow.Item("isvisibleoness"))
                'S.SANDEEP |11-NOV-2019| -- END

                'S.SANDEEP |02-MAR-2022| -- START
                mintPostuserunkid = CInt(dtRow.Item("postuserunkid"))
                'S.SANDEEP |02-MAR-2022| -- END
                Exit For
            Next
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetData; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If mobjDataOperation Is Nothing Then objDataOperation = Nothing
        End Try
    End Sub

    ''' <summary>
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function GetList(ByVal xDatabaseName As String, _
                            ByVal xUserUnkid As Integer, _
                            ByVal xYearUnkid As Integer, _
                            ByVal xCompanyUnkid As Integer, _
                            ByVal xPeriodStart As DateTime, _
                            ByVal xPeriodEnd As DateTime, _
                            ByVal xUserModeSetting As String, _
                            ByVal xOnlyApproved As Boolean, _
                            ByVal xIncludeIn_ActiveEmployee As Boolean, _
                            ByVal strTableName As String, _
                            Optional ByVal intEmpId As Integer = -1, _
                            Optional ByVal xFilterString As String = "", _
                            Optional ByVal blnApplyUACFilter As Boolean = True) As DataSet
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim objDataOperation As clsDataOperation
        If mobjDataOperation Is Nothing Then
            objDataOperation = New clsDataOperation
        Else
            objDataOperation = mobjDataOperation
        End If
        objDataOperation.ClearParameters()

        Dim xDateJoinQry, xDateFilterQry, xUACQry, xUACFiltrQry, xAdvanceJoinQry As String
        xDateJoinQry = "" : xDateFilterQry = "" : xUACQry = "" : xUACFiltrQry = "" : xAdvanceJoinQry = ""
        Call GetDatesFilterString(xDateJoinQry, xDateFilterQry, xPeriodStart, xPeriodEnd, , , xDatabaseName)
        If blnApplyUACFilter Then
            Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, xPeriodEnd, xOnlyApproved, xDatabaseName, xUserUnkid, xCompanyUnkid, xYearUnkid, xUserModeSetting)
        End If
        Call GetAdvanceFilterQry(xAdvanceJoinQry, xPeriodEnd, xDatabaseName)

        Try
            strQ = "SELECT " & _
                   "     CAST(0 AS BIT) AS ischeck " & _
                   "    ,reference_no " & _
                   "    ,chargedate " & _
                   "    ,interdictiondate AS interdictiondate " & _
                   "    ,hremployee_master.employeecode + ' - ' + hremployee_master.firstname + ' '+ hremployee_master.surname AS involved_employee " & _
                   "    ,charge_description " & _
                   "    ,Cnt.NoOfCount " & _
                   "    ,notification_date AS NotificationDate " & _
                   "    ,fhdate AS FirstDateHearing " & _
                   "    ,nhdate AS NextHearningDate " & _
                   "    ,Charge_Status " & _
                   "    ,ISNULL(Category,'') AS Category " & _
                   "    ,ISNULL(CategoryId,0) AS CategoryId " & _
                   "    ,ISNULL(hremployee_master.email,'') AS Email " & _
                   "    ,hrdiscipline_file_master.disciplinefileunkid " & _
                   "    ,hrdiscipline_file_master.involved_employeeunkid " & _
                   "    ,hrdiscipline_file_master.disciplinestatusunkid " & _
                   "    ,hrdiscipline_file_master.emailtranunkid " & _
                   "    ,hrdiscipline_file_master.emaildate " & _
                   "    ,hrdiscipline_file_master.userunkid " & _
                   "    ,hrdiscipline_file_master.isvoid " & _
                   "    ,hrdiscipline_file_master.voiduserunkid " & _
                   "    ,hrdiscipline_file_master.voidreason " & _
                   "    ,hrdiscipline_file_master.voiddatetime " & _
                   "    ,hrdiscipline_file_master.loginemployeeunkid " & _
                   "    ,hrdiscipline_file_master.notifyemployee " & _
                   "    ,StatusId " & _
                   "    ,isfinal " & _
                   "    ,isvisibleoness " & _
                   "    ,ISNULL(hrdiscipline_file_master.postuserunkid,0) AS postuserunkid " & _
                   "FROM hrdiscipline_file_master " & _
                   "JOIN " & _
                   "( " & _
                   "    SELECT " & _
                   "         hrdiscipline_file_tran.disciplinefileunkid " & _
                   "        ,COUNT(1) AS NoOfCount " & _
                   "    FROM hrdiscipline_file_tran " & _
                   "    WHERE hrdiscipline_file_tran.isvoid = 0 " & _
                   "    GROUP BY hrdiscipline_file_tran.disciplinefileunkid " & _
                   ") AS Cnt ON Cnt.disciplinefileunkid = hrdiscipline_file_master.disciplinefileunkid " & _
                   "JOIN hremployee_master ON hremployee_master.employeeunkid = hrdiscipline_file_master.involved_employeeunkid " & _
                   "LEFT JOIN " & _
                   "( " & _
                   "    SELECT " & _
                   "         CASE WHEN isexternal = 0 THEN @INTERNAL WHEN isexternal = 1 THEN @EXTERNAL END AS Category " & _
                   "        ,disciplinefileunkid AS FileUnkid " & _
                   "        ,disciplinestatusunkid AS StatusId " & _
                   "        ,statustranunkid AS StatusTranId " & _
                   "        ,CASE WHEN isexternal = 0 THEN 1 WHEN isexternal = 1 THEN 2 END AS CategoryId " & _
                   "        ,Charge_Status " & _
                   "    FROM " & _
                   "    ( " & _
                   "        SELECT " & _
                   "             hrdiscipline_status_tran.isexternal " & _
                   "            ,hrdiscipline_status_tran.disciplinefileunkid " & _
                   "            ,hrdiscipline_status_tran.disciplinestatusunkid " & _
                   "            ,hrdiscipline_status_tran.statustranunkid " & _
                   "            ,hrdisciplinestatus_master.name AS Charge_Status " & _
                   "            ,ROW_NUMBER() OVER(PARTITION BY hrdiscipline_status_tran.disciplinefileunkid ORDER BY hrdiscipline_status_tran.statustranunkid DESC) AS RNO " & _
                   "        FROM hrdiscipline_status_tran " & _
                   "            JOIN hrdisciplinestatus_master ON hrdiscipline_status_tran.disciplinestatusunkid = hrdisciplinestatus_master.disciplinestatusunkid " & _
                   "        WHERE isvoid = 0 " & _
                   "    ) AS Category " & _
                   "WHERE RNO = 1 " & _
                   ") AS A ON A.FileUnkid = hrdiscipline_file_master.disciplinefileunkid " & _
                   "LEFT JOIN " & _
                   "( " & _
                   "    SELECT " & _
                   "         FHD.disciplinefileunkid " & _
                   "        ,FHD.hearing_date AS fhdate " & _
                   "    FROM hrhearing_schedule_master AS FHD " & _
                   "    JOIN " & _
                   "    ( " & _
                   "        SELECT " & _
                   "            FSM.disciplinefileunkid " & _
                   "           ,MIN(FSM.hearingschedulemasterunkid) AS FHId " & _
                   "        FROM hrhearing_schedule_master AS FSM " & _
                   "        WHERE FSM.isvoid = 0 " & _
                   "        GROUP BY FSM.disciplinefileunkid " & _
                   "    ) AS FD ON FD.disciplinefileunkid = FHD.disciplinefileunkid AND FHD.hearingschedulemasterunkid = FD.FHId " & _
                   "    WHERE FHD.isvoid = 0 " & _
                   ") AS FH ON FH.disciplinefileunkid  = hrdiscipline_file_master.disciplinefileunkid " & _
                   "LEFT JOIN " & _
                   "( " & _
                   "    SELECT " & _
                   "         NHD.disciplinefileunkid " & _
                   "        ,NHD.hearing_date AS nhdate " & _
                   "    FROM hrhearing_schedule_master AS NHD " & _
                   "    JOIN " & _
                   "    ( " & _
                   "        SELECT " & _
                   "            NSM.disciplinefileunkid " & _
                   "           ,MAX(NSM.hearingschedulemasterunkid) AS NHId " & _
                   "        FROM hrhearing_schedule_master AS NSM " & _
                   "        WHERE NSM.isvoid = 0 AND NSM.status = 1 " & _
                   "        GROUP BY NSM.disciplinefileunkid " & _
                   "        HAVING COUNT(NSM.disciplinefileunkid) > 1 " & _
                   "    ) AS ND ON ND.disciplinefileunkid = NHD.disciplinefileunkid AND NHD.hearingschedulemasterunkid = ND.NHId " & _
                   "    WHERE NHD.isvoid = 0 " & _
                   ") AS NH ON NH.disciplinefileunkid  = hrdiscipline_file_master.disciplinefileunkid "
            'S.SANDEEP |02-MAR-2022| -- START {postuserunkid} -- END
            'S.SANDEEP |11-NOV-2019| -- START {isvisibleoness} -- END

            If xDateJoinQry.Trim.Length > 0 Then
                strQ &= xDateJoinQry
            End If

            'S.SANDEEP [15 NOV 2016] -- START
            If xUACQry.Trim.Length > 0 Then
                strQ &= xUACQry
            End If
            'S.SANDEEP [15 NOV 2016] -- END

            strQ &= " WHERE hrdiscipline_file_master.isvoid = 0 "

            If xIncludeIn_ActiveEmployee = False Then
                If xDateFilterQry.Trim.Length > 0 Then
                    strQ &= xDateFilterQry
                End If
            End If

            If xUACFiltrQry.Trim.Length > 0 Then
                strQ &= " AND " & xUACFiltrQry & " "
            End If

            If xFilterString.Trim.Length > 0 Then
                strQ &= " AND " & xFilterString
            End If

            objDataOperation.AddParameter("@INTERNAL", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 3, "Internal"))
            objDataOperation.AddParameter("@EXTERNAL", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 4, "External"))


            dsList = objDataOperation.ExecQuery(strQ, strTableName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If mobjDataOperation Is Nothing Then objDataOperation = Nothing
        End Try
        Return dsList
    End Function

    ''' <summary>
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> INSERT INTO Database Table (hrdiscipline_file_master) </purpose>
    Public Function Insert(ByVal dtCurrentDateTime As DateTime, _
                           ByVal intRefNoType As Integer, _
                           ByVal strRefNoPrefix As String, _
                           ByVal intCompanyId As Integer, _
                           ByVal dtScanAttachment As DataTable, _
                           ByVal strDocumentPath As String, _
                           ByVal strLstName As List(Of String), _
                           ByVal dtStartDate As Date, _
                           ByVal dtEndDate As Date, _
                           ByVal strDataBaseName As String, _
                           ByVal strScreenName As String, _
                           Optional ByVal mdtChargesTran As DataTable = Nothing) As Boolean 'S.SANDEEP [26 JUL 2016] -- START {strDocumentPath} -- END

        If intRefNoType = 0 Then
            If isExist(mstrReference_No, Nothing, -1, "") Then
                mstrMessage = Language.getMessage(mstrModuleName, 1, "Sorry, this reference number already exists. Please assign new reference number.")
                Return False
            End If
        End If

        If isExist("", mdtChargedate, mintInvolved_Employeeunkid, mstrCharge_Description) Then
            mstrMessage = Language.getMessage(mstrModuleName, 2, "This discipline charge is already filed for selected employee. Please add new discipline charge.")
            Return False
        End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim objDataOperation As clsDataOperation
        If mobjDataOperation Is Nothing Then
            objDataOperation = New clsDataOperation
        Else
            objDataOperation = mobjDataOperation
        End If
        objDataOperation.ClearParameters()
        objDataOperation.BindTransaction()
        Try
            objDataOperation.AddParameter("@reference_no", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrReference_No.ToString)
            objDataOperation.AddParameter("@chargedate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtChargedate)
            If mdtInterdictiondate <> Nothing Then
                objDataOperation.AddParameter("@interdictiondate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtInterdictiondate)
            Else
                objDataOperation.AddParameter("@interdictiondate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            End If
            objDataOperation.AddParameter("@charge_description", SqlDbType.VarChar, mstrCharge_Description.Length, mstrCharge_Description.ToString)
            objDataOperation.AddParameter("@involved_employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintInvolved_Employeeunkid.ToString)
            objDataOperation.AddParameter("@disciplinestatusunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintDisciplinestatusunkid.ToString)
            objDataOperation.AddParameter("@emailtranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmailtranunkid.ToString)
            If mdtEmaildate <> Nothing Then
                objDataOperation.AddParameter("@emaildate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtEmaildate.ToString)
            Else
                objDataOperation.AddParameter("@emaildate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            End If
            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid)
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)
            If mdtVoiddatetime <> Nothing Then
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime)
            Else
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            End If
            objDataOperation.AddParameter("@loginemployeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLoginemployeeId)
            objDataOperation.AddParameter("@notifyemployee", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnNotifyEmployee)
            If mdtNotificationDate <> Nothing Then
                objDataOperation.AddParameter("@notification_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtNotificationDate)
            Else
                objDataOperation.AddParameter("@notification_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            End If
            objDataOperation.AddParameter("@isfinal", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsFinal)
            'S.SANDEEP |11-NOV-2019| -- START
            'ISSUE/ENHANCEMENT : NMB DISCIPLINARY UAT COMMENTS
            objDataOperation.AddParameter("@isvisibleoness", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsVisibleonEss)
            'S.SANDEEP |11-NOV-2019| -- END

            'S.SANDEEP |02-MAR-2022| -- START
            objDataOperation.AddParameter("@postuserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPostuserunkid)
            'S.SANDEEP |02-MAR-2022| -- END

            strQ = "INSERT INTO hrdiscipline_file_master ( " & _
                       "  reference_no " & _
                       ", chargedate " & _
                       ", interdictiondate " & _
                       ", charge_description " & _
                       ", involved_employeeunkid " & _
                       ", disciplinestatusunkid " & _
                       ", emailtranunkid " & _
                       ", emaildate " & _
                       ", userunkid " & _
                       ", isvoid " & _
                       ", voiduserunkid " & _
                       ", voidreason " & _
                       ", voiddatetime " & _
                       ", loginemployeeunkid " & _
                       ", notifyemployee " & _
                       ", notification_date " & _
                       ", isfinal " & _
                       ", isvisibleoness " & _
                       ", postuserunkid " & _
                   ") VALUES (" & _
                       "  @reference_no " & _
                       ", @chargedate " & _
                       ", @interdictiondate " & _
                       ", @charge_description " & _
                       ", @involved_employeeunkid " & _
                       ", @disciplinestatusunkid " & _
                       ", @emailtranunkid " & _
                       ", @emaildate " & _
                       ", @userunkid " & _
                       ", @isvoid " & _
                       ", @voiduserunkid " & _
                       ", @voidreason " & _
                       ", @voiddatetime " & _
                       ", @loginemployeeunkid " & _
                       ", @notifyemployee " & _
                       ", @notification_date " & _
                       ", @isfinal " & _
                       ", @isvisibleoness " & _
                       ", @postuserunkid " & _
                   "); SELECT @@identity"
            'S.SANDEEP |02-MAR-2022| -- START {postuserunkid} -- END
            'S.SANDEEP |11-NOV-2019| -- START {isvisibleoness} -- END

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            mintDisciplinefileunkid = dsList.Tables(0).Rows(0).Item(0)

            If intRefNoType = 1 Then
                If Set_AutoNumber(objDataOperation, mintDisciplinefileunkid, "hrdiscipline_file_master", "reference_no", "disciplinefileunkid", "NextDisciplineRefNo", strRefNoPrefix, intCompanyId) = False Then
                    If objDataOperation.ErrorMessage <> "" Then
                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If
                End If

                If Get_Saved_Number(objDataOperation, mintDisciplinefileunkid, "hrdiscipline_file_master", "reference_no", "disciplinefileunkid", mstrReference_No) = False Then
                    If objDataOperation.ErrorMessage <> "" Then
                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If
                End If
            End If

            If mdtChargesTran IsNot Nothing Then

                objDiscFileTran._objDataOperation = objDataOperation
                objDiscFileTran._Disciplinefileunkid = mintDisciplinefileunkid
                objDiscFileTran._ChargesTable = mdtChargesTran.Copy
                objDiscFileTran._FormName = mstrFormName
                objDiscFileTran._LoginEmployeeunkid = mintLoginEmployeeunkid
                objDiscFileTran._ClientIP = mstrClientIP
                objDiscFileTran._HostName = mstrHostName
                objDiscFileTran._FromWeb = mblnIsWeb
                objDiscFileTran._AuditUserId = mintAuditUserId
objDiscFileTran._CompanyUnkid = mintCompanyUnkid
                objDiscFileTran._AuditDate = mdtAuditDate

                objDiscFileTran._ChargesTable = mdtChargesTran

                If objDiscFileTran.InsertUpdateDelete_DisciplineCharges(objDataOperation, mintUserunkid, dtCurrentDateTime, dtScanAttachment, strScreenName) = False Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If

            End If

            objDiscStatus._objDataOperation = objDataOperation
            objDiscStatus._Disciplinefileunkid = mintDisciplinefileunkid
            objDiscStatus._Disciplinestatusunkid = mintDisciplinestatusunkid
            objDiscStatus._Isvoid = False
            objDiscStatus._Remark = ""
            objDiscStatus._Statusdate = mdtChargedate
            objDiscStatus._Userunkid = mintUserunkid
            objDiscStatus._Voiddatetime = Nothing
            objDiscStatus._Voidreason = ""
            objDiscStatus._Voiduserunkid = -1
            objDiscStatus._Isexternal = mblnIsExternal
            objDiscStatus._LoginEmployeeunkid = mintLoginEmployeeunkid
            objDiscStatus._ClientIP = mstrClientIP
            objDiscStatus._HostName = mstrHostName
            objDiscStatus._FromWeb = mblnIsWeb
            objDiscStatus._AuditUserId = mintAuditUserId
objDiscStatus._CompanyUnkid = mintCompanyUnkid
            objDiscStatus._AuditDate = mdtAuditDate

            If objDiscStatus.Insert() = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If InsertDisciplineFileAuditTrails(dtCurrentDateTime, enAuditType.ADD, objDataOperation) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If


            'S.SANDEEP [24 MAY 2016] -- START
            'ENHANCEMENT : DISCIPLINE MAKE-OVER {BY ANDREW}

            'Pinkal (08-May-2021)-- Start
            'NMB Enhancement  -  Working on Disciplinary Module Enhancement for NMB.
            If dtScanAttachment IsNot Nothing Then
                For Each drow As DataRow In dtScanAttachment.Rows
                    drow("transactionunkid") = mintDisciplinefileunkid
                    drow("userunkid") = mintUserunkid
                Next

                Dim objDocument As New clsScan_Attach_Documents
                objDocument._Datatable = dtScanAttachment
                objDocument.InsertUpdateDelete_Documents(objDataOperation)

                If objDataOperation.ErrorMessage <> "" Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If
            End If

            'Pinkal (08-May-2021)-- End

            'S.SANDEEP [24 MAY 2016] -- END


            If mobjDataOperation Is Nothing Then objDataOperation.ReleaseTransaction(True)

            'S.SANDEEP [24 MAY 2016] -- Start
            'Email Notification
            If mblnNotifyEmployee = True AndAlso mdtNotificationDate = Nothing Then
                Call SendMailToEmployee(mdtChargesTran, mintDisciplinefileunkid, intCompanyId, dtScanAttachment, strDocumentPath, strLstName, dtStartDate, dtEndDate, strDataBaseName, dtCurrentDateTime)
            End If
            'S.SANDEEP [24 MAY 2016] -- End

            'S.SANDEEP |12-NOV-2020| -- START
            'ISSUE/ENHANCEMENT : SPRINT-6
            Call SendMailToSelecteUsers(mdtChargesTran, mintDisciplinefileunkid, intCompanyId, dtScanAttachment, strDocumentPath, strLstName, dtStartDate, dtEndDate, strDataBaseName, dtCurrentDateTime, mintInvolved_Employeeunkid, mstrCharge_Description)
            'S.SANDEEP |12-NOV-2020| -- END

            Return True
        Catch ex As Exception
            If mobjDataOperation Is Nothing Then objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Insert; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If mobjDataOperation Is Nothing Then objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> INSERT INTO Database Table (hrdiscipline_file_master) </purpose>
    Public Function Update(ByVal dtCurrentDateTime As DateTime, ByVal dtScanAttachment As DataTable, ByVal strScreenName As String, _
                           ByVal intCompanyId As Integer, ByVal strDocumentPath As String, ByVal strLstName As List(Of String), ByVal dtStartDate As Date, _
                           ByVal dtEndDate As Date, ByVal strDataBaseName As String, ByVal intYearId As Integer, Optional ByVal mdtChargesTran As DataTable = Nothing, Optional ByVal IsFromResponse As Boolean = False, Optional ByVal strSelfServiceUrl As String = "", _
                           Optional ByVal isSaveClose As Boolean = False) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim objDataOperation As clsDataOperation
        If mobjDataOperation Is Nothing Then
            objDataOperation = New clsDataOperation
        Else
            objDataOperation = mobjDataOperation
        End If

        If isExist(mstrReference_No, Nothing, -1, "", mintDisciplinefileunkid) Then
            mstrMessage = Language.getMessage(mstrModuleName, 1, "Sorry, this reference number already exists. Please assign new reference number.")
            Return False
        End If

        If isExist("", mdtChargedate, mintInvolved_Employeeunkid, mstrCharge_Description, mintDisciplinefileunkid) Then
            mstrMessage = Language.getMessage(mstrModuleName, 2, "This discipline charge is already filed for selected employee. Please add new discipline charge.")
            Return False
        End If

        objDataOperation.ClearParameters()
        objDataOperation.BindTransaction()
        Try
            objDataOperation.AddParameter("@disciplinefileunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintDisciplinefileunkid)
            objDataOperation.AddParameter("@reference_no", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrReference_No.ToString)
            objDataOperation.AddParameter("@chargedate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtChargedate)
            If mdtInterdictiondate <> Nothing Then
                objDataOperation.AddParameter("@interdictiondate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtInterdictiondate)
            Else
                objDataOperation.AddParameter("@interdictiondate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            End If
            objDataOperation.AddParameter("@charge_description", SqlDbType.VarChar, mstrCharge_Description.Length, mstrCharge_Description.ToString)
            objDataOperation.AddParameter("@involved_employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintInvolved_Employeeunkid.ToString)
            objDataOperation.AddParameter("@disciplinestatusunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintDisciplinestatusunkid.ToString)
            objDataOperation.AddParameter("@emailtranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmailtranunkid.ToString)
            If mdtEmaildate <> Nothing Then
                objDataOperation.AddParameter("@emaildate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtEmaildate.ToString)
            Else
                objDataOperation.AddParameter("@emaildate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            End If
            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid)
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)
            If mdtVoiddatetime <> Nothing Then
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime.ToString)
            Else
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            End If
            objDataOperation.AddParameter("@loginemployeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLoginemployeeId)
            objDataOperation.AddParameter("@notifyemployee", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnNotifyEmployee)
            If mdtNotificationDate <> Nothing Then
                objDataOperation.AddParameter("@notification_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtNotificationDate)
            Else
                objDataOperation.AddParameter("@notification_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            End If
            objDataOperation.AddParameter("@isfinal", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsFinal)
            'S.SANDEEP |11-NOV-2019| -- START
            'ISSUE/ENHANCEMENT : NMB DISCIPLINARY UAT COMMENTS
            objDataOperation.AddParameter("@isvisibleoness", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsVisibleonEss)
            'S.SANDEEP |11-NOV-2019| -- END

            strQ = "UPDATE hrdiscipline_file_master " & _
                   "SET  reference_no = @reference_no " & _
                   "    ,chargedate = @chargedate " & _
                   "    ,interdictiondate = @interdictiondate " & _
                   "    ,charge_description = @charge_description " & _
                   "    ,involved_employeeunkid = @involved_employeeunkid " & _
                   "    ,disciplinestatusunkid = @disciplinestatusunkid " & _
                   "    ,emailtranunkid = @emailtranunkid " & _
                   "    ,emaildate = @emaildate " & _
                   "    ,userunkid = @userunkid " & _
                   "    ,isvoid = @isvoid " & _
                   "    ,voiduserunkid = @voiduserunkid " & _
                   "    ,voidreason = @voidreason " & _
                   "    ,voiddatetime = @voiddatetime " & _
                   "    ,loginemployeeunkid = @loginemployeeunkid " & _
                   "    ,notifyemployee = @notifyemployee " & _
                   "    ,notification_date = @notification_date " & _
                   "    ,isfinal = @isfinal " & _
                   "    ,isvisibleoness = @isvisibleoness " & _
                   "WHERE disciplinefileunkid = @disciplinefileunkid "
            'S.SANDEEP |11-NOV-2019| -- START {isvisibleoness} -- END

            objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If mdtChargesTran IsNot Nothing Then

                objDiscFileTran._objDataOperation = objDataOperation
                objDiscFileTran._Disciplinefileunkid = mintDisciplinefileunkid
                objDiscFileTran._FormName = mstrFormName
                objDiscFileTran._LoginEmployeeunkid = mintLoginEmployeeunkid
                objDiscFileTran._ClientIP = mstrClientIP
                objDiscFileTran._HostName = mstrHostName
                objDiscFileTran._FromWeb = mblnIsWeb
                objDiscFileTran._AuditUserId = mintAuditUserId
objDiscFileTran._CompanyUnkid = mintCompanyUnkid
                objDiscFileTran._AuditDate = mdtAuditDate
                objDiscFileTran._ChargesTable = mdtChargesTran

                If objDiscFileTran.InsertUpdateDelete_DisciplineCharges(objDataOperation, mintUserunkid, dtCurrentDateTime, dtScanAttachment, strScreenName) = False Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If

            End If

            If isSaveClose Then
                objDiscStatus._objDataOperation = objDataOperation
                objDiscStatus._Disciplinefileunkid = mintDisciplinefileunkid
                objDiscStatus._Disciplinestatusunkid = mintDisciplinestatusunkid
                objDiscStatus._Isvoid = False
                objDiscStatus._Remark = ""
                objDiscStatus._Statusdate = mdtChargedate
                objDiscStatus._Userunkid = mintUserunkid
                objDiscStatus._Voiddatetime = Nothing
                objDiscStatus._Voidreason = ""
                objDiscStatus._Voiduserunkid = -1
                objDiscStatus._Isexternal = mblnIsExternal

                If objDiscStatus.Insert() = False Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If
            End If

            If InsertDisciplineFileAuditTrails(dtCurrentDateTime, enAuditType.EDIT, objDataOperation) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            'S.SANDEEP [24 MAY 2016] -- START
            'ENHANCEMENT : DISCIPLINE MAKE-OVER {BY ANDREW}

            'Pinkal (08-May-2021)-- Start
            'NMB Enhancement  -  Working on Disciplinary Module Enhancement for NMB.
            If dtScanAttachment IsNot Nothing Then
                Dim drRow = dtScanAttachment.AsEnumerable().Where(Function(x) x.Field(Of String)("AUD") <> "")
                If drRow.Count > 0 Then
                    Dim dtTable As DataTable = drRow.CopyToDataTable()
                    For Each drow As DataRow In dtTable.Rows
                        drow("transactionunkid") = mintDisciplinefileunkid
                        drow("userunkid") = mintUserunkid
                    Next

                    Dim objDocument As New clsScan_Attach_Documents
                    objDocument._Datatable = dtTable
                    objDocument.InsertUpdateDelete_Documents(objDataOperation)

                    If objDataOperation.ErrorMessage <> "" Then
                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If
                End If
            End If
            'Pinkal (08-May-2021) -- End

            'S.SANDEEP [24 MAY 2016] -- END

            If mblnIsFinal AndAlso IsFromResponse = True Then
                Call SendMailToReportingTo(mintDisciplinefileunkid, intCompanyId, strDocumentPath, strLstName, dtStartDate, dtEndDate, strDataBaseName, mintInvolved_Employeeunkid, dtScanAttachment, intYearId)
            End If

            If mobjDataOperation Is Nothing Then objDataOperation.ReleaseTransaction(True)

            Return True
        Catch ex As Exception
            If mobjDataOperation Is Nothing Then objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
        Finally
            If mobjDataOperation Is Nothing Then objDataOperation = Nothing
        End Try
    End Function

    Public Function Update(ByVal dtCurrentDateTime As DateTime) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim objDataOperation As clsDataOperation
        If mobjDataOperation Is Nothing Then
            objDataOperation = New clsDataOperation
        Else
            objDataOperation = mobjDataOperation
        End If

        If isExist(mstrReference_No, Nothing, -1, "", mintDisciplinefileunkid) Then
            mstrMessage = Language.getMessage(mstrModuleName, 1, "Sorry, this reference number already exists. Please assign new reference number.")
            Return False
        End If

        If isExist("", mdtChargedate, mintInvolved_Employeeunkid, mstrCharge_Description, mintDisciplinefileunkid) Then
            mstrMessage = Language.getMessage(mstrModuleName, 2, "This discipline charge is already filed for selected employee. Please add new discipline charge.")
            Return False
        End If

        objDataOperation.ClearParameters()
        objDataOperation.BindTransaction()
        Try
            objDataOperation.AddParameter("@disciplinefileunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintDisciplinefileunkid)
            objDataOperation.AddParameter("@reference_no", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrReference_No.ToString)
            objDataOperation.AddParameter("@chargedate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtChargedate)
            If mdtInterdictiondate <> Nothing Then
                objDataOperation.AddParameter("@interdictiondate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtInterdictiondate)
            Else
                objDataOperation.AddParameter("@interdictiondate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            End If
            objDataOperation.AddParameter("@charge_description", SqlDbType.VarChar, mstrCharge_Description.Length, mstrCharge_Description.ToString)
            objDataOperation.AddParameter("@involved_employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintInvolved_Employeeunkid.ToString)
            objDataOperation.AddParameter("@disciplinestatusunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintDisciplinestatusunkid.ToString)
            objDataOperation.AddParameter("@emailtranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmailtranunkid.ToString)
            If mdtEmaildate <> Nothing Then
                objDataOperation.AddParameter("@emaildate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtEmaildate.ToString)
            Else
                objDataOperation.AddParameter("@emaildate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            End If
            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid)
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)
            If mdtVoiddatetime <> Nothing Then
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime.ToString)
            Else
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            End If
            objDataOperation.AddParameter("@loginemployeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLoginemployeeId)
            objDataOperation.AddParameter("@notifyemployee", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnNotifyEmployee)
            If mdtNotificationDate <> Nothing Then
                objDataOperation.AddParameter("@notification_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtNotificationDate)
            Else
                objDataOperation.AddParameter("@notification_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            End If
            objDataOperation.AddParameter("@isfinal", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsFinal)
            'S.SANDEEP |11-NOV-2019| -- START
            'ISSUE/ENHANCEMENT : NMB DISCIPLINARY UAT COMMENTS
            objDataOperation.AddParameter("@isvisibleoness", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsVisibleonEss)
            'S.SANDEEP |11-NOV-2019| -- END

            strQ = "UPDATE hrdiscipline_file_master " & _
                   "SET  reference_no = @reference_no " & _
                   "    ,chargedate = @chargedate " & _
                   "    ,interdictiondate = @interdictiondate " & _
                   "    ,charge_description = @charge_description " & _
                   "    ,involved_employeeunkid = @involved_employeeunkid " & _
                   "    ,disciplinestatusunkid = @disciplinestatusunkid " & _
                   "    ,emailtranunkid = @emailtranunkid " & _
                   "    ,emaildate = @emaildate " & _
                   "    ,userunkid = @userunkid " & _
                   "    ,isvoid = @isvoid " & _
                   "    ,voiduserunkid = @voiduserunkid " & _
                   "    ,voidreason = @voidreason " & _
                   "    ,voiddatetime = @voiddatetime " & _
                   "    ,loginemployeeunkid = @loginemployeeunkid " & _
                   "    ,notifyemployee = @notifyemployee " & _
                   "    ,notification_date = @notification_date " & _
                   "    ,isfinal = @isfinal " & _
                   "    ,isvisibleoness = @isvisibleoness " & _
                   "WHERE disciplinefileunkid = @disciplinefileunkid "
            'S.SANDEEP |11-NOV-2019| -- START {isvisibleoness} -- END

            objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If InsertDisciplineFileAuditTrails(dtCurrentDateTime, enAuditType.EDIT, objDataOperation) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If mobjDataOperation Is Nothing Then objDataOperation.ReleaseTransaction(True)

            Return True
        Catch ex As Exception
            If mobjDataOperation Is Nothing Then objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
        Finally
            If mobjDataOperation Is Nothing Then objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isExist(ByVal strDisciplineRefNo As String, _
                            ByVal dtDisciplineDate As DateTime, _
                            ByVal intPersonInvolvedunkid As Integer, _
                            ByVal strChargeDescription As String, _
                            Optional ByVal intUnkid As Integer = -1) As Boolean

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim objDataOperation As clsDataOperation
        If mobjDataOperation Is Nothing Then
            objDataOperation = New clsDataOperation
        Else
            objDataOperation = mobjDataOperation
        End If
        objDataOperation.ClearParameters()
        Try
            strQ = "SELECT disciplinefileunkid " & _
                   "    ,reference_no " & _
                   "    ,chargedate " & _
                   "    ,interdictiondate " & _
                   "    ,charge_description " & _
                   "    ,involved_employeeunkid " & _
                   "    ,disciplinestatusunkid " & _
                   "    ,emailtranunkid " & _
                   "    ,emaildate " & _
                   "    ,userunkid " & _
                   "    ,isvoid " & _
                   "    ,voiduserunkid " & _
                   "    ,voidreason " & _
                   "    ,voiddatetime " & _
                   "    ,notifyemployee " & _
                   "    ,isvisibleoness " & _
                   "    ,ISNULL(postuserunkid,0) AS postuserunkid " & _
                   "FROM hrdiscipline_file_master " & _
                   "WHERE hrdiscipline_file_master.isvoid = 0 "
            'S.SANDEEP |02-MAR-2022| -- START {postuserunkid} -- END
            'S.SANDEEP |11-NOV-2019| -- START {isvisibleoness} -- END

            If strDisciplineRefNo.Trim.Length > 0 Then
                strQ &= " AND hrdiscipline_file_master.reference_no = @reference_no "
                objDataOperation.AddParameter("@reference_no", SqlDbType.NVarChar, 700, mstrReference_No)
            End If

            If dtDisciplineDate <> Nothing Then
                strQ &= " AND CONVERT(NVARCHAR(8),hrdiscipline_file_master.chargedate,112) = @chargedate "
                objDataOperation.AddParameter("@chargedate", SqlDbType.NVarChar, eZeeDataType.DATETIME_SIZE, eZeeDate.convertDate(dtDisciplineDate))
            End If

            If intPersonInvolvedunkid > 0 Then
                strQ &= " AND hrdiscipline_file_master.involved_employeeunkid = @involved_employeeunkid "
                objDataOperation.AddParameter("@involved_employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intPersonInvolvedunkid)
            End If

            If strChargeDescription.Trim.Length > 0 Then
                strQ &= " AND hrdiscipline_file_master.charge_description = @charge_description "
                objDataOperation.AddParameter("@charge_description", SqlDbType.VarChar, strChargeDescription.Length, strChargeDescription)
            End If

            If intUnkid > 0 Then
                strQ &= " AND hrdiscipline_file_master.disciplinefileunkid <> @disciplinefileunkid "
                objDataOperation.AddParameter("@disciplinefileunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)
            End If

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables(0).Rows.Count > 0

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isExist; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If mobjDataOperation Is Nothing Then objDataOperation = Nothing
        End Try
    End Function

    Private Function InsertDisciplineFileAuditTrails(ByVal dtCurrentDate As DateTime, ByVal eAudit As enAuditType, ByVal objDataOperation As clsDataOperation) As Boolean
        Dim StrQ As String = ""
        Dim exForce As Exception = Nothing
        Try
            StrQ = "INSERT INTO athrdiscipline_file_master " & _
                    "( " & _
                          "disciplinefileunkid " & _
                         ",reference_no " & _
                         ",chargedate " & _
                         ",interdictiondate " & _
                         ",charge_description " & _
                         ",involved_employeeunkid " & _
                         ",disciplinestatusunkid " & _
                         ",emailtranunkid " & _
                         ",emaildate " & _
                         ",audittype " & _
                         ",audituserunkid " & _
                         ",auditdatetime " & _
                         ",ip " & _
                         ",machine_name " & _
                         ",form_name " & _
                         ",isweb " & _
                         ",loginemployeeunkid " & _
                         ",notifyemployee " & _
                         ",isfinal " & _
                         ",isvisibleoness " & _
                         ",postuserunkid " & _
                    ") " & _
                    "VALUES " & _
                    "( " & _
                          "@disciplinefileunkid " & _
                         ",@reference_no " & _
                         ",@chargedate " & _
                         ",@interdictiondate " & _
                         ",@charge_description " & _
                         ",@involved_employeeunkid " & _
                         ",@disciplinestatusunkid " & _
                         ",@emailtranunkid " & _
                         ",@emaildate " & _
                         ",@audittype " & _
                         ",@audituserunkid " & _
                         ",@auditdatetime " & _
                         ",@ip " & _
                         ",@machine_name " & _
                         ",@form_name " & _
                         ",@isweb " & _
                         ",@loginemployeeunkid " & _
                         ",@notifyemployee " & _
                         ",@isfinal " & _
                         ",@isvisibleoness " & _
                         ",@postuserunkid " & _
                    ") "
            'S.SANDEEP |02-MAR-2022| -- START {postuserunkid} -- END
            'S.SANDEEP |11-NOV-2019| -- START {isvisibleoness} -- END

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@disciplinefileunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintDisciplinefileunkid)
            objDataOperation.AddParameter("@reference_no", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrReference_No.ToString)
            objDataOperation.AddParameter("@chargedate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtChargedate)
            If mdtInterdictiondate <> Nothing Then
                objDataOperation.AddParameter("@interdictiondate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtInterdictiondate)
            Else
                objDataOperation.AddParameter("@interdictiondate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            End If
            objDataOperation.AddParameter("@charge_description", SqlDbType.VarChar, mstrCharge_Description.Length, mstrCharge_Description.ToString)
            objDataOperation.AddParameter("@involved_employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintInvolved_Employeeunkid.ToString)
            objDataOperation.AddParameter("@disciplinestatusunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintDisciplinestatusunkid.ToString)
            objDataOperation.AddParameter("@emailtranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmailtranunkid.ToString)
            If mdtEmaildate <> Nothing Then
                objDataOperation.AddParameter("@emaildate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtEmaildate)
            Else
                objDataOperation.AddParameter("@emaildate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            End If
            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid)
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)
            If mdtVoiddatetime <> Nothing Then
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime)
            Else
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            End If
            objDataOperation.AddParameter("@loginemployeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLoginemployeeId)
            objDataOperation.AddParameter("@notifyemployee", SqlDbType.Int, eZeeDataType.INT_SIZE, mblnNotifyEmployee)
            objDataOperation.AddParameter("@audittype", SqlDbType.Int, eZeeDataType.INT_SIZE, eAudit)
            objDataOperation.AddParameter("@audituserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid)
            objDataOperation.AddParameter("@auditdatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, dtCurrentDate)
            objDataOperation.AddParameter("@ip", SqlDbType.NVarChar, eZeeDataType.IP_SIZE, mstrClientIP)
            objDataOperation.AddParameter("@machine_name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrHostName)
            objDataOperation.AddParameter("@form_name", SqlDbType.NVarChar, 500, mstrFormName)
            objDataOperation.AddParameter("@isweb", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsWeb)
            objDataOperation.AddParameter("@isfinal", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsFinal)
            'S.SANDEEP |11-NOV-2019| -- START
            'ISSUE/ENHANCEMENT : NMB DISCIPLINARY UAT COMMENTS
            objDataOperation.AddParameter("@isvisibleoness", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsVisibleonEss)
            'S.SANDEEP |11-NOV-2019| -- END

            'S.SANDEEP |02-MAR-2022| -- START
            objDataOperation.AddParameter("@postuserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPostuserunkid)
            'S.SANDEEP |02-MAR-2022| -- END

            objDataOperation.ExecNonQuery(StrQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return True

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: InsertDisciplineFileAuditTrails; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Function

    Public Function IsInterdictionDateExists(ByVal dtDate As Date, ByVal strRefNo As String, ByRef dtOrginalDate As Date) As Boolean
        Dim StrQ As String = String.Empty
        Dim exForce As Exception
        Dim dsList As New DataSet
        Dim blnFlag As Boolean = False
        Dim objDataOperation As clsDataOperation
        If mobjDataOperation Is Nothing Then
            objDataOperation = New clsDataOperation
        Else
            objDataOperation = mobjDataOperation
        End If
        objDataOperation.ClearParameters()
        Try
            If strRefNo.Trim.Length > 0 Then
                StrQ = "SELECT ISNULL(CONVERT(CHAR(8),interdictiondate,112),'') AS IDate FROM hrdiscipline_file_master WHERE reference_no = @RefNo AND interdictiondate IS NOT NULL"
                objDataOperation.AddParameter("@RefNo", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strRefNo)
                dsList = objDataOperation.ExecQuery(StrQ, "List")
                If objDataOperation.ErrorMessage <> "" Then
                    exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If
                If dsList.Tables("List").Rows.Count > 0 Then
                    If eZeeDate.convertDate(dtDate) <> dsList.Tables("List").Rows(0)("IDate") Then
                        dtOrginalDate = eZeeDate.convertDate(dsList.Tables("List").Rows(0)("IDate").ToString).ToShortDateString
                        blnFlag = False
                    Else
                        blnFlag = True
                    End If
                Else
                    blnFlag = True
                End If
            Else
                blnFlag = True
            End If
            Return blnFlag
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: IsInterdictionDateExists; Module Name: " & mstrModuleName)
        Finally
            If mobjDataOperation Is Nothing Then objDataOperation = Nothing
        End Try
    End Function

    'S.SANDEEP [24 MAY 2016] -- START
    'ENHANCEMENT : DISCIPLINE MAKE-OVER {BY ANDREW}
    Public Function GetComboList(ByVal xDatabaseName As String, _
                                 ByVal xUserUnkid As Integer, _
                                 ByVal xYearUnkid As Integer, _
                                 ByVal xCompanyUnkid As Integer, _
                                 ByVal xPeriodStart As DateTime, _
                                 ByVal xPeriodEnd As DateTime, _
                                 ByVal xUserModeSetting As String, _
                                 ByVal xOnlyApproved As Boolean, _
                                 ByVal xIncludeIn_ActiveEmployee As Boolean, _
                                 ByVal strTableName As String, _
                                 Optional ByVal blnFlag As Boolean = False, _
                                 Optional ByVal intDisciplineFileUnkid As Integer = -1, _
                                 Optional ByVal intEmpUnkId As Integer = -1, _
                                 Optional ByVal intStatusId As Integer = -1, _
                                 Optional ByVal strReferenceNo As String = "", _
                                 Optional ByVal xFilterString As String = "", _
                                 Optional ByVal blnApplyUserAccessFilter As Boolean = True, _
                                 Optional ByVal blnShowAllCharges As Boolean = True) As DataSet 'S.SANDEEP |11-NOV-2019| -- START {blnShowAllCharges} -- END
        'S.SANDEEP [24 MAY 2016] -- [blnApplyUserAccessFilter]
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim objDataOperation As clsDataOperation
        If mobjDataOperation Is Nothing Then
            objDataOperation = New clsDataOperation
        Else
            objDataOperation = mobjDataOperation
        End If
        objDataOperation.ClearParameters()

        Dim xDateJoinQry, xDateFilterQry, xUACQry, xUACFiltrQry, xAdvanceJoinQry As String
        xDateJoinQry = "" : xDateFilterQry = "" : xUACQry = "" : xUACFiltrQry = "" : xAdvanceJoinQry = ""
        Call GetDatesFilterString(xDateJoinQry, xDateFilterQry, xPeriodStart, xPeriodEnd, , , xDatabaseName)
        'S.SANDEEP [24 MAY 2016] -- START
        'ENHANCEMENT : DISCIPLINE MAKE-OVER {BY ANDREW}
        'Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, xPeriodEnd, xOnlyApproved, xDatabaseName, xUserUnkid, xCompanyUnkid, xYearUnkid, xUserModeSetting)
        If blnApplyUserAccessFilter = True Then
            Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, xPeriodEnd, xOnlyApproved, xDatabaseName, xUserUnkid, xCompanyUnkid, xYearUnkid, xUserModeSetting)
        End If
        'S.SANDEEP [24 MAY 2016] -- END
        Call GetAdvanceFilterQry(xAdvanceJoinQry, xPeriodEnd, xDatabaseName)

        Try
            strQ = ""
            If blnFlag Then
                strQ = "SELECT " & _
                       "    @SELECT AS reference_no " & _
                       "   ,0 AS disciplinefileunkid " & _
                       "   ,0 AS involved_employeeunkid " & _
                       "   ,NULL AS chargedate " & _
                       "   ,NULL AS interdictiondate " & _
                       "   ,0 AS disciplinestatusunkid " & _
                       "   ,'' AS charge_description " & _
                       "   ,0 AS notifyemployee " & _
                       "   ,NULL AS notification_date " & _
                       "   ,'' AS status " & _
                       "   ,'' AS employeecode " & _
                       "   ,'' AS employeename " & _
                       "   ,CAST(0 AS BIT) AS isvisibleoness " & _
                       "UNION ALL "
                'S.SANDEEP |11-NOV-2019| -- START {isvisibleoness} -- END
            End If
            strQ &= "SELECT " & _
                    "    hrdiscipline_file_master.reference_no " & _
                    "   ,hrdiscipline_file_master.disciplinefileunkid " & _
                    "   ,hrdiscipline_file_master.involved_employeeunkid " & _
                    "   ,hrdiscipline_file_master.chargedate " & _
                    "   ,hrdiscipline_file_master.interdictiondate " & _
                    "   ,hrdiscipline_file_master.disciplinestatusunkid " & _
                    "   ,hrdiscipline_file_master.charge_description " & _
                    "   ,hrdiscipline_file_master.notifyemployee " & _
                    "   ,hrdiscipline_file_master.notification_date " & _
                    "   ,hrdisciplinestatus_master.name AS status " & _
                    "   ,hremployee_master.employeecode AS employeecode " & _
                    "   ,hremployee_master.employeecode + ' - ' + hremployee_master.firstname + ' ' + hremployee_master.othername + ' ' + hremployee_master.surname AS employeename " & _
                    "   ,hrdiscipline_file_master.isvisibleoness " & _
                    "FROM hrdiscipline_file_master " & _
                    "   JOIN hremployee_master ON hremployee_master.employeeunkid = hrdiscipline_file_master.involved_employeeunkid " & _
                    "   LEFT JOIN hrdisciplinestatus_master ON hrdiscipline_file_master.disciplinestatusunkid = hrdisciplinestatus_master.disciplinestatusunkid "
            'S.SANDEEP |11-NOV-2019| -- START {isvisibleoness} -- END

            If xDateJoinQry.Trim.Length > 0 Then
                strQ &= xDateJoinQry
            End If

            'S.SANDEEP [15 NOV 2016] -- START
            If xUACQry.Trim.Length > 0 Then
                strQ &= xUACQry
            End If
            'S.SANDEEP [15 NOV 2016] -- END

            strQ &= " WHERE hrdiscipline_file_master.isvoid = 0 "

            If intDisciplineFileUnkid > 0 Then
                strQ &= "AND hrdiscipline_file_master.disciplinefileunkid = '" & intDisciplineFileUnkid & "' "
            End If

            If strReferenceNo.Trim.Length > 0 Then
                strQ &= "AND hrdiscipline_file_master.reference_no = '" & strReferenceNo & "' "
            End If

            If intEmpUnkId > 0 Then
                strQ &= "AND hrdiscipline_file_master.involved_employeeunkid = '" & intEmpUnkId & "' "
            End If

            If intStatusId > 0 Then
                strQ &= "AND hrdiscipline_file_master.disciplinestatusunkid = '" & intStatusId & "' "
            End If

            'S.SANDEEP |11-NOV-2019| -- START
            'ISSUE/ENHANCEMENT : NMB DISCIPLINARY UAT COMMENTS
            If blnShowAllCharges = False Then
                strQ &= "AND hrdiscipline_file_master.isvisibleoness = 1 "
            End If
            'S.SANDEEP |11-NOV-2019| -- END

            If xIncludeIn_ActiveEmployee = False Then
                If xDateFilterQry.Trim.Length > 0 Then
                    strQ &= xDateFilterQry
                End If
            End If

            If xUACFiltrQry.Trim.Length > 0 Then
                strQ &= " AND " & xUACFiltrQry & " "
            End If

            If xFilterString.Trim.Length > 0 Then
                strQ &= " AND " & xFilterString
            End If

            objDataOperation.AddParameter("@SELECT", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 8, "Select"))

            dsList = objDataOperation.ExecQuery(strQ, strTableName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetComboList; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If mobjDataOperation Is Nothing Then objDataOperation = Nothing
        End Try
        Return dsList
    End Function
    'S.SANDEEP [24 MAY 2016] -- END

    Public Function Delete(ByVal intDisciplineFileId As Integer, ByVal intEmployeeId As Integer, ByVal strDocument_Path As String, ByVal strScreenName As String) As Boolean
        Dim blnFlag As Boolean = False
        Dim StrQ As String = String.Empty
        Dim iCnt As Integer = 0
        Dim objScanAttachment As New clsScan_Attach_Documents
        Dim objDataOperation As New clsDataOperation
        Dim mdtTran As New DataTable
        Dim objDisciFileTran As New clsDiscipline_file_tran
        Try
            StrQ = "SELECT 1 FROM hrdiscipline_proceeding_master WHERE hrdiscipline_proceeding_master.isvoid = 0 AND hrdiscipline_proceeding_master.disciplinefileunkid = '" & intDisciplineFileId & "' "

            iCnt = objDataOperation.RecordCount(StrQ)

            If objDataOperation.ErrorMessage <> "" Then
                Throw New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
            End If

            If iCnt > 0 Then
                mstrMessage = Language.getMessage(mstrModuleName, 6, "Sorry, you cannot delete this charge. Reason : This charge is linked with discipline proceedings.")
                Return False
            End If


            StrQ = "SELECT 1 FROM hrhearing_schedule_master WHERE hrhearing_schedule_master.isvoid = 0 AND hrhearing_schedule_master.disciplinefileunkid = '" & intDisciplineFileId & "' "

            iCnt = objDataOperation.RecordCount(StrQ)

            If objDataOperation.ErrorMessage <> "" Then
                Throw New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
            End If

            If iCnt > 0 Then
                mstrMessage = Language.getMessage(mstrModuleName, 7, "Sorry, you cannot delete this charge. Reason : This charge is linked with discipline hearing scheduling.")
                Return False
            End If

            objDataOperation.BindTransaction()

            StrQ = "UPDATE hrdiscipline_file_master SET " & _
                   "  isvoid = @isvoid " & _
                   " ,voiduserunkid = @voiduserunkid " & _
                   " ,voidreason = @voidreason " & _
                   " ,voiddatetime = @voiddatetime " & _
                   "WHERE disciplinefileunkid = @disciplinefileunkid "


            objDataOperation.AddParameter("@disciplinefileunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intDisciplineFileId)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid)
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)
            objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime)

            objDataOperation.ExecNonQuery(StrQ)

            If objDataOperation.ErrorMessage <> "" Then
                Throw New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
            End If

            objDisciFileTran._Disciplinefileunkid = intDisciplineFileId
            mdtTran = objDisciFileTran._ChargesTable

            For Each dRow As DataRow In mdtTran.Rows
                dRow.Item("AUD") = "D"
                dRow.Item("isvoid") = False
                dRow.Item("voiduserunkid") = mintVoiduserunkid
                dRow.Item("voidreason") = mstrVoidreason
                dRow.Item("voiddatetime") = mdtVoiddatetime
            Next

            mdtTran.AcceptChanges()
            objDiscFileTran._ChargesTable = mdtTran

            Dim mdtScanAttachment As DataTable
            'S.SANDEEP |04-SEP-2021| -- START
            'objScanAttachment.GetList(strDocument_Path, "List", "", intEmployeeId)
            objScanAttachment.GetList(strDocument_Path, "List", "", intEmployeeId, , , , , CBool(IIf(intEmployeeId <= 0, True, False)))
            'S.SANDEEP |04-SEP-2021| -- END

            Dim xRow = objScanAttachment._Datatable.AsEnumerable().Where(Function(x) x.Field(Of Integer)("scanattachrefid") = enScanAttactRefId.DISCIPLINES And x.Field(Of String)("form_name") = "'" & strScreenName & "'")

            If xRow.Count > 0 Then
                mdtScanAttachment = xRow.CopyToDataTable()
            Else
                mdtScanAttachment = objScanAttachment._Datatable.Clone
            End If

            With objDisciFileTran
                ._FormName = mstrFormName
                ._LoginEmployeeunkid = mintLoginEmployeeunkid
                ._ClientIP = mstrClientIP
                ._HostName = mstrHostName
                ._FromWeb = mblnIsWeb
                ._AuditUserId = mintAuditUserId
._CompanyUnkid = mintCompanyUnkid
                ._AuditDate = mdtAuditDate
            End With

            If objDisciFileTran.InsertUpdateDelete_DisciplineCharges(objDataOperation, mintVoiduserunkid, mdtVoiddatetime, mdtScanAttachment, strScreenName) = False Then
                Throw New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
            End If

            objDataOperation.ReleaseTransaction(True)

            Return True

        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Delete; Module Name: " & mstrModuleName)
        Finally
            objScanAttachment = Nothing
        End Try
    End Function


    'S.SANDEEP |12-NOV-2020| -- START
    'ISSUE/ENHANCEMENT : SPRINT-6
    Public Function SendMailToSelecteUsers(ByVal dtChargeTable As DataTable, _
                                          ByVal intDisciplineFileUnkid As Integer, _
                                          ByVal intCompanyId As Integer, _
                                          ByVal dtDcouments As DataTable, _
                                          ByVal strDocumentPath As String, _
                                          ByVal strLstName As List(Of String), _
                                          ByVal dtStartDate As Date, _
                                          ByVal dtEndDate As Date, _
                                          ByVal strDataBase As String, _
                                          ByVal dtCurrentDateTime As DateTime, _
                                           ByVal intEmployeeId As Integer, _
                                           ByVal strChargeDescr As String) As Boolean
        Try
            Dim strMessage As String = String.Empty
            Dim objNetConn As New clsNetConnectivity
            Dim objMail As New clsSendMail
            Dim strArutiSelfServiceURL As String = ConfigParameter._Object._ArutiSelfServiceURL.ToString
            Dim strLink As String = String.Empty
            Dim objEmployee As New clsEmployee_Master
            Dim objConfig As New clsConfigOptions
            Dim objUser As New clsUserAddEdit
            Dim objCompany As New clsCompany_Master

            objCompany._Companyunkid = intCompanyId
            If objCompany._Senderaddress.Trim.Length <= 0 Then
                objCompany = Nothing
                mstrMessage = Language.getMessage(mstrModuleName, 30, "Sorry, Email Setup under company is not configured.")
                Return False
            End If
            objCompany = Nothing
            If objNetConn._Conected = False Then Exit Function

            If dtDcouments Is Nothing Then
                Dim objScanAttach As New clsScan_Attach_Documents
                dtDcouments = objScanAttach.GetQulificationAttachment(mintInvolved_Employeeunkid, enScanAttactRefId.DISCIPLINES, 0, strDocumentPath)
                objScanAttach = Nothing
            End If
            Dim StrUserId As String = ""
            StrUserId = objConfig.GetKeyValue(intCompanyId, "DisciplineNotifyChargePostedUserIds", Nothing)
            If StrUserId Is Nothing Then StrUserId = ""
            If StrUserId.Trim.Length > 0 Then
                objUser._Userunkid = mintUserunkid
                Dim strSenderAddress As String = IIf(objUser._Email = "", objUser._Username, objUser._Email)
                objEmployee._Employeeunkid(dtEndDate) = intEmployeeId
                For Each id As String In StrUserId.Split(CChar(","))
                    objUser._Userunkid = CInt(id)
                    If objUser._Email.Trim.Length > 0 Then
                        objMail._Subject = Language.getMessage(mstrModuleName, 9, "Discipline Charge Notification")
                        Dim strEmployeeName As String = objEmployee._Firstname + " " + objEmployee._Surname
                        strMessage = "<HTML><BODY>"
                        strMessage &= Language.getMessage(mstrModuleName, 11, "Dear") & " <b>" & getTitleCase(objUser._Username) & "</b>" & " ,<br>"
                        strMessage &= Language.getMessage(mstrModuleName, 500, "This is to inform you that the following disciplinary charge details with reference number") & " " & mstrReference_No & " " & Language.getMessage(mstrModuleName, 400, "have been posted against") & " " & getTitleCase(strEmployeeName) & "<br><br>"
                        strMessage &= "<B>" & Language.getMessage(mstrModuleName, 300, "Offence :") & "</B><br>"
                        strMessage &= strChargeDescr & "<br><br>"
                        strMessage &= "<p><center style='padding-top:25px;font-size:10.0pt;font-family:&quot;Verdana&quot;,&quot;Sans-Serif&quot;;color:#333399;margin-left:0px;margin-right:0px;margin-top:0px;margin-bottom:10px'><b>&quot;POWERED BY ARUTI HR &amp; PAYROLL MANAGEMENT SOFTWARE.&quot;</b></center></p>"
                        Dim strSuccess As String = ""
                        objMail._Message = strMessage
                        objMail._ToEmail = objUser._Email
                        If mintLoginTypeId <= 0 Then mintLoginTypeId = enLogin_Mode.DESKTOP
                        objMail._FormName = mstrFormName
                        objMail._LoginEmployeeunkid = mintLoginEmployeeunkid
                        objMail._ClientIP = mstrClientIP
                        objMail._HostName = mstrHostName
                        objMail._FromWeb = mblnIsWeb
                        objMail._AuditUserId = mintAuditUserId
                        objMail._CompanyUnkid = mintCompanyUnkid
                        objMail._AuditDate = mdtAuditDate                        
                        objMail._OperationModeId = mintLoginTypeId
                        objMail._UserUnkid = mintUserunkid
                        objMail._LogEmployeeUnkid = mintLoginemployeeId
                        objMail._OperationModeId = mintLoginTypeId
                        objMail._UserUnkid = mintUserunkid
                        objMail._SenderAddress = strSenderAddress

                        If dtDcouments.Rows.Count > 0 Then
                            Dim strAttachedFile As String = ""
                            Dim xQry = (From v In strLstName Join dt In dtDcouments.AsEnumerable() On dt.Field(Of String)("form_name") Equals v Select dt)
                            dtDcouments = xQry.CopyToDataTable()
                            strAttachedFile = String.Join(",", dtDcouments.AsEnumerable().Select(Function(x) x.Field(Of String)("filepath").ToString).ToArray)
                            objMail._AttachedFiles = strAttachedFile
                        End If

                        objMail._ModuleRefId = clsSendMail.enAT_VIEW_TYPE.DISCIPLINE_MGT
                        strSuccess = objMail.SendMail(intCompanyId)
                    End If
                Next
            End If


        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: SendMailToSelecteUsers; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Function
    'S.SANDEEP |12-NOV-2020| -- END

    'S.SANDEEP [24 MAY 2016] -- Start
    'Email Notification
    Public Function SendMailToEmployee(ByVal dtChargeTable As DataTable, _
                                       ByVal intDisciplineFileUnkid As Integer, ByVal intCompanyId As Integer, _
                                       ByVal dtDcouments As DataTable, _
                                       ByVal strDocumentPath As String, _
                                       ByVal strLstName As List(Of String), _
                                       ByVal dtStartDate As Date, ByVal dtEndDate As Date, _
                                       ByVal strDataBase As String, ByVal dtCurrentDateTime As DateTime) As Boolean
        Try
            Dim strMessage As String = String.Empty
            Dim objNetConn As New clsNetConnectivity
            Dim objMail As New clsSendMail
            Dim strArutiSelfServiceURL As String = ConfigParameter._Object._ArutiSelfServiceURL.ToString
            Dim strLink As String = String.Empty
            Dim objEmployee As New clsEmployee_Master
            Dim objUser As New clsUserAddEdit

            Dim objCompany As New clsCompany_Master
            objCompany._Companyunkid = intCompanyId
            If objCompany._Senderaddress.Trim.Length <= 0 Then
                objCompany = Nothing
                mstrMessage = Language.getMessage(mstrModuleName, 30, "Sorry, Email Setup under company is not configured.")
                Return False
            End If
            objCompany = Nothing
            If objNetConn._Conected = False Then Exit Function

            If dtDcouments Is Nothing Then
                Dim objScanAttach As New clsScan_Attach_Documents
                dtDcouments = objScanAttach.GetQulificationAttachment(mintInvolved_Employeeunkid, enScanAttactRefId.DISCIPLINES, 0, strDocumentPath)
                objScanAttach = Nothing
            End If

            objUser._Userunkid = mintUserunkid
            objEmployee._Employeeunkid(dtEndDate) = mintInvolved_Employeeunkid

            mstrMessage = ""
            If objEmployee._Email.Trim.Length <= 0 Then
                mstrMessage = Language.getMessage(mstrModuleName, 27, "Sorry, email is not assigned to the employee. Please make sure that you assign an email.")
                Return False
            End If

            'S.SANDEEP |01-OCT-2019| -- START
            'ISSUE/ENHANCEMENT : NMB DISCIPLINARY UAT COMMENTS
            'Dim strEmployeeName As String = objEmployee._Firstname + " " + objEmployee._Othername + " " + objEmployee._Surname
            Dim strEmployeeName As String = objEmployee._Firstname + " " + objEmployee._Surname
            'S.SANDEEP |01-OCT-2019| -- END

            objMail._Subject = Language.getMessage(mstrModuleName, 9, "Discipline Charge Notification") & " " & Language.getMessage(mstrModuleName, 10, " :  Reference No - ") & mstrReference_No

            strMessage = "<HTML><BODY>"

            'Gajanan [27-Mar-2019] -- Start
            'Enhancement - Change Email Language
            'strMessage &= Language.getMessage(mstrModuleName, 11, "Dear") & " " & "&nbsp;<b>" & strEmployeeName & "</b>" & " ,<BR></BR>"
            strMessage &= Language.getMessage(mstrModuleName, 11, "Dear") & " <b>" & getTitleCase(strEmployeeName) & "</b>" & " ,<BR></BR>"
            'Gajanan [27-Mar-2019] -- End


            'Gajanan [27-Mar-2019] -- Start
            'Enhancement - Change Email Language

            'strMessage &= "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" & Language.getMessage(mstrModuleName, 12, "Please refer following Disciplinary Charges details posted against you, By clicking link below and provide your response for the Charge. ") & "<BR></BR>"
            strMessage &= Language.getMessage(mstrModuleName, 12, "Please refer following Disciplinary Charges details posted against you, By clicking link below and provide your response for the Charge. ") & "<BR></BR>"

            'Gajanan [27-Mar-2019] -- End

            'S.SANDEEP |01-OCT-2019| -- START
            'ISSUE/ENHANCEMENT : NMB DISCIPLINARY UAT COMMENTS
            'strLink = strArutiSelfServiceURL & "/Discipline/Discipline_Charge/wPg_DisciplineCharge_Add_Edit.aspx?" & HttpUtility.UrlEncode(clsCrypto.Encrypt(intCompanyId.ToString & "|" & _
            '                                                                                                         intDisciplineFileUnkid.ToString & "|" & _
            '                                                                                                         mintInvolved_Employeeunkid.ToString))

            If Strings.Right(strArutiSelfServiceURL, 1) <> "/" Then
            strLink = strArutiSelfServiceURL & "/Discipline/Discipline_Charge/wPg_DisciplineCharge_Add_Edit.aspx?" & HttpUtility.UrlEncode(clsCrypto.Encrypt(intCompanyId.ToString & "|" & _
                                                                                                                     intDisciplineFileUnkid.ToString & "|" & _
                                                                                                                     mintInvolved_Employeeunkid.ToString))
            Else
                strLink = strArutiSelfServiceURL & "Discipline/Discipline_Charge/wPg_DisciplineCharge_Add_Edit.aspx?" & HttpUtility.UrlEncode(clsCrypto.Encrypt(intCompanyId.ToString & "|" & _
                                                                                                                        intDisciplineFileUnkid.ToString & "|" & _
                                                                                                                        mintInvolved_Employeeunkid.ToString))
            End If
            'S.SANDEEP |01-OCT-2019| -- END

            'Gajanan [27-Mar-2019] -- Start
            'Enhancement - Change Email Language
            'strMessage &= "&nbsp;&nbsp;&nbsp;<a href='" & strLink & "'>" & strLink & "</a><BR></BR><BR></BR>"
            strMessage &= "<a href='" & strLink & "'>" & strLink & "</a><BR></BR><BR></BR>"
            'Gajanan [27-Mar-2019] -- End


            'strMessage &= getChargeDetailForEmail(intDisciplineFileUnkid)
            'strMessage &= getCountForEmail(intDisciplineFileUnkid)


            'Gajanan [27-Mar-2019] -- Start
            'Enhancement - Change Email Language
            'strMessage &= "<BR></BR><BR></BR><center>""POWERED BY ARUTI HR & PAYROLL MANAGEMENT SOFTWARE""</center>"
            strMessage &= "<p><center style='padding-top:25px;font-size:10.0pt;font-family:&quot;Verdana&quot;,&quot;Sans-Serif&quot;;color:#333399;margin-left:0px;margin-right:0px;margin-top:0px;margin-bottom:10px'><b>&quot;POWERED BY ARUTI HR &amp; PAYROLL MANAGEMENT SOFTWARE.&quot;</b></center></p>"
            'Gajanan [27-Mar-2019] -- End



            strMessage &= "</BODY></HTML>"
            Dim strSuccess As String = ""
            objMail._Message = strMessage
            objMail._ToEmail = objEmployee._Email

            'S.SANDEEP [26 JUL 2016] -- START
            'ENHANCEMENT : DISCIPLINE MAKE-OVER {BY ANDREW}
            Dim suEmail As String = ""
            GetEmployeeReportingEmail(mintInvolved_Employeeunkid, dtStartDate, dtEndDate, strDataBase, suEmail, "", -1)
            If suEmail.Trim.Length > 0 Then
                objMail._CCAddress = suEmail
            End If
            'S.SANDEEP [26 JUL 2016] -- END

            If mintLoginTypeId <= 0 Then mintLoginTypeId = enLogin_Mode.DESKTOP
            objMail._FormName = mstrFormName
            objMail._LoginEmployeeunkid = mintLoginEmployeeunkid
            objMail._ClientIP = mstrClientIP
            objMail._HostName = mstrHostName
            objMail._FromWeb = mblnIsWeb
            objMail._AuditUserId = mintAuditUserId
objMail._CompanyUnkid = mintCompanyUnkid
            objMail._AuditDate = mdtAuditDate
            objMail._LoginEmployeeunkid = mintLoginemployeeId
            objMail._OperationModeId = mintLoginTypeId
            objMail._UserUnkid = mintUserunkid
            objMail._SenderAddress = IIf(objUser._Email = "", objUser._Username, objUser._Email)

            If dtDcouments.Rows.Count > 0 Then
                Dim strAttachedFile As String = ""
                Dim xQry = (From v In strLstName Join dt In dtDcouments.AsEnumerable() On dt.Field(Of String)("form_name") Equals v Select dt)
                dtDcouments = xQry.CopyToDataTable()
                strAttachedFile = String.Join(",", dtDcouments.AsEnumerable().Select(Function(x) x.Field(Of String)("filepath").ToString).ToArray)
                objMail._AttachedFiles = strAttachedFile
            End If

            objMail._ModuleRefId = clsSendMail.enAT_VIEW_TYPE.DISCIPLINE_MGT
            'Sohail (30 Nov 2017) -- Start
            'SUMATRA Enhancement – SUMATRA – issue # 0001669: Additional feature for Exchange Server configuration setting in 70.1.
            'strSuccess = objMail.SendMail()
            strSuccess = objMail.SendMail(intCompanyId)
            'Sohail (30 Nov 2017) -- End

            If strSuccess.Trim.Length <= 0 Then
                mdtNotificationDate = dtCurrentDateTime
                mdtEmaildate = dtCurrentDateTime
                mblnNotifyEmployee = True
                _FormName = mstrFormName
                _LoginEmployeeunkid = mintLoginEmployeeunkid
                _ClientIP = mstrClientIP
                _HostName = mstrHostName
                _FromWeb = mblnIsWeb
                _AuditUserId = mintAuditUserId
                _AuditDate = mdtAuditDate
                Call Update(dtCurrentDateTime)
                Return True
            Else
                mstrMessage = strSuccess
                Return False
            End If
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: SendMailToEmployee; Module Name: " & mstrModuleName)
        End Try
    End Function

    Public Function getChargeDetailForEmail(ByVal intDisciplineFileunkid As Integer) As String
        Try
            Dim strMessage As String = String.Empty
            Dim strEmployeeName As String = String.Empty
            Dim strBuilder As New StringBuilder
            Dim objEmployee As New clsEmployee_Master

            mintDisciplinefileunkid = intDisciplineFileunkid
            Call GetData()

            objEmployee._Employeeunkid(eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate)) = mintInvolved_Employeeunkid
            'S.SANDEEP |01-OCT-2019| -- START
            'ISSUE/ENHANCEMENT : NMB DISCIPLINARY UAT COMMENTS
            'strEmployeeName = objEmployee._Firstname + " " + objEmployee._Othername + " " + objEmployee._Surname
            strEmployeeName = objEmployee._Firstname + " " + objEmployee._Surname
            'S.SANDEEP |01-OCT-2019| -- END

            strBuilder.Append("<B>" & Language.getMessage(mstrModuleName, 13, "Charge Description :") & "</B><BR></BR>" & vbCrLf)
            strBuilder.Append("<TABLE BORDER=1 CELLSPACING=0 CELLPADDING=3 WIDTH='100%'>" & vbCrLf)
            strBuilder.Append("<TR WIDTH='100%'>" & vbCrLf)
            strBuilder.Append("<TD WIDTH='40%' BGCOLOR='#4682b4' ALIGN='LEFT'><FONT COLOR='WHITE' SIZE=2><B>" & Language.getMessage(mstrModuleName, 14, "Particular") & "</B></FONT></TD>" & vbCrLf)
            strBuilder.Append("<TD WIDTH='60%' BGCOLOR='#4682b4' ALIGN='LEFT'><FONT COLOR='WHITE' SIZE=2><B>" & Language.getMessage(mstrModuleName, 15, "Details") & "</B></FONT></TD>" & vbCrLf)
            strBuilder.Append("</TR>" & vbCrLf)
            strBuilder.Append("<TR WIDTH='100%'>" & vbCrLf)
            strBuilder.Append("<TD WIDTH='40%' ALIGN='LEFT'><FONT SIZE=2><B>" & Language.getMessage(mstrModuleName, 16, "Reference No.") & "</B></FONT></TD>" & vbCrLf)
            strBuilder.Append("<TD WIDTH='60%' ALIGN='LEFT'><FONT SIZE=2>" & mstrReference_No & "</FONT></TD>" & vbCrLf)
            strBuilder.Append("</TR>" & vbCrLf)
            strBuilder.Append("<TR WIDTH='100%'>" & vbCrLf)
            strBuilder.Append("<TD WIDTH='40%' ALIGN='LEFT'><FONT SIZE=2><B>" & Language.getMessage(mstrModuleName, 17, "Employee") & "</B></FONT></TD>" & vbCrLf)
            'S.SANDEEP |01-OCT-2019| -- START
            'ISSUE/ENHANCEMENT : NMB DISCIPLINARY UAT COMMENTS
            'strBuilder.Append("<TD WIDTH='60%' ALIGN='LEFT'><FONT SIZE=2>" & strEmployeeName & "</FONT></TD>" & vbCrLf)
            strBuilder.Append("<TD WIDTH='60%' ALIGN='LEFT'><FONT SIZE=2>" & getTitleCase(strEmployeeName) & "</FONT></TD>" & vbCrLf)
            'S.SANDEEP |01-OCT-2019| -- END

            strBuilder.Append("</TR>" & vbCrLf)
            strBuilder.Append("<TR WIDTH='100%'>" & vbCrLf)
            strBuilder.Append("<TD WIDTH='40%' ALIGN='LEFT'><FONT SIZE=2><B>" & Language.getMessage(mstrModuleName, 18, "Charge Date") & "</B></FONT></TD>" & vbCrLf)
            strBuilder.Append("<TD WIDTH='60%' ALIGN='LEFT'><FONT SIZE=2>" & mdtChargedate.ToShortDateString & "</FONT></TD>" & vbCrLf)
            strBuilder.Append("</TR>" & vbCrLf)
            strBuilder.Append("<TR WIDTH='100%'>" & vbCrLf)
            strBuilder.Append("<TD WIDTH='40%' ALIGN='LEFT' VALIGN = 'TOP' ><FONT SIZE=2><B>" & Language.getMessage(mstrModuleName, 19, "General Charge Description") & "</B></FONT></TD>" & vbCrLf)
            strBuilder.Append("<TD WIDTH='60%' ALIGN='LEFT'><FONT SIZE=2>" & mstrCharge_Description & "</FONT></TD>" & vbCrLf)
            strBuilder.Append("</TR>" & vbCrLf)
            'Hemant (22 Jan 2021) -- Start
            'Enhancement #OLD-275 - NMB Discipline Open Case Notification
            Dim objStatusTran As New clsDiscipline_StatusTran
            Dim objCMaster As New clsCommon_Master
            Dim strOpeningReason As String = String.Empty
            Dim dsStatusTran As DataSet = objStatusTran.GetList("List", intDisciplineFileunkid)
            dsStatusTran.Tables(0).DefaultView.Sort = "Statusdate desc"
            Dim dtStatusTran As DataTable = dsStatusTran.Tables(0).DefaultView.ToTable
            If dtStatusTran.Rows.Count > 0 Then
                strOpeningReason = dtStatusTran.Rows(0).Item("openreasonunkid")
                Dim dsCommonList As DataSet = objCMaster.getComboList(clsCommon_Master.enCommonMaster.DISCIPLINE_CASE_OPEN_REASON, True, "List")
                If CInt(dtStatusTran.Rows(0).Item("openreasonunkid")) > 0 Then
                    strOpeningReason = dsCommonList.Tables(0).Select("masterunkid = " & CInt(dtStatusTran.Rows(0).Item("openreasonunkid"))).FirstOrDefault.Item("name")
                    strBuilder.Append("<TR WIDTH='100%'>" & vbCrLf)
                    strBuilder.Append("<TD WIDTH='40%' ALIGN='LEFT' VALIGN = 'TOP' ><FONT SIZE=2><B>" & Language.getMessage(mstrModuleName, 32, "Opening Reason") & "</B></FONT></TD>" & vbCrLf)
                    strBuilder.Append("<TD WIDTH='60%' ALIGN='LEFT'><FONT SIZE=2>" & strOpeningReason & "</FONT></TD>" & vbCrLf)
                    strBuilder.Append("</TR>" & vbCrLf)
                End If
            End If
            'Hemant (22 Jan 2021) -- End

            strBuilder.Append("</TABLE><BR></BR>" & vbCrLf)

            strMessage &= strBuilder.ToString

            Return strMessage
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: getChargeDetailForEmail; Module Name: " & mstrModuleName)
        End Try
    End Function

    Public Function getCountForEmail(ByVal intDisciplineFileunkid As Integer) As String
        Dim strMessage As String = String.Empty
        Try
            Dim strBuilder As New StringBuilder
            Dim dtChargeTable As DataTable = Nothing
            Dim objDisciplineFileTran As New clsDiscipline_file_tran

            objDisciplineFileTran._Disciplinefileunkid = intDisciplineFileunkid
            dtChargeTable = objDisciplineFileTran._ChargesTable

            strBuilder.Append("<b>" & Language.getMessage(mstrModuleName, 20, "Charges Count :") & "</b><BR></BR>" & vbCrLf)

            strBuilder.Append("<TABLE BORDER=1 CELLSPACING=0 CELLPADDING=3 WIDTH='100%'>" & vbCrLf)
            strBuilder.Append("<TR WIDTH='100%'>" & vbCrLf)
            strBuilder.Append("<TD WIDTH='10%' BGCOLOR='#4682b4' ALIGN='LEFT'><FONT COLOR='WHITE' SIZE=2><B>" & Language.getMessage(mstrModuleName, 21, "Count") & "</B></FONT></TD>" & vbCrLf)
            strBuilder.Append("<TD WIDTH='30%' BGCOLOR='#4682b4' ALIGN='LEFT'><FONT COLOR='WHITE' SIZE=2><B>" & Language.getMessage(mstrModuleName, 22, "Incident") & "</B></FONT></TD>" & vbCrLf)
            strBuilder.Append("<TD WIDTH='25%' BGCOLOR='#4682b4' ALIGN='LEFT'><FONT COLOR='WHITE' SIZE=2><B>" & Language.getMessage(mstrModuleName, 23, "Offence Category") & "</B></FONT></TD>" & vbCrLf)
            strBuilder.Append("<TD WIDTH='25%' BGCOLOR='#4682b4' ALIGN='LEFT'><FONT COLOR='WHITE' SIZE=2><B>" & Language.getMessage(mstrModuleName, 24, "Offence Description") & "</B></FONT></TD>" & vbCrLf)
            strBuilder.Append("<TD WIDTH='10%' BGCOLOR='#4682b4' ALIGN='LEFT'><FONT COLOR='WHITE' SIZE=2><B>" & Language.getMessage(mstrModuleName, 25, "Severity") & "</B></FONT></TD>" & vbCrLf)
            strBuilder.Append("</TR>" & vbCrLf)
            Dim intChargeCount As Integer = 1
            If dtChargeTable IsNot Nothing Then
                For Each dRow As DataRow In dtChargeTable.Rows
                    If dRow.Item("AUD") <> "D" Then
                        strBuilder.Append("<TR WIDTH='100%'>" & vbCrLf)
                        strBuilder.Append("<TD WIDTH='10%' ALIGN='LEFT'><FONT SIZE=2>" & IIf(CInt(dRow.Item("charge_count")) <= 0, intChargeCount, CInt(dRow.Item("charge_count"))) & "</B></FONT></TD>" & vbCrLf)
                        strBuilder.Append("<TD WIDTH='30%' ALIGN='LEFT'><FONT SIZE=2>" & dRow.Item("incident_description") & "</FONT></TD>" & vbCrLf)
                        strBuilder.Append("<TD WIDTH='25%' ALIGN='LEFT'><FONT SIZE=2>" & dRow.Item("charge_category") & "</FONT></TD>" & vbCrLf)
                        strBuilder.Append("<TD WIDTH='25%' ALIGN='LEFT'><FONT SIZE=2>" & dRow.Item("charge_descr") & "</FONT></TD>" & vbCrLf)
                        strBuilder.Append("<TD WIDTH='10%' ALIGN='LEFT'><FONT SIZE=2>" & dRow.Item("charge_severity") & "</FONT></TD>" & vbCrLf)
                        strBuilder.Append("</TR>" & vbCrLf)

                        intChargeCount += 1
                    End If
                Next
            End If
            strBuilder.Append("</TABLE>" & vbCrLf)

            strMessage &= strBuilder.ToString

            Return strMessage
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: getCountForEmail; Module Name: " & mstrModuleName)
        End Try
    End Function
    'S.SANDEEP [24 MAY 2016] -- End

    'S.SANDEEP [26 JUL 2016] -- START
    'ENHANCEMENT : DISCIPLINE MAKE-OVER {BY ANDREW}
    Public Function SendMailToReportingTo(ByVal intDisciplineFileUnkid As Integer, _
                                          ByVal intCompanyId As Integer, _
                                          ByVal strDocumentPath As String, ByVal strLstName As List(Of String), _
                                          ByVal dtStartDate As Date, ByVal dtEndDate As Date, ByVal strDataBase As String, _
                                          ByVal intEmployeeId As Integer, ByVal dtDcouments As DataTable, ByVal intYearUnkid As Integer, Optional ByVal strSelfServiceUrl As String = "") As Boolean
        Dim strMessage As String = String.Empty
        Dim objNetConn As New clsNetConnectivity
        Dim objMail As New clsSendMail
        If strSelfServiceUrl.Trim.Length <= 0 Then strSelfServiceUrl = ConfigParameter._Object._ArutiSelfServiceURL.ToString
        Dim strArutiSelfServiceURL As String = strSelfServiceUrl
        Dim strLink As String = String.Empty
        Dim objEmployee As New clsEmployee_Master
        Try
            Dim objCompany As New clsCompany_Master
            objCompany._Companyunkid = intCompanyId
            If objCompany._Senderaddress.Trim.Length <= 0 Then
                objCompany = Nothing
                mstrMessage = Language.getMessage(mstrModuleName, 30, "Sorry, Email Setup under company is not configured.")
                Return False
            End If
            objCompany = Nothing
            If objNetConn._Conected = False Then Exit Function

            Dim dsList As New DataSet : Dim objUser As New clsUserAddEdit
            dsList = objUser.Get_UserBy_PrivilegeId(394, intYearUnkid)
            mstrMessage = ""
            Dim dCol As New DataColumn
            dCol.DataType = GetType(System.Boolean)
            dCol.ColumnName = "IsUser"
            dCol.DefaultValue = True
            dsList.Tables(0).Columns.Add(dCol)

            'Dim suEmail, suName As String : suEmail = "" : suName = ""
            'Dim suReportingEmpId As Integer = 0
            'GetEmployeeReportingEmail(intEmployeeId, dtStartDate, dtEndDate, strDataBase, suEmail, suName, suReportingEmpId)
            'Dim dr As DataRow = dsList.Tables(0).NewRow()
            'dr.Item("UName") = suName
            'dr.Item("UEmail") = suEmail
            'dr.Item("UId") = suReportingEmpId
            'dr.Item("IsUser") = False
            'dsList.Tables(0).Rows.Add(dr)

            'If dsList.Tables(0).Rows.Count <= 0 Then
            '    mstrMessage = Language.getMessage(mstrModuleName, 36, "Sorry, Privilege for adding proceeding for the discpline charges count or email are not assigned to the user. Please make sure that you assing the privilege and email.")
            '    Return False
            'End If

            If dtDcouments Is Nothing Then
                Dim objScanAttach As New clsScan_Attach_Documents
                dtDcouments = objScanAttach.GetQulificationAttachment(intEmployeeId, enScanAttactRefId.DISCIPLINES, 0, strDocumentPath)
                objScanAttach = Nothing
            End If
            objEmployee._Employeeunkid(dtEndDate) = intEmployeeId

            For Each row As DataRow In dsList.Tables(0).Rows
                objMail._Subject = Language.getMessage(mstrModuleName, 28, "Discipline Response Notification") & " " & Language.getMessage(mstrModuleName, 10, " :  Reference No - ") & mstrReference_No

                strMessage = "<HTML><BODY>"

                'Gajanan [27-Mar-2019] -- Start
                'Enhancement - Change Email Language
                'strMessage &= Language.getMessage(mstrModuleName, 11, "Dear") & " " & "&nbsp;<b>" & row.Item("UName") & "</b>" & " ,<BR></BR>"
                strMessage &= Language.getMessage(mstrModuleName, 11, "Dear") & " " & "&nbsp;<b>" & getTitleCase(row.Item("UName").ToString()) & "</b>" & " ,<BR></BR>"
                'Gajanan [27-Mar-2019] -- End


                If CBool(row.Item("IsUser")) = True Then

                    'Gajanan [27-Mar-2019] -- Start
                    'Enhancement - Change Email Language
                    'strMessage &= "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" & Language.getMessage(mstrModuleName, 29, "This is to inform you that, I have provided the response(s) for the charge posted against me, By clicking link below you can view my defence and attachments.") & "<BR></BR>"
                    strMessage &= " " & Language.getMessage(mstrModuleName, 29, "This is to inform you that, I have provided the response(s) for the charge posted against me, By clicking link below you can view my defence and attachments.") & "<BR></BR>"
                    'Gajanan [27-Mar-2019] -- End

                    strLink = strArutiSelfServiceURL & "/Discipline/Discipline_Charge/wPgViewEmployeeDisciplineResponse.aspx?" & HttpUtility.UrlEncode(clsCrypto.Encrypt(intCompanyId.ToString & "|" & _
                                                                                                                                 intDisciplineFileUnkid.ToString & "|" & _
                                                                                                                                 mintInvolved_Employeeunkid.ToString & "|" & _
                                                                                                                                 row.Item("UId").ToString & "|" & _
                                                                                                                                 row.Item("IsUser").ToString))
                End If



                'Gajanan [27-Mar-2019] -- Start
                'Enhancement - Change Email Language
                'strMessage &= "&nbsp;&nbsp;&nbsp;<a href='" & strLink & "'>" & strLink & "</a><BR></BR><BR></BR>"
                strMessage &= "<a href='" & strLink & "'>" & strLink & "</a><BR></BR><BR></BR>"
                'Gajanan [27-Mar-2019] -- End

                'S.SANDEEP |01-OCT-2019| -- START
                'ISSUE/ENHANCEMENT : NMB DISCIPLINARY UAT COMMENTS
                strMessage &= Language.getMessage(mstrModuleName, 31, "Regards,") & "<BR></BR>"
                strMessage &= getTitleCase(objEmployee._Firstname & " " & objEmployee._Surname)
                'S.SANDEEP |01-OCT-2019| -- END


                'Gajanan [27-Mar-2019] -- Start
                'Enhancement - Change Email Language
                'strMessage &= "<BR></BR><BR></BR><center>""POWERED BY ARUTI HR & PAYROLL MANAGEMENT SOFTWARE""</center>"
                strMessage &= "<p><center style='padding-top:25px;font-size:10.0pt;font-family:&quot;Verdana&quot;,&quot;Sans-Serif&quot;;color:#333399;margin-left:0px;margin-right:0px;margin-top:0px;margin-bottom:10px'><b>&quot;POWERED BY ARUTI HR &amp; PAYROLL MANAGEMENT SOFTWARE.&quot;</b></center></p>"
                'Gajanan [27-Mar-2019] -- End

                strMessage &= "</BODY></HTML>"
                Dim strSuccess As String = ""
                objMail._Message = strMessage
                objMail._ToEmail = row.Item("UEmail")
                If mintLoginTypeId <= 0 Then mintLoginTypeId = enLogin_Mode.DESKTOP
                objMail._FormName = mstrFormName
                objMail._LoginEmployeeunkid = mintLoginEmployeeunkid
                objMail._ClientIP = mstrClientIP
                objMail._HostName = mstrHostName
                objMail._FromWeb = mblnIsWeb
                objMail._AuditUserId = mintAuditUserId
objMail._CompanyUnkid = mintCompanyUnkid
                objMail._AuditDate = mdtAuditDate                
                objMail._LoginEmployeeunkid = mintLoginemployeeId
                objMail._OperationModeId = mintLoginTypeId
                objMail._UserUnkid = mintUserunkid
                objMail._SenderAddress = IIf(objEmployee._Email = "", objEmployee._Firstname & " " & objEmployee._Surname, objEmployee._Email)

                If dtDcouments.Rows.Count > 0 Then
                    Dim strAttachedFile As String = ""
                    Dim xQry = (From v In strLstName Join dt In dtDcouments.AsEnumerable() On dt.Field(Of String)("form_name") Equals v Select dt)
                    'S.SANDEEP |05-DEC-2019| -- START
                    'dtDcouments = xQry.CopyToDataTable()
                    'strAttachedFile = String.Join(",", dtDcouments.AsEnumerable().Select(Function(x) x.Field(Of String)("filepath").ToString).ToArray)
                    'objMail._AttachedFiles = strAttachedFile
                    If xQry IsNot Nothing AndAlso xQry.Count > 0 Then
                    dtDcouments = xQry.CopyToDataTable()
                    strAttachedFile = String.Join(",", dtDcouments.AsEnumerable().Select(Function(x) x.Field(Of String)("filepath").ToString).ToArray)
                    objMail._AttachedFiles = strAttachedFile
                End If
                    'S.SANDEEP |05-DEC-2019| -- START
                End If
                objMail._ModuleRefId = clsSendMail.enAT_VIEW_TYPE.DISCIPLINE_MGT
                'Sohail (30 Nov 2017) -- Start
                'SUMATRA Enhancement – SUMATRA – issue # 0001669: Additional feature for Exchange Server configuration setting in 70.1.
                'strSuccess = objMail.SendMail()
                strSuccess = objMail.SendMail(intCompanyId)
                'Sohail (30 Nov 2017) -- End
                If strSuccess.Trim.Length > 0 Then
                    mstrMessage = strSuccess
                    Return False
                End If
            Next
            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: SendMailToReportingTo; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Function

    Private Sub GetEmployeeReportingEmail(ByVal intEmployeeId As Integer, ByVal dtStartDate As Date, ByVal dtEndDate As Date, ByVal strDBName As String, ByRef suEmail As String, ByRef suName As String, ByRef suReportingEmpId As Integer)
        Dim objEmp As New clsEmployee_Master
        Dim dsList As New DataSet
        Try
            dsList = objEmp.GetEmployeeReporting(enEmployeeReporting.Employee_Reporting, intEmployeeId, dtStartDate, dtEndDate, strDBName)
            If dsList.Tables(0).Rows.Count > 0 Then
                Dim dRow() As DataRow = dsList.Tables(0).Select("ishierarchy = 1 AND Email <> ''")
                If dRow.Length > 0 Then
                    suEmail = dRow(0)("Email")
                    'S.SANDEEP |01-OCT-2019| -- START
                    'ISSUE/ENHANCEMENT : NMB DISCIPLINARY UAT COMMENTS
                    'suName = dRow(0)("Employee")
                    suName = getTitleCase(dRow(0)("dEmployee").ToString())
                    'S.SANDEEP |01-OCT-2019| -- END
                    suReportingEmpId = dRow(0)("reporttoemployeeunkid")
                End If
            End If
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetEmployeeReporting; Module Name: " & mstrModuleName)
        Finally
            objEmp = Nothing
        End Try
    End Sub
    'S.SANDEEP [26 JUL 2016] -- END

    'S.SANDEEP |11-NOV-2019| -- START
    'ISSUE/ENHANCEMENT : NMB DISCIPLINARY UAT COMMENTS
    Public Function GetComplainantInformation(Optional ByVal xDataOpr As clsDataOperation = Nothing, _
                                               Optional ByVal StrListName As String = "List") As DataTable
        Dim objDataOperation As clsDataOperation
        Dim StrQ As String = ""
        Dim exForce As Exception
        Dim dsCUser As New DataSet
        If xDataOpr IsNot Nothing Then
            objDataOperation = xDataOpr
        Else
            objDataOperation = New clsDataOperation
        End If
        objDataOperation.ClearParameters()
        Try
            Dim dsList As New DataSet
            If StrListName.Trim.Length <= 0 Then StrListName = "List"
            StrQ = "SELECT " & _
                   "     A.employeeunkid " & _
                   "    ,A.companyunkid " & _
                   "    ,A.userunkid " & _
                   "    ,A.DName " & _
                   "    ,A.EDate " & _
                   "FROM " & _
                   "( " & _
                   "    SELECT DISTINCT " & _
                   "         hrmsConfiguration..cfuser_master.employeeunkid " & _
                   "        ,cfuser_master.companyunkid " & _
                   "        ,cfuser_master.userunkid " & _
                   "        ,ISNULL(database_name, '') AS DName " & _
                   "        ,ISNULL(EffDt.key_value, CONVERT(CHAR(8), GETDATE(), 112)) AS EDate " & _
                   "        ,ROW_NUMBER()OVER(PARTITION BY cfuser_master.companyunkid,cfuser_master.userunkid ORDER BY isclosed) AS rno " & _
                   "    FROM hrdiscipline_file_master " & _
                   "        JOIN hrmsConfiguration..cfuser_master ON cfuser_master.userunkid = hrdiscipline_file_master.userunkid " & _
                   "        LEFT JOIN hrmsConfiguration..cffinancial_year_tran ON cffinancial_year_tran.companyunkid = cfuser_master.companyunkid " & _
                   "        LEFT JOIN hrmsConfiguration..cfconfiguration AS EffDt ON EffDt.companyunkid = cfuser_master.companyunkid AND UPPER(EffDt.key_name) = 'EMPLOYEEASONDATE' " & _
                   "    WHERE isvoid = 0 " & _
                   ") AS A WHERE A.rno = 1 "
            'S.SANDEEP |27-FEB-2020| -- START
            'ISSUE/ENHANCEMENT : ALL CHARGES DISPLAYED FOR PARTICULAR EMPLOYEE
            ',ROW_NUMBER()OVER(PARTITION BY cfuser_master.companyunkid ORDER BY isclosed) AS rno -- REMOVED
            ',ROW_NUMBER()OVER(PARTITION BY cfuser_master.companyunkid,cfuser_master.userunkid ORDER BY isclosed) AS rno -- Added
            'S.SANDEEP |27-FEB-2020| -- END

            dsList = objDataOperation.ExecQuery(StrQ, StrListName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Dim StrQ1, StrQ2 As String

            StrQ1 = "SELECT " & _
                    "    userunkid AS cId " & _
                    "   ,username AS cName " & _
                    "   ,'' AS cJob " & _
                    "FROM hrmsConfiguration..cfuser_master " & _
                    "WHERE isactive = 1 AND userunkid = #UID# "

            StrQ2 = "SELECT " & _
                    "    UM.userunkid AS cId " & _
                    "   ,hremployee_master.firstname + ' ' + surname AS cName " & _
                    "   ,cJ.job_name AS cJob " & _
                    "FROM #DName#hremployee_master " & _
                    "LEFT JOIN " & _
                    "( " & _
                    "   SELECT " & _
                    "        ect.employeeunkid AS empid " & _
                    "       ,ect.jobunkid " & _
                    "       ,ROW_NUMBER()OVER(PARTITION BY ect.employeeunkid ORDER BY ect.effectivedate DESC) as rno " & _
                    "   FROM #DName#hremployee_categorization_tran AS ect " & _
                    "   WHERE ect.isvoid = 0 AND CONVERT(NVARCHAR(8),ect.effectivedate,112) <= '#EDate#' " & _
                    ") AS eJ ON eJ.empid = #DName#hremployee_master.employeeunkid AND eJ.rno = 1 " & _
                    "JOIN #DName#hrjob_master AS cJ ON cJ.jobunkid = eJ.jobunkid " & _
                    "JOIN hrmsConfiguration..cfuser_master AS UM ON UM.employeeunkid = hremployee_master.employeeunkid " & _
                    "   AND UM.companyunkid = hremployee_master.companyunkid " & _
                    "WHERE #DName#hremployee_master.employeeunkid =  #UID# "
            'S.SANDEEP |27-FEB-2020| -- START
            'ISSUE/ENHANCEMENT : ALL CHARGES DISPLAYED FOR PARTICULAR EMPLOYEE
            'AND CONVERT(NVARCHAR(8),ect.effectivedate,112) <= '20191121' -- REMOVED
            'AND CONVERT(NVARCHAR(8),ect.effectivedate,112) <= '#EDate#' -- Added
            'S.SANDEEP |27-FEB-2020| -- END

            Dim dstmp As New DataSet
            If dsList.Tables(0).Rows.Count > 0 Then
                For Each dtRow As DataRow In dsList.Tables(0).Rows
                    If dtRow("companyunkid") <= 0 Then
                        StrQ = StrQ1
                        StrQ = StrQ.Replace("#UID#", dtRow("userunkid"))
                    Else
                        StrQ = StrQ2
                        StrQ = StrQ.Replace("#UID#", dtRow("employeeunkid"))
                        'S.SANDEEP |27-FEB-2020| -- START
                        'ISSUE/ENHANCEMENT : ALL CHARGES DISPLAYED FOR PARTICULAR EMPLOYEE
                        StrQ = StrQ.Replace("#EDate#", dtRow("EDate"))
                        'S.SANDEEP |27-FEB-2020| -- END
                        StrQ = StrQ.Replace("#DName#", dtRow("DName").ToString() & "..")
                    End If

                    dstmp = objDataOperation.ExecQuery(StrQ, "List")
                    If objDataOperation.ErrorMessage <> "" Then
                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If

                    If dsCUser.Tables.Count <= 0 Then
                        dsCUser = dstmp
                    Else
                        dsCUser.Tables(0).Merge(dstmp.Tables(0).Copy())
                    End If
                Next
            End If
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: ; Module Name: " & mstrModuleName)
        Finally
        End Try
        If dsCUser IsNot Nothing AndAlso dsCUser.Tables.Count > 0 Then
            Return dsCUser.Tables(0)
        Else
            Return Nothing
        End If
    End Function
    'S.SANDEEP |11-NOV-2019| -- END

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Public Shared Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Sorry, this reference number already exists. Please assign new reference number.")
            Language.setMessage(mstrModuleName, 2, "This discipline charge is already filed for selected employee. Please add new discipline charge.")
            Language.setMessage(mstrModuleName, 3, "Internal")
            Language.setMessage(mstrModuleName, 4, "External")
            Language.setMessage(mstrModuleName, 5, "WEB")
            Language.setMessage(mstrModuleName, 6, "Sorry, you cannot delete this charge. Reason : This charge is linked with discipline proceedings.")
            Language.setMessage(mstrModuleName, 7, "Sorry, you cannot delete this charge. Reason : This charge is linked with discipline hearing scheduling.")
            Language.setMessage(mstrModuleName, 8, "Select")
            Language.setMessage(mstrModuleName, 9, "Discipline Charge Notification")
            Language.setMessage(mstrModuleName, 10, " :  Reference No -")
            Language.setMessage(mstrModuleName, 11, "Dear")
            Language.setMessage(mstrModuleName, 12, "Please refer following Disciplinary Charges details posted against you, By clicking link below and provide your response for the Charge.")
            Language.setMessage(mstrModuleName, 13, "Charge Description :")
            Language.setMessage(mstrModuleName, 14, "Particular")
            Language.setMessage(mstrModuleName, 15, "Details")
            Language.setMessage(mstrModuleName, 16, "Reference No.")
            Language.setMessage(mstrModuleName, 17, "Employee")
            Language.setMessage(mstrModuleName, 18, "Charge Date")
            Language.setMessage(mstrModuleName, 19, "General Charge Description")
            Language.setMessage(mstrModuleName, 20, "Charges Count :")
            Language.setMessage(mstrModuleName, 21, "Count")
            Language.setMessage(mstrModuleName, 22, "Incident")
            Language.setMessage(mstrModuleName, 23, "Offence Category")
            Language.setMessage(mstrModuleName, 24, "Offence Description")
            Language.setMessage(mstrModuleName, 25, "Severity")
            Language.setMessage(mstrModuleName, 27, "Sorry, email is not assigned to the employee. Please make sure that you assign an email.")
            Language.setMessage(mstrModuleName, 28, "Discipline Response Notification")
            Language.setMessage(mstrModuleName, 29, "This is to inform you that, I have provided the response(s) for the charge posted against me, By clicking link below you can view my defence and attachments.")
            Language.setMessage(mstrModuleName, 30, "Sorry, Email Setup under company is not configured.")
            Language.setMessage(mstrModuleName, 31, "Regards,")
            Language.setMessage(mstrModuleName, 32, "Opening Reason")
        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class
