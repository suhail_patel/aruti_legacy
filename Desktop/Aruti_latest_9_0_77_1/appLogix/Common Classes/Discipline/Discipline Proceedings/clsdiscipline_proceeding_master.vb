﻿'************************************************************************************************************************************
'Class Name : clsdiscipline_proceeding_master.vb
'Purpose    :
'Date       : 23/06/2016
'Written By : Nilay
'Modified   :
'************************************************************************************************************************************

Imports eZeeCommonLib
Imports eZee.Common.eZeeForm
Imports System.Text

''' <summary>
''' Purpose: 
''' Developer: Nilay
''' </summary>
Public Class clsdiscipline_proceeding_master

    Private Shared ReadOnly mstrModuleName As String = "clsdiscipline_proceeding_master"
    Dim objDataOperation As clsDataOperation
    Dim mstrMessage As String = ""
    'S.SANDEEP |11-NOV-2019| -- START
    'ISSUE/ENHANCEMENT : NMB DISCIPLINARY UAT COMMENTS
    Private objDocument As New clsScan_Attach_Documents
    'S.SANDEEP |11-NOV-2019| -- END

#Region " Private variables "

    Private mintDisciplineProceedingMasterunkid As Integer = -1
    Private mdtProceeding As DataTable
    Private mintUserunkid As Integer = -1
    Private mstrWebFormName As String = String.Empty
    Private mstrWebClientIP As String = String.Empty
    Private mstrWebHostName As String = String.Empty
    Private mintLoginEmployeeunkid As Integer = -1
    'S.SANDEEP [24 MAY 2016] -- Start
    'Email Notification
    Private mintLoginTypeId As Integer = -1
    Private mstrNotificationContent As String = String.Empty
    Private mintDisciplineFileunkid As Integer = -1
    Private mblnIsApprovedDisApproved As Boolean = False
    'S.SANDEEP [24 MAY 2016] -- End

    'S.SANDEEP |01-MAY-2020| -- START
    'ISSUE/ENHANCEMENT : MODIFICATION ON REPORT (ADDTIONAL COLUMNS)
    Private mintDisciplineOpeningReasonId As Integer = 0
    'S.SANDEEP |01-MAY-2020| -- END
#End Region

#Region " Enum "
    Public Enum enProceedingCountStatus
        Open = 1
        Close = 2
    End Enum

    Public Enum enFinalStatus
        Approved = 1
        Pending = 2
    End Enum
#End Region

#Region " Properties "

    'S.SANDEEP [28-May-2018] -- START 
    'ISSUE/ENHANCEMENT : {Audit Trails} 
    Private mstrFormName As String = String.Empty
    Public WriteOnly Property _FormName() As String
        Set(ByVal value As String)
            mstrFormName = value
        End Set
    End Property

    Private mstrClientIP As String = ""
    Public WriteOnly Property _ClientIP() As String
        Set(ByVal value As String)
            mstrClientIP = value
        End Set
    End Property

    Private mstrHostName As String = ""
    Public WriteOnly Property _HostName() As String
        Set(ByVal value As String)
            mstrHostName = value
        End Set
    End Property

    Private mblnIsWeb As Boolean = False
    Public WriteOnly Property _FromWeb() As Boolean
        Set(ByVal value As Boolean)
            mblnIsWeb = value
        End Set
    End Property

    Private mintAuditUserId As Integer = 0
    Public WriteOnly Property _AuditUserId() As Integer
        Set(ByVal value As Integer)
            mintAuditUserId = value
        End Set
    End Property

    Private mintCompanyUnkid As Integer = 0
    Public WriteOnly Property _CompanyUnkid() As Integer
        Set(ByVal value As Integer)
            mintCompanyUnkid = value
        End Set
    End Property

    Private mdtAuditDate As DateTime = Now
    Public WriteOnly Property _AuditDate() As DateTime
        Set(ByVal value As DateTime)
            mdtAuditDate = value
        End Set
    End Property

    Private xDataOpr As clsDataOperation = Nothing
    Public Property _xDataOpr() As clsDataOperation
        Get
            Return xDataOpr
        End Get
        Set(ByVal value As clsDataOperation)
            xDataOpr = value
        End Set
    End Property

    Private mintVoidlogingemployeeunkid As Integer = 0
    Public Property _Voidloginemployeeunkid() As Integer
        Get
            Return mintVoidlogingemployeeunkid
        End Get
        Set(ByVal value As Integer)
            mintVoidlogingemployeeunkid = value
        End Set
    End Property
    'S.SANDEEP [28-May-2018] -- END 


    Public ReadOnly Property _Message() As String
        Get
            Return mstrMessage
        End Get
    End Property

    Public Property _DisciplineProceedingMasterunkid() As Integer
        Get
            Return mintDisciplineProceedingMasterunkid
        End Get
        Set(ByVal value As Integer)
            mintDisciplineProceedingMasterunkid = value
            Call GetProceedingDetail()
        End Set
    End Property

    Public Property _ProceedingTable() As DataTable
        Get
            Return mdtProceeding
        End Get
        Set(ByVal value As DataTable)
            mdtProceeding = value
        End Set
    End Property

    Public Property _Userunkid() As Integer
        Get
            Return mintUserunkid
        End Get
        Set(ByVal value As Integer)
            mintUserunkid = value
        End Set
    End Property

    'Public WriteOnly Property _WebFormName() As String
    '    Set(ByVal value As String)
    '        mstrWebFormName = value
    '    End Set
    'End Property

    'Public WriteOnly Property _WebClientIP() As String
    '    Set(ByVal value As String)
    '        mstrWebClientIP = value
    '    End Set
    'End Property

    'Public WriteOnly Property _WebHostName() As String
    '    Set(ByVal value As String)
    '        mstrWebHostName = value
    '    End Set
    'End Property

    Public Property _LoginEmployeeunkid() As Integer
        Get
            Return mintLoginEmployeeunkid
        End Get
        Set(ByVal value As Integer)
            mintLoginEmployeeunkid = value
        End Set
    End Property

    'S.SANDEEP [24 MAY 2016] -- Start
    'Email Notification
    Public Property _LoginTypeId() As Integer
        Get
            Return mintLoginTypeId
        End Get
        Set(ByVal value As Integer)
            mintLoginTypeId = value
        End Set
    End Property

    Public Property _DisciplineFileUnkid() As Integer
        Get
            Return mintDisciplineFileunkid
        End Get
        Set(ByVal value As Integer)
            mintDisciplineFileunkid = value
        End Set
    End Property

    Public Property _IsApprovedDisApproved() As Boolean
        Get
            Return mblnIsApprovedDisApproved
        End Get
        Set(ByVal value As Boolean)
            mblnIsApprovedDisApproved = value
        End Set
    End Property
    'S.SANDEEP [24 MAY 2016] -- End

    'S.SANDEEP |01-MAY-2020| -- START
    'ISSUE/ENHANCEMENT : MODIFICATION ON REPORT (ADDTIONAL COLUMNS)
    Public WriteOnly Property _DisciplineOpeningReasonId() As Integer
        Set(ByVal value As Integer)
            mintDisciplineOpeningReasonId = value
        End Set
    End Property
    'S.SANDEEP |01-MAY-2020| -- END

#End Region

#Region " Constructor "

    Public Sub New()
        Try
            mdtProceeding = New DataTable("Proceeding")

            mdtProceeding.Columns.Add("count", System.Type.GetType("System.String")).DefaultValue = ""
            mdtProceeding.Columns.Add("disciplineproceedingmasterunkid", System.Type.GetType("System.Int32")).DefaultValue = -1
            mdtProceeding.Columns.Add("disciplinefiletranunkid", System.Type.GetType("System.Int32")).DefaultValue = -1
            mdtProceeding.Columns.Add("disciplinefileunkid", System.Type.GetType("System.Int32")).DefaultValue = -1
            mdtProceeding.Columns.Add("committeemasterunkid", System.Type.GetType("System.Int32")).DefaultValue = -1
            mdtProceeding.Columns.Add("committee", System.Type.GetType("System.String")).DefaultValue = ""
            mdtProceeding.Columns.Add("investigator", System.Type.GetType("System.String")).DefaultValue = ""
            mdtProceeding.Columns.Add("proceeding_date", System.Type.GetType("System.DateTime")).DefaultValue = Nothing
            mdtProceeding.Columns.Add("proceeding_details", System.Type.GetType("System.String")).DefaultValue = ""
            mdtProceeding.Columns.Add("remarks_comments", System.Type.GetType("System.String")).DefaultValue = ""
            mdtProceeding.Columns.Add("person_involved_comments", System.Type.GetType("System.String")).DefaultValue = ""
            mdtProceeding.Columns.Add("actionreasonunkid", System.Type.GetType("System.Int32")).DefaultValue = -1
            mdtProceeding.Columns.Add("reason_action", System.Type.GetType("System.String")).DefaultValue = ""
            mdtProceeding.Columns.Add("isapproved", System.Type.GetType("System.Boolean")).DefaultValue = False
            mdtProceeding.Columns.Add("count_status", System.Type.GetType("System.Int32")).DefaultValue = 1
            mdtProceeding.Columns.Add("isvoid", System.Type.GetType("System.Boolean")).DefaultValue = False
            mdtProceeding.Columns.Add("voiduserunkid", System.Type.GetType("System.Int32")).DefaultValue = -1
            mdtProceeding.Columns.Add("voiddatetime", System.Type.GetType("System.DateTime")).DefaultValue = Nothing
            mdtProceeding.Columns.Add("voidreason", System.Type.GetType("System.String")).DefaultValue = ""
            mdtProceeding.Columns.Add("AUD", System.Type.GetType("System.String")).DefaultValue = ""
            mdtProceeding.Columns.Add("GUID", System.Type.GetType("System.String")).DefaultValue = ""
            mdtProceeding.Columns.Add("approval_date", GetType(DateTime)).DefaultValue = DBNull.Value
            mdtProceeding.Columns.Add("approval_remark", GetType(String)).DefaultValue = ""
            mdtProceeding.Columns.Add("status_name", GetType(String)).DefaultValue = ""
            mdtProceeding.Columns.Add("approved_status", GetType(String)).DefaultValue = ""
            mdtProceeding.Columns.Add("issubmitforapproval", GetType(Boolean)).DefaultValue = False
            mdtProceeding.Columns.Add("statusdate", GetType(DateTime)).DefaultValue = DBNull.Value
            mdtProceeding.Columns.Add("remark", GetType(String)).DefaultValue = ""
            mdtProceeding.Columns.Add("isexternal", GetType(Boolean)).DefaultValue = False


            'Pinkal (19-Dec-2020) -- Start
            'Enhancement  -  Working on Discipline module for NMB.
            mdtProceeding.Columns.Add("penalty_effectivedate", GetType(DateTime)).DefaultValue = DBNull.Value
            mdtProceeding.Columns.Add("penalty_expirydate", GetType(DateTime)).DefaultValue = DBNull.Value
            'Pinkal (19-Dec-2020) -- End


        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Constructor; Module Name: " & mstrModuleName)
        End Try
    End Sub

#End Region

    Private Sub GetProceedingDetail()
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation
        Try
            strQ = "SELECT " & _
                   "     ISNULL(CAST(C.count AS NVARCHAR(MAX)),'') AS count " & _
                   "    ,hrdiscipline_proceeding_master.disciplineproceedingmasterunkid " & _
                   "    ,hrdiscipline_proceeding_master.disciplinefiletranunkid " & _
                   "    ,hrdiscipline_proceeding_master.disciplinefileunkid " & _
                   "    ,Member.committeemasterunkid " & _
                   "    ,cfcommon_master.name AS committee " & _
                   "    ,Member.investigator " & _
                   "    ,hrdiscipline_proceeding_master.proceeding_date " & _
                   "    ,hrdiscipline_proceeding_master.proceeding_details " & _
                   "    ,hrdiscipline_proceeding_master.remarks_comments " & _
                   "    ,hrdiscipline_proceeding_master.person_involved_comments " & _
                   "    ,hrdiscipline_proceeding_master.actionreasonunkid " & _
                   "    ,hraction_reason_master.reason_action " & _
                   "    ,hrdiscipline_proceeding_master.isapproved " & _
                   "    ,hrdiscipline_proceeding_master.count_status " & _
                   "    ,hrdiscipline_proceeding_master.isvoid " & _
                   "    ,hrdiscipline_proceeding_master.voiduserunkid " & _
                   "    ,hrdiscipline_proceeding_master.voiddatetime " & _
                   "    ,hrdiscipline_proceeding_master.voidreason " & _
                   "    ,CASE WHEN hrdiscipline_proceeding_master.count_status = '" & enProceedingCountStatus.Open & "' THEN @Open " & _
                   "          WHEN hrdiscipline_proceeding_master.count_status = '" & enProceedingCountStatus.Close & "' THEN @Close " & _
                   "     ELSE '' END AS status_name " & _
                   "    ,CASE WHEN hrdiscipline_proceeding_master.isapproved = 1 Then @Approved " & _
                   "          WHEN hrdiscipline_proceeding_master.isapproved = 0 Then @Pending " & _
                   "     END AS approved_status " & _
                   "    ,'' AS AUD " & _
                   "    ,'' AS GUID " & _
                   "    ,hrdiscipline_proceeding_master.approval_date " & _
                   "    ,ISNULL(hrdiscipline_proceeding_master.approval_remark,'') AS approval_remark " & _
                   "    ,ISNULL(hrdiscipline_proceeding_master.issubmitforapproval,0) AS issubmitforapproval " & _
                   "    ,hrdiscipline_proceeding_master.statusdate " & _
                   "    ,ISNULL(hrdiscipline_proceeding_master.remark,'') AS remark " & _
                   "    ,ISNULL(hrdiscipline_proceeding_master.isexternal,0) AS isexternal " & _
                   "    ,ISNULL(hrdiscipline_proceeding_master.userunkid,0) AS userunkid " & _
                   "    ,hrdiscipline_proceeding_master.penalty_effectivedate " & _
                   "    ,hrdiscipline_proceeding_master.penalty_expirydate " & _
                   "FROM hrdiscipline_proceeding_master " & _
                   "LEFT JOIN " & _
                   " ( " & _
                   "        SELECT " & _
                   "               disciplinefiletranunkid " & _
                   "              ,ROW_NUMBER()OVER(PARTITION BY hrdiscipline_file_tran.disciplinefileunkid ORDER BY hrdiscipline_file_tran.disciplinefiletranunkid ASC) AS count " & _
                   "        FROM hrdiscipline_file_tran " & _
                   "        WHERE hrdiscipline_file_tran.isvoid = 0 " & _
                   " ) AS C ON C.disciplinefiletranunkid = hrdiscipline_proceeding_master.disciplinefiletranunkid " & _
                   "LEFT JOIN " & _
                   " ( " & _
                   "               SELECT DISTINCT " & _
                   "                   A.disciplineproceedingmasterunkid " & _
                   "                   ,CASE WHEN ISNULL(hrdiscipline_committee.committeemasterunkid,0) <= 0 THEN ISNULL(A.committeemstunkid,0) ELSE ISNULL(hrdiscipline_committee.committeemasterunkid,0)   END committeemasterunkid " & _
                   "                   ,( SELECT " & _
                   "                      STUFF( " & _
                   "                              ( SELECT " & _
                   "                                    ', ' + INVT.Name " & _
                   "                                FROM " & _
                   "                                ( " & _
                   "                                    SELECT " & _
                   "                                        CASE WHEN hrdiscipline_committee.employeeunkid <= 0 THEN hrdiscipline_committee.ex_name " & _
                   "                                             WHEN hrdiscipline_committee.employeeunkid > 0 THEN ISNULL(hremployee_master.firstname, '') + ' ' + ISNULL(hremployee_master.othername, '') + ' ' + ISNULL(hremployee_master.surname, '') END AS Name " & _
                   "                                    FROM hrdiscipline_members_tran " & _
                   "                                        JOIN hrdiscipline_proceeding_master ON hrdiscipline_proceeding_master.disciplineproceedingmasterunkid = hrdiscipline_members_tran.disciplineproceedingmasterunkid " & _
                   "                                            AND A.disciplineproceedingmasterunkid = @disciplineproceedingmasterunkid " & _
                   "                                        LEFT JOIN hrdiscipline_committee ON hrdiscipline_committee.committeetranunkid = hrdiscipline_members_tran.committeetranunkid " & _
                   "                                        LEFT JOIN hremployee_master ON hremployee_master.employeeunkid = hrdiscipline_committee.employeeunkid " & _
                   "                                    WHERE hrdiscipline_members_tran.isvoid = 0 AND hrdiscipline_members_tran.committeetranunkid > 0 " & _
                   "                                        AND A.disciplineproceedingmasterunkid = hrdiscipline_proceeding_master.disciplineproceedingmasterunkid " & _
                   "                                    UNION " & _
                   "                                    SELECT " & _
                   "                                        CASE WHEN hrdiscipline_members_tran.employeeunkid <= 0 THEN hrdiscipline_members_tran.ex_name " & _
                   "                                             WHEN hrdiscipline_members_tran.employeeunkid > 0 THEN ISNULL(hremployee_master.firstname, '') + ' ' + ISNULL(hremployee_master.othername, '') + ' ' + ISNULL(hremployee_master.surname, '') END AS Name " & _
                   "                                    FROM hrdiscipline_members_tran " & _
                   "                                        JOIN hrdiscipline_proceeding_master ON hrdiscipline_proceeding_master.disciplineproceedingmasterunkid = hrdiscipline_members_tran.disciplineproceedingmasterunkid " & _
                   "                                            AND A.disciplineproceedingmasterunkid = @disciplineproceedingmasterunkid " & _
                   "                                        LEFT JOIN hremployee_master ON hremployee_master.employeeunkid = hrdiscipline_members_tran.employeeunkid " & _
                   "                                    WHERE hrdiscipline_members_tran.isvoid = 0 AND hrdiscipline_members_tran.committeetranunkid <= 0 " & _
                   "                                        AND A.disciplineproceedingmasterunkid = hrdiscipline_proceeding_master.disciplineproceedingmasterunkid " & _
                   "                                ) AS INVT " & _
                   "                    FOR XML PATH ('')), 1, 1, '') " & _
                   "               ) AS investigator " & _
                   "               FROM hrdiscipline_members_tran AS A " & _
                   "              JOIN hrdiscipline_proceeding_master ON hrdiscipline_proceeding_master.disciplineproceedingmasterunkid = A.disciplineproceedingmasterunkid " & _
                   "                  AND A.disciplineproceedingmasterunkid = @disciplineproceedingmasterunkid " & _
                   "              LEFT JOIN hrdiscipline_committee ON hrdiscipline_committee.committeetranunkid = A.committeetranunkid AND ISNULL(hrdiscipline_committee.committeemasterunkid,0) > 0 " & _
                   "       Where A.isvoid = 0 " & _
                   " ) AS Member ON Member.disciplineproceedingmasterunkid = hrdiscipline_proceeding_master.disciplineproceedingmasterunkid " & _
                   " LEFT JOIN cfcommon_master ON cfcommon_master.masterunkid = Member.committeemasterunkid " & _
                   " LEFT JOIN hraction_reason_master ON hraction_reason_master.actionreasonunkid = hrdiscipline_proceeding_master.actionreasonunkid " & _
                   "WHERE hrdiscipline_proceeding_master.isvoid = 0 " & _
                   "AND hrdiscipline_proceeding_master.disciplineproceedingmasterunkid = @disciplineproceedingmasterunkid "

            'S.SANDEEP [25 JUL 2016] -- START
            'ENHANCEMENT : DISCIPLINE MAKE-OVER {BY ANDREW}
            '**************************************************************** ADDED START *******************
            '"    ,hrdiscipline_proceeding_master.statusdate "
            '"    ,ISNULL(hrdiscipline_proceeding_master.remark,'') AS remark "
            '"    ,ISNULL(hrdiscipline_proceeding_master.isexternal,0) AS isexternal "
            '"    ,ISNULL(hrdiscipline_proceeding_master.userunkid,0) AS userunkid "
            '**************************************************************** ADDED END *******************
            'S.SANDEEP [25 JUL 2016] -- END

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@disciplineproceedingmasterunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintDisciplineProceedingMasterunkid.ToString)
            objDataOperation.AddParameter("@Open", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 6, "Open"))
            objDataOperation.AddParameter("@Close", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 7, "Close"))
            objDataOperation.AddParameter("@Approved", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 4, "Approved"))
            objDataOperation.AddParameter("@Pending", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 5, "Pending"))

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            mdtProceeding.Rows.Clear()
            For Each drow As DataRow In dsList.Tables(0).Rows
                mdtProceeding.ImportRow(drow)
            Next

            If mdtProceeding.Rows.Count <= 0 Then
                mdtProceeding.Rows.Add(mdtProceeding.NewRow)
                mdtProceeding.Rows(0).Item("approved_status") = ""
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetProceedingDetail; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
        End Try
    End Sub

    Public Function getComboApprovalStatus(ByVal strList As String, ByVal blnAddSelect As Boolean) As DataSet
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Try
            objDataOperation = New clsDataOperation

            If blnAddSelect = True Then
                strQ = "SELECT 0 AS id, @Select AS name UNION "
            End If

            strQ &= "SELECT '" & enFinalStatus.Approved & "' AS id, @Approved AS name UNION " & _
                    "SELECT '" & enFinalStatus.Pending & "' AS id, @Pending AS name "

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@Select", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 3, "Select"))
            objDataOperation.AddParameter("@Approved", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 4, "Approved"))
            objDataOperation.AddParameter("@Pending", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 5, "Pending"))

            dsList = objDataOperation.ExecQuery(strQ, strList)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: getComboApprovalStatus; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
        End Try
        Return dsList
    End Function

    Public Function getProceedingCountStatus(ByVal strList As String, ByVal blnAddSelect As Boolean) As DataSet
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Try
            objDataOperation = New clsDataOperation

            If blnAddSelect = True Then
                strQ = "SELECT 0 AS id, @Select AS name UNION "
            End If

            strQ &= "SELECT '" & enProceedingCountStatus.Open & "' AS id, @Open AS name UNION " & _
                    "SELECT '" & enProceedingCountStatus.Close & "' AS id, @Close AS name "

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@Select", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 3, "Select"))
            objDataOperation.AddParameter("@Open", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 6, "Open"))
            objDataOperation.AddParameter("@Close", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 7, "Close"))

            dsList = objDataOperation.ExecQuery(strQ, strList)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: getProceedingCountStatus; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
        End Try
        Return dsList
    End Function

    Public Function GetList(ByVal xDatabaseName As String, _
                            ByVal xUserUnkid As Integer, _
                            ByVal xYearUnkid As Integer, _
                            ByVal xCompanyUnkid As Integer, _
                            ByVal xPeriodStart As DateTime, _
                            ByVal xPeriodEnd As DateTime, _
                            ByVal xUserModeSetting As String, _
                            ByVal xOnlyApproved As Boolean, _
                            ByVal xIncludeIn_ActiveEmployee As Boolean, _
                            ByVal strTableName As String, _
                            Optional ByVal strFilter As String = "", _
                            Optional ByVal blnAddProceedingAgainstEachCount As Boolean = True) As DataSet 'S.SANDEEP |25-OCT-2019| -- START {blnAddProceedingAgainstEachCount} -- END
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            Dim xDateJoinQry, xDateFilterQry, xUACQry, xUACFiltrQry As String
            xDateJoinQry = "" : xDateFilterQry = "" : xUACQry = "" : xUACFiltrQry = ""
            Call GetDatesFilterString(xDateJoinQry, xDateFilterQry, xPeriodStart, xPeriodEnd, , , xDatabaseName)
            Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, xPeriodEnd, xOnlyApproved, xDatabaseName, xUserUnkid, xCompanyUnkid, xYearUnkid, xUserModeSetting)

            'strQ = "SELECT " & _
            '       "     CAST(0 AS BIT) AS ischeck " & _
            '       "    ,hrdiscipline_file_master.reference_no " & _
            '       "    ,hrdiscipline_file_master.chargedate " & _
            '       "    ,hrdiscipline_file_master.charge_description " & _
            '       "    ,ISNULL(hremployee_master.firstname,'') + ' ' + ISNULL(hremployee_master.othername,'') + ' ' + ISNULL(hremployee_master.surname,'') AS person_involved " & _
            '       "    ,cnt.NoOfCount " & _
            '       "    ,cnt.incident_desc " & _
            '       "    ,cfcommon_master.name AS offence_category " & _
            '       "    ,hrdisciplinetype_master.name AS offence " & _
            '       "    ,CASE WHEN CA.statusunkid = " & enProceedingCountStatus.Open & " Then @Open " & _
            '       "           WHEN CA.statusunkid = " & enProceedingCountStatus.Close & " Then @Close " & _
            '       "      End As count_status " & _
            '       "    ,hrdiscipline_proceeding_master.count_status AS count_status_id " & _
            '       "    ,hrdiscipline_proceeding_master.proceeding_details as last_resolution " & _
            '       "    ,hraction_reason_master.reason_action AS disciplinary_penalty " & _
            '       "    ,CASE WHEN CA.isapproved = 1 Then @Approved " & _
            '       "           WHEN CA.isapproved = 0 Then @Pending " & _
            '       "      End As status " & _
            '       "    ,ISNULL(CA.isapproved,0) AS isapproved " & _
            '       "    ,hrdiscipline_proceeding_master.disciplineproceedingmasterunkid " & _
            '       "    ,hrdiscipline_proceeding_master.disciplinefileunkid " & _
            '       "    ,hrdiscipline_file_master.involved_employeeunkid " & _
            '       "FROM hrdiscipline_proceeding_master " & _
            '       "LEFT JOIN hrdiscipline_file_master ON hrdiscipline_file_master.disciplinefileunkid = hrdiscipline_proceeding_master.disciplinefileunkid " & _
            '       "JOIN " & _
            '       "( " & _
            '       "  SELECT " & _
            '       "       hrdiscipline_file_tran.disciplinefiletranunkid " & _
            '       "      ,hrdiscipline_file_tran.disciplinefileunkid " & _
            '       "      ,hrdiscipline_file_tran.offenceunkid " & _
            '       "      ,ROW_NUMBER() over(partition by hrdiscipline_file_tran.disciplinefileunkid ORDER BY hrdiscipline_file_tran.disciplinefiletranunkid) AS NoOfCount " & _
            '       "      ,ISNULL(hrdiscipline_file_tran.incident_description,'') AS incident_desc " & _
            '       "  FROM hrdiscipline_file_tran " & _
            '       "  WHERE hrdiscipline_file_tran.isvoid = 0 " & _
            '       ") AS cnt ON cnt.disciplinefileunkid = hrdiscipline_file_master.disciplinefileunkid AND cnt.disciplinefiletranunkid = hrdiscipline_proceeding_master.disciplinefiletranunkid " & _
            '       "LEFT JOIN " & _
            '       "( " & _
            '       "      SELECT " & _
            '       "           hrdiscipline_chargestatus_tran.isapproved " & _
            '       "          ,hrdiscipline_chargestatus_tran.issubmitforapproval " & _
            '       "          ,hrdiscipline_chargestatus_tran.approval_date " & _
            '       "          ,hrdiscipline_chargestatus_tran.approval_remark " & _
            '       "          ,hrdiscipline_chargestatus_tran.disciplinefiletranunkid " & _
            '       "          ,hrdiscipline_chargestatus_tran.disciplinefileunkid " & _
            '       "          ,hrdiscipline_chargestatus_tran.statusunkid " & _
            '       "          ,ROW_NUMBER()OVER(PARTITION BY hrdiscipline_chargestatus_tran.disciplinefiletranunkid ORDER BY hrdiscipline_chargestatus_tran.statusdate DESC) AS rno " & _
            '       "      FROM hrdiscipline_chargestatus_tran " & _
            '       "      WHERE hrdiscipline_chargestatus_tran.isvoid = 0 " & _
            '       ") AS CA ON CA.disciplinefiletranunkid = hrdiscipline_proceeding_master.disciplinefiletranunkid AND CA.rno = 1 " & _
            '       "LEFT JOIN hremployee_master ON hremployee_master.employeeunkid = hrdiscipline_file_master.involved_employeeunkid " & _
            '       "LEFT JOIN hrdisciplinetype_master ON hrdisciplinetype_master.disciplinetypeunkid = cnt.offenceunkid " & _
            '       "LEFT JOIN cfcommon_master ON cfcommon_master.masterunkid = hrdisciplinetype_master.offencecategoryunkid " & _
            '       "LEFT JOIN hraction_reason_master ON hraction_reason_master.actionreasonunkid = hrdiscipline_proceeding_master.actionreasonunkid "

            strQ = "SELECT " & _
                   "     CAST(0 AS BIT) AS ischeck " & _
                   "    ,hrdiscipline_file_master.reference_no " & _
                   "    ,hrdiscipline_file_master.chargedate " & _
                   "    ,hrdiscipline_file_master.charge_description " & _
                   "    ,ISNULL(hremployee_master.firstname,'') + ' ' + ISNULL(hremployee_master.othername,'') + ' ' + ISNULL(hremployee_master.surname,'') AS person_involved " & _
                   "    ,cnt.NoOfCount " & _
                   "    ,cnt.incident_desc " & _
                   "    ,cfcommon_master.name AS offence_category " & _
                   "    ,hrdisciplinetype_master.name AS offence " & _
                   "    ,CASE WHEN A.count_status = " & enProceedingCountStatus.Open & " Then @Open " & _
                   "          WHEN A.count_status = " & enProceedingCountStatus.Close & " Then @Close " & _
                   "     END AS count_status " & _
                   "    ,A.count_status AS count_status_id " & _
                   "    ,A.proceeding_details as last_resolution " & _
                   "    ,hraction_reason_master.reason_action AS disciplinary_penalty " & _
                   "    ,CASE WHEN A.isapproved = 1 Then @Approved " & _
                   "          WHEN A.isapproved = 0 Then @Pending " & _
                   "     END AS status " & _
                   "    ,ISNULL(A.isapproved,0) AS isapproved " & _
                   "    ,A.disciplineproceedingmasterunkid " & _
                   "    ,A.disciplinefileunkid " & _
                   "    ,hrdiscipline_file_master.involved_employeeunkid " & _
                   "    ,ISNULL(A.Category,'') AS Category " & _
                   "    ,ISNULL(A.CategoryId,0) AS CategoryId " & _
                   "    ,A.issubmitforapproval " & _
                   "FROM " & _
                   "( " & _
                   "    SELECT " & _
                   "         CAST(0 AS BIT) AS ischeck " & _
                   "        ,hrdiscipline_proceeding_master.disciplineproceedingmasterunkid " & _
                   "        ,hrdiscipline_proceeding_master.disciplinefiletranunkid " & _
                   "        ,hrdiscipline_proceeding_master.disciplinefileunkid " & _
                   "        ,hrdiscipline_proceeding_master.isapproved " & _
                   "        ,hrdiscipline_proceeding_master.issubmitforapproval " & _
                   "        ,hrdiscipline_proceeding_master.approval_date " & _
                   "        ,hrdiscipline_proceeding_master.approval_remark " & _
                   "        ,hrdiscipline_proceeding_master.count_status " & _
                   "        ,hrdiscipline_proceeding_master.actionreasonunkid " & _
                   "        ,hrdiscipline_proceeding_master.proceeding_date " & _
                   "        ,hrdiscipline_proceeding_master.proceeding_details " & _
                   "        ,hrdiscipline_proceeding_master.remarks_comments " & _
                   "        ,hrdiscipline_proceeding_master.person_involved_comments " & _
                   "        ,CASE WHEN isexternal = 0 THEN @INTERNAL WHEN isexternal = 1 THEN @EXTERNAL ELSE '' END AS Category " & _
                   "        ,CASE WHEN isexternal = 0 THEN 1 WHEN isexternal = 1 THEN 2 END AS CategoryId "

            If blnAddProceedingAgainstEachCount Then
                strQ &= "        ,ROW_NUMBER()OVER(PARTITION BY hrdiscipline_proceeding_master.disciplinefiletranunkid ORDER BY hrdiscipline_proceeding_master.statusdate DESC) AS rno "
            Else
                strQ &= "        ,ROW_NUMBER()OVER(PARTITION BY hrdiscipline_proceeding_master.disciplinefileunkid ORDER BY hrdiscipline_proceeding_master.statusdate DESC) AS rno "
            End If

            strQ &= "    FROM hrdiscipline_proceeding_master " & _
                   "    WHERE hrdiscipline_proceeding_master.isvoid = 0 " & _
                   ") AS A " & _
                   "LEFT JOIN hrdiscipline_file_master ON hrdiscipline_file_master.disciplinefileunkid = A.disciplinefileunkid " & _
                   "LEFT JOIN " & _
                   "( " & _
                   "    SELECT " & _
                   "         hrdiscipline_file_tran.disciplinefiletranunkid " & _
                   "        ,hrdiscipline_file_tran.disciplinefileunkid " & _
                   "        ,hrdiscipline_file_tran.offenceunkid " & _
                   "        ,ROW_NUMBER() over(partition by hrdiscipline_file_tran.disciplinefileunkid ORDER BY hrdiscipline_file_tran.disciplinefiletranunkid) AS NoOfCount " & _
                   "        ,ISNULL(hrdiscipline_file_tran.incident_description,'') AS incident_desc " & _
                   "    FROM hrdiscipline_file_tran " & _
                   "    WHERE hrdiscipline_file_tran.isvoid = 0 " & _
                   ") AS cnt ON cnt.disciplinefileunkid = hrdiscipline_file_master.disciplinefileunkid AND cnt.disciplinefiletranunkid = A.disciplinefiletranunkid " & _
                   "LEFT JOIN hremployee_master ON hremployee_master.employeeunkid = hrdiscipline_file_master.involved_employeeunkid " & _
                   "LEFT JOIN hrdisciplinetype_master ON hrdisciplinetype_master.disciplinetypeunkid = cnt.offenceunkid " & _
                   "LEFT JOIN cfcommon_master ON cfcommon_master.masterunkid = hrdisciplinetype_master.offencecategoryunkid " & _
                   "LEFT JOIN hraction_reason_master ON hraction_reason_master.actionreasonunkid = A.actionreasonunkid "

            If xDateJoinQry.Trim.Length > 0 Then
                strQ &= xDateJoinQry
            End If

            If xUACQry.Trim.Length > 0 Then
                strQ &= xUACQry
            End If

            strQ &= " WHERE A.rno = 1 "

            If xUACFiltrQry.Trim.Length > 0 Then
                strQ &= " AND " & xUACFiltrQry
            End If

            If xIncludeIn_ActiveEmployee = False Then
                If xDateFilterQry.Trim.Length > 0 Then
                    strQ &= xDateFilterQry
                End If
            End If

            If strFilter.Trim.Length > 0 Then
                strQ &= " AND " & strFilter
            End If

            strQ &= " ORDER by reference_no "

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@Approved", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 4, "Approved"))
            objDataOperation.AddParameter("@Pending", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 5, "Pending"))
            objDataOperation.AddParameter("@Open", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 6, "Open"))
            objDataOperation.AddParameter("@Close", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 7, "Close"))
            objDataOperation.AddParameter("@INTERNAL", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsDiscipline_file_master", 3, "Internal"))
            objDataOperation.AddParameter("@EXTERNAL", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsDiscipline_file_master", 4, "External"))

            dsList = objDataOperation.ExecQuery(strQ, strTableName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
        Return dsList
    End Function

    'Public Function Void_Counts(ByVal intUserId As Integer, ByVal dtCurrentDate As DateTime) As Boolean
    '    Dim exForce As Exception
    '    Dim objDataOp As New clsDataOperation
    '    Try
    '        objDataOp.BindTransaction()
    '        If InsertUpdateDeleteProceeding(objDataOp, mintUserunkid) = False Then
    '            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
    '            Throw exForce
    '        End If
    '        objDataOp.ReleaseTransaction(True)

    '    Catch ex As Exception
    '        objDataOp.ReleaseTransaction(False)
    '        Throw New Exception(ex.Message & "; Procedure Name: Void_Counts; Module Name: " & mstrModuleName)
    '    End Try
    '    Return True
    'End Function

    Public Function InsertUpdateDeleteProceeding(ByVal dtCurrentDateTime As DateTime, _
                                                 ByVal dtProcceding As DataTable, ByVal dtMemTran As DataTable, _
                                                 ByVal mdtScanDocs As DataTable, _
                                                 ByVal mdtEffectiveDate As Date, _
                                                 ByVal blnIsFromOpenCharge_ApproveDisApprove_Screen As Boolean, _
                                                 ByVal intCompanyUnkId As Integer, _
                                                 ByVal strTransactionScreen As String, _
                                                 ByVal blnIsOpenCharge As Boolean, _
                                                 ByVal blnIsProceedingAgainstEachCount As Boolean) As Boolean
        'S.SANDEEP |25-MAR-2020| -- START {blnIsOpenCharge}-- END
        'S.SANDEEP |11-NOV-2019| -- START {mdtScanDocs} -- END
        'Sohail (30 Nov 2017) - [intCompanyUnkId]
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim dsList As New DataSet

        objDataOperation = New clsDataOperation
        objDataOperation.BindTransaction()
        Try
            For Each dRow As DataRow In dtProcceding.Rows
                objDataOperation.ClearParameters()
                With dRow
                    If Not IsDBNull(.Item("AUD")) Then
                        Select Case .Item("AUD")
                            Case "A"
                                strQ = "INSERT INTO hrdiscipline_proceeding_master ( " & _
                                          "  disciplinefiletranunkid " & _
                                          ", disciplinefileunkid " & _
                                          ", proceeding_date " & _
                                          ", proceeding_details " & _
                                          ", remarks_comments " & _
                                          ", person_involved_comments " & _
                                          ", actionreasonunkid " & _
                                          ", count_status " & _
                                          ", userunkid " & _
                                          ", isvoid " & _
                                          ", voiduserunkid " & _
                                          ", voiddatetime " & _
                                          ", voidreason " & _
                                          ", issubmitforapproval " & _
                                          ", statusdate " & _
                                          ", remark " & _
                                          ", isexternal " & _
                                          ", isapproved " & _
                                          ", approval_date " & _
                                          ", approval_remark " & _
                                          ", penalty_effectivedate " & _
                                          ", penalty_expirydate " & _
                                       ") VALUES (" & _
                                          "  @disciplinefiletranunkid " & _
                                          ", @disciplinefileunkid " & _
                                          ", @proceeding_date " & _
                                          ", @proceeding_details " & _
                                          ", @remarks_comments " & _
                                          ", @person_involved_comments " & _
                                          ", @actionreasonunkid " & _
                                          ", @count_status " & _
                                          ", @userunkid " & _
                                          ", @isvoid " & _
                                          ", @voiduserunkid " & _
                                          ", @voiddatetime " & _
                                          ", @voidreason " & _
                                          ", @issubmitforapproval " & _
                                          ", @statusdate " & _
                                          ", @remark " & _
                                          ", @isexternal " & _
                                          ", @isapproved " & _
                                          ", @approval_date " & _
                                          ", @approval_remark " & _
                                          ", @penalty_effectivedate " & _
                                          ", @penalty_expirydate " & _
                                       "); SELECT @@identity"


                                'Pinkal (19-Dec-2020) -- Enhancement  -  Working on Discipline module for NMB.[ , penalty_effectivedate , penalty_expirydate " ]

                                objDataOperation.ClearParameters()
                                objDataOperation.AddParameter("@disciplinefiletranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("disciplinefiletranunkid"))
                                objDataOperation.AddParameter("@disciplinefileunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("disciplinefileunkid"))
                                If .Item("proceeding_date").ToString <> Nothing Then
                                    objDataOperation.AddParameter("@proceeding_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, .Item("proceeding_date"))
                                Else
                                    objDataOperation.AddParameter("@proceeding_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
                                End If
                                objDataOperation.AddParameter("@proceeding_details", SqlDbType.Text, .Item("proceeding_details").ToString.Length, .Item("proceeding_details"))
                                objDataOperation.AddParameter("@remarks_comments", SqlDbType.Text, .Item("remarks_comments").ToString.Length, .Item("remarks_comments"))
                                objDataOperation.AddParameter("@person_involved_comments", SqlDbType.Text, .Item("person_involved_comments").ToString.Length, .Item("person_involved_comments"))
                                objDataOperation.AddParameter("@actionreasonunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("actionreasonunkid"))
                                objDataOperation.AddParameter("@count_status", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("count_status"))
                                objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid)
                                objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, .Item("isvoid"))
                                objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("voiduserunkid"))
                                If .Item("voiddatetime").ToString <> Nothing Then
                                    objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, .Item("voiddatetime"))
                                Else
                                    objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
                                End If
                                objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, .Item("voidreason"))
                                objDataOperation.AddParameter("@issubmitforapproval", SqlDbType.Bit, eZeeDataType.BIT_SIZE, .Item("issubmitforapproval"))
                                objDataOperation.AddParameter("@statusdate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, .Item("statusdate"))
                                objDataOperation.AddParameter("@remark", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, .Item("remark"))
                                objDataOperation.AddParameter("@isexternal", SqlDbType.Bit, eZeeDataType.BIT_SIZE, .Item("isexternal"))

                                objDataOperation.AddParameter("@isapproved", SqlDbType.Bit, eZeeDataType.BIT_SIZE, .Item("isapproved"))
                                If .Item("approval_date").ToString <> Nothing Then
                                    objDataOperation.AddParameter("@approval_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, .Item("approval_date"))
                                Else
                                    objDataOperation.AddParameter("@approval_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
                                End If
                                objDataOperation.AddParameter("@approval_remark", SqlDbType.NVarChar, .Item("approval_remark").ToString.Length, .Item("approval_remark"))


                                'Pinkal (19-Dec-2020) -- Start
                                'Enhancement  -  Working on Discipline module for NMB.
                                If .Item("penalty_effectivedate").ToString <> Nothing Then
                                    objDataOperation.AddParameter("@penalty_effectivedate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, .Item("penalty_effectivedate"))
                                Else
                                    objDataOperation.AddParameter("@penalty_effectivedate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
                                End If

                                If .Item("penalty_expirydate").ToString <> Nothing Then
                                    objDataOperation.AddParameter("@penalty_expirydate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, .Item("penalty_expirydate"))
                                Else
                                    objDataOperation.AddParameter("@penalty_expirydate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
                                End If
                                'Pinkal (19-Dec-2020) -- End

                                dsList = objDataOperation.ExecQuery(strQ, "List")

                                If objDataOperation.ErrorMessage <> "" Then
                                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                    Throw exForce
                                End If

                                mintDisciplineProceedingMasterunkid = dsList.Tables(0).Rows(0).Item(0)

                                If InsertATProceedingMaster(objDataOperation, mintUserunkid, dtCurrentDateTime, dRow, enAuditType.ADD) = False Then
                                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                    Throw exForce
                                End If

                                If blnIsFromOpenCharge_ApproveDisApprove_Screen = True Then
                                    Call getOpenCountDetailsForEmail(dRow)
                                End If

                                Dim objMemTran As New clsdiscipline_members_tran

                                If dtMemTran IsNot Nothing Then
                                    objMemTran._DataOperation = objDataOperation
                                    objMemTran._DisciplineProceedingMasterunkid(mdtEffectiveDate) = mintDisciplineProceedingMasterunkid
                                    objMemTran._MemTranTable = dtMemTran
                                    'objMemTran._ClientIP = mstrWebClientIP
                                    'objMemTran._WebFormName = mstrWebFormName
                                    'objMemTran._HostName = mstrWebHostName

                                    objMemTran._ClientIP = mstrClientIP
                                    objMemTran._FormName = mstrFormName
                                    objMemTran._HostName = mstrHostName
                                    objMemTran._AuditDate = mdtAuditDate
                                    objMemTran._AuditUserId = mintAuditUserId
objMemTran._CompanyUnkid = mintCompanyUnkid
                                    objMemTran._FromWeb = mblnIsWeb

                                    objMemTran._DisciplineFileTranunkid = CInt(.Item("disciplinefiletranunkid"))
                                    'S.SANDEEP [24 MAY 2016] -- Start
                                    'Email Notification
                                    objMemTran._DisciplineFileUnkid = mintDisciplineFileunkid
                                    objMemTran._LoginEmployeeunkid = mintLoginEmployeeunkid
                                    objMemTran._LoginTypeId = mintLoginTypeId
                                    'S.SANDEEP [24 MAY 2016] -- End

                                    'Sohail (30 Nov 2017) -- Start
                                    'SUMATRA Enhancement – SUMATRA – issue # 0001669: Additional feature for Exchange Server configuration setting in 70.1.
                                    'If objMemTran.InsertUpdateDeleteMembersTran(objDataOperation, mintUserunkid, dtCurrentDateTime, _
                                    '                                            blnIsFromOpenCharge_ApproveDisApprove_Screen, mstrNotificationContent) = False Then
                                    'Hemant (22 Jan 2021) -- Start
                                    'Enhancement #OLD-275 - NMB Discipline Open Case Notification
                                    'If objMemTran.InsertUpdateDeleteMembersTran(objDataOperation, mintUserunkid, dtCurrentDateTime, _
                                    '                                            blnIsFromOpenCharge_ApproveDisApprove_Screen, intCompanyUnkId, mstrNotificationContent) = False Then
                                    If objMemTran.InsertUpdateDeleteMembersTran(objDataOperation, mintUserunkid, dtCurrentDateTime, _
                                                                            blnIsFromOpenCharge_ApproveDisApprove_Screen, intCompanyUnkId, mdtScanDocs, mstrNotificationContent) = False Then
                                        'Hemant (22 Jan 2021) -- End

                                        'Sohail (30 Nov 2017) -- End
                                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                        Throw exForce
                                    End If
                                End If

                                'Dim objChargeStatus As New clsDiscipline_chargestatus_tran
                                'objChargeStatus._mDataOperation = objDataOperation
                                'objChargeStatus._Disciplinefiletranunkid = .Item("disciplinefiletranunkid")
                                'objChargeStatus._Disciplinefileunkid = .Item("disciplinefileunkid")
                                'objChargeStatus._SetCaseOpenModeAutomatic = True
                                'objChargeStatus._Isvoid = False
                                'objChargeStatus._Remark = ""
                                'objChargeStatus._Statusdate = dtCurrentDateTime
                                'objChargeStatus._Statusunkid = .Item("count_status")
                                'objChargeStatus._Userunkid = mintUserunkid
                                'objChargeStatus._Voidatetime = Nothing
                                'objChargeStatus._Voidreason = ""
                                'objChargeStatus._Voiduserunkid = -1
                                'objChargeStatus._Approval_Remark = ""
                                'objChargeStatus._ApprovalDate = Nothing
                                'objChargeStatus._IsApproved = False
                                'objChargeStatus._IssubmitforApproval = False
                                'If objChargeStatus.Insert() = False Then
                                '    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                '    Throw exForce
                                'End If
                                'objChargeStatus = Nothing

                                'S.SANDEEP |25-MAR-2020| -- START
                                'ISSUE/ENHANCEMENT : NEW DISCIPLINE REPORT
                                'If IsCloseCharge(CInt(.Item("disciplinefileunkid")), objDataOperation) = True Then
                                '    Dim objDiscStatus As New clsDiscipline_StatusTran
                                '    objDiscStatus._Disciplinefileunkid = CInt(.Item("disciplinefileunkid"))
                                '    objDiscStatus._Disciplinestatusunkid = 1    'CASE CLOSED (hrdisciplinestatus_master Unkid = 1)
                                '    objDiscStatus._SetCaseOpenModeAutomatic = True
                                '    objDiscStatus._Isvoid = False
                                '    objDiscStatus._objDataOperation = objDataOperation
                                '    objDiscStatus._Remark = ""
                                '    objDiscStatus._Statusdate = dtCurrentDateTime
                                '    objDiscStatus._Userunkid = mintUserunkid
                                '    objDiscStatus._Voiddatetime = Nothing
                                '    objDiscStatus._Voidreason = ""
                                '    objDiscStatus._Voiduserunkid = -1
                                '    If objDiscStatus.Insert() = False Then
                                '        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                '        Throw exForce
                                '    End If
                                'End If
                                Dim objDiscStatus As New clsDiscipline_StatusTran
                                If blnIsOpenCharge = False Then                                    
                                    If blnIsProceedingAgainstEachCount = True Then
                                If IsCloseCharge(CInt(.Item("disciplinefileunkid")), objDataOperation) = True Then
                                    objDiscStatus._Disciplinefileunkid = CInt(.Item("disciplinefileunkid"))
                                    objDiscStatus._Disciplinestatusunkid = 1    'CASE CLOSED (hrdisciplinestatus_master Unkid = 1)
                                    objDiscStatus._SetCaseOpenModeAutomatic = True
                                    objDiscStatus._Isvoid = False
                                    objDiscStatus._objDataOperation = objDataOperation
                                    objDiscStatus._Remark = ""
                                    objDiscStatus._Statusdate = dtCurrentDateTime
                                    objDiscStatus._Userunkid = mintUserunkid
                                    objDiscStatus._Voiddatetime = Nothing
                                    objDiscStatus._Voidreason = ""
                                    objDiscStatus._Voiduserunkid = -1
With objDiscStatus
                                        ._FormName = mstrFormName
                                        ._LoginEmployeeunkid = mintLoginEmployeeunkid
                                        ._ClientIP = mstrClientIP
                                        ._HostName = mstrHostName
                                        ._FromWeb = mblnIsWeb
                                        ._AuditUserId = mintAuditUserId
._CompanyUnkid = mintCompanyUnkid
                                        ._AuditDate = mdtAuditDate
                                    End With                                    
If objDiscStatus.Insert() = False Then
                                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                        Throw exForce
                                    End If
                                End If
                                Else
                                    objDiscStatus._Disciplinefileunkid = CInt(.Item("disciplinefileunkid"))
                                        objDiscStatus._Disciplinestatusunkid = 1    'CASE CLOSED (hrdisciplinestatus_master Unkid = 1)
                                        objDiscStatus._SetCaseOpenModeAutomatic = True
                                        objDiscStatus._Isvoid = False
                                        objDiscStatus._objDataOperation = objDataOperation
                                        objDiscStatus._Remark = ""
                                        objDiscStatus._Statusdate = dtCurrentDateTime
                                        objDiscStatus._Userunkid = mintUserunkid
                                        objDiscStatus._Voiddatetime = Nothing
                                        objDiscStatus._Voidreason = ""
                                        objDiscStatus._Voiduserunkid = -1
                                    If objDiscStatus.Insert() = False Then
                                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                        Throw exForce
                                    End If
                                End If
                                Else
                                    objDiscStatus._Disciplinefileunkid = CInt(.Item("disciplinefileunkid"))
                                    objDiscStatus._Disciplinestatusunkid = 3    'CASE REOPNED (hrdisciplinestatus_master Unkid = 1)
                                    objDiscStatus._SetCaseOpenModeAutomatic = True
                                    objDiscStatus._Isvoid = False
                                    objDiscStatus._objDataOperation = objDataOperation
                                    objDiscStatus._Remark = ""
                                    objDiscStatus._Statusdate = dtCurrentDateTime
                                    objDiscStatus._Userunkid = mintUserunkid
                                    objDiscStatus._Voiddatetime = Nothing
                                    objDiscStatus._Voidreason = ""
                                    objDiscStatus._Voiduserunkid = -1
'S.SANDEEP |01-MAY-2020| -- START
                                    'ISSUE/ENHANCEMENT : MODIFICATION ON REPORT (ADDTIONAL COLUMNS)
                                    objDiscStatus._DisciplineOpeningReasonId = mintDisciplineOpeningReasonId
                                    'S.SANDEEP |01-MAY-2020| -- END
 With objDiscStatus
                                        ._FormName = mstrFormName
                                        ._LoginEmployeeunkid = mintLoginEmployeeunkid
                                        ._ClientIP = mstrClientIP
                                        ._HostName = mstrHostName
                                        ._FromWeb = mblnIsWeb
                                        ._AuditUserId = mintAuditUserId
._CompanyUnkid = mintCompanyUnkid
                                        ._AuditDate = mdtAuditDate
                                    End With                                    
If objDiscStatus.Insert() = False Then
                                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                        Throw exForce
                                    End If
                                End If
                                'S.SANDEEP |25-MAR-2020| -- END
                                

                                'S.SANDEEP |11-NOV-2019| -- START
                                'ISSUE/ENHANCEMENT : NMB DISCIPLINARY UAT COMMENTS
                                If mdtScanDocs IsNot Nothing AndAlso mdtScanDocs.Rows.Count > 0 Then
                                    Dim dtTab As DataTable = Nothing
                                    Dim row As DataRow() = mdtScanDocs.Select("AUD <> 'D'")
                                    If row.Length > 0 Then
                                        row.ToList.ForEach(Function(x) UpdateRowValue(x, mintDisciplineProceedingMasterunkid, strTransactionScreen))
                                        dtTab = row.CopyToDataTable()
                                        objDocument._Datatable = dtTab
                                        objDocument.InsertUpdateDelete_Documents(objDataOperation)
                                        If objDataOperation.ErrorMessage <> "" Then
                                            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                            Throw exForce
                                        End If
                                    End If
                                End If
                                'S.SANDEEP |11-NOV-2019| -- END

                            Case "U"
                                strQ = "UPDATE hrdiscipline_proceeding_master SET " & _
                                         "  disciplinefiletranunkid = @disciplinefiletranunkid " & _
                                         ", disciplinefileunkid = @disciplinefileunkid " & _
                                         ", proceeding_date = @proceeding_date " & _
                                         ", proceeding_details = @proceeding_details " & _
                                         ", remarks_comments = @remarks_comments " & _
                                         ", person_involved_comments = @person_involved_comments " & _
                                         ", actionreasonunkid = @actionreasonunkid " & _
                                         ", count_status = @count_status " & _
                                         ", userunkid = @userunkid" & _
                                         ", isvoid = @isvoid" & _
                                         ", voiduserunkid = @voiduserunkid" & _
                                         ", voiddatetime = @voiddatetime" & _
                                         ", voidreason = @voidreason " & _
                                         ", issubmitforapproval = @issubmitforapproval " & _
                                         ", statusdate = @statusdate " & _
                                         ", remark = @remark " & _
                                         ", isexternal = @isexternal " & _
                                         ", isapproved = @isapproved " & _
                                         ", approval_date = @approval_date " & _
                                         ", approval_remark = @approval_remark " & _
                                         ", penalty_effectivedate = @penalty_effectivedate " & _
                                         ", penalty_expirydate = @penalty_expirydate " & _
                                         " WHERE disciplineproceedingmasterunkid = @disciplineproceedingmasterunkid "


                                'Pinkal (19-Dec-2020) -- Enhancement  -  Working on Discipline module for NMB.[penalty_effectivedate = @penalty_effectivedate , penalty_expirydate = @penalty_expirydate]


                                objDataOperation.ClearParameters()
                                objDataOperation.AddParameter("@disciplineproceedingmasterunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("disciplineproceedingmasterunkid"))
                                objDataOperation.AddParameter("@disciplinefiletranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("disciplinefiletranunkid"))
                                objDataOperation.AddParameter("@disciplinefileunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("disciplinefileunkid"))
                                If .Item("proceeding_date").ToString <> Nothing Then
                                    objDataOperation.AddParameter("@proceeding_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, .Item("proceeding_date"))
                                Else
                                    objDataOperation.AddParameter("@proceeding_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
                                End If
                                objDataOperation.AddParameter("@proceeding_details", SqlDbType.Text, .Item("proceeding_details").ToString.Length, .Item("proceeding_details"))
                                objDataOperation.AddParameter("@remarks_comments", SqlDbType.Text, .Item("remarks_comments").ToString.Length, .Item("remarks_comments"))
                                objDataOperation.AddParameter("@person_involved_comments", SqlDbType.Text, .Item("person_involved_comments").ToString.Length, .Item("person_involved_comments"))
                                objDataOperation.AddParameter("@actionreasonunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("actionreasonunkid"))
                                objDataOperation.AddParameter("@count_status", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("count_status"))
                                objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid)
                                objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, .Item("isvoid"))
                                objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("voiduserunkid"))
                                If .Item("voiddatetime").ToString <> Nothing Then
                                    objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, .Item("voiddatetime"))
                                Else
                                    objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
                                End If
                                objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, .Item("voidreason"))

                                objDataOperation.AddParameter("@issubmitforapproval", SqlDbType.Bit, eZeeDataType.BIT_SIZE, .Item("issubmitforapproval"))
                                objDataOperation.AddParameter("@statusdate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, .Item("statusdate"))
                                objDataOperation.AddParameter("@remark", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, .Item("remark"))
                                objDataOperation.AddParameter("@isexternal", SqlDbType.Bit, eZeeDataType.BIT_SIZE, .Item("isexternal"))

                                objDataOperation.AddParameter("@isapproved", SqlDbType.Bit, eZeeDataType.BIT_SIZE, .Item("isapproved"))
                                If .Item("approval_date").ToString <> Nothing Then
                                    objDataOperation.AddParameter("@approval_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, .Item("approval_date"))
                                Else
                                    objDataOperation.AddParameter("@approval_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
                                End If
                                objDataOperation.AddParameter("@approval_remark", SqlDbType.NVarChar, .Item("approval_remark").ToString.Length, .Item("approval_remark"))


                                'Pinkal (19-Dec-2020) -- Start
                                'Enhancement  -  Working on Discipline module for NMB.
                                If .Item("penalty_effectivedate").ToString <> Nothing Then
                                    objDataOperation.AddParameter("@penalty_effectivedate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, .Item("penalty_effectivedate"))
                                Else
                                    objDataOperation.AddParameter("@penalty_effectivedate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
                                End If

                                If .Item("penalty_expirydate").ToString <> Nothing Then
                                    objDataOperation.AddParameter("@penalty_expirydate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, .Item("penalty_expirydate"))
                                Else
                                    objDataOperation.AddParameter("@penalty_expirydate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
                                End If
                                'Pinkal (19-Dec-2020) -- End


                                objDataOperation.ExecNonQuery(strQ)

                                If objDataOperation.ErrorMessage <> "" Then
                                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                    Throw exForce
                                End If

                                If InsertATProceedingMaster(objDataOperation, mintUserunkid, dtCurrentDateTime, dRow, enAuditType.EDIT) = False Then
                                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                    Throw exForce
                                End If

                                Call getOpenCountDetailsForEmail(dRow)

                                'Dim objChargeStatus As New clsDiscipline_chargestatus_tran
                                'Dim intLastStatus = 0 : Dim intLastChargeUnkid As Integer = 0
                                'intLastChargeUnkid = objChargeStatus.GetLastChargeStatusUnkid(.Item("disciplinefiletranunkid"))
                                'intLastStatus = objChargeStatus.GetLastStatus(.Item("disciplinefiletranunkid"))
                                'If intLastStatus <> CInt(.Item("count_status")) Then
                                '    objChargeStatus._mDataOperation = objDataOperation
                                '    objChargeStatus._Disciplinefiletranunkid = .Item("disciplinefiletranunkid")
                                '    objChargeStatus._Disciplinefileunkid = .Item("disciplinefileunkid")
                                '    objChargeStatus._SetCaseOpenModeAutomatic = True
                                '    objChargeStatus._Isvoid = False
                                '    objChargeStatus._Remark = ""
                                '    objChargeStatus._Statusdate = dtCurrentDateTime
                                '    objChargeStatus._Statusunkid = .Item("count_status")
                                '    objChargeStatus._Userunkid = mintUserunkid
                                '    objChargeStatus._Voidatetime = Nothing
                                '    objChargeStatus._Voidreason = ""
                                '    objChargeStatus._Voiduserunkid = -1
                                '    objChargeStatus._IsApproved = CBool(.Item("isapproved"))
                                '    objChargeStatus._Approval_Remark = CStr(.Item("approval_remark"))
                                '    objChargeStatus._ApprovalDate = .Item("approval_date")
                                '    If objChargeStatus.Insert() = False Then
                                '        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                '        Throw exForce
                                '    End If
                                'Else
                                '    objChargeStatus._mDataOperation = objDataOperation
                                '    objChargeStatus._Chargestatusunkid = intLastChargeUnkid
                                '    objChargeStatus._IsApproved = CBool(.Item("isapproved"))
                                '    objChargeStatus._Approval_Remark = CStr(.Item("approval_remark"))
                                '    objChargeStatus._ApprovalDate = .Item("approval_date")
                                '    If objChargeStatus.Update() = False Then
                                '        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                '        Throw exForce
                                '    End If
                                'End If
                                'objChargeStatus = Nothing

                                'S.SANDEEP |25-MAR-2020| -- START
                                'ISSUE/ENHANCEMENT : NEW DISCIPLINE REPORT
                                'If IsCloseCharge(CInt(.Item("disciplinefileunkid")), objDataOperation) = True Then
                                '    Dim objDiscStatus As New clsDiscipline_StatusTran
                                '    objDiscStatus._Disciplinefileunkid = CInt(.Item("disciplinefileunkid"))
                                '    objDiscStatus._Disciplinestatusunkid = 1    'CASE CLOSED (hrdisciplinestatus_master Unkid = 1)
                                '    objDiscStatus._SetCaseOpenModeAutomatic = True
                                '    objDiscStatus._Isvoid = False
                                '    objDiscStatus._objDataOperation = objDataOperation
                                '    objDiscStatus._Remark = ""
                                '    objDiscStatus._Statusdate = dtCurrentDateTime
                                '    objDiscStatus._Userunkid = mintUserunkid
                                '    objDiscStatus._Voiddatetime = Nothing
                                '    objDiscStatus._Voidreason = ""
                                '    objDiscStatus._Voiduserunkid = -1
                                '    If objDiscStatus.Insert() = False Then
                                '        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                '        Throw exForce
                                '    End If
                                'End If
                                Dim objDiscStatus As New clsDiscipline_StatusTran
                                If blnIsOpenCharge = False Then
                                    If blnIsProceedingAgainstEachCount = True Then
                                If IsCloseCharge(CInt(.Item("disciplinefileunkid")), objDataOperation) = True Then
                                    objDiscStatus._Disciplinefileunkid = CInt(.Item("disciplinefileunkid"))
                                    objDiscStatus._Disciplinestatusunkid = 1    'CASE CLOSED (hrdisciplinestatus_master Unkid = 1)
                                    objDiscStatus._SetCaseOpenModeAutomatic = True
                                    objDiscStatus._Isvoid = False
                                    objDiscStatus._objDataOperation = objDataOperation
                                    objDiscStatus._Remark = ""
                                    objDiscStatus._Statusdate = dtCurrentDateTime
                                    objDiscStatus._Userunkid = mintUserunkid
                                    objDiscStatus._Voiddatetime = Nothing
                                    objDiscStatus._Voidreason = ""
                                    objDiscStatus._Voiduserunkid = -1
With objDiscStatus
                                        ._FormName = mstrFormName
                                        ._LoginEmployeeunkid = mintLoginEmployeeunkid
                                        ._ClientIP = mstrClientIP
                                        ._HostName = mstrHostName
                                        ._FromWeb = mblnIsWeb
                                        ._AuditUserId = mintAuditUserId
._CompanyUnkid = mintCompanyUnkid
                                        ._AuditDate = mdtAuditDate
                                    End With                                    
If objDiscStatus.Insert() = False Then
                                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                        Throw exForce
                                    End If
                                End If
                                Else
                                    objDiscStatus._Disciplinefileunkid = CInt(.Item("disciplinefileunkid"))
                                        objDiscStatus._Disciplinestatusunkid = 1    'CASE CLOSED (hrdisciplinestatus_master Unkid = 1)
                                        objDiscStatus._SetCaseOpenModeAutomatic = True
                                        objDiscStatus._Isvoid = False
                                        objDiscStatus._objDataOperation = objDataOperation
                                        objDiscStatus._Remark = ""
                                        objDiscStatus._Statusdate = dtCurrentDateTime
                                        objDiscStatus._Userunkid = mintUserunkid
                                        objDiscStatus._Voiddatetime = Nothing
                                        objDiscStatus._Voidreason = ""
                                        objDiscStatus._Voiduserunkid = -1
                                    If objDiscStatus.Insert() = False Then
                                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                        Throw exForce
                                    End If
                                End If
                                Else
                                    objDiscStatus._Disciplinefileunkid = CInt(.Item("disciplinefileunkid"))
                                    objDiscStatus._Disciplinestatusunkid = 3    'CASE REOPNED (hrdisciplinestatus_master Unkid = 1)
                                    objDiscStatus._SetCaseOpenModeAutomatic = True
                                    objDiscStatus._Isvoid = False
                                    objDiscStatus._objDataOperation = objDataOperation
                                    objDiscStatus._Remark = ""
                                    objDiscStatus._Statusdate = dtCurrentDateTime
                                    objDiscStatus._Userunkid = mintUserunkid
                                    objDiscStatus._Voiddatetime = Nothing
                                    objDiscStatus._Voidreason = ""
                                    objDiscStatus._Voiduserunkid = -1
'S.SANDEEP |01-MAY-2020| -- START
                                    'ISSUE/ENHANCEMENT : MODIFICATION ON REPORT (ADDTIONAL COLUMNS)
                                    objDiscStatus._DisciplineOpeningReasonId = mintDisciplineOpeningReasonId
                                    'S.SANDEEP |01-MAY-2020| -- END
                                     With objDiscStatus
                                        ._FormName = mstrFormName
                                        ._LoginEmployeeunkid = mintLoginEmployeeunkid
                                        ._ClientIP = mstrClientIP
                                        ._HostName = mstrHostName
                                        ._FromWeb = mblnIsWeb
                                        ._AuditUserId = mintAuditUserId
._CompanyUnkid = mintCompanyUnkid
                                        ._AuditDate = mdtAuditDate
                                    End With
If objDiscStatus.Insert() = False Then
                                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                        Throw exForce
                                    End If
                                End If
                                'S.SANDEEP |25-MAR-2020| -- END



                                'Pinkal (19-Dec-2020) -- Start
                                'Enhancement  -  Working on Discipline module for NMB.


                                Dim objMemTran As New clsdiscipline_members_tran

                                If dtMemTran IsNot Nothing Then
                                    objMemTran._DataOperation = objDataOperation
                                    objMemTran._DisciplineProceedingMasterunkid(mdtEffectiveDate) = mintDisciplineProceedingMasterunkid
                                    objMemTran._MemTranTable = dtMemTran
                                    objMemTran._ClientIP = mstrWebClientIP
                                    objMemTran._FormName = mstrWebFormName
                                    objMemTran._HostName = mstrWebHostName
                                    objMemTran._DisciplineFileTranunkid = CInt(.Item("disciplinefiletranunkid"))
                                    objMemTran._DisciplineFileUnkid = mintDisciplineFileunkid
                                    objMemTran._LoginEmployeeunkid = mintLoginEmployeeunkid
                                    objMemTran._LoginTypeId = mintLoginTypeId
                                    
                                    'Hemant (22 Jan 2021) -- Start
                                    'Enhancement #OLD-275 - NMB Discipline Open Case Notification
                                    'If objMemTran.InsertUpdateDeleteMembersTran(objDataOperation, mintUserunkid, dtCurrentDateTime, _
                                    '                                            blnIsFromOpenCharge_ApproveDisApprove_Screen, intCompanyUnkId, mstrNotificationContent) = False Then
                                    If objMemTran.InsertUpdateDeleteMembersTran(objDataOperation, mintUserunkid, dtCurrentDateTime, _
                                                                            blnIsFromOpenCharge_ApproveDisApprove_Screen, intCompanyUnkId, mdtScanDocs, mstrNotificationContent) = False Then
                                        'Hemant (22 Jan 2021) -- End

                                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                        Throw exForce
                                    End If
                                End If
                                'Pinkal (19-Dec-2020) -- End



                                'S.SANDEEP |11-NOV-2019| -- START
                                'ISSUE/ENHANCEMENT : NMB DISCIPLINARY UAT COMMENTS
                                If mdtScanDocs IsNot Nothing AndAlso mdtScanDocs.Rows.Count > 0 Then
                                    Dim dtTab As DataTable = Nothing
                                    Dim row As DataRow() = mdtScanDocs.Select("transactionunkid = '" & .Item("disciplineproceedingmasterunkid").ToString & "' AND form_name = 'frmDisciplineProceedingList' AND AUD <> ''")
                                    If row.Length > 0 Then
                                        dtTab = row.CopyToDataTable()
                                        objDocument._Datatable = dtTab
                                        objDocument.InsertUpdateDelete_Documents(objDataOperation)
                                        If objDataOperation.ErrorMessage <> "" Then
                                            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                            Throw exForce
                                        End If
                                    End If
                                End If
                                'S.SANDEEP |11-NOV-2019| -- END

                            Case "D"
                                strQ = "UPDATE hrdiscipline_proceeding_master SET " & _
                                          "  isvoid = @isvoid" & _
                                          ", voiduserunkid = @voiduserunkid" & _
                                          ", voiddatetime = @voiddatetime" & _
                                          ", voidreason = @voidreason " & _
                                       "WHERE disciplineproceedingmasterunkid = @disciplineproceedingmasterunkid "

                                objDataOperation.ClearParameters()
                                objDataOperation.AddParameter("@disciplineproceedingmasterunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("disciplineproceedingmasterunkid"))
                                objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, .Item("isvoid"))
                                objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("voiduserunkid"))
                                If .Item("voiddatetime").ToString <> Nothing Then
                                    objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, .Item("voiddatetime"))
                                Else
                                    objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
                                End If
                                objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, .Item("voidreason"))

                                objDataOperation.ExecNonQuery(strQ)

                                If objDataOperation.ErrorMessage <> "" Then
                                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                    Throw exForce
                                End If

                                If InsertATProceedingMaster(objDataOperation, mintUserunkid, dtCurrentDateTime, dRow, enAuditType.DELETE) = False Then
                                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                    Throw exForce
                                End If

                                Dim objMemTran As New clsdiscipline_members_tran

                                If dtMemTran IsNot Nothing Then
                                    objMemTran._DataOperation = objDataOperation
                                    objMemTran._DisciplineProceedingMasterunkid(mdtEffectiveDate) = mintDisciplineProceedingMasterunkid
                                    objMemTran._MemTranTable = dtMemTran
                                    'objMemTran._ClientIP = mstrWebClientIP
                                    'objMemTran._WebFormName = mstrWebFormName
                                    'objMemTran._HostName = mstrWebHostName

                                    objMemTran._ClientIP = mstrClientIP
                                    objMemTran._FormName = mstrFormName
                                    objMemTran._HostName = mstrHostName
                                    objMemTran._AuditDate = mdtAuditDate
                                    objMemTran._AuditUserId = mintAuditUserId
objMemTran._CompanyUnkid = mintCompanyUnkid
                                    objMemTran._FromWeb = mblnIsWeb
                                    objMemTran._LoginEmployeeunkid = mintLoginEmployeeunkid

                                    objMemTran._DisciplineFileTranunkid = CInt(.Item("disciplinefiletranunkid"))

                                    'Sohail (30 Nov 2017) -- Start
                                    'SUMATRA Enhancement – SUMATRA – issue # 0001669: Additional feature for Exchange Server configuration setting in 70.1.
                                    'If objMemTran.InsertUpdateDeleteMembersTran(objDataOperation, mintUserunkid, dtCurrentDateTime, _
                                    '                                            blnIsFromOpenCharge_ApproveDisApprove_Screen) = False Then
                                    If objMemTran.InsertUpdateDeleteMembersTran(objDataOperation, mintUserunkid, dtCurrentDateTime, _
                                                                                blnIsFromOpenCharge_ApproveDisApprove_Screen, intCompanyUnkId, mdtScanDocs) = False Then
                                        'Hemant (22 Jan 2021) -- [mdtScanDocs]
                                        'Sohail (30 Nov 2017) -- End
                                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                        Throw exForce
                                    End If

                                End If

                                'S.SANDEEP |11-NOV-2019| -- START
                                'ISSUE/ENHANCEMENT : NMB DISCIPLINARY UAT COMMENTS
                                If mdtScanDocs IsNot Nothing AndAlso mdtScanDocs.Rows.Count > 0 Then
                                    Dim dtTab As DataTable = Nothing
                                    Dim row As DataRow() = mdtScanDocs.Select("transactionunkid = '" & .Item("disciplineproceedingmasterunkid").ToString & "' AND form_name = 'frmDisciplineProceedingList' AND AUD = 'D'")
                                    If row.Length > 0 Then
                                        dtTab = row.CopyToDataTable()
                                        objDocument._Datatable = dtTab
                                        objDocument.InsertUpdateDelete_Documents(objDataOperation)
                                        If objDataOperation.ErrorMessage <> "" Then
                                            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                            Throw exForce
                                        End If
                                    End If
                                End If
                                'S.SANDEEP |11-NOV-2019| -- END

                        End Select
                    End If
                End With
            Next

            objDataOperation.ReleaseTransaction(True)

            If blnIsFromOpenCharge_ApproveDisApprove_Screen = True Then
                'Sohail (30 Nov 2017) -- Start
                'SUMATRA Enhancement – SUMATRA – issue # 0001669: Additional feature for Exchange Server configuration setting in 70.1.
                'Call SendMailToEmployee(mintDisciplineFileunkid)
                Call SendMailToEmployee(mintDisciplineFileunkid, intCompanyUnkId)
                'Sohail (30 Nov 2017) -- End
            End If

        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: InsertUpdateDeleteProceeding; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
        End Try
        Return True
    End Function

    Private Function InsertATProceedingMaster(ByVal objDataOperation As clsDataOperation, ByVal intUserId As Integer, ByVal dtCurrentDate As DateTime, _
                                              ByVal dtRow As DataRow, ByVal eAudit As enAuditType) As Boolean
        Dim StrQ As String = ""
        Dim exForce As Exception = Nothing
        Try
            With dtRow
                StrQ = "INSERT INTO athrdiscipline_proceeding_master ( " & _
                                "   disciplineproceedingmasterunkid " & _
                                ",  disciplinefiletranunkid " & _
                                ",  disciplinefileunkid " & _
                                ",  proceeding_date " & _
                                ",  proceeding_details " & _
                                ",  remarks_comments " & _
                                ",  person_involved_comments " & _
                                ",  actionreasonunkid " & _
                                ",  count_status " & _
                                ",  audittype " & _
                                ",  audituserunkid " & _
                                ",  auditdatetime " & _
                                ",  ip " & _
                                ",  machine_name " & _
                                ",  form_name " & _
                                ",  isweb" & _
                                ",  loginemployeeunkid " & _
                                ",  issubmitforapproval " & _
                                ",  statusdate " & _
                                ",  remark " & _
                                ",  isexternal " & _
                                ",  isapproved " & _
                                ",  approval_date " & _
                                ",  approval_remark " & _
                                ",  penalty_effectivedate " & _
                                ",  penalty_expirydate " & _
                            ") VALUES (" & _
                                "   @disciplineproceedingmasterunkid " & _
                                ",  @disciplinefiletranunkid " & _
                                ",  @disciplinefileunkid " & _
                                ",  @proceeding_date " & _
                                ",  @proceeding_details " & _
                                ",  @remarks_comments " & _
                                ",  @person_involved_comments " & _
                                ",  @actionreasonunkid " & _
                                ",  @count_status " & _
                                ",  @audittype " & _
                                ",  @audituserunkid " & _
                                ",  @auditdatetime " & _
                                ",  @ip " & _
                                ",  @machine_name " & _
                                ",  @form_name " & _
                                ",  @isweb" & _
                                ",  @loginemployeeunkid " & _
                                ",  @issubmitforapproval " & _
                                ",  @statusdate " & _
                                ",  @remark " & _
                                ",  @isexternal " & _
                                ",  @isapproved " & _
                                ",  @approval_date " & _
                                ",  @approval_remark " & _
                                ",  @penalty_effectivedate " & _
                                ",  @penalty_expirydate " & _
                            ")  "

                objDataOperation.ClearParameters()
                objDataOperation.AddParameter("@disciplineproceedingmasterunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, IIf(mintDisciplineProceedingMasterunkid <= 0, .Item("disciplineproceedingmasterunkid"), mintDisciplineProceedingMasterunkid))
                objDataOperation.AddParameter("@disciplinefiletranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("disciplinefiletranunkid"))
                objDataOperation.AddParameter("@disciplinefileunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("disciplinefileunkid"))
                If .Item("proceeding_date").ToString <> Nothing Then
                    objDataOperation.AddParameter("@proceeding_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, .Item("proceeding_date"))
                Else
                    objDataOperation.AddParameter("@proceeding_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
                End If
                objDataOperation.AddParameter("@proceeding_details", SqlDbType.Text, .Item("proceeding_details").ToString.Length, .Item("proceeding_details"))
                objDataOperation.AddParameter("@remarks_comments", SqlDbType.Text, .Item("remarks_comments").ToString.Length, .Item("remarks_comments"))
                objDataOperation.AddParameter("@person_involved_comments", SqlDbType.Text, .Item("person_involved_comments").ToString.Length, .Item("person_involved_comments"))
                objDataOperation.AddParameter("@actionreasonunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("actionreasonunkid"))
                objDataOperation.AddParameter("@count_status", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("count_status"))

                objDataOperation.AddParameter("@issubmitforapproval", SqlDbType.Bit, eZeeDataType.BIT_SIZE, .Item("issubmitforapproval"))
                objDataOperation.AddParameter("@statusdate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, .Item("statusdate"))
                objDataOperation.AddParameter("@remark", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, .Item("remark"))
                objDataOperation.AddParameter("@isexternal", SqlDbType.Bit, eZeeDataType.BIT_SIZE, .Item("isexternal"))

                objDataOperation.AddParameter("@isapproved", SqlDbType.Bit, eZeeDataType.BIT_SIZE, .Item("isapproved"))
                If .Item("approval_date").ToString <> Nothing Then
                    objDataOperation.AddParameter("@approval_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, .Item("approval_date"))
                Else
                    objDataOperation.AddParameter("@approval_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
                End If
                objDataOperation.AddParameter("@approval_remark", SqlDbType.NVarChar, .Item("approval_remark").ToString.Length, .Item("approval_remark"))

                'Pinkal (19-Dec-2020) -- Start
                'Enhancement  -  Working on Discipline module for NMB.
                If .Item("penalty_effectivedate").ToString <> Nothing Then
                    objDataOperation.AddParameter("@penalty_effectivedate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, .Item("penalty_effectivedate"))
                Else
                    objDataOperation.AddParameter("@penalty_effectivedate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
                End If

                If .Item("penalty_expirydate").ToString <> Nothing Then
                    objDataOperation.AddParameter("@penalty_expirydate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, .Item("penalty_expirydate"))
                Else
                    objDataOperation.AddParameter("@penalty_expirydate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
                End If
                'Pinkal (19-Dec-2020) -- End


            End With
            objDataOperation.AddParameter("@audittype", SqlDbType.Int, eZeeDataType.INT_SIZE, eAudit)
            objDataOperation.AddParameter("@audituserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAuditUserId)
            objDataOperation.AddParameter("@auditdatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtAuditDate)
            objDataOperation.AddParameter("@ip", SqlDbType.NVarChar, eZeeDataType.IP_SIZE, mstrClientIP)
            objDataOperation.AddParameter("@machine_name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrHostName)
            objDataOperation.AddParameter("@form_name", SqlDbType.NVarChar, 500, mstrFormName)
            objDataOperation.AddParameter("@isweb", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsWeb)
            objDataOperation.AddParameter("@loginemployeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLoginEmployeeunkid.ToString)

            objDataOperation.ExecNonQuery(StrQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: InsertATProceedingMaster; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
        End Try
        Return True
    End Function

    'S.SANDEEP |01-OCT-2019| -- START
    'ISSUE/ENHANCEMENT : NMB DISCIPLINARY UAT COMMENTS
    'Public Function ProceedingApprove_Disapprove(ByVal intProceedingMasterUnkid As Integer, _
    '                                             Optional ByVal strListName As String = "List") As DataTable
    '    Dim dsList As New DataSet
    '    Dim dtTable As DataTable = Nothing
    '    Dim objDataOperation As New clsDataOperation
    '    Dim exForce As Exception
    '    Dim StrQ As String = String.Empty
    '    Try
    '        If strListName.Trim.Length <= 0 Then strListName = "List"
    '        dtTable = New DataTable(strListName)
    '        dtTable.Columns.Add("Particulars", GetType(String))
    '        dtTable.Columns.Add("Details", GetType(String))

    '        StrQ = "SELECT " & _
    '               "     @Incident+'|'+DFT.descr AS Row1 " & _
    '               "    ,@OffenceCategory+'|'+cfcommon_master.name AS Row2 " & _
    '               "    ,@OffenceDescription+'|'+hrdisciplinetype_master.name AS Row3 " & _
    '               "    ,@Severity+'|'+CAST(hrdisciplinetype_master.severity AS NVARCHAR(MAX)) AS Row4 " & _
    '               "    ,@Committee+'|'+ ISNULL(MCat.name,'') AS Row5 " & _
    '               "    ,@Member+'|'+Member.investigator AS Row6 " & _
    '               "    ,@Action+'|' + hraction_reason_master.reason_action AS Row7 " & _
    '               "    ,@CountStatus + '|' + CASE WHEN DPM.count_status = '" & enProceedingCountStatus.Open & "' THEN @Open WHEN DPM.count_status = '" & enProceedingCountStatus.Close & "' THEN @Close END AS Row8 " & _
    '               "    ,@ProceedingDetails+'|'+ DPM.proceeding_details AS Row9 " & _
    '               "    ,@RemarksComments+'|'+DPM.remarks_comments AS Row10 " & _
    '               "    ,@PersonInvolvedComments+'|'+DPM.person_involved_comments AS Row11 " & _
    '               "FROM hrdiscipline_proceeding_master AS DPM " & _
    '               "JOIN " & _
    '               "( " & _
    '               "    SELECT " & _
    '               "         dft.disciplinefiletranunkid " & _
    '               "        ,CAST(ROW_NUMBER()OVER(PARTITION BY dft.disciplinefileunkid ORDER BY dft.disciplinefiletranunkid ASC) AS NVARCHAR(MAX))+' - '+ dft.incident_description AS descr " & _
    '               "        ,dft.offenceunkid " & _
    '               "    FROM hrdiscipline_file_tran AS dft " & _
    '               "    WHERE dft.isvoid = 0 " & _
    '               ") AS DFT ON DFT.disciplinefiletranunkid = DPM.disciplinefiletranunkid " & _
    '               "JOIN hrdisciplinetype_master ON dft.offenceunkid = hrdisciplinetype_master.disciplinetypeunkid " & _
    '               "JOIN cfcommon_master ON hrdisciplinetype_master.offencecategoryunkid = cfcommon_master.masterunkid " & _
    '               "JOIN hraction_reason_master ON DPM.actionreasonunkid = hraction_reason_master.actionreasonunkid AND hraction_reason_master.isreason = 0 " & _
    '               "LEFT JOIN " & _
    '               " ( " & _
    '               "               SELECT DISTINCT " & _
    '               "                   A.disciplineproceedingmasterunkid " & _
    '               "              ,hrdiscipline_committee.committeemasterunkid " & _
    '               "                   ,( SELECT " & _
    '               "                      STUFF( " & _
    '               "                              ( SELECT " & _
    '               "                                    ', ' + INVT.Name " & _
    '               "                                FROM " & _
    '               "                                ( " & _
    '               "                                    SELECT " & _
    '               "                                        CASE WHEN hrdiscipline_committee.employeeunkid <= 0 THEN hrdiscipline_committee.ex_name " & _
    '               "                                             WHEN hrdiscipline_committee.employeeunkid > 0 THEN ISNULL(hremployee_master.firstname, '') + ' ' + ISNULL(hremployee_master.othername, '') + ' ' + ISNULL(hremployee_master.surname, '') END AS Name " & _
    '               "                                    FROM hrdiscipline_members_tran " & _
    '               "                                        JOIN hrdiscipline_proceeding_master ON hrdiscipline_proceeding_master.disciplineproceedingmasterunkid = hrdiscipline_members_tran.disciplineproceedingmasterunkid " & _
    '               "                                            AND A.disciplineproceedingmasterunkid = @disciplineproceedingmasterunkid " & _
    '               "                                        LEFT JOIN hrdiscipline_committee ON hrdiscipline_committee.committeetranunkid = hrdiscipline_members_tran.committeetranunkid " & _
    '               "                                        LEFT JOIN hremployee_master ON hremployee_master.employeeunkid = hrdiscipline_committee.employeeunkid " & _
    '               "                                    WHERE hrdiscipline_members_tran.isvoid = 0 AND hrdiscipline_members_tran.committeetranunkid > 0 " & _
    '               "                                        AND A.disciplineproceedingmasterunkid = hrdiscipline_proceeding_master.disciplineproceedingmasterunkid " & _
    '               "                                    UNION " & _
    '               "                                    SELECT " & _
    '               "                                        CASE WHEN hrdiscipline_members_tran.employeeunkid <= 0 THEN hrdiscipline_members_tran.ex_name " & _
    '               "                                             WHEN hrdiscipline_members_tran.employeeunkid > 0 THEN ISNULL(hremployee_master.firstname, '') + ' ' + ISNULL(hremployee_master.othername, '') + ' ' + ISNULL(hremployee_master.surname, '') END AS Name " & _
    '               "                                    FROM hrdiscipline_members_tran " & _
    '               "                                        JOIN hrdiscipline_proceeding_master ON hrdiscipline_proceeding_master.disciplineproceedingmasterunkid = hrdiscipline_members_tran.disciplineproceedingmasterunkid " & _
    '               "                                            AND A.disciplineproceedingmasterunkid = @disciplineproceedingmasterunkid " & _
    '               "                                        LEFT JOIN hremployee_master ON hremployee_master.employeeunkid = hrdiscipline_members_tran.employeeunkid " & _
    '               "                                    WHERE hrdiscipline_members_tran.isvoid = 0 AND hrdiscipline_members_tran.committeetranunkid <= 0 " & _
    '               "                                        AND A.disciplineproceedingmasterunkid = hrdiscipline_proceeding_master.disciplineproceedingmasterunkid " & _
    '               "                                ) AS INVT " & _
    '               "                    FOR XML PATH ('')), 1, 1, '') " & _
    '               "               ) AS investigator " & _
    '               "               FROM hrdiscipline_members_tran AS A " & _
    '               "              JOIN hrdiscipline_proceeding_master ON hrdiscipline_proceeding_master.disciplineproceedingmasterunkid = A.disciplineproceedingmasterunkid " & _
    '               "                  AND A.disciplineproceedingmasterunkid = @disciplineproceedingmasterunkid " & _
    '               "              LEFT JOIN hrdiscipline_committee ON hrdiscipline_committee.committeetranunkid = A.committeetranunkid AND ISNULL(hrdiscipline_committee.committeemasterunkid,0) > 0 " & _
    '               "       Where A.isvoid = 0 " & _
    '               " ) AS Member ON Member.disciplineproceedingmasterunkid = DPM.disciplineproceedingmasterunkid " & _
    '               "LEFT JOIN cfcommon_master AS MCat ON MCat.masterunkid = Member.committeemasterunkid " & _
    '               "WHERE DPM.disciplineproceedingmasterunkid = @disciplineproceedingmasterunkid " & _
    '               "AND DPM.isvoid = 0 "

    '        objDataOperation.AddParameter("@disciplineproceedingmasterunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intProceedingMasterUnkid)
    '        objDataOperation.AddParameter("@Incident", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 8, "Incident"))
    '        objDataOperation.AddParameter("@OffenceCategory", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 9, "Offence Category"))
    '        objDataOperation.AddParameter("@OffenceDescription", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 10, "Offence Description"))
    '        objDataOperation.AddParameter("@Severity", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 11, "Severity"))
    '        objDataOperation.AddParameter("@Committee", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 12, "Committee"))
    '        objDataOperation.AddParameter("@Member", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 13, "Member(s)"))
    '        objDataOperation.AddParameter("@Action", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 14, "Disciplinary Action"))
    '        objDataOperation.AddParameter("@CountStatus", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 15, "Count Status"))
    '        objDataOperation.AddParameter("@ProceedingDetails", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 16, "Proceeding Details"))
    '        objDataOperation.AddParameter("@RemarksComments", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 17, "Remarks Comments"))
    '        objDataOperation.AddParameter("@PersonInvolvedComments", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 18, "Person Involved Comments"))

    '        objDataOperation.AddParameter("@Open", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 6, "Open"))
    '        objDataOperation.AddParameter("@Close", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 7, "Close"))

    '        dsList = objDataOperation.ExecQuery(StrQ, "List")

    '        If objDataOperation.ErrorMessage <> "" Then
    '            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
    '            Throw exForce
    '        End If


    '        If dsList.Tables("List").Rows.Count > 0 Then
    '            Dim iColCount As Integer = dsList.Tables("List").Columns.Count
    '            For iColIdx As Integer = 1 To iColCount
    '                'S.SANDEEP |01-OCT-2019| -- START
    '                'ISSUE/ENHANCEMENT : NMB DISCIPLINARY UAT COMMENTS
    '                If dsList.Tables("List").Columns(iColIdx).ColumnName = "Row" & iColIdx.ToString Then
    '                    Continue For
    '                End If
    '                'S.SANDEEP |01-OCT-2019| -- END
    '                Dim dRow As DataRow = dtTable.NewRow()
    '                If IsDBNull(dsList.Tables("List").Rows(0).Item("Row" & iColIdx.ToString)) = False Then
    '                    dRow.Item("Particulars") = dsList.Tables("List").Rows(0).Item("Row" & iColIdx.ToString).ToString.Split("|")(0)
    '                    dRow.Item("Details") = dsList.Tables("List").Rows(0).Item("Row" & iColIdx.ToString).ToString.Split("|")(1)
    '                    dtTable.Rows.Add(dRow)
    '                End If
    '            Next
    '        End If

    '    Catch ex As Exception
    '        Throw New Exception(ex.Message & "; Procedure Name: ProceedingApprove_Disapprove; Module Name: " & mstrModuleName)
    '    Finally
    '        objDataOperation = Nothing : exForce = Nothing
    '    End Try
    '    Return dtTable
    'End Function

    Public Function ProceedingApprove_Disapprove(ByVal intProceedingMasterUnkid As Integer, _
                                                 Optional ByVal strListName As String = "List", _
                                                 Optional ByVal blnAddProceedingAgainstEachCount As Boolean = True) As DataTable
        Dim dsList As New DataSet
        Dim dtTable As DataTable = Nothing
        Dim objDataOperation As New clsDataOperation
        Dim exForce As Exception
        Dim StrQ As String = String.Empty
        Try
            If strListName.Trim.Length <= 0 Then strListName = "List"
            dtTable = New DataTable(strListName)
            dtTable.Columns.Add("Particulars", GetType(String))
            dtTable.Columns.Add("Details", GetType(String))

            If blnAddProceedingAgainstEachCount Then
            StrQ = "SELECT " & _
                   "     @Incident+'|'+DFT.descr AS Row1 " & _
                   "    ,@OffenceCategory+'|'+cfcommon_master.name AS Row2 " & _
                   "    ,@OffenceDescription+'|'+hrdisciplinetype_master.name AS Row3 " & _
                   "    ,@Severity+'|'+CAST(hrdisciplinetype_master.severity AS NVARCHAR(MAX)) AS Row4 " & _
                   "    ,@Committee+'|'+ ISNULL(MCat.name,'') AS Row5 " & _
                   "    ,@Member+'|'+Member.investigator AS Row6 " & _
                   "    ,@Action+'|' + hraction_reason_master.reason_action AS Row7 " & _
                   "    ,@CountStatus + '|' + CASE WHEN DPM.count_status = '" & enProceedingCountStatus.Open & "' THEN @Open WHEN DPM.count_status = '" & enProceedingCountStatus.Close & "' THEN @Close END AS Row8 " & _
                   "    ,@ProceedingDetails+'|'+ DPM.proceeding_details AS Row9 " & _
                   "    ,@RemarksComments+'|'+DPM.remarks_comments AS Row10 " & _
                   "    ,@PersonInvolvedComments+'|'+DPM.person_involved_comments AS Row11 " & _
                       "FROM hrdiscipline_proceeding_master AS DPM "
            Else
                StrQ = "SELECT " & _
                   "     @Incident+'|'+Row1 AS Row1 " & _
                   "    ,@OffenceCategory+'|'+Row2 AS Row2 " & _
                   "    ,@OffenceDescription+'|'+Row3 AS Row3 " & _
                   "    ,@Severity+'|'+Row4 AS Row4 " & _
                   "    ,@Committee+'|'+ ISNULL(MCat.name,'') AS Row5 " & _
                   "    ,@Member+'|'+Member.investigator AS Row6 " & _
                   "    ,@Action+'|' + hraction_reason_master.reason_action AS Row7 " & _
                   "    ,@CountStatus + '|' + CASE WHEN DPM.count_status = '" & enProceedingCountStatus.Open & "' THEN @Open WHEN DPM.count_status = '" & enProceedingCountStatus.Close & "' THEN @Close END AS Row8 " & _
                   "    ,@ProceedingDetails+'|'+ DPM.proceeding_details AS Row9 " & _
                   "    ,@RemarksComments+'|'+DPM.remarks_comments AS Row10 " & _
                   "    ,@PersonInvolvedComments+'|'+DPM.person_involved_comments AS Row11 " & _
                   "FROM hrdiscipline_proceeding_master AS DPM "
            End If
            

            If blnAddProceedingAgainstEachCount Then
                StrQ &= "JOIN " & _
                   "( " & _
                   "    SELECT " & _
                   "         dft.disciplinefiletranunkid " & _
                   "        ,CAST(ROW_NUMBER()OVER(PARTITION BY dft.disciplinefileunkid ORDER BY dft.disciplinefiletranunkid ASC) AS NVARCHAR(MAX))+' - '+ dft.incident_description AS descr " & _
                   "        ,dft.offenceunkid " & _
                   "    FROM hrdiscipline_file_tran AS dft " & _
                   "    WHERE dft.isvoid = 0 " & _
                   ") AS DFT ON DFT.disciplinefiletranunkid = DPM.disciplinefiletranunkid " & _
                   "JOIN hrdisciplinetype_master ON dft.offenceunkid = hrdisciplinetype_master.disciplinetypeunkid " & _
                        "JOIN cfcommon_master ON hrdisciplinetype_master.offencecategoryunkid = cfcommon_master.masterunkid "

            Else
                StrQ &= "LEFT JOIN " & _
                        "( " & _
                        "SELECT " & _
                        "    dfta.disciplinefileunkid " & _
                        "   ,STUFF((SELECT '~' + CAST(ROW_NUMBER() OVER (PARTITION BY dft.disciplinefileunkid ORDER BY dft.disciplinefiletranunkid ASC) AS NVARCHAR(MAX)) + ' - ' + dft.incident_description " & _
                        "           FROM hrdiscipline_file_tran AS dft " & _
                        "           WHERE dfta.disciplinefileunkid = dft.disciplinefileunkid " & _
                        "           AND dft.isvoid = 0 " & _
                        "           FOR XML PATH ('')), 1, 1, '') AS Row1 " & _
                        "   ,STUFF((SELECT '~' + cfcommon_master.name " & _
                        "           FROM hrdiscipline_file_tran AS dft " & _
                        "           JOIN hrdisciplinetype_master ON dft.offenceunkid = hrdisciplinetype_master.disciplinetypeunkid " & _
                        "           JOIN cfcommon_master ON hrdisciplinetype_master.offencecategoryunkid = cfcommon_master.masterunkid " & _
                        "           WHERE dfta.disciplinefileunkid = dft.disciplinefileunkid " & _
                        "           AND dft.isvoid = 0 " & _
                        "           FOR XML PATH ('')), 1, 1, '') AS Row2 " & _
                        "   ,STUFF((SELECT '~' + hrdisciplinetype_master.name " & _
                        "           FROM hrdiscipline_file_tran AS dft " & _
                        "           JOIN hrdisciplinetype_master ON dft.offenceunkid = hrdisciplinetype_master.disciplinetypeunkid " & _
                        "           WHERE dfta.disciplinefileunkid = dft.disciplinefileunkid " & _
                        "           AND dft.isvoid = 0 " & _
                        "           FOR XML PATH ('')), 1, 1, '') AS Row3 " & _
                        "   ,STUFF((SELECT '~' + CAST(hrdisciplinetype_master.severity AS NVARCHAR(MAX)) " & _
                        "           FROM hrdiscipline_file_tran AS dft " & _
                        "           JOIN hrdisciplinetype_master ON dft.offenceunkid = hrdisciplinetype_master.disciplinetypeunkid " & _
                        "           WHERE dfta.disciplinefileunkid = dft.disciplinefileunkid " & _
                        "           AND dft.isvoid = 0 " & _
                        "           FOR XML PATH ('')), 1, 1, '') AS Row4 " & _
                        "FROM hrdiscipline_file_tran dfta " & _
                        "WHERE dfta.isvoid = 0 " & _
                        "GROUP BY dfta.disciplinefileunkid " & _
                        ") AS DFT ON DFT.disciplinefileunkid = DPM.disciplinefileunkid "
            End If

            

            StrQ &= "JOIN hraction_reason_master ON DPM.actionreasonunkid = hraction_reason_master.actionreasonunkid AND hraction_reason_master.isreason = 0 " & _
                   "LEFT JOIN " & _
                   " ( " & _
                   "               SELECT DISTINCT " & _
                   "                   A.disciplineproceedingmasterunkid " & _
                   "              ,hrdiscipline_committee.committeemasterunkid " & _
                   "                   ,( SELECT " & _
                   "                      STUFF( " & _
                   "                              ( SELECT " & _
                   "                                    ', ' + INVT.Name " & _
                   "                                FROM " & _
                   "                                ( " & _
                   "                                    SELECT " & _
                   "                                        CASE WHEN hrdiscipline_committee.employeeunkid <= 0 THEN hrdiscipline_committee.ex_name " & _
                   "                                             WHEN hrdiscipline_committee.employeeunkid > 0 THEN ISNULL(hremployee_master.firstname, '') + ' ' + ISNULL(hremployee_master.othername, '') + ' ' + ISNULL(hremployee_master.surname, '') END AS Name " & _
                   "                                    FROM hrdiscipline_members_tran " & _
                   "                                        JOIN hrdiscipline_proceeding_master ON hrdiscipline_proceeding_master.disciplineproceedingmasterunkid = hrdiscipline_members_tran.disciplineproceedingmasterunkid " & _
                   "                                            AND A.disciplineproceedingmasterunkid = @disciplineproceedingmasterunkid " & _
                   "                                        LEFT JOIN hrdiscipline_committee ON hrdiscipline_committee.committeetranunkid = hrdiscipline_members_tran.committeetranunkid " & _
                   "                                        LEFT JOIN hremployee_master ON hremployee_master.employeeunkid = hrdiscipline_committee.employeeunkid " & _
                   "                                    WHERE hrdiscipline_members_tran.isvoid = 0 AND hrdiscipline_members_tran.committeetranunkid > 0 " & _
                   "                                        AND A.disciplineproceedingmasterunkid = hrdiscipline_proceeding_master.disciplineproceedingmasterunkid " & _
                   "                                    UNION " & _
                   "                                    SELECT " & _
                   "                                        CASE WHEN hrdiscipline_members_tran.employeeunkid <= 0 THEN hrdiscipline_members_tran.ex_name " & _
                   "                                             WHEN hrdiscipline_members_tran.employeeunkid > 0 THEN ISNULL(hremployee_master.firstname, '') + ' ' + ISNULL(hremployee_master.othername, '') + ' ' + ISNULL(hremployee_master.surname, '') END AS Name " & _
                   "                                    FROM hrdiscipline_members_tran " & _
                   "                                        JOIN hrdiscipline_proceeding_master ON hrdiscipline_proceeding_master.disciplineproceedingmasterunkid = hrdiscipline_members_tran.disciplineproceedingmasterunkid " & _
                   "                                            AND A.disciplineproceedingmasterunkid = @disciplineproceedingmasterunkid " & _
                   "                                        LEFT JOIN hremployee_master ON hremployee_master.employeeunkid = hrdiscipline_members_tran.employeeunkid " & _
                   "                                    WHERE hrdiscipline_members_tran.isvoid = 0 AND hrdiscipline_members_tran.committeetranunkid <= 0 " & _
                   "                                        AND A.disciplineproceedingmasterunkid = hrdiscipline_proceeding_master.disciplineproceedingmasterunkid " & _
                   "                                ) AS INVT " & _
                   "                    FOR XML PATH ('')), 1, 1, '') " & _
                   "               ) AS investigator " & _
                   "               FROM hrdiscipline_members_tran AS A " & _
                   "              JOIN hrdiscipline_proceeding_master ON hrdiscipline_proceeding_master.disciplineproceedingmasterunkid = A.disciplineproceedingmasterunkid " & _
                   "                  AND A.disciplineproceedingmasterunkid = @disciplineproceedingmasterunkid " & _
                   "              LEFT JOIN hrdiscipline_committee ON hrdiscipline_committee.committeetranunkid = A.committeetranunkid AND ISNULL(hrdiscipline_committee.committeemasterunkid,0) > 0 " & _
                   "       Where A.isvoid = 0 " & _
                   " ) AS Member ON Member.disciplineproceedingmasterunkid = DPM.disciplineproceedingmasterunkid " & _
                   "LEFT JOIN cfcommon_master AS MCat ON MCat.masterunkid = Member.committeemasterunkid " & _
                   "WHERE DPM.disciplineproceedingmasterunkid = @disciplineproceedingmasterunkid " & _
                   "AND DPM.isvoid = 0 "

            objDataOperation.AddParameter("@disciplineproceedingmasterunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intProceedingMasterUnkid)
            objDataOperation.AddParameter("@Incident", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 8, "Incident"))
            objDataOperation.AddParameter("@OffenceCategory", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 9, "Offence Category"))
            objDataOperation.AddParameter("@OffenceDescription", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 10, "Offence Description"))
            objDataOperation.AddParameter("@Severity", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 11, "Severity"))
            objDataOperation.AddParameter("@Committee", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 12, "Committee"))
            objDataOperation.AddParameter("@Member", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 13, "Member(s)"))
            objDataOperation.AddParameter("@Action", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 14, "Disciplinary Action"))
            objDataOperation.AddParameter("@CountStatus", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 15, "Count Status"))
            objDataOperation.AddParameter("@ProceedingDetails", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 16, "Proceeding Details"))
            objDataOperation.AddParameter("@RemarksComments", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 17, "Remarks Comments"))
            objDataOperation.AddParameter("@PersonInvolvedComments", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 18, "Person Involved Comments"))

            objDataOperation.AddParameter("@Open", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 6, "Open"))
            objDataOperation.AddParameter("@Close", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 7, "Close"))

            dsList = objDataOperation.ExecQuery(StrQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If


            If dsList.Tables("List").Rows.Count > 0 Then
                Dim iColCount As Integer = dsList.Tables("List").Columns.Count
                For iColIdx As Integer = 1 To iColCount
                    If dsList.Tables("List").Columns(iColIdx - 1).ColumnName = "Row4".ToString Then
                        Continue For
                    End If
                    Dim dRow As DataRow = dtTable.NewRow()
                    If IsDBNull(dsList.Tables("List").Rows(0).Item("Row" & iColIdx.ToString)) = False Then
                        dRow.Item("Particulars") = dsList.Tables("List").Rows(0).Item("Row" & iColIdx.ToString).ToString.Split("|")(0)
                        If blnAddProceedingAgainstEachCount Then
                        dRow.Item("Details") = dsList.Tables("List").Rows(0).Item("Row" & iColIdx.ToString).ToString.Split("|")(1)
                        Else
                            dRow.Item("Details") = dsList.Tables("List").Rows(0).Item("Row" & iColIdx.ToString).ToString.Split("|")(1).Replace("~", vbCrLf)
                        End If
                        dtTable.Rows.Add(dRow)
                    End If
                Next
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: ProceedingApprove_Disapprove; Module Name: " & mstrModuleName)
        Finally
            objDataOperation = Nothing : exForce = Nothing
        End Try
        Return dtTable
    End Function
    'S.SANDEEP |01-OCT-2019| -- END

    

    ''' <summary>
    ''' Check for Posting of Counts
    ''' </summary>
    ''' <param name="intDisciplineFileTranId">The Count Id</param>
    ''' <param name="intCheckType">0 = FOR PROCEEDING DATE, 1 = ALLOW POSTING FOR SAME COUNT</param>
    ''' <returns>String</returns>
    ''' <remarks></remarks>

    Public Function IsPostingAllowed(ByVal intDisciplineFileTranId As Integer, ByVal intCheckType As Integer, ByVal dtpProceedingDate As Date, ByVal blnAddProceedingAgainstEachCount As Boolean) As String
        'S.SANDEEP |01-OCT-2019| -- START
        'ISSUE/ENHANCEMENT : NMB DISCIPLINARY UAT COMMENTS
        'Public Function IsPostingAllowed(ByVal intDisciplineFileTranId As Integer, ByVal intCheckType As Integer, ByVal dtpProceedingDate As Date) As String
        'S.SANDEEP |01-OCT-2019| -- END

        Dim StrQ As String = String.Empty
        Dim StrMsg As String = String.Empty
        Dim dsList As New DataSet
        Dim objDataOperation As New clsDataOperation
        Dim exForce As Exception
        Try
            'S.SANDEEP |01-OCT-2019| -- START
            'ISSUE/ENHANCEMENT : NMB DISCIPLINARY UAT COMMENTS
            'Select Case intCheckType
            '    Case 0  'FOR PROCEEDING DATE
            '        StrQ = "SELECT " & _
            '               "    CONVERT(CHAR(8),DPM.proceeding_date,112) AS pDate " & _
            '               "FROM hrdiscipline_proceeding_master AS DPM " & _
            '               "WHERE DPM.isvoid = 0 AND DPM.disciplinefiletranunkid = @DisciplineFileTranId " & _
            '               " ORDER BY disciplineproceedingmasterunkid DESC "
            '    Case 1  'ALLOW POSTING FOR SAME COUNT
            '        StrQ = "SELECT " & _
            '               "     DPM.isapproved " & _
            '               "    ,CONVERT(CHAR(8),DPM.proceeding_date,112) AS pDate " & _
            '               "FROM hrdiscipline_proceeding_master AS DPM " & _
            '               "WHERE DPM.isvoid = 0 AND DPM.disciplinefiletranunkid = @DisciplineFileTranId AND DPM.count_status = 1 "
            'End Select
            'objDataOperation.AddParameter("@DisciplineFileTranId", SqlDbType.Int, eZeeDataType.INT_SIZE, intDisciplineFileTranId)
            Select Case intCheckType
                Case 0  'FOR PROCEEDING DATE
                    StrQ = "SELECT " & _
                           "    CONVERT(CHAR(8),DPM.proceeding_date,112) AS pDate " & _
                           "FROM hrdiscipline_proceeding_master AS DPM " & _
                           "WHERE DPM.isvoid = 0 AND #COND# " & _
                           " ORDER BY disciplineproceedingmasterunkid DESC "
                Case 1  'ALLOW POSTING FOR SAME COUNT
                    StrQ = "SELECT " & _
                           "     DPM.isapproved " & _
                           "    ,CONVERT(CHAR(8),DPM.proceeding_date,112) AS pDate " & _
                           "FROM hrdiscipline_proceeding_master AS DPM " & _
                           "WHERE DPM.isvoid = 0 AND #COND# AND DPM.count_status = 1 "
            End Select
            If blnAddProceedingAgainstEachCount Then
                StrQ = StrQ.Replace("#COND#", "DPM.disciplinefiletranunkid = @DisciplineFileTranId")
                objDataOperation.AddParameter("@DisciplineFileTranId", SqlDbType.Int, eZeeDataType.INT_SIZE, intDisciplineFileTranId)
            Else

                StrQ = StrQ.Replace("#COND#", "DPM.disciplinefileunkid = @disciplinefileunkid")
                objDataOperation.AddParameter("@disciplinefileunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intDisciplineFileTranId)
            End If
            'S.SANDEEP |01-OCT-2019| -- END

            dsList = objDataOperation.ExecQuery(StrQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If dsList.Tables(0).Rows.Count > 0 Then
                Select Case intCheckType
                    Case 0  'FOR PROCEEDING DATE
                        If eZeeDate.convertDate(dtpProceedingDate) <= dsList.Tables(0).Rows(0)("pDate") Then
                            StrMsg = Language.getMessage(mstrModuleName, 19, "Sorry, Proceeding date must be greater than the last proceeding date which is [") & eZeeDate.convertDate(dsList.Tables(0).Rows(0)("pDate").ToString).Date & _
                                     Language.getMessage(mstrModuleName, 20, "]. Please select a valid date to continue.")
                        End If
                    Case 1  'ALLOW POSTING FOR SAME COUNT
                        If CBool(dsList.Tables(0).Rows(0)("isapproved")) = False Then
                            StrMsg = Language.getMessage(mstrModuleName, 21, "Sorry, You cannot add another proceeding details for the selected count. Reason : The last proceeding is not yet approved.")
                        End If
                End Select
            End If
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: IsPostingAllowed; Module Name: " & mstrModuleName)
        Finally
            objDataOperation = Nothing
        End Try
        Return StrMsg
    End Function

    Private Function IsCloseCharge(ByVal intDisciplineFileUnkid As Integer, ByVal objDataOpr As clsDataOperation) As Boolean
        Dim StrQ As String = String.Empty
        Dim exForce As Exception
        Dim blnFlag As Boolean = False
        Try
            Dim intTotalCount, intClosedCount As Integer
            intTotalCount = 0 : intClosedCount = 0

            objDataOpr.ClearParameters()
            objDataOpr.AddParameter("@disciplinefileunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intDisciplineFileUnkid)

            StrQ = "SELECT " & _
                   "  @TC = COUNT(1) " & _
                   "FROM hrdiscipline_file_tran WHERE hrdiscipline_file_tran.isvoid = 0 AND hrdiscipline_file_tran.disciplinefileunkid = @disciplinefileunkid "

            objDataOpr.AddParameter("@TC", SqlDbType.Int, eZeeDataType.INT_SIZE, intTotalCount, ParameterDirection.InputOutput)

            objDataOpr.ExecNonQuery(StrQ)

            If objDataOpr.ErrorMessage <> "" Then
                exForce = New Exception(objDataOpr.ErrorNumber & " : " & objDataOpr.ErrorMessage)
                Throw exForce
            End If

            intTotalCount = CInt(objDataOpr.GetParameterValue("@TC"))

            StrQ = "SELECT " & _
                   "  @CC = COUNT(A.count_status) " & _
                   "FROM " & _
                   "( " & _
                   "    SELECT " & _
                   "         hrdiscipline_proceeding_master.count_status " & _
                   "        ,ROW_NUMBER()OVER(PARTITION BY hrdiscipline_proceeding_master.disciplinefiletranunkid ORDER BY hrdiscipline_proceeding_master.statusdate DESC) AS rno " & _
                   "    FROM hrdiscipline_proceeding_master " & _
                   "    WHERE hrdiscipline_proceeding_master.isvoid = 0 AND hrdiscipline_proceeding_master.count_status = '" & enProceedingCountStatus.Close & "' " & _
                   "        AND hrdiscipline_proceeding_master.disciplinefileunkid = @disciplinefileunkid " & _
                   ") AS A WHERE A.rno = 1 "

            objDataOpr.AddParameter("@CC", SqlDbType.Int, eZeeDataType.INT_SIZE, intClosedCount, ParameterDirection.InputOutput)

            objDataOpr.ExecNonQuery(StrQ)

            If objDataOpr.ErrorMessage <> "" Then
                exForce = New Exception(objDataOpr.ErrorNumber & " : " & objDataOpr.ErrorMessage)
                Throw exForce
            End If

            intClosedCount = CInt(objDataOpr.GetParameterValue("@CC"))

            If intTotalCount <> intClosedCount Then
                blnFlag = False
            Else
                blnFlag = True
            End If

            Return blnFlag
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: IsCloseCharge; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Function

    Public Function GetClosedCountForOpenProcess(ByVal intDisciplineFileUnkid As Integer, Optional ByVal strTableName As String = "List") As DataSet
        Dim StrQ As String = String.Empty
        Dim exForce As Exception
        Dim dsList As New DataSet
        Try
            If strTableName.Trim.Length <= 0 Then strTableName = "List"

            Using objData As New clsDataOperation

                StrQ = "SELECT " & _
                       "     B.charge_count " & _
                       "    ,B.incident_description " & _
                       "    ,B.charge_category " & _
                       "    ,B.charge_descr " & _
                       "    ,B.charge_severity " & _
                       "    ,B.offencecategoryunkid " & _
                       "    ,B.offenceunkid " & _
                       "    ,A.disciplineproceedingmasterunkid " & _
                       "    ,A.disciplinefiletranunkid " & _
                       "    ,A.disciplinefileunkid " & _
                       "    ,CAST(B.charge_count AS NVARCHAR(MAX))+' - '+ B.incident_description AS display_incident " & _
                       "FROM " & _
                       "( " & _
                       "    SELECT " & _
                       "         ROW_NUMBER()OVER(PARTITION BY hrdiscipline_proceeding_master.disciplinefiletranunkid ORDER BY hrdiscipline_proceeding_master.statusdate DESC) AS rno " & _
                       "        ,hrdiscipline_proceeding_master.disciplineproceedingmasterunkid " & _
                       "        ,hrdiscipline_proceeding_master.disciplinefiletranunkid " & _
                       "        ,hrdiscipline_proceeding_master.disciplinefileunkid " & _
                       "        ,hrdiscipline_proceeding_master.count_status " & _
                       "    FROM hrdiscipline_proceeding_master " & _
                       "    WHERE hrdiscipline_proceeding_master.isvoid = 0 AND hrdiscipline_proceeding_master.disciplinefileunkid = '" & intDisciplineFileUnkid & "' " & _
                       ") AS A " & _
                       "JOIN " & _
                       "( " & _
                       "    SELECT " & _
                       "         ROW_NUMBER()OVER(ORDER BY hrdiscipline_file_tran.disciplinefiletranunkid) AS charge_count " & _
                       "        ,incident_description " & _
                       "        ,cfcommon_master.name AS charge_category " & _
                       "        ,hrdisciplinetype_master.name AS charge_descr " & _
                       "        ,hrdisciplinetype_master.severity AS charge_severity " & _
                       "        ,hrdisciplinetype_master.offencecategoryunkid " & _
                       "        ,hrdisciplinetype_master.disciplinetypeunkid AS offenceunkid " & _
                       "        ,hrdiscipline_file_tran.disciplinefiletranunkid " & _
                       "    FROM hrdiscipline_file_tran " & _
                       "        JOIN hrdisciplinetype_master ON hrdiscipline_file_tran.offenceunkid = hrdisciplinetype_master.disciplinetypeunkid " & _
                       "        JOIN cfcommon_master ON cfcommon_master.masterunkid = hrdisciplinetype_master.offencecategoryunkid " & _
                       "    WHERE hrdiscipline_file_tran.disciplinefileunkid = '" & intDisciplineFileUnkid & "' AND hrdiscipline_file_tran.isvoid = 0 " & _
                       ") AS B ON A.disciplinefiletranunkid = B.disciplinefiletranunkid " & _
                       "WHERE A.rno = 1 AND A.count_status = '" & enProceedingCountStatus.Close & "' "

                dsList = objData.ExecQuery(StrQ, strTableName)

                If objData.ErrorMessage <> "" Then
                    exForce = New Exception(objData.ErrorNumber & " : " & objData.ErrorMessage)
                    Throw exForce
                End If

            End Using
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetClosedCountForOpenProcess; Module Name: " & mstrModuleName)
        Finally
        End Try
        Return dsList
    End Function

    Public Function IsValidOpen_Operation(ByVal intDisciplineFileTranId As Integer) As String
        Dim StrQ As String = String.Empty
        Dim StrMsg As String = String.Empty
        Try
            Using objDataOp As New clsDataOperation
                Dim iCnt As Integer = 0
                StrQ = "SELECT 1 FROM hrdiscipline_proceeding_master " & _
                       "WHERE hrdiscipline_proceeding_master.isvoid = 0 " & _
                       "    AND hrdiscipline_proceeding_master.count_status = '" & enProceedingCountStatus.Open & "' AND hrdiscipline_proceeding_master.disciplinefiletranunkid = '" & intDisciplineFileTranId & "' " & _
                       "ORDER BY hrdiscipline_proceeding_master.statusdate DESC "

                iCnt = objDataOp.RecordCount(StrQ)

                If objDataOp.ErrorMessage <> "" Then
                    Throw New Exception(objDataOp.ErrorNumber & " : " & objDataOp.ErrorMessage)
                End If

                If iCnt > 0 Then
                    StrMsg = Language.getMessage(mstrModuleName, 22, "Sorry, you cannot open the selected count. Reason: The same count is open for different proceeding date.")
                End If

            End Using
        Catch ex As Exception
            StrMsg = ex.Message
            Throw New Exception(ex.Message & "; Procedure Name: IsValidOpen_Operation; Module Name: " & mstrModuleName)
        Finally
        End Try
        Return StrMsg
    End Function

    'S.SANDEEP [24 MAY 2016] -- Start
    'Email Notification
    Public Function SendMailToEmployee(ByVal intDisciplineFileUnkid As Integer, ByVal intCompanyUnkId As Integer) As Boolean
        'Sohail (30 Nov 2017) - [intCompanyUnkId]
        Try
            Dim objGrpMast As New clsGroup_Master
            objGrpMast._Groupunkid = 1
            If objGrpMast._Groupname.ToString.ToUpper = "NMB PLC" Then Return True
            Dim strMessage As String = String.Empty
            Dim strBuilder As New StringBuilder
            Dim objNetConn As New clsNetConnectivity
            Dim objMail As New clsSendMail
            Dim objEmployee As New clsEmployee_Master
            Dim objUser As New clsUserAddEdit
            Dim objDiscFileMaster As New clsDiscipline_file_master

            If objNetConn._Conected = False Then Exit Function

            objUser._Userunkid = mintUserunkid
            objDiscFileMaster._Disciplinefileunkid = intDisciplineFileUnkid
            objEmployee._Employeeunkid(eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate)) = objDiscFileMaster._Involved_Employeeunkid
            'S.SANDEEP |01-OCT-2019| -- START
            'ISSUE/ENHANCEMENT : NMB DISCIPLINARY UAT COMMENTS
            'Dim strEmployeeName As String = objEmployee._Firstname + " " + objEmployee._Othername + " " + objEmployee._Surname
            Dim strEmployeeName As String = objEmployee._Firstname + " " + objEmployee._Surname
            'S.SANDEEP |01-OCT-2019| -- END

            If mblnIsApprovedDisApproved = True Then
                objMail._Subject = Language.getMessage(mstrModuleName, 34, "Approve/Disapprove Charge Count Notification") & " " & Language.getMessage(mstrModuleName, 26, " :  Reference No - ") & objDiscFileMaster._Reference_No
            Else
                objMail._Subject = Language.getMessage(mstrModuleName, 25, "Charge Opening Notification") & " " & Language.getMessage(mstrModuleName, 26, " :  Reference No - ") & objDiscFileMaster._Reference_No
            End If

            strMessage = "<HTML><BODY>"

            'Gajanan [27-Mar-2019] -- Start
            'Enhancement - Change Email Language
            'strMessage &= Language.getMessage(mstrModuleName, 27, "Dear") & " " & "&nbsp;<b>" & strEmployeeName & "</b>" & " ,<BR></BR>"
            strMessage &= Language.getMessage(mstrModuleName, 27, "Dear") & " " & "&nbsp;<b>" & getTitleCase(strEmployeeName) & "</b>" & " ,<BR></BR>"
            'Gajanan [27-Mar-2019] -- End

            If mblnIsApprovedDisApproved = True Then

                'Gajanan [27-Mar-2019] -- Start
                'Enhancement - Change Email Language
                'strMessage &= "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" & Language.getMessage(mstrModuleName, 35, "This is to infom you that, Charge count posted against you has been approved. Please refer following details.") & "<BR></BR><BR></BR><BR></BR>"
                strMessage &= Language.getMessage(mstrModuleName, 35, "This is to infom you that, Charge count posted against you has been approved. Please refer following details.") & "<BR></BR><BR></BR><BR></BR>"
                'Gajanan [27-Mar-2019] -- End

            Else
                'Gajanan [27-Mar-2019] -- Start
                'Enhancement - Change Email Language
                'strMessage &= "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" & Language.getMessage(mstrModuleName, 28, "This is to infom you that, Following charge count opening details.") & "<BR></BR><BR></BR><BR></BR>"
                strMessage &= Language.getMessage(mstrModuleName, 28, "This is to infom you that, Following charge count opening details.") & "<BR></BR><BR></BR><BR></BR>"
                'Gajanan [27-Mar-2019] -- End
            End If

            strMessage &= objDiscFileMaster.getChargeDetailForEmail(intDisciplineFileUnkid)

            strMessage &= "<b>" & Language.getMessage(mstrModuleName, 29, "Charges Count :") & "</b><BR></BR>"

            strMessage &= mstrNotificationContent


            'Gajanan [27-Mar-2019] -- Start
            'Enhancement - Change Email Language
            'strMessage &= "<BR></BR><BR></BR><center>""POWERED BY ARUTI HR & PAYROLL MANAGEMENT SOFTWARE""</center>"
            strMessage &= "<p><center style='padding-top:25px;font-size:10.0pt;font-family:&quot;Verdana&quot;,&quot;Sans-Serif&quot;;color:#333399;margin-left:0px;margin-right:0px;margin-top:0px;margin-bottom:10px'><b>&quot;POWERED BY ARUTI HR &amp; PAYROLL MANAGEMENT SOFTWARE.&quot;</b></center></p>"
            'Gajanan [27-Mar-2019] -- End

            strMessage &= "</BODY></HTML>"

            Dim strSuccess As String = ""
            objMail._Message = strMessage
            objMail._ToEmail = objEmployee._Email
            If mintLoginTypeId <= 0 Then mintLoginTypeId = enLogin_Mode.DESKTOP
            objMail._FormName = mstrFormName
            objMail._ClientIP = mstrClientIP
            objMail._HostName = mstrHostName
            objMail._LoginEmployeeunkid = mintLoginEmployeeunkid
            objMail._OperationModeId = mintLoginTypeId
            objMail._UserUnkid = mintUserunkid
            objMail._SenderAddress = IIf(objUser._Email = "", objUser._Username, objUser._Email)
            objMail._ModuleRefId = clsSendMail.enAT_VIEW_TYPE.DISCIPLINE_MGT
            'Sohail (30 Nov 2017) -- Start
            'SUMATRA Enhancement – SUMATRA – issue # 0001669: Additional feature for Exchange Server configuration setting in 70.1.
            'strSuccess = objMail.SendMail()
            strSuccess = objMail.SendMail(intCompanyUnkId)
            'Sohail (30 Nov 2017) -- End

            If strSuccess.Trim.Length <= 0 Then
                Return True
            Else
                Return False
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: SendMailToEmployee; Module Name: " & mstrModuleName)
        End Try
    End Function

    Public Function SubmitForApproval(ByVal intDisciplineProceedingMasterunkid As Integer, ByVal intDisciplineFileUnkid As Integer, _
                                      ByVal intYearUnkid As Integer, ByVal dtCurrentDateTime As DateTime, _
                                      ByVal strCoySenderAddress As String, _
                                      ByVal strFormNames As List(Of String), _
                                      ByVal strDocumentPath As String, _
                                      ByVal intCompanyUnkId As Integer) As Boolean
        'Sohail (30 Nov 2017) - [intCompanyUnkId]
        'S.SANDEEP [25 JUL 2016] -- START {strCoySenderAddress} -- END
        'S.SANDEEP [26 JUL 2016] -- START {strFormNames,strDocumentPath} -- END

        Dim exForce As Exception
        Try
            Dim strQ As String = String.Empty
            objDataOperation = New clsDataOperation
            Dim strMessage As String = String.Empty
            Dim strBuilder As New StringBuilder
            Dim objNetConn As New clsNetConnectivity
            Dim objMail As New clsSendMail
            Dim objUser As New clsUserAddEdit
            Dim objDiscFileMaster As New clsDiscipline_file_master
            Dim dsList As DataSet = Nothing

            'S.SANDEEP [25 JUL 2016] -- START
            'ENHANCEMENT : DISCIPLINE MAKE-OVER {BY ANDREW}
            If strCoySenderAddress.Trim.Length <= 0 Then
                mstrMessage = Language.getMessage(mstrModuleName, 36, "Sorry, Email Setup under company is not configured.")
                Return False
            End If
            'S.SANDEEP [25 JUL 2016] -- END

            If objNetConn._Conected = False Then Exit Function

            mintDisciplineProceedingMasterunkid = intDisciplineProceedingMasterunkid
            Call GetProceedingDetail()

            objUser._Userunkid = mintUserunkid
            objDiscFileMaster._Disciplinefileunkid = intDisciplineFileUnkid

            dsList = objUser.Get_UserBy_PrivilegeId(1020, intYearUnkid)

            'S.SANDEEP [25 JUL 2016] -- START
            'ENHANCEMENT : DISCIPLINE MAKE-OVER {BY ANDREW}
            mstrMessage = ""
            If dsList.Tables(0).Rows.Count <= 0 Then
                mstrMessage = Language.getMessage(mstrModuleName, 38, "Sorry, Privilege for approving the proceeding count or email are not assigned to the user. Please make sure that you assing the privilege and email.")
                Return False
            End If
            'S.SANDEEP [25 JUL 2016] -- END

            If dsList.Tables(0).Rows.Count > 0 Then

                For Each dRow As DataRow In dsList.Tables(0).Rows
                    objMail._Subject = Language.getMessage(mstrModuleName, 32, "Submit for Approval Notification") & " " & Language.getMessage(mstrModuleName, 26, " :  Reference No - ") & objDiscFileMaster._Reference_No

                    strMessage = "<HTML><BODY>"
                    'strMessage &= Language.getMessage(mstrModuleName, 27, "Dear") & " " & "&nbsp;<b>" & dRow.Item("UName") & "</b>" & " ,<BR></BR>"
                    strMessage &= Language.getMessage(mstrModuleName, 27, "Dear") & " " & "&nbsp;<b>" & getTitleCase(dRow.Item("UName").ToString()) & "</b>" & " ,<BR></BR>"

                    'Gajanan [27-Mar-2019] -- Start
                    'Enhancement - Change Email Language
                    'strMessage &= "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" & Language.getMessage(mstrModuleName, 33, "This is to infom you that, Following charge count details has been submitted for approval.") & "<BR></BR><BR></BR>"
                    strMessage &= Language.getMessage(mstrModuleName, 33, "This is to infom you that, Following charge count details has been submitted for approval.") & "<BR></BR><BR></BR>"
                    'Gajanan [27-Mar-2019] -- End

                    strMessage &= objDiscFileMaster.getChargeDetailForEmail(intDisciplineFileUnkid)

                    strMessage &= "<b>" & Language.getMessage(mstrModuleName, 29, "Charges Count :") & "</b><BR></BR>"

                    mstrNotificationContent = ""
                    Call getOpenCountDetailsForEmail(mdtProceeding.Rows(0))

                    strMessage &= mstrNotificationContent


                    'Gajanan [27-Mar-2019] -- Start
                    'Enhancement - Change Email Language
                    'strMessage &= "<BR></BR><BR></BR><center>""POWERED BY ARUTI HR & PAYROLL MANAGEMENT SOFTWARE""</center>"
                    strMessage &= "<p><center style='padding-top:25px;font-size:10.0pt;font-family:&quot;Verdana&quot;,&quot;Sans-Serif&quot;;color:#333399;margin-left:0px;margin-right:0px;margin-top:0px;margin-bottom:10px'><b>&quot;POWERED BY ARUTI HR &amp; PAYROLL MANAGEMENT SOFTWARE.&quot;</b></center></p>"
                    'Gajanan [27-Mar-2019] -- End

                    strMessage &= "</BODY></HTML>"

                    'S.SANDEEP [26 JUL 2016] -- START
                    'ENHANCEMENT : DISCIPLINE MAKE-OVER {BY ANDREW}
                    Dim dtDcouments As DataTable
                    Dim objScanAttach As New clsScan_Attach_Documents
                    dtDcouments = objScanAttach.GetQulificationAttachment(objDiscFileMaster._Involved_Employeeunkid, enScanAttactRefId.DISCIPLINES, intDisciplineProceedingMasterunkid, strDocumentPath)
                    objScanAttach = Nothing
                    'S.SANDEEP [26 JUL 2016] -- END


                    Dim strSuccess As String = ""
                    objMail._Message = strMessage

                    'S.SANDEEP [26 JUL 2016] -- START
                    'ENHANCEMENT : DISCIPLINE MAKE-OVER {BY ANDREW}
                    If dtDcouments.Rows.Count > 0 Then
                        Dim strAttachedFile As String = ""
                        Dim xQry = (From v In strFormNames Join dt In dtDcouments.AsEnumerable() On dt.Field(Of String)("form_name") Equals v Select dt)
                        'S.SANDEEP |11-NOV-2019| -- START
                        'ISSUE/ENHANCEMENT : NMB DISCIPLINARY UAT COMMENTS
                        'dtDcouments = xQry.CopyToDataTable()
                        'strAttachedFile = String.Join(",", dtDcouments.AsEnumerable().Select(Function(x) x.Field(Of String)("filepath").ToString).ToArray)
                        If xQry IsNot Nothing AndAlso xQry.Count > 0 Then
                        dtDcouments = xQry.CopyToDataTable()
                        strAttachedFile = String.Join(",", dtDcouments.AsEnumerable().Select(Function(x) x.Field(Of String)("filepath").ToString).ToArray)
                        End If
                        'S.SANDEEP |11-NOV-2019| -- END
                        objMail._AttachedFiles = strAttachedFile
                    End If
                    'S.SANDEEP [26 JUL 2016] -- END

                    objMail._ToEmail = dRow.Item("UEmail").ToString
                    If mintLoginTypeId <= 0 Then mintLoginTypeId = enLogin_Mode.DESKTOP
                    objMail._FormName = mstrFormName
                    objMail._ClientIP = mstrClientIP
                    objMail._HostName = mstrHostName
                    objMail._LoginEmployeeunkid = mintLoginEmployeeunkid
                    objMail._OperationModeId = mintLoginTypeId
                    objMail._UserUnkid = mintUserunkid
                    objMail._SenderAddress = IIf(objUser._Email = "", objUser._Username, objUser._Email)
                    objMail._ModuleRefId = clsSendMail.enAT_VIEW_TYPE.DISCIPLINE_MGT
                    'Sohail (30 Nov 2017) -- Start
                    'SUMATRA Enhancement – SUMATRA – issue # 0001669: Additional feature for Exchange Server configuration setting in 70.1.
                    'strSuccess = objMail.SendMail()
                    strSuccess = objMail.SendMail(intCompanyUnkId)
                    'Sohail (30 Nov 2017) -- End

                    If strSuccess.Trim.Length <= 0 Then

                        strQ = "UPDATE hrdiscipline_proceeding_master SET issubmitforapproval = 1 " & _
                               "WHERE disciplineproceedingmasterunkid = " & intDisciplineProceedingMasterunkid

                        objDataOperation.ExecNonQuery(strQ)

                        If objDataOperation.ErrorMessage <> "" Then
                            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                            Throw exForce
                        End If

                        If InsertATProceedingMaster(objDataOperation, mintUserunkid, dtCurrentDateTime, mdtProceeding.Rows(0), enAuditType.EDIT) = False Then
                            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                            Throw exForce
                        End If

                        Return True
                    Else
                        'S.SANDEEP [25 JUL 2016] -- START
                        'ENHANCEMENT : DISCIPLINE MAKE-OVER {BY ANDREW}
                        mstrMessage = strSuccess
                        'S.SANDEEP [25 JUL 2016] -- END
                        Return False
                    End If
                Next
            Else
                Return False
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: SubmitForApproval; Module Name: " & mstrModuleName)
        End Try
    End Function

    Public Sub getOpenCountDetailsForEmail(ByVal dRProceeding As DataRow)
        Try
            Dim strBuilder As New StringBuilder
            Dim dtChargeTable As DataTable = Nothing
            Dim objDisciplineFileTran As New clsDiscipline_file_tran

            objDisciplineFileTran._Disciplinefileunkid = dRProceeding.Item("disciplinefileunkid")
            dtChargeTable = objDisciplineFileTran._ChargesTable

            If dtChargeTable IsNot Nothing Then
                Dim row As DataRow() = dtChargeTable.Select("disciplinefiletranunkid = " & CInt(dRProceeding.Item("disciplinefiletranunkid")) & "")
                If row.Length > 0 Then
                    For Each dRow As DataRow In row
                        strBuilder.Append("<TABLE BORDER=1 CELLSPACING=0 CELLPADDING=3 WIDTH='100%'>" & vbCrLf)
                        strBuilder.Append("<TR WIDTH='100%'>" & vbCrLf)
                        strBuilder.Append("<TD WIDTH='40%' BGCOLOR='#4682b4' ALIGN='LEFT'><FONT COLOR='WHITE' SIZE=2><B>" & Language.getMessage(mstrModuleName, 23, "Particular") & "</B></FONT></TD>" & vbCrLf)
                        strBuilder.Append("<TD WIDTH='60%' BGCOLOR='#4682b4' ALIGN='LEFT'><FONT COLOR='WHITE' SIZE=2><B>" & Language.getMessage(mstrModuleName, 24, "Details") & "</B></FONT></TD>" & vbCrLf)
                        strBuilder.Append("</TR>" & vbCrLf)
                        strBuilder.Append("<TR WIDTH='100%'>" & vbCrLf)
                        strBuilder.Append("<TD WIDTH='40%' ALIGN='LEFT' VALIGN = 'TOP'><FONT SIZE=2><B>" & Language.getMessage(mstrModuleName, 30, "Proceeding Date") & "</B></FONT></TD>" & vbCrLf)
                        strBuilder.Append("<TD WIDTH='60%' ALIGN='LEFT'><FONT SIZE=2>" & CDate(dRProceeding.Item("proceeding_date")).Date & "</FONT></TD>" & vbCrLf)
                        strBuilder.Append("</TR>" & vbCrLf)
                        strBuilder.Append("<TR WIDTH='100%'>" & vbCrLf)
                        strBuilder.Append("<TD WIDTH='40%' ALIGN='LEFT' VALIGN = 'TOP'><FONT SIZE=2><B>" & Language.getMessage(mstrModuleName, 8, "Incident") & "</B></FONT></TD>" & vbCrLf)
                        strBuilder.Append("<TD WIDTH='60%' ALIGN='LEFT'><FONT SIZE=2>" & dRow("charge_count") & Language.getMessage(mstrModuleName, 31, " - ") & dRow("incident_description") & "</FONT></TD>" & vbCrLf)
                        strBuilder.Append("</TR>" & vbCrLf)
                        strBuilder.Append("<TR WIDTH='100%'>" & vbCrLf)
                        strBuilder.Append("<TD WIDTH='40%' ALIGN='LEFT' VALIGN = 'TOP'><FONT SIZE=2><B>" & Language.getMessage(mstrModuleName, 9, "Offence Category") & "</B></FONT></TD>" & vbCrLf)
                        strBuilder.Append("<TD WIDTH='60%' ALIGN='LEFT'><FONT SIZE=2>" & dRow("charge_category") & "</FONT></TD>" & vbCrLf)
                        strBuilder.Append("</TR>" & vbCrLf)
                        strBuilder.Append("<TR WIDTH='100%'>" & vbCrLf)
                        strBuilder.Append("<TD WIDTH='40%' ALIGN='LEFT' VALIGN = 'TOP'><FONT SIZE=2><B>" & Language.getMessage(mstrModuleName, 10, "Offence Description") & "</B></FONT></TD>" & vbCrLf)
                        strBuilder.Append("<TD WIDTH='60%' ALIGN='LEFT'><FONT SIZE=2>" & dRow("charge_descr") & "</FONT></TD>" & vbCrLf)
                        strBuilder.Append("</TR>" & vbCrLf)
                        'strBuilder.Append("<TR WIDTH='100%'>" & vbCrLf)
                        'strBuilder.Append("<TD WIDTH='40%' ALIGN='LEFT' VALIGN = 'TOP'><FONT SIZE=2><B>" & Language.getMessage(mstrModuleName, 12, "Committee") & "</B></FONT></TD>" & vbCrLf)
                        'strBuilder.Append("<TD WIDTH='60%' ALIGN='LEFT'><FONT SIZE=2>" & dRProceeding.Item("committee") & "</FONT></TD>" & vbCrLf)
                        'strBuilder.Append("</TR>" & vbCrLf)
                        'strBuilder.Append("<TR WIDTH='100%'>" & vbCrLf)
                        'strBuilder.Append("<TD WIDTH='40%' ALIGN='LEFT' VALIGN = 'TOP'><FONT SIZE=2><B>" & Language.getMessage(mstrModuleName, 13, "Member(s)") & "</B></FONT></TD>" & vbCrLf)
                        'strBuilder.Append("<TD WIDTH='60%' ALIGN='LEFT'><FONT SIZE=2>" & dRProceeding.Item("investigator") & "</FONT></TD>" & vbCrLf)
                        'strBuilder.Append("</TR>" & vbCrLf)
                        strBuilder.Append("<TR WIDTH='100%'>" & vbCrLf)
                        strBuilder.Append("<TD WIDTH='40%' ALIGN='LEFT' VALIGN = 'TOP'><FONT SIZE=2><B>" & Language.getMessage(mstrModuleName, 14, "Disciplinary Action") & "</B></FONT></TD>" & vbCrLf)
                        strBuilder.Append("<TD WIDTH='60%' ALIGN='LEFT'><FONT SIZE=2>" & dRProceeding.Item("reason_action") & "</FONT></TD>" & vbCrLf)
                        strBuilder.Append("</TR>" & vbCrLf)
                        strBuilder.Append("<TR WIDTH='100%'>" & vbCrLf)
                        strBuilder.Append("<TD WIDTH='40%' ALIGN='LEFT' VALIGN = 'TOP'><FONT SIZE=2><B>" & Language.getMessage(mstrModuleName, 15, "Count Status") & "</B></FONT></TD>" & vbCrLf)
                        strBuilder.Append("<TD WIDTH='60%' ALIGN='LEFT'><FONT SIZE=2>" & IIf(CInt(dRProceeding.Item("count_status")) = enProceedingCountStatus.Open, Language.getMessage(mstrModuleName, 6, "Open"), Language.getMessage(mstrModuleName, 7, "Close")) & "</FONT></TD>" & vbCrLf)
                        strBuilder.Append("</TR>" & vbCrLf)
                        strBuilder.Append("<TR WIDTH='100%'>" & vbCrLf)
                        strBuilder.Append("<TD WIDTH='40%' ALIGN='LEFT' VALIGN = 'TOP'><FONT SIZE=2><B>" & Language.getMessage(mstrModuleName, 16, "Proceeding Details") & "</B></FONT></TD>" & vbCrLf)
                        strBuilder.Append("<TD WIDTH='60%' ALIGN='LEFT'><FONT SIZE=2>" & dRProceeding.Item("proceeding_details") & "</FONT></TD>" & vbCrLf)
                        strBuilder.Append("</TR>" & vbCrLf)
                        strBuilder.Append("<TR WIDTH='100%'>" & vbCrLf)
                        strBuilder.Append("<TD WIDTH='40%' ALIGN='LEFT' VALIGN = 'TOP'><FONT SIZE=2><B>" & Language.getMessage(mstrModuleName, 17, "Remarks Comments") & "</B></FONT></TD>" & vbCrLf)
                        strBuilder.Append("<TD WIDTH='60%' ALIGN='LEFT'><FONT SIZE=2>" & IIf(dRProceeding.Item("remarks_comments").ToString.Trim.Length <= 0, "&nbsp;", dRProceeding.Item("remarks_comments")) & "</FONT></TD>" & vbCrLf)
                        strBuilder.Append("</TR>" & vbCrLf)
                        strBuilder.Append("<TR WIDTH='100%'>" & vbCrLf)
                        strBuilder.Append("<TD WIDTH='40%' ALIGN='LEFT' VALIGN = 'TOP'><FONT SIZE=2><B>" & Language.getMessage(mstrModuleName, 18, "Person Involved Comments") & "</B></FONT></TD>" & vbCrLf)
                        strBuilder.Append("<TD WIDTH='60%' ALIGN='LEFT'><FONT SIZE=2>" & IIf(dRProceeding.Item("person_involved_comments").ToString.Trim.Length <= 0, "&nbsp;", dRProceeding.Item("person_involved_comments")) & "</FONT></TD>" & vbCrLf)
                        strBuilder.Append("</TR>" & vbCrLf)
                        strBuilder.Append("</TABLE><BR></BR>" & vbCrLf)
                    Next
                End If
            End If

            mstrNotificationContent &= strBuilder.ToString

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: getOpenCountDetailsForEmail; Module Name: " & mstrModuleName)
        End Try
    End Sub
    'S.SANDEEP [24 MAY 2016] -- End

    'S.SANDEEP [26 JUL 2016] -- START
    'ENHANCEMENT : DISCIPLINE MAKE-OVER {BY ANDREW}
    Public Function GetComboListForCharge(ByVal intProceedingMasterId As Integer, Optional ByVal blnFlag As Boolean = False) As DataSet
        Dim StrQ As String = ""
        Dim dsList As New DataSet
        Try
            Using objDo As New clsDataOperation
                If blnFlag Then
                    StrQ = "SELECT 0 AS disciplinefileunkid, @Select AS reference_no UNION "
                End If
                StrQ &= "SELECT DISTINCT " & _
                        "    hrdiscipline_proceeding_master.disciplineproceedingmasterunkid AS disciplinefileunkid " & _
                        "   ,hrdiscipline_file_master.reference_no AS reference_no " & _
                        "FROM hrdiscipline_proceeding_master " & _
                        "   JOIN hrdiscipline_file_master ON hrdiscipline_file_master.disciplinefileunkid = hrdiscipline_proceeding_master.disciplinefileunkid " & _
                        "WHERE hrdiscipline_proceeding_master.isvoid = 0 AND hrdiscipline_proceeding_master.disciplineproceedingmasterunkid = @disciplineproceedingmasterunkid "

                objDo.AddParameter("@disciplineproceedingmasterunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intProceedingMasterId)
                objDo.AddParameter("@Select", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 37, "Select"))

                dsList = objDo.ExecQuery(StrQ, "List")

                If objDo.ErrorMessage <> "" Then
                    Throw New Exception(objDo.ErrorNumber & " : " & objDo.ErrorMessage)
                End If

            End Using
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetComboListForCharge; Module Name: " & mstrModuleName)
        Finally
        End Try
        Return dsList
    End Function
    'S.SANDEEP [26 JUL 2016] -- END

    'S.SANDEEP |11-NOV-2019| -- START
    'ISSUE/ENHANCEMENT : NMB DISCIPLINARY UAT COMMENTS
    Private Function UpdateRowValue(ByVal dr As DataRow, ByVal intValue As Integer, ByVal strTransactionScreen As String) As Boolean
        Try
            dr("transactionunkid") = intValue
            dr("form_name") = strTransactionScreen
            dr.AcceptChanges()
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: UpdateRowValue; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Function
    'S.SANDEEP |11-NOV-2019| -- END

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Public Shared Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "WEB")
            Language.setMessage(mstrModuleName, 3, "Select")
            Language.setMessage(mstrModuleName, 4, "Approved")
            Language.setMessage(mstrModuleName, 5, "Pending")
            Language.setMessage(mstrModuleName, 6, "Open")
            Language.setMessage(mstrModuleName, 7, "Close")
            Language.setMessage(mstrModuleName, 8, "Incident")
            Language.setMessage(mstrModuleName, 9, "Offence Category")
            Language.setMessage(mstrModuleName, 10, "Offence Description")
            Language.setMessage(mstrModuleName, 11, "Severity")
            Language.setMessage(mstrModuleName, 12, "Committee")
            Language.setMessage(mstrModuleName, 13, "Member(s)")
            Language.setMessage(mstrModuleName, 14, "Disciplinary Action")
            Language.setMessage(mstrModuleName, 15, "Count Status")
            Language.setMessage(mstrModuleName, 16, "Proceeding Details")
            Language.setMessage(mstrModuleName, 17, "Remarks Comments")
            Language.setMessage(mstrModuleName, 18, "Person Involved Comments")
            Language.setMessage(mstrModuleName, 19, "Sorry, Proceeding date must be greater than the last proceeding date which is [")
            Language.setMessage(mstrModuleName, 20, "]. Please select a valid date to continue.")
            Language.setMessage(mstrModuleName, 21, "Sorry, You cannot add another proceeding details for the selected count. Reason : The last proceeding is not yet approved.")
            Language.setMessage(mstrModuleName, 22, "Sorry, you cannot open the selected count. Reason: The same count is open for different proceeding date.")
            Language.setMessage(mstrModuleName, 23, "Particular")
            Language.setMessage(mstrModuleName, 24, "Details")
            Language.setMessage(mstrModuleName, 25, "Charge Opening Notification")
            Language.setMessage(mstrModuleName, 26, " :  Reference No -")
            Language.setMessage(mstrModuleName, 27, "Dear")
            Language.setMessage(mstrModuleName, 28, "This is to infom you that, Following charge count opening details.")
            Language.setMessage(mstrModuleName, 29, "Charges Count :")
            Language.setMessage(mstrModuleName, 30, "Proceeding Date")
            Language.setMessage(mstrModuleName, 31, " -")
            Language.setMessage(mstrModuleName, 32, "Submit for Approval Notification")
            Language.setMessage(mstrModuleName, 33, "This is to infom you that, Following charge count details has been submitted for approval.")
            Language.setMessage(mstrModuleName, 34, "Approve/Disapprove Charge Count Notification")
            Language.setMessage(mstrModuleName, 35, "This is to infom you that, Charge count posted against you has been approved. Please refer following details.")
            Language.setMessage(mstrModuleName, 36, "Sorry, Email Setup under company is not configured.")
            Language.setMessage(mstrModuleName, 37, "Select")
            Language.setMessage(mstrModuleName, 38, "Sorry, Privilege for approving the proceeding count or email are not assigned to the user. Please make sure that you assing the privilege and email.")
			Language.setMessage("clsDiscipline_file_master", 3, "Internal")
			Language.setMessage("clsDiscipline_file_master", 4, "External")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class