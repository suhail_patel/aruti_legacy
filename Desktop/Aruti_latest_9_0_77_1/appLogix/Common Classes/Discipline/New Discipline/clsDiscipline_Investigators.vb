﻿'************************************************************************************************************************************
'Class Name : clsDiscipline_Investigators.vb
'Purpose    :
'Date       :28/11/2011
'Written By :Sandeep J. Sharma
'Modified   :
'************************************************************************************************************************************
Imports eZeeCommonLib
''' <summary>
''' Purpose: 
''' Developer: Sandeep J. Sharma
''' </summary>

Public Class clsDiscipline_Investigators
    Private Const mstrModuleName = "clsDiscipline_Investigators"
    Private mstrMessage As String = ""
    Private mdtTran As DataTable
    Private mintResolutionUnkid As Integer
    Private objDataOp As clsDataOperation

#Region " Propertis "

    Public Property _ResolutionUnkid() As Integer
        Get
            Return mintResolutionUnkid
        End Get
        Set(ByVal value As Integer)
            mintResolutionUnkid = value
            Call Get_Investigators()
        End Set
    End Property

    Public Property _DataTable() As DataTable
        Get
            Return mdtTran
        End Get
        Set(ByVal value As DataTable)
            mdtTran = value
        End Set
    End Property

#End Region

#Region " Constructor "

    Public Sub New()

        mdtTran = New DataTable("Investigators")

        mdtTran.Columns.Add("investigatorunkid", System.Type.GetType("System.Int32")).DefaultValue = -1
        mdtTran.Columns.Add("resolutionunkid", System.Type.GetType("System.Int32")).DefaultValue = -1
        mdtTran.Columns.Add("employeeunkid", System.Type.GetType("System.Int32")).DefaultValue = -1
        mdtTran.Columns.Add("ex_name", System.Type.GetType("System.String")).DefaultValue = ""
        mdtTran.Columns.Add("ex_company", System.Type.GetType("System.String")).DefaultValue = ""
        mdtTran.Columns.Add("ex_department", System.Type.GetType("System.String")).DefaultValue = ""
        mdtTran.Columns.Add("ex_contactno", System.Type.GetType("System.String")).DefaultValue = ""
        mdtTran.Columns.Add("investigator_remark", System.Type.GetType("System.String")).DefaultValue = ""
        mdtTran.Columns.Add("isvoid", System.Type.GetType("System.Boolean")).DefaultValue = False
        mdtTran.Columns.Add("voiduserunkid", System.Type.GetType("System.Int32")).DefaultValue = -1
        mdtTran.Columns.Add("voiddatetime", System.Type.GetType("System.DateTime")).DefaultValue = DBNull.Value
        mdtTran.Columns.Add("voidreason", System.Type.GetType("System.String")).DefaultValue = ""
        mdtTran.Columns.Add("AUD", System.Type.GetType("System.String")).DefaultValue = ""
        mdtTran.Columns.Add("GUID", System.Type.GetType("System.String")).DefaultValue = ""
        mdtTran.Columns.Add("employee_name", System.Type.GetType("System.String")).DefaultValue = ""
        mdtTran.Columns.Add("company", System.Type.GetType("System.String")).DefaultValue = ""
        mdtTran.Columns.Add("department", System.Type.GetType("System.String")).DefaultValue = ""
        mdtTran.Columns.Add("contact", System.Type.GetType("System.String")).DefaultValue = ""
        mdtTran.Columns.Add("committeetranunkid", System.Type.GetType("System.Int32")).DefaultValue = -1        

    End Sub

#End Region

#Region " Private/Public Function/Procedures "

    Private Sub Get_Investigators()
        Dim StrQ As String = ""
        Dim exForce As Exception
        Dim dsList As New DataSet
        Try
            objDataOp = New clsDataOperation

            StrQ = "SELECT " & _
                    "  hrdiscipline_investigators.investigatorunkid " & _
                    ", hrdiscipline_investigators.resolutionunkid " & _
                    ", hrdiscipline_investigators.employeeunkid " & _
                    ", hrdiscipline_investigators.ex_name " & _
                    ", hrdiscipline_investigators.ex_company " & _
                    ", hrdiscipline_investigators.ex_department " & _
                    ", hrdiscipline_investigators.ex_contactno " & _
                    ", hrdiscipline_investigators.investigator_remark " & _
                    ", hrdiscipline_investigators.isvoid " & _
                    ", hrdiscipline_investigators.voiduserunkid " & _
                    ", hrdiscipline_investigators.voiddatetime " & _
                    ", hrdiscipline_investigators.voidreason " & _
                    ", '' AS AUD " & _
                    ", ISNULL(hremployee_master.firstname,'')+' '+ISNULL(hremployee_master.othername,'')+' '+ISNULL(hremployee_master.surname,'') AS employee_name " & _
                    ", ISNULL(hrdepartment_master.name,'') AS department " & _
                    ", ISNULL(hremployee_master.present_tel_no,'') AS contact " & _
                    "	,-1 AS committeetranunkid " & _
                    "FROM hrdiscipline_investigators " & _
                    " LEFT JOIN hremployee_master ON hrdiscipline_investigators.employeeunkid = hremployee_master.employeeunkid " & _
                    " LEFT JOIN hrdepartment_master ON hremployee_master.departmentunkid = hrdepartment_master.departmentunkid " & _
                    "WHERE isvoid = 0 AND hrdiscipline_investigators.committeetranunkid <=0 AND hrdiscipline_investigators.resolutionunkid = @resolutionunkid " & _
                    "UNION ALL " & _
                    "SELECT " & _
                    "	 hrdiscipline_investigators.investigatorunkid " & _
                    "	,hrdiscipline_investigators.resolutionunkid " & _
                    "	,hrdiscipline_committee.employeeunkid " & _
                    "	,hrdiscipline_committee.ex_name " & _
                    "	,hrdiscipline_committee.ex_company " & _
                    "	,hrdiscipline_committee.ex_department " & _
                    "	,hrdiscipline_committee.ex_contactno " & _
                    "	,hrdiscipline_investigators.investigator_remark " & _
                    "	,hrdiscipline_investigators.isvoid " & _
                    "	,hrdiscipline_investigators.voiduserunkid " & _
                    "	,hrdiscipline_investigators.voiddatetime " & _
                    "	,hrdiscipline_investigators.voidreason " & _
                    "	,'' AS AUD " & _
                    "	,ISNULL(hremployee_master.firstname,'')+' '+ISNULL(hremployee_master.othername,'')+' '+ISNULL(hremployee_master.surname,'') AS employee_name " & _
                    "	,ISNULL(hrdepartment_master.name,'') AS department " & _
                    "	,ISNULL(hremployee_master.present_tel_no,'') AS contact " & _
                    "	,hrdiscipline_committee.committeetranunkid AS committeetranunkid " & _
                    "FROM hrdiscipline_investigators " & _
                    "	JOIN hrdiscipline_committee ON hrdiscipline_investigators.committeetranunkid = hrdiscipline_committee.committeetranunkid " & _
                    "	LEFT JOIN hremployee_master ON hrdiscipline_committee.employeeunkid = hremployee_master.employeeunkid " & _
                    "	LEFT JOIN hrdepartment_master ON hremployee_master.departmentunkid = hrdepartment_master.departmentunkid " & _
                    "WHERE isvoid = 0 AND hrdiscipline_investigators.committeetranunkid > 0 AND hrdiscipline_investigators.resolutionunkid = @resolutionunkid "

            objDataOp.AddParameter("@resolutionunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintResolutionUnkid.ToString)

            dsList = objDataOp.ExecQuery(StrQ, "List")

            If objDataOp.ErrorMessage <> "" Then
                exForce = New Exception(objDataOp.ErrorNumber & ": " & objDataOp.ErrorMessage)
                Throw exForce
            End If

            mdtTran.Rows.Clear()
            For Each dtRow As DataRow In dsList.Tables(0).Rows
                mdtTran.ImportRow(dtRow)
            Next

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Get_Investigators", mstrModuleName)
        Finally
        End Try
    End Sub

    Public Function InsertUpdateDelete_Investigators(ByVal objDataOperation As clsDataOperation) As Boolean
        Dim i As Integer
        Dim StrQ As String = ""
        Dim exForce As Exception
        Dim dsList As New DataSet
        Dim mintInvestigatorUnkId As Integer = -1
        Try
            For i = 0 To mdtTran.Rows.Count - 1
                With mdtTran.Rows(i)
                    objDataOperation.ClearParameters()
                    If Not IsDBNull(.Item("AUD")) Then
                        Select Case .Item("AUD")
                            Case "A"
                                StrQ = "INSERT INTO hrdiscipline_investigators ( " & _
                                        "  resolutionunkid " & _
                                        ", employeeunkid " & _
                                        ", ex_name " & _
                                        ", ex_company " & _
                                        ", ex_department " & _
                                        ", ex_contactno " & _
                                        ", investigator_remark " & _
                                        ", isvoid " & _
                                        ", voiduserunkid " & _
                                        ", voiddatetime " & _
                                        ", voidreason" & _
                                        ", committeetranunkid " & _
                                      ") VALUES (" & _
                                        "  @resolutionunkid " & _
                                        ", @employeeunkid " & _
                                        ", @ex_name " & _
                                        ", @ex_company " & _
                                        ", @ex_department " & _
                                        ", @ex_contactno " & _
                                        ", @investigator_remark " & _
                                        ", @isvoid " & _
                                        ", @voiduserunkid " & _
                                        ", @voiddatetime " & _
                                        ", @voidreason" & _
                                        ", @committeetranunkid " & _
                                      "); SELECT @@identity "

                                objDataOperation.AddParameter("@resolutionunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintResolutionUnkid.ToString)
                                objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("employeeunkid").ToString)

                                If .Item("committeetranunkid") <= 0 Then
                                objDataOperation.AddParameter("@ex_name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, .Item("ex_name").ToString)
                                objDataOperation.AddParameter("@ex_company", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, .Item("ex_company").ToString)
                                objDataOperation.AddParameter("@ex_department", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, .Item("ex_department").ToString)
                                objDataOperation.AddParameter("@ex_contactno", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, .Item("ex_contactno").ToString)
                                Else
                                    objDataOperation.AddParameter("@ex_name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, "")
                                    objDataOperation.AddParameter("@ex_company", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, "")
                                    objDataOperation.AddParameter("@ex_department", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, "")
                                    objDataOperation.AddParameter("@ex_contactno", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, "")
                                End If

                                objDataOperation.AddParameter("@investigator_remark", SqlDbType.VarChar, 8000, .Item("investigator_remark").ToString)
                                objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, .Item("isvoid").ToString)
                                objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("voiduserunkid").ToString)
                                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, .Item("voiddatetime"))
                                objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, .Item("voidreason").ToString)
                                objDataOperation.AddParameter("@committeetranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("committeetranunkid").ToString)

                                dsList = objDataOperation.ExecQuery(StrQ, "List")

                                If objDataOperation.ErrorMessage <> "" Then
                                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                    Throw exForce
                                End If

                                mintInvestigatorUnkId = dsList.Tables(0).Rows(0).Item(0)

                                If .Item("resolutionunkid") > 0 Then
                                    ''S.SANDEEP [28-May-2018] -- START
                                    ''ISSUE/ENHANCEMENT : {Audit Trails} 
                                    Dim objCommonATLog As New clsCommonATLog
                                    'objCommonATLog._FormName = mstrFormName 
                                    'objCommonATLog._LoginEmployeeUnkid = mintLoginEmployeeunkid 
                                    '                                    objCommonATLog._ClientIP = mstrClientIP
                                    '                                    objCommonATLog._HostName = mstrHostName
                                    '                                    objCommonATLog._FromWeb = mblnIsWeb
                                    '                                    objCommonATLog._AuditUserId = mintAuditUserId
                                    'objCommonATLog._CompanyUnkid = mintCompanyUnkid
                                    '                                    objCommonATLog._AuditDate = mdtAuditDate
                                    '                                    'S.SANDEEP [28-May-2018] -- END

                                    If objCommonATLog.Insert_TranAtLog(objDataOperation, "hrdiscipline_resolutions", "resolutionunkid", .Item("resolutionunkid"), "hrdiscipline_investigators", "investigatorunkid", mintInvestigatorUnkId, 2, 1) = False Then
                                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                        Throw exForce
                                    End If

                                    'S.SANDEEP [28-May-2018] -- START
                                    'ISSUE/ENHANCEMENT : {Audit Trails} 
                                    objCommonATLog = Nothing
                                    'S.SANDEEP [28-May-2018] -- END

                                Else
                                    'S.SANDEEP [28-May-2018] -- START
                                    'ISSUE/ENHANCEMENT : {Audit Trails} 
                                    Dim objCommonATLog As New clsCommonATLog
                                    '                                    objCommonATLog._FormName = mstrFormName
                                    '                                    objCommonATLog._LoginEmployeeUnkid = mintLoginEmployeeunkid
                                    '                                    objCommonATLog._ClientIP = mstrClientIP
                                    '                                    objCommonATLog._HostName = mstrHostName
                                    '                                    objCommonATLog._FromWeb = mblnIsWeb
                                    '                                    objCommonATLog._AuditUserId = mintAuditUserId
                                    'objCommonATLog._CompanyUnkid = mintCompanyUnkid
                                    '                                    objCommonATLog._AuditDate = mdtAuditDate
                                    'S.SANDEEP [28-May-2018] -- END

                                    If objCommonATLog.Insert_TranAtLog(objDataOperation, "hrdiscipline_resolutions", "resolutionunkid", mintResolutionUnkid, "hrdiscipline_investigators", "investigatorunkid", mintInvestigatorUnkId, 1, 1) = False Then
                                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                        Throw exForce
                                    End If

                                    'S.SANDEEP [28-May-2018] -- START
                                    'ISSUE/ENHANCEMENT : {Audit Trails} 
                                    objCommonATLog = Nothing
                                    'S.SANDEEP [28-May-2018] -- END

                                End If

                            Case "U"
                                StrQ = "UPDATE hrdiscipline_investigators SET " & _
                                        "  resolutionunkid = @resolutionunkid" & _
                                        ", employeeunkid = @employeeunkid" & _
                                        ", ex_name = @ex_name" & _
                                        ", ex_company = @ex_company" & _
                                        ", ex_department = @ex_department" & _
                                        ", ex_contactno = @ex_contactno" & _
                                        ", investigator_remark = @investigator_remark" & _
                                        ", isvoid = @isvoid" & _
                                        ", voiduserunkid = @voiduserunkid" & _
                                        ", voiddatetime = @voiddatetime" & _
                                        ", voidreason = @voidreason " & _
                                        ", committeetranunkid = @committeetranunkid " & _
                                      "WHERE investigatorunkid = @investigatorunkid "

                                objDataOperation.AddParameter("@investigatorunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("investigatorunkid").ToString)
                                objDataOperation.AddParameter("@resolutionunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("resolutionunkid").ToString)
                                objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("employeeunkid").ToString)

                                If .Item("committeetranunkid") <= 0 Then
                                objDataOperation.AddParameter("@ex_name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, .Item("ex_name").ToString)
                                objDataOperation.AddParameter("@ex_company", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, .Item("ex_company").ToString)
                                objDataOperation.AddParameter("@ex_department", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, .Item("ex_department").ToString)
                                objDataOperation.AddParameter("@ex_contactno", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, .Item("ex_contactno").ToString)
                                Else
                                    objDataOperation.AddParameter("@ex_name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, "")
                                    objDataOperation.AddParameter("@ex_company", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, "")
                                    objDataOperation.AddParameter("@ex_department", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, "")
                                    objDataOperation.AddParameter("@ex_contactno", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, "")
                                End If                                

                                objDataOperation.AddParameter("@investigator_remark", SqlDbType.VarChar, 8000, .Item("investigator_remark").ToString)
                                objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, .Item("isvoid").ToString)
                                objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("voiduserunkid").ToString)
                                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, .Item("voiddatetime"))
                                objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, .Item("voidreason").ToString)
                                objDataOperation.AddParameter("@committeetranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("committeetranunkid").ToString)

                                Call objDataOperation.ExecNonQuery(StrQ)

                                If objDataOperation.ErrorMessage <> "" Then
                                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                    Throw exForce
                                End If

                                'S.SANDEEP [28-May-2018] -- START
                                'ISSUE/ENHANCEMENT : {Audit Trails} 
                                Dim objCommonATLog As New clsCommonATLog
                                '                                objCommonATLog._FormName = mstrFormName
                                '                                objCommonATLog._LoginEmployeeUnkid = mintLoginEmployeeunkid
                                '                                objCommonATLog._ClientIP = mstrClientIP
                                '                                objCommonATLog._HostName = mstrHostName
                                '                                objCommonATLog._FromWeb = mblnIsWeb
                                '                                objCommonATLog._AuditUserId = mintAuditUserId
                                'objCommonATLog._CompanyUnkid = mintCompanyUnkid
                                '                                objCommonATLog._AuditDate = mdtAuditDate
                                'S.SANDEEP [28-May-2018] -- END

                                If objCommonATLog.Insert_TranAtLog(objDataOperation, "hrdiscipline_resolutions", "resolutionunkid", .Item("resolutionunkid"), "hrdiscipline_investigators", "investigatorunkid", .Item("investigatorunkid"), 2, 2) = False Then
                                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                    Throw exForce
                                End If

                                'S.SANDEEP [28-May-2018] -- START
                                'ISSUE/ENHANCEMENT : {Audit Trails} 
                                objCommonATLog = Nothing
                                'S.SANDEEP [28-May-2018] -- END


                            Case "D"

                                If .Item("investigatorunkid") > 0 Then
                                    'S.SANDEEP [28-May-2018] -- START
                                    'ISSUE/ENHANCEMENT : {Audit Trails} 
                                    Dim objCommonATLog As New clsCommonATLog
                                    '                                    objCommonATLog._FormName = mstrFormName
                                    '                                    objCommonATLog._LoginEmployeeUnkid = mintLoginEmployeeunkid
                                    '                                    objCommonATLog._ClientIP = mstrClientIP
                                    '                                    objCommonATLog._HostName = mstrHostName
                                    '                                    objCommonATLog._FromWeb = mblnIsWeb
                                    '                                    objCommonATLog._AuditUserId = mintAuditUserId
                                    'objCommonATLog._CompanyUnkid = mintCompanyUnkid
                                    '                                    objCommonATLog._AuditDate = mdtAuditDate
                                    'S.SANDEEP [28-May-2018] -- END

                                    If objCommonATLog.Insert_TranAtLog(objDataOperation, "hrdiscipline_resolutions", "resolutionunkid", .Item("resolutionunkid"), "hrdiscipline_investigators", "investigatorunkid", .Item("investigatorunkid"), 2, 3) = False Then
                                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                        Throw exForce
                                    End If

                                    'S.SANDEEP [28-May-2018] -- START
                                    'ISSUE/ENHANCEMENT : {Audit Trails} 
                                    objCommonATLog = Nothing
                                    'S.SANDEEP [28-May-2018] -- END

                                End If

                                StrQ = "UPDATE hrdiscipline_investigators SET " & _
                                          " isvoid = 1 " & _
                                          ",voiddatetime=@voiddatetime " & _
                                          ",voidreason = @voidreason " & _
                                          ",voiduserunkid = @voiduserunkid " & _
                                       "WHERE investigatorunkid = @investigatorunkid "

                                objDataOperation.AddParameter("@investigatorunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("investigatorunkid").ToString)
                                objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, .Item("isvoid").ToString)
                                objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("voiduserunkid").ToString)
                                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, .Item("voiddatetime"))
                                objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, .Item("voidreason").ToString)

                        End Select
                    End If
                End With
            Next

            Return True

        Catch ex As Exception
            Throw New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
        Finally
        End Try
    End Function

#End Region

End Class
