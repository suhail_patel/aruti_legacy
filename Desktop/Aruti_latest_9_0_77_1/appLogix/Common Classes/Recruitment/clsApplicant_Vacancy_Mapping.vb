﻿'************************************************************************************************************************************
'Class Name : clsApplicant_Vacancy_Mapping.vb
'Purpose    :
'Date       :28/02/2012
'Written By :Sandeep J. Sharma
'Modified   :
'************************************************************************************************************************************

Imports eZeeCommonLib
Imports System.ComponentModel

''' <summary>
''' Purpose: 
''' Developer: Sandeep J. Sharma
''' </summary>
Public Class clsApplicant_Vacancy_Mapping
    Private Const mstrModuleName = "clsApplicant_Vacancy_Mapping"
    Dim objDataOperation As clsDataOperation
    Dim mstrMessage As String = ""

#Region " Private variables "
    Private mintAppvacancytranunkid As Integer
    Private mintApplicantunkid As Integer
    Private mintVacancyunkid As Integer
    Private mintUserunkid As Integer
    Private mblnIsactive As Boolean = True
    'S.SANDEEP [ 14 May 2013 ] -- START
    'ENHANCEMENT : TRA ENHANCEMENT
    Private mintImportuserunkid As Integer = 0
    Private mdtImportdatetime As DateTime = Nothing
    Private mblnIsimport As Boolean = False
    'S.SANDEEP [ 14 May 2013 ] -- END
    'Nilay (13 Apr 2017) -- Start
    Private mdtEarliestPossibleStartDate As Date = Nothing
    Private mstrComments As String = String.Empty
    Private mstrVacancyFoundOutFrom As String = String.Empty
    'Nilay (13 Apr 2017) -- End
    'Sohail (27 Sep 2019) -- Start
    'NMB Enhancement # TC029: On the recruitment portal, Vacancy found out from section should be a drop down menu and the items on the drop down should be definable in a master.
    Private mintVacancyFoundOutFromUnkiId As Integer = 0
    'Sohail (27 Sep 2019) -- End

    'Sohail (09 Oct 2018) -- Start
    'TANAPA - Support Issue Id # 2502 - linking recruitment server app_vacancy with aruti desktop app_vacancy in 75.1.
    Private mblnIsFromOnline As Boolean = False
    Private mintServerappvacancytranunkid As Integer = 0
    Private mblnIsvoid As Boolean
    Private mintVoiduserunkid As Integer
    Private mdtVoiddatetime As Date
    Private mstrVoidreason As String = String.Empty
    Private mdtTran As DataTable
    'Sohail (09 Oct 2018) -- End

#End Region

#Region " Properties "
'S.SANDEEP [28-May-2018] -- START 
'ISSUE/ENHANCEMENT : {Audit Trails} 
Private mstrFormName As String = String.Empty
Public WriteOnly Property _FormName() As String 
Set(ByVal value As String) 
mstrFormName = value 
End Set 
End Property 
 
Private mstrClientIP As String = ""
Public WriteOnly Property _ClientIP() As String 
Set(ByVal value As String) 
mstrClientIP = value 
End Set 
End Property 
 
Private mstrHostName As String = ""
Public WriteOnly Property _HostName() As String 
Set(ByVal value As String) 
mstrHostName = value 
End Set 
End Property 
 
Private mblnIsWeb As Boolean = False
Public WriteOnly Property _FromWeb() As Boolean 
Set(ByVal value As Boolean) 
mblnIsWeb = value 
End Set 
End Property 
 
    Private mintAuditUserId As Integer = 0
    Public WriteOnly Property _AuditUserId() As Integer
        Set(ByVal value As Integer)
            mintAuditUserId = value
        End Set
    End Property

    Private mintCompanyUnkid As Integer = 0
    Public WriteOnly Property _CompanyUnkid() As Integer
        Set(ByVal value As Integer)
            mintCompanyUnkid = value
        End Set
    End Property
 
Private mdtAuditDate As DateTime = Now
Public WriteOnly Property _AuditDate() As DateTime 
Set(ByVal value As DateTime) 
mdtAuditDate = value 
End Set 
End Property 
 
Private xDataOpr As clsDataOperation = Nothing
Public Property _xDataOpr() As clsDataOperation 
Get 
Return xDataOpr 
End Get 
Set(ByVal value As clsDataOperation) 
xDataOpr = value 
End Set 
End Property 
 
Private mintVoidlogingemployeeunkid As Integer=0
Public Property _Voidloginemployeeunkid() As Integer 
Get 
Return mintVoidlogingemployeeunkid 
End Get 
Set(ByVal value As Integer) 
mintVoidlogingemployeeunkid = value 
End Set 
End Property 
'S.SANDEEP [28-May-2018] -- END 

    ''' <summary>
    ''' Purpose: Get Message from Class 
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public ReadOnly Property _Message() As String
        Get
            Return mstrMessage
        End Get
    End Property

    ''' <summary>
    ''' Purpose: Get or Set appvacancytranunkid
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Appvacancytranunkid() As Integer
        Get
            Return mintAppvacancytranunkid
        End Get
        Set(ByVal value As Integer)
            mintAppvacancytranunkid = value
            Call GetData()
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set applicantunkid
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Applicantunkid() As Integer
        Get
            Return mintApplicantunkid
        End Get
        Set(ByVal value As Integer)
            mintApplicantunkid = value
            Call GetApplicantVacancyMapping() 'Sohail (09 Oct 2018)
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set vacancyunkid
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Vacancyunkid() As Integer
        Get
            Return mintVacancyunkid
        End Get
        Set(ByVal value As Integer)
            mintVacancyunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set userunkid
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Userunkid() As Integer
        Get
            Return mintUserunkid
        End Get
        Set(ByVal value As Integer)
            mintUserunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isactive
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Isactive() As Boolean
        Get
            Return mblnIsactive
        End Get
        Set(ByVal value As Boolean)
            mblnIsactive = value
        End Set
    End Property

    'S.SANDEEP [ 14 May 2013 ] -- START
    'ENHANCEMENT : TRA ENHANCEMENT
    ''' <summary>
    ''' Purpose: Get or Set importuserunkid
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Importuserunkid() As Integer
        Get
            Return mintImportuserunkid
        End Get
        Set(ByVal value As Integer)
            mintImportuserunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set importdatetime
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Importdatetime() As Date
        Get
            Return mdtImportdatetime
        End Get
        Set(ByVal value As Date)
            mdtImportdatetime = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isimport
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Isimport() As Boolean
        Get
            Return mblnIsimport
        End Get
        Set(ByVal value As Boolean)
            mblnIsimport = value
        End Set
    End Property
    'S.SANDEEP [ 14 May 2013 ] -- END

    'Nilay (13 Apr 2017) -- Start
    Public Property _EarliestPossibleStartDate() As Date
        Get
            Return mdtEarliestPossibleStartDate
        End Get
        Set(ByVal value As Date)
            mdtEarliestPossibleStartDate = value
        End Set
    End Property

    Public Property _Comments() As String
        Get
            Return mstrComments
        End Get
        Set(ByVal value As String)
            mstrComments = value
        End Set
    End Property

    Public Property _VacancyFoundOutFrom() As String
        Get
            Return mstrVacancyFoundOutFrom
        End Get
        Set(ByVal value As String)
            mstrVacancyFoundOutFrom = value
        End Set
    End Property
    'Nilay (13 Apr 2017) -- End

    'Sohail (27 Sep 2019) -- Start
    'NMB Enhancement # TC029: On the recruitment portal, Vacancy found out from section should be a drop down menu and the items on the drop down should be definable in a master.
    Public Property _VacancyFoundOutFromUnkId() As Integer
        Get
            Return mintVacancyFoundOutFromUnkiId
        End Get
        Set(ByVal value As Integer)
            mintVacancyFoundOutFromUnkiId = value
        End Set
    End Property
    'Sohail (27 Sep 2019) -- End

    'Sohail (09 Oct 2018) -- Start
    'TANAPA - Support Issue Id # 2502 - linking recruitment server app_vacancy with aruti desktop app_vacancy in 75.1.
    Public Property _IsFromOnline() As Boolean
        Get
            Return mblnIsFromOnline
        End Get
        Set(ByVal value As Boolean)
            mblnIsFromOnline = value
        End Set
    End Property

    Public Property _Serverappvacancytranunkid() As Integer
        Get
            Return mintServerappvacancytranunkid
        End Get
        Set(ByVal value As Integer)
            mintServerappvacancytranunkid = value
        End Set
    End Property

    Public Property _Isvoid() As Boolean
        Get
            Return mblnIsvoid
        End Get
        Set(ByVal value As Boolean)
            mblnIsvoid = value
        End Set
    End Property

    Public Property _Voiduserunkid() As Integer
        Get
            Return mintVoiduserunkid
        End Get
        Set(ByVal value As Integer)
            mintVoiduserunkid = value
        End Set
    End Property

    Public Property _Voiddatetime() As Date
        Get
            Return mdtVoiddatetime
        End Get
        Set(ByVal value As Date)
            mdtVoiddatetime = value
        End Set
    End Property

    Public Property _Voidreason() As String
        Get
            Return mstrVoidreason
        End Get
        Set(ByVal value As String)
            mstrVoidreason = value
        End Set
    End Property

    Public Property _DataList() As DataTable
        Get
            Return mdtTran
        End Get
        Set(ByVal value As DataTable)
            mdtTran = value
        End Set
    End Property
    'Sohail (09 Oct 2018) -- End

    'Sohail (25 Sep 2020) -- Start
    'NMB Enhancement : OLD-76 #  : Process of linking employee with an applicant, bind vacancy to employee and send notification to reporting to on applying vacancy.
    Private mintLoginemployeeunkid As Integer
    Public Property _Loginemployeeunkid() As Integer
        Get
            Return mintLoginemployeeunkid
        End Get
        Set(ByVal value As Integer)
            mintLoginemployeeunkid = value
        End Set
    End Property
    'Sohail (25 Sep 2020) -- End

#End Region

    'Sohail (09 Oct 2018) -- Start
    'TANAPA - Support Issue Id # 2502 - linking recruitment server app_vacancy with aruti desktop app_vacancy in 75.1.
#Region " Constructor "
    Public Sub New()
        mdtTran = New DataTable("app_vacancy_mapping")
        Dim dCol As DataColumn
        Try

            mdtTran.Columns.Add("appvacancytranunkid", System.Type.GetType("System.Int32")).DefaultValue = 0
            mdtTran.Columns.Add("applicantunkid", System.Type.GetType("System.Int32")).DefaultValue = 0
            mdtTran.Columns.Add("vacancyunkid", System.Type.GetType("System.Int32")).DefaultValue = 0
            mdtTran.Columns.Add("importuserunkid", System.Type.GetType("System.Int32"))
            mdtTran.Columns.Add("importdatetime", System.Type.GetType("System.DateTime"))
            mdtTran.Columns.Add("isimport", Type.GetType("System.Boolean")).DefaultValue = False
            mdtTran.Columns.Add("earliest_possible_startdate", System.Type.GetType("System.DateTime"))
            mdtTran.Columns.Add("comments", System.Type.GetType("System.String")).DefaultValue = ""
            mdtTran.Columns.Add("vacancy_found_out_from", System.Type.GetType("System.String")).DefaultValue = ""
            mdtTran.Columns.Add("isfromonline", Type.GetType("System.Boolean")).DefaultValue = False
            'Sohail (27 Sep 2019) -- Start
            'NMB Enhancement # TC029: On the recruitment portal, Vacancy found out from section should be a drop down menu and the items on the drop down should be definable in a master.
            mdtTran.Columns.Add("vacancy_found_out_from_unkid", System.Type.GetType("System.Int32")).DefaultValue = 0
            'Sohail (27 Sep 2019) -- End
            'Sohail (25 Sep 2020) -- Start
            'NMB Enhancement : OLD-76 #  : Process of linking employee with an applicant, bind vacancy to employee and send notification to reporting to on applying vacancy.
            mdtTran.Columns.Add("loginemployeeunkid", System.Type.GetType("System.Int32")).DefaultValue = 0
            'Sohail (25 Sep 2020) -- End

            dCol = New DataColumn("AUD")
            dCol.DataType = System.Type.GetType("System.String")
            dCol.AllowDBNull = True
            dCol.DefaultValue = DBNull.Value
            mdtTran.Columns.Add(dCol)

            mdtTran.Columns.Add("GUID", System.Type.GetType("System.String"))

            mdtTran.Columns.Add("serverappvacancytranunkid", System.Type.GetType("System.Int32")).DefaultValue = 0
            mdtTran.Columns.Add("userunkid", System.Type.GetType("System.Int32")).DefaultValue = 0
            mdtTran.Columns.Add("isactive", Type.GetType("System.Boolean")).DefaultValue = True
            mdtTran.Columns.Add("isvoid", System.Type.GetType("System.Boolean")).DefaultValue = False
            mdtTran.Columns.Add("voiduserunkid", System.Type.GetType("System.Int32")).DefaultValue = 0
            mdtTran.Columns.Add("voiddatetime", System.Type.GetType("System.DateTime")).DefaultValue = Nothing
            mdtTran.Columns.Add("voidreason", System.Type.GetType("System.String")).DefaultValue = ""

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: New; Module Name: " & mstrModuleName)
        End Try
    End Sub
#End Region

#Region " Private Methods "
    Private Sub GetApplicantVacancyMapping()
        Dim strQ As String = ""
        Dim dsList As DataSet = Nothing
        Dim strErrorMessage As String = ""
        Dim dRowID_Tran As DataRow
        Dim exForce As Exception

        objDataOperation = New clsDataOperation
        Try
            strQ = "SELECT " & _
                    "   rcapp_vacancy_mapping.appvacancytranunkid " & _
                    "  ,rcapp_vacancy_mapping.applicantunkid " & _
                    "  ,rcapp_vacancy_mapping.vacancyunkid " & _
                    "  ,rcapp_vacancy_mapping.userunkid " & _
                    "  ,rcapp_vacancy_mapping.isactive " & _
                    "  ,ISNULL(rcapplicant_master.applicant_code,'') AS appcode " & _
                    "  ,ISNULL(rcapplicant_master.firstname,'')+' '+ISNULL(rcapplicant_master.othername,'')+' '+ISNULL(rcapplicant_master.surname,'') AS appname " & _
                    "  ,ISNULL(cfcommon_master.name,'') AS vacancy " & _
                    "  ,CONVERT(CHAR(8),openingdate,112) AS ODate " & _
                    "  ,CONVERT(CHAR(8),closingdate,112) AS CDate " & _
                    " ,rcapp_vacancy_mapping.importuserunkid " & _
                    " ,rcapp_vacancy_mapping.importdatetime " & _
                    "  ,ISNULL(rcapp_vacancy_mapping.isimport,0) AS isimport " & _
                    "  ,rcapp_vacancy_mapping.earliest_possible_startdate " & _
                    "  ,rcapp_vacancy_mapping.comments " & _
                    "  ,CASE WHEN ISNULL(rcapp_vacancy_mapping.vacancy_found_out_from_unkid, 0) > 0 THEN  VFF.name ELSE rcapp_vacancy_mapping.vacancy_found_out_from END AS vacancy_found_out_from " & _
                    ", ISNULL(rcapp_vacancy_mapping.isfromonline, 0) AS isfromonline " & _
                    ", '' As AUD " & _
                    ", ISNULL(rcapp_vacancy_mapping.serverappvacancytranunkid, 0) AS serverappvacancytranunkid " & _
                     ", rcapp_vacancy_mapping.isvoid " & _
                     ", rcapp_vacancy_mapping.voiduserunkid " & _
                     ", rcapp_vacancy_mapping.voiddatetime " & _
                     ", rcapp_vacancy_mapping.voidreason " & _
                     ", ISNULL(rcapp_vacancy_mapping.loginemployeeunkid, 0) AS loginemployeeunkid " & _
                    "FROM rcapp_vacancy_mapping " & _
                    "  JOIN rcvacancy_master ON rcapp_vacancy_mapping.vacancyunkid = rcvacancy_master.vacancyunkid " & _
                    "  JOIN cfcommon_master ON cfcommon_master.masterunkid = rcvacancy_master.vacancytitle AND cfcommon_master.mastertype = 34 " & _
                    "  JOIN rcapplicant_master ON rcapp_vacancy_mapping.applicantunkid = rcapplicant_master.applicantunkid " & _
                    "  LEFT JOIN cfcommon_master AS VFF ON VFF.masterunkid = rcapp_vacancy_mapping.vacancy_found_out_from_unkid AND VFF.mastertype = " & clsCommon_Master.enCommonMaster.VACANCY_SOURCE & " " & _
                    "WHERE ISNULL(rcapp_vacancy_mapping.isvoid, 0) = 0 AND rcapp_vacancy_mapping.applicantunkid = @applicantunkid "
            'Sohail (25 Sep 2020) - [loginemployeeunkid]
            'Sohail (27 Sep 2019) - [rcapp_vacancy_mapping.vacancy_found_out_from]=[CASE WHEN ISNULL(rcapp_vacancy_mapping.vacancy_found_out_from_unkid, 0) > 0 THEN  VFF.name ELSE rcapp_vacancy_mapping.vacancy_found_out_from END AS vacancy_found_out_from], [LEFT JOIN cfcommon_master AS VFF]

            objDataOperation.AddParameter("@applicantunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintApplicantunkid.ToString)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            mdtTran.Rows.Clear()

            For i As Integer = 0 To dsList.Tables(0).Rows.Count - 1
                With dsList.Tables(0).Rows(i)
                    dRowID_Tran = mdtTran.NewRow
                    dRowID_Tran.Item("appvacancytranunkid") = .Item("appvacancytranunkid")
                    dRowID_Tran.Item("applicantunkid") = .Item("applicantunkid")
                    dRowID_Tran.Item("vacancyunkid") = .Item("vacancyunkid")
                    dRowID_Tran.Item("importuserunkid") = .Item("importuserunkid")
                    dRowID_Tran.Item("isimport") = .Item("isimport")
                    dRowID_Tran.Item("comments") = .Item("comments")
                    dRowID_Tran.Item("vacancy_found_out_from") = .Item("vacancy_found_out_from")
                    dRowID_Tran.Item("isfromonline") = .Item("isfromonline")
                    If IsDBNull(.Item("importdatetime")) = True Then
                        dRowID_Tran.Item("importdatetime") = DBNull.Value
                    Else
                        dRowID_Tran.Item("importdatetime") = .Item("importdatetime")
                    End If

                    If IsDBNull(.Item("earliest_possible_startdate")) = True Then
                        dRowID_Tran.Item("earliest_possible_startdate") = DBNull.Value
                    Else
                        dRowID_Tran.Item("earliest_possible_startdate") = .Item("earliest_possible_startdate")
                    End If

                    dRowID_Tran.Item("userunkid") = .Item("userunkid")
                    dRowID_Tran.Item("isactive") = .Item("isactive")
                    dRowID_Tran.Item("AUD") = .Item("AUD").ToString
                    dRowID_Tran.Item("serverappvacancytranunkid") = .Item("serverappvacancytranunkid")
                    dRowID_Tran.Item("isvoid") = .Item("isvoid")
                    dRowID_Tran.Item("voiduserunkid") = .Item("voiduserunkid")
                    If IsDBNull(.Item("voiddatetime")) = True Then
                        dRowID_Tran.Item("voiddatetime") = DBNull.Value
                    Else
                        dRowID_Tran.Item("voiddatetime") = .Item("voiddatetime")
                    End If
                    dRowID_Tran.Item("voidreason") = .Item("voidreason")
                    dRowID_Tran.Item("loginemployeeunkid") = .Item("loginemployeeunkid") 'Sohail (25 Sep 2020)
                    mdtTran.Rows.Add(dRowID_Tran)
                End With
            Next
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetApplicantVacancyMapping; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Sub

    Public Function InsertUpdateDelete_AppVacancyMapping() As Boolean
        Dim i As Integer
        Dim strQ As String = ""
        Dim strErrorMessage As String = ""
        Dim exForce As Exception

        Try
            objDataOperation = New clsDataOperation
            objDataOperation.BindTransaction()

            For i = 0 To mdtTran.Rows.Count - 1
                With mdtTran.Rows(i)
                    objDataOperation.ClearParameters()

                    If Not IsDBNull(.Item("AUD")) Then
                        Select Case .Item("AUD")
                            Case "A"

                                strQ = "INSERT INTO rcapp_vacancy_mapping ( " & _
                                                "  applicantunkid " & _
                                                ", vacancyunkid " & _
                                                ", userunkid " & _
                                                ", isactive" & _
                                                ", importuserunkid " & _
                                                ", importdatetime " & _
                                                ", isimport" & _
                                                ", earliest_possible_startdate " & _
                                                ", comments " & _
                                                ", vacancy_found_out_from " & _
                                                ", isfromonline " & _
                                                ", serverappvacancytranunkid " & _
                                                ", isvoid " & _
                                                ", voiduserunkid " & _
                                                ", voiddatetime " & _
                                                ", voidreason " & _
                                                ", vacancy_found_out_from_unkid " & _
                                                ", loginemployeeunkid " & _
                                        ") VALUES (" & _
                                                "  @applicantunkid " & _
                                                ", @vacancyunkid " & _
                                                ", @userunkid " & _
                                                ", @isactive" & _
                                                ", @importuserunkid " & _
                                                ", @importdatetime " & _
                                                ", @isimport" & _
                                                ", @earliest_possible_startdate " & _
                                                ", @comments " & _
                                                ", @vacancy_found_out_from " & _
                                                ", @isfromonline " & _
                                                ", @serverappvacancytranunkid " & _
                                                ", @isvoid " & _
                                                ", @voiduserunkid " & _
                                                ", @voiddatetime " & _
                                                ", @voidreason " & _
                                                ", @vacancy_found_out_from_unkid " & _
                                                ", @loginemployeeunkid " & _
                                        "); SELECT @@identity "
                                'Sohail (25 Sep 2020) - [loginemployeeunkid]
                                'Sohail (27 Sep 2019) - [vacancy_found_out_from_unkid]

                                objDataOperation.AddParameter("@applicantunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintApplicantunkid.ToString)
                                objDataOperation.AddParameter("@vacancyunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("vacancyunkid"))
                                objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("userunkid"))
                                objDataOperation.AddParameter("@isactive", SqlDbType.Bit, eZeeDataType.BIT_SIZE, .Item("isactive"))
                                objDataOperation.AddParameter("@isimport", SqlDbType.Bit, eZeeDataType.BIT_SIZE, .Item("isimport"))
                                objDataOperation.AddParameter("@importuserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("importuserunkid"))
                                If IsDBNull(.Item("importdatetime")) = False Then
                                    objDataOperation.AddParameter("@importdatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, .Item("importdatetime"))
                                Else
                                    objDataOperation.AddParameter("@importdatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
                                End If

                                If IsDBNull(.Item("earliest_possible_startdate")) = False Then
                                    objDataOperation.AddParameter("@earliest_possible_startdate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, .Item("earliest_possible_startdate"))
                                Else
                                    objDataOperation.AddParameter("@earliest_possible_startdate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
                                End If
                                objDataOperation.AddParameter("@comments", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, .Item("comments"))
                                objDataOperation.AddParameter("@vacancy_found_out_from", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, .Item("vacancy_found_out_from"))
                                objDataOperation.AddParameter("@isfromonline", SqlDbType.Bit, eZeeDataType.BIT_SIZE, .Item("isfromonline"))
                                If IsDBNull(.Item("serverappvacancytranunkid")) = False Then
                                    objDataOperation.AddParameter("@serverappvacancytranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("serverappvacancytranunkid"))
                                Else
                                    objDataOperation.AddParameter("@serverappvacancytranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, DBNull.Value)
                                End If
                                objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, .Item("isvoid"))
                                objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("voiduserunkid"))
                                If IsDBNull(.Item("voiddatetime")) = False Then
                                    objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, .Item("voiddatetime"))
                                Else
                                    objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
                                End If
                                objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, .Item("voidreason"))
                                'Sohail (27 Sep 2019) -- Start
                                'NMB Enhancement # TC029: On the recruitment portal, Vacancy found out from section should be a drop down menu and the items on the drop down should be definable in a master.
                                objDataOperation.AddParameter("@vacancy_found_out_from_unkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("vacancy_found_out_from_unkid"))
                                'Sohail (27 Sep 2019) -- End
                                'Sohail (25 Sep 2020) -- Start
                                'NMB Enhancement : OLD-76 #  : Process of linking employee with an applicant, bind vacancy to employee and send notification to reporting to on applying vacancy.
                                objDataOperation.AddParameter("@loginemployeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("loginemployeeunkid"))
                                'Sohail (25 Sep 2020) -- End

                                Dim dsList As DataSet = objDataOperation.ExecQuery(strQ, "List")

                                If objDataOperation.ErrorMessage <> "" Then
                                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                    Throw exForce
                                End If


                                mintAppvacancytranunkid = dsList.Tables(0).Rows(0)(0)

                                Dim objCommonATLog As New clsCommonATLog
                                objCommonATLog._FormName = mstrFormName
                                objCommonATLog._LoginEmployeeUnkid = mintLoginEmployeeunkid
                                objCommonATLog._ClientIP = mstrClientIP
                                objCommonATLog._HostName = mstrHostName
                                objCommonATLog._FromWeb = mblnIsWeb
                                objCommonATLog._AuditUserId = mintAuditUserId
                                objCommonATLog._CompanyUnkid = mintCompanyUnkid
                                objCommonATLog._AuditDate = mdtAuditDate

                                If .Item("applicantunkid") > 0 Then
                                    If objCommonATLog.Insert_TranAtLog(objDataOperation, "rcapplicant_master", "applicantunkid", mintApplicantunkid, "rcapp_vacancy_mapping", "appvacancytranunkid", mintAppvacancytranunkid, 2, 1) = False Then
                                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                        Throw exForce
                                    End If
                                Else
                                    If objCommonATLog.Insert_TranAtLog(objDataOperation, "rcapplicant_master", "applicantunkid", mintApplicantunkid, "rcapp_vacancy_mapping", "appvacancytranunkid", mintAppvacancytranunkid, 1, 1) = False Then
                                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                        Throw exForce
                                    End If
                                End If
                                objCommonATLog = Nothing

                            Case "U"
                                objDataOperation.AddParameter("@appvacancytranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("appvacancytranunkid").ToString)
                                objDataOperation.AddParameter("@applicantunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintApplicantunkid.ToString)
                                objDataOperation.AddParameter("@vacancyunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("vacancyunkid"))
                                objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("userunkid"))
                                objDataOperation.AddParameter("@isactive", SqlDbType.Bit, eZeeDataType.BIT_SIZE, .Item("isactive"))
                                objDataOperation.AddParameter("@isimport", SqlDbType.Bit, eZeeDataType.BIT_SIZE, .Item("isimport"))
                                objDataOperation.AddParameter("@importuserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("importuserunkid"))
                                If IsDBNull(.Item("importdatetime")) = False Then
                                    objDataOperation.AddParameter("@importdatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, .Item("importdatetime"))
                                Else
                                    objDataOperation.AddParameter("@importdatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
                                End If

                                If IsDBNull(.Item("earliest_possible_startdate")) = False Then
                                    objDataOperation.AddParameter("@earliest_possible_startdate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, .Item("earliest_possible_startdate"))
                                Else
                                    objDataOperation.AddParameter("@earliest_possible_startdate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
                                End If
                                objDataOperation.AddParameter("@comments", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, .Item("comments"))
                                objDataOperation.AddParameter("@vacancy_found_out_from", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, .Item("vacancy_found_out_from"))
                                objDataOperation.AddParameter("@isfromonline", SqlDbType.Bit, eZeeDataType.BIT_SIZE, .Item("isfromonline"))
                                If IsDBNull(.Item("serverappvacancytranunkid")) = False Then
                                    objDataOperation.AddParameter("@serverappvacancytranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("serverappvacancytranunkid"))
                                Else
                                    objDataOperation.AddParameter("@serverappvacancytranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, DBNull.Value)
                                End If
                                objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, .Item("isvoid"))
                                objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("voiduserunkid"))
                                If IsDBNull(.Item("voiddatetime")) = False Then
                                    objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, .Item("voiddatetime"))
                                Else
                                    objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
                                End If
                                objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, .Item("voidreason"))
                                'Sohail (27 Sep 2019) -- Start
                                'NMB Enhancement # TC029: On the recruitment portal, Vacancy found out from section should be a drop down menu and the items on the drop down should be definable in a master.
                                objDataOperation.AddParameter("@vacancy_found_out_from_unkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("vacancy_found_out_from_unkid"))
                                'Sohail (27 Sep 2019) -- End
                                'Sohail (25 Sep 2020) -- Start
                                'NMB Enhancement : OLD-76 #  : Process of linking employee with an applicant, bind vacancy to employee and send notification to reporting to on applying vacancy.
                                objDataOperation.AddParameter("@loginemployeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("loginemployeeunkid"))
                                'Sohail (25 Sep 2020) -- End

                                strQ = "UPDATE rcapp_vacancy_mapping SET " & _
                                            "  applicantunkid = @applicantunkid" & _
                                            ", vacancyunkid = @vacancyunkid" & _
                                            ", userunkid = @userunkid" & _
                                            ", isactive = @isactive " & _
                                            ", importuserunkid = @importuserunkid " & _
                                            ", importdatetime = @importdatetime " & _
                                            ", isimport = @isimport " & _
                                            ", earliest_possible_startdate = @earliest_possible_startdate " & _
                                            ", comments = @comments " & _
                                            ", vacancy_found_out_from = @vacancy_found_out_from " & _
                                            ", isfromonline = @isfromonline " & _
                                            ", serverappvacancytranunkid = @serverappvacancytranunkid " & _
                                            ", isvoid = @isvoid " & _
                                            ", voiduserunkid = @voiduserunkid " & _
                                            ", voiddatetime = @voiddatetime " & _
                                            ", voidreason = @voidreason " & _
                                            ", vacancy_found_out_from_unkid = @vacancy_found_out_from_unkid " & _
                                            ", loginemployeeunkid = @loginemployeeunkid " & _
                                       "WHERE appvacancytranunkid = @appvacancytranunkid "
                                'Sohail (27 Sep 2019) - [vacancy_found_out_from_unkid]

                                Call objDataOperation.ExecNonQuery(strQ)

                                If objDataOperation.ErrorMessage <> "" Then
                                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                    Throw exForce
                                End If

                                Dim objCommonATLog As New clsCommonATLog
                                objCommonATLog._FormName = mstrFormName
                                objCommonATLog._LoginEmployeeUnkid = mintLoginEmployeeunkid
                                objCommonATLog._ClientIP = mstrClientIP
                                objCommonATLog._HostName = mstrHostName
                                objCommonATLog._FromWeb = mblnIsWeb
                                objCommonATLog._AuditUserId = mintAuditUserId
                                objCommonATLog._CompanyUnkid = mintCompanyUnkid
                                objCommonATLog._AuditDate = mdtAuditDate

                                If objCommonATLog.Insert_TranAtLog(objDataOperation, "rcapplicant_master", "applicantunkid", mintApplicantunkid, "rcapp_vacancy_mapping", "appvacancytranunkid", .Item("appvacancytranunkid"), 2, 2) = False Then
                                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                    Throw exForce
                                End If
                                objCommonATLog = Nothing

                            Case "D"


                                If .Item("appvacancytranunkid") > 0 Then
                                    Dim objCommonATLog As New clsCommonATLog
                                    objCommonATLog._FormName = mstrFormName
                                    objCommonATLog._LoginEmployeeUnkid = mintLoginEmployeeunkid
                                    objCommonATLog._ClientIP = mstrClientIP
                                    objCommonATLog._HostName = mstrHostName
                                    objCommonATLog._FromWeb = mblnIsWeb
                                    objCommonATLog._AuditUserId = mintAuditUserId
                                    objCommonATLog._CompanyUnkid = mintCompanyUnkid
                                    objCommonATLog._AuditDate = mdtAuditDate
                                    If objCommonATLog.Insert_TranAtLog(objDataOperation, "rcapplicant_master", "applicantunkid", mintApplicantunkid, "rcapp_vacancy_mapping", "appvacancytranunkid", .Item("appvacancytranunkid"), 2, 3) = False Then
                                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                        Throw exForce
                                    End If
                                    objCommonATLog = Nothing
                                End If

                                strQ = "UPDATE rcapp_vacancy_mapping SET " & _
                                            "  isvoid = @isvoid " & _
                                            ", voiduserunkid = @voiduserunkid " & _
                                            ", voiddatetime = @voiddatetime " & _
                                            ", voidreason = @voidreason " & _
                                       "WHERE appvacancytranunkid = @appvacancytranunkid "

                                objDataOperation.AddParameter("@appvacancytranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("appvacancytranunkid").ToString)
                                objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, .Item("isvoid"))
                                objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("voiduserunkid"))
                                If IsDBNull(.Item("voiddatetime")) = False Then
                                    objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, .Item("voiddatetime"))
                                Else
                                    objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
                                End If
                                objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, .Item("voidreason"))

                                Call objDataOperation.ExecNonQuery(strQ)

                                If objDataOperation.ErrorMessage <> "" Then
                                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                    Throw exForce
                                End If
                        End Select
                    End If
                End With
            Next
            objDataOperation.ReleaseTransaction(True)
            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: InsertUpdateDelete_AppVacancyMapping; Module Name: " & mstrModuleName)
            Return False
        End Try
    End Function
#End Region
    'Sohail (09 Oct 2018) -- End

    ''' <summary>
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Sub GetData()
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "SELECT " & _
              "  appvacancytranunkid " & _
              ", applicantunkid " & _
              ", vacancyunkid " & _
              ", userunkid " & _
              ", isactive " & _
              ", ISNULL(importuserunkid,0) AS importuserunkid " & _
              ", importdatetime " & _
              ", ISNULL(isimport,0) AS isimport " & _
              ", earliest_possible_startdate " & _
              ", ISNULL(comments,'') AS comments " & _
              ", ISNULL(vacancy_found_out_from,'') AS vacancy_found_out_from " & _
                    ", ISNULL(isfromonline, 0) AS isfromonline " & _
                    ", ISNULL(serverappvacancytranunkid, 0) AS serverappvacancytranunkid " & _
                    ", isvoid " & _
                    ", voiduserunkid " & _
                    ", voiddatetime " & _
                    ", voidreason " & _
                    ", ISNULL(vacancy_found_out_from_unkid,'') AS vacancy_found_out_from_unkid " & _
                    ", ISNULL(loginemployeeunkid,0) AS loginemployeeunkid  " & _
             "FROM rcapp_vacancy_mapping " & _
             "WHERE appvacancytranunkid = @appvacancytranunkid "
            'Sohail (25 Sep 2020) - [loginemployeeunkid]
            'Sohail (27 Sep 2019) - [vacancy_found_out_from_unkid]
            'Sohail (09 Oct 2018) - [isfromonline, serverappvacancytranunkid, isvoid, voiduserunkid, voiddatetime, voidreason]
            'S.SANDEEP [ 14 May 2013 {isimport,importuserunkid,importdatetime}] -- START -- END
            'Nilay (13 Apr 2017) -- [earliest_possible_startdate, comments, vacancy_found_out_from]

            objDataOperation.AddParameter("@appvacancytranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAppvacancytranunkid.ToString)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For Each dtRow As DataRow In dsList.Tables(0).Rows
                mintAppvacancytranunkid = CInt(dtRow.Item("appvacancytranunkid"))
                mintApplicantunkid = CInt(dtRow.Item("applicantunkid"))
                mintVacancyunkid = CInt(dtRow.Item("vacancyunkid"))
                mintUserunkid = CInt(dtRow.Item("userunkid"))
                mblnIsactive = CBool(dtRow.Item("isactive"))
                'S.SANDEEP [ 14 May 2013 ] -- START
                'ENHANCEMENT : TRA ENHANCEMENT
                mintImportuserunkid = CInt(dtRow.Item("importuserunkid"))
                If IsDBNull(dtRow.Item("importdatetime")) Then
                    mdtImportdatetime = Nothing
                Else
                    mdtImportdatetime = dtRow.Item("importdatetime")
                End If
                mblnIsimport = CBool(dtRow.Item("isimport"))
                'S.SANDEEP [ 14 May 2013 ] -- END

                'Nilay (13 Apr 2017) -- Start
                If IsDBNull(dtRow.Item("earliest_possible_startdate")) Then
                    mdtEarliestPossibleStartDate = Nothing
                Else
                    mdtEarliestPossibleStartDate = dtRow.Item("earliest_possible_startdate")
                End If
                mstrComments = dtRow.Item("comments").ToString
                mstrVacancyFoundOutFrom = dtRow.Item("vacancy_found_out_from").ToString
                'Sohail (27 Sep 2019) -- Start
                'NMB Enhancement # TC029: On the recruitment portal, Vacancy found out from section should be a drop down menu and the items on the drop down should be definable in a master.
                mintVacancyFoundOutFromUnkiId = CInt(dtRow.Item("vacancy_found_out_from_unkid"))
                'Sohail (27 Sep 2019) -- End
                'Nilay (13 Apr 2017) -- End
                'Sohail (09 Oct 2018) -- Start
                'TANAPA - Support Issue Id # 2502 - linking recruitment server app_vacancy with aruti desktop app_vacancy in 75.1.
                mblnIsFromOnline = CBool(dtRow.Item("isfromonline"))
                If IsDBNull(dtRow.Item("serverappvacancytranunkid")) Then
                    mintServerappvacancytranunkid = Nothing
                Else
                    mintServerappvacancytranunkid = dtRow.Item("serverappvacancytranunkid")
                End If
                mblnIsvoid = dtRow.Item("isvoid")
                mintVoiduserunkid = dtRow.Item("voiduserunkid")
                If IsDBNull(dtRow.Item("voiddatetime")) Then
                    mdtVoiddatetime = Nothing
                Else
                    mdtVoiddatetime = dtRow.Item("voiddatetime")
                End If
                mstrVoidreason = dtRow.Item("voidreason")
                'Sohail (09 Oct 2018) -- End
                'Sohail (25 Sep 2020) -- Start
                'NMB Enhancement : OLD-76 #  : Process of linking employee with an applicant, bind vacancy to employee and send notification to reporting to on applying vacancy.
                mintLoginemployeeunkid = CInt(dtRow.Item("loginemployeeunkid"))
                'Sohail (25 Sep 2020) -- End

                Exit For
            Next
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetData; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Sub

    ''' <summary>
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function GetList(ByVal strTableName As String, Optional ByVal intApplicantId As Integer = -1) As DataSet
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "SELECT " & _
                   "   rcapp_vacancy_mapping.appvacancytranunkid " & _
                   "  ,rcapp_vacancy_mapping.applicantunkid " & _
                   "  ,rcapp_vacancy_mapping.vacancyunkid " & _
                   "  ,rcapp_vacancy_mapping.userunkid " & _
                   "  ,rcapp_vacancy_mapping.isactive " & _
                   "  ,ISNULL(rcapplicant_master.applicant_code,'') AS appcode " & _
                   "  ,ISNULL(rcapplicant_master.firstname,'')+' '+ISNULL(rcapplicant_master.othername,'')+' '+ISNULL(rcapplicant_master.surname,'') AS appname " & _
                   "  ,ISNULL(cfcommon_master.name,'') AS vacancy " & _
                   "  ,CONVERT(CHAR(8),openingdate,112) AS ODate " & _
                   "  ,CONVERT(CHAR(8),closingdate,112) AS CDate " & _
                   " ,rcapp_vacancy_mapping.importuserunkid " & _
                   " ,rcapp_vacancy_mapping.importdatetime " & _
                   "  ,ISNULL(rcapp_vacancy_mapping.isimport,0) AS isimport " & _
                   "  ,CONVERT(CHAR(8),rcapp_vacancy_mapping.earliest_possible_startdate,112) AS earliest_possible_startdate " & _
                   "  ,rcapp_vacancy_mapping.comments " & _
                   "  ,CASE WHEN ISNULL(rcapp_vacancy_mapping.vacancy_found_out_from_unkid, 0) > 0 THEN  VFF.name ELSE rcapp_vacancy_mapping.vacancy_found_out_from END AS vacancy_found_out_from " & _
                   "  ,ISNULL(rcapp_vacancy_mapping.isfromonline, 0) AS isfromonline " & _
                   ", ISNULL(rcapp_vacancy_mapping.serverappvacancytranunkid, 0) AS serverappvacancytranunkid " & _
                    ", rcapp_vacancy_mapping.isvoid " & _
                    ", rcapp_vacancy_mapping.voiduserunkid " & _
                    ", rcapp_vacancy_mapping.voiddatetime " & _
                    ", rcapp_vacancy_mapping.voidreason " & _
                    ", ISNULL(rcapp_vacancy_mapping.loginemployeeunkid,0) AS loginemployeeunkid  " & _
                   "FROM rcapp_vacancy_mapping " & _
                   "  JOIN rcvacancy_master ON rcapp_vacancy_mapping.vacancyunkid = rcvacancy_master.vacancyunkid " & _
                   "  JOIN cfcommon_master ON cfcommon_master.masterunkid = rcvacancy_master.vacancytitle AND cfcommon_master.mastertype = " & CInt(clsCommon_Master.enCommonMaster.VACANCY_MASTER) & " " & _
                   "  JOIN rcapplicant_master ON rcapp_vacancy_mapping.applicantunkid = rcapplicant_master.applicantunkid " & _
                   "  LEFT JOIN cfcommon_master AS VFF ON VFF.masterunkid = rcapp_vacancy_mapping.vacancy_found_out_from_unkid AND VFF.mastertype = " & clsCommon_Master.enCommonMaster.VACANCY_SOURCE & " " & _
                   "WHERE rcapp_vacancy_mapping.isactive = 1 AND ISNULL(rcapp_vacancy_mapping.isvoid, 0) = 0 "
            'Sohail (25 Sep 2020) - [loginemployeeunkid]
            'Sohail (27 Sep 2019) - [rcapp_vacancy_mapping.vacancy_found_out_from]=[CASE WHEN ISNULL(rcapp_vacancy_mapping.vacancy_found_out_from_unkid, 0) > 0 THEN  VFF.name ELSE rcapp_vacancy_mapping.vacancy_found_out_from END AS vacancy_found_out_from], [LEFT JOIN cfcommon_master AS VFF]
            'Sohail (09 Oct 2018) - [isfromonline, serverappvacancytranunkid, isvoid, voiduserunkid, voiddatetime, voidreason]
            'S.SANDEEP [ 14 May 2013 {isimport,importuserunkid,importdatetime}] -- START -- END
            'Nilay (13 Apr 2017) -- [earliest_possible_startdate, comments, vacancy_found_out_from]

            If intApplicantId > 0 Then
                strQ &= " AND rcapp_vacancy_mapping.applicantunkid = '" & intApplicantId & "' "
            End If

            strQ &= " ORDER BY appvacancytranunkid DESC "

            dsList = objDataOperation.ExecQuery(strQ, strTableName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For Each dRow As DataRow In dsList.Tables(0).Rows
                dRow.Item("vacancy") = dRow.Item("vacancy") & " (" & eZeeDate.convertDate(dRow.Item("ODate").ToString).ToShortDateString & " - " & eZeeDate.convertDate(dRow.Item("CDate").ToString).ToShortDateString & ")"
            Next

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
        Return dsList
    End Function

    ''' <summary>
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> INSERT INTO Database Table (rcapp_vacancy_mapping) </purpose>
    Public Function Insert() As Boolean
        If isExist(mintApplicantunkid, mintVacancyunkid) Then
            'Sohail (09 Oct 2018) -- Start
            'TANAPA - Support Issue Id # 2502 - linking recruitment server app_vacancy with aruti desktop app_vacancy in 75.1.
            'mstrMessage = "<Message>"
            mstrMessage = Language.getMessage(mstrModuleName, 1, "This Vacancy is already exist.")
            'Sohail (09 Oct 2018) -- End
            Return False
        End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation
        objDataOperation.BindTransaction()

        Try
            objDataOperation.AddParameter("@applicantunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintApplicantunkid.ToString)
            objDataOperation.AddParameter("@vacancyunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVacancyunkid.ToString)
            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
            objDataOperation.AddParameter("@isactive", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsactive.ToString)
            'S.SANDEEP [ 14 May 2013 ] -- START
            'ENHANCEMENT : TRA ENHANCEMENT
            objDataOperation.AddParameter("@isimport", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsimport.ToString)
            objDataOperation.AddParameter("@importuserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintImportuserunkid.ToString)
            If mdtImportdatetime <> Nothing Then
                objDataOperation.AddParameter("@importdatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtImportdatetime)
            Else
                objDataOperation.AddParameter("@importdatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            End If
            'S.SANDEEP [ 14 May 2013 ] -- END

            'Nilay (13 Apr 2017) -- Start
            If mdtEarliestPossibleStartDate <> Nothing Then
                objDataOperation.AddParameter("@earliest_possible_startdate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtEarliestPossibleStartDate)
            Else
                objDataOperation.AddParameter("@earliest_possible_startdate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            End If
            objDataOperation.AddParameter("@comments", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrComments)
            objDataOperation.AddParameter("@vacancy_found_out_from", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVacancyFoundOutFrom)
            'Nilay (13 Apr 2017) -- End
            'Sohail (09 Oct 2018) -- Start
            'TANAPA - Support Issue Id # 2502 - linking recruitment server app_vacancy with aruti desktop app_vacancy in 75.1.
            objDataOperation.AddParameter("@isfromonline", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsFromOnline)
            objDataOperation.AddParameter("@serverappvacancytranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintServerappvacancytranunkid)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid)
            If mdtVoiddatetime <> Nothing Then
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime)
            Else
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            End If
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason)
            'Sohail (09 Oct 2018) -- End
            'Sohail (27 Sep 2019) -- Start
            'NMB Enhancement # TC029: On the recruitment portal, Vacancy found out from section should be a drop down menu and the items on the drop down should be definable in a master.
            objDataOperation.AddParameter("@vacancy_found_out_from_unkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVacancyFoundOutFromUnkiId)
            'Sohail (27 Sep 2019) -- End
            'Sohail (25 Sep 2020) -- Start
            'NMB Enhancement : OLD-76 #  : Process of linking employee with an applicant, bind vacancy to employee and send notification to reporting to on applying vacancy.
            objDataOperation.AddParameter("@loginemployeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLoginemployeeunkid.ToString)
            'Sohail (25 Sep 2020) -- End

            strQ = "INSERT INTO rcapp_vacancy_mapping ( " & _
              "  applicantunkid " & _
              ", vacancyunkid " & _
              ", userunkid " & _
              ", isactive" & _
                            ", importuserunkid " & _
                            ", importdatetime " & _
                            ", isimport" & _
                            ", earliest_possible_startdate " & _
                            ", comments " & _
                            ", vacancy_found_out_from " & _
                            ", isfromonline " & _
                            ", serverappvacancytranunkid " & _
                            ", isvoid " & _
                            ", voiduserunkid " & _
                            ", voiddatetime " & _
                            ", voidreason " & _
                            ", vacancy_found_out_from_unkid " & _
                            ", loginemployeeunkid  " & _
            ") VALUES (" & _
              "  @applicantunkid " & _
              ", @vacancyunkid " & _
              ", @userunkid " & _
              ", @isactive" & _
                            ", @importuserunkid " & _
                            ", @importdatetime " & _
                            ", @isimport" & _
                            ", @earliest_possible_startdate " & _
                            ", @comments " & _
                            ", @vacancy_found_out_from " & _
                            ", @isfromonline " & _
                            ", @serverappvacancytranunkid " & _
                            ", @isvoid " & _
                            ", @voiduserunkid " & _
                            ", @voiddatetime " & _
                            ", @voidreason " & _
                            ", @vacancy_found_out_from_unkid " & _
                            ", @loginemployeeunkid  " & _
            "); SELECT @@identity "
            'Sohail (25 Sep 2020) - [loginemployeeunkid]
            'Sohail (27 Sep 2019) - [vacancy_found_out_from_unkid]
            'Sohail (09 Oct 2018) - [isfromonline, serverappvacancytranunkid, isvoid, voiduserunkid, voiddatetime, voidreason]
            'Nilay (13 Apr 2017) -- [earliest_possible_startdate, comments, vacancy_found_out_from]
            'S.SANDEEP [ 14 May 2013 {isimport,importuserunkid,importdatetime}] -- START -- END

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            mintAppvacancytranunkid = dsList.Tables(0).Rows(0).Item(0)

'S.SANDEEP [28-May-2018] -- START
'ISSUE/ENHANCEMENT : {Audit Trails} 
Dim objCommonATLog AS New clsCommonATLog 
objCommonATLog._FormName = mstrFormName 
objCommonATLog._LoginEmployeeUnkid = mintLoginEmployeeunkid 
            objCommonATLog._ClientIP = mstrClientIP
            objCommonATLog._HostName = mstrHostName
            objCommonATLog._FromWeb = mblnIsWeb
            objCommonATLog._AuditUserId = mintAuditUserId
objCommonATLog._CompanyUnkid = mintCompanyUnkid
            objCommonATLog._AuditDate = mdtAuditDate
            'S.SANDEEP [28-May-2018] -- END

            If objCommonATLog.Insert_AtLog(objDataOperation, 1, "rcapp_vacancy_mapping", "appvacancytranunkid", mintAppvacancytranunkid) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            'S.SANDEEP [28-May-2018] -- START
            'ISSUE/ENHANCEMENT : {Audit Trails} 
            objCommonATLog = Nothing
            'S.SANDEEP [28-May-2018] -- END


            objDataOperation.ReleaseTransaction(True)
            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Insert; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Update Database Table (rcapp_vacancy_mapping) </purpose>
    Public Function Update() As Boolean
        If isExist(mintApplicantunkid, mintVacancyunkid, mintAppvacancytranunkid) Then
            mstrMessage = "<Message>"
            Return False
        End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation
        objDataOperation.BindTransaction()

        Try
            objDataOperation.AddParameter("@appvacancytranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAppvacancytranunkid.ToString)
            objDataOperation.AddParameter("@applicantunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintApplicantunkid.ToString)
            objDataOperation.AddParameter("@vacancyunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVacancyunkid.ToString)
            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
            objDataOperation.AddParameter("@isactive", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsactive.ToString)
            'S.SANDEEP [ 14 May 2013 ] -- START
            'ENHANCEMENT : TRA ENHANCEMENT
            objDataOperation.AddParameter("@isimport", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsimport.ToString)
            objDataOperation.AddParameter("@importuserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintImportuserunkid.ToString)
            If mdtImportdatetime <> Nothing Then
                objDataOperation.AddParameter("@importdatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtImportdatetime)
            Else
                objDataOperation.AddParameter("@importdatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            End If
            'S.SANDEEP [ 14 May 2013 ] -- END
            'Nilay (13 Apr 2017) -- Start
            If mdtEarliestPossibleStartDate <> Nothing Then
                objDataOperation.AddParameter("@earliest_possible_startdate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtEarliestPossibleStartDate)
            Else
                objDataOperation.AddParameter("@earliest_possible_startdate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            End If
            objDataOperation.AddParameter("@comments", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrComments)
            objDataOperation.AddParameter("@vacancy_found_out_from", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVacancyFoundOutFrom)
            'Nilay (13 Apr 2017) -- End
            'Sohail (09 Oct 2018) -- Start
            'TANAPA - Support Issue Id # 2502 - linking recruitment server app_vacancy with aruti desktop app_vacancy in 75.1.
            objDataOperation.AddParameter("@isfromonline", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsFromOnline)
            objDataOperation.AddParameter("@serverappvacancytranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintServerappvacancytranunkid)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid)
            If mdtVoiddatetime <> Nothing Then
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime)
            Else
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            End If
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason)
            'Sohail (09 Oct 2018) -- End
            'Sohail (27 Sep 2019) -- Start
            'NMB Enhancement # TC029: On the recruitment portal, Vacancy found out from section should be a drop down menu and the items on the drop down should be definable in a master.
            objDataOperation.AddParameter("@vacancy_found_out_from_unkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVacancyFoundOutFromUnkiId)
            'Sohail (27 Sep 2019) -- End
            'Sohail (25 Sep 2020) -- Start
            'NMB Enhancement : OLD-76 #  : Process of linking employee with an applicant, bind vacancy to employee and send notification to reporting to on applying vacancy.
            objDataOperation.AddParameter("@loginemployeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLoginemployeeunkid.ToString)
            'Sohail (25 Sep 2020) -- End

            strQ = "UPDATE rcapp_vacancy_mapping SET " & _
              "  applicantunkid = @applicantunkid" & _
              ", vacancyunkid = @vacancyunkid" & _
              ", userunkid = @userunkid" & _
              ", isactive = @isactive " & _
                   ", importuserunkid = @importuserunkid " & _
                   ", importdatetime = @importdatetime " & _
                   ", isimport = @isimport " & _
                   ", earliest_possible_startdate = @earliest_possible_startdate " & _
                   ", comments = @comments " & _
                   ", vacancy_found_out_from = @vacancy_found_out_from " & _
                   ", isfromonline = @isfromonline " & _
                    ", serverappvacancytranunkid = @serverappvacancytranunkid " & _
                    ", isvoid = @isvoid " & _
                    ", voiduserunkid = @voiduserunkid " & _
                    ", voiddatetime = @voiddatetime " & _
                    ", voidreason = @voidreason " & _
                    ", vacancy_found_out_from_unkid = @vacancy_found_out_from_unkid " & _
                    " , loginemployeeunkid = @loginemployeeunkid  " & _
                   "WHERE appvacancytranunkid = @appvacancytranunkid "
            'Sohail (25 Sep 2020) - [loginemployeeunkid]
            'Sohail (27 Sep 2019) - [vacancy_found_out_from_unkid]
            'Sohail (09 Oct 2018) - [isfromonline, serverappvacancytranunkid, isvoid, voiduserunkid, voiddatetime, voidreason]
            'S.SANDEEP [ 14 May 2013 {isimport,importuserunkid,importdatetime}] -- START -- END
            'Nilay (13 Apr 2017) -- [earliest_possible_startdate, comments, vacancy_found_out_from]

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            'S.SANDEEP [28-May-2018] -- START
            'ISSUE/ENHANCEMENT : {Audit Trails} 
            Dim objCommonATLog As New clsCommonATLog
            objCommonATLog._FormName = mstrFormName
            objCommonATLog._LoginEmployeeUnkid = mintLoginEmployeeunkid
            objCommonATLog._ClientIP = mstrClientIP
            objCommonATLog._HostName = mstrHostName
            objCommonATLog._FromWeb = mblnIsWeb
            objCommonATLog._AuditUserId = mintAuditUserId
objCommonATLog._CompanyUnkid = mintCompanyUnkid
            objCommonATLog._AuditDate = mdtAuditDate
            'S.SANDEEP [28-May-2018] -- END

            If objCommonATLog.Insert_AtLog(objDataOperation, 2, "rcapp_vacancy_mapping", "appvacancytranunkid", mintAppvacancytranunkid) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            'S.SANDEEP [28-May-2018] -- START
            'ISSUE/ENHANCEMENT : {Audit Trails} 
            objCommonATLog = Nothing
            'S.SANDEEP [28-May-2018] -- END


            objDataOperation.ReleaseTransaction(True)
            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Delete Database Table (rcapp_vacancy_mapping) </purpose>
    Public Function Delete(ByVal intUnkid As Integer _
                           , ByVal blnVoid As Boolean _
                           , ByVal intVoidUser As Integer _
                           , ByVal dtVoidDate As DateTime _
                           , ByVal strReason As String _
                           ) As Boolean
        'Sohail (25 Sep 2020) - [blnVoid, intVoidUser, dtVoidDate, strReason]

        If isUsed(intUnkid) Then
            mstrMessage = "<Message>"
            Return False
        End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation
        objDataOperation.BindTransaction()

        Try

            'S.SANDEEP [28-May-2018] -- START
            'ISSUE/ENHANCEMENT : {Audit Trails} 
            Dim objCommonATLog As New clsCommonATLog
            objCommonATLog._FormName = mstrFormName
            objCommonATLog._LoginEmployeeUnkid = mintLoginEmployeeunkid
            objCommonATLog._ClientIP = mstrClientIP
            objCommonATLog._HostName = mstrHostName
            objCommonATLog._FromWeb = mblnIsWeb
            objCommonATLog._AuditUserId = mintAuditUserId
objCommonATLog._CompanyUnkid = mintCompanyUnkid
            objCommonATLog._AuditDate = mdtAuditDate
            'S.SANDEEP [28-May-2018] -- END

            If objCommonATLog.Insert_AtLog(objDataOperation, 3, "rcapp_vacancy_mapping", "appvacancytranunkid", intUnkid) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            'S.SANDEEP [28-May-2018] -- START
            'ISSUE/ENHANCEMENT : {Audit Trails} 
            objCommonATLog = Nothing
            'S.SANDEEP [28-May-2018] -- END


            'Sohail (25 Sep 2020) -- Start
            'NMB Enhancement : OLD-76 #  : Process of linking employee with an applicant, bind vacancy to employee and send notification to reporting to on applying vacancy.
            'strQ = "DELETE FROM rcapp_vacancy_mapping " & _
            '            "WHERE appvacancytranunkid = @appvacancytranunkid "
            strQ = "UPDATE rcapp_vacancy_mapping SET " & _
                          "  isvoid = @isvoid " & _
                          ", voiduserunkid = @voiduserunkid " & _
                          ", voiddatetime = @voiddatetime " & _
                                ", voidreason = @voidreason " & _
            "WHERE appvacancytranunkid = @appvacancytranunkid "
            'Sohail (25 Sep 2020) -- End

            objDataOperation.AddParameter("@appvacancytranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)
            'Sohail (25 Sep 2020) -- Start
            'NMB Enhancement : OLD-76 #  : Process of linking employee with an applicant, bind vacancy to employee and send notification to reporting to on applying vacancy.
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, blnVoid.ToString)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intVoidUser.ToString)
            objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, dtVoidDate)
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strReason.ToString)
            'Sohail (25 Sep 2020) -- End

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If
            objDataOperation.ReleaseTransaction(True)
            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isUsed(ByVal intUnkid As Integer) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "<Query>"

            objDataOperation.AddParameter("@appvacancytranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isUsed; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isExist(ByVal intApplicantunkid As Integer, ByVal intVacancyUnkid As Integer, Optional ByVal intUnkid As Integer = -1, Optional ByRef intAppVacancyTranUnkid As Integer = -1, Optional ByVal iExcludeImport As Boolean = False) As Boolean 'S.SANDEEP [ 14 May 2013 ] -- START -- END
        'Public Function isExist(ByVal intApplicantunkid As Integer, ByVal intVacancyUnkid As Integer, Optional ByVal intUnkid As Integer = -1) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "SELECT " & _
              "  appvacancytranunkid " & _
              ", applicantunkid " & _
              ", vacancyunkid " & _
              ", userunkid " & _
              ", isactive " & _
              ", CONVERT(CHAR(8),earliest_possible_startdate,112) AS earliest_possible_startdate " & _
              ", ISNULL(comments,'') AS comments " & _
              ", ISNULL(vacancy_found_out_from,'') AS vacancy_found_out_from " & _
              ", ISNULL(isfromonline,'') AS isfromonline " & _
                    ", ISNULL(serverappvacancytranunkid, 0) AS serverappvacancytranunkid " & _
                    ", isvoid " & _
                    ", voiduserunkid " & _
                    ", voiddatetime " & _
                    ", voidreason " & _
                    ", ISNULL(vacancy_found_out_from_unkid,'') AS vacancy_found_out_from_unkid " & _
             "FROM rcapp_vacancy_mapping " & _
             "WHERE applicantunkid = @applicantunkid " & _
             "AND vacancyunkid = @vacancyunkid " & _
             "AND isactive = 1 " & _
             "AND ISNULL(isvoid, 0) = 0 "
            'Sohail (27 Sep 2019) - [vacancy_found_out_from_unkid]
            'Sohail (09 Oct 2018) - [isfromonline, serverappvacancytranunkid, isvoid, voiduserunkid, voiddatetime, voidreason]
            'Nilay (13 Apr 2017) -- [earliest_possible_startdate, comments, vacancy_found_out_from]
            'Sohail (27 May 2013) - [isactive = 1]

            If intUnkid > 0 Then
                strQ &= " AND appvacancytranunkid <> @appvacancytranunkid"
            End If

            'S.SANDEEP [ 14 May 2013 ] -- START
            'ENHANCEMENT : TRA ENHANCEMENT
            If iExcludeImport = True Then
                strQ &= " AND rcapp_vacancy_mapping.isimport = 1 "
            End If
            'S.SANDEEP [ 14 May 2013 ] -- END

            objDataOperation.AddParameter("@applicantunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intApplicantunkid)
            objDataOperation.AddParameter("@vacancyunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intVacancyUnkid)
            objDataOperation.AddParameter("@appvacancytranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            'Sohail (27 May 2013) -- Start
            'TRA - ENHANCEMENT
            If dsList.Tables(0).Rows.Count > 0 Then
                intAppVacancyTranUnkid = CInt(dsList.Tables(0).Rows(0).Item("appvacancytranunkid"))
            End If
            'Sohail (27 May 2013) -- End

            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isExist; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Shared Function GetAssingedVacancyIds(ByVal intApplicantId As Integer) As String
        Dim StrVacancyIds As String = String.Empty
        Dim StrQ As String = String.Empty
        Dim dsList As New DataSet
        Try
            Dim objDataOperation As New clsDataOperation

            StrQ = "SELECT ISNULL(STUFF((SELECT ',' + CAST(s.vacancyunkid AS NVARCHAR(50)) FROM rcapp_vacancy_mapping s WHERE ISNULL(s.isvoid, 0) = 0 AND s.applicantunkid = '" & intApplicantId & "' ORDER BY s.vacancyunkid FOR XML PATH('')),1,1,''),'') AS CSV "
            'Sohail (09 Oct 2018) - [ISNULL(s.isvoid, 0) = 0]

            dsList = objDataOperation.ExecQuery(StrQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                Throw New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
            End If

            If dsList.Tables(0).Rows.Count > 0 Then
                StrVacancyIds = dsList.Tables(0).Rows(0)("CSV")
            End If

            Return StrVacancyIds

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isExist; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Function


    'S.SANDEEP [ 06 NOV 2012 ] -- START
    'ENHANCEMENT : TRA CHANGES
    'Public Shared Function GetLatestVacancy(ByVal intApplicantId As Integer, ByRef intVacancyId As Integer) As String
    Public Shared Function GetLatestVacancy(ByVal intApplicantId As Integer, ByRef intVacancyId As Integer, Optional ByVal intMappedVacancy As Integer = -1) As String
        'S.SANDEEP [ 06 NOV 2012 ] -- END
        Dim StrQ As String = String.Empty
        Dim dsList As New DataSet
        Dim StrVacancy As String = String.Empty
        Try
            Dim objDataOperation As New clsDataOperation

            'S.SANDEEP [ 06 NOV 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            'StrQ = "SELECT TOP 1 " & _
            '       "   ISNULL(cfcommon_master.name,'') AS Vacancy " & _
            '       "  ,CONVERT(CHAR(8),openingdate,112) AS ODate " & _
            '       "  ,CONVERT(CHAR(8),closingdate,112) AS CDate " & _
            '       "  ,rcapp_vacancy_mapping.vacancyunkid " & _
            '       "FROM rcapp_vacancy_mapping " & _
            '       "  JOIN rcvacancy_master ON rcapp_vacancy_mapping.vacancyunkid = rcvacancy_master.vacancyunkid " & _
            '       "  JOIN cfcommon_master ON cfcommon_master.masterunkid = rcvacancy_master.vacancytitle " & _
            '       "WHERE applicantunkid = '" & intApplicantId & "' ORDER BY appvacancytranunkid DESC "
            If intMappedVacancy > 0 Then
                StrQ = "SELECT TOP 1 " & _
                   "   ISNULL(cfcommon_master.name,'') AS Vacancy " & _
                   "  ,CONVERT(CHAR(8),openingdate,112) AS ODate " & _
                   "  ,CONVERT(CHAR(8),closingdate,112) AS CDate " & _
                   "  ,rcapp_vacancy_mapping.vacancyunkid " & _
                   "FROM rcapp_vacancy_mapping " & _
                   "  JOIN rcvacancy_master ON rcapp_vacancy_mapping.vacancyunkid = rcvacancy_master.vacancyunkid " & _
                   "  JOIN cfcommon_master ON cfcommon_master.masterunkid = rcvacancy_master.vacancytitle " & _
                   "WHERE ISNULL(rcapp_vacancy_mapping.isvoid, 0) = 0 AND applicantunkid = '" & intApplicantId & "' AND rcvacancy_master.vacancyunkid = '" & intMappedVacancy & "' ORDER BY appvacancytranunkid DESC "
                'Sohail (09 Oct 2018) - [ISNULL(rcapp_vacancy_mapping.isvoid, 0) = 0]
            Else
            StrQ = "SELECT TOP 1 " & _
                   "   ISNULL(cfcommon_master.name,'') AS Vacancy " & _
                   "  ,CONVERT(CHAR(8),openingdate,112) AS ODate " & _
                   "  ,CONVERT(CHAR(8),closingdate,112) AS CDate " & _
                   "  ,rcapp_vacancy_mapping.vacancyunkid " & _
                   "FROM rcapp_vacancy_mapping " & _
                   "  JOIN rcvacancy_master ON rcapp_vacancy_mapping.vacancyunkid = rcvacancy_master.vacancyunkid " & _
                   "  JOIN cfcommon_master ON cfcommon_master.masterunkid = rcvacancy_master.vacancytitle " & _
                       "WHERE ISNULL(rcapp_vacancy_mapping.isvoid, 0) = 0 AND applicantunkid = '" & intApplicantId & "' ORDER BY appvacancytranunkid DESC "
                'Sohail (09 Oct 2018) - [ISNULL(rcapp_vacancy_mapping.isvoid, 0) = 0]
            End If
            'S.SANDEEP [ 06 NOV 2012 ] -- END

            
            dsList = objDataOperation.ExecQuery(StrQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                Throw New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
            End If

            If dsList.Tables(0).Rows.Count > 0 Then
                StrVacancy = dsList.Tables(0).Rows(0)("Vacancy") & " ( " & _
                             eZeeDate.convertDate(dsList.Tables(0).Rows(0)("ODate").ToString).ToShortDateString & " - " & _
                             eZeeDate.convertDate(dsList.Tables(0).Rows(0)("CDate")) & " ) "
                intVacancyId = CInt(dsList.Tables(0).Rows(0)("vacancyunkid"))
            End If

            Return StrVacancy

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetLatestVacancy; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Function

    'Sohail (05 Jun 2020) -- Start
    'NMB Enhancement # : Upon successful application of a vacancy for internal vacancies, system to copy the reporting-to on the reverse email sent back to the employee.
    <DataObjectMethod(DataObjectMethodType.Select, True)> _
    Public Shared Function GetApplicantAppliedJob(ByVal intApplicantUnkId As Integer, Optional ByVal intAppvacancytranunkid As Integer = 0, Optional ByVal intVacancyUnkId As Integer = 0, Optional ByVal strFilter As String = "") As DataSet
        'Sohail (19 Jan 2021) - [strFilter]

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        Dim objDataOperation As New clsDataOperation

        Try
            strQ = "SELECT  t1.jobunkid  " & _
                          ", STUFF(( SELECT  ', ' + hrskill_master.skillname " & _
                                    "FROM    hrjob_skill_tran t2 " & _
                                            "JOIN hrskill_master ON t2.skillunkid = hrskill_master.skillunkid " & _
                                    "WHERE   t1.jobunkid = t2.jobunkid " & _
                                            "AND t2.isactive = 1 " & _
                                    "FOR " & _
                                    "XML PATH('') " & _
                                    "), 1, 1, '') Skill " & _
                            "INTO #Vac_Skill " & _
                    "FROM    hrjob_skill_tran t1 " & _
                            "JOIN hrskill_master ON t1.skillunkid = hrskill_master.skillunkid " & _
                    "WHERE    t1.isactive = 1 " & _
                    "GROUP BY t1.jobunkid " & _
                    " " & _
                    "SELECT  t1.jobunkid  " & _
                          ", STUFF(( SELECT  ', ' " & _
                                            "+ hrqualification_master.qualificationname " & _
                                    "FROM    hrjob_qualification_tran t2 " & _
                                            "JOIN hrqualification_master ON t2.qualificationunkid = hrqualification_master.qualificationunkid " & _
                                    "WHERE   t1.jobunkid = t2.jobunkid " & _
                                            "AND t2.isactive = 1 " & _
                                    "FOR " & _
                                    "XML PATH('') " & _
                                    "), 1, 1, '') Qualification " & _
                            "INTO #Vac_Quali " & _
                    "FROM    hrjob_qualification_tran t1 " & _
                            "JOIN hrqualification_master ON t1.qualificationunkid = hrqualification_master.qualificationunkid " & _
                    "WHERE   t1.isactive = 1 " & _
                    "GROUP BY t1.jobunkid " & _
                    " " & _
                    "SELECT  t1.jobunkid  " & _
                          ", STUFF(( SELECT  ', ' + cfcommon_master.name " & _
                                    "FROM    hrjob_language_tran t2 " & _
                                            "JOIN cfcommon_master ON cfcommon_master.masterunkid = t2.masterunkid " & _
                                    "WHERE   t1.jobunkid = t2.jobunkid " & _
                                            "AND t2.isactive = 1 " & _
                                    "FOR " & _
                                    "XML PATH('') " & _
                                    "), 1, 1, '') Lang " & _
                            "INTO #Vac_Lang " & _
                    "FROM    hrjob_language_tran t1 " & _
                            "JOIN cfcommon_master ON t1.masterunkid = cfcommon_master.masterunkid " & _
                    "WHERE   t1.isactive = 1 " & _
                    "GROUP BY t1.jobunkid " & _
                    " " & _
                    "SELECT " & _
                         "rcapp_vacancy_mapping.vacancyunkid AS vacancyid " & _
                        ",ISNULL(cfcommon_master.name,'') AS vacancytitle " & _
                        ",openingdate AS openingdate " & _
                        ",closingdate AS closingdate " & _
                        ",interview_Startdate AS interview_Startdate " & _
                        ",interview_closedate AS interview_closeddate " & _
                        ",experience as experience " & _
                        ",responsibilities_duties as duties " & _
                        ",remark as remark " & _
                        ",ISNULL(Vac_Skill.Skill,'') AS skill " & _
                        ",ISNULL(Vac_Quali.Qualification,'') AS Qualification " & _
                        ",ISNULL(Vac_Lang.Lang, '') AS Lang " & _
                        ",ISNULL(rcvacancy_master.noofposition,0) AS noposition " & _
                        ",ISNULL(pay_from,0) AS pay_from " & _
                        ",ISNULL(pay_to,0) AS pay_to " & _
                        ",ISNULL(isskillbold, 0) AS isskillbold " & _
                        ",ISNULL(isskillitalic, 0) AS isskillitalic " & _
                        ",ISNULL(isqualibold, 0) AS isqualibold " & _
                        ",ISNULL(isqualiitalic, 0) AS isqualiitalic " & _
                        ",ISNULL(isexpbold, 0) AS isexpbold " & _
                        ",ISNULL(isexpitalic, 0) AS isexpitalic " & _
                        ",rcvacancy_master.vacancytitle AS VacancyTitleUnkid " & _
                        ",rcapp_vacancy_mapping.appvacancytranunkid " & _
                        ", CONVERT(CHAR(8), GETDATE(), 112) AS Today " & _
                        ", rcapplicant_master.titleunkid " & _
                        ", rcapplicant_master.firstname " & _
                        ", rcapplicant_master.surname " & _
                        ", rcapplicant_master.othername " & _
                        ", rcapplicant_master.gender " & _
                        ", rcapplicant_master.email " & _
                        ", rcapplicant_master.present_address1 AS Applicant_Address1 " & _
                        ", rcapplicant_master.present_address2 AS Applicant_Address2 " & _
                        ", '' AS Applicant_City " & _
                        ", '' AS Applicant_State " & _
                        ", '' AS Applicant_Country " & _
                        ", rcapplicant_master.present_mobileno AS Mobileno " & _
                        ", CONVERT(CHAR(8), rcapplicant_master.birth_date, 112) AS BirthDate " & _
                        ", '' AS [Completed birth Age] " & _
                        ", '' AS [Running birth Age] " & _
                        ", '' AS Appreferenceno " & _
                        ", ISNULL(cfcommon_master.name,'') AS Vacancy_Without_Dates " & _
                        ", ISNULL(cfcommon_master.name,'') + ' [' + CONVERT(CHAR(8), rcvacancy_master.openingdate, 112) + ' : ' + CONVERT(CHAR(8), rcvacancy_master.closingdate, 112) +  ']'  AS Vacancy_With_Dates " & _
                        ", '' AS Branch " & _
                        ", '' AS Department_Group " & _
                        ", '' AS Job_Department " & _
                        ", ISNULL(cfcommon_master.name,'') AS VacancyName " & _
                        ", rcvacancy_master.pay_from AS Pay_Range_From " & _
                        ", rcvacancy_master.pay_to AS Pay_Range_To " & _
                        ", CONVERT(CHAR(8), rcvacancy_master.openingdate, 112) AS VacancyOpeningDate " & _
                        ", CONVERT(CHAR(8), rcvacancy_master.closingdate, 112) AS VacancyClosingDate " & _
                        ", rcvacancy_master.experience AS experience " & _
                        ", rcvacancy_master.noofposition AS noofposition " & _
                        ", rcvacancy_master.responsibilities_duties AS responsibilities_duties " & _
                        ", rcvacancy_master.remark AS remark " & _
                        ", ISNULL(hrjob_master.experience_comment, '') AS experience_comment " & _
                        ", ISNULL(rcvacancy_master.other_qualification, '') AS other_qualification " & _
                        ", ISNULL(rcvacancy_master.other_skill, '') AS other_skill " & _
                        ", ISNULL(rcvacancy_master.other_language, '') AS other_language " & _
                        ", ISNULL(rcvacancy_master.other_experience, '') AS other_experience " & _
                        ", ISNULL(hrclassgroup_master.name, '') AS classgroupname " & _
                        ", ISNULL(hrclasses_master.name, '') AS classname " & _
                    "FROM rcapp_vacancy_mapping " & _
                        "JOIN rcvacancy_master ON rcapp_vacancy_mapping.vacancyunkid = rcvacancy_master.vacancyunkid " & _
                        "JOIN rcapplicant_master ON rcapplicant_master.applicantunkid = rcapp_vacancy_mapping.applicantunkid " & _
                        "JOIN cfcommon_master ON cfcommon_master.masterunkid = rcvacancy_master.vacancytitle " & _
                        "LEFT JOIN #Vac_Skill AS Vac_Skill ON rcvacancy_master.jobunkid = Vac_Skill.jobunkid " & _
                        "LEFT JOIN #Vac_Quali AS Vac_Quali ON rcvacancy_master.jobunkid = Vac_Quali.jobunkid " & _
                        "LEFT JOIN #Vac_Lang AS Vac_Lang ON rcvacancy_master.jobunkid = Vac_Lang.jobunkid " & _
                        "LEFT JOIN hrjob_master ON hrjob_master.jobunkid = rcvacancy_master.jobunkid and hrjob_master.isactive = 1 " & _
                        "LEFT JOIN hrclassgroup_master ON rcvacancy_master.classgroupunkid = hrclassgroup_master.classgroupunkid AND hrclassgroup_master.isactive = 1 " & _
                        "LEFT JOIN hrclasses_master ON rcvacancy_master.classunkid = hrclasses_master.classesunkid  AND hrclasses_master.isactive = 1 " & _
                    "WHERE  rcapp_vacancy_mapping.isvoid = 0 " & _
                            "AND rcvacancy_master.isvoid = 0 " & _
                            "AND cfcommon_master.isactive = 1 "
            'Hemant (01 Nov 2021) -- [classgroupname,classname]
            'Sohail (13 Sep 2021) - [other_qualification, other_skill, other_language, other_experience]

            If intApplicantUnkId > 0 Then
                strQ &= " AND rcapp_vacancy_mapping.applicantunkid = @applicantunkid "
                objDataOperation.AddParameter("@applicantunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intApplicantUnkId)
            End If

            If intAppvacancytranunkid > 0 Then
                strQ &= " AND ISNULL(rcapp_vacancy_mapping.appvacancytranunkid, @appvacancytranunkid) = @appvacancytranunkid "
                objDataOperation.AddParameter("@appvacancytranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intAppvacancytranunkid)
            End If

            If intVacancyUnkId > 0 Then
                strQ &= " AND ISNULL(rcapp_vacancy_mapping.vacancyunkid, @vacancyunkid) = @vacancyunkid "
                objDataOperation.AddParameter("@vacancyunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intVacancyUnkId)
            End If

            'Sohail (19 Jan 2021) -- Start
            'New UI testing issue : - When applicant has not applied for any job, he can see all jobs on applied job page in self service.
            If strFilter <> Nothing AndAlso strFilter.Trim.Length > 0 Then
                strQ &= " " & strFilter & " "
            End If
            'Sohail (19 Jan 2021) -- End

            strQ &= "DROP TABLE #Vac_Skill " & _
                    "DROP TABLE #Vac_Quali " & _
                    "DROP TABLE #Vac_Lang "

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetApplicantAppliedJob; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
        Return dsList
    End Function
    'Sohail (05 Jun 2020) -- End

    'Sohail (25 Sep 2020) -- Start
    'NMB Enhancement : OLD-75 #  : Search job page with apply button in ESS (same as MSS, only current open vacancies).
    Public Function AppliedVacancy(ByVal intApplicantUnkId As Integer _
                                   , ByVal intVacancyUnkId As Integer _
                                   , ByVal dtEarliest_possible_startdate As Date _
                                   , ByVal strComments As String _
                                   , ByVal strVacancy_found_out_from As String _
                                   , ByVal intVacancy_found_out_from_UnkId As Integer _
                                   , ByVal intLoginEmployeeunkId As Integer _
                                   , Optional ByRef intRet_UnkID As Integer = 0 _
                                   ) As Boolean

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        intRet_UnkID = 0

        objDataOperation = New clsDataOperation

        Try
            strQ = "IF NOT EXISTS (SELECT 1 FROM rcapp_vacancy_mapping WHERE applicantunkid = @applicantunkid AND isvoid=0 AND vacancyunkid = @vacancyunkid and isvoid = 0 ) " & _
                    "BEGIN " & _
                        "INSERT INTO rcapp_vacancy_mapping ( " & _
                             "applicantunkid " & _
                            ",vacancyunkid " & _
                            ",isvoid " & _
                            ",voiddatetime " & _
                            ",voidreason " & _
                            ",vacancy_found_out_from " & _
                            ", earliest_possible_startdate " & _
                            ", comments " & _
                            ",vacancy_found_out_from_unkid " & _
                            ",userunkid " & _
                            ",isactive " & _
                            ",importuserunkid " & _
                            ",isimport " & _
                            ",isfromonline " & _
                            ",voiduserunkid " & _
                            ",loginemployeeunkid " & _
                            ") " & _
                        "VALUES ( " & _
                             "@applicantunkid " & _
                            ",@vacancyunkid " & _
                            ",0 " & _
                            ",null " & _
                            ",'' " & _
                            ",@vacancy_found_out_from " & _
                            ", @earliest_possible_startdate " & _
                            ", @comments " & _
                            ", @vacancy_found_out_from_unkid " & _
                            ", 1 " & _
                            ", 1 " & _
                            ", 0 " & _
                            ", 0 " & _
                            ", 0 " & _
                            ", -1 " & _
                            ",@loginemployeeunkid " & _
                            ");SELECT @@IDENTITY AS unkid " & _
                    "END "


            objDataOperation.AddParameter("@applicantunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intApplicantUnkId)
            objDataOperation.AddParameter("@vacancyunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intVacancyUnkId)
            If dtEarliest_possible_startdate = Nothing Then
                objDataOperation.AddParameter("@earliest_possible_startdate", SqlDbType.Date, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            Else
                objDataOperation.AddParameter("@earliest_possible_startdate", SqlDbType.Date, eZeeDataType.DATETIME_SIZE, dtEarliest_possible_startdate)
            End If
            objDataOperation.AddParameter("@comments", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, strComments.Trim.Replace("'", "''"))
            objDataOperation.AddParameter("@vacancy_found_out_from", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, strVacancy_found_out_from.Trim.Replace("'", "''"))
            objDataOperation.AddParameter("@vacancy_found_out_from_unkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intVacancy_found_out_from_UnkId)
            objDataOperation.AddParameter("@loginemployeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intLoginEmployeeunkId)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            intRet_UnkID = CInt(dsList.Tables(0).Rows(0).Item(0))

            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: AppliedVacancy; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function
    'Sohail (25 Sep 2020) -- End
End Class