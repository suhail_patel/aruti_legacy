﻿'************************************************************************************************************************************
'Class Name : clsStaffrequisition_Transfer_approver.vb
'Purpose    :
'Date       :07-02-2022
'Written By :Sohail
'Modified   :
'************************************************************************************************************************************

Imports eZeeCommonLib
''' <summary>
''' Purpose: 
''' Developer: Sohail
''' </summary>
Public Class clsStaffrequisition_Transfer_approver
    Private Const mstrModuleName = "clsStaffrequisition_Transfer_approver"
    Dim objDataOperation As clsDataOperation
    Dim mstrMessage As String = ""

#Region " Private variables "
    Private mintStaffreqtransferapproverunkid As Integer
    Private mintFromuserunkid As Integer
    Private mintTouserunkid As Integer
    Private mintFromlevelunkid As Integer
    Private mintTolevelunkid As Integer
    Private mintFromallocationid As Integer
    Private mintToallocationid As Integer
    Private mdtTransferdate As Date
    Private mstrTransferredallocationunkids As String = String.Empty
    Private mstrTransferredjobunkids As String = String.Empty
    Private mintUserunkid As Integer
    Private mblnIsactive As Boolean = True
#End Region

#Region " Properties "

    'S.SANDEEP [28-May-2018] -- START 
    'ISSUE/ENHANCEMENT : {Audit Trails} 
    Private mstrFormName As String = String.Empty
    Public WriteOnly Property _FormName() As String
        Set(ByVal value As String)
            mstrFormName = value
        End Set
    End Property

    Private mstrClientIP As String = ""
    Public WriteOnly Property _ClientIP() As String
        Set(ByVal value As String)
            mstrClientIP = value
        End Set
    End Property

    Private mstrHostName As String = ""
    Public WriteOnly Property _HostName() As String
        Set(ByVal value As String)
            mstrHostName = value
        End Set
    End Property

    Private mblnIsWeb As Boolean = False
    Public WriteOnly Property _FromWeb() As Boolean
        Set(ByVal value As Boolean)
            mblnIsWeb = value
        End Set
    End Property

    Private mintAuditUserId As Integer = 0
    Public WriteOnly Property _AuditUserId() As Integer
        Set(ByVal value As Integer)
            mintAuditUserId = value
        End Set
    End Property

    Private mintCompanyUnkid As Integer = 0
    Public WriteOnly Property _CompanyUnkid() As Integer
        Set(ByVal value As Integer)
            mintCompanyUnkid = value
        End Set
    End Property

    Private mdtAuditDate As DateTime = Now
    Public WriteOnly Property _AuditDate() As DateTime
        Set(ByVal value As DateTime)
            mdtAuditDate = value
        End Set
    End Property

    Private xDataOpr As clsDataOperation = Nothing
    Public Property _xDataOpr() As clsDataOperation
        Get
            Return xDataOpr
        End Get
        Set(ByVal value As clsDataOperation)
            xDataOpr = value
        End Set
    End Property

    Private mintLoginEmployeeunkid As Integer = 0
    Public Property _LoginEmployeeunkid() As Integer
        Get
            Return mintLoginEmployeeunkid
        End Get
        Set(ByVal value As Integer)
            mintLoginEmployeeunkid = value
        End Set
    End Property

    Private mintVoidlogingemployeeunkid As Integer = 0
    Public Property _Voidloginemployeeunkid() As Integer
        Get
            Return mintVoidlogingemployeeunkid
        End Get
        Set(ByVal value As Integer)
            mintVoidlogingemployeeunkid = value
        End Set
    End Property
    'S.SANDEEP [28-May-2018] -- END 

    ''' <summary>
    ''' Purpose: Get Message from Class 
    ''' Modify By: Sohail
    ''' </summary>
    Public ReadOnly Property _Message() As String
        Get
            Return mstrMessage
        End Get
    End Property

    ''' <summary>
    ''' Purpose: Get or Set staffreqtransferapproverunkid
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Staffreqtransferapproverunkid() As Integer
        Get
            Return mintStaffreqtransferapproverunkid
        End Get
        Set(ByVal value As Integer)
            mintStaffreqtransferapproverunkid = value
            Call GetData()
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set fromuserunkid
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Fromuserunkid() As Integer
        Get
            Return mintFromuserunkid
        End Get
        Set(ByVal value As Integer)
            mintFromuserunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set touserunkid
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Touserunkid() As Integer
        Get
            Return mintTouserunkid
        End Get
        Set(ByVal value As Integer)
            mintTouserunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set fromlevelunkid
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Fromlevelunkid() As Integer
        Get
            Return mintFromlevelunkid
        End Get
        Set(ByVal value As Integer)
            mintFromlevelunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set tolevelunkid
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Tolevelunkid() As Integer
        Get
            Return mintTolevelunkid
        End Get
        Set(ByVal value As Integer)
            mintTolevelunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set fromallocationid
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Fromallocationid() As Integer
        Get
            Return mintFromallocationid
        End Get
        Set(ByVal value As Integer)
            mintFromallocationid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set toallocationid
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Toallocationid() As Integer
        Get
            Return mintToallocationid
        End Get
        Set(ByVal value As Integer)
            mintToallocationid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set transferdate
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Transferdate() As Date
        Get
            Return mdtTransferdate
        End Get
        Set(ByVal value As Date)
            mdtTransferdate = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set transferredallocationunkids
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Transferredallocationunkids() As String
        Get
            Return mstrTransferredallocationunkids
        End Get
        Set(ByVal value As String)
            mstrTransferredallocationunkids = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set transferredjobunkids
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Transferredjobunkids() As String
        Get
            Return mstrTransferredjobunkids
        End Get
        Set(ByVal value As String)
            mstrTransferredjobunkids = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set userunkid
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Userunkid() As Integer
        Get
            Return mintUserunkid
        End Get
        Set(ByVal value As Integer)
            mintUserunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isactive
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Isactive() As Boolean
        Get
            Return mblnIsactive
        End Get
        Set(ByVal value As Boolean)
            mblnIsactive = value
        End Set
    End Property

#End Region

    ''' <summary>
    ''' Modify By: Sohail
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Sub GetData()
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "SELECT " & _
              "  staffreqtransferapproverunkid " & _
              ", fromuserunkid " & _
              ", touserunkid " & _
              ", fromlevelunkid " & _
              ", tolevelunkid " & _
              ", fromallocationid " & _
              ", toallocationid " & _
              ", transferdate " & _
              ", transferredallocationunkids " & _
              ", transferredjobunkids " & _
              ", userunkid " & _
              ", isactive " & _
             "FROM rcstaffrequisition_transfer_approver " & _
             "WHERE staffreqtransferapproverunkid = @staffreqtransferapproverunkid "

            objDataOperation.AddParameter("@staffreqtransferapproverunkid", SqlDbType.int, eZeeDataType.INT_SIZE, mintStaffreqTransferapproverUnkId.ToString)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For Each dtRow As DataRow In dsList.Tables(0).Rows
                mintStaffreqtransferapproverunkid = CInt(dtRow.Item("staffreqtransferapproverunkid"))
                mintFromuserunkid = CInt(dtRow.Item("fromuserunkid"))
                mintTouserunkid = CInt(dtRow.Item("touserunkid"))
                mintFromlevelunkid = CInt(dtRow.Item("fromlevelunkid"))
                mintTolevelunkid = CInt(dtRow.Item("tolevelunkid"))
                mintFromallocationid = CInt(dtRow.Item("fromallocationid"))
                mintToallocationid = CInt(dtRow.Item("toallocationid"))
                mdtTransferdate = CDate(dtRow.Item("transferdate"))
                mstrTransferredallocationunkids = dtRow.Item("transferredallocationunkids").ToString
                mstrTransferredjobunkids = dtRow.Item("transferredjobunkids").ToString
                mintUserunkid = CInt(dtRow.Item("userunkid"))
                mblnIsactive = CBool(dtRow.Item("isactive"))
                Exit For
            Next
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetData; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Sub


    ''' <summary>
    ''' Modify By: Sohail
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function GetList(ByVal strTableName As String) As DataSet
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "SELECT " & _
              "  staffreqtransferapproverunkid " & _
              ", fromuserunkid " & _
              ", touserunkid " & _
              ", fromlevelunkid " & _
              ", tolevelunkid " & _
              ", fromallocationid " & _
              ", toallocationid " & _
              ", transferdate " & _
              ", transferredallocationunkids " & _
              ", transferredjobunkids " & _
              ", userunkid " & _
              ", isactive " & _
             "FROM rcstaffrequisition_transfer_approver " & _
             " WHERE isactive = 1 "

            dsList = objDataOperation.ExecQuery(strQ, strTableName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
        Return dsList
    End Function


    ''' <summary>
    ''' Modify By: Sohail
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> INSERT INTO Database Table (rcstaffrequisition_transfer_approver) </purpose>
    Public Function Insert(ByVal xDataOpr As clsDataOperation) As Boolean
        'If isExist(mstrName) Then
        '    mstrMessage = "<Message>"
        '    Return False
        'End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        If xDataOpr Is Nothing Then
            objDataOperation = New clsDataOperation
            objDataOperation.BindTransaction()
        Else
            objDataOperation = xDataOpr
        End If
        objDataOperation.ClearParameters()

        Try
            objDataOperation.AddParameter("@fromuserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintFromuserunkid.ToString)
            objDataOperation.AddParameter("@touserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintTouserunkid.ToString)
            objDataOperation.AddParameter("@fromlevelunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintFromlevelunkid.ToString)
            objDataOperation.AddParameter("@tolevelunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintTolevelunkid.ToString)
            objDataOperation.AddParameter("@fromallocationid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintFromallocationid.ToString)
            objDataOperation.AddParameter("@toallocationid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintToallocationid.ToString)
            objDataOperation.AddParameter("@transferdate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtTransferdate.ToString)
            objDataOperation.AddParameter("@transferredallocationunkids", SqlDbType.NText, mstrTransferredallocationunkids.Length, mstrTransferredallocationunkids.ToString)
            objDataOperation.AddParameter("@transferredjobunkids", SqlDbType.NText, mstrTransferredjobunkids.Length, mstrTransferredjobunkids.ToString)
            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
            objDataOperation.AddParameter("@isactive", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsactive.ToString)

            strQ = "INSERT INTO rcstaffrequisition_transfer_approver ( " & _
                          "  fromuserunkid " & _
                          ", touserunkid " & _
                          ", fromlevelunkid " & _
                          ", tolevelunkid " & _
                          ", fromallocationid " & _
                          ", toallocationid " & _
                          ", transferdate " & _
                          ", transferredallocationunkids " & _
                          ", transferredjobunkids " & _
                          ", userunkid " & _
                          ", isactive" & _
                    ") VALUES (" & _
                          "  @fromuserunkid " & _
                          ", @touserunkid " & _
                          ", @fromlevelunkid " & _
                          ", @tolevelunkid " & _
                          ", @fromallocationid " & _
                          ", @toallocationid " & _
                          ", @transferdate " & _
                          ", @transferredallocationunkids " & _
                          ", @transferredjobunkids " & _
                          ", @userunkid " & _
                          ", @isactive" & _
                    "); SELECT @@identity"

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                objDataOperation.ReleaseTransaction(False)
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            mintStaffreqtransferapproverunkid = dsList.Tables(0).Rows(0).Item(0)

            Dim objCommonATLog As New clsCommonATLog
            objCommonATLog._FormName = mstrFormName
            objCommonATLog._LoginEmployeeUnkid = mintLoginEmployeeunkid
            objCommonATLog._ClientIP = mstrClientIP
            objCommonATLog._HostName = mstrHostName
            objCommonATLog._FromWeb = mblnIsWeb
            objCommonATLog._AuditUserId = mintAuditUserId
            objCommonATLog._CompanyUnkid = mintCompanyUnkid
            objCommonATLog._AuditDate = mdtAuditDate
            If objCommonATLog.Insert_AtLog(objDataOperation, 1, "rcstaffrequisition_transfer_approver", "staffreqtransferapproverunkid", mintStaffreqtransferapproverunkid, , mintUserunkid) = False Then
                objDataOperation.ReleaseTransaction(False)
                exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If xDataOpr Is Nothing Then objDataOperation.ReleaseTransaction(True)

            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Insert; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xDataOpr Is Nothing Then objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sohail
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Update Database Table (rcstaffrequisition_transfer_approver) </purpose>
    Public Function Update(ByVal xDataOpr As clsDataOperation) As Boolean
        'If isExist(mstrName, mintStaffreqtransferapproverunkid) Then
        '    mstrMessage = "<Message>"
        '    Return False
        'End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        If xDataOpr Is Nothing Then
            objDataOperation = New clsDataOperation
            objDataOperation.BindTransaction()
        Else
            objDataOperation = xDataOpr
        End If
        objDataOperation.ClearParameters()

        Try
            objDataOperation.AddParameter("@staffreqtransferapproverunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintStaffreqtransferapproverunkid.ToString)
            objDataOperation.AddParameter("@fromuserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintFromuserunkid.ToString)
            objDataOperation.AddParameter("@touserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintTouserunkid.ToString)
            objDataOperation.AddParameter("@fromlevelunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintFromlevelunkid.ToString)
            objDataOperation.AddParameter("@tolevelunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintTolevelunkid.ToString)
            objDataOperation.AddParameter("@fromallocationid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintFromallocationid.ToString)
            objDataOperation.AddParameter("@toallocationid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintToallocationid.ToString)
            objDataOperation.AddParameter("@transferdate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtTransferdate.ToString)
            objDataOperation.AddParameter("@transferredallocationunkids", SqlDbType.NText, eZeeDataType.NAME_SIZE, mstrTransferredallocationunkids.ToString)
            objDataOperation.AddParameter("@transferredjobunkids", SqlDbType.NText, eZeeDataType.NAME_SIZE, mstrTransferredjobunkids.ToString)
            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
            objDataOperation.AddParameter("@isactive", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsactive.ToString)

            strQ = "UPDATE rcstaffrequisition_transfer_approver SET " & _
                      "  fromuserunkid = @fromuserunkid" & _
                      ", touserunkid = @touserunkid" & _
                      ", fromlevelunkid = @fromlevelunkid" & _
                      ", tolevelunkid = @tolevelunkid" & _
                      ", fromallocationid = @fromallocationid" & _
                      ", toallocationid = @toallocationid" & _
                      ", transferdate = @transferdate" & _
                      ", transferredallocationunkids = @transferredallocationunkids" & _
                      ", transferredjobunkids = @transferredjobunkids" & _
                      ", userunkid = @userunkid" & _
                      ", isactive = @isactive " & _
            "WHERE staffreqtransferapproverunkid = @staffreqtransferapproverunkid "

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                objDataOperation.ReleaseTransaction(False)
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Dim objCommonATLog As New clsCommonATLog
            objCommonATLog._FormName = mstrFormName
            objCommonATLog._LoginEmployeeUnkid = mintLoginEmployeeunkid
            objCommonATLog._ClientIP = mstrClientIP
            objCommonATLog._HostName = mstrHostName
            objCommonATLog._FromWeb = mblnIsWeb
            objCommonATLog._AuditUserId = mintAuditUserId
            objCommonATLog._CompanyUnkid = mintCompanyUnkid
            objCommonATLog._AuditDate = mdtAuditDate
            If objCommonATLog.Insert_AtLog(objDataOperation, 2, "rcstaffrequisition_transfer_approver", "staffreqtransferapproverunkid", mintStaffreqtransferapproverunkid, , mintUserunkid) = False Then
                objDataOperation.ReleaseTransaction(False)
                exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If xDataOpr Is Nothing Then objDataOperation.ReleaseTransaction(True)

            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xDataOpr Is Nothing Then objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sohail
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Delete Database Table (rcstaffrequisition_transfer_approver) </purpose>
    Public Function Delete(ByVal xDataOpr As clsDataOperation, ByVal intUnkid As Integer) As Boolean
        If isUsed(intUnkid) Then
            mstrMessage = "<Message>"
            Return False
        End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        If xDataOpr Is Nothing Then
            objDataOperation = New clsDataOperation
            objDataOperation.BindTransaction()
        Else
            objDataOperation = xDataOpr
        End If
        objDataOperation.ClearParameters()

        Try
            strQ = "UPDATE rcstaffrequisition_transfer_approver SET " & _
                     " isactive = 0 " & _
           "WHERE staffreqtransferapproverunkid = @staffreqtransferapproverunkid "

            objDataOperation.AddParameter("@staffreqtransferapproverunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Dim objCommonATLog As New clsCommonATLog
            objCommonATLog._FormName = mstrFormName
            objCommonATLog._LoginEmployeeUnkid = mintLoginEmployeeunkid
            objCommonATLog._ClientIP = mstrClientIP
            objCommonATLog._HostName = mstrHostName
            objCommonATLog._FromWeb = mblnIsWeb
            objCommonATLog._AuditUserId = mintAuditUserId
            objCommonATLog._CompanyUnkid = mintCompanyUnkid
            objCommonATLog._AuditDate = mdtAuditDate
            If objCommonATLog.Insert_AtLog(objDataOperation, 3, "rcstaffrequisition_transfer_approver", "staffreqtransferapproverunkid", mintStaffreqtransferapproverunkid, , mintUserunkid) = False Then
                objDataOperation.ReleaseTransaction(False)
                exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If xDataOpr Is Nothing Then objDataOperation.ReleaseTransaction(True)

            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Delete; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xDataOpr Is Nothing Then objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sohail
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isUsed(ByVal intUnkid As Integer) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            StrQ = "<Query>"

            objDataOperation.AddParameter("@staffreqtransferapproverunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.tables(0).rows.count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isUsed; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sohail
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isExist(ByVal strCode As String, ByVal strName As String, Optional ByVal intUnkid As Integer = -1) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "SELECT " & _
              "  staffreqtransferapproverunkid " & _
              ", fromuserunkid " & _
              ", touserunkid " & _
              ", fromlevelunkid " & _
              ", tolevelunkid " & _
              ", fromallocationid " & _
              ", toallocationid " & _
              ", transferdate " & _
              ", transferredallocationunkids " & _
              ", transferredjobunkids " & _
              ", userunkid " & _
              ", isactive " & _
             "FROM rcstaffrequisition_transfer_approver " & _
             "WHERE name = @name " & _
             "AND code = @code "

            If intUnkid > 0 Then
                strQ &= " AND staffreqtransferapproverunkid <> @staffreqtransferapproverunkid"
            End If

            objDataOperation.AddParameter("@code", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strCode)
            objDataOperation.AddParameter("@name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strName)
            objDataOperation.AddParameter("@staffreqtransferapproverunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.tables(0).rows.count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isExist; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function


End Class
