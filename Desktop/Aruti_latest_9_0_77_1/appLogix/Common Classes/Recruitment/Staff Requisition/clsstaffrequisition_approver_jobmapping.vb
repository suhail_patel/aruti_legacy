﻿'************************************************************************************************************************************
'Class Name : clsstaffrequisition_approver_jobmapping.vb
'Purpose    :
'Date       :11/02/2021
'Written By :Pinkal
'Modified   :
'************************************************************************************************************************************

Imports eZeeCommonLib
''' <summary>
''' Purpose: 
''' Developer: Pinkal
''' </summary>
Public Class clsstaffrequisition_approver_jobmapping
    Private Const mstrModuleName = "clsstaffrequisition_approver_jobmapping"
    Dim objDataOperation As clsDataOperation
    Dim mstrMessage As String = ""

#Region " Private variables "
    Private mintStaffrequisitionjobapproverunkid As Integer
    Private mintUserapproverunkid As Integer
    Private mintLevelunkid As Integer
    Private mintAllocationId As Integer
    Private mintJobunkid As Integer
    Private mintUserunkid As Integer
    Private mblnIsvoid As Boolean
    Private mintVoiduserunkid As Integer
    Private mdtVoiddatetime As Date
    Private mstrVoidreason As String = String.Empty
    Private mstrWebClientIP As String = ""
    Private mstrWebHostName As String = ""
    Private mstrWebFormName As String = String.Empty
    Private mblnIsFromWeb As Boolean = False
#End Region

#Region " Properties "
    ''' <summary>
    ''' Purpose: Get Message from Class 
    ''' Modify By: Pinkal
    ''' </summary>
    Public ReadOnly Property _Message() As String
        Get
            Return mstrMessage
        End Get
    End Property

    ''' <summary>
    ''' Purpose: Get or Set staffrequisitionjobapproverunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Staffrequisitionjobapproverunkid(Optional ByVal objDoOperation As clsDataOperation = Nothing) As Integer
        Get
            Return mintStaffrequisitionjobapproverunkid
        End Get
        Set(ByVal value As Integer)
            mintStaffrequisitionjobapproverunkid = value
            Call GetData(objDoOperation)
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set staffrequisitionapproverunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Userapproverunkid() As Integer
        Get
            Return mintUserapproverunkid
        End Get
        Set(ByVal value As Integer)
            mintUserapproverunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set levelunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Levelunkid() As Integer
        Get
            Return mintLevelunkid
        End Get
        Set(ByVal value As Integer)
            mintLevelunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set AllocationId
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _AllocationId() As Integer
        Get
            Return mintAllocationId
        End Get
        Set(ByVal value As Integer)
            mintAllocationId = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set jobunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Jobunkid() As Integer
        Get
            Return mintJobunkid
        End Get
        Set(ByVal value As Integer)
            mintJobunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set userunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Userunkid() As Integer
        Get
            Return mintUserunkid
        End Get
        Set(ByVal value As Integer)
            mintUserunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isvoid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Isvoid() As Boolean
        Get
            Return mblnIsvoid
        End Get
        Set(ByVal value As Boolean)
            mblnIsvoid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiduserunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Voiduserunkid() As Integer
        Get
            Return mintVoiduserunkid
        End Get
        Set(ByVal value As Integer)
            mintVoiduserunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiddatetime
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Voiddatetime() As Date
        Get
            Return mdtVoiddatetime
        End Get
        Set(ByVal value As Date)
            mdtVoiddatetime = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voidreason
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Voidreason() As String
        Get
            Return mstrVoidreason
        End Get
        Set(ByVal value As String)
            mstrVoidreason = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set WebClientIP
    ''' Modify By: Hemant
    ''' </summary>
    Public Property _WebClientIP() As String
        Get
            Return mstrWebClientIP
        End Get
        Set(ByVal value As String)
            mstrWebClientIP = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set WebHostName
    ''' Modify By: Hemant
    ''' </summary>
    Public Property _WebHostName() As String
        Get
            Return mstrWebHostName
        End Get
        Set(ByVal value As String)
            mstrWebHostName = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set WebFormName
    ''' Modify By: Hemant
    ''' </summary>
    Public Property _WebFormName() As String
        Get
            Return mstrWebFormName
        End Get
        Set(ByVal value As String)
            mstrWebFormName = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set IsFromWeb
    ''' Modify By: Hemant
    ''' </summary>
    Public Property _IsFromWeb() As Boolean
        Get
            Return mblnIsFromWeb
        End Get
        Set(ByVal value As Boolean)
            mblnIsFromWeb = value
        End Set
    End Property

#End Region

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Sub GetData(Optional ByVal objDoOperation As clsDataOperation = Nothing)
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        If objDoOperation Is Nothing Then
            objDataOperation = New clsDataOperation
        Else
            objDataOperation = objDoOperation
        End If

        Try
            strQ = "SELECT " & _
                      "  staffrequisitionjobapproverunkid " & _
                      ", userapproverunkid " & _
                      ", levelunkid " & _
                      ", allocationid " & _
                      ", jobunkid " & _
                      ", userunkid " & _
                      ", isvoid " & _
                      ", voiduserunkid " & _
                      ", voiddatetime " & _
                      ", voidreason " & _
                     "FROM rcstaffrequisition_approver_jobmapping " & _
                     "WHERE staffrequisitionjobapproverunkid = @staffrequisitionjobapproverunkid "

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@staffrequisitionjobapproverunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintStaffrequisitionjobapproverunkid.ToString)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For Each dtRow As DataRow In dsList.Tables(0).Rows
                mintStaffrequisitionjobapproverunkid = CInt(dtRow.Item("staffrequisitionjobapproverunkid"))
                mintUserapproverunkid = CInt(dtRow.Item("userapproverunkid"))
                mintLevelunkid = CInt(dtRow.Item("levelunkid"))
                mintAllocationId = CInt(dtRow.Item("allocationid"))
                mintJobunkid = CInt(dtRow.Item("jobunkid"))
                mintUserunkid = CInt(dtRow.Item("userunkid"))
                mblnIsvoid = CBool(dtRow.Item("isvoid"))
                mintVoiduserunkid = CInt(dtRow.Item("voiduserunkid"))

                If IsDBNull(dtRow.Item("voiddatetime")) = False Then
                    mdtVoiddatetime = dtRow.Item("voiddatetime")
                End If
                mstrVoidreason = dtRow.Item("voidreason").ToString
                Exit For
            Next
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetData; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If objDoOperation Is Nothing Then objDataOperation = Nothing
        End Try
    End Sub

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function GetList(ByVal strTableName As String, ByVal xUserApproverUnkId As Integer _
                                    , Optional ByVal xLevelId As Integer = 0 _
                                    , Optional ByVal xAllocationId As Integer = 0 _
                                    , Optional ByVal strFilter As String = "" _
                                    , Optional ByVal objDoOperation As clsDataOperation = Nothing _
                                    , Optional ByVal strOrderBy As String = "" _
                                    ) As DataSet
        'Sohail (31 Jan 2022) - [strOrderBy]
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        If objDoOperation Is Nothing Then
            objDataOperation = New clsDataOperation
        Else
            objDataOperation = objDoOperation
        End If

        objDataOperation.ClearParameters()

        Try
            strQ = "SELECT " & _
                      "  rcstaffrequisition_approver_jobmapping.staffrequisitionjobapproverunkid " & _
                      ", rcstaffrequisition_approver_jobmapping.userapproverunkid " & _
                      ", rcstaffrequisition_approver_jobmapping.levelunkid " & _
                      ", rcstaffrequisition_approver_jobmapping.allocationid " & _
                      ", rcstaffrequisition_approver_jobmapping.jobunkid " & _
                      ", ISNULL(hrjob_master.job_name,'') AS Job " & _
                      ", rcstaffrequisition_approver_jobmapping.userunkid " & _
                      ", rcstaffrequisition_approver_jobmapping.isvoid " & _
                      ", rcstaffrequisition_approver_jobmapping.voiduserunkid " & _
                      ", rcstaffrequisition_approver_jobmapping.voiddatetime " & _
                      ", rcstaffrequisition_approver_jobmapping.voidreason " & _
                      ", '' AS AUD " & _
                      ", CAST(0 AS BIT) AS IsChecked " & _
                      " FROM rcstaffrequisition_approver_jobmapping " & _
                      " LEFT JOIN hrjob_master ON hrjob_master.jobunkid = rcstaffrequisition_approver_jobmapping.jobunkid " & _
                      " WHERE rcstaffrequisition_approver_jobmapping.isvoid = 0  AND rcstaffrequisition_approver_jobmapping.userapproverunkid = @userapproverunkid "
            'Sohail (31 Jan 2022) - [IsChecked]

            objDataOperation.AddParameter("@userapproverunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, xUserApproverUnkId)

            If xLevelId <> 0 Then
                strQ &= " AND levelunkid = @levelunkid "
                objDataOperation.AddParameter("@levelunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, xLevelId)
            End If

            If xAllocationId > 0 Then
                strQ &= " AND allocationid = @allocationid "
                objDataOperation.AddParameter("@allocationid", SqlDbType.Int, eZeeDataType.INT_SIZE, xAllocationId)
            End If

            If strFilter.Trim.Length > 0 Then
                strQ &= " AND " & strFilter
            End If

            'Sohail (31 Jan 2022) -- Start
            'Enhancement :  OLD-547 : NMB - Creation of staff requisition approver migration.
            If strOrderBy.Trim <> "" Then
                strQ &= " ORDER BY " & strOrderBy
            End If
            'Sohail (31 Jan 2022) -- End

            dsList = objDataOperation.ExecQuery(strQ, strTableName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If objDoOperation Is Nothing Then objDataOperation = Nothing
        End Try
        Return dsList
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> INSERT INTO Database Table (rcstaffrequisition_approver_jobmapping) </purpose>
    Public Function Insert(Optional ByVal objDataOp As clsDataOperation = Nothing) As Boolean
        If isExist(mintUserapproverunkid, mintLevelunkid, mintAllocationId, mintJobunkid, mintStaffrequisitionjobapproverunkid, objDataOp) Then
            'Sohail (31 Jan 2022) - [mintLevelunkid]
            mstrMessage = Language.getMessage(mstrModuleName, 1, "Sorry, This approver is already mapped with some level.")
            Return False
        End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        If objDataOp Is Nothing Then
            objDataOperation = New clsDataOperation
            objDataOperation.BindTransaction()
        Else
            objDataOperation = objDataOp
        End If

        Try
            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@userapproverunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserapproverunkid.ToString)
            objDataOperation.AddParameter("@levelunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLevelunkid.ToString)
            objDataOperation.AddParameter("@allocationid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAllocationId.ToString)
            objDataOperation.AddParameter("@jobunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintJobunkid.ToString)
            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
            objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, IIf(mdtVoiddatetime <> Nothing, mdtVoiddatetime, DBNull.Value))
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)

            strQ = "INSERT INTO rcstaffrequisition_approver_jobmapping ( " & _
                      "  userapproverunkid " & _
                      ", levelunkid " & _
                      ", allocationid " & _
                      ", jobunkid " & _
                      ", userunkid " & _
                      ", isvoid " & _
                      ", voiduserunkid " & _
                      ", voiddatetime " & _
                      ", voidreason" & _
                    ") VALUES (" & _
                      "  @userapproverunkid " & _
                      ", @levelunkid " & _
                      ", @allocationid " & _
                      ", @jobunkid " & _
                      ", @userunkid " & _
                      ", @isvoid " & _
                      ", @voiduserunkid " & _
                      ", @voiddatetime " & _
                      ", @voidreason" & _
                    "); SELECT @@identity"

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            mintStaffrequisitionjobapproverunkid = dsList.Tables(0).Rows(0).Item(0)


            If ATInsertstaffrequisition_approver_jobmapping(objDataOperation, enAuditType.ADD) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If objDataOp Is Nothing Then
                objDataOperation.ReleaseTransaction(True)
            End If

            Return True
        Catch ex As Exception
            If objDataOp Is Nothing Then objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Insert; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Update Database Table (rcstaffrequisition_approver_jobmapping) </purpose>
    Public Function Update() As Boolean
        If isExist(mintUserapproverunkid, mintLevelunkid, mintAllocationId, mintJobunkid, mintStaffrequisitionjobapproverunkid) Then
            'Sohail (31 Jan 2022) - [mintLevelunkid]
            mstrMessage = Language.getMessage(mstrModuleName, 1, "Sorry, This approver is already mapped with some level.")
            Return False
        End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation
        objDataOperation.BindTransaction()

        Try
            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@staffrequisitionjobapproverunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintStaffrequisitionjobapproverunkid.ToString)
            objDataOperation.AddParameter("@userapproverunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserapproverunkid.ToString)
            objDataOperation.AddParameter("@levelunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLevelunkid.ToString)
            objDataOperation.AddParameter("@allocationid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAllocationId.ToString)
            objDataOperation.AddParameter("@jobunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintJobunkid.ToString)
            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
            objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, IIf(mdtVoiddatetime <> Nothing, mdtVoiddatetime, DBNull.Value))
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)

            strQ = "UPDATE rcstaffrequisition_approver_jobmapping SET " & _
                      "  userapproverunkid = @userapproverunkid" & _
                      ", levelunkid = @levelunkid" & _
                      ", allocationid = @allocationid " & _
                      ", jobunkid = @jobunkid" & _
                      ", userunkid = @userunkid" & _
                      ", isvoid = @isvoid" & _
                      ", voiduserunkid = @voiduserunkid" & _
                      ", voiddatetime = @voiddatetime" & _
                      ", voidreason = @voidreason " & _
                      " WHERE staffrequisitionjobapproverunkid = @staffrequisitionjobapproverunkid "

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If ATInsertstaffrequisition_approver_jobmapping(objDataOperation, enAuditType.EDIT) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            objDataOperation.ReleaseTransaction(True)

            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Delete Database Table (rcstaffrequisition_approver_jobmapping) </purpose>
    Public Function Delete(ByVal intUnkid As Integer, Optional ByVal objDataOp As clsDataOperation = Nothing) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        If objDataOp Is Nothing Then
            objDataOperation = New clsDataOperation
            objDataOperation.BindTransaction()
        Else
            objDataOperation = objDataOp
        End If

        Try
            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
            objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime.ToString)
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)

            strQ = "UPDATE rcstaffrequisition_approver_jobmapping SET " & _
                   "  isvoid = @isvoid" & _
                   " ,voiduserunkid = @voiduserunkid" & _
                   " ,voiddatetime = @voiddatetime" & _
                   " ,voidreason = @voidreason " & _
                   " WHERE staffrequisitionjobapproverunkid = @staffrequisitionjobapproverunkid "

            objDataOperation.AddParameter("@staffrequisitionjobapproverunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If ATInsertstaffrequisition_approver_jobmapping(objDataOperation, enAuditType.DELETE) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If objDataOp Is Nothing Then objDataOperation.ReleaseTransaction(True)

            Return True
        Catch ex As Exception
            If objDataOp Is Nothing Then objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Delete; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If objDataOp Is Nothing Then objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isExist(ByVal xUserApproverId As Integer, ByVal xLevelUnkID As Integer, ByVal xAllocationID As Integer, ByVal xJobID As Integer, Optional ByVal intUnkid As Integer = -1, Optional ByVal objDataOp As clsDataOperation = Nothing) As Boolean
        'Sohail (31 Jan 2022) - [xLevelUnkID]
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        If objDataOp IsNot Nothing Then
            objDataOperation = objDataOp
            objDataOperation.ClearParameters()
        Else
            objDataOperation = New clsDataOperation
        End If

        Try
            strQ = "SELECT " & _
                   "  userapproverunkid " & _
                   " ,levelunkid " & _
                   ", allocationid " & _
                   " ,jobunkid " & _
                   " ,userunkid " & _
                   " ,isvoid " & _
                   " ,voiduserunkid " & _
                   " ,voiddatetime " & _
                   " ,voidreason " & _
                   " FROM rcstaffrequisition_approver_jobmapping " & _
                   " WHERE ISNULL(isvoid, 0) = 0 " & _
                   " AND userapproverunkid = @userapproverunkid " & _
                   " AND jobunkid = @jobunkid " & _
                   " AND allocationid = @allocationid " & _
                   " AND levelunkid = @levelunkid "
            'Sohail (31 Jan 2022) - [@levelunkid]

            If intUnkid > 0 Then
                strQ &= " AND staffrequisitionjobapproverunkid <> @staffrequisitionjobapproverunkid"
            End If

            objDataOperation.AddParameter("@jobunkid", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, xJobID)
            objDataOperation.AddParameter("@userapproverunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, xUserApproverId)
            objDataOperation.AddParameter("@allocationid", SqlDbType.Int, eZeeDataType.INT_SIZE, xAllocationID)
            objDataOperation.AddParameter("@levelunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, xLevelUnkID) 'Sohail (31 Jan 2022)
            objDataOperation.AddParameter("@staffrequisitionjobapproverunkid", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, intUnkid)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isExist; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If objDataOp Is Nothing Then objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' 
    Public Function ATInsertstaffrequisition_approver_jobmapping(ByVal objDataOperation As clsDataOperation, ByVal AuditType As Integer) As Boolean
        Dim strQ As String = ""
        Dim exForce As Exception
        Try
            strQ = "INSERT INTO atrcstaffrequisition_approver_jobmapping ( " & _
                      " staffrequisitionjobapproverunkid " & _
                      ", userapproverunkid " & _
                      ", levelunkid " & _
                      ", allocationid " & _
                      ", jobunkid " & _
                      ", audittype " & _
                      ", audituserunkid " & _
                      ", auditdatetime " & _
                      ", ip" & _
                      ", machine_name" & _
                      ", form_name " & _
                      ", isweb " & _
                      " ) VALUES (" & _
                      "  @staffrequisitionjobapproverunkid " & _
                      ", @userapproverunkid " & _
                      ", @levelunkid " & _
                      ", @allocationid " & _
                      ", @jobunkid " & _
                      ", @audittype " & _
                      ", @audituserunkid " & _
                      ", GETDATE() " & _
                      ", @ip" & _
                      ", @machine_name" & _
                      ", @form_name " & _
                      ", @isweb " & _
                      ") "


            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@staffrequisitionjobapproverunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintStaffrequisitionjobapproverunkid.ToString)
            objDataOperation.AddParameter("@userapproverunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserapproverunkid.ToString)
            objDataOperation.AddParameter("@levelunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLevelunkid.ToString)
            objDataOperation.AddParameter("@allocationid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAllocationId.ToString)
            objDataOperation.AddParameter("@jobunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintJobunkid.ToString)
            objDataOperation.AddParameter("@audittype", SqlDbType.Int, eZeeDataType.INT_SIZE, AuditType.ToString)
            objDataOperation.AddParameter("@audituserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid)
            objDataOperation.AddParameter("@ip", SqlDbType.NVarChar, eZeeDataType.IP_SIZE, mstrWebClientIP.Trim)
            objDataOperation.AddParameter("@machine_name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrWebHostName.Trim)
            objDataOperation.AddParameter("@form_name", SqlDbType.NVarChar, 500, mstrWebFormName)
            objDataOperation.AddParameter("@isweb", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsFromWeb)
            objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: ATInsertstaffrequisition_approver_jobmapping; Module Name: " & mstrModuleName)
            Return False
        Finally
        End Try
    End Function

End Class
