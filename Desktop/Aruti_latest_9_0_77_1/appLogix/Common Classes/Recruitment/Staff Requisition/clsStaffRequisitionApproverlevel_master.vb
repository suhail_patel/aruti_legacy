﻿'************************************************************************************************************************************
'Class Name : clsStaffRequisitionApproverlevel_master.vb
'Purpose    : All Approver Level Opration like getList, Insert, Update, Delete, checkDuplicate
'Date       :19/05/2014
'Written By :Sohail
'Modified   :
'************************************************************************************************************************************

Imports eZeeCommonLib
''' <summary>
''' Purpose: 
''' Developer: Sohail
''' </summary>
Public Class clsStaffRequisitionApproverlevel_master

    Private Shared ReadOnly mstrModuleName As String = "clsStaffRequisitionApproverlevel_master"
    Dim objDataOperation As clsDataOperation
    Dim mstrMessage As String = ""

#Region " Private variables "
    Private mintLevelunkid As Integer
    Private mstrLevelcode As String = String.Empty
    Private mstrLevelname As String = String.Empty
    Private mintPriority As Integer
    Private mintUserunkid As Integer
    Private mblnIsactive As Boolean = True
    Private mstrLevelname1 As String = String.Empty
    Private mstrLevelname2 As String = String.Empty

#End Region

#Region " Properties "
'S.SANDEEP [28-May-2018] -- START 
'ISSUE/ENHANCEMENT : {Audit Trails} 
Private mstrFormName As String = String.Empty
Public WriteOnly Property _FormName() As String 
Set(ByVal value As String) 
mstrFormName = value 
End Set 
End Property 
 
Private mstrClientIP As String = ""
Public WriteOnly Property _ClientIP() As String 
Set(ByVal value As String) 
mstrClientIP = value 
End Set 
End Property 
 
Private mstrHostName As String = ""
Public WriteOnly Property _HostName() As String 
Set(ByVal value As String) 
mstrHostName = value 
End Set 
End Property 
 
Private mblnIsWeb As Boolean = False
Public WriteOnly Property _FromWeb() As Boolean 
Set(ByVal value As Boolean) 
mblnIsWeb = value 
End Set 
End Property 
 
    Private mintAuditUserId As Integer = 0
    Public WriteOnly Property _AuditUserId() As Integer
        Set(ByVal value As Integer)
            mintAuditUserId = value
        End Set
    End Property

    Private mintCompanyUnkid As Integer = 0
    Public WriteOnly Property _CompanyUnkid() As Integer
        Set(ByVal value As Integer)
            mintCompanyUnkid = value
        End Set
    End Property
 
Private mdtAuditDate As DateTime = Now
Public WriteOnly Property _AuditDate() As DateTime 
Set(ByVal value As DateTime) 
mdtAuditDate = value 
End Set 
End Property 
 
Private xDataOpr As clsDataOperation = Nothing
Public Property _xDataOpr() As clsDataOperation 
Get 
Return xDataOpr 
End Get 
Set(ByVal value As clsDataOperation) 
xDataOpr = value 
End Set 
End Property 
 
Private mintLoginEmployeeunkid As Integer = 0
Public Property _LoginEmployeeunkid() As Integer 
Get 
Return mintLoginEmployeeunkid 
End Get 
Set(ByVal value As Integer) 
mintLoginEmployeeunkid = value 
End Set 
End Property 
 
Private mintVoidlogingemployeeunkid As Integer=0
Public Property _Voidloginemployeeunkid() As Integer 
Get 
Return mintVoidlogingemployeeunkid 
End Get 
Set(ByVal value As Integer) 
mintVoidlogingemployeeunkid = value 
End Set 
End Property 
'S.SANDEEP [28-May-2018] -- END 
    ''' <summary>
    ''' Purpose: Get Message from Class 
    ''' Modify By: Sohail
    ''' </summary>
    Public ReadOnly Property _Message() As String
        Get
            Return mstrMessage
        End Get
    End Property

    ''' <summary>
    ''' Purpose: Get or Set levelunkid
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Levelunkid() As Integer
        Get
            Return mintLevelunkid
        End Get
        Set(ByVal value As Integer)
            mintLevelunkid = value
            Call GetData()
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set levelcode
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Levelcode() As String
        Get
            Return mstrLevelcode
        End Get
        Set(ByVal value As String)
            mstrLevelcode = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set levelname
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Levelname() As String
        Get
            Return mstrLevelname
        End Get
        Set(ByVal value As String)
            mstrLevelname = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set priority
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Priority() As Integer
        Get
            Return mintPriority
        End Get
        Set(ByVal value As Integer)
            mintPriority = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set userunkid
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Userunkid() As Integer
        Get
            Return mintUserunkid
        End Get
        Set(ByVal value As Integer)
            mintUserunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isactive
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Isactive() As Boolean
        Get
            Return mblnIsactive
        End Get
        Set(ByVal value As Boolean)
            mblnIsactive = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set levelname1
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Levelname1() As String
        Get
            Return mstrLevelname1
        End Get
        Set(ByVal value As String)
            mstrLevelname1 = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set levelname2
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Levelname2() As String
        Get
            Return mstrLevelname2
        End Get
        Set(ByVal value As String)
            mstrLevelname2 = value
        End Set
    End Property

#End Region

    ''' <summary>
    ''' Modify By: Sohail
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Sub GetData()
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "SELECT " & _
              "  levelunkid " & _
              ", levelcode " & _
              ", levelname " & _
              ", priority " & _
              ", userunkid " & _
              ", isactive " & _
              ", levelname1 " & _
              ", levelname2 " & _
             "FROM rcstaffrequisitionlevel_master " & _
             "WHERE levelunkid = @levelunkid "

            objDataOperation.AddParameter("@levelunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLevelunkid.ToString)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For Each dtRow As DataRow In dsList.Tables(0).Rows
                mintLevelunkid = CInt(dtRow.Item("levelunkid"))
                mstrLevelcode = dtRow.Item("levelcode").ToString
                mstrLevelname = dtRow.Item("levelname").ToString
                If dtRow.Item("priority") IsNot DBNull.Value Then mintPriority = dtRow.Item("priority")
                mintUserunkid = CInt(dtRow.Item("userunkid"))
                mblnIsactive = CBool(dtRow.Item("isactive"))
                mstrLevelname1 = dtRow.Item("levelname1").ToString
                mstrLevelname2 = dtRow.Item("levelname2").ToString
                Exit For
            Next
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetData; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Sub


    ''' <summary>
    ''' Modify By: Sohail
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function GetList(ByVal strTableName As String, Optional ByVal blnOnlyActive As Boolean = True _
                            , Optional ByVal blnOrderByPriority As Boolean = False) As DataSet

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "SELECT " & _
              "  levelunkid " & _
              ", levelcode " & _
              ", levelname " & _
              ", priority " & _
              ", userunkid " & _
              ", isactive " & _
              ", levelname1 " & _
              ", levelname2 " & _
             "FROM rcstaffrequisitionlevel_master " & _
             "WHERE 1 = 1 "

            If blnOnlyActive Then
                strQ &= " AND isactive = 1 "
            End If

            If blnOrderByPriority = True Then
                strQ &= " ORDER BY priority  "
            End If

            dsList = objDataOperation.ExecQuery(strQ, strTableName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
        Return dsList
    End Function

    Public Function GetLevelListCount() As Integer
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "SELECT "
            strQ = "SELECT  COUNT(levelunkid) AS TotalCount " & _
                    "FROM    rcstaffrequisitionlevel_master " & _
                    "WHERE   isactive = 1 "

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables("List").Rows(0).Item("TotalCount")

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetLevelListCount; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function


    ''' <summary>
    ''' Modify By: Sohail
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> INSERT INTO Database Table (rcstaffrequisitionlevel_master) </purpose>
    Public Function Insert() As Boolean
        If isExist(mstrLevelcode) Then
            mstrMessage = Language.getMessage(mstrModuleName, 1, "This Approver Level Code is already defined. Please define new Approver Level Code.")
            Return False
        ElseIf isExist("", mstrLevelname) Then
            mstrMessage = Language.getMessage(mstrModuleName, 2, "This Approver Level Name is already defined. Please define new Approver Level Name.")
            Return False
        ElseIf isPriorityExist(mintPriority) Then
            mstrMessage = Language.getMessage(mstrModuleName, 3, "This Approver Level Priority is already assign. Please assign new Approver Level Priority.")
            Return False
        End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation
        objDataOperation.BindTransaction()

        Try
            objDataOperation.AddParameter("@levelcode", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrLevelcode.ToString)
            objDataOperation.AddParameter("@levelname", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrLevelname.ToString)
            objDataOperation.AddParameter("@priority", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPriority.ToString)
            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
            objDataOperation.AddParameter("@isactive", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsactive.ToString)
            objDataOperation.AddParameter("@levelname1", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrLevelname1.ToString)
            objDataOperation.AddParameter("@levelname2", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrLevelname2.ToString)

            strQ = "INSERT INTO rcstaffrequisitionlevel_master ( " & _
              "  levelcode " & _
              ", levelname " & _
              ", priority " & _
              ", userunkid " & _
              ", isactive " & _
              ", levelname1 " & _
              ", levelname2" & _
            ") VALUES (" & _
              "  @levelcode " & _
              ", @levelname " & _
              ", @priority " & _
              ", @userunkid " & _
              ", @isactive " & _
              ", @levelname1 " & _
              ", @levelname2" & _
            "); SELECT @@identity"

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            mintLevelunkid = dsList.Tables(0).Rows(0).Item(0)

'S.SANDEEP [28-May-2018] -- START
'ISSUE/ENHANCEMENT : {Audit Trails} 
Dim objCommonATLog AS New clsCommonATLog 
objCommonATLog._FormName = mstrFormName 
objCommonATLog._LoginEmployeeUnkid = mintLoginEmployeeunkid 
            objCommonATLog._ClientIP = mstrClientIP
            objCommonATLog._HostName = mstrHostName
            objCommonATLog._FromWeb = mblnIsWeb
            objCommonATLog._AuditUserId = mintAuditUserId
objCommonATLog._CompanyUnkid = mintCompanyUnkid
            objCommonATLog._AuditDate = mdtAuditDate
            'S.SANDEEP [28-May-2018] -- END

            If objCommonATLog.Insert_AtLog(objDataOperation, 1, "rcstaffrequisitionlevel_master", "levelunkid", mintLevelunkid) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            'S.SANDEEP [28-May-2018] -- START
            'ISSUE/ENHANCEMENT : {Audit Trails} 
            objCommonATLog = Nothing
            'S.SANDEEP [28-May-2018] -- END

            objDataOperation.ReleaseTransaction(True)

            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Insert; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sohail
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Update Database Table (rcstaffrequisitionlevel_master) </purpose>
    Public Function Update() As Boolean
        If isExist(mstrLevelcode, "", mintLevelunkid) Then
            mstrMessage = Language.getMessage(mstrModuleName, 1, "This Approver Level Code is already defined. Please define new Approver Level Code.")
            Return False
        ElseIf isExist("", mstrLevelname, mintLevelunkid) Then
            mstrMessage = Language.getMessage(mstrModuleName, 2, "This Approver Level Name is already defined. Please define new Approver Level Name.")
            Return False
        ElseIf isPriorityExist(mintPriority, mintLevelunkid) Then
            mstrMessage = Language.getMessage(mstrModuleName, 3, "This Approver Level Priority is already assign. Please assign new Approver Level Priority.")
            Return False
        End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation
        objDataOperation.BindTransaction()

        Try
            objDataOperation.AddParameter("@levelunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLevelunkid.ToString)
            objDataOperation.AddParameter("@levelcode", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrLevelcode.ToString)
            objDataOperation.AddParameter("@levelname", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrLevelname.ToString)
            objDataOperation.AddParameter("@priority", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPriority.ToString)
            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
            objDataOperation.AddParameter("@isactive", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsactive.ToString)
            objDataOperation.AddParameter("@levelname1", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrLevelname1.ToString)
            objDataOperation.AddParameter("@levelname2", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrLevelname2.ToString)

            strQ = "UPDATE rcstaffrequisitionlevel_master SET " & _
              "  levelcode = @levelcode " & _
              ", levelname = @levelname " & _
              ", priority  = @priority  " & _
              ", userunkid  = @userunkid  " & _
              ", isactive = @isactive" & _
              ", levelname1 = @levelname1" & _
              ", levelname2 = @levelname2 " & _
            "WHERE levelunkid = @levelunkid "

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            'S.SANDEEP [28-May-2018] -- START
            'ISSUE/ENHANCEMENT : {Audit Trails} 
            Dim objCommonATLog As New clsCommonATLog
            objCommonATLog._FormName = mstrFormName
            objCommonATLog._LoginEmployeeUnkid = mintLoginEmployeeunkid
            objCommonATLog._ClientIP = mstrClientIP
            objCommonATLog._HostName = mstrHostName
            objCommonATLog._FromWeb = mblnIsWeb
            objCommonATLog._AuditUserId = mintAuditUserId
objCommonATLog._CompanyUnkid = mintCompanyUnkid
            objCommonATLog._AuditDate = mdtAuditDate
            'S.SANDEEP [28-May-2018] -- END

            If objCommonATLog.IsTableDataUpdate("atcommon_log", "rcstaffrequisitionlevel_master", mintLevelunkid, "levelunkid", 2) Then
                If objCommonATLog.Insert_AtLog(objDataOperation, 2, "rcstaffrequisitionlevel_master", "levelunkid", mintLevelunkid) = False Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If
            End If

            'S.SANDEEP [28-May-2018] -- START
            'ISSUE/ENHANCEMENT : {Audit Trails} 
            objCommonATLog = Nothing
            'S.SANDEEP [28-May-2018] -- END


            objDataOperation.ReleaseTransaction(True)


            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sohail
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Delete Database Table (rcstaffrequisitionlevel_master) </purpose>
    Public Function Delete(ByVal intUnkid As Integer) As Boolean
        'If isUsed(intUnkid) Then
        '    mstrMessage = Language.getMessage(mstrModuleName, 4, "Sorry, You cannot delete selected Approver Level. Reason: This Approver Level is in use.")
        '    Return False
        'End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation
        objDataOperation.BindTransaction()

        Try
            strQ = "Update rcstaffrequisitionlevel_master set isactive = 0 " & _
            "WHERE levelunkid = @levelunkid "

            objDataOperation.AddParameter("@levelunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            'S.SANDEEP [28-May-2018] -- START
            'ISSUE/ENHANCEMENT : {Audit Trails} 
            Dim objCommonATLog As New clsCommonATLog
            objCommonATLog._FormName = mstrFormName
            objCommonATLog._LoginEmployeeUnkid = mintLoginEmployeeunkid
            objCommonATLog._ClientIP = mstrClientIP
            objCommonATLog._HostName = mstrHostName
            objCommonATLog._FromWeb = mblnIsWeb
            objCommonATLog._AuditUserId = mintAuditUserId
objCommonATLog._CompanyUnkid = mintCompanyUnkid
            objCommonATLog._AuditDate = mdtAuditDate
            'S.SANDEEP [28-May-2018] -- END

            If objCommonATLog.Insert_AtLog(objDataOperation, 3, "rcstaffrequisitionlevel_master", "levelunkid", intUnkid) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            'S.SANDEEP [28-May-2018] -- START
            'ISSUE/ENHANCEMENT : {Audit Trails} 
            objCommonATLog = Nothing
            'S.SANDEEP [28-May-2018] -- END

            objDataOperation.ReleaseTransaction(True)

            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sohail
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isUsed(ByVal intUnkid As Integer) As Boolean
        Dim strQ As String = ""
        Dim exForce As Exception
        objDataOperation = New clsDataOperation
        Try
            strQ = "select isnull(levelunkid,0) from rcstaffrequisition_approver_mapping WHERE ISNULL(isvoid, 0) = 0 AND levelunkid = @levelunkid"
            objDataOperation.AddParameter("@levelunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)
            If CInt(objDataOperation.RecordCount(strQ)) > 0 Then Return True

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isUsed; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sohail
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isExist(Optional ByVal strCode As String = "", Optional ByVal strName As String = "", Optional ByVal intUnkid As Integer = -1) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "SELECT " & _
              "  levelunkid " & _
              ", levelcode " & _
              ", levelname " & _
              ", priority " & _
              ", userunkid " & _
              ", isactive " & _
              ", levelname1 " & _
              ", levelname2 " & _
             "FROM rcstaffrequisitionlevel_master " & _
             "WHERE 1=1 "

            If strCode.Length > 0 Then
                strQ &= " AND levelcode = @code "
            End If
            If strName.Length > 0 Then
                strQ &= " AND levelname = @name "
            End If

            If intUnkid > 0 Then
                strQ &= " AND levelunkid <> @levelunkid"
            End If

            objDataOperation.AddParameter("@code", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strCode)
            objDataOperation.AddParameter("@name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strName)
            objDataOperation.AddParameter("@levelunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isExist; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function


    ''' <summary>
    ''' Modify By: Sohail
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isPriorityExist(ByVal mintPriority As Integer, Optional ByVal intUnkid As Integer = -1) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "SELECT " & _
              "  levelunkid " & _
              ", levelcode " & _
              ", levelname " & _
              ", priority " & _
              ", isactive " & _
              ", levelname1 " & _
              ", levelname2 " & _
             "FROM rcstaffrequisitionlevel_master " & _
             "WHERE priority = @priority"

            If intUnkid > 0 Then
                strQ &= " AND levelunkid <> @levelunkid"
            End If

            objDataOperation.AddParameter("@priority", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPriority)
            objDataOperation.AddParameter("@levelunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isPriorityExist; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function


    ''' <summary>
    ''' Modify By: Sohail
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function getListForCombo(Optional ByVal strListName As String = "List", Optional ByVal mblFlag As Boolean = False) As DataSet
        Dim dsList As New DataSet
        Dim objDataOperation As New clsDataOperation
        Dim strQ As String = String.Empty
        Dim exForce As Exception
        Try
            If mblFlag = True Then
                strQ = "SELECT 0 as levelunkid, ' ' +  @name  as name, ' ' AS code UNION "
            End If
            strQ &= "SELECT levelunkid, levelname as name, levelcode AS code FROM rcstaffrequisitionlevel_master where isactive = 1 ORDER BY name "

            objDataOperation.AddParameter("@name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 4, "Select"))
            dsList = objDataOperation.ExecQuery(strQ, strListName)
            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If
            Return dsList
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "getListForCombo", mstrModuleName)
            Return Nothing
        Finally
            exForce = Nothing
            objDataOperation = Nothing
            dsList.Dispose()
            dsList = Nothing
        End Try

    End Function

    Public Function GetLowerLevelPriority(ByVal intCurrentPriority As Integer _
                                          , ByVal mstrEmpAsOnDate As String _
                                          , Optional ByVal intAllocationID As Integer = 0 _
                                          , Optional ByVal intAllocationUnkID As Integer = 0 _
                                          , Optional ByVal intStaffReqTranUnkId As Integer = 0 _
                                          ) As Integer

        'Pinkal (01-Dec-2021)-- NMB Staff Requisition Approval Enhancements. [Optional byval  intStaffReqTranUnkId As Integer]

        'Pinkal (16-Nov-2021)-- NMB Staff Requisition Approval Enhancements. [ , ByVal mstrEmpAsOnDate As String _]

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation
        Try


            'Pinkal (16-Nov-2021)-- Start
            'NMB Staff Requisition Approval Enhancements. 

            'strQ = "SELECT  ISNULL(MAX(priority), -1) AS LowerLevelPriority " & _
            '        "FROM    rcstaffrequisition_approver_mapping " & _
            '                "LEFT JOIN rcstaffrequisitionlevel_master ON rcstaffrequisition_approver_mapping.levelunkid = rcstaffrequisitionlevel_master.levelunkid " & _
            '        "WHERE   isvoid = 0 " & _
            '                "AND isactive = 1 " & _
            '                "AND priority < @priority "


            'If intAllocationID > 0 Then
            '    strQ &= " AND allocationid = @allocationid "
            '    objDataOperation.AddParameter("@allocationid", SqlDbType.Int, eZeeDataType.INT_SIZE, intAllocationID)
            'End If

            'If intAllocationUnkID > 0 Then
            '    strQ &= " AND allocationunkid = @allocationunkid "
            '    objDataOperation.AddParameter("@allocationunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intAllocationUnkID)
            'End If


            strQ = " SELECT  ISNULL(MAX(priority), -1) AS LowerLevelPriority " & _
                      " FROM rcstaffrequisition_approver_mapping " & _
                      " JOIN rcstaffrequisition_approver_jobmapping ON rcstaffrequisition_approver_jobmapping.userapproverunkid =rcstaffrequisition_approver_mapping.userapproverunkid " & _
                      " AND rcstaffrequisition_approver_jobmapping.levelunkid = rcstaffrequisition_approver_mapping.levelunkid AND rcstaffrequisition_approver_jobmapping.allocationid = rcstaffrequisition_approver_mapping.allocationid " & _
                      " AND rcstaffrequisition_approver_jobmapping.isvoid = 0 " & _
                      " LEFT JOIN rcstaffrequisition_tran ON rcstaffrequisition_tran.staffrequisitionbyid = rcstaffrequisition_approver_mapping.allocationid " & _
                      " AND rcstaffrequisition_tran.allocationunkid = rcstaffrequisition_approver_mapping.allocationunkid " & _
                      " AND rcstaffrequisition_tran.jobunkid = rcstaffrequisition_approver_jobmapping.jobunkid " & _
                      " LEFT JOIN hrmsConfiguration..cfuser_master ON cfuser_master.userunkid = rcstaffrequisition_approver_mapping.userapproverunkid " & _
                      " LEFT JOIN (  " & _
                      "                  SELECT  " & _
                      "                      jobunkid " & _
                      "                     ,employeeunkid " & _
                      "                     ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno  " & _
                      "                  FROM hremployee_categorization_tran " & _
                      "                  WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= @EmpAsOnDate " & _
                      "              )  " & _
                      "  AS Jobs ON Jobs.employeeunkid = cfuser_master.employeeunkid " & _
                      " AND rcstaffrequisition_tran.jobunkid = Jobs.jobunkid " & _
                      " LEFT JOIN rcstaffrequisitionlevel_master ON rcstaffrequisition_approver_mapping.levelunkid = rcstaffrequisitionlevel_master.levelunkid " & _
                      " WHERE rcstaffrequisition_approver_mapping.isvoid = 0 AND rcstaffrequisitionlevel_master.isactive = 1 AND priority < @priority AND rcstaffrequisition_tran.isvoid = 0 AND jobs.jobunkid IS NULL"

            'Pinkal (01-Dec-2021)-- Start NMB Staff Requisition Approval Enhancements. [ " AND rcstaffrequisition_tran.jobunkid = rcstaffrequisition_approver_jobmapping.jobunkid " & _]

            If intAllocationID > 0 Then
                strQ &= " AND rcstaffrequisition_approver_mapping.allocationid = @allocationid "
                objDataOperation.AddParameter("@allocationid", SqlDbType.Int, eZeeDataType.INT_SIZE, intAllocationID)
            End If

            If intAllocationUnkID > 0 Then
                strQ &= " AND rcstaffrequisition_approver_mapping.allocationunkid = @allocationunkid "
                objDataOperation.AddParameter("@allocationunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intAllocationUnkID)
            End If
            'Sohail (15 Jul 2014) -- End

            'Pinkal (01-Dec-2021)-- Start
            'NMB Staff Requisition Approval Enhancements. 
            If intStaffReqTranUnkId > 0 Then
                strQ &= " AND rcstaffrequisition_tran.staffrequisitiontranunkid = @staffrequisitiontranunkid "
                objDataOperation.AddParameter("@staffrequisitiontranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intStaffReqTranUnkId)
            End If
            'Pinkal (01-Dec-2021)-- End

            strQ &= " ORDER BY LowerLevelPriority DESC"

            objDataOperation.AddParameter("@priority", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, intCurrentPriority)
            objDataOperation.AddParameter("@EmpAsOnDate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrEmpAsOnDate)

            'Pinkal (16-Nov-2021) -- End

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If dsList.Tables(0).Rows.Count > 0 Then
                Return CInt(dsList.Tables(0).Rows(0).Item("LowerLevelPriority"))
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetLowerLevelPriority", mstrModuleName)
        End Try
        Return -1
    End Function

    Public Function GetNextLevelPriority(ByVal intCurrentPriority As Integer _
                                         , ByVal mstrEmpAsOnDate As String _
                                         , Optional ByVal intAllocationID As Integer = 0 _
                                         , Optional ByVal intAllocationUnkID As Integer = 0 _
                                         , Optional ByVal intStaffReqTranUnkId As Integer = 0 _
                                         ) As Integer
        'Pinkal (01-Dec-2021)-- NMB Staff Requisition Approval Enhancements. [Optional byval  intStaffReqTranUnkId As Integer]

        'Pinkal (16-Nov-2021)-- NMB Staff Requisition Approval Enhancements. [ByVal mstrEmpAsOnDate As String]

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation
        Try


            'Pinkal (16-Nov-2021)-- Start
            'NMB Staff Requisition Approval Enhancements. 

            'strQ = "SELECT  ISNULL(MIN(priority), -1) AS NextLevelPriority " & _
            '        "FROM    rcstaffrequisition_approver_mapping " & _
            '                "LEFT JOIN rcstaffrequisitionlevel_master ON rcstaffrequisition_approver_mapping.levelunkid = rcstaffrequisitionlevel_master.levelunkid " & _
            '        "WHERE   isvoid = 0 " & _
            '                "AND isactive = 1 " & _
            '                "AND priority > @priority "

            'If intAllocationID > 0 Then
            '    strQ &= " AND allocationid = @allocationid "
            '    objDataOperation.AddParameter("@allocationid", SqlDbType.Int, eZeeDataType.INT_SIZE, intAllocationID)
            'End If

            'If intAllocationUnkID > 0 Then
            '    strQ &= " AND allocationunkid = @allocationunkid "
            '    objDataOperation.AddParameter("@allocationunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intAllocationUnkID)
            'End If
            'Sohail (15 Jul 2014) -- End

            strQ = "SELECT  ISNULL(MIN(priority), -1) AS NextLevelPriority " & _
                      " FROM  rcstaffrequisition_approver_mapping " & _
                      " JOIN rcstaffrequisition_approver_jobmapping ON rcstaffrequisition_approver_jobmapping.userapproverunkid =rcstaffrequisition_approver_mapping.userapproverunkid " & _
                      " AND rcstaffrequisition_approver_jobmapping.levelunkid = rcstaffrequisition_approver_mapping.levelunkid AND rcstaffrequisition_approver_jobmapping.allocationid = rcstaffrequisition_approver_mapping.allocationid " & _
                      " AND rcstaffrequisition_approver_jobmapping.isvoid = 0 " & _
                      " LEFT JOIN rcstaffrequisition_tran ON rcstaffrequisition_tran.staffrequisitionbyid = rcstaffrequisition_approver_mapping.allocationid " & _
                      " AND rcstaffrequisition_tran.allocationunkid = rcstaffrequisition_approver_mapping.allocationunkid " & _
                      " AND rcstaffrequisition_tran.jobunkid = rcstaffrequisition_approver_jobmapping.jobunkid " & _
                      " LEFT JOIN hrmsConfiguration..cfuser_master ON cfuser_master.userunkid = rcstaffrequisition_approver_mapping.userapproverunkid " & _
                      " LEFT JOIN (  " & _
                      "                  SELECT  " & _
                      "                      jobunkid " & _
                      "                     ,employeeunkid " & _
                      "                     ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno  " & _
                      "                  FROM hremployee_categorization_tran " & _
                      "                  WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= @EmpAsOnDate " & _
                      "              )  " & _
                      "  AS Jobs ON Jobs.employeeunkid = cfuser_master.employeeunkid " & _
                      " AND rcstaffrequisition_tran.jobunkid = Jobs.jobunkid " & _
                      " LEFT JOIN rcstaffrequisitionlevel_master ON rcstaffrequisition_approver_mapping.levelunkid = rcstaffrequisitionlevel_master.levelunkid " & _
                      " WHERE rcstaffrequisition_approver_mapping.isvoid = 0 AND rcstaffrequisitionlevel_master.isactive = 1 AND priority > @priority AND rcstaffrequisition_tran.isvoid = 0 AND jobs.jobunkid IS NULL "

            'Pinkal (01-Dec-2021)-- Start NMB Staff Requisition Approval Enhancements.[ " AND rcstaffrequisition_tran.jobunkid = rcstaffrequisition_approver_jobmapping.jobunkid " & _]

            If intAllocationID > 0 Then
                strQ &= " AND rcstaffrequisition_approver_mapping.allocationid = @allocationid "
                objDataOperation.AddParameter("@allocationid", SqlDbType.Int, eZeeDataType.INT_SIZE, intAllocationID)
            End If

            If intAllocationUnkID > 0 Then
                strQ &= " AND rcstaffrequisition_approver_mapping.allocationunkid = @allocationunkid "
                objDataOperation.AddParameter("@allocationunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intAllocationUnkID)
            End If

            'Pinkal (01-Dec-2021)-- Start
            'NMB Staff Requisition Approval Enhancements. 
            If intStaffReqTranUnkId > 0 Then
                strQ &= " AND rcstaffrequisition_tran.staffrequisitiontranunkid = @staffrequisitiontranunkid "
                objDataOperation.AddParameter("@staffrequisitiontranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intStaffReqTranUnkId)
            End If
            'Pinkal (01-Dec-2021)-- End

            objDataOperation.AddParameter("@priority", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, intCurrentPriority)
            objDataOperation.AddParameter("@EmpAsOnDate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrEmpAsOnDate)

            'Pinkal (16-Nov-2021) -- End


            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If dsList.Tables(0).Rows.Count > 0 Then
                Return CInt(dsList.Tables(0).Rows(0).Item("NextLevelPriority"))
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetNextLevelPriority", mstrModuleName)
        End Try
        Return -1
    End Function

    Public Function GetMaxPriority(ByVal mstrEmpAsOnDate As String, Optional ByVal intAllocationID As Integer = 0, Optional ByVal intAllocationUnkID As Integer = 0, Optional ByVal intStaffReqTranUnkId As Integer = 0) As Integer
        'Pinkal (01-Dec-2021)-- NMB Staff Requisition Approval Enhancements. [Optional ByVal intStaffReqTranUnkId As Integer = 0]
        'Pinkal (16-Nov-2021)-- NMB Staff Requisition Approval Enhancements.[ByVal mstrEmpAsOnDate As String]

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation
        Try

            'Pinkal (16-Nov-2021)-- Start
            'NMB Staff Requisition Approval Enhancements. 

            'strQ = "SELECT  ISNULL(MAX(priority), -1) AS MaxPriority " & _
            '        "FROM    rcstaffrequisition_approver_mapping " & _
            '                "LEFT JOIN rcstaffrequisitionlevel_master ON rcstaffrequisition_approver_mapping.levelunkid = rcstaffrequisitionlevel_master.levelunkid " & _
            '        "WHERE   isvoid = 0 " & _
            '                "AND isactive = 1 "

            'If intAllocationID > 0 Then
            '    strQ &= " AND allocationid = @allocationid "
            '    objDataOperation.AddParameter("@allocationid", SqlDbType.Int, eZeeDataType.INT_SIZE, intAllocationID)
            'End If

            'If intAllocationUnkID > 0 Then
            '    strQ &= " AND allocationunkid = @allocationunkid "
            '    objDataOperation.AddParameter("@allocationunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intAllocationUnkID)
            'End If
            'Sohail (15 Jul 2014) -- End

            strQ = "SELECT  ISNULL(MAX(priority), -1) AS MaxPriority " & _
                      " FROM    rcstaffrequisition_approver_mapping " & _
                      " JOIN rcstaffrequisition_approver_jobmapping ON rcstaffrequisition_approver_jobmapping.userapproverunkid =rcstaffrequisition_approver_mapping.userapproverunkid " & _
                      " AND rcstaffrequisition_approver_jobmapping.levelunkid = rcstaffrequisition_approver_mapping.levelunkid AND rcstaffrequisition_approver_jobmapping.allocationid = rcstaffrequisition_approver_mapping.allocationid " & _
                      " AND rcstaffrequisition_approver_jobmapping.isvoid = 0 " & _
                      " LEFT JOIN rcstaffrequisition_tran ON rcstaffrequisition_tran.staffrequisitionbyid = rcstaffrequisition_approver_mapping.allocationid " & _
                      " AND rcstaffrequisition_tran.allocationunkid = rcstaffrequisition_approver_mapping.allocationunkid " & _
                      " AND rcstaffrequisition_tran.jobunkid = rcstaffrequisition_approver_jobmapping.jobunkid " & _
                      " LEFT JOIN hrmsConfiguration..cfuser_master ON cfuser_master.userunkid = rcstaffrequisition_approver_mapping.userapproverunkid " & _
                      " LEFT JOIN (  " & _
                      "                  SELECT  " & _
                      "                      jobunkid " & _
                      "                     ,employeeunkid " & _
                      "                     ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno  " & _
                      "                  FROM hremployee_categorization_tran " & _
                      "                  WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= @EmpAsOnDate " & _
                      "              )  " & _
                      "  AS Jobs ON Jobs.employeeunkid = cfuser_master.employeeunkid " & _
                      " AND rcstaffrequisition_tran.jobunkid = Jobs.jobunkid " & _
                      " LEFT JOIN rcstaffrequisitionlevel_master ON rcstaffrequisition_approver_mapping.levelunkid = rcstaffrequisitionlevel_master.levelunkid " & _
                      " WHERE rcstaffrequisition_approver_mapping.isvoid = 0 AND rcstaffrequisitionlevel_master.isactive = 1 AND rcstaffrequisition_tran.isvoid = 0 AND jobs.jobunkid IS NULL "

            'Pinkal (01-Dec-2021)-- Start NMB Staff Requisition Approval Enhancements.[ " AND rcstaffrequisition_tran.jobunkid = rcstaffrequisition_approver_jobmapping.jobunkid " & _]

            If intAllocationID > 0 Then
                strQ &= " AND rcstaffrequisition_approver_mapping.allocationid = @allocationid "
                objDataOperation.AddParameter("@allocationid", SqlDbType.Int, eZeeDataType.INT_SIZE, intAllocationID)
            End If

            If intAllocationUnkID > 0 Then
                strQ &= " AND rcstaffrequisition_approver_mapping.allocationunkid = @allocationunkid "
                objDataOperation.AddParameter("@allocationunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intAllocationUnkID)
            End If

            'Pinkal (01-Dec-2021)-- Start
            'NMB Staff Requisition Approval Enhancements. 
            If intStaffReqTranUnkId > 0 Then
                strQ &= " AND rcstaffrequisition_tran.staffrequisitiontranunkid = @staffrequisitiontranunkid "
                objDataOperation.AddParameter("@staffrequisitiontranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intStaffReqTranUnkId)
            End If
            'Pinkal (01-Dec-2021)-- End

            objDataOperation.AddParameter("@EmpAsOnDate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrEmpAsOnDate)

            'Pinkal (16-Nov-2021)-- End

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If dsList.Tables(0).Rows.Count > 0 Then
                Return CInt(dsList.Tables(0).Rows(0).Item("MaxPriority"))
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetMaxPriority", mstrModuleName)
        End Try
        Return -1
    End Function

    Public Function GetMinPriority(ByVal mstrEmpAsOnDate As String, Optional ByVal intAllocationID As Integer = 0, Optional ByVal intAllocationUnkID As Integer = 0, Optional ByVal intStaffReqTranUnkId As Integer = 0) As Integer
        'Pinkal (01-Dec-2021)-- NMB Staff Requisition Approval Enhancements. [Optional byval  intStaffReqTranUnkId As Integer]
        'Pinkal (16-Nov-2021)-- NMB Staff Requisition Approval Enhancements.[ByVal mstrEmpAsOnDate As String]

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation
        Try


            'Pinkal (16-Nov-2021)-- Start
            'NMB Staff Requisition Approval Enhancements. 

            'strQ = "SELECT  ISNULL(MIN(priority), -1) AS MinPriority " & _
            '          " FROM    rcstaffrequisition_approver_mapping " & _
            '          " LEFT JOIN rcstaffrequisitionlevel_master ON rcstaffrequisition_approver_mapping.levelunkid = rcstaffrequisitionlevel_master.levelunkid " & _
            '          " WHERE   isvoid = 0 AND isactive = 1 "

            'If intAllocationID > 0 Then
            '    strQ &= " AND allocationid = @allocationid "
            '    objDataOperation.AddParameter("@allocationid", SqlDbType.Int, eZeeDataType.INT_SIZE, intAllocationID)
            'End If

            'If intAllocationUnkID > 0 Then
            '    strQ &= " AND allocationunkid = @allocationunkid "
            '    objDataOperation.AddParameter("@allocationunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intAllocationUnkID)
            'End If
            'Sohail (15 Jul 2014) -- End

            strQ = "SELECT  ISNULL(MIN(priority), -1) AS MinPriority " & _
                    " FROM rcstaffrequisition_approver_mapping " & _
                    " JOIN rcstaffrequisition_approver_jobmapping ON rcstaffrequisition_approver_jobmapping.userapproverunkid =rcstaffrequisition_approver_mapping.userapproverunkid " & _
                    " AND rcstaffrequisition_approver_jobmapping.levelunkid = rcstaffrequisition_approver_mapping.levelunkid AND rcstaffrequisition_approver_jobmapping.allocationid = rcstaffrequisition_approver_mapping.allocationid " & _
                    " AND rcstaffrequisition_approver_jobmapping.isvoid = 0 " & _
                    " LEFT JOIN rcstaffrequisition_tran ON rcstaffrequisition_tran.staffrequisitionbyid = rcstaffrequisition_approver_mapping.allocationid " & _
                    " AND rcstaffrequisition_tran.allocationunkid = rcstaffrequisition_approver_mapping.allocationunkid " & _
                    " AND rcstaffrequisition_tran.jobunkid = rcstaffrequisition_approver_jobmapping.jobunkid " & _
                    " LEFT JOIN hrmsConfiguration..cfuser_master ON cfuser_master.userunkid = rcstaffrequisition_approver_mapping.userapproverunkid " & _
                    " LEFT JOIN (  " & _
                    "                  SELECT  " & _
                    "                      jobunkid " & _
                    "                     ,employeeunkid " & _
                    "                     ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno  " & _
                    "                  FROM hremployee_categorization_tran " & _
                    "                  WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= @EmpAsOnDate " & _
                    "              )  " & _
                    "  AS Jobs ON Jobs.employeeunkid = cfuser_master.employeeunkid " & _
                    " AND rcstaffrequisition_tran.jobunkid = Jobs.jobunkid " & _
                    " LEFT JOIN rcstaffrequisitionlevel_master ON rcstaffrequisition_approver_mapping.levelunkid = rcstaffrequisitionlevel_master.levelunkid " & _
                    " WHERE rcstaffrequisition_approver_mapping.isvoid = 0 AND rcstaffrequisitionlevel_master.isactive = 1  AND rcstaffrequisition_tran.isvoid = 0 AND jobs.jobunkid IS NULL "


            'Pinkal (01-Dec-2021)-- Start NMB Staff Requisition Approval Enhancements.[ " AND rcstaffrequisition_tran.jobunkid = rcstaffrequisition_approver_jobmapping.jobunkid " & _]

            If intAllocationID > 0 Then
                strQ &= " AND rcstaffrequisition_approver_mapping.allocationid = @allocationid "
                objDataOperation.AddParameter("@allocationid", SqlDbType.Int, eZeeDataType.INT_SIZE, intAllocationID)
            End If

            If intAllocationUnkID > 0 Then
                strQ &= " AND rcstaffrequisition_approver_mapping.allocationunkid = @allocationunkid "
                objDataOperation.AddParameter("@allocationunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intAllocationUnkID)
            End If

            'Pinkal (01-Dec-2021)-- Start
            'NMB Staff Requisition Approval Enhancements. 
            If intStaffReqTranUnkId > 0 Then
                strQ &= " AND rcstaffrequisition_tran.staffrequisitiontranunkid = @staffrequisitiontranunkid "
                objDataOperation.AddParameter("@staffrequisitiontranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intStaffReqTranUnkId)
            End If
            'Pinkal (01-Dec-2021)-- End

            objDataOperation.AddParameter("@EmpAsOnDate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrEmpAsOnDate)

            'Pinkal (16-Nov-2021)-- End

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If dsList.Tables(0).Rows.Count > 0 Then
                Return CInt(dsList.Tables(0).Rows(0).Item("MinPriority"))
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetMinPriority", mstrModuleName)
        End Try
        Return -1
    End Function

	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Public Shared Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "This Approver Level Code is already defined. Please define new Approver Level Code.")
			Language.setMessage(mstrModuleName, 2, "This Approver Level Name is already defined. Please define new Approver Level Name.")
			Language.setMessage(mstrModuleName, 3, "This Approver Level Priority is already assign. Please assign new Approver Level Priority.")
			Language.setMessage(mstrModuleName, 4, "Select")

		Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class
