﻿'************************************************************************************************************************************
'Class Name : clsAppAttachFileTran.vb
'Purpose    :
'Date       :27/05/2017
'Written By :Sohail
'Modified   :
'************************************************************************************************************************************
Imports eZeeCommonLib
Imports Aruti.Data


Public Class clsAppAttachFileTran
    Private Const mstrModuleName = "clsAppAttachFileTran"
    Dim objDataOperation As clsDataOperation
    Dim mstrMessage As String = ""

#Region " Private Variables "
    Private mintAttachfiletranunkid As Integer
    Private mintApplicantunkid As Integer
    Private mdtTran As DataTable

#End Region

#Region " Properties "

'S.SANDEEP [28-May-2018] -- START 
'ISSUE/ENHANCEMENT : {Audit Trails} 
Private mstrFormName As String = String.Empty
Public WriteOnly Property _FormName() As String 
Set(ByVal value As String) 
mstrFormName = value 
End Set 
End Property 
 
Private mstrClientIP As String = ""
Public WriteOnly Property _ClientIP() As String 
Set(ByVal value As String) 
mstrClientIP = value 
End Set 
End Property 
 
Private mstrHostName As String = ""
Public WriteOnly Property _HostName() As String 
Set(ByVal value As String) 
mstrHostName = value 
End Set 
End Property 
 
Private mblnIsWeb As Boolean = False
Public WriteOnly Property _FromWeb() As Boolean 
Set(ByVal value As Boolean) 
mblnIsWeb = value 
End Set 
End Property 
 
    Private mintAuditUserId As Integer = 0
    Public WriteOnly Property _AuditUserId() As Integer
        Set(ByVal value As Integer)
            mintAuditUserId = value
        End Set
    End Property

    Private mintCompanyUnkid As Integer = 0
    Public WriteOnly Property _CompanyUnkid() As Integer
        Set(ByVal value As Integer)
            mintCompanyUnkid = value
        End Set
    End Property
 
Private mdtAuditDate As DateTime = Now
Public WriteOnly Property _AuditDate() As DateTime 
Set(ByVal value As DateTime) 
mdtAuditDate = value 
End Set 
End Property 
 
Private xDataOpr As clsDataOperation = Nothing
Public Property _xDataOpr() As clsDataOperation 
Get 
Return xDataOpr 
End Get 
Set(ByVal value As clsDataOperation) 
xDataOpr = value 
End Set 
End Property 
 
Private mintLoginEmployeeunkid As Integer = 0
Public Property _LoginEmployeeunkid() As Integer 
Get 
Return mintLoginEmployeeunkid 
End Get 
Set(ByVal value As Integer) 
mintLoginEmployeeunkid = value 
End Set 
End Property 
 
Private mintVoidlogingemployeeunkid As Integer=0
Public Property _Voidloginemployeeunkid() As Integer 
Get 
Return mintVoidlogingemployeeunkid 
End Get 
Set(ByVal value As Integer) 
mintVoidlogingemployeeunkid = value 
End Set 
End Property 
'S.SANDEEP [28-May-2018] -- END 

    Public Property _ApplicantUnkid() As Integer
        Get
            Return mintApplicantunkid
        End Get
        Set(ByVal value As Integer)
            mintApplicantunkid = value
            Call GetAttachFile_tran()
        End Set
    End Property

    Public Property _DataList() As DataTable
        Get
            Return mdtTran
        End Get
        Set(ByVal value As DataTable)
            mdtTran = value
        End Set
    End Property
#End Region

#Region " Constructor "
    Public Sub New()
        mdtTran = New DataTable("applicantattachfiletran")

        mdtTran.Columns.Add("attachfiletranunkid", System.Type.GetType("System.Int32")).DefaultValue = 0
        mdtTran.Columns.Add("applicantunkid", System.Type.GetType("System.Int32")).DefaultValue = 0
        mdtTran.Columns.Add("documentunkid", System.Type.GetType("System.Int32")).DefaultValue = 0
        mdtTran.Columns.Add("modulerefid", System.Type.GetType("System.Int32")).DefaultValue = 0
        mdtTran.Columns.Add("attachrefid", System.Type.GetType("System.Int32")).DefaultValue = 0
        mdtTran.Columns.Add("filepath", System.Type.GetType("System.String")).DefaultValue = ""
        mdtTran.Columns.Add("filename", System.Type.GetType("System.String")).DefaultValue = ""
        mdtTran.Columns.Add("fileuniquename", System.Type.GetType("System.String")).DefaultValue = ""
        mdtTran.Columns.Add("attached_date", System.Type.GetType("System.DateTime")).DefaultValue = Nothing
        mdtTran.Columns.Add("file_size", System.Type.GetType("System.Int32")).DefaultValue = 0
        mdtTran.Columns.Add("AUD", System.Type.GetType("System.String")).DefaultValue = ""
        mdtTran.Columns.Add("GUID", System.Type.GetType("System.String")).DefaultValue = ""
        mdtTran.Columns.Add("serverattachfiletranunkid", System.Type.GetType("System.Int32")).DefaultValue = 0
        mdtTran.Columns.Add("isvoid", System.Type.GetType("System.Boolean")).DefaultValue = False
        mdtTran.Columns.Add("voiduserunkid", System.Type.GetType("System.Int32")).DefaultValue = 0
        mdtTran.Columns.Add("voiddatetime", System.Type.GetType("System.DateTime")).DefaultValue = Nothing
        mdtTran.Columns.Add("voidreason", System.Type.GetType("System.String")).DefaultValue = ""

    End Sub
#End Region

#Region " Private Methods "
    Private Sub GetAttachFile_tran()
        Dim strQ As String = ""
        Dim strErrorMessage As String = ""
        Dim dsList As DataSet = Nothing
        Dim dRowID_Tran As DataRow
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try


          

            strQ = "SELECT  attachfiletranunkid" & _
                            ", applicantunkid " & _
                            ", documentunkid " & _
                            ", modulerefid " & _
                            ", attachrefid " & _
                            ", filepath " & _
                            ", filename " & _
                            ", fileuniquename " & _
                            ", attached_date " & _
                            ", ISNULL(file_size, 0) AS file_size " & _
                            ", '' AUD " & _
                            ", ISNULL(serverattachfiletranunkid, 0) AS serverattachfiletranunkid " & _
                            ", isvoid " & _
                            ", voiduserunkid " & _
                            ", voiddatetime " & _
                            ", voidreason " & _
                    "FROM rcattachfiletran " & _
                    "WHERE ISNULL(isvoid, 0) = 0 AND applicantunkid = @applicantunkid "


            objDataOperation.AddParameter("@applicantunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintApplicantunkid)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & " :" & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            mdtTran.Rows.Clear()

            For i As Integer = 0 To dsList.Tables(0).Rows.Count - 1
                With dsList.Tables(0).Rows(i)
                    dRowID_Tran = mdtTran.NewRow
                    dRowID_Tran.Item("attachfiletranunkid") = .Item("attachfiletranunkid").ToString
                    dRowID_Tran.Item("applicantunkid") = .Item("applicantunkid").ToString
                    dRowID_Tran.Item("documentunkid") = .Item("documentunkid").ToString
                    dRowID_Tran.Item("modulerefid") = .Item("modulerefid").ToString
                    dRowID_Tran.Item("attachrefid") = .Item("attachrefid").ToString
                    dRowID_Tran.Item("filepath") = .Item("filepath").ToString
                    dRowID_Tran.Item("filename") = .Item("filename").ToString
                    dRowID_Tran.Item("fileuniquename") = .Item("fileuniquename").ToString
                    If IsDBNull(.Item("attached_date")) = True Then
                        dRowID_Tran.Item("attached_date") = DBNull.Value
                    Else
                        dRowID_Tran.Item("attached_date") = .Item("attached_date")
                    End If
                    dRowID_Tran.Item("file_size") = .Item("file_size").ToString
                    dRowID_Tran.Item("AUD") = .Item("AUD").ToString
                    dRowID_Tran.Item("serverattachfiletranunkid") = .Item("serverattachfiletranunkid")
                    dRowID_Tran.Item("isvoid") = .Item("isvoid")
                    dRowID_Tran.Item("voiduserunkid") = .Item("voiduserunkid")
                    If IsDBNull(.Item("voiddatetime")) = True Then
                        dRowID_Tran.Item("voiddatetime") = DBNull.Value
                    Else
                        dRowID_Tran.Item("voiddatetime") = .Item("voiddatetime")
                    End If
                    dRowID_Tran.Item("voidreason") = .Item("voidreason")
                    mdtTran.Rows.Add(dRowID_Tran)
                End With
            Next

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetAttachFile_tran; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try

    End Sub

    Public Function InsertUpdateDelete_AttachFileTran() As Boolean
        Dim i As Integer
        Dim strQ As String = ""
        Dim strErrorMessage As String = ""
        Dim exForce As Exception

        Try
            objDataOperation = New clsDataOperation
            objDataOperation.BindTransaction()
            For i = 0 To mdtTran.Rows.Count - 1
                With mdtTran.Rows(i)
                    objDataOperation.ClearParameters()
                    If Not IsDBNull(.Item("AUD")) Then
                        Select Case .Item("AUD")
                            Case "A"


                                strQ = "INSERT INTO rcattachfiletran ( " & _
                                        "  applicantunkid " & _
                                        ", documentunkid " & _
                                        ", modulerefid " & _
                                        ", attachrefid " & _
                                        ", filepath " & _
                                        ", filename " & _
                                        ", fileuniquename " & _
                                        ", attached_date " & _
                                        ", file_size " & _
                                        ", serverattachfiletranunkid" & _
                                        ", isvoid" & _
                                        ", voiduserunkid" & _
                                        ", voiddatetime" & _
                                        ", voidreason" & _
                                ") VALUES (" & _
                                        "  @applicantunkid " & _
                                        ", @documentunkid " & _
                                        ", @modulerefid " & _
                                        ", @attachrefid " & _
                                        ", @filepath " & _
                                        ", @filename " & _
                                        ", @fileuniquename " & _
                                        ", @attached_date " & _
                                        ", @file_size " & _
                                        ", @serverattachfiletranunkid" & _
                                        ", @isvoid" & _
                                        ", @voiduserunkid" & _
                                        ", @voiddatetime" & _
                                        ", @voidreason" & _
                                "); SELECT @@identity"

                                objDataOperation.AddParameter("@applicantunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintApplicantunkid)
                                objDataOperation.AddParameter("@documentunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("documentunkid").ToString)
                                objDataOperation.AddParameter("@modulerefid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("modulerefid").ToString)
                                objDataOperation.AddParameter("@attachrefid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("attachrefid").ToString)
                                objDataOperation.AddParameter("@filepath", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, .Item("filepath").ToString)
                                objDataOperation.AddParameter("@filename", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, .Item("filename").ToString)
                                objDataOperation.AddParameter("@fileuniquename", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, .Item("fileuniquename").ToString)
                                If IsDBNull(.Item("attached_date")) = True Then
                                    objDataOperation.AddParameter("@attached_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
                                Else
                                    objDataOperation.AddParameter("@attached_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, .Item("attached_date"))
                                End If
                                objDataOperation.AddParameter("@file_size", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("file_size").ToString)
                                objDataOperation.AddParameter("@serverattachfiletranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("serverattachfiletranunkid"))
                                objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, .Item("isvoid"))
                                objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("voiduserunkid"))
                                If IsDBNull(.Item("voiddatetime")) = True Then
                                    objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
                                Else
                                    objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, .Item("voiddatetime"))
                                End If
                                objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, .Item("voidreason").ToString)

                                Dim dsList As DataSet = objDataOperation.ExecQuery(strQ, "List")

                                If objDataOperation.ErrorMessage <> "" Then
                                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                    Throw exForce
                                End If


                                mintAttachfiletranunkid = dsList.Tables(0).Rows(0)(0)

                                If .Item("applicantunkid") > 0 Then
'S.SANDEEP [28-May-2018] -- START
'ISSUE/ENHANCEMENT : {Audit Trails} 
Dim objCommonATLog AS New clsCommonATLog 
objCommonATLog._FormName = mstrFormName 
objCommonATLog._LoginEmployeeUnkid = mintLoginEmployeeunkid 
                                    objCommonATLog._ClientIP = mstrClientIP
                                    objCommonATLog._HostName = mstrHostName
                                    objCommonATLog._FromWeb = mblnIsWeb
                                    objCommonATLog._AuditUserId = mintAuditUserId
objCommonATLog._CompanyUnkid = mintCompanyUnkid
                                    objCommonATLog._AuditDate = mdtAuditDate
                                    'S.SANDEEP [28-May-2018] -- END
                                    'Sohail (09 Oct 2018) -- Start
                                    'TANAPA - Support Issue Id # 2502 - linking recruitment server app_vacancy with aruti desktop app_vacancy in 75.1.
                                    'If clsCommonATLog.Insert_TranAtLog(objDataOperation, "rcapplicant_master", "applicantunkid", .Item("applicantunkid"), "rcattachfiletran", "attachfiletranunkid", mintAttachfiletranunkid, 2, 1) = False Then
                                    If objCommonATLog.Insert_TranAtLog(objDataOperation, "rcapplicant_master", "applicantunkid", mintApplicantunkid, "rcattachfiletran", "attachfiletranunkid", mintAttachfiletranunkid, 2, 1) = False Then
                                        'Sohail (09 Oct 2018) -- End
                                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                        Throw exForce
                                    End If

                                    'S.SANDEEP [28-May-2018] -- START
                                    'ISSUE/ENHANCEMENT : {Audit Trails} 
                                    objCommonATLog = Nothing
                                    'S.SANDEEP [28-May-2018] -- END

                                Else
                                    'S.SANDEEP [28-May-2018] -- START
                                    'ISSUE/ENHANCEMENT : {Audit Trails} 
                                    Dim objCommonATLog As New clsCommonATLog
                                    objCommonATLog._FormName = mstrFormName
                                    objCommonATLog._LoginEmployeeUnkid = mintLoginEmployeeunkid
                                    objCommonATLog._ClientIP = mstrClientIP
                                    objCommonATLog._HostName = mstrHostName
                                    objCommonATLog._FromWeb = mblnIsWeb
                                    objCommonATLog._AuditUserId = mintAuditUserId
objCommonATLog._CompanyUnkid = mintCompanyUnkid
                                    objCommonATLog._AuditDate = mdtAuditDate
                                    'S.SANDEEP [28-May-2018] -- END

                                    If objCommonATLog.Insert_TranAtLog(objDataOperation, "rcapplicant_master", "applicantunkid", mintApplicantunkid, "rcattachfiletran", "attachfiletranunkid", mintAttachfiletranunkid, 1, 1) = False Then
                                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                        Throw exForce
                                    End If

                                    'S.SANDEEP [28-May-2018] -- START
                                    'ISSUE/ENHANCEMENT : {Audit Trails} 
                                    objCommonATLog = Nothing
                                    'S.SANDEEP [28-May-2018] -- END

                                End If


                            Case "U"


                                strQ = "UPDATE rcattachfiletran SET " & _
                                            "  applicantunkid = @applicantunkid" & _
                                            ", documentunkid = @documentunkid " & _
                                            ", modulerefid = @modulerefid " & _
                                            ", attachrefid = @attachrefid " & _
                                            ", filepath = @filepath " & _
                                            ", filename = @filename " & _
                                            ", fileuniquename = @fileuniquename " & _
                                            ", attached_date = @attached_date " & _
                                            ", file_size = @file_size " & _
                                            ", serverattachfiletranunkid = @serverattachfiletranunkid" & _
                                            ", isvoid = @isvoid" & _
                                            ", voiduserunkid = @voiduserunkid" & _
                                            ", voiddatetime = @voiddatetime" & _
                                            ", voidreason = @voidreason " & _
                                        "WHERE attachfiletranunkid = @attachfiletranunkid "

                                objDataOperation.AddParameter("@attachfiletranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("attachfiletranunkid").ToString)
                                objDataOperation.AddParameter("@applicantunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintApplicantunkid)
                                objDataOperation.AddParameter("@documentunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("documentunkid").ToString)
                                objDataOperation.AddParameter("@modulerefid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("modulerefid").ToString)
                                objDataOperation.AddParameter("@attachrefid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("attachrefid").ToString)
                                objDataOperation.AddParameter("@filepath", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, .Item("filepath").ToString)
                                objDataOperation.AddParameter("@filename", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, .Item("filename").ToString)
                                objDataOperation.AddParameter("@fileuniquename", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, .Item("fileuniquename").ToString)
                                If IsDBNull(.Item("attached_date")) = True Then
                                    objDataOperation.AddParameter("@attached_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
                                Else
                                    objDataOperation.AddParameter("@attached_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, .Item("attached_date"))
                                End If
                                objDataOperation.AddParameter("@file_size", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("file_size").ToString)
                                objDataOperation.AddParameter("@serverattachfiletranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("serverattachfiletranunkid"))
                                objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, .Item("isvoid"))
                                objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("voiduserunkid"))
                                If IsDBNull(.Item("voiddatetime")) = True Then
                                    objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
                                Else
                                    objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, .Item("voiddatetime"))
                                End If
                                objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, .Item("voidreason").ToString)

                                Call objDataOperation.ExecNonQuery(strQ)

                                If objDataOperation.ErrorMessage <> "" Then
                                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                    Throw exForce
                                End If

                                'S.SANDEEP [28-May-2018] -- START
                                'ISSUE/ENHANCEMENT : {Audit Trails} 
                                Dim objCommonATLog As New clsCommonATLog
                                objCommonATLog._FormName = mstrFormName
                                objCommonATLog._LoginEmployeeUnkid = mintLoginEmployeeunkid
                                objCommonATLog._ClientIP = mstrClientIP
                                objCommonATLog._HostName = mstrHostName
                                objCommonATLog._FromWeb = mblnIsWeb
                                objCommonATLog._AuditUserId = mintAuditUserId
objCommonATLog._CompanyUnkid = mintCompanyUnkid
                                objCommonATLog._AuditDate = mdtAuditDate
                                'S.SANDEEP [28-May-2018] -- END

                                If objCommonATLog.Insert_TranAtLog(objDataOperation, "rcapplicant_master", "applicantunkid", .Item("applicantunkid"), "rcattachfiletran", "attachfiletranunkid", .Item("attachfiletranunkid"), 2, 2) = False Then
                                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                    Throw exForce
                                End If

                                'S.SANDEEP [28-May-2018] -- START
                                'ISSUE/ENHANCEMENT : {Audit Trails} 
                                objCommonATLog = Nothing
                                'S.SANDEEP [28-May-2018] -- END



                            Case "D"

                                If .Item("attachfiletranunkid") > 0 Then
                                    'S.SANDEEP [28-May-2018] -- START
                                    'ISSUE/ENHANCEMENT : {Audit Trails} 
                                    Dim objCommonATLog As New clsCommonATLog
                                    objCommonATLog._FormName = mstrFormName
                                    objCommonATLog._LoginEmployeeUnkid = mintLoginEmployeeunkid
                                    objCommonATLog._ClientIP = mstrClientIP
                                    objCommonATLog._HostName = mstrHostName
                                    objCommonATLog._FromWeb = mblnIsWeb
                                    objCommonATLog._AuditUserId = mintAuditUserId
objCommonATLog._CompanyUnkid = mintCompanyUnkid
                                    objCommonATLog._AuditDate = mdtAuditDate
                                    'S.SANDEEP [28-May-2018] -- END

                                    If objCommonATLog.Insert_TranAtLog(objDataOperation, "rcapplicant_master", "applicantunkid", .Item("applicantunkid"), "rcattachfiletran", "attachfiletranunkid", .Item("attachfiletranunkid"), 2, 3) = False Then
                                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                        Throw exForce
                                    End If

                                    'S.SANDEEP [28-May-2018] -- START
                                    'ISSUE/ENHANCEMENT : {Audit Trails} 
                                    objCommonATLog = Nothing
                                    'S.SANDEEP [28-May-2018] -- END

                                End If


                                strQ = "DELETE FROM rcattachfiletran " & _
                                        "WHERE attachfiletranunkid = @attachfiletranunkid "

                                objDataOperation.AddParameter("@attachfiletranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("attachfiletranunkid").ToString)

                                Call objDataOperation.ExecNonQuery(strQ)

                                If objDataOperation.ErrorMessage <> "" Then
                                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                    Throw exForce
                                End If


                        End Select
                    End If
                End With
            Next
            objDataOperation.ReleaseTransaction(True)
            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: InsertUpdateDelete_AttachFileTran; Module Name: " & mstrModuleName)
            Return False
        End Try

    End Function

#End Region

End Class
