﻿'************************************************************************************************************************************
'Class Name : clsApplicant_Batchschedule_Tran.vb
'Purpose    :
'Date       : 8/30/2010
'Written By : Anjan
'Modified   :
'************************************************************************************************************************************

Imports eZeeCommonLib
Imports System.Web
Imports System.Threading

''' <summary>
''' Purpose: 
''' Developer: Anjan
''' </summary>
Public Class clsApplicant_Batchschedule_Tran
    Private Shared ReadOnly mstrModuleName As String = "clsApplicant_Batchschedule_Tran"
    Dim objDataOperation As clsDataOperation
    Dim mstrMessage As String = ""

#Region " Private variables "
    Private mintAppbatchscheduletranunkid As Integer
    Private mintBatchscheduleunkid As Integer
    Private mintApplicantunkid As Integer
    Private mintUserunkid As Integer
    Private mblnIscancel As Boolean = False
    Private mdtCanceldatetime As Date
    Private mstrCancelremark As String = String.Empty
    Private mblnIsvoid As Boolean = False
    Private mdtVoiddatetime As Date
    Private mintVoiduserunkid As Integer
    Private mstrVoidreason As String = String.Empty
    Private mblnIsChanged As Boolean = False
    'Hemant (07 Oct 2019) -- Start
    'ISSUE/ENHANCEMENT(NMB) :  RECRUITMENTS UAT CHANGES(Point No 14 : On interview scheduling, system should be able to send automatic emails to applicants that have been placed in a batch. The content of the email should be picked from a template set on configuration.)
    Private mstrWebFormName As String = String.Empty
    Private mstrWebClientIP As String = String.Empty
    Private mstrWebHostName As String = String.Empty
    Private mintLoginemployeeunkid As Integer = -1
    Dim objEmailList As List(Of clsEmailCollection)
    Private objThread As Thread
    'Hemant (07 Oct 2019) -- End

#End Region

#Region " Properties "

'S.SANDEEP [28-May-2018] -- START 
'ISSUE/ENHANCEMENT : {Audit Trails} 
Private mstrFormName As String = String.Empty
Public WriteOnly Property _FormName() As String 
Set(ByVal value As String) 
mstrFormName = value 
End Set 
End Property 
 
Private mstrClientIP As String = ""
Public WriteOnly Property _ClientIP() As String 
Set(ByVal value As String) 
mstrClientIP = value 
End Set 
End Property 
 
Private mstrHostName As String = ""
Public WriteOnly Property _HostName() As String 
Set(ByVal value As String) 
mstrHostName = value 
End Set 
End Property 
 
Private mblnIsWeb As Boolean = False
Public WriteOnly Property _FromWeb() As Boolean 
Set(ByVal value As Boolean) 
mblnIsWeb = value 
End Set 
End Property 
 
    Private mintAuditUserId As Integer = 0
    Public WriteOnly Property _AuditUserId() As Integer
        Set(ByVal value As Integer)
            mintAuditUserId = value
        End Set
    End Property

    Private mintCompanyUnkid As Integer = 0
    Public WriteOnly Property _CompanyUnkid() As Integer
        Set(ByVal value As Integer)
            mintCompanyUnkid = value
        End Set
    End Property
 
Private mdtAuditDate As DateTime = Now
Public WriteOnly Property _AuditDate() As DateTime 
Set(ByVal value As DateTime) 
mdtAuditDate = value 
End Set 
End Property 
 
Private xDataOpr As clsDataOperation = Nothing
Public Property _xDataOpr() As clsDataOperation 
Get 
Return xDataOpr 
End Get 
Set(ByVal value As clsDataOperation) 
xDataOpr = value 
End Set 
End Property 
 
    'Private mintLoginEmployeeunkid As Integer = 0
    'Public Property _LoginEmployeeunkid() As Integer 
    'Get 
    'Return mintLoginEmployeeunkid 
    'End Get 
    'Set(ByVal value As Integer) 
    'mintLoginEmployeeunkid = value 
    'End Set 
    'End Property 
 
Private mintVoidlogingemployeeunkid As Integer=0
Public Property _Voidloginemployeeunkid() As Integer 
Get 
Return mintVoidlogingemployeeunkid 
End Get 
Set(ByVal value As Integer) 
mintVoidlogingemployeeunkid = value 
End Set 
End Property 
'S.SANDEEP [28-May-2018] -- END 

    Public ReadOnly Property _Message() As String
        Get
            Return mstrMessage
        End Get
    End Property

    ''' <summary>
    ''' Purpose: Get or Set appbatchscheduletranunkid
    ''' Modify By: Anjan
    ''' </summary>
    Public Property _Appbatchscheduletranunkid() As Integer
        Get
            Return mintAppbatchscheduletranunkid
        End Get
        Set(ByVal value As Integer)
            mintAppbatchscheduletranunkid = Value
            Call getData()
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set batchscheduleunkid
    ''' Modify By: Anjan
    ''' </summary>
    Public Property _Batchscheduleunkid() As Integer
        Get
            Return mintBatchscheduleunkid
        End Get
        Set(ByVal value As Integer)
            mintBatchscheduleunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set applicantunkid
    ''' Modify By: Anjan
    ''' </summary>
    Public Property _Applicantunkid() As Integer
        Get
            Return mintApplicantunkid
        End Get
        Set(ByVal value As Integer)
            mintApplicantunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set userunkid
    ''' Modify By: Anjan
    ''' </summary>
    Public Property _Userunkid() As Integer
        Get
            Return mintUserunkid
        End Get
        Set(ByVal value As Integer)
            mintUserunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set iscancel
    ''' Modify By: Anjan
    ''' </summary>
    Public Property _Iscancel() As Boolean
        Get
            Return mblnIscancel
        End Get
        Set(ByVal value As Boolean)
            mblnIscancel = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set canceldatetime
    ''' Modify By: Anjan
    ''' </summary>
    Public Property _Canceldatetime() As Date
        Get
            Return mdtCanceldatetime
        End Get
        Set(ByVal value As Date)
            mdtCanceldatetime = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set cancelremark
    ''' Modify By: Anjan
    ''' </summary>
    Public Property _Cancelremark() As String
        Get
            Return mstrCancelremark
        End Get
        Set(ByVal value As String)
            mstrCancelremark = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isvoid
    ''' Modify By: Anjan
    ''' </summary>
    Public Property _Isvoid() As Boolean
        Get
            Return mblnIsvoid
        End Get
        Set(ByVal value As Boolean)
            mblnIsvoid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiddatetime
    ''' Modify By: Anjan
    ''' </summary>
    Public Property _Voiddatetime() As Date
        Get
            Return mdtVoiddatetime
        End Get
        Set(ByVal value As Date)
            mdtVoiddatetime = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiduserunkid
    ''' Modify By: Anjan
    ''' </summary>
    Public Property _Voiduserunkid() As Integer
        Get
            Return mintVoiduserunkid
        End Get
        Set(ByVal value As Integer)
            mintVoiduserunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voidreason
    ''' Modify By: Anjan
    ''' </summary>
    Public Property _Voidreason() As String
        Get
            Return mstrVoidreason
        End Get
        Set(ByVal value As String)
            mstrVoidreason = Value
        End Set
    End Property

    
    ''' <summary>
    ''' Purpose: Get or Set isChanged
    ''' Modify By: Anjan
    ''' </summary>
    Public Property _IsChanged() As Boolean
        Get
            Return mblnIsChanged
        End Get
        Set(ByVal value As Boolean)
            mblnIsChanged = value
        End Set
    End Property

    'Hemant (07 Oct 2019) -- Start
    'ISSUE/ENHANCEMENT(NMB) :  RECRUITMENTS UAT CHANGES(Point No 14 : On interview scheduling, system should be able to send automatic emails to applicants that have been placed in a batch. The content of the email should be picked from a template set on configuration.)
    Public Property _WebFormName() As String
        Get
            Return mstrWebFormName
        End Get
        Set(ByVal value As String)
            mstrWebFormName = value
        End Set
    End Property

    Public WriteOnly Property _WebClientIP() As String
        Set(ByVal value As String)
            mstrWebClientIP = value
        End Set
    End Property

    Public WriteOnly Property _WebHostName() As String
        Set(ByVal value As String)
            mstrWebHostName = value
        End Set
    End Property

    Public Property _Loginemployeeunkid() As Integer
        Get
            Return mintLoginemployeeunkid
        End Get
        Set(ByVal value As Integer)
            mintLoginemployeeunkid = value
        End Set
    End Property
    'Hemant (07 Oct 2019) -- End

#End Region

    ''' <summary>
    ''' Modify By: Anjan
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Sub GetData()
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "SELECT " & _
                      "  appbatchscheduletranunkid " & _
                      ", batchscheduleunkid " & _
                      ", applicantunkid " & _
                      ", userunkid " & _
                      ", iscancel " & _
                      ", canceldatetime " & _
                      ", cancelremark " & _
                      ", isvoid " & _
                      ", voiddatetime " & _
                      ", voiduserunkid " & _
                      ", voidreason " & _
                    "FROM rcapplicant_batchschedule_tran " & _
                    "WHERE appbatchscheduletranunkid = @appbatchscheduletranukid "

            objDataOperation.AddParameter("@appbatchscheduletranukid", SqlDbType.int, eZeeDataType.INT_SIZE, mintAppbatchscheduletranunkid.ToString)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For Each dtRow As DataRow In dsList.Tables(0).Rows
                mintAppbatchscheduletranunkid = CInt(dtRow.Item("appbatchscheduletranunkid"))
                mintbatchscheduleunkid = CInt(dtRow.Item("batchscheduleunkid"))
                mintapplicantunkid = CInt(dtRow.Item("applicantunkid"))
                mintuserunkid = CInt(dtRow.Item("userunkid"))
                mblnIscancel = CBool(dtRow.Item("iscancel"))
                If IsDBNull(dtRow.Item("canceldatetime")) Then
                    mdtCanceldatetime = Nothing
                Else
                    mdtCanceldatetime = dtRow.Item("canceldatetime")
                End If

                mstrCancelremark = dtRow.Item("cancelremark").ToString
                mblnIsvoid = CBool(dtRow.Item("isvoid"))
                If IsDBNull(dtRow.Item("voiddatetime")) Then
                    mdtVoiddatetime = Nothing
                Else
                    mdtVoiddatetime = dtRow.Item("voiddatetime")
                End If

                mintVoiduserunkid = CInt(dtRow.Item("voiduserunkid"))
                mstrVoidreason = dtRow.Item("voidreason").ToString
                Exit For
            Next
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetData; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Sub


    'S.SANDEEP [ 10 June 2011 ] -- START
    'ISSUE : MULTIPLE INTERVIEW TYPE FOR ONE VACANCY
    ''' <summary>
    ''' Modify By: Anjan
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    '''Public Function GetList(ByVal strTableName As String, Optional ByVal intAppBatchScheduleTranId As Integer = -1) As DataSet
    '''S.SANDEEP [ 10 June 2011 ] -- END 
    Public Function GetList(ByVal strTableName As String, Optional ByVal intAppBatchScheduleTranId As Integer = -1, Optional ByVal intBatchUnkid As Integer = -1, Optional ByVal strFilter As String = "") As DataSet 'S.SANDEEP [20-APR-2017] -- START {strFilter} -- END
        'Public Function GetList(ByVal strTableName As String, Optional ByVal intAppBatchScheduleTranId As Integer = -1, Optional ByVal intBatchUnkid As Integer = -1) As DataSet
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try

            'Sandeep [ 21 APRIL 2011 ] -- Start
            'strQ = "SELECT  rcapplicant_batchschedule_tran.appbatchscheduletranunkid " & _
            '                ",rcapplicant_batchschedule_tran.batchscheduleunkid " & _
            '               ",rcapplicant_batchschedule_tran.applicantunkid " & _
            '               ",rcbatchschedule_master.batchcode AS BatchCode " & _
            '               ",rcbatchschedule_master.batchname AS BatchName " & _
            '               ",rcapplicant_master.firstname + ' ' + rcapplicant_master.surname AS ApplicantName " & _
            '               ",rcvacancy_master.vacancytitle AS vacancy " & _
            '               ",rcbatchschedule_master.vacancyunkid " & _
            '               ",rcbatchschedule_master.interviewtypeunkid " & _
            '               ",cfcommon_master.name AS InterviewType " & _
            '               ",CONVERT(CHAR(8),rcbatchschedule_master.interviewdate,112) AS Interviewdate " & _
            '               ",CONVERT(CHAR(5),rcbatchschedule_master.interviewtime,108) AS Interviewtime " & _
            '               ",rcbatchschedule_master.location AS location " & _
            '               ",rcbatchschedule_master.description AS description " & _
            '               ",rcapplicant_batchschedule_tran.userunkid " & _
            '               ",rcapplicant_batchschedule_tran.iscancel " & _
            '               ",rcapplicant_batchschedule_tran.canceldatetime " & _
            '               ",rcapplicant_batchschedule_tran.cancelremark " & _
            '               ",rcapplicant_batchschedule_tran.isvoid " & _
            '               ",rcapplicant_batchschedule_tran.voiddatetime " & _
            '               ",rcapplicant_batchschedule_tran.voiduserunkid " & _
            '               ",rcapplicant_batchschedule_tran.voidreason " & _
            '               ",rcbatchschedule_master.resultgroupunkid " & _
            '               ",ISNULL(rcapplicant_batchschedule_tran.iscancel,0) " & _
            '               ",ISNULL(rcapplicant_batchschedule_tran.isvoid,0) " & _
            '        "FROM    rcapplicant_batchschedule_tran " & _
            '                "JOIN rcbatchschedule_master ON rcbatchschedule_master.batchscheduleunkid = rcapplicant_batchschedule_tran.batchscheduleunkid " & _
            '                "JOIN rcapplicant_master ON rcapplicant_master.applicantunkid = rcapplicant_batchschedule_tran.applicantunkid " & _
            '                "JOIN cfcommon_master ON cfcommon_master.masterunkid = rcbatchschedule_master.interviewtypeunkid " & _
            '                "JOIN rcvacancy_master ON rcvacancy_master.vacancyunkid = rcbatchschedule_master.vacancyunkid " & _
            '        "WHERE   1 = 1 "
            'S.SANDEEP [ 10 June 2011 ] -- START
            'ISSUE : MULTIPLE INTERVIEW TYPE FOR ONE VACANCY
            'strQ = "SELECT  rcapplicant_batchschedule_tran.appbatchscheduletranunkid " & _
            '                ",rcapplicant_batchschedule_tran.batchscheduleunkid " & _
            '               ",rcapplicant_batchschedule_tran.applicantunkid " & _
            '               ",rcbatchschedule_master.batchcode AS BatchCode " & _
            '               ",rcbatchschedule_master.batchname AS BatchName " & _
            '               ",rcapplicant_master.firstname + ' ' + rcapplicant_master.surname AS ApplicantName " & _
            '               ",rcvacancy_master.vacancytitle AS vacancy " & _
            '               ",rcbatchschedule_master.vacancyunkid " & _
            '               ",rcbatchschedule_master.interviewtypeunkid " & _
            '               ",cfcommon_master.name AS InterviewType " & _
            '               ",CONVERT(CHAR(8),rcbatchschedule_master.interviewdate,112) AS Interviewdate " & _
            '               ",CONVERT(CHAR(5),rcbatchschedule_master.interviewtime,108) AS Interviewtime " & _
            '               ",rcbatchschedule_master.location AS location " & _
            '               ",rcbatchschedule_master.description AS description " & _
            '               ",rcapplicant_batchschedule_tran.userunkid " & _
            '               ",rcapplicant_batchschedule_tran.iscancel " & _
            '               ",rcapplicant_batchschedule_tran.canceldatetime " & _
            '               ",rcapplicant_batchschedule_tran.cancelremark " & _
            '               ",rcapplicant_batchschedule_tran.isvoid " & _
            '               ",rcapplicant_batchschedule_tran.voiddatetime " & _
            '               ",rcapplicant_batchschedule_tran.voiduserunkid " & _
            '               ",rcapplicant_batchschedule_tran.voidreason " & _
            '               ",rcbatchschedule_master.resultgroupunkid " & _
            '               ",ISNULL(rcapplicant_batchschedule_tran.iscancel,0) " & _
            '               ",ISNULL(rcapplicant_batchschedule_tran.isvoid,0) " & _
            '        "FROM    rcapplicant_batchschedule_tran " & _
            '                "JOIN rcbatchschedule_master ON rcbatchschedule_master.batchscheduleunkid = rcapplicant_batchschedule_tran.batchscheduleunkid " & _
            '                "JOIN rcapplicant_master ON rcapplicant_master.applicantunkid = rcapplicant_batchschedule_tran.applicantunkid " & _
            '                "JOIN cfcommon_master ON cfcommon_master.masterunkid = rcbatchschedule_master.interviewtypeunkid " & _
            '                "JOIN rcvacancy_master ON rcvacancy_master.vacancyunkid = rcbatchschedule_master.vacancyunkid " & _
            '        "WHERE   1 = 1 AND rcapplicant_batchschedule_tran.isvoid = 0 "           



            'S.SANDEEP [ 28 FEB 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            'strQ = "SELECT  rcapplicant_batchschedule_tran.appbatchscheduletranunkid " & _
            '               ",rcapplicant_batchschedule_tran.batchscheduleunkid " & _
            '               ",rcapplicant_batchschedule_tran.applicantunkid " & _
            '               ",rcbatchschedule_master.batchcode AS BatchCode " & _
            '               ",rcbatchschedule_master.batchname AS BatchName " & _
            '               ",rcapplicant_master.firstname + ' ' + rcapplicant_master.surname AS ApplicantName " & _
            '               ",rcvacancy_master.vacancytitle AS vacancy " & _
            '               ",rcbatchschedule_master.vacancyunkid " & _
            '               ",rcbatchschedule_master.interviewtypeunkid " & _
            '               ",cfcommon_master.name AS InterviewType " & _
            '               ",CONVERT(CHAR(8),rcbatchschedule_master.interviewdate,112) AS Interviewdate " & _
            '               ",CONVERT(CHAR(5),rcbatchschedule_master.interviewtime,108) AS Interviewtime " & _
            '               ",rcbatchschedule_master.location AS location " & _
            '               ",rcbatchschedule_master.description AS description " & _
            '               ",rcapplicant_batchschedule_tran.userunkid " & _
            '               ",rcapplicant_batchschedule_tran.iscancel " & _
            '               ",rcapplicant_batchschedule_tran.canceldatetime " & _
            '               ",rcapplicant_batchschedule_tran.cancelremark " & _
            '               ",rcapplicant_batchschedule_tran.isvoid " & _
            '               ",rcapplicant_batchschedule_tran.voiddatetime " & _
            '               ",rcapplicant_batchschedule_tran.voiduserunkid " & _
            '               ",rcapplicant_batchschedule_tran.voidreason " & _
            '               ",rcbatchschedule_master.resultgroupunkid " & _
            '               ",ISNULL(rcapplicant_batchschedule_tran.iscancel,0) " & _
            '               ",ISNULL(rcapplicant_batchschedule_tran.isvoid,0) " & _
            '        "FROM rcapplicant_batchschedule_tran " & _
            '               "JOIN rcbatchschedule_master ON rcbatchschedule_master.batchscheduleunkid = rcapplicant_batchschedule_tran.batchscheduleunkid " & _
            '               "JOIN rcapplicant_master ON rcapplicant_master.applicantunkid = rcapplicant_batchschedule_tran.applicantunkid " & _
            '               "JOIN cfcommon_master ON cfcommon_master.masterunkid = rcbatchschedule_master.interviewtypeunkid " & _
            '               "JOIN rcvacancy_master ON rcvacancy_master.vacancyunkid = rcbatchschedule_master.vacancyunkid " & _
            '        "WHERE   1 = 1 AND rcapplicant_batchschedule_tran.isvoid = 0 "

            'S.SANDEEP [ 09 JULY 2013 ] -- START
            'ENHANCEMENT : OTHER CHANGES
            'strQ = "SELECT  rcapplicant_batchschedule_tran.appbatchscheduletranunkid " & _
            '       ",rcapplicant_batchschedule_tran.batchscheduleunkid " & _
            '       ",rcapplicant_batchschedule_tran.applicantunkid " & _
            '       ",rcbatchschedule_master.batchcode AS BatchCode " & _
            '       ",rcbatchschedule_master.batchname AS BatchName " & _
            '       ",rcapplicant_master.firstname + ' ' + rcapplicant_master.surname AS ApplicantName " & _
            '       ",VM.name AS vacancy " & _
            '       ",rcbatchschedule_master.vacancyunkid " & _
            '       ",rcbatchschedule_master.interviewtypeunkid " & _
            '       ",cfcommon_master.name AS InterviewType " & _
            '       ",CONVERT(CHAR(8),rcbatchschedule_master.interviewdate,112) AS Interviewdate " & _
            '       ",CONVERT(CHAR(5),rcbatchschedule_master.interviewtime,108) AS Interviewtime " & _
            '       ",rcbatchschedule_master.location AS location " & _
            '       ",rcbatchschedule_master.description AS description " & _
            '       ",rcapplicant_batchschedule_tran.userunkid " & _
            '       ",rcapplicant_batchschedule_tran.iscancel " & _
            '       ",rcapplicant_batchschedule_tran.canceldatetime " & _
            '       ",rcapplicant_batchschedule_tran.cancelremark " & _
            '       ",rcapplicant_batchschedule_tran.isvoid " & _
            '       ",rcapplicant_batchschedule_tran.voiddatetime " & _
            '       ",rcapplicant_batchschedule_tran.voiduserunkid " & _
            '       ",rcapplicant_batchschedule_tran.voidreason " & _
            '       ",rcbatchschedule_master.resultgroupunkid " & _
            '       ",ISNULL(rcapplicant_batchschedule_tran.iscancel,0) as iscancel " & _
            '       ",ISNULL(rcapplicant_batchschedule_tran.isvoid,0) as isvoid " & _
            '       ",CONVERT(CHAR(8),rcvacancy_master.openingdate,112) AS Odate " & _
            '       ",CONVERT(CHAR(8),rcvacancy_master.closingdate,112) AS Cdate " & _
            '       "FROM rcapplicant_batchschedule_tran " & _
            '       "JOIN rcbatchschedule_master ON rcbatchschedule_master.batchscheduleunkid = rcapplicant_batchschedule_tran.batchscheduleunkid " & _
            '       "JOIN rcapplicant_master ON rcapplicant_master.applicantunkid = rcapplicant_batchschedule_tran.applicantunkid " & _
            '       "JOIN cfcommon_master ON cfcommon_master.masterunkid = rcbatchschedule_master.interviewtypeunkid " & _
            '       "JOIN rcvacancy_master ON rcvacancy_master.vacancyunkid = rcbatchschedule_master.vacancyunkid " & _
            '       "JOIN cfcommon_master AS VM ON VM.masterunkid = rcvacancy_master.vacancytitle AND VM.mastertype = '" & clsCommon_Master.enCommonMaster.VACANCY_MASTER & "' " & _
            '       "WHERE   1 = 1 AND rcapplicant_batchschedule_tran.isvoid = 0 "

            strQ = "SELECT " & _
                       " @Batch+''+rcbatchschedule_master.batchname+' -> '+@AppName+''+rcapplicant_master.firstname + ' ' + rcapplicant_master.surname AS GName " & _
                       ",rcapplicant_batchschedule_tran.appbatchscheduletranunkid " & _
                           ",rcapplicant_batchschedule_tran.batchscheduleunkid " & _
                           ",rcapplicant_batchschedule_tran.applicantunkid " & _
                           ",rcbatchschedule_master.batchcode AS BatchCode " & _
                           ",rcbatchschedule_master.batchname AS BatchName " & _
                           ",rcapplicant_master.firstname + ' ' + rcapplicant_master.surname AS ApplicantName " & _
                           ",VM.name AS vacancy " & _
                           ",rcbatchschedule_master.vacancyunkid " & _
                           ",rcbatchschedule_master.interviewtypeunkid " & _
                           ",cfcommon_master.name AS InterviewType " & _
                           ",CONVERT(CHAR(8),rcbatchschedule_master.interviewdate,112) AS Interviewdate " & _
                           ",CONVERT(CHAR(5),rcbatchschedule_master.interviewtime,108) AS Interviewtime " & _
                           ",rcbatchschedule_master.location AS location " & _
                           ",rcbatchschedule_master.description AS description " & _
                           ",rcapplicant_batchschedule_tran.userunkid " & _
                           ",rcapplicant_batchschedule_tran.iscancel " & _
                           ",rcapplicant_batchschedule_tran.canceldatetime " & _
                           ",rcapplicant_batchschedule_tran.cancelremark " & _
                           ",rcapplicant_batchschedule_tran.isvoid " & _
                           ",rcapplicant_batchschedule_tran.voiddatetime " & _
                           ",rcapplicant_batchschedule_tran.voiduserunkid " & _
                           ",rcapplicant_batchschedule_tran.voidreason " & _
                           ",rcbatchschedule_master.resultgroupunkid " & _
                           ",ISNULL(rcapplicant_batchschedule_tran.iscancel,0) as iscancel " & _
                           ",ISNULL(rcapplicant_batchschedule_tran.isvoid,0) as isvoid " & _
                           ",CONVERT(CHAR(8),rcvacancy_master.openingdate,112) AS Odate " & _
                           ",CONVERT(CHAR(8),rcvacancy_master.closingdate,112) AS Cdate " & _
                       ",ISNULL(CASE WHEN rcbatchschedule_interviewer_tran.interviewerunkid > 0 AND resultcodeunkid > 0  THEN hremployee_master.firstname + ' ' + hremployee_master.surname " & _
                       "             WHEN rcbatchschedule_interviewer_tran.interviewerunkid <= 0 AND resultcodeunkid > 0 THEN rcbatchschedule_interviewer_tran.otherinterviewer_name END,'') AS reviewer " & _
                       ",ISNULL(hrresult_master.resultname,'') AS score " & _
                    "FROM rcapplicant_batchschedule_tran " & _
                       "LEFT JOIN rcinterviewanalysis_master ON rcapplicant_batchschedule_tran.appbatchscheduletranunkid = rcinterviewanalysis_master.appbatchscheduletranunkid AND rcinterviewanalysis_master.isvoid = 0 " & _
                       "LEFT JOIN rcinterviewanalysis_tran ON rcinterviewanalysis_master.analysisunkid = rcinterviewanalysis_tran.analysisunkid AND rcinterviewanalysis_tran.isvoid = 0 " & _
                       "LEFT JOIN rcbatchschedule_interviewer_tran ON rcbatchschedule_interviewer_tran.batchscheduleinterviewertranunkid = rcinterviewanalysis_tran.interviewertranunkid " & _
                       "LEFT JOIN hremployee_master ON hremployee_master.employeeunkid = rcbatchschedule_interviewer_tran.interviewerunkid " & _
                       "LEFT JOIN hrresult_master ON hrresult_master.resultunkid = rcinterviewanalysis_tran.resultcodeunkid " & _
                           "JOIN rcbatchschedule_master ON rcbatchschedule_master.batchscheduleunkid = rcapplicant_batchschedule_tran.batchscheduleunkid " & _
                           "JOIN rcapplicant_master ON rcapplicant_master.applicantunkid = rcapplicant_batchschedule_tran.applicantunkid " & _
                           "JOIN cfcommon_master ON cfcommon_master.masterunkid = rcbatchschedule_master.interviewtypeunkid " & _
                           "JOIN rcvacancy_master ON rcvacancy_master.vacancyunkid = rcbatchschedule_master.vacancyunkid " & _
                           "JOIN cfcommon_master AS VM ON VM.masterunkid = rcvacancy_master.vacancytitle AND VM.mastertype = '" & clsCommon_Master.enCommonMaster.VACANCY_MASTER & "' " & _
                   "WHERE 1 = 1 AND rcapplicant_batchschedule_tran.isvoid = 0 " & _
                   "AND (CASE WHEN rcinterviewanalysis_master.appbatchscheduletranunkid > 0 AND ISNULL(resultunkid,0) > 0 THEN 1 " & _
                             "WHEN rcinterviewanalysis_master.appbatchscheduletranunkid > 0 AND ISNULL(resultunkid,0) <= 0 THEN -1 " & _
                   "ELSE 0 END) IN (1,0) "
            'Hemant (30 Oct 2019) -- [rcinterviewer_tran --> rcbatchschedule_interviewer_tran]
            'S.SANDEEP [ 09 JULY 2013 ] -- END


            'S.SANDEEP [ 28 FEB 2012 ] -- END




            If intBatchUnkid > 0 Then
                strQ &= " AND rcbatchschedule_master.batchscheduleunkid = @Batchunkid "
                objDataOperation.AddParameter("@Batchunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intBatchUnkid)
            End If
            'S.SANDEEP [ 10 June 2011 ] -- END 
            'Sandeep [ 21 APRIL 2011 ] -- End 


            If intAppBatchScheduleTranId > 0 Then
                'S.SANDEEP [ 09 JULY 2013 ] -- START
                'ENHANCEMENT : OTHER CHANGES
                'strQ &= " AND rcapplicant_batchschedule_tran.appbatchscheduletranunkid = @AppbatchScheduleId "
                strQ &= " AND rcapplicant_batchschedule_tran.appbatchscheduletranunkid = @AppbatchScheduleId "
                'S.SANDEEP [ 09 JULY 2013 ] -- END
                objDataOperation.AddParameter("@AppbatchScheduleId", SqlDbType.Int, eZeeDataType.INT_SIZE, intAppBatchScheduleTranId)
            End If


            'S.SANDEEP [20-APR-2017] -- START
            'ISSUE/ENHANCEMENT : OPTIMIZING RECRUITMENT MODULE
            If strFilter.Trim.Length > 0 Then
                strQ &= " AND " & strFilter
            End If
            'S.SANDEEP [20-APR-2017] -- END



            'S.SANDEEP [ 09 JULY 2013 ] -- START
            'ENHANCEMENT : OTHER CHANGES
            objDataOperation.AddParameter("@Batch", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 2, "Batch : "))
            objDataOperation.AddParameter("@AppName", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 3, "Applicant : "))
            'S.SANDEEP [ 09 JULY 2013 ] -- END

            dsList = objDataOperation.ExecQuery(strQ, strTableName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            'S.SANDEEP [20-APR-2017] -- START
            'ISSUE/ENHANCEMENT : OPTIMIZING RECRUITMENT MODULE
            dsList.Tables(0).AsEnumerable.ToList.ForEach(Function(x) updatevacany(x))
            'S.SANDEEP [20-APR-2017] -- END



        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
        Return dsList
    End Function

    'S.SANDEEP [20-APR-2017] -- START
    'ISSUE/ENHANCEMENT : OPTIMIZING RECRUITMENT MODULE
    Private Function updatevacany(ByVal x As DataRow) As Boolean
        x.Item("vacancy") = x.Item("vacancy").ToString & " ( " & eZeeDate.convertDate(x.Item("Odate").ToString).ToShortDateString & " - " & eZeeDate.convertDate(x.Item("Cdate").ToString).ToShortDateString & " )"
        Return True
    End Function
    'S.SANDEEP [20-APR-2017] -- END


    


    ''' <summary>
    ''' Modify By: Anjan
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> INSERT INTO Database Table (rcapplicant_batchschedule_tran) </purpose>
    Public Function Insert() As Boolean
        'If isExist(mstrName) Then
        '    mstrMessage = "<Message>"
        '    Return False
        'End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation
        objDataOperation.BindTransaction()

        Try
            objDataOperation.AddParameter("@batchscheduleunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintBatchscheduleunkid.ToString)
            objDataOperation.AddParameter("@applicantunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintApplicantunkid.ToString)
            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
            objDataOperation.AddParameter("@iscancel", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIscancel.ToString)
            If mdtCanceldatetime = Nothing Then
                objDataOperation.AddParameter("@canceldatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            Else
                objDataOperation.AddParameter("@canceldatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtCanceldatetime)
            End If

            objDataOperation.AddParameter("@cancelremark", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrCancelremark.ToString)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            If mdtVoiddatetime = Nothing Then
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            Else
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime)
            End If

            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)

            strQ = "INSERT INTO rcapplicant_batchschedule_tran ( " & _
                          "  batchscheduleunkid " & _
                          ", applicantunkid " & _
                          ", userunkid " & _
                          ", iscancel " & _
                          ", canceldatetime " & _
                          ", cancelremark " & _
                          ", isvoid " & _
                          ", voiddatetime " & _
                          ", voiduserunkid " & _
                          ", voidreason" & _
                  ") VALUES (" & _
                          "  @batchscheduleunkid " & _
                          ", @applicantunkid " & _
                          ", @userunkid " & _
                          ", @iscancel " & _
                          ", @canceldatetime " & _
                          ", @cancelremark " & _
                          ", @isvoid " & _
                          ", @voiddatetime " & _
                          ", @voiduserunkid " & _
                          ", @voidreason" & _
                 "); SELECT @@identity"

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            mintAppbatchscheduletranunkid = dsList.Tables(0).Rows(0).Item(0)


            'Pinkal (24-Jul-2012) -- Start
            'Enhancement : TRA Changes

'S.SANDEEP [28-May-2018] -- START
'ISSUE/ENHANCEMENT : {Audit Trails} 
Dim objCommonATLog AS New clsCommonATLog 
objCommonATLog._FormName = mstrFormName 
objCommonATLog._LoginEmployeeUnkid = mintLoginEmployeeunkid 
            objCommonATLog._ClientIP = mstrClientIP
            objCommonATLog._HostName = mstrHostName
            objCommonATLog._FromWeb = mblnIsWeb
            objCommonATLog._AuditUserId = mintAuditUserId
objCommonATLog._CompanyUnkid = mintCompanyUnkid
            objCommonATLog._AuditDate = mdtAuditDate
            'S.SANDEEP [28-May-2018] -- END

            If objCommonATLog.Insert_AtLog(objDataOperation, 1, "rcapplicant_batchschedule_tran", "appbatchscheduletranunkid", mintAppbatchscheduletranunkid) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            'S.SANDEEP [28-May-2018] -- START
            'ISSUE/ENHANCEMENT : {Audit Trails} 
            objCommonATLog = Nothing
            'S.SANDEEP [28-May-2018] -- END


            objDataOperation.ReleaseTransaction(True)

            'Pinkal (24-Jul-2012) -- End

            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Insert; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    '''' <summary>
    '''' Modify By: Anjan
    '''' </summary>
    '''' <returns>Boolean</returns>
    '''' <purpose> Update Database Table (rcapplicant_batchschedule_tran) </purpose>
    'Public Function Update() As Boolean
    '    If isExist(mintBatchscheduleunkid, mintApplicantunkid, mintAppbatchscheduletranunkid) Then
    '        mstrMessage = Language.getMessage(mstrModuleName, 111, "This applicant already exists in this batch.")
    '        Return False
    '    End If

    '    Dim dsList As DataSet = Nothing
    '    Dim strQ As String = ""
    '    Dim exForce As Exception

    '    objDataOperation = New clsDataOperation

    '    Try
    '        objDataOperation.AddParameter("@appbatchscheduletranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAppbatchscheduletranunkid.ToString)
    '        objDataOperation.AddParameter("@batchscheduleunkid", SqlDbType.int, eZeeDataType.INT_SIZE, mintbatchscheduleunkid.ToString)
    '        objDataOperation.AddParameter("@applicantunkid", SqlDbType.int, eZeeDataType.INT_SIZE, mintapplicantunkid.ToString)
    '        objDataOperation.AddParameter("@userunkid", SqlDbType.int, eZeeDataType.INT_SIZE, mintuserunkid.ToString)
    '        objDataOperation.AddParameter("@iscancel", SqlDbType.bit, eZeeDataType.BIT_SIZE, mblniscancel.ToString)

    '        If mdtCanceldatetime = Nothing Then
    '            objDataOperation.AddParameter("@canceldatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
    '        Else
    '            objDataOperation.AddParameter("@canceldatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtCanceldatetime)
    '        End If

    '        objDataOperation.AddParameter("@cancelremark", SqlDbType.nvarchar, eZeeDataType.NAME_SIZE, mstrcancelremark.ToString)
    '        objDataOperation.AddParameter("@isvoid", SqlDbType.bit, eZeeDataType.BIT_SIZE, mblnisvoid.ToString)
    '        If mdtVoiddatetime = Nothing Then
    '            objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
    '        Else
    '            objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime)
    '        End If

    '        objDataOperation.AddParameter("@voiduserunkid", SqlDbType.int, eZeeDataType.INT_SIZE, mintvoiduserunkid.ToString)
    '        objDataOperation.AddParameter("@voidreason", SqlDbType.nvarchar, eZeeDataType.NAME_SIZE, mstrvoidreason.ToString)

    '        strQ = "UPDATE rcapplicant_batchschedule_tran SET " & _
    '                      "  batchscheduleunkid = @batchscheduleunkid" & _
    '                      ", applicantunkid = @applicantunkid" & _
    '                      ", userunkid = @userunkid" & _
    '                      ", iscancel = @iscancel" & _
    '                      ", canceldatetime = @canceldatetime" & _
    '                      ", cancelremark = @cancelremark" & _
    '                      ", isvoid = @isvoid" & _
    '                      ", voiddatetime = @voiddatetime" & _
    '                      ", voiduserunkid = @voiduserunkid" & _
    '                      ", voidreason = @voidreason " & _
    '                "WHERE appbatchscheduletranunkid = @appbatchscheduletranunkid "

    '        Call objDataOperation.ExecNonQuery(strQ)

    '        If objDataOperation.ErrorMessage <> "" Then
    '            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
    '            Throw exForce
    '        End If

    '        Return True
    '    Catch ex As Exception
    '        Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
    '        Return False
    '    Finally
    '        exForce = Nothing
    '        If dsList IsNot Nothing Then dsList.Dispose()
    '        objDataOperation = Nothing
    '    End Try
    'End Function

    ''' <summary>
    ''' Modify By: Anjan
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Delete Database Table (rcapplicant_batchschedule_tran) </purpose>
    Public Function Delete(ByVal intUnkid As Integer) As Boolean
        'Sandeep [ 09 Oct 2010 ] -- Start
        'If isUsed(intUnkid) Then
        '    mstrMessage = "<Message>"
        '    Return False
        'End If

        If isUsed(intUnkid) Then
            mstrMessage = Language.getMessage(mstrModuleName, 1, "Sorry you cannot delete this applicant form batch. Reason : This applicant is already linked with some transactions.")
            Return False
        End If
        'Sandeep [ 09 Oct 2010 ] -- End 

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation
        objDataOperation.BindTransaction()

        Try
            strQ = "UPDATE rcapplicant_batchschedule_tran SET " & _
                          "  isvoid = @isvoid" & _
                          ", voiddatetime = @voiddatetime" & _
                          ", voiduserunkid = @voiduserunkid" & _
                          ", voidreason = @voidreason " & _
                    "WHERE appbatchscheduletranunkid = @appbatchscheduletranunkid "


            objDataOperation.AddParameter("@appbatchscheduletranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            If mdtVoiddatetime = Nothing Then
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            Else
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime)
            End If

            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            'Pinkal (24-Jul-2012) -- Start
            'Enhancement : TRA Changes

            'S.SANDEEP [28-May-2018] -- START
            'ISSUE/ENHANCEMENT : {Audit Trails} 
            Dim objCommonATLog As New clsCommonATLog
            objCommonATLog._FormName = mstrFormName
            objCommonATLog._LoginEmployeeUnkid = mintLoginEmployeeunkid
            objCommonATLog._ClientIP = mstrClientIP
            objCommonATLog._HostName = mstrHostName
            objCommonATLog._FromWeb = mblnIsWeb
            objCommonATLog._AuditUserId = mintAuditUserId
objCommonATLog._CompanyUnkid = mintCompanyUnkid
            objCommonATLog._AuditDate = mdtAuditDate
            'S.SANDEEP [28-May-2018] -- END

            If objCommonATLog.Insert_AtLog(objDataOperation, 3, "rcapplicant_batchschedule_tran", "appbatchscheduletranunkid", intUnkid) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            'S.SANDEEP [28-May-2018] -- START
            'ISSUE/ENHANCEMENT : {Audit Trails} 
            objCommonATLog = Nothing
            'S.SANDEEP [28-May-2018] -- END


            objDataOperation.ReleaseTransaction(True)

            'Pinkal (24-Jul-2012) -- End

            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function


    ''' <summary>
    ''' Modify By: Anjan
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Cancel Database Table (rcapplicant_batchschedule_tran) </purpose>
    Public Function Cancel_Applicant(ByVal intUnkid As Integer) As Boolean
        'If isUsed(intUnkid) Then
        '    mstrMessage = "<Message>"
        '    Return False
        'End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "UPDATE rcapplicant_batchschedule_tran SET " & _
                          " iscancel = @iscancel" & _
                          ", canceldatetime = @canceldatetime" & _
                          ", cancelremark = @cancelremark " & _
                    "WHERE appbatchscheduletranunkid = @appbatchscheduletranunkid "


            objDataOperation.AddParameter("@appbatchscheduletranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)
            objDataOperation.AddParameter("@iscancel", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIscancel.ToString)

            If mdtCanceldatetime = Nothing Then
                objDataOperation.AddParameter("@canceldatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            Else
                objDataOperation.AddParameter("@canceldatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtCanceldatetime)
            End If

            objDataOperation.AddParameter("@cancelremark", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrCancelremark.ToString)

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function
    ''' <summary>
    ''' Modify By: Anjan
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isUsed(ByVal intUnkid As Integer) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try

            'Sandeep [ 09 Oct 2010 ] -- Start
            'strQ = "<Query>"
            strQ = "SELECT appbatchscheduletranunkid FROM rcinterviewanalysis_master WHERE appbatchscheduletranunkid = @appbatchscheduletranunkid "
            'Sandeep [ 09 Oct 2010 ] -- End 

            objDataOperation.AddParameter("@appbatchscheduletranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            mstrMessage = ""

            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isUsed; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Anjan
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isExist(Optional ByVal mintbatchscheduleunkid As Integer = -1, Optional ByVal mintApplicantunkid As Integer = -1, Optional ByVal intUnkid As Integer = -1) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "SELECT " & _
                      "  appbatchscheduletranunkid " & _
                      ", batchscheduleunkid " & _
                      ", applicantunkid " & _
                      ", userunkid " & _
                      ", iscancel " & _
                      ", canceldatetime " & _
                      ", cancelremark " & _
                      ", isvoid " & _
                      ", voiddatetime " & _
                      ", voiduserunkid " & _
                      ", voidreason " & _
                "FROM rcapplicant_batchschedule_tran " & _
                "WHERE batchscheduleunkid = @batchscheduleunkid " & _
                "AND applicantunkid = @applicantunkid "

            If intUnkid > 0 Then
                strQ &= " AND appbatchscheduletranunkid <> @appbatchscheduletranunkid"
            End If

            objDataOperation.AddParameter("@batchscheduleunkid", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mintbatchscheduleunkid)
            objDataOperation.AddParameter("@applicantunkid", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mintApplicantunkid)
            objDataOperation.AddParameter("@appbatchscheduletranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isExist; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    Public Function GetVacancySkills(ByVal strSillCategoryunkid As String, ByVal intJobunkid As Integer) As DataSet
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "SELECT  hrjob_skill_tran.skillcategoryunkid " & _
                            ", hrjob_skill_tran.skillunkid " & _
                            ", cfcommon_master.name AS skillcategory " & _
                            ", hrskill_master.skillname AS skillname " & _
                    "FROM  hrjob_skill_tran " & _
                    "JOIN cfcommon_master ON cfcommon_master.masterunkid=hrjob_skill_tran.skillcategoryunkid " & _
                    "JOIN hrskill_master ON hrskill_master.skillunkid=hrjob_skill_tran.skillunkid " & _
                    "WHERE  jobunkid=@jobunkid  AND " & _
                    "hrjob_skill_tran.skillcategoryunkid IN ( " & strSillCategoryunkid & " )"

            objDataOperation.AddParameter("@jobunkid", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, intJobunkid)


            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetVacancySkills", mstrModuleName)
            Return Nothing
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try

    End Function

    Public Function GetVacancySkillCategory(ByVal intJobunkid As Integer) As DataSet
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "SELECT  distinct hrjob_skill_tran.skillcategoryunkid " & _
                            ", cfcommon_master.name AS skillcategory " & _
                    "FROM  hrjob_skill_tran " & _
                    "JOIN cfcommon_master ON cfcommon_master.masterunkid=hrjob_skill_tran.skillcategoryunkid " & _
                    "WHERE jobunkid=@jobunkid "

            objDataOperation.AddParameter("@jobunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intJobunkid.ToString)


            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetVacancySkillCategory_skill", mstrModuleName)
            Return Nothing
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try

    End Function

    ''' <summary>
    ''' Modify By: Anjan
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function Update_changebatch(ByVal intappbatchschedule As Integer) As Boolean

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation
        objDataOperation.BindTransaction()

        Try

            strQ = "UPDATE rcapplicant_batchschedule_tran SET " & _
                        " batchscheduleunkid = @batchscheduleunkid " & _
                        " ,ischanged = @IsChanged " & _
                    "WHERE rcapplicant_batchschedule_tran.appbatchscheduletranunkid = @appbatchscheduleunkid "

            objDataOperation.AddParameter("@batchscheduleunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintBatchscheduleunkid)
            objDataOperation.AddParameter("@appbatchscheduleunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intappbatchschedule)
            objDataOperation.AddParameter("@IsChanged", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsChanged)

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            'S.SANDEEP [28-May-2018] -- START
            'ISSUE/ENHANCEMENT : {Audit Trails} 
            Dim objCommonATLog As New clsCommonATLog
            objCommonATLog._FormName = mstrFormName
            objCommonATLog._LoginEmployeeUnkid = mintLoginEmployeeunkid
            objCommonATLog._ClientIP = mstrClientIP
            objCommonATLog._HostName = mstrHostName
            objCommonATLog._FromWeb = mblnIsWeb
            objCommonATLog._AuditUserId = mintAuditUserId
objCommonATLog._CompanyUnkid = mintCompanyUnkid
            objCommonATLog._AuditDate = mdtAuditDate
            'S.SANDEEP [28-May-2018] -- END

            If objCommonATLog.Insert_AtLog(objDataOperation, 2, "rcapplicant_batchschedule_tran", "appbatchscheduletranunkid", intappbatchschedule) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            'S.SANDEEP [28-May-2018] -- START
            'ISSUE/ENHANCEMENT : {Audit Trails} 
            objCommonATLog = Nothing
            'S.SANDEEP [28-May-2018] -- END


            objDataOperation.ReleaseTransaction(True)
            Return True

        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            DisplayError.Show("-1", ex.Message, "Update_changebatch", mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try

    End Function
    ''' <summary>
    ''' Modify By: Anjan
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function GetApplicantBatch(ByVal strTableName As String, ByVal intApplicantId As Integer) As String
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim mstrBatchId As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = " SELECT  batchscheduleunkid " & _
                        "FROM rcapplicant_batchschedule_tran " & _
                    "WHERE applicantunkid = @Applicantunkid " & _
                    "AND ISNULL(iscancel ,0) = 0 " & _
                    "AND ISNULL(isvoid ,0) = 0 "

            objDataOperation.AddParameter("@Applicantunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intApplicantId)


            dsList = objDataOperation.ExecQuery(strQ, strTableName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For i As Integer = 0 To dsList.Tables(0).Rows.Count - 1
                If dsList.Tables(0).Rows.Count > 0 Then
                    If i = 0 Then
                        mstrBatchId = CStr(dsList.Tables(0).Rows(0).Item(0))
                    Else
                        mstrBatchId &= "," & CStr(dsList.Tables(0).Rows(i).Item(0))
                    End If
                End If
            Next

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
        Return mstrBatchId
    End Function

    'Hemant (07 Oct 2019) -- Start
    'ISSUE/ENHANCEMENT(NMB) :  RECRUITMENTS UAT CHANGES(Point No 14 : On interview scheduling, system should be able to send automatic emails to applicants that have been placed in a batch. The content of the email should be picked from a template set on configuration.)
    Public Function Set_Notification_Applicants(ByVal xDatabaseName As String, _
                                                 ByVal xUserId As Integer, _
                                                 ByVal xYearUnkId As Integer, _
                                                 ByVal xCompanyUnkid As Integer, _
                                                 ByVal strEmailContent As String, _
                                                 ByVal dtRow() As DataRow, _
                                                 Optional ByVal iLoginTypeId As Integer = 0, _
                                                 Optional ByVal iLoginEmployeeId As Integer = 0, _
                                                 Optional ByVal iUserId As Integer = 0, _
                                                 Optional ByVal blnIsSendMail As Boolean = True, _
                                                 Optional ByRef lstEmailList As List(Of clsEmailCollection) = Nothing, _
                                                 Optional ByVal strCCAddress As String = "" _
                                                 )
        'Hemant (12 Nov 2020) -- [strCCAddress]
        Dim objMail As New clsSendMail
        Dim strSubject As String = ""
        objEmailList = New List(Of clsEmailCollection)
        Try
            strSubject = Language.getMessage(mstrModuleName, 4, "Notification for Interview Schedule")

            objMail._Subject = strSubject

            objMail._Message = strEmailContent
            'Hemant (24 Feb 2022) -- Start         
            'ISSUE/ENHANCEMENT(NMB) : applicants are notified manually. they normally call them one by one -- Louis. 
            'objMail._ToEmail = dtRow(0).Item("email").ToString
            objMail._ToEmail = strCCAddress
            'Hemant (24 Feb 2022) -- End
            'Hemant (12 Nov 2020) -- Start
            'Enhancement : NMB Final Recruitment UAT v1 (Point - 5)- On interview scheduling screen, the notification sent to internal applicants should be cc to the employee reporting to
            'Hemant (24 Feb 2022) -- Start 
            'ISSUE/ENHANCEMENT(NMB) : applicants are notified manually. they normally call them one by one -- Louis. 
            'objMail._CCAddress = strCCAddress
            'Hemant (24 Feb 2022) -- End
            'Hemant (12 Nov 2020) -- End
            If iLoginTypeId <= 0 Then iLoginTypeId = enLogin_Mode.DESKTOP
            If mstrWebFormName.Trim.Length > 0 Then
                objMail._Form_Name = mstrWebFormName
            End If
            objMail._LogEmployeeUnkid = iLoginEmployeeId
            objMail._OperationModeId = iLoginTypeId
            objMail._UserUnkid = IIf(iUserId <= 0, User._Object._Userunkid, iUserId)
            objMail._SenderAddress = dtRow(0).Item("email").ToString
            objMail._ModuleRefId = clsSendMail.enAT_VIEW_TYPE.RECRUITMENT_MGT


            If iLoginTypeId = enLogin_Mode.EMP_SELF_SERVICE Then
                Dim objEmp As New clsEmployee_Master
                objEmp._Employeeunkid(ConfigParameter._Object._CurrentDateAndTime) = iLoginEmployeeId
                Dim objEmailColl As New clsEmailCollection(objMail._ToEmail, objMail._Subject, objMail._Message, mstrWebFormName, mintLoginemployeeunkid, _
                                                           mstrWebClientIP, mstrWebHostName, 0, iLoginTypeId, clsSendMail.enAT_VIEW_TYPE.RECRUITMENT_MGT, _
                                                           IIf(objEmp._Email = "", objEmp._Firstname & " " & objEmp._Surname, objEmp._Email), , , , objMail._CCAddress)
                'Hemant (12 Nov 2020) -- [objMail._CCAddress]
                objEmailList.Add(objEmailColl)


                objEmp = Nothing

            Else
                Dim objUser As New clsUserAddEdit
                objUser._Userunkid = IIf(iUserId <= 0, User._Object._Userunkid, iUserId)
                Dim objEmailColl As New clsEmailCollection(objMail._ToEmail, objMail._Subject, objMail._Message, mstrWebFormName, mintLoginemployeeunkid, _
                                                           mstrWebClientIP, mstrWebHostName, objUser._Userunkid, iLoginTypeId, clsSendMail.enAT_VIEW_TYPE.RECRUITMENT_MGT, _
                                                           IIf(objUser._Email = "", objUser._Firstname & " " & objUser._Lastname, objUser._Email), , , , objMail._CCAddress)
                'Hemant (12 Nov 2020) -- [objMail._CCAddress]

                objEmailList.Add(objEmailColl)

                objUser = Nothing

            End If

            If blnIsSendMail = True Then

                If objEmailList.Count > 0 Then
                    If HttpContext.Current Is Nothing Then
                        objThread = New Thread(AddressOf Send_Notification)
                        objThread.IsBackground = True
                        Dim arr(1) As Object
                        arr(0) = xCompanyUnkid
                        objThread.Start(arr)
                    Else
                        Call Send_Notification(xCompanyUnkid)
                    End If
                End If
            Else
                lstEmailList = objEmailList
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Set_Notification_Applicants", mstrModuleName)
            Return ""
        End Try
    End Function

    Private Sub Send_Notification(ByVal intCompanyUnkid As Object)
        Try
            If objEmailList.Count > 0 Then
                Dim objSendMail As New clsSendMail
                For Each objEmail In objEmailList
                    objSendMail._ToEmail = objEmail._EmailTo
                    objSendMail._Subject = objEmail._Subject
                    objSendMail._Message = objEmail._Message
                    objSendMail._Form_Name = objEmail._FormName
                    objSendMail._LogEmployeeUnkid = objEmail._LogEmployeeUnkid
                    objSendMail._OperationModeId = objEmail._OperationModeId
                    objSendMail._UserUnkid = objEmail._UserUnkid
                    objSendMail._SenderAddress = objEmail._SenderAddress
                    objSendMail._ModuleRefId = objEmail._ModuleRefId


                    Dim intCUnkId As Integer = 0
                    If TypeOf intCompanyUnkid Is Integer Then
                        intCUnkId = intCompanyUnkid
                    Else
                        intCUnkId = CInt(intCompanyUnkid(0))
                    End If
                    If objSendMail.SendMail(intCUnkId).ToString.Length > 0 Then

                        Continue For
                    End If
                Next
                objEmailList.Clear()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Send_Notification", mstrModuleName)
        End Try
    End Sub
    'Hemant (07 Oct 2019) -- End

    'Hemant (03 Jun 2020) -- Start
    'ISSUE/ENHANCEMENT(NMB) :  RECRUITMENTS FINAL UAT CHANGES(Point No - 1 : On the interview analysis screen, they want to record interview scores for all applicants in a single window instead of analysing the interview an applicant at a time. Also, they want to record all the interview scores for all the interviewers on the same screen.)
    Public Function GetList_InterviewAnalysis(ByVal strTableName As String, Optional ByVal intAppBatchScheduleTranId As Integer = -1, Optional ByVal intBatchUnkid As Integer = -1, Optional ByVal strFilter As String = "") As DataSet 'S.SANDEEP [20-APR-2017] -- START {strFilter} -- END

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try

            strQ = "SELECT " & _
                       " DISTINCT rcapplicant_batchschedule_tran.appbatchscheduletranunkid " & _
                           ",rcapplicant_batchschedule_tran.batchscheduleunkid " & _
                           ",rcapplicant_batchschedule_tran.applicantunkid " & _
                           ",rcbatchschedule_master.batchcode AS BatchCode " & _
                           ",rcbatchschedule_master.batchname AS BatchName " & _
                           ",rcapplicant_master.firstname + ' ' + rcapplicant_master.surname AS ApplicantName " & _
                           ",VM.name AS vacancy " & _
                           ",rcbatchschedule_master.vacancyunkid " & _
                           ",rcbatchschedule_master.interviewtypeunkid " & _
                           ",cfcommon_master.name AS InterviewType " & _
                           ",CONVERT(CHAR(8),rcbatchschedule_master.interviewdate,112) AS Interviewdate " & _
                           ",CONVERT(CHAR(5),rcbatchschedule_master.interviewtime,108) AS Interviewtime " & _
                           ",rcbatchschedule_master.location AS location " & _
                           ",rcbatchschedule_master.description AS description " & _
                           ",rcapplicant_batchschedule_tran.userunkid " & _
                           ",rcapplicant_batchschedule_tran.iscancel " & _
                           ",rcapplicant_batchschedule_tran.canceldatetime " & _
                           ",rcapplicant_batchschedule_tran.cancelremark " & _
                           ",rcapplicant_batchschedule_tran.isvoid " & _
                           ",rcapplicant_batchschedule_tran.voiddatetime " & _
                           ",rcapplicant_batchschedule_tran.voiduserunkid " & _
                           ",rcapplicant_batchschedule_tran.voidreason " & _
                           ",rcbatchschedule_master.resultgroupunkid " & _
                           ",ISNULL(rcapplicant_batchschedule_tran.iscancel,0) as iscancel " & _
                           ",ISNULL(rcapplicant_batchschedule_tran.isvoid,0) as isvoid " & _
                           ",CONVERT(CHAR(8),rcvacancy_master.openingdate,112) AS Odate " & _
                           ",CONVERT(CHAR(8),rcvacancy_master.closingdate,112) AS Cdate " & _
                       ", ISNULL(rcinterviewanalysis_master.analysisunkid, 0 ) AS analysisunkid " & _
                       ", '' as AUD " & _
                    "FROM rcapplicant_batchschedule_tran " & _
                       "LEFT JOIN rcinterviewanalysis_master ON rcapplicant_batchschedule_tran.appbatchscheduletranunkid = rcinterviewanalysis_master.appbatchscheduletranunkid AND rcinterviewanalysis_master.isvoid = 0 " & _
                       "LEFT JOIN rcinterviewanalysis_tran ON rcinterviewanalysis_master.analysisunkid = rcinterviewanalysis_tran.analysisunkid " & _
                       "LEFT JOIN rcbatchschedule_interviewer_tran ON rcbatchschedule_interviewer_tran.batchscheduleinterviewertranunkid = rcinterviewanalysis_tran.interviewertranunkid " & _
                       "LEFT JOIN hremployee_master ON hremployee_master.employeeunkid = rcbatchschedule_interviewer_tran.interviewerunkid " & _
                           "JOIN rcbatchschedule_master ON rcbatchschedule_master.batchscheduleunkid = rcapplicant_batchschedule_tran.batchscheduleunkid " & _
                           "JOIN rcapplicant_master ON rcapplicant_master.applicantunkid = rcapplicant_batchschedule_tran.applicantunkid " & _
                           "JOIN cfcommon_master ON cfcommon_master.masterunkid = rcbatchschedule_master.interviewtypeunkid " & _
                           "JOIN rcvacancy_master ON rcvacancy_master.vacancyunkid = rcbatchschedule_master.vacancyunkid " & _
                           "JOIN cfcommon_master AS VM ON VM.masterunkid = rcvacancy_master.vacancytitle AND VM.mastertype = '" & clsCommon_Master.enCommonMaster.VACANCY_MASTER & "' " & _
                   "WHERE 1 = 1 AND rcapplicant_batchschedule_tran.isvoid = 0 "


            If intBatchUnkid > 0 Then
                strQ &= " AND rcbatchschedule_master.batchscheduleunkid = @Batchunkid "
                objDataOperation.AddParameter("@Batchunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intBatchUnkid)
            End If


            If intAppBatchScheduleTranId > 0 Then
                strQ &= " AND rcapplicant_batchschedule_tran.appbatchscheduletranunkid = @AppbatchScheduleId "
                objDataOperation.AddParameter("@AppbatchScheduleId", SqlDbType.Int, eZeeDataType.INT_SIZE, intAppBatchScheduleTranId)
            End If


            If strFilter.Trim.Length > 0 Then
                strQ &= " AND " & strFilter
            End If

            strQ &= " SELECT " & _
                    "rcinterviewanalysis_tran.analysistranunkid " & _
                   ",rcinterviewanalysis_tran.analysisunkid " & _
                   ",rcinterviewanalysis_tran.interviewertranunkid " & _
                   ",rcinterviewanalysis_tran.analysis_date " & _
                   ",rcinterviewanalysis_tran.resultcodeunkid " & _
                   ",rcinterviewanalysis_tran.remark " & _
                   ",rcinterviewanalysis_tran.isvoid " & _
                   ",rcinterviewanalysis_tran.voiduserunkid " & _
                   ",rcinterviewanalysis_tran.voiddatetime " & _
                   ",rcinterviewanalysis_tran.voidreason " & _
                   ",rcbatchschedule_interviewer_tran.batchscheduleinterviewertranunkid " & _
                   ",rcbatchschedule_interviewer_tran.interviewerunkid " & _
                   ",rcinterviewanalysis_master.applicantunkid " & _
                   ", '' as AUD " & _
                "FROM rcinterviewanalysis_tran " & _
                "JOIN rcinterviewanalysis_master " & _
                    "ON rcinterviewanalysis_master.analysisunkid = rcinterviewanalysis_tran.analysisunkid AND rcinterviewanalysis_master.isvoid = 0  " & _
                "JOIN rcapplicant_batchschedule_tran " & _
                    "ON rcapplicant_batchschedule_tran.appbatchscheduletranunkid = rcinterviewanalysis_master.appbatchscheduletranunkid " & _
                "JOIN rcbatchschedule_master " & _
                    "ON rcbatchschedule_master.batchscheduleunkid = rcapplicant_batchschedule_tran.batchscheduleunkid " & _
                "LEFT JOIN rcbatchschedule_interviewer_tran " & _
                    "ON rcbatchschedule_interviewer_traN.batchscheduleinterviewertranunkid = rcinterviewanalysis_tran.interviewertranunkid " & _
                "WHERE rcinterviewanalysis_tran.isvoid = 0 "

            If intBatchUnkid > 0 Then
                strQ &= " AND rcbatchschedule_master.batchscheduleunkid = @Batchunkid "
            End If

            If strFilter.Trim.Length > 0 Then
                strQ &= " AND " & strFilter
            End If

            strQ &= " ORDER BY analysisunkid "

            objDataOperation.AddParameter("@Batch", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 2, "Batch : "))
            objDataOperation.AddParameter("@AppName", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 3, "Applicant : "))

            dsList = objDataOperation.ExecQuery(strQ, strTableName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            dsList.Tables(0).AsEnumerable.ToList.ForEach(Function(x) updatevacany(x))


        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetList_InterviewAnalysis; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
        Return dsList
    End Function
    'Hemant (03 Jun 2020) -- End


	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Public Shared Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "Sorry you cannot delete this applicant form batch. Reason : This applicant is already linked with some transactions.")
			Language.setMessage(mstrModuleName, 2, "Batch :")
			Language.setMessage(mstrModuleName, 3, "Applicant :")
			Language.setMessage(mstrModuleName, 4, "Notification for Interview Schedule")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class