﻿'************************************************************************************************************************************
'Class Name : clsinterviewer_tran.vb
'Purpose    :
'Date       :29/07/2010
'Written By :Anjan
'Modified   :
'************************************************************************************************************************************

Imports eZeeCommonLib
Imports Aruti.Data.clsCommon_Master
''' <summary>
''' Purpose: 
''' Developer: Anjan
''' </summary>
Public Class clsinterviewer_tran
    Private Shared ReadOnly mstrModuleName As String = "clsinterviewer_tran"
    Dim objDataOperation As clsDataOperation
    Dim mstrMessage As String = ""
    'Hemant (13 Sep 2019) -- Start
    'ISSUE/ENHANCEMENT(NMB) :  RECRUITMENTS UAT CHANGES(Point No - 4 : After publishing an external vacancy, system should send job alerts to all the applicants that have created profiles in the portal. Email content as below.)
    Private mblnIsvoid As Boolean
    Private mintVoiduserunkid As Integer
    Private mdtVoiddatetime As Date
    Private mstrVoidreason As String = String.Empty
    'Hemant (13 Sep 2019) -- End


#Region " Private variables "
    Private mintInterviewertranunkid As Integer
    Private mintVacancyunkid As Integer = -1
    Private mintInterviewerunkid As Integer
    'Private mstrOtherinterviewer_Name As String = String.Empty
    'Private mstrOthercompany As String = String.Empty
    'Private mstrOtherdepartment As String = String.Empty
    'Private mstrOthercontact_No As String = String.Empty
    'Private mintUserunkid As Integer
    'Private mblnIsvoid As Boolean
    'Private mintVoiduserunkid As Integer
    'Private mdtVoiddatetime As Date
    'Private mstrVoidreason As String = String.Empty

    Private mdtTran As DataTable

#End Region

#Region " Properties "
'S.SANDEEP [28-May-2018] -- START 
'ISSUE/ENHANCEMENT : {Audit Trails} 
Private mstrFormName As String = String.Empty
Public WriteOnly Property _FormName() As String 
Set(ByVal value As String) 
mstrFormName = value 
End Set 
End Property 
 
Private mstrClientIP As String = ""
Public WriteOnly Property _ClientIP() As String 
Set(ByVal value As String) 
mstrClientIP = value 
End Set 
End Property 
 
Private mstrHostName As String = ""
Public WriteOnly Property _HostName() As String 
Set(ByVal value As String) 
mstrHostName = value 
End Set 
End Property 
 
Private mblnIsWeb As Boolean = False
Public WriteOnly Property _FromWeb() As Boolean 
Set(ByVal value As Boolean) 
mblnIsWeb = value 
End Set 
End Property 
 
    Private mintAuditUserId As Integer = 0
    Public WriteOnly Property _AuditUserId() As Integer
        Set(ByVal value As Integer)
            mintAuditUserId = value
        End Set
    End Property

    Private mintCompanyUnkid As Integer = 0
    Public WriteOnly Property _CompanyUnkid() As Integer
        Set(ByVal value As Integer)
            mintCompanyUnkid = value
        End Set
    End Property
 
Private mdtAuditDate As DateTime = Now
Public WriteOnly Property _AuditDate() As DateTime 
Set(ByVal value As DateTime) 
mdtAuditDate = value 
End Set 
End Property 
 
Private xDataOpr As clsDataOperation = Nothing
Public Property _xDataOpr() As clsDataOperation 
Get 
Return xDataOpr 
End Get 
Set(ByVal value As clsDataOperation) 
xDataOpr = value 
End Set 
End Property 
 
Private mintLoginEmployeeunkid As Integer = 0
Public Property _LoginEmployeeunkid() As Integer 
Get 
Return mintLoginEmployeeunkid 
End Get 
Set(ByVal value As Integer) 
mintLoginEmployeeunkid = value 
End Set 
End Property 
 
Private mintVoidlogingemployeeunkid As Integer=0
Public Property _Voidloginemployeeunkid() As Integer 
Get 
Return mintVoidlogingemployeeunkid 
End Get 
Set(ByVal value As Integer) 
mintVoidlogingemployeeunkid = value 
End Set 
End Property 
'S.SANDEEP [28-May-2018] -- END 
    ''' <summary>
    ''' Purpose: Get Message from Class 
    ''' Modify By: Anjan
    ''' </summary>
    Public ReadOnly Property _Message() As String
        Get
            Return mstrMessage
        End Get
    End Property

    ''' <summary>
    ''' Purpose: Get or Set vacancyunkid
    ''' Modify By: Anjan
    ''' </summary>
    Public Property _VacancyUnkid() As Integer
        Get
            Return mintVacancyunkid
        End Get
        Set(ByVal value As Integer)
            mintVacancyunkid = value
            Call GetInterviewer_tran()
        End Set
    End Property
    ''' <summary>
    ''' Purpose: Get or Set vacancyunkid
    ''' Modify By: Anjan
    ''' </summary>
    Public Property _Interviewernkid() As Integer
        Get
            Return mintInterviewerunkid
        End Get
        Set(ByVal value As Integer)
            mintInterviewerunkid = value

        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set DataTable
    ''' Modify By: Anjan
    ''' </summary>
    Public Property _DataTable() As DataTable
        Get
            Return mdtTran
        End Get
        Set(ByVal value As DataTable)
            mdtTran = value
        End Set
    End Property
    '''' <summary>
    '''' Purpose: Get or Set vacancyunkid
    '''' Modify By: Anjan
    '''' </summary>
    'Public Property _Vacancyunkid() As Integer
    '    Get
    '        Return mintVacancyunkid
    '    End Get
    '    Set(ByVal value As Integer)
    '        mintVacancyunkid = Value
    '    End Set
    'End Property

    '''' <summary>
    '''' Purpose: Get or Set interviewtypeunkid
    '''' Modify By: Anjan
    '''' </summary>
    'Public Property _Interviewtypeunkid() As Integer
    '    Get
    '        Return mintInterviewtypeunkid
    '    End Get
    '    Set(ByVal value As Integer)
    '        mintInterviewtypeunkid = Value
    '    End Set
    'End Property

    '''' <summary>
    '''' Purpose: Get or Set interviewerunkid
    '''' Modify By: Anjan
    '''' </summary>
    'Public Property _Interviewerunkid() As Integer
    '    Get
    '        Return mintInterviewerunkid
    '    End Get
    '    Set(ByVal value As Integer)
    '        mintInterviewerunkid = Value
    '    End Set
    'End Property

    '''' <summary>
    '''' Purpose: Get or Set otherinterviewer_name
    '''' Modify By: Anjan
    '''' </summary>
    'Public Property _Otherinterviewer_Name() As String
    '    Get
    '        Return mstrOtherinterviewer_Name
    '    End Get
    '    Set(ByVal value As String)
    '        mstrOtherinterviewer_Name = Value
    '    End Set
    'End Property

    '''' <summary>
    '''' Purpose: Get or Set othercompany
    '''' Modify By: Anjan
    '''' </summary>
    'Public Property _Othercompany() As String
    '    Get
    '        Return mstrOthercompany
    '    End Get
    '    Set(ByVal value As String)
    '        mstrOthercompany = Value
    '    End Set
    'End Property

    '''' <summary>
    '''' Purpose: Get or Set otherdepartment
    '''' Modify By: Anjan
    '''' </summary>
    'Public Property _Otherdepartment() As String
    '    Get
    '        Return mstrOtherdepartment
    '    End Get
    '    Set(ByVal value As String)
    '        mstrOtherdepartment = Value
    '    End Set
    'End Property

    '''' <summary>
    '''' Purpose: Get or Set othercontact_no
    '''' Modify By: Anjan
    '''' </summary>
    'Public Property _Othercontact_No() As String
    '    Get
    '        Return mstrOthercontact_No
    '    End Get
    '    Set(ByVal value As String)
    '        mstrOthercontact_No = Value
    '    End Set
    'End Property

    '''' <summary>
    '''' Purpose: Get or Set userunkid
    '''' Modify By: Anjan
    '''' </summary>
    'Public Property _Userunkid() As Integer
    '    Get
    '        Return mintUserunkid
    '    End Get
    '    Set(ByVal value As Integer)
    '        mintUserunkid = Value
    '    End Set
    'End Property

    '''' <summary>
    '''' Purpose: Get or Set isvoid
    '''' Modify By: Anjan
    '''' </summary>
    'Public Property _Isvoid() As Boolean
    '    Get
    '        Return mblnIsvoid
    '    End Get
    '    Set(ByVal value As Boolean)
    '        mblnIsvoid = Value
    '    End Set
    'End Property

    '''' <summary>
    '''' Purpose: Get or Set voiduserunkid
    '''' Modify By: Anjan
    '''' </summary>
    'Public Property _Voiduserunkid() As Integer
    '    Get
    '        Return mintVoiduserunkid
    '    End Get
    '    Set(ByVal value As Integer)
    '        mintVoiduserunkid = Value
    '    End Set
    'End Property

    '''' <summary>
    '''' Purpose: Get or Set voiddatetime
    '''' Modify By: Anjan
    '''' </summary>
    'Public Property _Voiddatetime() As Date
    '    Get
    '        Return mdtVoiddatetime
    '    End Get
    '    Set(ByVal value As Date)
    '        mdtVoiddatetime = Value
    '    End Set
    'End Property

    '''' <summary>
    '''' Purpose: Get or Set voidreason
    '''' Modify By: Anjan
    '''' </summary>
    'Public Property _Voidreason() As String
    '    Get
    '        Return mstrVoidreason
    '    End Get
    '    Set(ByVal value As String)
    '        mstrVoidreason = Value
    '    End Set
    'End Property
    'Hemant (13 Sep 2019) -- Start
    'ISSUE/ENHANCEMENT(NMB) :  RECRUITMENTS UAT CHANGES(Point No - 4 : After publishing an external vacancy, system should send job alerts to all the applicants that have created profiles in the portal. Email content as below.)
    ''' <summary>
    ''' Purpose: Get or Set isvoid
    ''' Modify By: Hemant
    ''' </summary>
    Public Property _Isvoid() As Boolean
        Get
            Return mblnIsvoid
        End Get
        Set(ByVal value As Boolean)
            mblnIsvoid = value
        End Set
    End Property
    ''' <summary>
    ''' Purpose: Get or Set voiduserunkid
    ''' Modify By: Hemant
    ''' </summary>
    Public Property _Voiduserunkid() As Integer
        Get
            Return mintVoiduserunkid
        End Get
        Set(ByVal value As Integer)
            mintVoiduserunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiddatetime
    ''' Modify By: Hemant
    ''' </summary>
    Public Property _Voiddatetime() As Date
        Get
            Return mdtVoiddatetime
        End Get
        Set(ByVal value As Date)
            mdtVoiddatetime = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voidreason
    ''' Modify By: Hemant
    ''' </summary>
    Public Property _Voidreason() As String
        Get
            Return mstrVoidreason
        End Get
        Set(ByVal value As String)
            mstrVoidreason = value
        End Set
    End Property
    'Hemant (13 Sep 2019) -- End

#End Region
#Region " Constructor "
    Public Sub New()
        mdtTran = New DataTable("")
        Dim dCol As DataColumn


        Try

            dCol = New DataColumn("interviewertranunkid")
            dCol.DataType = System.Type.GetType("System.Int32")
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("vacancyunkid")
            dCol.DataType = System.Type.GetType("System.Int32")
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("interviewtypeunkid")
            dCol.DataType = System.Type.GetType("System.Int32")
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("interviewer_level")
            dCol.DataType = System.Type.GetType("System.Int32")
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("interviewerunkid")
            dCol.DataType = System.Type.GetType("System.Int32")
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("otherinterviewer_name")
            dCol.DataType = System.Type.GetType("System.String")
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("othercompany")
            dCol.DataType = System.Type.GetType("System.String")
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("otherdepartment")
            dCol.DataType = System.Type.GetType("System.String")
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("othercontact_no")
            dCol.DataType = System.Type.GetType("System.String")
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("userunkid")
            dCol.DataType = System.Type.GetType("System.Int32")
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("isvoid")
            dCol.DataType = System.Type.GetType("System.Boolean")
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("voiduserunkid")
            dCol.DataType = System.Type.GetType("System.Int32")
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("voiddatetime")
            dCol.DataType = System.Type.GetType("System.DateTime")
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("voidreason")
            dCol.DataType = System.Type.GetType("System.String")
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("GUID")
            dCol.DataType = System.Type.GetType("System.String")
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("AUD")
            dCol.DataType = System.Type.GetType("System.String")
            mdtTran.Columns.Add(dCol)



        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "New", mstrModuleName)
        End Try

    End Sub
#End Region


    ''' <summary>
    ''' Modify By: Anjan
    ''' </summary>
    ''' <purpose>To Get Transaction Records </purpose>
    Public Sub GetInterviewer_tran()
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim dRowID_Tran As DataRow
        Dim strErrorMessage As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "SELECT " & _
                      "rcinterviewer_tran.interviewertranunkid " & _
                      ", rcinterviewer_tran.vacancyunkid " & _
                      ", rcinterviewer_tran.interviewtypeunkid " & _
                      ", rcinterviewer_tran.interviewer_level " & _
                      ", rcinterviewer_tran.interviewerunkid " & _
                      ", rcinterviewer_tran.otherinterviewer_name " & _
                      ", rcinterviewer_tran.othercompany " & _
                      ", rcinterviewer_tran.otherdepartment " & _
                      ", rcinterviewer_tran.othercontact_no " & _
                      ", rcinterviewer_tran.userunkid " & _
                      ", rcinterviewer_tran.isvoid " & _
                      ", rcinterviewer_tran.voiduserunkid " & _
                      ", rcinterviewer_tran.voiddatetime " & _
                      ", rcinterviewer_tran.voidreason " & _
                      ", '' AUD " & _
                   "FROM rcinterviewer_tran " & _
                   "WHERE vacancyunkid = @vacancyunkid "
            'Hemant (13 Sep 2019) -- Start
            'ISSUE/ENHANCEMENT(NMB) :  RECRUITMENTS UAT CHANGES(Point No - 4 : After publishing an external vacancy, system should send job alerts to all the applicants that have created profiles in the portal. Email content as below.)
            strQ &= "AND ISNULL(isvoid, 0) = 0 "
            'Hemant (13 Sep 2019) -- End

            objDataOperation.AddParameter("@vacancyunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVacancyunkid.ToString)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            mdtTran.Clear()

            For i As Integer = 0 To dsList.Tables(0).Rows.Count - 1
                With dsList.Tables("List").Rows(i)
                    dRowID_Tran = mdtTran.NewRow
                    dRowID_Tran.Item("interviewertranunkid") = .Item("interviewertranunkid").ToString
                    dRowID_Tran.Item("vacancyunkid") = .Item("vacancyunkid").ToString
                    dRowID_Tran.Item("interviewtypeunkid") = .Item("interviewtypeunkid").ToString
                    dRowID_Tran.Item("interviewer_level") = .Item("interviewer_level").ToString
                    dRowID_Tran.Item("interviewerunkid") = .Item("interviewerunkid").ToString
                    dRowID_Tran.Item("otherinterviewer_name") = .Item("otherinterviewer_name").ToString
                    dRowID_Tran.Item("othercompany") = .Item("othercompany").ToString
                    dRowID_Tran.Item("otherdepartment") = .Item("otherdepartment").ToString
                    dRowID_Tran.Item("othercontact_no") = .Item("othercontact_no").ToString
                    dRowID_Tran.Item("userunkid") = .Item("userunkid").ToString
                    dRowID_Tran.Item("isvoid") = .Item("isvoid").ToString
                    dRowID_Tran.Item("voiduserunkid") = .Item("voiduserunkid").ToString
                    dRowID_Tran.Item("voiddatetime") = .Item("voiddatetime")
                    dRowID_Tran.Item("voidreason") = .Item("voidreason").ToString
                    dRowID_Tran.Item("AUD") = .Item("AUD")
                    mdtTran.Rows.Add(dRowID_Tran)
                End With

            Next

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetData; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Sub


    '''' <summary>
    '''' Modify By: Anjan
    '''' </summary>
    '''' <purpose> Assign all Property variable </purpose>
    'Public Function GetList(ByVal strTableName As String, Optional ByVal blnOnlyActive As Boolean = True) As DataSet
    '    Dim dsList As DataSet = Nothing
    '    Dim strQ As String = ""
    '    Dim exForce As Exception

    '    objDataOperation = New clsDataOperation

    '    Try
    '        StrQ = "SELECT " & _
    '          "  interviewertranunkid " & _
    '          ", vacancyunkid " & _
    '          ", interviewtypeunkid " & _
    '          ", interviewerunkid " & _
    '          ", otherinterviewer_name " & _
    '          ", othercompany " & _
    '          ", otherdepartment " & _
    '          ", othercontact_no " & _
    '          ", userunkid " & _
    '          ", isvoid " & _
    '          ", voiduserunkid " & _
    '          ", voiddatetime " & _
    '          ", voidreason " & _
    '         "FROM rcinterviewer_tran "

    '        If blnOnlyActive Then
    '            strQ &= " WHERE isactive = 1 "
    '        End If

    '        dsList = objDataOperation.ExecQuery(strQ, strTableName)

    '        If objDataOperation.ErrorMessage <> "" Then
    '            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
    '            Throw exForce
    '        End If

    '    Catch ex As Exception
    '        Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
    '    Finally
    '        exForce = Nothing
    '        If dsList IsNot Nothing Then dsList.Dispose()
    '        objDataOperation = Nothing
    '    End Try
    '    Return dsList
    'End Function


    '''' <summary>
    '''' Modify By: Anjan
    '''' </summary>
    '''' <returns>Boolean</returns>
    '''' <purpose> INSERT INTO Database Table (rcinterviewer_tran) </purpose>
    'Public Function Insert() As Boolean
    '    'If isExist(mstrName) Then
    '    '    mstrMessage = "<Message>"
    '    '    Return False
    '    'End If

    '    Dim dsList As DataSet = Nothing
    '    Dim strQ As String = ""
    '    Dim exForce As Exception

    '    objDataOperation = New clsDataOperation

    '    Try
    '        objDataOperation.AddParameter("@vacancyunkid", SqlDbType.int, eZeeDataType.INT_SIZE, mintvacancyunkid.ToString)
    '        objDataOperation.AddParameter("@interviewtypeunkid", SqlDbType.int, eZeeDataType.INT_SIZE, mintinterviewtypeunkid.ToString)
    '        objDataOperation.AddParameter("@interviewerunkid", SqlDbType.int, eZeeDataType.INT_SIZE, mintinterviewerunkid.ToString)
    '        objDataOperation.AddParameter("@otherinterviewer_name", SqlDbType.nvarchar, eZeeDataType.NAME_SIZE, mstrotherinterviewer_name.ToString)
    '        objDataOperation.AddParameter("@othercompany", SqlDbType.nvarchar, eZeeDataType.NAME_SIZE, mstrothercompany.ToString)
    '        objDataOperation.AddParameter("@otherdepartment", SqlDbType.nvarchar, eZeeDataType.NAME_SIZE, mstrotherdepartment.ToString)
    '        objDataOperation.AddParameter("@othercontact_no", SqlDbType.nvarchar, eZeeDataType.NAME_SIZE, mstrothercontact_no.ToString)
    '        objDataOperation.AddParameter("@userunkid", SqlDbType.int, eZeeDataType.INT_SIZE, mintuserunkid.ToString)
    '        objDataOperation.AddParameter("@isvoid", SqlDbType.bit, eZeeDataType.BIT_SIZE, mblnisvoid.ToString)
    '        objDataOperation.AddParameter("@voiduserunkid", SqlDbType.int, eZeeDataType.INT_SIZE, mintvoiduserunkid.ToString)
    '        If mdtVoiddatetime = Nothing Then
    '            objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
    '        Else
    '            objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime)
    '        End If

    '        objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)

    '        StrQ = "INSERT INTO rcinterviewer_tran ( " & _
    '          "  vacancyunkid " & _
    '          ", interviewtypeunkid " & _
    '          ", interviewerunkid " & _
    '          ", otherinterviewer_name " & _
    '          ", othercompany " & _
    '          ", otherdepartment " & _
    '          ", othercontact_no " & _
    '          ", userunkid " & _
    '          ", isvoid " & _
    '          ", voiduserunkid " & _
    '          ", voiddatetime " & _
    '          ", voidreason" & _
    '        ") VALUES (" & _
    '          "  @vacancyunkid " & _
    '          ", @interviewtypeunkid " & _
    '          ", @interviewerunkid " & _
    '          ", @otherinterviewer_name " & _
    '          ", @othercompany " & _
    '          ", @otherdepartment " & _
    '          ", @othercontact_no " & _
    '          ", @userunkid " & _
    '          ", @isvoid " & _
    '          ", @voiduserunkid " & _
    '          ", @voiddatetime " & _
    '          ", @voidreason" & _
    '        "); SELECT @@identity"

    '        dsList = objDataOperation.ExecQuery(strQ, "List")

    '        If objDataOperation.ErrorMessage <> "" Then
    '            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
    '            Throw exForce
    '        End If

    '        mintInterviewerTranUnkId = dsList.Tables(0).Rows(0).Item(0)

    '        Return True
    '    Catch ex As Exception
    '        Throw New Exception(ex.Message & "; Procedure Name: Insert; Module Name: " & mstrModuleName)
    '        Return False
    '    Finally
    '        exForce = Nothing
    '        If dsList IsNot Nothing Then dsList.Dispose()
    '        objDataOperation = Nothing
    '    End Try
    'End Function

    '''' <summary>
    '''' Modify By: Anjan
    '''' </summary>
    '''' <returns>Boolean</returns>
    '''' <purpose> Update Database Table (rcinterviewer_tran) </purpose>
    'Public Function Update() As Boolean
    '    'If isExist(mstrName, mintInterviewertranunkid) Then
    '    '    mstrMessage = "<Message>"
    '    '    Return False
    '    'End If

    '    Dim dsList As DataSet = Nothing
    '    Dim strQ As String = ""
    '    Dim exForce As Exception

    '    objDataOperation = New clsDataOperation

    '    Try
    '        objDataOperation.AddParameter("@interviewertranunkid", SqlDbType.int, eZeeDataType.INT_SIZE, mintinterviewertranunkid.ToString)
    '        objDataOperation.AddParameter("@vacancyunkid", SqlDbType.int, eZeeDataType.INT_SIZE, mintvacancyunkid.ToString)
    '        objDataOperation.AddParameter("@interviewtypeunkid", SqlDbType.int, eZeeDataType.INT_SIZE, mintinterviewtypeunkid.ToString)
    '        objDataOperation.AddParameter("@interviewerunkid", SqlDbType.int, eZeeDataType.INT_SIZE, mintinterviewerunkid.ToString)
    '        objDataOperation.AddParameter("@otherinterviewer_name", SqlDbType.nvarchar, eZeeDataType.NAME_SIZE, mstrotherinterviewer_name.ToString)
    '        objDataOperation.AddParameter("@othercompany", SqlDbType.nvarchar, eZeeDataType.NAME_SIZE, mstrothercompany.ToString)
    '        objDataOperation.AddParameter("@otherdepartment", SqlDbType.nvarchar, eZeeDataType.NAME_SIZE, mstrotherdepartment.ToString)
    '        objDataOperation.AddParameter("@othercontact_no", SqlDbType.nvarchar, eZeeDataType.NAME_SIZE, mstrothercontact_no.ToString)
    '        objDataOperation.AddParameter("@userunkid", SqlDbType.int, eZeeDataType.INT_SIZE, mintuserunkid.ToString)
    '        objDataOperation.AddParameter("@isvoid", SqlDbType.bit, eZeeDataType.BIT_SIZE, mblnisvoid.ToString)
    '        objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)

    '        If mdtVoiddatetime = Nothing Then
    '            objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
    '        Else
    '            objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime)
    '        End If

    '        objDataOperation.AddParameter("@voidreason", SqlDbType.nvarchar, eZeeDataType.NAME_SIZE, mstrvoidreason.ToString)

    '        StrQ = "UPDATE rcinterviewer_tran SET " & _
    '          "  vacancyunkid = @vacancyunkid" & _
    '          ", interviewtypeunkid = @interviewtypeunkid" & _
    '          ", interviewerunkid = @interviewerunkid" & _
    '          ", otherinterviewer_name = @otherinterviewer_name" & _
    '          ", othercompany = @othercompany" & _
    '          ", otherdepartment = @otherdepartment" & _
    '          ", othercontact_no = @othercontact_no" & _
    '          ", userunkid = @userunkid" & _
    '          ", isvoid = @isvoid" & _
    '          ", voiduserunkid = @voiduserunkid" & _
    '          ", voiddatetime = @voiddatetime" & _
    '          ", voidreason = @voidreason " & _
    '        "WHERE interviewertranunkid = @interviewertranunkid "

    '        Call objDataOperation.ExecNonQuery(strQ)

    '        If objDataOperation.ErrorMessage <> "" Then
    '            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
    '            Throw exForce
    '        End If

    '        Return True
    '    Catch ex As Exception
    '        Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
    '        Return False
    '    Finally
    '        exForce = Nothing
    '        If dsList IsNot Nothing Then dsList.Dispose()
    '        objDataOperation = Nothing
    '    End Try
    'End Function

    '''' <summary>
    '''' Modify By: Anjan
    '''' </summary>
    '''' <returns>Boolean</returns>
    '''' <purpose> Delete Database Table (rcinterviewer_tran) </purpose>
    'Public Function Delete(ByVal intUnkid As Integer) As Boolean
    '    If isUsed(intUnkid) Then
    '        mstrMessage = "<Message>"
    '        Return False
    '    End If

    '    Dim dsList As DataSet = Nothing
    '    Dim strQ As String = ""
    '    Dim exForce As Exception

    '    objDataOperation = New clsDataOperation

    '    Try
    '        StrQ = "DELETE FROM rcinterviewer_tran " & _
    '        "WHERE interviewertranunkid = @interviewertranunkid "

    '        objDataOperation.AddParameter("@interviewertranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

    '        Call objDataOperation.ExecNonQuery(strQ)

    '        If objDataOperation.ErrorMessage <> "" Then
    '            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
    '            Throw exForce
    '        End If

    '        Return True
    '    Catch ex As Exception
    '        Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
    '        Return False
    '    Finally
    '        exForce = Nothing
    '        If dsList IsNot Nothing Then dsList.Dispose()
    '        objDataOperation = Nothing
    '    End Try
    'End Function

    '''' <summary>
    '''' Modify By: Anjan
    '''' </summary>
    '''' <purpose> Assign all Property variable </purpose>
    'Public Function isUsed(ByVal intUnkid As Integer) As Boolean
    '    Dim dsList As DataSet = Nothing
    '    Dim strQ As String = ""
    '    Dim exForce As Exception

    '    objDataOperation = New clsDataOperation

    '    Try
    '        StrQ = "<Query>"

    '        objDataOperation.AddParameter("@interviewertranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

    '        dsList = objDataOperation.ExecQuery(strQ, "List")

    '        If objDataOperation.ErrorMessage <> "" Then
    '            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
    '            Throw exForce
    '        End If

    '        Return dsList.tables(0).rows.count > 0
    '    Catch ex As Exception
    '        Throw New Exception(ex.Message & "; Procedure Name: isUsed; Module Name: " & mstrModuleName)
    '    Finally
    '        exForce = Nothing
    '        If dsList IsNot Nothing Then dsList.Dispose()
    '        objDataOperation = Nothing
    '    End Try
    'End Function

    '''' <summary>
    '''' Modify By: Anjan
    '''' </summary>
    '''' <purpose> Assign all Property variable </purpose>
    'Public Function isExist(ByVal strCode As String, ByVal strName As String, Optional ByVal intUnkid As Integer = -1) As Boolean
    '    Dim dsList As DataSet = Nothing
    '    Dim strQ As String = ""
    '    Dim exForce As Exception

    '    objDataOperation = New clsDataOperation

    '    Try
    '        StrQ = "SELECT " & _
    '          "  interviewertranunkid " & _
    '          ", vacancyunkid " & _
    '          ", interviewtypeunkid " & _
    '          ", interviewerunkid " & _
    '          ", otherinterviewer_name " & _
    '          ", othercompany " & _
    '          ", otherdepartment " & _
    '          ", othercontact_no " & _
    '          ", userunkid " & _
    '          ", isvoid " & _
    '          ", voiduserunkid " & _
    '          ", voiddatetime " & _
    '          ", voidreason " & _
    '         "FROM rcinterviewer_tran " & _
    '         "WHERE name = @name " & _


    '        If intUnkid > 0 Then
    '            strQ &= " AND interviewertranunkid <> @interviewertranunkid"
    '        End If


    '        objDataOperation.AddParameter("@name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strName)
    '        objDataOperation.AddParameter("@interviewertranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

    '        dsList = objDataOperation.ExecQuery(strQ, "List")

    '        If objDataOperation.ErrorMessage <> "" Then
    '            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
    '            Throw exForce
    '        End If

    '        Return dsList.tables(0).rows.count > 0
    '    Catch ex As Exception
    '        Throw New Exception(ex.Message & "; Procedure Name: isExist; Module Name: " & mstrModuleName)
    '    Finally
    '        exForce = Nothing
    '        If dsList IsNot Nothing Then dsList.Dispose()
    '        objDataOperation = Nothing
    '    End Try
    'End Function

    Public Function InsertUpdateDelete_InterviewerTran(ByVal dtCurrentDateAndTime As Date, Optional ByVal xDataOp As clsDataOperation = Nothing) As Boolean
        'Hemant (13 Sep 2019) -- [dtCurrentDateAndTime,xDataOp]
        Dim i As Integer
        Dim strQ As String = ""
        Dim strErrorMessage As String = ""
        Dim exForce As Exception
        Try

            'Hemant (13 Sep 2019) -- Start
            'ISSUE/ENHANCEMENT(NMB) :  RECRUITMENTS UAT CHANGES(Point No - 4 : After publishing an external vacancy, system should send job alerts to all the applicants that have created profiles in the portal. Email content as below.)
            'objDataOperation = New clsDataOperation
            'objDataOperation.BindTransaction()
            If xDataOp IsNot Nothing Then
                objDataOperation = xDataOp
            Else
            objDataOperation = New clsDataOperation
            objDataOperation.BindTransaction()
            End If
            'Hemant (13 Sep 2019) -- End

            For i = 0 To mdtTran.Rows.Count - 1
                With mdtTran.Rows(i)
                    objDataOperation.ClearParameters()
                    If Not IsDBNull(.Item("AUD")) Then
                        Select Case .Item("AUD")
                            Case "A"
                                strQ = "INSERT INTO rcinterviewer_tran ( " & _
                                                 "  vacancyunkid " & _
                                                 ", interviewtypeunkid " & _
                                                 ", interviewer_level " & _
                                                 ", interviewerunkid " & _
                                                 ", otherinterviewer_name " & _
                                                 ", othercompany " & _
                                                 ", otherdepartment " & _
                                                 ", othercontact_no " & _
                                                 ", userunkid " & _
                                                 ", isvoid " & _
                                                 ", voiduserunkid " & _
                                                 ", voiddatetime " & _
                                                 ", voidreason" & _
                                               ") VALUES (" & _
                                                 "  @vacancyunkid " & _
                                                 ", @interviewtypeunkid " & _
                                                 ", @interviewer_level " & _
                                                 ", @interviewerunkid " & _
                                                 ", @otherinterviewer_name " & _
                                                 ", @othercompany " & _
                                                 ", @otherdepartment " & _
                                                 ", @othercontact_no " & _
                                                 ", @userunkid " & _
                                                 ", @isvoid " & _
                                                 ", @voiduserunkid " & _
                                                 ", @voiddatetime " & _
                                                 ", @voidreason" & _
                                               "); SELECT @@identity"



                                objDataOperation.AddParameter("@vacancyunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVacancyunkid)
                                objDataOperation.AddParameter("@interviewtypeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("interviewtypeunkid").ToString)
                                objDataOperation.AddParameter("@interviewer_level", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("interviewer_level").ToString)
                                objDataOperation.AddParameter("@interviewerunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("interviewerunkid").ToString)
                                objDataOperation.AddParameter("@otherinterviewer_name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, .Item("otherinterviewer_name").ToString)
                                objDataOperation.AddParameter("@othercompany", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, .Item("othercompany").ToString)
                                objDataOperation.AddParameter("@otherdepartment", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, .Item("otherdepartment").ToString)
                                objDataOperation.AddParameter("@othercontact_no", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, .Item("othercontact_no").ToString)
                                objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("userunkid").ToString)
                                objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, .Item("isvoid"))
                                objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("voiduserunkid").ToString)
                                If .Item("voiddatetime").ToString = Nothing Then
                                    objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
                                Else
                                    objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, .Item("voiddatetime").ToString)
                                End If

                                objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, .Item("voidreason").ToString)


                                'Anjan (12 Oct 2011)-Start
                                'ENHANCEMENT : AUDIT TRAIL MAINTENANCE.
                                'objDataOperation.ExecNonQuery(strQ)
                                Dim dsList As DataSet = objDataOperation.ExecQuery(strQ, "list")
                                'Anjan (12 Oct 2011)-End 


                                If objDataOperation.ErrorMessage <> "" Then
                                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                    Throw exForce
                                End If


                                'Anjan (12 Oct 2011)-Start
                                'ENHANCEMENT : AUDIT TRAIL MAINTENANCE.
                                mintInterviewertranunkid = dsList.Tables(0).Rows(0)(0)

                                If .Item("vacancyunkid") > 0 Then
'S.SANDEEP [28-May-2018] -- START
'ISSUE/ENHANCEMENT : {Audit Trails} 
Dim objCommonATLog AS New clsCommonATLog 
objCommonATLog._FormName = mstrFormName 
objCommonATLog._LoginEmployeeUnkid = mintLoginEmployeeunkid 
                                    objCommonATLog._ClientIP = mstrClientIP
                                    objCommonATLog._HostName = mstrHostName
                                    objCommonATLog._FromWeb = mblnIsWeb
                                    objCommonATLog._AuditUserId = mintAuditUserId
objCommonATLog._CompanyUnkid = mintCompanyUnkid
                                    objCommonATLog._AuditDate = mdtAuditDate
                                    'S.SANDEEP [28-May-2018] -- END

                                    If objCommonATLog.Insert_TranAtLog(objDataOperation, "rcvacancy_master", "vacancyunkid", mintVacancyunkid, "rcinterviewer_tran", "interviewertranunkid", mintInterviewertranunkid, 2, 1) = False Then
                                        Return False
                                    End If

                                    'S.SANDEEP [28-May-2018] -- START
                                    'ISSUE/ENHANCEMENT : {Audit Trails} 
                                    objCommonATLog = Nothing
                                    'S.SANDEEP [28-May-2018] -- END

                                Else
                                    'S.SANDEEP [28-May-2018] -- START
                                    'ISSUE/ENHANCEMENT : {Audit Trails} 
                                    Dim objCommonATLog As New clsCommonATLog
                                    objCommonATLog._FormName = mstrFormName
                                    objCommonATLog._LoginEmployeeUnkid = mintLoginEmployeeunkid
                                    objCommonATLog._ClientIP = mstrClientIP
                                    objCommonATLog._HostName = mstrHostName
                                    objCommonATLog._FromWeb = mblnIsWeb
                                    objCommonATLog._AuditUserId = mintAuditUserId
objCommonATLog._CompanyUnkid = mintCompanyUnkid
                                    objCommonATLog._AuditDate = mdtAuditDate
                                    'S.SANDEEP [28-May-2018] -- END

                                    If objCommonATLog.Insert_TranAtLog(objDataOperation, "rcvacancy_master", "vacancyunkid", mintVacancyunkid, "rcinterviewer_tran", "interviewertranunkid", mintInterviewertranunkid, 1, 1) = False Then
                                        Return False
                                    End If

                                    'S.SANDEEP [28-May-2018] -- START
                                    'ISSUE/ENHANCEMENT : {Audit Trails} 
                                    objCommonATLog = Nothing
                                    'S.SANDEEP [28-May-2018] -- END

                                End If
                                'Anjan (12 Oct 2011)-End 



                            Case "U"
                                strQ = "UPDATE rcinterviewer_tran SET " & _
                                              "  vacancyunkid = @vacancyunkid" & _
                                              ", interviewtypeunkid = @interviewtypeunkid" & _
                                              ", interviewer_level=@interviewer_level " & _
                                              ", interviewerunkid = @interviewerunkid" & _
                                              ", otherinterviewer_name = @otherinterviewer_name" & _
                                              ", othercompany = @othercompany" & _
                                              ", otherdepartment = @otherdepartment" & _
                                              ", othercontact_no = @othercontact_no" & _
                                              ", userunkid = @userunkid" & _
                                       " WHERE interviewertranunkid = @interviewertranunkid "

                                objDataOperation.AddParameter("@interviewertranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("interviewertranunkid").ToString)
                                objDataOperation.AddParameter("@vacancyunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVacancyunkid)
                                objDataOperation.AddParameter("@interviewtypeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("interviewtypeunkid").ToString)
                                objDataOperation.AddParameter("@interviewer_level", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("interviewer_level").ToString)
                                objDataOperation.AddParameter("@interviewerunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("interviewerunkid").ToString)
                                objDataOperation.AddParameter("@otherinterviewer_name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, .Item("otherinterviewer_name").ToString)
                                objDataOperation.AddParameter("@othercompany", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, .Item("othercompany").ToString)
                                objDataOperation.AddParameter("@otherdepartment", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, .Item("otherdepartment").ToString)
                                objDataOperation.AddParameter("@othercontact_no", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, .Item("othercontact_no").ToString)
                                objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("userunkid").ToString)

                                objDataOperation.ExecNonQuery(strQ)

                                If objDataOperation.ErrorMessage <> "" Then
                                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                    Throw exForce
                                End If


                                'S.SANDEEP [28-May-2018] -- START
                                'ISSUE/ENHANCEMENT : {Audit Trails} 
                                Dim objCommonATLog As New clsCommonATLog
                                objCommonATLog._FormName = mstrFormName
                                objCommonATLog._LoginEmployeeUnkid = mintLoginEmployeeunkid
                                objCommonATLog._ClientIP = mstrClientIP
                                objCommonATLog._HostName = mstrHostName
                                objCommonATLog._FromWeb = mblnIsWeb
                                objCommonATLog._AuditUserId = mintAuditUserId
objCommonATLog._CompanyUnkid = mintCompanyUnkid
                                objCommonATLog._AuditDate = mdtAuditDate
                                'S.SANDEEP [28-May-2018] -- END

                                If objCommonATLog.Insert_TranAtLog(objDataOperation, "rcvacancy_master", "vacancyunkid", .Item("vacancyunkid").ToString, "rcinterviewer_tran", "interviewertranunkid", .Item("interviewertranunkid").ToString, 2, 2) = False Then
                                    Return False
                                End If

                                'S.SANDEEP [28-May-2018] -- START
                                'ISSUE/ENHANCEMENT : {Audit Trails} 
                                objCommonATLog = Nothing
                                'S.SANDEEP [28-May-2018] -- END




                            Case "D"

                                If .Item("interviewertranunkid") > 0 Then

                                    'S.SANDEEP [28-May-2018] -- START
                                    'ISSUE/ENHANCEMENT : {Audit Trails} 
                                    Dim objCommonATLog As New clsCommonATLog
                                    objCommonATLog._FormName = mstrFormName
                                    objCommonATLog._LoginEmployeeUnkid = mintLoginEmployeeunkid
                                    objCommonATLog._ClientIP = mstrClientIP
                                    objCommonATLog._HostName = mstrHostName
                                    objCommonATLog._FromWeb = mblnIsWeb
                                    objCommonATLog._AuditUserId = mintAuditUserId
objCommonATLog._CompanyUnkid = mintCompanyUnkid
                                    objCommonATLog._AuditDate = mdtAuditDate
                                    'S.SANDEEP [28-May-2018] -- END

                                    If objCommonATLog.Insert_TranAtLog(objDataOperation, "rcvacancy_master", "vacancyunkid", .Item("vacancyunkid").ToString, "rcinterviewer_tran", "interviewertranunkid", .Item("interviewertranunkid").ToString, 2, 3) = False Then
                                        Return False
                                    End If
                                   
                                    'Hemant (13 Sep 2019) -- Start
                                    'ISSUE/ENHANCEMENT(NMB) :  RECRUITMENTS UAT CHANGES(Point No - 4 : After publishing an external vacancy, system should send job alerts to all the applicants that have created profiles in the portal. Email content as below.)
                                    'strQ = "DELETE FROM rcinterviewer_tran " & _
                                    '        "WHERE interviewertranunkid = @interviewertranunkid "
                                    strQ = "UPDATE rcinterviewer_tran SET " & _
                                              "  isvoid = 1 " & _
                                              ", voiduserunkid = @voiduserunkid" & _
                                              ", voiddatetime = @voiddatetime" & _
                                              ", voidreason = @voidreason " & _
                                        "WHERE interviewertranunkid = @interviewertranunkid "
                                    'Hemant (13 Sep 2019) -- End

                                objDataOperation.AddParameter("@interviewertranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("interviewertranunkid").ToString)
                                    'Hemant (13 Sep 2019) -- Start
                                    'ISSUE/ENHANCEMENT(NMB) :  RECRUITMENTS UAT CHANGES(Point No - 4 : After publishing an external vacancy, system should send job alerts to all the applicants that have created profiles in the portal. Email content as below.)
                                    objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid)
                                    objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, dtCurrentDateAndTime.ToString)
                                    objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)
                                    'Hemant (13 Sep 2019) -- End

                                Call objDataOperation.ExecNonQuery(strQ)

                                If objDataOperation.ErrorMessage <> "" Then
                                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                    Throw exForce
                                End If
                                End If
                                'Anjan (12 Oct 2011)-End 
                        End Select
                    End If
                End With
            Next
            'Hemant (13 Sep 2019) -- Start
            'ISSUE/ENHANCEMENT(NMB) :  RECRUITMENTS UAT CHANGES(Point No - 4 : After publishing an external vacancy, system should send job alerts to all the applicants that have created profiles in the portal. Email content as below.)
            'objDataOperation.ReleaseTransaction(True)
            If xDataOp Is Nothing Then objDataOperation.ReleaseTransaction(True)
            'Hemant (13 Sep 2019) -- End
            Return True
        Catch ex As Exception
            'Hemant (13 Sep 2019) -- Start
            'ISSUE/ENHANCEMENT(NMB) :  RECRUITMENTS UAT CHANGES(Point No - 4 : After publishing an external vacancy, system should send job alerts to all the applicants that have created profiles in the portal. Email content as below.)
            'objDataOperation.ReleaseTransaction(False)
            If xDataOp Is Nothing Then objDataOperation.ReleaseTransaction(False)
            'Hemant (13 Sep 2019) -- End
            DisplayError.Show("-1", ex.Message, "InsertUpdateDelete_InterviewerTran", mstrModuleName)
        End Try

    End Function

    ''' <summary>
    ''' Modify By: Anjan
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Delete Database Table (rcinterviewer_tran) </purpose>
    Public Function VoidAll(ByVal intUnkid As Integer, ByVal blnIsVoid As Boolean, ByVal intVoiduserunkid As Integer, ByVal dtVoidDateTime As DateTime, ByVal strVoidReason As String) As Boolean
        'If isUsed(intUnkid) Then
        '    mstrMessage = "<Message>"
        '    Return False
        'End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try



            'S.SANDEEP [28-May-2018] -- START
            'ISSUE/ENHANCEMENT : {Audit Trails} 
            Dim objCommonATLog As New clsCommonATLog
            objCommonATLog._FormName = mstrFormName
            objCommonATLog._LoginEmployeeUnkid = mintLoginEmployeeunkid
            objCommonATLog._ClientIP = mstrClientIP
            objCommonATLog._HostName = mstrHostName
objCommonATLog._FromWeb = mblnIsWeb 
objCommonATLog._AuditUserId = mintAuditUserId 
objCommonATLog._CompanyUnkid = mintCompanyUnkid
objCommonATLog._AuditDate = mdtAuditDate 
'S.SANDEEP [28-May-2018] -- END

            If objCommonATLog.VoidAtTranAtLog(objDataOperation, "rcvacancy_master", "vacancyunkid", intUnkid, "rcinterviewer_tran", "interviewertranunkid", 3, 3) = False Then
                Return False
            End If

'S.SANDEEP [28-May-2018] -- START
'ISSUE/ENHANCEMENT : {Audit Trails} 
objCommonATLog = Nothing 
'S.SANDEEP [28-May-2018] -- END



            strQ = " UPDATE rcinterviewer_tran SET " & _
                  "  isvoid = @isvoid " & _
                  ", voiduserunkid = @voiduserunkid " & _
                  ", voiddatetime = @voiddatetime  " & _
                  ", voidreason= @voidreason " & _
            "WHERE vacancyunkid = @vacancyunkid "

            objDataOperation.AddParameter("@vacancyunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)
            objDataOperation.AddParameter("@voiddatetime", SqlDbType.SmallDateTime, eZeeDataType.DATETIME_SIZE, dtVoidDateTime)
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strVoidReason.ToString)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, blnIsVoid)

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If


            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    Public Function getComboList(Optional ByVal intVacancyId As Integer = -1, _
                                 Optional ByVal blnFlag As Boolean = False, _
                                 Optional ByVal strList As String = "List", _
                                 Optional ByVal isFinalAnalysis As Boolean = False, _
                                 Optional ByVal isAllTrainer As Boolean = False) As DataSet
        Dim strQ As String = ""
        Dim strErrorMessage As String = ""
        Dim exForce As Exception
        Dim dsList As New DataSet
        objDataOperation = New clsDataOperation
        'Sandeep [ 02 Oct 2010 ] -- Start
        Dim intCnt As Integer = 0
        'Sandeep [ 02 Oct 2010 ] -- End
        Try
            'Sandeep [ 02 Oct 2010 ] -- Start
            strQ = "SELECT interviewerunkid FROM rcinterviewer_tran WHERE ISNULL(isvoid ,0) = 0 AND rcinterviewer_tran.vacancyunkid = @VacancyId"
            objDataOperation.AddParameter("@VacancyId", SqlDbType.Int, eZeeDataType.INT_SIZE, intVacancyId)
            intCnt = objDataOperation.RecordCount(strQ)
            'Sandeep [ 02 Oct 2010 ] -- End



            If blnFlag = True Then
                strQ = "SELECT 0 AS Id,@Select AS NAME ,0 AS interviewertranunkid UNION "
            End If
            strQ &= "SELECT  interviewerunkid AS Id " & _
                            ",CASE WHEN interviewerunkid = -1 " & _
                                    "THEN rcinterviewer_tran.otherinterviewer_name " & _
                                    "WHEN interviewerunkid > 0 " & _
                                    "THEN ISNULL(hremployee_master.firstname ,'') + ' ' " & _
                                        "+ ISNULL(hremployee_master.othername ,'') + ' ' " & _
                                        "+ ISNULL(hremployee_master.surname ,'') " & _
                            "END AS NAME " & _
                            ",interviewertranunkid " & _
                    "FROM rcinterviewer_tran " & _
                    "LEFT JOIN hremployee_master ON hremployee_master.employeeunkid = rcinterviewer_tran.interviewerunkid " & _
                    "WHERE ISNULL(isvoid ,0) = 0 " & _
                    "AND rcinterviewer_tran.vacancyunkid = @VacancyId "

            'Sandeep [ 02 Oct 2010 ] -- Start
            'If isAllTrainer = False Then
            '    If isFinalAnalysis = False Then
            '        strQ &= "AND rcinterviewer_tran.interviewer_level NOT IN (SELECT MAX(rcinterviewer_tran.interviewer_level) FROM rcinterviewer_tran WHERE rcinterviewer_tran.vacancyunkid = @VacancyId AND ISNULL(rcinterviewer_tran.isvoid, 0) = 0 ) "
            '    Else
            '        strQ &= "AND rcinterviewer_tran.interviewer_level IN (SELECT MAX(rcinterviewer_tran.interviewer_level) FROM rcinterviewer_tran WHERE rcinterviewer_tran.vacancyunkid = @VacancyId AND ISNULL(rcinterviewer_tran.isvoid, 0) = 0 ) "
            '    End If
            'End If

            If intCnt > 1 Then
                If isAllTrainer = False Then
                    If isFinalAnalysis = False Then
                        strQ &= "AND rcinterviewer_tran.interviewer_level NOT IN (SELECT MAX(rcinterviewer_tran.interviewer_level) FROM rcinterviewer_tran WHERE rcinterviewer_tran.vacancyunkid = @VacancyId AND ISNULL(rcinterviewer_tran.isvoid, 0) = 0 ) "
                    Else
                        strQ &= "AND rcinterviewer_tran.interviewer_level IN (SELECT MAX(rcinterviewer_tran.interviewer_level) FROM rcinterviewer_tran WHERE rcinterviewer_tran.vacancyunkid = @VacancyId AND ISNULL(rcinterviewer_tran.isvoid, 0) = 0 ) "
                    End If
                End If
            End If
            'Sandeep [ 02 Oct 2010 ] -- End

            strQ &= "ORDER BY interviewertranunkid "

            objDataOperation.AddParameter("@Select", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 1, "Select"))

            dsList = objDataOperation.ExecQuery(strQ, strList)
            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "getComboList", mstrModuleName)
            Return Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Anjan
    ''' </summary>
    ''' <purpose> To get the data of Interviewer from Table (rcinterviewer_tran) </purpose>
    Public Function GetInterviewerData(ByVal intInterviewertranId As Integer) As DataSet
        Dim strQ As String = ""
        Dim dsList As DataSet
        Dim exForce As Exception

        objDataOperation = New clsDataOperation
        Try
            strQ = "SELECT " & _
                      "rcinterviewer_tran.interviewertranunkid " & _
                      ", rcinterviewer_tran.vacancyunkid " & _
                      ", rcinterviewer_tran.interviewtypeunkid " & _
                      ", rcinterviewer_tran.interviewer_level " & _
                      ", rcinterviewer_tran.interviewerunkid " & _
                      ", rcinterviewer_tran.otherinterviewer_name " & _
                      ", rcinterviewer_tran.othercompany " & _
                      ", rcinterviewer_tran.otherdepartment " & _
                      ", rcinterviewer_tran.othercontact_no " & _
                      ", rcinterviewer_tran.userunkid " & _
                      ", rcinterviewer_tran.isvoid " & _
                      ", rcinterviewer_tran.voiduserunkid " & _
                      ", rcinterviewer_tran.voiddatetime " & _
                      ", rcinterviewer_tran.voidreason " & _
                   "FROM rcinterviewer_tran " & _
                   "WHERE interviewertranunkid = @interviewertranunkid " & _
                   "AND ISNULL(isvoid,0)=0 "

            objDataOperation.AddParameter("@interviewertranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intInterviewertranId)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If dsList.Tables(0).Rows.Count > 0 Then
                Return dsList
            Else
                Return Nothing
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetInterviewerData", mstrModuleName)
            Return Nothing
        End Try
    End Function


    'Sandeep [ 17 DEC 2010 ] -- Start
    Public Function getInterviewTypeCombo(ByVal intVacancyId As Integer, Optional ByVal StrList As String = "List", Optional ByVal blnFlag As Boolean = False) As DataSet
        Dim StrQ As String = String.Empty
        Dim dsList As New DataSet
        Dim exForce As Exception
        Try
            objDataOperation = New clsDataOperation
            If blnFlag = True Then
                StrQ = "SELECT 0 AS Id ,@Item AS Name UNION "
            End If
            StrQ &= "SELECT " & _
                    "	 cfcommon_master.masterunkid AS Id " & _
                    "	,ISNULL(cfcommon_master.name,'') AS Name " & _
                    "FROM rcinterviewer_tran " & _
                    "	JOIN cfcommon_master ON cfcommon_master.masterunkid = rcinterviewer_tran.interviewtypeunkid " & _
                    "WHERE vacancyunkid = @vacancyunkid AND isvoid=0 " & _
                    "ORDER BY Id "

            objDataOperation.AddParameter("@Item", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 1, "Select"))
            objDataOperation.AddParameter("@vacancyunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intVacancyId.ToString)

            dsList = objDataOperation.ExecQuery(StrQ, StrList)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "getInterviewTypeCombo", mstrModuleName)
            Return Nothing
        End Try
    End Function
    'Sandeep [ 17 DEC 2010 ] -- End 


	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Public Shared Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "Select")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class
