﻿'************************************************************************************************************************************
'Class Name : clsEarningDeduction.vb
'Purpose    :
'Date       :28/06/2010
'Written By :Suhail
'Modified   :
'************************************************************************************************************************************

Imports eZeeCommonLib
'S.SANDEEP [ 11 AUG 2012 ] -- START
'ENHANCEMENT : TRA CHANGES
Imports eZee.Common.eZeeForm
Imports System.ComponentModel
Imports System.Web
Imports System.Threading

'S.SANDEEP [ 11 AUG 2012 ] -- END

''' <summary>
''' Purpose: 
''' Developer: Suhail
''' </summary>
Public Class clsEarningDeduction
    Private Shared ReadOnly mstrModuleName As String = "clsearningdeduction"
    Dim objDataOperation As clsDataOperation
    Private xDataOperat As clsDataOperation = Nothing 'Sohail (10 Dec 2019)
    Dim mstrMessage As String = ""

#Region " Private variables "
    Private mintEdunkid As Integer
    Private mintEmployeeunkid As Integer
    Private mintTranheadunkid As Integer
    Private mintBatchtransactionunkid As Integer
    Private mdecAmount As Decimal = 0 'Sohail (11 May 2011)
    Private mblnIsdeduct As Boolean
    Private mintTrnheadtype_Id As Integer
    Private mintTypeof_Id As Integer
    Private mintCalctype_Id As Integer
    Private mintComputeon_Id As Integer
    Private mstrFormula As String = String.Empty
    Private mstrFormulaid As String = String.Empty
    Private mintCurrencyunkid As Integer
    Private mintVendorunkid As Integer
    Private mintUserunkid As Integer
    Private mblnIsvoid As Boolean
    Private mintVoiduserunkid As Integer
    Private mdtVoiddatetime As Date
    Private mstrVoidreason As String = String.Empty
    Private mintMembership_Categoryunkid As Integer
    'Sohail (14 Jun 2011) -- Start
    Private mblnIsapproved As Boolean
    Private mintApproveruserunkid As Integer = -1
    'Sohail (14 Jun 2011) -- End
    Private mintCostCenterUnkId As Integer = -1 'Sohail (08 Nov 2011)
    'Private mstrAUD As String = String.Empty
    'Private mstrGUID As String = String.Empty

    'Private mtblEarnDeductHeads As New DataTable

    'Sohail (13 Jan 2012) -- Start
    'TRA - ENHANCEMENT
    Private mintPeriodunkid As Integer
    Private mdtED As DataTable
    'Sohail (13 Jan 2012) -- End
    Private mstrMedicalrefno As String = String.Empty 'Sohail (18 Jan 2012)

    'S.SANDEEP [ 07 NOV 2011 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Private mintDisciplinefileunkid As Integer = -1
    'S.SANDEEP [ 07 NOV 2011 ] -- END

    'S.SANDEEP [ 19 JULY 2012 ] -- START
    'Enhancement : TRA Changes
    Private mstrWebFormName As String = String.Empty
    'S.SANDEEP [ 19 JULY 2012 ] -- END

    Private mstrDatabaseName As String = String.Empty  'Sohail (21 Jul 2012)

    'S.SANDEEP [ 13 AUG 2012 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Private mintLogEmployeeUnkid As Integer = -1
    'S.SANDEEP [ 13 AUG 2012 ] -- END

    'S.SANDEEP [ 17 OCT 2012 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Private mintMembershipTranUnkid As Integer = 0
    'S.SANDEEP [ 17 OCT 2012 ] -- END
    'Sohail (09 Nov 2013) -- Start
    'TRA - ENHANCEMENT
    Private mdtCumulative_Startdate As Date = Nothing
    Private mdtStop_Date As Date = Nothing
    'Sohail (09 Nov 2013) -- End

    'Pinkal (24-May-2013) -- Start
    'Enhancement : TRA Changes
    Private mstrWebClientID As String = ""
    Private mstrWebhostName As String = ""
    'Pinkal (24-May-2013) -- End
    'Sohail (24 Feb 2016) -- Start
    'Enhancement - Batch Posting changes as per online doc in 58.1 SP. (#15 (KBC & Kenya Project Comments List.xls))
    Private mintEmpbatchpostingunkid As Integer = 0
    'Sohail (24 Feb 2016) -- End
    'Sohail (24 Aug 2019) -- Start
    'NMB Payroll UAT # TC016 - 76.1 - System should be able to accept future amendments (Future period) and prorate. It should be possible to set start date of the specific transaction and prorate based on the start date. It should also be possible to set start date of the future period.
    Private mdtEDStart_Date As Date = Nothing
    'Sohail (24 Aug 2019) -- End
    'Sohail (02 Sep 2019) -- Start
    'Mainspring Resourcing Ghana Ltd Enhancement - 76.1 - # 0004098: Approval Flow for Flat Rate Heads Amount Uploaded.
    '1. Set ED in pending status if user has not priviledge of ED approval
    '2. Perform Global approver with option to filter for allocation and approved any posted (new /edited) E & D values in MSS
    '3. Send notifications to ED approvers for all employees ED in one email
    Private trd As Thread
    'Sohail (02 Sep 2019) -- End

#End Region

    '#Region " Constructor "
    '    Public Sub New()
    '        mtblEarnDeductHeads = New DataTable("ED")
    '        Dim dCol As New DataColumn

    '        Try
    '            dCol = New DataColumn("edunkid")
    '            dCol.DataType = System.Type.GetType("System.Int32")
    '            mtblEarnDeductHeads.Columns.Add(dCol)

    '            dCol = New DataColumn("batchtranunkid")
    '            dCol.DataType = System.Type.GetType("System.Int32")
    '            mtblEarnDeductHeads.Columns.Add(dCol)

    '            dCol = New DataColumn("tranheadunkid")
    '            dCol.DataType = System.Type.GetType("System.Int32")
    '            mtblEarnDeductHeads.Columns.Add(dCol)

    '            dCol = New DataColumn("tranhead")
    '            dCol.DataType = System.Type.GetType("System.String")
    '            mtblEarnDeductHeads.Columns.Add(dCol)

    '            dCol = New DataColumn("trnheadtypeID")
    '            dCol.DataType = System.Type.GetType("System.Int32")
    '            mtblEarnDeductHeads.Columns.Add(dCol)


    '            dCol = New DataColumn("trnheadtype")
    '            dCol.DataType = System.Type.GetType("System.String")
    '            mtblEarnDeductHeads.Columns.Add(dCol)

    '            dCol = New DataColumn("typeofId")
    '            dCol.DataType = System.Type.GetType("System.Int32")
    '            mtblEarnDeductHeads.Columns.Add(dCol)

    '            dCol = New DataColumn("calctypeId")
    '            dCol.DataType = System.Type.GetType("System.Int32")
    '            mtblEarnDeductHeads.Columns.Add(dCol)

    '            dCol = New DataColumn("calctype")
    '            dCol.DataType = System.Type.GetType("System.String")
    '            mtblEarnDeductHeads.Columns.Add(dCol)

    '            dCol = New DataColumn("computeonID")
    '            dCol.DataType = System.Type.GetType("System.Int32")
    '            mtblEarnDeductHeads.Columns.Add(dCol)

    '            dCol = New DataColumn("computeon")
    '            dCol.DataType = System.Type.GetType("System.String")
    '            mtblEarnDeductHeads.Columns.Add(dCol)

    '            dCol = New DataColumn("isdeduct")
    '            dCol.DataType = System.Type.GetType("System.Boolean")
    '            mtblEarnDeductHeads.Columns.Add(dCol)

    '            dCol = New DataColumn("amount")
    '            dCol.DataType = System.Type.GetType("System.Decimal")
    '            mtblEarnDeductHeads.Columns.Add(dCol)

    '            dCol = New DataColumn("currencyunkId")
    '            dCol.DataType = System.Type.GetType("System.Int32")
    '            mtblEarnDeductHeads.Columns.Add(dCol)

    '            dCol = New DataColumn("vendorunkId")
    '            dCol.DataType = System.Type.GetType("System.Int32")
    '            mtblEarnDeductHeads.Columns.Add(dCol)

    '            dCol = New DataColumn("userunkId")
    '            dCol.DataType = System.Type.GetType("System.Int32")
    '            mtblEarnDeductHeads.Columns.Add(dCol)

    '            dCol = New DataColumn("isvoid")
    '            dCol.DataType = System.Type.GetType("System.Boolean")
    '            mtblEarnDeductHeads.Columns.Add(dCol)

    '            dCol = New DataColumn("voiduserunkId")
    '            dCol.DataType = System.Type.GetType("System.Int32")
    '            mtblEarnDeductHeads.Columns.Add(dCol)

    '            dCol = New DataColumn("voiddatetime")
    '            dCol.DataType = System.Type.GetType("System.DateTime")
    '            mtblEarnDeductHeads.Columns.Add(dCol)

    '            dCol = New DataColumn("formula")
    '            dCol.DataType = System.Type.GetType("System.String")
    '            mtblEarnDeductHeads.Columns.Add(dCol)

    '            dCol = New DataColumn("formulaID")
    '            dCol.DataType = System.Type.GetType("System.String")
    '            mtblEarnDeductHeads.Columns.Add(dCol)

    '            dCol = New DataColumn("AUD")
    '            dCol.DataType = System.Type.GetType("System.String")
    '            mtblEarnDeductHeads.Columns.Add(dCol)

    '            dCol = New DataColumn("GUID")
    '            dCol.DataType = System.Type.GetType("System.String")
    '            mtblEarnDeductHeads.Columns.Add(dCol)

    '        Catch ex As Exception
    '            DisplayError.Show("-1", ex.Message, "New", mstrModuleName)
    '        End Try
    '    End Sub
    '#End Region

    'Sohail (13 Jan 2012) -- Start
    'TRA - ENHANCEMENT
#Region " Constructor "
    Public Sub New()
        mdtED = New DataTable("ED")
        Dim dCol As New DataColumn

        Try
            dCol = New DataColumn("edunkid")
            dCol.DataType = System.Type.GetType("System.Int32")
            dCol.DefaultValue = 0 'Sohail (30 Mar 2013)
            mdtED.Columns.Add(dCol)

            dCol = New DataColumn("batchtransactionunkid")
            dCol.DataType = System.Type.GetType("System.Int32")
            dCol.DefaultValue = 0 'Sohail (30 Mar 2013)
            mdtED.Columns.Add(dCol)

            dCol = New DataColumn("employeeunkid")
            dCol.DataType = System.Type.GetType("System.Int32")
            dCol.DefaultValue = 0 'Sohail (30 Mar 2013)
            mdtED.Columns.Add(dCol)

            dCol = New DataColumn("tranheadunkid")
            dCol.DataType = System.Type.GetType("System.Int32")
            dCol.DefaultValue = 0 'Sohail (30 Mar 2013)
            mdtED.Columns.Add(dCol)

            dCol = New DataColumn("trnheadname")
            dCol.DataType = System.Type.GetType("System.String")
            dCol.DefaultValue = "" 'Sohail (30 Mar 2013)
            mdtED.Columns.Add(dCol)

            dCol = New DataColumn("trnheadtype_id")
            dCol.DataType = System.Type.GetType("System.Int32")
            dCol.DefaultValue = 0 'Sohail (30 Mar 2013)
            mdtED.Columns.Add(dCol)


            dCol = New DataColumn("trnheadtype")
            dCol.DataType = System.Type.GetType("System.String")
            dCol.DefaultValue = "" 'Sohail (30 Mar 2013)
            mdtED.Columns.Add(dCol)

            dCol = New DataColumn("typeof_id")
            dCol.DataType = System.Type.GetType("System.Int32")
            dCol.DefaultValue = 0 'Sohail (30 Mar 2013)
            mdtED.Columns.Add(dCol)

            dCol = New DataColumn("calctype_id")
            dCol.DataType = System.Type.GetType("System.Int32")
            dCol.DefaultValue = 0 'Sohail (30 Mar 2013)
            mdtED.Columns.Add(dCol)

            dCol = New DataColumn("calctype")
            dCol.DataType = System.Type.GetType("System.String")
            dCol.DefaultValue = "" 'Sohail (30 Mar 2013)
            mdtED.Columns.Add(dCol)

            dCol = New DataColumn("computeon_id")
            dCol.DataType = System.Type.GetType("System.Int32")
            dCol.DefaultValue = 0 'Sohail (30 Mar 2013)
            mdtED.Columns.Add(dCol)

            dCol = New DataColumn("computeon")
            dCol.DataType = System.Type.GetType("System.String")
            dCol.DefaultValue = "" 'Sohail (30 Mar 2013)
            mdtED.Columns.Add(dCol)

            dCol = New DataColumn("isdeduct")
            dCol.DataType = System.Type.GetType("System.Boolean")
            dCol.DefaultValue = False 'Sohail (30 Mar 2013)
            mdtED.Columns.Add(dCol)

            dCol = New DataColumn("amount")
            dCol.DataType = System.Type.GetType("System.Decimal")
            dCol.DefaultValue = 0 'Sohail (30 Mar 2013)
            mdtED.Columns.Add(dCol)

            dCol = New DataColumn("currencyId")
            dCol.DataType = System.Type.GetType("System.Int32")
            dCol.DefaultValue = 0 'Sohail (30 Mar 2013)
            mdtED.Columns.Add(dCol)

            dCol = New DataColumn("vendorId")
            dCol.DataType = System.Type.GetType("System.Int32")
            mdtED.Columns.Add(dCol)

            dCol = New DataColumn("userunkId")
            dCol.DataType = System.Type.GetType("System.Int32")
            dCol.DefaultValue = 0 'Sohail (30 Mar 2013)
            mdtED.Columns.Add(dCol)

            dCol = New DataColumn("isvoid")
            dCol.DataType = System.Type.GetType("System.Boolean")
            dCol.DefaultValue = False 'Sohail (30 Mar 2013)
            mdtED.Columns.Add(dCol)

            dCol = New DataColumn("voiduserunkId")
            dCol.DataType = System.Type.GetType("System.Int32")
            dCol.DefaultValue = 0 'Sohail (30 Mar 2013)
            mdtED.Columns.Add(dCol)

            dCol = New DataColumn("voiddatetime")
            dCol.DataType = System.Type.GetType("System.DateTime")
            mdtED.Columns.Add(dCol)

            dCol = New DataColumn("voidreason")
            dCol.DataType = System.Type.GetType("System.String")
            dCol.DefaultValue = "" 'Sohail (30 Mar 2013)
            mdtED.Columns.Add(dCol)

            dCol = New DataColumn("formula")
            dCol.DataType = System.Type.GetType("System.String")
            dCol.DefaultValue = "" 'Sohail (30 Mar 2013)
            mdtED.Columns.Add(dCol)

            dCol = New DataColumn("formulaid")
            dCol.DataType = System.Type.GetType("System.String")
            dCol.DefaultValue = "" 'Sohail (30 Mar 2013)
            mdtED.Columns.Add(dCol)

            dCol = New DataColumn("membership_categoryunkid")
            dCol.DataType = System.Type.GetType("System.Int32")
            dCol.DefaultValue = 0 'Sohail (30 Mar 2013)
            mdtED.Columns.Add(dCol)

            dCol = New DataColumn("periodunkid")
            dCol.DataType = System.Type.GetType("System.Int32")
            dCol.DefaultValue = 0 'Sohail (30 Mar 2013)
            mdtED.Columns.Add(dCol)

            dCol = New DataColumn("period_name")
            dCol.DataType = System.Type.GetType("System.String")
            dCol.DefaultValue = "" 'Sohail (30 Mar 2013)
            mdtED.Columns.Add(dCol)

            dCol = New DataColumn("start_date")
            dCol.DataType = System.Type.GetType("System.String")
            dCol.DefaultValue = "" 'Sohail (30 Mar 2013)
            mdtED.Columns.Add(dCol)

            dCol = New DataColumn("end_date")
            dCol.DataType = System.Type.GetType("System.String")
            dCol.DefaultValue = "" 'Sohail (30 Mar 2013)
            mdtED.Columns.Add(dCol)

            dCol = New DataColumn("isapproved")
            dCol.DataType = System.Type.GetType("System.Boolean")
            dCol.DefaultValue = False 'Sohail (30 Mar 2013)
            mdtED.Columns.Add(dCol)

            dCol = New DataColumn("approveruserunkid")
            dCol.DataType = System.Type.GetType("System.Int32")
            dCol.DefaultValue = 0 'Sohail (30 Mar 2013)
            mdtED.Columns.Add(dCol)

            dCol = New DataColumn("AUD")
            dCol.DataType = System.Type.GetType("System.String")
            dCol.DefaultValue = "" 'Sohail (30 Mar 2013)
            mdtED.Columns.Add(dCol)

            dCol = New DataColumn("GUID")
            dCol.DataType = System.Type.GetType("System.String")
            mdtED.Columns.Add(dCol)

            'Sohail (18 Jan 2012) -- Start
            'TRA - ENHANCEMENT
            dCol = New DataColumn("costcenterunkid")
            dCol.DataType = System.Type.GetType("System.Int32")
            dCol.DefaultValue = 0 'Sohail (30 Mar 2013)
            mdtED.Columns.Add(dCol)

            dCol = New DataColumn("isrecurrent")
            dCol.DataType = System.Type.GetType("System.Boolean")
            dCol.DefaultValue = False 'Sohail (30 Mar 2013)
            mdtED.Columns.Add(dCol)
            'Sohail (18 Jan 2012) -- End

            'Sohail (18 Jan 2012) -- Start
            'TRA - ENHANCEMENT
            dCol = New DataColumn("medicalrefno")
            dCol.DataType = System.Type.GetType("System.String")
            dCol.DefaultValue = "" 'Sohail (30 Mar 2013)
            mdtED.Columns.Add(dCol)
            'Sohail (18 Jan 2012) -- End

            'S.SANDEEP [ 11 DEC 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            dCol = New DataColumn("membershiptranunkid")
            dCol.DataType = System.Type.GetType("System.Int32")
            dCol.DefaultValue = 0
            mdtED.Columns.Add(dCol)
            'S.SANDEEP [ 11 DEC 2012 ] -- END

            'S.SANDEEP [ 17 DEC 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            dCol = New DataColumn("disciplinefileunkid")
            dCol.DataType = System.Type.GetType("System.Int32")
            dCol.DefaultValue = 0
            mdtED.Columns.Add(dCol)
            'S.SANDEEP [ 17 DEC 2012 ] -- END

            'Sohail (09 Nov 2013) -- Start
            'TRA - ENHANCEMENT
            mdtED.Columns.Add("cumulative_startdate", System.Type.GetType("System.DateTime"))
            mdtED.Columns.Add("stop_date", System.Type.GetType("System.DateTime"))
            'Sohail (09 Nov 2013) -- End

            'Sohail (24 Aug 2019) -- Start
            'NMB Payroll UAT # TC016 - 76.1 - System should be able to accept future amendments (Future period) and prorate. It should be possible to set start date of the specific transaction and prorate based on the start date. It should also be possible to set start date of the future period.
            mdtED.Columns.Add("edstart_date", System.Type.GetType("System.DateTime"))
            'Sohail (24 Aug 2019) -- End

            'Sohail (24 Feb 2016) -- Start
            'Enhancement - Batch Posting changes as per online doc in 58.1 SP. (#15 (KBC & Kenya Project Comments List.xls))
            dCol = New DataColumn("empbatchpostingunkid", System.Type.GetType("System.Int32"))
            dCol.DefaultValue = 0
            mdtED.Columns.Add(dCol)
            'Sohail (24 Feb 2016) -- End

            'Sohail (21 Dec 2016) -- Start
            'ACB Enhancement - 64.1 - Loan data Integration with T24 ACB System.
            mdtED.Columns.Add("employeecode", System.Type.GetType("System.String")).DefaultValue = ""
            mdtED.Columns.Add("employeename", System.Type.GetType("System.String")).DefaultValue = ""
            mdtED.Columns.Add("rowtypeid", System.Type.GetType("System.Int32")).DefaultValue = 1
            mdtED.Columns.Add("rowtype_remark", System.Type.GetType("System.String")).DefaultValue = ""
            mdtED.Columns.Add("IsChecked", System.Type.GetType("System.Boolean")).DefaultValue = False
            'Sohail (21 Dec 2016) -- End

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "New", mstrModuleName)
        End Try
    End Sub
#End Region
    'Sohail (13 Jan 2012) -- End

#Region " Properties "

    'S.SANDEEP [28-May-2018] -- START 
    'ISSUE/ENHANCEMENT : {Audit Trails} 
    Private mstrFormName As String = String.Empty
    Public WriteOnly Property _FormName() As String
        Set(ByVal value As String)
            mstrFormName = value
        End Set
    End Property

    Private mstrClientIP As String = ""
    Public WriteOnly Property _ClientIP() As String
        Set(ByVal value As String)
            mstrClientIP = value
        End Set
    End Property

    Private mstrHostName As String = ""
    Public WriteOnly Property _HostName() As String
        Set(ByVal value As String)
            mstrHostName = value
        End Set
    End Property

    Private mblnIsWeb As Boolean = False
    Public WriteOnly Property _FromWeb() As Boolean
        Set(ByVal value As Boolean)
            mblnIsWeb = value
        End Set
    End Property

    Private mintAuditUserId As Integer = 0
    Public WriteOnly Property _AuditUserId() As Integer
        Set(ByVal value As Integer)
            mintAuditUserId = value
        End Set
    End Property

    Private mintCompanyUnkid As Integer = 0
    Public WriteOnly Property _CompanyUnkid() As Integer
        Set(ByVal value As Integer)
            mintCompanyUnkid = value
        End Set
    End Property

    Private mdtAuditDate As DateTime = Now
    Public WriteOnly Property _AuditDate() As DateTime
        Set(ByVal value As DateTime)
            mdtAuditDate = value
        End Set
    End Property

    Private xDataOpr As clsDataOperation = Nothing
    Public Property _xDataOpr() As clsDataOperation
        Get
            Return xDataOpr
        End Get
        Set(ByVal value As clsDataOperation)
            xDataOpr = value
        End Set
    End Property

    Private mintVoidlogingemployeeunkid As Integer = 0
    Public Property _Voidloginemployeeunkid() As Integer
        Get
            Return mintVoidlogingemployeeunkid
        End Get
        Set(ByVal value As Integer)
            mintVoidlogingemployeeunkid = value
        End Set
    End Property
    'S.SANDEEP [28-May-2018] -- END 
    ''' <summary>
    ''' Purpose: Get Message from Class 
    ''' Modify By: Suhail
    ''' </summary>
    Public ReadOnly Property _Message() As String
        Get
            Return mstrMessage
        End Get
    End Property

    'Sohail (23 Apr 2012) -- Start
    'TRA - ENHANCEMENT
    Public WriteOnly Property _DataOperation() As clsDataOperation
        Set(ByVal value As clsDataOperation)
            'Sohail (10 Dec 2019) -- Start
            'MAIN SPRING RESOURCES Support Issue Id # 0004311 : Add employee code, cost centre and class group columns in email notification.
            'objDataOperation = value
            xDataOperat = value
            'Sohail (10 Dec 2019) -- End
        End Set
    End Property
    'Sohail (23 Apr 2012) -- End

    ''' <summary>
    ''' Purpose: Get or Set edunkid
    ''' Modify By: Suhail
    ''' </summary>
    ''' <param name="xDatop">Pass Nothing from eZeePersonal project or any other Forms</param>
    ''' <param name="strDatabaseName">Current Financial Year Database Name</param>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Property _Edunkid(ByVal xDatop As clsDataOperation, ByVal strDatabaseName As String) As Integer
        Get
            Return mintEdunkid
        End Get
        Set(ByVal value As Integer)
            mintEdunkid = value
            'Sohail (23 Apr 2012) -- Start
            'TRA - ENHANCEMENT
            'Call GetData()
            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'Call GetData(objDataOperation)
            'Sohail (10 Dec 2019) -- Start
            'MAIN SPRING RESOURCES Support Issue Id # 0004311 : Add employee code, cost centre and class group columns in email notification.
            'Call GetData(objDataOperation, strDatabaseName)
            Call GetData(xDatop, strDatabaseName)
            'Sohail (10 Dec 2019) -- End
            'Sohail (21 Aug 2015) -- End
            'Sohail (23 Apr 2012) -- End
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set employeeunkid
    ''' Modify By: Suhail
    ''' </summary>
    Public Property _Employeeunkid(ByVal strDatabaseName As String) As Integer
        Get
            Return mintEmployeeunkid
        End Get
        Set(ByVal value As Integer)
            mintEmployeeunkid = value
            'Call GetData()
            'Sohail (13 Jan 2012) -- Start
            'TRA - ENHANCEMENT
            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'Call GetEmployeeED()
            'Sohail (01 Mar 2018) -- Start
            'Internal Issue : ExecureReader requires the command to have a transaction... in 70.1.
            'Call GetEmployeeED(strDatabaseName)
            'Sohail (10 Dec 2019) -- Start
            'MAIN SPRING RESOURCES Support Issue Id # 0004311 : Add employee code, cost centre and class group columns in email notification.
            'Call GetEmployeeED(objDataOperation, strDatabaseName)
            Call GetEmployeeED(xDataOperat, strDatabaseName)
            'Sohail (10 Dec 2019) -- End
            'Sohail (01 Mar 2018) -- End
            'Sohail (21 Aug 2015) -- End
            'Sohail (13 Jan 2012) -- End
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set tranheadunkid
    ''' Modify By: Suhail
    ''' </summary>
    Public Property _Tranheadunkid() As Integer
        Get
            Return mintTranheadunkid
        End Get
        Set(ByVal value As Integer)
            mintTranheadunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set batchtransactionunkid
    ''' Modify By: Suhail
    ''' </summary>
    Public Property _Batchtransactionunkid() As Integer
        Get
            Return mintBatchtransactionunkid
        End Get
        Set(ByVal value As Integer)
            mintBatchtransactionunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set amount
    ''' Modify By: Suhail
    ''' </summary>
    Public Property _Amount() As Decimal 'Sohail (11 May 2011)
        Get
            Return mdecAmount
        End Get
        Set(ByVal value As Decimal) 'Sohail (11 May 2011)
            mdecAmount = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isdeduct
    ''' Modify By: Suhail
    ''' </summary>
    Public Property _Isdeduct() As Boolean
        Get
            Return mblnIsdeduct
        End Get
        Set(ByVal value As Boolean)
            mblnIsdeduct = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set trnheadtype_id
    ''' Modify By: Suhail
    ''' </summary>
    Public Property _Trnheadtype_Id() As Integer
        Get
            Return mintTrnheadtype_Id
        End Get
        Set(ByVal value As Integer)
            mintTrnheadtype_Id = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set typeof_id
    ''' Modify By: Suhail
    ''' </summary>
    Public Property _Typeof_Id() As Integer
        Get
            Return mintTypeof_Id
        End Get
        Set(ByVal value As Integer)
            mintTypeof_Id = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set calctype_id
    ''' Modify By: Suhail
    ''' </summary>
    Public Property _Calctype_Id() As Integer
        Get
            Return mintCalctype_Id
        End Get
        Set(ByVal value As Integer)
            mintCalctype_Id = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set computeon_id
    ''' Modify By: Suhail
    ''' </summary>
    Public Property _Computeon_Id() As Integer
        Get
            Return mintComputeon_Id
        End Get
        Set(ByVal value As Integer)
            mintComputeon_Id = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set formula
    ''' Modify By: Suhail
    ''' </summary>
    Public Property _Formula() As String
        Get
            Return mstrFormula
        End Get
        Set(ByVal value As String)
            mstrFormula = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set formulaid
    ''' Modify By: Suhail
    ''' </summary>
    Public Property _FormulaId() As String
        Get
            Return mstrFormulaid
        End Get
        Set(ByVal value As String)
            mstrFormulaid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set currencyunkid
    ''' Modify By: Suhail
    ''' </summary>
    Public Property _Currencyunkid() As Integer
        Get
            Return mintCurrencyunkid
        End Get
        Set(ByVal value As Integer)
            mintCurrencyunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set vendorunkid
    ''' Modify By: Suhail
    ''' </summary>
    Public Property _Vendorunkid() As Integer
        Get
            Return mintVendorunkid
        End Get
        Set(ByVal value As Integer)
            mintVendorunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set userunkid
    ''' Modify By: Suhail
    ''' </summary>
    Public Property _Userunkid() As Integer
        Get
            Return mintUserunkid
        End Get
        Set(ByVal value As Integer)
            mintUserunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isvoid
    ''' Modify By: Suhail
    ''' </summary>
    Public Property _Isvoid() As Boolean
        Get
            Return mblnIsvoid
        End Get
        Set(ByVal value As Boolean)
            mblnIsvoid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiduserunkid
    ''' Modify By: Suhail
    ''' </summary>
    Public Property _Voiduserunkid() As Integer
        Get
            Return mintVoiduserunkid
        End Get
        Set(ByVal value As Integer)
            mintVoiduserunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiddatetime
    ''' Modify By: Suhail
    ''' </summary>
    Public Property _Voiddatetime() As Date
        Get
            Return mdtVoiddatetime
        End Get
        Set(ByVal value As Date)
            mdtVoiddatetime = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voidreason
    ''' Modify By: Suhail
    ''' </summary>
    Public Property _Voidreason() As String
        Get
            Return mstrVoidreason
        End Get
        Set(ByVal value As String)
            mstrVoidreason = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set membership_categoryunkid
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Membership_Categoryunkid() As Integer
        Get
            Return mintMembership_Categoryunkid
        End Get
        Set(ByVal value As Integer)
            mintMembership_Categoryunkid = value
        End Set
    End Property

    'Sohail (14 Jun 2011) -- Start
    ''' <summary>
    ''' Purpose: Get or Set isapproved
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Isapproved() As Boolean
        Get
            Return mblnIsapproved
        End Get
        Set(ByVal value As Boolean)
            mblnIsapproved = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set approveruserunkid
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Approveruserunkid() As Integer
        Get
            Return mintApproveruserunkid
        End Get
        Set(ByVal value As Integer)
            mintApproveruserunkid = value
        End Set
    End Property
    'Sohail (14 Jun 2011) -- End

    'Sohail (08 Nov 2011) -- Start
    'Sohail (21 Dec 2016) -- Start
    'ACB Enhancement - 64.1 - Loan data Integration with T24 ACB System.
    'Public WriteOnly Property _CostCenterUnkID()
    '    Set(ByVal value)
    '        mintCostCenterUnkId = value
    '    End Set
    'End Property
    Public WriteOnly Property _CostCenterUnkID() As Integer
        Set(ByVal value As Integer)
            mintCostCenterUnkId = value
        End Set
    End Property
    'Sohail (21 Dec 2016) -- End
    'Sohail (08 Nov 2011) -- End

    'Sohail (13 Jan 2012) -- Start
    'TRA - ENHANCEMENT
    ''' <summary>
    ''' Purpose: Get or Set periodunkid
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Periodunkid() As Integer
        Get
            Return mintPeriodunkid
        End Get
        Set(ByVal value As Integer)
            mintPeriodunkid = value
        End Set
    End Property

    Public ReadOnly Property _DataSource() As DataTable
        Get
            'Sohail (15 Dec 2018) -- Start
            'HJFMRI Enhancement - Copy Selected Period ED Slab option on Global Assign ED screen in 75.1.
            'Return mdtED
            Return mdtED.Copy
            'Sohail (15 Dec 2018) -- End
        End Get

    End Property
    'Sohail (13 Jan 2012) -- End

    'Sohail (18 Jan 2012) -- Start
    'TRA - ENHANCEMENT
    Public Property _MedicalRefNo() As String
        Get
            Return mstrMedicalrefno
        End Get
        Set(ByVal value As String)
            mstrMedicalrefno = value
        End Set
    End Property
    'Sohail (18 Jan 2012) -- End

    '''' <summary>
    '''' Get Earning and Deduction Transaction Heads List
    '''' </summary>
    '''' <value></value>
    '''' <returns></returns>
    '''' <remarks>Modify By: Suhail</remarks>
    'Public ReadOnly Property _EarnDeductHeads() As DataTable
    '    Get
    '        Return mtblEarnDeductHeads
    '    End Get

    'End Property

    '''' <summary>
    '''' Purpose: Get or Set AUD
    '''' Modify By: Suhail
    '''' </summary>
    'Public Property _AUD() As String
    '    Get
    '        Return mstrAUD
    '    End Get
    '    Set(ByVal value As String)
    '        mstrAUD = value
    '    End Set
    'End Property

    '''' <summary>
    '''' Purpose: Get or Set AUD
    '''' Modify By: Suhail
    '''' </summary>
    'Public Property _GUID() As String
    '    Get
    '        Return mstrGUID
    '    End Get
    '    Set(ByVal value As String)
    '        mstrGUID = value
    '    End Set
    'End Property



    'S.SANDEEP [ 07 NOV 2011 ] -- START
    'ENHANCEMENT : TRA CHANGES
    ''' <summary>
    ''' Purpose: Get or Set disciplinefileunkid
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Disciplinefileunkid() As Integer
        Get
            Return mintDisciplinefileunkid
        End Get
        Set(ByVal value As Integer)
            mintDisciplinefileunkid = value
        End Set
    End Property

    'S.SANDEEP [ 07 NOV 2011 ] -- END



    'S.SANDEEP [ 19 JULY 2012 ] -- START
    'Enhancement : TRA Changes

    Public Property _WebFormName() As String
        Get
            Return mstrWebFormName
        End Get
        Set(ByVal value As String)
            mstrWebFormName = value
        End Set
    End Property

    'S.SANDEEP [ 19 JULY 2012 ] -- END

    'S.SANDEEP [ 13 AUG 2012 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Public WriteOnly Property _LoginEmployeeUnkid() As Integer
        Set(ByVal value As Integer)
            mintLogEmployeeUnkid = value
        End Set
    End Property
    'S.SANDEEP [ 13 AUG 2012 ] -- END

    'Sohail (21 Jul 2012) -- Start
    'TRA - ENHANCEMENT
#Region " Other Properties "
    Public WriteOnly Property _DatabaseName() As String
        Set(ByVal value As String)
            mstrDatabaseName = value
        End Set
    End Property
#End Region
    'Sohail (21 Jul 2012) -- End

    'S.SANDEEP [ 17 OCT 2012 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Public Property _MembershipTranUnkid() As Integer
        Get
            Return mintMembershipTranUnkid
        End Get
        Set(ByVal value As Integer)
            mintMembershipTranUnkid = value
        End Set
    End Property
    'S.SANDEEP [ 17 OCT 2012 ] -- END

    'Sohail (09 Nov 2013) -- Start
    'TRA - ENHANCEMENT
    ''' <summary>
    ''' Purpose: Get or Set start_date
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Cumulative_Startdate() As Date
        Get
            Return mdtCumulative_Startdate
        End Get
        Set(ByVal value As Date)
            mdtCumulative_Startdate = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set stop_date
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Stop_Date() As Date
        Get
            Return mdtStop_Date
        End Get
        Set(ByVal value As Date)
            mdtStop_Date = value
        End Set
    End Property
    'Sohail (09 Nov 2013) -- End

    'Sohail (24 Aug 2019) -- Start
    'NMB Payroll UAT # TC016 - 76.1 - System should be able to accept future amendments (Future period) and prorate. It should be possible to set start date of the specific transaction and prorate based on the start date. It should also be possible to set start date of the future period.
    Public Property _EDStart_Date() As Date
        Get
            Return mdtEDStart_Date
        End Get
        Set(ByVal value As Date)
            mdtEDStart_Date = value
        End Set
    End Property
    'Sohail (24 Aug 2019) -- End



    'Pinkal (24-May-2013) -- Start
    'Enhancement : TRA Changes

    'Public WriteOnly Property _WebClientIP() As String
    '    Set(ByVal value As String)
    '        mstrWebClientID = value
    '    End Set
    'End Property

    'Public WriteOnly Property _WebHostName() As String
    '    Set(ByVal value As String)
    '        mstrWebhostName = value
    '    End Set
    'End Property

    'Sohail (24 Feb 2016) -- Start
    'Enhancement - Batch Posting changes as per online doc in 58.1 SP. (#15 (KBC & Kenya Project Comments List.xls))
    Public Property _EmpBatchPostingUnkid() As Integer
        Get
            Return mintEmpbatchpostingunkid
        End Get
        Set(ByVal value As Integer)
            mintEmpbatchpostingunkid = value
        End Set
    End Property
    'Sohail (24 Feb 2016) -- End

#End Region



    'Sohail (21 Aug 2015) -- Start
    'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
    '''' <summary>
    '''' Modify By: Suhail
    '''' </summary>
    '''' <purpose> Assign all Property variable </purpose>
    'Public Sub GetData(Optional ByVal objDataOperation As clsDataOperation = Nothing) 'Sohail (23 Apr 2012) - [objDataOperation]
    Private Sub GetData(ByVal objDataOp As clsDataOperation, ByVal strDatabaseName As String)
        'Private Sub GetData(ByVal objDataOperation As clsDataOperation, ByVal strDatabaseName As String) 'Sohail (01 Mar 2018)

        'Sohail (21 Aug 2015) -- End
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        'Sohail (23 Apr 2012) -- Start
        'TRA - ENHANCEMENT
        'objDataOperation = New clsDataOperation
        'Sohail (01 Mar 2018) -- Start
        'Internal Issue : ExecureReader requires the command to have a transaction... in 70.1.
        'If objDataOperation Is Nothing Then
        '    objDataOperation = New clsDataOperation
        'End If
        If objDataOp IsNot Nothing Then
            objDataOperation = objDataOp
        Else
            objDataOperation = New clsDataOperation
        End If
        'Sohail (01 Mar 2018) -- End
        objDataOperation.ClearParameters()
        'Sohail (23 Apr 2012) -- End

        'Sohail (21 Jul 2012) -- Start
        'TRA - ENHANCEMENT
        'Sohail (21 Aug 2015) -- Start
        'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
        'If mstrDatabaseName.Trim = "" Then mstrDatabaseName = FinancialYear._Object._DatabaseName
        mstrDatabaseName = strDatabaseName
        'Sohail (21 Aug 2015) -- End
        'Sohail (21 Jul 2012) -- End

        Try
            strQ = "SELECT " & _
              "  edunkid " & _
              ", employeeunkid " & _
              ", tranheadunkid " & _
              ", batchtransactionunkid " & _
              ", amount " & _
              ", isdeduct " & _
              ", trnheadtype_id " & _
              ", typeof_id " & _
              ", calctype_id " & _
              ", computeon_id " & _
              ", formula " & _
              ", formulaid " & _
              ", currencyunkid " & _
              ", vendorunkid " & _
              ", userunkid " & _
              ", isvoid " & _
              ", voiduserunkid " & _
              ", voiddatetime " & _
              ", voidreason " & _
              ", membership_categoryunkid " & _
              ", ISNULL(isapproved, 0) AS isapproved " & _
              ", ISNULL(approveruserunkid, 0) AS approveruserunkid " & _
              ", ISNULL(periodunkid, 0) AS periodunkid " & _
              ", ISNULL(medicalrefno, '') AS medicalrefno " & _
              ", ISNULL(disciplinefileunkid,0) As disciplinefileunkid  " & _
              ", ISNULL(membershiptranunkid,0) AS membershiptranunkid " & _
              ", cumulative_startdate " & _
              ", stop_date " & _
              ", edstart_date " & _
              ", ISNULL(prearningdeduction_master.empbatchpostingunkid,0) AS empbatchpostingunkid " & _
             "FROM " & mstrDatabaseName & "..prearningdeduction_master " & _
             "WHERE edunkid = @edunkid "
            'Sohail (24 Aug 2019) - [edstart_date]
            'Sohail (24 Feb 2016) - [empbatchpostingunkid]
            'Sohail (09 Nov 2013) - [cumulative_startdate, stop_date]
            'Sohail (16 Oct 2010), [isapproved, approveruserunkid - Sohail (14 Jun 2011)]  'S.SANDEEP [ 07 NOV 2011 ], [disciplinefileunkid]
            'S.SANDEEP [ 17 OCT 2012 ],[membershiptranunkid]

            objDataOperation.AddParameter("@edunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEdunkid.ToString)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For Each dtRow As DataRow In dsList.Tables(0).Rows
                mintEdunkid = CInt(dtRow.Item("edunkid"))
                mintEmployeeunkid = CInt(dtRow.Item("employeeunkid"))
                mintTranheadunkid = CInt(dtRow.Item("tranheadunkid"))
                mintBatchtransactionunkid = CInt(dtRow.Item("batchtransactionunkid"))
                mdecAmount = CDec(dtRow.Item("amount")) 'Sohail (11 May 2011)
                mblnIsdeduct = CBool(dtRow.Item("isdeduct"))
                mintTrnheadtype_Id = CInt(dtRow.Item("trnheadtype_id"))
                mintTypeof_Id = CInt(dtRow.Item("typeof_id"))
                mintCalctype_Id = CInt(dtRow.Item("calctype_id"))
                mintComputeon_Id = CInt(dtRow.Item("computeon_id"))
                mstrFormula = dtRow.Item("formula").ToString
                mstrFormulaid = dtRow.Item("formulaid").ToString
                mintCurrencyunkid = CInt(dtRow.Item("currencyunkid"))
                mintVendorunkid = CInt(dtRow.Item("vendorunkid"))
                mintUserunkid = CInt(dtRow.Item("userunkid"))
                mblnIsvoid = CBool(dtRow.Item("isvoid"))
                mintVoiduserunkid = CInt(dtRow.Item("voiduserunkid"))
                If dtRow.Item("voiddatetime").ToString = Nothing Then
                    mdtVoiddatetime = Nothing
                Else
                    mdtVoiddatetime = dtRow.Item("voiddatetime")
                End If
                mstrVoidreason = dtRow.Item("voidreason").ToString
                'Sandeep [ 15 Oct 2010 ] -- Start
                'mintMembership_Categoryunkid = CInt(dtRow.Item("membership_categoryunkid"))
                If IsDBNull(dtRow.Item("membership_categoryunkid")) Then
                    mintMembership_Categoryunkid = 0
                Else
                    mintMembership_Categoryunkid = CInt(dtRow.Item("membership_categoryunkid"))
                End If
                'Sandeep [ 15 Oct 2010 ] -- End 
                'Sohail (14 Jun 2011) -- Start
                mblnIsapproved = CBool(dtRow.Item("isapproved"))
                mintApproveruserunkid = CInt(dtRow.Item("approveruserunkid"))
                'Sohail (14 Jun 2011) -- End
                mintPeriodunkid = CInt(dtRow.Item("periodunkid")) 'Sohail (13 Jan 2012)
                mstrMedicalrefno = dtRow.Item("medicalrefno").ToString 'Sohail (18 Jan 2012)

                'S.SANDEEP [ 07 NOV 2011 ] -- START
                'ENHANCEMENT : TRA CHANGES
                mintDisciplinefileunkid = CInt(dtRow.Item("disciplinefileunkid"))
                'S.SANDEEP [ 07 NOV 2011 ] -- END

                'S.SANDEEP [ 17 OCT 2012 ] -- START
                'ENHANCEMENT : TRA CHANGES
                mintMembershipTranUnkid = CInt(dtRow.Item("membershiptranunkid"))
                'S.SANDEEP [ 17 OCT 2012 ] -- END
                'Sohail (09 Nov 2013) -- Start
                'TRA - ENHANCEMENT
                If dtRow.Item("cumulative_startdate").ToString = Nothing Then
                    mdtCumulative_Startdate = Nothing
                Else
                    mdtCumulative_Startdate = dtRow.Item("cumulative_startdate")
                End If
                If dtRow.Item("stop_date").ToString = Nothing Then
                    mdtStop_Date = Nothing
                Else
                    mdtStop_Date = dtRow.Item("stop_date")
                End If
                'Sohail (09 Nov 2013) -- End
                'Sohail (24 Aug 2019) -- Start
                'NMB Payroll UAT # TC016 - 76.1 - System should be able to accept future amendments (Future period) and prorate. It should be possible to set start date of the specific transaction and prorate based on the start date. It should also be possible to set start date of the future period.
                If dtRow.Item("edstart_date").ToString = Nothing Then
                    mdtEDStart_Date = Nothing
                Else
                    mdtEDStart_Date = dtRow.Item("edstart_date")
                End If
                'Sohail (24 Aug 2019) -- End

                'Sohail (24 Feb 2016) -- Start
                'Enhancement - Batch Posting changes as per online doc in 58.1 SP. (#15 (KBC & Kenya Project Comments List.xls))
                mintEmpbatchpostingunkid = CInt(dtRow.Item("empbatchpostingunkid"))
                'Sohail (24 Feb 2016) -- End

                Exit For
            Next
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetData; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            'Sohail (01 Mar 2018) -- Start
            'Internal Issue : ExecureReader requires the command to have a transaction... in 70.1.
            'objDataOperation = Nothing
            If objDataOp Is Nothing Then objDataOperation = Nothing
            'Sohail (01 Mar 2018) -- End
        End Try
    End Sub

    'Sohail (25 Jun 2020) -- Start
    'NMB Enhancement # : Performance enhancement on Import earning deduction heads.
    Public Function GetDataIfExist(ByVal xDataOp As clsDataOperation _
                                   , ByVal strDatabaseName As String _
                                   , ByVal intEmployeeUnkID As Integer _
                                   , ByVal intTranHeadUnkID As Integer _
                                   , ByVal intPeriodUnkId As Integer _
                                   , Optional ByVal intEDUnkid As Integer = -1 _
                                   ) As Boolean

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        If xDataOp IsNot Nothing Then
            objDataOperation = xDataOp
        Else
            objDataOperation = New clsDataOperation
        End If

        objDataOperation.ClearParameters()

        Try
            strQ = "SELECT " & _
              "  edunkid " & _
              ", employeeunkid " & _
              ", tranheadunkid " & _
              ", batchtransactionunkid " & _
              ", amount " & _
              ", isdeduct " & _
              ", trnheadtype_id " & _
              ", typeof_id " & _
              ", calctype_id " & _
              ", computeon_id " & _
              ", formula " & _
              ", formulaid " & _
              ", currencyunkid " & _
              ", vendorunkid " & _
              ", userunkid " & _
              ", isvoid " & _
              ", voiduserunkid " & _
              ", voiddatetime " & _
              ", voidreason " & _
              ", membership_categoryunkid " & _
              ", ISNULL(isapproved, 0) AS isapproved " & _
              ", ISNULL(approveruserunkid, 0) AS approveruserunkid " & _
              ", ISNULL(periodunkid, 0) AS periodunkid " & _
              ", ISNULL(medicalrefno, '') AS medicalrefno " & _
              ", ISNULL(disciplinefileunkid,0) As disciplinefileunkid  " & _
              ", ISNULL(membershiptranunkid,0) AS membershiptranunkid " & _
              ", cumulative_startdate " & _
              ", stop_date " & _
              ", edstart_date " & _
              ", ISNULL(prearningdeduction_master.empbatchpostingunkid,0) AS empbatchpostingunkid " & _
             "FROM  " & strDatabaseName & "..prearningdeduction_master " & _
             "WHERE employeeunkid = @empId " & _
             "AND tranheadunkid = @tranId " & _
             "AND ISNULL(isvoid,0) = 0 "

            If intEDUnkid > 0 Then
                strQ &= " AND edunkid <> @edunkid"
                objDataOperation.AddParameter("@edunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intEDUnkid)
            End If

            If intPeriodUnkId > 0 Then
                strQ &= " AND periodunkid = @periodunkid "
                objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intPeriodUnkId)
            End If

            objDataOperation.AddParameter("@empId", SqlDbType.Int, eZeeDataType.INT_SIZE, intEmployeeUnkID)
            objDataOperation.AddParameter("@tranId", SqlDbType.Int, eZeeDataType.INT_SIZE, intTranHeadUnkID)


            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For Each dtRow As DataRow In dsList.Tables(0).Rows
                mintEdunkid = CInt(dtRow.Item("edunkid"))
                mintEmployeeunkid = CInt(dtRow.Item("employeeunkid"))
                mintTranheadunkid = CInt(dtRow.Item("tranheadunkid"))
                mintBatchtransactionunkid = CInt(dtRow.Item("batchtransactionunkid"))
                mdecAmount = CDec(dtRow.Item("amount")) 'Sohail (11 May 2011)
                mblnIsdeduct = CBool(dtRow.Item("isdeduct"))
                mintTrnheadtype_Id = CInt(dtRow.Item("trnheadtype_id"))
                mintTypeof_Id = CInt(dtRow.Item("typeof_id"))
                mintCalctype_Id = CInt(dtRow.Item("calctype_id"))
                mintComputeon_Id = CInt(dtRow.Item("computeon_id"))
                mstrFormula = dtRow.Item("formula").ToString
                mstrFormulaid = dtRow.Item("formulaid").ToString
                mintCurrencyunkid = CInt(dtRow.Item("currencyunkid"))
                mintVendorunkid = CInt(dtRow.Item("vendorunkid"))
                mintUserunkid = CInt(dtRow.Item("userunkid"))
                mblnIsvoid = CBool(dtRow.Item("isvoid"))
                mintVoiduserunkid = CInt(dtRow.Item("voiduserunkid"))
                If dtRow.Item("voiddatetime").ToString = Nothing Then
                    mdtVoiddatetime = Nothing
                Else
                    mdtVoiddatetime = dtRow.Item("voiddatetime")
                End If
                mstrVoidreason = dtRow.Item("voidreason").ToString
                If IsDBNull(dtRow.Item("membership_categoryunkid")) Then
                    mintMembership_Categoryunkid = 0
                Else
                    mintMembership_Categoryunkid = CInt(dtRow.Item("membership_categoryunkid"))
                End If
                mblnIsapproved = CBool(dtRow.Item("isapproved"))
                mintApproveruserunkid = CInt(dtRow.Item("approveruserunkid"))
                mintPeriodunkid = CInt(dtRow.Item("periodunkid"))
                mstrMedicalrefno = dtRow.Item("medicalrefno").ToString

                mintDisciplinefileunkid = CInt(dtRow.Item("disciplinefileunkid"))

                mintMembershipTranUnkid = CInt(dtRow.Item("membershiptranunkid"))
                If dtRow.Item("cumulative_startdate").ToString = Nothing Then
                    mdtCumulative_Startdate = Nothing
                Else
                    mdtCumulative_Startdate = dtRow.Item("cumulative_startdate")
                End If
                If dtRow.Item("stop_date").ToString = Nothing Then
                    mdtStop_Date = Nothing
                Else
                    mdtStop_Date = dtRow.Item("stop_date")
                End If
                If dtRow.Item("edstart_date").ToString = Nothing Then
                    mdtEDStart_Date = Nothing
                Else
                    mdtEDStart_Date = dtRow.Item("edstart_date")
                End If
                mintEmpbatchpostingunkid = CInt(dtRow.Item("empbatchpostingunkid"))

                Exit For
            Next

            Return dsList.Tables(0).Rows.Count > 0

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetDataIfExist; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xDataOp Is Nothing Then objDataOperation = Nothing
        End Try
    End Function
    'Sohail (25 Jun 2020) -- End

    'Sohail (15 Dec 2018) -- Start
    'HJFMRI Enhancement - Copy Selected Period ED Slab option on Global Assign ED screen in 75.1.
    Private Sub GetData(ByVal dtRow As DataRow)

        Dim exForce As Exception

        Try

            mintEdunkid = CInt(dtRow.Item("edunkid"))
            mintEmployeeunkid = CInt(dtRow.Item("employeeunkid"))
            mintTranheadunkid = CInt(dtRow.Item("tranheadunkid"))
            mintBatchtransactionunkid = CInt(dtRow.Item("batchtransactionunkid"))
            mdecAmount = CDec(dtRow.Item("amount"))
            mblnIsdeduct = CBool(dtRow.Item("isdeduct"))
            mintTrnheadtype_Id = CInt(dtRow.Item("trnheadtype_id"))
            mintTypeof_Id = CInt(dtRow.Item("typeof_id"))
            mintCalctype_Id = CInt(dtRow.Item("calctype_id"))
            mintComputeon_Id = CInt(dtRow.Item("computeon_id"))
            mstrFormula = dtRow.Item("formula").ToString
            mstrFormulaid = dtRow.Item("formulaid").ToString
            mintCurrencyunkid = CInt(dtRow.Item("currencyid"))
            mintVendorunkid = CInt(dtRow.Item("vendorid"))
            mintUserunkid = CInt(dtRow.Item("userunkid"))
            mblnIsvoid = CBool(dtRow.Item("isvoid"))
            mintVoiduserunkid = CInt(dtRow.Item("voiduserunkid"))
            If IsDBNull(dtRow.Item("voiddatetime")) = True Then
                mdtVoiddatetime = Nothing
            Else
                mdtVoiddatetime = dtRow.Item("voiddatetime")
            End If
            mstrVoidreason = dtRow.Item("voidreason").ToString
            If IsDBNull(dtRow.Item("membership_categoryunkid")) Then
                mintMembership_Categoryunkid = 0
            Else
                mintMembership_Categoryunkid = CInt(dtRow.Item("membership_categoryunkid"))
            End If
            mblnIsapproved = CBool(dtRow.Item("isapproved"))
            mintApproveruserunkid = CInt(dtRow.Item("approveruserunkid"))
            mintPeriodunkid = CInt(dtRow.Item("periodunkid"))
            mstrMedicalrefno = dtRow.Item("medicalrefno").ToString
            mintDisciplinefileunkid = CInt(dtRow.Item("disciplinefileunkid"))
            mintMembershipTranUnkid = CInt(dtRow.Item("membershiptranunkid"))

            If IsDBNull(dtRow.Item("cumulative_startdate")) = True Then
                mdtCumulative_Startdate = Nothing
            Else
                mdtCumulative_Startdate = dtRow.Item("cumulative_startdate")
            End If
            If IsDBNull(dtRow.Item("stop_date")) = True Then
                mdtStop_Date = Nothing
            Else
                mdtStop_Date = dtRow.Item("stop_date")
            End If
            'Sohail (24 Aug 2019) -- Start
            'NMB Payroll UAT # TC016 - 76.1 - System should be able to accept future amendments (Future period) and prorate. It should be possible to set start date of the specific transaction and prorate based on the start date. It should also be possible to set start date of the future period.
            If IsDBNull(dtRow.Item("edstart_date")) = True Then
                mdtEDStart_Date = Nothing
            Else
                mdtEDStart_Date = dtRow.Item("edstart_date")
            End If
            'Sohail (24 Aug 2019) -- End

            mintEmpbatchpostingunkid = CInt(dtRow.Item("empbatchpostingunkid"))

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetData:dtRow; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
        End Try
    End Sub
    'Sohail (15 Dec 2018) -- End

    'Sohail (13 Jan 2012) -- Start
    'TRA - ENHANCEMENT
    'Sohail (01 Mar 2018) -- Start
    'Internal Issue : ExecureReader requires the command to have a transaction... in 70.1.
    'Public Sub GetEmployeeED(ByVal strDatabaseName As String)
    Public Sub GetEmployeeED(ByVal objDataOp As clsDataOperation, ByVal strDatabaseName As String, Optional ByVal strEmpIDs As String = "", Optional ByVal blnLastTwoSlabOnly As Boolean = False, Optional ByVal dtAsOnDate As Date = Nothing)
        'Sohail (25 Jun 2020) - [strEmpIDs, blnLastTwoSlabOnly, dtAsOnDate]
        'Sohail (01 Mar 2018) -- End
        'Sohail (21 Aug 2015) - [strDatabaseName]

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        'S.SANDEEP [19 OCT 2016] -- START
        'ISSUE : EMPLOYEE MASTER BIND TRANSACTION ISSUE
        'Sohail (01 Mar 2018) -- Start
        'Internal Issue : ExecureReader requires the command to have a transaction... in 70.1.
        'If objDataOperation IsNot Nothing Then
        '    objDataOperation = objDataOperation
        'Else
        '    objDataOperation = New clsDataOperation
        'End If
        If objDataOp IsNot Nothing Then
            objDataOperation = objDataOp
        Else
        objDataOperation = New clsDataOperation
        End If
        'Sohail (01 Mar 2018) -- End
        objDataOperation.ClearParameters()
        'S.SANDEEP [19 OCT 2016] -- END

        'Sohail (21 Jul 2012) -- Start
        'TRA - ENHANCEMENT
        'Sohail (21 Aug 2015) -- Start
        'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
        'If mstrDatabaseName.Trim = "" Then mstrDatabaseName = FinancialYear._Object._DatabaseName
        mstrDatabaseName = strDatabaseName
        'Sohail (21 Aug 2015) -- End
        'Sohail (21 Jul 2012) -- End

        Try
            'Sohail (25 Jun 2020) -- Start
            'NMB Enhancement # : Performance enhancement on Import earning deduction heads.
            If dtAsOnDate = Nothing Then dtAsOnDate = DateTime.Now

            strQ = "SELECT * FROM ( "
            'Sohail (25 Jun 2020) -- End

            strQ &= "SELECT " & _
              "  prearningdeduction_master.edunkid " & _
              ", prearningdeduction_master.employeeunkid " & _
              ", hremployee_master.employeecode " & _
              ", REPLACE(ISNULL(hremployee_master.firstname, '') + ' ' + ISNULL(hremployee_master.othername, '') + ' ' + ISNULL(hremployee_master.surname, ''), '  ', ' ') AS employeename " & _
              ", prearningdeduction_master.tranheadunkid " & _
              ", prtranhead_master.trnheadname " & _
              ", prearningdeduction_master.batchtransactionunkid " & _
              ", prearningdeduction_master.amount " & _
              ", prearningdeduction_master.isdeduct " & _
              ", prtranhead_master.trnheadtype_id " & _
              ", prtranhead_master.typeof_id " & _
              ", prtranhead_master.calctype_id " & _
              ", prtranhead_master.computeon_id " & _
              ", prtranhead_master.formula " & _
              ", prtranhead_master.formulaid " & _
              ", prtranhead_master.isrecurrent " & _
              ", prearningdeduction_master.currencyunkid " & _
              ", prearningdeduction_master.vendorunkid " & _
              ", prearningdeduction_master.userunkid " & _
              ", prearningdeduction_master.isvoid " & _
              ", prearningdeduction_master.voiduserunkid " & _
              ", prearningdeduction_master.voiddatetime " & _
              ", prearningdeduction_master.voidreason " & _
              ", prearningdeduction_master.membership_categoryunkid " & _
              ", ISNULL(prearningdeduction_master.isapproved, 0) AS isapproved " & _
              ", ISNULL(prearningdeduction_master.approveruserunkid, 0) AS approveruserunkid " & _
              ", ISNULL(prearningdeduction_master.periodunkid, 0) AS periodunkid " & _
              ", ISNULL(cfcommon_period_tran.period_name, '') AS period_name " & _
              ", ISNULL(CONVERT(CHAR(8), cfcommon_period_tran.start_date,112), '19000101') AS start_date " & _
              ", ISNULL(CONVERT(CHAR(8), cfcommon_period_tran.end_date,112), '19000101') AS end_date " & _
              ", ISNULL(prearningdeduction_master.medicalrefno, '') AS medicalrefno " & _
              ", 0 AS costcenterunkid " & _
              ", ISNULL(prearningdeduction_master.membershiptranunkid,0) AS membershiptranunkid " & _
              ",ISNULL(prearningdeduction_master.disciplinefileunkid,0) AS disciplinefileunkid " & _
              ", cumulative_startdate " & _
              ", ISNULL(prearningdeduction_master.empbatchpostingunkid,0) AS empbatchpostingunkid " & _
              ", stop_date " & _
              ", edstart_date " & _
              ", '' AS AUD "

            'Sohail (25 Jun 2020) -- Start
            'NMB Enhancement # : Performance enhancement on Import earning deduction heads.
            If blnLastTwoSlabOnly = True Then
                strQ &= ", DENSE_RANK() OVER(PARTITION BY prearningdeduction_master.employeeunkid ORDER BY cfcommon_period_tran.end_date DESC) AS ROWNO "
            End If
            'Sohail (25 Jun 2020) -- End

            strQ &= "FROM " & mstrDatabaseName & "..prearningdeduction_master " & _
             "LEFT JOIN " & mstrDatabaseName & "..prtranhead_master ON prearningdeduction_master.tranheadunkid = prtranhead_master.tranheadunkid " & _
             "LEFT JOIN " & mstrDatabaseName & "..cfcommon_period_tran ON prearningdeduction_master.periodunkid = cfcommon_period_tran.periodunkid " & _
             "LEFT JOIN " & mstrDatabaseName & "..hremployee_master ON prearningdeduction_master.employeeunkid = hremployee_master.employeeunkid " & _
             "WHERE ISNULL(prearningdeduction_master.isvoid, 0) = 0 " & _
             "  AND prtranhead_master.isvoid = 0 "

            'Sohail (25 Jun 2020) -- Start
            'NMB Enhancement # : Performance enhancement on Import earning deduction heads.
            If mintEmployeeunkid > 0 Then
                strQ &= "AND prearningdeduction_master.employeeunkid = @employeeunkid "
            End If

            If strEmpIDs.Trim <> "" Then
                strQ &= "AND prearningdeduction_master.employeeunkid IN (" & strEmpIDs & ") "
            End If

            If blnLastTwoSlabOnly = True Then
                strQ &= " AND CONVERT(CHAR(8), cfcommon_period_tran.end_date, 112) <= @end_date "
                objDataOperation.AddParameter("@end_date", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(dtAsOnDate))
            End If
            'Sohail (25 Jun 2020) -- End

            'Sohail (31 Oct 2019) - [employeecode, employeename, LEFT JOIN hremployee_master]
            'Sohail (24 Aug 2019) - [edstart_date]
            'Sohail (24 Feb 2016) - [empbatchpostingunkid]
            'Shani(19-Dec-2015) -- ["  AND prtranhead_master.isvoid = 0 " & _]

            'Sohail (09 Nov 2013) - [cumulative_startdate, stop_date]
            'Sohail (18 Jan 2012) - [isrecurrent]
            'S.SANDEEP [ 11 DEC 2012 ] -- START {membershiptranunkid} -- END
            'S.SANDEEP [ 17 DEC 2012 ] -- START {disciplinefileunkid} -- END

            'Sohail (25 Jun 2020) -- Start
            'NMB Enhancement # : Performance enhancement on Import earning deduction heads.
            strQ &= " ) AS A WHERE 1 = 1 "

            If blnLastTwoSlabOnly = True Then
                strQ &= " AND A.ROWNO In (1, 2) "
            End If
            'Sohail (25 Jun 2020) -- End

            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeunkid.ToString)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Dim dRow As DataRow
            mdtED.Rows.Clear()

            For Each dtRow As DataRow In dsList.Tables(0).Rows
                dRow = mdtED.NewRow

                dRow.Item("edunkid") = CInt(dtRow.Item("edunkid"))
                dRow.Item("employeeunkid") = CInt(dtRow.Item("employeeunkid"))
                dRow.Item("tranheadunkid") = CInt(dtRow.Item("tranheadunkid"))
                dRow.Item("trnheadname") = dtRow.Item("trnheadname").ToString
                dRow.Item("batchtransactionunkid") = CInt(dtRow.Item("batchtransactionunkid"))
                dRow.Item("amount") = CDec(dtRow.Item("amount"))
                dRow.Item("isdeduct") = CBool(dtRow.Item("isdeduct"))
                dRow.Item("trnheadtype_id") = CInt(dtRow.Item("trnheadtype_id"))
                dRow.Item("typeof_id") = CInt(dtRow.Item("typeof_id"))
                dRow.Item("calctype_id") = CInt(dtRow.Item("calctype_id"))
                dRow.Item("computeon_id") = CInt(dtRow.Item("computeon_id"))
                dRow.Item("formula") = dtRow.Item("formula").ToString
                dRow.Item("formulaid") = dtRow.Item("formulaid").ToString
                dRow.Item("currencyid") = CInt(dtRow.Item("currencyunkid"))
                dRow.Item("vendorid") = CInt(dtRow.Item("vendorunkid"))
                dRow.Item("userunkid") = CInt(dtRow.Item("userunkid"))
                dRow.Item("isvoid") = CBool(dtRow.Item("isvoid"))
                dRow.Item("voiduserunkid") = CInt(dtRow.Item("voiduserunkid"))
                dRow.Item("voiddatetime") = dtRow.Item("voiddatetime")
                dRow.Item("voidreason") = dtRow.Item("voidreason").ToString
                If IsDBNull(dtRow.Item("membership_categoryunkid")) Then
                    dRow.Item("membership_categoryunkid") = 0
                Else
                    dRow.Item("membership_categoryunkid") = CInt(dtRow.Item("membership_categoryunkid"))
                End If
                dRow.Item("isapproved") = CBool(dtRow.Item("isapproved"))
                dRow.Item("approveruserunkid") = CInt(dtRow.Item("approveruserunkid"))
                dRow.Item("periodunkid") = CInt(dtRow.Item("periodunkid"))
                dRow.Item("period_name") = dtRow.Item("period_name").ToString
                dRow.Item("start_date") = dtRow.Item("start_date").ToString
                dRow.Item("end_date") = dtRow.Item("end_date").ToString

                dRow.Item("AUD") = dtRow.Item("AUD").ToString
                'Sohail (18 Jan 2012) -- Start
                'TRA - ENHANCEMENT
                dRow.Item("isrecurrent") = CBool(dtRow.Item("isrecurrent"))
                dRow.Item("costcenterunkid") = CInt(dtRow.Item("costcenterunkid"))
                dRow.Item("medicalrefno") = dtRow.Item("medicalrefno").ToString
                'Sohail (18 Jan 2012) -- End

                'S.SANDEEP [ 11 DEC 2012 ] -- START
                'ENHANCEMENT : TRA CHANGES
                dRow.Item("membershiptranunkid") = CInt(dtRow.Item("membershiptranunkid"))
                'S.SANDEEP [ 11 DEC 2012 ] -- END

                'S.SANDEEP [ 17 DEC 2012 ] -- START
                'ENHANCEMENT : TRA CHANGES
                dRow.Item("disciplinefileunkid") = CInt(dtRow.Item("disciplinefileunkid"))
                'S.SANDEEP [ 17 DEC 2012 ] -- END
                'Sohail (09 Nov 2013) -- Start
                'TRA - ENHANCEMENT
                dRow.Item("cumulative_startdate") = dtRow.Item("cumulative_startdate")
                dRow.Item("stop_date") = dtRow.Item("stop_date")
                'Sohail (09 Nov 2013) -- End
                'Sohail (24 Aug 2019) -- Start
                'NMB Payroll UAT # TC016 - 76.1 - System should be able to accept future amendments (Future period) and prorate. It should be possible to set start date of the specific transaction and prorate based on the start date. It should also be possible to set start date of the future period.
                dRow.Item("edstart_date") = dtRow.Item("edstart_date")
                'Sohail (24 Aug 2019) -- End

                'Sohail (24 Feb 2016) -- Start
                'Enhancement - Batch Posting changes as per online doc in 58.1 SP. (#15 (KBC & Kenya Project Comments List.xls))
                dRow.Item("empbatchpostingunkid") = CInt(dtRow.Item("empbatchpostingunkid"))
                'Sohail (24 Feb 2016) -- End
                'Sohail (31 Oct 2019) -- Start
                'Mainspring Resourcing Ghana : Support Issue Id # 0004199 - Email notification not sent to approvers when there is uploaded, edited e & d.
                dRow.Item("employeecode") = dtRow.Item("employeecode").ToString
                dRow.Item("employeename") = dtRow.Item("employeename").ToString
                'Sohail (31 Oct 2019) -- End

                mdtED.Rows.Add(dRow)
            Next

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetEmployeeED; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()

            'S.SANDEEP [19 OCT 2016] -- START
            'ISSUE : EMPLOYEE MASTER BIND TRANSACTION ISSUE
            'objDataOperation = Nothing
            'Sohail (01 Mar 2018) -- Start
            'Internal Issue : ExecureReader requires the command to have a transaction... in 70.1.
            'If objDataOperation Is Nothing Then objDataOperation = Nothing
            If objDataOp Is Nothing Then objDataOperation = Nothing
            'Sohail (01 Mar 2018) -- End
            'S.SANDEEP [19 OCT 2016] -- END
        End Try
    End Sub
    'Sohail (13 Jan 2012) -- End

    'Sohail (21 Aug 2015) -- Start
    'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
    '''' <summary>
    '''' Modify By: Suhail
    '''' </summary>
    '''' <purpose> Assign all Property variable </purpose>
    '''' <param name="intPeriodID">Period ID for Employee Exemption to Check Head is exempted or not.</param>
    '''' <param name="dtPeriodEndDate">To show only current Earning Deduction Slab.</param>
    'Public Function GetList(Optional ByVal strTableName As String = "List", Optional ByVal strEmpUnkIDs As String = "", _
    '                        Optional ByVal intDeptID As Integer = 0, _
    '                        Optional ByVal intSectionID As Integer = 0, _
    '                        Optional ByVal intUnitID As Integer = 0, _
    '                        Optional ByVal intGradeID As Integer = 0, _
    '                        Optional ByVal intAccessID As Integer = 0, _
    '                        Optional ByVal intClassID As Integer = 0, _
    '                        Optional ByVal intCostCenterID As Integer = 0, _
    '                        Optional ByVal intServiceID As Integer = 0, _
    '                        Optional ByVal intJobID As Integer = 0, _
    '                        Optional ByVal intPayPointID As Integer = 0, _
    '                        Optional ByVal strSortField As String = "", _
    '                        Optional ByVal intPeriodID As Integer = 0, _
    '                        Optional ByVal intEDHeadApprovalStatus As Integer = enEDHeadApprovalStatus.All, _
    '                        Optional ByVal intTranHeadUnkID As Integer = 0, _
    '                        Optional ByVal blnIncludeInActiveEmployee As Boolean = True, _
    '                        Optional ByVal mstrAdvanceFilter As String = "" _
    '                        , Optional ByVal dtPeriodEndDate As DateTime = Nothing, Optional ByVal intdisciplinefileunkid As Integer = -1 _
    '                        , Optional ByVal strEmployeeAsOnDate As String = "" _
    '                        , Optional ByVal strUserAccessLevelFilterString As String = "" _
    '                        , Optional ByVal objDataOperation As clsDataOperation = Nothing) As DataSet '[intEDHeadApprovalStatus, intTranHeadUnkID - Sohail (14 Jun 2011)], 'Sohail (12 Oct 2011) - [blnIncludeInActiveEmployee]'S.SANDEEP [ 07 NOV 2011 ][intdisciplinefileunkid], 'Sohail (23 Apr 2012) - [strEmployeeAsOnDate,strUserAccessLevelFilterString]
    '    'Sohail (30 Mar 2013) - [(Optional ByVal intEmpUnkid As Integer = 0) is replaced with (Optional ByVal strEmpUnkIDs As String = "")]

    '    Dim dsList As DataSet = Nothing
    '    Dim strQ As String = ""
    '    Dim exForce As Exception

    '    'Sohail (23 Apr 2012) -- Start
    '    'TRA - ENHANCEMENT - Issue in bind transaction from employee master in Web
    '    'Dim objDataOperation As New clsDataOperation
    '    If objDataOperation Is Nothing Then
    '        objDataOperation = New clsDataOperation
    '    End If
    '    'Sohail (23 Apr 2012) -- End

    '    'Sohail (21 Jul 2012) -- Start
    '    'TRA - ENHANCEMENT
    '    If mstrDatabaseName.Trim = "" Then mstrDatabaseName = FinancialYear._Object._DatabaseName
    '    'Sohail (21 Jul 2012) -- End

    '    Try
    '        'Sohail (03 Nov 2010) -- Start
    '        'Changes : membershipname field added.
    '        'strQ = "SELECT " & _
    '        '  "  isexempt = CASE ISNULL(premployee_exemption_tran.tranheadunkid,0) WHEN 0 THEN 0 ELSE 1 END " & _
    '        '  ", prearningdeduction_master.edunkid " & _
    '        '  ", prearningdeduction_master.employeeunkid " & _
    '        '  ", hremployee_master.firstname + ' ' + hremployee_master.othername + ' ' + hremployee_master.surname AS employeename " & _
    '        '  ", prearningdeduction_master.tranheadunkid " & _
    '        '              ", prtranhead_master.trnheadname " & _
    '        '  ", prearningdeduction_master.batchtransactionunkid " & _
    '        '  ", prearningdeduction_master.amount " & _
    '        '  ", prearningdeduction_master.isdeduct " & _
    '        '              ", prtranhead_master.trnheadtype_id " & _
    '        '              ", prtranhead_master.typeof_id " & _
    '        '              ", prtranhead_master.calctype_id " & _
    '        '              ", prtranhead_master.computeon_id " & _
    '        '              ", prtranhead_master.formula " & _
    '        '              ", prtranhead_master.formulaid " & _
    '        '  ", prearningdeduction_master.currencyunkid " & _
    '        '  ", prearningdeduction_master.vendorunkid " & _
    '        '  ", prearningdeduction_master.userunkid " & _
    '        '  ", prearningdeduction_master.isvoid " & _
    '        '  ", prearningdeduction_master.voiduserunkid " & _
    '        '  ", prearningdeduction_master.voiddatetime " & _
    '        '  ", prearningdeduction_master.voidreason " & _
    '        '  ", prearningdeduction_master.membership_categoryunkid " & _
    '        '"FROM prearningdeduction_master " & _
    '        '    "LEFT JOIN prtranhead_master ON prearningdeduction_master.tranheadunkid = prtranhead_master.tranheadunkid " & _
    '        '    "LEFT JOIN hremployee_master ON prearningdeduction_master.employeeunkid = hremployee_master.employeeunkid " & _
    '        '"LEFT JOIN premployee_exemption_tran " & _
    '        '"ON prearningdeduction_master.employeeunkid = premployee_exemption_tran.employeeunkid " & _
    '        '"AND prearningdeduction_master.tranheadunkid = premployee_exemption_tran.tranheadunkid " & _
    '        '"AND prearningdeduction_master.isvoid = premployee_exemption_tran.isvoid " & _
    '        '"AND premployee_exemption_tran.periodunkid = @periodunkid " & _
    '        '"WHERE ISNULL(prearningdeduction_master.isvoid,0) = 0 " 'Sohail (16 Oct 2010)

    '        'Sohail (27 Aug 2012) -- Start
    '        'TRA - ENHANCEMENT
    '        strQ = "SELECT  isexempt " & _
    '                      ", edunkid " & _
    '                      ", employeeunkid " & _
    '                      ", employeecode " & _
    '                      ", employeename " & _
    '                      ", tranheadunkid " & _
    '                      ", trnheadname " & _
    '                      ", trnheadcode " & _
    '                      ", batchtransactionunkid " & _
    '                      ", amount " & _
    '                      ", isdeduct " & _
    '                      ", trnheadtype_id " & _
    '                      ", typeof_id " & _
    '                      ", calctype_id " & _
    '                      ", computeon_id " & _
    '                      ", formula " & _
    '                      ", formulaid " & _
    '                      ", currencyunkid " & _
    '                      ", vendorunkid " & _
    '                      ", userunkid " & _
    '                      ", isvoid " & _
    '                      ", voiduserunkid " & _
    '                      ", voiddatetime " & _
    '                      ", voidreason " & _
    '                      ", membership_categoryunkid " & _
    '                      ", membershipname " & _
    '                      ", isapproved " & _
    '                      ", approveruserunkid " & _
    '                      ", periodunkid " & _
    '                      ", period_name " & _
    '                      ", start_date " & _
    '                      ", end_date " & _
    '                      ", medicalrefno " & _
    '                      ", disciplinefileunkid " & _
    '                      ", membershiptranunkid " & _
    '                      ", cumulative_startdate " & _
    '                      ", stop_date " & _
    '                "FROM    ( "
    '        'Sohail (09 Nov 2013) - [cumulative_startdate, stop_date]
    '        'Sohail (27 Aug 2012) -- End

    '        strQ &= "SELECT  isexempt = CASE ISNULL(premployee_exemption_tran.tranheadunkid, 0) " & _
    '                                 "WHEN 0 THEN 0 ELSE 1 END " & _
    '  ", prearningdeduction_master.edunkid " & _
    '  ", prearningdeduction_master.employeeunkid " & _
    '          ", hremployee_master.employeecode " & _
    '  ", hremployee_master.firstname + ' ' + hremployee_master.othername + ' ' + hremployee_master.surname AS employeename " & _
    '  ", prearningdeduction_master.tranheadunkid " & _
    '              ", prtranhead_master.trnheadname " & _
    '                      ", prtranhead_master.trnheadcode " & _
    '  ", prearningdeduction_master.batchtransactionunkid " & _
    '  ", prearningdeduction_master.amount " & _
    '  ", prearningdeduction_master.isdeduct " & _
    '              ", prtranhead_master.trnheadtype_id " & _
    '              ", prtranhead_master.typeof_id " & _
    '              ", prtranhead_master.calctype_id " & _
    '              ", prtranhead_master.computeon_id " & _
    '              ", prtranhead_master.formula " & _
    '              ", prtranhead_master.formulaid " & _
    '  ", prearningdeduction_master.currencyunkid " & _
    '  ", prearningdeduction_master.vendorunkid " & _
    '  ", prearningdeduction_master.userunkid " & _
    '  ", prearningdeduction_master.isvoid " & _
    '  ", prearningdeduction_master.voiduserunkid " & _
    '  ", prearningdeduction_master.voiddatetime " & _
    '  ", prearningdeduction_master.voidreason " & _
    '  ", prearningdeduction_master.membership_categoryunkid " & _
    '                  ", ISNULL(hrmembership_master.membershipname, '') AS membershipname " & _
    '                              ", ISNULL(prearningdeduction_master.isapproved, 0) AS isapproved " & _
    '                              ", ISNULL(prearningdeduction_master.approveruserunkid, 0) AS approveruserunkid " & _
    '                  ", ISNULL(prearningdeduction_master.periodunkid, 0) AS periodunkid " & _
    '                  ", ISNULL(cfcommon_period_tran.period_name, '') AS period_name " & _
    '                  ", ISNULL(CONVERT(CHAR(8), cfcommon_period_tran.start_date,112), '19000101') AS start_date " & _
    '                  ", ISNULL(CONVERT(CHAR(8), cfcommon_period_tran.end_date,112), '19000101') AS end_date " & _
    '                      ", ISNULL(prearningdeduction_master.medicalrefno, '') AS medicalrefno " & _
    '                  ", ISNULL(prearningdeduction_master.disciplinefileunkid,-1) AS disciplinefileunkid " & _
    '                  ", ISNULL(membershiptranunkid,0) AS membershiptranunkid " & _
    '                  ", cumulative_startdate " & _
    '                  ", stop_date " & _
    '                  ", DENSE_RANK() OVER ( PARTITION  BY prearningdeduction_master.employeeunkid ORDER BY cfcommon_period_tran.end_date DESC ) AS ROWNO " & _
    '        "FROM " & mstrDatabaseName & "..prearningdeduction_master " & _
    '                    "LEFT JOIN " & mstrDatabaseName & "..prtranhead_master ON prearningdeduction_master.tranheadunkid = prtranhead_master.tranheadunkid " & _
    '            "LEFT JOIN " & mstrDatabaseName & "..hremployee_master ON prearningdeduction_master.employeeunkid = hremployee_master.employeeunkid " & _
    '                    "LEFT JOIN " & mstrDatabaseName & "..hrmembership_master ON hrmembership_master.membershipunkid = prearningdeduction_master.vendorunkid " & _
    '                    "LEFT JOIN " & mstrDatabaseName & "..cfcommon_period_tran ON prearningdeduction_master.periodunkid = cfcommon_period_tran.periodunkid " & _
    '                    "LEFT JOIN " & mstrDatabaseName & "..premployee_exemption_tran ON prearningdeduction_master.employeeunkid = premployee_exemption_tran.employeeunkid " & _
    '"AND prearningdeduction_master.tranheadunkid = premployee_exemption_tran.tranheadunkid " & _
    '"AND prearningdeduction_master.isvoid = premployee_exemption_tran.isvoid " & _
    '        "AND premployee_exemption_tran.periodunkid = @periodunkid "
    '        'Sohail (09 Nov 2013) - [cumulative_startdate, stop_date]
    '        'Sohail (28 Jan 2012) - [employeecode]
    '        'S.SANDEEP [ 20 MARCH 2012 --> trnheadcode ] -- START -- END
    '        'S.SANDEEP [ 17 OCT 2012 ], [membershiptranunkid]



    '        'Sohail (27 Aug 2012) -- Start
    '        'TRA - ENHANCEMENT
    '        ''Sohail (13 Jan 2012) -- Start
    '        ''TRA - ENHANCEMENT (To show only current earning deduction slab)
    '        'If dtPeriodEndDate <> Nothing Then
    '        '    strQ &= "JOIN ( SELECT  CONVERT(CHAR(8), MAX(end_date), 112)   AS enddate, " & _
    '        '                                                    "employeeunkid " & _
    '        '                                          "FROM      " & mstrDatabaseName & "..prearningdeduction_master " & _
    '        '                                                    "LEFT JOIN " & mstrDatabaseName & "..cfcommon_period_tran ON prearningdeduction_master.periodunkid = cfcommon_period_tran.periodunkid " & _
    '        '                                          "WHERE     ISNULL(prearningdeduction_master.isvoid,0) = 0 " & _
    '        '                                                    "AND CONVERT(CHAR(8), cfcommon_period_tran.end_date, 112) <= @end_date "
    '        '    'Sohail (16 May 2012) -- Start
    '        '    'TRA - ENHANCEMENT
    '        '    If intEmpUnkid > 0 Then
    '        '        strQ &= "AND prearningdeduction_master.employeeunkid = @employeeunkiid "
    '        '    End If
    '        '    'Sohail (16 May 2012) -- End

    '        '    strQ &= "GROUP BY employeeunkid " & _
    '        '                                        ") AS a ON prearningdeduction_master.employeeunkid = a.employeeunkid " & _
    '        '                                                "AND CONVERT(CHAR(8), cfcommon_period_tran.end_date, 112) = a.enddate "

    '        '    objDataOperation.AddParameter("@end_date", SqlDbType.VarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(dtPeriodEndDate))
    '        'End If
    '        ''Sohail (13 Jan 2012) -- End
    '        'Sohail (27 Aug 2012) -- End

    '        strQ &= "WHERE ISNULL(prearningdeduction_master.isvoid, 0) = 0 " '[isapproved, approveruserunkid - Sohail (14 Jun 2011)]
    '        'Sohail (03 Nov 2010) -- End

    '        'Sohail (27 Aug 2012) -- Start
    '        'TRA - ENHANCEMENT
    '        If dtPeriodEndDate <> Nothing Then
    '            strQ &= "AND CONVERT(CHAR(8), cfcommon_period_tran.end_date, 112) <= @end_date "
    '            objDataOperation.AddParameter("@end_date", SqlDbType.VarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(dtPeriodEndDate))
    '        End If
    '        'Sohail (27 Aug 2012) -- End

    '        objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intPeriodID) 'Sohail (13 Jan 2012)

    '        'Anjan (09 Aug 2011)-Start
    '        'Issue : For including setting of acitve and inactive employee.
    '        'If ConfigParameter._Object._IsIncludeInactiveEmp = False Then
    '        '    strQ &= " AND ISNULL(hremployee_master.isactive,0) = 1 "
    '        'End If
    '        'Anjan (09 Aug 2011)-End 

    '        'Sohail (12 Oct 2011) -- Start
    '        If blnIncludeInActiveEmployee = False Then
    '            'Sohail (06 Jan 2012) -- Start
    '            'TRA - ENHANCEMENT
    '            'strQ &= " AND ISNULL(hremployee_master.isactive,0) = 1 "
    '            strQ &= " AND CONVERT(CHAR(8),hremployee_master.appointeddate,112) <= @enddate " & _
    '                       " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_from_date,112),@startdate) >= @startdate " & _
    '                       " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_to_date,112),@startdate) >= @startdate " & _
    '                       " AND ISNULL(CONVERT(CHAR(8),hremployee_master.empl_enddate,112), @startdate) >= @startdate " 'S.SANDEEP [ 15 MAY 2012 empl_enddate ] -- START -- END

    '            'Sohail (23 Apr 2012) -- Start
    '            'TRA - ENHANCEMENT
    '            'objDataOperation.AddParameter("@startdate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, ConfigParameter._Object._EmployeeAsOnDate)
    '            'objDataOperation.AddParameter("@enddate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, ConfigParameter._Object._EmployeeAsOnDate)
    '            objDataOperation.AddParameter("@startdate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, IIf(strEmployeeAsOnDate = "", ConfigParameter._Object._EmployeeAsOnDate, strEmployeeAsOnDate))
    '            objDataOperation.AddParameter("@enddate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, IIf(strEmployeeAsOnDate = "", ConfigParameter._Object._EmployeeAsOnDate, strEmployeeAsOnDate))
    '            'Sohail (23 Apr 2012) -- End
    '            'Sohail (06 Jan 2012) -- End
    '        End If
    '        'Sohail (12 Oct 2011) -- End

    '        'Sohail (24 Jun 2011) -- Start
    '        'Issue : According to privilege that lower level user should not see superior level employees.

    '        'S.SANDEEP [ 04 FEB 2012 ] -- START
    '        'ENHANCEMENT : TRA CHANGES
    '        'If UserAccessLevel._AccessLevel.Length > 0 Then
    '        '    strQ &= " AND hremployee_master.jobunkid IN (" & UserAccessLevel._AccessLevel & ") "
    '        'End If
    '        'Sohail (23 Apr 2012) -- Start
    '        'TRA - ENHANCEMENT
    '        'Select Case ConfigParameter._Object._UserAccessModeSetting
    '        '    Case enAllocation.BRANCH
    '        '        If UserAccessLevel._AccessLevel.Length > 0 Then
    '        '            strQ &= " AND  hremployee_master.stationunkid IN (" & UserAccessLevel._AccessLevel & ") "
    '        '        End If
    '        '    Case enAllocation.DEPARTMENT_GROUP
    '        '        If UserAccessLevel._AccessLevel.Length > 0 Then
    '        '            strQ &= " AND  hremployee_master.deptgroupunkid IN (" & UserAccessLevel._AccessLevel & ") "
    '        '        End If
    '        '    Case enAllocation.DEPARTMENT
    '        '        If UserAccessLevel._AccessLevel.Length > 0 Then
    '        '            strQ &= " AND  hremployee_master.departmentunkid IN (" & UserAccessLevel._AccessLevel & ") "
    '        '        End If
    '        '    Case enAllocation.SECTION_GROUP
    '        '        If UserAccessLevel._AccessLevel.Length > 0 Then
    '        '            strQ &= " AND  hremployee_master.sectiongroupunkid IN (" & UserAccessLevel._AccessLevel & ") "
    '        '        End If
    '        '    Case enAllocation.SECTION
    '        '        If UserAccessLevel._AccessLevel.Length > 0 Then
    '        '            strQ &= " AND  hremployee_master.sectionunkid IN (" & UserAccessLevel._AccessLevel & ") "
    '        '        End If
    '        '    Case enAllocation.UNIT_GROUP
    '        '        If UserAccessLevel._AccessLevel.Length > 0 Then
    '        '            strQ &= " AND  hremployee_master.unitgroupunkid IN (" & UserAccessLevel._AccessLevel & ") "
    '        '        End If
    '        '    Case enAllocation.UNIT
    '        '        If UserAccessLevel._AccessLevel.Length > 0 Then
    '        '            strQ &= " AND  hremployee_master.unitunkid IN (" & UserAccessLevel._AccessLevel & ") "
    '        '        End If
    '        '    Case enAllocation.TEAM
    '        '        If UserAccessLevel._AccessLevel.Length > 0 Then
    '        '            strQ &= " AND  hremployee_master.teamunkid IN (" & UserAccessLevel._AccessLevel & ") "
    '        '        End If
    '        '    Case enAllocation.JOB_GROUP
    '        '        If UserAccessLevel._AccessLevel.Length > 0 Then
    '        '            strQ &= " AND  hremployee_master.jobgroupunkid IN (" & UserAccessLevel._AccessLevel & ") "
    '        '        End If
    '        '    Case enAllocation.JOBS
    '        '        If UserAccessLevel._AccessLevel.Length > 0 Then
    '        '            strQ &= " AND hremployee_master.jobunkid IN (" & UserAccessLevel._AccessLevel & ") "
    '        '        End If
    '        'End Select
    '        If strUserAccessLevelFilterString = "" Then
    '            strQ &= UserAccessLevel._AccessLevelFilterString
    '        Else
    '            strQ &= strUserAccessLevelFilterString
    '        End If
    '        'Sohail (23 Apr 2012) -- End
    '        'S.SANDEEP [ 04 FEB 2012 ] -- END

    '        'Sohail (24 Jun 2011) -- End

    '        'Sohail (30 Mar 2013) -- Start
    '        'TRA - ENHANCEMENT
    '        'If intEmpUnkid > 0 Then
    '        '    strQ &= "AND prearningdeduction_master.employeeunkid = @employeeunkiid "
    '        '    objDataOperation.AddParameter("@employeeunkiid", SqlDbType.Int, eZeeDataType.INT_SIZE, intEmpUnkid)
    '        'End If
    '        If strEmpUnkIDs.Trim <> "" AndAlso strEmpUnkIDs.Trim <> "0" AndAlso strEmpUnkIDs.Trim <> "-1" Then
    '            strQ &= "AND prearningdeduction_master.employeeunkid IN (" & strEmpUnkIDs & ") "
    '        End If
    '        'Sohail (30 Mar 2013) -- End

    '        'Sohail (14 Jun 2011) -- Start
    '        'Changes : New paramenter for new field 'IsApproved'
    '        If intEDHeadApprovalStatus = enEDHeadApprovalStatus.Approved Then
    '            strQ &= " AND prearningdeduction_master.isapproved = @isapproved "
    '            objDataOperation.AddParameter("@isapproved", SqlDbType.Int, eZeeDataType.INT_SIZE, intEDHeadApprovalStatus)
    '        ElseIf intEDHeadApprovalStatus = enEDHeadApprovalStatus.Pending Then
    '            strQ &= " AND prearningdeduction_master.isapproved = @isapproved "
    '            objDataOperation.AddParameter("@isapproved", SqlDbType.Int, eZeeDataType.INT_SIZE, 0)
    '        End If

    '        'Sohail (12 Apr 2013) -- Start
    '        'TRA - ENHANCEMENT
    '        'If intTranHeadUnkID > 0 Then
    '        '    strQ &= " AND prearningdeduction_master.tranheadunkid = @tranheadunkid "
    '        '    objDataOperation.AddParameter("@tranheadunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intTranHeadUnkID)
    '        'End If
    '        'Sohail (12 Apr 2013) -- End
    '        'Sohail (14 Jun 2011) -- End

    '        '================ PayPoint
    '        If intPayPointID > 0 Then
    '            strQ += " AND hremployee_master.paypointunkid = @paypointunkid"
    '            objDataOperation.AddParameter("@paypointunkid", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, intPayPointID)
    '        End If

    '        '================ Department
    '        If intDeptID > 0 Then
    '            strQ += " AND hremployee_master.departmentunkid = @departmentunkid"
    '            objDataOperation.AddParameter("@departmentunkid", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, intDeptID)
    '        End If

    '        '================ Section
    '        If intSectionID > 0 Then
    '            strQ += " AND hremployee_master.sectionunkid = @sectionunkid"
    '            objDataOperation.AddParameter("@sectionunkid", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, intSectionID)
    '        End If

    '        '================ Unit
    '        If intUnitID > 0 Then
    '            strQ += " AND hremployee_master.unitunkid = @unitunkid"
    '            objDataOperation.AddParameter("@unitunkid", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, intUnitID)
    '        End If

    '        '================ Job
    '        If intJobID > 0 Then
    '            strQ += " AND hremployee_master.jobunkid = @jobunkid"
    '            objDataOperation.AddParameter("@jobunkid", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, intJobID)
    '        End If

    '        '================ Grade
    '        If intGradeID > 0 Then
    '            strQ += " AND hremployee_master.gradeunkid = @gradeunkid"
    '            objDataOperation.AddParameter("@gradeunkid", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, intGradeID)
    '        End If

    '        '================ Access
    '        If intAccessID > 0 Then
    '            strQ += " AND hremployee_master.accessunkid = @accessunkid"
    '            objDataOperation.AddParameter("@accessunkid", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, intAccessID)
    '        End If

    '        '================ Class
    '        If intClassID > 0 Then
    '            strQ += " AND hremployee_master.classunkid = @classunkid"
    '            objDataOperation.AddParameter("@classunkid", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, intClassID)
    '        End If

    '        '================ Service
    '        If intServiceID > 0 Then
    '            strQ += " AND hremployee_master.serviceunkid = @serviceunkid"
    '            objDataOperation.AddParameter("@serviceunkid", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, intServiceID)
    '        End If

    '        '================ CostCenter
    '        If intCostCenterID > 0 Then
    '            strQ += " AND hremployee_master.costcenterunkid = @costcenterunkid"
    '            objDataOperation.AddParameter("@costcenterunkid", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, intCostCenterID)
    '        End If

    '        'S.SANDEEP [ 07 NOV 2011 ] -- START
    '        'ENHANCEMENT : TRA CHANGES
    '        If intdisciplinefileunkid > 0 Then
    '            strQ &= " AND ISNULL(prearningdeduction_master.disciplinefileunkid,0) = '" & intdisciplinefileunkid & "'"
    '            'Else
    '            '    strQ &= " AND ISNULL(prearningdeduction_master.disciplinefileunkid,0) <= 0 "
    '        End If
    '        'S.SANDEEP [ 07 NOV 2011 ] -- END

    '        'Anjan (21 Nov 2011)-Start
    '        'ENHANCEMENT : TRA COMMENTS
    '        If mstrAdvanceFilter.Length > 0 Then
    '            strQ &= "AND " & mstrAdvanceFilter
    '        End If
    '        'Anjan (21 Nov 2011)-End 

    '        'Sohail (27 Aug 2012) -- Start
    '        'TRA - ENHANCEMENT
    '        strQ &= " ) AS a "

    '        'Sohail (12 Apr 2013) -- Start
    '        'TRA - ENHANCEMENT
    '        'If dtPeriodEndDate <> Nothing Then
    '        '    strQ &= "WHERE   ROWNO = 1 "
    '        'End If
    '        strQ &= "WHERE   1 = 1 "
    '        If dtPeriodEndDate <> Nothing Then
    '            strQ &= " AND   ROWNO = 1 "
    '        End If
    '        If intTranHeadUnkID > 0 Then
    '            strQ &= " AND a.tranheadunkid = @tranheadunkid "
    '            objDataOperation.AddParameter("@tranheadunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intTranHeadUnkID)
    '        End If
    '        'Sohail (12 Apr 2013) -- End

    '        'Sohail (27 Aug 2012) -- End

    '        If strSortField.Trim.Length > 0 Then
    '            strQ += " ORDER BY " & strSortField
    '        End If

    '        dsList = objDataOperation.ExecQuery(strQ, strTableName)

    '        If objDataOperation.ErrorMessage <> "" Then
    '            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
    '            Throw exForce
    '        End If

    '    Catch ex As Exception
    '        Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
    '    Finally
    '        exForce = Nothing
    '        If dsList IsNot Nothing Then dsList.Dispose()
    '    End Try
    '    Return dsList
    'End Function
    Public Function GetList(ByVal xDatabaseName As String _
                            , ByVal xUserUnkid As Integer _
                            , ByVal xYearUnkid As Integer _
                            , ByVal xCompanyUnkid As Integer _
                            , ByVal xPeriodStart As DateTime _
                            , ByVal xPeriodEnd As DateTime _
                            , ByVal xUserModeSetting As String _
                            , ByVal xOnlyApproved As Boolean _
                            , ByVal xIncludeIn_ActiveEmployee As Boolean _
                            , ByVal strTableName As String _
                            , Optional ByVal blnApplyUserAccessFilter As Boolean = True _
                            , Optional ByVal strEmpUnkIDs As String = "" _
                            , Optional ByVal strSortField As String = "" _
                            , Optional ByVal intPeriodID As Integer = 0 _
                            , Optional ByVal intEDHeadApprovalStatus As Integer = enEDHeadApprovalStatus.All _
                            , Optional ByVal intTranHeadUnkID As Integer = 0 _
                            , Optional ByVal mstrAdvanceFilter As String = "" _
                            , Optional ByVal dtPeriodEndDate As DateTime = Nothing _
                            , Optional ByVal intdisciplinefileunkid As Integer = -1 _
                            , Optional ByVal xDataOp As clsDataOperation = Nothing _
                            , Optional ByVal blnAddGrouping As Boolean = False _
                            , Optional ByVal strFilter As String = "" _
                            , Optional ByVal blnApplyDateFilter As Boolean = True _
                            ) As DataSet
        'Sohail (12 Oct 2021) - [blnApplyDateFilter]
        'Sohail (30 Oct 2019) - [strFilter]
        'Sohail (13 Jul 2019) - [blnAddGrouping]
        'Sohail (01 Mar 2019) - [objDataOperation] = [xDataOp]

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        Dim xDateJoinQry, xDateFilterQry, xUACQry, xUACFiltrQry, xAdvanceJoinQry As String
        xDateJoinQry = "" : xDateFilterQry = "" : xUACQry = "" : xUACFiltrQry = "" : xAdvanceJoinQry = ""
        If blnApplyDateFilter = True Then 'Sohail (12 Oct 2021)
        Call GetDatesFilterString(xDateJoinQry, xDateFilterQry, xPeriodStart, xPeriodEnd, , , xDatabaseName)
        End If 'Sohail (12 Oct 2021)
        If blnApplyUserAccessFilter = True Then 'Sohail (12 Oct 2021)
        Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, xPeriodEnd, xOnlyApproved, xDatabaseName, xUserUnkid, xCompanyUnkid, xYearUnkid, xUserModeSetting)
        End If 'Sohail (12 Oct 2021)
        If mstrAdvanceFilter.Length > 0 Then 'Sohail (12 Oct 2021)
        Call GetAdvanceFilterQry(xAdvanceJoinQry, xPeriodEnd, xDatabaseName)
        End If 'Sohail (12 Oct 2021)

        'Sohail (01 Mar 2019) -- Start
        'KBC Support Issue 0003536 - 74.1 - The database is in single user mode on close period.
        'If objDataOperation Is Nothing Then
        '    objDataOperation = New clsDataOperation
        'End If
        If xDataOp IsNot Nothing Then
            objDataOperation = xDataOp
        Else
            objDataOperation = New clsDataOperation
        End If
        'Sohail (01 Mar 2019) -- End

        objDataOperation.ClearParameters()

        'Sohail (18 Jan 2016) -- Start
        'Enhancement - Performance Enhancement for employee selection change on Process payroll screen in 58.1.
        Dim objMaster As New clsMasterData
        'Sohail (01 Mar 2019) -- Start
        'KBC Support Issue 0003536 - 74.1 - The database is in single user mode on close period.
        'Dim ds As DataSet = objMaster.getComboListForHeadType("HeadType")
        Dim ds As DataSet = objMaster.getComboListForHeadType("HeadType", objDataOperation)
        'Sohail (01 Mar 2019) -- End
        Dim dicHeadType As Dictionary(Of Integer, String) = (From p In ds.Tables("HeadType") Select New With {.id = CInt(p.Item("id")), .name = p.Item("name").ToString}).ToDictionary(Function(x) x.id, Function(x) x.name)
        'Sohail (01 Mar 2019) -- Start
        'KBC Support Issue 0003536 - 74.1 - The database is in single user mode on close period.
        'ds = objMaster.getComboListTypeOf("TypeOf", , True)
        ds = objMaster.getComboListTypeOf("TypeOf", , True, , , objDataOperation)
        'Sohail (01 Mar 2019) -- End
        Dim dicTypeOf As Dictionary(Of Integer, String) = (From p In ds.Tables("TypeOf") Select New With {.id = CInt(p.Item("id")), .name = p.Item("name").ToString}).ToDictionary(Function(x) x.id, Function(x) x.name)
        'Sohail (01 Mar 2019) -- Start
        'KBC Support Issue 0003536 - 74.1 - The database is in single user mode on close period.
        'ds = objMaster.getComboListCalcType("CalcType", , True)
        ds = objMaster.getComboListCalcType("CalcType", , True, , , , objDataOperation)
        'Sohail (01 Mar 2019) -- End
        Dim dicCalcType As Dictionary(Of Integer, String) = (From p In ds.Tables("CalcType") Select New With {.id = CInt(p.Item("id")), .name = p.Item("name").ToString}).ToDictionary(Function(x) x.id, Function(x) x.name)
        'Sohail (18 Jan 2016) -- End

        'Sohail (21 Aug 2015) -- Start
        'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
        'If mstrDatabaseName.Trim = "" Then mstrDatabaseName = FinancialYear._Object._DatabaseName
        mstrDatabaseName = xDatabaseName
        'Sohail (21 Aug 2015) -- End

        'Sohail (01 Mar 2019) -- Start
        'KBC Support Issue 0003536 - 74.1 - The database is in single user mode on close period.
        objDataOperation.ClearParameters()
        'Sohail (01 Mar 2019) -- End

        Try

            'S.SANDEEP [15 NOV 2016] -- START
            'ENHANCEMENT : QUERY OPTIMIZATION
            'strQ = "SELECT  isexempt " & _
            '              ", edunkid " & _
            '              ", employeeunkid " & _
            '              ", employeecode " & _
            '              ", employeename " & _
            '              ", tranheadunkid " & _
            '              ", trnheadname " & _
            '              ", trnheadcode " & _
            '              ", batchtransactionunkid " & _
            '              ", amount " & _
            '              ", isdeduct " & _
            '              ", trnheadtype_id " & _
            '              ", typeof_id " & _
            '              ", calctype_id " & _
            '              ", computeon_id " & _
            '              ", formula " & _
            '              ", formulaid " & _
            '              ", currencyunkid " & _
            '              ", vendorunkid " & _
            '              ", userunkid " & _
            '              ", isvoid " & _
            '              ", voiduserunkid " & _
            '              ", voiddatetime " & _
            '              ", voidreason " & _
            '              ", membership_categoryunkid " & _
            '              ", membershipname " & _
            '              ", isapproved " & _
            '              ", approveruserunkid " & _
            '              ", periodunkid " & _
            '              ", period_name " & _
            '              ", start_date " & _
            '              ", end_date " & _
            '              ", medicalrefno " & _
            '              ", disciplinefileunkid " & _
            '              ", membershiptranunkid " & _
            '              ", cumulative_startdate " & _
            '              ", stop_date " & _
            '              ", empbatchpostingunkid "
            ''Sohail (24 Feb 2016) - [empbatchpostingunkid]

            ''Sohail (18 Jan 2016) -- Start
            ''Enhancement - Performance Enhancement for employee selection change on Process payroll screen in 58.1.
            'strQ &= ", CASE trnheadtype_id "
            'For Each pair In dicHeadType
            '    strQ &= " WHEN " & pair.Key & "  THEN '" & pair.Value & "' "
            'Next
            'strQ &= " END AS trnheadtype_name "

            'strQ &= ", CASE typeof_id "
            'For Each pair In dicTypeOf
            '    strQ &= " WHEN " & pair.Key & "  THEN '" & pair.Value & "' "
            'Next
            'strQ &= " END AS typeof_name "

            'strQ &= ", CASE calctype_id "
            'For Each pair In dicCalcType
            '    strQ &= " WHEN " & pair.Key & "  THEN '" & pair.Value & "' "
            'Next
            'strQ &= " END AS calctype_name "
            ''Sohail (18 Jan 2016) -- End

            'strQ &= "FROM    ( "

            'strQ &= "SELECT  isexempt = CASE ISNULL(premployee_exemption_tran.tranheadunkid, 0) " & _
            '                         "WHEN 0 THEN 0 ELSE 1 END " & _
            '  ", prearningdeduction_master.edunkid " & _
            '  ", prearningdeduction_master.employeeunkid " & _
            '  ", hremployee_master.employeecode " & _
            '  ", hremployee_master.firstname + ' ' + hremployee_master.othername + ' ' + hremployee_master.surname AS employeename " & _
            '  ", prearningdeduction_master.tranheadunkid " & _
            '              ", prtranhead_master.trnheadname " & _
            '              ", prtranhead_master.trnheadcode " & _
            '  ", prearningdeduction_master.batchtransactionunkid " & _
            '  ", prearningdeduction_master.amount " & _
            '  ", prearningdeduction_master.isdeduct " & _
            '              ", prtranhead_master.trnheadtype_id " & _
            '              ", prtranhead_master.typeof_id " & _
            '              ", prtranhead_master.calctype_id " & _
            '              ", prtranhead_master.computeon_id " & _
            '              ", prtranhead_master.formula " & _
            '              ", prtranhead_master.formulaid " & _
            '  ", prearningdeduction_master.currencyunkid " & _
            '  ", prearningdeduction_master.vendorunkid " & _
            '  ", prearningdeduction_master.userunkid " & _
            '  ", prearningdeduction_master.isvoid " & _
            '  ", prearningdeduction_master.voiduserunkid " & _
            '  ", prearningdeduction_master.voiddatetime " & _
            '  ", prearningdeduction_master.voidreason " & _
            '  ", prearningdeduction_master.membership_categoryunkid " & _
            '          ", ISNULL(hrmembership_master.membershipname, '') AS membershipname " & _
            '                      ", ISNULL(prearningdeduction_master.isapproved, 0) AS isapproved " & _
            '                      ", ISNULL(prearningdeduction_master.approveruserunkid, 0) AS approveruserunkid " & _
            '          ", ISNULL(prearningdeduction_master.periodunkid, 0) AS periodunkid " & _
            '          ", ISNULL(cfcommon_period_tran.period_name, '') AS period_name " & _
            '          ", ISNULL(CONVERT(CHAR(8), cfcommon_period_tran.start_date,112), '19000101') AS start_date " & _
            '          ", ISNULL(CONVERT(CHAR(8), cfcommon_period_tran.end_date,112), '19000101') AS end_date " & _
            '              ", ISNULL(prearningdeduction_master.medicalrefno, '') AS medicalrefno " & _
            '          ", ISNULL(prearningdeduction_master.disciplinefileunkid,-1) AS disciplinefileunkid " & _
            '          ", ISNULL(membershiptranunkid,0) AS membershiptranunkid " & _
            '          ", cumulative_startdate " & _
            '          ", stop_date " & _
            '          ", ISNULL(prearningdeduction_master.empbatchpostingunkid,0) AS empbatchpostingunkid " & _
            '          ", DENSE_RANK() OVER ( PARTITION  BY prearningdeduction_master.employeeunkid ORDER BY cfcommon_period_tran.end_date DESC ) AS ROWNO " & _
            '"FROM " & mstrDatabaseName & "..prearningdeduction_master " & _
            '            "LEFT JOIN " & mstrDatabaseName & "..prtranhead_master ON prearningdeduction_master.tranheadunkid = prtranhead_master.tranheadunkid " & _
            '    "LEFT JOIN " & mstrDatabaseName & "..hremployee_master ON prearningdeduction_master.employeeunkid = hremployee_master.employeeunkid " & _
            '            "LEFT JOIN " & mstrDatabaseName & "..hrmembership_master ON hrmembership_master.membershipunkid = prearningdeduction_master.vendorunkid " & _
            '            "LEFT JOIN " & mstrDatabaseName & "..cfcommon_period_tran ON prearningdeduction_master.periodunkid = cfcommon_period_tran.periodunkid " & _
            '            "LEFT JOIN " & mstrDatabaseName & "..premployee_exemption_tran ON prearningdeduction_master.employeeunkid = premployee_exemption_tran.employeeunkid " & _
            '"AND prearningdeduction_master.tranheadunkid = premployee_exemption_tran.tranheadunkid " & _
            '"AND prearningdeduction_master.isvoid = premployee_exemption_tran.isvoid " & _
            '"AND premployee_exemption_tran.periodunkid = @periodunkid "
            ''Sohail (24 Feb 2016) - [empbatchpostingunkid]

            'If xDateJoinQry.Trim.Length > 0 Then
            '    strQ &= xDateJoinQry
            'End If

            ''S.SANDEEP [15 NOV 2016] -- START
            ''If xUACQry.Trim.Length > 0 Then
            ''    strQ &= xUACQry
            ''End If
            'If blnApplyUserAccessFilter = True Then
            '    If xUACQry.Trim.Length > 0 Then
            '        strQ &= xUACQry
            '    End If
            'End If
            ''S.SANDEEP [15 NOV 2016] -- END



            'If xAdvanceJoinQry.Trim.Length > 0 Then
            '    strQ &= xAdvanceJoinQry
            'End If


            'strQ &= "WHERE ISNULL(prearningdeduction_master.isvoid, 0) = 0 " '[isapproved, approveruserunkid - Sohail (14 Jun 2011)]

            ''S.SANDEEP [15 NOV 2016] -- START
            ''If blnApplyUserAccessFilter = True Then
            ''    If xUACFiltrQry.Trim.Length > 0 Then
            ''        strQ &= " AND " & xUACFiltrQry
            ''    End If
            ''End If
            ''S.SANDEEP [15 NOV 2016] -- END


            'If xIncludeIn_ActiveEmployee = False Then
            '    If xDateFilterQry.Trim.Length > 0 Then
            '        strQ &= xDateFilterQry
            '    End If
            'End If

            'If dtPeriodEndDate <> Nothing Then
            '    strQ &= "AND CONVERT(CHAR(8), cfcommon_period_tran.end_date, 112) <= @end_date "
            '    objDataOperation.AddParameter("@end_date", SqlDbType.VarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(dtPeriodEndDate))
            'End If

            'objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intPeriodID) 'Sohail (13 Jan 2012)


            'If strEmpUnkIDs.Trim <> "" AndAlso strEmpUnkIDs.Trim <> "0" AndAlso strEmpUnkIDs.Trim <> "-1" Then
            '    strQ &= "AND prearningdeduction_master.employeeunkid IN (" & strEmpUnkIDs & ") "
            'End If

            'If intEDHeadApprovalStatus = enEDHeadApprovalStatus.Approved Then
            '    strQ &= " AND prearningdeduction_master.isapproved = @isapproved "
            '    objDataOperation.AddParameter("@isapproved", SqlDbType.Int, eZeeDataType.INT_SIZE, intEDHeadApprovalStatus)
            'ElseIf intEDHeadApprovalStatus = enEDHeadApprovalStatus.Pending Then
            '    strQ &= " AND prearningdeduction_master.isapproved = @isapproved "
            '    objDataOperation.AddParameter("@isapproved", SqlDbType.Int, eZeeDataType.INT_SIZE, 0)
            'End If


            ''Sohail (21 Aug 2015) -- Start
            ''Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            ''================ PayPoint
            ''If intPayPointID > 0 Then
            ''    strQ += " AND hremployee_master.paypointunkid = @paypointunkid"
            ''    objDataOperation.AddParameter("@paypointunkid", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, intPayPointID)
            ''End If

            ' ''================ Department
            ''If intDeptID > 0 Then
            ''    strQ += " AND hremployee_master.departmentunkid = @departmentunkid"
            ''    objDataOperation.AddParameter("@departmentunkid", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, intDeptID)
            ''End If

            ' ''================ Section
            ''If intSectionID > 0 Then
            ''    strQ += " AND hremployee_master.sectionunkid = @sectionunkid"
            ''    objDataOperation.AddParameter("@sectionunkid", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, intSectionID)
            ''End If

            ' ''================ Unit
            ''If intUnitID > 0 Then
            ''    strQ += " AND hremployee_master.unitunkid = @unitunkid"
            ''    objDataOperation.AddParameter("@unitunkid", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, intUnitID)
            ''End If

            ' ''================ Job
            ''If intJobID > 0 Then
            ''    strQ += " AND hremployee_master.jobunkid = @jobunkid"
            ''    objDataOperation.AddParameter("@jobunkid", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, intJobID)
            ''End If

            ' ''================ Grade
            ''If intGradeID > 0 Then
            ''    strQ += " AND hremployee_master.gradeunkid = @gradeunkid"
            ''    objDataOperation.AddParameter("@gradeunkid", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, intGradeID)
            ''End If

            ' ''================ Access
            ''If intAccessID > 0 Then
            ''    strQ += " AND hremployee_master.accessunkid = @accessunkid"
            ''    objDataOperation.AddParameter("@accessunkid", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, intAccessID)
            ''End If

            ' ''================ Class
            ''If intClassID > 0 Then
            ''    strQ += " AND hremployee_master.classunkid = @classunkid"
            ''    objDataOperation.AddParameter("@classunkid", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, intClassID)
            ''End If

            ' ''================ Service
            ''If intServiceID > 0 Then
            ''    strQ += " AND hremployee_master.serviceunkid = @serviceunkid"
            ''    objDataOperation.AddParameter("@serviceunkid", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, intServiceID)
            ''End If

            ' ''================ CostCenter
            ''If intCostCenterID > 0 Then
            ''    strQ += " AND hremployee_master.costcenterunkid = @costcenterunkid"
            ''    objDataOperation.AddParameter("@costcenterunkid", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, intCostCenterID)
            ''End If
            ''Sohail (21 Aug 2015) -- End

            ''S.SANDEEP [ 07 NOV 2011 ] -- START
            ''ENHANCEMENT : TRA CHANGES
            'If intdisciplinefileunkid > 0 Then
            '    strQ &= " AND ISNULL(prearningdeduction_master.disciplinefileunkid,0) = '" & intdisciplinefileunkid & "'"
            '    'Else
            '    '    strQ &= " AND ISNULL(prearningdeduction_master.disciplinefileunkid,0) <= 0 "
            'End If
            ''S.SANDEEP [ 07 NOV 2011 ] -- END

            'If mstrAdvanceFilter.Length > 0 Then
            '    strQ &= "AND " & mstrAdvanceFilter
            'End If

            'strQ &= " ) AS a "

            'strQ &= "WHERE   1 = 1 "
            'If dtPeriodEndDate <> Nothing Then
            '    strQ &= " AND   ROWNO = 1 "
            'End If

            'If intTranHeadUnkID > 0 Then
            '    strQ &= " AND a.tranheadunkid = @tranheadunkid "
            '    objDataOperation.AddParameter("@tranheadunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intTranHeadUnkID)
            'End If

            'If strSortField.Trim.Length > 0 Then
            '    strQ += " ORDER BY " & strSortField
            'End If


            'Sohail (31 Jul 2018) -- Start
            'Internal Issue : Performance issue on ED list screen on NMB database in 73.1.
            'strQ = "SELECT " & _
            '       " CASE ISNULL(premployee_exemption_tran.tranheadunkid,0) WHEN 0 THEN 0 ELSE 1 END AS isexempt " & _
            '  ", prearningdeduction_master.edunkid " & _
            '  ", prearningdeduction_master.employeeunkid " & _
            '  ", hremployee_master.employeecode " & _
            '  ", hremployee_master.firstname + ' ' + hremployee_master.othername + ' ' + hremployee_master.surname AS employeename " & _
            '  ", prearningdeduction_master.tranheadunkid " & _
            '              ", prtranhead_master.trnheadname " & _
            '              ", prtranhead_master.trnheadcode " & _
            '  ", prearningdeduction_master.batchtransactionunkid " & _
            '  ", prearningdeduction_master.amount " & _
            '  ", prearningdeduction_master.isdeduct " & _
            '              ", prtranhead_master.trnheadtype_id " & _
            '              ", prtranhead_master.typeof_id " & _
            '              ", prtranhead_master.calctype_id " & _
            '              ", prtranhead_master.computeon_id " & _
            '              ", prtranhead_master.formula " & _
            '              ", prtranhead_master.formulaid " & _
            '  ", prearningdeduction_master.currencyunkid " & _
            '  ", prearningdeduction_master.vendorunkid " & _
            '  ", prearningdeduction_master.userunkid " & _
            '  ", prearningdeduction_master.isvoid " & _
            '  ", prearningdeduction_master.voiduserunkid " & _
            '  ", prearningdeduction_master.voiddatetime " & _
            '  ", prearningdeduction_master.voidreason " & _
            '  ", prearningdeduction_master.membership_categoryunkid " & _
            '                      ", ISNULL(prearningdeduction_master.isapproved, 0) AS isapproved " & _
            '                      ", ISNULL(prearningdeduction_master.approveruserunkid, 0) AS approveruserunkid " & _
            '          ", ISNULL(prearningdeduction_master.periodunkid, 0) AS periodunkid " & _
            '          ", ISNULL(cfcommon_period_tran.period_name, '') AS period_name " & _
            '          ", ISNULL(CONVERT(CHAR(8), cfcommon_period_tran.start_date,112), '19000101') AS start_date " & _
            '          ", ISNULL(CONVERT(CHAR(8), cfcommon_period_tran.end_date,112), '19000101') AS end_date " & _
            '              ", ISNULL(prearningdeduction_master.medicalrefno, '') AS medicalrefno " & _
            '          ", ISNULL(prearningdeduction_master.disciplinefileunkid,-1) AS disciplinefileunkid " & _
            '       ",ISNULL(prearningdeduction_master.membershiptranunkid, 0) AS membershiptranunkid " & _
            '       ",ISNULL(hrmembership_master.membershipname, '') AS membershipname " & _
            '          ", cumulative_startdate " & _
            '          ", stop_date " & _
            '       ",ISNULL(prearningdeduction_master.empbatchpostingunkid, 0) AS empbatchpostingunkid "

            'strQ &= ", CASE prearningdeduction_master.trnheadtype_id "
            'For Each pair In dicHeadType
            '    strQ &= " WHEN " & pair.Key & "  THEN '" & pair.Value & "' "
            'Next
            'strQ &= " END AS trnheadtype_name "

            'strQ &= ", CASE prearningdeduction_master.typeof_id "
            'For Each pair In dicTypeOf
            '    strQ &= " WHEN " & pair.Key & "  THEN '" & pair.Value & "' "
            'Next
            'strQ &= " END AS typeof_name "

            'strQ &= ", CASE prearningdeduction_master.calctype_id "
            'For Each pair In dicCalcType
            '    strQ &= " WHEN " & pair.Key & "  THEN '" & pair.Value & "' "
            'Next
            'strQ &= " END AS calctype_name "

            'strQ &= "FROM prearningdeduction_master " & _
            '        "JOIN " & _
            '        "( " & _
            '        "   SELECT " & _
            '        "        eslb.EmpId " & _
            '        "       ,eslb.PrdId " & _
            '        "   FROM " & _
            '        "   ( " & _
            '        "       SELECT " & _
            '        "            prearningdeduction_master.employeeunkid AS EmpId " & _
            '        "           ,cfcommon_period_tran.periodunkid AS PrdId " & _
            '        "           ,ROW_NUMBER()OVER(PARTITION BY prearningdeduction_master.employeeunkid ORDER BY cfcommon_period_tran.end_date DESC) AS rno " & _
            '        "       FROM prearningdeduction_master " & _
            '        "           JOIN cfcommon_period_tran ON cfcommon_period_tran.periodunkid = prearningdeduction_master.periodunkid " & _
            '        "       WHERE prearningdeduction_master.isvoid = 0 AND CONVERT(CHAR(8),cfcommon_period_tran.end_date,112) <= @end_date " & _
            '        ") AS eslb WHERE eslb.rno = 1 " & _
            '        ") AS Mtab ON Mtab.EmpId = prearningdeduction_master.employeeunkid AND Mtab.PrdId = prearningdeduction_master.periodunkid " & _
            '        "LEFT JOIN " & mstrDatabaseName & "..hremployee_meminfo_tran ON hremployee_meminfo_tran.membershiptranunkid = prearningdeduction_master.membershiptranunkid " & _
            '        "LEFT JOIN " & mstrDatabaseName & "..hrmembership_master ON hrmembership_master.membershipunkid = hremployee_meminfo_tran.membershipunkid " & _
            '        "JOIN " & mstrDatabaseName & "..prtranhead_master ON prearningdeduction_master.tranheadunkid = prtranhead_master.tranheadunkid " & _
            '        "JOIN " & mstrDatabaseName & "..hremployee_master ON prearningdeduction_master.employeeunkid = hremployee_master.employeeunkid " & _
            '        "JOIN " & mstrDatabaseName & "..cfcommon_period_tran ON prearningdeduction_master.periodunkid = cfcommon_period_tran.periodunkid " & _
            '            "LEFT JOIN " & mstrDatabaseName & "..premployee_exemption_tran ON prearningdeduction_master.employeeunkid = premployee_exemption_tran.employeeunkid " & _
            '"AND prearningdeduction_master.tranheadunkid = premployee_exemption_tran.tranheadunkid " & _
            '"AND premployee_exemption_tran.isvoid = 0 " & _
            '"AND premployee_exemption_tran.periodunkid = @periodunkid "

            ''Hemant (20 June 2018) -- Added Filter ("AND premployee_exemption_tran.isvoid = 0 ") in above query for getting proper Status (Yes/No)
            '' for exempted Head

            'If xDateJoinQry.Trim.Length > 0 Then
            '    strQ &= xDateJoinQry
            'End If

            'If blnApplyUserAccessFilter = True Then
            'If xUACQry.Trim.Length > 0 Then
            '    strQ &= xUACQry
            'End If
            'End If

            'If xAdvanceJoinQry.Trim.Length > 0 Then
            '    strQ &= xAdvanceJoinQry
            'End If


            'strQ &= "WHERE ISNULL(prearningdeduction_master.isvoid, 0) = 0 "

            'If xIncludeIn_ActiveEmployee = False Then
            '    If xDateFilterQry.Trim.Length > 0 Then
            '        strQ &= xDateFilterQry
            '    End If
            'End If
            strQ = "SELECT   eslb.EmpId " & _
                          ", eslb.PrdId " & _
                   "INTO #mtab " & _
                   "FROM " & _
                   "( " & _
                     "SELECT   prearningdeduction_master.employeeunkid AS EmpId " & _
                            ", cfcommon_period_tran.periodunkid AS PrdId " & _
                            ", ROW_NUMBER()OVER(PARTITION BY prearningdeduction_master.employeeunkid ORDER BY cfcommon_period_tran.end_date DESC) AS rno " & _
                     "FROM prearningdeduction_master " & _
                     "JOIN cfcommon_period_tran ON cfcommon_period_tran.periodunkid = prearningdeduction_master.periodunkid " & _
                     "WHERE prearningdeduction_master.isvoid = 0 AND CONVERT(CHAR(8),cfcommon_period_tran.end_date,112) <= @end_date "

            'Sohail (12 Oct 2021) -- Start
            'NMB Issue :  : Performance issue on process payroll.
            If strEmpUnkIDs.Trim <> "" AndAlso strEmpUnkIDs.Trim <> "0" AndAlso strEmpUnkIDs.Trim <> "-1" Then
                strQ &= "AND prearningdeduction_master.employeeunkid IN (" & strEmpUnkIDs & ") "
            End If
            'Sohail (12 Oct 2021) -- End

            strQ &= ") AS eslb WHERE eslb.rno = 1 "

            strQ &= "SELECT   hremployee_master.employeeunkid " & _
                           ", hremployee_master.employeecode " & _
                           ", hremployee_master.firstname + ' ' + hremployee_master.othername + ' ' + hremployee_master.surname AS employeename " & _
                    "INTO #emp " & _
                    "FROM " & mstrDatabaseName & "..hremployee_master "

            If xDateJoinQry.Trim.Length > 0 Then
                strQ &= xDateJoinQry
            End If

            If blnApplyUserAccessFilter = True Then
                If xUACQry.Trim.Length > 0 Then
                    strQ &= xUACQry
                End If
            End If

            If xAdvanceJoinQry.Trim.Length > 0 Then
                strQ &= xAdvanceJoinQry
            End If

            strQ &= "WHERE 1 = 1 "

            'Sohail (12 Oct 2021) -- Start
            'NMB Issue :  : Performance issue on process payroll.
            If strEmpUnkIDs.Trim <> "" AndAlso strEmpUnkIDs.Trim <> "0" AndAlso strEmpUnkIDs.Trim <> "-1" Then
                strQ &= " AND hremployee_master.employeeunkid IN (" & strEmpUnkIDs & ") "
            End If
            'Sohail (12 Oct 2021) -- End

            If xIncludeIn_ActiveEmployee = False Then
                If xDateFilterQry.Trim.Length > 0 Then
                    strQ &= xDateFilterQry
                End If
            End If
            'Hemant (06 Aug 2018) -- Start
            'Issue #2432 : Resolved Error of "Multi-part Identifier "prtranhead_master.typeof_id" Column" 
            'If mstrAdvanceFilter.Length > 0 Then
            '    strQ &= "AND " & mstrAdvanceFilter
            'End If
            'Hemant (06 Aug 2018) -- End

            'Hemant (27 Sept 2018) -- Start
            'Issue #2557 : Error on setting a filter criteria(Allocation) to perform a search on E&D list
            'Sohail (03 Oct 2020) -- Start
            'Issue : OLD-177 #  : Bug on E&D screen when filtering using ALLOCATION option (invalid column name employmenttypeunkid).
            'If mstrAdvanceFilter.Length > 0 AndAlso mstrAdvanceFilter.Contains("ADF.") = True Then
            '    strQ &= "AND " & mstrAdvanceFilter
            'End If
            If mstrAdvanceFilter.Length > 0 Then
                'Sohail (24 Oct 2020) -- Start
                'Sheer Logic Issue : # OLD-196  : Bug when you filter By Branch system returns entries/records which belong to branches not selected in the filter on Earning Deduction Screen.
                'strQ &= "AND " & mstrAdvanceFilter.Replace("ADF.", "hremployee_master.")
                strQ &= " AND " & mstrAdvanceFilter
                'Sohail (24 Oct 2020) -- End
            End If
            'Sohail (03 Oct 2020) -- End
            'Hemant (27 Sept 2018) -- End

            strQ &= "SELECT " & _
                   " CASE ISNULL(premployee_exemption_tran.tranheadunkid,0) WHEN 0 THEN 0 ELSE 1 END AS isexempt " & _
              ", prearningdeduction_master.edunkid " & _
              ", prearningdeduction_master.employeeunkid " & _
                          ", #emp.employeecode " & _
                          ", #emp.employeename " & _
              ", prearningdeduction_master.tranheadunkid " & _
                          ", prtranhead_master.trnheadname " & _
                          ", prtranhead_master.trnheadcode " & _
              ", prearningdeduction_master.batchtransactionunkid " & _
              ", prearningdeduction_master.amount " & _
              ", prearningdeduction_master.isdeduct " & _
                          ", prtranhead_master.trnheadtype_id " & _
                          ", prtranhead_master.typeof_id " & _
                          ", prtranhead_master.calctype_id " & _
                          ", prtranhead_master.computeon_id " & _
                          ", prtranhead_master.formula " & _
                          ", prtranhead_master.formulaid " & _
              ", prearningdeduction_master.currencyunkid " & _
              ", prearningdeduction_master.vendorunkid " & _
              ", prearningdeduction_master.userunkid " & _
              ", prearningdeduction_master.isvoid " & _
              ", prearningdeduction_master.voiduserunkid " & _
              ", prearningdeduction_master.voiddatetime " & _
              ", prearningdeduction_master.voidreason " & _
              ", prearningdeduction_master.membership_categoryunkid " & _
                                  ", ISNULL(prearningdeduction_master.isapproved, 0) AS isapproved " & _
                                  ", ISNULL(prearningdeduction_master.approveruserunkid, 0) AS approveruserunkid " & _
                      ", ISNULL(prearningdeduction_master.periodunkid, 0) AS periodunkid " & _
                      ", ISNULL(cfcommon_period_tran.period_name, '') AS period_name " & _
                      ", ISNULL(CONVERT(CHAR(8), cfcommon_period_tran.start_date,112), '19000101') AS start_date " & _
                      ", ISNULL(CONVERT(CHAR(8), cfcommon_period_tran.end_date,112), '19000101') AS end_date " & _
                          ", ISNULL(prearningdeduction_master.medicalrefno, '') AS medicalrefno " & _
                      ", ISNULL(prearningdeduction_master.disciplinefileunkid,-1) AS disciplinefileunkid " & _
                   ",ISNULL(prearningdeduction_master.membershiptranunkid, 0) AS membershiptranunkid " & _
                   ",ISNULL(hrmembership_master.membershipname, '') AS membershipname " & _
                      ", cumulative_startdate " & _
                      ", stop_date " & _
                      ", edstart_date " & _
                   ",ISNULL(prearningdeduction_master.empbatchpostingunkid, 0) AS empbatchpostingunkid " & _
                   ", CAST(0 AS BIT) AS IsGrp " & _
                ", CAST(0 AS BIT) AS IsChecked "
            'Sohail (24 Aug 2019) - [edstart_date]
            'Sohail (13 Jul 2019) - [IsGrp, IsChecked]

            strQ &= ", CASE prtranhead_master.trnheadtype_id "
            For Each pair In dicHeadType
                strQ &= " WHEN " & pair.Key & "  THEN '" & pair.Value & "' "
            Next
            strQ &= " END AS trnheadtype_name "

            strQ &= ", CASE prtranhead_master.typeof_id "
            For Each pair In dicTypeOf
                strQ &= " WHEN " & pair.Key & "  THEN '" & pair.Value & "' "
            Next
            strQ &= " END AS typeof_name "

            strQ &= ", CASE prtranhead_master.calctype_id "
            For Each pair In dicCalcType
                strQ &= " WHEN " & pair.Key & "  THEN '" & pair.Value & "' "
            Next
            strQ &= " END AS calctype_name "

            strQ &= "FROM prearningdeduction_master " & _
                    "JOIN #emp ON prearningdeduction_master.employeeunkid = #emp.employeeunkid " & _
                    "JOIN #mtab AS Mtab ON Mtab.EmpId = prearningdeduction_master.employeeunkid AND Mtab.PrdId = prearningdeduction_master.periodunkid " & _
                    "LEFT JOIN " & mstrDatabaseName & "..hremployee_meminfo_tran ON hremployee_meminfo_tran.membershiptranunkid = prearningdeduction_master.membershiptranunkid " & _
                    "LEFT JOIN " & mstrDatabaseName & "..hrmembership_master ON hrmembership_master.membershipunkid = hremployee_meminfo_tran.membershipunkid " & _
                    "JOIN " & mstrDatabaseName & "..prtranhead_master ON prearningdeduction_master.tranheadunkid = prtranhead_master.tranheadunkid " & _
                    "JOIN " & mstrDatabaseName & "..cfcommon_period_tran ON prearningdeduction_master.periodunkid = cfcommon_period_tran.periodunkid " & _
                        "LEFT JOIN " & mstrDatabaseName & "..premployee_exemption_tran ON prearningdeduction_master.employeeunkid = premployee_exemption_tran.employeeunkid " & _
            "AND prearningdeduction_master.tranheadunkid = premployee_exemption_tran.tranheadunkid " & _
            "AND premployee_exemption_tran.isvoid = 0 " & _
            "AND premployee_exemption_tran.periodunkid = @periodunkid "

            strQ &= "WHERE prearningdeduction_master.isvoid = 0 "
            'Sohail (31 Jul 2018) -- End

                objDataOperation.AddParameter("@end_date", SqlDbType.VarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(dtPeriodEndDate))
            objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intPeriodID)

            If strEmpUnkIDs.Trim <> "" AndAlso strEmpUnkIDs.Trim <> "0" AndAlso strEmpUnkIDs.Trim <> "-1" Then
                strQ &= "AND prearningdeduction_master.employeeunkid IN (" & strEmpUnkIDs & ") "
            End If

            If intEDHeadApprovalStatus = enEDHeadApprovalStatus.Approved Then
                strQ &= " AND prearningdeduction_master.isapproved = @isapproved "
                objDataOperation.AddParameter("@isapproved", SqlDbType.Int, eZeeDataType.INT_SIZE, intEDHeadApprovalStatus)
            ElseIf intEDHeadApprovalStatus = enEDHeadApprovalStatus.Pending Then
                strQ &= " AND prearningdeduction_master.isapproved = @isapproved "
                objDataOperation.AddParameter("@isapproved", SqlDbType.Int, eZeeDataType.INT_SIZE, 0)
            End If

            If intdisciplinefileunkid > 0 Then
                strQ &= " AND ISNULL(prearningdeduction_master.disciplinefileunkid,0) = '" & intdisciplinefileunkid & "'"
            End If

            'Sohail (31 Jul 2018) -- Start
            'Internal Issue : Performance issue on ED list screen on NMB database in 73.1.
            'If mstrAdvanceFilter.Length > 0 Then
            '    strQ &= "AND " & mstrAdvanceFilter
            'End If
            'Sohail (31 Jul 2018) -- End

            If intTranHeadUnkID > 0 Then
                strQ &= " AND prearningdeduction_master.tranheadunkid = @tranheadunkid "
                objDataOperation.AddParameter("@tranheadunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intTranHeadUnkID)
            End If

            'Hemant (06 Aug 2018) -- Start
            'Issue #2432 : Resolved Error of "Multi-part Identifier "prtranhead_master.typeof_id" Column" 
            'Hemant (27 Sept 2018) -- Start
            'Issue #2557 : Error on setting a filter criteria(Allocation) to perform a search on E&D list
            'If mstrAdvanceFilter.Length > 0 Then
            '    strQ &= "AND " & mstrAdvanceFilter
            'End If
            'Sohail (03 Oct 2020) -- Start
            'Issue : OLD-177 #  : Bug on E&D screen when filtering using ALLOCATION option (invalid column name employmenttypeunkid).
            'If mstrAdvanceFilter.Length > 0 AndAlso mstrAdvanceFilter.Contains("ADF.") = False Then
            '    strQ &= "AND " & mstrAdvanceFilter
            'End If
            'Sohail (03 Oct 2020) -- End
            'Hemant (27 Sept 2018) -- End            
            'Hemant (06 Aug 2018) -- End

            'Sohail (30 Oct 2019) -- Start
            'Mainspring Resourcing Ghana - Enhancement # 4212 : Add a filter "Calculation Type" in Employee Earning & Deduction List screen..
            If strFilter.Trim <> "" Then
                strQ &= " " & strFilter & " "
            End If
            'Sohail (30 Oct 2019) -- End

            If strSortField.Trim.Length > 0 Then
                strQ += " ORDER BY " & strSortField
            End If
            'S.SANDEEP [15 NOV 2016] -- END

            'Sohail (31 Jul 2018) -- Start
            'Internal Issue : Performance issue on ED list screen on NMB database in 73.1.
            strQ &= " DROP TABLE #mtab " & _
                    " DROP TABLE #emp "
            'Sohail (31 Jul 2018) -- End

            dsList = objDataOperation.ExecQuery(strQ, strTableName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            'Sohail (13 Jul 2019) -- Start
            'Mainspring Resourcing Enhancement - Ref #  - 76.1 - 1.Global Approval for E &D  for all Add/Edit from MSS. 2. Set ED heads in pending status after importing update flat rate. 3. Notification for Approval with list of employees in one long email.
            If blnAddGrouping = True Then

                Dim dt As DataTable = New DataView(dsList.Tables(0)).ToTable(True, "employeename", "employeecode", "employeeunkid")
                Dim dtCol As New DataColumn("IsChecked", System.Type.GetType("System.Boolean"))
                dtCol.DefaultValue = False
                dtCol.AllowDBNull = False
                dt.Columns.Add(dtCol)

                dtCol = New DataColumn("IsGrp", System.Type.GetType("System.Boolean"))
                dtCol.DefaultValue = True
                dtCol.AllowDBNull = False
                dt.Columns.Add(dtCol)

                dt.Merge(dsList.Tables(0), False)
                'Sohail (12 Oct 2021) -- Start
                'NMB Issue :  : Performance issue on process payroll.
                'dt.DefaultView.Sort = "employeename, employeecode, employeeunkid, IsGrp DESC"
                dt.DefaultView.Sort = "employeename, employeecode, employeeunkid, IsGrp DESC, trnheadtype_id, typeof_id, calctype_id DESC"
                'Sohail (12 Oct 2021) -- End

                dsList.Tables.Clear()
                dsList.Tables.Add(dt.DefaultView.ToTable)
            End If
            'Sohail (13 Jul 2019) -- End

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            'Sohail (01 Mar 2019) -- Start
            'KBC Support Issue 0003536 - 74.1 - The database is in single user mode on close period.
            If xDataOp Is Nothing Then objDataOperation = Nothing
            'Sohail (01 Mar 2019) -- End
        End Try
        Return dsList
    End Function
    'Sohail (21 Aug 2015) -- End

    'Sohail (25 Jun 2020) -- Start
    'NMB Enhancement # : Performance enhancement on Import earning deduction heads.
    Public Function GetAllEDUnkIdForSelectedPeriod(ByVal xDataOp As clsDataOperation _
                                                   , ByVal strDatabaseName As String _
                                                   , ByVal intPeriodUnkID As Integer _
                                                   , ByVal strEmployeeIDs As String _
                                                   , Optional ByVal intTranHeadUnkID As Integer = 0 _
                                                   ) As DataSet

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        If xDataOp IsNot Nothing Then
            objDataOperation = xDataOp
        Else
            objDataOperation = New clsDataOperation
        End If

        mstrDatabaseName = strDatabaseName

        objDataOperation.ClearParameters()

        Try

            strQ = "SELECT " & _
                 "  edunkid " & _
                 ", employeeunkid " & _
                 ", periodunkid " & _
                 ", tranheadunkid " & _
              "FROM " & mstrDatabaseName & "..prearningdeduction_master " & _
                    "WHERE isvoid = 0  " & _
                    " AND periodunkid = @periodunkid "

            If strEmployeeIDs.Trim <> "" Then
                strQ &= "AND employeeunkid IN (" & strEmployeeIDs & ") "
            End If

            If intTranHeadUnkID > 0 Then
                strQ &= " AND tranheadunkid = @tranheadunkid "
                objDataOperation.AddParameter("@tranheadunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intTranHeadUnkID)
            End If

            objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intPeriodUnkID)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetAllEDUnkIdForSelectedPeriod; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xDataOp Is Nothing Then objDataOperation = Nothing
        End Try
        Return dsList
    End Function
    'Sohail (25 Jun 2020) -- End

    ''' <summary>
    ''' Modify By: Suhail
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> INSERT INTO Database Table (prearningdeduction_master) </purpose>
    Public Function Insert(ByVal strDatabaseName As String _
                           , ByVal xUserUnkid As Integer _
                           , ByVal xYearUnkid As Integer _
                           , ByVal xCompanyUnkid As Integer _
                           , ByVal xPeriodStart As DateTime _
                           , ByVal xPeriodEnd As DateTime _
                           , ByVal xUserModeSetting As String _
                           , ByVal xOnlyApproved As Boolean _
                           , ByVal xIncludeIn_ActiveEmployee As Boolean _
                           , ByVal blnGlobal As Boolean _
                           , ByVal dtCurrentDateAndTime As Date _
                           , Optional ByVal blnInsertSalaryHead As Boolean = False _
                           , Optional ByVal xDataOp As clsDataOperation = Nothing _
                           , Optional ByVal blnApplyUserAccessFilter As Boolean = True _
                           , Optional ByVal strFilerString As String = "" _
                           , Optional ByVal blnCheckMembershipHeads As Boolean = True _
                           , Optional ByVal blnAllowToApproveEarningDeduction As Boolean = False _
                           , Optional ByVal blnSkipInsertIfExist As Boolean = False _
                           ) As Boolean
        'Sohail (25 Jun 2020) - [blnSkipInsertIfExist]
        'Sohail (15 Dec 2018) - [objDataOperation] = [xDataOp]
        'Sohail (20 Jul 2018) - [blnAllowToApproveEarningDeduction]
        'Sohail (18 Nov 2016) - [blnCheckMembershipHeads]
        'Sohail (21 Aug 2015) - [strDatabaseName, xUserUnkid, xYearUnkid, xCompanyUnkid, xPeriodStart, xPeriodEnd, xUserModeSetting, xOnlyApproved, xIncludeIn_ActiveEmployee, dtCurrentDateAndTime, blnApplyUserAccessFilter, strFilerString]
        'Sohail (11 Nov 2010), 'Sohail (23 Apr 2012) - [objDataOperation]
        'Public Function Insert() As Boolean

        'Sohail (15 Dec 2018) -- Start
        'NMB Enhancement - On salary change screen, create a coonection between new scale text box and change amount text box. i.e if a user types in the new scale, the change amount calculates automatically in 75.1.
        If xDataOp IsNot Nothing Then
            objDataOperation = xDataOp
        Else
            objDataOperation = New clsDataOperation
            objDataOperation.BindTransaction()
        End If
        objDataOperation.ClearParameters()
        'Sohail (15 Dec 2018) -- End

        'Sohail (13 Jan 2012) -- Start
        'TRA - ENHANCEMENT
        'Sohail (23 Apr 2012) -- Start
        'TRA - ENHANCEMENT
        'Sohail (21 Aug 2015) -- Start
        'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
        'If isExist(mintEmployeeunkid, mintTranheadunkid, , mintPeriodunkid, objDataOperation) Then
        'Sohail (15 Dec 2018) -- Start
        'NMB Enhancement - On salary change screen, create a coonection between new scale text box and change amount text box. i.e if a user types in the new scale, the change amount calculates automatically in 75.1.
        'If isExist(strDatabaseName, mintEmployeeunkid, mintTranheadunkid, , mintPeriodunkid, objDataOperation) Then
        If isExist(strDatabaseName, mintEmployeeunkid, mintTranheadunkid, , mintPeriodunkid, objDataOperation) Then
            'Sohail (15 Dec 2018) -- End
            'Sohail (21 Aug 2015) -- End
            'If isExist(mintEmployeeunkid, mintTranheadunkid, , mintPeriodunkid) Then
            'Sohail (23 Apr 2012) -- End
            'If isExist(mintEmployeeunkid, mintTranheadunkid) Then
            'Sohail (13 Jan 2012) -- End
            'Sohail (25 Jun 2020) -- Start
            'NMB Enhancement # : Performance enhancement on Import earning deduction heads.
            'mstrMessage = Language.getMessage(mstrModuleName, 1, "This Transaction Head already exist.")
            'Return False
            If blnSkipInsertIfExist = True Then
                Return True
            Else
            mstrMessage = Language.getMessage(mstrModuleName, 1, "This Transaction Head already exist.")
            Return False
        End If
            'Sohail (25 Jun 2020) -- End
        End If

        'Sohail (18 Nov 2016) -- Start
        'Enhancement -  64.1 - Insert Membership heads on Ed if not assigned.
        'Sohail (20 Jul 2018) -- Start
        'Support Issue # 0002383 : VOLTAMP: Unable to assign Transaction heads using batch entry in Payroll transactions (Warning message of Membership heads not assigned)in 73.1.
        'If blnCheckMembershipHeads = True AndAlso IsExist_UnAssigned_MembershipHeads(strDatabaseName, xUserUnkid, xYearUnkid, xCompanyUnkid, xPeriodStart, xPeriodEnd, xUserModeSetting, xOnlyApproved, xIncludeIn_ActiveEmployee, False, mintPeriodunkid, objDataOperation) = True Then
        'Sohail (25 Jun 2020) -- Start
        'NMB Enhancement # : Validation moved to process payrol for Performance enhancement on Import earning deduction heads.
        'If blnCheckMembershipHeads = True AndAlso IsExist_UnAssigned_MembershipHeads(strDatabaseName, xUserUnkid, xYearUnkid, xCompanyUnkid, xPeriodStart, xPeriodEnd, xUserModeSetting, xOnlyApproved, xIncludeIn_ActiveEmployee, False, mintPeriodunkid, objDataOperation, " AND hremployee_meminfo_tran.employeeunkid <> " & mintEmployeeunkid & " ") = True Then
        'If blnCheckMembershipHeads = True AndAlso IsExist_UnAssigned_MembershipHeads(strDatabaseName, xUserUnkid, xYearUnkid, xCompanyUnkid, xPeriodStart, xPeriodEnd, xUserModeSetting, xOnlyApproved, xIncludeIn_ActiveEmployee, False, mintPeriodunkid, objDataOperation, "", 0, mintEmployeeunkid, False) = True Then
        '    'Sohail (20 Jul 2018) -- End
        '    mstrMessage = Language.getMessage(mstrModuleName, 4, "Sorry, Some active membership heads are not assigned on Earning Deductions. Please assign them from Earning Deduction List -> Operation -> Assign membership heads.")
        '    Return False
        'End If
        'Sohail (25 Jun 2020) -- End
        'Sohail (18 Nov 2016) -- End

        'Sohail (19 Jun 2017) -- Start
        'Enhancement - 68.1 - Prevent assigning head if formula is not set.
        'Sohail (25 Jun 2020) -- Start
        'NMB Enhancement # : Validation moved to process payroll screen for Performance enhancement on Import earning deduction heads.
        'Dim objTranHead As New clsTransactionHead
        'objTranHead._xDataOperation = objDataOperation
        'objTranHead._Tranheadunkid(strDatabaseName) = mintTranheadunkid
        'If objTranHead._Formulaid.Trim = "" AndAlso (objTranHead._Calctype_Id = enCalcType.AsComputedValue OrElse objTranHead._Calctype_Id = enCalcType.AsComputedOnWithINEXCESSOFTaxSlab) Then
        '    mstrMessage = Language.getMessage(mstrModuleName, 5, "Formula is not set on the Transaction Head. Please set formula from Transaction Head screen for") & " " & objTranHead._Trnheadname
        '    Return False
        'End If
        'Sohail (25 Jun 2020) -- End
        'Sohail (19 Jun 2017) -- End

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim blnResult As Boolean 'Sohail (14 Jun 2011)

        'S.SANDEEP [19 OCT 2016] -- START
        'ISSUE : EMPLOYEE MASTER BIND TRANSACTION ISSUE
        'objDataOperation = New clsDataOperation
        'Sohail (15 Dec 2018) -- Start
        'NMB Enhancement - On salary change screen, create a coonection between new scale text box and change amount text box. i.e if a user types in the new scale, the change amount calculates automatically in 75.1.
        'If objDataOperation IsNot Nothing Then
        '    objDataOperation = objDataOperation
        'Else
        '    objDataOperation = New clsDataOperation
        'End If
        'Sohail (15 Dec 2018) -- End
        objDataOperation.ClearParameters()
        'S.SANDEEP [19 OCT 2016] -- END

        'Sohail (11 Nov 2010) -- Start
        'Sohail (15 Dec 2018) -- Start
        'NMB Enhancement - On salary change screen, create a coonection between new scale text box and change amount text box. i.e if a user types in the new scale, the change amount calculates automatically in 75.1.
        'If blnGlobal = False Then
        '    'S.SANDEEP [19 OCT 2016] -- START
        '    'ISSUE : EMPLOYEE MASTER BIND TRANSACTION ISSUE
        '    'objDataOperation.BindTransaction()
        '    If objDataOperation Is Nothing Then
        '        objDataOperation.BindTransaction()
        '    End If
        '    'S.SANDEEP [19 OCT 2016] -- END
        'End If
        'Sohail (15 Dec 2018) -- End
        'Sohail (11 Nov 2010) -- End

        'Sohail (21 Jul 2012) -- Start
        'TRA - ENHANCEMENT
        'Sohail (21 Aug 2015) -- Start
        'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
        'If mstrDatabaseName.Trim = "" Then mstrDatabaseName = FinancialYear._Object._DatabaseName
        mstrDatabaseName = strDatabaseName
        'Sohail (21 Aug 2015) -- End
        'Sohail (21 Jul 2012) -- End

        'Sohail (13 Jan 2012) -- Start
        'TRA - ENHANCEMENT
        If blnInsertSalaryHead = True Then
            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'blnResult = Insert_SalaryHead(mintEmployeeunkid, mintPeriodunkid)
            blnResult = Insert_SalaryHead(strDatabaseName, xUserUnkid, xYearUnkid, xCompanyUnkid, xPeriodStart, xPeriodEnd, xUserModeSetting, xOnlyApproved, xIncludeIn_ActiveEmployee, mintEmployeeunkid, mintPeriodunkid, dtCurrentDateAndTime, objDataOperation, blnApplyUserAccessFilter, strFilerString)
            'Sohail (21 Aug 2015) -- End

            If blnResult = False Then
                Return False
            End If

            objDataOperation.ClearParameters()
        End If
        'Sohail (13 Jan 2012) -- End

        Try
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeunkid.ToString)
            objDataOperation.AddParameter("@tranheadunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintTranheadunkid.ToString)
            objDataOperation.AddParameter("@batchtransactionunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintBatchtransactionunkid.ToString)
            objDataOperation.AddParameter("@amount", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecAmount.ToString) 'Sohail (11 May 2011)
            objDataOperation.AddParameter("@isdeduct", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsdeduct.ToString)
            objDataOperation.AddParameter("@trnheadtype_id", SqlDbType.Int, eZeeDataType.INT_SIZE, mintTrnheadtype_Id.ToString)
            objDataOperation.AddParameter("@typeof_id", SqlDbType.Int, eZeeDataType.INT_SIZE, mintTypeof_Id.ToString)
            objDataOperation.AddParameter("@calctype_id", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCalctype_Id.ToString)
            objDataOperation.AddParameter("@computeon_id", SqlDbType.Int, eZeeDataType.INT_SIZE, mintComputeon_Id.ToString)
            objDataOperation.AddParameter("@formula", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrFormula.ToString)
            objDataOperation.AddParameter("@formulaid", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrFormulaid.ToString)
            objDataOperation.AddParameter("@currencyunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCurrencyunkid.ToString)
            objDataOperation.AddParameter("@vendorunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVendorunkid.ToString)
            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
            If mdtVoiddatetime = Nothing Then
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            Else
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime)
            End If
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)
            objDataOperation.AddParameter("@membership_categoryunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintMembership_Categoryunkid.ToString)
            'Sohail (14 Jun 2011) -- Start
            objDataOperation.AddParameter("@isapproved", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsapproved.ToString)
            objDataOperation.AddParameter("@approveruserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintApproveruserunkid.ToString)
            'Sohail (14 Jun 2011) -- End
            objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPeriodunkid.ToString) 'Sohail (13 Jan 2012)
            objDataOperation.AddParameter("@medicalrefno", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrMedicalrefno.ToString) 'Sohail (18 Jan 2012)

            'S.SANDEEP [ 07 NOV 2011 ] -- START
            'ENHANCEMENT : TRA CHANGES
            objDataOperation.AddParameter("@disciplinefileunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintDisciplinefileunkid.ToString)
            'S.SANDEEP [ 07 NOV 2011 ] -- END

            'S.SANDEEP [ 17 OCT 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            objDataOperation.AddParameter("@membershiptranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintMembershipTranUnkid.ToString)
            'S.SANDEEP [ 17 OCT 2012 ] -- END
            'Sohail (09 Nov 2013) -- Start
            'TRA - ENHANCEMENT
            If mdtCumulative_Startdate = Nothing Then
                objDataOperation.AddParameter("@cumulative_startdate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            Else
                objDataOperation.AddParameter("@cumulative_startdate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtCumulative_Startdate.ToString)
            End If
            If mdtStop_Date = Nothing Then
                objDataOperation.AddParameter("@stop_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            Else
                objDataOperation.AddParameter("@stop_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtStop_Date.ToString)
            End If
            'Sohail (09 Nov 2013) -- End
            'Sohail (24 Aug 2019) -- Start
            'NMB Payroll UAT # TC016 - 76.1 - System should be able to accept future amendments (Future period) and prorate. It should be possible to set start date of the specific transaction and prorate based on the start date. It should also be possible to set start date of the future period.
            If mdtEDStart_Date = Nothing Then
                objDataOperation.AddParameter("@edstart_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            Else
                objDataOperation.AddParameter("@edstart_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtEDStart_Date.ToString)
            End If
            'Sohail (24 Aug 2019) -- End
            'Sohail (24 Feb 2016) -- Start
            'Enhancement - Batch Posting changes as per online doc in 58.1 SP. (#15 (KBC & Kenya Project Comments List.xls))
            objDataOperation.AddParameter("@empbatchpostingunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmpbatchpostingunkid.ToString)
            'Sohail (24 Feb 2016) -- End

            strQ = "INSERT INTO " & mstrDatabaseName & "..prearningdeduction_master ( " & _
              "  employeeunkid " & _
              ", tranheadunkid " & _
              ", batchtransactionunkid " & _
              ", amount " & _
              ", isdeduct " & _
              ", trnheadtype_id " & _
              ", typeof_id " & _
              ", calctype_id " & _
              ", computeon_id " & _
              ", formula " & _
              ", formulaid " & _
              ", currencyunkid " & _
              ", vendorunkid " & _
              ", userunkid " & _
              ", isvoid " & _
              ", voiduserunkid " & _
              ", voiddatetime" & _
              ", voidreason " & _
              ", membership_categoryunkid " & _
              ", isapproved " & _
              ", approveruserunkid " & _
              ", periodunkid " & _
              ", medicalrefno " & _
              ", disciplinefileunkid " & _
              ", membershiptranunkid " & _
              ", cumulative_startdate " & _
              ", stop_date " & _
              ", empbatchpostingunkid " & _
              ", edstart_date " & _
            ") VALUES (" & _
              "  @employeeunkid " & _
              ", @tranheadunkid " & _
              ", @batchtransactionunkid " & _
              ", @amount " & _
              ", @isdeduct " & _
              ", @trnheadtype_id " & _
              ", @typeof_id " & _
              ", @calctype_id " & _
              ", @computeon_id " & _
              ", @formula " & _
              ", @formulaid " & _
              ", @currencyunkid " & _
              ", @vendorunkid " & _
              ", @userunkid " & _
              ", @isvoid " & _
              ", @voiduserunkid " & _
              ", @voiddatetime" & _
              ", @voidreason " & _
              ", @membership_categoryunkid " & _
              ", @isapproved " & _
              ", @approveruserunkid " & _
              ", @periodunkid " & _
              ", @medicalrefno " & _
              ", @disciplinefileunkid " & _
              ", @membershiptranunkid " & _
              ", @cumulative_startdate " & _
              ", @stop_date " & _
              ", @empbatchpostingunkid " & _
              ", @edstart_date " & _
            "); SELECT @@identity"
            'Sohail (24 Aug 2019) - [edstart_date]
            'Sohail (24 Feb 2016) - [empbatchpostingunkid]
            'Sohail (09 Nov 2013) - [cumulative_startdate, stop_date]
            '[isapproved, approveruserunkid - Sohail (14 Jun 2011)] 'S.SANDEEP [ 07 NOV 2011 - disciplinefileunkid ]
            'S.SANDEEP [ 17 OCT 2012 ], [membershiptranunkid]

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            mintEdunkid = dsList.Tables(0).Rows(0).Item(0)

            'Sohail (11 Nov 2010) -- Start
            'Sohail (14 Jun 2011) -- Start
            'Call InsertAuditTrailForEarningDeduction(objDataOperation, 1)
            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'blnResult = InsertAuditTrailForEarningDeduction(objDataOperation, 1)
            blnResult = InsertAuditTrailForEarningDeduction(objDataOperation, 1, dtCurrentDateAndTime)
            'Sohail (21 Aug 2015) -- End
            'Sohail (14 Jun 2011) -- End

            'Sohail (08 Nov 2011) -- Start
            If blnResult = True AndAlso mintCostCenterUnkId > 0 Then
                Dim objEmpCC As New clsemployee_costcenter_Tran
                With objEmpCC
                    ._DatabaseName = mstrDatabaseName 'Sohail (21 Jul 2011)
                    ._Employeeunkid = mintEmployeeunkid
                    ._Tranheadunkid = mintTranheadunkid
                    ._Costcenterunkid = mintCostCenterUnkId
                    ._Userunkid = mintUserunkid
                    ._Isvoid = mblnIsvoid
                    ._Voiduserunkid = mintVoiduserunkid
                    ._Voiddatetime = mdtVoiddatetime
                    ._Voidreason = mstrVoidreason
                    ._Periodunkid = mintPeriodunkid
                    'Sohail (07 Feb 2019) -- Start
                    'NMB Enhancement - 76.1 - Employee cost centre should be distributed on all allocations instead of only on cost centre.
                    ._AllocationbyId = enAllocation.COST_CENTER
                    'Sohail (07 Feb 2019) -- End
                    ._DatabaseName = mstrDatabaseName
                    ._FormName = mstrFormName
                    ._LoginEmployeeUnkid = mintLogEmployeeUnkid
                    ._ClientIP = mstrClientIP
                    ._HostName = mstrHostName
                    ._FromWeb = mblnIsWeb
                    ._AuditUserId = mintAuditUserId
._CompanyUnkid = mintCompanyUnkid
                    ._AuditDate = mdtAuditDate
                    blnResult = .Insert(strDatabaseName, objDataOperation, dtCurrentDateAndTime)
                End With
                objEmpCC = Nothing
            End If
            'Sohail (08 Nov 2011) -- End

            'Sohail (15 Dec 2018) -- Start
            'NMB Enhancement - On salary change screen, create a coonection between new scale text box and change amount text box. i.e if a user types in the new scale, the change amount calculates automatically in 75.1.
            'If blnGlobal = False Then
            '    'Sohail (14 Jun 2011) -- Start
            '    'Issue : on error in insertaudittrail record is inserted in main table while not inserted in audit table.
            '    'objDataOperation.ReleaseTransaction(True)
            '    If blnResult = True Then
            '        objDataOperation.ReleaseTransaction(True)
            '    Else
            '        objDataOperation.ReleaseTransaction(False)
            '        Return False
            '    End If
            '    'Sohail (14 Jun 2011) -- End
            'End If
            If blnResult = False Then
                    objDataOperation.ReleaseTransaction(False)
                    Return False
                End If
            'Sohail (15 Dec 2018) -- End
            'Sohail (11 Nov 2010) -- End

            'Sohail (20 Jul 2018) -- Start
            'Support Issue # 0002383 : VOLTAMP: Unable to assign Transaction heads using batch entry in Payroll transactions (Warning message of Membership heads not assigned)in 73.1.
            'Sohail (25 Jun 2020) -- Start
            'NMB Enhancement # : Validation moved to process payroll for Performance enhancement on Import earning deduction heads.
            'If blnCheckMembershipHeads = True AndAlso IsExist_UnAssigned_MembershipHeads(strDatabaseName, xUserUnkid, xYearUnkid, xCompanyUnkid, xPeriodStart, xPeriodEnd, xUserModeSetting, xOnlyApproved, xIncludeIn_ActiveEmployee, False, mintPeriodunkid, objDataOperation, " AND hremployee_meminfo_tran.employeeunkid = " & mintEmployeeunkid & " ") = True Then
            'If blnCheckMembershipHeads = True AndAlso IsExist_UnAssigned_MembershipHeads(strDatabaseName, xUserUnkid, xYearUnkid, xCompanyUnkid, xPeriodStart, xPeriodEnd, xUserModeSetting, xOnlyApproved, xIncludeIn_ActiveEmployee, False, mintPeriodunkid, objDataOperation, "", mintEmployeeunkid, 0, False) = True Then
            '    objDataOperation.ClearParameters()
            '    'Sohail (25 Jun 2020) -- Start
            '    'NMB Enhancement # : Performance enhancement on Import earning deduction heads.
            '    'Dim dsMemb As DataSet = GetList_UnAssigned_MembershipHeads(strDatabaseName, xUserUnkid, xYearUnkid, xCompanyUnkid, xPeriodStart, xPeriodEnd, xUserModeSetting, True, False, True, "List", mintPeriodunkid, objDataOperation, " AND hremployee_meminfo_tran.employeeunkid = " & mintEmployeeunkid & " ")
            '    Dim dsMemb As DataSet = GetList_UnAssigned_MembershipHeads(strDatabaseName, xUserUnkid, xYearUnkid, xCompanyUnkid, xPeriodStart, xPeriodEnd, xUserModeSetting, True, False, True, "List", mintPeriodunkid, objDataOperation, "", mintEmployeeunkid, 0, False)
            '    'Sohail (25 Jun 2020) -- End
            '    If dsMemb.Tables(0).Columns.Contains("IsChecked") = False Then
            '        Dim dtCol As New DataColumn("IsChecked", Type.GetType("System.Boolean"))
            '        dtCol.DefaultValue = True
            '        dtCol.AllowDBNull = False
            '        dsMemb.Tables(0).Columns.Add(dtCol)
            '    End If

            '    blnResult = Insert_MembershipHeads(strDatabaseName, xUserUnkid, xYearUnkid, xCompanyUnkid, xPeriodStart, xPeriodEnd, xUserModeSetting, True, False, mintPeriodunkid, blnAllowToApproveEarningDeduction, dsMemb.Tables(0), dtCurrentDateAndTime, objDataOperation, True)
            '    'Sohail (15 Dec 2018) -- Start
            '    'NMB Enhancement - On salary change screen, create a coonection between new scale text box and change amount text box. i.e if a user types in the new scale, the change amount calculates automatically in 75.1.
            '    'If blnGlobal = False Then
            '    '    If blnResult = True Then
            '    '        objDataOperation.ReleaseTransaction(True)
            '    '    Else
            '    '        objDataOperation.ReleaseTransaction(False)
            '    '        Return False
            '    '    End If
            '    'End If
            '    If blnResult = False Then
                '        objDataOperation.ReleaseTransaction(False)
                '        Return False
                '    End If
            '    'Sohail (15 Dec 2018) -- End
                'End If
            'Sohail (25 Jun 2020) -- End
            'Sohail (20 Jul 2018) -- End

            'Sohail (15 Dec 2018) -- Start
            'NMB Enhancement - On salary change screen, create a coonection between new scale text box and change amount text box. i.e if a user types in the new scale, the change amount calculates automatically in 75.1.
            If xDataOp Is Nothing Then objDataOperation.ReleaseTransaction(True)
            'Sohail (15 Dec 2018) -- End

            Return True
        Catch ex As Exception
            'Sohail (11 Nov 2010) -- Start
            'Sohail (15 Dec 2018) -- Start
            'NMB Enhancement - On salary change screen, create a coonection between new scale text box and change amount text box. i.e if a user types in the new scale, the change amount calculates automatically in 75.1.
            'If blnGlobal = False Then
            '    objDataOperation.ReleaseTransaction(False)
            'End If
                objDataOperation.ReleaseTransaction(False)
            'Sohail (15 Dec 2018) -- End
            'Sohail (11 Nov 2010) -- End
            Throw New Exception(ex.Message & "; Procedure Name: Insert; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            'Sohail (15 Dec 2018) -- Start
            'NMB Enhancement - On salary change screen, create a coonection between new scale text box and change amount text box. i.e if a user types in the new scale, the change amount calculates automatically in 75.1.
            'objDataOperation = Nothing
            If xDataOp Is Nothing Then objDataOperation = Nothing
            'Sohail (15 Dec 2018) -- End
        End Try
    End Function

    'Sohail (13 Jan 2012) -- Start
    'TRA - ENHANCEMENT
    Private Function Insert_SalaryHead(ByVal xDatabaseName As String _
                            , ByVal xUserUnkid As Integer _
                            , ByVal xYearUnkid As Integer _
                            , ByVal xCompanyUnkid As Integer _
                            , ByVal xPeriodStart As DateTime _
                            , ByVal xPeriodEnd As DateTime _
                            , ByVal xUserModeSetting As String _
                            , ByVal xOnlyApproved As Boolean _
                            , ByVal xIncludeIn_ActiveEmployee As Boolean _
                            , ByVal intEmployeeUnkId As Integer _
                            , ByVal intPeriodUnkId As Integer _
                            , ByVal dtCurrentDateAndTime As Date _
                            , Optional ByVal xDataOp As clsDataOperation = Nothing _
                            , Optional ByVal blnApplyUserAccessFilter As Boolean = True _
                            , Optional ByVal strFilerString As String = "" _
                            ) As Boolean 'Sohail (23 Apr 2012) - [objDataOperation]
        'Sohail (01 Mar 2019) - [objDataOperation] = [xDataOp]
        'Sohail (21 Aug 2015) - [xDatabaseName, xUserUnkid, xYearUnkid, xCompanyUnkid, xPeriodStart, xPeriodEnd, xUserModeSetting, xOnlyApproved, xIncludeIn_ActiveEmployee, dtCurrentDateAndTime, blnApplyUserAccessFilter, strFilerString]

        Dim objEmp As New clsEmployee_Master
        'Dim objTranHead As New clsTransactionHead 'Sohail (23 Apr 2012) 
        'Sohail (26 Feb 2015) -- Start
        'Issue - Salary head not inserted when current ed slab is exist without salary head and current salary head is defferent then previous period salary head.
        Dim objTranHead As New clsTransactionHead
        'Sohail (26 Feb 2015) -- End
        Dim intTranHeadUnkId As Integer
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        Try
            objEmp._DataOperation = objDataOperation 'Sohail (23 Apr 2012) 

            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'objEmp._Employeeunkid = intEmployeeUnkId
            'Sohail (23 Dec 2019) -- Start
            'NMB Issue # : Users not able to login in self service when doing any transaction on earning and deduction screen.
            'objEmp._Employeeunkid(xPeriodEnd) = intEmployeeUnkId
            objEmp._Employeeunkid(xPeriodEnd, objDataOperation) = intEmployeeUnkId
            'Sohail (23 Dec 2019) -- End
            'S.SANDEEP [04 JUN 2015] -- END

            intTranHeadUnkId = objEmp._Tranhedunkid
            'Sohail (23 Apr 2012) -- Start
            'TRA - ENHANCEMENT
            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'If isExist(intEmployeeUnkId, intTranHeadUnkId, , intPeriodUnkId, objDataOperation) = True Then
            If isExist(xDatabaseName, intEmployeeUnkId, intTranHeadUnkId, , intPeriodUnkId, objDataOperation) = True Then
                'Sohail (21 Aug 2015) -- End
                'If isExist(intEmployeeUnkId, intTranHeadUnkId, , intPeriodUnkId) = True Then
                'Sohail (23 Apr 2012) -- End
                Return True
            End If

            'Sohail (21 Sep 2012) -- Start
            'TRA - ENHANCEMENT
            'Dim ds As DataSet = GetList("SalaryED", intEmployeeUnkId, , , , , , , , , , , , , , intTranHeadUnkId)
            Dim objPeriod As New clscommom_period_Tran
            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'objPeriod._Periodunkid = intPeriodUnkId

            'S.SANDEEP [19 OCT 2016] -- START
            'ISSUE : EMPLOYEE MASTER BIND TRANSACTION ISSUE
            objPeriod._xDataOperation = objDataOperation
            'S.SANDEEP [19 OCT 2016] -- END

            objPeriod._Periodunkid(xDatabaseName) = intPeriodUnkId
            'Sohail (21 Aug 2015) -- End
            'Sohail (17 Dec 2013) -- Start
            'Issue - Salary head not inserted when current ed slab is exist without salary head
            'Dim ds As DataSet = GetList("SalaryED", intEmployeeUnkId, , , , , , , , , , , , , , intTranHeadUnkId, , , objPeriod._End_Date)
            'Sohail (26 Feb 2015) -- Start
            'Issue - Salary head not inserted when current ed slab is exist without salary head and current salary head is defferent then previous period salary head.
            'Dim ds As DataSet = GetList("SalaryED", intEmployeeUnkId, , , , , , , , , , , "end_date DESC", , , intTranHeadUnkId, , , objPeriod._Start_Date)
            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'Dim ds As DataSet = GetList("SalaryED", intEmployeeUnkId, , , , , , , , , , , "end_date DESC", , , , , "prtranhead_master.typeof_id = " & enTypeOf.Salary & "", objPeriod._Start_Date)

            'S.SANDEEP [19 OCT 2016] -- START
            'ISSUE : EMPLOYEE MASTER BIND TRANSACTION ISSUE
            'Dim ds As DataSet = GetList(xDatabaseName, xUserUnkid, xYearUnkid, xCompanyUnkid, xPeriodStart, xPeriodEnd, xUserModeSetting, xOnlyApproved, xIncludeIn_ActiveEmployee, "SalaryED", False, intEmployeeUnkId, "end_date DESC", , , , "prtranhead_master.typeof_id = " & enTypeOf.Salary & "", objPeriod._Start_Date)
            'Sohail (05 Mar 2019) -- Start
            'KBC Issue - 74.1 - single user mode issue on close period due to temp table is getting created in get list method which switched database connection and causing single user mode issue.
            'Dim ds As DataSet = GetList(xDatabaseName, xUserUnkid, xYearUnkid, xCompanyUnkid, xPeriodStart, xPeriodEnd, xUserModeSetting, xOnlyApproved, xIncludeIn_ActiveEmployee, "SalaryED", False, intEmployeeUnkId, "end_date DESC", , , , "prtranhead_master.typeof_id = " & enTypeOf.Salary & "", objPeriod._Start_Date, , objDataOperation)
            strQ = "SELECT   * " & _
                     "FROM ( " & _
                             "SELECT   prearningdeduction_master.edunkid " & _
                                    ", prearningdeduction_master.employeeunkid " & _
                                    ", prearningdeduction_master.tranheadunkid " & _
                                    ", prtranhead_master.trnheadname " & _
                                    ", prtranhead_master.trnheadcode " & _
                                    ", prearningdeduction_master.batchtransactionunkid " & _
                                    ", prearningdeduction_master.amount " & _
                                    ", prearningdeduction_master.isdeduct " & _
                                    ", prtranhead_master.trnheadtype_id " & _
                                    ", prtranhead_master.typeof_id " & _
                                    ", prtranhead_master.calctype_id " & _
                                    ", prtranhead_master.computeon_id " & _
                                    ", prtranhead_master.formula " & _
                                    ", prtranhead_master.formulaid " & _
                                    ", prearningdeduction_master.currencyunkid " & _
                                    ", prearningdeduction_master.vendorunkid " & _
                                    ", prearningdeduction_master.userunkid " & _
                                    ", prearningdeduction_master.isvoid " & _
                                    ", prearningdeduction_master.voiduserunkid " & _
                                    ", prearningdeduction_master.voiddatetime " & _
                                    ", prearningdeduction_master.voidreason " & _
                                    ", prearningdeduction_master.membership_categoryunkid " & _
                                    ", ISNULL(prearningdeduction_master.isapproved, 0) AS isapproved " & _
                                    ", ISNULL(prearningdeduction_master.approveruserunkid, 0) AS approveruserunkid " & _
                                    ", ISNULL(prearningdeduction_master.periodunkid, 0) AS periodunkid " & _
                                    ", ISNULL(cfcommon_period_tran.period_name, '') AS period_name " & _
                                    ", ISNULL(CONVERT(CHAR(8), cfcommon_period_tran.start_date,112), '19000101') AS start_date " & _
                                    ", ISNULL(CONVERT(CHAR(8), cfcommon_period_tran.end_date,112), '19000101') AS end_date " & _
                                    ", ISNULL(prearningdeduction_master.medicalrefno, '') AS medicalrefno " & _
                                    ", ISNULL(prearningdeduction_master.disciplinefileunkid,-1) AS disciplinefileunkid " & _
                                    ", ISNULL(prearningdeduction_master.membershiptranunkid, 0) AS membershiptranunkid " & _
                                    ", cumulative_startdate " & _
                                    ", stop_date " & _
                                    ", edstart_date " & _
                                    ", ISNULL(prearningdeduction_master.empbatchpostingunkid, 0) AS empbatchpostingunkid " & _
                                    ", DENSE_RANK() OVER(PARTITION BY prearningdeduction_master.employeeunkid ORDER BY cfcommon_period_tran.end_date DESC) AS ROWNO " & _
                             "FROM prearningdeduction_master " & _
                                     "JOIN cfcommon_period_tran ON cfcommon_period_tran.periodunkid = prearningdeduction_master.periodunkid " & _
                                     "LEFT JOIN prtranhead_master ON prtranhead_master.tranheadunkid = prearningdeduction_master.tranheadunkid " & _
                             "WHERE prearningdeduction_master.isvoid = 0 AND CONVERT(CHAR(8),cfcommon_period_tran.end_date,112) <= @end_date " & _
                                     "AND prearningdeduction_master.employeeunkid = @employeeunkid " & _
                            ") AS A WHERE A.ROWNO = 1 " & _
                                    "AND A.typeof_id = " & enTypeOf.Salary & " " & _
                                    "ORDER BY A.end_date DESC "
            'Sohail (24 Aug 2019) - [edstart_date]
            'Sohail (16 Apr 2019) - [ROW_NUMBER()] = [DENSE_RANK] (chances of missing salary head)

            objDataOperation.AddParameter("@end_date", SqlDbType.VarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(objPeriod._Start_Date))
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intEmployeeUnkId)

            Dim ds As DataSet = objDataOperation.ExecQuery(strQ, "SalaryED")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If
            'Sohail (05 Mar 2019) -- End
            'S.SANDEEP [19 OCT 2016] -- END

            'Sohail (21 Aug 2015) -- End
            'Sohail (26 Feb 2015) -- End
            'Sohail (17 Dec 2013) -- End
            'objPeriod = Nothing 'Sohail (26 Feb 2015)
            'Sohail (21 Sep 2012) -- End

            'Sohail (26 Feb 2015) -- Start
            'Issue - Salary head not inserted when current ed slab is exist without salary head and current salary head is defferent then previous period salary head.
            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'objTranHead._Tranheadunkid = intTranHeadUnkId

            'S.SANDEEP [19 OCT 2016] -- START
            'ISSUE : EMPLOYEE MASTER BIND TRANSACTION ISSUE
            objTranHead._xDataOperation = objDataOperation
            'S.SANDEEP [19 OCT 2016] -- END
            objTranHead._Tranheadunkid(xDatabaseName) = intTranHeadUnkId
            'Sohail (21 Aug 2015) -- End
            'Sohail (26 Feb 2015) -- End

            'Sohail (23 Apr 2012) -- Start
            'TRA - ENHANCEMENT
            'Dim objDataOperation As New clsDataOperation
            'Sohail (01 Mar 2019) -- Start
            'KBC Support Issue 0003536 - 74.1 - The database is in single user mode on close period.
            'If objDataOperation Is Nothing Then
            '    objDataOperation = New clsDataOperation
            'End If
            If xDataOp IsNot Nothing Then
                objDataOperation = xDataOp
            Else
                objDataOperation = New clsDataOperation
            End If
            'Sohail (01 Mar 2019) -- End
            objDataOperation.ClearParameters()
            'Sohail (23 Apr 2012) -- End

            'Sohail (21 Jul 2012) -- Start
            'TRA - ENHANCEMENT
            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'If mstrDatabaseName.Trim = "" Then mstrDatabaseName = FinancialYear._Object._DatabaseName
            mstrDatabaseName = xDatabaseName
            'Sohail (21 Aug 2015) -- End
            'Sohail (21 Jul 2012) -- End

            For Each dsRow As DataRow In ds.Tables("SalaryED").Rows
                objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intEmployeeUnkId.ToString)
                objDataOperation.AddParameter("@tranheadunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intTranHeadUnkId.ToString)
                objDataOperation.AddParameter("@batchtransactionunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, -1)
                'Sohail (26 Feb 2015) -- Start
                'Issue - Salary head not inserted when current ed slab is exist without salary head and current salary head is defferent then previous period salary head.
                'objDataOperation.AddParameter("@amount", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, dsRow.Item("amount").ToString)
                Dim decSalaryAmt As Decimal = 0
                If objTranHead._Calctype_Id = enCalcType.FlatRate_Others Then
                    Dim objMaster As New clsMasterData
                    Dim dsSalary As DataSet = objMaster.Get_Current_Scale("List", intEmployeeUnkId, objPeriod._End_Date)
                    If dsSalary.Tables(0).Rows.Count > 0 Then
                        decSalaryAmt = CDec(dsSalary.Tables(0).Rows(0).Item("newscale"))
                    End If
                    objDataOperation.AddParameter("@amount", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, decSalaryAmt)
                    objMaster = Nothing
                Else
                    objDataOperation.AddParameter("@amount", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, 0)
                End If
                'Sohail (26 Feb 2015) -- End
                objDataOperation.AddParameter("@isdeduct", SqlDbType.Bit, eZeeDataType.BIT_SIZE, dsRow.Item("isdeduct").ToString)
                objDataOperation.AddParameter("@trnheadtype_id", SqlDbType.Int, eZeeDataType.INT_SIZE, dsRow.Item("trnheadtype_id").ToString)
                objDataOperation.AddParameter("@typeof_id", SqlDbType.Int, eZeeDataType.INT_SIZE, dsRow.Item("typeof_id").ToString)
                objDataOperation.AddParameter("@calctype_id", SqlDbType.Int, eZeeDataType.INT_SIZE, dsRow.Item("calctype_id").ToString)
                objDataOperation.AddParameter("@computeon_id", SqlDbType.Int, eZeeDataType.INT_SIZE, dsRow.Item("computeon_id").ToString)
                objDataOperation.AddParameter("@formula", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, dsRow.Item("formula").ToString)
                objDataOperation.AddParameter("@formulaid", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, dsRow.Item("formulaid").ToString)
                objDataOperation.AddParameter("@currencyunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, dsRow.Item("currencyunkid").ToString)
                objDataOperation.AddParameter("@vendorunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, dsRow.Item("vendorunkid").ToString)
                'Sohail (14 Nov 2017) -- Start
                'Issue - 70.1 - Previous transaction user id was passed to current period transaction.
                'objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, dsRow.Item("userunkid").ToString)
                objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, xUserUnkid)
                'Sohail (14 Nov 2017) -- End
                objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, dsRow.Item("isvoid").ToString)
                objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, dsRow.Item("voiduserunkid").ToString)
                If dsRow.Item("voiddatetime").ToString = Nothing Then
                    objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
                Else
                    objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, dsRow.Item("voiddatetime").ToString)
                End If
                objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, dsRow.Item("voidreason").ToString)
                objDataOperation.AddParameter("@membership_categoryunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, dsRow.Item("membership_categoryunkid").ToString)
                objDataOperation.AddParameter("@isapproved", SqlDbType.Bit, eZeeDataType.BIT_SIZE, dsRow.Item("isapproved").ToString)
                'Sohail (14 Nov 2017) -- Start
                'Issue - 70.1 - Previous transaction user id was passed to current period transaction.
                'objDataOperation.AddParameter("@approveruserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, dsRow.Item("approveruserunkid").ToString)
                objDataOperation.AddParameter("@approveruserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, xUserUnkid)
                'Sohail (14 Nov 2017) -- End
                objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intPeriodUnkId.ToString)
                objDataOperation.AddParameter("@medicalrefno", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, dsRow.Item("medicalrefno").ToString) 'Sohail (18 Jan 2012)
                'Sohail (09 Nov 2013) -- Start
                'TRA - ENHANCEMENT
                objDataOperation.AddParameter("@disciplinefileunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, dsRow.Item("disciplinefileunkid").ToString)
                objDataOperation.AddParameter("@membershiptranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, dsRow.Item("membershiptranunkid").ToString)
                If dsRow.Item("cumulative_startdate").ToString = Nothing Then
                    objDataOperation.AddParameter("@cumulative_startdate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
                Else
                    objDataOperation.AddParameter("@cumulative_startdate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, dsRow.Item("cumulative_startdate").ToString)
                End If
                If dsRow.Item("stop_date").ToString = Nothing Then
                    objDataOperation.AddParameter("@stop_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
                Else
                    objDataOperation.AddParameter("@stop_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, dsRow.Item("stop_date").ToString)
                End If
                'Sohail (09 Nov 2013) -- End
                'Sohail (24 Aug 2019) -- Start
                'NMB Payroll UAT # TC016 - 76.1 - System should be able to accept future amendments (Future period) and prorate. It should be possible to set start date of the specific transaction and prorate based on the start date. It should also be possible to set start date of the future period.
                If dsRow.Item("edstart_date").ToString = Nothing Then
                    objDataOperation.AddParameter("@edstart_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
                Else
                    objDataOperation.AddParameter("@edstart_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, dsRow.Item("edstart_date").ToString)
                End If
                'Sohail (24 Aug 2019) -- End
                'Sohail (24 Feb 2016) -- Start
                'Enhancement - Batch Posting changes as per online doc in 58.1 SP. (#15 (KBC & Kenya Project Comments List.xls))
                objDataOperation.AddParameter("@empbatchpostingunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, dsRow.Item("empbatchpostingunkid").ToString)
                'Sohail (24 Feb 2016) -- End

                strQ = "INSERT INTO " & mstrDatabaseName & "..prearningdeduction_master ( " & _
                  "  employeeunkid " & _
                  ", tranheadunkid " & _
                  ", batchtransactionunkid " & _
                  ", amount " & _
                  ", isdeduct " & _
                  ", trnheadtype_id " & _
                  ", typeof_id " & _
                  ", calctype_id " & _
                  ", computeon_id " & _
                  ", formula " & _
                  ", formulaid " & _
                  ", currencyunkid " & _
                  ", vendorunkid " & _
                  ", userunkid " & _
                  ", isvoid " & _
                  ", voiduserunkid " & _
                  ", voiddatetime" & _
                  ", voidreason " & _
                  ", membership_categoryunkid " & _
                  ", isapproved " & _
                  ", approveruserunkid " & _
                  ", periodunkid " & _
                  ", medicalrefno " & _
                  ", disciplinefileunkid " & _
                  ", membershiptranunkid " & _
                  ", cumulative_startdate " & _
                  ", stop_date" & _
                  ", empbatchpostingunkid " & _
                  ", edstart_date" & _
                ") VALUES (" & _
                  "  @employeeunkid " & _
                  ", @tranheadunkid " & _
                  ", @batchtransactionunkid " & _
                  ", @amount " & _
                  ", @isdeduct " & _
                  ", @trnheadtype_id " & _
                  ", @typeof_id " & _
                  ", @calctype_id " & _
                  ", @computeon_id " & _
                  ", @formula " & _
                  ", @formulaid " & _
                  ", @currencyunkid " & _
                  ", @vendorunkid " & _
                  ", @userunkid " & _
                  ", @isvoid " & _
                  ", @voiduserunkid " & _
                  ", @voiddatetime" & _
                  ", @voidreason " & _
                  ", @membership_categoryunkid " & _
                  ", @isapproved " & _
                  ", @approveruserunkid " & _
                  ", @periodunkid " & _
                  ", @medicalrefno " & _
                  ", @disciplinefileunkid " & _
                  ", @membershiptranunkid " & _
                  ", @cumulative_startdate " & _
                  ", @stop_date" & _
                  ", @empbatchpostingunkid " & _
                  ", @edstart_date" & _
                "); SELECT @@identity"
                'Sohail (24 Aug 2019) - [edstart_date]
                'Sohail (24 Feb 2016) - [empbatchpostingunkid]
                'Sohail (09 Nov 2013) - [disciplinefileunkid, membershiptranunkid, cumulative_startdate, stop_date]
                '[isapproved, approveruserunkid - Sohail (14 Jun 2011)]


                dsList = objDataOperation.ExecQuery(strQ, "List")

                If objDataOperation.ErrorMessage <> "" Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If

                mintEdunkid = dsList.Tables(0).Rows(0).Item(0)

                'Sohail (18 Jan 2012) -- Start
                'TRA - ENHANCEMENT
                'blnResult = InsertAuditTrailForEarningDeduction(objDataOperation, 1)

                'If blnResult = False Then
                '    Return False
                'End If
                strQ = "INSERT INTO " & mstrDatabaseName & "..atprearningdeduction_master ( " & _
                        "  edunkid " & _
                        ", employeeunkid " & _
                        ", tranheadunkid " & _
                        ", batchtransactionunkid " & _
                        ", amount " & _
                        ", isdeduct " & _
                        ", trnheadtype_id " & _
                        ", typeof_id " & _
                        ", calctype_id " & _
                        ", computeon_id " & _
                        ", formula " & _
                        ", formulaid " & _
                        ", currencyunkid " & _
                        ", vendorunkid " & _
                        ", membership_categoryunkid " & _
                        ", isapproved " & _
                        ", approveruserunkid " & _
                        ", audittype " & _
                        ", audituserunkid " & _
                        ", auditdatetime " & _
                        ", ip " & _
                        ", machine_name" & _
                        ", periodunkid " & _
                        ", medicalrefno " & _
                        ", disciplinefileunkid " & _
                        ", membershiptranunkid " & _
                        ", cumulative_startdate " & _
                        ", stop_date" & _
                        ", empbatchpostingunkid " & _
                        ", edstart_date" & _
                      ", form_name " & _
                      " " & _
                      " " & _
                      " " & _
                      " " & _
                      " " & _
                      ", isweb " & _
                    ") VALUES (" & _
                        "  @edunkid " & _
                        ", @employeeunkid " & _
                        ", @tranheadunkid " & _
                        ", @batchtransactionunkid " & _
                        ", @amount " & _
                        ", @isdeduct " & _
                        ", @trnheadtype_id " & _
                        ", @typeof_id " & _
                        ", @calctype_id " & _
                        ", @computeon_id " & _
                        ", @formula " & _
                        ", @formulaid " & _
                        ", @currencyunkid " & _
                        ", @vendorunkid " & _
                        ", @membership_categoryunkid " & _
                        ", @isapproved " & _
                        ", @approveruserunkid " & _
                        ", @audittype " & _
                        ", @audituserunkid " & _
                        ", @auditdatetime " & _
                        ", @ip " & _
                        ", @machine_name" & _
                        ", @periodunkid " & _
                        ", @medicalrefno " & _
                        ", @disciplinefileunkid " & _
                        ", @membershiptranunkid " & _
                        ", @cumulative_startdate " & _
                        ", @stop_date" & _
                        ", @empbatchpostingunkid " & _
                        ", @edstart_date" & _
                      ", @form_name " & _
                      " " & _
                      " " & _
                      " " & _
                      " " & _
                      " " & _
                      ", @isweb " & _
                    "); SELECT @@identity"
                'Sohail (24 Aug 2019) - [edstart_date]
                'Sohail (24 Feb 2016) - [empbatchpostingunkid]
                'Sohail (09 Nov 2013) - [disciplinefileunkid, membershiptranunkid, cumulative_startdate, stop_date]
                '[isapproved, approveruserunkid - Sohail (14 Jun 2011)]
                'S.SANDEEP [ 19 JULY 2012 ] -- END


                objDataOperation.ClearParameters()
                objDataOperation.AddParameter("@edunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEdunkid.ToString)
                objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeunkid.ToString)
                objDataOperation.AddParameter("@tranheadunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intTranHeadUnkId.ToString)
                objDataOperation.AddParameter("@batchtransactionunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, -1)
                'Sohail (26 Feb 2015) -- Start
                'Issue - Salary head not inserted when current ed slab is exist without salary head and current salary head is defferent then previous period salary head.
                'objDataOperation.AddParameter("@amount", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, dsRow.Item("amount").ToString) 'Sohail (11 May 2011)
                If objTranHead._Calctype_Id = enCalcType.FlatRate_Others Then
                    objDataOperation.AddParameter("@amount", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, decSalaryAmt)
                Else
                    objDataOperation.AddParameter("@amount", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, 0)
                End If
                'Sohail (26 Feb 2015) -- End
                objDataOperation.AddParameter("@isdeduct", SqlDbType.Bit, eZeeDataType.BIT_SIZE, dsRow.Item("isdeduct").ToString)
                objDataOperation.AddParameter("@trnheadtype_id", SqlDbType.Int, eZeeDataType.INT_SIZE, dsRow.Item("trnheadtype_id").ToString)
                objDataOperation.AddParameter("@typeof_id", SqlDbType.Int, eZeeDataType.INT_SIZE, dsRow.Item("typeof_id").ToString)
                objDataOperation.AddParameter("@calctype_id", SqlDbType.Int, eZeeDataType.INT_SIZE, dsRow.Item("calctype_id").ToString)
                objDataOperation.AddParameter("@computeon_id", SqlDbType.Int, eZeeDataType.INT_SIZE, dsRow.Item("computeon_id").ToString)
                objDataOperation.AddParameter("@formula", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, dsRow.Item("formula").ToString)
                objDataOperation.AddParameter("@formulaid", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, dsRow.Item("formulaid").ToString)
                objDataOperation.AddParameter("@currencyunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, dsRow.Item("currencyunkid").ToString)
                objDataOperation.AddParameter("@vendorunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, dsRow.Item("vendorunkid").ToString)
                objDataOperation.AddParameter("@membership_categoryunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, dsRow.Item("membership_categoryunkid"))
                objDataOperation.AddParameter("@audittype", SqlDbType.Int, eZeeDataType.INT_SIZE, 1)
                'Sohail (14 Nov 2017) -- Start
                'Issue - 70.1 - Previous transaction user id was passed to current period transaction.
                'objDataOperation.AddParameter("@audituserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, dsRow.Item("userunkid").ToString)
                objDataOperation.AddParameter("@audituserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, xUserUnkid)
                'Sohail (14 Nov 2017) -- End
                'Sohail (21 Aug 2015) -- Start
                'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                'objDataOperation.AddParameter("@auditdatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, ConfigParameter._Object._CurrentDateAndTime)
                objDataOperation.AddParameter("@auditdatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, dtCurrentDateAndTime)
                'Sohail (21 Aug 2015) -- End
                objDataOperation.AddParameter("@ip", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, getIP)
                objDataOperation.AddParameter("@machine_name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, getHostName)
                objDataOperation.AddParameter("@isapproved", SqlDbType.Bit, eZeeDataType.BIT_SIZE, dsRow.Item("isapproved").ToString)
                'Sohail (14 Nov 2017) -- Start
                'Issue - 70.1 - Previous transaction user id was passed to current period transaction.
                'objDataOperation.AddParameter("@approveruserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, dsRow.Item("approveruserunkid").ToString)
                objDataOperation.AddParameter("@approveruserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, xUserUnkid)
                'Sohail (14 Nov 2017) -- End
                objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intPeriodUnkId.ToString)
                objDataOperation.AddParameter("@medicalrefno", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, dsRow.Item("medicalrefno").ToString)
                'Sohail (09 Nov 2013) -- Start
                'TRA - ENHANCEMENT
                objDataOperation.AddParameter("@disciplinefileunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, dsRow.Item("disciplinefileunkid").ToString)
                objDataOperation.AddParameter("@membershiptranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, dsRow.Item("membershiptranunkid").ToString)
                If dsRow.Item("cumulative_startdate").ToString = Nothing Then
                    objDataOperation.AddParameter("@cumulative_startdate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
                Else
                    objDataOperation.AddParameter("@cumulative_startdate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, dsRow.Item("cumulative_startdate").ToString)
                End If
                If dsRow.Item("stop_date").ToString = Nothing Then
                    objDataOperation.AddParameter("@stop_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
                Else
                    objDataOperation.AddParameter("@stop_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, dsRow.Item("stop_date").ToString)
                End If
                'Sohail (09 Nov 2013) -- End
                'Sohail (24 Aug 2019) -- Start
                'NMB Payroll UAT # TC016 - 76.1 - System should be able to accept future amendments (Future period) and prorate. It should be possible to set start date of the specific transaction and prorate based on the start date. It should also be possible to set start date of the future period.
                If dsRow.Item("edstart_date").ToString = Nothing Then
                    objDataOperation.AddParameter("@edstart_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
                Else
                    objDataOperation.AddParameter("@edstart_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, dsRow.Item("edstart_date").ToString)
                End If
                'Sohail (24 Aug 2019) -- End
                'Sohail (24 Feb 2016) -- Start
                'Enhancement - Batch Posting changes as per online doc in 58.1 SP. (#15 (KBC & Kenya Project Comments List.xls))
                objDataOperation.AddParameter("@empbatchpostingunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, dsRow.Item("empbatchpostingunkid").ToString)
                objDataOperation.AddParameter("@form_name", SqlDbType.NVarChar, 500, mstrFormName)
                objDataOperation.AddParameter("@isweb", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsWeb)

                'S.SANDEEP [ 19 JULY 2012 ] -- START
                'Enhancement : TRA Changes

                'Hemant (07 Jan 2019) -- Start
                'ISSUE/ENHANCEMENT : {Audit Trails} in 76.1.
                'If mstrWebFormName.Trim.Length <= 0 Then
                '    'S.SANDEEP [ 11 AUG 2012 ] -- START
                '    'ENHANCEMENT : TRA CHANGES
                '    'Dim frm As Form
                '    'For Each frm In Application.OpenForms
                '    '    If Form.ActiveForm.Name IsNot Nothing AndAlso Form.ActiveForm.Name = frm.Name Then
                '    '        objDoOp.AddParameter("@form_name", SqlDbType.NVarChar, 500, frm.Name)
                '    '        objDoOp.AddParameter("@isweb", SqlDbType.Bit, eZeeDataType.BIT_SIZE, False)
                '    '        objDoOp.AddParameter("@module_name1", SqlDbType.NVarChar, 500, StrModuleName1)
                '    '    End If
                '    'Next
                '    objDataOperation.AddParameter("@form_name", SqlDbType.NVarChar, 500, mstrForm_Name)
                '    objDataOperation.AddParameter("@isweb", SqlDbType.Bit, eZeeDataType.BIT_SIZE, False)

                '    'S.SANDEEP [ 11 AUG 2012 ] -- END
                'Else
                '    objDataOperation.AddParameter("@form_name", SqlDbType.NVarChar, 500, mstrWebFormName)
                '    objDataOperation.AddParameter("@isweb", SqlDbType.Bit, eZeeDataType.BIT_SIZE, True)
                '    objDataOperation.AddParameter("@module_name1", SqlDbType.NVarChar, 500, Language.getMessage(mstrModuleName, 3, "WEB"))
                'End If
                'Hemant (07 Jan 2019) -- End








                'S.SANDEEP [ 19 JULY 2012 ] -- END

                objDataOperation.ExecNonQuery(strQ)

                If objDataOperation.ErrorMessage <> "" Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If
                'Sohail (18 Jan 2012) -- End

                Exit For
            Next

            'Sohail (26 Feb 2015) -- Start
            'Issue - Salary head not inserted when current ed slab is exist without salary head and current salary head is defferent then previous period salary head.
            objPeriod = Nothing
            objTranHead = Nothing
            'Sohail (26 Feb 2015) -- End

            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Insert_SalaryHead; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
        End Try

    End Function
    'Sohail (13 Jan 2012) -- End

    'Sohail (18 Nov 2016) -- Start
    'Enhancement -  64.1 - Insert Membership heads on Ed if not assigned.
    Public Function IsExist_UnAssigned_MembershipHeads(ByVal xDatabaseName As String _
                                                       , ByVal xUserUnkid As Integer _
                                                       , ByVal xYearUnkid As Integer _
                                                       , ByVal xCompanyUnkid As Integer _
                                                       , ByVal xPeriodStart As DateTime _
                                                       , ByVal xPeriodEnd As DateTime _
                                                       , ByVal xUserModeSetting As String _
                                                       , ByVal xOnlyApproved As Boolean _
                                                       , ByVal xIncludeIn_ActiveEmployee As Boolean _
                                                       , ByVal blnApplyUserAccessFilter As Boolean _
                                                       , ByVal intPeriodUnkId As Integer _
                                                       , ByVal xDataOp As clsDataOperation _
                                                       , ByVal strFilter As String _
                                                       , Optional ByVal strEmpIDs As String = "" _
                                                       , Optional ByVal intExceptEmpId As Integer = 0 _
                                                       , Optional ByVal blnApplyEmpDateFilter As Boolean = True _
                                                       , Optional ByVal strAdvanceJoinFilter As String = "" _
                                                       ) As Boolean
        'Sohail (12 Oct 2021) - [strAdvanceJoinFilter]
        'Sohail (25 Jun 2020) - [strEmpIDs, intExceptEmpId, blnApplyEmpDateFilter]
        'Sohail (01 Mar 2019) - [objDataOperation] = [xDataOp]
        'Sohail (20 Jul 2018) - [strFilter]

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        Try
            Dim xDateJoinQry, xDateFilterQry, xUACQry, xUACFiltrQry, xAdvanceJoinQry As String
            xDateJoinQry = "" : xDateFilterQry = "" : xUACQry = "" : xUACFiltrQry = "" : xAdvanceJoinQry = ""
            If blnApplyEmpDateFilter = True Then 'Sohail (25 Jun 2020)
            Call GetDatesFilterString(xDateJoinQry, xDateFilterQry, xPeriodStart, xPeriodEnd, , , xDatabaseName)
            End If 'Sohail (25 Jun 2020)
            If blnApplyUserAccessFilter = True Then 'Sohail (25 Jun 2020)
            Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, xPeriodEnd, xOnlyApproved, xDatabaseName, xUserUnkid, xCompanyUnkid, xYearUnkid, xUserModeSetting)
            End If 'Sohail (25 Jun 2020)
            If strAdvanceJoinFilter.Trim.Length > 0 Then 'Sohail (12 Oct 2021)
            Call GetAdvanceFilterQry(xAdvanceJoinQry, xPeriodEnd, xDatabaseName)
            End If 'Sohail (12 Oct 2021)
            'Sohail (01 Mar 2019) -- Start
            'KBC Support Issue 0003536 - 74.1 - The database is in single user mode on close period.
            'If objDataOperation Is Nothing Then
            '    objDataOperation = New clsDataOperation
            'End If
            If xDataOp IsNot Nothing Then
                objDataOperation = xDataOp
            Else
                objDataOperation = New clsDataOperation
            End If
            'Sohail (01 Mar 2019) -- End
            objDataOperation.ClearParameters()

            'Sohail (25 Jun 2020) -- Start
            'NMB Enhancement # : Performance enhancement on Import earning deduction heads.
            strQ = "SELECT   hremployee_master.employeeunkid " & _
                   "INTO #TableEmp " & _
                   "FROM hremployee_master "

            If xDateJoinQry.Trim.Length > 0 Then
                strQ &= xDateJoinQry
            End If

            If blnApplyUserAccessFilter = True Then
                If xUACQry.Trim.Length > 0 Then
                    strQ &= xUACQry
                End If
            End If

            If xAdvanceJoinQry.Trim.Length > 0 Then
                strQ &= xAdvanceJoinQry
            End If

            strQ &= "WHERE 1 = 1 "

            If strEmpIDs.Trim <> "" Then
                strQ &= " AND hremployee_master.employeeunkid IN (" & strEmpIDs & ") "
            End If

            If intExceptEmpId > 0 Then
                strQ &= " AND hremployee_master.employeeunkid <> @exceptemployeeunkid "
                objDataOperation.AddParameter("@exceptemployeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intExceptEmpId)
            End If

            If xIncludeIn_ActiveEmployee = False Then
                If xDateFilterQry.Trim.Length > 0 Then
                    strQ &= xDateFilterQry
                End If
            End If
            'Sohail (25 Jun 2020) -- End

            'Sohail (18 Mar 2017) -- Start
            'Performance Issue -  65.1 - Taking 7 seconds for emp '744' of OGE for March 2017 period and taking 46 mins for 408 employees.
            'strQ = "SELECT  hremployee_meminfo_tran.membershiptranunkid  " & _
            '              ", hremployee_meminfo_tran.employeeunkid " & _
            '              ", hremployee_meminfo_tran.membership_categoryunkid " & _
            '              ", hremployee_meminfo_tran.membershipunkid " & _
            '              ", hrmembership_master.emptranheadunkid " & _
            '              ", hrmembership_master.cotranheadunkid " & _
            '        "FROM    hremployee_meminfo_tran " & _
            '                "LEFT JOIN hrmembership_master ON hremployee_meminfo_tran.membershipunkid = hrmembership_master.membershipunkid " & _
            '                "LEFT JOIN hremployee_master ON hremployee_master.employeeunkid = hremployee_meminfo_tran.employeeunkid " & _
            '                "LEFT JOIN ( SELECT  membershipcategoryunkid  " & _
            '                                  ", membershipunkid " & _
            '                                  ", employeeunkid " & _
            '                                  ", tranheadunkid " & _
            '                                  ", periodunkid " & _
            '                            "FROM    hrmembership_master " & _
            '                                    "JOIN prearningdeduction_master ON prearningdeduction_master.tranheadunkid = hrmembership_master.emptranheadunkid " & _
            '                            "WHERE   prearningdeduction_master.isvoid = 0 " & _
            '                                    "AND isactive = 1 " & _
            '                                    "AND prearningdeduction_master.periodunkid = @periodunkid " & _
            '                            "UNION " & _
            '                            "SELECT  membershipcategoryunkid  " & _
            '                                  ", membershipunkid " & _
            '                                  ", employeeunkid " & _
            '                                  ", cotranheadunkid " & _
            '                                  ", periodunkid " & _
            '                            "FROM    hrmembership_master " & _
            '                                    "JOIN prearningdeduction_master ON prearningdeduction_master.tranheadunkid = hrmembership_master.cotranheadunkid " & _
            '                            "WHERE   prearningdeduction_master.isvoid = 0 " & _
            '                                    "AND isactive = 1 " & _
            '                                    "AND prearningdeduction_master.periodunkid = @periodunkid " & _
            '                          ") AS A ON hremployee_meminfo_tran.employeeunkid = A.employeeunkid " & _
            '                                    "AND hremployee_meminfo_tran.membership_categoryunkid = A.membershipcategoryunkid " & _
            '                                    "AND hremployee_meminfo_tran.membershipunkid = A.membershipunkid " & _
            '                "LEFT JOIN prearningdeduction_master ON A.employeeunkid = prearningdeduction_master.employeeunkid " & _
            '                                                       "AND prearningdeduction_master.tranheadunkid = A.tranheadunkid " & _
            '                                                       "AND prearningdeduction_master.periodunkid = A.periodunkid " & _
            '                                                       "AND prearningdeduction_master.isvoid = 0 "
            strQ &= " SELECT  membershipcategoryunkid  " & _
                                              ", membershipunkid " & _
                                              ", prearningdeduction_master.employeeunkid " & _
                                              ", prearningdeduction_master.tranheadunkid " & _
                                              ", prearningdeduction_master.periodunkid " & _
                    "INTO    #cte " & _
                                        "FROM    hrmembership_master " & _
                                                "JOIN prearningdeduction_master ON prearningdeduction_master.tranheadunkid = hrmembership_master.emptranheadunkid " & _
                                                "JOIN #TableEmp ON prearningdeduction_master.employeeunkid = #TableEmp.employeeunkid " & _
                                        "WHERE   prearningdeduction_master.isvoid = 0 " & _
                                                "AND isactive = 1 " & _
                                                "AND prearningdeduction_master.periodunkid = @periodunkid " & _
                                        "UNION " & _
                                        "SELECT  membershipcategoryunkid  " & _
                                              ", membershipunkid " & _
                                              ", prearningdeduction_master.employeeunkid " & _
                                              ", cotranheadunkid " & _
                                              ", prearningdeduction_master.periodunkid " & _
                                        "FROM    hrmembership_master " & _
                                                "JOIN prearningdeduction_master ON prearningdeduction_master.tranheadunkid = hrmembership_master.cotranheadunkid " & _
                                                "JOIN #TableEmp ON prearningdeduction_master.employeeunkid = #TableEmp.employeeunkid " & _
                                        "WHERE   prearningdeduction_master.isvoid = 0 " & _
                                                "AND isactive = 1 " & _
                            "AND prearningdeduction_master.periodunkid = @periodunkid "
            'Sohail (25 Jun 2020) - [JOIN #TableEmp]

            'Sohail (20 Jul 2018) -- Start
            'Support Issue # 0002383 : VOLTAMP: Unable to assign Transaction heads using batch entry in Payroll transactions (Warning message of Membership heads not assigned)in 73.1.
            strQ &= "SELECT DISTINCT " & _
                        "prearningdeduction_master.employeeunkid " & _
                    "INTO #EMP " & _
                    "FROM prearningdeduction_master " & _
                    "JOIN #TableEmp ON prearningdeduction_master.employeeunkid = #TableEmp.employeeunkid " & _
                    "WHERE isvoid = 0 " & _
                          "AND periodunkid = @periodunkid "
            'Sohail (20 Jul 2018) -- End

            strQ &= "SELECT  hremployee_meminfo_tran.membershiptranunkid  " & _
                          ", hremployee_meminfo_tran.employeeunkid " & _
                          ", hremployee_meminfo_tran.membership_categoryunkid " & _
                          ", hremployee_meminfo_tran.membershipunkid " & _
                          ", hrmembership_master.emptranheadunkid " & _
                          ", hrmembership_master.cotranheadunkid " & _
                    "FROM    hremployee_meminfo_tran " & _
                            "LEFT JOIN hrmembership_master ON hremployee_meminfo_tran.membershipunkid = hrmembership_master.membershipunkid " & _
                            "JOIN #TableEmp ON hremployee_meminfo_tran.employeeunkid = #TableEmp.employeeunkid "
            'Sohail (25 Jun 2020) - [LEFT JOIN hremployee_master ON hremployee_master.employeeunkid = hremployee_meminfo_tran.employeeunkid] = [JOIN #TableEmp ON hremployee_meminfo_tran.employeeunkid = #TableEmp.employeeunkid]
            'Sohail (18 Mar 2017) -- End

            'Sohail (18 Mar 2017) -- Start
            'Performance Issue -  65.1 - Taking 7 seconds for emp '744' of OGE for March 2017 period and taking 46 mins for 408 employees.
            strQ &= "LEFT JOIN #cte AS A ON hremployee_meminfo_tran.employeeunkid = A.employeeunkid " & _
                                    "AND hremployee_meminfo_tran.membership_categoryunkid = A.membershipcategoryunkid " & _
                                    "AND hremployee_meminfo_tran.membershipunkid = A.membershipunkid " & _
                    "LEFT JOIN prearningdeduction_master ON A.employeeunkid = prearningdeduction_master.employeeunkid " & _
                                                           "AND prearningdeduction_master.tranheadunkid = A.tranheadunkid " & _
                                                           "AND prearningdeduction_master.periodunkid = A.periodunkid " & _
                                                           "AND prearningdeduction_master.isvoid = 0 "
            'Sohail (18 Mar 2017) -- End

            'Sohail (20 Jul 2018) -- Start
            'Support Issue # 0002383 : VOLTAMP: Unable to assign Transaction heads using batch entry in Payroll transactions (Warning message of Membership heads not assigned)in 73.1.
            'Sohail (25 Jun 2020) -- Start
            'NMB Enhancement # : Performance enhancement on Import earning deduction heads.
            'strQ &= "JOIN #EMP ON #EMP.employeeunkid = hremployee_master.employeeunkid "
            strQ &= "JOIN #EMP ON #EMP.employeeunkid = hremployee_meminfo_tran.employeeunkid "
            'Sohail (25 Jun 2020) -- End
            'Sohail (20 Jul 2018) -- End

            'Sohail (25 Jun 2020) -- Start
            'NMB Enhancement # : Performance enhancement on Import earning deduction heads.
            'If xDateJoinQry.Trim.Length > 0 Then
            '    strQ &= xDateJoinQry
            'End If

            'If xUACQry.Trim.Length > 0 Then
            '    strQ &= xUACQry
            'End If

            'If xAdvanceJoinQry.Trim.Length > 0 Then
            '    strQ &= xAdvanceJoinQry
            'End If
            'Sohail (25 Jun 2020) -- End

            strQ &= "    WHERE   hremployee_meminfo_tran.isactive = 1 " & _
                            "AND hrmembership_master.isactive = 1 " & _
                            "AND hremployee_meminfo_tran.isdeleted = 0 " & _
                            "AND ( hrmembership_master.emptranheadunkid > 0 " & _
                                  "OR hrmembership_master.cotranheadunkid > 0 " & _
                                ") " & _
                            "AND A.membershipunkid IS NULL "

            'Sohail (20 Jul 2018) -- Start
            'Support Issue # 0002383 : VOLTAMP: Unable to assign Transaction heads using batch entry in Payroll transactions (Warning message of Membership heads not assigned)in 73.1.
            If strFilter.Trim.Length > 0 Then
                strQ &= " " & strFilter & " "
            End If
            'Sohail (20 Jul 2018) -- End

            'If blnApplyUserAccessFilter = True Then
            '    If xUACFiltrQry.Trim.Length > 0 Then
            '        strQ &= " AND " & xUACFiltrQry
            '    End If
            'End If

            'If xIncludeIn_ActiveEmployee = False Then
            'Sohail (25 Jun 2020) -- Start
            'NMB Enhancement # : Performance enhancement on Import earning deduction heads.
            'If xDateFilterQry.Trim.Length > 0 Then
            '    strQ &= xDateFilterQry
            'End If
            'Sohail (25 Jun 2020) -- End
            'End If

            strQ &= " DROP TABLE #cte " 'Sohail (18 Mar 2017)
            'Sohail (20 Jul 2018) -- Start
            'Support Issue # 0002383 : VOLTAMP: Unable to assign Transaction heads using batch entry in Payroll transactions (Warning message of Membership heads not assigned)in 73.1.
            strQ &= " DROP TABLE #EMP "
            'Sohail (20 Jul 2018) -- End
            'Sohail (25 Jun 2020) -- Start
            'NMB Enhancement # : Performance enhancement on Import earning deduction heads.
            strQ &= " DROP TABLE #TableEmp "
            'Sohail (25 Jun 2020) -- End

            objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intPeriodUnkId)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If


            Return dsList.Tables("List").Rows.Count > 0

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: IsExist_UnAssigned_MembershipHeads; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            'Sohail (01 Mar 2019) -- Start
            'KBC Support Issue 0003536 - 74.1 - The database is in single user mode on close period.
            If xDataOp Is Nothing Then objDataOperation = Nothing
            'Sohail (01 Mar 2019) -- End
        End Try
    End Function

    Public Function GetList_UnAssigned_MembershipHeads(ByVal xDatabaseName As String _
                                                       , ByVal xUserUnkid As Integer _
                                                       , ByVal xYearUnkid As Integer _
                                                       , ByVal xCompanyUnkid As Integer _
                                                       , ByVal xPeriodStart As DateTime _
                                                       , ByVal xPeriodEnd As DateTime _
                                                       , ByVal xUserModeSetting As String _
                                                       , ByVal xOnlyApproved As Boolean _
                                                       , ByVal xIncludeIn_ActiveEmployee As Boolean _
                                                       , ByVal blnApplyUserAccessFilter As Boolean _
                                                       , ByVal strList As String _
                                                       , ByVal intPeriodUnkId As Integer _
                                                       , ByVal xDataOp As clsDataOperation _
                                                       , ByVal strFilter As String _
                                                       , Optional ByVal strEmpIDs As String = "" _
                                                       , Optional ByVal intExceptEmpId As Integer = 0 _
                                                       , Optional ByVal blnApplyEmpDateFilter As Boolean = True _
                                                       ) As DataSet
        'Sohail (25 Jun 2020) - [strEmpIDs, intExceptEmpId, blnApplyEmpDateFilter]
        'Sohail (01 Mar 2019) - [objDataOperation] = [xDataOp]
        'Sohail (20 Jul 2018) - [strFilter]

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        Try
            Dim xDateJoinQry, xDateFilterQry, xUACQry, xUACFiltrQry, xAdvanceJoinQry As String
            xDateJoinQry = "" : xDateFilterQry = "" : xUACQry = "" : xUACFiltrQry = "" : xAdvanceJoinQry = ""
            If blnApplyEmpDateFilter = True Then 'Sohail (25 Jun 2020)
            Call GetDatesFilterString(xDateJoinQry, xDateFilterQry, xPeriodStart, xPeriodEnd, , , xDatabaseName)
            End If 'Sohail (25 Jun 2020)
            If blnApplyUserAccessFilter = True Then 'Sohail (25 Jun 2020)
            Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, xPeriodEnd, xOnlyApproved, xDatabaseName, xUserUnkid, xCompanyUnkid, xYearUnkid, xUserModeSetting)
            End If 'Sohail (25 Jun 2020)
            Call GetAdvanceFilterQry(xAdvanceJoinQry, xPeriodEnd, xDatabaseName)

            'Sohail (01 Mar 2019) -- Start
            'KBC Support Issue 0003536 - 74.1 - The database is in single user mode on close period.
            'If objDataOperation Is Nothing Then
            '    objDataOperation = New clsDataOperation
            'End If
            If xDataOp IsNot Nothing Then
                objDataOperation = xDataOp
            Else
                objDataOperation = New clsDataOperation
            End If
            'Sohail (01 Mar 2019) -- End
            objDataOperation.ClearParameters()

            'Sohail (25 Jun 2020) -- Start
            'NMB Enhancement # : Performance enhancement on Import earning deduction heads.
            strQ = "SELECT   hremployee_master.employeeunkid " & _
                          ", hremployee_master.employeecode " & _
                          ", hremployee_master.firstname + ' ' + ISNULL(hremployee_master.othername, '') + ' ' + hremployee_master.surname AS employeename " & _
                   "INTO #TableEmp " & _
                   "FROM hremployee_master "

            If xDateJoinQry.Trim.Length > 0 Then
                strQ &= xDateJoinQry
            End If

            If blnApplyUserAccessFilter = True Then
                If xUACQry.Trim.Length > 0 Then
                    strQ &= xUACQry
                End If
            End If

            If xAdvanceJoinQry.Trim.Length > 0 Then
                strQ &= xAdvanceJoinQry
            End If

            strQ &= "WHERE 1 = 1 "

            If strEmpIDs.Trim <> "" Then
                strQ &= " AND hremployee_master.employeeunkid IN (" & strEmpIDs & ") "
            End If

            If intExceptEmpId > 0 Then
                strQ &= " AND hremployee_master.employeeunkid <> @exceptemployeeunkid "
                objDataOperation.AddParameter("@exceptemployeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intExceptEmpId)
            End If

            If xIncludeIn_ActiveEmployee = False Then
                If xDateFilterQry.Trim.Length > 0 Then
                    strQ &= xDateFilterQry
                End If
            End If
            'Sohail (25 Jun 2020) -- End

            'Sohail (18 Mar 2017) -- Start
            'Performance Issue -  65.1 - Taking 7 seconds for emp '744' of OGE for March 2017 period and taking 46 mins for 408 employees.
            'strQ = "SELECT  hremployee_meminfo_tran.membershiptranunkid  " & _
            '              ", hremployee_meminfo_tran.employeeunkid " & _
            '              ", hremployee_master.employeecode " & _
            '              ", hremployee_master.firstname + ' ' + ISNULL(hremployee_master.othername, '') + ' ' + hremployee_master.surname AS employeename " & _
            '              ", hremployee_meminfo_tran.membership_categoryunkid " & _
            '              ", cfcommon_master.code AS MemCategoryCode " & _
            '              ", cfcommon_master.name AS MemCategoryName " & _
            '              ", hremployee_meminfo_tran.membershipunkid " & _
            '              ", hrmembership_master.membershipcode " & _
            '              ", hrmembership_master.membershipname " & _
            '              ", hrmembership_master.emptranheadunkid " & _
            '              ", ISNULL(EmpHead.trnheadname, '') AS empheadname " & _
            '              ", hrmembership_master.cotranheadunkid " & _
            '              ", ISNULL(CoHead.trnheadname, '') AS coheadname " & _
            '        "FROM    hremployee_meminfo_tran " & _
            '                "LEFT JOIN hrmembership_master ON hremployee_meminfo_tran.membershipunkid = hrmembership_master.membershipunkid " & _
            '                "LEFT JOIN hremployee_master ON hremployee_master.employeeunkid = hremployee_meminfo_tran.employeeunkid " & _
            '                "LEFT JOIN prtranhead_master AS EmpHead ON EmpHead.tranheadunkid = hrmembership_master.emptranheadunkid AND EmpHead.isvoid = 0 " & _
            '                "LEFT JOIN prtranhead_master AS CoHead ON CoHead.tranheadunkid = hrmembership_master.cotranheadunkid AND CoHead.isvoid = 0 " & _
            '                "LEFT JOIN cfcommon_master ON cfcommon_master.masterunkid = hremployee_meminfo_tran.membership_categoryunkid AND cfcommon_master.isactive = 1 AND cfcommon_master.mastertype = " & clsCommon_Master.enCommonMaster.MEMBERSHIP_CATEGORY & " " & _
            '                "LEFT JOIN ( SELECT  membershipcategoryunkid  " & _
            '                                  ", membershipunkid " & _
            '                                  ", employeeunkid " & _
            '                                  ", tranheadunkid " & _
            '                                  ", periodunkid " & _
            '                            "FROM    hrmembership_master " & _
            '                                    "JOIN prearningdeduction_master ON prearningdeduction_master.tranheadunkid = hrmembership_master.emptranheadunkid " & _
            '                            "WHERE   prearningdeduction_master.isvoid = 0 " & _
            '                                    "AND isactive = 1 " & _
            '                                    "AND prearningdeduction_master.periodunkid = @periodunkid " & _
            '                            "UNION " & _
            '                            "SELECT  membershipcategoryunkid  " & _
            '                                  ", membershipunkid " & _
            '                                  ", employeeunkid " & _
            '                                  ", cotranheadunkid " & _
            '                                  ", periodunkid " & _
            '                            "FROM    hrmembership_master " & _
            '                                    "JOIN prearningdeduction_master ON prearningdeduction_master.tranheadunkid = hrmembership_master.cotranheadunkid " & _
            '                            "WHERE   prearningdeduction_master.isvoid = 0 " & _
            '                                    "AND isactive = 1 " & _
            '                                    "AND prearningdeduction_master.periodunkid = @periodunkid " & _
            '                          ") AS A ON hremployee_meminfo_tran.employeeunkid = A.employeeunkid " & _
            '                                    "AND hremployee_meminfo_tran.membership_categoryunkid = A.membershipcategoryunkid " & _
            '                                    "AND hremployee_meminfo_tran.membershipunkid = A.membershipunkid " & _
            '                "LEFT JOIN prearningdeduction_master ON A.employeeunkid = prearningdeduction_master.employeeunkid " & _
            '                                                       "AND prearningdeduction_master.tranheadunkid = A.tranheadunkid " & _
            '                                                       "AND prearningdeduction_master.periodunkid = A.periodunkid " & _
            '                                                       "AND prearningdeduction_master.isvoid = 0 "
            strQ &= "SELECT  membershipcategoryunkid  " & _
                                              ", membershipunkid " & _
                                              ", prearningdeduction_master.employeeunkid " & _
                                              ", prearningdeduction_master.tranheadunkid " & _
                                              ", prearningdeduction_master.periodunkid " & _
                    "INTO    #cte " & _
                                        "FROM    hrmembership_master " & _
                                                "JOIN prearningdeduction_master ON prearningdeduction_master.tranheadunkid = hrmembership_master.emptranheadunkid " & _
                                                "JOIN #TableEmp ON prearningdeduction_master.employeeunkid = #TableEmp.employeeunkid " & _
                                        "WHERE   prearningdeduction_master.isvoid = 0 " & _
                                                "AND isactive = 1 " & _
                                                "AND prearningdeduction_master.periodunkid = @periodunkid " & _
                                        "UNION " & _
                                        "SELECT  membershipcategoryunkid  " & _
                                              ", membershipunkid " & _
                                              ", prearningdeduction_master.employeeunkid " & _
                                              ", cotranheadunkid " & _
                                              ", prearningdeduction_master.periodunkid " & _
                                        "FROM    hrmembership_master " & _
                                                "JOIN prearningdeduction_master ON prearningdeduction_master.tranheadunkid = hrmembership_master.cotranheadunkid " & _
                                                "JOIN #TableEmp ON prearningdeduction_master.employeeunkid = #TableEmp.employeeunkid " & _
                                        "WHERE   prearningdeduction_master.isvoid = 0 " & _
                                                "AND isactive = 1 " & _
                            "AND prearningdeduction_master.periodunkid = @periodunkid "
            'Sohail (25 Jun 2020) - [JOIN #TableEmp]

            'Sohail (20 Jul 2018) -- Start
            'Support Issue # 0002383 : VOLTAMP: Unable to assign Transaction heads using batch entry in Payroll transactions (Warning message of Membership heads not assigned)in 73.1.
            strQ &= "SELECT DISTINCT " & _
                        "prearningdeduction_master.employeeunkid " & _
                    "INTO #EMP " & _
                    "FROM prearningdeduction_master " & _
                    "JOIN #TableEmp ON prearningdeduction_master.employeeunkid = #TableEmp.employeeunkid " & _
                    "WHERE isvoid = 0 " & _
                          "AND periodunkid = @periodunkid "
            'Sohail (20 Jul 2018) -- End

            strQ &= "SELECT  hremployee_meminfo_tran.membershiptranunkid  " & _
                          ", hremployee_meminfo_tran.employeeunkid " & _
                          ", #TableEmp.employeecode " & _
                          ", #TableEmp.employeename " & _
                          ", hremployee_meminfo_tran.membership_categoryunkid " & _
                          ", cfcommon_master.code AS MemCategoryCode " & _
                          ", cfcommon_master.name AS MemCategoryName " & _
                          ", hremployee_meminfo_tran.membershipunkid " & _
                          ", hrmembership_master.membershipcode " & _
                          ", hrmembership_master.membershipname " & _
                          ", hrmembership_master.emptranheadunkid " & _
                          ", ISNULL(EmpHead.trnheadname, '') AS empheadname " & _
                          ", hrmembership_master.cotranheadunkid " & _
                          ", ISNULL(CoHead.trnheadname, '') AS coheadname " & _
                    "FROM    hremployee_meminfo_tran " & _
                            "LEFT JOIN hrmembership_master ON hremployee_meminfo_tran.membershipunkid = hrmembership_master.membershipunkid " & _
                            "JOIN #TableEmp ON hremployee_meminfo_tran.employeeunkid = #TableEmp.employeeunkid " & _
                            "LEFT JOIN prtranhead_master AS EmpHead ON EmpHead.tranheadunkid = hrmembership_master.emptranheadunkid AND EmpHead.isvoid = 0 " & _
                            "LEFT JOIN prtranhead_master AS CoHead ON CoHead.tranheadunkid = hrmembership_master.cotranheadunkid AND CoHead.isvoid = 0 " & _
                            "LEFT JOIN cfcommon_master ON cfcommon_master.masterunkid = hremployee_meminfo_tran.membership_categoryunkid AND cfcommon_master.isactive = 1 AND cfcommon_master.mastertype = " & clsCommon_Master.enCommonMaster.MEMBERSHIP_CATEGORY & " "
            'Sohail (25 Jun 2020) - [LEFT JOIN hremployee_master ON hremployee_master.employeeunkid = hremployee_meminfo_tran.employeeunkid] = [JOIN #TableEmp ON hremployee_meminfo_tran.employeeunkid = #TableEmp.employeeunkid]

            'Sohail (18 Mar 2017) -- Start
            'Performance Issue -  65.1 - Taking 7 seconds for emp '744' of OGE for March 2017 period and taking 46 mins for 408 employees.
            strQ &= "LEFT JOIN #cte AS A ON hremployee_meminfo_tran.employeeunkid = A.employeeunkid " & _
                                        "AND hremployee_meminfo_tran.membership_categoryunkid = A.membershipcategoryunkid " & _
                                        "AND hremployee_meminfo_tran.membershipunkid = A.membershipunkid " & _
                    "LEFT JOIN prearningdeduction_master ON A.employeeunkid = prearningdeduction_master.employeeunkid " & _
                                                           "AND prearningdeduction_master.tranheadunkid = A.tranheadunkid " & _
                                                           "AND prearningdeduction_master.periodunkid = A.periodunkid " & _
                                                           "AND prearningdeduction_master.isvoid = 0 "
            'Sohail (18 Mar 2017) -- End

            'Sohail (20 Jul 2018) -- Start
            'Support Issue # 0002383 : VOLTAMP: Unable to assign Transaction heads using batch entry in Payroll transactions (Warning message of Membership heads not assigned)in 73.1.
            'Sohail (25 Jun 2020) -- Start
            'NMB Enhancement # : Performance enhancement on Import earning deduction heads.
            'strQ &= "JOIN #EMP ON #EMP.employeeunkid = hremployee_master.employeeunkid "
            strQ &= "JOIN #EMP ON #EMP.employeeunkid = hremployee_meminfo_tran.employeeunkid "
            'Sohail (25 Jun 2020) -- End
            'Sohail (20 Jul 2018) -- End

            'Sohail (25 Jun 2020) -- Start
            'NMB Enhancement # : Performance enhancement on Import earning deduction heads.
            'If xDateJoinQry.Trim.Length > 0 Then
            '    strQ &= xDateJoinQry
            'End If

            'If xUACQry.Trim.Length > 0 Then
            '    strQ &= xUACQry
            'End If

            'If xAdvanceJoinQry.Trim.Length > 0 Then
            '    strQ &= xAdvanceJoinQry
            'End If
            'Sohail (25 Jun 2020) -- End

            strQ &= "    WHERE   hremployee_meminfo_tran.isactive = 1 " & _
                            "AND hrmembership_master.isactive = 1 " & _
                            "AND hremployee_meminfo_tran.isdeleted = 0 " & _
                            "AND ( hrmembership_master.emptranheadunkid > 0 " & _
                                  "OR hrmembership_master.cotranheadunkid > 0 " & _
                                ") " & _
                            "AND A.membershipunkid IS NULL "

            'Sohail (20 Jul 2018) -- Start
            'Support Issue # 0002383 : VOLTAMP: Unable to assign Transaction heads using batch entry in Payroll transactions (Warning message of Membership heads not assigned)in 73.1.
            If strFilter.Trim.Length > 0 Then
                strQ &= " " & strFilter & " "
            End If

            'If blnApplyUserAccessFilter = True Then
            '    If xUACFiltrQry.Trim.Length > 0 Then
            '        strQ &= " AND " & xUACFiltrQry
            '    End If
            'End If
            'Sohail (20 Jul 2018) -- End

            'If xIncludeIn_ActiveEmployee = False Then
            'Sohail (25 Jun 2020) -- Start
            'NMB Enhancement # : Performance enhancement on Import earning deduction heads.
            'If xDateFilterQry.Trim.Length > 0 Then
            '    strQ &= xDateFilterQry
            'End If
            'Sohail (25 Jun 2020) -- End
            'End If

            strQ &= " DROP TABLE #cte " 'Sohail (18 Mar 2017)
            'Sohail (20 Jul 2018) -- Start
            'Support Issue # 0002383 : VOLTAMP: Unable to assign Transaction heads using batch entry in Payroll transactions (Warning message of Membership heads not assigned)in 73.1.
            strQ &= " DROP TABLE #EMP "
            'Sohail (20 Jul 2018) -- End
            'Sohail (25 Jun 2020) -- Start
            'NMB Enhancement # : Performance enhancement on Import earning deduction heads.
            strQ &= " DROP TABLE #TableEmp "
            'Sohail (25 Jun 2020) -- End

            objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intPeriodUnkId)

            dsList = objDataOperation.ExecQuery(strQ, strList)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If


        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetList_UnAssigned_MembershipHeads; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            'Sohail (01 Mar 2019) -- Start
            'KBC Support Issue 0003536 - 74.1 - The database is in single user mode on close period.
            If xDataOp Is Nothing Then objDataOperation = Nothing
            'Sohail (01 Mar 2019) -- End
        End Try
        Return dsList
    End Function

    Public Function Insert_MembershipHeads(ByVal xDatabaseName As String _
                                            , ByVal xUserUnkid As Integer _
                                            , ByVal xYearUnkid As Integer _
                                            , ByVal xCompanyUnkid As Integer _
                                            , ByVal xPeriodStart As DateTime _
                                            , ByVal xPeriodEnd As DateTime _
                                            , ByVal xUserModeSetting As String _
                                            , ByVal xOnlyApproved As Boolean _
                                            , ByVal xIncludeIn_ActiveEmployee As Boolean _
                                            , ByVal intPeriodUnkId As Integer _
                                            , ByVal blnAllowToApproveEarningDeduction As Boolean _
                                            , ByVal dtTable As DataTable _
                                            , ByVal dtCurrentDateAndTime As Date _
                                            , Optional ByVal xDataOp As clsDataOperation = Nothing _
                                            , Optional ByVal blnApplyUserAccessFilter As Boolean = True _
                                            ) As Boolean
        'Sohail (01 Mar 2019) - [objDataOperation] = [xDataOp]

        Dim objTranHead As New clsTransactionHead
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim blnFlag As Boolean = False

        Try

            'Sohail (01 Mar 2019) -- Start
            'KBC Support Issue 0003536 - 74.1 - The database is in single user mode on close period.
            'If objDataOperation Is Nothing Then
            '    objDataOperation = New clsDataOperation
            'End If
            If xDataOp IsNot Nothing Then
                objDataOperation = xDataOp
            Else
                objDataOperation = New clsDataOperation
            End If
            'Sohail (01 Mar 2019) -- End
            objDataOperation.ClearParameters()

            For Each dtRow As DataRow In dtTable.Rows

                If CInt(dtRow.Item("emptranheadunkid")) > 0 AndAlso isExist(xDatabaseName, CInt(dtRow.Item("employeeunkid")), CInt(dtRow.Item("emptranheadunkid")), , intPeriodUnkId, objDataOperation) = False Then
                    objDataOperation.ClearParameters()

                    mintEmployeeunkid = CInt(dtRow.Item("employeeunkid"))
                    mintTranheadunkid = CInt(dtRow.Item("emptranheadunkid"))
                    objTranHead._xDataOperation = objDataOperation 'Sohail (01 Mar 2019)
                    objTranHead._Tranheadunkid(xDatabaseName) = CInt(dtRow.Item("emptranheadunkid"))
                    mintBatchtransactionunkid = 0

                    mintTrnheadtype_Id = objTranHead._Trnheadtype_Id
                    mintTypeof_Id = objTranHead._Typeof_id
                    mintCalctype_Id = objTranHead._Calctype_Id
                    mintComputeon_Id = objTranHead._Computeon_Id
                    mstrFormula = objTranHead._Formula
                    mstrFormulaid = objTranHead._Formulaid
                    mdecAmount = 0

                    If objTranHead._Trnheadtype_Id = enTranHeadType.EarningForEmployees Then 'enTranHeadType.EarningForEmployees
                        mblnIsdeduct = False
                    ElseIf objTranHead._Trnheadtype_Id = enTranHeadType.EmployersStatutoryContributions OrElse objTranHead._Trnheadtype_Id = enTranHeadType.Informational Then 'enTranHeadType.EmployersStatutoryContributions, Informational
                        mblnIsdeduct = Nothing
                    Else
                        mblnIsdeduct = True
                    End If

                    mintCurrencyunkid = 0
                    mintVendorunkid = 0
                    mintUserunkid = IIf(xUserUnkid = 0, User._Object._Userunkid, xUserUnkid)
                    mblnIsvoid = False
                    mintVoiduserunkid = -1
                    mdtVoiddatetime = Nothing
                    mstrVoidreason = ""
                    mintMembership_Categoryunkid = 0

                    If blnAllowToApproveEarningDeduction = True Then
                        mblnIsapproved = True
                        mintApproveruserunkid = xUserUnkid
                    Else
                        mblnIsapproved = False
                        mintApproveruserunkid = -1
                    End If
                    mintPeriodunkid = intPeriodUnkId
                    mintCostCenterUnkId = 0
                    mstrMedicalrefno = ""
                    mintMembershipTranUnkid = CInt(dtRow.Item("membershiptranunkid"))
                    mintDisciplinefileunkid = 0
                    mdtCumulative_Startdate = Nothing
                    mdtStop_Date = Nothing
                    mintEmpbatchpostingunkid = 0
                    'Sohail (24 Aug 2019) -- Start
                    'NMB Payroll UAT # TC016 - 76.1 - System should be able to accept future amendments (Future period) and prorate. It should be possible to set start date of the specific transaction and prorate based on the start date. It should also be possible to set start date of the future period.
                    mdtEDStart_Date = Nothing
                    'Sohail (24 Aug 2019) -- End

                    'Sohail (20 Jul 2018) -- Start
                    'Support Issue # 0002383 : VOLTAMP: Unable to assign Transaction heads using batch entry in Payroll transactions (Warning message of Membership heads not assigned)in 73.1.
                    'blnFlag = Insert(xDatabaseName, xUserUnkid, xYearUnkid, xCompanyUnkid, xPeriodStart, xPeriodEnd, xUserModeSetting, xOnlyApproved, xIncludeIn_ActiveEmployee, True, dtCurrentDateAndTime, , objDataOperation, blnApplyUserAccessFilter, , False)
                    blnFlag = Insert(xDatabaseName, xUserUnkid, xYearUnkid, xCompanyUnkid, xPeriodStart, xPeriodEnd, xUserModeSetting, xOnlyApproved, xIncludeIn_ActiveEmployee, True, dtCurrentDateAndTime, , objDataOperation, blnApplyUserAccessFilter, "", False, blnAllowToApproveEarningDeduction)
                    'Sohail (20 Jul 2018) -- End

                    If blnFlag = False Then
                        Return False
                    End If

                End If



                If CInt(dtRow.Item("cotranheadunkid")) > 0 AndAlso isExist(xDatabaseName, CInt(dtRow.Item("employeeunkid")), CInt(dtRow.Item("cotranheadunkid")), , intPeriodUnkId, objDataOperation) = False Then
                    objDataOperation.ClearParameters()

                    mintEmployeeunkid = CInt(dtRow.Item("employeeunkid"))
                    mintTranheadunkid = CInt(dtRow.Item("cotranheadunkid"))
                    objTranHead._xDataOperation = objDataOperation 'Sohail (01 Mar 2019)
                    objTranHead._Tranheadunkid(xDatabaseName) = CInt(dtRow.Item("cotranheadunkid"))
                    mintBatchtransactionunkid = 0

                    mintTrnheadtype_Id = objTranHead._Trnheadtype_Id
                    mintTypeof_Id = objTranHead._Typeof_id
                    mintCalctype_Id = objTranHead._Calctype_Id
                    mintComputeon_Id = objTranHead._Computeon_Id
                    mstrFormula = objTranHead._Formula
                    mstrFormulaid = objTranHead._Formulaid
                    mdecAmount = 0

                    If objTranHead._Trnheadtype_Id = enTranHeadType.EarningForEmployees Then 'enTranHeadType.EarningForEmployees
                        mblnIsdeduct = False
                    ElseIf objTranHead._Trnheadtype_Id = enTranHeadType.EmployersStatutoryContributions OrElse objTranHead._Trnheadtype_Id = enTranHeadType.Informational Then 'enTranHeadType.EmployersStatutoryContributions, Informational
                        mblnIsdeduct = Nothing
                    Else
                        mblnIsdeduct = True
                    End If

                    mintCurrencyunkid = 0
                    mintVendorunkid = 0
                    mintUserunkid = IIf(xUserUnkid = 0, User._Object._Userunkid, xUserUnkid)
                    mblnIsvoid = False
                    mintVoiduserunkid = -1
                    mdtVoiddatetime = Nothing
                    mstrVoidreason = ""
                    mintMembership_Categoryunkid = 0

                    If blnAllowToApproveEarningDeduction = True Then
                        mblnIsapproved = True
                        mintApproveruserunkid = xUserUnkid
                    Else
                        mblnIsapproved = False
                        mintApproveruserunkid = -1
                    End If
                    mintPeriodunkid = intPeriodUnkId
                    mintCostCenterUnkId = 0
                    mstrMedicalrefno = ""
                    mintMembershipTranUnkid = CInt(dtRow.Item("membershiptranunkid"))
                    mintDisciplinefileunkid = 0
                    mdtCumulative_Startdate = Nothing
                    mdtStop_Date = Nothing
                    mintEmpbatchpostingunkid = 0
                    'Sohail (24 Aug 2019) -- Start
                    'NMB Payroll UAT # TC016 - 76.1 - System should be able to accept future amendments (Future period) and prorate. It should be possible to set start date of the specific transaction and prorate based on the start date. It should also be possible to set start date of the future period.
                    mdtEDStart_Date = Nothing
                    'Sohail (24 Aug 2019) -- End

                    'Sohail (20 Jul 2018) -- Start
                    'Support Issue # 0002383 : VOLTAMP: Unable to assign Transaction heads using batch entry in Payroll transactions (Warning message of Membership heads not assigned)in 73.1.
                    'blnFlag = Insert(xDatabaseName, xUserUnkid, xYearUnkid, xCompanyUnkid, xPeriodStart, xPeriodEnd, xUserModeSetting, xOnlyApproved, xIncludeIn_ActiveEmployee, True, dtCurrentDateAndTime, True, objDataOperation, blnApplyUserAccessFilter, , False)
                    blnFlag = Insert(xDatabaseName, xUserUnkid, xYearUnkid, xCompanyUnkid, xPeriodStart, xPeriodEnd, xUserModeSetting, xOnlyApproved, xIncludeIn_ActiveEmployee, True, dtCurrentDateAndTime, True, objDataOperation, blnApplyUserAccessFilter, "", False, blnAllowToApproveEarningDeduction)
                    'Sohail (20 Jul 2018) -- End

                    If blnFlag = False Then
                        Return False
                    End If

                End If
            Next


            strQ = "UPDATE  prearningdeduction_master " & _
                    "SET     prearningdeduction_master.membershiptranunkid = hremployee_meminfo_tran.membershiptranunkid " & _
                    "FROM    hremployee_meminfo_tran " & _
                            "JOIN ( SELECT   membershipcategoryunkid  " & _
                                          ", membershipunkid " & _
                                          ", employeeunkid " & _
                                          ", tranheadunkid " & _
                                          ", periodunkid " & _
                                   "FROM     hrmembership_master " & _
                                            "JOIN prearningdeduction_master ON prearningdeduction_master.tranheadunkid = hrmembership_master.emptranheadunkid " & _
                                   "WHERE    prearningdeduction_master.isvoid = 0 " & _
                                            "AND isactive = 1 " & _
                                            "AND prearningdeduction_master.periodunkid = @periodunkid " & _
                                   "UNION " & _
                                   "SELECT   membershipcategoryunkid  " & _
                                          ", membershipunkid " & _
                                          ", employeeunkid " & _
                                          ", cotranheadunkid " & _
                                          ", periodunkid " & _
                                   "FROM     hrmembership_master " & _
                                            "JOIN prearningdeduction_master ON prearningdeduction_master.tranheadunkid = hrmembership_master.emptranheadunkid " & _
                                   "WHERE    prearningdeduction_master.isvoid = 0 " & _
                                            "AND isactive = 1 " & _
                                            "AND prearningdeduction_master.periodunkid = @periodunkid " & _
                                 ") AS A ON hremployee_meminfo_tran.employeeunkid = A.employeeunkid " & _
                                           "AND hremployee_meminfo_tran.membership_categoryunkid = A.membershipcategoryunkid " & _
                                           "AND hremployee_meminfo_tran.isactive = 1 " & _
                                           "AND hremployee_meminfo_tran.membershipunkid = A.membershipunkid " & _
                    "WHERE   prearningdeduction_master.employeeunkid = A.employeeunkid " & _
                            "AND prearningdeduction_master.tranheadunkid = A.tranheadunkid " & _
                            "AND prearningdeduction_master.periodunkid = A.periodunkid " & _
                            "AND prearningdeduction_master.isvoid = 0 " & _
                            "AND prearningdeduction_master.membershiptranunkid <= 0 "

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intPeriodUnkId)

            objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If


            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Insert_MembershipHeads; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            objTranHead = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            'Sohail (01 Mar 2019) -- Start
            'KBC Support Issue 0003536 - 74.1 - The database is in single user mode on close period.
            If xDataOp Is Nothing Then objDataOperation = Nothing
            'Sohail (01 Mar 2019) -- End
        End Try

    End Function
    'Sohail (18 Nov 2016) -- End

    ''' <summary>
    ''' Modify By: Suhail
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Update Database Table (prearningdeduction_master) </purpose>
    Public Function Update(ByVal strDatabaseName As String, ByVal blnGlobal As Boolean _
                           , ByVal xDataOp As clsDataOperation _
                           , ByVal dtCurrentDateAndTime As Date) As Boolean 'Sohail (11 Nov 2010), 'Sohail (23 Apr 2012) - [objDataOperation]
        'Sohail (15 Dec 2018) - [objDataOperation] = [xDataOp]
        'Sohail (21 Aug 2015) - [strDatabaseName, dtCurrentDateAndTime]
        'Public Function Update() As Boolean

        'Sohail (15 Dec 2018) -- Start
        'NMB Enhancement - On salary change screen, create a coonection between new scale text box and change amount text box. i.e if a user types in the new scale, the change amount calculates automatically in 75.1.
        If xDataOp IsNot Nothing Then
            objDataOperation = xDataOp
        Else
            objDataOperation = New clsDataOperation
            objDataOperation.BindTransaction()
        End If
        objDataOperation.ClearParameters()
        'Sohail (15 Dec 2018) -- End

        'Sohail (17 Jan 2012) -- Start
        'TRA - ENHANCEMENT
        'Sohail (23 Apr 2012) -- Start
        'TRA - ENHANCEMENT
        'Sohail (21 Aug 2015) -- Start
        'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
        'If isExist(mintEmployeeunkid, mintTranheadunkid, mintEdunkid, mintPeriodunkid, objDataOperation) Then
        If isExist(strDatabaseName, mintEmployeeunkid, mintTranheadunkid, mintEdunkid, mintPeriodunkid, objDataOperation) Then
            'Sohail (21 Aug 2015) -- End
            'If isExist(mintEmployeeunkid, mintTranheadunkid, mintEdunkid, mintPeriodunkid) Then
            'Sohail (23 Apr 2012) -- End
            'If isExist(mintEmployeeunkid, mintTranheadunkid, mintEdunkid) Then
            'Sohail (17 Jan 2012) -- End
            mstrMessage = Language.getMessage(mstrModuleName, 1, "This Transaction Head already exist.")
            Return False
        End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim blnResult As Boolean 'Sohail (14 Jun 2011)

        'Sohail (15 Dec 2018) -- Start
        'NMB Enhancement - On salary change screen, create a coonection between new scale text box and change amount text box. i.e if a user types in the new scale, the change amount calculates automatically in 75.1.
        'objDataOperation = New clsDataOperation

        ''Sohail (11 Nov 2010) -- Start
        'If blnGlobal = False Then
        '    objDataOperation.BindTransaction()
        'End If
        ''Sohail (11 Nov 2010) -- End
        objDataOperation.ClearParameters()
        'Sohail (15 Dec 2018) -- End

        Try
            objDataOperation.AddParameter("@edunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEdunkid.ToString)
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeunkid.ToString)
            objDataOperation.AddParameter("@tranheadunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintTranheadunkid.ToString)
            objDataOperation.AddParameter("@batchtransactionunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintBatchtransactionunkid.ToString)
            objDataOperation.AddParameter("@amount", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecAmount.ToString) 'Sohail (11 May 2011)
            objDataOperation.AddParameter("@isdeduct", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsdeduct.ToString)
            objDataOperation.AddParameter("@trnheadtype_id", SqlDbType.Int, eZeeDataType.INT_SIZE, mintTrnheadtype_Id.ToString)
            objDataOperation.AddParameter("@typeof_id", SqlDbType.Int, eZeeDataType.INT_SIZE, mintTypeof_Id.ToString)
            objDataOperation.AddParameter("@calctype_id", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCalctype_Id.ToString)
            objDataOperation.AddParameter("@computeon_id", SqlDbType.Int, eZeeDataType.INT_SIZE, mintComputeon_Id.ToString)
            objDataOperation.AddParameter("@formula", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrFormula.ToString)
            objDataOperation.AddParameter("@formulaid", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrFormulaid.ToString)
            objDataOperation.AddParameter("@currencyunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCurrencyunkid.ToString)
            objDataOperation.AddParameter("@vendorunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVendorunkid.ToString)
            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
            If mdtVoiddatetime = Nothing Then
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            Else
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime)
            End If
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)
            objDataOperation.AddParameter("@membership_categoryunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintMembership_Categoryunkid.ToString)
            'Sohail (14 Jun 2011) -- Start
            objDataOperation.AddParameter("@isapproved", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsapproved.ToString)
            objDataOperation.AddParameter("@approveruserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintApproveruserunkid.ToString)
            'Sohail (14 Jun 2011) -- End
            objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPeriodunkid.ToString) 'Sohail (13 Jan 2012)
            objDataOperation.AddParameter("@medicalrefno", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrMedicalrefno.ToString) 'Sohail (18 Jan 2012)

            'S.SANDEEP [ 07 NOV 2011 ] -- START
            'ENHANCEMENT : TRA CHANGES
            objDataOperation.AddParameter("@disciplinefileunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintDisciplinefileunkid.ToString)
            'S.SANDEEP [ 07 NOV 2011 ] -- END
            objDataOperation.AddParameter("@membershiptranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintMembershipTranUnkid.ToString) 'Sohail (30 Mar 2013)
            'Sohail (09 Nov 2013) -- Start
            'TRA - ENHANCEMENT
            If mdtCumulative_Startdate = Nothing Then
                objDataOperation.AddParameter("@cumulative_startdate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            Else
                objDataOperation.AddParameter("@cumulative_startdate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtCumulative_Startdate.ToString)
            End If
            If mdtStop_Date = Nothing Then
                objDataOperation.AddParameter("@stop_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            Else
                objDataOperation.AddParameter("@stop_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtStop_Date.ToString)
            End If
            'Sohail (09 Nov 2013) -- End
            'Sohail (24 Aug 2019) -- Start
            'NMB Payroll UAT # TC016 - 76.1 - System should be able to accept future amendments (Future period) and prorate. It should be possible to set start date of the specific transaction and prorate based on the start date. It should also be possible to set start date of the future period.
            If mdtEDStart_Date = Nothing Then
                objDataOperation.AddParameter("@edstart_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            Else
                objDataOperation.AddParameter("@edstart_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtEDStart_Date.ToString)
            End If
            'Sohail (24 Aug 2019) -- End
            'Sohail (24 Feb 2016) -- Start
            'Enhancement - Batch Posting changes as per online doc in 58.1 SP. (#15 (KBC & Kenya Project Comments List.xls))
            objDataOperation.AddParameter("@empbatchpostingunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmpbatchpostingunkid.ToString)
            'Sohail (24 Feb 2016) -- End

            strQ = "UPDATE prearningdeduction_master SET " & _
              "  employeeunkid = @employeeunkid" & _
              ", tranheadunkid = @tranheadunkid" & _
              ", batchtransactionunkid = @batchtransactionunkid" & _
              ", amount = @amount" & _
              ", isdeduct = @isdeduct" & _
              ", trnheadtype_id = @trnheadtype_id" & _
              ", typeof_id = @typeof_id" & _
              ", calctype_id = @calctype_id" & _
              ", computeon_id = @computeon_id" & _
              ", formula = @formula" & _
              ", formulaid = @formulaid" & _
              ", currencyunkid = @currencyunkid" & _
              ", vendorunkid = @vendorunkid" & _
              ", userunkid = @userunkid" & _
              ", isvoid = @isvoid" & _
              ", voiduserunkid = @voiduserunkid" & _
              ", voiddatetime = @voiddatetime " & _
              ", voidreason = @voidreason " & _
              ", membership_categoryunkid = @membership_categoryunkid " & _
              ", isapproved = @isapproved " & _
              ", approveruserunkid = @approveruserunkid " & _
              ", periodunkid = @periodunkid " & _
              ", medicalrefno = @medicalrefno " & _
              ", disciplinefileunkid = @disciplinefileunkid " & _
              ", membershiptranunkid = @membershiptranunkid " & _
              ", cumulative_startdate = @cumulative_startdate " & _
              ", stop_date = @stop_date " & _
              ", edstart_date = @edstart_date " & _
              ", empbatchpostingunkid = @empbatchpostingunkid " & _
            "WHERE edunkid = @edunkid "
            'Sohail (24 Aug 2019) - [edstart_date]
            'Sohail (24 Feb 2016) - [empbatchpostingunkid]
            'Sohail (09 Nov 2013) - [cumulative_startdate, stop_date]
            '[isapproved, approveruserunkid - Sohail (14 Jun 2011)] , 'S.SANDEEP [ 07 NOV 2011 - disciplinefileunkid ]
            'Sohail (30 Mar 2013) - [membershiptranunkid]

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            'Sohail (11 Nov 2010) -- Start
            'Sohail (14 Jun 2011) -- Start
            'Issue : on error in insertaudittrail record is inserted in main table while not inserted in audit table.
            'Call InsertAuditTrailForEarningDeduction(objDataOperation, 2)
            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'blnResult = InsertAuditTrailForEarningDeduction(objDataOperation, 2)
            If IsTableDataUpdate(mintEdunkid, objDataOperation) = True Then 'Sohail (21 Jan 2022)
            blnResult = InsertAuditTrailForEarningDeduction(objDataOperation, 2, dtCurrentDateAndTime)
                'Sohail (21 Jan 2022) -- Start
                'Enhancement :  : AT Log data optimization on earning and deduction.
            Else
                blnResult = True
            End If
            'Sohail (21 Jan 2022) -- End
            'Sohail (21 Aug 2015) -- End
            'Sohail (14 Jun 2011) -- End

            'Sohail (08 Nov 2011) -- Start
            'Sohail (26 Apr 2018) -- Start
            'Internal Issue : Cost center was getting updated in closed period as well in 71.1.
            'If blnResult = True AndAlso mintCostCenterUnkId > 0 Then
            'Dim objEmpCC As New clsemployee_costcenter_Tran
            'With objEmpCC
            '    'Sohail (29 Mar 2017) -- Start
            '    'CCBRT Enhancement - 66.1 - Employee cost center distribution for non-salary heads, Effective period option on Employee Cost Center.
            '    'Dim intEmpCCID As Integer = .GetUnkID(mintEmployeeunkid, mintTranheadunkid)
            '    Dim objPeriod As New clscommom_period_Tran
            '    objPeriod._Periodunkid(strDatabaseName) = mintPeriodunkid
            '    Dim intEmpCCID As Integer = .GetUnkID(mintEmployeeunkid, mintTranheadunkid, objPeriod._End_Date)
            '    'Sohail (29 Mar 2017) -- End
            '    If intEmpCCID > 0 Then
            '        'Sohail (21 Aug 2015) -- Start
            '        'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            '        '._Costcentertranunkid = intEmpCCID
            '        ._Costcentertranunkid(strDatabaseName) = intEmpCCID
            '        'Sohail (21 Aug 2015) -- End

            '    End If
            '    ._Employeeunkid = mintEmployeeunkid
            '    ._Tranheadunkid = mintTranheadunkid
            '    ._Costcenterunkid = mintCostCenterUnkId
            '    ._Userunkid = mintUserunkid
            '    ._Isvoid = mblnIsvoid
            '    ._Voiduserunkid = mintVoiduserunkid
            '    ._Voiddatetime = mdtVoiddatetime
            '    ._Voidreason = mstrVoidreason
            '    ._DatabaseName = mstrDatabaseName 'Sohail (03 Dec 2013)
            '    'Sohail (29 Mar 2017) -- Start
            '    'CCBRT Enhancement - 66.1 - Employee cost center distribution for non-salary heads, Effective period option on Employee Cost Center.
            '    ._Periodunkid = mintPeriodunkid
            '    'Sohail (29 Mar 2017) -- End
            '    If intEmpCCID > 0 Then
            '        'Sohail (03 Dec 2013) -- Start
            '        'Enhancement - TBC
            '        'blnResult = .Update()
            '        'Sohail (21 Aug 2015) -- Start
            '        'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            '        'blnResult = .Update(objDataOperation)
            '        blnResult = .Update(strDatabaseName, objDataOperation, dtCurrentDateAndTime)
            '        'Sohail (21 Aug 2015) -- End
            '        'Sohail (03 Dec 2013) -- End
            '    Else
            '        'Sohail (03 Dec 2013) -- Start
            '        'Enhancement - TBC
            '        'blnResult = .Insert()
            '        'Sohail (21 Aug 2015) -- Start
            '        'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            '        'blnResult = .Insert(objDataOperation)
            '        blnResult = .Insert(strDatabaseName, objDataOperation, dtCurrentDateAndTime)
            '        'Sohail (21 Aug 2015) -- End
            '        'Sohail (03 Dec 2013) -- End
            '    End If
            'End With
            'objEmpCC = Nothing
            'End If
            ''Sohail (08 Nov 2011) -- End
            'Sohail (19 Dec 2018) -- Start
            'Internal Issue - 76.1 - Employee Cost center get removed ob Editing Earning Deduction when distributed (multiple) cost center has been set for any head.
            'If blnResult = True Then
            If blnResult = True AndAlso mintCostCenterUnkId > 0 Then
                'Sohail (19 Dec 2018) -- End
                Dim objEmpCC As New clsemployee_costcenter_Tran
                With objEmpCC
                    Dim objPeriod As New clscommom_period_Tran
                    objPeriod._xDataOperation = objDataOperation
                    objPeriod._Periodunkid(strDatabaseName) = mintPeriodunkid
                    'Sohail (19 Dec 2018) -- Start
                    'Internal Issue - 76.1 - Employee Cost center get removed ob Editing Earning Deduction when distributed (multiple) cost center has been set for any head.
                    'Dim intEmpCCID As Integer = .GetUnkID(mintEmployeeunkid, mintTranheadunkid, objPeriod._End_Date)
                    'If intEmpCCID > 0 Then
                    '    ._Costcentertranunkid(strDatabaseName) = intEmpCCID
                    'End If
                    Dim intRecCount As Integer = 0
                    'Sohail (07 Feb 2019) -- Start
                    'NMB Enhancement - 76.1 - Employee cost centre should be distributed on all allocations instead of only on cost centre.
                    'Dim intEmpCCID As Integer = .GetUnkID(mintEmployeeunkid, mintTranheadunkid, objPeriod._End_Date, objDataOperation, intRecCount)
                    Dim intAllocationById As Integer = enAllocation.COST_CENTER
                    Dim intEmpCCID As Integer = .GetUnkID(mintEmployeeunkid, mintTranheadunkid, objPeriod._End_Date, objDataOperation, intRecCount, intAllocationById)
                    'Sohail (07 Feb 2019) -- End
                    If intEmpCCID > 0 Then
                        ._objDataOp = objDataOperation
                        ._Costcentertranunkid(strDatabaseName) = intEmpCCID
                    End If
                    'Sohail (19 Dec 2018) -- End

                    'Sohail (19 Dec 2018) -- Start
                    'Internal Issue - 76.1 - Employee Cost center get removed ob Editing Earning Deduction when distributed (multiple) cost center has been set for any head.
                    'If (intEmpCCID > 0 AndAlso ._Costcenterunkid <> mintCostCenterUnkId) OrElse mintCostCenterUnkId > 0 Then
                    If intRecCount <= 1 AndAlso ((intEmpCCID <= 0 AndAlso mintCostCenterUnkId > 0) OrElse (intEmpCCID > 0 AndAlso ._Costcenterunkid <> mintCostCenterUnkId)) Then
                        'Sohail (19 Dec 2018) -- End
                    ._Employeeunkid = mintEmployeeunkid
                    ._Tranheadunkid = mintTranheadunkid
                    ._Costcenterunkid = mintCostCenterUnkId
                    ._Userunkid = mintUserunkid
                    ._Isvoid = mblnIsvoid
                    ._Voiduserunkid = mintVoiduserunkid
                    ._Voiddatetime = mdtVoiddatetime
                    ._Voidreason = mstrVoidreason
                        ._DatabaseName = mstrDatabaseName
                        If intEmpCCID > 0 Then
                            If ._Periodunkid <> mintPeriodunkid Then
                                intEmpCCID = 0
                            End If
                            ._Periodunkid = mintPeriodunkid
                        Else
                    ._Periodunkid = mintPeriodunkid
                        End If

                        ._FormName = mstrFormName
                        ._LoginEmployeeUnkid = 0
                        ._ClientIP = getIP()
                        ._HostName = getHostName()
                        ._FromWeb = False
                        ._AuditUserId = User._Object._Userunkid
                        ._AuditDate = ConfigParameter._Object._CurrentDateAndTime

                    If intEmpCCID > 0 Then
                        blnResult = .Update(strDatabaseName, objDataOperation, dtCurrentDateAndTime)
                    Else
                            'Sohail (07 Feb 2019) -- Start
                            'NMB Enhancement - 76.1 - Employee cost centre should be distributed on all allocations instead of only on cost centre.
                            ._AllocationbyId = intAllocationById
                            'Sohail (07 Feb 2019) -- End
                        blnResult = .Insert(strDatabaseName, objDataOperation, dtCurrentDateAndTime)
                    End If
                    End If

                End With
                objEmpCC = Nothing
            End If
            'Sohail (26 Apr 2018) -- End

            'Sohail (15 Dec 2018) -- Start
            'NMB Enhancement - On salary change screen, create a coonection between new scale text box and change amount text box. i.e if a user types in the new scale, the change amount calculates automatically in 75.1.
            'If blnGlobal = False Then
            '    'Sohail (14 Jun 2011) -- Start
            '    'Issue : on error in insertaudittrail record is inserted in main table while not inserted in audit table.
            '    'objDataOperation.ReleaseTransaction(True)
            '    If blnResult = True Then
            '        objDataOperation.ReleaseTransaction(True)
            '    Else
            '        objDataOperation.ReleaseTransaction(False)
            '        Return False
            '    End If
            '    'Sohail (14 Jun 2011) -- End
            'End If
            If blnResult = False Then
                    objDataOperation.ReleaseTransaction(False)
                    Return False
                End If
            'Sohail (15 Dec 2018) -- End
            'Sohail (11 Nov 2010) -- End

            'Sohail (15 Dec 2018) -- Start
            'NMB Enhancement - On salary change screen, create a coonection between new scale text box and change amount text box. i.e if a user types in the new scale, the change amount calculates automatically in 75.1.
            If xDataOp Is Nothing Then objDataOperation.ReleaseTransaction(True)
            'Sohail (15 Dec 2018) -- End

            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False) 'Sohail (11 Nov 2010)
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            'Sohail (15 Dec 2018) -- Start
            'NMB Enhancement - On salary change screen, create a coonection between new scale text box and change amount text box. i.e if a user types in the new scale, the change amount calculates automatically in 75.1.
            'objDataOperation = Nothing
            If xDataOp Is Nothing Then objDataOperation = Nothing
            'Sohail (15 Dec 2018) -- End
        End Try
    End Function

    'Sohail (21 Jan 2022) -- Start
    'Enhancement :  : AT Log data optimization on earning and deduction.
    Public Function IsTableDataUpdate(ByVal intUnkid As Integer, Optional ByVal objDooperation As clsDataOperation = Nothing) As Boolean
        Dim strQ As String = ""
        Dim strFields As String = ""
        Dim dsList As New DataSet
        Dim exForce As Exception
        Try
            If objDooperation Is Nothing Then
                objDataOperation = New clsDataOperation
            Else
                objDataOperation = objDooperation
            End If

            strFields = " employeeunkid,tranheadunkid,batchtransactionunkid,amount,isdeduct,trnheadtype_id,typeof_id,calctype_id,computeon_id,formula,formulaid,currencyunkid,vendorunkid,membership_categoryunkid,isapproved,periodunkid,medicalrefno,disciplinefileunkid,membershiptranunkid,stop_date,cumulative_startdate,empbatchpostingunkid,edstart_date "

            strQ = "WITH CTE AS ( " & _
                            "SELECT TOP 1 " & strFields & " " & _
                            "FROM atprearningdeduction_master " & _
                            "WHERE atprearningdeduction_master.edunkid = @edunkid " & _
                            "AND atprearningdeduction_master.audittype <> 3 " & _
                            "ORDER BY atprearningdeduction_master.atedtranunkid DESC " & _
                        ") " & _
                    " " & _
                    "SELECT " & strFields & " " & _
                    "FROM prearningdeduction_master " & _
                    "WHERE prearningdeduction_master.edunkid = @edunkid " & _
                    "EXCEPT " & _
                    "SELECT * FROM cte "

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@edunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables(0).Rows.Count > 0

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: IsTableDataUpdate; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If objDooperation Is Nothing Then objDataOperation = Nothing
        End Try
    End Function
    'Sohail (21 Jan 2022) -- End

    ''' <summary>
    ''' Modify By: Suhail
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Void Database Table (prearningdeduction_master) </purpose>
    Public Function Void(ByVal intUnkid As Integer, ByVal intVoidUserID As Integer, ByVal dtVoidDatetime As DateTime, ByVal strVoidReason As String, ByVal strDatabaseName As String) As Boolean
        'Sohail (21 Aug 2015) - [strDatabaseName]
        'If isUsed(intUnkid) Then
        '    mstrMessage = Language.getMessage(mstrModuleName, 2, "This Transaction Head is already in used.")
        '    Return False
        'End If

        Dim objDataOperation As New clsDataOperation 'Sohail (11 Nov 2010)

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        'S.SANDEEP [12-JUL-2018] -- START
        'ISSUE/ENHANCEMENT : {Ref#223|ARUTI-239}
        Dim objAppraisal As New clsAppraisal_Final_Emp
        'S.SANDEEP [12-JUL-2018] -- END

        objDataOperation = New clsDataOperation
        objDataOperation.BindTransaction() 'Sohail (11 Nov 2010)

        Try
            'strQ = "DELETE FROM prearningdeduction_master " & _
            '"WHERE edunkid = @edunkid "
            strQ = "UPDATE prearningdeduction_master SET " & _
             " isvoid = 1" & _
             ", voiduserunkid = @voiduserunkid" & _
             ", voiddatetime = @voiddatetime " & _
             ", voidreason = @voidreason " & _
           "WHERE edunkid = @edunkid "

            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intVoidUserID.ToString)
            If dtVoidDatetime = Nothing Then
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            Else
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, dtVoidDatetime)
            End If
            objDataOperation.AddParameter("@edunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strVoidReason.ToString)

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If


            'Pinkal (18-Jun-2013) -- Start
            'Enhancement : TRA Changes
            Me._DataOperation = objDataOperation
            'Pinkal (18-Jun-2013) -- End


            'Sohail (11 Nov 2010) -- Start
            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'Me._Edunkid = intUnkid
            Me._Edunkid(objDataOperation, strDatabaseName) = intUnkid
            'Sohail (21 Aug 2015) -- End
            'Sohail (14 Jun 2011) -- Start

            'Issue : on error in insertaudittrail record is inserted in main table while not inserted in audit table.
            'Call InsertAuditTrailForEarningDeduction(objDataOperation, 3)
            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'If InsertAuditTrailForEarningDeduction(objDataOperation, 3) = False Then
            If InsertAuditTrailForEarningDeduction(objDataOperation, 3, dtVoidDatetime) = False Then
                'Sohail (21 Aug 2015) -- End
                objDataOperation.ReleaseTransaction(False)
                Return False
            End If
            'Sohail (14 Jun 2011) -- End

            'S.SANDEEP [12-JUL-2018] -- START
            'ISSUE/ENHANCEMENT : {Ref#223|ARUTI-239}
            If objAppraisal.UpdateEarningDeductionId(-1, -1, intUnkid, -1, True, intVoidUserID, objDataOperation) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If
            'S.SANDEEP [12-JUL-2018] -- END

            objDataOperation.ReleaseTransaction(True)
            'Sohail (11 Nov 2010) -- End

            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Void; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
            'S.SANDEEP [12-JUL-2018] -- START
            'ISSUE/ENHANCEMENT : {Ref#223|ARUTI-239}
            objAppraisal = Nothing
            'S.SANDEEP [12-JUL-2018] -- END
        End Try
    End Function
    'Sohail (23 Apr 2012) -- Start
    'TRA - ENHANCEMENT - Overload funcion created as  Issue of bind transaction from employee master from aruti self service using new web commonlib.
    Public Function Void(ByVal xDataOp As clsDataOperation, ByVal intUnkid As Integer, ByVal intVoidUserID As Integer, ByVal dtVoidDatetime As DateTime, ByVal strVoidReason As String, ByVal strDatabaseName As String) As Boolean
        'Sohail (01 Mar 2019) - [objDataOperation] = [xDataOp]
        'Sohail (21 Aug 2015) - [strDatabaseName]

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        'Sohail (01 Mar 2019) -- Start
        'KBC Support Issue 0003536 - 74.1 - The database is in single user mode on close period.
        If xDataOp IsNot Nothing Then
            objDataOperation = xDataOp
        Else
            objDataOperation = New clsDataOperation
        End If
        'Sohail (01 Mar 2019) -- End

        Try
            strQ = "UPDATE prearningdeduction_master SET " & _
             " isvoid = 1" & _
             ", voiduserunkid = @voiduserunkid" & _
             ", voiddatetime = @voiddatetime " & _
             ", voidreason = @voidreason " & _
           "WHERE edunkid = @edunkid "

            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intVoidUserID.ToString)
            If dtVoidDatetime = Nothing Then
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            Else
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, dtVoidDatetime)
            End If
            objDataOperation.AddParameter("@edunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strVoidReason.ToString)

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Me._DataOperation = objDataOperation
            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'Me._Edunkid = intUnkid
            Me._Edunkid(objDataOperation, strDatabaseName) = intUnkid
            'Sohail (21 Aug 2015) -- End
            If InsertAuditTrailForEarningDeduction(objDataOperation, 3, dtVoidDatetime) = False Then
                objDataOperation.ReleaseTransaction(False)
                Return False
            End If

            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Void; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            'Sohail (01 Mar 2019) -- Start
            'KBC Support Issue 0003536 - 74.1 - The database is in single user mode on close period.
            'objDataOperation = Nothing
            If xDataOp Is Nothing Then objDataOperation = Nothing
            'Sohail (01 Mar 2019) -- End
        End Try
    End Function
    'Sohail (23 Apr 2012) -- End

    'Sohail (24 Feb 2016) -- Start
    'Enhancement - Batch Posting changes as per online doc in 58.1 SP. (#15 (KBC & Kenya Project Comments List.xls))
    Public Function VoidEDbyBatchPostingUnkID(ByVal strDatabaseName As String _
                                              , ByVal intEmpBatchPostingUnkid As Integer _
                                              , ByVal intPeriodID As Integer _
                                              , ByVal intVoidUserID As Integer _
                                              , ByVal dtVoidDatetime As DateTime _
                                              , ByVal strVoidReason As String _
                                              , ByVal strIP As String _
                                              , ByVal strWebhostName As String _
                                              , ByVal intLogEmployeeUnkid As Integer _
                                              , ByVal strWebFormName As String _
                                              , ByVal sModuleName1 As String _
                                              , ByVal sModuleName2 As String _
                                              , ByVal sModuleName3 As String _
                                              , ByVal sModuleName4 As String _
                                              , ByVal sModuleName5 As String _
                                              ) As Boolean

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim intRowsAffected As Integer = 0

        Dim objDataOperation As New clsDataOperation
        objDataOperation.BindTransaction()

        Try

            strQ = "INSERT INTO " & strDatabaseName & "..atprearningdeduction_master ( " & _
                        "  edunkid " & _
                        ", employeeunkid " & _
                        ", tranheadunkid " & _
                        ", batchtransactionunkid " & _
                        ", amount " & _
                        ", isdeduct " & _
                        ", trnheadtype_id " & _
                        ", typeof_id " & _
                        ", calctype_id " & _
                        ", computeon_id " & _
                        ", formula " & _
                        ", formulaid " & _
                        ", currencyunkid " & _
                        ", vendorunkid " & _
                        ", membership_categoryunkid " & _
                        ", isapproved " & _
                        ", approveruserunkid " & _
                        ", periodunkid " & _
                        ", medicalrefno " & _
                        ", disciplinefileunkid " & _
                        ", membershiptranunkid " & _
                        ", cumulative_startdate " & _
                        ", stop_date " & _
                        ", empbatchpostingunkid " & _
                        ", edstart_date " & _
                        ", audittype " & _
                        ", audituserunkid " & _
                        ", auditdatetime " & _
                        ", ip " & _
                        ", machine_name" & _
                        ", form_name " & _
                        " " & _
                        " " & _
                        " " & _
                        " " & _
                        " " & _
                        ", isweb " & _
                        ", loginemployeeunkid " & _
                        " ) " & _
                   " SELECT " & _
                        "  prearningdeduction_master.edunkid " & _
                        ", prearningdeduction_master.employeeunkid " & _
                        ", prearningdeduction_master.tranheadunkid " & _
                        ", prearningdeduction_master.batchtransactionunkid " & _
                        ", prearningdeduction_master.amount " & _
                        ", prearningdeduction_master.isdeduct " & _
                        ", prearningdeduction_master.trnheadtype_id " & _
                        ", prearningdeduction_master.typeof_id " & _
                        ", prearningdeduction_master.calctype_id " & _
                        ", prearningdeduction_master.computeon_id " & _
                        ", prearningdeduction_master.formula " & _
                        ", prearningdeduction_master.formulaid " & _
                        ", prearningdeduction_master.currencyunkid " & _
                        ", prearningdeduction_master.vendorunkid " & _
                        ", prearningdeduction_master.membership_categoryunkid " & _
                        ", prearningdeduction_master.isapproved " & _
                        ", prearningdeduction_master.approveruserunkid " & _
                        ", prearningdeduction_master.periodunkid " & _
                        ", prearningdeduction_master.medicalrefno " & _
                        ", prearningdeduction_master.disciplinefileunkid " & _
                        ", prearningdeduction_master.membershiptranunkid " & _
                        ", prearningdeduction_master.cumulative_startdate " & _
                        ", prearningdeduction_master.stop_date " & _
                        ", prearningdeduction_master.empbatchpostingunkid " & _
                        ", prearningdeduction_master.edstart_date " & _
                        ", @audittype " & _
                        ", @audituserunkid " & _
                        ", @auditdatetime " & _
                        ", @ip " & _
                        ", @machine_name" & _
                        ", @form_name " & _
                        " " & _
                        " " & _
                        " " & _
                        " " & _
                        " " & _
                        ", @isweb " & _
                        ", @loginemployeeunkid " & _
                   "FROM     " & strDatabaseName & "..prearningdeduction_master " & _
                   "WHERE    prearningdeduction_master.isvoid = 0 " & _
                            "AND prearningdeduction_master.empbatchpostingunkid = @empbatchpostingunkid " & _
                            "AND prearningdeduction_master.periodunkid = @periodunkid "

            objDataOperation.AddParameter("@empbatchpostingunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intEmpBatchPostingUnkid)
            objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intPeriodID)
            objDataOperation.AddParameter("@audittype", SqlDbType.Int, eZeeDataType.INT_SIZE, 3)
            objDataOperation.AddParameter("@audituserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intVoidUserID)
            objDataOperation.AddParameter("@auditdatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, dtVoidDatetime)
            objDataOperation.AddParameter("@ip", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrClientIP)
            objDataOperation.AddParameter("@machine_name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrHostName)
            objDataOperation.AddParameter("@loginemployeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLogEmployeeUnkid)
            objDataOperation.AddParameter("@form_name", SqlDbType.NVarChar, 500, mstrFormName)
            objDataOperation.AddParameter("@isweb", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsWeb)

            intRowsAffected = objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            strQ = "UPDATE prearningdeduction_master SET " & _
                     " isvoid = 1 " & _
                     ", voiduserunkid = @voiduserunkid" & _
                     ", voiddatetime = @voiddatetime " & _
                     ", voidreason = @voidreason " & _
           "WHERE empbatchpostingunkid = @empbatchpostingunkid " & _
                     "AND isvoid = 0 " & _
                     "AND periodunkid = @periodunkid "

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intVoidUserID.ToString)
            If dtVoidDatetime = Nothing Then
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            Else
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, dtVoidDatetime)
            End If
            objDataOperation.AddParameter("@empbatchpostingunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intEmpBatchPostingUnkid)
            objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intPeriodID)
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strVoidReason.ToString)

            intRowsAffected = objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            objDataOperation.ReleaseTransaction(True)
            Return True

        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: VoidEDbyBatchPostingUnkID; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function
    'Sohail (24 Feb 2016) -- End


    ''' <summary>
    ''' Modify By: Suhail
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isExist(ByVal strDatabaseName As String _
                            , ByVal intEmployeeUnkID As Integer _
                            , ByVal intTranHeadUnkID As Integer _
                            , Optional ByVal intEDUnkid As Integer = -1 _
                            , Optional ByVal intPeriodUnkId As Integer = 0 _
                            , Optional ByVal xDataOp As clsDataOperation = Nothing _
                            ) As Boolean 'Sohail (13 Jan 2012), 'Sohail (23 Apr 2012) - [objDataOperation]
        'Sohail (01 Mar 2019) - [objDataOperation] = [xDataOp]
        'Sohail (21 Aug 2015) - [strDatabaseName]

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        'Sohail (23 Apr 2012) -- Start
        'TRA - ENHANCEMENT
        'Dim objDataOperation As New clsDataOperation
        'Sohail (01 Mar 2019) -- Start
        'KBC Support Issue 0003536 - 74.1 - The database is in single user mode on close period.
        'If objDataOperation Is Nothing Then
        '    objDataOperation = New clsDataOperation
        'End If
        If xDataOp IsNot Nothing Then
            objDataOperation = xDataOp
        Else
            objDataOperation = New clsDataOperation
        End If
        'Sohail (01 Mar 2019) -- End
        objDataOperation.ClearParameters()
        'Sohail (23 Apr 2012) -- End

        'Sohail (21 Jul 2012) -- Start
        'TRA - ENHANCEMENT
        'Sohail (21 Aug 2015) -- Start
        'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
        'If mstrDatabaseName.Trim = "" Then mstrDatabaseName = FinancialYear._Object._DatabaseName
        mstrDatabaseName = strDatabaseName
        'Sohail (21 Aug 2015) -- End
        'Sohail (21 Jul 2012) -- End

        Try
            strQ = "SELECT " & _
              "  edunkid " & _
              ", employeeunkid " & _
              ", tranheadunkid " & _
              ", batchtransactionunkid " & _
              ", amount " & _
              ", isdeduct " & _
              ", trnheadtype_id " & _
              ", typeof_id " & _
              ", calctype_id " & _
              ", computeon_id " & _
              ", formula " & _
              ", formulaid " & _
              ", currencyunkid " & _
              ", vendorunkid " & _
              ", userunkid " & _
              ", isvoid " & _
              ", voiduserunkid " & _
              ", voiddatetime " & _
              ", voidreason " & _
              ", membership_categoryunkid " & _
              ", isapproved " & _
              ", approveruserunkid " & _
              ", ISNULL(periodunkid, 0) AS periodunkid " & _
              ", ISNULL(medicalrefno, '') AS medicalrefno " & _
             "FROM  " & mstrDatabaseName & "..prearningdeduction_master " & _
             "WHERE employeeunkid = @empId " & _
             "AND tranheadunkid = @tranId " & _
             "AND ISNULL(isvoid,0) = 0 " 'Sohail (16 Oct 2010), [isapproved, approveruserunkid - Sohail (14 Jun 2011)]

            If intEDUnkid > 0 Then
                strQ &= " AND edunkid <> @edunkid"
                objDataOperation.AddParameter("@edunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intEDUnkid)
            End If

            'Sohail (13 Jan 2012) -- Start
            'TRA - ENHANCEMENT
            If intPeriodUnkId > 0 Then
                strQ &= " AND periodunkid = @periodunkid "
                objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intPeriodUnkId)
            End If
            'Sohail (13 Jan 2012) -- End

            objDataOperation.AddParameter("@empId", SqlDbType.Int, eZeeDataType.INT_SIZE, intEmployeeUnkID)
            objDataOperation.AddParameter("@tranId", SqlDbType.Int, eZeeDataType.INT_SIZE, intTranHeadUnkID)


            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isExist; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            'Sohail (01 Mar 2019) -- Start
            'KBC Support Issue 0003536 - 74.1 - The database is in single user mode on close period.
            If xDataOp Is Nothing Then objDataOperation = Nothing
            'Sohail (01 Mar 2019) -- End
        End Try
    End Function

    Public Function GetEDUnkID(ByVal strDatabaseName As String _
                               , ByVal intEmployeeUnkID As Integer _
                               , ByVal intTranHeadUnkID As Integer _
                               , Optional ByVal intPeriodUnkID As Integer = 0 _
                               , Optional ByVal xDataOp As clsDataOperation = Nothing _
                               ) As Integer 'Sohail (17 Jan 2012), 'Sohail (23 Apr 2012) - [objDataOperation]
        'Sohail (15 Dec 2018) - [objDataOperation] = [xDataOp]
        'Sohail (21 Aug 2015) - [strDatabaseName]

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        'Sohail (23 Apr 2012) -- Start
        'TRA - ENHANCEMENT
        'objDataOperation = New clsDataOperation
        'Sohail (15 Dec 2018) -- Start
        'HJFMRI Enhancement - Copy Selected Period ED Slab option on Global Assign ED screen in 75.1.
        'If objDataOperation Is Nothing Then
        '    objDataOperation = New clsDataOperation
        'End If
        If xDataOp Is Nothing Then
            objDataOperation = New clsDataOperation
        Else
            objDataOperation = xDataOp
        End If
        'Sohail (15 Dec 2018) -- End
        objDataOperation.ClearParameters()
        'Sohail (23 Apr 2012) -- End

        'Sohail (21 Jul 2012) -- Start
        'TRA - ENHANCEMENT
        'Sohail (21 Aug 2015) -- Start
        'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
        'If mstrDatabaseName.Trim = "" Then mstrDatabaseName = FinancialYear._Object._DatabaseName
        mstrDatabaseName = strDatabaseName
        'Sohail (21 Aug 2015) -- End
        'Sohail (21 Jul 2012) -- End

        Try
            'Sohail (21 Jul 2012) -- Start
            'TRA - ENHANCEMENT
            'strQ = "SELECT " & _
            '  "  edunkid " & _
            '  ", employeeunkid " & _
            '  ", tranheadunkid " & _
            '  ", batchtransactionunkid " & _
            '  ", amount " & _
            '  ", isdeduct " & _
            '  ", trnheadtype_id " & _
            '  ", typeof_id " & _
            '  ", calctype_id " & _
            '  ", computeon_id " & _
            '  ", formula " & _
            '  ", formulaid " & _
            '  ", currencyunkid " & _
            '  ", vendorunkid " & _
            '  ", userunkid " & _
            '  ", isvoid " & _
            '  ", voiduserunkid " & _
            '  ", voiddatetime " & _
            '  ", voidreason " & _
            '  ", membership_categoryunkid " & _
            '  ", isapproved " & _
            '  ", approveruserunkid " & _
            '  ", ISNULL(periodunkid, 0) AS periodunkid " & _
            '  ", ISNULL(medicalrefno, '') AS medicalrefno " & _
            ' "FROM prearningdeduction_master " & _
            ' "WHERE employeeunkid = @empId " & _
            ' "AND tranheadunkid = @tranId " & _
            ' "AND ISNULL(isvoid,0) = 0 " 'Sohail (16 Oct 2010), [isapproved, approveruserunkid - Sohail (14 Jun 2011)]
            strQ = "SELECT " & _
              "  edunkid " & _
           "FROM " & mstrDatabaseName & "..prearningdeduction_master " & _
             "WHERE employeeunkid = @empId " & _
             "AND tranheadunkid = @tranId " & _
             "AND ISNULL(isvoid,0) = 0 " 'Sohail (16 Oct 2010), [isapproved, approveruserunkid - Sohail (14 Jun 2011)]
            'Sohail (21 Jul 2012) -- End

            'Sohail (13 Jan 2012) -- Start
            'TRA - ENHANCEMENT
            If intPeriodUnkID > 0 Then
                strQ &= " AND periodunkid = @periodunkid "
                objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intPeriodUnkID)
            End If
            'Sohail (13 Jan 2012) -- End

            objDataOperation.AddParameter("@empId", SqlDbType.Int, eZeeDataType.INT_SIZE, intEmployeeUnkID)
            objDataOperation.AddParameter("@tranId", SqlDbType.Int, eZeeDataType.INT_SIZE, intTranHeadUnkID)


            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If dsList.Tables(0).Rows.Count > 0 Then
                Return CInt(dsList.Tables(0).Rows(0).Item("edunkid").ToString)
            Else
                Return -1
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetEDUnkID; Module Name: " & mstrModuleName)
            Return -1
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            'Sohail (15 Dec 2018) -- Start
            'HJFMRI Enhancement - Copy Selected Period ED Slab option on Global Assign ED screen in 75.1.
            'objDataOperation = Nothing
            If xDataOp Is Nothing Then objDataOperation = Nothing
            'Sohail (15 Dec 2018) -- End
        End Try
    End Function


    'Sohail (15 Dec 2018) -- Start
    'HJFMRI Enhancement - Copy Selected Period ED Slab option on Global Assign ED screen in 75.1.
    '    Public Function InsertAllByEmployeeList(ByVal strDatabaseName As String _
    '                                            , ByVal xUserUnkid As Integer _
    '                                            , ByVal xYearUnkid As Integer _
    '                                            , ByVal xCompanyUnkid As Integer _
    '                                            , ByVal xPeriodStart As DateTime _
    '                                            , ByVal xPeriodEnd As DateTime _
    '                                            , ByVal xUserModeSetting As String _
    '                                            , ByVal xOnlyApproved As Boolean _
    '                                            , ByVal xIncludeIn_ActiveEmployee As Boolean _
    '                                            , ByVal strEmployeeList As String _
    '                                            , ByVal intTranHeadID As Integer _
    '                                            , ByVal intCurrencyID As Integer _
    '                                            , ByVal intVendorID As Integer _
    '                                            , ByVal intBatchID As Double _
    '                                            , ByVal decAmount As Decimal _
    '                                            , ByVal blnOverWrite As Boolean _
    '                                            , ByVal blnAddToExist As Boolean _
    '                                            , ByVal intPeriodUnkID As Integer _
    '                                            , ByVal strMedicalRefNo As String _
    '                                            , ByVal dtCumulative_Startdate As Date _
    '                                            , ByVal dtStop_date As Date _
    '                                            , ByVal blnAllowToApproveEarningDeduction As Boolean _
    '                                            , ByVal dtCurrentDateAndTime As Date _
    '                                            , Optional ByVal blnInsertSalaryHead As Boolean = False _
    '                                            , Optional ByVal blnCopyPreviousEDSlab As Boolean = False _
    '                                            , Optional ByVal blnOverwritePreviousEDSlabHeads As Boolean = False _
    '                                            , Optional ByVal blnApplyUserAccessFilter As Boolean = True _
    '                                            , Optional ByVal strFilerString As String = "" _
    '                                            , Optional ByVal bw As BackgroundWorker = Nothing _
    '                              ) As Boolean
    '        ''Hemant (20 July 2018) - [bw]
    '        'Sohail (21 Aug 2015) - [strDatabaseName, xUserUnkid, xYearUnkid, xCompanyUnkid, xPeriodStart, xPeriodEnd, xUserModeSetting, xOnlyApproved, xIncludeIn_ActiveEmployee, dtCurrentDateTime, blnApplyUserAccessFilter, strFilerString]
    '        '                     'Sohail (09 Nov 2013) - [dtCumulative_Startdate, dtStop_date]
    '        '                     'Sohail (11 May 2011), 'Sohail (13 Jan 2012), 'Sohail (07 Mar 2012) - [blnCopyPreviousEDSlab]
    '        '                     'Sohail (26 Jul 2012) - [blnOverwritePreviousEDSlabHeads]
    '        Dim objDataOperation As clsDataOperation

    '        Dim dsList As DataSet = Nothing
    '        Dim strQ As String = ""
    '        Dim arrEmp As String()
    '        Dim exForce As Exception
    '        Dim blnFlag As Boolean
    '        Dim intEDID As Integer
    '        Dim objTranHead As New clsTransactionHead
    '        'Sohail (07 Mar 2012) -- Start
    '        'TRA - ENHANCEMENT
    '        Dim mdtPeriod_startdate As Date
    '        Dim mdtPeriod_enddate As Date
    '        'Sohail (07 Mar 2012) -- End

    '        objDataOperation = New clsDataOperation
    '        objDataOperation.BindTransaction()

    '        Try

    '            arrEmp = strEmployeeList.Split(",")

    '            'Sohail (07 Mar 2012) -- Start
    '            'TRA - ENHANCEMENT
    '            Dim objPeriod As New clscommom_period_Tran
    '            'Sohail (21 Aug 2015) -- Start
    '            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
    '            'objPeriod._DatabaseName = mstrDatabaseName
    '            'objPeriod._Periodunkid = intPeriodUnkID
    '            objPeriod._DatabaseName = strDatabaseName
    '            objPeriod._Periodunkid(strDatabaseName) = intPeriodUnkID
    '            'Sohail (21 Aug 2015) -- End
    '            mdtPeriod_startdate = objPeriod._Start_Date
    '            mdtPeriod_enddate = objPeriod._End_Date
    '            'Sohail (07 Mar 2012) -- End

    '            'Hemant (20 July 2018) -- Start
    '            'Support Issue # : 0002390: FINCA UGANDA :Earning and deduction not Comming on July period due to july was wrongly configured during closure of june period
    '            clsClosePeriod._ProgressTotalCount = arrEmp.Length
    '            'Hemant (20 July 2018) -- End

    '            For i As Integer = 0 To arrEmp.Length - 1
    '                mintEmployeeunkid = CInt(arrEmp(i))

    '                'Sohail (13 Jan 2012) -- Start
    '                'TRA - ENHANCEMENT
    '                'intEDID = GetEDUnkID(mintEmployeeunkid, intTranHeadID)
    '                If blnInsertSalaryHead = True Then
    '                    'Sohail (21 Aug 2015) -- Start
    '                    'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
    '                    'blnFlag = Insert_SalaryHead(mintEmployeeunkid, intPeriodUnkID)
    '                    blnFlag = Insert_SalaryHead(strDatabaseName, xUserUnkid, xYearUnkid, xCompanyUnkid, xPeriodStart, xPeriodEnd, xUserModeSetting, xOnlyApproved, xIncludeIn_ActiveEmployee, mintEmployeeunkid, intPeriodUnkID, dtCurrentDateAndTime, objDataOperation, blnApplyUserAccessFilter, strFilerString)
    '                    'Sohail (21 Aug 2015) -- End

    '                    If blnFlag = False Then
    '                        objDataOperation.ReleaseTransaction(False)
    '                        Return False
    '                    End If
    '                End If

    '                If intTranHeadID <= 0 Then GoTo COPY_PREVIOUS_ED_SLAB 'Sohail (21 Jul 2012) - [To copy previous ed slab when closing period]

    '                'Sohail (21 Aug 2015) -- Start
    '                'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
    '                'intEDID = GetEDUnkID(mintEmployeeunkid, intTranHeadID, intPeriodUnkID)
    '                intEDID = GetEDUnkID(strDatabaseName, mintEmployeeunkid, intTranHeadID, intPeriodUnkID, objDataOperation)
    '                'Sohail (21 Aug 2015) -- End
    '                'Sohail (13 Jan 2012) -- End

    '                objDataOperation.ClearParameters()

    '                If intEDID > 0 Then
    '                    'Sohail (21 Aug 2015) -- Start
    '                    'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
    '                    'Me._Edunkid = intEDID
    '                    Me._Edunkid(objDataOperation, strDatabaseName) = intEDID
    '                    'Sohail (21 Aug 2015) -- End

    '                    If blnOverWrite = True Then
    '                        mdecAmount = decAmount
    '                        mintCurrencyunkid = intCurrencyID
    '                        mintVendorunkid = intVendorID
    '                        mintMembership_Categoryunkid = 0
    '                        mintPeriodunkid = intPeriodUnkID 'Sohail (13 Jan 2012)
    '                        mstrMedicalrefno = strMedicalRefNo 'Sohail (18 Jan 2012)
    '                        'Sohail (09 Nov 2013) -- Start
    '                        'TRA - ENHANCEMENT
    '                        mdtCumulative_Startdate = dtCumulative_Startdate
    '                        mdtStop_Date = dtStop_date
    '                        'Sohail (09 Nov 2013) -- End
    '                    ElseIf blnAddToExist = True Then
    '                        mdecAmount += decAmount
    '                    End If

    '                    If blnOverWrite = True OrElse blnAddToExist = True Then
    '                        'Sohail (21 Aug 2015) -- Start
    '                        'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
    '                        'mintUserunkid = User._Object._Userunkid
    '                        mintUserunkid = xUserUnkid
    '                        'Sohail (21 Aug 2015) -- End
    '                        'Sohail (11 Nov 2010) -- Start
    '                        'blnFlag = Update()
    '                        'Sohail (21 Aug 2015) -- Start
    '                        'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
    '                        'blnFlag = Update(True)
    '                        blnFlag = Update(strDatabaseName, True, objDataOperation, dtCurrentDateAndTime)
    '                        'Sohail (21 Aug 2015) -- End
    '                        'Sohail (11 Nov 2010) -- End
    '                    Else
    '                        blnFlag = True
    '                    End If
    '                Else
    '                    mintTranheadunkid = intTranHeadID
    '                    'Sohail (21 Aug 2015) -- Start
    '                    'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
    '                    'objTranHead._Tranheadunkid = intTranHeadID
    '                    objTranHead._Tranheadunkid(strDatabaseName) = intTranHeadID
    '                    'Sohail (21 Aug 2015) -- End
    '                    mintBatchtransactionunkid = intBatchID

    '                    mintTrnheadtype_Id = objTranHead._Trnheadtype_Id
    '                    mintTypeof_Id = objTranHead._Typeof_id
    '                    mintCalctype_Id = objTranHead._Calctype_Id
    '                    mintComputeon_Id = objTranHead._Computeon_Id
    '                    mstrFormula = objTranHead._Formula
    '                    mstrFormulaid = objTranHead._Formulaid
    '                    mdecAmount = decAmount

    '                    If objTranHead._Trnheadtype_Id = 1 Then 'enTranHeadType.EarningForEmployees
    '                        mblnIsdeduct = False
    '                    ElseIf objTranHead._Trnheadtype_Id = 4 Or objTranHead._Trnheadtype_Id = 5 Then 'enTranHeadType.EmployersStatutoryContributions , Informational
    '                        mblnIsdeduct = Nothing
    '                    Else
    '                        mblnIsdeduct = True
    '                    End If

    '                    mintCurrencyunkid = intCurrencyID
    '                    mintVendorunkid = intVendorID
    '                    'Sohail (21 Aug 2015) -- Start
    '                    'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
    '                    'mintUserunkid = User._Object._Userunkid 'Sohail (14 Oct 2010)
    '                    mintUserunkid = xUserUnkid
    '                    'Sohail (21 Aug 2015) -- End
    '                    mblnIsvoid = False
    '                    mintVoiduserunkid = -1
    '                    mdtVoiddatetime = Nothing
    '                    mstrVoidreason = ""
    '                    mintMembership_Categoryunkid = 0
    '                    'Sohail (14 Jun 2011) -- Start
    '                    'Sohail (05 Jul 2011) -- Start
    '                    'Sohail (21 Aug 2015) -- Start
    '                    'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
    '                    'If User._Object.Privilege._AllowToApproveEarningDeduction = True Then
    '                    If blnAllowToApproveEarningDeduction = True Then
    '                        'Sohail (21 Aug 2015) -- End
    '                        mblnIsapproved = True
    '                        'Sohail (21 Aug 2015) -- Start
    '                        'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
    '                        'mintApproveruserunkid = User._Object._Userunkid
    '                        mintApproveruserunkid = xUserUnkid
    '                        'Sohail (21 Aug 2015) -- End
    '                    Else
    '                        mblnIsapproved = False
    '                        mintApproveruserunkid = -1
    '                    End If
    '                    'Sohail (14 Jun 2011) -- End
    '                    mintPeriodunkid = intPeriodUnkID 'Sohail (13 Jan 2012)
    '                    mstrMedicalrefno = strMedicalRefNo 'Sohail (18 Jan 2012)
    '                    'Sohail (30 Mar 2013) -- Start
    '                    'TRA - ENHANCEMENT
    '                    mintMembershipTranUnkid = 0
    '                    mintDisciplinefileunkid = 0
    '                    'Sohail (30 Mar 2013) -- End
    '                    'Sohail (09 Nov 2013) -- Start
    '                    'TRA - ENHANCEMENT
    '                    mdtCumulative_Startdate = dtCumulative_Startdate
    '                    mdtStop_Date = dtStop_date
    '                    'Sohail (09 Nov 2013) -- End
    '                    mintEmpbatchpostingunkid = 0 'Sohail (24 Feb 2016)

    '                    'Sohail (11 Nov 2010) -- Start
    '                    'blnFlag = Insert()
    '                    'Sohail (21 Aug 2015) -- Start
    '                    'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
    '                    'blnFlag = Insert(True)
    '                    'Sohail (20 Jul 2018) -- Start
    '                    'Support Issue # 0002383 : VOLTAMP: Unable to assign Transaction heads using batch entry in Payroll transactions (Warning message of Membership heads not assigned)in 73.1.
    '                    'blnFlag = Insert(strDatabaseName, xUserUnkid, xYearUnkid, xCompanyUnkid, xPeriodStart, xPeriodEnd, xUserModeSetting, xOnlyApproved, xIncludeIn_ActiveEmployee, True, dtCurrentDateAndTime, , objDataOperation, blnApplyUserAccessFilter, strFilerString)
    '                    blnFlag = Insert(strDatabaseName, xUserUnkid, xYearUnkid, xCompanyUnkid, xPeriodStart, xPeriodEnd, xUserModeSetting, xOnlyApproved, xIncludeIn_ActiveEmployee, True, dtCurrentDateAndTime, , objDataOperation, blnApplyUserAccessFilter, strFilerString, True, blnAllowToApproveEarningDeduction)
    '                    'Sohail (20 Jul 2018) -- End
    '                    'Sohail (21 Aug 2015) -- End
    '                    'Sohail (11 Nov 2010) -- End
    '                End If

    '                If blnFlag = False Then
    '                    objDataOperation.ReleaseTransaction(False)
    '                    Return False
    '                End If


    'COPY_PREVIOUS_ED_SLAB:  'Sohail (21 Jul 2012)

    '                'Sohail (07 Mar 2012) -- Start
    '                'TRA - ENHANCEMENT
    '                '**** Copy Previous ED Slab
    '                If blnCopyPreviousEDSlab = True Then
    '                    'Sohail (21 Aug 2015) -- Start
    '                    'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
    '                    'Call GetEmployeeED()
    '                    'Sohail (01 Mar 2018) -- Start
    '                    'Internal Issue : ExecureReader requires the command to have a transaction... in 70.1.
    '                    'Call GetEmployeeED(strDatabaseName)
    '                    Call GetEmployeeED(objDataOperation, strDatabaseName)
    '                    'Sohail (01 Mar 2018) -- End
    '                    'Sohail (21 Aug 2015) -- End

    '                    Dim strPrevEndDate As String = ""
    '                    Dim drRow As DataRow()

    '                    'Sohail (09 Nov 2013) -- Start
    '                    'TRA - ENHANCEMENT
    '                    'drRow = mdtED.Select("AUD = '' AND end_date <= '" & eZeeDate.convertDate(mdtPeriod_startdate) & "' AND isrecurrent = 1 ", "end_date DESC")
    '                    drRow = mdtED.Select("AUD = '' AND end_date <= '" & eZeeDate.convertDate(mdtPeriod_startdate) & "' AND isrecurrent = 1 AND (stop_date IS NULL OR stop_date >= '" & mdtPeriod_startdate & "') ", "end_date DESC")
    '                    'Sohail (09 Nov 2013) -- End

    '                    If drRow.Length > 0 Then
    '                        For Each dRow As DataRow In drRow
    '                            If strPrevEndDate <> "" AndAlso strPrevEndDate <> dRow.Item("end_date").ToString Then
    '                                Exit For
    '                            End If

    '                            objTranHead._DatabaseName = mstrDatabaseName 'Sohail (21 Jul 2012)
    '                            'Sohail (21 Aug 2015) -- Start
    '                            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
    '                            'objTranHead._Tranheadunkid = CInt(dRow.Item("tranheadunkid"))
    '                            objTranHead._Tranheadunkid(strDatabaseName) = CInt(dRow.Item("tranheadunkid"))
    '                            'Sohail (21 Aug 2015) -- End
    '                            'Sohail (25 May 2012) -- Start
    '                            'TRA - ENHANCEMENT
    '                            If objTranHead._Typeof_id = enTypeOf.Salary OrElse CInt(dRow.Item("tranheadunkid")) = intTranHeadID Then
    '                                'If objTranHead._Typeof_id = enTypeOf.Salary Then
    '                                'Sohail (25 May 2012) -- End
    '                                strPrevEndDate = dRow.Item("end_date").ToString
    '                                Continue For
    '                            End If

    '                            'Sohail (21 Aug 2015) -- Start
    '                            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
    '                            'intEDID = GetEDUnkID(mintEmployeeunkid, CInt(dRow.Item("tranheadunkid")), intPeriodUnkID)
    '                            intEDID = GetEDUnkID(strDatabaseName, mintEmployeeunkid, CInt(dRow.Item("tranheadunkid")), intPeriodUnkID, objDataOperation)
    '                            'Sohail (21 Aug 2015) -- End

    '                            If intEDID > 0 Then
    '                                'Sohail (21 Aug 2015) -- Start
    '                                'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
    '                                'Me._Edunkid = intEDID 'Sohail (25 May 2012)
    '                                Me._Edunkid(objDataOperation, strDatabaseName) = intEDID
    '                                'Sohail (21 Aug 2015) -- End
    '                                'Sohail (26 Jul 2012) -- Start
    '                                'TRA - ENHANCEMENT
    '                                'If blnOverWrite = True Then
    '                                If blnOverwritePreviousEDSlabHeads = True Then
    '                                    'Sohail (26 Jul 2012) -- End
    '                                    mdecAmount = CDec(dRow.Item("amount"))
    '                                    mintCurrencyunkid = CInt(dRow.Item("currencyid"))
    '                                    mintVendorunkid = CInt(dRow.Item("vendorid"))
    '                                    If IsDBNull(dRow.Item("membership_categoryunkid")) Then
    '                                        mintMembership_Categoryunkid = 0
    '                                    Else
    '                                        mintMembership_Categoryunkid = CInt(dRow.Item("membership_categoryunkid"))
    '                                    End If
    '                                    mintCostCenterUnkId = CInt(dRow.Item("costcenterunkid"))
    '                                    mstrMedicalrefno = dRow.Item("medicalrefno").ToString
    '                                    'Sohail (21 Aug 2015) -- Start
    '                                    'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
    '                                    'mintUserunkid = User._Object._Userunkid
    '                                    mintUserunkid = xUserUnkid
    '                                    'Sohail (21 Aug 2015) -- End
    '                                    'Sohail (20 Feb 2013) -- Start
    '                                    'TRA - ENHANCEMENT
    '                                    mintMembershipTranUnkid = CInt(dRow.Item("membershiptranunkid"))
    '                                    mintDisciplinefileunkid = CInt(dRow.Item("disciplinefileunkid"))
    '                                    'Sohail (20 Feb 2013) -- End
    '                                    'Sohail (09 Nov 2013) -- Start
    '                                    'TRA - ENHANCEMENT
    '                                    If IsDBNull(dRow.Item("cumulative_startdate")) = True Then
    '                                        mdtCumulative_Startdate = Nothing
    '                                    Else
    '                                        mdtCumulative_Startdate = dRow.Item("cumulative_startdate")
    '                                    End If
    '                                    If IsDBNull(dRow.Item("stop_date")) = True Then
    '                                        mdtStop_Date = Nothing
    '                                    Else
    '                                        mdtStop_Date = dRow.Item("stop_date")
    '                                    End If
    '                                    'Sohail (09 Nov 2013) -- End
    '                                    mintEmpbatchpostingunkid = 0 'Sohail (24 Feb 2016)
    '                                    'Sohail (21 Aug 2015) -- Start
    '                                    'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
    '                                    'blnFlag = Update(True)
    '                                    blnFlag = Update(strDatabaseName, True, objDataOperation, dtCurrentDateAndTime)
    '                                    'Sohail (21 Aug 2015) -- End
    '                                Else
    '                                    blnFlag = True
    '                                End If
    '                            Else
    '                                mintTranheadunkid = CInt(dRow.Item("tranheadunkid"))
    '                                objTranHead._DatabaseName = mstrDatabaseName 'Sohail (21 Jul 2012)
    '                                'Sohail (21 Aug 2015) -- Start
    '                                'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
    '                                'objTranHead._Tranheadunkid = CInt(dRow.Item("tranheadunkid"))
    '                                objTranHead._Tranheadunkid(strDatabaseName) = CInt(dRow.Item("tranheadunkid"))
    '                                'Sohail (21 Aug 2015) -- End
    '                                mintBatchtransactionunkid = CInt(dRow.Item("batchtransactionunkid"))

    '                                mintTrnheadtype_Id = objTranHead._Trnheadtype_Id
    '                                mintTypeof_Id = objTranHead._Typeof_id
    '                                mintCalctype_Id = objTranHead._Calctype_Id
    '                                mintComputeon_Id = objTranHead._Computeon_Id
    '                                mstrFormula = objTranHead._Formula
    '                                mstrFormulaid = objTranHead._Formulaid
    '                                mdecAmount = CDec(dRow.Item("amount"))

    '                                If objTranHead._Trnheadtype_Id = enTranHeadType.EarningForEmployees Then
    '                                    mblnIsdeduct = False
    '                                ElseIf objTranHead._Trnheadtype_Id = enTranHeadType.EmployersStatutoryContributions OrElse objTranHead._Trnheadtype_Id = enTranHeadType.Informational Then
    '                                    mblnIsdeduct = Nothing
    '                                Else
    '                                    mblnIsdeduct = True
    '                                End If

    '                                mintCurrencyunkid = CInt(dRow.Item("currencyid"))
    '                                mintVendorunkid = CInt(dRow.Item("vendorid"))
    '                                'Sohail (21 Aug 2015) -- Start
    '                                'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
    '                                'mintUserunkid = User._Object._Userunkid
    '                                mintUserunkid = xUserUnkid
    '                                'Sohail (21 Aug 2015) -- End
    '                                mblnIsvoid = False
    '                                mintVoiduserunkid = -1
    '                                mdtVoiddatetime = Nothing
    '                                mstrVoidreason = ""
    '                                mintMembership_Categoryunkid = 0
    '                                'Sohail (21 Aug 2015) -- Start
    '                                'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
    '                                'If User._Object.Privilege._AllowToApproveEarningDeduction = True Then
    '                                If blnAllowToApproveEarningDeduction = True Then
    '                                    'Sohail (21 Aug 2015) -- End
    '                                    mblnIsapproved = True
    '                                    'Sohail (21 Aug 2015) -- Start
    '                                    'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
    '                                    'mintApproveruserunkid = User._Object._Userunkid
    '                                    mintApproveruserunkid = xUserUnkid
    '                                    'Sohail (21 Aug 2015) -- End
    '                                Else
    '                                    mblnIsapproved = False
    '                                    mintApproveruserunkid = -1
    '                                End If
    '                                mintPeriodunkid = intPeriodUnkID
    '                                mintCostCenterUnkId = 0
    '                                mstrMedicalrefno = ""
    '                                'Sohail (20 Feb 2013) -- Start
    '                                'TRA - ENHANCEMENT
    '                                mintMembershipTranUnkid = CInt(dRow.Item("membershiptranunkid"))
    '                                mintDisciplinefileunkid = CInt(dRow.Item("disciplinefileunkid"))
    '                                'Sohail (20 Feb 2013) -- End
    '                                'Sohail (09 Nov 2013) -- Start
    '                                'TRA - ENHANCEMENT
    '                                If IsDBNull(dRow.Item("cumulative_startdate")) = True Then
    '                                    mdtCumulative_Startdate = Nothing
    '                                Else
    '                                    mdtCumulative_Startdate = dRow.Item("cumulative_startdate")
    '                                End If
    '                                If IsDBNull(dRow.Item("stop_date")) = True Then
    '                                    mdtStop_Date = Nothing
    '                                Else
    '                                    mdtStop_Date = dRow.Item("stop_date")
    '                                End If
    '                                'Sohail (09 Nov 2013) -- End
    '                                mintEmpbatchpostingunkid = 0 'Sohail (24 Feb 2016)
    '                                'Sohail (21 Aug 2015) -- Start
    '                                'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
    '                                'blnFlag = Insert(True)
    '                                'Sohail (20 Jul 2018) -- Start
    '                                'Support Issue # 0002383 : VOLTAMP: Unable to assign Transaction heads using batch entry in Payroll transactions (Warning message of Membership heads not assigned)in 73.1.
    '                                'blnFlag = Insert(strDatabaseName, xUserUnkid, xYearUnkid, xCompanyUnkid, xPeriodStart, xPeriodEnd, xUserModeSetting, xOnlyApproved, xIncludeIn_ActiveEmployee, True, dtCurrentDateAndTime, , objDataOperation, blnApplyUserAccessFilter, strFilerString)
    '                                blnFlag = Insert(strDatabaseName, xUserUnkid, xYearUnkid, xCompanyUnkid, xPeriodStart, xPeriodEnd, xUserModeSetting, xOnlyApproved, xIncludeIn_ActiveEmployee, True, dtCurrentDateAndTime, , objDataOperation, blnApplyUserAccessFilter, strFilerString, False, blnAllowToApproveEarningDeduction)
    '                                'Sohail (20 Jul 2018) -- End
    '                                'Sohail (21 Aug 2015) -- End
    '                            End If

    '                            If blnFlag = False Then
    '                                objDataOperation.ReleaseTransaction(False)
    '                                Return False
    '                            End If

    '                            strPrevEndDate = dRow.Item("end_date").ToString
    '                        Next
    '                    End If
    '                End If
    '                'Sohail (07 Mar 2012) -- End

    '                'Hemant (20 July 2018) -- Start
    '                'Support Issue # : 0002390: FINCA UGANDA :Earning and deduction not Comming on July period due to july was wrongly configured during closure of june period
    '                clsClosePeriod._ProgressCurrCount = i + 1
    '                If bw IsNot Nothing Then
    '                    bw.ReportProgress(i + 1)
    '                End If
    '                'Hemant (20 July 2018) -- End

    '            Next

    '            objDataOperation.ReleaseTransaction(True)
    '            'Hemant (20 July 2018) -- Start
    '            'Support Issue # : 0002390: FINCA UGANDA :Earning and deduction not Comming on July period due to july was wrongly configured during closure of june period
    '            clsClosePeriod._ProgressTotalCount = 0
    '            'Hemant (20 July 2018) -- End

    '            Return True
    '        Catch ex As Exception
    '            objDataOperation.ReleaseTransaction(False)
    '            Throw New Exception(ex.Message & "; Procedure Name: InsertAllByEmployeeList; Module Name: " & mstrModuleName)
    '            Return False
    '        Finally
    '            exForce = Nothing
    '            If dsList IsNot Nothing Then dsList.Dispose()
    '            objTranHead = Nothing
    '            objDataOperation = Nothing
    '        End Try
    '    End Function

    '    'S.SANDEEP [ 04 SEP 2012 ] -- START
    '    'ENHANCEMENT : TRA CHANGES
    '    'PURPOSE : INCLUSION OF SALARY HEAD IN INFORMATIONAL HEAD AND GIVE SELECTION TO USER
    '    'NOTE : (PLEASE CHANGE IN BOTH METHODS IF ANY CHANGE IN QUERY)
    '    Public Function InsertAllByEmployeeList(ByVal strDatabaseName As String _
    '                                            , ByVal xUserUnkid As Integer _
    '                                            , ByVal xYearUnkid As Integer _
    '                                            , ByVal xCompanyUnkid As Integer _
    '                                            , ByVal xPeriodStart As DateTime _
    '                                            , ByVal xPeriodEnd As DateTime _
    '                                            , ByVal xUserModeSetting As String _
    '                                            , ByVal xOnlyApproved As Boolean _
    '                                            , ByVal xIncludeIn_ActiveEmployee As Boolean _
    '                                            , ByVal strEmployeeList As String _
    '                                            , ByVal intTranHeadID As Integer _
    '                                            , ByVal intCurrencyID As Integer _
    '                                            , ByVal intVendorID As Integer _
    '                                            , ByVal intBatchID As Double _
    '                                            , ByVal decAmount As Decimal _
    '                                            , ByVal blnOverWrite As Boolean _
    '                                            , ByVal blnAddToExist As Boolean _
    '                                            , ByVal intPeriodUnkID As Integer _
    '                                            , ByVal strMedicalRefNo As String _
    '                                            , ByVal dtCumulative_Startdate As Date _
    '                                            , ByVal dtStop_date As Date _
    '                                            , ByVal blnAllowToApproveEarningDeduction As Boolean _
    '                                            , ByVal objDataOperation As clsDataOperation _
    '                                            , ByVal dtCurrentDateAndTime As Date _
    '                                            , Optional ByVal blnInsertSalaryHead As Boolean = False _
    '                                            , Optional ByVal blnCopyPreviousEDSlab As Boolean = False _
    '                                            , Optional ByVal blnOverwritePreviousEDSlabHeads As Boolean = False _
    '                                            , Optional ByVal blnApplyUserAccessFilter As Boolean = True _
    '                                            , Optional ByVal strFilerString As String = "" _
    '                                            , Optional ByVal bw As BackgroundWorker = Nothing _
    '                                            , Optional ByVal blnCheckMembershipHeads As Boolean = True _
    '                              ) As Boolean
    '        '                     'Sohail (02 Dec 2016) - [blnCheckMembershipHeads]
    '        '                     'Sohail (12 Dec 2015) - [bw]
    '        '                     'Sohail (21 Aug 2015) - [strDatabaseName, xUserUnkid, xYearUnkid, xCompanyUnkid, xPeriodStart, xPeriodEnd, xUserModeSetting, xOnlyApproved, xIncludeIn_ActiveEmployee, blnAllowToApproveEarningDeduction, dtCurrentDateAndTime, blnApplyUserAccessFilter, strFilerString]
    '        '                     'Sohail (09 Nov 2013) - [dtCumulative_Startdate, dtStop_date]

    '        Dim dsList As DataSet = Nothing
    '        Dim strQ As String = ""
    '        Dim arrEmp As String()
    '        Dim exForce As Exception
    '        Dim blnFlag As Boolean
    '        Dim intEDID As Integer
    '        Dim objTranHead As New clsTransactionHead
    '        Dim mdtPeriod_startdate As Date
    '        Dim mdtPeriod_enddate As Date
    '        Try

    '            arrEmp = strEmployeeList.Split(",")

    '            Dim objPeriod As New clscommom_period_Tran
    '            'Sohail (21 Aug 2015) -- Start
    '            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
    '            'objPeriod._DatabaseName = mstrDatabaseName
    '            'objPeriod._Periodunkid = intPeriodUnkID

    '            'S.SANDEEP [19 OCT 2016] -- START
    '            'ISSUE : EMPLOYEE MASTER BIND TRANSACTION ISSUE
    '            objPeriod._xDataOperation = objDataOperation
    '            'S.SANDEEP [19 OCT 2016] -- END


    '            objPeriod._DatabaseName = strDatabaseName
    '            objPeriod._Periodunkid(strDatabaseName) = intPeriodUnkID
    '            'Sohail (21 Aug 2015) -- End
    '            mdtPeriod_startdate = objPeriod._Start_Date
    '            mdtPeriod_enddate = objPeriod._End_Date

    '            'Sohail (12 Dec 2015) -- Start
    '            'Enhancement - Showing Progress of closing period.
    '            clsClosePeriod._ProgressTotalCount = arrEmp.Length
    '            'Sohail (12 Dec 2015) -- End

    '            For i As Integer = 0 To arrEmp.Length - 1
    '                mintEmployeeunkid = CInt(arrEmp(i))

    '                If blnInsertSalaryHead = True Then
    '                    'Sohail (21 Aug 2015) -- Start
    '                    'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
    '                    'blnFlag = Insert_SalaryHead(mintEmployeeunkid, intPeriodUnkID, objDataOperation)
    '                    blnFlag = Insert_SalaryHead(strDatabaseName, xUserModeSetting, xYearUnkid, xCompanyUnkid, xPeriodStart, xPeriodEnd, xUserModeSetting, xOnlyApproved, xIncludeIn_ActiveEmployee, mintEmployeeunkid, intPeriodUnkID, dtCurrentDateAndTime, objDataOperation, blnApplyUserAccessFilter, strFilerString)
    '                    'Sohail (21 Aug 2015) -- End

    '                    If blnFlag = False Then
    '                        objDataOperation.ReleaseTransaction(False)
    '                        Return False
    '                    End If
    '                End If

    '                If intTranHeadID <= 0 Then GoTo COPY_PREVIOUS_ED_SLAB '[To copy previous ed slab when closing period]

    '                'Sohail (21 Aug 2015) -- Start
    '                'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
    '                'intEDID = GetEDUnkID(mintEmployeeunkid, intTranHeadID, intPeriodUnkID, objDataOperation)
    '                intEDID = GetEDUnkID(strDatabaseName, mintEmployeeunkid, intTranHeadID, intPeriodUnkID, objDataOperation)
    '                'Sohail (21 Aug 2015) -- End

    '                objDataOperation.ClearParameters()

    '                If intEDID > 0 Then
    '                    'Sohail (21 Aug 2015) -- Start
    '                    'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
    '                    'Me._Edunkid = intEDID
    '                    Me._Edunkid(objDataOperation, strDatabaseName) = intEDID
    '                    'Sohail (21 Aug 2015) -- End

    '                    If blnOverWrite = True Then
    '                        mdecAmount = decAmount
    '                        mintCurrencyunkid = intCurrencyID
    '                        mintVendorunkid = intVendorID
    '                        mintMembership_Categoryunkid = 0
    '                        mintPeriodunkid = intPeriodUnkID
    '                        mstrMedicalrefno = strMedicalRefNo
    '                        'Sohail (09 Nov 2013) -- Start
    '                        'TRA - ENHANCEMENT
    '                        mdtCumulative_Startdate = dtCumulative_Startdate
    '                        mdtStop_Date = dtStop_date
    '                        'Sohail (09 Nov 2013) -- End
    '                    ElseIf blnAddToExist = True Then
    '                        mdecAmount += decAmount
    '                    End If

    '                    If blnOverWrite = True OrElse blnAddToExist = True Then
    '                        'Sohail (21 Aug 2015) -- Start
    '                        'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
    '                        'mintUserunkid = User._Object._Userunkid
    '                        mintUserunkid = xUserUnkid
    '                        'Sohail (21 Aug 2015) -- End
    '                        'Sohail (21 Aug 2015) -- Start
    '                        'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
    '                        'blnFlag = Update(True, objDataOperation)
    '                        blnFlag = Update(strDatabaseName, True, objDataOperation, dtCurrentDateAndTime)
    '                        'Sohail (21 Aug 2015) -- End
    '                    Else
    '                        blnFlag = True
    '                    End If
    '                Else
    '                    mintTranheadunkid = intTranHeadID
    '                    'Sohail (21 Aug 2015) -- Start
    '                    'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
    '                    'objTranHead._Tranheadunkid = intTranHeadID

    '                    'S.SANDEEP [19 OCT 2016] -- START
    '                    'ISSUE : EMPLOYEE MASTER BIND TRANSACTION ISSUE
    '                    objTranHead._xDataOperation = objDataOperation
    '                    'S.SANDEEP [19 OCT 2016] -- END


    '                    objTranHead._Tranheadunkid(strDatabaseName) = intTranHeadID
    '                    'Sohail (21 Aug 2015) -- End
    '                    mintBatchtransactionunkid = intBatchID

    '                    mintTrnheadtype_Id = objTranHead._Trnheadtype_Id
    '                    mintTypeof_Id = objTranHead._Typeof_id
    '                    mintCalctype_Id = objTranHead._Calctype_Id
    '                    mintComputeon_Id = objTranHead._Computeon_Id
    '                    mstrFormula = objTranHead._Formula
    '                    mstrFormulaid = objTranHead._Formulaid
    '                    mdecAmount = decAmount

    '                    If objTranHead._Trnheadtype_Id = 1 Then 'enTranHeadType.EarningForEmployees
    '                        mblnIsdeduct = False
    '                    ElseIf objTranHead._Trnheadtype_Id = 4 Or objTranHead._Trnheadtype_Id = 5 Then 'enTranHeadType.EmployersStatutoryContributions , Informational
    '                        mblnIsdeduct = Nothing
    '                    Else
    '                        mblnIsdeduct = True
    '                    End If

    '                    mintCurrencyunkid = intCurrencyID
    '                    mintVendorunkid = intVendorID
    '                    'Sohail (21 Aug 2015) -- Start
    '                    'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
    '                    'mintUserunkid = User._Object._Userunkid 'Sohail (14 Oct 2010)
    '                    mintUserunkid = xUserUnkid
    '                    'Sohail (21 Aug 2015) -- End
    '                    mblnIsvoid = False
    '                    mintVoiduserunkid = -1
    '                    mdtVoiddatetime = Nothing
    '                    mstrVoidreason = ""
    '                    mintMembership_Categoryunkid = 0
    '                    'Sohail (14 Jun 2011) -- Start
    '                    'Sohail (05 Jul 2011) -- Start
    '                    'Sohail (21 Aug 2015) -- Start
    '                    'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
    '                    'If User._Object.Privilege._AllowToApproveEarningDeduction = True Then
    '                    If blnAllowToApproveEarningDeduction = True Then
    '                        'Sohail (21 Aug 2015) -- End
    '                        mblnIsapproved = True
    '                        'Sohail (21 Aug 2015) -- Start
    '                        'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
    '                        'mintApproveruserunkid = User._Object._Userunkid
    '                        mintApproveruserunkid = xUserUnkid
    '                        'Sohail (21 Aug 2015) -- End
    '                    Else
    '                        mblnIsapproved = False
    '                        mintApproveruserunkid = -1
    '                    End If
    '                    mintPeriodunkid = intPeriodUnkID 'Sohail (13 Jan 2012)
    '                    mstrMedicalrefno = strMedicalRefNo 'Sohail (18 Jan 2012)
    '                    'Sohail (30 Mar 2013) -- Start
    '                    'TRA - ENHANCEMENT
    '                    mintMembershipTranUnkid = 0
    '                    mintDisciplinefileunkid = 0
    '                    'Sohail (30 Mar 2013) -- End
    '                    'Sohail (09 Nov 2013) -- Start
    '                    'TRA - ENHANCEMENT
    '                    mdtCumulative_Startdate = dtCumulative_Startdate
    '                    mdtStop_Date = dtStop_date
    '                    'Sohail (09 Nov 2013) -- End
    '                    mintEmpbatchpostingunkid = 0 'Sohail (24 Feb 2016)

    '                    'Sohail (21 Aug 2015) -- Start
    '                    'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
    '                    'blnFlag = Insert(True, , objDataOperation)
    '                    'Sohail (02 Dec 2016) -- Start
    '                    'Enhancement - 64.1 - Insert Membership heads on Ed if not assigned.
    '                    'blnFlag = Insert(strDatabaseName, xUserUnkid, xYearUnkid, xCompanyUnkid, xPeriodStart, xPeriodEnd, xUserModeSetting, xOnlyApproved, xIncludeIn_ActiveEmployee, True, dtCurrentDateAndTime, , objDataOperation, blnApplyUserAccessFilter, strFilerString)
    '                    'Sohail (20 Jul 2018) -- Start
    '                    'Support Issue # 0002383 : VOLTAMP: Unable to assign Transaction heads using batch entry in Payroll transactions (Warning message of Membership heads not assigned)in 73.1.
    '                    'blnFlag = Insert(strDatabaseName, xUserUnkid, xYearUnkid, xCompanyUnkid, xPeriodStart, xPeriodEnd, xUserModeSetting, xOnlyApproved, xIncludeIn_ActiveEmployee, True, dtCurrentDateAndTime, , objDataOperation, blnApplyUserAccessFilter, strFilerString, blnCheckMembershipHeads)
    '                    blnFlag = Insert(strDatabaseName, xUserUnkid, xYearUnkid, xCompanyUnkid, xPeriodStart, xPeriodEnd, xUserModeSetting, xOnlyApproved, xIncludeIn_ActiveEmployee, True, dtCurrentDateAndTime, , objDataOperation, blnApplyUserAccessFilter, strFilerString, blnCheckMembershipHeads, blnAllowToApproveEarningDeduction)
    '                    'Sohail (20 Jul 2018) -- End
    '                    'Sohail (02 Dec 2016) -- End
    '                    'Sohail (21 Aug 2015) -- End
    '                End If

    '                If blnFlag = False Then
    '                    'S.SANDEEP [21-JUN-2017] -- START
    '                    'objDataOperation.ReleaseTransaction(False)
    '                    If objDataOperation Is Nothing Then
    '                    objDataOperation.ReleaseTransaction(False)
    '                    End If
    '                    'S.SANDEEP [21-JUN-2017] -- END
    '                    Return False
    '                End If


    'COPY_PREVIOUS_ED_SLAB:  'Sohail (21 Jul 2012)

    '                'Sohail (07 Mar 2012) -- Start
    '                'TRA - ENHANCEMENT
    '                '**** Copy Previous ED Slab
    '                If blnCopyPreviousEDSlab = True Then
    '                    'Sohail (21 Aug 2015) -- Start
    '                    'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
    '                    'Call GetEmployeeED()
    '                    'Sohail (01 Mar 2018) -- Start
    '                    'Internal Issue : ExecureReader requires the command to have a transaction... in 70.1.
    '                    'Call GetEmployeeED(strDatabaseName)
    '                    Call GetEmployeeED(objDataOperation, strDatabaseName)
    '                    'Sohail (01 Mar 2018) -- End
    '                    'Sohail (21 Aug 2015) -- End

    '                    Dim strPrevEndDate As String = ""
    '                    Dim drRow As DataRow()

    '                    'Sohail (09 Nov 2013) -- Start
    '                    'TRA - ENHANCEMENT
    '                    'drRow = mdtED.Select("AUD = '' AND end_date <= '" & eZeeDate.convertDate(mdtPeriod_startdate) & "' AND isrecurrent = 1 ", "end_date DESC")
    '                    drRow = mdtED.Select("AUD = '' AND end_date <= '" & eZeeDate.convertDate(mdtPeriod_startdate) & "' AND isrecurrent = 1 AND (stop_date IS NULL OR stop_date >= '" & mdtPeriod_startdate & "') ", "end_date DESC")
    '                    'Sohail (09 Nov 2013) -- End

    '                    If drRow.Length > 0 Then
    '                        For Each dRow As DataRow In drRow
    '                            If strPrevEndDate <> "" AndAlso strPrevEndDate <> dRow.Item("end_date").ToString Then
    '                                Exit For
    '                            End If

    '                            objTranHead._DatabaseName = mstrDatabaseName 'Sohail (21 Jul 2012)
    '                            'Sohail (21 Aug 2015) -- Start
    '                            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
    '                            'objTranHead._Tranheadunkid = CInt(dRow.Item("tranheadunkid"))
    '                            objTranHead._Tranheadunkid(strDatabaseName) = CInt(dRow.Item("tranheadunkid"))
    '                            'Sohail (21 Aug 2015) -- End
    '                            'Sohail (25 May 2012) -- Start
    '                            'TRA - ENHANCEMENT
    '                            If objTranHead._Typeof_id = enTypeOf.Salary OrElse CInt(dRow.Item("tranheadunkid")) = intTranHeadID Then
    '                                'If objTranHead._Typeof_id = enTypeOf.Salary Then
    '                                'Sohail (25 May 2012) -- End
    '                                strPrevEndDate = dRow.Item("end_date").ToString
    '                                Continue For
    '                            End If

    '                            'Sohail (21 Aug 2015) -- Start
    '                            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
    '                            'intEDID = GetEDUnkID(mintEmployeeunkid, CInt(dRow.Item("tranheadunkid")), intPeriodUnkID)
    '                            intEDID = GetEDUnkID(strDatabaseName, mintEmployeeunkid, CInt(dRow.Item("tranheadunkid")), intPeriodUnkID, objDataOperation)
    '                            'Sohail (21 Aug 2015) -- End

    '                            If intEDID > 0 Then
    '                                'Sohail (21 Aug 2015) -- Start
    '                                'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
    '                                'Me._Edunkid = intEDID 'Sohail (25 May 2012)
    '                                Me._Edunkid(objDataOperation, strDatabaseName) = intEDID
    '                                'Sohail (21 Aug 2015) -- End
    '                                'Sohail (26 Jul 2012) -- Start
    '                                'TRA - ENHANCEMENT
    '                                'If blnOverWrite = True Then
    '                                If blnOverwritePreviousEDSlabHeads = True Then
    '                                    'Sohail (26 Jul 2012) -- End
    '                                    mdecAmount = CDec(dRow.Item("amount"))
    '                                    mintCurrencyunkid = CInt(dRow.Item("currencyid"))
    '                                    mintVendorunkid = CInt(dRow.Item("vendorid"))
    '                                    If IsDBNull(dRow.Item("membership_categoryunkid")) Then
    '                                        mintMembership_Categoryunkid = 0
    '                                    Else
    '                                        mintMembership_Categoryunkid = CInt(dRow.Item("membership_categoryunkid"))
    '                                    End If
    '                                    mintCostCenterUnkId = CInt(dRow.Item("costcenterunkid"))
    '                                    mstrMedicalrefno = dRow.Item("medicalrefno").ToString
    '                                    'Sohail (21 Aug 2015) -- Start
    '                                    'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
    '                                    'mintUserunkid = User._Object._Userunkid
    '                                    mintUserunkid = xUserUnkid
    '                                    'Sohail (21 Aug 2015) -- End
    '                                    'Sohail (20 Feb 2013) -- Start
    '                                    'TRA - ENHANCEMENT
    '                                    mintMembershipTranUnkid = CInt(dRow.Item("membershiptranunkid"))
    '                                    mintDisciplinefileunkid = CInt(dRow.Item("disciplinefileunkid"))
    '                                    'Sohail (20 Feb 2013) -- End
    '                                    'Sohail (09 Nov 2013) -- Start
    '                                    'TRA - ENHANCEMENT
    '                                    If IsDBNull(dRow.Item("cumulative_startdate")) = True Then
    '                                        mdtCumulative_Startdate = Nothing
    '                                    Else
    '                                        mdtCumulative_Startdate = dRow.Item("cumulative_startdate")
    '                                    End If
    '                                    If IsDBNull(dRow.Item("stop_date")) = True Then
    '                                        mdtStop_Date = Nothing
    '                                    Else
    '                                        mdtStop_Date = dRow.Item("stop_date")
    '                                    End If
    '                                    'Sohail (09 Nov 2013) -- End
    '                                    mintEmpbatchpostingunkid = 0 'Sohail (24 Feb 2016)
    '                                    'Sohail (21 Aug 2015) -- Start
    '                                    'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
    '                                    'blnFlag = Update(True)
    '                                    blnFlag = Update(strDatabaseName, True, objDataOperation, dtCurrentDateAndTime)
    '                                    'Sohail (21 Aug 2015) -- End
    '                                Else
    '                                    blnFlag = True
    '                                End If
    '                            Else
    '                                mintTranheadunkid = CInt(dRow.Item("tranheadunkid"))
    '                                objTranHead._DatabaseName = mstrDatabaseName 'Sohail (21 Jul 2012)
    '                                'Sohail (21 Aug 2015) -- Start
    '                                'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
    '                                'objTranHead._Tranheadunkid = CInt(dRow.Item("tranheadunkid"))
    '                                objTranHead._Tranheadunkid(strDatabaseName) = CInt(dRow.Item("tranheadunkid"))
    '                                'Sohail (21 Aug 2015) -- End
    '                                mintBatchtransactionunkid = CInt(dRow.Item("batchtransactionunkid"))

    '                                mintTrnheadtype_Id = objTranHead._Trnheadtype_Id
    '                                mintTypeof_Id = objTranHead._Typeof_id
    '                                mintCalctype_Id = objTranHead._Calctype_Id
    '                                mintComputeon_Id = objTranHead._Computeon_Id
    '                                mstrFormula = objTranHead._Formula
    '                                mstrFormulaid = objTranHead._Formulaid
    '                                mdecAmount = CDec(dRow.Item("amount"))

    '                                If objTranHead._Trnheadtype_Id = enTranHeadType.EarningForEmployees Then
    '                                    mblnIsdeduct = False
    '                                ElseIf objTranHead._Trnheadtype_Id = enTranHeadType.EmployersStatutoryContributions OrElse objTranHead._Trnheadtype_Id = enTranHeadType.Informational Then
    '                                    mblnIsdeduct = Nothing
    '                                Else
    '                                    mblnIsdeduct = True
    '                                End If

    '                                mintCurrencyunkid = CInt(dRow.Item("currencyid"))
    '                                mintVendorunkid = CInt(dRow.Item("vendorid"))
    '                                'Sohail (21 Aug 2015) -- Start
    '                                'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
    '                                'mintUserunkid = User._Object._Userunkid
    '                                mintUserunkid = xUserUnkid
    '                                'Sohail (21 Aug 2015) -- End
    '                                mblnIsvoid = False
    '                                mintVoiduserunkid = -1
    '                                mdtVoiddatetime = Nothing
    '                                mstrVoidreason = ""
    '                                mintMembership_Categoryunkid = 0
    '                                'Sohail (21 Aug 2015) -- Start
    '                                'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
    '                                'If User._Object.Privilege._AllowToApproveEarningDeduction = True Then
    '                                If blnAllowToApproveEarningDeduction = True Then
    '                                    'Sohail (21 Aug 2015) -- End
    '                                    mblnIsapproved = True
    '                                    'Sohail (21 Aug 2015) -- Start
    '                                    'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
    '                                    'mintApproveruserunkid = User._Object._Userunkid
    '                                    mintApproveruserunkid = xUserUnkid
    '                                    'Sohail (21 Aug 2015) -- End
    '                                Else
    '                                    mblnIsapproved = False
    '                                    mintApproveruserunkid = -1
    '                                End If
    '                                mintPeriodunkid = intPeriodUnkID
    '                                mintCostCenterUnkId = 0
    '                                mstrMedicalrefno = ""
    '                                'Sohail (20 Feb 2013) -- Start
    '                                'TRA - ENHANCEMENT
    '                                mintMembershipTranUnkid = CInt(dRow.Item("membershiptranunkid"))
    '                                mintDisciplinefileunkid = CInt(dRow.Item("disciplinefileunkid"))
    '                                'Sohail (20 Feb 2013) -- End
    '                                'Sohail (09 Nov 2013) -- Start
    '                                'TRA - ENHANCEMENT
    '                                If IsDBNull(dRow.Item("cumulative_startdate")) = True Then
    '                                    mdtCumulative_Startdate = Nothing
    '                                Else
    '                                    mdtCumulative_Startdate = dRow.Item("cumulative_startdate")
    '                                End If
    '                                If IsDBNull(dRow.Item("stop_date")) = True Then
    '                                    mdtStop_Date = Nothing
    '                                Else
    '                                    mdtStop_Date = dRow.Item("stop_date")
    '                                End If
    '                                'Sohail (09 Nov 2013) -- End
    '                                mintEmpbatchpostingunkid = 0 'Sohail (24 Feb 2016)
    '                                'Sohail (21 Aug 2015) -- Start
    '                                'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
    '                                'blnFlag = Insert(True)
    '                                'Sohail (02 Dec 2016) -- Start
    '                                'Enhancement - 64.1 - Insert Membership heads on Ed if not assigned.
    '                                'blnFlag = Insert(strDatabaseName, xUserUnkid, xYearUnkid, xCompanyUnkid, xPeriodStart, xPeriodEnd, xUserModeSetting, xOnlyApproved, xIncludeIn_ActiveEmployee, True, dtCurrentDateAndTime, , objDataOperation, blnApplyUserAccessFilter, strFilerString)
    '                                'Sohail (20 Jul 2018) -- Start
    '                                'Support Issue # 0002383 : VOLTAMP: Unable to assign Transaction heads using batch entry in Payroll transactions (Warning message of Membership heads not assigned)in 73.1.
    '                                'blnFlag = Insert(strDatabaseName, xUserUnkid, xYearUnkid, xCompanyUnkid, xPeriodStart, xPeriodEnd, xUserModeSetting, xOnlyApproved, xIncludeIn_ActiveEmployee, True, dtCurrentDateAndTime, , objDataOperation, blnApplyUserAccessFilter, strFilerString, blnCheckMembershipHeads)
    '                                blnFlag = Insert(strDatabaseName, xUserUnkid, xYearUnkid, xCompanyUnkid, xPeriodStart, xPeriodEnd, xUserModeSetting, xOnlyApproved, xIncludeIn_ActiveEmployee, True, dtCurrentDateAndTime, , objDataOperation, blnApplyUserAccessFilter, strFilerString, False, blnAllowToApproveEarningDeduction)
    '                                'Sohail (20 Jul 2018) -- End
    '                                'Sohail (02 Dec 2016) -- End
    '                                'Sohail (21 Aug 2015) -- End
    '                            End If

    '                            If blnFlag = False Then
    '                                objDataOperation.ReleaseTransaction(False)
    '                                Return False
    '                            End If

    '                            strPrevEndDate = dRow.Item("end_date").ToString
    '                        Next
    '                    End If
    '                End If
    '                'Sohail (07 Mar 2012) -- End

    '                'Sohail (12 Dec 2015) -- Start
    '                'Enhancement - Showing Progress of closing period.
    '                If bw IsNot Nothing Then
    '                    clsClosePeriod._ProgressCurrCount = i + 1
    '                    bw.ReportProgress(7)
    '                End If
    '                'Sohail (12 Dec 2015) -- End
    '            Next

    '            'Sohail (12 Dec 2015) -- Start
    '            'Enhancement - Showing Progress of closing period.
    '            clsClosePeriod._ProgressTotalCount = 0
    '            'Sohail (12 Dec 2015) -- End

    '            Return True
    '        Catch ex As Exception
    '            objDataOperation.ReleaseTransaction(False)
    '            Throw New Exception(ex.Message & "; Procedure Name: InsertAllByEmployeeList; Module Name: " & mstrModuleName)
    '            Return False
    '        Finally
    '            exForce = Nothing
    '            If dsList IsNot Nothing Then dsList.Dispose()
    '            objTranHead = Nothing
    '        End Try
    '    End Function
    '    'S.SANDEEP [ 04 SEP 2012 ] -- END
    Public Function InsertAllByEmployeeList(ByVal strDatabaseName As String _
                                            , ByVal xUserUnkid As Integer _
                                            , ByVal xYearUnkid As Integer _
                                            , ByVal xCompanyUnkid As Integer _
                                            , ByVal xPeriodStart As DateTime _
                                            , ByVal xPeriodEnd As DateTime _
                                            , ByVal xUserModeSetting As String _
                                            , ByVal xOnlyApproved As Boolean _
                                            , ByVal xIncludeIn_ActiveEmployee As Boolean _
                                            , ByVal strEmployeeList As String _
                                            , ByVal intTranHeadID As Integer _
                                            , ByVal intCurrencyID As Integer _
                                            , ByVal intVendorID As Integer _
                                            , ByVal intBatchID As Double _
                                            , ByVal decAmount As Decimal _
                                            , ByVal blnOverWrite As Boolean _
                                            , ByVal blnAddToExist As Boolean _
                                            , ByVal intPeriodUnkID As Integer _
                                            , ByVal strMedicalRefNo As String _
                                            , ByVal dtCumulative_Startdate As Date _
                                            , ByVal dtStop_date As Date _
                                            , ByVal blnAllowToApproveEarningDeduction As Boolean _
                                            , ByVal xDataOp As clsDataOperation _
                                            , ByVal dtCurrentDateAndTime As Date _
                                            , Optional ByVal blnInsertSalaryHead As Boolean = False _
                                            , Optional ByVal blnCopyPreviousEDSlab As Boolean = False _
                                            , Optional ByVal blnOverwritePreviousEDSlabHeads As Boolean = False _
                                            , Optional ByVal blnApplyUserAccessFilter As Boolean = True _
                                            , Optional ByVal strFilerString As String = "" _
                                            , Optional ByVal bw As BackgroundWorker = Nothing _
                                            , Optional ByVal blnCheckMembershipHeads As Boolean = True _
                                            , Optional ByVal intCopySelectedPeriodUnkId As Integer = 0 _
                                            , Optional ByVal dtEDStart_date As Date = Nothing _
                                            , Optional ByVal blnSkipInsertIfExist As Boolean = False _
                              ) As Boolean
        'Sohail (25 Jun 2020) - [blnSkipInsertIfExist]
        'Sohail (24 Aug 2019) - [edstart_date]

        Dim dsList As DataSet = Nothing
        Dim dsHead As DataSet = Nothing
        Dim strQ As String = ""
        Dim arrEmp As String()
        Dim exForce As Exception
        Dim blnFlag As Boolean
        Dim objTranHead As New clsTransactionHead
        Dim mdtPeriod_startdate As Date
        Dim mdtPeriod_enddate As Date

        If xDataOp IsNot Nothing Then
            objDataOperation = xDataOp
        Else
            objDataOperation = New clsDataOperation
            'objDataOperation.BindTransaction()'Sohail (10 Dec 2019)
        End If
        objDataOperation.ClearParameters()

        Try

            arrEmp = strEmployeeList.Split(",")

            Dim objPeriod As New clscommom_period_Tran

            objPeriod._xDataOperation = objDataOperation
            objPeriod._DatabaseName = strDatabaseName
            objPeriod._Periodunkid(strDatabaseName) = intPeriodUnkID
            mdtPeriod_startdate = objPeriod._Start_Date
            mdtPeriod_enddate = objPeriod._End_Date

            'Sohail (18 Jan 2019) -- Start
            'Issue - 74.1 - Bind Transaction issue.
            'dsHead = objTranHead.GetList(strDatabaseName, -1, -1, False, True, 0, False, "", True, True, True, 2)
            'Sohail (25 Jun 2020) -- Start
            'NMB Enhancement # : Performance enhancement on Import earning deduction heads.
            'dsHead = objTranHead.GetList(strDatabaseName, -1, -1, False, True, 0, False, "", True, True, True, 2, objDataOperation)
            dsHead = objTranHead.GetList(strDatabaseName, -1, -1, False, True, 0, False, "", True, True, True, 2, objDataOperation, xPeriodEnd)
            'Sohail (25 Jun 2020) -- End
            'Sohail (18 Jan 2019) -- End

            clsClosePeriod._ProgressTotalCount = arrEmp.Length

            For i As Integer = 0 To arrEmp.Length - 1
                mintEmployeeunkid = CInt(arrEmp(i))

                'Sohail (10 Dec 2019) -- Start
                'NMB UAT Enhancement # : Not able to access employee payroll while doing batch entry. 
                If xDataOp IsNot Nothing Then
                    objDataOperation = xDataOp
                Else
                    objDataOperation = New clsDataOperation
                    objDataOperation.BindTransaction()
                End If
                'Sohail (10 Dec 2019) -- End

                If blnInsertSalaryHead = True Then
                    blnFlag = Insert_SalaryHead(strDatabaseName, xUserUnkid, xYearUnkid, xCompanyUnkid, xPeriodStart, xPeriodEnd, xUserModeSetting, xOnlyApproved, xIncludeIn_ActiveEmployee, mintEmployeeunkid, intPeriodUnkID, dtCurrentDateAndTime, objDataOperation, blnApplyUserAccessFilter, strFilerString)

                    If blnFlag = False Then
                        objDataOperation.ReleaseTransaction(False)
                        Return False
                    End If
                End If

                If intTranHeadID <= 0 Then GoTo COPY_PREVIOUS_ED_SLAB '[To copy previous ed slab when closing period]

                'Sohail (25 Jun 2020) -- Start
                'NMB Enhancement # : Performance enhancement on Import earning deduction heads.
                'Call GetEmployeeED(objDataOperation, strDatabaseName)

                'Dim drED() As DataRow = mdtED.Select("employeeunkid = " & mintEmployeeunkid & " AND tranheadunkid = " & intTranHeadID & " AND periodunkid = " & intPeriodUnkID & " ")
                'Sohail (25 Jun 2020) -- End

                objDataOperation.ClearParameters()

                'Sohail (25 Jun 2020) -- Start
                'NMB Enhancement # : Performance enhancement on Import earning deduction heads.
                'If drED.Length > 0 Then
                '    Call GetData(drED(0))
                If GetDataIfExist(objDataOperation, strDatabaseName, mintEmployeeunkid, intTranHeadID, intPeriodUnkID) = True Then
                    'Sohail (25 Jun 2020) -- End
                    If blnOverWrite = True Then
                        mdecAmount = decAmount
                        mintCurrencyunkid = intCurrencyID
                        mintVendorunkid = intVendorID
                        mintMembership_Categoryunkid = 0
                        mintPeriodunkid = intPeriodUnkID
                        mstrMedicalrefno = strMedicalRefNo
                        mdtCumulative_Startdate = dtCumulative_Startdate
                        mdtStop_Date = dtStop_date
                        'Sohail (24 Aug 2019) -- Start
                        'NMB Payroll UAT # TC016 - 76.1 - System should be able to accept future amendments (Future period) and prorate. It should be possible to set start date of the specific transaction and prorate based on the start date. It should also be possible to set start date of the future period.
                        mdtEDStart_Date = dtEDStart_date
                        'Sohail (24 Aug 2019) -- End
                    ElseIf blnAddToExist = True Then
                        mdecAmount += decAmount
                    End If

                    'Sohail (02 Sep 2019) -- Start
                    'Mainspring Resourcing Ghana Ltd Enhancement - 76.1 - # 0004098: Approval Flow for Flat Rate Heads Amount Uploaded.
                    '1. Set ED in pending status if user has not priviledge of ED approval
                    '2. Perform Global approver with option to filter for allocation and approved any posted (new /edited) E & D values in MSS
                    '3. Send notifications to ED approvers for all employees ED in one email
                    If blnAllowToApproveEarningDeduction = True Then
                        mblnIsapproved = True
                        mintApproveruserunkid = xUserUnkid
                    Else
                        mblnIsapproved = False
                        mintApproveruserunkid = -1
                    End If
                    'Sohail (02 Sep 2019) -- End

                    If blnOverWrite = True OrElse blnAddToExist = True Then
                        mintUserunkid = xUserUnkid
                        blnFlag = Update(strDatabaseName, True, objDataOperation, dtCurrentDateAndTime)
                    Else
                        blnFlag = True
                    End If

                Else
                    mintTranheadunkid = intTranHeadID

                    Dim drHead() As DataRow = dsHead.Tables(0).Select("tranheadunkid = " & intTranHeadID & " ")
                    mintBatchtransactionunkid = intBatchID

                    If drHead.Length <= 0 Then
                        mstrMessage = Language.getMessage(mstrModuleName, 6, "Transaction head doees not exist.") & "[" & intTranHeadID & "]"
                        objDataOperation.ReleaseTransaction(False)
                        Return False
                    Else

                        mintTrnheadtype_Id = CInt(drHead(0).Item("trnheadtype_id"))
                        mintTypeof_Id = CInt(drHead(0).Item("typeof_id"))
                        mintCalctype_Id = CInt(drHead(0).Item("calctype_id"))
                        mintComputeon_Id = CInt(drHead(0).Item("computeon_id"))
                        mstrFormula = drHead(0).Item("formula").ToString
                        mstrFormulaid = drHead(0).Item("formulaid")
                    mdecAmount = decAmount

                        If CInt(drHead(0).Item("trnheadtype_id")) = CInt(enTranHeadType.EarningForEmployees) Then 'enTranHeadType.EarningForEmployees
                        mblnIsdeduct = False
                        ElseIf CInt(drHead(0).Item("trnheadtype_id")) = CInt(enTranHeadType.EmployersStatutoryContributions) OrElse CInt(drHead(0).Item("trnheadtype_id")) = CInt(enTranHeadType.Informational) Then 'enTranHeadType.EmployersStatutoryContributions , Informational
                        mblnIsdeduct = Nothing
                    Else
                        mblnIsdeduct = True
                    End If

                    mintCurrencyunkid = intCurrencyID
                    mintVendorunkid = intVendorID
                    mintUserunkid = xUserUnkid
                    mblnIsvoid = False
                    mintVoiduserunkid = -1
                    mdtVoiddatetime = Nothing
                    mstrVoidreason = ""
                    mintMembership_Categoryunkid = 0

                    If blnAllowToApproveEarningDeduction = True Then
                        mblnIsapproved = True
                        mintApproveruserunkid = xUserUnkid
                    Else
                        mblnIsapproved = False
                        mintApproveruserunkid = -1
                    End If
                        mintPeriodunkid = intPeriodUnkID
                        mstrMedicalrefno = strMedicalRefNo
                    mintMembershipTranUnkid = 0
                    mintDisciplinefileunkid = 0
                    mdtCumulative_Startdate = dtCumulative_Startdate
                    mdtStop_Date = dtStop_date
                        'Sohail (24 Aug 2019) -- Start
                        'NMB Payroll UAT # TC016 - 76.1 - System should be able to accept future amendments (Future period) and prorate. It should be possible to set start date of the specific transaction and prorate based on the start date. It should also be possible to set start date of the future period.
                        mdtEDStart_Date = dtEDStart_date
                        'Sohail (24 Aug 2019) -- End
                        mintEmpbatchpostingunkid = 0

                        'Sohail (25 Jun 2020) -- Start
                        'NMB Enhancement # : Performance enhancement on Import earning deduction heads.
                        'blnFlag = Insert(strDatabaseName, xUserUnkid, xYearUnkid, xCompanyUnkid, xPeriodStart, xPeriodEnd, xUserModeSetting, xOnlyApproved, xIncludeIn_ActiveEmployee, True, dtCurrentDateAndTime, , objDataOperation, blnApplyUserAccessFilter, strFilerString, blnCheckMembershipHeads, blnAllowToApproveEarningDeduction)
                        blnFlag = Insert(strDatabaseName, xUserUnkid, xYearUnkid, xCompanyUnkid, xPeriodStart, xPeriodEnd, xUserModeSetting, xOnlyApproved, xIncludeIn_ActiveEmployee, True, dtCurrentDateAndTime, , objDataOperation, blnApplyUserAccessFilter, strFilerString, blnCheckMembershipHeads, blnAllowToApproveEarningDeduction, blnSkipInsertIfExist)
                        'Sohail (25 Jun 2020) -- End

                    End If
                End If

                If blnFlag = False Then
                    objDataOperation.ReleaseTransaction(False)
                    Return False
                End If


COPY_PREVIOUS_ED_SLAB:

                '**** Copy Previous ED Slab
                If blnCopyPreviousEDSlab = True OrElse intCopySelectedPeriodUnkId > 0 Then

                    'Sohail (25 Jun 2020) -- Start
                    ''NMB Enhancement # : Performance enhancement on Import earning deduction heads.
                    'Call GetEmployeeED(objDataOperation, strDatabaseName)

                    'Sohail (19 Sep 2020) -- Start
                    'NMB Issue : #  : Copy previous slab pick latest slab based on current date on close period.
                    'Call GetEmployeeED(objDataOperation, strDatabaseName, "", True)
                    Call GetEmployeeED(objDataOperation, strDatabaseName, "", True, xPeriodEnd)
                    'Sohail (19 Sep 2020) -- End
                    'Sohail (25 Jun 2020) -- End

                    Dim strPrevEndDate As String = ""
                    Dim drRow As DataRow()


                    If blnCopyPreviousEDSlab = True Then
                        'Sohail (24 Aug 2019) -- Start
                        'NMB Payroll UAT # TC016 - 76.1 - System should be able to accept future amendments (Future period) and prorate. It should be possible to set start date of the specific transaction and prorate based on the start date. It should also be possible to set start date of the future period.
                        'drRow = mdtED.Select("AUD = '' AND end_date <= '" & eZeeDate.convertDate(mdtPeriod_startdate) & "' AND isrecurrent = 1 AND (stop_date IS NULL OR stop_date >= '" & mdtPeriod_startdate & "') ", "end_date DESC")
                        drRow = mdtED.Select("AUD = '' AND end_date <= '" & eZeeDate.convertDate(mdtPeriod_startdate) & "' AND isrecurrent = 1 AND (stop_date IS NULL OR stop_date >= '" & mdtPeriod_startdate & "') AND (edstart_date IS NULL OR edstart_date <= '" & mdtPeriod_enddate & "') ", "end_date DESC")
                        'Sohail (24 Aug 2019) -- End
                    ElseIf intCopySelectedPeriodUnkId > 0 Then
                        drRow = mdtED.Select("AUD = '' AND periodunkid = " & intCopySelectedPeriodUnkId & "  ", "end_date DESC")
                    Else
                        drRow = Nothing
                    End If

                    If drRow.Length > 0 Then
                        For Each dRow As DataRow In drRow
                            If strPrevEndDate <> "" AndAlso strPrevEndDate <> dRow.Item("end_date").ToString Then
                                Exit For
                            End If

                            Dim drHead() As DataRow = dsHead.Tables(0).Select("tranheadunkid = " & CInt(dRow.Item("tranheadunkid")) & " ")

                            If drHead.Length <= 0 Then
                                strPrevEndDate = dRow.Item("end_date").ToString
                                Continue For
                            Else
                                If CInt(drHead(0).Item("typeof_id")) = enTypeOf.Salary OrElse CInt(dRow.Item("tranheadunkid")) = intTranHeadID Then
                                strPrevEndDate = dRow.Item("end_date").ToString
                                Continue For
                            End If
                            End If

                            'Sohail (25 Jun 2020) -- Start
                            'NMB Enhancement # : Performance enhancement on Import earning deduction heads.
                            'drED() = mdtED.Select("employeeunkid = " & mintEmployeeunkid & " AND tranheadunkid = " & CInt(dRow.Item("tranheadunkid")) & " AND periodunkid = " & intPeriodUnkID & " ")
                            Dim drED() As DataRow = mdtED.Select("employeeunkid = " & mintEmployeeunkid & " AND tranheadunkid = " & CInt(dRow.Item("tranheadunkid")) & " AND periodunkid = " & intPeriodUnkID & " ")
                            'Sohail (25 Jun 2020) -- End

                            If drED.Length > 0 Then
                                Call GetData(drED(0))

                                If blnOverwritePreviousEDSlabHeads = True Then
                                    mdecAmount = CDec(dRow.Item("amount"))
                                    mintCurrencyunkid = CInt(dRow.Item("currencyid"))
                                    mintVendorunkid = CInt(dRow.Item("vendorid"))
                                    If IsDBNull(dRow.Item("membership_categoryunkid")) Then
                                        mintMembership_Categoryunkid = 0
                                    Else
                                        mintMembership_Categoryunkid = CInt(dRow.Item("membership_categoryunkid"))
                                    End If
                                    mintCostCenterUnkId = CInt(dRow.Item("costcenterunkid"))
                                    mstrMedicalrefno = dRow.Item("medicalrefno").ToString
                                    mintUserunkid = xUserUnkid
                                    mintMembershipTranUnkid = CInt(dRow.Item("membershiptranunkid"))
                                    mintDisciplinefileunkid = CInt(dRow.Item("disciplinefileunkid"))

                                    If IsDBNull(dRow.Item("cumulative_startdate")) = True Then
                                        mdtCumulative_Startdate = Nothing
                                    Else
                                        mdtCumulative_Startdate = dRow.Item("cumulative_startdate")
                                    End If
                                    If IsDBNull(dRow.Item("stop_date")) = True Then
                                        mdtStop_Date = Nothing
                                    Else
                                        mdtStop_Date = dRow.Item("stop_date")
                                    End If
                                    'Sohail (24 Aug 2019) -- Start
                                    'NMB Payroll UAT # TC016 - 76.1 - System should be able to accept future amendments (Future period) and prorate. It should be possible to set start date of the specific transaction and prorate based on the start date. It should also be possible to set start date of the future period.
                                    If IsDBNull(dRow.Item("edstart_date")) = True Then
                                        mdtEDStart_Date = Nothing
                                    Else
                                        mdtEDStart_Date = dRow.Item("edstart_date")
                                    End If
                                    'Sohail (24 Aug 2019) -- End
                                    mintEmpbatchpostingunkid = 0

                                    'Sohail (02 Sep 2019) -- Start
                                    'Mainspring Resourcing Ghana Ltd Enhancement - 76.1 - # 0004098: Approval Flow for Flat Rate Heads Amount Uploaded.
                                    '1. Set ED in pending status if user has not priviledge of ED approval
                                    '2. Perform Global approver with option to filter for allocation and approved any posted (new /edited) E & D values in MSS
                                    '3. Send notifications to ED approvers for all employees ED in one email
                                    If blnAllowToApproveEarningDeduction = True Then
                                        mblnIsapproved = True
                                        mintApproveruserunkid = mintUserunkid
                                    Else
                                        mblnIsapproved = False
                                        mintApproveruserunkid = -1
                                    End If
                                    'Sohail (02 Sep 2019) -- End

                                    blnFlag = Update(strDatabaseName, True, objDataOperation, dtCurrentDateAndTime)
                                Else
                                    blnFlag = True
                                End If
                            Else
                                mintTranheadunkid = CInt(dRow.Item("tranheadunkid"))
                                drHead = dsHead.Tables(0).Select("tranheadunkid = " & CInt(dRow.Item("tranheadunkid")) & " ")
                                mintBatchtransactionunkid = CInt(dRow.Item("batchtransactionunkid"))

                                If drHead.Length <= 0 Then
                                    strPrevEndDate = dRow.Item("end_date").ToString
                                    Continue For
                                End If

                                mintTrnheadtype_Id = CInt(drHead(0).Item("trnheadtype_id"))
                                mintTypeof_Id = CInt(drHead(0).Item("typeof_id"))
                                mintCalctype_Id = CInt(drHead(0).Item("calctype_id"))
                                mintComputeon_Id = CInt(drHead(0).Item("computeon_id"))
                                mstrFormula = drHead(0).Item("formula").ToString
                                mstrFormulaid = drHead(0).Item("formulaid")
                                mdecAmount = CDec(dRow.Item("amount"))

                                If CInt(drHead(0).Item("trnheadtype_id")) = enTranHeadType.EarningForEmployees Then
                                    mblnIsdeduct = False
                                ElseIf CInt(drHead(0).Item("trnheadtype_id")) = enTranHeadType.EmployersStatutoryContributions OrElse CInt(drHead(0).Item("trnheadtype_id")) = enTranHeadType.Informational Then
                                    mblnIsdeduct = Nothing
                                Else
                                    mblnIsdeduct = True
                                End If

                                mintCurrencyunkid = CInt(dRow.Item("currencyid"))
                                mintVendorunkid = CInt(dRow.Item("vendorid"))
                                mintUserunkid = xUserUnkid
                                mblnIsvoid = False
                                mintVoiduserunkid = -1
                                mdtVoiddatetime = Nothing
                                mstrVoidreason = ""
                                mintMembership_Categoryunkid = 0
                                If blnAllowToApproveEarningDeduction = True Then
                                    mblnIsapproved = True
                                    mintApproveruserunkid = xUserUnkid
                                Else
                                    mblnIsapproved = False
                                    mintApproveruserunkid = -1
                                End If
                                mintPeriodunkid = intPeriodUnkID
                                mintCostCenterUnkId = 0
                                mstrMedicalrefno = ""
                                mintMembershipTranUnkid = CInt(dRow.Item("membershiptranunkid"))
                                mintDisciplinefileunkid = CInt(dRow.Item("disciplinefileunkid"))
                                If IsDBNull(dRow.Item("cumulative_startdate")) = True Then
                                    mdtCumulative_Startdate = Nothing
                                Else
                                    mdtCumulative_Startdate = dRow.Item("cumulative_startdate")
                                End If
                                If IsDBNull(dRow.Item("stop_date")) = True Then
                                    mdtStop_Date = Nothing
                                Else
                                    mdtStop_Date = dRow.Item("stop_date")
                                End If
                                'Sohail (24 Aug 2019) -- Start
                                'NMB Payroll UAT # TC016 - 76.1 - System should be able to accept future amendments (Future period) and prorate. It should be possible to set start date of the specific transaction and prorate based on the start date. It should also be possible to set start date of the future period.
                                If IsDBNull(dRow.Item("edstart_date")) = True Then
                                    mdtEDStart_Date = Nothing
                                Else
                                    mdtEDStart_Date = dRow.Item("edstart_date")
                                End If
                                'Sohail (24 Aug 2019) -- End
                                mintEmpbatchpostingunkid = 0

                                'Sohail (25 Jun 2020) -- Start
                                'NMB Enhancement # : Performance enhancement on Import earning deduction heads.
                                'blnFlag = Insert(strDatabaseName, xUserUnkid, xYearUnkid, xCompanyUnkid, xPeriodStart, xPeriodEnd, xUserModeSetting, xOnlyApproved, xIncludeIn_ActiveEmployee, True, dtCurrentDateAndTime, , objDataOperation, blnApplyUserAccessFilter, strFilerString, False, blnAllowToApproveEarningDeduction)
                                blnFlag = Insert(strDatabaseName, xUserUnkid, xYearUnkid, xCompanyUnkid, xPeriodStart, xPeriodEnd, xUserModeSetting, xOnlyApproved, xIncludeIn_ActiveEmployee, True, dtCurrentDateAndTime, , objDataOperation, blnApplyUserAccessFilter, strFilerString, False, blnAllowToApproveEarningDeduction, blnSkipInsertIfExist)
                                'Sohail (25 Jun 2020) -- End
                            End If

                            If blnFlag = False Then
                                objDataOperation.ReleaseTransaction(False)
                                Return False
                            End If

                            strPrevEndDate = dRow.Item("end_date").ToString
                        Next
                    End If
                End If

                'Sohail (10 Dec 2019) -- Start
                'NMB UAT Enhancement # : Not able to access employee payroll while doing batch entry. 
                If xDataOp Is Nothing Then objDataOperation.ReleaseTransaction(True)
                'Sohail (10 Dec 2019) -- End

                clsClosePeriod._ProgressCurrCount = i + 1
                If bw IsNot Nothing Then
                    bw.ReportProgress(7)
                End If
            Next

            clsClosePeriod._ProgressTotalCount = 0

            'If xDataOp Is Nothing Then objDataOperation.ReleaseTransaction(True) 'Sohail (10 Dec 2019)

            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: InsertAllByEmployeeList; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objTranHead = Nothing
            If xDataOp Is Nothing Then objDataOperation = Nothing
        End Try
    End Function
    'Sohail (15 Dec 2018) -- End

    'Sohail (15 Dec 2018) -- Start
    'HJFMRI Enhancement - Copy Selected Period ED Slab option on Global Assign ED screen in 75.1.
    'Public Function InsertAllByDataTable(ByVal strDatabaseName As String _
    '                                     , ByVal xYearUnkid As Integer _
    '                                     , ByVal xCompanyUnkid As Integer _
    '                                     , ByVal xPeriodStart As DateTime _
    '                                     , ByVal xPeriodEnd As DateTime _
    '                                     , ByVal xUserModeSetting As String _
    '                                     , ByVal xOnlyApproved As Boolean _
    '                                     , ByVal xIncludeIn_ActiveEmployee As Boolean _
    '                                     , ByVal strEmployeeList As String _
    '                                     , ByVal dtTable As DataTable _
    '                                     , ByVal blnOverWrite As Boolean _
    '                                     , ByVal intUserUnkId As Integer _
    '                                     , ByVal blnAllowToApproveEarningDeduction As Boolean _
    '                                     , ByVal dtCurrentDateAndTime As Date _
    '                                     , Optional ByVal blnInsertSalaryHead As Boolean = False _
    '                                     , Optional ByVal blnCopyPreviousEDSlab As Boolean = False _
    '                                     , Optional ByVal blnOverwritePreviousEDSlabHeads As Boolean = False _
    '                                     , Optional ByVal blnApplyUserAccessFilter As Boolean = True _
    '                                     , Optional ByVal strFilerString As String = "" _
    '                                     ) As Boolean 'Sohail (13 Jan 2012), 'Sohail (07 Mar 2012) - [blnCopyPreviousEDSlab], 'Sohail (23 Apr 2012) - [intUserUnkId,strAllowToApproveEarningDeduction]
    '    '                                'Sohail (21 Aug 2015) - [strDatabaseName, xYearUnkid, xCompanyUnkid, xPeriodStart, xPeriodEnd, xUserModeSetting, xOnlyApproved, xIncludeIn_ActiveEmployee, dtCurrentDateAndTime, blnApplyUserAccessFilter, strFilerString]
    '    '                                'Sohail (26 Jul 2012) - [blnOverwritePreviousEDSlabHeads]

    '    Dim objDataOperation As clsDataOperation

    '    Dim dsList As DataSet = Nothing
    '    Dim strQ As String = ""
    '    Dim exForce As Exception
    '    Dim blnFlag As Boolean
    '    Dim intEDID As Integer
    '    Dim intBatchID As Integer
    '    Dim intTranHeadID As Integer
    '    Dim decAmount As Decimal 'Sohail (11 May 2011)
    '    Dim intCurrencyID As Integer
    '    Dim intVendorID As Integer
    '    Dim intPeriodId As Integer 'Sohail (13 Jan 2012)
    '    'Sohail (18 Jan 2012) -- Start
    '    'TRA - ENHANCEMENT
    '    Dim intCostCenterID As Integer
    '    Dim strMedicalRefNo As String
    '    'Sohail (18 Jan 2012) -- End
    '    'Sohail (09 Nov 2013) -- Start
    '    'TRA - ENHANCEMENT
    '    Dim dtCumulative_Startdate As Date
    '    Dim dtStop_date As Date
    '    'Sohail (09 Nov 2013) -- End
    '    Dim objTranHead As New clsTransactionHead
    '    'Dim objExRate As New clsExchangeRate
    '    Dim arrEmp As String()
    '    'Sohail (07 Mar 2012) -- Start
    '    'TRA - ENHANCEMENT
    '    Dim mdtPeriod_startdate As Date
    '    Dim mdtPeriod_enddate As Date
    '    'Sohail (07 Mar 2012) -- End
    '    Dim intEmpbatchpostingunkid As Integer 'Sohail (24 Feb 2016)

    '    objDataOperation = New clsDataOperation
    '    objDataOperation.BindTransaction()

    '    Try
    '        'Sohail (11 Sep 2010) -- Start
    '        arrEmp = strEmployeeList.Split(",")

    '        For i As Integer = 0 To arrEmp.Length - 1
    '            mintEmployeeunkid = CInt(arrEmp(i))
    '            'Sohail (11 Sep 2010) -- End

    '            For Each dtRow As DataRow In dtTable.Rows

    '                objDataOperation.ClearParameters()

    '                'mintEmployeeunkid = intEmployeeUnkID
    '                intTranHeadID = CInt(dtRow.Item("tranheadunkid").ToString)
    '                intBatchID = CInt(dtRow.Item("batchtransactionunkid").ToString)
    '                intCurrencyID = CInt(dtRow.Item("currencyid").ToString)
    '                'objExRate._ExchangeRateunkid = intCurrencyID
    '                'dblAmount = cdec(Format(dtRow.Item("Amount"), objExRate._fmtCurrency))
    '                decAmount = CDec(dtRow.Item("Amount").ToString)
    '                intVendorID = CInt(dtRow.Item("vendorid").ToString)
    '                intPeriodId = CInt(dtRow.Item("periodunkid").ToString) 'Sohail (13 Jan 2012)
    '                'Sohail (18 Jan 2012) -- Start
    '                'TRA - ENHANCEMENT
    '                intCostCenterID = CInt(dtRow.Item("costcenterunkid").ToString)
    '                strMedicalRefNo = dtRow.Item("medicalrefno").ToString
    '                'Sohail (18 Jan 2012) -- End
    '                'Sohail (07 Mar 2012) -- Start
    '                'TRA - ENHANCEMENT
    '                mdtPeriod_startdate = eZeeDate.convertDate(dtRow.Item("start_date").ToString)
    '                mdtPeriod_enddate = eZeeDate.convertDate(dtRow.Item("end_date").ToString)
    '                'Sohail (07 Mar 2012) -- End
    '                'Sohail (09 Nov 2013) -- Start
    '                'TRA - ENHANCEMENT
    '                If IsDBNull(dtRow.Item("cumulative_startdate")) = True Then
    '                    dtCumulative_Startdate = Nothing
    '                Else
    '                    dtCumulative_Startdate = dtRow.Item("cumulative_startdate")
    '                End If
    '                If IsDBNull(dtRow.Item("stop_date")) = True Then
    '                    dtStop_date = Nothing
    '                Else
    '                    dtStop_date = dtRow.Item("stop_date")
    '                End If
    '                'Sohail (09 Nov 2013) -- End
    '                intEmpbatchpostingunkid = CInt(dtRow.Item("empbatchpostingunkid").ToString) 'Sohail (24 Feb 2016)

    '                'Sohail (13 Jan 2012) -- Start
    '                'TRA - ENHANCEMENT
    '                'intEDID = GetEDUnkID(mintEmployeeunkid, intTranHeadID)
    '                If blnInsertSalaryHead = True Then
    '                    'Sohail (21 Aug 2015) -- Start
    '                    'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
    '                    'blnFlag = Insert_SalaryHead(mintEmployeeunkid, intPeriodId)
    '                    blnFlag = Insert_SalaryHead(strDatabaseName, intUserUnkId, xYearUnkid, xCompanyUnkid, xPeriodStart, xPeriodEnd, xUserModeSetting, xOnlyApproved, xIncludeIn_ActiveEmployee, mintEmployeeunkid, intPeriodId, dtCurrentDateAndTime, objDataOperation, False, strFilerString)
    '                    'Sohail (21 Aug 2015) -- End

    '                    If blnFlag = False Then
    '                        objDataOperation.ReleaseTransaction(False)
    '                        Return False
    '                    End If
    '                End If

    '                'Sohail (21 Aug 2015) -- Start
    '                'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
    '                'intEDID = GetEDUnkID(mintEmployeeunkid, intTranHeadID, intPeriodId)
    '                intEDID = GetEDUnkID(strDatabaseName, mintEmployeeunkid, intTranHeadID, intPeriodId, objDataOperation)
    '                'Sohail (21 Aug 2015) -- End
    '                'Sohail (13 Jan 2012) -- End

    '                If intEDID > 0 Then
    '                    'Sohail (21 Aug 2015) -- Start
    '                    'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
    '                    'Me._Edunkid = intEDID
    '                    Me._Edunkid(objDataOperation, strDatabaseName) = intEDID
    '                    'Sohail (21 Aug 2015) -- End

    '                    If blnOverWrite = True Then
    '                        mdecAmount = decAmount
    '                        mintCurrencyunkid = intCurrencyID
    '                        mintVendorunkid = intVendorID
    '                        mintMembership_Categoryunkid = 0
    '                        'Sohail (18 Jan 2012) -- Start
    '                        'TRA - ENHANCEMENT
    '                        mintCostCenterUnkId = intCostCenterID
    '                        mstrMedicalrefno = strMedicalRefNo
    '                        'Sohail (18 Jan 2012) -- End
    '                        'Sohail (30 Mar 2013) -- Start
    '                        'TRA - ENHANCEMENT
    '                        mintMembershipTranUnkid = CInt(dtRow.Item("membershiptranunkid"))
    '                        mintDisciplinefileunkid = CInt(dtRow.Item("disciplinefileunkid"))
    '                        'Sohail (30 Mar 2013) -- End
    '                        'Sohail (09 Nov 2013) -- Start
    '                        'TRA - ENHANCEMENT
    '                        mdtCumulative_Startdate = dtCumulative_Startdate
    '                        mdtStop_Date = dtStop_date
    '                        'Sohail (09 Nov 2013) -- End
    '                        mintEmpbatchpostingunkid = intEmpbatchpostingunkid 'Sohail (24 Feb 2016)
    '                    End If

    '                    If blnOverWrite = True Then
    '                        'Sohail (23 Apr 2012) -- Start
    '                        'TRA - ENHANCEMENT
    '                        'mintUserunkid = User._Object._Userunkid
    '                        mintUserunkid = IIf(intUserUnkId = 0, User._Object._Userunkid, intUserUnkId)
    '                        'Sohail (23 Apr 2012) -- End
    '                        'Sohail (11 Nov 2010) -- Start
    '                        'blnFlag = Update()
    '                        'Sohail (21 Aug 2015) -- Start
    '                        'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
    '                        'blnFlag = Update(True)
    '                        blnFlag = Update(strDatabaseName, True, objDataOperation, dtCurrentDateAndTime)
    '                        'Sohail (21 Aug 2015) -- End
    '                        'Sohail (11 Nov 2010) -- End
    '                    Else
    '                        blnFlag = True
    '                    End If
    '                Else
    '                    mintTranheadunkid = intTranHeadID
    '                    'Sohail (21 Aug 2015) -- Start
    '                    'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
    '                    'objTranHead._Tranheadunkid = intTranHeadID
    '                    objTranHead._Tranheadunkid(strDatabaseName) = intTranHeadID
    '                    'Sohail (21 Aug 2015) -- End
    '                    mintBatchtransactionunkid = intBatchID

    '                    mintTrnheadtype_Id = objTranHead._Trnheadtype_Id
    '                    mintTypeof_Id = objTranHead._Typeof_id
    '                    mintCalctype_Id = objTranHead._Calctype_Id
    '                    mintComputeon_Id = objTranHead._Computeon_Id
    '                    mstrFormula = objTranHead._Formula
    '                    mstrFormulaid = objTranHead._Formulaid
    '                    mdecAmount = decAmount

    '                    If objTranHead._Trnheadtype_Id = 1 Then 'enTranHeadType.EarningForEmployees
    '                        mblnIsdeduct = False
    '                    ElseIf objTranHead._Trnheadtype_Id = 4 Or objTranHead._Trnheadtype_Id = 5 Then 'enTranHeadType.EmployersStatutoryContributions, Informational
    '                        mblnIsdeduct = Nothing
    '                    Else
    '                        mblnIsdeduct = True
    '                    End If

    '                    mintCurrencyunkid = intCurrencyID
    '                    mintVendorunkid = intVendorID
    '                    'Sohail (23 Apr 2012) -- Start
    '                    'TRA - ENHANCEMENT
    '                    'mintUserunkid = User._Object._Userunkid 'Sohail (14 Oct 2010)
    '                    mintUserunkid = IIf(intUserUnkId = 0, User._Object._Userunkid, intUserUnkId)
    '                    'Sohail (23 Apr 2012) -- End
    '                    mblnIsvoid = False
    '                    mintVoiduserunkid = -1
    '                    mdtVoiddatetime = Nothing
    '                    mstrVoidreason = ""
    '                    mintMembership_Categoryunkid = 0
    '                    'Sohail (14 Jun 2011) -- Start
    '                    'Sohail (05 Jul 2011) -- Start

    '                    'Sohail (23 Apr 2012) -- Start
    '                    'TRA - ENHANCEMENT
    '                    'Sohail (21 Aug 2015) -- Start
    '                    'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
    '                    'If strAllowToApproveEarningDeduction = "" Then strAllowToApproveEarningDeduction = User._Object.Privilege._AllowToApproveEarningDeduction.ToString()
    '                    'If CBool(strAllowToApproveEarningDeduction) = True Then
    '                    If blnAllowToApproveEarningDeduction = True Then
    '                        'Sohail (21 Aug 2015) -- End
    '                        'If User._Object.Privilege._AllowToApproveEarningDeduction = True Then
    '                        'Sohail (23 Apr 2012) -- End
    '                        mblnIsapproved = True
    '                        'Sohail (23 Apr 2012) -- Start
    '                        'TRA - ENHANCEMENT
    '                        'mintApproveruserunkid = User._Object._Userunkid
    '                        mintApproveruserunkid = IIf(intUserUnkId = 0, User._Object._Userunkid, intUserUnkId)
    '                        'Sohail (23 Apr 2012) -- End
    '                    Else
    '                        mblnIsapproved = False
    '                        mintApproveruserunkid = -1
    '                    End If
    '                    'Sohail (05 Jul 2011) -- End
    '                    'Sohail (14 Jun 2011) -- End
    '                    mintPeriodunkid = intPeriodId 'Sohail (13 Jan 2012)
    '                    'Sohail (18 Jan 2012) -- Start
    '                    'TRA - ENHANCEMENT
    '                    mintCostCenterUnkId = intCostCenterID
    '                    mstrMedicalrefno = strMedicalRefNo
    '                    'Sohail (18 Jan 2012) -- End

    '                    'S.SANDEEP [ 17 DEC 2012 ] -- START
    '                    'ENHANCEMENT : TRA CHANGES
    '                    mintMembershipTranUnkid = CInt(dtRow.Item("membershiptranunkid"))
    '                    mintDisciplinefileunkid = CInt(dtRow.Item("disciplinefileunkid"))
    '                    'S.SANDEEP [ 17 DEC 2012 ] -- END
    '                    'Sohail (09 Nov 2013) -- Start
    '                    'TRA - ENHANCEMENT
    '                    mdtCumulative_Startdate = dtCumulative_Startdate
    '                    mdtStop_Date = dtStop_date
    '                    'Sohail (09 Nov 2013) -- End
    '                    mintEmpbatchpostingunkid = intEmpbatchpostingunkid 'Sohail (24 Feb 2016)

    '                    'Sohail (11 Nov 2010) -- Start
    '                    'blnFlag = Insert()
    '                    'Sohail (21 Aug 2015) -- Start
    '                    'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
    '                    'blnFlag = Insert(True)
    '                    'Sohail (20 Jul 2018) -- Start
    '                    'Support Issue # 0002383 : VOLTAMP: Unable to assign Transaction heads using batch entry in Payroll transactions (Warning message of Membership heads not assigned)in 73.1.
    '                    'blnFlag = Insert(strDatabaseName, intUserUnkId, xYearUnkid, xCompanyUnkid, xPeriodStart, xPeriodEnd, xUserModeSetting, xOnlyApproved, xIncludeIn_ActiveEmployee, True, dtCurrentDateAndTime, , objDataOperation, blnApplyUserAccessFilter, strFilerString)
    '                    blnFlag = Insert(strDatabaseName, intUserUnkId, xYearUnkid, xCompanyUnkid, xPeriodStart, xPeriodEnd, xUserModeSetting, xOnlyApproved, xIncludeIn_ActiveEmployee, True, dtCurrentDateAndTime, , objDataOperation, blnApplyUserAccessFilter, strFilerString, True, blnAllowToApproveEarningDeduction)
    '                    'Sohail (20 Jul 2018) -- End
    '                    'Sohail (21 Aug 2015) -- End
    '                    'Sohail (11 Nov 2010) -- End
    '                End If

    '                If blnFlag = False Then
    '                    objDataOperation.ReleaseTransaction(False)
    '                    Return False
    '                End If

    '            Next

    '            'Sohail (07 Mar 2012) -- Start
    '            'TRA - ENHANCEMENT
    '            '**** Copy Previous ED Slab
    '            If blnCopyPreviousEDSlab = True Then
    '                'Sohail (21 Aug 2015) -- Start
    '                'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
    '                'Call GetEmployeeED()
    '                'Sohail (01 Mar 2018) -- Start
    '                'Internal Issue : ExecureReader requires the command to have a transaction... in 70.1.
    '                'Call GetEmployeeED(strDatabaseName)
    '                Call GetEmployeeED(objDataOperation, strDatabaseName)
    '                'Sohail (01 Mar 2018) -- End
    '                'Sohail (21 Aug 2015) -- End

    '                Dim strPrevEndDate As String = ""
    '                Dim drRow As DataRow()

    '                'Sohail (09 Nov 2013) -- Start
    '                'TRA - ENHANCEMENT
    '                'drRow = mdtED.Select("AUD = '' AND end_date <= '" & eZeeDate.convertDate(mdtPeriod_startdate) & "' AND isrecurrent = 1 ", "end_date DESC")
    '                drRow = mdtED.Select("AUD = '' AND end_date <= '" & eZeeDate.convertDate(mdtPeriod_startdate) & "' AND isrecurrent = 1 AND (stop_date IS NULL OR stop_date >= '" & mdtPeriod_startdate & "') ", "end_date DESC")
    '                'Sohail (09 Nov 2013) -- End
    '                If drRow.Length > 0 Then
    '                    For Each dRow As DataRow In drRow
    '                        If strPrevEndDate <> "" AndAlso strPrevEndDate <> dRow.Item("end_date").ToString Then
    '                            Exit For
    '                        End If

    '                        'Sohail (21 Aug 2015) -- Start
    '                        'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
    '                        'objTranHead._Tranheadunkid = CInt(dRow.Item("tranheadunkid"))
    '                        objTranHead._Tranheadunkid(strDatabaseName) = CInt(dRow.Item("tranheadunkid"))
    '                        'Sohail (21 Aug 2015) -- End
    '                        'Sohail (25 May 2012) -- Start
    '                        'TRA - ENHANCEMENT

    '                        'Sohail (24 Feb 2016) -- Start
    '                        'Enhancement - Batch Posting changes as per online doc in 58.1 SP. (#15 (KBC & Kenya Project Comments List.xls))
    '                        'If objTranHead._Typeof_id = enTypeOf.Salary OrElse CInt(dRow.Item("tranheadunkid")) = intTranHeadID Then
    '                        If objTranHead._Typeof_id = enTypeOf.Salary OrElse dtTable.Select("tranheadunkid = " & CInt(dRow.Item("tranheadunkid")) & " ").Length > 0 Then
    '                            'Sohail (24 Feb 2016) -- End
    '                            'If objTranHead._Typeof_id = enTypeOf.Salary Then
    '                            'Sohail (25 May 2012) -- End
    '                            strPrevEndDate = dRow.Item("end_date").ToString
    '                            Continue For
    '                        End If

    '                        'Sohail (21 Aug 2015) -- Start
    '                        'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
    '                        'intEDID = GetEDUnkID(mintEmployeeunkid, CInt(dRow.Item("tranheadunkid")), intPeriodId)
    '                        intEDID = GetEDUnkID(strDatabaseName, mintEmployeeunkid, CInt(dRow.Item("tranheadunkid")), intPeriodId, objDataOperation)
    '                        'Sohail (21 Aug 2015) -- End

    '                        If intEDID > 0 Then
    '                            'Sohail (21 Aug 2015) -- Start
    '                            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
    '                            'Me._Edunkid = intEDID 'Sohail (25 May 2012)
    '                            Me._Edunkid(objDataOperation, strDatabaseName) = intEDID
    '                            'Sohail (21 Aug 2015) -- End
    '                            'Sohail (26 Jul 2012) -- Start
    '                            'TRA - ENHANCEMENT
    '                            'If blnOverWrite = True Then
    '                            If blnOverwritePreviousEDSlabHeads = True Then
    '                                'Sohail (26 Jul 2012) -- End
    '                                mdecAmount = CDec(dRow.Item("amount"))
    '                                mintCurrencyunkid = CInt(dRow.Item("currencyid"))
    '                                mintVendorunkid = CInt(dRow.Item("vendorid"))
    '                                If IsDBNull(dRow.Item("membership_categoryunkid")) Then
    '                                    mintMembership_Categoryunkid = 0
    '                                Else
    '                                    mintMembership_Categoryunkid = CInt(dRow.Item("membership_categoryunkid"))
    '                                End If
    '                                mintCostCenterUnkId = Int(dRow.Item("costcenterunkid"))
    '                                mstrMedicalrefno = dRow.Item("medicalrefno").ToString
    '                                'Sohail (23 Apr 2012) -- Start
    '                                'TRA - ENHANCEMENT
    '                                'mintUserunkid = User._Object._Userunkid
    '                                mintUserunkid = IIf(intUserUnkId = 0, User._Object._Userunkid, intUserUnkId)
    '                                'Sohail (23 Apr 2012) -- End
    '                                'Sohail (20 Feb 2013) -- Start
    '                                'TRA - ENHANCEMENT
    '                                mintMembershipTranUnkid = CInt(dRow.Item("membershiptranunkid"))
    '                                mintDisciplinefileunkid = CInt(dRow.Item("disciplinefileunkid"))
    '                                'Sohail (20 Feb 2013) -- End
    '                                'Sohail (09 Nov 2013) -- Start
    '                                'TRA - ENHANCEMENT
    '                                If IsDBNull(dRow.Item("cumulative_startdate")) = True Then
    '                                    mdtCumulative_Startdate = Nothing
    '                                Else
    '                                    mdtCumulative_Startdate = dRow.Item("cumulative_startdate")
    '                                End If
    '                                If IsDBNull(dRow.Item("stop_date")) = True Then
    '                                    mdtStop_Date = Nothing
    '                                Else
    '                                    mdtStop_Date = dRow.Item("stop_date")
    '                                End If
    '                                'Sohail (09 Nov 2013) -- End
    '                                mintEmpbatchpostingunkid = 0 'Sohail (24 Feb 2016)

    '                                'Sohail (21 Aug 2015) -- Start
    '                                'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
    '                                'blnFlag = Update(True)
    '                                blnFlag = Update(strDatabaseName, True, objDataOperation, dtCurrentDateAndTime)
    '                                'Sohail (21 Aug 2015) -- End
    '                            Else
    '                                blnFlag = True
    '                            End If
    '                        Else
    '                            mintTranheadunkid = CInt(dRow.Item("tranheadunkid"))
    '                            'Sohail (21 Aug 2015) -- Start
    '                            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
    '                            'objTranHead._Tranheadunkid = CInt(dRow.Item("tranheadunkid"))
    '                            objTranHead._Tranheadunkid(strDatabaseName) = CInt(dRow.Item("tranheadunkid"))
    '                            'Sohail (21 Aug 2015) -- End
    '                            mintBatchtransactionunkid = CInt(dRow.Item("batchtransactionunkid"))

    '                            mintTrnheadtype_Id = objTranHead._Trnheadtype_Id
    '                            mintTypeof_Id = objTranHead._Typeof_id
    '                            mintCalctype_Id = objTranHead._Calctype_Id
    '                            mintComputeon_Id = objTranHead._Computeon_Id
    '                            mstrFormula = objTranHead._Formula
    '                            mstrFormulaid = objTranHead._Formulaid
    '                            mdecAmount = CDec(dRow.Item("amount"))

    '                            If objTranHead._Trnheadtype_Id = enTranHeadType.EarningForEmployees Then
    '                                mblnIsdeduct = False
    '                            ElseIf objTranHead._Trnheadtype_Id = enTranHeadType.EmployersStatutoryContributions OrElse objTranHead._Trnheadtype_Id = enTranHeadType.Informational Then
    '                                mblnIsdeduct = Nothing
    '                            Else
    '                                mblnIsdeduct = True
    '                            End If

    '                            mintCurrencyunkid = CInt(dRow.Item("currencyid"))
    '                            mintVendorunkid = CInt(dRow.Item("vendorid"))
    '                            'Sohail (23 Apr 2012) -- Start
    '                            'TRA - ENHANCEMENT
    '                            'mintUserunkid = User._Object._Userunkid
    '                            mintUserunkid = IIf(intUserUnkId = 0, User._Object._Userunkid, intUserUnkId)
    '                            'Sohail (23 Apr 2012) -- End
    '                            mblnIsvoid = False
    '                            mintVoiduserunkid = -1
    '                            mdtVoiddatetime = Nothing
    '                            mstrVoidreason = ""
    '                            mintMembership_Categoryunkid = 0
    '                            'Sohail (23 Apr 2012) -- Start
    '                            'TRA - ENHANCEMENT
    '                            'Sohail (21 Aug 2015) -- Start
    '                            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
    '                            'If strAllowToApproveEarningDeduction = "" Then strAllowToApproveEarningDeduction = User._Object.Privilege._AllowToApproveEarningDeduction.ToString()
    '                            'If CBool(strAllowToApproveEarningDeduction) = True Then
    '                            If blnAllowToApproveEarningDeduction = True Then
    '                                'Sohail (21 Aug 2015) -- End
    '                                'If User._Object.Privilege._AllowToApproveEarningDeduction = True Then
    '                                'Sohail (23 Apr 2012) -- End
    '                                mblnIsapproved = True
    '                                'Sohail (23 Apr 2012) -- Start
    '                                'TRA - ENHANCEMENT
    '                                'mintApproveruserunkid = User._Object._Userunkid
    '                                mintApproveruserunkid = IIf(intUserUnkId = 0, User._Object._Userunkid, intUserUnkId)
    '                                'Sohail (23 Apr 2012) -- End
    '                            Else
    '                                mblnIsapproved = False
    '                                mintApproveruserunkid = -1
    '                            End If
    '                            mintPeriodunkid = intPeriodId
    '                            mintCostCenterUnkId = 0
    '                            mstrMedicalrefno = ""

    '                            'S.SANDEEP [ 17 DEC 2012 ] -- START
    '                            'ENHANCEMENT : TRA CHANGES
    '                            mintMembershipTranUnkid = CInt(dRow.Item("membershiptranunkid"))
    '                            mintDisciplinefileunkid = CInt(dRow.Item("disciplinefileunkid"))
    '                            'S.SANDEEP [ 17 DEC 2012 ] -- END
    '                            'Sohail (09 Nov 2013) -- Start
    '                            'TRA - ENHANCEMENT
    '                            If IsDBNull(dRow.Item("cumulative_startdate")) = True Then
    '                                mdtCumulative_Startdate = Nothing
    '                            Else
    '                                mdtCumulative_Startdate = dRow.Item("cumulative_startdate")
    '                            End If
    '                            If IsDBNull(dRow.Item("stop_date")) = True Then
    '                                mdtStop_Date = Nothing
    '                            Else
    '                                mdtStop_Date = dRow.Item("stop_date")
    '                            End If
    '                            'Sohail (09 Nov 2013) -- End
    '                            mintEmpbatchpostingunkid = 0 'Sohail (24 Feb 2016)

    '                            'Sohail (21 Aug 2015) -- Start
    '                            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
    '                            'blnFlag = Insert(True)
    '                            'Sohail (20 Jul 2018) -- Start
    '                            'Support Issue # 0002383 : VOLTAMP: Unable to assign Transaction heads using batch entry in Payroll transactions (Warning message of Membership heads not assigned)in 73.1.
    '                            'blnFlag = Insert(strDatabaseName, intUserUnkId, xYearUnkid, xCompanyUnkid, xPeriodStart, xPeriodEnd, xUserModeSetting, xOnlyApproved, xIncludeIn_ActiveEmployee, True, dtCurrentDateAndTime, , objDataOperation, blnApplyUserAccessFilter, strFilerString)
    '                            blnFlag = Insert(strDatabaseName, intUserUnkId, xYearUnkid, xCompanyUnkid, xPeriodStart, xPeriodEnd, xUserModeSetting, xOnlyApproved, xIncludeIn_ActiveEmployee, True, dtCurrentDateAndTime, , objDataOperation, blnApplyUserAccessFilter, strFilerString, False, blnAllowToApproveEarningDeduction)
    '                            'Sohail (20 Jul 2018) -- End
    '                            'Sohail (21 Aug 2015) -- End
    '                        End If

    '                        If blnFlag = False Then
    '                            objDataOperation.ReleaseTransaction(False)
    '                            Return False
    '                        End If

    '                        strPrevEndDate = dRow.Item("end_date").ToString
    '                    Next
    '                End If
    '            End If
    '            'Sohail (07 Mar 2012) -- End

    '        Next
    '        objDataOperation.ReleaseTransaction(True)

    '        Return True
    '    Catch ex As Exception
    '        objDataOperation.ReleaseTransaction(False)
    '        Throw New Exception(ex.Message & "; Procedure Name: InsertAllByDataTable; Module Name: " & mstrModuleName)
    '        Return False
    '    Finally
    '        exForce = Nothing
    '        If dsList IsNot Nothing Then dsList.Dispose()
    '        objTranHead = Nothing
    '        'objExRate = Nothing
    '        objDataOperation = Nothing
    '    End Try
    'End Function
    'Sohail (23 Apr 2012) -- Start
    'TRA - ENHANCEMENT - Overload funcion created as  Issue of bind transaction from employee master from aruti self service using new web commonlib.
    'Public Function InsertAllByDataTable(ByVal strDatabaseName As String _
    '                                     , ByVal xYearUnkid As Integer _
    '                                     , ByVal xCompanyUnkid As Integer _
    '                                     , ByVal xPeriodStart As DateTime _
    '                                     , ByVal xPeriodEnd As DateTime _
    '                                     , ByVal xUserModeSetting As String _
    '                                     , ByVal xOnlyApproved As Boolean _
    '                                     , ByVal xIncludeIn_ActiveEmployee As Boolean _
    '                                     , ByVal objDataOperation As clsDataOperation _
    '                                     , ByVal strEmployeeList As String _
    '                                     , ByVal dtTable As DataTable _
    '                                     , ByVal blnOverWrite As Boolean _
    '                                     , ByVal intUserUnkId As Integer _
    '                                     , ByVal blnAllowToApproveEarningDeduction As Boolean _
    '                                     , ByVal dtCurrentDateAndTime As Date _
    '                                     , Optional ByVal blnInsertSalaryHead As Boolean = False _
    '                                     , Optional ByVal blnCopyPreviousEDSlab As Boolean = False _
    '                                     , Optional ByVal blnOverwritePreviousEDSlabHeads As Boolean = False _
    '                                     , Optional ByVal blnApplyUserAccessFilter As Boolean = True _
    '                                     , Optional ByVal strFilerString As String = "" _
    '                                     , Optional ByVal blnCheckMembershipHeads As Boolean = True _
    '                                     ) As Boolean
    '    '                                'Sohail (01 Dec 2016) - [blnCheckMembershipHeads]
    '    '                                'Sohail (21 Aug 2015) - [strDatabaseName, xYearUnkid, xCompanyUnkid, xPeriodStart, xPeriodEnd, xUserModeSetting, xOnlyApproved, xIncludeIn_ActiveEmployee, blnAllowToApproveEarningDeduction, blnApplyUserAccessFilter, strFilerString]
    '    '                                'Sohail (26 Jul 2012) - [blnOverwritePreviousEDSlabHeads]

    '    Dim dsList As DataSet = Nothing
    '    Dim strQ As String = ""
    '    Dim exForce As Exception
    '    Dim blnFlag As Boolean
    '    Dim intEDID As Integer
    '    Dim intBatchID As Integer
    '    Dim intTranHeadID As Integer
    '    Dim decAmount As Decimal
    '    Dim intCurrencyID As Integer
    '    Dim intVendorID As Integer
    '    Dim intPeriodId As Integer
    '    Dim intCostCenterID As Integer
    '    Dim strMedicalRefNo As String
    '    'Sohail (09 Nov 2013) -- Start
    '    'TRA - ENHANCEMENT
    '    Dim dtCumulative_Startdate As Date
    '    Dim dtStop_date As Date
    '    'Sohail (09 Nov 2013) -- End
    '    Dim objTranHead As New clsTransactionHead
    '    Dim arrEmp As String()
    '    Dim mdtPeriod_startdate As Date
    '    Dim mdtPeriod_enddate As Date
    '    Dim intEmpbatchpostingunkid As Integer 'Sohail (24 Feb 2016)

    '    Try
    '        arrEmp = strEmployeeList.Split(",")

    '        For i As Integer = 0 To arrEmp.Length - 1
    '            mintEmployeeunkid = CInt(arrEmp(i))

    '            For Each dtRow As DataRow In dtTable.Rows

    '                objDataOperation.ClearParameters()

    '                intTranHeadID = CInt(dtRow.Item("tranheadunkid").ToString)
    '                intBatchID = CInt(dtRow.Item("batchtransactionunkid").ToString)
    '                intCurrencyID = CInt(dtRow.Item("currencyid").ToString)
    '                decAmount = CDec(dtRow.Item("Amount").ToString)
    '                intVendorID = CInt(dtRow.Item("vendorid").ToString)
    '                intPeriodId = CInt(dtRow.Item("periodunkid").ToString)
    '                intCostCenterID = CInt(dtRow.Item("costcenterunkid").ToString)
    '                strMedicalRefNo = dtRow.Item("medicalrefno").ToString
    '                mdtPeriod_startdate = eZeeDate.convertDate(dtRow.Item("start_date").ToString)
    '                mdtPeriod_enddate = eZeeDate.convertDate(dtRow.Item("end_date").ToString)
    '                'Sohail (09 Nov 2013) -- Start
    '                'TRA - ENHANCEMENT
    '                If IsDBNull(dtRow.Item("cumulative_startdate")) = True Then
    '                    dtCumulative_Startdate = Nothing
    '                Else
    '                    dtCumulative_Startdate = dtRow.Item("cumulative_startdate")
    '                End If
    '                If IsDBNull(dtRow.Item("stop_date")) = True Then
    '                    dtStop_date = Nothing
    '                Else
    '                    dtStop_date = dtRow.Item("stop_date")
    '                End If
    '                'Sohail (09 Nov 2013) -- End
    '                intEmpbatchpostingunkid = CInt(dtRow.Item("empbatchpostingunkid").ToString) 'Sohail (24 Feb 2016)

    '                If blnInsertSalaryHead = True Then
    '                    'Sohail (21 Aug 2015) -- Start
    '                    'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
    '                    'blnFlag = Insert_SalaryHead(mintEmployeeunkid, intPeriodId, objDataOperation)
    '                    'Sohail (01 Dec 2016) -- Start
    '                    'Enhancement - 64.1 - Insert Membership heads on Ed if not assigned.
    '                    'blnFlag = Insert_SalaryHead(strDatabaseName, intUserUnkId, xYearUnkid, xCompanyUnkid, xPeriodStart, xPeriodEnd, xUserModeSetting, xOnlyApproved, xIncludeIn_ActiveEmployee, mintEmployeeunkid, intPeriodId, dtCurrentDateAndTime, objDataOperation, False, strFilerString)
    '                    blnFlag = Insert_SalaryHead(strDatabaseName, intUserUnkId, xYearUnkid, xCompanyUnkid, mdtPeriod_startdate, mdtPeriod_enddate, xUserModeSetting, xOnlyApproved, xIncludeIn_ActiveEmployee, mintEmployeeunkid, intPeriodId, dtCurrentDateAndTime, objDataOperation, False, strFilerString)
    '                    'Sohail (01 Dec 2016) -- End
    '                    'Sohail (21 Aug 2015) -- End

    '                    If blnFlag = False Then
    '                        objDataOperation.ReleaseTransaction(False)
    '                        Return False
    '                    End If
    '                End If

    '                'Sohail (21 Aug 2015) -- Start
    '                'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
    '                'intEDID = GetEDUnkID(mintEmployeeunkid, intTranHeadID, intPeriodId, objDataOperation)
    '                intEDID = GetEDUnkID(strDatabaseName, mintEmployeeunkid, intTranHeadID, intPeriodId, objDataOperation)
    '                'Sohail (21 Aug 2015) -- End

    '                If intEDID > 0 Then
    '                    'Sohail (21 Aug 2015) -- Start
    '                    'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
    '                    'Me._Edunkid = intEDID
    '                    Me._Edunkid(objDataOperation, strDatabaseName) = intEDID
    '                    'Sohail (21 Aug 2015) -- End

    '                    If blnOverWrite = True Then
    '                        mdecAmount = decAmount
    '                        mintCurrencyunkid = intCurrencyID
    '                        mintVendorunkid = intVendorID
    '                        mintMembership_Categoryunkid = 0
    '                        mintCostCenterUnkId = intCostCenterID
    '                        mstrMedicalrefno = strMedicalRefNo
    '                        'Sohail (30 Mar 2013) -- Start
    '                        'TRA - ENHANCEMENT
    '                        mintMembershipTranUnkid = CInt(dtRow.Item("membershiptranunkid"))
    '                        mintDisciplinefileunkid = CInt(dtRow.Item("disciplinefileunkid"))
    '                        'Sohail (30 Mar 2013) -- End
    '                        'Sohail (09 Nov 2013) -- Start
    '                        'TRA - ENHANCEMENT
    '                        mdtCumulative_Startdate = dtCumulative_Startdate
    '                        mdtStop_Date = dtStop_date
    '                        'Sohail (09 Nov 2013) -- End
    '                        mintEmpbatchpostingunkid = intEmpbatchpostingunkid 'Sohail (24 Feb 2016)
    '                    End If

    '                    If blnOverWrite = True Then
    '                        mintUserunkid = IIf(intUserUnkId = 0, User._Object._Userunkid, intUserUnkId)
    '                        'Sohail (21 Aug 2015) -- Start
    '                        'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
    '                        'blnFlag = Update(True, objDataOperation)
    '                        blnFlag = Update(strDatabaseName, True, objDataOperation, dtCurrentDateAndTime)
    '                        'Sohail (21 Aug 2015) -- End
    '                    Else
    '                        blnFlag = True
    '                    End If
    '                Else
    '                    mintTranheadunkid = intTranHeadID
    '                    'Sohail (21 Aug 2015) -- Start
    '                    'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
    '                    'objTranHead._Tranheadunkid = intTranHeadID
    '                    objTranHead._Tranheadunkid(strDatabaseName) = intTranHeadID
    '                    'Sohail (21 Aug 2015) -- End
    '                    mintBatchtransactionunkid = intBatchID

    '                    mintTrnheadtype_Id = objTranHead._Trnheadtype_Id
    '                    mintTypeof_Id = objTranHead._Typeof_id
    '                    mintCalctype_Id = objTranHead._Calctype_Id
    '                    mintComputeon_Id = objTranHead._Computeon_Id
    '                    mstrFormula = objTranHead._Formula
    '                    mstrFormulaid = objTranHead._Formulaid
    '                    mdecAmount = decAmount

    '                    If objTranHead._Trnheadtype_Id = 1 Then 'enTranHeadType.EarningForEmployees
    '                        mblnIsdeduct = False
    '                    ElseIf objTranHead._Trnheadtype_Id = 4 Or objTranHead._Trnheadtype_Id = 5 Then 'enTranHeadType.EmployersStatutoryContributions, Informational
    '                        mblnIsdeduct = Nothing
    '                    Else
    '                        mblnIsdeduct = True
    '                    End If

    '                    mintCurrencyunkid = intCurrencyID
    '                    mintVendorunkid = intVendorID
    '                    mintUserunkid = IIf(intUserUnkId = 0, User._Object._Userunkid, intUserUnkId)
    '                    mblnIsvoid = False
    '                    mintVoiduserunkid = -1
    '                    mdtVoiddatetime = Nothing
    '                    mstrVoidreason = ""
    '                    mintMembership_Categoryunkid = 0

    '                    'Sohail (21 Aug 2015) -- Start
    '                    'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
    '                    'If strAllowToApproveEarningDeduction = "" Then strAllowToApproveEarningDeduction = User._Object.Privilege._AllowToApproveEarningDeduction.ToString()
    '                    'If CBool(strAllowToApproveEarningDeduction) = True Then
    '                    If blnAllowToApproveEarningDeduction = True Then
    '                        'Sohail (21 Aug 2015) -- End
    '                        mblnIsapproved = True
    '                        'Sohail (21 Aug 2015) -- Start
    '                        'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
    '                        'mintApproveruserunkid = User._Object._Userunkid
    '                        mintApproveruserunkid = intUserUnkId
    '                        'Sohail (21 Aug 2015) -- End
    '                    Else
    '                        mblnIsapproved = False
    '                        mintApproveruserunkid = -1
    '                    End If
    '                    mintPeriodunkid = intPeriodId
    '                    mintCostCenterUnkId = intCostCenterID
    '                    mstrMedicalrefno = strMedicalRefNo
    '                    'S.SANDEEP [ 11 DEC 2012 ] -- START
    '                    'ENHANCEMENT : TRA CHANGES
    '                    mintMembershipTranUnkid = CInt(dtRow.Item("membershiptranunkid"))
    '                    'S.SANDEEP [ 11 DEC 2012 ] -- END
    '                    'S.SANDEEP [ 17 DEC 2012 ] -- START
    '                    'ENHANCEMENT : TRA CHANGES
    '                    mintDisciplinefileunkid = CInt(dtRow.Item("disciplinefileunkid"))
    '                    'S.SANDEEP [ 17 DEC 2012 ] -- END
    '                    'Sohail (09 Nov 2013) -- Start
    '                    'TRA - ENHANCEMENT
    '                    mdtCumulative_Startdate = dtCumulative_Startdate
    '                    mdtStop_Date = dtStop_date
    '                    'Sohail (09 Nov 2013) -- End
    '                    mintEmpbatchpostingunkid = intEmpbatchpostingunkid 'Sohail (24 Feb 2016)

    '                    'Sohail (21 Aug 2015) -- Start
    '                    'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
    '                    'blnFlag = Insert(True, , objDataOperation)
    '                    'Sohail (01 Dec 2016) -- Start
    '                    'Enhancement - 64.1 - Insert Membership heads on Ed if not assigned.
    '                    'blnFlag = Insert(strDatabaseName, intUserUnkId, xYearUnkid, xCompanyUnkid, xPeriodStart, xPeriodEnd, xUserModeSetting, xOnlyApproved, xIncludeIn_ActiveEmployee, True, dtCurrentDateAndTime, , objDataOperation, blnApplyUserAccessFilter, strFilerString)
    '                    'Sohail (20 Jul 2018) -- Start
    '                    'Support Issue # 0002383 : VOLTAMP: Unable to assign Transaction heads using batch entry in Payroll transactions (Warning message of Membership heads not assigned)in 73.1.
    '                    'blnFlag = Insert(strDatabaseName, intUserUnkId, xYearUnkid, xCompanyUnkid, mdtPeriod_startdate, mdtPeriod_enddate, xUserModeSetting, xOnlyApproved, xIncludeIn_ActiveEmployee, True, dtCurrentDateAndTime, , objDataOperation, blnApplyUserAccessFilter, strFilerString, blnCheckMembershipHeads)
    '                    blnFlag = Insert(strDatabaseName, intUserUnkId, xYearUnkid, xCompanyUnkid, mdtPeriod_startdate, mdtPeriod_enddate, xUserModeSetting, xOnlyApproved, xIncludeIn_ActiveEmployee, True, dtCurrentDateAndTime, , objDataOperation, blnApplyUserAccessFilter, strFilerString, blnCheckMembershipHeads, blnAllowToApproveEarningDeduction)
    '                    'Sohail (20 Jul 2018) -- End
    '                    'Sohail (01 Dec 2016) -- End
    '                    'Sohail (21 Aug 2015) -- End
    '                End If

    '                If blnFlag = False Then
    '                    objDataOperation.ReleaseTransaction(False)
    '                    Return False
    '                End If

    '            Next

    '            '**** Copy Previous ED Slab
    '            If blnCopyPreviousEDSlab = True Then
    '                'Sohail (21 Aug 2015) -- Start
    '                'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
    '                'Call GetEmployeeED()
    '                'Sohail (01 Mar 2018) -- Start
    '                'Internal Issue : ExecureReader requires the command to have a transaction... in 70.1.
    '                'Call GetEmployeeED(strDatabaseName)
    '                Call GetEmployeeED(objDataOperation, strDatabaseName)
    '                'Sohail (01 Mar 2018) -- End
    '                'Sohail (21 Aug 2015) -- End

    '                Dim strPrevEndDate As String = ""
    '                Dim drRow As DataRow()

    '                'Sohail (09 Nov 2013) -- Start
    '                'TRA - ENHANCEMENT
    '                'drRow = mdtED.Select("AUD = '' AND end_date <= '" & eZeeDate.convertDate(mdtPeriod_startdate) & "' AND isrecurrent = 1 ", "end_date DESC")
    '                drRow = mdtED.Select("AUD = '' AND end_date <= '" & eZeeDate.convertDate(mdtPeriod_startdate) & "' AND isrecurrent = 1 AND (stop_date IS NULL OR stop_date >= '" & mdtPeriod_startdate & "') ", "end_date DESC")
    '                'Sohail (09 Nov 2013) -- End

    '                If drRow.Length > 0 Then
    '                    For Each dRow As DataRow In drRow
    '                        If strPrevEndDate <> "" AndAlso strPrevEndDate <> dRow.Item("end_date").ToString Then
    '                            Exit For
    '                        End If

    '                        'Sohail (21 Aug 2015) -- Start
    '                        'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
    '                        'objTranHead._Tranheadunkid = CInt(dRow.Item("tranheadunkid"))
    '                        objTranHead._Tranheadunkid(strDatabaseName) = CInt(dRow.Item("tranheadunkid"))
    '                        'Sohail (21 Aug 2015) -- End
    '                        'Sohail (25 May 2012) -- Start
    '                        'TRA - ENHANCEMENT
    '                        'Sohail (24 Feb 2016) -- Start
    '                        'Enhancement - Batch Posting changes as per online doc in 58.1 SP. (#15 (KBC & Kenya Project Comments List.xls))
    '                        'If objTranHead._Typeof_id = enTypeOf.Salary OrElse CInt(dRow.Item("tranheadunkid")) = intTranHeadID Then
    '                        If objTranHead._Typeof_id = enTypeOf.Salary OrElse dtTable.Select("tranheadunkid = " & CInt(dRow.Item("tranheadunkid")) & " ").Length > 0 Then
    '                            'Sohail (24 Feb 2016) -- End
    '                            'If objTranHead._Typeof_id = enTypeOf.Salary Then
    '                            'Sohail (25 May 2012) -- End
    '                            strPrevEndDate = dRow.Item("end_date").ToString
    '                            Continue For
    '                        End If

    '                        'Sohail (21 Aug 2015) -- Start
    '                        'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
    '                        'intEDID = GetEDUnkID(mintEmployeeunkid, CInt(dRow.Item("tranheadunkid")), intPeriodId)
    '                        intEDID = GetEDUnkID(strDatabaseName, mintEmployeeunkid, CInt(dRow.Item("tranheadunkid")), intPeriodId, objDataOperation)
    '                        'Sohail (21 Aug 2015) -- End

    '                        If intEDID > 0 Then
    '                            'Sohail (21 Aug 2015) -- Start
    '                            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
    '                            'Me._Edunkid = intEDID 'Sohail (25 May 2012)
    '                            Me._Edunkid(objDataOperation, strDatabaseName) = intEDID
    '                            'Sohail (21 Aug 2015) -- End
    '                            'Sohail (26 Jul 2012) -- Start
    '                            'TRA - ENHANCEMENT
    '                            'If blnOverWrite = True Then
    '                            If blnOverwritePreviousEDSlabHeads = True Then
    '                                'Sohail (26 Jul 2012) -- End
    '                                mdecAmount = CDec(dRow.Item("amount"))
    '                                mintCurrencyunkid = CInt(dRow.Item("currencyid"))
    '                                mintVendorunkid = CInt(dRow.Item("vendorid"))
    '                                If IsDBNull(dRow.Item("membership_categoryunkid")) Then
    '                                    mintMembership_Categoryunkid = 0
    '                                Else
    '                                    mintMembership_Categoryunkid = CInt(dRow.Item("membership_categoryunkid"))
    '                                End If
    '                                mintCostCenterUnkId = Int(dRow.Item("costcenterunkid"))
    '                                mstrMedicalrefno = dRow.Item("medicalrefno").ToString
    '                                'Sohail (23 Apr 2012) -- Start
    '                                'TRA - ENHANCEMENT
    '                                'mintUserunkid = User._Object._Userunkid
    '                                mintUserunkid = IIf(intUserUnkId = 0, User._Object._Userunkid, intUserUnkId)
    '                                'Sohail (23 Apr 2012) -- End
    '                                'Sohail (20 Feb 2013) -- Start
    '                                'TRA - ENHANCEMENT
    '                                mintMembershipTranUnkid = CInt(dRow.Item("membershiptranunkid"))
    '                                mintDisciplinefileunkid = CInt(dRow.Item("disciplinefileunkid"))
    '                                'Sohail (20 Feb 2013) -- End
    '                                'Sohail (09 Nov 2013) -- Start
    '                                'TRA - ENHANCEMENT
    '                                If IsDBNull(dRow.Item("cumulative_startdate")) = True Then
    '                                    mdtCumulative_Startdate = Nothing
    '                                Else
    '                                    mdtCumulative_Startdate = dRow.Item("cumulative_startdate")
    '                                End If
    '                                If IsDBNull(dRow.Item("stop_date")) = True Then
    '                                    mdtStop_Date = Nothing
    '                                Else
    '                                    mdtStop_Date = dRow.Item("stop_date")
    '                                End If
    '                                'Sohail (09 Nov 2013) -- End
    '                                mintEmpbatchpostingunkid = 0 'Sohail (24 Feb 2016)

    '                                'Sohail (21 Aug 2015) -- Start
    '                                'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
    '                                'blnFlag = Update(True)
    '                                blnFlag = Update(strDatabaseName, True, objDataOperation, dtCurrentDateAndTime)
    '                                'Sohail (21 Aug 2015) -- End
    '                            Else
    '                                blnFlag = True
    '                            End If
    '                        Else
    '                            mintTranheadunkid = CInt(dRow.Item("tranheadunkid"))
    '                            'Sohail (21 Aug 2015) -- Start
    '                            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
    '                            'objTranHead._Tranheadunkid = CInt(dRow.Item("tranheadunkid"))
    '                            objTranHead._Tranheadunkid(strDatabaseName) = CInt(dRow.Item("tranheadunkid"))
    '                            'Sohail (21 Aug 2015) -- End
    '                            mintBatchtransactionunkid = CInt(dRow.Item("batchtransactionunkid"))

    '                            mintTrnheadtype_Id = objTranHead._Trnheadtype_Id
    '                            mintTypeof_Id = objTranHead._Typeof_id
    '                            mintCalctype_Id = objTranHead._Calctype_Id
    '                            mintComputeon_Id = objTranHead._Computeon_Id
    '                            mstrFormula = objTranHead._Formula
    '                            mstrFormulaid = objTranHead._Formulaid
    '                            mdecAmount = CDec(dRow.Item("amount"))

    '                            If objTranHead._Trnheadtype_Id = enTranHeadType.EarningForEmployees Then
    '                                mblnIsdeduct = False
    '                            ElseIf objTranHead._Trnheadtype_Id = enTranHeadType.EmployersStatutoryContributions OrElse objTranHead._Trnheadtype_Id = enTranHeadType.Informational Then
    '                                mblnIsdeduct = Nothing
    '                            Else
    '                                mblnIsdeduct = True
    '                            End If

    '                            mintCurrencyunkid = CInt(dRow.Item("currencyid"))
    '                            mintVendorunkid = CInt(dRow.Item("vendorid"))
    '                            'Sohail (23 Apr 2012) -- Start
    '                            'TRA - ENHANCEMENT
    '                            'mintUserunkid = User._Object._Userunkid
    '                            mintUserunkid = IIf(intUserUnkId = 0, User._Object._Userunkid, intUserUnkId)
    '                            'Sohail (23 Apr 2012) -- End
    '                            mblnIsvoid = False
    '                            mintVoiduserunkid = -1
    '                            mdtVoiddatetime = Nothing
    '                            mstrVoidreason = ""
    '                            mintMembership_Categoryunkid = 0
    '                            'Sohail (23 Apr 2012) -- Start
    '                            'TRA - ENHANCEMENT
    '                            'Sohail (21 Aug 2015) -- Start
    '                            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
    '                            'If strAllowToApproveEarningDeduction = "" Then strAllowToApproveEarningDeduction = User._Object.Privilege._AllowToApproveEarningDeduction.ToString()
    '                            'If CBool(strAllowToApproveEarningDeduction) = True Then
    '                            If blnAllowToApproveEarningDeduction = True Then
    '                                'Sohail (21 Aug 2015) -- End
    '                                'If User._Object.Privilege._AllowToApproveEarningDeduction = True Then
    '                                'Sohail (23 Apr 2012) -- End
    '                                mblnIsapproved = True
    '                                'Sohail (23 Apr 2012) -- Start
    '                                'TRA - ENHANCEMENT
    '                                'mintApproveruserunkid = User._Object._Userunkid
    '                                mintApproveruserunkid = IIf(intUserUnkId = 0, User._Object._Userunkid, intUserUnkId)
    '                                'Sohail (23 Apr 2012) -- End
    '                            Else
    '                                mblnIsapproved = False
    '                                mintApproveruserunkid = -1
    '                            End If
    '                            mintPeriodunkid = intPeriodId
    '                            mintCostCenterUnkId = 0
    '                            mstrMedicalrefno = ""
    '                            'Sohail (20 Feb 2013) -- Start
    '                            'TRA - ENHANCEMENT
    '                            mintMembershipTranUnkid = CInt(dRow.Item("membershiptranunkid"))
    '                            mintDisciplinefileunkid = CInt(dRow.Item("disciplinefileunkid"))
    '                            'Sohail (20 Feb 2013) -- End
    '                            'Sohail (09 Nov 2013) -- Start
    '                            'TRA - ENHANCEMENT
    '                            If IsDBNull(dRow.Item("cumulative_startdate")) = True Then
    '                                mdtCumulative_Startdate = Nothing
    '                            Else
    '                                mdtCumulative_Startdate = dRow.Item("cumulative_startdate")
    '                            End If
    '                            If IsDBNull(dRow.Item("stop_date")) = True Then
    '                                mdtStop_Date = Nothing
    '                            Else
    '                                mdtStop_Date = dRow.Item("stop_date")
    '                            End If
    '                            'Sohail (09 Nov 2013) -- End
    '                            mintEmpbatchpostingunkid = 0 'Sohail (24 Feb 2016)

    '                            'Sohail (21 Aug 2015) -- Start
    '                            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
    '                            'blnFlag = Insert(True)

    '                            'Sohail (01 Dec 2016) -- Start
    '                            'Enhancement - 64.1 - Insert Membership heads on Ed if not assigned.
    '                            'blnFlag = Insert(strDatabaseName, intUserUnkId, xYearUnkid, xCompanyUnkid, xPeriodStart, xPeriodEnd, xUserModeSetting, xOnlyApproved, xIncludeIn_ActiveEmployee, True, dtCurrentDateAndTime, , objDataOperation, blnApplyUserAccessFilter, strFilerString)
    '                            'Sohail (20 Jul 2018) -- Start
    '                            'Support Issue # 0002383 : VOLTAMP: Unable to assign Transaction heads using batch entry in Payroll transactions (Warning message of Membership heads not assigned)in 73.1.
    '                            'blnFlag = Insert(strDatabaseName, intUserUnkId, xYearUnkid, xCompanyUnkid, mdtPeriod_startdate, mdtPeriod_enddate, xUserModeSetting, xOnlyApproved, xIncludeIn_ActiveEmployee, True, dtCurrentDateAndTime, , objDataOperation, blnApplyUserAccessFilter, strFilerString, blnCheckMembershipHeads)
    '                            blnFlag = Insert(strDatabaseName, intUserUnkId, xYearUnkid, xCompanyUnkid, mdtPeriod_startdate, mdtPeriod_enddate, xUserModeSetting, xOnlyApproved, xIncludeIn_ActiveEmployee, True, dtCurrentDateAndTime, , objDataOperation, blnApplyUserAccessFilter, strFilerString, False, blnAllowToApproveEarningDeduction)
    '                            'Sohail (20 Jul 2018) -- End
    '                            'Sohail (01 Dec 2016) -- End
    '                            'Sohail (21 Aug 2015) -- End
    '                        End If

    '                        If blnFlag = False Then
    '                            objDataOperation.ReleaseTransaction(False)
    '                            Return False
    '                        End If

    '                        strPrevEndDate = dRow.Item("end_date").ToString
    '                    Next
    '                End If
    '            End If

    '        Next

    '        Return True
    '    Catch ex As Exception
    '        objDataOperation.ReleaseTransaction(False)
    '        Throw New Exception(ex.Message & "; Procedure Name: InsertAllByDataTable; Module Name: " & mstrModuleName)
    '        Return False
    '    Finally
    '        exForce = Nothing
    '        If dsList IsNot Nothing Then dsList.Dispose()
    '        objTranHead = Nothing
    '        objDataOperation = Nothing
    '    End Try
    'End Function
    Public Function InsertAllByDataTable(ByVal strDatabaseName As String _
                                         , ByVal xYearUnkid As Integer _
                                         , ByVal xCompanyUnkid As Integer _
                                         , ByVal xPeriodStart As DateTime _
                                         , ByVal xPeriodEnd As DateTime _
                                         , ByVal xUserModeSetting As String _
                                         , ByVal xOnlyApproved As Boolean _
                                         , ByVal xIncludeIn_ActiveEmployee As Boolean _
                                         , ByVal xDataOp As clsDataOperation _
                                         , ByVal strEmployeeList As String _
                                         , ByVal dtTable As DataTable _
                                         , ByVal blnOverWrite As Boolean _
                                         , ByVal intUserUnkId As Integer _
                                         , ByVal blnAllowToApproveEarningDeduction As Boolean _
                                         , ByVal dtCurrentDateAndTime As Date _
                                         , Optional ByVal blnInsertSalaryHead As Boolean = False _
                                         , Optional ByVal blnCopyPreviousEDSlab As Boolean = False _
                                         , Optional ByVal blnOverwritePreviousEDSlabHeads As Boolean = False _
                                         , Optional ByVal blnApplyUserAccessFilter As Boolean = True _
                                         , Optional ByVal strFilerString As String = "" _
                                         , Optional ByVal blnCheckMembershipHeads As Boolean = True _
                                         , Optional ByVal blnSkipInsertIfExist As Boolean = False _
                                         ) As Boolean
        'Sohail (25 Jun 2020) - [blnSkipInsertIfExist]

        Dim dsList As DataSet = Nothing
        Dim dsHead As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim blnFlag As Boolean
        Dim intBatchID As Integer
        Dim intTranHeadID As Integer
        Dim decAmount As Decimal
        Dim intCurrencyID As Integer
        Dim intVendorID As Integer
        Dim intPeriodId As Integer
        Dim intCostCenterID As Integer
        Dim strMedicalRefNo As String
        Dim dtCumulative_Startdate As Date
        Dim dtStop_date As Date
        'Sohail (24 Aug 2019) -- Start
        'NMB Payroll UAT # TC016 - 76.1 - System should be able to accept future amendments (Future period) and prorate. It should be possible to set start date of the specific transaction and prorate based on the start date. It should also be possible to set start date of the future period.
        Dim dtEDStart_date As Date
        'Sohail (24 Aug 2019) -- End
        Dim objTranHead As New clsTransactionHead
        Dim arrEmp As String()
        Dim mdtPeriod_startdate As Date
        Dim mdtPeriod_enddate As Date
        Dim intEmpbatchpostingunkid As Integer

        If xDataOp IsNot Nothing Then
            objDataOperation = xDataOp
        Else
            objDataOperation = New clsDataOperation
            'objDataOperation.BindTransaction() 'Sohail (10 Dec 2019)
        End If
        objDataOperation.ClearParameters()

        Try
            arrEmp = strEmployeeList.Split(",")

            'Sohail (18 Jan 2019) -- Start
            'Issue - 74.1 - Bind Transaction issue.
            'dsHead = objTranHead.GetList(strDatabaseName, -1, -1, False, True, 0, False, "", True, True, True, 2)

            'S.SANDEEP |24-JUN-2019| -- START
            'ISSUE/ENHANCEMENT : {Get All Transaction Heads}
            'dsHead = objTranHead.GetList(strDatabaseName, -1, -1, False, True, 0, False, "", True, True, True, 2, objDataOperation)
            'Sohail (25 Jun 2020) -- Start
            'NMB Enhancement # : Performance enhancement on Import earning deduction heads.
            'dsHead = objTranHead.GetList(strDatabaseName, -1, -1, False, True, 0, False, "", True, True, True, 0, objDataOperation)
            dsHead = objTranHead.GetList(strDatabaseName, -1, -1, False, True, 0, False, "", True, True, True, 0, objDataOperation, xPeriodEnd)
            'Sohail (25 Jun 2020) -- End
            'S.SANDEEP |24-JUN-2019| -- END

            'Sohail (18 Jan 2019) -- End

            For i As Integer = 0 To arrEmp.Length - 1
                mintEmployeeunkid = CInt(arrEmp(i))

                'Sohail (10 Dec 2019) -- Start
                'NMB UAT Enhancement # : Not able to access employee payroll while doing batch entry. 
                If xDataOp IsNot Nothing Then
                    objDataOperation = xDataOp
                Else
                    objDataOperation = New clsDataOperation
                    objDataOperation.BindTransaction()
                End If
                'Sohail (10 Dec 2019) -- End

                For Each dtRow As DataRow In dtTable.Rows

                    objDataOperation.ClearParameters()

                    intTranHeadID = CInt(dtRow.Item("tranheadunkid").ToString)
                    intBatchID = CInt(dtRow.Item("batchtransactionunkid").ToString)
                    intCurrencyID = CInt(dtRow.Item("currencyid").ToString)
                    decAmount = CDec(dtRow.Item("Amount").ToString)
                    intVendorID = CInt(dtRow.Item("vendorid").ToString)
                    intPeriodId = CInt(dtRow.Item("periodunkid").ToString)
                    intCostCenterID = CInt(dtRow.Item("costcenterunkid").ToString)
                    strMedicalRefNo = dtRow.Item("medicalrefno").ToString
                    mdtPeriod_startdate = eZeeDate.convertDate(dtRow.Item("start_date").ToString)
                    mdtPeriod_enddate = eZeeDate.convertDate(dtRow.Item("end_date").ToString)
                    If IsDBNull(dtRow.Item("cumulative_startdate")) = True Then
                        dtCumulative_Startdate = Nothing
                    Else
                        dtCumulative_Startdate = dtRow.Item("cumulative_startdate")
                    End If
                    If IsDBNull(dtRow.Item("stop_date")) = True Then
                        dtStop_date = Nothing
                    Else
                        dtStop_date = dtRow.Item("stop_date")
                    End If
                    'Sohail (24 Aug 2019) -- Start
                    'NMB Payroll UAT # TC016 - 76.1 - System should be able to accept future amendments (Future period) and prorate. It should be possible to set start date of the specific transaction and prorate based on the start date. It should also be possible to set start date of the future period.
                    If IsDBNull(dtRow.Item("edstart_date")) = True Then
                        dtEDStart_date = Nothing
                    Else
                        dtEDStart_date = dtRow.Item("edstart_date")
                    End If
                    'Sohail (24 Aug 2019) -- End
                    intEmpbatchpostingunkid = CInt(dtRow.Item("empbatchpostingunkid").ToString)

                    If blnInsertSalaryHead = True Then
                        blnFlag = Insert_SalaryHead(strDatabaseName, intUserUnkId, xYearUnkid, xCompanyUnkid, mdtPeriod_startdate, mdtPeriod_enddate, xUserModeSetting, xOnlyApproved, xIncludeIn_ActiveEmployee, mintEmployeeunkid, intPeriodId, dtCurrentDateAndTime, objDataOperation, False, strFilerString)

                        If blnFlag = False Then
                            objDataOperation.ReleaseTransaction(False)
                            Return False
                        End If
                    End If

                    'Sohail (25 Jun 2020) -- Start
                    'NMB Enhancement # : Performance enhancement on Import earning deduction heads.
                    'Call GetEmployeeED(objDataOperation, strDatabaseName)

                    'Dim drED() As DataRow = mdtED.Select("employeeunkid = " & mintEmployeeunkid & " AND tranheadunkid = " & intTranHeadID & " AND periodunkid = " & intPeriodId & " ")

                    'If drED.Length > 0 Then
                    '    Call GetData(drED(0))
                    If GetDataIfExist(objDataOperation, strDatabaseName, mintEmployeeunkid, intTranHeadID, intPeriodId) = True Then
                        'Sohail (25 Jun 2020) -- End

                        If blnOverWrite = True Then
                            mdecAmount = decAmount
                            mintCurrencyunkid = intCurrencyID
                            mintVendorunkid = intVendorID
                            mintMembership_Categoryunkid = 0
                            mintCostCenterUnkId = intCostCenterID
                            mstrMedicalrefno = strMedicalRefNo
                            mintMembershipTranUnkid = CInt(dtRow.Item("membershiptranunkid"))
                            mintDisciplinefileunkid = CInt(dtRow.Item("disciplinefileunkid"))
                            mdtCumulative_Startdate = dtCumulative_Startdate
                            mdtStop_Date = dtStop_date
                            'Sohail (24 Aug 2019) -- Start
                            'NMB Payroll UAT # TC016 - 76.1 - System should be able to accept future amendments (Future period) and prorate. It should be possible to set start date of the specific transaction and prorate based on the start date. It should also be possible to set start date of the future period.
                            mdtEDStart_Date = dtEDStart_date
                            'Sohail (24 Aug 2019) -- End
                            mintEmpbatchpostingunkid = intEmpbatchpostingunkid

                            mintUserunkid = IIf(intUserUnkId = 0, User._Object._Userunkid, intUserUnkId)

                            'Sohail (02 Sep 2019) -- Start
                            'Mainspring Resourcing Ghana Ltd Enhancement - 76.1 - # 0004098: Approval Flow for Flat Rate Heads Amount Uploaded.
                            '1. Set ED in pending status if user has not priviledge of ED approval
                            '2. Perform Global approver with option to filter for allocation and approved any posted (new /edited) E & D values in MSS
                            '3. Send notifications to ED approvers for all employees ED in one email
                            If blnAllowToApproveEarningDeduction = True Then
                                mblnIsapproved = True
                                mintApproveruserunkid = mintUserunkid
                            Else
                                mblnIsapproved = False
                                mintApproveruserunkid = -1
                            End If
                            'Sohail (02 Sep 2019) -- End

                            blnFlag = Update(strDatabaseName, True, objDataOperation, dtCurrentDateAndTime)
                        Else
                            blnFlag = True
                        End If

                    Else
                        mintTranheadunkid = intTranHeadID

                        Dim drHead() As DataRow = dsHead.Tables(0).Select("tranheadunkid = " & intTranHeadID & " ")
                        mintBatchtransactionunkid = intBatchID

                        If drHead.Length <= 0 Then
                            mstrMessage = Language.getMessage(mstrModuleName, 6, "Transaction head doees not exist.") & "[" & intTranHeadID & "]"
                            objDataOperation.ReleaseTransaction(False)
                            Return False
                        Else

                            mintTrnheadtype_Id = CInt(drHead(0).Item("trnheadtype_id"))
                            mintTypeof_Id = CInt(drHead(0).Item("typeof_id"))
                            mintCalctype_Id = CInt(drHead(0).Item("calctype_id"))
                            mintComputeon_Id = CInt(drHead(0).Item("computeon_id"))
                            mstrFormula = drHead(0).Item("formula").ToString
                            mstrFormulaid = drHead(0).Item("formulaid")
                        mdecAmount = decAmount

                            If CInt(drHead(0).Item("trnheadtype_id")) = CInt(enTranHeadType.EarningForEmployees) Then 'enTranHeadType.EarningForEmployees
                            mblnIsdeduct = False
                            ElseIf CInt(drHead(0).Item("trnheadtype_id")) = CInt(enTranHeadType.EmployersStatutoryContributions) OrElse CInt(drHead(0).Item("trnheadtype_id")) = CInt(enTranHeadType.Informational) Then 'enTranHeadType.EmployersStatutoryContributions , Informational
                            mblnIsdeduct = Nothing
                        Else
                            mblnIsdeduct = True
                        End If

                        mintCurrencyunkid = intCurrencyID
                        mintVendorunkid = intVendorID
                        mintUserunkid = IIf(intUserUnkId = 0, User._Object._Userunkid, intUserUnkId)
                        mblnIsvoid = False
                        mintVoiduserunkid = -1
                        mdtVoiddatetime = Nothing
                        mstrVoidreason = ""
                        mintMembership_Categoryunkid = 0

                        If blnAllowToApproveEarningDeduction = True Then
                            mblnIsapproved = True
                            mintApproveruserunkid = intUserUnkId
                        Else
                            mblnIsapproved = False
                            mintApproveruserunkid = -1
                        End If
                        mintPeriodunkid = intPeriodId
                        mintCostCenterUnkId = intCostCenterID
                        mstrMedicalrefno = strMedicalRefNo
                        mintMembershipTranUnkid = CInt(dtRow.Item("membershiptranunkid"))
                        mintDisciplinefileunkid = CInt(dtRow.Item("disciplinefileunkid"))
                        mdtCumulative_Startdate = dtCumulative_Startdate
                        mdtStop_Date = dtStop_date
                            'Sohail (24 Aug 2019) -- Start
                            'NMB Payroll UAT # TC016 - 76.1 - System should be able to accept future amendments (Future period) and prorate. It should be possible to set start date of the specific transaction and prorate based on the start date. It should also be possible to set start date of the future period.
                            mdtEDStart_Date = dtEDStart_date
                            'Sohail (24 Aug 2019) -- End
                            mintEmpbatchpostingunkid = intEmpbatchpostingunkid

                            'Sohail (25 Jun 2020) -- Start
                            'NMB Enhancement # : Performance enhancement on Import earning deduction heads.
                            'blnFlag = Insert(strDatabaseName, intUserUnkId, xYearUnkid, xCompanyUnkid, mdtPeriod_startdate, mdtPeriod_enddate, xUserModeSetting, xOnlyApproved, xIncludeIn_ActiveEmployee, True, dtCurrentDateAndTime, , objDataOperation, blnApplyUserAccessFilter, strFilerString, blnCheckMembershipHeads, blnAllowToApproveEarningDeduction)
                            blnFlag = Insert(strDatabaseName, intUserUnkId, xYearUnkid, xCompanyUnkid, mdtPeriod_startdate, mdtPeriod_enddate, xUserModeSetting, xOnlyApproved, xIncludeIn_ActiveEmployee, True, dtCurrentDateAndTime, , objDataOperation, blnApplyUserAccessFilter, strFilerString, blnCheckMembershipHeads, blnAllowToApproveEarningDeduction, blnSkipInsertIfExist)
                            'Sohail (25 Jun 2020) -- End

                        End If

                    End If

                    If blnFlag = False Then
                        objDataOperation.ReleaseTransaction(False)
                        Return False
                    End If

                Next

                '**** Copy Previous ED Slab
                If blnCopyPreviousEDSlab = True Then
                    'Sohail (25 Jun 2020) -- Start
                    'NMB Enhancement # : Performance enhancement on Import earning deduction heads.
                    'Call GetEmployeeED(objDataOperation, strDatabaseName)
                    'Sohail (19 Sep 2020) -- Start
                    'NMB Issue : #  : Copy previous slab pick latest slab based on current date on close period.
                    'Call GetEmployeeED(objDataOperation, strDatabaseName, "", True)
                    Call GetEmployeeED(objDataOperation, strDatabaseName, "", True, xPeriodEnd)
                    'Sohail (19 Sep 2020) -- End
                    'Sohail (25 Jun 2020) -- End

                    Dim strPrevEndDate As String = ""
                    Dim drRow As DataRow()

                    'Sohail (24 Aug 2019) -- Start
                    'NMB Payroll UAT # TC016 - 76.1 - System should be able to accept future amendments (Future period) and prorate. It should be possible to set start date of the specific transaction and prorate based on the start date. It should also be possible to set start date of the future period.
                    'drRow = mdtED.Select("AUD = '' AND end_date <= '" & eZeeDate.convertDate(mdtPeriod_startdate) & "' AND isrecurrent = 1 AND (stop_date IS NULL OR stop_date >= '" & mdtPeriod_startdate & "') ", "end_date DESC")
                    drRow = mdtED.Select("AUD = '' AND end_date <= '" & eZeeDate.convertDate(mdtPeriod_startdate) & "' AND isrecurrent = 1 AND (stop_date IS NULL OR stop_date >= '" & mdtPeriod_startdate & "') AND (edstart_date IS NULL OR edstart_date <= '" & mdtPeriod_enddate & "') ", "end_date DESC")
                    'Sohail (24 Aug 2019) -- End

                    If drRow.Length > 0 Then
                        For Each dRow As DataRow In drRow
                            If strPrevEndDate <> "" AndAlso strPrevEndDate <> dRow.Item("end_date").ToString Then
                                Exit For
                            End If

                            Dim drHead() As DataRow = dsHead.Tables(0).Select("tranheadunkid = " & CInt(dRow.Item("tranheadunkid")) & " ")

                            If drHead.Length <= 0 Then
                                strPrevEndDate = dRow.Item("end_date").ToString
                                Continue For
                            Else
                                If CInt(drHead(0).Item("typeof_id")) = enTypeOf.Salary OrElse CInt(dRow.Item("tranheadunkid")) = intTranHeadID Then
                                strPrevEndDate = dRow.Item("end_date").ToString
                                Continue For
                            End If
                            End If

                            Dim drED() As DataRow = mdtED.Select("employeeunkid = " & mintEmployeeunkid & " AND tranheadunkid = " & CInt(dRow.Item("tranheadunkid")) & " AND periodunkid = " & intPeriodId & " ")

                            If drED.Length > 0 Then
                                Call GetData(drED(0))

                                If blnOverwritePreviousEDSlabHeads = True Then
                                    mdecAmount = CDec(dRow.Item("amount"))
                                    mintCurrencyunkid = CInt(dRow.Item("currencyid"))
                                    mintVendorunkid = CInt(dRow.Item("vendorid"))
                                    If IsDBNull(dRow.Item("membership_categoryunkid")) Then
                                        mintMembership_Categoryunkid = 0
                                    Else
                                        mintMembership_Categoryunkid = CInt(dRow.Item("membership_categoryunkid"))
                                    End If
                                    mintCostCenterUnkId = Int(dRow.Item("costcenterunkid"))
                                    mstrMedicalrefno = dRow.Item("medicalrefno").ToString
                                    mintUserunkid = IIf(intUserUnkId = 0, User._Object._Userunkid, intUserUnkId)
                                    mintMembershipTranUnkid = CInt(dRow.Item("membershiptranunkid"))
                                    mintDisciplinefileunkid = CInt(dRow.Item("disciplinefileunkid"))
                                    If IsDBNull(dRow.Item("cumulative_startdate")) = True Then
                                        mdtCumulative_Startdate = Nothing
                                    Else
                                        mdtCumulative_Startdate = dRow.Item("cumulative_startdate")
                                    End If
                                    If IsDBNull(dRow.Item("stop_date")) = True Then
                                        mdtStop_Date = Nothing
                                    Else
                                        mdtStop_Date = dRow.Item("stop_date")
                                    End If
                                    'Sohail (24 Aug 2019) -- Start
                                    'NMB Payroll UAT # TC016 - 76.1 - System should be able to accept future amendments (Future period) and prorate. It should be possible to set start date of the specific transaction and prorate based on the start date. It should also be possible to set start date of the future period.
                                    If IsDBNull(dRow.Item("edstart_date")) = True Then
                                        mdtEDStart_Date = Nothing
                                    Else
                                        mdtEDStart_Date = dRow.Item("edstart_date")
                                    End If
                                    'Sohail (24 Aug 2019) -- End
                                    mintEmpbatchpostingunkid = 0

                                    'Sohail (02 Sep 2019) -- Start
                                    'Mainspring Resourcing Ghana Ltd Enhancement - 76.1 - # 0004098: Approval Flow for Flat Rate Heads Amount Uploaded.
                                    '1. Set ED in pending status if user has not priviledge of ED approval
                                    '2. Perform Global approver with option to filter for allocation and approved any posted (new /edited) E & D values in MSS
                                    '3. Send notifications to ED approvers for all employees ED in one email
                                    If blnAllowToApproveEarningDeduction = True Then
                                        mblnIsapproved = True
                                        mintApproveruserunkid = mintUserunkid
                                    Else
                                        mblnIsapproved = False
                                        mintApproveruserunkid = -1
                                    End If
                                    'Sohail (02 Sep 2019) -- End

                                    blnFlag = Update(strDatabaseName, True, objDataOperation, dtCurrentDateAndTime)
                                Else
                                    blnFlag = True
                                End If
                            Else
                                mintTranheadunkid = CInt(dRow.Item("tranheadunkid"))
                                drHead = dsHead.Tables(0).Select("tranheadunkid = " & CInt(dRow.Item("tranheadunkid")) & " ")
                                mintBatchtransactionunkid = CInt(dRow.Item("batchtransactionunkid"))

                                If drHead.Length <= 0 Then
                                    strPrevEndDate = dRow.Item("end_date").ToString
                                    Continue For
                                End If

                                mintTrnheadtype_Id = CInt(drHead(0).Item("trnheadtype_id"))
                                mintTypeof_Id = CInt(drHead(0).Item("typeof_id"))
                                mintCalctype_Id = CInt(drHead(0).Item("calctype_id"))
                                mintComputeon_Id = CInt(drHead(0).Item("computeon_id"))
                                mstrFormula = drHead(0).Item("formula").ToString
                                mstrFormulaid = drHead(0).Item("formulaid")
                                mdecAmount = CDec(dRow.Item("amount"))

                                If CInt(drHead(0).Item("trnheadtype_id")) = enTranHeadType.EarningForEmployees Then
                                    mblnIsdeduct = False
                                ElseIf CInt(drHead(0).Item("trnheadtype_id")) = enTranHeadType.EmployersStatutoryContributions OrElse CInt(drHead(0).Item("trnheadtype_id")) = enTranHeadType.Informational Then
                                    mblnIsdeduct = Nothing
                                Else
                                    mblnIsdeduct = True
                                End If

                                mintCurrencyunkid = CInt(dRow.Item("currencyid"))
                                mintVendorunkid = CInt(dRow.Item("vendorid"))
                                mintUserunkid = IIf(intUserUnkId = 0, User._Object._Userunkid, intUserUnkId)
                                mblnIsvoid = False
                                mintVoiduserunkid = -1
                                mdtVoiddatetime = Nothing
                                mstrVoidreason = ""
                                mintMembership_Categoryunkid = 0
                                If blnAllowToApproveEarningDeduction = True Then
                                    mblnIsapproved = True
                                    mintApproveruserunkid = IIf(intUserUnkId = 0, User._Object._Userunkid, intUserUnkId)
                                Else
                                    mblnIsapproved = False
                                    mintApproveruserunkid = -1
                                End If
                                mintPeriodunkid = intPeriodId
                                mintCostCenterUnkId = 0
                                mstrMedicalrefno = ""
                                mintMembershipTranUnkid = CInt(dRow.Item("membershiptranunkid"))
                                mintDisciplinefileunkid = CInt(dRow.Item("disciplinefileunkid"))
                                If IsDBNull(dRow.Item("cumulative_startdate")) = True Then
                                    mdtCumulative_Startdate = Nothing
                                Else
                                    mdtCumulative_Startdate = dRow.Item("cumulative_startdate")
                                End If
                                If IsDBNull(dRow.Item("stop_date")) = True Then
                                    mdtStop_Date = Nothing
                                Else
                                    mdtStop_Date = dRow.Item("stop_date")
                                End If
                                'Sohail (24 Aug 2019) -- Start
                                'NMB Payroll UAT # TC016 - 76.1 - System should be able to accept future amendments (Future period) and prorate. It should be possible to set start date of the specific transaction and prorate based on the start date. It should also be possible to set start date of the future period.
                                If IsDBNull(dRow.Item("edstart_date")) = True Then
                                    mdtEDStart_Date = Nothing
                                Else
                                    mdtEDStart_Date = dRow.Item("edstart_date")
                                End If
                                'Sohail (24 Aug 2019) -- End
                                mintEmpbatchpostingunkid = 0

                                'Sohail (25 Jun 2020) -- Start
                                'NMB Enhancement # : Performance enhancement on Import earning deduction heads.
                                'blnFlag = Insert(strDatabaseName, intUserUnkId, xYearUnkid, xCompanyUnkid, mdtPeriod_startdate, mdtPeriod_enddate, xUserModeSetting, xOnlyApproved, xIncludeIn_ActiveEmployee, True, dtCurrentDateAndTime, , objDataOperation, blnApplyUserAccessFilter, strFilerString, False, blnAllowToApproveEarningDeduction)
                                blnFlag = Insert(strDatabaseName, intUserUnkId, xYearUnkid, xCompanyUnkid, mdtPeriod_startdate, mdtPeriod_enddate, xUserModeSetting, xOnlyApproved, xIncludeIn_ActiveEmployee, True, dtCurrentDateAndTime, , objDataOperation, blnApplyUserAccessFilter, strFilerString, False, blnAllowToApproveEarningDeduction, blnSkipInsertIfExist)
                                'Sohail (25 Jun 2020) -- End
                            End If

                            If blnFlag = False Then
                                objDataOperation.ReleaseTransaction(False)
                                Return False
                            End If

                            strPrevEndDate = dRow.Item("end_date").ToString
                        Next
                    End If
                End If

                'Sohail (10 Dec 2019) -- Start
                'NMB UAT Enhancement # : Not able to access employee payroll while doing batch entry. 
                If xDataOp Is Nothing Then objDataOperation.ReleaseTransaction(True)
                'Sohail (10 Dec 2019) -- End

            Next

            'If xDataOp Is Nothing Then objDataOperation.ReleaseTransaction(True) 'Sohail (10 Dec 2019)

            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: InsertAllByDataTable; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objTranHead = Nothing
            If xDataOp Is Nothing Then objDataOperation = Nothing
        End Try
    End Function
    'Sohail (23 Apr 2012) -- End
    'Sohail (15 Dec 2018) -- End

    'Sohail (15 Dec 2018) -- Start
    'HJFMRI Enhancement - Copy Selected Period ED Slab option on Global Assign ED screen in 75.1.
    'Sohail (05 Dec 2012) -- Start
    'TRA - ENHANCEMENT
    'Public Function InsertAllByDataTable(ByVal strDatabaseName As String _
    '                                     , ByVal xYearUnkid As Integer _
    '                                     , ByVal xCompanyUnkid As Integer _
    '                                     , ByVal xPeriodStart As DateTime _
    '                                     , ByVal xPeriodEnd As DateTime _
    '                                     , ByVal xUserModeSetting As String _
    '                                     , ByVal xOnlyApproved As Boolean _
    '                                     , ByVal xIncludeIn_ActiveEmployee As Boolean _
    '                                     , ByVal dtTable As DataTable _
    '                                     , ByVal blnOverWrite As Boolean _
    '                                     , ByVal intUserUnkId As Integer _
    '                                     , ByVal blnAllowToApproveEarningDeduction As Boolean _
    '                                     , ByVal dtCurrentDateAndTime As Date _
    '                                     , Optional ByVal blnInsertSalaryHead As Boolean = False _
    '                                     , Optional ByVal blnCopyPreviousEDSlab As Boolean = False _
    '                                     , Optional ByVal blnOverwritePreviousEDSlabHeads As Boolean = False _
    '                                     , Optional ByVal blnApplyUserAccessFilter As Boolean = True _
    '                                     , Optional ByVal strFilerString As String = "" _
    '                                     ) As Boolean
    '    '                               'Sohail (21 Aug 2015) - [strDatabaseName, xYearUnkid, xCompanyUnkid, xPeriodStart, xPeriodEnd, xUserModeSetting, xOnlyApproved, xIncludeIn_ActiveEmployee, dtCurrentDateAndTime, blnApplyUserAccessFilter, strFilerString]

    '    Dim objDataOperation As clsDataOperation

    '    Dim dsList As DataSet = Nothing
    '    Dim strQ As String = ""
    '    Dim exForce As Exception
    '    Dim blnFlag As Boolean
    '    Dim intEDID As Integer
    '    Dim intBatchID As Integer
    '    Dim intTranHeadID As Integer
    '    Dim decAmount As Decimal
    '    Dim intCurrencyID As Integer
    '    Dim intVendorID As Integer
    '    Dim intPeriodId As Integer
    '    Dim intCostCenterID As Integer
    '    Dim strMedicalRefNo As String
    '    'Sohail (09 Nov 2013) -- Start
    '    'TRA - ENHANCEMENT
    '    Dim dtCumulative_Startdate As Date
    '    Dim dtStop_date As Date
    '    'Sohail (09 Nov 2013) -- End
    '    Dim objTranHead As New clsTransactionHead
    '    Dim dicEmpList As New Dictionary(Of String, ArrayList)
    '    Dim arrEmp As ArrayList
    '    Dim mdtPeriod_startdate As Date
    '    Dim mdtPeriod_enddate As Date
    '    Dim intEmpbatchpostingunkid As Integer 'Sohail (24 Feb 2016)

    '    objDataOperation = New clsDataOperation
    '    objDataOperation.BindTransaction()

    '    Try


    '        For Each dtRow As DataRow In dtTable.Rows

    '            objDataOperation.ClearParameters()

    '            mintEmployeeunkid = CInt(dtRow.Item("employeeunkid").ToString)
    '            intTranHeadID = CInt(dtRow.Item("tranheadunkid").ToString)
    '            intBatchID = CInt(dtRow.Item("batchtransactionunkid").ToString)
    '            intCurrencyID = CInt(dtRow.Item("currencyid").ToString)
    '            decAmount = CDec(dtRow.Item("Amount").ToString)
    '            intVendorID = CInt(dtRow.Item("vendorid").ToString)
    '            intPeriodId = CInt(dtRow.Item("periodunkid").ToString)
    '            intCostCenterID = CInt(dtRow.Item("costcenterunkid").ToString)
    '            strMedicalRefNo = dtRow.Item("medicalrefno").ToString
    '            mdtPeriod_startdate = eZeeDate.convertDate(dtRow.Item("start_date").ToString)
    '            mdtPeriod_enddate = eZeeDate.convertDate(dtRow.Item("end_date").ToString)
    '            'Sohail (09 Nov 2013) -- Start
    '            'TRA - ENHANCEMENT
    '            If IsDBNull(dtRow.Item("cumulative_startdate")) = True Then
    '                dtCumulative_Startdate = Nothing
    '            Else
    '                dtCumulative_Startdate = dtRow.Item("cumulative_startdate")
    '            End If
    '            If IsDBNull(dtRow.Item("stop_date")) = True Then
    '                dtStop_date = Nothing
    '            Else
    '                dtStop_date = dtRow.Item("stop_date")
    '            End If
    '            'Sohail (09 Nov 2013) -- End
    '            intEmpbatchpostingunkid = CInt(dtRow.Item("empbatchpostingunkid").ToString) 'Sohail (24 Feb 2016)

    '            If dicEmpList.ContainsKey(mintEmployeeunkid.ToString & "|" & intPeriodId.ToString) = False Then
    '                arrEmp = New ArrayList
    '                arrEmp.Add(mintEmployeeunkid)
    '                arrEmp.Add(intPeriodId)
    '                arrEmp.Add(mdtPeriod_startdate)
    '                arrEmp.Add(mdtPeriod_enddate)
    '                dicEmpList.Add(mintEmployeeunkid.ToString & "|" & intPeriodId.ToString, arrEmp)
    '            End If

    '            If blnInsertSalaryHead = True Then
    '                'Sohail (21 Aug 2015) -- Start
    '                'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
    '                'blnFlag = Insert_SalaryHead(mintEmployeeunkid, intPeriodId)
    '                blnFlag = Insert_SalaryHead(strDatabaseName, intUserUnkId, xYearUnkid, xCompanyUnkid, xPeriodStart, xPeriodEnd, xUserModeSetting, xOnlyApproved, xIncludeIn_ActiveEmployee, mintEmployeeunkid, intPeriodId, dtCurrentDateAndTime, objDataOperation, blnApplyUserAccessFilter, strFilerString)
    '                'Sohail (21 Aug 2015) -- End

    '                If blnFlag = False Then
    '                    objDataOperation.ReleaseTransaction(False)
    '                    Return False
    '                End If
    '            End If

    '            'Sohail (21 Aug 2015) -- Start
    '            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
    '            'intEDID = GetEDUnkID(mintEmployeeunkid, intTranHeadID, intPeriodId)
    '            intEDID = GetEDUnkID(strDatabaseName, mintEmployeeunkid, intTranHeadID, intPeriodId, objDataOperation)
    '            'Sohail (21 Aug 2015) -- End

    '            If intEDID > 0 Then
    '                'Sohail (21 Aug 2015) -- Start
    '                'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
    '                'Me._Edunkid = intEDID
    '                Me._Edunkid(objDataOperation, strDatabaseName) = intEDID
    '                'Sohail (21 Aug 2015) -- End

    '                If blnOverWrite = True Then
    '                    mdecAmount = decAmount
    '                    mintCurrencyunkid = intCurrencyID
    '                    mintVendorunkid = intVendorID
    '                    mintMembership_Categoryunkid = 0
    '                    mintCostCenterUnkId = intCostCenterID
    '                    mstrMedicalrefno = strMedicalRefNo
    '                    'Sohail (30 Mar 2013) -- Start
    '                    'TRA - ENHANCEMENT
    '                    mintMembershipTranUnkid = CInt(dtRow.Item("membershiptranunkid"))
    '                    mintDisciplinefileunkid = CInt(dtRow.Item("disciplinefileunkid"))
    '                    'Sohail (30 Mar 2013) -- End
    '                    'Sohail (09 Nov 2013) -- Start
    '                    'TRA - ENHANCEMENT
    '                    mdtCumulative_Startdate = dtCumulative_Startdate
    '                    mdtStop_Date = dtStop_date
    '                    'Sohail (09 Nov 2013) -- End
    '                    mintEmpbatchpostingunkid = intEmpbatchpostingunkid 'Sohail (24 Feb 2016)

    '                    mintUserunkid = IIf(intUserUnkId = 0, User._Object._Userunkid, intUserUnkId)

    '                    'Sohail (21 Aug 2015) -- Start
    '                    'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
    '                    'blnFlag = Update(True)
    '                    blnFlag = Update(strDatabaseName, True, objDataOperation, dtCurrentDateAndTime)
    '                    'Sohail (21 Aug 2015) -- End
    '                Else
    '                    blnFlag = True
    '                End If

    '            Else
    '                mintTranheadunkid = intTranHeadID
    '                'Sohail (21 Aug 2015) -- Start
    '                'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
    '                'objTranHead._Tranheadunkid = intTranHeadID
    '                objTranHead._Tranheadunkid(strDatabaseName) = intTranHeadID
    '                'Sohail (21 Aug 2015) -- End
    '                mintBatchtransactionunkid = intBatchID

    '                mintTrnheadtype_Id = objTranHead._Trnheadtype_Id
    '                mintTypeof_Id = objTranHead._Typeof_id
    '                mintCalctype_Id = objTranHead._Calctype_Id
    '                mintComputeon_Id = objTranHead._Computeon_Id
    '                mstrFormula = objTranHead._Formula
    '                mstrFormulaid = objTranHead._Formulaid
    '                mdecAmount = decAmount

    '                If objTranHead._Trnheadtype_Id = enTranHeadType.EarningForEmployees Then
    '                    mblnIsdeduct = False
    '                ElseIf objTranHead._Trnheadtype_Id = enTranHeadType.EmployersStatutoryContributions OrElse objTranHead._Trnheadtype_Id = enTranHeadType.Informational Then
    '                    mblnIsdeduct = Nothing
    '                Else
    '                    mblnIsdeduct = True
    '                End If

    '                mintCurrencyunkid = intCurrencyID
    '                mintVendorunkid = intVendorID
    '                mintUserunkid = IIf(intUserUnkId = 0, User._Object._Userunkid, intUserUnkId)
    '                mblnIsvoid = False
    '                mintVoiduserunkid = -1
    '                mdtVoiddatetime = Nothing
    '                mstrVoidreason = ""
    '                mintMembership_Categoryunkid = 0

    '                'Sohail (21 Aug 2015) -- Start
    '                'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
    '                'If strAllowToApproveEarningDeduction = "" Then strAllowToApproveEarningDeduction = User._Object.Privilege._AllowToApproveEarningDeduction.ToString()
    '                'If CBool(strAllowToApproveEarningDeduction) = True Then
    '                If blnAllowToApproveEarningDeduction = True Then
    '                    'Sohail (21 Aug 2015) -- End
    '                    mblnIsapproved = True
    '                    mintApproveruserunkid = IIf(intUserUnkId = 0, User._Object._Userunkid, intUserUnkId)
    '                Else
    '                    mblnIsapproved = False
    '                    mintApproveruserunkid = -1
    '                End If
    '                mintPeriodunkid = intPeriodId
    '                mintCostCenterUnkId = intCostCenterID
    '                mstrMedicalrefno = strMedicalRefNo
    '                'Sohail (09 Nov 2013) -- Start
    '                'TRA - ENHANCEMENT
    '                mintMembershipTranUnkid = CInt(dtRow.Item("membershiptranunkid"))
    '                mintDisciplinefileunkid = CInt(dtRow.Item("disciplinefileunkid"))
    '                mdtCumulative_Startdate = dtCumulative_Startdate
    '                mdtStop_Date = dtStop_date
    '                'Sohail (09 Nov 2013) -- End
    '                mintEmpbatchpostingunkid = intEmpbatchpostingunkid 'Sohail (24 Feb 2016)

    '                'Sohail (21 Aug 2015) -- Start
    '                'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
    '                'blnFlag = Insert(True)
    '                'Sohail (20 Jul 2018) -- Start
    '                'Support Issue # 0002383 : VOLTAMP: Unable to assign Transaction heads using batch entry in Payroll transactions (Warning message of Membership heads not assigned)in 73.1.
    '                'blnFlag = Insert(strDatabaseName, intUserUnkId, xYearUnkid, xCompanyUnkid, xPeriodStart, xPeriodEnd, xUserModeSetting, xOnlyApproved, xIncludeIn_ActiveEmployee, True, dtCurrentDateAndTime, , objDataOperation, blnApplyUserAccessFilter, strFilerString)
    '                blnFlag = Insert(strDatabaseName, intUserUnkId, xYearUnkid, xCompanyUnkid, xPeriodStart, xPeriodEnd, xUserModeSetting, xOnlyApproved, xIncludeIn_ActiveEmployee, True, dtCurrentDateAndTime, , objDataOperation, blnApplyUserAccessFilter, strFilerString, True, blnAllowToApproveEarningDeduction)
    '                'Sohail (20 Jul 2018) -- End
    '                'Sohail (21 Aug 2015) -- End
    '            End If

    '            If blnFlag = False Then
    '                objDataOperation.ReleaseTransaction(False)
    '                Return False
    '            End If

    '        Next

    '        '**** Copy Previous ED Slab
    '        If blnCopyPreviousEDSlab = True AndAlso dicEmpList.Count > 0 Then

    '            For Each pair In dicEmpList
    '                arrEmp = pair.Value

    '                mintEmployeeunkid = arrEmp(0) 'employeeunkid
    '                intPeriodId = arrEmp(1) 'periodunkid
    '                mdtPeriod_startdate = arrEmp(2) 'start_date
    '                mdtPeriod_enddate = arrEmp(3) 'end_date

    '                'Sohail (21 Aug 2015) -- Start
    '                'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
    '                'Call GetEmployeeED()
    '                'Sohail (01 Mar 2018) -- Start
    '                'Internal Issue : ExecureReader requires the command to have a transaction... in 70.1.
    '                'Call GetEmployeeED(strDatabaseName)
    '                Call GetEmployeeED(objDataOperation, strDatabaseName)
    '                'Sohail (01 Mar 2018) -- End
    '                'Sohail (21 Aug 2015) -- End

    '                Dim strPrevEndDate As String = ""
    '                Dim drRow As DataRow()

    '                'Sohail (09 Nov 2013) -- Start
    '                'TRA - ENHANCEMENT
    '                'drRow = mdtED.Select("AUD = '' AND end_date <= '" & eZeeDate.convertDate(mdtPeriod_startdate) & "' AND isrecurrent = 1 ", "end_date DESC")
    '                drRow = mdtED.Select("AUD = '' AND end_date <= '" & eZeeDate.convertDate(mdtPeriod_startdate) & "' AND isrecurrent = 1 AND (stop_date IS NULL OR stop_date >= '" & mdtPeriod_startdate & "') ", "end_date DESC")
    '                'Sohail (09 Nov 2013) -- End

    '                If drRow.Length > 0 Then
    '                    For Each dRow As DataRow In drRow
    '                        If strPrevEndDate <> "" AndAlso strPrevEndDate <> dRow.Item("end_date").ToString Then
    '                            Exit For
    '                        End If

    '                        'Sohail (21 Aug 2015) -- Start
    '                        'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
    '                        'objTranHead._Tranheadunkid = CInt(dRow.Item("tranheadunkid"))
    '                        objTranHead._Tranheadunkid(strDatabaseName) = CInt(dRow.Item("tranheadunkid"))
    '                        'Sohail (21 Aug 2015) -- End
    '                        'Sohail (24 Feb 2016) -- Start
    '                        'Enhancement - Batch Posting changes as per online doc in 58.1 SP. (#15 (KBC & Kenya Project Comments List.xls))
    '                        'If objTranHead._Typeof_id = enTypeOf.Salary Then
    '                        If objTranHead._Typeof_id = enTypeOf.Salary OrElse dtTable.Select("tranheadunkid = " & CInt(dRow.Item("tranheadunkid")) & " ").Length > 0 Then
    '                            'Sohail (24 Feb 2016) -- End
    '                            strPrevEndDate = dRow.Item("end_date").ToString
    '                            Continue For
    '                        End If

    '                        '*** If this head is already inserted from above datatable by passing datatable from Form by entering data by user.
    '                        Dim dtRow() As DataRow = dtTable.Select("periodunkid = " & intPeriodId & " AND employeeunkid = " & mintEmployeeunkid & " AND tranheadunkid = " & CInt(dRow.Item("tranheadunkid")) & " ")
    '                        If dtRow.Length > 0 Then
    '                            strPrevEndDate = dRow.Item("end_date").ToString
    '                            Continue For
    '                        End If

    '                        'Sohail (21 Aug 2015) -- Start
    '                        'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
    '                        'intEDID = GetEDUnkID(mintEmployeeunkid, CInt(dRow.Item("tranheadunkid")), intPeriodId)
    '                        intEDID = GetEDUnkID(strDatabaseName, mintEmployeeunkid, CInt(dRow.Item("tranheadunkid")), intPeriodId, objDataOperation)
    '                        'Sohail (21 Aug 2015) -- End

    '                        If intEDID > 0 Then
    '                            'Sohail (21 Aug 2015) -- Start
    '                            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
    '                            'Me._Edunkid = intEDID
    '                            Me._Edunkid(objDataOperation, strDatabaseName) = intEDID
    '                            'Sohail (21 Aug 2015) -- End
    '                            If blnOverwritePreviousEDSlabHeads = True Then
    '                                mdecAmount = CDec(dRow.Item("amount"))
    '                                mintCurrencyunkid = CInt(dRow.Item("currencyid"))
    '                                mintVendorunkid = CInt(dRow.Item("vendorid"))
    '                                If IsDBNull(dRow.Item("membership_categoryunkid")) Then
    '                                    mintMembership_Categoryunkid = 0
    '                                Else
    '                                    mintMembership_Categoryunkid = CInt(dRow.Item("membership_categoryunkid"))
    '                                End If
    '                                mintCostCenterUnkId = Int(dRow.Item("costcenterunkid"))
    '                                mstrMedicalrefno = dRow.Item("medicalrefno").ToString
    '                                mintUserunkid = IIf(intUserUnkId = 0, User._Object._Userunkid, intUserUnkId)
    '                                'Sohail (20 Feb 2013) -- Start
    '                                'TRA - ENHANCEMENT
    '                                mintMembershipTranUnkid = CInt(dRow.Item("membershiptranunkid"))
    '                                mintDisciplinefileunkid = CInt(dRow.Item("disciplinefileunkid"))
    '                                'Sohail (20 Feb 2013) -- End
    '                                'Sohail (09 Nov 2013) -- Start
    '                                'TRA - ENHANCEMENT
    '                                If IsDBNull(dRow.Item("cumulative_startdate")) = True Then
    '                                    mdtCumulative_Startdate = Nothing
    '                                Else
    '                                    mdtCumulative_Startdate = dRow.Item("cumulative_startdate")
    '                                End If
    '                                If IsDBNull(dRow.Item("stop_date")) = True Then
    '                                    mdtStop_Date = Nothing
    '                                Else
    '                                    mdtStop_Date = dRow.Item("stop_date")
    '                                End If
    '                                'Sohail (09 Nov 2013) -- End
    '                                mintEmpbatchpostingunkid = 0 'Sohail (24 Feb 2016)

    '                                'Sohail (21 Aug 2015) -- Start
    '                                'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
    '                                'blnFlag = Update(True)
    '                                blnFlag = Update(strDatabaseName, True, objDataOperation, dtCurrentDateAndTime)
    '                                'Sohail (21 Aug 2015) -- End
    '                            Else
    '                                blnFlag = True
    '                            End If
    '                        Else
    '                            mintTranheadunkid = CInt(dRow.Item("tranheadunkid"))
    '                            'Sohail (21 Aug 2015) -- Start
    '                            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
    '                            'objTranHead._Tranheadunkid = CInt(dRow.Item("tranheadunkid"))
    '                            objTranHead._Tranheadunkid(strDatabaseName) = CInt(dRow.Item("tranheadunkid"))
    '                            'Sohail (21 Aug 2015) -- End
    '                            mintBatchtransactionunkid = CInt(dRow.Item("batchtransactionunkid"))

    '                            mintTrnheadtype_Id = objTranHead._Trnheadtype_Id
    '                            mintTypeof_Id = objTranHead._Typeof_id
    '                            mintCalctype_Id = objTranHead._Calctype_Id
    '                            mintComputeon_Id = objTranHead._Computeon_Id
    '                            mstrFormula = objTranHead._Formula
    '                            mstrFormulaid = objTranHead._Formulaid
    '                            mdecAmount = CDec(dRow.Item("amount"))

    '                            If objTranHead._Trnheadtype_Id = enTranHeadType.EarningForEmployees Then
    '                                mblnIsdeduct = False
    '                            ElseIf objTranHead._Trnheadtype_Id = enTranHeadType.EmployersStatutoryContributions OrElse objTranHead._Trnheadtype_Id = enTranHeadType.Informational Then
    '                                mblnIsdeduct = Nothing
    '                            Else
    '                                mblnIsdeduct = True
    '                            End If

    '                            mintCurrencyunkid = CInt(dRow.Item("currencyid"))
    '                            mintVendorunkid = CInt(dRow.Item("vendorid"))
    '                            mintUserunkid = IIf(intUserUnkId = 0, User._Object._Userunkid, intUserUnkId)
    '                            mblnIsvoid = False
    '                            mintVoiduserunkid = -1
    '                            mdtVoiddatetime = Nothing
    '                            mstrVoidreason = ""
    '                            mintMembership_Categoryunkid = 0
    '                            'Sohail (21 Aug 2015) -- Start
    '                            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
    '                            'If strAllowToApproveEarningDeduction = "" Then strAllowToApproveEarningDeduction = User._Object.Privilege._AllowToApproveEarningDeduction.ToString()
    '                            'If CBool(strAllowToApproveEarningDeduction) = True Then
    '                            If blnAllowToApproveEarningDeduction = True Then
    '                                'Sohail (21 Aug 2015) -- End
    '                                mblnIsapproved = True
    '                                mintApproveruserunkid = IIf(intUserUnkId = 0, User._Object._Userunkid, intUserUnkId)
    '                            Else
    '                                mblnIsapproved = False
    '                                mintApproveruserunkid = -1
    '                            End If
    '                            mintPeriodunkid = intPeriodId
    '                            mintCostCenterUnkId = 0
    '                            mstrMedicalrefno = ""
    '                            'Sohail (20 Feb 2013) -- Start
    '                            'TRA - ENHANCEMENT
    '                            mintMembershipTranUnkid = CInt(dRow.Item("membershiptranunkid"))
    '                            mintDisciplinefileunkid = CInt(dRow.Item("disciplinefileunkid"))
    '                            'Sohail (20 Feb 2013) -- End
    '                            'Sohail (09 Nov 2013) -- Start
    '                            'TRA - ENHANCEMENT
    '                            If IsDBNull(dRow.Item("cumulative_startdate")) = True Then
    '                                mdtCumulative_Startdate = Nothing
    '                            Else
    '                                mdtCumulative_Startdate = dRow.Item("cumulative_startdate")
    '                            End If
    '                            If IsDBNull(dRow.Item("stop_date")) = True Then
    '                                mdtStop_Date = Nothing
    '                            Else
    '                                mdtStop_Date = dRow.Item("stop_date")
    '                            End If
    '                            'Sohail (09 Nov 2013) -- End
    '                            mintEmpbatchpostingunkid = 0 'Sohail (24 Feb 2016)

    '                            'Sohail (21 Aug 2015) -- Start
    '                            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
    '                            'blnFlag = Insert(True)
    '                            'Sohail (20 Jul 2018) -- Start
    '                            'Support Issue # 0002383 : VOLTAMP: Unable to assign Transaction heads using batch entry in Payroll transactions (Warning message of Membership heads not assigned)in 73.1.
    '                            'blnFlag = Insert(strDatabaseName, intUserUnkId, xYearUnkid, xCompanyUnkid, xPeriodStart, xPeriodEnd, xUserModeSetting, xOnlyApproved, xIncludeIn_ActiveEmployee, True, dtCurrentDateAndTime, , objDataOperation, blnApplyUserAccessFilter, strFilerString)
    '                            blnFlag = Insert(strDatabaseName, intUserUnkId, xYearUnkid, xCompanyUnkid, xPeriodStart, xPeriodEnd, xUserModeSetting, xOnlyApproved, xIncludeIn_ActiveEmployee, True, dtCurrentDateAndTime, , objDataOperation, blnApplyUserAccessFilter, strFilerString, False, blnAllowToApproveEarningDeduction)
    '                            'Sohail (20 Jul 2018) -- End
    '                            'Sohail (21 Aug 2015) -- End
    '                        End If

    '                        If blnFlag = False Then
    '                            objDataOperation.ReleaseTransaction(False)
    '                            Return False
    '                        End If

    '                        strPrevEndDate = dRow.Item("end_date").ToString
    '                    Next
    '                End If
    '            Next
    '        End If

    '        objDataOperation.ReleaseTransaction(True)

    '        Return True
    '    Catch ex As Exception
    '        objDataOperation.ReleaseTransaction(False)
    '        Throw New Exception(ex.Message & "; Procedure Name: InsertAllByDataTable; Module Name: " & mstrModuleName)
    '        Return False
    '    Finally
    '        exForce = Nothing
    '        If dsList IsNot Nothing Then dsList.Dispose()
    '        objTranHead = Nothing
    '        'objExRate = Nothing
    '        objDataOperation = Nothing
    '    End Try
    'End Function
    'Sohail (05 Dec 2012) -- End
    Public Function InsertAllByDataTable(ByVal strDatabaseName As String _
                                         , ByVal xYearUnkid As Integer _
                                         , ByVal xCompanyUnkid As Integer _
                                         , ByVal xPeriodStart As DateTime _
                                         , ByVal xPeriodEnd As DateTime _
                                         , ByVal xUserModeSetting As String _
                                         , ByVal xOnlyApproved As Boolean _
                                         , ByVal xIncludeIn_ActiveEmployee As Boolean _
                                         , ByVal dtTable As DataTable _
                                         , ByVal blnOverWrite As Boolean _
                                         , ByVal intUserUnkId As Integer _
                                         , ByVal blnAllowToApproveEarningDeduction As Boolean _
                                         , ByVal dtCurrentDateAndTime As Date _
                                         , ByVal xDataOp As clsDataOperation _
                                         , Optional ByVal blnInsertSalaryHead As Boolean = False _
                                         , Optional ByVal blnCopyPreviousEDSlab As Boolean = False _
                                         , Optional ByVal blnOverwritePreviousEDSlabHeads As Boolean = False _
                                         , Optional ByVal blnApplyUserAccessFilter As Boolean = True _
                                         , Optional ByVal strFilerString As String = "" _
                                         , Optional ByVal blnSkipInsertIfExist As Boolean = False _
                                         ) As Boolean
        'Sohail (25 Jun 2020) - [blnSkipInsertIfExist]

        Dim dsList As DataSet = Nothing
        Dim dsHead As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim blnFlag As Boolean
        Dim intBatchID As Integer
        Dim intTranHeadID As Integer
        Dim decAmount As Decimal
        Dim intCurrencyID As Integer
        Dim intVendorID As Integer
        Dim intPeriodId As Integer
        Dim intCostCenterID As Integer
        Dim strMedicalRefNo As String
        Dim dtCumulative_Startdate As Date
        Dim dtStop_date As Date
        'Sohail (24 Aug 2019) -- Start
        'NMB Payroll UAT # TC016 - 76.1 - System should be able to accept future amendments (Future period) and prorate. It should be possible to set start date of the specific transaction and prorate based on the start date. It should also be possible to set start date of the future period.
        Dim dtEDStart_date As Date
        'Sohail (24 Aug 2019) -- End
        Dim objTranHead As New clsTransactionHead
        Dim dicEmpList As New Dictionary(Of String, ArrayList)
        Dim arrEmp As ArrayList
        Dim mdtPeriod_startdate As Date
        Dim mdtPeriod_enddate As Date
        Dim intEmpbatchpostingunkid As Integer

        If xDataOp IsNot Nothing Then
            objDataOperation = xDataOp
        Else
        objDataOperation = New clsDataOperation
        objDataOperation.BindTransaction()
        End If
        objDataOperation.ClearParameters()

        Try

            'Sohail (18 Jan 2019) -- Start
            'Issue - 74.1 - Bind Transaction issue.
            'dsHead = objTranHead.GetList(strDatabaseName, -1, -1, False, True, 0, False, "", True, True, True, 2)
            'Sohail (25 Jun 2020) -- Start
            'NMB Enhancement # : Performance enhancement on Import earning deduction heads.
            'dsHead = objTranHead.GetList(strDatabaseName, -1, -1, False, True, 0, False, "", True, True, True, 2, objDataOperation)
            dsHead = objTranHead.GetList(strDatabaseName, -1, -1, False, True, 0, False, "", True, True, True, 2, objDataOperation, xPeriodEnd)
            Dim strEmpIDs As String = String.Join(",", (From p In dtTable Select (p.Item("employeeunkid").ToString)).Distinct.ToArray)
            'Sohail (25 Jun 2020) -- End
            'Sohail (18 Jan 2019) -- End

            For Each dtRow As DataRow In dtTable.Rows

                objDataOperation.ClearParameters()

                mintEmployeeunkid = CInt(dtRow.Item("employeeunkid").ToString)
                intTranHeadID = CInt(dtRow.Item("tranheadunkid").ToString)
                intBatchID = CInt(dtRow.Item("batchtransactionunkid").ToString)
                intCurrencyID = CInt(dtRow.Item("currencyid").ToString)
                decAmount = CDec(dtRow.Item("Amount").ToString)
                intVendorID = CInt(dtRow.Item("vendorid").ToString)
                intPeriodId = CInt(dtRow.Item("periodunkid").ToString)
                intCostCenterID = CInt(dtRow.Item("costcenterunkid").ToString)
                strMedicalRefNo = dtRow.Item("medicalrefno").ToString
                mdtPeriod_startdate = eZeeDate.convertDate(dtRow.Item("start_date").ToString)
                mdtPeriod_enddate = eZeeDate.convertDate(dtRow.Item("end_date").ToString)
                If IsDBNull(dtRow.Item("cumulative_startdate")) = True Then
                    dtCumulative_Startdate = Nothing
                Else
                    dtCumulative_Startdate = dtRow.Item("cumulative_startdate")
                End If
                If IsDBNull(dtRow.Item("stop_date")) = True Then
                    dtStop_date = Nothing
                Else
                    dtStop_date = dtRow.Item("stop_date")
                End If
                'Sohail (24 Aug 2019) -- Start
                'NMB Payroll UAT # TC016 - 76.1 - System should be able to accept future amendments (Future period) and prorate. It should be possible to set start date of the specific transaction and prorate based on the start date. It should also be possible to set start date of the future period.
                If IsDBNull(dtRow.Item("edstart_date")) = True Then
                    dtEDStart_date = Nothing
                Else
                    dtEDStart_date = dtRow.Item("edstart_date")
                End If
                'Sohail (24 Aug 2019) -- End
                intEmpbatchpostingunkid = CInt(dtRow.Item("empbatchpostingunkid").ToString)

                If dicEmpList.ContainsKey(mintEmployeeunkid.ToString & "|" & intPeriodId.ToString) = False Then
                    arrEmp = New ArrayList
                    arrEmp.Add(mintEmployeeunkid)
                    arrEmp.Add(intPeriodId)
                    arrEmp.Add(mdtPeriod_startdate)
                    arrEmp.Add(mdtPeriod_enddate)
                    dicEmpList.Add(mintEmployeeunkid.ToString & "|" & intPeriodId.ToString, arrEmp)
                End If

                If blnInsertSalaryHead = True Then
                    blnFlag = Insert_SalaryHead(strDatabaseName, intUserUnkId, xYearUnkid, xCompanyUnkid, xPeriodStart, xPeriodEnd, xUserModeSetting, xOnlyApproved, xIncludeIn_ActiveEmployee, mintEmployeeunkid, intPeriodId, dtCurrentDateAndTime, objDataOperation, blnApplyUserAccessFilter, strFilerString)

                    If blnFlag = False Then
                        objDataOperation.ReleaseTransaction(False)
                        Return False
                    End If
                End If

                'Sohail (25 Jun 2020) -- Start
                'NMB Enhancement # : Performance enhancement on Import earning deduction heads.
                'Call GetEmployeeED(objDataOperation, strDatabaseName)

                'Dim drED() As DataRow = mdtED.Select("employeeunkid = " & mintEmployeeunkid & " AND tranheadunkid = " & intTranHeadID & " AND periodunkid = " & intPeriodId & " ")

                'If drED.Length > 0 Then
                '    Call GetData(drED(0))
                If GetDataIfExist(objDataOperation, strDatabaseName, mintEmployeeunkid, intTranHeadID, intPeriodId) = True Then
                    'Sohail (25 Jun 2020) -- End

                    If blnOverWrite = True Then
                        mdecAmount = decAmount
                        mintCurrencyunkid = intCurrencyID
                        mintVendorunkid = intVendorID
                        mintMembership_Categoryunkid = 0
                        mintCostCenterUnkId = intCostCenterID
                        mstrMedicalrefno = strMedicalRefNo
                        mintMembershipTranUnkid = CInt(dtRow.Item("membershiptranunkid"))
                        mintDisciplinefileunkid = CInt(dtRow.Item("disciplinefileunkid"))
                        mdtCumulative_Startdate = dtCumulative_Startdate
                        mdtStop_Date = dtStop_date
                        'Sohail (24 Aug 2019) -- Start
                        'NMB Payroll UAT # TC016 - 76.1 - System should be able to accept future amendments (Future period) and prorate. It should be possible to set start date of the specific transaction and prorate based on the start date. It should also be possible to set start date of the future period.
                        mdtEDStart_Date = dtEDStart_date
                        'Sohail (24 Aug 2019) -- End
                        mintEmpbatchpostingunkid = intEmpbatchpostingunkid

                        mintUserunkid = IIf(intUserUnkId = 0, User._Object._Userunkid, intUserUnkId)

                        'Sohail (02 Sep 2019) -- Start
                        'Mainspring Resourcing Ghana Ltd Enhancement - 76.1 - # 0004098: Approval Flow for Flat Rate Heads Amount Uploaded.
                        '1. Set ED in pending status if user has not priviledge of ED approval
                        '2. Perform Global approver with option to filter for allocation and approved any posted (new /edited) E & D values in MSS
                        '3. Send notifications to ED approvers for all employees ED in one email
                        If blnAllowToApproveEarningDeduction = True Then
                            mblnIsapproved = True
                            mintApproveruserunkid = mintUserunkid
                        Else
                            mblnIsapproved = False
                            mintApproveruserunkid = -1
                        End If
                        'Sohail (02 Sep 2019) -- End

                        blnFlag = Update(strDatabaseName, True, objDataOperation, dtCurrentDateAndTime)
                    Else
                        blnFlag = True
                    End If

                Else
                    mintTranheadunkid = intTranHeadID

                    Dim drHead() As DataRow = dsHead.Tables(0).Select("tranheadunkid = " & intTranHeadID & " ")
                    mintBatchtransactionunkid = intBatchID

                    If drHead.Length <= 0 Then
                        mstrMessage = Language.getMessage(mstrModuleName, 6, "Transaction head doees not exist.") & "[" & intTranHeadID & "]"
                        objDataOperation.ReleaseTransaction(False)
                        Return False
                    Else

                        mintTrnheadtype_Id = CInt(drHead(0).Item("trnheadtype_id"))
                        mintTypeof_Id = CInt(drHead(0).Item("typeof_id"))
                        mintCalctype_Id = CInt(drHead(0).Item("calctype_id"))
                        mintComputeon_Id = CInt(drHead(0).Item("computeon_id"))
                        mstrFormula = drHead(0).Item("formula").ToString
                        mstrFormulaid = drHead(0).Item("formulaid")
                    mdecAmount = decAmount

                        If CInt(drHead(0).Item("trnheadtype_id")) = CInt(enTranHeadType.EarningForEmployees) Then 'enTranHeadType.EarningForEmployees
                        mblnIsdeduct = False
                        ElseIf CInt(drHead(0).Item("trnheadtype_id")) = CInt(enTranHeadType.EmployersStatutoryContributions) OrElse CInt(drHead(0).Item("trnheadtype_id")) = CInt(enTranHeadType.Informational) Then 'enTranHeadType.EmployersStatutoryContributions , Informational
                        mblnIsdeduct = Nothing
                    Else
                        mblnIsdeduct = True
                    End If

                    mintCurrencyunkid = intCurrencyID
                    mintVendorunkid = intVendorID
                    mintUserunkid = IIf(intUserUnkId = 0, User._Object._Userunkid, intUserUnkId)
                    mblnIsvoid = False
                    mintVoiduserunkid = -1
                    mdtVoiddatetime = Nothing
                    mstrVoidreason = ""
                    mintMembership_Categoryunkid = 0

                    If blnAllowToApproveEarningDeduction = True Then
                        mblnIsapproved = True
                        mintApproveruserunkid = IIf(intUserUnkId = 0, User._Object._Userunkid, intUserUnkId)
                    Else
                        mblnIsapproved = False
                        mintApproveruserunkid = -1
                    End If
                    mintPeriodunkid = intPeriodId
                    mintCostCenterUnkId = intCostCenterID
                    mstrMedicalrefno = strMedicalRefNo
                    mintMembershipTranUnkid = CInt(dtRow.Item("membershiptranunkid"))
                    mintDisciplinefileunkid = CInt(dtRow.Item("disciplinefileunkid"))
                    mdtCumulative_Startdate = dtCumulative_Startdate
                    mdtStop_Date = dtStop_date
                        'Sohail (24 Aug 2019) -- Start
                        'NMB Payroll UAT # TC016 - 76.1 - System should be able to accept future amendments (Future period) and prorate. It should be possible to set start date of the specific transaction and prorate based on the start date. It should also be possible to set start date of the future period.
                        mdtEDStart_Date = dtEDStart_date
                        'Sohail (24 Aug 2019) -- End
                        mintEmpbatchpostingunkid = intEmpbatchpostingunkid

                        'Sohail (25 Jun 2020) -- Start
                        'NMB Enhancement # : Performance enhancement on Import earning deduction heads.
                        'blnFlag = Insert(strDatabaseName, intUserUnkId, xYearUnkid, xCompanyUnkid, xPeriodStart, xPeriodEnd, xUserModeSetting, xOnlyApproved, xIncludeIn_ActiveEmployee, True, dtCurrentDateAndTime, , objDataOperation, blnApplyUserAccessFilter, strFilerString, True, blnAllowToApproveEarningDeduction)
                        blnFlag = Insert(strDatabaseName, intUserUnkId, xYearUnkid, xCompanyUnkid, xPeriodStart, xPeriodEnd, xUserModeSetting, xOnlyApproved, xIncludeIn_ActiveEmployee, True, dtCurrentDateAndTime, , objDataOperation, blnApplyUserAccessFilter, strFilerString, True, blnAllowToApproveEarningDeduction, blnSkipInsertIfExist)
                        'Sohail (25 Jun 2020) -- End

                    End If
                End If

                If blnFlag = False Then
                    objDataOperation.ReleaseTransaction(False)
                    Return False
                End If

            Next

            '**** Copy Previous ED Slab
            If blnCopyPreviousEDSlab = True AndAlso dicEmpList.Count > 0 Then

                'Sohail (25 Jun 2020) -- Start
                'NMB Enhancement # : Performance enhancement on Import earning deduction heads.
                mintEmployeeunkid = 0
                Call GetEmployeeED(objDataOperation, strDatabaseName, strEmpIDs, True, xPeriodEnd)
                'Sohail (25 Jun 2020) -- End

                For Each pair In dicEmpList
                    arrEmp = pair.Value

                    mintEmployeeunkid = arrEmp(0) 'employeeunkid
                    intPeriodId = arrEmp(1) 'periodunkid
                    mdtPeriod_startdate = arrEmp(2) 'start_date
                    mdtPeriod_enddate = arrEmp(3) 'end_date

                    'Sohail (25 Jun 2020) -- Start
                    'NMB Enhancement # : Performance enhancement on Import earning deduction heads.
                    'Call GetEmployeeED(objDataOperation, strDatabaseName)
                    'Sohail (25 Jun 2020) -- End

                    Dim strPrevEndDate As String = ""
                    Dim drRow As DataRow()

                    'Sohail (24 Aug 2019) -- Start
                    'NMB Payroll UAT # TC016 - 76.1 - System should be able to accept future amendments (Future period) and prorate. It should be possible to set start date of the specific transaction and prorate based on the start date. It should also be possible to set start date of the future period.
                    'drRow = mdtED.Select("AUD = '' AND end_date <= '" & eZeeDate.convertDate(mdtPeriod_startdate) & "' AND isrecurrent = 1 AND (stop_date IS NULL OR stop_date >= '" & mdtPeriod_startdate & "') ", "end_date DESC")
                    'Sohail (25 Jun 2020) -- Start
                    'NMB Enhancement # : Performance enhancement on Import earning deduction heads.
                    'drRow = mdtED.Select("AUD = '' AND end_date <= '" & eZeeDate.convertDate(mdtPeriod_startdate) & "' AND isrecurrent = 1 AND (stop_date IS NULL OR stop_date >= '" & mdtPeriod_startdate & "') AND (edstart_date IS NULL OR edstart_date <= '" & mdtPeriod_enddate & "') ", "end_date DESC")
                    drRow = mdtED.Select("AUD = '' AND employeeunkid = " & mintEmployeeunkid & " AND end_date <= '" & eZeeDate.convertDate(mdtPeriod_startdate) & "' AND isrecurrent = 1 AND (stop_date IS NULL OR stop_date >= '" & mdtPeriod_startdate & "') AND (edstart_date IS NULL OR edstart_date <= '" & mdtPeriod_enddate & "') ", "end_date DESC")
                    'Sohail (25 Jun 2020) -- End
                    'Sohail (24 Aug 2019) -- End

                    If drRow.Length > 0 Then
                        For Each dRow As DataRow In drRow
                            If strPrevEndDate <> "" AndAlso strPrevEndDate <> dRow.Item("end_date").ToString Then
                                Exit For
                            End If

                            Dim drHead() As DataRow = dsHead.Tables(0).Select("tranheadunkid = " & CInt(dRow.Item("tranheadunkid")) & " ")
                            If drHead.Length <= 0 Then
                                strPrevEndDate = dRow.Item("end_date").ToString
                                Continue For
                            Else
                                If CInt(drHead(0).Item("typeof_id")) = enTypeOf.Salary OrElse dtTable.Select("tranheadunkid = " & CInt(dRow.Item("tranheadunkid")) & " ").Length > 0 Then
                                strPrevEndDate = dRow.Item("end_date").ToString
                                Continue For
                            End If
                            End If

                            '*** If this head is already inserted from above datatable by passing datatable from Form by entering data by user.
                            Dim dtRow() As DataRow = dtTable.Select("periodunkid = " & intPeriodId & " AND employeeunkid = " & mintEmployeeunkid & " AND tranheadunkid = " & CInt(dRow.Item("tranheadunkid")) & " ")
                            If dtRow.Length > 0 Then
                                strPrevEndDate = dRow.Item("end_date").ToString
                                Continue For
                            End If

                            Dim drED() As DataRow = mdtED.Select("employeeunkid = " & mintEmployeeunkid & " AND tranheadunkid = " & CInt(dRow.Item("tranheadunkid")) & " AND periodunkid = " & intPeriodId & " ")

                            If drED.Length > 0 Then
                                Call GetData(drED(0))

                                If blnOverwritePreviousEDSlabHeads = True Then
                                    mdecAmount = CDec(dRow.Item("amount"))
                                    mintCurrencyunkid = CInt(dRow.Item("currencyid"))
                                    mintVendorunkid = CInt(dRow.Item("vendorid"))
                                    If IsDBNull(dRow.Item("membership_categoryunkid")) Then
                                        mintMembership_Categoryunkid = 0
                                    Else
                                        mintMembership_Categoryunkid = CInt(dRow.Item("membership_categoryunkid"))
                                    End If
                                    mintCostCenterUnkId = Int(dRow.Item("costcenterunkid"))
                                    mstrMedicalrefno = dRow.Item("medicalrefno").ToString
                                    mintUserunkid = IIf(intUserUnkId = 0, User._Object._Userunkid, intUserUnkId)
                                    mintMembershipTranUnkid = CInt(dRow.Item("membershiptranunkid"))
                                    mintDisciplinefileunkid = CInt(dRow.Item("disciplinefileunkid"))
                                    If IsDBNull(dRow.Item("cumulative_startdate")) = True Then
                                        mdtCumulative_Startdate = Nothing
                                    Else
                                        mdtCumulative_Startdate = dRow.Item("cumulative_startdate")
                                    End If
                                    If IsDBNull(dRow.Item("stop_date")) = True Then
                                        mdtStop_Date = Nothing
                                    Else
                                        mdtStop_Date = dRow.Item("stop_date")
                                    End If
                                    'Sohail (24 Aug 2019) -- Start
                                    'NMB Payroll UAT # TC016 - 76.1 - System should be able to accept future amendments (Future period) and prorate. It should be possible to set start date of the specific transaction and prorate based on the start date. It should also be possible to set start date of the future period.
                                    If IsDBNull(dRow.Item("edstart_date")) = True Then
                                        mdtEDStart_Date = Nothing
                                    Else
                                        mdtEDStart_Date = dRow.Item("edstart_date")
                                    End If
                                    'Sohail (24 Aug 2019) -- End
                                    mintEmpbatchpostingunkid = 0

                                    'Sohail (02 Sep 2019) -- Start
                                    'Mainspring Resourcing Ghana Ltd Enhancement - 76.1 - # 0004098: Approval Flow for Flat Rate Heads Amount Uploaded.
                                    '1. Set ED in pending status if user has not priviledge of ED approval
                                    '2. Perform Global approver with option to filter for allocation and approved any posted (new /edited) E & D values in MSS
                                    '3. Send notifications to ED approvers for all employees ED in one email
                                    If blnAllowToApproveEarningDeduction = True Then
                                        mblnIsapproved = True
                                        mintApproveruserunkid = mintUserunkid
                                    Else
                                        mblnIsapproved = False
                                        mintApproveruserunkid = -1
                                    End If
                                    'Sohail (02 Sep 2019) -- End

                                    blnFlag = Update(strDatabaseName, True, objDataOperation, dtCurrentDateAndTime)
                                Else
                                    blnFlag = True
                                End If
                            Else
                                mintTranheadunkid = CInt(dRow.Item("tranheadunkid"))
                                drHead = dsHead.Tables(0).Select("tranheadunkid = " & CInt(dRow.Item("tranheadunkid")) & " ")
                                mintBatchtransactionunkid = CInt(dRow.Item("batchtransactionunkid"))

                                If drHead.Length <= 0 Then
                                    strPrevEndDate = dRow.Item("end_date").ToString
                                    Continue For
                                End If

                                mintTrnheadtype_Id = CInt(drHead(0).Item("trnheadtype_id"))
                                mintTypeof_Id = CInt(drHead(0).Item("typeof_id"))
                                mintCalctype_Id = CInt(drHead(0).Item("calctype_id"))
                                mintComputeon_Id = CInt(drHead(0).Item("computeon_id"))
                                mstrFormula = drHead(0).Item("formula").ToString
                                mstrFormulaid = drHead(0).Item("formulaid")
                                mdecAmount = CDec(dRow.Item("amount"))

                                If CInt(drHead(0).Item("trnheadtype_id")) = enTranHeadType.EarningForEmployees Then
                                    mblnIsdeduct = False
                                ElseIf CInt(drHead(0).Item("trnheadtype_id")) = enTranHeadType.EmployersStatutoryContributions OrElse CInt(drHead(0).Item("trnheadtype_id")) = enTranHeadType.Informational Then
                                    mblnIsdeduct = Nothing
                                Else
                                    mblnIsdeduct = True
                                End If

                                mintCurrencyunkid = CInt(dRow.Item("currencyid"))
                                mintVendorunkid = CInt(dRow.Item("vendorid"))
                                mintUserunkid = IIf(intUserUnkId = 0, User._Object._Userunkid, intUserUnkId)
                                mblnIsvoid = False
                                mintVoiduserunkid = -1
                                mdtVoiddatetime = Nothing
                                mstrVoidreason = ""
                                mintMembership_Categoryunkid = 0
                                If blnAllowToApproveEarningDeduction = True Then
                                    mblnIsapproved = True
                                    mintApproveruserunkid = IIf(intUserUnkId = 0, User._Object._Userunkid, intUserUnkId)
                                Else
                                    mblnIsapproved = False
                                    mintApproveruserunkid = -1
                                End If
                                mintPeriodunkid = intPeriodId
                                mintCostCenterUnkId = 0
                                mstrMedicalrefno = ""
                                mintMembershipTranUnkid = CInt(dRow.Item("membershiptranunkid"))
                                mintDisciplinefileunkid = CInt(dRow.Item("disciplinefileunkid"))
                                If IsDBNull(dRow.Item("cumulative_startdate")) = True Then
                                    mdtCumulative_Startdate = Nothing
                                Else
                                    mdtCumulative_Startdate = dRow.Item("cumulative_startdate")
                                End If
                                If IsDBNull(dRow.Item("stop_date")) = True Then
                                    mdtStop_Date = Nothing
                                Else
                                    mdtStop_Date = dRow.Item("stop_date")
                                End If
                                'Sohail (24 Aug 2019) -- Start
                                'NMB Payroll UAT # TC016 - 76.1 - System should be able to accept future amendments (Future period) and prorate. It should be possible to set start date of the specific transaction and prorate based on the start date. It should also be possible to set start date of the future period.
                                If IsDBNull(dRow.Item("edstart_date")) = True Then
                                    mdtEDStart_Date = Nothing
                                Else
                                    mdtEDStart_Date = dRow.Item("edstart_date")
                                End If
                                'Sohail (24 Aug 2019) -- End
                                mintEmpbatchpostingunkid = 0

                                'Sohail (25 Jun 2020) -- Start
                                'NMB Enhancement # : Performance enhancement on Import earning deduction heads.
                                'blnFlag = Insert(strDatabaseName, intUserUnkId, xYearUnkid, xCompanyUnkid, xPeriodStart, xPeriodEnd, xUserModeSetting, xOnlyApproved, xIncludeIn_ActiveEmployee, True, dtCurrentDateAndTime, , objDataOperation, blnApplyUserAccessFilter, strFilerString, False, blnAllowToApproveEarningDeduction)
                                blnFlag = Insert(strDatabaseName, intUserUnkId, xYearUnkid, xCompanyUnkid, xPeriodStart, xPeriodEnd, xUserModeSetting, xOnlyApproved, xIncludeIn_ActiveEmployee, True, dtCurrentDateAndTime, , objDataOperation, blnApplyUserAccessFilter, strFilerString, False, blnAllowToApproveEarningDeduction, blnSkipInsertIfExist)
                                'Sohail (25 Jun 2020) -- End
                            End If

                            If blnFlag = False Then
                                objDataOperation.ReleaseTransaction(False)
                                Return False
                            End If

                            strPrevEndDate = dRow.Item("end_date").ToString
                        Next
                    End If
                Next
            End If

            If xDataOp Is Nothing Then objDataOperation.ReleaseTransaction(True)

            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: InsertAllByDataTable; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objTranHead = Nothing
            If xDataOp Is Nothing Then objDataOperation = Nothing
        End Try
    End Function
    'Sohail (15 Dec 2018) -- End

    'Sohail (12 Dec 2015) -- Start
    'Enhancement - Showing Progress of closing period.
    Public Function UpdateNonRecurrentHeadOnED(ByVal xDataOp As clsDataOperation _
                                                , ByVal strDatabaseName As String _
                                                , ByVal intPeriodID As Integer _
                                                , ByVal intUserunkid As Integer _
                                                , ByVal dtCurrentDateAndTime As Date _
                                                , ByVal strWebClientID As String _
                                                , ByVal strWebhostName As String _
                                                , ByVal intLogEmployeeUnkid As Integer _
                                                , ByVal strWebFormName As String _
                                                , ByVal sModuleName1 As String _
                                                , ByVal sModuleName2 As String _
                                                , ByVal sModuleName3 As String _
                                                , ByVal sModuleName4 As String _
                                                , ByVal sModuleName5 As String _
                                                ) As Boolean
'Sohail (01 Mar 2019) - [objDataOperation] = [xDataOp]

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim blnFlag As Boolean = False

        'Sohail (01 Mar 2019) -- Start
        'KBC Support Issue 0003536 - 74.1 - The database is in single user mode on close period.
        'objDataOperation = New clsDataOperation
        If xDataOp IsNot Nothing Then
            objDataOperation = xDataOp
        Else
        objDataOperation = New clsDataOperation
        End If
        objDataOperation.ClearParameters()
        'Sohail (01 Mar 2019) -- End

        Try

            strQ = "INSERT INTO " & strDatabaseName & "..atprearningdeduction_master ( " & _
                        "  edunkid " & _
                        ", employeeunkid " & _
                        ", tranheadunkid " & _
                        ", batchtransactionunkid " & _
                        ", amount " & _
                        ", isdeduct " & _
                        ", trnheadtype_id " & _
                        ", typeof_id " & _
                        ", calctype_id " & _
                        ", computeon_id " & _
                        ", formula " & _
                        ", formulaid " & _
                        ", currencyunkid " & _
                        ", vendorunkid " & _
                        ", membership_categoryunkid " & _
                        ", isapproved " & _
                        ", approveruserunkid " & _
                        ", periodunkid " & _
                        ", medicalrefno " & _
                        ", disciplinefileunkid " & _
                        ", membershiptranunkid " & _
                        ", cumulative_startdate " & _
                        ", stop_date " & _
                        ", empbatchpostingunkid " & _
                        ", edstart_date " & _
                        ", audittype " & _
                        ", audituserunkid " & _
                        ", auditdatetime " & _
                        ", ip " & _
                        ", machine_name" & _
                        ", form_name " & _
                        " " & _
                        " " & _
                        " " & _
                        " " & _
                        " " & _
                        ", isweb " & _
                        ", loginemployeeunkid " & _
                        " ) " & _
                   " SELECT " & _
                        "  prearningdeduction_master.edunkid " & _
                        ", prearningdeduction_master.employeeunkid " & _
                        ", prearningdeduction_master.tranheadunkid " & _
                        ", prearningdeduction_master.batchtransactionunkid " & _
                        ", prearningdeduction_master.amount " & _
                        ", prearningdeduction_master.isdeduct " & _
                        ", prearningdeduction_master.trnheadtype_id " & _
                        ", prearningdeduction_master.typeof_id " & _
                        ", prearningdeduction_master.calctype_id " & _
                        ", prearningdeduction_master.computeon_id " & _
                        ", prearningdeduction_master.formula " & _
                        ", prearningdeduction_master.formulaid " & _
                        ", prearningdeduction_master.currencyunkid " & _
                        ", prearningdeduction_master.vendorunkid " & _
                        ", prearningdeduction_master.membership_categoryunkid " & _
                        ", prearningdeduction_master.isapproved " & _
                        ", prearningdeduction_master.approveruserunkid " & _
                        ", prearningdeduction_master.periodunkid " & _
                        ", prearningdeduction_master.medicalrefno " & _
                        ", prearningdeduction_master.disciplinefileunkid " & _
                        ", prearningdeduction_master.membershiptranunkid " & _
                        ", prearningdeduction_master.cumulative_startdate " & _
                        ", prearningdeduction_master.stop_date " & _
                        ", prearningdeduction_master.empbatchpostingunkid " & _
                        ", prearningdeduction_master.edstart_date " & _
                        ", @audittype " & _
                        ", @audituserunkid " & _
                        ", @auditdatetime " & _
                        ", @ip " & _
                        ", @machine_name" & _
                        ", @form_name " & _
                        " " & _
                        " " & _
                        " " & _
                        " " & _
                        " " & _
                        ", @isweb " & _
                        ", @loginemployeeunkid " & _
                   "FROM     " & strDatabaseName & "..prearningdeduction_master " & _
                   "         LEFT JOIN " & strDatabaseName & "..prtranhead_master ON prearningdeduction_master.tranheadunkid = prtranhead_master.tranheadunkid " & _
                   "WHERE    prearningdeduction_master.tranheadunkid = prtranhead_master.tranheadunkid " & _
                            "AND ISNULL(prearningdeduction_master.isvoid, 0) = 0 " & _
                            "AND ISNULL(prtranhead_master.isvoid, 0) = 0 " & _
                            "AND prtranhead_master.calctype_id = " & enCalcType.FlatRate_Others & " " & _
                            "AND prtranhead_master.isrecurrent = 0 " & _
                            "AND prearningdeduction_master.amount <> 0 " & _
                            "AND prearningdeduction_master.periodunkid = " & intPeriodID & " "
            'Sohail (24 Aug 2019) - [edstart_date]
            'Sohail (24 Feb 2016) - [empbatchpostingunkid]

            objDataOperation.AddParameter("@audittype", SqlDbType.Int, eZeeDataType.INT_SIZE, 2)
            objDataOperation.AddParameter("@audituserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUserunkid)
            objDataOperation.AddParameter("@auditdatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, dtCurrentDateAndTime)
            objDataOperation.AddParameter("@ip", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrClientIP)
            objDataOperation.AddParameter("@machine_name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrHostName)
            objDataOperation.AddParameter("@loginemployeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLogEmployeeUnkid)
            objDataOperation.AddParameter("@form_name", SqlDbType.NVarChar, 500, mstrFormName)
            objDataOperation.AddParameter("@isweb", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsWeb)

            objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If


            strQ = "UPDATE   " & strDatabaseName & "..prearningdeduction_master " & _
                    "SET     prearningdeduction_master.amount = 0 " & _
                          ", prearningdeduction_master.medicalrefno = '' " & _
                    "FROM    " & strDatabaseName & "..prtranhead_master " & _
                    "WHERE   prearningdeduction_master.tranheadunkid = prtranhead_master.tranheadunkid " & _
                            "AND ISNULL(prearningdeduction_master.isvoid, 0) = 0 " & _
                            "AND ISNULL(prtranhead_master.isvoid, 0) = 0 " & _
                            "AND prtranhead_master.calctype_id = " & enCalcType.FlatRate_Others & " " & _
                            "AND prtranhead_master.isrecurrent = 0 " & _
                            "AND prearningdeduction_master.amount <> 0 " & _
                            "AND prearningdeduction_master.periodunkid = " & mintPeriodunkid & " "

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If


            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: UpdateNonRecurrentHeadOnED; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xDataOp Is Nothing Then objDataOperation = Nothing 'Sohail (01 Mar 2019)
        End Try
    End Function
    'Sohail (12 Dec 2015) -- End

    'Sohail (11 Sep 2010) -- Start
    ''' <summary>
    ''' Get Active Transaction Head List for Combo for given Employee.
    ''' Modify by : Sohail
    ''' </summary>
    ''' <param name="strTableName"></param>
    ''' <param name="intEmpUnkID"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function getListForCombo_TransactionHead(ByVal strTableName As String, ByVal intEmpUnkID As Integer, ByVal dtPeriodEnd As Date, Optional ByVal blnInformationalHead As Boolean = False) As DataSet
        'Sohail (29 Mar 2017) - [dtPeriodEnd]
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            'Sohail (29 Mar 2017) -- Start
            'CCBRT Enhancement - 66.1 - Employee cost center distribution for non-salary heads, Effective period option on Employee Cost Center.
            'strQ = "SELECT 0 AS tranheadunkid , '' AS Code, @Select AS trnheadname UNION " & _
            '       "SELECT " & _
            '              "  prearningdeduction_master.tranheadunkid " & _
            '              ", ISNULL(prtranhead_master.trnheadcode, '') AS Code " & _
            '              ", ISNULL(prtranhead_master.trnheadname,'') AS trnheadname " & _
            '        "FROM prearningdeduction_master " & _
            '            "LEFT JOIN prtranhead_master ON prearningdeduction_master.tranheadunkid = prtranhead_master.tranheadunkid " & _
            '        "WHERE ISNULL(prearningdeduction_master.isvoid,0) = 0 " & _
            '            "AND prearningdeduction_master.employeeunkid = @employeeunkiid " 'Sohail (16 Oct 2010)
            ''Sohail (21 Mar 2017) - [Code]

            'If blnInformationalHead = False Then
            '    strQ &= "AND prtranhead_master.trnheadtype_id <> 5 " 'Informational
            'End If

            'Hemant (26 Dec 2018) -- Start
            'Enhancement - On employee cost centre, provide a global option to assign all employees, on a single transaction head to one cost centre. 
            'Also on this screen, provide an analysis by option where user can filter out employees in 76.1.

            
            'strQ = "SELECT  0 AS tranheadunkid  " & _
            '              ", '' AS Code " & _
            '              ", @Select AS trnheadname " & _
            '        "UNION " & _
            '        "SELECT  A.tranheadunkid  " & _
            '              ", A.Code " & _
            '              ", A.trnheadname " & _
            '        "FROM    ( SELECT    prearningdeduction_master.tranheadunkid  " & _
            '              ", ISNULL(prtranhead_master.trnheadcode, '') AS Code " & _
            '                          ", ISNULL(prtranhead_master.trnheadname, '') AS trnheadname " & _
            '                          ", DENSE_RANK() OVER ( PARTITION BY prearningdeduction_master.employeeunkid ORDER BY cfcommon_period_tran.end_date DESC ) AS ROWNO " & _
            '        "FROM prearningdeduction_master " & _
            '            "LEFT JOIN prtranhead_master ON prearningdeduction_master.tranheadunkid = prtranhead_master.tranheadunkid " & _
            '                            "LEFT JOIN cfcommon_period_tran ON cfcommon_period_tran.periodunkid = prearningdeduction_master.periodunkid " & _
            '                  "WHERE     ISNULL(prearningdeduction_master.isvoid, 0) = 0 " & _
            '                            "AND cfcommon_period_tran.isactive = 1 " & _
            '                            "AND cfcommon_period_tran.modulerefid = 1 " & _
            '                            "AND prearningdeduction_master.employeeunkid = @employeeunkiid " & _
            '                            "AND CONVERT(CHAR(8), cfcommon_period_tran.end_date, 112) <= @end_date "
            strQ = "SELECT  0 AS tranheadunkid  " & _
                          ", '' AS Code " & _
                          ", @Select AS trnheadname " & _
                    "UNION " & _
                    "SELECT  A.tranheadunkid  " & _
                          ", A.Code " & _
                          ", A.trnheadname " & _
                    "FROM    ( SELECT    prearningdeduction_master.tranheadunkid  " & _
                          ", ISNULL(prtranhead_master.trnheadcode, '') AS Code " & _
                                      ", ISNULL(prtranhead_master.trnheadname, '') AS trnheadname " & _
                                      ", DENSE_RANK() OVER ( PARTITION BY prearningdeduction_master.employeeunkid ORDER BY cfcommon_period_tran.end_date DESC ) AS ROWNO " & _
                    "FROM prearningdeduction_master " & _
                        "LEFT JOIN prtranhead_master ON prearningdeduction_master.tranheadunkid = prtranhead_master.tranheadunkid " & _
                                        "LEFT JOIN cfcommon_period_tran ON cfcommon_period_tran.periodunkid = prearningdeduction_master.periodunkid " & _
                              "WHERE     ISNULL(prearningdeduction_master.isvoid, 0) = 0 " & _
                                        "AND cfcommon_period_tran.isactive = 1 " & _
                                        "AND cfcommon_period_tran.modulerefid = 1 " & _
                                        "AND CONVERT(CHAR(8), cfcommon_period_tran.end_date, 112) <= @end_date "
            If intEmpUnkID > 0 Then
                strQ &= "AND prearningdeduction_master.employeeunkid = @employeeunkiid "
            End If
            'Hemant (26 Dec 2018) -- End


            If blnInformationalHead = False Then
                strQ &= "AND prtranhead_master.trnheadtype_id <> " & enTranHeadType.Informational & " " 'Informational
            End If

            strQ &= "       ) AS A " & _
                    "WHERE   A.ROWNO = 1 "

            objDataOperation.AddParameter("@end_date", SqlDbType.VarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(dtPeriodEnd))
            'Sohail (29 Mar 2017) -- End

            objDataOperation.AddParameter("@Select", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 2, "Select"))

            'Hemant (26 Dec 2018) -- Start
            'Enhancement - On employee cost centre, provide a global option to assign all employees, on a single transaction head to one cost centre. 
            'Also on this screen, provide an analysis by option where user can filter out employees in 76.1.
            If intEmpUnkID > 0 Then
                'Hemant (26 Dec 2018) -- End
            objDataOperation.AddParameter("@employeeunkiid", SqlDbType.Int, eZeeDataType.INT_SIZE, intEmpUnkID)
            End If


            dsList = objDataOperation.ExecQuery(strQ, strTableName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: getListForCombo_TransactionHead; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
        Return dsList
    End Function
    'Sohail (11 Sep 2010) -- End

    'Sohail (11 Nov 2010) -- Start
    'Sohail (14 Jun 2011) -- Start
    'Issue : on error in insertaudittrail record is inserted in main table while not inserted in audit table.
    Public Function InsertAuditTrailForEarningDeduction(ByVal objDataOperation As clsDataOperation, ByVal AuditType As Integer, ByVal dtCurrentDateAndTime As Date) As Boolean
        'Sohail (21 Aug 2015) - [dtCurrentDateAndTime]
        'Public Sub InsertAuditTrailForEarningDeduction(ByVal objDataOperation As clsDataOperation, ByVal AuditType As Integer)
        'Sohail (14 Jun 2011) -- End
        Dim strQ As String = ""
        Dim exForce As Exception

        'S.SANDEEP [25 OCT 2016] -- START
        'ENHANCEMENT : APPOINTMENT DATE CHANGES TO TRANSACTION TABLE
        'objDataOperation = New clsDataOperation
        'S.SANDEEP [25 OCT 2016] -- END


        Try
            'S.SANDEEP [ 19 JULY 2012 ] -- START
            'Enhancement : TRA Changes

            strQ = "INSERT INTO " & mstrDatabaseName & "..atprearningdeduction_master ( " & _
                        "  edunkid " & _
                        ", employeeunkid " & _
                        ", tranheadunkid " & _
                        ", batchtransactionunkid " & _
                        ", amount " & _
                        ", isdeduct " & _
                        ", trnheadtype_id " & _
                        ", typeof_id " & _
                        ", calctype_id " & _
                        ", computeon_id " & _
                        ", formula " & _
                        ", formulaid " & _
                        ", currencyunkid " & _
                        ", vendorunkid " & _
                        ", membership_categoryunkid " & _
                        ", isapproved " & _
                        ", approveruserunkid " & _
                        ", audittype " & _
                        ", audituserunkid " & _
                        ", auditdatetime " & _
                        ", ip " & _
                        ", machine_name" & _
                        ", periodunkid " & _
                        ", medicalrefno " & _
                        ", disciplinefileunkid " & _
                      ", form_name " & _
                      " " & _
                      " " & _
                      " " & _
                      " " & _
                      " " & _
                      ", isweb " & _
                        ", membershiptranunkid " & _
                      ", cumulative_startdate " & _
                      ", stop_date " & _
                      ", loginemployeeunkid " & _
                      ", empbatchpostingunkid " & _
                      ", edstart_date " & _
                    ") VALUES (" & _
                        "  @edunkid " & _
                        ", @employeeunkid " & _
                        ", @tranheadunkid " & _
                        ", @batchtransactionunkid " & _
                        ", @amount " & _
                        ", @isdeduct " & _
                        ", @trnheadtype_id " & _
                        ", @typeof_id " & _
                        ", @calctype_id " & _
                        ", @computeon_id " & _
                        ", @formula " & _
                        ", @formulaid " & _
                        ", @currencyunkid " & _
                        ", @vendorunkid " & _
                        ", @membership_categoryunkid " & _
                        ", @isapproved " & _
                        ", @approveruserunkid " & _
                        ", @audittype " & _
                        ", @audituserunkid " & _
                        ", @auditdatetime " & _
                        ", @ip " & _
                        ", @machine_name" & _
                        ", @periodunkid " & _
                        ", @medicalrefno " & _
                        ", @disciplinefileunkid " & _
                      ", @form_name " & _
                      " " & _
                      " " & _
                      " " & _
                      " " & _
                      " " & _
                      ", @isweb " & _
                        ", @membershiptranunkid " & _
                      ", @cumulative_startdate " & _
                      ", @stop_date " & _
                      ", @loginemployeeunkid " & _
                      ", @empbatchpostingunkid " & _
                      ", @edstart_date " & _
                    "); SELECT @@identity"
            'Sohail (24 Aug 2019) - [edstart_date]
            'Sohail (24 Feb 2016) - [empbatchpostingunkid]
            '       'Sohail (12 Dec 2015) - [loginemployeeunkid]
            '       'Sohail (09 Nov 2013) - [cumulative_startdate, stop_date]
            '       [isapproved, approveruserunkid - Sohail (14 Jun 2011)]
            '       'S.SANDEEP [ 19 JULY 2012 ] -- END
            '       'S.SANDEEP [ 17 OCT 2012 ],[membershiptranunkid]



            'Pinkal (24-May-2013) -- Start
            'Enhancement : TRA Changes

            If mstrWebClientID.ToString().Length <= 0 Then
                mstrWebClientID = getIP()
            End If

            If mstrWebhostName.ToString().Length <= 0 Then
                mstrWebhostName = getHostName()
            End If

            'Pinkal (24-May-2013) -- End



            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@edunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEdunkid.ToString)
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeunkid.ToString)
            objDataOperation.AddParameter("@tranheadunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintTranheadunkid.ToString)
            objDataOperation.AddParameter("@batchtransactionunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintBatchtransactionunkid.ToString)
            objDataOperation.AddParameter("@amount", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecAmount.ToString) 'Sohail (11 May 2011)
            objDataOperation.AddParameter("@isdeduct", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsdeduct.ToString)
            objDataOperation.AddParameter("@trnheadtype_id", SqlDbType.Int, eZeeDataType.INT_SIZE, mintTrnheadtype_Id.ToString)
            objDataOperation.AddParameter("@typeof_id", SqlDbType.Int, eZeeDataType.INT_SIZE, mintTypeof_Id.ToString)
            objDataOperation.AddParameter("@calctype_id", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCalctype_Id.ToString)
            objDataOperation.AddParameter("@computeon_id", SqlDbType.Int, eZeeDataType.INT_SIZE, mintComputeon_Id.ToString)
            objDataOperation.AddParameter("@formula", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrFormula.ToString)
            objDataOperation.AddParameter("@formulaid", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrFormulaid.ToString)
            objDataOperation.AddParameter("@currencyunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCurrencyunkid.ToString)
            objDataOperation.AddParameter("@vendorunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVendorunkid.ToString)
            objDataOperation.AddParameter("@membership_categoryunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintMembership_Categoryunkid)
            objDataOperation.AddParameter("@audittype", SqlDbType.Int, eZeeDataType.INT_SIZE, AuditType.ToString)
            'Sohail (23 Apr 2012) -- Start
            'TRA - ENHANCEMENT
            'objDataOperation.AddParameter("@audituserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, User._Object._Userunkid)
            objDataOperation.AddParameter("@audituserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid)
            'Sohail (23 Apr 2012) -- End
            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'objDataOperation.AddParameter("@auditdatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, ConfigParameter._Object._CurrentDateAndTime)
            objDataOperation.AddParameter("@auditdatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, dtCurrentDateAndTime)
            'Sohail (21 Aug 2015) -- End


            'Pinkal (24-May-2013) -- Start
            'Enhancement : TRA Changes

            'objDataOperation.AddParameter("@ip", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, getIP)
            'objDataOperation.AddParameter("@machine_name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, getHostName)

            objDataOperation.AddParameter("@ip", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrWebClientID)
            objDataOperation.AddParameter("@machine_name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrWebhostName)

            'Pinkal (24-May-2013) -- End


            'Sohail (14 Jun 2011) -- Start
            objDataOperation.AddParameter("@isapproved", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsapproved.ToString)
            objDataOperation.AddParameter("@approveruserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintApproveruserunkid.ToString)
            'Sohail (14 Jun 2011) -- End
            objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPeriodunkid.ToString) 'Sohail (13 Jan 2012)
            objDataOperation.AddParameter("@medicalrefno", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrMedicalrefno.ToString) 'Sohail (18 Jan 2012)

            'S.SANDEEP [ 07 NOV 2011 ] -- START
            'ENHANCEMENT : TRA CHANGES
            objDataOperation.AddParameter("@disciplinefileunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintDisciplinefileunkid.ToString)
            'S.SANDEEP [ 07 NOV 2011 ] -- END

            'S.SANDEEP [ 17 OCT 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            objDataOperation.AddParameter("@membershiptranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintMembershipTranUnkid.ToString)
            'S.SANDEEP [ 17 OCT 2012 ] -- END
            'Sohail (09 Nov 2013) -- Start
            'TRA - ENHANCEMENT
            If mdtCumulative_Startdate = Nothing Then
                objDataOperation.AddParameter("@cumulative_startdate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            Else
                objDataOperation.AddParameter("@cumulative_startdate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtCumulative_Startdate.ToString)
            End If
            If mdtStop_Date = Nothing Then
                objDataOperation.AddParameter("@stop_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            Else
                objDataOperation.AddParameter("@stop_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtStop_Date.ToString)
            End If
            'Sohail (09 Nov 2013) -- End
            'Sohail (24 Aug 2019) -- Start
            'NMB Payroll UAT # TC016 - 76.1 - System should be able to accept future amendments (Future period) and prorate. It should be possible to set start date of the specific transaction and prorate based on the start date. It should also be possible to set start date of the future period.
            If mdtEDStart_Date = Nothing Then
                objDataOperation.AddParameter("@edstart_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            Else
                objDataOperation.AddParameter("@edstart_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtEDStart_Date.ToString)
            End If
            'Sohail (24 Aug 2019) -- End
            'Sohail (24 Feb 2016) -- Start
            'Enhancement - Batch Posting changes as per online doc in 58.1 SP. (#15 (KBC & Kenya Project Comments List.xls))
            objDataOperation.AddParameter("@empbatchpostingunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmpbatchpostingunkid.ToString)
            'Sohail (24 Feb 2016) -- End

            'Sohail (12 Dec 2015) -- Start
            'Enhancement - Showing Progress of closing period.
            objDataOperation.AddParameter("@loginemployeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLogEmployeeUnkid)
            'Sohail (12 Dec 2015) -- End
  objDataOperation.AddParameter("@form_name", SqlDbType.NVarChar, 500, mstrFormName)
            objDataOperation.AddParameter("@isweb", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsWeb)

            objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return True 'Sohail (14 Jun 2011)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "InsertAuditTrailForEarningDeduction", mstrModuleName)
        Finally
            'objDataOperation = Nothing
        End Try
    End Function
    'Sohail (11 Nov 2010) -- End

    'Public Function InsertUpdateDelete(ByVal strEmployeeList As String, ByVal dtTable As DataTable) As Boolean
    '    Dim i As Integer = 0
    '    Dim strEmp As String() = Nothing
    '    Dim strQ As String = ""
    '    Dim strErrorMessage As String = ""
    '    Dim intReturn As Integer = 0
    '    Dim n As System.DBNull = DBNull.Value
    '    Dim exForce As Exception
    '    Dim strAUD As Object = Nothing

    '    objDataOperation = New clsDataOperation

    '    objDataOperation.BindTransaction()

    '    strEmp = strEmployeeList.Split(",")

    '    Try
    '        For j As Integer = 0 To strEmp.Length - 1
    '            For i = 0 To dtTable.Rows.Count - 1
    '                With dtTable.Rows(i)
    '                    objDataOperation.ClearParameters()

    '                    strAUD = .Item("AUD")

    '                    If isExist(strEmp(j).ToString, dtTable.Rows(i)("tranheadunkid").ToString) = False Then
    '                        If IsDBNull(strAUD) = False Then
    '                            If strAUD = "U" Then
    '                                .Item("AUD") = "A"
    '                            End If
    '                        Else
    '                            .Item("AUD") = "A"
    '                        End If
    '                    ElseIf IsDBNull(strAUD) = False Then
    '                        If strAUD = "A" Then
    '                            Continue For
    '                        End If
    '                    Else
    '                        Continue For
    '                    End If

    '                    If Not IsDBNull(.Item("AUD")) Then

    '                        Select Case .Item("AUD")
    '                            Case "A"



    '                                strQ = "INSERT INTO prearningdeduction_master ( " & _
    '                                  "  employeeunkid " & _
    '                                  ", tranheadunkid " & _
    '                                  ", batchtransactionunkid " & _
    '                                  ", amount " & _
    '                                  ", isdeduct " & _
    '                                  ", trnheadtype_id " & _
    '                                  ", typeof_id " & _
    '                                  ", calctype_id " & _
    '                                  ", computeon_id " & _
    '                                  ", formula " & _
    '                                  ", formulaid " & _
    '                                  ", currencyunkid " & _
    '                                  ", vendorunkid " & _
    '                                  ", userunkid " & _
    '                                  ", isvoid " & _
    '                                  ", voiduserunkid " & _
    '                                  ", voiddatetime" & _
    '                                ") VALUES (" & _
    '                                  "  @employeeunkid " & _
    '                                  ", @tranheadunkid " & _
    '                                  ", @batchtransactionunkid " & _
    '                                  ", @amount " & _
    '                                  ", @isdeduct " & _
    '                                  ", @trnheadtype_id " & _
    '                                  ", @typeof_id " & _
    '                                  ", @calctype_id " & _
    '                                  ", @computeon_id " & _
    '                                  ", @formula " & _
    '                                  ", @formulaid " & _
    '                                  ", @currencyunkid " & _
    '                                  ", @vendorunkid " & _
    '                                  ", @userunkid " & _
    '                                  ", @isvoid " & _
    '                                  ", @voiduserunkid " & _
    '                                  ", @voiddatetime" & _
    '                                "); SELECT @@identity"

    '                                objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, strEmp(j).ToString)
    '                                objDataOperation.AddParameter("@tranheadunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("tranheadunkid").ToString)
    '                                objDataOperation.AddParameter("@batchtransactionunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("batchtranunkid").ToString)
    '                                If .Item("amount").ToString = Nothing Then
    '                                    objDataOperation.AddParameter("@amount", SqlDbType.Money, eZeeDataType.MONEY_SIZE, DBNull.Value)
    '                                Else
    '                                    objDataOperation.AddParameter("@amount", SqlDbType.Money, eZeeDataType.MONEY_SIZE, .Item("amount").ToString)
    '                                End If
    '                                objDataOperation.AddParameter("@isdeduct", SqlDbType.Bit, eZeeDataType.BIT_SIZE, .Item("isdeduct").ToString)
    '                                objDataOperation.AddParameter("@trnheadtype_id", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("trnheadtypeid").ToString)
    '                                objDataOperation.AddParameter("@typeof_id", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("typeofid").ToString)
    '                                objDataOperation.AddParameter("@calctype_id", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("calctypeid").ToString)
    '                                If .Item("computeonid").ToString = Nothing Then
    '                                    objDataOperation.AddParameter("@computeon_id", SqlDbType.Int, eZeeDataType.INT_SIZE, DBNull.Value)
    '                                Else
    '                                    objDataOperation.AddParameter("@computeon_id", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("computeonid").ToString)
    '                                End If
    '                                objDataOperation.AddParameter("@formula", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, .Item("formula").ToString)
    '                                objDataOperation.AddParameter("@formulaid", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, .Item("formulaid").ToString)
    '                                objDataOperation.AddParameter("@currencyunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("currencyunkid").ToString)
    '                                objDataOperation.AddParameter("@vendorunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("vendorunkid").ToString)
    '                                If .Item("userunkid").ToString = Nothing Then
    '                                    objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, DBNull.Value)
    '                                Else
    '                                    objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("userunkid").ToString)
    '                                End If
    '                                If .Item("isvoid").ToString = Nothing Then
    '                                    objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, DBNull.Value)
    '                                Else
    '                                    objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, .Item("isvoid").ToString)
    '                                End If
    '                                If .Item("voiduserunkid").ToString = Nothing Then
    '                                    objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, DBNull.Value)
    '                                Else
    '                                    objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("voiduserunkid").ToString)
    '                                End If
    '                                If .Item("voiddatetime").ToString = Nothing Then
    '                                    objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
    '                                Else
    '                                    objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, .Item("voiddatetime").ToString)
    '                                End If

    '                                objDataOperation.ExecNonQuery(strQ)

    '                                If objDataOperation.ErrorMessage <> "" Then
    '                                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
    '                                    Throw exForce
    '                                End If

    '                            Case "U"

    '                                strQ = "UPDATE prearningdeduction_master SET " & _
    '                                             "  employeeunkid = @employeeunkid" & _
    '                                             ", tranheadunkid = @tranheadunkid" & _
    '                                             ", batchtransactionunkid = @batchtransactionunkid" & _
    '                                             ", amount = @amount" & _
    '                                             ", isdeduct = @isdeduct" & _
    '                                             ", trnheadtype_id = @trnheadtype_id" & _
    '                                             ", typeof_id = @typeof_id" & _
    '                                             ", calctype_id = @calctype_id" & _
    '                                             ", computeon_id = @computeon_id" & _
    '                                             ", formula = @formula" & _
    '                                             ", formulaid = @formulaid" & _
    '                                             ", currencyunkid = @currencyunkid" & _
    '                                             ", vendorunkid = @vendorunkid" & _
    '                                             ", userunkid = @userunkid" & _
    '                                             ", isvoid = @isvoid" & _
    '                                             ", voiduserunkid = @voiduserunkid" & _
    '                                             ", voiddatetime = @voiddatetime " & _
    '                                           "WHERE edunkid = @edunkid "

    '                                objDataOperation.AddParameter("@edunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("edunkid").ToString)
    '                                objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, strEmp(j).ToString)
    '                                objDataOperation.AddParameter("@tranheadunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("tranheadunkid").ToString)
    '                                objDataOperation.AddParameter("@batchtransactionunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("batchtranunkid").ToString)
    '                                If .Item("amount").ToString = Nothing Then
    '                                    objDataOperation.AddParameter("@amount", SqlDbType.Money, eZeeDataType.MONEY_SIZE, DBNull.Value)
    '                                Else
    '                                    objDataOperation.AddParameter("@amount", SqlDbType.Money, eZeeDataType.MONEY_SIZE, .Item("amount").ToString)
    '                                End If
    '                                objDataOperation.AddParameter("@isdeduct", SqlDbType.Bit, eZeeDataType.BIT_SIZE, .Item("isdeduct").ToString)
    '                                objDataOperation.AddParameter("@trnheadtype_id", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("trnheadtypeid").ToString)
    '                                objDataOperation.AddParameter("@typeof_id", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("typeofid").ToString)
    '                                objDataOperation.AddParameter("@calctype_id", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("calctypeid").ToString)
    '                                If .Item("computeonid").ToString = Nothing Then
    '                                    objDataOperation.AddParameter("@computeon_id", SqlDbType.Int, eZeeDataType.INT_SIZE, DBNull.Value)
    '                                Else
    '                                    objDataOperation.AddParameter("@computeon_id", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("computeonid").ToString)
    '                                End If
    '                                objDataOperation.AddParameter("@formula", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, .Item("formula").ToString)
    '                                objDataOperation.AddParameter("@formulaid", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, .Item("formulaid").ToString)
    '                                objDataOperation.AddParameter("@currencyunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("currencyunkid").ToString)
    '                                objDataOperation.AddParameter("@vendorunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("vendorunkid").ToString)
    '                                If .Item("userunkid").ToString = Nothing Then
    '                                    objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, DBNull.Value)
    '                                Else
    '                                    objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("userunkid").ToString)
    '                                End If
    '                                If .Item("isvoid").ToString = Nothing Then
    '                                    objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, DBNull.Value)
    '                                Else
    '                                    objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, .Item("isvoid").ToString)
    '                                End If
    '                                If .Item("voiduserunkid").ToString = Nothing Then
    '                                    objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, DBNull.Value)
    '                                Else
    '                                    objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("voiduserunkid").ToString)
    '                                End If
    '                                If .Item("voiddatetime").ToString = Nothing Then
    '                                    objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
    '                                Else
    '                                    objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, cdate(.Item("voiddatetime")))
    '                                End If

    '                                intReturn = objDataOperation.ExecNonQuery(strQ)

    '                                If objDataOperation.ErrorMessage <> "" Then
    '                                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
    '                                    Throw exForce
    '                                End If

    '                            Case "D"
    '                                strQ = "DELETE FROM prearningdeduction_master " & _
    '                                        "WHERE employeeunkid = @employeeunkid " & _
    '                                    "AND tranheadunkid = @tranheadunkid "

    '                                objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, strEmp(j).ToString)
    '                                objDataOperation.AddParameter("@tranheadunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("tranheadunkid").ToString)

    '                                Call objDataOperation.ExecNonQuery(strQ)


    '                                If objDataOperation.ErrorMessage <> "" Then
    '                                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
    '                                    Throw exForce
    '                                End If
    '                        End Select
    '                    End If
    '                    .Item("AUD") = strAUD
    '                    .AcceptChanges()
    '                End With
    '            Next
    '        Next
    '        objDataOperation.ReleaseTransaction(True)
    '        Return True
    '    Catch ex As Exception
    '        objDataOperation.ReleaseTransaction(False)
    '        Throw New Exception(ex.Message & "; Procedure Name: InsertUpdateDelete; Module Name: " & mstrModuleName)
    '    Finally
    '        exForce = Nothing
    '        objDataOperation = Nothing
    '    End Try
    'End Function


    '''' <summary>
    '''' Modify By: Suhail
    '''' </summary>
    '''' <purpose> Assign all Property variable </purpose>
    'Public Function isUsed(ByVal intUnkid As Integer) As Boolean
    '    Dim dsList As DataSet = Nothing
    '    Dim strQ As String = ""
    '    Dim exForce As Exception

    '    objDataOperation = New clsDataOperation

    '    Try
    '        strQ = "<Query>"

    '        objDataOperation.AddParameter("@edunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

    '        dsList = objDataOperation.ExecQuery(strQ, "List")

    '        If objDataOperation.ErrorMessage <> "" Then
    '            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
    '            Throw exForce
    '        End If

    '        Return dsList.Tables(0).Rows.Count > 0
    '    Catch ex As Exception
    '        Throw New Exception(ex.Message & "; Procedure Name: isUsed; Module Name: " & mstrModuleName)
    '    Finally
    '        exForce = Nothing
    '        If dsList IsNot Nothing Then dsList.Dispose()
    '        objDataOperation = Nothing
    '    End Try
    'End Function

    'Public Sub SetTable()
    '    Try
    '        Dim coledUnkid As New DataColumn("edunkid", System.Type.GetType("System.Int32"))
    '        Dim colBatchtranunkId As New DataColumn("batchtranunkid", System.Type.GetType("System.Int32"))
    '        Dim colTranheadunkId As New DataColumn("tranheadunkid", System.Type.GetType("System.Int32"))
    '        Dim colTranhead As New DataColumn("tranhead")
    '        Dim colTrnheadtypeID As New DataColumn("trnheadtypeID", System.Type.GetType("System.Int32"))
    '        Dim colTrnheadtype As New DataColumn("trnheadtype")
    '        Dim colTypeofId As New DataColumn("typeofId", System.Type.GetType("System.Int32"))
    '        Dim colCalctypeId As New DataColumn("calctypeId", System.Type.GetType("System.Int32"))
    '        Dim colCalctype As New DataColumn("calctype")
    '        Dim colComputeonID As New DataColumn("computeonID", System.Type.GetType("System.Int32"))
    '        Dim colComputeon As New DataColumn("computeon")
    '        Dim colIsdeduct As New DataColumn("isdeduct")
    '        Dim colAmount As New DataColumn("amount")

    '        Dim colCurrencyinkId As New DataColumn("currencyunkId")
    '        Dim colVendorunkId As New DataColumn("vendorunkId")
    '        Dim colUserunkId As New DataColumn("userunkId")
    '        Dim colIsvoid As New DataColumn("isvoid")
    '        Dim colVoiduserunkId As New DataColumn("voiduserunkId")
    '        Dim colVoiddatetime As New DataColumn("voiddatetime")
    '        Dim colFormula As New DataColumn("formula")
    '        Dim colAUD As New DataColumn("AUD")
    '        Dim colGUID As New DataColumn("GUID")


    '        With mtblEarnDeductHeads
    '            .Columns.Add(coledUnkid)
    '            .Columns.Add(colBatchtranunkId)
    '            .Columns.Add(colTranheadunkId)
    '            .Columns.Add(colTranhead)
    '            .Columns.Add(colTrnheadtypeID)
    '            .Columns.Add(colTrnheadtype)
    '            .Columns.Add(colTypeofId)
    '            .Columns.Add(colCalctypeId)
    '            .Columns.Add(colCalctype)
    '            .Columns.Add(colComputeonID)
    '            .Columns.Add(colComputeon)
    '            .Columns.Add(colIsdeduct)
    '            .Columns.Add(colAmount)
    '            .Columns.Add(colCurrencyinkId)
    '            .Columns.Add(colVendorunkId)
    '            .Columns.Add(colUserunkId)
    '            .Columns.Add(colIsvoid)
    '            .Columns.Add(colVoiduserunkId)
    '            .Columns.Add(colVoiddatetime)
    '            .Columns.Add(colFormula)
    '            .Columns.Add(colAUD)
    '            .Columns.Add(colGUID)

    '            .Rows.Clear()
    '        End With

    '    Catch ex As Exception
    '        Throw New Exception(ex.Message & "; Procedure Name: SetTable; Module Name: " & mstrModuleName)
    '    End Try
    'End Sub


    'Sohail (23 Apr 2011) -- Start
    Public Function GetEmpFlatRateHeadList(ByVal xDatabaseName As String _
                                           , ByVal xUserUnkid As Integer _
                                           , ByVal xYearUnkid As Integer _
                                           , ByVal xCompanyUnkid As Integer _
                                           , ByVal xPeriodStart As DateTime _
                                           , ByVal xPeriodEnd As DateTime _
                                           , ByVal xUserModeSetting As String _
                                           , ByVal xOnlyApproved As Boolean _
                                           , ByVal xIncludeIn_ActiveEmployee As Boolean _
                                           , Optional ByVal strTableName As String = "List" _
                                           , Optional ByVal strEmpCode As String = "" _
                                           , Optional ByVal dtPeriodEndDate As Date = Nothing _
                                           , Optional ByVal blnApplyUserAccessFilter As Boolean = True _
                                           , Optional ByVal strFilerString As String = "" _
                                           ) As DataSet
        'Sohail (21 Aug 2015) - [xDatabaseName, xUserUnkid, xYearUnkid, xCompanyUnkid, xPeriodStart, xPeriodEnd, xUserModeSetting, xOnlyApproved, xIncludeIn_ActiveEmployee, blnApplyUserAccessFilter, strFilerString]

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        'Sohail (21 Aug 2015) -- Start
        'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
        Dim xDateJoinQry, xDateFilterQry, xUACQry, xUACFiltrQry, xAdvanceJoinQry As String
        xDateJoinQry = "" : xDateFilterQry = "" : xUACQry = "" : xUACFiltrQry = "" : xAdvanceJoinQry = ""
        Call GetDatesFilterString(xDateJoinQry, xDateFilterQry, xPeriodStart, xPeriodEnd, , , xDatabaseName)
        Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, xPeriodEnd, xOnlyApproved, xDatabaseName, xUserUnkid, xCompanyUnkid, xYearUnkid, xUserModeSetting)
        Call GetAdvanceFilterQry(xAdvanceJoinQry, xPeriodEnd, xDatabaseName)
        'Sohail (21 Aug 2015) -- End

        objDataOperation = New clsDataOperation

        Try

            strQ = "DECLARE @query VARCHAR(MAX) " & _
                    "DECLARE @tranname VARCHAR(MAX) " & _
                    "DECLARE @selecttranname VARCHAR(MAX) " & _
                    "SELECT  @selecttranname = COALESCE(@selecttranname + ', ISNULL([' + prtranhead_master.trnheadname + '], 0) AS [' + prtranhead_master.trnheadname + ']', 'ISNULL([' + prtranhead_master.trnheadname + '], 0) AS [' + prtranhead_master.trnheadname + ']') " & _
                    "FROM    prtranhead_master " & _
                    "WHERE   ISNULL(prtranhead_master.isvoid, 0) = 0 " & _
                            "AND prtranhead_master.calctype_id = " & enCalcType.FlatRate_Others & " " & _
                    "PRINT @selecttranname " & _
                    "SELECT  @tranname = COALESCE(@tranname + ',[' + prtranhead_master.trnheadname + ']', '[' + prtranhead_master.trnheadname + ']') " & _
                    "FROM    prtranhead_master " & _
                    "WHERE   ISNULL(prtranhead_master.isvoid, 0) = 0 " & _
                            "AND prtranhead_master.calctype_id = 7 " & _
                    "PRINT @tranname " & _
                     "SET @query = 'SELECT employeecode,'+ @selecttranname + ' FROM " & _
                     "( " & _
                      "SELECT    prearningdeduction_master.employeeunkid " & _
                                ", hremployee_master.employeecode " & _
                              ", prtranhead_master.trnheadname " & _
                              ", prearningdeduction_master.amount " & _
                      "FROM      prearningdeduction_master " & _
                                "LEFT JOIN prtranhead_master ON prtranhead_master.tranheadunkid = prearningdeduction_master.tranheadunkid " & _
                                "LEFT JOIN hremployee_master ON hremployee_master.employeeunkid = prearningdeduction_master.employeeunkid " & _
                                "LEFT JOIN cfcommon_period_tran ON prearningdeduction_master.periodunkid = cfcommon_period_tran.periodunkid "

            'Sohail (18 Jan 2012) -- Start
            'TRA - ENHANCEMENT
            If dtPeriodEndDate <> Nothing Then
                strQ &= "JOIN ( SELECT  CONVERT(CHAR(8), MAX(end_date), 112)   AS enddate, " & _
                                                                "employeeunkid " & _
                                                      "FROM      prearningdeduction_master " & _
                                                                "LEFT JOIN cfcommon_period_tran ON prearningdeduction_master.periodunkid = cfcommon_period_tran.periodunkid " & _
                      "WHERE     ISNULL(prearningdeduction_master.isvoid, 0) = 0 " & _
                                                                "AND CONVERT(CHAR(8), cfcommon_period_tran.end_date, 112) <= ' + @end_date + ' " & _
                                                      "GROUP BY employeeunkid " & _
                                                    ") AS a ON prearningdeduction_master.employeeunkid = a.employeeunkid " & _
                                                            "AND CONVERT(CHAR(8), cfcommon_period_tran.end_date, 112) = a.enddate "

                objDataOperation.AddParameter("@end_date", SqlDbType.VarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(dtPeriodEndDate))
            End If
            'Sohail (18 Jan 2012) -- End

            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            If xDateJoinQry.Trim.Length > 0 Then
                strQ &= xDateJoinQry.Replace("'", "''")
            End If

            'S.SANDEEP [15 NOV 2016] -- START
            'If xUACQry.Trim.Length > 0 Then
            '    strQ &= xUACQry.Replace("'", "''")
            'End If
            If blnApplyUserAccessFilter = True Then
            If xUACQry.Trim.Length > 0 Then
                strQ &= xUACQry.Replace("'", "''")
            End If
            End If
            'S.SANDEEP [15 NOV 2016] -- END



            If xAdvanceJoinQry.Trim.Length > 0 Then
                strQ &= xAdvanceJoinQry.Replace("'", "''")
            End If
            'Sohail (21 Aug 2015) -- End

            strQ &= "WHERE     ISNULL(prearningdeduction_master.isvoid, 0) = 0 " & _
                                "AND prtranhead_master.calctype_id = " & enCalcType.FlatRate_Others & " "
            If strEmpCode.Trim <> "" Then
                strQ &= "AND employeecode = @employeecode "
                objDataOperation.AddParameter("@employeecode", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strEmpCode)
            End If

            'Sohail (24 Jun 2011) -- Start
            'Issue : According to privilege that lower level user should not see superior level employees.

            'S.SANDEEP [ 04 FEB 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            'If UserAccessLevel._AccessLevel.Length > 0 Then
            '    strQ &= " AND hremployee_master.jobunkid IN (" & UserAccessLevel._AccessLevel & ") "
            'End If

            'S.SANDEEP [ 01 JUNE 2012 ] -- START
            'ENHANCEMENT : TRA DISCIPLINE CHANGES
            'Select Case ConfigParameter._Object._UserAccessModeSetting
            '    Case enAllocation.BRANCH
            '        If UserAccessLevel._AccessLevel.Length > 0 Then
            '            strQ &= " AND  hremployee_master.stationunkid IN (" & UserAccessLevel._AccessLevel & ") "
            '        End If
            '    Case enAllocation.DEPARTMENT_GROUP
            '        If UserAccessLevel._AccessLevel.Length > 0 Then
            '            strQ &= " AND  hremployee_master.deptgroupunkid IN (" & UserAccessLevel._AccessLevel & ") "
            '        End If
            '    Case enAllocation.DEPARTMENT
            '        If UserAccessLevel._AccessLevel.Length > 0 Then
            '            strQ &= " AND  hremployee_master.departmentunkid IN (" & UserAccessLevel._AccessLevel & ") "
            '        End If
            '    Case enAllocation.SECTION_GROUP
            '        If UserAccessLevel._AccessLevel.Length > 0 Then
            '            strQ &= " AND  hremployee_master.sectiongroupunkid IN (" & UserAccessLevel._AccessLevel & ") "
            '        End If
            '    Case enAllocation.SECTION
            '        If UserAccessLevel._AccessLevel.Length > 0 Then
            '            strQ &= " AND  hremployee_master.sectionunkid IN (" & UserAccessLevel._AccessLevel & ") "
            '        End If
            '    Case enAllocation.UNIT_GROUP
            '        If UserAccessLevel._AccessLevel.Length > 0 Then
            '            strQ &= " AND  hremployee_master.unitgroupunkid IN (" & UserAccessLevel._AccessLevel & ") "
            '        End If
            '    Case enAllocation.UNIT
            '        If UserAccessLevel._AccessLevel.Length > 0 Then
            '            strQ &= " AND  hremployee_master.unitunkid IN (" & UserAccessLevel._AccessLevel & ") "
            '        End If
            '    Case enAllocation.TEAM
            '        If UserAccessLevel._AccessLevel.Length > 0 Then
            '            strQ &= " AND  hremployee_master.teamunkid IN (" & UserAccessLevel._AccessLevel & ") "
            '        End If
            '    Case enAllocation.JOB_GROUP
            '        If UserAccessLevel._AccessLevel.Length > 0 Then
            '            strQ &= " AND  hremployee_master.jobgroupunkid IN (" & UserAccessLevel._AccessLevel & ") "
            '        End If
            '    Case enAllocation.JOBS
            '        If UserAccessLevel._AccessLevel.Length > 0 Then
            '            strQ &= " AND hremployee_master.jobunkid IN (" & UserAccessLevel._AccessLevel & ") "
            '        End If
            'End Select
            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'strQ &= UserAccessLevel._AccessLevelFilterString
            If blnApplyUserAccessFilter = True Then
                If xUACFiltrQry.Trim.Length > 0 Then
                    strQ &= " AND " & xUACFiltrQry.Replace("'", "''")
                End If
            End If

            If xIncludeIn_ActiveEmployee = False Then
                If xDateFilterQry.Trim.Length > 0 Then
                    strQ &= xDateFilterQry.Replace("'", "''")
                End If
            End If

            If strFilerString.Trim.Length > 0 Then
                strQ &= " AND " & strFilerString.Replace("'", "''")
            End If
            'Sohail (21 Aug 2015) -- End
            'S.SANDEEP [ 01 JUNE 2012 ] -- END


            'S.SANDEEP [ 04 FEB 2012 ] -- END
            'Sohail (24 Jun 2011) -- End

            strQ &= ") AS A " & _
                     "PIVOT (SUM(amount) FOR trnheadname IN (' + @tranname + ')) AS PVT ;' " & _
                     "EXECUTE(@query) "

            dsList = objDataOperation.ExecQuery(strQ, strTableName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetEmpFlatRateHeadList; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
        Return dsList
    End Function
    'Sohail (23 Apr 2011) -- End

    'Sohail (08 Nov 2011) -- Start
    Public Function isUsed(ByVal intEmpId As Integer, ByVal intTranHeadId As Integer) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "SELECT  costcentertranunkid " & _
                    "FROM    premployee_costcenter_tran " & _
                    "WHERE   ISNULL(isvoid, 0) = 0 " & _
                            "AND employeeunkid = @employeeunkid " & _
                            "AND tranheadunkid = @tranheadunkid "

            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intEmpId)
            objDataOperation.AddParameter("@tranheadunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intTranHeadId)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isUsed; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function
    'Sohail (08 Nov 2011) -- End

    'Sohail (18 Jan 2012) -- Start
    'TRA - ENHANCEMENT
    Public Function GetCurrentSlabEDPeriodUnkID(ByVal intEmpUnkID As Integer, ByVal dtPeriodEnd As Date) As Integer
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim intPeriodID As Integer = 0
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try

            strQ = "SELECT  cfcommon_period_tran.periodunkid " & _
                   "FROM    ( SELECT    CONVERT(CHAR(8), MAX(end_date), 112) AS enddate " & _
                             "FROM      prearningdeduction_master " & _
                                       "LEFT JOIN cfcommon_period_tran ON prearningdeduction_master.periodunkid = cfcommon_period_tran.periodunkid " & _
                             "WHERE     ISNULL(prearningdeduction_master.isvoid, 0) = 0 " & _
                                       "AND prearningdeduction_master.employeeunkid = @employeeunkid " & _
                                       "AND CONVERT(CHAR(8), cfcommon_period_tran.end_date, 112) <= @end_date " & _
                           ") AS a " & _
                           "JOIN cfcommon_period_tran ON a.enddate = CONVERT(CHAR(8), cfcommon_period_tran.end_date, 112) " & _
                                    "AND cfcommon_period_tran.modulerefid = " & enModuleReference.Payroll & " " & _
                                    "AND cfcommon_period_tran.isactive = 1 "

            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intEmpUnkID)
            objDataOperation.AddParameter("@end_date", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(dtPeriodEnd))

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If dsList.Tables("List").Rows.Count > 0 Then
                intPeriodID = dsList.Tables("List").Rows(0).Item("periodunkid")
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetAssignedEDPeriodList; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
        Return intPeriodID
    End Function

    Public Function GetAssignedEDPeriodList(ByVal strTableName As String _
                                            , ByVal intEmpUnkID As Integer _
                                            , ByVal intYearUnkid As Integer _
                                            , Optional ByVal mblFlag As Boolean = False _
                                            , Optional ByVal intStatusID As Integer = 0) As DataSet
        'Sohail (21 Aug 2015) - []
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            If mblFlag = True Then
                strQ = "SELECT 0 as periodunkid, ' ' AS period_code, ' ' +  @Select  as period_name, '19000101' AS start_date, '19000101' AS end_date UNION "
            End If

            strQ &= "SELECT DISTINCT " & _
                            "prearningdeduction_master.periodunkid " & _
                          ", cfcommon_period_tran.period_code " & _
                          ", cfcommon_period_tran.period_name " & _
                          ", CONVERT(CHAR(8),start_date,112) AS start_date " & _
                          ", CONVERT(CHAR(8),end_date,112) AS end_date " & _
                    "FROM    prearningdeduction_master " & _
                            "JOIN cfcommon_period_tran ON prearningdeduction_master.periodunkid = cfcommon_period_tran.periodunkid " & _
                                                             "AND cfcommon_period_tran.modulerefid = " & enModuleReference.Payroll & " " & _
                    "WHERE   ISNULL(isvoid, 0) = 0 " & _
                    " AND cfcommon_period_tran.isactive = 1 " & _
                    " AND cfcommon_period_tran.yearunkid = " & intYearUnkid & " "
            'Sohail (21 Aug 2015) - [" AND cfcommon_period_tran.yearunkid = " & FinancialYear._Object._YearUnkid & " "]

            If intEmpUnkID > 0 Then
                strQ &= " AND prearningdeduction_master.employeeunkid = @employeeunkid "
                objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intEmpUnkID)
            End If

            If intStatusID > 0 Then
                strQ &= "AND cfcommon_period_tran.statusid = @statusid "
                objDataOperation.AddParameter("@statusid", SqlDbType.Int, eZeeDataType.INT_SIZE, intStatusID)
            End If

            strQ &= "ORDER BY end_date "

            objDataOperation.AddParameter("@Select", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 2, "Select"))


            dsList = objDataOperation.ExecQuery(strQ, strTableName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetAssignedEDPeriodList; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
        Return dsList
    End Function
    'Sohail (18 Jan 2012) -- End

    'Sohail (16 May 2012) -- Start
    'TRA - ENHANCEMENT
    Public Function GetCurrentSlabEDHeadIdArrayList(ByVal intEmpUnkID As Integer, ByVal dtPeriodEndDate As Date) As ArrayList
        Dim dtTable As DataTable = Nothing
        Dim strQ As String = ""
        Dim arrHead As New ArrayList
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try

            'Sohail (27 Aug 2012) -- Start
            'TRA - ENHANCEMENT
            'strQ = "SELECT  prearningdeduction_master.tranheadunkid " & _
            '        "FROM    prearningdeduction_master " & _
            '                "LEFT JOIN cfcommon_period_tran ON prearningdeduction_master.periodunkid = cfcommon_period_tran.periodunkid " & _
            '                "JOIN ( SELECT   CONVERT(CHAR(8), MAX(end_date), 112) AS enddate " & _
            '                              ", employeeunkid " & _
            '                       "FROM     prearningdeduction_master " & _
            '                                "LEFT JOIN cfcommon_period_tran ON prearningdeduction_master.periodunkid = cfcommon_period_tran.periodunkid " & _
            '                       "WHERE    ISNULL(prearningdeduction_master.isvoid, 0) = 0 " & _
            '                                "AND CONVERT(CHAR(8), cfcommon_period_tran.end_date, 112) <= @end_date " & _
            '                                "AND prearningdeduction_master.employeeunkid = @employeeunkid " & _
            '                       "GROUP BY employeeunkid " & _
            '                     ") AS a ON prearningdeduction_master.employeeunkid = a.employeeunkid " & _
            '                               "AND CONVERT(CHAR(8), cfcommon_period_tran.end_date, 112) = a.enddate " & _
            '        "WHERE   ISNULL(isvoid, 0) = 0 " & _
            '                "AND prearningdeduction_master.employeeunkid = @employeeunkid "
            strQ = "SELECT  a.tranheadunkid " & _
                    "FROM    ( SELECT    DENSE_RANK() OVER ( PARTITION  BY employeeunkid ORDER BY cfcommon_period_tran.end_date DESC ) AS ROWNO " & _
                                      ", tranheadunkid " & _
                    "FROM    prearningdeduction_master " & _
                                        "JOIN cfcommon_period_tran ON prearningdeduction_master.periodunkid = cfcommon_period_tran.periodunkid " & _
                              "WHERE     ISNULL(isvoid, 0) = 0 " & _
                                        "AND end_date <= @end_date " & _
                                            "AND prearningdeduction_master.employeeunkid = @employeeunkid " & _
                            ") AS a " & _
                    "WHERE   ROWNO = 1 "
            'Sohail (27 Aug 2012) -- End

            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intEmpUnkID)
            objDataOperation.AddParameter("@end_date", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(dtPeriodEndDate))

            dtTable = objDataOperation.ExecQuery(strQ, "List").Tables(0)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For Each dtRow As DataRow In dtTable.Rows
                arrHead.Add(CInt(dtRow.Item("tranheadunkid")))
            Next

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetCurrentSlabEDHeadIdArrayList; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dtTable IsNot Nothing Then dtTable.Dispose()
            objDataOperation = Nothing
        End Try
        Return arrHead
    End Function
    'Sohail (16 May 2012) -- End

    'Sohail (24 Sep 2014) -- Start
    'Enhancement - Two Salary heads issue on ED Period Slab.
    Public Function GetSalaryHeadPeriodList(ByVal strTableName As String, ByVal strDatabaseName As String, ByVal intEmployeeUnkId As Integer, ByVal intModuleReferenceID As Integer, Optional ByVal blnOnlyOpenPeriod As Boolean = False, Optional ByVal xDataOp As clsDataOperation = Nothing, Optional ByVal intPeriodId As Integer = 0) As DataSet
        'Sohail (18 Feb 2019) - [intPeriodId], [objDataOperation = xDataOp]
        'Sohail (21 Aug 2015) - [strDatabaseName]

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        If xDataOp Is Nothing Then 'Sohail (18 Feb 2019) - [objDataOperation = xDataOp]
            objDataOperation = New clsDataOperation
            'Sohail (18 Feb 2019) -- Start
            'NMB Enhancement - 76.1 - Provide setting on transaction head to mark as default heads.
        Else
            objDataOperation = xDataOp
            'Sohail (18 Feb 2019) -- End
        End If
        objDataOperation.ClearParameters() 'Sohail (18 Feb 2019)

        Try
            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'If mstrDatabaseName.Trim = "" Then mstrDatabaseName = FinancialYear._Object._DatabaseName
            mstrDatabaseName = strTableName
            'Sohail (21 Aug 2015) -- End

            objDataOperation.ClearParameters()

            strQ = "SELECT  edunkid  " & _
                          ", prearningdeduction_master.periodunkid " & _
                          ", prearningdeduction_master.employeeunkid " & _
                          ", prearningdeduction_master.tranheadunkid " & _
                          ", prtranhead_master.trnheadtype_id " & _
                          ", prtranhead_master.typeof_id " & _
                          ", prtranhead_master.calctype_id " & _
                          ", prtranhead_master.formula " & _
                          ", prtranhead_master.formulaid " & _
                          ", CONVERT(CHAR(8), cfcommon_period_tran.start_date, 112) AS start_date " & _
                          ", CONVERT(CHAR(8), cfcommon_period_tran.end_date, 112) AS end_date " & _
                    "FROM    prearningdeduction_master " & _
                            "LEFT JOIN prtranhead_master ON prearningdeduction_master.tranheadunkid = prtranhead_master.tranheadunkid " & _
                            "LEFT JOIN cfcommon_period_tran ON cfcommon_period_tran.periodunkid = prearningdeduction_master.periodunkid " & _
                    "WHERE   prearningdeduction_master.isvoid = 0 " & _
                            "AND prtranhead_master.isvoid = 0 " & _
                            "AND cfcommon_period_tran.isactive = 1 " & _
                            "AND modulerefid = @modulerefid " & _
                            "AND employeeunkid = @employeeunkid " & _
                            "AND prtranhead_master.typeof_id = 1 "
            'Sohail (18 Feb 2019) - [tranheadunkid]
            'Shani(16-Sep-2015) -- [start_date, end_date]


            If blnOnlyOpenPeriod = True Then
                strQ &= " AND statusid = 1 "
            End If

            'Sohail (18 Feb 2019) -- Start
            'NMB Enhancement - 76.1 - Provide setting on transaction head to mark as default heads.
            If intPeriodId > 0 Then
                strQ &= " AND prearningdeduction_master.periodunkid = @periodunkid "
                objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intPeriodId)
            End If
            'Sohail (18 Feb 2019) -- End

            objDataOperation.AddParameter("@modulerefid", SqlDbType.Int, eZeeDataType.INT_SIZE, intModuleReferenceID)
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intEmployeeUnkId)

            dsList = objDataOperation.ExecQuery(strQ, strTableName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetSalaryHeadPeriodList; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
        End Try
        Return dsList
    End Function
    'Sohail (24 Sep 2014) -- End

    'Sohail (26 Aug 2016) -- Start
    'Enhancement - 57.4 & 63.1 - Process Payroll Performance enhancement for TRA.
    Public Function GetCurrentSlabEDHeadId(ByVal dtPeriodEndDate As Date, Optional ByVal intEmpUnkID As Integer = 0, Optional ByVal xDataOp As clsDataOperation = Nothing, Optional ByVal strEmpIDs As String = "") As DataSet
        'Sohail (10 Feb 2020) - [strEmpIDs]
        'Sohail (03 May 2018) - [xDataOp]
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim arrHead As New ArrayList
        Dim exForce As Exception

        'Sohail (03 May 2018) -- Start
        'Enhancement : Removing global objects and passing objDataOperation in process payroll in 72.1.
        'objDataOperation = New clsDataOperation
        If xDataOp Is Nothing Then
        objDataOperation = New clsDataOperation
        Else
            objDataOperation = xDataOp
        End If
        objDataOperation.ClearParameters()
        'Sohail (03 May 2018) -- End

        Try

            strQ = "SELECT  a.tranheadunkid " & _
                          ", employeeunkid " & _
                    "FROM    ( SELECT    DENSE_RANK() OVER ( PARTITION  BY employeeunkid ORDER BY cfcommon_period_tran.end_date DESC ) AS ROWNO " & _
                                      ", tranheadunkid " & _
                                      ", employeeunkid " & _
                    "FROM    prearningdeduction_master " & _
                                        "JOIN cfcommon_period_tran ON prearningdeduction_master.periodunkid = cfcommon_period_tran.periodunkid " & _
                              "WHERE     ISNULL(isvoid, 0) = 0 " & _
                                        "AND end_date <= @end_date "

            If intEmpUnkID > 0 Then
                strQ &= "AND prearningdeduction_master.employeeunkid = @employeeunkid "
            End If

            'Sohail (10 Feb 2020) -- Start
            'NMB Enhancement # : Pick least amount benefit if head is mapped in more than one benefit and both benefits effective dates are same.
            If strEmpIDs.Trim <> "" Then
                strQ &= "AND prearningdeduction_master.employeeunkid IN (" & strEmpIDs & ") "
            End If
            'Sohail (10 Feb 2020) -- End

            strQ &= "   ) AS a " & _
                        "WHERE   ROWNO = 1 "

            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intEmpUnkID)
            objDataOperation.AddParameter("@end_date", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(dtPeriodEndDate))

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetCurrentSlabEDHeadId; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            'Sohail (03 May 2018) -- Start
            'Enhancement : Removing global objects and passing objDataOperation in process payroll in 72.1.
            'objDataOperation = Nothing
            If xDataOp Is Nothing Then objDataOperation = Nothing
            'Sohail (03 May 2018) -- End
        End Try
        Return dsList
    End Function
    'Sohail (26 Aug 2016) -- End

    'Sohail (18 Feb 2019) -- Start
    'NMB Enhancement - 76.1 - Provide setting on transaction head to mark as default heads.
    Public Function AssignDefaultTransactionHeads(ByVal xDataOp As clsDataOperation _
                                                  , ByVal strDatabaseName As String _
                                                  , ByVal intYearUnkId As Integer _
                                                  , ByVal intCompanyUnkId As Integer _
                                                  , ByVal intPeriodId As Integer _
                                                  , ByVal dtPeriodStart As Date _
                                                  , ByVal dtPeriodEnd As Date _
                                                  , ByVal strUserModeSetting As String _
                                                  , ByVal blnOnlyApproved As Boolean _
                                                  , ByVal xIncludeIn_ActiveEmployee As Boolean _
                                                  , ByVal blnOverWrite As Boolean _
                                                  , ByVal intUserUnkId As Integer _
                                                  , ByVal blnAllowToApproveEarningDeduction As Boolean _
                                                  , ByVal dtCurrentDateAndTime As Date _
                                                  , ByVal strEmployeeIDs As String _
                                                  , Optional ByVal blnInsertSalaryHead As Boolean = False _
                                                  , Optional ByVal blnCopyPreviousEDSlab As Boolean = False _
                                                  , Optional ByVal blnOverwritePreviousEDSlabHeads As Boolean = False _
                                                  , Optional ByVal blnApplyUserAccessFilter As Boolean = True _
                                                  , Optional ByVal strFilerString As String = "" _
                                                  , Optional ByVal blnCheckMembershipHeads As Boolean = True _
                                                  ) As Boolean

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim objHead As New clsTransactionHead
        Dim blnResult As Boolean

        If xDataOp IsNot Nothing Then
            objDataOperation = xDataOp
        Else
            objDataOperation = New clsDataOperation
        End If
        objDataOperation.ClearParameters()

        Try
            'Sohail (21 Aug 2021) -- Start
            'AH-3872 Ocean Beach Hotel - Error Processing Payroll (The given key was not present in dictionary).
            'dsList = objHead.getComboList(strDatabaseName, "List", False, 0, 0, -1, True, True, "typeof_id <> " & enTypeOf.Salary & " AND isdefault = 1 ", False, False, False, 2)
            'Sohail (14 Jan 2022) -- Start
            'dsList = objHead.getComboList(strDatabaseName, "List", False, 0, 0, -1, True, True, "typeof_id <> " & enTypeOf.Salary & " AND isdefault = 1 AND calctype_id <> " & enCalcType.EMPLOYER_CONTRIBUTION_PAYABLE & " ", False, False, False, 2)
            dsList = objHead.getComboList(strDatabaseName, "List", False, 0, 0, -1, True, True, "typeof_id <> " & enTypeOf.Salary & " AND isdefault = 1 AND calctype_id <> " & enCalcType.EMPLOYER_CONTRIBUTION_PAYABLE & " ", False, False, False, 2, , objDataOperation)
            objDataOperation.ClearParameters()
            'Sohail (14 Jan 2022) -- End
            'Sohail (21 Aug 2021) -- End

            Me._DataOperation = objDataOperation
            Me._Employeeunkid(strDatabaseName) = 999999
            Dim dtTable As DataTable = Me._DataSource

            dtTable.Rows.Clear()

            If dsList.Tables(0).Rows.Count > 0 Then

                For Each dsRow As DataRow In dsList.Tables(0).Rows

                    Dim dr As DataRow = dtTable.NewRow

                    dr.Item("tranheadunkid") = dsRow.Item("tranheadunkid")
                    dr.Item("batchtransactionunkid") = -1
                    dr.Item("currencyid") = 0
                    dr.Item("Amount") = 0
                    dr.Item("vendorid") = -1
                    dr.Item("periodunkid") = intPeriodId
                    dr.Item("costcenterunkid") = 0
                    dr.Item("medicalrefno") = ""
                    dr.Item("start_date") = eZeeDate.convertDate(dtPeriodStart)
                    dr.Item("end_date") = eZeeDate.convertDate(dtPeriodEnd)
                    dr.Item("cumulative_startdate") = DBNull.Value
                    dr.Item("stop_date") = DBNull.Value
                    'Sohail (24 Aug 2019) -- Start
                    'NMB Payroll UAT # TC016 - 76.1 - System should be able to accept future amendments (Future period) and prorate. It should be possible to set start date of the specific transaction and prorate based on the start date. It should also be possible to set start date of the future period.
                    dr.Item("edstart_date") = DBNull.Value
                    'Sohail (24 Aug 2019) -- End
                    dr.Item("empbatchpostingunkid") = -1
                    dr.Item("membershiptranunkid") = 0
                    dr.Item("disciplinefileunkid") = 0

                    dtTable.Rows.Add(dr)
                Next

                blnResult = InsertAllByDataTable(strDatabaseName, intYearUnkId, intCompanyUnkId, dtPeriodStart, dtPeriodEnd, strUserModeSetting, blnOnlyApproved, xIncludeIn_ActiveEmployee, objDataOperation, strEmployeeIDs, dtTable, blnOverWrite, intUserUnkId, blnAllowToApproveEarningDeduction, dtCurrentDateAndTime, blnInsertSalaryHead, blnCopyPreviousEDSlab, blnOverwritePreviousEDSlabHeads, blnApplyUserAccessFilter, strFilerString, blnCheckMembershipHeads)

                If blnResult = False Then
                    Return False
                End If
            End If

            Return True

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: AssignDefaultTransactionHeads; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            objHead = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xDataOp Is Nothing Then objDataOperation = Nothing
        End Try
    End Function

    'Sohail (18 Feb 2019) -- End

    'Sohail (06 Sep 2019) -- Start
    'Mainspring Resourcing Ghana Ltd REQ # 0004099: Email Notification to approve / reject Flat Rate Amounts Uploaded/edited/updated		
    Public Function ApproveAll(ByVal xDataOp As clsDataOperation _
                               , ByVal dtTable As DataTable _
                               , ByVal blnIsApprove As Boolean _
                               , ByVal intUserUnkId As Integer _
                               , ByVal dtCurrentDateAndTime As Date _
                               , ByVal strIP As String _
                               , ByVal strHostName As String _
                               , ByVal strFormName As String _
                               , ByVal intLogEmployeeUnkid As Integer _
                               , ByVal blnIsweb As Boolean _
                               ) As Boolean

        Dim strUnkIDs As String = ""
        Dim strQ As String = ""

        If xDataOp IsNot Nothing Then
            objDataOperation = xDataOp
        Else
            objDataOperation = New clsDataOperation
        End If
        objDataOperation.ClearParameters()

        Try
            strUnkIDs = String.Join(",", (From p In dtTable Select (p.Item("edunkid").ToString)).ToArray)

            If isExistEDApproval(objDataOperation, strUnkIDs, blnIsApprove) Then
                mstrMessage = Language.getMessage(mstrModuleName, 21, "Sorry, ED Approval is already done for some of the selected transaction heads. Please refresh the list and try again.")
                Return False
            End If

            objDataOperation.ClearParameters()
            If xDataOp Is Nothing Then objDataOperation.BindTransaction()

            strQ = "UPDATE prearningdeduction_master SET " & _
                       " isapproved = @isapproved " & _
                       ", approveruserunkid = @approveruserunkid " & _
                   "WHERE edunkid IN (" & strUnkIDs & ") "

            objDataOperation.AddParameter("@isapproved", SqlDbType.Bit, eZeeDataType.BIT_SIZE, blnIsApprove)
            objDataOperation.AddParameter("@approveruserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUserUnkId)

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                objDataOperation.ReleaseTransaction(False)
                Throw New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
            End If

            strQ = "INSERT INTO atprearningdeduction_master ( " & _
                        "  edunkid " & _
                        ", employeeunkid " & _
                        ", tranheadunkid " & _
                        ", batchtransactionunkid " & _
                        ", amount " & _
                        ", isdeduct " & _
                        ", trnheadtype_id " & _
                        ", typeof_id " & _
                        ", calctype_id " & _
                        ", computeon_id " & _
                        ", formula " & _
                        ", formulaid " & _
                        ", currencyunkid " & _
                        ", vendorunkid " & _
                        ", membership_categoryunkid " & _
                        ", isapproved " & _
                        ", approveruserunkid " & _
                        ", periodunkid " & _
                        ", medicalrefno " & _
                        ", disciplinefileunkid " & _
                        ", membershiptranunkid " & _
                        ", cumulative_startdate " & _
                        ", stop_date " & _
                        ", empbatchpostingunkid " & _
                        ", edstart_date " & _
                        ", audittype " & _
                        ", audituserunkid " & _
                        ", auditdatetime " & _
                        ", ip " & _
                        ", machine_name" & _
                        ", form_name " & _
                        " " & _
                        " " & _
                        " " & _
                        " " & _
                        " " & _
                        ", isweb " & _
                        ", loginemployeeunkid " & _
                        " ) " & _
                   " SELECT " & _
                        "  prearningdeduction_master.edunkid " & _
                        ", prearningdeduction_master.employeeunkid " & _
                        ", prearningdeduction_master.tranheadunkid " & _
                        ", prearningdeduction_master.batchtransactionunkid " & _
                        ", prearningdeduction_master.amount " & _
                        ", prearningdeduction_master.isdeduct " & _
                        ", prearningdeduction_master.trnheadtype_id " & _
                        ", prearningdeduction_master.typeof_id " & _
                        ", prearningdeduction_master.calctype_id " & _
                        ", prearningdeduction_master.computeon_id " & _
                        ", prearningdeduction_master.formula " & _
                        ", prearningdeduction_master.formulaid " & _
                        ", prearningdeduction_master.currencyunkid " & _
                        ", prearningdeduction_master.vendorunkid " & _
                        ", prearningdeduction_master.membership_categoryunkid " & _
                        ", prearningdeduction_master.isapproved " & _
                        ", prearningdeduction_master.approveruserunkid " & _
                        ", prearningdeduction_master.periodunkid " & _
                        ", prearningdeduction_master.medicalrefno " & _
                        ", prearningdeduction_master.disciplinefileunkid " & _
                        ", prearningdeduction_master.membershiptranunkid " & _
                        ", prearningdeduction_master.cumulative_startdate " & _
                        ", prearningdeduction_master.stop_date " & _
                        ", prearningdeduction_master.empbatchpostingunkid " & _
                        ", prearningdeduction_master.edstart_date " & _
                        ", @audittype " & _
                        ", @audituserunkid " & _
                        ", @auditdatetime " & _
                        ", @ip " & _
                        ", @machine_name" & _
                        ", @form_name " & _
                        " " & _
                        " " & _
                        " " & _
                        " " & _
                        " " & _
                        ", @isweb " & _
                        ", @loginemployeeunkid " & _
                   "FROM     prearningdeduction_master " & _
                   "         LEFT JOIN prtranhead_master ON prearningdeduction_master.tranheadunkid = prtranhead_master.tranheadunkid " & _
                   "WHERE    prearningdeduction_master.isvoid = 0 " & _
                            "AND prtranhead_master.isvoid = 0 " & _
                            "AND prearningdeduction_master.edunkid IN (" & strUnkIDs & ") "

            'Hemant (23 Apr 2020) -- [", '' " & _ --> " " & _]


            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@audittype", SqlDbType.Int, eZeeDataType.INT_SIZE, 2)
            objDataOperation.AddParameter("@audituserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUserUnkId)
            objDataOperation.AddParameter("@auditdatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, dtCurrentDateAndTime)
            objDataOperation.AddParameter("@ip", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strIP)
            objDataOperation.AddParameter("@machine_name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strHostName)
            objDataOperation.AddParameter("@form_name", SqlDbType.NVarChar, 500, strFormName)
            objDataOperation.AddParameter("@isweb", SqlDbType.Bit, eZeeDataType.BIT_SIZE, blnIsweb)
            objDataOperation.AddParameter("@loginemployeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intLogEmployeeUnkid)

            objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                objDataOperation.ReleaseTransaction(False)
                Throw New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
            End If

            If xDataOp Is Nothing Then objDataOperation.ReleaseTransaction(True)

            Return True

        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: ApproveAll; Module Name: " & mstrModuleName)
            Return False
        Finally
            If xDataOp Is Nothing Then objDataOperation = Nothing
        End Try
    End Function

    Public Function isExistEDApproval(ByVal xDataOp As clsDataOperation _
                                      , ByVal strEDUnkIDs As String _
                                      , ByVal bnlIsApprove As Boolean _
                                      ) As Boolean

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        If xDataOp IsNot Nothing Then
            objDataOperation = xDataOp
        Else
            objDataOperation = New clsDataOperation
        End If
        objDataOperation.ClearParameters()

        Try
            strQ = "SELECT " & _
                  "  1 " & _
                 "FROM prearningdeduction_master " & _
                 "WHERE isvoid = 0 " & _
                 "AND edunkid IN ( " & strEDUnkIDs & " ) " & _
                 "AND isapproved = @isapproved "

            objDataOperation.AddParameter("@isapproved", SqlDbType.Bit, eZeeDataType.BIT_SIZE, bnlIsApprove)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables(0).Rows.Count > 0

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isExistEDApproval; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xDataOp Is Nothing Then objDataOperation = Nothing
        End Try
    End Function

    Public Function VoidAll(ByVal xDataOp As clsDataOperation _
                            , ByVal xYearUnkid As Integer _
                            , ByVal dtTable As DataTable _
                            , ByVal strVoidReason As String _
                            , ByVal intUserUnkId As Integer _
                            , ByVal dtCurrentDateAndTime As Date _
                            , ByVal strIP As String _
                            , ByVal strHostName As String _
                            , ByVal strFormName As String _
                            , ByVal intLogEmployeeUnkid As Integer _
                            , ByVal blnIsweb As Boolean _
                            ) As Boolean

        Dim strUnkIDs As String = ""
        Dim strOtherUnkIDs As String = ""
        Dim strQ As String = ""
        Dim dsList As DataSet = Nothing
        Dim objMData As New clsMasterData
        Dim objMTran As New clsMembershipTran

        If xDataOp IsNot Nothing Then
            objDataOperation = xDataOp
        Else
            objDataOperation = New clsDataOperation
        End If
        objDataOperation.ClearParameters()

        Try
            strUnkIDs = String.Join(",", (From p In dtTable Select (p.Item("edunkid").ToString)).ToArray)

            If isExistVoidedED(objDataOperation, strUnkIDs) Then
                mstrMessage = Language.getMessage(mstrModuleName, 22, "Sorry, Some of the selected transaction heads are already voided. Please refresh the list and try again.")
                Return False
            End If

            If xDataOp Is Nothing Then objDataOperation.BindTransaction()

            Dim intCurrPeriodUnkid As Integer = objMData.getFirstPeriodID(enModuleReference.Payroll, xYearUnkid, enStatusType.OPEN)

            Dim dr() As DataRow = dtTable.Select("membershiptranunkid > 0")
            If dr.Length > 0 Then

                strQ = "SELECT edunkid, employeeunkid, periodunkid, membershiptranunkid INTO #ed FROM prearningdeduction_master WHERE isvoid = 0 AND edunkid IN (" & strUnkIDs & ") " & _
                       "SELECT prearningdeduction_master.edunkid FROM prearningdeduction_master " & _
                       "JOIN #ed ON #ed.employeeunkid = prearningdeduction_master.employeeunkid " & _
                            "AND #ed.periodunkid = prearningdeduction_master.periodunkid " & _
                            "AND #ed.membershiptranunkid = prearningdeduction_master.membershiptranunkid " & _
                       "WHERE prearningdeduction_master.isvoid = 0 " & _
                       "AND prearningdeduction_master.membershiptranunkid > 0 " & _
                       "AND prearningdeduction_master.edunkid NOT IN (" & strUnkIDs & ") "

                strQ &= "DROP TABLE #ed "

                dsList = objDataOperation.ExecQuery(strQ, "List")

                If objDataOperation.ErrorMessage <> "" Then
                    objDataOperation.ReleaseTransaction(False)
                    Throw New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                End If

                If dsList.Tables(0).Rows.Count > 0 Then
                    strOtherUnkIDs = String.Join(",", (From p In dsList.Tables(0) Select (p.Item("edunkid").ToString)).Distinct.ToArray)
                End If
            End If

            strQ = "INSERT INTO atprearningdeduction_master ( " & _
                        "  edunkid " & _
                        ", employeeunkid " & _
                        ", tranheadunkid " & _
                        ", batchtransactionunkid " & _
                        ", amount " & _
                        ", isdeduct " & _
                        ", trnheadtype_id " & _
                        ", typeof_id " & _
                        ", calctype_id " & _
                        ", computeon_id " & _
                        ", formula " & _
                        ", formulaid " & _
                        ", currencyunkid " & _
                        ", vendorunkid " & _
                        ", membership_categoryunkid " & _
                        ", isapproved " & _
                        ", approveruserunkid " & _
                        ", periodunkid " & _
                        ", medicalrefno " & _
                        ", disciplinefileunkid " & _
                        ", membershiptranunkid " & _
                        ", cumulative_startdate " & _
                        ", stop_date " & _
                        ", empbatchpostingunkid " & _
                        ", edstart_date " & _
                        ", audittype " & _
                        ", audituserunkid " & _
                        ", auditdatetime " & _
                        ", ip " & _
                        ", machine_name" & _
                        ", form_name " & _
                        " " & _
                        " " & _
                        " " & _
                        " " & _
                        " " & _
                        ", isweb " & _
                        ", loginemployeeunkid " & _
                        " ) " & _
                   " SELECT " & _
                        "  prearningdeduction_master.edunkid " & _
                        ", prearningdeduction_master.employeeunkid " & _
                        ", prearningdeduction_master.tranheadunkid " & _
                        ", prearningdeduction_master.batchtransactionunkid " & _
                        ", prearningdeduction_master.amount " & _
                        ", prearningdeduction_master.isdeduct " & _
                        ", prearningdeduction_master.trnheadtype_id " & _
                        ", prearningdeduction_master.typeof_id " & _
                        ", prearningdeduction_master.calctype_id " & _
                        ", prearningdeduction_master.computeon_id " & _
                        ", prearningdeduction_master.formula " & _
                        ", prearningdeduction_master.formulaid " & _
                        ", prearningdeduction_master.currencyunkid " & _
                        ", prearningdeduction_master.vendorunkid " & _
                        ", prearningdeduction_master.membership_categoryunkid " & _
                        ", prearningdeduction_master.isapproved " & _
                        ", prearningdeduction_master.approveruserunkid " & _
                        ", prearningdeduction_master.periodunkid " & _
                        ", prearningdeduction_master.medicalrefno " & _
                        ", prearningdeduction_master.disciplinefileunkid " & _
                        ", prearningdeduction_master.membershiptranunkid " & _
                        ", prearningdeduction_master.cumulative_startdate " & _
                        ", prearningdeduction_master.stop_date " & _
                        ", prearningdeduction_master.empbatchpostingunkid " & _
                        ", prearningdeduction_master.edstart_date " & _
                        ", @audittype " & _
                        ", @audituserunkid " & _
                        ", @auditdatetime " & _
                        ", @ip " & _
                        ", @machine_name" & _
                        ", @form_name " & _
                        " " & _
                        " " & _
                        " " & _
                        " " & _
                        " " & _
                        ", @isweb " & _
                        ", @loginemployeeunkid " & _
                   "FROM     prearningdeduction_master " & _
                   "         LEFT JOIN prtranhead_master ON prearningdeduction_master.tranheadunkid = prtranhead_master.tranheadunkid " & _
                   "WHERE    prearningdeduction_master.isvoid = 0 " & _
                            "AND prtranhead_master.isvoid = 0 " & _
                            "AND prearningdeduction_master.edunkid IN (" & strUnkIDs & ") "
            'Hemant (23 Apr 2020) -- [", '' " & _  --> " " & _]

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@audittype", SqlDbType.Int, eZeeDataType.INT_SIZE, 3)
            objDataOperation.AddParameter("@audituserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUserUnkId)
            objDataOperation.AddParameter("@auditdatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, dtCurrentDateAndTime)
            objDataOperation.AddParameter("@ip", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strIP)
            objDataOperation.AddParameter("@machine_name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strHostName)
            objDataOperation.AddParameter("@form_name", SqlDbType.NVarChar, 500, strFormName)
            objDataOperation.AddParameter("@isweb", SqlDbType.Bit, eZeeDataType.BIT_SIZE, blnIsweb)
            objDataOperation.AddParameter("@loginemployeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intLogEmployeeUnkid)

            objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                objDataOperation.ReleaseTransaction(False)
                Throw New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
            End If

            strQ = "UPDATE prearningdeduction_master SET " & _
                     " isvoid = 1 " & _
                     ", voiduserunkid = @voiduserunkid" & _
                     ", voiddatetime = @voiddatetime " & _
                     ", voidreason = @voidreason " & _
                   "WHERE isvoid = 0 " & _
                   "AND edunkid IN (" & strUnkIDs & ") "

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUserUnkId.ToString)
            objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, dtCurrentDateAndTime)
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strVoidReason.ToString)

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                objDataOperation.ReleaseTransaction(False)
                Throw New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
            End If

            '*** Void other head(employee / employer) if head is membership head
            If strOtherUnkIDs.Trim.Length > 0 Then

                strQ = "INSERT INTO atprearningdeduction_master ( " & _
                            "  edunkid " & _
                            ", employeeunkid " & _
                            ", tranheadunkid " & _
                            ", batchtransactionunkid " & _
                            ", amount " & _
                            ", isdeduct " & _
                            ", trnheadtype_id " & _
                            ", typeof_id " & _
                            ", calctype_id " & _
                            ", computeon_id " & _
                            ", formula " & _
                            ", formulaid " & _
                            ", currencyunkid " & _
                            ", vendorunkid " & _
                            ", membership_categoryunkid " & _
                            ", isapproved " & _
                            ", approveruserunkid " & _
                            ", periodunkid " & _
                            ", medicalrefno " & _
                            ", disciplinefileunkid " & _
                            ", membershiptranunkid " & _
                            ", cumulative_startdate " & _
                            ", stop_date " & _
                            ", empbatchpostingunkid " & _
                            ", edstart_date " & _
                            ", audittype " & _
                            ", audituserunkid " & _
                            ", auditdatetime " & _
                            ", ip " & _
                            ", machine_name" & _
                            ", form_name " & _
                            " " & _
                            " " & _
                            " " & _
                            " " & _
                            " " & _
                            ", isweb " & _
                            ", loginemployeeunkid " & _
                            " ) " & _
                       " SELECT " & _
                            "  prearningdeduction_master.edunkid " & _
                            ", prearningdeduction_master.employeeunkid " & _
                            ", prearningdeduction_master.tranheadunkid " & _
                            ", prearningdeduction_master.batchtransactionunkid " & _
                            ", prearningdeduction_master.amount " & _
                            ", prearningdeduction_master.isdeduct " & _
                            ", prearningdeduction_master.trnheadtype_id " & _
                            ", prearningdeduction_master.typeof_id " & _
                            ", prearningdeduction_master.calctype_id " & _
                            ", prearningdeduction_master.computeon_id " & _
                            ", prearningdeduction_master.formula " & _
                            ", prearningdeduction_master.formulaid " & _
                            ", prearningdeduction_master.currencyunkid " & _
                            ", prearningdeduction_master.vendorunkid " & _
                            ", prearningdeduction_master.membership_categoryunkid " & _
                            ", prearningdeduction_master.isapproved " & _
                            ", prearningdeduction_master.approveruserunkid " & _
                            ", prearningdeduction_master.periodunkid " & _
                            ", prearningdeduction_master.medicalrefno " & _
                            ", prearningdeduction_master.disciplinefileunkid " & _
                            ", prearningdeduction_master.membershiptranunkid " & _
                            ", prearningdeduction_master.cumulative_startdate " & _
                            ", prearningdeduction_master.stop_date " & _
                            ", prearningdeduction_master.empbatchpostingunkid " & _
                            ", prearningdeduction_master.edstart_date " & _
                            ", @audittype " & _
                            ", @audituserunkid " & _
                            ", @auditdatetime " & _
                            ", @ip " & _
                            ", @machine_name" & _
                            ", @form_name " & _
                            " " & _
                            " " & _
                            " " & _
                            " " & _
                            " " & _
                            ", @isweb " & _
                            ", @loginemployeeunkid " & _
                       "FROM     prearningdeduction_master " & _
                       "         LEFT JOIN prtranhead_master ON prearningdeduction_master.tranheadunkid = prtranhead_master.tranheadunkid " & _
                       "WHERE    prearningdeduction_master.isvoid = 0 " & _
                                "AND prtranhead_master.isvoid = 0 " & _
                                "AND prearningdeduction_master.edunkid IN (" & strOtherUnkIDs & ") "

                'Hemant (23 Apr 2020) -- [", '' " & _ --> " " & _]

                objDataOperation.ClearParameters()
                objDataOperation.AddParameter("@audittype", SqlDbType.Int, eZeeDataType.INT_SIZE, 3)
                objDataOperation.AddParameter("@audituserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUserUnkId)
                objDataOperation.AddParameter("@auditdatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, dtCurrentDateAndTime)
                objDataOperation.AddParameter("@ip", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strIP)
                objDataOperation.AddParameter("@machine_name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strHostName)
                objDataOperation.AddParameter("@form_name", SqlDbType.NVarChar, 500, strFormName)
                objDataOperation.AddParameter("@isweb", SqlDbType.Bit, eZeeDataType.BIT_SIZE, blnIsweb)
                objDataOperation.AddParameter("@loginemployeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intLogEmployeeUnkid)

                objDataOperation.ExecNonQuery(strQ)

                If objDataOperation.ErrorMessage <> "" Then
                    objDataOperation.ReleaseTransaction(False)
                    Throw New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                End If

                strQ = "UPDATE prearningdeduction_master SET " & _
                     " isvoid = 1 " & _
                     ", voiduserunkid = @voiduserunkid" & _
                     ", voiddatetime = @voiddatetime " & _
                     ", voidreason = @voidreason " & _
                   "WHERE isvoid = 0 " & _
                   "AND edunkid IN (" & strOtherUnkIDs & ") "

                objDataOperation.ClearParameters()
                objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUserUnkId.ToString)
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, dtCurrentDateAndTime)
                objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strVoidReason.ToString)

                Call objDataOperation.ExecNonQuery(strQ)

                If objDataOperation.ErrorMessage <> "" Then
                    objDataOperation.ReleaseTransaction(False)
                    Throw New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                End If
            End If

            '*** Void membership if head is membership head
            If intCurrPeriodUnkid > 0 Then
                Dim lstMembershipTran = (From p In dtTable Where (CInt(p.Item("periodunkid")) = intCurrPeriodUnkid AndAlso CInt(p.Item("membershiptranunkid")) > 0) Select New With {Key .EmployeeUnkId = CInt(p.Item("employeeunkid")), Key .MembershipTranUnkId = CInt(p.Item("membershiptranunkid"))}).ToList

                If lstMembershipTran.Count > 0 Then
                    For Each item In lstMembershipTran
                        If objMTran.Void_Membership(item.MembershipTranUnkId, item.EmployeeUnkId, objDataOperation) = False Then
                            objDataOperation.ReleaseTransaction(False)
                            Exit Try
                        End If
                    Next
                End If
            End If
            
            If xDataOp Is Nothing Then objDataOperation.ReleaseTransaction(True)

            Return True

        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: VoidAll; Module Name: " & mstrModuleName)
            Return False
        Finally
            If xDataOp Is Nothing Then objDataOperation = Nothing
        End Try
    End Function

    Public Function isExistVoidedED(ByVal xDataOp As clsDataOperation _
                                    , ByVal strEDUnkIDs As String _
                                    ) As Boolean

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        If xDataOp IsNot Nothing Then
            objDataOperation = xDataOp
        Else
            objDataOperation = New clsDataOperation
        End If
        objDataOperation.ClearParameters()

        Try
            strQ = "SELECT " & _
                  "  1 " & _
                 "FROM prearningdeduction_master " & _
                 "WHERE isvoid = 1 " & _
                 "AND edunkid IN ( " & strEDUnkIDs & " ) "

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables(0).Rows.Count > 0

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isExistVoidedED; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xDataOp Is Nothing Then objDataOperation = Nothing
        End Try
    End Function
    'Sohail (06 Sep 2019) -- End

    'Sohail (02 Sep 2019) -- Start
    'Mainspring Resourcing Ghana Ltd Enhancement - 76.1 - # 0004098: Approval Flow for Flat Rate Heads Amount Uploaded.
    '1. Set ED in pending status if user has not priviledge of ED approval
    '2. Perform Global approver with option to filter for allocation and approved any posted (new /edited) E & D values in MSS
    '3. Send notifications to ED approvers for all employees ED in one email
    Public Sub SendMailToApprover(ByVal xDatabaseName As String _
                                  , ByVal xYearUnkid As Integer _
                                  , ByVal xCompanyUnkid As Integer _
                                  , ByVal intUserUnkid As Integer _
                                  , ByVal xPeriodStart As Date _
                                  , ByVal xPeriodEnd As Date _
                                  , ByVal xUserModeSetting As String _
                                  , ByVal xOnlyApproved As Boolean _
                                  , ByVal xIncludeIn_ActiveEmployee As Boolean _
                                  , ByVal blnAllowToApproveEarningDeduction As Boolean _
                                  , ByVal dtTable As DataTable _
                                  , ByVal intPeriodUnkId As Integer _
                                  , ByVal strFmtCurrency As String _
                                  , ByVal en_Login_Mode As enLogin_Mode _
                                  , ByVal mstrWebFrmName As String _
                                  , ByVal iLoginEmployeeId As Integer _
                                  )

        Dim objMaster As New clsMasterData
        Dim dsList As DataSet = Nothing
        Dim objUser As New clsUserAddEdit
        Dim objMail As New clsSendMail
        Dim objPeriod As New clscommom_period_Tran
        Dim StrMessage As String = String.Empty
        Dim strUserName As String = ""
        Dim strUserEmail As String = ""
        Dim strApproverName As String = ""
        Dim strApproverEmail As String = ""
        Dim dUList As New DataSet
        Try
            If dtTable Is Nothing OrElse dtTable.Rows.Count <= 0 Then Exit Try
            'Sohail (31 Oct 2019) -- Start
            'Mainspring Resourcing Ghana : Support Issue Id # 0004199 - Email notification not sent to approvers when there is uploaded, edited e & d.
            dtTable = New DataView(dtTable, "calctype_id = " & enCalcType.FlatRate_Others & " ", "", DataViewRowState.CurrentRows).ToTable
            If dtTable Is Nothing OrElse dtTable.Rows.Count <= 0 Then Exit Try
            'Sohail (31 Oct 2019) -- End

            Dim objNet As New clsNetConnectivity
            If objNet._Conected = False Then Exit Sub

            If blnAllowToApproveEarningDeduction = True Then Exit Try

            gobjEmailList = New List(Of clsEmailCollection)

            objUser._Userunkid = intUserUnkid
            strUserName = objUser._Firstname & " " & objUser._Lastname
            If strUserName.Trim.Length <= 0 Then strUserName = objUser._Username
            strUserEmail = objUser._Email

            objPeriod._Periodunkid(xDatabaseName) = intPeriodUnkId

            Dim strEmpIDs As String = String.Join(",", (From p In dtTable Select (p.Item("employeeunkid").ToString)).Distinct.ToArray)

            'Sohail (10 Dec 2019) -- Start
            'MAIN SPRING RESOURCES Support Issue Id # 0004311 : Add employee code, cost centre and class group columns in email notification.
            Dim dsAllocation As DataSet = (New clsMasterData).GetEAllocation_Notification("List", , , True)

            If xDataOperat IsNot Nothing Then
                objDataOperation = xDataOperat
            Else
                objDataOperation = New clsDataOperation
            End If
            objDataOperation.ClearParameters()

            Dim strQ As String = ""
            Dim strEmp As String = ""

            strEmp = "SELECT employeeunkid, employeecode, classgroupunkid, costcenterunkid INTO #tblEmp FROM hremployee_master WHERE employeeunkid IN (" & strEmpIDs & ") "

            strQ = strEmp
            strQ &= "SELECT * INTO #TRF " & _
                    "FROM " & _
                      "(SELECT ETT.employeeunkid " & _
                            ", ETT.classgroupunkid " & _
                            ", DENSE_RANK() OVER (PARTITION BY ETT.employeeunkid ORDER BY ETT.effectivedate DESC) AS ROWNO " & _
                       "FROM hremployee_transfer_tran AS ETT " & _
                       "JOIN #tblEmp ON #tblEmp.employeeunkid = ETT.employeeunkid " & _
                       "LEFT JOIN cfcommon_master AS RTT ON RTT.masterunkid = ETT.changereasonunkid " & _
                        "AND RTT.mastertype = " & clsCommon_Master.enCommonMaster.RE_HIRE & " " & _
                       "LEFT JOIN cfcommon_master AS TTC ON TTC.masterunkid = ETT.changereasonunkid " & _
                        "AND TTC.mastertype = " & clsCommon_Master.enCommonMaster.TRANSFERS & " " & _
                       "WHERE ETT.isvoid = 0 " & _
                         "AND CONVERT(CHAR(8), ETT.effectivedate, 112) <= @enddate) AS Trf " & _
                    "WHERE Trf.ROWNO = 1 "

            strQ &= "SELECT * INTO #CCT " & _
                   "FROM " & _
                     "(SELECT CTT.employeeunkid " & _
                           ", CTT.cctranheadvalueid AS costcenterunkid " & _
                           ", DENSE_RANK() OVER (PARTITION BY CTT.employeeunkid ORDER BY CTT.effectivedate DESC) AS ROWNO " & _
                      "FROM hremployee_cctranhead_tran AS CTT " & _
                      "JOIN #tblEmp ON #tblEmp.employeeunkid = CTT.employeeunkid " & _
                      "LEFT JOIN cfcommon_master AS RTT ON RTT.masterunkid = CTT.changereasonunkid " & _
                       "AND RTT.mastertype = " & clsCommon_Master.enCommonMaster.RE_HIRE & " " & _
                      "LEFT JOIN cfcommon_master AS TTC ON TTC.masterunkid = CTT.changereasonunkid " & _
                       "AND TTC.mastertype = " & clsCommon_Master.enCommonMaster.COST_CENTER & " " & _
                      "WHERE CTT.isvoid = 0 " & _
                        "AND CONVERT(CHAR(8), CTT.effectivedate, 112) <= @enddate) AS CTT " & _
                   "WHERE CTT.ROWNO = 1 "

            strQ &= "SELECT #tblEmp.employeeunkid " & _
                         ", #tblEmp.employeecode " & _
                         ", ISNULL(#TRF.classgroupunkid, #tblEmp.classgroupunkid) AS classgroupunkid " & _
                         ", ISNULL(hrclassgroup_master.name, '') AS classgroupname " & _
                         ", ISNULL(#CCT.costcenterunkid, #tblEmp.costcenterunkid) AS costcenterunkid " & _
                         ", ISNULL(prcostcenter_master.costcentername, '') AS costcentername " & _
                    "FROM #tblEmp " & _
                    "LEFT JOIN #TRF ON #tblEmp.employeeunkid = #TRF.employeeunkid " & _
                    "LEFT JOIN #CCT ON #tblEmp.employeeunkid = #CCT.employeeunkid " & _
                    "LEFT JOIN hrclassgroup_master ON #TRF.classgroupunkid = hrclassgroup_master.classgroupunkid " & _
                    "LEFT JOIN prcostcenter_master ON #CCT.costcenterunkid = prcostcenter_master.costcenterunkid " & _
                    "AND hrclassgroup_master.isactive = 1 "

            strQ &= "DROP TABLE #TRF "
            strQ &= "DROP TABLE #CCT "
            strQ &= "DROP TABLE #tblEmp "

            objDataOperation.AddParameter("@enddate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(xPeriodEnd))

            Dim dsEmp As DataSet = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                Throw New Exception(objDataOperation.ErrorMessage)
                Exit Sub
            End If

            strQ = strEmp
            strQ &= "SELECT * " & _
                    "FROM " & _
                      "(SELECT premployee_costcenter_tran.employeeunkid " & _
                            ", #tblEmp.employeecode " & _
                            ", premployee_costcenter_tran.tranheadunkid " & _
                            ", ISNULL(premployee_costcenter_tran.allocationbyid, " & enAllocation.COST_CENTER & ") AS allocationbyid " & _
                            ", premployee_costcenter_tran.costcenterunkid " & _
                            ", ISNULL(premployee_costcenter_tran.isinactive, 0) AS isinactive " & _
                            ", DENSE_RANK() OVER (PARTITION BY premployee_costcenter_tran.employeeunkid, premployee_costcenter_tran.tranheadunkid ORDER BY cfcommon_period_tran.end_date DESC) AS ROWNO "
            'Sohail (25 Jul 2020) = [isinactive]

            strQ &= ", CASE ISNULL(premployee_costcenter_tran.allocationbyid, " & enAllocation.COST_CENTER & ") "
            For Each dsRow As DataRow In dsAllocation.Tables(0).Rows
                strQ &= " WHEN " & CInt(dsRow.Item("Id")) & "  THEN '" & dsRow.Item("Name").ToString & "' "
            Next
            strQ &= " END AS AllocationByName "

            strQ &= ", CASE ISNULL(premployee_costcenter_tran.allocationbyid, " & enAllocation.COST_CENTER & ") "
            For Each dsRow As DataRow In dsAllocation.Tables(0).Rows
                If CInt(dsRow.Item("Id")) <= 0 Then
                    strQ &= " WHEN " & CInt(dsRow.Item("Id")) & "  THEN '" & dsRow.Item("AllocationName").ToString & "' "
                Else
                    strQ &= " WHEN " & CInt(dsRow.Item("Id")) & "  THEN " & dsRow.Item("AllocationName").ToString & " "
                End If
            Next
            strQ &= " END AS AllocationName "

            strQ &= "FROM premployee_costcenter_tran " & _
                    "JOIN #tblEmp ON #tblEmp.employeeunkid = premployee_costcenter_tran.employeeunkid " & _
                       "LEFT JOIN prcostcenter_master ON prcostcenter_master.costcenterunkid = premployee_costcenter_tran.costcenterunkid " & _
                            "AND ISNULL(premployee_costcenter_tran.allocationbyid, " & enAllocation.COST_CENTER & ") = " & enAllocation.COST_CENTER & " " & _
                       "LEFT JOIN cfcommon_period_tran ON premployee_costcenter_tran.periodunkid = cfcommon_period_tran.periodunkid " & _
                       "LEFT JOIN hrdepartment_master ON hrdepartment_master.departmentunkid = premployee_costcenter_tran.costcenterunkid AND ISNULL(premployee_costcenter_tran.allocationbyid, " & enAllocation.COST_CENTER & ") = " & CInt(enAllocation.DEPARTMENT) & " " & _
                        "LEFT JOIN hrjob_master ON hrjob_master.jobunkid = premployee_costcenter_tran.costcenterunkid AND ISNULL(premployee_costcenter_tran.allocationbyid, " & enAllocation.COST_CENTER & ") = " & CInt(enAllocation.JOBS) & " " & _
                        "LEFT JOIN hrstation_master ON hrstation_master.stationunkid = premployee_costcenter_tran.costcenterunkid AND ISNULL(premployee_costcenter_tran.allocationbyid, " & enAllocation.COST_CENTER & ") = " & CInt(enAllocation.BRANCH) & " " & _
                        "LEFT JOIN hrdepartment_group_master ON hrdepartment_group_master.deptgroupunkid = premployee_costcenter_tran.costcenterunkid AND ISNULL(premployee_costcenter_tran.allocationbyid, " & enAllocation.COST_CENTER & ") = " & CInt(enAllocation.DEPARTMENT_GROUP) & " " & _
                        "LEFT JOIN hrsection_master ON hrsection_master.sectionunkid = premployee_costcenter_tran.costcenterunkid AND ISNULL(premployee_costcenter_tran.allocationbyid, " & enAllocation.COST_CENTER & ") = " & CInt(enAllocation.SECTION) & " " & _
                        "LEFT JOIN hrunit_master ON hrunit_master.unitunkid = premployee_costcenter_tran.costcenterunkid AND ISNULL(premployee_costcenter_tran.allocationbyid, " & enAllocation.COST_CENTER & ") = " & CInt(enAllocation.UNIT) & " " & _
                        "LEFT JOIN hrjobgroup_master ON hrjobgroup_master.jobgroupunkid = premployee_costcenter_tran.costcenterunkid AND ISNULL(premployee_costcenter_tran.allocationbyid, " & enAllocation.COST_CENTER & ") = " & CInt(enAllocation.JOB_GROUP) & " " & _
                        "LEFT JOIN hrclassgroup_master ON hrclassgroup_master.classgroupunkid = premployee_costcenter_tran.costcenterunkid AND ISNULL(premployee_costcenter_tran.allocationbyid, " & enAllocation.COST_CENTER & ") = " & CInt(enAllocation.CLASS_GROUP) & " " & _
                        "LEFT JOIN hrclasses_master ON hrclasses_master.classesunkid = premployee_costcenter_tran.costcenterunkid AND ISNULL(premployee_costcenter_tran.allocationbyid, " & enAllocation.COST_CENTER & ") = " & CInt(enAllocation.CLASSES) & " " & _
                        "LEFT JOIN hrteam_master ON hrteam_master.teamunkid = premployee_costcenter_tran.costcenterunkid AND ISNULL(premployee_costcenter_tran.allocationbyid, " & enAllocation.COST_CENTER & ") = " & CInt(enAllocation.TEAM) & " " & _
                        "LEFT JOIN hrunitgroup_master ON premployee_costcenter_tran.costcenterunkid = hrunitgroup_master.unitgroupunkid AND ISNULL(premployee_costcenter_tran.allocationbyid, " & enAllocation.COST_CENTER & ") = " & CInt(enAllocation.UNIT_GROUP) & " " & _
                        "LEFT JOIN hrsectiongroup_master ON premployee_costcenter_tran.costcenterunkid = hrsectiongroup_master.sectiongroupunkid AND ISNULL(premployee_costcenter_tran.allocationbyid, " & enAllocation.COST_CENTER & ") = " & CInt(enAllocation.SECTION_GROUP) & " " & _
                       "WHERE premployee_costcenter_tran.isvoid = 0 " & _
                         "AND CONVERT(CHAR(8), cfcommon_period_tran.end_date, 112) <= @enddate) AS A " & _
                    "WHERE A.ROWNO = 1 " & _
                        "AND A.isinactive = 0 "
            'Sohail (25 Jul 2020) = [isinactive]

            'strQ &= "DROP TABLE #CC "
            strQ &= "DROP TABLE #tblEmp "

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@enddate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(xPeriodEnd))

            Dim dsCC As DataSet = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                Throw New Exception(objDataOperation.ErrorMessage)
                Exit Sub
            End If
            'Sohail (10 Dec 2019) -- End

            dUList = objUser.Get_UserBy_PrivilegeId(enUserPriviledge.AllowToApproveEarningDeduction)

            If dUList.Tables(0).Rows.Count > 0 Then
                For Each dRow As DataRow In dUList.Tables(0).Rows

                    StrMessage = "<HTML> <BODY style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#333399;'>"

                    StrMessage &= Language.getMessage(mstrModuleName, 7, "Dear") & " <B>#APPROVERNAME#</B>, <BR><BR>"

                    'StrMessage &= "  <p>" & Language.getMessage(mstrModuleName, 8, "This is the reminder for approving Earning Deduction for the period of ") & " <B>#PERIODNAME#</B>" & _
                    '                              Language.getMessage(mstrModuleName, 9, " for the following employees.")
                    StrMessage &= "  <p>" & Language.getMessage(mstrModuleName, 8, "This is to notify you that there is a flat rate transaction heads uploaded / edited in Aruti awaiting your approval.") & " " & _
                                                  Language.getMessage(mstrModuleName, 9, "Please login to the system to approve / reject the transaction.")


                    StrMessage &= "&nbsp;" & Language.getMessage(mstrModuleName, 10, "This Earning Deduction has been uploadeded / edited by") & " <B>#USERNAME#</B>." & "</p>"

                    StrMessage &= "#LIST#"


                    objUser._Userunkid = CInt(dRow.Item("UId"))
                    strApproverName = objUser._Firstname & " " & objUser._Lastname
                    If strApproverName.Trim.Length <= 0 Then strApproverName = objUser._Username
                    strApproverEmail = objUser._Email

                    Dim strAppEmpIDs As String = objMaster.GetUserAccessLevelEmployees(xDatabaseName, CInt(dRow.Item("UId")), xYearUnkid, xCompanyUnkid, xPeriodStart, xPeriodEnd, xUserModeSetting, xOnlyApproved, xIncludeIn_ActiveEmployee)

                    Dim lstEmp As List(Of DataRow) = (From p In dtTable Where strAppEmpIDs.Split(",").Contains(CInt(p.Item("employeeunkid").ToString)) Order By p.Item("employeename"), p.Item("tranheadname").ToString Select p).ToList
                    If lstEmp.Count <= 0 Then Continue For

                    'Sohail (10 Dec 2019) -- Start
                    'MAIN SPRING RESOURCES Support Issue Id # 0004311 : Add employee code, cost centre and class group columns in email notification.

                    'Sohail (10 Dec 2019) -- End

                    Dim strList As String = "<div style='max-height:500px;overflow:auto;' ><TABLE border='1' style='width:100%;'>"
                    strList &= "<TR style='background-color:Purple;color:White;'>"

                    'Sohail (10 Dec 2019) -- Start
                    'MAIN SPRING RESOURCES Support Issue Id # 0004311 : Add employee code, cost centre and class group columns in email notification.
                    strList &= "<TD align='left' style='width:15%'>"
                    strList &= Language.getMessage(mstrModuleName, 23, "Employee Code")
                    strList &= "</TD>"
                    'Sohail (10 Dec 2019) -- End

                    strList &= "<TD align='left' style='width:25%'>"
                    strList &= Language.getMessage(mstrModuleName, 11, "Employee")
                    strList &= "</TD>"

                    strList &= "<TD align='left' style='width:;15%'>"
                    strList &= Language.getMessage(mstrModuleName, 12, "Tran. Head")
                    strList &= "</TD>"

                    strList &= "<TD align='right' style='width:15%'>"
                    strList &= Language.getMessage(mstrModuleName, 13, "Amount")
                    strList &= "</TD>"

                    'Sohail (10 Dec 2019) -- Start
                    'MAIN SPRING RESOURCES Support Issue Id # 0004311 : Add employee code, cost centre and class group columns in email notification.
                    strList &= "<TD align='left' style='width:15%'>"
                    strList &= Language.getMessage(mstrModuleName, 24, "Cost Center")
                    strList &= "</TD>"

                    strList &= "<TD align='left' style='width:15%'>"
                    strList &= Language.getMessage(mstrModuleName, 25, "Class Group")
                    strList &= "</TD>"
                    'Sohail (10 Dec 2019) -- End

                    strList &= "</TR>"
                    '-----------------------------

                    For Each dtRow As DataRow In lstEmp
                        strList &= "<TR>"

                        'Sohail (10 Dec 2019) -- Start
                        'MAIN SPRING RESOURCES Support Issue Id # 0004311 : Add employee code, cost centre and class group columns in email notification.
                        Dim strEmpCode As String = ""
                        Dim strCostCenter As String = ""
                        Dim strClassGroup As String = ""

                        Dim dr() As DataRow = dsEmp.Tables(0).Select("employeeunkid = " & CInt(dtRow.Item("employeeunkid")) & " ")
                        If dr.Length > 0 Then
                            strEmpCode = dr(0).Item("employeecode").ToString
                            strClassGroup = dr(0).Item("classgroupname").ToString
                            strCostCenter = dr(0).Item("costcentername").ToString
                        End If

                        dr = dsCC.Tables(0).Select("employeeunkid = " & CInt(dtRow.Item("employeeunkid")) & " AND tranheadunkid = " & CInt(dtRow.Item("tranheadunkid")) & " ")
                        If dr.Length > 0 Then
                            strCostCenter = String.Join(", ", (From p In dr.AsEnumerable Select (p.Item("AllocationByName").ToString & ":" & p.Item("AllocationName").ToString)).ToArray)
                        End If

                        strList &= "<TD align='left' style='width:15%'>"
                        strList &= strEmpCode
                        strList &= "</TD>"
                        'Sohail (10 Dec 2019) -- End

                        strList &= "<TD align='left' style='width:25%'>"
                        strList &= dtRow.Item("employeename").ToString
                        strList &= "</TD>"

                        strList &= "<TD align='left' style='width:;15%'>"
                        strList &= dtRow.Item("tranheadname").ToString
                        strList &= "</TD>"

                        strList &= "<TD align='right' style='width:15%'>"
                        strList &= Format(CDec(dtRow.Item("amount")), strFmtCurrency)
                        strList &= "</TD>"

                        'Sohail (10 Dec 2019) -- Start
                        'MAIN SPRING RESOURCES Support Issue Id # 0004311 : Add employee code, cost centre and class group columns in email notification.
                        strList &= "<TD align='left' style='width:15%'>"
                        strList &= strClassGroup
                        strList &= "</TD>"

                        strList &= "<TD align='left' style='width:15%'>"
                        strList &= strCostCenter
                        strList &= "</TD>"
                        'Sohail (10 Dec 2019) -- End

                        strList &= "</TR>"
                    Next

                    StrMessage &= "</TABLE></div>"

                    StrMessage &= "<p><center style='padding-top:25px;font-size:10.0pt;font-family:&quot;Verdana&quot;,&quot;Sans-Serif&quot;;color:#333399;margin-left:0px;margin-right:0px;margin-top:0px;margin-bottom:10px'><b>&quot;POWERED BY ARUTI HR &amp; PAYROLL MANAGEMENT SOFTWARE.&quot;</b></center></p>"

                    StrMessage &= "</BODY></HTML>"

                    StrMessage = StrMessage.Replace("#USERNAME#", strUserName)
                    StrMessage = StrMessage.Replace("#APPROVERNAME#", strApproverName)
                    StrMessage = StrMessage.Replace("#PERIODNAME#", objPeriod._Period_Name)
                    StrMessage = StrMessage.Replace("#LIST#", strList)

                    objMail._Message = StrMessage
                    objMail._ToEmail = strApproverEmail
                    objMail._Subject = Language.getMessage(mstrModuleName, 14, "Reminder for Approving Earning Deduction")

                    If mstrWebFrmName.Trim.Length > 0 Then
                        objMail._Form_Name = mstrWebFrmName
                    End If
                    objMail._LogEmployeeUnkid = iLoginEmployeeId
                    objMail._OperationModeId = en_Login_Mode
                    objMail._UserUnkid = intUserUnkid
                    objMail._SenderAddress = IIf(strUserEmail = "", strUserName, strUserEmail)
                    objMail._ModuleRefId = clsSendMail.enAT_VIEW_TYPE.PAYROLL_MGT

                    gobjEmailList.Add(New clsEmailCollection(objMail._ToEmail, objMail._Subject, objMail._Message, mstrWebFrmName, iLoginEmployeeId, "", "", intUserUnkid, en_Login_Mode, clsSendMail.enAT_VIEW_TYPE.PAYROLL_MGT, IIf(strUserEmail = "", strUserName, strUserEmail)))
                Next

                If HttpContext.Current Is Nothing Then
                    trd = New Thread(AddressOf Send_Notification)
                    trd.IsBackground = True
                    Dim arr(1) As Object
                    arr(0) = xCompanyUnkid
                    trd.Start(arr)
                Else
                    Call Send_Notification(xCompanyUnkid)
                End If
            End If
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: SendMailToApprover; Module Name: " & mstrModuleName)
        End Try
    End Sub

    'Sohail (06 Sep 2019) -- Start
    'Mainspring Resourcing Ghana Ltd REQ # 0004099: Email Notification to approve / reject Flat Rate Amounts Uploaded/edited/updated		
    Public Sub SendMailToInitiator(ByVal xDatabaseName As String _
                                  , ByVal xYearUnkid As Integer _
                                  , ByVal xCompanyUnkid As Integer _
                                  , ByVal intUserUnkid As Integer _
                                  , ByVal xPeriodStart As Date _
                                  , ByVal xPeriodEnd As Date _
                                  , ByVal xUserModeSetting As String _
                                  , ByVal xOnlyApproved As Boolean _
                                  , ByVal xIncludeIn_ActiveEmployee As Boolean _
                                  , ByVal dtTable As DataTable _
                                  , ByVal blnIsApprove As Boolean _
                                  , ByVal strRemark As String _
                                  , ByVal strFmtCurrency As String _
                                  , ByVal en_Login_Mode As enLogin_Mode _
                                  , ByVal mstrWebFrmName As String _
                                  , ByVal iLoginEmployeeId As Integer _
                                  )

        Dim objMaster As New clsMasterData
        Dim dsList As DataSet = Nothing
        Dim objUser As New clsUserAddEdit
        Dim objMail As New clsSendMail
        Dim StrMessage As String = String.Empty
        Dim strUserName As String = ""
        Dim strUserEmail As String = ""
        Dim strInitiatorName As String = ""
        Dim strInitiatorEmail As String = ""
        Try
            If dtTable Is Nothing OrElse dtTable.Rows.Count <= 0 Then Exit Try

            Dim objNet As New clsNetConnectivity
            If objNet._Conected = False Then Exit Sub


            gobjEmailList = New List(Of clsEmailCollection)

            objUser._Userunkid = intUserUnkid
            strUserName = objUser._Firstname & " " & objUser._Lastname
            If strUserName.Trim.Length <= 0 Then strUserName = objUser._Username
            strUserEmail = objUser._Email

            Dim strEmpIDs As String = String.Join(",", (From p In dtTable Select (p.Item("employeeunkid").ToString)).Distinct.ToArray)
            Dim strUserIDs As String = String.Join(",", (From p In dtTable Select (p.Item("userunkid").ToString)).Distinct.ToArray)

            If strUserIDs.Trim.Length > 0 Then
                For Each strID As String In strUserIDs.Split(",")

                    StrMessage = "<HTML> <BODY style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#333399;'>"

                    StrMessage &= Language.getMessage(mstrModuleName, 15, "Dear") & " <B>#INITIATORNAME#</B>, <BR><BR>"

                    If blnIsApprove = True Then
                        StrMessage &= "  <p>" & Language.getMessage(mstrModuleName, 16, "This is to inform you that the transaction heads you submitted for approval has been approved by") & " <B>#USERNAME#</B>.</p>"
                    Else
                        StrMessage &= "  <p>" & Language.getMessage(mstrModuleName, 17, "This is to inform you that the transaction heads you submitted for approval has been rejected by") & " <B>#USERNAME#</B>.</p>"
                    End If

                    StrMessage &= "  <p>" & Language.getMessage(mstrModuleName, 18, "Comments :") & " " & strRemark & "</p>"

                    StrMessage &= "#LIST#"


                    objUser._Userunkid = CInt(strID)
                    strInitiatorName = objUser._Firstname & " " & objUser._Lastname
                    If strInitiatorName.Trim.Length <= 0 Then strInitiatorName = objUser._Username
                    strInitiatorEmail = objUser._Email

                    Dim strAppEmpIDs As String = objMaster.GetUserAccessLevelEmployees(xDatabaseName, CInt(strID), xYearUnkid, xCompanyUnkid, xPeriodStart, xPeriodEnd, xUserModeSetting, xOnlyApproved, xIncludeIn_ActiveEmployee)

                    Dim lstEmp As List(Of DataRow) = (From p In dtTable Where strAppEmpIDs.Split(",").Contains(CInt(p.Item("employeeunkid").ToString)) Order By p.Item("employeename"), p.Item("trnheadname").ToString Select p).ToList
                    If lstEmp.Count <= 0 Then Continue For

                    Dim strList As String = "<div style='max-height:500px;overflow:auto;' ><TABLE border='1' style='width:100%;'>"
                    strList &= "<TR style='background-color:Purple;color:White;'>"

                    strList &= "<TD align='left' style='width:40%'>"
                    strList &= Language.getMessage(mstrModuleName, 11, "Employee")
                    strList &= "</TD>"

                    strList &= "<TD align='left' style='width:;35%'>"
                    strList &= Language.getMessage(mstrModuleName, 12, "Tran. Head")
                    strList &= "</TD>"

                    strList &= "<TD align='right' style='width:25%'>"
                    strList &= Language.getMessage(mstrModuleName, 13, "Amount")
                    strList &= "</TD>"

                    strList &= "</TR>"
                    '-----------------------------

                    For Each dtRow As DataRow In lstEmp
                        strList &= "<TR>"

                        strList &= "<TD align='left' style='width:40%'>"
                        strList &= dtRow.Item("employeename").ToString
                        strList &= "</TD>"

                        strList &= "<TD align='left' style='width:;35%'>"
                        strList &= dtRow.Item("trnheadname").ToString
                        strList &= "</TD>"

                        strList &= "<TD align='right' style='width:25%'>"
                        strList &= Format(CDec(dtRow.Item("amount")), strFmtCurrency)
                        strList &= "</TD>"

                        strList &= "</TR>"
                    Next

                    StrMessage &= "</TABLE></div>"

                    StrMessage &= "<p><center style='padding-top:25px;font-size:10.0pt;font-family:&quot;Verdana&quot;,&quot;Sans-Serif&quot;;color:#333399;margin-left:0px;margin-right:0px;margin-top:0px;margin-bottom:10px'><b>&quot;POWERED BY ARUTI HR &amp; PAYROLL MANAGEMENT SOFTWARE.&quot;</b></center></p>"

                    StrMessage &= "</BODY></HTML>"

                    StrMessage = StrMessage.Replace("#USERNAME#", strUserName)
                    StrMessage = StrMessage.Replace("#INITIATORNAME#", strInitiatorName)
                    StrMessage = StrMessage.Replace("#LIST#", strList)

                    objMail._Message = StrMessage
                    objMail._ToEmail = strInitiatorEmail
                    If blnIsApprove = True Then
                        objMail._Subject = Language.getMessage(mstrModuleName, 19, "Earning Deduction has been approved")
                    Else
                        objMail._Subject = Language.getMessage(mstrModuleName, 20, "Earning Deduction has been rejected")
                    End If

                    If mstrWebFrmName.Trim.Length > 0 Then
                        objMail._Form_Name = mstrWebFrmName
                    End If
                    objMail._LogEmployeeUnkid = iLoginEmployeeId
                    objMail._OperationModeId = en_Login_Mode
                    objMail._UserUnkid = intUserUnkid
                    objMail._SenderAddress = IIf(strUserEmail = "", strUserName, strUserEmail)
                    objMail._ModuleRefId = clsSendMail.enAT_VIEW_TYPE.PAYROLL_MGT

                    gobjEmailList.Add(New clsEmailCollection(objMail._ToEmail, objMail._Subject, objMail._Message, mstrWebFrmName, iLoginEmployeeId, "", "", intUserUnkid, en_Login_Mode, clsSendMail.enAT_VIEW_TYPE.PAYROLL_MGT, IIf(strUserEmail = "", strUserName, strUserEmail)))
                Next

                If HttpContext.Current Is Nothing Then
                    trd = New Thread(AddressOf Send_Notification)
                    trd.IsBackground = True
                    Dim arr(1) As Object
                    arr(0) = xCompanyUnkid
                    trd.Start(arr)
                Else
                    Call Send_Notification(xCompanyUnkid)
                End If
            End If
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: SendMailToInitiator; Module Name: " & mstrModuleName)
        End Try
    End Sub
    'Sohail (06 Sep 2019) -- End

    Private Sub Send_Notification(ByVal intCompanyUnkId As Object)
        Try
            If gobjEmailList.Count > 0 Then

                Dim objSendMail As New clsSendMail
                For Each obj In gobjEmailList
                    objSendMail._ToEmail = obj._EmailTo
                    objSendMail._Subject = obj._Subject
                    objSendMail._Message = obj._Message
                    objSendMail._Form_Name = obj._FormName
                    objSendMail._LogEmployeeUnkid = obj._LogEmployeeUnkid
                    objSendMail._OperationModeId = obj._OperationModeId
                    objSendMail._UserUnkid = obj._UserUnkid
                    objSendMail._SenderAddress = obj._SenderAddress
                    objSendMail._ModuleRefId = obj._ModuleRefId
                    Try
                        If TypeOf intCompanyUnkId Is Integer Then
                            objSendMail.SendMail(CInt(intCompanyUnkId))
                        Else
                            objSendMail.SendMail(CInt(intCompanyUnkId(0)))
                        End If
                    Catch ex As Exception

                    End Try
                Next
                gobjEmailList.Clear()
            End If
        Catch ex As Exception
            Throw New Exception(mstrModuleName & ":Send_Notification:- " & ex.Message)
        Finally
            If gobjEmailList.Count > 0 Then
                gobjEmailList.Clear()
            End If
        End Try
    End Sub
    'Sohail (02 Sep 2019) -- End

#Region " Message List "
    '1, "This Transaction Head is already exist."
    '2, "This Transaction Head is already in used."
    '3, "Select"
#End Region

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Public Shared Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "This Transaction Head already exist.")
            Language.setMessage(mstrModuleName, 2, "Select")
            Language.setMessage(mstrModuleName, 3, "WEB")
			Language.setMessage(mstrModuleName, 4, "Sorry, Some active membership heads are not assigned on Earning Deductions. Please assign them from Earning Deduction List -> Operation -> Assign membership heads.")
			Language.setMessage(mstrModuleName, 5, "Formula is not set on the Transaction Head. Please set formula from Transaction Head screen for")
			Language.setMessage(mstrModuleName, 6, "Transaction head doees not exist.")
			Language.setMessage(mstrModuleName, 7, "Dear")
            Language.setMessage(mstrModuleName, 8, "This is to notify you that there is a flat rate transaction heads uploaded / edited in Aruti awaiting your approval.")
			Language.setMessage(mstrModuleName, 9, "Please login to the system to approve / reject the transaction.")
			Language.setMessage(mstrModuleName, 10, "This Earning Deduction has been uploadeded / edited by")
			Language.setMessage(mstrModuleName, 11, "Employee")
			Language.setMessage(mstrModuleName, 12, "Tran. Head")
			Language.setMessage(mstrModuleName, 13, "Amount")
			Language.setMessage(mstrModuleName, 14, "Reminder for Approving Earning Deduction")
			Language.setMessage(mstrModuleName, 15, "Dear")
			Language.setMessage(mstrModuleName, 16, "This is to inform you that the transaction heads you submitted for approval has been approved by")
			Language.setMessage(mstrModuleName, 17, "This is to inform you that the transaction heads you submitted for approval has been rejected by")
			Language.setMessage(mstrModuleName, 18, "Comments :")
			Language.setMessage(mstrModuleName, 19, "Earning Deduction has been approved")
			Language.setMessage(mstrModuleName, 20, "Earning Deduction has been rejected")
			Language.setMessage(mstrModuleName, 21, "Sorry, ED Approval is already done for some of the selected transaction heads. Please refresh the list and try again.")
			Language.setMessage(mstrModuleName, 22, "Sorry, Some of the selected transaction heads are already voided. Please refresh the list and try again.")

        Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class
