﻿'************************************************************************************************************************************
'Class Name : clsemployee_exemption_Tran.vb
'Purpose    :
'Date       :23/07/2010
'Written By :Pinkal
'Modified   :
'Last Message Index = 
'************************************************************************************************************************************

Imports eZeeCommonLib
'S.SANDEEP [ 11 AUG 2012 ] -- START
'ENHANCEMENT : TRA CHANGES
Imports eZee.Common.eZeeForm
'S.SANDEEP [ 11 AUG 2012 ] -- END

''' <summary>
''' Purpose: 
''' Developer: Pinkal
''' </summary>
Public Class clsemployee_exemption_Tran
    Private Shared ReadOnly mstrModuleName As String = "clsemployee_exemption_Tran"
    Dim objDataOperation As clsDataOperation
    Dim mstrMessage As String = ""

#Region " Private variables "
    Private mintExemptionunkid As Integer
    Private mintYearunkid As Integer
    Private mintPeriodunkid As Integer
    Private mintEmployeeunkid As Integer
    Private mintTranheadunkid As Integer
    Private mintUserunkid As Integer
    Private mblnIsvoid As Boolean
    Private mintVoiduserunkid As Integer
    Private mdtVoiddatetime As Date
    Private mstrVoidreason As String = String.Empty

    'Vimal (01 Nov 2010) -- Start 
    Private mblnExemptFlag As Boolean
    'Vimal (01 Nov 2010) -- End

    'Sohail (26 Nov 2011) -- Start
    Private mblnIsapproved As Boolean
    Private mintApproveruserunkid As Integer = -1
    'Sohail (26 Nov 2011) -- End

    'S.SANDEEP [ 07 NOV 2011 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Private mintDisciplineFileUnkid As Integer = -1
    'S.SANDEEP [ 07 NOV 2011 ] -- END

    'S.SANDEEP [ 19 JULY 2012 ] -- START
    'Enhancement : TRA Changes
    Private mstrWebFormName As String = String.Empty
    'S.SANDEEP [ 19 JULY 2012 ] -- END

    'S.SANDEEP [ 13 AUG 2012 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Private mintLogEmployeeUnkid As Integer = -1
    'S.SANDEEP [ 13 AUG 2012 ] -- END

#End Region

#Region " Properties "

    'S.SANDEEP [28-May-2018] -- START 
    'ISSUE/ENHANCEMENT : {Audit Trails} 
    Private mstrFormName As String = String.Empty
    Public WriteOnly Property _FormName() As String
        Set(ByVal value As String)
            mstrFormName = value
        End Set
    End Property

    Private mstrClientIP As String = ""
    Public WriteOnly Property _ClientIP() As String
        Set(ByVal value As String)
            mstrClientIP = value
        End Set
    End Property

    Private mstrHostName As String = ""
    Public WriteOnly Property _HostName() As String
        Set(ByVal value As String)
            mstrHostName = value
        End Set
    End Property

    Private mblnIsWeb As Boolean = False
    Public WriteOnly Property _FromWeb() As Boolean
        Set(ByVal value As Boolean)
            mblnIsWeb = value
        End Set
    End Property

    Private mintAuditUserId As Integer = 0
    Public WriteOnly Property _AuditUserId() As Integer
        Set(ByVal value As Integer)
            mintAuditUserId = value
        End Set
    End Property

    Private mintCompanyUnkid As Integer = 0
    Public WriteOnly Property _CompanyUnkid() As Integer
        Set(ByVal value As Integer)
            mintCompanyUnkid = value
        End Set
    End Property

    Private mdtAuditDate As DateTime = Now
    Public WriteOnly Property _AuditDate() As DateTime
        Set(ByVal value As DateTime)
            mdtAuditDate = value
        End Set
    End Property

    Private xDataOpr As clsDataOperation = Nothing
    Public Property _xDataOpr() As clsDataOperation
        Get
            Return xDataOpr
        End Get
        Set(ByVal value As clsDataOperation)
            xDataOpr = value
        End Set
    End Property

    Private mintVoidlogingemployeeunkid As Integer = 0
    Public Property _Voidloginemployeeunkid() As Integer
        Get
            Return mintVoidlogingemployeeunkid
        End Get
        Set(ByVal value As Integer)
            mintVoidlogingemployeeunkid = value
        End Set
    End Property
    'S.SANDEEP [28-May-2018] -- END 

    Public ReadOnly Property _Message() As String
        Get
            Return mstrMessage
        End Get
    End Property

    ''' <summary>
    ''' Purpose: Get or Set exemptionunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Exemptionunkid() As Integer
        Get
            Return mintExemptionunkid
        End Get
        Set(ByVal value As Integer)
            mintExemptionunkid = Value
            Call getData()
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set yearunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Yearunkid() As Integer
        Get
            Return mintYearunkid
        End Get
        Set(ByVal value As Integer)
            mintYearunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set periodunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Periodunkid() As Integer
        Get
            Return mintPeriodunkid
        End Get
        Set(ByVal value As Integer)
            mintPeriodunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set employeeunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Employeeunkid() As Integer
        Get
            Return mintEmployeeunkid
        End Get
        Set(ByVal value As Integer)
            mintEmployeeunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set tranheadunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Tranheadunkid() As Integer
        Get
            Return mintTranheadunkid
        End Get
        Set(ByVal value As Integer)
            mintTranheadunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set userunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Userunkid() As Integer
        Get
            Return mintUserunkid
        End Get
        Set(ByVal value As Integer)
            mintUserunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isvoid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Isvoid() As Boolean
        Get
            Return mblnIsvoid
        End Get
        Set(ByVal value As Boolean)
            mblnIsvoid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiduserunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Voiduserunkid() As Integer
        Get
            Return mintVoiduserunkid
        End Get
        Set(ByVal value As Integer)
            mintVoiduserunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiddatetime
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Voiddatetime() As Date
        Get
            Return mdtVoiddatetime
        End Get
        Set(ByVal value As Date)
            mdtVoiddatetime = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voidreason
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Voidreason() As String
        Get
            Return mstrVoidreason
        End Get
        Set(ByVal value As String)
            mstrVoidreason = Value
        End Set
    End Property


    'Vimal (01 Nov 2010) -- Start 
    Public Property _ExemptFlag() As Boolean
        Get
            Return mblnExemptFlag
        End Get
        Set(ByVal value As Boolean)
            mblnExemptFlag = value
        End Set
    End Property
    'Vimal (01 Nov 2010) -- End

    'Sohail (26 Nov 2011) -- Start
    ''' <summary>
    ''' Purpose: Get or Set isapproved
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Isapproved() As Boolean
        Get
            Return mblnIsapproved
        End Get
        Set(ByVal value As Boolean)
            mblnIsapproved = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set approveruserunkid
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Approveruserunkid() As Integer
        Get
            Return mintApproveruserunkid
        End Get
        Set(ByVal value As Integer)
            mintApproveruserunkid = Value
        End Set
    End Property
    'Sohail (26 Nov 2011) -- End


    'S.SANDEEP [ 07 NOV 2011 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Public Property _DisciplineFileUnkid() As Integer
        Get
            Return mintDisciplineFileUnkid
        End Get
        Set(ByVal value As Integer)
            mintDisciplineFileUnkid = value
        End Set
    End Property
    'S.SANDEEP [ 07 NOV 2011 ] -- END


    'S.SANDEEP [ 19 JULY 2012 ] -- START
    'Enhancement : TRA Changes

    Public Property _WebFormName() As String
        Get
            Return mstrWebFormName
        End Get
        Set(ByVal value As String)
            mstrWebFormName = value
        End Set
    End Property

    'S.SANDEEP [ 19 JULY 2012 ] -- END

    'S.SANDEEP [ 13 AUG 2012 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Public WriteOnly Property _LoginEmployeeUnkid() As Integer
        Set(ByVal value As Integer)
            mintLogEmployeeUnkid = value
        End Set
    End Property
    'S.SANDEEP [ 13 AUG 2012 ] -- END

#End Region

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Sub GetData()
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            StrQ = "SELECT " & _
              "  exemptionunkid " & _
              ", yearunkid " & _
              ", periodunkid " & _
              ", employeeunkid " & _
              ", tranheadunkid " & _
              ", userunkid " & _
              ", isvoid " & _
              ", voiduserunkid " & _
              ", voiddatetime " & _
              ", voidreason " & _
              ", ISNULL(isapproved, 0) AS isapproved " & _
              ", ISNULL(approveruserunkid, 0) AS approveruserunkid " & _
              ", ISNULL(disciplinefileunkid,0) AS disciplinefileunkid " & _
             "FROM premployee_exemption_tran " & _
             "WHERE exemptionunkid = @exemptionunkid " 'Sohail (26 Nov 2011) - [isapproved, approveruserunkid]  'S.SANDEEP [ 07 NOV 2011 ] - [disciplinefileunkid]

            objDataOperation.AddParameter("@exemptionunkid", SqlDbType.int, eZeeDataType.INT_SIZE, mintExemptionUnkId.ToString)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For Each dtRow As DataRow In dsList.Tables(0).Rows
                mintexemptionunkid = CInt(dtRow.Item("exemptionunkid"))
                mintyearunkid = CInt(dtRow.Item("yearunkid"))
                mintperiodunkid = CInt(dtRow.Item("periodunkid"))
                mintemployeeunkid = CInt(dtRow.Item("employeeunkid"))
                minttranheadunkid = CInt(dtRow.Item("tranheadunkid"))
                mintuserunkid = CInt(dtRow.Item("userunkid"))
                mblnisvoid = CBool(dtRow.Item("isvoid"))
                mintvoiduserunkid = CInt(dtRow.Item("voiduserunkid"))
                If mdtVoiddatetime <> Nothing Then mdtVoiddatetime = dtRow.Item("voiddatetime")
                mstrVoidreason = dtRow.Item("voidreason").ToString
                'Sohail (26 Nov 2011) -- Start
                mblnIsapproved = CBool(dtRow.Item("isapproved"))
                mintApproveruserunkid = CInt(dtRow.Item("approveruserunkid"))
                'Sohail (26 Nov 2011) -- End

                'S.SANDEEP [ 07 NOV 2011 ] -- START
                'ENHANCEMENT : TRA CHANGES
                mintDisciplineFileUnkid = CInt(dtRow.Item("disciplinefileunkid"))
                'S.SANDEEP [ 07 NOV 2011 ] -- END

                Exit For
            Next
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetData; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Sub


    'S.SANDEEP [04 JUN 2015] -- START
    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function GetList(ByVal xDatabaseName As String _
                            , ByVal xUserUnkid As Integer _
                            , ByVal xYearUnkid As Integer _
                            , ByVal xCompanyUnkid As Integer _
                            , ByVal xPeriodStart As DateTime _
                            , ByVal xPeriodEnd As DateTime _
                            , ByVal xUserModeSetting As String _
                            , ByVal xOnlyApproved As Boolean _
                            , ByVal xIncludeIn_ActiveEmployee As Boolean _
                            , ByVal strTableName As String _
                            , Optional ByVal blnOnlyActive As Boolean = True _
                            , Optional ByVal blnApplyUserAccessFilter As Boolean = True _
                            , Optional ByVal intEmpUnkId As Integer = 0 _
                            , Optional ByVal intTranHeadUnkId As Integer = 0 _
                            , Optional ByVal intPeriodUnkId As Integer = 0 _
                            , Optional ByVal intExempHeadApprovalStatus As Integer = enExempHeadApprovalStatus.All _
                            , Optional ByVal intDisciplineFileUnkid As Integer = -1 _
                            , Optional ByVal strEmpUnkIDs As String = "" _
                            , Optional ByVal strPeriodUnkIDs As String = "" _
                            ) As DataSet
        'Sohail (17 Sep 2019) - [strEmpUnkIDs, strPeriodUnkIDs]

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        Dim xDateJoinQry, xDateFilterQry, xUACQry, xUACFiltrQry, xAdvanceJoinQry As String
        xDateJoinQry = "" : xDateFilterQry = "" : xUACQry = "" : xUACFiltrQry = "" : xAdvanceJoinQry = ""
        Call GetDatesFilterString(xDateJoinQry, xDateFilterQry, xPeriodStart, xPeriodEnd, , , xDatabaseName)
        Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, xPeriodEnd, xOnlyApproved, xDatabaseName, xUserUnkid, xCompanyUnkid, xYearUnkid, xUserModeSetting)

        objDataOperation = New clsDataOperation

        Try
            strQ = "SELECT " & _
              "  exemptionunkid " & _
              ", premployee_exemption_tran.yearunkid " & _
              ", " & FinancialYear._Object._ConfigDatabaseName & "..cffinancial_year_tran.financialyear_name as year " & _
              ", premployee_exemption_tran.periodunkid " & _
              ", cfcommon_period_tran.period_name as period " & _
              ", premployee_exemption_tran.employeeunkid " & _
              ", ISNULL(hremployee_master.employeecode,'') AS employeecode " & _
              ", isnull(hremployee_master.firstname,'') + ' '  + isnull(hremployee_master.surname,'') as employeename " & _
              ", premployee_exemption_tran.tranheadunkid " & _
              ", prtranhead_master.trnheadname " & _
              ", premployee_exemption_tran.userunkid " & _
              ", premployee_exemption_tran.isvoid " & _
              ", premployee_exemption_tran.voiduserunkid " & _
              ", premployee_exemption_tran.voiddatetime " & _
              ", premployee_exemption_tran.voidreason " & _
              ", ISNULL(premployee_exemption_tran.isapproved, 0) AS isapproved " & _
              ", ISNULL(premployee_exemption_tran.approveruserunkid, 0) AS approveruserunkid " & _
              ", ISNULL(disciplinefileunkid,0) AS disciplinefileunkid " & _
              ", cfcommon_period_tran.start_date AS start_date " & _
              ", cfcommon_period_tran.end_date AS end_date " & _
             " FROM premployee_exemption_tran " & _
             " LEFT JOIN " & FinancialYear._Object._ConfigDatabaseName & "..cffinancial_year_tran on cffinancial_year_tran.yearunkid = premployee_exemption_tran.yearunkid " & _
             " LEFT JOIN cfcommon_period_tran on cfcommon_period_tran.periodunkid =  premployee_exemption_tran.periodunkid " & _
             " LEFT JOIN hremployee_master on hremployee_master.employeeunkid = premployee_exemption_tran.employeeunkid " & _
             " LEFT JOIN prtranhead_master on prtranhead_master.tranheadunkid = premployee_exemption_tran.tranheadunkid "

            If xDateJoinQry.Trim.Length > 0 Then
                strQ &= xDateJoinQry
            End If

            'S.SANDEEP [15 NOV 2016] -- START
            'If xUACQry.Trim.Length > 0 Then
            '    StrQ &= xUACQry
            'End If
            If blnApplyUserAccessFilter = True Then
            If xUACQry.Trim.Length > 0 Then
                strQ &= xUACQry
            End If
            End If
            'S.SANDEEP [15 NOV 2016] -- END

            strQ &= " WHERE 1 = 1 "

            If intDisciplineFileUnkid > 0 Then
                strQ &= " AND ISNULL(disciplinefileunkid,0) = '" & intDisciplineFileUnkid & "' "
            End If

            If blnOnlyActive Then
                strQ &= " AND premployee_exemption_tran.isvoid = 0 "
            End If

            If blnApplyUserAccessFilter = True Then
                If xUACFiltrQry.Trim.Length > 0 Then
                    strQ &= " AND " & xUACFiltrQry
                End If
            End If

            If xIncludeIn_ActiveEmployee = False Then
                If xDateFilterQry.Trim.Length > 0 Then
                    strQ &= xDateFilterQry
                End If
            End If


            If intEmpUnkId > 0 Then
                strQ &= " AND hremployee_master.employeeunkid = @employeeunkid "
                objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intEmpUnkId.ToString)
            End If

            'Sohail (17 Sep 2019) -- Start
            'NMB Enhancement # TC006: system should be able to bind earning/deduction (benefit) with specific allocation and automatically assign to employee as per his/her allocation.
            If strEmpUnkIDs.Trim.Length > 0 Then
                strQ &= " AND premployee_exemption_tran.employeeunkid IN (" & strEmpUnkIDs & ") "
            End If

            If strPeriodUnkIDs.Trim.Length > 0 Then
                strQ &= " AND premployee_exemption_tran.periodunkid IN (" & strPeriodUnkIDs & ") "
            End If
            'Sohail (17 Sep 2019) -- End

            If intTranHeadUnkId > 0 Then
                strQ &= " AND prtranhead_master.tranheadunkid = @tranheadunkid "
                objDataOperation.AddParameter("@tranheadunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intTranHeadUnkId.ToString)
            End If

            If intPeriodUnkId > 0 Then
                strQ &= " AND cfcommon_period_tran.periodunkid = @periodunkid "
                objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intPeriodUnkId.ToString)
            End If

            If intExempHeadApprovalStatus = enExempHeadApprovalStatus.Approved Then
                strQ &= " AND premployee_exemption_tran.isapproved = @isapproved "
                objDataOperation.AddParameter("@isapproved", SqlDbType.Int, eZeeDataType.INT_SIZE, intExempHeadApprovalStatus)
            ElseIf intExempHeadApprovalStatus = enExempHeadApprovalStatus.Pending Then
                strQ &= " AND premployee_exemption_tran.isapproved = @isapproved "
                objDataOperation.AddParameter("@isapproved", SqlDbType.Int, eZeeDataType.INT_SIZE, 0)
            End If

            dsList = objDataOperation.ExecQuery(strQ, strTableName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
        Return dsList
    End Function

    'Public Function GetList(ByVal strTableName As String, _
    '                        Optional ByVal blnOnlyActive As Boolean = True, _
    '                        Optional ByVal intEmpUnkId As Integer = 0, _
    '                        Optional ByVal intTranHeadUnkId As Integer = 0, _
    '                        Optional ByVal intPeriodUnkId As Integer = 0, _
    '                        Optional ByVal intExempHeadApprovalStatus As Integer = enExempHeadApprovalStatus.All, _
    '                        Optional ByVal intDisciplineFileUnkid As Integer = -1) As DataSet 'Sohail (26 Nov 2011) - [intEmpUnkId, intTranHeadUnkId, intPeriodUnkId] 'S.SANDEEP [ 07 NOV 2011 ] - [disciplinefileunkid]

    '    Dim dsList As DataSet = Nothing
    '    Dim strQ As String = ""
    '    Dim exForce As Exception

    '    objDataOperation = New clsDataOperation

    '    Try
    '        strQ = "SELECT " & _
    '          "  exemptionunkid " & _
    '          ", premployee_exemption_tran.yearunkid " & _
    '          ", " & FinancialYear._Object._ConfigDatabaseName & "..cffinancial_year_tran.financialyear_name as year " & _
    '          ", premployee_exemption_tran.periodunkid " & _
    '          ", cfcommon_period_tran.period_name as period " & _
    '          ", premployee_exemption_tran.employeeunkid " & _
    '          ", ISNULL(hremployee_master.employeecode,'') AS employeecode " & _
    '          ", isnull(hremployee_master.firstname,'') + ' '  + isnull(hremployee_master.surname,'') as employeename " & _
    '          ", premployee_exemption_tran.tranheadunkid " & _
    '          ", prtranhead_master.trnheadname " & _
    '          ", premployee_exemption_tran.userunkid " & _
    '          ", premployee_exemption_tran.isvoid " & _
    '          ", premployee_exemption_tran.voiduserunkid " & _
    '          ", premployee_exemption_tran.voiddatetime " & _
    '          ", premployee_exemption_tran.voidreason " & _
    '          ", ISNULL(premployee_exemption_tran.isapproved, 0) AS isapproved " & _
    '          ", ISNULL(premployee_exemption_tran.approveruserunkid, 0) AS approveruserunkid " & _
    '          ", ISNULL(disciplinefileunkid,0) AS disciplinefileunkid " & _
    '          ", cfcommon_period_tran.start_date AS start_date " & _
    '          ", cfcommon_period_tran.end_date AS end_date " & _
    '         " FROM premployee_exemption_tran " & _
    '         " LEFT JOIN " & FinancialYear._Object._ConfigDatabaseName & "..cffinancial_year_tran on cffinancial_year_tran.yearunkid = premployee_exemption_tran.yearunkid " & _
    '         " LEFT JOIN cfcommon_period_tran on cfcommon_period_tran.periodunkid =  premployee_exemption_tran.periodunkid " & _
    '         " LEFT JOIN hremployee_master on hremployee_master.employeeunkid = premployee_exemption_tran.employeeunkid " & _
    '         " LEFT JOIN prtranhead_master on prtranhead_master.tranheadunkid = premployee_exemption_tran.tranheadunkid " & _
    '         " WHERE 1 = 1 " 'Anjan (09 Aug 2011), 'Sohail (26 Nov 2011) - [isapproved, approveruserunkid]  'S.SANDEEP [ 07 NOV 2011 ] - [disciplinefileunkid,cfcommon_period_tran.start_date,cfcommon_period_tran.end_date]
    '        'Sohail (28 Jan 2012) - [employeecode]

    '        'S.SANDEEP [ 07 NOV 2011 ] -- START
    '        'ENHANCEMENT : TRA CHANGES
    '        If intDisciplineFileUnkid > 0 Then
    '            strQ &= " AND ISNULL(disciplinefileunkid,0) = '" & intDisciplineFileUnkid & "' "
    '            'Else
    '            '    strQ &= " AND ISNULL(disciplinefileunkid,0) <= 0 "
    '        End If
    '        'S.SANDEEP [ 07 NOV 2011 ] -- END

    '        If blnOnlyActive Then
    '            'Anjan (09 Aug 2011)-Start
    '            'Issue : For including setting of acitve and inactive employee.
    '            'strQ &= " WHERE premployee_exemption_tran.isvoid = 0 "
    '            strQ &= " AND premployee_exemption_tran.isvoid = 0 "
    '            'Anjan (09 Aug 2011)-End 
    '        End If

    '        'Anjan (09 Aug 2011)-Start
    '        'Issue : For including setting of acitve and inactive employee.
    '        If ConfigParameter._Object._IsIncludeInactiveEmp = False Then
    '            '    strQ &= " AND ISNULL(hremployee_master.isactive,0) = 1 "
    '            'Sohail (06 Jan 2012) -- Start
    '            'TRA - ENHANCEMENT
    '            strQ &= " AND CONVERT(CHAR(8),hremployee_master.appointeddate,112) <= @enddate " & _
    '                       " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_from_date,112),@startdate) >= @startdate " & _
    '                       " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_to_date,112),@startdate) >= @startdate " & _
    '                       " AND ISNULL(CONVERT(CHAR(8),hremployee_master.empl_enddate,112), @startdate) >= @startdate " 'S.SANDEEP [ 15 MAY 2012 empl_enddate ] -- START -- END

    '            objDataOperation.AddParameter("@startdate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, ConfigParameter._Object._EmployeeAsOnDate)
    '            objDataOperation.AddParameter("@enddate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, ConfigParameter._Object._EmployeeAsOnDate)
    '            'Sohail (06 Jan 2012) -- End
    '        End If
    '        'Anjan (09 Aug 2011)-End 

    '        'Sohail (24 Jun 2011) -- Start
    '        'Issue : According to privilege that lower level user should not see superior level employees.


    '        'S.SANDEEP [ 04 FEB 2012 ] -- START
    '        'ENHANCEMENT : TRA CHANGES
    '        'If UserAccessLevel._AccessLevel.Length > 0 Then
    '        '    strQ &= " AND hremployee_master.jobunkid IN (" & UserAccessLevel._AccessLevel & ") "
    '        'End If

    '        'S.SANDEEP [ 01 JUNE 2012 ] -- START
    '        'ENHANCEMENT : TRA DISCIPLINE CHANGES
    '        'Select Case ConfigParameter._Object._UserAccessModeSetting
    '        '    Case enAllocation.BRANCH
    '        '        If UserAccessLevel._AccessLevel.Length > 0 Then
    '        '            strQ &= " AND  hremployee_master.stationunkid IN (" & UserAccessLevel._AccessLevel & ") "
    '        '        End If
    '        '    Case enAllocation.DEPARTMENT_GROUP
    '        '        If UserAccessLevel._AccessLevel.Length > 0 Then
    '        '            strQ &= " AND  hremployee_master.deptgroupunkid IN (" & UserAccessLevel._AccessLevel & ") "
    '        '        End If
    '        '    Case enAllocation.DEPARTMENT
    '        '        If UserAccessLevel._AccessLevel.Length > 0 Then
    '        '            strQ &= " AND  hremployee_master.departmentunkid IN (" & UserAccessLevel._AccessLevel & ") "
    '        '        End If
    '        '    Case enAllocation.SECTION_GROUP
    '        '        If UserAccessLevel._AccessLevel.Length > 0 Then
    '        '            strQ &= " AND  hremployee_master.sectiongroupunkid IN (" & UserAccessLevel._AccessLevel & ") "
    '        '        End If
    '        '    Case enAllocation.SECTION
    '        '        If UserAccessLevel._AccessLevel.Length > 0 Then
    '        '            strQ &= " AND  hremployee_master.sectionunkid IN (" & UserAccessLevel._AccessLevel & ") "
    '        '        End If
    '        '    Case enAllocation.UNIT_GROUP
    '        '        If UserAccessLevel._AccessLevel.Length > 0 Then
    '        '            strQ &= " AND  hremployee_master.unitgroupunkid IN (" & UserAccessLevel._AccessLevel & ") "
    '        '        End If
    '        '    Case enAllocation.UNIT
    '        '        If UserAccessLevel._AccessLevel.Length > 0 Then
    '        '            strQ &= " AND  hremployee_master.unitunkid IN (" & UserAccessLevel._AccessLevel & ") "
    '        '        End If
    '        '    Case enAllocation.TEAM
    '        '        If UserAccessLevel._AccessLevel.Length > 0 Then
    '        '            strQ &= " AND  hremployee_master.teamunkid IN (" & UserAccessLevel._AccessLevel & ") "
    '        '        End If
    '        '    Case enAllocation.JOB_GROUP
    '        '        If UserAccessLevel._AccessLevel.Length > 0 Then
    '        '            strQ &= " AND  hremployee_master.jobgroupunkid IN (" & UserAccessLevel._AccessLevel & ") "
    '        '        End If
    '        '    Case enAllocation.JOBS
    '        '        If UserAccessLevel._AccessLevel.Length > 0 Then
    '        '            strQ &= " AND hremployee_master.jobunkid IN (" & UserAccessLevel._AccessLevel & ") "
    '        '        End If
    '        'End Select
    '        strQ &= UserAccessLevel._AccessLevelFilterString
    '        'S.SANDEEP [ 01 JUNE 2012 ] -- END


    '        'S.SANDEEP [ 04 FEB 2012 ] -- END
    '        'Sohail (24 Jun 2011) -- End

    '        'Sohail (26 Nov 2011) -- Start
    '        If intEmpUnkId > 0 Then
    '            strQ &= " AND hremployee_master.employeeunkid = @employeeunkid "
    '            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intEmpUnkId.ToString)
    '        End If

    '        If intTranHeadUnkId > 0 Then
    '            strQ &= " AND prtranhead_master.tranheadunkid = @tranheadunkid "
    '            objDataOperation.AddParameter("@tranheadunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intTranHeadUnkId.ToString)
    '        End If

    '        If intPeriodUnkId > 0 Then
    '            strQ &= " AND cfcommon_period_tran.periodunkid = @periodunkid "
    '            objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intPeriodUnkId.ToString)
    '        End If

    '        If intExempHeadApprovalStatus = enExempHeadApprovalStatus.Approved Then
    '            strQ &= " AND premployee_exemption_tran.isapproved = @isapproved "
    '            objDataOperation.AddParameter("@isapproved", SqlDbType.Int, eZeeDataType.INT_SIZE, intExempHeadApprovalStatus)
    '        ElseIf intExempHeadApprovalStatus = enExempHeadApprovalStatus.Pending Then
    '            strQ &= " AND premployee_exemption_tran.isapproved = @isapproved "
    '            objDataOperation.AddParameter("@isapproved", SqlDbType.Int, eZeeDataType.INT_SIZE, 0)
    '        End If
    '        'Sohail (26 Nov 2011) -- End

    '        dsList = objDataOperation.ExecQuery(strQ, strTableName)

    '        If objDataOperation.ErrorMessage <> "" Then
    '            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
    '            Throw exForce
    '        End If

    '    Catch ex As Exception
    '        Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
    '    Finally
    '        exForce = Nothing
    '        If dsList IsNot Nothing Then dsList.Dispose()
    '        objDataOperation = Nothing
    '    End Try
    '    Return dsList
    'End Function

    'S.SANDEEP [04 JUN 2015] -- END



    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> INSERT INTO Database Table (premployee_exemption_tran) </purpose>
    Public Function Insert(ByVal mdtCurrentDateTime As DateTime) As Boolean 'S.SANDEEP [04 JUN 2015] -- START-- END
        'Public Function Insert() As Boolean
        If isExist(mintPeriodunkid, mintEmployeeunkid, mintTranheadunkid) Then
            mstrMessage = Language.getMessage(mstrModuleName, 1, "This Exemption is already defined for this employee. Please define new Exemption.")
            Return False
        End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation
        'S.SANDEEP [04 JUN 2015] -- START
        'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
        objDataOperation.BindTransaction()
        'S.SANDEEP [04 JUN 2015] -- END

        Try
            objDataOperation.AddParameter("@yearunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintYearunkid.ToString)
            objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPeriodunkid.ToString)
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeunkid.ToString)
            objDataOperation.AddParameter("@tranheadunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintTranheadunkid.ToString)
            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
            objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, IIf(mdtVoiddatetime <> Nothing, mdtVoiddatetime, DBNull.Value))
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)
            'Sohail (26 Nov 2011) -- Start
            objDataOperation.AddParameter("@isapproved", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsapproved.ToString)
            objDataOperation.AddParameter("@approveruserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintApproveruserunkid.ToString)
            'Sohail (26 Nov 2011) -- End

            'S.SANDEEP [ 07 NOV 2011 ] -- START
            'ENHANCEMENT : TRA CHANGES
            objDataOperation.AddParameter("@disciplinefileunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintDisciplineFileUnkid)
            'S.SANDEEP [ 07 NOV 2011 ] -- END

            strQ = "INSERT INTO premployee_exemption_tran ( " & _
              "  yearunkid " & _
              ", periodunkid " & _
              ", employeeunkid " & _
              ", tranheadunkid " & _
              ", userunkid " & _
              ", isvoid " & _
              ", voiduserunkid " & _
              ", voiddatetime " & _
              ", voidreason" & _
              ", isapproved " & _
              ", approveruserunkid " & _
              ", disciplinefileunkid " & _
            ") VALUES (" & _
              "  @yearunkid " & _
              ", @periodunkid " & _
              ", @employeeunkid " & _
              ", @tranheadunkid " & _
              ", @userunkid " & _
              ", @isvoid " & _
              ", @voiduserunkid " & _
              ", @voiddatetime " & _
              ", @voidreason" & _
              ", @isapproved " & _
              ", @approveruserunkid " & _
              ", @disciplinefileunkid " & _
            "); SELECT @@identity" 'Sohail (26 Nov 2011) - [isapproved, approveruserunkid]  'S.SANDEEP [ 07 NOV 2011 ] - [disciplinefileunkid]

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            mintExemptionunkid = dsList.Tables(0).Rows(0).Item(0)

            'Sohail (11 Nov 2010) -- Start
            'Anjan (11 Jun 2011)-Start
            'Issue : In main table entry was get inserted if there any error occurs at time of insertion in AT table.
            'Call InsertAuditTrailForEmployeeExemption(objDataOperation, 1)
            If InsertAuditTrailForEmployeeExemption(objDataOperation, 1, mdtCurrentDateTime) = False Then
                objDataOperation.ReleaseTransaction(False)
                Return False
            End If
            'Anjan (11 Jun 2011)-End

            'Sohail (11 Nov 2010) -- End

            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            objDataOperation.ReleaseTransaction(True)
            'S.SANDEEP [04 JUN 2015] -- END


            Return True
        Catch ex As Exception
            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            objDataOperation.ReleaseTransaction(False)
            'S.SANDEEP [04 JUN 2015] -- END
            Throw New Exception(ex.Message & "; Procedure Name: Insert; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Update Database Table (premployee_exemption_tran) </purpose>
    Public Function Update(ByVal mdtCurrentDateTime As DateTime) As Boolean 'S.SANDEEP [04 JUN 2015] -- START -- END
        'Public Function Update() As Boolean
        If isExist(mintPeriodunkid, mintEmployeeunkid, mintTranheadunkid, mintExemptionunkid) Then
            mstrMessage = Language.getMessage(mstrModuleName, 1, "This Exemption is already defined for this employee. Please define new Exemption.")
            Return False
        End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        'S.SANDEEP [04 JUN 2015] -- START
        'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
        objDataOperation.BindTransaction()
        'S.SANDEEP [04 JUN 2015] -- END

        Try
            objDataOperation.AddParameter("@exemptionunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintExemptionunkid.ToString)
            objDataOperation.AddParameter("@yearunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintYearunkid.ToString)
            objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPeriodunkid.ToString)
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeunkid.ToString)
            objDataOperation.AddParameter("@tranheadunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintTranheadunkid.ToString)
            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
            objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, IIf(mdtVoiddatetime <> Nothing, mdtVoiddatetime, DBNull.Value))
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)
            'Sohail (26 Nov 2011) -- Start
            objDataOperation.AddParameter("@isapproved", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsapproved.ToString)
            objDataOperation.AddParameter("@approveruserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintApproveruserunkid.ToString)
            'Sohail (26 Nov 2011) -- End

            'S.SANDEEP [ 07 NOV 2011 ] -- START
            'ENHANCEMENT : TRA CHANGES
            objDataOperation.AddParameter("@disciplinefileunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintDisciplineFileUnkid)
            'S.SANDEEP [ 07 NOV 2011 ] -- END

            strQ = "UPDATE premployee_exemption_tran SET " & _
              "  yearunkid = @yearunkid" & _
              ", periodunkid = @periodunkid" & _
              ", employeeunkid = @employeeunkid" & _
              ", tranheadunkid = @tranheadunkid" & _
              ", userunkid = @userunkid" & _
              ", isvoid = @isvoid" & _
              ", voiduserunkid = @voiduserunkid" & _
              ", voiddatetime = @voiddatetime" & _
              ", voidreason = @voidreason " & _
              ", isapproved = @isapproved " & _
              ", approveruserunkid = @approveruserunkid " & _
              ", disciplinefileunkid = @disciplinefileunkid " & _
            "WHERE exemptionunkid = @exemptionunkid " 'Sohail (26 Nov 2011) - [isapproved, approveruserunkid] 'S.SANDEEP [ 07 NOV 2011 ] - [disciplinefileunkid]

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            If InsertAuditTrailForEmployeeExemption(objDataOperation, 2, mdtCurrentDateTime) = False Then
                objDataOperation.ReleaseTransaction(False)
                Return False
            End If

            objDataOperation.ReleaseTransaction(True)
            'S.SANDEEP [04 JUN 2015] -- END


            Return True
        Catch ex As Exception
            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            objDataOperation.ReleaseTransaction(False)
            'S.SANDEEP [04 JUN 2015] -- END
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Delete Database Table (premployee_exemption_tran) </purpose>
    '''  Public Function Delete(ByVal intUnkid As Integer) As Boolean
    Public Function Delete(ByVal strUnkids As String, ByVal mdtCurrentDateTime As DateTime) As Boolean 'S.SANDEEP [04 JUN 2015] -- START -- END
        'Public Function Delete(ByVal strUnkids As String) As Boolean
        'If isUsed(intUnkid) Then
        '    mstrMessage = "<Message>"
        '    Return False
        'End If

        Dim objDataOperation As clsDataOperation 'Sohail (11 Nov 2010)

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation
        objDataOperation.BindTransaction() 'Sohail (11 Nov 2010)

        Try

            'Sandeep [ 16 Oct 2010 ] -- Start
            'strQ = "Update premployee_exemption_tran set isvoid = 1,voiddatetime = getdate(),voidreason = @voidreason " & _
            '           "WHERE exemptionunkid = @exemptionunkid "
            'Sohail (28 Dec 2010) -- Start
            'strQ = "Update premployee_exemption_tran set isvoid = 1,voiddatetime = @voiddate,voidreason = @voidreason " & _
            '          "WHERE exemptionunkid = @exemptionunkid "


            'Sandeep [ 01 MARCH 2011 ] -- Start
            'Issue: For multiple exemption can be deleted, before this only one was deleted.
            'strQ = "UPDATE premployee_exemption_tran SET " & _
            '  " isvoid = 1 " & _
            '  ", voiduserunkid = @voiduserunkid" & _
            '  ", voiddatetime = @voiddate" & _
            '  ", voidreason = @voidreason " & _
            '"WHERE exemptionunkid = @exemptionunkid "

            strQ = "UPDATE premployee_exemption_tran SET " & _
              " isvoid = 1 " & _
              ", voiduserunkid = @voiduserunkid" & _
              ", voiddatetime = @voiddate" & _
              ", voidreason = @voidreason " & _
            "WHERE exemptionunkid IN ( " & strUnkids & ")"
            'Sandeep [ 01 MARCH 2011 ] -- End 



            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
            'Sohail (28 Dec 2010) -- End

            objDataOperation.AddParameter("@voiddate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime)
            'Sandeep [ 16 Oct 2010 ] -- End 

            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, mstrVoidreason)


            'Sandeep [ 01 MARCH 2011 ] -- Start
            'objDataOperation.AddParameter("@exemptionunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)
            'Sandeep [ 01 MARCH 2011 ] -- End 


            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            'Sohail (11 Nov 2010) -- Start


            'Sandeep [ 01 MARCH 2011 ] -- Start
            'Me._Exemptionunkid = intUnkid
            'Call InsertAuditTrailForEmployeeExemption(objDataOperation, 3)

            For Each strId As String In strUnkids.Split(",")
                Me._Exemptionunkid = CInt(strId)
                'Anjan (11 Jun 2011)-Start
                'Issue : In main table entry was get inserted if there any error occurs at time of insertion in AT table.
                'Call InsertAuditTrailForEmployeeExemption(objDataOperation, 3)
                If InsertAuditTrailForEmployeeExemption(objDataOperation, 3, mdtCurrentDateTime) = False Then
                    objDataOperation.ReleaseTransaction(False)
                    Return False
                End If
                'Anjan (11 Jun 2011)-End
            Next
            'Sandeep [ 01 MARCH 2011 ] -- End 


            objDataOperation.ReleaseTransaction(True)
            'Sohail (11 Nov 2010) -- End

            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False) 'Sohail (11 Nov 2010)
            Throw New Exception(ex.Message & "; Procedure Name: Delete; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isUsed(ByVal intUnkid As Integer) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "<Query>"

            objDataOperation.AddParameter("@exemptionunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isUsed; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isExist(ByVal intPeriodunkid As Integer, ByVal intEmployeeunkid As Integer, ByVal intTranheadunkid As Integer, Optional ByVal intUnkid As Integer = -1, Optional ByVal xDataOp As clsDataOperation = Nothing) As Boolean
        'Sohail (03 May 2018) - [xDataOp]
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        'Sohail (03 May 2018) -- Start
        'Enhancement : Removing global objects and passing objDataOperation in process payroll in 72.1.
        'objDataOperation = New clsDataOperation
        Dim objDataOperation As clsDataOperation
        If xDataOp Is Nothing Then
        objDataOperation = New clsDataOperation
        Else
            objDataOperation = xDataOp
        End If
        objDataOperation.ClearParameters()
        'Sohail (03 May 2018) -- End

        Try
            strQ = "SELECT " & _
              "  exemptionunkid " & _
              ", yearunkid " & _
              ", periodunkid " & _
              ", employeeunkid " & _
              ", tranheadunkid " & _
              ", userunkid " & _
              ", isvoid " & _
              ", voiduserunkid " & _
              ", voiddatetime " & _
              ", voidreason " & _
              ", isapproved " & _
              ", approveruserunkid " & _
             " FROM premployee_exemption_tran " & _
             " WHERE periodunkid = @periodunkid " & _
             " AND employeeunkid = @employeeunkid " & _
             " AND tranheadunkid = @tranheadunkid AND ISNULL(isvoid,0) = 0 " 'Sohail (16 Oct 2010), 'Sohail (26 Nov 2011) - [isapproved, approveruserunkid]

            If intUnkid > 0 Then
                strQ &= " AND exemptionunkid <> @exemptionunkid"
            End If

            objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intPeriodunkid)
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intEmployeeunkid)
            objDataOperation.AddParameter("@tranheadunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intTranheadunkid)
            objDataOperation.AddParameter("@exemptionunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isExist; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            'Sohail (03 May 2018) -- Start
            'Enhancement : Removing global objects and passing objDataOperation in process payroll in 72.1.
            'objDataOperation = Nothing
            If xDataOp Is Nothing Then objDataOperation = Nothing
            'Sohail (03 May 2018) -- End
        End Try
    End Function

    'Sohail (11 sep 2010) -- Start
    Public Function InsertByPeriod(ByVal strEmployeeList As String, ByVal strPeriodList As String, ByVal mdtCurrentdateTime As DateTime) As Boolean 'S.SANDEEP [04 JUN 2015] -- START -- END
        'Public Function InsertByPeriod(ByVal strEmployeeList As String, ByVal strPeriodList As String) As Boolean
        'Public Function InsertByPeriod(ByVal strPeriodList As String) As Boolean 'Sohail (09 Oct 2010)
        Dim objDataOperation As clsDataOperation

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim arrList() As String
        Dim arrEmpList() As String 'Sohail (09 Oct 2010)

        objDataOperation = New clsDataOperation
        objDataOperation.BindTransaction()

        Try

            arrList = strPeriodList.Split(",")
            arrEmpList = strEmployeeList.Split(",") 'Sohail (09 Oct 2010)

            For i = 0 To arrEmpList.Length - 1 'Sohail (09 Oct 2010)
                mintEmployeeunkid = CInt(arrEmpList(i)) 'Sohail (09 Oct 2010)

                For j = 0 To arrList.Length - 1
                    mintPeriodunkid = CInt(arrList(j))

                    If isExist(mintPeriodunkid, mintEmployeeunkid, mintTranheadunkid) = False Then
                        'S.SANDEEP [04 JUN 2015] -- START
                        'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                        'If Insert() = False Then
                        If Insert(mdtCurrentdateTime) = False Then
                            'S.SANDEEP [04 JUN 2015] -- END
                            objDataOperation.ReleaseTransaction(False)
                            Return False
                        End If
                    End If

                Next j
            Next i

            objDataOperation.ReleaseTransaction(True)

            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: InsertByPeriod; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function
    'Sohail (11 sep 2010) -- End

    'Sohail (11 Nov 2010) -- Start
    'Anjan (11 Jun 2011)-Start
    'Issue : In main table entry was get inserted if there any error occurs at time of insertion in AT table.
    'Public Sub InsertAuditTrailForEmployeeExemption(ByVal objDataOperation As clsDataOperation, ByVal AuditType As Integer)

    'S.SANDEEP [04 JUN 2015] -- START
    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
    'Public Function InsertAuditTrailForEmployeeExemption(ByVal objDataOperation As clsDataOperation, ByVal AuditType As Integer) As Boolean
    '    'Anjan (11 Jun 2011)-End
    '    Dim strQ As String = ""
    '    Dim exForce As Exception

    '    objDataOperation = New clsDataOperation

    '    Try


    '        'S.SANDEEP [ 19 JULY 2012 ] -- START
    '        'Enhancement : TRA Changes

    '        'strQ = "INSERT INTO atpremployee_exemption_tran ( " & _
    '        '              "  exemptionunkid " & _
    '        '              ", yearunkid " & _
    '        '              ", periodunkid " & _
    '        '              ", employeeunkid " & _
    '        '              ", tranheadunkid " & _
    '        '              ", isapproved " & _
    '        '              ", approveruserunkid " & _
    '        '              ", audittype " & _
    '        '              ", audituserunkid " & _
    '        '              ", auditdatetime " & _
    '        '              ", ip " & _
    '        '              ", machine_name" & _
    '        '              ", disciplinefileunkid " & _
    '        '") VALUES (" & _
    '        '              "  @exemptionunkid " & _
    '        '              ", @yearunkid " & _
    '        '              ", @periodunkid " & _
    '        '              ", @employeeunkid " & _
    '        '              ", @tranheadunkid " & _
    '        '              ", @isapproved " & _
    '        '              ", @approveruserunkid " & _
    '        '              ", @audittype " & _
    '        '              ", @audituserunkid " & _
    '        '              ", @auditdatetime " & _
    '        '              ", @ip " & _
    '        '              ", @machine_name" & _
    '        '              ", @disciplinefileunkid " & _
    '        '"); SELECT @@identity" 'Sohail (26 Nov 2011) - [isapproved, approveruserunkid]


    '        strQ = "INSERT INTO atpremployee_exemption_tran ( " & _
    '                      "  exemptionunkid " & _
    '                      ", yearunkid " & _
    '                      ", periodunkid " & _
    '                      ", employeeunkid " & _
    '                      ", tranheadunkid " & _
    '                      ", isapproved " & _
    '                      ", approveruserunkid " & _
    '                      ", audittype " & _
    '                      ", audituserunkid " & _
    '                      ", auditdatetime " & _
    '                      ", ip " & _
    '                      ", machine_name" & _
    '                      ", disciplinefileunkid " & _
    '                     ", form_name " & _
    '                     ", module_name1 " & _
    '                     ", module_name2 " & _
    '                     ", module_name3 " & _
    '                     ", module_name4 " & _
    '                     ", module_name5 " & _
    '                     ", isweb " & _
    '        ") VALUES (" & _
    '                      "  @exemptionunkid " & _
    '                      ", @yearunkid " & _
    '                      ", @periodunkid " & _
    '                      ", @employeeunkid " & _
    '                      ", @tranheadunkid " & _
    '                      ", @isapproved " & _
    '                      ", @approveruserunkid " & _
    '                      ", @audittype " & _
    '                      ", @audituserunkid " & _
    '                      ", @auditdatetime " & _
    '                      ", @ip " & _
    '                      ", @machine_name" & _
    '                      ", @disciplinefileunkid " & _
    '                     ", @form_name " & _
    '                     ", @module_name1 " & _
    '                     ", @module_name2 " & _
    '                     ", @module_name3 " & _
    '                     ", @module_name4 " & _
    '                     ", @module_name5 " & _
    '                     ", @isweb " & _
    '        "); SELECT @@identity" 'Sohail (26 Nov 2011) - [isapproved, approveruserunkid]

    '        'S.SANDEEP [ 19 JULY 2012 ] -- END


    '        objDataOperation.ClearParameters()
    '        objDataOperation.AddParameter("@exemptionunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintExemptionunkid.ToString)
    '        objDataOperation.AddParameter("@yearunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintYearunkid.ToString)
    '        objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPeriodunkid.ToString)
    '        objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeunkid.ToString)
    '        objDataOperation.AddParameter("@tranheadunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintTranheadunkid.ToString)
    '        objDataOperation.AddParameter("@audittype", SqlDbType.Int, eZeeDataType.INT_SIZE, AuditType.ToString)
    '        objDataOperation.AddParameter("@audituserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, User._Object._Userunkid)
    '        objDataOperation.AddParameter("@auditdatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, ConfigParameter._Object._CurrentDateAndTime)
    '        objDataOperation.AddParameter("@ip", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, getIP)
    '        objDataOperation.AddParameter("@machine_name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, getHostName)
    '        'Sohail (26 Nov 2011) -- Start
    '        objDataOperation.AddParameter("@isapproved", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsapproved.ToString)
    '        objDataOperation.AddParameter("@approveruserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintApproveruserunkid.ToString)
    '        'Sohail (26 Nov 2011) -- End

    '        'S.SANDEEP [ 07 NOV 2011 ] -- START
    '        'ENHANCEMENT : TRA CHANGES
    '        objDataOperation.AddParameter("@disciplinefileunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintDisciplineFileUnkid)
    '        'S.SANDEEP [ 07 NOV 2011 ] -- END


    '        'S.SANDEEP [ 19 JULY 2012 ] -- START
    '        'Enhancement : TRA Changes

    '        If mstrWebFormName.Trim.Length <= 0 Then
    '            'S.SANDEEP [ 11 AUG 2012 ] -- START
    '            'ENHANCEMENT : TRA CHANGES
    '            'Dim frm As Form
    '            'For Each frm In Application.OpenForms
    '            '    If Form.ActiveForm.Name IsNot Nothing AndAlso Form.ActiveForm.Name = frm.Name Then
    '            '        objDataOperation.AddParameter("@form_name", SqlDbType.NVarChar, 500, frm.Name)
    '            '        objDataOperation.AddParameter("@isweb", SqlDbType.Bit, eZeeDataType.BIT_SIZE, False)
    '            '        
    '            '    End If
    '            'Next
    '            objDataOperation.AddParameter("@form_name", SqlDbType.NVarChar, 500, mstrForm_Name)
    '            objDataOperation.AddParameter("@isweb", SqlDbType.Bit, eZeeDataType.BIT_SIZE, False)
    '            
    '            'S.SANDEEP [ 11 AUG 2012 ] -- END
    '        Else
    '            objDataOperation.AddParameter("@form_name", SqlDbType.NVarChar, 500, mstrWebFormName)
    '            objDataOperation.AddParameter("@isweb", SqlDbType.Bit, eZeeDataType.BIT_SIZE, True)
    '            objDataOperation.AddParameter("@module_name1", SqlDbType.NVarChar, 500, Language.getMessage(mstrModuleName, 2, "WEB"))
    '        End If
    '        
    '        
    '        
    '        

    '        'S.SANDEEP [ 19 JULY 2012 ] -- END


    '        objDataOperation.ExecNonQuery(strQ)

    '        If objDataOperation.ErrorMessage <> "" Then
    '            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
    '            Throw exForce
    '        End If

    '        'Anjan (11 Jun 2011)-Start
    '        'Issue : In main table entry was get inserted if there any error occurs at time of insertion in AT table.
    '        Return True
    '        'Anjan (11 Jun 2011)-End

    '    Catch ex As Exception
    '        DisplayError.Show("-1", ex.Message, "InsertAuditTrailForEmployeeExemption", mstrModuleName)
    '        'Anjan (11 Jun 2011)-Start
    '        'Issue : In main table entry was get inserted if there any error occurs at time of insertion in AT table.
    '        Return False
    '        'Anjan (11 Jun 2011)-End
    '    Finally
    '        objDataOperation = Nothing
    '    End Try
    'End Function

    Public Function InsertAuditTrailForEmployeeExemption(ByVal objDataOperation As clsDataOperation, ByVal AuditType As Integer, ByVal dtCurrentDateTime As DateTime) As Boolean
        'Anjan (11 Jun 2011)-End
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try


            'S.SANDEEP [ 19 JULY 2012 ] -- START
            'Enhancement : TRA Changes

            'strQ = "INSERT INTO atpremployee_exemption_tran ( " & _
            '              "  exemptionunkid " & _
            '              ", yearunkid " & _
            '              ", periodunkid " & _
            '              ", employeeunkid " & _
            '              ", tranheadunkid " & _
            '              ", isapproved " & _
            '              ", approveruserunkid " & _
            '              ", audittype " & _
            '              ", audituserunkid " & _
            '              ", auditdatetime " & _
            '              ", ip " & _
            '              ", machine_name" & _
            '              ", disciplinefileunkid " & _
            '") VALUES (" & _
            '              "  @exemptionunkid " & _
            '              ", @yearunkid " & _
            '              ", @periodunkid " & _
            '              ", @employeeunkid " & _
            '              ", @tranheadunkid " & _
            '              ", @isapproved " & _
            '              ", @approveruserunkid " & _
            '              ", @audittype " & _
            '              ", @audituserunkid " & _
            '              ", @auditdatetime " & _
            '              ", @ip " & _
            '              ", @machine_name" & _
            '              ", @disciplinefileunkid " & _
            '"); SELECT @@identity" 'Sohail (26 Nov 2011) - [isapproved, approveruserunkid]


            strQ = "INSERT INTO atpremployee_exemption_tran ( " & _
                          "  exemptionunkid " & _
                          ", yearunkid " & _
                          ", periodunkid " & _
                          ", employeeunkid " & _
                          ", tranheadunkid " & _
                          ", isapproved " & _
                          ", approveruserunkid " & _
                          ", audittype " & _
                          ", audituserunkid " & _
                          ", auditdatetime " & _
                          ", ip " & _
                          ", machine_name" & _
                          ", disciplinefileunkid " & _
                         ", form_name " & _
                         " " & _
                         " " & _
                         " " & _
                         " " & _
                         " " & _
                         ", isweb " & _
            ") VALUES (" & _
                          "  @exemptionunkid " & _
                          ", @yearunkid " & _
                          ", @periodunkid " & _
                          ", @employeeunkid " & _
                          ", @tranheadunkid " & _
                          ", @isapproved " & _
                          ", @approveruserunkid " & _
                          ", @audittype " & _
                          ", @audituserunkid " & _
                          ", @auditdatetime " & _
                          ", @ip " & _
                          ", @machine_name" & _
                          ", @disciplinefileunkid " & _
                         ", @form_name " & _
                         " " & _
                         " " & _
                         " " & _
                         " " & _
                         " " & _
                         ", @isweb " & _
            "); SELECT @@identity" 'Sohail (26 Nov 2011) - [isapproved, approveruserunkid]

            'S.SANDEEP [ 19 JULY 2012 ] -- END


            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@exemptionunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintExemptionunkid.ToString)
            objDataOperation.AddParameter("@yearunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintYearunkid.ToString)
            objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPeriodunkid.ToString)
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeunkid.ToString)
            objDataOperation.AddParameter("@tranheadunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintTranheadunkid.ToString)
            objDataOperation.AddParameter("@audittype", SqlDbType.Int, eZeeDataType.INT_SIZE, AuditType.ToString)
            objDataOperation.AddParameter("@audituserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid)
            objDataOperation.AddParameter("@auditdatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, dtCurrentDateTime)
            objDataOperation.AddParameter("@ip", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, getIP)
            objDataOperation.AddParameter("@machine_name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, getHostName)
            objDataOperation.AddParameter("@isapproved", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsapproved.ToString)
            objDataOperation.AddParameter("@approveruserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintApproveruserunkid.ToString)

            objDataOperation.AddParameter("@disciplinefileunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintDisciplineFileUnkid)



            If mstrWebFormName.Trim.Length <= 0 Then
                objDataOperation.AddParameter("@form_name", SqlDbType.NVarChar, 500, mstrFormName)
                objDataOperation.AddParameter("@isweb", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsWeb)

            Else
                objDataOperation.AddParameter("@form_name", SqlDbType.NVarChar, 500, mstrWebFormName)
                objDataOperation.AddParameter("@isweb", SqlDbType.Bit, eZeeDataType.BIT_SIZE, True)
                objDataOperation.AddParameter("@module_name1", SqlDbType.NVarChar, 500, Language.getMessage(mstrModuleName, 2, "WEB"))
            End If







            objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            'Anjan (11 Jun 2011)-Start
            'Issue : In main table entry was get inserted if there any error occurs at time of insertion in AT table.
            Return True
            'Anjan (11 Jun 2011)-End

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "InsertAuditTrailForEmployeeExemption", mstrModuleName)
            'Anjan (11 Jun 2011)-Start
            'Issue : In main table entry was get inserted if there any error occurs at time of insertion in AT table.
            Return False
            'Anjan (11 Jun 2011)-End
        Finally
            objDataOperation = Nothing
        End Try
    End Function


    'S.SANDEEP [04 JUN 2015] -- END

    'Sohail (11 Nov 2010) -- End

    'Sohail (28 Jan 2012) -- Start
    'TRA - ENHANCEMENT
    Public Function GetExemtedTranHead(ByVal strTableName As String, Optional ByVal intTranHeadUnkId As Integer = 0, Optional ByVal intPeriodStatusId As Integer = 0) As DataSet

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "SELECT DISTINCT " & _
                            "premployee_exemption_tran.tranheadunkid " & _
                          ", premployee_exemption_tran.periodunkid " & _
                          ", cfcommon_period_tran.period_code " & _
                          ", cfcommon_period_tran.period_name " & _
                          ", CONVERT(CHAR(8), cfcommon_period_tran.start_date, 112) AS start_date " & _
                          ", CONVERT(CHAR(8), cfcommon_period_tran.end_date, 112) AS end_date " & _
                          ", cfcommon_period_tran.statusid " & _
                    "FROM    premployee_exemption_tran " & _
                            "JOIN cfcommon_period_tran ON premployee_exemption_tran.periodunkid = cfcommon_period_tran.periodunkid " & _
                    "WHERE   ISNULL(premployee_exemption_tran.isvoid, 0) = 0 "


            If intTranHeadUnkId > 0 Then
                strQ &= " AND premployee_exemption_tran.tranheadunkid = @tranheadunkid "
                objDataOperation.AddParameter("@tranheadunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intTranHeadUnkId.ToString)
            End If

            If intPeriodStatusId > 0 Then
                strQ &= " AND cfcommon_period_tran.statusid = @statusid "
                objDataOperation.AddParameter("@statusid", SqlDbType.Int, eZeeDataType.INT_SIZE, intPeriodStatusId.ToString)
            End If


            dsList = objDataOperation.ExecQuery(strQ, strTableName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetExemtedTranHead; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
        Return dsList
    End Function
    'Sohail (28 Jan 2012) -- End

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Public Shared Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "This Exemption is already defined for this employee. Please define new Exemption.")
            Language.setMessage(mstrModuleName, 2, "WEB")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class