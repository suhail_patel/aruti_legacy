﻿'************************************************************************************************************************************
'Class Name : clsTranhead_formula_measurement_amount_Tran.vb
'Purpose    :
'Date       :27/06/2013
'Written By :Sohail
'Modified   :
'************************************************************************************************************************************

Imports eZeeCommonLib
''' <summary>
''' Purpose: 
''' Developer: Sohail
''' </summary>
Public Class clsTranhead_formula_measurement_amount_Tran
    Private Const mstrModuleName = "clsTranhead_formula_measurement_amount_Tran"
    Dim objDataOperation As clsDataOperation
    Dim mstrMessage As String = ""

#Region " Private variables "
    Private mintTranheadformulameasurementamounttranunkid As Integer
    Private mintFormulatranheadunkid As Integer
    Private mintMeasureunkid As Integer
    Private mblnIsvoid As Boolean
    Private mintVoiduserunkid As Integer
    Private mdtVoiddatetime As Date
    Private mstrVoidreason As String = String.Empty
    Private mintDefaultvalue_Id As Integer
    Private mintPeriodunkid As Integer
    Private mintTranheadslabtranunkid As Integer = 0 'Sohail (24 Sep 2013)

    Private mdtFormula As DataTable
#End Region

#Region " Constructor "
    Public Sub New()
        mdtFormula = New DataTable("FormulaMeasurement")
        Dim dCol As New DataColumn

        Try
            mdtFormula.Columns.Add("formulatranheadunkid", System.Type.GetType("System.Int32")).DefaultValue = -1
            mdtFormula.Columns.Add("measureunkid", System.Type.GetType("System.Int32")).DefaultValue = -1
            mdtFormula.Columns.Add("isvoid", System.Type.GetType("System.Boolean")).DefaultValue = False
            mdtFormula.Columns.Add("voiduserunkid", System.Type.GetType("System.Int32")).DefaultValue = -1
            mdtFormula.Columns.Add("voiddatetime", System.Type.GetType("System.DateTime")).DefaultValue = Nothing
            mdtFormula.Columns.Add("voidreason", System.Type.GetType("System.String")).DefaultValue = String.Empty
            mdtFormula.Columns.Add("defaultvalue_id", System.Type.GetType("System.Int32")).DefaultValue = 0
            mdtFormula.Columns.Add("periodunkid", System.Type.GetType("System.Int32")).DefaultValue = 0
            mdtFormula.Columns.Add("tranheadslabtranunkid", System.Type.GetType("System.Int32")).DefaultValue = 0 'Sohail (24 Sep 2013)
            mdtFormula.Columns.Add("AUD", System.Type.GetType("System.String")).DefaultValue = String.Empty

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "New", mstrModuleName)
        End Try
    End Sub
#End Region

#Region " Properties "

    'S.SANDEEP [28-May-2018] -- START 
    'ISSUE/ENHANCEMENT : {Audit Trails} 
    Private mstrFormName As String = String.Empty
    Public WriteOnly Property _FormName() As String
        Set(ByVal value As String)
            mstrFormName = value
        End Set
    End Property

    Private mstrClientIP As String = ""
    Public WriteOnly Property _ClientIP() As String
        Set(ByVal value As String)
            mstrClientIP = value
        End Set
    End Property

    Private mstrHostName As String = ""
    Public WriteOnly Property _HostName() As String
        Set(ByVal value As String)
            mstrHostName = value
        End Set
    End Property

    Private mblnIsWeb As Boolean = False
    Public WriteOnly Property _FromWeb() As Boolean
        Set(ByVal value As Boolean)
            mblnIsWeb = value
        End Set
    End Property

    Private mintAuditUserId As Integer = 0
    Public WriteOnly Property _AuditUserId() As Integer
        Set(ByVal value As Integer)
            mintAuditUserId = value
        End Set
    End Property

    Private mintCompanyUnkid As Integer = 0
    Public WriteOnly Property _CompanyUnkid() As Integer
        Set(ByVal value As Integer)
            mintCompanyUnkid = value
        End Set
    End Property

    Private mdtAuditDate As DateTime = Now
    Public WriteOnly Property _AuditDate() As DateTime
        Set(ByVal value As DateTime)
            mdtAuditDate = value
        End Set
    End Property


    Private mintLoginEmployeeunkid As Integer = 0
    Public Property _LoginEmployeeunkid() As Integer
        Get
            Return mintLoginEmployeeunkid
        End Get
        Set(ByVal value As Integer)
            mintLoginEmployeeunkid = value
        End Set
    End Property

    Private mintVoidlogingemployeeunkid As Integer = 0
    Public Property _Voidloginemployeeunkid() As Integer
        Get
            Return mintVoidlogingemployeeunkid
        End Get
        Set(ByVal value As Integer)
            mintVoidlogingemployeeunkid = value
        End Set
    End Property
    'S.SANDEEP [28-May-2018] -- END 


    Public Property _Datasource() As DataTable
        Get
            Return mdtFormula
        End Get
        Set(ByVal value As DataTable)
            mdtFormula = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set formulatranheadunkid
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Formulatranheadunkid() As Integer
        Get
            Return mintFormulatranheadunkid
        End Get
        Set(ByVal value As Integer)
            mintFormulatranheadunkid = value
            Call GetData()
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get Message from Class 
    ''' Modify By: Sohail
    ''' </summary>
    Public ReadOnly Property _Message() As String
        Get
            Return mstrMessage
        End Get
    End Property

    ''' <summary>
    ''' Purpose: Get or Set tranheadformulameasurementamounttranunkid
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Tranheadformulameasurementamounttranunkid() As Integer
        Get
            Return mintTranheadformulameasurementamounttranunkid
        End Get
        Set(ByVal value As Integer)
            mintTranheadformulameasurementamounttranunkid = value
            'Call GetData()
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set measureunkid
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Measureunkid() As Integer
        Get
            Return mintMeasureunkid
        End Get
        Set(ByVal value As Integer)
            mintMeasureunkid = value
        End Set
    End Property

    'Sohail (28 Jan 2019) -- Start
    'NMB Enhancement - 76.1 - Option "Prorate Flat Rate head" on Transaction Head for Recurrent Head.
    Private xDataOpr As clsDataOperation = Nothing
    Public WriteOnly Property _DataOperation() As clsDataOperation
        Set(ByVal value As clsDataOperation)
            xDataOpr = value
        End Set
    End Property
    'Sohail (28 Jan 2019) -- End

#End Region

    ''' <summary>
    ''' Modify By: Sohail
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Sub GetData()
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        'Sohail (28 Jan 2019) -- Start
        'NMB Enhancement - 76.1 - Option "Prorate Flat Rate head" on Transaction Head for Recurrent Head.
        'objDataOperation = New clsDataOperation
        If xDataOpr Is Nothing Then
            objDataOperation = New clsDataOperation
        Else
            objDataOperation = xDataOpr
        End If
        objDataOperation.ClearParameters()
        'Sohail (28 Jan 2019) -- End

        Try
            strQ = "SELECT " & _
              "  tranheadformulameasurementamounttranunkid " & _
              ", formulatranheadunkid " & _
              ", measureunkid " & _
              ", isvoid " & _
              ", voiduserunkid " & _
              ", voiddatetime " & _
              ", voidreason " & _
              ", defaultvalue_id " & _
              ", periodunkid " & _
              ", ISNULL(tranheadslabtranunkid, 0) AS tranheadslabtranunkid " & _
              ", '' AS AUD " & _
             "FROM prtranhead_formula_measurement_amount_tran " & _
             "WHERE formulatranheadunkid = @formulatranheadunkid " & _
             "AND ISNULL(isvoid,0) = 0 "
            'Sohail (24 Sep 2013) - [tranheadslabtranunkid]

            objDataOperation.AddParameter("@formulatranheadunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintFormulatranheadunkid.ToString)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            mdtFormula.Rows.Clear()
            Dim dRow As DataRow

            For Each dtRow As DataRow In dsList.Tables(0).Rows
                dRow = mdtFormula.NewRow
                dRow.Item("formulatranheadunkid") = CInt(dtRow.Item("formulatranheadunkid"))
                dRow.Item("measureunkid") = CInt(dtRow.Item("measureunkid"))
                mblnIsvoid = CBool(dtRow.Item("isvoid"))
                mintVoiduserunkid = CInt(dtRow.Item("voiduserunkid"))
                If IsDBNull(dtRow.Item("voiddatetime")) = True Then
                    mdtVoiddatetime = Nothing
                Else
                    mdtVoiddatetime = dtRow.Item("voiddatetime").ToString
                End If
                mstrVoidreason = dtRow.Item("voidreason").ToString
                dRow.Item("defaultvalue_id") = CInt(dtRow.Item("defaultvalue_id"))
                dRow.Item("periodunkid") = CInt(dtRow.Item("periodunkid"))
                dRow.Item("tranheadslabtranunkid") = CInt(dtRow.Item("tranheadslabtranunkid")) 'Sohail (24 Sep 2013)
                dRow.Item("AUD") = dtRow.Item("AUD").ToString
                mdtFormula.Rows.Add(dRow)
            Next
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetData; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            'Sohail (28 Jan 2019) -- Start
            'NMB Enhancement - 76.1 - Option "Prorate Flat Rate head" on Transaction Head for Recurrent Head.
            'objDataOperation = Nothing
            If xDataOpr Is Nothing Then objDataOperation = Nothing
            'Sohail (28 Jan 2019) -- End
        End Try
    End Sub


    ''' <summary>
    ''' Modify By: Sohail
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function GetList(ByVal strTableName As String, Optional ByVal blnOnlyActive As Boolean = True) As DataSet
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        'Sohail (28 Jan 2019) -- Start
        'NMB Enhancement - 76.1 - Option "Prorate Flat Rate head" on Transaction Head for Recurrent Head.
        'objDataOperation = New clsDataOperation
        If xDataOpr Is Nothing Then
            objDataOperation = New clsDataOperation
        Else
            objDataOperation = xDataOpr
        End If
        objDataOperation.ClearParameters()
        'Sohail (28 Jan 2019) -- End

        Try
            strQ = "SELECT " & _
              "  tranheadformulameasurementamounttranunkid " & _
              ", formulatranheadunkid " & _
              ", measureunkid " & _
              ", isvoid " & _
              ", voiduserunkid " & _
              ", voiddatetime " & _
              ", voidreason " & _
              ", defaultvalue_id " & _
              ", periodunkid " & _
              ", ISNULL(prtranhead_formula_measurement_amount_tran.tranheadslabtranunkid, 0) AS tranheadslabtranunkid " & _
             "FROM prtranhead_formula_measurement_amount_tran " & _
             "WHERE ISNULL(isvoid, 0) = 0 "
            'Sohail (24 Sep 2013) - [tranheadslabtranunkid]

            If blnOnlyActive Then
                strQ &= " WHERE isactive = 1 "
            End If

            dsList = objDataOperation.ExecQuery(strQ, strTableName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            'Sohail (28 Jan 2019) -- Start
            'NMB Enhancement - 76.1 - Option "Prorate Flat Rate head" on Transaction Head for Recurrent Head.
            'objDataOperation = Nothing
            If xDataOpr Is Nothing Then objDataOperation = Nothing
            'Sohail (28 Jan 2019) -- End
        End Try
        Return dsList
    End Function

    Public Function InsertDelete(ByVal mintOldTranheadslabtranunkid As Integer, ByVal mintNewTranheadslabtranunkid As Integer, ByVal intUserunkid As Integer, ByVal dtCurrentDateAndTime As Date) As Boolean
        'Sohail (21 Aug 2015) - [intUserunkid, dtCurrentDateAndTime]

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        'Sohail (28 Jan 2019) -- Start
        'NMB Enhancement - 76.1 - Option "Prorate Flat Rate head" on Transaction Head for Recurrent Head.
        'objDataOperation = New clsDataOperation
        If xDataOpr Is Nothing Then
            objDataOperation = New clsDataOperation
        Else
            objDataOperation = xDataOpr
        End If
        objDataOperation.ClearParameters()
        'Sohail (28 Jan 2019) -- End

        Try
            For i = 0 To mdtFormula.Rows.Count - 1
                With mdtFormula.Rows(i)
                    objDataOperation.ClearParameters()
                    If Not IsDBNull(.Item("AUD")) Then

                        'Sohail (24 Sep 2013) -- Start
                        'TRA - ENHANCEMENT
                        If CInt(.Item("tranheadslabtranunkid")) <> mintOldTranheadslabtranunkid Then Continue For
                        'Sohail (24 Sep 2013) -- End

                        Select Case .Item("AUD")
                            Case "A"

                                strQ = "INSERT INTO prtranhead_formula_measurement_amount_tran ( " & _
                                          "  formulatranheadunkid " & _
                                          ", measureunkid " & _
                                          ", isvoid " & _
                                          ", voiduserunkid " & _
                                          ", voiddatetime " & _
                                          ", voidreason " & _
                                          ", defaultvalue_id " & _
                                          ", periodunkid" & _
                                          ", tranheadslabtranunkid " & _
                                        ") VALUES (" & _
                                          "  @formulatranheadunkid " & _
                                          ", @measureunkid " & _
                                          ", @isvoid " & _
                                          ", @voiduserunkid " & _
                                          ", @voiddatetime " & _
                                          ", @voidreason " & _
                                          ", @defaultvalue_id " & _
                                          ", @periodunkid" & _
                                          ", @tranheadslabtranunkid " & _
                                        "); SELECT @@identity"
                                'Sohail (24 Sep 2013) - [tranheadslabtranunkid]

                                objDataOperation.AddParameter("@formulatranheadunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintFormulatranheadunkid.ToString)
                                objDataOperation.AddParameter("@measureunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("measureunkid").ToString)
                                objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, CBool(.Item("isvoid")))
                                objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("voiduserunkid").ToString)
                                If .Item("voiddatetime").ToString = Nothing Then
                                    objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
                                Else
                                    objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, .Item("voiddatetime").ToString)
                                End If
                                objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, .Item("voidreason").ToString)
                                objDataOperation.AddParameter("@defaultvalue_id", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("defaultvalue_id").ToString)
                                objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("periodunkid").ToString)
                                objDataOperation.AddParameter("@tranheadslabtranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintNewTranheadslabtranunkid) 'Sohail (24 Sep 2013)

                                dsList = objDataOperation.ExecQuery(strQ, "List")

                                If objDataOperation.ErrorMessage <> "" Then
                                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                    Throw exForce
                                End If

                                mintTranheadformulameasurementamounttranunkid = dsList.Tables(0).Rows(0)(0)
                                If .Item("formulatranheadunkid") > 0 Then
                                    'S.SANDEEP [28-May-2018] -- START
                                    'ISSUE/ENHANCEMENT : {Audit Trails} 
                                    Dim objCommonATLog As New clsCommonATLog
                                    objCommonATLog._FormName = mstrFormName
                                    objCommonATLog._LoginEmployeeUnkid = mintLoginEmployeeunkid
                                    objCommonATLog._ClientIP = mstrClientIP
                                    objCommonATLog._HostName = mstrHostName
                                    objCommonATLog._FromWeb = mblnIsWeb
                                    objCommonATLog._AuditUserId = mintAuditUserId
                                    objCommonATLog._CompanyUnkid = mintCompanyUnkid
                                    objCommonATLog._AuditDate = mdtAuditDate
                                    'S.SANDEEP [28-May-2018] -- END

                                    If objCommonATLog.Insert_TranAtLog(objDataOperation, "prtranhead_master", "tranheadunkid", mintFormulatranheadunkid, "prtranhead_formula_measurement_amount_tran", "tranheadformulameasurementamounttranunkid", mintTranheadformulameasurementamounttranunkid, 2, 1) = False Then
                                        Return False
                                    End If

                                    'S.SANDEEP [28-May-2018] -- START
                                    'ISSUE/ENHANCEMENT : {Audit Trails} 
                                    objCommonATLog = Nothing
                                    'S.SANDEEP [28-May-2018] -- END

                                Else
                                    'S.SANDEEP [28-May-2018] -- START
                                    'ISSUE/ENHANCEMENT : {Audit Trails} 
                                    Dim objCommonATLog As New clsCommonATLog
                                    objCommonATLog._FormName = mstrFormName
                                    objCommonATLog._LoginEmployeeUnkid = mintLoginEmployeeunkid
                                    objCommonATLog._ClientIP = mstrClientIP
                                    objCommonATLog._HostName = mstrHostName
                                    objCommonATLog._FromWeb = mblnIsWeb
                                    objCommonATLog._AuditUserId = mintAuditUserId
                                    objCommonATLog._CompanyUnkid = mintCompanyUnkid
                                    objCommonATLog._AuditDate = mdtAuditDate
                                    'S.SANDEEP [28-May-2018] -- END

                                    If objCommonATLog.Insert_TranAtLog(objDataOperation, "prtranhead_master", "tranheadunkid", mintFormulatranheadunkid, "prtranhead_formula_measurement_amount_tran", "tranheadformulameasurementamounttranunkid", mintTranheadformulameasurementamounttranunkid, 1, 1) = False Then
                                        Return False
                                    End If

                                    'S.SANDEEP [28-May-2018] -- START
                                    'ISSUE/ENHANCEMENT : {Audit Trails} 
                                    objCommonATLog = Nothing
                                    'S.SANDEEP [28-May-2018] -- END

                                End If

                            Case "D"

                                If .Item("formulatranheadunkid") > 0 Then
                                    'S.SANDEEP [28-May-2018] -- START
                                    'ISSUE/ENHANCEMENT : {Audit Trails} 
                                    Dim objCommonATLog As New clsCommonATLog
                                    objCommonATLog._FormName = mstrFormName
                                    objCommonATLog._LoginEmployeeUnkid = mintLoginEmployeeunkid
                                    objCommonATLog._ClientIP = mstrClientIP
                                    objCommonATLog._HostName = mstrHostName
                                    objCommonATLog._FromWeb = mblnIsWeb
                                    objCommonATLog._AuditUserId = mintAuditUserId
                                    objCommonATLog._CompanyUnkid = mintCompanyUnkid
                                    objCommonATLog._AuditDate = mdtAuditDate
                                    'S.SANDEEP [28-May-2018] -- END

                                    If objCommonATLog.VoidAtTranAtLog(objDataOperation, "prtranhead_master", "tranheadunkid", mintFormulatranheadunkid, "prtranhead_formula_measurement_amount_tran", "tranheadformulameasurementamounttranunkid", 2, 3, "formulatranheadunkid", "measureunkid = " & .Item("measureunkid") & " AND tranheadslabtranunkid = " & .Item("tranheadslabtranunkid") & "") = False Then
                                        Return False
                                    End If

                                    'S.SANDEEP [28-May-2018] -- START
                                    'ISSUE/ENHANCEMENT : {Audit Trails} 
                                    objCommonATLog = Nothing
                                    'S.SANDEEP [28-May-2018] -- END


                                    strQ = "UPDATE prtranhead_formula_measurement_amount_tran SET " & _
                                            " isvoid = 1 " & _
                                            ", voiduserunkid = @voiduserunkid " & _
                                            ", voiddatetime = @voiddatetime " & _
                                            ", voidreason = @voidreason " & _
                                        "WHERE formulatranheadunkid = @formulatranheadunkid " & _
                                        "AND measureunkid = @measureunkid " & _
                                        "AND tranheadslabtranunkid = @tranheadslabtranunkid "
                                    'Sohail (24 Sep 2013) - [tranheadslabtranunkid]

                                    objDataOperation.AddParameter("@formulatranheadunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintFormulatranheadunkid.ToString)
                                    objDataOperation.AddParameter("@measureunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("measureunkid").ToString) 'Sohail (22 Feb 2012)
                                    'Sohail (21 Aug 2015) -- Start
                                    'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                                    'objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, User._Object._Userunkid)
                                    'objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, ConfigParameter._Object._CurrentDateAndTime)
                                    objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUserunkid)
                                    objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, dtCurrentDateAndTime)
                                    'Sohail (21 Aug 2015) -- End
                                    objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, .Item("voidreason").ToString)
                                    objDataOperation.AddParameter("@tranheadslabtranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("tranheadslabtranunkid").ToString) 'Sohail (24 Sep 2013)

                                    dsList = objDataOperation.ExecQuery(strQ, "List")

                                    If objDataOperation.ErrorMessage <> "" Then
                                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                        Throw exForce
                                    End If
                                End If

                        End Select
                    End If
                End With
            Next

            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: InsertDelete; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            'Sohail (28 Jan 2019) -- Start
            'NMB Enhancement - 76.1 - Option "Prorate Flat Rate head" on Transaction Head for Recurrent Head.
            'objDataOperation = Nothing
            If xDataOpr Is Nothing Then objDataOperation = Nothing
            'Sohail (28 Jan 2019) -- End
        End Try
    End Function

    Public Function VoidByTranHeadUnkID(ByVal intTranHeadUnkid As Integer, _
                                        ByVal intVoidUserID As Integer, ByVal dtVoidDateTime As DateTime, ByVal strVoidReason As String) As Boolean

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        'Sohail (28 Jan 2019) -- Start
        'NMB Enhancement - 76.1 - Option "Prorate Flat Rate head" on Transaction Head for Recurrent Head.
        'objDataOperation = New clsDataOperation
        If xDataOpr Is Nothing Then
            objDataOperation = New clsDataOperation
        Else
            objDataOperation = xDataOpr
        End If
        objDataOperation.ClearParameters()
        'Sohail (28 Jan 2019) -- End

        Try
            'S.SANDEEP [28-May-2018] -- START
            'ISSUE/ENHANCEMENT : {Audit Trails} 
            Dim objCommonATLog As New clsCommonATLog
            objCommonATLog._FormName = mstrFormName
            objCommonATLog._LoginEmployeeUnkid = mintLoginEmployeeunkid
            objCommonATLog._ClientIP = mstrClientIP
            objCommonATLog._HostName = mstrHostName
            objCommonATLog._FromWeb = mblnIsWeb
            objCommonATLog._AuditUserId = mintAuditUserId
            objCommonATLog._CompanyUnkid = mintCompanyUnkid
            objCommonATLog._AuditDate = mdtAuditDate
            'S.SANDEEP [28-May-2018] -- END

            If objCommonATLog.VoidAtTranAtLog(objDataOperation, "prtranhead_master", "tranheadunkid", intTranHeadUnkid, "prtranhead_formula_measurement_amount_tran", "tranheadformulameasurementamounttranunkid", 3, 3, "formulatranheadunkid") = False Then
                Return False
            End If

            'S.SANDEEP [28-May-2018] -- START
            'ISSUE/ENHANCEMENT : {Audit Trails} 
            objCommonATLog = Nothing
            'S.SANDEEP [28-May-2018] -- END


            strQ = "UPDATE prtranhead_formula_measurement_amount_tran SET " & _
                        " isvoid = 1 " & _
                        ", voiduserunkid = @voiduserunkid " & _
                        ", voiddatetime = @voiddatetime " & _
                        ", voidreason = @voidreason " & _
                    "WHERE formulatranheadunkid = @formulatranheadunkid "

            objDataOperation.AddParameter("@formulatranheadunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intTranHeadUnkid)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intVoidUserID)
            objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, dtVoidDateTime)
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strVoidReason)

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: VoidByTranHeadUnkID; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            'Sohail (28 Jan 2019) -- Start
            'NMB Enhancement - 76.1 - Option "Prorate Flat Rate head" on Transaction Head for Recurrent Head.
            'objDataOperation = Nothing
            If xDataOpr Is Nothing Then objDataOperation = Nothing
            'Sohail (28 Jan 2019) -- End
        End Try
    End Function





    '''' <summary>
    '''' Modify By: Sohail
    '''' </summary>
    '''' <returns>Boolean</returns>
    '''' <purpose> INSERT INTO Database Table (prtranhead_formula_measurement_amount_tran) </purpose>
    'Public Function Insert() As Boolean
    '    'If isExist(mstrName) Then
    '    '    mstrMessage = "<Message>"
    '    '    Return False
    '    'End If

    '    Dim dsList As DataSet = Nothing
    '    Dim strQ As String = ""
    '    Dim exForce As Exception

    '    objDataOperation = New clsDataOperation

    '    Try
    '        objDataOperation.AddParameter("@formulatranheadunkid", SqlDbType.int, eZeeDataType.INT_SIZE, mintformulatranheadunkid.ToString)
    '        objDataOperation.AddParameter("@measureunkid", SqlDbType.int, eZeeDataType.INT_SIZE, mintmeasureunkid.ToString)
    '        objDataOperation.AddParameter("@isvoid", SqlDbType.bit, eZeeDataType.BIT_SIZE, mblnisvoid.ToString)
    '        objDataOperation.AddParameter("@voiduserunkid", SqlDbType.int, eZeeDataType.INT_SIZE, mintvoiduserunkid.ToString)
    '        objDataOperation.AddParameter("@voiddatetime", SqlDbType.datetime, eZeeDataType.DATETIME_SIZE, mdtvoiddatetime.ToString)
    '        objDataOperation.AddParameter("@voidreason", SqlDbType.nvarchar, eZeeDataType.NAME_SIZE, mstrvoidreason.ToString)
    '        objDataOperation.AddParameter("@defaultvalue_id", SqlDbType.int, eZeeDataType.INT_SIZE, mintdefaultvalue_id.ToString)
    '        objDataOperation.AddParameter("@periodunkid", SqlDbType.int, eZeeDataType.INT_SIZE, mintperiodunkid.ToString)

    '        StrQ = "INSERT INTO prtranhead_formula_measurement_amount_tran ( " & _
    '          "  formulatranheadunkid " & _
    '          ", measureunkid " & _
    '          ", isvoid " & _
    '          ", voiduserunkid " & _
    '          ", voiddatetime " & _
    '          ", voidreason " & _
    '          ", defaultvalue_id " & _
    '          ", periodunkid" & _
    '        ") VALUES (" & _
    '          "  @formulatranheadunkid " & _
    '          ", @measureunkid " & _
    '          ", @isvoid " & _
    '          ", @voiduserunkid " & _
    '          ", @voiddatetime " & _
    '          ", @voidreason " & _
    '          ", @defaultvalue_id " & _
    '          ", @periodunkid" & _
    '        "); SELECT @@identity"

    '        dsList = objDataOperation.ExecQuery(strQ, "List")

    '        If objDataOperation.ErrorMessage <> "" Then
    '            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
    '            Throw exForce
    '        End If

    '        minttranheadformulameasurementamounttranunkid = dsList.Tables(0).Rows(0).Item(0)

    '        Return True
    '    Catch ex As Exception
    '        Throw New Exception(ex.Message & "; Procedure Name: Insert; Module Name: " & mstrModuleName)
    '        Return False
    '    Finally
    '        exForce = Nothing
    '        If dsList IsNot Nothing Then dsList.Dispose()
    '        objDataOperation = Nothing
    '    End Try
    'End Function

    '''' <summary>
    '''' Modify By: Sohail
    '''' </summary>
    '''' <returns>Boolean</returns>
    '''' <purpose> Update Database Table (prtranhead_formula_measurement_amount_tran) </purpose>
    'Public Function Update() As Boolean
    '    'If isExist(mstrName, mintTranheadformulameasurementamounttranunkid) Then
    '    '    mstrMessage = "<Message>"
    '    '    Return False
    '    'End If

    '    Dim dsList As DataSet = Nothing
    '    Dim strQ As String = ""
    '    Dim exForce As Exception

    '    objDataOperation = New clsDataOperation

    '    Try
    '        objDataOperation.AddParameter("@tranheadformulameasurementamounttranunkid", SqlDbType.int, eZeeDataType.INT_SIZE, minttranheadformulameasurementamounttranunkid.ToString)
    '        objDataOperation.AddParameter("@formulatranheadunkid", SqlDbType.int, eZeeDataType.INT_SIZE, mintformulatranheadunkid.ToString)
    '        objDataOperation.AddParameter("@measureunkid", SqlDbType.int, eZeeDataType.INT_SIZE, mintmeasureunkid.ToString)
    '        objDataOperation.AddParameter("@isvoid", SqlDbType.bit, eZeeDataType.BIT_SIZE, mblnisvoid.ToString)
    '        objDataOperation.AddParameter("@voiduserunkid", SqlDbType.int, eZeeDataType.INT_SIZE, mintvoiduserunkid.ToString)
    '        objDataOperation.AddParameter("@voiddatetime", SqlDbType.datetime, eZeeDataType.DATETIME_SIZE, mdtvoiddatetime.ToString)
    '        objDataOperation.AddParameter("@voidreason", SqlDbType.nvarchar, eZeeDataType.NAME_SIZE, mstrvoidreason.ToString)
    '        objDataOperation.AddParameter("@defaultvalue_id", SqlDbType.int, eZeeDataType.INT_SIZE, mintdefaultvalue_id.ToString)
    '        objDataOperation.AddParameter("@periodunkid", SqlDbType.int, eZeeDataType.INT_SIZE, mintperiodunkid.ToString)

    '        StrQ = "UPDATE prtranhead_formula_measurement_amount_tran SET " & _
    '          "  formulatranheadunkid = @formulatranheadunkid" & _
    '          ", measureunkid = @measureunkid" & _
    '          ", isvoid = @isvoid" & _
    '          ", voiduserunkid = @voiduserunkid" & _
    '          ", voiddatetime = @voiddatetime" & _
    '          ", voidreason = @voidreason" & _
    '          ", defaultvalue_id = @defaultvalue_id" & _
    '          ", periodunkid = @periodunkid " & _
    '        "WHERE tranheadformulameasurementamounttranunkid = @tranheadformulameasurementamounttranunkid "

    '        Call objDataOperation.ExecNonQuery(strQ)

    '        If objDataOperation.ErrorMessage <> "" Then
    '            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
    '            Throw exForce
    '        End If

    '        Return True
    '    Catch ex As Exception
    '        Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
    '        Return False
    '    Finally
    '        exForce = Nothing
    '        If dsList IsNot Nothing Then dsList.Dispose()
    '        objDataOperation = Nothing
    '    End Try
    'End Function

    '''' <summary>
    '''' Modify By: Sohail
    '''' </summary>
    '''' <returns>Boolean</returns>
    '''' <purpose> Delete Database Table (prtranhead_formula_measurement_amount_tran) </purpose>
    'Public Function Delete(ByVal intUnkid As Integer) As Boolean
    '    If isUsed(intUnkid) Then
    '        mstrMessage = "<Message>"
    '        Return False
    '    End If

    '    Dim dsList As DataSet = Nothing
    '    Dim strQ As String = ""
    '    Dim exForce As Exception

    '    objDataOperation = New clsDataOperation

    '    Try
    '        StrQ = "DELETE FROM prtranhead_formula_measurement_amount_tran " & _
    '        "WHERE tranheadformulameasurementamounttranunkid = @tranheadformulameasurementamounttranunkid "

    '        objDataOperation.AddParameter("@tranheadformulameasurementamounttranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

    '        Call objDataOperation.ExecNonQuery(strQ)

    '        If objDataOperation.ErrorMessage <> "" Then
    '            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
    '            Throw exForce
    '        End If

    '        Return True
    '    Catch ex As Exception
    '        Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
    '        Return False
    '    Finally
    '        exForce = Nothing
    '        If dsList IsNot Nothing Then dsList.Dispose()
    '        objDataOperation = Nothing
    '    End Try
    'End Function

    '''' <summary>
    '''' Modify By: Sohail
    '''' </summary>
    '''' <purpose> Assign all Property variable </purpose>
    'Public Function isUsed(ByVal intUnkid As Integer) As Boolean
    '    Dim dsList As DataSet = Nothing
    '    Dim strQ As String = ""
    '    Dim exForce As Exception

    '    objDataOperation = New clsDataOperation

    '    Try
    '        StrQ = "<Query>"

    '        objDataOperation.AddParameter("@tranheadformulameasurementamounttranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

    '        dsList = objDataOperation.ExecQuery(strQ, "List")

    '        If objDataOperation.ErrorMessage <> "" Then
    '            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
    '            Throw exForce
    '        End If

    '        Return dsList.tables(0).rows.count > 0
    '    Catch ex As Exception
    '        Throw New Exception(ex.Message & "; Procedure Name: isUsed; Module Name: " & mstrModuleName)
    '    Finally
    '        exForce = Nothing
    '        If dsList IsNot Nothing Then dsList.Dispose()
    '        objDataOperation = Nothing
    '    End Try
    'End Function

    '''' <summary>
    '''' Modify By: Sohail
    '''' </summary>
    '''' <purpose> Assign all Property variable </purpose>
    'Public Function isExist(ByVal strCode As String, ByVal strName As String, Optional ByVal intUnkid As Integer = -1) As Boolean
    '    Dim dsList As DataSet = Nothing
    '    Dim strQ As String = ""
    '    Dim exForce As Exception

    '    objDataOperation = New clsDataOperation

    '    Try
    '        StrQ = "SELECT " & _
    '          "  tranheadformulameasurementamounttranunkid " & _
    '          ", formulatranheadunkid " & _
    '          ", measureunkid " & _
    '          ", isvoid " & _
    '          ", voiduserunkid " & _
    '          ", voiddatetime " & _
    '          ", voidreason " & _
    '          ", defaultvalue_id " & _
    '          ", periodunkid " & _
    '         "FROM prtranhead_formula_measurement_amount_tran " & _
    '         "WHERE name = @name " & _
    '         "AND code = @code "

    '        If intUnkid > 0 Then
    '            strQ &= " AND tranheadformulameasurementamounttranunkid <> @tranheadformulameasurementamounttranunkid"
    '        End If

    '        objDataOperation.AddParameter("@code", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strCode)
    '        objDataOperation.AddParameter("@name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strName)
    '        objDataOperation.AddParameter("@tranheadformulameasurementamounttranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

    '        dsList = objDataOperation.ExecQuery(strQ, "List")

    '        If objDataOperation.ErrorMessage <> "" Then
    '            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
    '            Throw exForce
    '        End If

    '        Return dsList.tables(0).rows.count > 0
    '    Catch ex As Exception
    '        Throw New Exception(ex.Message & "; Procedure Name: isExist; Module Name: " & mstrModuleName)
    '    Finally
    '        exForce = Nothing
    '        If dsList IsNot Nothing Then dsList.Dispose()
    '        objDataOperation = Nothing
    '    End Try
    'End Function

End Class
