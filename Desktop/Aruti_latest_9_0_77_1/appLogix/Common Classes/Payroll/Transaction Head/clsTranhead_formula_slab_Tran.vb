﻿'************************************************************************************************************************************
'Class Name : clsTranhead_formula_slab_Tran.vb
'Purpose    :
'Date       :12/04/2013
'Written By :Sohail
'Modified   :
'************************************************************************************************************************************

Imports eZeeCommonLib
''' <summary>
''' Purpose: 
''' Developer: Sohail
''' </summary>
Public Class clsTranhead_formula_slab_Tran
    Private Const mstrModuleName = "clsTranhead_formula_slab_Tran"
    Dim objDataOperation As clsDataOperation
    Dim mstrMessage As String = ""

#Region " Private variables "
    Private mintTranheadformulaslabtranunkid As Integer
    Private mintTranheadunkid As Integer
    Private mintPeriodunkid As Integer
    Private mstrFormula As String = String.Empty
    Private mstrFormulaid As String = String.Empty
    Private mintUserunkid As Integer
    Private mblnIsvoid As Boolean
    Private mintVoiduserunkid As Integer
    Private mdtVoiddatetime As Date
    Private mstrVoidreason As String = String.Empty

    Private mdtFormula As DataTable
#End Region

#Region " Constructor "
    Public Sub New()
        mdtFormula = New DataTable("FormulaSlab")
        Dim dCol As New DataColumn

        Try
            mdtFormula.Columns.Add("tranheadformulaslabtranunkid", System.Type.GetType("System.Int32")).DefaultValue = -1
            mdtFormula.Columns.Add("tranheadunkid", System.Type.GetType("System.Int32")).DefaultValue = -1
            mdtFormula.Columns.Add("periodunkid", System.Type.GetType("System.Int32")).DefaultValue = -1
            mdtFormula.Columns.Add("end_date", System.Type.GetType("System.String")).DefaultValue = String.Empty
            mdtFormula.Columns.Add("formula", System.Type.GetType("System.String")).DefaultValue = String.Empty
            mdtFormula.Columns.Add("formulaid", System.Type.GetType("System.String")).DefaultValue = String.Empty
            mdtFormula.Columns.Add("userunkid", System.Type.GetType("System.Int32")).DefaultValue = -1
            mdtFormula.Columns.Add("isvoid", System.Type.GetType("System.Boolean")).DefaultValue = False
            mdtFormula.Columns.Add("voiduserunkid", System.Type.GetType("System.Int32")).DefaultValue = -1
            mdtFormula.Columns.Add("voiddatetime", System.Type.GetType("System.DateTime")).DefaultValue = Nothing
            mdtFormula.Columns.Add("voidreason", System.Type.GetType("System.String")).DefaultValue = String.Empty
            mdtFormula.Columns.Add("AUD", System.Type.GetType("System.String")).DefaultValue = String.Empty

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "New", mstrModuleName)
        End Try
    End Sub
#End Region

#Region " Properties "

    'S.SANDEEP [28-May-2018] -- START 
    'ISSUE/ENHANCEMENT : {Audit Trails} 
    Private mstrFormName As String = String.Empty
    Public WriteOnly Property _FormName() As String
        Set(ByVal value As String)
            mstrFormName = value
        End Set
    End Property

    Private mstrClientIP As String = ""
    Public WriteOnly Property _ClientIP() As String
        Set(ByVal value As String)
            mstrClientIP = value
        End Set
    End Property

    Private mstrHostName As String = ""
    Public WriteOnly Property _HostName() As String
        Set(ByVal value As String)
            mstrHostName = value
        End Set
    End Property

    Private mblnIsWeb As Boolean = False
    Public WriteOnly Property _FromWeb() As Boolean
        Set(ByVal value As Boolean)
            mblnIsWeb = value
        End Set
    End Property

    Private mintAuditUserId As Integer = 0
    Public WriteOnly Property _AuditUserId() As Integer
        Set(ByVal value As Integer)
            mintAuditUserId = value
        End Set
    End Property

    Private mintCompanyUnkid As Integer = 0
    Public WriteOnly Property _CompanyUnkid() As Integer
        Set(ByVal value As Integer)
            mintCompanyUnkid = value
        End Set
    End Property

    Private mdtAuditDate As DateTime = Now
    Public WriteOnly Property _AuditDate() As DateTime
        Set(ByVal value As DateTime)
            mdtAuditDate = value
        End Set
    End Property


    Private mintLoginEmployeeunkid As Integer = 0
    Public Property _LoginEmployeeunkid() As Integer
        Get
            Return mintLoginEmployeeunkid
        End Get
        Set(ByVal value As Integer)
            mintLoginEmployeeunkid = value
        End Set
    End Property

    Private mintVoidlogingemployeeunkid As Integer = 0
    Public Property _Voidloginemployeeunkid() As Integer
        Get
            Return mintVoidlogingemployeeunkid
        End Get
        Set(ByVal value As Integer)
            mintVoidlogingemployeeunkid = value
        End Set
    End Property
    'S.SANDEEP [28-May-2018] -- END 

    Public ReadOnly Property _Message() As String
        Get
            Return mstrMessage
        End Get
    End Property

    ''' <summary>
    ''' Purpose: Get or Set tranheadformulaslabtranunkid
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Tranheadformulaslabtranunkid() As Integer
        Get
            Return mintTranheadformulaslabtranunkid
        End Get
        Set(ByVal value As Integer)
            mintTranheadformulaslabtranunkid = value
            'Call getData()
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set tranheadunkid
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Tranheadunkid() As Integer
        Get
            Return mintTranheadunkid
        End Get
        Set(ByVal value As Integer)
            mintTranheadunkid = value
            Call GetData()
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set periodunkid
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Periodunkid() As Integer
        Get
            Return mintPeriodunkid
        End Get
        Set(ByVal value As Integer)
            mintPeriodunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set formula
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Formula() As String
        Get
            Return mstrFormula
        End Get
        Set(ByVal value As String)
            mstrFormula = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set formulaid
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Formulaid() As String
        Get
            Return mstrFormulaid
        End Get
        Set(ByVal value As String)
            mstrFormulaid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set userunkid
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Userunkid() As Integer
        Get
            Return mintUserunkid
        End Get
        Set(ByVal value As Integer)
            mintUserunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isvoid
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Isvoid() As Boolean
        Get
            Return mblnIsvoid
        End Get
        Set(ByVal value As Boolean)
            mblnIsvoid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiduserunkid
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Voiduserunkid() As Integer
        Get
            Return mintVoiduserunkid
        End Get
        Set(ByVal value As Integer)
            mintVoiduserunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiddatetime
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Voiddatetime() As Date
        Get
            Return mdtVoiddatetime
        End Get
        Set(ByVal value As Date)
            mdtVoiddatetime = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voidreason
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Voidreason() As String
        Get
            Return mstrVoidreason
        End Get
        Set(ByVal value As String)
            mstrVoidreason = value
        End Set
    End Property

    Public Property _Datasource() As DataTable
        Get
            Return mdtFormula
        End Get
        Set(ByVal value As DataTable)
            mdtFormula = value
        End Set
    End Property

    'Sohail (28 Jan 2019) -- Start
    'NMB Enhancement - 76.1 - Option "Prorate Flat Rate head" on Transaction Head for Recurrent Head.
    Private xDataOpr As clsDataOperation = Nothing
    Public WriteOnly Property _DataOperation() As clsDataOperation
        Set(ByVal value As clsDataOperation)
            xDataOpr = value
        End Set
    End Property
    'Sohail (28 Jan 2019) -- End

#End Region

    ''' <summary>
    ''' Modify By: Sohail
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Sub GetData()
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        'Sohail (28 Jan 2019) -- Start
        'NMB Enhancement - 76.1 - Option "Prorate Flat Rate head" on Transaction Head for Recurrent Head.
        'objDataOperation = New clsDataOperation
        If xDataOpr Is Nothing Then
            objDataOperation = New clsDataOperation
        Else
            objDataOperation = xDataOpr
        End If
        objDataOperation.ClearParameters()
        'Sohail (28 Jan 2019) -- End

        Try
            strQ = "SELECT " & _
              "  tranheadformulaslabtranunkid " & _
              ", tranheadunkid " & _
              ", prtranhead_formula_slab_tran.periodunkid " & _
              ", CONVERT(CHAR(8), cfcommon_period_tran.end_date, 112) AS end_date " & _
              ", formula " & _
              ", formulaid " & _
              ", prtranhead_formula_slab_tran.userunkid " & _
              ", isvoid " & _
              ", voiduserunkid " & _
              ", voiddatetime " & _
              ", voidreason " & _
              ", '' AS AUD " & _
             "FROM prtranhead_formula_slab_tran " & _
             "LEFT JOIN cfcommon_period_tran ON cfcommon_period_tran.periodunkid = prtranhead_formula_slab_tran.periodunkid " & _
             "WHERE tranheadunkid = @tranheadunkid " & _
             "AND ISNULL(isvoid, 0) = 0 "

            objDataOperation.AddParameter("@tranheadunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintTranheadunkid.ToString)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            mdtFormula.Rows.Clear()
            Dim dRow As DataRow

            For Each dtRow As DataRow In dsList.Tables(0).Rows
                dRow = mdtFormula.NewRow

                dRow.Item("tranheadformulaslabtranunkid") = CInt(dtRow.Item("tranheadformulaslabtranunkid"))
                dRow.Item("tranheadunkid") = CInt(dtRow.Item("tranheadunkid"))
                dRow.Item("periodunkid") = CInt(dtRow.Item("periodunkid"))
                dRow.Item("end_date") = dtRow.Item("end_date").ToString
                dRow.Item("formula") = dtRow.Item("formula").ToString
                dRow.Item("formulaid") = dtRow.Item("formulaid").ToString
                dRow.Item("isvoid") = CBool(dtRow.Item("isvoid"))
                dRow.Item("voiduserunkid") = CInt(dtRow.Item("voiduserunkid"))
                If IsDBNull(dtRow.Item("voiddatetime")) = True Then
                    mdtVoiddatetime = Nothing
                Else
                    mdtVoiddatetime = dtRow.Item("voiddatetime").ToString
                End If
                dRow.Item("voidreason") = dtRow.Item("voidreason").ToString
                dRow.Item("AUD") = dtRow.Item("AUD").ToString

                mdtFormula.Rows.Add(dRow)
            Next
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetData; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            'Sohail (28 Jan 2019) -- Start
            'NMB Enhancement - 76.1 - Option "Prorate Flat Rate head" on Transaction Head for Recurrent Head.
            'objDataOperation = Nothing
            If xDataOpr Is Nothing Then objDataOperation = Nothing
            'Sohail (28 Jan 2019) -- End
        End Try
    End Sub


    ''' <summary>
    ''' Modify By: Sohail
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function GetList(ByVal strTableName As String, Optional ByVal intTranheadUnkId As Integer = 0, Optional ByVal dtPeriodEndDate As Date = Nothing, Optional ByVal xDataOp As clsDataOperation = Nothing) As DataSet
        'Sohail (03 May 2018) - [xDataOp]
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        'Sohail (03 May 2018) -- Start
        'Enhancement : Removing global objects and passing objDataOperation in process payroll in 72.1.
        'objDataOperation = New clsDataOperation
        If xDataOp Is Nothing Then
            objDataOperation = New clsDataOperation
        Else
            objDataOperation = xDataOp
        End If
        objDataOperation.ClearParameters()
        'Sohail (03 May 2018) -- End

        Try

            strQ = "SELECT  a.tranheadformulaslabtranunkid " & _
                          ", a.tranheadunkid " & _
                          ", a.periodunkid " & _
                          ", a.period_name " & _
                          ", a.formula " & _
                          ", a.formulaid " & _
                          ", a.userunkid " & _
                          ", a.isvoid " & _
                          ", a.voiduserunkid " & _
                          ", a.voiddatetime " & _
                          ", a.voidreason " & _
                          ", a.ROWNO " & _
                    "FROM    ( SELECT    tranheadformulaslabtranunkid " & _
                                      ", tranheadunkid " & _
                                      ", prtranhead_formula_slab_tran.periodunkid " & _
                                      ", ISNULL(cfcommon_period_tran.period_name, '') AS period_name " & _
                                      ", formula " & _
                                      ", formulaid " & _
                                      ", prtranhead_formula_slab_tran.userunkid " & _
                                      ", isvoid " & _
                                      ", voiduserunkid " & _
                                      ", voiddatetime " & _
                                      ", voidreason " & _
                                      ", DENSE_RANK() OVER ( PARTITION  BY prtranhead_formula_slab_tran.tranheadunkid ORDER BY cfcommon_period_tran.end_date DESC ) AS ROWNO " & _
                              "FROM      prtranhead_formula_slab_tran " & _
                                        "LEFT JOIN cfcommon_period_tran ON cfcommon_period_tran.periodunkid = prtranhead_formula_slab_tran.periodunkid " & _
                                                                          "AND cfcommon_period_tran.modulerefid = " & enModuleReference.Payroll & " " & _
                              "WHERE     ISNULL(prtranhead_formula_slab_tran.isvoid, 0) = 0 "

            If intTranheadUnkId > 0 Then
                strQ &= " AND tranheadunkid = @tranheadunkid "
                objDataOperation.AddParameter("@tranheadunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intTranheadUnkId)
            End If

            If dtPeriodEndDate <> Nothing Then
                strQ &= "AND CONVERT(CHAR(8), end_date, 112) <= @end_date "
                objDataOperation.AddParameter("@end_date", SqlDbType.VarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(dtPeriodEndDate))
            End If

            strQ &= ") AS a " & _
                    "WHERE 1 = 1 "

            If dtPeriodEndDate <> Nothing Then
                strQ &= "AND   a.ROWNO = 1 "
            End If


            dsList = objDataOperation.ExecQuery(strQ, strTableName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            'Sohail (03 May 2018) -- Start
            'Enhancement : Removing global objects and passing objDataOperation in process payroll in 72.1.
            'objDataOperation = Nothing
            If xDataOp Is Nothing Then objDataOperation = Nothing
            'Sohail (03 May 2018) -- End
        End Try
        Return dsList
    End Function


    Public Function InserUpdateDelete(ByVal intUserunkid As Integer, ByVal dtCurrentDateAndTime As Date) As Boolean
        'Sohail (21 Aug 2015) - [intUserunkid, dtCurrentDateAndTime]

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        'Sohail (28 Jan 2019) -- Start
        'NMB Enhancement - 76.1 - Option "Prorate Flat Rate head" on Transaction Head for Recurrent Head.
        'objDataOperation = New clsDataOperation
        If xDataOpr Is Nothing Then
            objDataOperation = New clsDataOperation
        Else
            objDataOperation = xDataOpr
        End If
        objDataOperation.ClearParameters()
        'Sohail (28 Jan 2019) -- End

        Try
            For i = 0 To mdtFormula.Rows.Count - 1
                With mdtFormula.Rows(i)
                    objDataOperation.ClearParameters()
                    If Not IsDBNull(.Item("AUD")) Then
                        Select Case .Item("AUD")
                            Case "A"

                                strQ = "INSERT INTO prtranhead_formula_slab_tran ( " & _
                                         "  tranheadunkid " & _
                                         ", periodunkid " & _
                                         ", formula " & _
                                         ", formulaid " & _
                                         ", userunkid " & _
                                         ", isvoid " & _
                                         ", voiduserunkid " & _
                                         ", voiddatetime " & _
                                         ", voidreason" & _
                                       ") VALUES (" & _
                                         "  @tranheadunkid " & _
                                         ", @periodunkid " & _
                                         ", @formula " & _
                                         ", @formulaid " & _
                                         ", @userunkid " & _
                                         ", @isvoid " & _
                                         ", @voiduserunkid " & _
                                         ", @voiddatetime " & _
                                         ", @voidreason" & _
                                       "); SELECT @@identity"

                                objDataOperation.AddParameter("@tranheadunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintTranheadunkid.ToString)
                                objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("periodunkid").ToString)
                                objDataOperation.AddParameter("@formula", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, .Item("formula").ToString)
                                objDataOperation.AddParameter("@formulaid", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, .Item("formulaid").ToString)
                                objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("userunkid").ToString)
                                objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, CBool(.Item("isvoid")))
                                objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("voiduserunkid").ToString)
                                If .Item("voiddatetime").ToString = Nothing Then
                                    objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
                                Else
                                    objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, .Item("voiddatetime").ToString)
                                End If
                                objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, .Item("voidreason").ToString)


                                dsList = objDataOperation.ExecQuery(strQ, "List")

                                If objDataOperation.ErrorMessage <> "" Then
                                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                    Throw exForce
                                End If

                                mintTranheadformulaslabtranunkid = dsList.Tables(0).Rows(0)(0)
                                If .Item("tranheadunkid") > 0 Then
                                    'S.SANDEEP [28-May-2018] -- START
                                    'ISSUE/ENHANCEMENT : {Audit Trails} 
                                    Dim objCommonATLog As New clsCommonATLog
                                    objCommonATLog._FormName = mstrFormName
                                    objCommonATLog._LoginEmployeeUnkid = mintLoginEmployeeunkid
                                    objCommonATLog._ClientIP = mstrClientIP
                                    objCommonATLog._HostName = mstrHostName
                                    objCommonATLog._FromWeb = mblnIsWeb
                                    objCommonATLog._AuditUserId = mintAuditUserId
                                    objCommonATLog._CompanyUnkid = mintCompanyUnkid
                                    objCommonATLog._AuditDate = mdtAuditDate
                                    'S.SANDEEP [28-May-2018] -- END

                                    If objCommonATLog.Insert_TranAtLog(objDataOperation, "prtranhead_master", "tranheadunkid", mintTranheadunkid, "prtranhead_formula_slab_tran", "tranheadformulaslabtranunkid", mintTranheadformulaslabtranunkid, 2, 1) = False Then
                                        Return False
                                    End If

                                    'S.SANDEEP [28-May-2018] -- START
                                    'ISSUE/ENHANCEMENT : {Audit Trails} 
                                    objCommonATLog = Nothing
                                    'S.SANDEEP [28-May-2018] -- END

                                Else
                                    'S.SANDEEP [28-May-2018] -- START
                                    'ISSUE/ENHANCEMENT : {Audit Trails} 
                                    Dim objCommonATLog As New clsCommonATLog
                                    objCommonATLog._FormName = mstrFormName
                                    objCommonATLog._LoginEmployeeUnkid = mintLoginEmployeeunkid
                                    objCommonATLog._ClientIP = mstrClientIP
                                    objCommonATLog._HostName = mstrHostName
                                    objCommonATLog._FromWeb = mblnIsWeb
                                    objCommonATLog._AuditUserId = mintAuditUserId
                                    objCommonATLog._CompanyUnkid = mintCompanyUnkid
                                    objCommonATLog._AuditDate = mdtAuditDate
                                    'S.SANDEEP [28-May-2018] -- END

                                    If objCommonATLog.Insert_TranAtLog(objDataOperation, "prtranhead_master", "tranheadunkid", mintTranheadunkid, "prtranhead_formula_slab_tran", "tranheadformulaslabtranunkid", mintTranheadformulaslabtranunkid, 1, 1) = False Then
                                        Return False
                                    End If

                                    'S.SANDEEP [28-May-2018] -- START
                                    'ISSUE/ENHANCEMENT : {Audit Trails} 
                                    objCommonATLog = Nothing
                                    'S.SANDEEP [28-May-2018] -- END

                                End If

                            Case "U"

                                strQ = "UPDATE prtranhead_formula_slab_tran SET " & _
                                         "  tranheadunkid = @tranheadunkid" & _
                                         ", periodunkid = @periodunkid" & _
                                         ", formula = @formula" & _
                                         ", formulaid = @formulaid" & _
                                         ", userunkid = @userunkid" & _
                                         ", isvoid = @isvoid" & _
                                         ", voiduserunkid = @voiduserunkid" & _
                                         ", voiddatetime = @voiddatetime" & _
                                         ", voidreason = @voidreason " & _
                                       "WHERE tranheadformulaslabtranunkid = @tranheadformulaslabtranunkid "

                                objDataOperation.AddParameter("@tranheadformulaslabtranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("tranheadformulaslabtranunkid").ToString)
                                objDataOperation.AddParameter("@tranheadunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("tranheadunkid").ToString)
                                objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("periodunkid").ToString)
                                objDataOperation.AddParameter("@formula", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, .Item("formula").ToString)
                                objDataOperation.AddParameter("@formulaid", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, .Item("formulaid").ToString)
                                objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("userunkid").ToString)
                                objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, CBool(.Item("isvoid")))
                                objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("voiduserunkid").ToString)
                                If .Item("voiddatetime").ToString = Nothing Then
                                    objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
                                Else
                                    objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, .Item("voiddatetime").ToString)
                                End If
                                objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, .Item("voidreason").ToString)

                                dsList = objDataOperation.ExecQuery(strQ, "List")

                                If objDataOperation.ErrorMessage <> "" Then
                                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                    Throw exForce
                                End If

                                'S.SANDEEP [28-May-2018] -- START
                                'ISSUE/ENHANCEMENT : {Audit Trails} 
                                Dim objCommonATLog As New clsCommonATLog
                                objCommonATLog._FormName = mstrFormName
                                objCommonATLog._LoginEmployeeUnkid = mintLoginEmployeeunkid
                                objCommonATLog._ClientIP = mstrClientIP
                                objCommonATLog._HostName = mstrHostName
                                objCommonATLog._FromWeb = mblnIsWeb
                                objCommonATLog._AuditUserId = mintAuditUserId
                                objCommonATLog._CompanyUnkid = mintCompanyUnkid
                                objCommonATLog._AuditDate = mdtAuditDate
                                'S.SANDEEP [28-May-2018] -- END

                                If objCommonATLog.Insert_TranAtLog(objDataOperation, "prtranhead_master", "tranheadunkid", .Item("tranheadunkid").ToString, "prtranhead_formula_slab_tran", "tranheadformulaslabtranunkid", .Item("tranheadformulaslabtranunkid").ToString, 2, 2) = False Then
                                    Return False
                                End If

                                'S.SANDEEP [28-May-2018] -- START
                                'ISSUE/ENHANCEMENT : {Audit Trails} 
                                objCommonATLog = Nothing
                                'S.SANDEEP [28-May-2018] -- END


                            Case "D"

                                If .Item("tranheadunkid") > 0 Then
                                    'S.SANDEEP [28-May-2018] -- START
                                    'ISSUE/ENHANCEMENT : {Audit Trails} 
                                    Dim objCommonATLog As New clsCommonATLog
                                    objCommonATLog._FormName = mstrFormName
                                    objCommonATLog._LoginEmployeeUnkid = mintLoginEmployeeunkid
                                    objCommonATLog._ClientIP = mstrClientIP
                                    objCommonATLog._HostName = mstrHostName
                                    objCommonATLog._FromWeb = mblnIsWeb
                                    objCommonATLog._AuditUserId = mintAuditUserId
                                    objCommonATLog._CompanyUnkid = mintCompanyUnkid
                                    objCommonATLog._AuditDate = mdtAuditDate
                                    'S.SANDEEP [28-May-2018] -- END

                                    If objCommonATLog.Insert_TranAtLog(objDataOperation, "prtranhead_master", "tranheadunkid", mintTranheadunkid, "prtranhead_formula_slab_tran", "tranheadformulaslabtranunkid", .Item("tranheadformulaslabtranunkid").ToString, 2, 3) = False Then
                                        Return False
                                    End If

                                    'S.SANDEEP [28-May-2018] -- START
                                    'ISSUE/ENHANCEMENT : {Audit Trails} 
                                    objCommonATLog = Nothing
                                    'S.SANDEEP [28-May-2018] -- END



                                    strQ = "UPDATE prtranhead_formula_slab_tran SET " & _
                                            " isvoid = 1 " & _
                                            ", voiduserunkid = @voiduserunkid " & _
                                            ", voiddatetime = @voiddatetime " & _
                                            ", voidreason = @voidreason " & _
                                        "WHERE tranheadformulaslabtranunkid = @tranheadformulaslabtranunkid " & _
                                        "AND isvoid = 0 "

                                    objDataOperation.AddParameter("@tranheadformulaslabtranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("tranheadformulaslabtranunkid").ToString)
                                    'Sohail (21 Aug 2015) -- Start
                                    'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                                    'objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, User._Object._Userunkid)
                                    'objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, ConfigParameter._Object._CurrentDateAndTime)
                                    objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUserunkid)
                                    objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, dtCurrentDateAndTime)
                                    'Sohail (21 Aug 2015) -- End
                                    objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, .Item("voidreason").ToString)

                                    dsList = objDataOperation.ExecQuery(strQ, "List")

                                    If objDataOperation.ErrorMessage <> "" Then
                                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                        Throw exForce
                                    End If
                                End If

                        End Select
                    End If
                End With
            Next

            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: InserUpdateDelete; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            'Sohail (28 Jan 2019) -- Start
            'NMB Enhancement - 76.1 - Option "Prorate Flat Rate head" on Transaction Head for Recurrent Head.
            'objDataOperation = Nothing
            If xDataOpr Is Nothing Then objDataOperation = Nothing
            'Sohail (28 Jan 2019) -- End
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sohail
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> INSERT INTO Database Table (prtranhead_formula_slab_tran) </purpose>
    Public Function Insert() As Boolean
        'If isExist(mstrName) Then
        '    mstrMessage = "<Message>"
        '    Return False
        'End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        'Sohail (28 Jan 2019) -- Start
        'NMB Enhancement - 76.1 - Option "Prorate Flat Rate head" on Transaction Head for Recurrent Head.
        'objDataOperation = New clsDataOperation
        If xDataOpr Is Nothing Then
            objDataOperation = New clsDataOperation
        Else
            objDataOperation = xDataOpr
        End If
        objDataOperation.ClearParameters()
        'Sohail (28 Jan 2019) -- End

        Try
            objDataOperation.AddParameter("@tranheadunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintTranheadunkid.ToString)
            objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPeriodunkid.ToString)
            objDataOperation.AddParameter("@formula", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, mstrFormula.ToString)
            objDataOperation.AddParameter("@formulaid", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, mstrFormulaid.ToString)
            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
            objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime.ToString)
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)

            strQ = "INSERT INTO prtranhead_formula_slab_tran ( " & _
              "  tranheadunkid " & _
              ", periodunkid " & _
              ", formula " & _
              ", formulaid " & _
              ", userunkid " & _
              ", isvoid " & _
              ", voiduserunkid " & _
              ", voiddatetime " & _
              ", voidreason" & _
            ") VALUES (" & _
              "  @tranheadunkid " & _
              ", @periodunkid " & _
              ", @formula " & _
              ", @formulaid " & _
              ", @userunkid " & _
              ", @isvoid " & _
              ", @voiduserunkid " & _
              ", @voiddatetime " & _
              ", @voidreason" & _
            "); SELECT @@identity"

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            mintTranheadformulaslabtranunkid = dsList.Tables(0).Rows(0).Item(0)

            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Insert; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            'Sohail (28 Jan 2019) -- Start
            'NMB Enhancement - 76.1 - Option "Prorate Flat Rate head" on Transaction Head for Recurrent Head.
            'objDataOperation = Nothing
            If xDataOpr Is Nothing Then objDataOperation = Nothing
            'Sohail (28 Jan 2019) -- End
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sohail
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Update Database Table (prtranhead_formula_slab_tran) </purpose>
    Public Function Update() As Boolean
        'If isExist(mstrName, mintTranheadformulaslabtranunkid) Then
        '    mstrMessage = "<Message>"
        '    Return False
        'End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        'Sohail (28 Jan 2019) -- Start
        'NMB Enhancement - 76.1 - Option "Prorate Flat Rate head" on Transaction Head for Recurrent Head.
        'objDataOperation = New clsDataOperation
        If xDataOpr Is Nothing Then
            objDataOperation = New clsDataOperation
        Else
            objDataOperation = xDataOpr
        End If
        objDataOperation.ClearParameters()
        'Sohail (28 Jan 2019) -- End

        Try
            objDataOperation.AddParameter("@tranheadformulaslabtranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintTranheadformulaslabtranunkid.ToString)
            objDataOperation.AddParameter("@tranheadunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintTranheadunkid.ToString)
            objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPeriodunkid.ToString)
            objDataOperation.AddParameter("@formula", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, mstrFormula.ToString)
            objDataOperation.AddParameter("@formulaid", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, mstrFormulaid.ToString)
            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
            objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime.ToString)
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)

            strQ = "UPDATE prtranhead_formula_slab_tran SET " & _
              "  tranheadunkid = @tranheadunkid" & _
              ", periodunkid = @periodunkid" & _
              ", formula = @formula" & _
              ", formulaid = @formulaid" & _
              ", userunkid = @userunkid" & _
              ", isvoid = @isvoid" & _
              ", voiduserunkid = @voiduserunkid" & _
              ", voiddatetime = @voiddatetime" & _
              ", voidreason = @voidreason " & _
            "WHERE tranheadformulaslabtranunkid = @tranheadformulaslabtranunkid "

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            'Sohail (28 Jan 2019) -- Start
            'NMB Enhancement - 76.1 - Option "Prorate Flat Rate head" on Transaction Head for Recurrent Head.
            'objDataOperation = Nothing
            If xDataOpr Is Nothing Then objDataOperation = Nothing
            'Sohail (28 Jan 2019) -- End
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sohail
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Delete Database Table (prtranhead_formula_slab_tran) </purpose>
    Public Function Delete(ByVal intUnkid As Integer) As Boolean
        If isUsed(intUnkid) Then
            mstrMessage = "<Message>"
            Return False
        End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        'Sohail (28 Jan 2019) -- Start
        'NMB Enhancement - 76.1 - Option "Prorate Flat Rate head" on Transaction Head for Recurrent Head.
        'objDataOperation = New clsDataOperation
        If xDataOpr Is Nothing Then
            objDataOperation = New clsDataOperation
        Else
            objDataOperation = xDataOpr
        End If
        objDataOperation.ClearParameters()
        'Sohail (28 Jan 2019) -- End

        Try
            strQ = "DELETE FROM prtranhead_formula_slab_tran " & _
            "WHERE tranheadformulaslabtranunkid = @tranheadformulaslabtranunkid "

            objDataOperation.AddParameter("@tranheadformulaslabtranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            'Sohail (28 Jan 2019) -- Start
            'NMB Enhancement - 76.1 - Option "Prorate Flat Rate head" on Transaction Head for Recurrent Head.
            'objDataOperation = Nothing
            If xDataOpr Is Nothing Then objDataOperation = Nothing
            'Sohail (28 Jan 2019) -- End
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sohail
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isUsed(ByVal intUnkid As Integer) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "<Query>"

            objDataOperation.AddParameter("@tranheadformulaslabtranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isUsed; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sohail
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isExist(ByVal strCode As String, ByVal strName As String, Optional ByVal intUnkid As Integer = -1) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        'Sohail (28 Jan 2019) -- Start
        'NMB Enhancement - 76.1 - Option "Prorate Flat Rate head" on Transaction Head for Recurrent Head.
        'objDataOperation = New clsDataOperation
        If xDataOpr Is Nothing Then
            objDataOperation = New clsDataOperation
        Else
            objDataOperation = xDataOpr
        End If
        objDataOperation.ClearParameters()
        'Sohail (28 Jan 2019) -- End

        Try
            strQ = "SELECT " & _
              "  tranheadformulaslabtranunkid " & _
              ", tranheadunkid " & _
              ", periodunkid " & _
              ", formula " & _
              ", formulaid " & _
              ", userunkid " & _
              ", isvoid " & _
              ", voiduserunkid " & _
              ", voiddatetime " & _
              ", voidreason " & _
             "FROM prtranhead_formula_slab_tran " & _
             "WHERE name = @name " & _
             "AND code = @code "

            If intUnkid > 0 Then
                strQ &= " AND tranheadformulaslabtranunkid <> @tranheadformulaslabtranunkid"
            End If

            objDataOperation.AddParameter("@code", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strCode)
            objDataOperation.AddParameter("@name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strName)
            objDataOperation.AddParameter("@tranheadformulaslabtranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isExist; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            'Sohail (28 Jan 2019) -- Start
            'NMB Enhancement - 76.1 - Option "Prorate Flat Rate head" on Transaction Head for Recurrent Head.
            'objDataOperation = Nothing
            If xDataOpr Is Nothing Then objDataOperation = Nothing
            'Sohail (28 Jan 2019) -- End
        End Try
    End Function

End Class