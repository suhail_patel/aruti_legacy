﻿'************************************************************************************************************************************
'Class Name : clsPayment_tran.vb
'Purpose    :
'Date       :12/07/2010
'Written By :Sandeep J. Sharma
'Modified   :
'************************************************************************************************************************************

Imports eZeeCommonLib
'S.SANDEEP [ 11 AUG 2012 ] -- START
'ENHANCEMENT : TRA CHANGES
Imports eZee.Common.eZeeForm
'S.SANDEEP [ 11 AUG 2012 ] -- END
Imports System.Threading 'Sohail (10 Mar 2014)
Imports System.ComponentModel
Imports System.Web

''' <summary>
''' Purpose: 
''' Developer: Sandeep J. Sharma
''' </summary>
Public Class clsPayment_tran
    Private Shared ReadOnly mstrModuleName As String = "clsPayment_tran"
    Dim objDataOperation As clsDataOperation
    Dim objSavingStatus As New clsSaving_Status_Tran
    Dim objEmployeeSaving As New clsSaving_Tran
    Dim mstrMessage As String = ""
    'Sandeep [ 17 DEC 2010 ] -- Start
    Private objCashDenome As New clsCashDenomination
    'Sandeep [ 17 DEC 2010 ] -- End 

    'Sohail (10 Mar 2014) -- Start
    'Enhancement - Send Emails in Application Backgropund.
    'Private gobjEmailList As New List(Of clsEmailCollection)
    Private trd As Thread
    'Sohail (10 Mar 2014) -- End

#Region " Enum "
    Public Enum enPaymentRefId
        LOAN = 1
        ADVANCE = 2
        PAYSLIP = 3
        SAVINGS = 4
    End Enum

    Public Enum enPayTypeId
        PAYMENT = 1
        RECEIVED = 2
        WITHDRAWAL = 3
        REPAYMENT = 4
        'Sohail (21 Nov 2015) -- Start
        'Enhancement - Provide Deposit feaure in Employee Saving.
        DEPOSIT = 5
        'Sohail (21 Nov 2015) -- End
    End Enum
#End Region

#Region " Loan/Advance Region "
    Private objLoan_Advance As New clsLoan_Advance
    Private objLoanStatusTran As New clsLoan_Status_tran
    Private mblnIsFromStatus As Boolean = False
    Private mstrStatus_Data As String = ""

    Public WriteOnly Property _IsFromStatus() As Boolean
        Set(ByVal value As Boolean)
            mblnIsFromStatus = value
        End Set
    End Property

    Public WriteOnly Property _strData() As String
        Set(ByVal value As String)
            mstrStatus_Data = value
        End Set
    End Property

#End Region

#Region " Private variables "
    Private mintPaymenttranunkid As Integer
    Private mstrVoucherno As String = String.Empty
    Private mintPeriodunkid As Integer
    Private mdtPaymentdate As Date
    Private mintEmployeeunkid As Integer
    Private mintPaymentrefid As Integer
    Private mintPaymentmodeid As Integer
    Private mintBranchunkid As Integer
    Private mstrChequeno As String = String.Empty
    Private mintPaymentbyid As Integer
    Private mdblPercentage As Double
    Private mdecAmount As Decimal 'Sohail (11 May 2011)
    Private mstrVoucherref As String = String.Empty
    Private mintPayreftranunkid As Integer
    Private mintUserunkid As Integer
    Private mblnIsvoid As Boolean
    Private mintVoiduserunkid As Integer
    Private mdtVoiddatetime As Date
    Private mintPaymentTypeId As Integer
    Private mblnIsReceipt As Boolean = False
    Private mstrVoidreason As String = String.Empty
    Private mdecOldAmount As Decimal = 0 'Sohail (11 May 2011)
    Private mblIsInsertStatus As Boolean = False
    'Sohail (16 Oct 2010) -- Start
    Private mblnIsglobalpayment As Boolean
    'Sohail (16 Oct 2010) -- End

    'Sandeep [ 17 DEC 2010 ] -- Start
    Private mdtTranTable As DataTable
    'Sandeep [ 17 DEC 2010 ] -- End 
    'Sohail (08 Oct 2011) -- Start
    Private mintBasecurrencyid As Integer
    Private mdecBaseexchangerate As Decimal
    Private mintPaidcurrencyid As Integer
    Private mdecExpaidrate As Decimal
    Private mdecExpaidamt As Decimal
    'Sohail (08 Oct 2011) -- End

    'S.SANDEEP [ 20 APRIL 2012 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Private mstrGlobalvocno As String = String.Empty
    'S.SANDEEP [ 20 APRIL 2012 ] -- END

    Private mblnIsauthorized As Boolean = False 'Sohail (02 Jul 2012)
    Private mstrAccountno As String = String.Empty 'Sohail (21 Jul 2012)
    Private mintCountryunkid As Integer 'Sohail (03 Sep 2012)
    Private mblnIsapproved As Boolean = False 'Sohail (13 Feb 2013)
   'S.SANDEEP [ 19 JULY 2012 ] -- START
    'Enhancement : TRA Changes
    Private mstrWebFormName As String = String.Empty
    'S.SANDEEP [ 19 JULY 2012 ] -- END

    'S.SANDEEP [ 13 AUG 2012 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Private mintLogEmployeeUnkid As Integer = -1
    'S.SANDEEP [ 13 AUG 2012 ] -- END

    'Sohail (22 Oct 2013) -- Start
    'TRA - ENHANCEMENT
    Private mintRoundingtypeid As Integer
    Private mintRoundingmultipleid As Integer
    'Sohail (22 Oct 2013) -- End


    'Sohail (18 May 2013) -- Start
    'TRA - ENHANCEMENT
    Private mintPaymentVocNoType As Integer = -1
    Private mintCompanyunkid As Integer = -1
    Private mintYearUnkid As Integer = -1
    Private mstrAccessLevelFilterString As String = ""
    'Sohail (18 May 2013) -- End

    'Pinkal (24-May-2013) -- Start
    'Enhancement : TRA Changes
    Private mstrWebIP As String = ""
    Private mstrWebhostName As String = ""
    'Pinkal (24-May-2013) -- End

    Private mdecRoundingAdjustment As Decimal = 0 'Sohail (21 Mar 2014)

'S.SANDEEP [ 28 JAN 2014 ] -- START
    Private mstrWebFrmName As String = String.Empty
    'S.SANDEEP [ 28 JAN 2014 ] -- END

    Private mstrRemarks As String = "" 'Sohail (24 Dec 2014)

    'Sohail (07 May 2015) -- Start
    'Enhancement - New Loan Redesign with allow to change interest rate, tenure, topup, receipt.
    Private mintPaymentDate_Periodunkid As Integer
    Private mdecOldAmountPaidCurrency As Decimal = 0
    'Sohail (07 May 2015) -- End

#End Region

#Region " Properties "

    'S.SANDEEP [28-May-2018] -- START 
    'ISSUE/ENHANCEMENT : {Audit Trails} 
    Private mstrFormName As String = String.Empty
    Public WriteOnly Property _FormName() As String
        Set(ByVal value As String)
            mstrFormName = value
        End Set
    End Property

    Private mstrClientIP As String = ""
    Public WriteOnly Property _ClientIP() As String
        Set(ByVal value As String)
            mstrClientIP = value
        End Set
    End Property

    Private mstrHostName As String = ""
    Public WriteOnly Property _HostName() As String
        Set(ByVal value As String)
            mstrHostName = value
        End Set
    End Property

    Private mblnIsWeb As Boolean = False
    Public WriteOnly Property _FromWeb() As Boolean
        Set(ByVal value As Boolean)
            mblnIsWeb = value
        End Set
    End Property

    Private mintAuditUserId As Integer = 0
    Public WriteOnly Property _AuditUserId() As Integer
        Set(ByVal value As Integer)
            mintAuditUserId = value
        End Set
    End Property

    Private mdtAuditDate As DateTime = Now
    Public WriteOnly Property _AuditDate() As DateTime
        Set(ByVal value As DateTime)
            mdtAuditDate = value
        End Set
    End Property

    Private xDataOpr As clsDataOperation = Nothing
    Public Property _xDataOpr() As clsDataOperation
        Get
            Return xDataOpr
        End Get
        Set(ByVal value As clsDataOperation)
            xDataOpr = value
        End Set
    End Property

    Private mintVoidlogingemployeeunkid As Integer = 0
    Public Property _Voidloginemployeeunkid() As Integer
        Get
            Return mintVoidlogingemployeeunkid
        End Get
        Set(ByVal value As Integer)
            mintVoidlogingemployeeunkid = value
        End Set
    End Property
    'S.SANDEEP [28-May-2018] -- END 

    Public ReadOnly Property _Message() As String
        Get
            Return mstrMessage
        End Get
    End Property

    ''' <summary>
    ''' Purpose: Get or Set paymenttranunkid
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Paymenttranunkid() As Integer
        Get
            Return mintPaymenttranunkid
        End Get
        Set(ByVal value As Integer)
            mintPaymenttranunkid = value
            'Sohail (12 May 2012) -- Start
            'TRA - ENHANCEMENT
            'Call GetData()
            Call GetData(objDataOperation)
            'Sohail (12 May 2012) -- End
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voucherno
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Voucherno() As String
        Get
            Return mstrVoucherno
        End Get
        Set(ByVal value As String)
            mstrVoucherno = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set periodunkid
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Periodunkid() As Integer
        Get
            Return mintPeriodunkid
        End Get
        Set(ByVal value As Integer)
            mintPeriodunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set paymentdate
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Paymentdate() As Date
        Get
            Return mdtPaymentdate
        End Get
        Set(ByVal value As Date)
            mdtPaymentdate = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set employeeunkid
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Employeeunkid() As Integer
        Get
            Return mintEmployeeunkid
        End Get
        Set(ByVal value As Integer)
            mintEmployeeunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set referenceid
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Paymentrefid() As Integer
        Get
            Return mintPaymentrefid
        End Get
        Set(ByVal value As Integer)
            mintPaymentrefid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set paymentmode
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Paymentmodeid() As Integer
        Get
            Return mintPaymentmodeid
        End Get
        Set(ByVal value As Integer)
            mintPaymentmodeid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set branchunkid
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Branchunkid() As Integer
        Get
            Return mintBranchunkid
        End Get
        Set(ByVal value As Integer)
            mintBranchunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set chequeno
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Chequeno() As String
        Get
            Return mstrChequeno
        End Get
        Set(ByVal value As String)
            mstrChequeno = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set paymentbyid
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Paymentbyid() As Integer
        Get
            Return mintPaymentbyid
        End Get
        Set(ByVal value As Integer)
            mintPaymentbyid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set percentage
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Percentage() As Double
        Get
            Return mdblPercentage
        End Get
        Set(ByVal value As Double)
            mdblPercentage = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set amount
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Amount() As Decimal 'Sohail (11 May 2011)
        Get
            Return mdecAmount
        End Get
        Set(ByVal value As Decimal) 'Sohail (11 May 2011)
            mdecAmount = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voucherref
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Voucherref() As String
        Get
            Return mstrVoucherref
        End Get
        Set(ByVal value As String)
            mstrVoucherref = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set referencetranunkid
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Payreftranunkid() As Integer
        Get
            Return mintPayreftranunkid
        End Get
        Set(ByVal value As Integer)
            mintPayreftranunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set userunkid
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Userunkid() As Integer
        Get
            Return mintUserunkid
        End Get
        Set(ByVal value As Integer)
            mintUserunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isvoid
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Isvoid() As Boolean
        Get
            Return mblnIsvoid
        End Get
        Set(ByVal value As Boolean)
            mblnIsvoid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiduserunkid
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Voiduserunkid() As Integer
        Get
            Return mintVoiduserunkid
        End Get
        Set(ByVal value As Integer)
            mintVoiduserunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiddatetime
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Voiddatetime() As Date
        Get
            Return mdtVoiddatetime
        End Get
        Set(ByVal value As Date)
            mdtVoiddatetime = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set PaymentTypeId
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _PaymentTypeId() As Integer
        Get
            Return mintPaymentTypeId
        End Get
        Set(ByVal value As Integer)
            mintPaymentTypeId = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set Receipt
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Is_Receipt() As Boolean
        Get
            Return mblnIsReceipt
        End Get
        Set(ByVal value As Boolean)
            mblnIsReceipt = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voidreason
    ''' Modify By: Samdeep J. Sharma
    ''' </summary>
    Public Property _Voidreason() As String
        Get
            Return mstrVoidreason
        End Get
        Set(ByVal value As String)
            mstrVoidreason = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get InsertStatus
    ''' Modify By: Anjan
    ''' </summary>
    Public WriteOnly Property _IsInsertStatus() As Boolean
        Set(ByVal value As Boolean)
            mblIsInsertStatus = value
        End Set
    End Property



    Public Property _Old_Amount() As Decimal 'Sohail (11 May 2011)
        Get
            Return mdecOldAmount
        End Get
        Set(ByVal value As Decimal) 'Sohail (11 May 2011)
            mdecOldAmount = value
        End Set
    End Property

    'Sohail (07 May 2015) -- Start
    'Enhancement - New Loan Redesign with allow to change interest rate, tenure, topup, receipt.
    Public Property _Old_AmountPaidCurrency() As Decimal 'Sohail (11 May 2011)
        Get
            Return mdecOldAmountPaidCurrency
        End Get
        Set(ByVal value As Decimal) 'Sohail (11 May 2011)
            mdecOldAmountPaidCurrency = value
        End Set
    End Property
    'Sohail (07 May 2015) -- End


    'Sohail (16 Oct 2010) -- Start
    ''' <summary>
    ''' Purpose: Get or Set isglobalpayment
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Isglobalpayment() As Boolean
        Get
            Return mblnIsglobalpayment
        End Get
        Set(ByVal value As Boolean)
            mblnIsglobalpayment = Value
        End Set
    End Property
    'Sohail (16 Oct 2010) -- End

    'Sohail (08 Oct 2011) -- Start
    Public Property _Basecurrencyid() As Integer
        Get
            Return mintBasecurrencyid
        End Get
        Set(ByVal value As Integer)
            mintBasecurrencyid = value
        End Set
    End Property

    Public Property _Baseexchangerate() As Decimal
        Get
            Return mdecBaseexchangerate
        End Get
        Set(ByVal value As Decimal)
            mdecBaseexchangerate = value
        End Set
    End Property

    Public Property _Paidcurrencyid() As Integer
        Get
            Return mintPaidcurrencyid
        End Get
        Set(ByVal value As Integer)
            mintPaidcurrencyid = value
        End Set
    End Property

    Public Property _Expaidrate() As Decimal
        Get
            Return mdecExpaidrate
        End Get
        Set(ByVal value As Decimal)
            mdecExpaidrate = value
        End Set
    End Property

    Public Property _Expaidamt() As Decimal
        Get
            Return mdecExpaidamt
        End Get
        Set(ByVal value As Decimal)
            mdecExpaidamt = value
        End Set
    End Property
    'Sohail (08 Oct 2011) -- End

    'Sandeep [ 17 DEC 2010 ] -- Start
    ''' <summary>
    ''' Purpose: Set Denomination DataTable
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public WriteOnly Property _Denom_Table() As DataTable
        Set(ByVal value As DataTable)
            mdtTranTable = value
        End Set
    End Property
    'Sandeep [ 17 DEC 2010 ] -- End 

    'S.SANDEEP [ 20 APRIL 2012 ] -- START
    'ENHANCEMENT : TRA CHANGES
    ''' <summary>
    ''' Purpose: Get or Set globalvocno
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Globalvocno() As String
        Get
            Return mstrGlobalvocno
        End Get
        Set(ByVal value As String)
            mstrGlobalvocno = Value
        End Set
    End Property
    'S.SANDEEP [ 20 APRIL 2012 ] -- END

    'Sohail (12 May 2012) -- Start
    'TRA - ENHANCEMENT
    Public WriteOnly Property _DataOperation() As clsDataOperation
        Set(ByVal value As clsDataOperation)
            objDataOperation = value
        End Set
    End Property
    'Sohail (12 May 2012) -- End

    'Sohail (02 Jul 2012) -- Start
    'TRA - ENHANCEMENT
    Public Property _Isauthorized() As Boolean
        Get
            Return mblnIsauthorized
        End Get
        Set(ByVal value As Boolean)
            mblnIsauthorized = value
        End Set
    End Property
    'Sohail (02 Jul 2012) -- End

 'S.SANDEEP [ 19 JULY 2012 ] -- START
    'Enhancement : TRA Changes

    Public Property _WebFormName() As String
        Get
            Return mstrWebFormName
        End Get
        Set(ByVal value As String)
            mstrWebFormName = value
        End Set
    End Property

    'S.SANDEEP [ 19 JULY 2012 ] -- END

    'Sohail (21 Jul 2012) -- Start
    'TRA - ENHANCEMENT
    Public Property _Accountno() As String
        Get
            Return mstrAccountno
        End Get
        Set(ByVal value As String)
            mstrAccountno = Value
        End Set
    End Property
    'Sohail (21 Jul 2012) -- End

    'Sohail (03 Sep 2012) -- Start
    'TRA - ENHANCEMENT
    Public Property _Countryunkid() As Integer
        Get
            Return mintCountryunkid
        End Get
        Set(ByVal value As Integer)
            mintCountryunkid = value
        End Set
    End Property
    'Sohail (03 Sep 2012) -- End

    'Sohail (13 Feb 2013) -- Start
    'TRA - ENHANCEMENT
    ''' <summary>
    ''' Purpose: Get or Set isapproved
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Isapproved() As Boolean
        Get
            Return mblnIsapproved
        End Get
        Set(ByVal value As Boolean)
            mblnIsapproved = Value
        End Set
    End Property
    'Sohail (13 Feb 2013) -- End

    'S.SANDEEP [ 13 AUG 2012 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Public WriteOnly Property _LoginEmployeeUnkid() As Integer
        Set(ByVal value As Integer)
            mintLogEmployeeUnkid = value
        End Set
    End Property
    'S.SANDEEP [ 13 AUG 2012 ] -- END

    'Sohail (22 Oct 2013) -- Start
    'TRA - ENHANCEMENT
    ''' <summary>
    ''' Purpose: Get or Set roundingtypeid
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Roundingtypeid() As Integer
        Get
            Return mintRoundingtypeid
        End Get
        Set(ByVal value As Integer)
            mintRoundingtypeid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set roundingmultipleid
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Roundingmultipleid() As Integer
        Get
            Return mintRoundingmultipleid
        End Get
        Set(ByVal value As Integer)
            mintRoundingmultipleid = Value
        End Set
    End Property
    'Sohail (22 Oct 2013) -- End


    'Sohail (18 May 2013) -- Start
    'TRA - ENHANCEMENT
    Public WriteOnly Property _PaymentVocNoType() As Integer
        Set(ByVal value As Integer)
            mintPaymentVocNoType = value
        End Set
    End Property

    Public WriteOnly Property _Companyunkid() As Integer
        Set(ByVal value As Integer)
            mintCompanyunkid = value
        End Set
    End Property

    Public WriteOnly Property _YearUnkid() As Integer
        Set(ByVal value As Integer)
            mintYearUnkid = value
        End Set
    End Property

    Public WriteOnly Property _AccessLevelFilterString() As String
        Set(ByVal value As String)
            mstrAccessLevelFilterString = value
        End Set
    End Property
    'Sohail (18 May 2013) -- End


    'Public WriteOnly Property _WebIP() As String
    '    Set(ByVal value As String)
    '        mstrClientIP = value
    '    End Set
    'End Property

    'Public WriteOnly Property _WebHostName() As String
    '    Set(ByVal value As String)
    '        mstrHostName = value
    '    End Set
    'End Property

    'Sohail (21 Mar 2014) -- Start
    'Enhancement - Set Limit for C/F Netpay balance and Payment JV Report
    Public Property _RoundingAdjustment() As Decimal
        Get
            Return mdecRoundingAdjustment
        End Get
        Set(ByVal value As Decimal)
            mdecRoundingAdjustment = value
        End Set
    End Property
    'Sohail (21 Mar 2014) -- End

    'Public WriteOnly Property _WebFrmName() As String
    '    Set(ByVal value As String)
    '        mstrWebFrmName = value
    '    End Set
    'End Property

    'Sohail (24 Dec 2014) -- Start
    'Enhancement - Provide Remark option on Payslip Payment and Global Payslip Payment.
    ''' <summary>
    ''' Purpose: Get or Set remark
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Remarks() As String
        Get
            Return mstrRemarks
        End Get
        Set(ByVal value As String)
            mstrRemarks = value
        End Set
    End Property
    'Sohail (24 Dec 2014) -- End

    'Sohail (07 May 2015) -- Start
    'Enhancement - New Loan Redesign with allow to change interest rate, tenure, topup, receipt.
    Public Property _PaymentDate_Periodunkid() As Integer
        Get
            Return mintPaymentDate_Periodunkid
        End Get
        Set(ByVal value As Integer)
            mintPaymentDate_Periodunkid = value
        End Set
    End Property
    'Sohail (07 May 2015) -- End

#End Region

    ''' <summary>
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Sub GetData(Optional ByVal objDataOperation As clsDataOperation = Nothing, Optional ByVal intPaymenttranunkid As Integer = 0)
        'Public Sub GetData(Optional ByVal objDataOperation As clsDataOperation = Nothing) 'Sohail (21 Aug 2015)
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        'Sohail (12 May 2012) -- Start
        'TRA - ENHANCEMENT
        'objDataOperation = New clsDataOperation
        If objDataOperation Is Nothing Then
        objDataOperation = New clsDataOperation
        End If
        objDataOperation.ClearParameters()
        'Sohail (12 May 2012) -- End

        'Sohail (21 Aug 2015) -- Start
        'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
        If intPaymenttranunkid > 0 Then mintPaymenttranunkid = intPaymenttranunkid
        'Sohail (21 Aug 2015) -- End

        Try
            'Sohail (16 Oct 2010) -- Start
            'Changes : New field 'isglobalpayment' added.
            'strQ = "SELECT " & _
            '          "  paymenttranunkid " & _
            '          ", voucherno " & _
            '          ", periodunkid " & _
            '          ", paymentdate " & _
            '          ", employeeunkid " & _
            '          ", referenceid " & _
            '          ", paymentmode " & _
            '          ", branchunkid " & _
            '          ", chequeno " & _
            '          ", paymentby " & _
            '          ", percentage " & _
            '          ", amount " & _
            '          ", voucherref " & _
            '          ", referencetranunkid " & _
            '          ", userunkid " & _
            '          ", isvoid " & _
            '          ", voiduserunkid " & _
            '          ", voiddatetime " & _
            '          ", paytypeid " & _
            '          ", isreceipt " & _
            '          ", voidreason " & _
            '        "FROM prpayment_tran " & _
            '        "WHERE paymenttranunkid = @paymenttranunkid "
            strQ = "SELECT " & _
                      "  paymenttranunkid " & _
                      ", voucherno " & _
                      ", periodunkid " & _
                      ", paymentdate " & _
                      ", employeeunkid " & _
                      ", referenceid " & _
                      ", paymentmode " & _
                      ", branchunkid " & _
                      ", chequeno " & _
                      ", paymentby " & _
                      ", percentage " & _
                      ", amount " & _
                      ", voucherref " & _
                      ", referencetranunkid " & _
                      ", userunkid " & _
                      ", isvoid " & _
                      ", voiduserunkid " & _
                      ", voiddatetime " & _
                      ", paytypeid " & _
                      ", isreceipt " & _
                      ", voidreason " & _
                      ", isglobalpayment " & _
                      ", basecurrencyid " & _
                      ", baseexchangerate " & _
                      ", paidcurrencyid " & _
                      ", expaidrate " & _
                      ", expaidamt " & _
                      ", ISNULL(globalvocno,'') AS globalvocno " & _
                      ", ISNULL(isauthorized, 0) AS isauthorized " & _
                      ", ISNULL(accountno, '') AS accountno " & _
                      ", ISNULL(prpayment_tran.countryunkid, 0) AS countryunkid " & _
                      ", ISNULL(prpayment_tran.isapproved, 0) AS isapproved " & _
                      ", ISNULL(prpayment_tran.roundingtypeid, 0) AS roundingtypeid " & _
                      ", ISNULL(prpayment_tran.roundingmultipleid, 0) AS roundingmultipleid " & _
                      ", ISNULL(prpayment_tran.roundingadjustment, 0) AS roundingadjustment " & _
                      ", ISNULL(prpayment_tran.remarks, '') AS remarks " & _
                      ", ISNULL(paymentdate_periodunkid, 0) AS paymentdate_periodunkid " & _
                    "FROM prpayment_tran " & _
                    "WHERE paymenttranunkid = @paymenttranunkid "
            '       'Sohail (07 May 2015) - [paymentdate_periodunkid]
            '       'Sohail (24 Dec 2014) - [remarks]
            '       'Sohail (21 Mar 2014) - [roundingadjustment]
            '       'Sohail (22 Oct 2013) - [roundingtypeid, roundingmultipleid]
            '       'Sohail (08 Oct 2011)(basecurrencyid,baseexchangerate,paidcurrencyid,expaidrate,expaidamt added.) _
            '                                                     'Sohail (02 Jul 2012) - [isauthorized]
            '                                                     'Sohail (21 Jul 2012) - [accountno]
            '                                                     'Sohail (03 Sep 2012) - [countryunkid]
            '                                                     'Sohail (13 Feb 2013) - [isapproved]
            'Sohail (16 Oct 2010) -- End

            'S.SANDEEP [ 20 APRIL 2012 globalvocno ] -- START -- END

            objDataOperation.AddParameter("@paymenttranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPaymenttranunkid.ToString)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For Each dtRow As DataRow In dsList.Tables(0).Rows
                mintPaymenttranunkid = CInt(dtRow.Item("paymenttranunkid"))
                mstrVoucherno = dtRow.Item("voucherno").ToString
                mintPeriodunkid = CInt(dtRow.Item("periodunkid"))
                mdtPaymentdate = dtRow.Item("paymentdate")
                mintEmployeeunkid = CInt(dtRow.Item("employeeunkid"))
                mintPaymentrefid = CInt(dtRow.Item("referenceid"))
                mintPaymentmodeid = CInt(dtRow.Item("paymentmode"))
                mintBranchunkid = CInt(dtRow.Item("branchunkid"))
                mstrChequeno = dtRow.Item("chequeno").ToString
                mintPaymentbyid = CInt(dtRow.Item("paymentby"))
                mdblPercentage = CDec(dtRow.Item("percentage"))
                mdecAmount = CDec(dtRow.Item("amount")) 'Sohail (11 May 2011)
                mstrVoucherref = dtRow.Item("voucherref").ToString
                mintPayreftranunkid = CInt(dtRow.Item("referencetranunkid"))
                mintUserunkid = CInt(dtRow.Item("userunkid"))
                mblnIsvoid = CBool(dtRow.Item("isvoid"))
                mintVoiduserunkid = CInt(dtRow.Item("voiduserunkid"))
                If IsDBNull(dtRow.Item("voiddatetime")) Then
                    mdtVoiddatetime = Nothing
                Else
                    mdtVoiddatetime = dtRow.Item("voiddatetime")
                End If
                mintPaymentTypeId = CInt(dtRow.Item("paytypeid"))
                mblnIsReceipt = CBool(dtRow.Item("isreceipt"))
                mstrVoidreason = dtRow.Item("voidreason").ToString
                mdecOldAmount = CDec(dtRow.Item("amount")) 'Sohail (11 May 2011)
                'Sohail (16 Oct 2010) -- Start
                mblnIsglobalpayment = CBool(dtRow.Item("isglobalpayment"))
                'Sohail (08 Oct 2011) -- Start
                mintBasecurrencyid = CInt(dtRow.Item("basecurrencyid"))
                mdecBaseexchangerate = CDec(dtRow.Item("baseexchangerate"))
                mintPaidcurrencyid = CInt(dtRow.Item("paidcurrencyid"))
                mdecExpaidrate = CDec(dtRow.Item("expaidrate"))
                mdecExpaidamt = CDec(dtRow.Item("expaidamt"))
                'Sohail (08 Oct 2011) -- End
                'Sohail (16 Oct 2010) -- End

                'S.SANDEEP [ 20 APRIL 2012 ] -- START
                'ENHANCEMENT : TRA CHANGES
                mstrGlobalvocno = dtRow.Item("globalvocno").ToString
                'S.SANDEEP [ 20 APRIL 2012 ] -- END

                mblnIsauthorized = CBool(dtRow.Item("isauthorized")) 'Sohail (02 Jul 2012)
                mstrAccountno = dtRow.Item("accountno").ToString 'Sohail (21 Jul 2012)
                mintCountryunkid = CInt(dtRow.Item("countryunkid")) 'Sohail (03 Sep 2012)
                mblnIsapproved = CBool(dtRow.Item("isapproved")) 'Sohail (13 Feb 2013)
                'Sohail (22 Oct 2013) -- Start
                'TRA - ENHANCEMENT
                mintRoundingtypeid = CInt(dtRow.Item("roundingtypeid"))
                mintRoundingmultipleid = CInt(dtRow.Item("roundingmultipleid"))
                'Sohail (22 Oct 2013) -- End
                mdecRoundingAdjustment = CDec(dtRow.Item("roundingadjustment")) 'Sohail (21 Mar 2014)
                mstrRemarks = dtRow.Item("remarks").ToString 'Sohail (24 Dec 2014)
                'Sohail (07 May 2015) -- Start
                'Enhancement - New Loan Redesign with allow to change interest rate, tenure, topup, receipt.
                mintPaymentDate_Periodunkid = CInt(dtRow.Item("paymentdate_periodunkid"))
                mdecOldAmountPaidCurrency = CDec(dtRow.Item("expaidamt"))
                'Sohail (07 May 2015) -- End

                Exit For
            Next
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetData; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()

            'S.SANDEEP [ 29 May 2013 ] -- START
            'ENHANCEMENT : ATLAS COPCO WEB CHANGES
            'objDataOperation = Nothing
            'S.SANDEEP [ 29 May 2013 ] -- END
        End Try
    End Sub


    'Sohail (30 Oct 2018) -- Start
    'FDRC Enhancement - Global Payment performance issue on FDRC database in 75.1.
    Public Sub GetData(ByVal dtRow As DataRow)
        Dim exForce As Exception

        Try

            mintPaymenttranunkid = CInt(dtRow.Item("paymenttranunkid"))
            mstrVoucherno = dtRow.Item("voucherno").ToString
            mintPeriodunkid = CInt(dtRow.Item("periodunkid"))
            mdtPaymentdate = dtRow.Item("paymentdate")
            mintEmployeeunkid = CInt(dtRow.Item("employeeunkid"))
            mintPaymentrefid = CInt(dtRow.Item("referenceid"))
            mintPaymentmodeid = CInt(dtRow.Item("paymentmode"))
            mintBranchunkid = CInt(dtRow.Item("branchunkid"))
            mstrChequeno = dtRow.Item("chequeno").ToString
            mintPaymentbyid = CInt(dtRow.Item("paymentby"))
            mdblPercentage = CDec(dtRow.Item("percentage"))
            mdecAmount = CDec(dtRow.Item("amount"))
            mstrVoucherref = dtRow.Item("voucherref").ToString
            mintPayreftranunkid = CInt(dtRow.Item("referencetranunkid"))
            mintUserunkid = CInt(dtRow.Item("userunkid"))
            mblnIsvoid = CBool(dtRow.Item("isvoid"))
            mintVoiduserunkid = CInt(dtRow.Item("voiduserunkid"))
            If IsDBNull(dtRow.Item("voiddatetime")) Then
                mdtVoiddatetime = Nothing
            Else
                mdtVoiddatetime = dtRow.Item("voiddatetime")
            End If
            mintPaymentTypeId = CInt(dtRow.Item("paytypeid"))
            mblnIsReceipt = CBool(dtRow.Item("isreceipt"))
            mstrVoidreason = dtRow.Item("voidreason").ToString
            mdecOldAmount = CDec(dtRow.Item("amount"))
            mblnIsglobalpayment = CBool(dtRow.Item("isglobalpayment"))
            mintBasecurrencyid = CInt(dtRow.Item("basecurrencyid"))
            mdecBaseexchangerate = CDec(dtRow.Item("baseexchangerate"))
            mintPaidcurrencyid = CInt(dtRow.Item("paidcurrencyid"))
            mdecExpaidrate = CDec(dtRow.Item("expaidrate"))
            mdecExpaidamt = CDec(dtRow.Item("expaidamt"))

            mstrGlobalvocno = dtRow.Item("globalvocno").ToString

            mblnIsauthorized = CBool(dtRow.Item("isauthorized"))
            mstrAccountno = dtRow.Item("accountno").ToString
            mintCountryunkid = CInt(dtRow.Item("countryunkid"))
            mblnIsapproved = CBool(dtRow.Item("isapproved"))
            mintRoundingtypeid = CInt(dtRow.Item("roundingtypeid"))
            mintRoundingmultipleid = CInt(dtRow.Item("roundingmultipleid"))
            mdecRoundingAdjustment = CDec(dtRow.Item("roundingadjustment"))
            mstrRemarks = dtRow.Item("remarks").ToString
            mintPaymentDate_Periodunkid = CInt(dtRow.Item("paymentdate_periodunkid"))
            mdecOldAmountPaidCurrency = CDec(dtRow.Item("expaidamt"))


        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetData; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
        End Try
    End Sub
    'Sohail (30 Oct 2018) -- End

    'Sohail (21 Aug 2015) -- Start
    'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
    '''' <summary>
    '''' Modify By: Sandeep J. Sharma
    '''' </summary>
    '''' <purpose> Assign all Property variable </purpose>
    'Public Function GetList(ByVal strTableName As String, _
    '                        ByVal intPayRefId As Integer, _
    '                        ByVal intPayTranRefId As Integer, _
    '                        ByVal intPaymentTypeId As Integer, _
    '                        Optional ByVal strFilterQuery As String = "", _
    '                        Optional ByVal strAccessLevelFilterString As String = "",Optional ByVal objDataOperation As clsDataOperation = Nothing) As DataSet 'Sohail (02 Jul 2012) - [strFilterQuery]
    '    '                   'Sohail (18 May 2013) - [strAccessLevelFilterString]

    '    Dim dsList As DataSet = Nothing
    '    Dim strQ As String = ""
    '    Dim exForce As Exception

    '    'S.SANDEEP [ 29 May 2013 ] -- START
    '    'ENHANCEMENT : ATLAS COPCO WEB CHANGES
    '    'objDataOperation = New clsDataOperation
    '    If objDataOperation Is Nothing Then
        'objDataOperation = New clsDataOperation
    '    End If
    '    objDataOperation.ClearParameters()
    '    'S.SANDEEP [ 29 May 2013 ] -- END

    '    Try

    '        'Sohail (18 May 2013) -- Start
    '        'TRA - ENHANCEMENT
    '        If strAccessLevelFilterString.Trim = "" Then strAccessLevelFilterString = UserAccessLevel._AccessLevelFilterString
    '        'Sohail (18 May 2013) -- End

    '        'Sohail (16 Oct 2010) -- Start
    '        'Changes : New field 'isglobalpayment' added.
    '        'strQ = "SELECT " & _
    '        '           " CONVERT(CHAR(8),prpayment_tran.paymentdate,112) AS Date " & _
    '        '           ",ISNULL(prpayment_tran.voucherno,'') AS Voc " & _
    '        '           ",ISNULL(hremployee_master.firstname,'')+' '+ISNULL(hremployee_master.othername,'')+' '+ISNULL(hremployee_master.surname,'') AS EmpName " & _
    '        '           ",ISNULL(" & FinancialYear._Object._ConfigDatabaseName & "..cffinancial_year_tran.financialyear_name,'') AS YearName " & _
    '        '           ",ISNULL(cfcommon_period_tran.period_name,'') AS PeriodName " & _
    '        '           ",ISNULL(prpayment_tran.amount,0) AS Amount " & _
    '        '           ",hremployee_master.employeeunkid " & _
    '        '           "," & FinancialYear._Object._ConfigDatabaseName & "..cffinancial_year_tran.yearunkid " & _
    '        '           ",cfcommon_period_tran.periodunkid " & _
    '        '           ",prpayment_tran.paymenttranunkid " & _
    '        '        "FROM prpayment_tran " & _
    '        '           "LEFT JOIN hremployee_master ON prpayment_tran.employeeunkid = hremployee_master.employeeunkid " & _
    '        '           "LEFT JOIN cfcommon_period_tran ON prpayment_tran.periodunkid = cfcommon_period_tran.periodunkid " & _
    '        '           "LEFT JOIN " & FinancialYear._Object._ConfigDatabaseName & "..cffinancial_year_tran ON cfcommon_period_tran.yearunkid = cffinancial_year_tran.yearunkid " & _
    '        '        "WHERE prpayment_tran.referenceid = @referenceid " & _
    '        '        "AND prpayment_tran.referencetranunkid = @referencetranunkid " & _
    '        '        "AND prpayment_tran.paytypeid = @paytypeid " & _
    '        '        "AND ISNULL(prpayment_tran.isvoid,0) = 0 "


    '        'S.SANDEEP [ 20 APRIL 2012 ] -- START
    '        'ENHANCEMENT : TRA CHANGES
    '        'strQ = "SELECT " & _
    '        '           " CONVERT(CHAR(8),prpayment_tran.paymentdate,112) AS Date " & _
    '        '           ",ISNULL(prpayment_tran.voucherno,'') AS Voc " & _
    '        '           ",ISNULL(hremployee_master.employeecode,'') AS employeecode " & _
    '        '           ",ISNULL(hremployee_master.firstname,'')+' '+ISNULL(hremployee_master.othername,'')+' '+ISNULL(hremployee_master.surname,'') AS EmpName " & _
    '        '           ",ISNULL(" & FinancialYear._Object._ConfigDatabaseName & "..cffinancial_year_tran.financialyear_name,'') AS YearName " & _
    '        '           ",ISNULL(cfcommon_period_tran.period_name,'') AS PeriodName " & _
    '        '           ",ISNULL(prpayment_tran.amount,0) AS Amount " & _
    '        '           ",hremployee_master.employeeunkid " & _
    '        '           "," & FinancialYear._Object._ConfigDatabaseName & "..cffinancial_year_tran.yearunkid " & _
    '        '           ",cfcommon_period_tran.periodunkid " & _
    '        '           ",prpayment_tran.paymenttranunkid " & _
    '        '           ",isglobalpayment " & _
    '        '           ", basecurrencyid " & _
    '        '           ", baseexchangerate " & _
    '        '           ", paidcurrencyid " & _
    '        '           ", expaidrate " & _
    '        '           ", expaidamt " & _
    '        '        "FROM prpayment_tran " & _
    '        '           "LEFT JOIN hremployee_master ON prpayment_tran.employeeunkid = hremployee_master.employeeunkid " & _
    '        '           "LEFT JOIN cfcommon_period_tran ON prpayment_tran.periodunkid = cfcommon_period_tran.periodunkid " & _
    '        '           "LEFT JOIN " & FinancialYear._Object._ConfigDatabaseName & "..cffinancial_year_tran ON cfcommon_period_tran.yearunkid = cffinancial_year_tran.yearunkid " & _
    '        '        "WHERE prpayment_tran.referenceid = @referenceid " & _
    '        '        "AND prpayment_tran.referencetranunkid = @referencetranunkid " & _
    '        '        "AND prpayment_tran.paytypeid = @paytypeid " & _
    '        '        "AND ISNULL(prpayment_tran.isvoid,0) = 0 " 'Sohail (08 Oct 2011)(basecurrencyid,baseexchangerate,paidcurrencyid,expaidrate,expaidamt added.), 'Sohail (28 Jan 2012) - [employeecode]
    '        ''Sohail (16 Oct 2010) -- End


            'strQ = "SELECT " & _
            '           " CONVERT(CHAR(8),prpayment_tran.paymentdate,112) AS Date " & _
    '                   ",CASE WHEN ISNULL(isglobalpayment,0) = 0 THEN ISNULL(prpayment_tran.voucherno,'') " & _
    '                   "           WHEN ISNULL(isglobalpayment,0) = 1 THEN ISNULL(prglobalvoc_master.globalvocno,'') END AS Voc " & _
            '           ",ISNULL(hremployee_master.employeecode,'') AS employeecode " & _
            '           ",ISNULL(hremployee_master.firstname,'')+' '+ISNULL(hremployee_master.othername,'')+' '+ISNULL(hremployee_master.surname,'') AS EmpName " & _
            '           ",ISNULL(" & FinancialYear._Object._ConfigDatabaseName & "..cffinancial_year_tran.financialyear_name,'') AS YearName " & _
            '           ",ISNULL(cfcommon_period_tran.period_name,'') AS PeriodName " & _
            '           ",ISNULL(prpayment_tran.amount,0) AS Amount " & _
            '           ",hremployee_master.employeeunkid " & _
            '           "," & FinancialYear._Object._ConfigDatabaseName & "..cffinancial_year_tran.yearunkid " & _
            '           ",cfcommon_period_tran.periodunkid " & _
            '           ",prpayment_tran.paymenttranunkid " & _
            '           ",isglobalpayment " & _
            '           ", basecurrencyid " & _
            '           ", baseexchangerate " & _
            '           ", paidcurrencyid " & _
            '           ", expaidrate " & _
            '           ", expaidamt " & _
    '                   ", ISNULL(isauthorized, 0) AS isauthorized " & _
    '                   ", paymentmode " & _
    '                   ", ISNULL(accountno, '') AS accountno " & _
    '                   ", ISNULL(prpayment_tran.countryunkid, 0) AS countryunkid " & _
    '                   ", ISNULL(prpayment_tran.isapproved, 0) AS isapproved " & _
    '                   ", prpayment_tran.paymentdate " & _
    '                   ", ISNULL(prpayment_tran.roundingtypeid, 0) AS roundingtypeid " & _
    '                   ", ISNULL(prpayment_tran.roundingmultipleid, 0) AS roundingmultipleid " & _
    '                   ", ISNULL(prpayment_tran.roundingadjustment, 0) AS roundingadjustment " & _
    '                   ", ISNULL(prpayment_tran.remarks, '') AS remarks " & _
            '        "FROM prpayment_tran " & _
    '                   "LEFT JOIN prglobalvoc_master ON prpayment_tran.globalvocno = prglobalvoc_master.globalvocunkid " & _
            '           "LEFT JOIN hremployee_master ON prpayment_tran.employeeunkid = hremployee_master.employeeunkid " & _
            '           "LEFT JOIN cfcommon_period_tran ON prpayment_tran.periodunkid = cfcommon_period_tran.periodunkid " & _
            '           "LEFT JOIN " & FinancialYear._Object._ConfigDatabaseName & "..cffinancial_year_tran ON cfcommon_period_tran.yearunkid = cffinancial_year_tran.yearunkid " & _
            '        "WHERE prpayment_tran.referenceid = @referenceid " & _
            '        "AND prpayment_tran.paytypeid = @paytypeid " & _
    '                "AND ISNULL(prpayment_tran.isvoid,0) = 0 "
    '        '       'Sohail (24 Dec 2014) - [remarks]
    '        '       'Sohail (21 Mar 2014) - [roundingadjustment]
    '        '       'Sohail (22 Oct 2013) - [roundingtypeid, roundingmultipleid]
    '        '       'Sohail (02 Jul 2012) - [isauthorized]
    '        '       'Sohail (03 Sep 2012) - [countryunkid]
    '                 'Sohail (13 Feb 2013) - [isapproved]
    '        'S.SANDEEP [ 20 APRIL 2012 ] -- END

    '        'S.SANDEEP [ 29 May 2013 ] -- START {paymentdate} -- END


    '        'S.SANDEEP [ 11 JAN 2012 ] -- START {globalvocno New Table}-- END

    '        '"AND prpayment_tran.referencetranunkid = @referencetranunkid " & _  'Sohail (02 Jul 2012) - [moved to IF condition]



    '        'Anjan (09 Aug 2011)-Start
    '        'Issue : For including setting of acitve and inactive employee.
    '        'commented due to employee was active in particular period and when payment authorization is done in next period and in that period if this employee is iactive then system was not allowing to authorize. 
    '        'If ConfigParameter._Object._IsIncludeInactiveEmp = False Then
    '        ''    strQ &= " AND ISNULL(hremployee_master.isactive,0) = 1 "
    '        '    'Sohail (06 Jan 2012) -- Start
    '        '    'TRA - ENHANCEMENT
    '        '    strQ &= " AND CONVERT(CHAR(8),hremployee_master.appointeddate,112) <= @enddate " & _
    '        '               " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_from_date,112),@startdate) >= @startdate " & _
    '        '               " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_to_date,112),@startdate) >= @startdate " & _
    '        '               " AND ISNULL(CONVERT(CHAR(8),hremployee_master.empl_enddate,112), @startdate) >= @startdate " 'S.SANDEEP [ 15 MAY 2012 empl_enddate ] -- START -- END

    '        '    objDataOperation.AddParameter("@startdate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, ConfigParameter._Object._EmployeeAsOnDate)
    '        '    objDataOperation.AddParameter("@enddate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, ConfigParameter._Object._EmployeeAsOnDate)
    '        '    'Sohail (06 Jan 2012) -- End
    '        'End If
    '        'Anjan (09 Aug 2011)-End 

    '        'Sohail (24 Jun 2011) -- Start
    '        'Issue : According to privilege that lower level user should not see superior level employees.

    '        'S.SANDEEP [ 04 FEB 2012 ] -- START
    '        'ENHANCEMENT : TRA CHANGES
    '        'If UserAccessLevel._AccessLevel.Length > 0 Then
    '        '    strQ &= " AND hremployee_master.jobunkid IN (" & UserAccessLevel._AccessLevel & ") "
    '        'End If

    '        'S.SANDEEP [ 01 JUNE 2012 ] -- START
    '        'ENHANCEMENT : TRA DISCIPLINE CHANGES
    '        'Select Case ConfigParameter._Object._UserAccessModeSetting
    '        '    Case enAllocation.BRANCH
    '        '        If UserAccessLevel._AccessLevel.Length > 0 Then
    '        '            strQ &= " AND  hremployee_master.stationunkid IN (" & UserAccessLevel._AccessLevel & ") "
    '        '        End If
    '        '    Case enAllocation.DEPARTMENT_GROUP
    '        '        If UserAccessLevel._AccessLevel.Length > 0 Then
    '        '            strQ &= " AND  hremployee_master.deptgroupunkid IN (" & UserAccessLevel._AccessLevel & ") "
    '        '        End If
    '        '    Case enAllocation.DEPARTMENT
    '        '        If UserAccessLevel._AccessLevel.Length > 0 Then
    '        '            strQ &= " AND  hremployee_master.departmentunkid IN (" & UserAccessLevel._AccessLevel & ") "
    '        '        End If
    '        '    Case enAllocation.SECTION_GROUP
    '        '        If UserAccessLevel._AccessLevel.Length > 0 Then
    '        '            strQ &= " AND  hremployee_master.sectiongroupunkid IN (" & UserAccessLevel._AccessLevel & ") "
    '        '        End If
    '        '    Case enAllocation.SECTION
    '        '        If UserAccessLevel._AccessLevel.Length > 0 Then
    '        '            strQ &= " AND  hremployee_master.sectionunkid IN (" & UserAccessLevel._AccessLevel & ") "
    '        '        End If
    '        '    Case enAllocation.UNIT_GROUP
    '        '        If UserAccessLevel._AccessLevel.Length > 0 Then
    '        '            strQ &= " AND  hremployee_master.unitgroupunkid IN (" & UserAccessLevel._AccessLevel & ") "
    '        '        End If
    '        '    Case enAllocation.UNIT
    '        '        If UserAccessLevel._AccessLevel.Length > 0 Then
    '        '            strQ &= " AND  hremployee_master.unitunkid IN (" & UserAccessLevel._AccessLevel & ") "
    '        '        End If
    '        '    Case enAllocation.TEAM
    '        '        If UserAccessLevel._AccessLevel.Length > 0 Then
    '        '            strQ &= " AND  hremployee_master.teamunkid IN (" & UserAccessLevel._AccessLevel & ") "
    '        '        End If
    '        '    Case enAllocation.JOB_GROUP
    '        '        If UserAccessLevel._AccessLevel.Length > 0 Then
    '        '            strQ &= " AND  hremployee_master.jobgroupunkid IN (" & UserAccessLevel._AccessLevel & ") "
    '        '        End If
    '        '    Case enAllocation.JOBS
    '        '        If UserAccessLevel._AccessLevel.Length > 0 Then
    '        '            strQ &= " AND hremployee_master.jobunkid IN (" & UserAccessLevel._AccessLevel & ") "
    '        '        End If
    '        'End Select

    '        'Sohail (18 May 2013) -- Start
    '        'TRA - ENHANCEMENT
    '        'strQ &= UserAccessLevel._AccessLevelFilterString
    '        strQ &= strAccessLevelFilterString
    '        'Sohail (18 May 2013) -- End
    '        'S.SANDEEP [ 01 JUNE 2012 ] -- END


    '        'S.SANDEEP [ 04 FEB 2012 ] -- END

    '        'Sohail (24 Jun 2011) -- End

    '        'Sohail (02 Jul 2012) -- Start
    '        'TRA - ENHANCEMENT
    '        If intPayTranRefId > 0 Then
    '            strQ &= " AND prpayment_tran.referencetranunkid = @referencetranunkid "
    '        End If

    '        If strFilterQuery.Trim <> "" Then
    '            strQ &= " AND " & strFilterQuery
    '        End If
    '        'Sohail (02 Jul 2012) -- End

    '        objDataOperation.AddParameter("@referenceid", SqlDbType.Int, eZeeDataType.INT_SIZE, intPayRefId.ToString)
    '        objDataOperation.AddParameter("@referencetranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intPayTranRefId.ToString)
    '        objDataOperation.AddParameter("@paytypeid", SqlDbType.Int, eZeeDataType.INT_SIZE, intPaymentTypeId.ToString)

    '        dsList = objDataOperation.ExecQuery(strQ, strTableName)

    '        If objDataOperation.ErrorMessage <> "" Then
    '            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
    '            Throw exForce
    '        End If



    '    Catch ex As Exception
    '        Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
    '    Finally
    '        exForce = Nothing
    '        If dsList IsNot Nothing Then dsList.Dispose()
    '        'S.SANDEEP [ 29 May 2013 ] -- START
    '        'ENHANCEMENT : ATLAS COPCO WEB CHANGES
    '        'objDataOperation = Nothing
    '        'S.SANDEEP [ 29 May 2013 ] -- END
    '    End Try
    '    Return dsList
    'End Function
    Public Function GetList(ByVal xDatabaseName As String _
                            , ByVal xUserUnkid As Integer _
                            , ByVal xYearUnkid As Integer _
                            , ByVal xCompanyUnkid As Integer _
                            , ByVal xPeriodStart As DateTime _
                            , ByVal xPeriodEnd As DateTime _
                            , ByVal xUserModeSetting As String _
                            , ByVal xOnlyApproved As Boolean _
                            , ByVal xIncludeIn_ActiveEmployee As Boolean _
                            , ByVal strTableName As String _
                            , ByVal intPayRefId As Integer _
                            , ByVal intPayTranRefId As Integer _
                            , ByVal intPaymentTypeId As Integer _
                            , Optional ByVal blnApplyUserAccessFilter As Boolean = True _
                            , Optional ByVal strFilterQuery As String = "" _
                            , Optional ByVal objDataOperation As clsDataOperation = Nothing) As DataSet

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        Dim xDateJoinQry, xDateFilterQry, xUACQry, xUACFiltrQry, xAdvanceJoinQry As String
        xDateJoinQry = "" : xDateFilterQry = "" : xUACQry = "" : xUACFiltrQry = "" : xAdvanceJoinQry = ""
        Call GetDatesFilterString(xDateJoinQry, xDateFilterQry, xPeriodStart, xPeriodEnd, , , xDatabaseName)
        Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, xPeriodEnd, xOnlyApproved, xDatabaseName, xUserUnkid, xCompanyUnkid, xYearUnkid, xUserModeSetting)
        Call GetAdvanceFilterQry(xAdvanceJoinQry, xPeriodEnd, xDatabaseName)

        If objDataOperation Is Nothing Then
            objDataOperation = New clsDataOperation
        End If
        objDataOperation.ClearParameters()

        Try

            'If strAccessLevelFilterString.Trim = "" Then strAccessLevelFilterString = UserAccessLevel._AccessLevelFilterString

            'Sohail (26 May 2017) -- Start
            'Issue - 67.1 - Authorize payment total not matching with global payment total.
            Dim objExchangeRate As New clsExchangeRate
            Dim ds As DataSet = objExchangeRate.getComboList("List", True, False)
            Dim dicExchangeRate As Dictionary(Of Integer, String) = (From p In ds.Tables("List") Select New With {.id = CInt(p.Item("exchangerateunkid")), .name = p.Item("currency_sign").ToString}).ToDictionary(Function(x) x.id, Function(x) x.name)
            'Sohail (26 May 2017) -- End

            strQ = "SELECT   CONVERT(CHAR(8),prpayment_tran.paymentdate,112) AS Date " & _
                       ",CASE WHEN ISNULL(isglobalpayment,0) = 0 THEN ISNULL(prpayment_tran.voucherno,'') " & _
                       "           WHEN ISNULL(isglobalpayment,0) = 1 THEN ISNULL(prglobalvoc_master.globalvocno,'') END AS Voc " & _
                       ",ISNULL(hremployee_master.employeecode,'') AS employeecode " & _
                       ",ISNULL(hremployee_master.firstname,'')+' '+ISNULL(hremployee_master.othername,'')+' '+ISNULL(hremployee_master.surname,'') AS EmpName " & _
                       ",ISNULL(" & FinancialYear._Object._ConfigDatabaseName & "..cffinancial_year_tran.financialyear_name,'') AS YearName " & _
                       ",ISNULL(cfcommon_period_tran.period_name,'') AS PeriodName " & _
                       ",ISNULL(prpayment_tran.amount,0) AS Amount " & _
                       ",hremployee_master.employeeunkid " & _
                       "," & FinancialYear._Object._ConfigDatabaseName & "..cffinancial_year_tran.yearunkid " & _
                       ",cfcommon_period_tran.periodunkid " & _
                       ",prpayment_tran.paymenttranunkid " & _
                       ",isglobalpayment " & _
                       ", basecurrencyid " & _
                       ", baseexchangerate " & _
                       ", paidcurrencyid " & _
                       ", ISNULL(cfexchange_rate.currency_sign,'') AS currency_sign " & _
                       ", expaidrate " & _
                       ", expaidamt " & _
                       ", ISNULL(isauthorized, 0) AS isauthorized " & _
                       ", paymentmode " & _
                       ", ISNULL(accountno, '') AS accountno " & _
                       ", ISNULL(prpayment_tran.countryunkid, 0) AS countryunkid " & _
                       ", ISNULL(prpayment_tran.isapproved, 0) AS isapproved " & _
                       ", prpayment_tran.paymentdate " & _
                       ", ISNULL(prpayment_tran.roundingtypeid, 0) AS roundingtypeid " & _
                       ", ISNULL(prpayment_tran.roundingmultipleid, 0) AS roundingmultipleid " & _
                       ", ISNULL(prpayment_tran.roundingadjustment, 0) AS roundingadjustment " & _
                       ", ISNULL(prpayment_tran.remarks, '') AS remarks " & _
                       ", ISNULL(prpayment_tran.paymentdate_periodunkid, 0) AS paymentdate_periodunkid " & _
                       ", prpayment_tran.referenceid " & _
                       ", prpayment_tran.paytypeid " & _
                       ", prpayment_tran.branchunkid " & _
                       ", prpayment_tran.chequeno " & _
                       ", prpayment_tran.referencetranunkid " & _
                       ", prpayment_tran.paymentby " & _
                       ", prpayment_tran.percentage " & _
                       ", prpayment_tran.voucherref " & _
                       ", prpayment_tran.voucherno " & _
                       ", prpayment_tran.globalvocno " & _
                       ", prpayment_tran.isreceipt " & _
                       ", prpayment_tran.userunkid " & _
                       ", prpayment_tran.isvoid " & _
                       ", prpayment_tran.voiduserunkid " & _
                       ", prpayment_tran.voiddatetime " & _
                       ", prpayment_tran.voidreason "
            'Sohail (30 Oct 2018) - [paymentby, percentage, voucherref, voucherno, globalvocno, userunkid, isvoid, voiduserunkid, voiddatetime, voidreason]

            'Sohail (26 May 2017) -- Start
            'Issue - 67.1 - Authorize payment total not matching with global payment total.
            strQ &= ", CASE prpayment_tran.paidcurrencyid "
            For Each pair In dicExchangeRate
                strQ &= " WHEN " & pair.Key & "  THEN '" & pair.Value & "' "
            Next
            strQ &= " END AS paidcurrencysign "
            'Sohail (26 May 2017) -- End

            strQ &= "FROM prpayment_tran " & _
                       "LEFT JOIN cfexchange_rate ON prpayment_tran.paidcurrencyid = cfexchange_rate.exchangerateunkid " & _
                       "LEFT JOIN prglobalvoc_master ON prpayment_tran.globalvocno = prglobalvoc_master.globalvocunkid " & _
                       "LEFT JOIN hremployee_master ON prpayment_tran.employeeunkid = hremployee_master.employeeunkid " & _
                       "LEFT JOIN cfcommon_period_tran ON prpayment_tran.periodunkid = cfcommon_period_tran.periodunkid " & _
                       "LEFT JOIN " & FinancialYear._Object._ConfigDatabaseName & "..cffinancial_year_tran ON cfcommon_period_tran.yearunkid = cffinancial_year_tran.yearunkid "
            'Sohail (05 Dec 2015) -- - [referenceid, paytypeid, branchunkid, chequeno, referencetranunkid] - [Updating Accounting Integration Projects to Aruti 58.1 .]

            'Nilay (28-Aug-2015) -- Start
            'Purpose : To display Currency_Sign following ADDED
            '", ISNULL(cfexchange_rate.currency_sign,'') AS currency_sign " & _   --- ADDED
            '"LEFT JOIN cfexchange_rate ON prpayment_tran.paidcurrencyid = cfexchange_rate.exchangerateunkid " & _ --- ADDED
            'Nilay (28-Aug-2015) -- End

            If xDateJoinQry.Trim.Length > 0 Then
                strQ &= xDateJoinQry
            End If

            'S.SANDEEP [15 NOV 2016] -- START
            'If xUACQry.Trim.Length > 0 Then
            '    StrQ &= xUACQry
            'End If
            If blnApplyUserAccessFilter = True Then
            If xUACQry.Trim.Length > 0 Then
                strQ &= xUACQry
            End If
            End If
            'S.SANDEEP [15 NOV 2016] -- END

            If xAdvanceJoinQry.Trim.Length > 0 Then
                strQ &= xAdvanceJoinQry
            End If

            strQ &= "WHERE prpayment_tran.referenceid = @referenceid " & _
                    "AND prpayment_tran.paytypeid = @paytypeid " & _
                    "AND ISNULL(prpayment_tran.isvoid,0) = 0 "

            
            If blnApplyUserAccessFilter = True Then
                If xUACFiltrQry.Trim.Length > 0 Then
                    strQ &= " AND " & xUACFiltrQry
                End If
            End If

            If xIncludeIn_ActiveEmployee = False Then
                If xDateFilterQry.Trim.Length > 0 Then
                    strQ &= xDateFilterQry
                End If
            End If

            If intPayTranRefId > 0 Then
                strQ &= " AND prpayment_tran.referencetranunkid = @referencetranunkid "
            End If

            If strFilterQuery.Trim <> "" Then
                strQ &= " AND " & strFilterQuery
            End If

            objDataOperation.AddParameter("@referenceid", SqlDbType.Int, eZeeDataType.INT_SIZE, intPayRefId.ToString)
            objDataOperation.AddParameter("@referencetranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intPayTranRefId.ToString)
            objDataOperation.AddParameter("@paytypeid", SqlDbType.Int, eZeeDataType.INT_SIZE, intPaymentTypeId.ToString)

            dsList = objDataOperation.ExecQuery(strQ, strTableName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
        End Try
        Return dsList
    End Function
    'Sohail (21 Aug 2015) -- End

    ''' <summary>
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> INSERT INTO Database Table (prpayment_tran) </purpose>
    Public Function Insert(ByVal strDatabaseName As String _
                         , ByVal xUserUnkid As Integer _
                           , ByVal intYearUnkid As Integer _
                           , ByVal intCompanyunkid As Integer _
                           , ByVal xPeriodStart As Date _
                           , ByVal xPeriodEnd As Date _
                           , ByVal strUserAccessModeSetting As String _
                           , ByVal xOnlyApproved As Boolean _
                           , ByVal blnIsIncludeInactiveEmp As Boolean _
                           , ByVal intPaymentVocNoType As Integer _
                           , ByVal strPaymentVocPrefix As String _
                           , ByVal dtCurrentDateAndTime As Date _
                           , ByVal blnApplyUserAccessFilter As Boolean _
                           , ByVal strFilter As String _
                           , ByVal mdtActivityAdjustment As DataTable _
                           ) As Boolean 'Sohail (21 Aug 2015)
        'Sohail (23 May 2017) - [mdtActivityAdjustment]
        'Nilay (25-Mar-2016) -- [xUserUnkid]
        'Sohail (21 Aug 2015) - [xPeriodStart, xPeriodEnd, xOnlyApproved, blnApplyUserAccessFilter, strFilter]
        'Public Function Insert() As Boolean
        Dim objDataOperation As clsDataOperation

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim dtTran As DataTable
        objDataOperation = New clsDataOperation

        Try

            '*************************************************************************************
            '***    PLEASE ADD NEW FIELD in    UpdatePaymentAuthorization     METHOD IN THIS CLASS
            '*************************************************************************************

            'Sohail (17 Dec 2014) -- Start
            'Enhancement - Provide Link for Payroll approval/Authorize Payslip Payment.
            If mintPaymentrefid = enPaymentRefId.PAYSLIP Then
                'Sohail (30 Oct 2018) -- Start
                'FDRC Enhancement - Global Payment performance issue on FDRC database in 75.1.
                'If IsPaymentDone(mintPaymentTypeId, mintPayreftranunkid, mintPaymentrefid) = True Then
                If IsPaymentDone(mintPaymentTypeId, mintPayreftranunkid, mintPaymentrefid, objDataOperation) = True Then
                    'Sohail (30 Oct 2018) -- End
                    mstrMessage = Language.getMessage(mstrModuleName, 29, "Sorry, Payment is already done.")
                    Exit Try
                End If
            End If
            'Sohail (17 Dec 2014) -- End

            objDataOperation.BindTransaction()


            'Anjan (21 Jan 2012)-Start
            'ENHANCEMENT : TRA COMMENTS 
            'Issue : To fix the issue on network of autocode concurrency when more than 1 user press save at same time.
            'Sandeep [ 16 Oct 2010 ] -- Start
            'Issue : Auto No. Generation



            'S.SANDEEP [ 20 APRIL 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            'Dim intPayVocNoType As Integer = 0
            'Dim strPayVocPrifix As String = ""
            'Dim intNextPayVocNo As Integer = 0

            'intPayVocNoType = ConfigParameter._Object._PaymentVocNoType

            'If intPayVocNoType = 1 Then
            '    ConfigParameter._Object.Refresh() 'Sohail (15 Dec 2010) Issue : When Payment is done from Multiple machine.
            '    strPayVocPrifix = ConfigParameter._Object._PaymentVocPrefix
            '    intNextPayVocNo = ConfigParameter._Object._NextPaymentVocNo
            '    mstrVoucherno = strPayVocPrifix & intNextPayVocNo
            'End If
            ''Sandeep [ 16 Oct 2010 ] -- End 

            'If isExist(mstrVoucherno) Then
            '    'Sohail (15 Dec 2010) -- Start
            '    'mstrMessage = Language.getMessage(mstrModuleName, 1, "Voucher No. is already defined. Please define new Voucher No.")
            '    'Return False
            '    If intPayVocNoType = 1 Then
            '        intNextPayVocNo = GetMaxPaymentVoucherNo() + 1
            '        mstrVoucherno = strPayVocPrifix & intNextPayVocNo
            '        'mstrMessage = Language.getMessage(mstrModuleName, 2, "Voucher No. '" & mstrVoucherno & "' is already defined. Please define new Voucher No. from : Aruti Configuration -> Option -> Payment Voc# No.")
            '    Else
            '        mstrMessage = Language.getMessage(mstrModuleName, 1, "This Voucher No. is already defined. Please define new Voucher No.")
            '        Return False
            '    End If
            '    'Sohail (15 Dec 2010) -- End
            'End If
            Dim intPayVocNoType As Integer = 0
            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'intPayVocNoType = ConfigParameter._Object._PaymentVocNoType
            intPayVocNoType = intPaymentVocNoType
            'Sohail (21 Aug 2015) -- End
            If intPayVocNoType = 0 Then
            If isExist(mstrVoucherno) Then
                    mstrMessage = Language.getMessage(mstrModuleName, 1, "This Voucher No. is already defined. Please define new Voucher No.")
                    objDataOperation.ReleaseTransaction(False)
                    Return False
                End If
            End If
            'S.SANDEEP [ 20 APRIL 2012 ] -- END

            'Anjan (21 Jan 2012)-End 

            'Sohail (07 May 2015) -- Start
            'Enhancement - New Loan Redesign with allow to change interest rate, tenure, topup, receipt.
            Dim objPeriod As New clscommom_period_Tran
            'Nilay (10-Oct-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'objPeriod._Periodunkid = mintPeriodunkid

            'Sohail (15 Dec 2015) -- Start
            'Enhancement - New Loan Calculation type Reducing balance with Fixed Principal EMI and new interest calculation Monthly apart from Daily for KBC.
            'objPeriod._Periodunkid(strDatabaseName) = mintPeriodunkid
            objPeriod._Periodunkid(strDatabaseName) = mintPaymentDate_Periodunkid
            'Sohail (15 Dec 2015) -- End

            'Nilay (10-Oct-2015) -- End
            Dim dsLoan As DataSet = Nothing
            If mintPaymentrefid = enPaymentRefId.LOAN AndAlso mintPaymentTypeId = enPayTypeId.RECEIVED Then
                'Nilay (25-Mar-2016) -- Start
                'dsLoan = objLoan_Advance.Calculate_LoanBalanceInfo("List", mdtPaymentdate, "", mintPayreftranunkid)
                dsLoan = objLoan_Advance.Calculate_LoanBalanceInfo(strDatabaseName, xUserUnkid, intYearUnkid, intCompanyunkid, _
                                                                   xPeriodStart, xPeriodEnd, strUserAccessModeSetting, xOnlyApproved, _
                                                                   "List", mdtPaymentdate, "", mintPayreftranunkid)
                'Nilay (25-Mar-2016) -- End

            ElseIf mintPaymentrefid = enPaymentRefId.PAYSLIP AndAlso mintPaymentTypeId = enPayTypeId.PAYMENT Then
                'Nilay (25-Mar-2016) -- Start
                'dsLoan = objLoan_Advance.Calculate_LoanBalanceInfo("List", objPeriod._End_Date.AddDays(1), "", 0, objPeriod._Start_Date, True)
                dsLoan = objLoan_Advance.Calculate_LoanBalanceInfo(strDatabaseName, xUserUnkid, intYearUnkid, intCompanyunkid, _
                                                                   xPeriodStart, xPeriodEnd, strUserAccessModeSetting, xOnlyApproved, _
                                                                   "List", objPeriod._End_Date.AddDays(1), "", 0, objPeriod._Start_Date, True)
                'Nilay (25-Mar-2016) -- End
            End If
            'Sohail (07 May 2015) -- End

            'Sohail (14 Dec 2018) -- Start
            'Twaweza Issue : Payment Time not coming on payroll report prepared by.
            objDataOperation.ClearParameters()
            'Sohail (14 Dec 2018) -- End
            objDataOperation.AddParameter("@voucherno", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoucherno.ToString)
            objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPeriodunkid.ToString)
            objDataOperation.AddParameter("@paymentdate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtPaymentdate)
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeunkid.ToString)
            objDataOperation.AddParameter("@referenceid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPaymentrefid.ToString)
            objDataOperation.AddParameter("@paymentmode", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPaymentmodeid.ToString)
            objDataOperation.AddParameter("@branchunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintBranchunkid.ToString)
            objDataOperation.AddParameter("@chequeno", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrChequeno.ToString)
            objDataOperation.AddParameter("@paymentby", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPaymentbyid.ToString)
            objDataOperation.AddParameter("@percentage", SqlDbType.Float, eZeeDataType.MONEY_SIZE, mdblPercentage.ToString)
            objDataOperation.AddParameter("@amount", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecAmount.ToString) 'Sohail (11 May 2011)
            objDataOperation.AddParameter("@voucherref", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoucherref.ToString)
            objDataOperation.AddParameter("@referencetranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPayreftranunkid.ToString)
            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)

            If mdtVoiddatetime = Nothing Then
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            Else
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime)
            End If
            objDataOperation.AddParameter("@paytypeid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPaymentTypeId.ToString)
            objDataOperation.AddParameter("@isreceipt", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsReceipt.ToString)
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)
            'Sohail (16 Oct 2010) -- Start
            objDataOperation.AddParameter("@isglobalpayment", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsglobalpayment.ToString)
            'Sohail (16 Oct 2010) -- End
            'Sohail (08 Oct 2011) -- Start
            objDataOperation.AddParameter("@basecurrencyid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintBasecurrencyid.ToString)
            objDataOperation.AddParameter("@baseexchangerate", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecBaseexchangerate.ToString)
            objDataOperation.AddParameter("@paidcurrencyid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPaidcurrencyid.ToString)
            objDataOperation.AddParameter("@expaidrate", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecExpaidrate.ToString)
            objDataOperation.AddParameter("@expaidamt", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecExpaidamt.ToString)
            'Sohail (08 Oct 2011) -- End

            'S.SANDEEP [ 20 APRIL 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            objDataOperation.AddParameter("@globalvocno", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrGlobalvocno.ToString)
            'S.SANDEEP [ 20 APRIL 2012 ] -- END
            objDataOperation.AddParameter("@isauthorized", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsauthorized.ToString) 'Sohail (02 Jul 2012)
            objDataOperation.AddParameter("@accountno", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrAccountno.ToString) 'Sohail (21 Jul 2012)
            objDataOperation.AddParameter("@countryunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCountryunkid.ToString) 'Sohail (03 Sep 2012)
            objDataOperation.AddParameter("@isapproved", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsapproved.ToString) 'Sohail (13 Feb 2013)
            'Sohail (22 Oct 2013) -- Start
            'TRA - ENHANCEMENT
            objDataOperation.AddParameter("@roundingtypeid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintRoundingtypeid.ToString)
            objDataOperation.AddParameter("@roundingmultipleid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintRoundingmultipleid.ToString)
            'Sohail (22 Oct 2013) -- End
            objDataOperation.AddParameter("@roundingadjustment", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecRoundingAdjustment) 'Sohail (21 Mar 2014)
            objDataOperation.AddParameter("@remarks", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, mstrRemarks.ToString) 'Sohail (24 Dec 2014)
            'Sohail (07 May 2015) -- Start
            'Enhancement - New Loan Redesign with allow to change interest rate, tenure, topup, receipt.
            objDataOperation.AddParameter("@paymentdate_periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPaymentDate_Periodunkid.ToString)
            'Sohail (07 May 2015) -- End

            'Sohail (16 Oct 2010) -- Start
            'Changes : New field 'isglobalpayment' added.
            'strQ = "INSERT INTO prpayment_tran ( " & _
            '          "  voucherno " & _
            '          ", periodunkid " & _
            '          ", paymentdate " & _
            '          ", employeeunkid " & _
            '          ", referenceid " & _
            '          ", paymentmode " & _
            '          ", branchunkid " & _
            '          ", chequeno " & _
            '          ", paymentby " & _
            '          ", percentage " & _
            '          ", amount " & _
            '          ", voucherref " & _
            '          ", referencetranunkid " & _
            '          ", userunkid " & _
            '          ", isvoid " & _
            '          ", voiduserunkid " & _
            '          ", voiddatetime " & _
            '          ", paytypeid " & _
            '          ", isreceipt " & _
            '          ", voidreason" & _
            '        ") VALUES (" & _
            '          "  @voucherno " & _
            '          ", @periodunkid " & _
            '          ", @paymentdate " & _
            '          ", @employeeunkid " & _
            '          ", @referenceid " & _
            '          ", @paymentmode " & _
            '          ", @branchunkid " & _
            '          ", @chequeno " & _
            '          ", @paymentby " & _
            '          ", @percentage " & _
            '          ", @amount " & _
            '          ", @voucherref " & _
            '          ", @referencetranunkid " & _
            '          ", @userunkid " & _
            '          ", @isvoid " & _
            '          ", @voiduserunkid " & _
            '          ", @voiddatetime " & _
            '          ", @paytypeid " & _
            '          ", @isreceipt " & _
            '          ", @voidreason" & _
            '        "); SELECT @@identity"


            'S.SANDEEP [ 20 APRIL 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            'strQ = "INSERT INTO prpayment_tran ( " & _
            '          "  voucherno " & _
            '          ", periodunkid " & _
            '          ", paymentdate " & _
            '          ", employeeunkid " & _
            '          ", referenceid " & _
            '          ", paymentmode " & _
            '          ", branchunkid " & _
            '          ", chequeno " & _
            '          ", paymentby " & _
            '          ", percentage " & _
            '          ", amount " & _
            '          ", voucherref " & _
            '          ", referencetranunkid " & _
            '          ", userunkid " & _
            '          ", isvoid " & _
            '          ", voiduserunkid " & _
            '          ", voiddatetime " & _
            '          ", paytypeid " & _
            '          ", isreceipt " & _
            '          ", voidreason" & _
            '          ", isglobalpayment " & _
            '          ", basecurrencyid " & _
            '          ", baseexchangerate " & _
            '          ", paidcurrencyid " & _
            '          ", expaidrate " & _
            '          ", expaidamt " & _
            '        ") VALUES (" & _
            '          "  @voucherno " & _
            '          ", @periodunkid " & _
            '          ", @paymentdate " & _
            '          ", @employeeunkid " & _
            '          ", @referenceid " & _
            '          ", @paymentmode " & _
            '          ", @branchunkid " & _
            '          ", @chequeno " & _
            '          ", @paymentby " & _
            '          ", @percentage " & _
            '          ", @amount " & _
            '          ", @voucherref " & _
            '          ", @referencetranunkid " & _
            '          ", @userunkid " & _
            '          ", @isvoid " & _
            '          ", @voiduserunkid " & _
            '          ", @voiddatetime " & _
            '          ", @paytypeid " & _
            '          ", @isreceipt " & _
            '          ", @voidreason" & _
            '          ", @isglobalpayment" & _
            '          ", @basecurrencyid " & _
            '          ", @baseexchangerate " & _
            '          ", @paidcurrencyid " & _
            '          ", @expaidrate " & _
            '          ", @expaidamt " & _
            '        "); SELECT @@identity" 'Sohail (08 Oct 2011)(basecurrencyid,baseexchangerate,paidcurrencyid,expaidrate,expaidamt added.)
            ''Sohail (16 Oct 2010) -- End
            strQ = "INSERT INTO prpayment_tran ( " & _
                      "  voucherno " & _
                      ", periodunkid " & _
                      ", paymentdate " & _
                      ", employeeunkid " & _
                      ", referenceid " & _
                      ", paymentmode " & _
                      ", branchunkid " & _
                      ", chequeno " & _
                      ", paymentby " & _
                      ", percentage " & _
                      ", amount " & _
                      ", voucherref " & _
                      ", referencetranunkid " & _
                      ", userunkid " & _
                      ", isvoid " & _
                      ", voiduserunkid " & _
                      ", voiddatetime " & _
                      ", paytypeid " & _
                      ", isreceipt " & _
                      ", voidreason" & _
                      ", isglobalpayment " & _
                      ", basecurrencyid " & _
                      ", baseexchangerate " & _
                      ", paidcurrencyid " & _
                      ", expaidrate " & _
                      ", expaidamt " & _
                      ", globalvocno " & _
                      ", isauthorized " & _
                      ", accountno " & _
                      ", countryunkid " & _
                      ", isapproved " & _
                      ", roundingtypeid " & _
                      ", roundingmultipleid " & _
                      ", roundingadjustment " & _
                      ", remarks " & _
                      ", paymentdate_periodunkid " & _
                    ") VALUES (" & _
                      "  @voucherno " & _
                      ", @periodunkid " & _
                      ", @paymentdate " & _
                      ", @employeeunkid " & _
                      ", @referenceid " & _
                      ", @paymentmode " & _
                      ", @branchunkid " & _
                      ", @chequeno " & _
                      ", @paymentby " & _
                      ", @percentage " & _
                      ", @amount " & _
                      ", @voucherref " & _
                      ", @referencetranunkid " & _
                      ", @userunkid " & _
                      ", @isvoid " & _
                      ", @voiduserunkid " & _
                      ", @voiddatetime " & _
                      ", @paytypeid " & _
                      ", @isreceipt " & _
                      ", @voidreason" & _
                      ", @isglobalpayment" & _
                      ", @basecurrencyid " & _
                      ", @baseexchangerate " & _
                      ", @paidcurrencyid " & _
                      ", @expaidrate " & _
                      ", @expaidamt " & _
                      ", @globalvocno " & _
                      ", @isauthorized " & _
                      ", @accountno " & _
                      ", @countryunkid " & _
                      ", @isapproved " & _
                      ", @roundingtypeid " & _
                      ", @roundingmultipleid " & _
                      ", @roundingadjustment " & _
                      ", @remarks " & _
                      ", @paymentdate_periodunkid " & _
                    "); SELECT @@identity"
            '       'Sohail (07 May 2015) - [paymentdate_periodunkid]
            '       'Sohail (24 Dec 2014) - [remarks]
            '       'Sohail (21 Mar 2014) - [roundingadjustment]
            '       'Sohail (22 Oct 2013) - [roundingtypeid, roundingmultipleid]
            '       'Sohail (02 Jul 2012) - [isauthorized], 'Sohail (21 Jul 2012) - [accountno]
            '       'Sohail (03 Sep 2012) - [countryunkid]
            '       'Sohail (13 Feb 2013) - [isapproved]
            'S.SANDEEP [ 20 APRIL 2012 ] -- END



            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            mintPaymenttranunkid = dsList.Tables(0).Rows(0).Item(0)

            'S.SANDEEP [ 20 APRIL 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES

            If intPayVocNoType = 1 Then
                'Sohail (21 Aug 2015) -- Start
                'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                'If Set_AutoNumber(objDataOperation, mintPaymenttranunkid, "prpayment_tran", "voucherno", "paymenttranunkid", "NextPaymentVocNo", ConfigParameter._Object._PaymentVocPrefix) = False Then
                'Hemant (07 Jan 2019) -- Start
                'ISSUE/ENHANCEMENT : {Audit Trails} in 76.1.
                'If Set_AutoNumber(objDataOperation, mintPaymenttranunkid, "prpayment_tran", "voucherno", "paymenttranunkid", "NextPaymentVocNo", strPaymentVocPrefix) = False Then
                If Set_AutoNumber(objDataOperation, mintPaymenttranunkid, "prpayment_tran", "voucherno", "paymenttranunkid", "NextPaymentVocNo", strPaymentVocPrefix, intCompanyunkid) = False Then
                    'Hemant (07 Jan 2019) -- End
                    'Sohail (21 Aug 2015) -- End
                    'S.SANDEEP [ 17 NOV 2012 ] -- START
                    'ENHANCEMENT : TRA CHANGES
                    'If objDataOperation.ErrorMessage <> "" Then
                    '    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    '    Throw exForce
                    'End If

                    If objDataOperation.ErrorMessage <> "" Then
                        objDataOperation.ReleaseTransaction(False)
                        Return False
                    End If
                    'S.SANDEEP [ 17 NOV 2012 ] -- END
                End If

                If Get_Saved_Number(objDataOperation, mintPaymenttranunkid, "prpayment_tran", "voucherno", "paymenttranunkid", mstrVoucherno) = False Then
                    If objDataOperation.ErrorMessage <> "" Then
                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If
                End If
            End If

            'S.SANDEEP [ 20 APRIL 2012 ] -- END


            'Sandeep [ 17 DEC 2010 ] -- Start
            If mdtTranTable IsNot Nothing Then
                Dim dtRow() As DataRow = mdtTranTable.Select("employeeunkid = " & _Employeeunkid)
                If dtRow.Length > 0 Then
                    objCashDenome._PeriodId = mintPeriodunkid
                    With objCashDenome
                        ._FormName = mstrFormName
                        ._LoginEmployeeunkid = mintLogEmployeeUnkid
                        ._ClientIP = mstrClientIP
                        ._HostName = mstrHostName
                        ._FromWeb = mblnIsWeb
                        ._AuditUserId = mintAuditUserId
._CompanyUnkid = mintCompanyUnkid
                        ._AuditDate = mdtAuditDate
                    End With
                    If objCashDenome.Insert(_Employeeunkid, mintPaymenttranunkid, dtRow(0), intYearUnkid, mintUserunkid, objDataOperation) = False Then
                        'Sohail (21 Aug 2015) -- End
                        objDataOperation.ReleaseTransaction(False)
                        Return False
                    End If
                    'S.SANDEEP [ 04 DEC 2013 ] -- END
                End If
            End If
            'Sandeep [ 17 DEC 2010 ] -- End 


            'Sohail (11 Nov 2010) -- Start

            'Anjan (11 Jun 2011)-Start
            'Issue : In main table entry was get inserted if there any error occurs at time of insertion in AT table.
            'Call InsertAuditTrailForPaymentTran(objDataOperation, 1)
            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'If InsertAuditTrailForPaymentTran(objDataOperation, 1) = False Then
            If InsertAuditTrailForPaymentTran(objDataOperation, 1, dtCurrentDateAndTime) = False Then
                'Sohail (21 Aug 2015) -- End
                objDataOperation.ReleaseTransaction(False)
                Return False
            End If
            'Anjan (11 Jun 2011)-End

            'Sohail (11 Nov 2010) -- End

            Dim blnFlag As Boolean = False

            Select Case mintPaymentrefid
                Case enPaymentRefId.LOAN, enPaymentRefId.ADVANCE
                    Select Case mintPaymentTypeId
                        Case enPayTypeId.PAYMENT
                            objLoan_Advance._Loanadvancetranunkid = mintPayreftranunkid
                            'Sohail (07 May 2015) -- Start
                            'Enhancement - New Loan Redesign with allow to change interest rate, tenure, topup, receipt.
                            'If objLoan_Advance._Balance_Amount = 0 Then
                            If objLoan_Advance._Balance_AmountPaidCurrency = 0 Then
                                'Sohail (07 May 2015) -- End
                                If objLoan_Advance._Isloan = True Then
                                    'Sohail (07 May 2015) -- Start
                                    'Enhancement - New Loan Redesign with allow to change interest rate, tenure, topup, receipt.
                                    objLoan_Advance._Balance_Amount = objLoan_Advance._Basecurrency_amount
                                    objLoan_Advance._Balance_AmountPaidCurrency = objLoan_Advance._Loan_Amount
                                    'Sohail (07 May 2015) -- End
                                Else
                                    'Sohail (07 May 2015) -- Start
                                    'Enhancement - New Loan Redesign with allow to change interest rate, tenure, topup, receipt.
                                    'objLoan_Advance._Balance_Amount = objLoan_Advance._Advance_Amount
                                    objLoan_Advance._Balance_Amount = objLoan_Advance._Basecurrency_amount
                                    objLoan_Advance._Balance_AmountPaidCurrency = objLoan_Advance._Advance_Amount
                                    'Sohail (07 May 2015) -- End
                                End If

                                'objLoan_Advance._ClientIP = mstrClientIP
                                'objLoan_Advance._WebFormName = mstrWebFormName
                                'objLoan_Advance._HostName = mstrHostName

                                objLoan_Advance._ClientIP = mstrClientIP
                                objLoan_Advance._FormName = mstrFormName
                                objLoan_Advance._HostName = mstrHostName
                                objLoan_Advance._AuditUserId = mintAuditUserId
objLoan_Advance._CompanyUnkid = mintCompanyUnkid
                                objLoan_Advance._AuditDate = mdtAuditDate
                                objLoan_Advance._FromWeb = mblnIsWeb
                                objLoan_Advance._LoginEmployeeUnkid = mintLogEmployeeUnkid

                                'Nilay (05-May-2016) -- Start
                                'blnFlag = objLoan_Advance.Update(, objDataOperation)
                                blnFlag = objLoan_Advance.Update(objDataOperation)
                                'Nilay (05-May-2016) -- End

                                'S.SANDEEP [ 04 DEC 2013 ] -- END
                                'Sohail (05 Mar 2014) -- Start
                                'Enhancement - Error Message not displayed
                                'If blnFlag = False Then objDataOperation.ReleaseTransaction(False)
                                If blnFlag = False AndAlso objLoan_Advance._Message <> "" Then
                                    exForce = New Exception(objLoan_Advance._Message)
                                    Throw exForce
                                    objDataOperation.ReleaseTransaction(False)
                                    Return False
                                End If
                                'Sohail (05 Mar 2014) -- End


                                'Sohail (25 Mar 2015) -- Start
                                'B.C.Patel & Co Enhancement - New Custom CSV and XLS EFT for Advance module Payment and distribute payment as per employee banks.
                                If Not (mintPaymentmodeid = enPaymentMode.CASH OrElse mintPaymentmodeid = enPaymentMode.CASH_AND_CHEQUE) Then
                                    Dim objEmpBank As New clsEmployeeBanks

                                    'Sohail (07 May 2015) -- Start
                                    'Enhancement - New Loan Redesign with allow to change interest rate, tenure, topup, receipt.
                                    'Dim objPeriod As New clscommom_period_Tran
                                    'objPeriod._Periodunkid = mintPeriodunkid
                                    'Sohail (07 May 2015) -- End

                                    'Sohail (21 Aug 2015) -- Start
                                    'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                                    'Dim ds As DataSet = objEmpBank.GetList("Banks", mintEmployeeunkid.ToString, objPeriod._End_Date, , "end_date DESC, EmpName, priority", mintPeriodunkid)
                                    Dim ds As DataSet = objEmpBank.GetList(strDatabaseName, mintUserunkid, intYearUnkid, intCompanyunkid, objPeriod._Start_Date, objPeriod._End_Date, strUserAccessModeSetting, True, blnIsIncludeInactiveEmp, "Banks", True, , mintEmployeeunkid.ToString, objPeriod._End_Date, "end_date DESC, EmpName, priority")
                                    'Sohail (21 Aug 2015) -- End
                                    dtTran = New DataView(ds.Tables(0)).ToTable


                                    Dim lstPerc As Integer = (From p In dtTran Where CInt(p.Item("modeid")) = enPaymentBy.Percentage).Count()
                                    If lstPerc > 0 Then

                                        For Each dsRow As DataRow In dtTran.Rows
                                            If CDec(dsRow.Item("percentage").ToString) > 0 Then
                                                Dim objEmpSalary As New clsEmpSalaryTran
                                                With objEmpSalary
                                                    ._Paymenttranunkid = mintPaymenttranunkid
                                                    ._Paymentdate = mdtPaymentdate
                                                    ._Employeeunkid = mintEmployeeunkid
                                                    ._Empbanktranid = CInt(dsRow.Item("empbanktranunkid").ToString)
                                                    ._Amount = mdecAmount * CDec(dsRow.Item("percentage").ToString) / 100
                                                    'Sohail (21 Aug 2015) -- Start
                                                    'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                                                    '._Userunkid = User._Object._Userunkid
                                                    ._Userunkid = mintUserunkid
                                                    'Sohail (21 Aug 2015) -- End
                                                    ._Isvoid = False
                                                    ._Voiduserunkid = 0
                                                    ._Voiddatetime = Nothing
                                                    ._Voidreason = ""
                                                    ._Basecurrencyid = mintBasecurrencyid
                                                    ._Baseexchangerate = mdecBaseexchangerate
                                                    ._Paidcurrencyid = mintPaidcurrencyid
                                                    ._Expaidrate = mdecExpaidrate
                                                    ._Expaidamt = mdecExpaidamt * CDec(dsRow.Item("percentage").ToString) / 100
                                                    ._FormName = mstrFormName
                                                    ._LoginEmployeeUnkid = mintLogEmployeeUnkid
                                                    ._ClientIP = mstrClientIP
                                                    ._HostName = mstrHostName
                                                    ._FromWeb = mblnIsWeb
                                                    ._AuditUserId = mintAuditUserId
._CompanyUnkid = mintCompanyUnkid
                                                    ._AuditDate = mdtAuditDate
                                                    If .Insert() = False Then
                                                        objDataOperation.ReleaseTransaction(False)
                                                        Return False
                                                    End If
                                                    objEmpSalary = Nothing
                                                End With
                                            End If
                                        Next

                                    Else
                                        Dim lst As List(Of DataRow) = (From p In dtTran Where CInt(p.Item("modeid")) = enPaymentBy.Value).DefaultIfEmpty.ToList
                                        Dim decPriorityTotal As Decimal = (From p In lst Select CDec(p.Item("Amount"))).DefaultIfEmpty.Sum

                                        If decPriorityTotal < mdecAmount Then
                                            Dim intMinPriority As Integer = (From p In lst Select CInt(p.Item("priority"))).DefaultIfEmpty.Min
                                            Dim decOtherTotal As Decimal = (From p In lst Where CInt(p.Item("priority")) <> intMinPriority Select CDec(p.Item("Amount"))).DefaultIfEmpty.Sum
                                            Dim objEmpSalary As clsEmpSalaryTran

                                            For Each dsRow As DataRow In lst

                                                objEmpSalary = New clsEmpSalaryTran
                                                With objEmpSalary
                                                    ._Paymenttranunkid = mintPaymenttranunkid
                                                    ._Paymentdate = mdtPaymentdate
                                                    ._Employeeunkid = mintEmployeeunkid
                                                    ._Empbanktranid = CInt(dsRow.Item("empbanktranunkid").ToString)
                                                    ._Userunkid = mintUserunkid
                                                    ._Isvoid = False
                                                    ._Voiduserunkid = 0
                                                    ._Voiddatetime = Nothing
                                                    ._Voidreason = ""
                                                    ._Basecurrencyid = mintBasecurrencyid
                                                    ._Baseexchangerate = mdecBaseexchangerate
                                                    ._Paidcurrencyid = mintPaidcurrencyid
                                                    ._Expaidrate = mdecExpaidrate

                                                    ._ClientIP = mstrClientIP
                                                    ._FormName = mstrFormName
                                                    ._HostName = mstrHostName
                                                    ._AuditUserId = mintAuditUserId
._CompanyUnkid = mintCompanyUnkid
                                                    ._AuditDate = mdtAuditDate
                                                    ._FromWeb = mblnIsWeb
                                                    ._LoginEmployeeUnkid = mintLogEmployeeUnkid

                                                    If CInt(dsRow.Item("priority")) = intMinPriority Then
                                                        ._Amount = (mdecAmount - decOtherTotal)
                                                        If mdecBaseexchangerate <> 0 Then
                                                            ._Expaidamt = (mdecAmount - decOtherTotal) * mdecExpaidrate / mdecBaseexchangerate
                                                        Else
                                                            ._Expaidamt = (mdecAmount - decOtherTotal) * mdecExpaidrate
                                                        End If
                                                    Else
                                                        ._Amount = CDec(dsRow.Item("Amount"))
                                                        If mdecBaseexchangerate <> 0 Then
                                                            ._Expaidamt = CDec(dsRow.Item("Amount")) * mdecExpaidrate / mdecBaseexchangerate
                                                        Else
                                                            ._Expaidamt = CDec(dsRow.Item("Amount")) * mdecExpaidrate
                                                        End If
                                                    End If

                                                    If .Insert() = False Then
                                                        objDataOperation.ReleaseTransaction(False)
                                                        Return False
                                                    End If
                                                End With

                                            Next
                                            objEmpSalary = Nothing
                                        Else

                                            Dim objEmpSalary As clsEmpSalaryTran
                                            Dim decBalance As Decimal = mdecAmount

                                            For Each dsRow As DataRow In lst

                                                If decBalance <= 0 Then Exit For

                                                objEmpSalary = New clsEmpSalaryTran
                                                With objEmpSalary
                                                    ._Paymenttranunkid = mintPaymenttranunkid
                                                    ._Paymentdate = mdtPaymentdate
                                                    ._Employeeunkid = mintEmployeeunkid
                                                    ._Empbanktranid = CInt(dsRow.Item("empbanktranunkid").ToString)
                                                    ._Userunkid = mintUserunkid
                                                    ._Isvoid = False
                                                    ._Voiduserunkid = 0
                                                    ._Voiddatetime = Nothing
                                                    ._Voidreason = ""
                                                    ._Basecurrencyid = mintBasecurrencyid
                                                    ._Baseexchangerate = mdecBaseexchangerate
                                                    ._Paidcurrencyid = mintPaidcurrencyid
                                                    ._Expaidrate = mdecExpaidrate

                                                    ._ClientIP = mstrClientIP
                                                    ._FormName = mstrFormName
                                                    ._HostName = mstrHostName
                                                    ._AuditUserId = mintAuditUserId
._CompanyUnkid = mintCompanyUnkid
                                                    ._AuditDate = mdtAuditDate
                                                    ._FromWeb = mblnIsWeb
                                                    ._LoginEmployeeUnkid = mintLogEmployeeUnkid

                                                    If decBalance > CDec(dsRow.Item("Amount")) Then
                                                        ._Amount = CDec(dsRow.Item("Amount"))
                                                        If mdecBaseexchangerate <> 0 Then
                                                            ._Expaidamt = CDec(dsRow.Item("Amount")) * mdecExpaidrate / mdecBaseexchangerate
                                                        Else
                                                            ._Expaidamt = CDec(dsRow.Item("Amount")) * mdecExpaidrate
                                                        End If
                                                    Else
                                                        ._Amount = decBalance
                                                        If mdecBaseexchangerate <> 0 Then
                                                            ._Expaidamt = decBalance * mdecExpaidrate / mdecBaseexchangerate
                                                        Else
                                                            ._Expaidamt = decBalance * mdecExpaidrate
                                                        End If
                                                    End If

                                                    decBalance -= CDec(dsRow.Item("Amount"))

                                                    If .Insert() = False Then
                                                        objDataOperation.ReleaseTransaction(False)
                                                        Return False
                                                    End If
                                                End With
                                            Next
                                        End If
                                    End If
                                    objEmpBank = Nothing
                                End If
                                'Sohail (25 Mar 2015) -- End
                            End If
                        Case enPayTypeId.RECEIVED

                            'Sohail (07 May 2015) -- Start
                            'Enhancement - New Loan Redesign with allow to change interest rate, tenure, topup, receipt.
                            Dim decCFAmt As Decimal = 0
                            Dim decCFAmtPaidCurrency As Decimal = 0
                            If mintPaymentrefid = enPaymentRefId.LOAN Then
                                Dim decBalanceAmount As Decimal = 0
                                Dim decBalanceAmountPaidCurrency As Decimal = 0
                                Dim intDaysDiff As Integer = 0
                                Dim decIntRate As Decimal = 0
                                Dim decTotPMT As Decimal = 0
                                Dim decTotPMTPaidCurrency As Decimal = 0
                                Dim decTotIntAmt As Decimal = 0
                                Dim decTotIntAmtPaidCurrency As Decimal = 0
                                Dim decTotPrincipalAmt As Decimal = 0
                                Dim decTotPrincipalAmtPaidCurrency As Decimal = 0
                                Dim intLoanSchemeUnkId As Integer = 0
                                Dim dtLoanEndDate As Date = DateAndTime.Today.Date
                                Dim intPeriodID As Integer = 0

                                Dim intNextEffectiveDays As Integer = 0
                                Dim intNextEffectiveMonths As Integer = 0 'Sohail (15 Dec 2015)
                                If dsLoan.Tables("List").Rows.Count > 0 Then
                                    With dsLoan.Tables("List").Rows(0)
                                        decBalanceAmount = CDec(.Item("BalanceAmount"))
                                        decBalanceAmountPaidCurrency = CDec(.Item("BalanceAmountPaidCurrency"))
                                        intDaysDiff = CInt(.Item("DaysDiff"))
                                        decIntRate = CDec(.Item("interest_rate"))
                                        decTotPMT = CDec(.Item("TotPMTAmount"))
                                        decTotPMTPaidCurrency = CDec(.Item("TotPMTAmountPaidCurrency"))
                                        decTotIntAmt = CDec(.Item("TotInterestAmount"))
                                        decTotIntAmtPaidCurrency = CDec(.Item("TotInterestAmountPaidCurrency"))
                                        decTotPrincipalAmt = CDec(.Item("TotPrincipalAmount"))
                                        decTotPrincipalAmtPaidCurrency = CDec(.Item("TotPrincipalAmountPaidCurrency"))
                                        decCFAmt = CDec(.Item("LastProjectedBalance"))
                                        decCFAmtPaidCurrency = CDec(.Item("LastProjectedBalancePaidCurrency"))
                                        intNextEffectiveDays = CInt(.Item("nexteffective_days"))
                                        intNextEffectiveMonths = CInt(.Item("nexteffective_months")) 'Sohail (15 Dec 2015)
                                        intLoanSchemeUnkId = CInt(.Item("loanschemeunkid"))
                                        dtLoanEndDate = eZeeDate.convertDate(.Item("LoanEndDate").ToString)

                                        'S.SANDEEP [04 JUN 2015] -- START
                                        'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                                        'intPeriodID = (New clsMasterData).getCurrentPeriodID(enModuleReference.Payroll, mdtPaymentdate.AddDays(-1), 0, FinancialYear._Object._YearUnkid)
                                        intPeriodID = (New clsMasterData).getCurrentPeriodID(enModuleReference.Payroll, mdtPaymentdate.AddDays(-1), intYearUnkid, 0)
                                        'S.SANDEEP [04 JUN 2015] -- END

                                        If intPeriodID <= 0 Then intPeriodID = CInt(.Item("periodunkid"))

                                        decCFAmtPaidCurrency -= mdecExpaidamt
                                        decCFAmt = decCFAmtPaidCurrency / CDec(.Item("exchange_rate"))

                                        strQ = "INSERT  INTO lnloan_balance_tran " & _
                                                        "( loanschemeunkid  " & _
                                                        ", transaction_periodunkid " & _
                                                        ", transactiondate " & _
                                                        ", periodunkid " & _
                                                        ", end_date " & _
                                                        ", loanadvancetranunkid " & _
                                                        ", employeeunkid " & _
                                                        ", payrollprocesstranunkid " & _
                                                        ", paymenttranunkid " & _
                                                        ", bf_amount " & _
                                                        ", bf_amountpaidcurrency " & _
                                                        ", amount " & _
                                                        ", amountpaidcurrency " & _
                                                        ", cf_amount " & _
                                                        ", cf_amountpaidcurrency " & _
                                                        ", userunkid " & _
                                                        ", isvoid " & _
                                                        ", voiduserunkid " & _
                                                        ", voiddatetime " & _
                                                        ", voidreason " & _
                                                        ", lninteresttranunkid " & _
                                                        ", lnemitranunkid " & _
                                                        ", lntopuptranunkid " & _
                                                        ", days " & _
                                                        ", bf_balance " & _
                                                        ", bf_balancepaidcurrency " & _
                                                        ", principal_amount " & _
                                                        ", principal_amountpaidcurrency " & _
                                                        ", topup_amount " & _
                                                        ", topup_amountpaidcurrency " & _
                                                        ", repayment_amount " & _
                                                        ", repayment_amountpaidcurrency " & _
                                                        ", interest_rate " & _
                                                        ", interest_amount " & _
                                                        ", interest_amountpaidcurrency " & _
                                                        ", cf_balance " & _
                                                        ", cf_balancepaidcurrency " & _
                                                        ", isonhold " & _
                                                        ", isreceipt  " & _
                                                        ", nexteffective_days " & _
                                                        ", nexteffective_months " & _
                                                        ", exchange_rate " & _
                                                        ", isbrought_forward " & _
                                                        ", loanstatustranunkid " & _
                                                        ") " & _
                                                "VALUES  ( " & intLoanSchemeUnkId & "  " & _
                                                        ", " & mintPaymentDate_Periodunkid & " " & _
                                                        ", '" & Format(mdtPaymentdate, "yyyyMMdd HH:mm:ss") & "' " & _
                                                        ", " & intPeriodID & " " & _
                                                        ", '" & Format(mdtPaymentdate.AddDays(-1), "yyyyMMdd HH:mm:ss") & "' " & _
                                                        ", " & mintPayreftranunkid & " " & _
                                                        ", " & mintEmployeeunkid & " " & _
                                                        ", 0 " & _
                                                        ", " & mintPaymenttranunkid & " " & _
                                                        ", " & decCFAmt & " " & _
                                                        ", " & decCFAmtPaidCurrency & " " & _
                                                        ", " & decTotPMT & " " & _
                                                        ", " & decTotPMTPaidCurrency & " " & _
                                                        ", " & decCFAmt & " " & _
                                                        ", " & decCFAmtPaidCurrency & " " & _
                                                        ", " & mintUserunkid & " " & _
                                                        ", 0 " & _
                                                        ", -1 " & _
                                                        ", NULL " & _
                                                        ", '' " & _
                                                        ", -1 " & _
                                                        ", -1 " & _
                                                        ", -1 " & _
                                                        ", " & intDaysDiff & " " & _
                                                        ", " & decBalanceAmount & " " & _
                                                        ", " & decBalanceAmountPaidCurrency & " " & _
                                                        ", " & decTotPrincipalAmt & " " & _
                                                        ", " & decTotPrincipalAmtPaidCurrency & " " & _
                                                        ", 0 " & _
                                                        ", 0 " & _
                                                        ", " & mdecAmount & " " & _
                                                        ", " & mdecExpaidamt & " " & _
                                                        ", " & decIntRate & " " & _
                                                        ", " & decTotIntAmt & " " & _
                                                        ", " & decTotIntAmtPaidCurrency & " " & _
                                                        ", " & decCFAmt & " " & _
                                                        ", " & decCFAmtPaidCurrency & " " & _
                                                        ", " & CInt(Int(.Item("isonhold"))) & " " & _
                                                        ", 1 " & _
                                                        ", " & DateDiff(DateInterval.Day, mdtPaymentdate, dtLoanEndDate.AddDays(1)) & " " & _
                                                        ", " & DateDiff(DateInterval.Month, mdtPaymentdate, dtLoanEndDate.AddDays(1)) & " " & _
                                                        ", " & CDec(.Item("exchange_rate")) & " " & _
                                                        ", 0 " & _
                                                        ", 0 " & _
                                                        ") "
                                        'Sohail (15 Dec 2015) - [loanstatustranunkid,nexteffective_months]

                                        dsList = objDataOperation.ExecQuery(strQ, "List")

                                        If objDataOperation.ErrorMessage <> "" Then
                                            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                            Throw exForce
                                        End If

                                    End With
                                End If
                            Else
                                objLoan_Advance._Loanadvancetranunkid = mintPayreftranunkid
                                decCFAmt = objLoan_Advance._Balance_Amount - mdecAmount
                                decCFAmtPaidCurrency = objLoan_Advance._Balance_AmountPaidCurrency - mdecExpaidamt
                            End If
                            'Sohail (07 May 2015) -- End

                            If mblnIsFromStatus = True Then
                                Dim StrStatus() As String = mstrStatus_Data.Split("|")
                                objLoanStatusTran._Isvoid = False 'StrStatus(0)   'IsVoid
                                objLoanStatusTran._Loanadvancetranunkid = StrStatus(1)    'LoanAdvanceTranUnkid
                                objLoanStatusTran._Remark = StrStatus(2) 'Remark
                                objLoanStatusTran._Settle_Amount = mdecAmount
                                'Nilay (20-Sept-2016) -- Start
                                'Enhancement : Cancel feature for approved but not assigned loan application
                                'objLoanStatusTran._Staus_Date = IIf(CDate(StrStatus(4)) <> Nothing, CDate(StrStatus(4)), mdtPaymentdate)  'StatusDate
                                objLoanStatusTran._Staus_Date = IIf(CDate(StrStatus(4)) <> Nothing, CDate(StrStatus(4)), dtCurrentDateAndTime)
                                'Nilay (20-Sept-2016) -- End

                                objLoanStatusTran._Voiddatetime = Nothing 'StrStatus(5) 'VoidDateTime
                                objLoanStatusTran._Voiduserunkid = -1 'StrStatus(6)   'VoidUserunkid
                                objLoanStatusTran._Statusunkid = StrStatus(7)  'SatausUnkid
                                'S.SANDEEP [15 JUN 2015] -- START
                                objLoanStatusTran._Periodunkid = StrStatus(8)  'PeriodUnkid
                                'S.SANDEEP [15 JUN 2015] -- END

                                'Nilay (01-Apr-2016) -- Start
                                'ENHANCEMENT - Approval Process in Loan Other Operations
                                objLoanStatusTran._Userunkid = xUserUnkid
                                'Nilay (01-Apr-2016) -- End

                                'objLoanStatusTran._ClientIP = mstrClientIP
                                'objLoanStatusTran._WebFormName = mstrWebFrmName
                                'objLoanStatusTran._HostName = mstrHostName

                                objLoanStatusTran._ClientIP = mstrClientIP
                                objLoanStatusTran._FormName = mstrFormName
                                objLoanStatusTran._HostName = mstrHostName
                                objLoanStatusTran._AuditUserId = mintAuditUserId
objLoanStatusTran._CompanyUnkid = mintCompanyUnkid
                                objLoanStatusTran._AuditDate = mdtAuditDate
                                objLoanStatusTran._FromWeb = mblnIsWeb
                                objLoanStatusTran._LoginEmployeeunkid = mintLogEmployeeUnkid

                                'Sohail (15 Dec 2015) -- Start
                                'Enhancement - New Loan Calculation type Reducing balance with Fixed Principal EMI and new interest calculation Monthly apart from Daily for KBC.
                                'blnFlag = objLoanStatusTran.Insert(objDataOperation)

                                'Nilay (25-Mar-2016) -- Start
                                'blnFlag = objLoanStatusTran.Insert(strDatabaseName, intYearUnkid, objDataOperation, True)
                                'Sohail (30 Apr 2019) -- Start
                                'BBLMS Issue - Support Issue Id # 0003781 - 74.1 - Payslip global payment takes hours to process help needed urgently.
                                'blnFlag = objLoanStatusTran.Insert(strDatabaseName, xUserUnkid, intYearUnkid, intCompanyunkid, xPeriodStart, xPeriodEnd, _
                                '                                   strUserAccessModeSetting, xOnlyApproved, objDataOperation, True)
                                blnFlag = objLoanStatusTran.Insert(strDatabaseName, xUserUnkid, intYearUnkid, intCompanyunkid, xPeriodStart, xPeriodEnd, _
                                                                   strUserAccessModeSetting, xOnlyApproved, objDataOperation, True, mintEmployeeunkid)
                                'Sohail (30 Apr 2019) -- End
                                'Nilay (25-Mar-2016) -- End

                                'Sohail (15 Dec 2015) -- End

                                'S.SANDEEP [ 04 DEC 2013 ] -- END

                                'Sohail (05 Mar 2014) -- Start
                                'Enhancement - Error Message not displayed
                                'If blnFlag = False Then objDataOperation.ReleaseTransaction(False)
                                If blnFlag = False AndAlso objLoanStatusTran._Message <> "" Then
                                    exForce = New Exception(objLoanStatusTran._Message)
                                    Throw exForce
                                    objDataOperation.ReleaseTransaction(False)
                                    Return False
                                End If
                                'Sohail (05 Mar 2014) -- End

                            End If
                            objLoan_Advance._Loanadvancetranunkid = mintPayreftranunkid
                            'Sohail (07 May 2015) -- Start
                            'Enhancement - New Loan Redesign with allow to change interest rate, tenure, topup, receipt.
                            'objLoan_Advance._Balance_Amount = objLoan_Advance._Balance_Amount - mdecAmount
                            objLoan_Advance._Balance_Amount = decCFAmt
                            objLoan_Advance._Balance_AmountPaidCurrency = decCFAmtPaidCurrency
                            'Sohail (07 May 2015) -- End

                            'Sohail (07 May 2015) -- Start
                            'Enhancement - New Loan Redesign with allow to change interest rate, tenure, topup, receipt.
                            'If objLoan_Advance._Balance_Amount = 0 Then
                            'Hemant (04 June 2019) -- Start
                            'ISSUE/ENHANCEMENT : The employee loan still gets deducted even after loan tenure is over
                            'If decCFAmtPaidCurrency <= 0.01 Then
                            'Hemant (08 Nov 2019) -- Start
                            'ISSUE/ENHANCEMENT#4271(TUJIJENGE- TZ) - Not able to close Payroll period due to Loan deduction mismatch.
                            'If decCFAmtPaidCurrency <= 0.1 Then
                            If decCFAmtPaidCurrency <= 1 Then
                                'Hemant (08 Nov 2019) -- End
                                'Hemant (04 June 2019) -- End
                                'Sohail (07 May 2015) -- End
                                objLoan_Advance._LoanStatus = 4
                                objLoanStatusTran._Loanadvancetranunkid = objLoan_Advance._Loanadvancetranunkid
                                objLoanStatusTran._Isvoid = False
                                objLoanStatusTran._Voiddatetime = Nothing
                                objLoanStatusTran._Voiduserunkid = -1
                                objLoanStatusTran._Statusunkid = 4
                                'Nilay (20-Sept-2016) -- Start
                                'Enhancement : Cancel feature for approved but not assigned loan application
                                'objLoanStatusTran._Staus_Date = mdtPaymentdate
                                objLoanStatusTran._Staus_Date = dtCurrentDateAndTime
                                'Nilay (20-Sept-2016) -- End

                                'Sohail (15 Dec 2015) -- Start
                                'Enhancement - New Loan Calculation type Reducing balance with Fixed Principal EMI and new interest calculation Monthly apart from Daily for KBC.
                                'objLoanStatusTran._Periodunkid = mintPeriodunkid 'Sohail (07 May 2015)
                                objLoanStatusTran._Periodunkid = mintPaymentDate_Periodunkid
                                'Sohail (15 Dec 2015) -- End

                                'Nilay (01-Apr-2016) -- Start
                                'ENHANCEMENT - Approval Process in Loan Other Operations
                                objLoanStatusTran._Userunkid = xUserUnkid
                                'Nilay (01-Apr-2016) -- End

                                'objLoanStatusTran._ClientIP = mstrClientIP
                                'objLoanStatusTran._WebFormName = mstrWebFrmName
                                'objLoanStatusTran._HostName = mstrHostName

                                objLoanStatusTran._ClientIP = mstrClientIP
                                objLoanStatusTran._FormName = mstrFormName
                                objLoanStatusTran._HostName = mstrHostName
                                objLoanStatusTran._AuditDate = mdtAuditDate
                                objLoanStatusTran._AuditUserId = mintAuditUserId
objLoanStatusTran._CompanyUnkid = mintCompanyUnkid
                                objLoanStatusTran._FromWeb = mblnIsWeb
                                objLoanStatusTran._LoginEmployeeunkid = mintLogEmployeeUnkid

                                'Sohail (15 Dec 2015) -- Start
                                'Enhancement - New Loan Calculation type Reducing balance with Fixed Principal EMI and new interest calculation Monthly apart from Daily for KBC.
                                'blnFlag = objLoanStatusTran.Insert(objDataOperation)

                                'Nilay (25-Mar-2016) -- Start
                                'blnFlag = objLoanStatusTran.Insert(strDatabaseName, intYearUnkid, objDataOperation, True)
                                'Sohail (30 Apr 2019) -- Start
                                'BBLMS Issue - Support Issue Id # 0003781 - 74.1 - Payslip global payment takes hours to process help needed urgently.
                                'blnFlag = objLoanStatusTran.Insert(strDatabaseName, xUserUnkid, intYearUnkid, intCompanyunkid, xPeriodStart, xPeriodEnd, _
                                '                                   strUserAccessModeSetting, xOnlyApproved, objDataOperation, True)
                                blnFlag = objLoanStatusTran.Insert(strDatabaseName, xUserUnkid, intYearUnkid, intCompanyunkid, xPeriodStart, xPeriodEnd, _
                                                                   strUserAccessModeSetting, xOnlyApproved, objDataOperation, True, mintEmployeeunkid)
                                'Sohail (30 Apr 2019) -- End
                                'Nilay (25-Mar-2016) -- End

                                'Sohail (15 Dec 2015) -- End

                                'S.SANDEEP [ 04 DEC 2013 ] -- END

                                'Sohail (05 Mar 2014) -- Start
                                'Enhancement - Error Message not displayed
                                'If blnFlag = False Then objDataOperation.ReleaseTransaction(False)
                                If blnFlag = False AndAlso objLoanStatusTran._Message <> "" Then
                                    exForce = New Exception(objLoanStatusTran._Message)
                                    Throw exForce
                                    objDataOperation.ReleaseTransaction(False)
                                    Return False
                                End If
                                'Sohail (05 Mar 2014) -- End
                            End If

                            If mblnIsFromStatus = True Then
                                objLoan_Advance._LoanStatus = objLoanStatusTran._Statusunkid
                            End If

                            'objLoan_Advance._ClientIP = mstrClientIP
                            'objLoan_Advance._WebFormName = mstrWebFormName
                            'objLoan_Advance._HostName = mstrHostName

                            objLoan_Advance._ClientIP = mstrClientIP
                            objLoan_Advance._FormName = mstrFormName
                            objLoan_Advance._HostName = mstrHostName
                            objLoan_Advance._AuditUserId = mintAuditUserId
objLoan_Advance._CompanyUnkid = mintCompanyUnkid
                            objLoan_Advance._AuditDate = mdtAuditDate
                            objLoan_Advance._FromWeb = mblnIsWeb
                            objLoan_Advance._LoginEmployeeUnkid = mintLogEmployeeUnkid

                            'S.SANDEEP [ 04 DEC 2013 ] -- START
                            'blnFlag = objLoan_Advance.Update

                            'Nilay (05-May-2016) -- Start
                            'blnFlag = objLoan_Advance.Update(, objDataOperation)
                            blnFlag = objLoan_Advance.Update(objDataOperation)
                            'Nilay (05-May-2016) -- End

                            'S.SANDEEP [ 04 DEC 2013 ] -- END
                            'Sohail (05 Mar 2014) -- Start
                            'Enhancement - Error Message not displayed
                            'If blnFlag = False Then objDataOperation.ReleaseTransaction(False)
                            If blnFlag = False AndAlso objLoan_Advance._Message <> "" Then
                                exForce = New Exception(objLoan_Advance._Message)
                                Throw exForce
                                objDataOperation.ReleaseTransaction(False)
                                Return False
                            End If
                            'Sohail (05 Mar 2014) -- End
                    End Select
                Case enPaymentRefId.PAYSLIP

                    'Sohail (16 Oct 2010) -- Start
                    'Sohail (11 Sep 2012) -- Start
                    'TRA - ENHANCEMENT
                    'If mintPaymentmodeid <> 1 Then '1 = CASH
                    If Not (mintPaymentmodeid = enPaymentMode.CASH OrElse mintPaymentmodeid = enPaymentMode.CASH_AND_CHEQUE) Then
                        'Sohail (11 Sep 2012) -- End
                        Dim objEmpBank As New clsEmployeeBanks

                        'Sohail (25 Apr 2014) -- Start
                        'Enhancement - Employee Bank Details Period Wise.
                        'objEmpBank._Employeeunkid = mintEmployeeunkid
                        'dtTran = objEmpBank._DataTable
                        'Sohail (07 May 2015) -- Start
                        'Enhancement - New Loan Redesign with allow to change interest rate, tenure, topup, receipt.
                        'Dim objPeriod As New clscommom_period_Tran
                        'objPeriod._Periodunkid = mintPeriodunkid
                        'Sohail (07 May 2015) -- End

                        'Sohail (21 Aug 2015) -- Start
                        'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                        'Dim ds As DataSet = objEmpBank.GetList("Banks", mintEmployeeunkid.ToString, objPeriod._End_Date, , "end_date DESC, EmpName, priority", mintPeriodunkid)
                        Dim ds As DataSet = objEmpBank.GetList(strDatabaseName, mintUserunkid, intYearUnkid, intCompanyunkid, objPeriod._Start_Date, objPeriod._End_Date, strUserAccessModeSetting, True, blnIsIncludeInactiveEmp, "Banks", True, "", mintEmployeeunkid.ToString, objPeriod._End_Date, "end_date DESC, EmpName, priority")
                        'Sohail (21 Aug 2015) -- End
                        dtTran = New DataView(ds.Tables(0)).ToTable
                        'Sohail (25 Apr 2014) -- End


                        'Sohail (21 Apr 2014) -- Start
                        'Enhancement - Salary Distribution by Amount and bank priority.
                        Dim lstPerc As Integer = (From p In dtTran Where CInt(p.Item("modeid")) = enPaymentBy.Percentage).Count()
                        If lstPerc > 0 Then
                            'Sohail (21 Apr 2014) -- End
                        For Each dsRow As DataRow In dtTran.Rows
                            If CDec(dsRow.Item("percentage").ToString) > 0 Then
                                Dim objEmpSalary As New clsEmpSalaryTran
                                With objEmpSalary
                                    ._Paymenttranunkid = mintPaymenttranunkid
                                    ._Paymentdate = mdtPaymentdate
                                    ._Employeeunkid = mintEmployeeunkid
                                    ._Empbanktranid = CInt(dsRow.Item("empbanktranunkid").ToString)
                                    ._Amount = mdecAmount * CDec(dsRow.Item("percentage").ToString) / 100 'Sohail (11 May 2011)
                                        'Sohail (21 Aug 2015) -- Start
                                        'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                                        '._Userunkid = User._Object._Userunkid
                                        ._Userunkid = mintUserunkid
                                        'Sohail (21 Aug 2015) -- End
                                    ._Isvoid = False
                                    ._Voiduserunkid = 0
                                    ._Voiddatetime = Nothing
                                    ._Voidreason = ""
                                    'Sohail (25 May 2012) -- Start
                                    'TRA - ENHANCEMENT
                                    ._Basecurrencyid = mintBasecurrencyid
                                    ._Baseexchangerate = mdecBaseexchangerate
                                    ._Paidcurrencyid = mintPaidcurrencyid
                                    ._Expaidrate = mdecExpaidrate
                                    ._Expaidamt = mdecExpaidamt * CDec(dsRow.Item("percentage").ToString) / 100
                                        ._FormName = mstrFormName
                                        ._LoginEmployeeUnkid = mintLogEmployeeUnkid
                                        ._ClientIP = mstrClientIP
                                        ._HostName = mstrHostName
                                        ._FromWeb = mblnIsWeb
                                        ._AuditUserId = mintAuditUserId
._CompanyUnkid = mintCompanyUnkid
                                        ._AuditDate = mdtAuditDate
                                    If .Insert() = False Then
                                        objDataOperation.ReleaseTransaction(False)
                                        Return False
                                    End If
                                    objEmpSalary = Nothing
                                End With
                            End If
                        Next
                            'Sohail (21 Apr 2014) -- Start
                            'Enhancement - Salary Distribution by Amount and bank priority.
                        Else
                            Dim lst As List(Of DataRow) = (From p In dtTran Where CInt(p.Item("modeid")) = enPaymentBy.Value).DefaultIfEmpty.ToList
                            Dim decPriorityTotal As Decimal = (From p In lst Select CDec(p.Item("Amount"))).DefaultIfEmpty.Sum

                            If decPriorityTotal < mdecAmount Then
                                Dim intMinPriority As Integer = (From p In lst Select CInt(p.Item("priority"))).DefaultIfEmpty.Min
                                Dim decOtherTotal As Decimal = (From p In lst Where CInt(p.Item("priority")) <> intMinPriority Select CDec(p.Item("Amount"))).DefaultIfEmpty.Sum
                                Dim objEmpSalary As clsEmpSalaryTran

                                For Each dsRow As DataRow In lst

                                    objEmpSalary = New clsEmpSalaryTran
                                    With objEmpSalary
                                        ._Paymenttranunkid = mintPaymenttranunkid
                                        ._Paymentdate = mdtPaymentdate
                                        ._Employeeunkid = mintEmployeeunkid
                                        ._Empbanktranid = CInt(dsRow.Item("empbanktranunkid").ToString)
                                        ._Userunkid = mintUserunkid
                                        ._Isvoid = False
                                        ._Voiduserunkid = 0
                                        ._Voiddatetime = Nothing
                                        ._Voidreason = ""
                                        ._Basecurrencyid = mintBasecurrencyid
                                        ._Baseexchangerate = mdecBaseexchangerate
                                        ._Paidcurrencyid = mintPaidcurrencyid
                                        ._Expaidrate = mdecExpaidrate


                                        ._FormName = mstrFormName
                                        ._LoginEmployeeUnkid = mintLogEmployeeUnkid
                                        ._ClientIP = mstrClientIP
                                        ._HostName = mstrHostName
                                        ._FromWeb = mblnIsWeb
                                        ._AuditUserId = mintAuditUserId
._CompanyUnkid = mintCompanyUnkid
                                        ._AuditDate = mdtAuditDate


                                        If CInt(dsRow.Item("priority")) = intMinPriority Then
                                            ._Amount = (mdecAmount - decOtherTotal)
                                            If mdecBaseexchangerate <> 0 Then
                                                ._Expaidamt = (mdecAmount - decOtherTotal) * mdecExpaidrate / mdecBaseexchangerate
                                            Else
                                                ._Expaidamt = (mdecAmount - decOtherTotal) * mdecExpaidrate
                                            End If
                                        Else
                                            ._Amount = CDec(dsRow.Item("Amount"))
                                            If mdecBaseexchangerate <> 0 Then
                                                ._Expaidamt = CDec(dsRow.Item("Amount")) * mdecExpaidrate / mdecBaseexchangerate
                                            Else
                                                ._Expaidamt = CDec(dsRow.Item("Amount")) * mdecExpaidrate
                                            End If
                                        End If

                                        If .Insert() = False Then
                                            objDataOperation.ReleaseTransaction(False)
                                            Return False
                                        End If
                                    End With

                                Next
                                objEmpSalary = Nothing
                            Else

                                Dim objEmpSalary As clsEmpSalaryTran
                                Dim decBalance As Decimal = mdecAmount

                                For Each dsRow As DataRow In lst

                                    If decBalance <= 0 Then Exit For

                                    objEmpSalary = New clsEmpSalaryTran
                                    With objEmpSalary
                                        ._Paymenttranunkid = mintPaymenttranunkid
                                        ._Paymentdate = mdtPaymentdate
                                        ._Employeeunkid = mintEmployeeunkid
                                        ._Empbanktranid = CInt(dsRow.Item("empbanktranunkid").ToString)
                                        ._Userunkid = mintUserunkid
                                        ._Isvoid = False
                                        ._Voiduserunkid = 0
                                        ._Voiddatetime = Nothing
                                        ._Voidreason = ""
                                        ._Basecurrencyid = mintBasecurrencyid
                                        ._Baseexchangerate = mdecBaseexchangerate
                                        ._Paidcurrencyid = mintPaidcurrencyid
                                        ._Expaidrate = mdecExpaidrate

                                        ._FormName = mstrFormName
                                        ._LoginEmployeeUnkid = mintLogEmployeeUnkid
                                        ._ClientIP = mstrClientIP
                                        ._HostName = mstrHostName
                                        ._FromWeb = mblnIsWeb
                                        ._AuditUserId = mintAuditUserId
._CompanyUnkid = mintCompanyUnkid
                                        ._AuditDate = mdtAuditDate


                                        If decBalance > CDec(dsRow.Item("Amount")) Then
                                            ._Amount = CDec(dsRow.Item("Amount"))
                                            If mdecBaseexchangerate <> 0 Then
                                                ._Expaidamt = CDec(dsRow.Item("Amount")) * mdecExpaidrate / mdecBaseexchangerate
                                            Else
                                                ._Expaidamt = CDec(dsRow.Item("Amount")) * mdecExpaidrate
                                            End If
                                        Else
                                            ._Amount = decBalance
                                            If mdecBaseexchangerate <> 0 Then
                                                ._Expaidamt = decBalance * mdecExpaidrate / mdecBaseexchangerate
                                            Else
                                                ._Expaidamt = decBalance * mdecExpaidrate
                                            End If
                                        End If

                                        decBalance -= CDec(dsRow.Item("Amount"))

                                        If .Insert() = False Then
                                            objDataOperation.ReleaseTransaction(False)
                                            Return False
                                        End If
                                    End With
                                Next
                            End If
                        End If
                        'Sohail (21 Apr 2014) -- End
                        objEmpBank = Nothing
                    End If
                    'Sohail (16 Oct 2010) -- End

                    'Sohail (02 Jul 2012) -- Start
                    'TRA - ENHANCEMENT
                    Dim objPaymentAuthorize As New clsPayment_authorize_tran
                    With objPaymentAuthorize
                        ._Paymenttranunkid = mintPaymenttranunkid
                        'Sohail (21 Aug 2015) -- Start
                        'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                        '._Authorize_Date = ConfigParameter._Object._CurrentDateAndTime
                        ._Authorize_Date = dtCurrentDateAndTime
                        'Sohail (21 Aug 2015) -- End
                        ._Isauthorized = False
                        'Sohail (21 Aug 2015) -- Start
                        'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                        '._Userunkid = User._Object._Userunkid
                        ._Userunkid = mintUserunkid
                        'Sohail (21 Aug 2015) -- End
                        ._Isactive = True
                        ._Remarks = "" 'Sohail (27 Feb 2013)
                        ._FormName = mstrFormName
                        ._LoginEmployeeUnkid = mintLogEmployeeUnkid
                        ._ClientIP = mstrClientIP
                        ._HostName = mstrHostName
                        ._FromWeb = mblnIsWeb
                        ._AuditUserId = mintAuditUserId
._CompanyUnkid = mintCompanyUnkid
                        ._AuditDate = mdtAuditDate
                        If .Insert() = False Then
                            objDataOperation.ReleaseTransaction(False)
                            Return False
                        End If
                        objPaymentAuthorize = Nothing
                    End With
                    'Sohail (02 Jul 2012) -- End

                    'Sohail (13 Feb 2013) -- Start
                    'TRA - ENHANCEMENT
                    Dim objPaymentApproval As New clsPayment_approval_tran
                    With objPaymentApproval
                        ._Paymenttranunkid = mintPaymenttranunkid
                        ._Levelunkid = -1
                        ._Priority = -1
                        'Sohail (21 Aug 2015) -- Start
                        'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                        '._Approval_Date = ConfigParameter._Object._CurrentDateAndTime
                        ._Approval_Date = dtCurrentDateAndTime
                        'Sohail (21 Aug 2015) -- End
                        ._Statusunkid = 0
                        'Sohail (21 Aug 2015) -- Start
                        'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                        '._Userunkid = User._Object._Userunkid
                        ._Userunkid = mintUserunkid
                        'Sohail (21 Aug 2015) -- End
                        ._Isvoid = False
                        ._Voiduserunkid = -1
                        ._Voidreason = ""
                        ._Disapprovalreason = ""
                        ._Remarks = "" 'Sohail (27 Feb 2013)
                        ._FormName = mstrFormName
                        ._LoginEmployeeUnkid = mintLogEmployeeUnkid
                        ._ClientIP = mstrClientIP
                        ._HostName = mstrHostName
                        ._FromWeb = mblnIsWeb
                        ._AuditUserId = mintAuditUserId
._CompanyUnkid = mintCompanyUnkid
                        ._AuditDate = mdtAuditDate
                        If .Insert() = False Then
                            objDataOperation.ReleaseTransaction(False)
                            Return False
                        End If
                        objPaymentApproval = Nothing
                    End With
                    'Sohail (13 Feb 2013) -- End

                    Dim objpayrollProcess As New clsPayrollProcessTran
                    'Sohail (21 Aug 2015) -- Start
                    'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                    'Dim dsProcess As DataSet = objpayrollProcess.GetList("List", mintEmployeeunkid, mintPeriodunkid, mintPayreftranunkid)
                    Dim dsProcess As DataSet = objpayrollProcess.GetList(strDatabaseName, mintUserunkid, intYearUnkid, intCompanyunkid, xPeriodStart, xPeriodEnd, strUserAccessModeSetting, xOnlyApproved, blnIsIncludeInactiveEmp, "List", mintEmployeeunkid, mintPeriodunkid, mintPayreftranunkid, blnApplyUserAccessFilter, strFilter)
                    'Sohail (21 Aug 2015) -- End

                    If dsProcess IsNot Nothing Then
                        If dsProcess.Tables(0).Rows.Count > 0 Then
                            For Each drRow As DataRow In dsProcess.Tables(0).Rows
                                If CDec(drRow("loanadvancetranunkid")) > 0 Then

                                    ' FOR UPDATE BALANCEs IN LOAN ADVANCE TRAN TABLE 
                                    'Sohail (07 May 2015) -- Start
                                    'Enhancement - New Loan Redesign with allow to change interest rate, tenure, topup, receipt.
                                    'strQ = " Update lnloan_advance_tran set " & _
                                    '       "  balance_amount = balance_amount - @amount " & _
                                    '       " WHERE loanadvancetranunkid = @loanadvancetranunkid "

                                    'objDataOperation.ClearParameters()
                                    'objDataOperation.AddParameter("@amount", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, drRow("amount").ToString()) 'Sohail (11 May 2011)
                                    'objDataOperation.AddParameter("@loanadvancetranunkid", SqlDbType.Money, eZeeDataType.MONEY_SIZE, drRow("loanadvancetranunkid").ToString())

                                    'objDataOperation.ExecNonQuery(strQ)
                                    Dim dr_Row() As DataRow = dsLoan.Tables("List").Select("loanadvancetranunkid = " & CInt(drRow("loanadvancetranunkid")) & " ")
                                    If dr_Row.Length > 0 Then
                                    strQ = " Update lnloan_advance_tran set " & _
                                           " balance_amount = balance_amount - @amount " & _
                                               ", balance_amountPaidCurrency = balance_amountPaidCurrency - @amountPaidCurrency " & _
                                           " WHERE loanadvancetranunkid = @loanadvancetranunkid "

                                    objDataOperation.ClearParameters()
                                        objDataOperation.AddParameter("@amount", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, CDec(dr_Row(0).Item("TotPrincipalAmount")))
                                        objDataOperation.AddParameter("@amountPaidCurrency", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, CDec(dr_Row(0).Item("TotPrincipalAmountPaidCurrency")))
                                    objDataOperation.AddParameter("@loanadvancetranunkid", SqlDbType.Money, eZeeDataType.MONEY_SIZE, drRow("loanadvancetranunkid").ToString())

                                    objDataOperation.ExecNonQuery(strQ)

                                        If objDataOperation.ErrorMessage <> "" Then
                                            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                            Throw exForce
                                        End If
                                    End If
                                    'Sohail (07 May 2015) -- End

                                    strQ = "Select balance_amount, balance_amountPaidCurrency from lnloan_advance_tran  " & _
                                           " WHERE loanadvancetranunkid = @loanadvancetranunkid "
                                    'Sohail (07 May 2015) - [amountPaidCurrency]

                                    objDataOperation.ClearParameters()
                                    objDataOperation.AddParameter("@loanadvancetranunkid", SqlDbType.Money, eZeeDataType.MONEY_SIZE, drRow("loanadvancetranunkid").ToString())
                                    Dim dsAmount As DataSet = objDataOperation.ExecQuery(strQ, "Balance")

                                    If objDataOperation.ErrorMessage <> "" Then
                                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                        Throw exForce
                                    End If

                                    If dsAmount IsNot Nothing Then
                                        If dsAmount.Tables("Balance").Rows.Count > 0 Then
                                            'Sohail (07 May 2015) -- Start
                                            'Enhancement - New Loan Redesign with allow to change interest rate, tenure, topup, receipt.
                                            'If CDec(dsAmount.Tables("Balance").Rows(0)("balance_amount")) < 0.01 Then 'Sohail (11 May 2011)
                                            'Hemant (04 June 2019) -- Start
                                            'ISSUE/ENHANCEMENT : The employee loan still gets deducted even after loan tenure is over
                                            'If CDec(dsAmount.Tables("Balance").Rows(0)("balance_amountPaidCurrency")) < 0.01 Then
                                            'Hemant (08 Nov 2019) -- Start
                                            'ISSUE/ENHANCEMENT#4271(TUJIJENGE- TZ) - Not able to close Payroll period due to Loan deduction mismatch.
                                            'If CDec(dsAmount.Tables("Balance").Rows(0)("balance_amountPaidCurrency")) < 0.1 Then
                                            If CDec(dsAmount.Tables("Balance").Rows(0)("balance_amountPaidCurrency")) <= 1 Then
                                                'Hemant (08 Nov 2019) -- End
                                                'Hemant (04 June 2019) -- End
                                                'Sohail (07 May 2015) -- End

                                                strQ = " Update lnloan_advance_tran set " & _
                                                      " loan_statusunkid = 4 " & _
                                                      " WHERE loanadvancetranunkid = @loanadvancetranunkid "
                                                objDataOperation.ClearParameters()
                                                objDataOperation.AddParameter("@loanadvancetranunkid", SqlDbType.Money, eZeeDataType.MONEY_SIZE, drRow("loanadvancetranunkid").ToString())
                                                objDataOperation.ExecNonQuery(strQ)

                                                If objDataOperation.ErrorMessage <> "" Then
                                                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                                    Throw exForce
                                                End If

                                                objLoanStatusTran._Loanadvancetranunkid = CInt(drRow("loanadvancetranunkid"))
                                                'Nilay (20-Sept-2016) -- Start
                                                'Enhancement : Cancel feature for approved but not assigned loan application
                                                'objLoanStatusTran._Staus_Date = Now
                                                objLoanStatusTran._Staus_Date = dtCurrentDateAndTime
                                                'Nilay (20-Sept-2016) -- End
                                                objLoanStatusTran._Isvoid = False
                                                objLoanStatusTran._Voiddatetime = Nothing
                                                objLoanStatusTran._Voiduserunkid = -1
                                                objLoanStatusTran._Statusunkid = 4
                                                objLoanStatusTran._Periodunkid = mintPeriodunkid 'Sohail (07 May 2015)

                                                'Nilay (01-Apr-2016) -- Start
                                                'ENHANCEMENT - Approval Process in Loan Other Operations
                                                objLoanStatusTran._Userunkid = xUserUnkid
                                                'Nilay (01-Apr-2016) -- End

                                                objLoanStatusTran._ClientIP = mstrClientIP
                                                objLoanStatusTran._FormName = mstrFormName
                                                objLoanStatusTran._HostName = mstrHostName
                                                objLoanStatusTran._AuditDate = mdtAuditDate
                                                objLoanStatusTran._AuditUserId = mintAuditUserId
objLoanStatusTran._CompanyUnkid = mintCompanyUnkid
                                                objLoanStatusTran._FromWeb = mblnIsWeb
                                                objLoanStatusTran._LoginEmployeeunkid = mintLogEmployeeUnkid

                                                'Sohail (15 Dec 2015) -- Start
                                                'Enhancement - New Loan Calculation type Reducing balance with Fixed Principal EMI and new interest calculation Monthly apart from Daily for KBC.
                                                'blnFlag = objLoanStatusTran.Insert()

                                                'Nilay (25-Mar-2016) -- Start
                                                'blnFlag = objLoanStatusTran.Insert(strDatabaseName, intYearUnkid, objDataOperation, True)
                                                'Sohail (30 Apr 2019) -- Start
                                                'BBLMS Issue - Support Issue Id # 0003781 - 74.1 - Payslip global payment takes hours to process help needed urgently.
                                                'blnFlag = objLoanStatusTran.Insert(strDatabaseName, xUserUnkid, intYearUnkid, intCompanyunkid, _
                                                '                                   xPeriodStart, xPeriodEnd, strUserAccessModeSetting, xOnlyApproved, _
                                                '                                   objDataOperation, True)
                                                blnFlag = objLoanStatusTran.Insert(strDatabaseName, xUserUnkid, intYearUnkid, intCompanyunkid, _
                                                                                   xPeriodStart, xPeriodEnd, strUserAccessModeSetting, xOnlyApproved, _
                                                                                   objDataOperation, True, mintEmployeeunkid)
                                                'Sohail (30 Apr 2019) -- End
                                                'Nilay (25-Mar-2016) -- End


                                                'Sohail (15 Dec 2015) -- End

                                                If blnFlag = False Then objDataOperation.ReleaseTransaction(False)

                                            End If
                                        End If
                                    End If


                                    If objDataOperation.ErrorMessage <> "" Then
                                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                        Throw exForce
                                    End If

                                ElseIf CDec(drRow("savingtranunkid")) > 0 Then

                                    ' FOR UPDATE BALANCE IN SAVING TRAN TABLE 

                                    'Sohail (21 Aug 2010) -- Start
                                    'Changes:Total_contribution field added to update
                                    'strQ = " Update svsaving_tran set " & _
                                    '      " balance_amount = balance_amount + @amount " & _
                                    '      " WHERE savingtranunkid = @savingtranunkid "
                                    strQ = " Update svsaving_tran set " & _
                                          " balance_amount = balance_amount + @amount " & _
                                          ",total_contribution = total_contribution + @amount " & _
                                          " WHERE savingtranunkid = @savingtranunkid "
                                    'Sohail (21 Aug 2010) -- End

                                    objDataOperation.ClearParameters()
                                    objDataOperation.AddParameter("@amount", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, drRow("amount").ToString()) 'Sohail (11 May 2011)
                                    objDataOperation.AddParameter("@savingtranunkid", SqlDbType.Money, eZeeDataType.MONEY_SIZE, drRow("savingtranunkid").ToString())
                                    objDataOperation.ExecNonQuery(strQ)


                                    If objDataOperation.ErrorMessage <> "" Then
                                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                        Throw exForce
                                    End If

                                End If



                            Next

                            'Sohail (21 Mar 2014) -- Start
                            'Enhancement - Set Limit for C/F Netpay balance and Payment JV Report
                            If Math.Abs(mdecRoundingAdjustment) > 0 Then
                                Dim objEmp As New clsEmployee_Master
                                Dim objTranHead As New clsTransactionHead
                                Dim mintRoundingAdjustmentHeadID As Integer = objTranHead.GetRoundingAdjustmentHeadID()

                                objpayrollProcess = New clsPayrollProcessTran
                                objpayrollProcess._Tnaleavetranunkid = mintPayreftranunkid
                                objpayrollProcess._Employeeunkid = mintEmployeeunkid
                                objpayrollProcess._Tranheadunkid = mintRoundingAdjustmentHeadID
                                objpayrollProcess._Loanadvancetranunkid = -1
                                objpayrollProcess._Savingtranunkid = -1
                                objpayrollProcess._Amount = Math.Abs(mdecRoundingAdjustment)
                                If mdecRoundingAdjustment < 0 Then
                                    objpayrollProcess._Add_deduct = 1
                                Else
                                    objpayrollProcess._Add_deduct = 2
                                End If
                                objpayrollProcess._Currencyunkid = 0
                                objpayrollProcess._Vendorunkid = 0
                                objpayrollProcess._Broughtforward = False
                                objpayrollProcess._Userunkid = mintUserunkid
                                objpayrollProcess._Isvoid = False
                                objpayrollProcess._Voiduserunkid = -1
                                objpayrollProcess._Voiddatetime = Nothing
                                objpayrollProcess._Voidreason = ""
                                objpayrollProcess._MembershipTranUnkid = 0
                                objpayrollProcess._ActivityUnkid = -1

                                objEmp = New clsEmployee_Master

                                'S.SANDEEP [04 JUN 2015] -- START
                                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                                'objEmp._Employeeunkid = mintEmployeeunkid
                                'Sohail (23 Dec 2019) -- Start
                                'NMB Issue # : Users not able to login in self service when doing any transaction on earning and deduction screen.
                                'objEmp._Employeeunkid(xPeriodEnd) = mintEmployeeunkid
                                objEmp._Employeeunkid(xPeriodEnd, objDataOperation) = mintEmployeeunkid
                                'Sohail (23 Dec 2019) -- End
                                'S.SANDEEP [04 JUN 2015] -- END

                                objpayrollProcess._Costcenterunkid = objEmp._Costcenterunkid

                              With objpayrollProcess
                                    ._FormName = mstrFormName
                                    ._LoginEmployeeunkid = mintLogEmployeeUnkid
                                    ._ClientIP = mstrClientIP
                                    ._HostName = mstrHostName
                                    ._Isweb = mblnIsWeb
                                    ._AuditUserId = mintAuditUserId
                                    ._CompanyUnkid = mintCompanyunkid
                                    ._AuditDate = mdtAuditDate
                                End With

'Sohail (29 Nov 2021) -- Start
                                'Issue : : SQL date time overflow error on global payment screen when C/F Rounding above option is set.
                                objpayrollProcess._AuditUserId = mintUserunkid
                                objpayrollProcess._Loginemployeeunkid = mintLogEmployeeUnkid
                                objpayrollProcess._AuditDate = dtCurrentDateAndTime
                                If mstrWebFormName.Trim.Length <= 0 Then
                                    objpayrollProcess._Isweb = False
                                    objpayrollProcess._FormName = mstrForm_Name
                                Else
                                    objpayrollProcess._Isweb = True
                                    objpayrollProcess._FormName = mstrWebFormName
                                End If
                                objpayrollProcess._ClientIP = mstrWebIP
                                objpayrollProcess._HostName = mstrWebhostName
                                'Sohail (29 Nov 2021) -- End

                                'Sohail (02 Jan 2017) -- Start
                                'TRA Enhancement - 64.1 - Performance enhancement in process payroll, and global void payment.
                                'blnFlag = objpayrollProcess.Insert()
                                blnFlag = objpayrollProcess.Insert(objDataOperation)
                                'Sohail (02 Jan 2017) -- End
                                If blnFlag = False Then
                                    objDataOperation.ReleaseTransaction(False)
                                    Return False
                                End If
                                objEmp = Nothing
                                objTranHead = Nothing
                            End If
                            'Sohail (21 Mar 2014) -- End

                            'FOR UPDATE BALANCE  IN TNA LEAVE TRAN TABLE
                            'Sohail (21 Mar 2014) -- Start
                            'Enhancement - Set Limit for C/F Netpay balance and Payment JV Report
                            'strQ = " Update prtnaleave_tran set " & _
                            '       " balanceamount = balanceamount - @amount " & _
                            '       " WHERE tnaleavetranunkid = @tnaleavetranunkid "
                            strQ = " Update prtnaleave_tran set " & _
                                   " balanceamount = balanceamount - @amount - @roundingadjustment " & _
                                   " WHERE tnaleavetranunkid = @tnaleavetranunkid "
                            'Sohail (21 Mar 2014) -- End

                            objDataOperation.ClearParameters()
                            objDataOperation.AddParameter("@amount", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecAmount) 'Sohail (11 May 2011)
                            objDataOperation.AddParameter("@tnaleavetranunkid", SqlDbType.Money, eZeeDataType.MONEY_SIZE, mintPayreftranunkid)
                            objDataOperation.AddParameter("@roundingadjustment", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecRoundingAdjustment) 'Sohail (21 Mar 2014)
                            objDataOperation.ExecNonQuery(strQ)


                            If objDataOperation.ErrorMessage <> "" Then
                                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                Throw exForce
                            End If

                            'Sohail (16 Apr 2018) -- Start
                            'Voltamp Enhancement - Ref # 227 : Changes in "Salary Format WPS" report in 72.1.
                        Else
                            If Math.Abs(mdecRoundingAdjustment) > 0 Then
                                Dim objEmp As New clsEmployee_Master
                                Dim objTranHead As New clsTransactionHead
                                Dim mintRoundingAdjustmentHeadID As Integer = objTranHead.GetRoundingAdjustmentHeadID()

                                objpayrollProcess = New clsPayrollProcessTran
                                objpayrollProcess._Tnaleavetranunkid = mintPayreftranunkid
                                objpayrollProcess._Employeeunkid = mintEmployeeunkid
                                objpayrollProcess._Tranheadunkid = mintRoundingAdjustmentHeadID
                                objpayrollProcess._Loanadvancetranunkid = -1
                                objpayrollProcess._Savingtranunkid = -1
                                objpayrollProcess._Amount = Math.Abs(mdecRoundingAdjustment)
                                If mdecRoundingAdjustment < 0 Then
                                    objpayrollProcess._Add_deduct = 1
                                Else
                                    objpayrollProcess._Add_deduct = 2
                                End If
                                objpayrollProcess._Currencyunkid = 0
                                objpayrollProcess._Vendorunkid = 0
                                objpayrollProcess._Broughtforward = False
                                objpayrollProcess._Userunkid = mintUserunkid
                                objpayrollProcess._Isvoid = False
                                objpayrollProcess._Voiduserunkid = -1
                                objpayrollProcess._Voiddatetime = Nothing
                                objpayrollProcess._Voidreason = ""
                                objpayrollProcess._MembershipTranUnkid = 0
                                objpayrollProcess._ActivityUnkid = -1

                                objEmp = New clsEmployee_Master

                                'Sohail (23 Dec 2019) -- Start
                                'NMB Issue # : Users not able to login in self service when doing any transaction on earning and deduction screen.
                                'objEmp._Employeeunkid(xPeriodEnd) = mintEmployeeunkid
                                objEmp._Employeeunkid(xPeriodEnd, objDataOperation) = mintEmployeeunkid
                                'Sohail (23 Dec 2019) -- End

                                objpayrollProcess._Costcenterunkid = objEmp._Costcenterunkid
                                With objpayrollProcess
                                    ._FormName = mstrFormName
                                    ._LoginEmployeeunkid = mintLogEmployeeUnkid
                                    ._ClientIP = mstrClientIP
                                    ._HostName = mstrHostName
                                    ._Isweb = mblnIsWeb
                                    ._AuditUserId = mintAuditUserId
._CompanyUnkid = mintCompanyUnkid
                                    ._AuditDate = mdtAuditDate
                                End With

'Sohail (29 Nov 2021) -- Start
                                'Issue : : SQL date time overflow error on global payment screen when C/F Rounding above option is set.
                                objpayrollProcess._AuditUserId = mintUserunkid
                                objpayrollProcess._Loginemployeeunkid = mintLogEmployeeUnkid
                                objpayrollProcess._AuditDate = dtCurrentDateAndTime
                                If mstrWebFormName.Trim.Length <= 0 Then
                                    objpayrollProcess._Isweb = False
                                    objpayrollProcess._FormName = mstrForm_Name
                                Else
                                    objpayrollProcess._Isweb = True
                                    objpayrollProcess._FormName = mstrWebFormName
                                End If
                                objpayrollProcess._ClientIP = mstrWebIP
                                objpayrollProcess._HostName = mstrWebhostName
                                'Sohail (29 Nov 2021) -- End

                                blnFlag = objpayrollProcess.Insert(objDataOperation)
                                If blnFlag = False Then
                                    objDataOperation.ReleaseTransaction(False)
                                    Return False
                                End If
                                objEmp = Nothing
                                objTranHead = Nothing
                            End If

                            strQ = " Update prtnaleave_tran set " & _
                                   " balanceamount = balanceamount - @amount - @roundingadjustment " & _
                                   " WHERE tnaleavetranunkid = @tnaleavetranunkid "

                            objDataOperation.ClearParameters()
                            objDataOperation.AddParameter("@amount", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecAmount)
                            objDataOperation.AddParameter("@tnaleavetranunkid", SqlDbType.Money, eZeeDataType.MONEY_SIZE, mintPayreftranunkid)
                            objDataOperation.AddParameter("@roundingadjustment", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecRoundingAdjustment)
                            objDataOperation.ExecNonQuery(strQ)


                            If objDataOperation.ErrorMessage <> "" Then
                                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                Throw exForce
                            End If
                            'Sohail (16 Apr 2018) -- End

                        End If
                    End If

                Case 4
                    objEmployeeSaving._Savingtranunkid = mintPayreftranunkid
                    'Sohail (21 Nov 2015) -- Start
                    'Enhancement - Provide Deposit feaure in Employee Saving.
                    'objEmployeeSaving._Balance_Amount = objEmployeeSaving._Balance_Amount - mdecAmount
                    If mintPaymentTypeId = enPayTypeId.DEPOSIT Then
                        objEmployeeSaving._Balance_Amount = objEmployeeSaving._Balance_Amount + mdecAmount
                    Else
                    objEmployeeSaving._Balance_Amount = objEmployeeSaving._Balance_Amount - mdecAmount
                    End If
                    'Sohail (21 Nov 2015) -- End

                    With objEmployeeSaving
                        ._FormName = mstrFormName
                        ._LoginEmployeeUnkid = mintLogEmployeeUnkid
                        ._ClientIP = mstrClientIP
                        ._HostName = mstrHostName
                        ._FromWeb = mblnIsWeb
                        ._AuditUserId = mintAuditUserId
._CompanyUnkid = mintCompanyUnkid
                        ._AuditDate = mdtAuditDate
                    End With
                    blnFlag = objEmployeeSaving.Update(dtCurrentDateAndTime, objDataOperation)
                    'Shani(24-Aug-2015) -- End

                    'S.SANDEEP [ 04 DEC 2013 ] -- END

                    'Sohail (25 Mar 2015) -- Start
                    'B.C.Patel & Co Enhancement - New Custom CSV and XLS EFT for Advance module Payment and distribute payment as per employee banks.
                    If Not (mintPaymentmodeid = enPaymentMode.CASH OrElse mintPaymentmodeid = enPaymentMode.CASH_AND_CHEQUE) Then
                        Dim objEmpBank As New clsEmployeeBanks
                        'Sohail (07 May 2015) -- Start
                        'Enhancement - New Loan Redesign with allow to change interest rate, tenure, topup, receipt.
                        'Dim objPeriod As New clscommom_period_Tran
                        'objPeriod._Periodunkid = mintPeriodunkid
                        'Sohail (07 May 2015) -- End

                        'Sohail (21 Aug 2015) -- Start
                        'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                        'Dim ds As DataSet = objEmpBank.GetList("Banks", mintEmployeeunkid.ToString, objPeriod._End_Date, , "end_date DESC, EmpName, priority", mintPeriodunkid)
                        Dim ds As DataSet = objEmpBank.GetList(strDatabaseName, mintUserunkid, intYearUnkid, intCompanyunkid, objPeriod._Start_Date, objPeriod._End_Date, strUserAccessModeSetting, True, blnIsIncludeInactiveEmp, "Banks", True, , mintEmployeeunkid.ToString, objPeriod._End_Date, "end_date DESC, EmpName, priority")
                        'Sohail (21 Aug 2015) -- End
                        dtTran = New DataView(ds.Tables(0)).ToTable


                        Dim lstPerc As Integer = (From p In dtTran Where CInt(p.Item("modeid")) = enPaymentBy.Percentage).Count()
                        If lstPerc > 0 Then

                            For Each dsRow As DataRow In dtTran.Rows
                                If CDec(dsRow.Item("percentage").ToString) > 0 Then
                                    Dim objEmpSalary As New clsEmpSalaryTran
                                    With objEmpSalary
                                        ._Paymenttranunkid = mintPaymenttranunkid
                                        ._Paymentdate = mdtPaymentdate
                                        ._Employeeunkid = mintEmployeeunkid
                                        ._Empbanktranid = CInt(dsRow.Item("empbanktranunkid").ToString)
                                        ._Amount = mdecAmount * CDec(dsRow.Item("percentage").ToString) / 100
                                        'Sohail (21 Aug 2015) -- Start
                                        'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                                        '._Userunkid = User._Object._Userunkid
                                        ._Userunkid = mintUserunkid
                                        'Sohail (21 Aug 2015) -- End
                                        ._Isvoid = False
                                        ._Voiduserunkid = 0
                                        ._Voiddatetime = Nothing
                                        ._Voidreason = ""
                                        ._Basecurrencyid = mintBasecurrencyid
                                        ._Baseexchangerate = mdecBaseexchangerate
                                        ._Paidcurrencyid = mintPaidcurrencyid
                                        ._Expaidrate = mdecExpaidrate
                                        ._Expaidamt = mdecExpaidamt * CDec(dsRow.Item("percentage").ToString) / 100
                                        ._FormName = mstrFormName
                                        ._LoginEmployeeUnkid = mintLogEmployeeUnkid
                                        ._ClientIP = mstrClientIP
                                        ._HostName = mstrHostName
                                        ._FromWeb = mblnIsWeb
                                        ._AuditUserId = mintAuditUserId
._CompanyUnkid = mintCompanyUnkid
                                        ._AuditDate = mdtAuditDate
                                        If .Insert() = False Then
                                            objDataOperation.ReleaseTransaction(False)
                                            Return False
                                        End If
                                        objEmpSalary = Nothing
                                    End With
                                End If
                            Next

                        Else
                            Dim lst As List(Of DataRow) = (From p In dtTran Where CInt(p.Item("modeid")) = enPaymentBy.Value).DefaultIfEmpty.ToList
                            Dim decPriorityTotal As Decimal = (From p In lst Select CDec(p.Item("Amount"))).DefaultIfEmpty.Sum

                            If decPriorityTotal < mdecAmount Then
                                Dim intMinPriority As Integer = (From p In lst Select CInt(p.Item("priority"))).DefaultIfEmpty.Min
                                Dim decOtherTotal As Decimal = (From p In lst Where CInt(p.Item("priority")) <> intMinPriority Select CDec(p.Item("Amount"))).DefaultIfEmpty.Sum
                                Dim objEmpSalary As clsEmpSalaryTran

                                For Each dsRow As DataRow In lst

                                    objEmpSalary = New clsEmpSalaryTran
                                    With objEmpSalary
                                        ._Paymenttranunkid = mintPaymenttranunkid
                                        ._Paymentdate = mdtPaymentdate
                                        ._Employeeunkid = mintEmployeeunkid
                                        ._Empbanktranid = CInt(dsRow.Item("empbanktranunkid").ToString)
                                        ._Userunkid = mintUserunkid
                                        ._Isvoid = False
                                        ._Voiduserunkid = 0
                                        ._Voiddatetime = Nothing
                                        ._Voidreason = ""
                                        ._Basecurrencyid = mintBasecurrencyid
                                        ._Baseexchangerate = mdecBaseexchangerate
                                        ._Paidcurrencyid = mintPaidcurrencyid
                                        ._Expaidrate = mdecExpaidrate


                                        ._FormName = mstrFormName
                                        ._LoginEmployeeUnkid = mintLogEmployeeUnkid
                                        ._ClientIP = mstrClientIP
                                        ._HostName = mstrHostName
                                        ._FromWeb = mblnIsWeb
                                        ._AuditUserId = mintAuditUserId
._CompanyUnkid = mintCompanyUnkid
                                        ._AuditDate = mdtAuditDate

                                        If CInt(dsRow.Item("priority")) = intMinPriority Then
                                            ._Amount = (mdecAmount - decOtherTotal)
                                            If mdecBaseexchangerate <> 0 Then
                                                ._Expaidamt = (mdecAmount - decOtherTotal) * mdecExpaidrate / mdecBaseexchangerate
                                            Else
                                                ._Expaidamt = (mdecAmount - decOtherTotal) * mdecExpaidrate
                                            End If
                                        Else
                                            ._Amount = CDec(dsRow.Item("Amount"))
                                            If mdecBaseexchangerate <> 0 Then
                                                ._Expaidamt = CDec(dsRow.Item("Amount")) * mdecExpaidrate / mdecBaseexchangerate
                                            Else
                                                ._Expaidamt = CDec(dsRow.Item("Amount")) * mdecExpaidrate
                                            End If
                                        End If

                                        If .Insert() = False Then
                                            objDataOperation.ReleaseTransaction(False)
                                            Return False
                                        End If
                                    End With

                                Next
                                objEmpSalary = Nothing
                            Else

                                Dim objEmpSalary As clsEmpSalaryTran
                                Dim decBalance As Decimal = mdecAmount

                                For Each dsRow As DataRow In lst

                                    If decBalance <= 0 Then Exit For

                                    objEmpSalary = New clsEmpSalaryTran
                                    With objEmpSalary
                                        ._Paymenttranunkid = mintPaymenttranunkid
                                        ._Paymentdate = mdtPaymentdate
                                        ._Employeeunkid = mintEmployeeunkid
                                        ._Empbanktranid = CInt(dsRow.Item("empbanktranunkid").ToString)
                                        ._Userunkid = mintUserunkid
                                        ._Isvoid = False
                                        ._Voiduserunkid = 0
                                        ._Voiddatetime = Nothing
                                        ._Voidreason = ""
                                        ._Basecurrencyid = mintBasecurrencyid
                                        ._Baseexchangerate = mdecBaseexchangerate
                                        ._Paidcurrencyid = mintPaidcurrencyid
                                        ._Expaidrate = mdecExpaidrate

                                        ._FormName = mstrFormName
                                        ._LoginEmployeeUnkid = mintLogEmployeeUnkid
                                        ._ClientIP = mstrClientIP
                                        ._HostName = mstrHostName
                                        ._FromWeb = mblnIsWeb
                                        ._AuditUserId = mintAuditUserId
._CompanyUnkid = mintCompanyUnkid
                                        ._AuditDate = mdtAuditDate

                                        If decBalance > CDec(dsRow.Item("Amount")) Then
                                            ._Amount = CDec(dsRow.Item("Amount"))
                                            If mdecBaseexchangerate <> 0 Then
                                                ._Expaidamt = CDec(dsRow.Item("Amount")) * mdecExpaidrate / mdecBaseexchangerate
                                            Else
                                                ._Expaidamt = CDec(dsRow.Item("Amount")) * mdecExpaidrate
                                            End If
                                        Else
                                            ._Amount = decBalance
                                            If mdecBaseexchangerate <> 0 Then
                                                ._Expaidamt = decBalance * mdecExpaidrate / mdecBaseexchangerate
                                            Else
                                                ._Expaidamt = decBalance * mdecExpaidrate
                                            End If
                                        End If

                                        decBalance -= CDec(dsRow.Item("Amount"))

                                        If .Insert() = False Then
                                            objDataOperation.ReleaseTransaction(False)
                                            Return False
                                        End If
                                    End With
                                Next
                            End If
                        End If
                        objEmpBank = Nothing
                    End If
                    'Sohail (25 Mar 2015) -- End

                    If blnFlag = False Then objDataOperation.ReleaseTransaction(False)


            End Select

            'Sohail (23 May 2017) -- Start
            'Enhancement - 67.1 - Link budget with Payroll.
            If mdtActivityAdjustment IsNot Nothing AndAlso mdtActivityAdjustment.Rows.Count > 0 Then
                Dim objActAdj As clsFundActivityAdjustment_Tran
                For Each dtRow As DataRow In mdtActivityAdjustment.Rows
                    objActAdj = New clsFundActivityAdjustment_Tran

                    objActAdj._FundActivityunkid = CInt(dtRow.Item("fundactivityunkid"))
                    objActAdj._TransactionDate = eZeeDate.convertDate(dtRow.Item("transactiondate").ToString())
                    objActAdj._CurrentBalance = CDec(dtRow.Item("Current Balance"))
                    objActAdj._IncrDecrAmount = CDec(dtRow.Item("Actual Salary")) * -1
                    objActAdj._NewBalance = CDec(dtRow.Item("Current Balance")) - CDec(dtRow.Item("Actual Salary"))
                    objActAdj._Remark = dtRow.Item("remark").ToString()
                    objActAdj._Userunkid = mintUserunkid
                    objActAdj._Paymenttranunkid = 0
                    objActAdj._Globalvocunkid = mintPaymenttranunkid
                    With objActAdj
                        ._FormName = mstrFormName
                        ._LoginEmployeeunkid = mintLogEmployeeUnkid
                        ._ClientIP = mstrClientIP
                        ._HostName = mstrHostName
                        ._FromWeb = mblnIsWeb
                        ._AuditUserId = mintAuditUserId
._CompanyUnkid = mintCompanyUnkid
                        ._AuditDate = mdtAuditDate
                    End With
                    If objActAdj.Insert(dtCurrentDateAndTime, objDataOperation, True) = False Then
                        objDataOperation.ReleaseTransaction(False)
                        Throw New Exception(objActAdj._Message)
                        Return False
                    End If
                Next
            End If
            'Sohail (23 May 2017) -- End

            'S.SANDEEP [ 20 APRIL 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            ''Sandeep [ 16 Oct 2010 ] -- Start
            ''Issue : Auto No. Generation
            'If intPayVocNoType = 1 Then
            '    ConfigParameter._Object._NextPaymentVocNo = intNextPayVocNo + 1
            '    ConfigParameter._Object.updateParam()
            '    ConfigParameter._Object.Refresh()
            'End If
            ''Sandeep [ 16 Oct 2010 ] -- End 
            'S.SANDEEP [ 20 APRIL 2012 ] -- END


            

            objDataOperation.ReleaseTransaction(True)




            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Insert; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ' <summary>
    ' Modify By: Sandeep J. Sharma
    ' </summary>
    ' <returns>Boolean</returns>
    ' <purpose> Update Database Table (prpayment_tran) </purpose>

    'Nilay (25-Mar-2016) -- Start
    'Public Function Update(ByVal strDatabaseName As String, ByVal intYearUnkid As Integer, ByVal dtCurrentDateAndTime As Date, Optional ByVal objDataOpr As clsDataOperation = Nothing) As Boolean
    Public Function Update(ByVal xDatabaseName As String, _
                           ByVal xUserUnkid As Integer, _
                           ByVal xYearUnkid As Integer, _
                           ByVal xCompanyUnkid As Integer, _
                           ByVal xPeriodStart As Date, _
                           ByVal xPeriodEnd As Date, _
                           ByVal xUserModeSetting As String, _
                           ByVal xOnlyApproved As Boolean, _
                           ByVal dtCurrentDateAndTime As Date, _
                           Optional ByVal objDataOpr As clsDataOperation = Nothing) As Boolean
        'Nilay (25-Mar-2016) -- End

        'Sohail (15 Dec 2015) -- [strDatabaseName, intYearUnkid]

        'Sohail (21 Aug 2015) - [dtCurrentDateAndTime]

        If isExist(mstrVoucherno, mintPaymenttranunkid) Then
            mstrMessage = Language.getMessage(mstrModuleName, 1, "This Voucher No. is already defined. Please define new Voucher No.") 'Sohail (15 Dec 2010)
            Return False
        End If

        'Dim objDataOperation As clsDataOperation 'Sohail (03 Dec 2013)

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception


        Try

            'Sohail (03 Dec 2013) -- Start
            'Enhancement - TBC
            objDataOperation = New clsDataOperation
            'objDataOperation.BindTransaction()
            If objDataOpr Is Nothing Then
            objDataOperation.BindTransaction()
            Else
                objDataOperation = objDataOpr
                objDataOperation.ClearParameters()
            End If
            'Sohail (03 Dec 2013) -- End
            

            objDataOperation.AddParameter("@paymenttranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPaymenttranunkid.ToString)
            objDataOperation.AddParameter("@voucherno", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoucherno.ToString)
            objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPeriodunkid.ToString)
            objDataOperation.AddParameter("@paymentdate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtPaymentdate)
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeunkid.ToString)
            objDataOperation.AddParameter("@referenceid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPaymentrefid.ToString)
            objDataOperation.AddParameter("@paymentmode", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPaymentmodeid.ToString)
            objDataOperation.AddParameter("@branchunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintBranchunkid.ToString)
            objDataOperation.AddParameter("@chequeno", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrChequeno.ToString)
            objDataOperation.AddParameter("@paymentby", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPaymentbyid.ToString)
            objDataOperation.AddParameter("@percentage", SqlDbType.Float, eZeeDataType.MONEY_SIZE, mdblPercentage.ToString)
            objDataOperation.AddParameter("@amount", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecAmount.ToString) 'Sohail (11 May 2011)
            objDataOperation.AddParameter("@voucherref", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoucherref.ToString)
            objDataOperation.AddParameter("@referencetranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPayreftranunkid.ToString)
            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)

            If mdtVoiddatetime = Nothing Then
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            Else
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime)
            End If
            objDataOperation.AddParameter("@paytypeid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPaymentTypeId.ToString)
            objDataOperation.AddParameter("@isreceipt", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsReceipt.ToString)
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)
            'Sohail (16 Oct 2010) -- Start
            objDataOperation.AddParameter("@isglobalpayment", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsglobalpayment.ToString)
            'Sohail (16 Oct 2010) -- End
            'Sohail (08 Oct 2011) -- Start
            objDataOperation.AddParameter("@basecurrencyid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintBasecurrencyid.ToString)
            objDataOperation.AddParameter("@baseexchangerate", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecBaseexchangerate.ToString)
            objDataOperation.AddParameter("@paidcurrencyid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPaidcurrencyid.ToString)
            objDataOperation.AddParameter("@expaidrate", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecExpaidrate.ToString)
            objDataOperation.AddParameter("@expaidamt", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecExpaidamt.ToString)
            'Sohail (08 Oct 2011) -- End

            'S.SANDEEP [ 20 APRIL 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            objDataOperation.AddParameter("@globalvocno", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrGlobalvocno.ToString)
            'S.SANDEEP [ 20 APRIL 2012 ] -- END
            objDataOperation.AddParameter("@isauthorized", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsauthorized.ToString) 'Sohail (02 Jul 2012)
            objDataOperation.AddParameter("@accountno", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrAccountno.ToString)  'Sohail (21 Jul 2012)
            objDataOperation.AddParameter("@countryunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCountryunkid.ToString) 'Sohail (03 Sep 2012)
            objDataOperation.AddParameter("@isapproved", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsapproved.ToString) 'Sohail (13 Feb 2013)
            'Sohail (22 Oct 2013) -- Start
            'TRA - ENHANCEMENT
            objDataOperation.AddParameter("@roundingtypeid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintRoundingtypeid.ToString)
            objDataOperation.AddParameter("@roundingmultipleid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintRoundingmultipleid.ToString)
            objDataOperation.AddParameter("@roundingadjustment", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecRoundingAdjustment.ToString) 'Sohail (21 Mar 2014)
            objDataOperation.AddParameter("@remarks", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, mstrRemarks.ToString) 'Sohail (24 Dec 2014)
            objDataOperation.AddParameter("@paymentdate_periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPaymentDate_Periodunkid.ToString) 'Sohail (07 May 2015)

            'Sohail (21 Mar 2014)
            'Sohail (16 Oct 2010) -- Start
            'Changes : New field 'isglobalpayment' added.
            'strQ = "UPDATE prpayment_tran SET " & _
            '          "  voucherno = @voucherno" & _
            '          ", periodunkid = @periodunkid" & _
            '          ", paymentdate = @paymentdate" & _
            '          ", employeeunkid = @employeeunkid" & _
            '          ", referenceid = @referenceid" & _
            '          ", paymentmode = @paymentmode" & _
            '          ", branchunkid = @branchunkid" & _
            '          ", chequeno = @chequeno" & _
            '          ", paymentby = @paymentby" & _
            '          ", percentage = @percentage" & _
            '          ", amount = @amount" & _
            '          ", voucherref = @voucherref" & _
            '          ", referencetranunkid = @referencetranunkid" & _
            '          ", userunkid = @userunkid" & _
            '          ", isvoid = @isvoid" & _
            '          ", voiduserunkid = @voiduserunkid" & _
            '          ", voiddatetime = @voiddatetime " & _
            '          ", paytypeid = @paytypeid " & _
            '          ", isreceipt = @isreceipt " & _
            '          ", voidreason = @voidreason " & _
            '    "WHERE paymenttranunkid = @paymenttranunkid "


            'S.SANDEEP [ 20 APRIL 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            'strQ = "UPDATE prpayment_tran SET " & _
            '          "  voucherno = @voucherno" & _
            '          ", periodunkid = @periodunkid" & _
            '          ", paymentdate = @paymentdate" & _
            '          ", employeeunkid = @employeeunkid" & _
            '          ", referenceid = @referenceid" & _
            '          ", paymentmode = @paymentmode" & _
            '          ", branchunkid = @branchunkid" & _
            '          ", chequeno = @chequeno" & _
            '          ", paymentby = @paymentby" & _
            '          ", percentage = @percentage" & _
            '          ", amount = @amount" & _
            '          ", voucherref = @voucherref" & _
            '          ", referencetranunkid = @referencetranunkid" & _
            '          ", userunkid = @userunkid" & _
            '          ", isvoid = @isvoid" & _
            '          ", voiduserunkid = @voiduserunkid" & _
            '          ", voiddatetime = @voiddatetime " & _
            '          ", paytypeid = @paytypeid " & _
            '          ", isreceipt = @isreceipt " & _
            '          ", voidreason = @voidreason " & _
            '          ", isglobalpayment = @isglobalpayment " & _
            '          ", basecurrencyid = @basecurrencyid " & _
            '          ", baseexchangerate = @baseexchangerate " & _
            '          ", paidcurrencyid = @paidcurrencyid " & _
            '          ", expaidrate = @expaidrate " & _
            '          ", expaidamt = @expaidamt " & _
            '    "WHERE paymenttranunkid = @paymenttranunkid " 'Sohail (08 Oct 2011)(basecurrencyid,baseexchangerate,paidcurrencyid,expaidrate,expaidamt added.)
            ''Sohail (16 Oct 2010) -- End

            strQ = "UPDATE prpayment_tran SET " & _
                      "  voucherno = @voucherno" & _
                      ", periodunkid = @periodunkid" & _
                      ", paymentdate = @paymentdate" & _
                      ", employeeunkid = @employeeunkid" & _
                      ", referenceid = @referenceid" & _
                      ", paymentmode = @paymentmode" & _
                      ", branchunkid = @branchunkid" & _
                      ", chequeno = @chequeno" & _
                      ", paymentby = @paymentby" & _
                      ", percentage = @percentage" & _
                      ", amount = @amount" & _
                      ", voucherref = @voucherref" & _
                      ", referencetranunkid = @referencetranunkid" & _
                      ", userunkid = @userunkid" & _
                      ", isvoid = @isvoid" & _
                      ", voiduserunkid = @voiduserunkid" & _
                      ", voiddatetime = @voiddatetime " & _
                      ", paytypeid = @paytypeid " & _
                      ", isreceipt = @isreceipt " & _
                      ", voidreason = @voidreason " & _
                      ", isglobalpayment = @isglobalpayment " & _
                      ", basecurrencyid = @basecurrencyid " & _
                      ", baseexchangerate = @baseexchangerate " & _
                      ", paidcurrencyid = @paidcurrencyid " & _
                      ", expaidrate = @expaidrate " & _
                      ", expaidamt = @expaidamt " & _
                      ", globalvocno = @globalvocno " & _
                      ", isauthorized = @isauthorized " & _
                      ", accountno = @accountno" & _
                      ", countryunkid = @countryunkid " & _
                      ", isapproved = @isapproved " & _
                      ", roundingtypeid = @roundingtypeid " & _
                      ", roundingmultipleid = @roundingmultipleid " & _
                      ", roundingadjustment = @roundingadjustment " & _
                      ", remarks = @remarks " & _
                      ", paymentdate_periodunkid = @paymentdate_periodunkid " & _
                "WHERE paymenttranunkid = @paymenttranunkid "
            '   'Sohail (07 May 2015) - [paymentdate_periodunkid]
            '   'Sohail (24 Dec 2014) - [remarks]
            '   'Sohail (22 Oct 2013) - [roundingtypeid, roundingmultipleid]
            '   'Sohail (02 Jul 2012) - [isauthorized], 'Sohail (21 Jul 2012) - [accountno]
            '         'Sohail (03 Sep 2012) - [countryunkid]
            '         'Sohail (13 Feb 2013) - [isapproved]
            'S.SANDEEP [ 20 APRIL 2012 ] -- END



            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            'Sohail (11 Nov 2010) -- Start
            'Anjan (11 Jun 2011)-Start
            'Issue : In main table entry was get inserted if there any error occurs at time of insertion in AT table.
            'Call InsertAuditTrailForPaymentTran(objDataOperation, 2)
            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'If InsertAuditTrailForPaymentTran(objDataOperation, 2) = False Then
            If InsertAuditTrailForPaymentTran(objDataOperation, 2, dtCurrentDateAndTime) = False Then
                'Sohail (21 Aug 2015) -- End
                objDataOperation.ReleaseTransaction(False)
                Return False
            End If
            'Anjan (11 Jun 2011)-End

            'Sohail (11 Nov 2010) -- End

            Dim blnFlag As Boolean = False

            Select Case mintPaymentrefid
                Case enPaymentRefId.LOAN, enPaymentRefId.ADVANCE
                    Select Case mintPaymentTypeId
                        Case enPayTypeId.PAYMENT
                            objLoan_Advance._Loanadvancetranunkid = mintPayreftranunkid
                            'Sohail (07 May 2015) -- Start
                            'Enhancement - New Loan Redesign with allow to change interest rate, tenure, topup, receipt.
                            'If objLoan_Advance._Balance_Amount = 0 Then
                            If objLoan_Advance._Balance_AmountPaidCurrency = 0 Then
                                'Sohail (07 May 2015) -- End
                               
                                'Sohail (07 May 2015) -- Start
                                'Enhancement - New Loan Redesign with allow to change interest rate, tenure, topup, receipt.
                                'If objLoan_Advance._Calctype_Id = enLoanCalcId.Reducing_Amount Then
                                '    objLoan_Advance._Balance_Amount = objLoan_Advance._Loan_Amount
                                'ElseIf objLoan_Advance._Calctype_Id = enLoanCalcId.Simple_Interest Then
                                '    objLoan_Advance._Balance_Amount = objLoan_Advance._Net_Amount
                                'ElseIf objLoan_Advance._Calctype_Id = enLoanCalcId.No_Interest Then
                                '    objLoan_Advance._Balance_Amount = objLoan_Advance._Loan_Amount
                                'End If
                                If objLoan_Advance._Isloan = True Then
                                    objLoan_Advance._Balance_Amount = objLoan_Advance._Basecurrency_amount
                                    objLoan_Advance._Balance_AmountPaidCurrency = objLoan_Advance._Loan_Amount
                                Else
                                    objLoan_Advance._Balance_Amount = objLoan_Advance._Basecurrency_amount
                                    objLoan_Advance._Balance_AmountPaidCurrency = objLoan_Advance._Advance_Amount
                                End If
                                'Sohail (07 May 2015) -- End
                                'S.SANDEEP [11 JUN 2015] -- END

                                'objLoan_Advance._ClientIP = mstrClientIP
                                'objLoan_Advance._WebFormName = mstrWebFormName
                                'objLoan_Advance._HostName = mstrHostName

                                objLoan_Advance._ClientIP = mstrClientIP
                                objLoan_Advance._FormName = mstrFormName
                                objLoan_Advance._HostName = mstrHostName
                                objLoan_Advance._AuditUserId = mintAuditUserId
objLoan_Advance._CompanyUnkid = mintCompanyUnkid
                                objLoan_Advance._AuditDate = mdtAuditDate
                                objLoan_Advance._FromWeb = mblnIsWeb
                                objLoan_Advance._LoginEmployeeUnkid = mintLogEmployeeUnkid

                                'S.SANDEEP [ 04 DEC 2013 ] -- START
                                'blnFlag = objLoan_Advance.Update

                                'Nilay (05-May-2016) -- Start
                                'blnFlag = objLoan_Advance.Update(, objDataOperation)
                                blnFlag = objLoan_Advance.Update(objDataOperation)
                                'Nilay (05-May-2016) -- End

                                'S.SANDEEP [ 04 DEC 2013 ] -- END
                                If blnFlag = False Then objDataOperation.ReleaseTransaction(False)
                            End If
                        Case enPayTypeId.RECEIVED
                            If mdecAmount <> mdecOldAmount Then
                                objLoan_Advance._Loanadvancetranunkid = mintPayreftranunkid
                                If mdecAmount > mdecOldAmount Then
                                    mdecAmount -= mdecOldAmount
                                Else
                                    mdecOldAmount -= mdecAmount
                                    mdecOldAmountPaidCurrency -= mdecExpaidamt 'Sohail (07 May 2015)
                                End If

                                If objLoan_Advance._Balance_Amount = 0 Then
                                    objLoan_Advance._Balance_Amount = objLoan_Advance._Balance_Amount + mdecOldAmount
                                    objLoan_Advance._Balance_AmountPaidCurrency = objLoan_Advance._Balance_AmountPaidCurrency + mdecOldAmountPaidCurrency 'Sohail (07 May 2015)
                                    objLoan_Advance._LoanStatus = 1
                                Else
                                    objLoan_Advance._Balance_Amount = objLoan_Advance._Balance_Amount - mdecAmount
                                    objLoan_Advance._Balance_AmountPaidCurrency = objLoan_Advance._Balance_AmountPaidCurrency - mdecExpaidamt 'Sohail (07 May 2015)
                                End If

                                If objLoan_Advance._Balance_Amount = 0 Then
                                    objLoan_Advance._LoanStatus = 4
                                    objLoanStatusTran._Loanadvancetranunkid = objLoan_Advance._Loanadvancetranunkid
                                    objLoanStatusTran._Isvoid = False
                                    'Nilay (20-Sept-2016) -- Start
                                    'Enhancement : Cancel feature for approved but not assigned loan application
                                    'objLoanStatusTran._Staus_Date = mdtPaymentdate
                                    objLoanStatusTran._Staus_Date = dtCurrentDateAndTime
                                    'Nilay (20-Sept-2016) -- End
                                    objLoanStatusTran._Voiddatetime = Nothing
                                    objLoanStatusTran._Voiduserunkid = -1
                                    objLoanStatusTran._Statusunkid = 4
                                    'Sohail (15 Dec 2015) -- Start
                                    'Enhancement - New Loan Calculation type Reducing balance with Fixed Principal EMI and new interest calculation Monthly apart from Daily for KBC.
                                    'objLoanStatusTran._Periodunkid = mintPeriodunkid 'Sohail (07 May 2015)
                                    objLoanStatusTran._Periodunkid = mintPaymentDate_Periodunkid
                                    'Sohail (15 Dec 2015) -- End

                                    'Nilay (01-Apr-2016) -- Start
                                    'ENHANCEMENT - Approval Process in Loan Other Operations
                                    objLoanStatusTran._Userunkid = xUserUnkid
                                    'Nilay (01-Apr-2016) -- End

                                    objLoanStatusTran._FormName = mstrFormName
                                    objLoanStatusTran._LoginEmployeeunkid = mintLogEmployeeUnkid
                                    objLoanStatusTran._ClientIP = mstrClientIP
                                    objLoanStatusTran._HostName = mstrHostName
                                    objLoanStatusTran._FromWeb = mblnIsWeb
                                    objLoanStatusTran._AuditUserId = mintAuditUserId
objLoanStatusTran._CompanyUnkid = mintCompanyUnkid
                                    objLoanStatusTran._AuditDate = mdtAuditDate

                                    'S.SANDEEP [ 04 DEC 2013 ] -- START
                                    'blnFlag = objLoanStatusTran.Insert()

                                    'Sohail (15 Dec 2015) -- Start
                                    'Enhancement - New Loan Calculation type Reducing balance with Fixed Principal EMI and new interest calculation Monthly apart from Daily for KBC.
                                    'blnFlag = objLoanStatusTran.Insert(objDataOperation)

                                    'Nilay (25-Mar-2016) -- Start
                                    'blnFlag = objLoanStatusTran.Insert(strDatabaseName, intYearUnkid, objDataOperation, True)
                                    'Sohail (30 Apr 2019) -- Start
                                    'BBLMS Issue - Support Issue Id # 0003781 - 74.1 - Payslip global payment takes hours to process help needed urgently.
                                    'blnFlag = objLoanStatusTran.Insert(xDatabaseName, xUserUnkid, xYearUnkid, xCompanyUnkid, xPeriodStart, xPeriodEnd, _
                                    '                                   xUserModeSetting, xOnlyApproved, objDataOperation, True)
                                    blnFlag = objLoanStatusTran.Insert(xDatabaseName, xUserUnkid, xYearUnkid, xCompanyUnkid, xPeriodStart, xPeriodEnd, _
                                                                       xUserModeSetting, xOnlyApproved, objDataOperation, True, mintEmployeeunkid)
                                    'Sohail (30 Apr 2019) -- End
                                    'Nilay (25-Mar-2016) -- End

                                    'Sohail (15 Dec 2015) -- End

                                    'S.SANDEEP [ 04 DEC 2013 ] -- END
                                    If blnFlag = False Then objDataOperation.ReleaseTransaction(False)
                                End If

                                'objLoan_Advance._ClientIP = mstrClientIP
                                'objLoan_Advance._WebFormName = mstrWebFormName
                                'objLoan_Advance._HostName = mstrHostName

                                objLoan_Advance._ClientIP = mstrClientIP
                                objLoan_Advance._FormName = mstrFormName
                                objLoan_Advance._HostName = mstrHostName
                                objLoan_Advance._AuditUserId = mintAuditUserId
objLoan_Advance._CompanyUnkid = mintCompanyUnkid
                                objLoan_Advance._AuditDate = mdtAuditDate
                                objLoan_Advance._FromWeb = mblnIsWeb
                                objLoan_Advance._LoginEmployeeUnkid = mintLogEmployeeUnkid

                                'S.SANDEEP [ 04 DEC 2013 ] -- START
                                'blnFlag = objLoan_Advance.Update

                                'Nilay (05-May-2016) -- Start
                                'blnFlag = objLoan_Advance.Update(, objDataOperation)
                                blnFlag = objLoan_Advance.Update(objDataOperation)
                                'Nilay (05-May-2016) -- End

                                'S.SANDEEP [ 04 DEC 2013 ] -- END
                                If blnFlag = False Then objDataOperation.ReleaseTransaction(False)
                            End If
                    End Select
                Case 3
                Case 4


                    objEmployeeSaving._Savingtranunkid = mintPayreftranunkid
                    objEmployeeSaving._Balance_Amount = (objEmployeeSaving._Balance_Amount + mdecOldAmount) - mdecAmount
                    With objEmployeeSaving
                        ._FormName = mstrFormName
                        ._LoginEmployeeUnkid = mintLogEmployeeUnkid
                        ._ClientIP = mstrClientIP
                        ._HostName = mstrHostName
                        ._FromWeb = mblnIsWeb
                        ._AuditUserId = mintAuditUserId
._CompanyUnkid = mintCompanyUnkid
                        ._AuditDate = mdtAuditDate
                    End With

                    'Shani(24-Aug-2015) -- Start
                    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                    'blnFlag = objEmployeeSaving.Update(objDataOperation)
                    blnFlag = objEmployeeSaving.Update(dtCurrentDateAndTime, objDataOperation)
                    'Shani(24-Aug-2015) -- End

                    'S.SANDEEP [ 04 DEC 2013 ] -- END


                    If blnFlag = False Then objDataOperation.ReleaseTransaction(False)

            End Select
            If objDataOpr Is Nothing Then 'Sohail (03 Dec 2013)
            objDataOperation.ReleaseTransaction(True)
            End If 'Sohail (03 Dec 2013)
            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    'Sohail (21 Aug 2015) -- Start
    'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
    '''' <summary>
    '''' Modify By: Sandeep J. Sharma
    '''' </summary>
    '''' <returns>Boolean</returns>
    '''' <purpose> Delete Database Table (prpayment_tran) </purpose>
    'Public Function Delete(ByVal intUnkid As Integer, Optional ByVal strUserAccessFilter As String = "") As Boolean
    '    'If isUsed(intUnkid) Then
    '    '    mstrMessage = "<Message>"
    '    '    Return False
    '    'End If

    '    '***************************
    '    '   IF YOU CHANGE ANYTHING IN THIS METHOD PLEASE GIVE SAME EFFECT IN OTHER OVERLOADED DELETE FUNCTION AS WELL.
    '    '***************************

    '    Dim objDataOperation As clsDataOperation

    '    Dim dsList As DataSet = Nothing
    '    Dim strQ As String = ""
    '    Dim exForce As Exception

    '    objDataOperation = New clsDataOperation
    '    objDataOperation.BindTransaction()
    '    Try

    '        'Sohail (18 May 2013) -- Start
    '        'TRA - ENHANCEMENT
    '        If mstrWebIP.Trim = "" Then mstrWebIP = getIP()
    '        If mstrWebhostName.Trim = "" Then mstrWebhostName = getHostName()
    '        'Sohail (18 May 2013) -- End

    '        'Sohail (27 Jul 2012) -- Start
    '        'TRA - ENHANCEMENT - Insert in AT Log
    '        If mstrWebFormName.Trim.Length <= 0 Then
    '            'S.SANDEEP [ 11 AUG 2012 ] -- START
    '            'ENHANCEMENT : TRA CHANGES
    '            'Dim frm As Form
    '            'For Each frm In Application.OpenForms
    '            '    If Form.ActiveForm.Name IsNot Nothing AndAlso Form.ActiveForm.Name = frm.Name Then
    '            '        objDataOperation.AddParameter("@form_name", SqlDbType.NVarChar, 500, frm.Name)
    '            '        objDataOperation.AddParameter("@isweb", SqlDbType.Bit, eZeeDataType.BIT_SIZE, False)
    '            '        
    '            '    End If
    '            'Next
    '            objDataOperation.AddParameter("@form_name", SqlDbType.NVarChar, 500, mstrForm_Name)
    '            objDataOperation.AddParameter("@isweb", SqlDbType.Bit, eZeeDataType.BIT_SIZE, False)
    '            
    '            'S.SANDEEP [ 11 AUG 2012 ] -- END
    '        Else
    '            objDataOperation.AddParameter("@form_name", SqlDbType.NVarChar, 500, mstrWebFormName)
    '            objDataOperation.AddParameter("@isweb", SqlDbType.Bit, eZeeDataType.BIT_SIZE, True)
    '            objDataOperation.AddParameter("@module_name1", SqlDbType.NVarChar, 500, Language.getMessage(mstrModuleName, 2, "WEB"))
    '        End If
    '        
    '        
    '        
    '        

    '        strQ = "INSERT INTO atprempsalary_tran " & _
    '                        "( empsalarytranunkid, paymenttranunkid, paymentdate, employeeunkid, empbanktranid, amount, audittype, audituserunkid, auditdatetime, ip, machine_name, basecurrencyid, baseexchangerate, paidcurrencyid, expaidrate, expaidamt, form_name, module_name1, module_name2, module_name3, module_name4, module_name5, isweb ) " & _
    '                "SELECT " & _
    '                     "empsalarytranunkid, paymenttranunkid, paymentdate, employeeunkid, empbanktranid, amount, 3, @voiduserunkid, @voiddatetime, '" & mstrWebIP & "', '" & mstrWebhostName & "', basecurrencyid, baseexchangerate, paidcurrencyid, expaidrate, expaidamt, @form_name, @module_name1, @module_name2, @module_name3, @module_name4, @module_name5, @isweb " & _
    '                "FROM prempsalary_tran " & _
    '                     "WHERE ISNULL(isvoid, 0) = 0 " & _
    '                     "AND paymenttranunkid = @paymenttranunkid "

    '        objDataOperation.AddParameter("@paymenttranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)
    '        objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
    '        objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
    '        If mdtVoiddatetime = Nothing Then
    '            objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
    '        Else
    '            objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime)
    '        End If

    '        objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)

    '        Call objDataOperation.ExecNonQuery(strQ)

    '        If objDataOperation.ErrorMessage <> "" Then
    '            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
    '            Throw exForce
    '        End If
    '        'Sohail (27 Jul 2012) -- End


    '        strQ = "UPDATE prempsalary_tran SET " & _
    '                  "  isvoid = @isvoid" & _
    '                  ", voiduserunkid = @voiduserunkid" & _
    '                  ", voiddatetime = @voiddatetime " & _
    '                  ", voidreason = @voidreason " & _
    '        "WHERE paymenttranunkid = @paymenttranunkid "

    '        'Sohail (27 Jul 2012) -- Start
    '        'TRA - ENHANCEMENT
    '        'objDataOperation.AddParameter("@paymenttranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)
    '        'objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
    '        'objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
    '        'If mdtVoiddatetime = Nothing Then
    '        '    objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
    '        'Else
    '        '    objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime)
    '        'End If

    '        'objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)
    '        'Sohail (27 Jul 2012) -- End

    '        Call objDataOperation.ExecNonQuery(strQ)

    '        If objDataOperation.ErrorMessage <> "" Then
    '            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
    '            Throw exForce
    '        End If

    '        'Sohail (16 Oct 2010) -- Start
    '        '*** Delete entries from prempsalary_tran Table
    '        strQ = "UPDATE prpayment_tran SET " & _
    '                  "  isvoid = @isvoid" & _
    '                  ", voiduserunkid = @voiduserunkid" & _
    '                  ", voiddatetime = @voiddatetime " & _
    '                  ", voidreason = @voidreason " & _
    '        "WHERE paymenttranunkid = @paymenttranunkid "

    '        objDataOperation.ClearParameters()
    '        objDataOperation.AddParameter("@paymenttranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)
    '        objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
    '        objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
    '        If mdtVoiddatetime = Nothing Then
    '            objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
    '        Else
    '            objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime)
    '        End If

    '        objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)

    '        Call objDataOperation.ExecNonQuery(strQ)

    '        If objDataOperation.ErrorMessage <> "" Then
    '            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
    '            Throw exForce
    '        End If
    '        'Sohail (16 Oct 2010) -- End


    '        'Sohail (27 Jul 2012) -- Start
    '        'TRA - ENHANCEMENT - Insert in AT Log
    '        If mstrWebFormName.Trim.Length <= 0 Then
    '            'S.SANDEEP [ 11 AUG 2012 ] -- START
    '            'ENHANCEMENT : TRA CHANGES
    '            'Dim frm As Form
    '            'For Each frm In Application.OpenForms
    '            '    If Form.ActiveForm.Name IsNot Nothing AndAlso Form.ActiveForm.Name = frm.Name Then
    '            '        objDataOperation.AddParameter("@form_name", SqlDbType.NVarChar, 500, frm.Name)
    '            '        objDataOperation.AddParameter("@isweb", SqlDbType.Bit, eZeeDataType.BIT_SIZE, False)
    '            '        
    '            '    End If
    '            'Next
    '            objDataOperation.AddParameter("@form_name", SqlDbType.NVarChar, 500, mstrForm_Name)
    '            objDataOperation.AddParameter("@isweb", SqlDbType.Bit, eZeeDataType.BIT_SIZE, False)
    '            
    '            'S.SANDEEP [ 11 AUG 2012 ] -- END
    '            objDataOperation.AddParameter("@loginemployeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, -1) 'mintLogEmployeeUnkid 'Sohail (13 Feb 2013)
    '        Else
    '            objDataOperation.AddParameter("@form_name", SqlDbType.NVarChar, 500, mstrWebFormName)
    '            objDataOperation.AddParameter("@isweb", SqlDbType.Bit, eZeeDataType.BIT_SIZE, True)
    '            objDataOperation.AddParameter("@module_name1", SqlDbType.NVarChar, 500, Language.getMessage(mstrModuleName, 2, "WEB"))
    '            objDataOperation.AddParameter("@loginemployeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLogEmployeeUnkid) 'Sohail (13 Feb 2013)
    '        End If
    '        
    '        
    '        
    '        

    '        strQ = "INSERT INTO atprpayment_authorize_tran " & _
    '                        "( paymentauthorizetranunkid, paymenttranunkid, authorize_date, isauthorized, audittype, audituserunkid, auditdatetime, ip, machine_name, form_name, module_name1, module_name2, module_name3, module_name4, module_name5, isweb, remarks ) " & _
    '                "SELECT " & _
    '                     "paymentauthorizetranunkid, paymenttranunkid, authorize_date, isauthorized, 3, @voiduserunkid, @voiddatetime, '" & mstrWebIP & "', '" & mstrWebhostName & "', @form_name, @module_name1, @module_name2, @module_name3, @module_name4, @module_name5, @isweb, remarks " & _
    '                "FROM prpayment_authorize_tran " & _
    '                "WHERE isactive = 1 " & _
    '                     "AND paymenttranunkid = @paymenttranunkid "

    '        Call objDataOperation.ExecNonQuery(strQ)

    '        If objDataOperation.ErrorMessage <> "" Then
    '            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
    '            Throw exForce
    '        End If
    '        'Sohail (27 Jul 2012) -- End

    '        'Sohail (02 Jul 2012) -- Start
    '        'TRA - ENHANCEMENT - *** Delete entries from prpayment_authorize_tran Table
    '        strQ = "UPDATE  prpayment_authorize_tran " & _
    '                "SET     isactive = 0 " & _
    '                      ", voiddatetime = @voiddatetime " & _
    '                      ", voiduserunkid = @voiduserunkid " & _
    '                      ", voidreason = @voidreason " & _
    '                "WHERE   paymenttranunkid = @paymenttranunkid " 'Sohail (27 Jul 2012) - [voiddatetime, voiduserunkid, voidreason]

    '        Call objDataOperation.ExecNonQuery(strQ)

    '        If objDataOperation.ErrorMessage <> "" Then
    '            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
    '            Throw exForce
    '        End If
    '        'Sohail (02 Jul 2012) -- End

    '        'Sohail (13 Feb 2013) -- Start
    '        'TRA - ENHANCEMENT
    '        strQ = "INSERT INTO atprpayment_approval_tran " & _
    '                        "( paymentapprovaltranunkid, paymenttranunkid, levelunkid, priority, approval_date, statusunkid, disapprovalreason, audittype, audituserunkid, auditdatetime, ip, machine_name, form_name, module_name1, module_name2, module_name3, module_name4, module_name5, isweb, loginemployeeunkid, remarks ) " & _
    '                "SELECT " & _
    '                     "paymentapprovaltranunkid, paymenttranunkid, levelunkid, priority, approval_date, statusunkid, disapprovalreason, 3, @voiduserunkid, @voiddatetime, '" & mstrWebIP & "', '" & mstrWebhostName & "', @form_name, @module_name1, @module_name2, @module_name3, @module_name4, @module_name5, @isweb, @loginemployeeunkid, remarks " & _
    '                "FROM     prpayment_approval_tran " & _
    '                "WHERE ISNULL(isvoid, 0) = 0 " & _
    '                "AND paymenttranunkid = @paymenttranunkid "
    '        'Sohail (27 Feb 2013) - [remarks]

    '        Call objDataOperation.ExecNonQuery(strQ)

    '        If objDataOperation.ErrorMessage <> "" Then
    '            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
    '            Throw exForce
    '        End If

    '        strQ = "UPDATE  prpayment_approval_tran " & _
    '                "SET     isvoid = 1 " & _
    '                      ", voiddatetime = @voiddatetime " & _
    '                      ", voiduserunkid = @voiduserunkid " & _
    '                      ", voidreason = @voidreason " & _
    '                "WHERE   paymenttranunkid = @paymenttranunkid " & _
    '                "AND ISNULL(isvoid, 0) = 0 "

    '        Call objDataOperation.ExecNonQuery(strQ)

    '        If objDataOperation.ErrorMessage <> "" Then
    '            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
    '            Throw exForce
    '        End If
    '        'Sohail (13 Feb 2013) -- End

    '        'Sandeep [ 17 DEC 2010 ] -- Start

    '        'S.SANDEEP [ 04 DEC 2013 ] -- START
    '        'If objCashDenome.VoidCashDenomination(intUnkid, mblnIsvoid, mintVoiduserunkid, mdtVoiddatetime, mstrVoidreason) = False Then
    '        '    objDataOperation.ReleaseTransaction(False)
    '        '    Return False
    '        'End If

    '        If objCashDenome.VoidCashDenomination(intUnkid, mblnIsvoid, mintVoiduserunkid, mdtVoiddatetime, mstrVoidreason, objDataOperation) = False Then
    '            objDataOperation.ReleaseTransaction(False)
    '            Return False
    '        End If
    '        'S.SANDEEP [ 04 DEC 2013 ] -- END


    '        'Sandeep [ 17 DEC 2010 ] -- End 

    '        'Sohail (11 Nov 2010) -- Start

    '        'S.SANDEEP [ 29 May 2013 ] -- START
    '        'ENHANCEMENT : ATLAS COPCO WEB CHANGES
    '        _DataOperation = objDataOperation
    '        'S.SANDEEP [ 29 May 2013 ] -- END


    '        Me._Paymenttranunkid = intUnkid

    '        'Anjan (11 Jun 2011)-Start
    '        'Issue : In main table entry was get inserted if there any error occurs at time of insertion in AT table.
    '        'Call InsertAuditTrailForPaymentTran(objDataOperation, 3)
    '        If InsertAuditTrailForPaymentTran(objDataOperation, 3) = False Then
    '            objDataOperation.ReleaseTransaction(False)
    '    Return False
    'End If
    '        'Anjan (11 Jun 2011)-End

    '        'Sohail (11 Nov 2010) -- End

    '        Dim blnFlag As Boolean = False
    '        Select Case mintPaymentrefid
    '            Case enPaymentRefId.LOAN, enPaymentRefId.ADVANCE
    '                Select Case mintPaymentTypeId
    '                    Case enPayTypeId.PAYMENT
    '                        'S.SANDEEP [ 29 May 2013 ] -- START
    '                        'ENHANCEMENT : ATLAS COPCO WEB CHANGES
    '                        objLoan_Advance._DataOpr = objDataOperation
    '                        'S.SANDEEP [ 29 May 2013 ] -- END
    '                        objLoan_Advance._Loanadvancetranunkid = mintPayreftranunkid
    '                        '<TODO Please Put One Check Point of Payment......!! >

    '                        Dim dsDataList As New DataSet

    '                        'S.SANDEEP [ 29 May 2013 ] -- START
    '                        'ENHANCEMENT : ATLAS COPCO WEB CHANGES
    '                        'dsDataList = GetList("Test", mintPaymentrefid, mintPayreftranunkid, mintPaymentTypeId)
    '                        'Sohail (21 Aug 2015) -- Start
    '                        'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
    '                        'dsDataList = GetList("Test", mintPaymentrefid, mintPayreftranunkid, mintPaymentTypeId, , strUserAccessFilter, objDataOperation)
    '                        dsDataList = GetList("Test", mintPaymentrefid, mintPayreftranunkid, mintPaymentTypeId, , strUserAccessFilter, objDataOperation)
    '                        'Sohail (21 Aug 2015) -- End
    '                        'S.SANDEEP [ 29 May 2013 ] -- END


    '                        If dsDataList.Tables(0).Rows.Count > 0 Then
    '                            'objDataOperation.ReleaseTransaction(True)
    '                        Else
    '                            'If objLoan_Advance._Balance_Amount = 0 Then
    '                            '    If objLoan_Advance._Isloan = True Then
    '                            '        objLoan_Advance._Balance_Amount = objLoan_Advance._Net_Amount
    '                            '    Else
    '                            '        objLoan_Advance._Balance_Amount = objLoan_Advance._Advance_Amount
    '                            '    End If
    '                            '    blnFlag = objLoan_Advance.Update
    '                            '    If blnFlag = False Then objDataOperation.ReleaseTransaction(False)
    '                            'End If

    '                            objLoan_Advance._Balance_Amount = 0

    '                            'S.SANDEEP [ 29 May 2013 ] -- START
    '                            'ENHANCEMENT : ATLAS COPCO WEB CHANGES
    '                            'blnFlag = objLoan_Advance.Update
    '                            blnFlag = objLoan_Advance.Update(mintVoiduserunkid, objDataOperation)
    '                            'S.SANDEEP [ 29 May 2013 ] -- END
    '                            If blnFlag = False Then objDataOperation.ReleaseTransaction(False)
    '                        End If
    '                    Case enPayTypeId.RECEIVED
    '                        'S.SANDEEP [ 29 May 2013 ] -- START
    '                        'ENHANCEMENT : ATLAS COPCO WEB CHANGES
    '                        objLoan_Advance._DataOpr = objDataOperation
    '                        'S.SANDEEP [ 29 May 2013 ] -- END
    '                        objLoan_Advance._Loanadvancetranunkid = mintPayreftranunkid
    '                        objLoan_Advance._Balance_Amount = objLoan_Advance._Balance_Amount + mdecAmount
    '                        'S.SANDEEP [ 29 May 2013 ] -- START
    '                        'ENHANCEMENT : ATLAS COPCO WEB CHANGES
    '                        'blnFlag = objLoan_Advance.Update
    '                        blnFlag = objLoan_Advance.Update(mintVoiduserunkid, objDataOperation)
    '                        'S.SANDEEP [ 29 May 2013 ] -- END
    '                        If blnFlag = False Then objDataOperation.ReleaseTransaction(False)
    '                End Select

    '            Case enPaymentRefId.PAYSLIP

    '                If mintPaymentTypeId = enPayTypeId.PAYMENT Then



    '                    Dim objpayrollProcess As New clsPayrollProcessTran
    '                    Dim dsProcess As DataSet = objpayrollProcess.GetList("List", mintEmployeeunkid, mintPeriodunkid, mintPayreftranunkid, , mstrAccessLevelFilterString)

    '                    If dsProcess IsNot Nothing Then
    '                        If dsProcess.Tables(0).Rows.Count > 0 Then
    '                            For Each drRow As DataRow In dsProcess.Tables(0).Rows
    '                                If CDec(drRow("loanadvancetranunkid")) > 0 Then

    '                                    ' FOR UPDATE BALANCEs IN LOAN ADVANCE TRAN TABLE 

    '                                    strQ = " Update lnloan_advance_tran set " & _
    '                                           " balance_amount = balance_amount + @amount " & _
    '                                           " WHERE loanadvancetranunkid = @loanadvancetranunkid "
    '                                    objDataOperation.ClearParameters()
    '                                    objDataOperation.AddParameter("@amount", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, drRow("amount").ToString()) 'Sohail (11 May 2011)
    '                                    objDataOperation.AddParameter("@loanadvancetranunkid", SqlDbType.Money, eZeeDataType.MONEY_SIZE, drRow("loanadvancetranunkid").ToString())
    '                                    objDataOperation.ExecNonQuery(strQ)


    '                                    strQ = "Select balance_amount from lnloan_advance_tran  " & _
    '                                     " WHERE loanadvancetranunkid = @loanadvancetranunkid "
    '                                    objDataOperation.ClearParameters()
    '                                    objDataOperation.AddParameter("@loanadvancetranunkid", SqlDbType.Money, eZeeDataType.MONEY_SIZE, drRow("loanadvancetranunkid").ToString())
    '                                    Dim dsAmount As DataSet = objDataOperation.ExecQuery(strQ, "Balance")

    '                                    If dsAmount IsNot Nothing Then

    '                                        If dsAmount.Tables("Balance").Rows.Count > 0 Then

    '                                            If CDec(dsAmount.Tables("Balance").Rows(0)("balance_amount")) > 0 Then 'Sohail (11 May 2011)

    '                                                strQ = " Update lnloan_advance_tran set " & _
    '                                                      " loan_statusunkid = 1 " & _
    '                                                      " WHERE loanadvancetranunkid = @loanadvancetranunkid "
    '                                                objDataOperation.ClearParameters()
    '                                                objDataOperation.AddParameter("@loanadvancetranunkid", SqlDbType.Money, eZeeDataType.MONEY_SIZE, drRow("loanadvancetranunkid").ToString())
    '                                                objDataOperation.ExecNonQuery(strQ)

    '                                                Dim dsStatus As DataSet = objLoanStatusTran.GetList("LoanStatus", True)
    '                                                Dim dtStatus As DataTable = New DataView(dsStatus.Tables("LoanStatus"), "loanadvancetranunkid= " & CInt(drRow("loanadvancetranunkid")) & " AND statusunkid = 4 AND isvoid = 0", "", DataViewRowState.CurrentRows).ToTable
    '                                                If dtStatus IsNot Nothing Then
    '                                                    If dtStatus.Rows.Count > 0 Then
    '                                                        objLoanStatusTran._Loanstatustranunkid = CInt(dtStatus.Rows(0)("loanstatustranunkid"))
    '                                                        objLoanStatusTran._Isvoid = True
    '                                                        objLoanStatusTran._Voiddatetime = Now
    '                                                        objLoanStatusTran._Voiduserunkid = 0
    '                                                        blnFlag = objLoanStatusTran.Update()
    '                                                        If blnFlag = False Then objDataOperation.ReleaseTransaction(False)
    '                                                    End If
    '                                                End If
    '                                            End If

    '                                        End If

    '                                    End If


    '                                    If objDataOperation.ErrorMessage <> "" Then
    '                                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
    '                                        Throw exForce
    '                                    End If

    '                                ElseIf CDec(drRow("savingtranunkid")) > 0 Then

    '                                    ' FOR UPDATE BALANCE IN SAVING TRAN TABLE 

    '                                    'Sohail (15 Dec 2010) -- Start
    '                                    'Changes:Total_contribution field added to update
    '                                    'strQ = " Update svsaving_tran set " & _
    '                                    '      " balance_amount = balance_amount - @amount " & _
    '                                    '      " WHERE savingtranunkid = @savingtranunkid "
    '                                    strQ = " Update svsaving_tran set " & _
    '                                          " balance_amount = balance_amount - @amount, " & _
    '                                          " total_contribution = total_contribution - @amount " & _
    '                                          " WHERE savingtranunkid = @savingtranunkid "
    '                                    'Sohail (15 Dec 2010) -- End

    '                                    objDataOperation.ClearParameters()
    '                                    objDataOperation.AddParameter("@amount", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, drRow("amount").ToString()) 'Sohail (11 May 2011)
    '                                    objDataOperation.AddParameter("@savingtranunkid", SqlDbType.Money, eZeeDataType.MONEY_SIZE, drRow("savingtranunkid").ToString())
    '                                    objDataOperation.ExecNonQuery(strQ)


    '                                    If objDataOperation.ErrorMessage <> "" Then
    '                                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
    '                                        Throw exForce
    '                                    End If

    '                                End If

    '                            Next

    '                            'Sohail (21 Mar 2014) -- Start
    '                            'Enhancement - Set Limit for C/F Netpay balance and Payment JV Report
    '                            If Math.Abs(mdecRoundingAdjustment) > 0 Then
    '                                Dim objTranHead As New clsTransactionHead
    '                                Dim mintRoundingAdjustmentHeadID As Integer = objTranHead.GetRoundingAdjustmentHeadID()

    '                                strQ = "UPDATE  prpayrollprocess_tran " & _
    '                                        "SET     isvoid = 1 " & _
    '                                              ", voiduserunkid = 1 " & _
    '                                              ", voiddatetime = GETDATE() " & _
    '                                              ", voidreason = 'Payment Voided' " & _
    '                                        "WHERE   isvoid = 0 " & _
    '                                                "AND tnaleavetranunkid = @tnaleavetranunkid " & _
    '                                                "AND tranheadunkid = @tranheadunkid " & _
    '                                                "AND amount = @roundingadjustment "

    '                                objDataOperation.ClearParameters()
    '                                objDataOperation.AddParameter("@tranheadunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintRoundingAdjustmentHeadID)
    '                                objDataOperation.AddParameter("@tnaleavetranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPayreftranunkid)
    '                                objDataOperation.AddParameter("@roundingadjustment", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, Math.Abs(mdecRoundingAdjustment))
    '                                objDataOperation.ExecNonQuery(strQ)

    '                                If objDataOperation.ErrorMessage <> "" Then
    '                                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
    '                                    Throw exForce
    '                                End If

    '                                objTranHead = Nothing
    '                            End If
    '                            'Sohail (21 Mar 2014) -- End

    '                            'FOR UPDATE BALANCE  IN TNA LEAVE TRAN TABLE
    '                            'Sohail (21 Mar 2014) -- Start
    '                            'Enhancement - Set Limit for C/F Netpay balance and Payment JV Report
    '                            'strQ = " Update prtnaleave_tran set " & _
    '                            '       " balanceamount = balanceamount + @amount " & _
    '                            '       " WHERE tnaleavetranunkid = @tnaleavetranunkid "
    '                            strQ = " Update prtnaleave_tran set " & _
    '                                   " balanceamount = balanceamount + @amount + @roundingadjustment " & _
    '                                   " WHERE tnaleavetranunkid = @tnaleavetranunkid "
    '                            'Sohail (21 Mar 2014) -- End

    '                            objDataOperation.ClearParameters()
    '                            objDataOperation.AddParameter("@amount", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecAmount) 'Sohail (11 May 2011)
    '                            objDataOperation.AddParameter("@tnaleavetranunkid", SqlDbType.Money, eZeeDataType.MONEY_SIZE, mintPayreftranunkid)
    '                            objDataOperation.AddParameter("@roundingadjustment", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecRoundingAdjustment) 'Sohail (21 Mar 2014)
    '                            objDataOperation.ExecNonQuery(strQ)


    '                            If objDataOperation.ErrorMessage <> "" Then
    '                                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
    '                                Throw exForce
    '                            End If

    '                        End If

    '                    End If
    '                    objpayrollProcess = Nothing
    '                End If

    '                'Sohail (04 Mar 2011) -- Start
    '            Case enPaymentRefId.SAVINGS

    '                strQ = " Update svsaving_tran set " & _
    '                            " balance_amount = balance_amount + @amount " & _
    '                      " WHERE savingtranunkid = @savingtranunkid "

    '                objDataOperation.ClearParameters()
    '                objDataOperation.AddParameter("@amount", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecAmount) 'Sohail (11 May 2011)
    '                objDataOperation.AddParameter("@savingtranunkid", SqlDbType.Money, eZeeDataType.MONEY_SIZE, mintPayreftranunkid)
    '                objDataOperation.ExecNonQuery(strQ)


    '                If objDataOperation.ErrorMessage <> "" Then
    '                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
    '                    Throw exForce
    '                End If
    '                'Sohail (04 Mar 2011) -- End

    '        End Select

    '        objDataOperation.ReleaseTransaction(True)

    '        Return True
    '    Catch ex As Exception
    '        objDataOperation.ReleaseTransaction(False)
    '        Throw New Exception(ex.Message & "; Procedure Name: Delete; Module Name: " & mstrModuleName)
    '        Return False
    '    Finally
    '        exForce = Nothing
    '        If dsList IsNot Nothing Then dsList.Dispose()
    '        objDataOperation = Nothing
    '    End Try
    'End Function
    Public Function Delete(ByVal xDatabaseName As String _
                           , ByVal xUserUnkid As Integer _
                           , ByVal xYearUnkid As Integer _
                           , ByVal xCompanyUnkid As Integer _
                           , ByVal xPeriodStart As DateTime _
                           , ByVal xPeriodEnd As DateTime _
                           , ByVal xUserModeSetting As String _
                           , ByVal xOnlyApproved As Boolean _
                           , ByVal xIncludeIn_ActiveEmployee As Boolean _
                           , ByVal intUnkid As Integer _
                           , ByVal dtCurrentDateAndTime As Date _
                           , Optional ByVal blnApplyUserAccessFilter As Boolean = True _
                           , Optional ByVal strFilerString As String = "" _
                           , Optional ByVal mdtActivityAdjustment As DataTable = Nothing _
                           ) As Boolean
        'Sohail (23 May 2017) - [mdtActivityAdjustment]
        'Sohail (21 Aug 2015) - [dtCurrentDateAndTime]

        '***************************
        '   IF YOU CHANGE ANYTHING IN THIS METHOD PLEASE GIVE SAME EFFECT IN OTHER OVERLOADED DELETE FUNCTION AS WELL.
        '***************************

        Dim objDataOperation As clsDataOperation

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation
        objDataOperation.BindTransaction()
        Try

            'If mstrClientIP.Trim = "" Then mstrClientIP = getIP()
            'If mstrHostName.Trim = "" Then mstrHostName = getHostName()

            objDataOperation.AddParameter("@form_name", SqlDbType.NVarChar, 500, mstrFormName)
            objDataOperation.AddParameter("@isweb", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsWeb)

            strQ = "INSERT INTO atprempsalary_tran " & _
                            "( empsalarytranunkid, paymenttranunkid, paymentdate, employeeunkid, empbanktranid, amount, audittype, audituserunkid, auditdatetime, ip, machine_name, basecurrencyid, baseexchangerate, paidcurrencyid, expaidrate, expaidamt, form_name, isweb ) " & _
                    "SELECT " & _
                         "empsalarytranunkid, paymenttranunkid, paymentdate, employeeunkid, empbanktranid, amount, 3, @voiduserunkid, @voiddatetime, '" & mstrClientIP & "', '" & mstrHostName & "', basecurrencyid, baseexchangerate, paidcurrencyid, expaidrate, expaidamt, @form_name, @isweb " & _
                    "FROM prempsalary_tran " & _
                         "WHERE ISNULL(isvoid, 0) = 0 " & _
                         "AND paymenttranunkid = @paymenttranunkid "

            objDataOperation.AddParameter("@paymenttranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
            If mdtVoiddatetime = Nothing Then
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            Else
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime)
            End If

            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If


            strQ = "UPDATE prempsalary_tran SET " & _
                      "  isvoid = @isvoid" & _
                      ", voiduserunkid = @voiduserunkid" & _
                      ", voiddatetime = @voiddatetime " & _
                      ", voidreason = @voidreason " & _
            "WHERE paymenttranunkid = @paymenttranunkid "

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            '*** Delete entries from prempsalary_tran Table
            strQ = "UPDATE prpayment_tran SET " & _
                      "  isvoid = @isvoid" & _
                      ", voiduserunkid = @voiduserunkid" & _
                      ", voiddatetime = @voiddatetime " & _
                      ", voidreason = @voidreason " & _
            "WHERE paymenttranunkid = @paymenttranunkid "

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@paymenttranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
            If mdtVoiddatetime = Nothing Then
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            Else
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime)
            End If

            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If


            If mstrWebFormName.Trim.Length <= 0 Then
                objDataOperation.AddParameter("@form_name", SqlDbType.NVarChar, 500, mstrFormName)
                objDataOperation.AddParameter("@isweb", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsWeb)

                objDataOperation.AddParameter("@loginemployeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, -1) 'mintLogEmployeeUnkid 'Sohail (13 Feb 2013)
            Else
                objDataOperation.AddParameter("@form_name", SqlDbType.NVarChar, 500, mstrWebFormName)
                objDataOperation.AddParameter("@isweb", SqlDbType.Bit, eZeeDataType.BIT_SIZE, True)
                objDataOperation.AddParameter("@module_name1", SqlDbType.NVarChar, 500, Language.getMessage(mstrModuleName, 2, "WEB"))
                objDataOperation.AddParameter("@loginemployeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLogEmployeeUnkid) 'Sohail (13 Feb 2013)
            End If

            strQ = "INSERT INTO atprpayment_authorize_tran " & _
                            "( paymentauthorizetranunkid, paymenttranunkid, authorize_date, isauthorized, audittype, audituserunkid, auditdatetime, ip, machine_name, form_name, isweb, remarks ) " & _
                    "SELECT " & _
                         "paymentauthorizetranunkid, paymenttranunkid, authorize_date, isauthorized, 3, @voiduserunkid, @voiddatetime, '" & mstrClientIP & "', '" & mstrHostName & "', @form_name, @isweb, remarks " & _
                    "FROM prpayment_authorize_tran " & _
                    "WHERE isactive = 1 " & _
                         "AND paymenttranunkid = @paymenttranunkid "

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            'TRA - ENHANCEMENT - *** Delete entries from prpayment_authorize_tran Table
            strQ = "UPDATE  prpayment_authorize_tran " & _
                    "SET     isactive = 0 " & _
                          ", voiddatetime = @voiddatetime " & _
                          ", voiduserunkid = @voiduserunkid " & _
                          ", voidreason = @voidreason " & _
                    "WHERE   paymenttranunkid = @paymenttranunkid "

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            strQ = "INSERT INTO atprpayment_approval_tran " & _
                            "( paymentapprovaltranunkid, paymenttranunkid, levelunkid, priority, approval_date, statusunkid, disapprovalreason, audittype, audituserunkid, auditdatetime, ip, machine_name, form_name, isweb, loginemployeeunkid, remarks ) " & _
                    "SELECT " & _
                         "paymentapprovaltranunkid, paymenttranunkid, levelunkid, priority, approval_date, statusunkid, disapprovalreason, 3, @voiduserunkid, @voiddatetime, '" & mstrClientIP & "', '" & mstrHostName & "', @form_name, @isweb, @loginemployeeunkid, remarks " & _
                    "FROM     prpayment_approval_tran " & _
                    "WHERE ISNULL(isvoid, 0) = 0 " & _
                    "AND paymenttranunkid = @paymenttranunkid "

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            strQ = "UPDATE  prpayment_approval_tran " & _
                    "SET     isvoid = 1 " & _
                          ", voiddatetime = @voiddatetime " & _
                          ", voiduserunkid = @voiduserunkid " & _
                          ", voidreason = @voidreason " & _
                    "WHERE   paymenttranunkid = @paymenttranunkid " & _
                    "AND ISNULL(isvoid, 0) = 0 "

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If
            With objCashDenome
                ._FormName = mstrFormName
                ._LoginEmployeeunkid = mintLogEmployeeUnkid
                ._ClientIP = mstrClientIP
                ._HostName = mstrHostName
                ._FromWeb = mblnIsWeb
                ._AuditUserId = mintAuditUserId
                ._CompanyUnkid = mintCompanyUnkid
                ._AuditDate = mdtAuditDate
            End With
            If objCashDenome.VoidCashDenomination(intUnkid, mblnIsvoid, mintVoiduserunkid, mdtVoiddatetime, mstrVoidreason, objDataOperation) = False Then
                objDataOperation.ReleaseTransaction(False)
                Return False
            End If

            'Sohail (07 May 2015) -- Start
            'Enhancement - New Loan Redesign with allow to change interest rate, tenure, topup, receipt.
            Dim intVoidUserId As Integer = mintVoiduserunkid
            Dim strVoidReason As String = mstrVoidreason
            'Sohail (07 May 2015) -- End             

            _DataOperation = objDataOperation

            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'Me._Paymenttranunkid = intUnkid
            Call GetData(objDataOperation, intUnkid)
            'Sohail (21 Aug 2015) -- End

            'Sohail (07 May 2015) -- Start
            'Enhancement - New Loan Redesign with allow to change interest rate, tenure, topup, receipt.
            Dim intLoanBalanceTranUnkId As Integer = -1
            If mintPaymentrefid = enPaymentRefId.LOAN AndAlso mintPaymentTypeId = enPayTypeId.RECEIVED Then
                Dim objLoan As New clsLoan_Advance
                Dim ds As DataSet = objLoan.GetLastLoanBalance("List", mintPayreftranunkid)
                If Not (ds.Tables("List").Rows.Count > 0 AndAlso CInt(ds.Tables("List").Rows(0).Item("paymenttranunkid")) = intUnkid) Then
                    Dim strMsg As String = " Loan"
                    If ds.Tables("List").Rows.Count > 0 Then
                        If CInt(ds.Tables("List").Rows(0).Item("payrollprocesstranunkid")) > 0 Then
                            strMsg = " Process Payroll"
                        ElseIf CInt(ds.Tables("List").Rows(0).Item("paymenttranunkid")) > 0 Then
                            strMsg = " Payment List"
                        ElseIf CInt(ds.Tables("List").Rows(0).Item("lninteresttranunkid")) > 0 Then
                            strMsg = " Loan Interest Rate"
                        ElseIf CInt(ds.Tables("List").Rows(0).Item("lnemitranunkid")) > 0 Then
                            strMsg = " Loan EMI"
                        ElseIf CInt(ds.Tables("List").Rows(0).Item("lntopuptranunkid")) > 0 Then
                            strMsg = " Loan Topup"
                        End If
                    End If
                    mstrMessage = Language.getMessage(mstrModuleName, 32, "Sorry, You cannot delete this transaction. Please delete last transaction from the screen of") & strMsg
                    objDataOperation.ReleaseTransaction(False)
                    Return False
                Else
                    intLoanBalanceTranUnkId = CInt(ds.Tables("List").Rows(0).Item("loanbalancetranunkid"))
                End If
            End If
            'Sohail (07 May 2015) -- End

            'Issue : In main table entry was get inserted if there any error occurs at time of insertion in AT table.
            'Call InsertAuditTrailForPaymentTran(objDataOperation, 3)
            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'If InsertAuditTrailForPaymentTran(objDataOperation, 3) = False Then
            If InsertAuditTrailForPaymentTran(objDataOperation, 3, dtCurrentDateAndTime) = False Then
                'Sohail (21 Aug 2015) -- End
                objDataOperation.ReleaseTransaction(False)
                Return False
            End If

            Dim blnFlag As Boolean = False
            Select Case mintPaymentrefid
                Case enPaymentRefId.LOAN, enPaymentRefId.ADVANCE
                    Select Case mintPaymentTypeId
                        Case enPayTypeId.PAYMENT
                            objLoan_Advance._DataOpr = objDataOperation
                            objLoan_Advance._Loanadvancetranunkid = mintPayreftranunkid
                            '<TODO Please Put One Check Point of Payment......!! >

                            Dim dsDataList As New DataSet

                            'Sohail (21 Aug 2015) -- Start
                            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                            'dsDataList = GetList("Test", mintPaymentrefid, mintPayreftranunkid, mintPaymentTypeId, , strUserAccessFilter, objDataOperation)
                            dsDataList = GetList(xDatabaseName, xUserUnkid, xYearUnkid, xCompanyUnkid, xPeriodStart, xPeriodEnd, xUserModeSetting, xOnlyApproved, xIncludeIn_ActiveEmployee, "Test", mintPaymentrefid, mintPayreftranunkid, mintPaymentTypeId, True, , objDataOperation)
                            'Sohail (21 Aug 2015) -- End


                            If dsDataList.Tables(0).Rows.Count > 0 Then
                                'objDataOperation.ReleaseTransaction(True)
                            Else
                                'If objLoan_Advance._Balance_Amount = 0 Then
                                '    If objLoan_Advance._Isloan = True Then
                                '        objLoan_Advance._Balance_Amount = objLoan_Advance._Net_Amount
                                '    Else
                                '        objLoan_Advance._Balance_Amount = objLoan_Advance._Advance_Amount
                                '    End If
                                '    blnFlag = objLoan_Advance.Update
                                '    If blnFlag = False Then objDataOperation.ReleaseTransaction(False)
                                'End If

                                objLoan_Advance._Balance_Amount = 0
                                objLoan_Advance._Balance_AmountPaidCurrency = 0 'Sohail (07 May 2015)
                                'objLoan_Advance._ClientIP = mstrClientIP
                                'objLoan_Advance._WebFormName = mstrWebFormName
                                'objLoan_Advance._HostName = mstrHostName

                                objLoan_Advance._ClientIP = mstrClientIP
                                objLoan_Advance._FormName = mstrFormName
                                objLoan_Advance._HostName = mstrHostName
                                objLoan_Advance._AuditUserId = mintAuditUserId
                                objLoan_Advance._CompanyUnkid = mintCompanyUnkid
                                objLoan_Advance._AuditDate = mdtAuditDate
                                objLoan_Advance._FromWeb = mblnIsWeb
                                objLoan_Advance._LoginEmployeeUnkid = mintLogEmployeeUnkid

                                'Nilay (05-May-2016) -- Start
                                'blnFlag = objLoan_Advance.Update(mintVoiduserunkid, objDataOperation)
                                blnFlag = objLoan_Advance.Update(objDataOperation)
                                'Nilay (05-May-2016) -- End

                                If blnFlag = False Then objDataOperation.ReleaseTransaction(False)
                            End If
                        Case enPayTypeId.RECEIVED
                            'Sohail (07 May 2015) -- Start
                            'Enhancement - New Loan Redesign with allow to change interest rate, tenure, topup, receipt.
                            If mintPaymentrefid = enPaymentRefId.LOAN Then
                                strQ = "UPDATE lnloan_balance_tran SET isvoid = 1, voiduserunkid = " & intVoidUserId & ", voiddatetime = GETDATE(), voidreason = '" & strVoidReason & "' WHERE lnloan_balance_tran.loanbalancetranunkid= " & intLoanBalanceTranUnkId & " "
                                objDataOperation.ExecNonQuery(strQ)
                                If objDataOperation.ErrorMessage <> "" Then
                                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                    Throw exForce
                                End If
                            End If
                            'Sohail (07 May 2015) -- End
                            objLoan_Advance._DataOpr = objDataOperation
                            'Sohail (07 May 2015) -- Start
                            'Enhancement - New Loan Redesign with allow to change interest rate, tenure, topup, receipt.
                            Dim dsLoan As DataSet = Nothing
                            'Nilay (25-Mar-2016) -- Start
                            'dsLoan = objLoan_Advance.Calculate_LoanBalanceInfo("List", mdtPaymentdate, "", mintPayreftranunkid, , False, , , True)
                            dsLoan = objLoan_Advance.Calculate_LoanBalanceInfo(xDatabaseName, xUserUnkid, xYearUnkid, xCompanyUnkid, _
                                                                               xPeriodStart, xPeriodEnd, xUserModeSetting, xOnlyApproved, _
                                                                               "List", mdtPaymentdate, "", mintPayreftranunkid, , False, , , True)
                            'Nilay (25-Mar-2016) -- End

                            'Sohail (07 May 2015) -- End
                            objLoan_Advance._Loanadvancetranunkid = mintPayreftranunkid
                            objLoan_Advance._Balance_Amount = objLoan_Advance._Balance_Amount + mdecAmount
                            'Sohail (07 May 2015) -- Start
                            'Enhancement - New Loan Redesign with allow to change interest rate, tenure, topup, receipt.
                            objLoan_Advance._Balance_AmountPaidCurrency = objLoan_Advance._Balance_AmountPaidCurrency + mdecExpaidamt
                            If dsLoan.Tables(0).Rows.Count > 0 Then
                                objLoan_Advance._Balance_Amount = objLoan_Advance._Balance_Amount + CDec(dsLoan.Tables(0).Rows(0).Item("TotPrincipalAmount"))
                                objLoan_Advance._Balance_AmountPaidCurrency = objLoan_Advance._Balance_AmountPaidCurrency + CDec(dsLoan.Tables(0).Rows(0).Item("TotPrincipalAmountPaidCurrency"))
                            End If
                            'Sohail (07 May 2015) -- End

                            'objLoan_Advance._ClientIP = mstrClientIP
                            'objLoan_Advance._WebFormName = mstrWebFormName
                            'objLoan_Advance._HostName = mstrHostName

                            objLoan_Advance._ClientIP = mstrClientIP
                            objLoan_Advance._FormName = mstrFormName
                            objLoan_Advance._HostName = mstrHostName
                            objLoan_Advance._AuditUserId = mintAuditUserId
                            objLoan_Advance._CompanyUnkid = mintCompanyUnkid
                            objLoan_Advance._AuditDate = mdtAuditDate
                            objLoan_Advance._FromWeb = mblnIsWeb
                            objLoan_Advance._LoginEmployeeUnkid = mintLogEmployeeUnkid

                            'Nilay (05-May-2016) -- Start
                            'blnFlag = objLoan_Advance.Update(mintVoiduserunkid, objDataOperation)
                            blnFlag = objLoan_Advance.Update(objDataOperation)
                            'Nilay (05-May-2016) -- End

                            If blnFlag = False Then objDataOperation.ReleaseTransaction(False)
                    End Select

                Case enPaymentRefId.PAYSLIP

                    If mintPaymentTypeId = enPayTypeId.PAYMENT Then

                        'Sohail (07 May 2015) -- Start
                        'Enhancement - New Loan Redesign with allow to change interest rate, tenure, topup, receipt.
                        Dim objPeriod As New clscommom_period_Tran
                        'Nilay (10-Oct-2015) -- Start
                        'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                        'objPeriod._Periodunkid = mintPeriodunkid
                        objPeriod._Periodunkid(xDatabaseName) = mintPeriodunkid
                        'Nilay (10-Oct-2015) -- End
                        Dim dsLoan As DataSet = Nothing
                        'Nilay (25-Mar-2016) -- Start
                        'dsLoan = objLoan_Advance.Calculate_LoanBalanceInfo("List", objPeriod._End_Date.AddDays(1), "", 0, objPeriod._Start_Date, True, , , True, , , , True)
                        'Sohail (02 Jan 2017) -- Start
                        'TRA Enhancement - 64.1 - Performance enhancement in process payroll, and global void payment.
                        'dsLoan = objLoan_Advance.Calculate_LoanBalanceInfo(xDatabaseName, xUserUnkid, xYearUnkid, xCompanyUnkid, _
                        '                                                   xPeriodStart, xPeriodEnd, xUserModeSetting, xOnlyApproved, _
                        '                                                   "List", objPeriod._End_Date.AddDays(1), "", 0, objPeriod._Start_Date, True, , , True, , , , True)
                        dsLoan = objLoan_Advance.Calculate_LoanBalanceInfo(xDatabaseName, xUserUnkid, xYearUnkid, xCompanyUnkid, _
                                                                           xPeriodStart, xPeriodEnd, xUserModeSetting, xOnlyApproved, _
                                                                           "List", objPeriod._End_Date.AddDays(1), mintEmployeeunkid.ToString, 0, objPeriod._Start_Date, True, , , True, , , , True)
                        'Sohail (02 Jan 2017) -- End
                        'Nilay (25-Mar-2016) -- End

                        'Sohail (07 May 2015) -- End

                        Dim objpayrollProcess As New clsPayrollProcessTran
                        'Sohail (21 Aug 2015) -- Start
                        'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                        'Dim dsProcess As DataSet = objpayrollProcess.GetList("List", mintEmployeeunkid, mintPeriodunkid, mintPayreftranunkid, , mstrAccessLevelFilterString)
                        'Sohail (02 Jan 2017) -- Start
                        'TRA Enhancement - 64.1 - Performance enhancement in process payroll, and global void payment.
                        'Dim dsProcess As DataSet = objpayrollProcess.GetList(xDatabaseName, xUserUnkid, xYearUnkid, xCompanyUnkid, xPeriodStart, xPeriodEnd, xUserModeSetting, xOnlyApproved, xIncludeIn_ActiveEmployee, "List", mintEmployeeunkid, mintPeriodunkid, mintPayreftranunkid, blnApplyUserAccessFilter, strFilerString)
                        Dim dsProcess As DataSet = objpayrollProcess.GetList(xDatabaseName, xUserUnkid, xYearUnkid, xCompanyUnkid, xPeriodStart, xPeriodEnd, xUserModeSetting, xOnlyApproved, xIncludeIn_ActiveEmployee, "List", mintEmployeeunkid, mintPeriodunkid, mintPayreftranunkid, blnApplyUserAccessFilter, " (prpayrollprocess_tran.loanadvancetranunkid > 0 OR prpayrollprocess_tran.savingtranunkid > 0) ")
                        'Sohail (02 Jan 2017) -- End
                        'Sohail (21 Aug 2015) -- End

                        If dsProcess IsNot Nothing Then
                            If dsProcess.Tables(0).Rows.Count > 0 Then
                                For Each drRow As DataRow In dsProcess.Tables(0).Rows
                                    If CDec(drRow("loanadvancetranunkid")) > 0 Then

                                        ' FOR UPDATE BALANCEs IN LOAN ADVANCE TRAN TABLE 
                                        'Sohail (07 May 2015) -- Start
                                        'Enhancement - New Loan Redesign with allow to change interest rate, tenure, topup, receipt.
                                        'strQ = " Update lnloan_advance_tran set " & _
                                        '       "  balance_amount = balance_amount + @amount " & _
                                        '       " WHERE loanadvancetranunkid = @loanadvancetranunkid "
                                        'objDataOperation.ClearParameters()
                                        'objDataOperation.AddParameter("@amount", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, drRow("amount").ToString()) 'Sohail (11 May 2011)
                                        'objDataOperation.AddParameter("@loanadvancetranunkid", SqlDbType.Money, eZeeDataType.MONEY_SIZE, drRow("loanadvancetranunkid").ToString())
                                        'objDataOperation.AddParameter("@amountPaidCurrency", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, drRow("amountPaidCurrency").ToString()) 'Sohail (07 May 2015)
                                        'objDataOperation.ExecNonQuery(strQ)
                                        Dim dr_Row() As DataRow = dsLoan.Tables("List").Select("loanadvancetranunkid = " & CInt(drRow("loanadvancetranunkid")) & " ")
                                        If dr_Row.Length > 0 Then
                                            'Sohail (30 Jan 2020) -- Start
                                            'Tujijenge Tanzania Issue # 0004470 : Incomplete Loans Showing As Complete
                                            'strQ = " Update lnloan_advance_tran set " & _
                                            '       " balance_amount = balance_amount + @amount " & _
                                            '           ", balance_amountPaidCurrency = balance_amountPaidCurrency + @amountPaidCurrency " & _
                                            '       " WHERE loanadvancetranunkid = @loanadvancetranunkid "
                                            strQ = " Update lnloan_advance_tran set " & _
                                                   " balance_amount = @amount " & _
                                                       ", balance_amountPaidCurrency = @amountPaidCurrency " & _
                                                   " WHERE loanadvancetranunkid = @loanadvancetranunkid "
                                            'Sohail (30 Jan 2020) -- End

                                            objDataOperation.ClearParameters()
                                            'Sohail (30 Jan 2020) -- Start
                                            'Tujijenge Tanzania Issue # 0004470 : Incomplete Loans Showing As Complete
                                            'objDataOperation.AddParameter("@amount", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, CDec(dr_Row(0).Item("TotPrincipalAmount")))
                                            'objDataOperation.AddParameter("@amountPaidCurrency", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, CDec(dr_Row(0).Item("TotPrincipalAmountPaidCurrency")))
                                            'objDataOperation.AddParameter("@loanadvancetranunkid", SqlDbType.Money, eZeeDataType.MONEY_SIZE, drRow("loanadvancetranunkid").ToString())
                                            'Sohail (16 Apr 2020) -- Start
                                            'EKO SUPREME NIGERIA issue # 0004654 : Advance amount disappears when user voids payment.
                                            'objDataOperation.AddParameter("@amount", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, CDec(dr_Row(0).Item("BalanceAmount")) + CDec(drRow.Item("principal_amount")))
                                            'objDataOperation.AddParameter("@amountPaidCurrency", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, CDec(dr_Row(0).Item("BalanceAmountPaidCurrency")) + CDec(drRow.Item("principal_amountPaidCurrency")))
                                            If CBool(dr_Row(0).Item("isloan")) = True Then
                                                'Hemant (24 Jun 2020) -- Start
                                                'ISSUE(VOLTAMP) :  Advance not taking in Payroll Process                                                
                                                'objDataOperation.AddParameter("@amount", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, CDec(dr_Row(0).Item("BalanceAmount")) + CDec(drRow.Item("principal_amount")))
                                                'objDataOperation.AddParameter("@amountPaidCurrency", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, CDec(dr_Row(0).Item("BalanceAmountPaidCurrency")) + CDec(drRow.Item("principal_amountPaidCurrency")))
                                                objDataOperation.AddParameter("@amount", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, CDec(dr_Row(0).Item("Balance_Amount")) + CDec(drRow.Item("principal_amount")))
                                                objDataOperation.AddParameter("@amountPaidCurrency", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, CDec(dr_Row(0).Item("Balance_AmountPaidCurrency")) + CDec(drRow.Item("principal_amountPaidCurrency")))
                                                'Hemant (24 Jun 2020) -- End                                                
                                            Else 'Advance
                                                objDataOperation.AddParameter("@amount", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, CDec(dr_Row(0).Item("BalanceAmount")) + CDec(drRow.Item("amount")))
                                                objDataOperation.AddParameter("@amountPaidCurrency", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, CDec(dr_Row(0).Item("BalanceAmountPaidCurrency")) + CDec(drRow.Item("amountPaidCurrency")))
                                            End If
                                            'Sohail (16 Apr 2020) -- End
                                            objDataOperation.AddParameter("@loanadvancetranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, drRow("loanadvancetranunkid").ToString())
                                            'Sohail (30 Jan 2020) -- End

                                            objDataOperation.ExecNonQuery(strQ)

                                            If objDataOperation.ErrorMessage <> "" Then
                                                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                                Throw exForce
                                            End If
                                        End If
                                        'Sohail (07 May 2015) -- End

                                        strQ = "Select balance_amount, balance_amountPaidCurrency from lnloan_advance_tran  " & _
                                         " WHERE loanadvancetranunkid = @loanadvancetranunkid "
                                        'Sohail (07 May 2015) - [balance_amountPaidCurrency]

                                        objDataOperation.ClearParameters()
                                        'Sohail (30 Jan 2020) -- Start
                                        'Tujijenge Tanzania Issue # 0004470 : Incomplete Loans Showing As Complete
                                        'objDataOperation.AddParameter("@loanadvancetranunkid", SqlDbType.Money, eZeeDataType.MONEY_SIZE, drRow("loanadvancetranunkid").ToString())
                                        objDataOperation.AddParameter("@loanadvancetranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, drRow("loanadvancetranunkid").ToString())
                                        'Sohail (30 Jan 2020) -- End
                                        Dim dsAmount As DataSet = objDataOperation.ExecQuery(strQ, "Balance")

                                        If objDataOperation.ErrorMessage <> "" Then
                                            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                            Throw exForce
                                        End If

                                        If dsAmount IsNot Nothing Then

                                            If dsAmount.Tables("Balance").Rows.Count > 0 Then

                                                'Sohail (07 May 2015) -- Start
                                                'Enhancement - New Loan Redesign with allow to change interest rate, tenure, topup, receipt.
                                                'If CDec(dsAmount.Tables("Balance").Rows(0)("balance_amount")) > 0 Then 'Sohail (11 May 2011)
                                                If CDec(dsAmount.Tables("Balance").Rows(0)("balance_amountPaidCurrency")) > 0 Then
                                                    'Sohail (07 May 2015) -- End

                                                    strQ = " Update lnloan_advance_tran set " & _
                                                          " loan_statusunkid = 1 " & _
                                                          " WHERE loanadvancetranunkid = @loanadvancetranunkid "
                                                    objDataOperation.ClearParameters()
                                                    objDataOperation.AddParameter("@loanadvancetranunkid", SqlDbType.Money, eZeeDataType.MONEY_SIZE, drRow("loanadvancetranunkid").ToString())
                                                    objDataOperation.ExecNonQuery(strQ)

                                                    If objDataOperation.ErrorMessage <> "" Then
                                                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                                        Throw exForce
                                                    End If

                                                    'Sohail (04 Jun 2018) -- Start
                                                    'CCK Issue - Support Issue Id # 0002297 - Mismatch of amount deducted from loan between payroll report and loan report. Tried running the Written off loan tool but experienced an error on it. (Completed loans were coming in process payroll) in 72.1.
                                                    'Dim dsStatus As DataSet = objLoanStatusTran.GetList("LoanStatus", True)
                                                    'Dim dtStatus As DataTable = New DataView(dsStatus.Tables("LoanStatus"), "loanadvancetranunkid= " & CInt(drRow("loanadvancetranunkid")) & " AND statusunkid = 4 AND isvoid = 0", "", DataViewRowState.CurrentRows).ToTable
                                                    Dim dsStatus As DataSet = objLoanStatusTran.GetList("LoanStatus", True, mintPeriodunkid, mintEmployeeunkid)
                                                    Dim dtStatus As DataTable = New DataView(dsStatus.Tables("LoanStatus"), "loanadvancetranunkid= " & CInt(drRow("loanadvancetranunkid")) & " AND statusunkid = 4 AND isvoid = 0 AND periodunkid = " & mintPeriodunkid & "", "", DataViewRowState.CurrentRows).ToTable
                                                    'Sohail (04 Jun 2018) -- End
                                                    If dtStatus IsNot Nothing Then
                                                        If dtStatus.Rows.Count > 0 Then
                                                            objLoanStatusTran._Loanstatustranunkid = CInt(dtStatus.Rows(0)("loanstatustranunkid"))
                                                            objLoanStatusTran._Isvoid = True
                                                            'Nilay (20-Sept-2016) -- Start
                                                            'Enhancement : Cancel feature for approved but not assigned loan application
                                                            'objLoanStatusTran._Voiddatetime = Now
                                                            objLoanStatusTran._Voiddatetime = dtCurrentDateAndTime
                                                            'Nilay (20-Sept-2016) -- End
                                                            objLoanStatusTran._Voiduserunkid = xUserUnkid 'Nilay (01-Apr-2016)
                                                            objLoan_Advance._ClientIP = mstrClientIP
                                                            objLoan_Advance._FormName = mstrFormName
                                                            objLoan_Advance._HostName = mstrHostName
                                                            objLoan_Advance._AuditUserId = mintAuditUserId
                                                            objLoan_Advance._CompanyUnkid = mintCompanyUnkid
                                                            objLoan_Advance._AuditDate = mdtAuditDate
                                                            objLoan_Advance._FromWeb = mblnIsWeb
                                                            objLoan_Advance._LoginEmployeeUnkid = mintLogEmployeeUnkid
                                                            blnFlag = objLoanStatusTran.Update()
                                                            If blnFlag = False Then objDataOperation.ReleaseTransaction(False)

                                                            'Sohail (24 Apr 2019) -- Start
                                                            'NPK Issue - Support Issue Id # 0003771 - 76.1 - Not Able to delete the advance after voiding payroll payment of March (due to transaction was not getting voided from loan_balance_tran on voiding payment).
                                                            strQ = "UPDATE lnloan_balance_tran SET isvoid = 1, voiduserunkid = " & xUserUnkid & ", voiddatetime = GETDATE(), voidreason = '" & strVoidReason & "' WHERE lnloan_balance_tran.loanstatustranunkid= " & CInt(dtStatus.Rows(0)("loanstatustranunkid")) & " "
                                                            objDataOperation.ExecNonQuery(strQ)
                                                            If objDataOperation.ErrorMessage <> "" Then
                                                                objDataOperation.ReleaseTransaction(False)
                                                                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                                                Throw exForce
                                                            End If
                                                            'Sohail (24 Apr 2019) -- End

                                                        End If
                                                    End If
                                                End If

                                            End If

                                        End If


                                        If objDataOperation.ErrorMessage <> "" Then
                                            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                            Throw exForce
                                        End If

                                    ElseIf CDec(drRow("savingtranunkid")) > 0 Then

                                        ' FOR UPDATE BALANCE IN SAVING TRAN TABLE 

                                        'Sohail (15 Dec 2010) -- Start
                                        'Changes:Total_contribution field added to update
                                        'strQ = " Update svsaving_tran set " & _
                                        '      " balance_amount = balance_amount - @amount " & _
                                        '      " WHERE savingtranunkid = @savingtranunkid "
                                        strQ = " Update svsaving_tran set " & _
                                              " balance_amount = balance_amount - @amount, " & _
                                              " total_contribution = total_contribution - @amount " & _
                                              " WHERE savingtranunkid = @savingtranunkid "
                                        'Sohail (15 Dec 2010) -- End

                                        objDataOperation.ClearParameters()
                                        objDataOperation.AddParameter("@amount", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, drRow("amount").ToString()) 'Sohail (11 May 2011)
                                        objDataOperation.AddParameter("@savingtranunkid", SqlDbType.Money, eZeeDataType.MONEY_SIZE, drRow("savingtranunkid").ToString())
                                        objDataOperation.ExecNonQuery(strQ)


                                        If objDataOperation.ErrorMessage <> "" Then
                                            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                            Throw exForce
                                        End If

                                    End If

                                Next
                            End If 'Sohail (24 Jan 2017) 

                            'Sohail (21 Mar 2014) -- Start
                            'Enhancement - Set Limit for C/F Netpay balance and Payment JV Report
                            If Math.Abs(mdecRoundingAdjustment) > 0 Then
                                Dim objTranHead As New clsTransactionHead
                                Dim mintRoundingAdjustmentHeadID As Integer = objTranHead.GetRoundingAdjustmentHeadID()

                                strQ = "UPDATE  prpayrollprocess_tran " & _
                                        "SET     isvoid = 1 " & _
                                              ", voiduserunkid = 1 " & _
                                              ", voiddatetime = GETDATE() " & _
                                              ", voidreason = 'Payment Voided' " & _
                                        "WHERE   isvoid = 0 " & _
                                                "AND tnaleavetranunkid = @tnaleavetranunkid " & _
                                                "AND tranheadunkid = @tranheadunkid " & _
                                                "AND amount = @roundingadjustment "

                                objDataOperation.ClearParameters()
                                objDataOperation.AddParameter("@tranheadunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintRoundingAdjustmentHeadID)
                                objDataOperation.AddParameter("@tnaleavetranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPayreftranunkid)
                                objDataOperation.AddParameter("@roundingadjustment", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, Math.Abs(mdecRoundingAdjustment))
                                objDataOperation.ExecNonQuery(strQ)

                                If objDataOperation.ErrorMessage <> "" Then
                                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                    Throw exForce
                                End If

                                objTranHead = Nothing
                            End If
                            'Sohail (21 Mar 2014) -- End

                            'FOR UPDATE BALANCE  IN TNA LEAVE TRAN TABLE
                            'Sohail (21 Mar 2014) -- Start
                            'Enhancement - Set Limit for C/F Netpay balance and Payment JV Report
                            'strQ = " Update prtnaleave_tran set " & _
                            '       " balanceamount = balanceamount + @amount " & _
                            '       " WHERE tnaleavetranunkid = @tnaleavetranunkid "
                            strQ = " Update prtnaleave_tran set " & _
                                   " balanceamount = balanceamount + @amount + @roundingadjustment " & _
                                   " WHERE tnaleavetranunkid = @tnaleavetranunkid "
                            'Sohail (21 Mar 2014) -- End

                            objDataOperation.ClearParameters()
                            objDataOperation.AddParameter("@amount", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecAmount) 'Sohail (11 May 2011)
                            objDataOperation.AddParameter("@tnaleavetranunkid", SqlDbType.Money, eZeeDataType.MONEY_SIZE, mintPayreftranunkid)
                            objDataOperation.AddParameter("@roundingadjustment", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecRoundingAdjustment) 'Sohail (21 Mar 2014)
                            objDataOperation.ExecNonQuery(strQ)


                            If objDataOperation.ErrorMessage <> "" Then
                                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                Throw exForce
                            End If

                        End If

                        'End If 'Sohail (24 Jan 2017) 
                        objpayrollProcess = Nothing
                    End If

                    'Sohail (04 Mar 2011) -- Start
                Case enPaymentRefId.SAVINGS

                    'Sohail (21 Nov 2015) -- Start
                    'Enhancement - Provide Deposit feaure in Employee Saving.
                    'strQ = " Update svsaving_tran set " & _
                    '            " balance_amount = balance_amount + @amount " & _
                    '      " WHERE savingtranunkid = @savingtranunkid "
                    Select Case mintPaymentTypeId

                        Case enPayTypeId.DEPOSIT
                            strQ = " Update svsaving_tran set " & _
                                        " balance_amount = balance_amount - @amount " & _
                                  " WHERE savingtranunkid = @savingtranunkid "

                        Case Else
                            strQ = " Update svsaving_tran set " & _
                                        " balance_amount = balance_amount + @amount " & _
                                  " WHERE savingtranunkid = @savingtranunkid "
                    End Select
                    'Sohail (21 Nov 2015) -- End

                    objDataOperation.ClearParameters()
                    objDataOperation.AddParameter("@amount", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecAmount) 'Sohail (11 May 2011)
                    objDataOperation.AddParameter("@savingtranunkid", SqlDbType.Money, eZeeDataType.MONEY_SIZE, mintPayreftranunkid)
                    objDataOperation.ExecNonQuery(strQ)


                    If objDataOperation.ErrorMessage <> "" Then
                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If
                    'Sohail (04 Mar 2011) -- End

            End Select

            'Sohail (23 May 2017) -- Start
            'Enhancement - 67.1 - Link budget with Payroll.
            If mdtActivityAdjustment IsNot Nothing AndAlso mdtActivityAdjustment.Rows.Count > 0 Then
                Dim objActAdj As clsFundActivityAdjustment_Tran
                For Each dtRow As DataRow In mdtActivityAdjustment.Rows
                    objActAdj = New clsFundActivityAdjustment_Tran

                    objActAdj._FundActivityunkid = CInt(dtRow.Item("fundactivityunkid"))
                    objActAdj._TransactionDate = eZeeDate.convertDate(dtRow.Item("transactiondate").ToString())
                    objActAdj._CurrentBalance = CDec(dtRow.Item("Current Balance"))
                    objActAdj._IncrDecrAmount = CDec(dtRow.Item("Actual Salary"))
                    objActAdj._NewBalance = CDec(dtRow.Item("Current Balance")) + CDec(dtRow.Item("Actual Salary"))
                    objActAdj._Remark = dtRow.Item("remark").ToString()
                    objActAdj._Userunkid = xUserUnkid
                    objActAdj._Paymenttranunkid = intUnkid
                    objActAdj._Globalvocunkid = 0
                    With objActAdj
                        ._FormName = mstrFormName
                        ._LoginEmployeeunkid = mintLogEmployeeUnkid
                        ._ClientIP = mstrClientIP
                        ._HostName = mstrHostName
                        ._FromWeb = mblnIsWeb
                        ._AuditUserId = mintAuditUserId
                        ._CompanyUnkid = mintCompanyUnkid
                        ._AuditDate = mdtAuditDate
                    End With
                    If objActAdj.Insert(dtCurrentDateAndTime, objDataOperation, True) = False Then
                        objDataOperation.ReleaseTransaction(False)
                        Throw New Exception(objActAdj._Message)
                        Return False
                    End If
                Next
            End If
            'Sohail (23 May 2017) -- End

            objDataOperation.ReleaseTransaction(True)

            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Delete; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function
    'Sohail (21 Aug 2015) -- End

    'Sohail (12 May 2012) -- Start
    'TRA - ENHANCEMENT
    'Sohail (21 Aug 2015) -- Start
    'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
    'Private Function Delete(ByVal objDataOperation As clsDataOperation, ByVal intUnkid As Integer) As Boolean

    '    '***************************
    '    '   IF YOU CHANGE ANYTHING IN THIS METHOD PLEASE GIVE SAME EFFECT IN OTHER OVERLOADED DELETE FUNCTION AS WELL.
    '    '***************************

    '    Dim dsList As DataSet = Nothing
    '    Dim strQ As String = ""
    '    Dim exForce As Exception


    '    Try
    '        objDataOperation.ClearParameters()

    '        'Sohail (18 May 2013) -- Start
    '        'TRA - ENHANCEMENT
    '        If mstrWebIP.Trim = "" Then mstrWebIP = getIP()
    '        If mstrWebhostName.Trim = "" Then mstrWebhostName = getHostName()
    '        'Sohail (18 May 2013) -- End

    '        'Sohail (27 Jul 2012) -- Start
    '        'TRA - ENHANCEMENT - Insert in AT Log
    '        If mstrWebFormName.Trim.Length <= 0 Then
    '            'S.SANDEEP [ 11 AUG 2012 ] -- START
    '            'ENHANCEMENT : TRA CHANGES
    '            'Dim frm As Form
    '            'For Each frm In Application.OpenForms
    '            '    If Form.ActiveForm.Name IsNot Nothing AndAlso Form.ActiveForm.Name = frm.Name Then
    '            '        objDataOperation.AddParameter("@form_name", SqlDbType.NVarChar, 500, frm.Name)
    '            '        objDataOperation.AddParameter("@isweb", SqlDbType.Bit, eZeeDataType.BIT_SIZE, False)
    '            '        
    '            '    End If
    '            'Next
    '            objDataOperation.AddParameter("@form_name", SqlDbType.NVarChar, 500, mstrForm_Name)
    '            objDataOperation.AddParameter("@isweb", SqlDbType.Bit, eZeeDataType.BIT_SIZE, False)
    '            
    '            'S.SANDEEP [ 11 AUG 2012 ] -- END
    '        Else
    '            objDataOperation.AddParameter("@form_name", SqlDbType.NVarChar, 500, mstrWebFormName)
    '            objDataOperation.AddParameter("@isweb", SqlDbType.Bit, eZeeDataType.BIT_SIZE, True)
    '            objDataOperation.AddParameter("@module_name1", SqlDbType.NVarChar, 500, Language.getMessage(mstrModuleName, 2, "WEB"))
    '        End If
    '        
    '        
    '        
    '        

    '        strQ = "INSERT INTO atprempsalary_tran " & _
    '                        "( empsalarytranunkid, paymenttranunkid, paymentdate, employeeunkid, empbanktranid, amount, audittype, audituserunkid, auditdatetime, ip, machine_name, basecurrencyid, baseexchangerate, paidcurrencyid, expaidrate, expaidamt, form_name, module_name1, module_name2, module_name3, module_name4, module_name5, isweb ) " & _
    '                "SELECT " & _
    '                     "empsalarytranunkid, paymenttranunkid, paymentdate, employeeunkid, empbanktranid, amount, 3, @voiduserunkid, @voiddatetime, '" & mstrWebIP & "', '" & mstrWebhostName & "', basecurrencyid, baseexchangerate, paidcurrencyid, expaidrate, expaidamt, @form_name, @module_name1, @module_name2, @module_name3, @module_name4, @module_name5, @isweb " & _
    '                "FROM prempsalary_tran " & _
    '                     "WHERE ISNULL(isvoid, 0) = 0 " & _
    '                     "AND paymenttranunkid = @paymenttranunkid "

    '        objDataOperation.AddParameter("@paymenttranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)
    '        objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
    '        objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
    '        If mdtVoiddatetime = Nothing Then
    '            objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
    '        Else
    '            objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime)
    '        End If

    '        objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)

    '        Call objDataOperation.ExecNonQuery(strQ)

    '        If objDataOperation.ErrorMessage <> "" Then
    '            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
    '            Throw exForce
    '        End If
    '        'Sohail (27 Jul 2012) -- End


    '        strQ = "UPDATE prempsalary_tran SET " & _
    '                  "  isvoid = @isvoid" & _
    '                  ", voiduserunkid = @voiduserunkid" & _
    '                  ", voiddatetime = @voiddatetime " & _
    '                  ", voidreason = @voidreason " & _
    '        "WHERE paymenttranunkid = @paymenttranunkid "

    '        'Sohail (27 Jul 2012) -- Start
    '        'TRA - ENHANCEMENT
    '        'objDataOperation.AddParameter("@paymenttranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)
    '        'objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
    '        'objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
    '        'If mdtVoiddatetime = Nothing Then
    '        '    objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
    '        'Else
    '        '    objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime)
    '        'End If

    '        'objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)
    '        'Sohail (27 Jul 2012) -- End

    '        Call objDataOperation.ExecNonQuery(strQ)

    '        If objDataOperation.ErrorMessage <> "" Then
    '            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
    '            Throw exForce
    '        End If


    '        strQ = "UPDATE prpayment_tran SET " & _
    '                  "  isvoid = @isvoid" & _
    '                  ", voiduserunkid = @voiduserunkid" & _
    '                  ", voiddatetime = @voiddatetime " & _
    '                  ", voidreason = @voidreason " & _
    '        "WHERE paymenttranunkid = @paymenttranunkid "

    '        objDataOperation.ClearParameters()
    '        objDataOperation.AddParameter("@paymenttranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)
    '        objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
    '        objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
    '        If mdtVoiddatetime = Nothing Then
    '            objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
    '        Else
    '            objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime)
    '        End If

    '        objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)

    '        Call objDataOperation.ExecNonQuery(strQ)

    '        If objDataOperation.ErrorMessage <> "" Then
    '            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
    '            Throw exForce
    '        End If


    '        'Sohail (27 Jul 2012) -- Start
    '        'TRA - ENHANCEMENT - Insert in AT Log
    '        If mstrWebFormName.Trim.Length <= 0 Then
    '            'S.SANDEEP [ 11 AUG 2012 ] -- START
    '            'ENHANCEMENT : TRA CHANGES
    '            'Dim frm As Form
    '            'For Each frm In Application.OpenForms
    '            '    If Form.ActiveForm.Name IsNot Nothing AndAlso Form.ActiveForm.Name = frm.Name Then
    '            '        objDataOperation.AddParameter("@form_name", SqlDbType.NVarChar, 500, frm.Name)
    '            '        objDataOperation.AddParameter("@isweb", SqlDbType.Bit, eZeeDataType.BIT_SIZE, False)
    '            '        
    '            '    End If
    '            'Next
    '            objDataOperation.AddParameter("@form_name", SqlDbType.NVarChar, 500, mstrForm_Name)
    '            objDataOperation.AddParameter("@isweb", SqlDbType.Bit, eZeeDataType.BIT_SIZE, False)
    '            
    '            'S.SANDEEP [ 11 AUG 2012 ] -- END
    '            objDataOperation.AddParameter("@loginemployeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, -1) 'mintLogEmployeeUnkid 'Sohail (13 Feb 2013)
    '        Else
    '            objDataOperation.AddParameter("@form_name", SqlDbType.NVarChar, 500, mstrWebFormName)
    '            objDataOperation.AddParameter("@isweb", SqlDbType.Bit, eZeeDataType.BIT_SIZE, True)
    '            objDataOperation.AddParameter("@module_name1", SqlDbType.NVarChar, 500, Language.getMessage(mstrModuleName, 2, "WEB"))
    '            objDataOperation.AddParameter("@loginemployeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLogEmployeeUnkid) 'Sohail (13 Feb 2013)
    '        End If
    '        
    '        
    '        
    '        

    '        strQ = "INSERT INTO atprpayment_authorize_tran " & _
    '                         "( paymentauthorizetranunkid, paymenttranunkid, authorize_date, isauthorized, audittype, audituserunkid, auditdatetime, ip, machine_name, form_name, module_name1, module_name2, module_name3, module_name4, module_name5, isweb, remarks ) " & _
    '                 "SELECT " & _
    '                      "paymentauthorizetranunkid, paymenttranunkid, authorize_date, isauthorized, 3, @voiduserunkid, @voiddatetime, '" & mstrWebIP & "', '" & mstrWebhostName & "', @form_name, @module_name1, @module_name2, @module_name3, @module_name4, @module_name5, @isweb, remarks " & _
    '                 "FROM prpayment_authorize_tran " & _
    '                 "WHERE isactive = 1 " & _
    '                      "AND paymenttranunkid = @paymenttranunkid "

    '        Call objDataOperation.ExecNonQuery(strQ)

    '        If objDataOperation.ErrorMessage <> "" Then
    '            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
    '            Throw exForce
    '        End If
    '        'Sohail (27 Jul 2012) -- End

    '        'Sohail (02 Jul 2012) -- Start
    '        'TRA - ENHANCEMENT - *** Delete entries from prpayment_authorize_tran Table
    '        strQ = "UPDATE  prpayment_authorize_tran " & _
    '                "SET     isactive = 0 " & _
    '                      ", voiddatetime = @voiddatetime " & _
    '                      ", voiduserunkid = @voiduserunkid " & _
    '                      ", voidreason = @voidreason " & _
    '                "WHERE   paymenttranunkid = @paymenttranunkid " 'Sohail (27 Jul 2012) - [voiddatetime, voiduserunkid, voidreason]

    '        Call objDataOperation.ExecNonQuery(strQ)

    '        If objDataOperation.ErrorMessage <> "" Then
    '            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
    '            Throw exForce
    '        End If
    '        'Sohail (02 Jul 2012) -- End

    '        'Sohail (13 Feb 2013) -- Start
    '        'TRA - ENHANCEMENT
    '        strQ = "INSERT INTO atprpayment_approval_tran " & _
    '                        "( paymentapprovaltranunkid, paymenttranunkid, levelunkid, priority, approval_date, statusunkid, disapprovalreason, audittype, audituserunkid, auditdatetime, ip, machine_name, form_name, module_name1, module_name2, module_name3, module_name4, module_name5, isweb, loginemployeeunkid, remarks ) " & _
    '                "SELECT " & _
    '                     "paymentapprovaltranunkid, paymenttranunkid, levelunkid, priority, approval_date, statusunkid, disapprovalreason, 3, @voiduserunkid, @voiddatetime, '" & mstrWebIP & "', '" & mstrWebhostName & "', @form_name, @module_name1, @module_name2, @module_name3, @module_name4, @module_name5, @isweb, @loginemployeeunkid, remarks  " & _
    '                "FROM     prpayment_approval_tran " & _
    '                "WHERE ISNULL(isvoid, 0) = 0 " & _
    '                "AND paymenttranunkid = @paymenttranunkid "

    '        Call objDataOperation.ExecNonQuery(strQ)

    '        If objDataOperation.ErrorMessage <> "" Then
    '            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
    '            Throw exForce
    '        End If

    '        strQ = "UPDATE  prpayment_approval_tran " & _
    '                "SET     isvoid = 1 " & _
    '                      ", voiddatetime = @voiddatetime " & _
    '                      ", voiduserunkid = @voiduserunkid " & _
    '                      ", voidreason = @voidreason " & _
    '                "WHERE   paymenttranunkid = @paymenttranunkid " & _
    '                "AND ISNULL(isvoid, 0) = 0 "

    '        Call objDataOperation.ExecNonQuery(strQ)

    '        If objDataOperation.ErrorMessage <> "" Then
    '            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
    '            Throw exForce
    '        End If
    '        'Sohail (13 Feb 2013) -- End

    '        If objCashDenome.VoidCashDenomination(intUnkid, mblnIsvoid, mintVoiduserunkid, mdtVoiddatetime, mstrVoidreason, objDataOperation) = False Then
    '            objDataOperation.ReleaseTransaction(False)
    '            Return False
    '        End If

    '        _DataOperation = objDataOperation
    '        Me._Paymenttranunkid = intUnkid

    '        If InsertAuditTrailForPaymentTran(objDataOperation, 3) = False Then
    '            objDataOperation.ReleaseTransaction(False)
    '            Return False
    '        End If

    '        Dim blnFlag As Boolean = False
    '        Select Case mintPaymentrefid
    '            Case enPaymentRefId.LOAN, enPaymentRefId.ADVANCE
    '                Select Case mintPaymentTypeId
    '                    Case enPayTypeId.PAYMENT
    '                        objLoan_Advance._Loanadvancetranunkid = mintPayreftranunkid
    '                        '<TODO Please Put One Check Point of Payment......!! >

    '                        Dim dsDataList As New DataSet
    '                        dsDataList = GetList("Test", mintPaymentrefid, mintPayreftranunkid, mintPaymentTypeId)
    '                        If dsDataList.Tables(0).Rows.Count > 0 Then
    '                        Else
    '                            objLoan_Advance._Balance_Amount = 0
    '                            blnFlag = objLoan_Advance.Update
    '                            If blnFlag = False Then objDataOperation.ReleaseTransaction(False)
    '                        End If
    '                    Case enPayTypeId.RECEIVED
    '                        objLoan_Advance._Loanadvancetranunkid = mintPayreftranunkid
    '                        objLoan_Advance._Balance_Amount = objLoan_Advance._Balance_Amount + mdecAmount
    '                        blnFlag = objLoan_Advance.Update
    '                        If blnFlag = False Then objDataOperation.ReleaseTransaction(False)
    '                End Select

    '            Case enPaymentRefId.PAYSLIP

    '                If mintPaymentTypeId = enPayTypeId.PAYMENT Then



    '                    Dim objpayrollProcess As New clsPayrollProcessTran
    '                    Dim dsProcess As DataSet = objpayrollProcess.GetList("List", mintEmployeeunkid, mintPeriodunkid, mintPayreftranunkid, , mstrAccessLevelFilterString)

    '                    If dsProcess IsNot Nothing Then
    '                        If dsProcess.Tables(0).Rows.Count > 0 Then
    '                            For Each drRow As DataRow In dsProcess.Tables(0).Rows
    '                                If CDec(drRow("loanadvancetranunkid")) > 0 Then

    '                                    ' FOR UPDATE BALANCEs IN LOAN ADVANCE TRAN TABLE 

    '                                    strQ = " Update lnloan_advance_tran set " & _
    '                                           " balance_amount = balance_amount + @amount " & _
    '                                           " WHERE loanadvancetranunkid = @loanadvancetranunkid "
    '                                    objDataOperation.ClearParameters()
    '                                    objDataOperation.AddParameter("@amount", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, drRow("amount").ToString()) 'Sohail (11 May 2011)
    '                                    objDataOperation.AddParameter("@loanadvancetranunkid", SqlDbType.Money, eZeeDataType.MONEY_SIZE, drRow("loanadvancetranunkid").ToString())
    '                                    objDataOperation.ExecNonQuery(strQ)


    '                                    strQ = "Select balance_amount from lnloan_advance_tran  " & _
    '                                     " WHERE loanadvancetranunkid = @loanadvancetranunkid "
    '                                    objDataOperation.ClearParameters()
    '                                    objDataOperation.AddParameter("@loanadvancetranunkid", SqlDbType.Money, eZeeDataType.MONEY_SIZE, drRow("loanadvancetranunkid").ToString())
    '                                    Dim dsAmount As DataSet = objDataOperation.ExecQuery(strQ, "Balance")

    '                                    If dsAmount IsNot Nothing Then

    '                                        If dsAmount.Tables("Balance").Rows.Count > 0 Then

    '                                            If CDec(dsAmount.Tables("Balance").Rows(0)("balance_amount")) > 0 Then 'Sohail (11 May 2011)

    '                                                strQ = " Update lnloan_advance_tran set " & _
    '                                                      " loan_statusunkid = 1 " & _
    '                                                      " WHERE loanadvancetranunkid = @loanadvancetranunkid "
    '                                                objDataOperation.ClearParameters()
    '                                                objDataOperation.AddParameter("@loanadvancetranunkid", SqlDbType.Money, eZeeDataType.MONEY_SIZE, drRow("loanadvancetranunkid").ToString())
    '                                                objDataOperation.ExecNonQuery(strQ)

    '                                                Dim dsStatus As DataSet = objLoanStatusTran.GetList("LoanStatus", True)
    '                                                Dim dtStatus As DataTable = New DataView(dsStatus.Tables("LoanStatus"), "loanadvancetranunkid= " & CInt(drRow("loanadvancetranunkid")) & " AND statusunkid = 4 AND isvoid = 0", "", DataViewRowState.CurrentRows).ToTable
    '                                                If dtStatus IsNot Nothing Then
    '                                                    If dtStatus.Rows.Count > 0 Then
    '                                                        objLoanStatusTran._Loanstatustranunkid = CInt(dtStatus.Rows(0)("loanstatustranunkid"))
    '                                                        objLoanStatusTran._Isvoid = True
    '                                                        objLoanStatusTran._Voiddatetime = Now
    '                                                        objLoanStatusTran._Voiduserunkid = 0
    '                                                        blnFlag = objLoanStatusTran.Update()
    '                                                        If blnFlag = False Then objDataOperation.ReleaseTransaction(False)
    '                                                    End If
    '                                                End If
    '                                            End If

    '                                        End If

    '                                    End If


    '                                    If objDataOperation.ErrorMessage <> "" Then
    '                                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
    '                                        Throw exForce
    '                                    End If

    '                                ElseIf CDec(drRow("savingtranunkid")) > 0 Then

    '                                    ' FOR UPDATE BALANCE IN SAVING TRAN TABLE 

    '                                    strQ = " Update svsaving_tran set " & _
    '                                          " balance_amount = balance_amount - @amount, " & _
    '                                          " total_contribution = total_contribution - @amount " & _
    '                                          " WHERE savingtranunkid = @savingtranunkid "

    '                                    objDataOperation.ClearParameters()
    '                                    objDataOperation.AddParameter("@amount", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, drRow("amount").ToString()) 'Sohail (11 May 2011)
    '                                    objDataOperation.AddParameter("@savingtranunkid", SqlDbType.Money, eZeeDataType.MONEY_SIZE, drRow("savingtranunkid").ToString())
    '                                    objDataOperation.ExecNonQuery(strQ)


    '                                    If objDataOperation.ErrorMessage <> "" Then
    '                                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
    '                                        Throw exForce
    '                                    End If

    '                                End If

    '                            Next

    '                            'Sohail (21 Mar 2014) -- Start
    '                            'Enhancement - Set Limit for C/F Netpay balance and Payment JV Report
    '                            If Math.Abs(mdecRoundingAdjustment) > 0 Then
    '                                Dim objTranHead As New clsTransactionHead
    '                                Dim mintRoundingAdjustmentHeadID As Integer = objTranHead.GetRoundingAdjustmentHeadID()

    '                                strQ = "UPDATE  prpayrollprocess_tran " & _
    '                                        "SET     isvoid = 1 " & _
    '                                              ", voiduserunkid = 1 " & _
    '                                              ", voiddatetime = GETDATE() " & _
    '                                              ", voidreason = 'Payment Voided' " & _
    '                                        "WHERE   isvoid = 0 " & _
    '                                                "AND tnaleavetranunkid = @tnaleavetranunkid " & _
    '                                                "AND tranheadunkid = @tranheadunkid " & _
    '                                                "AND amount = @roundingadjustment "

    '                                objDataOperation.ClearParameters()
    '                                objDataOperation.AddParameter("@tranheadunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintRoundingAdjustmentHeadID)
    '                                objDataOperation.AddParameter("@tnaleavetranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPayreftranunkid)
    '                                objDataOperation.AddParameter("@roundingadjustment", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, Math.Abs(mdecRoundingAdjustment))
    '                                objDataOperation.ExecNonQuery(strQ)

    '                                If objDataOperation.ErrorMessage <> "" Then
    '                                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
    '                                    Throw exForce
    '                                End If

    '                                objTranHead = Nothing
    '                            End If
    '                            'Sohail (21 Mar 2014) -- End

    '                            'FOR UPDATE BALANCE  IN TNA LEAVE TRAN TABLE
    '                            'Sohail (21 Mar 2014) -- Start
    '                            'Enhancement - Set Limit for C/F Netpay balance and Payment JV Report
    '                            'strQ = " Update prtnaleave_tran set " & _
    '                            '       " balanceamount = balanceamount + @amount " & _
    '                            '       " WHERE tnaleavetranunkid = @tnaleavetranunkid "
    '                            strQ = " Update prtnaleave_tran set " & _
    '                                   " balanceamount = balanceamount + @amount + @roundingadjustment " & _
    '                                   " WHERE tnaleavetranunkid = @tnaleavetranunkid "
    '                            'Sohail (21 Mar 2014) -- End

    '                            objDataOperation.ClearParameters()
    '                            objDataOperation.AddParameter("@amount", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecAmount) 'Sohail (11 May 2011)
    '                            objDataOperation.AddParameter("@tnaleavetranunkid", SqlDbType.Money, eZeeDataType.MONEY_SIZE, mintPayreftranunkid)
    '                            objDataOperation.AddParameter("@roundingadjustment", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecRoundingAdjustment) 'Sohail (21 Mar 2014)
    '                            objDataOperation.ExecNonQuery(strQ)


    '                            If objDataOperation.ErrorMessage <> "" Then
    '                                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
    '                                Throw exForce
    '                            End If

    '                        End If

    '                    End If
    '                    objpayrollProcess = Nothing
    '                End If

    '            Case enPaymentRefId.SAVINGS

    '                strQ = " Update svsaving_tran set " & _
    '                            " balance_amount = balance_amount + @amount " & _
    '                      " WHERE savingtranunkid = @savingtranunkid "

    '                objDataOperation.ClearParameters()
    '                objDataOperation.AddParameter("@amount", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecAmount) 'Sohail (11 May 2011)
    '                objDataOperation.AddParameter("@savingtranunkid", SqlDbType.Money, eZeeDataType.MONEY_SIZE, mintPayreftranunkid)
    '                objDataOperation.ExecNonQuery(strQ)


    '                If objDataOperation.ErrorMessage <> "" Then
    '                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
    '                    Throw exForce
    '                End If

    '        End Select


    '        Return True
    '    Catch ex As Exception
    '        objDataOperation.ReleaseTransaction(False)
    '        Throw New Exception(ex.Message & "; Procedure Name: Delete; Module Name: " & mstrModuleName)
    '        Return False
    '    Finally
    '        exForce = Nothing
    '        If dsList IsNot Nothing Then dsList.Dispose()
    '        objDataOperation = Nothing
    '    End Try
    'End Function
    Private Function Delete(ByVal xDatabaseName As String _
                           , ByVal xUserUnkid As Integer _
                           , ByVal xYearUnkid As Integer _
                           , ByVal xCompanyUnkid As Integer _
                           , ByVal xPeriodStart As DateTime _
                           , ByVal xPeriodEnd As DateTime _
                           , ByVal xUserModeSetting As String _
                           , ByVal xOnlyApproved As Boolean _
                           , ByVal xIncludeIn_ActiveEmployee As Boolean _
                           , ByVal objDataOperation As clsDataOperation _
                           , ByVal intUnkid As Integer _
                           , ByVal dtCurrentDateAndTime As Date _
                           , Optional ByVal blnApplyUserAccessFilter As Boolean = True _
                           , Optional ByVal strFilerString As String = "" _
                           , Optional ByVal mdtActivityAdjustment As DataTable = Nothing _
                           ) As Boolean
        'Sohail (23 May 2017) - [mdtActivityAdjustment]
        'Sohail (21 Aug 2015) - [dtCurrentDateAndTime]

        '***************************
        '   IF YOU CHANGE ANYTHING IN THIS METHOD PLEASE GIVE SAME EFFECT IN OTHER OVERLOADED DELETE FUNCTION AS WELL.
        '***************************

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception


        Try
            objDataOperation.ClearParameters()

            'If mstrClientIP.Trim = "" Then mstrClientIP = getIP()
            'If mstrHostName.Trim = "" Then mstrHostName = getHostName()

            objDataOperation.AddParameter("@form_name", SqlDbType.NVarChar, 500, mstrFormName)
            objDataOperation.AddParameter("@isweb", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsWeb)

            strQ = "INSERT INTO atprempsalary_tran " & _
                            "( empsalarytranunkid, paymenttranunkid, paymentdate, employeeunkid, empbanktranid, amount, audittype, audituserunkid, auditdatetime, ip, machine_name, basecurrencyid, baseexchangerate, paidcurrencyid, expaidrate, expaidamt, form_name, isweb ) " & _
                    "SELECT " & _
                         "empsalarytranunkid, paymenttranunkid, paymentdate, employeeunkid, empbanktranid, amount, 3, @voiduserunkid, @voiddatetime, '" & mstrClientIP & "', '" & mstrHostName & "', basecurrencyid, baseexchangerate, paidcurrencyid, expaidrate, expaidamt, @form_name, @isweb " & _
                    "FROM prempsalary_tran " & _
                         "WHERE ISNULL(isvoid, 0) = 0 " & _
                         "AND paymenttranunkid = @paymenttranunkid "

            objDataOperation.AddParameter("@paymenttranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
            If mdtVoiddatetime = Nothing Then
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            Else
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime)
            End If

            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If


            strQ = "UPDATE prempsalary_tran SET " & _
                      "  isvoid = @isvoid" & _
                      ", voiduserunkid = @voiduserunkid" & _
                      ", voiddatetime = @voiddatetime " & _
                      ", voidreason = @voidreason " & _
            "WHERE paymenttranunkid = @paymenttranunkid "



            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            strQ = "UPDATE prpayment_tran SET " & _
                      "  isvoid = @isvoid" & _
                      ", voiduserunkid = @voiduserunkid" & _
                      ", voiddatetime = @voiddatetime " & _
                      ", voidreason = @voidreason " & _
            "WHERE paymenttranunkid = @paymenttranunkid "

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@paymenttranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
            If mdtVoiddatetime = Nothing Then
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            Else
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime)
            End If

            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If


            If mstrWebFormName.Trim.Length <= 0 Then
                objDataOperation.AddParameter("@form_name", SqlDbType.NVarChar, 500, mstrFormName)
                objDataOperation.AddParameter("@isweb", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsWeb)

                objDataOperation.AddParameter("@loginemployeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, -1) 'mintLogEmployeeUnkid 'Sohail (13 Feb 2013)
            Else
                objDataOperation.AddParameter("@form_name", SqlDbType.NVarChar, 500, mstrWebFormName)
                objDataOperation.AddParameter("@isweb", SqlDbType.Bit, eZeeDataType.BIT_SIZE, True)
                objDataOperation.AddParameter("@module_name1", SqlDbType.NVarChar, 500, Language.getMessage(mstrModuleName, 2, "WEB"))
                objDataOperation.AddParameter("@loginemployeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLogEmployeeUnkid) 'Sohail (13 Feb 2013)
            End If





            strQ = "INSERT INTO atprpayment_authorize_tran " & _
                             "( paymentauthorizetranunkid, paymenttranunkid, authorize_date, isauthorized, audittype, audituserunkid, auditdatetime, ip, machine_name, form_name, isweb, remarks ) " & _
                     "SELECT " & _
                          "paymentauthorizetranunkid, paymenttranunkid, authorize_date, isauthorized, 3, @voiduserunkid, @voiddatetime, '" & mstrClientIP & "', '" & mstrHostName & "', @form_name, @isweb, remarks " & _
                     "FROM prpayment_authorize_tran " & _
                     "WHERE isactive = 1 " & _
                          "AND paymenttranunkid = @paymenttranunkid "

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            'TRA - ENHANCEMENT - *** Delete entries from prpayment_authorize_tran Table
            strQ = "UPDATE  prpayment_authorize_tran " & _
                    "SET     isactive = 0 " & _
                          ", voiddatetime = @voiddatetime " & _
                          ", voiduserunkid = @voiduserunkid " & _
                          ", voidreason = @voidreason " & _
                    "WHERE   paymenttranunkid = @paymenttranunkid "

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            strQ = "INSERT INTO atprpayment_approval_tran " & _
                            "( paymentapprovaltranunkid, paymenttranunkid, levelunkid, priority, approval_date, statusunkid, disapprovalreason, audittype, audituserunkid, auditdatetime, ip, machine_name, form_name, isweb, loginemployeeunkid, remarks ) " & _
                    "SELECT " & _
                         "paymentapprovaltranunkid, paymenttranunkid, levelunkid, priority, approval_date, statusunkid, disapprovalreason, 3, @voiduserunkid, @voiddatetime, '" & mstrClientIP & "', '" & mstrHostName & "', @form_name, @isweb, @loginemployeeunkid, remarks  " & _
                    "FROM     prpayment_approval_tran " & _
                    "WHERE ISNULL(isvoid, 0) = 0 " & _
                    "AND paymenttranunkid = @paymenttranunkid "

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            strQ = "UPDATE  prpayment_approval_tran " & _
                    "SET     isvoid = 1 " & _
                          ", voiddatetime = @voiddatetime " & _
                          ", voiduserunkid = @voiduserunkid " & _
                          ", voidreason = @voidreason " & _
                    "WHERE   paymenttranunkid = @paymenttranunkid " & _
                    "AND ISNULL(isvoid, 0) = 0 "

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If
            With objCashDenome
                ._FormName = mstrFormName
                ._LoginEmployeeunkid = mintLogEmployeeUnkid
                ._ClientIP = mstrClientIP
                ._HostName = mstrHostName
                ._FromWeb = mblnIsWeb
                ._AuditUserId = mintAuditUserId
                ._CompanyUnkid = mintCompanyUnkid
                ._AuditDate = mdtAuditDate
            End With
            If objCashDenome.VoidCashDenomination(intUnkid, mblnIsvoid, mintVoiduserunkid, mdtVoiddatetime, mstrVoidreason, objDataOperation) = False Then
                objDataOperation.ReleaseTransaction(False)
                Return False
            End If

            'Sohail (07 May 2015) -- Start
            'Enhancement - New Loan Redesign with allow to change interest rate, tenure, topup, receipt.
            Dim intVoidUserId As Integer = mintVoiduserunkid
            Dim strVoidReason As String = mstrVoidreason
            'Sohail (07 May 2015) -- End

            _DataOperation = objDataOperation
            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'Me._Paymenttranunkid = intUnkid
            Call GetData(objDataOperation, intUnkid)
            'Sohail (21 Aug 2015) -- End

            'Sohail (07 May 2015) -- Start
            'Enhancement - New Loan Redesign with allow to change interest rate, tenure, topup, receipt.
            Dim intLoanBalanceTranUnkId As Integer = -1
            If mintPaymentrefid = enPaymentRefId.LOAN AndAlso mintPaymentTypeId = enPayTypeId.RECEIVED Then
                Dim objLoan As New clsLoan_Advance
                Dim ds As DataSet = objLoan.GetLastLoanBalance("List", mintPayreftranunkid)
                If Not (ds.Tables("List").Rows.Count > 0 AndAlso CInt(ds.Tables("List").Rows(0).Item("paymenttranunkid")) = intUnkid) Then
                    Dim strMsg As String = " Loan"
                    If ds.Tables("List").Rows.Count > 0 Then
                        If CInt(ds.Tables("List").Rows(0).Item("payrollprocesstranunkid")) > 0 Then
                            strMsg = " Process Payroll"
                        ElseIf CInt(ds.Tables("List").Rows(0).Item("paymenttranunkid")) > 0 Then
                            strMsg = " Payment List"
                        ElseIf CInt(ds.Tables("List").Rows(0).Item("lninteresttranunkid")) > 0 Then
                            strMsg = " Loan Interest Rate"
                        ElseIf CInt(ds.Tables("List").Rows(0).Item("lnemitranunkid")) > 0 Then
                            strMsg = " Loan EMI"
                        ElseIf CInt(ds.Tables("List").Rows(0).Item("lntopuptranunkid")) > 0 Then
                            strMsg = " Loan Topup"
                        End If
                    End If
                    mstrMessage = Language.getMessage(mstrModuleName, 32, "Sorry, You cannot delete this transaction. Please delete last transaction from the screen of") & strMsg
                    objDataOperation.ReleaseTransaction(False)
                    Return False
                Else
                    intLoanBalanceTranUnkId = CInt(ds.Tables("List").Rows(0).Item("loanbalancetranunkid"))
                End If
            End If
            'Sohail (07 May 2015) -- End

            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'If InsertAuditTrailForPaymentTran(objDataOperation, 3) = False Then
            If InsertAuditTrailForPaymentTran(objDataOperation, 3, dtCurrentDateAndTime) = False Then
                'Sohail (21 Aug 2015) -- End
                objDataOperation.ReleaseTransaction(False)
                Return False
            End If

            Dim blnFlag As Boolean = False
            Select Case mintPaymentrefid
                Case enPaymentRefId.LOAN, enPaymentRefId.ADVANCE
                    Select Case mintPaymentTypeId
                        Case enPayTypeId.PAYMENT
                            objLoan_Advance._Loanadvancetranunkid = mintPayreftranunkid
                            '<TODO Please Put One Check Point of Payment......!! >

                            Dim dsDataList As New DataSet
                            'Sohail (21 Aug 2015) -- Start
                            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                            'dsDataList = GetList("Test", mintPaymentrefid, mintPayreftranunkid, mintPaymentTypeId)
                            dsDataList = GetList(xDatabaseName, xUserUnkid, xYearUnkid, xCompanyUnkid, xPeriodStart, xPeriodEnd, xUserModeSetting, xOnlyApproved, xIncludeIn_ActiveEmployee, "Test", mintPaymentrefid, mintPayreftranunkid, mintPaymentTypeId, True, , objDataOperation)
                            'Sohail (21 Aug 2015) -- End
                            If dsDataList.Tables(0).Rows.Count > 0 Then
                            Else
                                objLoan_Advance._Balance_Amount = 0
                                'objLoan_Advance._ClientIP = mstrClientIP
                                'objLoan_Advance._WebFormName = mstrWebFormName
                                'objLoan_Advance._HostName = mstrHostName

                                objLoan_Advance._ClientIP = mstrClientIP
                                objLoan_Advance._FormName = mstrFormName
                                objLoan_Advance._HostName = mstrHostName
                                objLoan_Advance._AuditUserId = mintAuditUserId
                                objLoan_Advance._CompanyUnkid = mintCompanyUnkid
                                objLoan_Advance._AuditDate = mdtAuditDate
                                objLoan_Advance._FromWeb = mblnIsWeb
                                objLoan_Advance._LoginEmployeeUnkid = mintLogEmployeeUnkid

                                'Nilay (05-May-2016) -- Start
                                'blnFlag = objLoan_Advance.Update
                                blnFlag = objLoan_Advance.Update(objDataOperation)
                                'Nilay (05-May-2016) -- End

                                If blnFlag = False Then objDataOperation.ReleaseTransaction(False)
                            End If
                        Case enPayTypeId.RECEIVED
                            'Sohail (07 May 2015) -- Start
                            'Enhancement - New Loan Redesign with allow to change interest rate, tenure, topup, receipt.
                            If mintPaymentrefid = enPaymentRefId.LOAN Then
                                strQ = "UPDATE lnloan_balance_tran SET isvoid = 1, voiduserunkid = " & intVoidUserId & ", voiddatetime = GETDATE(), voidreason = '" & strVoidReason & "' WHERE lnloan_balance_tran.loanbalancetranunkid= " & intLoanBalanceTranUnkId & " "
                                objDataOperation.ExecNonQuery(strQ)
                                If objDataOperation.ErrorMessage <> "" Then
                                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                    Throw exForce
                                End If
                            End If
                            'Sohail (07 May 2015) -- End

                            'Sohail (07 May 2015) -- Start
                            'Enhancement - New Loan Redesign with allow to change interest rate, tenure, topup, receipt.
                            Dim dsLoan As DataSet = Nothing
                            'Nilay (25-Mar-2016) -- Start
                            'dsLoan = objLoan_Advance.Calculate_LoanBalanceInfo("List", mdtPaymentdate, "", mintPayreftranunkid, , False, , , True)
                            dsLoan = objLoan_Advance.Calculate_LoanBalanceInfo(xDatabaseName, xUserUnkid, xYearUnkid, xCompanyUnkid, _
                                                                               xPeriodStart, xPeriodEnd, xUserModeSetting, xOnlyApproved, _
                                                                               "List", mdtPaymentdate, "", mintPayreftranunkid, , False, , , True)
                            'Nilay (25-Mar-2016) -- End

                            'Sohail (07 May 2015) -- End

                            objLoan_Advance._Loanadvancetranunkid = mintPayreftranunkid
                            objLoan_Advance._Balance_Amount = objLoan_Advance._Balance_Amount + mdecAmount
                            'Sohail (07 May 2015) -- Start
                            'Enhancement - New Loan Redesign with allow to change interest rate, tenure, topup, receipt.
                            objLoan_Advance._Balance_AmountPaidCurrency = objLoan_Advance._Balance_AmountPaidCurrency + mdecExpaidamt
                            If dsLoan.Tables(0).Rows.Count > 0 Then
                                objLoan_Advance._Balance_Amount = objLoan_Advance._Balance_Amount + CDec(dsLoan.Tables(0).Rows(0).Item("TotPrincipalAmount"))
                                objLoan_Advance._Balance_AmountPaidCurrency = objLoan_Advance._Balance_AmountPaidCurrency + CDec(dsLoan.Tables(0).Rows(0).Item("TotPrincipalAmountPaidCurrency"))
                            End If
                            'Sohail (07 May 2015) -- End

                            'objLoan_Advance._ClientIP = mstrClientIP
                            'objLoan_Advance._WebFormName = mstrWebFormName
                            'objLoan_Advance._HostName = mstrHostName

                            objLoan_Advance._ClientIP = mstrClientIP
                            objLoan_Advance._FormName = mstrFormName
                            objLoan_Advance._HostName = mstrHostName
                            objLoan_Advance._AuditUserId = mintAuditUserId
                            objLoan_Advance._CompanyUnkid = mintCompanyUnkid
                            objLoan_Advance._AuditDate = mdtAuditDate
                            objLoan_Advance._FromWeb = mblnIsWeb
                            objLoan_Advance._LoginEmployeeUnkid = mintLogEmployeeUnkid

                            'blnFlag = objLoan_Advance.Update
                            blnFlag = objLoan_Advance.Update(objDataOperation)
                            'Nilay (05-May-2016) -- End

                            If blnFlag = False Then objDataOperation.ReleaseTransaction(False)
                    End Select

                Case enPaymentRefId.PAYSLIP

                    If mintPaymentTypeId = enPayTypeId.PAYMENT Then

                        'Sohail (07 May 2015) -- Start
                        'Enhancement - New Loan Redesign with allow to change interest rate, tenure, topup, receipt.
                        Dim objPeriod As New clscommom_period_Tran
                        'Nilay (10-Oct-2015) -- Start
                        'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                        'objPeriod._Periodunkid = mintPeriodunkid
                        objPeriod._Periodunkid(xDatabaseName) = mintPeriodunkid
                        'Nilay (10-Oct-2015) -- End
                        Dim dsLoan As DataSet = Nothing
                        'Nilay (25-Mar-2016) -- Start
                        'dsLoan = objLoan_Advance.Calculate_LoanBalanceInfo("List", objPeriod._End_Date.AddDays(1), "", 0, objPeriod._Start_Date, True, , , True, , , , True)
                        'Sohail (02 Jan 2017) -- Start
                        'TRA Enhancement - 64.1 - Performance enhancement in process payroll, and global void payment.
                        'dsLoan = objLoan_Advance.Calculate_LoanBalanceInfo(xDatabaseName, xUserUnkid, xYearUnkid, xCompanyUnkid, _
                        '                                                   xPeriodStart, xPeriodEnd, xUserModeSetting, xOnlyApproved, _
                        '                                                   "List", objPeriod._End_Date.AddDays(1), "", 0, objPeriod._Start_Date, True, , , True, , , , True)
                        dsLoan = objLoan_Advance.Calculate_LoanBalanceInfo(xDatabaseName, xUserUnkid, xYearUnkid, xCompanyUnkid, _
                                                                           xPeriodStart, xPeriodEnd, xUserModeSetting, xOnlyApproved, _
                                                                           "List", objPeriod._End_Date.AddDays(1), mintEmployeeunkid.ToString, 0, objPeriod._Start_Date, True, , , True, , , , True)
                        'Sohail (02 Jan 2017) -- End
                        'Nilay (25-Mar-2016) -- End

                        'Sohail (07 May 2015) -- End

                        Dim objpayrollProcess As New clsPayrollProcessTran
                        'Sohail (21 Aug 2015) -- Start
                        'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                        'Dim dsProcess As DataSet = objpayrollProcess.GetList("List", mintEmployeeunkid, mintPeriodunkid, mintPayreftranunkid, , mstrAccessLevelFilterString)
                        'Sohail (02 Jan 2017) -- Start
                        'TRA Enhancement - 64.1 - Performance enhancement in process payroll, and global void payment.
                        'Dim dsProcess As DataSet = objpayrollProcess.GetList(xDatabaseName, xUserUnkid, xYearUnkid, xCompanyUnkid, xPeriodStart, xPeriodEnd, xUserModeSetting, xOnlyApproved, xIncludeIn_ActiveEmployee, "List", mintEmployeeunkid, mintPeriodunkid, mintPayreftranunkid, blnApplyUserAccessFilter, strFilerString)
                        Dim dsProcess As DataSet = objpayrollProcess.GetList(xDatabaseName, xUserUnkid, xYearUnkid, xCompanyUnkid, xPeriodStart, xPeriodEnd, xUserModeSetting, xOnlyApproved, xIncludeIn_ActiveEmployee, "List", mintEmployeeunkid, mintPeriodunkid, mintPayreftranunkid, blnApplyUserAccessFilter, " (prpayrollprocess_tran.loanadvancetranunkid > 0 OR prpayrollprocess_tran.savingtranunkid > 0) ")
                        'Sohail (02 Jan 2017) -- End
                        'Sohail (21 Aug 2015) -- End

                        If dsProcess IsNot Nothing Then
                            If dsProcess.Tables(0).Rows.Count > 0 Then
                                For Each drRow As DataRow In dsProcess.Tables(0).Rows
                                    If CDec(drRow("loanadvancetranunkid")) > 0 Then

                                        ' FOR UPDATE BALANCEs IN LOAN ADVANCE TRAN TABLE 
                                        'Sohail (07 May 2015) -- Start
                                        'Enhancement - New Loan Redesign with allow to change interest rate, tenure, topup, receipt.
                                        'strQ = " Update lnloan_advance_tran set " & _
                                        '       "  balance_amount = balance_amount + @amount " & _
                                        '       " WHERE loanadvancetranunkid = @loanadvancetranunkid "
                                        'objDataOperation.ClearParameters()
                                        'objDataOperation.AddParameter("@amount", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, drRow("amount").ToString()) 'Sohail (11 May 2011)
                                        'objDataOperation.AddParameter("@loanadvancetranunkid", SqlDbType.Money, eZeeDataType.MONEY_SIZE, drRow("loanadvancetranunkid").ToString())
                                        'objDataOperation.AddParameter("@amountPaidCurrency", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, drRow("amountPaidCurrency").ToString()) 'Sohail (07 May 2015)
                                        'objDataOperation.ExecNonQuery(strQ)
                                        Dim dr_Row() As DataRow = dsLoan.Tables("List").Select("loanadvancetranunkid = " & CInt(drRow("loanadvancetranunkid")) & " ")
                                        If dr_Row.Length > 0 Then
                                            'Sohail (30 Jan 2020) -- Start
                                            'Tujijenge Tanzania Issue # 0004470 : Incomplete Loans Showing As Complete
                                            'strQ = " Update lnloan_advance_tran set " & _
                                            '       " balance_amount = balance_amount + @amount " & _
                                            '           ", balance_amountPaidCurrency = balance_amountPaidCurrency + @amountPaidCurrency " & _
                                            '       " WHERE loanadvancetranunkid = @loanadvancetranunkid "
                                            strQ = " Update lnloan_advance_tran set " & _
                                                   " balance_amount =  @amount " & _
                                                       ", balance_amountPaidCurrency = @amountPaidCurrency " & _
                                                   " WHERE loanadvancetranunkid = @loanadvancetranunkid "
                                            'Sohail (30 Jan 2020) -- End

                                            objDataOperation.ClearParameters()
                                            'Sohail (30 Jan 2020) -- Start
                                            'Tujijenge Tanzania Issue # 0004470 : Incomplete Loans Showing As Complete
                                            'objDataOperation.AddParameter("@amount", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, CDec(dr_Row(0).Item("TotPrincipalAmount")))
                                            'objDataOperation.AddParameter("@amountPaidCurrency", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, CDec(dr_Row(0).Item("TotPrincipalAmountPaidCurrency")))
                                            'objDataOperation.AddParameter("@loanadvancetranunkid", SqlDbType.Money, eZeeDataType.MONEY_SIZE, drRow("loanadvancetranunkid").ToString())
                                            'Sohail (16 Apr 2020) -- Start
                                            'EKO SUPREME NIGERIA issue # 0004654 : Advance amount disappears when user voids payment.
                                            'objDataOperation.AddParameter("@amount", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, CDec(dr_Row(0).Item("BalanceAmount")) + CDec(drRow.Item("principal_amount")))
                                            'objDataOperation.AddParameter("@amountPaidCurrency", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, CDec(dr_Row(0).Item("BalanceAmountPaidCurrency")) + CDec(drRow.Item("principal_amountPaidCurrency")))
                                            If CBool(dr_Row(0).Item("isloan")) = True Then
                                                'Hemant (24 Jun 2020) -- Start
                                                'ISSUE(VOLTAMP) :  Advance not taking in Payroll Process
                                                'objDataOperation.AddParameter("@amount", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, CDec(dr_Row(0).Item("BalanceAmount")) + CDec(drRow.Item("principal_amount")))
                                                'objDataOperation.AddParameter("@amountPaidCurrency", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, CDec(dr_Row(0).Item("BalanceAmountPaidCurrency")) + CDec(drRow.Item("principal_amountPaidCurrency")))
                                                objDataOperation.AddParameter("@amount", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, CDec(dr_Row(0).Item("Balance_Amount")) + CDec(drRow.Item("principal_amount")))
                                                objDataOperation.AddParameter("@amountPaidCurrency", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, CDec(dr_Row(0).Item("Balance_AmountPaidCurrency")) + CDec(drRow.Item("principal_amountPaidCurrency")))
                                                'Hemant (24 Jun 2020) -- End                                                
                                            Else 'Advance
                                                objDataOperation.AddParameter("@amount", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, CDec(dr_Row(0).Item("BalanceAmount")) + CDec(drRow.Item("amount")))
                                                objDataOperation.AddParameter("@amountPaidCurrency", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, CDec(dr_Row(0).Item("BalanceAmountPaidCurrency")) + CDec(drRow.Item("amountPaidCurrency")))
                                            End If
                                            'Sohail (16 Apr 2020) -- End
                                            objDataOperation.AddParameter("@loanadvancetranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, drRow("loanadvancetranunkid").ToString())
                                            'Sohail (30 Jan 2020) -- End

                                            objDataOperation.ExecNonQuery(strQ)

                                            If objDataOperation.ErrorMessage <> "" Then
                                                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                                Throw exForce
                                            End If
                                        End If
                                        'Sohail (07 May 2015) -- End

                                        strQ = "Select balance_amount, balance_amountPaidCurrency from lnloan_advance_tran  " & _
                                         " WHERE loanadvancetranunkid = @loanadvancetranunkid "
                                        'Sohail (07 May 2015) - [balance_amountPaidCurrency]

                                        objDataOperation.ClearParameters()
                                        'Sohail (30 Jan 2020) -- Start
                                        'Tujijenge Tanzania Issue # 0004470 : Incomplete Loans Showing As Complete
                                        'objDataOperation.AddParameter("@loanadvancetranunkid", SqlDbType.Money, eZeeDataType.MONEY_SIZE, drRow("loanadvancetranunkid").ToString())
                                        objDataOperation.AddParameter("@loanadvancetranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, drRow("loanadvancetranunkid").ToString())
                                        'Sohail (30 Jan 2020) -- End
                                        Dim dsAmount As DataSet = objDataOperation.ExecQuery(strQ, "Balance")

                                        If objDataOperation.ErrorMessage <> "" Then
                                            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                            Throw exForce
                                        End If

                                        If dsAmount IsNot Nothing Then

                                            If dsAmount.Tables("Balance").Rows.Count > 0 Then

                                                'Sohail (07 May 2015) -- Start
                                                'Enhancement - New Loan Redesign with allow to change interest rate, tenure, topup, receipt.
                                                'If CDec(dsAmount.Tables("Balance").Rows(0)("balance_amount")) > 0 Then 'Sohail (11 May 2011)
                                                If CDec(dsAmount.Tables("Balance").Rows(0)("balance_amountPaidCurrency")) > 0 Then
                                                    'Sohail (07 May 2015) -- End

                                                    strQ = " Update lnloan_advance_tran set " & _
                                                          " loan_statusunkid = 1 " & _
                                                          " WHERE loanadvancetranunkid = @loanadvancetranunkid "
                                                    objDataOperation.ClearParameters()
                                                    objDataOperation.AddParameter("@loanadvancetranunkid", SqlDbType.Money, eZeeDataType.MONEY_SIZE, drRow("loanadvancetranunkid").ToString())
                                                    objDataOperation.ExecNonQuery(strQ)

                                                    If objDataOperation.ErrorMessage <> "" Then
                                                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                                        Throw exForce
                                                    End If

                                                    'Sohail (04 Jun 2018) -- Start
                                                    'CCK Issue - Support Issue Id # 0002297 - Mismatch of amount deducted from loan between payroll report and loan report. Tried running the Written off loan tool but experienced an error on it. (Completed loans were coming in process payroll) in 72.1.
                                                    'Dim dsStatus As DataSet = objLoanStatusTran.GetList("LoanStatus", True)
                                                    'Dim dtStatus As DataTable = New DataView(dsStatus.Tables("LoanStatus"), "loanadvancetranunkid= " & CInt(drRow("loanadvancetranunkid")) & " AND statusunkid = 4 AND isvoid = 0", "", DataViewRowState.CurrentRows).ToTable
                                                    Dim dsStatus As DataSet = objLoanStatusTran.GetList("LoanStatus", True, mintPeriodunkid, mintEmployeeunkid)
                                                    Dim dtStatus As DataTable = New DataView(dsStatus.Tables("LoanStatus"), "loanadvancetranunkid= " & CInt(drRow("loanadvancetranunkid")) & " AND statusunkid = 4 AND isvoid = 0 AND periodunkid = " & mintPeriodunkid & "", "", DataViewRowState.CurrentRows).ToTable
                                                    'Sohail (04 Jun 2018) -- End
                                                    If dtStatus IsNot Nothing Then
                                                        If dtStatus.Rows.Count > 0 Then
                                                            objLoanStatusTran._Loanstatustranunkid = CInt(dtStatus.Rows(0)("loanstatustranunkid"))
                                                            objLoanStatusTran._Isvoid = True
                                                            'Nilay (20-Sept-2016) -- Start
                                                            'Enhancement : Cancel feature for approved but not assigned loan application
                                                            'objLoanStatusTran._Voiddatetime = Now
                                                            objLoanStatusTran._Voiddatetime = dtCurrentDateAndTime
                                                            'Nilay (20-Sept-2016) -- End
                                                            objLoanStatusTran._Voiduserunkid = xUserUnkid 'Nilay (01-Apr-2016)
                                                            objLoanStatusTran._ClientIP = mstrClientIP
                                                            objLoanStatusTran._FormName = mstrFormName
                                                            objLoanStatusTran._HostName = mstrHostName
                                                            objLoanStatusTran._AuditDate = mdtAuditDate
                                                            objLoanStatusTran._AuditUserId = mintAuditUserId
                                                            objLoanStatusTran._CompanyUnkid = mintCompanyUnkid
                                                            objLoanStatusTran._FromWeb = mblnIsWeb
                                                            objLoanStatusTran._LoginEmployeeunkid = mintLogEmployeeUnkid
                                                            blnFlag = objLoanStatusTran.Update()
                                                            If blnFlag = False Then objDataOperation.ReleaseTransaction(False)

                                                            'Sohail (24 Apr 2019) -- Start
                                                            'NPK Issue - Support Issue Id # 0003771 - 76.1 - Not Able to delete the advance after voiding payroll payment of March (due to transaction was not getting voided from loan_balance_tran on voiding payment).
                                                            strQ = "UPDATE lnloan_balance_tran SET isvoid = 1, voiduserunkid = " & xUserUnkid & ", voiddatetime = GETDATE(), voidreason = '" & strVoidReason & "' WHERE lnloan_balance_tran.loanstatustranunkid= " & CInt(dtStatus.Rows(0)("loanstatustranunkid")) & " "
                                                            objDataOperation.ExecNonQuery(strQ)
                                                            If objDataOperation.ErrorMessage <> "" Then
                                                                objDataOperation.ReleaseTransaction(False)
                                                                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                                                Throw exForce
                                                            End If
                                                            'Sohail (24 Apr 2019) -- End

                                                        End If
                                                    End If
                                                End If

                                            End If

                                        End If


                                        If objDataOperation.ErrorMessage <> "" Then
                                            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                            Throw exForce
                                        End If

                                    ElseIf CDec(drRow("savingtranunkid")) > 0 Then

                                        ' FOR UPDATE BALANCE IN SAVING TRAN TABLE 

                                        strQ = " Update svsaving_tran set " & _
                                              " balance_amount = balance_amount - @amount, " & _
                                              " total_contribution = total_contribution - @amount " & _
                                              " WHERE savingtranunkid = @savingtranunkid "

                                        objDataOperation.ClearParameters()
                                        objDataOperation.AddParameter("@amount", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, drRow("amount").ToString()) 'Sohail (11 May 2011)
                                        objDataOperation.AddParameter("@savingtranunkid", SqlDbType.Money, eZeeDataType.MONEY_SIZE, drRow("savingtranunkid").ToString())
                                        objDataOperation.ExecNonQuery(strQ)


                                        If objDataOperation.ErrorMessage <> "" Then
                                            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                            Throw exForce
                                        End If

                                    End If

                                Next
                            End If 'Sohail (24 Jan 2017) 

                            'Enhancement - Set Limit for C/F Netpay balance and Payment JV Report
                            If Math.Abs(mdecRoundingAdjustment) > 0 Then
                                Dim objTranHead As New clsTransactionHead
                                Dim mintRoundingAdjustmentHeadID As Integer = objTranHead.GetRoundingAdjustmentHeadID()

                                strQ = "UPDATE  prpayrollprocess_tran " & _
                                        "SET     isvoid = 1 " & _
                                              ", voiduserunkid = 1 " & _
                                              ", voiddatetime = GETDATE() " & _
                                              ", voidreason = 'Payment Voided' " & _
                                        "WHERE   isvoid = 0 " & _
                                                "AND tnaleavetranunkid = @tnaleavetranunkid " & _
                                                "AND tranheadunkid = @tranheadunkid " & _
                                                "AND amount = @roundingadjustment "

                                objDataOperation.ClearParameters()
                                objDataOperation.AddParameter("@tranheadunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintRoundingAdjustmentHeadID)
                                objDataOperation.AddParameter("@tnaleavetranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPayreftranunkid)
                                objDataOperation.AddParameter("@roundingadjustment", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, Math.Abs(mdecRoundingAdjustment))
                                objDataOperation.ExecNonQuery(strQ)

                                If objDataOperation.ErrorMessage <> "" Then
                                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                    Throw exForce
                                End If

                                objTranHead = Nothing
                            End If

                            'FOR UPDATE BALANCE  IN TNA LEAVE TRAN TABLE
                            'Enhancement - Set Limit for C/F Netpay balance and Payment JV Report
                            strQ = " Update prtnaleave_tran set " & _
                                   " balanceamount = balanceamount + @amount + @roundingadjustment " & _
                                   " WHERE tnaleavetranunkid = @tnaleavetranunkid "

                            objDataOperation.ClearParameters()
                            objDataOperation.AddParameter("@amount", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecAmount) 'Sohail (11 May 2011)
                            objDataOperation.AddParameter("@tnaleavetranunkid", SqlDbType.Money, eZeeDataType.MONEY_SIZE, mintPayreftranunkid)
                            objDataOperation.AddParameter("@roundingadjustment", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecRoundingAdjustment) 'Sohail (21 Mar 2014)
                            objDataOperation.ExecNonQuery(strQ)


                            If objDataOperation.ErrorMessage <> "" Then
                                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                Throw exForce
                            End If

                        End If

                        'End If 'Sohail (24 Jan 2017) 
                        objpayrollProcess = Nothing
                    End If

                Case enPaymentRefId.SAVINGS

                    'Sohail (21 Nov 2015) -- Start
                    'Enhancement - Provide Deposit feaure in Employee Saving.
                    'strQ = " Update svsaving_tran set " & _
                    '            " balance_amount = balance_amount + @amount " & _
                    '      " WHERE savingtranunkid = @savingtranunkid "
                    Select Case mintPaymentTypeId

                        Case enPayTypeId.DEPOSIT
                            strQ = " Update svsaving_tran set " & _
                                        " balance_amount = balance_amount - @amount " & _
                                  " WHERE savingtranunkid = @savingtranunkid "

                        Case Else
                            strQ = " Update svsaving_tran set " & _
                                        " balance_amount = balance_amount + @amount " & _
                                  " WHERE savingtranunkid = @savingtranunkid "
                    End Select
                    'Sohail (21 Nov 2015) -- End


                    objDataOperation.ClearParameters()
                    objDataOperation.AddParameter("@amount", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecAmount) 'Sohail (11 May 2011)
                    objDataOperation.AddParameter("@savingtranunkid", SqlDbType.Money, eZeeDataType.MONEY_SIZE, mintPayreftranunkid)
                    objDataOperation.ExecNonQuery(strQ)


                    If objDataOperation.ErrorMessage <> "" Then
                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If

            End Select

            'Sohail (23 May 2017) -- Start
            'Enhancement - 67.1 - Link budget with Payroll.
            If mdtActivityAdjustment IsNot Nothing AndAlso mdtActivityAdjustment.Rows.Count > 0 Then
                Dim objActAdj As clsFundActivityAdjustment_Tran
                For Each dtRow As DataRow In mdtActivityAdjustment.Rows
                    objActAdj = New clsFundActivityAdjustment_Tran

                    objActAdj._FundActivityunkid = CInt(dtRow.Item("fundactivityunkid"))
                    objActAdj._TransactionDate = eZeeDate.convertDate(dtRow.Item("transactiondate").ToString())
                    objActAdj._CurrentBalance = CDec(dtRow.Item("Current Balance"))
                    objActAdj._IncrDecrAmount = CDec(dtRow.Item("Actual Salary"))
                    objActAdj._NewBalance = CDec(dtRow.Item("Current Balance")) + CDec(dtRow.Item("Actual Salary"))
                    objActAdj._Remark = dtRow.Item("remark").ToString()
                    objActAdj._Userunkid = xUserUnkid
                    objActAdj._Paymenttranunkid = intUnkid
                    objActAdj._Globalvocunkid = 0
                    With objActAdj
                        ._FormName = mstrFormName
                        ._LoginEmployeeunkid = mintLogEmployeeUnkid
                        ._ClientIP = mstrClientIP
                        ._HostName = mstrHostName
                        ._FromWeb = mblnIsWeb
                        ._AuditUserId = mintAuditUserId
                        ._CompanyUnkid = mintCompanyUnkid
                        ._AuditDate = mdtAuditDate
                    End With
                    If objActAdj.Insert(dtCurrentDateAndTime, objDataOperation, True) = False Then
                        objDataOperation.ReleaseTransaction(False)
                        Throw New Exception(objActAdj._Message)
                        Return False
                    End If
                Next
            End If
            'Sohail (23 May 2017) -- End

            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Delete; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function
    'Sohail (21 Aug 2015) -- End
    'Sohail (12 May 2012) -- End

    ''' <summary>
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isUsed(ByVal intUnkid As Integer) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "<Query>"

            objDataOperation.AddParameter("@paymenttranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isUsed; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isExist(ByVal strName As String, Optional ByVal intUnkid As Integer = -1) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        'objDataOperation = New clsDataOperation
        Dim objDataOperation As New clsDataOperation

        Try
            strQ = "SELECT " & _
                  "  paymenttranunkid " & _
                  ", voucherno " & _
                  ", periodunkid " & _
                  ", paymentdate " & _
                  ", employeeunkid " & _
                  ", referenceid " & _
                  ", paymentmode " & _
                  ", branchunkid " & _
                  ", chequeno " & _
                  ", paymentby " & _
                  ", percentage " & _
                  ", amount " & _
                  ", voucherref " & _
                  ", referencetranunkid " & _
                  ", userunkid " & _
                  ", isvoid " & _
                  ", voiduserunkid " & _
                  ", voiddatetime " & _
                  ", paytypeid " & _
                  ", isreceipt " & _
                  ", voidreason " & _
                  ", isglobalpayment " & _
                  ", basecurrencyid " & _
                  ", baseexchangerate " & _
                  ", paidcurrencyid " & _
                  ", expaidrate " & _
                  ", expaidamt " & _
                 "FROM prpayment_tran " & _
                 "WHERE voucherno = @voucherno " 'Sohail (08 Oct 2011)(basecurrencyid,baseexchangerate,paidcurrencyid,expaidrate,expaidamt added.)

            If intUnkid > 0 Then
                strQ &= " AND paymenttranunkid <> @paymenttranunkid"
            End If

            objDataOperation.AddParameter("@voucherno", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strName)
            objDataOperation.AddParameter("@paymenttranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isExist; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            'objDataOperation = Nothing
        End Try
    End Function

    Public Function IsPaymentDone(ByVal intPayTypeId As Integer, ByVal intPayTranRefId As Integer, ByVal eRefMode As enPaymentRefId, Optional ByVal xDataOp As clsDataOperation = Nothing) As Boolean    'S.SANDEEP [ 29 May 2013 ] -- START -- END 
        'Sohail (30 Oct 2018) - xDataOp
        'Public Function IsPaymentDone(ByVal intPayTypeId As Integer, ByVal intPayTranRefId As Integer) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        'Sohail (30 Oct 2018) -- Start
        'FDRC Enhancement - Global Payment performance issue on FDRC database in 75.1.
        'objDataOperation = New clsDataOperation
        If xDataOp Is Nothing Then
            objDataOperation = New clsDataOperation
        Else
            objDataOperation = xDataOp
        End If
        objDataOperation.ClearParameters()
        'Sohail (30 Oct 2018) -- End

        Try
            strQ = "SELECT " & _
                    "	paymenttranunkid " & _
                    "FROM prpayment_tran " & _
                    "WHERE prpayment_tran.paytypeid = @paytypeid " & _
                    "AND prpayment_tran.referencetranunkid = @referencetranunkid " & _
                    "AND ISNULL(prpayment_tran.isvoid,0) = 0 "

            'S.SANDEEP [ 29 May 2013 ] -- START
            'ENHANCEMENT : ATLAS COPCO WEB CHANGES
            strQ &= " AND referenceid = '" & eRefMode & "' "
            'S.SANDEEP [ 29 May 2013 ] -- END

            objDataOperation.AddParameter("@paytypeid", SqlDbType.Int, eZeeDataType.INT_SIZE, intPayTypeId)
            objDataOperation.AddParameter("@referencetranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intPayTranRefId)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables(0).Rows.Count > 0

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "IsPaymentDone", mstrModuleName)
        End Try
    End Function

    Public Function Get_Payment_Total(ByVal intTransactionId As Integer, _
                                      ByVal intPayRefId As Integer, _
                                      ByVal intPayTypeId As Integer, _
                                      ByRef decAmount As Decimal, _
                                      Optional ByVal blnIsReceipt As Boolean = False, _
                                      Optional ByVal blnEditMode As Boolean = False) As Decimal 'Sohail (11 May 2011)

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation
        Try

            strQ = "SELECT " & _
                            "  prpayment_tran.paymenttranunkid " & _
                            ", ISNULL(prpayment_tran.amount,0) Amount " & _
                       "FROM prpayment_tran " & _
                       "WHERE ISNULL(prpayment_tran.isvoid,0) = 0 " & _
                            "AND referencetranunkid = @referencetranunkid " & _
                            "AND referenceid = @referenceid " & _
                            "AND paytypeid = @paytypeid "



            If blnIsReceipt = True Then
                strQ &= "AND isreceipt = @isreceipt"
            End If

            If blnEditMode = True Then
                strQ &= " AND paymenttranunkid = @paymenttranunkid "
                objDataOperation.AddParameter("@paymenttranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPaymenttranunkid.ToString)
            End If

            objDataOperation.AddParameter("@referencetranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intTransactionId)
            objDataOperation.AddParameter("@referenceid", SqlDbType.Int, eZeeDataType.INT_SIZE, intPayRefId)
            objDataOperation.AddParameter("@paytypeid", SqlDbType.Int, eZeeDataType.INT_SIZE, intPayTypeId)
            objDataOperation.AddParameter("@isreceipt", SqlDbType.Bit, eZeeDataType.BIT_SIZE, blnIsReceipt.ToString)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Dim intTotalRow As Integer = -1
            Dim intCnt As Integer = 0
            intTotalRow = dsList.Tables(0).Rows.Count


            For Each dtRow As DataRow In dsList.Tables(0).Rows
                If blnEditMode = True Then
                    If intTotalRow - 1 <> intCnt Then
                        decAmount += dtRow.Item("Amount")
                    ElseIf intTotalRow = 1 Then
                        decAmount += dtRow.Item("Amount")
                    End If
                Else
                    decAmount += dtRow.Item("Amount")
                End If

                intCnt += 1
            Next

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Get_Payment_Total", mstrModuleName)
        End Try
    End Function

    Public Function Get_Max_PaymentId(ByVal intTransactionId As Integer, _
                                      ByVal intPayRefId As Integer, _
                                      ByVal intPayTypeId As Integer, _
                                      ByRef intMaxId As Integer) As Integer
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation
        Try
            strQ = "SELECT " & _
                     "	MAX(prpayment_tran.paymenttranunkid) AS Id " & _
                   "FROM prpayment_tran " & _
                   "WHERE ISNULL(prpayment_tran.isvoid,0) = 0  " & _
                     "AND referencetranunkid = @referencetranunkid " & _
                     "AND referenceid = @referenceid " & _
                     "AND paytypeid = @paytypeid  "

            objDataOperation.AddParameter("@referencetranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intTransactionId)
            objDataOperation.AddParameter("@referenceid", SqlDbType.Int, eZeeDataType.INT_SIZE, intPayRefId)
            objDataOperation.AddParameter("@paytypeid", SqlDbType.Int, eZeeDataType.INT_SIZE, intPayTypeId)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If IsDBNull(dsList.Tables(0).Rows(0)("Id")) Then
                intMaxId = 0
            Else
                intMaxId = CInt(dsList.Tables(0).Rows(0)("Id"))
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Get_Max_PaymentId", mstrModuleName)
        End Try
    End Function

    'Sohail (21 Aug 2015) -- Start
    'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
    '''' <summary>
    '''' Modify By: Sohail
    '''' </summary>
    '''' <purpose> Assign all Property variable </purpose>
    'Public Function GetListByPeriod(ByVal strTableName As String, _
    '                        ByVal intPayRefId As Integer, _
    '                        ByVal intPayPeriodId As Integer, _
    '                        ByVal intPaymentTypeId As Integer, _
    '                        Optional ByVal strEmployeeIDs As String = "", _
    '                        Optional ByVal strAccessLevelFilterString As String = "", _
    '                        Optional ByVal strFilter As String = "") As DataSet
    '    'Sohail (03 Jul 2014) - [strAccessLevelFilterString, strFilter]
    '    'Sohail (24 Sep 2012) - [intEmployeeId = strEmployeeIDs]
    '    'Sohail (24 Aug 2012) - [intEmployeeId]

    '    Dim dsList As DataSet = Nothing
    '    Dim strQ As String = ""
    '    Dim exForce As Exception

    '    objDataOperation = New clsDataOperation

    '    Try
    '        If strAccessLevelFilterString.Trim = "" Then strAccessLevelFilterString = UserAccessLevel._AccessLevelFilterString 'Sohail (03 Jul 2014)

    '        'Sohail (16 Oct 2010) -- Start
    '        'Changes : New field 'isglobalpayment' added.
    '        'strQ = "SELECT " & _
    '        '          "  paymenttranunkid " & _
    '        '          ", voucherno " & _
    '        '          ", periodunkid " & _
    '        '          ", paymentdate " & _
    '        '          ", employeeunkid " & _
    '        '          ", referenceid " & _
    '        '          ", paymentmode " & _
    '        '          ", branchunkid " & _
    '        '          ", chequeno " & _
    '        '          ", paymentby " & _
    '        '          ", percentage " & _
    '        '          ", amount " & _
    '        '          ", voucherref " & _
    '        '          ", referencetranunkid " & _
    '        '          ", userunkid " & _
    '        '          ", isvoid " & _
    '        '          ", voiduserunkid " & _
    '        '          ", voiddatetime " & _
    '        '          ", paytypeid " & _
    '        '          ", isreceipt " & _
    '        '          ", voidreason " & _
    '        '        "FROM prpayment_tran " & _
    '        '        "WHERE referenceid = @referenceid " & _
    '        '        "AND periodunkid = @periodunkid " & _
    '        '        "AND paytypeid = @paytypeid " & _
    '        '        "AND ISNULL(isvoid,0) = 0 "
    '        'Sohail (12 Oct 2011) -- Start
    '        'Changes : basecurrencyid,baseexchangerate,paidcurrencyid,expaidrate,expaidamt added., join on employee, period, year
    '        'strQ = "SELECT " & _
    '        '          "  paymenttranunkid " & _
    '        '          ", voucherno " & _
    '        '          ", periodunkid " & _
    '        '          ", paymentdate " & _
    '        '          ", employeeunkid " & _
    '        '          ", referenceid " & _
    '        '          ", paymentmode " & _
    '        '          ", branchunkid " & _
    '        '          ", chequeno " & _
    '        '          ", paymentby " & _
    '        '          ", percentage " & _
    '        '          ", amount " & _
    '        '          ", voucherref " & _
    '        '          ", referencetranunkid " & _
    '        '          ", userunkid " & _
    '        '          ", isvoid " & _
    '        '          ", voiduserunkid " & _
    '        '          ", voiddatetime " & _
    '        '          ", paytypeid " & _
    '        '          ", isreceipt " & _
    '        '          ", voidreason " & _
    '        '          ", isglobalpayment " & _
    '        '          ", basecurrencyid " & _
    '        '          ", baseexchangerate " & _
    '        '          ", paidcurrencyid " & _
    '        '          ", expaidrate " & _
    '        '          ", expaidamt " & _
    '        '        "FROM prpayment_tran " & _
    '        '        "WHERE referenceid = @referenceid " & _
    '        '        "AND periodunkid = @periodunkid " & _
    '        '        "AND paytypeid = @paytypeid " & _
    '        '        "AND ISNULL(isvoid,0) = 0 "


    '        'S.SANDEEP [ 20 APRIL 2012 ] -- START
    '        'ENHANCEMENT : TRA CHANGES
    '        'strQ = "SELECT " & _
    '        '           "  CONVERT(CHAR(8),prpayment_tran.paymentdate,112) AS paymentdate " & _
    '        '           ", ISNULL(prpayment_tran.voucherno,'') AS voucherno " & _
    '        '           ", ISNULL(hremployee_master.employeecode,'') AS employeecode " & _
    '        '           ", ISNULL(hremployee_master.firstname,'')+' '+ISNULL(hremployee_master.othername,'')+' '+ISNULL(hremployee_master.surname,'') AS EmpName " & _
    '        '           ", ISNULL(" & FinancialYear._Object._ConfigDatabaseName & "..cffinancial_year_tran.financialyear_name,'') AS YearName " & _
    '        '           ", ISNULL(cfcommon_period_tran.period_name,'') AS PeriodName " & _
    '        '           ", ISNULL(prpayment_tran.amount,0) AS Amount " & _
    '        '           ", hremployee_master.employeeunkid " & _
    '        '           ", " & FinancialYear._Object._ConfigDatabaseName & "..cffinancial_year_tran.yearunkid " & _
    '        '           ", cfcommon_period_tran.periodunkid " & _
    '        '           ", prpayment_tran.paymenttranunkid " & _
    '        '           ", isglobalpayment " & _
    '        '           ", paymenttranunkid " & _
    '        '          ", referenceid " & _
    '        '          ", paymentmode " & _
    '        '          ", branchunkid " & _
    '        '          ", chequeno " & _
    '        '          ", paymentby " & _
    '        '          ", percentage " & _
    '        '          ", voucherref " & _
    '        '          ", referencetranunkid " & _
    '        '          ", isreceipt " & _
    '        '          ", basecurrencyid " & _
    '        '          ", baseexchangerate " & _
    '        '          ", paidcurrencyid " & _
    '        '          ", expaidrate " & _
    '        '          ", expaidamt " & _
    '        '        "FROM prpayment_tran " & _
    '        '           "LEFT JOIN hremployee_master ON prpayment_tran.employeeunkid = hremployee_master.employeeunkid " & _
    '        '           "LEFT JOIN cfcommon_period_tran ON prpayment_tran.periodunkid = cfcommon_period_tran.periodunkid " & _
    '        '           "LEFT JOIN " & FinancialYear._Object._ConfigDatabaseName & "..cffinancial_year_tran ON cfcommon_period_tran.yearunkid = cffinancial_year_tran.yearunkid " & _
    '        '        "WHERE prpayment_tran.referenceid = @referenceid " & _
    '        '        "AND prpayment_tran.periodunkid = @periodunkid " & _
    '        '        "AND prpayment_tran.paytypeid = @paytypeid " & _
    '        '        "AND ISNULL(prpayment_tran.isvoid,0) = 0 " 'Sohail (28 Jan 2012) - [employeecode]
    'strQ = "SELECT " & _
    '           "  CONVERT(CHAR(8),prpayment_tran.paymentdate,112) AS paymentdate " & _
    '               ",CASE WHEN ISNULL(isglobalpayment,0) = 0 THEN ISNULL(prpayment_tran.voucherno,'') " & _
    '                   "            WHEN ISNULL(isglobalpayment,0) = 1 THEN ISNULL(prglobalvoc_master.globalvocno,'') END AS voucherno " & _
    '           ", ISNULL(hremployee_master.employeecode,'') AS employeecode " & _
    '           ", ISNULL(hremployee_master.firstname,'')+' '+ISNULL(hremployee_master.othername,'')+' '+ISNULL(hremployee_master.surname,'') AS EmpName " & _
    '           ", ISNULL(" & FinancialYear._Object._ConfigDatabaseName & "..cffinancial_year_tran.financialyear_name,'') AS YearName " & _
    '           ", ISNULL(cfcommon_period_tran.period_name,'') AS PeriodName " & _
    '           ", ISNULL(prpayment_tran.amount,0) AS Amount " & _
    '           ", hremployee_master.employeeunkid " & _
    '           ", " & FinancialYear._Object._ConfigDatabaseName & "..cffinancial_year_tran.yearunkid " & _
    '           ", cfcommon_period_tran.periodunkid " & _
    '           ", prpayment_tran.paymenttranunkid " & _
    '           ", isglobalpayment " & _
    '           ", paymenttranunkid " & _
    '          ", referenceid " & _
    '          ", paymentmode " & _
    '          ", branchunkid " & _
    '          ", chequeno " & _
    '          ", paymentby " & _
    '          ", percentage " & _
    '          ", voucherref " & _
    '          ", referencetranunkid " & _
    '          ", isreceipt " & _
    '          ", basecurrencyid " & _
    '          ", baseexchangerate " & _
    '          ", paidcurrencyid " & _
    '          ", expaidrate " & _
    '          ", expaidamt " & _
    '                  ", ISNULL(isauthorized, 0) AS isauthorized " & _
    '                  ", ISNULL(accountno, '') AS accountno " & _
    '                  ", ISNULL(prpayment_tran.countryunkid, 0) AS countryunkid " & _
    '                  ", ISNULL(prpayment_tran.isapproved, 0) AS isapproved " & _
    '                  ", ISNULL(prpayment_tran.roundingtypeid, 0) AS roundingtypeid " & _
    '                  ", ISNULL(prpayment_tran.roundingmultipleid, 0) AS roundingmultipleid " & _
    '                  ", ISNULL(prpayment_tran.roundingadjustment, 0) AS roundingadjustment " & _
    '                  ", ISNULL(prpayment_tran.remarks, '') AS remarks " & _
    '        "FROM prpayment_tran " & _
    '                   "LEFT JOIN prglobalvoc_master ON prpayment_tran.globalvocno = prglobalvoc_master.globalvocunkid " & _
    '           "LEFT JOIN hremployee_master ON prpayment_tran.employeeunkid = hremployee_master.employeeunkid " & _
    '           "LEFT JOIN cfcommon_period_tran ON prpayment_tran.periodunkid = cfcommon_period_tran.periodunkid " & _
    '           "LEFT JOIN " & FinancialYear._Object._ConfigDatabaseName & "..cffinancial_year_tran ON cfcommon_period_tran.yearunkid = cffinancial_year_tran.yearunkid " & _
    '        "WHERE prpayment_tran.referenceid = @referenceid " & _
    '        "AND prpayment_tran.periodunkid = @periodunkid " & _
    '        "AND prpayment_tran.paytypeid = @paytypeid " & _
    '                "AND ISNULL(prpayment_tran.isvoid,0) = 0 "
    '        '       'Sohail (24 Dec 2014) - [remarks]
    '        '       'Sohail (21 Mar 2014) - [roundingadjustment]
    '        '       'Sohail (22 Oct 2013) - [roundingtypeid, roundingmultipleid]
    '        '       'Sohail (28 Jan 2012) - [employeecode], 'Sohail (02 Jul 2012) - [isauthorized], 'Sohail (21 Jul 2012) - [accountno]
    '        '       'Sohail (03 Sep 2012) - [countryunkid]
    '        '       'Sohail (13 Feb 2013) - [isapproved]
    '        'S.SANDEEP [ 20 APRIL 2012 ] -- END

    '        'S.SANDEEP [ 11 JAN 2012 ] -- START {globalvocno New Table}-- END

    '        'Sohail (24 Aug 2012) -- Start
    '        'TRA - ENHANCEMENT
    '        'Sohail (24 Sep 2012) -- Start
    '        'TRA - ENHANCEMENT
    '        'If intEmployeeId > 0 Then
    '        '    strQ &= " AND prpayment_tran.employeeunkid = @employeeunkid "
    '        '    objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intEmployeeId.ToString)
    '        'End If
    '        If strEmployeeIDs.Trim <> "" AndAlso strEmployeeIDs.Trim <> "0" AndAlso strEmployeeIDs.Trim <> "-1" Then
    '            strQ &= " AND prpayment_tran.employeeunkid IN (" & strEmployeeIDs & ") "
    '        End If
    '        'Sohail (24 Sep 2012) -- End
    '        'Sohail (24 Aug 2012) -- End

    '        strQ &= strAccessLevelFilterString 'Sohail (03 Jul 2014)

    '        'Sohail (12 Oct 2011) -- End
    '        'Sohail (16 Oct 2010) -- End

    '        'Sohail (03 Jul 2014) -- Start
    '        'Enhancement - Advance Filter and Voucher Combobox on Global Void Payment list.
    '        If strFilter.Trim <> "" Then
    '            strQ &= " AND " & strFilter
    '        End If
    '        'Sohail (03 Jul 2014) -- End

    '        objDataOperation.AddParameter("@referenceid", SqlDbType.Int, eZeeDataType.INT_SIZE, intPayRefId.ToString)
    '        objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intPayPeriodId.ToString)
    '        objDataOperation.AddParameter("@paytypeid", SqlDbType.Int, eZeeDataType.INT_SIZE, intPaymentTypeId.ToString)

    '        dsList = objDataOperation.ExecQuery(strQ, strTableName)

    '        If objDataOperation.ErrorMessage <> "" Then
    '            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
    '            Throw exForce
    '        End If

    '    Catch ex As Exception
    '        Throw New Exception(ex.Message & "; Procedure Name: GetListByPeriod; Module Name: " & mstrModuleName)
    '    Finally
    '        exForce = Nothing
    '        If dsList IsNot Nothing Then dsList.Dispose()
    '        objDataOperation = Nothing
    '    End Try
    '    Return dsList
    'End Function
    Public Function GetListByPeriod(ByVal xDatabaseName As String _
                                    , ByVal xUserUnkid As Integer _
                                    , ByVal xYearUnkid As Integer _
                                    , ByVal xCompanyUnkid As Integer _
                                    , ByVal xPeriodStart As DateTime _
                                    , ByVal xPeriodEnd As DateTime _
                                    , ByVal xUserModeSetting As String _
                                    , ByVal xOnlyApproved As Boolean _
                                    , ByVal xIncludeIn_ActiveEmployee As Boolean _
                                    , ByVal strTableName As String _
                                    , ByVal intPayRefId As Integer _
                                    , ByVal intPayPeriodId As Integer _
                                    , ByVal intPaymentTypeId As Integer _
                                    , Optional ByVal strEmployeeIDs As String = "" _
                                    , Optional ByVal blnApplyUserAccessFilter As Boolean = True _
                                    , Optional ByVal strAdvanceFilter As String = "" _
                                    , Optional ByVal blnApplyDateFilter As Boolean = True _
                                    ) As DataSet
        'Sohail (12 Oct 2021) - [blnApplyDateFilter]

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        Dim xDateJoinQry, xDateFilterQry, xUACQry, xUACFiltrQry, xAdvanceJoinQry As String
        xDateJoinQry = "" : xDateFilterQry = "" : xUACQry = "" : xUACFiltrQry = "" : xAdvanceJoinQry = ""
        If blnApplyDateFilter = True Then 'Sohail (12 Oct 2021)
        Call GetDatesFilterString(xDateJoinQry, xDateFilterQry, xPeriodStart, xPeriodEnd, , , xDatabaseName)
        End If 'Sohail (12 Oct 2021)
        If blnApplyUserAccessFilter = True Then 'Sohail (12 Oct 2021)
        Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, xPeriodEnd, xOnlyApproved, xDatabaseName, xUserUnkid, xCompanyUnkid, xYearUnkid, xUserModeSetting)
        End If 'Sohail (12 Oct 2021)
        If strAdvanceFilter.Trim.Length > 0 Then 'Sohail (26 Oct 2020)
        Call GetAdvanceFilterQry(xAdvanceJoinQry, xPeriodEnd, xDatabaseName)
        End If 'Sohail (26 Oct 2020)

        objDataOperation = New clsDataOperation

        Try

            'Sohail (26 Apr 2016) -- Start
            'Enhancement - 59.1 - Performance optimization for process payroll employee list when payment is done (taking 20 seconds on KBC database for 900 employees, now 0.67 seconds).
            strQ = "CREATE TABLE #cteEmp " & _
                   "( " & _
                     "  employeeunkid INT " & _
                     ", employeecode VARCHAR(MAX) " & _
                     ", firstname VARCHAR(MAX) " & _
                     ", othername VARCHAR(MAX) " & _
                     ", surname VARCHAR(MAX) " & _
                   ") " & _
                   "INSERT INTO #cteEmp " & _
                   "SELECT hremployee_master.employeeunkid " & _
                        ", hremployee_master.employeecode " & _
                        ", hremployee_master.firstname " & _
                        ", hremployee_master.othername " & _
                        ", hremployee_master.surname " & _
                   "FROM hremployee_master "

            If xDateJoinQry.Trim.Length > 0 Then
                strQ &= xDateJoinQry
            End If

            'S.SANDEEP [15 NOV 2016] -- START
            'If xUACQry.Trim.Length > 0 Then
            '    StrQ &= xUACQry
            'End If
            If blnApplyUserAccessFilter = True Then
                If xUACQry.Trim.Length > 0 Then
                    strQ &= xUACQry
                End If
            End If
            'S.SANDEEP [15 NOV 2016] -- END

            If xAdvanceJoinQry.Trim.Length > 0 Then
                strQ &= xAdvanceJoinQry
            End If

            strQ &= "WHERE 1 = 1 "

            If blnApplyUserAccessFilter = True Then
                If xUACFiltrQry.Trim.Length > 0 Then
                    strQ &= " AND " & xUACFiltrQry
                End If
            End If

            If xIncludeIn_ActiveEmployee = False Then
                If xDateFilterQry.Trim.Length > 0 Then
                    strQ &= xDateFilterQry
                End If
            End If

            'Sohail (24 Feb 2017) -- Start
            'Issue - 64.1 - The multi-part identifier "ADF.teamunkid" could not be found while applying Advance Filter on Payroll Report and Global Void Payment.
            If strAdvanceFilter.Trim <> "" Then
                strQ &= " AND " & strAdvanceFilter
            End If
            'Sohail (24 Feb 2017) -- End

            If strEmployeeIDs.Trim <> "" AndAlso strEmployeeIDs.Trim <> "0" AndAlso strEmployeeIDs.Trim <> "-1" Then
                strQ &= " AND hremployee_master.employeeunkid IN (" & strEmployeeIDs & ") "
            End If
            'Sohail (26 Apr 2016) -- End

            'Sohail (26 Apr 2016) -- Start
            'Enhancement - 59.1 - Performance optimization for process payroll employee list when payment is done (taking 20 seconds on KBC database for 900 employees, now 0.67 seconds).
            'strQ = "SELECT   CONVERT(CHAR(8),prpayment_tran.paymentdate,112) AS paymentdate " & _
            '       ",CASE WHEN ISNULL(isglobalpayment,0) = 0 THEN ISNULL(prpayment_tran.voucherno,'') " & _
            '           "            WHEN ISNULL(isglobalpayment,0) = 1 THEN ISNULL(prglobalvoc_master.globalvocno,'') END AS voucherno " & _
            '           ", ISNULL(hremployee_master.employeecode,'') AS employeecode " & _
            '           ", ISNULL(hremployee_master.firstname,'')+' '+ISNULL(hremployee_master.othername,'')+' '+ISNULL(hremployee_master.surname,'') AS EmpName " & _
            '           ", ISNULL(" & FinancialYear._Object._ConfigDatabaseName & "..cffinancial_year_tran.financialyear_name,'') AS YearName " & _
            '           ", ISNULL(cfcommon_period_tran.period_name,'') AS PeriodName " & _
            '           ", ISNULL(prpayment_tran.amount,0) AS Amount " & _
            '           ", hremployee_master.employeeunkid " & _
            '           ", " & FinancialYear._Object._ConfigDatabaseName & "..cffinancial_year_tran.yearunkid " & _
            '           ", cfcommon_period_tran.periodunkid " & _
            '           ", prpayment_tran.paymenttranunkid " & _
            '           ", isglobalpayment " & _
            '           ", paymenttranunkid " & _
            '          ", referenceid " & _
            '          ", paymentmode " & _
            '          ", branchunkid " & _
            '          ", chequeno " & _
            '          ", paymentby " & _
            '          ", percentage " & _
            '          ", voucherref " & _
            '          ", referencetranunkid " & _
            '          ", isreceipt " & _
            '          ", basecurrencyid " & _
            '          ", baseexchangerate " & _
            '          ", paidcurrencyid " & _
            '          ", expaidrate " & _
            '          ", expaidamt " & _
            '          ", ISNULL(isauthorized, 0) AS isauthorized " & _
            '          ", ISNULL(accountno, '') AS accountno " & _
            '          ", ISNULL(prpayment_tran.countryunkid, 0) AS countryunkid " & _
            '          ", ISNULL(prpayment_tran.isapproved, 0) AS isapproved " & _
            '          ", ISNULL(prpayment_tran.roundingtypeid, 0) AS roundingtypeid " & _
            '          ", ISNULL(prpayment_tran.roundingmultipleid, 0) AS roundingmultipleid " & _
            '          ", ISNULL(prpayment_tran.roundingadjustment, 0) AS roundingadjustment " & _
            '          ", ISNULL(prpayment_tran.remarks, '') AS remarks " & _
            '          ", ISNULL(prpayment_tran.paymentdate_periodunkid, 0) AS paymentdate_periodunkid " & _
            '        "FROM prpayment_tran " & _
            '           "LEFT JOIN prglobalvoc_master ON prpayment_tran.globalvocno = prglobalvoc_master.globalvocunkid " & _
            '           "LEFT JOIN hremployee_master ON prpayment_tran.employeeunkid = hremployee_master.employeeunkid " & _
            '           "LEFT JOIN cfcommon_period_tran ON prpayment_tran.periodunkid = cfcommon_period_tran.periodunkid " & _
            '           "LEFT JOIN " & FinancialYear._Object._ConfigDatabaseName & "..cffinancial_year_tran ON cfcommon_period_tran.yearunkid = cffinancial_year_tran.yearunkid "

            'If xDateJoinQry.Trim.Length > 0 Then
            '    strQ &= xDateJoinQry
            'End If

            'If xUACQry.Trim.Length > 0 Then
            '    strQ &= xUACQry
            'End If

            'If xAdvanceJoinQry.Trim.Length > 0 Then
            '    strQ &= xAdvanceJoinQry
            'End If

            'strQ &= "WHERE prpayment_tran.referenceid = @referenceid " & _
            '       "AND prpayment_tran.periodunkid = @periodunkid " & _
            '       "AND prpayment_tran.paytypeid = @paytypeid " & _
            '       "AND ISNULL(prpayment_tran.isvoid,0) = 0 "

            'If blnApplyUserAccessFilter = True Then
            '    If xUACFiltrQry.Trim.Length > 0 Then
            '        strQ &= " AND " & xUACFiltrQry
            '    End If
            'End If

            'If xIncludeIn_ActiveEmployee = False Then
            '    If xDateFilterQry.Trim.Length > 0 Then
            '        strQ &= xDateFilterQry
            '    End If
            'End If
            strQ &= "SELECT   CONVERT(CHAR(8),prpayment_tran.paymentdate,112) AS paymentdate " & _
                   ",CASE WHEN ISNULL(isglobalpayment,0) = 0 THEN ISNULL(prpayment_tran.voucherno,'') " & _
                       "            WHEN ISNULL(isglobalpayment,0) = 1 THEN ISNULL(prglobalvoc_master.globalvocno,'') END AS voucherno " & _
                       ", ISNULL(#cteEmp.employeecode,'') AS employeecode " & _
                       ", ISNULL(#cteEmp.firstname,'')+' '+ISNULL(#cteEmp.othername,'')+' '+ISNULL(#cteEmp.surname,'') AS EmpName " & _
                       ", ISNULL(" & FinancialYear._Object._ConfigDatabaseName & "..cffinancial_year_tran.financialyear_name,'') AS YearName " & _
                       ", ISNULL(cfcommon_period_tran.period_name,'') AS PeriodName " & _
                       ", ISNULL(prpayment_tran.amount,0) AS Amount " & _
                       ", #cteEmp.employeeunkid " & _
                       ", " & FinancialYear._Object._ConfigDatabaseName & "..cffinancial_year_tran.yearunkid " & _
                       ", cfcommon_period_tran.periodunkid " & _
                       ", prpayment_tran.paymenttranunkid " & _
                       ", isglobalpayment " & _
                       ", paymenttranunkid " & _
                      ", referenceid " & _
                      ", paymentmode " & _
                      ", branchunkid " & _
                      ", chequeno " & _
                      ", paymentby " & _
                      ", percentage " & _
                      ", voucherref " & _
                      ", referencetranunkid " & _
                      ", isreceipt " & _
                      ", basecurrencyid " & _
                      ", baseexchangerate " & _
                      ", paidcurrencyid " & _
                      ", expaidrate " & _
                      ", expaidamt " & _
                      ", ISNULL(isauthorized, 0) AS isauthorized " & _
                      ", ISNULL(accountno, '') AS accountno " & _
                      ", ISNULL(prpayment_tran.countryunkid, 0) AS countryunkid " & _
                      ", ISNULL(prpayment_tran.isapproved, 0) AS isapproved " & _
                      ", ISNULL(prpayment_tran.roundingtypeid, 0) AS roundingtypeid " & _
                      ", ISNULL(prpayment_tran.roundingmultipleid, 0) AS roundingmultipleid " & _
                      ", ISNULL(prpayment_tran.roundingadjustment, 0) AS roundingadjustment " & _
                      ", ISNULL(prpayment_tran.remarks, '') AS remarks " & _
                      ", ISNULL(prpayment_tran.paymentdate_periodunkid, 0) AS paymentdate_periodunkid " & _
                    "FROM prpayment_tran " & _
                       "LEFT JOIN prglobalvoc_master ON prpayment_tran.globalvocno = prglobalvoc_master.globalvocunkid " & _
                       "LEFT JOIN #cteEmp ON prpayment_tran.employeeunkid = #cteEmp.employeeunkid " & _
                       "LEFT JOIN cfcommon_period_tran ON prpayment_tran.periodunkid = cfcommon_period_tran.periodunkid " & _
                             "LEFT JOIN " & FinancialYear._Object._ConfigDatabaseName & "..cffinancial_year_tran ON cfcommon_period_tran.yearunkid = cffinancial_year_tran.yearunkid "

            strQ &= "WHERE prpayment_tran.isvoid = 0 " & _
                    "AND #cteEmp.employeeunkid IS NOT NULL " & _
                    "AND prpayment_tran.periodunkid = @periodunkid " & _
                    "AND prpayment_tran.paytypeid = @paytypeid " & _
                    "AND prpayment_tran.referenceid = @referenceid "
            'Sohail (26 Apr 2016) -- End

            If strEmployeeIDs.Trim <> "" AndAlso strEmployeeIDs.Trim <> "0" AndAlso strEmployeeIDs.Trim <> "-1" Then
                strQ &= " AND prpayment_tran.employeeunkid IN (" & strEmployeeIDs & ") "
            End If

            'Sohail (24 Feb 2017) -- Start
            'Issue - 64.1 - The multi-part identifier "ADF.teamunkid" could not be found while applying Advance Filter on Payroll Report and Global Void Payment.
            'If strFilter.Trim <> "" Then
            '    strQ &= " AND " & strFilter
            'End If
            'Sohail (24 Feb 2017) -- End

            'Sohail (26 Apr 2016) -- Start
            'Enhancement - 59.1 - Performance optimization for process payroll employee list when payment is done (taking 20 seconds on KBC database for 900 employees, now 0.67 seconds).
            strQ &= " DROP TABLE #cteEmp "
            'Sohail (26 Apr 2016) -- End

            objDataOperation.AddParameter("@referenceid", SqlDbType.Int, eZeeDataType.INT_SIZE, intPayRefId.ToString)
            objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intPayPeriodId.ToString)
            objDataOperation.AddParameter("@paytypeid", SqlDbType.Int, eZeeDataType.INT_SIZE, intPaymentTypeId.ToString)

            dsList = objDataOperation.ExecQuery(strQ, strTableName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetListByPeriod; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
        Return dsList
    End Function
    'Sohail (21 Aug 2015) -- End

    'Sohail (11 Mar 2013) -- Start
    'TRA - ENHANCEMENT
    'Sohail (21 Aug 2015) -- Start
    'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
    'Public Function GetLastPaymentDetail(ByVal strTableName As String, _
    '                                     ByVal intPayRefId As Integer, _
    '                                     ByVal intPaymentTypeId As Integer, _
    '                                     Optional ByVal intPeriodId As Integer = 0, _
    '                                     Optional ByVal strFilterQuery As String = "" _
    '                                     ) As DataSet

    '    Dim dsList As DataSet = Nothing
    '    Dim strQ As String = ""
    '    Dim exForce As Exception

    '    objDataOperation = New clsDataOperation

    '    Try

    '        strQ = "SELECT TOP 1 " & _
    '                   "  CONVERT(CHAR(8),prpayment_tran.paymentdate,112) AS PayDate " & _
    '                   ", prpayment_tran.paymentdate " & _
    '                   ", CASE WHEN ISNULL(isglobalpayment,0) = 0 THEN ISNULL(prpayment_tran.voucherno,'') " & _
    '                   "           WHEN ISNULL(isglobalpayment,0) = 1 THEN ISNULL(prglobalvoc_master.globalvocno,'') END AS Voc " & _
    '                   ", ISNULL(hremployee_master.employeecode,'') AS employeecode " & _
    '                   ", ISNULL(hremployee_master.firstname,'')+' '+ISNULL(hremployee_master.othername,'')+' '+ISNULL(hremployee_master.surname,'') AS EmpName " & _
    '                   ", ISNULL(" & FinancialYear._Object._ConfigDatabaseName & "..cffinancial_year_tran.financialyear_name,'') AS YearName " & _
    '                   ", ISNULL(cfcommon_period_tran.period_name,'') AS PeriodName " & _
    '                   ", ISNULL(prpayment_tran.amount,0) AS Amount " & _
    '                   ", hremployee_master.employeeunkid " & _
    '                   ", " & FinancialYear._Object._ConfigDatabaseName & "..cffinancial_year_tran.yearunkid " & _
    '                   ", cfcommon_period_tran.periodunkid " & _
    '                   ", prpayment_tran.paymenttranunkid " & _
    '                   ", isglobalpayment " & _
    '                   ", basecurrencyid " & _
    '                   ", baseexchangerate " & _
    '                   ", paidcurrencyid " & _
    '                   ", expaidrate " & _
    '                   ", expaidamt " & _
    '                   ", ISNULL(isauthorized, 0) AS isauthorized " & _
    '                   ", paymentmode " & _
    '                   ", prpayment_tran.userunkid " & _
    '                   ", ISNULL(cfuser_master.username, '') AS username " & _
    '                   ", ISNULL(cfuser_master.firstname, '') + ' ' + ISNULL(cfuser_master.lastname, '') AS userfullname " & _
    '                   ", ISNULL(accountno, '') AS accountno " & _
    '                   ", ISNULL(prpayment_tran.countryunkid, 0) AS countryunkid " & _
    '                   ", ISNULL(prpayment_tran.isapproved, 0) AS isapproved " & _
    '                   ", ISNULL(prpayment_tran.roundingtypeid, 0) AS roundingtypeid " & _
    '                   ", ISNULL(prpayment_tran.roundingmultipleid, 0) AS roundingmultipleid " & _
    '                   ", ISNULL(prpayment_tran.roundingadjustment, 0) AS roundingadjustment " & _
    '                   ", ISNULL(prpayment_tran.remarks, '') AS remarks " & _
    '                "FROM prpayment_tran " & _
    '                   "LEFT JOIN prglobalvoc_master ON prpayment_tran.globalvocno = prglobalvoc_master.globalvocunkid " & _
    '                   "LEFT JOIN hremployee_master ON prpayment_tran.employeeunkid = hremployee_master.employeeunkid " & _
    '                   "LEFT JOIN cfcommon_period_tran ON prpayment_tran.periodunkid = cfcommon_period_tran.periodunkid " & _
    '                   "LEFT JOIN " & FinancialYear._Object._ConfigDatabaseName & "..cffinancial_year_tran ON cfcommon_period_tran.yearunkid = cffinancial_year_tran.yearunkid " & _
    '                   "LEFT JOIN " & FinancialYear._Object._ConfigDatabaseName & "..cfuser_master ON cfuser_master.userunkid = prpayment_tran.userunkid " & _
    '                "WHERE ISNULL(prpayment_tran.isvoid,0) = 0 " & _
    '                "AND prpayment_tran.referenceid = @referenceid " & _
    '                "AND prpayment_tran.paytypeid = @paytypeid "
    '        '       'Sohail (24 Dec 2014) - [remarks]
    '        '       'Sohail (21 Mar 2014) - [roundingadjustment]
    '        '       'Sohail (22 Oct 2013) - [roundingtypeid, roundingmultipleid]
    '        '       'Sohail (16 Mar 2013) - [userfullname]


    '        'strQ &= UserAccessLevel._AccessLevelFilterString

    '        If intPeriodId > 0 Then
    '            strQ &= " AND prpayment_tran.periodunkid = @periodunkid "
    '            objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intPeriodId.ToString)
    '        End If

    '        If strFilterQuery.Trim <> "" Then
    '            strQ &= " AND " & strFilterQuery
    '        End If

    '        strQ &= " ORDER BY prpayment_tran.paymenttranunkid DESC "

    '        objDataOperation.AddParameter("@referenceid", SqlDbType.Int, eZeeDataType.INT_SIZE, intPayRefId.ToString)
    '        objDataOperation.AddParameter("@paytypeid", SqlDbType.Int, eZeeDataType.INT_SIZE, intPaymentTypeId.ToString)

    '        dsList = objDataOperation.ExecQuery(strQ, strTableName)

    '        If objDataOperation.ErrorMessage <> "" Then
    '            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
    '            Throw exForce
    '        End If


    '    Catch ex As Exception
    '        Throw New Exception(ex.Message & "; Procedure Name: GetLastPaymentDetail; Module Name: " & mstrModuleName)
    '    Finally
    '        exForce = Nothing
    '        If dsList IsNot Nothing Then dsList.Dispose()
    '        objDataOperation = Nothing
    '    End Try
    '    Return dsList
    'End Function
    Public Function GetLastPaymentDetail(ByVal xDatabaseName As String _
                                         , ByVal xUserUnkid As Integer _
                                         , ByVal xYearUnkid As Integer _
                                         , ByVal xCompanyUnkid As Integer _
                                         , ByVal xPeriodStart As DateTime _
                                         , ByVal xPeriodEnd As DateTime _
                                         , ByVal xUserModeSetting As String _
                                         , ByVal xOnlyApproved As Boolean _
                                         , ByVal xIncludeIn_ActiveEmployee As Boolean _
                                         , ByVal strTableName As String _
                                         , ByVal intPayRefId As Integer _
                                         , ByVal intPaymentTypeId As Integer _
                                         , Optional ByVal intPeriodId As Integer = 0 _
                                         , Optional ByVal blnApplyUserAccessFilter As Boolean = False _
                                         , Optional ByVal strFilterQuery As String = "" _
                                         ) As DataSet

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        Dim xDateJoinQry, xDateFilterQry, xUACQry, xUACFiltrQry, xAdvanceJoinQry As String
        xDateJoinQry = "" : xDateFilterQry = "" : xUACQry = "" : xUACFiltrQry = "" : xAdvanceJoinQry = ""
        Call GetDatesFilterString(xDateJoinQry, xDateFilterQry, xPeriodStart, xPeriodEnd, , , xDatabaseName)
        Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, xPeriodEnd, xOnlyApproved, xDatabaseName, xUserUnkid, xCompanyUnkid, xYearUnkid, xUserModeSetting)
        Call GetAdvanceFilterQry(xAdvanceJoinQry, xPeriodEnd, xDatabaseName)

        objDataOperation = New clsDataOperation

        Try

            strQ = "SELECT TOP 1 " & _
                       "  CONVERT(CHAR(8),prpayment_tran.paymentdate,112) AS PayDate " & _
                       ", prpayment_tran.paymentdate " & _
                       ", CASE WHEN ISNULL(isglobalpayment,0) = 0 THEN ISNULL(prpayment_tran.voucherno,'') " & _
                       "           WHEN ISNULL(isglobalpayment,0) = 1 THEN ISNULL(prglobalvoc_master.globalvocno,'') END AS Voc " & _
                       ", ISNULL(hremployee_master.employeecode,'') AS employeecode " & _
                       ", ISNULL(hremployee_master.firstname,'')+' '+ISNULL(hremployee_master.othername,'')+' '+ISNULL(hremployee_master.surname,'') AS EmpName " & _
                       ", ISNULL(" & FinancialYear._Object._ConfigDatabaseName & "..cffinancial_year_tran.financialyear_name,'') AS YearName " & _
                       ", ISNULL(cfcommon_period_tran.period_name,'') AS PeriodName " & _
                       ", ISNULL(prpayment_tran.amount,0) AS Amount " & _
                       ", hremployee_master.employeeunkid " & _
                       ", " & FinancialYear._Object._ConfigDatabaseName & "..cffinancial_year_tran.yearunkid " & _
                       ", cfcommon_period_tran.periodunkid " & _
                       ", prpayment_tran.paymenttranunkid " & _
                       ", isglobalpayment " & _
                       ", basecurrencyid " & _
                       ", baseexchangerate " & _
                       ", paidcurrencyid " & _
                       ", expaidrate " & _
                       ", expaidamt " & _
                       ", ISNULL(isauthorized, 0) AS isauthorized " & _
                       ", paymentmode " & _
                       ", prpayment_tran.userunkid " & _
                       ", ISNULL(cfuser_master.username, '') AS username " & _
                       ", ISNULL(cfuser_master.firstname, '') + ' ' + ISNULL(cfuser_master.lastname, '') AS userfullname " & _
                       ", ISNULL(accountno, '') AS accountno " & _
                       ", ISNULL(prpayment_tran.countryunkid, 0) AS countryunkid " & _
                       ", ISNULL(prpayment_tran.isapproved, 0) AS isapproved " & _
                       ", ISNULL(prpayment_tran.roundingtypeid, 0) AS roundingtypeid " & _
                       ", ISNULL(prpayment_tran.roundingmultipleid, 0) AS roundingmultipleid " & _
                       ", ISNULL(prpayment_tran.roundingadjustment, 0) AS roundingadjustment " & _
                       ", ISNULL(prpayment_tran.remarks, '') AS remarks " & _
                       ", ISNULL(prpayment_tran.paymentdate_periodunkid, 0) AS paymentdate_periodunkid " & _
                    "FROM prpayment_tran " & _
                       "LEFT JOIN prglobalvoc_master ON prpayment_tran.globalvocno = prglobalvoc_master.globalvocunkid " & _
                       "LEFT JOIN hremployee_master ON prpayment_tran.employeeunkid = hremployee_master.employeeunkid " & _
                       "LEFT JOIN cfcommon_period_tran ON prpayment_tran.periodunkid = cfcommon_period_tran.periodunkid " & _
                       "LEFT JOIN " & FinancialYear._Object._ConfigDatabaseName & "..cffinancial_year_tran ON cfcommon_period_tran.yearunkid = cffinancial_year_tran.yearunkid " & _
                           "LEFT JOIN " & FinancialYear._Object._ConfigDatabaseName & "..cfuser_master ON cfuser_master.userunkid = prpayment_tran.userunkid "

            If xDateJoinQry.Trim.Length > 0 Then
                strQ &= xDateJoinQry
            End If

            'S.SANDEEP [15 NOV 2016] -- START
            'If xUACQry.Trim.Length > 0 Then
            '    StrQ &= xUACQry
            'End If
            'S.SANDEEP [15 NOV 2016] -- END

            If xAdvanceJoinQry.Trim.Length > 0 Then
                strQ &= xAdvanceJoinQry
            End If

            strQ &= "WHERE ISNULL(prpayment_tran.isvoid,0) = 0 " & _
                    "AND prpayment_tran.referenceid = @referenceid " & _
                    "AND prpayment_tran.paytypeid = @paytypeid "


            'strQ &= UserAccessLevel._AccessLevelFilterString
            'If blnApplyUserAccessFilter = True Then
            '    If xUACFiltrQry.Trim.Length > 0 Then
            '        strQ &= " AND " & xUACFiltrQry
            '    End If
            'End If

            If xIncludeIn_ActiveEmployee = False Then
                If xDateFilterQry.Trim.Length > 0 Then
                    strQ &= xDateFilterQry
                End If
            End If

            If intPeriodId > 0 Then
                strQ &= " AND prpayment_tran.periodunkid = @periodunkid "
                objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intPeriodId.ToString)
            End If

            If strFilterQuery.Trim <> "" Then
                strQ &= " AND " & strFilterQuery
            End If

            strQ &= " ORDER BY prpayment_tran.paymenttranunkid DESC "

            objDataOperation.AddParameter("@referenceid", SqlDbType.Int, eZeeDataType.INT_SIZE, intPayRefId.ToString)
            objDataOperation.AddParameter("@paytypeid", SqlDbType.Int, eZeeDataType.INT_SIZE, intPaymentTypeId.ToString)

            dsList = objDataOperation.ExecQuery(strQ, strTableName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If


        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetLastPaymentDetail; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
        Return dsList
    End Function
    'Sohail (21 Aug 2015) -- End

    Public Function GetTotalPaymentCount(ByVal xDatabaseName As String _
                                        , ByVal xUserUnkid As Integer _
                                        , ByVal intYearID As Integer _
                                        , ByVal xCompanyUnkid As Integer _
                                        , ByVal xPeriodStart As DateTime _
                                        , ByVal xPeriodEnd As DateTime _
                                        , ByVal xUserModeSetting As String _
                                        , ByVal xOnlyApproved As Boolean _
                                        , ByVal blnIncludeInActiveEmployee As Boolean _
                                        , ByVal blnApplyUserAccessFilter As Boolean _
                                        , ByVal intPayRefId As Integer _
                                        , ByVal intPaymentTypeId As Integer _
                                        , Optional ByVal intPeriodId As Integer = 0 _
                                        , Optional ByVal strFilterQuery As String = "" _
                                        , Optional ByVal strAdvanceFilter As String = "" _
                                         ) As Integer
        'Sohail (24 Jun 2019) - [xDatabaseName, xUserUnkid, intYearID, xCompanyUnkid, xPeriodStart, xPeriodEnd, xUserModeSetting, xOnlyApproved, blnIncludeInActiveEmployee, blnApplyUserAccessFilter, strAdvanceFilter]

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try

            'Sohail (24 Jun 2019) -- Start
            'NMB Enhancement - REF # - 76.1 - TC012: There should be payroll authorization check before payroll batch posting to flex cube.
            Dim xDateJoinQry, xDateFilterQry, xUACQry, xUACFiltrQry, xAdvanceJoinQry As String
            xDateJoinQry = "" : xDateFilterQry = "" : xUACQry = "" : xUACFiltrQry = "" : xAdvanceJoinQry = ""
            Call GetDatesFilterString(xDateJoinQry, xDateFilterQry, xPeriodStart, xPeriodEnd, , , xDatabaseName)
            Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, xPeriodEnd, xOnlyApproved, xDatabaseName, xUserUnkid, xCompanyUnkid, intYearID, xUserModeSetting)
            Call GetAdvanceFilterQry(xAdvanceJoinQry, xPeriodEnd, xDatabaseName)

            strQ = "SELECT  hremployee_master.employeeunkid " & _
                        ", hremployee_master.employeecode " & _
                        ", ISNULL(hremployee_master.firstname + ' ' + hremployee_master.othername + ' ' + hremployee_master.surname,'') AS employeename " & _
                        "INTO #tblEmp " & _
                  "FROM    hremployee_master "

            If xDateJoinQry.Trim.Length > 0 Then
                strQ &= xDateJoinQry
            End If

            If blnApplyUserAccessFilter = True Then
                If xUACQry.Trim.Length > 0 Then
                    strQ &= xUACQry
                End If
            End If

            If xAdvanceJoinQry.Trim.Length > 0 Then
                strQ &= xAdvanceJoinQry
            End If

            strQ &= "WHERE 1 = 1 "

            If blnApplyUserAccessFilter = True Then
                If xUACFiltrQry.Trim.Length > 0 Then
                    strQ &= " AND " & xUACFiltrQry
                End If
            End If

            If blnIncludeInActiveEmployee = False Then
                If xDateFilterQry.Trim.Length > 0 Then
                    strQ &= xDateFilterQry
                End If
            End If

            If strAdvanceFilter.Trim <> "" Then
                strQ &= " AND " & strAdvanceFilter & " "
            End If
            'Sohail (24 Jun 2019) -- End

            strQ &= "SELECT COUNT(paymenttranunkid) AS TotalCount " & _
                    "FROM prpayment_tran " & _
                    "JOIN #tblEmp AS emp ON prpayment_tran.employeeunkid = emp.employeeunkid " & _
                    "WHERE ISNULL(prpayment_tran.isvoid,0) = 0 " & _
                    "AND prpayment_tran.referenceid = @referenceid " & _
                    "AND prpayment_tran.paytypeid = @paytypeid "
            'Sohail (24 Jun 2019) - [JOIN #tblEmp]

            'strQ &= UserAccessLevel._AccessLevelFilterString

            If intPeriodId > 0 Then
                strQ &= " AND prpayment_tran.periodunkid = @periodunkid "
                objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intPeriodId)
            End If

            If strFilterQuery.Trim <> "" Then
                strQ &= " AND " & strFilterQuery
            End If

            'Sohail (24 Jun 2019) -- Start
            'NMB Enhancement - REF # - 76.1 - TC012: There should be payroll authorization check before payroll batch posting to flex cube.
            strQ &= " DROP TABLE #tblEmp "
            'Sohail (24 Jun 2019) -- End

            objDataOperation.AddParameter("@referenceid", SqlDbType.Int, eZeeDataType.INT_SIZE, intPayRefId)
            objDataOperation.AddParameter("@paytypeid", SqlDbType.Int, eZeeDataType.INT_SIZE, intPaymentTypeId)

            dsList = objDataOperation.ExecQuery(strQ, "PaidCount")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables("PaidCount").Rows(0).Item("TotalCount")

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetLastPaymentDetail; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function
    'Sohail (11 Mar 2013) -- End

    'Sohail (01 Feb 2014) -- Start
    Public Function GetDistinctPaymentUsers(ByVal xDatabaseName As String _
                                            , ByVal xUserUnkid As Integer _
                                            , ByVal xYearUnkid As Integer _
                                            , ByVal xCompanyUnkid As Integer _
                                            , ByVal xPeriodStart As DateTime _
                                            , ByVal xPeriodEnd As DateTime _
                                            , ByVal xUserModeSetting As String _
                                            , ByVal xOnlyApproved As Boolean _
                                            , ByVal xIncludeIn_ActiveEmployee As Boolean _
                                            , ByVal strTableName As String _
                                            , ByVal intPayRefId As Integer _
                                            , ByVal intPaymentTypeId As Integer _
                                            , Optional ByVal intPeriodId As Integer = 0 _
                                            , Optional ByVal blnApplyUserAccessFilter As Boolean = True _
                                            , Optional ByVal strFilterQuery As String = "" _
                                         ) As DataSet

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        Dim xDateJoinQry, xDateFilterQry, xUACQry, xUACFiltrQry, xAdvanceJoinQry As String
        xDateJoinQry = "" : xDateFilterQry = "" : xUACQry = "" : xUACFiltrQry = "" : xAdvanceJoinQry = ""
        Call GetDatesFilterString(xDateJoinQry, xDateFilterQry, xPeriodStart, xPeriodEnd, , , xDatabaseName)
        Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, xPeriodEnd, xOnlyApproved, xDatabaseName, xUserUnkid, xCompanyUnkid, xYearUnkid, xUserModeSetting)
        Call GetAdvanceFilterQry(xAdvanceJoinQry, xPeriodEnd, xDatabaseName)

        objDataOperation = New clsDataOperation

        Try

            strQ = "SELECT DISTINCT " & _
                       "  CONVERT(CHAR(8),prpayment_tran.paymentdate,112) AS PayDate " & _
                       ", prpayment_tran.paymentdate " & _
                       ", prpayment_tran.userunkid " & _
                       ", ISNULL(cfuser_master.username, '') AS username " & _
                       ", ISNULL(cfuser_master.firstname, '') + ' ' + ISNULL(cfuser_master.lastname, '') AS userfullname " & _
                    "FROM prpayment_tran " & _
                       "LEFT JOIN prglobalvoc_master ON prpayment_tran.globalvocno = prglobalvoc_master.globalvocunkid " & _
                       "LEFT JOIN hremployee_master ON prpayment_tran.employeeunkid = hremployee_master.employeeunkid " & _
                       "LEFT JOIN cfcommon_period_tran ON prpayment_tran.periodunkid = cfcommon_period_tran.periodunkid " & _
                       "LEFT JOIN " & FinancialYear._Object._ConfigDatabaseName & "..cffinancial_year_tran ON cfcommon_period_tran.yearunkid = cffinancial_year_tran.yearunkid " & _
                       "LEFT JOIN " & FinancialYear._Object._ConfigDatabaseName & "..cfuser_master ON cfuser_master.userunkid = prpayment_tran.userunkid "

            If xDateJoinQry.Trim.Length > 0 Then
                strQ &= xDateJoinQry
            End If

            'S.SANDEEP [15 NOV 2016] -- START
            'If xUACQry.Trim.Length > 0 Then
            '    StrQ &= xUACQry
            'End If
            'S.SANDEEP [15 NOV 2016] -- END

            If xAdvanceJoinQry.Trim.Length > 0 Then
                strQ &= xAdvanceJoinQry
            End If

            strQ &= "WHERE ISNULL(prpayment_tran.isvoid,0) = 0 " & _
                    "AND prpayment_tran.referenceid = @referenceid " & _
                    "AND prpayment_tran.paytypeid = @paytypeid "


            'strQ &= UserAccessLevel._AccessLevelFilterString
            'If blnApplyUserAccessFilter = True Then
            '    If xUACFiltrQry.Trim.Length > 0 Then
            '        strQ &= " AND " & xUACFiltrQry
            '    End If
            'End If

            If xIncludeIn_ActiveEmployee = False Then
                If xDateFilterQry.Trim.Length > 0 Then
                    strQ &= xDateFilterQry
                End If
            End If


            If intPeriodId > 0 Then
                strQ &= " AND prpayment_tran.periodunkid = @periodunkid "
                objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intPeriodId.ToString)
            End If

            If strFilterQuery.Trim <> "" Then
                strQ &= " AND " & strFilterQuery
            End If

            objDataOperation.AddParameter("@referenceid", SqlDbType.Int, eZeeDataType.INT_SIZE, intPayRefId.ToString)
            objDataOperation.AddParameter("@paytypeid", SqlDbType.Int, eZeeDataType.INT_SIZE, intPaymentTypeId.ToString)

            dsList = objDataOperation.ExecQuery(strQ, strTableName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If


        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetLastPaymentDetail; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
        Return dsList
    End Function
    'Sohail (01 Feb 2014) -- End

    'Sohail (16 Oct 2010) -- Start

    'Sohail (21 Aug 2015) -- Start
    'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
    'Public Function InsertAll(ByVal dtTable As DataTable) As Boolean
    Public Function InsertAll(ByVal dtTable As DataTable _
                              , ByVal intPaymentVocNoType As Integer _
                              , ByVal strDatabaseName As String _
                              , ByVal intCompanyunkid As Integer _
                              , ByVal intYearUnkid As Integer _
                              , ByVal xPeriodStart As Date _
                              , ByVal xPeriodEnd As Date _
                              , ByVal strUserAccessModeSetting As String _
                              , ByVal xOnlyApproved As Boolean _
                              , ByVal blnIsIncludeInactiveEmp As Boolean _
                              , ByVal intUserunkid As Integer _
                              , ByVal dtCurrentDateAndTime As Date _
                              , ByVal blnApplyUserAccessFilter As Boolean _
                              , ByVal strTableName As String _
                              , ByVal strFilter As String _
                              , ByVal mdtActivityAdjustment As DataTable _
                              , ByVal strEmployeeIDs As String _
                              , Optional ByVal bw As BackgroundWorker = Nothing _
                              , Optional ByVal strFmtCurrency As String = "" _
                              ) As Boolean
        'Sohail (26 Oct 2020) - [strFmtCurrency]
        'Sohail (30 Apr 2019) - [bw]
        'Sohail (30 Oct 2018) - [strEmployeeIDs]
        'Sohail (23 May 2017) - [mdtActivityAdjustment]
        'Sohail (21 Aug 2015) -- End


        Dim objDataOperation As clsDataOperation

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim blnFlag As Boolean = False
        Dim dtTran As DataTable = Nothing
        Dim objEmp As New clsEmployee_Master 'Sohail (21 Mar 2014)

        objDataOperation = New clsDataOperation

        Try

            objDataOperation.BindTransaction()

            'Sohail (30 Apr 2019) -- Start
            'BBLMS Issue - Support Issue Id # 0003781 - 74.1 - Payslip global payment takes hours to process help needed urgently.
            Dim intCount As Integer = 0
            If bw IsNot Nothing Then
                bw.ReportProgress(0)
            End If
            'Sohail (30 Apr 2019) -- End

            'Sohail (18 May 2013) -- Start
            'TRA - ENHANCEMENT
            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'If mintPaymentVocNoType = -1 Then mintPaymentVocNoType = ConfigParameter._Object._PaymentVocNoType
            'If mintCompanyunkid <= 0 Then mintCompanyunkid = Company._Object._Companyunkid
            'If mintYearUnkid <= 0 Then mintYearUnkid = FinancialYear._Object._YearUnkid
            'If mintUserunkid <= 0 Then mintUserunkid = User._Object._Userunkid
            If mintPaymentVocNoType = -1 Then mintPaymentVocNoType = intPaymentVocNoType
            If mintCompanyunkid <= 0 Then mintCompanyunkid = intCompanyunkid
            If mintYearUnkid <= 0 Then mintYearUnkid = intYearUnkid
            If mintUserunkid <= 0 Then mintUserunkid = intUserunkid
            'If mstrClientIP.Trim = "" Then mstrClientIP = getIP()
            'If mstrHostName.Trim = "" Then mstrHostName = getHostName()

            'Sohail (07 May 2015) -- Start
            'Enhancement - New Loan Redesign with allow to change interest rate, tenure, topup, receipt.
            Dim objPeriod As New clscommom_period_Tran
            Dim dsLoan As New DataSet
            Dim intPrevPeriod As Integer = 0
            'Sohail (07 May 2015) -- End

            'Anjan (21 Jan 2012)-Start
            'ENHANCEMENT : TRA COMMENTS 
            'Issue : To fix the issue on network of autocode concurrency when more than 1 user press save at same time.
            'Sohail (15 Dec 2010) -- Start


            'S.SANDEEP [ 20 APRIL 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            'Dim intPayVocNoType As Integer = 0
            'Dim strPayVocPrifix As String = ""
            'Dim intNextPayVocNo As Integer = 0

            'intPayVocNoType = ConfigParameter._Object._PaymentVocNoType

            'If intPayVocNoType = 1 Then
            '    ConfigParameter._Object.Refresh()
            '    strPayVocPrifix = ConfigParameter._Object._PaymentVocPrefix
            '    intNextPayVocNo = ConfigParameter._Object._NextPaymentVocNo
            '    mstrVoucherno = strPayVocPrifix & intNextPayVocNo
            'End If
            ''Sohail (15 Dec 2010) -- End
            'If isExist(mstrVoucherno) Then
            '    'Sohail (15 Dec 2010) -- Start
            '    'mstrMessage = Language.getMessage(mstrModuleName, 1, "Voucher No. is already defined. Please define new Voucher No.")
            '    'Return False
            '    If intPayVocNoType = 1 Then
            '        intNextPayVocNo = GetMaxPaymentVoucherNo() + 1
            '        mstrVoucherno = strPayVocPrifix & intNextPayVocNo
            '        'mstrMessage = Language.getMessage(mstrModuleName, 2, "Voucher No. '" & mstrVoucherno & "' is already defined. Please define new Voucher No. from : Aruti Configuration -> Option -> Payment Voc# No.")
            '    Else
            '        mstrMessage = Language.getMessage(mstrModuleName, 1, "This Voucher No. is already defined. Please define new Voucher No.")
            '        Return False
            '    End If
            '    'Sohail (15 Dec 2010) -- End

            'End If
            Dim intPayVocNoType As Integer = 0
            'Sohail (18 May 2013) -- Start
            'TRA - ENHANCEMENT
            'intPayVocNoType = ConfigParameter._Object._PaymentVocNoType
            intPayVocNoType = mintPaymentVocNoType
            'Sohail (18 May 2013) -- End
            'S.SANDEEP [ 24 DEC 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            Dim iGlobalVocNoId As Integer = 0
            'S.SANDEEP [ 24 DEC 2012 ] -- END
            If intPayVocNoType = 0 Then
                If isExist(mstrVoucherno) Then
                    mstrMessage = Language.getMessage(mstrModuleName, 1, "This Voucher No. is already defined. Please define new Voucher No.")
                    objDataOperation.ReleaseTransaction(False)
                    Return False
                End If
            Else
                'Sohail (18 May 2013) -- Start
                'TRA - ENHANCEMENT
                'mstrVoucherno = GetMaxPaymentVocNo(objDataOperation, Company._Object._Companyunkid)
                mstrVoucherno = GetMaxPaymentVocNo(objDataOperation, mintCompanyunkid)
                'Sohail (18 May 2013) -- End
                'S.SANDEEP [ 24 DEC 2012 ] -- START
                'ENHANCEMENT : TRA CHANGES

                'Nilay (04-Nov-2016) -- Start
                'Enhancements: Global Change Status feature requested by {Rutta}
                'NOTE: When global payment done and voc# is not auto incremented that time globalvocno fall 0 in prpayment_tran
                'If mstrVoucherno.ToString.Trim.Length > 0 Then
                '    iGlobalVocNoId = Insert_GvocNo(objDataOperation, mstrVoucherno)
                '    If iGlobalVocNoId <= 0 Then
                '        objDataOperation.ReleaseTransaction(False)
                '        Return False
                '    End If
                'End If
                'Nilay (04-Nov-2016) -- End
                'S.SANDEEP [ 24 DEC 2012 ] -- END
            End If
            'S.SANDEEP [ 20 APRIL 2012 ] -- END
            'Anjan (21 Jan 2012)-End 

            'Nilay (04-Nov-2016) -- Start
            'Enhancements: Global Change Status feature requested by {Rutta}
            'NOTE: To make entry of globalvocno in prpayment_tran  when paymentvoc# is set to default
            If mstrVoucherno.ToString.Trim.Length > 0 Then
                iGlobalVocNoId = Insert_GvocNo(objDataOperation, mstrVoucherno)
                If iGlobalVocNoId <= 0 Then
                    objDataOperation.ReleaseTransaction(False)
                    Return False
                End If
            End If
            'Nilay (04-Nov-2016) -- End

            'S.SANDEEP [ 20 APRIL 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES

            'S.SANDEEP [ 24 DEC 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            'mstrGlobalvocno = mstrVoucherno
            mstrGlobalvocno = iGlobalVocNoId
            'S.SANDEEP [ 24 DEC 2012 ] -- END
            'S.SANDEEP [ 20 APRIL 2012 ] -- END

            'Sohail (21 Mar 2014) -- Start
            'Enhancement - Set Limit for C/F Netpay balance and Payment JV Report
            Dim objTranHead As New clsTransactionHead
            Dim mintRoundingAdjustmentHeadID As Integer = objTranHead.GetRoundingAdjustmentHeadID()
            objTranHead = Nothing
            'Sohail (21 Mar 2014) -- End

            'Sohail (30 Oct 2018) -- Start
            'FDRC Enhancement - Global Payment performance issue on FDRC database in 75.1.
            Dim objEmpBank As New clsEmployeeBanks
            Dim objpayrollProcess As New clsPayrollProcessTran
            Dim ds As DataSet = Nothing
            Dim dsProcess As DataSet = Nothing
            Dim intDefaultCostCenterUnkId As Integer = 0
            If mintPaymentrefid = CInt(enPaymentRefId.PAYSLIP) AndAlso Not (mintPaymentmodeid = CInt(enPaymentMode.CASH) OrElse mintPaymentmodeid = CInt(enPaymentMode.CASH_AND_CHEQUE)) Then
                ds = objEmpBank.GetList(strDatabaseName, intUserunkid, intYearUnkid, intCompanyunkid, xPeriodStart, xPeriodEnd, strUserAccessModeSetting, True, blnIsIncludeInactiveEmp, "Banks", True, , strEmployeeIDs, xPeriodEnd, "end_date DESC, EmpName, priority")
            End If
            'Sohail (29 Nov 2018) -- Start
            'BBL Issue - Error on Global Payment in 74.1.
            'dsProcess = objpayrollProcess.GetList(strDatabaseName, intUserunkid, intYearUnkid, intCompanyunkid, xPeriodStart, xPeriodEnd, strUserAccessModeSetting, xOnlyApproved, blnIsIncludeInactiveEmp, strTableName, 0, mintPeriodunkid, mintPayreftranunkid, blnApplyUserAccessFilter, " prpayrollprocess_tran.loanadvancetranunkid > 0 OR prpayrollprocess_tran.savingtranunkid > 0 ", strEmployeeIDs)
            dsProcess = objpayrollProcess.GetList(strDatabaseName, intUserunkid, intYearUnkid, intCompanyunkid, xPeriodStart, xPeriodEnd, strUserAccessModeSetting, xOnlyApproved, blnIsIncludeInactiveEmp, strTableName, 0, mintPeriodunkid, mintPayreftranunkid, blnApplyUserAccessFilter, " (prpayrollprocess_tran.loanadvancetranunkid > 0 OR prpayrollprocess_tran.savingtranunkid > 0 ) ", strEmployeeIDs)
            'Sohail (29 Nov 2018) -- End
            'Sohail (30 Oct 2018) -- End

            For Each dtRow As DataRow In dtTable.Rows
                objDataOperation.ClearParameters()

                'mstrVoucherno = dtRow.Item("Voucherno").ToString 'Sohail (15 Dec 2010)
                mintPeriodunkid = dtRow.Item("Periodunkid").ToString
                mdtPaymentdate = CDate(dtRow.Item("Paymentdate").ToString)
                mintEmployeeunkid = dtRow.Item("Employeeunkid").ToString
                mintPaymentrefid = dtRow.Item("Paymentrefid").ToString
                mintPaymentmodeid = dtRow.Item("Paymentmodeid").ToString
                mintBranchunkid = dtRow.Item("Branchunkid").ToString
                mstrChequeno = dtRow.Item("Chequeno").ToString
                mintPaymentbyid = dtRow.Item("Paymentbyid").ToString
                mdblPercentage = dtRow.Item("Percentage").ToString
                mdecAmount = dtRow.Item("Amount").ToString
                mstrVoucherref = dtRow.Item("Voucherref").ToString
                mintPayreftranunkid = dtRow.Item("Payreftranunkid").ToString
                mintUserunkid = dtRow.Item("Userunkid").ToString
                mblnIsvoid = dtRow.Item("Isvoid").ToString
                mintVoiduserunkid = dtRow.Item("Voiduserunkid").ToString
                If dtRow.Item("Voiddatetime").ToString = Nothing Then
                    mdtVoiddatetime = Nothing
                Else
                    mdtVoiddatetime = dtRow.Item("Voiddatetime").ToString
                End If
                mintPaymentTypeId = dtRow.Item("PaymentTypeId").ToString
                mblnIsReceipt = dtRow.Item("IsReceipt").ToString
                mstrVoidreason = dtRow.Item("Voidreason").ToString
                mblnIsglobalpayment = dtRow.Item("Isglobalpayment").ToString
                'Sohail (08 Oct 2011) -- Start
                mintBasecurrencyid = CInt(dtRow.Item("basecurrencyid"))
                mdecBaseexchangerate = CDec(dtRow.Item("baseexchangerate"))
                mintPaidcurrencyid = CInt(dtRow.Item("paidcurrencyid"))
                mdecExpaidrate = CDec(dtRow.Item("expaidrate"))
                mdecExpaidamt = CDec(dtRow.Item("expaidamt"))
                'Sohail (08 Oct 2011) -- End
                mblnIsauthorized = False 'Sohail (02 Jul 2012)
                mstrAccountno = dtRow.Item("Accountno").ToString 'Sohail (21 Jul 2012)
                mintCountryunkid = CInt(dtRow.Item("countryunkid")) 'Sohail (03 Sep 2012)
                mblnIsapproved = CBool(dtRow.Item("isapproved"))  'Sohail (13 Feb 2013)
                'Sohail (22 Oct 2013) -- Start
                'TRA - ENHANCEMENT
                mintRoundingtypeid = CInt(dtRow.Item("roundingtypeid"))
                mintRoundingmultipleid = CInt(dtRow.Item("roundingmultipleid"))
                'Sohail (22 Oct 2013) -- End
                mdecRoundingAdjustment = CDec(dtRow.Item("roundingadjustment")) 'Sohail (21 Mar 2014)
                mstrRemarks = dtRow.Item("remarks").ToString 'Sohail (24 Dec 2014)
                mintPaymentDate_Periodunkid = dtRow.Item("paymentdate_periodunkid").ToString 'Sohail (07 May 2015)

                'Sohail (30 Oct 2018) -- Start
                'FDRC Enhancement - Global Payment performance issue on FDRC database in 75.1.
                intDefaultCostCenterUnkId = CInt(dtRow.Item("default_costcenterunkid"))
                'Sohail (30 Oct 2018) -- End

                'Sohail (07 May 2015) -- Start
                'Enhancement - New Loan Redesign with allow to change interest rate, tenure, topup, receipt.
                If intPrevPeriod <> mintPeriodunkid Then
                    objPeriod = New clscommom_period_Tran
                    'Nilay (10-Oct-2015) -- Start
                    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                    'objPeriod._Periodunkid = mintPeriodunkid
                    objPeriod._Periodunkid(strDatabaseName) = mintPeriodunkid
                    'Nilay (10-Oct-2015) -- End

                    If mintPaymentrefid = enPaymentRefId.PAYSLIP AndAlso mintPaymentTypeId = enPayTypeId.PAYMENT Then
                        'Nilay (25-Mar-2016) -- Start
                        'dsLoan = objLoan_Advance.Calculate_LoanBalanceInfo("List", objPeriod._End_Date.AddDays(1), "", 0, objPeriod._Start_Date, True)
                        'Sohail (30 Oct 2018) -- Start
                        'FDRC Enhancement - Global Payment performance issue on FDRC database in 75.1.
                        'dsLoan = objLoan_Advance.Calculate_LoanBalanceInfo(strDatabaseName, intUserunkid, intYearUnkid, intCompanyunkid, _
                        '                                                   xPeriodStart, xPeriodEnd, strUserAccessModeSetting, xOnlyApproved, _
                        '                                                   "List", objPeriod._End_Date.AddDays(1), "", 0, objPeriod._Start_Date, True)
                        dsLoan = objLoan_Advance.Calculate_LoanBalanceInfo(strDatabaseName, intUserunkid, intYearUnkid, intCompanyunkid, _
                                                                           xPeriodStart, xPeriodEnd, strUserAccessModeSetting, xOnlyApproved, _
                                                                           "List", objPeriod._End_Date.AddDays(1), strEmployeeIDs, 0, objPeriod._Start_Date, True, strFmtCurrency:=strFmtCurrency)
                        'Sohail (26 Oct 2020) - [strFmtCurrency]
                        'Sohail (30 Oct 2018) -- End
                        'Nilay (25-Mar-2016) -- End
                    End If
                End If
                'Sohail (07 May 2015) -- End


                'Sohail (30 Oct 2018) -- Start
                'FDRC Enhancement - Global Payment performance issue on FDRC database in 75.1.
                If mintPaymentrefid = enPaymentRefId.PAYSLIP Then
                    If IsPaymentDone(mintPaymentTypeId, mintPayreftranunkid, mintPaymentrefid, objDataOperation) = True Then
                        objEmp = New clsEmployee_Master

                        'Sohail (23 Dec 2019) -- Start
                        'NMB Issue # : Users not able to login in self service when doing any transaction on earning and deduction screen.
                        'objEmp._Employeeunkid(xPeriodEnd) = mintEmployeeunkid
                        objEmp._Employeeunkid(xPeriodEnd, objDataOperation) = mintEmployeeunkid
                        'Sohail (23 Dec 2019) -- End

                        mstrMessage = Language.getMessage(mstrModuleName, 30, "Sorry, Payment is already done for the employee") & " " & objEmp._Firstname & " " & objEmp._Surname
                        objDataOperation.ReleaseTransaction(False)
                        Exit Try
                    End If
                End If
                objDataOperation.ClearParameters()
                'Sohail (30 Oct 2018) -- End

                'S.SANDEEP [ 20 APRIL 2012 ] -- START
                'ENHANCEMENT : TRA CHANGES
                'objDataOperation.AddParameter("@voucherno", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoucherno.ToString)
                objDataOperation.AddParameter("@voucherno", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Guid.NewGuid.ToString)
                objDataOperation.AddParameter("@globalvocno", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrGlobalvocno)
                'S.SANDEEP [ 20 APRIL 2012 ] -- END
                objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPeriodunkid.ToString)
                objDataOperation.AddParameter("@paymentdate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtPaymentdate)
                objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeunkid.ToString)
                objDataOperation.AddParameter("@referenceid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPaymentrefid.ToString)
                objDataOperation.AddParameter("@paymentmode", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPaymentmodeid.ToString)
                objDataOperation.AddParameter("@branchunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintBranchunkid.ToString)
                objDataOperation.AddParameter("@chequeno", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrChequeno.ToString)
                objDataOperation.AddParameter("@paymentby", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPaymentbyid.ToString)
                objDataOperation.AddParameter("@percentage", SqlDbType.Float, eZeeDataType.MONEY_SIZE, mdblPercentage.ToString)
                objDataOperation.AddParameter("@amount", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecAmount.ToString) 'Sohail (11 May 2011)
                objDataOperation.AddParameter("@voucherref", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoucherref.ToString)
                objDataOperation.AddParameter("@referencetranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPayreftranunkid.ToString)
                objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
                objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
                objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
                If mdtVoiddatetime = Nothing Then
                    objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
                Else
                    objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime)
                End If
                objDataOperation.AddParameter("@paytypeid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPaymentTypeId.ToString)
                objDataOperation.AddParameter("@isreceipt", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsReceipt.ToString)
                objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)
                objDataOperation.AddParameter("@isglobalpayment", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsglobalpayment.ToString)
                'Sohail (08 Oct 2011) -- Start
                objDataOperation.AddParameter("@basecurrencyid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintBasecurrencyid.ToString)
                objDataOperation.AddParameter("@baseexchangerate", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecBaseexchangerate.ToString)
                objDataOperation.AddParameter("@paidcurrencyid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPaidcurrencyid.ToString)
                objDataOperation.AddParameter("@expaidrate", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecExpaidrate.ToString)
                objDataOperation.AddParameter("@expaidamt", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecExpaidamt.ToString)
                'Sohail (08 Oct 2011) -- End
                objDataOperation.AddParameter("@isauthorized", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsauthorized.ToString) 'Sohail (02 Jul 2012)
                objDataOperation.AddParameter("@accountno", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrAccountno.ToString) 'Sohail (21 Jul 2012)
                objDataOperation.AddParameter("@countryunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCountryunkid.ToString) 'Sohail (03 Sep 2012)
                objDataOperation.AddParameter("@isapproved", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsapproved.ToString) 'Sohail (13 Feb 2013)
                'Sohail (22 Oct 2013) -- Start
                'TRA - ENHANCEMENT
                objDataOperation.AddParameter("@roundingtypeid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintRoundingtypeid.ToString)
                objDataOperation.AddParameter("@roundingmultipleid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintRoundingmultipleid.ToString)
                'Sohail (22 Oct 2013) -- End
                objDataOperation.AddParameter("@roundingadjustment", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecRoundingAdjustment) 'Sohail (21 Mar 2014)
                objDataOperation.AddParameter("@remarks", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, mstrRemarks.ToString) 'Sohail (24 Dec 2014)
                objDataOperation.AddParameter("@paymentdate_periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPaymentDate_Periodunkid.ToString) 'Sohail (07 May 2015)


                'Sohail (30 Oct 2018) -- Start
                'FDRC Enhancement - Global Payment performance issue on FDRC database in 75.1.
                ''Sohail (17 Dec 2014) -- Start
                ''Enhancement - Provide Link for Payroll approval/Authorize Payslip Payment.
                'If mintPaymentrefid = enPaymentRefId.PAYSLIP Then
                '    If IsPaymentDone(mintPaymentTypeId, mintPayreftranunkid, mintPaymentrefid) = True Then
                '        objEmp = New clsEmployee_Master

                '        'S.SANDEEP [04 JUN 2015] -- START
                '        'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                '        'objEmp._Employeeunkid = mintEmployeeunkid
                '        objEmp._Employeeunkid(xPeriodEnd) = mintEmployeeunkid
                '        'S.SANDEEP [04 JUN 2015] -- END

                '        mstrMessage = Language.getMessage(mstrModuleName, 30, "Sorry, Payment is already done for the employee") & " " & objEmp._Firstname & " " & objEmp._Surname
                '        objDataOperation.ReleaseTransaction(False)
                '        Exit Try
                '    End If
                'End If
                ''Sohail (17 Dec 2014) -- End
                'Sohail (30 Oct 2018) -- End

                'S.SANDEEP [ 20 APRIL 2012 ] -- START
                'ENHANCEMENT : TRA CHANGES
                'strQ = "INSERT INTO prpayment_tran ( " & _
                '                          "  voucherno " & _
                '                          ", periodunkid " & _
                '                          ", paymentdate " & _
                '                          ", employeeunkid " & _
                '                          ", referenceid " & _
                '                          ", paymentmode " & _
                '                          ", branchunkid " & _
                '                          ", chequeno " & _
                '                          ", paymentby " & _
                '                          ", percentage " & _
                '                          ", amount " & _
                '                          ", voucherref " & _
                '                          ", referencetranunkid " & _
                '                          ", userunkid " & _
                '                          ", isvoid " & _
                '                          ", voiduserunkid " & _
                '                          ", voiddatetime " & _
                '                          ", paytypeid " & _
                '                          ", isreceipt " & _
                '                          ", voidreason" & _
                '                          ", isglobalpayment " & _
                '                          ", basecurrencyid " & _
                '                          ", baseexchangerate " & _
                '                          ", paidcurrencyid " & _
                '                          ", expaidrate " & _
                '                          ", expaidamt " & _
                '                        ") VALUES (" & _
                '                          "  @voucherno " & _
                '                          ", @periodunkid " & _
                '                          ", @paymentdate " & _
                '                          ", @employeeunkid " & _
                '                          ", @referenceid " & _
                '                          ", @paymentmode " & _
                '                          ", @branchunkid " & _
                '                          ", @chequeno " & _
                '                          ", @paymentby " & _
                '                          ", @percentage " & _
                '                          ", @amount " & _
                '                          ", @voucherref " & _
                '                          ", @referencetranunkid " & _
                '                          ", @userunkid " & _
                '                          ", @isvoid " & _
                '                          ", @voiduserunkid " & _
                '                          ", @voiddatetime " & _
                '                          ", @paytypeid " & _
                '                          ", @isreceipt " & _
                '                          ", @voidreason" & _
                '                          ", @isglobalpayment" & _
                '                          ", @basecurrencyid " & _
                '                          ", @baseexchangerate " & _
                '                          ", @paidcurrencyid " & _
                '                          ", @expaidrate " & _
                '                          ", @expaidamt " & _
                '                        "); SELECT @@identity" 'Sohail (08 Oct 2011)(basecurrencyid,baseexchangerate,paidcurrencyid,expaidrate,expaidamt added.)
                strQ = "INSERT INTO prpayment_tran ( " & _
                          "  voucherno " & _
                          ", periodunkid " & _
                          ", paymentdate " & _
                          ", employeeunkid " & _
                          ", referenceid " & _
                          ", paymentmode " & _
                          ", branchunkid " & _
                          ", chequeno " & _
                          ", paymentby " & _
                          ", percentage " & _
                          ", amount " & _
                          ", voucherref " & _
                          ", referencetranunkid " & _
                          ", userunkid " & _
                          ", isvoid " & _
                          ", voiduserunkid " & _
                          ", voiddatetime " & _
                          ", paytypeid " & _
                          ", isreceipt " & _
                          ", voidreason" & _
                          ", isglobalpayment " & _
                          ", basecurrencyid " & _
                          ", baseexchangerate " & _
                          ", paidcurrencyid " & _
                          ", expaidrate " & _
                          ", expaidamt " & _
                          ", globalvocno " & _
                          ", isauthorized " & _
                          ", accountno " & _
                          ", countryunkid " & _
                          ", isapproved " & _
                          ", roundingtypeid " & _
                          ", roundingmultipleid " & _
                          ", roundingadjustment " & _
                          ", remarks " & _
                          ", paymentdate_periodunkid " & _
                        ") VALUES (" & _
                          "  @voucherno " & _
                          ", @periodunkid " & _
                          ", @paymentdate " & _
                          ", @employeeunkid " & _
                          ", @referenceid " & _
                          ", @paymentmode " & _
                          ", @branchunkid " & _
                          ", @chequeno " & _
                          ", @paymentby " & _
                          ", @percentage " & _
                          ", @amount " & _
                          ", @voucherref " & _
                          ", @referencetranunkid " & _
                          ", @userunkid " & _
                          ", @isvoid " & _
                          ", @voiduserunkid " & _
                          ", @voiddatetime " & _
                          ", @paytypeid " & _
                          ", @isreceipt " & _
                          ", @voidreason" & _
                          ", @isglobalpayment" & _
                          ", @basecurrencyid " & _
                          ", @baseexchangerate " & _
                          ", @paidcurrencyid " & _
                          ", @expaidrate " & _
                          ", @expaidamt " & _
                          ", @globalvocno " & _
                          ", @isauthorized " & _
                          ", @accountno " & _
                          ", @countryunkid " & _
                          ", @isapproved " & _
                          ", @roundingtypeid " & _
                          ", @roundingmultipleid " & _
                          ", @roundingadjustment " & _
                          ", @remarks " & _
                          ", @paymentdate_periodunkid " & _
                        "); SELECT @@identity"
                '       'Sohail (07 May 2015) - [paymentdate_periodunkid]
                '       'Sohail (24 Dec 2014) - [remarks]
                '       'Sohail (22 Oct 2013) - [roundingtypeid, roundingmultipleid]
                '       'Sohail (21 Jul 2012) - [accountno]
                '           'Sohail (03 Sep 2012) - [countryunkid]
                '           'Sohail (13 Feb 2013) - [isapproved]
                'S.SANDEEP [ 20 APRIL 2012 ] -- END


                dsList = objDataOperation.ExecQuery(strQ, "List")

                If objDataOperation.ErrorMessage <> "" Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If

                mintPaymenttranunkid = dsList.Tables(0).Rows(0).Item(0)

                'Sandeep [ 17 DEC 2010 ] -- Start
                If mdtTranTable IsNot Nothing Then
                    Dim dtGRow() As DataRow = mdtTranTable.Select("employeeunkid = " & mintEmployeeunkid)
                    If dtGRow.Length > 0 Then
                        objCashDenome._PeriodId = mintPeriodunkid
                        'Sohail (18 May 2013) -- Start
                        'TRA - ENHANCEMENT
                        objCashDenome._YearUnkid = mintYearUnkid
                        objCashDenome._Userunkid = mintUserunkid

                        With objCashDenome
                            ._FormName = mstrFormName
                            ._LoginEmployeeunkid = mintLogEmployeeUnkid
                            ._ClientIP = mstrClientIP
                            ._HostName = mstrHostName
                            ._FromWeb = mblnIsWeb
                            ._AuditUserId = mintAuditUserId
                            ._CompanyUnkid = mintCompanyUnkid
                            ._AuditDate = mdtAuditDate
                        End With
                        'Sohail (18 May 2013) -- End
                        'Sohail (21 Aug 2015) -- Start
                        'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                        'If objCashDenome.Insert(mintEmployeeunkid, mintPaymenttranunkid, dtGRow(0)) = False Then
                        If objCashDenome.Insert(mintEmployeeunkid, mintPaymenttranunkid, dtGRow(0), intYearUnkid, intUserunkid, objDataOperation) = False Then
                            'Sohail (21 Aug 2015) -- End
                            objDataOperation.ReleaseTransaction(False)
                            Return False
                        End If
                    End If
                End If
                'Sandeep [ 17 DEC 2010 ] -- End 


                'Sohail (11 Nov 2010) -- Start
                'Anjan (11 Jun 2011)-Start
                'Issue : In main table entry was get inserted if there any error occurs at time of insertion in AT table.
                'Call InsertAuditTrailForPaymentTran(objDataOperation, 1)
                'Sohail (21 Aug 2015) -- Start
                'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                'If InsertAuditTrailForPaymentTran(objDataOperation, 1, True) = False Then
                If InsertAuditTrailForPaymentTran(objDataOperation, 1, dtCurrentDateAndTime, True) = False Then
                    'Sohail (21 Aug 2015) -- End
                    objDataOperation.ReleaseTransaction(False)
                    Return False
                End If
                'Anjan (11 Jun 2011)-End

                'Sohail (11 Nov 2010) -- End

                Select Case mintPaymentrefid
                    'Case enPaymentRefId.LOAN, enPaymentRefId.ADVANCE
                    '    Select Case mintPaymentTypeId
                    '        Case enPayTypeId.PAYMENT
                    '            objLoan_Advance._Loanadvancetranunkid = mintPayreftranunkid
                    '            If objLoan_Advance._Balance_Amount = 0 Then
                    '                If objLoan_Advance._Isloan = True Then
                    '                    objLoan_Advance._Balance_Amount = objLoan_Advance._Net_Amount
                    '                Else
                    '                    objLoan_Advance._Balance_Amount = objLoan_Advance._Advance_Amount
                    '                End If
                    '                blnFlag = objLoan_Advance.Update
                    '                If blnFlag = False Then objDataOperation.ReleaseTransaction(False)
                    '            End If
                    '        Case enPayTypeId.RECEIVED
                    '            If mblnIsFromStatus = True Then
                    '                Dim StrStatus() As String = mstrStatus_Data.Split("|")
                    '                objLoanStatusTran._Isvoid = StrStatus(0)   'IsVoid
                    '                objLoanStatusTran._Loanadvancetranunkid = StrStatus(1)    'LoanAdvanceTranUnkid
                    '                objLoanStatusTran._Remark = StrStatus(2) 'Remark
                    '                objLoanStatusTran._Settle_Amount = mdblAmount
                    '                objLoanStatusTran._Staus_Date = StrStatus(4)   'StatusDate
                    '                objLoanStatusTran._Voiddatetime = StrStatus(5) 'VoidDateTime
                    '                objLoanStatusTran._Voiduserunkid = StrStatus(6)   'VoidUserunkid
                    '                objLoanStatusTran._Statusunkid = StrStatus(7)  'SatausUnkid

                    '                blnFlag = objLoanStatusTran.Insert()
                    '                If blnFlag = False Then objDataOperation.ReleaseTransaction(False)
                    '            End If
                    '            objLoan_Advance._Loanadvancetranunkid = mintPayreftranunkid
                    '            objLoan_Advance._Balance_Amount = objLoan_Advance._Balance_Amount - mdblAmount

                    '            If objLoan_Advance._Balance_Amount = 0 Then
                    '                objLoan_Advance._LoanStatus = 4
                    '                objLoanStatusTran._Loanadvancetranunkid = objLoan_Advance._Loanadvancetranunkid
                    '                objLoanStatusTran._Isvoid = False
                    '                objLoanStatusTran._Voiddatetime = Nothing
                    '                objLoanStatusTran._Voiduserunkid = -1
                    '                objLoanStatusTran._Statusunkid = 4
                    '                objLoanStatusTran._Staus_Date = mdtPaymentdate
                    '                blnFlag = objLoanStatusTran.Insert()
                    '                If blnFlag = False Then objDataOperation.ReleaseTransaction(False)
                    '            End If

                    '            If mblnIsFromStatus = True Then
                    '                objLoan_Advance._LoanStatus = objLoanStatusTran._Statusunkid
                    '            End If
                    '            blnFlag = objLoan_Advance.Update
                    '            If blnFlag = False Then objDataOperation.ReleaseTransaction(False)
                    '    End Select
                    Case enPaymentRefId.PAYSLIP

                        'Sohail (15 Dec 2010) -- Start
                        If Not (mintPaymentmodeid = enPaymentMode.CASH OrElse mintPaymentmodeid = enPaymentMode.CASH_AND_CHEQUE) Then
                            'If mintPaymentmodeid <> 1 Then '1 = CASH 
                            'Sohail (15 Dec 2010) -- End
                            'Dim objEmpBank As New clsEmployeeBanks 'Sohail (30 Oct 2018)

                            'Sohail (25 Apr 2014) -- Start
                            'Enhancement - Employee Bank Details Period Wise.
                            'objEmpBank._Employeeunkid = mintEmployeeunkid
                            'dtTran = objEmpBank._DataTable
                            'Dim objPeriod As New clscommom_period_Tran
                            'Sohail (21 Aug 2015) -- Start
                            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                            'objPeriod._Periodunkid = mintPeriodunkid
                            'objPeriod._Periodunkid(strDatabaseName) = mintPeriodunkid 'Sohail (30 Oct 2018)
                            'Sohail (21 Aug 2015) -- End
                            'Sohail (21 Aug 2015) -- Start
                            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                            'Dim ds As DataSet = objEmpBank.GetList("Banks", mintEmployeeunkid.ToString, objPeriod._End_Date, , "end_date DESC, EmpName, priority", mintPeriodunkid)
                            'Dim ds As DataSet = objEmpBank.GetList(strDatabaseName, intUserunkid, intYearUnkid, intCompanyunkid, objPeriod._Start_Date, objPeriod._End_Date, strUserAccessModeSetting, True, blnIsIncludeInactiveEmp, "Banks", True, , mintEmployeeunkid.ToString, objPeriod._End_Date, "end_date DESC, EmpName, priority") 'Sohail (30 Oct 2018)
                            'Sohail (21 Aug 2015) -- End
                            'Sohail (30 Oct 2018) -- Start
                            'FDRC Enhancement - Global Payment performance issue on FDRC database in 75.1.
                            'dtTran = New DataView(ds.Tables(0)).ToTable
                            dtTran = New DataView(ds.Tables(0), "employeeunkid = " & mintEmployeeunkid & " ", "end_date DESC, EmpName, priority", DataViewRowState.CurrentRows).ToTable
                            'Sohail (30 Oct 2018) -- End
                            'Sohail (25 Apr 2014) -- End
                            'Sohail (21 Apr 2014) -- Start
                            'Enhancement - Salary Distribution by Amount and bank priority.
                            Dim lstPerc As Integer = (From p In dtTran Where CInt(p.Item("modeid")) = enPaymentBy.Percentage).Count()
                            If lstPerc > 0 Then
                                'Sohail (21 Apr 2014) -- End

                                For Each dsRow As DataRow In dtTran.Rows
                                    If CDec(dsRow.Item("percentage").ToString) > 0 Then
                                        Dim objEmpSalary As New clsEmpSalaryTran
                                        With objEmpSalary
                                            ._Paymenttranunkid = mintPaymenttranunkid
                                            ._Paymentdate = mdtPaymentdate
                                            ._Employeeunkid = mintEmployeeunkid
                                            ._Empbanktranid = CInt(dsRow.Item("empbanktranunkid").ToString)
                                            ._Amount = mdecAmount * CDec(dsRow.Item("percentage").ToString) / 100 'Sohail (11 May 2011)
                                            'Sohail (18 May 2013) -- Start
                                            'TRA - ENHANCEMENT
                                            '._Userunkid = User._Object._Userunkid
                                            ._Userunkid = mintUserunkid
                                            'Sohail (18 May 2013) -- End
                                            ._Isvoid = False
                                            ._Voiduserunkid = 0
                                            ._Voiddatetime = Nothing
                                            ._Voidreason = ""
                                            'Sohail (25 May 2012) -- Start
                                            'TRA - ENHANCEMENT
                                            ._Basecurrencyid = mintBasecurrencyid
                                            ._Baseexchangerate = mdecBaseexchangerate
                                            ._Paidcurrencyid = mintPaidcurrencyid
                                            ._Expaidrate = mdecExpaidrate
                                            ._Expaidamt = mdecExpaidamt * CDec(dsRow.Item("percentage").ToString) / 100
                                            'Sohail (25 May 2012) -- End

                                            ._FormName = mstrFormName
                                            ._LoginEmployeeUnkid = mintLogEmployeeUnkid
                                            ._ClientIP = mstrClientIP
                                            ._HostName = mstrHostName
                                            ._FromWeb = mblnIsWeb
                                            ._AuditUserId = mintAuditUserId
                                            ._CompanyUnkid = mintCompanyUnkid
                                            ._AuditDate = mdtAuditDate

                                            If .Insert() = False Then
                                                objDataOperation.ReleaseTransaction(False)
                                                Return False
                                            End If
                                            objEmpSalary = Nothing
                                        End With
                                    End If
                                Next
                                'Sohail (21 Apr 2014) -- Start
                                'Enhancement - Salary Distribution by Amount and bank priority.
                            Else
                                Dim lst As List(Of DataRow) = (From p In dtTran Where CInt(p.Item("modeid")) = enPaymentBy.Value).DefaultIfEmpty.ToList
                                Dim decPriorityTotal As Decimal = (From p In lst Select CDec(p.Item("Amount"))).DefaultIfEmpty.Sum

                                If decPriorityTotal < mdecAmount Then
                                    Dim intMinPriority As Integer = (From p In lst Select CInt(p.Item("priority"))).DefaultIfEmpty.Min
                                    Dim decOtherTotal As Decimal = (From p In lst Where CInt(p.Item("priority")) <> intMinPriority Select CDec(p.Item("Amount"))).DefaultIfEmpty.Sum
                                    Dim objEmpSalary As clsEmpSalaryTran

                                    For Each dsRow As DataRow In lst

                                        objEmpSalary = New clsEmpSalaryTran
                                        With objEmpSalary
                                            ._Paymenttranunkid = mintPaymenttranunkid
                                            ._Paymentdate = mdtPaymentdate
                                            ._Employeeunkid = mintEmployeeunkid
                                            ._Empbanktranid = CInt(dsRow.Item("empbanktranunkid").ToString)
                                            ._Userunkid = mintUserunkid
                                            ._Isvoid = False
                                            ._Voiduserunkid = 0
                                            ._Voiddatetime = Nothing
                                            ._Voidreason = ""
                                            ._Basecurrencyid = mintBasecurrencyid
                                            ._Baseexchangerate = mdecBaseexchangerate
                                            ._Paidcurrencyid = mintPaidcurrencyid
                                            ._Expaidrate = mdecExpaidrate


                                            '._WebFormName = mstrWebFormName
                                            '._HostName = mstrHostName
                                            '._WebIP = mstrClientIP

                                            ._FormName = mstrFormName
                                            ._LoginEmployeeUnkid = mintLogEmployeeUnkid
                                            ._ClientIP = mstrClientIP
                                            ._HostName = mstrHostName
                                            ._FromWeb = mblnIsWeb
                                            ._AuditUserId = mintAuditUserId
                                            ._CompanyUnkid = mintCompanyUnkid
                                            ._AuditDate = mdtAuditDate

                                            If CInt(dsRow.Item("priority")) = intMinPriority Then
                                                ._Amount = (mdecAmount - decOtherTotal)
                                                If mdecBaseexchangerate <> 0 Then
                                                    ._Expaidamt = (mdecAmount - decOtherTotal) * mdecExpaidrate / mdecBaseexchangerate
                                                Else
                                                    ._Expaidamt = (mdecAmount - decOtherTotal) * mdecExpaidrate
                                                End If
                                            Else
                                                ._Amount = CDec(dsRow.Item("Amount"))
                                                If mdecBaseexchangerate <> 0 Then
                                                    ._Expaidamt = CDec(dsRow.Item("Amount")) * mdecExpaidrate / mdecBaseexchangerate
                                                Else
                                                    ._Expaidamt = CDec(dsRow.Item("Amount")) * mdecExpaidrate
                                                End If
                                            End If

                                            If .Insert() = False Then
                                                objDataOperation.ReleaseTransaction(False)
                                                Return False
                                            End If
                                        End With

                                    Next
                                    objEmpSalary = Nothing
                                Else

                                    Dim objEmpSalary As clsEmpSalaryTran
                                    Dim decBalance As Decimal = mdecAmount

                                    For Each dsRow As DataRow In lst

                                        If decBalance <= 0 Then Exit For

                                        objEmpSalary = New clsEmpSalaryTran
                                        With objEmpSalary
                                            ._Paymenttranunkid = mintPaymenttranunkid
                                            ._Paymentdate = mdtPaymentdate
                                            ._Employeeunkid = mintEmployeeunkid
                                            ._Empbanktranid = CInt(dsRow.Item("empbanktranunkid").ToString)
                                            ._Userunkid = mintUserunkid
                                            ._Isvoid = False
                                            ._Voiduserunkid = 0
                                            ._Voiddatetime = Nothing
                                            ._Voidreason = ""
                                            ._Basecurrencyid = mintBasecurrencyid
                                            ._Baseexchangerate = mdecBaseexchangerate
                                            ._Paidcurrencyid = mintPaidcurrencyid
                                            ._Expaidrate = mdecExpaidrate

                                            ._FormName = mstrFormName
                                            ._LoginEmployeeUnkid = mintLogEmployeeUnkid
                                            ._ClientIP = mstrClientIP
                                            ._HostName = mstrHostName
                                            ._FromWeb = mblnIsWeb
                                            ._AuditUserId = mintAuditUserId
                                            ._CompanyUnkid = mintCompanyUnkid
                                            ._AuditDate = mdtAuditDate


                                            If decBalance > CDec(dsRow.Item("Amount")) Then
                                                ._Amount = CDec(dsRow.Item("Amount"))
                                                If mdecBaseexchangerate <> 0 Then
                                                    ._Expaidamt = CDec(dsRow.Item("Amount")) * mdecExpaidrate / mdecBaseexchangerate
                                                Else
                                                    ._Expaidamt = CDec(dsRow.Item("Amount")) * mdecExpaidrate
                                                End If
                                            Else
                                                ._Amount = decBalance
                                                If mdecBaseexchangerate <> 0 Then
                                                    ._Expaidamt = decBalance * mdecExpaidrate / mdecBaseexchangerate
                                                Else
                                                    ._Expaidamt = decBalance * mdecExpaidrate
                                                End If
                                            End If

                                            decBalance -= CDec(dsRow.Item("Amount"))

                                            If .Insert() = False Then
                                                objDataOperation.ReleaseTransaction(False)
                                                Return False
                                            End If
                                        End With
                                    Next
                                End If
                            End If
                            'Sohail (21 Apr 2014) -- End
                            objEmpBank = Nothing
                        End If

                        'Sohail (02 Jul 2012) -- Start
                        'TRA - ENHANCEMENT
                        Dim objPaymentAuthorize As New clsPayment_authorize_tran
                        With objPaymentAuthorize
                            ._Paymenttranunkid = mintPaymenttranunkid
                            'Sohail (21 Aug 2015) -- Start
                            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                            '._Authorize_Date = ConfigParameter._Object._CurrentDateAndTime
                            ._Authorize_Date = dtCurrentDateAndTime
                            'Sohail (21 Aug 2015) -- End
                            ._Isauthorized = False
                            'Sohail (18 May 2013) -- Start
                            'TRA - ENHANCEMENT
                            '._Userunkid = User._Object._Userunkid
                            ._Userunkid = mintUserunkid
                            'Sohail (18 May 2013) -- End
                            ._Isactive = True
                            ._Remarks = "" 'Sohail (27 Feb 2013)
                            '._WebFormName = mstrWebFormName
                            '._WebIP = mstrClientIP
                            '._HostName = mstrHostName

                            ._FormName = mstrFormName
                            ._LoginEmployeeUnkid = mintLogEmployeeUnkid
                            ._ClientIP = mstrClientIP
                            ._HostName = mstrHostName
                            ._FromWeb = mblnIsWeb
                            ._AuditUserId = mintAuditUserId
                            ._CompanyUnkid = mintCompanyUnkid
                            ._AuditDate = mdtAuditDate
                            If .Insert() = False Then
                                objDataOperation.ReleaseTransaction(False)
                                Return False
                            End If
                            objPaymentAuthorize = Nothing
                        End With
                        'Sohail (02 Jul 2012) -- End

                        'Sohail (13 Feb 2013) -- Start
                        'TRA - ENHANCEMENT
                        Dim objPaymentApproval As New clsPayment_approval_tran
                        With objPaymentApproval
                            ._Paymenttranunkid = mintPaymenttranunkid
                            ._Levelunkid = -1
                            ._Priority = -1
                            'Sohail (21 Aug 2015) -- Start
                            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                            '._Approval_Date = ConfigParameter._Object._CurrentDateAndTime
                            ._Approval_Date = dtCurrentDateAndTime
                            'Sohail (21 Aug 2015) -- End
                            ._Statusunkid = 0
                            'Sohail (18 May 2013) -- Start
                            'TRA - ENHANCEMENT
                            '._Userunkid = User._Object._Userunkid
                            ._Userunkid = mintUserunkid
                            'Sohail (18 May 2013) -- End
                            ._Isvoid = False
                            ._Voiduserunkid = -1
                            ._Voidreason = ""
                            ._Disapprovalreason = ""
                            ._Remarks = "" 'Sohail (27 Feb 2013)
                            ._FormName = mstrFormName
                            ._LoginEmployeeUnkid = mintLogEmployeeUnkid
                            ._ClientIP = mstrClientIP
                            ._HostName = mstrHostName
                            ._FromWeb = mblnIsWeb
                            ._AuditUserId = mintAuditUserId
                            ._CompanyUnkid = mintCompanyUnkid
                            ._AuditDate = mdtAuditDate

                            If .Insert() = False Then
                                objDataOperation.ReleaseTransaction(False)
                                Return False
                            End If
                            objPaymentApproval = Nothing
                        End With
                        'Sohail (13 Feb 2013) -- End

                        'Dim objpayrollProcess As New clsPayrollProcessTran 'Sohail (30 Oct 2018)
                        'Sohail (18 May 2013) -- Start
                        'TRA - ENHANCEMENT
                        'Dim dsProcess As DataSet = objpayrollProcess.GetList("List", mintEmployeeunkid, mintPeriodunkid, mintPayreftranunkid)
                        'Sohail (21 Aug 2015) -- Start
                        'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                        'Dim dsProcess As DataSet = objpayrollProcess.GetList("List", mintEmployeeunkid, mintPeriodunkid, mintPayreftranunkid, , mstrAccessLevelFilterString)
                        'Sohail (30 Oct 2018) -- Start
                        'FDRC Enhancement - Global Payment performance issue on FDRC database in 75.1.
                        'Dim dsProcess As DataSet = objpayrollProcess.GetList(strDatabaseName, intUserunkid, intYearUnkid, intCompanyunkid, xPeriodStart, xPeriodEnd, strUserAccessModeSetting, xOnlyApproved, blnIsIncludeInactiveEmp, strTableName, mintEmployeeunkid, mintPeriodunkid, mintPayreftranunkid, blnApplyUserAccessFilter, strFilter)
                        'Sohail (29 Nov 2018) -- Start
                        'BBL Issue - Error on Global Payment in 74.1.
                        'Dim dtProcess As DataTable = New DataView(dsProcess.Tables(0), "employeeunkid = " & mintEmployeeunkid & " ", "", DataViewRowState.CurrentRows).ToTable
                        Dim dtProcess As DataTable = New DataView(dsProcess.Tables(0), "employeeunkid = " & mintEmployeeunkid & " AND tnaleavetranunkid = " & mintPayreftranunkid & " ", "", DataViewRowState.CurrentRows).ToTable
                        'Sohail (29 Nov 2018) -- End
                        'Sohail (30 Oct 2018) -- End
                        'Sohail (21 Aug 2015) -- End
                        'Sohail (18 May 2013) -- End

                        'Sohail (30 Oct 2018) -- Start
                        'FDRC Enhancement - Global Payment performance issue on FDRC database in 75.1.
                        'If dsProcess IsNot Nothing Then
                        '    If dsProcess.Tables(0).Rows.Count > 0 Then
                        '        For Each drRow As DataRow In dsProcess.Tables(0).Rows
                        If dtProcess IsNot Nothing Then
                            If dtProcess.Rows.Count > 0 Then
                                For Each drRow As DataRow In dtProcess.Rows
                                    'Sohail (30 Oct 2018) -- End
                                    If CDec(drRow("loanadvancetranunkid")) > 0 Then

                                        '*** FOR UPDATE BALANCEs IN LOAN ADVANCE TRAN TABLE 
                                        'Sohail (07 May 2015) -- Start
                                        'Enhancement - New Loan Redesign with allow to change interest rate, tenure, topup, receipt.
                                        'strQ = " Update lnloan_advance_tran set " & _
                                        '       "  balance_amount = balance_amount - @amount " & _
                                        '       ", balance_amountPaidCurrency = balance_amountPaidCurrency - @amountPaidCurrency " & _
                                        '       " WHERE loanadvancetranunkid = @loanadvancetranunkid "
                                        'objDataOperation.ClearParameters()
                                        'objDataOperation.AddParameter("@amount", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, drRow("amount").ToString()) 'Sohail (11 May 2011)
                                        'objDataOperation.AddParameter("@loanadvancetranunkid", SqlDbType.Money, eZeeDataType.MONEY_SIZE, drRow("loanadvancetranunkid").ToString())
                                        'objDataOperation.AddParameter("@amountPaidCurrency", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, drRow("amountPaidCurrency").ToString()) 'Sohail (07 May 2015)
                                        'objDataOperation.ExecNonQuery(strQ)
                                        Dim dr_Row() As DataRow = dsLoan.Tables("List").Select("loanadvancetranunkid = " & CInt(drRow("loanadvancetranunkid")) & " ")
                                        If dr_Row.Length > 0 Then
                                            strQ = " Update lnloan_advance_tran set " & _
                                                   " balance_amount = balance_amount - @amount " & _
                                                       ", balance_amountPaidCurrency = balance_amountPaidCurrency - @amountPaidCurrency " & _
                                                   " WHERE loanadvancetranunkid = @loanadvancetranunkid "

                                            objDataOperation.ClearParameters()
                                            objDataOperation.AddParameter("@amount", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, CDec(dr_Row(0).Item("TotPrincipalAmount")))
                                            objDataOperation.AddParameter("@amountPaidCurrency", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, CDec(dr_Row(0).Item("TotPrincipalAmountPaidCurrency")))
                                            objDataOperation.AddParameter("@loanadvancetranunkid", SqlDbType.Money, eZeeDataType.MONEY_SIZE, drRow("loanadvancetranunkid").ToString())

                                            objDataOperation.ExecNonQuery(strQ)

                                            If objDataOperation.ErrorMessage <> "" Then
                                                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                                Throw exForce
                                            End If
                                        End If
                                        'Sohail (07 May 2015) -- End

                                        strQ = "Select balance_amount, balance_amountPaidCurrency from lnloan_advance_tran  " & _
                                               " WHERE loanadvancetranunkid = @loanadvancetranunkid "
                                        'Sohail (07 May 2015) - [PaidCurrency]

                                        objDataOperation.ClearParameters()
                                        objDataOperation.AddParameter("@loanadvancetranunkid", SqlDbType.Money, eZeeDataType.MONEY_SIZE, drRow("loanadvancetranunkid").ToString())
                                        Dim dsAmount As DataSet = objDataOperation.ExecQuery(strQ, "Balance")

                                        If objDataOperation.ErrorMessage <> "" Then
                                            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                            Throw exForce
                                        End If

                                        If dsAmount IsNot Nothing Then
                                            If dsAmount.Tables("Balance").Rows.Count > 0 Then
                                                'Sohail (07 May 2015) -- Start
                                                'Enhancement - New Loan Redesign with allow to change interest rate, tenure, topup, receipt.
                                                'If CDec(dsAmount.Tables("Balance").Rows(0)("balance_amount")) = 0 Then 'Sohail (11 May 2011)
                                                'Hemant (04 June 2019) -- Start
                                                'ISSUE/ENHANCEMENT : The employee loan still gets deducted even after loan tenure is over
                                                'If CDec(dsAmount.Tables("Balance").Rows(0)("balance_amountPaidCurrency")) < 0.01 Then
                                                'Hemant (08 Nov 2019) -- Start
                                                'ISSUE/ENHANCEMENT#4271(TUJIJENGE- TZ) - Not able to close Payroll period due to Loan deduction mismatch.
                                                'If CDec(dsAmount.Tables("Balance").Rows(0)("balance_amountPaidCurrency")) < 0.1 Then
                                                If CDec(dsAmount.Tables("Balance").Rows(0)("balance_amountPaidCurrency")) <= 1 Then
                                                    'Hemant (08 Nov 2019) -- End
                                                    'Hemant (04 June 2019) -- End
                                                    'Sohail (07 May 2015) -- End

                                                    strQ = " Update lnloan_advance_tran set " & _
                                                          " loan_statusunkid = 4 " & _
                                                          " WHERE loanadvancetranunkid = @loanadvancetranunkid "
                                                    objDataOperation.ClearParameters()
                                                    objDataOperation.AddParameter("@loanadvancetranunkid", SqlDbType.Money, eZeeDataType.MONEY_SIZE, drRow("loanadvancetranunkid").ToString())
                                                    objDataOperation.ExecNonQuery(strQ)

                                                    If objDataOperation.ErrorMessage <> "" Then
                                                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                                        Throw exForce
                                                    End If

                                                    objLoanStatusTran._Loanadvancetranunkid = CInt(drRow("loanadvancetranunkid"))
                                                    'Nilay (20-Sept-2016) -- Start
                                                    'Enhancement : Cancel feature for approved but not assigned loan application
                                                    'objLoanStatusTran._Staus_Date = Now
                                                    objLoanStatusTran._Staus_Date = dtCurrentDateAndTime
                                                    'Nilay (20-Sept-2016) -- End
                                                    objLoanStatusTran._Isvoid = False
                                                    objLoanStatusTran._Voiddatetime = Nothing
                                                    objLoanStatusTran._Voiduserunkid = -1
                                                    objLoanStatusTran._Statusunkid = 4
                                                    'Sohail (15 Dec 2015) -- Start
                                                    'Enhancement - New Loan Calculation type Reducing balance with Fixed Principal EMI and new interest calculation Monthly apart from Daily for KBC.
                                                    'objLoanStatusTran._Periodunkid = mintPeriodunkid 'Sohail (07 May 2015)
                                                    objLoanStatusTran._Periodunkid = mintPaymentDate_Periodunkid
                                                    'Sohail (15 Dec 2015) -- End

                                                    'Nilay (01-Apr-2016) -- Start
                                                    'ENHANCEMENT - Approval Process in Loan Other Operations
                                                    objLoanStatusTran._Userunkid = intUserunkid
                                                    'Nilay (01-Apr-2016) -- End

                                                    objLoanStatusTran._FormName = mstrFormName
                                                    objLoanStatusTran._LoginEmployeeunkid = mintLogEmployeeUnkid
                                                    objLoanStatusTran._ClientIP = mstrClientIP
                                                    objLoanStatusTran._HostName = mstrHostName
                                                    objLoanStatusTran._FromWeb = mblnIsWeb
                                                    objLoanStatusTran._AuditUserId = mintAuditUserId
                                                    objLoanStatusTran._CompanyUnkid = mintCompanyUnkid
                                                    objLoanStatusTran._AuditDate = mdtAuditDate

                                                    'Nilay (28-Aug-2015) -- Start
                                                    'In order to avoid Parallel Transaction problem in Web
                                                    'blnFlag = objLoanStatusTran.Insert()

                                                    'Sohail (15 Dec 2015) -- Start
                                                    'Enhancement - New Loan Calculation type Reducing balance with Fixed Principal EMI and new interest calculation Monthly apart from Daily for KBC.
                                                    'blnFlag = objLoanStatusTran.Insert(objDataOperation)

                                                    'Nilay (25-Mar-2016) -- Start
                                                    'blnFlag = objLoanStatusTran.Insert(strDatabaseName, intYearUnkid, objDataOperation, True)
                                                    'Sohail (30 Apr 2019) -- Start
                                                    'BBLMS Issue - Support Issue Id # 0003781 - 74.1 - Payslip global payment takes hours to process help needed urgently.
                                                    'blnFlag = objLoanStatusTran.Insert(strDatabaseName, intUserunkid, intYearUnkid, intCompanyunkid, _
                                                    '                                   xPeriodStart, xPeriodEnd, strUserAccessModeSetting, xOnlyApproved, _
                                                    '                                   objDataOperation, True)
                                                    blnFlag = objLoanStatusTran.Insert(strDatabaseName, intUserunkid, intYearUnkid, intCompanyunkid, _
                                                                                       xPeriodStart, xPeriodEnd, strUserAccessModeSetting, xOnlyApproved, _
                                                                                       objDataOperation, True, mintEmployeeunkid, strFmtCurrency)
                                                    'Sohail (26 Oct 2020) - [strFmtCurrency]
                                                    'Sohail (30 Apr 2019) -- End
                                                    'Nilay (25-Mar-2016) -- End

                                                    'Sohail (15 Dec 2015) -- End


                                                    'Nilay (28-Aug-2015) -- End
                                                    If blnFlag = False Then objDataOperation.ReleaseTransaction(False)

                                                End If
                                            End If
                                        End If


                                        If objDataOperation.ErrorMessage <> "" Then
                                            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                            Throw exForce
                                        End If

                                    ElseIf CDec(drRow("savingtranunkid")) > 0 Then

                                        ' FOR UPDATE BALANCE IN SAVING TRAN TABLE 

                                        'Sohail (21 Aug 2010) -- Start
                                        'Changes:Total_contribution field added to update
                                        'strQ = " Update svsaving_tran set " & _
                                        '      " balance_amount = balance_amount + @amount " & _
                                        '      " WHERE savingtranunkid = @savingtranunkid "
                                        strQ = " Update svsaving_tran set " & _
                                              " balance_amount = balance_amount + @amount " & _
                                              ",total_contribution = total_contribution + @amount " & _
                                              " WHERE savingtranunkid = @savingtranunkid "
                                        'Sohail (21 Aug 2010) -- End

                                        objDataOperation.ClearParameters()
                                        objDataOperation.AddParameter("@amount", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, drRow("amount").ToString()) 'Sohail (11 May 2011)
                                        objDataOperation.AddParameter("@savingtranunkid", SqlDbType.Money, eZeeDataType.MONEY_SIZE, drRow("savingtranunkid").ToString())
                                        objDataOperation.ExecNonQuery(strQ)


                                        If objDataOperation.ErrorMessage <> "" Then
                                            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                            Throw exForce
                                        End If

                                    End If



                                Next

                                'Sohail (21 Mar 2014) -- Start
                                'Enhancement - Set Limit for C/F Netpay balance and Payment JV Report
                                If Math.Abs(mdecRoundingAdjustment) > 0 Then
                                    objpayrollProcess = New clsPayrollProcessTran
                                    objpayrollProcess._Tnaleavetranunkid = mintPayreftranunkid
                                    objpayrollProcess._Employeeunkid = mintEmployeeunkid
                                    objpayrollProcess._Tranheadunkid = mintRoundingAdjustmentHeadID
                                    objpayrollProcess._Loanadvancetranunkid = -1
                                    objpayrollProcess._Savingtranunkid = -1
                                    objpayrollProcess._Amount = Math.Abs(mdecRoundingAdjustment)
                                    If mdecRoundingAdjustment < 0 Then
                                        objpayrollProcess._Add_deduct = 1
                                    Else
                                        objpayrollProcess._Add_deduct = 2
                                    End If
                                    objpayrollProcess._Currencyunkid = 0
                                    objpayrollProcess._Vendorunkid = 0
                                    objpayrollProcess._Broughtforward = False
                                    objpayrollProcess._Userunkid = mintUserunkid
                                    objpayrollProcess._Isvoid = False
                                    objpayrollProcess._Voiduserunkid = -1
                                    objpayrollProcess._Voiddatetime = Nothing
                                    objpayrollProcess._Voidreason = ""
                                    objpayrollProcess._MembershipTranUnkid = 0
                                    objpayrollProcess._ActivityUnkid = -1

                                    'Sohail (30 Oct 2018) -- Start
                                    'FDRC Enhancement - Global Payment performance issue on FDRC database in 75.1.
                                    'objEmp = New clsEmployee_Master

                                    ''S.SANDEEP [04 JUN 2015] -- START
                                    ''ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                                    ''objEmp._Employeeunkid = mintEmployeeunkid
                                    'objEmp._Employeeunkid(xPeriodEnd) = mintEmployeeunkid
                                    ''S.SANDEEP [04 JUN 2015] -- END

                                    'objpayrollProcess._Costcenterunkid = objEmp._Costcenterunkid
                                    objpayrollProcess._Costcenterunkid = intDefaultCostCenterUnkId
                                    'Sohail (30 Oct 2018) -- End
                                    With objpayrollProcess
                                        ._FormName = mstrFormName
                                        ._LoginEmployeeunkid = mintLogEmployeeUnkid
                                        ._ClientIP = mstrClientIP
                                        ._HostName = mstrHostName
                                        ._Isweb = mblnIsWeb
                                        ._AuditUserId = mintAuditUserId
                                        ._CompanyUnkid = mintCompanyUnkid
                                        ._AuditDate = mdtAuditDate
                                    End With

                                    'Sohail (29 Nov 2021) -- Start
                                    'Issue : : SQL date time overflow error on global payment screen when C/F Rounding above option is set.
                                    objpayrollProcess._AuditUserId = mintUserunkid
                                    objpayrollProcess._Loginemployeeunkid = mintLogEmployeeUnkid
                                    objpayrollProcess._AuditDate = dtCurrentDateAndTime
                                    If mstrWebFormName.Trim.Length <= 0 Then
                                        objpayrollProcess._Isweb = False
                                        objpayrollProcess._FormName = mstrForm_Name
                                    Else
                                        objpayrollProcess._Isweb = True
                                        objpayrollProcess._FormName = mstrWebFormName
                                    End If
                                    objpayrollProcess._ClientIP = mstrWebIP
                                    objpayrollProcess._HostName = mstrWebhostName
                                    'Sohail (29 Nov 2021) -- End

                                    'Sohail (02 Jan 2017) -- Start
                                    'TRA Enhancement - 64.1 - Performance enhancement in process payroll, and global void payment.
                                    'blnFlag = objpayrollProcess.Insert()
                                    blnFlag = objpayrollProcess.Insert(objDataOperation)
                                    'Sohail (02 Jan 2017) -- End
                                    If blnFlag = False Then
                                        objDataOperation.ReleaseTransaction(False)
                                        Return False
                                    End If
                                End If
                                'Sohail (21 Mar 2014) -- End

                                'FOR UPDATE BALANCE  IN TNA LEAVE TRAN TABLE
                                'Sohail (21 Mar 2014) -- Start
                                'Enhancement - Set Limit for C/F Netpay balance and Payment JV Report
                                'strQ = " Update prtnaleave_tran set " & _
                                '       " balanceamount = balanceamount - @amount " & _
                                '       " WHERE tnaleavetranunkid = @tnaleavetranunkid "
                                strQ = " Update prtnaleave_tran set " & _
                                       " balanceamount = balanceamount - @amount - @roundingadjustment " & _
                                       " WHERE tnaleavetranunkid = @tnaleavetranunkid "
                                'Sohail (21 Mar 2014) -- End

                                objDataOperation.ClearParameters()
                                objDataOperation.AddParameter("@amount", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecAmount) 'Sohail (11 May 2011)
                                objDataOperation.AddParameter("@tnaleavetranunkid", SqlDbType.Money, eZeeDataType.MONEY_SIZE, mintPayreftranunkid)
                                objDataOperation.AddParameter("@roundingadjustment", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecRoundingAdjustment) 'Sohail (21 Mar 2014)
                                objDataOperation.ExecNonQuery(strQ)


                                If objDataOperation.ErrorMessage <> "" Then
                                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                    Throw exForce
                                End If

                                'Sohail (16 Apr 2018) -- Start
                                'Voltamp Enhancement - Ref # 227 : Changes in "Salary Format WPS" report in 72.1.
                            Else
                                If Math.Abs(mdecRoundingAdjustment) > 0 Then
                                    objpayrollProcess = New clsPayrollProcessTran
                                    objpayrollProcess._Tnaleavetranunkid = mintPayreftranunkid
                                    objpayrollProcess._Employeeunkid = mintEmployeeunkid
                                    objpayrollProcess._Tranheadunkid = mintRoundingAdjustmentHeadID
                                    objpayrollProcess._Loanadvancetranunkid = -1
                                    objpayrollProcess._Savingtranunkid = -1
                                    objpayrollProcess._Amount = Math.Abs(mdecRoundingAdjustment)
                                    If mdecRoundingAdjustment < 0 Then
                                        objpayrollProcess._Add_deduct = 1
                                    Else
                                        objpayrollProcess._Add_deduct = 2
                                    End If
                                    objpayrollProcess._Currencyunkid = 0
                                    objpayrollProcess._Vendorunkid = 0
                                    objpayrollProcess._Broughtforward = False
                                    objpayrollProcess._Userunkid = mintUserunkid
                                    objpayrollProcess._Isvoid = False
                                    objpayrollProcess._Voiduserunkid = -1
                                    objpayrollProcess._Voiddatetime = Nothing
                                    objpayrollProcess._Voidreason = ""
                                    objpayrollProcess._MembershipTranUnkid = 0
                                    objpayrollProcess._ActivityUnkid = -1

                                    'Sohail (30 Oct 2018) -- Start
                                    'FDRC Enhancement - Global Payment performance issue on FDRC database in 75.1.
                                    'objEmp = New clsEmployee_Master

                                    'objEmp._Employeeunkid(xPeriodEnd) = mintEmployeeunkid

                                    'objpayrollProcess._Costcenterunkid = objEmp._Costcenterunkid
                                    objpayrollProcess._Costcenterunkid = intDefaultCostCenterUnkId
                                    'Sohail (30 Oct 2018) -- End
                                    With objpayrollProcess
                                        ._FormName = mstrFormName
                                        ._LoginEmployeeunkid = mintLogEmployeeUnkid
                                        ._ClientIP = mstrClientIP
                                        ._HostName = mstrHostName
                                        ._Isweb = mblnIsWeb
                                        ._AuditUserId = mintAuditUserId
                                        ._CompanyUnkid = mintCompanyUnkid
                                        ._AuditDate = mdtAuditDate
                                    End With

'Sohail (29 Nov 2021) -- Start
                                    'Issue : : SQL date time overflow error on global payment screen when C/F Rounding above option is set.
                                    objpayrollProcess._AuditUserId = mintUserunkid
                                    objpayrollProcess._Loginemployeeunkid = mintLogEmployeeUnkid
                                    objpayrollProcess._AuditDate = dtCurrentDateAndTime
                                    If mstrWebFormName.Trim.Length <= 0 Then
                                        objpayrollProcess._Isweb = False
                                        objpayrollProcess._FormName = mstrForm_Name
                                    Else
                                        objpayrollProcess._Isweb = True
                                        objpayrollProcess._FormName = mstrWebFormName
                                    End If
                                    objpayrollProcess._ClientIP = mstrWebIP
                                    objpayrollProcess._HostName = mstrWebhostName
                                    'Sohail (29 Nov 2021) -- End

                                    blnFlag = objpayrollProcess.Insert(objDataOperation)
                                    If blnFlag = False Then
                                        objDataOperation.ReleaseTransaction(False)
                                        Return False
                                    End If
                                End If

                                strQ = " Update prtnaleave_tran set " & _
                                       " balanceamount = balanceamount - @amount - @roundingadjustment " & _
                                       " WHERE tnaleavetranunkid = @tnaleavetranunkid "

                                objDataOperation.ClearParameters()
                                objDataOperation.AddParameter("@amount", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecAmount)
                                objDataOperation.AddParameter("@tnaleavetranunkid", SqlDbType.Money, eZeeDataType.MONEY_SIZE, mintPayreftranunkid)
                                objDataOperation.AddParameter("@roundingadjustment", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecRoundingAdjustment)
                                objDataOperation.ExecNonQuery(strQ)


                                If objDataOperation.ErrorMessage <> "" Then
                                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                    Throw exForce
                                End If

                                'Sohail (16 Apr 2018) -- End

                            End If
                        End If

                        'Case 4
                        '    objEmployeeSaving._Savingtranunkid = mintPayreftranunkid
                        '    objEmployeeSaving._Balance_Amount = objEmployeeSaving._Balance_Amount - mdblAmount

                        '    blnFlag = objEmployeeSaving.Update()
                        '    If blnFlag = False Then objDataOperation.ReleaseTransaction(False)


                End Select

                'Sohail (07 May 2015) -- Start
                'Enhancement - New Loan Redesign with allow to change interest rate, tenure, topup, receipt.
                intPrevPeriod = mintPeriodunkid
                'Sohail (07 May 2015) -- End

                'Sohail (30 Apr 2019) -- Start
                'BBLMS Issue - Support Issue Id # 0003781 - 74.1 - Payslip global payment takes hours to process help needed urgently.
                If bw IsNot Nothing Then
                    intCount = intCount + 1
                    bw.ReportProgress(intCount)
                End If
                'Sohail (30 Apr 2019) -- End

            Next

            'Sohail (23 May 2017) -- Start
            'Enhancement - 67.1 - Link budget with Payroll.
            If mdtActivityAdjustment IsNot Nothing AndAlso mdtActivityAdjustment.Rows.Count > 0 Then
                Dim objActAdj As clsFundActivityAdjustment_Tran
                For Each dtRow As DataRow In mdtActivityAdjustment.Rows
                    objActAdj = New clsFundActivityAdjustment_Tran

                    objActAdj._FundActivityunkid = CInt(dtRow.Item("fundactivityunkid"))
                    objActAdj._TransactionDate = eZeeDate.convertDate(dtRow.Item("transactiondate").ToString())
                    objActAdj._CurrentBalance = CDec(dtRow.Item("Current Balance"))
                    objActAdj._IncrDecrAmount = CDec(dtRow.Item("Actual Salary")) * -1
                    objActAdj._NewBalance = CDec(dtRow.Item("Current Balance")) - CDec(dtRow.Item("Actual Salary"))
                    objActAdj._Remark = dtRow.Item("remark").ToString()
                    objActAdj._Userunkid = mintUserunkid
                    objActAdj._Paymenttranunkid = 0
                    objActAdj._Globalvocunkid = iGlobalVocNoId
                    With objActAdj
                        ._FormName = mstrFormName
                        ._LoginEmployeeunkid = mintLogEmployeeUnkid
                        ._ClientIP = mstrClientIP
                        ._HostName = mstrHostName
                        ._FromWeb = mblnIsWeb
                        ._AuditUserId = mintAuditUserId
                        ._CompanyUnkid = mintCompanyUnkid
                        ._AuditDate = mdtAuditDate
                    End With
                    If objActAdj.Insert(dtCurrentDateAndTime, objDataOperation, True) = False Then
                        objDataOperation.ReleaseTransaction(False)
                        Throw New Exception(objActAdj._Message)
                        Return False
                    End If
                Next
            End If
            'Sohail (23 May 2017) -- End

            objDataOperation.ReleaseTransaction(True)

            'Sohail (15 Dec 2010) -- Start
            If intPayVocNoType = 1 Then
                'S.SANDEEP [ 20 APRIL 2012 ] -- START
                'ENHANCEMENT : TRA CHANGES
                'ConfigParameter._Object._NextPaymentVocNo = intNextPayVocNo + 1
                'ConfigParameter._Object.updateParam()
                'ConfigParameter._Object.Refresh()
                'Sohail (18 May 2013) -- Start
                'TRA - ENHANCEMENT
                'strQ = "UPDATE hrmsConfiguration..cfconfiguration SET key_value = CAST(key_value AS INT) + 1 WHERE companyunkid = '" & Company._Object._Companyunkid & "' AND key_name = 'NextPaymentVocNo'"
                strQ = "UPDATE hrmsConfiguration..cfconfiguration SET key_value = CAST(key_value AS INT) + 1 WHERE companyunkid = '" & mintCompanyunkid & "' AND key_name = 'NextPaymentVocNo'"
                'Sohail (18 May 2013) -- End

                objDataOperation.ExecNonQuery(strQ)

                If objDataOperation.ErrorMessage <> "" Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If
                'S.SANDEEP [ 20 APRIL 2012 ] -- END
            End If
            'Sohail (15 Dec 2010) -- End

            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: InsertAll; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
            objEmp = Nothing 'Sohail (21 Mar 2014)
        End Try
    End Function
    'Sohail (16 Oct 2010) -- End

    'Sohail (11 Nov 2010) -- Start
    'Anjan (11 Jun 2011)-Start
    'Issue : In main table entry was get inserted if there any error occurs at time of insertion in AT table.
    'Public Sub InsertAuditTrailForPaymentTran(ByVal objDataOperation As clsDataOperation, ByVal AuditType As Integer)



    'S.SANDEEP [ 20 APRIL 2012 ] -- START
    'ENHANCEMENT : TRA CHANGES
    'Public Function InsertAuditTrailForPaymentTran(ByVal objDataOperation As clsDataOperation, ByVal AuditType As Integer) As Boolean
    Public Function InsertAuditTrailForPaymentTran(ByVal objDataOperation As clsDataOperation, ByVal AuditType As Integer, ByVal dtCurrentDateAndTime As Date, Optional ByVal blnIsGlobal As Boolean = False) As Boolean
        'Sohail (21 Aug 2015) - [dtCurrentDateAndTime]
        'S.SANDEEP [ 20 APRIL 2012 ] -- END

        'Anjan (11 Jun 2011)-End
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try

            'If mstrClientIP.Trim = "" Then mstrClientIP = getIP()
            'If mstrHostName.Trim = "" Then mstrHostName = getHostName()


            'S.SANDEEP [ 19 JULY 2012 ] -- START
            'Enhancement : TRA Changes


            'strQ = "INSERT INTO atprpayment_tran ( " & _
            '          "  paymenttranunkid " & _
            '          ", voucherno " & _
            '          ", periodunkid " & _
            '          ", paymentdate " & _
            '          ", employeeunkid " & _
            '          ", referenceid " & _
            '          ", paymentmode " & _
            '          ", branchunkid " & _
            '          ", chequeno " & _
            '          ", paymentby " & _
            '          ", percentage " & _
            '          ", amount " & _
            '          ", voucherref " & _
            '          ", referencetranunkid " & _
            '          ", paytypeid " & _
            '          ", isreceipt " & _
            '          ", isglobalpayment " & _
            '          ", audittype " & _
            '          ", audituserunkid " & _
            '          ", auditdatetime " & _
            '          ", ip " & _
            '          ", machine_name" & _
            '          ", basecurrencyid " & _
            '          ", baseexchangerate " & _
            '          ", paidcurrencyid " & _
            '          ", expaidrate " & _
            '          ", expaidamt " & _
            '          ", globalvocno" & _
            '") VALUES (" & _
            '          "  @paymenttranunkid " & _
            '          ", @voucherno " & _
            '          ", @periodunkid " & _
            '          ", @paymentdate " & _
            '          ", @employeeunkid " & _
            '          ", @referenceid " & _
            '          ", @paymentmode " & _
            '          ", @branchunkid " & _
            '          ", @chequeno " & _
            '          ", @paymentby " & _
            '          ", @percentage " & _
            '          ", @amount " & _
            '          ", @voucherref " & _
            '          ", @referencetranunkid " & _
            '          ", @paytypeid " & _
            '          ", @isreceipt " & _
            '          ", @isglobalpayment " & _
            '          ", @audittype " & _
            '          ", @audituserunkid " & _
            '          ", @auditdatetime " & _
            '          ", @ip " & _
            '          ", @machine_name" & _
            '          ", @basecurrencyid " & _
            '          ", @baseexchangerate " & _
            '          ", @paidcurrencyid " & _
            '          ", @expaidrate " & _
            '          ", @expaidamt " & _
            '          ", @globalvocno" & _
            '"); SELECT @@identity"
            ''S.SANDEEP [ 20 APRIL 2012 ] -- END

            strQ = "INSERT INTO atprpayment_tran ( " & _
                      "  paymenttranunkid " & _
                      ", voucherno " & _
                      ", periodunkid " & _
                      ", paymentdate " & _
                      ", employeeunkid " & _
                      ", referenceid " & _
                      ", paymentmode " & _
                      ", branchunkid " & _
                      ", chequeno " & _
                      ", paymentby " & _
                      ", percentage " & _
                      ", amount " & _
                      ", voucherref " & _
                      ", referencetranunkid " & _
                      ", paytypeid " & _
                      ", isreceipt " & _
                      ", isglobalpayment " & _
                      ", audittype " & _
                      ", audituserunkid " & _
                      ", auditdatetime " & _
                      ", ip " & _
                      ", machine_name" & _
                      ", basecurrencyid " & _
                      ", baseexchangerate " & _
                      ", paidcurrencyid " & _
                      ", expaidrate " & _
                      ", expaidamt " & _
                      ", globalvocno" & _
                      ", isauthorized " & _
                      ", accountno " & _
                      ", countryunkid " & _
                      ", form_name " & _
                      " " & _
                      " " & _
                      " " & _
                      " " & _
                      " " & _
                      ", isweb " & _
                      ", isapproved" & _
                      ", roundingtypeid " & _
                      ", roundingmultipleid " & _
                      ", roundingadjustment " & _
                      ", remarks " & _
                      ", paymentdate_periodunkid " & _
            ") VALUES (" & _
                      "  @paymenttranunkid " & _
                      ", @voucherno " & _
                      ", @periodunkid " & _
                      ", @paymentdate " & _
                      ", @employeeunkid " & _
                      ", @referenceid " & _
                      ", @paymentmode " & _
                      ", @branchunkid " & _
                      ", @chequeno " & _
                      ", @paymentby " & _
                      ", @percentage " & _
                      ", @amount " & _
                      ", @voucherref " & _
                      ", @referencetranunkid " & _
                      ", @paytypeid " & _
                      ", @isreceipt " & _
                      ", @isglobalpayment " & _
                      ", @audittype " & _
                      ", @audituserunkid " & _
                      ", @auditdatetime " & _
                      ", @ip " & _
                      ", @machine_name" & _
                      ", @basecurrencyid " & _
                      ", @baseexchangerate " & _
                      ", @paidcurrencyid " & _
                      ", @expaidrate " & _
                      ", @expaidamt " & _
                      ", @globalvocno" & _
                      ", @isauthorized " & _
                      ", @accountno " & _
                      ", @countryunkid " & _
                      ", @form_name " & _
                    " " & _
                    " " & _
                    " " & _
                    " " & _
                    " " & _
                    ", @isweb " & _
                    ", @isapproved" & _
                      ", @roundingtypeid " & _
                      ", @roundingmultipleid " & _
                      ", @roundingadjustment " & _
                      ", @remarks " & _
                      ", @paymentdate_periodunkid " & _
                "); SELECT @@identity"
            '   'Sohail (07 May 2015) - [paymentdate_periodunkid]
            '   'Sohail (24 Dec 2014) - [remarks]
            '   'Sohail (21 Mar 2014) - [roundingadjustment]
            '   'Sohail (22 Oct 2013) - [roundingtypeid, roundingmultipleid]
            '   'Sohail (02 Jul 2012) - [isauthorized], 'Sohail (21 Jul 2012) - [accountno]
            '       'Sohail (03 Sep 2012) - [countryunkid]
            '       'Sohail (13 Feb 2013) - [isapproved]
            'S.SANDEEP [ 20 APRIL 2012 ] -- END


            'S.SANDEEP [ 19 JULY 2012 ] -- END


            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@paymenttranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPaymenttranunkid.ToString)
            If blnIsGlobal Then
                objDataOperation.AddParameter("@voucherno", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Guid.NewGuid.ToString)
            Else
                objDataOperation.AddParameter("@voucherno", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoucherno.ToString)
            End If
            objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPeriodunkid.ToString)
            objDataOperation.AddParameter("@paymentdate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtPaymentdate)
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeunkid.ToString)
            objDataOperation.AddParameter("@referenceid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPaymentrefid.ToString)
            objDataOperation.AddParameter("@paymentmode", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPaymentmodeid.ToString)
            'Hemant (07 Jan 2019) -- Start
            'ISSUE/ENHANCEMENT : {Audit Trails} in 76.1.
            'objDataOperation.AddParameter("@branchunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPaymentbyid.ToString)
            objDataOperation.AddParameter("@branchunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintBranchunkid.ToString)
            'Hemant (07 Jan 2019) -- End
            objDataOperation.AddParameter("@chequeno", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrChequeno.ToString)
            objDataOperation.AddParameter("@paymentby", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPaymentbyid.ToString)
            objDataOperation.AddParameter("@percentage", SqlDbType.Float, eZeeDataType.MONEY_SIZE, mdblPercentage.ToString)
            objDataOperation.AddParameter("@amount", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecAmount.ToString) 'Sohail (11 May 2011)
            objDataOperation.AddParameter("@voucherref", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoucherref.ToString)
            objDataOperation.AddParameter("@referencetranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPayreftranunkid.ToString)
            objDataOperation.AddParameter("@paytypeid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPaymentTypeId.ToString)
            objDataOperation.AddParameter("@isreceipt", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsReceipt.ToString)
            objDataOperation.AddParameter("@isglobalpayment", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsglobalpayment.ToString)
            objDataOperation.AddParameter("@audittype", SqlDbType.Int, eZeeDataType.INT_SIZE, AuditType.ToString)
            objDataOperation.AddParameter("@audituserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAuditUserId)
            objDataOperation.AddParameter("@auditdatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtAuditDate)
            objDataOperation.AddParameter("@ip", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrClientIP)
            objDataOperation.AddParameter("@machine_name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrHostName)
            objDataOperation.AddParameter("@basecurrencyid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintBasecurrencyid.ToString)
            objDataOperation.AddParameter("@baseexchangerate", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecBaseexchangerate.ToString)
            objDataOperation.AddParameter("@paidcurrencyid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPaidcurrencyid.ToString)
            objDataOperation.AddParameter("@expaidrate", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecExpaidrate.ToString)
            objDataOperation.AddParameter("@expaidamt", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecExpaidamt.ToString)

            objDataOperation.AddParameter("@globalvocno", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrGlobalvocno.ToString)
            objDataOperation.AddParameter("@isauthorized", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsauthorized.ToString) 'Sohail (02 Jul 2012)
            objDataOperation.AddParameter("@accountno", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrAccountno.ToString) 'Sohail (21 Jul 2012)
            objDataOperation.AddParameter("@countryunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCountryunkid.ToString) 'Sohail (03 Sep 2012)
            objDataOperation.AddParameter("@isapproved", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsapproved.ToString) 'Sohail (13 Feb 2013)
            objDataOperation.AddParameter("@roundingtypeid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintRoundingtypeid.ToString)
            objDataOperation.AddParameter("@roundingmultipleid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintRoundingmultipleid.ToString) 'Sohail (21 Mar 2014)
            objDataOperation.AddParameter("@roundingadjustment", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecRoundingAdjustment.ToString)
            objDataOperation.AddParameter("@remarks", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, mstrRemarks.ToString) 'Sohail (24 Dec 2014)
            objDataOperation.AddParameter("@paymentdate_periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPaymentDate_Periodunkid.ToString) 'Sohail (07 May 2015)
            objDataOperation.AddParameter("@form_name", SqlDbType.NVarChar, 500, mstrFormName)
            objDataOperation.AddParameter("@isweb", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsWeb)

            objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            'Anjan (11 Jun 2011)-Start
            'Issue : In main table entry was get inserted if there any error occurs at time of insertion in AT table.
            Return True
            'Anjan (11 Jun 2011)-End

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "InsertAuditTrailForPaymentTran", mstrModuleName)
            'Anjan (11 Jun 2011)-Start
            'Issue : In main table entry was get inserted if there any error occurs at time of insertion in AT table.
            Return False
            'Anjan (11 Jun 2011)-End
        Finally
            objDataOperation = Nothing
        End Try
    End Function
    'Sohail (11 Nov 2010) -- End
    'Sohail (15 Dec 2010) -- Start
    Public Function GetMaxPaymentVoucherNo() As Integer
        Dim StrQ As String = ""
        Dim objDataoperation As New clsDataOperation
        Dim dsList As DataSet
        Dim exForce As Exception

        Dim intMaxVocNo As Integer = 0
        Try
            StrQ = "SELECT  ISNULL(MAX(CASE ISNUMERIC(voucherno) WHEN 1 THEN CAST(voucherno AS INT) END), 0) AS MaxVocNo " & _
                   "FROM    prpayment_tran "

            dsList = objDataoperation.ExecQuery(StrQ, "List")

            If objDataoperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataoperation.ErrorNumber & ": " & objDataoperation.ErrorMessage)
                Throw exForce
            End If

            intMaxVocNo = CInt(dsList.Tables("List").Rows(0).Item("MaxVocNo").ToString)

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetMaxPaymentVoucherNo", mstrModuleName)
        Finally
            exForce = Nothing
            objDataoperation = Nothing
        End Try
        Return intMaxVocNo
    End Function
    'Sohail (15 Dec 2010) -- End

    'Sohail (25 Jan 2011) -- Start
    'Sohail (02 Jul 2012) -- Start
    'TRA - ENHANCEMENT
    'Public Function Get_List(ByVal strTableName As String, _
    '                       ByVal intRefrenceId As Integer, _
    '                       ByVal intPayTypeId As Integer, _
    '                       Optional ByVal intPaymentMode As Integer = 0, _
    '                       Optional ByVal intRefTranUnkID As Integer = 0, _
    '                       Optional ByVal intEmpID As Integer = 0) As DataSet

    '    Dim dsList As DataSet = Nothing
    '    Dim strQ As String = ""
    '    Dim exForce As Exception

    '    objDataOperation = New clsDataOperation

    '    Try
    '        'S.SANDEEP [ 20 APRIL 2012 ] -- START
    '        'ENHANCEMENT : TRA CHANGES
    '        'strQ = "SELECT " & _
    '        '           " CONVERT(CHAR(8),prpayment_tran.paymentdate,112) AS Date " & _
    '        '           ",ISNULL(prpayment_tran.voucherno,'') AS Voc " & _
    '        '           ",ISNULL(hremployee_master.firstname,'')+' '+ISNULL(hremployee_master.othername,'')+' '+ISNULL(hremployee_master.surname,'') AS EmpName " & _
    '        '           ",ISNULL(" & FinancialYear._Object._ConfigDatabaseName & "..cffinancial_year_tran.financialyear_name,'') AS YearName " & _
    '        '           ",ISNULL(cfcommon_period_tran.period_name,'') AS PeriodName " & _
    '        '           ",ISNULL(prpayment_tran.amount,0) AS Amount " & _
    '        '           ",hremployee_master.employeeunkid " & _
    '        '           "," & FinancialYear._Object._ConfigDatabaseName & "..cffinancial_year_tran.yearunkid " & _
    '        '           ",cfcommon_period_tran.periodunkid " & _
    '        '           ",prpayment_tran.paymenttranunkid " & _
    '        '           ",prpayment_tran.paymentby" & _
    '        '           ",isglobalpayment " & _
    '        '           ",prpayment_tran.referenceid " & _
    '        '           ",prpayment_tran.referencetranunkid " & _
    '        '           ",prpayment_tran.paytypeid " & _
    '        '           ",prpayment_tran.paymentmode " & _
    '        '           ",prpayment_tran.branchunkid " & _
    '        '           ",prpayment_tran.chequeno " & _
    '        '           ",prpayment_tran.isreceipt " & _
    '        '           ",prpayment_tran.isglobalpayment " & _
    '        '           ",prpayment_tran.basecurrencyid " & _
    '        '           ",prpayment_tran.baseexchangerate " & _
    '        '           ",prpayment_tran.paidcurrencyid " & _
    '        '           ",prpayment_tran.expaidrate " & _
    '        '           ",prpayment_tran.expaidamt " & _
    '        '        "FROM prpayment_tran " & _
    '        '           "LEFT JOIN hremployee_master ON prpayment_tran.employeeunkid = hremployee_master.employeeunkid " & _
    '        '           "LEFT JOIN cfcommon_period_tran ON prpayment_tran.periodunkid = cfcommon_period_tran.periodunkid " & _
    '        '           "LEFT JOIN " & FinancialYear._Object._ConfigDatabaseName & "..cffinancial_year_tran ON cfcommon_period_tran.yearunkid = cffinancial_year_tran.yearunkid " & _
    '        '        "WHERE prpayment_tran.referenceid = @referenceid " & _
    '        '        "AND prpayment_tran.paytypeid = @paytypeid " & _
    '        '        "AND ISNULL(prpayment_tran.isvoid,0) = 0 " 'Sohail (08 Oct 2011)(basecurrencyid,baseexchangerate,paidcurrencyid,expaidrate,expaidamt added.)

    'strQ = "SELECT " & _
    '           " CONVERT(CHAR(8),prpayment_tran.paymentdate,112) AS Date " & _
    '                   ",CASE WHEN ISNULL(isglobalpayment,0) = 0 THEN ISNULL(prpayment_tran.voucherno,'') " & _
    '                   "      WHEN ISNULL(isglobalpayment,0) = 1 THEN ISNULL(prpayment_tran.globalvocno,'') END AS Voc " & _
    '           ",ISNULL(hremployee_master.firstname,'')+' '+ISNULL(hremployee_master.othername,'')+' '+ISNULL(hremployee_master.surname,'') AS EmpName " & _
    '           ",ISNULL(" & FinancialYear._Object._ConfigDatabaseName & "..cffinancial_year_tran.financialyear_name,'') AS YearName " & _
    '           ",ISNULL(cfcommon_period_tran.period_name,'') AS PeriodName " & _
    '           ",ISNULL(prpayment_tran.amount,0) AS Amount " & _
    '           ",hremployee_master.employeeunkid " & _
    '           "," & FinancialYear._Object._ConfigDatabaseName & "..cffinancial_year_tran.yearunkid " & _
    '           ",cfcommon_period_tran.periodunkid " & _
    '           ",prpayment_tran.paymenttranunkid " & _
    '           ",prpayment_tran.paymentby" & _
    '           ",isglobalpayment " & _
    '           ",prpayment_tran.referenceid " & _
    '           ",prpayment_tran.referencetranunkid " & _
    '           ",prpayment_tran.paytypeid " & _
    '           ",prpayment_tran.paymentmode " & _
    '           ",prpayment_tran.branchunkid " & _
    '           ",prpayment_tran.chequeno " & _
    '           ",prpayment_tran.isreceipt " & _
    '           ",prpayment_tran.isglobalpayment " & _
    '           ",prpayment_tran.basecurrencyid " & _
    '           ",prpayment_tran.baseexchangerate " & _
    '           ",prpayment_tran.paidcurrencyid " & _
    '           ",prpayment_tran.expaidrate " & _
    '           ",prpayment_tran.expaidamt " & _
    '        "FROM prpayment_tran " & _
    '           "LEFT JOIN hremployee_master ON prpayment_tran.employeeunkid = hremployee_master.employeeunkid " & _
    '           "LEFT JOIN cfcommon_period_tran ON prpayment_tran.periodunkid = cfcommon_period_tran.periodunkid " & _
    '           "LEFT JOIN " & FinancialYear._Object._ConfigDatabaseName & "..cffinancial_year_tran ON cfcommon_period_tran.yearunkid = cffinancial_year_tran.yearunkid " & _
    '        "WHERE prpayment_tran.referenceid = @referenceid " & _
    '        "AND prpayment_tran.paytypeid = @paytypeid " & _
    '                "AND ISNULL(prpayment_tran.isvoid,0) = 0 "
    '        'S.SANDEEP [ 20 APRIL 2012 ] -- END




    '        objDataOperation.AddParameter("@referenceid", SqlDbType.Int, eZeeDataType.INT_SIZE, intRefrenceId.ToString)
    '        objDataOperation.AddParameter("@paytypeid", SqlDbType.Int, eZeeDataType.INT_SIZE, intPayTypeId.ToString)

    '        'Anjan (09 Aug 2011)-Start
    '        'Issue : For including setting of acitve and inactive employee.
    '        If ConfigParameter._Object._IsIncludeInactiveEmp = False Then
    '            '    strQ &= " AND ISNULL(hremployee_master.isactive,0) = 1 "
    '            'Sohail (06 Jan 2012) -- Start
    '            'TRA - ENHANCEMENT
    '            strQ &= " AND CONVERT(CHAR(8),hremployee_master.appointeddate,112) <= @enddate " & _
    '                       " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_from_date,112),@startdate) >= @startdate " & _
    '                       " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_to_date,112),@startdate) >= @startdate " & _
    '                       " AND ISNULL(CONVERT(CHAR(8),hremployee_master.empl_enddate,112), @startdate) >= @startdate " 'S.SANDEEP [ 15 MAY 2012 empl_enddate ] -- START -- END

    '            objDataOperation.AddParameter("@startdate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, ConfigParameter._Object._EmployeeAsOnDate)
    '            objDataOperation.AddParameter("@enddate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, ConfigParameter._Object._EmployeeAsOnDate)
    '            'Sohail (06 Jan 2012) -- End
    '        End If
    '        'Anjan (09 Aug 2011)-End 

    '        'Sohail (24 Jun 2011) -- Start
    '        'Issue : According to privilege that lower level user should not see superior level employees.

    '        'S.SANDEEP [ 04 FEB 2012 ] -- START
    '        'ENHANCEMENT : TRA CHANGES
    '        'If UserAccessLevel._AccessLevel.Length > 0 Then
    '        '    strQ &= " AND hremployee_master.jobunkid IN (" & UserAccessLevel._AccessLevel & ") "
    '        'End If

    '        'S.SANDEEP [ 01 JUNE 2012 ] -- START
    '        'ENHANCEMENT : TRA DISCIPLINE CHANGES
    '        'Select Case ConfigParameter._Object._UserAccessModeSetting
    '        '    Case enAllocation.BRANCH
    '        '        If UserAccessLevel._AccessLevel.Length > 0 Then
    '        '            strQ &= " AND  hremployee_master.stationunkid IN (" & UserAccessLevel._AccessLevel & ") "
    '        '        End If
    '        '    Case enAllocation.DEPARTMENT_GROUP
    '        '        If UserAccessLevel._AccessLevel.Length > 0 Then
    '        '            strQ &= " AND  hremployee_master.deptgroupunkid IN (" & UserAccessLevel._AccessLevel & ") "
    '        '        End If
    '        '    Case enAllocation.DEPARTMENT
    '        '        If UserAccessLevel._AccessLevel.Length > 0 Then
    '        '            strQ &= " AND  hremployee_master.departmentunkid IN (" & UserAccessLevel._AccessLevel & ") "
    '        '        End If
    '        '    Case enAllocation.SECTION_GROUP
    '        '        If UserAccessLevel._AccessLevel.Length > 0 Then
    '        '            strQ &= " AND  hremployee_master.sectiongroupunkid IN (" & UserAccessLevel._AccessLevel & ") "
    '        '        End If
    '        '    Case enAllocation.SECTION
    '        '        If UserAccessLevel._AccessLevel.Length > 0 Then
    '        '            strQ &= " AND  hremployee_master.sectionunkid IN (" & UserAccessLevel._AccessLevel & ") "
    '        '        End If
    '        '    Case enAllocation.UNIT_GROUP
    '        '        If UserAccessLevel._AccessLevel.Length > 0 Then
    '        '            strQ &= " AND  hremployee_master.unitgroupunkid IN (" & UserAccessLevel._AccessLevel & ") "
    '        '        End If
    '        '    Case enAllocation.UNIT
    '        '        If UserAccessLevel._AccessLevel.Length > 0 Then
    '        '            strQ &= " AND  hremployee_master.unitunkid IN (" & UserAccessLevel._AccessLevel & ") "
    '        '        End If
    '        '    Case enAllocation.TEAM
    '        '        If UserAccessLevel._AccessLevel.Length > 0 Then
    '        '            strQ &= " AND  hremployee_master.teamunkid IN (" & UserAccessLevel._AccessLevel & ") "
    '        '        End If
    '        '    Case enAllocation.JOB_GROUP
    '        '        If UserAccessLevel._AccessLevel.Length > 0 Then
    '        '            strQ &= " AND  hremployee_master.jobgroupunkid IN (" & UserAccessLevel._AccessLevel & ") "
    '        '        End If
    '        '    Case enAllocation.JOBS
    '        '        If UserAccessLevel._AccessLevel.Length > 0 Then
    '        '            strQ &= " AND hremployee_master.jobunkid IN (" & UserAccessLevel._AccessLevel & ") "
    '        '        End If
    '        'End Select
    '        strQ &= UserAccessLevel._AccessLevelFilterString
    '        'S.SANDEEP [ 01 JUNE 2012 ] -- END


    '        'S.SANDEEP [ 04 FEB 2012 ] -- END

    '        'Sohail (24 Jun 2011) -- End

    '        If intPaymentMode > 0 Then
    '            strQ &= "AND prpayment_tran.paymentmode = @paymentmode "
    '            objDataOperation.AddParameter("@paymentmode", SqlDbType.Int, eZeeDataType.INT_SIZE, intPaymentMode.ToString)
    '        End If

    '        If intRefTranUnkID > 0 Then
    '            strQ &= "AND prpayment_tran.referencetranunkid = @referencetranunkid "
    '            objDataOperation.AddParameter("@referencetranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intRefTranUnkID.ToString)
    '        End If

    '        If intEmpID > 0 Then
    '            strQ &= "AND prpayment_tran.employeeunkid = @employeeunkid "
    '            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intEmpID.ToString)
    '        End If

    '        dsList = objDataOperation.ExecQuery(strQ, strTableName)

    '        If objDataOperation.ErrorMessage <> "" Then
    '            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
    '            Throw exForce
    '        End If


    '    Catch ex As Exception
    '        Throw New Exception(ex.Message & "; Procedure Name: Get_List; Module Name: " & mstrModuleName)
    '    Finally
    '        exForce = Nothing
    '        If dsList IsNot Nothing Then dsList.Dispose()
    '        objDataOperation = Nothing
    '    End Try
    '    Return dsList
    'End Function
    'Sohail (02 Jul 2012) -- End
    'Sohail (25 Jan 2011) -- End

    'Sohail (12 Oct 2011) -- Start
    'Sohail (21 Aug 2015) -- Start
    'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
    'Public Function VoidAll(ByVal strUnkIdList As String, ByVal UserId As Integer, ByVal VoidDateTime As DateTime, ByVal strVoidReason As String, Optional ByVal bw As BackgroundWorker = Nothing) As Boolean
    '    'Sohail (22 May 2014) - [bw]

    '    Dim objDataOperation As clsDataOperation

    '    Dim dsList As DataSet = Nothing
    '    Dim strQ As String = ""
    '    Dim strQP As String = ""
    '    Dim strQS As String = ""
    '    Dim exForce As Exception
    '    Dim intUnkid As Integer
    '    Dim arrIDs() As String

    '    objDataOperation = New clsDataOperation
    '    objDataOperation.BindTransaction()

    '    Try
    '        arrIDs = strUnkIdList.Split(",")

    '        'Sohail (22 May 2014) -- Start
    '        'Enhancement - Show Progress on Global Void Payment.
    '        If bw IsNot Nothing Then
    '            bw.ReportProgress(0)
    '        End If
    '        Dim intCount As Integer = 0
    '        'Sohail (22 May 2014) -- End

    '        For i = 0 To arrIDs.Count - 1
    '            intUnkid = CInt(arrIDs(i))
    '            mblnIsvoid = True
    '            mintVoiduserunkid = UserId
    '            mdtVoiddatetime = VoidDateTime
    '            mstrVoidreason = strVoidReason

    '            'Sohail (12 May 2012) -- Start
    '            'TRA - ENHANCEMENT
    '            If Delete(objDataOperation, intUnkid) = False Then
    '                'If Delete(intUnkid) = False Then
    '                'Sohail (12 May 2012) -- End
    '                objDataOperation.ReleaseTransaction(False)
    '                Return False
    '            End If

    '            'Sohail (22 May 2014) -- Start
    '            'Enhancement - Show Progress on Global Void Payment.
    '            If bw IsNot Nothing Then
    '                intCount = intCount + 1
    '                bw.ReportProgress(intCount)
    '            End If
    '            'Sohail (22 May 2014) -- End
    '        Next

    '        objDataOperation.ReleaseTransaction(True)

    '        Return True
    '    Catch ex As Exception
    '        objDataOperation.ReleaseTransaction(False)
    '        Throw New Exception(ex.Message & "; Procedure Name: VoidAll; Module Name: " & mstrModuleName)
    '        Return False
    '    Finally
    '        exForce = Nothing
    '        If dsList IsNot Nothing Then dsList.Dispose()
    '        objDataOperation = Nothing
    '    End Try
    'End Function
    Public Function VoidAll(ByVal xDatabaseName As String _
                            , ByVal xYearUnkid As Integer _
                            , ByVal xCompanyUnkid As Integer _
                            , ByVal xPeriodStart As DateTime _
                            , ByVal xPeriodEnd As DateTime _
                            , ByVal xUserModeSetting As String _
                            , ByVal xOnlyApproved As Boolean _
                            , ByVal xIncludeIn_ActiveEmployee As Boolean _
                            , ByVal strUnkIdList As String _
                            , ByVal UserId As Integer _
                            , ByVal VoidDateTime As DateTime _
                            , ByVal strVoidReason As String _
                            , ByVal intPaymentrefid As Integer _
                            , ByVal intPaymentTypeId As Integer _
                            , Optional ByVal blnApplyUserAccessFilter As Boolean = True _
                            , Optional ByVal bw As BackgroundWorker = Nothing _
                            , Optional ByVal strFilerString As String = "" _
                            , Optional ByVal mdtActivityAdjustment As DataTable = Nothing _
                            , Optional ByVal strEmployeeIDs As String = "" _
                            , Optional ByVal strFmtCurrency As String = "" _
                            ) As Boolean
        'Sohail (26 Oct 2020) - [strFmtCurrency]
        'Sohail (30 Oct 2018) - [intPaymentrefid, intPaymentTypeId, strEmployeeIDs]
        'Sohail (23 May 2017) - [mdtActivityAdjustment]

        Dim objDataOperation As clsDataOperation

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim strQP As String = ""
        Dim strQS As String = ""
        Dim exForce As Exception
        Dim intUnkid As Integer
        Dim arrIDs() As String

        objDataOperation = New clsDataOperation
        objDataOperation.BindTransaction()

        Try
            arrIDs = strUnkIdList.Split(",")

            If bw IsNot Nothing Then
                bw.ReportProgress(0)
            End If
            Dim intCount As Integer = 0
            'Sohail (30 Oct 2018) -- Start
            'FDRC Enhancement - Global Payment performance issue on FDRC database in 75.1.
            Dim intPrevPeriodId As Integer = 0
            Dim dsLoan As DataSet = Nothing
            Dim dsProcess As DataSet = Nothing
            Dim dsPayment As DataSet = GetList(xDatabaseName, UserId, xYearUnkid, xCompanyUnkid, xPeriodStart, xPeriodEnd, xUserModeSetting, xOnlyApproved, xIncludeIn_ActiveEmployee, "Test", intPaymentrefid, 0, intPaymentTypeId, True, " prpayment_tran.paymenttranunkid IN (" & strUnkIdList & ") ", objDataOperation)
            'Sohail (30 Oct 2018) -- End

            For i = 0 To arrIDs.Count - 1
                intUnkid = CInt(arrIDs(i))
                mblnIsvoid = True
                mintVoiduserunkid = UserId
                mdtVoiddatetime = VoidDateTime
                mstrVoidreason = strVoidReason

                'Sohail (30 Oct 2018) -- Start
                'FDRC Enhancement - Global Payment performance issue on FDRC database in 75.1.
                'Sohail (21 Aug 2015) -- Start
                'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                'If Delete(objDataOperationintUnkid) = False Then
                'If Delete(xDatabaseName, UserId, xYearUnkid, xCompanyUnkid, xPeriodStart, xPeriodEnd, xUserModeSetting, xOnlyApproved, xIncludeIn_ActiveEmployee, objDataOperation, intUnkid, VoidDateTime, True) = False Then
                '    'Sohail (21 Aug 2015) -- End
                '    'If Delete(intUnkid) = False Then
                '    'Sohail (12 May 2012) -- End
                '    objDataOperation.ReleaseTransaction(False)
                '    Return False
                'End If
                objDataOperation.ClearParameters()

                If mstrWebIP.Trim = "" Then mstrWebIP = getIP()
                If mstrWebhostName.Trim = "" Then mstrWebhostName = getHostName()

                'Hemant (07 Jan 2019) -- Start
                'ISSUE/ENHANCEMENT : {Audit Trails} in 76.1.
                'If mstrWebFormName.Trim.Length <= 0 Then
                '    objDataOperation.AddParameter("@form_name", SqlDbType.NVarChar, 500, mstrForm_Name)
                '    objDataOperation.AddParameter("@isweb", SqlDbType.Bit, eZeeDataType.BIT_SIZE, False)

                'Else
                '    objDataOperation.AddParameter("@form_name", SqlDbType.NVarChar, 500, mstrWebFormName)
                '    objDataOperation.AddParameter("@isweb", SqlDbType.Bit, eZeeDataType.BIT_SIZE, True)
                '    objDataOperation.AddParameter("@module_name1", SqlDbType.NVarChar, 500, Language.getMessage(mstrModuleName, 2, "WEB"))
                'End If
                objDataOperation.AddParameter("@form_name", SqlDbType.NVarChar, 500, mstrFormName)
                objDataOperation.AddParameter("@isweb", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsWeb)


                'strQ = "INSERT INTO atprempsalary_tran " & _
                '                "( empsalarytranunkid, paymenttranunkid, paymentdate, employeeunkid, empbanktranid, amount, audittype, audituserunkid, auditdatetime, ip, machine_name, basecurrencyid, baseexchangerate, paidcurrencyid, expaidrate, expaidamt, form_name, module_name1, module_name2, module_name3, module_name4, module_name5, isweb ) " & _
                '        "SELECT " & _
                '             "empsalarytranunkid, paymenttranunkid, paymentdate, employeeunkid, empbanktranid, amount, 3, @voiduserunkid, @voiddatetime, '" & mstrWebIP & "', '" & mstrWebhostName & "', basecurrencyid, baseexchangerate, paidcurrencyid, expaidrate, expaidamt, @form_name, @module_name1, @module_name2, @module_name3, @module_name4, @module_name5, @isweb " & _
                '        "FROM prempsalary_tran " & _
                '             "WHERE ISNULL(isvoid, 0) = 0 " & _
                '             "AND paymenttranunkid = @paymenttranunkid "
                strQ = "INSERT INTO atprempsalary_tran " & _
                                "( empsalarytranunkid, paymenttranunkid, paymentdate, employeeunkid, empbanktranid, amount, audittype, audituserunkid, auditdatetime, ip, machine_name, basecurrencyid, baseexchangerate, paidcurrencyid, expaidrate, expaidamt, form_name, isweb ) " & _
                        "SELECT " & _
                             "empsalarytranunkid, paymenttranunkid, paymentdate, employeeunkid, empbanktranid, amount, 3, @voiduserunkid, @voiddatetime, '" & mstrWebIP & "', '" & mstrWebhostName & "', basecurrencyid, baseexchangerate, paidcurrencyid, expaidrate, expaidamt, @form_name,  @isweb " & _
                        "FROM prempsalary_tran " & _
                             "WHERE ISNULL(isvoid, 0) = 0 " & _
                             "AND paymenttranunkid = @paymenttranunkid "
                'Hemant (07 Jan 2019) -- End

                objDataOperation.AddParameter("@paymenttranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)
                objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
                objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
                If mdtVoiddatetime = Nothing Then
                    objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
                Else
                    objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime)
                End If

                objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)

                Call objDataOperation.ExecNonQuery(strQ)

                If objDataOperation.ErrorMessage <> "" Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If


                strQ = "UPDATE prempsalary_tran SET " & _
                          "  isvoid = @isvoid" & _
                          ", voiduserunkid = @voiduserunkid" & _
                          ", voiddatetime = @voiddatetime " & _
                          ", voidreason = @voidreason " & _
                "WHERE paymenttranunkid = @paymenttranunkid "



                Call objDataOperation.ExecNonQuery(strQ)

                If objDataOperation.ErrorMessage <> "" Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If

                strQ = "UPDATE prpayment_tran SET " & _
                          "  isvoid = @isvoid" & _
                          ", voiduserunkid = @voiduserunkid" & _
                          ", voiddatetime = @voiddatetime " & _
                          ", voidreason = @voidreason " & _
                "WHERE paymenttranunkid = @paymenttranunkid "

                objDataOperation.ClearParameters()
                objDataOperation.AddParameter("@paymenttranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)
                objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
                objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
                If mdtVoiddatetime = Nothing Then
                    objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
                Else
                    objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime)
                End If

                objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)

                Call objDataOperation.ExecNonQuery(strQ)

                If objDataOperation.ErrorMessage <> "" Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If


                'Hemant (07 Jan 2019) -- Start
                'ISSUE/ENHANCEMENT : {Audit Trails} in 76.1.

                'If mstrWebFormName.Trim.Length <= 0 Then
                '    objDataOperation.AddParameter("@form_name", SqlDbType.NVarChar, 500, mstrForm_Name)
                '    objDataOperation.AddParameter("@isweb", SqlDbType.Bit, eZeeDataType.BIT_SIZE, False)

                '    objDataOperation.AddParameter("@loginemployeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, -1) 'mintLogEmployeeUnkid 'Sohail (13 Feb 2013)
                'Else
                '    objDataOperation.AddParameter("@form_name", SqlDbType.NVarChar, 500, mstrWebFormName)
                '    objDataOperation.AddParameter("@isweb", SqlDbType.Bit, eZeeDataType.BIT_SIZE, True)
                '    objDataOperation.AddParameter("@module_name1", SqlDbType.NVarChar, 500, Language.getMessage(mstrModuleName, 2, "WEB"))
                '    objDataOperation.AddParameter("@loginemployeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLogEmployeeUnkid) 'Sohail (13 Feb 2013)
                'End If

                'strQ = "INSERT INTO atprpayment_authorize_tran " & _
                '                 "( paymentauthorizetranunkid, paymenttranunkid, authorize_date, isauthorized, audittype, audituserunkid, auditdatetime, ip, machine_name, form_name, module_name1, module_name2, module_name3, module_name4, module_name5, isweb, remarks ) " & _
                '         "SELECT " & _
                '              "paymentauthorizetranunkid, paymenttranunkid, authorize_date, isauthorized, 3, @voiduserunkid, @voiddatetime, '" & mstrWebIP & "', '" & mstrWebhostName & "', @form_name, @module_name1, @module_name2, @module_name3, @module_name4, @module_name5, @isweb, remarks " & _
                '         "FROM prpayment_authorize_tran " & _
                '         "WHERE isactive = 1 " & _
                '              "AND paymenttranunkid = @paymenttranunkid "
                objDataOperation.AddParameter("@form_name", SqlDbType.NVarChar, 500, mstrFormName)
                objDataOperation.AddParameter("@isweb", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsWeb)
                objDataOperation.AddParameter("@loginemployeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLogEmployeeUnkid)
                strQ = "INSERT INTO atprpayment_authorize_tran " & _
                                 "( paymentauthorizetranunkid, paymenttranunkid, authorize_date, isauthorized, audittype, audituserunkid, auditdatetime, ip, machine_name, form_name, isweb, remarks ) " & _
                         "SELECT " & _
                              "paymentauthorizetranunkid, paymenttranunkid, authorize_date, isauthorized, 3, @voiduserunkid, @voiddatetime, '" & mstrWebIP & "', '" & mstrWebhostName & "', @form_name, @isweb, remarks " & _
                         "FROM prpayment_authorize_tran " & _
                         "WHERE isactive = 1 " & _
                              "AND paymenttranunkid = @paymenttranunkid "
                'Hemant (07 Jan 2019) -- End
                Call objDataOperation.ExecNonQuery(strQ)

                If objDataOperation.ErrorMessage <> "" Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If

                'TRA - ENHANCEMENT - *** Delete entries from prpayment_authorize_tran Table
                strQ = "UPDATE  prpayment_authorize_tran " & _
                        "SET     isactive = 0 " & _
                              ", voiddatetime = @voiddatetime " & _
                              ", voiduserunkid = @voiduserunkid " & _
                              ", voidreason = @voidreason " & _
                        "WHERE   paymenttranunkid = @paymenttranunkid "

                Call objDataOperation.ExecNonQuery(strQ)

                If objDataOperation.ErrorMessage <> "" Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If


                'Hemant (07 Jan 2019) -- Start
                'ISSUE/ENHANCEMENT : {Audit Trails} in 76.1.
                'strQ = "INSERT INTO atprpayment_approval_tran " & _
                '                               "( paymentapprovaltranunkid, paymenttranunkid, levelunkid, priority, approval_date, statusunkid, disapprovalreason, audittype, audituserunkid, auditdatetime, ip, machine_name, form_name, module_name1, module_name2, module_name3, module_name4, module_name5, isweb, loginemployeeunkid, remarks ) " & _
                '                       "SELECT " & _
                '                            "paymentapprovaltranunkid, paymenttranunkid, levelunkid, priority, approval_date, statusunkid, disapprovalreason, 3, @voiduserunkid, @voiddatetime, '" & mstrWebIP & "', '" & mstrWebhostName & "', @form_name, @module_name1, @module_name2, @module_name3, @module_name4, @module_name5, @isweb, @loginemployeeunkid, remarks  " & _
                '                       "FROM     prpayment_approval_tran " & _
                '                       "WHERE ISNULL(isvoid, 0) = 0 " & _
                '                       "AND paymenttranunkid = @paymenttranunkid "
                strQ = "INSERT INTO atprpayment_approval_tran " & _
                               "( paymentapprovaltranunkid, paymenttranunkid, levelunkid, priority, approval_date, statusunkid, disapprovalreason, audittype, audituserunkid, auditdatetime, ip, machine_name, form_name, isweb, loginemployeeunkid, remarks ) " & _
                        "SELECT " & _
                            "paymentapprovaltranunkid, paymenttranunkid, levelunkid, priority, approval_date, statusunkid, disapprovalreason, 3, @voiduserunkid, @voiddatetime, '" & mstrWebIP & "', '" & mstrWebhostName & "', @form_name, @isweb, @loginemployeeunkid, remarks  " & _
                        "FROM     prpayment_approval_tran " & _
                        "WHERE ISNULL(isvoid, 0) = 0 " & _
                        "AND paymenttranunkid = @paymenttranunkid "
                'Hemant (07 Jan 2019) -- End


                Call objDataOperation.ExecNonQuery(strQ)

                If objDataOperation.ErrorMessage <> "" Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If

                strQ = "UPDATE  prpayment_approval_tran " & _
                        "SET     isvoid = 1 " & _
                              ", voiddatetime = @voiddatetime " & _
                              ", voiduserunkid = @voiduserunkid " & _
                              ", voidreason = @voidreason " & _
                        "WHERE   paymenttranunkid = @paymenttranunkid " & _
                        "AND ISNULL(isvoid, 0) = 0 "

                Call objDataOperation.ExecNonQuery(strQ)

                If objDataOperation.ErrorMessage <> "" Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If
                'Hemant (26 Feb 2019) -- Start
                'ISSUE/ENHANCEMENT : {Audit Trails}
                With objCashDenome
                    ._FormName = mstrFormName
                    ._LoginEmployeeunkid = mintLogEmployeeUnkid
                    ._ClientIP = mstrClientIP
                    ._HostName = mstrHostName
                    ._FromWeb = mblnIsWeb
                    ._AuditUserId = mintAuditUserId
                    ._CompanyUnkid = mintCompanyunkid
                    ._AuditDate = mdtAuditDate
                End With
                'Hemant (26 Feb 2019) -- End
                If objCashDenome.VoidCashDenomination(intUnkid, mblnIsvoid, mintVoiduserunkid, mdtVoiddatetime, mstrVoidreason, objDataOperation) = False Then
                    objDataOperation.ReleaseTransaction(False)
                    Return False
                End If

                'Dim intVoidUserId As Integer = mintVoiduserunkid
                'Dim strVoidReason As String = mstrVoidreason

                '_DataOperation = objDataOperation
                'Call GetData(objDataOperation, intUnkid)
                Dim dr() As DataRow = dsPayment.Tables(0).Select("paymenttranunkid = " & intUnkid & " ")
                If dr.Length > 0 Then
                    Call GetData(dr(0))

                    mblnIsvoid = True
                    mintVoiduserunkid = UserId
                    mdtVoiddatetime = VoidDateTime
                    mstrVoidreason = strVoidReason
                End If

                If intPrevPeriodId <> mintPeriodunkid Then
                    Dim objPeriod As New clscommom_period_Tran
                    objPeriod._Periodunkid(xDatabaseName) = mintPeriodunkid
                    dsLoan = objLoan_Advance.Calculate_LoanBalanceInfo(xDatabaseName, UserId, xYearUnkid, xCompanyUnkid, _
                                                                       xPeriodStart, xPeriodEnd, xUserModeSetting, xOnlyApproved, _
                                                                       "List", objPeriod._End_Date.AddDays(1), strEmployeeIDs, 0, objPeriod._Start_Date, True, , , True, , , , True, strFmtCurrency:=strFmtCurrency)
                    'Sohail (26 Oct 2020) - [strFmtCurrency]

                    Dim objpayrollProcess As New clsPayrollProcessTran
                    dsProcess = objpayrollProcess.GetList(xDatabaseName, UserId, xYearUnkid, xCompanyUnkid, xPeriodStart, xPeriodEnd, xUserModeSetting, xOnlyApproved, xIncludeIn_ActiveEmployee, "List", 0, mintPeriodunkid, 0, blnApplyUserAccessFilter, " (prpayrollprocess_tran.loanadvancetranunkid > 0 OR prpayrollprocess_tran.savingtranunkid > 0) ", strEmployeeIDs)

                End If

                Dim intLoanBalanceTranUnkId As Integer = -1
                If mintPaymentrefid = CInt(enPaymentRefId.LOAN) AndAlso mintPaymentTypeId = CInt(enPayTypeId.RECEIVED) Then
                    Dim objLoan As New clsLoan_Advance
                    Dim ds As DataSet = objLoan.GetLastLoanBalance("List", mintPayreftranunkid)
                    If Not (ds.Tables("List").Rows.Count > 0 AndAlso CInt(ds.Tables("List").Rows(0).Item("paymenttranunkid")) = intUnkid) Then
                        Dim strMsg As String = " Loan"
                        If ds.Tables("List").Rows.Count > 0 Then
                            If CInt(ds.Tables("List").Rows(0).Item("payrollprocesstranunkid")) > 0 Then
                                strMsg = " Process Payroll"
                            ElseIf CInt(ds.Tables("List").Rows(0).Item("paymenttranunkid")) > 0 Then
                                strMsg = " Payment List"
                            ElseIf CInt(ds.Tables("List").Rows(0).Item("lninteresttranunkid")) > 0 Then
                                strMsg = " Loan Interest Rate"
                            ElseIf CInt(ds.Tables("List").Rows(0).Item("lnemitranunkid")) > 0 Then
                                strMsg = " Loan EMI"
                            ElseIf CInt(ds.Tables("List").Rows(0).Item("lntopuptranunkid")) > 0 Then
                                strMsg = " Loan Topup"
                            End If
                        End If
                        mstrMessage = Language.getMessage(mstrModuleName, 32, "Sorry, You cannot delete this transaction. Please delete last transaction from the screen of") & strMsg
                        objDataOperation.ReleaseTransaction(False)
                        Return False
                    Else
                        intLoanBalanceTranUnkId = CInt(ds.Tables("List").Rows(0).Item("loanbalancetranunkid"))
                    End If
                End If

                If InsertAuditTrailForPaymentTran(objDataOperation, 3, VoidDateTime) = False Then
                    objDataOperation.ReleaseTransaction(False)
                    Return False
                End If

                Dim blnFlag As Boolean = False
                Select Case mintPaymentrefid
                    Case enPaymentRefId.LOAN, enPaymentRefId.ADVANCE
                        Select Case mintPaymentTypeId
                            Case enPayTypeId.PAYMENT
                                objLoan_Advance._Loanadvancetranunkid = mintPayreftranunkid
                                '<TODO Please Put One Check Point of Payment......!! >

                                Dim dsDataList As New DataSet
                                dsDataList = GetList(xDatabaseName, UserId, xYearUnkid, xCompanyUnkid, xPeriodStart, xPeriodEnd, xUserModeSetting, xOnlyApproved, xIncludeIn_ActiveEmployee, "Test", mintPaymentrefid, mintPayreftranunkid, mintPaymentTypeId, True, , objDataOperation)

                                If dsDataList.Tables(0).Rows.Count > 0 Then
                                Else
                                    objLoan_Advance._Balance_Amount = 0
                                    With objLoan_Advance
                                        ._FormName = mstrFormName
                                        ._LoginEmployeeUnkid = mintLogEmployeeUnkid
                                        ._ClientIP = mstrClientIP
                                        ._HostName = mstrHostName
                                        ._FromWeb = mblnIsWeb
                                        ._AuditUserId = mintAuditUserId
                                        ._CompanyUnkid = mintCompanyunkid
                                        ._AuditDate = mdtAuditDate
                                    End With

                                    blnFlag = objLoan_Advance.Update(objDataOperation)

                                    If blnFlag = False Then objDataOperation.ReleaseTransaction(False)
                                End If
                            Case enPayTypeId.RECEIVED
                                If mintPaymentrefid = enPaymentRefId.LOAN Then
                                    strQ = "UPDATE lnloan_balance_tran SET isvoid = 1, voiduserunkid = " & UserId & ", voiddatetime = GETDATE(), voidreason = '" & strVoidReason & "' WHERE lnloan_balance_tran.loanbalancetranunkid= " & intLoanBalanceTranUnkId & " "
                                    objDataOperation.ExecNonQuery(strQ)
                                    If objDataOperation.ErrorMessage <> "" Then
                                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                        Throw exForce
                                    End If
                                End If

                                Dim dtLoan As DataTable = New DataView(dsLoan.Tables(0), "loanadvancetranunkid = " & mintPayreftranunkid & " ", "", DataViewRowState.CurrentRows).ToTable

                                objLoan_Advance._Loanadvancetranunkid = mintPayreftranunkid
                                objLoan_Advance._Balance_Amount = objLoan_Advance._Balance_Amount + mdecAmount
                                objLoan_Advance._Balance_AmountPaidCurrency = objLoan_Advance._Balance_AmountPaidCurrency + mdecExpaidamt
                                If dtLoan.Rows.Count > 0 Then
                                    objLoan_Advance._Balance_Amount = objLoan_Advance._Balance_Amount + CDec(dtLoan.Rows(0).Item("TotPrincipalAmount"))
                                    objLoan_Advance._Balance_AmountPaidCurrency = objLoan_Advance._Balance_AmountPaidCurrency + CDec(dtLoan.Rows(0).Item("TotPrincipalAmountPaidCurrency"))
                                End If

                                With objLoan_Advance
                                    ._FormName = mstrFormName
                                    ._LoginEmployeeUnkid = mintLogEmployeeUnkid
                                    ._ClientIP = mstrClientIP
                                    ._HostName = mstrHostName
                                    ._FromWeb = mblnIsWeb
                                    ._AuditUserId = mintAuditUserId
                                    ._CompanyUnkid = mintCompanyunkid
                                    ._AuditDate = mdtAuditDate
                                End With

                                blnFlag = objLoan_Advance.Update(objDataOperation)

                                If blnFlag = False Then objDataOperation.ReleaseTransaction(False)
                        End Select

                    Case enPaymentRefId.PAYSLIP

                        If mintPaymentTypeId = enPayTypeId.PAYMENT Then

                            'Dim objPeriod As New clscommom_period_Tran
                            'objPeriod._Periodunkid(xDatabaseName) = mintPeriodunkid
                            'Dim dsLoan As DataSet = Nothing
                            'dsLoan = objLoan_Advance.Calculate_LoanBalanceInfo(xDatabaseName, UserId, xYearUnkid, xCompanyUnkid, _
                            '                                                   xPeriodStart, xPeriodEnd, xUserModeSetting, xOnlyApproved, _
                            '                                                   "List", objPeriod._End_Date.AddDays(1), mintEmployeeunkid.ToString, 0, objPeriod._Start_Date, True, , , True, , , , True)
                            'Sohail (02 Jan 2017) -- End
                            'Nilay (25-Mar-2016) -- End
                            'Sohail (07 May 2015) -- End
                            Dim dtLoan As DataTable = New DataView(dsLoan.Tables(0), "employeeunkid = " & mintEmployeeunkid & " ", "", DataViewRowState.CurrentRows).ToTable

                            'Dim objpayrollProcess As New clsPayrollProcessTran
                            'Dim dsProcess As DataSet = objpayrollProcess.GetList(xDatabaseName, UserId, xYearUnkid, xCompanyUnkid, xPeriodStart, xPeriodEnd, xUserModeSetting, xOnlyApproved, xIncludeIn_ActiveEmployee, "List", mintEmployeeunkid, mintPeriodunkid, mintPayreftranunkid, blnApplyUserAccessFilter, " (prpayrollprocess_tran.loanadvancetranunkid > 0 OR prpayrollprocess_tran.savingtranunkid > 0) ")
                            Dim dtProcess As DataTable = New DataView(dsProcess.Tables(0), "employeeunkid = " & mintEmployeeunkid & " AND tnaleavetranunkid = " & mintPayreftranunkid & " ", "", DataViewRowState.CurrentRows).ToTable

                            If dtProcess IsNot Nothing Then
                                If dtProcess.Rows.Count > 0 Then
                                    For Each drRow As DataRow In dtProcess.Rows
                                        If CDec(drRow("loanadvancetranunkid")) > 0 Then

                                            Dim dr_Row() As DataRow = dtLoan.Select("loanadvancetranunkid = " & CInt(drRow("loanadvancetranunkid")) & " ")
                                            If dr_Row.Length > 0 Then
                                                'Sohail (30 Jan 2020) -- Start
                                                'Tujijenge Tanzania Issue # 0004470 : Incomplete Loans Showing As Complete
                                                'strQ = " Update lnloan_advance_tran set " & _
                                                '       " balance_amount = balance_amount + @amount " & _
                                                '           ", balance_amountPaidCurrency = balance_amountPaidCurrency + @amountPaidCurrency " & _
                                                '       " WHERE loanadvancetranunkid = @loanadvancetranunkid "
                                                strQ = " Update lnloan_advance_tran set " & _
                                                       " balance_amount = @amount " & _
                                                           ", balance_amountPaidCurrency = @amountPaidCurrency " & _
                                                       " WHERE loanadvancetranunkid = @loanadvancetranunkid "
                                                'Sohail (30 Jan 2020) -- End

                                                objDataOperation.ClearParameters()
                                                'Sohail (30 Jan 2020) -- Start
                                                'Tujijenge Tanzania Issue # 0004470 : Incomplete Loans Showing As Complete
                                                'objDataOperation.AddParameter("@amount", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, CDec(dr_Row(0).Item("TotPrincipalAmount")))
                                                'objDataOperation.AddParameter("@amountPaidCurrency", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, CDec(dr_Row(0).Item("TotPrincipalAmountPaidCurrency")))
                                                'objDataOperation.AddParameter("@loanadvancetranunkid", SqlDbType.Money, eZeeDataType.MONEY_SIZE, drRow("loanadvancetranunkid").ToString())
                                                'Sohail (16 Apr 2020) -- Start
                                                'EKO SUPREME NIGERIA issue # 0004654 : Advance amount disappears when user voids payment.
                                                'objDataOperation.AddParameter("@amount", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, CDec(dr_Row(0).Item("BalanceAmount")) + CDec(drRow.Item("principal_amount")))
                                                'objDataOperation.AddParameter("@amountPaidCurrency", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, CDec(dr_Row(0).Item("BalanceAmountPaidCurrency")) + CDec(drRow.Item("principal_amountPaidCurrency")))
                                                If CBool(dr_Row(0).Item("isloan")) = True Then
                                                    'Hemant (24 Jun 2020) -- Start
                                                    'ISSUE(VOLTAMP) :  Advance not taking in Payroll Process
                                                    'objDataOperation.AddParameter("@amount", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, CDec(dr_Row(0).Item("BalanceAmount")) + CDec(drRow.Item("principal_amount")))
                                                    'objDataOperation.AddParameter("@amountPaidCurrency", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, CDec(dr_Row(0).Item("BalanceAmountPaidCurrency")) + CDec(drRow.Item("principal_amountPaidCurrency")))
                                                    objDataOperation.AddParameter("@amount", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, CDec(dr_Row(0).Item("Balance_Amount")) + CDec(drRow.Item("principal_amount")))
                                                    objDataOperation.AddParameter("@amountPaidCurrency", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, CDec(dr_Row(0).Item("Balance_AmountPaidCurrency")) + CDec(drRow.Item("principal_amountPaidCurrency")))
                                                    'Hemant (24 Jun 2020) -- End                                                    
                                                Else 'Advance
                                                    objDataOperation.AddParameter("@amount", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, CDec(dr_Row(0).Item("BalanceAmount")) + CDec(drRow.Item("amount")))
                                                    objDataOperation.AddParameter("@amountPaidCurrency", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, CDec(dr_Row(0).Item("BalanceAmountPaidCurrency")) + CDec(drRow.Item("amountPaidCurrency")))
                                                End If
                                                'Sohail (16 Apr 2020) -- End
                                                objDataOperation.AddParameter("@loanadvancetranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, drRow("loanadvancetranunkid").ToString())
                                                'Sohail (30 Jan 2020) -- End

                                                objDataOperation.ExecNonQuery(strQ)

                                                If objDataOperation.ErrorMessage <> "" Then
                                                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                                    Throw exForce
                                                End If
                                            End If

                                            strQ = "Select balance_amount, balance_amountPaidCurrency from lnloan_advance_tran  " & _
                                             " WHERE loanadvancetranunkid = @loanadvancetranunkid "

                                            objDataOperation.ClearParameters()
                                            'Sohail (30 Jan 2020) -- Start
                                            'Tujijenge Tanzania Issue # 0004470 : Incomplete Loans Showing As Complete
                                            'objDataOperation.AddParameter("@loanadvancetranunkid", SqlDbType.Money, eZeeDataType.MONEY_SIZE, drRow("loanadvancetranunkid").ToString())
                                            objDataOperation.AddParameter("@loanadvancetranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, drRow("loanadvancetranunkid").ToString())
                                            'Sohail (30 Jan 2020) -- End
                                            Dim dsAmount As DataSet = objDataOperation.ExecQuery(strQ, "Balance")

                                            If objDataOperation.ErrorMessage <> "" Then
                                                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                                Throw exForce
                                            End If

                                            If dsAmount IsNot Nothing Then

                                                If dsAmount.Tables("Balance").Rows.Count > 0 Then

                                                    'Sohail (07 May 2015) -- Start
                                                    'Enhancement - New Loan Redesign with allow to change interest rate, tenure, topup, receipt.
                                                    'If CDec(dsAmount.Tables("Balance").Rows(0)("balance_amount")) > 0 Then 'Sohail (11 May 2011)
                                                    If CDec(dsAmount.Tables("Balance").Rows(0)("balance_amountPaidCurrency")) > 0 Then
                                                        'Sohail (07 May 2015) -- End

                                                        strQ = " Update lnloan_advance_tran set " & _
                                                              " loan_statusunkid = 1 " & _
                                                              " WHERE loanadvancetranunkid = @loanadvancetranunkid "
                                                        objDataOperation.ClearParameters()
                                                        objDataOperation.AddParameter("@loanadvancetranunkid", SqlDbType.Money, eZeeDataType.MONEY_SIZE, drRow("loanadvancetranunkid").ToString())
                                                        objDataOperation.ExecNonQuery(strQ)

                                                        If objDataOperation.ErrorMessage <> "" Then
                                                            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                                            Throw exForce
                                                        End If

                                                        Dim dsStatus As DataSet = objLoanStatusTran.GetList("LoanStatus", True, mintPeriodunkid, mintEmployeeunkid)
                                                        Dim dtStatus As DataTable = New DataView(dsStatus.Tables("LoanStatus"), "loanadvancetranunkid= " & CInt(drRow("loanadvancetranunkid")) & " AND statusunkid = 4 AND isvoid = 0 AND periodunkid = " & mintPeriodunkid & "", "", DataViewRowState.CurrentRows).ToTable
                                                        If dtStatus IsNot Nothing Then
                                                            If dtStatus.Rows.Count > 0 Then
                                                                objLoanStatusTran._Loanstatustranunkid = CInt(dtStatus.Rows(0)("loanstatustranunkid"))
                                                                objLoanStatusTran._Isvoid = True
                                                                objLoanStatusTran._Voiddatetime = VoidDateTime
                                                                objLoanStatusTran._Voiduserunkid = UserId

                                                                With objLoanStatusTran
                                                                    ._FormName = mstrFormName
                                                                    ._LoginEmployeeunkid = mintLogEmployeeUnkid
                                                                    ._ClientIP = mstrClientIP
                                                                    ._HostName = mstrHostName
                                                                    ._FromWeb = mblnIsWeb
                                                                    ._AuditUserId = mintAuditUserId
                                                                    ._CompanyUnkid = mintCompanyunkid
                                                                    ._AuditDate = mdtAuditDate
                                                                End With
                                                                blnFlag = objLoanStatusTran.Update()
                                                                If blnFlag = False Then objDataOperation.ReleaseTransaction(False)

                                                                'Sohail (24 Apr 2019) -- Start
                                                                'NPK Issue - Support Issue Id # 0003771 - 76.1 - Not Able to delete the advance after voiding payroll payment of March (due to transaction was not getting voided from loan_balance_tran on voiding payment).
                                                                strQ = "UPDATE lnloan_balance_tran SET isvoid = 1, voiduserunkid = " & UserId & ", voiddatetime = GETDATE(), voidreason = '" & strVoidReason & "' WHERE lnloan_balance_tran.loanstatustranunkid= " & CInt(dtStatus.Rows(0)("loanstatustranunkid")) & " "
                                                                objDataOperation.ExecNonQuery(strQ)
                                                                If objDataOperation.ErrorMessage <> "" Then
                                                                    objDataOperation.ReleaseTransaction(False)
                                                                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                                                    Throw exForce
                                                                End If
                                                                'Sohail (24 Apr 2019) -- End

                                                            End If
                                                        End If
                                                    End If

                                                End If

                                            End If


                                            If objDataOperation.ErrorMessage <> "" Then
                                                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                                Throw exForce
                                            End If

                                        ElseIf CDec(drRow("savingtranunkid")) > 0 Then

                                            ' FOR UPDATE BALANCE IN SAVING TRAN TABLE 

                                            strQ = " Update svsaving_tran set " & _
                                                  " balance_amount = balance_amount - @amount, " & _
                                                  " total_contribution = total_contribution - @amount " & _
                                                  " WHERE savingtranunkid = @savingtranunkid "

                                            objDataOperation.ClearParameters()
                                            objDataOperation.AddParameter("@amount", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, drRow("amount").ToString()) 'Sohail (11 May 2011)
                                            objDataOperation.AddParameter("@savingtranunkid", SqlDbType.Money, eZeeDataType.MONEY_SIZE, drRow("savingtranunkid").ToString())
                                            objDataOperation.ExecNonQuery(strQ)


                                            If objDataOperation.ErrorMessage <> "" Then
                                                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                                Throw exForce
                                            End If

                                        End If

                                    Next
                                End If 'Sohail (24 Jan 2017) 

                                If Math.Abs(mdecRoundingAdjustment) > 0 Then
                                    Dim objTranHead As New clsTransactionHead
                                    Dim mintRoundingAdjustmentHeadID As Integer = objTranHead.GetRoundingAdjustmentHeadID()

                                    strQ = "UPDATE  prpayrollprocess_tran " & _
                                            "SET     isvoid = 1 " & _
                                                  ", voiduserunkid = 1 " & _
                                                  ", voiddatetime = GETDATE() " & _
                                                  ", voidreason = 'Payment Voided' " & _
                                            "WHERE   isvoid = 0 " & _
                                                    "AND tnaleavetranunkid = @tnaleavetranunkid " & _
                                                    "AND tranheadunkid = @tranheadunkid " & _
                                                    "AND amount = @roundingadjustment "

                                    objDataOperation.ClearParameters()
                                    objDataOperation.AddParameter("@tranheadunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintRoundingAdjustmentHeadID)
                                    objDataOperation.AddParameter("@tnaleavetranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPayreftranunkid)
                                    objDataOperation.AddParameter("@roundingadjustment", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, Math.Abs(mdecRoundingAdjustment))
                                    objDataOperation.ExecNonQuery(strQ)

                                    If objDataOperation.ErrorMessage <> "" Then
                                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                        Throw exForce
                                    End If

                                    objTranHead = Nothing
                                End If

                                'FOR UPDATE BALANCE  IN TNA LEAVE TRAN TABLE
                                'Enhancement - Set Limit for C/F Netpay balance and Payment JV Report
                                strQ = " Update prtnaleave_tran set " & _
                                       " balanceamount = balanceamount + @amount + @roundingadjustment " & _
                                       " WHERE tnaleavetranunkid = @tnaleavetranunkid "

                                objDataOperation.ClearParameters()
                                objDataOperation.AddParameter("@amount", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecAmount) 'Sohail (11 May 2011)
                                objDataOperation.AddParameter("@tnaleavetranunkid", SqlDbType.Money, eZeeDataType.MONEY_SIZE, mintPayreftranunkid)
                                objDataOperation.AddParameter("@roundingadjustment", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecRoundingAdjustment) 'Sohail (21 Mar 2014)
                                objDataOperation.ExecNonQuery(strQ)


                                If objDataOperation.ErrorMessage <> "" Then
                                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                    Throw exForce
                                End If

                            End If

                            'objpayrollProcess = Nothing
                        End If

                    Case enPaymentRefId.SAVINGS

                        Select Case mintPaymentTypeId

                            Case enPayTypeId.DEPOSIT
                                strQ = " Update svsaving_tran set " & _
                                            " balance_amount = balance_amount - @amount " & _
                                      " WHERE savingtranunkid = @savingtranunkid "

                            Case Else
                                strQ = " Update svsaving_tran set " & _
                                            " balance_amount = balance_amount + @amount " & _
                                      " WHERE savingtranunkid = @savingtranunkid "
                        End Select


                        objDataOperation.ClearParameters()
                        objDataOperation.AddParameter("@amount", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecAmount) 'Sohail (11 May 2011)
                        objDataOperation.AddParameter("@savingtranunkid", SqlDbType.Money, eZeeDataType.MONEY_SIZE, mintPayreftranunkid)
                        objDataOperation.ExecNonQuery(strQ)


                        If objDataOperation.ErrorMessage <> "" Then
                            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                            Throw exForce
                        End If

                End Select

                If mdtActivityAdjustment IsNot Nothing AndAlso mdtActivityAdjustment.Rows.Count > 0 Then
                    Dim objActAdj As clsFundActivityAdjustment_Tran
                    For Each dtRow As DataRow In mdtActivityAdjustment.Rows
                        objActAdj = New clsFundActivityAdjustment_Tran

                        objActAdj._FundActivityunkid = CInt(dtRow.Item("fundactivityunkid"))
                        objActAdj._TransactionDate = eZeeDate.convertDate(dtRow.Item("transactiondate").ToString())
                        objActAdj._CurrentBalance = CDec(dtRow.Item("Current Balance"))
                        objActAdj._IncrDecrAmount = CDec(dtRow.Item("Actual Salary"))
                        objActAdj._NewBalance = CDec(dtRow.Item("Current Balance")) + CDec(dtRow.Item("Actual Salary"))
                        objActAdj._Remark = dtRow.Item("remark").ToString()
                        objActAdj._Userunkid = UserId
                        objActAdj._Paymenttranunkid = intUnkid
                        objActAdj._Globalvocunkid = 0
                        With objActAdj
                            ._FormName = mstrFormName
                            ._LoginEmployeeunkid = mintLogEmployeeUnkid
                            ._ClientIP = mstrClientIP
                            ._HostName = mstrHostName
                            ._FromWeb = mblnIsWeb
                            ._AuditUserId = mintAuditUserId
                            ._CompanyUnkid = mintCompanyUnkid
                            ._AuditDate = mdtAuditDate
                        End With
                        If objActAdj.Insert(VoidDateTime, objDataOperation, True) = False Then
                            objDataOperation.ReleaseTransaction(False)
                            Throw New Exception(objActAdj._Message)
                            Return False
                        End If
                    Next
                End If

                intPrevPeriodId = mintPeriodunkid
                'Sohail (30 Oct 2018) -- End

                'Sohail (22 May 2014) -- Start
                'Enhancement - Show Progress on Global Void Payment.
                If bw IsNot Nothing Then
                    intCount = intCount + 1
                    bw.ReportProgress(intCount)
                End If
                'Sohail (22 May 2014) -- End
            Next

            'Sohail (30 Oct 2018) -- Start
            'FDRC Enhancement - Global Payment performance issue on FDRC database in 75.1.
            ''Sohail (23 May 2017) -- Start
            ''Enhancement - 67.1 - Link budget with Payroll.
            'If mdtActivityAdjustment IsNot Nothing AndAlso mdtActivityAdjustment.Rows.Count > 0 Then
            '    Dim objActAdj As clsFundActivityAdjustment_Tran
            '    For Each dtRow As DataRow In mdtActivityAdjustment.Rows
            '        objActAdj = New clsFundActivityAdjustment_Tran

            '        objActAdj._FundActivityunkid = CInt(dtRow.Item("fundactivityunkid"))
            '        objActAdj._TransactionDate = eZeeDate.convertDate(dtRow.Item("transactiondate").ToString())
            '        objActAdj._CurrentBalance = CDec(dtRow.Item("Current Balance"))
            '        objActAdj._IncrDecrAmount = CDec(dtRow.Item("Actual Salary"))
            '        objActAdj._NewBalance = CDec(dtRow.Item("Current Balance")) + CDec(dtRow.Item("Actual Salary"))
            '        objActAdj._Remark = dtRow.Item("remark").ToString()
            '        objActAdj._Userunkid = UserId
            '        objActAdj._Paymenttranunkid = intUnkid
            '        objActAdj._Globalvocunkid = 0

            '        If objActAdj.Insert(VoidDateTime, objDataOperation, True) = False Then
            '            objDataOperation.ReleaseTransaction(False)
            '            Throw New Exception(objActAdj._Message)
            '            Return False
            '        End If
            '    Next
            'End If
            ''Sohail (23 May 2017) -- End
            'Sohail (30 Oct 2018) -- End

            objDataOperation.ReleaseTransaction(True)

            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: VoidAll; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function
    'Sohail (21 Aug 2015) -- End
    'Sohail (12 Oct 2011) -- End

    'S.SANDEEP [ 20 APRIL 2012 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Public Function GetMaxPaymentVocNo(ByVal objData As clsDataOperation, ByVal iCompanyId As Integer) As String
        Dim StrQ As String = ""
        Dim dsList As New DataSet
        Dim StrVocNo As String = String.Empty
        Try
            StrQ = "SELECT " & _
                    "	 CASE WHEN ISNULL(key_value,'') = '' THEN VNO ELSE key_value + VNO END AS VocNo " & _
                    "	,VNO " & _
                    "	,key_value " & _
                    "FROM hrmsConfiguration..cfconfiguration " & _
                    "JOIN " & _
                    "( " & _
                    "	SELECT " & _
                    "		 key_value  AS VNO " & _
                    "		,companyunkid AS Cid " & _
                    "	FROM hrmsConfiguration..cfconfiguration " & _
                    "	WHERE companyunkid = '" & iCompanyId & "' AND key_name = 'NextPaymentVocNo' " & _
                    ")AS B ON B.Cid = hrmsConfiguration..cfconfiguration.companyunkid " & _
                    "WHERE companyunkid = '" & iCompanyId & "' AND key_name = 'PaymentVocPrefix' "

            dsList = objData.ExecQuery(StrQ, "List")

            If objData.ErrorMessage <> "" Then
                Throw New Exception(objData.ErrorNumber & " : " & objData.ErrorMessage)
            End If

            If dsList.Tables("List").Rows.Count > 0 Then
                StrVocNo = dsList.Tables("List").Rows(0)("VocNo")
            End If

            If StrVocNo.Trim.Length > 0 Then
                If isExist(StrVocNo) = True Then
                    If dsList.Tables("List").Rows(0)("key_value").ToString.Trim.Length > 0 Then
                        StrVocNo = dsList.Tables("List").Rows(0)("key_value") & CInt(dsList.Tables("List").Rows(0)("VNO")) + 1
                    Else
                        StrVocNo = CStr(CInt(dsList.Tables("List").Rows(0)("VNO")) + 1)
                    End If
                End If
            Else
                Return ""
            End If

            Return StrVocNo

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetMaxPaymentVocNo; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Function
    'S.SANDEEP [ 20 APRIL 2012 ] -- END

    'Sohail (02 Jul 2012) -- Start
    'TRA - ENHANCEMENT
    'Sohail (27 Feb 2013) -- Start
    'TRA - ENHANCEMENT
    'Public Function UpdatePaymentAuthorization(ByVal blnIsAuthorized As Boolean _
    '                                           , ByVal strPaymentTranUnkIdsList As String _
    '                                           , ByVal intUserId As Integer _
    '                                           , ByVal blnActive As Boolean _
    '                                           , ByVal strRemarks As String _
    '                                           ) As Boolean
    '    'Sohail (27 Feb 2013) - [strRemarks]

    '    Dim objDataOperation As clsDataOperation

    '    Dim StrQ As String = ""
    '    Dim dsList As New DataSet
    '    Dim StrVocNo As String = String.Empty

    '    objDataOperation = New clsDataOperation
    '    objDataOperation.BindTransaction()

    '    Try

    '        StrQ = "UPDATE  prpayment_tran " & _
    '                "SET     isauthorized = @isauthorized " & _
    '                "WHERE   paymenttranunkid IN ( " & strPaymentTranUnkIdsList & " ) " & _
    '                "AND ISNULL(isvoid, 0) = 0 "
    '        'Sohail (27 Feb 2013) - [isvoid]

    '        objDataOperation.AddParameter("@isauthorized", SqlDbType.Bit, eZeeDataType.BIT_SIZE, blnIsAuthorized.ToString)

    '        objDataOperation.ExecNonQuery(StrQ)

    '        If objDataOperation.ErrorMessage <> "" Then
    '            objDataOperation.ReleaseTransaction(False)
    '            Throw New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
    '        End If

    '        'Sohail (27 Jul 2012) -- Start
    '        'TRA - ENHANCEMENT - void old entries
    '        StrQ = "UPDATE  prpayment_authorize_tran " & _
    '                "SET     isactive = 0 " & _
    '                      ", voiddatetime = GETDATE() " & _
    '                      ", voiduserunkid = @userunkid " & _
    '                      ", voidreason = '' " & _
    '                "WHERE   isactive = 1 " & _
    '                        "AND paymenttranunkid IN ( " & strPaymentTranUnkIdsList & " ) "
    '        'Sohail (27 Jul 2012) -- End

    '        StrQ &= "; INSERT  INTO prpayment_authorize_tran " & _
    '                        "( paymenttranunkid " & _
    '                        ", authorize_date " & _
    '                        ", isauthorized " & _
    '                        ", userunkid " & _
    '                        ", isactive " & _
    '                        ", voiduserunkid " & _
    '                        ", voiddatetime " & _
    '                        ", voidreason " & _
    '                        ", remarks " & _
    '                        ") " & _
    '                        "SELECT  paymenttranunkid " & _
    '                              ", GETDATE() " & _
    '                              ", @isauthorized " & _
    '                              ", @userunkid " & _
    '                              ", @isactive " & _
    '                              ", @voiduserunkid " & _
    '                              ", @voiddatetime " & _
    '                              ", @voidreason " & _
    '                              ", @remarks " & _
    '                        "FROM    prpayment_tran " & _
    '                        "WHERE   paymenttranunkid IN ( " & strPaymentTranUnkIdsList & " ) " & _
    '                        "AND ISNULL(isvoid, 0) = 0 "
    '        'Sohail (27 Feb 2013) - [isvoid, remarks]

    '        objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUserId.ToString)
    '        objDataOperation.AddParameter("@isactive", SqlDbType.Bit, eZeeDataType.BIT_SIZE, blnActive.ToString)
    '        'Sohail (27 Jul 2012) -- Start
    '        'TRA - ENHANCEMENT
    '        objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUserId.ToString)
    '        objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
    '        objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, String.Empty)
    '        'Sohail (27 Jul 2012) -- End
    '        objDataOperation.AddParameter("@remarks", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, strRemarks.ToString) 'Sohail (27 Feb 2013)

    '        objDataOperation.ExecNonQuery(StrQ)

    '        If objDataOperation.ErrorMessage <> "" Then
    '            objDataOperation.ReleaseTransaction(False)
    '            Throw New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
    '        End If

    '        'Sohail (27 Jul 2012) -- Start
    '        'TRA - ENHANCEMENT - Insert in AT Log
    '        If mstrWebFormName.Trim.Length <= 0 Then
    '            'S.SANDEEP [ 11 AUG 2012 ] -- START
    '            'ENHANCEMENT : TRA CHANGES
    '            'Dim frm As Form
    '            'For Each frm In Application.OpenForms
    '            '    If Form.ActiveForm.Name IsNot Nothing AndAlso Form.ActiveForm.Name = frm.Name Then
    '            '        objDataOperation.AddParameter("@form_name", SqlDbType.NVarChar, 500, frm.Name)
    '            '        objDataOperation.AddParameter("@isweb", SqlDbType.Bit, eZeeDataType.BIT_SIZE, False)
    '            '        
    '            '    End If
    '            'Next
    '            objDataOperation.AddParameter("@form_name", SqlDbType.NVarChar, 500, mstrForm_Name)
    '            objDataOperation.AddParameter("@isweb", SqlDbType.Bit, eZeeDataType.BIT_SIZE, False)
    '            
    '            'S.SANDEEP [ 11 AUG 2012 ] -- END
    '        Else
    '            objDataOperation.AddParameter("@form_name", SqlDbType.NVarChar, 500, mstrWebFormName)
    '            objDataOperation.AddParameter("@isweb", SqlDbType.Bit, eZeeDataType.BIT_SIZE, True)
    '            objDataOperation.AddParameter("@module_name1", SqlDbType.NVarChar, 500, Language.getMessage(mstrModuleName, 2, "WEB"))
    '        End If
    '        
    '        
    '        
    '        

    '        StrQ = "INSERT INTO atprpayment_tran " & _
    '                        "( paymenttranunkid, voucherno, periodunkid, paymentdate, employeeunkid, referenceid, paymentmode, branchunkid, chequeno, paymentby, percentage, amount, voucherref, referencetranunkid, paytypeid, isreceipt, isglobalpayment, audittype, audituserunkid, auditdatetime, ip, machine_name, basecurrencyid, baseexchangerate, paidcurrencyid, expaidrate, expaidamt, globalvocno, isauthorized, accountno, form_name, module_name1, module_name2, module_name3, module_name4, module_name5, isweb, countryunkid, isapproved) " & _
    '                "SELECT " & _
    '                          "paymenttranunkid, voucherno, periodunkid, paymentdate, employeeunkid, referenceid, paymentmode, branchunkid, chequeno, paymentby, percentage, amount, voucherref, referencetranunkid, paytypeid, isreceipt, isglobalpayment, 2, @userunkid, GETDATE(), '" & getIP() & "', '" & getHostName() & "', basecurrencyid, baseexchangerate, paidcurrencyid, expaidrate, expaidamt, globalvocno, isauthorized, accountno, @form_name, @module_name1, @module_name2, @module_name3, @module_name4, @module_name5, @isweb, @countryunkid, @isapproved " & _
    '                "FROM prpayment_tran " & _
    '                "WHERE ISNULL(isvoid, 0) = 0 " & _
    '                "AND paymenttranunkid IN ( " & strPaymentTranUnkIdsList & " ) "
    '        'Sohail (27 Feb 2013) - [countryunkid, isapproved]

    '        objDataOperation.ExecNonQuery(StrQ)

    '        If objDataOperation.ErrorMessage <> "" Then
    '            objDataOperation.ReleaseTransaction(False)
    '            Throw New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
    '        End If

    '        StrQ = "INSERT INTO atprpayment_authorize_tran " & _
    '                        "( paymentauthorizetranunkid, paymenttranunkid, authorize_date, isauthorized, audittype, audituserunkid, auditdatetime, ip, machine_name, form_name, module_name1, module_name2, module_name3, module_name4, module_name5, isweb, remarks ) " & _
    '                "SELECT " & _
    '                     "paymentauthorizetranunkid, paymenttranunkid, authorize_date, isauthorized, 1, @userunkid, GETDATE(), '" & getIP() & "', '" & getHostName() & "', @form_name, @module_name1, @module_name2, @module_name3, @module_name4, @module_name5, @isweb, @remarks " & _
    '                "FROM prpayment_authorize_tran " & _
    '                "WHERE isactive = @isactive " & _
    '                     "AND paymenttranunkid IN ( " & strPaymentTranUnkIdsList & " ) "
    '        'Sohail (27 Feb 2013) - [remarks]

    '        Call objDataOperation.ExecNonQuery(StrQ)

    '        If objDataOperation.ErrorMessage <> "" Then
    '            objDataOperation.ReleaseTransaction(False)
    '            Throw New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
    '        End If
    '        'Sohail (27 Jul 2012) -- End

    '        objDataOperation.ReleaseTransaction(True)

    '        Return True

    '    Catch ex As Exception
    '        objDataOperation.ReleaseTransaction(False)
    '        Throw New Exception(ex.Message & "; Procedure Name: UpdatePaymentAuthorization; Module Name: " & mstrModuleName)
    '    Finally
    '    End Try
    'End Function
    Public Function UpdatePaymentAuthorization(ByVal blnIsAuthorized As Boolean _
                                               , ByVal strPaymentTranUnkIdsList As String _
                                               , ByVal intUserId As Integer _
                                               , ByVal blnActive As Boolean _
                                               , ByVal strRemarks As String _
                                               ) As Boolean

        Dim objDataOperation As clsDataOperation

        Dim StrQ As String = ""
        Dim dsList As New DataSet
        Dim StrVocNo As String = String.Empty

        objDataOperation = New clsDataOperation
        objDataOperation.BindTransaction()

        Try

            objDataOperation.AddParameter("@isauthorized", SqlDbType.Bit, eZeeDataType.BIT_SIZE, blnIsAuthorized.ToString)
            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUserId.ToString)
            objDataOperation.AddParameter("@isactive", SqlDbType.Bit, eZeeDataType.BIT_SIZE, blnActive.ToString)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUserId.ToString)
            objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, String.Empty)
            objDataOperation.AddParameter("@remarks", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, strRemarks.ToString)
            If objDataOperation.ErrorMessage <> "" Then
                objDataOperation.ReleaseTransaction(False)
                Throw New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
            End If

            'Hemant (07 Jan 2019) -- Start
            'ISSUE/ENHANCEMENT : {Audit Trails} in 76.1.
            'If mstrWebFormName.Trim.Length <= 0 Then
            '    objDataOperation.AddParameter("@form_name", SqlDbType.NVarChar, 500, mstrForm_Name)
            '    objDataOperation.AddParameter("@isweb", SqlDbType.Bit, eZeeDataType.BIT_SIZE, False)

            'Else
            '    objDataOperation.AddParameter("@form_name", SqlDbType.NVarChar, 500, mstrWebFormName)
            '    objDataOperation.AddParameter("@isweb", SqlDbType.Bit, eZeeDataType.BIT_SIZE, True)
            '    objDataOperation.AddParameter("@module_name1", SqlDbType.NVarChar, 500, Language.getMessage(mstrModuleName, 2, "WEB"))
            'End If
            objDataOperation.AddParameter("@form_name", SqlDbType.NVarChar, 500, mstrFormName)
            objDataOperation.AddParameter("@isweb", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsWeb)
            'Hemant (07 Jan 2019) -- End

            'Pinkal (24-May-2013) -- Start
            'Enhancement : TRA Changes





            If mstrClientIP.ToString().Length <= 0 Then
                mstrClientIP = getIP()
            End If

            If mstrHostName.ToString().Length <= 0 Then
                mstrHostName = getHostName()
            End If

            'Pinkal (24-May-2013) -- End

            'Sohail (17 Dec 2014) -- Start
            'Enhancement - Provide Link for Payroll approval/Authorize Payslip Payment.
            If isExistPaymentAuthorization(strPaymentTranUnkIdsList, blnIsAuthorized) Then
                mstrMessage = Language.getMessage(mstrModuleName, 31, "Sorry, Payment Authorization is already done for some of the selected payments. Please Re-select Period and process again.")
                objDataOperation.ReleaseTransaction(False)
                Return False
            End If
            'Sohail (17 Dec 2014) -- End


            '*** Insert Log for void old entries
            StrQ = "INSERT INTO atprpayment_authorize_tran " & _
                           "( paymentauthorizetranunkid, paymenttranunkid, authorize_date, isauthorized, audittype, audituserunkid, auditdatetime, ip, machine_name, form_name, isweb, remarks ) " & _
                   "SELECT " & _
                        "paymentauthorizetranunkid, paymenttranunkid, authorize_date, isauthorized, 3, @userunkid, GETDATE(), '" & mstrClientIP & "', '" & mstrHostName & "', @form_name, @isweb, @remarks " & _
                   "FROM prpayment_authorize_tran " & _
                   "WHERE isactive = 1 " & _
                        "AND paymenttranunkid IN ( " & strPaymentTranUnkIdsList & " ) "

            Call objDataOperation.ExecNonQuery(StrQ)

            If objDataOperation.ErrorMessage <> "" Then
                objDataOperation.ReleaseTransaction(False)
                Throw New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
            End If

            '*** void old entries
            StrQ = "UPDATE  prpayment_authorize_tran " & _
                    "SET     isactive = 0 " & _
                          ", voiddatetime = GETDATE() " & _
                          ", voiduserunkid = @userunkid " & _
                          ", voidreason = '' " & _
                    "WHERE   isactive = 1 " & _
                            "AND paymenttranunkid IN ( " & strPaymentTranUnkIdsList & " ) "

            '*** insert same entries with isactive = @isactive (TRUE)
            StrQ &= "; INSERT  INTO prpayment_authorize_tran " & _
                            "( paymenttranunkid " & _
                            ", authorize_date " & _
                            ", isauthorized " & _
                            ", userunkid " & _
                            ", isactive " & _
                            ", voiduserunkid " & _
                            ", voiddatetime " & _
                            ", voidreason " & _
                            ", remarks " & _
                            ") " & _
                            "SELECT  paymenttranunkid " & _
                                  ", GETDATE() " & _
                                  ", @isauthorized " & _
                                  ", @userunkid " & _
                                  ", @isactive " & _
                                  ", @voiduserunkid " & _
                                  ", @voiddatetime " & _
                                  ", @voidreason " & _
                                  ", @remarks " & _
                            "FROM    prpayment_tran " & _
                            "WHERE   paymenttranunkid IN ( " & strPaymentTranUnkIdsList & " ) " & _
                            "AND ISNULL(isvoid, 0) = 0 "

            objDataOperation.ExecNonQuery(StrQ)

            If objDataOperation.ErrorMessage <> "" Then
                objDataOperation.ReleaseTransaction(False)
                Throw New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
            End If

            '*** Insert Log entries with isactive = @isactive (TRUE)
            StrQ = "INSERT INTO atprpayment_authorize_tran " & _
                           "( paymentauthorizetranunkid, paymenttranunkid, authorize_date, isauthorized, audittype, audituserunkid, auditdatetime, ip, machine_name, form_name, isweb, remarks ) " & _
                   "SELECT " & _
                        "paymentauthorizetranunkid, paymenttranunkid, authorize_date, isauthorized, 1, @userunkid, GETDATE(), '" & mstrClientIP & "', '" & mstrHostName & "', @form_name, @isweb, @remarks " & _
                   "FROM prpayment_authorize_tran " & _
                   "WHERE isactive = @isactive " & _
                        "AND paymenttranunkid IN ( " & strPaymentTranUnkIdsList & " ) "

            Call objDataOperation.ExecNonQuery(StrQ)

            If objDataOperation.ErrorMessage <> "" Then
                objDataOperation.ReleaseTransaction(False)
                Throw New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
            End If



            StrQ = "INSERT INTO atprpayment_tran " & _
                           "( paymenttranunkid, voucherno, periodunkid, paymentdate, employeeunkid, referenceid, paymentmode, branchunkid, chequeno, paymentby, percentage, amount, voucherref, referencetranunkid, paytypeid, isreceipt, isglobalpayment, audittype, audituserunkid, auditdatetime, ip, machine_name, basecurrencyid, baseexchangerate, paidcurrencyid, expaidrate, expaidamt, globalvocno, isauthorized, accountno, form_name, isweb, countryunkid, isapproved, roundingtypeid, roundingmultipleid, roundingadjustment, remarks, paymentdate_periodunkid) " & _
                    "SELECT " & _
                             "paymenttranunkid, voucherno, periodunkid, paymentdate, employeeunkid, referenceid, paymentmode, branchunkid, chequeno, paymentby, percentage, amount, voucherref, referencetranunkid, paytypeid, isreceipt, isglobalpayment, 2, @userunkid, GETDATE(), '" & mstrClientIP & "', '" & mstrHostName & "', basecurrencyid, baseexchangerate, paidcurrencyid, expaidrate, expaidamt, globalvocno, @isauthorized, accountno, @form_name, @isweb, countryunkid, isapproved, roundingtypeid, roundingmultipleid, roundingadjustment, remarks, paymentdate_periodunkid " & _
                    "FROM prpayment_tran " & _
                    "WHERE ISNULL(isvoid, 0) = 0 " & _
                    "AND paymenttranunkid IN ( " & strPaymentTranUnkIdsList & " ) "

            objDataOperation.ExecNonQuery(StrQ)

            If objDataOperation.ErrorMessage <> "" Then
                objDataOperation.ReleaseTransaction(False)
                Throw New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
            End If

            StrQ = "UPDATE  prpayment_tran " & _
                    "SET     isauthorized = @isauthorized " & _
                    "WHERE   paymenttranunkid IN ( " & strPaymentTranUnkIdsList & " ) " & _
                    "AND ISNULL(isvoid, 0) = 0 "

            objDataOperation.ExecNonQuery(StrQ)

            If objDataOperation.ErrorMessage <> "" Then
                objDataOperation.ReleaseTransaction(False)
                Throw New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
            End If


            objDataOperation.ReleaseTransaction(True)

            Return True

        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: UpdatePaymentAuthorization; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Function
    'Sohail (27 Feb 2013) -- End
    'Sohail (02 Jul 2012) -- End

    'Sohail (07 Dec 2013) -- Start
    'Enhancement - OMAN
    Public Function Get_DIST_ChequeNo(ByVal strTableName As String, _
                                      ByVal blnAddSelect As Boolean, _
                                      ByVal intPayRefId As Integer, _
                                      ByVal intPaymentTypeId As Integer, _
                                      ByVal strDatabaseName As String, _
                                      Optional ByVal intPeriodId As Integer = 0, _
                                      Optional ByVal strFilterQuery As String = "" _
                                      ) As DataSet

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try

            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'If strDatabaseName.Trim = "" Then strDatabaseName = FinancialYear._Object._DatabaseName
            'Sohail (21 Aug 2015) -- End

            If blnAddSelect = True Then
                strQ = "SELECT ' ' + @Select AS chequeno UNION "
                objDataOperation.AddParameter("@Select", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 28, "Select"))
            End If

            strQ &= "SELECT DISTINCT " & _
                            "chequeno " & _
                    "FROM    " & strDatabaseName & "..prpayment_tran " & _
                    "WHERE   ISNULL(isvoid, 0) = 0 " & _
                            "AND prpayment_tran.referenceid = @referenceid " & _
                            "AND prpayment_tran.paytypeid = @paytypeid " & _
                            "AND LTRIM(RTRIM(chequeno)) <> '' "

            If intPeriodId > 0 Then
                strQ &= " AND prpayment_tran.periodunkid = @periodunkid "
                objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intPeriodId.ToString)
            End If

            If strFilterQuery.Trim <> "" Then
                strQ &= " AND " & strFilterQuery
            End If

            objDataOperation.AddParameter("@referenceid", SqlDbType.Int, eZeeDataType.INT_SIZE, intPayRefId.ToString)
            objDataOperation.AddParameter("@paytypeid", SqlDbType.Int, eZeeDataType.INT_SIZE, intPaymentTypeId.ToString)

            dsList = objDataOperation.ExecQuery(strQ, strTableName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If


        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Get_DIST_ChequeNo; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
        Return dsList
    End Function
    'Sohail (07 Dec 2013) -- End

    'Sohail (18 Feb 2014) -- Start
    'Enhancement - AGKN
    'Sohail (21 Aug 2015) -- Start
    'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
    'Public Function Get_DIST_VoucherNo(ByVal strTableName As String, _
    '                                  ByVal blnAddSelect As Boolean, _
    '                                  ByVal intPayRefId As Integer, _
    '                                  ByVal intPaymentTypeId As Integer, _
    '                                  Optional ByVal intPeriodId As Integer = 0, _
    '                                  Optional ByVal strDatabaseName As String = "", _
    '                                  Optional ByVal strFilterQuery As String = "" _
    '                                  ) As DataSet
    Public Function Get_DIST_VoucherNo(ByVal strTableName As String, _
                                      ByVal blnAddSelect As Boolean, _
                                      ByVal intPayRefId As Integer, _
                                      ByVal intPaymentTypeId As Integer, _
                                      ByVal strDatabaseName As String, _
                                      Optional ByVal intPeriodId As Integer = 0, _
                                      Optional ByVal strFilterQuery As String = "" _
                                      ) As DataSet
        'Sohail (21 Aug 2015) -- End

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try

            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'If strDatabaseName.Trim = "" Then strDatabaseName = FinancialYear._Object._DatabaseName
            'Sohail (21 Aug 2015) -- End

            If blnAddSelect = True Then
                strQ = "SELECT ' ' + @Select AS voucherno UNION "
                objDataOperation.AddParameter("@Select", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 28, "Select"))
            End If

            strQ &= "SELECT DISTINCT " & _
                            "CASE WHEN ISNULL(isglobalpayment,0) = 0 THEN ISNULL(prpayment_tran.voucherno,'') " & _
                            "      WHEN ISNULL(isglobalpayment,0) = 1 THEN ISNULL(prglobalvoc_master.globalvocno,'') END AS voucherno " & _
                    "FROM    " & strDatabaseName & "..prpayment_tran " & _
                    "LEFT JOIN " & strDatabaseName & "..prglobalvoc_master ON prpayment_tran.globalvocno = prglobalvoc_master.globalvocunkid " & _
                    "WHERE   ISNULL(isvoid, 0) = 0 " & _
                            "AND prpayment_tran.referenceid = @referenceid " & _
                            "AND prpayment_tran.paytypeid = @paytypeid " & _
                            "AND LTRIM(RTRIM(voucherno)) <> '' "

            If intPeriodId > 0 Then
                strQ &= " AND prpayment_tran.periodunkid = @periodunkid "
                objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intPeriodId.ToString)
            End If

            If strFilterQuery.Trim <> "" Then
                strQ &= " AND " & strFilterQuery
            End If

            objDataOperation.AddParameter("@referenceid", SqlDbType.Int, eZeeDataType.INT_SIZE, intPayRefId.ToString)
            objDataOperation.AddParameter("@paytypeid", SqlDbType.Int, eZeeDataType.INT_SIZE, intPaymentTypeId.ToString)

            dsList = objDataOperation.ExecQuery(strQ, strTableName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If


        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Get_DIST_VoucherNo; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
        Return dsList
    End Function
    'Sohail (18 Feb 2014) -- End

    'S.SANDEEP [ 24 DEC 2012 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Private Function Insert_GvocNo(ByVal objDataOperation As clsDataOperation, ByVal mStrGVocNo As String) As Integer
        Dim iGVocUnkid As Integer = -1
        Dim StrQ As String = String.Empty
        Dim exForce As Exception
        Dim dList As New DataSet
        Dim iCnt As Integer = -1
        Try
            StrQ = "SELECT globalvocunkid,globalvocno FROM prglobalvoc_master WHERE globalvocno = '" & mStrGVocNo & "'"

            iCnt = objDataOperation.RecordCount(StrQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If iCnt <= 0 Then
                StrQ = "INSERT INTO prglobalvoc_master ( " & _
                                    "  globalvocno " & _
                                   ") VALUES (" & _
                                    "  @gvocno " & _
                                   "); SELECT @@identity"

                objDataOperation.AddParameter("@gvocno", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mStrGVocNo.ToString)

                dList = objDataOperation.ExecQuery(StrQ, "List")

                If objDataOperation.ErrorMessage <> "" Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If

                iGVocUnkid = dList.Tables(0).Rows(0).Item(0)
            Else
                mstrMessage = Language.getMessage(mstrModuleName, 3, "Voucher No. '") & mStrGVocNo & Language.getMessage(mstrModuleName, 4, "' is already defined. Please define new Voucher No. from : Aruti Configuration -> Option -> Payment Voc# No.")
            End If

            Return iGVocUnkid

        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "Insert_GvocNo", mstrModuleName)
        End Try
    End Function
    'S.SANDEEP [ 24 DEC 2012 ] -- END

    'Sohail (20 Feb 2013) -- Start
    'TRA - ENHANCEMENT
    Public Sub SendMailToApprover(ByVal blnFromPayment As Boolean _
                                  , ByVal intApproverUserUnkid As Integer _
                                  , ByVal strEmployeeIDs As String _
                                  , ByVal intPeriodId As Integer _
                                  , ByVal blnVoidApproval As Boolean _
                                  , ByVal intBankPaid As Integer _
                                  , ByVal intCashPaid As Integer _
                                  , ByVal intTotalPaid As Integer _
                                  , ByVal decBankAmount As Decimal _
                                  , ByVal decCashAmount As Decimal _
                                  , ByVal decTotalAmount As Decimal _
                                  , ByVal strCurrencySign As String _
                                  , ByVal strRemarks As String _
                                  , ByVal strFmtCurrency As String _
                                  , ByVal intCompanyUnkId As Integer _
                                  , ByVal intYearUnkId As Integer _
                                  , ByVal strDatabaseName As String _
                                  , ByVal xPeriodStart As Date _
                                  , ByVal xPeriodEnd As Date _
                                  , ByVal xUserModeSetting As String _
                                  , ByVal xOnlyApproved As Boolean _
                                  , ByVal xIncludeIn_ActiveEmployee As Boolean _
                                  , Optional ByVal mdtTable As DataTable = Nothing _
                                  , Optional ByVal iLoginTypeId As Integer = -1 _
                                  , Optional ByVal iLoginEmployeeId As Integer = -1 _
                                  , Optional ByVal intCurrencyUnkID As Integer = 0 _
                                  , Optional ByVal strArutiSelfServiceURL As String = "" _
                                  )
        '                         'Sohail (16 Nov 2016) - [xPeriodStart, xPeriodEnd, xUserModeSetting, xOnlyApproved, xIncludeIn_ActiveEmployee]
        '                         'S.SANDEEP [ 28 JAN 2014 ] -- START {iLoginTypeId,iLoginEmployeeId} -- END
        '                         'Sohail (17 Dec 2014) - [intCurrencyUnkID, strArutiSelfServiceURL]
        '                         'Sohail (18 Feb 2014) - [mdtTable]
        '                         'Sohail (18 May 2013) - [strFmtCurrency]
        '                         'Sohail (06 Mar 2013) - [strRemarks]

        Dim objApproverLevel As New clsPaymentApproverlevel_master
        Dim objApproverMap As New clspayment_approver_mapping
        Dim objUser As New clsUserAddEdit
        Dim objPeriod As New clscommom_period_Tran
        Dim objEmp As New clsEmployee_Master
        Dim objMail As New clsSendMail
        Dim strMessage As String = ""
        Dim dsList As DataSet
        Dim intCurrLevelPriority As Integer = -1
        Dim intNextLevelPriority As Integer = -1
        Dim intMaxLevelPriority As Integer = -1
        Dim strApproverName As String
        Dim strApproverUnkIDs As String = ""
        Dim strArrayIDs() As String
        Dim strArrayEmpIDs() As String
        Dim strDIV As String = ""
        Dim blnFinalApproved As Boolean = False
        'Sohail (18 Feb 2014) -- Start
        'Enhancement - AGKN
        Dim intEmpCount As Integer = 0
        Dim int_BankPaid As Integer = intBankPaid
        Dim int_CashPaid As Integer = intCashPaid
        Dim dec_BankAmount As Decimal = decBankAmount
        Dim dec_CashAmount As Decimal = decCashAmount
        'Sohail (18 Feb 2014) -- End
        'Sohail (17 Dec 2014) -- Start
        'Enhancement - Provide Link for Payroll approval/Authorize Payslip Payment.
        Dim strLink As String = ""
        'Sohail (17 Dec 2014) -- End

        Try

            If strEmployeeIDs.Trim = "" Then Exit Try
            strArrayEmpIDs = strEmployeeIDs.Split(",")

            Dim objNet As New clsNetConnectivity
            If objNet._Conected = False Then Exit Sub

            'Sohail (18 Feb 2014) -- Start
            'Enhancement - AGKN
            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            If intCompanyUnkId = 0 Then intCompanyUnkId = Company._Object._Companyunkid
            If intYearUnkId = 0 Then intYearUnkId = FinancialYear._Object._YearUnkid
            If strDatabaseName.Trim = "" Then strDatabaseName = FinancialYear._Object._DatabaseName
            'Sohail (21 Aug 2015) -- End
            'Sohail (18 Feb 2014) -- End

            If blnFromPayment = True Then
                intNextLevelPriority = objApproverLevel.GetMinPriority
            Else
                dsList = objApproverMap.GetList("Approver", intApproverUserUnkid)
                If dsList.Tables("Approver").Rows.Count > 0 Then
                    intCurrLevelPriority = CInt(dsList.Tables("Approver").Rows(0).Item("priority"))
                    intNextLevelPriority = objApproverLevel.GetNextLevelPriority(intCurrLevelPriority)
                    If intNextLevelPriority = -1 Then
                        intMaxLevelPriority = objApproverLevel.GetMaxPriority
                    End If
                End If
            End If
            If blnVoidApproval = False Then 'Sohail (10 Mar 2014)
            If intCurrLevelPriority > -1 AndAlso intCurrLevelPriority = intMaxLevelPriority AndAlso intNextLevelPriority = -1 Then 'If Final Approved
                strApproverUnkIDs = objApproverMap.GetAuthorizePaymentUserUnkIDs()
                blnFinalApproved = True
            Else
                strApproverUnkIDs = objApproverMap.GetApproverUnkIDs(intNextLevelPriority)
            End If
                'Sohail (10 Mar 2014) -- Start
                'Enhancement - Check list box on Payment Approval and Authorize Payment Screen.
            Else
                strApproverUnkIDs = objApproverMap.GetLowerApproverUnkIDs(intCurrLevelPriority)
            End If
            'Sohail (10 Mar 2014) -- End

            If strApproverUnkIDs.Trim = "" Then Exit Try

            strArrayIDs = strApproverUnkIDs.Split(",")



            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'objPeriod._Periodunkid = intPeriodId
            objPeriod._Periodunkid(strDatabaseName) = intPeriodId
            'Sohail (21 Aug 2015) -- End
            objUser._Userunkid = intApproverUserUnkid
            strApproverName = objUser._Firstname & " " & objUser._Lastname
            'Sohail (10 Mar 2014) -- Start
            'Enhancement - Send Emails in Application Backgropund.
            Dim objMaster As New clsMasterData
            gobjEmailList = New List(Of clsEmailCollection)
            'Sohail (10 Mar 2014) -- End

            For Each strApproverID As String In strArrayIDs
                objUser = New clsUserAddEdit

                objUser._Userunkid = CInt(strApproverID)
                If objUser._Email.Trim = "" Then Continue For

                'Sohail (18 Feb 2014) -- Start
                'Enhancement - AGKN
                If mdtTable Is Nothing Then
                    intEmpCount = strArrayEmpIDs.Length
                Else

                    'Sohail (16 Nov 2016) -- Start
                    'Enhancement - 64.1 - Get User Access Level Employees from new employee transfer table and employee categorization table.
                    'Dim strAppEmpIDs As String = objMaster.GetUserAccessLevelEmployees(CInt(strApproverID), intCompanyUnkId, intYearUnkId, strDatabaseName)
                    Dim strAppEmpIDs As String = objMaster.GetUserAccessLevelEmployees(strDatabaseName, CInt(strApproverID), intYearUnkId, intCompanyUnkId, xPeriodStart, xPeriodEnd, xUserModeSetting, xOnlyApproved, xIncludeIn_ActiveEmployee)
                    'Sohail (16 Nov 2016) -- End
                    Dim lstEmp As List(Of DataRow) = (From p In mdtTable Where strAppEmpIDs.Split(",").Contains(CInt(p.Item("employeeunkid").ToString)) Select p).ToList
                    'Dim lstEmp As List(Of DataRow) = (From p In mdtTable Where strAppEmpIDs.Split(",").Contains(p.Item("employeeunkid").ToString) Select p).DefaultIfEmpty.ToList
                    If lstEmp.Count <= 0 Then Continue For
                    intEmpCount = lstEmp.Count

                    Dim lstBankPaid As List(Of DataRow) = (From p In mdtTable Where strAppEmpIDs.Split(",").Contains(CInt(p.Item("employeeunkid").ToString)) AndAlso (CInt(p.Item("Paymentmodeid")) = enPaymentMode.CHEQUE OrElse CInt(p.Item("Paymentmodeid")) = enPaymentMode.TRANSFER) Select p).ToList
                    int_BankPaid = lstBankPaid.Count

                    dec_BankAmount = (From p In mdtTable Where strAppEmpIDs.Split(",").Contains(CInt(p.Item("employeeunkid").ToString)) AndAlso (CInt(p.Item("Paymentmodeid")) = enPaymentMode.CHEQUE OrElse CInt(p.Item("Paymentmodeid")) = enPaymentMode.TRANSFER) Select CDec(p.Item("expaidamt"))).DefaultIfEmpty.Sum()


                    Dim lstCashPaid As List(Of DataRow) = (From p In mdtTable Where strAppEmpIDs.Split(",").Contains(CInt(p.Item("employeeunkid").ToString)) AndAlso (CInt(p.Item("Paymentmodeid")) = enPaymentMode.CASH OrElse CInt(p.Item("Paymentmodeid")) = enPaymentMode.CASH_AND_CHEQUE) Select p).ToList
                    int_CashPaid = lstCashPaid.Count

                    dec_CashAmount = (From p In mdtTable Where strAppEmpIDs.Split(",").Contains(CInt(p.Item("employeeunkid").ToString)) AndAlso (CInt(p.Item("Paymentmodeid")) = enPaymentMode.CASH OrElse CInt(p.Item("Paymentmodeid")) = enPaymentMode.CASH_AND_CHEQUE) Select CDec(p.Item("expaidamt"))).DefaultIfEmpty.Sum()

                    If (int_BankPaid + int_CashPaid) <= 0 Then Continue For 'Sohail (26 Apr 2017)
                End If
                'Sohail (18 Feb 2014) -- End

                strMessage = "<HTML> <BODY style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#333399;'>"


                'Gajanan [27-Mar-2019] -- Start
                'Enhancement - Change Email Language

                'strMessage &= Language.getMessage(mstrModuleName, 12, "Dear") & " <B>" & objUser._Firstname & " " & objUser._Lastname & "</B>, <BR><BR>"
                strMessage &= Language.getMessage(mstrModuleName, 12, "Dear") & " <B>" & getTitleCase(objUser._Firstname & " " & objUser._Lastname) & "</B>, <BR><BR>"
                'Gajanan [27-Mar-2019] -- End

                If blnVoidApproval = True Then

                    objMail._Subject = Language.getMessage(mstrModuleName, 5, "Payslip Payment Disapproved for ") & objPeriod._Period_Name

                    'Sohail (18 Feb 2014) -- Start
                    'Enhancement - AGKN
                    'strMessage &= "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" & Language.getMessage(mstrModuleName, 6, "This is to inform you that Payslip Payment for the period of ") & " <B>" & objPeriod._Period_Name & "</B>" & _
                    '                            Language.getMessage(mstrModuleName, 11, " for ") & strArrayEmpIDs.Length & Language.getMessage(mstrModuleName, 7, " employees have been Disapproved by") & " <B>" & strApproverName & "</B> " & Language.getMessage(mstrModuleName, 8, ". Kindly take a note of it.")


                    'Gajanan [27-Mar-2019] -- Start
                    'Enhancement - Change Email Language

                    'strMessage &= "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" & Language.getMessage(mstrModuleName, 6, "This is to inform you that Payslip Payment for the period of ") & " <B>" & objPeriod._Period_Name & "</B>" & _
                    '                            Language.getMessage(mstrModuleName, 11, " for ") & intEmpCount.ToString & Language.getMessage(mstrModuleName, 7, " employees have been Disapproved by") & " <B>" & strApproverName & "</B> " & Language.getMessage(mstrModuleName, 8, ". Kindly take a note of it.")
                    strMessage &= " " & Language.getMessage(mstrModuleName, 6, "This is to inform you that Payslip Payment for the period of ") & " <B>" & objPeriod._Period_Name & "</B>" & _
                                                Language.getMessage(mstrModuleName, 11, " for ") & intEmpCount.ToString & Language.getMessage(mstrModuleName, 7, " employees have been Disapproved by") & " <B>" & strApproverName & "</B> " & Language.getMessage(mstrModuleName, 8, ". Kindly take a note of it.")
                    'Gajanan [27-Mar-2019] -- End

                    'Sohail (18 Feb 2014) -- End

                ElseIf blnFinalApproved = True Then

                    'Sohail (17 Dec 2014) -- Start
                    'Enhancement - Provide Link for Payroll approval/Authorize Payslip Payment.
                    strLink = strArutiSelfServiceURL & "/Payroll/wPg_AuthorizedPayment.aspx?" & HttpUtility.UrlEncode(clsCrypto.Encrypt(intCompanyUnkId.ToString & "|" & CInt(strApproverID).ToString & "|1|" & intPeriodId.ToString & "|" & intCurrencyUnkID.ToString))
                    'Sohail (17 Dec 2014) -- End

                    objMail._Subject = Language.getMessage(mstrModuleName, 17, "Reminder for Authorizing Payslip Payment")

                    'Sohail (18 Feb 2014) -- Start
                    'Enhancement - AGKN
                    'strMessage &= "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" & Language.getMessage(mstrModuleName, 10, "This is the reminder for authorizing Payslip Payment for the period of ") & " <B>" & objPeriod._Period_Name & "</B>" & _
                    '                       Language.getMessage(mstrModuleName, 11, " for ") & strArrayEmpIDs.Length & Language.getMessage(mstrModuleName, 13, " employees.") 'for the employees listed below.

                    'Gajanan [27-Mar-2019] -- Start
                    'Enhancement - Change Email Language

                    'strMessage &= "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" & Language.getMessage(mstrModuleName, 10, "This is the reminder for authorizing Payslip Payment for the period of ") & " <B>" & objPeriod._Period_Name & "</B>" & _
                    '                       Language.getMessage(mstrModuleName, 11, " for ") & intEmpCount.ToString & Language.getMessage(mstrModuleName, 13, " employees.") 'for the employees listed below.

                    strMessage &= " " & Language.getMessage(mstrModuleName, 10, "This is the reminder for authorizing Payslip Payment for the period of ") & " <B>" & objPeriod._Period_Name & "</B>" & _
                                           Language.getMessage(mstrModuleName, 11, " for ") & intEmpCount.ToString & Language.getMessage(mstrModuleName, 13, " employees.") 'for the employees listed below.
                    'Gajanan [27-Mar-2019] -- End

                    'Sohail (18 Feb 2014) -- End

                    'Sohail (06 Mar 2013) -- Start
                    'TRA - ENHANCEMENT

                    'Gajanan [27-Mar-2019] -- Start
                    'Enhancement - Change Email Language

                    'strMessage &= "&nbsp;" & Language.getMessage(mstrModuleName, 27, "This Payment has been final approved by") & " <B>" & strApproverName & "</B>."
                    strMessage &= " " & Language.getMessage(mstrModuleName, 27, "This Payment has been final approved by") & " <B>" & strApproverName & "</B>."

                    'Gajanan [27-Mar-2019] -- End

                    'Sohail (17 Dec 2014) -- Start
                    'Enhancement - Provide Link for Payroll approval/Authorize Payslip Payment.
                    'strMessage &= "&nbsp;" & Language.getMessage(mstrModuleName, 23, "So please login to Aruti HRMS System to Authorize/Unauthorize the payment.")

                    'Gajanan [27-Mar-2019] -- Start
                    'Enhancement - Change Email Language

                    'strMessage &= "<BR></BR><BR></BR>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" & Language.getMessage(mstrModuleName, 23, "Please click on the following link to Authorize/Unauthorize the payment.")
                    'strMessage &= "<BR></BR>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href='" & strLink & "'>" & strLink & "</a>"

                    strMessage &= "<BR></BR><BR></BR>" & Language.getMessage(mstrModuleName, 23, "Please click on the following link to Authorize/Unauthorize the payment.")
                    strMessage &= "<BR></BR><a href='" & strLink & "'>" & strLink & "</a>"
                    'Gajanan [27-Mar-2019] -- End
                    'Sohail (17 Dec 2014) -- End
                    'Sohail (06 Mar 2013) -- End

                Else

                    'Sohail (17 Dec 2014) -- Start
                    'Enhancement - Provide Link for Payroll approval/Authorize Payslip Payment.
                    strLink = strArutiSelfServiceURL & "/Payroll/wPg_Paymentapprovedisapprove.aspx?" & HttpUtility.UrlEncode(clsCrypto.Encrypt(intCompanyUnkId.ToString & "|" & CInt(strApproverID).ToString & "|1|" & intPeriodId.ToString & "|" & intCurrencyUnkID.ToString))
                    'Sohail (17 Dec 2014) -- End

                    objMail._Subject = Language.getMessage(mstrModuleName, 9, "Reminder for approving Payslip Payment")

                    'Sohail (18 Feb 2014) -- Start
                    'Enhancement - AGKN
                    'strMessage &= "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" & Language.getMessage(mstrModuleName, 18, "This is the reminder for approving Payslip Payment for the period of ") & " <B>" & objPeriod._Period_Name & "</B>" & _
                    '                       Language.getMessage(mstrModuleName, 11, " for ") & strArrayEmpIDs.Length & Language.getMessage(mstrModuleName, 13, " employees.") 'for the employees listed below.


                    'Gajanan [27-Mar-2019] -- Start
                    'Enhancement - Change Email Language


                    'strMessage &= "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" & Language.getMessage(mstrModuleName, 18, "This is the reminder for approving Payslip Payment for the period of ") & " <B>" & objPeriod._Period_Name & "</B>" & _
                    '                       Language.getMessage(mstrModuleName, 11, " for ") & intEmpCount.ToString & Language.getMessage(mstrModuleName, 13, " employees.") 'for the employees listed below.


                    strMessage &= " " & Language.getMessage(mstrModuleName, 18, "This is the reminder for approving Payslip Payment for the period of ") & " <B>" & objPeriod._Period_Name & "</B>" & _
                                           Language.getMessage(mstrModuleName, 11, " for ") & intEmpCount.ToString & Language.getMessage(mstrModuleName, 13, " employees.") 'for the employees listed below.

                    'Gajanan [27-Mar-2019] -- End

                    'Sohail (18 Feb 2014) -- End

                    'Sohail (06 Mar 2013) -- Start
                    'TRA - ENHANCEMENT
                    If blnFromPayment = True Then
                        strMessage &= "&nbsp;" & Language.getMessage(mstrModuleName, 24, "This Payment has been done by") & " <B>" & strApproverName & "</B>."
                    Else
                        strMessage &= "&nbsp;" & Language.getMessage(mstrModuleName, 22, "This Payment has been approved by") & " <B>" & strApproverName & "</B>."
                    End If
                    'Sohail (17 Dec 2014) -- Start
                    'Enhancement - Provide Link for Payroll approval/Authorize Payslip Payment.
                    'strMessage &= "&nbsp;" & Language.getMessage(mstrModuleName, 25, "So please login to Aruti HRMS System to Approve/Reject the payment.")

                    'Gajanan [27-Mar-2019] -- Start
                    'Enhancement - Change Email Language

                    'strMessage &= "<BR></BR><BR></BR>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" & Language.getMessage(mstrModuleName, 25, "Please click on the following link to Approve/Reject the payment.")
                    'strMessage &= "<BR></BR>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href='" & strLink & "'>" & strLink & "</a>"

                    strMessage &= "<BR></BR><BR></BR>" & Language.getMessage(mstrModuleName, 25, "Please click on the following link to Approve/Reject the payment.")
                    strMessage &= "<BR></BR><a href='" & strLink & "'>" & strLink & "</a>"
                    'Gajanan [27-Mar-2019] -- End

                    'Sohail (17 Dec 2014) -- End
                    'Sohail (06 Mar 2013) -- End

                End If

                strMessage &= "<BR><BR>"

                'Gajanan [27-Mar-2019] -- Start
                'Enhancement - Change Email Language
                'strMessage &= "<TABLE border='1' style='margin-left:50px;width:502px;'>"
                strMessage &= "<TABLE border='1' style='width:502px;'>"
                'Gajanan [27-Mar-2019] -- End


                strMessage &= "<TR style='background-color:Purple;color:White;'>"

                strMessage &= "<TD align='center' style='width:300px'>"
                strMessage &= Language.getMessage(mstrModuleName, 19, "Particulars")
                strMessage &= "</TD>"

                strMessage &= "<TD align='center' style='width:200px'>"
                strMessage &= Language.getMessage(mstrModuleName, 20, "Emp. Count")
                strMessage &= "</TD>"

                strMessage &= "<TD align='center' style='width:200px'>"
                strMessage &= Language.getMessage(mstrModuleName, 21, "Amount") & " (" & strCurrencySign & ")"
                strMessage &= "</TD>"

                strMessage &= "</TR>"
                '-----------------------------

                strMessage &= "<TR>"
                strMessage &= "<TD style='width:300px'>"
                strMessage &= Language.getMessage(mstrModuleName, 14, "Total Bank Payment")
                strMessage &= "</TD>"

                strMessage &= "<TD align='center' style='width:200px'>"
                'Sohail (18 Feb 2014) -- Start
                'Enhancement - AGKN
                'strMessage &= intBankPaid.ToString
                strMessage &= int_BankPaid.ToString
                'Sohail (18 Feb 2014) -- End
                strMessage &= "</TD>"

                strMessage &= "<TD align='right' style='width:200px'>"
                'Sohail (18 May 2013) -- Start
                'TRA - ENHANCEMENT
                'strMessage &= decBankAmount.ToString(GUI.fmtCurrency)
                'Sohail (18 Feb 2014) -- Start
                'Enhancement - AGKN
                'strMessage &= decBankAmount.ToString(strFmtCurrency)
                strMessage &= dec_BankAmount.ToString(strFmtCurrency)
                'Sohail (18 Feb 2014) -- End
                'Sohail (18 May 2013) -- End
                strMessage &= "</TD>"

                strMessage &= "</TR>"

                strMessage &= "<TR>"
                strMessage &= "<TD style='width:300px'>"
                strMessage &= Language.getMessage(mstrModuleName, 15, "Total Cash Payment")
                strMessage &= "</TD>"

                strMessage &= "<TD width='200px' align='center'>"
                'Sohail (18 Feb 2014) -- Start
                'Enhancement - AGKN
                'strMessage &= intCashPaid.ToString
                strMessage &= int_CashPaid.ToString
                'Sohail (18 Feb 2014) -- End
                strMessage &= "</TD>"

                strMessage &= "<TD width='200px' align='right'>"
                'Sohail (18 May 2013) -- Start
                'TRA - ENHANCEMENT
                'strMessage &= decCashAmount.ToString(GUI.fmtCurrency)
                'Sohail (18 Feb 2014) -- Start
                'Enhancement - AGKN
                'strMessage &= decCashAmount.ToString(strFmtCurrency)
                strMessage &= dec_CashAmount.ToString(strFmtCurrency)
                'Sohail (18 Feb 2014) -- End
                'Sohail (18 May 2013) -- End
                strMessage &= "</TD>"

                strMessage &= "</TR>"

                strMessage &= "<TR>"
                strMessage &= "<TD style='width:300px'>"
                strMessage &= Language.getMessage(mstrModuleName, 16, "Total Payment")
                strMessage &= "</TD>"

                strMessage &= "<TD align='center' style='width:200px'>"
                'Sohail (18 Feb 2014) -- Start
                'Enhancement - AGKN
                'strMessage &= intTotalPaid.ToString
                strMessage &= (int_BankPaid + int_CashPaid).ToString
                'Sohail (18 Feb 2014) -- End
                strMessage &= "</TD>"

                strMessage &= "<TD align='right' style='width:200px'>"
                'Sohail (18 May 2013) -- Start
                'TRA - ENHANCEMENT
                'strMessage &= decTotalAmount.ToString(GUI.fmtCurrency)
                'Sohail (18 Feb 2014) -- Start
                'Enhancement - AGKN
                'strMessage &= decTotalAmount.ToString(strFmtCurrency)
                strMessage &= (dec_BankAmount + dec_CashAmount).ToString(strFmtCurrency)
                'Sohail (18 Feb 2014) -- End

                'Sohail (18 May 2013) -- End
                strMessage &= "</TD>"

                strMessage &= "</TR>"

                'Sohail (06 Mar 2013) -- Start
                'TRA - ENHANCEMENT
                strMessage &= "<TR>"
                strMessage &= "<TD style='width:300px'>"
                strMessage &= strApproverName & " " & Language.getMessage(mstrModuleName, 26, "Comments")
                strMessage &= "</TD>"

                strMessage &= "<TD colspan='2' >"
                'Sohail (17 Dec 2014) -- Start
                'Enhancement - Provide Link for Payroll approval/Authorize Payslip Payment.
                'strMessage &= strRemarks
                If strRemarks.Trim <> "" Then
                strMessage &= strRemarks
                Else
                    strMessage &= "&nbsp;"
                End If
                'Sohail (17 Dec 2014) -- End
                strMessage &= "</TD>"

                strMessage &= "</TR>"
                'Sohail (06 Mar 2013) -- End

                strMessage &= "</TABLE>"

                'strMessage &= "<BR><BR>"
                'strMessage &= "<TABLE border='1' width='90%' style='margin-left:5%;'>"
                'strMessage &= "<TR style='background-color:Purple;color:White;'>"

                'strMessage &= "<TD width='15%'>"
                'strMessage &= "Emp. Code"
                'strMessage &= "</TD>"

                'strMessage &= "<TD width='40%'>"
                'strMessage &= "Emp. Name"
                'strMessage &= "</TD>"

                'strMessage &= "<TD width='20%'>"
                'strMessage &= "Department"
                'strMessage &= "</TD>"

                'strMessage &= "<TD width='20%'>"
                'strMessage &= "Job Title"
                'strMessage &= "</TD>"

                'strMessage &= "</TR>"

                'strMessage &= "</TABLE>"


                'For Each strEmpId As String In strArrayEmpIDs
                '    dsList = objEmp.GetList("Employee", False, , , , CInt(strEmpId))

                '    If dsList.Tables("Employee").Rows.Count > 0 Then
                '        strDIV &= "<TR>"

                '        strDIV &= "<TD width='15%'>"
                '        strDIV &= dsList.Tables("Employee").Rows(0).Item("employeecode").ToString
                '        strDIV &= "</TD>"

                '        strDIV &= "<TD width='40%'>"
                '        strDIV &= dsList.Tables("Employee").Rows(0).Item("name").ToString
                '        strDIV &= "</TD>"

                '        strDIV &= "<TD width='20%'>"
                '        strDIV &= dsList.Tables("Employee").Rows(0).Item("DeptName").ToString
                '        strDIV &= "</TD>"

                '        strDIV &= "<TD width='20%'>"
                '        strDIV &= dsList.Tables("Employee").Rows(0).Item("job_name").ToString
                '        strDIV &= "</TD>"

                '        strDIV &= "</TR>"
                '    End If

                'Next

                'If strDIV.Trim <> "" Then
                '    strMessage &= "<DIV style='height:450px;overflow:auto;margin-left:5%;width:90%;'>"
                '    strMessage &= "<TABLE border='1' style='width:90%;'>"
                '    strMessage &= strDIV
                '    strMessage &= "</TABLE>"
                '    strMessage &= "</DIV>"
                'End If



                'Gajanan [27-Mar-2019] -- Start
                'Enhancement - Change Email Language
                'strMessage &= "<BR></BR><BR></BR>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;POWERED BY ARUTI HR & PAYROLL MANAGEMENT SOFTWARE"

                strMessage &= "<p><center style='padding-top:25px;font-size:10.0pt;font-family:&quot;Verdana&quot;,&quot;Sans-Serif&quot;;color:#333399;margin-left:0px;margin-right:0px;margin-top:0px;margin-bottom:10px'><b>&quot;POWERED BY ARUTI HR &amp; PAYROLL MANAGEMENT SOFTWARE.&quot;</b></center></p>"

                'Gajanan [27-Mar-2019] -- End

                strMessage &= "</BODY></HTML>"

                objMail._Message = strMessage
                objMail._ToEmail = objUser._Email

                'S.SANDEEP [ 28 JAN 2014 ] -- START
                If iLoginTypeId <= 0 Then iLoginTypeId = enLogin_Mode.DESKTOP
                objMail._FormName = mstrFormName
                objMail._ClientIP = mstrClientIP
                objMail._HostName = mstrHostName
                objMail._FromWeb = mblnIsWeb
                objMail._AuditUserId = mintAuditUserId
objMail._CompanyUnkid = mintCompanyUnkid
                objMail._AuditDate = mdtAuditDate
                objMail._LoginEmployeeunkid = iLoginEmployeeId
                objMail._OperationModeId = iLoginTypeId
                objMail._UserUnkid = intApproverUserUnkid
                Dim objUsr As New clsUserAddEdit
                objUsr._Userunkid = intApproverUserUnkid
                objMail._SenderAddress = IIf(objUsr._Email = "", objUsr._Firstname & " " & objUsr._Lastname, objUsr._Email)
                'objUsr = Nothing
                objMail._ModuleRefId = clsSendMail.enAT_VIEW_TYPE.PAYROLL_MGT
                'S.SANDEEP [ 28 JAN 2014 ] -- END

                'Sohail (10 Mar 2014) -- Start
                'Enhancement - Send Emails in Application Backgropund.
                'objMail.SendMail()
                'Sohail (21 Apr 2014) -- Start
                'Enhancement - Salary Distribution by Amount and bank priority.
                'gobjEmailList.Add(New clsEmailCollection(objMail._ToEmail, objMail._Subject, objMail._Message))
                gobjEmailList.Add(New clsEmailCollection(objMail._ToEmail, objMail._Subject, objMail._Message, mstrWebFrmName, iLoginEmployeeId, "", "", intApproverUserUnkid, iLoginTypeId, clsSendMail.enAT_VIEW_TYPE.PAYROLL_MGT, IIf(objUsr._Email = "", objUsr._Firstname & " " & objUsr._Lastname, objUsr._Email)))
                objUsr = Nothing
                'Sohail (21 Apr 2014) -- End
                'Sohail (10 Mar 2014) -- End
            Next

            'Sohail (10 Mar 2014) -- Start
            'Enhancement - Send Emails in Application Backgropund.
            'Sohail (17 Dec 2014) -- Start
            'Enhancement - Provide Link for Payroll approval/Authorize Payslip Payment.
            'trd = New Thread(AddressOf Send_Notification)
            'trd.IsBackground = True
            'trd.Start()
            If HttpContext.Current Is Nothing Then
            trd = New Thread(AddressOf Send_Notification)
            trd.IsBackground = True
                'Sohail (30 Nov 2017) -- Start
                'SUMATRA Enhancement – SUMATRA – issue # 0001669: Additional feature for Exchange Server configuration setting in 70.1.
                'trd.Start()
                Dim arr(1) As Object
                arr(0) = intCompanyUnkId
                trd.Start(arr)
                'Sohail (30 Nov 2017) -- End
            Else
                'Sohail (30 Nov 2017) -- Start
                'SUMATRA Enhancement – SUMATRA – issue # 0001669: Additional feature for Exchange Server configuration setting in 70.1.
                'Call Send_Notification()
                Call Send_Notification(intCompanyUnkId)
                'Sohail (30 Nov 2017) -- End
            End If
            
            'Sohail (17 Dec 2014) -- End
            'Sohail (10 Mar 2014) -- End

        Catch ex As Exception
            'DisplayError.Show("-1", ex.Message, "SendMailToApprover", mstrModuleName)
            Throw New Exception(ex.Message & "; Procedure Name: SendMailToApprover; Module Name: " & mstrModuleName)
        Finally
            objApproverLevel = Nothing
            objApproverMap = Nothing
            objUser = Nothing
            objPeriod = Nothing
            objEmp = Nothing
            objMail = Nothing
        End Try
    End Sub
    'Sohail (20 Feb 2013) -- End

    'Sohail (10 Mar 2014) -- Start
    'Enhancement - Send Emails in Application Backgropund.
    Private Sub Send_Notification(ByVal intCompanyUnkId As Object)
        'Sohail (30 Nov 2017) - [intCompanyUnkId]
        Try
            If gobjEmailList.Count > 0 Then

                Dim objSendMail As New clsSendMail
                For Each obj In gobjEmailList
                    objSendMail._ToEmail = obj._EmailTo
                    objSendMail._Subject = obj._Subject
                    objSendMail._Message = obj._Message
                    objSendMail._FormName = obj._FormName
                    objSendMail._LoginEmployeeunkid = obj._LogEmployeeUnkid
                    objSendMail._OperationModeId = obj._OperationModeId
                    objSendMail._UserUnkid = obj._UserUnkid
                    objSendMail._SenderAddress = obj._SenderAddress
                    objSendMail._ModuleRefId = obj._ModuleRefId
                    objSendMail._ClientIP = mstrClientIP
                    objSendMail._HostName = mstrHostName
                    objSendMail._FromWeb = mblnIsWeb
                    objSendMail._AuditUserId = mintAuditUserId
objSendMail._CompanyUnkid = mintCompanyUnkid
                    objSendMail._AuditDate = mdtAuditDate
                    Try
                        'Sohail (30 Nov 2017) -- Start
                        'SUMATRA Enhancement – SUMATRA – issue # 0001669: Additional feature for Exchange Server configuration setting in 70.1.
                        'objSendMail.SendMail()
                        'Sohail (13 Dec 2017) -- Start
                        'SUMATRA Enhancement – SUMATRA – issue # 0001669: Additional feature for Exchange Server configuration setting in 70.1.
                        'objSendMail.SendMail(intCompanyUnkId)
                        If TypeOf intCompanyUnkId Is Integer Then
                            objSendMail.SendMail(CInt(intCompanyUnkId))
                        Else
                            objSendMail.SendMail(CInt(intCompanyUnkId(0)))
                        End If
                        'Sohail (13 Dec 2017) -- End
                        'Sohail (30 Nov 2017) -- End
                    Catch ex As Exception

                    End Try
                Next
                gobjEmailList.Clear()
            End If
        Catch ex As Exception
            Throw New Exception(mstrModuleName & ":Send_Notification:- " & ex.Message)
        Finally
            If gobjEmailList.Count > 0 Then
                gobjEmailList.Clear()
            End If
        End Try
    End Sub
    'Sohail (10 Mar 2014) -- End

    'Sohail (17 Dec 2014) -- Start
    'Enhancement - Provide Link for Payroll approval/Authorize Payslip Payment.
    Public Function isExistPaymentAuthorization(ByVal strPaymenttranunkIDs As String, ByVal bnlIsAuthorized As Boolean) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        Dim objDataOperation As New clsDataOperation

        Try
            strQ = "SELECT " & _
                  "  1 " & _
                 "FROM prpayment_tran " & _
                 "WHERE ISNULL(isvoid, 0) = 0 " & _
                 "AND paymenttranunkid IN ( " & strPaymenttranunkIDs & " ) " & _
                 "AND isauthorized = @isauthorized "

            objDataOperation.AddParameter("@isauthorized", SqlDbType.Bit, eZeeDataType.BIT_SIZE, bnlIsAuthorized)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isExistPaymentAuthorization; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            'objDataOperation = Nothing
        End Try
    End Function
    'Sohail (17 Dec 2014) -- End

    'Sohail (15 Dec 2010) -- Start

'Pinkal (13-Jan-2015) -- Start
    'Enhancement - CHANGES FOR VOLTAMP GIVE WARNING IN LEAVE & TNA TRANSACTION WHEN PAYMENT IS DONE AND PERIOD IS OPEN. 

    Public Function GetTotalPaymentCountWithinTnAPeriod(ByVal intPayRefId As Integer, _
                                       ByVal intPaymentTypeId As Integer, _
                                       ByVal intEmployeeID As Integer, _
                                       ByVal mdtTnAPeriodStartDate As Date, _
                                       Optional ByVal intPeriodId As Integer = 0, _
                                       Optional ByVal mdtDate As DateTime = Nothing, _
                                       Optional ByVal strFilterQuery As String = "", _
                                       Optional ByVal xDataOperation As clsDataOperation = Nothing, _
                                       Optional ByVal blnConsiderLeaveOnTnAPeriod As Boolean = True) As Integer


        'Pinkal (01-Jan-2019) -- Enhancement [Ref #: 0003287] - Working on Leave Frequency Change for Voltamp.[Optional ByVal blnConsiderLeaveOnTnAPeriod As Boolean = True]


        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        'S.SANDEEP [19 OCT 2016] -- START
        'ISSUE : EMPLOYEE MASTER BIND TRANSACTION ISSUE
        'objDataOperation = New clsDataOperation
        If xDataOperation IsNot Nothing Then
            objDataOperation = xDataOperation
        Else
        objDataOperation = New clsDataOperation
        End If
        objDataOperation.ClearParameters()
        'S.SANDEEP [19 OCT 2016] -- END

        Try

            strQ = "SELECT COUNT(paymenttranunkid) AS TotalCount " & _
                    "FROM prpayment_tran "

            If intPeriodId > 0 Then
                strQ &= " JOIN cfcommon_period_tran ON cfcommon_period_tran.periodunkid = prpayment_tran.periodunkid "
            End If

            strQ &= "WHERE ISNULL(prpayment_tran.isvoid,0) = 0 " & _
                    "AND prpayment_tran.referenceid = @referenceid " & _
                    "AND prpayment_tran.paytypeid = @paytypeid " & _
                    "AND prpayment_tran.employeeunkid = @employeeunkid "


            If intPeriodId > 0 Then
                strQ &= " AND prpayment_tran.periodunkid = @periodunkid "
                objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intPeriodId)
            End If

            If intPeriodId > 0 AndAlso mdtDate <> Nothing Then

                'Pinkal (01-Jan-2019) -- Start
                'Enhancement [Ref #: 0003287] - Working on Leave Frequency Change for Voltamp.
                If blnConsiderLeaveOnTnAPeriod Then
                strQ &= " AND @date BETWEEN  @tnaStartDate  AND CONVERT(CHAR(8),tna_enddate,112) "
                Else
                    strQ &= " AND @date BETWEEN  @tnaStartDate  AND CONVERT(CHAR(8),end_date,112) "
                End If
                'Pinkal (01-Jan-2019) -- End

                objDataOperation.AddParameter("@tnaStartDate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(mdtTnAPeriodStartDate))
                objDataOperation.AddParameter("@date", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(mdtDate))
            End If

            If strFilterQuery.Trim <> "" Then
                strQ &= " AND " & strFilterQuery
            End If

            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intEmployeeID)
            objDataOperation.AddParameter("@referenceid", SqlDbType.Int, eZeeDataType.INT_SIZE, intPayRefId)
            objDataOperation.AddParameter("@paytypeid", SqlDbType.Int, eZeeDataType.INT_SIZE, intPaymentTypeId)

            dsList = objDataOperation.ExecQuery(strQ, "PaidCount")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables("PaidCount").Rows(0).Item("TotalCount")

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetTotalPaymentCountWithinTnAPeriod; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            'S.SANDEEP [19 OCT 2016] -- START
            'ISSUE : EMPLOYEE MASTER BIND TRANSACTION ISSUE
            'objDataOperation = Nothing
            If xDataOperation Is Nothing Then objDataOperation = Nothing
            'S.SANDEEP [19 OCT 2016] -- END
        End Try
    End Function

    'Pinkal (13-Jan-2015) -- End
#Region " Messages "
    '1, "This Voucher No. is already defined. Please define new Voucher No."
    '2, "Voucher No. '" & mstrVoucherno & "' is already defined. Please define new Voucher No. from : Aruti Configuration -> Option -> Payment Voc# No."
#End Region
    'Sohail (15 Dec 2010) -- End

	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Public Shared Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "This Voucher No. is already defined. Please define new Voucher No.")
			Language.setMessage(mstrModuleName, 2, "WEB")
			Language.setMessage(mstrModuleName, 3, "Voucher No. '")
			Language.setMessage(mstrModuleName, 4, "' is already defined. Please define new Voucher No. from : Aruti Configuration -> Option -> Payment Voc# No.")
			Language.setMessage(mstrModuleName, 5, "Payslip Payment Disapproved for")
			Language.setMessage(mstrModuleName, 6, "This is to inform you that Payslip Payment for the period of")
			Language.setMessage(mstrModuleName, 7, " employees have been Disapproved by")
			Language.setMessage(mstrModuleName, 8, ". Kindly take a note of it.")
			Language.setMessage(mstrModuleName, 9, "Reminder for approving Payslip Payment")
			Language.setMessage(mstrModuleName, 10, "This is the reminder for authorizing Payslip Payment for the period of")
			Language.setMessage(mstrModuleName, 11, " for")
			Language.setMessage(mstrModuleName, 12, "Dear")
			Language.setMessage(mstrModuleName, 13, " employees.")
			Language.setMessage(mstrModuleName, 14, "Total Bank Payment")
			Language.setMessage(mstrModuleName, 15, "Total Cash Payment")
			Language.setMessage(mstrModuleName, 16, "Total Payment")
			Language.setMessage(mstrModuleName, 17, "Reminder for Authorizing Payslip Payment")
			Language.setMessage(mstrModuleName, 18, "This is the reminder for approving Payslip Payment for the period of")
			Language.setMessage(mstrModuleName, 19, "Particulars")
			Language.setMessage(mstrModuleName, 20, "Emp. Count")
			Language.setMessage(mstrModuleName, 21, "Amount")
			Language.setMessage(mstrModuleName, 22, "This Payment has been approved by")
			Language.setMessage(mstrModuleName, 23, "Please click on the following link to Authorize/Unauthorize the payment.")
			Language.setMessage(mstrModuleName, 24, "This Payment has been done by")
			Language.setMessage(mstrModuleName, 25, "Please click on the following link to Approve/Reject the payment.")
			Language.setMessage(mstrModuleName, 26, "Comments")
			Language.setMessage(mstrModuleName, 27, "This Payment has been final approved by")
			Language.setMessage(mstrModuleName, 28, "Select")
			Language.setMessage(mstrModuleName, 29, "Sorry, Payment is already done.")
			Language.setMessage(mstrModuleName, 30, "Sorry, Payment is already done for the employee")
			Language.setMessage(mstrModuleName, 31, "Sorry, Payment Authorization is already done for some of the selected payments. Please Re-select Period and process again.")
			Language.setMessage(mstrModuleName, 32, "Sorry, You cannot delete this transaction. Please delete last transaction from the screen of")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class