﻿'************************************************************************************************************************************
'Class Name : clsPayment_authorize_tran.vb
'Purpose    :
'Date       :03/07/2012
'Written By :Sohail
'Modified   :
'************************************************************************************************************************************

Imports eZeeCommonLib
'S.SANDEEP [ 11 AUG 2012 ] -- START
'ENHANCEMENT : TRA CHANGES
Imports eZee.Common.eZeeForm
'S.SANDEEP [ 11 AUG 2012 ] -- END

''' <summary>
''' Purpose: 
''' Developer: Sohail
''' </summary>
Public Class clsPayment_authorize_tran
    Private Shared ReadOnly mstrModuleName As String = "clsprpayment_authorize_tran"
    Dim objDataOperation As clsDataOperation
    Dim mstrMessage As String = ""


#Region " Private variables "
    Private mintPaymentauthorizetranunkid As Integer
    Private mintPaymenttranunkid As Integer
    Private mdtAuthorize_Date As Date
    Private mblnIsauthorized As Boolean
    Private mintUserunkid As Integer
    Private mblnIsactive As Boolean = True
    Private mstrRemarks As String = String.Empty 'Sohail (27 Feb 2013)

    'Sohail (27 Jul 2012) -- Start
    'TRA - ENHANCEMENT
    Private mintVoiduserunkid As Integer = 0
    Private mdtVoiddatetime As Date = Nothing
    Private mstrVoidreason As String = String.Empty
    Private mstrWebFormName As String = String.Empty
    'Sohail (27 Jul 2012) -- End

    'Sohail (18 May 2013) -- Start
    'TRA - ENHANCEMENT
    Private mstrWebIP As String = ""
    Private mstrWebhostName As String = ""
    'Sohail (18 May 2013) -- End

    'S.SANDEEP [ 13 AUG 2012 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Private mintLogEmployeeUnkid As Integer = -1
    'S.SANDEEP [ 13 AUG 2012 ] -- END

#End Region

#Region " Properties "

    'S.SANDEEP [28-May-2018] -- START 
    'ISSUE/ENHANCEMENT : {Audit Trails} 
    Private mstrFormName As String = String.Empty
    Public WriteOnly Property _FormName() As String
        Set(ByVal value As String)
            mstrFormName = value
        End Set
    End Property

    Private mstrClientIP As String = ""
    Public WriteOnly Property _ClientIP() As String
        Set(ByVal value As String)
            mstrClientIP = value
        End Set
    End Property

    Private mstrHostName As String = ""
    Public WriteOnly Property _HostName() As String
        Set(ByVal value As String)
            mstrHostName = value
        End Set
    End Property

    Private mblnIsWeb As Boolean = False
    Public WriteOnly Property _FromWeb() As Boolean
        Set(ByVal value As Boolean)
            mblnIsWeb = value
        End Set
    End Property

    Private mintAuditUserId As Integer = 0
    Public WriteOnly Property _AuditUserId() As Integer
        Set(ByVal value As Integer)
            mintAuditUserId = value
        End Set
    End Property

    Private mintCompanyUnkid As Integer = 0
    Public WriteOnly Property _CompanyUnkid() As Integer
        Set(ByVal value As Integer)
            mintCompanyUnkid = value
        End Set
    End Property

    Private mdtAuditDate As DateTime = Now
    Public WriteOnly Property _AuditDate() As DateTime
        Set(ByVal value As DateTime)
            mdtAuditDate = value
        End Set
    End Property

    Private xDataOpr As clsDataOperation = Nothing
    Public Property _xDataOpr() As clsDataOperation
        Get
            Return xDataOpr
        End Get
        Set(ByVal value As clsDataOperation)
            xDataOpr = value
        End Set
    End Property

    Private mintVoidlogingemployeeunkid As Integer = 0
    Public Property _Voidloginemployeeunkid() As Integer
        Get
            Return mintVoidlogingemployeeunkid
        End Get
        Set(ByVal value As Integer)
            mintVoidlogingemployeeunkid = value
        End Set
    End Property
    'S.SANDEEP [28-May-2018] -- END 

    Public ReadOnly Property _Message() As String
        Get
            Return mstrMessage
        End Get
    End Property

    ''' <summary>
    ''' Purpose: Get or Set paymentauthorizetranunkid
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Paymentauthorizetranunkid() As Integer
        Get
            Return mintPaymentauthorizetranunkid
        End Get
        Set(ByVal value As Integer)
            mintPaymentauthorizetranunkid = Value
            Call getData()
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set paymenttranunkid
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Paymenttranunkid() As Integer
        Get
            Return mintPaymenttranunkid
        End Get
        Set(ByVal value As Integer)
            mintPaymenttranunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set authorize_date
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Authorize_Date() As Date
        Get
            Return mdtAuthorize_Date
        End Get
        Set(ByVal value As Date)
            mdtAuthorize_Date = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isauthorized
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Isauthorized() As Boolean
        Get
            Return mblnIsauthorized
        End Get
        Set(ByVal value As Boolean)
            mblnIsauthorized = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set userunkid
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Userunkid() As Integer
        Get
            Return mintUserunkid
        End Get
        Set(ByVal value As Integer)
            mintUserunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isactive
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Isactive() As Boolean
        Get
            Return mblnIsactive
        End Get
        Set(ByVal value As Boolean)
            mblnIsactive = Value
        End Set
    End Property

    'Sohail (27 Feb 2013) -- Start
    'TRA - ENHANCEMENT
    ''' <summary>
    ''' Purpose: Get or Set remark
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Remarks() As String
        Get
            Return mstrRemarks
        End Get
        Set(ByVal value As String)
            mstrRemarks = value
        End Set
    End Property
    'Sohail (27 Feb 2013) -- End

    'Sohail (27 Jul 2012) -- Start
    'TRA - ENHANCEMENT
    ''' <summary>
    ''' Purpose: Get or Set voiduserunkid
    ''' Modify By: Suhail
    ''' </summary>
    Public Property _Voiduserunkid() As Integer
        Get
            Return mintVoiduserunkid
        End Get
        Set(ByVal value As Integer)
            mintVoiduserunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiddatetime
    ''' Modify By: Suhail
    ''' </summary>
    Public Property _Voiddatetime() As Date
        Get
            Return mdtVoiddatetime
        End Get
        Set(ByVal value As Date)
            mdtVoiddatetime = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voidreason
    ''' Modify By: Suhail
    ''' </summary>
    Public Property _Voidreason() As String
        Get
            Return mstrVoidreason
        End Get
        Set(ByVal value As String)
            mstrVoidreason = Value
        End Set
    End Property

    'Public Property _WebFormName() As String
    '    Get
    '        Return mstrWebFormName
    '    End Get
    '    Set(ByVal value As String)
    '        mstrWebFormName = value
    '    End Set
    'End Property

    'Public WriteOnly Property _WebIP() As String
    '    Set(ByVal value As String)
    '        mstrWebIP = value
    '    End Set
    'End Property

    'Public WriteOnly Property _WebHostName() As String
    '    Set(ByVal value As String)
    '        mstrWebhostName = value
    '    End Set
    'End Property

    'S.SANDEEP [ 13 AUG 2012 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Public WriteOnly Property _LoginEmployeeUnkid() As Integer
        Set(ByVal value As Integer)
            mintLogEmployeeUnkid = value
        End Set
    End Property
    'S.SANDEEP [ 13 AUG 2012 ] -- END

#End Region

    ''' <summary>
    ''' Modify By: Sohail
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Sub GetData()
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "SELECT " & _
              "  paymentauthorizetranunkid " & _
              ", paymenttranunkid " & _
              ", authorize_date " & _
              ", isauthorized " & _
              ", userunkid " & _
              ", isactive " & _
              ", voiduserunkid " & _
              ", voiddatetime " & _
              ", voidreason " & _
              ", ISNULL(remarks, '') AS remarks " & _
             "FROM prpayment_authorize_tran " & _
             "WHERE paymentauthorizetranunkid = @paymentauthorizetranunkid "
            'Sohail (27 Feb 2013) - [remarks]

            objDataOperation.AddParameter("@paymentauthorizetranunkid", SqlDbType.int, eZeeDataType.INT_SIZE, mintPaymentauthorizeTranUnkId.ToString)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For Each dtRow As DataRow In dsList.Tables(0).Rows
                mintpaymentauthorizetranunkid = CInt(dtRow.Item("paymentauthorizetranunkid"))
                mintpaymenttranunkid = CInt(dtRow.Item("paymenttranunkid"))
                mdtauthorize_date = dtRow.Item("authorize_date")
                mblnIsauthorized = CBool(dtRow.Item("isauthorized"))
                mintuserunkid = CInt(dtRow.Item("userunkid"))
                mblnIsactive = CBool(dtRow.Item("isactive"))
                'Sohail (27 Jul 2012) -- Start
                'TRA - ENHANCEMENT
                mintVoiduserunkid = CInt(dtRow.Item("voiduserunkid"))
                If IsDBNull(dtRow.Item("voiddatetime")) = True Then
                    mdtVoiddatetime = Nothing
                Else
                    mdtVoiddatetime = dtRow.Item("voiddatetime")
                End If
                mstrVoidreason = dtRow.Item("voidreason").ToString
                'Sohail (27 Jul 2012) -- End
                mstrRemarks = dtRow.Item("remarks").ToString 'Sohail (27 Feb 2013)
                Exit For
            Next
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetData; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Sub

    ''' <summary>
    ''' Modify By: Sohail
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function GetList(ByVal strTableName As String, Optional ByVal blnOnlyActive As Boolean = True) As DataSet
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "SELECT " & _
              "  paymentauthorizetranunkid " & _
              ", paymenttranunkid " & _
              ", authorize_date " & _
              ", isauthorized " & _
              ", userunkid " & _
              ", isactive " & _
              ", voiduserunkid " & _
              ", voiddatetime " & _
              ", voidreason " & _
              ", ISNULL(remarks, '') AS remarks " & _
             "FROM prpayment_authorize_tran "
            'Sohail (27 Feb 2013) - [remarks]

            If blnOnlyActive Then
                strQ &= " WHERE isactive = 1 "
            End If

            dsList = objDataOperation.ExecQuery(strQ, strTableName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
        Return dsList
    End Function

    'Sohail (11 Mar 2013) -- Start
    'TRA - ENHANCEMENT
    Public Function GetAuthorizedTotalCount(ByVal xDatabaseName As String _
                                            , ByVal xUserUnkid As Integer _
                                            , ByVal intYearID As Integer _
                                            , ByVal xCompanyUnkid As Integer _
                                            , ByVal xPeriodStart As DateTime _
                                            , ByVal xPeriodEnd As DateTime _
                                            , ByVal xUserModeSetting As String _
                                            , ByVal xOnlyApproved As Boolean _
                                            , ByVal blnIncludeInActiveEmployee As Boolean _
                                            , ByVal blnApplyUserAccessFilter As Boolean _
                                            , ByVal intPayRefId As Integer _
                                            , ByVal intPaymentTypeId As Integer _
                                            , Optional ByVal intPeriodId As Integer = 0 _
                                            , Optional ByVal strFilterQuery As String = "" _
                                            , Optional ByVal strAdvanceFilter As String = "" _
                                            ) As Integer
        'Sohail (24 Jun 2019) - [xDatabaseName, xUserUnkid, intYearID, xCompanyUnkid, xPeriodStart, xPeriodEnd, xUserModeSetting, xOnlyApproved, blnIncludeInActiveEmployee, blnApplyUserAccessFilter, strAdvanceFilter]

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            'Sohail (24 Jun 2019) -- Start
            'NMB Enhancement - REF # - 76.1 - TC012: There should be payroll authorization check before payroll batch posting to flex cube.
            Dim xDateJoinQry, xDateFilterQry, xUACQry, xUACFiltrQry, xAdvanceJoinQry As String
            xDateJoinQry = "" : xDateFilterQry = "" : xUACQry = "" : xUACFiltrQry = "" : xAdvanceJoinQry = ""
            Call GetDatesFilterString(xDateJoinQry, xDateFilterQry, xPeriodStart, xPeriodEnd, , , xDatabaseName)
            Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, xPeriodEnd, xOnlyApproved, xDatabaseName, xUserUnkid, xCompanyUnkid, intYearID, xUserModeSetting)
            Call GetAdvanceFilterQry(xAdvanceJoinQry, xPeriodEnd, xDatabaseName)

            strQ = "SELECT  hremployee_master.employeeunkid " & _
                        ", hremployee_master.employeecode " & _
                        ", ISNULL(hremployee_master.firstname + ' ' + hremployee_master.othername + ' ' + hremployee_master.surname,'') AS employeename " & _
                        "INTO #tblEmp " & _
                  "FROM    hremployee_master "

            If xDateJoinQry.Trim.Length > 0 Then
                strQ &= xDateJoinQry
            End If

            If blnApplyUserAccessFilter = True Then
                If xUACQry.Trim.Length > 0 Then
                    strQ &= xUACQry
                End If
            End If

            If xAdvanceJoinQry.Trim.Length > 0 Then
                strQ &= xAdvanceJoinQry
            End If

            strQ &= "WHERE 1 = 1 "

            If blnApplyUserAccessFilter = True Then
                If xUACFiltrQry.Trim.Length > 0 Then
                    strQ &= " AND " & xUACFiltrQry
                End If
            End If

            If blnIncludeInActiveEmployee = False Then
                If xDateFilterQry.Trim.Length > 0 Then
                    strQ &= xDateFilterQry
                End If
            End If

            If strAdvanceFilter.Trim <> "" Then
                strQ &= " AND " & strAdvanceFilter & " "
            End If
            'Sohail (24 Jun 2019) -- End

            strQ &= "SELECT COUNT(paymenttranunkid) As TotalCount " & _
                    "FROM prpayment_tran " & _
                    "JOIN #tblEmp AS emp ON prpayment_tran.employeeunkid = emp.employeeunkid " & _
                    "WHERE ISNULL(prpayment_tran.isvoid,0) = 0 " & _
                    "AND  prpayment_tran.isauthorized = 1 " & _
                    "AND prpayment_tran.referenceid = @referenceid " & _
                    "AND prpayment_tran.paytypeid = @paytypeid "
            'Sohail (24 Jun 2019) - [JOIN #tblEmp]
            'strQ &= UserAccessLevel._AccessLevelFilterString

            If intPeriodId > 0 Then
                strQ &= " AND prpayment_tran.periodunkid = @periodunkid "
                objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intPeriodId.ToString)
            End If

            If strFilterQuery.Trim <> "" Then
                strQ &= " AND " & strFilterQuery
            End If

            'Sohail (24 Jun 2019) -- Start
            'NMB Enhancement - REF # - 76.1 - TC012: There should be payroll authorization check before payroll batch posting to flex cube.
            strQ &= " DROP TABLE #tblEmp "
            'Sohail (24 Jun 2019) -- End

            objDataOperation.AddParameter("@referenceid", SqlDbType.Int, eZeeDataType.INT_SIZE, intPayRefId.ToString)
            objDataOperation.AddParameter("@paytypeid", SqlDbType.Int, eZeeDataType.INT_SIZE, intPaymentTypeId.ToString)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables("List").Rows(0).Item("TotalCount")

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetAuthorizedTotalCount; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    Public Function GetDISTINCTauthorizedUsersCount(ByVal intPayRefId As Integer, _
                                                    ByVal intPaymentTypeId As Integer, _
                                                    Optional ByVal intPeriodId As Integer = 0, _
                                                    Optional ByVal intUserUnkId As Integer = 0, _
                                                    Optional ByVal strFilterQuery As String = "" _
                                                    ) As Integer

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try

            strQ = "SELECT   COUNT(DISTINCT prpayment_authorize_tran.userunkid) As TotalCount " & _
                    "FROM    prpayment_tran " & _
                            "LEFT JOIN prpayment_authorize_tran ON prpayment_authorize_tran.paymenttranunkid = prpayment_tran.paymenttranunkid " & _
                            "LEFT JOIN hrmsConfiguration..cfuser_master ON cfuser_master.userunkid = prpayment_authorize_tran.userunkid " & _
                    "WHERE   ISNULL(prpayment_tran.isvoid, 0) = 0 " & _
                            "AND prpayment_authorize_tran.isactive = 1 " & _
                            "AND prpayment_tran.referenceid = @referenceid " & _
                            "AND prpayment_tran.paytypeid = @paytypeid " & _
                            "AND prpayment_tran.isauthorized = 1 "

            'strQ &= UserAccessLevel._AccessLevelFilterString

            If intPeriodId > 0 Then
                strQ &= " AND prpayment_tran.periodunkid = @periodunkid "
                objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intPeriodId)
            End If

            If intUserUnkId > 0 Then
                strQ &= " AND prpayment_authorize_tran.userunkid = @userunkid "
                objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUserUnkId)
            End If

            If strFilterQuery.Trim <> "" Then
                strQ &= " AND " & strFilterQuery
            End If

            objDataOperation.AddParameter("@referenceid", SqlDbType.Int, eZeeDataType.INT_SIZE, intPayRefId.ToString)
            objDataOperation.AddParameter("@paytypeid", SqlDbType.Int, eZeeDataType.INT_SIZE, intPaymentTypeId.ToString)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables("List").Rows(0).Item("TotalCount")

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetDISTINCTauthorizedUsersCount; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try

    End Function

    Public Function GetDISTINCTauthorizedUsers(ByVal strTableName As String, _
                                               ByVal intPayRefId As Integer, _
                                               ByVal intPaymentTypeId As Integer, _
                                               Optional ByVal intPeriodId As Integer = 0, _
                                               Optional ByVal intUserUnkId As Integer = 0, _
                                               Optional ByVal strFilterQuery As String = "" _
                                               ) As DataSet

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try

            strQ = "SELECT   DISTINCT prpayment_authorize_tran.userunkid " & _
                          ", ISNULL(cfuser_master.username, '') AS username " & _
                    "FROM    prpayment_tran " & _
                            "LEFT JOIN prpayment_authorize_tran ON prpayment_authorize_tran.paymenttranunkid = prpayment_tran.paymenttranunkid " & _
                            "LEFT JOIN hrmsConfiguration..cfuser_master ON cfuser_master.userunkid = prpayment_authorize_tran.userunkid " & _
                    "WHERE   ISNULL(prpayment_tran.isvoid, 0) = 0 " & _
                            "AND prpayment_authorize_tran.isactive = 1 " & _
                            "AND prpayment_tran.referenceid = @referenceid " & _
                            "AND prpayment_tran.paytypeid = @paytypeid " & _
                            "AND prpayment_tran.isauthorized = 1 "

            'strQ &= UserAccessLevel._AccessLevelFilterString

            If intPeriodId > 0 Then
                strQ &= " AND prpayment_tran.periodunkid = @periodunkid "
                objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intPeriodId)
            End If

            If intUserUnkId > 0 Then
                strQ &= " AND prpayment_authorize_tran.userunkid = @userunkid "
                objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUserUnkId)
            End If

            If strFilterQuery.Trim <> "" Then
                strQ &= " AND " & strFilterQuery
            End If

            objDataOperation.AddParameter("@referenceid", SqlDbType.Int, eZeeDataType.INT_SIZE, intPayRefId.ToString)
            objDataOperation.AddParameter("@paytypeid", SqlDbType.Int, eZeeDataType.INT_SIZE, intPaymentTypeId.ToString)

            dsList = objDataOperation.ExecQuery(strQ, strTableName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If


        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetDISTINCTauthorizedUsers; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
        Return dsList
    End Function

    Public Function GetLastAuthorizationDetail(ByVal strTableName As String, _
                                               ByVal intPayRefId As Integer, _
                                               ByVal intPaymentTypeId As Integer, _
                                               Optional ByVal intPeriodId As Integer = 0, _
                                               Optional ByVal intUserUnkId As Integer = 0, _
                                               Optional ByVal strFilterQuery As String = "" _
                                               ) As DataSet

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try

            strQ = "SELECT DISTINCT " & _
                            "MAX(prpayment_authorize_tran.authorize_date) AS authorize_date " & _
                          ", prpayment_authorize_tran.userunkid " & _
                          ", ISNULL(cfuser_master.username, '') AS username " & _
                          ", ISNULL(cfuser_master.firstname, '') + ' ' + ISNULL(cfuser_master.lastname, '') AS userfullname " & _
                    "FROM    prpayment_tran " & _
                            "LEFT JOIN prpayment_authorize_tran ON prpayment_authorize_tran.paymenttranunkid = prpayment_tran.paymenttranunkid " & _
                            "LEFT JOIN hrmsConfiguration..cfuser_master ON cfuser_master.userunkid = prpayment_authorize_tran.userunkid " & _
                    "WHERE   ISNULL(prpayment_tran.isvoid, 0) = 0 " & _
                            "AND prpayment_authorize_tran.isactive = 1 " & _
                            "AND prpayment_tran.referenceid = @referenceid " & _
                            "AND prpayment_tran.paytypeid = @paytypeid " & _
                            "AND prpayment_tran.isauthorized = 1 "
            '       'Sohail (16 Mar 2013) - [userfullname]

            'strQ &= UserAccessLevel._AccessLevelFilterString

            If intPeriodId > 0 Then
                strQ &= " AND prpayment_tran.periodunkid = @periodunkid "
                objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intPeriodId)
            End If

            If intUserUnkId > 0 Then
                strQ &= " AND prpayment_authorize_tran.userunkid = @userunkid "
                objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUserUnkId)
            End If

            If strFilterQuery.Trim <> "" Then
                strQ &= " AND " & strFilterQuery
            End If

            strQ &= " GROUP BY prpayment_authorize_tran.userunkid " & _
                            ", ISNULL(cfuser_master.username, '') " & _
                            ", ISNULL(cfuser_master.firstname, '') + ' ' + ISNULL(cfuser_master.lastname, '') "
            '   'Sohail (16 Mar 2013) - [ISNULL(cfuser_master.firstname, '') + ' ' + ISNULL(cfuser_master.lastname, '')]

            objDataOperation.AddParameter("@referenceid", SqlDbType.Int, eZeeDataType.INT_SIZE, intPayRefId.ToString)
            objDataOperation.AddParameter("@paytypeid", SqlDbType.Int, eZeeDataType.INT_SIZE, intPaymentTypeId.ToString)

            dsList = objDataOperation.ExecQuery(strQ, strTableName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If


        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetLastAuthorizationDetail; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
        Return dsList
    End Function
    'Sohail (11 Mar 2013) -- End

    ''' <summary>
    ''' Modify By: Sohail
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> INSERT INTO Database Table (prpayment_authorize_tran) </purpose>
    Public Function Insert() As Boolean
        'If isExist(mstrName) Then
        '    mstrMessage = "<Message>"
        '    Return False
        'End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            objDataOperation.AddParameter("@paymenttranunkid", SqlDbType.int, eZeeDataType.INT_SIZE, mintpaymenttranunkid.ToString)
            objDataOperation.AddParameter("@authorize_date", SqlDbType.datetime, eZeeDataType.DATETIME_SIZE, mdtauthorize_date.ToString)
            objDataOperation.AddParameter("@isauthorized", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsauthorized.ToString)
            objDataOperation.AddParameter("@userunkid", SqlDbType.int, eZeeDataType.INT_SIZE, mintuserunkid.ToString)
            objDataOperation.AddParameter("@isactive", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsactive.ToString)
            'Sohail (27 Jul 2012) -- Start
            'TRA - ENHANCEMENT
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
            If mdtVoiddatetime = Nothing Then
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            Else
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime)
            End If
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)
            'Sohail (27 Jul 2012) -- End
            objDataOperation.AddParameter("@remarks", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, mstrRemarks.ToString) 'Sohail (27 Feb 2013)

            strQ = "INSERT INTO prpayment_authorize_tran ( " & _
              "  paymenttranunkid " & _
              ", authorize_date " & _
              ", isauthorized " & _
              ", userunkid " & _
              ", isactive" & _
              ", voiduserunkid " & _
              ", voiddatetime " & _
              ", voidreason " & _
              ", remarks " & _
            ") VALUES (" & _
              "  @paymenttranunkid " & _
              ", @authorize_date " & _
              ", @isauthorized " & _
              ", @userunkid " & _
              ", @isactive" & _
              ", @voiduserunkid " & _
              ", @voiddatetime " & _
              ", @voidreason " & _
              ", @remarks " & _
            "); SELECT @@identity"
            'Sohail (27 Feb 2013) - [remarks]

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            mintPaymentauthorizeTranUnkId = dsList.Tables(0).Rows(0).Item(0)

            'Sohail (27 Jul 2012) -- Start
            'TRA - ENHANCEMENT
            If InsertAuditTrailForPaymentAuthorizeTran(objDataOperation, 1) = False Then
                Return False
            End If
            'Sohail (27 Jul 2012) -- End

            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Insert; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function


    'Sohail (27 Jul 2012) -- Start
    'TRA - ENHANCEMENT
    Public Function InsertAuditTrailForPaymentAuthorizeTran(ByVal objDataOperation As clsDataOperation, ByVal AuditType As Integer) As Boolean

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try

            'Sohail (18 May 2013) -- Start
            'TRA - ENHANCEMENT
            If mstrWebIP.Trim = "" Then mstrWebIP = getIP()
            If mstrWebhostName.Trim = "" Then mstrWebhostName = getHostName()
            'Sohail (18 May 2013) -- End

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@paymentauthorizetranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPaymentauthorizetranunkid.ToString)
            objDataOperation.AddParameter("@paymenttranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPaymenttranunkid.ToString)
            objDataOperation.AddParameter("@authorize_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtAuthorize_Date.ToString)
            objDataOperation.AddParameter("@isauthorized", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsauthorized.ToString)
            objDataOperation.AddParameter("@audittype", SqlDbType.Int, eZeeDataType.INT_SIZE, AuditType.ToString)
            objDataOperation.AddParameter("@audituserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid)
            objDataOperation.AddParameter("@auditdatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtAuditDate)
            objDataOperation.AddParameter("@ip", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrWebIP)
            objDataOperation.AddParameter("@machine_name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrWebhostName)
            objDataOperation.AddParameter("@remarks", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, mstrRemarks.ToString) 'Sohail (27 Feb 2013)
            If mstrWebFormName.Trim.Length <= 0 Then
                objDataOperation.AddParameter("@form_name", SqlDbType.NVarChar, 500, mstrFormName)
                objDataOperation.AddParameter("@isweb", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsWeb)

            Else
                objDataOperation.AddParameter("@form_name", SqlDbType.NVarChar, 500, mstrWebFormName)
                objDataOperation.AddParameter("@isweb", SqlDbType.Bit, eZeeDataType.BIT_SIZE, True)
                objDataOperation.AddParameter("@module_name1", SqlDbType.NVarChar, 500, Language.getMessage(mstrModuleName, 1, "WEB"))
            End If





            strQ = "INSERT INTO atprpayment_authorize_tran ( " & _
                "  paymentauthorizetranunkid " & _
                ", paymenttranunkid " & _
                ", authorize_date " & _
                ", isauthorized " & _
                ", audittype " & _
                ", audituserunkid " & _
                ", auditdatetime " & _
                ", ip " & _
                ", machine_name" & _
                ", form_name " & _
                " " & _
                " " & _
                " " & _
                " " & _
                " " & _
                ", isweb " & _
                ", remarks " & _
            ") VALUES (" & _
                "  @paymentauthorizetranunkid " & _
                ", @paymenttranunkid " & _
                ", @authorize_date " & _
                ", @isauthorized " & _
                ", @audittype " & _
                ", @audituserunkid " & _
                ", @auditdatetime " & _
                ", @ip " & _
                ", @machine_name" & _
                ", @form_name " & _
                " " & _
                " " & _
                " " & _
                " " & _
                " " & _
                ", @isweb " & _
                ", @remarks " & _
            "); SELECT @@identity"
            'Sohail (27 Feb 2013) - [remarks]

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            mintPaymentauthorizetranunkid = dsList.Tables(0).Rows(0).Item(0)

            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: InsertAuditTrailForPaymentAuthorizeTran; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function
    'Sohail (27 Jul 2012) -- End    


    '''' <summary>
    '''' Modify By: Sohail
    '''' </summary>
    '''' <returns>Boolean</returns>
    '''' <purpose> Update Database Table (prpayment_authorize_tran) </purpose>
    'Public Function Update() As Boolean
    '    'If isExist(mstrName, mintPaymentauthorizetranunkid) Then
    '    '    mstrMessage = "<Message>"
    '    '    Return False
    '    'End If

    '    Dim dsList As DataSet = Nothing
    '    Dim strQ As String = ""
    '    Dim exForce As Exception

    '    objDataOperation = New clsDataOperation

    '    Try
    '        objDataOperation.AddParameter("@paymentauthorizetranunkid", SqlDbType.int, eZeeDataType.INT_SIZE, mintpaymentauthorizetranunkid.ToString)
    '        objDataOperation.AddParameter("@paymenttranunkid", SqlDbType.int, eZeeDataType.INT_SIZE, mintpaymenttranunkid.ToString)
    '        objDataOperation.AddParameter("@authorize_date", SqlDbType.datetime, eZeeDataType.DATETIME_SIZE, mdtauthorize_date.ToString)
    '        objDataOperation.AddParameter("@isauthorized", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsauthorized.ToString)
    '        objDataOperation.AddParameter("@userunkid", SqlDbType.int, eZeeDataType.INT_SIZE, mintuserunkid.ToString)
    '        objDataOperation.AddParameter("@isactive", SqlDbType.bit, eZeeDataType.BIT_SIZE, mblnisactive.ToString)

    '        strQ = "UPDATE prpayment_authorize_tran SET " & _
    '          "  paymenttranunkid = @paymenttranunkid" & _
    '          ", authorize_date = @authorize_date" & _
    '          ", isauthorized = @isauthorized" & _
    '          ", userunkid = @userunkid" & _
    '          ", isactive = @isactive " & _
    '        "WHERE paymentauthorizetranunkid = @paymentauthorizetranunkid "

    '        Call objDataOperation.ExecNonQuery(strQ)

    '        If objDataOperation.ErrorMessage <> "" Then
    '            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
    '            Throw exForce
    '        End If

    '        'Sohail (27 Jul 2012) -- Start
    '        'TRA - ENHANCEMENT
    '        If InsertAuditTrailForPaymentAuthorizeTran(objDataOperation, 2) = False Then
    '            Return False
    '        End If
    '        'Sohail (27 Jul 2012) -- End

    '        Return True
    '    Catch ex As Exception
    '        Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
    '        Return False
    '    Finally
    '        exForce = Nothing
    '        If dsList IsNot Nothing Then dsList.Dispose()
    '        objDataOperation = Nothing
    '    End Try
    'End Function

    '''' <summary>
    '''' Modify By: Sohail
    '''' </summary>
    '''' <returns>Boolean</returns>
    '''' <purpose> Delete Database Table (prpayment_authorize_tran) </purpose>
    'Public Function Delete(ByVal intUnkid As Integer) As Boolean
    '    If isUsed(intUnkid) Then
    '        mstrMessage = "<Message>"
    '        Return False
    '    End If

    '    Dim dsList As DataSet = Nothing
    '    Dim strQ As String = ""
    '    Dim exForce As Exception

    '    objDataOperation = New clsDataOperation

    '    Try
    '        StrQ = "DELETE FROM prpayment_authorize_tran " & _
    '        "WHERE paymentauthorizetranunkid = @paymentauthorizetranunkid "

    '        objDataOperation.AddParameter("@paymentauthorizetranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

    '        Call objDataOperation.ExecNonQuery(strQ)

    '        If objDataOperation.ErrorMessage <> "" Then
    '            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
    '            Throw exForce
    '        End If

    '        'Sohail (27 Jul 2012) -- Start
    '        'TRA - ENHANCEMENT
    '        If InsertAuditTrailForPaymentAuthorizeTran(objDataOperation, 3) = False Then
    '            Return False
    '        End If
    '        'Sohail (27 Jul 2012) -- End

    '        Return True
    '    Catch ex As Exception
    '        Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
    '        Return False
    '    Finally
    '        exForce = Nothing
    '        If dsList IsNot Nothing Then dsList.Dispose()
    '        objDataOperation = Nothing
    '    End Try
    'End Function

    '''' <summary>
    '''' Modify By: Sohail
    '''' </summary>
    '''' <purpose> Assign all Property variable </purpose>
    'Public Function isUsed(ByVal intUnkid As Integer) As Boolean
    '    Dim dsList As DataSet = Nothing
    '    Dim strQ As String = ""
    '    Dim exForce As Exception

    '    objDataOperation = New clsDataOperation

    '    Try
    '        StrQ = "<Query>"

    '        objDataOperation.AddParameter("@paymentauthorizetranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

    '        dsList = objDataOperation.ExecQuery(strQ, "List")

    '        If objDataOperation.ErrorMessage <> "" Then
    '            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
    '            Throw exForce
    '        End If

    '        Return dsList.tables(0).rows.count > 0
    '    Catch ex As Exception
    '        Throw New Exception(ex.Message & "; Procedure Name: isUsed; Module Name: " & mstrModuleName)
    '    Finally
    '        exForce = Nothing
    '        If dsList IsNot Nothing Then dsList.Dispose()
    '        objDataOperation = Nothing
    '    End Try
    'End Function

    '''' <summary>
    '''' Modify By: Sohail
    '''' </summary>
    '''' <purpose> Assign all Property variable </purpose>
    'Public Function isExist(ByVal strCode As String, ByVal strName As String, Optional ByVal intUnkid As Integer = -1) As Boolean
    '    Dim dsList As DataSet = Nothing
    '    Dim strQ As String = ""
    '    Dim exForce As Exception

    '    objDataOperation = New clsDataOperation

    '    Try
    '        strQ = "SELECT " & _
    '          "  paymentauthorizetranunkid " & _
    '          ", paymenttranunkid " & _
    '          ", authorize_date " & _
    '          ", isauthorized " & _
    '          ", userunkid " & _
    '          ", isactive " & _
    '         "FROM prpayment_authorize_tran " & _
    '         "WHERE name = @name " & _
    '         "AND code = @code "

    '        If intUnkid > 0 Then
    '            strQ &= " AND paymentauthorizetranunkid <> @paymentauthorizetranunkid"
    '        End If

    '        objDataOperation.AddParameter("@code", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strCode)
    '        objDataOperation.AddParameter("@name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strName)
    '        objDataOperation.AddParameter("@paymentauthorizetranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

    '        dsList = objDataOperation.ExecQuery(strQ, "List")

    '        If objDataOperation.ErrorMessage <> "" Then
    '            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
    '            Throw exForce
    '        End If

    '        Return dsList.tables(0).rows.count > 0
    '    Catch ex As Exception
    '        Throw New Exception(ex.Message & "; Procedure Name: isExist; Module Name: " & mstrModuleName)
    '    Finally
    '        exForce = Nothing
    '        If dsList IsNot Nothing Then dsList.Dispose()
    '        objDataOperation = Nothing
    '    End Try
    'End Function




	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Public Shared Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "WEB")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class
