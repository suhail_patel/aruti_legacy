﻿'************************************************************************************************************************************
'Class Name : clspayrollgroup_master.vb
'Purpose    : All Payroll Group Opration like getList, Insert, Update, Delete, checkDuplicate
'Date       :30/06/2010
'Written By :Pinkal
'Modified   :
'************************************************************************************************************************************
'Last Message Index  = 7


Imports eZeeCommonLib

Public Enum PayrollGroupType
    CostCenter = 1
    Vendor = 2
    Bank = 3
End Enum

''' <summary>
''' Purpose: 
''' Developer: Pinkal
''' </summary>
Public Class clspayrollgroup_master
    Private Shared ReadOnly mstrModuleName As String = "clspayrollgroup_master"
    Dim objDataOperation As clsDataOperation
    Dim mstrMessage As String = ""

#Region " Private variables "
    Private mintGroupmasterunkid As Integer
    Private mstrGroupalias As String = String.Empty
    Private mstrGroupcode As String = String.Empty
    Private mstrGroupname As String = String.Empty
    Private mintGrouptype_Id As Integer
    Private mstrDescription As String = String.Empty
    Private mstrWebsite As String = String.Empty
    Private mblnIsactive As Boolean = True
    Private mstrGroupname1 As String = String.Empty
    Private mstrGroupname2 As String = String.Empty

    'Pinkal (12-Oct-2011) -- Start
    Private mstrSwiftCode As String = String.Empty
    'Pinkal (12-Oct-2011) -- End
    Private mstrReferenceNo As String = String.Empty 'Sohail (31 Mar 2014)
#End Region

#Region " Properties "
'S.SANDEEP [28-May-2018] -- START 
'ISSUE/ENHANCEMENT : {Audit Trails} 
Private mstrFormName As String = String.Empty
Public WriteOnly Property _FormName() As String 
Set(ByVal value As String) 
mstrFormName = value 
End Set 
End Property 
 
Private mstrClientIP As String = ""
Public WriteOnly Property _ClientIP() As String 
Set(ByVal value As String) 
mstrClientIP = value 
End Set 
End Property 
 
Private mstrHostName As String = ""
Public WriteOnly Property _HostName() As String 
Set(ByVal value As String) 
mstrHostName = value 
End Set 
End Property 
 
Private mblnIsWeb As Boolean = False
Public WriteOnly Property _FromWeb() As Boolean 
Set(ByVal value As Boolean) 
mblnIsWeb = value 
End Set 
End Property 
 
    Private mintAuditUserId As Integer = 0
    Public WriteOnly Property _AuditUserId() As Integer
        Set(ByVal value As Integer)
            mintAuditUserId = value
        End Set
    End Property

    Private mintCompanyUnkid As Integer = 0
    Public WriteOnly Property _CompanyUnkid() As Integer
        Set(ByVal value As Integer)
            mintCompanyUnkid = value
        End Set
    End Property
 
Private mdtAuditDate As DateTime = Now
Public WriteOnly Property _AuditDate() As DateTime 
Set(ByVal value As DateTime) 
mdtAuditDate = value 
End Set 
End Property 
 
Private xDataOpr As clsDataOperation = Nothing
Public Property _xDataOpr() As clsDataOperation 
Get 
Return xDataOpr 
End Get 
Set(ByVal value As clsDataOperation) 
xDataOpr = value 
End Set 
End Property 
 
Private mintLoginEmployeeunkid As Integer = 0
Public Property _LoginEmployeeunkid() As Integer 
Get 
Return mintLoginEmployeeunkid 
End Get 
Set(ByVal value As Integer) 
mintLoginEmployeeunkid = value 
End Set 
End Property 
 
Private mintVoidlogingemployeeunkid As Integer=0
Public Property _Voidloginemployeeunkid() As Integer 
Get 
Return mintVoidlogingemployeeunkid 
End Get 
Set(ByVal value As Integer) 
mintVoidlogingemployeeunkid = value 
End Set 
End Property 
'S.SANDEEP [28-May-2018] -- END 
    ''' <summary>
    ''' Purpose: Get Message from Class 
    ''' Modify By: Pinkal
    ''' </summary>
    Public ReadOnly Property _Message() As String
        Get
            Return mstrMessage
        End Get
    End Property

    ''' <summary>
    ''' Purpose: Get or Set groupmasterunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Groupmasterunkid() As Integer
        Get
            Return mintGroupmasterunkid
        End Get
        Set(ByVal value As Integer)
            mintGroupmasterunkid = Value
            Call getData()
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set groupalias
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Groupalias() As String
        Get
            Return mstrGroupalias
        End Get
        Set(ByVal value As String)
            mstrGroupalias = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set groupcode
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Groupcode() As String
        Get
            Return mstrGroupcode
        End Get
        Set(ByVal value As String)
            mstrGroupcode = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set groupname
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Groupname() As String
        Get
            Return mstrGroupname
        End Get
        Set(ByVal value As String)
            mstrGroupname = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set grouptype_id
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Grouptype_Id() As Integer
        Get
            Return mintGrouptype_Id
        End Get
        Set(ByVal value As Integer)
            mintGrouptype_Id = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set website
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Website() As String
        Get
            Return mstrWebsite
        End Get
        Set(ByVal value As String)
            mstrWebsite = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set description
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Description() As String
        Get
            Return mstrDescription
        End Get
        Set(ByVal value As String)
            mstrDescription = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isactive
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Isactive() As Boolean
        Get
            Return mblnIsactive
        End Get
        Set(ByVal value As Boolean)
            mblnIsactive = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set groupname1
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Groupname1() As String
        Get
            Return mstrGroupname1
        End Get
        Set(ByVal value As String)
            mstrGroupname1 = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set groupname2
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Groupname2() As String
        Get
            Return mstrGroupname2
        End Get
        Set(ByVal value As String)
            mstrGroupname2 = Value
        End Set
    End Property


    'Pinkal (12-Oct-2011) -- Start

    ''' <summary>
    ''' Purpose: Get or Set swiftcode
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Swiftcode() As String
        Get
            Return mstrSwiftCode
        End Get
        Set(ByVal value As String)
            mstrSwiftCode = value
        End Set
    End Property

    'Pinkal (12-Oct-2011) -- End

    'Sohail (31 Mar 2014) -- Start
    'ENHANCEMENT - Show Bank Reference No on Bank Payment List.
    Public Property _ReferenceNo() As String
        Get
            Return mstrReferenceNo
        End Get
        Set(ByVal value As String)
            mstrReferenceNo = value
        End Set
    End Property
    'Sohail (31 Mar 2014) -- End



#End Region

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Sub GetData()
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try


            'Pinkal (12-Oct-2011) -- Start

            'strQ = "SELECT " & _
            '  "  groupmasterunkid " & _
            '  ", groupalias " & _
            '  ", groupcode " & _
            '  ", groupname " & _
            '  ", grouptype_id " & _
            '  ", description " & _
            '  ", website " & _
            '  ", isactive " & _
            '  ", groupname1 " & _
            '  ", groupname2 " & _
            ' "FROM hrmsConfiguration..cfpayrollgroup_master " & _
            ' "WHERE groupmasterunkid = @groupmasterunkid "


            strQ = "SELECT " & _
              "  groupmasterunkid " & _
              ", groupalias " & _
              ", groupcode " & _
              ", groupname " & _
              ", grouptype_id " & _
              ", description " & _
              ", website " & _
              ", isactive " & _
              ", groupname1 " & _
              ", groupname2 " & _
              ",swiftcode " & _
              ", ISNULL(referenceno, '') AS referenceno " & _
             "FROM hrmsConfiguration..cfpayrollgroup_master " & _
             "WHERE groupmasterunkid = @groupmasterunkid "
            'Sohail (31 Mar 2014) - referenceno

            'Pinkal (12-Oct-2011) -- End

            objDataOperation.AddParameter("@groupmasterunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintGroupmasterunkid.ToString)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For Each dtRow As DataRow In dsList.Tables(0).Rows
                mintGroupmasterunkid = CInt(dtRow.Item("groupmasterunkid"))
                mstrGroupalias = dtRow.Item("groupalias").ToString
                mstrGroupcode = dtRow.Item("groupcode").ToString
                mstrGroupname = dtRow.Item("groupname").ToString
                mintGrouptype_Id = CInt(dtRow.Item("grouptype_id"))
                mstrDescription = dtRow.Item("description").ToString
                mstrWebsite = dtRow.Item("website").ToString
                mblnIsactive = CBool(dtRow.Item("isactive"))
                mstrGroupname1 = dtRow.Item("groupname1").ToString
                mstrGroupname2 = dtRow.Item("groupname2").ToString

                'Pinkal (12-Oct-2011) -- Start

                If dtRow.Item("swiftcode").ToString IsNot Nothing Then
                    mstrSwiftCode = dtRow.Item("swiftcode").ToString
                End If

                'Pinkal (12-Oct-2011) -- End

                mstrReferenceNo = dtRow.Item("referenceno").ToString 'Sohail (31 Mar 2014)

                Exit For
            Next
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetData; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Sub


    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function GetList(ByVal strTableName As String, Optional ByVal blnOnlyActive As Boolean = True) As DataSet
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try


            'Pinkal (12-Oct-2011) -- Start

            'strQ &= "SELECT " & _
            '  "  groupmasterunkid " & _
            '  ", groupalias " & _
            '  ", groupcode " & _
            '  ", groupname " & _
            '  ", grouptype_id " & _
            ' ", case when grouptype_id  = 1 then 'Cost Center'  " & _
            '          " when grouptype_id = 2 then 'Vendor'  " & _
            '          " When grouptype_id = 3 then 'Bank' end grouptype " & _
            '  ", description " & _
            '  ", website " & _
            '  ", isactive " & _
            '  ", groupname1 " & _
            '  ", groupname2 " & _
            ' "FROM hrmsConfiguration..cfpayrollgroup_master "


            strQ &= "SELECT " & _
              "  groupmasterunkid " & _
              ", groupalias " & _
              ", groupcode " & _
              ", groupname " & _
              ", grouptype_id " & _
           ", case when grouptype_id  = 1 then @CostCenter  " & _
                    " when grouptype_id = 2 then @Vendor  " & _
                    " When grouptype_id = 3 then @Bank end grouptype " & _
              ", description " & _
              ", website " & _
              ", isactive " & _
              ", groupname1 " & _
              ", groupname2 " & _
            ", swiftcode " & _
            ", ISNULL(referenceno, '') AS referenceno " & _
             "FROM hrmsConfiguration..cfpayrollgroup_master "
            'Sohail (31 Mar 2014) - [referenceno]

            If blnOnlyActive Then
                strQ &= " WHERE isactive = 1 "
            End If


            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@CostCenter", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 4, "Cost Center"))
            objDataOperation.AddParameter("@Vendor", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 5, "Vendor"))
            objDataOperation.AddParameter("@Bank", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 6, "Bank"))

            'Pinkal (12-Oct-2011) -- End

            dsList = objDataOperation.ExecQuery(strQ, strTableName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
        Return dsList
    End Function


    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> INSERT INTO Database Table (hrmsConfiguration..cfpayrollgroup_master) </purpose>
    Public Function Insert() As Boolean
        If isExist(mstrGroupcode) Then
            mstrMessage = Language.getMessage(mstrModuleName, 1, "This Code is already defined. Please define new Code.")
            Return False
        ElseIf isExist("", mstrGroupname) Then
            mstrMessage = Language.getMessage(mstrModuleName, 2, "This Name is already defined. Please define new Name.")
            Return False
        End If


        'Pinkal (12-Oct-2011) -- Start
        If mstrSwiftCode <> "" Then
            If isSwiftcodeExist(mstrSwiftCode) Then
                mstrMessage = Language.getMessage(mstrModuleName, 7, "This Swift code is already defined. Please define new Swift code.")
                Return False
            End If
        End If
        'Pinkal (12-Oct-2011) -- End

Dim objDataOperation As clsDataOperation 'Sohail (12 Oct 2011)
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation
        objDataOperation.BindTransaction() 'Sohail (12 Oct 2011)

        Try
            objDataOperation.AddParameter("@groupalias", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrGroupalias.ToString)
            objDataOperation.AddParameter("@groupcode", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrGroupcode.ToString)
            objDataOperation.AddParameter("@groupname", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrGroupname.ToString)
            objDataOperation.AddParameter("@grouptype_id", SqlDbType.Int, eZeeDataType.INT_SIZE, mintGrouptype_Id.ToString)
            objDataOperation.AddParameter("@description", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrDescription.ToString)
            objDataOperation.AddParameter("@website", SqlDbType.NVarChar, eZeeDataType.WEBSITE_SIZE, mstrWebsite.ToString)
            objDataOperation.AddParameter("@isactive", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsactive.ToString)
            objDataOperation.AddParameter("@groupname1", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrGroupname1.ToString)
            objDataOperation.AddParameter("@groupname2", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrGroupname2.ToString)


            'Pinkal (12-Oct-2011) -- Start
            objDataOperation.AddParameter("@swiftcode", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrSwiftCode.ToString)
            objDataOperation.AddParameter("@referenceno", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrReferenceNo.ToString) 'Sohail (31 Mar 2014)

            'strQ = "INSERT INTO hrmsConfiguration..cfpayrollgroup_master ( " & _
            '  "  groupalias " & _
            '  ", groupcode " & _
            '  ", groupname " & _
            '  ", grouptype_id " & _
            '  ", description " & _
            '  ", website " & _
            '  ", isactive " & _
            '  ", groupname1 " & _
            '  ", groupname2" & _
            '") VALUES (" & _
            '  "  @groupalias " & _
            '  ", @groupcode " & _
            '  ", @groupname " & _
            '  ", @grouptype_id " & _
            '  ", @description " & _
            '  ", @website " & _
            '  ", @isactive " & _
            '  ", @groupname1 " & _
            '  ", @groupname2" & _
            '"); SELECT @@identity"


            strQ = "INSERT INTO hrmsConfiguration..cfpayrollgroup_master ( " & _
              "  groupalias " & _
              ", groupcode " & _
              ", groupname " & _
              ", grouptype_id " & _
              ", description " & _
              ", website " & _
              ", isactive " & _
              ", groupname1 " & _
              ", groupname2" & _
              ", swiftcode " & _
              ", referenceno " & _
            ") VALUES (" & _
              "  @groupalias " & _
              ", @groupcode " & _
              ", @groupname " & _
              ", @grouptype_id " & _
              ", @description " & _
              ", @website " & _
              ", @isactive " & _
              ", @groupname1 " & _
              ", @groupname2" & _
              ", @swiftcode " & _
              ", @referenceno " & _
            "); SELECT @@identity"
            'Sohail (31 Mar 2014) - [referenceno]
            'Pinkal (12-Oct-2011) -- End


            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            mintGroupmasterunkid = dsList.Tables(0).Rows(0).Item(0)

'S.SANDEEP [28-May-2018] -- START
'ISSUE/ENHANCEMENT : {Audit Trails} 
Dim objCommonATLog AS New clsCommonATLog 
objCommonATLog._FormName = mstrFormName 
objCommonATLog._LoginEmployeeUnkid = mintLoginEmployeeunkid 
            objCommonATLog._ClientIP = mstrClientIP
            objCommonATLog._HostName = mstrHostName
            objCommonATLog._FromWeb = mblnIsWeb
            objCommonATLog._AuditUserId = mintAuditUserId
objCommonATLog._CompanyUnkid = mintCompanyUnkid
            objCommonATLog._AuditDate = mdtAuditDate
            'S.SANDEEP [28-May-2018] -- END

            If objCommonATLog.Insert_AtLog(objDataOperation, 1, "hrmsConfiguration..cfpayrollgroup_master", "groupmasterunkid", mintGroupmasterunkid, True) = False Then
                objDataOperation.ReleaseTransaction(False)
                Return False
            Else
                objDataOperation.ReleaseTransaction(True)
            Return True
            End If

            'S.SANDEEP [28-May-2018] -- START
            'ISSUE/ENHANCEMENT : {Audit Trails} 
            objCommonATLog = Nothing
            'S.SANDEEP [28-May-2018] -- END

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Insert; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Update Database Table (hrmsConfiguration..cfpayrollgroup_master) </purpose>
    Public Function Update() As Boolean

        'Pinkal (12-Oct-2011) -- Start

        If isExist(mstrGroupcode, "", mintGroupmasterunkid) Then
            mstrMessage = Language.getMessage(mstrModuleName, 1, "This Code is already defined. Please define new Code.")
            Return False

        ElseIf isExist("", mstrGroupname, mintGroupmasterunkid) Then
            mstrMessage = Language.getMessage(mstrModuleName, 2, "This Name is already defined. Please define new Name.")
            Return False
        End If


        'Pinkal (12-Oct-2011) -- Start
        If mstrSwiftCode <> "" Then
            If isSwiftcodeExist(mstrSwiftCode, mintGroupmasterunkid) Then
                mstrMessage = Language.getMessage(mstrModuleName, 7, "This Swift code is already defined. Please define new Swift code.")
                Return False
            End If
        End If
        'Pinkal (12-Oct-2011) -- End


        'Pinkal (12-Oct-2011) -- End

Dim objDataOperation As clsDataOperation 'Sohail (12 Oct 2011)
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation
        objDataOperation.BindTransaction() 'Sohail (12 Oct 2011)

        Try
            objDataOperation.AddParameter("@groupmasterunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintGroupmasterunkid.ToString)
            objDataOperation.AddParameter("@groupalias", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrGroupalias.ToString)
            objDataOperation.AddParameter("@groupcode", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrGroupcode.ToString)
            objDataOperation.AddParameter("@groupname", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrGroupname.ToString)
            objDataOperation.AddParameter("@grouptype_id", SqlDbType.Int, eZeeDataType.INT_SIZE, mintGrouptype_Id.ToString)
            objDataOperation.AddParameter("@description", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrDescription.ToString)
            objDataOperation.AddParameter("@website", SqlDbType.NVarChar, eZeeDataType.WEBSITE_SIZE, mstrWebsite.ToString)
            objDataOperation.AddParameter("@isactive", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsactive.ToString)
            objDataOperation.AddParameter("@groupname1", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrGroupname1.ToString)
            objDataOperation.AddParameter("@groupname2", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrGroupname2.ToString)

            'Pinkal (12-Oct-2011) -- Start

            objDataOperation.AddParameter("@swiftcode", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrSwiftCode.ToString)
            objDataOperation.AddParameter("@referenceno", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrReferenceNo.ToString) 'Sohail (31 Mar 2014)

            'strQ = "UPDATE hrmsConfiguration..cfpayrollgroup_master SET " & _
            '  "  groupalias = @groupalias" & _
            '  ", groupcode = @groupcode" & _
            '  ", groupname = @groupname" & _
            '  ", grouptype_id = @grouptype_id" & _
            '  ", description = @description" & _
            '  ", website = @website " & _
            '  ", isactive = @isactive" & _
            '  ", groupname1 = @groupname1" & _
            '  ", groupname2 = @groupname2 " & _
            '"WHERE groupmasterunkid = @groupmasterunkid "

            strQ = "UPDATE hrmsConfiguration..cfpayrollgroup_master SET " & _
              "  groupalias = @groupalias" & _
              ", groupcode = @groupcode" & _
              ", groupname = @groupname" & _
              ", grouptype_id = @grouptype_id" & _
              ", description = @description" & _
              ", website = @website " & _
              ", isactive = @isactive" & _
              ", groupname1 = @groupname1" & _
              ", groupname2 = @groupname2 " & _
              ", swiftcode = @swiftcode " & _
              ", referenceno = @referenceno " & _
            "WHERE groupmasterunkid = @groupmasterunkid "
            'Sohail (31 Mar 2014) - [referenceno]
            'Pinkal (12-Oct-2011) -- End



            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            'S.SANDEEP [28-May-2018] -- START
            'ISSUE/ENHANCEMENT : {Audit Trails} 
            Dim objCommonATLog As New clsCommonATLog
            objCommonATLog._FormName = mstrFormName
            objCommonATLog._LoginEmployeeUnkid = mintLoginEmployeeunkid
            objCommonATLog._ClientIP = mstrClientIP
            objCommonATLog._HostName = mstrHostName
            objCommonATLog._FromWeb = mblnIsWeb
            objCommonATLog._AuditUserId = mintAuditUserId
objCommonATLog._CompanyUnkid = mintCompanyUnkid
            objCommonATLog._AuditDate = mdtAuditDate
            'S.SANDEEP [28-May-2018] -- END

            If objCommonATLog.IsConfigTableDataUpdate("atconfigcommon_log", "hrmsConfiguration..cfpayrollgroup_master", mintGroupmasterunkid, "groupmasterunkid", 2) Then
                If objCommonATLog.Insert_AtLog(objDataOperation, 2, "hrmsConfiguration..cfpayrollgroup_master", "groupmasterunkid", mintGroupmasterunkid, True) = False Then
                    objDataOperation.ReleaseTransaction(False)
                    Return False
                End If
            End If

            'S.SANDEEP [28-May-2018] -- START
            'ISSUE/ENHANCEMENT : {Audit Trails} 
            objCommonATLog = Nothing
            'S.SANDEEP [28-May-2018] -- END


            objDataOperation.ReleaseTransaction(True)

            Return True

            'Sohail (12 Oct 2011) -- End
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Delete Database Table (hrmsConfiguration..cfpayrollgroup_master) </purpose>
    Public Function Delete(ByVal intUnkid As Integer) As Boolean
        'If isUsed(intUnkid) Then
        '    mstrMessage = "<Message>"
        '    Return False
        'End If

        Dim objDataOperation As clsDataOperation 'Sohail (12 Oct 2011)
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation
        objDataOperation.BindTransaction() 'Sohail (12 Oct 2011)

        Try
            'strQ = "DELETE FROM hrmsConfiguration..cfpayrollgroup_master " & _
            '      "WHERE groupmasterunkid = @groupmasterunkid "


            strQ = "UPDATE hrmsConfiguration..cfpayrollgroup_master " & _
                    "SET isactive = 0 WHERE groupmasterunkid = @groupmasterunkid "

            objDataOperation.AddParameter("@groupmasterunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            'S.SANDEEP [28-May-2018] -- START
            'ISSUE/ENHANCEMENT : {Audit Trails} 
            Dim objCommonATLog As New clsCommonATLog
            objCommonATLog._FormName = mstrFormName
            objCommonATLog._LoginEmployeeUnkid = mintLoginEmployeeunkid
            objCommonATLog._ClientIP = mstrClientIP
            objCommonATLog._HostName = mstrHostName
            objCommonATLog._FromWeb = mblnIsWeb
            objCommonATLog._AuditUserId = mintAuditUserId
objCommonATLog._CompanyUnkid = mintCompanyUnkid
            objCommonATLog._AuditDate = mdtAuditDate
            'S.SANDEEP [28-May-2018] -- END

            If objCommonATLog.Insert_AtLog(objDataOperation, 3, "hrmsConfiguration..cfpayrollgroup_master", "groupmasterunkid", intUnkid, True) = False Then
                objDataOperation.ReleaseTransaction(False)
                Return False
            Else
                objDataOperation.ReleaseTransaction(True)
            Return True
            End If

            'S.SANDEEP [28-May-2018] -- START
            'ISSUE/ENHANCEMENT : {Audit Trails} 
            objCommonATLog = Nothing
            'S.SANDEEP [28-May-2018] -- END

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isUsed(ByVal intUnkid As Integer, ByVal intPayrollGroupId As Integer) As Boolean
        'Public Function isUsed(ByVal intUnkid As Integer) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            'Sohail (19 Nov 2010) -- Start
            'strQ = "<Query>"

            ''objDataOperation.AddParameter("@groupmasterunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)
            If intPayrollGroupId = PayrollGroupType.CostCenter Then
                strQ = "SELECT  costcenterunkid " & _
                       "FROM    prcostcenter_master " & _
                       "WHERE   isactive = 1 " & _
                               "AND costcentergroupmasterunkid = @costcentergroupmasterunkid "

                objDataOperation.AddParameter("@costcentergroupmasterunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)
            ElseIf intPayrollGroupId = PayrollGroupType.Bank Then
                strQ = "SELECT  bankgroupunkid " & _
                       "FROM    hrmsConfiguration..cfbankbranch_master " & _
                       "WHERE   isactive = 1 " & _
                               "AND bankgroupunkid = @bankgroupunkid " & _
                       "UNION " & _
                       "SELECT  bankgroupunkid " & _
                       "FROM    prbankedi_master " & _
                       "WHERE   ISNULL(isvoid, 0) = 0 " & _
                               "AND bankgroupunkid = @bankgroupunkid "

                objDataOperation.AddParameter("@bankgroupunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)
            Else
                strQ = "SELECT  branchunkid FROM hrmsConfiguration..cfbankbranch_master WHERE 1=2 "
            End If
            'Sohail (19 Nov 2010) -- End

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isUsed; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isExist(Optional ByVal strCode As String = "", Optional ByVal strName As String = "", Optional ByVal intUnkid As Integer = -1) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try


            'Pinkal (12-Oct-2011) -- Start

            'strQ = "SELECT " & _
            '  "  groupmasterunkid " & _
            '  ", groupalias " & _
            '  ", groupcode " & _
            '  ", groupname " & _
            '  ", grouptype_id " & _
            '  ", description " & _
            '  ", website " & _
            '  ", isactive " & _
            '  ", groupname1 " & _
            '  ", groupname2 " & _
            ' "FROM hrmsConfiguration..cfpayrollgroup_master " & _
            ' "WHERE 1=1 AND grouptype_id=@grouptype_id"


            strQ = "SELECT " & _
              "  groupmasterunkid " & _
              ", groupalias " & _
              ", groupcode " & _
              ", groupname " & _
              ", grouptype_id " & _
              ", description " & _
              ", website " & _
              ", isactive " & _
              ", groupname1 " & _
              ", groupname2 " & _
            ", swiftcode " & _
            ", referenceno " & _
             "FROM hrmsConfiguration..cfpayrollgroup_master " & _
             "WHERE 1=1 AND grouptype_id=@grouptype_id"
            'Sohail (31 Mar 2014) - [referenceno]
            'Pinkal (12-Oct-2011) -- End

            If strCode.Trim.Length > 0 Then
                strQ &= " AND groupcode=@groupcode "
            End If

            If strName.Trim.Length > 0 Then
                strQ &= " AND groupname=@groupname "
            End If

            If intUnkid > 0 Then
                strQ &= " AND groupmasterunkid <> @groupmasterunkid "
            End If

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@groupcode", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strCode)
            objDataOperation.AddParameter("@groupname", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strName)
            objDataOperation.AddParameter("@grouptype_id", SqlDbType.Int, eZeeDataType.INT_SIZE, mintGrouptype_Id)
            objDataOperation.AddParameter("@groupmasterunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)


            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isExist; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function getListForCombo(ByVal mintGroupTypeId As Integer, Optional ByVal strListName As String = "List", Optional ByVal mblFlag As Boolean = False) As DataSet
        Dim dsList As New DataSet
        Dim objDataOperation As New clsDataOperation
        Dim strQ As String = String.Empty
        Dim exForce As Exception
        Try
            If mblFlag = True Then
                'Sohail (10 Aug 2012) -- Start
                'TRA - ENHANCEMENT
                'strQ = "SELECT 0 as groupmasterunkid, ' ' +  @name  as name UNION "
                strQ = "SELECT 0 as groupmasterunkid, '' AS Code, ' ' +  @name  as name UNION "
                'Sohail (10 Aug 2012) -- End
            End If
            'Sohail (10 Aug 2012) -- Start
            'TRA - ENHANCEMENT
            'strQ &= "SELECT groupmasterunkid, groupname as name FROM hrmsConfiguration..cfpayrollgroup_master where isactive = 1 and grouptype_id = @grouptype_id ORDER BY name "
            strQ &= "SELECT groupmasterunkid, groupcode AS Code, groupname as name FROM hrmsConfiguration..cfpayrollgroup_master where isactive = 1 and grouptype_id = @grouptype_id ORDER BY name "
            'Sohail (10 Aug 2012) -- End

            objDataOperation.AddParameter("@name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 3, "Select Group"))
            objDataOperation.AddParameter("@grouptype_id", SqlDbType.Int, eZeeDataType.INT_SIZE, mintGroupTypeId)

            dsList = objDataOperation.ExecQuery(strQ, strListName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If
            Return dsList
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "getListForCombo", mstrModuleName)
            Return Nothing
        Finally
            exForce = Nothing
            objDataOperation = Nothing
            dsList.Dispose()
            dsList = Nothing
        End Try

    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    ''' 
    Public Function GetPayrollGrpUnkId(ByVal intPayrollGrpID As Integer, ByVal mstrName As String) As Integer
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation
        Try

            strQ = " SELECT " & _
                      " groupmasterunkid " & _
                      "  FROM hrmsConfiguration..cfpayrollgroup_master " & _
                      " WHERE groupname = @name and grouptype_id = @grouptype "

            'S.SANDEEP [19 AUG 2016] -- START
            'ISSUE : ISACTIVE WAS NOT KEPT FOR CHECKING
            strQ &= " AND hrmsConfiguration..cfpayrollgroup_master.isactive = 1 "
            'S.SANDEEP [19 AUG 2016] -- START

            objDataOperation.AddParameter("@name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrName)
            objDataOperation.AddParameter("@grouptype", SqlDbType.Int, eZeeDataType.INT_SIZE, intPayrollGrpID)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For Each dt As DataRow In dsList.Tables(0).Rows
                Return dt("groupmasterunkid")
            Next

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetPayrollGrpUnkId", mstrModuleName)
        End Try
        Return -1
    End Function

    'Pinkal (10-Mar-2011) -- End

 'Pinkal (12-Oct-2011) -- Start

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isSwiftcodeExist(Optional ByVal strSwiftcode As String = "", Optional ByVal intUnkid As Integer = -1) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try

            strQ = "SELECT " & _
            " swiftcode " & _
           "FROM hrmsConfiguration..cfpayrollgroup_master " & _
           "WHERE 1=1 AND grouptype_id=@grouptype_id"

            If strSwiftcode.Trim.Length > 0 Then
                strQ &= " AND swiftcode=@swiftcode "
            End If

            If intUnkid > 0 Then
                strQ &= " AND groupmasterunkid <> @groupmasterunkid "
            End If

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@swiftcode", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strSwiftcode)
            objDataOperation.AddParameter("@grouptype_id", SqlDbType.Int, eZeeDataType.INT_SIZE, mintGrouptype_Id)
            objDataOperation.AddParameter("@groupmasterunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isSwiftcodeExist; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    'Pinkal (12-Oct-2011) -- End


	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Public Shared Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "This Code is already defined. Please define new Code.")
			Language.setMessage(mstrModuleName, 2, "This Name is already defined. Please define new Name.")
			Language.setMessage(mstrModuleName, 3, "Select Group")
			Language.setMessage(mstrModuleName, 4, "Cost Center")
			Language.setMessage(mstrModuleName, 5, "Vendor")
			Language.setMessage(mstrModuleName, 6, "Bank")
			Language.setMessage(mstrModuleName, 7, "This Swift code is already defined. Please define new Swift code.")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class

''************************************************************************************************************************************
''Class Name : clspayrollgroup_master.vb
''Purpose    : All Payroll Group Opration like getList, Insert, Update, Delete, checkDuplicate
''Date       :30/06/2010
''Written By :Pinkal
''Modified   :
''************************************************************************************************************************************

'Imports eZeeCommonLib

'Public Enum PayrollGroupType
'    CostCenter = 1
'    Vendor = 2
'    Bank = 3
'End Enum

'''' <summary>
'''' Purpose: 
'''' Developer: Pinkal
'''' </summary>
'Public Class clspayrollgroup_master
'    Private Shared Readonly mstrModuleName As String = "clspayrollgroup_master"
'    Dim objDataOperation As clsDataOperation
'    Dim mstrMessage As String = ""

'#Region " Private variables "
'    Private mintGroupmasterunkid As Integer
'    Private mstrGroupalias As String = String.Empty
'    Private mstrGroupcode As String = String.Empty
'    Private mstrGroupname As String = String.Empty
'    Private mintGrouptype_Id As Integer
'    Private mstrDescription As String = String.Empty
'    Private mstrWebsite As String = String.Empty
'    Private mblnIsactive As Boolean = True
'    Private mstrGroupname1 As String = String.Empty
'    Private mstrGroupname2 As String = String.Empty
'#End Region

'#Region " Properties "
'    ''' <summary>
'    ''' Purpose: Get Message from Class 
'    ''' Modify By: Pinkal
'    ''' </summary>
'    Public ReadOnly Property _Message() As String
'        Get
'            Return mstrMessage
'        End Get
'    End Property

'    ''' <summary>
'    ''' Purpose: Get or Set groupmasterunkid
'    ''' Modify By: Pinkal
'    ''' </summary>
'    Public Property _Groupmasterunkid() As Integer
'        Get
'            Return mintGroupmasterunkid
'        End Get
'        Set(ByVal value As Integer)
'            mintGroupmasterunkid = Value
'            Call getData()
'        End Set
'    End Property

'    ''' <summary>
'    ''' Purpose: Get or Set groupalias
'    ''' Modify By: Pinkal
'    ''' </summary>
'    Public Property _Groupalias() As String
'        Get
'            Return mstrGroupalias
'        End Get
'        Set(ByVal value As String)
'            mstrGroupalias = Value
'        End Set
'    End Property

'    ''' <summary>
'    ''' Purpose: Get or Set groupcode
'    ''' Modify By: Pinkal
'    ''' </summary>
'    Public Property _Groupcode() As String
'        Get
'            Return mstrGroupcode
'        End Get
'        Set(ByVal value As String)
'            mstrGroupcode = Value
'        End Set
'    End Property

'    ''' <summary>
'    ''' Purpose: Get or Set groupname
'    ''' Modify By: Pinkal
'    ''' </summary>
'    Public Property _Groupname() As String
'        Get
'            Return mstrGroupname
'        End Get
'        Set(ByVal value As String)
'            mstrGroupname = Value
'        End Set
'    End Property

'    ''' <summary>
'    ''' Purpose: Get or Set grouptype_id
'    ''' Modify By: Pinkal
'    ''' </summary>
'    Public Property _Grouptype_Id() As Integer
'        Get
'            Return mintGrouptype_Id
'        End Get
'        Set(ByVal value As Integer)
'            mintGrouptype_Id = Value
'        End Set
'    End Property

'    ''' <summary>
'    ''' Purpose: Get or Set website
'    ''' Modify By: Pinkal
'    ''' </summary>
'    Public Property _Website() As String
'        Get
'            Return mstrWebsite
'        End Get
'        Set(ByVal value As String)
'            mstrWebsite = value
'        End Set
'    End Property

'    ''' <summary>
'    ''' Purpose: Get or Set description
'    ''' Modify By: Pinkal
'    ''' </summary>
'    Public Property _Description() As String
'        Get
'            Return mstrDescription
'        End Get
'        Set(ByVal value As String)
'            mstrDescription = Value
'        End Set
'    End Property

'    ''' <summary>
'    ''' Purpose: Get or Set isactive
'    ''' Modify By: Pinkal
'    ''' </summary>
'    Public Property _Isactive() As Boolean
'        Get
'            Return mblnIsactive
'        End Get
'        Set(ByVal value As Boolean)
'            mblnIsactive = Value
'        End Set
'    End Property

'    ''' <summary>
'    ''' Purpose: Get or Set groupname1
'    ''' Modify By: Pinkal
'    ''' </summary>
'    Public Property _Groupname1() As String
'        Get
'            Return mstrGroupname1
'        End Get
'        Set(ByVal value As String)
'            mstrGroupname1 = Value
'        End Set
'    End Property

'    ''' <summary>
'    ''' Purpose: Get or Set groupname2
'    ''' Modify By: Pinkal
'    ''' </summary>
'    Public Property _Groupname2() As String
'        Get
'            Return mstrGroupname2
'        End Get
'        Set(ByVal value As String)
'            mstrGroupname2 = Value
'        End Set
'    End Property

'#End Region

'    ''' <summary>
'    ''' Modify By: Pinkal
'    ''' </summary>
'    ''' <purpose> Assign all Property variable </purpose>
'    Public Sub GetData()
'        Dim dsList As DataSet = Nothing
'        Dim strQ As String = ""
'        Dim exForce As Exception

'        objDataOperation = New clsDataOperation

'        Try
'            strQ = "SELECT " & _
'              "  groupmasterunkid " & _
'              ", groupalias " & _
'              ", groupcode " & _
'              ", groupname " & _
'              ", grouptype_id " & _
'              ", description " & _
'              ", website " & _
'              ", isactive " & _
'              ", groupname1 " & _
'              ", groupname2 " & _
'             "FROM prpayrollgroup_master " & _
'             "WHERE groupmasterunkid = @groupmasterunkid "

'            objDataOperation.AddParameter("@groupmasterunkid", SqlDbType.int, eZeeDataType.INT_SIZE, mintGroupmasterUnkId.ToString)

'            dsList = objDataOperation.ExecQuery(strQ, "List")

'            If objDataOperation.ErrorMessage <> "" Then
'                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
'                Throw exForce
'            End If

'            For Each dtRow As DataRow In dsList.Tables(0).Rows
'                mintgroupmasterunkid = CInt(dtRow.Item("groupmasterunkid"))
'                mstrgroupalias = dtRow.Item("groupalias").ToString
'                mstrgroupcode = dtRow.Item("groupcode").ToString
'                mstrgroupname = dtRow.Item("groupname").ToString
'                mintgrouptype_id = CInt(dtRow.Item("grouptype_id"))
'                mstrDescription = dtRow.Item("description").ToString
'                mstrWebsite = dtRow.Item("website").ToString
'                mblnisactive = CBool(dtRow.Item("isactive"))
'                mstrgroupname1 = dtRow.Item("groupname1").ToString
'                mstrgroupname2 = dtRow.Item("groupname2").ToString
'                Exit For
'            Next
'        Catch ex As Exception
'            Throw New Exception(ex.Message & "; Procedure Name: GetData; Module Name: " & mstrModuleName)
'        Finally
'            exForce = Nothing
'            If dsList IsNot Nothing Then dsList.Dispose()
'            objDataOperation = Nothing
'        End Try
'    End Sub


'    ''' <summary>
'    ''' Modify By: Pinkal
'    ''' </summary>
'    ''' <purpose> Assign all Property variable </purpose>
'    Public Function GetList(ByVal strTableName As String, Optional ByVal blnOnlyActive As Boolean = True) As DataSet
'        Dim dsList As DataSet = Nothing
'        Dim strQ As String = ""
'        Dim exForce As Exception

'        objDataOperation = New clsDataOperation

'        Try

'            strQ &= "SELECT " & _
'              "  groupmasterunkid " & _
'              ", groupalias " & _
'              ", groupcode " & _
'              ", groupname " & _
'              ", grouptype_id " & _
'              ", description " & _
'              ", website " & _
'              ", isactive " & _
'              ", groupname1 " & _
'              ", groupname2 " & _
'             "FROM prpayrollgroup_master "

'            If blnOnlyActive Then
'                strQ &= " WHERE isactive = 1 "
'            End If

'            dsList = objDataOperation.ExecQuery(strQ, strTableName)

'            If objDataOperation.ErrorMessage <> "" Then
'                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
'                Throw exForce
'            End If

'        Catch ex As Exception
'            Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
'        Finally
'            exForce = Nothing
'            If dsList IsNot Nothing Then dsList.Dispose()
'            objDataOperation = Nothing
'        End Try
'        Return dsList
'    End Function


'    ''' <summary>
'    ''' Modify By: Pinkal
'    ''' </summary>
'    ''' <returns>Boolean</returns>
'    ''' <purpose> INSERT INTO Database Table (prpayrollgroup_master) </purpose>
'    Public Function Insert() As Boolean
'        If isExist(mstrGroupcode) Then
'            mstrMessage = Language.getMessage(mstrModuleName, 1, "This Code is already defined. Please define new Code.")
'            Return False
'        ElseIf isExist("", mstrGroupname) Then
'            mstrMessage = Language.getMessage(mstrModuleName, 2, "This Name is already defined. Please define new Name.")
'            Return False
'        End If

'        Dim dsList As DataSet = Nothing
'        Dim strQ As String = ""
'        Dim exForce As Exception

'        objDataOperation = New clsDataOperation

'        Try
'            objDataOperation.AddParameter("@groupalias", SqlDbType.nvarchar, eZeeDataType.NAME_SIZE, mstrgroupalias.ToString)
'            objDataOperation.AddParameter("@groupcode", SqlDbType.nvarchar, eZeeDataType.NAME_SIZE, mstrgroupcode.ToString)
'            objDataOperation.AddParameter("@groupname", SqlDbType.nvarchar, eZeeDataType.NAME_SIZE, mstrgroupname.ToString)
'            objDataOperation.AddParameter("@grouptype_id", SqlDbType.int, eZeeDataType.INT_SIZE, mintgrouptype_id.ToString)
'            objDataOperation.AddParameter("@description", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrDescription.ToString)
'            objDataOperation.AddParameter("@website", SqlDbType.NVarChar, eZeeDataType.WEBSITE_SIZE, mstrWebsite.ToString)
'            objDataOperation.AddParameter("@isactive", SqlDbType.bit, eZeeDataType.BIT_SIZE, mblnisactive.ToString)
'            objDataOperation.AddParameter("@groupname1", SqlDbType.nvarchar, eZeeDataType.NAME_SIZE, mstrgroupname1.ToString)
'            objDataOperation.AddParameter("@groupname2", SqlDbType.nvarchar, eZeeDataType.NAME_SIZE, mstrgroupname2.ToString)

'            strQ = "INSERT INTO prpayrollgroup_master ( " & _
'              "  groupalias " & _
'              ", groupcode " & _
'              ", groupname " & _
'              ", grouptype_id " & _
'              ", description " & _
'              ", website " & _
'              ", isactive " & _
'              ", groupname1 " & _
'              ", groupname2" & _
'            ") VALUES (" & _
'              "  @groupalias " & _
'              ", @groupcode " & _
'              ", @groupname " & _
'              ", @grouptype_id " & _
'              ", @description " & _
'              ", @website " & _
'              ", @isactive " & _
'              ", @groupname1 " & _
'              ", @groupname2" & _
'            "); SELECT @@identity"

'            dsList = objDataOperation.ExecQuery(strQ, "List")

'            If objDataOperation.ErrorMessage <> "" Then
'                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
'                Throw exForce
'            End If

'            mintGroupmasterUnkId = dsList.Tables(0).Rows(0).Item(0)

'            Return True
'        Catch ex As Exception
'            Throw New Exception(ex.Message & "; Procedure Name: Insert; Module Name: " & mstrModuleName)
'            Return False
'        Finally
'            exForce = Nothing
'            If dsList IsNot Nothing Then dsList.Dispose()
'            objDataOperation = Nothing
'        End Try
'    End Function

'    ''' <summary>
'    ''' Modify By: Pinkal
'    ''' </summary>
'    ''' <returns>Boolean</returns>
'    ''' <purpose> Update Database Table (prpayrollgroup_master) </purpose>
'    Public Function Update() As Boolean

'        If isExist(mstrGroupcode, "", mintGroupmasterunkid) Then
'            mstrMessage = Language.getMessage(mstrModuleName, 1, "This Code is already defined. Please define new Code.")
'            Return False
'        ElseIf isExist("", mstrGroupname, mintGroupmasterunkid) Then
'            mstrMessage = Language.getMessage(mstrModuleName, 2, "This Name is already defined. Please define new Name.")
'            Return False
'        End If

'        Dim dsList As DataSet = Nothing
'        Dim strQ As String = ""
'        Dim exForce As Exception

'        objDataOperation = New clsDataOperation

'        Try
'            objDataOperation.AddParameter("@groupmasterunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintGroupmasterunkid.ToString)
'            objDataOperation.AddParameter("@groupalias", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrGroupalias.ToString)
'            objDataOperation.AddParameter("@groupcode", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrGroupcode.ToString)
'            objDataOperation.AddParameter("@groupname", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrGroupname.ToString)
'            objDataOperation.AddParameter("@grouptype_id", SqlDbType.Int, eZeeDataType.INT_SIZE, mintGrouptype_Id.ToString)
'            objDataOperation.AddParameter("@description", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrDescription.ToString)
'            objDataOperation.AddParameter("@website", SqlDbType.NVarChar, eZeeDataType.WEBSITE_SIZE, mstrWebsite.ToString)
'            objDataOperation.AddParameter("@isactive", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsactive.ToString)
'            objDataOperation.AddParameter("@groupname1", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrGroupname1.ToString)
'            objDataOperation.AddParameter("@groupname2", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrGroupname2.ToString)

'            strQ = "UPDATE prpayrollgroup_master SET " & _
'              "  groupalias = @groupalias" & _
'              ", groupcode = @groupcode" & _
'              ", groupname = @groupname" & _
'              ", grouptype_id = @grouptype_id" & _
'              ", description = @description" & _
'              ", website = @website " & _
'              ", isactive = @isactive" & _
'              ", groupname1 = @groupname1" & _
'              ", groupname2 = @groupname2 " & _
'            "WHERE groupmasterunkid = @groupmasterunkid "

'            Call objDataOperation.ExecNonQuery(strQ)

'            If objDataOperation.ErrorMessage <> "" Then
'                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
'                Throw exForce
'            End If

'            Return True
'        Catch ex As Exception
'            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
'            Return False
'        Finally
'            exForce = Nothing
'            If dsList IsNot Nothing Then dsList.Dispose()
'            objDataOperation = Nothing
'        End Try
'    End Function

'    ''' <summary>
'    ''' Modify By: Pinkal
'    ''' </summary>
'    ''' <returns>Boolean</returns>
'    ''' <purpose> Delete Database Table (prpayrollgroup_master) </purpose>
'    Public Function Delete(ByVal intUnkid As Integer) As Boolean
'        'If isUsed(intUnkid) Then
'        '    mstrMessage = "<Message>"
'        '    Return False
'        'End If

'        Dim dsList As DataSet = Nothing
'        Dim strQ As String = ""
'        Dim exForce As Exception

'        objDataOperation = New clsDataOperation

'        Try
'            StrQ = "DELETE FROM prpayrollgroup_master " & _
'            "WHERE groupmasterunkid = @groupmasterunkid "

'            objDataOperation.AddParameter("@groupmasterunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

'            Call objDataOperation.ExecNonQuery(strQ)

'            If objDataOperation.ErrorMessage <> "" Then
'                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
'                Throw exForce
'            End If

'            Return True
'        Catch ex As Exception
'            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
'            Return False
'        Finally
'            exForce = Nothing
'            If dsList IsNot Nothing Then dsList.Dispose()
'            objDataOperation = Nothing
'        End Try
'    End Function

'    ''' <summary>
'    ''' Modify By: Pinkal
'    ''' </summary>
'    ''' <purpose> Assign all Property variable </purpose>
'    Public Function isUsed(ByVal intUnkid As Integer) As Boolean
'        Dim dsList As DataSet = Nothing
'        Dim strQ As String = ""
'        Dim exForce As Exception

'        objDataOperation = New clsDataOperation

'        Try
'            StrQ = "<Query>"

'            objDataOperation.AddParameter("@groupmasterunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

'            dsList = objDataOperation.ExecQuery(strQ, "List")

'            If objDataOperation.ErrorMessage <> "" Then
'                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
'                Throw exForce
'            End If

'            Return dsList.tables(0).rows.count > 0
'        Catch ex As Exception
'            Throw New Exception(ex.Message & "; Procedure Name: isUsed; Module Name: " & mstrModuleName)
'        Finally
'            exForce = Nothing
'            If dsList IsNot Nothing Then dsList.Dispose()
'            objDataOperation = Nothing
'        End Try
'    End Function

'    ''' <summary>
'    ''' Modify By: Pinkal
'    ''' </summary>
'    ''' <purpose> Assign all Property variable </purpose>
'    Public Function isExist(Optional ByVal strCode As String = "", Optional ByVal strName As String = "", Optional ByVal intUnkid As Integer = -1) As Boolean
'        Dim dsList As DataSet = Nothing
'        Dim strQ As String = ""
'        Dim exForce As Exception

'        objDataOperation = New clsDataOperation

'        Try
'            strQ = "SELECT " & _
'              "  groupmasterunkid " & _
'              ", groupalias " & _
'              ", groupcode " & _
'              ", groupname " & _
'              ", grouptype_id " & _
'              ", description " & _
'              ", website " & _
'              ", isactive " & _
'              ", groupname1 " & _
'              ", groupname2 " & _
'             "FROM prpayrollgroup_master " & _
'             "WHERE 1=1 AND grouptype_id=@grouptype_id"

'            If strCode.Length > 0 Then
'                strQ &= " AND groupcode=@groupcode "
'            End If

'            If strName.Length > 0 Then
'                strQ &= " AND groupname=@groupname "
'            End If

'            If intUnkid > 0 Then
'                strQ &= " AND groupmasterunkid <> @groupmasterunkid "
'            End If

'            objDataOperation.AddParameter("@groupcode", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strCode)
'            objDataOperation.AddParameter("@groupname", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strName)
'            objDataOperation.AddParameter("@grouptype_id", SqlDbType.Int, eZeeDataType.INT_SIZE, mintGrouptype_Id)
'            objDataOperation.AddParameter("@groupmasterunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)


'            dsList = objDataOperation.ExecQuery(strQ, "List")

'            If objDataOperation.ErrorMessage <> "" Then
'                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
'                Throw exForce
'            End If

'            Return dsList.Tables(0).Rows.Count > 0
'        Catch ex As Exception
'            Throw New Exception(ex.Message & "; Procedure Name: isExist; Module Name: " & mstrModuleName)
'        Finally
'            exForce = Nothing
'            If dsList IsNot Nothing Then dsList.Dispose()
'            objDataOperation = Nothing
'        End Try
'    End Function

'    ''' <summary>
'    ''' Modify By: Pinkal
'    ''' </summary>
'    ''' <purpose> Assign all Property variable </purpose>
'    Public Function getListForCombo(ByVal mintGroupTypeId As Integer, Optional ByVal strListName As String = "List", Optional ByVal mblFlag As Boolean = False) As DataSet
'        Dim dsList As New DataSet
'        Dim objDataOperation As New clsDataOperation
'        Dim strQ As String = String.Empty
'        Dim exForce As Exception
'        Try
'            If mblFlag = True Then
'                strQ = "SELECT 0 as groupmasterunkid, ' ' +  @name  as name UNION "
'            End If
'            strQ &= "SELECT groupmasterunkid, groupname as name FROM prpayrollgroup_master where isactive = 1 and grouptype_id = @grouptype_id ORDER BY name "

'            objDataOperation.AddParameter("@name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 2, "Select Group"))
'            objDataOperation.AddParameter("@grouptype_id", SqlDbType.Int, eZeeDataType.INT_SIZE, mintGroupTypeId)

'            dsList = objDataOperation.ExecQuery(strQ, strListName)

'            If objDataOperation.ErrorMessage <> "" Then
'                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
'                Throw exForce
'            End If
'            Return dsList
'        Catch ex As Exception
'            DisplayError.Show(-1, ex.Message, "getListForCombo", mstrModuleName)
'            Return Nothing
'        Finally
'            exForce = Nothing
'            objDataOperation = Nothing
'            dsList.Dispose()
'            dsList = Nothing
'        End Try

'    End Function

'End Class