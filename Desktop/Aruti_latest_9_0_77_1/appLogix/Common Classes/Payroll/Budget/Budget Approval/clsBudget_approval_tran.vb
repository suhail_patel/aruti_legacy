﻿'************************************************************************************************************************************
'Class Name : clsBudget_approval_tran.vb
'Purpose    :
'Date       :11/08/2016
'Written By :Sohail
'Modified   :
'************************************************************************************************************************************

Imports eZeeCommonLib
Imports System.Text
Imports eZee.Common.eZeeForm
Imports System.Web


''' <summary>
''' Purpose: 
''' Developer: Sohail
''' </summary>
Public Class clsBudget_approval_tran
    Private Shared ReadOnly mstrModuleName As String = "clsBudget_approval_tran"
    Dim objDataOperation As clsDataOperation
    Dim mstrMessage As String = ""

#Region " Private variables "
    Private mintBudgetapprovaltranunkid As Integer
    Private mintBudgetunkid As Integer
    Private mintLevelunkid As Integer
    Private mintPriority As Integer
    Private mdtApproval_Date As Date
    Private mintStatusunkid As Integer
    Private mstrDisapprovalreason As String = String.Empty
    Private mstrRemarks As String = String.Empty
    Private mintUserunkid As Integer
    Private mblnIsvoid As Boolean
    Private mdtVoiddatetime As Date
    Private mintVoiduserunkid As Integer
    Private mstrVoidreason As String = String.Empty

    'Private mstrWebFormName As String = String.Empty
    Private mintLogEmployeeUnkid As Integer = -1
    'Private mstrWebIP As String = ""
    'Private mstrWebhostName As String = ""

    Private mdtTable As DataTable
#End Region

#Region " Constructor "

    Public Sub New()
        Try
            mdtTable = New DataTable

            mdtTable.Columns.Add("budgetapprovaltranunkid", System.Type.GetType("System.Int32")).DefaultValue = -1
            mdtTable.Columns.Add("budgetunkid", System.Type.GetType("System.Int32")).DefaultValue = -1
            mdtTable.Columns.Add("levelunkid", System.Type.GetType("System.Int32")).DefaultValue = -1
            mdtTable.Columns.Add("priority", System.Type.GetType("System.Int32")).DefaultValue = -1
            mdtTable.Columns.Add("approval_date", System.Type.GetType("System.DateTime"))
            mdtTable.Columns.Add("statusunkid", System.Type.GetType("System.Int32")).DefaultValue = -1
            mdtTable.Columns.Add("remarks", System.Type.GetType("System.String")).DefaultValue = String.Empty
            mdtTable.Columns.Add("userunkid", System.Type.GetType("System.Int32")).DefaultValue = -1
            mdtTable.Columns.Add("disapprovalreason", System.Type.GetType("System.String")).DefaultValue = String.Empty
            mdtTable.Columns.Add("isvoid", System.Type.GetType("System.Boolean")).DefaultValue = False
            mdtTable.Columns.Add("voiduserunkid", System.Type.GetType("System.Int32")).DefaultValue = -1
            mdtTable.Columns.Add("voiddatetime", System.Type.GetType("System.DateTime"))
            mdtTable.Columns.Add("voidreason", System.Type.GetType("System.String")).DefaultValue = String.Empty

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: New; Module Name: " & mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Properties "

    'S.SANDEEP [28-May-2018] -- START 
    'ISSUE/ENHANCEMENT : {Audit Trails} 
    Private mstrFormName As String = String.Empty
    Public WriteOnly Property _FormName() As String
        Set(ByVal value As String)
            mstrFormName = value
        End Set
    End Property

    Private mstrClientIP As String = ""
    Public WriteOnly Property _ClientIP() As String
        Set(ByVal value As String)
            mstrClientIP = value
        End Set
    End Property

    Private mstrHostName As String = ""
    Public WriteOnly Property _HostName() As String
        Set(ByVal value As String)
            mstrHostName = value
        End Set
    End Property

    Private mblnIsWeb As Boolean = False
    Public WriteOnly Property _FromWeb() As Boolean
        Set(ByVal value As Boolean)
            mblnIsWeb = value
        End Set
    End Property

    Private mintAuditUserId As Integer = 0
    Public WriteOnly Property _AuditUserId() As Integer
        Set(ByVal value As Integer)
            mintAuditUserId = value
        End Set
    End Property

    Private mintCompanyUnkid As Integer = 0
    Public WriteOnly Property _CompanyUnkid() As Integer
        Set(ByVal value As Integer)
            mintCompanyUnkid = value
        End Set
    End Property

    Private mdtAuditDate As DateTime = Now
    Public WriteOnly Property _AuditDate() As DateTime
        Set(ByVal value As DateTime)
            mdtAuditDate = value
        End Set
    End Property

    Private xDataOpr As clsDataOperation = Nothing
    Public Property _xDataOpr() As clsDataOperation
        Get
            Return xDataOpr
        End Get
        Set(ByVal value As clsDataOperation)
            xDataOpr = value
        End Set
    End Property

    Private mintLoginEmployeeunkid As Integer = 0
    Public Property _LoginEmployeeunkid() As Integer
        Get
            Return mintLoginEmployeeunkid
        End Get
        Set(ByVal value As Integer)
            mintLoginEmployeeunkid = value
        End Set
    End Property

    Private mintVoidlogingemployeeunkid As Integer = 0
    Public Property _Voidloginemployeeunkid() As Integer
        Get
            Return mintVoidlogingemployeeunkid
        End Get
        Set(ByVal value As Integer)
            mintVoidlogingemployeeunkid = value
        End Set
    End Property
    'S.SANDEEP [28-May-2018] -- END 

    Public ReadOnly Property _Message() As String
        Get
            Return mstrMessage
        End Get
    End Property

    ''' <summary>
    ''' Purpose: Get or Set budgetapprovaltranunkid
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Budgetapprovaltranunkid() As Integer
        Get
            Return mintBudgetapprovaltranunkid
        End Get
        Set(ByVal value As Integer)
            mintBudgetapprovaltranunkid = value
            Call GetData()
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set budgetunkid
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Budgetunkid() As Integer
        Get
            Return mintBudgetunkid
        End Get
        Set(ByVal value As Integer)
            mintBudgetunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set levelunkid
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Levelunkid() As Integer
        Get
            Return mintLevelunkid
        End Get
        Set(ByVal value As Integer)
            mintLevelunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set priority
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Priority() As Integer
        Get
            Return mintPriority
        End Get
        Set(ByVal value As Integer)
            mintPriority = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set approval_date
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Approval_Date() As Date
        Get
            Return mdtApproval_Date
        End Get
        Set(ByVal value As Date)
            mdtApproval_Date = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set statusunkid
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Statusunkid() As Integer
        Get
            Return mintStatusunkid
        End Get
        Set(ByVal value As Integer)
            mintStatusunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set disapprovalreason
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Disapprovalreason() As String
        Get
            Return mstrDisapprovalreason
        End Get
        Set(ByVal value As String)
            mstrDisapprovalreason = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set remarks
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Remarks() As String
        Get
            Return mstrRemarks
        End Get
        Set(ByVal value As String)
            mstrRemarks = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set userunkid
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Userunkid() As Integer
        Get
            Return mintUserunkid
        End Get
        Set(ByVal value As Integer)
            mintUserunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isvoid
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Isvoid() As Boolean
        Get
            Return mblnIsvoid
        End Get
        Set(ByVal value As Boolean)
            mblnIsvoid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiddatetime
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Voiddatetime() As Date
        Get
            Return mdtVoiddatetime
        End Get
        Set(ByVal value As Date)
            mdtVoiddatetime = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiduserunkid
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Voiduserunkid() As Integer
        Get
            Return mintVoiduserunkid
        End Get
        Set(ByVal value As Integer)
            mintVoiduserunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voidreason
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Voidreason() As String
        Get
            Return mstrVoidreason
        End Get
        Set(ByVal value As String)
            mstrVoidreason = value
        End Set
    End Property

    'Public Property _WebFormName() As String
    '    Get
    '        Return mstrWebFormName
    '    End Get
    '    Set(ByVal value As String)
    '        mstrWebFormName = value
    '    End Set
    'End Property

    'Public WriteOnly Property _WebIP() As String
    '    Set(ByVal value As String)
    '        mstrWebIP = value
    '    End Set
    'End Property

    'Public WriteOnly Property _WebHostName() As String
    '    Set(ByVal value As String)
    '        mstrWebhostName = value
    '    End Set
    'End Property

#End Region

    ''' <summary>
    ''' Modify By: Sohail
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Sub GetData()
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "SELECT " & _
              "  budgetapprovaltranunkid " & _
              ", budgetunkid " & _
              ", levelunkid " & _
              ", priority " & _
              ", approval_date " & _
              ", statusunkid " & _
              ", disapprovalreason " & _
              ", remarks " & _
              ", userunkid " & _
              ", isvoid " & _
              ", voiddatetime " & _
              ", voiduserunkid " & _
              ", voidreason " & _
             "FROM bgbudget_approval_tran " & _
             "WHERE budgetapprovaltranunkid = @budgetapprovaltranunkid "

            objDataOperation.AddParameter("@budgetapprovaltranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintBudgetapprovaltranunkid.ToString)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For Each dtRow As DataRow In dsList.Tables(0).Rows
                mintBudgetapprovaltranunkid = CInt(dtRow.Item("budgetapprovaltranunkid"))
                mintBudgetunkid = CInt(dtRow.Item("budgetunkid"))
                mintLevelunkid = CInt(dtRow.Item("levelunkid"))
                mintPriority = CInt(dtRow.Item("priority"))
                mdtApproval_Date = dtRow.Item("approval_date")
                mintStatusunkid = CInt(dtRow.Item("statusunkid"))
                mstrDisapprovalreason = dtRow.Item("disapprovalreason").ToString
                mstrRemarks = dtRow.Item("remarks").ToString
                mintUserunkid = CInt(dtRow.Item("userunkid"))
                mblnIsvoid = CBool(dtRow.Item("isvoid"))
                If IsDBNull(dtRow.Item("voiddatetime")) = True Then
                    mdtVoiddatetime = Nothing
                Else
                    mdtVoiddatetime = dtRow.Item("voiddatetime")
                End If
                mintVoiduserunkid = CInt(dtRow.Item("voiduserunkid"))
                mstrVoidreason = dtRow.Item("voidreason").ToString
                Exit For
            Next
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetData; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Sub


    ''' <summary>
    ''' Modify By: Sohail
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function GetList(ByVal strTableName As String _
                            , ByVal intApproverUserUnkId As Integer _
                            , Optional ByVal intBudgetApprovalTranUnkid As Integer = 0 _
                            , Optional ByVal strBudgetUnkIDs As String = "" _
                            , Optional ByVal intLevelUnkID As Integer = 0 _
                            , Optional ByVal intPriority As Integer = 0 _
                            , Optional ByVal blnAddPendingRecord As Boolean = False _
                            , Optional ByVal intStatusId As Integer = 0 _
                            , Optional ByVal strFilter As String = "" _
                            ) As DataSet

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim strBudgetFilter As String = ""

        objDataOperation = New clsDataOperation

        Try

            If strBudgetUnkIDs.Trim <> "" Then
                strBudgetFilter &= " AND bgbudget_master.budgetunkid IN ( " & strBudgetUnkIDs & " ) "
            End If

            If intStatusId > 0 Then
                strBudgetFilter &= " AND bgbudget_master.budget_statusunkid =  " & intStatusId & " "
            End If

            ''''''
            Dim objApproverMap As New clsbudget_approver_mapping
            Dim objApproverLevel As New clsbudgetapproverlevel_master
            Dim ds As DataSet
            Dim strLowerLevelUserIDs As String = ""
            Dim intCurrPriority As Integer = 0
            ds = objApproverMap.GetList("ApproverLevel", intApproverUserUnkId)
            If ds.Tables("ApproverLevel").Rows.Count <= 0 Then Exit Try

            intCurrPriority = CInt(ds.Tables("ApproverLevel").Rows(0).Item("priority"))
            Dim intLowerPriority As Integer = objApproverLevel.GetLowerLevelPriority(intCurrPriority)
            If intLowerPriority >= 0 Then
                strLowerLevelUserIDs = objApproverMap.GetApproverUnkIDs(intLowerPriority)
            Else
                'strLowerLevelUserIDs = "-1"
            End If
            '''''''

            Dim objMaster As New clsMasterData
            Dim dsCombo As DataSet = objMaster.getComboListForBudgetViewBy("ViewBy")
            Dim dicViewBy As Dictionary(Of Integer, String) = (From p In dsCombo.Tables("ViewBy") Select New With {.id = CInt(p.Item("id")), .name = p.Item("name").ToString}).ToDictionary(Function(x) x.id, Function(x) x.name)
            dsCombo = objMaster.GetEAllocation_Notification("Allocation")
            Dim dicAllocation As Dictionary(Of Integer, String) = (From p In dsCombo.Tables("Allocation") Select New With {.id = CInt(p.Item("id")), .name = p.Item("name").ToString}).ToDictionary(Function(x) x.id, Function(x) x.name)
            dsCombo = objMaster.getComboListForBudgetPresentation("Presentation")
            Dim dicPresentation As Dictionary(Of Integer, String) = (From p In dsCombo.Tables("Presentation") Select New With {.id = CInt(p.Item("id")), .name = p.Item("name").ToString}).ToDictionary(Function(x) x.id, Function(x) x.name)
            dsCombo = objMaster.getComboListForBudgetWhoToInclude("WhoToInclude")
            Dim dicWhoToInclude As Dictionary(Of Integer, String) = (From p In dsCombo.Tables("WhoToInclude") Select New With {.id = CInt(p.Item("id")), .name = p.Item("name").ToString}).ToDictionary(Function(x) x.id, Function(x) x.name)
            dsCombo = objMaster.getComboListForBudgetSalaryLevel("SalaryLevel", , True)
            Dim dicSalaryLevel As Dictionary(Of Integer, String) = (From p In dsCombo.Tables("SalaryLevel") Select New With {.id = CInt(p.Item("id")), .name = p.Item("name").ToString}).ToDictionary(Function(x) x.id, Function(x) x.name)
            dsCombo = objMaster.getApprovalStatus("ApprovalStatus", True, , True, True, True, True, False)
            Dim mdicApprovalStatus As Dictionary(Of Integer, String) = (From p In dsCombo.Tables("ApprovalStatus").AsEnumerable Select New With {Key .ID = CInt(p.Item("Id")), Key .NAME = p.Item("name").ToString}).ToDictionary(Function(x) x.ID, Function(x) x.NAME)

            If blnAddPendingRecord = True Then


                strQ = "SELECT  B.budgetapprovaltranunkid " & _
                              ", B.budgetunkid " & _
                              ", budget_code " & _
                              ", budget_name " & _
                              ", payyearunkid " & _
                              ", payyear " & _
                              ", viewbyid " & _
                              ", allocationbyid " & _
                              ", presentationmodeid " & _
                              ", whotoincludeid " & _
                              ", salarylevelid " & _
                              ", levelunkid " & _
                              ", levelcode " & _
                              ", levelname " & _
                              ", priority " & _
                              ", NULL AS approval_date " & _
                              ", '' AS strapproval_date " & _
                              ", 1 AS statusunkid " & _
                              ", '' AS disapprovalreason " & _
                              ", '' AS remarks " & _
                              ", userunkid AS approveruserunkid " & _
                              ", username AS approverusername " & _
                              ", 0 AS isvoid " & _
                              ", NULL AS voiddatetime " & _
                              ", -1 AS voiduserunkid " & _
                              ", '' AS voidreason "

                strQ &= ", CASE viewbyid "
                For Each pair In dicViewBy
                    strQ &= " WHEN " & pair.Key & "  THEN '" & pair.Value & "' "
                Next
                strQ &= " END AS ViewBy "

                strQ &= ", CASE allocationbyid "
                For Each pair In dicAllocation
                    strQ &= " WHEN " & pair.Key & "  THEN '" & pair.Value & "' "
                Next
                strQ &= " WHEN 0  THEN 'Employee' "
                strQ &= " END AS AllocationBy "

                strQ &= ", CASE presentationmodeid "
                For Each pair In dicPresentation
                    strQ &= " WHEN " & pair.Key & "  THEN '" & pair.Value & "' "
                Next
                strQ &= " END AS PresentationMode "

                strQ &= ", CASE whotoincludeid "
                For Each pair In dicWhoToInclude
                    strQ &= " WHEN " & pair.Key & "  THEN '" & pair.Value & "' "
                Next
                strQ &= " END AS WhoToInclude "

                strQ &= ", CASE salarylevelid "
                For Each pair In dicSalaryLevel
                    strQ &= " WHEN " & pair.Key & "  THEN '" & pair.Value & "' "
                Next
                strQ &= " END AS SalaryLevel "

                If mdicApprovalStatus.ContainsKey(1) = True Then
                    strQ &= ", '" & mdicApprovalStatus.Item(1) & "' AS Status "
                Else
                    strQ &= ", '' AS Status "
                End If

                strQ &= "FROM    ( SELECT  DISTINCT " & _
                                            "0 AS budgetapprovaltranunkid " & _
                                              ", bgbudget_master.budgetunkid " & _
                                              ", bgbudget_master.budget_code " & _
                                              ", bgbudget_master.budget_name " & _
                                              ", bgbudget_master.payyearunkid " & _
                                              ", bgbudget_master.payyearname AS payyear " & _
                                              ", bgbudget_master.viewbyid " & _
                                              ", bgbudget_master.allocationbyid " & _
                                              ", bgbudget_master.presentationmodeid " & _
                                              ", bgbudget_master.whotoincludeid " & _
                                              ", bgbudget_master.salarylevelid " & _
                                              ", A.levelunkid " & _
                                              ", A.levelcode " & _
                                              ", A.levelname " & _
                                              ", A.priority " & _
                                              ", NULL AS approval_date " & _
                                              ", '' AS strapproval_date " & _
                                              ", 1 AS statusunkid " & _
                                              ", '' AS disapprovalreason " & _
                                              ", '' AS remarks " & _
                                              ", A.userunkid " & _
                                              ", A.username " & _
                                              ", 0 AS isvoid " & _
                                              ", NULL AS voiddatetime " & _
                                              ", -1 AS voiduserunkid " & _
                                              ", '' AS voidreason " & _
                                        "FROM    bgbudget_master " & _
                                                "LEFT JOIN bgbudget_approval_tran ON bgbudget_approval_tran.budgetunkid = bgbudget_master.budgetunkid " & _
                                                        "AND bgbudget_approval_tran.isvoid = 0 "

                If intCurrPriority > 0 Then
                    strQ &= " AND bgbudget_approval_tran.priority = @currpriority "
                    objDataOperation.AddParameter("@currpriority", SqlDbType.Int, eZeeDataType.INT_SIZE, intCurrPriority)
                End If

                strQ &= "        LEFT JOIN bgbudget_approver_mapping ON bgbudget_approver_mapping.levelunkid = bgbudget_approval_tran.levelunkid " & _
                                "LEFT JOIN bgbudgetapproverlevel_master ON bgbudget_approver_mapping.levelunkid = bgbudgetapproverlevel_master.levelunkid " & _
                                "LEFT JOIN hrmsConfiguration..cfuser_master ON cfuser_master.userunkid = bgbudget_approver_mapping.userapproverunkid " & _
                                "LEFT JOIN ( SELECT DISTINCT " & _
                                                    "bgbudgetapproverlevel_master.levelunkid " & _
                                                  ", bgbudgetapproverlevel_master.levelcode " & _
                                                  ", bgbudgetapproverlevel_master.levelname " & _
                                                  ", bgbudgetapproverlevel_master.priority " & _
                                                  ", bgbudget_approver_mapping.userapproverunkid AS userunkid " & _
                                                  ", cfuser_master.username " & _
                                            "FROM    bgbudget_approver_mapping " & _
                                                    "LEFT JOIN bgbudgetapproverlevel_master ON bgbudget_approver_mapping.levelunkid = bgbudgetapproverlevel_master.levelunkid " & _
                                                    "LEFT JOIN hrmsConfiguration..cfuser_master ON cfuser_master.userunkid = bgbudget_approver_mapping.userapproverunkid " & _
                                            "WHERE   ISNULL(bgbudget_approver_mapping.isvoid, 0) = 0 " & _
                                                    "AND ISNULL(bgbudgetapproverlevel_master.isvoid, 0) = 0 " & _
                                                    "AND bgbudget_approver_mapping.userapproverunkid = @userapproverunkid " & _
                                          ") AS A ON 1 = 1 " & _
                        "WHERE   ISNULL(bgbudget_master.isvoid, 0) = 0 " & _
                                "AND ISNULL(bgbudget_approver_mapping.isvoid, 0) = 0 " & _
                                "AND ISNULL(bgbudgetapproverlevel_master.isvoid, 0) = 0 " & _
                                "AND ISNULL(bgbudget_approval_tran.isvoid, 0) = 0 " & _
                                "AND ISNULL(bgbudget_approval_tran.statusunkid, 0) <> " & enApprovalStatus.PENDING & " " & _
                                "/*AND bgbudgetapproverlevel_master.priority = A.priority*/ " & _
                                "AND bgbudget_approval_tran.budgetapprovaltranunkid IS NULL "


                strQ &= strBudgetFilter

                If strLowerLevelUserIDs.Trim <> "" Then
                    'strQ &= " AND bgbudget_approver_mapping.userapproverunkid IN (" & strLowerLevelUserIDs & ") "
                    '" AND bgbudget_approval_tran.userunkid = bgbudget_approver_mapping.userapproverunkid "
                Else
                    'strQ &= " AND bgbudget_approver_mapping.userapproverunkid = @userapproverunkid "

                End If

                strQ &= ") B "

                If intLowerPriority > 0 Then

                    strQ &= "JOIN ( SELECT   bgbudget_master.budgetunkid " & _
                                   "FROM     bgbudget_master " & _
                                            "LEFT JOIN bgbudget_approval_tran ON bgbudget_approval_tran.budgetunkid = bgbudget_master.budgetunkid " & _
                                   "WHERE    ISNULL(dbo.bgbudget_master.isvoid, 0) = 0 " & _
                                            "AND ISNULL(dbo.bgbudget_approval_tran.isvoid, 0) = 0 " & _
                                            "AND bgbudget_approval_tran.priority = @lowerpriority " & _
                                            "AND statusunkid <> 1 "

                    strQ &= strBudgetFilter

                    strQ &= ") AS C ON C.budgetunkid = B.budgetunkid "

                    'objDataOperation.AddParameter("@lowerpriority", SqlDbType.Int, eZeeDataType.INT_SIZE, intLowerPriority) 

                End If


                strQ &= "UNION ALL "
            End If

            strQ &= "SELECT  bgbudget_approval_tran.budgetapprovaltranunkid " & _
                               ", bgbudget_approval_tran.budgetunkid " & _
                               ", bgbudget_master.budget_code " & _
                               ", bgbudget_master.budget_name " & _
                               ", bgbudget_master.payyearunkid " & _
                               ", bgbudget_master.payyearname AS payyear " & _
                               ", bgbudget_master.viewbyid " & _
                               ", bgbudget_master.allocationbyid " & _
                               ", bgbudget_master.presentationmodeid " & _
                               ", bgbudget_master.whotoincludeid " & _
                               ", bgbudget_master.salarylevelid " & _
                               ", bgbudget_approval_tran.levelunkid " & _
                               ", bgbudgetapproverlevel_master.levelcode " & _
                               ", bgbudgetapproverlevel_master.levelname " & _
                               ", bgbudget_approval_tran.priority " & _
                               ", bgbudget_approval_tran.approval_date AS approval_date " & _
                               ", CONVERT(CHAR(8), bgbudget_approval_tran.approval_date, 112) AS strapproval_date " & _
                               ", bgbudget_approval_tran.statusunkid " & _
                               ", bgbudget_approval_tran.disapprovalreason " & _
                               ", bgbudget_approval_tran.remarks " & _
                               ", bgbudget_approval_tran.userunkid AS approveruserunkid " & _
                               ", cfuser_master.username AS approverusername " & _
                               ", bgbudget_approval_tran.isvoid " & _
                               ", bgbudget_approval_tran.voiddatetime " & _
                               ", bgbudget_approval_tran.voiduserunkid " & _
                               ", bgbudget_approval_tran.voidreason "

            strQ &= ", CASE viewbyid "
            For Each pair In dicViewBy
                strQ &= " WHEN " & pair.Key & "  THEN '" & pair.Value & "' "
            Next
            strQ &= " END AS ViewBy "

            strQ &= ", CASE allocationbyid "
            For Each pair In dicAllocation
                strQ &= " WHEN " & pair.Key & "  THEN '" & pair.Value & "' "
            Next
            strQ &= " WHEN 0  THEN 'Employee' "
            strQ &= " END AS AllocationBy "

            strQ &= ", CASE presentationmodeid "
            For Each pair In dicPresentation
                strQ &= " WHEN " & pair.Key & "  THEN '" & pair.Value & "' "
            Next
            strQ &= " END AS PresentationMode "

            strQ &= ", CASE whotoincludeid "
            For Each pair In dicWhoToInclude
                strQ &= " WHEN " & pair.Key & "  THEN '" & pair.Value & "' "
            Next
            strQ &= " END AS WhoToInclude "

            strQ &= ", CASE salarylevelid "
            For Each pair In dicSalaryLevel
                strQ &= " WHEN " & pair.Key & "  THEN '" & pair.Value & "' "
            Next
            strQ &= " END AS SalaryLevel "

            strQ &= ", CASE bgbudget_approval_tran.statusunkid "
            For Each pair In mdicApprovalStatus
                strQ &= " WHEN " & pair.Key & "  THEN '" & pair.Value & "' "
            Next
            strQ &= " END AS Status "

            strQ &= "FROM    bgbudget_approval_tran " & _
                                 "LEFT JOIN bgbudget_master ON bgbudget_approval_tran.budgetunkid = bgbudget_master.budgetunkid " & _
                                 "LEFT JOIN bgbudgetapproverlevel_master ON bgbudgetapproverlevel_master.levelunkid = bgbudget_approval_tran.levelunkid " & _
                                 "LEFT JOIN hrmsConfiguration..cfuser_master ON cfuser_master.userunkid = bgbudget_approval_tran.userunkid "


            If intLowerPriority > 0 Then

                strQ &= "JOIN ( SELECT   bgbudget_master.budgetunkid " & _
                               "FROM     bgbudget_master " & _
                                        "LEFT JOIN bgbudget_approval_tran ON bgbudget_approval_tran.budgetunkid = bgbudget_master.budgetunkid " & _
                               "WHERE    ISNULL(dbo.bgbudget_master.isvoid, 0) = 0 " & _
                                        "AND ISNULL(bgbudget_approval_tran.isvoid, 0) = 0 " & _
                                        "AND bgbudget_approval_tran.priority = @lowerpriority " & _
                                        "AND statusunkid <> 1 "

                strQ &= strBudgetFilter

                strQ &= ") AS D ON D.budgetunkid = bgbudget_master.budgetunkid "

                'objDataOperation.AddParameter("@lowerpriority", SqlDbType.Int, eZeeDataType.INT_SIZE, intLowerPriority)
            End If

            strQ &= "WHERE   bgbudget_approval_tran.isvoid = 0 " & _
                                 "AND ISNULL(bgbudget_master.isvoid, 0) = 0 " & _
                                 "AND ISNULL(bgbudgetapproverlevel_master.isvoid, 0) = 0 "

            strQ &= strBudgetFilter

            If intBudgetApprovalTranUnkid > 0 Then
                strQ &= " AND bgbudget_approval_tran.budgetapprovaltranunkid = @budgetapprovaltranunkid "
                objDataOperation.AddParameter("@budgetapprovaltranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intBudgetApprovalTranUnkid)
            End If

            If intLevelUnkID > 0 Then
                strQ &= " AND bgbudget_approval_tran.levelunkid = @levelunkid "
                objDataOperation.AddParameter("@levelunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intLevelUnkID)
            End If

            If intPriority > 0 Then
                strQ &= " AND bgbudget_approval_tran.priority = @priority "
            End If

            If intApproverUserUnkId > 0 Then
                objDataOperation.AddParameter("@userapproverunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intApproverUserUnkId)
            End If

            If intPriority > 0 Then
                objDataOperation.AddParameter("@priority", SqlDbType.Int, eZeeDataType.INT_SIZE, intPriority)
            End If

            If intLowerPriority > 0 Then
                objDataOperation.AddParameter("@lowerpriority", SqlDbType.Int, eZeeDataType.INT_SIZE, intLowerPriority)
            End If

            If strFilter.Trim <> "" Then
                strQ &= " AND " & strFilter
            End If

            strQ &= " ORDER BY budget_code, priority DESC "

            dsList = objDataOperation.ExecQuery(strQ, strTableName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
        Return dsList
    End Function

    ''' <summary>
    ''' Modify By: Sohail
    ''' </summary>    
    Public Function GetListForApproval(ByVal strTableName As String _
                                       , ByVal intUserUnkId As Integer _
                                       , ByVal blnIsApprove As Boolean _
                                       , ByVal intCurrLevelPriority As Integer _
                                       , ByVal intLowerLevelPriority As Integer _
                                       , ByVal intMaxPriority As Integer _
                                       , Optional ByVal intIsApproved As Integer = -1 _
                                       , Optional ByVal strFilterQuery As String = "" _
                                       ) As DataSet

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim strMainQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try

            strQ = "SELECT  ISNULL(bgbudget_approval_tran.budgetapprovaltranunkid, -999) AS budgetapprovaltranunkid " & _
                          ", bgbudget_master.budgetunkid " & _
                          ", bgbudget_master.budget_code " & _
                          ", bgbudget_master.budget_name " & _
                          ", bgbudget_master.viewbyid " & _
                          ", '' AS viewbyname " & _
                          ", bgbudget_master.allocationbyid " & _
                          ", '' AS allocationbyname " & _
                          ", bgbudget_master.presentationmodeid " & _
                          ", '' AS presentationmodename " & _
                          ", bgbudget_master.whotoincludeid " & _
                          ", '' AS whotoincludename " & _
                          ", bgbudget_master.isapproved " & _
                          ", bgbudget_master.voidreason " & _
                          ", bgbudget_approval_tran.levelunkid " & _
                          ", bgbudgetapproverlevel_master.levelcode " & _
                          ", bgbudgetapproverlevel_master.levelname " & _
                          ", bgbudget_approval_tran.priority " & _
                          ", bgbudget_approval_tran.approval_date " & _
                          ", bgbudget_approval_tran.statusunkid " & _
                          ", bgbudget_approval_tran.userunkid " & _
                          ", bgbudget_approval_tran.isvoid " & _
                          ", bgbudget_approval_tran.voiddatetime " & _
                          ", bgbudget_approval_tran.voiduserunkid " & _
                          ", bgbudget_approval_tran.voidreason " & _
                          ", bgbudget_approval_tran.disapprovalreason " & _
                          ", ISNULL(bgbudget_approval_tran.remarks, '') AS remarks " & _
                    "FROM    bgbudget_master " & _
                            "LEFT JOIN bgbudget_approval_tran ON bgbudget_master.budgetunkid = bgbudget_approval_tran.budgetunkid " & _
                                                                          "AND ISNULL(bgbudget_approval_tran.isvoid, 0) = 0 " & _
                            "LEFT JOIN bgbudgetapproverlevel_master ON bgbudget_approval_tran.levelunkid = bgbudgetapproverlevel_master.levelunkid " & _
                    "WHERE   ISNULL(bgbudget_master.isvoid, 0) = 0 " & _
                            "AND ISNULL(bgbudget_approver_mapping.isvoid, 0) = 0 " & _
                            "AND ISNULL(bgbudgetapproverlevel_master.isvoid, 0) = 0 " & _
                            "AND bgbudget_approver_mapping.userapproverunkid = @userapproverunkid "


            If blnIsApprove = False AndAlso intMaxPriority = intCurrLevelPriority Then
                strQ &= " AND (bgbudget_approval_tran.priority = @lowerpriority OR bgbudget_approval_tran.priority = @currpriority) AND bgbudget_approval_tran.statusunkid IN (0, 1) "
                objDataOperation.AddParameter("@currpriority", SqlDbType.Int, eZeeDataType.INT_SIZE, intCurrLevelPriority)
            Else
                strQ &= " AND bgbudget_approval_tran.priority = @lowerpriority AND bgbudget_approval_tran.statusunkid IN (0, 1) "
            End If
            objDataOperation.AddParameter("@lowerpriority", SqlDbType.Int, eZeeDataType.INT_SIZE, intLowerLevelPriority)


            If intIsApproved = 0 OrElse intIsApproved = 1 Then
                strQ &= " AND ISNULL(bgbudget_master.isapproved, 0) = @isapproved "
                objDataOperation.AddParameter("@isapproved", SqlDbType.Bit, eZeeDataType.BIT_SIZE, intIsApproved)
            End If

            If strFilterQuery.Trim <> "" Then
                strQ &= " AND " & strFilterQuery
            End If

            objDataOperation.AddParameter("@userapproverunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUserUnkId)

            dsList = objDataOperation.ExecQuery(strQ, strTableName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetListForApproval; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
        Return dsList
    End Function

    Public Function GetApproverStatus(ByVal strTableName As String _
                                      , Optional ByVal intPriorityUpto As Integer = -1 _
                                      , Optional ByVal strFilter As String = "" _
                                      ) As DataSet

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "SELECT  a.rowno " & _
                          ", a.atbudgetapprovaltranunkid " & _
                          ", a.budgetunkid " & _
                          ", a.levelunkid " & _
                          ", a.levelname " & _
                          ", a.priority " & _
                          ", a.statusunkid " & _
                          ", a.audituserunkid " & _
                          ", a.auditusername " & _
                          ", a.approval_date " & _
                          ", a.remarks " & _
                    "FROM    ( SELECT    RANK() OVER ( PARTITION BY atbgbudget_approval_tran.budgetunkid, " & _
                                                      "audituserunkid ORDER BY atbudgetapprovaltranunkid DESC ) AS rowno " & _
                                      ", atbgbudget_approval_tran.atbudgetapprovaltranunkid " & _
                                      ", atbgbudget_approval_tran.budgetunkid " & _
                                      ", atbgbudget_approval_tran.levelunkid " & _
                                      ", bgbudgetapproverlevel_master.levelname " & _
                                      ", atbgbudget_approval_tran.priority " & _
                                      ", atbgbudget_approval_tran.statusunkid " & _
                                      ", atbgbudget_approval_tran.audituserunkid " & _
                                      ", ISNULL(cfuser_master.username, '') AS auditusername " & _
                                      ", atbgbudget_approval_tran.approval_date " & _
                                      ", atbgbudget_approval_tran.remarks " & _
                              "FROM      atbgbudget_approval_tran " & _
                                        "JOIN bgbudget_master ON atbgbudget_approval_tran.budgetunkid = bgbudget_master.budgetunkid " & _
                                        "LEFT JOIN hrmsConfiguration..cfuser_master ON cfuser_master.userunkid = atbgbudget_approval_tran.audituserunkid " & _
                                        "LEFT JOIN bgbudgetapproverlevel_master ON bgbudgetapproverlevel_master.levelunkid = atbgbudget_approval_tran.levelunkid " & _
                              "WHERE     ISNULL(bgbudget_master.isvoid, 0) = 0 " & _
                                        "AND atbgbudget_approval_tran.levelunkid <> -1 " & _
                                        "AND atbgbudget_approval_tran.priority <> -1 "

            If intPriorityUpto >= 0 Then
                strQ &= " AND atbgbudget_approval_tran.priority <= @priorityupto "
                objDataOperation.AddParameter("@priorityupto", SqlDbType.Int, eZeeDataType.INT_SIZE, intPriorityUpto)
            End If

            If strFilter.Trim <> "" Then
                strQ &= " AND " & strFilter
            End If

            strQ &= "       ) AS a " & _
                    "WHERE   a.rowno = 1 " & _
                    "ORDER BY a.budgetunkid " & _
                          ", a.atbudgetapprovaltranunkid DESC "


            dsList = objDataOperation.ExecQuery(strQ, strTableName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetApproverStatus; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
        Return dsList
    End Function

    Public Function GetTotalApprovedCount(ByVal intPeriodId As Integer _
                                          , Optional ByVal intLevelUnkID As Integer = 0 _
                                          , Optional ByVal intPriority As Integer = -1 _
                                          , Optional ByVal strFilter As String = "" _
                                          ) As Integer
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "SELECT  COUNT(a.rowno) AS TotalCount " & _
                     "FROM    ( SELECT    RANK() OVER ( PARTITION BY atbgbudget_approval_tran.budgetunkid, audituserunkid ORDER BY atbudgetapprovaltranunkid DESC ) AS rowno " & _
                               "FROM      atbgbudget_approval_tran " & _
                                         "JOIN bgbudget_master ON atbgbudget_approval_tran.budgetunkid = bgbudget_master.budgetunkid " & _
                               "WHERE     ISNULL(bgbudget_master.isvoid, 0) = 0 " & _
                                         "AND bgbudget_master.periodunkid = @periodunkid " & _
                                         "AND atbgbudget_approval_tran.levelunkid <> -1 " & _
                                         "AND atbgbudget_approval_tran.priority <> -1 " & _
                                         "AND atbgbudget_approval_tran.statusunkid = 1 "


            If intLevelUnkID > 0 Then
                strQ &= " AND atbgbudget_approval_tran.levelunkid = @levelunkid "
                objDataOperation.AddParameter("@levelunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intLevelUnkID)
            End If

            If intPriority > -1 Then
                strQ &= " AND atbgbudget_approval_tran.priority = @priority "
                objDataOperation.AddParameter("@priority", SqlDbType.Int, eZeeDataType.INT_SIZE, intPriority)
            End If

            If strFilter.Trim <> "" Then
                strQ &= " AND " & strFilter
            End If

            strQ &= "       ) AS a " & _
                  "WHERE   a.rowno = 1 "

            objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intPeriodId)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables("List").Rows(0).Item("TotalCount")

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetTotalApprovedCount; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    Public Function GetBudgetApprovalTranUnkId(ByVal intBudgetUnkId As Integer _
                                               , ByVal intPriority As Integer _
                                               ) As Integer
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try

            strQ = "SELECT  bgbudget_approval_tran.budgetapprovaltranunkid " & _
                    "FROM    bgbudget_approval_tran " & _
                    "WHERE   bgbudget_approval_tran.isvoid = 0 " & _
                            "AND bgbudget_approval_tran.budgetunkid = @budgetunkid " & _
                            "AND bgbudget_approval_tran.priority = @priority "

            objDataOperation.AddParameter("@budgetunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intBudgetUnkId)
            objDataOperation.AddParameter("@priority", SqlDbType.Int, eZeeDataType.INT_SIZE, intPriority)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables("List").Rows(0).Item("budgetapprovaltranunkid")

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetBudgetApprovalTranUnkId; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    Public Function GetListForBudget(ByVal strTableName As String _
                                      , ByVal intBudgetUnkId As Integer _
                                      , Optional ByVal strFilter As String = "" _
                                      ) As DataSet

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try

            strQ = "SELECT  bgbudget_approval_tran.budgetapprovaltranunkid  " & _
                          ", bgbudget_approval_tran.budgetunkid " & _
                          ", bgbudget_approval_tran.levelunkid " & _
                          ", bgbudget_approval_tran.priority " & _
                          ", CONVERT(CHAR(8), bgbudget_approval_tran.approval_date, 112) AS approval_date " & _
                          ", bgbudget_approval_tran.statusunkid " & _
                          ", bgbudget_approval_tran.disapprovalreason " & _
                          ", bgbudget_approval_tran.remarks " & _
                          ", bgbudget_approval_tran.userunkid " & _
                          ", bgbudget_approval_tran.isvoid " & _
                          ", bgbudget_approval_tran.voiduserunkid " & _
                          ", bgbudget_approval_tran.voiddatetime " & _
                          ", bgbudget_approval_tran.voidreason " & _
                    "FROM    bgbudget_approval_tran " & _
                    "WHERE   bgbudget_approval_tran.isvoid = 0 "

            If intBudgetUnkId > 0 Then
                strQ &= "AND bgbudget_approval_tran.budgetunkid = @budgetunkid "
                objDataOperation.AddParameter("@budgetunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intBudgetUnkId)
            End If

            If strFilter.Trim <> "" Then
                strQ &= " " & strFilter
            End If

            dsList = objDataOperation.ExecQuery(strQ, strTableName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetApproverStatus; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
        Return dsList
    End Function

    Public Function GetLastApprovedDetail(ByVal strTableName As String _
                                          , ByVal intPeriodId As Integer _
                                          , Optional ByVal intLevelUnkID As Integer = 0 _
                                          , Optional ByVal intPriority As Integer = -1 _
                                          , Optional ByVal intApprovedUserUnkId As Integer = 0 _
                                          , Optional ByVal strFilter As String = "" _
                                          ) As DataSet
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "SELECT a.rowno " & _
                          ", MAX(a.approval_date) AS approval_date " & _
                          ", a.audituserunkid " & _
                          ", ISNULL(cfuser_master.username, '') AS approvername " & _
                          ", ISNULL(cfuser_master.firstname, '') + ' ' + ISNULL(cfuser_master.lastname, '') AS approverfullname " & _
                          ", a.levelname " & _
                     "FROM    ( SELECT    RANK() OVER ( PARTITION BY atbgbudget_approval_tran.budgetunkid, audituserunkid ORDER BY atbudgetapprovaltranunkid DESC ) AS rowno " & _
                                       ", atbgbudget_approval_tran.approval_date " & _
                                       ", atbgbudget_approval_tran.audituserunkid " & _
                                       ", bgbudgetapproverlevel_master.levelname " & _
                               "FROM      atbgbudget_approval_tran " & _
                                         "JOIN bgbudget_master ON atbgbudget_approval_tran.budgetunkid = bgbudget_master.budgetunkid " & _
                                         "LEFT JOIN bgbudgetapproverlevel_master ON bgbudgetapproverlevel_master.levelunkid = atbgbudget_approval_tran.levelunkid " & _
                               "WHERE     ISNULL(bgbudget_master.isvoid, 0) = 0 " & _
                                         "AND bgbudget_master.periodunkid = @periodunkid " & _
                                         "AND atbgbudget_approval_tran.levelunkid <> -1 " & _
                                         "AND atbgbudget_approval_tran.priority <> -1 " & _
                                         "AND atbgbudget_approval_tran.statusunkid = 1 "


            If intLevelUnkID > 0 Then
                strQ &= " AND atbgbudget_approval_tran.levelunkid = @levelunkid "
                objDataOperation.AddParameter("@levelunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intLevelUnkID)
            End If

            If intPriority > -1 Then
                strQ &= " AND atbgbudget_approval_tran.priority = @priority "
                objDataOperation.AddParameter("@priority", SqlDbType.Int, eZeeDataType.INT_SIZE, intPriority)
            End If

            If intApprovedUserUnkId > 0 Then
                strQ &= " AND atbgbudget_approval_tran.audituserunkid = @audituserunkid "
                objDataOperation.AddParameter("@audituserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intApprovedUserUnkId)
            End If

            If strFilter.Trim <> "" Then
                strQ &= " AND " & strFilter
            End If

            strQ &= ") AS a " & _
                   "LEFT JOIN hrmsConfiguration..cfuser_master ON cfuser_master.userunkid = a.audituserunkid " & _
           "WHERE   a.rowno = 1 " & _
           "GROUP BY a.rowno " & _
                  ", a.audituserunkid " & _
                  ", ISNULL(cfuser_master.username, '') " & _
                  ", ISNULL(cfuser_master.firstname, '') + ' ' + ISNULL(cfuser_master.lastname, '') " & _
                  ", a.levelname "

            objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intPeriodId)

            dsList = objDataOperation.ExecQuery(strQ, strTableName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetTotalApprovedCount; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
        Return dsList
    End Function

    ''' <summary>
    ''' Modify By: Sohail
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> INSERT INTO Database Table (bgbudget_approval_tran) </purpose>
    Public Function Insert(ByVal dtCurrentDateAndTime As Date) As Boolean

        'If isExist(mstrName) Then
        '    mstrMessage = "<Message>"
        '    Return False
        'End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            objDataOperation.AddParameter("@budgetunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintBudgetunkid.ToString)
            objDataOperation.AddParameter("@levelunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLevelunkid.ToString)
            objDataOperation.AddParameter("@priority", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPriority.ToString)
            objDataOperation.AddParameter("@approval_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtApproval_Date.ToString)
            objDataOperation.AddParameter("@statusunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintStatusunkid.ToString)
            objDataOperation.AddParameter("@disapprovalreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrDisapprovalreason.ToString)
            objDataOperation.AddParameter("@remarks", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrRemarks.ToString)
            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            If mdtVoiddatetime = Nothing Then
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            Else
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime)
            End If
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)

            strQ = "INSERT INTO bgbudget_approval_tran ( " & _
              "  budgetunkid " & _
              ", levelunkid " & _
              ", priority " & _
              ", approval_date " & _
              ", statusunkid " & _
              ", disapprovalreason " & _
              ", remarks " & _
              ", userunkid " & _
              ", isvoid " & _
              ", voiddatetime " & _
              ", voiduserunkid " & _
              ", voidreason" & _
            ") VALUES (" & _
              "  @budgetunkid " & _
              ", @levelunkid " & _
              ", @priority " & _
              ", @approval_date " & _
              ", @statusunkid " & _
              ", @disapprovalreason " & _
              ", @remarks " & _
              ", @userunkid " & _
              ", @isvoid " & _
              ", @voiddatetime " & _
              ", @voiduserunkid " & _
              ", @voidreason" & _
            "); SELECT @@identity"

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            mintBudgetapprovaltranunkid = dsList.Tables(0).Rows(0).Item(0)

            If InsertAuditTrailForBudgetApprovalTran(objDataOperation, 1, dtCurrentDateAndTime) = False Then
                Return False
            End If

            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Insert; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sohail
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Update Database Table (bgbudget_approval_tran) </purpose>
    Public Function Update(ByVal dtCurrentDateAndTime As Date) As Boolean

        'If isExist(mstrName, mintBudgetapprovaltranunkid) Then
        '    mstrMessage = "<Message>"
        '    Return False
        'End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            objDataOperation.AddParameter("@budgetapprovaltranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintBudgetapprovaltranunkid.ToString)
            objDataOperation.AddParameter("@budgetunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintBudgetunkid.ToString)
            objDataOperation.AddParameter("@levelunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLevelunkid.ToString)
            objDataOperation.AddParameter("@priority", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPriority.ToString)
            objDataOperation.AddParameter("@approval_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtApproval_Date.ToString)
            objDataOperation.AddParameter("@statusunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintStatusunkid.ToString)
            objDataOperation.AddParameter("@disapprovalreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrDisapprovalreason.ToString)
            objDataOperation.AddParameter("@remarks", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrRemarks.ToString)
            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            If mdtVoiddatetime = Nothing Then
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            Else
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime)
            End If
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)

            strQ = "UPDATE bgbudget_approval_tran SET " & _
              "  budgetunkid = @budgetunkid" & _
              ", levelunkid = @levelunkid" & _
              ", priority = @priority" & _
              ", approval_date = @approval_date" & _
              ", statusunkid = @statusunkid" & _
              ", disapprovalreason = @disapprovalreason" & _
              ", remarks = @remarks" & _
              ", userunkid = @userunkid" & _
              ", isvoid = @isvoid" & _
              ", voiddatetime = @voiddatetime" & _
              ", voiduserunkid = @voiduserunkid" & _
              ", voidreason = @voidreason " & _
            "WHERE budgetapprovaltranunkid = @budgetapprovaltranunkid "

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If InsertAuditTrailForBudgetApprovalTran(objDataOperation, 2, dtCurrentDateAndTime) = False Then
                Return False
            End If

            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sohail
    ''' </summary>
    ''' <remarks></remarks>
    Public Function UpdateBudgetApproval(ByVal intBudgetapprovaltranunkid As Integer _
                                         , ByVal intUserUnkId As Integer _
                                         , ByVal dtCurrentDateAndTime As Date _
                                         , Optional ByVal intIsApproved As Integer = 0 _
                                         ) As Boolean

        Dim objDataOperation As clsDataOperation

        Dim StrQ As String = ""
        Dim dsList As New DataSet

        objDataOperation = New clsDataOperation
        objDataOperation.BindTransaction()

        Try


            If intBudgetapprovaltranunkid > 0 Then
                If Update(dtCurrentDateAndTime) = False Then
                    objDataOperation.ReleaseTransaction(False)
                    Return False
                End If
            Else
                If Insert(dtCurrentDateAndTime) = False Then
                    objDataOperation.ReleaseTransaction(False)
                    Return False
                End If
            End If


            
            If intIsApproved = enApprovalStatus.APPROVED OrElse intIsApproved = enApprovalStatus.REJECTED OrElse intIsApproved = enApprovalStatus.CANCELLED Then
                Dim objBudget As New clsBudget_MasterNew
                objBudget._Budgetunkid = mintBudgetunkid
                If intIsApproved = enApprovalStatus.CANCELLED Then
                    objBudget._Budget_statusunkid = enApprovalStatus.PENDING
                Else
                    objBudget._Budget_statusunkid = intIsApproved
                End If
                objBudget._Userunkid = intUserUnkId
                objBudget._FormName = mstrFormName '"frmApproveDisapproveBudget"
                objBudget._HostName = mstrHostName
                objBudget._ClientIP = mstrClientIP
                objBudget._AuditUserId = mintAuditUserId
objBudget._CompanyUnkid = mintCompanyUnkid
                objBudget._AuditDate = mdtAuditDate
                objBudget._FromWeb = mblnIsWeb



                If objBudget.Update(dtCurrentDateAndTime, objDataOperation) = False Then
                    objDataOperation.ReleaseTransaction(False)
                    Return False
                End If

                If intIsApproved = enApprovalStatus.CANCELLED Then
                    Dim objBudgetLevel As New clsbudgetapproverlevel_master
                    dsList = GetListForBudget("List", mintBudgetunkid)
                    For Each dsRow As DataRow In dsList.Tables("List").Rows
                        Me._Budgetapprovaltranunkid = CInt(dsRow.Item("budgetapprovaltranunkid"))
                        Me._Voiduserunkid = intUserUnkId
                        Me._Voiddatetime = dtCurrentDateAndTime
                        Me._Voidreason = "Budget approval cancelled"
                        If Void(CInt(dsRow.Item("budgetapprovaltranunkid")), dtCurrentDateAndTime, objDataOperation) = False Then
                            objDataOperation.ReleaseTransaction(False)
                            Return False
                        End If
                    Next
                    
                End If
            End If

            objDataOperation.ReleaseTransaction(True)

            Return True

        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: UpdateBudgetApproval; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sohail
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Delete Database Table (bgbudget_approval_tran) </purpose>
    Public Function Void(ByVal intUnkid As Integer, ByVal dtCurrentDateAndTime As Date, Optional ByVal xDataOpr As clsDataOperation = Nothing) As Boolean
        'If isUsed(intUnkid) Then
        '    mstrMessage = "<Message>"
        '    Return False
        'End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        If xDataOpr Is Nothing Then
            objDataOperation = New clsDataOperation
            objDataOperation.BindTransaction()
        Else
            objDataOperation = xDataOpr
            objDataOperation.ClearParameters()
        End If

        Try
            strQ = "UPDATE bgbudget_approval_tran SET " & _
                      " isvoid = 1 " & _
                      ", voiddatetime = @voiddatetime" & _
                      ", voiduserunkid = @voiduserunkid" & _
                      ", voidreason = @voidreason " & _
                    "WHERE budgetapprovaltranunkid = @budgetapprovaltranunkid "

            objDataOperation.AddParameter("@budgetapprovaltranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid.ToString)
            objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, dtCurrentDateAndTime)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If InsertAuditTrailForBudgetApprovalTran(objDataOperation, 3, dtCurrentDateAndTime) = False Then
               objDataOperation.ReleaseTransaction(False)
                Return False
            End If

            If xDataOpr Is Nothing Then
                objDataOperation.ReleaseTransaction(True)
            End If

            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Void; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xDataOpr Is Nothing Then
                objDataOperation = Nothing
            End If
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sohail
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isUsed(ByVal intUnkid As Integer) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "<Query>"

            objDataOperation.AddParameter("@budgetapprovaltranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isUsed; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sohail
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isExist(ByVal strCode As String, ByVal strName As String, Optional ByVal intUnkid As Integer = -1) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "SELECT " & _
              "  budgetapprovaltranunkid " & _
              ", budgetunkid " & _
              ", levelunkid " & _
              ", priority " & _
              ", approval_date " & _
              ", statusunkid " & _
              ", disapprovalreason " & _
              ", remarks " & _
              ", userunkid " & _
              ", isvoid " & _
              ", voiddatetime " & _
              ", voiduserunkid " & _
              ", voidreason " & _
             "FROM bgbudget_approval_tran " & _
             "WHERE name = @name " & _
             "AND code = @code "

            If intUnkid > 0 Then
                strQ &= " AND budgetapprovaltranunkid <> @budgetapprovaltranunkid"
            End If

            objDataOperation.AddParameter("@code", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strCode)
            objDataOperation.AddParameter("@name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strName)
            objDataOperation.AddParameter("@budgetapprovaltranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isExist; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    Public Sub SendMailToApprover(ByVal intStatusID As Integer _
                                , ByVal intApproverUserUnkid As Integer _
                                , ByVal intBudgetApprovalTranUnkId As Integer _
                                , ByVal intBudgetUnkId As Integer _
                                , ByVal strBudgetCode As String _
                                , ByVal strRemarks As String _
                                , ByVal intCompanyUnkId As Integer _
                                , ByVal blnIsArutiDemo As Boolean _
                                , ByVal strArutiSelfServiceURL As String _
                                , ByVal intYearUnkid As Integer _
                                , ByVal strUserAccessModeSetting As String _
                                , Optional ByVal iLoginTypeId As Integer = -1 _
                                , Optional ByVal iLoginEmployeeId As Integer = -1 _
                                )

        Dim objApproverLevel As New clsbudgetapproverlevel_master
        Dim objApproverMap As New clsbudget_approver_mapping
        Dim objUser As New clsUserAddEdit
        Dim objMail As New clsSendMail
        Dim strMessage As New StringBuilder
        Dim dsList As DataSet
        Dim intCurrLevelPriority As Integer = -1
        Dim intNextLevelPriority As Integer = -1
        Dim intMaxLevelPriority As Integer = -1
        Dim strApproverName As String
        Dim strApproverUnkIDs As String = ""
        Dim strArrayIDs() As String
        Dim blnFinalApproved As Boolean = False
        Dim oUser As New clsUserAddEdit

        Try

            If intStatusID = enApprovalStatus.PENDING Then
                intNextLevelPriority = objApproverLevel.GetMinPriority()
            Else
                dsList = objApproverMap.GetList("Approver", intApproverUserUnkid)
                If dsList.Tables("Approver").Rows.Count > 0 Then
                    intCurrLevelPriority = CInt(dsList.Tables("Approver").Rows(0).Item("priority"))
                    intNextLevelPriority = objApproverLevel.GetNextLevelPriority(intCurrLevelPriority)
                    If intNextLevelPriority = -1 Then
                        intMaxLevelPriority = objApproverLevel.GetMaxPriority()
                    End If
                End If
            End If
            If Not (intStatusID = enApprovalStatus.CANCELLED OrElse intStatusID = enApprovalStatus.REJECTED) Then
                If intCurrLevelPriority > -1 AndAlso intCurrLevelPriority = intMaxLevelPriority AndAlso intNextLevelPriority = -1 Then
                    Dim ds As DataSet = oUser.Get_UserBy_PrivilegeId(1042) '1042 = AllowToVoidApprovedBudget
                    strApproverUnkIDs = String.Join(",", (From p In ds.Tables(0) Select (p.Item("UId").ToString)).ToArray)
                    blnFinalApproved = True
                Else
                    strApproverUnkIDs = objApproverMap.GetApproverUnkIDs(intNextLevelPriority)
                End If
            ElseIf intStatusID = enApprovalStatus.PUBLISHED Then
                'strApproverUnkIDs = objApproverMap.GetPublishStaffRequisitionUserUnkIDs()
            ElseIf intStatusID = enApprovalStatus.CANCELLED Then
                'strApproverUnkIDs = objApproverMap.GetCancelStaffRequisitionUserUnkIDs(intApproverUserUnkid)
            Else
                strApproverUnkIDs = objApproverMap.GetLowerApproverUnkIDs(intCurrLevelPriority)
            End If

            If strApproverUnkIDs.Trim = "" Then Exit Try

            strArrayIDs = strApproverUnkIDs.Split(",")


            objUser._Userunkid = intApproverUserUnkid
            strApproverName = objUser._Firstname & " " & objUser._Lastname
            Dim objMaster As New clsMasterData
            gobjEmailList = New List(Of clsEmailCollection)

            For Each strApproverID As String In strArrayIDs

                Dim mblnFlag As Boolean = True
                If blnFinalApproved = True Then
                    'Dim dtUserAccess As DataTable = oUser.GetUserAccessFromUser(CInt(strApproverID), intCompanyUnkId, intYearUnkid, strUserAccessModeSetting)
                    'If dtUserAccess IsNot Nothing AndAlso dtUserAccess.Rows.Count > 0 Then

                    '    For Each AID As String In strUserAccessModeSetting.Trim.Split(CChar(","))
                    '        Dim drRow() As DataRow = Nothing

                    '        Select Case CInt(AID)

                    '            Case enAllocation.DEPARTMENT
                    '                drRow = dtUserAccess.Select("allocationunkid = " & intAllocationUnkId & " AND referenceunkid = " & enAllocation.DEPARTMENT)
                    '                If drRow.Length > 0 Then
                    '                    If intStaffReqById = CInt(AID) Then mblnFlag = True
                    '                Else
                    '                    mblnFlag = False
                    '                    Exit For
                    '                End If

                    '            Case enAllocation.JOBS
                    '                drRow = dtUserAccess.Select("allocationunkid = " & intAllocationUnkId & " AND referenceunkid = " & enAllocation.JOBS)
                    '                If drRow.Length > 0 Then
                    '                    If intStaffReqById = CInt(AID) Then mblnFlag = True
                    '                Else
                    '                    mblnFlag = False
                    '                    Exit For
                    '                End If

                    '            Case enAllocation.CLASS_GROUP
                    '                drRow = dtUserAccess.Select("allocationunkid = " & intAllocationUnkId & " AND referenceunkid = " & enAllocation.CLASS_GROUP)
                    '                If drRow.Length > 0 Then
                    '                    If intStaffReqById = CInt(AID) Then mblnFlag = True
                    '                Else
                    '                    mblnFlag = False
                    '                    Exit For
                    '                End If

                    '            Case enAllocation.CLASSES
                    '                drRow = dtUserAccess.Select("allocationunkid = " & intAllocationUnkId & " AND referenceunkid = " & enAllocation.CLASSES)
                    '                If drRow.Length > 0 Then
                    '                    If intStaffReqById = CInt(AID) Then mblnFlag = True
                    '                Else
                    '                    mblnFlag = False
                    '                    Exit For
                    '                End If

                    '            Case Else
                    '                mblnFlag = True
                    '        End Select
                    '    Next
                    'End If
                End If

                If mblnFlag = False AndAlso blnFinalApproved = True Then Continue For

                strMessage = New StringBuilder

                objUser = New clsUserAddEdit

                objUser._Userunkid = CInt(strApproverID)
                If objUser._Email.Trim = "" Then Continue For

                strMessage.Append("<HTML> <BODY style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#333399;'>")

                'Gajanan [27-Mar-2019] -- Start
                'Enhancement - Change Email Language

                'strMessage.Append(Language.getMessage(mstrModuleName, 1, "Dear") & " <B>" & objUser._Firstname & " " & objUser._Lastname & "</B>, <BR><BR>")
                strMessage.Append(Language.getMessage(mstrModuleName, 1, "Dear") & " <B>" & getTitleCase(objUser._Firstname & " " & objUser._Lastname) & "</B>, <BR><BR>")
                'Gajanan [27-Mar-2019] -- End

                If blnFinalApproved = True Then

                    objMail._Subject = strBudgetCode & " " & Language.getMessage(mstrModuleName, 2, "budget approved")

                    'Gajanan [27-Mar-2019] -- Start
                    'Enhancement - Change Email Language

                    'strMessage.Append("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" & Language.getMessage(mstrModuleName, 3, "This is to notify you that a Budget") & "  <B>" & strBudgetCode & "</B>  ")
                    strMessage.Append(" " & Language.getMessage(mstrModuleName, 3, "This is to notify you that a Budget") & "  <B>" & strBudgetCode & "</B>  ")
                    strMessage.Append(Language.getMessage(mstrModuleName, 4, "has been final approved."))
                    'strMessage.Append("<BR><BR>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" & Language.getMessage(mstrModuleName, 5, "This Budget has been final approved by") & " <B>" & strApproverName & "</B>.")
                    strMessage.Append("<BR><BR> " & Language.getMessage(mstrModuleName, 5, "This Budget has been final approved by") & " <B>" & strApproverName & "</B>.")
                    'Gajanan [27-Mar-2019] -- End

                ElseIf intStatusID = enApprovalStatus.PENDING Then

                    objMail._Subject = Language.getMessage(mstrModuleName, 6, "Reminder for approving Budget") & " " & strBudgetCode


                    'Gajanan [27-Mar-2019] -- Start
                    'Enhancement - Change Email Language

                    'strMessage.Append("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" & Language.getMessage(mstrModuleName, 7, "This is to notify you that a request to a budget ") & "  <B>" & strBudgetCode & "</B>  ")
                    strMessage.Append(" " & Language.getMessage(mstrModuleName, 7, "This is to notify you that a request to a budget ") & "  <B>" & strBudgetCode & "</B>  ")
                    strMessage.Append(Language.getMessage(mstrModuleName, 8, "is awaiting for your approval."))
                    'strMessage.Append("<BR><BR>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" & Language.getMessage(mstrModuleName, 9, "This budget has been created by") & " <B>" & strApproverName & "</B>.")
                    strMessage.Append("<BR><BR> " & Language.getMessage(mstrModuleName, 9, "This budget has been created by") & " <B>" & strApproverName & "</B>.")
                    'Gajanan [27-Mar-2019] -- End

                    If strRemarks.Trim <> "" Then

                        'Gajanan [27-Mar-2019] -- Start
                        'Enhancement - Change Email Language

                        'strMessage.Append("<BR><BR>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<I>" & Language.getMessage(mstrModuleName, 10, "*Remarks :") & " " & strRemarks & "</I>")
                        strMessage.Append("<BR><BR> <I>" & Language.getMessage(mstrModuleName, 10, "*Remarks :") & " " & strRemarks & "</I>")
                        'Gajanan [27-Mar-2019] -- End

                    End If

                    'If blnIsArutiDemo = True OrElse ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Manager_Self_Service) = True Then
                    '    strMessage.Append("<BR><BR>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" & Language.getMessage(mstrModuleName, 100, "Click the link below to approve/reject the requisition."))
                    '    Dim strLink As String
                    '    strLink = strArutiSelfServiceURL & "/Recruitment/Staff_Requisition/wPg_ApproveDisapproveStaffRequisition.aspx?" & HttpUtility.UrlEncode(clsCrypto.Encrypt(intCompanyUnkId.ToString & "|" & strApproverID & "|" & intBudgetApprovalTranUnkId.ToString & "|" & intBudgetUnkId.ToString & "|" & enApprovalStatus.PENDING & ""))

                    '    strMessage.Append("<BR>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href='" & strLink & "'>" & strLink & "</a>")
                    'End If

                ElseIf intStatusID = enApprovalStatus.APPROVED Then

                    objMail._Subject = Language.getMessage(mstrModuleName, 6, "Reminder for approving Budget") & " " & strBudgetCode

                    'Gajanan [27-Mar-2019] -- Start
                    'Enhancement - Change Email Language

                    'strMessage.Append("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" & Language.getMessage(mstrModuleName, 7, "This is to notify you that a request to a budget ") & "  <B>" & strBudgetCode & "</B>  ")
                    strMessage.Append(" " & Language.getMessage(mstrModuleName, 7, "This is to notify you that a request to a budget ") & "  <B>" & strBudgetCode & "</B>  ")
                    strMessage.Append(Language.getMessage(mstrModuleName, 8, "is awaiting for your approval."))
                    'strMessage.Append("<BR><BR>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" & Language.getMessage(mstrModuleName, 11, "This Budget has been approved by") & " <B>" & strApproverName & "</B>.")
                    strMessage.Append("<BR><BR> " & Language.getMessage(mstrModuleName, 11, "This Budget has been approved by") & " <B>" & strApproverName & "</B>.")
                    'Gajanan [27-Mar-2019] -- End

                    'If blnIsArutiDemo = True OrElse ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Manager_Self_Service) = True Then
                    '    strMessage.Append("<BR><BR>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" & Language.getMessage(mstrModuleName, 100, "Click the link below to approve/reject the requisition."))
                    '    Dim strLink As String
                    '    strLink = strArutiSelfServiceURL & "/Recruitment/Staff_Requisition/wPg_ApproveDisapproveStaffRequisition.aspx?" & HttpUtility.UrlEncode(clsCrypto.Encrypt(intCompanyUnkId.ToString & "|" & strApproverID & "|" & intBudgetApprovalTranUnkId.ToString & "|" & intBudgetUnkId.ToString & "|" & enApprovalStatus.PENDING & ""))

                    '    strMessage.Append("<BR>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href='" & strLink & "'>" & strLink & "</a>")
                    'End If

                ElseIf intStatusID = enApprovalStatus.REJECTED Then

                    objMail._Subject = strBudgetCode & " " & Language.getMessage(mstrModuleName, 12, "budget rejected.")


                    'Gajanan [27-Mar-2019] -- Start
                    'Enhancement - Change Email Language

                    'strMessage.Append("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" & Language.getMessage(mstrModuleName, 3, "This is to notify you that a Budget") & "  <B>" & strBudgetCode & "</B>  ")
                    strMessage.Append(" " & Language.getMessage(mstrModuleName, 3, "This is to notify you that a Budget") & "  <B>" & strBudgetCode & "</B>  ")
                    strMessage.Append(Language.getMessage(mstrModuleName, 14, "has been rejected."))
                    'strMessage.Append("<BR><BR>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" & Language.getMessage(mstrModuleName, 15, "This Budget has been rejected by") & " <B>" & strApproverName & "</B>.")
                    strMessage.Append("<BR><BR> " & Language.getMessage(mstrModuleName, 15, "This Budget has been rejected by") & " <B>" & strApproverName & "</B>.")
                    'Gajanan [27-Mar-2019] -- End

                ElseIf intStatusID = enApprovalStatus.CANCELLED Then

                    objMail._Subject = strBudgetCode & " " & Language.getMessage(mstrModuleName, 16, "budget cancelled for")

                    'Gajanan [27-Mar-2019] -- Start
                    'Enhancement - Change Email Language
                    'strMessage.Append("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" & Language.getMessage(mstrModuleName, 3, "This is to notify you that a Budget") & "  <B>" & strBudgetCode & "</B>  ")
                    strMessage.Append(" " & Language.getMessage(mstrModuleName, 3, "This is to notify you that a Budget") & "  <B>" & strBudgetCode & "</B>  ")
                    strMessage.Append(Language.getMessage(mstrModuleName, 17, "has been cancelled."))
                    'strMessage.Append("<BR><BR>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" & Language.getMessage(mstrModuleName, 18, "This Budget has been cancelled by") & " <B>" & strApproverName & "</B>.")
                    strMessage.Append("<BR><BR> " & Language.getMessage(mstrModuleName, 18, "This Budget has been cancelled by") & " <B>" & strApproverName & "</B>.")
                    'Gajanan [27-Mar-2019] -- End

                ElseIf intStatusID = enApprovalStatus.PUBLISHED Then

                    objMail._Subject = strBudgetCode & " " & Language.getMessage(mstrModuleName, 19, "budget published for")

                    'Gajanan [27-Mar-2019] -- Start
                    'Enhancement - Change Email Language

                    'strMessage.Append("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" & Language.getMessage(mstrModuleName, 3, "This is to notify you that a Budget") & "  <B>" & strBudgetCode & "</B>  ")
                    strMessage.Append(" " & Language.getMessage(mstrModuleName, 3, "This is to notify you that a Budget") & "  <B>" & strBudgetCode & "</B>  ")
                    strMessage.Append(Language.getMessage(mstrModuleName, 20, "has been published."))
                    'strMessage.Append("<BR><BR>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" & Language.getMessage(mstrModuleName, 21, "This Budget has been published by") & " <B>" & strApproverName & "</B>.")
                    strMessage.Append("<BR><BR> " & Language.getMessage(mstrModuleName, 21, "This Budget has been published by") & " <B>" & strApproverName & "</B>.")
                    'Gajanan [27-Mar-2019] -- End

                End If


                'Gajanan [27-Mar-2019] -- Start
                'Enhancement - Change Email Language

                'strMessage.Append("<BR><BR><BR><BR></BR>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<B>&quot;POWERED BY ARUTI HR & PAYROLL MANAGEMENT SOFTWARE.&quot;</B>")
                strMessage.Append("<p><center style='padding-top:25px;font-size:10.0pt;font-family:&quot;Verdana&quot;,&quot;Sans-Serif&quot;;color:#333399;margin-left:0px;margin-right:0px;margin-top:0px;margin-bottom:10px'><b>&quot;POWERED BY ARUTI HR &amp; PAYROLL MANAGEMENT SOFTWARE.&quot;</b></center></p>")
                'Gajanan [27-Mar-2019] -- End

                strMessage.Append("</BODY></HTML>")

                objMail._Message = strMessage.ToString
                objMail._ToEmail = objUser._Email

                If iLoginTypeId <= 0 Then iLoginTypeId = enLogin_Mode.DESKTOP
                objMail._FormName = mstrFormName
                objMail._HostName = mstrHostName
                objMail._AuditUserId = mintAuditUserId
objMail._CompanyUnkid = mintCompanyUnkid
                objMail._AuditDate = mdtAuditDate
                objMail._FromWeb = mblnIsWeb
                objMail._LoginEmployeeunkid = iLoginEmployeeId
                objMail._OperationModeId = iLoginTypeId
                objMail._UserUnkid = intApproverUserUnkid
                Dim objUsr As New clsUserAddEdit
                objUsr._Userunkid = intApproverUserUnkid
                objMail._SenderAddress = IIf(objUsr._Email = "", objUsr._Firstname & " " & objUsr._Lastname, objUsr._Email)
                objMail._ModuleRefId = clsSendMail.enAT_VIEW_TYPE.PAYROLL_MGT

                gobjEmailList.Add(New clsEmailCollection(objMail._ToEmail, objMail._Subject, objMail._Message, mstrFormName, iLoginEmployeeId, mstrClientIP, mstrHostName, intApproverUserUnkid, iLoginTypeId, clsSendMail.enAT_VIEW_TYPE.RECRUITMENT_MGT, IIf(objUsr._Email = "", objUsr._Firstname & " " & objUsr._Lastname, objUsr._Email)))
                objUsr = Nothing
            Next

            'trd = New Thread(AddressOf Send_Notification)
            'trd.IsBackground = True
            'trd.Start()

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SendMailToApprover", mstrModuleName)
        Finally
            objApproverLevel = Nothing
            objApproverMap = Nothing
            objUser = Nothing
            objMail = Nothing
        End Try
    End Sub

    'Private Sub Send_Notification()
    '    Try
    '        If gobjEmailList.Count > 0 Then
    '            Dim objSendMail As New clsSendMail
    '            For Each obj In gobjEmailList
    '                objSendMail._ToEmail = obj._EmailTo
    '                objSendMail._Subject = obj._Subject
    '                objSendMail._Message = obj._Message
    '                objSendMail._Form_Name = obj._Form_Name
    '                objSendMail._LogEmployeeUnkid = obj._LogEmployeeUnkid
    '                objSendMail._OperationModeId = obj._OperationModeId
    '                objSendMail._UserUnkid = obj._UserUnkid
    '                objSendMail._SenderAddress = obj._SenderAddress
    '                objSendMail._ModuleRefId = obj._ModuleRefId
    '                Try
    '                    objSendMail.SendMail()
    '                Catch ex As Exception

    '                End Try
    '            Next
    '            gobjEmailList.Clear()
    '        End If
    '    Catch ex As Exception
    '        Throw New Exception(mstrModuleName & ":Send_Notification:- " & ex.Message)
    '    Finally
    '        If gobjEmailList.Count > 0 Then
    '            gobjEmailList.Clear()
    '        End If
    '    End Try
    'End Sub

    Public Function InsertAuditTrailForBudgetApprovalTran(ByVal objDataOperation As clsDataOperation, ByVal AuditType As Integer, ByVal dtCurrentDateAndTime As Date) As Boolean

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            'If mstrWebIP.Trim = "" Then mstrWebIP = getIP()
            'If mstrWebhostName.Trim = "" Then mstrWebhostName = getHostName()

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@budgetapprovaltranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintBudgetapprovaltranunkid.ToString)
            objDataOperation.AddParameter("@budgetunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintBudgetunkid.ToString)
            objDataOperation.AddParameter("@levelunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLevelunkid.ToString)
            objDataOperation.AddParameter("@priority", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPriority.ToString)
            objDataOperation.AddParameter("@approval_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtApproval_Date.ToString)
            objDataOperation.AddParameter("@statusunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintStatusunkid.ToString)
            objDataOperation.AddParameter("@disapprovalreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrDisapprovalreason.ToString)
            objDataOperation.AddParameter("@audittype", SqlDbType.Int, eZeeDataType.INT_SIZE, AuditType.ToString)
            objDataOperation.AddParameter("@audituserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAuditUserId)
            objDataOperation.AddParameter("@auditdatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtAuditDate)
            objDataOperation.AddParameter("@ip", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrClientIP)
            objDataOperation.AddParameter("@machine_name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrHostName)
            objDataOperation.AddParameter("@form_name", SqlDbType.NVarChar, 500, mstrFormName)
            objDataOperation.AddParameter("@isweb", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsWeb)
            objDataOperation.AddParameter("@remarks", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, mstrRemarks.ToString)

            strQ = "INSERT INTO atbgbudget_approval_tran ( " & _
                "  budgetapprovaltranunkid " & _
                ", budgetunkid " & _
                ", levelunkid " & _
                ", priority " & _
                ", approval_date " & _
                ", statusunkid " & _
                ", disapprovalreason " & _
                ", remarks " & _
                ", audittype " & _
                ", audituserunkid " & _
                ", auditdatetime " & _
                ", ip " & _
                ", machine_name" & _
                ", form_name " & _
                " " & _
                " " & _
                " " & _
                " " & _
                " " & _
                ", isweb " & _
            ") VALUES (" & _
                "  @budgetapprovaltranunkid " & _
                ", @budgetunkid " & _
                ", @levelunkid " & _
                ", @priority " & _
                ", @approval_date " & _
                ", @statusunkid " & _
                ", @disapprovalreason " & _
                ", @remarks " & _
                ", @audittype " & _
                ", @audituserunkid " & _
                ", @auditdatetime " & _
                ", @ip " & _
                ", @machine_name" & _
                ", @form_name " & _
                " " & _
                " " & _
                " " & _
                " " & _
                " " & _
                ", @isweb " & _
            "); SELECT @@identity"

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            mintBudgetapprovaltranunkid = dsList.Tables(0).Rows(0).Item(0)

            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: InsertAuditTrailForBudgetApprovalTran; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            'objDataOperation = Nothing
        End Try
    End Function
	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Public Shared Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "Dear")
			Language.setMessage(mstrModuleName, 2, "budget approved")
			Language.setMessage(mstrModuleName, 3, "This is to notify you that a Budget")
			Language.setMessage(mstrModuleName, 4, "has been final approved.")
			Language.setMessage(mstrModuleName, 5, "This Budget has been final approved by")
			Language.setMessage(mstrModuleName, 6, "Reminder for approving Budget")
			Language.setMessage(mstrModuleName, 7, "This is to notify you that a request to a budget")
			Language.setMessage(mstrModuleName, 8, "is awaiting for your approval.")
			Language.setMessage(mstrModuleName, 9, "This budget has been created by")
			Language.setMessage(mstrModuleName, 10, "*Remarks :")
			Language.setMessage(mstrModuleName, 11, "This Budget has been approved by")
			Language.setMessage(mstrModuleName, 12, "budget rejected.")
			Language.setMessage(mstrModuleName, 13, "WEB")
			Language.setMessage(mstrModuleName, 14, "has been rejected.")
			Language.setMessage(mstrModuleName, 15, "This Budget has been rejected by")
			Language.setMessage(mstrModuleName, 16, "budget cancelled for")
			Language.setMessage(mstrModuleName, 17, "has been cancelled.")
			Language.setMessage(mstrModuleName, 18, "This Budget has been cancelled by")
			Language.setMessage(mstrModuleName, 19, "budget published for")
			Language.setMessage(mstrModuleName, 20, "has been published.")
			Language.setMessage(mstrModuleName, 21, "This Budget has been published by")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class
