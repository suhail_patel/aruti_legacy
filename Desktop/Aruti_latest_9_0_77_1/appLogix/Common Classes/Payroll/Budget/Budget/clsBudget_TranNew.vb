﻿'************************************************************************************************************************************
'Class Name : clsBudget_TranNew.vb
'Purpose    :
'Date       :07/06/2016
'Written By :Sohail
'Modified   :
'************************************************************************************************************************************

Imports eZeeCommonLib
Imports eZee.Common.eZeeForm

''' <summary>
''' Purpose: 
''' Developer: Sohail
''' </summary>
Public Class clsBudget_TranNew
    Private Shared ReadOnly mstrModuleName As String = "clsBudget_TranNew"
    Dim objDataOperation As clsDataOperation
    Dim mstrMessage As String = ""

#Region " Private variables "
    Private mintBudgettranunkid As Integer
    Private mintBudgetunkid As Integer
    Private mintAllocationtranunkid As Integer
    Private mintTranheadunkid As Integer
    Private mintJobUnkId As Integer = 0
    Private mdecAmount As Decimal
    Private mdecBudgetamount As Decimal
    Private mdecSalaryamount As Decimal
    Private mdecBudgetsalaryamount As Decimal
    Private mdecOtherpayrollamount As Decimal
    Private mdecBudgetotherpayrollamount As Decimal
    Private mintUserunkid As Integer
    Private mblnIsvoid As Boolean
    Private mintVoiduserunkid As Integer
    Private mdtVoiddatetime As Date
    Private mstrVoidreason As String = String.Empty

    'Private mstrWebFormName As String = String.Empty
    'Private mstrWebIP As String = ""
    'Private mstrWebHost As String = ""
#End Region

#Region " Properties "

'S.SANDEEP [28-May-2018] -- START 
'ISSUE/ENHANCEMENT : {Audit Trails} 
Private mstrFormName As String = String.Empty
Public WriteOnly Property _FormName() As String 
Set(ByVal value As String) 
mstrFormName = value 
End Set 
End Property 
 
Private mstrClientIP As String = ""
Public WriteOnly Property _ClientIP() As String 
Set(ByVal value As String) 
mstrClientIP = value 
End Set 
End Property 
 
Private mstrHostName As String = ""
Public WriteOnly Property _HostName() As String 
Set(ByVal value As String) 
mstrHostName = value 
End Set 
End Property 
 
Private mblnIsWeb As Boolean = False
Public WriteOnly Property _FromWeb() As Boolean 
Set(ByVal value As Boolean) 
mblnIsWeb = value 
End Set 
End Property 
 
    Private mintAuditUserId As Integer = 0
    Public WriteOnly Property _AuditUserId() As Integer
        Set(ByVal value As Integer)
            mintAuditUserId = value
        End Set
    End Property

    Private mintCompanyUnkid As Integer = 0
    Public WriteOnly Property _CompanyUnkid() As Integer
        Set(ByVal value As Integer)
            mintCompanyUnkid = value
        End Set
    End Property
 
Private mdtAuditDate As DateTime = Now
Public WriteOnly Property _AuditDate() As DateTime 
Set(ByVal value As DateTime) 
mdtAuditDate = value 
End Set 
End Property 
 
Private xDataOpr As clsDataOperation = Nothing
Public Property _xDataOpr() As clsDataOperation 
Get 
Return xDataOpr 
End Get 
Set(ByVal value As clsDataOperation) 
xDataOpr = value 
End Set 
End Property 
 
Private mintLoginEmployeeunkid As Integer = 0
Public Property _LoginEmployeeunkid() As Integer 
Get 
Return mintLoginEmployeeunkid 
End Get 
Set(ByVal value As Integer) 
mintLoginEmployeeunkid = value 
End Set 
End Property 
 
Private mintVoidlogingemployeeunkid As Integer=0
Public Property _Voidloginemployeeunkid() As Integer 
Get 
Return mintVoidlogingemployeeunkid 
End Get 
Set(ByVal value As Integer) 
mintVoidlogingemployeeunkid = value 
End Set 
End Property 
'S.SANDEEP [28-May-2018] -- END 

    Public ReadOnly Property _Message() As String
        Get
            Return mstrMessage
        End Get
    End Property

    ''' <summary>
    ''' Purpose: Get or Set budgettranunkid
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Budgettranunkid() As Integer
        Get
            Return mintBudgettranunkid
        End Get
        Set(ByVal value As Integer)
            mintBudgettranunkid = Value
            Call getData()
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set budgetunkid
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Budgetunkid() As Integer
        Get
            Return mintBudgetunkid
        End Get
        Set(ByVal value As Integer)
            mintBudgetunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set allocationtranunkid
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Allocationtranunkid() As Integer
        Get
            Return mintAllocationtranunkid
        End Get
        Set(ByVal value As Integer)
            mintAllocationtranunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set tranheadunkid
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Tranheadunkid() As Integer
        Get
            Return mintTranheadunkid
        End Get
        Set(ByVal value As Integer)
            mintTranheadunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set tranheadunkid
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _JobUnkId() As Integer
        Get
            Return mintJobUnkId
        End Get
        Set(ByVal value As Integer)
            mintJobUnkId = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set amount
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Amount() As Decimal
        Get
            Return mdecAmount
        End Get
        Set(ByVal value As Decimal)
            mdecAmount = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set budgetamount
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Budgetamount() As Decimal
        Get
            Return mdecBudgetamount
        End Get
        Set(ByVal value As Decimal)
            mdecBudgetamount = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set salaryamount
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Salaryamount() As Decimal
        Get
            Return mdecSalaryamount
        End Get
        Set(ByVal value As Decimal)
            mdecSalaryamount = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set budgetsalaryamount
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Budgetsalaryamount() As Decimal
        Get
            Return mdecBudgetsalaryamount
        End Get
        Set(ByVal value As Decimal)
            mdecBudgetsalaryamount = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set otherpayrollamount
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Otherpayrollamount() As Decimal
        Get
            Return mdecOtherpayrollamount
        End Get
        Set(ByVal value As Decimal)
            mdecOtherpayrollamount = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set budgetotherpayrollamount
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Budgetotherpayrollamount() As Decimal
        Get
            Return mdecBudgetotherpayrollamount
        End Get
        Set(ByVal value As Decimal)
            mdecBudgetotherpayrollamount = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set userunkid
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Userunkid() As Integer
        Get
            Return mintUserunkid
        End Get
        Set(ByVal value As Integer)
            mintUserunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isvoid
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Isvoid() As Boolean
        Get
            Return mblnIsvoid
        End Get
        Set(ByVal value As Boolean)
            mblnIsvoid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiduserunkid
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Voiduserunkid() As Integer
        Get
            Return mintVoiduserunkid
        End Get
        Set(ByVal value As Integer)
            mintVoiduserunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiddatetime
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Voiddatetime() As Date
        Get
            Return mdtVoiddatetime
        End Get
        Set(ByVal value As Date)
            mdtVoiddatetime = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voidreason
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Voidreason() As String
        Get
            Return mstrVoidreason
        End Get
        Set(ByVal value As String)
            mstrVoidreason = Value
        End Set
    End Property

    'Public WriteOnly Property _WebFormName() As String
    '    Set(ByVal value As String)
    '        mstrWebFormName = value
    '    End Set
    'End Property

    'Public WriteOnly Property _WebIP() As String
    '    Set(ByVal value As String)
    '        mstrWebIP = value
    '    End Set
    'End Property

    'Public WriteOnly Property _WebHost() As String
    '    Set(ByVal value As String)
    '        mstrWebHost = value
    '    End Set
    'End Property
#End Region

    ''' <summary>
    ''' Modify By: Sohail
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Sub GetData(Optional ByVal xDataOpr As clsDataOperation = Nothing)
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        If xDataOpr Is Nothing Then
        objDataOperation = New clsDataOperation
        Else
            objDataOperation = xDataOpr
        End If

        Try
            strQ = "SELECT " & _
                    "  budgettranunkid " & _
                    ", budgetunkid " & _
                    ", allocationtranunkid " & _
                    ", tranheadunkid " & _
                    ", jobunkid " & _
                    ", amount " & _
                    ", budgetamount " & _
                    ", salaryamount " & _
                    ", budgetsalaryamount " & _
                    ", otherpayrollamount " & _
                    ", budgetotherpayrollamount " & _
                    ", userunkid " & _
                    ", isvoid " & _
                    ", voiduserunkid " & _
                    ", voiddatetime " & _
                    ", voidreason " & _
             "FROM bgbudget_tran " & _
             "WHERE budgettranunkid = @budgettranunkid "

            objDataOperation.AddParameter("@budgettranunkid", SqlDbType.int, eZeeDataType.INT_SIZE, mintBudgetTranUnkId.ToString)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For Each dtRow As DataRow In dsList.Tables(0).Rows
                mintbudgettranunkid = CInt(dtRow.Item("budgettranunkid"))
                mintbudgetunkid = CInt(dtRow.Item("budgetunkid"))
                mintallocationtranunkid = CInt(dtRow.Item("allocationtranunkid"))
                mintTranheadunkid = CInt(dtRow.Item("tranheadunkid"))
                mintJobUnkId = CInt(dtRow.Item("jobunkid"))
                mdecAmount = dtRow.Item("amount")
                mdecBudgetamount = dtRow.Item("budgetamount")
                mdecSalaryamount = dtRow.Item("salaryamount")
                mdecBudgetsalaryamount = dtRow.Item("budgetsalaryamount")
                mdecOtherpayrollamount = dtRow.Item("otherpayrollamount")
                mdecBudgetotherpayrollamount = dtRow.Item("budgetotherpayrollamount")
                mintuserunkid = CInt(dtRow.Item("userunkid"))
                mblnisvoid = CBool(dtRow.Item("isvoid"))
                mintvoiduserunkid = CInt(dtRow.Item("voiduserunkid"))
                If IsDBNull(dtRow.Item("voiddatetime")) = True Then
                    mdtVoiddatetime = Nothing
                Else
                    mdtVoiddatetime = dtRow.Item("voiddatetime")
                End If
                mstrvoidreason = dtRow.Item("voidreason").ToString
                Exit For
            Next
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetData; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xDataOpr Is Nothing Then objDataOperation = Nothing
        End Try
    End Sub

    'Nilay (07-Jul-2016) -- Start
    Public Function getEmployeeIds(ByVal intBudgetUnkid As Integer) As String
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim strEmployeeIds As String = String.Empty

        objDataOperation = New clsDataOperation
        Try
            strQ = "SELECT " & _
                   "    STUFF( " & _
                   "            ( " & _
                   "                SELECT DISTINCT " & _
                   "                    ', ' + CAST(allocationtranunkid AS NVARCHAR(MAX)) " & _
                   "                FROM bgbudget_tran " & _
                   "                WHERE isvoid = 0 " & _
                   "                    AND budgetunkid = " & intBudgetUnkid & " " & _
                   "                FOR XML PATH ('')), 1, 1, '' " & _
                   "         ) AS EmployeeIds "

            dsList = objDataOperation.ExecQuery(strQ, "Ids")

            If dsList.Tables("Ids").Rows.Count > 0 Then
                strEmployeeIds = dsList.Tables("Ids").Rows(0).Item("EmployeeIds")
            End If

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: getEmployeeIds; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
        End Try
        Return strEmployeeIds
    End Function
    'Nilay (07-Jul-2016) -- End

    ''' <summary>
    ''' Modify By: Sohail
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function GetList(ByVal strTableName As String, Optional ByVal intBudgetUnkId As Integer = 0) As DataSet
        'Sohail (06 May 2017) - [intBudgetUnkId]
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "SELECT " & _
              "  budgettranunkid " & _
              ", budgetunkid " & _
              ", allocationtranunkid " & _
              ", tranheadunkid " & _
              ", jobunkid " & _
              ", amount " & _
              ", budgetamount " & _
              ", salaryamount " & _
              ", budgetsalaryamount " & _
              ", otherpayrollamount " & _
              ", budgetotherpayrollamount " & _
              ", userunkid " & _
              ", isvoid " & _
              ", voiduserunkid " & _
              ", voiddatetime " & _
              ", voidreason " & _
                 "FROM bgbudget_tran " & _
                 "WHERE isvoid = 0 "

            'Sohail (06 May 2017) -- Start
            'MST Enhancement - 66.1 - Allow to map employees with planned employees in payroll budget.
            If intBudgetUnkId > 0 Then
                strQ &= " AND budgetunkid = @budgetunkid "
                objDataOperation.AddParameter("@budgetunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intBudgetUnkId)
            End If
            'Sohail (06 May 2017) -- End

            dsList = objDataOperation.ExecQuery(strQ, strTableName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
        Return dsList
    End Function

    Public Function GetAllocationTranUnkIDs(ByVal intBudgetUnkId As Integer) As String
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim strIDs As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try

            strQ = "SELECT  ISNULL(STUFF(( SELECT   ',' " & _
                                                    "+ CAST(allocationtranunkid AS VARCHAR(MAX)) " & _
                                           "FROM     ( SELECT DISTINCT " & _
                                                                "bgbudget_tran.allocationtranunkid " & _
                                                      "FROM      bgbudget_tran " & _
                                                      "WHERE     bgbudget_tran.isvoid = 0 " & _
                                                                "AND bgbudget_tran.budgetunkid = @budgetunkid " & _
                                                    ") AS A " & _
                                         "FOR " & _
                                           "XML PATH('') " & _
                                         "), 1, 1, ''), '') AS IDs "

            objDataOperation.AddParameter("@budgetunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intBudgetUnkId)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            strIDs = dsList.Tables(0).Rows(0).Item(0).ToString

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetAllocationTranUnkIDs; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
        Return strIDs
    End Function

    ''' <summary>
    ''' Modify By: Sohail
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> INSERT INTO Database Table (bgbudget_tran) </purpose>
    Public Function Insert(Optional ByVal xDataOpr As clsDataOperation = Nothing) As Boolean
        'If isExist(mstrName) Then
        '    mstrMessage = "<Message>"
        '    Return False
        'End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        If xDataOpr Is Nothing Then
            objDataOperation = New clsDataOperation
            objDataOperation.BindTransaction()
        Else
            objDataOperation = xDataOpr
        End If
        objDataOperation.ClearParameters()

        Try
            objDataOperation.AddParameter("@budgetunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintBudgetunkid.ToString)
            objDataOperation.AddParameter("@allocationtranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAllocationtranunkid.ToString)
            objDataOperation.AddParameter("@tranheadunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintTranheadunkid.ToString)
            objDataOperation.AddParameter("@jobunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintJobUnkId.ToString)
            objDataOperation.AddParameter("@amount", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecAmount.ToString)
            objDataOperation.AddParameter("@budgetamount", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecBudgetamount.ToString)
            objDataOperation.AddParameter("@salaryamount", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecSalaryamount.ToString)
            objDataOperation.AddParameter("@budgetsalaryamount", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecBudgetsalaryamount.ToString)
            objDataOperation.AddParameter("@otherpayrollamount", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecOtherpayrollamount.ToString)
            objDataOperation.AddParameter("@budgetotherpayrollamount", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecBudgetotherpayrollamount.ToString)
            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
            If mdtVoiddatetime = Nothing Then
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            Else
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime.ToString)
            End If
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)

            strQ = "INSERT INTO bgbudget_tran ( " & _
                        "  budgetunkid " & _
                        ", allocationtranunkid " & _
                        ", tranheadunkid " & _
                        ", jobunkid " & _
                        ", amount " & _
                        ", budgetamount " & _
                        ", salaryamount " & _
                        ", budgetsalaryamount " & _
                        ", otherpayrollamount " & _
                        ", budgetotherpayrollamount " & _
                        ", userunkid " & _
                        ", isvoid " & _
                        ", voiduserunkid " & _
                        ", voiddatetime " & _
                        ", voidreason" & _
                ") VALUES (" & _
                        "  @budgetunkid " & _
                        ", @allocationtranunkid " & _
                        ", @tranheadunkid " & _
                        ", @jobunkid " & _
                        ", @amount " & _
                        ", @budgetamount " & _
                        ", @salaryamount " & _
                        ", @budgetsalaryamount " & _
                        ", @otherpayrollamount " & _
                        ", @budgetotherpayrollamount " & _
                        ", @userunkid " & _
                        ", @isvoid " & _
                        ", @voiduserunkid " & _
                        ", @voiddatetime " & _
                        ", @voidreason" & _
                "); SELECT @@identity"

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            mintBudgettranunkid = dsList.Tables(0).Rows(0).Item(0)

            If InsertAuditBudget_Tran(objDataOperation, enAuditType.ADD) = False Then
                If xDataOpr Is Nothing Then
                    objDataOperation.ReleaseTransaction(False)
                End If
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If xDataOpr Is Nothing Then
                objDataOperation.ReleaseTransaction(True)
            End If

            Return True
        Catch ex As Exception
            If xDataOpr Is Nothing Then
                objDataOperation.ReleaseTransaction(False)
            End If
            Throw New Exception(ex.Message & "; Procedure Name: Insert; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xDataOpr Is Nothing Then
                objDataOperation = Nothing
            End If
        End Try
    End Function

    Public Function InsertAll(ByVal objDataOp As clsDataOperation, ByVal dtTable As DataTable, ByVal dtCurrentDateTime As Date) As Boolean

        Dim objBudgetFundSource As New clsBudgetfundsource_Tran
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception


        objDataOp.ClearParameters()

        Try

            For Each dtRow As DataRow In dtTable.Rows

                mintBudgettranunkid = CInt(dtRow.Item("budgettranunkid"))
                mintAllocationtranunkid = CInt(dtRow.Item("allocationtranunkid"))
                mintJobUnkId = CInt(dtRow.Item("jobunkid"))
                If dtTable.Columns.Contains("tranheadunkid") = True Then
                    mintTranheadunkid = CInt(dtRow.Item("tranheadunkid"))
                    mdecAmount = CDec(dtRow.Item("pramount"))
                    mdecSalaryamount = 0
                    mdecOtherpayrollamount = 0
                Else
                    mintTranheadunkid = -1
                    mdecAmount = 0
                    mdecSalaryamount = CDec(dtRow.Item("_|1"))
                    mdecOtherpayrollamount = CDec(dtRow.Item("_|2"))
                End If
                mdecBudgetamount = CDec(dtRow.Item("budgetamount"))
                mdecBudgetsalaryamount = CDec(dtRow.Item("budgetsalaryamount"))
                mdecBudgetotherpayrollamount = CDec(dtRow.Item("budgetotherpayrollamount"))

                'If mintBudgettranunkid <= 0 Then
                If Insert(objDataOp) = False Then
                    objDataOp.ReleaseTransaction(False)
                    Return False
                End If
                'Else
                '    If Update(objDataOp) = False Then
                '        objDataOp.ReleaseTransaction(False)
                '        Return False
                '    End If
                'End If

                objBudgetFundSource._Budgettranunkid = mintBudgettranunkid
                objBudgetFundSource._Userunkid = mintUserunkid
                objBudgetFundSource._Isvoid = mblnIsvoid
                objBudgetFundSource._Voiduserunkid = mintVoiduserunkid
                objBudgetFundSource._Voiddatetime = mdtVoiddatetime
                objBudgetFundSource._Voidreason = mstrVoidreason
                With objBudgetFundSource
                    ._FormName = mstrFormName
                    ._LoginEmployeeunkid = mintLoginEmployeeunkid
                    ._ClientIP = mstrClientIP
                    ._HostName = mstrHostName
                    ._FromWeb = mblnIsWeb
                    ._AuditUserId = mintAuditUserId
._CompanyUnkid = mintCompanyUnkid
                    ._AuditDate = mdtAuditDate
                End With
                If objBudgetFundSource.InsertAll(dtRow, objDataOp) = False Then
                    objDataOp.ReleaseTransaction(False)
                    Return False
                End If

            Next

            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: InsertAll; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sohail
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Update Database Table (bgbudget_tran) </purpose>
    Public Function Update(Optional ByVal xDataOpr As clsDataOperation = Nothing) As Boolean
        'If isExist(mstrName, mintBudgettranunkid) Then
        '    mstrMessage = "<Message>"
        '    Return False
        'End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        If xDataOpr Is Nothing Then
            objDataOperation = New clsDataOperation
            objDataOperation.BindTransaction()
        Else
            objDataOperation = xDataOpr
        End If
        objDataOperation.ClearParameters()

        Try
            objDataOperation.AddParameter("@budgettranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintBudgettranunkid.ToString)
            objDataOperation.AddParameter("@budgetunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintBudgetunkid.ToString)
            objDataOperation.AddParameter("@allocationtranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAllocationtranunkid.ToString)
            objDataOperation.AddParameter("@tranheadunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintTranheadunkid.ToString)
            objDataOperation.AddParameter("@jobunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintJobUnkId.ToString)
            objDataOperation.AddParameter("@amount", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecAmount.ToString)
            objDataOperation.AddParameter("@budgetamount", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecBudgetamount.ToString)
            objDataOperation.AddParameter("@salaryamount", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecSalaryamount.ToString)
            objDataOperation.AddParameter("@budgetsalaryamount", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecBudgetsalaryamount.ToString)
            objDataOperation.AddParameter("@otherpayrollamount", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecOtherpayrollamount.ToString)
            objDataOperation.AddParameter("@budgetotherpayrollamount", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecBudgetotherpayrollamount.ToString)
            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
            If mdtVoiddatetime = Nothing Then
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            Else
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime.ToString)
            End If
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)

            strQ = "UPDATE bgbudget_tran SET " & _
              "  budgetunkid = @budgetunkid" & _
              ", allocationtranunkid = @allocationtranunkid" & _
              ", tranheadunkid = @tranheadunkid" & _
              ", jobunkid = @jobunkid" & _
              ", amount = @amount" & _
              ", budgetamount = @budgetamount" & _
              ", salaryamount = @salaryamount" & _
              ", budgetsalaryamount = @budgetsalaryamount" & _
              ", otherpayrollamount = @otherpayrollamount" & _
              ", budgetotherpayrollamount = @budgetotherpayrollamount" & _
              ", userunkid = @userunkid" & _
              ", isvoid = @isvoid" & _
              ", voiduserunkid = @voiduserunkid" & _
              ", voiddatetime = @voiddatetime" & _
              ", voidreason = @voidreason " & _
            "WHERE budgettranunkid = @budgettranunkid "

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If InsertAuditBudget_Tran(objDataOperation, enAuditType.EDIT) = False Then
                If xDataOpr Is Nothing Then
                    objDataOperation.ReleaseTransaction(False)
                End If
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If xDataOpr Is Nothing Then
                objDataOperation.ReleaseTransaction(True)
            End If

            Return True
        Catch ex As Exception
            If xDataOpr Is Nothing Then
                objDataOperation.ReleaseTransaction(False)
            End If
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xDataOpr Is Nothing Then
                objDataOperation = Nothing
            End If
        End Try
    End Function

    'Sohail (06 May 2017) -- Start
    'MST Enhancement - 66.1 - Allow to map employees with planned employees in payroll budget.
    Public Function UpdatePlannedEmployees(ByVal intBudgetUnkId As Integer, ByVal intPeriodUnkId As Integer, ByVal dtTable As DataTable, ByVal dtCurrentDateAndTime As Date, Optional ByVal xDataOpr As clsDataOperation = Nothing) As Boolean
        'If isExist(mstrName, mintBudgettranunkid) Then
        '    mstrMessage = "<Message>"
        '    Return False
        'End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        If xDataOpr Is Nothing Then
            objDataOperation = New clsDataOperation
            objDataOperation.BindTransaction()
        Else
            objDataOperation = xDataOpr
        End If

        Try

            For Each dtRow As DataRow In dtTable.Rows
                objDataOperation.ClearParameters()

                mintBudgetunkid = intBudgetUnkId
                mintJobUnkId = CInt(dtRow.Item("jobunkid"))
                mintAllocationtranunkid = CInt(dtRow.Item("allocationtranunkid"))

                strQ = "SELECT  bgbudget_tran.budgettranunkid " & _
                        "FROM    bgbudget_tran " & _
                        "WHERE   bgbudget_tran.isvoid = 0 " & _
                                "AND bgbudget_tran.budgetunkid = @budgetunkid " & _
                                "AND bgbudget_tran.jobunkid = @jobunkid " & _
                                "AND bgbudget_tran.allocationtranunkid = @allocationtranunkid "

                objDataOperation.AddParameter("@budgetunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintBudgetunkid.ToString)
                objDataOperation.AddParameter("@jobunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintJobUnkId.ToString)
                objDataOperation.AddParameter("@allocationtranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAllocationtranunkid.ToString)

                dsList = objDataOperation.ExecQuery(strQ, "List")

                If objDataOperation.ErrorMessage <> "" Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If

                For Each dsRow As DataRow In dsList.Tables(0).Rows

                    mintBudgettranunkid = CInt(dsRow.Item("budgettranunkid"))
                    objDataOperation.ClearParameters()
                    Call GetData(objDataOperation)

                    mintAllocationtranunkid = CInt(dtRow.Item("employeeunkid"))

                    objDataOperation.ClearParameters()
                    objDataOperation.AddParameter("@budgettranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintBudgettranunkid.ToString)
                    objDataOperation.AddParameter("@allocationtranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAllocationtranunkid.ToString)

                    strQ = "UPDATE bgbudget_tran SET " & _
                              "  allocationtranunkid = @allocationtranunkid " & _
                              ", jobunkid = jobunkid * -1 " & _
                            "WHERE budgettranunkid = @budgettranunkid "

                    Call objDataOperation.ExecNonQuery(strQ)

                    If objDataOperation.ErrorMessage <> "" Then
                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If

                    If InsertAuditBudget_Tran(objDataOperation, enAuditType.EDIT) = False Then
                        If xDataOpr Is Nothing Then
                            objDataOperation.ReleaseTransaction(False)
                        End If
                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If

                Next

            Next

            Dim objBudgetCodeTran As New clsBudgetcodes_Tran
            With objBudgetCodeTran
                ._FormName = mstrFormName
                ._LoginEmployeeunkid = mintLoginEmployeeunkid
                ._ClientIP = mstrClientIP
                ._HostName = mstrHostName
                ._FromWeb = mblnIsWeb
                ._AuditUserId = mintAuditUserId
._CompanyUnkid = mintCompanyUnkid
                ._AuditDate = mdtAuditDate
            End With
            If objBudgetCodeTran.UpdatePlannedEmployees(intBudgetUnkId, intPeriodUnkId, dtTable, dtCurrentDateAndTime, objDataOperation) = False Then
                objDataOperation.ReleaseTransaction(False)
                Throw New Exception(objBudgetCodeTran._Message)
            End If

            If xDataOpr Is Nothing Then
                objDataOperation.ReleaseTransaction(True)
            End If

            Return True
        Catch ex As Exception
            If xDataOpr Is Nothing Then
                objDataOperation.ReleaseTransaction(False)
            End If
            Throw New Exception(ex.Message & "; Procedure Name: UpdatePlannedEmployees; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xDataOpr Is Nothing Then
                objDataOperation = Nothing
            End If
        End Try
    End Function
    'Sohail (06 May 2017) -- End

    ''' <summary>
    ''' Modify By: Sohail
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Delete Database Table (bgbudget_tran) </purpose>
    Public Function Void(ByVal intUnkid As Integer, Optional ByVal xDataOpr As clsDataOperation = Nothing) As Boolean
        'If isUsed(intUnkid) Then
        '    mstrMessage = "<Message>"
        '    Return False
        'End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        If xDataOpr Is Nothing Then
            objDataOperation = New clsDataOperation
            objDataOperation.BindTransaction()
        Else
            objDataOperation = xDataOpr
        End If
        objDataOperation.ClearParameters()

        Try

            objDataOperation.AddParameter("@budgettranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid.ToString)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
            If mdtVoiddatetime = Nothing Then
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            Else
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime.ToString)
            End If
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)

            strQ = "UPDATE bgbudget_tran SET " & _
               " isvoid = @isvoid" & _
               ", voiduserunkid = @voiduserunkid" & _
               ", voiddatetime = @voiddatetime" & _
               ", voidreason = @voidreason " & _
             "WHERE budgettranunkid = @budgettranunkid "

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Me._Budgettranunkid = intUnkid

            If InsertAuditBudget_Tran(objDataOperation, enAuditType.DELETE) = False Then
                If xDataOpr Is Nothing Then
                    objDataOperation.ReleaseTransaction(False)
                End If
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If xDataOpr Is Nothing Then
                objDataOperation.ReleaseTransaction(True)
            End If

            Return True
        Catch ex As Exception
            If xDataOpr Is Nothing Then
                objDataOperation.ReleaseTransaction(False)
            End If
            Throw New Exception(ex.Message & "; Procedure Name: Void; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xDataOpr Is Nothing Then
                objDataOperation = Nothing
            End If
        End Try
    End Function

    Public Function VoidAllByBudgetUnkId(ByVal intBudgetUnkid As Integer _
                                         , ByVal intVoiduserunkid As Integer _
                                         , ByVal dtVoiddatetime As Date _
                                         , ByVal strVoidreason As String _
                                         , Optional ByVal xDataOpr As clsDataOperation = Nothing _
                                         ) As Boolean
        'If isUsed(intUnkid) Then
        '    mstrMessage = "<Message>"
        '    Return False
        'End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim intRowsAffected As Integer = 0

        If xDataOpr Is Nothing Then
            objDataOperation = New clsDataOperation
            objDataOperation.BindTransaction()
        Else
            objDataOperation = xDataOpr
        End If
        objDataOperation.ClearParameters()

        Try
            'If mstrWebIP.Trim = "" Then mstrWebIP = getIP()
            'If mstrWebHost.Trim = "" Then mstrWebHost = getHostName()

            objDataOperation.AddParameter("@form_name", SqlDbType.NVarChar, 500, mstrFormName)
            objDataOperation.AddParameter("@isweb", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsWeb)

            strQ = "INSERT INTO atbgbudget_tran ( " & _
                                "  budgettranunkid " & _
                                ", budgetunkid " & _
                                ", allocationtranunkid " & _
                                ", tranheadunkid " & _
                                ", jobunkid " & _
                                ", amount " & _
                                ", budgetamount " & _
                                ", salaryamount " & _
                                ", budgetsalaryamount " & _
                                ", otherpayrollamount " & _
                                ", budgetotherpayrollamount " & _
                                ", audittype " & _
                                ", audituserunkid " & _
                                ", auditdatetime " & _
                                ", ip " & _
                                ", machine_name " & _
                                ", form_name " & _
                                " " & _
                                " " & _
                                " " & _
                                " " & _
                                " " & _
                                ", isweb " & _
                         ") " & _
                         "SELECT " & _
                                "  budgettranunkid " & _
                                ", budgetunkid " & _
                                ", allocationtranunkid " & _
                                ", tranheadunkid " & _
                                ", jobunkid " & _
                                ", amount " & _
                                ", budgetamount " & _
                                ", salaryamount " & _
                                ", budgetsalaryamount " & _
                                ", otherpayrollamount " & _
                                ", budgetotherpayrollamount " & _
                                ", 3 " & _
                                ", @voiduserunkid " & _
                                ", @voiddatetime " & _
                                ", '" & mstrClientIP & "' " & _
                                ", '" & mstrHostName & "' " & _
                                ", @form_name " & _
                                " " & _
                                " " & _
                                " " & _
                                " " & _
                                " " & _
                                ", @isweb " & _
                         "FROM bgbudget_tran " & _
                         "WHERE budgetunkid = @budgetunkid " & _
                         "AND isvoid = 0 "

            objDataOperation.AddParameter("@budgetunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intBudgetUnkid.ToString)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, True)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intVoiduserunkid.ToString)
            If dtVoiddatetime = Nothing Then
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            Else
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, dtVoiddatetime.ToString)
            End If
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strVoidreason.ToString)

            intRowsAffected = objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                objDataOperation.ReleaseTransaction(False)
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            strQ = "UPDATE bgbudget_tran SET " & _
               " isvoid = @isvoid " & _
               ", voiduserunkid = @voiduserunkid" & _
               ", voiddatetime = @voiddatetime" & _
               ", voidreason = @voidreason " & _
             "WHERE budgetunkid = @budgetunkid " & _
             "AND isvoid = 0 "

            intRowsAffected = objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                objDataOperation.ReleaseTransaction(False)
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If xDataOpr Is Nothing Then
                objDataOperation.ReleaseTransaction(True)
            End If

            Return True
        Catch ex As Exception
            If xDataOpr Is Nothing Then
                objDataOperation.ReleaseTransaction(False)
            End If
            Throw New Exception(ex.Message & "; Procedure Name: VoidAllByBudgetUnkId; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xDataOpr Is Nothing Then
                objDataOperation = Nothing
            End If
        End Try
    End Function

    Public Function VoidAllByBudgetTranUnkId(ByVal strBudgetTranUnkIDs As String _
                                         , ByVal intVoiduserunkid As Integer _
                                         , ByVal dtVoiddatetime As Date _
                                         , ByVal strVoidreason As String _
                                         , Optional ByVal xDataOpr As clsDataOperation = Nothing _
                                         ) As Boolean
        'If isUsed(intUnkid) Then
        '    mstrMessage = "<Message>"
        '    Return False
        'End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim intRowsAffected As Integer = 0

        If xDataOpr Is Nothing Then
            objDataOperation = New clsDataOperation
            objDataOperation.BindTransaction()
        Else
            objDataOperation = xDataOpr
        End If
        objDataOperation.ClearParameters()

        Try
            'If mstrWebIP.Trim = "" Then mstrWebIP = getIP()
            'If mstrWebHost.Trim = "" Then mstrWebHost = getHostName()

            objDataOperation.AddParameter("@form_name", SqlDbType.NVarChar, 500, mstrFormName)
            objDataOperation.AddParameter("@isweb", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsWeb)

            strQ = "SELECT budgettranunkid INTO #cteBTran FROM bgbudget_tran WHERE budgettranunkid IN (" & strBudgetTranUnkIDs & "); "

            strQ &= "INSERT INTO atbgbudget_tran ( " & _
                                "  budgettranunkid " & _
                                ", budgetunkid " & _
                                ", allocationtranunkid " & _
                                ", tranheadunkid " & _
                                ", jobunkid " & _
                                ", amount " & _
                                ", budgetamount " & _
                                ", salaryamount " & _
                                ", budgetsalaryamount " & _
                                ", otherpayrollamount " & _
                                ", budgetotherpayrollamount " & _
                                ", audittype " & _
                                ", audituserunkid " & _
                                ", auditdatetime " & _
                                ", ip " & _
                                ", machine_name " & _
                                ", form_name " & _
                                " " & _
                                " " & _
                                " " & _
                                " " & _
                                " " & _
                                ", isweb " & _
                         ") " & _
                         "SELECT " & _
                                "  bgbudget_tran.budgettranunkid " & _
                                ", budgetunkid " & _
                                ", allocationtranunkid " & _
                                ", tranheadunkid " & _
                                ", jobunkid " & _
                                ", amount " & _
                                ", budgetamount " & _
                                ", salaryamount " & _
                                ", budgetsalaryamount " & _
                                ", otherpayrollamount " & _
                                ", budgetotherpayrollamount " & _
                                ", 3 " & _
                                ", @voiduserunkid " & _
                                ", @voiddatetime " & _
                                ", '" & mstrClientIP & "' " & _
                                ", '" & mstrHostName & "' " & _
                                ", @form_name " & _
                                " " & _
                                " " & _
                                " " & _
                                " " & _
                                " " & _
                                ", @isweb " & _
                         "FROM bgbudget_tran " & _
                         "JOIN #cteBTran ON #cteBTran.budgettranunkid = bgbudget_tran.budgettranunkid " & _
                         "WHERE isvoid = 0 "

            strQ &= "DROP TABLE #cteBTran"

            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, True)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intVoiduserunkid.ToString)
            If dtVoiddatetime = Nothing Then
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            Else
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, dtVoiddatetime.ToString)
            End If
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strVoidreason.ToString)

            intRowsAffected = objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                objDataOperation.ReleaseTransaction(False)
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            strQ = "SELECT budgettranunkid INTO #cteBTran FROM bgbudget_tran WHERE budgettranunkid IN (" & strBudgetTranUnkIDs & "); "

            strQ &= "UPDATE bgbudget_tran SET " & _
               " isvoid = @isvoid " & _
               ", voiduserunkid = @voiduserunkid" & _
               ", voiddatetime = @voiddatetime" & _
               ", voidreason = @voidreason " & _
               "FROM #cteBTran WHERE #cteBTran.budgettranunkid = bgbudget_tran.budgettranunkid " & _
             "AND isvoid = 0 "

            strQ &= "DROP TABLE #cteBTran"

            intRowsAffected = objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                objDataOperation.ReleaseTransaction(False)
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If xDataOpr Is Nothing Then
                objDataOperation.ReleaseTransaction(True)
            End If

            Return True
        Catch ex As Exception
            If xDataOpr Is Nothing Then
                objDataOperation.ReleaseTransaction(False)
            End If
            Throw New Exception(ex.Message & "; Procedure Name: VoidAllByBudgetTranUnkId; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xDataOpr Is Nothing Then
                objDataOperation = Nothing
            End If
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sohail
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isUsed(ByVal intUnkid As Integer) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            StrQ = "<Query>"

            objDataOperation.AddParameter("@budgettranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.tables(0).rows.count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isUsed; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sohail
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isExist(ByVal strCode As String, ByVal strName As String, Optional ByVal intUnkid As Integer = -1) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            StrQ = "SELECT " & _
              "  budgettranunkid " & _
              ", budgetunkid " & _
              ", allocationtranunkid " & _
              ", tranheadunkid " & _
              ", amount " & _
              ", budgetamount " & _
				", salaryamount " & _ 
				", budgetsalaryamount " & _ 
				", otherpayrollamount " & _ 
				", budgetotherpayrollamount " & _ 
              ", userunkid " & _
              ", isvoid " & _
              ", voiduserunkid " & _
              ", voiddatetime " & _
              ", voidreason " & _
             "FROM bgbudget_tran " & _
             "WHERE name = @name " & _
             "AND code = @code "

            If intUnkid > 0 Then
                strQ &= " AND budgettranunkid <> @budgettranunkid"
            End If

            objDataOperation.AddParameter("@code", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strCode)
            objDataOperation.AddParameter("@name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strName)
            objDataOperation.AddParameter("@budgettranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.tables(0).rows.count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isExist; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    'Sohail (06 May 2017) -- Start
    'MST Enhancement - 66.1 - Allow to map employees with planned employees in payroll budget.
    Public Function GetListForPlannedJobMapping(ByVal strTableName As String, ByVal intBudgetUnkId As Integer, ByVal intJobUnkId As Integer) As DataSet
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "SELECT DISTINCT " & _
                            "bgbudget_tran.budgetunkid  " & _
                          ", bgbudget_tran.allocationtranunkid " & _
                          ", bgbudget_tran.jobunkid " & _
                          ", hrjob_master.job_name " & _
                          ", hrjob_master.job_name + ' ' " & _
                            "+ CAST(bgbudget_tran.allocationtranunkid * -1 AS NVARCHAR(MAX)) AS GName  " & _
                          ", hrjob_master.job_code + ' ' " & _
                            "+ CAST(bgbudget_tran.allocationtranunkid * -1 AS NVARCHAR(MAX)) AS GCode " & _
                    "FROM    bgbudget_tran " & _
                            "LEFT JOIN hrjob_master ON hrjob_master.jobunkid = bgbudget_tran.jobunkid " & _
                    "WHERE   bgbudget_tran.isvoid = 0 " & _
                            "AND bgbudget_tran.budgetunkid = @budgetunkid " & _
                            "AND bgbudget_tran.allocationtranunkid < 0 " & _
                            "AND bgbudget_tran.jobunkid > 0 "

            If intJobUnkId > 0 Then
                strQ &= " AND bgbudget_tran.jobunkid = @jobunkid "
                objDataOperation.AddParameter("@jobunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intJobUnkId)
            End If

            strQ &= "ORDER BY hrjob_master.job_name + ' ' " & _
                            "+ CAST(bgbudget_tran.allocationtranunkid * -1 AS NVARCHAR(MAX)) "

            objDataOperation.AddParameter("@budgetunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intBudgetUnkId)

            dsList = objDataOperation.ExecQuery(strQ, strTableName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetListForPlannedJobMapping; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
        Return dsList
    End Function
    'Sohail (06 May 2017) -- End

    Private Function InsertAuditBudget_Tran(ByVal xDataOpr As clsDataOperation, ByVal xAuditType As enAuditType) As Boolean
        Dim strQ As String = ""
        Dim strErrorMessage As String = ""
        Dim exForce As Exception = Nothing
        Try
            strQ = "INSERT INTO atbgbudget_tran ( " & _
                                "  budgettranunkid " & _
                                ", budgetunkid " & _
                                ", allocationtranunkid " & _
                                ", tranheadunkid " & _
                                ", jobunkid " & _
                                ", amount " & _
                                ", budgetamount " & _
                                ", salaryamount " & _
                                ", budgetsalaryamount " & _
                                ", otherpayrollamount " & _
                                ", budgetotherpayrollamount " & _
                                ", audittype " & _
                                ", audituserunkid " & _
                                ", auditdatetime " & _
                                ", ip " & _
                                ", machine_name " & _
                                ", form_name " & _
                                " " & _
                                " " & _
                                " " & _
                                " " & _
                                " " & _
                                ", isweb " & _
                         ") VALUES (" & _
                                "  @budgettranunkid " & _
                                ", @budgetunkid " & _
                                ", @allocationtranunkid " & _
                                ", @tranheadunkid " & _
                                ", @jobunkid " & _
                                ", @amount " & _
                                ", @budgetamount " & _
                                ", @salaryamount " & _
                                ", @budgetsalaryamount " & _
                                ", @otherpayrollamount " & _
                                ", @budgetotherpayrollamount " & _
                                ", @audittype " & _
                                ", @audituserunkid " & _
                                ", @auditdatetime " & _
                                ", @ip " & _
                                ", @machine_name " & _
                                ", @form_name " & _
                                " " & _
                                " " & _
                                " " & _
                                " " & _
                                " " & _
                                ", @isweb " & _
                         ") "


            xDataOpr.ClearParameters()
            xDataOpr.AddParameter("@budgettranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintBudgettranunkid.ToString)
            xDataOpr.AddParameter("@budgetunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintBudgetunkid.ToString)
            xDataOpr.AddParameter("@allocationtranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAllocationtranunkid.ToString)
            xDataOpr.AddParameter("@tranheadunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintTranheadunkid.ToString)
            xDataOpr.AddParameter("@jobunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintJobUnkId.ToString)
            xDataOpr.AddParameter("@amount", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecAmount.ToString)
            xDataOpr.AddParameter("@budgetamount", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecBudgetamount.ToString)
            xDataOpr.AddParameter("@salaryamount", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecSalaryamount.ToString)
            xDataOpr.AddParameter("@budgetsalaryamount", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecBudgetsalaryamount.ToString)
            xDataOpr.AddParameter("@otherpayrollamount", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecOtherpayrollamount.ToString)
            xDataOpr.AddParameter("@budgetotherpayrollamount", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecBudgetotherpayrollamount.ToString)
            xDataOpr.AddParameter("@audittype", SqlDbType.Int, eZeeDataType.INT_SIZE, xAuditType)
            xDataOpr.AddParameter("@audituserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAuditUserId)
            xDataOpr.AddParameter("@auditdatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtAuditDate)
            xDataOpr.AddParameter("@ip", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrClientIP)
            xDataOpr.AddParameter("@machine_name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrHostName)
            xDataOpr.AddParameter("@form_name", SqlDbType.NVarChar, 500, mstrFormName)
            xDataOpr.AddParameter("@isweb", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsWeb)

            xDataOpr.ExecNonQuery(strQ)

            If xDataOpr.ErrorMessage <> "" Then
                exForce = New Exception(xDataOpr.ErrorNumber & ": " & xDataOpr.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: InsertAuditBudget_Tran; Module Name: " & mstrModuleName)
            Return False
        Finally
        End Try
        Return True
    End Function
	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Public Shared Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "WEB")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class