﻿'************************************************************************************************************************************
'Class Name : clsfundactivity_Tran.vb
'Purpose    :
'Date       :7/15/2016
'Written By :Pinkal
'Modified   :
'************************************************************************************************************************************

Imports eZeeCommonLib
Imports eZee.Common.eZeeForm
Imports System

''' <summary>
''' Purpose: 
''' Developer: Pinkal
''' </summary>
Public Class clsfundactivity_Tran
    Private Shared ReadOnly mstrModuleName As String = "clsfundactivity_Tran"
    Dim objDataOperation As clsDataOperation
    Dim mstrMessage As String = ""

#Region " Private variables "
    Private mintFundactivityunkid As Integer
    Private mstrActivity_Code As String = String.Empty
    Private mstrActivity_Name As String = String.Empty
    Private mstrActivity_Name1 As String = String.Empty
    Private mstrActivity_Name2 As String = String.Empty
    Private mintFundsourceunkid As Integer
    Private mintFundProjectCodeunkid As Integer
    Private mdecAmount As Decimal
    Private mdecNotify_amount As Decimal
    Private mintUserunkid As Integer
    Private mblnIsvoid As Boolean
    Private mintVoiduserunkid As Integer
    Private mdtVoiddatetime As Date
    Private mstrVoidreason As String = String.Empty
    Private mstrWebFormName As String = String.Empty
    'Sohail (02 Sep 2016) -- Start
    'Enhancement -  63.1 - New Budget Redesign for Marie Stopes (Description on Fund Activity).
    Private mstrRemarks As String = String.Empty
    'Sohail (02 Sep 2016) -- End
#End Region

#Region " Properties "

'S.SANDEEP [28-May-2018] -- START 
'ISSUE/ENHANCEMENT : {Audit Trails} 
Private mstrFormName As String = String.Empty
Public WriteOnly Property _FormName() As String 
Set(ByVal value As String) 
mstrFormName = value 
End Set 
End Property 
 
Private mstrClientIP As String = ""
Public WriteOnly Property _ClientIP() As String 
Set(ByVal value As String) 
mstrClientIP = value 
End Set 
End Property 
 
Private mstrHostName As String = ""
Public WriteOnly Property _HostName() As String 
Set(ByVal value As String) 
mstrHostName = value 
End Set 
End Property 
 
Private mblnIsWeb As Boolean = False
Public WriteOnly Property _FromWeb() As Boolean 
Set(ByVal value As Boolean) 
mblnIsWeb = value 
End Set 
End Property 
 
    Private mintAuditUserId As Integer = 0
    Public WriteOnly Property _AuditUserId() As Integer
        Set(ByVal value As Integer)
            mintAuditUserId = value
        End Set
    End Property

    Private mintCompanyUnkid As Integer = 0
    Public WriteOnly Property _CompanyUnkid() As Integer
        Set(ByVal value As Integer)
            mintCompanyUnkid = value
        End Set
    End Property
 
Private mdtAuditDate As DateTime = Now
Public WriteOnly Property _AuditDate() As DateTime 
Set(ByVal value As DateTime) 
mdtAuditDate = value 
End Set 
End Property 
 
Private xDataOpr As clsDataOperation = Nothing
Public Property _xDataOpr() As clsDataOperation 
Get 
Return xDataOpr 
End Get 
Set(ByVal value As clsDataOperation) 
xDataOpr = value 
End Set 
End Property 
 
Private mintLoginEmployeeunkid As Integer = 0
Public Property _LoginEmployeeunkid() As Integer 
Get 
Return mintLoginEmployeeunkid 
End Get 
Set(ByVal value As Integer) 
mintLoginEmployeeunkid = value 
End Set 
End Property 
 
Private mintVoidlogingemployeeunkid As Integer=0
Public Property _Voidloginemployeeunkid() As Integer 
Get 
Return mintVoidlogingemployeeunkid 
End Get 
Set(ByVal value As Integer) 
mintVoidlogingemployeeunkid = value 
End Set 
End Property 
'S.SANDEEP [28-May-2018] -- END 


    Public ReadOnly Property _Message() As String
        Get
            Return mstrMessage
        End Get
    End Property

    ''' <summary>
    ''' Purpose: Get or Set fundactivityunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Fundactivityunkid() As Integer
        Get
            Return mintFundactivityunkid
        End Get
        Set(ByVal value As Integer)
            mintFundactivityunkid = value
            Call GetData()
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set activity_code
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Activity_Code() As String
        Get
            Return mstrActivity_Code
        End Get
        Set(ByVal value As String)
            mstrActivity_Code = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set activity_name
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Activity_Name() As String
        Get
            Return mstrActivity_Name
        End Get
        Set(ByVal value As String)
            mstrActivity_Name = value
        End Set
    End Property

    '''' <summary>
    '''' Purpose: Get or Set fundsourceunkid
    '''' Modify By: Pinkal
    '''' </summary>
    'Public Property _Fundsourceunkid() As Integer
    '    Get
    '        Return mintFundsourceunkid
    '    End Get
    '    Set(ByVal value As Integer)
    '        mintFundsourceunkid = value
    '    End Set
    'End Property

    ''' <summary>
    ''' Purpose: Get or Set fundprojectcodeunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _FundProjectCodeunkid() As Integer
        Get
            Return mintFundProjectCodeunkid
        End Get
        Set(ByVal value As Integer)
            mintFundProjectCodeunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set amount
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Amount() As Decimal
        Get
            Return mdecAmount
        End Get
        Set(ByVal value As Decimal)
            mdecAmount = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set notify_amount
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Notify_Amount() As Decimal
        Get
            Return mdecNotify_amount
        End Get
        Set(ByVal value As Decimal)
            mdecNotify_amount = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set userunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Userunkid() As Integer
        Get
            Return mintUserunkid
        End Get
        Set(ByVal value As Integer)
            mintUserunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isvoid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Isvoid() As Boolean
        Get
            Return mblnIsvoid
        End Get
        Set(ByVal value As Boolean)
            mblnIsvoid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiduserunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Voiduserunkid() As Integer
        Get
            Return mintVoiduserunkid
        End Get
        Set(ByVal value As Integer)
            mintVoiduserunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiddatetime
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Voiddatetime() As Date
        Get
            Return mdtVoiddatetime
        End Get
        Set(ByVal value As Date)
            mdtVoiddatetime = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voidreason
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Voidreason() As String
        Get
            Return mstrVoidreason
        End Get
        Set(ByVal value As String)
            mstrVoidreason = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set webformname
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public Property _WebFormName() As String
        Get
            Return mstrWebFormName
        End Get
        Set(ByVal value As String)
            mstrWebFormName = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set Activity_Name1
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Activity_Name1() As String
        Get
            Return mstrActivity_Name1
        End Get
        Set(ByVal value As String)
            mstrActivity_Name1 = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set Activity_Name2
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Activity_Name2() As String
        Get
            Return mstrActivity_Name2
        End Get
        Set(ByVal value As String)
            mstrActivity_Name2 = value
        End Set
    End Property

    'Sohail (02 Sep 2016) -- Start
    'Enhancement -  63.1 - New Budget Redesign for Marie Stopes (Description on Fund Activity).
    Public Property _Remarks() As String
        Get
            Return mstrRemarks
        End Get
        Set(ByVal value As String)
            mstrRemarks = value
        End Set
    End Property
    'Sohail (02 Sep 2016) -- End

#End Region

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Sub GetData()
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        Dim objDataOperation As New clsDataOperation

        Try
            strQ = "SELECT " & _
                      "  fundactivityunkid " & _
                      ", activity_code " & _
                      ", activity_name " & _
                      ", fundsourceunkid " & _
                      ", fundprojectcodeunkid " & _
                      ", amount " & _
                      ", notify_amount " & _
                      ", userunkid " & _
                      ", isvoid " & _
                      ", voiduserunkid " & _
                      ", voiddatetime " & _
                      ", voidreason " & _
                      ", activity_name1 " & _
                      ", activity_name2 " & _
                      ", ISNULL(remarks, '') AS remarks " & _
                     "FROM bgfundactivity_tran " & _
                     "WHERE fundactivityunkid = @fundactivityunkid AND isvoid = 0 "
            'Sohail (02 Sep 2016) - [remarks]

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@fundactivityunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintFundactivityunkid.ToString)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For Each dtRow As DataRow In dsList.Tables(0).Rows
                mintFundactivityunkid = CInt(dtRow.Item("fundactivityunkid"))
                mstrActivity_Code = dtRow.Item("activity_code").ToString
                mstrActivity_Name = dtRow.Item("activity_name").ToString
                mintFundsourceunkid = CInt(dtRow.Item("fundsourceunkid"))
                mintFundProjectCodeunkid = CInt(dtRow.Item("fundprojectcodeunkid"))
                mdecAmount = CDec(dtRow.Item("amount"))
                mdecNotify_amount = CDec(dtRow.Item("notify_amount"))
                mintUserunkid = CInt(dtRow.Item("userunkid"))
                mblnIsvoid = CBool(dtRow.Item("isvoid"))
                mintVoiduserunkid = CInt(dtRow.Item("voiduserunkid"))
                If IsDBNull(dtRow.Item("voiddatetime")) = False Then mdtVoiddatetime = dtRow.Item("voiddatetime")
                mstrVoidreason = dtRow.Item("voidreason").ToString
                mstrActivity_Name1 = dtRow.Item("activity_name1").ToString
                mstrActivity_Name2 = dtRow.Item("activity_name2").ToString
                'Sohail (26 Aug 2016) -- Start
                'Enhancement - 57.4 & 63.1 - Process Payroll Performance enhancement for TRA.
                mstrRemarks = dtRow.Item("remarks").ToString
                'Sohail (26 Aug 2016) -- End
                Exit For
            Next
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetData; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Sub

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function GetList(ByVal strTableName As String, Optional ByVal mstrFilter As String = "", Optional ByVal blnOrderByCode As Boolean = False) As DataSet
        'Sohail (13 Oct 2017) - [blnOrderByCode]
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        Dim objDataOperation As New clsDataOperation
        objDataOperation.ClearParameters()

        Try
            strQ = "SELECT " & _
                      "  bgfundactivity_tran.fundactivityunkid " & _
                      ", bgfundactivity_tran.activity_code " & _
                      ", bgfundactivity_tran.activity_name " & _
                      ", bgfundactivity_tran.fundsourceunkid " & _
                      ", bgfundactivity_tran.fundprojectcodeunkid " & _
                      ", bgfundprojectcode_master.fundprojectcode " & _
                      ", bgfundprojectcode_master.fundprojectname " & _
                      ", bgfundactivity_tran.amount " & _
                      ", bgfundactivity_tran.notify_amount " & _
                      ", ISNULL(bgfundactivity_tran.remarks, '') AS remarks " & _
                      ", bgfundactivity_tran.userunkid " & _
                      ", bgfundactivity_tran.isvoid " & _
                      ", bgfundactivity_tran.voiduserunkid " & _
                      ", bgfundactivity_tran.voiddatetime " & _
                      ", bgfundactivity_tran.voidreason " & _
                      " FROM bgfundactivity_tran " & _
                      " LEFT JOIN bgfundprojectcode_master ON bgfundprojectcode_master.fundprojectcodeunkid = bgfundactivity_tran.fundprojectcodeunkid " & _
                      "WHERE bgfundactivity_tran.isvoid = 0 " & _
                      "AND bgfundprojectcode_master.isvoid = 0 "
            'Sohail (02 Sep 2016) - [remarks]

            If mstrFilter.Trim.Length > 0 Then
                strQ &= " AND " & mstrFilter
            End If

            'Sohail (13 Oct 2017) -- Start
            'CCBRT Enhancement - 70.1 - (Ref. Id - 110, 111) - for all Four reports the user to be able to select a range of consecutive periods so as to be able to see the total. This Feature will enable the user to select more than one default budget so as to get a consolidated report.
            'strQ &= " ORDER BY bgfundprojectcode_master.fundprojectname, bgfundactivity_tran.activity_name "
            If blnOrderByCode = True Then
                strQ &= " ORDER BY bgfundprojectcode_master.fundprojectcode, bgfundactivity_tran.activity_code "
            Else
            strQ &= " ORDER BY bgfundprojectcode_master.fundprojectname, bgfundactivity_tran.activity_name "
            End If
            'Sohail (13 Oct 2017) -- End

            dsList = objDataOperation.ExecQuery(strQ, strTableName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
        Return dsList
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> INSERT INTO Database Table (bgfundactivity_tran) </purpose>
    Public Function Insert() As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        If isExist(mstrActivity_Code, "", -1) Then
            mstrMessage = Language.getMessage(mstrModuleName, 1, "This Fund Activity Code is already defined. Please define new Fund Activity Code.")
            Return False
        End If

        'Sohail (02 Sep 2016) -- Start
        'Marie Stopes Enhancement -  63.1 - Allow to share same activity name).
        'If isExist("", mstrActivity_Name, -1) Then
        '    mstrMessage = Language.getMessage(mstrModuleName, 2, "This Fund Activity Name is already defined. Please define new Fund Activity Name.")
        '    Return False
        'End If
        If isExistActivityName(mstrActivity_Name, mintFundProjectCodeunkid, -1) Then
            mstrMessage = Language.getMessage(mstrModuleName, 2, "This Fund Activity Name is already defined. Please define new Fund Activity Name.")
            Return False
        End If
        'Sohail (02 Sep 2016) -- End

        Dim objDataOperation As New clsDataOperation
        objDataOperation.BindTransaction()

        Try
            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@activity_code", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrActivity_Code.ToString)
            objDataOperation.AddParameter("@activity_name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrActivity_Name.ToString)
            objDataOperation.AddParameter("@fundsourceunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintFundsourceunkid.ToString)
            objDataOperation.AddParameter("@fundprojectcodeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintFundProjectCodeunkid.ToString)
            objDataOperation.AddParameter("@amount", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecAmount.ToString)
            objDataOperation.AddParameter("@notify_amount", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecNotify_amount.ToString)
            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
            objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, IIf(mdtVoiddatetime <> Nothing, mdtVoiddatetime, DBNull.Value))
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)
            objDataOperation.AddParameter("@activity_name1", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrActivity_Name1.ToString)
            objDataOperation.AddParameter("@activity_name2", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrActivity_Name2.ToString)
            objDataOperation.AddParameter("@remarks", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, mstrRemarks.ToString) 'Sohail (02 Sep 2016)

            strQ = "INSERT INTO bgfundactivity_tran ( " & _
                      "  activity_code " & _
                      ", activity_name " & _
                      ", fundsourceunkid " & _
                      ", fundprojectcodeunkid " & _
                      ", amount " & _
                      ", notify_amount " & _
                      ", remarks " & _
                      ", userunkid " & _
                      ", isvoid " & _
                      ", voiduserunkid " & _
                      ", voiddatetime " & _
                      ", voidreason" & _
                      ", activity_name1 " & _
                      ", activity_name2 " & _
                    ") VALUES (" & _
                      "  @activity_code " & _
                      ", @activity_name " & _
                      ", @fundsourceunkid " & _
                      ", @fundprojectcodeunkid " & _
                      ", @amount " & _
                      ", @notify_amount " & _
                      ", @remarks " & _
                      ", @userunkid " & _
                      ", @isvoid " & _
                      ", @voiduserunkid " & _
                      ", @voiddatetime " & _
                      ", @voidreason" & _
                      ", @activity_name1 " & _
                      ", @activity_name2 " & _
                    "); SELECT @@identity"
            'Sohail (02 Sep 2016) - [remarks]

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            mintFundactivityunkid = dsList.Tables(0).Rows(0).Item(0)


            If InsertAudiTrailForFundActivity(objDataOperation, 1) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            objDataOperation.ReleaseTransaction(True)

            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Insert; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Update Database Table (bgfundactivity_tran) </purpose>
    Public Function Update(ByVal dtCurrentDateTime As DateTime, Optional ByVal objDataOp As clsDataOperation = Nothing) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        If isExist(mstrActivity_Code, "", mintFundactivityunkid) Then
            mstrMessage = Language.getMessage(mstrModuleName, 1, "This Fund Activity Code is already defined. Please define new Fund Activity Code.")
            Return False
        End If

        'Sohail (02 Sep 2016) -- Start
        'Marie Stopes Enhancement -  63.1 - Allow to share same activity name).
        'If isExist("", mstrActivity_Name, mintFundactivityunkid) Then
        '    mstrMessage = Language.getMessage(mstrModuleName, 2, "This Fund Activity Name is already defined. Please define new Fund Activity Name.")
        '    Return False
        'End If
        If isExistActivityName(mstrActivity_Name, mintFundProjectCodeunkid, mintFundactivityunkid) Then
            mstrMessage = Language.getMessage(mstrModuleName, 2, "This Fund Activity Name is already defined. Please define new Fund Activity Name.")
            Return False
        End If
        'Sohail (02 Sep 2016) -- End


        If objDataOp IsNot Nothing Then
            objDataOperation = objDataOp
        Else
            objDataOperation = New clsDataOperation
            objDataOperation.BindTransaction()
        End If

        Try
            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@fundactivityunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintFundactivityunkid.ToString)
            objDataOperation.AddParameter("@activity_code", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrActivity_Code.ToString)
            objDataOperation.AddParameter("@activity_name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrActivity_Name.ToString)
            objDataOperation.AddParameter("@fundsourceunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintFundsourceunkid.ToString)
            objDataOperation.AddParameter("@fundprojectcodeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintFundProjectCodeunkid.ToString)
            objDataOperation.AddParameter("@amount", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecAmount.ToString)
            objDataOperation.AddParameter("@notify_amount", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecNotify_amount.ToString)
            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
            objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, IIf(mdtVoiddatetime <> Nothing, mdtVoiddatetime, DBNull.Value))
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)
            objDataOperation.AddParameter("@activity_name1", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrActivity_Name1.ToString)
            objDataOperation.AddParameter("@activity_name2", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrActivity_Name2.ToString)
            objDataOperation.AddParameter("@remarks", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, mstrRemarks.ToString) 'Sohail (02 Sep 2016)

            strQ = "UPDATE bgfundactivity_tran SET " & _
                      "  activity_code = @activity_code" & _
                      ", activity_name = @activity_name" & _
                      ", fundsourceunkid = @fundsourceunkid" & _
                      ", fundprojectcodeunkid = @fundprojectcodeunkid" & _
                      ", amount = @amount" & _
                      ", notify_amount = @notify_amount" & _
                      ", remarks = @remarks" & _
                      ", activity_name1 = @activity_name1" & _
                      ", activity_name2 = @activity_name2 " & _
                      " WHERE fundactivityunkid = @fundactivityunkid AND isvoid = 0 "
            'Sohail (02 Sep 2016) - [remarks]

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If InsertAudiTrailForFundActivity(objDataOperation, 2) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If objDataOp Is Nothing Then objDataOperation.ReleaseTransaction(True)

            Return True
        Catch ex As Exception
            If objDataOp Is Nothing Then objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If objDataOp Is Nothing Then objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Delete Database Table (bgfundactivity_tran) </purpose>
    Public Function Delete(ByVal intUnkid As Integer) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        Dim objDataOperation As New clsDataOperation
        objDataOperation.BindTransaction()

        Try

            strQ = "UPDATE bgfundactivity_tran SET " & _
                     "  isvoid  = 1" & _
                     ", voiduserunkid = @voiduserunkid" & _
                     ", voiddatetime = Getdate()" & _
                     ", voidreason = @voidreason" & _
                    " WHERE fundactivityunkid = @fundactivityunkid AND isvoid = 0 "

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@fundactivityunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid)
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, mstrVoidreason)

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If InsertAudiTrailForFundActivity(objDataOperation, 3) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            objDataOperation.ReleaseTransaction(True)
            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Delete; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isUsed(ByVal intUnkid As Integer, ByRef strFormName As String) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        strFormName = ""

        objDataOperation = New clsDataOperation

        Try
            strQ = "SELECT  bgbudgetfundsource_tran.fundactivityunkid " & _
                         ", 'bgbudgetfundsource_tran' AS TName " & _
                    "FROM    bgbudgetfundsource_tran " & _
                    "WHERE   bgbudgetfundsource_tran.isvoid = 0 " & _
                            "AND bgbudgetfundsource_tran.fundactivityunkid = @fundactivityunkid " & _
                    "UNION " & _
                    "SELECT  bgfundactivityadjustment_tran.fundactivityunkid " & _
                         ", 'bgfundactivityadjustment_tran' AS TName " & _
                    "FROM    bgfundactivityadjustment_tran " & _
                    "WHERE   bgfundactivityadjustment_tran.isvoid = 0 " & _
                            "AND bgfundactivityadjustment_tran.fundactivityunkid = @fundactivityunkid "

            objDataOperation.AddParameter("@fundactivityunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If dsList.Tables(0).Rows.Count > 0 Then

                Select Case dsList.Tables(0).Rows(0).Item("TName").ToString.ToUpper

                    Case "BGBUDGETFUNDSOURCE_TRAN"
                        strFormName = "Payroll Budgeting"

                    Case "BGFUNDACTIVITYADJUSTMENT_TRAN"
                        strFormName = "Activity Adjustment"

                End Select

                If strFormName.Trim <> "" Then
                    strFormName = "  " & Language.getMessage(mstrModuleName, 5, "on") & " " & strFormName & " " & Language.getMessage(mstrModuleName, 6, "screen.")
                End If
            End If

            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isUsed; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isExist(ByVal strCode As String, ByVal strName As String, Optional ByVal intUnkid As Integer = -1) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        Dim objDataOperation As New clsDataOperation

        Try
            strQ = "SELECT " & _
                      "  fundactivityunkid " & _
                      ", activity_code " & _
                      ", activity_name " & _
                      ", fundsourceunkid " & _
                      ", fundprojectcodeunkid " & _
                      ", amount " & _
                      ", notify_amount " & _
                      ", userunkid " & _
                      ", isvoid " & _
                      ", voiduserunkid " & _
                      ", voiddatetime " & _
                      ", voidreason " & _
                      ", activity_name1 " & _
                      ", activity_name2 " & _
                      " FROM bgfundactivity_tran " & _
                      " WHERE  isvoid = 0 "

            If strCode.Trim.Length > 0 Then
                strQ &= " AND activity_code = @code  "
            End If

            If strName.Trim.Length > 0 Then
                strQ &= " AND activity_name = @name "
            End If

            If intUnkid > 0 Then
                strQ &= " AND fundactivityunkid <> @fundactivityunkid"
            End If

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@code", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strCode)
            objDataOperation.AddParameter("@name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strName)
            objDataOperation.AddParameter("@fundactivityunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isExist; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    'Sohail (02 Sep 2016) -- Start
    'Marie Stopes Enhancement -  63.1 - Allow to share same activity name).
    Public Function isExistActivityName(ByVal strName As String, ByVal intProjectCodeUnkID As Integer, Optional ByVal intUnkid As Integer = -1) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        Dim objDataOperation As New clsDataOperation

        Try
            strQ = "SELECT " & _
                      "  fundactivityunkid " & _
                      ", activity_code " & _
                      ", activity_name " & _
                      ", fundsourceunkid " & _
                      ", fundprojectcodeunkid " & _
                      ", amount " & _
                      ", notify_amount " & _
                      ", userunkid " & _
                      ", isvoid " & _
                      ", voiduserunkid " & _
                      ", voiddatetime " & _
                      ", voidreason " & _
                      ", activity_name1 " & _
                      ", activity_name2 " & _
                  " FROM bgfundactivity_tran " & _
                  " WHERE  isvoid = 0 " & _
                      " AND activity_name = @name " & _
                      " AND fundprojectcodeunkid = @fundprojectcodeunkid "

            If intUnkid > 0 Then
                strQ &= " AND fundactivityunkid <> @fundactivityunkid"
            End If

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strName)
            objDataOperation.AddParameter("@fundprojectcodeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intProjectCodeUnkID)
            objDataOperation.AddParameter("@fundactivityunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isExist; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function
    'Sohail (02 Sep 2016) -- End

    'Sohail (01 Mar 2017) -- Start
    'Enhancement - 65.1 - Export and Import option on Budget Codes.
    Public Function isExists(ByVal strFilter As String, Optional ByRef intrefFundActivityUnkid As Integer = 0) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        Dim objDataOperation As New clsDataOperation
        intrefFundActivityUnkid = 0

        Try
            strQ = "SELECT " & _
                      "  fundactivityunkid " & _
                      ", activity_code " & _
                      ", activity_name " & _
                      ", fundsourceunkid " & _
                      ", fundprojectcodeunkid " & _
                      ", amount " & _
                      ", notify_amount " & _
                      ", userunkid " & _
                      ", isvoid " & _
                      ", voiduserunkid " & _
                      ", voiddatetime " & _
                      ", voidreason " & _
                      ", activity_name1 " & _
                      ", activity_name2 " & _
                      " FROM bgfundactivity_tran " & _
                      " WHERE  isvoid = 0 "

            If strFilter.Trim.Length > 0 Then
                strQ &= " " & strFilter
            End If


            objDataOperation.ClearParameters()

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If dsList.Tables(0).Rows.Count > 0 Then
                intrefFundActivityUnkid = CInt(dsList.Tables(0).Rows(0).Item("fundactivityunkid"))
            End If

            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isExist; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function
    'Sohail (01 Mar 2017) -- End

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function GetComboList(Optional ByVal strTableName As String = "", Optional ByVal blnAddSelect As Boolean = True, Optional ByVal xFundProjectCodeID As Integer = 0, Optional ByVal blnOrderByCode As Boolean = False) As DataSet
        'Sohail (13 Oct 2017) - [blnOrderByCode]
        Dim strQ As String = String.Empty
        Dim dsList As New DataSet
        Dim exForce As Exception
        Try

            Dim objDataOperation As New clsDataOperation
            objDataOperation.ClearParameters()


            If blnAddSelect = True Then
                strQ = " SELECT " & _
                            "  0 AS fundactivityunkid " & _
                            ", ' ' + @select AS activitycode " & _
                            ", ' ' + @select AS activityname " & _
                       " UNION "
                'Nilay (22 Nov 2016) -- Start
                'Enhancement #24 -  65.1 - Userability and enhancement for budgeting screen and budget code screen
                '['' AS activitycode] REPLACED BY [' ' + @select AS activitycode]
                'Nilay (22 Nov 2016) -- End

                objDataOperation.AddParameter("@select", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 3, "Select"))
            End If

            strQ &= " SELECT " & _
                            "  bgfundactivity_tran.fundactivityunkid AS fundactivityunkid " & _
                            ", bgfundactivity_tran.activity_code AS activitycode " & _
                            ", bgfundactivity_tran.activity_name AS activityname " & _
                    " FROM bgfundactivity_tran " & _
                    " WHERE isvoid = 0 "

            If xFundProjectCodeID > 0 Then
                strQ &= " AND bgfundactivity_tran.fundprojectcodeunkid = @fundprojectcodeunkid "
                objDataOperation.AddParameter("@fundprojectcodeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, xFundProjectCodeID)
            Else
                strQ &= " AND bgfundactivity_tran.fundprojectcodeunkid > 0 "
            End If

            'Sohail (13 Oct 2017) -- Start
            'CCBRT Enhancement - 70.1 - (Ref. Id - 110, 111) - for all Four reports the user to be able to select a range of consecutive periods so as to be able to see the total. This Feature will enable the user to select more than one default budget so as to get a consolidated report.
            'strQ &= " ORDER BY activityname "
            If blnOrderByCode = True Then
                strQ &= " ORDER BY activitycode "
            Else
            strQ &= " ORDER BY activityname "
            End If
            'Sohail (13 Oct 2017) -- End

            dsList = objDataOperation.ExecQuery(strQ, strTableName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetComboList; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
        Return dsList
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function GetFundActivityTotal(ByVal xFundProjectCodeID As Integer, ByRef mdecBalance As Decimal, ByRef strProjectCode As String) As Decimal
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim mdecTotal As Decimal = 0
        Dim ret_strProjectCode As String = ""
        Dim exForce As Exception

        Dim objDataOperation As New clsDataOperation

        Try
            strQ = "SELECT " & _
                      " ISNULL(SUM(amount),0.00) AS Total " & _
                      " FROM bgfundactivity_tran " & _
                      " WHERE  isvoid = 0 AND  fundprojectcodeunkid = @fundprojectcodeunkid "

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@fundprojectcodeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, xFundProjectCodeID)
            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If dsList.Tables(0).Rows.Count > 0 Then
                mdecTotal = CDec(dsList.Tables(0).Rows(0)("Total"))
            End If

            strQ = "SELECT " & _
                      " ISNULL(currentceilingbalance,0.00) AS balance " & _
                      ", bgfundprojectcode_master.fundprojectcode " & _
                      " FROM bgfundprojectcode_master " & _
                      " WHERE  isvoid = 0 AND  fundprojectcodeunkid = @fundprojectcodeunkid "
            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@fundprojectcodeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, xFundProjectCodeID)
            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If dsList.Tables(0).Rows.Count > 0 Then
                mdecBalance = CDec(dsList.Tables(0).Rows(0)("balance"))
                ret_strProjectCode = dsList.Tables(0).Rows(0)("fundprojectcode").ToString
            End If

            strProjectCode = ret_strProjectCode
          
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetFundActivityTotal; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
        Return mdecTotal
    End Function

    ''' <summary>
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function InsertAudiTrailForFundActivity(ByVal objDataOperation As clsDataOperation, ByVal AuditType As Integer) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Try

            strQ = "INSERT INTO atbgfundactivity_tran ( " & _
                      " fundactivityunkid " & _
                      ", activity_code " & _
                      ", activity_name " & _
                      ", fundsourceunkid " & _
                      ", fundprojectcodeunkid " & _
                      ", amount " & _
                      ", notify_amount " & _
                      ", remarks " & _
                      ", activity_name1 " & _
                      ", activity_name2 " & _
                      ", AuditType " & _
                      ", audituserunkid " & _
                      ", auditdatetime " & _
                      ", ip " & _
                      ", machine_name " & _
                      ", form_name " & _
                      " " & _
                      " " & _
                      " " & _
                      " " & _
                      " " & _
                      ", isweb " & _
                ") VALUES (" & _
                     " @fundactivityunkid " & _
                     ", @activity_code " & _
                     ", @activity_name " & _
                     ", @fundsourceunkid " & _
                     ", @fundprojectcodeunkid " & _
                     ", @amount " & _
                     ", @notify_amount " & _
                     ", @remarks " & _
                     ", @activity_name1 " & _
                     ", @activity_name2 " & _
                     ", @auditType " & _
                     ", @audituserunkid " & _
                     ", getdate() " & _
                     ", @ip " & _
                     ", @machine_name " & _
                     ", @form_name " & _
                     " " & _
                     " " & _
                     " " & _
                     " " & _
                     " " & _
                     ", @isweb " & _
                "); "

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@fundactivityunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintFundactivityunkid.ToString)
            objDataOperation.AddParameter("@activity_code", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrActivity_Code.ToString)
            objDataOperation.AddParameter("@activity_name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrActivity_Name.ToString)
            objDataOperation.AddParameter("@fundsourceunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintFundsourceunkid.ToString)
            objDataOperation.AddParameter("@fundprojectcodeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintFundProjectCodeunkid.ToString)
            objDataOperation.AddParameter("@amount", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecAmount.ToString)
            objDataOperation.AddParameter("@notify_amount", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecNotify_amount.ToString)
            objDataOperation.AddParameter("@activity_name1", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrActivity_Name1.ToString)
            objDataOperation.AddParameter("@activity_name2", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrActivity_Name2.ToString) 'Sohail (02 Sep 2016)
            objDataOperation.AddParameter("@remarks", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, mstrRemarks.ToString)
            objDataOperation.AddParameter("@audittype", SqlDbType.Int, eZeeDataType.INT_SIZE, AuditType.ToString)
            objDataOperation.AddParameter("@audituserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid)
            objDataOperation.AddParameter("@ip", SqlDbType.NVarChar, eZeeDataType.IP_SIZE, getIP)
            objDataOperation.AddParameter("@machine_name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, getHostName)

            If mstrWebFormName.Trim.Length <= 0 Then
                objDataOperation.AddParameter("@form_name", SqlDbType.NVarChar, 500, mstrFormName)
                objDataOperation.AddParameter("@isweb", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsWeb)

            Else
                objDataOperation.AddParameter("@form_name", SqlDbType.NVarChar, 500, mstrWebFormName)
                objDataOperation.AddParameter("@isweb", SqlDbType.Bit, eZeeDataType.BIT_SIZE, True)
                objDataOperation.AddParameter("@module_name1", SqlDbType.NVarChar, 500, Language.getMessage(mstrModuleName, 4, "WEB"))
            End If





            objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If
            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: InsertAudiTrailForFundActivity; Module Name: " & mstrModuleName)
            Return False
        End Try
    End Function

	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Public Shared Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "This Fund Activity Code is already defined. Please define new Fund Activity Code.")
			Language.setMessage(mstrModuleName, 2, "This Fund Activity Name is already defined. Please define new Fund Activity Name.")
			Language.setMessage(mstrModuleName, 3, "Select")
			Language.setMessage(mstrModuleName, 4, "WEB")
			Language.setMessage(mstrModuleName, 5, "on")
			Language.setMessage(mstrModuleName, 6, "screen.")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class