﻿'************************************************************************************************************************************
'Class Name : clsFundProjectCode.vb
'Purpose    : New Budget Module
'Date       : 20/05/2016
'Written By : Sohail
'Modified   :
'************************************************************************************************************************************

Imports eZeeCommonLib
Imports eZee.Common.eZeeForm
''' <summary>
''' Purpose: 
''' Developer: Sohail
''' </summary>
Public Class clsFundProjectCode
    Private Shared ReadOnly mstrModuleName As String = "clsFundProjectCode"
    Dim objDataOperation As clsDataOperation
    Dim mstrMessage As String = ""

#Region " Private variables "
    Private mintFundProjectcodeunkid As Integer
    Private mstrFundProjectCode As String = String.Empty
    Private mstrFundProjectName As String = String.Empty
    Private mstrFundProjectName1 As String = String.Empty
    Private mstrFundProjectName2 As String = String.Empty
    Private mintFundSourceunkid As Integer
    Private mdecCurrentCeilingBalance As Decimal = 0
    Private mdtFundProjectCodeExpiryDate As DateTime
    Private mdecNotify_amount As Decimal
    Private mintUserunkid As Integer
    Private mblnIsvoid As Boolean
    Private mintVoiduserunkid As Integer
    Private mdtVoiddatetime As DateTime
    Private mstrVoidreason As String = String.Empty
    'Private mstrWebClientIP As String = String.Empty
    'Private mstrWebHostName As String = String.Empty
    'Private mstrWebFormName As String = String.Empty
#End Region

#Region " Properties "

    'S.SANDEEP [28-May-2018] -- START 
    'ISSUE/ENHANCEMENT : {Audit Trails} 
    Private mstrFormName As String = String.Empty
    Public WriteOnly Property _FormName() As String
        Set(ByVal value As String)
            mstrFormName = value
        End Set
    End Property

    Private mstrClientIP As String = ""
    Public WriteOnly Property _ClientIP() As String
        Set(ByVal value As String)
            mstrClientIP = value
        End Set
    End Property

    Private mstrHostName As String = ""
    Public WriteOnly Property _HostName() As String
        Set(ByVal value As String)
            mstrHostName = value
        End Set
    End Property

    Private mblnIsWeb As Boolean = False
    Public WriteOnly Property _FromWeb() As Boolean
        Set(ByVal value As Boolean)
            mblnIsWeb = value
        End Set
    End Property

    Private mintAuditUserId As Integer = 0
    Public WriteOnly Property _AuditUserId() As Integer
        Set(ByVal value As Integer)
            mintAuditUserId = value
        End Set
    End Property

    Private mintCompanyUnkid As Integer = 0
    Public WriteOnly Property _CompanyUnkid() As Integer
        Set(ByVal value As Integer)
            mintCompanyUnkid = value
        End Set
    End Property

    Private mdtAuditDate As DateTime = Now
    Public WriteOnly Property _AuditDate() As DateTime
        Set(ByVal value As DateTime)
            mdtAuditDate = value
        End Set
    End Property

    Private xDataOpr As clsDataOperation = Nothing
    Public Property _xDataOpr() As clsDataOperation
        Get
            Return xDataOpr
        End Get
        Set(ByVal value As clsDataOperation)
            xDataOpr = value
        End Set
    End Property

    Private mintLoginEmployeeunkid As Integer = 0
    Public Property _LoginEmployeeunkid() As Integer
        Get
            Return mintLoginEmployeeunkid
        End Get
        Set(ByVal value As Integer)
            mintLoginEmployeeunkid = value
        End Set
    End Property

    Private mintVoidlogingemployeeunkid As Integer = 0
    Public Property _Voidloginemployeeunkid() As Integer
        Get
            Return mintVoidlogingemployeeunkid
        End Get
        Set(ByVal value As Integer)
            mintVoidlogingemployeeunkid = value
        End Set
    End Property
    'S.SANDEEP [28-May-2018] -- END 

    Public ReadOnly Property _Message() As String
        Get
            Return mstrMessage
        End Get
    End Property

    ''' <summary>
    ''' Purpose: Get or Set fundprojectcodeunkid
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _FundProjectCodeunkid() As Integer
        Get
            Return mintFundProjectcodeunkid
        End Get
        Set(ByVal value As Integer)
            mintFundProjectcodeunkid = value
            Call GetData()
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set fundprojectcode
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _FundProjectCode() As String
        Get
            Return mstrFundProjectCode
        End Get
        Set(ByVal value As String)
            mstrFundProjectCode = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set fundprojectname
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _FundProjectName() As String
        Get
            Return mstrFundProjectName
        End Get
        Set(ByVal value As String)
            mstrFundProjectName = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set fundprojectname
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _FundProjectName1() As String
        Get
            Return mstrFundProjectName1
        End Get
        Set(ByVal value As String)
            mstrFundProjectName1 = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set fundprojectname
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _FundProjectName2() As String
        Get
            Return mstrFundProjectName2
        End Get
        Set(ByVal value As String)
            mstrFundProjectName2 = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set fundsourceunkid
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _FundSourceunkid() As Integer
        Get
            Return mintFundSourceunkid
        End Get
        Set(ByVal value As Integer)
            mintFundSourceunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set currentceilingbalance
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _CurrentCeilingBalance() As Decimal
        Get
            Return mdecCurrentCeilingBalance
        End Get
        Set(ByVal value As Decimal)
            mdecCurrentCeilingBalance = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set fundprojectcodeexpirydate
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _FundProjectCodeExpiryDate() As DateTime
        Get
            Return mdtFundProjectCodeExpiryDate
        End Get
        Set(ByVal value As DateTime)
            mdtFundProjectCodeExpiryDate = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set notify_amount
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Notify_Amount() As Decimal
        Get
            Return mdecNotify_amount
        End Get
        Set(ByVal value As Decimal)
            mdecNotify_amount = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set userunkid
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Userunkid() As Integer
        Get
            Return mintUserunkid
        End Get
        Set(ByVal value As Integer)
            mintUserunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isvoid
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Isvoid() As Boolean
        Get
            Return mblnIsvoid
        End Get
        Set(ByVal value As Boolean)
            mblnIsvoid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiduserunkid
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Voiduserunkid() As Integer
        Get
            Return mintVoiduserunkid
        End Get
        Set(ByVal value As Integer)
            mintVoiduserunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiddatetime
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Voiddatetime() As DateTime
        Get
            Return mdtVoiddatetime
        End Get
        Set(ByVal value As DateTime)
            mdtVoiddatetime = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voidreason
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Voidreason() As String
        Get
            Return mstrVoidreason
        End Get
        Set(ByVal value As String)
            mstrVoidreason = value
        End Set
    End Property

    'Public Property _WebClientIP() As String
    '    Get
    '        Return mstrWebClientIP
    '    End Get
    '    Set(ByVal value As String)
    '        mstrWebClientIP = value
    '    End Set
    'End Property

    'Public Property _WebHostName() As String
    '    Get
    '        Return mstrWebHostName
    '    End Get
    '    Set(ByVal value As String)
    '        mstrWebHostName = value
    '    End Set
    'End Property

    'Public Property _WebFormName() As String
    '    Get
    '        Return mstrWebFormName
    '    End Get
    '    Set(ByVal value As String)
    '        mstrWebFormName = value
    '    End Set
    'End Property


#End Region

    ''' <summary>
    ''' Modify By: Sohail
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Sub GetData(Optional ByVal objDataOp As clsDataOperation = Nothing)
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        If objDataOp IsNot Nothing Then
            objDataOperation = objDataOp
        Else
            objDataOperation = New clsDataOperation
        End If

        Try
            strQ = " SELECT " & _
                        "  fundprojectcodeunkid " & _
                        ", fundprojectcode " & _
                        ", fundprojectname " & _
                        ", fundprojectname1 " & _
                        ", fundprojectname2 " & _
                        ", fundsourceunkid " & _
                        ", currentceilingbalance " & _
                        ", fundprojectcodeexpirydate " & _
                        ", notify_amount " & _
                        ", userunkid " & _
                        ", isvoid " & _
                        ", voiduserunkid " & _
                        ", voiddatetime " & _
                        ", voidreason " & _
                   " FROM bgfundprojectcode_master " & _
                   " WHERE fundprojectcodeunkid = @fundprojectcodeunkid "

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@fundprojectcodeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintFundProjectcodeunkid.ToString)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For Each dtRow As DataRow In dsList.Tables(0).Rows
                mintFundProjectcodeunkid = CInt(dtRow.Item("fundprojectcodeunkid"))
                mstrFundProjectCode = dtRow.Item("fundprojectcode").ToString
                mstrFundProjectName = dtRow.Item("fundprojectname").ToString
                mstrFundProjectName1 = dtRow.Item("fundprojectname1").ToString
                mstrFundProjectName2 = dtRow.Item("fundprojectname2").ToString
                mintFundSourceunkid = CInt(dtRow.Item("fundsourceunkid"))
                mdecCurrentCeilingBalance = dtRow.Item("currentceilingbalance")
                mdtFundProjectCodeExpiryDate = dtRow.Item("fundprojectcodeexpirydate")
                mdecNotify_amount = CDec(dtRow.Item("notify_amount"))
                mintUserunkid = dtRow.Item("userunkid")
                mblnIsvoid = CBool(dtRow.Item("isvoid"))
                mintVoiduserunkid = CInt(dtRow.Item("voiduserunkid"))
                mdtVoiddatetime = IIf(IsDBNull(dtRow.Item("voiddatetime")), Nothing, dtRow.Item("voiddatetime"))
                mstrVoidreason = dtRow.Item("voidreason").ToString
                Exit For
            Next

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetData; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
        End Try
    End Sub


    ''' <summary>
    ''' Modify By: Sohail
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function GetList(ByVal strTableName As String, Optional ByVal strFilter As String = "", Optional ByVal blnOrderByCode As Boolean = False, Optional ByVal blnExcludeExpiredProjectCode As Boolean = False, Optional ByVal dtExpiredDate As Date = Nothing) As DataSet
        'Hemant (22 Mar 2019) - [blnExcludeExpiredProjectCode,dtExpiredDate]
        'Sohail (13 Oct 2017) - [blnOrderByCode]
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = " SELECT " & _
                      "  bgfundprojectcode_master.fundprojectcodeunkid " & _
                      ", bgfundprojectcode_master.fundprojectcode " & _
                      ", bgfundprojectcode_master.fundprojectname " & _
                      ", bgfundprojectcode_master.fundprojectname1 " & _
                      ", bgfundprojectcode_master.fundprojectname2 " & _
                      ", bgfundprojectcode_master.fundsourceunkid " & _
                      ", bgfundsource_master.fundname " & _
                      ", bgfundprojectcode_master.currentceilingbalance " & _
                      ", bgfundprojectcode_master.fundprojectcodeexpirydate " & _
                      ", bgfundprojectcode_master.notify_amount " & _
                      ", bgfundprojectcode_master.userunkid " & _
                      ", bgfundprojectcode_master.isvoid " & _
                      ", bgfundprojectcode_master.voiduserunkid " & _
                      ", bgfundprojectcode_master.voiddatetime " & _
                      ", bgfundprojectcode_master.voidreason " & _
                   " FROM bgfundprojectcode_master " & _
                   " LEFT JOIN bgfundsource_master ON bgfundprojectcode_master.fundsourceunkid = bgfundsource_master.fundsourceunkid " & _
                   " WHERE bgfundprojectcode_master.isvoid = 0 " & _
                   " AND bgfundsource_master.isvoid = 0 "

            'Hemant (22 Mar 2019) -- Start
            'ISSUE/ENHANCEMENT#3621: Expiry Dates for Projects should have some impacts in some screens and reports.
            If blnExcludeExpiredProjectCode = True Then
                strQ &= " AND CONVERT(CHAR(8),fundprojectcodeexpirydate,112) >= @ExpiredDate "
                objDataOperation.AddParameter("@ExpiredDate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(dtExpiredDate).ToString())
            End If
            'Hemant (22 Mar 2019) -- End

            If strFilter.Trim.Length > 0 Then
                strQ &= " AND " & strFilter
            End If

            'Sohail (13 Oct 2017) -- Start
            'CCBRT Enhancement - 70.1 - (Ref. Id - 110, 111) - for all Four reports the user to be able to select a range of consecutive periods so as to be able to see the total. This Feature will enable the user to select more than one default budget so as to get a consolidated report.
            If blnOrderByCode = True Then
                strQ &= " Order By fundprojectcode "
            Else
                strQ &= " Order By fundprojectname "
            End If
            'Sohail (13 Oct 2017) -- End

            dsList = objDataOperation.ExecQuery(strQ, strTableName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
        Return dsList
    End Function

    ''' <summary>
    ''' Modify By: Sohail
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function GetComboList(Optional ByVal strTableName As String = "", Optional ByVal blnAddSelect As Boolean = True, Optional ByVal xFundSourceID As Integer = 0, Optional ByVal blnOrderByCode As Boolean = False) As DataSet
        'Pinkal (21-Oct-2016) 'Enhancement - Working on Budget Employee Timesheet as Per Mr.Requirement.[Optional ByVal xFundSourceID As Integer = 0, blnOrderByCode]


        Dim strQ As String = String.Empty
        Dim objDataOperation As New clsDataOperation
        objDataOperation.ClearParameters()
        Dim dsList As New DataSet
        Dim exForce As Exception
        Try
            If blnAddSelect = True Then
                strQ = " SELECT " & _
                            "  0 AS fundprojectcodeunkid " & _
                            ", ' ' + @select AS fundprojectcode " & _
                            ", ' ' + @select AS fundprojectname " & _
                       " UNION "
                'Nilay (22 Nov 2016) -- Start
                'Enhancement #24 -  65.1 - Userability and enhancement for budgeting screen and budget code screen
                '['' AS fundprojectcode] REPLACED BY [' ' + @select AS fundprojectcode]
                'Nilay (22 Nov 2016) -- End
                objDataOperation.AddParameter("@select", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 4, "Select"))
            End If

            strQ &= " SELECT " & _
                            "  fundprojectcodeunkid " & _
                            ", fundprojectcode " & _
                            ", fundprojectname " & _
                    " FROM bgfundprojectcode_master " & _
                    " WHERE isvoid = 0 "


            'Pinkal (21-Oct-2016) -- Start
            'Enhancement - Working on Budget Employee Timesheet as Per Mr.Requirement.
            If xFundSourceID > 0 Then
                strQ &= " AND fundsourceunkid = @fundsourceunkid"
                objDataOperation.AddParameter("@fundsourceunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, xFundSourceID)
            End If

            If blnOrderByCode = True Then
                strQ &= " ORDER BY fundprojectcode "
            Else
            strQ &= " ORDER BY fundprojectname "
            End If
            'Pinkal (21-Oct-2016) -- End

            dsList = objDataOperation.ExecQuery(strQ, strTableName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetComboList; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
        Return dsList
    End Function

    ''' <summary>
    ''' Modify By: Sohail
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> INSERT INTO Database Table (bgfundprojectcode_master) </purpose>
    Public Function Insert(ByVal dtCurrentDateTime As DateTime) As Boolean

        If isExist(mstrFundProjectCode) Then
            mstrMessage = Language.getMessage(mstrModuleName, 1, "This Project Code is already defined. Please define new Project Code.")
            Return False
        End If

        If isExist("", mstrFundProjectName) Then
            mstrMessage = Language.getMessage(mstrModuleName, 2, "This Project Name is already defined. Please define new Project Name.")
            Return False
        End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation
        objDataOperation.BindTransaction()

        Try
            objDataOperation.AddParameter("@fundprojectcode", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrFundProjectCode.ToString)
            objDataOperation.AddParameter("@fundprojectname", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrFundProjectName.ToString)
            objDataOperation.AddParameter("@fundprojectname1", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrFundProjectName1.ToString)
            objDataOperation.AddParameter("@fundprojectname2", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrFundProjectName2.ToString)
            objDataOperation.AddParameter("@fundsourceunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintFundSourceunkid.ToString)
            objDataOperation.AddParameter("@currentceilingbalance", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecCurrentCeilingBalance.ToString)
            objDataOperation.AddParameter("@fundprojectcodeexpirydate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, IIf(mdtFundProjectCodeExpiryDate <> Nothing, mdtFundProjectCodeExpiryDate, DBNull.Value))
            objDataOperation.AddParameter("@notify_amount", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecNotify_amount.ToString)
            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
            objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, IIf(mdtVoiddatetime <> Nothing, mdtVoiddatetime, DBNull.Value))
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)

            strQ = "INSERT INTO bgfundprojectcode_master ( " & _
                       "  fundprojectcode " & _
                       ", fundprojectname " & _
                       ", fundprojectname1 " & _
                       ", fundprojectname2 " & _
                       ", fundsourceunkid " & _
                       ", currentceilingbalance " & _
                       ", fundprojectcodeexpirydate " & _
                       ", notify_amount " & _
                       ", userunkid " & _
                       ", isvoid " & _
                       ", voiduserunkid " & _
                       ", voiddatetime " & _
                       ", voidreason" & _
                   ") VALUES (" & _
                       "  @fundprojectcode " & _
                       ", @fundprojectname " & _
                       ", @fundprojectname1 " & _
                       ", @fundprojectname2 " & _
                       ", @fundsourceunkid " & _
                       ", @currentceilingbalance " & _
                       ", @fundprojectcodeexpirydate " & _
                       ", @notify_amount " & _
                       ", @userunkid " & _
                       ", @isvoid " & _
                       ", @voiduserunkid " & _
                       ", @voiddatetime " & _
                       ", @voidreason" & _
                   "); SELECT @@identity"

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            mintFundProjectcodeunkid = dsList.Tables(0).Rows(0).Item(0)

            If InsertATFundProjectCodeMaster(objDataOperation, enAuditType.ADD, dtCurrentDateTime) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            'Sohail (01 Apr 2017) -- Start
            'MST Issue - 65.2 - Budget codes % were not importing for the project codes which are created after budget is approved.
            Dim objFundSource As New clsBudgetfundsource_Tran
            With objFundSource
                ._FormName = mstrFormName
                ._LoginEmployeeunkid = mintLoginEmployeeunkid
                ._ClientIP = mstrClientIP
                ._HostName = mstrHostName
                ._FromWeb = mblnIsWeb
                ._AuditUserId = mintAuditUserId
._CompanyUnkid = mintCompanyUnkid
                ._AuditDate = mdtAuditDate
            End With
            If objFundSource.InsertAllForNewProjectCodes(objDataOperation, mintFundProjectcodeunkid, 1, mintUserunkid, mstrClientIP, mstrHostName, mstrFormName, dtCurrentDateTime) = False Then
                objDataOperation.ReleaseTransaction(False)
                Return False
            End If
            'Sohail (01 Apr 2017) -- End

            objDataOperation.ReleaseTransaction(True)
            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Insert; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sohail
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Update Database Table (bgfundprojectcode_master) </purpose>
    Public Function Update(ByVal dtCurrentDateTime As DateTime, Optional ByVal objDataOp As clsDataOperation = Nothing) As Boolean

        If isExist(mstrFundProjectCode, "", 0, mintFundProjectcodeunkid) Then
            mstrMessage = Language.getMessage(mstrModuleName, 1, "This Project Code is already defined. Please define new Project Code.")
            Return False
        End If

        If isExist("", mstrFundProjectName, 0, mintFundProjectcodeunkid) Then
            mstrMessage = Language.getMessage(mstrModuleName, 2, "This Project Name is already defined. Please define new Project Name.")
            Return False
        End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        If objDataOp IsNot Nothing Then
            objDataOperation = objDataOp
        Else
            objDataOperation = New clsDataOperation
            objDataOperation.BindTransaction()
        End If

        Try
            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@fundprojectcodeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintFundProjectcodeunkid.ToString)
            objDataOperation.AddParameter("@fundprojectcode", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrFundProjectCode.ToString)
            objDataOperation.AddParameter("@fundprojectname", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrFundProjectName.ToString)
            objDataOperation.AddParameter("@fundprojectname1", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrFundProjectName1.ToString)
            objDataOperation.AddParameter("@fundprojectname2", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrFundProjectName2.ToString)
            objDataOperation.AddParameter("@fundsourceunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintFundSourceunkid.ToString)
            objDataOperation.AddParameter("@currentceilingbalance", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecCurrentCeilingBalance.ToString)
            objDataOperation.AddParameter("@notify_amount", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecNotify_amount.ToString)
            objDataOperation.AddParameter("@fundprojectcodeexpirydate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, IIf(mdtFundProjectCodeExpiryDate <> Nothing, mdtFundProjectCodeExpiryDate, DBNull.Value))
            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
            objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, IIf(mdtVoiddatetime <> Nothing, mdtVoiddatetime, DBNull.Value))
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)

            strQ = "UPDATE bgfundprojectcode_master SET " & _
                       "  fundprojectcode = @fundprojectcode" & _
                       ", fundprojectname = @fundprojectname" & _
                       ", fundprojectname1 = @fundprojectname1" & _
                       ", fundprojectname2 = @fundprojectname2" & _
                       ", fundsourceunkid = @fundsourceunkid" & _
                       ", currentceilingbalance = @currentceilingbalance" & _
                       ", fundprojectcodeexpirydate = @fundprojectcodeexpirydate" & _
                       ", notify_amount = @notify_amount" & _
                       ", userunkid = @userunkid " & _
                       ", isvoid = @isvoid" & _
                       ", voiduserunkid = @voiduserunkid" & _
                       ", voiddatetime = @voiddatetime" & _
                       ", voidreason = @voidreason " & _
                   "WHERE fundprojectcodeunkid = @fundprojectcodeunkid "

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            'S.SANDEEP [28-May-2018] -- START
            'ISSUE/ENHANCEMENT : {Audit Trails} 
            Dim objCommonATLog As New clsCommonATLog
            objCommonATLog._FormName = mstrFormName
            objCommonATLog._LoginEmployeeUnkid = mintLoginEmployeeunkid
            objCommonATLog._ClientIP = mstrClientIP
            objCommonATLog._HostName = mstrHostName
            objCommonATLog._FromWeb = mblnIsWeb
            objCommonATLog._AuditUserId = mintAuditUserId
objCommonATLog._CompanyUnkid = mintCompanyUnkid
            objCommonATLog._AuditDate = mdtAuditDate
            'S.SANDEEP [28-May-2018] -- END

            If objCommonATLog.IsTableDataUpdateExists("atbgfundprojectcode_master", "bgfundprojectcode_master", mintFundProjectcodeunkid, _
                                                      "fundprojectcodeunkid", 2, objDataOperation) Then
                If InsertATFundProjectCodeMaster(objDataOperation, enAuditType.EDIT, dtCurrentDateTime) = False Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If
            End If

            Dim objFundSource As New clsBudgetfundsource_Tran
            With objFundSource
                ._FormName = mstrFormName
                ._LoginEmployeeunkid = mintLoginEmployeeunkid
                ._ClientIP = mstrClientIP
                ._HostName = mstrHostName
                ._FromWeb = mblnIsWeb
                ._AuditUserId = mintAuditUserId
._CompanyUnkid = mintCompanyUnkid
                ._AuditDate = mdtAuditDate
            End With
            If objFundSource.InsertAllForNewProjectCodes(objDataOperation, mintFundProjectcodeunkid, 1, mintUserunkid, mstrClientIP, mstrHostName, mstrFormName, dtCurrentDateTime) = False Then
                objDataOperation.ReleaseTransaction(False)
                Return False
            End If
            'Sohail (01 Apr 2017) -- End

            If objDataOp Is Nothing Then objDataOperation.ReleaseTransaction(True)
            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If objDataOp Is Nothing Then objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sohail
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Delete Database Table (bgfundprojectcode_master) </purpose>
    Public Function Delete(ByVal intFundProjectCodeunkid As Integer, ByVal dtCurrentDateTime As DateTime) As Boolean

        If isUsed(intFundProjectCodeunkid) Then
            mstrMessage = Language.getMessage(mstrModuleName, 4, "Sorry, You cannot Delete this Fund Source. Reason: This Fund Source is in use.")
            Return False
        End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation
        objDataOperation.BindTransaction()

        Try
            strQ = "UPDATE bgfundprojectcode_master SET " & _
                        "  isvoid = @isvoid " & _
                        ", voiduserunkid = @voiduserunkid " & _
                        ", voiddatetime = @voiddatetime " & _
                        ", voidreason = @voidreason " & _
                   "WHERE fundprojectcodeunkid = @fundprojectcodeunkid "

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@fundprojectcodeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intFundProjectCodeunkid)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
            objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, IIf(mdtVoiddatetime <> Nothing, mdtVoiddatetime, DBNull.Value))
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            mintFundProjectcodeunkid = intFundProjectCodeunkid
            Call GetData(objDataOperation)

            If InsertATFundProjectCodeMaster(objDataOperation, enAuditType.DELETE, dtCurrentDateTime) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            objDataOperation.ReleaseTransaction(True)
            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Delete; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sohail
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isUsed(ByVal intUnkid As Integer) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim blnIsUsed As Boolean = False

        objDataOperation = New clsDataOperation

        Try
            strQ = "SELECT " & _
                   "       'SELECT 1 FROM ' + INFORMATION_SCHEMA.TABLES.TABLE_NAME + ' WHERE ' + COLUMN_NAME + ' = @fundprojectcodeunkid AND isvoid = 0 ' AS SQL_QRY " & _
                   " FROM INFORMATION_SCHEMA.TABLES " & _
                   "       JOIN INFORMATION_SCHEMA.COLUMNS ON INFORMATION_SCHEMA.TABLES.TABLE_NAME = INFORMATION_SCHEMA.COLUMNS.TABLE_NAME " & _
                   " WHERE TABLES.TABLE_NAME like 'bg%' AND TABLES.TABLE_NAME NOT IN('bgfundprojectcode_master') " & _
                   "       AND COLUMN_NAME IN ('fundprojectcodeunkid') "

            objDataOperation.AddParameter("@fundprojectcodeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            strQ = ""

            For Each dRow As DataRow In dsList.Tables("List").Rows

                strQ = dRow.Item("SQL_QRY").ToString.Replace("@fundprojectcodeunkid", intUnkid)
                'Sohail (23 May 2017) -- Start
                'Enhancement - 67.1 - Link budget with Payroll.
                If strQ.Contains("FROM bgbudgetcodesfundsource_tran") = True OrElse strQ.Contains("FROM bgbudgetfundsource_tran") = True Then
                    strQ &= " AND percentage > 0 "
                End If
                'Sohail (23 May 2017) -- End
                dsList = objDataOperation.ExecQuery(strQ, "Used")

                If dsList.Tables("Used").Rows.Count > 0 Then
                    blnIsUsed = True
                    Exit For
                End If
            Next

            Return blnIsUsed

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isUsed; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sohail
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isExist(Optional ByVal strCode As String = "", Optional ByVal strName As String = "", Optional ByVal intFundsourceunkid As Integer = 0, Optional ByVal intUnkid As Integer = -1) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        Dim objDataOperation As New clsDataOperation

        Try
            strQ = "SELECT " & _
                       "  fundprojectcodeunkid " & _
                       ", fundprojectcode " & _
                       ", fundprojectname " & _
                       ", fundsourceunkid " & _
                       ", currentceilingbalance " & _
                       ", fundprojectcodeexpirydate " & _
                       ", notify_amount " & _
                       ", userunkid " & _
                       ", isvoid " & _
                       ", voiduserunkid " & _
                       ", voiddatetime " & _
                       ", voidreason " & _
                   "FROM bgfundprojectcode_master " & _
                   "WHERE isvoid = 0 "

            If strCode.Trim.Length > 0 Then
                strQ &= " AND fundprojectcode = @fundprojectcode "
            End If

            If strName.Trim.Length > 0 Then
                strQ &= " AND fundprojectname = @fundprojectname "
            End If

            If intFundsourceunkid > 0 Then
                strQ &= " AND fundsourceunkid = @fundsourceunkid "
            End If

            If intUnkid > 0 Then
                strQ &= " AND fundprojectcodeunkid <> @fundprojectcodeunkid"
            End If

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@fundprojectcode", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strCode)
            objDataOperation.AddParameter("@fundprojectname", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strName)
            objDataOperation.AddParameter("@fundsourceunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intFundsourceunkid)
            objDataOperation.AddParameter("@fundprojectcodeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isExist; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sohail
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function InsertATFundProjectCodeMaster(ByVal objDataOperation As clsDataOperation, ByVal intAuditType As enAuditType, _
                                                  ByVal dtCurrentDateTime As DateTime) As Boolean

        Dim strQ As String = ""
        Dim exForce As Exception

        Try
            strQ = "INSERT INTO atbgfundprojectcode_master ( " & _
                       "  fundprojectcodeunkid " & _
                       ", fundprojectcode " & _
                       ", fundprojectname " & _
                       ", fundprojectname1 " & _
                       ", fundprojectname2 " & _
                       ", fundsourceunkid " & _
                       ", currentceilingbalance " & _
                       ", fundprojectcodeexpirydate " & _
                       ", notify_amount " & _
                       ", audittype " & _
                       ", audituserunkid " & _
                       ", auditdatetime " & _
                       ", ip " & _
                       ", machine_name " & _
                       ", form_name " & _
                       " " & _
                       " " & _
                       " " & _
                       " " & _
                       " " & _
                       ", isweb" & _
                   ") VALUES (" & _
                       "  @fundprojectcodeunkid " & _
                       ", @fundprojectcode " & _
                       ", @fundprojectname " & _
                       ", @fundprojectname1 " & _
                       ", @fundprojectname2 " & _
                       ", @fundsourceunkid " & _
                       ", @currentceilingbalance " & _
                       ", @fundprojectcodeexpirydate " & _
                       ", @notify_amount " & _
                       ", @audittype " & _
                       ", @audituserunkid " & _
                       ", @auditdatetime " & _
                       ", @ip " & _
                       ", @machine_name " & _
                       ", @form_name " & _
                       " " & _
                       " " & _
                       " " & _
                       " " & _
                       " " & _
                       ", @isweb" & _
                   ") "

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@fundprojectcodeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintFundProjectcodeunkid)
            objDataOperation.AddParameter("@fundprojectcode", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrFundProjectCode.ToString)
            objDataOperation.AddParameter("@fundprojectname1", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrFundProjectName1.ToString)
            objDataOperation.AddParameter("@fundprojectname2", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrFundProjectName2.ToString)
            objDataOperation.AddParameter("@fundprojectname", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrFundProjectName.ToString)
            objDataOperation.AddParameter("@fundsourceunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintFundSourceunkid.ToString)
            objDataOperation.AddParameter("@currentceilingbalance", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecCurrentCeilingBalance.ToString)
            objDataOperation.AddParameter("@fundprojectcodeexpirydate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, IIf(mdtFundProjectCodeExpiryDate <> Nothing, mdtFundProjectCodeExpiryDate.ToString, DBNull.Value))
            objDataOperation.AddParameter("@notify_amount", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecNotify_amount.ToString)
            objDataOperation.AddParameter("@audittype", SqlDbType.Int, eZeeDataType.INT_SIZE, intAuditType)
            objDataOperation.AddParameter("@audituserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid)
            objDataOperation.AddParameter("@auditdatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, dtCurrentDateTime)
            objDataOperation.AddParameter("@ip", SqlDbType.NVarChar, eZeeDataType.IP_SIZE, mstrClientIP)
            objDataOperation.AddParameter("@machine_name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrHostName)
            objDataOperation.AddParameter("@form_name", SqlDbType.NVarChar, 500, mstrFormName)
            objDataOperation.AddParameter("@isweb", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsWeb)

            objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: InsertATFundProjectCodeMaster, Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
        End Try
        Return True
    End Function

	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Public Shared Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "This Project Code is already defined. Please define new Project Code.")
			Language.setMessage(mstrModuleName, 2, "This Project Name is already defined. Please define new Project Name.")
			Language.setMessage(mstrModuleName, 3, "WEB")
			Language.setMessage(mstrModuleName, 4, "Select")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class
