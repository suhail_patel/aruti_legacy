﻿'************************************************************************************************************************************
'Class Name : clsFundSource_Master.vb
'Purpose    : New Budget Module
'Date       : 20/05/2016
'Written By : Nilay
'Modified   :
'************************************************************************************************************************************

Imports eZeeCommonLib
Imports eZee.Common.eZeeForm
''' <summary>
''' Purpose: 
''' Developer: Nilay
''' </summary>
Public Class clsFundSource_Master
    Private Shared ReadOnly mstrModuleName As String = "clsFundSource_Master"
    Dim objDataOperation As clsDataOperation
    Dim mstrMessage As String = ""

#Region " Private variables "
    Private mintFundSourceunkid As Integer
    Private mstrFundCode As String = String.Empty
    Private mstrFundName As String = String.Empty
    'Nilay (26-Aug-2016) -- Start
    'For Other Language ADD Name1, Name2 
    Private mstrFundName1 As String = String.Empty
    Private mstrFundName2 As String = String.Empty
    'Nilay (26-Aug-2016) -- END
    Private mstrProject_Code As String = String.Empty
    Private mdecCurrentCeilingBalance As Decimal = 0
    Private mdtFundExpiryDate As DateTime
    Private mdecNotify_amount As Decimal
    Private mintUserunkid As Integer
    Private mblnIsvoid As Boolean
    Private mintVoiduserunkid As Integer
    Private mdtVoiddatetime As DateTime
    Private mstrVoidreason As String = String.Empty
    'Private mstrWebClientIP As String = String.Empty
    'Private mstrWebHostName As String = String.Empty
    'Private mstrWebFormName As String = String.Empty
    Private mintLoginEmployeeunkid As Integer = -1
#End Region

#Region " Properties "

    'S.SANDEEP [28-May-2018] -- START 
    'ISSUE/ENHANCEMENT : {Audit Trails} 
    Private mstrFormName As String = String.Empty
    Public WriteOnly Property _FormName() As String
        Set(ByVal value As String)
            mstrFormName = value
        End Set
    End Property

    Private mstrClientIP As String = ""
    Public WriteOnly Property _ClientIP() As String
        Set(ByVal value As String)
            mstrClientIP = value
        End Set
    End Property

    Private mstrHostName As String = ""
    Public WriteOnly Property _HostName() As String
        Set(ByVal value As String)
            mstrHostName = value
        End Set
    End Property

    Private mblnIsWeb As Boolean = False
    Public WriteOnly Property _FromWeb() As Boolean
        Set(ByVal value As Boolean)
            mblnIsWeb = value
        End Set
    End Property

    Private mintAuditUserId As Integer = 0
    Public WriteOnly Property _AuditUserId() As Integer
        Set(ByVal value As Integer)
            mintAuditUserId = value
        End Set
    End Property

    Private mintCompanyUnkid As Integer = 0
    Public WriteOnly Property _CompanyUnkid() As Integer
        Set(ByVal value As Integer)
            mintCompanyUnkid = value
        End Set
    End Property

    Private mdtAuditDate As DateTime = Now
    Public WriteOnly Property _AuditDate() As DateTime
        Set(ByVal value As DateTime)
            mdtAuditDate = value
        End Set
    End Property

    Private xDataOpr As clsDataOperation = Nothing
    Public Property _xDataOpr() As clsDataOperation
        Get
            Return xDataOpr
        End Get
        Set(ByVal value As clsDataOperation)
            xDataOpr = value
        End Set
    End Property

    Private mintVoidlogingemployeeunkid As Integer = 0
    Public Property _Voidloginemployeeunkid() As Integer
        Get
            Return mintVoidlogingemployeeunkid
        End Get
        Set(ByVal value As Integer)
            mintVoidlogingemployeeunkid = value
        End Set
    End Property
    'S.SANDEEP [28-May-2018] -- END 

    Public ReadOnly Property _Message() As String
        Get
            Return mstrMessage
        End Get
    End Property

    ''' <summary>
    ''' Purpose: Get or Set fundsourceunkid
    ''' Modify By: Nilay
    ''' </summary>
    Public Property _FundSourceunkid() As Integer
        Get
            Return mintFundSourceunkid
        End Get
        Set(ByVal value As Integer)
            mintFundSourceunkid = value
            Call GetData()
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set fundcode
    ''' Modify By: Nilay
    ''' </summary>
    Public Property _FundCode() As String
        Get
            Return mstrFundCode
        End Get
        Set(ByVal value As String)
            mstrFundCode = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set fundname
    ''' Modify By: Nilay
    ''' </summary>
    Public Property _FundName() As String
        Get
            Return mstrFundName
        End Get
        Set(ByVal value As String)
            mstrFundName = value
        End Set
    End Property

    'Nilay (26-Aug-2016) -- Start
    'For Other Language ADD Name1, Name2 
    ''' <summary>
    ''' Purpose: Get or Set fundname1
    ''' Modify By: Nilay
    ''' </summary>
    Public Property _FundName1() As String
        Get
            Return mstrFundName1
        End Get
        Set(ByVal value As String)
            mstrFundName1 = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set fundname2
    ''' Modify By: Nilay
    ''' </summary>
    Public Property _FundName2() As String
        Get
            Return mstrFundName2
        End Get
        Set(ByVal value As String)
            mstrFundName2 = value
        End Set
    End Property
    'Nilay (26-Aug-2016) -- END


    ''' <summary>
    ''' Purpose: Get or Set Project_Code
    ''' Modify By: Nilay
    ''' </summary>
    Public Property _Project_Code() As String
        Get
            Return mstrProject_Code
        End Get
        Set(ByVal value As String)
            mstrProject_Code = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set currentceilingbalance
    ''' Modify By: Nilay
    ''' </summary>
    Public Property _CurrentCeilingBalance() As Decimal
        Get
            Return mdecCurrentCeilingBalance
        End Get
        Set(ByVal value As Decimal)
            mdecCurrentCeilingBalance = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set fundexpirydate
    ''' Modify By: Nilay
    ''' </summary>
    Public Property _FundExpiryDate() As DateTime
        Get
            Return mdtFundExpiryDate
        End Get
        Set(ByVal value As DateTime)
            mdtFundExpiryDate = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set notify_amount
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Notify_Amount() As Decimal
        Get
            Return mdecNotify_amount
        End Get
        Set(ByVal value As Decimal)
            mdecNotify_amount = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set userunkid
    ''' Modify By: Nilay
    ''' </summary>
    Public Property _Userunkid() As Integer
        Get
            Return mintUserunkid
        End Get
        Set(ByVal value As Integer)
            mintUserunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isvoid
    ''' Modify By: Nilay
    ''' </summary>
    Public Property _Isvoid() As Boolean
        Get
            Return mblnIsvoid
        End Get
        Set(ByVal value As Boolean)
            mblnIsvoid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiduserunkid
    ''' Modify By: Nilay
    ''' </summary>
    Public Property _Voiduserunkid() As Integer
        Get
            Return mintVoiduserunkid
        End Get
        Set(ByVal value As Integer)
            mintVoiduserunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiddatetime
    ''' Modify By: Nilay
    ''' </summary>
    Public Property _Voiddatetime() As DateTime
        Get
            Return mdtVoiddatetime
        End Get
        Set(ByVal value As DateTime)
            mdtVoiddatetime = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voidreason
    ''' Modify By: Nilay
    ''' </summary>
    Public Property _Voidreason() As String
        Get
            Return mstrVoidreason
        End Get
        Set(ByVal value As String)
            mstrVoidreason = value
        End Set
    End Property

    'Public Property _WebClientIP() As String
    '    Get
    '        Return mstrWebClientIP
    '    End Get
    '    Set(ByVal value As String)
    '        mstrWebClientIP = value
    '    End Set
    'End Property

    'Public Property _WebHostName() As String
    '    Get
    '        Return mstrWebHostName
    '    End Get
    '    Set(ByVal value As String)
    '        mstrWebHostName = value
    '    End Set
    'End Property

    'Public Property _WebFormName() As String
    '    Get
    '        Return mstrWebFormName
    '    End Get
    '    Set(ByVal value As String)
    '        mstrWebFormName = value
    '    End Set
    'End Property

    ''' <summary>
    ''' Purpose: Get or Set loginemployeeunkid
    ''' Modify By: Nilay
    ''' </summary>
    Public Property _LoginEmployeeunkid() As Integer
        Get
            Return mintLoginEmployeeunkid
        End Get
        Set(ByVal value As Integer)
            mintLoginEmployeeunkid = value
        End Set
    End Property

#End Region

    ''' <summary>
    ''' Modify By: Nilay
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Sub GetData(Optional ByVal objDataOp As clsDataOperation = Nothing)
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        If objDataOp IsNot Nothing Then
            objDataOperation = objDataOp
        Else
            objDataOperation = New clsDataOperation
        End If

        Try
            strQ = " SELECT " & _
                        "  fundsourceunkid " & _
                        ", fundcode " & _
                        ", fundname " & _
                        ", fundname1 " & _
                        ", fundname2 " & _
                        ", project_code " & _
                        ", currentceilingbalance " & _
                        ", fundexpirydate " & _
                        ", notify_amount " & _
                        ", userunkid " & _
                        ", isvoid " & _
                        ", voiduserunkid " & _
                        ", voiddatetime " & _
                        ", voidreason " & _
                   " FROM bgfundsource_master " & _
                   " WHERE fundsourceunkid = @fundsourceunkid "
            'Nilay (26-Aug-2016) -- [fundname1,fundname2]

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@fundsourceunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintFundSourceunkid.ToString)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For Each dtRow As DataRow In dsList.Tables(0).Rows
                mintFundSourceunkid = CInt(dtRow.Item("fundsourceunkid"))
                mstrFundCode = dtRow.Item("fundcode").ToString
                mstrFundName = dtRow.Item("fundname").ToString
                'Nilay (26-Aug-2016) -- Start
                'For Other Language ADD Name1, Name2 
                mstrFundName1 = dtRow.Item("fundname1").ToString
                mstrFundName2 = dtRow.Item("fundname2").ToString
                'Nilay (26-Aug-2016) -- END
                mstrProject_Code = dtRow.Item("project_code").ToString
                mdecCurrentCeilingBalance = dtRow.Item("currentceilingbalance")
                mdtFundExpiryDate = dtRow.Item("fundexpirydate")
                mdecNotify_amount = CDec(dtRow.Item("notify_amount"))
                mintUserunkid = dtRow.Item("userunkid")
                mblnIsvoid = CBool(dtRow.Item("isvoid"))
                mintVoiduserunkid = CInt(dtRow.Item("voiduserunkid"))
                mdtVoiddatetime = IIf(IsDBNull(dtRow.Item("voiddatetime")), Nothing, dtRow.Item("voiddatetime"))
                mstrVoidreason = dtRow.Item("voidreason").ToString
                Exit For
            Next

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetData; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
        End Try
    End Sub


    ''' <summary>
    ''' Modify By: Nilay
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function GetList(ByVal strTableName As String, Optional ByVal strFilter As String = "") As DataSet
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = " SELECT " & _
                      "  fundsourceunkid " & _
                      ", fundcode " & _
                      ", fundname " & _
                      ", fundname1 " & _
                      ", fundname2 " & _
                      ", project_code " & _
                      ", currentceilingbalance " & _
                      ", fundexpirydate " & _
                      ", notify_amount " & _
                      ", userunkid " & _
                      ", isvoid " & _
                      ", voiduserunkid " & _
                      ", voiddatetime " & _
                      ", voidreason " & _
                   " FROM bgfundsource_master " & _
                   " WHERE isvoid = 0 "
            'Nilay (26-Aug-2016) -- [fundname1,fundname2]

            If strFilter.Trim.Length > 0 Then
                strQ &= " AND " & strFilter
            End If

            dsList = objDataOperation.ExecQuery(strQ, strTableName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
        Return dsList
    End Function

    ''' <summary>
    ''' Modify By: Nilay
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function GetComboList(Optional ByVal strTableName As String = "", Optional ByVal blnAddSelect As Boolean = True) As DataSet
        Dim strQ As String = String.Empty
        Dim objDataOperation As New clsDataOperation
        Dim dsList As New DataSet
        Dim exForce As Exception
        Try
            If blnAddSelect = True Then
                strQ = " SELECT " & _
                            "  0 AS fundsourceunkid " & _
                            ", '' AS fundcode " & _
                            ", ' ' + @select AS fundname " & _
                       " UNION "
                objDataOperation.AddParameter("@select", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 4, "Select"))
            End If

            strQ &= " SELECT " & _
                            "  fundsourceunkid AS fundsourceunkid " & _
                            ", fundcode AS fundcode " & _
                            ", fundname AS fundname " & _
                    " FROM bgfundsource_master " & _
                    " WHERE isvoid = 0 " & _
                    " ORDER BY fundname "

            dsList = objDataOperation.ExecQuery(strQ, strTableName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetComboList; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
        Return dsList
    End Function

    ''' <summary>
    ''' Modify By: Nilay
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> INSERT INTO Database Table (bgfundsource_master) </purpose>
    Public Function Insert(ByVal dtCurrentDateTime As DateTime) As Boolean

        If isExist(mstrFundCode) Then
            mstrMessage = Language.getMessage(mstrModuleName, 1, "This Code is already defined. Please define new Code.")
            Return False
        End If

        If isExist("", mstrFundName) Then
            mstrMessage = Language.getMessage(mstrModuleName, 2, "This Fund Name is already defined. Please define new Fund Name.")
            Return False
        End If

        'If isExist("", "", mstrProject_Code) Then
        '    mstrMessage = Language.getMessage(mstrModuleName, 3, "This Project Code is already defined. Please define new Project Code.")
        '    Return False
        'End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation
        objDataOperation.BindTransaction()

        Try
            objDataOperation.AddParameter("@fundcode", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrFundCode.ToString)
            objDataOperation.AddParameter("@fundname", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrFundName.ToString)
            'Nilay (26-Aug-2016) -- Start
            'For Other Language ADD Name1, Name2 
            objDataOperation.AddParameter("@fundname1", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrFundName1.ToString)
            objDataOperation.AddParameter("@fundname2", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrFundName2.ToString)
            'Nilay (26-Aug-2016) -- END
            objDataOperation.AddParameter("@project_code", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrProject_Code.ToString)
            objDataOperation.AddParameter("@currentceilingbalance", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecCurrentCeilingBalance.ToString)
            objDataOperation.AddParameter("@fundexpirydate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, IIf(mdtFundExpiryDate <> Nothing, mdtFundExpiryDate, DBNull.Value))
            objDataOperation.AddParameter("@notify_amount", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecNotify_amount.ToString)
            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
            objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, IIf(mdtVoiddatetime <> Nothing, mdtVoiddatetime, DBNull.Value))
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)

            strQ = "INSERT INTO bgfundsource_master ( " & _
                       "  fundcode " & _
                       ", fundname " & _
                       ", fundname1 " & _
                       ", fundname2 " & _
                       ", project_code " & _
                       ", currentceilingbalance " & _
                       ", fundexpirydate " & _
                       ", notify_amount " & _
                       ", userunkid " & _
                       ", isvoid " & _
                       ", voiduserunkid " & _
                       ", voiddatetime " & _
                       ", voidreason" & _
                   ") VALUES (" & _
                       "  @fundcode " & _
                       ", @fundname " & _
                       ", @fundname1 " & _
                       ", @fundname2 " & _
                       ", @project_code " & _
                       ", @currentceilingbalance " & _
                       ", @fundexpirydate " & _
                       ", @notify_amount " & _
                       ", @userunkid " & _
                       ", @isvoid " & _
                       ", @voiduserunkid " & _
                       ", @voiddatetime " & _
                       ", @voidreason" & _
                   "); SELECT @@identity"
            'Nilay (26-Aug-2016) -- [fundname1,fundname2]
            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            mintFundSourceunkid = dsList.Tables(0).Rows(0).Item(0)

            If InsertATFundSourceMaster(objDataOperation, enAuditType.ADD, dtCurrentDateTime) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            objDataOperation.ReleaseTransaction(True)
            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Insert; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Nilay
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Update Database Table (bgfundsource_master) </purpose>
    Public Function Update(ByVal dtCurrentDateTime As DateTime, Optional ByVal objDataOp As clsDataOperation = Nothing) As Boolean

        If isExist(mstrFundCode, "", "", mintFundSourceunkid) Then
            mstrMessage = Language.getMessage(mstrModuleName, 1, "This Code is already defined. Please define new Code.")
            Return False
        End If

        If isExist("", mstrFundName, "", mintFundSourceunkid) Then
            mstrMessage = Language.getMessage(mstrModuleName, 2, "This Fund Name is already defined. Please define new Fund Name.")
            Return False
        End If

        'If isExist("", "", mstrProject_Code, mintFundSourceunkid) Then
        '    mstrMessage = Language.getMessage(mstrModuleName, 2, "This Project Code is already defined. Please define new Project Code.")
        '    Return False
        'End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        If objDataOp IsNot Nothing Then
            objDataOperation = objDataOp
        Else
        objDataOperation = New clsDataOperation
        objDataOperation.BindTransaction()
        End If

        Try
            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@fundsourceunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintFundSourceunkid.ToString)
            objDataOperation.AddParameter("@fundcode", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrFundCode.ToString)
            objDataOperation.AddParameter("@fundname", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrFundName.ToString)
            'Nilay (26-Aug-2016) -- Start
            'For Other Language ADD Name1, Name2 
            objDataOperation.AddParameter("@fundname1", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrFundName1.ToString)
            objDataOperation.AddParameter("@fundname2", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrFundName2.ToString)
            'Nilay (26-Aug-2016) -- END
            objDataOperation.AddParameter("@project_code", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrProject_Code.ToString)
            objDataOperation.AddParameter("@currentceilingbalance", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecCurrentCeilingBalance.ToString)
            objDataOperation.AddParameter("@notify_amount", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecNotify_amount.ToString)
            objDataOperation.AddParameter("@fundexpirydate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, IIf(mdtFundExpiryDate <> Nothing, mdtFundExpiryDate, DBNull.Value))
            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
            objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, IIf(mdtVoiddatetime <> Nothing, mdtVoiddatetime, DBNull.Value))
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)

            strQ = "UPDATE bgfundsource_master SET " & _
                       "  fundcode = @fundcode" & _
                       ", fundname = @fundname" & _
                       ", fundname1 = @fundname1 " & _
                       ", fundname2 = @fundname2 " & _
                       ", project_code = @project_code" & _
                       ", currentceilingbalance = @currentceilingbalance" & _
                       ", fundexpirydate = @fundexpirydate" & _
                       ", notify_amount = @notify_amount" & _
                       ", userunkid = @userunkid " & _
                       ", isvoid = @isvoid" & _
                       ", voiduserunkid = @voiduserunkid" & _
                       ", voiddatetime = @voiddatetime" & _
                       ", voidreason = @voidreason " & _
                   "WHERE fundsourceunkid = @fundsourceunkid "
            'Nilay (26-Aug-2016) -- [fundname1,fundname2]

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            'S.SANDEEP [28-May-2018] -- START
            'ISSUE/ENHANCEMENT : {Audit Trails} 
            Dim objCommonATLog As New clsCommonATLog
            objCommonATLog._FormName = mstrFormName
            objCommonATLog._LoginEmployeeUnkid = mintLoginEmployeeunkid
            objCommonATLog._ClientIP = mstrClientIP
            objCommonATLog._HostName = mstrHostName
            objCommonATLog._FromWeb = mblnIsWeb
            objCommonATLog._AuditUserId = mintAuditUserId
objCommonATLog._CompanyUnkid = mintCompanyUnkid
            objCommonATLog._AuditDate = mdtAuditDate
            'S.SANDEEP [28-May-2018] -- END

            If objCommonATLog.IsTableDataUpdateExists("atbgfundsource_master", "bgfundsource_master", mintFundSourceunkid, _
                                                      "fundsourceunkid", 2, objDataOperation) Then
                If InsertATFundSourceMaster(objDataOperation, enAuditType.EDIT, dtCurrentDateTime) = False Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If
            End If

            If objDataOp Is Nothing Then objDataOperation.ReleaseTransaction(True)
            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If objDataOp Is Nothing Then objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Nilay
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Delete Database Table (bgfundsource_master) </purpose>
    Public Function Delete(ByVal intFundSourceunkid As Integer, ByVal dtCurrentDateTime As DateTime) As Boolean

        'If isUsed(intFundSourceunkid) Then
        '    mstrMessage = Language.getMessage(mstrModuleName, 4, "Sorry, You cannot Delete this Fund Source. Reason: This Fund Source is in use.")
        '    Return False
        'End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation
        objDataOperation.BindTransaction()

        Try
            strQ = "UPDATE bgfundsource_master SET " & _
                        "  isvoid = @isvoid " & _
                        ", voiduserunkid = @voiduserunkid " & _
                        ", voiddatetime = @voiddatetime " & _
                        ", voidreason = @voidreason " & _
                   "WHERE fundsourceunkid = @fundsourceunkid "

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@fundsourceunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intFundSourceunkid)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
            objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, IIf(mdtVoiddatetime <> Nothing, mdtVoiddatetime, DBNull.Value))
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            mintFundSourceunkid = intFundSourceunkid
            Call GetData(objDataOperation)

            If InsertATFundSourceMaster(objDataOperation, enAuditType.DELETE, dtCurrentDateTime) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            objDataOperation.ReleaseTransaction(True)
            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Delete; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Nilay
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isUsed(ByVal intUnkid As Integer) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim blnIsUsed As Boolean = False

        objDataOperation = New clsDataOperation

        Try
            strQ = "SELECT " & _
                   "       'SELECT 1 FROM ' + INFORMATION_SCHEMA.TABLES.TABLE_NAME + ' WHERE ' + COLUMN_NAME + ' = @fundsourceunkid AND isvoid = 0 ' AS SQL_QRY " & _
                   " FROM INFORMATION_SCHEMA.TABLES " & _
                   "       JOIN INFORMATION_SCHEMA.COLUMNS ON INFORMATION_SCHEMA.TABLES.TABLE_NAME = INFORMATION_SCHEMA.COLUMNS.TABLE_NAME " & _
                   " WHERE TABLES.TABLE_NAME like 'bg%' AND TABLES.TABLE_NAME NOT IN('bgfundsource_master') " & _
                   "       AND COLUMN_NAME IN ('fundsourceunkid') "

            objDataOperation.AddParameter("@fundsourceunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            strQ = ""

            For Each dRow As DataRow In dsList.Tables("List").Rows

                strQ = dRow.Item("SQL_QRY").ToString.Replace("@fundsourceunkid", intUnkid)
                dsList = objDataOperation.ExecQuery(strQ, "Used")

                If dsList.Tables("Used").Rows.Count > 0 Then
                    blnIsUsed = True
                    Exit For
                End If
            Next

            Return blnIsUsed

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isUsed; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Nilay
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isExist(Optional ByVal strCode As String = "", Optional ByVal strName As String = "", Optional ByVal strProjectCode As String = "", Optional ByVal intUnkid As Integer = -1) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        Dim objDataOperation As New clsDataOperation

        Try
            strQ = "SELECT " & _
                       "  fundsourceunkid " & _
                       ", fundcode " & _
                       ", fundname " & _
                       ", project_code " & _
                       ", currentceilingbalance " & _
                       ", fundexpirydate " & _
                       ", notify_amount " & _
                       ", userunkid " & _
                       ", isvoid " & _
                       ", voiduserunkid " & _
                       ", voiddatetime " & _
                       ", voidreason " & _
                   "FROM bgfundsource_master " & _
                   "WHERE isvoid = 0 "

            If strCode.Trim.Length > 0 Then
                strQ &= " AND fundcode = @fundcode "
            End If

            If strName.Trim.Length > 0 Then
                strQ &= " AND fundname = @fundname "
            End If

            If strProjectCode.Trim.Length > 0 Then
                strQ &= " AND project_code = @project_code "
            End If

            If intUnkid > 0 Then
                strQ &= " AND fundsourceunkid <> @fundsourceunkid"
            End If

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@fundcode", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strCode)
            objDataOperation.AddParameter("@fundname", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strName)
            objDataOperation.AddParameter("@project_code", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strProjectCode)
            objDataOperation.AddParameter("@fundsourceunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isExist; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Nilay
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function InsertATFundSourceMaster(ByVal objDataOperation As clsDataOperation, ByVal intAuditType As enAuditType, _
                                             ByVal dtCurrentDateTime As DateTime) As Boolean

        Dim strQ As String = ""
        Dim exForce As Exception

        Try
            strQ = "INSERT INTO atbgfundsource_master ( " & _
                       "  fundsourceunkid " & _
                       ", fundcode " & _
                       ", fundname " & _
                       ", fundname1 " & _
                       ", fundname2 " & _
                       ", project_code " & _
                       ", currentceilingbalance " & _
                       ", fundexpirydate " & _
                       ", notify_amount " & _
                       ", audittype " & _
                       ", audituserunkid " & _
                       ", auditdatetime " & _
                       ", ip " & _
                       ", machine_name " & _
                       ", form_name " & _
                       " " & _
                       " " & _
                       " " & _
                       " " & _
                       " " & _
                       ", isweb" & _
                       ", loginemployeeunkid " & _
                   ") VALUES (" & _
                       "  @fundsourceunkid " & _
                       ", @fundcode " & _
                       ", @fundname " & _
                       ", @fundname1 " & _
                       ", @fundname2 " & _
                       ", @project_code " & _
                       ", @currentceilingbalance " & _
                       ", @fundexpirydate " & _
                       ", @notify_amount " & _
                       ", @audittype " & _
                       ", @audituserunkid " & _
                       ", @auditdatetime " & _
                       ", @ip " & _
                       ", @machine_name " & _
                       ", @form_name " & _
                       " " & _
                       " " & _
                       " " & _
                       " " & _
                       " " & _
                       ", @isweb" & _
                       ", @loginemployeeunkid " & _
                   ") "
            'Nilay (26-Aug-2016) -- [fundname1,fundname2]

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@fundsourceunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintFundSourceunkid)
            objDataOperation.AddParameter("@fundcode", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrFundCode.ToString)
            objDataOperation.AddParameter("@fundname", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrFundName.ToString)
            'Nilay (26-Aug-2016) -- Start
            'For Other Language ADD Name1, Name2 
            objDataOperation.AddParameter("@fundname1", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrFundName1.ToString)
            objDataOperation.AddParameter("@fundname2", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrFundName2.ToString)
            'Nilay (26-Aug-2016) -- END
            objDataOperation.AddParameter("@project_code", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrProject_Code.ToString)
            objDataOperation.AddParameter("@currentceilingbalance", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecCurrentCeilingBalance.ToString)
            objDataOperation.AddParameter("@fundexpirydate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, IIf(mdtFundExpiryDate <> Nothing, mdtFundExpiryDate.ToString, DBNull.Value))
            objDataOperation.AddParameter("@notify_amount", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecNotify_amount.ToString)
            objDataOperation.AddParameter("@audittype", SqlDbType.Int, eZeeDataType.INT_SIZE, intAuditType)
            objDataOperation.AddParameter("@audituserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAuditUserId)
            objDataOperation.AddParameter("@auditdatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtAuditDate)
            objDataOperation.AddParameter("@ip", SqlDbType.NVarChar, eZeeDataType.IP_SIZE, mstrClientIP)
            objDataOperation.AddParameter("@machine_name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrHostName)
            objDataOperation.AddParameter("@form_name", SqlDbType.NVarChar, 500, mstrFormName)
            objDataOperation.AddParameter("@isweb", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsWeb)
            objDataOperation.AddParameter("@loginemployeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLoginEmployeeunkid.ToString)

            objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: InsertATFundSourceMaster, Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
        End Try
        Return True
    End Function

	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Public Shared Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "This Code is already defined. Please define new Code.")
			Language.setMessage(mstrModuleName, 2, "This Fund Name is already defined. Please define new Fund Name.")
			Language.setMessage(mstrModuleName, 3, "WEB")
			Language.setMessage(mstrModuleName, 4, "Select")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class