﻿'************************************************************************************************************************************
'Class Name : clsbudgetapproverlevel_master.vb
'Purpose    :
'Date       :8/8/2016
'Written By :Pinkal
'Modified   :
'************************************************************************************************************************************

Imports eZeeCommonLib
Imports eZee.Common.eZeeForm

''' <summary>
''' Purpose: 
''' Developer: Pinkal
''' </summary>
Public Class clsbudgetapproverlevel_master
    Private Shared ReadOnly mstrModuleName As String = "clsbudgetapproverlevel_master"
    Dim objDataOperation As clsDataOperation
    Dim mstrMessage As String = ""

#Region " Private variables "
    Private mintLevelunkid As Integer
    Private mstrLevelcode As String = String.Empty
    Private mstrLevelname As String = String.Empty
    Private mstrLevelname1 As String = String.Empty
    Private mstrLevelname2 As String = String.Empty
    Private mintPriority As Integer = 1
    Private mintUserunkid As Integer
    Private mblnIsvoid As Boolean
    Private mintVoiduserunkid As Integer
    Private mdtVoiddatetime As Date
    Private mstrVoidreason As String = String.Empty
    'Private mstrWebFormName As String = String.Empty
    'Private mstrWebClientIP As String = String.Empty
    'Private mstrWebHostName As String = String.Empty

#End Region

#Region " Properties "

'S.SANDEEP [28-May-2018] -- START 
'ISSUE/ENHANCEMENT : {Audit Trails} 
Private mstrFormName As String = String.Empty
Public WriteOnly Property _FormName() As String 
Set(ByVal value As String) 
mstrFormName = value 
End Set 
End Property 
 
Private mstrClientIP As String = ""
Public WriteOnly Property _ClientIP() As String 
Set(ByVal value As String) 
mstrClientIP = value 
End Set 
End Property 
 
Private mstrHostName As String = ""
Public WriteOnly Property _HostName() As String 
Set(ByVal value As String) 
mstrHostName = value 
End Set 
End Property 
 
Private mblnIsWeb As Boolean = False
Public WriteOnly Property _FromWeb() As Boolean 
Set(ByVal value As Boolean) 
mblnIsWeb = value 
End Set 
End Property 
 
    Private mintAuditUserId As Integer = 0
    Public WriteOnly Property _AuditUserId() As Integer
        Set(ByVal value As Integer)
            mintAuditUserId = value
        End Set
    End Property

    Private mintCompanyUnkid As Integer = 0
    Public WriteOnly Property _CompanyUnkid() As Integer
        Set(ByVal value As Integer)
            mintCompanyUnkid = value
        End Set
    End Property
 
Private mdtAuditDate As DateTime = Now
Public WriteOnly Property _AuditDate() As DateTime 
Set(ByVal value As DateTime) 
mdtAuditDate = value 
End Set 
End Property 
 
Private xDataOpr As clsDataOperation = Nothing
Public Property _xDataOpr() As clsDataOperation 
Get 
Return xDataOpr 
End Get 
Set(ByVal value As clsDataOperation) 
xDataOpr = value 
End Set 
End Property 
 
Private mintLoginEmployeeunkid As Integer = 0
Public Property _LoginEmployeeunkid() As Integer 
Get 
Return mintLoginEmployeeunkid 
End Get 
Set(ByVal value As Integer) 
mintLoginEmployeeunkid = value 
End Set 
End Property 
 
Private mintVoidlogingemployeeunkid As Integer=0
Public Property _Voidloginemployeeunkid() As Integer 
Get 
Return mintVoidlogingemployeeunkid 
End Get 
Set(ByVal value As Integer) 
mintVoidlogingemployeeunkid = value 
End Set 
End Property 
'S.SANDEEP [28-May-2018] -- END 

    Public ReadOnly Property _Message() As String
        Get
            Return mstrMessage
        End Get
    End Property

    ''' <summary>
    ''' Purpose: Get or Set levelunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Levelunkid() As Integer
        Get
            Return mintLevelunkid
        End Get
        Set(ByVal value As Integer)
            mintLevelunkid = Value
            Call getData()
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set levelcode
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Levelcode() As String
        Get
            Return mstrLevelcode
        End Get
        Set(ByVal value As String)
            mstrLevelcode = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set levelname
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Levelname() As String
        Get
            Return mstrLevelname
        End Get
        Set(ByVal value As String)
            mstrLevelname = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set levelname1
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Levelname1() As String
        Get
            Return mstrLevelname1
        End Get
        Set(ByVal value As String)
            mstrLevelname1 = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set levelname2
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Levelname2() As String
        Get
            Return mstrLevelname2
        End Get
        Set(ByVal value As String)
            mstrLevelname2 = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set priority
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Priority() As Integer
        Get
            Return mintPriority
        End Get
        Set(ByVal value As Integer)
            mintPriority = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set userunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Userunkid() As Integer
        Get
            Return mintUserunkid
        End Get
        Set(ByVal value As Integer)
            mintUserunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isvoid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Isvoid() As Boolean
        Get
            Return mblnIsvoid
        End Get
        Set(ByVal value As Boolean)
            mblnIsvoid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiduserunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Voiduserunkid() As Integer
        Get
            Return mintVoiduserunkid
        End Get
        Set(ByVal value As Integer)
            mintVoiduserunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiddatetime
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Voiddatetime() As Date
        Get
            Return mdtVoiddatetime
        End Get
        Set(ByVal value As Date)
            mdtVoiddatetime = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voidreason
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Voidreason() As String
        Get
            Return mstrVoidreason
        End Get
        Set(ByVal value As String)
            mstrVoidreason = Value
        End Set
    End Property

#End Region

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Sub GetData()
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "SELECT " & _
                      "  levelunkid " & _
                      ", levelcode " & _
                      ", levelname " & _
                      ", levelname1 " & _
                      ", levelname2 " & _
                      ", priority " & _
                      ", userunkid " & _
                      ", isvoid " & _
                      ", voiduserunkid " & _
                      ", voiddatetime " & _
                      ", voidreason " & _
                     "FROM bgbudgetapproverlevel_master " & _
                     "WHERE levelunkid = @levelunkid "

            objDataOperation.AddParameter("@levelunkid", SqlDbType.int, eZeeDataType.INT_SIZE, mintLevelUnkId.ToString)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For Each dtRow As DataRow In dsList.Tables(0).Rows
                mintlevelunkid = CInt(dtRow.Item("levelunkid"))
                mstrlevelcode = dtRow.Item("levelcode").ToString
                mstrlevelname = dtRow.Item("levelname").ToString
                mstrlevelname1 = dtRow.Item("levelname1").ToString
                mstrlevelname2 = dtRow.Item("levelname2").ToString
                mintpriority = CInt(dtRow.Item("priority"))
                mintuserunkid = CInt(dtRow.Item("userunkid"))
                mblnisvoid = CBool(dtRow.Item("isvoid"))
                mintvoiduserunkid = CInt(dtRow.Item("voiduserunkid"))
                If Not IsDBNull(dtRow.Item("voiddatetime")) Then mdtVoiddatetime = dtRow.Item("voiddatetime")
                mstrvoidreason = dtRow.Item("voidreason").ToString
                Exit For
            Next
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetData; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Sub

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function GetList(ByVal strTableName As String, Optional ByVal blnOnlyActive As Boolean = True) As DataSet
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "SELECT " & _
                      "  levelunkid " & _
                      ", levelcode " & _
                      ", levelname " & _
                      ", levelname1 " & _
                      ", levelname2 " & _
                      ", priority " & _
                      ", userunkid " & _
                      ", isvoid " & _
                      ", voiduserunkid " & _
                      ", voiddatetime " & _
                      ", voidreason " & _
                     "FROM bgbudgetapproverlevel_master "

            If blnOnlyActive Then
                strQ &= " WHERE isvoid = 0 "
            End If

            dsList = objDataOperation.ExecQuery(strQ, strTableName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
        Return dsList
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> INSERT INTO Database Table (bgbudgetapproverlevel_master) </purpose>
    Public Function Insert() As Boolean
        If isExist(mstrLevelcode, "") Then
            mstrMessage = Language.getMessage(mstrModuleName, 1, "This Approver Level Code is already defined. Please define new Approver Level Code.")
            Return False
        ElseIf isExist("", mstrLevelname) Then
            mstrMessage = Language.getMessage(mstrModuleName, 2, "This Approver Level Name is already defined. Please define new Approver Level Name.")
            Return False
        ElseIf isPriorityExist(mintPriority) Then
            mstrMessage = Language.getMessage(mstrModuleName, 3, "This Approver Level Priority is already assign. Please assign new Approver Level Priority.")
            Return False
        End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation
        objDataOperation.BindTransaction()

        Try
            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@levelcode", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrLevelcode.ToString)
            objDataOperation.AddParameter("@levelname", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrLevelname.ToString)
            objDataOperation.AddParameter("@levelname1", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrLevelname1.ToString)
            objDataOperation.AddParameter("@levelname2", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrLevelname2.ToString)
            objDataOperation.AddParameter("@priority", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPriority.ToString)
            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
            objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, IIf(mdtVoiddatetime <> Nothing, mdtVoiddatetime, DBNull.Value))
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)

            strQ = "INSERT INTO bgbudgetapproverlevel_master ( " & _
                      "  levelcode " & _
                      ", levelname " & _
                      ", levelname1 " & _
                      ", levelname2 " & _
                      ", priority " & _
                      ", userunkid " & _
                      ", isvoid " & _
                      ", voiduserunkid " & _
                      ", voiddatetime " & _
                      ", voidreason" & _
                    ") VALUES (" & _
                      "  @levelcode " & _
                      ", @levelname " & _
                      ", @levelname1 " & _
                      ", @levelname2 " & _
                      ", @priority " & _
                      ", @userunkid " & _
                      ", @isvoid " & _
                      ", @voiduserunkid " & _
                      ", @voiddatetime " & _
                      ", @voidreason" & _
                    "); SELECT @@identity"

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            mintLevelunkid = dsList.Tables(0).Rows(0).Item(0)

            If ATInsertApproverLevel_Master(objDataOperation, 1) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            objDataOperation.ReleaseTransaction(True)

            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Insert; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Update Database Table (bgbudgetapproverlevel_master) </purpose>
    Public Function Update() As Boolean
        If isExist(mstrLevelcode, "", mintLevelunkid) Then
            mstrMessage = Language.getMessage(mstrModuleName, 1, "This Approver Level Code is already defined. Please define new Approver Level Code.")
            Return False
        ElseIf isExist("", mstrLevelname, mintLevelunkid) Then
            mstrMessage = Language.getMessage(mstrModuleName, 2, "This Approver Level Name is already defined. Please define new Approver Level Name.")
            Return False
        ElseIf isPriorityExist(mintPriority, mintLevelunkid) Then
            mstrMessage = Language.getMessage(mstrModuleName, 3, "This Approver Level Priority is already assign. Please assign new Approver Level Priority.")
            Return False
        End If


        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation
        objDataOperation.BindTransaction()

        Try
            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@levelunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLevelunkid.ToString)
            objDataOperation.AddParameter("@levelcode", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrLevelcode.ToString)
            objDataOperation.AddParameter("@levelname", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrLevelname.ToString)
            objDataOperation.AddParameter("@levelname1", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrLevelname1.ToString)
            objDataOperation.AddParameter("@levelname2", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrLevelname2.ToString)
            objDataOperation.AddParameter("@priority", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPriority.ToString)
            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
            objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, IIf(mdtVoiddatetime <> Nothing, mdtVoiddatetime, DBNull.Value))
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)

            strQ = "UPDATE bgbudgetapproverlevel_master SET " & _
                      "  levelcode = @levelcode" & _
                      ", levelname = @levelname" & _
                      ", levelname1 = @levelname1" & _
                      ", levelname2 = @levelname2" & _
                      ", priority = @priority" & _
                      ", userunkid = @userunkid" & _
                      ", isvoid = @isvoid" & _
                      ", voiduserunkid = @voiduserunkid" & _
                      ", voiddatetime = @voiddatetime" & _
                      ", voidreason = @voidreason " & _
                    "WHERE levelunkid = @levelunkid AND isvoid = 0 "

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If ATInsertApproverLevel_Master(objDataOperation, 2) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            objDataOperation.ReleaseTransaction(True)

            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Delete Database Table (bgbudgetapproverlevel_master) </purpose>
    Public Function Delete(ByVal intUnkid As Integer) As Boolean

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation
        objDataOperation.BindTransaction()

        Try

            strQ = "UPDATE bgbudgetapproverlevel_master SET " & _
                      " isvoid = @isvoid" & _
                      ", voiduserunkid = @voiduserunkid" & _
                      ", voiddatetime = GetDate()" & _
                      ", voidreason = @voidreason " & _
                      " WHERE levelunkid = @levelunkid AND isvoid = 0 "

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, True)
            objDataOperation.AddParameter("@levelunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid)
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, mstrVoidreason)
            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If ATInsertApproverLevel_Master(objDataOperation, 3) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            objDataOperation.ReleaseTransaction(True)

            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Delete; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isUsed(ByVal intUnkid As Integer) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            StrQ = "<Query>"

            objDataOperation.AddParameter("@levelunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.tables(0).rows.count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isUsed; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isExist(ByVal strCode As String, ByVal strName As String, Optional ByVal intUnkid As Integer = -1) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "SELECT " & _
                      "  levelunkid " & _
                      ", levelcode " & _
                      ", levelname " & _
                      ", levelname1 " & _
                      ", levelname2 " & _
                      ", priority " & _
                      ", userunkid " & _
                      ", isvoid " & _
                      ", voiduserunkid " & _
                      ", voiddatetime " & _
                      ", voidreason " & _
                     " FROM bgbudgetapproverlevel_master " & _
                     " WHERE levelname = @name " & _
                     " AND levelcode = @code AND isvoid = 0 "

            If intUnkid > 0 Then
                strQ &= " AND levelunkid <> @levelunkid"
            End If

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@code", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strCode)
            objDataOperation.AddParameter("@name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strName)
            objDataOperation.AddParameter("@levelunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.tables(0).rows.count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isExist; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isPriorityExist(ByVal mintPriority As Integer, Optional ByVal intUnkid As Integer = -1) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "SELECT " & _
                      "  levelunkid " & _
                      ", levelcode " & _
                      ", levelname " & _
                      ", levelname1 " & _
                      ", levelname2 " & _
                      ", priority " & _
                      ", userunkid " & _
                      ", isvoid " & _
                      ", voiduserunkid " & _
                      ", voiddatetime " & _
                      ", voidreason " & _
                     " FROM bgbudgetapproverlevel_master " & _
                     " WHERE priority = @priority AND isvoid = 0 "

            If intUnkid > 0 Then
                strQ &= " AND levelunkid <> @levelunkid"
            End If

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@priority", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPriority)
            objDataOperation.AddParameter("@levelunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isPriorityExist; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function getListForCombo(Optional ByVal strListName As String = "List", Optional ByVal mblFlag As Boolean = False, Optional ByVal blnOnlyActive As Boolean = False) As DataSet
        Dim dsList As New DataSet
        Dim objDataOperation As New clsDataOperation
        Dim strQ As String = String.Empty
        Dim exForce As Exception
        Try
            If mblFlag = True Then
                strQ = "SELECT 0 as levelunkid, ' ' +  @name  as name UNION "
            End If
            strQ &= "SELECT levelunkid, levelname as name FROM bgbudgetapproverlevel_master where isvoid = 0 "

            If blnOnlyActive = True Then
                strQ &= " AND isvoid = 0 "
            End If

            strQ &= " ORDER BY name "

            objDataOperation.AddParameter("@name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 4, "Select"))
            dsList = objDataOperation.ExecQuery(strQ, strListName)
            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If
            Return dsList
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "getListForCombo", mstrModuleName)
            Return Nothing
        Finally
            exForce = Nothing
            objDataOperation = Nothing
            dsList.Dispose()
            dsList = Nothing
        End Try

    End Function

    Public Function GetLowerLevelPriority(ByVal intCurrentPriority As Integer _
                                      ) As Integer
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation
        Try

            strQ = "SELECT  ISNULL(MAX(priority), -1) AS LowerLevelPriority " & _
                    "FROM    bgbudget_approver_mapping " & _
                            "LEFT JOIN bgbudgetapproverlevel_master ON bgbudget_approver_mapping.levelunkid = bgbudgetapproverlevel_master.levelunkid " & _
                    "WHERE   bgbudget_approver_mapping.isvoid = 0 " & _
                            "AND bgbudgetapproverlevel_master.isvoid = 0 " & _
                            "AND priority < @priority "

            strQ &= " ORDER BY LowerLevelPriority DESC"

            objDataOperation.AddParameter("@priority", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, intCurrentPriority)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If dsList.Tables(0).Rows.Count > 0 Then
                Return CInt(dsList.Tables(0).Rows(0).Item("LowerLevelPriority"))
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetLowerLevelPriority", mstrModuleName)
        End Try
        Return -1
    End Function

    Public Function GetNextLevelPriority(ByVal intCurrentPriority As Integer _
                                         ) As Integer
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation
        Try
            strQ = "SELECT  ISNULL(MIN(priority), -1) AS NextLevelPriority " & _
                    "FROM    bgbudget_approver_mapping " & _
                            "LEFT JOIN bgbudgetapproverlevel_master ON bgbudget_approver_mapping.levelunkid = bgbudgetapproverlevel_master.levelunkid " & _
                    "WHERE   bgbudget_approver_mapping.isvoid = 0 " & _
                            "AND bgbudgetapproverlevel_master.isvoid = 0 " & _
                            "AND priority > @priority "

            objDataOperation.AddParameter("@priority", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, intCurrentPriority)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If dsList.Tables(0).Rows.Count > 0 Then
                Return CInt(dsList.Tables(0).Rows(0).Item("NextLevelPriority"))
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetNextLevelPriority", mstrModuleName)
        End Try
        Return -1
    End Function

    Public Function GetMaxPriority() As Integer
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation
        Try
            strQ = "SELECT  ISNULL(MAX(priority), -1) AS MaxPriority " & _
                    "FROM    bgbudget_approver_mapping " & _
                            "LEFT JOIN bgbudgetapproverlevel_master ON bgbudget_approver_mapping.levelunkid = bgbudgetapproverlevel_master.levelunkid " & _
                    "WHERE   bgbudget_approver_mapping.isvoid = 0 " & _
                            "AND bgbudgetapproverlevel_master.isvoid = 0 "

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If dsList.Tables(0).Rows.Count > 0 Then
                Return CInt(dsList.Tables(0).Rows(0).Item("MaxPriority"))
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetMaxPriority", mstrModuleName)
        End Try
        Return -1
    End Function

    Public Function GetMinPriority() As Integer
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation
        Try
            strQ = "SELECT  ISNULL(MIN(priority), -1) AS MinPriority " & _
                    "FROM    bgbudget_approver_mapping " & _
                            "LEFT JOIN bgbudgetapproverlevel_master ON bgbudget_approver_mapping.levelunkid = bgbudgetapproverlevel_master.levelunkid " & _
                    "WHERE   bgbudget_approver_mapping.isvoid = 0 " & _
                            "AND bgbudgetapproverlevel_master.isvoid = 0 "

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If dsList.Tables(0).Rows.Count > 0 Then
                Return CInt(dsList.Tables(0).Rows(0).Item("MinPriority"))
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetMinPriority", mstrModuleName)
        End Try
        Return -1
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Private Function ATInsertApproverLevel_Master(ByVal xDataOpr As clsDataOperation, ByVal xAuditType As enAuditType) As Boolean
        Dim strQ As String = ""
        Dim strErrorMessage As String = ""
        Dim exForce As Exception = Nothing
        Try
            strQ = "INSERT INTO atbgbudgetapproverlevel_master ( " & _
                              "  levelunkid " & _
                              ", levelcode " & _
                              ", levelname " & _
                              ", levelname1 " & _
                              ", levelname2 " & _
                              ", priority " & _
                             ", audittype " & _
                             ", audituserunkid " & _
                             ", auditdatetime " & _
                             ", ip " & _
                             ", machine_name " & _
                             ", form_name " & _
                             " " & _
                             " " & _
                             " " & _
                             " " & _
                             " " & _
                             ", isweb " & _
                         ") VALUES (" & _
                              " @levelunkid " & _
                             ", @levelcode " & _
                             ", @levelname " & _
                             ", @levelname1 " & _
                             ", @levelname2 " & _
                             ", @priority " & _
                             ", @audittype " & _
                             ", @audituserunkid " & _
                             ", GetDate() " & _
                             ", @ip " & _
                             ", @machine_name " & _
                             ", @form_name " & _
                             " " & _
                             " " & _
                             " " & _
                             " " & _
                             " " & _
                             ", @isweb " & _
                         ") "


            xDataOpr.ClearParameters()
            xDataOpr.AddParameter("@levelunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLevelunkid.ToString)
            xDataOpr.AddParameter("@levelcode", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrLevelcode.ToString)
            xDataOpr.AddParameter("@levelname", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrLevelname.ToString)
            xDataOpr.AddParameter("@levelname1", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrLevelname1.ToString)
            xDataOpr.AddParameter("@levelname2", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrLevelname2.ToString)
            xDataOpr.AddParameter("@priority", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPriority.ToString)
            xDataOpr.AddParameter("@audittype", SqlDbType.Int, eZeeDataType.INT_SIZE, xAuditType)
            xDataOpr.AddParameter("@audituserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAuditUserId)
            xDataOpr.AddParameter("@ip", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrClientIP)
            xDataOpr.AddParameter("@machine_name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrHostName)
            xDataOpr.AddParameter("@form_name", SqlDbType.NVarChar, 500, mstrFormName)
            xDataOpr.AddParameter("@isweb", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsWeb)

            xDataOpr.ExecNonQuery(strQ)

            If xDataOpr.ErrorMessage <> "" Then
                exForce = New Exception(xDataOpr.ErrorNumber & ": " & xDataOpr.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: ATInsertApproverLevel_Master; Module Name: " & mstrModuleName)
            Return False
        Finally
        End Try
        Return True
    End Function

	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Public Shared Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "This Approver Level Code is already defined. Please define new Approver Level Code.")
			Language.setMessage(mstrModuleName, 2, "This Approver Level Name is already defined. Please define new Approver Level Name.")
			Language.setMessage(mstrModuleName, 3, "This Approver Level Priority is already assign. Please assign new Approver Level Priority.")
			Language.setMessage(mstrModuleName, 4, "Select")
			Language.setMessage(mstrModuleName, 5, "WEB")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class