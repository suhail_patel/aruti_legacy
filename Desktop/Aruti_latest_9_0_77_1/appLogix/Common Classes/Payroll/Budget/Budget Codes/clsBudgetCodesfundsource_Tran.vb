﻿'************************************************************************************************************************************
'Class Name : clsBudgetCodesfundsource_Tran.vb
'Purpose    :
'Date       :02/09/2016
'Written By :Sohail
'Modified   :
'************************************************************************************************************************************

Imports eZeeCommonLib
Imports eZee.Common.eZeeForm

''' <summary>
''' Purpose: 
''' Developer: Sohail
''' </summary>
Public Class clsBudgetCodesfundsource_Tran
    Private Shared ReadOnly mstrModuleName As String = "clsBudgetCodesfundsource_Tran"
    Dim objDataOperation As clsDataOperation
    Dim mstrMessage As String = ""

#Region " Private variables "
    Private mintBudgetCodesfundsourcetranunkid As Integer
    Private mintBudgettranunkid As Integer
    Private mintPeriodUnkId As Integer
    Private mintFundProjectCodeunkid As Integer
    Private mdecPercentage As Decimal
    Private mintFundactivityunkid As Integer
    Private mintUserunkid As Integer
    Private mblnIsvoid As Boolean
    Private mintVoiduserunkid As Integer
    Private mdtVoiddatetime As Date
    Private mstrVoidreason As String = String.Empty
    'Sohail (03 May 2017) -- Start
    'Enhancement - 66.1 - Allow to include newly hired employees on budget codes screen and allow to map employees with planned employees.
    Private mintBudgetcodestranunkid As Integer
    'Sohail (03 May 2017) -- End

    'Private mstrWebFormName As String = String.Empty
    'Private mstrWebIP As String = ""
    'Private mstrWebHost As String = ""
#End Region

#Region " Properties "

    'S.SANDEEP [28-May-2018] -- START 
    'ISSUE/ENHANCEMENT : {Audit Trails} 
    Private mstrFormName As String = String.Empty
    Public WriteOnly Property _FormName() As String
        Set(ByVal value As String)
            mstrFormName = value
        End Set
    End Property

    Private mstrClientIP As String = ""
    Public WriteOnly Property _ClientIP() As String
        Set(ByVal value As String)
            mstrClientIP = value
        End Set
    End Property

    Private mstrHostName As String = ""
    Public WriteOnly Property _HostName() As String
        Set(ByVal value As String)
            mstrHostName = value
        End Set
    End Property

    Private mblnIsWeb As Boolean = False
    Public WriteOnly Property _FromWeb() As Boolean
        Set(ByVal value As Boolean)
            mblnIsWeb = value
        End Set
    End Property

    Private mintAuditUserId As Integer = 0
    Public WriteOnly Property _AuditUserId() As Integer
        Set(ByVal value As Integer)
            mintAuditUserId = value
        End Set
    End Property

    Private mintCompanyUnkid As Integer = 0
    Public WriteOnly Property _CompanyUnkid() As Integer
        Set(ByVal value As Integer)
            mintCompanyUnkid = value
        End Set
    End Property

    Private mdtAuditDate As DateTime = Now
    Public WriteOnly Property _AuditDate() As DateTime
        Set(ByVal value As DateTime)
            mdtAuditDate = value
        End Set
    End Property

    Private xDataOpr As clsDataOperation = Nothing
    Public Property _xDataOpr() As clsDataOperation
        Get
            Return xDataOpr
        End Get
        Set(ByVal value As clsDataOperation)
            xDataOpr = value
        End Set
    End Property

    Private mintLoginEmployeeunkid As Integer = 0
    Public Property _LoginEmployeeunkid() As Integer
        Get
            Return mintLoginEmployeeunkid
        End Get
        Set(ByVal value As Integer)
            mintLoginEmployeeunkid = value
        End Set
    End Property

    Private mintVoidlogingemployeeunkid As Integer = 0
    Public Property _Voidloginemployeeunkid() As Integer
        Get
            Return mintVoidlogingemployeeunkid
        End Get
        Set(ByVal value As Integer)
            mintVoidlogingemployeeunkid = value
        End Set
    End Property
    'S.SANDEEP [28-May-2018] -- END 

    Public ReadOnly Property _Message() As String
        Get
            Return mstrMessage
        End Get
    End Property

    ''' <summary>
    ''' Purpose: Get or Set budgetcodesfundsourcetranunkid
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _BudgetCodesfundsourcetranunkid() As Integer
        Get
            Return mintBudgetCodesfundsourcetranunkid
        End Get
        Set(ByVal value As Integer)
            mintBudgetCodesfundsourcetranunkid = value
            Call GetData()
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set budgettranunkid
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Budgettranunkid() As Integer
        Get
            Return mintBudgettranunkid
        End Get
        Set(ByVal value As Integer)
            mintBudgettranunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set periodunkid
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _PeriodUnkId() As Integer
        Get
            Return mintPeriodUnkId
        End Get
        Set(ByVal value As Integer)
            mintPeriodUnkId = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set fundprojectcodeunkid
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _FundProjectCodeunkid() As Integer
        Get
            Return mintFundProjectCodeunkid
        End Get
        Set(ByVal value As Integer)
            mintFundProjectCodeunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set percentage
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Percentage() As Decimal
        Get
            Return mdecPercentage
        End Get
        Set(ByVal value As Decimal)
            mdecPercentage = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set fundactivityunkid
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Fundactivityunkid() As Integer
        Get
            Return mintFundactivityunkid
        End Get
        Set(ByVal value As Integer)
            mintFundactivityunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set userunkid
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Userunkid() As Integer
        Get
            Return mintUserunkid
        End Get
        Set(ByVal value As Integer)
            mintUserunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isvoid
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Isvoid() As Boolean
        Get
            Return mblnIsvoid
        End Get
        Set(ByVal value As Boolean)
            mblnIsvoid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiduserunkid
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Voiduserunkid() As Integer
        Get
            Return mintVoiduserunkid
        End Get
        Set(ByVal value As Integer)
            mintVoiduserunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiddatetime
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Voiddatetime() As Date
        Get
            Return mdtVoiddatetime
        End Get
        Set(ByVal value As Date)
            mdtVoiddatetime = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voidreason
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Voidreason() As String
        Get
            Return mstrVoidreason
        End Get
        Set(ByVal value As String)
            mstrVoidreason = value
        End Set
    End Property

    'Public WriteOnly Property _WebFormName() As String
    '    Set(ByVal value As String)
    '        mstrWebFormName = value
    '    End Set
    'End Property

    'Public WriteOnly Property _WebIP() As String
    '    Set(ByVal value As String)
    '        mstrWebIP = value
    '    End Set
    'End Property

    'Public WriteOnly Property _WebHost() As String
    '    Set(ByVal value As String)
    '        mstrWebHost = value
    '    End Set
    'End Property

    'Sohail (03 May 2017) -- Start
    'Enhancement - 66.1 - Allow to include newly hired employees on budget codes screen and allow to map employees with planned employees.
    Public Property _Budgetcodestranunkid() As Integer
        Get
            Return mintBudgetcodestranunkid
        End Get
        Set(ByVal value As Integer)
            mintBudgetcodestranunkid = Value
        End Set
    End Property
    'Sohail (03 May 2017) -- End

#End Region

    ''' <summary>
    ''' Modify By: Sohail
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Sub GetData()
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "SELECT " & _
              "  budgetcodesfundsourcetranunkid " & _
              ", budgetcodestranunkid " & _
              ", budgettranunkid " & _
              ", periodunkid " & _
              ", fundprojectcodeunkid " & _
              ", percentage " & _
              ", fundactivityunkid " & _
              ", userunkid " & _
              ", isvoid " & _
              ", voiduserunkid " & _
              ", voiddatetime " & _
              ", voidreason " & _
             "FROM bgbudgetcodesfundsource_tran " & _
             "WHERE budgetcodesfundsourcetranunkid = @budgetcodesfundsourcetranunkid "
            'Sohail (03 May 2017) - [budgetcodestranunkid]

            objDataOperation.AddParameter("@budgetcodesfundsourcetranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintBudgetCodesfundsourcetranunkid.ToString)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For Each dtRow As DataRow In dsList.Tables(0).Rows
                mintBudgetCodesfundsourcetranunkid = CInt(dtRow.Item("budgetcodesfundsourcetranunkid"))
                'Sohail (03 May 2017) -- Start
                'Enhancement - 66.1 - Allow to include newly hired employees on budget codes screen and allow to map employees with planned employees.
                mintBudgetcodestranunkid = CInt(dtRow.Item("budgetcodestranunkid"))
                'Sohail (03 May 2017) -- End
                mintPeriodUnkId = CInt(dtRow.Item("periodunkid"))
                mintFundProjectCodeunkid = CInt(dtRow.Item("fundprojectcodeunkid"))
                mdecPercentage = dtRow.Item("percentage")
                mintFundactivityunkid = CInt(dtRow.Item("fundactivityunkid"))
                mintUserunkid = CInt(dtRow.Item("userunkid"))
                mblnIsvoid = CBool(dtRow.Item("isvoid"))
                mintVoiduserunkid = CInt(dtRow.Item("voiduserunkid"))
                If IsDBNull(dtRow.Item("voiddatetime")) = True Then
                    mdtVoiddatetime = Nothing
                Else
                    mdtVoiddatetime = dtRow.Item("voiddatetime")
                End If
                mstrVoidreason = dtRow.Item("voidreason").ToString
                Exit For
            Next
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetData; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            'objDataOperation = Nothing
        End Try
    End Sub


    ''' <summary>
    ''' Modify By: Sohail
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function GetList(ByVal strTableName As String, Optional ByVal strFilter As String = "", Optional ByVal xDataOp As clsDataOperation = Nothing) As DataSet
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        If xDataOp Is Nothing Then
        objDataOperation = New clsDataOperation
        Else
            objDataOperation = xDataOp
        End If
        objDataOperation.ClearParameters()

        Try
            strQ = "SELECT " & _
              "  budgetcodesfundsourcetranunkid " & _
              ", budgetcodestranunkid " & _
              ", budgettranunkid " & _
              ", fundprojectcodeunkid " & _
              ", percentage " & _
              ", fundactivityunkid " & _
              ", userunkid " & _
              ", isvoid " & _
              ", voiduserunkid " & _
              ", voiddatetime " & _
              ", voidreason " & _
             "FROM bgbudgetcodesfundsource_tran " & _
             "WHERE isvoid = 0 "
            'Sohail (03 May 2017) - [budgetcodestranunkid]

            If strFilter.Trim <> "" Then
                strQ &= " " & strFilter & " "
            End If

            dsList = objDataOperation.ExecQuery(strQ, strTableName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xDataOp Is Nothing Then objDataOperation = Nothing
        End Try
        Return dsList
    End Function


    ''' <summary>
    ''' Modify By: Sohail
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> INSERT INTO Database Table (bgbudgetcodesfundsource_tran) </purpose>
    Public Function Insert(ByVal dtCurrentDateAndTime As Date, Optional ByVal xDataOpr As clsDataOperation = Nothing) As Boolean
        'If isExist(mstrName) Then
        '    mstrMessage = "<Message>"
        '    Return False
        'End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        If xDataOpr Is Nothing Then
            objDataOperation = New clsDataOperation
            objDataOperation.BindTransaction()
        Else
            objDataOperation = xDataOpr
        End If
        objDataOperation.ClearParameters()

        Try
            objDataOperation.AddParameter("@budgettranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintBudgettranunkid.ToString)
            objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPeriodUnkId.ToString)
            objDataOperation.AddParameter("@fundprojectcodeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintFundProjectCodeunkid.ToString)
            objDataOperation.AddParameter("@percentage", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecPercentage.ToString)
            objDataOperation.AddParameter("@fundactivityunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintFundactivityunkid.ToString)
            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
            If mdtVoiddatetime = Nothing Then
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            Else
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime.ToString)
            End If
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)
            'Sohail (03 May 2017) -- Start
            'Enhancement - 66.1 - Allow to include newly hired employees on budget codes screen and allow to map employees with planned employees.
            objDataOperation.AddParameter("@budgetcodestranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintBudgetcodestranunkid.ToString)
            'Sohail (03 May 2017) -- End

            strQ = "INSERT INTO bgbudgetcodesfundsource_tran ( " & _
              "  budgettranunkid " & _
              ", periodunkid " & _
              ", fundprojectcodeunkid " & _
              ", percentage " & _
              ", fundactivityunkid " & _
              ", userunkid " & _
              ", isvoid " & _
              ", voiduserunkid " & _
              ", voiddatetime " & _
              ", voidreason" & _
              ", budgetcodestranunkid " & _
            ") VALUES (" & _
              "  @budgettranunkid " & _
              ", @periodunkid " & _
              ", @fundprojectcodeunkid " & _
              ", @percentage " & _
              ", @fundactivityunkid " & _
              ", @userunkid " & _
              ", @isvoid " & _
              ", @voiduserunkid " & _
              ", @voiddatetime " & _
              ", @voidreason" & _
              ", @budgetcodestranunkid " & _
            "); SELECT @@identity"
            'Sohail (03 May 2017) - [budgetcodestranunkid]

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            mintBudgetCodesfundsourcetranunkid = dsList.Tables(0).Rows(0).Item(0)

            If InsertAuditFundSourceTran(objDataOperation, enAuditType.ADD, dtCurrentDateAndTime) = False Then
                If xDataOpr Is Nothing Then
                    objDataOperation.ReleaseTransaction(False)
                End If
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If xDataOpr Is Nothing Then
                objDataOperation.ReleaseTransaction(True)
            End If

            Return True
        Catch ex As Exception
            If xDataOpr Is Nothing Then
                objDataOperation.ReleaseTransaction(False)
            End If
            Throw New Exception(ex.Message & "; Procedure Name: Insert; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xDataOpr Is Nothing Then
                objDataOperation = Nothing
            End If
        End Try
    End Function

    Public Function InsertAll(ByVal dtRow As DataRow, ByVal xDataOpr As clsDataOperation, ByVal dtCurrentDateAndTime As Date) As Boolean
        'If isExist(mstrName) Then
        '    mstrMessage = "<Message>"
        '    Return False
        'End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception


        xDataOpr.ClearParameters()

        Try

            Dim cols As List(Of DataColumn) = (From p As DataColumn In dtRow.Table.Columns Where (p.ColumnName.StartsWith("|_")) Select (p)).ToList
            For Each col As DataColumn In cols
                'mintFundsourceunkid = CInt(col.ColumnName.Replace("|_", ""))
                mintFundProjectCodeunkid = CInt(col.ColumnName.Replace("|_", ""))
                mdecPercentage = CDec(dtRow.Item(col.ColumnName))
                mintFundactivityunkid = CInt(dtRow.Item(col.ColumnName.Replace("|_", "||_")))
                'If isExist(mintBudgettranunkid, mintFundsourceunkid, , mintBudgetfundsourcetranunkid) = False Then
                If Insert(dtCurrentDateAndTime, xDataOpr) = False Then
                    Return False
                End If
                'Else
                'If Update(dtCurrentDateAndTime, xDataOpr) = False Then
                '    Return False
                'End If
                'End If

            Next

            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: InsertAll; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            'objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sohail
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Update Database Table (bgbudgetcodesfundsource_tran) </purpose>
    Public Function Update(ByVal dtCurrentDateAndTime As Date, ByVal dsATFundSource As DataSet, Optional ByVal xDataOpr As clsDataOperation = Nothing) As Boolean
        'If isExist(mstrName, mintBudgetfundsourcetranunkid) Then
        '    mstrMessage = "<Message>"
        '    Return False
        'End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        If xDataOpr Is Nothing Then
            objDataOperation = New clsDataOperation
            objDataOperation.BindTransaction()
        Else
            objDataOperation = xDataOpr
        End If
        objDataOperation.ClearParameters()

        Try
            objDataOperation.AddParameter("@budgetcodesfundsourcetranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintBudgetCodesfundsourcetranunkid.ToString)
            objDataOperation.AddParameter("@budgettranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintBudgettranunkid.ToString)
            objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPeriodUnkId.ToString)
            objDataOperation.AddParameter("@fundprojectcodeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintFundProjectCodeunkid.ToString)
            objDataOperation.AddParameter("@percentage", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecPercentage.ToString)
            objDataOperation.AddParameter("@fundactivityunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintFundactivityunkid.ToString)
            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
            If mdtVoiddatetime = Nothing Then
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            Else
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime.ToString)
            End If
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)
            'Sohail (03 May 2017) -- Start
            'Enhancement - 66.1 - Allow to include newly hired employees on budget codes screen and allow to map employees with planned employees.
            objDataOperation.AddParameter("@budgetcodestranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintBudgetcodestranunkid.ToString)
            'Sohail (03 May 2017) -- End

            strQ = "UPDATE bgbudgetcodesfundsource_tran SET " & _
              "  budgettranunkid = @budgettranunkid" & _
              ", periodunkid = @periodunkid" & _
              ", fundprojectcodeunkid = @fundprojectcodeunkid" & _
              ", percentage = @percentage" & _
              ", fundactivityunkid = @fundactivityunkid" & _
              ", userunkid = @userunkid" & _
              ", isvoid = @isvoid" & _
              ", voiduserunkid = @voiduserunkid" & _
              ", voiddatetime = @voiddatetime" & _
              ", voidreason = @voidreason " & _
              ", budgetcodestranunkid = @budgetcodestranunkid " & _
            " WHERE budgetcodesfundsourcetranunkid = @budgetcodesfundsourcetranunkid "
            'Sohail (03 May 2017) - [budgetcodestranunkid]

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If IsTableDataUpdate(mintBudgetCodesfundsourcetranunkid, dsATFundSource, objDataOperation) = False Then
            If InsertAuditFundSourceTran(objDataOperation, enAuditType.EDIT, dtCurrentDateAndTime) = False Then
                If xDataOpr Is Nothing Then
                    objDataOperation.ReleaseTransaction(False)
                End If
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If
            End If


            If xDataOpr Is Nothing Then
                objDataOperation.ReleaseTransaction(True)
            End If

            Return True
        Catch ex As Exception
            If xDataOpr Is Nothing Then
                objDataOperation.ReleaseTransaction(False)
            End If
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xDataOpr Is Nothing Then
                objDataOperation = Nothing
            End If
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sohail
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Delete Database Table (bgbudgetcodesfundsource_tran) </purpose>
    Public Function Void(ByVal intUnkid As Integer, ByVal dtCurrentDateAndTime As Date, Optional ByVal xDataOpr As clsDataOperation = Nothing) As Boolean
        'If isUsed(intUnkid) Then
        '    mstrMessage = "<Message>"
        '    Return False
        'End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        If xDataOpr Is Nothing Then
            objDataOperation = New clsDataOperation
            objDataOperation.BindTransaction()
        Else
            objDataOperation = xDataOpr
        End If
        objDataOperation.ClearParameters()

        Try

            objDataOperation.AddParameter("@budgetcodesfundsourcetranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid.ToString)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
            If mdtVoiddatetime = Nothing Then
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            Else
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime.ToString)
            End If
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)

            strQ = "UPDATE bgbudgetcodesfundsource_tran SET " & _
                     "  isvoid = @isvoid" & _
                     ", voiduserunkid = @voiduserunkid" & _
                     ", voiddatetime = @voiddatetime" & _
                     ", voidreason = @voidreason " & _
           "WHERE budgetcodesfundsourcetranunkid = @budgetcodesfundsourcetranunkid "

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Me._BudgetCodesfundsourcetranunkid = intUnkid

            If InsertAuditFundSourceTran(objDataOperation, enAuditType.DELETE, dtCurrentDateAndTime) = False Then
                If xDataOpr Is Nothing Then
                    objDataOperation.ReleaseTransaction(False)
                End If
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If xDataOpr Is Nothing Then
                objDataOperation.ReleaseTransaction(True)
            End If

            Return True
        Catch ex As Exception
            If xDataOpr Is Nothing Then
                objDataOperation.ReleaseTransaction(False)
            End If
            Throw New Exception(ex.Message & "; Procedure Name: Void; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xDataOpr Is Nothing Then
                objDataOperation = Nothing
            End If
        End Try
    End Function

    Public Function VoidAllByBudgetUnkId(ByVal intBudgetUnkid As Integer _
                                         , ByVal intPeriodUnkId As Integer _
                                         , ByVal intVoiduserunkid As Integer _
                                         , ByVal dtVoiddatetime As Date _
                                         , ByVal strVoidreason As String _
                                         , Optional ByVal xDataOpr As clsDataOperation = Nothing _
                                         ) As Boolean
        'If isUsed(intUnkid) Then
        '    mstrMessage = "<Message>"
        '    Return False
        'End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim intRowsAffected As Integer = 0

        If xDataOpr Is Nothing Then
            objDataOperation = New clsDataOperation
            objDataOperation.BindTransaction()
        Else
            objDataOperation = xDataOpr
        End If
        objDataOperation.ClearParameters()

        Try

            'If mstrWebIP.Trim = "" Then mstrWebIP = getIP()
            'If mstrWebHost.Trim = "" Then mstrWebHost = getHostName()
            objDataOperation.AddParameter("@form_name", SqlDbType.NVarChar, 500, mstrFormName)
            objDataOperation.AddParameter("@isweb", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsWeb)

            strQ = "INSERT INTO atbgbudgetcodesfundsource_tran ( " & _
                                "  budgetcodesfundsourcetranunkid " & _
                                ", budgettranunkid " & _
                                ", periodunkid " & _
                                ", fundprojectcodeunkid " & _
                                ", percentage " & _
                                ", fundactivityunkid " & _
                                ", budgetcodestranunkid " & _
                                ", audittype " & _
                                ", audituserunkid " & _
                                ", auditdatetime " & _
                                ", ip " & _
                                ", machine_name " & _
                                ", form_name " & _
                                " " & _
                                " " & _
                                " " & _
                                " " & _
                                " " & _
                                ", isweb " & _
                         ") " & _
                         "SELECT " & _
                                "  budgetcodesfundsourcetranunkid " & _
                                ", bgbudgetcodesfundsource_tran.budgettranunkid " & _
                                ", bgbudgetcodesfundsource_tran.periodunkid " & _
                                ", fundprojectcodeunkid " & _
                                ", percentage " & _
                                ", fundactivityunkid " & _
                                ", bgbudgetcodesfundsource_tran.budgetcodestranunkid " & _
                                ", 3 " & _
                                ", @voiduserunkid " & _
                                ", @voiddatetime " & _
                                ", '" & mstrClientIP & "' " & _
                                ", '" & mstrHostName & "' " & _
                                ", @form_name " & _
                                " " & _
                                " " & _
                                " " & _
                                " " & _
                                " " & _
                                ", @isweb " & _
                         "FROM bgbudgetcodesfundsource_tran " & _
                         "LEFT JOIN bgbudgetcodes_tran ON bgbudgetcodes_tran.budgetcodestranunkid = bgbudgetcodesfundsource_tran.budgetcodestranunkid " & _
                         "WHERE bgbudgetcodesfundsource_tran.isvoid = 0 " & _
                         "AND bgbudgetcodes_tran.isvoid = 0 " & _
                         "AND budgetunkid = @budgetunkid " & _
                         "AND bgbudgetcodesfundsource_tran.periodunkid = @periodunkid "
            'Sohail (03 May 2017) - [budgetcodestranunkid], [LEFT JOIN bgbudget_tran ON bgbudget_tran.budgettranunkid = bgbudgetcodesfundsource_tran.budgettranunkid]=[LEFT JOIN bgbudgetcodes_tran ON bgbudgetcodes_tran.budgetcodestranunkid = bgbudgetcodesfundsource_tran.budgetcodestranunkid]

            objDataOperation.AddParameter("@budgetunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intBudgetUnkid.ToString)
            objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intPeriodUnkId.ToString)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, True)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intVoiduserunkid.ToString)
            If dtVoiddatetime = Nothing Then
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            Else
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, dtVoiddatetime.ToString)
            End If
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strVoidreason.ToString)

            intRowsAffected = objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                objDataOperation.ReleaseTransaction(False)
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            'Sohail (03 May 2017) -- Start
            'Enhancement - 66.1 - Allow to include newly hired employees on budget codes screen and allow to map employees with planned employees.
            'strQ = "UPDATE bgbudgetcodesfundsource_tran SET " & _
            '        "  isvoid = @isvoid" & _
            '        ", voiduserunkid = @voiduserunkid" & _
            '        ", voiddatetime = @voiddatetime" & _
            '        ", voidreason = @voidreason " & _
            '    "FROM bgbudget_tran " & _
            '    "WHERE bgbudget_tran.budgettranunkid = bgbudgetcodesfundsource_tran.budgettranunkid " & _
            '    "AND bgbudgetcodesfundsource_tran.isvoid = 0 " & _
            '    "AND bgbudget_tran.isvoid = 0 " & _
            '    "AND budgetunkid = @budgetunkid " & _
            '    "AND bgbudgetcodesfundsource_tran.periodunkid = @periodunkid "
            strQ = "UPDATE bgbudgetcodesfundsource_tran SET " & _
                    "  isvoid = @isvoid" & _
                    ", voiduserunkid = @voiduserunkid" & _
                    ", voiddatetime = @voiddatetime" & _
                    ", voidreason = @voidreason " & _
                "FROM bgbudgetcodes_tran " & _
                "WHERE bgbudgetcodes_tran.budgetcodestranunkid = bgbudgetcodesfundsource_tran.budgetcodestranunkid " & _
                "AND bgbudgetcodesfundsource_tran.isvoid = 0 " & _
                "AND bgbudgetcodes_tran.isvoid = 0 " & _
                "AND budgetunkid = @budgetunkid " & _
                "AND bgbudgetcodesfundsource_tran.periodunkid = @periodunkid "
            'Sohail (03 May 2017) -- End

            intRowsAffected = objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                objDataOperation.ReleaseTransaction(False)
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If xDataOpr Is Nothing Then
                objDataOperation.ReleaseTransaction(True)
            End If

            Return True
        Catch ex As Exception
            If xDataOpr Is Nothing Then
                objDataOperation.ReleaseTransaction(False)
            End If
            Throw New Exception(ex.Message & "; Procedure Name: VoidAllByBudgetUnkId; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xDataOpr Is Nothing Then
                objDataOperation = Nothing
            End If
        End Try
    End Function

    'Sohail (19 Apr 2017) -- Start
    'MST Enhancement - 66.1 - applying user access filter on payroll budget.
    Public Function VoidAllByBudgetTranUnkId(ByVal strBudgetTranUnkIDs As String _
                                         , ByVal intPeriodUnkId As Integer _
                                         , ByVal intVoiduserunkid As Integer _
                                         , ByVal dtVoiddatetime As Date _
                                         , ByVal strVoidreason As String _
                                         , Optional ByVal xDataOpr As clsDataOperation = Nothing _
                                         ) As Boolean
        'If isUsed(intUnkid) Then
        '    mstrMessage = "<Message>"
        '    Return False
        'End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim intRowsAffected As Integer = 0

        If xDataOpr Is Nothing Then
            objDataOperation = New clsDataOperation
            objDataOperation.BindTransaction()
        Else
            objDataOperation = xDataOpr
        End If
        objDataOperation.ClearParameters()

        Try

            'If mstrWebIP.Trim = "" Then mstrWebIP = getIP()
            'If mstrWebHost.Trim = "" Then mstrWebHost = getHostName()

            objDataOperation.AddParameter("@form_name", SqlDbType.NVarChar, 500, mstrFormName)
            objDataOperation.AddParameter("@isweb", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsWeb)

            strQ = "SELECT budgettranunkid INTO #cteBTran FROM bgbudget_tran WHERE budgettranunkid IN (" & strBudgetTranUnkIDs & "); "

            strQ &= "INSERT INTO atbgbudgetcodesfundsource_tran ( " & _
                                "  budgetcodesfundsourcetranunkid " & _
                                ", budgettranunkid " & _
                                ", periodunkid " & _
                                ", fundprojectcodeunkid " & _
                                ", percentage " & _
                                ", fundactivityunkid " & _
                                ", audittype " & _
                                ", audituserunkid " & _
                                ", auditdatetime " & _
                                ", ip " & _
                                ", machine_name " & _
                                ", form_name " & _
                                " " & _
                                " " & _
                                " " & _
                                " " & _
                                " " & _
                                ", isweb " & _
                         ") " & _
                         "SELECT " & _
                                "  budgetcodesfundsourcetranunkid " & _
                                ", bgbudgetcodesfundsource_tran.budgettranunkid " & _
                                ", periodunkid " & _
                                ", fundprojectcodeunkid " & _
                                ", percentage " & _
                                ", fundactivityunkid " & _
                                ", 3 " & _
                                ", @voiduserunkid " & _
                                ", @voiddatetime " & _
                                ", '" & mstrClientIP & "' " & _
                                ", '" & mstrHostName & "' " & _
                                ", @form_name " & _
                                " " & _
                                " " & _
                                " " & _
                                " " & _
                                " " & _
                                ", @isweb " & _
                         "FROM bgbudgetcodesfundsource_tran " & _
                         "LEFT JOIN bgbudget_tran ON bgbudget_tran.budgettranunkid = bgbudgetcodesfundsource_tran.budgettranunkid " & _
                         "JOIN #cteBTran ON #cteBTran.budgettranunkid = bgbudget_tran.budgettranunkid " & _
                         "WHERE bgbudgetcodesfundsource_tran.isvoid = 0 " & _
                         "AND bgbudget_tran.isvoid = 0 " & _
                         "AND bgbudgetcodesfundsource_tran.periodunkid = @periodunkid "

            strQ &= "DROP TABLE #cteBTran"

            objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intPeriodUnkId.ToString)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, True)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intVoiduserunkid.ToString)
            If dtVoiddatetime = Nothing Then
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            Else
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, dtVoiddatetime.ToString)
            End If
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strVoidreason.ToString)

            intRowsAffected = objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                objDataOperation.ReleaseTransaction(False)
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            strQ = "SELECT budgettranunkid INTO #cteBTran FROM bgbudget_tran WHERE budgettranunkid IN (" & strBudgetTranUnkIDs & "); "

            strQ &= "UPDATE bgbudgetcodesfundsource_tran SET " & _
                    "  isvoid = @isvoid" & _
                    ", voiduserunkid = @voiduserunkid" & _
                    ", voiddatetime = @voiddatetime" & _
                    ", voidreason = @voidreason " & _
                "FROM bgbudget_tran " & _
                "JOIN #cteBTran ON #cteBTran.budgettranunkid = bgbudget_tran.budgettranunkid " & _
                "WHERE bgbudget_tran.budgettranunkid = bgbudgetcodesfundsource_tran.budgettranunkid " & _
                "AND bgbudgetcodesfundsource_tran.isvoid = 0 " & _
                "AND bgbudget_tran.isvoid = 0 " & _
                "AND bgbudgetcodesfundsource_tran.periodunkid = @periodunkid "

            strQ &= "DROP TABLE #cteBTran"

            intRowsAffected = objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                objDataOperation.ReleaseTransaction(False)
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If xDataOpr Is Nothing Then
                objDataOperation.ReleaseTransaction(True)
            End If

            Return True
        Catch ex As Exception
            If xDataOpr Is Nothing Then
                objDataOperation.ReleaseTransaction(False)
            End If
            Throw New Exception(ex.Message & "; Procedure Name: VoidAllByBudgetTranUnkId; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xDataOpr Is Nothing Then
                objDataOperation = Nothing
            End If
        End Try
    End Function
    'Sohail (19 Apr 2017) -- End

    'Sohail (03 May 2017) -- Start
    'Enhancement - 66.1 - Allow to include newly hired employees on budget codes screen and allow to map employees with planned employees.
    Public Function VoidAllByBudgetCodesTranUnkId(ByVal strBudgetCodesTranUnkIDs As String _
                                                 , ByVal intPeriodUnkId As Integer _
                                                 , ByVal intVoiduserunkid As Integer _
                                                 , ByVal dtVoiddatetime As Date _
                                                 , ByVal strVoidreason As String _
                                                 , Optional ByVal xDataOpr As clsDataOperation = Nothing _
                                                 ) As Boolean
        'If isUsed(intUnkid) Then
        '    mstrMessage = "<Message>"
        '    Return False
        'End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim intRowsAffected As Integer = 0

        If xDataOpr Is Nothing Then
            objDataOperation = New clsDataOperation
            objDataOperation.BindTransaction()
        Else
            objDataOperation = xDataOpr
        End If
        objDataOperation.ClearParameters()

        Try

            'If mstrWebIP.Trim = "" Then mstrWebIP = getIP()
            'If mstrWebHost.Trim = "" Then mstrWebHost = getHostName()

            objDataOperation.AddParameter("@form_name", SqlDbType.NVarChar, 500, mstrFormName)
            objDataOperation.AddParameter("@isweb", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsWeb)

            strQ = "SELECT budgetcodestranunkid INTO #cteBTran FROM bgbudgetcodes_tran WHERE budgetcodestranunkid IN (" & strBudgetCodesTranUnkIDs & "); "

            strQ &= "INSERT INTO atbgbudgetcodesfundsource_tran ( " & _
                                "  budgetcodesfundsourcetranunkid " & _
                                ", budgetcodestranunkid " & _
                                ", budgettranunkid " & _
                                ", periodunkid " & _
                                ", fundprojectcodeunkid " & _
                                ", percentage " & _
                                ", fundactivityunkid " & _
                                ", audittype " & _
                                ", audituserunkid " & _
                                ", auditdatetime " & _
                                ", ip " & _
                                ", machine_name " & _
                                ", form_name " & _
                                " " & _
                                " " & _
                                " " & _
                                " " & _
                                " " & _
                                ", isweb " & _
                         ") " & _
                         "SELECT " & _
                                "  budgetcodesfundsourcetranunkid " & _
                                ", bgbudgetcodesfundsource_tran.budgetcodestranunkid " & _
                                ", bgbudgetcodesfundsource_tran.budgettranunkid " & _
                                ", periodunkid " & _
                                ", fundprojectcodeunkid " & _
                                ", percentage " & _
                                ", fundactivityunkid " & _
                                ", 3 " & _
                                ", @voiduserunkid " & _
                                ", @voiddatetime " & _
                                ", '" & mstrClientIP & "' " & _
                                ", '" & mstrHostName & "' " & _
                                ", @form_name " & _
                                " " & _
                                " " & _
                                " " & _
                                " " & _
                                " " & _
                                ", @isweb " & _
                         "FROM bgbudgetcodesfundsource_tran " & _
                         "JOIN #cteBTran ON #cteBTran.budgetcodestranunkid = bgbudgetcodesfundsource_tran.budgetcodestranunkid " & _
                         "WHERE bgbudgetcodesfundsource_tran.isvoid = 0 " & _
                         "AND bgbudgetcodesfundsource_tran.periodunkid = @periodunkid "

            strQ &= "DROP TABLE #cteBTran"

            objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intPeriodUnkId.ToString)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, True)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intVoiduserunkid.ToString)
            If dtVoiddatetime = Nothing Then
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            Else
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, dtVoiddatetime.ToString)
            End If
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strVoidreason.ToString)

            intRowsAffected = objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                objDataOperation.ReleaseTransaction(False)
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            strQ = "SELECT budgetcodestranunkid INTO #cteBTran FROM bgbudgetcodes_tran WHERE budgetcodestranunkid IN (" & strBudgetCodesTranUnkIDs & "); "

            strQ &= "UPDATE bgbudgetcodesfundsource_tran SET " & _
                    "  isvoid = @isvoid" & _
                    ", voiduserunkid = @voiduserunkid" & _
                    ", voiddatetime = @voiddatetime" & _
                    ", voidreason = @voidreason " & _
                "FROM #cteBTran " & _
                "WHERE #cteBTran.budgetcodestranunkid = bgbudgetcodesfundsource_tran.budgetcodestranunkid " & _
                "AND bgbudgetcodesfundsource_tran.isvoid = 0 " & _
                "AND bgbudgetcodesfundsource_tran.periodunkid = @periodunkid "

            strQ &= "DROP TABLE #cteBTran"

            intRowsAffected = objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                objDataOperation.ReleaseTransaction(False)
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If xDataOpr Is Nothing Then
                objDataOperation.ReleaseTransaction(True)
            End If

            Return True
        Catch ex As Exception
            If xDataOpr Is Nothing Then
                objDataOperation.ReleaseTransaction(False)
            End If
            Throw New Exception(ex.Message & "; Procedure Name: VoidAllByBudgetCodesTranUnkId; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xDataOpr Is Nothing Then
                objDataOperation = Nothing
            End If
        End Try
    End Function
    'Sohail (03 May 2017) -- End

    ''' <summary>
    ''' Modify By: Sohail
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isUsed(ByVal intUnkid As Integer) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "<Query>"

            objDataOperation.AddParameter("@budgetcodesfundsourcetranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isUsed; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sohail
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isExist(ByVal intBudgetCodestranunkid As Integer, ByVal intFundActivityUnkid As Integer, ByVal intFundProjectCodeUnkid As Integer, Optional ByVal xDataOp As clsDataOperation = Nothing, Optional ByVal intUnkid As Integer = -1) As Integer
        'Sohail (01 May 2021) - [intBudgetCodestranunkid, intFundActivityUnkid, intFundProjectCodeUnkid]
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim intRet_UnkId As Integer = 0

        If xDataOp Is Nothing Then
        objDataOperation = New clsDataOperation
        Else
            objDataOperation = xDataOp
        End If
        objDataOperation.ClearParameters()

        Try
            strQ = "SELECT " & _
              "  budgetcodesfundsourcetranunkid " & _
             "FROM bgbudgetcodesfundsource_tran " & _
             "WHERE isvoid = 0 "

            If intBudgetCodestranunkid > 0 Then
                strQ &= " AND budgetcodestranunkid = @budgetcodestranunkid "
                objDataOperation.AddParameter("@budgetcodestranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intBudgetCodestranunkid)
            End If

            If intFundActivityUnkid > 0 Then
                strQ &= " AND fundactivityunkid = @fundactivityunkid "
                objDataOperation.AddParameter("@fundactivityunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intFundActivityUnkid)
            End If

            If intFundProjectCodeUnkid > 0 Then
                strQ &= " AND fundprojectcodeunkid = @fundprojectcodeunkid "
                objDataOperation.AddParameter("@fundprojectcodeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intFundProjectCodeUnkid)
            End If

            If intUnkid > 0 Then
                strQ &= " AND budgetcodesfundsourcetranunkid <> @budgetcodesfundsourcetranunkid "
                objDataOperation.AddParameter("@budgetcodesfundsourcetranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)
            End If


            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If dsList.Tables(0).Rows.Count > 0 Then
                intRet_UnkId = CInt(dsList.Tables(0).Rows(0).Item("budgetcodesfundsourcetranunkid"))
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isExist; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xDataOp Is Nothing Then objDataOperation = Nothing
        End Try
        Return intRet_UnkId
    End Function

    'Sohail (01 May 2021) -- Start
    'IHI Issue : : Project codes are missing for some employees on editong budget codes.
    Public Function Save(ByVal dtRow As DataRow, ByVal xDataOpr As clsDataOperation, ByVal dtCurrentDateAndTime As Date, ByVal dsFundSource As DataSet, ByVal dsATFundSource As DataSet) As Boolean
        'If isExist(mstrName) Then
        '    mstrMessage = "<Message>"
        '    Return False
        'End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception


        xDataOpr.ClearParameters()

        Try

            Dim cols As List(Of DataColumn) = (From p As DataColumn In dtRow.Table.Columns Where (p.ColumnName.StartsWith("|_")) Select (p)).ToList
            'Dim cols As List(Of DataColumn) = (From p As DataColumn In dtRow.Table.Columns Where (p.ColumnName.StartsWith("|_") AndAlso CDec(dtRow.Item(p.ColumnName)) <> 0) Select (p)).ToList
            For Each col As DataColumn In cols
                'mintFundsourceunkid = CInt(col.ColumnName.Replace("|_", ""))
                mintFundProjectCodeunkid = CInt(col.ColumnName.Replace("|_", ""))
                mdecPercentage = CDec(dtRow.Item(col.ColumnName))
                mintFundactivityunkid = CInt(dtRow.Item(col.ColumnName.Replace("|_", "||_")))

                'mintBudgetCodesfundsourcetranunkid = isExist(mintBudgetcodestranunkid, mintFundactivityunkid, mintFundProjectCodeunkid, xDataOpr)
                Dim dr() As DataRow = dsFundSource.Tables(0).Select("budgetcodestranunkid = " & mintBudgetcodestranunkid & " AND fundactivityunkid = " & mintFundactivityunkid & " AND fundprojectcodeunkid = " & mintFundProjectCodeunkid & " ")

                If dr.Length <= 0 Then
                    If Insert(dtCurrentDateAndTime, xDataOpr) = False Then
                        Return False
                    End If
                Else
                    mintBudgetCodesfundsourcetranunkid = CInt(dr(0).Item("budgetcodesfundsourcetranunkid"))

                    If Update(dtCurrentDateAndTime, dsATFundSource, xDataOpr) = False Then
                        Return False
                    End If
                End If

            Next

            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Save; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            'objDataOperation = Nothing
        End Try
    End Function

    Public Function GetLastATRecords(ByVal strTableName As String, Optional ByVal strFilter As String = "", Optional ByVal xDataOp As clsDataOperation = Nothing) As DataSet
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        If xDataOp Is Nothing Then
            objDataOperation = New clsDataOperation
        Else
            objDataOperation = xDataOp
        End If
        objDataOperation.ClearParameters()

        Try
            strQ = "SELECT * " & _
                    "FROM " & _
                    "( " & _
                        "SELECT * " & _
                             ", DENSE_RANK() OVER (PARTITION BY budgetcodesfundsourcetranunkid " & _
                                                  "ORDER BY atbgbudgetcodesfundsource_tran.auditdatetime DESC " & _
                                                 ") AS ROWNO " & _
                        "FROM atbgbudgetcodesfundsource_tran " & _
                        "WHERE audittype <> 3 "

            If strFilter.Trim <> "" Then
                strQ &= " " & strFilter & " "
            End If

            strQ &= ") AS A " & _
                    "WHERE A.ROWNO = 1 "

            dsList = objDataOperation.ExecQuery(strQ, strTableName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xDataOp Is Nothing Then objDataOperation = Nothing
        End Try
        Return dsList
    End Function

    Public Function IsTableDataUpdate(ByVal unkid As Integer, ByVal dsATFundSource As DataSet, Optional ByVal objDooperation As clsDataOperation = Nothing) As Boolean
        Dim strQ As String = ""
        Dim dsList As New DataSet
        Dim exForce As Exception
        Try
            If objDooperation Is Nothing Then
                objDataOperation = New clsDataOperation
            Else
                objDataOperation = objDooperation
            End If

            'strQ = "SELECT TOP 1 * FROM atbgbudgetcodesfundsource_tran WHERE budgetcodesfundsourcetranunkid = @budgetcodesfundsourcetranunkid AND audittype <> 3 ORDER BY auditdatetime DESC"

            'objDataOperation.ClearParameters()
            'objDataOperation.AddParameter("@budgetcodesfundsourcetranunkid", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, unkid)

            'dsList = objDataOperation.ExecQuery(strQ, "List")

            'If objDataOperation.ErrorMessage <> "" Then
            '    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
            '    Throw exForce
            'End If

            Dim row() As DataRow = dsATFundSource.Tables(0).Select("budgetcodesfundsourcetranunkid = " & mintBudgetCodesfundsourcetranunkid & " ")

            For Each dr As DataRow In row

                If dr("budgettranunkid").ToString() = mintBudgettranunkid AndAlso dr("periodunkid").ToString() = mintPeriodUnkId AndAlso dr("fundprojectcodeunkid").ToString() = mintFundProjectCodeunkid _
                    AndAlso CDec(dr("percentage")) = mdecPercentage AndAlso dr("fundactivityunkid").ToString() = mintFundactivityunkid _
                    AndAlso dr("budgetcodestranunkid").ToString() = mintBudgetcodestranunkid _
                    Then

                    Return True
                Else
                    Return False
                End If
            Next

            Return False

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: IsTableDataUpdate; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If objDooperation Is Nothing Then objDataOperation = Nothing
        End Try
    End Function
    'Sohail (01 May 2021) -- End

    Private Function InsertAuditFundSourceTran(ByVal xDataOpr As clsDataOperation, ByVal xAuditType As enAuditType, ByVal dtCurrentDateAndTime As Date) As Boolean
        Dim strQ As String = ""
        Dim strErrorMessage As String = ""
        Dim exForce As Exception = Nothing
        Try
            strQ = "INSERT INTO atbgbudgetcodesfundsource_tran ( " & _
                            "  budgetcodesfundsourcetranunkid " & _
                            ", budgetcodestranunkid " & _
                            ", budgettranunkid " & _
                            ", periodunkid " & _
                            ", fundprojectcodeunkid " & _
                            ", percentage " & _
                            ", fundactivityunkid " & _
                            ", audittype " & _
                            ", audituserunkid " & _
                            ", auditdatetime " & _
                            ", ip " & _
                            ", machine_name " & _
                            ", form_name " & _
                            " " & _
                            " " & _
                            " " & _
                            " " & _
                            " " & _
                            ", isweb " & _
                    ") VALUES (" & _
                            "  @budgetcodesfundsourcetranunkid " & _
                            ", @budgetcodestranunkid " & _
                            ", @budgettranunkid " & _
                            ", @periodunkid " & _
                            ", @fundprojectcodeunkid " & _
                            ", @percentage " & _
                            ", @fundactivityunkid " & _
                            ", @audittype " & _
                            ", @audituserunkid " & _
                            ", @auditdatetime " & _
                            ", @ip " & _
                            ", @machine_name " & _
                            ", @form_name " & _
                            " " & _
                            " " & _
                            " " & _
                            " " & _
                            " " & _
                            ", @isweb " & _
                         ") "


            xDataOpr.ClearParameters()
            xDataOpr.AddParameter("@budgetcodesfundsourcetranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintBudgetCodesfundsourcetranunkid.ToString)
            xDataOpr.AddParameter("@budgettranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintBudgettranunkid.ToString)
            xDataOpr.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPeriodUnkId.ToString)
            xDataOpr.AddParameter("@fundprojectcodeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintFundProjectCodeunkid.ToString)
            xDataOpr.AddParameter("@percentage", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecPercentage.ToString)
            xDataOpr.AddParameter("@fundactivityunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintFundactivityunkid.ToString)
            xDataOpr.AddParameter("@audittype", SqlDbType.Int, eZeeDataType.INT_SIZE, xAuditType)
            xDataOpr.AddParameter("@audituserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAuditUserId)
            xDataOpr.AddParameter("@auditdatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtAuditDate)
            xDataOpr.AddParameter("@budgetcodestranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintBudgetcodestranunkid.ToString) 'Sohail (03 May 2017)
            xDataOpr.AddParameter("@ip", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrClientIP)
            xDataOpr.AddParameter("@machine_name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrHostName)
            xDataOpr.AddParameter("@form_name", SqlDbType.NVarChar, 500, mstrFormName)
            xDataOpr.AddParameter("@isweb", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsWeb)

            xDataOpr.ExecNonQuery(strQ)

            If xDataOpr.ErrorMessage <> "" Then
                exForce = New Exception(xDataOpr.ErrorNumber & ": " & xDataOpr.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: InsertAuditFundSourceTran; Module Name: " & mstrModuleName)
            Return False
        Finally
        End Try
        Return True
    End Function



    'Pinkal (06-Dec-2016) -- Start
    'Enhancement - Working on Budget Employee timesheet Changes as Per Suzan and Andrew Requirement.

    Public Function GetEmployeeAssignActivities(ByVal xPeriodId As Integer, ByVal xEmployeeId As Integer _
                                                                     , ByVal mintShiftHrsinSec As Integer, ByVal xFromDate As Date _
                                                                     , ByVal xToDate As Date, ByVal blnAllowActivityHoursByPercentage As Boolean _
                                                                     , Optional ByVal xEmpTimesheetID As Integer = 0) As DataSet
        'Nilay (21 Mar 2017) -- [blnAllowActivityHoursByPercentage]


        'Pinkal (13-Oct-2017) --    'Enhancement - Ref Id 62 Working on Global Budget Timesheet Change.[  ByVal xFromDate As Date, ByVal xToDate As Date]

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        Try

            Dim objDataOperation As New clsDataOperation
            objDataOperation.ClearParameters()

            strQ = " SELECT DISTINCT " & _
                      "  bgbudget_master.budgetunkid " & _
                      " ,bgbudgetcodesfundsource_tran.periodunkid " & _
                      " ,bgbudgetcodes_tran.allocationtranunkid AS Employeeunkid " & _
                      " ,bgfundsource_master.fundsourceunkid " & _
                      " ,ISNULL(bgfundsource_master.fundname,'') AS Donor " & _
                      " ,bgbudgetcodesfundsource_tran.fundprojectcodeunkid " & _
                      " ,ISNULL(bgfundprojectcode_master.fundprojectname,'') AS Project " & _
                      " ,bgbudgetcodesfundsource_tran.fundactivityunkid " & _
                      " ,ISNULL(bgfundactivity_tran.activity_name,'') AS Activity " & _
                      " ,bgbudgetcodesfundsource_tran.percentage " & _
                      " ,((@shifthrs *  bgbudgetcodesfundsource_tran.percentage) / 100) /60 As AssignedActivityHrsInMin " & _
                      " ,0 AS  emptimesheetunkid " & _
                      ", '' AS submission_remark "

            'Pinkal (28-Jul-2018) -- 'Enhancement - Doing for PACT Budget Timesheet Requirment [Ref #249,250,251][", '' AS submission_remark "]


            'Sohail (03 May 2017) - [bgbudget_tran.allocationtranunkid = bgbudgetcodes_tran.allocationtranunkid]

            'Pinkal (13-Oct-2017) --  Ref Id 62 Working on Global Budget Timesheet Change.[" ,ISNULL(ltbemployee_timesheet.emptimesheetunkid,0) AS  emptimesheetunkid " & _]

            'Nilay (02-Jan-2017) -- Start
            'Issue #33: Enhancement - Implementing Budget Employee Time Sheet
            If xEmpTimesheetID > 0 Then
                strQ &= ",  ISNULL(RIGHT('00' + CONVERT(NVARCHAR(max),CAST(activity_hrs / 60 AS INT)), 2) + ':'  + RIGHT('00'+ CONVERT(VARCHAR(2),  CAST(activity_hrs % 60   AS INT )), 2), '00:00') AS ActivityHours " & _
                            ",  ltbemployee_timesheet.activity_hrs AS ActivityHoursInMin " & _
                            ",  CASE WHEN ISNULL(ltbemployee_timesheet.emptimesheetunkid,0) <= 0  THEN '' ELSE ltbemployee_timesheet.description END AS Description "
            Else
                'Nilay (21 Mar 2017) -- Start
                'ISSUE #23: Enhancements: Budget Employee TimeSheet Changes
                If blnAllowActivityHoursByPercentage = True Then
                strQ &= ",  ISNULL(RIGHT('00' + CONVERT(NVARCHAR(max),CAST(((@shifthrs *  bgbudgetcodesfundsource_tran.percentage) / 100) / 3600 AS INT)), 2) + ':' " & _
                                "   + RIGHT('00'+ CONVERT(VARCHAR(2),  CAST(((@shifthrs *  bgbudgetcodesfundsource_tran.percentage) / 100) % 3600  / 60 AS DECIMAL )), 2), '00:00') AS ActivityHours " & _
                            ",   ((@shifthrs *  bgbudgetcodesfundsource_tran.percentage) / 100) /60 AS ActivityHoursInMin " & _
                            ", '' AS Description "
                Else
                    strQ &= ", '00:00' AS ActivityHours " & _
                            ", 0 AS ActivityHoursInMin " & _
                            ", '' AS Description "
                End If
                'Nilay (21 Mar 2017) -- End

            End If
            'Nilay (02-Jan-2017) -- End

            'Pinkal (28-Mar-2018) -- Start
            'Enhancement - (RefNo: 198)  Working on Project Targeted/Acutal Hours Details.
            '",  ISNULL(RIGHT('00' + CONVERT(NVARCHAR(max),CAST(((@shifthrs *  bgbudgetcodesfundsource_tran.percentage) / 100) / 3600 AS INT)), 2) + ':' " & _
            '"   + RIGHT('00'+ CONVERT(VARCHAR(2),  CAST(((@shifthrs *  bgbudgetcodesfundsource_tran.percentage) / 100) % 3600  / 60 AS INT )), 2), '00:00') AS ActivityHours " & _
            'Pinkal (28-Mar-2018) -- End


            'Sohail (03 May 2017) -- Start
            'Enhancement - 66.1 - Allow to include newly hired employees on budget codes screen and allow to map employees with planned employees.
            'strQ &= " FROM bgbudgetcodesfundsource_tran " & _
            '          " LEFT JOIN bgbudget_tran ON bgbudgetcodesfundsource_tran.budgettranunkid =  bgbudget_tran.budgettranunkid  " & _
            '          " LEFT JOIN bgbudget_master ON bgbudget_master.budgetunkid=bgbudget_tran.budgetunkid  " & _
            '          " LEFT JOIN bgfundprojectcode_master ON bgfundprojectcode_master.fundprojectcodeunkid = bgbudgetcodesfundsource_tran.fundprojectcodeunkid " & _
            '          " LEFT JOIN bgfundsource_master ON bgfundsource_master.fundsourceunkid = bgfundprojectcode_master.fundsourceunkid " & _
            '          " LEFT JOIN bgfundactivity_tran ON bgfundactivity_tran.fundactivityunkid = bgbudgetcodesfundsource_tran.fundactivityunkid " & _
            '          " LEFT JOIN ltbemployee_timesheet ON ltbemployee_timesheet.periodunkid = bgbudgetcodesfundsource_tran.periodunkid " & _
            '          " AND ltbemployee_timesheet.employeeunkid = bgbudget_tran.allocationtranunkid AND ltbemployee_timesheet.activityunkid= bgbudgetcodesfundsource_tran.fundactivityunkid " & _
            '          " AND ltbemployee_timesheet.isvoid = 0 AND convert(char(8),ltbemployee_timesheet.activitydate,112) = @activitydate " & _
            '          " WHERE bgbudgetcodesfundsource_tran.isvoid = 0 And bgbudget_tran.isvoid = 0 And bgbudget_master.isvoid = 0 And bgbudgetcodesfundsource_tran.percentage > 0 " & _
            '          " AND bgbudget_master.allocationbyid = 0 AND  bgbudgetcodesfundsource_tran.periodunkid = @periodId and bgbudget_tran.allocationtranunkid = @EmpID " & _
            '          " AND bgbudget_master.isdefault = 1 "
            strQ &= " FROM bgbudgetcodesfundsource_tran " & _
                      " LEFT JOIN bgbudgetcodes_tran ON bgbudgetcodesfundsource_tran.budgetcodestranunkid =  bgbudgetcodes_tran.budgetcodestranunkid  " & _
                      " LEFT JOIN bgbudget_master ON bgbudget_master.budgetunkid = bgbudgetcodes_tran.budgetunkid  " & _
                      " LEFT JOIN bgfundprojectcode_master ON bgfundprojectcode_master.fundprojectcodeunkid = bgbudgetcodesfundsource_tran.fundprojectcodeunkid " & _
                      " LEFT JOIN bgfundsource_master ON bgfundsource_master.fundsourceunkid = bgfundprojectcode_master.fundsourceunkid " & _
                      " LEFT JOIN bgfundactivity_tran ON bgfundactivity_tran.fundactivityunkid = bgbudgetcodesfundsource_tran.fundactivityunkid " & _
                      " LEFT JOIN ltbemployee_timesheet ON ltbemployee_timesheet.periodunkid = bgbudgetcodesfundsource_tran.periodunkid " & _
                      " AND ltbemployee_timesheet.employeeunkid = bgbudgetcodes_tran.allocationtranunkid AND ltbemployee_timesheet.activityunkid= bgbudgetcodesfundsource_tran.fundactivityunkid " & _
                      " AND ltbemployee_timesheet.isvoid = 0 AND convert(char(8),ltbemployee_timesheet.activitydate,112) BETWEEN @fromdate AND @todate " & _
                      " WHERE bgbudgetcodesfundsource_tran.isvoid = 0 And bgbudgetcodes_tran.isvoid = 0 And bgbudget_master.isvoid = 0 And bgbudgetcodesfundsource_tran.percentage > 0 " & _
                      " AND bgbudget_master.allocationbyid = 0 AND  bgbudgetcodesfundsource_tran.periodunkid = @periodId and bgbudgetcodes_tran.allocationtranunkid = @EmpID " & _
                      " AND bgbudget_master.isdefault = 1 "
            'Sohail (03 May 2017) -- End





            If xEmpTimesheetID > 0 Then
                strQ &= " AND ltbemployee_timesheet.emptimesheetunkid = @emptimesheetunkid "
                objDataOperation.AddParameter("@emptimesheetunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, xEmpTimesheetID)
            End If

            objDataOperation.AddParameter("@periodId", SqlDbType.Int, eZeeDataType.INT_SIZE, xPeriodId)
            objDataOperation.AddParameter("@EmpID", SqlDbType.Int, eZeeDataType.INT_SIZE, xEmployeeId)

            'Pinkal (13-Oct-2017) -- Start
            'Enhancement - Working on B5 Plus TnA Enhancement for Overtime(Extra Hrs).
            'objDataOperation.AddParameter("@activitydate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(xActivitydate))
            objDataOperation.AddParameter("@fromdate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(xFromDate))
            objDataOperation.AddParameter("@todate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(xToDate))
            'Pinkal (13-Oct-2017) -- End


            objDataOperation.AddParameter("@shifthrs", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mintShiftHrsinSec)
            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetEmployeeAssignActivities; Module Name: " & mstrModuleName)
        End Try
        Return dsList
    End Function
    'Pinkal (06-Dec-2016) -- End



	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Public Shared Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "WEB")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class
