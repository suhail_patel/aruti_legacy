﻿'************************************************************************************************************************************
'Class Name : clsBudgetcodes_Tran.vb
'Purpose    :
'Date       :25/07/2016
'Written By :Sohail
'Modified   :
'************************************************************************************************************************************

Imports eZeeCommonLib
Imports eZee.Common.eZeeForm
Imports System.ComponentModel

''' <summary>
''' Purpose: 
''' Developer: Sohail
''' </summary>
Public Class clsBudgetcodes_Tran
    Private Shared ReadOnly mstrModuleName As String = "clsBudgetcodes_Tran"
    Dim objDataOperation As clsDataOperation
    Dim mstrMessage As String = ""

#Region " Private variables "
    Private mintBudgetcodestranunkid As Integer
    Private mintBudgetunkid As Integer
    Private mintPeriodUnkId As Integer
    Private mintBudgettranunkid As Integer
    Private mdecAmount As Decimal
    Private mdecBudgetamount As Decimal
    Private mdecBudgetsalaryamount As Decimal
    Private mdecBudgetotherpayrollamount As Decimal
    Private mintUserunkid As Integer
    Private mblnIsvoid As Boolean
    Private mintVoiduserunkid As Integer
    Private mdtVoiddatetime As Date
    Private mstrVoidreason As String = String.Empty
    'Sohail (03 May 2017) -- Start
    'Enhancement - 66.1 - Allow to include newly hired employees on budget codes screen and allow to map employees with planned employees.
    Private mintAllocationtranunkid As Integer
    Private mintTranheadunkid As Integer
    Private mintJobUnkId As Integer = 0
    'Sohail (03 May 2017) -- End
    'Sohail (13 Oct 2017) -- Start
    'CCBRT Enhancement - 70.1 - (Ref. Id - 110, 111) - for all Four reports the user to be able to select a range of consecutive periods so as to be able to see the total. This Feature will enable the user to select more than one default budget so as to get a consolidated report.
    Private mintBudgetcodesunkid As Integer = 0
    'Sohail (13 Oct 2017) -- End


    'Private mstrWebFormName As String = String.Empty
    'Private mstrWebIP As String = ""
    'Private mstrWebHost As String = ""
#End Region

#Region " Properties "

'S.SANDEEP [28-May-2018] -- START 
'ISSUE/ENHANCEMENT : {Audit Trails} 
Private mstrFormName As String = String.Empty
Public WriteOnly Property _FormName() As String 
Set(ByVal value As String) 
mstrFormName = value 
End Set 
End Property 
 
Private mstrClientIP As String = ""
Public WriteOnly Property _ClientIP() As String 
Set(ByVal value As String) 
mstrClientIP = value 
End Set 
End Property 
 
Private mstrHostName As String = ""
Public WriteOnly Property _HostName() As String 
Set(ByVal value As String) 
mstrHostName = value 
End Set 
End Property 
 
Private mblnIsWeb As Boolean = False
Public WriteOnly Property _FromWeb() As Boolean 
Set(ByVal value As Boolean) 
mblnIsWeb = value 
End Set 
End Property 
 
    Private mintAuditUserId As Integer = 0
    Public WriteOnly Property _AuditUserId() As Integer
        Set(ByVal value As Integer)
            mintAuditUserId = value
        End Set
    End Property

    Private mintCompanyUnkid As Integer = 0
    Public WriteOnly Property _CompanyUnkid() As Integer
        Set(ByVal value As Integer)
            mintCompanyUnkid = value
        End Set
    End Property
 
Private mdtAuditDate As DateTime = Now
Public WriteOnly Property _AuditDate() As DateTime 
Set(ByVal value As DateTime) 
mdtAuditDate = value 
End Set 
End Property 
 
Private xDataOpr As clsDataOperation = Nothing
Public Property _xDataOpr() As clsDataOperation 
Get 
Return xDataOpr 
End Get 
Set(ByVal value As clsDataOperation) 
xDataOpr = value 
End Set 
End Property 
 
Private mintLoginEmployeeunkid As Integer = 0
Public Property _LoginEmployeeunkid() As Integer 
Get 
Return mintLoginEmployeeunkid 
End Get 
Set(ByVal value As Integer) 
mintLoginEmployeeunkid = value 
End Set 
End Property 
 
Private mintVoidlogingemployeeunkid As Integer=0
Public Property _Voidloginemployeeunkid() As Integer 
Get 
Return mintVoidlogingemployeeunkid 
End Get 
Set(ByVal value As Integer) 
mintVoidlogingemployeeunkid = value 
End Set 
End Property 
'S.SANDEEP [28-May-2018] -- END 

    Public ReadOnly Property _Message() As String
        Get
            Return mstrMessage
        End Get
    End Property

    ''' <summary>
    ''' Purpose: Get or Set budgetcodestranunkid
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Budgetcodestranunkid() As Integer
        Get
            Return mintBudgetcodestranunkid
        End Get
        Set(ByVal value As Integer)
            mintBudgetcodestranunkid = Value
            Call getData()
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set budgetunkid
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Budgetunkid() As Integer
        Get
            Return mintBudgetunkid
        End Get
        Set(ByVal value As Integer)
            mintBudgetunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set periodunkid
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _PeriodUnkId() As Integer
        Get
            Return mintPeriodUnkId
        End Get
        Set(ByVal value As Integer)
            mintPeriodUnkId = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set budgettranunkid
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Budgettranunkid() As Integer
        Get
            Return mintBudgettranunkid
        End Get
        Set(ByVal value As Integer)
            mintBudgettranunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set amount
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Amount() As Decimal
        Get
            Return mdecAmount
        End Get
        Set(ByVal value As Decimal)
            mdecAmount = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set budgetamount
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Budgetamount() As Integer
        Get
            Return mdecBudgetamount
        End Get
        Set(ByVal value As Integer)
            mdecBudgetamount = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set budgetsalaryamount
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Budgetsalaryamount() As Integer
        Get
            Return mdecBudgetsalaryamount
        End Get
        Set(ByVal value As Integer)
            mdecBudgetsalaryamount = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set budgetotherpayrollamount
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Budgetotherpayrollamount() As Decimal
        Get
            Return mdecBudgetotherpayrollamount
        End Get
        Set(ByVal value As Decimal)
            mdecAmount = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set userunkid
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Userunkid() As Integer
        Get
            Return mintUserunkid
        End Get
        Set(ByVal value As Integer)
            mintUserunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isvoid
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Isvoid() As Boolean
        Get
            Return mblnIsvoid
        End Get
        Set(ByVal value As Boolean)
            mblnIsvoid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiduserunkid
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Voiduserunkid() As Integer
        Get
            Return mintVoiduserunkid
        End Get
        Set(ByVal value As Integer)
            mintVoiduserunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiddatetime
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Voiddatetime() As Date
        Get
            Return mdtVoiddatetime
        End Get
        Set(ByVal value As Date)
            mdtVoiddatetime = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voidreason
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Voidreason() As String
        Get
            Return mstrVoidreason
        End Get
        Set(ByVal value As String)
            mstrVoidreason = Value
        End Set
    End Property

    'Public WriteOnly Property _WebFormName() As String
    '    Set(ByVal value As String)
    '        mstrWebFormName = value
    '    End Set
    'End Property

    'Public WriteOnly Property _WebIP() As String
    '    Set(ByVal value As String)
    '        mstrWebIP = value
    '    End Set
    'End Property

    'Public WriteOnly Property _WebHost() As String
    '    Set(ByVal value As String)
    '        mstrWebHost = value
    '    End Set
    'End Property

    'Sohail (03 May 2017) -- Start
    'Enhancement - 66.1 - Allow to include newly hired employees on budget codes screen and allow to map employees with planned employees.
    ''' <summary>
    ''' Purpose: Get or Set allocationtranunkid
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Allocationtranunkid() As Integer
        Get
            Return mintAllocationtranunkid
        End Get
        Set(ByVal value As Integer)
            mintAllocationtranunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set tranheadunkid
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Tranheadunkid() As Integer
        Get
            Return mintTranheadunkid
        End Get
        Set(ByVal value As Integer)
            mintTranheadunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set tranheadunkid
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _JobUnkId() As Integer
        Get
            Return mintJobUnkId
        End Get
        Set(ByVal value As Integer)
            mintJobUnkId = value
        End Set
    End Property
    'Sohail (03 May 2017) -- End

    'Sohail (13 Oct 2017) -- Start
    'CCBRT Enhancement - 70.1 - (Ref. Id - 110, 111) - for all Four reports the user to be able to select a range of consecutive periods so as to be able to see the total. This Feature will enable the user to select more than one default budget so as to get a consolidated report.
    Public Property _Budgetcodesunkid() As Integer
        Get
            Return mintBudgetcodesunkid
        End Get
        Set(ByVal value As Integer)
            mintBudgetcodesunkid = value

        End Set
    End Property
    'Sohail (13 Oct 2017) -- End

#End Region

    ''' <summary>
    ''' Modify By: Sohail
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Sub GetData(Optional ByVal xDataOpr As clsDataOperation = Nothing)
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        If xDataOpr Is Nothing Then
        objDataOperation = New clsDataOperation
        Else
            objDataOperation = xDataOpr
        End If

        Try
            strQ = "SELECT " & _
              "  budgetcodestranunkid " & _
              ", budgetunkid " & _
              ", periodunkid " & _
              ", budgettranunkid " & _
              ", amount " & _
              ", budgetamount " & _
              ", budgetsalaryamount " & _
              ", budgetotherpayrollamount " & _
              ", userunkid " & _
              ", isvoid " & _
              ", voiduserunkid " & _
              ", voiddatetime " & _
              ", voidreason " & _
              ", allocationtranunkid " & _
              ", tranheadunkid " & _
              ", jobunkid " & _
              ", ISNULL(budgetcodesunkid, 0) AS budgetcodesunkid " & _
             "FROM bgbudgetcodes_tran " & _
             "WHERE budgetcodestranunkid = @budgetcodestranunkid "
            'Sohail (13 Oct 2017) - [budgetcodesunkid]
            'Sohail (03 May 2017) - [allocationtranunkid, tranheadunkid, jobunkid]

            objDataOperation.AddParameter("@budgetcodestranunkid", SqlDbType.int, eZeeDataType.INT_SIZE, mintBudgetcodesTranUnkId.ToString)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For Each dtRow As DataRow In dsList.Tables(0).Rows
                mintbudgetcodestranunkid = CInt(dtRow.Item("budgetcodestranunkid"))
                mintBudgetunkid = CInt(dtRow.Item("budgetunkid"))
                mintPeriodUnkId = CInt(dtRow.Item("periodunkid"))
                mintbudgettranunkid = CInt(dtRow.Item("budgettranunkid"))
                mdecBudgetamount = CInt(dtRow.Item("allocationtranunkid"))
                mdecBudgetsalaryamount = CInt(dtRow.Item("tranheadunkid"))
                mdecAmount = dtRow.Item("amount")
                mintuserunkid = CInt(dtRow.Item("userunkid"))
                mblnisvoid = CBool(dtRow.Item("isvoid"))
                mintvoiduserunkid = CInt(dtRow.Item("voiduserunkid"))
                If IsDBNull(dtRow.Item("voiddatetime")) = True Then
                    mdtVoiddatetime = Nothing
                Else
                    mdtVoiddatetime = dtRow.Item("voiddatetime")
                End If
                mstrVoidreason = dtRow.Item("voidreason").ToString
                'Sohail (03 May 2017) -- Start
                'Enhancement - 66.1 - Allow to include newly hired employees on budget codes screen and allow to map employees with planned employees.
                mintAllocationtranunkid = CInt(dtRow.Item("allocationtranunkid"))
                mintTranheadunkid = CInt(dtRow.Item("tranheadunkid"))
                mintJobUnkId = CInt(dtRow.Item("jobunkid"))
                'Sohail (03 May 2017) -- End
                'Sohail (13 Oct 2017) -- Start
                'CCBRT Enhancement - 70.1 - (Ref. Id - 110, 111) - for all Four reports the user to be able to select a range of consecutive periods so as to be able to see the total. This Feature will enable the user to select more than one default budget so as to get a consolidated report.
                mintBudgetcodesunkid = CInt(dtRow.Item("budgetcodesunkid"))
                'Sohail (13 Oct 2017) -- End
                Exit For
            Next
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetData; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xDataOpr Is Nothing Then objDataOperation = Nothing
        End Try
    End Sub


    ''' <summary>
    ''' Modify By: Sohail
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function GetList(ByVal strTableName As String) As DataSet
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "SELECT " & _
              "  budgetcodestranunkid " & _
              ", budgetunkid " & _
              ", budgettranunkid " & _
              ", amount " & _
              ", budgetamount " & _
              ", budgetsalaryamount " & _
              ", budgetotherpayrollamount " & _
              ", userunkid " & _
              ", isvoid " & _
              ", voiduserunkid " & _
              ", voiddatetime " & _
              ", voidreason " & _
              ", allocationtranunkid " & _
              ", tranheadunkid " & _
              ", jobunkid " & _
              ", ISNULL(budgetcodesunkid, 0) AS budgetcodesunkid " & _
             "FROM bgbudgetcodes_tran " & _
             "WHERE isvoid = 0 "
            'Sohail (13 Oct 2017) - [budgetcodesunkid]
            'Sohail (03 May 2017) - [allocationtranunkid, tranheadunkid, jobunkid]

            dsList = objDataOperation.ExecQuery(strQ, strTableName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
        Return dsList
    End Function


    ''' <summary>
    ''' Modify By: Sohail
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> INSERT INTO Database Table (bgbudgetcodes_tran) </purpose>
    Public Function Insert(ByVal dtCurrentDateAndTime As Date, Optional ByVal xDataOpr As clsDataOperation = Nothing) As Boolean
        'If isExist(mstrName) Then
        '    mstrMessage = "<Message>"
        '    Return False
        'End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        If xDataOpr Is Nothing Then
            objDataOperation = New clsDataOperation
            objDataOperation.BindTransaction()
        Else
            objDataOperation = xDataOpr
        End If
        objDataOperation.ClearParameters()

        Try
            objDataOperation.AddParameter("@budgetunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintBudgetunkid.ToString)
            objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPeriodUnkId.ToString)
            objDataOperation.AddParameter("@budgettranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintBudgettranunkid.ToString)
            objDataOperation.AddParameter("@amount", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecAmount.ToString)
            objDataOperation.AddParameter("@budgetamount", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecBudgetamount.ToString)
            objDataOperation.AddParameter("@budgetsalaryamount", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecBudgetsalaryamount.ToString)
            objDataOperation.AddParameter("@budgetotherpayrollamount", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecBudgetotherpayrollamount.ToString)
            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
            If mdtVoiddatetime = Nothing Then
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            Else
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime.ToString)
            End If
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)
            'Sohail (03 May 2017) -- Start
            'Enhancement - 66.1 - Allow to include newly hired employees on budget codes screen and allow to map employees with planned employees.
            objDataOperation.AddParameter("@allocationtranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAllocationtranunkid.ToString)
            objDataOperation.AddParameter("@tranheadunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintTranheadunkid.ToString)
            objDataOperation.AddParameter("@jobunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintJobUnkId.ToString)
            'Sohail (03 May 2017) -- End
            objDataOperation.AddParameter("@budgetcodesunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintBudgetcodesunkid.ToString) 'Sohail (13 Oct 2017)


            strQ = "INSERT INTO bgbudgetcodes_tran ( " & _
              "  budgetunkid " & _
              ", periodunkid " & _
              ", budgettranunkid " & _
              ", amount " & _
              ", budgetamount " & _
              ", budgetsalaryamount " & _
              ", budgetotherpayrollamount " & _
              ", userunkid " & _
              ", isvoid " & _
              ", voiduserunkid " & _
              ", voiddatetime " & _
              ", voidreason" & _
              ", allocationtranunkid " & _
              ", tranheadunkid " & _
              ", jobunkid " & _
              ", budgetcodesunkid " & _
            ") VALUES (" & _
              "  @budgetunkid " & _
              ", @periodunkid " & _
              ", @budgettranunkid " & _
              ", @amount " & _
              ", @budgetamount " & _
              ", @budgetsalaryamount " & _
              ", @budgetotherpayrollamount " & _
              ", @userunkid " & _
              ", @isvoid " & _
              ", @voiduserunkid " & _
              ", @voiddatetime " & _
              ", @voidreason" & _
              ", @allocationtranunkid " & _
              ", @tranheadunkid " & _
              ", @jobunkid " & _
              ", @budgetcodesunkid " & _
            "); SELECT @@identity"
            'Sohail (13 Oct 2017) - [budgetcodesunkid]

            dsList = objDataOperation.ExecQuery(strQ, "List")
            'Sohail (03 May 2017) - [allocationtranunkid, tranheadunkid, jobunkid]

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            mintBudgetcodestranunkid = dsList.Tables(0).Rows(0).Item(0)

            If InsertAuditBudgetCodeTran(objDataOperation, enAuditType.ADD, dtCurrentDateAndTime) = False Then
                If xDataOpr Is Nothing Then
                    objDataOperation.ReleaseTransaction(False)
                End If
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If xDataOpr Is Nothing Then
                objDataOperation.ReleaseTransaction(True)
            End If

            Return True
        Catch ex As Exception
            If xDataOpr Is Nothing Then
                objDataOperation.ReleaseTransaction(False)
            End If
            Throw New Exception(ex.Message & "; Procedure Name: Insert; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xDataOpr Is Nothing Then
                objDataOperation = Nothing
            End If
        End Try
    End Function

    Public Function InsertAll(ByVal objDataOp As clsDataOperation, ByVal dtTable As DataTable, ByVal dtCurrentDateTime As Date, Optional ByVal bw As BackgroundWorker = Nothing, Optional ByRef intProgress As Integer = 0) As Boolean
        'Sohail (01 May 2021) - [bw, intProgress]
        Dim objBudgetCodesFundSource As New clsBudgetCodesfundsource_Tran
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        intProgress = 0 'Sohail (01 May 2021)

        objDataOp.ClearParameters()

        Try

            For Each dtRow As DataRow In dtTable.Rows

                mintBudgetcodestranunkid = CInt(dtRow.Item("budgetcodestranunkid"))
                mintBudgettranunkid = CInt(dtRow.Item("budgettranunkid"))
                'Sohail (03 May 2017) -- Start
                'Enhancement - 66.1 - Allow to include newly hired employees on budget codes screen and allow to map employees with planned employees.
                mintAllocationtranunkid = CInt(dtRow.Item("allocationtranunkid"))
                mintJobUnkId = CInt(dtRow.Item("jobunkid"))
                'Sohail (03 May 2017) -- End

                If dtTable.Columns.Contains("tranheadunkid") = True Then
                    mintTranheadunkid = CInt(dtRow.Item("tranheadunkid")) 'Sohail (03 May 2017)
                    mdecAmount = CDec(dtRow.Item("pramount"))
                    'mdecSalaryamount = 0
                    'mdecOtherpayrollamount = 0
                Else
                    mintTranheadunkid = -1 'Sohail (03 May 2017)
                    mdecAmount = 0
                    'mdecSalaryamount = CDec(dtRow.Item("_|1"))
                    'mdecOtherpayrollamount = CDec(dtRow.Item("_|2"))
                End If
                mdecBudgetamount = CDec(dtRow.Item("budgetamount"))
                mdecBudgetsalaryamount = CDec(dtRow.Item("budgetsalaryamount"))
                mdecBudgetotherpayrollamount = CDec(dtRow.Item("budgetotherpayrollamount"))

                'If mintBudgetcodestranunkid <= 0 Then
                    If Insert(dtCurrentDateTime, objDataOp) = False Then
                        objDataOp.ReleaseTransaction(False)
                        Return False
                    End If
                'Else
                '    If Update(dtCurrentDateTime, objDataOp) = False Then
                '        objDataOp.ReleaseTransaction(False)
                '        Return False
                '    End If
                'End If

                objBudgetCodesFundSource._Budgettranunkid = mintBudgettranunkid
                objBudgetCodesFundSource._PeriodUnkId = mintPeriodUnkId
                objBudgetCodesFundSource._Userunkid = mintUserunkid
                objBudgetCodesFundSource._Isvoid = mblnIsvoid
                objBudgetCodesFundSource._Voiduserunkid = mintVoiduserunkid
                objBudgetCodesFundSource._Voiddatetime = mdtVoiddatetime
                objBudgetCodesFundSource._Voidreason = mstrVoidreason
                'Sohail (03 May 2017) -- Start
                'Enhancement - 66.1 - Allow to include newly hired employees on budget codes screen and allow to map employees with planned employees.
                objBudgetCodesFundSource._Budgetcodestranunkid = mintBudgetcodestranunkid
                'Sohail (03 May 2017) -- End
                
                With objBudgetCodesFundSource
                    ._FormName = mstrFormName
                    ._LoginEmployeeunkid = mintLoginEmployeeunkid
                    ._ClientIP = mstrClientIP
                    ._HostName = mstrHostName
                    ._FromWeb = mblnIsWeb
                    ._AuditUserId = mintAuditUserId
._CompanyUnkid = mintCompanyUnkid
                    ._AuditDate = mdtAuditDate
                End With
If objBudgetCodesFundSource.InsertAll(dtRow, objDataOp, dtCurrentDateTime) = False Then
                    objDataOp.ReleaseTransaction(False)
                    Return False
                End If

                'Sohail (01 May 2021) -- Start
                'IHI Issue : : Project codes are missing for some employees on editong budget codes.
                If bw IsNot Nothing Then
                    bw.ReportProgress(intProgress + 1)
                End If

                intProgress += 1
                'Sohail (01 May 2021) -- End
            Next

            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: InsertAll; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sohail
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Update Database Table (bgbudgetcodes_tran) </purpose>
    Public Function Update(ByVal dtCurrentDateAndTime As Date, Optional ByVal xDataOpr As clsDataOperation = Nothing) As Boolean
        'If isExist(mstrName, mintBudgetcodestranunkid) Then
        '    mstrMessage = "<Message>"
        '    Return False
        'End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        If xDataOpr Is Nothing Then
            objDataOperation = New clsDataOperation
            objDataOperation.BindTransaction()
        Else
            objDataOperation = xDataOpr
        End If
        objDataOperation.ClearParameters()

        Try
            objDataOperation.AddParameter("@budgetcodestranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintBudgetcodestranunkid.ToString)
            objDataOperation.AddParameter("@budgetunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintBudgetunkid.ToString)
            objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPeriodUnkId.ToString)
            objDataOperation.AddParameter("@budgettranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintBudgettranunkid.ToString)
            objDataOperation.AddParameter("@amount", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecAmount.ToString)
            objDataOperation.AddParameter("@budgetamount", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecBudgetamount.ToString)
            objDataOperation.AddParameter("@budgetsalaryamount", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecBudgetsalaryamount.ToString)
            objDataOperation.AddParameter("@budgetotherpayrollamount", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecBudgetotherpayrollamount.ToString)
            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
            If mdtVoiddatetime = Nothing Then
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            Else
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime.ToString)
            End If
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)
            'Sohail (03 May 2017) -- Start
            'Enhancement - 66.1 - Allow to include newly hired employees on budget codes screen and allow to map employees with planned employees.
            objDataOperation.AddParameter("@allocationtranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAllocationtranunkid.ToString)
            objDataOperation.AddParameter("@tranheadunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintTranheadunkid.ToString)
            objDataOperation.AddParameter("@jobunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintJobUnkId.ToString)
            'Sohail (03 May 2017) -- End
            objDataOperation.AddParameter("@budgetcodesunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintBudgetcodesunkid.ToString) 'Sohail (13 Oct 2017)

            strQ = "UPDATE   bgbudgetcodes_tran SET " & _
                          "  budgetunkid = @budgetunkid" & _
                          ", periodunkid = @periodunkid" & _
                          ", budgettranunkid = @budgettranunkid" & _
                          ", amount = @amount" & _
                          ", budgetamount = @budgetamount" & _
                          ", budgetsalaryamount = @budgetsalaryamount" & _
                          ", budgetotherpayrollamount = @budgetotherpayrollamount" & _
                          ", userunkid = @userunkid" & _
                          ", isvoid = @isvoid" & _
                          ", voiduserunkid = @voiduserunkid" & _
                          ", voiddatetime = @voiddatetime" & _
                          ", voidreason = @voidreason " & _
                          ", allocationtranunkid = @allocationtranunkid" & _
                          ", tranheadunkid = @tranheadunkid" & _
                          ", jobunkid = @jobunkid" & _
                          ", budgetcodesunkid = @budgetcodesunkid " & _
            " WHERE budgetcodestranunkid = @budgetcodestranunkid "
            'Sohail (13 Oct 2017) - [budgetcodesunkid]
            'Sohail (03 May 2017) - [allocationtranunkid, tranheadunkid, jobunkid]

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If IsTableDataUpdate(mintBudgetcodestranunkid, objDataOperation) = False Then
            If InsertAuditBudgetCodeTran(objDataOperation, enAuditType.EDIT, dtCurrentDateAndTime) = False Then
                If xDataOpr Is Nothing Then
                    objDataOperation.ReleaseTransaction(False)
                End If
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If
            End If


            If xDataOpr Is Nothing Then
                objDataOperation.ReleaseTransaction(True)
            End If

            Return True
        Catch ex As Exception
            If xDataOpr Is Nothing Then
                objDataOperation.ReleaseTransaction(False)
            End If
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xDataOpr Is Nothing Then
                objDataOperation = Nothing
            End If
        End Try
    End Function

    'Sohail (06 May 2017) -- Start
    'MST Enhancement - 66.1 - Allow to map employees with planned employees in payroll budget.
    Public Function UpdatePlannedEmployees(ByVal intBudgetUnkId As Integer, ByVal intPeriodUnkId As Integer, ByVal dtTable As DataTable, ByVal dtCurrentDateAndTime As Date, Optional ByVal xDataOpr As clsDataOperation = Nothing) As Boolean
        'If isExist(mstrName, mintBudgettranunkid) Then
        '    mstrMessage = "<Message>"
        '    Return False
        'End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        If xDataOpr Is Nothing Then
            objDataOperation = New clsDataOperation
            objDataOperation.BindTransaction()
        Else
            objDataOperation = xDataOpr
        End If

        Try

            For Each dtRow As DataRow In dtTable.Rows
                objDataOperation.ClearParameters()

                mintBudgetunkid = intBudgetUnkId
                mintPeriodUnkId = intPeriodUnkId
                mintJobUnkId = CInt(dtRow.Item("jobunkid"))
                mintAllocationtranunkid = CInt(dtRow.Item("allocationtranunkid"))

                strQ = "SELECT  bgbudgetcodes_tran.budgetcodestranunkid " & _
                        "FROM    bgbudgetcodes_tran " & _
                        "WHERE   bgbudgetcodes_tran.isvoid = 0 " & _
                                "AND bgbudgetcodes_tran.budgetunkid = @budgetunkid " & _
                                "AND bgbudgetcodes_tran.periodunkid = @periodunkid " & _
                                "AND bgbudgetcodes_tran.jobunkid = @jobunkid " & _
                                "AND bgbudgetcodes_tran.allocationtranunkid = @allocationtranunkid "

                objDataOperation.AddParameter("@budgetunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintBudgetunkid.ToString)
                objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPeriodUnkId.ToString)
                objDataOperation.AddParameter("@jobunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintJobUnkId.ToString)
                objDataOperation.AddParameter("@allocationtranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAllocationtranunkid.ToString)
                objDataOperation.AddParameter("@budgetcodesunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintBudgetcodesunkid.ToString) 'Sohail (13 Oct 2017)

                dsList = objDataOperation.ExecQuery(strQ, "List")

                If objDataOperation.ErrorMessage <> "" Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If

                For Each dsRow As DataRow In dsList.Tables(0).Rows

                    mintBudgetcodestranunkid = CInt(dsRow.Item("budgetcodestranunkid"))
                    objDataOperation.ClearParameters()
                    Call GetData(objDataOperation)

                    mintAllocationtranunkid = CInt(dtRow.Item("employeeunkid"))

                    objDataOperation.ClearParameters()
                    objDataOperation.AddParameter("@budgetcodestranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintBudgetcodestranunkid.ToString)
                    objDataOperation.AddParameter("@allocationtranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAllocationtranunkid.ToString)

                    strQ = "UPDATE bgbudgetcodes_tran SET " & _
                              "  allocationtranunkid = @allocationtranunkid " & _
                              ", jobunkid = jobunkid * -1 " & _
                            "WHERE budgetcodestranunkid = @budgetcodestranunkid "

                    Call objDataOperation.ExecNonQuery(strQ)

                    If objDataOperation.ErrorMessage <> "" Then
                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If

                    If InsertAuditBudgetCodeTran(objDataOperation, enAuditType.EDIT, dtCurrentDateAndTime) = False Then
                        If xDataOpr Is Nothing Then
                            objDataOperation.ReleaseTransaction(False)
                        End If
                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If

                Next

            Next

            If xDataOpr Is Nothing Then
                objDataOperation.ReleaseTransaction(True)
            End If

            Return True
        Catch ex As Exception
            If xDataOpr Is Nothing Then
                objDataOperation.ReleaseTransaction(False)
            End If
            Throw New Exception(ex.Message & "; Procedure Name: UpdatePlannedEmployees; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xDataOpr Is Nothing Then
                objDataOperation = Nothing
            End If
        End Try
    End Function
    'Sohail (06 May 2017) -- End

    ''' <summary>
    ''' Modify By: Sohail
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Void Database Table (bgbudgetcodes_tran) </purpose>
    Public Function Void(ByVal intUnkid As Integer, ByVal dtCurrentDateAndTime As Date, Optional ByVal xDataOpr As clsDataOperation = Nothing) As Boolean
        'If isUsed(intUnkid) Then
        '    mstrMessage = "<Message>"
        '    Return False
        'End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        If xDataOpr Is Nothing Then
            objDataOperation = New clsDataOperation
            objDataOperation.BindTransaction()
        Else
            objDataOperation = xDataOpr
        End If
        objDataOperation.ClearParameters()

        Try

            objDataOperation.AddParameter("@budgetcodestranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid.ToString)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
            If mdtVoiddatetime = Nothing Then
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            Else
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime.ToString)
            End If
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)

            strQ = "UPDATE bgbudgetcodes_tran SET " & _
               " isvoid = @isvoid" & _
               ", voiduserunkid = @voiduserunkid" & _
               ", voiddatetime = @voiddatetime" & _
               ", voidreason = @voidreason " & _
             "WHERE budgetcodestranunkid = @budgetcodestranunkid "

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Me._Budgetcodestranunkid = intUnkid

            If InsertAuditBudgetCodeTran(objDataOperation, enAuditType.DELETE, dtCurrentDateAndTime) = False Then
                If xDataOpr Is Nothing Then
                    objDataOperation.ReleaseTransaction(False)
                End If
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If xDataOpr Is Nothing Then
                objDataOperation.ReleaseTransaction(True)
            End If

            Return True
        Catch ex As Exception
            If xDataOpr Is Nothing Then
                objDataOperation.ReleaseTransaction(False)
            End If
            Throw New Exception(ex.Message & "; Procedure Name: Void; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xDataOpr Is Nothing Then
                objDataOperation = Nothing
            End If
        End Try
    End Function

    Public Function VoidAllByBudgetUnkId(ByVal intBudgetUnkid As Integer _
                                         , ByVal intPeriodUnkId As Integer _
                                         , ByVal intVoiduserunkid As Integer _
                                         , ByVal dtVoiddatetime As Date _
                                         , ByVal strVoidreason As String _
                                         , Optional ByVal xDataOpr As clsDataOperation = Nothing _
                                         ) As Boolean
        'If isUsed(intUnkid) Then
        '    mstrMessage = "<Message>"
        '    Return False
        'End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim intRowsAffected As Integer = 0

        If xDataOpr Is Nothing Then
            objDataOperation = New clsDataOperation
            objDataOperation.BindTransaction()
        Else
            objDataOperation = xDataOpr
        End If
        objDataOperation.ClearParameters()

        Try

            'If mstrWebIP.Trim = "" Then mstrWebIP = getIP()
            'If mstrWebHost.Trim = "" Then mstrWebHost = getHostName()
            objDataOperation.AddParameter("@form_name", SqlDbType.NVarChar, 500, mstrFormName)
            objDataOperation.AddParameter("@isweb", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsWeb)

            strQ = "INSERT INTO atbgbudgetcodes_tran ( " & _
                                "  budgetcodestranunkid " & _
                                ", budgettranunkid " & _
                                ", budgetunkid " & _
                                ", periodunkid " & _
                                ", amount " & _
                                ", budgetamount " & _
                                ", budgetsalaryamount " & _
                                ", budgetotherpayrollamount " & _
                                ", allocationtranunkid " & _
                                ", tranheadunkid " & _
                                ", jobunkid " & _
                                ", budgetcodesunkid " & _
                                ", audittype " & _
                                ", audituserunkid " & _
                                ", auditdatetime " & _
                                ", ip " & _
                                ", machine_name " & _
                                ", form_name " & _
                                " " & _
                                " " & _
                                " " & _
                                " " & _
                                " " & _
                                ", isweb " & _
                         ") " & _
                         "SELECT " & _
                                "  budgetcodestranunkid " & _
                                ", budgettranunkid " & _
                                ", budgetunkid " & _
                                ", periodunkid " & _
                                ", amount " & _
                                ", budgetamount " & _
                                ", budgetsalaryamount " & _
                                ", budgetotherpayrollamount " & _
                                ", allocationtranunkid " & _
                                ", tranheadunkid " & _
                                ", jobunkid " & _
                                ", budgetcodesunkid " & _
                                ", 3 " & _
                                ", @voiduserunkid " & _
                                ", @voiddatetime " & _
                                ", '" & mstrClientIP & "' " & _
                                ", '" & mstrHostName & "' " & _
                                ", @form_name " & _
                                " " & _
                                " " & _
                                " " & _
                                " " & _
                                " " & _
                                ", @isweb " & _
                         "FROM bgbudgetcodes_tran " & _
                         "WHERE bgbudgetcodes_tran.isvoid = 0 " & _
                         "AND bgbudgetcodes_tran.budgetunkid = @budgetunkid " & _
                         "AND bgbudgetcodes_tran.periodunkid = @periodunkid "
            'Sohail (13 Oct 2017) - [allocationtranunkid, tranheadunkid, jobunkid, budgetcodesunkid]

            objDataOperation.AddParameter("@budgetunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intBudgetUnkid.ToString)
            objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intPeriodUnkId.ToString)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, True)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intVoiduserunkid.ToString)
            If dtVoiddatetime = Nothing Then
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            Else
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, dtVoiddatetime.ToString)
            End If
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strVoidreason.ToString)

            intRowsAffected = objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                objDataOperation.ReleaseTransaction(False)
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            strQ = "UPDATE bgbudgetcodes_tran SET " & _
               " isvoid = @isvoid " & _
               ", voiduserunkid = @voiduserunkid" & _
               ", voiddatetime = @voiddatetime" & _
               ", voidreason = @voidreason " & _
             "WHERE isvoid = 0 " & _
             "AND budgetunkid = @budgetunkid " & _
             "AND periodunkid = @periodunkid "

            intRowsAffected = objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                objDataOperation.ReleaseTransaction(False)
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If xDataOpr Is Nothing Then
                objDataOperation.ReleaseTransaction(True)
            End If

            Return True
        Catch ex As Exception
            If xDataOpr Is Nothing Then
                objDataOperation.ReleaseTransaction(False)
            End If
            Throw New Exception(ex.Message & "; Procedure Name: VoidAllByBudgetUnkId; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xDataOpr Is Nothing Then
                objDataOperation = Nothing
            End If
        End Try
    End Function

    'Sohail (19 Apr 2017) -- Start
    'MST Enhancement - 66.1 - applying user access filter on payroll budget.
    Public Function VoidAllByBudgetTranUnkId(ByVal strBudgetTranUnkIDs As String _
                                         , ByVal intPeriodUnkId As Integer _
                                         , ByVal intVoiduserunkid As Integer _
                                         , ByVal dtVoiddatetime As Date _
                                         , ByVal strVoidreason As String _
                                         , Optional ByVal xDataOpr As clsDataOperation = Nothing _
                                         ) As Boolean
        'If isUsed(intUnkid) Then
        '    mstrMessage = "<Message>"
        '    Return False
        'End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim intRowsAffected As Integer = 0

        If xDataOpr Is Nothing Then
            objDataOperation = New clsDataOperation
            objDataOperation.BindTransaction()
        Else
            objDataOperation = xDataOpr
        End If
        objDataOperation.ClearParameters()

        Try
            'If mstrWebIP.Trim = "" Then mstrWebIP = getIP()
            'If mstrWebHost.Trim = "" Then mstrWebHost = getHostName()
            objDataOperation.AddParameter("@form_name", SqlDbType.NVarChar, 500, mstrFormName)
            objDataOperation.AddParameter("@isweb", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsWeb)

            strQ = "SELECT budgettranunkid INTO #cteBTran FROM bgbudget_tran WHERE budgettranunkid IN (" & strBudgetTranUnkIDs & "); "

            strQ &= "INSERT INTO atbgbudgetcodes_tran ( " & _
                                "  budgetcodestranunkid " & _
                                ", budgettranunkid " & _
                                ", budgetunkid " & _
                                ", periodunkid " & _
                                ", amount " & _
                                ", budgetamount " & _
                                ", budgetsalaryamount " & _
                                ", budgetotherpayrollamount " & _
                                ", allocationtranunkid " & _
                                ", tranheadunkid " & _
                                ", jobunkid " & _
                                ", budgetcodesunkid " & _
                                ", audittype " & _
                                ", audituserunkid " & _
                                ", auditdatetime " & _
                                ", ip " & _
                                ", machine_name " & _
                                ", form_name " & _
                                " " & _
                                " " & _
                                " " & _
                                " " & _
                                " " & _
                                ", isweb " & _
                         ") " & _
                         "SELECT " & _
                                "  budgetcodestranunkid " & _
                                ", bgbudgetcodes_tran.budgettranunkid " & _
                                ", budgetunkid " & _
                                ", periodunkid " & _
                                ", amount " & _
                                ", budgetamount " & _
                                ", budgetsalaryamount " & _
                                ", budgetotherpayrollamount " & _
                                ", allocationtranunkid " & _
                                ", tranheadunkid " & _
                                ", jobunkid " & _
                                ", budgetcodesunkid " & _
                                ", 3 " & _
                                ", @voiduserunkid " & _
                                ", @voiddatetime " & _
                                ", '" & mstrClientIP & "' " & _
                                ", '" & mstrHostName & "' " & _
                                ", @form_name " & _
                                " " & _
                                " " & _
                                " " & _
                                " " & _
                                " " & _
                                ", @isweb " & _
                         "FROM bgbudgetcodes_tran " & _
                         "JOIN #cteBTran ON #cteBTran.budgettranunkid = bgbudgetcodes_tran.budgettranunkid " & _
                         "WHERE bgbudgetcodes_tran.isvoid = 0 " & _
                         "AND bgbudgetcodes_tran.periodunkid = @periodunkid "
            'Sohail (13 Oct 2017) - [budgetcodesunkid]
            'Sohail (03 May 2017) - [allocationtranunkid, tranheadunkid, jobunkid]

            strQ &= "DROP TABLE #cteBTran"

            objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intPeriodUnkId.ToString)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, True)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intVoiduserunkid.ToString)
            If dtVoiddatetime = Nothing Then
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            Else
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, dtVoiddatetime.ToString)
            End If
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strVoidreason.ToString)

            intRowsAffected = objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                objDataOperation.ReleaseTransaction(False)
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            strQ = "SELECT budgettranunkid INTO #cteBTran FROM bgbudget_tran WHERE budgettranunkid IN (" & strBudgetTranUnkIDs & "); "

            strQ &= "UPDATE bgbudgetcodes_tran SET " & _
               " isvoid = @isvoid " & _
               ", voiduserunkid = @voiduserunkid" & _
               ", voiddatetime = @voiddatetime" & _
               ", voidreason = @voidreason " & _
             "FROM #cteBTran WHERE #cteBTran.budgettranunkid = bgbudgetcodes_tran.budgettranunkid AND isvoid = 0 " & _
             "AND periodunkid = @periodunkid "

            strQ &= "DROP TABLE #cteBTran"

            intRowsAffected = objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                objDataOperation.ReleaseTransaction(False)
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If xDataOpr Is Nothing Then
                objDataOperation.ReleaseTransaction(True)
            End If

            Return True
        Catch ex As Exception
            If xDataOpr Is Nothing Then
                objDataOperation.ReleaseTransaction(False)
            End If
            Throw New Exception(ex.Message & "; Procedure Name: VoidAllByBudgetTranUnkId; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xDataOpr Is Nothing Then
                objDataOperation = Nothing
            End If
        End Try
    End Function
    'Sohail (19 Apr 2017) -- End

    'Sohail (03 May 2017) -- Start
    'Enhancement - 66.1 - Allow to include newly hired employees on budget codes screen and allow to map employees with planned employees.
    Public Function VoidAllByBudgetCodesTranUnkId(ByVal strBudgetCodesTranUnkIDs As String _
                                                 , ByVal intPeriodUnkId As Integer _
                                                 , ByVal intVoiduserunkid As Integer _
                                                 , ByVal dtVoiddatetime As Date _
                                                 , ByVal strVoidreason As String _
                                                 , Optional ByVal xDataOpr As clsDataOperation = Nothing _
                                                 ) As Boolean
        'If isUsed(intUnkid) Then
        '    mstrMessage = "<Message>"
        '    Return False
        'End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim intRowsAffected As Integer = 0

        If xDataOpr Is Nothing Then
            objDataOperation = New clsDataOperation
            objDataOperation.BindTransaction()
        Else
            objDataOperation = xDataOpr
        End If
        objDataOperation.ClearParameters()

        Try
            'If mstrWebIP.Trim = "" Then mstrWebIP = getIP()
            'If mstrWebHost.Trim = "" Then mstrWebHost = getHostName()

            objDataOperation.AddParameter("@form_name", SqlDbType.NVarChar, 500, mstrFormName)
            objDataOperation.AddParameter("@isweb", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsWeb)

            strQ = "SELECT budgetcodestranunkid INTO #cteBTran FROM bgbudgetcodes_tran WHERE budgetcodestranunkid IN (" & strBudgetCodesTranUnkIDs & "); "

            strQ &= "INSERT INTO atbgbudgetcodes_tran ( " & _
                                "  budgetcodestranunkid " & _
                                ", budgettranunkid " & _
                                ", budgetunkid " & _
                                ", periodunkid " & _
                                ", amount " & _
                                ", budgetamount " & _
                                ", budgetsalaryamount " & _
                                ", budgetotherpayrollamount " & _
                                ", allocationtranunkid " & _
                                ", tranheadunkid " & _
                                ", jobunkid " & _
                                ", budgetcodesunkid " & _
                                ", audittype " & _
                                ", audituserunkid " & _
                                ", auditdatetime " & _
                                ", ip " & _
                                ", machine_name " & _
                                ", form_name " & _
                                " " & _
                                " " & _
                                " " & _
                                " " & _
                                " " & _
                                ", isweb " & _
                         ") " & _
                         "SELECT " & _
                                "  bgbudgetcodes_tran.budgetcodestranunkid " & _
                                ", bgbudgetcodes_tran.budgettranunkid " & _
                                ", budgetunkid " & _
                                ", periodunkid " & _
                                ", amount " & _
                                ", budgetamount " & _
                                ", budgetsalaryamount " & _
                                ", budgetotherpayrollamount " & _
                                ", allocationtranunkid " & _
                                ", tranheadunkid " & _
                                ", jobunkid " & _
                                ", budgetcodesunkid " & _
                                ", 3 " & _
                                ", @voiduserunkid " & _
                                ", @voiddatetime " & _
                                ", '" & mstrClientIP & "' " & _
                                ", '" & mstrHostName & "' " & _
                                ", @form_name " & _
                                " " & _
                                " " & _
                                " " & _
                                " " & _
                                " " & _
                                ", @isweb " & _
                         "FROM bgbudgetcodes_tran " & _
                         "JOIN #cteBTran ON #cteBTran.budgetcodestranunkid = bgbudgetcodes_tran.budgetcodestranunkid " & _
                         "WHERE bgbudgetcodes_tran.isvoid = 0 " & _
                         "AND bgbudgetcodes_tran.periodunkid = @periodunkid "
            'Sohail (13 Oct 2017) - [budgetcodesunkid]

            strQ &= "DROP TABLE #cteBTran"

            objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intPeriodUnkId.ToString)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, True)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intVoiduserunkid.ToString)
            If dtVoiddatetime = Nothing Then
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            Else
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, dtVoiddatetime.ToString)
            End If
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strVoidreason.ToString)

            intRowsAffected = objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                objDataOperation.ReleaseTransaction(False)
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            strQ = "SELECT budgetcodestranunkid INTO #cteBTran FROM bgbudgetcodes_tran WHERE budgetcodestranunkid IN (" & strBudgetCodesTranUnkIDs & "); "

            strQ &= "UPDATE bgbudgetcodes_tran SET " & _
               " isvoid = @isvoid " & _
               ", voiduserunkid = @voiduserunkid" & _
               ", voiddatetime = @voiddatetime" & _
               ", voidreason = @voidreason " & _
             "FROM #cteBTran WHERE #cteBTran.budgetcodestranunkid = bgbudgetcodes_tran.budgetcodestranunkid AND isvoid = 0 " & _
             "AND periodunkid = @periodunkid "

            strQ &= "DROP TABLE #cteBTran"

            intRowsAffected = objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                objDataOperation.ReleaseTransaction(False)
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If xDataOpr Is Nothing Then
                objDataOperation.ReleaseTransaction(True)
            End If

            Return True
        Catch ex As Exception
            If xDataOpr Is Nothing Then
                objDataOperation.ReleaseTransaction(False)
            End If
            Throw New Exception(ex.Message & "; Procedure Name: VoidAllByBudgetCodesTranUnkId; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xDataOpr Is Nothing Then
                objDataOperation = Nothing
            End If
        End Try
    End Function
    'Sohail (03 May 2017) -- End

    'Sohail (13 Oct 2017) -- Start
    'CCBRT Enhancement - 70.1 - (Ref. Id - 110, 111) - for all Four reports the user to be able to select a range of consecutive periods so as to be able to see the total. This Feature will enable the user to select more than one default budget so as to get a consolidated report.
    Public Function getEmployeeIds(ByVal strBudgetCodesUnkIDs As String) As String
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim strEmployeeIds As String = String.Empty

        objDataOperation = New clsDataOperation
        Try
            strQ = "SELECT " & _
                   "    ISNULL(STUFF( " & _
                   "            ( " & _
                   "                SELECT DISTINCT " & _
                   "                    ', ' + CAST(allocationtranunkid AS NVARCHAR(MAX)) " & _
                   "                FROM bgbudgetcodes_tran " & _
                   "                WHERE isvoid = 0 " & _
                   "                    AND budgetcodesunkid IN ( " & strBudgetCodesUnkIDs & " ) " & _
                   "                FOR XML PATH ('')), 1, 1, '' " & _
                   "         ),'-999') AS EmployeeIds "

            dsList = objDataOperation.ExecQuery(strQ, "Ids")

            If dsList.Tables("Ids").Rows.Count > 0 Then
                strEmployeeIds = dsList.Tables("Ids").Rows(0).Item("EmployeeIds")
            End If

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: getEmployeeIds; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
        End Try
        Return strEmployeeIds
    End Function
    'Sohail (13 Oct 2017) -- End

    ''' <summary>
    ''' Modify By: Sohail
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isUsed(ByVal intUnkid As Integer) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            StrQ = "<Query>"

            objDataOperation.AddParameter("@budgetcodestranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.tables(0).rows.count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isUsed; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sohail
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isExist(ByVal intBudgetCodesUnkid As Integer, ByVal intAllocationTranUnkid As Integer, ByVal intTranheadUnkid As Integer, Optional ByVal xDataOp As clsDataOperation = Nothing, Optional ByVal intUnkid As Integer = -1) As Integer
        'Sohail (01 May 2021) - [intBudgetCodesUnkid, intAllocationTranUnkid, intTranheadUnkid, xDataOp]
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim intRet_UnkId As Integer = 0

        If xDataOp Is Nothing Then
        objDataOperation = New clsDataOperation
        Else
            objDataOperation = xDataOp
        End If
        objDataOperation.ClearParameters()

        Try
            strQ = "SELECT " & _
              "  budgetcodestranunkid " & _
             "FROM bgbudgetcodes_tran " & _
             "WHERE isvoid = 0 "

            If intBudgetCodesUnkid > 0 Then
                strQ &= " AND budgetcodesunkid = @budgetcodesunkid "
                objDataOperation.AddParameter("@budgetcodesunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intBudgetCodesUnkid)
            End If

            If intAllocationTranUnkid > 0 Then
                strQ &= " AND allocationtranunkid = @allocationtranunkid "
                objDataOperation.AddParameter("@allocationtranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intAllocationTranUnkid)
            End If

            If intTranheadUnkid > 0 Then
                strQ &= " AND tranheadunkid = @tranheadunkid "
                objDataOperation.AddParameter("@tranheadunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intTranheadUnkid)
            End If

            If intUnkid > 0 Then
                strQ &= " AND budgetcodestranunkid <> @budgetcodestranunkid "
                objDataOperation.AddParameter("@budgetcodestranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)
            End If


            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If dsList.Tables(0).Rows.Count > 0 Then
                intRet_UnkId = CInt(dsList.Tables(0).Rows(0).Item("budgetcodestranunkid"))
            End If
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isExist; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xDataOp Is Nothing Then objDataOperation = Nothing
        End Try
        Return intRet_UnkId
    End Function

    'Sohail (01 May 2021) -- Start
    'IHI Issue : : Project codes are missing for some employees on editong budget codes.
    Public Function Save(ByVal objDataOp As clsDataOperation, ByVal dtTable As DataTable, ByVal dtCurrentDateTime As Date, ByVal dsFundSource As DataSet, ByVal dsATFundSource As DataSet, Optional ByVal bw As BackgroundWorker = Nothing, Optional ByRef intProgress As Integer = 0) As Boolean

        Dim objBudgetCodesFundSource As New clsBudgetCodesfundsource_Tran
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        intProgress = 0

        objDataOp.ClearParameters()

        Try

            For Each dtRow As DataRow In dtTable.Rows

                mintBudgetcodestranunkid = CInt(dtRow.Item("budgetcodestranunkid"))
                mintBudgettranunkid = CInt(dtRow.Item("budgettranunkid"))
                mintAllocationtranunkid = CInt(dtRow.Item("allocationtranunkid"))
                mintJobUnkId = CInt(dtRow.Item("jobunkid"))

                If dtTable.Columns.Contains("tranheadunkid") = True Then
                    mintTranheadunkid = CInt(dtRow.Item("tranheadunkid")) 'Sohail (03 May 2017)
                    mdecAmount = CDec(dtRow.Item("pramount"))
                    'mdecSalaryamount = 0
                    'mdecOtherpayrollamount = 0
                Else
                    mintTranheadunkid = -1 'Sohail (03 May 2017)
                    mdecAmount = 0
                    'mdecSalaryamount = CDec(dtRow.Item("_|1"))
                    'mdecOtherpayrollamount = CDec(dtRow.Item("_|2"))
                End If
                mdecBudgetamount = CDec(dtRow.Item("budgetamount"))
                mdecBudgetsalaryamount = CDec(dtRow.Item("budgetsalaryamount"))
                mdecBudgetotherpayrollamount = CDec(dtRow.Item("budgetotherpayrollamount"))

                'mintBudgetcodestranunkid = isExist(mintBudgetcodesunkid, mintAllocationtranunkid, mintTranheadunkid, objDataOp)

                If mintBudgetcodestranunkid <= 0 Then
                    If Insert(dtCurrentDateTime, objDataOp) = False Then
                        objDataOp.ReleaseTransaction(False)
                        Return False
                    End If
                Else
                    If Update(dtCurrentDateTime, objDataOp) = False Then
                        objDataOp.ReleaseTransaction(False)
                        Return False
                    End If
                End If

                objBudgetCodesFundSource._Budgettranunkid = mintBudgettranunkid
                objBudgetCodesFundSource._PeriodUnkId = mintPeriodUnkId
                objBudgetCodesFundSource._Userunkid = mintUserunkid
                objBudgetCodesFundSource._Isvoid = mblnIsvoid
                objBudgetCodesFundSource._Voiduserunkid = mintVoiduserunkid
                objBudgetCodesFundSource._Voiddatetime = mdtVoiddatetime
                objBudgetCodesFundSource._Voidreason = mstrVoidreason
                objBudgetCodesFundSource._Budgetcodestranunkid = mintBudgetcodestranunkid
                If objBudgetCodesFundSource.Save(dtRow, objDataOp, dtCurrentDateTime, dsFundSource, dsATFundSource) = False Then
                    objDataOp.ReleaseTransaction(False)
                    Return False
                End If

                If bw IsNot Nothing Then
                    bw.ReportProgress(intProgress + 1)
                End If

                intProgress += 1
            Next

            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Save; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
        End Try
    End Function

    Public Function IsTableDataUpdate(ByVal unkid As Integer, Optional ByVal objDooperation As clsDataOperation = Nothing) As Boolean
        Dim strQ As String = ""
        Dim dsList As New DataSet
        Dim exForce As Exception
        Try
            If objDooperation Is Nothing Then
                objDataOperation = New clsDataOperation
            Else
                objDataOperation = objDooperation
            End If

            strQ = "SELECT TOP 1 * FROM atbgbudgetcodes_tran WHERE budgetcodestranunkid = @budgetcodestranunkid AND audittype <> 3 ORDER BY auditdatetime DESC"

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@budgetcodestranunkid", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, unkid)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For Each dr As DataRow In dsList.Tables(0).Rows

                If dr("budgetunkid").ToString() = mintBudgetunkid AndAlso dr("periodunkid").ToString() = mintPeriodUnkId AndAlso dr("budgettranunkid").ToString() = mintBudgettranunkid _
                    AndAlso CDec(dr("amount")) = mdecAmount AndAlso CDec(dr("budgetamount")) = mdecBudgetamount AndAlso CDec(dr("budgetsalaryamount")) = mdecBudgetsalaryamount AndAlso CDec(dr("budgetotherpayrollamount")) = mdecBudgetotherpayrollamount _
                    AndAlso dr("allocationtranunkid").ToString() = mintAllocationtranunkid AndAlso dr("tranheadunkid").ToString() = mintTranheadunkid AndAlso dr("jobunkid").ToString() = mintJobUnkId AndAlso dr("budgetcodesunkid").ToString() = mintBudgetcodesunkid _
                    Then

                    Return True
                Else
                    Return False
                End If
            Next
            Return False

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: IsTableDataUpdate; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If objDooperation Is Nothing Then objDataOperation = Nothing
        End Try
    End Function
    'Sohail (01 May 2021) -- End

    Public Function InsertAuditBudgetCodeTran(ByVal xDataOpr As clsDataOperation, ByVal xAuditType As enAuditType, ByVal dtCurrentDateAndTime As Date) As Boolean
        Dim strQ As String = ""
        Dim exForce As Exception

        Try

            strQ = "INSERT INTO atbgbudgetcodes_tran ( " & _
                               "  budgetunkid " & _
                               ", periodunkid " & _
                               ", budgetcodestranunkid " & _
                               ", budgettranunkid " & _
                               ", amount " & _
                               ", budgetamount " & _
                               ", budgetsalaryamount " & _
                               ", budgetotherpayrollamount " & _
                                ", allocationtranunkid " & _
                                ", tranheadunkid " & _
                                ", jobunkid " & _
                                ", budgetcodesunkid " & _
                                ", audittype " & _
                                ", audituserunkid " & _
                                ", auditdatetime " & _
                                ", ip " & _
                                ", machine_name " & _
                                ", form_name " & _
                                " " & _
                                " " & _
                                " " & _
                                " " & _
                                " " & _
                                ", isweb " & _
             ") VALUES (" & _
                               "  @budgetunkid " & _
                               ", @periodunkid " & _
                               ", @budgetcodestranunkid " & _
                               ", @budgettranunkid " & _
                               ", @amount " & _
                               ", @budgetamount " & _
                               ", @budgetsalaryamount " & _
                               ", @budgetotherpayrollamount " & _
                                ", @allocationtranunkid " & _
                                ", @tranheadunkid " & _
                                ", @jobunkid " & _
                                ", @budgetcodesunkid " & _
                                ", @audittype " & _
                                ", @audituserunkid " & _
                                ", @auditdatetime " & _
                                ", @ip " & _
                                ", @machine_name " & _
                                ", @form_name " & _
                                " " & _
                                " " & _
                                " " & _
                                " " & _
                                " " & _
                                ", @isweb " & _
             "); SELECT @@identity"
            'Sohail (13 Oct 2017) - [budgetcodesunkid]
            'Sohail (03 May 2017) - [allocationtranunkid, tranheadunkid, jobunkid]

            xDataOpr.ClearParameters()
            xDataOpr.AddParameter("@budgetcodestranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintBudgetcodestranunkid.ToString)
            xDataOpr.AddParameter("@budgetunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintBudgetunkid.ToString)
            xDataOpr.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPeriodUnkId.ToString)
            xDataOpr.AddParameter("@budgettranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintBudgettranunkid.ToString)
            xDataOpr.AddParameter("@amount", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecAmount.ToString)
            xDataOpr.AddParameter("@budgetamount", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecBudgetamount.ToString)
            xDataOpr.AddParameter("@budgetsalaryamount", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecBudgetsalaryamount.ToString)
            xDataOpr.AddParameter("@budgetotherpayrollamount", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecBudgetotherpayrollamount.ToString)
            xDataOpr.AddParameter("@audittype", SqlDbType.Int, eZeeDataType.INT_SIZE, xAuditType)
            xDataOpr.AddParameter("@audituserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAuditUserId)
            xDataOpr.AddParameter("@auditdatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtAuditDate)
            xDataOpr.AddParameter("@allocationtranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAllocationtranunkid.ToString)
            xDataOpr.AddParameter("@tranheadunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintTranheadunkid.ToString)
            xDataOpr.AddParameter("@jobunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintJobUnkId.ToString)
            xDataOpr.AddParameter("@budgetcodesunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintBudgetcodesunkid.ToString) 'Sohail (13 Oct 2017)
            'Sohail (12 Jan 2019) -- Start
            'AT Testing - 76.1 - Fixing AT Issues.
            xDataOpr.AddParameter("@ip", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrClientIP)
            'Sohail (12 Jan 2019) -- End
            xDataOpr.AddParameter("@machine_name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrHostName)
            xDataOpr.AddParameter("@form_name", SqlDbType.NVarChar, 500, mstrFormName)
            xDataOpr.AddParameter("@isweb", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsWeb)

            xDataOpr.ExecNonQuery(strQ)

            If xDataOpr.ErrorMessage <> "" Then
                exForce = New Exception(xDataOpr.ErrorNumber & ": " & xDataOpr.ErrorMessage)
                Throw exForce
            End If

            Return True

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: InsertAuditBudgetCodeTran; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
        End Try
    End Function

	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Public Shared Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "WEB")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class