﻿'************************************************************************************************************************************
'Class Name : clsBudgetformula_master.vb
'Purpose    :
'Date       :21/06/2016
'Written By :Sohail
'Modified   :
'************************************************************************************************************************************

Imports eZeeCommonLib
Imports eZee.Common.eZeeForm
Imports System

''' <summary>
''' Purpose: 
''' Developer: Sohail
''' </summary>
Public Class clsBudgetformula_master
    Private Shared ReadOnly mstrModuleName As String = "clsBudgetformula_master"
    Dim objDataOperation As clsDataOperation
    Dim mstrMessage As String = ""
  

#Region " Private variables "

    Private mintBudgetformulaunkid As Integer
    Private mstrFormula_Code As String = String.Empty
    Private mstrFormula_Name As String = String.Empty
    'Nilay (26-Aug-2016) -- Start
    'For Other Language ADD Name1, Name2 
    Private mstrFormula_Name1 As String = String.Empty
    Private mstrFormula_Name2 As String = String.Empty
    'Nilay (26-Aug-2016) -- END
    Private mintUserunkid As Integer
    Private mblnIsvoid As Boolean
    Private mintVoiduserunkid As Integer
    Private mdtVoiddatetime As Date
    Private mstrVoidreason As String = String.Empty
    Private mdtFormulaTran As DataTable
    Private mdtFormula_Tranhead As DataTable
    'Private mstrWebFormName As String = String.Empty
    'Private mstrWebClientIP As String = ""
    'Private mstrWebHostName As String = ""

#End Region

#Region " Properties "

'S.SANDEEP [28-May-2018] -- START 
'ISSUE/ENHANCEMENT : {Audit Trails} 
Private mstrFormName As String = String.Empty
Public WriteOnly Property _FormName() As String 
Set(ByVal value As String) 
mstrFormName = value 
End Set 
End Property 
 
Private mstrClientIP As String = ""
Public WriteOnly Property _ClientIP() As String 
Set(ByVal value As String) 
mstrClientIP = value 
End Set 
End Property 
 
Private mstrHostName As String = ""
Public WriteOnly Property _HostName() As String 
Set(ByVal value As String) 
mstrHostName = value 
End Set 
End Property 
 
Private mblnIsWeb As Boolean = False
Public WriteOnly Property _FromWeb() As Boolean 
Set(ByVal value As Boolean) 
mblnIsWeb = value 
End Set 
End Property 
 
    Private mintAuditUserId As Integer = 0
    Public WriteOnly Property _AuditUserId() As Integer
        Set(ByVal value As Integer)
            mintAuditUserId = value
        End Set
    End Property

    Private mintCompanyUnkid As Integer = 0
    Public WriteOnly Property _CompanyUnkid() As Integer
        Set(ByVal value As Integer)
            mintCompanyUnkid = value
        End Set
    End Property
 
Private mdtAuditDate As DateTime = Now
Public WriteOnly Property _AuditDate() As DateTime 
Set(ByVal value As DateTime) 
mdtAuditDate = value 
End Set 
End Property 
 
Private xDataOpr As clsDataOperation = Nothing
Public Property _xDataOpr() As clsDataOperation 
Get 
Return xDataOpr 
End Get 
Set(ByVal value As clsDataOperation) 
xDataOpr = value 
End Set 
End Property 
 
Private mintLoginEmployeeunkid As Integer = 0
Public Property _LoginEmployeeunkid() As Integer 
Get 
Return mintLoginEmployeeunkid 
End Get 
Set(ByVal value As Integer) 
mintLoginEmployeeunkid = value 
End Set 
End Property 
 
Private mintVoidlogingemployeeunkid As Integer=0
Public Property _Voidloginemployeeunkid() As Integer 
Get 
Return mintVoidlogingemployeeunkid 
End Get 
Set(ByVal value As Integer) 
mintVoidlogingemployeeunkid = value 
End Set 
End Property 
'S.SANDEEP [28-May-2018] -- END 

    Public ReadOnly Property _Message() As String
        Get
            Return mstrMessage
        End Get
    End Property

    ''' <summary>
    ''' Purpose: Get or Set Message from Class 
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _FormulaTran() As DataTable
        Get
            Return mdtFormulaTran
        End Get
        Set(ByVal value As DataTable)
            mdtFormulaTran = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set Message from Class 
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Formula_Tranhead() As DataTable
        Get
            Return mdtFormula_Tranhead
        End Get
        Set(ByVal value As DataTable)
            mdtFormula_Tranhead = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set budgetformulaunkid
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Budgetformulaunkid() As Integer
        Get
            Return mintBudgetformulaunkid
        End Get
        Set(ByVal value As Integer)
            mintBudgetformulaunkid = value
            Call GetData()
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set formula_code
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Formula_Code() As String
        Get
            Return mstrFormula_Code
        End Get
        Set(ByVal value As String)
            mstrFormula_Code = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set formula_name
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Formula_Name() As String
        Get
            Return mstrFormula_Name
        End Get
        Set(ByVal value As String)
            mstrFormula_Name = Value
        End Set
    End Property

    'Nilay (26-Aug-2016) -- Start
    'For Other Language ADD Name1, Name2 
    Public Property _Formula_Name1() As String
        Get
            Return mstrFormula_Name1
        End Get
        Set(ByVal value As String)
            mstrFormula_Name1 = value
        End Set
    End Property

    Public Property _Formula_Name2() As String
        Get
            Return mstrFormula_Name2
        End Get
        Set(ByVal value As String)
            mstrFormula_Name2 = value
        End Set
    End Property
    'Nilay (26-Aug-2016) -- END

    ''' <summary>
    ''' Purpose: Get or Set userunkid
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Userunkid() As Integer
        Get
            Return mintUserunkid
        End Get
        Set(ByVal value As Integer)
            mintUserunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isvoid
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Isvoid() As Boolean
        Get
            Return mblnIsvoid
        End Get
        Set(ByVal value As Boolean)
            mblnIsvoid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiduserunkid
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Voiduserunkid() As Integer
        Get
            Return mintVoiduserunkid
        End Get
        Set(ByVal value As Integer)
            mintVoiduserunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiddatetime
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Voiddatetime() As Date
        Get
            Return mdtVoiddatetime
        End Get
        Set(ByVal value As Date)
            mdtVoiddatetime = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voidreason
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Voidreason() As String
        Get
            Return mstrVoidreason
        End Get
        Set(ByVal value As String)
            mstrVoidreason = Value
        End Set
    End Property

    'Public Property _WebFormName() As String
    '    Get
    '        Return mstrWebFormName
    '    End Get
    '    Set(ByVal value As String)
    '        mstrWebFormName = value
    '    End Set
    'End Property

    'Public WriteOnly Property _WebClientIP() As String
    '    Set(ByVal value As String)
    '        mstrWebClientIP = value
    '    End Set
    'End Property

    'Public WriteOnly Property _WebHostName() As String
    '    Set(ByVal value As String)
    '        mstrWebHostName = value
    '    End Set
    'End Property

#End Region

#Region " Constructor "

    Public Sub New()
        Try

            mdtFormulaTran = New DataTable
            mdtFormulaTran.Columns.Add("effectivedate", Type.GetType("System.String")).DefaultValue = String.Empty
            mdtFormulaTran.Columns.Add("functionid", Type.GetType("System.String")).DefaultValue = String.Empty
            mdtFormulaTran.Columns.Add("function", Type.GetType("System.String")).DefaultValue = String.Empty
            mdtFormulaTran.Columns.Add("headid", Type.GetType("System.String")).DefaultValue = String.Empty
            mdtFormulaTran.Columns.Add("head", Type.GetType("System.String")).DefaultValue = String.Empty
            mdtFormulaTran.Columns.Add("defaultid", Type.GetType("System.Int32")).DefaultValue = 0
            mdtFormulaTran.Columns.Add("default", Type.GetType("System.String")).DefaultValue = String.Empty
            mdtFormulaTran.Columns.Add("AUD", Type.GetType("System.String")).DefaultValue = String.Empty


            mdtFormula_Tranhead = New DataTable
            mdtFormula_Tranhead.Columns.Add("effectivedate", Type.GetType("System.String")).DefaultValue = String.Empty
            mdtFormula_Tranhead.Columns.Add("tranheadunkid", Type.GetType("System.String")).DefaultValue = String.Empty
            mdtFormula_Tranhead.Columns.Add("defaultid", Type.GetType("System.Int32")).DefaultValue = 0
            mdtFormula_Tranhead.Columns.Add("default", Type.GetType("System.String")).DefaultValue = String.Empty
            mdtFormula_Tranhead.Columns.Add("userunkid", Type.GetType("System.Int32")).DefaultValue = 0
            mdtFormula_Tranhead.Columns.Add("isvoid", Type.GetType("System.Boolean")).DefaultValue = 0
            mdtFormula_Tranhead.Columns.Add("voiduserunkid", Type.GetType("System.Int32")).DefaultValue = 0
            mdtFormula_Tranhead.Columns.Add("voiddatetime", Type.GetType("System.DateTime")).DefaultValue = Nothing
            mdtFormula_Tranhead.Columns.Add("voidreason", Type.GetType("System.String")).DefaultValue = String.Empty
            mdtFormula_Tranhead.Columns.Add("AUD", Type.GetType("System.String")).DefaultValue = String.Empty

        Catch ex As Exception

        End Try
    End Sub
#End Region

    ''' <summary>
    ''' Modify By: Sohail
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Sub GetData()
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        Dim objDataOperation As New clsDataOperation

        Try
            strQ = "SELECT " & _
              "  budgetformulaunkid " & _
              ", formula_code " & _
              ", formula_name " & _
              ", formula_name1 " & _
              ", formula_name2 " & _
              ", userunkid " & _
              ", isvoid " & _
              ", voiduserunkid " & _
              ", voiddatetime " & _
              ", voidreason " & _
             "FROM bgbudgetformula_master " & _
             "WHERE budgetformulaunkid = @budgetformulaunkid "
            'Nilay (26-Aug-2016) -- [formula_name1,formula_name2]

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@budgetformulaunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintBudgetformulaunkid.ToString)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For Each dtRow As DataRow In dsList.Tables(0).Rows
                mintBudgetformulaunkid = CInt(dtRow.Item("budgetformulaunkid"))
                mstrFormula_Code = dtRow.Item("formula_code").ToString
                mstrFormula_Name = dtRow.Item("formula_name").ToString
                'Nilay (26-Aug-2016) -- Start
                'For Other Language ADD Name1, Name2 
                mstrFormula_Name1 = dtRow.Item("formula_name1").ToString
                mstrFormula_Name2 = dtRow.Item("formula_name2").ToString
                'Nilay (26-Aug-2016) -- END
                mintUserunkid = CInt(dtRow.Item("userunkid"))
                mblnIsvoid = CBool(dtRow.Item("isvoid"))
                mintVoiduserunkid = CInt(dtRow.Item("voiduserunkid"))
                If IsDBNull(dtRow.Item("voiddatetime")) = False Then mdtVoiddatetime = CDate(dtRow.Item("voiddatetime"))
                mstrVoidreason = dtRow.Item("voidreason").ToString
                Exit For
            Next

            dsList = Nothing
            Dim objFormulaTran As New clsBudgetformula_Tran
            objFormulaTran._Budgetformulaunkid = mintBudgetformulaunkid
            dsList = objFormulaTran.GetList("List", mintBudgetformulaunkid, mdtTransferAsOnDate)
            mdtFormulaTran = dsList.Tables(0).Copy
            dsList.Clear()
            dsList = Nothing
            objFormulaTran = Nothing

            Dim objFormulaHeadTran As New clsBudgetformula_head_tran
            objFormulaHeadTran._Budgetformulaunkid = mintBudgetformulaunkid
            dsList = objFormulaHeadTran.GetList("List", mintBudgetformulaunkid, mdtTransferAsOnDate)
            mdtFormula_Tranhead = dsList.Tables(0).Copy
            dsList.Clear()
            dsList = Nothing
            objFormulaHeadTran = Nothing

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetData; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Sub

    ''' <summary>
    ''' Modify By: Sohail
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function GetList(ByVal strTableName As String, Optional ByVal blnOnlyActive As Boolean = True) As DataSet
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        Dim objDataOperation As New clsDataOperation
        objDataOperation.ClearParameters()
        Try
            strQ = "SELECT " & _
              "  budgetformulaunkid " & _
              ", formula_code " & _
              ", formula_name " & _
              ", formula_name1 " & _
              ", formula_name2 " & _
              ", userunkid " & _
              ", isvoid " & _
              ", voiduserunkid " & _
              ", voiddatetime " & _
              ", voidreason " & _
             "FROM bgbudgetformula_master "
            'Nilay (26-Aug-2016) -- [formula_name1,formula_name2]

            If blnOnlyActive Then
                strQ &= " WHERE isvoid  = 0"
            End If

            dsList = objDataOperation.ExecQuery(strQ, strTableName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
        Return dsList
    End Function

    ''' <summary>
    ''' Modify By: Sohail
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> INSERT INTO Database Table (bgbudgetformula_master) </purpose>
    Public Function Insert() As Boolean

        If isExist("", mstrFormula_Name) Then
            mstrMessage = Language.getMessage(mstrModuleName, 1, "This Transaction Head Name is already defined. Please define new Transaction Head Name.")
            Return False
        End If
        If isExist(mstrFormula_Code, "") Then
            mstrMessage = Language.getMessage(mstrModuleName, 2, "This Transaction Head Code is already defined. Please define new Transaction Head Code.")
            Return False
        End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation
        objDataOperation.BindTransaction()
        objDataOperation.ClearParameters()

        Try
            objDataOperation.AddParameter("@formula_code", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrFormula_Code.ToString)
            objDataOperation.AddParameter("@formula_name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrFormula_Name.ToString)
            'Nilay (26-Aug-2016) -- Start
            'For Other Language ADD Name1, Name2 
            objDataOperation.AddParameter("@formula_name1", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrFormula_Name1.ToString)
            objDataOperation.AddParameter("@formula_name2", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrFormula_Name2.ToString)
            'Nilay (26-Aug-2016) -- END
            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
            objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, IIf(mdtVoiddatetime <> Nothing, mdtVoiddatetime, DBNull.Value))
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)

            strQ = "INSERT INTO bgbudgetformula_master ( " & _
              "  formula_code " & _
              ", formula_name " & _
              ", formula_name1 " & _
              ", formula_name2 " & _
              ", userunkid " & _
              ", isvoid " & _
              ", voiduserunkid " & _
              ", voiddatetime " & _
              ", voidreason" & _
            ") VALUES (" & _
              "  @formula_code " & _
              ", @formula_name " & _
              ", @formula_name1 " & _
              ", @formula_name2 " & _
              ", @userunkid " & _
              ", @isvoid " & _
              ", @voiduserunkid " & _
              ", @voiddatetime " & _
              ", @voidreason" & _
            "); SELECT @@identity"
            'Nilay (26-Aug-2016) -- [formula_name1,formula_name2]

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            mintBudgetformulaunkid = dsList.Tables(0).Rows(0).Item(0)

            If ATInsertBudgetFormula_Master(objDataOperation, 1) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Dim objBudgetTran As New clsBudgetformula_Tran
            objBudgetTran._Budgetformulaunkid = mintBudgetformulaunkid
            objBudgetTran._Formula_Tran = mdtFormulaTran
            objBudgetTran._Isvoid = False
            objBudgetTran._Voiduserunkid = -1
            objBudgetTran._Voiddatetime = Nothing
            objBudgetTran._Voidreason = ""
            objBudgetTran._Userunkid = mintUserunkid
            objBudgetTran._ClientIP = mstrClientIP
            objBudgetTran._FormName = mstrFormName
            objBudgetTran._HostName = mstrHostName
            objBudgetTran._AuditDate = mdtAuditDate
            objBudgetTran._AuditUserId = mintAuditUserId
objBudgetTran._CompanyUnkid = mintCompanyUnkid
            objBudgetTran._FromWeb = mblnIsWeb
            objBudgetTran._LoginEmployeeunkid = mintLoginEmployeeunkid

            If objBudgetTran.InsertDelete_Formula(objDataOperation) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If
            objBudgetTran = Nothing

            Dim objBugdetTranhead As New clsBudgetformula_head_tran
            objBugdetTranhead._Budgetformulaunkid = mintBudgetformulaunkid
            objBugdetTranhead._Formula_Tranhead = mdtFormula_Tranhead
            objBugdetTranhead._Isvoid = False
            objBugdetTranhead._Voiduserunkid = -1
            objBugdetTranhead._Voiddatetime = Nothing
            objBugdetTranhead._Voidreason = ""
            objBugdetTranhead._Userunkid = mintUserunkid
            objBugdetTranhead._ClientIP = mstrClientIP
            objBugdetTranhead._FormName = mstrFormName
            objBugdetTranhead._HostName = mstrHostName
            objBugdetTranhead._AuditDate = mdtAuditDate
            objBugdetTranhead._AuditUserId = mintAuditUserId
objBugdetTranhead._CompanyUnkid = mintCompanyUnkid
            objBugdetTranhead._FromWeb = mblnIsWeb
            objBugdetTranhead._LoginEmployeeunkid = mintLoginEmployeeunkid
            If objBugdetTranhead.InsertDelete_TranHead(objDataOperation) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If
            objBugdetTranhead = Nothing

            objDataOperation.ReleaseTransaction(True)
            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Insert; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sohail
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Update Database Table (bgbudgetformula_master) </purpose>
    Public Function Update() As Boolean
        If isExist(mstrFormula_Code, "", mintBudgetformulaunkid) Then
            mstrMessage = Language.getMessage(mstrModuleName, 2, "This Transaction Head Code is already defined. Please define new Transaction Head Code.")
            Return False
        End If
        If isExist("", mstrFormula_Name, mintBudgetformulaunkid) Then
            mstrMessage = Language.getMessage(mstrModuleName, 1, "This Transaction Head Name is already defined. Please define new Transaction Head Name.")
            Return False
        End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation
        objDataOperation.BindTransaction()
        objDataOperation.ClearParameters()

        Try
            objDataOperation.AddParameter("@budgetformulaunkid", SqlDbType.int, eZeeDataType.INT_SIZE, mintbudgetformulaunkid.ToString)
            objDataOperation.AddParameter("@formula_code", SqlDbType.nvarchar, eZeeDataType.NAME_SIZE, mstrformula_code.ToString)
            objDataOperation.AddParameter("@formula_name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrFormula_Name.ToString)
            'Nilay (26-Aug-2016) -- Start
            'For Other Language ADD Name1, Name2 
            objDataOperation.AddParameter("@formula_name1", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrFormula_Name1.ToString)
            objDataOperation.AddParameter("@formula_name2", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrFormula_Name2.ToString)
            'Nilay (26-Aug-2016) -- END
            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
            objDataOperation.AddParameter("@isvoid", SqlDbType.bit, eZeeDataType.BIT_SIZE, mblnisvoid.ToString)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.int, eZeeDataType.INT_SIZE, mintvoiduserunkid.ToString)
            objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, IIf(mdtVoiddatetime <> Nothing, mdtVoiddatetime, DBNull.Value))
            objDataOperation.AddParameter("@voidreason", SqlDbType.nvarchar, eZeeDataType.NAME_SIZE, mstrvoidreason.ToString)

            strQ = "UPDATE bgbudgetformula_master SET " & _
              "  formula_code = @formula_code" & _
              ", formula_name = @formula_name" & _
              ", formula_name1 = @formula_name1 " & _
              ", formula_name2 = @formula_name2 " & _
              ", userunkid = @userunkid" & _
              ", isvoid = @isvoid" & _
              ", voiduserunkid = @voiduserunkid" & _
              ", voiddatetime = @voiddatetime" & _
              ", voidreason = @voidreason " & _
            "WHERE budgetformulaunkid = @budgetformulaunkid "
            'Nilay (26-Aug-2016) -- [formula_name1,formula_name2]

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If ATInsertBudgetFormula_Master(objDataOperation, 2) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If


            Dim objBudgetTran As New clsBudgetformula_Tran
            objBudgetTran._Budgetformulaunkid = mintBudgetformulaunkid
            objBudgetTran._Formula_Tran = mdtFormulaTran
            objBudgetTran._Isvoid = False
            objBudgetTran._Voiduserunkid = -1
            objBudgetTran._Voiddatetime = Nothing
            objBudgetTran._Voidreason = ""
            objBudgetTran._Userunkid = mintUserunkid
            objBudgetTran._ClientIP = mstrClientIP
            objBudgetTran._FormName = mstrFormName
            objBudgetTran._HostName = mstrHostName
            objBudgetTran._AuditDate = mdtAuditDate
            objBudgetTran._AuditUserId = mintAuditUserId
objBudgetTran._CompanyUnkid = mintCompanyUnkid
            objBudgetTran._FromWeb = mblnIsWeb
            objBudgetTran._LoginEmployeeunkid = mintLoginEmployeeunkid
            If objBudgetTran.InsertDelete_Formula(objDataOperation) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If
            objBudgetTran = Nothing

            Dim objBugdetTranhead As New clsBudgetformula_head_tran
            objBugdetTranhead._Budgetformulaunkid = mintBudgetformulaunkid
            objBugdetTranhead._Formula_Tranhead = mdtFormula_Tranhead
            objBugdetTranhead._Isvoid = False
            objBugdetTranhead._Voiduserunkid = -1
            objBugdetTranhead._Voiddatetime = Nothing
            objBugdetTranhead._Voidreason = ""
            objBugdetTranhead._Userunkid = mintUserunkid
            objBugdetTranhead._ClientIP = mstrClientIP
            objBugdetTranhead._FormName = mstrFormName
            objBugdetTranhead._HostName = mstrHostName
            objBugdetTranhead._AuditDate = mdtAuditDate
            objBugdetTranhead._AuditUserId = mintAuditUserId
            objBugdetTranhead._CompanyUnkid = mintCompanyUnkid
            objBugdetTranhead._FromWeb = mblnIsWeb
            If objBugdetTranhead.InsertDelete_TranHead(objDataOperation) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If
            objBugdetTranhead = Nothing

            objDataOperation.ReleaseTransaction(True)

            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sohail
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Delete Database Table (bgbudgetformula_master) </purpose>
    Public Function Delete() As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        Dim objDataOperation As New clsDataOperation
        objDataOperation.BindTransaction()

        Try

            Dim objBudgetHeadTran As New clsBudgetformula_head_tran
            objBudgetHeadTran._Budgetformulaunkid = mintBudgetformulaunkid
            objBudgetHeadTran._Voiduserunkid = mintVoiduserunkid
            objBudgetHeadTran._Voidreason = mstrVoidreason
            With objBudgetHeadTran
                ._FormName = mstrFormName
                ._LoginEmployeeunkid = mintLoginEmployeeunkid
                ._ClientIP = mstrClientIP
                ._HostName = mstrHostName
                ._FromWeb = mblnIsWeb
                ._AuditUserId = mintAuditUserId
._CompanyUnkid = mintCompanyUnkid
                ._AuditDate = mdtAuditDate
            End With
            If objBudgetHeadTran.Delete(objDataOperation) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If
            objBudgetHeadTran = Nothing


            Dim objBudgetFormulaTran As New clsBudgetformula_Tran
            objBudgetFormulaTran._Budgetformulaunkid = mintBudgetformulaunkid
            objBudgetFormulaTran._Voiduserunkid = mintVoiduserunkid
            objBudgetFormulaTran._Voidreason = mstrVoidreason
            With objBudgetFormulaTran
                ._FormName = mstrFormName
                ._LoginEmployeeunkid = mintLoginEmployeeunkid
                ._ClientIP = mstrClientIP
                ._HostName = mstrHostName
                ._FromWeb = mblnIsWeb
                ._AuditUserId = mintAuditUserId
._CompanyUnkid = mintCompanyUnkid
                ._AuditDate = mdtAuditDate
            End With
            If objBudgetFormulaTran.Delete(objDataOperation) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If
            objBudgetFormulaTran = Nothing


            strQ = " Update bgbudgetformula_master Set " & _
                      "  isvoid = 1" & _
                      ", voiduserunkid = @voiduserunkid" & _
                      ", voiddatetime = GetDate()" & _
                      ", voidreason = @voidreason " & _
            "WHERE budgetformulaunkid = @budgetformulaunkid "

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@budgetformulaunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintBudgetformulaunkid)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid)
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason)
            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If ATInsertBudgetFormula_Master(objDataOperation, 3) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            objDataOperation.ReleaseTransaction(True)

            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Delete; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sohail
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isUsed(ByVal intUnkid As Integer) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "SELECT  budgetformulaunkid " & _
                    "FROM    bgbudgetformula_head_tran " & _
                    "WHERE   bgbudgetformula_head_tran.isvoid = 0 " & _
                            "AND bgbudgetformula_head_tran.budgetformulaunkid = @budgetformulaunkid " & _
                    "UNION ALL " & _
                    "SELECT  mappedformulaheadunkid " & _
                    "FROM    bgbudgetformulaheadmapping_tran " & _
                    "WHERE   bgbudgetformulaheadmapping_tran.isvoid = 0 " & _
                            "AND bgbudgetformulaheadmapping_tran.mappedformulaheadunkid = @budgetformulaunkid "

            objDataOperation.AddParameter("@budgetformulaunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.tables(0).rows.count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isUsed; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sohail
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isExist(ByVal strCode As String, ByVal strName As String, Optional ByVal intUnkid As Integer = -1) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        Dim objDataOperation As New clsDataOperation
        objDataOperation.ClearParameters()

        Try
            strQ = "SELECT " & _
              "  budgetformulaunkid " & _
              ", formula_code " & _
              ", formula_name " & _
              ", userunkid " & _
              ", isvoid " & _
              ", voiduserunkid " & _
              ", voiddatetime " & _
              ", voidreason " & _
             "FROM bgbudgetformula_master " & _
                     " WHERE 1 = 1 "


            If strCode <> "" Then
                strQ &= " AND formula_code = @formula_code "
                objDataOperation.AddParameter("@formula_code", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strCode)
            End If

            If strName <> "" Then
                strQ &= " AND formula_name = @formula_name "
                objDataOperation.AddParameter("@formula_name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strName)
            End If

            If intUnkid > 0 Then
                strQ &= " AND budgetformulaunkid <> @budgetformulaunkid"
                objDataOperation.AddParameter("@budgetformulaunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)
            End If

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.tables(0).rows.count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isExist; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sohail
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function getComboList(ByVal strTableName As String, ByVal blnAddSelect As Boolean) As DataSet
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try

            If blnAddSelect = True Then
                strQ = "SELECT 0 AS Id, '' AS formula_code, @Select AS Name UNION ALL "
            End If

            strQ &= "SELECT " & _
                  "  budgetformulaunkid AS Id " & _
                  ", formula_code " & _
                  ", formula_name AS Name " & _
             "FROM bgbudgetformula_master " & _
             "WHERE isvoid = 0 "

            objDataOperation.AddParameter("@Select", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 3, "Select"))

            dsList = objDataOperation.ExecQuery(strQ, strTableName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: getComboList; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
        Return dsList
    End Function

    ''' <summary>
    ''' Modify By: Sohail
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Private Function ATInsertBudgetFormula_Master(ByVal objDataOperation As clsDataOperation, ByVal intAuditType As Integer) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Try

            strQ = "INSERT INTO atbgbudgetformula_master ( " & _
                     " budgetformulaunkid " & _
                     ", formula_code " & _
                     ", formula_name " & _
                     ", formula_name1 " & _
                     ", formula_name2 " & _
                     ", AuditType " & _
                     ", audituserunkid " & _
                     ", auditdatetime " & _
                     ", ip " & _
                     ", machine_name " & _
                     ", form_name " & _
                     " " & _
                     " " & _
                     " " & _
                     " " & _
                     " " & _
                     ", isweb " & _
               ") VALUES (" & _
                    " @budgetformulaunkid " & _
                    ", @formula_code " & _
                    ", @formula_name " & _
                    ", @formula_name1 " & _
                    ", @formula_name2 " & _
                    ", @auditType " & _
                    ", @audituserunkid " & _
                    ", getdate() " & _
                    ", @ip " & _
                    ", @machine_name " & _
                    ", @form_name " & _
                    " " & _
                    " " & _
                    " " & _
                    " " & _
                    " " & _
                    ", @isweb " & _
               "); "

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@budgetformulaunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintBudgetformulaunkid.ToString)
            objDataOperation.AddParameter("@formula_code", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrFormula_Code.ToString)
            objDataOperation.AddParameter("@formula_name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrFormula_Name.ToString)
            objDataOperation.AddParameter("@formula_name1", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrFormula_Name1.ToString)
            objDataOperation.AddParameter("@formula_name2", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrFormula_Name2.ToString)
            objDataOperation.AddParameter("@audittype", SqlDbType.Int, eZeeDataType.INT_SIZE, intAuditType.ToString)
            'objDataOperation.AddParameter("@audituserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid)

            'If mstrWebClientIP.ToString().Length <= 0 Then
            '    mstrWebClientIP = getIP()
            'End If

            'If mstrWebHostName.ToString().Length <= 0 Then
            '    mstrWebHostName = getHostName()
            'End If

            'objDataOperation.AddParameter("@ip", SqlDbType.NVarChar, eZeeDataType.IP_SIZE, mstrWebClientIP)
            'objDataOperation.AddParameter("@machine_name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrWebHostName)

            'If mstrWebFormName.Trim.Length <= 0 Then
            '    objDataOperation.AddParameter("@form_name", SqlDbType.NVarChar, 500, mstrFormName)
            '    objDataOperation.AddParameter("@isweb", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsWeb)
            '    
            'Else
            '    objDataOperation.AddParameter("@form_name", SqlDbType.NVarChar, 500, mstrWebFormName)
            '    objDataOperation.AddParameter("@isweb", SqlDbType.Bit, eZeeDataType.BIT_SIZE, True)
            '    objDataOperation.AddParameter("@module_name1", SqlDbType.NVarChar, 500, Language.getMessage(mstrModuleName, 4, "WEB"))
            'End If
            '
            '
            '
            '

            objDataOperation.AddParameter("@audituserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAuditUserId)
            objDataOperation.AddParameter("@ip", SqlDbType.NVarChar, eZeeDataType.IP_SIZE, mstrClientIP)
            objDataOperation.AddParameter("@machine_name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrHostName)
            objDataOperation.AddParameter("@form_name", SqlDbType.NVarChar, 500, mstrFormName)
            objDataOperation.AddParameter("@isweb", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsWeb)

            objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If
            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: ATInsertBudgetFormula_Master; Module Name: " & mstrModuleName)
            Return False
        End Try
    End Function
    
	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Public Shared Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "This Transaction Head Name is already defined. Please define new Transaction Head Name.")
			Language.setMessage(mstrModuleName, 2, "This Transaction Head Code is already defined. Please define new Transaction Head Code.")
			Language.setMessage(mstrModuleName, 3, "Select")
			Language.setMessage(mstrModuleName, 4, "WEB")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class