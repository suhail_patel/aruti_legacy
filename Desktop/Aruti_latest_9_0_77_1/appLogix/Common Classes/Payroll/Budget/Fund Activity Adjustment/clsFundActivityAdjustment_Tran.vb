﻿'************************************************************************************************************************************
'Class Name : clsFundActivityAdjustment_Tran.vb
'Purpose    :
'Date       : 18/07/2016
'Written By : Sohail
'Modified   :
'************************************************************************************************************************************

Imports eZeeCommonLib
Imports eZee.Common.eZeeForm
''' <summary>
''' Purpose: 
''' Developer: Sohail
''' </summary>
Public Class clsFundActivityAdjustment_Tran
    Private Shared ReadOnly mstrModuleName As String = "clsFundActivityAdjustment_Tran"
    Dim objDataOperation As clsDataOperation
    Dim mstrMessage As String = ""

#Region " Private variables "
    Private mintFundActivityAdjustmentunkid As Integer
    Private mintFundactivityunkid As Integer
    Private mdtTransactionDate As DateTime
    Private mdecCurrentBalance As Decimal = 0
    Private mdecIncrDecrAmount As Decimal = 0
    Private mstrRemark As String = String.Empty
    Private mdecNewBalance As Decimal = 0
    Private mintUserunkid As Integer = -1
    Private mblnIsvoid As Boolean
    Private mintVoiduserunkid As Integer = -1
    Private mdtVoiddatetime As Date = Nothing
    Private mstrVoidreason As String = String.Empty
    'Private mstrWebClientIP As String = String.Empty
    'Private mstrWebHostName As String = String.Empty
    'Private mstrWebFormName As String = String.Empty
    Private mintLoginEmployeeunkid As Integer = -1
    'Sohail (23 May 2017) -- Start
    'Enhancement - 67.1 - Link budget with Payroll.
    Private mintPaymenttranunkid As Integer = 0
    Private mintGlobalvocunkid As Integer = 0
    'Sohail (23 May 2017) -- End
#End Region

#Region " Properties "

    'S.SANDEEP [28-May-2018] -- START 
    'ISSUE/ENHANCEMENT : {Audit Trails} 
    Private mstrFormName As String = String.Empty
    Public WriteOnly Property _FormName() As String
        Set(ByVal value As String)
            mstrFormName = value
        End Set
    End Property

    Private mstrClientIP As String = ""
    Public WriteOnly Property _ClientIP() As String
        Set(ByVal value As String)
            mstrClientIP = value
        End Set
    End Property

    Private mstrHostName As String = ""
    Public WriteOnly Property _HostName() As String
        Set(ByVal value As String)
            mstrHostName = value
        End Set
    End Property

    Private mblnIsWeb As Boolean = False
    Public WriteOnly Property _FromWeb() As Boolean
        Set(ByVal value As Boolean)
            mblnIsWeb = value
        End Set
    End Property

    Private mintAuditUserId As Integer = 0
    Public WriteOnly Property _AuditUserId() As Integer
        Set(ByVal value As Integer)
            mintAuditUserId = value
        End Set
    End Property

    Private mintCompanyUnkid As Integer = 0
    Public WriteOnly Property _CompanyUnkid() As Integer
        Set(ByVal value As Integer)
            mintCompanyUnkid = value
        End Set
    End Property

    Private mdtAuditDate As DateTime = Now
    Public WriteOnly Property _AuditDate() As DateTime
        Set(ByVal value As DateTime)
            mdtAuditDate = value
        End Set
    End Property

    Private xDataOpr As clsDataOperation = Nothing
    Public Property _xDataOpr() As clsDataOperation
        Get
            Return xDataOpr
        End Get
        Set(ByVal value As clsDataOperation)
            xDataOpr = value
        End Set
    End Property

    Private mintVoidlogingemployeeunkid As Integer = 0
    Public Property _Voidloginemployeeunkid() As Integer
        Get
            Return mintVoidlogingemployeeunkid
        End Get
        Set(ByVal value As Integer)
            mintVoidlogingemployeeunkid = value
        End Set
    End Property
    'S.SANDEEP [28-May-2018] -- END 

    Public ReadOnly Property _Message() As String
        Get
            Return mstrMessage
        End Get
    End Property

    ''' <summary>
    ''' Purpose: Get or Set fundactivityadjustmentunkid
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Fundactivityadjustmentunkid() As Integer
        Get
            Return mintFundActivityAdjustmentunkid
        End Get
        Set(ByVal value As Integer)
            mintFundActivityAdjustmentunkid = value
            Call GetData()
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set fundactivityunkid
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _FundActivityunkid() As Integer
        Get
            Return mintFundactivityunkid
        End Get
        Set(ByVal value As Integer)
            mintFundactivityunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set transactiondate
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _TransactionDate() As Date
        Get
            Return mdtTransactionDate
        End Get
        Set(ByVal value As Date)
            mdtTransactionDate = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set currentbalance
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _CurrentBalance() As Decimal
        Get
            Return mdecCurrentBalance
        End Get
        Set(ByVal value As Decimal)
            mdecCurrentBalance = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set incrdecramount
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _IncrDecrAmount() As Decimal
        Get
            Return mdecIncrDecrAmount
        End Get
        Set(ByVal value As Decimal)
            mdecIncrDecrAmount = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set remark
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Remark() As String
        Get
            Return mstrRemark
        End Get
        Set(ByVal value As String)
            mstrRemark = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set newbalance
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _NewBalance() As Decimal
        Get
            Return mdecNewBalance
        End Get
        Set(ByVal value As Decimal)
            mdecNewBalance = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set userunkid
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Userunkid() As Integer
        Get
            Return mintUserunkid
        End Get
        Set(ByVal value As Integer)
            mintUserunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isvoid
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Isvoid() As Boolean
        Get
            Return mblnIsvoid
        End Get
        Set(ByVal value As Boolean)
            mblnIsvoid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiduserunkid
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Voiduserunkid() As Integer
        Get
            Return mintVoiduserunkid
        End Get
        Set(ByVal value As Integer)
            mintVoiduserunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiddatetime
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Voiddatetime() As Date
        Get
            Return mdtVoiddatetime
        End Get
        Set(ByVal value As Date)
            mdtVoiddatetime = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voidreason
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Voidreason() As String
        Get
            Return mstrVoidreason
        End Get
        Set(ByVal value As String)
            mstrVoidreason = value
        End Set
    End Property

    'Public Property _WebClientIP() As String
    '    Get
    '        Return mstrWebClientIP
    '    End Get
    '    Set(ByVal value As String)
    '        mstrWebClientIP = value
    '    End Set
    'End Property

    'Public Property _WebHostName() As String
    '    Get
    '        Return mstrWebHostName
    '    End Get
    '    Set(ByVal value As String)
    '        mstrWebHostName = value
    '    End Set
    'End Property

    'Public Property _WebFormName() As String
    '    Get
    '        Return mstrWebFormName
    '    End Get
    '    Set(ByVal value As String)
    '        mstrWebFormName = value
    '    End Set
    'End Property

    ''' <summary>
    ''' Purpose: Get or Set loginemployeeunkid
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _LoginEmployeeunkid() As Integer
        Get
            Return mintLoginEmployeeunkid
        End Get
        Set(ByVal value As Integer)
            mintLoginEmployeeunkid = value
        End Set
    End Property

    'Sohail (23 May 2017) -- Start
    'Enhancement - 67.1 - Link budget with Payroll.
    Public Property _Paymenttranunkid() As Integer
        Get
            Return mintPaymenttranunkid
        End Get
        Set(ByVal value As Integer)
            mintPaymenttranunkid = value
        End Set
    End Property

    Public Property _Globalvocunkid() As Integer
        Get
            Return mintGlobalvocunkid
        End Get
        Set(ByVal value As Integer)
            mintGlobalvocunkid = value
        End Set
    End Property
    'Sohail (23 May 2017) -- End

#End Region

    ''' <summary>
    ''' Modify By: Sohail
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Sub GetData(Optional ByVal objDataOp As clsDataOperation = Nothing)
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        If objDataOp IsNot Nothing Then
            objDataOperation = objDataOp
        Else
            objDataOperation = New clsDataOperation
        End If

        Try
            strQ = "SELECT " & _
                      "  fundactivityadjustmentunkid " & _
                      ", fundactivityunkid " & _
                      ", transactiondate " & _
                      ", currentbalance " & _
                      ", incrdecramount " & _
                      ", remark " & _
                      ", newbalance " & _
                      ", userunkid " & _
                      ", isvoid " & _
                      ", voiduserunkid " & _
                      ", voiddatetime " & _
                      ", voidreason " & _
                      ", ISNULL(paymenttranunkid, 0) AS paymenttranunkid " & _
                      ", ISNULL(globalvocunkid, 0) AS globalvocunkid " & _
                   "FROM bgfundactivityadjustment_tran " & _
                   "WHERE fundactivityadjustmentunkid = @fundactivityadjustmentunkid "
            'Sohail (23 May 2017) - [paymenttranunkid, globalvocunkid]

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@fundactivityadjustmentunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintFundActivityAdjustmentunkid.ToString)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For Each dtRow As DataRow In dsList.Tables(0).Rows
                mintFundActivityAdjustmentunkid = CInt(dtRow.Item("fundactivityadjustmentunkid"))
                mintFundactivityunkid = CInt(dtRow.Item("fundactivityunkid"))
                If IsDBNull(dtRow.Item("transactiondate")) Then
                    mdtTransactionDate = Nothing
                Else
                    mdtTransactionDate = dtRow.Item("transactiondate")
                End If
                mdecCurrentBalance = dtRow.Item("currentbalance")
                mdecIncrDecrAmount = dtRow.Item("incrdecramount")
                mstrRemark = dtRow.Item("remark").ToString
                mdecNewBalance = dtRow.Item("newbalance")
                mintUserunkid = CInt(dtRow.Item("userunkid"))
                mblnIsvoid = CBool(dtRow.Item("isvoid"))
                mintVoiduserunkid = CInt(dtRow.Item("voiduserunkid"))
                If IsDBNull(dtRow.Item("voiddatetime")) Then
                    mdtVoiddatetime = Nothing
                Else
                    mdtVoiddatetime = dtRow.Item("voiddatetime")
                End If
                mstrVoidreason = dtRow.Item("voidreason").ToString
                'Sohail (23 May 2017) -- Start
                'Enhancement - 67.1 - Link budget with Payroll.
                mintPaymenttranunkid = CInt(dtRow.Item("paymenttranunkid"))
                mintGlobalvocunkid = CInt(dtRow.Item("globalvocunkid"))
                'Sohail (23 May 2017) -- End
                Exit For
            Next
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetData; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If objDataOp Is Nothing Then objDataOperation = Nothing
        End Try
    End Sub

    ''' <summary>
    ''' Modify By: Sohail
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function GetList(ByVal strTableName As String, Optional ByVal strFilter As String = "") As DataSet
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "SELECT  bgfundactivityadjustment_tran.fundactivityadjustmentunkid  " & _
                          ", bgfundactivityadjustment_tran.fundactivityunkid " & _
                          ", bgfundactivity_tran.fundprojectcodeunkid " & _
                          ", bgfundprojectcode_master.fundprojectcode " & _
                          ", bgfundprojectcode_master.fundprojectname " & _
                          ", ISNULL(bgfundactivity_tran.activity_code, '') AS activity_code " & _
                          ", ISNULL(bgfundactivity_tran.activity_name, '') AS activity_name " & _
                          ", bgfundactivityadjustment_tran.transactiondate " & _
                          ", ISNULL(bgfundactivityadjustment_tran.currentbalance, 0) AS currentbalance " & _
                          ", ISNULL(bgfundactivityadjustment_tran.incrdecramount, 0) AS incrdecramount " & _
                          ", ISNULL(bgfundactivityadjustment_tran.newbalance, 0) AS newbalance " & _
                          ", ISNULL(bgfundactivityadjustment_tran.remark, '') AS remark " & _
                          ", bgfundactivityadjustment_tran.userunkid " & _
                          ", bgfundactivityadjustment_tran.isvoid " & _
                          ", bgfundactivityadjustment_tran.voiduserunkid " & _
                          ", bgfundactivityadjustment_tran.voiddatetime " & _
                          ", bgfundactivityadjustment_tran.voidreason " & _
                          ", ISNULL(bgfundactivityadjustment_tran.paymenttranunkid, 0) AS paymenttranunkid " & _
                          ", ISNULL(bgfundactivityadjustment_tran.globalvocunkid, 0) AS globalvocunkid " & _
                    "FROM    bgfundactivityadjustment_tran " & _
                            "LEFT JOIN bgfundactivity_tran ON bgfundactivityadjustment_tran.fundactivityunkid = bgfundactivity_tran.fundactivityunkid " & _
                            "LEFT JOIN bgfundprojectcode_master ON bgfundactivity_tran.fundprojectcodeunkid = bgfundprojectcode_master.fundprojectcodeunkid " & _
                    "WHERE   bgfundactivityadjustment_tran.isvoid = 0 " & _
                            "AND bgfundactivity_tran.isvoid = 0 " & _
                            "AND bgfundprojectcode_master.isvoid = 0 " & _
                    "AND bgfundactivity_tran.fundprojectcodeunkid > 0 "
            'Sohail (23 May 2017) - [paymenttranunkid, globalvocunkid]
            'Sohail (22 Nov 2016) - [fundprojectcodeunkid, fundprojectcode, fundprojectname, activity_code]

            If strFilter.Trim.Length > 0 Then
                strQ &= " AND " & strFilter
            End If

            'Sohail (22 Nov 2016) -- Start
            'Enhancement #24 -  65.1 - Increase / decrease fund project code balance on activity adjustment as per comment given by andrew.
            'strQ &= " ORDER BY ISNULL(bgfundactivity_tran.activity_name, ''), transactiondate DESC, fundactivityadjustmentunkid DESC "
            strQ &= " ORDER BY bgfundprojectcode_master.fundprojectname, ISNULL(bgfundactivity_tran.activity_code, ''), ISNULL(bgfundactivity_tran.activity_name, ''), transactiondate DESC, fundactivityadjustmentunkid DESC "
            'Sohail (22 Nov 2016) -- End

            dsList = objDataOperation.ExecQuery(strQ, strTableName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
        Return dsList
    End Function

    ''' <summary>
    ''' Modify By: Sohail
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function GetLastCurrentBalance(ByVal strTableName As String, ByVal intFundactivityunkid As Integer, ByVal dtAsOnDate As Date, _
                                          Optional ByVal objDataOp As clsDataOperation = Nothing) As DataSet
        'Sohail (23 May 2017) - [dtAsOnDate]

        Dim dsList As New DataSet
        Dim strQ As String = String.Empty
        Dim exForce As Exception
        Try
            If objDataOp IsNot Nothing Then
                objDataOperation = objDataOp
            Else
                objDataOperation = New clsDataOperation
            End If
            objDataOperation.ClearParameters() 'Sohail (23 May 2017)

            'Sohail (23 May 2017) -- Start
            'Enhancement - 67.1 - Link budget with Payroll.
            'strQ = " SELECT " & _
            '            "   fundactivityadjustmentunkid " & _
            '            ",  fundactivityunkid " & _
            '            ",  transactiondate " & _
            '            ",  currentbalance " & _
            '            ",  incrdecramount " & _
            '            ",  remark " & _
            '            ",  newbalance " & _
            '       " FROM bgfundactivityadjustment_tran " & _
            '       " WHERE isvoid = 0 AND fundactivityunkid = @fundactivityunkid "           
            strQ = "SELECT  * " & _
                    "FROM    ( SELECT    fundactivityadjustmentunkid  " & _
                        ",  fundactivityunkid " & _
                        ",  transactiondate " & _
                        ",  currentbalance " & _
                        ",  incrdecramount " & _
                        ",  remark " & _
                        ",  newbalance " & _
                                      ", ISNULL(paymenttranunkid, 0) AS paymenttranunkid " & _
                                      ", ISNULL(globalvocunkid, 0) AS globalvocunkid " & _
                                      ", DENSE_RANK() OVER ( PARTITION BY bgfundactivityadjustment_tran.fundactivityunkid ORDER BY bgfundactivityadjustment_tran.transactiondate DESC, bgfundactivityadjustment_tran.fundactivityadjustmentunkid DESC ) AS ROWNO " & _
                              "FROM      bgfundactivityadjustment_tran " & _
                              "WHERE     isvoid = 0 "
            'Sohail (23 May 2017) -- End

            'Sohail (23 May 2017) -- Start
            'Enhancement - 67.1 - Link budget with Payroll.
            If intFundactivityunkid > 0 Then
                strQ &= " AND bgfundactivityadjustment_tran.fundactivityunkid = @fundactivityunkid "
            End If

            If dtAsOnDate <> Nothing Then
                strQ &= " AND CONVERT(CHAR(8), bgfundactivityadjustment_tran.transactiondate, 112) <= @transactiondate "
                objDataOperation.AddParameter("@transactiondate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(dtAsOnDate).ToString)
            End If
            'Sohail (23 May 2017) -- End

            'Sohail (23 May 2017) -- Start
            'Enhancement - 67.1 - Link budget with Payroll.
            'strQ &= " ORDER BY transactiondate DESC, fundactivityadjustmentunkid DESC "
            strQ &= "       ) AS A " & _
                            "WHERE   A.ROWNO = 1 "
            'Sohail (23 May 2017) -- End

            'objDataOperation.ClearParameters() 'Sohail (23 May 2017)
            objDataOperation.AddParameter("@fundactivityunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intFundactivityunkid.ToString)

            dsList = objDataOperation.ExecQuery(strQ, strTableName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetLastCurrentBalance", mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If objDataOp Is Nothing Then objDataOperation = Nothing
        End Try
        Return dsList
    End Function


    ''' <summary>
    ''' Modify By: Sohail
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> INSERT INTO Database Table (bgfundactivityadjustment_tran) </purpose>
    Public Function Insert(ByVal dtCurrentDateTime As DateTime, ByVal objDataOp As clsDataOperation, Optional ByVal blnIgnorePaymentTransactions As Boolean = False) As Boolean
        'Sohail (23 May 2017) -- Start
        'Enhancement - 67.1 - Link budget with Payroll.
        'If isExist(mdtTransactionDate, mintFundactivityunkid) Then
        If isExist(mdtTransactionDate, mintFundactivityunkid, , blnIgnorePaymentTransactions) Then
            'Sohail (23 May 2017) -- End
            mstrMessage = Language.getMessage(mstrModuleName, 2, "Please define another Transaction date. Reason: Transaction date must be greater than last fund adjustments date.")
            Return False
        End If

        'Sohail (22 Nov 2016) -- Start
        'Enhancement #24 -  65.1 - Increase / decrease fund project code balance on activity adjustment as per comment given by andrew.
        If isLastTransaction(mintFundactivityunkid, mdtTransactionDate) = False Then
            mstrMessage = Language.getMessage(mstrModuleName, 2, "Please define another Transaction date. Reason: Transaction date must be greater than last fund adjustments date.") & " " & mdtTransactionDate.ToShortDateString
            Return False
        End If

        Dim objFundAdjust As New clsFundAdjustment_Tran
        Dim objFundActivity As New clsfundactivity_Tran
        objFundActivity._Fundactivityunkid = mintFundactivityunkid
        If objFundAdjust.isLastTransaction(objFundActivity._FundProjectCodeunkid, mdtTransactionDate) = False Then
            mstrMessage = Language.getMessage(mstrModuleName, 2, "Please define another Transaction date. Reason: Transaction date must be greater than last fund adjustments date.") & " " & mdtTransactionDate.ToShortDateString
            Return False
        End If
        'Sohail (22 Nov 2016) -- End

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        'Sohail (22 Nov 2016) -- Start
        'Enhancement #24 -  65.1 - Increase / decrease fund project code balance on activity adjustment as per comment given by andrew.
        'objDataOperation = New clsDataOperation
        'objDataOperation.BindTransaction()
        If objDataOp Is Nothing Then
        objDataOperation = New clsDataOperation
        objDataOperation.BindTransaction()
        Else
            objDataOperation = objDataOp
        End If
        objDataOperation.ClearParameters()
        'Sohail (22 Nov 2016) -- End

        Try
            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@fundactivityunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintFundactivityunkid.ToString)
            objDataOperation.AddParameter("@transactiondate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, IIf(mdtTransactionDate <> Nothing, mdtTransactionDate, DBNull.Value))
            objDataOperation.AddParameter("@currentbalance", SqlDbType.Decimal, eZeeDataType.FLOAT_SIZE, mdecCurrentBalance.ToString)
            objDataOperation.AddParameter("@incrdecramount", SqlDbType.Decimal, eZeeDataType.FLOAT_SIZE, mdecIncrDecrAmount.ToString)
            objDataOperation.AddParameter("@remark", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, mstrRemark.ToString)
            objDataOperation.AddParameter("@newbalance", SqlDbType.Decimal, eZeeDataType.FLOAT_SIZE, mdecNewBalance.ToString)
            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
            objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, IIf(mdtVoiddatetime <> Nothing, mdtVoiddatetime, DBNull.Value))
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)
            'Sohail (23 May 2017) -- Start
            'Enhancement - 67.1 - Link budget with Payroll.
            objDataOperation.AddParameter("@paymenttranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPaymenttranunkid.ToString)
            objDataOperation.AddParameter("@globalvocunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintGlobalvocunkid.ToString)
            'Sohail (23 May 2017) -- End

            strQ = "INSERT INTO bgfundactivityadjustment_tran ( " & _
              "  fundactivityunkid " & _
              ", transactiondate " & _
              ", currentbalance " & _
              ", incrdecramount " & _
              ", remark " & _
              ", newbalance " & _
              ", userunkid " & _
              ", isvoid " & _
              ", voiduserunkid " & _
              ", voiddatetime " & _
              ", voidreason" & _
              ", paymenttranunkid" & _
              ", globalvocunkid" & _
            ") VALUES (" & _
              "  @fundactivityunkid " & _
              ", @transactiondate " & _
              ", @currentbalance " & _
              ", @incrdecramount " & _
              ", @remark " & _
              ", @newbalance " & _
              ", @userunkid " & _
              ", @isvoid " & _
              ", @voiduserunkid " & _
              ", @voiddatetime " & _
              ", @voidreason" & _
              ", @paymenttranunkid" & _
              ", @globalvocunkid" & _
            "); SELECT @@identity"
            'Sohail (23 May 2017) - [paymenttranunkid, globalvocunkid]

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            mintFundActivityAdjustmentunkid = dsList.Tables(0).Rows(0).Item(0)
            Call GetData(objDataOperation)

            If InsertATFundActivityAdjustmentTran(objDataOperation, enAuditType.ADD, dtCurrentDateTime) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Dim objFundactivityMaster As New clsfundactivity_Tran
            objFundactivityMaster._Fundactivityunkid = mintFundactivityunkid
            objFundactivityMaster._Amount = mdecNewBalance
            With objFundactivityMaster
                ._FormName = mstrFormName
                ._LoginEmployeeunkid = mintLoginEmployeeunkid
                ._ClientIP = mstrClientIP
                ._HostName = mstrHostName
                ._FromWeb = mblnIsWeb
                ._AuditUserId = mintAuditUserId
._CompanyUnkid = mintCompanyUnkid
                ._AuditDate = mdtAuditDate
            End With
            If objFundactivityMaster.Update(dtCurrentDateTime, objDataOperation) = False Then
                If objFundactivityMaster._Message <> "" Then
                    exForce = New Exception(objFundactivityMaster._Message)
                    Throw exForce
                End If
                If objDataOp Is Nothing Then objDataOperation.ReleaseTransaction(False)
                Return False
            End If

            'Sohail (22 Nov 2016) -- Start
            'Enhancement #24 -  65.1 - Increase / decrease fund project code balance on activity adjustment as per comment given by andrew.
            If Not (mintPaymenttranunkid > 0 OrElse mintGlobalvocunkid > 0) Then 'Sohail (23 May 2017)
            Dim objFundAdjustment As New clsFundAdjustment_Tran
            Dim objProjectCode As New clsFundProjectCode
            objProjectCode._FundProjectCodeunkid = objFundactivityMaster._FundProjectCodeunkid

            'mintFundSourceunkid = -1
            objFundAdjustment._FundProjectCodeunkid = objFundactivityMaster._FundProjectCodeunkid
            objFundAdjustment._TransactionDate = mdtTransactionDate
            objFundAdjustment._CurrentBalance = objProjectCode._CurrentCeilingBalance
            objFundAdjustment._IncrDecrAmount = mdecIncrDecrAmount * -1
            objFundAdjustment._NewBalance = objProjectCode._CurrentCeilingBalance + (mdecIncrDecrAmount * -1)
            objFundAdjustment._Remark = mstrRemark
            objFundAdjustment._Userunkid = mintUserunkid
            objFundAdjustment._Isvoid = mblnIsvoid
            objFundAdjustment._Voiduserunkid = mintVoiduserunkid
            If mdtVoiddatetime = Nothing Then
                objFundAdjustment._Voiddatetime = Nothing
            Else
                objFundAdjustment._Voiddatetime = mdtVoiddatetime
            End If
            objFundAdjustment._Voidreason = mstrVoidreason
            objFundAdjustment._Fundactivityadjustmentunkid = mintFundActivityAdjustmentunkid
                With objFundAdjustment
                    ._FormName = mstrFormName
                    ._LoginEmployeeunkid = mintLoginEmployeeunkid
                    ._ClientIP = mstrClientIP
                    ._HostName = mstrHostName
                    ._FromWeb = mblnIsWeb
                    ._AuditUserId = mintAuditUserId
._CompanyUnkid = mintCompanyUnkid
                    ._AuditDate = mdtAuditDate
                End With
            If objFundAdjustment.Insert(dtCurrentDateTime, objDataOperation) = False Then
                If objDataOp Is Nothing Then objDataOperation.ReleaseTransaction(False)
                Return False
            End If
            'Sohail (22 Nov 2016) -- End
            End If 'Sohail (23 May 2017)

            If objDataOp Is Nothing Then objDataOperation.ReleaseTransaction(True)
            Return True
        Catch ex As Exception
            If objDataOp Is Nothing Then objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Insert; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If objDataOp Is Nothing Then objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sohail
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Update Database Table (bgfundactivityadjustment_tran) </purpose>
    Public Function Update(ByVal dtCurrentDateTime As DateTime, ByVal objDataOp As clsDataOperation) As Boolean
        'If isExist(mstrName, mintfundactivityadjustmentunkid) Then
        '    mstrMessage = "<Message>"
        '    Return False
        'End If

        'Sohail (22 Nov 2016) -- Start
        'Enhancement #24 -  65.1 - Increase / decrease fund project code balance on activity adjustment as per comment given by andrew.
        If isLastTransaction(mintFundactivityunkid, mdtTransactionDate, mintFundActivityAdjustmentunkid) = False Then
            mstrMessage = Language.getMessage(mstrModuleName, 2, "Please define another Transaction date. Reason: Transaction date must be greater than last fund adjustments date.") & " " & mdtTransactionDate.ToShortDateString
            Return False
        End If

        Dim objFundAdjust As New clsFundAdjustment_Tran
        Dim intFundAdjustment As Integer = objFundAdjust.GetFundAdjustmentUnkId(mintFundActivityAdjustmentunkid)
        Dim objFundActivity As New clsfundactivity_Tran
        objFundActivity._Fundactivityunkid = mintFundactivityunkid
        If objFundAdjust.isLastTransaction(objFundActivity._FundProjectCodeunkid, mdtTransactionDate, intFundAdjustment) = False Then
            mstrMessage = Language.getMessage(mstrModuleName, 2, "Please define another Transaction date. Reason: Transaction date must be greater than last fund adjustments date.") & " " & mdtTransactionDate.ToShortDateString
            Return False
        End If
        'Sohail (22 Nov 2016) -- End

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        'Sohail (22 Nov 2016) -- Start
        'Enhancement #24 -  65.1 - Increase / decrease fund project code balance on activity adjustment as per comment given by andrew.
        'objDataOperation = New clsDataOperation
        'objDataOperation.BindTransaction()
        If objDataOp Is Nothing Then
        objDataOperation = New clsDataOperation
        objDataOperation.BindTransaction()
        Else
            objDataOperation = objDataOp
        End If
        objDataOperation.ClearParameters()
        'Sohail (22 Nov 2016) -- End

        Try
            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@fundactivityadjustmentunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintFundActivityAdjustmentunkid.ToString)
            objDataOperation.AddParameter("@fundactivityunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintFundactivityunkid.ToString)
            objDataOperation.AddParameter("@transactiondate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, IIf(mdtTransactionDate <> Nothing, mdtTransactionDate, DBNull.Value))
            objDataOperation.AddParameter("@currentbalance", SqlDbType.Decimal, eZeeDataType.FLOAT_SIZE, mdecCurrentBalance.ToString)
            objDataOperation.AddParameter("@incrdecramount", SqlDbType.Decimal, eZeeDataType.FLOAT_SIZE, mdecIncrDecrAmount.ToString)
            objDataOperation.AddParameter("@remark", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, mstrRemark.ToString)
            objDataOperation.AddParameter("@newbalance", SqlDbType.Decimal, eZeeDataType.FLOAT_SIZE, mdecNewBalance.ToString)
            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
            objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, IIf(mdtVoiddatetime <> Nothing, mdtVoiddatetime, DBNull.Value))
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)
            'Sohail (23 May 2017) -- Start
            'Enhancement - 67.1 - Link budget with Payroll.
            objDataOperation.AddParameter("@paymenttranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPaymenttranunkid.ToString)
            objDataOperation.AddParameter("@globalvocunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintGlobalvocunkid.ToString)
            'Sohail (23 May 2017) -- End

            strQ = "UPDATE bgfundactivityadjustment_tran SET " & _
                      "  fundactivityunkid = @fundactivityunkid" & _
                      ", transactiondate = @transactiondate" & _
                      ", currentbalance = @currentbalance" & _
                      ", incrdecramount = @incrdecramount" & _
                      ", remark = @remark" & _
                      ", newbalance = @newbalance" & _
                      ", userunkid = @userunkid" & _
                      ", isvoid = @isvoid" & _
                      ", voiduserunkid = @voiduserunkid" & _
                      ", voiddatetime = @voiddatetime" & _
                      ", voidreason = @voidreason " & _
                      ", paymenttranunkid = @paymenttranunkid " & _
                      ", globalvocunkid = @globalvocunkid " & _
                   "WHERE fundactivityadjustmentunkid = @fundactivityadjustmentunkid "
            'Sohail (23 May 2017) - [paymenttranunkid, globalvocunkid]

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            'S.SANDEEP [28-May-2018] -- START
            'ISSUE/ENHANCEMENT : {Audit Trails} 
            Dim objCommonATLog As New clsCommonATLog
            objCommonATLog._FormName = mstrFormName
            objCommonATLog._LoginEmployeeUnkid = mintLoginEmployeeunkid
            objCommonATLog._ClientIP = mstrClientIP
            objCommonATLog._HostName = mstrHostName
            objCommonATLog._FromWeb = mblnIsWeb
            objCommonATLog._AuditUserId = mintAuditUserId
objCommonATLog._CompanyUnkid = mintCompanyUnkid
            objCommonATLog._AuditDate = mdtAuditDate
            'S.SANDEEP [28-May-2018] -- END

            If objCommonATLog.IsTableDataUpdateExists("atbgfundactivityadjustment_tran", "bgfundactivityadjustment_tran", mintFundActivityAdjustmentunkid, _
                                                      "fundactivityadjustmentunkid", 2, objDataOperation) = True Then

                If InsertATFundActivityAdjustmentTran(objDataOperation, enAuditType.EDIT, dtCurrentDateTime) = False Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If
            End If

            Dim objFundactivityMaster As New clsfundactivity_Tran
            objFundactivityMaster._fundactivityunkid = mintFundactivityunkid
            objFundactivityMaster._Amount = mdecNewBalance
            With objFundactivityMaster
                ._FormName = mstrFormName
                ._LoginEmployeeunkid = mintLoginEmployeeunkid
                ._ClientIP = mstrClientIP
                ._HostName = mstrHostName
                ._FromWeb = mblnIsWeb
                ._AuditUserId = mintAuditUserId
._CompanyUnkid = mintCompanyUnkid
                ._AuditDate = mdtAuditDate
            End With
            If objFundactivityMaster.Update(dtCurrentDateTime, objDataOperation) = False Then
                If objFundactivityMaster._Message <> "" Then
                    exForce = New Exception(objFundactivityMaster._Message)
                    Throw exForce
                End If
                If objDataOp Is Nothing Then objDataOperation.ReleaseTransaction(False)
            End If

            'Sohail (22 Nov 2016) -- Start
            'Enhancement #24 -  65.1 - Increase / decrease fund project code balance on activity adjustment as per comment given by andrew.
            If intFundAdjustment > 0 Then
                objFundAdjust._FundAdjustmentunkid = intFundAdjustment
                objFundAdjust._IncrDecrAmount = mdecIncrDecrAmount * -1
                objFundAdjust._NewBalance = objFundAdjust._CurrentBalance + (mdecIncrDecrAmount * -1)
                objFundAdjust._Remark = mstrRemark
                With objFundAdjust
                    ._FormName = mstrFormName
                    ._LoginEmployeeunkid = mintLoginEmployeeunkid
                    ._ClientIP = mstrClientIP
                    ._HostName = mstrHostName
                    ._FromWeb = mblnIsWeb
                    ._AuditUserId = mintAuditUserId
._CompanyUnkid = mintCompanyUnkid
                    ._AuditDate = mdtAuditDate
                End With
                If objFundAdjust.Update(dtCurrentDateTime, objDataOperation) = False Then
                    If objDataOp Is Nothing Then objDataOperation.ReleaseTransaction(False)
                    Return False
                End If
            End If
            'Sohail (22 Nov 2016) -- End

            If objDataOp Is Nothing Then objDataOperation.ReleaseTransaction(True)
            Return True
        Catch ex As Exception
            If objDataOp Is Nothing Then objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If objDataOp Is Nothing Then objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sohail
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Delete Database Table (bgfundactivityadjustment_tran) </purpose>
    Public Function Delete(ByVal intfundactivityadjustmentunkid As Integer, ByVal dtCurrentDateTime As DateTime, ByVal objDataOp As clsDataOperation) As Boolean
        'If isUsed(intUnkid) Then
        '    mstrMessage = "<Message>"
        '    Return False
        'End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        'Sohail (22 Nov 2016) -- Start
        'Enhancement #24 -  65.1 - Increase / decrease fund project code balance on activity adjustment as per comment given by andrew.
        'objDataOperation = New clsDataOperation
        'objDataOperation.BindTransaction()
        If objDataOp Is Nothing Then
        objDataOperation = New clsDataOperation
        objDataOperation.BindTransaction()
        Else
            objDataOperation = objDataOp
        End If
        objDataOperation.ClearParameters()
        'Sohail (22 Nov 2016) -- End

        Try
            strQ = "UPDATE bgfundactivityadjustment_tran SET " & _
                        "  isvoid = @isvoid " & _
                        ", voiduserunkid = @voiduserunkid " & _
                        ", voiddatetime = @voiddatetime " & _
                        ", voidreason = @voidreason " & _
                   "WHERE fundactivityadjustmentunkid = @fundactivityadjustmentunkid "

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@fundactivityadjustmentunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intfundactivityadjustmentunkid)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
            objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, IIf(mdtVoiddatetime <> Nothing, mdtVoiddatetime, DBNull.Value))
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            mintFundActivityAdjustmentunkid = intfundactivityadjustmentunkid
            Call GetData(objDataOperation)

            If InsertATFundActivityAdjustmentTran(objDataOperation, enAuditType.DELETE, dtCurrentDateTime) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Dim objFundActivityMaster As New clsfundactivity_Tran

            'Sohail (23 May 2017) -- Start
            'Enhancement - 67.1 - Link budget with Payroll.
            'dsList = GetLastCurrentBalance("List", mintFundactivityunkid, objDataOperation)
            dsList = GetLastCurrentBalance("List", mintFundactivityunkid, mdtTransactionDate, objDataOperation)
            'Sohail (23 May 2017) -- End
            objFundActivityMaster._Fundactivityunkid = mintFundactivityunkid
            If dsList.Tables("List").Rows.Count > 0 Then
                objFundActivityMaster._Amount = CDec(dsList.Tables("List").Rows(0).Item("newbalance"))
            Else
                objFundActivityMaster._Amount = 0
            End If

            With objFundActivityMaster
                ._FormName = mstrFormName
                ._LoginEmployeeunkid = mintLoginEmployeeunkid
                ._ClientIP = mstrClientIP
                ._HostName = mstrHostName
                ._FromWeb = mblnIsWeb
                ._AuditUserId = mintAuditUserId
._CompanyUnkid = mintCompanyUnkid
                ._AuditDate = mdtAuditDate
            End With
            If objFundActivityMaster.Update(dtCurrentDateTime, objDataOperation) = False Then
                If objFundActivityMaster._Message <> "" Then
                    exForce = New Exception(objFundActivityMaster._Message)
                    Throw exForce
                End If
                'Sohail (22 Nov 2016) -- Start
                'Enhancement #24 -  65.1 - Increase / decrease fund project code balance on activity adjustment as per comment given by andrew.
                If objDataOp Is Nothing Then objDataOperation.ReleaseTransaction(False)
                Return False
                'Sohail (22 Nov 2016) -- End
            End If

            'Sohail (22 Nov 2016) -- Start
            'Enhancement #24 -  65.1 - Increase / decrease fund project code balance on activity adjustment as per comment given by andrew.
            Dim objFundAdjust As New clsFundAdjustment_Tran
            Dim intFundAdjust As Integer = objFundAdjust.GetFundAdjustmentUnkId(mintFundActivityAdjustmentunkid)
            If intFundAdjust > 0 Then
                objFundAdjust._FundAdjustmentunkid = intFundAdjust
                If objFundAdjust._Isvoid = False Then
                    objFundAdjust._Isvoid = True
                    objFundAdjust._Voiduserunkid = mintUserunkid
                    objFundAdjust._Voiddatetime = dtCurrentDateTime
                    objFundAdjust._Voidreason = mstrVoidreason
                    'Sohail (12 Jan 2019) -- Start
                    'AT Testing - 76.1 - Fixing AT Issues.
                    With objFundAdjust
                        ._FormName = mstrFormName
                        ._LoginEmployeeunkid = mintLoginEmployeeunkid
                        ._ClientIP = mstrClientIP
                        ._HostName = mstrHostName
                        ._FromWeb = mblnIsWeb
                        ._AuditUserId = mintAuditUserId
                        ._CompanyUnkid = mintCompanyUnkid
                        ._AuditDate = mdtAuditDate
                    End With
                    'Sohail (12 Jan 2019) -- End
                    If objFundAdjust.Delete(intFundAdjust, dtCurrentDateTime, objDataOperation) = False Then
                        If objFundAdjust._Message <> "" Then
                            exForce = New Exception(objFundActivityMaster._Message)
                            Throw exForce
                        End If
                        If objDataOp Is Nothing Then objDataOperation.ReleaseTransaction(False)
                        Return False
                    End If
                End If
            End If
            'Sohail (22 Nov 2016) -- End

            If objDataOp Is Nothing Then objDataOperation.ReleaseTransaction(True)
            Return True
        Catch ex As Exception
            If objDataOp Is Nothing Then objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Delete; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If objDataOp Is Nothing Then objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sohail
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isUsed(ByVal intUnkid As Integer) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "<Query>"

            objDataOperation.AddParameter("@fundactivityadjustmentunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isUsed; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sohail
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isExist(ByVal dtTransactionDate As Date, ByVal intFundActivityunkid As Integer, Optional ByVal intfundactivityadjustmentunkid As Integer = -1, Optional ByVal blnIgnorePaymentTransactions As Boolean = False) As Boolean
        'Sohail (23 May 2017) - [blnIgnorePaymentTransactions]
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "SELECT " & _
                      "  fundactivityadjustmentunkid " & _
                      ", fundactivityunkid " & _
                      ", transactiondate " & _
                      ", currentbalance " & _
                      ", incrdecramount " & _
                      ", remark " & _
                      ", newbalance " & _
                      ", userunkid " & _
                      ", isvoid " & _
                      ", voiduserunkid " & _
                      ", voiddatetime " & _
                      ", voidreason " & _
                   "FROM bgfundactivityadjustment_tran " & _
                   "WHERE isvoid = 0 AND CONVERT(CHAR(8),transactiondate,112) >= @transactiondate " & _
                      "  AND fundactivityunkid = @fundactivityunkid "

            If intfundactivityadjustmentunkid > 0 Then
                strQ &= " AND fundactivityadjustmentunkid <> @fundactivityadjustmentunkid "
            End If

            'Sohail (23 May 2017) -- Start
            'Enhancement - 67.1 - Link budget with Payroll.
            If blnIgnorePaymentTransactions = True Then
                strQ &= " AND paymenttranunkid > 0 AND globalvocunkid > 0 "
            End If
            'Sohail (23 May 2017) -- End

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@transactiondate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(dtTransactionDate).ToString)
            objDataOperation.AddParameter("@fundactivityunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intFundActivityunkid)
            objDataOperation.AddParameter("@fundactivityadjustmentunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intfundactivityadjustmentunkid)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isExist; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    'Sohail (22 Nov 2016) -- Start
    'Enhancement #24 -  65.1 - Increase / decrease fund project code balance on activity adjustment as per comment given by andrew.
    Public Function isLastTransaction(ByVal intFundActivityunkid As Integer, ByRef dtTransactionDate As Date, Optional ByVal intFundActivityAdjustmentUnkid As Integer = 0) As Boolean

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation
        objDataOperation.ClearParameters()

        Try
            strQ = "SELECT  bgfundactivityadjustment_tran.fundactivityadjustmentunkid  " & _
                          ", bgfundactivityadjustment_tran.fundactivityunkid " & _
                          ", bgfundactivityadjustment_tran.transactiondate " & _
                    "FROM    bgfundactivityadjustment_tran " & _
                    "WHERE   bgfundactivityadjustment_tran.isvoid = 0 "

            If intFundActivityunkid > 0 Then
                strQ &= " AND bgfundactivityadjustment_tran.fundactivityunkid = @fundactivityunkid "
                objDataOperation.AddParameter("@fundactivityunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intFundActivityunkid)
            End If

            If dtTransactionDate <> Nothing Then
                strQ &= " AND CONVERT(CHAR(8), bgfundactivityadjustment_tran.transactiondate, 112) > @transactiondate "
                objDataOperation.AddParameter("@transactiondate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(dtTransactionDate).ToString)
            End If

            If intFundActivityAdjustmentUnkid > 0 Then 'For Edit Delete mode
                strQ &= " AND bgfundactivityadjustment_tran.fundactivityadjustmentunkid > @fundactivityadjustmentunkid "
                objDataOperation.AddParameter("@fundactivityadjustmentunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intFundActivityAdjustmentUnkid)
            End If

            strQ &= " ORDER BY bgfundactivityadjustment_tran.transactiondate DESC "

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If dsList.Tables(0).Rows.Count > 0 Then
                dtTransactionDate = dsList.Tables(0).Rows(0).Item("transactiondate")
            End If

            Return dsList.Tables(0).Rows.Count <= 0

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isLastTransaction; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function
    'Sohail (22 Nov 2016) -- End

    ''' <summary>
    ''' Modify By: Sohail
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function InsertATFundActivityAdjustmentTran(ByVal objDataOperation As clsDataOperation, ByVal intAuditType As enAuditType, _
                                               ByVal dtCurrentDateTime As DateTime) As Boolean

        Dim strQ As String = ""
        Dim exForce As Exception

        Try
            strQ = "INSERT INTO atbgfundactivityadjustment_tran ( " & _
                       "  fundactivityadjustmentunkid " & _
                       ", fundactivityunkid " & _
                       ", transactiondate " & _
                       ", currentbalance " & _
                       ", incrdecramount " & _
                       ", remark " & _
                       ", newbalance " & _
                       ", paymenttranunkid " & _
                       ", globalvocunkid " & _
                       ", audittype " & _
                       ", audituserunkid " & _
                       ", auditdatetime " & _
                       ", ip " & _
                       ", machine_name " & _
                       ", form_name " & _
                       " " & _
                       " " & _
                       " " & _
                       " " & _
                       " " & _
                       ", isweb" & _
                       ", loginemployeeunkid " & _
                   ") VALUES (" & _
                       "  @fundactivityadjustmentunkid " & _
                       ", @fundactivityunkid " & _
                       ", @transactiondate " & _
                       ", @currentbalance " & _
                       ", @incrdecramount " & _
                       ", @remark " & _
                       ", @newbalance " & _
                       ", @paymenttranunkid " & _
                       ", @globalvocunkid " & _
                       ", @audittype " & _
                       ", @audituserunkid " & _
                       ", @auditdatetime " & _
                       ", @ip " & _
                       ", @machine_name " & _
                       ", @form_name " & _
                       " " & _
                       " " & _
                       " " & _
                       " " & _
                       " " & _
                       ", @isweb" & _
                       ", @loginemployeeunkid " & _
                   ") "
            'Sohail (23 May 2017) - [paymenttranunkid, globalvocunkid]

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@fundactivityadjustmentunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintFundActivityAdjustmentunkid.ToString)
            objDataOperation.AddParameter("@fundactivityunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintFundactivityunkid.ToString)
            objDataOperation.AddParameter("@transactiondate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, IIf(mdtTransactionDate <> Nothing, mdtTransactionDate.ToString, DBNull.Value))
            objDataOperation.AddParameter("@currentbalance", SqlDbType.Decimal, eZeeDataType.FLOAT_SIZE, mdecCurrentBalance.ToString)
            objDataOperation.AddParameter("@incrdecramount", SqlDbType.Decimal, eZeeDataType.FLOAT_SIZE, mdecIncrDecrAmount.ToString)
            objDataOperation.AddParameter("@remark", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, mstrRemark.ToString)
            objDataOperation.AddParameter("@newbalance", SqlDbType.Decimal, eZeeDataType.FLOAT_SIZE, mdecNewBalance.ToString)
            objDataOperation.AddParameter("@audittype", SqlDbType.Int, eZeeDataType.INT_SIZE, intAuditType)
            objDataOperation.AddParameter("@audituserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAuditUserId)
            objDataOperation.AddParameter("@auditdatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtAuditDate)
            objDataOperation.AddParameter("@ip", SqlDbType.NVarChar, eZeeDataType.IP_SIZE, mstrClientIP)
            objDataOperation.AddParameter("@machine_name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrHostName)
            objDataOperation.AddParameter("@paymenttranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPaymenttranunkid.ToString)
            objDataOperation.AddParameter("@globalvocunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintGlobalvocunkid.ToString)
            objDataOperation.AddParameter("@form_name", SqlDbType.NVarChar, 500, mstrFormName)
            objDataOperation.AddParameter("@isweb", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsWeb)
            objDataOperation.AddParameter("@loginemployeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLoginEmployeeunkid.ToString)

            objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: InsertATFundActivityAdjustmentTran , Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
        End Try
        Return True
    End Function

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Public Shared Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "WEB")
            Language.setMessage(mstrModuleName, 2, "Please define another Transaction date. Reason: Transaction date must be greater than last fund adjustments date.")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class